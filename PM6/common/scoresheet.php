<?php
/**
 * AA-584 - Updated to include Period Under Assessment column - only impacts KPA section.  [JC] April 2021
 * Required variables:
 * @var string $page_action = "SELF" || "MOD" || "FINAL" || "REPORT"
 * @var string $page_section = self || moderation || final || "report"
 * @var array $user_ids = array()
 * @var int $assess_id
 * @var array $onscreen_display=array()
 * @var PM6_HEADINGS $headingObject - from inc_header
 * @var array $module_setup - from inc_header
 * @var PM6_DISPLAY $displayObject - from inc_header
 */
//require_once("inc_header.php");
//error_reporting(-1);
//$create_type = "PROJ";
//$create_name = "Project";


//ASSIST_HELPER::arrPrint($module_setup);
//$head = $headingObject->getScoreHeadingsForDisplay("KPI");


$triggerObj = new PM6_TRIGGER();
$assessObj = new PM6_ASSESSMENT();
$error = false;
$is_view_page = false;

$s = "";

$disable_final_cc_scoring = false;
$auto_final_score_from_mod = false;

if($page_action == $triggerObj->getViewType() || $page_action == $triggerObj->getReportType()) {
	$is_view_page = true;
} elseif($page_action == $triggerObj->getFinalReviewType()) {
	//if in final review then check for necessary setup preferences
	$prefObject = new PM6_SETUP_PREFERENCES();
	$disable_final_cc_scoring = $prefObject->getAnswerToQuestionByCode("final_cc_scoring");
	$auto_final_score_from_mod = $prefObject->getAnswerToQuestionByCode("auto_final_scoring");
}


$weighted_score_rounding_precision = $triggerObj->getWeightedScoreRoundingPrecision();


$secondary_page_section = !isset($secondary_page_section) ? false : $secondary_page_section;
$object_ids = $assessObj->getRelatedIDs($assess_id, $page_action, $user_ids, $secondary_page_section);
if(!isset($object_ids['scorecard']) && $is_view_page) {
	$object_ids = $assessObj->getRelatedIDs($assess_id, strtoupper($page_section), $user_ids, $secondary_page_section);
}
if(!isset($object_ids['scorecard'])) {
	if($page_action == $triggerObj->getReportType()) {
		$assessObj->displayResult(array("error", "Assessment not yet completed."));
		$error = true;
	} else {
		$assessObj->displayResult(array("error", "An error occurred while getting the scoresheet information for assessment ".$assess_id.".  Please reload the page and try again."));
		exit();
	}
}
//echo "<hr /><h1 class='red'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($object_ids);
//echo "<hr />";

//assessment_start_date is used to check if source deleted items are applicable to this assessment or not.  #AA-568 JC
$assessment_start_date = $object_ids['assess_start_date'];
//echo "<hr /><h1 class='orange'>Helllloooo $assessment_start_date from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";


if(!$error) {


	$self_name = $triggerObj->getTypeName($triggerObj->getSelfType());
	$mod_name = $triggerObj->getTypeName($triggerObj->getModeratorType());
	$moderator_name = $triggerObj->replaceAllNames("|MODERATOR|");
	$moderators_name = $triggerObj->replaceAllNames("|MODERATORS|");
	$final_name = $triggerObj->getTypeName($triggerObj->getFinalReviewType());


	$scd_id = $object_ids['scorecard'];
	$trigger_id = $object_ids['trigger'];
	$period_id = $object_ids['period'];


	$time_periods = $assessObj->getTimePeriodsForAssessment($period_id, $assess_id);
	$tp = array_keys($time_periods);
	$start_time_id = $tp[0];
	$max = $tp[count($tp) - 1];
	$time_ids = array();
	$final_time_id = 0;
	for($t = 1; $t <= $max; $t++) {
		$time_ids[] = $t;
		$final_time_id = $t;
	}
	// #AA-584 - If first time id is not 1 then assume that Period Under Assessment column must display - JC 2 April 2021
	//if($start_time_id>1) {
		$display_pua = true;
	//} else {
	//	$display_pua = false;
	//}






	$scdObj = new PM6_SCORECARD();
	$sources = $scdObj->getKPASources();
	$scorecard = $scdObj->getAObjectSummary($scd_id);
	$kpas = $scdObj->getKPAs();

	$bonus_scale_id = $scorecard['bonus_scale_id'];
	$sdbp5Obj = new SDBP6_PMS();
	$head = $sdbp5Obj->getGenericHeadings($scdObj->getPrimaryKPASource(), true, false, SDBP6_DEPTKPI::OBJECT_TYPE);
	unset($head['main']['sys_ref']);
	$kpi_field_prefix = $sdbp5Obj->getKPITableField();
	$top_field_prefix = $sdbp5Obj->getTopTableField();
//$kpas = $sdbp5Obj->getKPAs($scdObj->getPrimaryKPASource());
	$results_head = $headingObject->getResultsHeadingsForDisplay("KPI");
	$head['results'] = $results_head['results'];
//ASSIST_HELPER::arrPrint($results_head);

//echo $assess_id;
//ASSIST_HELPER::arrPrint($scorecard);
//ASSIST_HELPER::arrPrint($object_ids);

$scoresheet_page_heading = $scdObj->getAssessmentScoreHeading($scd_id, $assess_id, $trigger_id,false);
$trigger_details = $scoresheet_page_heading['trigger'];
$current_trigger_status = $trigger_details['raw_status'];

	$echo = array(
		'start' => $scoresheet_page_heading['display'],
		'end' => "",
		'form' => array(
			'start' => "",
			'end' => ""
		),
		'kpa' => "",
		'cc' => "",
		'jal' => "",
		'bonus' => "",
		'dashboard' => "",
		'js' => "",
	);

	$echo['js'] .= "
<script type='text/javascript'>
	var current_trigger_status = ".$current_trigger_status.";
$(function() {
	
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.99;
	var ifrHeight = (dlgHeight-50)*0.99;

	var validScore = [\"1\",\"2\",\"3\",\"4\",\"5\"];
	var validDecimal = [\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"0\"];
	var validPoint = ";
	$echo['js'] .= "\"";
	$echo['js'] .= ".";
	$echo['js'] .= "\"";
	$echo['js'] .= ";";
//above 4 lines done as multiline to avoid PhPStorm giving an error.  Original code: var validPoint = \".\";

$layout_js = "";

//if view then display table with assessment status so that user knows what has been completed
	$final_is_complete = false;
	$mod_is_complete = false;
	if($is_view_page) {
		$assessment_status = $triggerObj->getTriggersByParentID($assess_id);
		$assessment_status = $assessment_status[$assess_id];
//	ASSIST_HELPER::arrPrint($assessment_status);
		?>
		<div id=div_status>
			<h5><?php echo $triggerObj->replaceAllNames("|ASSESSMENT| Status"); ?></h5>
			<table>
				<tr>
					<td class=b><?php echo $self_name; ?>:</td>
					<td><?php
						$type = $triggerObj->getSelfType();
						if(isset($assessment_status[$type]['status'])) {
							echo ASSIST_HELPER::displayIconAsDiv($assessment_status[$type]['status_icon'])." ".$assessment_status[$type]['status'];
						} else {
							echo ASSIST_HELPER::getDisplayIconAsDiv("warn")." Not yet triggered";
						}
						?></td>
				</tr>
				<tr>
					<td class=b><?php echo $mod_name; ?>:</td>
					<td><?php
						$type = $triggerObj->getModeratorType();
						if(isset($assessment_status[$type]) && count($assessment_status[$type]) > 0) {
							$mod_is_complete = true;
							$status_echo = array();
							if(count($assessment_status[$type]) == 1) {
								foreach($assessment_status[$type] as $mod_id => $mod) {
									$status_echo[] = ASSIST_HELPER::getDisplayIconAsDiv($mod['status_icon'])." ".$mod['status'];
									if($mod['is_complete'] === false) {
										$mod_is_complete = false;
									}
								}
							} else {
								foreach($assessment_status[$type] as $mod_id => $mod) {
									$status_echo[] = $moderator_name." ".$mod_id.": ".ASSIST_HELPER::getDisplayIconAsDiv($mod['status_icon'])." ".$mod['status'];
								}
							}
							echo implode("<br />", $status_echo);//echo $assessment_status[$type]['status'];
						} else {
							echo ASSIST_HELPER::getDisplayIconAsDiv("warn")." Not yet triggered";
						}
						?></td>
				</tr>
				<tr>
					<td class=b><?php echo $final_name; ?>:</td>
					<td><?php
						$type = $triggerObj->getFinalReviewType();
						if(isset($assessment_status[$type]['status'])) {
							$final_is_complete = $assessment_status[$type]['is_complete'];
							echo ASSIST_HELPER::displayIconAsDiv($assessment_status[$type]['status_icon'])." ".$assessment_status[$type]['status'];
						} else {
							echo ASSIST_HELPER::getDisplayIconAsDiv("warn")." Not yet triggered";
						}
						?></td>
				</tr>
			</table>
		</div>
		<?php
	} else {


$options = array('id'=>"display_ytd",'name'=>"display_ytd",'select_next'=>false);
$ytd_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$ytd_button['js'];
$options = array('id'=>"display_pua",'name'=>"display_pua",'select_next'=>false);
$pua_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$pua_button['js'];


?>
	<div id=div_status>
		<h5><?php echo $triggerObj->replaceAllNames("|ASSESSMENT| Display"); ?></h5>
		<table id="tbl_status_display">
			<tr class="b" id="tr_head">
				<td></td>
				<td>Status</td>
				<td class="no-print">Display</td>
			</tr>
			<tr>
				<td class=b>Year to <?php echo $time_periods[$max]; ?>:</td>
				<td class="center">-</td>
				<td><?php echo $ytd_button['display']; ?></td>
			</tr>
			<tr>
				<td class=b>Period Under Assessment:</td>
				<td class="center">-</td>
				<td><?php echo $pua_button['display']; ?></td>
			</tr>
		</table>
	</div>
<?php


	}
//don't call scores on view page if final not yet complete - this is to stop the scoresheet from displaying non-final scores as the final scores and calculating performance bonus on self/mod scores
	$scoreObj = new PM6_SCORE();
	if($final_is_complete) {
		$final_trigger_id = $object_ids['trigger'];
		$scores = $scoreObj->getScoresForCompletingAssessment($final_trigger_id);
	} elseif(!$is_view_page) {
		$scores = $scoreObj->getScoresForCompletingAssessment($trigger_id);
	} else {
		$scores = array();
	}


	if($page_action != $triggerObj->getSelfType()) {
		$self_scores = $triggerObj->getSelfScoresByAssessmentID($assess_id);
	}

	if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
		$mod_scores = $triggerObj->getModeratorScoresByAssessmentID($assess_id);
		$mod_averages = $mod_scores['average'];
		unset($mod_scores['average']);
	}
//unset($head['main']['kpi_natkpaid']);


//echo "<hr /><h1 class='orange'>Helllloooo $start_time_id from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($time_periods);
//ASSIST_HELPER::arrPrint($time_ids);
//echo "<hr />";


//echo $page_action." ".$trigger_id;
//ASSIST_HELPER::arrPrint($object_ids);
//	ASSIST_HELPER::arrPrint($scores);


	$echo['form']['start'] .= "
	<form name=frm_assessment>
		<input type=hidden name=assess_id value=\"$assess_id\" />
		<input type=hidden name=trigger_id value=\"$trigger_id\" id=trigger />
		<input type=hidden name=trigger_type value=\"$page_action\" />
		<input type=hidden name=existing_scores value=\"".count($scores)."\" />
		<input type=hidden name=post_action id=post_action value=\"PENDING\" />
	";

	ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


	$lineObj = new PM6_LINE();
	//Active_only = false so that the function will return all line details just in case a line has been deleted subsequently to the assessment being triggered #AA-568 JC
	$lines = $lineObj->getFullLinesByType($scd_id, true, $time_ids, "SDBIP", $module_setup,false,$start_time_id);
	$weights = $lineObj->getLineWeights($scd_id);
	$kpa_weights = $lineObj->getKPAWeightsByType($scd_id);
//	$types = array("KPI", "PROJ", "TOP");
//	$names = array('KPI' => "KPIs", 'TOP' => "Top Layer KPIs", 'PROJ' => "Projects");
	$types = $scdObj->getObjectTypes();
	$names = $scdObj->getObjectNames();
	$colspan = 0;
	$scoring_span = 2;

//echo "<hr /><h1 class='orange'>Helllloooo $assessment_start_date from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//$x = array_keys($lines);
//$y = $lines[$x[0]]['KPI'];
//ASSIST_HELPER::arrPrint($y);
//die("<h1 class=orange>Death at ".__LINE__."</h1>");
//ASSIST_HELPER::arrPrint($kpas);
//ASSIST_HELPER::arrPrint($head);
//work out is KPA section must be displayed...
	$display_kpa = false;
	foreach($kpas as $k => $a) {
		foreach($types as $kt) {
			if(isset($lines[$k][$kt]) && count($lines[$k][$kt])) {
				$display_kpa = true;
			}
		}
	}


	if($display_kpa) {
		$section = "KPA";

		$echo['kpa'] .= "
<h3>KPAs</h3>
<table id=tbl_kpa class=list>";
		$table_heading = "
	<tr>
		<th rowspan=2>Ref</th>";
		foreach($head['main'] as $fld => $name) {
			$table_heading .= "<th rowspan=2>".$name."</th>";
			$colspan++;
		}
		$table_heading .= "<th rowspan=2>KPA<br />Weight</th>
		<th rowspan=2>Overall<br />Weight</th>";
		$colspan += 2;
		$table_heading .= "<th class='ytd_title' colspan=".count($head['results']).">Year To ".$time_periods[$final_time_id]."</th>";
		$colspan++;
		if($display_pua) {
			$table_heading .= "<th class=pua_title colspan=".count($head['results']).">Period Under Assessment</th>";
			$colspan++;
		}

		if($page_action != $triggerObj->getSelfType()) {
			$table_heading .= "<th colspan=2>$self_name</th>";
		}
		if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
			$table_heading .= "<th colspan=".(count($mod_scores)).">$moderators_name</th><th rowspan=2>Average $moderators_name Score</th>";
			$scoring_span++;
			$colspan++;
		}
		if($page_action == $triggerObj->getViewType()) {
			$table_heading .= "<th rowspan=2>Average $moderators_name Score</th>";
			$scoring_span++;
			$colspan++;
		}
		if($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
			$table_heading .= "
			<th colspan=2>$final_name</th>
			<th rowspan=2>KPA Weighted Score</th>
			<th rowspan=2>Overall Weighted Score</th>";
		} elseif($page_action == $triggerObj->getFinalReviewType()) {
			$table_heading .= "
			<th colspan=2>$final_name</th>";
		} else {
			$table_heading .= "
			<th rowspan=2>Score</th>
			<th rowspan=2>Comment</th>
			";
		}
		$table_heading .= "</tr>
	<tr>";
		$colspan += 2;
		/*foreach($time_periods as $ti => $tp) {
			foreach($head['results'] as $fld => $h) {
				$table_heading.="<th >".$h."</th>";
				$colspan++;
			}
		}*/
		foreach($head['results'] as $fld => $h) {
			$table_heading .= "<th class='ytd_col'>".$h."</th>";
			$colspan++;
			$scoring_span++;
		}
		if($display_pua) {
			foreach($head['results'] as $fld => $h) {
				$table_heading .= "<th class='pua_col' >".$h."</th>";
				$colspan++;
				$scoring_span++;
			}
		}
		//If not Self Assessment then display the extra column headings for the SELF section
		if($page_action != $triggerObj->getSelfType()) {
			$table_heading .= "<th>Score</th><th>Comment</th>";
			$colspan += 2;
			$scoring_span += 2;
		}
		//If in Final / Reporting  then display the extra columns for the Moderators - Not needed for Manage > View (individual mods don't display)
		if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
			foreach($mod_scores as $mt) {
				$table_heading .= "<th>$moderator_name ".$mt['id']."</th>";
				$colspan++;
				$scoring_span++;
			}
		}
		if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
			$table_heading .= "<th>Score</th><th>Comment</th>";
			$colspan += 2;
			$scoring_span += 2;
		}
		$table_heading .= "
	</tr>
	<tbody>
	";

		$grand_tot = 0;
		$auto_final_scores = array();
		$total_score = array();

		$default_score = $triggerObj->getDefaultScore();

		foreach($kpas as $k => $a) {
			foreach($types as $kt) {
				$tot = 0;
				$kpa_score = 0;
				$kpa_o_score = 0;
				if(isset($lines[$k][$kt]) && count($lines[$k][$kt])) {
					$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
					$echo['kpa'] .= $table_heading."<tr class=subth><td colspan=".$colspan.">".$a." - ".$names[$kt]."</td></tr>";

					$objects = $lines[$k][$kt];
					foreach($objects as $i => $obj) {
						//AA-584
						$is_scored_ytd = ($obj['results']['YTD']['r']['id'] > 0) && $obj['active'] == 1;
						if($display_pua) {
							$is_scored_pua = ($obj['results']['PUA']['r']['id'] > 0) && $obj['active'] == 1;
							$is_scored = ($is_scored_ytd || $is_scored_pua);
						} else {
							$is_scored = $is_scored_ytd;
						}
						//Check if is_deleted is set AND deleted_date falls BEFORE assessment trigger date (very first trigger / assessment.insertdate)
						//If both conditions are met then mark line as deleted & not to be scored #AA-568 JC
						if($obj['is_deleted']==true && strlen($obj['deleted_date'])>0 && strtotime($obj['deleted_date'])<strtotime($assessment_start_date)) {
							$is_scored = false;
							$is_deleted = true;
						} else {
							$is_deleted = false;
						}
						$tr_class = "";
						$note = "";
						if($is_deleted) {
							$tr_class = "inactive";
							$note = "Deleted at source.";
						} elseif(!$is_scored) {
							$tr_class = "disabled";
							$note = "Not applicable for this period.";
						}
						$echo['kpa'] .= "
			<tr class='".$tr_class."'>
				<td class='b center'><span class=spn_ref>".$obj['ref']."</span><br /><span style='font-weight:normal' class='i'>[Line:&nbsp;".$lineObj->getRefTag().$i."]</span><br /><button style='margin-top: 5px' class='btn_view' obj_id=".$i.">View</button></td>";
						foreach($head['main'] as $generic_fld => $name) {
							if(isset($obj[$kpi_field_prefix."_".$generic_fld])) {
								$fld = $kpi_field_prefix."_".$generic_fld;
							} elseif(isset($obj[$top_field_prefix."_".$generic_fld])) {
								$fld = $top_field_prefix."_".$generic_fld;
							} else {
								$fld = $generic_fld;
							}
							/*if(!isset($obj[$fld])) {
								echo "<h1 class=red>".$fld." == ".$i."</h1>";
								ASSIST_HELPER::arrPrint($obj);
							}*/
							$str = $obj[$fld];
							if(strlen($str) > 100) {
								$str = "<span id=spn_".$fld.$i."_full title='".$str."'>".substr(ASSIST_HELPER::decode($str), 0, 100)."...</span><span class='expander orange' id='spn_".$fld.$i."' action=open>&nbsp;[+]</span>";
							}
							$echo['kpa'] .= "<td>".$str."</td>";
						}
						$w = (isset($weights[$i]) ? $weights[$i] : 1);
						$kpaw = $kw>0?round(($w / $kw) * 100, $weighted_score_rounding_precision):0;
						$echo['kpa'] .= "
				<td id=td_".$k.$kt."_".$i." class=right>".$kpaw."%</td>
				<td class=right><div style='width:60px'>".$w."%</div></td>";

						$ti = "YTD";
						foreach($head['results'] as $fld => $h) {
							if($fld == "result") {
								$echo['kpa'] .= "<td class='ytd_col'><div style='text-align: center; padding: 3px; background-color: ".$obj['results'][$ti]['r']['color']."; color: #FFFFFF'>".$obj['results'][$ti]['r']['code']."</div></td>";
							} else {
								$echo['kpa'] .= "<td class='ytd_col'>".$obj['results'][$ti][$fld]."</td>";
							}
						}
						//#AA-584
						if($display_pua) {
							$ti = "PUA";
							foreach($head['results'] as $fld => $h) {
								if($fld == "result") {
									$echo['kpa'] .= "<td class='td_pua pua_col'><div style='text-align: center; padding: 3px; background-color: ".$obj['results'][$ti]['r']['color']."; color: #FFFFFF'>".$obj['results'][$ti]['r']['code']."</div></td>";
								} else {
									$echo['kpa'] .= "<td class='td_pua pua_col'>".$obj['results'][$ti][$fld]."</td>";
								}
							}
						}


						if($page_action != $triggerObj->getSelfType()) {
							if(!$is_scored) {
								$c = 0;
								if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
									$c = 2 + count($mod_scores) + 1;
								} elseif($page_action == $triggerObj->getViewType()) {
									$c = 2 + 1;
								} else {
									$c = 2 + ($page_action == $triggerObj->getModeratorType() ? 2 : 0);
								}
								$echo['kpa'] .= "<td class=center colspan=".$c.">";
								$echo['kpa'] .= $note;
								$echo['kpa'] .= "</td>";
							} else {
								$echo['kpa'] .= "<td class=center>".(isset($self_scores[$i]) && $self_scores[$i]['score'] > 0 ? number_format(round($self_scores[$i]['score'], 2), 2) : "N/A")."</td><td>".(isset($self_scores[$i]) ? $self_scores[$i]['comment'] : "N/A")."</td>";
							}
						}
						if($is_scored && ($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType())) {
							if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
								foreach($mod_scores as $mt) {
									$echo['kpa'] .= "<td class=center>".(isset($mt['scores'][$i]) && ($mt['scores'][$i]['score']) > 0 ? number_format(round($mt['scores'][$i]['score'], 2), 2) : "N/A").($page_action == $triggerObj->getFinalReviewType() && isset($mt['scores'][$i]) && strlen($mt['scores'][$i]['comment']) > 0 ? "<span style='cursor:pointer' title=\"".($mt['scores'][$i]['comment'])."\">*</span>" : "")."</td>";
								}
							}
							//if auto populating of final review scores has been selected, then save
							if($auto_final_score_from_mod) {
								$auto_final_scores[$i] = (isset($mod_averages[$i]) ? round($mod_averages[$i], $weighted_score_rounding_precision) : false);
							}
							$echo['kpa'] .= "<td class=center>".(isset($mod_averages[$i]) ? round($mod_averages[$i], $weighted_score_rounding_precision) : "N/A")."</td>";
						}

						if($is_view_page) {
							if(!$is_scored) {
								$scores[$i] = array('score' => $default_score,
									'comment' => "Default value applied.",);
							}
							//only process scores on view page if final has been done.
							if($final_is_complete) {
								$s = isset($scores[$i]['score']) ? $scores[$i]['score'] : "N/A";
								$k_score = (isset($scores[$i]) ? round(($scores[$i]['score'] * $kpaw / 100), $weighted_score_rounding_precision) : 0);
								$kpa_score += $k_score;
								$o_score = (isset($scores[$i]) ? round(($scores[$i]['score'] * $w / 100), $weighted_score_rounding_precision) : 0);
								//$total_score+=$o_score;
								$kpa_o_score += $o_score;
								$s_c = (isset($scores[$i]) ? $scores[$i]['comment'] : "");
							} elseif(!$is_scored) {
								$s = "N/A";
								$s_c = "N/A";
							} else {
								$s = "N/A";
								$k_score = "N/A";
								$o_score = "N/A";
								$s_c = "<span class=i style='font-size:85%;line-height:100%;color:#ababab;'>No final score available as $final_name has not yet been completed. </span>";
							}
							$echo['kpa'] .= "
							<td class=center>".$s."</td>
							<td >".$s_c."</td>
							<td class=center>".$k_score."</td>
							<td class=center>".$o_score."</td>
						";
						} else {
							if($is_scored) {
								if(isset($scores[$i]) && count($scores[$i]) > 0) {
									if(isset($scores[$i]['score']) && $scores[$i]['score']>0) {
										$s = number_format($scores[$i]['score'], 2);
									} elseif($page_action == $triggerObj->getFinalReviewType() && isset($auto_final_scores[$i]) && $auto_final_scores[$i] !== false) {
										$s = $auto_final_scores[$i];
									}
									$s_id = $scores[$i]['id'];
									$s_c = isset($scores[$i]['comment']) ? $scores[$i]['comment'] : "";
								} elseif($page_action == $triggerObj->getFinalReviewType() && isset($auto_final_scores[$i]) && $auto_final_scores[$i] !== false) {
									$s = $auto_final_scores[$i];
									$s_id = "";
									$s_c = "";
								} else {
									$s = "";
									$s_id = "";
									$s_c = "";
								}
								/** Next_field attribute added to get about Chrome bug that puts blur events into infinite loops (see link in related comment in JS section) [JC - 11 May 2019] */
								$echo['kpa'] .= "
								<td class=right><input type=text name=kpa_score[$i] class='txt_score section_".$section."' next_field='kpa_comment_".$i."' size=5 value='".((strlen($s)==0 && (!$is_scored_pua&$is_scored_ytd))?$default_score:$s)."' line_id='$i' /> 
									<input type=hidden name=score_id[$i] value='".$s_id."' id='score_id_".$i."' /></td>
								<td class=right>
									<textarea rows=7 cols=40 class='txt_comment section_".$section."' id='kpa_comment_".$i."' name='kpa_comment[$i]' line_id='$i' >".(strlen($s_c)==0 && (!$is_scored_pua&$is_scored_ytd)?"Applicable in prior period. Default score applied.":$s_c)."</textarea>
								".((!$is_scored_pua&$is_scored_ytd)?"<span class=i>[Applicable in prior period]</span>":"")."
								</td>
							";
							} elseif($page_action == $triggerObj->getFinalReviewType()) {
								$echo['kpa'] .= "
								<td class=right>".$default_score."</td>
								<td class=right>Default value applied.</td>
							";
							} elseif($page_action == $triggerObj->getSelfType()) {
								$echo['kpa'] .= "<td class=center colspan=2>".$note."</td>";
							}
						}
						$echo['kpa'] .= "</tr>";
						$tot += (isset($weights[$i]) ? $weights[$i] : 1);
					}//end foreach
					$total_score[$kt][$k] = $kpa_o_score;
					//$total_score[$a." - ".$names[$kt]] = $kpa_o_score;
					$echo['kpa'] .= "
					<tr class=total>
						<th class='right b' colspan=".(count($head['main']) + 1).">Total ".$a." - ".$names[$kt]." Weight:</th>
						<th class=right>100%</th>
						<th class='right kpa_tot' id=td_".$k.$kt.">".$tot."%</th>";
					if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
						$scor_span = $scoring_span - 2;
						$echo['kpa'] .= "
							<th class=right colspan=".($scor_span).">Total Score:</th>
							<th class=center>".$kpa_score."</th>
							<th class=center>".$kpa_o_score."</th>
							";
					} else {
						$echo['kpa'] .= "
							<th class=right colspan=".($scoring_span + count($head['results']) * count($time_periods))."></th>";
					}
					$echo['kpa'] .= "
					</tr>";
				}
				$grand_tot += $tot;
			}
		}

		$t_score = 0;
		foreach($total_score as $ky => $ts) {
			$t_score += array_sum($ts);
		}

		$echo['kpa'] .= "
	</tbody>
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=".(1 + count($head['main'])).">Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot>".$grand_tot."%</th>";
		if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
			$scor_span = $scoring_span - 1;
			$echo['kpa'] .= "
			<th class=right colspan='".($scor_span)."'>Total Score:</th>
			<th class=center>".($t_score)."</th>";
		} else {
			$echo['kpa'] .= "<th class=right colspan='".($scoring_span + count($head['results']) * count($time_periods))."'></th>";
		}
		$echo['kpa'] .= "
		</tr>
	</tfoot>
</table>";


	}//end if kpas exist


	$sources = $scdObj->getJALSources();


	$primary_source = $scdObj->getPrimaryJALSource();
	if($primary_source !== false) {
		$jalObj = new JAL1_PMS();
		$functions = $jalObj->getKPAs($primary_source);
		$head = $jalObj->getHeadings($primary_source, true, false);
		unset($head['main']['activity_kpi_link']); //$jalObj->arrPrint($head['main']);
		unset($head['main']['activity_subfunction_id']);
		$lines = $lineObj->getFullLinesForJAL($scd_id,false);
		$weights = $lineObj->getLineWeights($scd_id);
	} else {
		$functions = array();
		$head = array();
		$lines = array();
		$weights = array();
	}
	$create_names = "Activities";
	$create_name = "Activity";
	$create_type = "JAL";
//echo "<hr /><h1 class='blue'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//	$scdObj->arrPrint($lines);

	$c = 0;
	$types = array("JAL",);
	$display_jal = false;
	if(count($lines) > 0 && count($functions) > 0) {
		foreach($functions as $k => $a) {
			if(isset($lines[$k]) && count($lines[$k]) > 0) {
				$objects = $lines[$k];
				$display_jal = true;
				foreach($objects as $i => $obj) {
					$c++;
				}
			}
		}
	}
	if($c > 0) {
		$blank = false;
		$default_weight = round(100 / $c, $weighted_score_rounding_precision);
	} else {
		$blank = true;
		$default_weight = 0;
	}
	$names = array('JAL' => "Activities");


	$total_jal_score = array();

	if($display_jal) {

		$section="JAL";
		$echo['jal'] .= "
<h3>Function Activities</h3>
<table id=tbl_jal class=list>
";

		if($page_action == $triggerObj->getModeratorType() || $page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
			$rowspan = 2;
		} else {
			$rowspan = 1;
		}

		$auto_final_scores = array();
		$grand_tot = 0;
		$visible = false;
		$kt = "JAL";
		foreach($functions as $k => $a) {
			$tot = 0;
			$kpa_score = 0;
			$kpa_o_score = 0;

			if(isset($lines[$k][$kt]) && count($lines[$k][$kt]) > 0) {
				$scoring_span = 2;
				$colspan = 0;
				$objects = $lines[$k][$kt];
				$visible = true;
				$kw = isset($kpa_weights[$k][$kt]) && !($kpa_weights[$k][$kt] < 0) ? $kpa_weights[$k][$kt] : count($objects) * $default_weight;
				$echo['jal'] .= "
		<tr>";
				foreach($head['main'] as $fld => $name) {
					$echo['jal'] .= "<th rowspan=".$rowspan.">".$name."</th>";
					$colspan++;
				}
				$echo['jal'] .= "
			<th rowspan=".$rowspan.">Function<br />Weight</th>
			<th rowspan=".$rowspan.">Overall<br />Weight</th>";
				$colspan += 2;
				if($page_action != $triggerObj->getSelfType()) {
					$echo['jal'] .= "<th colspan=2>$self_name</th>";
					$colspan += 2;
					$scoring_span += 2;
				}
				if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
					$echo['jal'] .= "<th colspan=".(count($mod_scores)).">$moderators_name</th><th rowspan=2>Average<br />$moderators_name<br />Score</th>";
					$colspan++;
					$scoring_span++;
				} elseif($page_action == $triggerObj->getViewType()) {
					$echo['jal'] .= "<th rowspan=2>Average<br />$moderators_name<br />Score</th>";
					$colspan++;
					$scoring_span++;
				}
				if($page_action == $triggerObj->getFinalReviewType()) {
					$echo['jal'] .= "
			<th colspan=2>$final_name</th>
			";
					$scoring_span++;
				} elseif($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
					$echo['jal'] .= "
			<th colspan=2>$final_name</th>
			<th rowspan=2>Function Weighted Score</th>
			<th rowspan=2>Overall Weighted Score</th>
			";
					$scoring_span += 2;
				} else {
					$echo['jal'] .= "
			<th rowspan=".$rowspan.">Score</th>
			<th rowspan=".$rowspan.">Comment</th>
			";
				}

				$echo['jal'] .= "
		</tr>";

				if($page_action != $triggerObj->getSelfType()) {
					$echo['jal'] .= "
		<tr>
			<th>Score</th>
			<th>Comment</th>";
					if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
						foreach($mod_scores as $mt) {
							$echo['jal'] .= "<th>$moderator_name ".$mt['id']."</th>";
							$colspan++;
						}
					}
					if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
						$echo['jal'] .= "
			<th>Score</th>
			<th>Comment</th>";
					}
					$echo['jal'] .= "
		</tr>";
				}
				$total_cc_score = 0;

				$echo['jal'] .= "<tr class=subth><td colspan=".($colspan + $scoring_span).">".$a."</td></tr>";
				foreach($objects as $i => $obj) {
					$is_scored = true;
					$note = "";
					if($obj['active'] == 1 && ($obj['activity_kpi_link'] == "Yes")) {
						$obj['active'] = 2;
						$is_scored = false;
						$note = "Scored in SDBIP";
					}
					//Check if is_deleted is set AND deleted_date falls BEFORE assessment trigger date (very first trigger / assessment.insertdate)
					//If both conditions are met then mark line as deleted & not to be scored #AA-568 JC
					if($obj['is_deleted']==true && strlen($obj['deleted_date'])>0 && strtotime($obj['deleted_date'])<strtotime($assessment_start_date)) {
						$is_scored = false;
						$note = "Deleted at source";
					}
					$echo['jal'] .= "
			<tr class=".(!$is_scored ? "inactive" : "").">
			";
					foreach($head['main'] as $fld => $name) {
						if($fld == "ref") {
							$echo['jal'] .= "
						<td class='b center'><span class=spn_ref>".$obj['ref']."</span>
						<br /><span style='font-weight:normal' class='i'>[Line:&nbsp;".$lineObj->getRefTag().$i."]</span><br /><button style='margin-top: 5px' class=btn_view obj_id=".$i.">View</button>
						</td>";


						} else {

							$echo['jal'] .= "
						<td>
							".$obj[$fld].(
								$fld == "ref" && $obj['active'] == 0 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
								)."
							</td>";
						}
					}
					$w = (isset($weights[$i]) && !($weights[$i] < 0) ? $weights[$i] : $default_weight);
					$tot += $w;
					$kpaw = ($kw > 0 ? round(($w / $kw) * 100, $weighted_score_rounding_precision) : 0);
					$echo['jal'] .= "
				<td id=td_".$k.$kt."_".$i." class='jal_line_perc right'>".(count($objects) > 1 ? $kpaw : "100.00")."%</td>
				<td class=right>".$w."%</td>";
					if($page_action != $triggerObj->getSelfType()) {
						if(!$is_scored) {
							$c = 0;
							if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
								$c = 2 + count($mod_scores) + 1;
							} elseif($page_action == $triggerObj->getViewType()) {
								$c = 2 + 1;
							} else {
								$c = 2 + ($page_action == $triggerObj->getModeratorType() ? 2 : 0);
							}
							$echo['jal'] .= "<td class=center colspan=".$c.">";
							$echo['jal'] .= $note;
							$echo['jal'] .= "</td>";
						} else {
							$echo['jal'] .= "<td class=center>".(isset($self_scores[$i]) && $self_scores[$i]['score'] > 0 ? number_format(round($self_scores[$i]['score'], 2), 2) : "N/A")."</td><td>".(isset($self_scores[$i]) ? $self_scores[$i]['comment'] : "N/A")."</td>";
						}
					}
					if($is_scored && ($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType())) {
						if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
							foreach($mod_scores as $mt) {
								$echo['jal'] .= "<td class=center>".(isset($mt['scores'][$i]) && ($mt['scores'][$i]['score']) > 0 ? number_format(round($mt['scores'][$i]['score'], 2), 2) : "N/A").($page_action == $triggerObj->getFinalReviewType() && isset($mt['scores'][$i]) && strlen($mt['scores'][$i]['comment']) > 0 ? "<span style='cursor:pointer' title=\"".($mt['scores'][$i]['comment'])."\">*</span>" : "")."</td>";
							}
						}
						//if auto populating of final review scores has been selected, then save
						if($auto_final_score_from_mod) {
							$auto_final_scores[$i] = (isset($mod_averages[$i]) ? round($mod_averages[$i], $weighted_score_rounding_precision) : false);
						}
						$echo['jal'] .= "<td class=center>".(isset($mod_averages[$i]) ? round($mod_averages[$i], $weighted_score_rounding_precision) : "N/A")."</td>";
					}

					if($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
						if(!$is_scored) {
							$scores[$i] = array(
								'score' => $default_score,
								'comment' => "Default value applied.",
							);
						}
						$k_score = (isset($scores[$i]) ? round(($scores[$i]['score'] * $kpaw / 100), $weighted_score_rounding_precision) : 0);
						$kpa_score += $k_score;
						$o_score = (isset($scores[$i]) ? round(($scores[$i]['score'] * $w / 100), $weighted_score_rounding_precision) : 0);
						//$total_score+=$o_score;
						$kpa_o_score += $o_score;
						$echo['jal'] .= "
					<td class=center>".(isset($scores[$i]) ? $scores[$i]['score'] : "")."</td>
					<td >".(isset($scores[$i]) ? $scores[$i]['comment'] : "")."</td>
					<td class=center>".$k_score."</td>
					<td class=center>".$o_score."</td>
				";
					} else {
						if($is_scored) {
							if(isset($scores[$i]) && count($scores[$i]) > 0) {
								if(isset($scores[$i]['score']) && $scores[$i]['score']>0) {
									$s = number_format($scores[$i]['score'], 2);
								} elseif($page_action == $triggerObj->getFinalReviewType() && isset($auto_final_scores[$i]) && $auto_final_scores[$i] !== false) {
									$s = $auto_final_scores[$i];
								}
//								$s = isset($scores[$i]['score']) && $scores[$i]['score']>0 ? number_format($scores[$i]['score'], 2) : "";
								$s_id = $scores[$i]['id'];
								$s_c = isset($scores[$i]['comment']) ? $scores[$i]['comment'] : "";
							} elseif($page_action == $triggerObj->getFinalReviewType() && isset($auto_final_scores[$i]) && $auto_final_scores[$i] != false) {
								$s = $auto_final_scores[$i];
								$s_id = "";
								$s_c = "";
							} else {
								$s = "";
								$s_id = "";
								$s_c = "";
							}
							/** Next_field attribute added to get about Chrome bug that puts blur events into infinite loops (see link in related comment in JS section) [JC - 11 May 2019] */
							$echo['jal'] .= "
						<td class=right><input type=text name=jal_score[$i] class='txt_score section_".$section."' size=5 next_field='jal_comment_".$i."' value='".$s."' line_id='$i'  /> 
							<input type=hidden name=score_id[$i] value='".$s_id."' id='score_id_".$i."' /></td>
						<td class=right><textarea rows=7 cols=40 class='txt_comment section_".$section."' name=jal_comment[$i] id='jal_comment_".$i."' line_id='$i' >".($s_c)."</textarea></td>
					";
						} elseif($page_action == $triggerObj->getFinalReviewType()) {
							$echo['jal'] .= "
						<td class=right>".$default_score."</td>
						<td class=right>Default value applied.</td>
					";
						} elseif($page_action == $triggerObj->getSelfType()) {
							$echo['jal'] .= "<td class=center colspan=2>".$note."</td>";
						}
					}
					$echo['jal'] .= "</tr>";
				}
				$total_jal_score[$names[$kt]][$k] = $kpa_o_score;
				$echo['jal'] .= "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." Weight:</th>
			<th class=right>100%</th>
			<th class='right kpa_tot'>".$tot."%</th>";
				if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
					$scor_span = $scoring_span - 2;
					$echo['jal'] .= "
				<th class=right colspan=".($scor_span).">Total Score:</th>
				<th class=center>".$kpa_score."</th>
				<th class=center>".$kpa_o_score."</th>
				";
				} else {
					$echo['jal'] .= "
				<th class=right colspan=".($scoring_span+1)."></th>";
				}
				$echo['jal'] .= "
		</tr>";
				$grand_tot += $tot;
			}
		}


		$echo['jal'] .= "
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=".(count($head['main'])).">Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot>".$grand_tot."%</th>";
		if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
			$scor_span = $scoring_span - 1;
			$echo['jal'] .= "
			<th class=right colspan='".($scor_span)."'>Total Score:</th>
			<th class=center>".array_sum($total_jal_score[$names[$kt]])."</th>";
		} else {
			$echo['jal'] .= "<th class=right colspan='".($scoring_span+1)."'></th>";
		}
		$echo['jal'] .= "
		</tr>
	</tfoot>

</table>
";
	} else {    //if display_jal==true
	}


	$echo['cc'] .= "
<h3>Core Competencies</h3>
";
	$display_cc = true;
$section = "CC";
	$total_cc_score = 0;
//$listObj = new PM6_LIST("competencies");
//$objects = $listObj->getListItems();
	$listObj = new PM6_LIST("competency_category");
	$category_objects = $listObj->getActiveListItemsFormattedForSelect();
	$listObj->changeListType("competencies");
	$competency_objects = $listObj->getListItemsGroupedByParent($listObj->getParentField());

	$lines = $lineObj->getLineSrcIDs($scd_id, $lineObj->getModRef(), "CC");

	$ccsObj = new PM6_CC_SCORE();
	$ccs_progress = $ccsObj->getProgressOfScoring($trigger_id);

//ASSIST_HELPER::arrPrint($lines);


	if($page_action == $triggerObj->getModeratorType() || $page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
		$rowspan = 2;
	} else {
		$rowspan = 1;
	}
	$echo['cc'] .= "
<table class=list id=tbl_cc>";
	$colspan = 3;
	$cate_colspan = 6;
	$echo['cc'] .= "
	<tr>
		<th rowspan=".$rowspan.">Ref</th>
		<th rowspan=".$rowspan.">Core Competency</th>
		<th rowspan=".$rowspan.">Description</th>
		<th rowspan=".$rowspan.">Weight</th>";
	if($page_action != $triggerObj->getSelfType()) {
		$echo['cc'] .= "<th colspan=2>$self_name</th>";
		$colspan += 2;
		$cate_colspan += 2;
	}
	if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
		$echo['cc'] .= "<th colspan=".(count($mod_scores)).">$moderators_name</th><th rowspan=2>Average<br />$moderators_name<br />Score</th>";
		$colspan++;
		$cate_colspan++;
	} elseif($page_action == $triggerObj->getViewType()) {
		$echo['cc'] .= "<th rowspan=2>Average<br />$moderators_name<br />Score</th>";
		$colspan++;
		$cate_colspan++;
	}
	if($page_action == $triggerObj->getFinalReviewType()) {
		$echo['cc'] .= "
			<th colspan=2>$final_name</th>
			";
	} elseif($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
		$echo['cc'] .= "
			<th colspan=2>$final_name</th>
			<th rowspan=2>Weighted Score</th>
			";
		$cate_colspan++;
	} else {
		$echo['cc'] .= "
			<th rowspan=".$rowspan.">Score</th>
			<th rowspan=".$rowspan.">Comment</th>
			";
	}
	$echo['cc'] .= "
	</tr>";
	if($page_action != $triggerObj->getSelfType()) {
		$echo['cc'] .= "
		<tr>
			<th>Score</th>
			<th>Comment</th>";
		if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
			foreach($mod_scores as $mt) {
				$echo['cc'] .= "<th>$moderator_name ".$mt['id']."</th>";
				$colspan++;
				$cate_colspan++;
			}
		}
		if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
			$echo['cc'] .= "
			<th>Score</th>
			<th>Comment</th>";
		}
		$echo['cc'] .= "
		</tr>";
	}
	$tot = 0;
	$auto_final_scores = array();
	foreach($category_objects as $cate_id => $cate) {
		$objects = $competency_objects[$cate_id];
		if(count($objects) > 0) {
			$display_section = false;
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$display_section = true;
					break;
				}
			}
			if($display_section) {
				$echo['cc'] .= "<tr class=subth><td colspan='".$cate_colspan."'>$cate</td></tr>";
			}
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$li = $lines[$i];
					$w = isset($weights[$li]) ? $weights[$li] : 1;
					$tot += $w;
					$echo['cc'] .= "
			<tr cc_id=".$i." line_id=".$li.">
				<td class='center b'>".$lineObj->getCCRefTag(true).$obj['id']."<br /><span  style='font-weight:normal' class='i'>[Line:&nbsp;".$lineObj->getRefTag().$li."]</span></td>
				<td>".$obj['name']."</td>
				<td>".$obj['description']."</td>
				<td class=right>".$w."%</td>";
					if($page_action != $triggerObj->getSelfType()) {
						$echo['cc'] .= "<td class=center>".(isset($self_scores[$li]) && $self_scores[$li]['score'] > 0 ? number_format(round($self_scores[$li]['score'], 2), 2) : "N/A")."</td><td>".(isset($self_scores[$li]) ? $self_scores[$li]['comment'] : "N/A")."</td>";
						if($page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType()) {
							foreach($mod_scores as $mt) {
								$echo['cc'] .= "<td class=center>".(isset($mt['scores'][$li]) && ($mt['scores'][$li]['score']) > 0 ? number_format(round($mt['scores'][$li]['score'], 2), 2) : "N/A").($page_action == $triggerObj->getFinalReviewType() && isset($mt['scores'][$i]) && strlen($mt['scores'][$i]['comment']) > 0 ? "<span style='cursor:pointer' title=\"".($mt['scores'][$i]['comment'])."\">*</span>" : "")."</td>";
							}
							//if auto populating of final review scores has been selected, then save
							if($auto_final_score_from_mod) {
								$auto_final_scores[$li] = (isset($mod_averages[$li]) ? round($mod_averages[$li], $weighted_score_rounding_precision) : false);
							}
							$echo['cc'] .= "<td class=center>".(isset($mod_averages[$li]) ? round($mod_averages[$li], $weighted_score_rounding_precision) : "N/A")."</td>";
						} elseif($page_action == $triggerObj->getViewType()) {
							$echo['cc'] .= "<td class=center>".(isset($mod_averages[$li]) ? round($mod_averages[$li], $weighted_score_rounding_precision) : "N/A")."</td>";
						}
					}
					if($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
						if(isset($scores[$li]['score'])) {
							$o_score = round(($scores[$li]['score'] * $w / 100), $weighted_score_rounding_precision);
						} else {
							$o_score = 0;
						}

						$total_cc_score += $o_score;
						$echo['cc'] .= "
						<td class=center>".(isset($scores[$li]) ? $scores[$li]['score'] : "")."</td>
						<td>".(isset($scores[$li]) ? $scores[$li]['comment'] : "")."</td>
						<td class=center>".(isset($scores[$li]) ? $o_score : "")."</td>
					</tr>";
					} else {
						/* Remove fancy scoring popup if doing final review [JC/RK - 11 May 2019] */
						if($page_action == $triggerObj->getFinalReviewType() && $disable_final_cc_scoring == true) {
							if(isset($scores[$li]) && count($scores[$li]) > 0) {
//								$s = isset($scores[$li]['score']) && $scores[$li]['score']>0 ? number_format($scores[$li]['score'], 2) : "";
								if(isset($scores[$li]['score']) && $scores[$li]['score']>0) {
									$s = number_format($scores[$li]['score'], 2);
								} elseif($page_action == $triggerObj->getFinalReviewType() && isset($auto_final_scores[$li]) && $auto_final_scores[$li] !== false) {
									$s = $auto_final_scores[$li];
								}
								$s_id = $scores[$li]['id'];
							} elseif(isset($auto_final_scores[$li]) && $auto_final_scores[$li] != false) {
								$s = $auto_final_scores[$li];
								$s_id = "";
							} else {
								$s = "";
								$s_id = "";
							}
							/** Next_field attribute added to get about Chrome bug that puts blur events into infinite loops (see link in related comment in JS section) [JC - 11 May 2019] */
							$echo['cc'] .= "
							<td class=right>
								<input type=text name=cc_score[$li] class='txt_score section_".$section."' size=5 value='".$s."'  next_field='cc_comment_".$li."' line_id='$li'  /> 
								<input type=hidden name=score_id[$li] value='".$s_id."' id='score_id_".$li."' />
							</td>
							<td><textarea rows=3 cols=40 class='txt_comment section_".$section."' name=cc_comment[$li] id='cc_comment_".$li."' line_id='$li' >".(isset($scores[$li]) ? $scores[$li]['comment'] : "")."</textarea></td>
						</tr>";
							/*								<input type=text disabled=disabled class=display_score id=display_cc_".$i." name=display_cc_score[$li] size=5 value='$s' />
															<br /><button class=btn_cc_score style='margin-top:3px'>Score</button>
															<input type=hidden name=cc_score[$li] id=cc_score_".$i." size=5 value='".(isset($scores[$li]) ? $scores[$li]['score'] : "")."' />
															<input type=hidden name=score_id[$li] value='".(isset($scores[$li]) ? $scores[$li]['id'] : "")."' />*/

						} else {
							if(isset($scores[$li]) && count($scores[$li]) > 0) {
								$s = isset($scores[$li]['score']) && $scores[$li]['score']>0 ? number_format($scores[$li]['score'],0) : "";
								$s_id = $scores[$li]['id'];
							} elseif(isset($ccs_progress[$li]) && count($ccs_progress[$li]) > 0) {
								$s = "TBC";
								$s_id = isset($scores[$li]['id']) ? $scores[$li]['id'] : "";
							} else {
								$s = "";
								$s_id = "";
							}
							$echo['cc'] .= "
							<td class=center>
								<input type=text disabled=disabled class=display_score id=display_cc_".$i." name=display_cc_score[$li] size=5 value='$s' />
								<br /><button class=btn_cc_score style='margin-top:3px'>Score</button>
								<input type=hidden class=hidden_cc_score name=cc_score[$li] id=cc_score_".$i." size=5 value='".(isset($scores[$li]) ? number_format($scores[$li]['score'],0) : "")."' line_id='".$li."' />
								<input type=hidden name=score_id[$li] value='".(isset($scores[$li]) ? $scores[$li]['id'] : "")."' id='score_id_".$li."' />
							</td>
							<td><textarea rows=3 cols=40 name='cc_comment[$li]'  line_id='$li'>".(isset($scores[$li]) ? $scores[$li]['comment'] : "")."</textarea></td>
						</tr>";
						}
					}
				}
			}
		}
	}
	$echo['cc'] .= "
	<tr class=total>
		<td colspan=2></td>
		<td class=right>Total weight:</td>
		<td class=right id=td_tot>".$tot."%</td>";
	if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
		$echo['cc'] .= "<td colspan='".($colspan - 1)."' class=right>Total Score:</td><td class=center>".$total_cc_score."</td>";
	} else {
		$echo['cc'] .= "<td colspan='$colspan'></td>";
	}
	$echo['cc'] .= "
	</tr>
</table>
<input type=hidden id=cc_score_saved value='na' />
<div id=dlg_cc_scoring>
	<iframe id=ifr_cc_scoring></iframe>
</div>

";


	if($page_action != $triggerObj->getReportType() && $page_action != $triggerObj->getViewType()) {
//	<p class='center'><span style='float:left; font-size:75%' class=i>Page drawn on: ".date("d F Y H:i")."</span><span class=no-print><button id=btn_save_pending>Save & Continue Later</button>&nbsp;&nbsp;&nbsp;<button id=btn_save_submit>Save & Finalise</button></span>
		$echo['form']['end'] .= "
	<p class='center'><span class=no-print><button id=btn_save_pending>Save & Continue Later</button>&nbsp;&nbsp;&nbsp;<button id=btn_save_submit>Save & Finalise</button></span>
	<span id='spn_last_saved' style='border:1px solid black;width:150px;float:right'></span>
	</p>
	";
	} elseif($final_is_complete !== true) {
		$echo['dashboard'] .= "<p>&nbsp;</p>".ASSIST_HELPER::getDisplayResult(array("info", "Please note that the Performance Dashboard with the final scores and Bonus calculation will only be available once the $final_name has been completed."));
	} else {
		$bonusperc = $assessObj->getBonusRatingTable($bonus_scale_id);
		$echo['bonus'] .= "
	<div class=float>
		".$bonusperc."
	</div>";

		$echo['dashboard'] .= "
	<div>
	<h3>Performance Dashboard</h3>
	";
		$component = $displayObject->getComponentWeightingForScoring($scd_id, $scorecard, isset($total_score) ? $total_score : array(),
			isset($total_jal_score[$names['JAL']]) ? $total_jal_score[$names['JAL']] : array(),
			isset($total_cc_score) ? $total_cc_score : array(),
			false
		);
		$echo['dashboard'] .= $component['display'];
		$echo['js'] .= $component['js'];

		$echo['dashboard'] .= "

	</div>
			";
	}


	$echo['form']['end'] .= "


	</form>

	";
	$echo['end'] .= "

<p class='i' style='font-size:90%'>Page drawn on ".date("d F Y H:i:s")."</p>


	<div id=dlg_view title='View'>
		<iframe id='ifr_view' style='border: 0px solid #cc0001;padding:0px;margin:0px;'>

		</iframe>
</div>
";


//ASSIST_HELPER::arrPrint($_REQUEST);

	$echo['js'] .= "".$layout_js."
	$('#ifr_view').css({'width':ifrWidth+'px','height':ifrHeight+'px'});
	$('#dlg_view').dialog({
		modal: true,
		autoOpen: false,
		width: dlgWidth,
		height: dlgHeight,
		close: function() {
			var r = AssistHelper.doAjax('inc_iframe_session_fix.php','action=FIX');
			$('#ifr_view').prop('src','').html('');
		}
	});

	//CC scoring
	$('#ifr_cc_scoring').css({'width':'100%','height':ifrHeight+'px','border':'1px solid #ffffff'});
	$('#dlg_cc_scoring').dialog({
		modal: true,
		autoOpen: false,
		width: dlgWidth,
		height: dlgHeight,
		beforeClose: function() {
			if($('#cc_score_saved').val()=='na') {
				if(confirm('Are you sure you wish to close? Any changes since your last save will be lost!')==true) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		},
		close: function() {
			$('#ifr_cc_scoring').prop('src','').html('');
			// un-lock scroll position
			var html = jQuery('html');
			//var scrollPosition = html.data('scroll-position');
			html.css('overflow', html.data('previous-overflow'));
			//window.scrollTo(scrollPosition[0], scrollPosition[1])
		}
	});

	$('.btn_cc_score').button().click(function(e) {
		e.preventDefault();
		var title = $(this).parent().parent().find('td:eq(1)').html()+' Scoring';
		var cc_id = $(this).parent().parent().attr('cc_id');
		var line_id = $(this).parent().parent().attr('line_id');
		var trigger_id = $('#trigger').val();
		var dta = 'cc_id='+cc_id+'&line='+line_id+'&trigger='+trigger_id;
		$('#dlg_cc_scoring').dialog({'title':title});
		$('#ifr_cc_scoring').prop('src','manage_cc_scoresheet.php?'+dta);
		$('#cc_score_saved').val('na');

		var scrollPosition = [
		  self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
		  self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
		];
		var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
		html.data('scroll-position', scrollPosition);
		html.data('previous-overflow', html.css('overflow'));
		html.css('overflow', 'hidden');
		//window.scrollTo(scrollPosition[0], scrollPosition[1]);




		$('#dlg_cc_scoring').dialog('open');
	}).css({'font-size':'75%'});



	$('table.noborder, table.noborder td').css('border','0px');






	

	$('.txt_score').blur(function() {
		$(this).removeClass('required');
		$(this).prop('title','');
		var r = validateScore('check',$(this));
		/** required to get around Chrome bug which puts blur events into infinite loop - see: https://groups.google.com/a/chromium.org/forum/#!topic/chromium-discuss/2LqXp2DMIIs **/
		var next_field = $(this).attr('next_fld');
		$('#'+next_field).focus();
		/** end chrome bug hack **/
		if(r[0]) {
			//alert(r[1]);
			$(this).addClass('required');
			$(this).prop('title',r[1]);
		} else {
			tempSave('SCORE',$(this));
		}
	});
	function validateScore(action,$"."obj) {
		$"."obj.removeClass('required');
		var val = $"."obj.val();
		var err = false;
		var errMsg = '';
		//if len = 1
		if(val.length==1) {
			//validate only 1, 2, 3, 4, 5
			if($.inArray(val,validScore)<0 || (val==0 && $('#post_action').val()=='SUBMIT')) {
				err = true;
				errMsg = 'Invalid score';
			}
		//else if len = 0
		} else if(val.length==0) {
			//required field
			if($('#post_action').val()!='PENDING') {
				err = true;
				errMsg = 'All scores are required';
			}
		//else if len = 2 || > 4
		} else if(val.length==2 || val.length > 4) {
			//error - invalid number
			err = true;
			err = true;
			errMsg = 'Invalid number format.  Only 1 - 5 with 2 decimal places are permitted.';
			errMsg = 'Invalid number format.  Only 1 - 5 with 2 decimal places are permitted.';
		//else
		} else {
			if($.inArray(x,validScore)==4 && (val.charAt(2)!='0' && val.charAt(3)!='0')) {
				err = true;
				errMsg = 'Invalid score.  Maximum score permitted is '+validScore[4]+'.00.';
			} else {
				var x = '';
				var start = '';
				for(i=0;i<val.length;i++) {
					x = val.charAt(i);
					switch(i) {
						case 0:
							//validate that 1st char is 1, 2, 3, 4, 5
							start = x;
							if($.inArray(x,validScore)<0) {
								err = true;
								errMsg = 'Invalid score.  Only 1 - 5 with 2 decimal places are permitted.';
							}
							break;
						case 1:
							//validate that 2nd char is '.'
							if(x!=validPoint) {
								err = true;
								errMsg = 'Invalid decimal point marker (2nd character).';
							}
							break;
						case 2:
						case 3:
							//validate that 3rd char is 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
							//validate that 4th char, if present, is 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
							if($.inArray(x,validDecimal)<0) {
								err = true;
								errMsg = 'Invalid score.  Only 1 - 5 with 2 decimal places are permitted.';
							}
							break;
					}
					if(err) {
						break;
					}
				}
			}
		}
		if(err) {
			$"."obj.addClass('required');
		}
		return [err,errMsg];
	}



	$('.btn_view').button({
		icons: {primary: 'ui-icon-newwin'},
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var obj_id = $(this).attr('obj_id');
		var ref = $(this).parent().find('span.spn_ref').html();
		//console.log(':'+obj_id+':');
		//console.log(':'+ref+':');
		$('#dlg_view').dialog('option','title','View: '+ref);
		var url = 'view_line.php?width='+ifrWidth+'&height='+ifrHeight+'&obj_id='+obj_id;
		//console.log(url);
		$('#ifr_view').prop('src',url);
		setTimeout(function(){ 
			$('#dlg_view').dialog('open'); 
			AssistHelper.closeProcessing();
		}, 5000);
	}).children('.ui-button-text').css({'padding-top':'4px','padding-bottom':'4px','padding-left':'25px','font-size':'80%'
	});



	$('#btn_save_submit').button({
		icons: {primary: 'ui-icon-disk', secondary: 'ui-icon-check'},
	}).click(function(e) {
		e.preventDefault();
		if(confirm('This will mark the Assessment as complete and you will no longer be able to make any changes.  Are you sure you wish to continue?')==true) {
			$('#post_action').val('SUBMIT');
			processForm('SUBMIT',$(this));
		}
	}).removeClass('ui-state-default').addClass('ui-button-state-ok').css({'color':'#009900','border':'1px solid #009900'
	});

	$('#btn_save_pending').button({
		icons: {primary: 'ui-icon-disk'},
	}).click(function(e) {
		e.preventDefault();
		$('#post_action').val('PENDING');
		processForm('PENDING',$(this));
	}).removeClass('ui-state-default').addClass('ui-button-state-info').css({'color':'#fe9900','border':'1px solid #fe9900'
	});
	";
$echo['js'] .= "
	function processForm(post_action,$"."src_object) {
	";
		if(isset($displaying_in_iframe) && $displaying_in_iframe===true) {
$echo['js'] .= "
		window.parent.showProcessing();
";
		} else {
$echo['js'] .= "
		AssistHelper.processing();
";
		}
$echo['js'] .= "
			var err = false;
			var errs = [];
			var errMsg = '';
			if(post_action=='SUBMIT') {
				$('.txt_score').each(function() {
					var r = validateScore('form',$(this));
					if(r[0]==true) {
						err = true;
					}
				});
			}
			if(err) {
				errMsg = 'There are invalid scores entered. Only 1 - 5 with 2 decimal places are permitted. Please review the scores highlighted.';
	";
		if(isset($displaying_in_iframe) && $displaying_in_iframe===true) {
$echo['js'] .= "
				window.parent.finishProcessing(errMsg);
";
		} else {
$echo['js'] .= "
				AssistHelper.finishedProcessing('error',errMsg);
";
		}
$echo['js'] .= "
			} else {
				var dta = AssistForm.serialize($('form[name=frm_assessment]'));
//				console.log(dta);
				var result = AssistHelper.doAjax('inc_controller_assessment.php?action=Trigger.saveScore',dta);
				if(result[0]=='ok') {";
				//return to FPD or go back to manage page AA-560 JC 11 March 2021
		if(isset($return_to_fpd) && $return_to_fpd===true) {
			$echo['js'] .= "
					parent.header.$('#backHome').trigger('click');
					";
			//else redirect to folowon function if in iframe so that parent window is triggered instead
		} elseif(isset($displaying_in_iframe) && $displaying_in_iframe===true) {
			$echo['js'].="
				window.parent.redirectToNextPage(result[1]);
			";
		} else { //normal routing if not from front page dashboard AA-560 JC 11 March 2021
			$echo['js'] .= "
					var url = 'manage_".$page_section.".php?r[]=ok&r[]='+result[1];
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
					";
		}
		$echo['js'] .= "
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
	}

//temp save comments
	$('#spn_last_saved').hide();
	$('textarea').blur(function() {
		var type_action = 'COMMENT';
		$(this).val(AssistString.code($(this).val()));
		tempSave(type_action,$(this));
		$(this).val(AssistString.decode($(this).val()));
	});



	$('.expander').css('cursor','pointer').click(function() {
		var i = $(this).prop('id');
		var a = $(this).attr('action');

		var t = $('#'+i+'_full').html();
		var r = $('#'+i+'_full').prop('title');

		$('#'+i+'_full').html(r);
		$('#'+i+'_full').prop('title',t);

		if(a=='open') {
			$(this).html('&nbsp;[-]');
			$(this).attr('action','close');
		} else {
			$(this).html('&nbsp;[+]');
			$(this).attr('action','open');
		}
	});

".($is_view_page?"
	$('#div_status').addClass('float').css({'position':'relative','bottom':'50px','border':'1px solid #fe9900','border-radius':'5px','padding':'5px'}).find('h5').addClass('orange').css('margin-bottom','10px');
	$('#div_status').find('table').css('margin-right','10px');
":"
	$('#div_status').addClass('float').css({'position':'relative','bottom':'0px','border':'1px solid #fe9900','border-radius':'5px','padding':'5px'}).find('h5').addClass('orange').css('margin-bottom','10px');
	$('#div_status').find('table').css('margin-right','10px');
	$('#tbl_status_display #tr_head td').addClass('b center');
	$('#tbl_status_display td').css('vertical-align','middle').css('padding','5px');
	$('#tbl_status_display, #tbl_status_display td').addClass('noborder');
	$('#tbl_status_display tr').find('td:last').addClass('no-print');
	$('#tbl_status_display tr').find('td:not(:last)').css('padding-right','20px');
	$('#div_status').css('padding','15px');


	$('#display_ytd_yes').click(function() {
		if(!($('.ytd_title:first').is(':visible'))) {
			$('.ytd_title').show();
			$('.ytd_col').show();
			resetColspan('SHOW','RESULTS');
		}
	});
	$('#display_ytd_no').click(function() {
		if($('.ytd_title:first').is(':visible')) {
			$('.ytd_title').hide();
			$('.ytd_col').hide();
			resetColspan('HIDE','RESULTS');
		}
	});


	$('#display_pua_yes').click(function() {
		if(!($('.pua_title:first').is(':visible'))) {
			$('.pua_title').show();
			$('.pua_col').show();
			resetColspan('SHOW','RESULTS');
		}
	});
	$('#display_pua_no').click(function() {
		if($('.pua_title:first').is(':visible')) {
			$('.pua_title').hide();
			$('.pua_col').hide();
			resetColspan('HIDE','RESULTS');
		}
	});


	function resetColspan(act,typ) {
".($display_kpa?"
		//KPA - change 2 columns if SCORE else 4 columns for YTD/PUA (RESULTS) #AA-584
		var c = 0;
		$('#tbl_kpa tr:first').find('th:visible').each(function() {
			c+=($(this).prop('colspan'))*1;
		});
//		var c = $('#tbl_kpa tr:eq(3)').find('td:visible').length;
		$('#tbl_kpa').find('tr.subth').find('td').prop('colspan',c);
		var t = $('#tbl_kpa').find('tr.total:first').find('th:eq(3)').prop('colspan');
		if(act=='SHOW') { t=t+(typ=='SCORE'?2:4); } else { t=t-(typ=='SCORE'?2:4); }
		$('#tbl_kpa').find('tr.total').find('th:eq(3)').prop('colspan',t);
		$('#tbl_kpa').find('tr.gtotal').find('th:eq(3)').prop('colspan',t+1);
":"")."
".($display_jal?"
		//JAL - only change if typ==SCORE (no impact if YTD/PUA are being displayed) #AA-584
		if(typ=='SCORE') {
			c = $('#tbl_jal tr:eq(3)').find('td:visible').length;
			$('#tbl_jal').find('tr.subth').find('td').prop('colspan',c);
			t = $('#tbl_jal').find('tr.total:first').find('th:eq(3)').prop('colspan');
			if(act=='SHOW') { t=t+2; } else { t=t-2; }
			$('#tbl_jal').find('tr.total').find('th:eq(3)').prop('colspan',t);
			$('#tbl_jal').find('tr.gtotal').find('th:eq(3)').prop('colspan',t+1);
			var p = $('#tbl_jal').find('td.jal_note').prop('colspan');
			if(act=='SHOW') { p=p+2; } else { p=p-2; }
			$('#tbl_jal').find('td.jal_note').prop('colspan',p);
		}
":"")."
".($display_cc?"
		//CC - only change if typ==SCORE (no impact if YTD/PUA are being displayed) #AA-584
		if(typ=='SCORE') {
			c = $('#tbl_cc tr:eq(4)').find('td:visible').length;
			$('#tbl_cc').find('tr.subth').find('td').prop('colspan',c);
			if($('#tbl_cc').find('tr.total').find('td:eq(3)').is(':visible')) {
				t = $('#tbl_cc').find('tr.total:first').find('td:eq(3)').prop('colspan');
				if(act=='SHOW') { t=t+2; } else { t=t-2; }
			} else {
				t = 2;
			}
			$('#tbl_cc').find('tr.total').find('td:eq(3)').prop('colspan',t);
			if(t==0) {
				$('#tbl_cc').find('tr.total').find('td:eq(3)').hide();
				$('#tbl_cc').find('tr.total').find('td:eq(1)').html('Total:');
			} else {
				$('#tbl_cc').find('tr.total').find('td:eq(3)').show();
				$('#tbl_cc').find('tr.total').find('td:eq(1)').html('Total Weight:');
			}
		}
":"")."
	}

")."

});

function saveHiddenCCScore(element_id) {
	$(function() {
		$"."src_object = $(element_id);
		tempSave('CC_SCORE',$"."src_object);
	});
}
function tempSave(type_action,$"."src_object) {
	$(function() {
//		AssistHelper.processing();
		$('#spn_last_saved').show();
		$"."src_object.css('border-color','#fe9900');
		
		var value_to_save = AssistString.str_replace('&','|_|',$"."src_object.val());
		
		//get identifying features
		var trigger_id = $('#trigger').val();
		var line_id = $"."src_object.attr('line_id'); 
		var score_id = $('#score_id_'+line_id).val();
		if($"."src_object.hasClass('txt_score') || $"."src_object.hasClass('hidden_cc_score')) { var field_type = 'SCORE'; } else { var field_type = 'COMMENT'; }
		
		//send all variables, including record id & source id, just to act as a double check to prevent bad updates
		var dta = 'trigger_type=".$page_action."&current_trigger_status='+current_trigger_status+'&score_id='+score_id+'&trigger_id='+trigger_id+'&line_id='+line_id+'&field_type='+field_type+'&save_value='+value_to_save;
		console.log(dta);
		
		var result = AssistHelper.doAjax('inc_controller_assessment.php?action=Score.saveTempScore',dta);
//		console.log(result);
		if(result[0]=='ok') {
			if(current_trigger_status==1) { current_trigger_status = 2; }
			$"."src_object.css('border-color','#009900');
			$('#spn_last_saved').css('color','#009900');
		} else {
			$"."src_object.css('border-color','#cc0001');
			$('#spn_last_saved').css('color','#cc0001');
		}
		//restore border back to normal colour so that user can see the next time it changes to green	
		setTimeout(function () { $"."src_object.css('border-color',''); }, 3000);
		
		$('#spn_last_saved').html(''+result[1]+'');
//		AssistHelper.closeProcessing();
	});
}
";
		if(isset($displaying_in_iframe) && $displaying_in_iframe===true) {
$echo['js'] .= "
window.parent.finishedLoadingIframe();
$(function() {
function nextFunction() {
	var url = 'manage_".$page_section.".php?r[]=ok&r[]='+result[1];
	window.parent.document.location.href = url;
}
});
";
		}
$echo['js'] .= "
</script>
";


	if(!isset($onscreen_display) || !is_array($onscreen_display) || count($onscreen_display) == 0) {
		$onscreen_display = array();
		switch($page_action) {
			case $triggerObj->getSelfType():
			case $triggerObj->getModeratorType():
			case $triggerObj->getFinalReviewType():
				$onscreen_display = array(
					'form' => true,
					'kpa' => true,
					'cc' => true,
					'jal' => true,
					'bonus' => false,
					'dashboard' => false,
					'js' => true,
				);
				break;
			case $triggerObj->getReportType():
			case $triggerObj->getViewType():
			default:
				$onscreen_display = array(
					'form' => false,
					'kpa' => (isset($display_kpa) ? $display_kpa : true),
					'cc' => (isset($display_cc) ? $display_cc : true),
					'jal' => (isset($display_jal) ? $display_jal : true),
					'bonus' => (isset($display_bonus) ? $display_bonus : true),
					'dashboard' => (isset($display_dashboard) ? $display_dashboard : true),
					'js' => true,
				);
				break;
		}
	}
	echo $echo['start'];

	if($onscreen_display['form']) {
		echo $echo['form']['start'];
	}
	if($onscreen_display['kpa']) {
		echo $echo['kpa'];
	}
	if($onscreen_display['jal']) {
		echo $echo['jal'];
	}
	if($onscreen_display['cc']) {
		echo $echo['cc'];
	}
	if($onscreen_display['bonus']) {
		echo $echo['bonus'];
	}
	if($onscreen_display['dashboard']) {
		echo $echo['dashboard'];
	}
	if($onscreen_display['form']) {
		echo $echo['form']['end'];
	}
	if($onscreen_display['js']) {
		echo $echo['js'];
	}

	echo $echo['end'];
}


?>