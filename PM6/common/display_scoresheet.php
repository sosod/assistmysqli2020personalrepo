<?php
/**
 * @var string $page_action = FINAL || MOD || SELF || MAN = what are you working on?
 * @var string $page_section = final || moderation || self || manager = where are you working
 * @var PM6 $helper - from inc_header
 */

include "inc_header.php";

echo "<div id=div_message>";
ASSIST_HELPER::displayResult(array("info","Preparing scoresheet for use..."));
echo "</div>";




$assessObj = new PM6_ASSESSMENT();

//Prep a SCORE object to assist with temporary saving of scores
if(isset($_SESSION['temp_object_storage'][$_SESSION['ref']]['PM6_SCORE'])) {
	$scoreObject = unserialize($_SESSION['temp_object_storage'][$_SESSION['ref']]['PM6_SCORE']);
} else {
	$scoreObject = new PM6_SCORE();
	$_SESSION['temp_object_storage'][$_SESSION['ref']]['PM6_SCORE'] = serialize($scoreObject);
}


$user_ids = $helper->getUserID();
$assess_id = $_REQUEST['obj_id'];
$secondary_page_section = !isset($secondary_page_section) ? false : $secondary_page_section;
$object_ids = $assessObj->getRelatedIDs($assess_id, $page_action, $user_ids, $secondary_page_section);


$trigger_id = $object_ids['trigger'];
$scd_id = $object_ids['scorecard'];
$scoreObject->prepTempScores($trigger_id,$scd_id);


?>
<iframe id="ifr_scoresheet" style="width:500px;height:500px;border:0px solid #ffffff;">

</iframe>
<script type="text/javascript">
	$(function() {
		AssistHelper.processing();
		<?php
		$data = array();
		foreach($_REQUEST as $key => $value) {
			$data[] = $key."=".$value;
		}
		echo "var dta = '".implode("&",$data)."';";
		?>
		var url = "manage_<?php echo $page_section; ?>_details_frame.php?"+dta;
		$("#ifr_scoresheet").prop("src",url);
	});
	function finishedLoadingIframe() {
		$(function() {
			$("#div_message").hide();
			resizeIframe();
			AssistHelper.closeProcessing();
		});
	}
	function resizeIframe() {
		$(function () {
			var win_size = AssistHelper.getWindowSize();
			$("#ifr_scoresheet").width(win_size.width - 5);
			$("#ifr_scoresheet").height(win_size.height - 85);
		});
	}
	$(window).resize(resizeIframe);
	function showProcessing() {
		AssistHelper.processing();
	}
	function finishProcessing(errMsg) {
		AssistHelper.finishedProcessing("error",errMsg);
	}
	function hideProcessing() {
		AssistHelper.closeProcessing();
	}
	function redirectToNextPage(msg) {
		var url = "manage_<?php echo $page_section; ?>.php?r[]=ok&r[]="+msg;
		AssistHelper.finishedProcessingWithRedirect("ok",msg,url);
	}
</script>
