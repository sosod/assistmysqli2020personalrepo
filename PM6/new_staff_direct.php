<?php
require_once("inc_header.php");

//TODO - Consider impact to New > Staff section if user has a different job now than during the given year [noted on 10 July 2020 during AA-404 development by JC]

//TODO - If IP_EMPLOYEE then send to review page, don't let it jump around with anyone making changes


$info_message = "This page allows you to build a new |scorecard| for one of your immediate staff or approve/reject a new |scorecard| belonging to one of your immediate staff which has been built/amended by someone else.  Once the |scorecard| has been approved by both you and the relevant employee, the |scorecard| will then be available in the ".$helper->replaceAllNames($helper->createPageTitleBreadcrumb($helper->createMenuTrailFromLastLink("menu_manage"),"|"))." section and any further amendments and approvals will take place there.";
$info_message.="<br /><br />This staff list is drawn from the Employee Assist module where 'Immediate staff' is classified as any employee where you are indicated as their manager or, where no manager has been indicated, your job title is set as their manager and you are the only person allocated to that job title.  This does not include staff where you are acting as their manager.
<br /><br />If you believe that there is an error on this page, please contact the Employee Assist administrator or Assist Support.";
echo "<div id=div_info class='info-divs'>";
ASSIST_HELPER::displayResult(array("info",$helper->replaceAllNames($info_message)));
echo "</div>";


ASSIST_HELPER::displayResult(isset($_REQUEST['r'])?$_REQUEST['r']:array());

$settingObject = new PM6_SETUP_EMPLOYEE();
$empObject = new PM6_EMPLOYEE();
$scdObject = new PM6_SCORECARD();

//1 Does the user have access to create their own scorecard? OR is there a SCD waiting Review?
$can_i_create_active_users_staff_scorecard = $settingObject->canICreateActiveUserStaffScorecards();
$can_i_create_non_users_staff_scorecard = $settingObject->canICreateNonUserStaffScorecards();
$are_any_scorecards_waiting_for_my_review = $scdObject->areAnyScorecardsWaitingMyReview();



//Get financial year info
$start_date = $empObject->getFinancialYearStartDate();
$end_date = $empObject->getFinancialYearEndDate();

//2 Get JOB details that fall into the selected financial year
$staff_data = $empObject->getDirectStaffDetailsByUserID(false,$start_date,$end_date,$can_i_create_active_users_staff_scorecard,$can_i_create_non_users_staff_scorecard);
$jobs = isset($staff_data['jobs']) ? $staff_data['jobs'] : array();
$employee_tkids = isset($staff_data['employee_tkids']) && is_array($staff_data['employee_tkids']) ? array_keys($staff_data['employee_tkids']) : array();

//3 Get SCD details that relate to JOBs
$all_scorecards = $scdObject->getAllCurrentScorecardEmployeesAndJobsWithStatus($employee_tkids);


//4 Format Jobs & Scorecards for display
$available_objects = array();
foreach($jobs as $job) {
	$display_me = true;
	$user_id = $job['user_id'];
	$employee_user_id = $user_id;
	$my_scorecards = isset($all_scorecards[$employee_user_id]) ? $all_scorecards[$employee_user_id] : array();
	$page_id = "employee_id=";
	$page_id.= $user_id."&emp_id=";
	$emp_id = $job['emp_id'];
	$page_id.= $emp_id."&job_id=";
	$id = $job['job_id'];
	$page_id.=$id."&obj_id=".(isset($my_scorecards[$id])?$my_scorecards[$id]['id']:"0");

	$name = $job['user_name'];//TODO - name = employee name and not //$job['job_title']."";
	$raw_status = isset($my_scorecards[$id]['new_status']) ? $my_scorecards[$id]['new_status'] : PM6::NOT_YET_STARTED;
	$scd_status = isset($my_scorecards[$id]['status']) ? $my_scorecards[$id]['status'] : PM6::ACTIVE;
	$page_id.="&new_status=".$raw_status;
	$page_id.="&scd_status=".$scd_status;
	if(($scd_status&PM6::ACTIVATED)==PM6::ACTIVATED) {
		$step = "view";
	} elseif(($scd_status&PM6::CONFIRMED)==PM6::CONFIRMED) {
		$step = "review";
	} elseif(($raw_status&PM6::IP_MANAGER)==PM6::IP_MANAGER) {
		$step = "continue";
	} elseif($raw_status==0) {
		$step = "new";
		if(!$can_i_create_non_users_staff_scorecard && !$can_i_create_active_users_staff_scorecard) {
			$display_me = false;
		}
	} else {
		$step = "view";
	}
	$page_id.="&step=".$step;
	$status = $scdObject->convertNewStatusToText($raw_status);
	$general_status = $scdObject->convertSCDStatusToText($scd_status);
	//$colour = isset($my_scorecards[$id]['status']) ? "orange" : "red";
	$colour = $scdObject->convertNewStatusToColour($raw_status);
	$general_colours = $scdObject->convertSCDStatusToColour($scd_status);
	$bonus_scale = isset($my_scorecards[$id]['bonus_scale_id']) ? $my_scorecards[$id]['bonus_scale_id'] : 0;
	$page_id.="&bonus_scale_id=".$bonus_scale;
	$job_dates = date("d F Y",strtotime($job['job_start']))." - ".(($job['job_end']!="1970-01-01" && $job['job_end']!="0000-00-00") ? date("d F Y",strtotime($job['job_end'])) : "On-going");
	$extra_info_arr = array();
	if(strlen($job['job_title'])>0) {
		$extra_info_arr['Job Title'] = $job['job_title'];
	}
	if(strlen($job['job_dept'])>0) {
		$extra_info_arr['Department'] = $job['job_dept'];
	}
	if(strlen($job['job_loc'])>0) {
		$extra_info_arr['Location'] = $job['job_loc'];
	}
		$extra_info_arr['Manager'] = (strlen($job['job_manager'])>0?$job['job_manager']." / ":"").$job['job_manager_title'];

	$extra_info_arr['Dates'] = $job_dates;
	$extra_info = "";
	foreach($extra_info_arr as $fld => $value) {
		$extra_info.="<span class=b>".$fld.":</span> ".$value."<br />";
	}
	if(strlen($job['job_description'])>0) {
		$extra_info.="<br /><span class=i>".$job['job_description']."</span>";
	}
	if($display_me) {
		$available_objects[$page_id] = array(
			'name'=>$name,
			'extra_info'=>$extra_info,
			'status'=>array_merge($general_status,$status),
			'colour'=>array_merge($general_colours,$colour)
		);
	}
}


if(count($available_objects)>0) {
//5 Display SDBIP chooser style button to access SCD
	$include_status = true;
	$include_colour = false;
	$url = "new_staff_direct_start.php?";
	$button_name = "Open";
	include("common/object_chooser.php");

} else {
	ASSIST_HELPER::displayResult(array("info","No Scorecards waiting for review."));
}


?>