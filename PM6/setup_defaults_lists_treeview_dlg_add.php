<?php
$displayObject = new PM6_DISPLAY();
$js = "";
?>
<table id=ed_frm class=form>
    <?php foreach($fields as $fld => $name){ ?>
        <tr>
            <th><?php echo $name; ?>:</th>
            <td fld=edit_td_<?php echo $fld; ?> id="edit_td_<?php echo $fld; ?>">
                <?php if($fld == "status"){; ?>
                    Active
                <?php }else if($fld == "category"){ ?>
                    <?php $js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$fld, 'name'=>$fld, 'options'=>$cat_sups)); ?>
                <?php }else if($fld == "competency"){ ?>
                    <?php
                    $field_name = $fld . '_id';
                    if(isset($competency)){
                        $field_value = $competency[$competency_id]['name'];
                    }else{
                        $field_value = 'N/A';
                    }
                    echo $field_value;
                    ?>
                <?php }else if($fld == "proficiency"){ ?>
                    <?php
                    $field_name = $fld . '_id';
                    if(isset($proficiencies)){
                        $proficiency_options = array();
                        foreach($proficiencies as $key => $val){
                            $proficiency_options[$val['id']]=$val['name'];
                        }
                        $js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$field_name, 'name'=>$field_name, 'options'=>$proficiency_options), $parent_id);
                    }else{
                        $js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$field_name, 'name'=>$field_name, 'options'=>array('N/A')));
                    }
                    ?>
                <?php }else{ ?>
                    <?php $js.=$displayObject->drawFormField($types[$fld],array('id'=>"edit_".$fld, 'name'=>$fld)); ?>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
</table>

<script type="text/javascript">
    $(function() {
        <?php
        /*****************
         * echo stored $js strings here
         */
        echo  $js ;
        ?>
    });
</script>