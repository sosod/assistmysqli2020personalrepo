<?php
//$page_action = ? required from calling page
require_once("inc_header.php");
$create_step = 7;
$create_type = "JAL";
$create_name = "Activity";
$create_names = "Activities";

$scdObj = new PM6_SCORECARD();
$sources = $scdObj->getJALSources();


$primary_source = $scdObj->getPrimaryJALSource();

if($primary_source!==false) {
	$sdbp5Obj = new JAL1_PMS();
	$kpas = $sdbp5Obj->getKPAs($scdObj->getPrimaryJALSource());
	$head = $sdbp5Obj->getHeadings($scdObj->getPrimaryJALSource(), true, false);
} else {
	$head = array();
	$kpas = array();
}
	echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);
//ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
if(count($kpas)>0) {


	$ok_to_continue_as_normal = true;
	$next_step = $create_step+1;

?>
<h3>Filter</h3>
<table id=tbl_filter>
	<tr>
		<td>Source:</td>
		<td>
			<select id=sel_source>
				<option selected value=X>--- SELECT ---</option>
				<?php
				foreach($sources as $src) {
					echo "<option value=".$src['modref']." modloc='".$src['modlocation']."' >".$src['modtext']." (".$src['modref'].")</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Filter by:</td>
		<td><select id=sel_filter><option value=X selected>CHOOSE SOURCE ABOVE</option></select></td>
	</tr>
	<tr>
		<td></td>
		<td><button id=btn_filter>Go</button></td>
	</tr>
</table>
<h3>Existing <?php echo $create_names; ?></h3>
<?php
$lineObj = new PM6_LINE();
$lines = $lineObj->getAllLines($_REQUEST['obj_id'],$create_type);


$visible=false;

foreach($kpas as $k => $a) {
	if(isset($lines[$k])) {
		echo "<h4>".$a."</h4>";
		$objects = $lines[$k];
		$visible = true;
		?>
	<table id=tbl_list_<?php echo $k; ?> class=list width=100%>
	<thead>
		<tr>
			<?php
			foreach($head['main'] as $fld=>$name) {
				echo "<th>".$name."</th>";
			}
			?>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($objects as $i => $obj) {
			?>
			<tr class="<?php echo ($obj['active']==1 ? "" : "inactive"); ?>">
				<?php
				foreach($head['main'] as $fld=>$name) {
					echo "<td>
							".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."
							</td>";
				}
				?>
				<td><button class=btn_del key=<?php echo $i; ?>>Delete</button></td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
		<?php
	} else {
	//	echo "<p>No KPIs assigned.</p>";
	}
}
if(!$visible) {
	echo "<p>No ".$create_names." selected.  To select ".$create_names.", please select the appropriate filters above and click the 'Go' button.</p>";
}





} else {	//if there are kpas


	$ok_to_continue_as_normal = false;
	echo "<p>No ".$create_names." available to be selected.</p>";
	$next_step = $create_step + 2;



}



?>


<p class='center no-print'><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Next Step</button></p>





<div id=dlg_kpi title="Choose <?php echo $create_names; ?>">
	<iframe id=frm_kpi style='border:0px solid #ffffff;'>

	</iframe>
</div>

<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$_REQUEST['obj_id'],$page_redirect_url);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;


	var object_name = "<?php echo $create_name; ?>";
	var object_names = "<?php echo $create_names; ?>";


	$("#frm_kpi").css({"width":ifrWidth+"px","height":ifrHeight+"px"});

	$("table.noborder, table.noborder td").css("border","0px");
	$("#tbl_filter td:first").addClass("b");

	$("#sel_source").change(function() {
		if($(this).val()!="X") {
			AssistHelper.processing();
			var modloc = $("#sel_source option:selected").attr("modloc");
			dta = "modloc="+modloc+"&modref="+$(this).val();
			//console.log(dta);
			r = AssistHelper.doAjax("inc_controller_assessment.php?action=SRCOBJECT.getFilters",dta);
			if((r.subs!=null && r.subs!="undefined") || (r.owners!=null && r.owners!="undefined")) {
				if(r.subs.length>0 || r.owners.length>0) {
					$("#sel_filter option").remove();
					$("#sel_filter").append("<option value=X selected>--- SELECT FILTER ---</option>");
					if(r.subs.length>0) {
						if(r.names.subs.length>0) {
							$("#sel_filter").append("<option value=XS>--- "+r.names.subs+" ---</option>");
						}
						var x = r.subs;
						for(i in x) {
							$("#sel_filter").append("<option filter_type=sub value="+x[i]['id']+">"+x[i]['value']+" ("+x[i]['c']+" "+(x[i]['c']!=1?object_names:object_name)+")</option>");
						}
					}
					if(r.owners.length>0) {
						if(r.names.owners.length>0) {
							$("#sel_filter").append("<option value=XO>--- "+r.names.owners+" ---</option>");
						}
						x = r.owners;
						for(i in x) {
							$("#sel_filter").append("<option filter_type=owner value="+x[i]['id']+">"+x[i]['value']+" ("+x[i]['c']+" "+(x[i]['c']!=1?object_names:object_name)+")</option>");
						}
					}
				} else {
					$("#sel_filter option").remove();
					$("#sel_filter").append("<option value=X selected>NO FILTERS AVAILABLE FOR THE SELECTED SOURCE</option>");
				}
			} else {
				$("#sel_filter option").remove();
				$("#sel_filter").append("<option value=X selected>NO FILTERS AVAILABLE FOR THE SELECTED SOURCE</option>");
			}
			AssistHelper.closeProcessing();
		} else {
			$("#sel_filter option").remove();
			$("#sel_filter").append("<option value=X selected>CHOOSE SOURCE ABOVE</option>");
		}
	});

	$("#dlg_kpi").dialog({
		autoOpen: false,
		modal: true,
		width: dlgWidth,
		height: dlgHeight
	});

	$("#btn_filter").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).click(function() {
		AssistHelper.processing();
		var src = $("#sel_source").val();
		var ft = $("#sel_filter").val();
		if(src=="X" || ft=="X") {
			AssistHelper.finishedProcessing("error","Please select the filter.");
		} else {
			var ft_type = $("#sel_filter option:selected").attr("filter_type");
			$("#dlg_kpi #frm_kpi").prop("src","assessment_<?php echo strtolower($create_type); ?>.php?page_action=<?php echo $page_action; ?>&obj_id=<?php echo $_REQUEST['obj_id']; ?>&modref="+src+"&filter_type="+ft_type+"&filter_id="+ft);
		}

		//$("#dlg_kpi").dialog("open");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});


	$(".btn_del").button({
		icons: {primary: "ui-icon-trash"},
	}).click(function() {
		var ref = ($(this).parent().parent().find("td:first").html());
		var key = $(this).attr("key");
		if(confirm("Are you sure you want to remove <?php echo $create_name; ?> "+ref+"? "+key)==true) {
			AssistHelper.processing();
			var dta = "type=<?php echo $create_type; ?>&object_id="+key+"&ref="+ref;
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.Deactivate",dta);
			if(result[0]=="ok") {
				document.location.href = "<?php echo $page_redirect_url; ?>_step<?php echo $create_step; ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>&r[]=ok&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

	var next_url = '<?php echo $page_redirect_url; ?>_step<?php echo ($next_step); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';

	$("#btn_next").button({
		icons: {secondary: "ui-icon-arrowthick-1-e"},
	}).click(function() {
		AssistHelper.processing();
		document.location.href = next_url;
	}).removeClass("ui-state-default").addClass("ui-button-bold-green");

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		AssistHelper.processing();
		document.location.href = '<?php echo $page_redirect_url; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-bold-red");

	if($("#sel_source option").length==2) {
		//var x = $("#sel_source option:eq(1)").val();
		//$("#sel_source").val(x);
		$("#sel_source option:eq(1)").prop("selected","selected");
		$("#sel_source").trigger("change");
	}

	<?php

	if(!$ok_to_continue_as_normal) {
		echo "
		AssistHelper.processing();
		AssistHelper.finishedProcessingWithRedirect('info','No ".$create_names." available to select.  Continuing to next step.',next_url);
		";
	}

	?>


});
function openDialog() {
	$(function() {
		$("#dlg_kpi").dialog("open");
		AssistHelper.closeProcessing();
		//AssistHelper.hideDialogTitlebar("id","dlg_kpi");
	});
}
function reloadPage(r) {
	document.location.href = "<?php echo $page_redirect_url; ?>_step<?php echo $create_step; ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>&r[]=ok&r[]="+r[1];
}
</script>