<?php
require_once("inc_header.php");

$bonus_scale_id = $_REQUEST['bonus_scale_id'];
$list_id = 'bonus_scale';

$listObj = new PM6_LIST($list_id);
$fields = $listObj->getFieldNames();
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
$items = $listObj->getListTable();



$bonus_object = new PM6_SETUP_BONUS();
$bonus_scale = $bonus_object->getBonusScaleByID($bonus_scale_id);

$me = new PM6();
$scale = $me->getCustomBonusScaleItemsDirectlyFromDatabaseByBonusScaleID($bonus_scale_id, true);

?>
<h2>Bonus Scale</h2>
<table class=form id=tbl_list style="width:500px">
    <?php foreach($fields as $fld=>$name){ ?>
        <tr>
            <th style="width:150px"><?php echo $name ?>:</th>
            <?php if($fld == "status"){ ?>
                <td><?php echo "Active" ?></td>
            <?php }else{ ?>
                <td><?php echo $bonus_scale[$fld] ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
    <tr>
        <th></th>
        <td><button data-bonus_scale_id="<?php echo $bonus_scale_id; ?>" id="edit_button">Edit</button><span class="float"><button data-bonus_scale_id="<?php echo $bonus_scale_id; ?>" id="del_button">Delete</button></span></td>
    </tr>
</table>
<br/>
<br/>
<div id=dlg_scale_edit title="Integration Details">
    <iframe id=ifr_scale_edit></iframe>
</div>

<h2>Bonus Scale Items</h2>
<?php
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_bonus>
<input type="hidden" id="bonus_scale_id" name="bonus_scale_id" value="<?php echo $bonus_scale_id; ?>" />
<table id=tbl_bonus class=list>
	<tr>
		<th>Ref</th>
		<th>Bonus</th>
		<th>Performance Bracket*</th>
		<th>In Use?</th>
	</tr>

    <!--  BONUS SCALE LIST ITEM ADD FORM  -->
    <tr>
        <th></th>
        <td><?php $js.=$displayObject->drawFormField("LRGVC",array('id'=>"add_bonus",'name'=>"add_bonus"),""); ?></td>
        <td>
            <?php $js.=$displayObject->drawFormField("SMLVC",array('id'=>"add_start",'name'=>"add_start"),number_format(0,2)); ?>
            <span>% to </span>
            <?php $js.=$displayObject->drawFormField("SMLVC",array('id'=>"add_end",'name'=>"add_end"),'###'); ?>
            <span>%</span>
        </td>
        <td>
            <?php $js.=$displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_status",'name'=>"add_status",'extra_act'=>"processStatusChange",'button_extra'=>"key='p'"),(2/2)); ?>
        </td>
    </tr>

    <tr>
        <th></th>
        <td colspan="3" style="text-align: center"><button data-bonus_scale_id="<?php echo $bonus_scale_id; ?>" id="add_button">Add Bonus Scale List Item</button></td>
    </tr>
    <!--  BONUS SCALE LIST ITEM ADD FORM  -->
<?php
$last_id = 0;
foreach($scale as $key => $q) {
	$bonus = $q['bonus'];
	//$key = $q['id'];
	$last_id = $key;
	echo "
	<tr>
		<th>$key</th>
		<td >$bonus</td>
		<td>";
		$js.=$displayObject->drawFormField("SMLVC",array('id'=>"start_".$key,'name'=>"start[$key]",'class'=>"start_perf",'key'=>$key),number_format($q['start'],2));
		echo "<input type=hidden value='".number_format($q['start'],2)."' class=start_perf_validation />
		% to ";
		$js.=$displayObject->drawFormField("LABEL",array('id'=>"end_".$key,'name'=>"end[$key]",'class'=>"end_label"),"");
		echo "</td>
		<td>";
		$js.=$displayObject->drawFormField("BOOL_BUTTON",array('id'=>"status_".$key,'name'=>"status[$key]",'extra_act'=>"processStatusChange",'button_extra'=>"key='$key'"),($q['status']/2));
		echo "
		</td>
	</tr>";
}
if(count($scale)>0) {
?>
	<tr>
		<th></th>
		<td colspan=3 class=center><button class=saveform><?php echo $helper->getActivityName("save"); ?></button></td>
	</tr>
	<?php
}
	?>
</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack("setup_defaults_bonus_scale_list_table.php"),"setup",array('section'=>"BS")); ?></td>
	</tr>
</table>

<style type="text/css" >
	.saveform {
		font-size: 90%;
	}
	.saveform-active .ui-icon { background-image: url(/library/images/ui-icons_009900_256x240.png); }
	.saveform-active {
		border: 1px solid #009900;
		color: #009900;
		background-color: #ffffff;
		background-image: url();
	}
</style>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	var first_id = 0;
	var last_id = <?php echo $last_id; ?>;
	$("#tbl_bonus td").css("vertical-align","middle");
	$(".saveform").button({ icons: { primary: "ui-icon-disk" } })
		.addClass("saveform-active")
		.hover(function() { $(this).removeClass("saveform-active"); },function() { $(this).addClass("saveform-active"); })
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_bonus]"));
			console.log(dta);

			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=SetupBonus.UPDATEBYBONUSSCALEID",dta);
			console.log(result);
			result = result['result_message'];
			if(result[0]=="ok") {
                var id = $('#bonus_scale_id').val();
                var url = "setup_defaults_bonus.php?bonus_scale_id="+id;
                AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				//document.location.href = "setup_defaults_bonus.php?r[]=ok&r[]="+result[1];
			//} else if(result[0]=="okerror") {
				//document.location.href = "setup_defaults_preferences.php?r[]=error&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		});
	$(".start_perf").blur(function() {
		//.prev(label) didn't work so resorted to using the key attr
		var key = $(this).attr("key")*1;
		var last_id = $(".start_perf:enabled:last").attr("key")*1;
		if(key==first_id) {
			//do nothing for the first one
		} else {
			var prev_key = key-1;
			var $prev_label = $("#end_"+prev_key);
			var $prev_text = $("#start_"+prev_key);
			var prev_disabled = $prev_text.attr("disabled");
			var is_disabled = (prev_disabled=="disabled" || prev_disabled==true);
			while(is_disabled==true) {
				prev_key--;
				$prev_label = $("#end_"+prev_key);
				$prev_text = $("#start_"+prev_key);
				prev_disabled = $prev_text.attr("disabled");
				is_disabled = (prev_disabled=="disabled" || prev_disabled==true);
			}
			if(prev_key==last_id) {
				$prev_label.html("onwards");
			} else {
				
				$prev_label.html((($(this).val()*1)-0.01).toFixed(2)+"%");
			}
		}
	});
	$(".start_perf").trigger("blur");
	$("#end_"+last_id).html("onwards");

	//replace "in use" buttons with "required" for 0% bonus - cannot be disabled
	$("#status_"+first_id+"_yes").parent().html("Required");
	//function to act on changes to "in use" option
	function processStatusChange($btn,act) {
		var key = $btn.attr("key");
		if(act=="no") {
			$("#start_"+key).prop("disabled","disabled").parent().parent().find("td").css("color","#ababab");
			$("#end_"+key).prop("disabled","disabled");
		} else {
			$("#start_"+key).prop("disabled","").parent().parent().find("td").css("color","");
		}
		$(".start_perf").trigger("blur");
	}

	$(".btn_no").each(function() {
		if($(this).attr("button_status")=="active") {
			processStatusChange($(this),"no");
		}
	});

    /******************************************************************************************************************/
    /******************************************************************************************************************/
    /******************************************************************************************************************/

    //Determine the necessary width and height settings for the dlg_lines & ifr_lines elements
    var scr = AssistHelper.getWindowSize();
    var dlg_width = scr['width']*0.95;
    var dlg_height = scr['height']*0.95;
    var ifr_width = dlg_width-20;
    var ifr_height = dlg_height-100;

    $('#edit_button').button({
		icons: {primary: "ui-icon-pencil"}
	})
        .click(function(e) {
            e.preventDefault();
            AssistHelper.processing();

            var bonus_scale_id =  $(this).data('bonus_scale_id');

            $("#dlg_scale_edit").dialog("option","title","Edit Bonus Scale");
            var url = "setup_defaults_bonus_scale_edit_dlg.php?bonus_scale_id=" + bonus_scale_id;
            //call iframe in dialog
            $("#ifr_scale_edit").prop("src",url);

            //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
            //$("#dlg_scale_edit").dialog("open");

        }).removeClass("ui-state-default").addClass("ui-state-green");

    /**
     * Dialog to display available lines after clicking the "Go" button
     */
    $("#dlg_scale_edit").dialog({
        autoOpen: false,
        modal: true,
        width: dlg_width,
        height: dlg_height,
        buttons: [{
            text:"Save",
            icons: {primary: "ui-icon-disk"},
            click:function(e) {
                e.preventDefault();
                AssistHelper.processing();

                var form = $("#ifr_scale_edit").contents().find("form[name=frm_list]");
                var dta = AssistForm.serialize(form);
                var result = AssistHelper.doAjax("inc_controller.php?action=Lists.SimpleEdit",dta);

                //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                if(result[0]=="ok") {
                    var url = document.location.href;
                    AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                } else {
                    //if result was not "ok" then display the message
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }
            }
        },{
            text: "Cancel",
            click:function(e){
                e.preventDefault();
                $("#dlg_scale_edit").dialog("close");
            }
        }]
    });
    AssistHelper.formatDialogButtonsByClass($("#dlg_scale_edit"),0,"ui-state-green");
    /**
     * Iframe within the dlg_lines div
     */
    $("#ifr_scale_edit").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");


    $('#del_button').button({
		icons: {primary: "ui-icon-trash"}
	}).click(function(e) {
            e.preventDefault();
            AssistHelper.processing();
            if(confirm('Are you sure you wish to deactivate this scale')) {
                var list_id = "bonus_scale";
                var id = $(this).data('bonus_scale_id');

                var dta = "list_id=" + list_id + "&id=" + id ;

                var result = AssistHelper.doAjax("inc_controller.php?action=Lists.DELETE",dta);

                //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                if(result[0]=="ok") {
                    var url = "setup_defaults_bonus_scale_list_table.php";
                    AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                } else {
                    //if result was not "ok" then display the message
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }
            }
        }).removeClass("ui-state-default").addClass("ui-button-minor-grey").css("margin-left", "20px")
			.hover(function() {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-red");
			}, function() {
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-red");
			});


	$('#add_button').button({
		icons: {primary: "ui-icon-disk"}
	})
        .click(function(e) {
            e.preventDefault();
            AssistHelper.processing();



            var id = $(this).data('bonus_scale_id');
            var bonus = AssistString.code($('#add_bonus').serialize()).substring(4);//automatically includes "add_bonus=" due to .serialize() = substring to remove add_ part and then don't include field name in dta var
            var start = $('#add_start').val();
            var end = $('#add_end').val();

            var err = false;

			if(isNaN(parseFloat(start))) {
            	err = true;
            	AssistHelper.finishedProcessing("error","You have entered an invalid Performance Bracket.  Please enter numbers only up to 2 decimal places.");
			} else {

            	var start = parseFloat(start);
				$(".start_perf_validation").each(function() {
					if(parseFloat($(this).val())==start) {
						err = true;
						AssistHelper.finishedProcessing("error","You've started an invalid Performance Bracket.  Please make sure that you are not duplicating a start %.<br /><br />If you have made changes to an existing Performance Bracket, please make sure that you have saved those changes first before trying to add this one again.");
					}
				});

			}

            if(!err) {
				var dta = "bonus_scale_id=" + id + "&" + bonus + "&start=" + start + "&end=" + end ;
				console.log(dta);

				var result = AssistHelper.doAjax("inc_controller.php?action=BONUS.ADDITEM",dta);
				//If the process was finished with an "ok" result, display and then reload the page to reflect the changes
				if(result[0]=="ok") {
					var url = "setup_defaults_bonus.php?bonus_scale_id="+id;
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				} else {
					//if result was not "ok" then display the message
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
        }).removeClass("ui-state-default").addClass("ui-state-green");

    $('#add_end').prop('disabled', true);
});
/**
 * Function called by the ifr_lines Iframe to open the dlg_lines dialog once the iframe source page has loaded and is ready to be displayed to the user
 */
function openDialog() {
    $(function() {
        $("#dlg_scale_edit").dialog("open");
        AssistHelper.closeProcessing();
    });
}
</script>