<?php
/**
 * Added job title & Assist user status #AA-128 JC 1 Apr 2021
 * 		- chose not to include manager & department to try to prevent the table from getting too wide.
 * Filtered for non-grouped SCDs #AA-581 JC 12 April 2021
 */

$page_action = "trigger";

require_once("inc_header.php");



$apObject = new PM6_PERIOD();
$periods = $apObject->getAllAssessmentPeriods();
if(count($periods)==0) {
	ASSIST_HELPER::displayResult(array("error","No Assessment Periods have been created.  Please go to Assessments > Assessment Periods to auto-generate Assessment Periods."));
	die();
}
unset($apObject);

$scdObj = new PM6_SCORECARD();
//AA-581 - only display non-group SCDs here
$scorecards = $scdObj->getObjectsForTrigger(true);

//AA-581 - only display non-group SCDs here at the top... maybe display grouped SCDs at the bottom with option to claim [JC]
?>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Job Title</th>
		<th>Assist Status</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Function<br />Activities</th>
		<th><?php echo $scdObj->getObjectName(PM6_ASSESSMENT::OBJECT_TYPE,true); ?></th>
		<th></th>
	</tr>
<?php
$found_scorecards = false;
if(count($scorecards)>0) {
	$found_scorecards= true;
	foreach($scorecards as $pa) {
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
			<td>".str_replace(" (Started","<br /><span class='i'>(Started",$pa['job'])."</span></td>
			<td>".$scdObj->getAssistStatusInWords($pa['assist_status'])."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td class=center>".(isset($pa['assessments']) ? $pa['assessments'] : 0)."</td>
			<td class=center>
			<button class=btn_open obj_id=".$pa['obj_id'].">Open</button>
			</td>
		</tr>";
	}
} else {

	echo "
	<tr>
		<td id=td_none colspan=7>No ".$scdObj->getObjectName($scdObj->getObjectType(),true)." available.</td>
	</tr>";
}

?>
</table>
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");

	$(".btn_open").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "admin_assessments_triggers_details.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});


	<?php
	if(!$found_scorecards) {
		?>
	var c = $("#tbl_list tr:first").children("th").size();
	$("#td_none").prop("colspan",c).removeClass("center");
	<?php
	}
	?>
});
</script>