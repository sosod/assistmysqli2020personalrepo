<?php
/**
 * @var string $js - catchall variable for centralised JS source:inc_header
 * Added Employee Details section to give a place to add the Assist User Status field #AA-128 JC 1 Apr 2021
 * 		This info is needed so that the user can tell what process the assessment will follow once triggered - non-users will follow a different path
 */
$page_redirect_url = "admin_assessments_triggers";
$page_action = "trigger"; 

require_once("inc_header.php");

$obj_id = $_REQUEST['obj_id'];
$scd_id = $obj_id;
$scdObj = new PM6_SCORECARD();
$scorecard = $scdObj->getAObjectSummary($obj_id);
//die();



//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($triggers);

//Change it to return data and not echo directly - need the assist_status value for AA-570 to auto skip SELF if non-assist user #AA-128 JC 1 April 2021
$assessment_display = $scdObj->getAssessmentScoreHeading($obj_id,false,false,false);
echo $assessment_display['display'];
$assist_status = $assessment_display['assist_status'];
?>
<h3>Scorecard Structure</h3>
<table class=list id=tbl_list>
	<tr>
		<th>&nbsp;</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Function<br />Activities</th>
		<th>Assessments</th>
	</tr>
<?php
//$apObj->arrPrint($scorecard);
$pa = $scorecard;
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class=b>Count:</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td class=center>".(isset($pa['assessments']) ? $pa['assessments'] : 0)."</td>
		</tr>
		<tr>
			<td class=b>Weight:</td>
			<td colspan=6 class=center>".(isset($pa['weights']['kpa']) && strlen($pa['weights']['kpa'])>0 ? $pa['weights']['kpa'] : "0")."%</td>
			<td  class=center>".(isset($pa['weights']['cc']) && strlen($pa['weights']['cc'])>0 ? $pa['weights']['cc'] : "0")."%</td>
			<td  class=center>".(isset($pa['weights']['jal']) && strlen($pa['weights']['jal'])>0 ? $pa['weights']['jal'] : "0")."%</td>
			<td class=center>N/A</td>
		</tr>
		";

?>
</table>
<?php 
/** *********************************** START OF CENTRALISED TRIGGER FUNCTIONALITY *************************************************** */
$trigger_source_type = "SCD";
$trigger_source_obj_id = $obj_id;
$trigger_employee_tkid = $scorecard['employee_tkid'];
$trigger_page_redirect_url = $page_redirect_url;
$allow_trigger_management = true;

include("common/trigger_assessment.php");
?>


   
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");

	//$("table.noborder").find("td").addClass("noborder");
	
	<?php echo $js;  ?>



});
</script>