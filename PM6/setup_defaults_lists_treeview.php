<?php
require_once("inc_header.php");

$page_redirect = "setup_defaults_lists_treeview.php?some_blank_key=because_im_too_lazy";
foreach($_REQUEST as $key => $value) {
	if($key!="r") {
		$page_redirect.="&".$key."=".urlencode($value);
	}
}


$competency_id = $_REQUEST['comp_id'];
$ids_for_audit_log = array(1);//first item must be "true" so that convertArrayToSQL function knows to use "IN" and not "NOT IN" when joining

$competency_object = new PM6_COMPETENCIES();

$treeview_datastructure = $competency_object->getCompetencyTreeViewDataStructureByCompetencyID($competency_id);
//ASSIST_HELPER::arrPrint($treeview_datastructure);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>

<table class='tbl-container not-max'>
    <tr>
        <td>
            <table class=tbl-tree>
                <?php foreach($treeview_datastructure as $comp_id => $comp_info){ ?>
                    <tr class="grand-parent">
                        <td class="uni-pos" colspan="4">Competency: <?php echo $comp_info['name'] . ' [' . $comp_id . ']'; ?></td>
                        <td class="center td-button" object_type="competencies" object_id="<?php echo $comp_id; ?>" data-competency_id="<?php echo $comp_id; ?>">
                            <button class='action-button view-btn' parent_id="<?php echo $comp_id; ?>">View</button>
                            <button class='action-button edit-btn' parent_id="<?php echo $comp_id; ?>">Edit</button>
                        </td>
                        <td class="uni-pos center"></td>
                        <td class="uni-pos center"></td>
                    </tr>

                    <tr>
                        <td class="td-line-break" colspan="7">&nbsp;</td>
                    </tr>
                    <?php
					foreach($comp_info['proficiencies'] as $prof_id => $prof_info){
						$class = "";
						$show_add_button = true;
						if($prof_info['is_active']!=true) {
							$class = "inactive";
							$show_add_button = false;
						}

						?>
                        <tr class="parent <?php echo $class; ?>" >
                        <tr class="parent" style="background-color: rgb(186, 186, 186);">
                            <td class="tree-icon uni-icon" width="20px"><button class="ibutton expand-btn">X</button></td>
                            <td class="bi-pos" colspan="3">Proficiency: <?php echo $prof_info['name']; ?></td>
                            <td class='center td-button' object_type="proficiencies" object_id="<?php echo $prof_id; ?>" data-competency_id="<?php echo $comp_id; ?>">
                                <button class='action-button view-btn' parent_id="<?php echo $prof_id; ?>">View</button>
                                <button class='action-button edit-btn' parent_id="<?php echo $prof_id; ?>">Edit</button>
                            </td>
                            <td class="bi-pos center"></td>
                            <td class="bi-pos center"></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td class=bi-pos object_id="0" object_type="competencies_levels" data-competency_id="<?php echo $comp_id; ?>">
                                <?php if($show_add_button) { ?><button class='action-button add-btn' parent_id="<?php echo $prof_id; ?>">Add Competency Level</button><?php } ?>
                                <span class='float count'><?php echo (isset($prof_info['competency_levels']) ? count($prof_info['competency_levels']) : 0) . ' Competency Levels'; ?></span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
						<?php
						foreach($prof_info['competency_levels'] as $comp_level_id => $comp_level_info){
                        	$ids_for_audit_log[] = $comp_level_id;
                        	?>
                            <tr class="child <?php echo (($comp_level_info['is_active'] && !$comp_level_info['is_inactive']) ? "" : "inactive"); ?>">
                                <td class=""></td>
                                <td class="tree-icon bi-icon" width="20px"><button class='ibutton sub-btn'>X</button></td></td>
                                <td class="tri-pos" colspan="2"><?php echo $comp_level_info['name']; ?><span class="float" style="font-size:75%">[<?php echo $comp_level_info['ref']; ?>]</span></td>
                                <td class='center td-button' object_type="competencies_levels" object_id="<?php echo $comp_level_id; ?>" data-competency_id="<?php echo $comp_id; ?>">
									<?php if($comp_level_info['is_active'] && !$comp_level_info['is_inactive']) { ?>
										<button class='action-button view-btn' parent_id="<?php echo $comp_level_id; ?>">View</button>
										<button class='action-button edit-btn' parent_id="<?php echo $comp_level_id; ?>">Edit</button>
									<?php } else { ?>
										<button class='action-button restore-btn' parent_id="<?php echo $comp_level_id; ?>">Restore</button>
									<?php } ?>
                                </td>
                                <td class="tri-pos center"></td>
                                <td class="tri-pos center"></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </table>
        </td>
    </tr>
	<tr><td>
<?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"list",array('section'=>"competencies_levels",'item_id'=>$ids_for_audit_log)); ?>
		</td></tr>
</table>


<div id=dlg_list title="Select lines">
    <iframe id=ifr_list></iframe>
</div>
<div id=dlg_view title="View">
    <iframe id=ifr_view></iframe>
</div>
<!--    ********************************* STYLE BASED JS  - START **********************  -->
<script type="text/javascript">
	var what_am_i_doing_if_i_call_for_confirmation = "";
	var restore_dta = "";
    $(function() {
    	<?php echo $js; ?>
        var object_names = [];
        object_names['competencies'] = "Competency";
        object_names['proficiencies'] = "Proficiency";
        object_names['competencies_levels'] = "Competency LEVEL";

        var my_window = AssistHelper.getWindowSize();
        //alert(my_window['height']);
        if(my_window['width']>800) {
            var my_width = 850;
        } else {
            var my_width = 800;
        }
        var my_height = my_window['height']-50;

        $("table.tbl-tree").find("td.tree-icon").addClass("right").prop("width","20px");
        //Set the column spanning for each object description cell based on their position in the hierarchy but excluding the last two columns
        $("table.tbl-tree").find("td.cate-pos, td.td-line-break").prop("colspan","7");
        $("table.tbl-tree").find("td.uni-pos").not("td.center").prop("colspan","4");
        $("table.tbl-tree").find("td.bi-pos").not("td.center").prop("colspan","3");
        $("table.tbl-tree").find("td.tri-pos").not("td.center").prop("colspan","2");
        $("table.tbl-tree").find("td.quad-pos").not("td.center").prop("colspan","1");

        //Action positional buttons
        $("button.sub-btn").button({
            icons: {primary: "ui-icon-carat-1-sw"},
            text: false
        }).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
        //Parent positional buttons
        $("button.expand-btn").button({
            icons: {primary: "ui-icon-triangle-1-se"},
            text: false
        }).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});

        //Edit button
        $("button.edit-btn").button({
            icons: {primary: "ui-icon-pencil"},
        }).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","Edit Object").html("<p>This is where the form to edit a new object will display.").dialog("open");
        */
        //Restore button
        $("button.restore-btn").button({
            icons: {primary: "ui-icon-arrow-1-n"},
        }).removeClass("ui-state-default").addClass("ui-button-state-grey")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","Edit Object").html("<p>This is where the form to edit a new object will display.").dialog("open");
        */
        //View button
        $("button.view-btn").button({
            icons: {primary: "ui-icon-extlink"},
        }).removeClass("ui-state-default").addClass("ui-button-state-info").css("color","#fe9900")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
        */
        //Add button
        $("button.add-btn").button({
            icons: {primary: "ui-icon-circle-plus"},
        }).removeClass("ui-state-default").addClass("ui-button-state-ok").css("color","#009900")
            .children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"25px","font-size":"80%"});
        /*			}).click(function() {
                        $("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
        */
        //Button styling and positioning
        $("button.ibutton:not(.sub-btn)").hover(
            function() {
                $(this).css({"border-width":"0px","background":"url()"})
            },
            function() {}
        );
        $("button.ibutton").each(function() {
            if($(this).prev().hasClass("ibutton")) {
                $(this).css("margin-left","-5px");
            }
            if($(this).next().hasClass("ibutton")) {
                $(this).css("margin-right","-5px");
            }
        });

        //Styling based on object type
        $("table.tbl-tree tr.grand-parent").css("background-color","#AAAAAA").find("td.td-button").css({"background-color":"#FFFFFF"});
        $("table.tbl-tree tr.parent").css("background-color","#BABABA").find("td.td-button").css("background-color","#FFFFFF");
        $("table.tbl-tree tr.sub-parent").css("background-color","#DEDEDE").find("td.td-button").css("background-color","#FFFFFF");

        //General styling
        $("span.count").css({"font-size":"80%","margin-top":"-3px"});
        $("table.tbl-tree th").css({"padding":"10px"});



        $("button.action-button").click(function(e) {
            //if($(this).parent("td").attr("object_type") == 'proficiencies' && $(this).hasClass("edit-btn")){
                e.preventDefault();
           // }else{
                AssistHelper.processing();
                var object_id = $(this).parent("td").attr("object_id");
                var object_type = $(this).parent("td").attr("object_type");

                var competency_id = $(this).parent("td").data("competency_id");
                var parent_id = $(this).attr("parent_id");


                var act = "view";
                if($(this).hasClass("edit-btn")) {
                    act = "edit";
                } else if($(this).hasClass("add-btn")) {
                    act = "add";
                } else if($(this).hasClass("restore-btn")) {
                    act = "restore";
                }

                /*
                * What we're doing: action as either "view" or "add" or "edit
                * The type of object we're acting on: "competency", "proficiency" or "competency_level"
                * The ID of the odject
                * "*/

                var dta = "object_id=" + object_id + "&object_type=" + object_type + "&action=" + act + "&competency_id=" + competency_id + "&parent_id=" + parent_id;

                var heading = AssistString.ucwords(act) + " " + object_names[object_type];
                //var parent_id = $(this).attr("parent_id");
                var url = "setup_defaults_lists_treeview_dlg.php?" + dta;
if(act=="view") {
	$("#dlg_view").dialog("option", "title", heading);

	//call iframe in dialog
	$("#ifr_view").prop("src", url);
} else if(act=="restore") {
	what_am_i_doing_if_i_call_for_confirmation = "restore";
	restore_dta = "object_id=" + object_id + "&object_type=" + object_type;
	AssistHelper.askForConfirmation("<p>Are you sure you want to restore this item?</p>");
} else {
	$("#dlg_list").dialog("option", "title", heading);
	if(act=="add" || object_type!="competencies_levels") {
		$("#dlg_list").dialog("widget").find(".ui-dialog-buttonpane button").eq(2).hide();
	} else {
		$("#dlg_list").dialog("widget").find(".ui-dialog-buttonpane button").eq(2).show();
	}
                //call iframe in dialog
                $("#ifr_list").prop("src",url);
}
                //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
                //$("#dlg_list").dialog("open");
            //}
        });


        /************************************************************************************************/
        /************************************************************************************************/
        /************************************************************************************************/
		$("#dlg_view").dialog({
			autoOpen: false,
			modal: true,
			width: my_width,
			height: my_height,
            buttons: [{
				text: "Close",
				icons: {primary: "ui-icon-closethick"},
				click: function(e) {
					e.preventDefault();
                    $("#dlg_view").dialog("close");
				}
			}]
		});
        $("#dlg_list").dialog({
            autoOpen: false,
            modal: true,
            width: my_width,
            height: my_height,
            buttons: [{
                text:"Save",
                icons: {primary: "ui-icon-disk"},
                click:function(e) {
                    e.preventDefault();
                    AssistHelper.processing();
                    var object_id = $("#ifr_list").contents().find("#object_id").val();
                    var object_type = $("#ifr_list").contents().find("#object_type").val();
                    var action = $("#ifr_list").contents().find("#action").val();

                    // console.log('Object Type AGN:');
                    // console.log(object_type);
                    // console.log('Object ID AGN:');
                    // console.log(object_id);
                    // console.log('ACTION AGN:');
                    // console.log(action);

                    var err = false;
                    $("#ifr_list").contents().find("input:text, textarea, select").each(function() {
                    	$(this).removeClass("required");
                    	var v = $(this).val();
                    	if($(this).hasClass("is_required") && v.length==0) {
                    		$(this).addClass("required");
                    		err = true;
						}
					});

                   // var field_value_pairs = new Array();
/*
                    $("#ifr_list").contents().find("td").each(function() {
                        var field_name =  $(this).attr('fld');
                        field_name = field_name.replace('edit_td_', ''); //'123456';

                        $(this).children().each(function(){
                            var field_value;
                            if($(this).is('textarea')){
                                field_value = $(this).val();
                            }else if($(this).is('select')){
                                field_value = $(this).val();
                            }else{
                                field_value = null;
                            }

                            if(field_value !== null){
                                //THIS IS WHERE WE ADD THE ACTUAL FIELD VALUES
                                field_value = field_value.trim();
                                var field_value_pair_string = field_name + "=" + (field_value == "" ? "null" : field_value);
                                field_value_pairs.push(field_value_pair_string);
                            }
                        });
                    });

                    // console.log('FV PAIRS AGN:');
                    // console.log(field_value_pairs);
*/
                    var dta = "object_id="+object_id;
/*
                    field_value_pairs.forEach(function(element) {
                        dta += '&' + element;
                    });

                    dta = encodeURI(dta);

                    // console.log('DTA:');
                    // console.log(dta);
*/
                    var ajax_action = 'DO.NOTHING';
                    if(object_type == 'competencies_levels' && action == 'edit'){
                        ajax_action = 'COMPETENCIES.EDITLEVELS';
                    }else if(object_type == 'competencies_levels' && action == 'add'){
                        var competency_id = $("#ifr_list").contents().find("#competency_id").val();
                        var parent_id = $("#ifr_list").contents().find("#parent_id").val();
                        dta += '&competency_id=' + competency_id;
                        dta += '&proficiency_id=' + parent_id;
                        ajax_action = 'COMPETENCIES.ADDLEVEL';
                    }else if(object_type == 'competencies' && action == 'edit'){
                        ajax_action = 'COMPETENCIES.EDITCOMPETENCY';
                    }

                    if(ajax_action != 'DO.NOTHING') {
                        var result = AssistHelper.doAjax("inc_controller.php?action=" + ajax_action,dta);

                        // console.log('THESE ARE THE RESULTS === FORMATTED FOR DEV RIGHT NOW');
                        // console.log(result);
                    } else {
                        var result = ["error","Sorry, I could not figure out what you wanted me to do.  Please try again."];
                    }


                    //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                    if(result[0]=="ok") {
                        var url = document.location.href;
                        AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                    } else {
                        //if result was not "ok" then display the message
                        AssistHelper.finishedProcessing(result[0],result[1]);
                    }
                }
            },{
                text: "Cancel",
                click:function(e){
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_list").dialog("close");
                }
			}, {
				text: "Deactivate",
				icons: {primary: "ui-icon-trash"},
				click: function (e) {
					e.preventDefault();
					what_am_i_doing_if_i_call_for_confirmation = "deactivate";
					//Confirm deactivation - function auto redirects to continueAfterConfirmation() or cancelAfterConfirmation() below
					AssistHelper.askForConfirmation("<p>Are you sure you want to deactivate this item?</p>");
				}
			}]
        });

		AssistHelper.formatDialogButtonsByClass($("#dlg_list"), 0, "ui-button-state-green");
		AssistHelper.formatDialogButtonsByClass($("#dlg_list"), 1, "ui-button-state-grey");
		AssistHelper.formatDialogButtonsByClass($("#dlg_list"), 2, "ui-button-state-red");

        /**
         * Iframe within the dlg_lines div
         */
        // $("#ifr_list").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").css("border","1px solid #000000");
        $("#ifr_list").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").css("border","0px solid #000000");
        $("#ifr_view").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").css("border","0px solid #000000");

    });

    function openDialog(act) {
         $(function() {
        	if(act=="view") {
            	$("#dlg_view").dialog("open");

			} else {
            	$("#dlg_list").dialog("open");
            	$("#ifr_list").contents().find("input:text, textarea").first().focus();
			}
             AssistHelper.closeProcessing();
         });
     }

	function continueAfterConfirmation() {
		$(function () {
			AssistHelper.processing();
			if(what_am_i_doing_if_i_call_for_confirmation=="deactivate") {
				var object_id = $("#ifr_list").contents().find("#object_id").val();
				var object_type = $("#ifr_list").contents().find("#object_type").val();
				var dta = "object_id=" + object_id + "&object_type=" + object_type;
				var result = AssistHelper.doAjax("inc_controller.php?action=Competencies.Deactivate", dta);
				$("#dlg_list").dialog("close");
			} else if(what_am_i_doing_if_i_call_for_confirmation="restore") {
				var dta = restore_dta;
				var result = AssistHelper.doAjax("inc_controller.php?action=Competencies.Restore", dta);
			} else {
				var result = new Array("info","Sorry, I don't know what you want me to do.  Please try again.");
			}
			if (result[0] == "ok") {
				document.location.href = "<?php echo $page_redirect; ?>&r[]=ok&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		});
	}

	function cancelAfterConfirmation() {
		//do nothing but close main dialog
		$("#dlg_list").dialog("close");
	}
</script>
<!--    ********************************* STYLE BASED JS  - ENDT **********************  -->