<?php
require_once ("../module/autoloader.php");

$userObject = new PM6_USERACCESS();
$my_useraccess = $userObject->getMyUserAccess();


if($my_useraccess['trigger']==true) {
	//re route user to admin_assessments_triggers
	header("Location: admin_assessments_triggers.php");
} elseif($my_useraccess['manage_object']==true) {
	//reroute user to admin_scorecards_edit
	header("Location: admin_scorecards_edit.php");
} else {
	require_once("inc_header.php");
	ASSIST_HELPER::displayResult(array("error","Oops!  It looks like you've not got the correct access.  Please try a different section of this module."));
}


?>