<?php
/******************************
 * DEV NOTES:
 * 21 April 2021 AA-581
 *  - Triggering:
 * 	X	- Add
 * 	X	- Display # on AAG
 * 	X	- Display on AAGD - [Do I need to record base details in a group triggers table or do I just use the trigger from a linked SCD?] - decision: take trigger from linked SCD for now [JC - 21 April 2021]
 * 	X	- Trigger Mod - beetle version = same mods/finals for everyone, rolls royce = different mods/finals
 * 	X	- Trigger Final
 * 	X	- Delete trigger SELF
 * 	X	- Delete trigger MOD
 * 	X	- Delete trigger Final
 * 		- Edit SELF - NEW functionality [AA-561]
 * 		- Edit MOD - NEW functionality [AA-561]
 * 		- Edit FINAL - NEW functionality [AA-561]
 * 	X	- Prevent Adding of SCDs after triggering
 * 	X	- Hide Feedback View button in Trigger section & Display in Scorecards section
 * 		- LOG LOG LOG LOG Group activities
 * VARIABLES
 * @var string $js - catchall variable for any JS - source: inc_header
 * @var PM6_DISPLAY $displayObject - created in inc_header
 * @var array $assessments - array of assessments triggered for a scorecard in the group, assumption is that all scorecards have the same triggers therefore group has been triggered source:common/trigger_assessments
 */

$page_redirect_url = "admin_assessments_group";
$page_action = "trigger"; 

require_once("inc_header.php");

if(isset($_REQUEST['object_id'])) {
	$obj_id = $_REQUEST['object_id'];
} elseif(isset($_REQUEST['obj_id'])) {
	$obj_id = $_REQUEST['obj_id'];
} else {
	ASSIST_HELPER::displayResult(array("error","An error has occurred.  Please go back and try again.  Error code: AAGD".__LINE__));
	die();
}
$groupObject = new PM6_ASSESSMENT_GROUP();
$apObj = new PM6_PERIOD();
$scdObj = new PM6_SCORECARD();

$group_details = $groupObject->getFullGroupDetails($obj_id);

$periods = $apObj->getAssessmentPeriodsForSelect(true,$obj_id);
$available_periods = count($periods)>0?true:false;
$has_the_group_been_triggered = false;
?>
<table class='tbl-container not-max'><tr><td>
	<span class=float style='padding-top: 20px;'><button id=btn_edit_grp grp_id=<?php echo $obj_id; ?>>Edit Group</button></span>
	<h2>Group Details</h2>
	<table class="form" id="tbl_group">
		<tr>
			<th>Name:</th>
			<td><?php echo $group_details['group_name']; ?></td>
		</tr>
		<tr>
			<th>Group Type:</th>
			<td><?php echo $group_details['group_type_text']; ?></td>
		</tr>
		<tr>
			<th>Grouped By:</th>
			<td><?php echo $group_details['group_item_text']; ?></td>
		</tr>
	</table>
</td></tr></table>
<?php
/** *********************************** START OF CENTRALISED TRIGGER FUNCTIONALITY *************************************************** */
$trigger_source_type = "GRP";
$trigger_source_obj_id = $obj_id;
$trigger_employee_tkid = false;
$trigger_page_redirect_url = $page_redirect_url;
$allow_trigger_management = true;

include("common/trigger_assessment.php");


/**
 * Included tigger page will create a variable $assessments which will give count of the number of assessments triggered - if > 0 then group has been triggered
 */
if(count($assessments)>0) {
	$has_the_group_been_triggered = true;
}

/** *********************************** END OF CENTRALISED TRIGGER FUNCTIONALITY *************************************************** */
?>
<?php
$scorecards = $scdObj->getObjectsByGroup($obj_id);
?>
<table class='tbl-container not-max'><tr><td>
	<?php if($has_the_group_been_triggered===false) { ?><span class=float style='padding-top: 10px;'><button id=btn_add_scd >Add Scorecards</button></span><?php } ?>
	<h2>Scorecard Details</h2>
	<table class=list id=tbl_scd_list>
		<tr>
			<th>Ref</th>
			<th>Employee</th>
			<th>Job</th>
			<th>Assist Status</th>
			<th>Organisational KPIs</th>
			<th>Individual KPIs</th>
			<th>Core<br />Competencies</th>
			<th>Function<br />Activities</th>
			<th></th>
		</tr>
	<?php
	$found_scorecards = false;
	if(count($scorecards)>0) {
		$found_scorecards= true;
		foreach($scorecards as $pa) {
			$assess_id = 0;
			$org_count = (isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)+(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)+(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0);
			$ikpi_count = (isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)+(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)+(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0);
			echo "
			<tr>
				<td class='center b'>".$pa['ref']."</td>
				<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
				<td>".str_replace(" (Started","<br /><span class='i'>(Started",$pa['job'])."</span></td>
				<td>".$scdObj->getAssistStatusInWords($pa['assist_status'])."</td>
				<td>".$org_count."</td>
				<td>".$ikpi_count."</td>
				<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
				<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
				<td class=center><button class=btn-trigger-details scd_id=".$pa['obj_id'].">Trigger Details</button><span class='float'><button class='btn-remove' scd_id='".$pa['obj_id']."'>Remove from Group</button></span></td>
			</tr>";
		}
	} else {

		echo "
		<tr>
			<td id=td_none colspan=7>No ".$scdObj->getObjectName($scdObj->getObjectType(),true)." available.</td>
		</tr>";
	}

	?>
	</table>
<?php
ASSIST_HELPER::displayResult(array("info","<p>Please note:</p>
<ul class='black'>
<li>Once the Group has been triggered you will no longer be able to add additional Scorecards.</li>
<li>Scorecards can be removed from the Group at any time.</li>
<li>If you remove a Scorecard from the group, you can add it back provided that neither the Scorecard nor the Group have been triggered.</li>
<li>Group Type and Group By cannot be edited at any time.  To change those you need to delete the group and recreate it.</li>
<li>To access the View/Feedback View pages for a specific Scorecard, use the Trigger Details button in the Scorecard Details section.</li>
</ul>"))
?>
</td></tr></table>
<div id="dlg_edit_grp" title="Edit Group">
	<table id="tbl_edit_grp" class="form">
		<tr>
			<th width="150px">Current Name:</th>
			<td><?php echo $group_details['group_name']; ?></td>
		</tr>
		<tr>
			<th>New Name:</th>
			<td><?php $js.=$displayObject->drawFormField("LRGVC",array('id'=>"grp_name"),ASSIST_HELPER::decode($group_details['group_name'])); ?></td>
		</tr>
		<tr>
			<td colspan="2" class="right">
				<button id="btn_grp_edit_save">Save Changes</button>&nbsp;<button id="btn_grp_edit_cancel">Cancel</button>&nbsp;<button id="btn_grp_delete">Delete</button>
			</td>
		</tr>
	</table>
</div>
<div id="dlg_trigger_details_view">
	<iframe id="ifr_trigger_details_view">

	</iframe>
</div>
<script type="text/javascript">
var confirmation_action = "";
$(function() {
	//Formatting & centralised display class JS
	<?php
	$js.="
		//format trigger details buttons
	$('.btn-trigger-details').button({
		icons: {primary: 'ui-icon-newwin'},
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var ttle = $(this).closest('tr').find('td:eq(1)').html()+' ['+$(this).closest('tr').find('td:first').html()+']';
		$('#dlg_trigger_details_view').dialog({'title':ttle});
		$('#ifr_trigger_details_view').prop('src','admin_assessments_group_details_trigger_view.php?obj_id='+$(this).attr('scd_id'));
		//called from inside iframe - kept here for DEV purposes
//		$('#dlg_trigger_details_view').dialog('open');
	});
	resizeButtons($('.btn-trigger-details'));
	";
	echo $js;
	?>
	$("#tbl_scd_list td").addClass("center");
	$("#tbl_scd_list tr").find("td:lt(2)").removeClass("center");

	//Group Edit dialog
	$("#btn_grp_edit_save").button({icons: {primary:"ui-icon-disk"}})
		.removeClass("ui-state-default").addClass("ui-button-bold-green")
		.hover(function() {$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");},function() {$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");})
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var v = $("#grp_name").val();
			if(v.length==0) {
				AssistHelper.finishedProcessing("info","No changes found to be saved.");
			} else {
				var dta = "object_id=<?php echo $obj_id; ?>&name="+v;
				console.log(dta);
				var result = AssistHelper.doAjax("inc_controller.php?action=GROUP.EDITGroupObject",dta);
				console.log(result);
				if(result[0]=="ok") {
					AssistHelper.finishedProcessingWithRedirect("ok",result[1],document.location.href);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		});
	$("#btn_grp_edit_cancel").button({icons: {primary:"ui-icon-closethick"}})
		.removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(function() {$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");},function() {$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");})
		.click(function(e) {
			e.preventDefault();
			$("#dlg_edit_grp").dialog("close");
		});


	$("#btn_grp_delete").button({icons: {primary:"ui-icon-trash"}})
		.removeClass("ui-state-default").addClass("ui-button-minor-red")
		.hover(function() {$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-red");},function() {$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-red");})
		.click(function(e) {
			e.preventDefault();
			confirmation_action = "DELETE_GROUP";
			AssistHelper.askForConfirmation("<h1 class=red>Delete Group??</h1><p class=b>Are you sure?  This can not be undone.</p><p class=i>If Assessments have been triggered then the scorecards in this group will not be available for adding to a new group and will have to be managed manually in the Per Scorecard section.</p>");
		});

	var w = $("#tbl_edit_grp").width()+50;
	$("#dlg_edit_grp").dialog({
		autoOpen:false,
		modal:true,
		width:w
	});
	$("#btn_edit_grp").button({icons: {primary:"ui-icon-pencil"}})
		.removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(function() {$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");},function() {$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");})
		.click(function(e) {
			e.preventDefault();
			$("#dlg_edit_grp").dialog("open");
		});

	//Trigger Details View
	var windowSize = AssistHelper.getWindowSize();
	var dlgWidth = windowSize.width<800?windowSize.width*0.9:windowSize.width*0.75;
	var ifrWidth = dlgWidth-2;
	var dlgHeight = windowSize.height*0.9;
	var ifrHeight = dlgHeight-37;
	$('#ifr_trigger_details_view').prop('width',ifrWidth).prop('height',ifrHeight).css({'border':'0px solid #ababab','margin':'0px'});
	$("#dlg_trigger_details_view").dialog({
		autoOpen:false,
		modal:true,
		width:dlgWidth,
		height:dlgHeight,
		close:function(e) {
			AssistHelper.closeProcessing();
		}
	}).css("padding","0px");
	//Add Trigger Dialog
	$("#btn_add").button({icons: {primary:"ui-icon-circle-plus"}})
		.removeClass("ui-state-default").addClass("ui-button-state-green")
		.hover(function() {$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");},function() {$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");})
		.click(function(e) {
			e.preventDefault();
		});
	//Add Scorecard function
	$("#btn_add_scd").button({icons: {primary:"ui-icon-circle-plus"}})
		.removeClass("ui-state-default").addClass("ui-button-state-green")
		.hover(function() {$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");},function() {$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");})
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = 'admin_assessments_group_step2.php?adding_scorecards_to_existing_group=1&group_id=<?php echo $obj_id; ?>';
		});
	//Remove Scorecard function
	$(".btn-remove").button({icons:{primary:"ui-icon-trash"}})
		.removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(function() {$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");},function() {$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");})
		.click(function(e) {
			e.preventDefault();
			var scd_id = $(this).attr("scd_id");
			var ref = $(this).closest("tr").find("td:first").html();
			var html_formatted_msg = "<h1 class=red>Remove Scorecard</h1><p>Are you sure you wish to remove "+ref+" from this Group?</p>";
			askForConfirmationOnRemoveScorecard(scd_id,html_formatted_msg);
		});

});
//What to do when asked for confirmation of deletion of group
function continueAfterConfirmation() {
	$(function() {
		AssistHelper.processing();
		if(confirmation_action==="DELETE_GROUP") {
			var dta = "object_id=<?php echo $obj_id; ?>";
			var result = AssistHelper.doAjax("inc_controller.php?action=GROUP.DELETE", dta);
			if (result[0] == "ok") {
				AssistHelper.finishedProcessingWithRedirect("ok", result[1], "admin_assessments_group.php");
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		}
	});
}
function cancelAfterConfirmation() {
	//do nothing
}
function askForConfirmationOnRemoveScorecard(scd_id,html_formatted_msg) {
		//only display if not already open
				$('<div />',{id:"dlg_ask_for_confirmation",html:html_formatted_msg})
				.dialog({
					modal:true,
					closeOnEscape:false,
					buttons: [{
						text:"Continue",
						icons:{primary:"ui-icon-check"},
						click:function(e) {
							e.preventDefault();
							AssistHelper.processing();
							$("#dlg_ask_for_confirmation").dialog("destroy");
							var dta = "object_id=<?php echo $obj_id; ?>&scd_id="+scd_id;
							var result = AssistHelper.doAjax("inc_controller.php?action=GROUP.REMOVESCORECARD", dta);
							if (result[0] == "ok") {
								AssistHelper.finishedProcessingWithRedirect("ok", result[1], document.location.href);
							} else {
								AssistHelper.finishedProcessing(result[0], result[1]);
							}
						}
					},{
						text:"Cancel",
						icons:{primary:"ui-icon-closethick"},
						click:function(e) {
							e.preventDefault();
							cancelAfterConfirmation();
							$("#dlg_ask_for_confirmation").dialog("destroy");
						}

					}]
				});
				//$('.ui-dialog-titlebar').hide();
				AssistHelper.hideDialogTitlebar("id","dlg_ask_for_confirmation");
				AssistHelper.formatDialogButtonsByClass($("#dlg_ask_for_confirmation"),0,"ui-button-state-green");
				AssistHelper.formatDialogButtonsByClass($("#dlg_ask_for_confirmation"),1,"ui-button-minor-grey");
				AssistHelper.focusDialogButtons($("#dlg_ask_for_confirmation"),1);
	}
	function ifrResize(pW,pH) {
		$(function() {
			// console.log(pW);
			// console.log(pH);
			$("#ifr_trigger_details_view").prop("width",pW);
			$("#ifr_trigger_details_view").prop("height",pH);
			$("#dlg_trigger_details_view").dialog({"width":pW+2});
			$("#dlg_trigger_details_view").dialog({"height":pH+37});
		});
	}
</script>