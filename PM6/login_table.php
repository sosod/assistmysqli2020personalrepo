<?php
/**
 * @var string $modref - from calling page
 * @var int $today - from calling page
 */
/**
 * AA-557 - JC - 10 March 2021
 * Get PM6 onto front page dashboard
 */
$now = strtotime(date("d F Y"));
//echo "<P>".date("d F Y H:i:s");
//echo "<P>".date("d F Y H:i:s",$now);
/** @var INT $next_due - from main_login.php = number of days to display */
$future = strtotime(date("d F Y",($today+( ($next_due)*24*3600)))); //echo date("d F Y H:i:s",$future);

/**
 * Get headings & variables for FPD
 */
//if(!isset($pm6Object)) {
//remove reliance on login_stats having the trigger object - to accommodate running multiple modules of the PM6 version
	$pm6Object = new PM6_TRIGGER(0,$modref);
//}
	$self_name = $pm6Object->getTypeName($pm6Object->getSelfType());
	$mod_name = $pm6Object->getTypeName($pm6Object->getModeratorType());
	$moderator_name = $pm6Object->replaceAllNames("|MODERATOR|");
	$moderators_name = $pm6Object->replaceAllNames("|MODERATORS|");
	$final_name = $pm6Object->getTypeName($pm6Object->getFinalReviewType());
				$head = array();	$blank_action = array();
//Ref
				$head['ref'] = array('text'=>"Ref", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['ref'] = 0;
//Employee
				$head['employee'] = array('text'=>"Employee", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['employee'] = "";
//Assessment Period
				$head['period'] = array('text'=>"Assessment Period", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['period'] = "";
//Trigger type
				$head['type'] = array('text'=>"Type", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['type'] = "";
//Trigger status
				$head['status'] = array('text'=>"Status", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['status'] = "";
//Trigger Deadline
				$head['deadline'] = array('text'=>"Deadline", 'long'=>false, 'deadline'=>true, 'type'=>"D");
				$blank_action['deadline'] = "";
//Link to scoresheet
				$head['link'] = array('text'=>"", 'long'=>false, 'deadline'=>false,'type'=>"S");
				$blank_action['link'] = "";
/**
 * Get actions
 */
$actions = array();
//list of triggers should exist from login_stats but just in case something happened check here & get again if necessary

if(!isset($remember_pm6_incomplete_triggers_for_multiple_modules[$modref]) || count($remember_pm6_incomplete_triggers_for_multiple_modules[$modref])==0) {
	$pm6_incomplete_triggers = $pm6Object->getTriggersForCompletion("FPD");
} else {
	$pm6_incomplete_triggers = $remember_pm6_incomplete_triggers_for_multiple_modules[$modref];
}

foreach($pm6_incomplete_triggers['objects'] as $trgr) {
	$i = $trgr['child_id'];
	$l = $trgr['id'];
	$t = $trgr['trigger_type_raw'];
	$actions[$i] = $blank_action;
	$actions[$i]['ref'] = $pm6_incomplete_triggers['details'][$trgr['parent_id']]['ref'];
	$actions[$i]['employee'] = $trgr['employee'];
	$actions[$i]['period'] = $trgr['period'];
//	Add formatting to deadline field
		$v = strtotime($trgr['deadline']);
		$diff = $v - $now;
		$diff = floor($diff/(3600*24));
		if($v < $now) {			//overdue
			$diff*=-1;
			$v = "<span class=overdue><b>$diff day".($diff==1?"":"s")."</b> overdue</span>";
		} elseif($v==$now) {	//due today
			$v = "<span class=today>Due today</span>";
		} else {				//due in the future
			$v = "<span>Due in $diff days [".($trgr['deadline'])."]</span>";
		}
	$actions[$i]['deadline'] = $v;
	$actions[$i]['status'] = $trgr['status'];
	if($t==$pm6Object->getSelfType()) {
		$actions[$i]['type'] = $self_name;
	} elseif($t==$pm6Object->getFinalReviewType()) {
		$actions[$i]['type'] = $final_name;
	} elseif(strpos($t,"MOD")!==false) {
		$actions[$i]['type'] = $mod_name;
	} else {
		$actions[$i]['type'] = "Unknown";
	}
	$actions[$i]['link'] = "update.php?redirect=dashboard|i=".$l."|type=".$t;


}

unset($pm6Object);
?>