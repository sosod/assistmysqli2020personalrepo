<?php

$create_type = !isset($create_type) ? "KPI" : $create_type;

$display_navigation_buttons = false;
$no_page_heading = true;
require_once("inc_header.php");

//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($module_setup);

$page_action = $_REQUEST['page_action'];

$scdObj = new PM6_SCORECARD();
$source_name = $scdObj->getAKPASourceName($_REQUEST['modref']);
$group_field = $scdObj->getGroupingField();
$_REQUEST['group_field'] = $group_field;
$_REQUEST['primary_source'] = $scdObj->getPrimaryKPASource();
$_REQUEST['sdbip_id'] = $scdObj->getSourceObject($_REQUEST['modref']);

$modloc = $scdObj->getSourceModloc($_REQUEST['modref']);
$class_name = $modloc."_PMS";
/** @var SDBP6_PMS $extObject */
/** @var IKPI2_PMS $extObject */
$extObj = new $class_name();
if($create_type=="KPI") {
	$result = $extObj->getKPIs($_REQUEST);
} elseif($create_type=="TOP") {
	$result = $extObj->getTopKPIs($_REQUEST);
} else {
	$result = $extObj->getProjects($_REQUEST);
}
//die();
$head = $result['head'];
$objects = $result['objects'];
$latest_group_list = $result['grouping'];
$scdObj->updateGroupingList($latest_group_list,$_REQUEST['modref'],$modloc);
$group_map_for_conversion_to_local_id = $scdObj->getGroupMapConvertingSourceToLocal($_REQUEST['modref']);
//ASSIST_HELPER::arrPrint($head);

$lineObj = new PM6_LINE();
$existing_lines = $lineObj->getLineSrcIDs($_REQUEST['obj_id'], $_REQUEST['modref'],$create_type);
$lines_in_use = $lineObj->getLinesInUse($_REQUEST['modref'],$create_type,array_keys($objects));

?>
<h3><?php echo $source_name; ?></h3>
<form name=frm_list>
	<input type=hidden name=obj_id value=<?php echo $_REQUEST['obj_id']; ?> />
	<input type=hidden name=modref value=<?php echo $_REQUEST['modref']; ?> />
	<input type=hidden name=create_type value=<?php echo $create_type; ?> />
<table id=tbl_list>
	<thead>
		<tr>
			<th><input type="checkbox" id=select_all name=all value=0 /></th>
			<?php
			foreach($head['main'] as $fld=>$name) {
				echo "<th>".$name."</th>";
			}
			?>
			<th>In Use</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($objects as $i => $obj) {
				?>
				<tr <?php echo "  class=\"".($obj['active']==1 ? "" : "inactive")."\""; ?> >
					<td class=center><?php
					if(!in_array($i,$existing_lines)) {
						echo "<input type=hidden name=reftag[".$i."] value=".$obj['ref']." />
						<input type=checkbox class=sel_all name=obj[] value=".$i." />
						<input type=hidden name=group[".$i."] value='".(isset($group_map_for_conversion_to_local_id[$obj['group']])?$group_map_for_conversion_to_local_id[$obj['group']]:0)."' />";
						if(isset($lines_in_use[$i]) && count($lines_in_use[$i])>0) {
							echo "<div style='margin:0 auto;width:16px;text-align:center;padding:0px;'>".$lineObj->getDisplayIcon("person","title='Already assigned to ".count($lines_in_use[$i])." employee".(count($lines_in_use[$i])==1?"":"s")."'","cc0001")."</div>";
						}
					} else {
						echo $lineObj->getDisplayIcon("ok");
					}
					?></td>
					<?php
					foreach($head['main'] as $fld=>$name) {
						if($fld=="ref") {
							echo "<td>".$obj[$fld];
							if($obj['active']!=true) {
								echo "<p><span class=i style='font-size: 75%'>[Deleted at source]</span></p>";
							}
							echo "</td>";
						} else {
							echo "<td>".$obj[$fld]."</td>";
						}
					}
					echo "<td>";
					if(isset($lines_in_use[$i]) && count($lines_in_use[$i])>0) {
						echo implode("<br />",$lines_in_use[$i]);
					}
					echo "</td>";
					?>
				</tr>
				<?php

		}
		?>
	</tbody>
</table>
<p><button name=btn_save id=btn_save>Save</button></p>
</form>
<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


?>
<script type="text/javascript">
$(function() {
	//$("#dlg_kpi", window.parent.parent.opener).dialog("open");
	window.parent.openDialog();

	$("#btn_save").button({
		icons: {primary: "ui-icon-disk"},
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).click(function(e) {
		//Handled by form submit
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	$("form[name=frm_list]").submit(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var err = true;
		$("input:checkbox:gt(0)").each(function() {
			if($(this).prop("checked")==true) {
				err = false;
			}
		});
		if(err) {
			AssistHelper.finishedProcessing("error","No lines have been selected.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_list]"));
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=LINES.ADD",dta);
			if(result[0]=="ok") {
				$("#dlg_msg")
					.html(AssistHelper.getHTMLResult(result[0],result[1],""))
					.dialog("option","buttons",[{
						text: "Ok", click: function() {	window.parent.reloadPage(result);	}
					}]);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
		return false;
	});
	$("#select_all").click(function() {
		$("input:checkbox.sel_all").prop("checked",$(this).prop("checked"));
	});

});
</script>