<?php
$page_redirect_url = "new_scorecards_person";
require_once("inc_header.php");

//find out if user has access to all scorecards or only their own employees
//TO BE PROGRAMMED - ASSUME limit = false
$limit = false;


//then check for any users who already have assessments
$scdObj = new PM6_SCORECARD();
$pending_objects = $scdObj->getPendingObjects($limit);
$activated_objects = $scdObj->getActivatedObjects($limit);
//$active_users = $scdObj->getUsersWithActiveAssessments();

//Get financial year info
$start_date = $scdObj->getFinancialYearStartDate(); $start_date = strtotime($start_date);
$end_date = $scdObj->getFinancialYearEndDate(); $end_date = strtotime($end_date);

$empObj = new PM6_EMPLOYEE();
$available_employees = $empObj->getAvailableEmployeesForCreate($start_date,$end_date);

$bonusObject = new PM6_SETUP_BONUS();
$bonus_scales = $bonusObject->getListOfActiveBonusScales();

ASSIST_HELPER::displayResult(array("info","Only Employees who are non-system users or who have menu access to this module, are available to have scorecards created.  Any Employee who already has an existing scorecard, cannot have another created until that scorecard has been deactivated."));
?>
<span class="float"><button id=btn_add>Create New</button></span>
<h4>Scorecards In Progress</h4>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Job Title</th>
		<th>Bonus Scale</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Activities</th>
		<th>Core<br />Competencies</th>
		<th>Created</th>
		<th>Status</th>
		<th></th>
	</tr>
<?php
$found_scorecards = false;
if(count($pending_objects)>0) {
	$found_scorecards = true;
	foreach($pending_objects as $pa) {
		$raw_status = isset($pa['new_status']) ? $pa['new_status'] : PM6::NOT_YET_STARTED;
		$scd_status = isset($pa['status']) ? $pa['status'] : PM6::ACTIVE;
		$status = $scdObj->convertNewStatusToText($raw_status);
		$general_status = $scdObj->convertSCDStatusToText($scd_status);
		$colour = $scdObj->convertNewStatusToColour($raw_status);
		$general_colour = $scdObj->convertSCDStatusToColour($scd_status);
		$status = array_merge($general_status,$status);
		$colour = array_merge($general_colour,$colour);
		if(is_array($status)) {
			foreach($status as $si => $stat) {
				$colr = $colour[$si];
				$status[$si] = "<span class='".$colr." b'>".$stat."</span>";
			}
			$status = implode("<br />",$status);
		} else {
			$status = "<span class='".$colour." b'>".$status."</span>";
		}

		if(($scd_status&PM6::ACTIVATED)==PM6::ACTIVATED) {
			$step = "view";
		} elseif(($scd_status&PM6::CONFIRMED)==PM6::CONFIRMED) {
			$step = "review";
		} elseif(($raw_status&PM6::IP_SUPER)==PM6::IP_SUPER) {
			$step = "continue";
		} elseif($raw_status==0) {
			$step = "new";
		} else {
			$step = "view";
		}
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class=''>".$pa['employee']."</td>
			<td class=''>".(str_replace("(Started","<br /><small><i>(Started",$pa['job'])."</i></small>")."</td>
			<td class=''>".$pa['bonus_scale']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>".date("d M Y H:i",strtotime($pa['scd_insertdate']))." by ".$pa['create_username']."</td>
			<td class='center'>".$status."</td>
			<td class=center>";
		switch($step) {
			case "view":
			case "review":
				echo "<button class=btn_open obj_id=".$pa['obj_id']." step='$step'>Open</button>";
				break;
			case "new":
			case "continue":
			default:
				echo "<button class=btn_continue obj_id=".$pa['obj_id'].">Continue</button>";
				break;
		}
			echo "<br /><button class=btn_delete obj_id=".$pa['obj_id'].">Delete</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td id='td_none' colspan=11>No pending scorecards awaiting completion.</td>
	</tr>";
}

?>
</table>
<?php




?>
<div id=dlg_new title="Create New">
	<h2>Select Employee</h2>
	<p><?php
	if(count($available_employees)>0) {
		echo "
		<input type=hidden name=emp_id id=emp_id value='' />
		<select name=sel_employee id=sel_employee>
		<option selected value=X>--- SELECT EMPLOYEE ---</option>";
		foreach($available_employees as $tkid => $emp) {
			if($emp['job']===false) {
				$emp_job = "0";
			} else {
				$emp_job = implode("|",$emp['job']);
			}
			echo "<option value=".$tkid." emp_id='".$emp['emp_id']."' jobs='".$emp_job."'>".$emp['name']."</option>";
		}

		echo "</select></p><p><select id=sel_job name=sel_job>
		<option selected value=X>--- SELECT JOB ---</option>";
		echo "</select></p><p><select id=sel_bonusscale name=sel_bonus>
		<option selected value=X>--- SELECT ".($scdObj->replaceObjectNames("|bonusscale|"))." ---</option>";
		foreach($bonus_scales as $b => $s) {
			echo "<option value=".$b.">".$s."</option>";
		}
		echo "</select></p><p><button id=btn_new_next1>Next </button>";
	} else {
		echo "No employees available.";
	}
	?>
	</p>
</div>
<h4>Activated Scorecards</h4>
<table class=list id=tbl_list_activated>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Job Title</th>
		<th>Bonus Scale</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Activities</th>
		<th>Core<br />Competencies</th>
		<th>Created</th>
		<th>Status</th>
		<th></th>
	</tr>
<?php
$found_scorecards = false;
if(count($activated_objects)>0) {
	$found_scorecards = true;
	foreach($activated_objects as $pa) {
		$raw_status = isset($pa['new_status']) ? $pa['new_status'] : PM6::NOT_YET_STARTED;
		$scd_status = isset($pa['status']) ? $pa['status'] : PM6::ACTIVE;
		if($pa['assessments']>0) {
			$status = "Triggered";
			$colour = "black";
			$can_undo = false;
		} else {
			$status = $scdObj->convertNewStatusToText($raw_status);
			$general_status = $scdObj->convertSCDStatusToText($scd_status);
			$colour = $scdObj->convertNewStatusToColour($raw_status);
			$general_colour = $scdObj->convertSCDStatusToColour($scd_status);
			$status = array_merge($general_status,$status);
			$colour = array_merge($general_colour,$colour);
			$can_undo = true;
		}
		if(is_array($status)) {
			foreach($status as $si => $stat) {
				$colr = $colour[$si];
				$status[$si] = "<span class='".$colr." b'>".$stat."</span>";
			}
			$status = implode("<br />",$status);
		} else {
			$status = "<span class='".$colour." b'>".$status."</span>";
		}

		if(($scd_status&PM6::ACTIVATED)==PM6::ACTIVATED) {
			$step = "view";
		} elseif(($scd_status&PM6::CONFIRMED)==PM6::CONFIRMED) {
			$step = "review";
		} elseif(($raw_status&PM6::IP_SUPER)==PM6::IP_SUPER) {
			$step = "continue";
		} elseif($raw_status==0) {
			$step = "new";
		} else {
			$step = "view";
		}
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class=''>".$pa['employee']."</td>
			<td class=''>".(str_replace("(Started","<br /><small><i>(Started",$pa['job'])."</i></small>")."</td>
			<td class=''>".$pa['bonus_scale']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>".date("d M Y H:i",strtotime($pa['scd_insertdate']))." by ".$pa['create_username']."</td>
			<td class='center'>".$status."</td>
			<td class=center>";
		switch($step) {
			case "view":
			case "review":
				echo "<button class=btn_open obj_id=".$pa['obj_id']." step='$step'>Open</button>";
				break;
			case "new":
			case "continue":
			default:
				break;
		}
		if($can_undo) {
			echo "<br /><button class=btn_undo_activation obj_id=".$pa['obj_id'].">Undo Activation</button>";
		}
			echo "<br /><button class=btn_delete obj_id=".$pa['obj_id'].">Delete</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td id='td_none' colspan=11>No activated scorecards to display.</td>
	</tr>";
}
$scd_object_name = $scdObj->getObjectName($scdObj->getMyObjectType());
?>
</table>
<div id=dlg_create_edit title="Undo Activation">
	<input type=text id=create_edit_obj_id value="" />
	<p>"Undo Activation" for <?php $scd_object_name; ?> <label id=lbl_create_edit_obj_id for=create_edit_obj_id /></label> will undo the confirmation and activation done on step <?php echo $scdObj->getNumberOfSteps(); ?> of the creation process and return the <?php echo $scd_object_name; ?> to the create process in New for further amendments.</p>
	<p>The <?php echo $scd_object_name; ?> will not be available for triggering until you complete the entire create process, including approval.</p>
	<p>The <?php echo $scd_object_name; ?> will be marked as In Progress (Super User) and will be available for editing in New > All Scorecards until it is confirmed & approved.</p>
	<p>Are you sure you wish to continue?</p>
</div>
<script type="text/javascript">
$(function() {
	var document_url = "<?php echo $page_redirect_url; ?>.php";
	$("#tbl_list td, #tbl_list_activated td").addClass("center");
	$("#tbl_list tr, #tbl_list_activated tr").each(function() {
		$(this).find("td:eq(1)").removeClass("center");
		$(this).find("td:eq(2)").removeClass("center");
	});
	var t = 0;
	$("#tbl_list_activated tr:first th").each(function() {
		console.log($(this).width()+":"+$("#tbl_list tr:first th:eq("+t+")").width());
		$(this).width($("#tbl_list tr:first th:eq("+t+")").width());
		t++;
	});

	$("#dlg_new").dialog({
		modal: true,
		autoOpen: false
	});
	$("#dlg_create_edit").dialog({
		modal: true,
		autoOpen: false,
		buttons: [{
			text: "Yes",
			icons: {
				primary: "ui-icon-check"
			},
			click: function () {
				AssistHelper.processing();
				var obj_id = $("#dlg_create_edit #create_edit_obj_id").val();
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=SCORECARD.UndoActivation","obj_id="+obj_id);
				if(result[0]=="ok") {
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],document_url);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		}, {
			text: "No",
			icons: {
				primary: "ui-icon-closethick"
			},
			click: function () {
				$("#dlg_create_edit").dialog("close");
			}
		}]
	});
	AssistHelper.hideDialogTitlebar("id","dlg_create_edit");
	AssistHelper.formatDialogButtonsByClass($("#dlg_create_edit"),0,"ui-state-ok");
	AssistHelper.formatDialogButtonsByClass($("#dlg_create_edit"),1,"ui-state-error");

	$("#btn_add").button({
		icons: {primary: "ui-icon-circle-plus"},
	}).click(function() {
		$("#dlg_new").dialog("open");
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	$("#btn_new_next1").button({
		icons: {secondary: "ui-icon-circle-arrow-e"},
	}).click(function() {
		$("#sel_employee").css("background-color","");
		$("#sel_job").css("background-color","");
		$("#sel_bonusscale").css("background-color","");
		var i = $("#sel_employee").val();
		var ji = $("#sel_job").val();
		var bsi = $("#sel_bonusscale").val();
		if(i.length<4) {
			$("#sel_employee").css("background-color","#cc0001");
			alert("Please select an employee.");
		} else if(ji=="X") {
			$("#sel_job").css("background-color","#cc0001");
			alert("Please select which Job / Contract this scorecard relates to.");
		} else if(bsi=="X") {
			$("#sel_bonusscale").css("background-color","#cc0001");
			alert("Please select which <?php echo $scdObj->replaceObjectNames("|bonusscale|"); ?> this scorecard must use for Performance % and Bonus calculations.");
		} else {
			AssistHelper.processing();
			var dta = "step=0&obj_id=0&employee_id="+i+"&emp_id="+$("#emp_id").val()+"&job_id="+$("#sel_job").val()+"&bonus_id="+$("#sel_bonusscale").val();

			var r = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Add",dta);
			if(r[0]=="ok") {
				document.location.href = "<?php echo $page_redirect_url; ?>_step1.php?obj_id="+r['obj_id']+"&r[]=ok&r[]="+r[1];
			} else {
				AssistHelper.finishedProcessing(r[0],r[1]);
			}
		}
	}).removeClass("ui-state-default").addClass("ui-button-bold-grey");
	//.children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-right":"25px","font-size":"80%"
	//});
	$("#btn_new_next1").prop("disabled","disabled").css("cursor","not-allowed");

	$("#sel_job").hide();
	$("#sel_bonusscale").hide();

	$("#sel_employee").change(function() {
		if($(this).val()=="X") {
			$("#sel_job").hide();
			$("#btn_new_next1").prop("disabled","disabled").css("cursor","not-allowed").removeClass("ui-button-bold-green").addClass("ui-button-bold-grey");
		} else {
			var $sel_option = $(this).find("option:selected");
			var emp_id = $sel_option.attr("emp_id");
			$("#emp_id").val(emp_id);
			var jobs_ids = $sel_option.attr("jobs");
			var jobs = AssistHelper.doAjax("inc_controller_assessment.php?action=EMP.getJobsFormattedForSelect","job_id="+jobs_ids);

			if(jobs.length==0 || jobs=="0" || jobs==0 || jobs===false) {
				$("#sel_job").hide();
				alert("Oops, that employee does not appear to have any available Jobs/Contracts for the given Financial Year.  Please select another Employee.");
			} else {
				$("#sel_job").show();
				$("#sel_job").find("option:gt(0)").remove();
				for(i in jobs) {
					$("#sel_job").append($("<option />",{value:i,html:jobs[i]}));
				}
				$("#dlg_new").dialog("option","width","auto");
			}
		}
	});
	$("#sel_job").change(function() {
		if($(this).val()=="X") {
			$("#sel_bonusscale").hide();
		} else {
			$("#sel_bonusscale").show();
		}
	});
	$("#sel_bonusscale").change(function() {
		if($(this).val()=="X") {
			$("#btn_new_next1").prop("disabled","disabled").css("cursor","not-allowed").removeClass("ui-button-bold-green").addClass("ui-button-bold-grey");
		} else {
			$("#btn_new_next1").prop("disabled","").css("cursor","pointer").removeClass("ui-button-bold-grey").addClass("ui-button-bold-green");
		}
	});

	$(".btn_continue").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).removeClass("ui-state-default").addClass("ui-button-bold-grey")
		.hover(
			function() {
				$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
			},function(){
				$(this).addClass("ui-button-bold-grey").removeClass("ui-button-bold-orange");
			}
		).click(function() {
			AssistHelper.processing();
		document.location.href = "<?php echo $page_redirect_url; ?>_step1.php?obj_id="+$(this).attr("obj_id");
	});
	//}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	$(".btn_open").button({
		icons: {primary: "ui-icon-newwin"},
	}).removeClass("ui-state-default").addClass("ui-button-bold-grey")
		.hover(
			function() {
				$(this).removeClass("ui-button-bold-grey").addClass("ui-button-state-orange");
			},function(){
				$(this).addClass("ui-button-bold-grey").removeClass("ui-button-state-orange");
			}
		).click(function() {
			AssistHelper.processing();
			var step = $(this).attr("step");
		document.location.href = "<?php echo $page_redirect_url; ?>_step10.php?page_action="+step+"&obj_id="+$(this).attr("obj_id");
	});

	$(".btn_delete").removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(
			function() {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-red");
			},function(){
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-red");
			}
		).click(function() {
		//document.location.href = "assessment_create_step1.php?obj_id="+$(this).attr("obj_id");
		var ref = $(this).parent().parent().find("td:first").html();
		if(confirm("Are you sure you wish to delete Scorecard "+ref+"?")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.Delete","obj_id="+key);
			if(result[0]=="ok") {
				var url = document_url;
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

	$(".btn_undo_activation").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(
			function() {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
			},function(){
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
			}
		).click(function(e) {
			e.preventDefault();
			var dlg_name = "#dlg_create_edit";
			$(dlg_name+" #create_edit_obj_id").val($(this).attr("obj_id"));
			var ref = $(this).parent().parent().find("td:first").html();
			$("#lbl_create_edit_obj_id").html(ref);
			$(dlg_name).dialog("open");

	/*	if(confirm("Are you sure you wish to undo the activation of Scorecard "+ref+"? WARNING: This will also reverse any employee/manager approval.")==true) {
			AssistHelper.processing();
			var key = $(this).attr("obj_id");
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Scorecard.UndoActivation","obj_id="+key);
			if(result[0]=="ok") {
				var url = document_url;
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}*/
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});



	<?php
	if(!$found_scorecards) {
		?>
	var c = $("#tbl_list tr:first").children("th").size();
	$("#td_none").prop("colspan",c).removeClass("center");
	<?php
	}
	?>
});
</script>