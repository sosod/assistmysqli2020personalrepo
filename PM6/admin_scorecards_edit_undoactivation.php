<?php

$page_action = "edit";
$page_redirect_url = "admin_scorecards_edit.php?obj_id=";
require_once("inc_header.php");

$userObj = new PM6_USERACCESS();
$user = $userObj->getMyUserAccess();
$is_create_super_user = $user['create_object'];

$object_id = $_REQUEST['obj_id'];

$scdObject = new PM6_SCORECARD();
$result = $scdObject->undoActivation($object_id);

if($is_create_super_user) {
	$page_redirect_url = "new_scorecards_person_step1.php?obj_id=".$object_id;
} else {
	$page_redirect_url.=$object_id;
}


?>
<script type="text/javascript">
	$(function () {
		AssistHelper.processing();
		AssistHelper.finishedProcessingWithRedirect("<?php echo $result[0]; ?>","<?php echo $result[1]; ?>","<?php echo $page_redirect_url; ?>");
	});
</script>
