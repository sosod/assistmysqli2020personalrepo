<?php
/**
 * CREATED FOR AA-481 [replaced by AA-581 11 April 2021]
 * JC 28 Sep 2020
 */
$page_action = "trigger_report";

require_once("inc_header.php");



$apObject = new PM6_PERIOD();
$periods = $apObject->getAllAssessmentPeriods();
if(count($periods)==0) {
	ASSIST_HELPER::displayResult(array("error","No Assessment Periods have been created.  Please go to Assessments > Assessment Periods to auto-generate Assessment Periods."));
	die();
}
unset($apObject);

$scdObj = new PM6_SCORECARD();
//For Report - display all SCDs
$scorecards = $scdObj->getObjectsForAssessmentReport();
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($scorecards);
//echo "<hr />";

?>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Job</th>
		<th>Assist Status</th>
		<th>Organisational KPIs</th>
		<th>Individual KPIs</th>
		<th>Core<br />Competencies</th>
		<th>Function<br />Activities</th>
		<th><?php echo $scdObj->getObjectName(PM6_ASSESSMENT::OBJECT_TYPE,true); ?></th>
		<th>Status</th>
	</tr>
<?php
$found_scorecards = false;
if(count($scorecards)>0) {
	$found_scorecards= true;
	foreach($scorecards as $pa) {
		$org_count = (isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)+(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)+(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0);
		$ikpi_count = (isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)+(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)+(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0);
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
			<td class='center'>".str_replace(" (Started","<br /><span class='i'>(Started",$pa['job'])."</span></td>
			<td class='center'>".$scdObj->getAssistStatusInWords($pa['assist_status'])."</td>
			<td>".$org_count."</td>
			<td>".$ikpi_count."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td class=center>".(isset($pa['assessments']) ? $pa['assessments'] : 0)."</td>
			<td class=center>".(isset($pa['status']) ? $pa['status'] : "Error - Unknown")."</td>
		</tr>";
	}
} else {

	echo "
	<tr>
		<td id=td_none colspan=7>No ".$scdObj->getObjectName($scdObj->getObjectType(),true)." available.</td>
	</tr>";
}

?>
</table>
<?php
$info = "
<p class='b u'>Status Glossary:</p>
<ul class='black'>
<li><span class='b'>New:</span> Scorecard has not yet been activated and is either being built or is waiting on approval in the New section;</li>
<li><span class='b'>Available:</span> Scorecard has been completed and approved and is available for triggering or for allocation to an Assessment Group;</li>
<li><span class='b'>Triggered:</span> Scorecard has been triggered for Assessment as an individual Scorecard and is available in Admin > Assessments > Per Scorecard;</li>
<li><span class='b'>Assessed By Group:</span> Scorecard has been allocated to an Assessment Group and can be found in the named Group in Admin > Assessments > Per Group;</li>
</ul>
";
ASSIST_HELPER::displayResult(array("info",$info));

?>
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:lt(2)").removeClass("center");

	$(".btn_open").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "admin_assessments_triggers_details.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});


	<?php
	if(!$found_scorecards) {
		?>
	var c = $("#tbl_list tr:first").children("th").size();
	$("#td_none").prop("colspan",c).removeClass("center");
	<?php
	}
	?>
});
</script>