<?php  
ini_set('max_execution_time',60); 

function processText($v) {
	global $newline;
	return ASSIST_HELPER::decode(str_replace(chr(10),$newline,$v));
}

function processPOE($x) {
	$x = unserialize($x);
	$poe = array();
	if(isset($x['poe'])) { $poe[] = $x['poe']; }
	$a = isset($x['attach']) ? $x['attach'] : array();
	if(count($a)>0) { 
		$attach = array();
		foreach($a as $b) {
			$attach[] = $b['original_filename'];
		}
		$poe[] = "- ".implode(chr(10)."- ",$attach); 
	}
	$v = implode(chr(10),$poe);
	return $v;
}

//ASSIST_HELPER::arrPrint($_REQUEST);

/*** GET REQUEST ***/
	$fields = $_REQUEST['fields'];  //arrPrint($fields);
	$r_calc = $_REQUEST['r_calc'];
	$krsum = $_REQUEST['summary'];
	$filter = $_REQUEST['filter'];
	$groupby = $_REQUEST['groupby'];
	$sort = $_REQUEST['sort'];
	$output = $_REQUEST['output'];
	$kpi_status = isset($_REQUEST['kpi_status']) ? $_REQUEST['kpi_status'] : "normal";
	$log_type = isset($_REQUEST['log_type']) ? $_REQUEST['log_type'] : "ALL";
	$report_head = strlen($_REQUEST['rhead'])>0 ? $_REQUEST['rhead'] : $_SESSION['modtext'].": ".$section_head." Report";
	$report_time = array($_REQUEST['r_from'],$_REQUEST['r_to']);
	sort($report_time);
	if(isset($filter['result']) && ($filter['result']=="ALL" || !is_numeric($filter['result']))) {
		$filter['result'] = array();
		foreach($result_settings as $i => $r) { $filter['result'][$i] = "Y"; }
	} else {
		$i = $filter['result'];
		$filter['result'] = array();
		$filter['result'][$i] = "Y";
	}
/* GROUP BY */
$group = array(); 
if($groupby != "X") {
	if($groupby=="dir" || $groupby == "top_dirid") {
		$g_sql = "SELECT id, value FROM ".$dbref."_dir WHERE active = true ORDER BY sort";
		$g_rs = getRS($g_sql);
		while($g = mysql_fetch_assoc($g_rs)) {
			$group[$g['id']] = $g;
			$group[$g['id']]['objects'] = array();
		}
	} elseif($groupby==$table_fld."subid") {
		$g_sql = "SELECT s.id, CONCAT_WS(' - ',d.value, s.value) as value 
					FROM ".$dbref."_subdir s
					INNER JOIN ".$dbref."_dir d ON s.dirid = d.id AND d.active = true
					WHERE s.active = true ORDER BY d.sort, s.sort";
		$g_rs = getRS($g_sql);
		while($g = mysql_fetch_assoc($g_rs)) {
			$group[$g['id']] = $g;
			$group[$g['id']]['objects'] = array();
		}
	} else {
		switch($head[$groupby]['h_type']) {
		case "LIST":
			$g_sql = "SELECT id, value FROM ".$dbref."_list_".$head[$groupby]['h_table']." WHERE active = true ORDER BY sort, value";
			$g_rs = getRS($g_sql);
			while($g = mysql_fetch_assoc($g_rs)) {
				$group[$g['id']] = $g;
				$group[$g['id']]['objects'] = array();
			}
			break;
		case "BOOL":
			$group[0] = array('id'=>0,'value'=>$head[$groupby]['h_client'],'objects'=>array());
			$group[0] = array('id'=>0,'value'=>"Not ".$head[$groupby]['h_client'],'objects'=>array());
			break;
		}
	}
} else {
	$group["oX"] = array('id'=>"oX",'value'=>"",'objects'=>array());
}

if($kpi_status!="normal") {
	$logs = array();
	$log_ids = array();
	if($kpi_status!="ANY") {
		switch($kpi_status) {
		case "DEL":
			$kpi_active = 0;
			$log_act = "D";
			break;
		case "ADD":
			$kpi_active = 1;
			$log_act = "C";
			break;
		case "EDIT":
			$kpi_active = 1;
			$log_act = "E";
			break;
		}
		switch($section) {
		case "TOP":
			$log_sql = "SELECT DISTINCT tlog_topid as k FROM ".$dbref."_top_log 
					INNER JOIN ".$dbref."_top ON tlog_topid = top_id AND top_active = ".$kpi_active."
					WHERE tlog_act = '".$log_act."'";
					//echo $log_sql;
			break;
		case "KPI":
			$log_sql = "SELECT DISTINCT klog_kpiid as k FROM ".$dbref."_kpi_log 
					INNER JOIN ".$dbref."_kpi ON klog_kpiid = kpi_id AND kpi_active = ".$kpi_active."
					WHERE klog_act = '".$log_act."'";
			break;
		}
		$log_ids = mysql_fetch_fld_one($log_sql,"k");
		$log_ids[] = 0;
	}
	if(count($log_ids)>0 || $kpi_status == "ANY") {
		$log_filter = "";
		switch($section) {
		case "TOP":
			switch($log_type) { case "UPDATE": $log_filter = " AND tlog_act IN ('A','U')"; break; case "EDIT": $log_filter = " AND tlog_act NOT IN ('A','U')"; break; case "ALL": default: $log_filter = ""; }
			$l_sql = "SELECT l.*, CONCAT_WS(' ',t.tkname,t.tksurname) as tkn, l.tlog_tkid as log_userid
						FROM ".$dbref."_top_log l
						LEFT OUTER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = l.tlog_tkid
						WHERE tlog_yn = 'Y' ".($kpi_status != "ANY" ? "AND tlog_topid IN (".implode(",",$log_ids).") " : "" ).$log_filter;
			$l_id = "tlog_topid";
			$l_fld = "tlog";
			break;
		case "KPI":
			switch($log_type) { 
				case "UPDATE": $log_filter = " AND klog_act IN ('U')"; break; 
				case "EDIT": $log_filter = " AND klog_act NOT IN ('U')"; break; 
				case "ALL": default: $log_filter = ""; 
			}
			$l_sql = "SELECT k.*, CONCAT_WS(' ',t.tkname,t.tksurname) as tkn, k.klog_tkid as log_userid
						FROM ".$dbref."_kpi_log k
						LEFT OUTER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = k.klog_tkid
						WHERE klog_yn = 'Y' ".($kpi_status!="ANY" ? "AND klog_kpiid IN (".implode(",",$log_ids).") " : "").$log_filter;
			$l_id = "klog_kpiid";
			$l_fld = "klog";
			break;
		}
		$rs = getRS($l_sql);
			while($row = mysql_fetch_assoc($rs)) {
				$logs[$row[$l_id]][] = " - ".stripslashes(decode($row[$l_fld.'_transaction']))." [performed by ".($row['log_userid']=="IA" ? "Ignite Assist Automated Update" : decode($row['tkn']))." on ".date("d M Y H:i:s",$row[$l_fld.'_date'])."]";
			}
		unset($rs);
	}
} else {
	$logs = array();
	$log_ids = array();
	$kpi_active = 1;
}
//arrPrint($log_ids);
//arrPrint($logs);
//arrPrint($group);

/* WARDS / AREA */
$wa = array();
$fld = $table_fld."wards";
$f = "wards";
$wa[$f] = array();
$k = $wa_tbl_fld[$f];
if($filter[$fld][0]!="ANY" && count($lists[$f])>1) {
	$sql = "SELECT DISTINCT ".$k."_".$table_id."id FROM ".$dbref."_".$fld." WHERE ".$k."_active = true AND ".$k."_listid IN (".implode(",",$filter[$fld]).")";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) { $wa[$f][] = $row[$k."_".$table_id."id"]; }
	$wmnr = count($wa[$f]);
} else {
	$wmnr = "all";
}
$fld = $table_fld."area";
$f = "area";
$wa[$f] = array();
$k = $wa_tbl_fld[$f];
if($filter[$fld][0]!="ANY" && count($lists[$f])>1) {
	$sql = "SELECT DISTINCT ".$k."_".$table_id."id FROM ".$dbref."_".$fld." WHERE ".$k."_active = true AND ".$k."_listid IN (".implode(",",$filter[$fld]).")";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) { $wa[$f][] = $row[$k."_".$table_id."id"]; }
	$amnr = count($wa[$f]);
} else {
	$amnr = "all";
}
if($wmnr == "all" && is_numeric($amnr)) {
	$wa_obj = $wa['area'];
} elseif($amnr == "all" && is_numeric($wmnr)) {
	$wa_obj = $wa['wards'];
} elseif(is_numeric($amnr) && is_numeric($wmnr)) {
	$wa_obj = array_intersect($wa['wards'],$wa['area']);
}
//GET VALUES
$object_wa = array();
$fld = $table_fld."wards";
$f = "wards";
$object_wa[$f] = array();
$k = $wa_tbl_fld[$f];
$sql = "SELECT ".$k."_".$table_id."id as id, code as value FROM ".$dbref."_".$fld." INNER JOIN ".$dbref."_list_".$f." ON ".$k."_listid = id AND active = true WHERE ".$k."_active = true ".((isset($wa_obj) && count($wa_obj)>0) ? "AND ".$k."_".$table_id."id IN (".implode(",",$wa_obj).")" : "");
$rs = getRS($sql);
while($row = mysql_fetch_assoc($rs)) {
	if(!isset($object_wa[$f][$row['id']])) { $object_wa[$f][$row['id']] = array(); }
	$object_wa[$f][$row['id']][] = $row['value'];
}
$fld = $table_fld."area";
$f = "area";
$object_wa[$f] = array();
$k = $wa_tbl_fld[$f];
$sql = "SELECT ".$k."_".$table_id."id as id, value FROM ".$dbref."_".$fld." INNER JOIN ".$dbref."_list_".$f." ON ".$k."_listid = id AND active = true WHERE ".$k."_active = true ".((isset($wa_obj) && count($wa_obj)>0) ? "AND ".$k."_".$table_id."id IN (".implode(",",$wa_obj).")" : "");
$rs = getRS($sql);
while($row = mysql_fetch_assoc($rs)) {
	if(!isset($object_wa[$f][$row['id']])) { $object_wa[$f][$row['id']] = array(); }
	$object_wa[$f][$row['id']][] = $row['value'];
}

/* REPCATE */
//$repcate_items = $listObj->getList("repcate");  //arrPrint($repcate_items);

/* OBJECT SQL */
$sql_fields = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "LIST":
			$sql_fields[] = $h['h_table'].".value as ".$fld;
			if($fld!="dir") {
				$sql_fields[] = "o.".$fld." as o".$fld;
			} else {
				$sql_fields[] = "dir.id as odir";
			}
			break;
		default: $sql_fields[] = "o.".$fld;
	}
}

switch($section) {
case "TOP":
	$object_sqla[0] = "SELECT o.top_id as obj_id, ".implode(", ",$sql_fields);
	$object_sqla[1] = "FROM ".$dbref."_top o
						INNER JOIN ".$dbref."_dir as dir ON o.top_dirid = dir.id AND dir.active = true";
						foreach($mheadings[$section] as $fld => $h) {
							if($h['h_type']=="LIST" && $h['h_table']!="subdir" && $h['h_table']!="dir") {
								$a = $h['h_table']=="calctype" ? "code" : "id";
								$object_sqla[1].= " LEFT OUTER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
							}
						}
	$object_sqla[1].= " WHERE ".($kpi_status!="ANY" ? "o.top_active = ".$kpi_active : "o.top_active >= 0" );
	if(!in_array($kpi_status,array("normal","ANY")) && count($log_ids)>0 ) {
		$object_sqla[1].= " AND o.top_id IN (".implode(",",$log_ids).")";
	}
	break;
case "KPI": default:
	$object_sqla[0] = "SELECT o.kpi_id as obj_id, ".implode(", ",$sql_fields);
	$object_sqla[1] = "FROM ".$dbref."_kpi o
						INNER JOIN ".$dbref."_subdir as subdir ON o.kpi_subid = subdir.id AND subdir.active = true
						INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id AND dir.active = true";
						foreach($mheadings[$section] as $fld => $h) {
							if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
								$a = $h['h_table']=="calctype" ? "code" : "id";
								$object_sqla[1].= " LEFT OUTER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
							}
						}
	$object_sqla[1].= " WHERE ".($kpi_status!="ANY" ? "o.kpi_active = ".$kpi_active : "o.kpi_active >= 0");
	if(!in_array($kpi_status,array("normal","ANY")) && count($log_ids)>0 ) {
		$object_sqla[1].= " AND o.kpi_id IN (".implode(",",$log_ids).")";
	}
	break;
}
$my_sort = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
			if(is_numeric($amnr) || is_numeric($wmnr)) {
				$object_sqla[1].= " AND o.".$table_fld."id IN (".implode(",",$wa_obj).") ";
			}
			break;
		case "AREA":
			break;
		case "TOP":
			switch($filter[$fld][0]) {
				case "ALL": break;
				case "0": $object_sqla[1].=" AND o.kpi_topid=0 "; break;
				case "1": $object_sqla[1].=" AND o.kpi_topid>0 "; break;
			}
			break;
		case "CAP":
			switch($filter[$fld][0]) {
				case "ALL": break;
				case "0": $object_sqla[1].=" AND o.kpi_capitalid=0 "; break;
				case "1": $object_sqla[1].=" AND o.kpi_capitalid>0 "; break;
			}
			break;
		case "BOOL":
			if(is_numeric($filter[$fld])) {
				$object_sqla[1].=" AND o.".$fld." = ".$filter[$fld]." ";
			}
			if(!in_array($fld,$nosort)) { $key = array_keys($sort,$fld);			$sort[$key[0]] = "o.".$fld." DESC"; }
			break;
		case "LIST":
			if(isset($filter[$fld]) && count($filter[$fld])>0 && $filter[$fld][0]!="ANY") {
				if($fld!=$table_fld."calctype") {
					$object_sqla[1].=" AND ".$h['h_table'].".id IN (".implode(",",$filter[$fld]).") ";
				} else {
					$object_sqla[1].=" AND ".$h['h_table'].".code IN ('".implode("','",$filter[$fld])."') ";
				}
			}
			if(!in_array($fld,$nosort)) { $key = array_keys($sort,$fld);			$sort[$key[0]] = $h['h_table'].".value"; }
			break;
		case "TEXTLIST":
			if(isset($filter[$fld]) && count($filter[$fld])>0 && $filter[$fld][0]!="ANY" ) {
				$tlf = array();
				foreach($filter[$fld] as $tl) {
					$tlf[] = "o.".$fld." LIKE '%;".$tl.";%'";
					if($tl=="0" || $tl == 0) {
						$tlf[] = "o.".$fld." = '".$tl."'";
					}
				}
				$object_sqla[1].=" AND (".implode(" OR ",$tlf).") ";
			}
			break;
		default:
			if(isset($filter[$fld])) {
				$flt = ASSIST_HELPER::decode(trim($filter[$fld][0]));
				$match = $filter[$fld][1];
				if(strlen($flt)>0) {
					switch($match) {
						case "EXACT":	$object_sqla[1].=" AND o.".$fld." LIKE '%".$flt."%' "; break;
						case "ALL":
							$flt = explode(" ",$flt);
							foreach($flt as $f) { $object_sqla[1].=" AND o.".$fld." LIKE '%".$f."%' "; }
							break;
						case "ANY":
						default:
							$flt = explode(" ",$flt);
							$object_sqla[1].= " AND (o.".$fld." LIKE '%";
							$object_sqla[1].= implode("%' OR o.".$fld." LIKE '%",$flt);
							$object_sqla[1].= "%') ";
							break;
					}
				}
				if(!in_array($fld,$nosort)) { $key = array_keys($sort,$fld);			$sort[$key[0]] = "o.".$fld; }
			}
			break;
	}
}
if($section=="KPI") { $key = array_keys($sort,"dir");			$sort[$key[0]] = "dir.sort, subdir.sort"; }
$object_sqla[2]=" ORDER BY ".implode(",",$sort);
$getsql = implode(" ",$object_sqla);
$rs = getRS($getsql);
//echo $getsql;
$objects = array();
//echo implode(" ",$object_sqla);
$object_keys = array();
while($row = mysql_fetch_assoc($rs)) { 
	$objects[$row['obj_id']] = $row;
	$object_keys[] = $row['obj_id']; 
	if($groupby!="X") {
//		echo "<P>".$row['obj_id']." :: ".'o'.$groupby." :: ".$row['o'.$groupby]; arrPrint($group[$row['o'.$groupby]]);
		if($groupby=="top_repkpi") {
			if(isset($group[$row[$groupby]])) { $group[$row[$groupby]]['objects'][$row['obj_id']] = "Y"; }
		} else {
			if(isset($group[$row['o'.$groupby]])) { $group[$row['o'.$groupby]]['objects'][$row['obj_id']] = "Y"; }
		}
	} else {
		$group['oX']['objects'][$row['obj_id']] = "Y";
	}
}









/* ASSURANCE LOGS */
$logs = array();
$kas = array();
if(count($object_keys)>0) {
	$sql = "SELECT k.*, CONCAT(tk.tkname,' ',tk.tksurname) as user
			 FROM ".$dbref."_kpi_assurance k
			 LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk ON k.kas_updateuser = tk.tkid
			 WHERE kas_timeid = ".$var['filter']['when']." AND kas_kpiid IN ('".implode("','",$object_keys)."')";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$oi = $row['kas_kpiid'];
		$kas[$oi] = $row;
	}
	
	
	$sql = "SELECT l.*, CONCAT(tk.tkname,' ',tk.tksurname) as user 
			FROM ".$dbref."_kpi_log l
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk ON klog_tkid = tkid 
			WHERE klog_act IN ('A','AC') AND klog_timeid = ".$var['filter']['when']." AND klog_kpiid IN ('".implode("','",$object_keys)."')
			ORDER BY klog_date DESC";
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$oi = $row['klog_kpiid'];
		$i = $row['klog_date'];
		$logs[$oi][$i] = $row['klog_transaction']." [By: ".$row['user']." on ".date("d M Y H:i",$row['klog_date'])."]";
	}
}








//echo $groupby;
//ASSIST_HELPER::arrPrint($group);
/* RESULTS */
$r_sql = "SELECT r.*, CONCAT(tk.tkname,' ',tk.tksurname) as rupdateuser, ".$r_table_fld."updatedate as rupdatedate, ".$r_table_fld."update as rupdate 
			FROM ".$dbref."_".$table_tbl."_results r
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			    ON tk.tkid = r.".$r_table_fld."updateuser
			WHERE ".$r_table_fld."timeid <= ".$report_time[1]." AND ".$r_table_fld.$table_id."id IN (SELECT ".$table_fld."id ".$object_sqla[1].") 
			ORDER BY ".$r_table_fld.$table_id."id, ".$r_table_fld."timeid";
$results = mysql_fetch_alls2($r_sql,"".$r_table_fld.$table_id."id","".$r_table_fld."timeid");
if(in_array("results",$fields)) {
	foreach($objects as $obj_id => $obj) {
		$res = $results[$obj_id];
		$obj_tt = $obj["o".$table_fld.'targettype'];
		$obj_ct = $obj["o".$table_fld.'calctype'];
		$values = array('target'=>array(),'actual'=>array());
		for($t=(($r_calc=="ytd") ? $first_month : $report_time[0]);$t<=$report_time[1];$t+=$time_period_size) {
			if($r_calc=="ytd" || $r_calc=="ptd") { $tp = "ALL"; } else { $tp = $t; }
			$values['target'][$t] = $res[$t][$fld_target];
			$values['actual'][$t] = $res[$t][$fld_actual];
			$r = KPIcalcResult($values,$obj_ct,$report_time,$tp);
			$results[$obj_id][$t]['r'] = $r;
		}
		$t = "ALL";
		$r = KPIcalcResult($values,$obj_ct,$report_time,$t);
		$results[$obj_id][$t]['r'] = $r;
		if(!isset($filter['result'][$r['r']]) || $filter['result'][$r['r']]!="Y") {
			//unset($group[$obj['o'.$groupby]]['objects'][$obj_id]);
			unset($group[$objects[$obj_id]['o'.$groupby]]['objects'][$obj_id]);
			unset($objects[$obj_id]);
			unset($results[$obj_id]);
		} elseif($filter['kr_assurance_status']!="ALL") {
			$act = false;
			switch($filter['kr_assurance_status']) {
				case "ok":			///signed off
					$act = !($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == KPI::ASSURANCE_ACCEPT);
					break;
				case "no_all":		//all rejected
					$act = !(
						($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == KPI::ASSURANCE_REJECT)
						||
						($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == KPI::ASSURANCE_UPDATED)
					);
					break;
				case "no_no":		//rejected without updated
					$act = !($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == KPI::ASSURANCE_REJECT);
					break;
				case "no_yes":		//rejected but user responded
					$act = !($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == KPI::ASSURANCE_UPDATED);
					break;
				case "na_all":		//not reviewed
					$act = !($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == 0);
					break;
				case "na_do":		//not reviewed but with target
					$act = !($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == 0);
					break;
				case "na_ignore":	//not reviewed but with no target
					$act = !($results[$obj_id][$var['filter']['when']]['kr_assurance_status'] == 0);
					break;
			}
			if($act) {
				unset($group[$objects[$obj_id]['o'.$groupby]]['objects'][$obj_id]);				
				unset($objects[$obj_id]);
				unset($results[$obj_id]);
			}
		}
	}
}
//arrPrint($filter);
$tr_dept_comments = array();
if($section=="KPI") {
	/* TOP LAYER */
	if(in_array("kpi_topid",$fields) && count($objects)>0) {
		$top_sql = "SELECT top_id as id, top_value as value FROM ".$dbref."_top WHERE top_active = true AND top_id IN (SELECT kpi_topid FROM ".$dbref."_kpi WHERE kpi_id IN (".implode(",",array_keys($objects))."))";
		$captop['TOP'] = mysql_fetch_alls($top_sql,"id");
	} else {
		$captop['TOP'] = array();
	}
	/* CAPITAL PROJECTS */
	if(in_array("kpi_capitalid",$fields) && count($objects)>0) {
		$cap_sql = "SELECT cap_id as id, cap_name as value FROM ".$dbref."_capital WHERE cap_active = true AND cap_id IN (SELECT kpi_capitalid FROM ".$dbref."_kpi WHERE kpi_id IN (".implode(",",array_keys($objects))."))";
		$captop['CAP'] = mysql_fetch_alls($cap_sql,"id");
	} else {
		$captop['CAP'] = array();
	}
} elseif($section=="TOP" && count($objects)>0 && (in_array("tr_dept",$fields) || in_array("tr_dept_correct",$fields) || in_array("tr_dept_attachment",$fields) ) ) {
	$top_sql = "SELECT own.value as owner, kr.kr_perf as tr_dept, kr.kr_correct as tr_dept_correct, kr.kr_attachment as tr_dept_attachment, k.kpi_id as kpiid, k.kpi_topid as topid, kr.kr_timeid as timeid
				FROM ".$myObj->getDBRef()."_kpi k
				INNER JOIN ".$myObj->getDBRef()."_kpi_results kr
				  ON kr.kr_kpiid = k.kpi_id
				INNER JOIN ".$myObj->getDBRef()."_list_owner own
				  ON k.kpi_ownerid = own.id
				WHERE k.kpi_active = true
				AND k.kpi_topid IN (".implode(",",array_keys($objects)).")
				AND kr.kr_timeid <= ".$report_time[1]."";
	$rows = $myObj->mysql_fetch_all($top_sql);
	foreach($rows as $r) {
		$tr_dept_comments[$r['topid']][$r['timeid']][$r['kpiid']] = $r;
	}
	$all_time = $timeObj->getAllTime("id");
	//arrPrint($all_time);
}













/*** START OUTPUT ***/
$c2 = count($mheadings[$r_section])+($kpi_status!="normal" ? 1 : 0);
foreach($mheadings[$r_section] as $fld => $h) {
	switch($h['h_type']) {
		case "NUM":
			if(!in_array("results",$fields)) { $c2--; }
			break;
		case "TEXT":
		default:
			if(!in_array($fld,$fields)) { $c2--; }
			break;
	}
}
if(in_array("results",$fields)) { $c2++; }
$fc = 0;
$fc++;//			$echo .= $cella[12]."Ref".$cellz[12];
			foreach($head as $fld => $h) { 				if(in_array($fld,$fields)) { 
$fc++;//				$echo .= $cella[12].decode($h['h_client']).$cellz[12]; 
			} 			}
			for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
$fc+=$c2;//				$echo.=$cella[10].($r_calc=="ytd" ? "Year-To-Date As At ": "").($r_calc=="ptd" ? "Period-To-Date As At " : "").$time[$t]['display_full'].$cellz[10];
			}
			//Overall
			if($c2>0 && $report_time[0]!=$report_time[1] && $r_calc=="captured") {
$fc+=3;//				$echo.=$cella[11]."Overall Performance for ".$time[$report_time[0]]['display_short']." to ".$time[$report_time[1]]['display_short'].$cellz[11];
			}
			
$echo = "";
switch($output) {
case "csv":
	$formatting = array(
		'b1'=>"",
		'b2'=>"",
		'i1'=>"",
		'i2'=>"",
	);
	$applied_formatting = "CSV";
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	for($c=1;$c<$c2;$c++) { $cellz[10].="\"\","; }
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22]['OVERALL'] = "\"";	//ptd normal cell
	$cella[22]['NUM'] = "\""; //result cell
	$cella[22]['DOUBLE'] = "\""; //result cell
	$cella[22]['TEXT'] = "\""; //result cell
	$cella[22]['R'][0] = "\""; //result cell
	$cella[22]['R'][1] = "\""; //result cell
	$cella[22]['R'][2] = "\""; //result cell
	$cella[22]['R'][3] = "\""; //result cell
	$cella[22]['R'][4] = "\""; //result cell
	$cella[22]['R'][5] = "\""; //result cell
	$cellz[22]['R'][0] = "\","; //close result cell
	$cellz[22]['R'][1] = "\","; //close result cell
	$cellz[22]['R'][2] = "\","; //close result cell
	$cellz[22]['R'][3] = "\","; //close result cell
	$cellz[22]['R'][4] = "\","; //close result cell
	$cellz[22]['R'][5] = "\","; //close result cell
	$rowa = "";
	$rowz = "\r\n";
	$pagea="\"".$cmpname."\"\r\n\"".$report_head."\"";
	$table[1] = "\r\n";
	$table[9] = "";
	$pagez = "\r\n\"\",\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	break;
case "excel":
	$formatting = array(
		'b1'=>"<b>",
		'b2'=>"</b>",
		'i1'=>"<i>",
		'i2'=>"</i>",
	);
	$applied_formatting = "XLS";
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	if(in_array("results",$fields)) { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[10] = "<td class=\"head\" colspan=$c2>"; //time heading cell
	$cellz[10] = "</td>"; //time heading cell
	$cella[11] = "<td class=\"head\" colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</td>"; //period-to-date heading cell
	$cella[12] = "<td class=\"head\" rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</td>"; //rowspan heading cell
	$cella[2] = "<td class=kpi>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=kpi style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22]['OVERALL'] = "<td style=\"text-align: right;background-color: #eeeeee\" class=kpi>";	//ptd normal cell
	$cella[22]['NUM'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[22]['DOUBLE'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[22]['TEXT'] = "<td class=kpi>"; //result cell
	$cella[22]['R'][0] = "<td class=kpi_r style=\"background-color:".$result_settings[0]['color']."\">"; //result cell
	$cella[22]['R'][1] = "<td class=kpi_r style=\"background-color:".$result_settings[1]['color']."\">&nbsp;&nbsp;"; //result cell
	$cella[22]['R'][2] = "<td class=kpi_r style=\"background-color:".$result_settings[2]['color']."\">&nbsp;&nbsp;"; //result cell
	$cella[22]['R'][3] = "<td class=kpi_r style=\"background-color:".$result_settings[3]['color']."\">&nbsp;&nbsp;"; //result cell
	$cella[22]['R'][4] = "<td class=kpi_r style=\"background-color:".$result_settings[4]['color']."\">&nbsp;"; //result cell
	$cella[22]['R'][5] = "<td class=kpi_r style=\"background-color:".$result_settings[5]['color']."\">&nbsp;&nbsp;"; //result cell
	$cellz[22]['R'][0] = "</td>"; //close result cell
	$cellz[22]['R'][1] = "&nbsp;&nbsp;</td>"; //close result cell
	$cellz[22]['R'][2] = "&nbsp;&nbsp;</td>"; //close result cell
	$cellz[22]['R'][3] = "&nbsp;&nbsp;</td>"; //close result cell
	$cellz[22]['R'][4] = "&nbsp;</td>"; //close result cell
	$cellz[22]['R'][5] = "&nbsp;&nbsp;</td>"; //close result cell
	$rowa = chr(10)."<tr>";
	$rowz = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>KPI Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>"
			.chr(10)." td { font-style: Calibri; font-size:11pt; } 
			.kpi { border-width: thin; border-color: #000000; border-style: solid; vertical-align: top; } 
			.kpi_r { border-width: thin; border-color: #000000; border-style: solid; text-align: center; color: #FFFFFF; vertical-align: top; } 
			.head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } "
			.chr(10)." .title { font-size:20pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:underline; text-align: center; } "
			.chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:none; text-align: center;} 
			.title3 { font-size:14pt; font-style: Calibri; color:#000099; font-weight:bold;} 
			.title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}
			</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$fc>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$fc>";
		$pagea.=$report_head;
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td></td><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = "<br style=\"mso-data-placement:same-cell;\">";
	break;
default:
	$formatting = array(
		'b1'=>"<b>",
		'b2'=>"</b>",
		'i1'=>"<i>",
		'i2'=>"</i>",
	);
	$applied_formatting = "HTML";
	$gcell[1] = "<tr><td colspan=14 style='font-weight:bold;font-size:2em;padding-top:20px;padding-bottom:5px;'>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	if($c2>0) { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[10] = "<th colspan=$c2>"; //time heading cell
	$cellz[10] = "</th>"; //time heading cell
	$cella[11] = "<th colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<th rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</th>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[22]['OVERALL'] = "<td style=\"background-color: #eeeeee\" class=right>";	//ptd normal cell
	$cella[22]['NUM'] = "<td class=right>"; //result cell
	$cella[22]['DOUBLE'] = "<td class=right>"; //result cell
	$cella[22]['STAT'] = "<td class=center>"; //result cell
	$cella[22]['TEXT'] = "<td><div style=\"font-size: 7pt;\">"; //result cell
	$cella[22]['R'][0] = "<td class=\"".$result_settings[0]['style']."\">"; //result cell
	$cella[22]['R'][1] = "<td class=\"".$result_settings[1]['style']."\">&nbsp;&nbsp;"; //result cell
	$cella[22]['R'][2] = "<td class=\"".$result_settings[2]['style']."\">&nbsp;&nbsp;"; //result cell
	$cella[22]['R'][3] = "<td class=\"".$result_settings[3]['style']."\">&nbsp;&nbsp;"; //result cell
	$cella[22]['R'][4] = "<td class=\"".$result_settings[4]['style']."\">&nbsp;"; //result cell
	$cella[22]['R'][5] = "<td class=\"".$result_settings[5]['style']."\">&nbsp;&nbsp;"; //result cell
	$cellz[22]['R'][0] = "</td>"; //close result cell
	$cellz[22]['R'][1] = "&nbsp;&nbsp;</td>"; //close result cell
	$cellz[22]['R'][2] = "&nbsp;&nbsp;</td>"; //close result cell
	$cellz[22]['R'][3] = "&nbsp;&nbsp;</td>"; //close result cell
	$cellz[22]['R'][4] = "&nbsp;</td>"; //close result cell
	$cellz[22]['R'][5] = "&nbsp;&nbsp;</td>"; //close result cell
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"inc/main.css\" type=\"text/css\">";
	if(!isset($_REQUEST['show_title']) || $_REQUEST['show_title']==true) {
		$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 style=\"text-align: center;\">".$cmpname."</h1><h2 style=\"text-align: center;\">";
		$pagea.=$report_head;
		$pagea.= "</h2><table width=100%>";
	} else {
		$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
		$pagea.= "<table width=100%>";
	}	
	$table[1] = ""; //"<table>"
	$table[9] = ""; //"</table>";
	$pagez = "</table><p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	break;
}
$totals = array('all'=>0);
foreach($result_settings as $i => $r) { $totals[$i] = 0; }
//START PAGE
$echo = $pagea;
$echo.= $table[1];
//GROUP BY 
foreach($group as $i => $g) {
	$g_objects = $g['objects'];
	if(count($g_objects)>0) {
		$groupresults = array('total'=>0);
		foreach($result_settings as $r) { $groupresults[$r['r']]=0; }
		if(strlen($g['value'])>0) {
			$echo.= $table[9].$gcell[1].ASSIST_HELPER::decode($g['value']).$gcell[9].$table[1];
		}
		//Main heading
		$echo.= $rowa;
			$echo .= $cella[12]."Ref".$cellz[12];
			foreach($head as $fld => $h) {
				if(in_array($fld,$fields)) { $echo .= $cella[12].ASSIST_HELPER::decode($h['h_client']).$cellz[12]; }
			}
			for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
				$echo.=$cella[10].($r_calc=="ytd" ? "Year-To-Date As At ": "").($r_calc=="ptd" ? "Period-To-Date As At " : "").$time[$t]['display_full'].$cellz[10];
			}
			//Overall
			if($c2>0 && $report_time[0]!=$report_time[1] && $r_calc=="captured") {
				$echo.=$cella[11]."Overall Performance for ".$time[$report_time[0]]['display_short']." to ".$time[$report_time[1]]['display_short'].$cellz[11];
			}
			if($kpi_status!="normal") {
				$echo .= $cella[12]."Change Log".$cellz[12];
			}
		$echo .= $rowz;
		//Time heading
		if($c2>0) {
			$echo.= $rowa;
			if($output=="csv") {
				for($c=1;$c<=count($head);$c++) $echo.=$cella[12].$cellz[12];
			}
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
					foreach($mheadings[$r_section] as $fld => $h) {
						switch($h['h_type']) {
							case "NUM":
								if(in_array("results",$fields)) { 
									$echo.=$cella[1].ASSIST_HELPER::decode($h['h_client']).$cellz[1];
									if($fld == $fld_actual) { $echo.=$cella[1]."R".$cellz[1]; }
								}
								break;
							case "TEXT":
								if(in_array($fld,$fields)) { $echo.=$cella[1].ASSIST_HELPER::decode($h['h_client']).$cellz[1]; }
								break;
							default:
								if(in_array($fld,$fields)) { $echo.=$cella[1].decode($h['h_client']).$cellz[1]; }
								break;
						}
					}
					if($kpi_status!="normal") { $echo.=$cella[1]."Status".$cellz[1]; }
				}
				if($c2>0 && $report_time[0]!=$report_time[1] && $r_calc=="captured") {
					foreach($mheadings[$r_section] as $fld => $h) {
						switch($h['h_type']) {
							case "NUM":
									$echo.=$cella[1].$h['h_client'].$cellz[1]; 
									if(($section=="KPI" || $section=="TOP") && $fld == $r_table_fld."actual") { $echo.=$cella[1]."R".$cellz[1]; }
								break;
							case "TEXT":
								break;
						}
					}
				}
			$echo .= $rowz;
		} //arrPrint($repcate);
		foreach($g_objects as $obj_id => $g_obj) {
		if(isset($objects[$obj_id])) {
			$obj = $objects[$obj_id];
			$echo .= $rowa.$cella[2].$id_labels_all[$section].$obj_id.$cellz[2];
			foreach($head as $fld => $h) {
				if(in_array($fld,$fields)) { 
					$echo.=$cella[2];
					switch($h['h_type']) {
						case "WARDS":
						case "AREA":
							$echo.= isset($object_wa[strtolower($h['h_type'])][$obj_id]) ? decode(implode("; ",$object_wa[strtolower($h['h_type'])][$obj_id])) : "";
							break;
						case "TOP":
						case "CAP":
							if($obj[$fld]>0 && isset($captop[$h['h_type']][$obj[$fld]])) {
								$echo .= decode($captop[$h['h_type']][$obj[$fld]]['value'])." [".$captop[$h['h_type']][$obj[$fld]]['id']."]";
							}
							break;
						case "TEXTLIST":
							$tl = $lists[$h['h_table']];
							$v = explode(";",$obj[$fld]);
							$s = array();
							foreach($v as $x) {
								//$echo.="<br />".$x."::".isset($tl[$x]);
								if(isset($tl[$x])) {
									$s[] = $tl[$x]['value'];
								}
							}
							if(count($s)>0) {
								$echo.=implode("; ",$s);
							} else {
								$echo.=$unspecified;
							}
							//$echo.= implode("-",$v);
							break;
						case "LIST":
						default:
							if($fld=="kpi_annual" || $fld == "kpi_revised" || $fld=="top_annual" || $fld == "top_revised") {
								$echo.= KPIresultDisplay($obj[$fld],$obj["o".$table_fld.'targettype']);
							} else {
								$echo.= str_replace(chr(10),$newline,decode($obj[$fld]));
							}
							break;
					}
					$echo.=$cellz[2];
				}
			}
			if($c2>0) { //results
				/** MONTHLY RESULTS **/
				$res = $results[$obj_id]; //arrPrint($res);
				$obj_tt = $obj["o".$table_fld.'targettype'];
				$obj_ct = $obj["o".$table_fld.'calctype'];
				$values = array('target'=>array(),'actual'=>array());
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
					//$values['target'][$t] = $res[$t][$fld_target];
					//$values['actual'][$t] = $res[$t][$fld_actual];
					//$r = KPIcalcResult($values,$obj_ct,$report_time,$t);
					$r = $res[$t]['r'];
					foreach($mheadings[$r_section] as $fld => $h) {
						switch($h['h_type']) {
							case "NUM":
								if(in_array("results",$fields)) { 
									$echo.=$cella[22][$h['h_type']].KPIresultDisplay($r[substr($fld,3,6)],$obj_tt).$cellz[2];
									if($fld==$r_table_fld."actual") {
										$echo.=$cella[22]['R'][$r['r']].$r['text'].$cellz[22]['R'][$r['r']];
									}
								}
								break;
							case "TEXT":
								if(in_array($fld,$fields)) { 
									$echo.=$cella[22][$h['h_type']];
									if(substr($fld,0,7)=="tr_dept") {
										//$echo.=decode(displayTRDept(str_replace(chr(10),$newline,$res[$t][$fld]),$applied_formatting));
										//$echo.="CHECK DEPARTMENTAL FOR COMMENTS";
										//$echo.=serialize($tr_dept_comments[$obj_id][$t]);
										$v = array();
										$t_start = $t - $time_period_size + 1;
										for($ti=$t_start;$ti<=$t;$ti++) {
											$r = isset($tr_dept_comments[$obj_id][$ti]) ? $tr_dept_comments[$obj_id][$ti] : array();
											if(count($r)>0) {
												foreach($r as $a) {
													if($fld=="tr_dept_attachment") {
														$b = processPOE($a[$fld]);
													} else {
														$b = $a[$fld];
													}
													if(strlen($b)>0) {
														$x = $formatting['b1']."[".DEPT::REFTAG.$a['kpiid']."] ".$a['owner'].": ".$formatting['b2'].$b.$formatting['i1']." (".$all_time[$ti]['display'].")".$formatting['i2'];
														$v[] = processText($x);
													}
												}
											}
										}
										//$v[] = $t_start;
										//$v[] = $t;
										$echo.=implode($newline,$v);
									} else {
										$v = $res[$t][$fld];
										if(in_array($fld,array('tr_attachment','kr_attachment'))) {
											$v = processPOE($v);
										}
										$echo.=processText($v);
									}
									$echo.=$cellz[2];
								}
								break;
							case "STAT":
								$v = $res[$t][$fld];
								$echo.=$cella[22]['STAT'];
								switch($v){
									case KPI::ASSURANCE_ACCEPT:
										$v = ASSIST_HELPER::drawStatus("Y")."Sign-off";
										break;
									case KPI::ASSURANCE_REJECT:
										$v = ASSIST_HELPER::drawStatus("N")."Rejected";
										break;
									case KPI::ASSURANCE_UPDATED:
										$v = ASSIST_HELPER::drawStatus("warn")."Updated";
										break;
									default:
										$v = ASSIST_HELPER::drawStatus("info")."Not reviewed";
										break;
								}
								$echo.=$v;
								$echo.=$cellz[2];
								break;
							case "LOG":
								$echo.=$cella[22]['TEXT'];
								$v = array();
								if(isset($kas[$obj_id])) {
									$k = $kas[$obj_id];
									$v[] = "<span class=b>Current Assurance Status: </span>";
									if(strlen($k['kas_response'])==0) {
										$v[] = ($k['kas_result']==KPI::ASSURANCE_ACCEPT ? "Update signed-off" : "Update rejected").";";
									} else {
										$v[] = $kas[$obj_id]['kas_response'].";";
									}
									if($k['kas_result']!=KPI::ASSURANCE_ACCEPT) {
										$v[] = "<br />Update user: ".$k['user'].";";
										$v[] = "<br />Deadline: ".($k['kas_deadline']!="0000-00-00"?$k['kas_deadline']:"Time Period Closure")."";
									}
								}
								if(isset($logs[$obj_id]) && count($logs[$obj_id])) {
									$v[] = "<br /><span class=b>Assurance History:</span>";
									foreach($logs[$obj_id] as $l) {
										$v[] = "<br />- ".$l."";
									}
								}
								$echo.=implode("",$v).$cellz[2];
								break;
						}
					}
					if($kpi_status!="normal") { 
						$response = "";
						if($res[$t]['rupdate']==true) {
							$response = "Updated by ";
							if(strlen($res[$t]['rupdateuser'])>0) {
								if($res[$t]['rupdateuser']=="AUTO") {
									$response.="Auto Update";
								} else {
									$response.= $res[$t]['rupdateuser'];
								}
							} elseif($res[$t][$r_table_fld."updateuser"]=="AUTO") {
								$response.="Auto Update";
							} else {
								$response.="Unknown User";
							}
							$response.=" on ".date("d-M-Y",strtotime($res[$t]['rupdatedate']));
						} else {
							$response = "Not updated";
						}
						$echo.=$cella[2].$response.$cellz[2]; 
					}
				}
				/** OVERALL **/
				$r = $res['ALL']['r'];
				$groupresults[$r['r']]++;
				$groupresults['total']++;
				$totals[$r['r']]++;
				$totals['all']++;
				if($report_time[0]!=$report_time[1] && $r_calc=="captured") {
					//$r = KPIcalcResult($values,$obj_ct,$report_time,"ALL");
					foreach($mheadings[$r_section] as $fld => $h) {
						switch($h['h_type']) {
							case "NUM":
									$echo.=$cella[22]['OVERALL'].KPIresultDisplay($r[substr($fld,3,6)],$obj_tt).$cellz[2]; 
									if($fld==$r_table_fld."actual") {
										$echo.=$cella[22]['R'][$r['r']].$r['text'].$cellz[22]['R'][$r['r']];
									}
								break;
							case "TEXT":
								break;
						}
					}
				}
			}
			if($kpi_status!="normal" && isset($logs[$obj_id]) && is_array($logs[$obj_id])) {
				$echo.= $cella[2].implode($newline,$logs[$obj_id]).$cellz[2];
			}
			$echo.=$rowz;
		} else {
			$groupresults['total']++;
			$totals['all']++;
		}	//if object set i.e. not unset by result filter
		} //foreach g['objects']
		if($krsum == "Y" && in_array("results",$fields) && $g['id']!="oX") {
			$echo.= drawKRSum('title0',$g['value'],0,0);
			foreach($result_settings as $i => $r) {
				if(isset($filter['result'][$i]) && $filter['result'][$i]=="Y") {
					$echo.= drawKRSum('row',$r['value'],$groupresults[$r['r']],$r['r']);
				}
			}
			$echo.= drawKRSum('sum','Total KPIs',$groupresults['total'],0);
		}
	} //if count(g['objects']) > 0
} //foreach group
$echo .= $table[9];
if($krsum=="Y" && in_array("results",$fields)) {
			$echo.= drawKRSum('title','',0,0);
			foreach($result_settings as $i => $r) {
				if(isset($filter['result'][$i]) && $filter['result'][$i]=="Y") {
					$echo.= drawKRSum('row',$r['value'],$totals[$r['r']],$r['r']);
				}
			}
			$echo.= drawKRSum('sum','Total KPIs',$totals['all'],0);
}
$echo.= $pagez;

if($output == "csv") { $ext = ".csv"; $content = 'text/plain'; } else { $ext = ".xls"; $content = 'application/ms-excel'; }
switch($output) {
case "csv":
case "excel":
	$path = $report_save_path;
	$sys_filename = $modref."_".$section."_".date("Ymd_His").$ext;
	saveEcho($path,$sys_filename,$echo);
	$usr_filename = $section."_report_".date("Ymd_His",$today).$ext;
	downloadFile2($path, $sys_filename, $usr_filename, $content);
	break;
default:
	echo $echo;
	break;
}

//arrPrint($_REQUEST);
?>