<?php 
ini_set('max_execution_time',1800);
include_once ("inc/header.php");
/*** GET REQUEST ***/
$fields = $_REQUEST['fields'];
//$krsum = $_REQUEST['summary'];
$filter = $_REQUEST['filter'];
$groupby = $_REQUEST['groupby'];
$sort = $_REQUEST['sort'];
$output = $_REQUEST['output'];
$report_head = strlen($_REQUEST['rhead'])>0 ? $_REQUEST['rhead'] : $_SESSION['modtext'].": ".$section_head." Report";
$report_time = array($_REQUEST['r_from'],$_REQUEST['r_to']);
sort($report_time);

//arrPrint(get_defined_vars());

/* GROUP BY */
$group = array();
if($groupby != "X") {
	if($groupby=="dir") {
		$g_sql = "SELECT id, value FROM ".$dbref."_dir WHERE active = true ORDER BY sort";
		//$g_rs = getRS($g_sql);
        $rows = $helperObj->mysql_fetch_all($g_sql);
		//while($g = mysql_fetch_assoc($g_rs)) {
        foreach ($rows as $g){
			$group[$g['id']] = $g;
			$group[$g['id']]['objects'] = array();
		}
	} elseif($groupby == $table_fld."subid") {
		$g_sql = "SELECT s.id, CONCAT_WS(' - ',d.value, s.value) as value 
					FROM ".$dbref."_subdir s
					INNER JOIN ".$dbref."_dir d ON s.dirid = d.id AND d.active = true
					WHERE s.active = true ORDER BY d.sort, s.sort";
        $rows = $helperObj->mysql_fetch_all($g_sql);
        //while($g = mysql_fetch_assoc($g_rs)) {
        foreach ($rows as $g){
			$group[$g['id']] = $g;
			$group[$g['id']]['objects'] = array();
		}
	} else {
		switch($head[$groupby]['h_type']) {
		case "LIST":
			$g_sql = "SELECT id, value FROM ".$dbref."_list_".$head[$groupby]['h_table']." WHERE active = true ORDER BY sort, value";
            $rows = $helperObj->mysql_fetch_all($g_sql);
            //while($g = mysql_fetch_assoc($g_rs)) {
            foreach ($rows as $g){
				$group[$g['id']] = $g;
				$group[$g['id']]['objects'] = array();
			}
			break;
		case "BOOL":
			$group[0] = array('id'=>0,'value'=>$head[$groupby]['h_client'],'objects'=>array());
			$group[0] = array('id'=>0,'value'=>"Not ".$head[$groupby]['h_client'],'objects'=>array());
			break;
		}
	}
} else {
	$group["oX"] = array('id'=>"oX",'value'=>"",'objects'=>array());
}

/* WARDS / AREA / FUND SRC */
if(isset($wa_tbl_fld)) {
	$wa = array();
	//WARDS 
	$fld = $table_tbl."_wards";
	$f = "wards";
	$wa[$f] = array();
	$k = $wa_tbl_fld[$f];
	if($filter[$fld][0]!="ANY" && count($lists[$f])>1) {
		$sql = "SELECT DISTINCT ".$k."_".$table_id."id FROM ".$dbref."_".$fld." WHERE ".$k."_active = true AND ".$k."_listid IN (".implode(",",$filter[$fld]).")";
		//$rs = getRS($sql);
        $rows = $helperObj->mysql_fetch_all($sql);
		//while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
		    $wa[$f][] = $row[$k."_".$table_id."id"]; }
		$wmnr = count($wa[$f]);
	} else {
		$wmnr = "all";
	}
	//AREA
	$fld = $table_tbl."_area";
	$f = "area";
	$wa[$f] = array();
	$k = $wa_tbl_fld[$f];
	if($filter[$fld][0]!="ANY" && count($lists[$f])>1) {
		$sql = "SELECT DISTINCT ".$k."_".$table_id."id FROM ".$dbref."_".$fld." WHERE ".$k."_active = true AND ".$k."_listid IN (".implode(",",$filter[$fld]).")";
		//$rs = getRS($sql);
        $rows = $helperObj->mysql_fetch_all($sql);
		//while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
		    $wa[$f][] = $row[$k."_".$table_id."id"]; }
		$amnr = count($wa[$f]);
	} else {
		$amnr = "all";
	}
	//FUND SRC
	$fld = $table_tbl."_fundsource";
	$f = "fundsource";
	$wa[$f] = array();
	$k = $wa_tbl_fld[$f];
	if($filter[$fld][0]!="ANY" && count($lists[$f])>1) {
		$sql = "SELECT DISTINCT ".$k."_".$table_id."id FROM ".$dbref."_".$fld." WHERE ".$k."_active = true AND ".$k."_listid IN (".implode(",",$filter[$fld]).")";
		//$rs = getRS($sql);
        $rows = $helperObj->mysql_fetch_all($sql);
		//while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
		    $wa[$f][] = $row[$k."_".$table_id."id"]; }
		$fmnr = count($wa[$f]);
	} else {
		$fmnr = "all";
	}
	/*if($wmnr == "all" && is_numeric($amnr)) {
		$wa_obj = $wa['area'];
	} elseif($amnr == "all" && is_numeric($wmnr)) {
		$wa_obj = $wa['wards'];
	} elseif(is_numeric($amnr) && is_numeric($wmnr)) {
		$wa_obj = array_intersect($wa['wards'],$wa['area']);
	}*/
	if(!(!ASSIST_HELPER::checkIntRef($wmnr) && !ASSIST_HELPER::checkIntRef($amnr) && !ASSIST_HELPER::checkIntRef($fmnr))) {
		if(checkIntRef($wmnr)) $wa_obj = $wa['wards'];
		if(checkIntRef($amnr)) { if(isset($wa_obj)) $wa_obj = array_intersect($wa_obj, $wa['area']); else $wa_obj = $wa['area']; }
		if(checkIntRef($fmnr)) { if(isset($wa_obj)) $wa_obj = array_intersect($wa_obj, $wa['fundsource']); else $wa_obj = $wa['fundsource']; }
	}
	//GET VALUES - WARDS
	$object_wa = array();
	$fld = $table_tbl."_wards";
	$f = "wards";
	$object_wa[$f] = array();
	$k = $wa_tbl_fld[$f];
	$sql = "SELECT ".$k."_".$table_id."id as id, code as value FROM ".$dbref."_".$fld." INNER JOIN ".$dbref."_list_".$f." ON ".$k."_listid = id AND active = true WHERE ".$k."_active = true ".((isset($wa_obj) && count($wa_obj)>0) ? "AND ".$k."_".$table_id."id IN (".implode(",",$wa_obj).")" : "");
	//$rs = getRS($sql);
    $rows = $helperObj->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		if(!isset($object_wa[$f][$row['id']])) { $object_wa[$f][$row['id']] = array(); }
		$object_wa[$f][$row['id']][] = $row['value'];
	}
	//GET VALUES - AREA
	$fld = $table_tbl."_area";
	$f = "area";
	$object_wa[$f] = array();
	$k = $wa_tbl_fld[$f];
	$sql = "SELECT ".$k."_".$table_id."id as id, value FROM ".$dbref."_".$fld." INNER JOIN ".$dbref."_list_".$f." ON ".$k."_listid = id AND active = true WHERE ".$k."_active = true ".((isset($wa_obj) && count($wa_obj)>0) ? "AND ".$k."_".$table_id."id IN (".implode(",",$wa_obj).")" : "");
	//$rs = getRS($sql);
    $rows = $helperObj->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		if(!isset($object_wa[$f][$row['id']])) { $object_wa[$f][$row['id']] = array(); }
		$object_wa[$f][$row['id']][] = $row['value'];
	}
	//GET VALUES - FUNDSRC
	$fld = $table_tbl."_fundsource";
	$f = "fundsource";
	$object_wa[$f] = array();
	$k = $wa_tbl_fld[$f];
	$sql = "SELECT ".$k."_".$table_id."id as id, value FROM ".$dbref."_".$fld." INNER JOIN ".$dbref."_list_".$f." ON ".$k."_listid = id AND active = true WHERE ".$k."_active = true ".((isset($wa_obj) && count($wa_obj)>0) ? "AND ".$k."_".$table_id."id IN (".implode(",",$wa_obj).")" : "");
	//$rs = getRS($sql);
    $rows = $helperObj->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		if(!isset($object_wa[$f][$row['id']])) { $object_wa[$f][$row['id']] = array(); }
		$object_wa[$f][$row['id']][] = $row['value'];
	}
}
/* OBJECT SQL */
$sql_fields = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			break;
		case "LIST":
			$sql_fields[] = $h['h_table'].".value as ".$fld;
			if($fld!="dir") {
				$sql_fields[] = "o.".$fld." as o".$fld;
			} else {
				$sql_fields[] = "dir.id as odir";
			}
			break;
		default: $sql_fields[] = "o.".$fld;
	}
}
switch($section) {
case "CAP":
	$object_sqla[0] = "SELECT o.cap_id as obj_id, ".implode(", ",$sql_fields);
	$object_sqla[1] = "FROM ".$dbref."_capital o
						INNER JOIN ".$dbref."_subdir as subdir ON o.cap_subid = subdir.id AND subdir.active = true
						INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id AND dir.active = true";
						foreach($mheadings[$section] as $fld => $h) {
							$a = "id";
							if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
								$object_sqla[1].= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
							}
						}
	$object_sqla[1].= " WHERE o.cap_active = true";
	break;
case "CF":
	$object_sqla[0] = "SELECT o.cf_id as obj_id, ".implode(", ",$sql_fields);
	$object_sqla[1] = "FROM ".$dbref."_cashflow o
						INNER JOIN ".$dbref."_subdir as subdir ON o.cf_subid = subdir.id AND subdir.active = true
						INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id AND dir.active = true";
						foreach($mheadings[$section] as $fld => $h) {
							$a = "id";
							if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
								$object_sqla[1].= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
							}
						}
	$object_sqla[1].= " WHERE o.cf_active = true";
	break;
}
	$my_sort = array();
foreach($head as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
			if(is_numeric($amnr) || is_numeric($wmnr)) {
				$object_sqla[1].= " AND o.".$table_fld."id IN (".implode(",",$wa_obj).") ";
			}
			break;
		case "AREA":
		case "FUNDSRC":
			break;
		case "DATE":
			$datefilter = $filter[$fld];
			if( ( strlen($datefilter[0])+strlen($datefilter[1]) ) > 0 ) {	//if there is a date then do something
				if($datefilter[0]==$datefilter[1] || strlen($datefilter[0])==0 || strlen($datefilter[1])==0) {	//if there is only one date
					if(strlen($datefilter[0])==0 || $datefilter[0]==$datefilter[1]) {
						$df = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[1]." 23:59:59") : strtotime($datefilter[1]); 
					} else {
						$df = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[0]." 23:59:59") : strtotime($datefilter[0]); 
					}
					$object_sqla[1].= " AND o.".$fld." = $df ";
				} else {
					$df0 = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[0]." 23:59:59") : strtotime($datefilter[0]); 
					$df1 = ($fld=="cap_planend" || $fld=="cap_actualend") ? strtotime($datefilter[1]." 23:59:59") : strtotime($datefilter[1]); 
					$object_sqla[1].= " AND o.".$fld." >= $df0 AND o.$fld <= $df1 ";
				}
			}
			break;
		case "LIST":
			if($filter[$fld][0]!="ANY") {
				if($fld!=$table_fld."calctype") {
					$object_sqla[1].=" AND ".$h['h_table'].".id IN (".implode(",",$filter[$fld]).") ";
				} else {
					$object_sqla[1].=" AND ".$h['h_table'].".code IN ('".implode("','",$filter[$fld])."') ";
				}
			}
			if(!in_array($fld,$nosort)) { $key = array_keys($sort,$fld);			$sort[$key[0]] = $h['h_table'].".value"; }
			break;
		default:
			$flt = decode(trim($filter[$fld][0]));
			$match = $filter[$fld][1];
			if(strlen($flt)>0) {
				switch($match) {
					case "EXACT":	$object_sqla[1].=" AND o.".$fld." LIKE '%".$flt."%' "; break;
					case "ALL":
						$flt = explode(" ",$flt);
						foreach($flt as $f) { $object_sqla[1].=" AND o.".$fld." LIKE '%".$f."%' "; }
						break;
					case "ANY":
					default:
						$flt = explode(" ",$flt);
						$object_sqla[1].= " AND (o.".$fld." LIKE '%";
						$object_sqla[1].= implode("%' OR o.".$fld." LIKE '%",$flt);
						$object_sqla[1].= "%') ";
						break;
				}
			}
			if(!in_array($fld,$nosort)) { $key = array_keys($sort,$fld);			$sort[$key[0]] = "o.".$fld; }
			break;
	}
}
$key = array_keys($sort,"dir");			 $sort[$key[0]] = "dir.sort, subdir.sort"; 
$object_sqla[2]=" ORDER BY ".implode(",",$sort).", o.".$table_fld."id";
//$rs = getRS(implode(" ",$object_sqla));
$rows = $helperObj->mysql_fetch_all(implode(" ",$object_sqla));
$objects = array();
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
	$objects[$row['obj_id']] = $row; 
	if($groupby!="X") {
//		echo "<P>".$row['obj_id']." :: ".'o'.$groupby." :: ".$row['o'.$groupby]; arrPrint($group[$row['o'.$groupby]]);
		if(isset($group[$row['o'.$groupby]])) { $group[$row['o'.$groupby]]['objects'][$row['obj_id']] = "Y"; }
	} else {
		$group['oX']['objects'][$row['obj_id']] = "Y";
	}
}

/* RESULTS */
$r_sql = "SELECT * FROM ".$dbref."_".$table_tbl."_results WHERE ".$r_table_fld."timeid <= ".$report_time[1]." AND ".$r_table_fld.$table_id."id IN (SELECT ".$table_fld."id ".$object_sqla[1].") ORDER BY ".$r_table_fld.$table_id."id, ".$r_table_fld."timeid";
$results = $helperObj->mysql_fetch_all_fld2($r_sql,"".$r_table_fld.$table_id."id","".$r_table_fld."timeid");

if($section=="CAP") {
	//arrPrint($results);
	foreach($results as $cap_id => $r) {
		//$last = end($r);
			foreach($r as $key=>$s) {
				if($s['cr_tot_actual']==0 && $s['cr_tot_target']>0 && $s['cr_tot_var']==0) {
					$results[$cap_id][$key]['cr_var'] = $s['cr_target']*(1);
					$results[$cap_id][$key]['cr_ytd_var'] = $s['cr_ytd_target']*(1);
					$results[$cap_id][$key]['cr_tot_var'] = $s['cr_tot_target']*(1);
				}
			}
	}
}

/*** START OUTPUT ***/
//$c2 = count($mheadings[$h_section])*count($mheadings[$r_section]);
$c2 = 0;
$total_colspan = count($fields);
foreach($mheadings[$h_section] as $fld => $h) { if(in_array($fld,$fields)) { $c2+= count($mheadings[$r_section]); $total_colspan--; } }
$fc = $total_colspan+($c2*(1+$report_time[1]-$report_time[0]))+1;
$echo = "";
//$cspan = 1 + count($fields);
//$tspan = (count($time) * $c2) + 3;
//$cspan += $tspan;
switch($output) {
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	for($c=1;$c<$c2;$c++) { $cellz[10].="\"\","; }
	$cella[19] = "\""; //heading cell
	$cellz[19] = "\","; //heading cell
	for($c=1;$c<count($mheadings[$r_section]);$c++) { $cellz[19].="\"\","; }
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22]['OVERALL'] = "\"";	//ptd normal cell
	$cella[22]['NUM'] = "\""; //result cell
	$cella[22]['DOUBLE'] = "\""; //result cell
	$cella[22]['PERC'] = "\""; //result cell
	$cella[3] = "\""; //total result cell
	for($c=1;$c<=$total_colspan;$c++) { $cella[3].="\",\""; }
	$cella[32]['OVERALL'] = "\"";	//ptd normal cell
	$cella[32]['NUM'] = "\""; //result cell
	$cella[32]['DOUBLE'] = "\""; //result cell
	$cella[32]['PERC'] = "\""; //result cell
	$rowa = "";
	$rowz = "\r\n";
	$pagea="\"".$cmpname."\"\r\n\"".$report_head."\"";
	$table[1] = "\r\n";
	$table[9] = "";
	$pagez = "\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	$decimal = ".";
	$thou = "";
	break;
case "excel":
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	if($c2>0) { $rowspan = 3; } else { $rowspan = 1; }
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[10] = "<td class=\"head\" colspan=$c2>"; //time heading cell
	$cellz[10] = "</td>"; //time heading cell
	$cella[19] = "<td class=\"head\" colspan=".count($mheadings[$r_section]).">"; //period-to-date heading cell
	$cellz[19] = "</td>"; //period-to-date heading cell
	$cella[12] = "<td class=\"head\" rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</td>"; //rowspan heading cell
	$cella[2] = "<td class=kpi>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=kpi style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22]['OVERALL'] = "<td style=\"text-align: right;background-color: #eeeeee\" class=kpi>";	//ptd normal cell
	$cella[22]['NUM'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[22]['DOUBLE'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[22]['PERC'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
	$cella[3] = "<td colspan=".($total_colspan+1)." class=kpi style=\"text-align: right; font-weight: bold; background-color: #eeeeee;\">"; //total result cell
	$cella[32]['NUM'] = "<td class=kpi style=\"text-align: right;font-weight: bold; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['DOUBLE'] = "<td class=kpi style=\"text-align: right;background-color: #eeeeee;\">"; //result cell total
	$cella[32]['PERC'] = "<td class=kpi style=\"text-align: right;background-color: #eeeeee;\">"; //result cell total
	$rowa = chr(10)."<tr>";
	$rowz = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>KPI Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>"
			.chr(10)." td { font-style: Calibri; font-size:11pt; } 
			.kpi { border-width: thin; border-color: #000000; border-style: solid; vertical-align: top; } 
			.kpi_r { border-width: thin; border-color: #000000; border-style: solid; text-align: center; color: #FFFFFF; vertical-align: top; } 
			.head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } "
			.chr(10)." .title { font-size:20pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:underline; text-align: center; } "
			.chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:none; text-align: center;} 
			.title3 { font-size:14pt; font-style: Calibri; color:#000099; font-weight:bold;} 
			.title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}
			</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$fc>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$fc>";
		$pagea.=$report_head;
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = " ";
	$decimal = ".";
	$thou = " ";
	break;
default:
	$gcell[1] = "<h3>";	//start of group
	$gcell[9] = "</h3>";	//end of group
	if($c2>0) { $rowspan = 3; } else { $rowspan = 1; }
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[10] = "<th colspan=$c2>"; //time heading cell
	$cellz[10] = "</th>"; //time heading cell
	$cella[19] = "<th colspan=".count($mheadings[$r_section]).">"; //time heading cell
	$cellz[19] = "</th>"; //time heading cell
	$cella[11] = "<th colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<th rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</th>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[22]['OVERALL'] = "<td style=\"background-color: #eeeeee\" class=right>";	//ptd normal cell
	$cella[22]['NUM'] = "<td class=right>"; //result cell
	$cella[22]['DOUBLE'] = "<td class=right>"; //result cell
	$cella[22]['PERC'] = "<td class=right>"; //result cell
	$cella[3] = "<td colspan=".($total_colspan+1)." class=right style=\"font-weight: bold; background-color: #eeeeee;\">"; //total result cell
	$cella[32]['NUM'] = "<td class=right style=\"font-weight: bold; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['DOUBLE'] = "<td class=right style=\"font-weight: bold; background-color: #eeeeee;\">"; //result cell total
	$cella[32]['PERC'] = "<td class=right style=\"font-weight: bold; background-color: #eeeeee;\">"; //result cell total
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"inc/main.css\" type=\"text/css\">";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 style=\"text-align: center;\">".$cmpname."</h1><h2 style=\"text-align: center;\">";
	$pagea.=$report_head;
	$pagea.= "</h2>";
	$table[1] = "<table width=100%>";
	$table[9] = "</table>";
	$pagez = "<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	$decimal = ".";
	$thou = ",";
	break;
}
$totals = array();
foreach($result_settings as $i => $r) { $totals[$i] = 0; }
//START PAGE
$echo = $pagea;
$echo.= $table[1];
//GROUP BY 
foreach($group as $i => $g) {
	$g_objects = $g['objects'];
	if(count($g_objects)>0) {
		$groupresults = array();
		if(strlen($g['value'])>0) {
			$echo.= $table[9].$gcell[1].$helperObj->decode($g['value']).$gcell[9].$table[1];
		}
		//Main heading
		$echo.= $rowa;
			$echo .= $cella[12]."Ref".$cellz[12];
			foreach($head as $fld => $h) {
				if(in_array($fld,$fields)) { $echo .= $cella[12].$helperObj->decode($h['h_client']).$cellz[12]; }
			}
			for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
				$echo.=$cella[10].$time[$t]['display_full'].$cellz[10];
			}
		$echo .= $rowz;
		//Time heading
		if($c2>0) {
			$echo.= $rowa;
			if($output=="csv") {
				for($c=0;$c<=count($head);$c++) $echo.=$cella[12].$cellz[12];
			}
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
					foreach($mheadings[$h_section] as $fld => $h) {
						if(in_array($fld,$fields)) { $echo.=$cella[19].$helperObj->decode($h['h_client']).$cellz[19]; }
					}
				}
			$echo .= $rowz;
			$echo.= $rowa;
			if($output=="csv") {
				for($c=0;$c<=count($head);$c++) $echo.=$cella[12].$cellz[12];
			}
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
					foreach($mheadings[$h_section] as $fldh => $hh) {
						foreach($mheadings[$r_section] as $fld => $h) {
							if(in_array($fldh,$fields)) { $echo.=$cella[1].$helperObj->decode($h['h_client']).$cellz[1]; }
						}
					}
				}
			$echo .= $rowz;
		}
		foreach($g_objects as $obj_id => $g_obj) {
			$obj = $objects[$obj_id];
			$echo .= $rowa.$cella[2].$id_labels_all[$section].$obj_id.$cellz[2];
			foreach($head as $fld => $h) {
				if(in_array($fld,$fields)) {
					$echo.=$cella[2];
					switch($h['h_type']) {
						case "WARDS":
						case "AREA":
						case "FUNDSRC":
							$echo.= $helperObj->decode(implode("; ",$object_wa[strtolower($h['h_table'])][$obj_id]));
							break;
						case "DATE":
							$echo.= $obj[$fld]>0 ? date("d M Y",$obj[$fld]) : "";
							break;
						case "LIST":
						default:
							$echo.= str_replace(chr(10),$newline,$helperObj->decode($obj[$fld]));
							break;
					}
					$echo.=$cellz[2];
				}
			}
			if($c2>0) { //results
				/** MONTHLY RESULTS **/
				$res = $results[$obj_id];
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
					foreach($mheadings[$h_section] as $fldh => $hh) {
					if(in_array($fldh,$fields)) { 
						if($section=="CF") { $fldh = $r_table_fld.$fldh; }
						foreach($mheadings[$r_section] as $fld => $h) {
							switch($h['h_type']) {
							case "PERC":
								$echo.=$cella[22][$h['h_type']].number_format($res[$t][$fldh.$fld],2,$decimal,$thou)."%".$cellz[2];
								break;
							case "DOUBLE":
							default:
								if(!isset($groupresults[$t][$fldh.$fld])) { $groupresults[$t][$fldh.$fld] = 0; } $groupresults[$t][$fldh.$fld]+=$res[$t][$fldh.$fld];
								$echo.=$cella[22][$h['h_type']].number_format($res[$t][$fldh.$fld],2,$decimal,$thou).$cellz[2];
								break;
							}
						}
					}}
				}
			}
			$echo.=$rowz;
		} //foreach g['objects']
		/* GROUP TOTAL */
		$echo.=$rowa.$cella[3]."Total:".$cellz[2];
			$res = $groupresults;
				for($t=$report_time[0];$t<=$report_time[1];$t+=$time_period_size) {
					foreach($mheadings[$h_section] as $fldh => $hh) {
					if(in_array($fldh,$fields)) { 
						if($section=="CF") { $fldh = $r_table_fld.$fldh; }
						foreach($mheadings[$r_section] as $fld => $h) {
							switch($h['h_type']) {
							case "PERC":
								if($section=="CF") {
									//$v = $fld;
									$f = substr($fld,1);
									$b = "_".($f-3);
									$a = "_".($f-2);
									$v = number_format( ($res[$t][$fldh.$a]/$res[$t][$fldh.$b])*100,2,$decimal,$thou)."%";
								} elseif($section="CAP") {
									$b = "target";
									$a = "actual";
									$v = number_format( ($res[$t][$fldh.$a]/$res[$t][$fldh.$b])*100,2,$decimal,$thou)."%";
								} else {
									$v = "";
								}
								$echo.=$cella[32][$h['h_type']].$v.$cellz[2];
								break;
							case "DOUBLE":
							default:
								if(!isset($groupresults[$t][$fldh.$fld])) { $groupresults[$t][$fldh.$fld] = 0; } $groupresults[$t][$fldh.$fld]+=$res[$t][$fldh.$fld];
								$echo.=$cella[32][$h['h_type']].number_format($res[$t][$fldh.$fld],2,$decimal,$thou).$cellz[2];
								break;
							}
						}
					}}
				}
	} //if count(g['objects']) > 0
} //foreach group
$echo .= $table[9];
$echo.= $pagez;

if($output == "csv") { $ext = ".csv"; $content = "text/plain"; } else { $ext = ".xls"; $content = "application/ms-excel"; }
switch($output) {
case "csv":
case "excel":
	$path = $report_save_path;
	$sys_filename = $modref."_".$section."_".date("Ymd_His").$ext;
    $helperObj->saveEcho($path,$sys_filename,$echo);
	$usr_filename = $section."_report_".date("Ymd_His",$today).$ext;
    $helperObj->downloadFile2($path, $sys_filename, $usr_filename, $content);
	break;
default:
	echo $echo;
	break;
}
?>