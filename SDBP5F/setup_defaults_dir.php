<?php 

//$page_title = "".$head_dir."s";
$page_title = "";
$log_section = "DIR";
include("inc/header.php"); 

$result = (isset($_REQUEST['r'])) ? $_REQUEST['r'] : array();

if(isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case "EDIT-DIR":
			//arrPrint($_REQUEST);
			$value = $helperObj->code($_REQUEST['dir_value']);
			$id = $_REQUEST['dir_id'];
			if($helperObj->checkIntRef($id)) {
				$sql = "SELECT value FROM ".$dbref."_dir WHERE id = ".$id;
				$row = $helperObj->mysql_fetch_all($sql);
				$r = $row[0];
				$sql = "UPDATE ".$dbref."_dir SET value = '".$value."' WHERE id = '".$id."'";
				$mar = $helperObj->db_update($sql);
				if($mar>0) { 
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>"Changed ".$head_dir." $id name to <i>".$value."</i> from <i>".$r['value']."</i>.",
											'old'=>$r['value'],
											'new'=>$value,
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					$result = array("ok","".$head_dir." updated.");
				} else {
					$result = array("info","No change found to be made.");
				}
			} else {
				$result = array("error","An error occurred.  Please try again.");
			}
			break;
		case "ADD-DIR":
			$value = $helperObj->code($_REQUEST['dir_value']);
			$subs = $_REQUEST['sub_value'];
			if(strlen($value)>0) {
				$sql = "SELECT * FROM ".$dbref."_dir WHERE active = true ORDER BY sort DESC";
				//$rs = getRS($sql);
				if($helperObj->db_get_num_rows($sql)>0) {
					$row = $helperObj->mysql_fetch_one($sql);
					$sort = $row['sort']+1;
				} else {
					$sort = 1;
				}
				$sql = "INSERT INTO ".$dbref."_dir (id, value, active, sort) VALUES (null,'$value',true,$sort)";
				$id = $helperObj->db_insert($sql);
				$res = array();
				if($helperObj->checkIntRef($id)) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>"Added new ".$head_dir." <i>".$value."</i>.",
											'old'=>"",
											'new'=>$value,
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					$res = "".$head_dir." '".$value."' ";
					$s = 1;
					foreach($subs as $svalue) {
						if(strlen($svalue)>0) {
							$svalue = $helperObj->code($svalue);
							$sort = $s;
							if($s>1) { $head = "false"; } else { $head = "true"; }
							$sql = "INSERT INTO ".$dbref."_subdir (id, value, active, sort, head, dirid) VALUES (null, '$svalue',true,$sort,$head,$id)";
							$sid = $helperObj->db_insert($sql);
							if($helperObj->checkIntRef($sid)) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$sid,
											'field'=>"subdir",
											'text'=>"Added new ".$head_sub." <i>".$svalue."</i> to ".$head_dir." ".$id.".",
											'old'=>"",
											'new'=>$svalue,
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
								$s++;
							}
						}
					}
					if($s>1) { $res.="and associated ".$head_sub."s have been added."; } else { $res.=" has been added."; }
					$result = array("ok",$res);
				} else {
					$result = array("error","An error occurred while trying to add ".$head_dir." '$value'.");
				}
			} else {
				$result = array("info","No ".$head_dir." name given.");
			}
			break;
		case "DELETE-DIR":
			$id = $_REQUEST['dir_id'];
			if($helperObj->checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_dir SET active = false WHERE id = ".$id;
				$mar = $helperObj->db_update($sql);
				if($mar>0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>"Deactivated ".$head_dir." <i>".$id."</i>.",
											'old'=>"1",
											'new'=>"0",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					$result = array("ok","".$head_dir." ".$id." has been deactivated.");
				} else {
					$result = array("error","An error occurred while trying to deactivate ".$head_dir." ".$id.".");
				}
			} else {
				$result = array("error","An error occurred while trying to deactivate ".$head_dir." ".$id.".");
			}
			break;
		case "RESTORE-DIR":
			$id = $_REQUEST['dir_id'];
			if($helperObj->checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_dir SET active = true WHERE id = ".$id;
				$mar = $helperObj->db_update($sql);
				if($mar>0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>"Restored ".$head_dir." <i>".$id."</i>.",
											'old'=>"0",
											'new'=>"1",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					$result = array("ok","".$head_dir." ".$id." has been restored.");
				} else {
					$result = array("error","An error occurred while trying to restore ".$head_dir." ".$id.".");
				}
			} else {
				$result = array("error","An error occurred while trying to restore ".$head_dir." ".$id.".");
			}
			break;
		case "EDIT-SUB":
			$value = $helperObj->code($_REQUEST['sub_value']);
			$id = $_REQUEST['sub_id'];
			$sub_head = $_REQUEST['sub_head'];
			$dir_id = $_REQUEST['dir_id'];
			$sub_dirid = isset($_REQUEST['sub_dirid']) ? $_REQUEST['sub_dirid'] : 0;
			$new_dir_id = $helperObj->checkIntRef($sub_dirid) ? $sub_dirid : $dir_id;
			if($helperObj->checkIntRef($id) && strlen($value)>0) {
				$sql = "SELECT value FROM ".$dbref."_subdir WHERE id = ".$id;
				$row = $helperObj->mysql_fetch_all($sql);
				$r = $row[0];
				if($sub_head=="true" && $r['head']!=true) {
					$are_there_changes = true;
					$sql = "SELECT * FROM ".$dbref."_subdir WHERE dirid = ".$new_dir_id." ORDER BY head DESC, sort DESC";
					//$rs = getRS($sql);
					$pri = $helperObj->mysql_fetch_one($sql);
					if($helperObj->db_get_num_rows($sql)>1) {	$rb = $helperObj->mysql_fetch_one($sql);	} else {	$rb = $pri;	}
					$sort_head = $rb['sort']+1;
					$sql = "UPDATE ".$dbref."_subdir SET head = false, sort = ".$sort_head." WHERE head = true AND dirid = ".$new_dir_id;
					$mar = $helperObj->db_update($sql);
					if($mar>0) {
						$v_log = array(
							'section'=>$log_section,
							'ref'=>$id,
							'field'=>"subdir",
							'text'=>"Removed ".$head_sub." ".$pri['id']." as Primary for ".$head_dir." ".$new_dir_id.".",
							'old'=>"true",
							'new'=>"false",
							'YN'=>"Y"
						);
						logChanges('SETUP',0,$v_log,$helperObj->code($sql));
						if($value != $r['value'] || $new_dir_id != $dir_id) {
							$log_text = "Set ".$head_sub." $id as Primary for ".$head_dir." ".$new_dir_id;
							$insert_data = array(); 
							if($value != $r['value']) {
								$insert_data[] = " value = '$value' ";
								$log_text.="<br />Changed the name to <i>".$value."</i> from <i>".$r['value']."</i>";
							}
							if($new_dir_id!=$dir_id) {
								$insert_data[] = " dirid = $new_dir_id ";
								$log_text.="<br />Changed the parent $head_dir to <i>".$new_dir_id."</i> from <i>".$dir_id."</i>";
							}
							$log_text.=".";
							$sql = "UPDATE ".$dbref."_subdir SET ".implode(",",$insert_data).", head = true, sort = 1 WHERE id = ".$id;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>$log_text,
											'old'=>$r['value'],
											'new'=>$value,
											'YN'=>"Y"
										);
						} else {
							$sql = "UPDATE ".$dbref."_subdir SET head = true, sort = 1 WHERE id = ".$id;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>"Set ".$head_sub." $id as Primary for ".$head_dir." ".$dir_id.".",
											'old'=>$r['value'],
											'new'=>$value,
											'YN'=>"Y"
										);
						}
					} else {
						$result = array("error","An error occurred while trying to change the Primary ".$head_sub.".");
						break;
					}
				} elseif($value!=$r['value'] || $new_dir_id!=$dir_id){
					$are_there_changes = true;
					/*$sql = "UPDATE ".$dbref."_subdir SET value = '".$value."' WHERE id = ".$id;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>"Changed ".$head_sub." $id name to <i>".$value."</i> from <i>".$r['value']."</i>.",
											'old'=>$r['value'],
											'new'=>$value,
											'YN'=>"Y"
										);*/
							$log_text = array();
							$insert_data = array(); 
							if($value != $r['value']) {
								$insert_data[] = " value = '$value' ";
								$log_text[]="Changed the name to <i>".$value."</i> from <i>".$r['value']."</i>";
							}
							if($new_dir_id!=$dir_id) {
								$insert_data[] = " dirid = $new_dir_id ";
								$log_text[]="Changed the parent $head_dir to <i>".$new_dir_id."</i> from <i>".$dir_id."</i>";
							}
							$sql = "UPDATE ".$dbref."_subdir SET ".implode(",",$insert_data)." WHERE id = ".$id;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"dir",
											'text'=>implode("<br />",$log_text).".",
											'old'=>$r['value'],
											'new'=>$value,
											'YN'=>"Y"
										);
																				
				} else {
					$are_there_changes = false;
				}
				if($are_there_changes) {
					$mar = $helperObj->db_update($sql);
					if($mar>0) { 
						logChanges('SETUP',0,$v_log,$helperObj->code($sql));
						$result = array("ok","".$head_sub." updated.");
					} else {
						$result = array("info","No change found to be made.");
					}
				} else {
					$result = array("info","No change found to be made.");
				}
			} else {
				$result = array("error","An error occurred.  Please try again.");
			}
			break;
		case "ADD-SUB":
			$value = $helperObj->code($_REQUEST['sub_value']);
			$dir_id = $_REQUEST['dir_id'];
			if(strlen($value)>0 && $helperObj->checkIntRef($dir_id)) {
				$sql = "SELECT * FROM ".$dbref."_subdir WHERE dirid = ".$dir_id." AND active = true ORDER BY sort DESC";
				//$rs = getRS($sql);
				if($helperObj->db_get_num_rows($sql)>0) {
					$row = $helperObj->mysql_fetch_one($sql);
					$head = "false";
					$sort = $row['sort']+1;
				} else {
					$sort = 1;
					$head = "true";
				}
				$sql = "INSERT INTO ".$dbref."_subdir (id, value, active, sort, head, dirid) VALUES (null,'".$value."',true,$sort,$head,".$dir_id.")";
				$id = $helperObj->db_insert($sql);
				if($helperObj->checkIntRef($id)) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"subdir",
											'text'=>"Added new ".$head_sub." <i>".$value."</i> to ".$head_dir." ".$dir_id.".",
											'old'=>"",
											'new'=>$value,
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					$result = array("ok","".$head_sub." '".$value."' has been added.");
				} else {
					$result = array("error","An error occurred while trying to add ".$head_sub." '".$value."'");
				}
			} else {
				$result = array("info","No ".$head_sub." name given.");
			}
			break;
		case "DELETE-SUB":	
			$id = $_REQUEST['sub_id'];
			if($helperObj->checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_subdir SET active = false WHERE id = ".$id;
				$mar = $helperObj->db_update($sql);
				if($mar>0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"subdir",
											'text'=>"Deactivated ".$head_sub." <i>".$id."</i>.",
											'old'=>"1",
											'new'=>"0",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					$result = array("ok","".$head_sub." ".$id." has been deactivated.");
				} else {
					$result = array("error","An error occurred while trying to deactivate ".$head_sub." ".$id.".");
				}
			} else {
				$result = array("error","An error occurred while trying to deactivate ".$head_sub." ".$id.".");
			}
			break;
		case "RESTORE-SUB":	
			$id = $_REQUEST['sub_id'];
			if($helperObj->checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_subdir SET active = true WHERE id = ".$id;
				$mar = $helperObj->db_update($sql);
				if($mar>0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"subdir",
											'text'=>"Restored ".$head_sub." <i>".$id."</i>.",
											'old'=>"0",
											'new'=>"1",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					$result = array("ok","".$head_sub." ".$id." has been restored.");
				} else {
					$result = array("error","An error occurred while trying to restore ".$head_sub." ".$id.".");
				}
			} else {
				$result = array("error","An error occurred while trying to restore ".$head_sub." ".$id.".");
			}
			break;
	}
}

$helperObj->displayResult($result);

?>
<p><input type=button value=Add class=dir-add /> <input type=button value=Sort id=dir-sort /></p>
<?php
$sql = "SELECT * FROM ".$dbref."_dir ORDER BY sort";
$dir = $helperObj->mysql_fetch_all($sql);
$subs = $helperObj->mysql_fetch_all_fld2("SELECT * FROM ".$dbref."_subdir ORDER BY dirid, sort","dirid","id");
?>
<table width=700>
	<tr>
		<th>Ref</th>
		<th><?php echo $head_dir ?></th>
		<th><?php echo $head_sub ?></th>
		<th>&nbsp;</th>
	<tr>
<?php
	foreach($dir as $d) {
		echo "<tr>";
			echo "<th ".(!$d['active'] ? "class=inact" : "").">".$d['id']."</th>";
			echo "<td ".(!$d['active'] ? "class=inact" : "")."><label id=dir-".$d['id']."-value>".$helperObj->decode($d['value'])."</label></td>";
			echo "<td style=\"padding: 0 0 0 0;\"><table width=100% class=noborder style=\"background-color: #FFFFFF;\">";
			foreach($subs[$d['id']] as $s) {
				echo "
				<tr id=hover2>
					<th class=\"th2 noborder ".(!$s['active'] || !$d['active'] ? "inact" : "")."\" width=30>".$s['id']."</th>
					<td class='noborder ".(!$s['active'] || !$d['active'] ? "inact" : "")."'>".($s['head'] ? "<i>" : "")."<label id=sub-".$s['id']."-value>".decode($s['value'])."</label></i></td>
					<td class=\"right noborder ".(!$s['active'] || !$d['active'] ? "inact" : "")."\">";
				if($s['active'] && $d['active']) {
					if($s['head']) {
						echo "<input type=button value=Edit class=sub-edit-head id=".$s['id']." name=".$d['id']." />";
					} else {
						echo "<input type=button value=Edit class=sub-edit id=".$s['id']." name=".$d['id']." />";
					}
				} elseif(!$s['active']) {
					echo "<input type=button value=Restore class=irestore id=".$s['id']." typ=SUB />";
				}
			}
			if($d['active']) { echo "<tr id=hover2><th class=\"th2\" width=30></th><td colspan=2 class=noborder><input type=button value=Add class=sub-add name=".$d['id']." /> <input type=button value=Sort class=sub-sort name=\"".$d['id']."\" /></td></tr>"; }
			echo "</table></td>"; 
			echo "<td ".(!$d['active'] ? "class=inact" : "").">";
				if($d['active']) {
					echo "<input type=button value=Edit class=dir-edit id=".$d['id']." />";
				} else {
					echo "<input type=button value=Restore class=irestore id=".$d['id']." typ=DIR />";
				}
			echo "</td>";
		echo "</tr>";
	}
?>
</table>
<div id=dialog-dir-edit title="Edit <?php echo $head_dir; ?>"><form id=form-dir-edit action=<?php echo $self; ?> method=post><input type=hidden name=action value=EDIT-DIR />
	<table width=100%>
		<tr>
			<th class=left>Reference:</th>
			<td><input type=hidden name=dir_id value="" id=dir-edit-id><label id=dir-ref for=dir-edit-id></label></td>
		</tr>
		<tr>
			<th class=left>Old Value:</th>
			<td><label for=dir-edit-id id=dir-edit-old></label></td>
		</tr>
		<tr>
			<th class=left>New Values:</th>
			<td><input type=text value="" size=50 id=dir-edit-value name=dir_value /></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td><input type=submit value="Save Changes" class=isubmit /><span class=float><input type=button value="Deactivate" class=idelete id=DIR /></span></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=cancel-dir-edit /></p>
</form></div>
<div id=dialog-dir-add title="Add <?php echo $head_dir; ?>"><form id=form-dir-add action=<?php echo $self; ?> method=post><input type=hidden name=action value=ADD-DIR />
	<table width=100%>
		<tr>
			<th class=left><?php echo $head_dir; ?>:</th>
			<td><input type=text value="" size=50 id=dir-add-value name=dir_value /></td>
		</tr>
		<tr>
			<th class=left rowspan=5 valign=top><?php echo $head_sub; ?>:</th>
			<td><input type=text value="" size=50 name=sub_value[] id=s1 /> <small><i>(Primary)</i></small></td>
		</tr>
		<tr>
			<td><input type=text value="" size=50 name=sub_value[] id=s2 /></td>
		</tr>
		<tr>
			<td><input type=text value="" size=50 name=sub_value[] id=s3 /></td>
		</tr>
		<tr>
			<td><input type=text value="" size=50 name=sub_value[] id=s4 /></td>
		</tr>
		<tr>
			<td><input type=text value="" size=50 name=sub_value[] id=s5 /></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td><input type=button value="Save Changes" class=isubmit id=submit-dir-add /></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=cancel-dir-add /></p>
</form></div>




<div id=dialog-sub-edit title="Edit <?php echo $head_sub; ?>">
	<form id=form-sub-edit action=<?php echo $self; ?> method=post>
		<input type=hidden name=action value=EDIT-SUB />
	<table width=100%>
		<tr>
			<th class=left>Reference:</th>
			<td>
				<label id=sub-ref for=sub-edit-id></label>
				<input type=hidden name=sub_id value="" id=sub-edit-id>
				<input type=hidden name=dir_id value="" id=sub-dir-id>
			</td>
		</tr>
		<tr>
			<th class=left>Old <?php echo $head_dir; ?>:</th>
			<td><label for=sub-edit-parent id=sub-edit-parent-old></label></td>
		</tr>
		<tr>
			<th class=left>New <?php echo $head_dir; ?>:</th>
			<td><select id=sub-edit-parent name=sub_dirid><?php
				foreach($dir as $d) {
					echo "<option value=".$d['id'].">".$d['value']."</option>";
				}
				?></select></td>
		</tr>
		<tr>
			<th class=left>Old Value:</th>
			<td><label for=sub-edit-id id=sub-edit-old></label></td>
		</tr>
		<tr>
			<th class=left>New Values:</th>
			<td><input type=text value="" size=50 id=sub-edit-value name=sub_value /></td>
		</tr>
		<tr>
			<th class=left>Primary:</th>
			<td><select id=sub-edit-head name=sub_head><option selected value=false>No</option><option value=true>Yes</option></select><label id=sub-edit-head-display></label></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td><input type=submit value="Save Changes" class=isubmit /><span class=float><input type=button value="Deactivate" id=SUB class=idelete  /></span></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=cancel-sub-edit /></p>
</form></div>
<div id=dialog-sub-add title="Add <?php echo $head_sub; ?>"><form id=form-sub-add action=<?php echo $self; ?> method=post><input type=hidden name=action value=ADD-SUB />
	<table width=100%>
		<tr>
			<th class=left><?php echo $head_dir; ?>:</th>
			<td><input type=hidden name=dir_id value="" id=sub-add-id><label id=sub-add-dir></label></td>
		</tr>
		<tr>
			<th class=left><?php echo $head_sub; ?>:</th>
			<td><input type=text value="" size=50 id=sub-add-value name=sub_value /></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td><input type=submit value="Save Changes" class=isubmit /></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=cancel-sub-add /></p>
</form></div>




<script type=text/javascript>
$(function() {
	
	var dir_list = [];
	<?php
	foreach($dir as $d) {
		echo "
		dir_list[".$d['id']."] = '".$d['value']."'";
	}
	?>
	
	$("#dialog-dir-edit").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$("#dialog-dir-add").dialog({
		autoOpen: false, 
		width: 550, 
		height: "auto",
		modal: true
	});
	$("#dialog-sub-edit").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$("#dialog-sub-add").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$("table th.inact").each(function() {
		if($(this).hasClass("th2")) 
			$(this).css({"background-color":"#ababab"});
		else
			$(this).css({"background-color":"#555555","color":"#FFFFFF"});
	});
	$(".dir-add").click(function() {
		$("#dialog-dir-add").dialog("open");
	});
	$("#submit-dir-add").click(function() {
		var l = 0;
		var s = 0;
		var t = "";
		t = "#dir-add-value";
		var v = "";
		v = $(t).val();
		if(v.length==0) {
			alert("Please enter a <?php echo $head_dir; ?> name.");
		} else {
			for(s=1;s<=5;s++) {
				t = "#s"+s;
				v = $(t).val();
				l+= v.length;
			}
			if(l==0) {
				alert("Please enter at least one <?php echo $head_sub; ?> to be used as the Primary.");
			} else {
				$("#form-dir-add").submit();
			}
		}
	});
	$(".dir-edit").click(function() {
		var id = $(this).prop("id");
		var t = "#dir-"+id+"-value";
		var v = $(t).html();
		$("#dir-edit-id").val(id);
		$("#dir-ref").html(id);
		$("#dir-edit-value").val(v);
		$("#dir-edit-value").focus();
		$("#dir-edit-old").html(v);
		$("#dialog-dir-edit").dialog("open");
	});
	$(".sub-add").click(function() {
		var id = $(this).prop("name");
		var t = "#dir-"+id+"-value";
		var v = $(t).html();
		$("#sub-add-id").val(id);
		$("#sub-add-dir").html(v);
		$("#sub-add-value").val("");
		$("#sub-add-value").focus();
		$("#dialog-sub-add").dialog("open");
	});
	$(".sub-edit").click(function() {
		var id = $(this).prop("id");
		var n = $(this).prop("name");
		var t = "#sub-"+id+"-value";
		var v = $(t).html();
		$("#sub-dir-id").val(n);
		$("#sub-edit-id").val(id);
		$("#sub-ref").html(id);
		$("#sub-edit-value").val(AssistString.decode(v));
		$("#sub-edit-old").html(v);
		$("#sub-edit-parent-old").html(dir_list[n]);
		$("#sub-edit-parent").prop("disabled",false).val(n);
		$("#SUB").prop("disabled",false);
		$("#SUB").addClass("idelete");
		$("#sub-edit-head-display").html("");
		$("#sub-edit-head option").remove();
		$("#sub-edit-head").append("<option selected value=false>No</option><option value=true>Yes</option>");
		$("#sub-edit-head").show();
		$("#sub-edit-value").focus();
		$("#dialog-sub-edit").dialog("open");
	});
	$(".sub-edit-head").click(function() {
		var id = $(this).prop("id");
		var t = "#sub-"+id+"-value";
		var v = $(t).html();
		var n = $(this).prop("name");
		$("#sub-dir-id").val(n);
		$("#sub-edit-id").val(id);
		$("#sub-ref").html(id);
		$("#sub-edit-value").val(AssistString.decode(v));
		$("#sub-edit-old").html(v);
		$("#sub-edit-parent-old").html(dir_list[n]);
		$("#sub-edit-parent").val(n).prop("disabled",true);
		$("#SUB").removeClass("idelete");
		$("#SUB").prop("disabled",true);
		$("#sub-edit-head-display").html("Yes");
		$("#sub-edit-head option").remove();
		$("#sub-edit-head").append("<option selected value=keep>Keep</option>");
		$("#sub-edit-head").hide();
		$("#dialog-sub-edit").dialog("open");
	});
	$("#cancel-dir-edit").click(function() {
		$("#dialog-dir-edit").dialog("close");
	});
	$("#cancel-dir-add").click(function() {
		$("#dialog-dir-add").dialog("close");
	});
	$("#cancel-sub-edit").click(function() {
		$("#dialog-sub-edit").dialog("close");
	});
	$("#cancel-sub-add").click(function() {
		$("#dialog-sub-add").dialog("close");
	});
	$(".idelete").click(function() {
		var typ = $(this).prop("id");
		switch(typ) {
			case "DIR":
				if(confirm("Are you sure you wish to deactivate this <?php echo $head_dir; ?>?")) {
					var id = $("#dir-edit-id").val();
					document.location.href = "<?php echo $self; ?>?dir_id="+id+"&action=DELETE-DIR";
				}
				break;
			case "SUB":
				if(confirm("Are you sure you wish to deactivate this <?php echo $head_sub; ?>?")) {
					var id = $("#sub-edit-id").val();
					document.location.href = "<?php echo $self; ?>?sub_id="+id+"&action=DELETE-SUB";
				}
				break;
		}
	});
	$("input:button.irestore").click(function() {
		var typ = $(this).attr("typ");
		var id = $(this).prop("id");
		switch(typ) {
			case "DIR":
				if(confirm("Are you sure you wish to restore this <?php echo $head_dir; ?>?")) {
					document.location.href = "<?php echo $self; ?>?dir_id="+id+"&action=RESTORE-DIR";
				}
				break;
			case "SUB":
				if(confirm("Are you sure you wish to restore this <?php echo $head_sub; ?>?")) {
					document.location.href = "<?php echo $self; ?>?sub_id="+id+"&action=RESTORE-SUB";
				}
				break;
			default: alert("An error has occurred."); break;
		}
	});
	$("#dir-sort").click(function() {
		document.location.href = "setup_defaults_dir_sort.php?type=D";
	});
	$(".sub-sort").click(function() {
		var id = $(this).prop("name");
		document.location.href = "setup_defaults_dir_sort.php?type=S&i="+id;
	});
});
</script>
<?php
$helperObj->getGoBack("setup.php","Back to Setup");
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>
</body>
</html>