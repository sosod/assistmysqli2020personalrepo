<?php 
include("inc/header.php"); 

$default_lists = array();
$sql = "SELECT h_client, h_table, h_length FROM ".$dbref."_setup_headings WHERE h_active = 1 AND h_type IN ('".implode("','",$list_types)."') AND h_table NOT IN ('dir','subdir')";
$client_headings = mysql_fetch_all($sql);
foreach($client_headings as $chead) {
	$default_lists[$chead['h_table']] = array('name'=>$chead['h_client']);
}

foreach($default_lists as $tbl => $list) {
	echo "<h3>".($tbl=="idp_top" ? "Top Layer: " : "").($tbl=="idp_kpi" ? "Departmental: " : "").$list['name']."</h3>
	<table>
		<tr>
			<th>Ref</th>
			<th>List Item</th>
			".(in_array($tbl,$code_lists) ? "<th>Code</th>" : "")."
		</tr>
		";
		$rs = getRS("SELECT * FROM ".$dbref."_list_".$tbl." WHERE active = true ORDER BY sort, value");
		while($row = mysql_fetch_assoc($rs)) {
			echo "
			<tr>
				<th>".$row['id']."</th>
				<td>".$row['value']."&nbsp;&nbsp;</td>
				".(in_array($tbl,$code_lists) ? "<td class=centre>".$row['code']."</td>" : "")."
			</tr>
			";
		}
		?>
	</table>
	<?php
}



goBack("support_import.php","");
?>

</body>
</html>