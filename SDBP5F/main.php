<?php 
include("inc/header.php"); 
/*
<div align=center>
<table>
	<tr height=30px>
		<th colspan=2>Development Progress</th>
	</tr>
	<tr>
		<td>4 July </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Capital Projects & Monthly Cashflow -> Updated columns to differentiate between Monthly, YTD & Total figures.
	</tr>
	<tr>
		<td>20 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Defaults > Administrators
	</tr>
	<tr>
		<td>19 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup - Logging of all activity
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Defaults > Time Periods (finalised)
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Support > Time Periods 
	</tr>
	<tr>
		<td>15 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup  > Defaults > Directorates/Sub-Directorates
	</tr>
	<tr>
		<td>13 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup  > User Access
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Manage - user access limitations
	</tr>
	<tr>
		<td>11 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Manage > Update > Departmental SDBIP
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Manage > Update > Top Layer SDBIP (started)
	</tr>
	<tr>
		<td>9 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Freeze frames
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Limited / All column option
	</tr>
	<tr>
		<td>8 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Capital Projects
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Monthly Cashflow
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Revenue By Source
	</tr>
	<tr>
		<td>7 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Departmental SDBIP
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Top Layer SDBIP
	</tr>
	<tr>
		<td>5 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Support > Import - Added function to keep a log of imports
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Support > Import - Fixed bugs with import
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> SDBIP 2011/2012 Create function loaded onto IGN0001
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Updated Live site.
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Updated Dev site.
	</tr>
	<tr>
		<td>2 June </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Glossary
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Terminology - updated to allow for client specific display order, required fields, display columns in list view and glossary text.
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Departmental SDBIP [List view] - basic structure created (results, filtering & paging to follow).
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Top Layer [List view] - basic structure created (results, filtering & paging to follow).
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> View > Capital Projects [List view] - basic structure created (financials, filtering & paging to follow).
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Support > Import > Capital Projects - corrected problem with GFS column.
	</tr>
	<tr>
		<td>31 May </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Support > Import
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Terminology (rename Headings)
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Lists
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Module Defaults
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Heading structure created.</td>
	</tr>
	<tr>
		<td>7 May </td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Defaults > Time Periods 
		<br /><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Setup > Defaults > Time Periods > Update All Automatic Dates
	</tr>
	<tr>
		<td>6 May 15h00</td>
		<td class=left><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> Base structure created.</td>
	</tr>
</table>
</div>*/
?>
</body>
</html>