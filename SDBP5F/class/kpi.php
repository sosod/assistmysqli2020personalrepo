<?php
@ini_set(default_charset, "");
class KPI extends SDBP5B_HELPER {

	protected $sdbp5;
	protected $section_details;

	protected $table_name;
	protected $results_table_name;
	protected $results_id_name;

	protected $assurance_table_name;
	protected $assurance_id_name;
	protected $assurance_status_codes;

	protected $kpi_update_status;

	const DELETED = 0;
	const ACTIVE = 1;

	const ASSURANCE_ACCEPT = 2;
	const ASSURANCE_REJECT = 4;
	const ASSURANCE_UPDATED = 8;

	private $head;
	private $rhead;

	private $page_section;

	public function __construct($h,$rh) {
		parent::__construct();
		$this->sdbp5 = new SDBP5();
		$this->section_details = $this->sdbp5->getSection($this->section);
		$this->setAttachmentDownloadOptions("section=".$this->section);
		$this->setAttachmentDeleteOptions("section=".$this->section."&results_fld=".$this->getResultsFieldPrefix());
		$this->setHelperAttachmentFolder($this->getAttachFolder());
		$this->table_name = $this->getDBRef()."_".$this->table;
		$this->results_table_name = $this->getDBRef()."_".$this->table."_results";
		$this->results_id_name = $this->results_table_field.$this->id_field."id";
		if($this->section!="TOP") {
			$this->assurance_table_name = $this->getDBRef()."_".$this->table."_assurance";
			$this->assurance_id_name = $this->assurance_table_field.$this->id_field."id";
		}
		$this->head = $h;
		$this->rhead = $rh;
		$this->kpi_update_status = array(
			0=>"New",
			1=>"Complete",
			2=>"In Progress",
		);
		$this->assurance_status_codes = array(
			'all'		=> array(
				'name' => "All Assurance Statuses",
				'description' => "All KPIs irrespective of assurance status.",
				'code' => "all",
				'icon' => "",
				'status'=> "",
			),
			'ok'		=> array(
				'name' => "Signed-off",
				'description' => "All KPIs which have been reviewed and signed off.",
				'code' => "ok",
				'icon' => "Y",
			),
			'no_all'	=> array(
				'name' => "Rejected (All)",
				'description' => "All KPIs which have been reviewed and returned to the user.",
				'code' => "no_all",
				'icon' => "N",
			),
			'no_no'		=> array(
				'name' => "Rejected (No response)",
				'description' => "All KPIs which have been reviewed and returned to the user where the user has not yet responded.",
				'code' => "no_no",
				'icon' => "N",
			),
			'no_yes'	=> array(
				'name' => "Rejected (User responded)",
				'description' => "All KPIs which have been reviewed and returned to the user where the user has responded.",
				'code' => "no_yes",
				'icon' => "warn",
			),
			'na_all'	=> array(
				'name' => "Not Reviewed (All)",
				'description' => "All KPIs which have not yet been reviewed.",
				'code' => "na_all",
				'icon' => "info",
			),
			'na_do'		=> array(
				'name' => "Not Reviewed (Applicable)",
				'description' => "All KPIs which have not yet been reviewed but where an update is required.",
				'code' => "na_do",
				'icon' => "info",
			),
			'na_ignore'	=> array(
				'name' => "Not Reviewed (Not Applicable)",
				'description' => "All KPIs which have not yet been reviewed which do not require an update.",
				'code' => "na_ignore",
				'icon' => "info",
			),
		);
	}

	/*******************************
	       GET FUNCTIONS
	*******************************/
	public function getAttachFolder($assurance=false) {
		if($assurance) {
			return $this->getModRef()."/".$this->assurance_section;
		} else {
			return $this->getModRef()."/".$this->section;
		}
	}
	public function getHeadingCodes() {				return array($this->head,$this->rhead);	}
	public function getRHeadCode() { 				return $this->rhead; }
	public function getHeadCode() { 				return $this->head; }
	public function getSection() {					return $this->section;	}
	public function getAssuranceSection() {			return $this->assurance_section;	}
	public function getSectionHead() {				return $this->section_head;	}
	public function getTimeIDs() {					return $this->time_ids;	}
	public function getFirstMonth() {				return $this->first_month;	}
	public function getTimeIncrement() {			return $this->time_increment;	}
	public function getAbsoluteRequiredFields() {	return $this->absolute_required_fields; }
	//field names
	public function getTableName() { 				return $this->table_name; }
	public function getTable() {	 				return $this->table; }
	public function getValueField() { 	 			return $this->table_field."value"; }
	public function getUnitField() { 	 			return $this->table_field."unit"; }
	public function getPOEField() { 	 			return $this->table_field."poe"; }
	public function getCTField() { 		 			return $this->table_field."calctype"; }
	public function getRPerfField() { 	 			return $this->results_table_field."perf"; }
	public function getRCorrectField() { 			return $this->results_table_field."correct"; }
	public function getRAttachField() {  			return $this->results_table_field."attachment"; }
	public function getRTargetField() {  			return $this->results_table_field."target"; }
	public function getRActualField() {  			return $this->results_table_field."actual"; }
	public function getFieldPrefix() {		 		return $this->table_field; }
	public function getIDFieldName() {				return $this->id_field; }
	public function getResultsFieldPrefix() {		return $this->results_table_field; }
	public function getWardsAreaTableDetails() { 	return $this->wa_table_field; }
	public function isManage() {					return ($this->page_section=="MANAGE"); }
	public function isAdmin() {						return ($this->page_section=="ADMIN"); }
	public function isTop() {						return ($this->section=="TOP"); }
	public function getAssuranceStatusCodes() {		return $this->assurance_status_codes; }

	public function getKPIUpdateStatusName($u)	{ 	return isset($this->kpi_update_status[$u]) ? $this->kpi_update_status[$u] : $this->kpi_update_status[0]; }

	//kpi
	public function getObject($i) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_".$this->table." WHERE ".$this->table_field."id = $i";
		return $this->mysql_fetch_one($sql);
	}

	//attachment
	function getAttachmentDetails($kpi_id,$time_id,$attach_id) {
		$record = $this->getUpdateRecord($kpi_id,$time_id);
		if($this->section!="KAS") {
			$attachments = $record['attachment']['attach'];
		} else {
			$attachments = $record['attachment'];
		}
		if(isset($attachments[$attach_id]) || isset($attachments[$kpi_id."_".$time_id."_".$attach_id])) {
			//echo "<h3>Attachment found!</h3>";
			if(!isset($attachments[$attach_id])) {
				$attach_id = $kpi_id."_".$time_id."_".$attach_id;
			}
			$a = $attachments[$attach_id];
			$a[0] = true;
			$a[2] = $record;
			return $a;
		} else {
			return array(0=>false,1=>$attachments,2=>$record);
		}
	}




	public function getUpdateRecord($i,$t) {
		$data = array();
		$sql = "SELECT * FROM ".$this->results_table_name." WHERE ".$this->results_id_name." = $i AND ".$this->results_table_field."timeid = $t ";
		$data = $this->mysql_fetch_one($sql);
		$p = $data[$this->results_table_field.'attachment'];

		$comm = $data[$this->results_table_field.'perf'];
		$comm = $this->stripSpecialCharacters($comm);
		$data[$this->results_table_field.'perf'] = $comm;
		$comm = $data[$this->results_table_field.'correct'];
		$comm = $this->stripSpecialCharacters($comm);
		$data[$this->results_table_field.'correct'] = $comm;

/*		if(strlen($p)>0) {
			$p = unserialize($p);
		} else {
			$p = array('poe'=>"",'attach'=>array());
		}*/
		$p = $this->processPOEserializedArray($p);
		$data['attachment'] = $p;
		return $data;
	}



	public function getAssociatesKPIs($i,$field) {
		$sql = "SELECT ".$this->table_field."id as id FROM ".$this->table_name." WHERE ".$this->table_field.$field." = ".$i." AND ".$this->table_field."active & ".self::ACTIVE." = ".self::ACTIVE;
		$keys = $this->mysql_fetch_all_by_value($sql,"id");
		return $keys;
	}






	public function getAssuranceRecord($i,$t) {
		$data = array();
		if($this->section!="TOP") {

			$sql = "SELECT * FROM ".$this->assurance_table_name." WHERE ".$this->assurance_id_name." = $i AND ".$this->assurance_table_field."timeid = $t ";
			$data = $this->mysql_fetch_one($sql);
			if(isset($data['kas_id'])) {
				$p = $data[$this->assurance_table_field.'attachment'];
	/*			if(strlen($p)>0) {
					$p = unserialize($p);
				} else {
					$p = array();
				}*/
				$p = $this->processPOEserializedArray($p);
				$data['attachment'] = $p;
			} else {
				$data = array(
					'kas_id'=>false,
					'kas_kpiid'=>$i,
					'kas_timeid'=>$t,
					'kas_response'=>"",
					'kas_result'=>"0",
					'kas_deadline'=>"0",
					'kas_attachment'=>array(),
					'kas_status'=>"0",
					'kas_user'=>"",
					'kas_date'=>"0",
				);
			}
		}
		return $data;
	}








	/*****************************
	SET FUNCTIONS
	*****************************/
	public function setAsManage() {
		$this->page_section = "MANAGE";
	}
	public function setAsAdmin() {
		$this->page_section = "ADMIN";
	}
	public function setPageSection($m) {
		$this->page_section = strtoupper($m);
	}










/*********************************
 * Function to hack bad serialisation of ' " : ; when storing POE & update attachments
 */
	public function processPOEserializedArray($w) {
		/***************************************
		* START OF HACK TO GET AROUND bug IN serialize FUNCTION RELATING TO " ' : ; CHARACTERS IN STRINGS
		****************************************/
		$return = array('poe'=>"",'attach'=>array());
		if(strlen($w)>0) {
				//replace double quotes " with a double pipe place holder
			$x = str_replace("&quot;","||",$w);
				//restore HTML tags
			$x = $this->decode($x);

				//break up the serialized array based on the semi-colon
			$y = explode(";",$x);
				//temp store the POE string portion
			$z = $y[1];
				//replace POE string portion with a blank string
			$y[1] = 's:0:""';
				//put serialized array back together
			$a = implode(";",$y);
				//break up the POE temp string on the serialized " placeholders
			$x = explode('"',$z);
				//replace the double pipe with the original double quotes character
			$y = str_replace('||','"',$x[1]);
		//	$y = $this->code($y);
				//unserialize the serialized array with blank POE string place holder
			$return = unserialize($a);
				//replace the blank temp POE string with the original
			$return['poe'] = $y;
		}
		/***************************************
		* END OF serialize HACK
		****************************************/
		$return['poe'] = $this->stripSpecialCharacters($return['poe']);

		return $return;
	}





	public function getUsersWithAssuranceNotificationRights($type,$id) {
		$sql = "SELECT DISTINCT tkid
				FROM ".$this->getDBRef()."_user_admins
				WHERE act_assuranceemails = true AND
				type = '$type' AND
				ref = '$id' AND
				active = true
				";
		$user_ids = $this->mysql_fetch_all_by_value($sql,"tkid");
		//$extras = array($sql,$user_ids);
		if(is_array($user_ids) && count($user_ids)>0) {
			$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkemail as email FROM
					assist_".$this->getCmpCode()."_timekeep
					INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
					WHERE tkid IN ('".implode("','",$user_ids)."')
					AND tkstatus = 1
					AND tkemail <> ''
					ORDER BY tkname, tksurname
					";
					//$extras[] = $sql;
			$rows = $this->mysql_fetch_all_by_id($sql, "id");
			//$rows['sql']=$extras;
			return $rows;
		} else {
			return array();
		}
	}
















}


?>