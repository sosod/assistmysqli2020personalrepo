<?php
$width = $_SESSION['screen']['width'];
if($width*.75<700) {
	$width = 700;
} else {
	$width*=0.75;
}


function maxlenOption($v) {
	global $select_option_maxlen;
	if(strlen($v)>$select_option_maxlen) {
		$v = substr($v,0,($select_option_maxlen-3))."...";
	}
	return $v;
}

//arrPrint($object);

$my_head = $mheadings[$section];
$result_head = $mheadings[$r_section];
if($section=="KPI" || $section == "TOP") {
	$obj_tt = $object['tt'];
	$obj_ct = $object['ct'];
}

ASSIST_HELPER::displayResult(array("info","** indicates required fields.")); 


?>
<form id=frm_edit method=post action=<?php echo $self; ?>>
<input type=hidden name=page_id value="<?php echo $page_id; ?>" />
<input type=hidden name=act value="SAVE" />
<table class=noborder>
	<tr class=no-highlight>
		<td class=noborder>
<h2>Details</h2>
<table id=tbl width=<?php echo $width; ?>>
	<tr>
		<th class=left>Reference</th>
		<td><?php echo (isset($id_label)?$id_label:"").$obj_id; ?><input type=hidden name=id id=obj_id value=<?php echo $obj_id; ?> /></td>
	</tr>
<?php 
foreach($my_head as $fld => $h) {
	echo chr(10)."<tr>";
		echo "<th class=\"left top\">".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;".($h['i_required']+$h['c_required']>0 ? "**<input type=hidden id=".$fld."-r value=Y />" : "<input type=hidden id=".$fld."-r value=N />")."</th>";
	switch($h['h_type']) {
		case "CAP":
			echo "<td>";
				if(isset($object[$fld]) && $object[$fld]>0) {
					echo "<label for=".$fld." id=".$fld."_disp>".$object[$fld.'_disp']."</label><input type=hidden name=".$fld." id=".$fld." value=".$object[$fld]." /><span class=float><input type=button value=\"Remove Link\" class=idelete id=del_cap />";
				} else {
					echo "<select name=$fld id=$fld>";
						echo "<option ".(!isset($object[$fld]) || $object[$fld]==0 ? "selected" : "")." value=0>N/A</option>";
						$other_sql = "SELECT cap_id as id, cap_name as value FROM ".$dbref."_capital WHERE cap_active = true AND cap_name <> '' AND cap_id NOT IN (SELECT kpi_capitalid FROM ".$dbref."_kpi WHERE kpi_capitalid > 0 AND kpi_active = true)";
						$others = mysql_fetch_alls($other_sql,"id");
						foreach($others as $i => $o) {
							$v = maxlenOption(decode($o['value']));
							echo "<option ".($object[$fld]==$i ? "selected" : "")." value=".$i.">[".$id_labels_all[$h['h_type']].$i."] ".$v."</option>";
						}
					echo "</select>";
				}
			echo "</td>";
			break;
		case "TOP":
			echo "<td>";
				echo $object[$fld];
			echo "</td>";
			break;
		case "WARDS":
		case "AREA":
		case "TEXTLIST":
		case "FUNDSRC":
			$val = array();
			if($h['h_type']=="TEXTLIST") {
			//echo $object[$fld];
				$values = array();
				$v = explode(";",$object[$fld]);
				foreach($v as $vb) {
					if(strlen($vb)>0) {
						$values[$vb] = $vb;
					}
				}
				
			} else {
				$values = $extras[$fld];
				//foreach($values as $v) {
					//$val[] = ((strlen($v['code'])>0 && strtolower($v['value'])!="all") ? decode($v['code']).": " : "").maxlenOption(decode($v['value']));
				//}
			}
			echo "<td>";
			//arrPrint($values);
			//arrPrint($lists[$h['h_table']]);
				//echo implode("<br />",$val);
			if(count($lists[$h['h_table']])>1) {
				$lt = $lists[$h['h_table']];
				echo "
				<select name=".$fld."[] id=$fld multiple ".(count($lists[$h['h_table']])<=10 ? "size=".count($lists[$h['h_table']]) : "size=10").">
						";
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<option ".(isset($values[$i]) ? "selected " : "")." value=$i full=\"".addslashes((isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").(decode($l['value'])))."\">".(isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").maxlenOption(decode($l['value']))."</option>";
					}
				echo "</select>";
				echo "<br /><span class=\"ctrlclick iinform\">CTRL + Click to select multiple options</span>
				<div id=div_".$fld." class='display_me ui-state-ok' style='width: 500px;margin: 5px;'></div>";
			} else {
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<input type=hidden name=".$fld."[] id=$fld value=$i>".(isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").(decode($l['value']))."";
					}
			}
			echo "</td>";
			break;
		case "DATE":
			echo "<td>".($object[$fld]>0 ? date("d M Y",$object[$fld]) : "")."</td>";
			break;
		case "LIST":
			echo "<td>";
			echo "<input type=hidden id=lbl-".$fld."-v value=\"".$object['o'.$fld]."\" />
					<label for=".$fld." id=lbl-".$fld."-text>".$object[$fld]."</label>
					<span id=lbl-".$fld."-lb></span>";
			if(($h['h_table']!="calctype" && $h['h_table']!="targettype") || (($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") || $base[1]=="topadmin") {
				echo "<select id=".$fld." name=".$fld."><option value=X>--- SELECT ---</option>";
				if($h['h_table']=="subdir") {
						foreach($lists['dir'] as $di => $dl) {
							$d = $dl['value'];
							foreach($lists[$h['h_table']]['dir'][$di] as $i => $l) {
								if( (($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") || (isset($my_access['manage']['DIR'][$di]) && $my_access['manage']['DIR'][$di]['act_edit']==true) || (isset($my_access['manage']['SUB'][$i]) && $my_access['manage']['SUB'][$i]['act_edit']==true)) {
									echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i." full=\"".addslashes($l['value'])."\">".maxlenOption(decode($d." - ".$l['value']))."</option>";
								}
							}
						}
				} elseif($h['h_table']=="dir" && $base[1]=="top") {
					foreach($lists['dir'] as $i => $l) {
						if(isset($my_access['manage']['TOP'][$i]) && $my_access['manage']['TOP'][$i]['act_edit']==true) {
							echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i." full=\"".addslashes((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."\">".maxlenOption((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."</option>";
						}
					}
				} else {
					if($h['i_required']+$h['c_required']==0) {
						echo "<option ".(0==$object['o'.$fld] ? "selected" : "")." value=0 full=\"".$unspecified."\">".$unspecified."</option>";
					}
						foreach($lists[$h['h_table']] as $i => $l) {
							echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i." full=\"".addslashes((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."\">".maxlenOption((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."</option>";
						}
				}
				echo "</select>
				<div id=div_".$fld." class='display_me ui-state-ok' style='width: 500px;margin: 5px;'></div>";
			} else {
				echo "<input type=hidden name=$fld id=$fld value=".$object['o'.$fld]." />";
			}
			echo "</td>";
			break;
		case "BOOL":
			echo "<td>";
				echo "<input type=hidden id=lbl-".$fld."-v value=\"".$object[$fld]."\" /><label for=".$fld." id=lbl-".$fld."-text>".$object[$fld]."</label><span id=lbl-".$fld."-lb></span>";
				echo "<select id=".$fld." name=".$fld."><option ".($object[$fld]!=0 ? "selected" : "")." value=1>Yes</option><option ".(!$object[$fld] ? "selected" : "")." value=0>No</option>";
				echo "</select>";
			echo "</td>";
			break;
		default:
			switch($fld) {
				case "kpi_annual":
				case "top_annual":
					echo "<td>";
					if($myObj->isAdmin()) {
//						echo "<div style='width:100px' class='float ui-state-info'>This field should not be changed after the KPI has been created.  Any changes in targets should be reflected in the '".$my_head[$myObj->getFieldPrefix()."revised"]['h_client']."' field.</div>";
						echo "<div style='width: ".($width*0.6)."px' class=float>"; $myObj->displayResult(array("info","This field should not be changed after the KPI has been created.  Any changes in targets should be reflected in the '".$my_head[$myObj->getFieldPrefix()."revised"]['h_client']."' field.")); echo "</div>";
						echo KPIresultEdit($object[$fld],$obj_tt,$fld);
					} else {
						echo KPIresultDisplay($object[$fld],$obj_tt,$fld)." ".KPIresultEdit($object[$fld],$obj_tt,$fld,false,true);
					}
					echo "</td>";
					break;
				case "kpi_revised":
				case "top_revised":
					echo "<td>";
					if($myObj->isAdmin()) {
//						echo "<div style='width:100px' class='float ui-state-info'>This field should not be changed after the KPI has been created.  Any changes in targets should be reflected in the '".$my_head[$myObj->getFieldPrefix()."revised"]['h_client']."' field.</div>";
						echo "<div style='width: ".($width*0.6)."px' class=float>"; $myObj->displayResult(array("info","The system will automatically update this field should the ".$my_head[$myObj->getCTField()]['h_client']." or any ".$result_head[$myObj->getRTargetField()]['h_client']."s (below) be changed.")); echo "</div>";
						echo KPIresultEdit($object[$fld],$obj_tt,$fld);
					} else {
						echo KPIresultDisplay($object[$fld],$obj_tt,$fld)." ".KPIresultEdit($object[$fld],$obj_tt,$fld,false,true);
					}
					echo "</td>";
					break;
				default:
					echo "<td>";
					if($h['c_maxlen']<=0 || $h['c_maxlen']>90) {
						echo "<textarea rows=4 cols=60 name=$fld id=$fld>".decode($object[$fld])."</textarea>";
						if($h['c_maxlen']>0) {
							echo "<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
						}
					} else {
						echo "<input type=text id=".$fld." name=".$fld." value=\"".$object[$fld]."\" size=".($h['c_maxlen']>90 ? "90" : $h['c_maxlen']+5)." maxlength=".$h['c_maxlen']." /> 
							<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
					}
					echo "</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
 ?>
</table>
<?php displayGoBack("",""); ?>
<h2>Results</h2>
<?php 
switch($section) {
	case "KPI":
	case "TOP":
		if($section=="TOP") {
			$mytime = $timeObj->getTopTime();
		} else {
			$mytime = $timeObj->getAllTime();
		}
/** Updated on 2016-11-16 [JC] to try to deal with DB errors where kpi_id is not set. */
		//$kr = $myObj->getUpdateFormDetails($obj_id);
		//$drawObj->drawKPIResults($myObj,$result_head,$mytime,$kr,true,$obj_id);
		if(isset($obj_id) && strlen($obj_id)>0 && is_numeric($obj_id) && $obj_id>0) {
			$kr = $myObj->getUpdateFormDetails($obj_id);
			$drawObj->drawKPIResults($myObj,$result_head,$mytime,$kr,true,$obj_id);
		} elseif(isset($_REQUEST['id']) && strlen($_REQUEST['id'])>0 && is_numeric($_REQUEST['id']) && $_REQUEST['id']>0) {
			$obj_id = $_REQUEST['id'];
			$kr = $myObj->getUpdateFormDetails($obj_id);
			$drawObj->drawKPIResults($myObj,$result_head,$mytime,$kr,true,$obj_id);
		} else {
			echo "<p>An error occurred while trying to display the KPI Results section.  Please go back and try again.</p>";
		}

		/*if($section=="TOP") {
			echo "<h2>Forecast Annual Target</h2>
					<table id=tblfore>";
				foreach($lists['years'] as $i => $l) {
					if($i>1) {
						echo "<tr>
								<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
								<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['tf_value']) ? $forecast[$i]['tf_value'] : 0),$obj_tt)."&nbsp;</td>
							</tr>";
					}
				}			
			echo "</table>";
		}
		break;*/
	/*	$width=700;
		$comment_fields = array('kr_perf','kr_correct','kr_attachment');
		echo "<table id=tblres>";
			echo "<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>";
			foreach($comment_fields as $fld) {
				echo "<th rowspan=2>".$result_head[$fld]['h_client']."</th>";
			}
			echo "</tr>";
			echo "<tr>";
			for($i=1;$i<=2;$i++) {
				foreach($result_head as $fld => $h) {
					if(!in_array($fld,$comment_fields)) {
						echo "<th>".$h['h_client']."</th>";
					}
					if($fld=="kr_actual") {
						echo "<th width=30>R</th>";
					}
				}
			}
			echo "</tr>";
		$values = array();
		foreach($time as $ti => $t) {
			if($my_access['access']['second']) { $active_fld = "active_secondary"; } else { $active_fld = "active_primary"; }
			$res_obj = $results[$ti];
			$values['target'][$ti] = $res_obj['kr_target'];
			$values['actual'][$ti] = $res_obj['kr_actual'];
			$r[1] = KPIcalcResult($values,$obj_ct,array(1,$ti),$ti);
			$r[2] = KPIcalcResult($values,$obj_ct,array(1,$ti),"ALL");
			echo "<tr>";
				echo "<th class=left width=150>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
				for($i=1;$i<=2;$i++) {
					echo "<td class=\"right\">".((($my_access['access']['module'] || $my_access['access']['kpi']) && $i==1 && $base[1]=="admin" && $t[$active_fld]) ? KPIresultEdit($r[$i]['target'],$obj_tt,"kr_target_".$ti) : KPIresultDisplay($r[$i]['target'],$obj_tt))."&nbsp;</td>";
					if($today < $t['start_stamp']) {
						echo "<td class=\"right\">&nbsp;</td>";
						echo "<td></td>";
					} elseif(!$t[$active_fld]) {
						echo "<td class=\"right\">".(KPIresultDisplay($r[$i]['actual'],$obj_tt))."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					} else {
						echo "<td class=\"right\">".(($i==1) ? KPIresultEdit($r[$i]['actual'],$obj_tt,"kr_actual_".$ti) : KPIresultDisplay($r[$i]['actual'],$obj_tt))."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					}
				}
				$flds = $comment_fields;
				foreach($flds as $fld) {
					echo "<td>";
					$active_time = $today < $t['start_stamp'] || !$t[$active_fld] ? true : false;
					$v = $res_obj[$fld];
					$attach_text = "";
					if($fld=="kr_attachment") {
						$x = unserialize($v);
						$z = array();
						//if(strlen($x['poe'])>0) { 
							$v = $me->decode($x['poe']); 
						//}
						$a = $x['attach'];
						if(count($a)>0) {
							$attachments = array();
							foreach($a as $b=>$c) {
								$attachments[$obj_id."_".$ti."_".$b] = $c;
							}
							$attach_text = $me->getObjectAttachmentDisplay(!$active_time,$attachments,$myObj->getAttachFolder(),$myObj->getAttachmentDownloadLink(),$myObj->getAttachmentDownloadOptions(),$myObj->getAttachmentDeleteLink(),$myObj->getAttachmentDeleteOptions());
						}
					} else {
						$v = decode($v);
					}
					if($active_time) {
						echo str_replace(chr(10),"<br />",$v);
					} else {
						echo "<textarea name=\"".$fld."_".$ti."\" rows=3 cols=40>".$v."</textarea>";
					}
					echo $attach_text;
					echo "</td>";
				}
			echo "</tr>";
		}
		echo "</table>";
		//break;
	/*case "TOP":
		$width=900; $active_fld = "active_secondary";
		echo "<table id=tblres>";
			echo "<tr>
				<td  rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>
				<th  colspan=3>Period Performance</th>
				<th  colspan=3>Overall Performance</th>
				<th  rowspan=2>".$result_head['tr_perf']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				<th  rowspan=2>".$result_head['tr_correct']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				<th  rowspan=2>".$result_head['tr_dept']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				<th  rowspan=2>".$result_head['tr_dept_correct']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				</tr>";
			echo "<tr>";
			for($i=1;$i<=2;$i++) {
				foreach($result_head as $fld => $h) {
					if($fld=="tr_target" || $fld=="tr_actual") {
						echo "<th>".$h['h_client']."</th>";
					}
					if($fld=="tr_actual") {
						echo "<th>R</th>";
					}
				}
			}
			echo "</tr>";
		$values = array();
		$start = strtotime("2011-07-01 00:00:00");
		foreach($time as $ti => $t) {
			$res_obj = $results[$ti];
			$values['target'][$ti] = $res_obj['tr_target'];
			$values['actual'][$ti] = $res_obj['tr_actual'];
			$r[1] = KPIcalcResult($values,$obj_ct,array(1,$ti),$ti);
			$r[2] = KPIcalcResult($values,$obj_ct,array(1,$ti),"ALL");
			echo "<tr>";
				echo "<th class=left>Quarter ending<br />".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
				for($i=1;$i<=2;$i++) {
/*					echo "<td class=\"right\">".KPIresultDisplay($r[$i]['target'],$obj_tt)."&nbsp;</td>";
					if($today < $start) {
						echo "<td class=\"right\">&nbsp;</td>";
						echo "<td></td>";
					} else {
						echo "<td class=\"right\">".KPIresultDisplay($r[$i]['actual'],$obj_tt)."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					}*//*
					echo "<td class=\"right\">".((($my_access['access']['module'] || $my_access['access']['toplayer']) && $i==1) ? KPIresultEdit($r[$i]['target'],$obj_tt,"tr_target_".$ti) : KPIresultDisplay($r[$i]['target'],$obj_tt))."&nbsp;</td>";
					if($today < $start) {
						echo "<td class=\"right\">&nbsp;</td>";
						echo "<td></td>";
					} elseif(!$t[$active_fld]) {
						echo "<td class=\"right\">".(KPIresultDisplay($r[$i]['actual'],$obj_tt))."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					} else {
						echo "<td class=\"right\">".(($i==1) ? KPIresultEdit($r[$i]['actual'],$obj_tt,"tr_actual_".$ti) : KPIresultDisplay($r[$i]['actual'],$obj_tt))."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					}
				}
				$flds = array("tr_perf","tr_correct","tr_dept","tr_dept_correct");
				foreach($flds as $fld) {
					//echo "<td>".str_replace(chr(10),"<br />",decode($res_obj[$fld]))."</td>";
					echo "<td>";
					if($today < $start || !$t[$active_fld] || in_array($fld,$dept_flds)) {
						echo displayTRDept(str_replace(chr(10),"<br />",decode($res_obj[$fld])),"HTML");
					} else {
						echo "<textarea name=\"".$fld."_".$ti."\" rows=4 cols=30>".decode($res_obj[$fld])."</textarea>";
					}
					echo "</td>";
				}
			$start = $t['end_stamp']+1;
			echo "</tr>";
		}
		echo "</table>";*/
		if($section=="TOP") {
			echo "<h2>Forecast Annual Target</h2>
					<table id=tblfore>";
				foreach($lists['years'] as $i => $l) {
					if($i>1) {
						echo "<tr>
								<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
								<td class=right width=150>".KPIresultEdit((isset($forecast[$i]['tf_value']) ? $forecast[$i]['tf_value'] : 0),$obj_tt,"years_".$i)."&nbsp;</td>
							</tr>";
					}
				}			
			echo "</table>";
		}
		break;
	case "CAP":
		$first_head = $mheadings['CAP_H'];
		echo "<table>";
			echo "<tr>
				<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
			foreach($first_head as $fldh => $f) {
				echo "<th colspan=".count($result_head).">".$f['h_client']."</th>";
			}
			echo "</tr>";
			echo "<tr>";
			foreach($first_head as $fldh => $f) {
				foreach($result_head as $fld => $h) {
					echo "<th>".$h['h_client']."</th>";
				}
			}
			echo "</tr>";
		$values = array();
		$start = strtotime("2011-07-01 00:00:00");
		foreach($time as $ti => $t) {
			$res_obj = $results[$ti];
			echo "<tr>";
				echo "<th class=left>".$t['display_full']."</th>";
				foreach($first_head as $fldh => $f) {
					foreach($result_head as $fld => $h) {
						$r_fld = $fldh.$fld;
						echo "<td class=right>";
						if($today >= $start || $fld=="target") {
							switch($h['h_type']) {
								case "PERC":
									echo number_format($res_obj[$r_fld],2)."%";
									break;
								case "NUM":
								default:
									echo number_format($res_obj[$r_fld],2);
									break;
							}
						}
						echo "</td>";
					}
				}
				$start = $t['end_stamp']+1;
			echo "</tr>";
		}
		echo "</table>";
		break;
}
?>
<?php displayGoBack("",""); ?>
	<table width=100% id=ftbl><tr class=no-highlight><td class=center>
		<input type=hidden value=N id=goto_plc name=goto_plc />
		<input type=submit value="Save Changes" class=isubmit /> 
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?><span id=plc><input type=button value="Save Changes & Go To Project Life Cycle" id=trigger_plc class=isubmit /></span><?php } ?> <input type=reset /> 
		<?php 
if( (($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") || (($my_access['access']['module'] || $my_access['access']['toplayer']) && $base[1]=="topadmin")) { 
			if($base[1]=="top" || $base[1]=="topadmin") {

				$sql = "SELECT count(kpi_id) as kc FROM ".$dbref."_kpi WHERE kpi_topid = ".$obj_id." AND kpi_active = 1";
				$kc = mysql_fetch_one($sql);

				if($kc['kc']>0) {
					$err = true;
				} else {
					$err = false;
				}
			} elseif($base[1]=="kpi") {
				if($object['kpi_topid']!="N/A" || checkIntRef($object['kpi_capitalid'])) {
					$err = true;
				} else {
					$err = false;
				}
			} else {
				$err = false;
			}
			if(!$err) {
				?><span class=float><input type=button value=Delete class=idelete id=delete_kpi /><?php 
			}
		} 

?></span>
	</td></tr></table>
</td></tr></table>
</form>
<p>&nbsp;</p>
<script type=text/javascript>
	$(document).ready(function() {
		$("#tblres input, #tblres textarea").hover(
			function() { $(this).css("background-color","#DDFFDD"); },
			function() { $(this).css("background-color",""); }
		);
		$("#tblres input, #tblres textarea").focus(
			function() { $(this).css("background-color","#DDFFDD"); }
		);
		$("#tblres input, #tblres textarea").blur(
			function() { $(this).css("background-color",""); }
		);
		$("tr").unbind('mouseenter mouseleave');
	});
	$(function () {
		var head = [];
		<?php foreach($my_head as $fld => $h) { echo chr(10)." head['".$fld."'] = '".addslashes(decode($h['h_client']))."';";	} ?>
		var mytime = [];
		<?php foreach($time as $ti => $t) { echo chr(10)." mytime['".$ti."'] = '".addslashes(decode($t['display_full']))."';";	} ?>
		
		
	<?php if($section=="TOP") { ?>
		var myyears = [];
		<?php foreach($lists['years'] as $i => $l) { echo chr(10)." myyears['".$i."'] = '".addslashes(decode($l['value']))."';";	} ?>
	<?php } ?>

	var field_prefix = '<?php echo $myObj->getFieldPrefix(); ?>';
	
		/* * ON DOCUMENT LOAD * */
		$("div.display_me").hide();
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?> 
				var ct = $("#tbl #kpi_calctype").val();
				var tt = $("#tbl #kpi_targettype").val();
				if(ct=="CO" && tt=="2") {
					$("#ftbl #plc").show();
				} else {
					$("#ftbl #plc").hide();
				}
		<?php } ?>
		$("#tbl select").each(function() {
			id = $(this).prop("id");
						$("#lbl-"+id+"-lb").hide();
						$("#lbl-"+id+"-text").hide();
			$(this).trigger("change");
		});

		
		/* * ON FORM EDIT * */
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?> 
		//WARNING WARNING WARNING - DONE REIMPLEMENT THIS SECTION WITHOUT TAKING calctype_change FUNCTION BELOW INTO ACCOUNT!!!
		/*$("#tbl #kpi_calctype, #tbl #kpi_targettype").change(function() {
			var ct = $("#tbl #kpi_calctype").val();
			var tt = $("#tbl #kpi_targettype").val();
			if(ct=="CO" && tt=="2") {
				$("#ftbl #plc").show();
			} else {
				$("#ftbl #plc").hide();
			}
		});*/
		<?php } ?>
		$("#tbl textarea, #tbl input").keyup(function() {
			//get details
			v = $(this).val();
			v = v.trim();
			i = $(this).prop("id");
			//check maxlength limit
			var mfld = "#"+i+"-max";
			//if(i!="kpi_value" && i!="top_value") {
			if($(mfld).length>0) {
				m = $(mfld).val();
				rem = m - v.length;
				if(rem>0) {
					$("#"+i+"-c").text(rem);
					if(rem>=10) {
						$("#"+i+"-c").css({"color":"#000000","font-weight":"normal","background-color":"#FFFFFF","padding-left":"0px"});
					} else {
						$("#"+i+"-c").css({"color":"#000000","font-weight":"bold","background-color":"#FE9900","padding-left":"6px"});
					}
				} else {
					$("#"+i+"-c").text("0");
						if($(this).get(0).tagName=="textarea")
							$(this).text(v.substring(0,m));
						else
							$(this).val(v.substring(0,m));
					$("#"+i+"-c").css({"color":"#FFFFFF","font-weight":"bold","background-color":"#CC0001","padding-left":"6px"});
				}
			}
			//check required
			req = $("#"+i+"-r").val();
			if(req=="Y") {
				if(v.length==0) {
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			}
		});
		$("#tbl textarea, #tbl input:text").each(function() {
			i = $(this).prop("id");
			if(i.indexOf("-")<0) {
				$(this).trigger("keyup");
			}
		});
		
		$("#tbl select").change(function() {
			i = $(this).prop("id");
			if(!$(this).val() || ($(this).val()=="X" && i!="kpi_topid" && i!="kpi_capitalid" && i!="top_repkpi")) {
				$(this).addClass("required");
				$("#div_"+i).text('').hide();
				//alert($(this).val());
			} else {
				$(this).removeClass("required");
				if(!$(this).prop("multiple")) {
					var alt = $(this).find(":selected").attr("full");
					alt = stripslashes(alt);
					if(alt!="undefined" && alt!=$(this).find(":selected").text()) {
						$("#div_"+i).show().html('<p>'+alt+'</p>');
					} else {
						$("#div_"+i).html('').hide();
					}
				} else {
					var $opt = $(this).find(":selected");
					var txt = "";
					var alt = "";
					$opt.each(function() {
						alt = stripslashes($(this).attr("full"));
						if(alt.length>0 && txt.length>0) { 
							txt+="<br />"; 
						}
						txt+="+ "+alt;
					});
					$("#div_"+i).show().html('<p>'+txt+'</p>');
				}
			}
		});
		//trigger onchange event for current select options
		$("#tbl select").each(function() { $(this).trigger("change"); });
		$("#tblres input").blur(function() {
			v = $(this).val();
			valid8b = validateActual(v);
			if(!valid8b) {
			//if(isNaN(parseFloat(v)) || !isFinite(v)) {
				$(this).addClass("required");
			} else {
				$(this).removeClass("required");
			}
			
		});
		<?php if($section=="TOP") { ?>
			$("#tblfore input").blur(function() {
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
		<?php } ?>
		
		
		/* * SUBMIT FORM * */
		$("#trigger_plc").click(function() {
			if(confirm("Moving to the Project Life Cycle page will save the changes you have made to this KPI.\n\nDo you wish to continue?")==true) {
				$("#goto_plc").val("Y");
				$("form").trigger("submit");
			}
		});
		$("form").submit(function() {
			var err = "";
			$("#tbl input, #tbl select, #tbl textarea").each(function() {
				i = $(this).prop("id");
				if(i=="obj_id") {
				} else if(i.indexOf("-")<0) {
					req = $("#"+i+"-r").val();
					if(req=="Y") {
						ok = true;
						v = $(this).val();
//						v = v.trim();
						if(i!="kpi_topid" && i!="kpi_capitalid") {
							if($(this).hasClass("required")) 			{ ok = false; }
							else if(v.length==0)			 		{ ok = false; }
							else if((i=="kpi_calctype" || i=="top_calctype") && (parseInt(v)==0))		{ ok = false; }
							else if( $(this).get(0).tagName=="SELECT" && i!="kpi_calctype" && i!="top_calctype" && i!="top_repkpi" && (v=="X" || v.length==0) )	{ ok = false; }
							if(!ok) err+="\n - "+head[i];
						}
					}
				}
			});
			err_res = "";
			//$(".valid8me").each(function() {
			$("#tblres input").each(function() {
				i = $(this).prop("name");
				v = $(this).val();
				valid8b = validateActual(v);
				if(!valid8b) {
				//if(isNaN(parseFloat(v)) || !isFinite(v)) {
					if(i.substring(3,9)=="target") { n = "Target"; } else { n = "Actual"; }
					ti = i.substring(10,12);
					n+=" for "+mytime[ti];
					err_res+="\n - "+n;
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			});
			err_fore = "";
		<?php if($section=="TOP") { ?>
			$("#tblfore input").each(function() {
				i = $(this).prop("name");
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
					//if(isNaN(parseFloat(v)) || !isFinite(v)) {
						n = "Forecast Target";
						yi = i.substring(6,8);
						n+=" for "+myyears[yi];
						err_fore+="\n - "+n;
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
		<?php } ?>
			if(err.length>0 || err_res.length>0 || err_fore.length>0) {
				display = "";
				if(err.length>0) display = "The following required fields have not been completed:"+err;
				if(err.length>0 && err_res.length>0) display+="\n\n";
				if(err_res.length>0) display+="The following time periods have invalid values:"+err_res;
				if(err.length>0 && err_fore.length>0) display+="\n\n";
				if(err_fore.length>0) display+="The following forecast years have invalid values:"+err_fore;
				alert(display);
				$("#goto_plc").val("N");
				return false;
			} else {
				return true;
			}
		});
		$("#del_cap").click(function() {
			if(confirm("Are you sure you wish to remove the link between this KPI and the related Capital Project?")==true) {
				$("#tbl #kpi_capitalid").val(0);
				$("#tbl #kpi_capitalid_disp").css("color","#999999");
				$("#tbl #del_cap").prop("disabled",true);
				alert("Please click the 'Save Changes' button at the bottom of the page to save this change.");
			}
		});
		$("input.annual_revised").blur(function() {
			recalculateRevisedTarget(field_prefix);
		});
		$("#"+field_prefix+"calctype").change(function() {
			recalculateRevisedTarget(field_prefix);
		});
<?php 
if( (($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") || (($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="topadmin")) {
?>
	$("#delete_kpi").click(function() {
		if(confirm("Are you sure you wish to delete KPI <?php echo $id_labels_all[$section].$obj_id; ?>?")==true) {
			document.location.href = '<?php echo $self; ?>?act=DELETE&page_id=<?php echo $page_id; ?>&id=<?php echo $obj_id; ?>';
		}
	});
<?php 
} //kpi admin section
?>
	});
	function recalculateRevisedTarget(field_prefix) {
		$(function() {
			var x = 0;
			var sum = 0;
			var count = 0;
			var revised = 0;
			var ct = $("#"+field_prefix+"calctype").val();
			$("input.annual_revised").each(function() {
				x = ($(this).val())*1;
				if(x>0) {
					count++;
					if(ct=="CO") {
						if(x>sum) {
							sum = x;
						}
					} else {
						sum+=x;
					}
				}
			});
			if(ct=="STD" || ct == "REV") {				revised = sum/count;			} else {				revised = sum;			}
			$("input[name="+field_prefix+"revised]").val(revised);
		});
	}
</script>