<?php
	$target_start = 100;	//all
	$forecast_start = 100;	//top
	$forecast2_start = 100;	//cap
	$month_head = array();
	for($i=1;$i<13;$i++) {
		$month_head[$i] = date("F",mktime(12,0,0,$i+6,1,2012));
	}
switch($import_section) {
case "LIST":
	$import_columns = array(
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"dir",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"dir",
			'txt'=>"Name",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"subdir",
			'txt'=>"Parent ".$listoflists['dir']['h_client']." Ref",
		),
		array(
			'fld'=>"subdir",
			'txt'=>"Parent ".$listoflists['dir']['h_client']." Ref",
		),
		array(
			'fld'=>"subdir",
			'txt'=>"Name",
		),
		array(
			'fld'=>"subdir",
			'txt'=>"Primary Y/N",
		),
		array(
			'fld'=>"subdir",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Core Function Y/N",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Sub-function",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"List Display",
		),
		array(
			'fld'=>"list_gfs",
			'txt'=>"Sub-Function Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_munkpa",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_munkpa",
			'txt'=>"Short Code",
		),
		array(
			'fld'=>"list_munkpa",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_natkpa",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_natkpa",
			'txt'=>"Short Code",
		),
		array(
			'fld'=>"list_natkpa",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_ndp",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_ndp",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_pdo",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_pdo",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_natoutcome",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_natoutcome",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Associated ".$listoflists['list_munkpa']['h_client']." Name",
		),
		array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Associated ".$listoflists['list_natkpa']['h_client']." Name",
		),
		array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_idp_top",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_idp_top",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_kpiconcept",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_kpiconcept",
			'txt'=>"Short Code",
		),
		array(
			'fld'=>"list_kpiconcept",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_kpitype",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_kpitype",
			'txt'=>"Short Code",
		),
		array(
			'fld'=>"list_kpitype",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_riskrating",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_riskrating",
			'txt'=>"Short Code",
		),
		array(
			'fld'=>"list_riskrating",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_wards",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"list_wards",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_wards",
			'txt'=>"Municipal Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_area",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"list_area",
			'txt'=>"Name",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_owner",
			'txt'=>"Name",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_fundsource",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_fundsource",
			'txt'=>"Ref",
		),
		array(
			'fld'=>"",
			'txt'=>"",
		),
		array(
			'fld'=>"list_repcate",
			'txt'=>"Name",
		),
		array(
			'fld'=>"list_repcate",
			'txt'=>"Ref",
		),
	);
	break;
case "KPI":
	$target_start = 54;
	$overall = 66;
	$target_text = "Target";
				$import_columns = array(
					0=>array(
						'fld'=>"kpi_id",
						'txt'=>"KPI Ref"),
					1=>array(
						'fld'=>"kpi_subid",
						'txt'=>"Ref"),
					2=>array(
						'fld'=>"kpi_subid",
						'txt'=>"Directorate"),
					3=>array(
						'fld'=>"kpi_subid",
						'txt'=>"Value"),
					4=>array(
						'fld'=>"kpi_topid",
						'txt'=>"Ref"),
					5=>array(
						'fld'=>"kpi_topid",
						'txt'=>"Directorate"),
					6=>array(
						'fld'=>"kpi_topid",
						'txt'=>"KPI Name"),
					7=>array(
						'fld'=>"kpi_gfsid",
						'txt'=>"Ref"),
					8=>array(
						'fld'=>"kpi_gfsid",
						'txt'=>"Value"),
					9=>array(
						'fld'=>'kpi_idpref',
						'txt'=>""),
					10=>array(
						'fld'=>'kpi_natoutcomeid',
						'txt'=>"Ref"),
					11=>array(
						'fld'=>'kpi_natoutcomeid',
						'txt'=>"Value"),
					12=>array(
						'fld'=>'kpi_idpid',
						'txt'=>"Ref"),
					13=>array(
						'fld'=>'kpi_idpid',
						'txt'=>"Value"),
					14=>array(
						'fld'=>'kpi_natkpaid',
						'txt'=>"Ref"),
					15=>array(
						'fld'=>'kpi_natkpaid',
						'txt'=>"Value"),
					16=>array(
						'fld'=>'kpi_munkpaid',
						'txt'=>"Ref"),
					17=>array(
						'fld'=>'kpi_munkpaid',
						'txt'=>"Value"),
					18=>array(
						'fld'=>'kpi_pdoid',
						'txt'=>"Ref"),
					19=>array(
						'fld'=>'kpi_pdoid',
						'txt'=>"Value"),
					20=>array(
						'fld'=>'kpi_ndpid',
						'txt'=>"Ref"),
					21=>array(
						'fld'=>'kpi_ndpid',
						'txt'=>"Value"),
					22=>array(
						'fld'=>'kpi_capitalid',
						'txt'=>"Ref"),
					23=>array(
						'fld'=>'kpi_capitalid',
						'txt'=>"Directorate"),
					24=>array(
						'fld'=>'kpi_capitalid',
						'txt'=>"Project Name"),
					25=>array(
						'fld'=>'kpi_value',
						'txt'=>""),
					26=>array(
						'fld'=>'kpi_unit',
						'txt'=>""),
					27=>array(
						'fld'=>'kpi_conceptid',
						'txt'=>"Ref"),
					28=>array(
						'fld'=>'kpi_conceptid',
						'txt'=>"Value"),
					29=>array(
						'fld'=>'kpi_typeid',
						'txt'=>"Ref"),
					30=>array(
						'fld'=>'kpi_typeid',
						'txt'=>"Value"),
					31=>array(
						'fld'=>'kpi_riskref',
						'txt'=>""),
					32=>array(
						'fld'=>'kpi_risk',
						'txt'=>""),
					33=>array(
						'fld'=>'kpi_riskratingid',
						'txt'=>"Ref"),
					34=>array(
						'fld'=>'kpi_riskratingid',
						'txt'=>"Value"),
					35=>array(
						'fld'=>'kpi_wards',
						'txt'=>""),
					36=>array(
						'fld'=>'kpi_area',
						'txt'=>""),
					37=>array(
						'fld'=>'kpi_ownerid',
						'txt'=>""),
					38=>array(
						'fld'=>'kpi_baseline',
						'txt'=>""),
					39=>array(
						'fld'=>'kpi_pyp',
						'txt'=>""),
					40=>array(
						'fld'=>'kpi_perfstd',
						'txt'=>""),
					41=>array(
						'fld'=>'kpi_poe',
						'txt'=>""),
					42=>array(
						'fld'=>'kpi_mtas',
						'txt'=>""),
					43=>array(
						'fld'=>'kpi_text1',
						'txt'=>""),
					44=>array(
						'fld'=>'kpi_text2',
						'txt'=>""),
					45=>array(
						'fld'=>'kpi_text3',
						'txt'=>""),
					46=>array(
						'fld'=>'kpi_repcate',
						'txt'=>""),
					47=>array(
						'fld'=>'kpi_calctype',
						'txt'=>"Ref"),
					48=>array(
						'fld'=>'kpi_calctype',
						'txt'=>"Value"),
					49=>array(
						'fld'=>'kpi_targettype',
						'txt'=>"Ref"),
					50=>array(
						'fld'=>'kpi_targettype',
						'txt'=>"Value"),
					51=>array(
						'fld'=>"kpi_annual",
						'txt'=>"Number"),
					52=>array(
						'fld'=>"kpi_revised",
						'txt'=>"Number"),
					53=>array(
						'fld'=>"",
						'txt'=>""),
					54=>array(
						'fld'=>'target_1',
						'txt'=>$month_head[1]),
					55=>array(
						'fld'=>'target_2',
						'txt'=>$month_head[2]),
					56=>array(
						'fld'=>'target_3',
						'txt'=>$month_head[3]),
					57=>array(
						'fld'=>'target_4',
						'txt'=>$month_head[4]),
					58=>array(
						'fld'=>'target_5',
						'txt'=>$month_head[5]),
					59=>array(
						'fld'=>'target_6',
						'txt'=>$month_head[6]),
					60=>array(
						'fld'=>'target_7',
						'txt'=>$month_head[7]),
					61=>array(
						'fld'=>'target_8',
						'txt'=>$month_head[8]),
					62=>array(
						'fld'=>'target_9',
						'txt'=>$month_head[9]),
					63=>array(
						'fld'=>'target_10',
						'txt'=>$month_head[10]),
					64=>array(
						'fld'=>'target_11',
						'txt'=>$month_head[11]),
					65=>array(
						'fld'=>'target_12',
						'txt'=>$month_head[12]),
				);
	break;
	case "TOP":
		$target_start = 42;//39;
		$forecast_start = 47;//44;
		$target_text = "Target";
		$overall = 46;//43;
				$import_columns = array(
					0=>array(
						'fld'=>"top_id",
						'txt'=>"KPI Ref"),
					1=>array(
						'fld'=>"top_dirid",
						'txt'=>"Ref"),
					2=>array(
						'fld'=>"top_dirid",
						'txt'=>"Value"),
					3=>array(
						'fld'=>"top_repkpi",
						'txt'=>""),
					4=>array(
						'fld'=>"top_pmsref",
						'txt'=>""),
					5=>array(
						'fld'=>"top_gfsid",
						'txt'=>"Ref"),
					6=>array(
						'fld'=>"top_gfsid",
						'txt'=>"Value"),
					7=>array(
						'fld'=>"top_natoutcomeid",
						'txt'=>"Ref"),
					8=>array(
						'fld'=>"top_natoutcomeid",
						'txt'=>"Value"),
					9=>array(
						'fld'=>"top_natkpaid",
						'txt'=>"Ref"),
					10=>array(
						'fld'=>"top_natkpaid",
						'txt'=>"Value"),
					11=>array(
						'fld'=>"top_pdoid",
						'txt'=>"Ref"),
					12=>array(
						'fld'=>"top_pdoid",
						'txt'=>"Value"),
					13=>array(
						'fld'=>"top_ndpid",
						'txt'=>"Ref"),
					14=>array(
						'fld'=>"top_ndpid",
						'txt'=>"Value"),
					15=>array(
						'fld'=>"top_idp",
						'txt'=>"Ref"),
					16=>array(
						'fld'=>"top_idp",
						'txt'=>"Value"),
					17=>array(
						'fld'=>"top_munkpaid",
						'txt'=>"Ref"),
					18=>array(
						'fld'=>"top_munkpaid",
						'txt'=>"Value"),
					19=>array(
						'fld'=>"top_value",
						'txt'=>""),
					20=>array(
						'fld'=>"top_unit",
						'txt'=>""),
					21=>array(
						'fld'=>"top_risk",
						'txt'=>""),
					22=>array(
						'fld'=>"top_riskratingid",
						'txt'=>"Ref"),
					23=>array(
						'fld'=>"top_riskratingid",
						'txt'=>"Value"),
					24=>array(
						'fld'=>"top_wards",
						'txt'=>""),
					25=>array(
						'fld'=>"top_area",
						'txt'=>""),
					26=>array(
						'fld'=>"top_ownerid",
						'txt'=>""),
					27=>array(
						'fld'=>"top_baseline",
						'txt'=>""),
					28=>array(
						'fld'=>"top_poe",
						'txt'=>""),
					29=>array(
						'fld'=>"top_pyp",
						'txt'=>""),
					30=>array(
						'fld'=>"top_mtas",
						'txt'=>""),
					31=>array(
						'fld'=>"top_text1",
						'txt'=>""),
					32=>array(
						'fld'=>"top_text2",
						'txt'=>""),
					33=>array(
						'fld'=>"top_text3",
						'txt'=>""),
					34=>array(
						'fld'=>"top_repcate",
						'txt'=>""),
					35=>array(
						'fld'=>"top_calctype",
						'txt'=>"Ref"),
					36=>array(
						'fld'=>"top_calctype",
						'txt'=>"Value"),
					37=>array(
						'fld'=>"top_targettype",
						'txt'=>"Ref"),
					38=>array(
						'fld'=>"top_targettype",
						'txt'=>"Value"),
					39=>array(
						'fld'=>"top_annual",
						'txt'=>""),
					40=>array(
						'fld'=>"top_revised",
						'txt'=>""),
					41=>array(
						'fld'=>"",
						'txt'=>""),
					42=>array(
						'fld'=>"target_1",
						'txt'=>$month_head[3]),
					43=>array(
						'fld'=>"target_2",
						'txt'=>$month_head[6]),
					44=>array(
						'fld'=>"target_3",
						'txt'=>$month_head[9]),
					45=>array(
						'fld'=>"target_4",
						'txt'=>$month_head[12]),
					46=>array(
						'fld'=>"",
						'txt'=>""),
					47=>array(
						'fld'=>"forecast_1",
						'txt'=>"2013/2014"),
					48=>array(
						'fld'=>"forecast_2",
						'txt'=>"2014/2015"),
					49=>array(
						'fld'=>"forecast_3",
						'txt'=>"2015/2016"),
					50=>array(
						'fld'=>"forecast_4",
						'txt'=>"2016/2017"),
					51=>array(
						'fld'=>"forecast_5",
						'txt'=>"2017/2018"),
				);
	break;
case "RS":
		$target_start = 3;
		$target_text = "Budget";
		$colspanned[2]=array();
		$colspanned[3]=array();
				$import_columns = array(
					0=>array(
						'fld'=>"rs_id",
						'txt'=>"Line Ref"),
					1=>array(
						'fld'=>"rs_value",
						'txt'=>""),
					2=>array(
						'fld'=>"rs_vote",
						'txt'=>""),
					3=>array(
						'fld'=>"budget_1",
						'txt'=>$month_head[1]),
					4=>array(
						'fld'=>"budget_2",
						'txt'=>$month_head[2]),
					5=>array(
						'fld'=>"budget_3",
						'txt'=>$month_head[3]),
					6=>array(
						'fld'=>"budget_4",
						'txt'=>$month_head[4]),
					7=>array(
						'fld'=>"budget_5",
						'txt'=>$month_head[5]),
					8=>array(
						'fld'=>"budget_6",
						'txt'=>$month_head[6]),
					9=>array(
						'fld'=>"budget_7",
						'txt'=>$month_head[7]),
					10=>array(
						'fld'=>"budget_8",
						'txt'=>$month_head[8]),
					11=>array(
						'fld'=>"budget_9",
						'txt'=>$month_head[9]),
					12=>array(
						'fld'=>"budget_10",
						'txt'=>$month_head[10]),
					13=>array(
						'fld'=>"budget_11",
						'txt'=>$month_head[11]),
					14=>array(
						'fld'=>"budget_12",
						'txt'=>$month_head[12]),
				);
	break;
	case "CAP":
		$forecast2_start = 31;
		$target_start = 18;
		$target_text = "Budget";
				$import_columns = array(
					0=>array(
						'fld'=>"cap_id",
						'txt'=>"Project Ref"),
					1=>array(
						'fld'=>"cap_subid",
						'txt'=>"Ref"),
					2=>array(
						'fld'=>"cap_subid",
						'txt'=>"Directorate"),
					3=>array(
						'fld'=>"cap_subid",
						'txt'=>"Value"),
					4=>array(
						'fld'=>"cap_gfsid",
						'txt'=>"Ref"),
					5=>array(
						'fld'=>"cap_gfsid",
						'txt'=>"Value"),
					6=>array(
						'fld'=>"cap_cpref",
						'txt'=>""),
					7=>array(
						'fld'=>"cap_idpref",
						'txt'=>""),
					8=>array(
						'fld'=>"cap_vote",
						'txt'=>""),
					9=>array(
						'fld'=>"cap_name",
						'txt'=>""),
					10=>array(
						'fld'=>"cap_descrip",
						'txt'=>""),
					11=>array(
						'fld'=>"capital_fundsource",
						'txt'=>""),
					12=>array(
						'fld'=>"cap_planstart",
						'txt'=>""),
					13=>array(
						'fld'=>"cap_planend",
						'txt'=>""),
					14=>array(
						'fld'=>"cap_actualstart",
						'txt'=>""),
					15=>array(
						'fld'=>"capital_wards",
						'txt'=>""),
					16=>array(
						'fld'=>"capital_area",
						'txt'=>""),
					17=>array(
						'fld'=>"",
						'txt'=>""),
					18=>array(
						'fld'=>"target_1",
						'txt'=>$month_head[1]),
					19=>array(
						'fld'=>"target_2",
						'txt'=>$month_head[2]),
					20=>array(
						'fld'=>"target_3",
						'txt'=>$month_head[3]),
					21=>array(
						'fld'=>"target_4",
						'txt'=>$month_head[4]),
					22=>array(
						'fld'=>"target_5",
						'txt'=>$month_head[5]),
					23=>array(
						'fld'=>"target_6",
						'txt'=>$month_head[6]),
					24=>array(
						'fld'=>"target_7",
						'txt'=>$month_head[7]),
					25=>array(
						'fld'=>"target_8",
						'txt'=>$month_head[8]),
					26=>array(
						'fld'=>"target_9",
						'txt'=>$month_head[9]),
					27=>array(
						'fld'=>"target_10",
						'txt'=>$month_head[10]),
					28=>array(
						'fld'=>"target_11",
						'txt'=>$month_head[11]),
					29=>array(
						'fld'=>"target_12",
						'txt'=>$month_head[12]),
					30=>array(
						'fld'=>"",
						'txt'=>""),
					31=>array(
						'fld'=>"forecast_1_1",
						'txt'=>"Current Year"),
					32=>array(
						'fld'=>"forecast_1_2",
						'txt'=>"Current Year"),
					33=>array(
						'fld'=>"forecast_2_1",
						'txt'=>"Next Year"),
					34=>array(
						'fld'=>"forecast_2_2",
						'txt'=>"Next Year"),
					35=>array(
						'fld'=>"forecast_3_1",
						'txt'=>"Year+2"),
					36=>array(
						'fld'=>"forecast_3_2",
						'txt'=>"Year+2"),
					37=>array(
						'fld'=>"forecast_4_1",
						'txt'=>"Year+3"),
					38=>array(
						'fld'=>"forecast_4_2",
						'txt'=>"Year+3"),
					39=>array(
						'fld'=>"forecast_5_1",
						'txt'=>"Year+4"),
					40=>array(
						'fld'=>"forecast_5_2",
						'txt'=>"Year+4"),
				);
	break;
case "CF":
	$target_start = 8;
	$target_text = "Budget";
				$import_columns = array(
					0=>array(
						'fld'=>"cf_subid",
						'txt'=>"Ref"),
					1=>array(
						'fld'=>"cf_subid",
						'txt'=>"Directorate"),
					2=>array(
						'fld'=>"cf_subid",
						'txt'=>"Value"),
					3=>array(
						'fld'=>"cf_value",
						'txt'=>""),
					4=>array(
						'fld'=>"cf_gfsid",
						'txt'=>"Ref"),
					5=>array(
						'fld'=>"cf_gfsid",
						'txt'=>"Value"),
					6=>array(
						'fld'=>"cf_vote",
						'txt'=>""),
					7=>array(
						'fld'=>"",
						'txt'=>""),
					8=>array(
						'fld'=>"cf_rev_1_1",
						'txt'=>""),
					9=>array(
						'fld'=>"cf_opex_1_1",
						'txt'=>""),
					10=>array(
						'fld'=>"cf_capex_1_1",
						'txt'=>""),
					11=>array(
						'fld'=>"cf_rev_1_2",
						'txt'=>""),
					12=>array(
						'fld'=>"cf_opex_1_2",
						'txt'=>""),
					13=>array(
						'fld'=>"cf_capex_1_2",
						'txt'=>""),
					14=>array(
						'fld'=>"cf_rev_1_3",
						'txt'=>""),
					15=>array(
						'fld'=>"cf_opex_1_3",
						'txt'=>""),
					16=>array(
						'fld'=>"cf_capex_1_3",
						'txt'=>""),
					17=>array(
						'fld'=>"cf_rev_1_4",
						'txt'=>""),
					18=>array(
						'fld'=>"cf_opex_1_4",
						'txt'=>""),
					19=>array(
						'fld'=>"cf_capex_1_4",
						'txt'=>""),
					20=>array(
						'fld'=>"cf_rev_1_5",
						'txt'=>""),
					21=>array(
						'fld'=>"cf_opex_1_5",
						'txt'=>""),
					22=>array(
						'fld'=>"cf_capex_1_5",
						'txt'=>""),
					23=>array(
						'fld'=>"cf_rev_1_6",
						'txt'=>""),
					24=>array(
						'fld'=>"cf_opex_1_6",
						'txt'=>""),
					25=>array(
						'fld'=>"cf_capex_1_6",
						'txt'=>""),
					26=>array(
						'fld'=>"cf_rev_1_7",
						'txt'=>""),
					27=>array(
						'fld'=>"cf_opex_1_7",
						'txt'=>""),
					28=>array(
						'fld'=>"cf_capex_1_7",
						'txt'=>""),
					29=>array(
						'fld'=>"cf_rev_1_8",
						'txt'=>""),
					30=>array(
						'fld'=>"cf_opex_1_8",
						'txt'=>""),
					31=>array(
						'fld'=>"cf_capex_1_8",
						'txt'=>""),
					32=>array(
						'fld'=>"cf_rev_1_9",
						'txt'=>""),
					33=>array(
						'fld'=>"cf_opex_1_9",
						'txt'=>""),
					34=>array(
						'fld'=>"cf_capex_1_9",
						'txt'=>""),
					35=>array(
						'fld'=>"cf_rev_1_10",
						'txt'=>""),
					36=>array(
						'fld'=>"cf_opex_1_10",
						'txt'=>""),
					37=>array(
						'fld'=>"cf_capex_1_10",
						'txt'=>""),
					38=>array(
						'fld'=>"cf_rev_1_11",
						'txt'=>""),
					39=>array(
						'fld'=>"cf_opex_1_11",
						'txt'=>""),
					40=>array(
						'fld'=>"cf_capex_1_11",
						'txt'=>""),
					41=>array(
						'fld'=>"cf_rev_1_12",
						'txt'=>""),
					42=>array(
						'fld'=>"cf_opex_1_12",
						'txt'=>""),
					43=>array(
						'fld'=>"cf_capex_1_12",
						'txt'=>""),
				);

	break;
}
$cells = array();
foreach($import_columns as $c => $cl) {
	if(strlen($cl['fld'])==0 || !in_array($cl['fld'],$cells)) {
		$cells[$c] = $cl['fld'];
	}
}







/** FUNCTIONS **/
function drawCSVColumnsGuidelines($id_fld,$target_text="Target") {
	global $mheadings, $import_columns;
	global $target_start, $forecast_start, $forecast2_start;
	global $import_section;
	global $month_head;
	echo "
			<h3>CSV Columns</h3>
			<table>
				<tr>
					<th>Column</th>
					<th>Heading</th>
				</tr>";
for($i=1;$i<=count($import_columns);$i++) {
	echo "<tr><td class='center b'>";
	if($i<=26) {
		echo chr(64+$i);
	} else {
		echo chr(64+floor(($i-1)/26)).chr(64+$i-(floor(($i-1)/26)*26));
	}
	$col = $import_columns[$i-1];
	$fld = isset($col['fld']) ? $col['fld'] : "";
	if($fld==$id_fld) { 
		$txt = $col['txt']; 
	} else {
		if(strlen($fld)==0) { 
			$txt = "";
		} elseif($i>$forecast2_start) { 
			$txt = $col['txt']." Forecast ".$target_text.": ".($i % 2 == 1 ? "Other" : "CRR");
		} elseif($i>$forecast_start) { 
			$txt = "Forecast ".$target_text;
		} elseif($i>$target_start) { 
			if($import_section=="CF") { 
				$calc = ($i-$target_start)%3;
				switch($calc) {
				case 1:	$txt = "Revenue"; break;
				case 2: $txt = "Op. Exp."; break;
				case 0: $txt = "Cap. Exp."; break;
				}
				$txt.= " ".$target_text;
				$mon = explode("_",$fld);
				$txt = $month_head[$mon[3]]." ".$txt;
			} else {
				$txt = $target_text;
			}
		} else {
			$txt = isset($mheadings[$import_section][$fld]['h_client']) ? $mheadings[$import_section][$fld]['h_client'] : "";
		}
		$txt.= isset($col['txt']) && $i<=$forecast2_start && strlen($col['txt'])>0 ? ": ".$col['txt'] : "";
	}
	echo "</td><td>";
		echo $txt;
	echo "</td></tr>";
}
	echo "
	</table>";
	displayGoBack("support_import.php");
}

function getImportHeadings() {
	global $mheadings, $import_columns, $cells, $colspanned;
	global $doc_headings, $doc_headings2;
	global $target_start, $forecast_start, $forecast2_start, $overall;
	global $import_section;
	global $month_head;
$onscreen = "<tr><th rowspan=2 style='vertical-align: middle;'> ? </th>";
	$cdata = array();
		for($i=0;$i<count($import_columns);$i++) {
			if(isset($cells[$i])) {
				$col = $import_columns[$i];
				$fld = isset($col['fld']) ? $col['fld'] : "";
				$colspan = 1;
					if(isset($colspanned[2]) && is_array($colspanned[2]) && in_array($i,$colspanned[2])) {
						$colspan = 2;
					} elseif(isset($colspanned[3]) && is_array($colspanned[3]) && in_array($i,$colspanned[3])) {
						$colspan = 3;
					}
				if(strlen($fld)==0) {
					$txt = "";
				} elseif($fld==strtolower($import_section)."_id") { 
					$txt = $col['txt']; 
				} elseif($i>=$forecast2_start) { 
					$txt = ($i%2==0 ? $col['txt'].": Other" : $col['txt'].": CRR");
				} elseif($i>=$forecast_start) { 
					$txt = $col['txt']." Forecast";
				} elseif($i>=$target_start) { 
					if($import_section=="CF") { 
						$f = explode("_",$fld);
						$txt = $month_head[$f[3]];
						switch($f[1]) {
							case "rev": $txt.= ": Revenue"; break;
							case "opex": $txt.= ": Op.&nbsp;Exp."; break;
							case "capex": $txt.= ": Cap.&nbsp;Exp."; break;
						}
					} else {
						$txt = isset($col['txt']) && strlen($col['txt'])>0 ? $col['txt'] : "";
					}
				} else {
					$txt = isset($mheadings[$import_section][$fld]['h_client']) ? $mheadings[$import_section][$fld]['h_client'] : "";
				}
				if(isset($overall) && $i==$overall) {
				} else {
					$cdata[]="<th class=ignite colspan=".$colspan.">".$txt."</th>";
				}
				if(isset($overall) && $i == $overall-1) {
					$cdata[] = "<th class=ignite rowspan=2 colspan=".$colspan.">Calculated Annual Target</th>";
				}
				//$cdata[] = "<th>".$txt.":".$i."</th>";
			} else {
				$txt = "";
			}
		}
	$onscreen.= implode("",$cdata);
	
					$onscreen.= "</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							if($cell < count($import_columns)) {
								$colspan = is_array($colspanned) && isset($colspanned[2]) && in_array($cell,$colspanned[2]) ? 2 : 1;
								$colspan = is_array($colspanned) && isset($colspanned[2]) && in_array($cell,$colspanned[3]) ? 3 : $colspan;
								if($import_section=="CAP" && $cell>=$forecast2_start) {
									$c = (strlen($c)>0 ? $c : $doc_headings[$cell-1]).": ".$doc_headings2[$cell];
								} elseif($import_section=="CF" && $cell>=$target_start) {
									if(strlen($c)==0) {
										$c = $doc_headings[$cell-1];
									}
									if(strlen($c)==0) {
										$c = $doc_headings[$cell-2];
									}
									$c.=": ".$doc_headings2[$cell];
								}
								if((strlen($c)>0 || $cell>=($target_start-1)) && (isset($overall) && $cell!=$overall)) {
									$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
								}
							}
						}
					$onscreen.= "</tr>";

	return $onscreen;
}
?>