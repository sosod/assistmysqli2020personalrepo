<?php 
$page_title = "Departmental SDBIP";
$import_section = "KPI";
$get_headings = array("KPI","KPI_R");
$dirid = isset($_REQUEST['d']) ? $_REQUEST['d'] : 0;
include("support_import_headings.php");
include("inc/header.php"); 
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
//error_reporting(-1);
$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > ".$page_title, $self);


$valid_numbers = array("0","1","2","3","4","5","6","7","8","9",".");

//GET ALL LIST VALUES
$listoflists = array(
	'dir'=>				array('name'=>"Directorate",'data'=>array()),
	'subdir'=>			array('name'=>"Sub-Directorate",'data'=>array()),
	'list_gfs'=>		array('name'=>"Function",'data'=>array()),
	'list_munkpa'=>		array('name'=>"Municipal KPA",'data'=>array()),
	'list_natkpa'=>		array('name'=>"National KPA",'data'=>array()),
	'list_natoutcome'=>	array('name'=>"National Outcome",'data'=>array()),
	'list_idp_kpi'=>	array('name'=>"Departmental SDBIP: IDP Objective",'data'=>array()),
	//'list_idp_top'=>	array('name'=>"Top Layer: IDP Objective",'data'=>array()),
	'list_kpiconcept'=>	array('name'=>"KPI Concept",'data'=>array()),
	'list_kpitype'=>	array('name'=>"KPI Type",'data'=>array()),
	'list_riskrating'=>	array('name'=>"Risk Rating",'data'=>array()),
	'list_area'=>		array('name'=>"Area",'data'=>array()),
	'list_wards'=>		array('name'=>"Ward",'data'=>array()),
	'list_owner'=>		array('name'=>"KPI Owner / Driver",'data'=>array()),
	//'list_fundsource'=>	array('name'=>"Funding Source",'data'=>array()),
	'list_calctype'=>	array('name'=>"Calculation Type",'data'=>array()),
	'list_targettype'=>	array('name'=>"Target Type",'data'=>array()),
	'list_pdo'=>		array('name'=>"Pre-determined Objectives",'data'=>array()),
	'list_ndp'=>		array('name'=>"NDP Objectives",'data'=>array()),
	'list_repcate'=>	array('name'=>"Reporting Category",'data'=>array()),
);
$owner = array();
$wa = array(
	'kpi_wards'=>array(),
	'kpi_area'=>array(),
	'kpi_repcate'=>array(),
);
foreach($listoflists as $tbl => $l) {
	$rs = getRS("SELECT * FROM ".$dbref."_".$tbl." WHERE active = true");
	while($row = mysql_fetch_assoc($rs)) {
		if($tbl!="list_calctype") {
			$listoflists[$tbl]['data'][$row['id']] = $row;
		} else {
			$listoflists[$tbl]['data'][$row['code']] = $row;
		}
		switch($tbl) {
			case "list_owner": $owner[strtolower(decode($row['value']))] = $row['id']; break;
			case "list_wards": 
				if(strtolower($row['value'])=="all") {
					$wa['kpi_wards']['ALL'] = $row['id'];
				} else {
					$wa['kpi_wards'][$row['code']] = $row['id'];
				}
				break;
			case "list_area":	$wa['kpi_area'][$row['id']] = $row['id']; break;
			case "list_repcate":	$wa['kpi_repcate'][$row['id']] = $row['id']; break;
		} 
	}
}
//arrPrint($listoflists['list_area']);
//arrPrint($wa['area']);
//arrPrint($listoflists['list_wards']);
//arrPrint($wa['wards']);
function valError($v,$e) {
	return $v."<br /><b>-> ".$e."</b>";
}
function valList($v,$e) {
	return $v."<br />=> ".$e;
}

$file_error = array("error","An error occurred while trying to import the file.  Please go back and try again.");
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VIEW";

switch($act) {
	case "RESET":
		$r_sql = array();
		$r_sql[] = "TRUNCATE ".$dbref."_kpi";
		$r_sql[] = "TRUNCATE ".$dbref."_kpi_results";
		$r_sql[] = "TRUNCATE ".$dbref."_kpi_wards";
		$r_sql[] = "TRUNCATE ".$dbref."_kpi_area";
		foreach($r_sql as $sql) {	
			$mnr = db_update($sql);
		}
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = '".$import_section."'");
		$import_status[$import_section] = false;
		$result = array("ok","All $page_title tables successfully reset.");
		logImport($import_log_file, $page_title." > Reset tables", $self);
		break;
	case "MARK":
		db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = '$import_section'");
		$import_status[$import_section] = true;
		$result = array("ok","Departmental SDBIP now available.");
		logImport($import_log_file, $page_title." > Marked Departmental SDBIP as imported", $self);
		break;
	case "UNMARK":
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = '$import_section'");
		$import_status[$import_section] = false;
		$result = array("ok","Departmental SDBIP no longer available.");
		logImport($import_log_file, $page_title." > Reversed marking of Departmental SDBIP as imported", $self);
		break;
}

?>		<style type=text/css>
		table th {
			vertical-align: top;
		}
		.dark-info {
			background: #FE9900 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		.dark-error {
			background: #CC0001 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		th.ignite { background-color: #555555; }
		th.doc { background-color: #999999; }
		</style>
<?php
if(($act=="PREVIEW" || $act=="ACCEPT") && (!checkIntRef($dirid) || !isset($listoflists['dir']['data'][$dirid]))) { displayResult(array("error","Please select a valid Directorate before clicking the Import button.")); $act = "VIEW"; }
logImport($import_log_file, $page_title." > ".$act, $self);
$save_echo_filename = $import_section."_".date("YmdHis")."_".$act.".html";
$onscreen = "";

switch($act) {
	case "VIEW": case "RESET": case "GENERATE": case "MARK": case "UNMARK":
		$kpirs = getRS("SELECT kpi_id FROM ".$dbref."_kpi ORDER BY kpi_id DESC LIMIT 1");
		$row = mysql_fetch_assoc($kpirs);
		$kpiid = (isset($row['kpi_id']) && checkIntRef($row['kpi_id'])) ? $row['kpi_id'] : 0;
		$sql = "SELECT d.id, count(kpi_id) as kc FROM ".$dbref."_kpi INNER JOIN ".$dbref."_subdir s ON kpi_subid = s.id AND s.active = true INNER JOIN ".$dbref."_dir d ON s.dirid = d.id AND d.active = true WHERE kpi_active = true GROUP BY d.id";
		$kcrs = getRS($sql);
		$kc = array();
		while($row = mysql_fetch_array($kcrs)) {
			$kc[$row['id']] = $row['kc'];
		}
		//echo $kpiid;
		?>
		<script>
			$(function() {
				$(":button").click(function() {
					var id = $(this).prop("id");
					switch(id) {
						case "generate": document.location.href = 'support_import_generate.php?i=<?php echo $import_section; ?>'; break;
						case "reset": if(confirm("This will delete any existing data!\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=RESET'; } break;
						case "mark": if(confirm("This will make the Departmental SDBIP available to all other users.\nNo further importing will be possible without deleting existing data.\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=MARK'; } break;
						case "unmark": if(confirm("This will make the Departmental SDBIP NO LONGER available to all other users.\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=UNMARK'; } break;
					}
				});
				$("#isubmit:button").click(function() {
					var ifl = $("#ifl").prop("value");
					if(ifl.length > 0) 
						$("#list").submit();
					else
						alert("Please select a file for import.");
				});
			});
		</script>
		<?php displayResult($result); ?>
		<p>In order to generate the CSV files necessary for the import, simply go to each worksheet in the SDBIP document and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'.
		<br /><b>PLEASE ENSURE THAT THE COLUMNS MATCH THOSE AS GIVEN ON THE IMPORT PAGE FOR EACH SECTION!!!</b></p>
		<?php
$i = 1;
			?>
			<form id=list action="<?php echo $self; ?>" method=post enctype="multipart/form-data">
				<input type=hidden name=act value=PREVIEW />
				<table>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Generate the CSV file:
							<br />+ <input type=button value=Generate id=generate /> the CSV template here and copy the data into the sheet from the SDBIP template  OR
							<br />+ Open the SDBIP template, go to each Directorate sheet, click File > Save As & change the File Type option to Comma-delimited (*.csv).
							<br />&nbsp;<br />Please ensure that the columns match up to those given below.&nbsp;
						</td>
					</tr>
					<?php //if($kpiid > 0) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reset the tables to delete previously loaded information (if necessary).&nbsp;<span class=float><input type=button value="Reset Tables" id=reset class=idelete /></span></td>
					</tr>
					<?php //} 
					if(!$import_status['KPI']) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td><ul><li>Select the CSV file &nbsp;&nbsp;<input type=file name=ifile id=ifl />&nbsp;</li>
						<li>Select the Directorate: <select name=d><option selected value=0>--- SELECT Directorate ---</option><?php
							foreach($listoflists['dir']['data'] as $d) {
								echo "<option value=".$d['id'].">".$d['value']." (".(isset($kc[$d['id']]) ? $kc[$d['id']] : 0)." kpis)</option>";
							}
						?></select></li>
						<li>Click the Import button.<span class=float><input type=button id=isubmit value=Import class=isubmit /></span></li></ul></td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Review the information.  If the information is correct, click the Accept button.
						<br /><b>NO DATA WILL BE IMPORTED UNTIL THE FINAL ACCEPT BUTTON HAS BEEN CLICKED!!</b></td>
					</tr>
					<?php
					}
					if($kpiid > 0 && !$import_status['KPI']) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Mark the Departmental SDBIP as fully imported.&nbsp;&nbsp;<span class=float><input type=button id=mark value=Mark class=iinform /></span>
						<br />The Departmental SDBIP will not be made available for viewing/updating etc until this step has been completed.<br />Only do this once the entire Departmental SDBIP has been loaded.</td>
					</tr>
					<?php } 
					if($kpiid > 0 && $import_status['KPI']) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reopen Departmental SDBIP for import.&nbsp;&nbsp;<span class=float><input type=button id=unmark value='Undo Mark' class=iinform /></span>
						<br />The Departmental SDBIP will no longer be available for viewing/updating etc after this step has been completed.<br />Only do this if you need to add additional KPIs after the import has been completed.</td>
					</tr>
					<?php } 
?>
				</table>
			</form>
			<?php displayGoBack("support_import.php");
			
	drawCSVColumnsGuidelines("kpi_id",$target_text);		
			
			
		break;	//switch act case view/reset/generate
	case "PREVIEW":
		$onscreen.= "<h1 class=centre>$page_title Preview</h1><p class=centre>Please click the \"Accept\" button at the bottom of the page<br />in order to finalise the import of the list items.</p>";
		$onscreen.= "<p class=centre>Any errors highlighted in <span class=dark-error>red</span> are errors that prevent the Departmental SDBIP from importing (fatal errors).<br />The \"Accept\" button will not be available until all of these errors have been corrected.</p>";
		$onscreen.= "<p class=centre>Any kpi data cells highlighted in <span class=dark-info>orange</span> are where the value associated with the given list reference does not match the value given in the next cell.<br />These errors do not prevent the Departmental SDBIP from being imported (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Any kpi target cells highlighted in <span class=dark-info>orange</span> are where target type is percentage and the value in the cell is 1 or less which is potentially an incorrectly captured 100%.<br />These errors do not prevent the Departmental SDBIP from being imported (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Please remember, if you get any \"Too long\" errors, that any non-alphanumeric characters in text fields are converted a special code to allow them to be uploaded to the database.
		<br />This code is generally 6 characters long and counts towards the overall length of the text field.</p>
		<p class=center><span class=iinform>Please note:</span> The top heading row displays the headings from the module as per Setup > Defaults > Headings.
		<br />The second heading row displays the headings from the import file.
		<br />Please ensure that they match up to ensure that the correct data is being imported into the correct field.</p>";
	case "ACCEPT":
		if($act!="PREVIEW") {
			$accept_text = "<h1 class=centre>Processing...</h1><p class=centre>The import is being processed.<br />Please be patient.  You will be redirected back to the Support >> Import page when the processing is complete.</p>";
			$onscreen.= $accept_text;
			echo $accept_text;
		}
		$onscreen.= "<form id=save method=post action=\"$self\">";
		$onscreen.= "<input type=hidden name=act value=ACCEPT /><input type=hidden name=d value=".$dirid." />";
		$err = false;
		$file_location = $modref."/import/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$modref."/import/";
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				logImport($import_log_file, $page_title." > ERROR > $file doesn't exist", $self);
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [Too big]", $self);
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [No file found]", $self);
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"], $self);
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					logImport($import_log_file, $page_title." > ERROR > Invalid file extension - ".$ext, $self);
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
					logImport($import_log_file, $page_title." > $file uploaded", $self);
				}
			}
		}
		//READ FILE
		if(!$err) {
			$onscreen.= "<input type=hidden name=f value='".(isset($filen) ? $filen : $_REQUEST['f'])."' />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);
$import_columns[] = array();
			if(count($data)>2) {
				$doc_headings = $data[1]; //arrPrint($doc_headings);
				$doc_headings2 = $data[0];
				unset($data[0]); unset($data[1]); unset($data[2]);
				$columns[0] = array(
					"Assist",
					"Sub-Directorate [R]","","",
					"Top Layer KPI","","",
					"Function [R]","",
					"IDP Ref",
					"National Outcome [R]","",
					"IDP Objective [R]","",
					"National KPA [R]","",
					"Municipal KPA [R]","",
					"MTAS Indicator",
					"Capital Project","","",
					"KPI [R]",
					"Unit of Measurement",
					"KPI Concept [R]","",
					"KPI Type [R]","",
					"Risk Reg. Ref",
					"Risk",
					"Risk Rating [R]","",
					"Ward [R]",
					"Area [R]",
					"KPI Owner",
					"Baseline",
					"Previous Year Actual Performance",
					"Performance Standard",
					"POE",
					"KPI Calculation Type [R]","",
					"Target Type [R]","",
					" ",
					"July 2011","August 2011","September 2011","October 2011","November 2011","December 2011","January 2012","February 2012","March 2012","April 2012","May 2012","June 2012"
				);
				$columns[1] = array(
					"Ref",
					"Assist","Directorate","List",
					"Assist ref","Directorate","Top Layer KPI Name",
					"Assist","List",
					"40 characters",
					"Assist","List",
					"Assist","List",
					"Assist","List",
					"Assist","List",
					"100 characters",
					"Assist Ref","Capital Project Directorate","Capital Project Name",
					"65000 characters",
					"200 characters",
					"Assist","List",
					"Assist","List",
					"30 characters",
					"200 characters",
					"Assist","List",
					"Mun. Ref separated ;",
					"Assist ref separated by ;",
					"List",
					"100 characters",
					"100 characters",
					"100 characters",
					"200 characters",
					"Assist","List",
					"Assist","List",
					" ",
					"Number","Number","Text","Number","Number","Number","Number","Number","Number","Number","Number","Number");
				$colspanned[2] = array(7,10,12,14,16,18,20,27,29,33,47,49);
				$colspanned[3] = array(1,4,22);
				/*$cells = array(
					0=>'kpi_id',
					1=>'kpi_subid',
					4=>'kpi_topid',
					7=>'kpi_gfsid',
					9=>'kpi_idpref',
					10=>'kpi_natoutcomeid',
					12=>'kpi_idpid',
					14=>'kpi_natkpaid',
					16=>'kpi_munkpaid',
					18=>'kpi_mtas',
					19=>'kpi_capitalid',
					22=>'kpi_value',
					23=>'kpi_unit',
					24=>'kpi_conceptid',
					26=>'kpi_typeid',
					28=>'kpi_riskref',
					29=>'kpi_risk',
					30=>'kpi_riskratingid',
					32=>'wards',
					33=>'area',
					34=>'kpi_ownerid',
					35=>'kpi_baseline',
					36=>'kpi_pyp',
					37=>'kpi_perfstd',
					38=>'kpi_poe',
					39=>'kpi_calctype',
					41=>'kpi_targettype',
					44=>'target_1',
					45=>'target_2',
					46=>'target_3',
					47=>'target_4',
					48=>'target_5',
					49=>'target_6',
					50=>'target_7',
					51=>'target_8',
					52=>'target_9',
					53=>'target_10',
					54=>'target_11',
					55=>'target_12'
				);*/
				
				$listinfo = array(
					'kpi_topid'			=> array('tbl' => "top", 'other' => 6, 'dir'=>5),
					'kpi_capitalid'		=> array('tbl' => "capital", 'other' => 24, 'dir'=>23),
					'kpi_subid' 		=> array('tbl' => "subdir", 'other' => 3, 'dir'=>2),
					'kpi_gfsid' 		=> array('tbl' => "list_gfs", 'other' => 8),
					'kpi_natoutcomeid' 	=> array('tbl' => "list_natoutcome", 'other' => 11),
					'kpi_idpid' 		=> array('tbl' => "list_idp_kpi", 'other' => 13),
					'kpi_natkpaid' 		=> array('tbl' => "list_natkpa", 'other' => 15),
					'kpi_munkpaid' 		=> array('tbl' => "list_munkpa", 'other' => 17),
					'kpi_pdoid' 		=> array('tbl' => "list_pdo", 'other' => 19),
					'kpi_ndpid' 		=> array('tbl' => "list_ndp", 'other' => 21),
					'kpi_conceptid'		=> array('tbl' => "list_kpiconcept", 'other'=>28),
					'kpi_typeid'		=> array('tbl' => "list_kpitype", 'other'=>30),
					'kpi_riskratingid'	=> array('tbl' => "list_riskrating", 'other'=>34),
					'kpi_calctype'		=> array('tbl' => "list_calctype", 'other'=>48),
					'kpi_targettype'	=> array('tbl' => "list_targettype", 'other'=>50),
					'kpi_wards'			=> array('tbl' => "list_wards"),
					'kpi_area'			=> array('tbl' => "list_area"),
					'kpi_repcate'		=> array('tbl' => "list_repcate"),
				);
				$required = array(0,1,7,10,12,14,16,18,20,25,27,29,33,35,36,37,43,44,46);
				$top_refs = array();
				$top_details = array();
				$rs = getRS("SELECT top_id, top_dirid, top_value FROM ".$dbref."_top WHERE top_active = true");
				while($row = mysql_fetch_assoc($rs)) {
					$top_refs[] = $row['top_id'];
					$top_details[$row['top_id']] = $row;
				}
				$cap_refs = array();
				$cap_details = array();
				$rs = getRS("SELECT cap_id, cap_subid, cap_name FROM ".$dbref."_capital WHERE cap_active = true");
				while($row = mysql_fetch_assoc($rs)) {
					$cap_refs[] = $row['cap_id'];
					$cap_details[$row['cap_id']] = $row;
				}
				$sub_refs = array();
				$rs = getRS("SELECT id FROM ".$dbref."_subdir WHERE dirid = $dirid AND active = true");
				while($row = mysql_fetch_assoc($rs)) {
					$sub_refs[] = $row['id'];
				}
				
				$onscreen.="<table>".getImportHeadings();
//					$onscreen.="<tr><th>?</td>";
					//$onscreen.= "<tr><th rowspan=2 style='vertical-align: middle;'> ? </td>";
						/*foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							$colspan = ($cell==1 || $cell==4||$cell==19) ? 3 : $colspan;
							if(strlen($c)>0) {
								if(isset($cells[$cell])) {
									$c = $cells[$cell];
								}
								$onscreen.= "<th colspan=$colspan class=ignite>".$c."</th>";
							}
						}
					$onscreen.= "</tr><tr>";*/
						/*foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							$colspan = ($cell==1 || $cell==4||$cell==19) ? 3 : $colspan;
							if(strlen($c)>0) {
								if(isset($cells[$cell])) {
									$fld = $cells[$cell];
									if(isset($mheadings['KPI'][$fld]['h_client'])) {
										$c = $mheadings['KPI'][$fld]['h_client'];
									} elseif(isset($mheadings['KPI']['kpi_'.$fld]['h_client'])) {
										$c = $mheadings['KPI']['kpi_'.$fld]['h_client'];
									}
								}
								$onscreen.= "<th colspan=$colspan class=ignite>".$c."</th>";
							}
						}
					$onscreen.= "</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							$colspan = ($cell==1 || $cell==4||$cell==19) ? 3 : $colspan;
							if(strlen($c)>0 || $cell>42) {
								$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
							}
						}
					$onscreen.="</tr>";*/
					
					$t_sql = array();
					$r_sql = array();
					$f_sql = array();
					$refs = array();
					$db_refs = array();
					$rs = getRS("SELECT kpi_id FROM ".$dbref."_kpi");
					while($row = mysql_fetch_assoc($rs)) {
						$db_refs[] = $row['kpi_id'];
					}
					$values = array();
					$all_valid = true;
					$error_count = 0;
					$info_count = 0;
					foreach($data as $i => $d) {
						$targettype = 0;
						$valid = true;
						$row = array();
						$display = array();
						$val = array();
						foreach($cells as $c => $fld) {
							$v = $d[$c];
							$val[$c][0] = 1;
							//DATA VALIDATION HERE
							switch($fld) {
								case "kpi_id":		//IGNITE REF
									$v = $v*1;
									if(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(in_array($v,$db_refs)) {	//duplicate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Duplicate reference: KPI with Ref $v already exists");
									} elseif(in_array($v,$refs)) {	//duplicate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Duplicate reference");
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = $v;
										$refs[] = $v;
									}
									break;
								case "kpi_subid":		//DIRECTORATE
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$dir = $listinfo[$fld]['dir'];

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} elseif(!in_array($v,$sub_refs)) {			//doesn't belong to this directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Sub-Directore does not belong to the given Directorate");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
										$val[$dir] = array(1,$d[$dir]);
									}
									break;
								case "kpi_topid":
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$dir = $listinfo[$fld]['dir'];
									if(strlen(trim($v))==0) {			//required
										$row[$fld] = 0;
										$val[$other] = array(1,'');
										$val[$dir] = array(1,'');
										$val[$c][1] = '';
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!in_array($v,$top_refs)) {			//doesn't belong to this directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Top Layer KPI $v does not exist");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($top_details[$v]['top_value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$top_details[$v]['top_value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
										$val[$dir] = array(1,$d[$dir]);
									}
									break;
								case "kpi_capitalid":
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$dir = $listinfo[$fld]['dir'];
									if(strlen(trim($v))==0) {			//required
										$row[$fld] = 0;
										$val[$other] = array(1,'');
										$val[$dir] = array(1,'');
										$val[$c][1] = '';
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!in_array($v,$cap_refs)) {			//doesn't belong to this directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Capital Project $v does not exist");
									} else {
										$cap_sub = $cap_details[$v]['cap_subid'];
										if($cap_sub!=$row['kpi_subid']) {
											$valid = 0; $error_count++;
											$val[$c][0] = 0;
											$val[$c][1] = valError($v,"Capital Project Sub-Directorate does not match KPI Sub-Directorate");
										} else {
											$v2 = strtolower(trim(code($d[$other])));
											$me = strtolower($cap_details[$v]['cap_name']);
											if($v2!=$me) {	
												$val[$c][0] = 2;
												$info_count++;
											}
											$val[$c][1] = valList($v,$cap_details[$v]['cap_name']);
											$row[$fld] = $v;
											$val[$other] = array(1,$d[$other]);
											$val[$dir] = array(1,$d[$dir]);
										}
									}
									break;
								case "kpi_gfsid":
								case "kpi_natoutcomeid":
								case "kpi_natkpaid":
								case "kpi_munkpaid":
								case "kpi_pdoid":
								case "kpi_ndpid":
								case "kpi_idpid":
								case "kpi_targettype":
								case "kpi_calctype":
								case "kpi_conceptid":
								case "kpi_riskratingid":
								case "kpi_typeid":
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$targettype = ($fld=="kpi_targettype") ? 0 : $targettype;

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v) && $fld!="kpi_calctype") {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me && ($fld!="kpi_riskratingid" || (strlen(trim($v2))>0 && $v!=5) ) ) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
										$targettype = ($fld=="kpi_targettype") ? $v : $targettype;
									}
									break;
								case "kpi_idpref":
								case "kpi_mtas":
								case "kpi_value":
								case "kpi_unit":
								case "kpi_risk":
								case "kpi_baseline":
								case "kpi_pyp":
								case "kpi_perfstd":
								case "kpi_poe":
									$c_len = $mheadings[$import_section][$fld]['c_maxlen'];
									$i_len = $mheadings[$import_section][$fld]['i_maxlen'];
									$type = $mheadings[$import_section][$fld]['h_type'];
									if(($mheadings[$import_section][$fld]['i_required']+$mheadings[$import_section][$fld]['c_required'])>0 && strlen(trim(code($v))) == 0) {			//required field
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif((strlen(trim($v))> $c_len && $c_len>0) && $type!="TEXT") {			//too long
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Too long: ".strlen(trim($v))." chars (max ".$c_len." chars)");
									} elseif((strlen(trim(code($v)))> $i_len) && ($type!="TEXT" && $i_len>0)) {			//too long
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Too long: ".strlen(trim(code($v)))." chars (max ".$i_len." chars)");
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = trim(code($v));
									}
									break;
								case "kpi_repkpi": 	//reporting kpi?
									if(strtolower($v)!="y" && strtolower($v)!="n") {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data");
									} else {
										switch(strtolower($v)) {
											case "y":
												$row[$fld] = "true";
												break;
											case "n":
												$row[$fld] = "false";
												break;
										}
										$val[$c][1] = $v;
									}
									break; 
								case "kpi_ownerid":
									$v2 = strtolower(trim($v));
									if(!isset($owner[$v2])) {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data");
									} else {
										$row[$fld] = $owner[$v2];
										$val[$c][1] = valList($v,$owner[$v2]." (".$listoflists['list_owner']['data'][$owner[$v2]]['value'].")");
									}
									break;
								case "target_1":
								case "target_2":
								case "target_3":
								case "target_4":
								case "target_5":
								case "target_6":
								case "target_7":
								case "target_8":
								case "target_9":
								case "target_10":
								case "target_11":
								case "target_12":
								case "kpi_annual":
								case "kpi_revised":
									$v = trim($v);
									if(strlen($v)==0) { $v = 0; }
									if(!is_numeric($v) && strlen($v)>0) {	//invlid number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data: number only");
									} elseif($fld=="kpi_revised" && $v!=$row['kpi_annual']) {
										//$valid = 0; 
										$row[$fld] = $v;
										$info_count++;
										$val[$c][0] = 2;
										$val[$c][1] = valError($v,"Doesn't equal Annual Target");
									} else {
										if(strlen($v)==0) {
											$v = 0;
										}
										$row[$fld] = $v;
										$val[$c][1] = $v;
										if($targettype==2 && $v <=1 && $v>0) {
											$val[$c][0]=2; $info_count++;
										}
									}
									if($fld=="target_12" && $valid!=0) {
										//KPIcalcResult($values,$ct,$filter,$t)
										$calc = array(
											'target'=>array(),
											'actual'=>array()
										);
										$c_time = array();
										for($i=1;$i<12;$i++) {
											$calc['target'][$i] = $row['target_'.$i];
											$calc['actual'][$i] = 0;
											$c_time[] = $i;
										}
										$i=12;
											$calc['target'][$i] = $v;
											$calc['actual'][$i] = 0;
											$c_time[] = $i;
										$row_results = KPIcalcResult($calc,$row['kpi_calctype'],$c_time,"ALL");
										$row['overall'] = $row_results;
										if($row_results['target']!=$row['kpi_annual']) {
											$val[$c+1][0] = 2; $info_count++;
											$val[$c+1][1] = valError($row_results['target'],"Calculated Overall doesn't match given Annual Target");
										} else {
											$val[$c+1][0] = 1;
											$val[$c+1][1] = "<div class='b center ui-state-ok'>".$row_results['target']."</div>";
										}
									}
									break;
								case "kpi_wards":
								case "kpi_area":
								case "kpi_repcate":
									if(strlen(trim($v))==0) {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} else {
										$tbl = $listinfo[$fld]['tbl'];
										$row[$fld] = array();
										$val[$c][1] = "";
										$v = strtoupper($v);
										$va = explode(";",$v);
										if($fld=="kpi_wards" && in_array("ALL",$va)) {
											if(isset($wa[$fld]['ALL'])) {
												$row[$fld][] = $wa[$fld]['ALL'];
												$val[$c][1] = "All";
											} else {
												$val[$c][1] = valError("All","Not found");
												$valid = 0; $error_count++;
												$val[$c][0] = 0;
											}
										} elseif($fld=="kpi_wards" && in_array("NA",$va)) {
											if(isset($wa[$fld]['NA'])) {
												$row[$fld][] = $wa[$fld]['NA'];
												$val[$c][1] = "Unspecified";
											} else {
												$val[$c][1] = valError("Unspecified","Not found");
												$valid = 0; $error_count++;
												$val[$c][0] = 0;
											}
										} else {
											foreach($va as $a) {
												$a = trim($a);
												if(!checkIntRef($a)) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Invalid reference - number only");
												} elseif(!isset($a,$wa[$fld][$a])) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Reference not found");
												} elseif(!checkIntRef($wa[$fld][$a])) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Invalid Ignite Reference found");
												} else {
													$row[$fld][] = $wa[$fld][$a];
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").$a." => ".$listoflists[$tbl]['data'][$wa[$fld][$a]]['value'];
												}
											}
										}
									}
									break;
								default:
									$val[$c][1] = code($v);
									$row[$fld] = code($v);
							}
						}
						$val['a'] = $valid;
						$onscreen.= "<tr>";
							switch($val['a']) {
								case true:
									$onscreen.= "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									break;
								case false:
								default:
									$onscreen.= "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
							}
							
							foreach($import_columns as $cell => $c) {
								if(isset($val[$cell])) {
									$v = $val[$cell];
									switch($v[0]) {
										case 0:
											$onscreen.= "<td class=\"dark-error\">";
											break;
										case 2:
											$onscreen.= "<td class=\"dark-info\">";
											break;
										case 1:
										default:
											$onscreen.= "<td>";
									}
									$onscreen.= $val[$cell][1];
									$onscreen.= "</td>";
								} else {
									$onscreen.= "<td></td>";
								}
							}
						$onscreen.= "</tr>";
						if($valid) {
							$values[$i] = $val;
							//INSERT INTO $dbref_kpi (kpi_id,kpi_subid,kpi_topid,kpi_gfsid,kpi_idpref,kpi_natoutcomeid,kpi_idpid,kpi_natkpaid,kpi_munkpaid,kpi_mtas,kpi_capitalid,kpi_value,kpi_unit,kpi_conceptid,kpi_typeid,kpi_riskref,kpi_risk,kpi_riskratingid,kpi_ownerid,kpi_baseline,kpi_pyp,kpi_perfstd,kpi_poe,kpi_calctype,kpi_targettype,kpi_active,kpi_pdoid,kpi_ndpid,kpi_repcate) VALUES ...
							$t_sql[] = "(".$row['kpi_id'].",".$row['kpi_subid'].",".$row['kpi_topid'].",".$row['kpi_gfsid'].",'".$row['kpi_idpref']."',".$row['kpi_natoutcomeid'].",".$row['kpi_idpid'].",".$row['kpi_natkpaid'].",".$row['kpi_munkpaid'].",'".$row['kpi_mtas']."',".$row['kpi_capitalid'].",'".$row['kpi_value']."','".$row['kpi_unit']."',".$row['kpi_conceptid'].",".$row['kpi_typeid'].",'".$row['kpi_riskref']."','".$row['kpi_risk']."',".$row['kpi_riskratingid'].",".$row['kpi_ownerid'].",'".$row['kpi_baseline']."','".$row['kpi_pyp']."','".$row['kpi_perfstd']."','".$row['kpi_poe']."','".$row['kpi_calctype']."',".$row['kpi_targettype'].",true,".$row['kpi_pdoid'].",".$row['kpi_ndpid'].",';".implode(";",$row['kpi_repcate']).";',".$row['kpi_annual'].",".$row['kpi_revised'].",'".$row['kpi_text1']."','".$row['kpi_text2']."','".$row['kpi_text3']."')";
							//INSERT INTO $dbref_kpi_results (tr_id, tr_topid, tr_timeid, tr_target, tr_actual, tr_perf, tr_correct, tr_dept) VALUES ...
							$ressql = "INSERT INTO ".$dbref."_kpi_results (kr_id, kr_kpiid, kr_timeid, kr_target, kr_actual) VALUES (null,".$row['kpi_id'].",1,".$row['target_1'].",0)";
							for($t=2;$t<=12;$t++) {
								$ressql.= ", (null,".$row['kpi_id'].",$t,".$row['target_'.$t].",0)";
							}
							$r_sql[] = $ressql;
							//INSERT INTO ".$dbref."_kpi_wards (tw_id, tw_topid, tw_listid, tw_active) VALUES...
							$r_sql[] = "INSERT INTO ".$dbref."_kpi_wards (kw_id, kw_kpiid, kw_listid, kw_active) VALUES	(null,".$row['kpi_id'].",".implode(",true),(null,".$row['kpi_id'].",",$row['kpi_wards']).",true)";
							//INSERT INTO ".$dbref."_kpi_area (tw_id, tw_topid, tw_listid, tw_active) VALUES...
							$r_sql[] = "INSERT INTO ".$dbref."_kpi_area (ka_id, ka_kpiid, ka_listid, ka_active) VALUES	(null,".$row['kpi_id'].",".implode(",true),(null,".$row['kpi_id'].",",$row['kpi_area']).",true)";
						} else {
							$all_valid = false;
						}
					}
					
					
				$onscreen.="</table>";
				//arrPrint($r_sql);
				if($act!="ACCEPT") {
					$onscreen.= "<p class=centre>There are $error_count fatal error(s).<br />There are $info_count non-fatal error(s).</p>";
					$onscreen.= "<p class=centre><input type=submit value=\"Accept\" class=isubmit id=submit_me /> <input type=button id=reset_me class=idelete value=Reject class=ireset /><p>";
					if(!$all_valid || $error_count > 0) {
						$onscreen.= "<script>$(function() {
							$('#submit_me').prop('disabled',true);
							$('#submit_me').removeClass('isubmit');
							$('#reset_me').click(function() { document.location.href = 'support_import_kpi.php'; });
						});</script>";
					}
					//echo "<p class=centre>Warning: When you click the Accept button, only those list items with a <span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> will be imported.</p>";
					echo $onscreen;
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
				} else {
					$z_sql = array();
					$z=0;
					$z_sql[$z] = "INSERT INTO ".$dbref."_kpi (kpi_id,kpi_subid,kpi_topid,kpi_gfsid,kpi_idpref,kpi_natoutcomeid,kpi_idpid,kpi_natkpaid,kpi_munkpaid,kpi_mtas,kpi_capitalid,kpi_value,kpi_unit,kpi_conceptid,kpi_typeid,kpi_riskref,kpi_risk,kpi_riskratingid,kpi_ownerid,kpi_baseline,kpi_pyp,kpi_perfstd,kpi_poe,kpi_calctype,kpi_targettype,kpi_active,kpi_pdoid,kpi_ndpid,kpi_repcate,kpi_annual,kpi_revised,kpi_text1,kpi_text2,kpi_text3) VALUES ";
					$a = 0;
					foreach($t_sql as $t) {
						if($a>=50) {
							$z++;
							$z_sql[$z] = "INSERT INTO ".$dbref."_kpi (kpi_id,kpi_subid,kpi_topid,kpi_gfsid,kpi_idpref,kpi_natoutcomeid,kpi_idpid,kpi_natkpaid,kpi_munkpaid,kpi_mtas,kpi_capitalid,kpi_value,kpi_unit,kpi_conceptid,kpi_typeid,kpi_riskref,kpi_risk,kpi_riskratingid,kpi_ownerid,kpi_baseline,kpi_pyp,kpi_perfstd,kpi_poe,kpi_calctype,kpi_targettype,kpi_active,kpi_pdoid,kpi_ndpid,kpi_repcate,kpi_annual,kpi_revised,kpi_text1,kpi_text2,kpi_text3) VALUES ";
							$a=0;
						} elseif($a>0) {
							$z_sql[$z].=",";
						}
						$z_sql[$z].=$t;
						$a++;
					}
					foreach($z_sql as $key => $sql) {
						//echo "<P>".$sql;
						$id = db_insert($sql);
						//echo "   ::   ".$id;
						$onscreen.= "<P>".$sql;
					}
					foreach($r_sql as $key => $sql) {
						//echo "<P>".$sql;
						$id = db_insert($sql);
						//echo "   ::   ".$id;
						$onscreen.= "<P>".$sql;
					}
					//db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = '$import_section'");
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
					echo "<script>document.location.href = 'support_import_kpi.php?r[]=ok&r[]=Departmental+SDBIP+imported.';</script>";
				}
				echo "</form>";
			} else {	//data count
				logImport($import_log_file, $page_title." > ERROR > No info to upload [".$file."]", $self);
				die(displayResult(array("error","Couldn't find any information to import.")));
			}
			
		} else {	//if !err
			die(displayResult($file_error));
		}
		break;	//switch act case accept
}	//switch act
?>

</body>
</html>