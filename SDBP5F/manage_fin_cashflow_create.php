<?php
global $helperObj;
$get_open_time = false;
$time = getTime($get_time,$get_open_time);

if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
//arrPrint($_REQUEST);


	
	
	
	
	JSdisplayResultPrep("Processing...");
	//echo "<table><tr><th>Fld</th><th>Type</th><th>New</th><th>Old</th><th>Action</th></tr>";
	$updates = array();
	$trans = array();
	//$changes = array();
	$changes[] = $table_fld."active = true";
	$sql = array();
	$extras_table_fld = array("WARDS"=>"cw","AREA"=>"ca","FUNDSRC"=>"cs");
    $object = isset($object) ? $object : array();
	foreach($mheadings[$section] as $fld => $h) {
		$log_trans = "";
		switch($h['h_type']) {
			case "DATE":
				$new = isset($_REQUEST[$fld]) ? strtotime($_REQUEST[$fld]) : "";
				if($helperObj->checkIntRef($new)) {
					if(strpos($fld,"end")>0) { $new+=86400-1; }
					if($new!=$old) {
						$log_trans = addslashes("Edited ".$h['h_client']." to '".date("d M Y",$new).(($helperObj->checkIntRef($old)) ? "' from '".date("d M Y",$old) : "")."'");
						$trans[$fld]['lt'] = $log_trans;
						$trans[$fld]['n'] = $new;
						$trans[$fld]['o'] = isset($old) ? $old : "";
						$changes[] = $fld." = '".$new."'";
					}
				} else {$changes[] = $fld." = '0'";}
				break;
			case "LIST":
				$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
                $object_fld = isset($object[$fld]) ? $object[$fld] : "";
					$log_trans = addslashes("Set ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."' from '".$object_fld."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = isset($old) ? $old : "";
					$changes[] = $fld." = '".$new."'";
				break;
			default:
				$new = isset($_REQUEST[$fld]) ? $helperObj->code($_REQUEST[$fld]) : "";
					$log_trans = addslashes("Set ".$h['h_client']." to '".$new."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = isset($old) ? $old : "";
					$changes[] = $fld." = '".$new."'";
				break;
		}
		/*echo "<tr>
		<th>".$fld."</th>
		<td>".$h['h_type']."</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	if(count($changes)>0) {
		$sb = "INSERT INTO ".$dbref."_".$table_tbl." SET ".implode(", ",$changes); //." WHERE ".$table_fld."id = ".$obj_id;
		$obj_id = $helperObj->db_insert($sb);
		//echo "<P>".$sb;
		//$obj_id = 1;
		if($helperObj->checkIntRef($obj_id)) {
			$ss = "UPDATE ".$dbref."_".$table_tbl." SET cf_sort = ".$obj_id." WHERE cf_id = ".$obj_id;
            $helperObj->db_update($ss);
			foreach($trans as $fld => $lt) {
				$v = array(
					'fld'=>$fld,
					'timeid'=>0,
					'text'=>$lt['lt'],
					'old'=>$lt['o'],
					'new'=>$lt['n'],
					'act'=>"C",
					'YN'=>"N"
				);
				logChanges($section,$obj_id,$v,$helperObj->code($sb));
			}
							$v = array(
								'fld'=>"cf_id",
								'timeid'=>0,
								'text'=>"Created Monthly Cashflow Line Item ".$id_labels_all[$section].$obj_id.".",
								'old'=>0,
								'new'=>0,
								'act'=>"C",
								'YN'=>"Y"
							);
							logChanges($section,$obj_id,$v,$helperObj->code($sb));
			$total = array();
			$results = array();
			$first_head = $mheadings['CF_H'];
			$fields = array(
				'original'=>"_1",
				'adj'=>"_4",
				'ytd'=>"_8",
				'total'=>"_12"
			);
			foreach($_REQUEST['ti'] as $ti) {
				$results[$ti] = array();
				foreach($first_head as $fldh => $f) {
					if(!isset($total[$fldh])) { $total[$fldh] = 0; }
					$r_fld = $r_table_fld.$fldh.$fields['original'];
					$target = (isset($_REQUEST[$r_fld][$ti]) && $helperObj->checkIntRef($_REQUEST[$r_fld][$ti])) ? $_REQUEST[$r_fld][$ti] : 0;
					$total[$fldh] += $target;
					$results[$ti][$fldh]['original'] = $target;
					$results[$ti][$fldh]['adj'] = $target;
					$results[$ti][$fldh]['ytd'] = $total[$fldh];
				}
			}
			$r_sql = array();
			foreach($results as $ti => $r) {
				$txt = "";
				foreach($first_head as $fldh => $fh) {
					foreach($fields as $fl => $f) {
						$txt.=",".($fl=="total" ? $total[$fldh] : $r[$fldh][$fl]);
					}
				}
				$r_sql[] = "(".$obj_id.",$ti".$txt.")";
			}
			$sd = "INSERT INTO ".$dbref."_".$table_tbl."_results (cr_cfid,cr_timeid";
				foreach($first_head as $fldh => $fh) {
					foreach($fields as $fl => $f) {
						$sd.=",cr_".$fldh.$f;
					}
				}
			$sd.= ") VALUES ".implode(",",$r_sql);
			//arrPrint($r_sql);
			//echo "<P>".$sd;
            $helperObj->db_insert($sd);
							$v = array(
								'fld'=>$fld_target,
								'timeid'=>0,
								'text'=>"Set budget.",
								'old'=>0,
								'new'=>0,
								'act'=>"C",
								'YN'=>"N"
							);
							logChanges($section,$obj_id,$v,$helperObj->code($sd));
			//echo "</table>";
			echo "<script>JSdisplayResult('ok','ok','Line Item ".$id_labels_all[$section].$obj_id." successfully created.');</script>";
		} else {	//checkintref obj_id
			echo "<script>JSdisplayResult('error','error','An error occurred while trying to create the new Line Item.  Please try again.');</script>";
		}
	} else {	//count changes > 0
		echo "<script>JSdisplayResult('error','error','An error occurred while trying to create the new Line Item.  Please try again.');</script>";
	}
}

//if(isset($result)) { displayResult($result); }

$obj_id = "";
include("manage_fin_table_detail.php");

?>