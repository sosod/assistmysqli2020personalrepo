<?php 
$page_title = "Time Periods";
$log_section = "TIME";
include("inc/header.php"); 



$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "";
$flds = array("finance");

/*** DATABASE ACTIONS ***/
if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
	case "SAVE":
		switch($page_id) {
			case "all":
				$tot = 0;
				$days['rem_finance'] = (isset($_REQUEST['rem_finance_days']) && is_numeric($_REQUEST['rem_finance_days']) ) ? $_REQUEST['rem_finance_days'] : 7;
				$days['close_finance'] = (isset($_REQUEST['close_finance_days']) && is_numeric($_REQUEST['close_finance_days']) ) ? $_REQUEST['close_finance_days'] : 14;
				foreach($time as $t) {
					$old = $t;
					$id = $t['id'];
					$rem_finance = 0;
					$close_finance = 0;
					if($t['active_finance']) {
						$rem_finance = $t['end_stamp'] + ( ($days['rem_finance']) * 86400 )-86399;//last day + number of days * seconds in a day - seconds to go back to midnight from 23:59:59
						$close_finance = $t['end_stamp'] + ( ($days['close_finance']) * 86400 );
						$rem_finance = ($rem_finance > $today) ? $rem_finance : 0;
						$close_finance = ($close_finance > $today) ? $close_finance : 0;
					}
					$sql = "UPDATE ".$dbref."_list_time SET rem_finance = ".$rem_finance.", close_finance = ".$close_finance." WHERE id = ".$t['id'];
					$mar = db_update($sql);
					$tot += $mar;
					if($mar > 0) {
						$log_change = array();
						if($t['active_finance']) {
							$f = "finance";
							if($rem_finance!=$old['rem_'.$f]) {
								$log_text = "";
								if($rem_finance>0) {
									if($old['rem_'.$f]>0) {
										$log_text = " - Changed reminder date to <i>".date("d M Y",$rem_finance)."</i> (from: <i>".date("d M Y",$old['rem_'.$f])."</i>) for ".$f." users.";
									} else {
										$log_text = " - Added reminder date <i>".date("d M Y",$rem_finance)."</i> for ".$f." users.";
									}
								} else {
									$log_text = " - Removed reminder date for ".$f." users."; 
								}
								$log_change[] = $log_text;
							}
							if($close_finance!=$old['close_'.$f]) {
								$log_text = "";
								if($close_finance>0) {
									if($old['close_'.$f]>0) {
										$log_text = " - Changed closure date to <i>".date("d M Y",$close_finance)."</i> (from: <i>".date("d M Y",$old['close_'.$f])."</i>) for ".$f." users.";
									} else {
										$log_text = " - Added closure date <i>".date("d M Y",$close_finance)."</i> for ".$f." users.";
									}
								} else {
									$log_text = " - Removed closure date for ".$f." users."; 
								}
								$log_change[] = $log_text;
							}
						}

										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"",
											'text'=>"Updated time period ".$id." (Update All function):<br />".implode("<br />",$log_change),
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					}
				}
				$page_id = "";
				if($tot>0) {
					$result = array("ok","Time Periods Automatic dates updated.");
				} else {
					$result = array("info","No change was found to be made.");
				}
				break;
			case "update":
				if(isset($_REQUEST['i']) && ASSIST_HELPER::checkIntRef($_REQUEST['i'])) {
					$change = array();
					$log_change = array();
					$id = $_REQUEST['i'];
					$old = $time[$id];
					foreach($flds as $f) {
						$rem = (isset($_REQUEST['rem_'.$f]) && strlen($_REQUEST['rem_'.$f])>0) ? strtotime($_REQUEST['rem_'.$f]." 00:00:00") : 0;
						if($rem < $today) { $rem = 0; }
						if($rem!=$old['rem_'.$f]) { 
							$change[] = "rem_".$f." = ".$rem; 
							$log_text = "";
							if($rem>0) {
								if($old['rem_'.$f]>0) {
									$log_text = " - Changed reminder date to <i>".date("d M Y",$rem)."</i> (from: <i>".date("d M Y",$old['rem_'.$f])."</i>) for ".$f." users.";
								} else {
									$log_text = " - Added reminder date <i>".date("d M Y",$rem)."</i> for ".$f." users.";
								}
							} else {
								$log_text = " - Removed reminder date for ".$f." users."; 
							}
							$log_change[] = $log_text;
						}
						$close = (isset($_REQUEST['close_'.$f]) && strlen($_REQUEST['close_'.$f])>0) ? strtotime($_REQUEST['close_'.$f]." 23:59:59") : 0;
						if($close < $today) { $close = 0; }
						if($close!=$old['close_'.$f]) { 
							$change[] = "close_".$f." = ".$close; 
							$log_text = "";
							if($close>0) {
								if($old['close_'.$f]>0) {
									$log_text = " - Changed closure date to <i>".date("d M Y",$close)."</i>".( ($old['close_'.$f]>0) ? " (from: <i>".date("d M Y",$old['close_'.$f])."</i>)" : "")." for ".$f." users.";
								} else {
									$log_text = " - Added closure date <i>".date("d M Y",$close)."</i> for ".$f." users.";
								}
							} else {
								$log_text = " - Removed closure date for ".$f." users."; 
							}
							$log_change[] = $log_text;
						}
					}
					if(count($change)>0) {
						$sql = "UPDATE ".$dbref."_list_time SET ".implode(", ",$change)." WHERE id = ".$id;
						$mar = db_update($sql);
						if($mar > 0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"",
											'text'=>"Updated time period ".$id.":<br />".implode("<br />",$log_change),
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
							$result = array("ok","Time period ".$id." updated.");
						} else {	
							$result = array("info","No change was found to be made.");
						}
					}
				} else {
					$result = array("error","An error occurred while trying to perform an update.  Please go back and try again.");
				}
				$page_id = "";
				break;
			default:
				break;
		}
		break;
	case "CLOSE":
		if(isset($_REQUEST['fld']) && isset($_REQUEST['i']) && ASSIST_HELPER::checkIntRef($_REQUEST['i'])) {
			$fld = $_REQUEST['fld'];
			$id = $_REQUEST['i'];
			$sql = "UPDATE ".$dbref."_list_time SET rem_".$fld." = 0, close_".$fld." = 0, active_".$fld." = false WHERE id = $id";
			$mar = db_update($sql);
			if($mar > 0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"active_".$fld,
											'text'=>"Closed time period ".$id." for ".$fld." users.",
											'old'=>"true",
											'new'=>"false",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
				$result = array("ok","Time period ".$id." has been closed for ".$fld." users.");
			} else {
				$result = array("info","No change was found to be made.");
			}
		} else {
			$result = array("error","An error occurred.  Please go back and try again.");
		}
		break;
	}
	$time = getTime($get_time, $get_open_time);
}




/*** ONSCREEN ****/
switch($page_id) {
	case "all":
		$days['rem_finance'] = (isset($_REQUEST['rem_finance_days']) && is_numeric($_REQUEST['rem_finance_days']) ) ? $_REQUEST['rem_finance_days'] : 7;
		$days['close_finance'] = (isset($_REQUEST['close_finance_days']) && is_numeric($_REQUEST['close_finance_days']) ) ? $_REQUEST['close_finance_days'] : 14;
		echo "<h2>Update All Automatic Dates</h2>";
		echo "<form name=time id=time method=post action=setup_defaults_time.php?page_id=all >";
		echo "<input type=hidden name=page_id value=all />
				<input type=hidden name=act value=PREVIEW id=act />";
		foreach($flds as $f) {
//			if($f!="finance") {
				echo "<h3>".ucfirst($f)."</h3>
					<table>
						<tr>
							<th class=left>Email Reminder:</th>
							<td><input type=text name=rem_".$f."_days size=5 value=".$days['rem_'.$f]." /> days after the last day of the month.</td>
						</tr>
						<tr>
							<th class=left>Auto Closure:</th>
							<td><input type=text name=close_".$f."_days size=5 value=".$days['close_'.$f]." /> days after the last day of the month.</td>
						</tr>
					</table>";
//			}
		}
		echo "<p><input type=button value=Save id=save /> <input type=button value=Preview id=preview /> <input type=button value=Reset id=reset /></p>";
		echo "</form>";
		displayGoBack("setup_defaults_time.php");
		?>
		<script>
			$(function() {
				$("#save").click(function() {
					$("#act").val("SAVE");
					$("#time").submit();
				});
				$("#preview").click(function() {
					$("#act").val("PREVIEW");
					$("#time").submit();
				});
				$("#reset").click(function() {
					document.location.href = 'setup_defaults_time.php?page_id=all';
				});
			});
		</script>
		<?php
		if(isset($_REQUEST['act']) && $_REQUEST['act']=="PREVIEW") {
			echo "<h2>Preview</h2>";
			?>
			<table>
				<tr>
					<th rowspan=2>Ref</th>
					<th rowspan=2>Time Period</th>
<!--					<th colspan=3>Primary</th>-->
<!--					<th colspan=3>Secondary</th>-->
					<th colspan=3>Finance</th>
				</tr>
				<tr>
<!--					<th>Status</th>-->
<!--					<th>Reminder</th>-->
<!--					<th>Closure</th>-->
<!--					<th>Status</th>-->
<!--					<th>Reminder</th>-->
<!--					<th>Closure</th>-->
					<th>Status</th>
					<th>Reminder</th>
					<th>Closure</th>
				</tr>
				<?php
			foreach($time as $t) {
				echo "<tr>";
					echo "<th>".$t['id']."</th>";
					echo "<td>Month ending ".date("d F Y",$t['end_stamp'])."</td>";
					foreach($flds as $fld) {
						echo "<td class=centre>".($t['active_'.$fld] ? "<div class=ui-state-ok>Open</div>" : "<div class=ui-state-error>Closed</div>")."</td>";
						//if($fld!="finance") {
							$dates = array("rem","close");
							foreach($dates as $d) {
								$a = $d."_".$fld;
								$new = $t['end_stamp'] + (($days[$a]) * 86400) + ($d=="close" ? 0 : 0);
								//if($new < $today+86400) { $new = 0; }
								$diff = $new-$today;
								$week = 86400*7;
								$div = ($diff < $week && $diff > 86400 && $t['active_'.$fld]) ? "<div class=ui-state-info>" : "";
								echo "<td class=centre>".$div.(($new>($today+86400) && $t['active_'.$fld]) ? date("d M 'y",$new)." <small><i>(".date("D",$new).")</i></small>" : "")."</div></td>";
							}
//						} else {
//							$dates = array("rem","close");
//							foreach($dates as $d) {
//								$a = $d."_".$fld;
//								$diff = $t[$a]-$today;
//								$week = 86400*7;
//								$div = ($diff < $week && $diff > 0) ? "<div class=ui-state-info>" : "";
//								echo "<td class=centre>".$div.(($t[$a]>$today && $t['active_'.$fld]) ? date("d M 'y",$t[$a])." <small><i>(".date("D",$t[$a]).")</i></small>" : "")."</div></td>";
//							}
//						}
					}
				echo "</tr>";
			}
				?>
			</table>			
			<?php
			displayGoBack("setup_defaults_time.php");
		}
		break;
	
	
	case "update":
		$id = $_REQUEST['i'];
		$t = $time[$id];
		echo "<h2>Month Ending ".date("d F Y",$t['end_stamp'])."</h2>";
		
		?>
		<style type=text/css>
		table th { border-color: #ababab; }
		table th.th2 { border-color: #ababab; }
		</style>
		<form id=update action=setup_defaults_time.php method=post >
		<input type=hidden name=act value=SAVE />
		<input type=hidden name=page_id value=update />
		<input type=hidden name=i value=<?php echo $id; ?> />
		<table width=300>
		<?php
		foreach($flds as $f) {
			?>
				<tr>
					<th class=left style="font-size: 10pt; line-height: 15pt;" colspan=2><?php echo ucfirst($f).($t['active_'.$f] ? "<input type=button value=Close class=\"float ireset\" id=".$f." />" : ""); ?></th>
				</tr>
				<tr>
					<th class="left th2" width=100>&nbsp;&nbsp;Status:&nbsp;&nbsp;</th>
					<td><?php echo (($t['active_'.$f]) ? "<div class=\"ui-state-ok\">&nbsp;Open </div> " : "<div class=\"ui-state-error\">&nbsp;Closed </div>" ); ?></td>
				</tr>
				<?php if($t['active_'.$f]) { ?>
				<tr>
					<th class="left th2">&nbsp;&nbsp;Reminder:&nbsp;&nbsp;</th>
					<td>
						<input type=text class=jdate2012 size=10 name="rem_<?php echo $f; ?>" id="rem_<?php echo $f; ?>" value="<?php if($t['rem_'.$f]>$today && $t['active_'.$f]) { echo date("d-M-Y",$t['rem_'.$f]); } ?>" />
						<input type=button value=Clear class=clear id=r_<?php echo $f; ?> />
					</td>
				</tr>
				<tr>
					<th class="left th2">&nbsp;&nbsp;Closure:&nbsp;&nbsp;</th>
					<td>
						<input type=text class=jdate2012 size=10 name="close_<?php echo $f; ?>" id="close_<?php echo $f; ?>" value="<?php if($t['close_'.$f]>$today && $t['active_'.$f]) { echo date("d-M-Y",$t['close_'.$f]); } ?>" />
						<input type=button value=Clear class=clear id=c_<?php echo $f; ?> />
					</td>
				</tr>
				<?php } ?>
			<?php 
		}
		?>
		</table>
		<table width=300 style="margin-top: 10px;">
			<tr>
				<td class="blank centre"><input type=button value="Save Changes" class=isubmit /> <span class=float><input type=reset value=Reset /></span></td>
			</tr>
		</table>
	</form>
		<script>
			$(function() {
				$(".jdate2012").datepicker("option","minDate","+<?php echo $today<$t['start_stamp'] ? floor(($t['start_stamp']-$today)/86400) : "1"; ?>").prop("size",12);
				$('#ui-datepicker-div').css('clip', 'auto'); 
				$(".ireset").click(function() {
					if(confirm("Are you sure that you wish to close this time period for "+$(this).prop("id")+" users?")) {
						document.location.href = '<?php echo $self; ?>?act=CLOSE&i=<?php echo $id; ?>&fld='+$(this).prop("id");
					}
				});
				$(".clear").click(function() {
					var id = $(this).prop("id");
					var start = id.substr(0,1);
					var fld = "";
					if(start=="r") { fld = "rem_"; } else { fld = "close_"; }
					var end = id.substr(2,id.length);
					fld = "#"+fld+end;
					$(fld).val("");
				});
				$(".isubmit").click(function() {
					var submit = true;
					var rem = "";
					var close = "";
					var err = "";
					var fld = "primary";
					var today = new Date();
					if($("#rem_"+fld).length && $("#close_"+fld).length) {
						rem = $("#rem_"+fld).val();
						close = $("#close_"+fld).val();
						if(rem.length > 0 && close.length > 0) {
							var rDate = new Date(rem.replace("-"," "));
							if(rDate<=today) {
								submit = false;
								err = err + "\n - The reminder date must fall after today ("+fld+" users).";
							}
							var cDate = new Date(close.replace("-"," "));
							if(cDate<=today) {
								submit = false;
								err = err + "\n - The closure date must fall after today ("+fld+" users).";
							}
							if(!(rDate < cDate)) {
								submit = false;
								err = err + "\n - The closure date must fall after the reminder date ("+fld+" users).";
							}
							delete rDate;
							delete cDate;
						} else {
							if(rem.length > 0 && close.length ==0) {
								submit = false;
								err = err + "\n - Please enter a closure date to go with the reminder date ("+fld+" users).";
							}
						}
					}
					fld = "secondary";
					if($("#rem_"+fld).length && $("#close_"+fld).length) {
						rem = $("#rem_"+fld).val();
						close = $("#close_"+fld).val();
						if(rem.length > 0 && close.length > 0) {
							var rDate = new Date(rem.replace("-"," "));
							if(rDate<=today) {
								submit = false;
								err = err + "\n - The reminder date must fall after today ("+fld+" users).";
							}
							var cDate = new Date(close.replace("-"," "));
							if(cDate<=today) {
								submit = false;
								err = err + "\n - The closure date must fall after today ("+fld+" users).";
							}
							if(!(rDate < cDate)) {
								submit = false;
								err = err + "\n - The closure date must fall after the reminder date ("+fld+" users).";
							}
							delete rDate;
							delete cDate;
						} else {
							if(rem.length > 0 && close.length ==0) {
								submit = false;
								err = err + "\n - Please enter a closure date to go with the reminder date ("+fld+" users).";
							}
						}
					}
					fld = "finance";
					if($("#rem_"+fld).length && $("#close_"+fld).length) {
						rem = $("#rem_"+fld).val();
						close = $("#close_"+fld).val();
						if(rem.length > 0 && close.length > 0) {
							var rDate = new Date(rem.replace("-"," "));
							if(rDate<=today) {
								submit = false;
								err = err + "\n - The reminder date must fall after today ("+fld+" users).";
							}
							var cDate = new Date(close.replace("-"," "));
							if(cDate<=today) {
								submit = false;
								err = err + "\n - The closure date must fall after today ("+fld+" users).";
							}
							if(!(rDate < cDate)) {
								submit = false;
								err = err + "\n - The closure date must fall after the reminder date ("+fld+" users).";
							}
							delete rDate;
							delete cDate;
						} else {
							if(rem.length > 0 && close.length ==0) {
								submit = false;
								err = err + "\n - Please enter a closure date to go with the reminder date ("+fld+" users).";
							}
						}
					}
					if(!submit) {
						alert("The following errors occurred:"+err);
					} else {
						$("#update").submit();
					}
				});
			});
		</script>
		<?php
		displayGoBack("setup_defaults_time.php");
			$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_ref = '".$id."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
			displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
		break;
	
	
	default:
		displayResult($result);
		?>
		<table style="margin-top: -10px;" class=noborder><tr class="no-highlight"><td class="noborder">
		<p class=float>Automatic dates which fall within the next 7 days are <span class=ui-state-info>highlighted</span>.</p>
		<p><input type=button value="Update All Automatic Dates" id=all /></p>
		<table>
			<tr>
				<th rowspan=2>Ref</th>
				<th rowspan=2>Time Period</th>
<!--				<th colspan=3>Primary</th>-->
<!--				<th colspan=3>Secondary</th>-->
				<th colspan=3>Finance</th>
				<th rowspan=2>&nbsp;</th>
			</tr>
			<tr>
<!--				<th>Status</th>-->
<!--				<th>Reminder</th>-->
<!--				<th>Closure</th>-->
<!--				<th>Status</th>-->
<!--				<th>Reminder</th>-->
<!--				<th>Closure</th>-->
				<th>Status</th>
				<th>Reminder</th>
				<th>Closure</th>
			</tr>
			<?php
		foreach($time as $t) {
			$open = 0;
			echo "<tr>";
				echo "<th>".$t['id']."</th>";
				echo "<td>Month ending ".date("d F Y",$t['end_stamp'])."</td>";
				foreach($flds as $fld) {
					$open+=$t['active_'.$fld];
					echo "<td class=centre>".($t['active_'.$fld] ? "<div class=ui-state-ok>Open</div>" : "<div class=ui-state-error>Closed</div>")."</td>";
					$dates = array("rem","close");
					foreach($dates as $d) {
						$a = $d."_".$fld;
						$diff = $t[$a]-$today;
						$week = 86400*7;
						$div = ($diff < $week && $diff > 0) ? "<div class=ui-state-info>" : "";
						echo "<td class=centre>".$div.(($t[$a]>$today && $t['active_'.$fld]) ? date("d M 'y",$t[$a])." <small><i>(".date("D",$t[$a]).")</i></small>" : "")."</div></td>";
					}
				}
				echo "<td>";
					if($open>0) { echo "<input type=button value=Update class=update id=".$t['id']." />"; }
				echo "</td>";
			echo "</tr>";
		}
			?>
		</table>
		</td></tr></table>
		<script>
			$(function() {
				$("#all").click(function() {
					document.location.href = 'setup_defaults_time.php?page_id=all';
				});
				$(".update:button").click(function() {
					var i = $(this).prop("id");
					document.location.href = 'setup_defaults_time.php?page_id=update&i='+i;
				});
			});
		</script>
		<?php
			displayGoBack("setup.php","Back to Setup");
			$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
			displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
		break;
} ?>
</body>
</html>