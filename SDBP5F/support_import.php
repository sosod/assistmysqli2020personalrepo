<?php 
$ait_ip = "196.210.230.125a";
$my_ip = $_SERVER["REMOTE_ADDR"];

include("inc/header.php"); 



$import_log_file = openLogImport();
//fdata = "date("Y-m-d H:i:s")","$_SERVER['REMOTE_ADDR']","action","$self","implode(",",$_REQUEST)"
logImport($import_log_file, "Open Support > Import", $self);
?>
<script>
$(function() {
	$(":button").click(function() {
		var id = $(this).attr("id");
		if(id=="list") {
			var t = $("#tbl").attr("value");
			var url = "support_import_list.php?t="+t;
		} else {
			var url = "support_import_"+id+".php";
		}
		document.location.href = url;
	});
});
</script>
<?php
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
displayResult($result);
?>
<p>Please note that the various sections of the SDBIP must be loaded in the order in which they appear:
	<ul>
		<li>All list items must be loaded before Capital Projects & Monthly Cashflows can be loaded.  These must be loaded manually via the Setup > Defaults > Lists section</li>
	</ul>
</p>
<table>
	<tr>
		<th class=left>Lists:&nbsp;</th>
		<td>Lists already loaded.&nbsp;&nbsp;<span class=float><input type=hidden id=tbl value=all /><input type=button value="  Go  " id=def /></span></td>
	</tr>

	<tr>
		<th class=left>Capital Projects:&nbsp;</th>
		<?php
			if(!$import_status['CAP']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
			echo "<span class=float><input type=button value=\"  Go  \" id=cap /></td>";
		?>
	</tr>
	<tr>
		<th class=left>Monthly Cashflow:&nbsp;</th>
		<?php
			if(!$import_status['CF']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
			echo "<span class=float><input type=button value=\"  Go  \" id=cf /></td>";
		?>
	</tr>
	<tr>
		<th class=left>Revenue By Source:&nbsp;</th>
		<?php
			if(!$import_status['RS']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
			?>&nbsp;&nbsp;<span class=float><input type=button value="  Go  " id=rs /></span>
		</td>
	</tr>
	<tr>
		<th class=left>Import Log:&nbsp;</th>
		<td>&nbsp;&nbsp;<span class=float><input type=button value="  View  " id=log /></span>
		</td>
	</tr>
</table>
<?php
fclose($import_log_file);

?>
</body>
</html>