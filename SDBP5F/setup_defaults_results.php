<?php
$page_title = "Module Setup";
$log_section = "RES";
include("inc/header.php");

/* Get settings from $dbref_setup_results table */
$result_settings = $me->getCustomResultSettingsDirectlyFromDatabase();
$default_result_settings = $me->getDefaultResultSettings();
foreach($default_result_settings as $i => $drs) {
	if($i>0) {
		$default_result_settings[$i]['limit_int'].= "%";
	} else {
		$default_result_settings[$i]['limit_int'] = "N/A";
	}
}
$result_settings_headings = $me->getResultSettingsHeadings();

/* set some properties */
$result_setting_management_properties = $me->getResultSettingsManagementProperties();
$settings_with_limit_int = $result_setting_management_properties['settings_with_limit_int'];
$settings_with_linked_limit_int = $result_setting_management_properties['settings_with_linked_limit_int'];
$setting_with_fixed_limit = $result_setting_management_properties['setting_with_fixed_limit'];
$setting_with_no_limit = $result_setting_management_properties['setting_with_no_limit'];

/* Save changes & Update session */
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
	$save_result = $me->saveCustomResultSettings($_REQUEST);
	ASSIST_HELPER::displayResult($save_result['result_message']);
	$result_settings = $save_result['result_settings'];
}


/* Display table */
?>
<style type="text/css" >
	#tbl_results td {
		padding: 7px;
		vertical-align: middle;
	}
	#tbl_results input {
		padding: 5px;
	}
</style>
<form name=frm_save method=post action=<?php echo $self; ?>>
<input type=hidden name=act value=SAVE />
<table id=tbl_results>
<tr>
	<th>Default<br /><?php echo $result_settings_headings['value']; ?></th>
	<th>Default<br /><?php echo $result_settings_headings['text']; ?></th>
	<th>Default<br /><?php echo $result_settings_headings['limit_int']; ?></th>
	<th>Color</th>
	<th>Your<br /><?php echo $result_settings_headings['value']; ?></th>
	<th>Your<br /><?php echo $result_settings_headings['text']; ?></th>
	<th>Your<br /><?php echo $result_settings_headings['limit_int']; ?></th>
	<th>Explanation</th>
</tr>
<?php
foreach($result_settings as $i => $result) {
	$limit_perc = $result['limit_int'];
	if($result['limit_int']==false) {
		if($result['limit_link']!=false) {
			$limit_perc = $result_settings[$result['limit_link']]['limit_int'];
		} else {
			$limit_perc = "";
		}
	}
	if(strlen($limit_perc)>0) {
		if($i!=$setting_with_no_limit && $i!=$setting_with_fixed_limit && !in_array($i,$settings_with_limit_int)) {
			$limit_perc = "<input type=text size=3 class=validate_limit name=limit_int[".$result['limit_link']."] value='".$limit_perc."' />%";
		} else {
			$limit_perc.="%";
		}
	} else {
		$limit_perc = "N/A";
	}
		//colour editing not possible in current version as results display is linked to preset styl
		//<td><input class=\"jscolor {closable:true,closeText:'Close', hash:true}\" value='".$result['color']."' size=10 /></td>

	echo "
	<tr>
		<td>".$default_result_settings[$i]['value']."</td>
		<td class=center>".$default_result_settings[$i]['text']."</td>
		".(!in_array($i,$settings_with_limit_int) ? "
		<td class=center ".(in_array($i,$settings_with_linked_limit_int) ? "rowspan=2" : "").">".$default_result_settings[$i+(in_array($i,$settings_with_linked_limit_int) ? 1 : 0)]['limit_int']."</td>
		" : "
		")."
		<td><div style='background-color:".$result['color']."'>&nbsp;</div></td>
		<td><input type=text size=25 name=value[$i] value=\"".ASSIST_HELPER::code($result['value'])."\" /></td>
		<td class=center><input type=text size=5 name=text[$i] value='".$result['text']."' maxlength=3 class=center /></td>
		".(!in_array($i,$settings_with_limit_int) ? "
		<td class=center ".(in_array($i,$settings_with_linked_limit_int) ? "rowspan=2" : "").">".$limit_perc."</td>
		" : "")."<td>";
		switch($i) {
		/*	case 1:
				echo "0% <= Actual/Target <= ".($result_settings[$result['limit_link']]['limit_int']-0.001)."%";
				break;
			case 2:
				echo $result['limit_int'].".000% <= Actual/Target <= 99.999%";
				break;
			case 4:
				echo "100.001% <= Actual/Target <= ".($result_settings[$result['limit_link']]['limit_int']-0.001)."%";
				break;
			case 5:
				echo $result['limit_int'].".000% <= Actual/Target";
				break;
			case 3:
				echo "Actual meets Target (Actual/Target = 100%)";
				break;
			case 0:*/
			default:
				echo $result['glossary'];
		}
		echo "</td>
	</tr>";
}
?>

<tr>
	<td colspan=8 class=center><input type=submit value="Save Changes" class=isubmit /></td>
</tr>
</table>
</form>
<script type="text/javascript">
$(function() {
	$(".isubmit").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var err = false;
		var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9");
		$("#tbl_results input:text").each(function() {
			$(this).removeClass("required");
			var input_value = $(this).val();
			if(input_value.length==0) {
				//all fields are required
				$(this).addClass("required");
				err = true;
			} else if($(this).hasClass("validate_limit")) {
				if(isNaN(parseInt(input_value))) {
					//limits can only be numbers
					err = true;
					$(this).addClass("required");
				} else if(parseInt(input_value)==0 || parseInt(input_value)==100) {
					//limits can NOT be 0 or 100 - 100 is reserved for the KPI Met point and 0 is for N/A (no target & no actual)
					err = true;
					$(this).addClass("required");
				} else {
					//final test for only numbers and no decimal places etc
					var a = input_value.split("");
					for(x in a) {
						if($.inArray(a[x],validNumbers)<0) {
							err = true;
							$(this).addClass("required");
							break;
						}
					}
				}
			}
		});
		if(err) {
			AssistHelper.finishedProcessing("error","Please review the errors highlighted in red.<br />Please note: All fields are required and Limits must be a whole number and cannot be 0 or 100.");
			return false;
		} else {
			$("form[name=frm_save]").submit();
		}
	});
});

</script>
<?php
goBack("setup.php","Back to Setup");
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>
</body>
</html>