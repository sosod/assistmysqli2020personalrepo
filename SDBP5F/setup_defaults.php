<?php
include("inc/header.php");



?>
<script type=text/javascript>
$(function() {
	$(":button").click(function() {
		var id = $(this).prop("id");
		var t = $(this).attr("t");
		var url = 'setup_defaults_'+id+'.php?t='+t;
		document.location.href = url;
	});
});
</script>
<style>
table th {
	text-align: left;
}
</style>
<table cellpadding=3 cellspacing=0>
	<tr>
		<th><?php echo $head_dir; ?>:&nbsp;</th>
		<td>Setup the <?php echo $head_dir; ?> and <?php echo $head_sub; ?> structure.&nbsp;<span class=float><input type=button value="Configure" id=dir t=d /></span></td>
	</tr>
	<tr>
		<th>Headings:&nbsp;</th>
		<td>Setup the headings used by the module.&nbsp;<span class=float><input type=button value="Configure" id=head t=h /></span></td>
	</tr>
	<tr>
		<th>Lists:&nbsp;</th>
		<td>Setup the lists used by the module.&nbsp;<span class=float><input type=button value="Configure" id=lists t=l /></span></td>
	</tr>
	<tr>
		<th>Time Periods:&nbsp;</th>
		<td>Setup the time period automatic reminder & closures.&nbsp;<span class=float><input type=button value="Configure" id=time t=0 /></span></td>
	</tr>
</table>
</body>
</html>