<?php
//error_reporting(-1);
if(!isset($time2)) { $time2 = getTime($get_time,false); }
//echo ":";arrPrint($time2);
$my_head = $mheadings[$object_head];
$result_head = $mheadings[$r_section];
$update_head = $result_head;
if(isset($update_head['kr_target'])) { unset($update_head['kr_target']); }
if(isset($update_head['tr_target'])) { unset($update_head['tr_target']); }
if(isset($update_head['tr_dept'])) {
	unset($result_head['tr_dept']);
	unset($update_head['tr_dept']);
}
if(isset($update_head['tr_dept_correct'])) {
	unset($result_head['tr_dept_correct']);
	unset($update_head['tr_dept_correct']);
}
if(isset($update_head['tr_dept_attachment'])) {
	unset($result_head['tr_dept_attachment']);
	unset($update_head['tr_dept_attachment']);
}

$performance_fields = array("tr_perf","kr_perf");

$wide_keys = array_flip($wide_headings);

//arrPrint($wide_keys);

unset($wide_headings[$wide_keys['kpi_topid']]); unset($wide_headings[$wide_keys['kpi_capitalid']]);

if(!isset($mheadings['dir']) && isset($mheadings[$section][$table_fld."dirid"])) {
	$mheadings['dir'][0] = $mheadings[$section][$table_fld."dirid"];
}
//arrPrint($mheadings);

/*************************
****** SAVING UPDATE *****
*************************/
//arrPrint($_REQUEST);



$flds = array();
echo "<form id=save_object method=post action=$self ><input type=hidden name=obj_id[] id=obj_id value=\"\" />";
	drawManageFormFilter(true,true,true,true,$filter,$page_id);
	foreach($update_head as $fld => $h) { $flds[] = $fld;  echo "<input type=hidden name=".$fld."[] id=$fld value=\"\" />"; }
echo "</form>";

if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if($section=="KPI" || $section == "TOP") {
	$colspan = count($result_head)+($section=="TOP" ? 1 : 0)+1;
} elseif($section=="CF" || $section=="CAP") {
	if($filter['display']!="LIMIT") {
		$colspan = count($mheadings[$rheadings]);
	} else {
		$colspan = 0;
		foreach($mheadings[$rheadings] as $h) {
			if($h['c_list'])
				$colspan++;
		}
	}
} else {
	$colspan = 2;
}

echo "<form method=post action=$self id=save_all >";
drawManageFormFilter(true,true,true,true,$filter,$page_id);
?>


<table id=tbl_update>
<thead>
<?php
$total_columns = 0;
	$total = array();
	$total['all']['target'] = 0;
	$total['all']['actual'] = 0;
	echo "<tr>
		<th rowspan=2>Ref</th>";
		foreach($mheadings['FIXED'] as $key => $row) {
			$total_columns++;
			echo "<th rowspan=2 class=fixed>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
		}
		foreach($my_head as $key => $row) {
			if(($filter['display']!= "LIMIT" || $row['c_list']) && !$row['fixed']) {
				$total_columns++;
				echo "<th rowspan=2>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
			}
		}
		$tc = 0;
		$t_css = 2;
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $ti => $row) {
				$tc++;
				echo "<th colspan=$colspan class=\"time".$t_css."\">".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
			}
		} else {
			$ti = $filter['when'];
			$t = $time[$ti];
					$tc++;
			$ti_past = $ti - $time_increment;
			$ti_next = $ti + $time_increment;
			/*if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
					$col_past = 0;
					if($setup_defaults[$section."_past_r"]=="Y") { $col_past+= 3; }
					if($setup_defaults[$section."_past_c"]=="Y") { $col_past+= $section=="TOP" ? 4 : 2; }
					echo "<th colspan=".$col_past." class=\" time".($t_css+1)."\">".$time2[$ti_past]['display_full']."</th>";
			}*/
					echo "<th colspan=".($colspan)." class=\" time".$t_css."\">".$t['display_full']."</th>";
					$total[$ti]['target'] = 0;
					$total[$ti]['actual'] = 0;
			/*if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
					$col_next = 0;
					if($setup_defaults[$section."_next_r"]=="Y") { $col_next+= 1; }
					echo "<th colspan=".$col_next." class=\" time".($t_css-1)."\">".$time2[$ti_next]['display_full']."</th>";
			}*/
					$t_css = ($t_css>=4) ? 1 : $t_css+1;
		}
	echo "	</tr>";
	echo "	<tr>";
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $h => $cf_h) {
				foreach($mheadings['CF_R'] as $r => $cf_r) {
					if($filter['display']!="LIMIT" || $cf_r['c_list']) {
						echo "<th class=\"time".$t_css."\">".(strlen($cf_r['h_client'])>0 ? $cf_r['h_client'] : $cf_r['h_ignite'])."</th>";
						$total[$h][$r] = 0;
					}
				}
				$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			$t_css = 2;
				switch($section) {
					case "TOP":
					case "KPI":
						/*if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
								foreach($result_head as $fld => $h) {
									if( ($setup_defaults[$section."_past_r"]=="Y" && ($fld==$fld_target || $fld==$fld_actual)) || ($setup_defaults[$section."_past_c"]=="Y" && $fld!=$fld_target && $fld!=$fld_actual ) ) {
										echo "<th class=\"time".($t_css+1)."\" >".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'])."</th>";
										if($setup_defaults[$section."_past_r"]=="Y" && $fld==$fld_actual) {
											echo "<th class=\"time".($t_css+1)."\" >R</th>";
										}
									}
								}
						}*/
						foreach($result_head as $fld => $h) {
							/*if($fld==$fld_target) {
								echo "<th class=\"time".$t_css."\" >&nbsp;</th>";
							}*/
							echo "<th class=\"time".$t_css."\" >".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).((in_array($fld,$performance_fields) && $setup_defaults['perfcomm']=="Y") ? "<br /><span class=i style=\"font-size:6.5pt;\">(Required)</span>" : "")."</th>";
						}
						if($section=="TOP") {
							echo "<th class=\"time".$t_css."\" >Auto Update</th>";
						}
						echo "<th class=\"time".$t_css."\" >Assurance Status</th>";
						/*if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
								echo "<th class=\" time".($t_css-1)."\">".$result_head[$fld_target]['h_client']."</th>";
						}*/
						break;
					/*case "CAP":
						foreach($mheadings[$rheadings] as $r => $cr) {
							if($filter['display']!="LIMIT" || $cr['c_list']) {
								echo "<th class=\"time".$t_css."\">".(strlen($cr['h_client'])>0 ? $cr['h_client'] : $cr['h_ignite'])."</th>";
							}
						}
						break;
					default:
						echo "<th  class=\"time".$t_css."\" >Budget</th>
							<th  class=\"time".$t_css."\" >Actual</th>";*/
				}
		}
	echo "	</tr>";
?>
</thead>


<tbody>

<?php

//echo $object_sql;

$rc = 0;
	$object_rs = getRS($object_sql);
	while($object = mysql_fetch_assoc($object_rs)) { //arrPrint($object);
		$obj_id = $object[$table_fld.'id'];
		echo "<tr title='Click to update this KPI' id=".$obj_id." update=1>
			<th id=ref_".$obj_id.">".(isset($id_label)?$id_label:"").$obj_id."</th>";
				foreach($mheadings['FIXED'] as $key => $h) {
					echo "<td class=fixed id=".$key."_".$obj_id.">";
					if($h['h_type']=="LIST") {
						if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
							echo "<div class=centre>".$lists[$h['h_table']][$object[$key]]['code']."</div>";
						} else {
							echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
						}
					} else {
						echo displayValue($object[$key]);
					}
					echo "</td>";
				}
				foreach($my_head as $key => $h) {
					if(($filter['display']!="LIMIT" || $h['c_list']) && !$h['fixed']) {
						echo "<td>";
						if($h['h_type']=="LIST") {
							if(in_array($h['h_table'],$code_lists) && isset($lists[$h['h_table']][$object[$key]]) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
								echo "<div class=centre title=\"".$lists[$h['h_table']][$object[$key]]['value']."\" style=\"text-decoration: underline\">".$lists[$h['h_table']][$object[$key]]['code']."</div>";
							} elseif(isset($lists[$h['h_table']][$object[$key]])) {
								echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
							} else {
								echo $unspecified;
							}
							if(in_array($key,array("kpi_calctype","top_calctype"))) {
								echo "<input type=hidden value=".$object[$key]." name=abc[] id=ct_".$obj_id." />";
							}
						} elseif($h['h_type']=="TEXTLIST") {
							$x = $object[$key];
							$a = array();
							if(strlen($x)>0 && $x != "0") {
								$y = explode(";",$x);
								foreach($y as $z) {
									if(isset($lists[$h['h_table']][$z]['value'])) {
										$a[] = $lists[$h['h_table']][$z]['value'];
									}
								}
							}
							if(count($a)>0) {
								$v = implode("; ",$a);
							} else {
								$v = $unspecified;
							}
							echo displayValue($v);
						} else {
							switch($key) {
								case $table_fld."area":
								case $table_fld."wards":
								case "capital_area":
								case "capital_wards":
								case "capital_fundsource":
									if(isset($wa) && isset($wa[$key]) && isset($wa[$key][$obj_id])) {
										//echo $key." :: "; arrPrint($wa[$key][$obj_id]);
										foreach($wa[$key][$obj_id] as $v) {
											if(isset($v['code']) && strlen($v['code'])) {
												echo $v['code'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											} else {
												echo $v['value'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											}
										}
									}
									break;
								case $table_fld."topid":
									if(isset($tops) && isset($tops[$object[$key]])) {
										//echo displayValue($tops[$object[$key]]['top_value']." [".$id_labels_all['TOP'].$object[$key]."]");
										echo $id_labels_all['TOP'].$object[$key];
									}
									break;
								case $table_fld."capitalid":
									if(isset($caps) && isset($caps[$object[$key]])) {
										//echo displayValue($caps[$object[$key]]['cap_name']." [".$id_labels_all['CAP'].$object[$key]."]");
										echo $id_labels_all['CAP'].$object[$key];
									}
									break;
								case "top_annual":
								case "top_revised":
									echo "<div class=right>".KPIresultDisplay($object[$key],$object[$table_fld.'targettype'])."</div>";
									break;
								case "cap_planstart":
								case "cap_planend":
								case "cap_actualstart":
								case "cap_actualend":
									if(strlen($object[$key])>1) {
										echo date("d M Y",$object[$key]);
									}
									break;
								default:
									echo displayValue($object[$key]);
									break;
							}
						}
						echo "</td>";
					}
				}
				$result_object = $results[$obj_id];
				$t_css = 2;
				switch($section) {
					case "KPI":
					case "TOP":
						$obj_tt = $object[$table_fld.'targettype'];
						$obj_ct = $object[$table_fld.'calctype'];
						$values = array();
						//DISPLAY PAST MONTH
						/*if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
							$values_past = $result_object[$ti_past];
							$v_past = array(
								'target'=>array($ti_past=>$values_past[$fld_target]),
								'actual'=>array($ti_past=>$values_past[$fld_actual]),
							);
							if($setup_defaults[$section."_past_r"]=="Y") {
								$r_past = KPIcalcResult($v_past,$obj_ct,array(),$ti_past);
							}
								foreach($result_head as $fld => $h) {
									if( ($setup_defaults[$section."_past_r"]=="Y" && ($fld==$fld_target || $fld==$fld_actual)) || ($setup_defaults[$section."_past_c"]=="Y" && $fld!=$fld_target && $fld!=$fld_actual ) ) {
										echo "<td class=\"right time".($t_css+1)."\" >";
										if($fld=="tr_dept" || $fld == "tr_dept_correct") {
											echo "<div class=left>".displayTRDept($values_past[$fld],"HTML")."</div>";
										} else {
											switch($h['h_type']) {
												case "TEXT":
													echo "<div class=left>".decode($values_past[$fld])."</div>"; break;
												case "NUM":
												default:	echo KPIresultDisplay($values_past[$fld],$obj_tt); break;
											}
										}
										if($setup_defaults[$section."_past_r"]=="Y" && $fld == $fld_actual) {
											echo "</td><td class=\"".$r_past['style']."\" id=".$obj_id."_r>".$r_past['text'];
										}
										echo "</td>";
									}
								}
						}*/
						//DISPLAY CURRENT MONTH
						$update_class = $obj_id."updateclass";
						//echo "<td class=\"center time".$t_css." ".$update_class."\">".$result_object[$ti]['kr_update'].ASSIST_HELPER::drawStatus($result_object[$ti]['kr_update'])."</td>";
						foreach($result_head as $fld => $h) {
							$values[$fld] = $result_object[$ti][$fld];
							echo "
							<td class=\"right time".$t_css." ".$update_class."\" id=".$obj_id."_".$fld.">";
							if($fld=="kr_target" || $fld == "tr_target") {
								echo KPIresultDisplay($values[$fld],$obj_tt);//."<input type=hidden value=\"".$values[$fld]."\" name=abc[] id=tar_".$obj_id." />";
							} elseif($fld=="tr_dept" || $fld=="tr_dept_correct") {
								echo "<div class=left>".displayTRDept($values[$fld],"HTML")."</div>";
							} else {
								switch($h['h_type']) {
									case "TEXT":
										if(in_array($fld,array('tr_perf','kr_perf'))) {
											$class = "valid8me perf";
										} elseif(in_array($fld,array('kr_correct'))) {
											$class = "valid8me correct";
										} else {
											$class = "";
										}
										if(in_array($fld,array('tr_attachment','kr_attachment'))) {
											$x = unserialize($values[$fld]);
											$poe = array();
											if(isset($x['poe']) && strlen($x['poe'])>0) { $poe[] = $x['poe']; }
											$a = isset($x['attach']) ? $x['attach'] : array();
											if(count($a)>0) {
												$attach = array();
												foreach($a as $b) {
													$attach[] = $b['original_filename'];
												}
												$poe[] = "".implode("<br />",$attach);
											}
											$v = implode("<br />",$poe);
										} else {
											$v = $values[$fld];
										}
										echo ASSIST_HELPER::decode($v);
										break;
									case "NUM":
										echo KPIresultDisplay($values[$fld],$obj_tt); break;
									default:	break;//echo ($obj_tt==1?"R":"")."".$values[$fld]."".($obj_tt==2?"%":"")."<input type=hidden size=5 value=\"".$values[$fld]."\" name=old_".$fld."[] class=\"\" id=old_".$fld."_".$obj_id." />"; break;
								}
							}
							if($fld==$fld_actual) {
								$ds = "N";
								if($result_object[$ti][$r_table_fld.'update']==1) {
									$ds = "Y";
								} elseif($result_object[$ti][$r_table_fld.'update']==2) {
									$ds = "in_progress";
								} elseif($result_object[$ti][$r_table_fld.'target']==0) {
									$ds = "info";
								}
								echo ASSIST_HELPER::drawStatus($ds);
							}
							echo "</td>";
						}
						if($section=="TOP") {
							echo "<td class=\"center time".$t_css."\">";
							if($object['assoc']>0) {
								$assoc_ct = explode("|",$object['assoc_calctype']);
								$assoc_tt = explode("|",$object['assoc_targettype']);
								if(count($assoc_ct)>1 || !in_array($obj_ct,$assoc_ct) || count($assoc_tt)>1 || !in_array($obj_tt,$assoc_tt)) {
									echo $me->drawStatus("warn");
								} else {
									switch($result_object[$ti]['tr_auto']) {
										case 1:
											echo $me->drawStatus(true);
											break;
										case 0:
											echo $me->drawStatus("lock");
											break;
									}
								}
							} else {
								echo $me->drawStatus(false);
							}
							echo "</td>";
						}
						echo "<td class=\"center time".$t_css."  ".$update_class." ass-icon\">";
							/*if(!isset($object['kas_result']) || is_null($object['kas_result'])) {
								$kas = "info";
							} elseif($object['kas_result']==KPI::ASSURANCE_ACCEPT) {
								$kas = "Y";
							} else {
								$kas = "N";
							}*/
							$assurance_status = $result_object[$ti]['kr_assurance_status'];
							if(isset($object['kas_result']) && $object['kas_result']==KPI::ASSURANCE_ACCEPT) {
									$kas = "Y";
								} elseif($assurance_status==KPI::ASSURANCE_REJECT) {
									$kas = "N";
								} elseif($assurance_status==KPI::ASSURANCE_UPDATED) {
									$kas = "warn";
								} else {
									$kas = "info";
								}

							echo ASSIST_HELPER::drawStatus($kas);
						/*if($object['assoc']>0) {
							$assoc_ct = explode("|",$object['assoc_calctype']);
							$assoc_tt = explode("|",$object['assoc_targettype']);
							if(count($assoc_ct)>1 || !in_array($obj_ct,$assoc_ct) || count($assoc_tt)>1 || !in_array($obj_tt,$assoc_tt)) {
								echo $me->drawStatus("warn");
							} else {
								switch($result_object[$ti]['tr_auto']) {
									case 1:
										echo $me->drawStatus(true);
										break;
									case 0:
										echo $me->drawStatus("lock");
										break;
								}
							}
						} else {
							echo $me->drawStatus(false);
						}*/
						echo "</td>";
						//DISPLAY NEXT MONTH
						/*if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
							$values_next = $result_object[$ti_next][$fld_target];
								echo "<td class=\"right time".($t_css-1)."\">".KPIresultDisplay($values_next,$obj_tt)."</td>";
						}*/
						break;
					case "CF":
						$result_object = $results[$obj_id][$filter['when'][0]];
						$t_css = 3;
						foreach($mheadings['CF_H'] as $rs => $cf_h) {
							$cfh_values = array();
							$budget = 0;
							foreach($mheadings['CF_R'] as $res => $cf_r) {
								$fld = "cr_".$cf_h['field'].$cf_r['field'];
								$cfh_values[$fld] = $result_object[$fld];
								switch($res) {
									case "_1": case "_2": case "_3":	$budget+=$cfh_values[$fld];				break;	//o. budget, adj. est., vire
									case "_4":							$cfh_values[$fld] = $budget;			break;	//adj. budget
									case "_5":							$actual = $cfh_values[$fld];			break;	//actual
									case "_6":							$cfh_values[$fld] = $actual - $budget;	break;	//variance
								}
								//if($filter['display']!="LIMIT" || $cf_r['c_list']) {
									$val = $cfh_values[$fld];
									$total[$rs][$res]+= $val;
									echo "<td class=\"right time".$t_css."\">";
										switch($cf_r['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
									echo "</td>";
								//}
							}
							$t_css = ($t_css<=1) ? 3 : $t_css-1;
						}
						break;
					case "CAP":
						$values = array();
						/*foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {*/
								foreach($mheadings[$rheadings] as $r => $cr) {
									if(!isset($total[$ti][$r])) { $total[$ti][$r] = 0; }
									if(!isset($values[$r])) { $values[$r] = 0; }
									$val = $result_object[$ti][$r];
									$total[$ti][$r]+=$val;
									$values[$r]+=$val;
									//if($filter['display']!="LIMIT" || $cr['c_list']) {
										echo "<td class=\"right time".$t_css."\">";
										switch($cr['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
										echo "</th>";
									//}
								}
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
						break;
					default:
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								$total[$ti]['target']+=$values['target'][$ti];
								$total[$ti]['actual']+=$values['actual'][$ti];
								echo "<td class=\"right time".$t_css."\">";
									echo "<input type=text value=\"".$values['target'][$ti]."\" />";
								echo "</td><td class=\"right time".$t_css."\">";
									echo number_format($values['actual'][$ti],2);
								echo "</td>";
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
						break;
				}
			/*echo "<td class=centre><input type=hidden name=obj_id[] value=".$obj_id." class=objid />
						<input type=button value=Save id=".$obj_id." class=act1 />";
			if($section=="TOP" && !$result_object[$ti]['tr_auto']) {
				echo " <input type=button value=Unlock id=".$obj_id." class=act1 />";
			} elseif($section=="KPI" && $obj_tt==2 && $obj_ct=="CO" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") {
				echo " <input type=button value=PLC id=".$obj_id." class=plc />";
			}
			echo "</td>";*/
		echo "</tr>";
	}
	?>
	</tbody>
</table>

</form>

<?php
$modDisplayObj = new ASSIST_MODULE_DISPLAY();
$js = "";
?>

<div id=div_update title="">
		<input type=hidden name=after_action value='' id=after_action />
	<form name=frm_update method=post action="controller/assurance_review.php" language=jscript enctype="multipart/form-data">
		<input type=hidden name=junk id=junk value='' autofocus />
		<input type=hidden name=section value='<?php echo $section; ?>' id=section />
		<input type=hidden name=action value='SAVE_ASSURANCE' id=action />
		<input type=hidden name=obj_id value='' id=obj_id />
		<input type=hidden name=time_id value='<?php echo $ti; ?>' id=time_id />
		<input type=hidden name=ass_updateuser value='' id=ass_updateuser />
		<input type=hidden name=ass_updateuser_original value='' id=ass_updateuser_original />
		<input type=hidden name=ass_timecheck value='<?php echo ($time2[$ti]['active_primary']==true && $time2[$ti]['close_primary']>0) ? "open" : "closed"; ?>' id=ass_timecheck />
		<input type=hidden name=ass_timecheck_TEST value='closed' id=ass_timecheck_TEST />
	<div id=div_detail_display style='background-color: #ffffff;'>
	</div>

	<div id=div_assurance_form style='background-color: #ffffff'>
		<h2>Assurance Review <?php echo $head; ?></h2>
		<table class=form width=100%>
			<tr>
				<th>Sign-off:<br />(required)</th>
				<td><?php
				$js.= $modDisplayObj->drawBoolButton(0,array('id'=>"ass_signoff"));
					?></td>
			</tr>
			<tr>
				<th>Response:<br />(required if update rejected)</th>
				<td>
					<?php
					$js.= $modDisplayObj->drawTextArea("",array('id'=>"ass_response",'name'=>"ass_response"));
					?>
			</tr>
			<tr>
				<th>Update user:</th>
				<td id=td_frm_user></td>
			</tr>
			<tr>
				<th>Update deadline:</th>
				<td><?php //if($time2[$ti]['active_primary']==true) {
					//echo "Time period is currently open.  An update deadline cannot be set.<input type=hidden size=12 id=ass_deadline_hide name=ass_deadline value='X' />";
				//} else {
					echo "<input type=text size=12 id=ass_deadline name=ass_deadline />";
				//}
				?>
				</td>
			</tr>
			<Tr>
				<th>Additional Notifications:<br />(On Rejection)</th>
				<td id=td_frm_notifications></td>
			</Tr>
<!--			<tr>
				<th>Attachment:</th>
				<td >
					<div id=attach_form><?php $me->displayAttachmentForm(); ?></div>
					<div id=div_update_attach_display></div>
				</td>
			</tr>
	-->
		<!--

			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."target"]['h_client']; ?>:</th>
				<td id=target>0</td>
			</tr><tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?>:</th>
				<td id=td_actual><input type=text size=10 id=actual name="<?php echo $r_table_fld."actual"; ?>" /></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required)</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=perfcomm name="<?php echo $r_table_fld."perf"; ?>"></textarea></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required if ".$mheadings[$r_section][$r_table_fld."actual"]['h_client']." does not meet ".$mheadings[$r_section][$r_table_fld."target"]['h_client'].")</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=correct name="<?php echo $r_table_fld."correct"; ?>"></textarea></td>
			</tr><?php
			if($setup_defaults['POE']=="T" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>: <span class=float>
				<?php
				if(isset($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) && strlen($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) >0) {
					$me->displayIcon("info","id=poe_glossary");
				} ?></span>
				</th>
				<td ><textarea cols=50 rows=7 id=proof name="poe"></textarea></td>
			</tr>
			<?php
			}	//if setup defaults allows for textfield
			if($setup_defaults['POE']=="A" || $setup_defaults['POE']=="B") {
			?>
			<?php
			}	//if setup defaults allows for attachments
			?>  -->
			<tr>
				<th></th>
				<td><input type=button value="Save Update" class=isubmit id=btn_save_assurance /> <span class=float><input type=button value=Cancel class=cancel /></span></td>
			</tr>
		</table>
		<h2>Assurance Review History</h2>
		<div id=div_assurance_log></div>
		<p>&nbsp;</p>
	</div>
	<div id=div_no_border_display style='background-color: #ffffff;'>
		<h2>Assurance Review <?php echo $head; ?></h2>
		<p>No update has been completed so no Assurance is possible.</p>
		<span class=float><input type=button value=Close class=cancel /></span>
	</div>
<!--	<p class=float style='padding-bottom: 20px;'><input type=button value=Cancel class=cancel /></p> -->
	<div id=div_poe_glossary title="<?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>">
		<p><?php echo str_replace(chr(10),"<br />",decode($mheadings[$r_section][$r_table_fld."attachment"]['glossary'])); ?></p>
	</div>
	<div id=dlg_result>
	</div>
</div>
<!-- <div style='position:fixed;right:10px;top:10px;width:300px;'>
	<?php $me->displayResult(array("info","To update a KPI, click anywhere in the row.")); ?>
</div> -->
<div id=dlg_msg title=Processing>
</div>
<div id=dlg_response title=Success>
<?php echo str_replace(chr(10),"",$me->getDisplayResult(array("ok","Update processed successfully."))); ?>
</div>
<?php
$x = new DEPT();
$js.=$x->getAttachmentJavaForDownload2();
?>
<script type="text/javascript">
//alert("hello");
//	setTimeout(function () { $("#dispRes").css('display','none') }, 2500);
		var windowSize = getWindowSize();
		var dialogWidth = windowSize['width']<850 ? windowSize['width']-50 : 800;
		var dialogHeight = windowSize['height']<850 ? windowSize['height']-50 : 800;
		var section = "<?php echo $section; ?>";
		//var use_plc = "<?php echo ($me->usePLC() ? "Y" : "N"); ?>";
		//var require_comment = <?php echo $me->requireUpdateComments()==true ? "true" : "false"; ?>;
		//var comment_length = <?php echo $me->requiredCommentLength(); ?>;
		var okIcon = "<?php echo ASSIST_HELPER::drawStatus(true); ?>";
		var errIcon = "<?php echo ASSIST_HELPER::drawStatus(false); ?>";
		var timeInc = <?php echo $time_increment; ?>;
		var month1 = <?php echo $first_month; ?>;
		var head = "<?php echo $head; ?>";
		var processing_html = "<p class=center>Please wait while your form is processed...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p><p class=center>This may take some time depending on the size of the attachments added.</p>";
		var loading_html = "<p class='b center'>Loading...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p>";

	$(function() {
		<?php echo $js; ?>


		$('#ass_deadline').datepicker({
			minDate: "+0D",
			showOn: 'both',
			buttonImage: '/library/jquery/css/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'dd-M-yy',
			changeMonth: true,
			changeYear: true
		});

		$("tr.no-highlight").unbind("mouseenter mouseleave");

		$("form[name=frm_update] input").keypress(function(e) {
			if(e.keyCode==13) {
				e.preventDefault();
			}
		});


		var attach_form = $("#attach_form").html();

		var loadingSize = {width:250,height:300};
		var dlgLoadingOpts = ({
			modal: true,
			closeOnEscape: false,
			autoOpen: false,
			width: loadingSize['width'],
			height: loadingSize['height'],
			position: { my: "center", at: "center", of: $("#div_update") }
		});

		var dlgResultsOpts = ({
			closeOnEscape: true,
			modal: true,
			autoOpen: false,
			position: { my: "center", at: "center", of: $("#div_update") },
			buttons: [{ text: "Ok", click: function() {
					$("#dlg_response").dialog("close");
				}
			}],
			close: function() {
				$("#div_update").dialog("close");
			}

		});
		var dlgErrorOpts = ({
			modal: true,
			autoOpen: false,
			width: 300,
			height: 320,
			buttons:[{
				text: "Ok",
				click: function() { $(this).dialog("close"); }
			}]
		});
		$("#dlg_msg").dialog({modal: true,autoOpen: false}).html(processing_html);
		$("#dlg_msg").dialog("option",dlgLoadingOpts);
		$("#dlg_response").dialog(dlgResultsOpts);
		$(".ui-dialog-titlebar").hide();
		//createResultsDialog();
		$("#div_poe_glossary").dialog({
			autoOpen: false,
			modal: true
		});
		$("#div_update #poe_glossary").css("cursor","pointer").click(function() {
			$("#div_poe_glossary").dialog("open").dialog("option","position",{ my: "center", at: "center", of: $("#div_update") });
		});
		$("#div_update").dialog({
			autoOpen: false,
			modal: true,
			width: dialogWidth,
			height: dialogHeight,
			close: function() {
				$("textarea, input:text").removeClass("required");
				$("input:file").val("");
				enable_scroll();
				//$("body").css("overflow","scroll");
			},
			open: function() {
				$(this).find("#junk").focus();
			}
		});
		$("#div_update input:button.cancel").click(function() {
			$("#div_update").dialog("close");
		});
		$("#tbl_update tbody tr").click(function() {
			//$("#dlg_msg").html(loading_html).dialog('option',dlgLoadingOpts).dialog("open");
			//$("#attach_form").html(attach_form);
			$("#ass_response").val("");
			$("#ass_deadline").val("");
			$("#div_assurance_log").html("");
			$("button.btn_no").trigger("click");
			var i = $(this).prop("id");
			var ti = $("#div_update #time_id").val();
			var dta = "action=GET_UPDATE_DETAILS&id="+i+"&time_id="+ti+"&section="+section;
			//alert(dta);
			//console.log(dta);
			var d = doAjax("controller/assurance_review.php",dta);
			//console.log(d);
			//alertArray(d,0);
			//alert(d[1]);
			if(d[0]==1) {
				$("#div_detail_display").html("<h2>Details</h2>"+d['detail_display']);
				$("#div_assurance_log").html(d['log_display']);
				$("#div_update #obj_id").val(i);
				$("#div_update #ass_updateuser").val(0);
				if(d['data']['results']['update'][ti]>0) {
					$("#div_no_border_display").hide();
					$("#div_assurance_form").show();
					if(d['data']['results']['updateuser_status'][ti]!=1) {
						var ass_user = d['data']['results']['ass_updateuser_id'][ti];
						var dta2 = "action=GET_AVAILABLE_USERS_FOR_KPI&id="+i+"&section="+section;
						var d2 = doAjax("controller/assurance_review.php",dta2);
						$("#div_update #ass_updateuser_original").val(d['data']['results']['ass_updateuser_id'][ti]);
						if(d2[0]==1 && d2['user_count']>0) {
							$("#div_update #ass_updateuser").val(0);
							$("#div_assurance_form #td_frm_user").html("");
							$("#div_assurance_form #td_frm_user").append(
								$('<select />',{id:'ass_updateuser2',name:'ass_updateuser2'})
							);
							for(i in d2['data']) {
								if(i==d['data']['results']['ass_updateuser_id'][ti]) {
									$("#ass_updateuser2").append($('<option />',{value:i,text:d2['data'][i],selected:true}));
								} else {
									$("#ass_updateuser2").append($('<option />',{value:i,text:d2['data'][i]}));
								}
							}
						} else {
							$("#div_update #ass_updateuser").val("X");
							$("#div_assurance_form #td_frm_user").html("Error, user no longer active on Assist");
						}
					} else {
							$("#div_update #ass_updateuser_original").val(d['data']['results']['updateuser_id'][ti]);
							$("#div_update #ass_updateuser").val(d['data']['results']['updateuser_id'][ti]);
							$("#div_assurance_form #td_frm_user").html(d['data']['results']['updateuser'][ti]);
					}
					$("#div_assurance_form #td_frm_notifications").html(d['data']['notifications']);

					//$("#div_update_form #div_update_attach_display").html(d['attach_display']);

					//if(!d['data']['time'][ti]['active_primary']) {
					//	$("#div_assurance_form #div_deadline").show();
					//	$("#div_assurance_form #lbl_deadline").hide();
					//} else {
					//	$("#div_assurance_form #div_deadline").hide();
					//	$("#div_assurance_form #lbl_deadline").show();
					//}

				} else {
					$("#div_no_border_display").show();
					$("#div_assurance_form").hide();
				}
				$("#div_update").dialog("option","title","Assurance Review: KPI "+d['ref']+" for "+head).dialog("open").dialog("option","position",{ my: "center", at: "top", of: document });
				$("li.hover").hover(
					function(){ $(this).addClass("trhover-dark"); },
					function(){ $(this).removeClass("trhover-dark"); }
				);
			}
			$("#dlg_msg").dialog("close");

		});

		$("#div_update input:button.isubmit").click(function() {
			$("#dlg_msg").html(processing_html).dialog('option','buttons',[]).dialog('option','height',loadingSize['height']).dialog("open");
			var err = "";
			var valid = true;
			var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9",".");

			//active update user
			if($("#div_update #ass_updateuser").val()=="X" && $("#ass_signoff").val()==0) {
				valid = false;
				err+="<li>No active user available to correct update</li>";
			}

			//ass_signoff
			if($("#ass_signoff").val().length==0) {
				valid = false;
				err+="<li>Sign-off: Yes or No?</li>";
			}
			//ass_response
			$("#ass_response").parent().parent().find("th").removeClass("required");
			if($("#ass_signoff").val()=="0" && $("#ass_response").val().length==0) {
				$("#ass_response").parent().parent().find("th").addClass("required");
				valid = false;
				err+="<li>A response is required if you are not accepting the update.</li>";
			}
			//ass_deadline
			if($("#ass_timecheck").val()=="closed") {
				$("#ass_deadline").parent().parent().find("th").removeClass("required");
				if($("#ass_signoff").val()=="0" && $("#ass_deadline").val().length==0) {
					$("#ass_deadline").parent().parent().find("th").addClass("required");
					valid = false;
					err+="<li>A deadline is required if you are not accepting the update and the current time period is not open or does not have a closure date.</li>";
				}
			}

			if(valid) {//} && valid_perf && valid_correct) {
				var obj_id = $("#div_update #obj_id").val(); //console.log(obj_id);
				$form = $("form[name=frm_update]");
				//console.log(AssistForm.serialize($form));
				var res = AssistHelper.doAjax('controller/assurance_review.php',AssistForm.serialize($form));
				//console.log(res);
				if(res[0]=="ok") {
					if($("#ass_signoff").val()==1) {
						var ico = okIcon;
					} else {
						var ico = errIcon;
					}
					//console.log(ico);
					$("#tbl_update td."+obj_id+"updateclass.ass-icon").html(ico);
				}
				showResponse(res[1]);
				$('#dlg_msg').dialog('close');
				//console.log(AssistForm.serialize($form));
			//else not valid then display error message
			} else {
				//alert("DEV NOTE: DISPLAY ERROR MESSAGE HERE: "+err);
				$("#dlg_msg").html("<h1 class=red>Error</h1><p>Please complete the following required fields:</p><ul id=ul_req>"+err+"</ul>").dialog("option",dlgErrorOpts);
			}
		});


	});
	function showResponse(txt) {
		$(function() {
			//alert(txt);
			$("#dlg_response #spn_displayResultText").text(txt);
			$("#dlg_response").dialog("open");
		});
	}
	function showDialog(r,response) {
		$(function() {
				$('<div />',{id:'response_msg', html:response}).dialog({
					autoOpen	: true,
					modal 		: true,
					title 		: r,
					position	: { my: 'center', at: 'center', of: $('#div_update') },
					width 		: 'auto',
					height    	: 'auto',
					buttons		: [{ text: 'Ok', click: function() {
										$('#response_msg').dialog('destroy');
										$('#response_msg').remove();
									}
								}],
					});
		});
	}

</script>