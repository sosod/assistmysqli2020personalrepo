<?php 
$section = "TOP";
$table_fld = "top_";
$table_tbl = "top";
$get_headings = array("TOP","TOP_R");
$get_lists = true;
$get_active_lists = false;
$get_time = array(1,3,4,6,7,9,10,12);
$locked_column = true;
if(isset($_REQUEST['page_id']) && $_REQUEST['page_id']=="auto") { 
	$locked_column = false; 
	
}

include("inc/header.php"); 
//arrPrint($_REQUEST);
$time2 = getTime($get_time,false);
//arrPrint($time);
for($i=3;$i<=12;$i+=3) {
	$time[$i]['start_date'] = $time[$i-2]['start_date'];
	$time[$i]['start_stamp'] = $time[$i-2]['start_stamp'];
	unset($time[$i-2]);
	if($time[$i]['start_stamp']>$today) { unset($time[$i]); }
}
//arrPrint($time);
if(!isset($_REQUEST['page_id']) || $_REQUEST['page_id']!="change") {
	$active_fld = "secondary";
	$time = getOpenTime($time,$active_fld);
	if(count($time)==0 && $page_id !="update") { 
		$time = getTime(array(12),false); 
	}
} else {
	$time = getTime(array(3,6,9,12),false);
}
$year_validation_time = getTime(array(1,12),false);
//arrPrint($time);

if(!$import_status[$section]) {
	die("<p>Top Layer SDBIP has not yet been created.  Please try again later.</p>");
} else {

//GENERIC OBJECT SQL
			/*$object_sql = "SELECT t.* FROM ".$dbref."_top t
			INNER JOIN ".$dbref."_dir dir ON t.top_dirid = dir.id AND dir.active = true
			WHERE top_active = true ";*/
//GENERIC OBJECT SQL
			$object_sql = "SELECT t.*, t.top_id as kpi_id FROM ".$dbref."_top t
			INNER JOIN ".$dbref."_dir dir ON t.top_dirid = dir.id AND dir.active = true
			LEFT OUTER JOIN ".$dbref."_kpi k ON k.kpi_topid = t.top_id
			WHERE top_active = true 
			";
			$r_object_sql = "SELECT t.* FROM ".$dbref."_top t
			INNER JOIN ".$dbref."_dir dir ON t.top_dirid = dir.id AND dir.active = true
			LEFT OUTER JOIN ".$dbref."_kpi k ON k.kpi_topid = t.top_id
			WHERE top_active = true 
			";



	switch($page_id) {
	case "audit":	include("manage_top_audit.php"); break;
	case "auto":	include("manage_top_auto.php"); break;
	case "create":	include("manage_top_create.php"); break;
	case "change":	include("assurance_topadmin_change.php"); break;
	default:   
		if(count($time)>0 || $page_id!="update") {
	unset($mheadings['TOP']['top_repkpi']);
//arrPrint($year_validation_time);
	//Filter settings
	$filter = isset($_SESSION[$self."_".$page_id]) ? $_SESSION[$self."_".$page_id] : array();
		//WHEN
		if(isset($_REQUEST['filter_when'])) {
			$filter_when = $_REQUEST['filter_when'];
		} elseif($today < strtotime($year_validation_time[1]['start_date'])) {
			$filter_when = 1;
		} elseif($today > strtotime($year_validation_time[12]['end_date'])) {
			$filter_when = 12;
		} else {
			$filter_when = (date("m",$today)>6) ? (date("m",$today)-6) : (date("m",$today)+6) ;
		}
		$filter['when'] = ceil($filter_when/3)*3;
		//WHO
		if(isset($_REQUEST['filter_who'])) {
			$f_who = $_REQUEST['filter_who'];
			if(strtolower($f_who)=="all") {
				$filter_who = array("","All");
			} else {
				$filter_who = explode("_",$f_who);
			}
		} else {
			$filter_who = array("","All");
		}
		$filter['who'] = $filter_who;
		//WHAT
		$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
		//DISPLAY
		$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";






		$dir_sql = "SELECT d.id as dir FROM ".$dbref."_dir d
			WHERE d.id IN (SELECT DISTINCT t.top_dirid FROM ".$dbref."_top t
			WHERE top_active = true ) AND d.active = true";
		$valid_dir_sub = mysql_fetch_alls($dir_sql,"dir");

		//arrPrint($time);

		//drawViewFilter($who,$what,$when,$sel,$filter)
		$head = "Quarter ending ".$time[$filter['when']]['display_full'];
		$filter['who'] = drawManageFilter(true,false,true,true,$filter,$head);


		//SQL
		/*$object_sql = "SELECT t.* FROM ".$dbref."_top t
		INNER JOIN ".$dbref."_dir dir ON t.top_dirid = dir.id AND dir.active = true
		WHERE top_active = true ".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND top_dirid = ".$filter['who'][1] : "");*/
				$object_sql.=($filter['who'][0]=="D" && ASSIST_HELPER::checkIntRef($filter['who'][1]) ? " AND top_dirid = ".$filter['who'][1] : "");
		
		$results_sql = "SELECT r.* FROM ".$dbref."_top_results r
		INNER JOIN ".$dbref."_top t ON r.tr_topid = t.top_id AND t.top_active = true
		WHERE tr_timeid IN (".($filter['when']-3).",".$filter['when'].",".($filter['when']+3).") AND tr_topid IN (SELECT t.top_id FROM ".substr($r_object_sql,16,strlen($r_object_sql)).")";
		
		$results = mysql_fetch_alls2($results_sql,"tr_topid","tr_timeid");
		$fld_target = "tr_target";
		$fld_actual = "tr_actual";
		
				$dept_sql = "SELECT kpi_topid, count(kpi_id) as kc FROM ".$dbref."_kpi WHERE kpi_active = true AND kpi_topid IN (SELECT t.top_id FROM ".substr($r_object_sql,16,strlen($r_object_sql)).") GROUP BY kpi_topid";
				$dept_kpis = mysql_fetch_alls($dept_sql,"kpi_topid");
				
		if(isset($mheadings[$section][$table_fld.'wards'])) {
			$wards_sql = "SELECT tw_topid, id, value, code FROM ".$dbref."_top_wards INNER JOIN ".$dbref."_list_wards ON tw_listid = id 
			WHERE tw_active = true AND tw_topid IN (SELECT t.top_id FROM ".substr($r_object_sql,16,strlen($r_object_sql)).")";
			$wa[$table_fld.'wards'] = mysql_fetch_alls2($wards_sql,"tw_topid","id");
		}
		if(isset($mheadings[$section][$table_fld.'area'])) {
			$area_sql = "SELECT ta_topid, id, value, code FROM ".$dbref."_top_area INNER JOIN ".$dbref."_list_area ON ta_listid = id 
			WHERE ta_active = true AND ta_topid IN (SELECT t.top_id FROM ".substr($r_object_sql,16,strlen($r_object_sql)).")";
			$wa[$table_fld.'area'] = mysql_fetch_alls2($area_sql,"ta_topid","id");
		}

		
		//$object_sql.= " LIMIT 50";
						$object_sql.= " GROUP BY t.top_id";
//			$object_sql = str_replace(" FROM",", count(k.kpi_id) as assoc FROM",$object_sql);
		$object_sql = str_replace(" FROM",", count(k.kpi_id) as assoc, GROUP_CONCAT(DISTINCT k.kpi_calctype ORDER BY k.kpi_calctype SEPARATOR '|') as assoc_calctype, GROUP_CONCAT(DISTINCT k.kpi_targettype ORDER BY k.kpi_targettype SEPARATOR '|') as assoc_targettype FROM",$object_sql);

		$_SESSION[$self."_".$page_id] = $filter;
		switch($page_id) {
			case "update":	include("manage_table_update.php");	break;
			case "edit":	include("manage_table_edit.php");	break;
		}

	} else {
		echo "<p>There are no open time periods to update.</p>";
	}
	
}	//switch page_id	
}	//if import_status['import_Section'] == false else	
?>
</body>
</html>