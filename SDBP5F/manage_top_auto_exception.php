<h2>Exception Report</h2>
<p style="font-style: italic; margin-top: -10px;">As At <?php echo date("d M Y H:i"); ?></p>
<style>
	table td.log {
		font-style: italic;
		font-size: 7pt;
	}
	table th.log {	
		font-size: 7pt;
	}
</style>
<?php
//arrPrint($lists['dir']);
$dept_sql = "SELECT kpi_topid, kpi_id, kpi_calctype, kpi_targettype, kpi_value, CONCAT_WS(' - ',dir.value, sub.value) as kpi_sub FROM ".$dbref."_kpi 
		INNER JOIN ".$dbref."_subdir sub ON kpi_subid = sub.id AND sub.active = true
		INNER JOIN ".$dbref."_dir dir ON sub.dirid = dir.id AND dir.active = true
		WHERE kpi_active = true AND kpi_topid > 0";
$dept_kpis = mysql_fetch_alls2($dept_sql,"kpi_topid","kpi_id");
//arrPrint($dept_kpis);
?>
<table>
	<tr>	
		<th>Ref</th>
		<th><?php echo $mheadings['TOP']['top_dirid']['h_client']; ?></th>
		<th><?php echo $mheadings['TOP']['top_value']['h_client']; ?></th>
		<th>Assoc. Dept KPIs</th>
		<th><?php echo $mheadings['TOP']['top_calctype']['h_client']; ?></th>
		<th><?php echo $mheadings['TOP']['top_targettype']['h_client']; ?></th>
		<th>Result</th>
	</tr><tbody>
<?php
$results = array();
$objects = mysql_fetch_alls($object_sql,"top_id");
foreach($objects as $obj_id => $obj) {
	$echo = "";
	$err = false;
	foreach($dept_kpis[$obj_id] as $k_id => $k) {
		$echo.=	"<tr>
					<th class=log>".$id_labels_all['KPI'].$k_id."</th>
					<td class=log>".$k['kpi_sub']."</td>
					<td class=log>".$k['kpi_value']."</td>
					<td class=log></td>
					<td class=\"log center\">".$lists['calctype'][$k['kpi_calctype']]['value']."</td>
					<td class=\"log center\">".$lists['targettype'][$k['kpi_targettype']]['code']."</td>
				</tr>";
				if($k['kpi_calctype']!=$obj['top_calctype'] || $k['kpi_targettype']!=$obj['top_targettype']) { $err = true; }
	}
	if($err) {
		$results[$obj_id] = array('td'=>"ui-state-error",'icon'=>"ui-icon-closethick",'txt'=>"");
		if($k['kpi_calctype']!=$obj['top_calctype']) {
			$results[$obj_id]['txt'] = "Calculation Type mismatch";
		}
		if($k['kpi_targettype']!=$obj['top_targettype']) {
			if(strlen($results[$obj_id]['txt'])>0) { $results[$obj_id]['txt'].="<br />"; }
			$results[$obj_id]['txt'] = "Target Type mismatch";
		}
	} else {
		$results[$obj_id] = array('td'=>"ui-state-ok",'icon'=>"ui-icon-check",'txt'=>"All OK");
	}
	echo	"<tr>
				<th>".$id_labels_all[$section].$obj_id."</th>
				<td>".$lists['dir'][$obj['top_dirid']]['value']."</td>
				<td>".$obj['top_value']."</td>
				<td class=center>".count($dept_kpis[$obj_id])."</td>
				<td class=center>".$lists['calctype'][$obj['top_calctype']]['value']."</td>
				<td class=center>".$lists['targettype'][$obj['top_targettype']]['code']."</td>
				<td rowspan=".(count($dept_kpis[$obj_id])+1)." class=\"".$results[$obj_id]['td']."\" id=TD".$obj_id."><span id=ICON".$obj_id." class=\"ui-icon ".$results[$obj_id]['icon']."\" style=\"float: left;\"></span><label id=TXT".$obj_id.">".$results[$obj_id]['txt']."</label></td>
			</tr>"
			.$echo;
}
?></tbody>
</table>