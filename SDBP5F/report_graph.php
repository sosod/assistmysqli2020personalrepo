<style type=text/css>table .noborder, td .noborder { border: 1px dashed #ffffff; } table tbody td { padding-top : 5px; padding-bottom: 5px; } </style>
<?php
$get_lists = true;
$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "graph_dept";
switch($page_id) {
	case "graph_dept": 	$section = "KPI"; break;
	case "graph_top":	$section = "TOP"; break;
	case "graph_cashflow":	$section = "CF";  break;
	case "graph_capital":	$section = "CAP"; break;
}
include("inc/header.php");
$form = $base[0]."_".$page_id.".php";
$h2 = 1;

//arrPrint($lists['repcate']);

echo "<form action=".$form." method=post>
		<input type=hidden name=page_id value=".$page_id." />
		<input type=hidden name=act value=GENERATE />
		<input type=hidden name=display id=disp value='ONS' />
		<input type=hidden name=page_layout id=page value='MOBILE' />
				";
//Container table
echo "<table class=noborder width=700><tr class=no-highlight><td class=noborder>";
echo "<h2>$h2. Select the filter you wish to apply:</h2>";$h2++;
echo "	<table id=filter width=100% class=form><tbody>";
if($section=="TOP") {
	$sql = "SELECT dir.id as dirid, dir.value as dirtxt FROM ".$dbref."_dir dir
			WHERE dir.active = true
			AND dir.id IN (SELECT DISTINCT top_dirid FROM ".$dbref."_top WHERE top_active = true)
			ORDER BY dir.sort";
	$dir = mysql_fetch_alls($sql,"dirid");
	echo "		<tr>
					<th>Directorate:</th>
					<td><select name=dirsub_filter id=dirsub><option value=ALL selected>Entire Municipality</option>";
					foreach($dir as $did => $d) {
						echo "<option value=D_".$did.">".$d['dirtxt']."</option>";
					}
	echo "			</select></td>
				</tr>";
} else {
	$sql = "SELECT dir.id as dirid, dir.value as dirtxt, sub.id as subid, sub.value as subtxt FROM ".$dbref."_dir dir
			INNER JOIN ".$dbref."_subdir sub ON sub.dirid = dir.id AND sub.active = true
			WHERE dir.active = true
			AND sub.id IN (SELECT DISTINCT ".$table_fld."subid FROM ".$dbref."_".$table_tbl." WHERE ".$table_fld."active = true)
			ORDER BY dir.sort, sub.sort";
	$dir = array();
	$subs = array();
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$subs[$row['dirid']][$row['subid']] = $row;
		if(!isset($dir[$row['dirid']])) { $dir[$row['dirid']] = array('dirid'=>$row['dirid'],'dirtxt'=>$row['dirtxt']); }
	}
	echo "		<tr>
					<th>[Sub-]Directorate:</th>
					<td><select name=dirsub_filter id=dirsub><option value=ALL selected>Entire Municipality</option>";
					foreach($dir as $did => $d) {
						echo "<option value=D_".$did.">".$d['dirtxt']."</option>";
						foreach($subs[$did] as $sid => $s) {
							echo "<option value=S_".$sid.">&nbsp;- ".$s['subtxt']."</option>";
						}
					}
	echo "			</select></td>
				</tr>";
}
echo "		<tr>
				<th width=170>Time period:</th>
				<td>From: <select name=from>";
				foreach($time as $ti => $t) {
					echo "<option value=$ti ".($ti==1 ? "selected" : "").">".$t['display_full'].($t['active_primary'] ? "*" : "")."</option>";
				}
echo "			</select> to <select name=to>";
				foreach($time as $ti => $t) {
					echo "<option value=$ti ".($ti==$current_time_id ? "selected" : "").">".$t['display_full'].($t['active_primary'] ? "*" : "")."</option>";
				}
echo "			</select><br />
				<span style=\"font-size: 6.5pt\" class=i>* indicates time periods which are still open for updating.</span><br />
				<div style=\"font-size: 7.5pt; border: 1px solid #fe9900; padding: 3px; margin: 3px;\">Please note: The report is generated based on the results actually captured during the period selected.  To report on the overall status please select July as the start month.</div></td>
			</tr>";
if(in_array($section,array("KPI","TOP"))) {
	echo "		<tr>
					<th>".$mheadings[$section][$table_fld.'repcate']['h_client'].":</th>
					<td><input type=checkbox name=repcate[] checked=checked value=0 /> ".$unspecified;
	foreach($lists['repcate'] as $key => $l) {
		echo "<br /><input type=checkbox name=repcate[] checked=checked value=".$key." /> ".$l['value']."";
	}
	echo "
					</td>
				</tr>
				<tr>
					<th>KPI Filter:</th>
					<td><select name=kpi_notmeasured><option selected value=\"EXCLUDE\">Exclude</option><option value=INCLUDE>Include</option></select> KPIs Not Measured<br />
					<span style=\"font-size: 6.5pt\" class=i>* KPIs with no targets & actuals prior to the end of the time period selected.</span></td>
				</tr>
				<tr>
					<th>KPI's Met:</th>
					<td><select name=kpi_met><option selected value=SEPARATE>Separate</option><option value=COMBINE>Combine</option></select> <span class=".$result_settings[3]['style'].">&nbsp;&nbsp;</span> ".$result_settings[3]['value']." <span class=".$result_settings[4]['style'].">&nbsp;&nbsp;</span> ".$result_settings[4]['value']." <span class=".$result_settings[5]['style'].">&nbsp;&nbsp;</span> ".$result_settings[5]['value']."</td>
				</tr>";
}
echo "	</tbody></table>";
echo "<h2>$h2. Select the layout:</h2>";$h2++;
echo "	<table id=layout width=100%><tbody>";
	echo "
		<tr>
			<th width=170>Include Sub-graphs?</th>
			<td><select name=do_group id=sel_do><option selected value=Y>Yes</option><option value=N>No</option></select></td>
		</tr>
		<tr class=tr_group>
					<th width=170>Group By:</th>
					<td><select name=groupby>";
			if($section!="TOP") { echo "<option value=obj_dirid selected id=group_1>Directorate</option>"; }
		foreach($mheadings[$section] as $fld => $h) {
			if((!in_array($fld,$nosort) && !in_array($fld,$nogroup))) {
				echo "<option value=".$fld." ".($section=="TOP" && $fld=="top_dirid" ? " id=group_1" : "").">".$h['h_client']."</option>";
			}
		}
	echo "			</select></td>
				</tr>";
if(in_array($section,array("KPI","TOP"))) {
	echo "		<tr class=tr_group>
					<th>Graph Type:</th>
					<td><select name=graph_type id=sel_gtype>
						<option selected value=BAR_PERC>Bar graph (% of KPI Status)</option>
						<option value=BAR_REG>Bar graph (Number of KPIs)</option>
						<option value=PIE_ABOVE>Individual Pie graphs (Separate primary graph)</option>
						<option value=PIE_INLINE>Individual Pie graphs (Primary graph inline)</option>
					</select>&nbsp;&nbsp;<span id=spn_gtype><input type=checkbox value=1 name=bar_grid id=chk_grid><label for=chk_grid> Display grid?</label></span></td>
				</tr>
				";
/*				<tr>
					<th>Display type:</th>
					<td><select name=display id=disp><option selected value=ONS>Onscreen</option><option value=PRINT>Print</select>&nbsp;
 * <select name=page_layout id=page><option selected value=LAND>Landscape</option><option value=PORT>Portrait</select></td>
				</tr>*/
} elseif(in_array($section,array("CF","CAP"))) {
	echo "		<tr class=tr_group>
					<th>Value Axis:</th>
					<td><select name=valAxis>
						<option selected value=STD>Standardise value axis for all sub-graphs</option>
						<option value=GEN>Do not standardise value axis for all sub-graphs</option>
					</select></td>
				</tr>";
}
echo "	<tbody></table>";
echo "<h2>$h2. Generate the graph:</h2>";$h2++;
echo "<p><span class=b>Report title:</span> <input type=text name=report_title size=50 value=\"\" />";
echo "<p><input type=submit value=Generate /> <input type=reset value=Reset /></p>";
echo "</form>";

?>

<script type=text/javascript>
$(function() {
	var section = '<?php echo $section; ?>';
	$("th").addClass("left");
	$("#dirsub").change(function() {
		var g1 = $("#group_1").val();
		var f = $("#dirsub").val();
		switch(section) {
		case "TOP":
			if(f=="ALL" && g1!="obj_dirid") {
				$("#group_1").val("obj_dirid");
				$("#group_1").text("Directorate");
			} else if(f.substring(0,1)=="D" && g1!="NA") {
				$("#group_1").val("NA");
				$("#group_1").text("No Grouping");
			}
			break;
		case "KPI":
		default:
			if(f=="ALL" && g1!="obj_dirid") {
				$("#group_1").val("obj_dirid");
				$("#group_1").text("Directorate");
			} else if(f.substring(0,1)=="D" && g1!="obj_subid") {
				$("#group_1").val("obj_subid");
				$("#group_1").text("Sub-Directorate");
			} else if(f.substring(0,1)=="S" && g1!="NA") {
				$("#group_1").val("NA");
				$("#group_1").text("No Grouping");
			}
		}
	});
	$("#disp").change(function() {
		//$("#page option").remove();
/*		if($(this).val()=="ONS") {
			$("#page option:eq(0)").replaceWith("<option value=COMP>Computer</option>");
			$("#page option:eq(1)").replaceWith("<option value=MOBILE>Mobile</option>");
			$("#page").val("COMP");
		} else {
			$("#page option:eq(0)").replaceWith("<option value=PORT>Portrait</option>");
			$("#page option:eq(1)").replaceWith("<option value=LAND>Landscape</option>");
			$("#page").val("PORT");
		}*/
	});
	$("#disp").trigger("change");
	$("#sel_gtype").change(function() {
		if($(this).val()=="BAR_PERC" || $(this).val() == "BAR_REG") {
			$("#spn_gtype").show();
		} else {
			$("#spn_gtype").hide();
		}
	});
	$("#sel_gtype").trigger("change");
	$("#sel_do").change(function() {
		if($(this).val()=="Y") {
			$(".tr_group").show();
		} else {
			$(".tr_group").hide();
		}
	});
});
</script>
</body>
</html>