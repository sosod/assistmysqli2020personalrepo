<?php
$get_lists = false;
$section = "CAP";
$page_title = "Detail";
$obj_id = $_REQUEST['id'];
include("inc/header.php"); 
$lists['years'] = getList("years","id","true");
global $helperObj;
//arrPrint($mheadings[$section]);

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			break;
		default: $fields[] = "c.".$fld;
	}
}

$object_sql = "SELECT dir.value, ".implode(", ",$fields)."
FROM ".$dbref."_capital c
INNER JOIN ".$dbref."_subdir as subdir ON c.cap_subid = subdir.id
INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE c.cap_id = ".$obj_id;
//echo "<P>".$object_sql;
$object = $helperObj->mysql_fetch_all($object_sql);
$object = $object[0];

$extras = array();
//WARDS
$fld = "capital_wards";
$tbl = "wards";
$a = "w";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.c".$a."_listid AND b.c".$a."_active = true AND b.c".$a."_capid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = $helperObj->mysql_fetch_all($a_sql);
//AREAS
$fld = "capital_area";
$tbl = "area";
$a = "a";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.c".$a."_listid AND b.c".$a."_active = true AND b.c".$a."_capid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = $helperObj->mysql_fetch_all($a_sql);
//FUNDSOURCE
$fld = "capital_fundsource";
$tbl = "fundsource";
$a = "s";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.c".$a."_listid AND b.c".$a."_active = true AND b.c".$a."_capid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = $helperObj->mysql_fetch_all($a_sql);


//FORECAST
$f_sql = "SELECT * FROM ".$dbref."_capital_forecast WHERE cf_capid = ".$obj_id." AND cf_active = true";
$forecast = $helperObj->mysql_fetch_all_fld($f_sql,"cf_listid");




$result_sql = "SELECT * FROM ".$dbref."_capital_results WHERE cr_capid = ".$obj_id." ORDER BY cr_timeid";
$results = $helperObj->mysql_fetch_all_fld($result_sql,"cr_timeid");


include("view_table_detail.php");
	$log_sql = "SELECT clog_date, CONCAT_WS(' ',tkname,tksurname) as clog_tkname, clog_transaction FROM ".$dbref."_capital_log INNER JOIN assist_".$cmpcode."_timekeep ON clog_tkid = tkid WHERE clog_capid = '".$obj_id."' AND clog_yn = 'Y' ORDER BY clog_id DESC";
	$log_res = $helperObj->mysql_fetch_all($log_sql);
$helperObj->displayAuditLog($log_res,array('date'=>"clog_date",'user'=>"clog_tkname",'action'=>"clog_transaction"));

?>