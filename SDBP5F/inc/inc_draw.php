<?php
/****************************************
This file is for large functions which draw pages
******************************************/





function drawPLC($plcrow) {
	global $dbref;
	$plcid = $plcrow['plcid'];
	//PLC DETAILS
	$echo = "<h2>Project Life Cycle</h2>
				<table>
					<tr>
						<th class=left>Project Ref:</th>
						<td>PLC".$plcrow['plcid']."</td>
					</tr>
					<tr>
						<th class=left>Project Name:</th>
						<td>".$plcrow['plcname']."</td>
					</tr>
					<tr>
						<th class=left>Project Description:&nbsp;</th>
						<td>".$plcrow['plcdescription']."</td>
					</tr>
					<tr>
						<th class=left>Start Date:</th>
						<td>".date("d M Y",$plcrow['plcstartdate'])."</td>
					</tr>
					<tr>
						<th class=left>End Date:</th>
						<td>".date("d M Y",$plcrow['plcenddate'])."</td>
					</tr>
				</table>";

	//PLC PHASES
	$total_target = 0;
	$echo.= "<h3>Project Phases</h3>
			<table>
				<tr>
					<th height=27 colspan=2 >Project Phase</th>
					<th width=100>Calendar<br>Days</th>
					<th>Start<br>Date</th>
					<th>End<br>Date</th>
					<th>% Target<br>of Project</th>
					<th>Completed?</th>
					<th>Date of<br>Completion</th>
				</tr>";
	$sql = "SELECT * FROM ".$dbref."_list_plc_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
		$phases_list_std = mysql_fetch_alls($sql,"id");
	$sql2 = "SELECT * FROM ".$dbref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
		$phases_plc = mysql_fetch_alls2($sql2,"pptype","ppphaseid"); 
	$sql = "SELECT * FROM ".$dbref."_list_plc_proc WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
		$phases_list['P'] = mysql_fetch_alls($sql,"id");
	$sql = "SELECT * FROM ".$dbref."_list_plc_custom WHERE yn = 'Y' OR yn = 'R' ORDER BY value";
		$phases_list['A'] = mysql_fetch_alls($sql,"id");
	//arrPRint($phases_plc);
	foreach($phases_list_std as $pi => $pp) {
		switch($pp['type']) {
			case "A":	$rowspan = isset($phases_plc['C']) ? count($phases_plc['C']) : 0;	$colspan = 1;	$pph = isset($phases_plc['C']) ? $phases_plc['C'] : array();	break;
			case "P":	$rowspan = isset($phases_plc['P']) ? count($phases_plc['P']) : 0;	$colspan = 1;	$pph = isset($phases_plc['C']) ? $phases_plc['P'] : array();	break;
			case "S":	$rowspan = (isset($phases_plc['S'][$pi]) ? 1 : 0);					$colspan = 2;	$pph = (isset($phases_plc['S'][$pi]) ? array(0=>$phases_plc['S'][$pi]) : array());	break;
		}
		$p = 0;
		if($rowspan>0) {
			foreach($pph as $row2) {
				$p++;
				$ppdone = "";
				$ppdonedate = "";
				if($row2['ppdone']=="Y") {
					$ppdone = "<img src=/pics/tick_sml.gif>";
					$ppdonedate = (strlen($row2['ppdonedate'])>0) ? date("d M Y",$row2['ppdonedate']) : "";
				}
				$echo.="		<tr>";
				if($p==1) {
					$echo.="	<td colspan=$colspan rowspan=$rowspan ><b>".$pp['value'].":</b></td>";
				}
				if($pp['type']!="S") {
					$echo.="	<td class=i>".$phases_list[$pp['type']][$row2['ppphaseid']]['value'].":</td>";
				}
				$echo.="	<td class=center>".$row2['ppdays']." day".($row2['ppdays']!=1 ? "s" : "")."</td>
							<td class=center>".date("d M Y",$row2['ppstartdate'])."</td>
							<td class=center>".date("d M Y",$row2['ppenddate'])."</td>
							<td class=center>".$row2['pptarget']." %</td>
							<td class=center>".$ppdone."</td>
							<td class=center>".$ppdonedate."</td>
						</tr>";
				$total_target += $row2['pptarget'];
			}
		}
	}
	$echo.="	<tr>
					<th  colspan=5 class=right>Total Target % of Project:</th>
					<th class=center>".$total_target." %</th>
					<th class=center colspan=2>&nbsp;</th>
				</tr>
			</table>";
				
	return $echo;


}


/*******************************
******* INPUT VARIABLES ********
$plcid = id of plc
$plctbl=>Which table = current=>phases or temp=>phases_temp
$plcsrc=>Log text = new => Created PLC or existing => Updated PLC
*******************************/
function logPLC($plcid,$plctbl,$plcsrc) {
	global $dbref, $cmpcode;
	$modtxt = $_SESSION['modtext'];
	$modref = $_SESSION['modref'];
	$echo = "";
	if($plctbl=="current") { 
		$tbl = ""; 
		$orderby = "plcid";
	} else { 
		$tbl = "_temp"; 
		$orderby = "tmpid";
	}
	//HEADER
$echo.= htmlPageHeader()."<h1>".$modtxt.": Project Life Cycle History</h1>
<p>Project Life Cycle as at ".date("d M Y H:i:s")." [updated by: ".$_SESSION['tkn']."]";
	
	//PLC DETAILS
	$sql = "SELECT * FROM ".$dbref."_kpi_plc".$tbl." WHERE plcid = ".$plcid." ORDER BY $orderby DESC";
	$plcrow = mysql_fetch_one($sql);

	$echo.= "<h2>Project Life Cycle</h2>
				<table>
					<tr>
						<th class=left>Project Ref:</th>
						<td>PLC".$plcrow['plcid']."</td>
					</tr>
					<tr>
						<th class=left>Project Name:</th>
						<td>".$plcrow['plcname']."</td>
					</tr>
					<tr>
						<th class=left>Project Description:&nbsp;</th>
						<td>".$plcrow['plcdescription']."</td>
					</tr>
					<tr>
						<th class=left>Start Date:</th>
						<td>".date("d M Y",$plcrow['plcstartdate'])."</td>
					</tr>
					<tr>
						<th class=left>End Date:</th>
						<td>".date("d M Y",$plcrow['plcenddate'])."</td>
					</tr>
				</table>";

	//PLC PHASES
	$total_target = 0;
	$echo.= "<h3>Project Phases</h3>
			<table>
				<tr>
					<th height=27 colspan=2 >Project Phase</th>
					<th width=100>Calendar<br>Days</th>
					<th>Start<br>Date</th>
					<th>End<br>Date</th>
					<th>% Target<br>of Project</th>
					<th>Completed?</th>
					<th>Date of<br>Completion</th>
				</tr>";
	$sql = "SELECT * FROM ".$dbref."_list_plc_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
		$phases_list_std = mysql_fetch_alls($sql,"id");
	$sql2 = "SELECT * FROM ".$dbref."_kpi_plc_phases".$tbl." WHERE ppplcid = ".$plcid;
		$phases_plc = mysql_fetch_alls2($sql2,"pptype","ppphaseid");
	$sql = "SELECT * FROM ".$dbref."_list_plc_proc WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
		$phases_list['P'] = mysql_fetch_alls($sql,"id");
	$sql = "SELECT * FROM ".$dbref."_list_plc_custom WHERE yn = 'Y' OR yn = 'R' ORDER BY value";
		$phases_list['A'] = mysql_fetch_alls($sql,"id");
	//arrPRint($phases_plc);
	foreach($phases_list_std as $pi => $pp) {
		switch($pp['type']) {
			/*case "A":	$rowspan = count($phases_plc['C']);	$colspan = 1;	$pph = $phases_plc['C'];	break;
			case "P":	$rowspan = count($phases_plc['P']);	$colspan = 1;	$pph = $phases_plc['P'];	break;
			case "S":	$rowspan = 1;	$colspan = 2;						$pph = array(0=>$phases_plc['S'][$pi]);	break;
		}
		$p = 0;*/
			case "A":	$rowspan = isset($phases_plc['C']) ? count($phases_plc['C']) : 0;	$colspan = 1;	$pph = isset($phases_plc['C']) ? $phases_plc['C'] : array();	break;
			case "P":	$rowspan = isset($phases_plc['P']) ? count($phases_plc['P']) : 0;	$colspan = 1;	$pph = isset($phases_plc['C']) ? $phases_plc['P'] : array();	break;
			case "S":	$rowspan = (isset($phases_plc['S'][$pi]) ? 1 : 0);					$colspan = 2;	$pph = (isset($phases_plc['S'][$pi]) ? array(0=>$phases_plc['S'][$pi]) : array());	break;
		}
		$p = 0;
		if($rowspan>0) {
			foreach($pph as $row2) {
				$p++;
				$ppdone = "";
				$ppdonedate = "";
				if($row2['ppdone']=="Y") {
					$ppdone = "<img src=https://assist.ignite4u.co.za/pics/tick_sml.gif>";
					$ppdonedate = (strlen($row2['ppdonedate'])>0) ? date("d M Y",$row2['ppdonedate']) : "";
				}
				$echo.="		<tr>";
				if($p==1) {
					$echo.="	<td colspan=$colspan rowspan=$rowspan ><b>".$pp['value'].":</b></td>";
				}
				if($pp['type']!="S") {
					$echo.="	<td class=i>".$phases_list[$pp['type']][$row2['ppphaseid']]['value'].":</td>";
				}
				$echo.="	<td class=center>".$row2['ppdays']." day".($row2['ppdays']!=1 ? "s" : "")."</td>
							<td class=center>".date("d M Y",$row2['ppstartdate'])."</td>
							<td class=center>".date("d M Y",$row2['ppenddate'])."</td>
							<td class=center>".$row2['pptarget']." %</td>
							<td class=center>".$ppdone."</td>
							<td class=center>".$ppdonedate."</td>
						</tr>";
				$total_target += $row2['pptarget'];
			}
		}
	}
	$echo.="	<tr>
					<th  colspan=5 class=right>Total Target % of Project:</th>
					<th class=center>".$total_target." %</th>
					<th class=center colspan=2>&nbsp;</th>
				</tr>
			</table>";
	$echo.="</body></html>";
	

	$path = strtoupper($modref)."/PLC";
	checkFolder($path);
	$path = "../../files/".$cmpcode."/".$path."/";
	$filen = "PLC".$plcid."_".date("Ymd_His").".html";
	$filename = $path.$filen;
	$file = fopen($filename,"w");
	fwrite($file,$echo."\n");
	fclose($file);
	
	
	return $filename;


}




?>