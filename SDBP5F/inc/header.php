<?php
error_reporting(-1);
if(isset($_SESSION['ia_cmp_reseller']) && $_SESSION['ia_cmp_reseller']=="IGN0001") { $has_pmsps = true; } else { $has_pmsps = false; }


/****** NEW CLASS STRUCTURE *****/
require_once ("../inc_assist.php");
require_once("../module/autoloader.php");
spl_autoload_register("Loader::autoload");
$helperObj = new SDBP5B_HELPER();
$me = new SDBP5B();
$me_lists = new LISTS();
$listObj = new LISTS();
$headObj = new HEADINGS();
$timeObj = new STIME();
$drawObj = new DISPLAY();
$result_settings = $me->getResultSettings();
/************************************/



$page_src = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['PHP_SELF'];
$p = explode("?",$page_src);
$p = explode("/",$p[0]);
$page_src = (isset($p[3])?$p[3]:"").(isset($src) ? "_".$src : "");

$self = $_SERVER['PHP_SELF'];
$page = explode("/",$self);
$self = $page[count($page)-1];
$page = substr($self,0,-4);
$base = explode("_",$page);
if(!isset($src)) {
	$src = isset($base[1]) ? $base[1] : "";
}
$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "";



$unspecified = "[Unspecified]";
$select_option_maxlen = 90;

$id_labels_all = array(
	'KPI'=>"D",
	'TOP'=>"TL",
	'CAP'=>"CP",
	'CF'=>"CF",
	'RS'=>"RS"
);
if(isset($section)) {
	$id_label = isset($id_labels_all[$section]) ? $id_labels_all[$section] : "";
	switch($section) {
		case "KPI":
		case "TOP":
			if($section=="KPI") {
				$myObj = new DEPT();
				$section_reftag = DEPT::REFTAG;
			} else {
				$myObj = new TOP();
				$section_reftag = TOP::REFTAG;
				$dept_flds = array("tr_dept","tr_dept_correct");
				//$myObj->arrPrint($myObj->getUpdateFormDetails(1));
			}
			$myObj->setPageSection($base[0]);
			$r_section = $myObj->getRHeadCode();
			$object_head = $myObj->getHeadCode();
			$table_fld = $myObj->getFieldPrefix();
			$table_tbl = $myObj->getTable();
			$table_id = $myObj->getIDFieldName();
			$get_headings = isset($get_headings) ? $get_headings : $myObj->getHeadingCodes();
			$get_time = isset($get_time) ? $get_time : $myObj->getTimeIDs();
			$first_month = $myObj->getFirstMonth();
			$time_period_size = $myObj->getTimeIncrement();
			$time_increment = $myObj->getTimeIncrement();
			$r_table_fld = $myObj->getResultsFieldPrefix();
			$section_head = $myObj->getSectionHead();
			$fld_target = $myObj->getRTargetField();
			$fld_actual = $myObj->getRActualField();
			$wa_tbl_fld = $myObj->getWardsAreaTableDetails();
			$detail_link = $myObj->getValueField();
			$absolute_required_fields = $myObj->getAbsoluteRequiredFields();
			//$section_object = new DEPT();
			break;
		case "CAP":
			$myObj = new CAPITAL();
			$table_fld = "cap_";
			$table_tbl = "capital";
			$table_id = "cap";
			$get_headings = array("CAP","CAP_H","CAP_R");
			$detail_link = "cap_name";
			$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
			$r_section = "CAP_R";
			$h_section = "CAP_H";
			$r_table_fld = "cr_";
			$section_head = "Capital Projects";
			$fld_target = "cr_target";
			$fld_actual = "cr_actual";
			$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs",'capital_wards'=>"cw",'capital_area'=>"ca",'capital_fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			$time_increment = 1;
			break;
		case "CF":
			$myObj = new CASHFLOW();
			$table_fld = "cf_";
			$table_tbl = "cashflow";
			$table_id = "cf";
			$get_headings = array("CF","CF_H","CF_R");
			$detail_link = "cf_value";
			$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
			$r_section = "CF_R";
			$h_section = "CF_H";
			$r_table_fld = "cr_";
			$section_head = "Monthly Cashflow";
			$fld_target = "4";
			$fld_actual = "5";
			//$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			$time_increment = 1;
			break;
		case "RS":
			$myObj = new REVBYSOURCE();
			$table_fld = "rs_";
			$table_tbl = "revbysrc";
			$table_id = "rs";
			$get_headings = array("RS","RS_R");
			$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
			$detail_link = "rs_value";
			$r_section = "RS_R";
			$r_table_fld = "rr_";
			$section_head = "Revenue By Source";
			$fld_target = "rr_budget";
			$fld_actual = "rr_actual";
			//$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			$time_increment = 1;
			break;
		default:
			$myObj = new SDBP5();
			$table_fld = "";
			$table_tbl = "";
			$table_id = "";
			$get_headings = array("KPI","TOP","CAP","CF","RS");
			$get_time = array();
			$detail_link = "";
			$r_section = "";
			$r_table_fld = "";
			$section_head = "";
			$fld_target = "";
			$fld_actual = "";
			//$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			$time_increment = 1;
			break;
	}
}

$user_access_fields = array(
	'finance'=>"Finance<br />Admin",
	'setup'=>"Setup",
);


$head_dir = $_SESSION['cmp_terminology']['DEPT'];
$head_sub = $_SESSION['cmp_terminology']['SUB'];

require_once("inc_ignite.php");
require_once("inc.php");
require_once("inc_draw.php");



//GET USER DETAILS
$helperObj->logUser();
$my_access = getUserAccess($tkid);

//arrPrint($my_access);

$all_time = getTime(array(1,12),false);
//$today = $_SERVER['REQUEST_TIME'];
$today = $helperObj->today;
if($today>=strtotime($all_time[12]['end_date'])) { $current_time_id = 12; } else { $current_time_id = (date("m",$today)>6 ? date("m",$today)-6 : date("m"+6,$today) ); }
$get_open_time = !isset($get_open_time) ? false : $get_open_time;
$get_lists = !isset($get_lists) ? false : $get_lists;
$result = array();

//include("demo.php");

//arrPrint($mheadings);
function breadCrumb($bc) {
	global $self;
	if($self!=$bc)
		return "<a href=".$bc." class=breadcrumb>";
	else
		return false;
}


require_once("inc_module.php");

//arrPrint($my_access);
//IF THE PAGE IS NOT A REPORT THEN DISPLAY THE BODY PORTION
if(!isset($onscreen_header) && !(($base[0]=="report" || $page_id=="change") && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE")) {
	$_REQUEST['jquery_version']="1.10.0";
	$me->displayPageHeader();
?>
	<link rel="stylesheet" href="inc/main.css" type="text/css">
	<script type ="text/javascript" src="inc/main.js?<?php echo time(); ?>"></script>
	<script type ="text/javascript" src="/library/jquery-plugins/jscolor-2.0.4/jscolor.js"></script>
	<style type=text/css>
	.hand { cursor: pointer; }
	//.noborder { border: 2px dashed #009900; background-color: #ff9999;}
  //height: 100%;
.stop-scrolling {
  overflow: hidden;
}	</style>
<?php
//$result = $_REQUEST['r'];
//arrPrint($my_access['access']);

/** GENERATE NAV BUTTONS & PAGE TITLE **/
if(!isset($page_src) || $page_src!="main_home") {
	/* 2ND TIER MENU / 1ST TIER NAV BUTTONS */
	$menu = array(
		'base_pages' => array(
							'main' 	=> $_SESSION['modtext'],
							'setup' => "Setup"
		),
		'setup' => array(
							'defaults' 	 => "Defaults",
							'useraccess' => "User Access"
		),
		'manage' => array(
//							'dept' 	 	=> "Departmental SDBIP",
//							'top'		=> "Top Layer SDBIP",
							'fin'		=> "Financials",
							//'admin'		=> "KPI Admin",
							//'topadmin'	=> "Top Layer Admin",
							//'profile'	=> "My Profile",
//							'assurance'	=> "Assurance"
		),
//		'admin' => array(
//							'admin'		=> "Departmental SDBIP",
//							'topadmin'	=> "Top Layer SDBIP",
//		),
		'view' => array(
//							'dept' 	 	=> "Departmental SDBIP",
//							'top'		=> "Top Layer SDBIP",
							'capital'	=> "Capital Projects",
							'cashflow'	=> "Monthly Cashflow",
							'revbysrc'	=> "Revenue By Source",
							'dash'		=> "YTD Dashboard",
		),
		'report' => array(
//							'dept' 	 	=> "Departmental SDBIP",
//							'top'		=> "Top Layer SDBIP",
							'capital'	=> "Capital Projects",
							'cashflow'	=> "Monthly Cashflow",
							'revbysrc'	=> "Revenue By Source",
//							'graph'		=> "Graphs",
		),
		'support'	=>	array(
							'fin'		=>	"Financials",
//							'dkpi'		=>	"Deleted Departmental KPIs",
		),
	);
	$redirect = array(
		'setup' 			=> "setup_defaults.php",
		'manage' 			=> "manage_fin.php",
		'view' 				=> "view_capital.php",
		'report'			=> "report_capital.php",
		'report_graph'			=> "report_graph_cashflow.php",
		'support_fin'		=> "support.php",
		'main'				=> "view_capital.php",
	);


	if(isset($redirect[$page])) {
		echo "<script type=text/javascript>document.location.href = '".$redirect[$page]."';</script>";
		//echo $page;
	}

	//arrPrint($my_access);

	if(!isset($show_header) || $show_header) {
	if(!isset($_REQUEST['nav']) || $_REQUEST['nav']) {

		if(isset($menu[$base[0]]) && $base[0]!="support") {
			$submenu = $menu[$base[0]];
			$nav = array();
			foreach($submenu as $id => $display) {
				$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$id.".php",'active'=>(($src==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
			}
			//echoNavigation(1,$nav);
			echo $me->generateNavigationButtons($nav, 1);// standardization [SD]
		}
		/* 3RD TIER MENU / 2ND TIER NAV BUTTONS */
		$page_section = $base[0];
		//CHECK USER ACCESS FOR ADMIN TO SEE WHERE TO REROUTE
		if($page_section=="admin" && !isset($base[1])) {
			if($my_access['access']['kpi'] && strtoupper($_SESSION['cc']) != "KZN999D") {
				echo "<script type=text/javascript>document.location.href = 'admin_admin.php';</script>";
			} elseif($my_access['access']['toplayer']) {
				echo "<script type=text/javascript>document.location.href = 'admin_topadmin.php';</script>";
			} else {
				die("<p>You do not have access to view anything in this section.  Please choose another page to view.</p>");
			}
		}
		switch($base[0]) {
			case "manage":
			case "admin":
				$m = array("profile","admin","assurance","fin","top");
				switch($base[1]) {
				case "topadmin":
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "update";
					$menu2 = array(
						'update' => "Update",
						'edit' => "Edit",
						'create' => "Create",
						'auto' => "Auto Update",
						'change' => "Audit Log Report",
					);
					$nav = array();
					foreach($menu2 as $id => $display) {
						$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
					}
					//echoNavigation(2,$nav);
					echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
					if(!isset($page_title)) {
						$page_title = $menu2[$page_id];
					}
					break;
				case "top":
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "update";
					$menu2 = array(
						'update' => "Update",
						'edit' => "Edit",
						//'approve' => "--Approve",
						'create' => "Create",
					);
					$nav = array();
					foreach($menu2 as $id => $display) {
						$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
					}
					//echoNavigation(2,$nav);
					echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
					if(!isset($page_title)) {
						$page_title = $menu2[$page_id];
					}
					echo chr(10)."<script>$(function() { ";
						if(!isset($my_access['act']['TOP']['create']) || $my_access['act']['TOP']['create']==0) {
							echo chr(10)."	$(\"#create\").button(\"option\",\"disabled\",true); ";
						}
						if(!isset($my_access['act']['TOP']['edit']) || $my_access['act']['TOP']['edit']==0) {
							echo chr(10)."	$(\"#edit\").button(\"option\",\"disabled\",true); ";
						}
					echo chr(10)."});</script>";
					break;
				case "admin":
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "deptupdate";
					$menu2 = array(
						'deptupdate' 	 	=> "Update",
						'deptadmin' 	 	=> "Edit",
						'deptcreate'		=> "Create",
						'change' 			=> "Audit Log Report",
					);

					$nav = array();
					foreach($menu2 as $id => $display) {
						$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
					}
					//echoNavigation(2,$nav);
					echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
					$page_title = $menu2[$page_id];
					break;
				case "fin":
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "capital";
					$menu2 = array(
						'capital' 	 	=> "Capital Projects",
						'cashflow'		=> "Monthly Cashflows",
						'revbysrc'	 	=> "Revenue By Source",
					);
					$nav = array();
					foreach($menu2 as $id => $display) {
						$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
					}
					//echoNavigation(2,$nav);
					echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
					$page_title = $menu2[$page_id];
					break;
				case "assurance":
					//$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "review";

					if($page_id == "review_kpi" && $my_access['access']['assurance']!=1) {
						$page_id = "deptchange";
						$_REQUEST['page_id'] = $page_id;
					}
					$menu2 = array(
						'review_kpi' 			=> "Departmental SDBIP Assurance",
						'review_kpi_report'		=> "Departmental Assurance Report",
						'deptchange' 			=> "Departmental Audit Log Report",
						'topchange' 			=> "Top Layer Audit Log Report",
					);
						//'review_top' 			=> "Top Layer SDBIP Assurance",
					$nav = array();
					foreach($menu2 as $id => $display) {
						$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
					}
					//echoNavigation(2,$nav);
					echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
					echo chr(10)."<script>$(function() { ";
						if($my_access['access']['audit_log']!=1) {
							echo chr(10)."	$(\"#deptchange\").button(\"option\",\"disabled\",true); ";
							echo chr(10)."	$(\"#topchange\").button(\"option\",\"disabled\",true); ";
						}
						if($my_access['access']['assurance']!=1) {
							echo chr(10)."	$(\"#review_kpi\").button(\"option\",\"disabled\",true); ";
							echo chr(10)."	$(\"#review_kpi_report\").button(\"option\",\"disabled\",true); ";
							echo chr(10)."	$(\"#review_top\").button(\"option\",\"disabled\",true); ";
						}
					echo chr(10)."});</script>";

					$page_title = $menu2[$page_id];
					break;
				default:
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "update";

					$menu2 = array(
						'update' => "Update",
						'edit' => "Edit",
						//'approve' => "--Approve",
						'create' => "Create",
					);
					$nav = array();
					foreach($menu2 as $id => $display) {
						$nav[$id] = array('id'=>$id,'link'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
					}
					//echoNavigation(2,$nav);
					echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
					if(!isset($page_title)) {
						$page_title = $menu2[$page_id];
					}
					echo chr(10)."<script>$(function() { ";
						if($my_access['act']['KPI']['create']==0) {
							echo chr(10)."	$(\"#create\").button(\"option\",\"disabled\",true); ";
						}
						if($my_access['act']['KPI']['edit']==0) {
							echo chr(10)."	$(\"#edit\").button(\"option\",\"disabled\",true); ";
						}
					echo chr(10)."});</script>";
					break;
				}
				echo chr(10)."<script>$(function() { ";
				if(!isset($my_access['manage']['TOP']) || !(count($my_access['manage']['TOP'])>0 || count($my_access['manage']['T_OWN'])>0)) {
					echo chr(10)."	$(\"#top\").button(\"option\",\"disabled\",true); ";
				}
				if(!$my_access['access']['toplayer']) {
					echo chr(10)."	$(\"#topadmin\").button(\"option\",\"disabled\",true); ";
				}
				if(!$my_access['access']['kpi']) {
					echo "	$(\"#admin\").button(\"option\",\"disabled\",true); ";
				}
				if(!$my_access['access']['assurance'] && !$my_access['access']['audit_log']) {
					echo chr(10)."	$(\"#assurance\").button(\"option\",\"disabled\",true); ";
				}
				if(!$my_access['access']['finance']) {
					echo chr(10)."	$(\"#fin\").button(\"option\",\"disabled\",true); ";
				}
				//echo "	$(\"#profile\").button(\"option\",\"disabled\",true); ";
				echo chr(10)."});</script>";
				break;
			case "view":
				echo "<script>$(function() {
					//$(\"#dash\").button(\"option\",\"disabled\",true);
				});</script>";
				break;
			case "report":
				if($base[1]!="graph") {
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "generate";
					$menu2 = array(
						'generate' => "Generator",
						'quick' => "Quick Reports",
		//				'fixed' => "Fixed Reports",
					);
				} else {
					$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "graph_dept";
					$menu2 = array(
						'graph_cashflow' => "Monthly Cashflow",
					);
				}
				$nav = array();
				foreach($menu2 as $id => $display) {
					$nav[$id] = array('id'=>$id,'link'=>$self."?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
				}
				//echoNavigation(2,$nav);
				echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
				$page_title = $menu2[$page_id];
				echo "<script>$(function() {
					$(\"#quick\").button(\"option\",\"disabled\",true); ";
		//			$(\"#graph\").button(\"option\",\"disabled\",true);
				echo "});</script>";

				break;
			case "setup":
				if($base[1]=="defaults") {
					if(isset($base[2]) && $base[2] == "head") {
						$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "term";
						$menu2 = array(
									'term'	=> "Terminology",
									'cap'	=> "Capital Projects",
									'cf'	=> "Monthly Cashflow",
									'rs'	=> "Revenue By Source",
						);
						$nav = array();
						foreach($menu2 as $id => $display) {
							$nav[$id] = array('id'=>$id,'link'=>$self."?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
						}
						//echoNavigation(2,$nav);
						echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
						$page_title = (isset($page_title) ? $page_title." >> " : "").$menu2[$page_id];
					} elseif(isset($base[2]) && $base[2] == "admins") {
						$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "dir";
						$page_type = isset($page_type) ? $page_type : "DEPT";
						if($page_type=="TOP") {
							$menu2 = array(
										'dir'		=> $head_dir,
										'owner'		=> strlen($mheadings['TOP']['top_ownerid']['h_client'])>0 ? $mheadings['TOP']['top_ownerid']['h_client'] : $mheadings['TOP']['top_ownerid']['h_ignite'],
										'report'	=> "Report",
							);
						} else {
							$menu2 = array(
										'dir'		=> $head_dir,
										'subdir' 	=> $head_sub,
										'owner'		=> strlen($mheadings['KPI']['kpi_ownerid']['h_client'])>0 ? $mheadings['KPI']['kpi_ownerid']['h_client'] : $mheadings['KPI']['kpi_ownerid']['h_ignite'],
										'report'	=> "Report",
							);
						}
						$nav = array();
						foreach($menu2 as $id => $display) {
							$nav[$id] = array('id'=>$id,'link'=>$self."?t=".$page_type."&page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'name'=>$display,'help'=>'');
						}
						//echoNavigation(2,$nav);
						echo $me->generateNavigationButtons($nav, 2);// standardization [SD]
						$page_title = (isset($page_title) ? $page_title." >> " : "").$menu2[$page_id];
					} elseif(isset($base[2]) && $base[2] == "dir") {
						$page_title = $head_dir;
					}
				}
				break;
			case "support":
				break;
		}
		}	//request_nav = true
		/* PAGE TITLE */
		//ONLY FOR DEVELOPMENT!!!
		//echo "<p class=float>[Pretending to be: ".date("d M Y H:i:s",$today)."]</p>";
		echo "<h1>".breadCrumb($base[0].".php").(isset($menu['base_pages'][$base[0]]) ? $menu['base_pages'][$base[0]] : ucfirst($base[0]));
		if(isset($base[1])) {
			$breadcrumb = $base[0]."_".$base[1].".php";
			echo "</a> >> ".breadCrumb($breadcrumb).(isset($menu[$base[0]][$base[1]]) ? $menu[$base[0]][$base[1]] : ucfirst($base[1]));
		}
		if(isset($page_title)) {
			echo "</a> >> ".$page_title;
		}
		echo "</a></h1>";
	}//onscreenheader

	//arrPrint($my_access);
} 	//end if page_src
}	//endif display header
if(isset($section)) {
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		"dir",$table_fld."subid",
		$table_fld."annual",
		$table_fld."revised",
		$table_tbl."_wards",
		$table_tbl."_area",
		$table_tbl."_repcate",
		$table_tbl."_fundsource",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
		$table_fld."pmsref",
		"cap_cpref", "cap_name","cap_descrip","cap_planstart","cap_planend","cap_actualstart","cap_actualend",
		$table_fld."vote",
	);
}



?>