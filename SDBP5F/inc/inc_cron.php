<?php
function getFileDate($fn) {
	$fdate = strFn("substr",$fn,0,8);
	$d = strFn("substr",$fdate,6,2);
	$m = strFn("substr",$fdate,4,2);
	$y = strFn("substr",$fdate,0,4);
	$ftime = strFn("substr",$fn,9,6);
	$H = strFn("substr",$ftime,0,2);
	$M = strFn("substr",$ftime,2,2);
	$s = strFn("substr",$ftime,4,2);
	return mktime($H,$M,$s,$m,$d,$y);
}



	
/******
	TL Auto Update
		*******/

function returnTLAutoUpdate($cmpcode) {		
	global $modref;
	global $modtitle;
	global $today;
	$total_records_changed = 0;

$id_labels_all = array(
	'KPI'=>"D",
	'TOP'=>"TL",
	'CAP'=>"CP",
	'CF'=>"CF",
	'RS'=>"RS"
);

			global $dbref;
			global $section;
			global $lists;
			global $time;

	$top_time = array(
		3	=>	$time[3],
		6	=>	$time[6],
		9	=>	$time[9],
		12	=>	$time[12]
	);
	$top_time[3]['start_stamp'] = $time[1]['start_stamp'];
	$top_time[6]['start_stamp'] = $time[4]['start_stamp'];
	$top_time[9]['start_stamp'] = $time[7]['start_stamp'];
	$top_time[12]['start_stamp'] = $time[10]['start_stamp'];
	$top_time[3]['start_date'] = $time[1]['start_date'];
	$top_time[6]['start_date'] = $time[4]['start_date'];
	$top_time[9]['start_date'] = $time[7]['start_date'];
	$top_time[12]['start_date'] = $time[10]['start_date'];

	$start_time = 3;

	/* PAGE HEADER */
	$echo = "<html>
				<head>
					<title>www.Action4u.co.za</title>
					<link rel=\"stylesheet\" href=\"/library/jquery/css/jquery-ui.css\" type=\"text/css\">
					<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\">
					<link rel=\"stylesheet\" href=\"/styles/SDBP4.css\" type=\"text/css\">
				</head>
				<style>
					table td.log {
						font-style: italic;
						font-size: 7pt;
						background-color: #f5f5f5;
					}
					table th.log {	
						font-size: 7pt;
					}
					.noborder { border: 0px; }
				</style>
				<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
				<h1>SDBIP $modtitle: Top Level: Automatic Update</h1>
				<table cellpadding=3 cellspacing=0>";

	/* TABLE HEADER */
	$echo.=chr(10)."<thead>
				<tr>
					<th rowspan=3>Ref</th>
					<th rowspan=3>Directorate</th>
					<th rowspan=3>KPI<img src=/pics/blank.gif height=1 width=200 /></th>
					<th rowspan=3>Unit of measurement<img src=/pics/blank.gif height=1 width=200 /></th>
					<th rowspan=3>KPI Owner</th>
					<th rowspan=3>Baseline</th>
					<th rowspan=3>Annual Target</th>
					<th rowspan=3>Revised Target</th>
					<th rowspan=3>Assoc. Dept. KPIs</th>
					<th rowspan=3>Calculation Type</th>
					<th rowspan=3>Target Type</th>
					<th rowspan=3>Auto Update</th>";
		foreach($top_time as $tti => $tt) {
			if($tt['start_stamp']<$today) { $colspan = 12; $rowspan=1; $start_time = $tti; } else { $colspan = 1; $rowspan=3; }
			$echo.="<th rowspan=$rowspan colspan=$colspan>Quarter ending ".$tt['display_full']."</th>";
		}
	$echo.="	</tr>
				<tr>";
		foreach($top_time as $tti => $tt) {
			if($tt['start_stamp']<$today) {
				$echo.="		<th colspan=3 class=time1>Original Results</th>
								<th colspan=5 class=time2>Departmental KPI Actuals</th>
								<th colspan=3 class=time3>New Results</th>
								<th rowspan=2>Final Result</th>";
			}
		}
	$echo.="	</tr>
				<tr>";
		foreach($top_time as $tti => $tt) {
			if($tt['start_stamp']<$today) {
				$echo.="
						<th class=time1>Target</th>
						<th class=time1>Actual</th>
						<th class=time1>R</th>
						<th class=time2>&nbsp;</th>
						<th class=time2>".($tti-2)."</th>
						<th class=time2>".($tti-1)."</th>
						<th class=time2>$tti</th>
						<th class=time2>Q</th>
						<th class=time3>Target</th>
						<th class=time3>Actual</th>
						<th class=time3>R</th>";
			}
		}
	$echo.="	</tr>
			</thead>";


	/* TABLE BODY */
	$echo.=chr(10)."<tbody>";

	//Associated Dept KPIS
	$dept_sql = "SELECT * FROM ".$dbref."_kpi WHERE kpi_active = true AND kpi_topid > 0";
	$dept_kpis = mysql_fetch_alls2($dept_sql,"kpi_topid","kpi_id");
	$dept_res_sql = "SELECT * FROM ".$dbref."_kpi_results WHERE kr_kpiid IN (SELECT kpi_id FROM ".$dbref."_kpi WHERE kpi_active = true AND kpi_topid > 0) AND kr_timeid <= ".$start_time." ORDER BY kr_kpiid, kr_timeid";
	$dept_results = mysql_fetch_alls2($dept_res_sql,"kr_kpiid","kr_timeid");
	//arrPrint($dept_results);
	//TO PObjects
	$object_sql = "SELECT t.* FROM ".$dbref."_top t
	INNER JOIN ".$dbref."_dir dir ON t.top_dirid = dir.id AND dir.active = true
	WHERE top_active = true ";
	$objects = mysql_fetch_alls($object_sql,"top_id");

	$result_sql = "SELECT * FROM ".$dbref."_top_results WHERE tr_timeid <= ".$start_time." ORDER BY tr_topid, tr_timeid";
	$results = mysql_fetch_alls2($result_sql,"tr_topid","tr_timeid");
	$res_action = "SUM";
	//arrPrint($results);
	
	foreach($objects as $obj_id => $obj) {
if(isset($dept_kpis[$obj_id])) {
		unset($res_action);
		$topecho = "";
		$dkpi = count($dept_kpis[$obj_id]);
		$obj_ct = $obj['top_calctype'];
		$obj_tt = $obj['top_targettype'];
		$err = false;
		$errtxt = array();
		foreach($dept_kpis[$obj_id] as $k_id => $k) {
			$topecho.=chr(10)."<tr>
								<th rowspan=2 class=log>".$id_labels_all['KPI'].$k_id."</th>
								<td rowspan=2 class=log>".$lists['subdir'][$k['kpi_subid']]['value']."</td>
								<td rowspan=2 class=log><small>".$k['kpi_value']."</small></td>
								<td rowspan=2 class=log><small>".$k['kpi_unit']."</small></td>
								<td rowspan=2 class=log>".$lists['owner'][$k['kpi_ownerid']]['value']."</td>
								<td rowspan=2 class=log>".$k['kpi_baseline']."</td>
								<td rowspan=2 class='log center'></td>
								<td rowspan=2 class='log center'></td>
								<td rowspan=2 class='log center'></td>
								<td rowspan=2 class='log center'>".$lists['calctype'][$k['kpi_calctype']]['value']."</td>
								<td rowspan=2 class='log center'>".$lists['targettype'][$k['kpi_targettype']]['code']."</td>";
					if($k['kpi_calctype']!=$obj['top_calctype'] || $k['kpi_targettype']!=$obj['top_targettype']) { 
						$err = true; 
						if($k['kpi_calctype']!=$obj['top_calctype']) {
							$errtxt[] = $id_labels_all['KPI'].$k_id.": Calculation Type mismatch";
						}
						if($k['kpi_targettype']!=$obj['top_targettype']) {
							$errtxt[] = $id_labels_all['KPI'].$k_id.": Target Type mismatch";
						}
					}
					//ACTUALS
			foreach($top_time as $tti => $tt) {
				$d_v = array();
				if($tt['start_stamp']<$today) {
					$res = $results[$obj_id][$tti];
					if(!isset($res['tr_auto']) || $res['tr_auto']==1) {
						//original values
							$topecho.="<td rowspan=2 colspan=3></td>";
						//kpi
						$values = array('target'=>array(),'actual'=>array());
						if($err) {
							$topecho.="<td colspan=5></td>";
						} else {
							$topecho.="<td class=time2><b>A:</b></td>";
							for($i=$tti-2;$i<=$tti;$i++) {
								$d_res = $dept_results[$k_id][$i];
								$d_values['actual'][$i] = $d_res['kr_actual'];	$d_v['actual'][$i] = $d_res['kr_actual'];
								$d_values['target'][$i] = $d_res['kr_target'];	$d_v['target'][$i] = $d_res['kr_target'];
								$topecho.="<td class=time2>".KPIresultDisplay($d_res['kr_actual'],$obj_tt)."</td>";
							}
							$d_r = KPIcalcResult($d_v,$obj_ct,array($tti-2,$tti),"ALL");
							$results[$obj_id][$tti]['kpi']['target'][] = $d_r['target'];
							$results[$obj_id][$tti]['kpi']['actual'][] = $d_r['actual'];
							//Create tr_dept
							$topecho.="<td class=time2><b>".KPIresultDisplay($d_r['actual'],$obj_tt)."</b></td>";
						}
						//new values
							$topecho.="<td rowspan=2 colspan=3></td>";
					} else {
						$topecho.="<td colspan=11 ></td>";
					}
				}
			} 
			$topecho.="</tr><tr>";
					//TARGETS
			foreach($top_time as $tti => $tt) {
				if($tt['start_stamp']<$today) {
					$res = $results[$obj_id][$tti];
					if(!isset($res['tr_auto']) || $res['tr_auto']==1) {
						if($err) {
								$topecho.="<td colspan=5></td>";
						} else {
							$topecho.="<td class=time2><b>T:</b></td>";
							for($j=($tti-2);$j<=$tti;$j++) {
								$topecho.="<td class=time2>".KPIresultDisplay($d_values['target'][$j],$obj_tt)."</td>";
							}
							$topecho.="<td class=time2><b>".KPIresultDisplay($d_r['target'],$obj_tt)."</b></td>";
						}
					} else {
						$topecho.="<td colspan=11 ></td>";
					}
				}
			} 
			
			$topecho.="		</tr>";
		}
		if($err) {
			$results[$obj_id]['act'] = array('err'=>$err,'td'=>"ui-state-error",'icon'=>"ui-icon-closethick",'txt'=>implode("<br />",$errtxt));
		} else {
			$results[$obj_id]['act'] = array('err'=>$err,'td'=>"ui-state-ok",'icon'=>"ui-icon-check",'txt'=>"All OK");
		}
		$echo.=chr(10)."<tr>
							<th>".$id_labels_all['TOP'].$obj_id."</th>
							<td>".$lists['dir'][$obj['top_dirid']]['value']."</td>
							<td><small>".$obj['top_value']."</small></td>
							<td><small>".$obj['top_unit']."</small></td>
							<td>".$lists['owner'][$obj['top_ownerid']]['value']."</td>
							<td>".$obj['top_baseline']."</td>
							<td class=right>".KPIresultDisplay($obj['top_annual'],$obj_tt)."</td>
							<td class=right>".KPIresultDisplay($obj['top_revised'],$obj_tt)."</td>
							<td class=center>$dkpi</td>
							<td class=center>".$lists['calctype'][$obj['top_calctype']]['value']."</td>
							<td class=center>".$lists['targettype'][$obj['top_targettype']]['code']."</td>
							<td rowspan=".(count($dept_kpis[$obj_id])*2+1)." class=\"".$results[$obj_id]['act']['td']."\" id=TD".$obj_id."><span id=ICON".$obj_id." class=\"ui-icon ".$results[$obj_id]['act']['icon']."\" style=\"float: left;\"></span><label id=TXT".$obj_id.">".$results[$obj_id]['act']['txt']."</label></td>";
		
			$res_acts = array();
			foreach($top_time as $tti => $tt) {
				if(!isset($res_action)) {
					$update_action = "";
					$res = $results[$obj_id][$tti];
					$kt_count = 0;
					if(count($res['kpi']['target'])>0) {
						foreach($res['kpi']['target'] as $x) {
							$kt_count+=($x>0 ? 1 : 0);
						}
					}
					if($kt_count>0) {
						if( ( array_sum($res['kpi']['target']) / $kt_count ) ==$res['tr_target']) { 
							$res_action = "AVE"; 
						} else { 
							$res_action = "SUM"; 
						}
					}
				}
			}
			if(!isset($res_action)) {
				$res_action = "SUM";
			}

			foreach($top_time as $tti => $tt) {
				$update_action = "";
				if($tt['start_stamp']<$today) {
					$res = $results[$obj_id][$tti];
					if(!isset($res['tr_auto']) || $res['tr_auto']==1) {
						$values = array('target'=>array(),'actual'=>array());
						$values['target'][$tti] = $res['tr_target'];
						$values['actual'][$tti] = $res['tr_actual'];
						$r = KPIcalcResult($values,$obj_ct,array(),$tti); 
						//original values
							$echo.="<td class=\"time1 right\">".KPIresultDisplay($res['tr_target'],$obj_tt)."</td>";
							$echo.="<td class=\"time1 right\">".KPIresultDisplay($res['tr_actual'],$obj_tt)."</td>";
							$echo.="<td class=\"time1\"><div class=\"".$r['style']."\">".$r['text']."</div></td>";
						//kpi
							$echo.="<td colspan=5></td>";
						if($err) {
							$echo.="<td colspan=5></td>";
							$update_action = "No update was done due Calculation/Target type mismatch."; $update_icon = "error";
						} else {
						//new values
							$new_values = array();
							$new_values['target'][$tti] = $res['tr_target'];
//							if( ( array_sum($res['kpi']['target']) / count($res['kpi']['target']) ) ==$res['tr_target'] && !isset($res_action)) { $res_action = "AVE"; } elseif(!isset($res_action)) { $res_action = "SUM"; }
							$new_actual = array_sum($res['kpi']['actual']);
							if($res_action == "AVE") { $new_actual/=count($res['kpi']['actual']); }
							$new_values['actual'][$tti] = $new_actual;
							$echo.="<td class=\"time3 right\">".KPIresultDisplay($res['tr_target'],$obj_tt)."</td>";
							$echo.="<td class=\"time3 right\">".KPIresultDisplay($new_actual,$obj_tt)."<br />[".$res_action."]</td>";
							$new_r = KPIcalcResult($new_values,$obj_ct,array(),$tti);
							$echo.="<td class=time3><div class=".$new_r['style'].">".$new_r['text']."</div></td>";
							
							//DETERMINE ACTION TO BE TAKEN
							$update_actual = false;
							$update_tr_dept = false;
							$update_tr_dept_correct = false;
							$trans = array();
							if(number_format($new_actual,2,".","")!=number_format($res['tr_actual'],2,".","")) {
								$update_actual = true;
									$fld = "tr_actual";
									$new = number_format($new_actual,2,".",""); $old = $res['tr_actual'];
									$log_trans = "Updated Actual for ".$time[$tti]['display_full']." to \'$new\' from \'$old\'";
									$trans[$fld]['lt'] = $log_trans;
									$trans[$fld]['n'] = $new;
									$trans[$fld]['o'] = $old;
							}
							if($update_actual) {// || $update_tr_dept || $update_tr_dept_correct) {
								$updates = array();
								$update_actions = array();
								/*if($update_actual && !$update_tr_dept) {
									//$update_action = "Updated actual";
									$updates[] = "tr_actual = ".number_format($new_actual,2,".","");
								} elseif(!$update_actual && $update_tr_dept) {
									$update_action = "Updated Departmental Comments";
									$updates[] = "tr_dept = '".code($tr_dept)."'";
								} else {
									$update_action = "Updated actual and departmental comments";
									$updates[] = "tr_actual = ".number_format($new_actual,2,".","");
									$updates[] = "tr_dept = '".code($tr_dept)."'";
								}*/
								$updates[] = "tr_actual = ".number_format($new_actual,2,".","");
								$update_actions[] = "Actual";
								$update_icon = "ok";
								$update_action = "Updated ".implode(", ",$update_actions);
								$sql = "UPDATE ".$dbref."_top_results SET ".implode(", ",$updates).", tr_update = 1, tr_updateuser = 'AUTO', tr_updatedate = now() WHERE tr_timeid = ".$tti." AND tr_topid = ".$obj_id;
								$total_records_changed+=db_update($sql);
								foreach($trans as $fld => $lt) {
									$v = array(
										'fld'=>$fld,
										'timeid'=>$tti,
										'text'=>$lt['lt'],
										'old'=>$lt['o'],
										'new'=>$lt['n'],
										'act'=>"A",
										'YN'=>"Y"
									);
									logChanges("TOP",$obj_id,$v,code($sql));
								}
							} else {
								$update_action = "No changes were found";
								$update_icon = "";
							}
						}
					} else {
						$echo.="<td colspan=11 ></td>";
						$update_action = "KPI locked for manual updating of this time period."; $update_icon = "info";
					}
					$echo.="<td class=time4 rowspan=".(count($dept_kpis[$obj_id])*2+1)."><div class=ui-state-".$update_icon.">".$update_action."</div></td>";
				} else {
					$echo.="<td rowspan=".(count($dept_kpis[$obj_id])*2+1).">Time period not yet open for updating.</td>";
				}
			}		
		$echo.="		</tr>".$topecho;
}
	}
	$echo.="</tbody>";
	/* END PAGE */
	$echo.="</table><p>Update run at ".date("d M Y H:i:s")." - ".$total_records_changed." change(s) were made.</p></body></html>";

	$path = strtoupper($modref)."/STASKS/".$section;
	checkFolder($path);
	$filename = "../files/".$cmpcode."/".$path."/".date("Ymd_His").".html";
	$file = fopen($filename,"w");
	fwrite($file,$echo."\n");
	fclose($file);
//echo $echo;
	return $total_records_changed;
	
}
		
		

?>