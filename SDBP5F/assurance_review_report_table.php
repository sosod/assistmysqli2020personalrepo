<?php
$var = $_REQUEST;
//ASSIST_HELPER::arrPrint(get_defined_vars());

$_REQUEST['act'] = "GENERATE";
$_REQUEST['fields'] = Array(
            0 => "kpi_value",
            1 => "kpi_unit",
            2 => "kpi_ownerid",
            3 => "kpi_poe",
            4 => "kpi_calctype",
            5 => "results",
            6 => "kr_perf",
            7 => "kr_correct",
            8 => "kr_attachment",
            9 => "kr_assurance_status",
            10=> "assurance_logs",
        );

$_REQUEST['r_from'] = $var['filter']['when'];
$_REQUEST['r_to'] = $var['filter']['when'];
$_REQUEST['r_calc'] = "captured";
$_REQUEST['summary'] = "N";
$_REQUEST['filter'] = Array(
	'result'=>"ALL",
	'kpi_wards' => Array("ANY"),
	'kpi_area' => Array("ANY"),
	'kpi_repcate' => Array("ANY"),
	'kpi_topid' => Array("ANY"),
	'kpi_capitalid' => Array("ANY"),
);

if($var['filter']['who']!="ALL") {
	$x = explode("_",$var['filter']['who']);
	if($x[0]=="D") {
		$_REQUEST['filter']['dir'] = array($x[1]);
	} else {
		$_REQUEST['filter']['kpi_subid'] = array($x[1]);
	}
}
$_REQUEST['filter']['kr_assurance_status'] = $var['filter']['what'];
/*
switch($var['filter']['what']) {
	case "all":			//all assurance status
		$_REQUEST['filter']['kr_assurance_status'] = array("ALL");
		break;
	case "ok":			///signed off
		$_REQUEST['filter']['kr_assurance_status'] = array(KPI::ASSURANCE_ACCEPT);
		break;
	case "no_all":		//all rejected
		$_REQUEST['filter']['kr_assurance_status'] = KPI::ASSURANCE_ACCEPT;
	case "no_no":		//rejected without updated
	case "no_yes":		//rejected but user responded
	case "na_all":		//not reviewed
	case "na_do":		//not reviewed but with target
	case "na_ignore":	//not reviewed but with no target
}
*/
$_REQUEST['groupby'] = "kpi_subid";
$_REQUEST['sort'] = Array(
            0 => "kpi_id",
            1 => "dir",
            2 => "kpi_gfsid",
            3 => "kpi_idpref",
            4 => "kpi_natoutcomeid",
            5 => "kpi_idpid",
            6 => "kpi_natkpaid",
            7 => "kpi_munkpaid",
            8 => "kpi_pdoid",
            9 => "kpi_ndpid",
            10 => "kpi_value",
            11 => "kpi_unit",
            12 => "kpi_conceptid",
            13 => "kpi_typeid",
            14 => "kpi_riskref",
            15 => "kpi_risk",
            16 => "kpi_riskratingid",
            17 => "kpi_ownerid",
            18 => "kpi_baseline",
            19 => "kpi_pyp",
            20 => "kpi_perfstd",
            21 => "kpi_poe",
            22 => "kpi_mtas",
            23 => "kpi_annual",
            24 => "kpi_revised",
        );
$_REQUEST['output'] = "display";
$_REQUEST['rhead'] = "";
$_REQUEST['jquery_version'] = "1.10.0";
$_REQUEST['show_title'] = false;

	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",$table_fld."repcate",
		"dir",$table_fld."subid",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
	);
	$head0['dir'] = $mheadings['dir'][0];
	$head = array_merge($head0,$mheadings[$section]);

//ASSIST_HELPER::arrPrint($mheadings[$r_section]);
$mheadings[$r_section]['kr_assurance_status'] = array(
	'h_client' => "Assurance Status",
	'h_ignite' => "Assurance Status",
	'id' => 54,
	'head_id' => 46,
	'section' => "KPI_R",
	'fixed' => 0,
	'i_sort' => 199,
	'c_sort' => 199,
	'c_list' => 1,
	'i_required' => 0,
	'c_required' => 0,
	'i_maxlen' => 0,
	'c_maxlen' => 0,
	'field' => "kr_assurance_status",
	'active' => 1,
	'glossary' => "",
	'h_type' => "STAT",
	'h_table' =>"" ,
);
$mheadings[$r_section]['assurance_logs'] = array(
	'h_client' => "Assurance Logs",
	'h_ignite' => "Assurance Logs",
	'id' => 54,
	'head_id' => 46,
	'section' => "KPI_R",
	'fixed' => 0,
	'i_sort' => 199,
	'c_sort' => 199,
	'c_list' => 1,
	'i_required' => 0,
	'c_required' => 0,
	'i_maxlen' => 0,
	'c_maxlen' => 0,
	'field' => "assurance_logs",
	'active' => 1,
	'glossary' => "",
	'h_type' => "LOG",
	'h_table' =>"" ,
);
include("assurance_report_process.php");








/**************

Array
(
    [act] => GENERATE
    [page_id] => generate
    [fields] => Array
        (
            [0] => dir
            [1] => kpi_subid
            [2] => kpi_topid
            [3] => kpi_gfsid
            [4] => kpi_idpref
            [5] => kpi_natoutcomeid
            [6] => kpi_idpid
            [7] => kpi_natkpaid
            [8] => kpi_munkpaid
            [9] => kpi_pdoid
            [10] => kpi_ndpid
            [11] => kpi_capitalid
            [12] => kpi_value
            [13] => kpi_unit
            [14] => kpi_conceptid
            [15] => kpi_typeid
            [16] => kpi_riskref
            [17] => kpi_risk
            [18] => kpi_riskratingid
            [19] => kpi_wards
            [20] => kpi_area
            [21] => kpi_ownerid
            [22] => kpi_baseline
            [23] => kpi_pyp
            [24] => kpi_perfstd
            [25] => kpi_poe
            [26] => kpi_mtas
            [27] => kpi_repcate
            [28] => kpi_annual
            [29] => kpi_revised
            [30] => kpi_calctype
            [31] => results
            [32] => kr_perf
            [33] => kr_correct
            [34] => kr_attachment
        )

    [r_from] => 1
    [r_to] => 4
    [r_calc] => captured
    [summary] => Y
    [filter] => Array
        (
            [dir] => Array
                (
                    [0] => ANY
                )

            [kpi_subid] => Array
                (
                    [0] => ANY
                )

            [kpi_topid] => Array
                (
                    [0] => ALL
                )

            [kpi_gfsid] => Array
                (
                    [0] => ANY
                )

            [kpi_idpref] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_natoutcomeid] => Array
                (
                    [0] => ANY
                )

            [kpi_idpid] => Array
                (
                    [0] => ANY
                )

            [kpi_natkpaid] => Array
                (
                    [0] => ANY
                )

            [kpi_munkpaid] => Array
                (
                    [0] => ANY
                )

            [kpi_pdoid] => Array
                (
                    [0] => ANY
                )

            [kpi_ndpid] => Array
                (
                    [0] => ANY
                )

            [kpi_capitalid] => Array
                (
                    [0] => ALL
                )

            [kpi_value] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_unit] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_conceptid] => Array
                (
                    [0] => ANY
                )

            [kpi_typeid] => Array
                (
                    [0] => ANY
                )

            [kpi_riskref] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_risk] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_riskratingid] => Array
                (
                    [0] => ANY
                )

            [kpi_wards] => Array
                (
                    [0] => ANY
                )

            [kpi_area] => Array
                (
                    [0] => ANY
                )

            [kpi_ownerid] => Array
                (
                    [0] => ANY
                )

            [kpi_baseline] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_pyp] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_perfstd] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_poe] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_mtas] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_repcate] => Array
                (
                    [0] => ANY
                )

            [kpi_annual] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_revised] => Array
                (
                    [0] => 
                    [1] => ANY
                )

            [kpi_calctype] => Array
                (
                    [0] => ANY
                )

            [kpi_targettype] => Array
                (
                    [0] => ANY
                )

            [result] => ALL
        )

    [groupby] => X
    [sort] => Array
        (
            [0] => kpi_id
            [1] => dir
            [2] => kpi_gfsid
            [3] => kpi_idpref
            [4] => kpi_natoutcomeid
            [5] => kpi_idpid
            [6] => kpi_natkpaid
            [7] => kpi_munkpaid
            [8] => kpi_pdoid
            [9] => kpi_ndpid
            [10] => kpi_value
            [11] => kpi_unit
            [12] => kpi_conceptid
            [13] => kpi_typeid
            [14] => kpi_riskref
            [15] => kpi_risk
            [16] => kpi_riskratingid
            [17] => kpi_ownerid
            [18] => kpi_baseline
            [19] => kpi_pyp
            [20] => kpi_perfstd
            [21] => kpi_poe
            [22] => kpi_mtas
            [23] => kpi_annual
            [24] => kpi_revised
        )

    [output] => display
    [rhead] => 
    [jquery_version] => 1.10.0
)
*/
?>