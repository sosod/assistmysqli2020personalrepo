<?php
$width = $_SESSION['screen']['width'];
if($width*.75<700) {
	$width = 700;
} else {
	$width*=0.75;
}

$my_head = $mheadings[$section];
$result_head = $mheadings[$r_section];
if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if(isset($my_head[$table_fld.'repkpi'])) {
	unset($my_head[$table_fld.'repkpi']);
}
if($section=="KPI" || $section == "TOP") {
	$obj_tt = $object['tt'];
	$obj_ct = $object['ct'];
}
//arrPrint($my_head);
//arrPrint($object);
//arrPrint($extras);
//if($_SESSION['cc']=="helpdsk") { echo "."; }
?>
<h2>Details</h2>
<table width=<?php echo $width; ?>>
	<tr>
		<th width=25% class=left>Reference</th>
		<td><?php echo (isset($id_label)?$id_label:"").$obj_id; ?></td>
	</tr>
<?php
foreach($my_head as $fld => $h) {
	$v = (!isset($object[$fld]) || ($h['h_type']=="LIST" && strlen($object[$fld])==0)) ? $unspecified : $object[$fld];
	echo chr(10)."<tr>";
		echo "<th class=left>".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;</th>";
	switch($h['h_type']) {
/*		case "CAP":
			echo "<td>".$fld;
			echo "</td>";
			break;*/
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			$values = $extras[$fld];
			$val = array();
			foreach($values as $v) {
				$val[] = ((strlen($v['code'])>0 && strtolower($v['value'])!="all") ? ASSIST_HELPER::decode($v['code']).": " : "").decode($v['value']);
			}
			echo "<td>";
				echo implode("<br />",$val);
			echo "</td>";
			break;
		case "DATE":
			echo "<td>".($v>0 ? date("d M Y",$v) : "")."</td>";
			break;
		case "TEXTLIST":
			echo "<td>";//.$v;
				$a = array();
				if(strlen($v)>0) {
					$a = explode(";",$v);
					if(in_array("0",$a)) {
						$a = array("0");
					}
				} else {
					$a[] = "0";
				}
				//arrPrint($a);
				$v = array();
				foreach($a as $k){
					//echo "<P>".$k." ::> ".$fld.(isset($extras[$fld][$k]))."</p>";
					if(strlen($k)>0) {
						if($k=="0" || !isset($extras[$fld][$k])) {
							$v[] = $unspecified;
							break;
						} else {
							$v[] = $extras[$fld][$k]['value'];
						}
					}
				}
				echo count($v)>0 ? implode("; ",$v) : $unspecified;
			echo "</td>";
			break;
		default:
			switch($fld) {
				case "kpi_annual":
				case "kpi_revised":
				case "top_annual":
				case "top_revised":
					echo "<td>".KPIresultDisplay($v,$obj_tt)."</td>";
					break;
				default:
					echo "<td>".$v."</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
?>
</table>
<h2>Results</h2>
<?php //echo "ABC"; arrPrint($lists);
switch($section) {
	case "KPI":
	case "TOP":
		if($section=="TOP") {
			$mytime = $timeObj->getTopTime();
		} else {
			$mytime = $timeObj->getAllTime();
		}
		$kr = $myObj->getUpdateFormDetails($obj_id); //arrPrint($kr);
//if($_SESSION['cc']=="helpdsk") {
//arrPrint($kr);
//}

		$drawObj->drawKPIResults($myObj,$result_head,$mytime,$kr,false,$obj_id);

		if($section=="TOP") {
			echo "<h2>Forecast Annual Target</h2>
					<table id=tblfore>";
				foreach($lists['years'] as $i => $l) {
					if($i>1) {
						echo "<tr>
								<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
								<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['tf_value']) ? $forecast[$i]['tf_value'] : 0),$obj_tt)."&nbsp;</td>
							</tr>";
					}
				}
			echo "</table>";
		}
		break;
	case "CAP":
		$drawObj->drawCapitalResults($myObj,$mheadings,$timeObj->getAllTime(),$myObj->getResults($obj_id));


		echo "<h2>Forecast Annual Budget</h2>
				<table id=tblfore>
					<tr>
						<td></td>
						<th>CRR</th>
						<th>Other</th>
					</tr>";
			foreach($lists['years'] as $i => $l) {
					echo "<tr>
							<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['cf_crr']) ? $forecast[$i]['cf_crr'] : 0),1)."&nbsp;</td>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['cf_other']) ? $forecast[$i]['cf_other'] : 0),1)."&nbsp;</td>
						</tr>";
			}
		echo "</table>";
		break;
}



displayGoBack("","");
?>