<?php 
$page_title = "Module Setup";
$log_section = "DEF";
include("inc/header.php"); 

/* Get defaults from $dbref_setup_defaults table */
$sql = "SELECT * FROM ".$dbref."_setup_defaults WHERE active = true ORDER BY sort";
$defaults_questions = array();
$defaults_questions = mysql_fetch_alls($sql,'code');
foreach($defaults_questions as $d => $def) {
	$os = $def['options'];
	$os = explode("_",$os);
	$options = array();
	foreach($os as $o => $opt) {
		$x = explode("|",$opt);
		$options[$x[0]] = $x[1];
		//echo "<option ".($setup_defaults[$def['code']]==$o[0] ? " selected ":"")."value=".$o[0].">".decode($o[1])."</option>";
	}
	$defaults_questions[$d]['options'] = $options;
}

/* Save */
$var = $_REQUEST;
if(isset($var['act']) && $var['act']=="SAVE") {
	$changes = array();
	foreach($defaults_questions as $def) {
		if(isset($var[$def['code']])) {
			if($var[$def['code']]!=$def['value']) { $changes[$def['code']] = $var[$def['code']]; }
		}
	}
	if(isset($changes) && count($changes)>0) {
		foreach($changes as $code => $value) {
			$sql = "UPDATE ".$dbref."_setup_defaults SET value = '".code($value)."' WHERE code = '$code'";
			$mar = db_update($sql);
			if($mar>0) {
				$log_q = stripos($defaults_questions[$code]['descrip'],"<br")!==false ? substr($defaults_questions[$code]['descrip'],0,stripos($defaults_questions[$code]['descrip'],"<br")) : $defaults_questions[$code]['descrip'];
				
				$v_log = array(
					'section'=>$log_section,
					'ref'=>"$code",
					'field'=>"value",
					'text'=>"Updated <i>".code($log_q)."</i> to <i>".code($defaults_questions[$code]['options'][$value])."</i> from <i>".code($defaults_questions[$code]['options'][$setup_defaults[$code]])."</i>.",
					'old'=>$setup_defaults[$code],
					'new'=>code($value),
					'YN'=>"Y"
				);
				logChanges('SETUP',0,$v_log,code($sql));
				$setup_defaults[$code] = code($value);
			}
		}
		$result = array("ok","Changes saved.");
		$setup_defaults = getModuleDefaults();
	} else {
		$result = array("error","No changes found to save.");
	}
}


displayResult($result);
/* Display questions */


?>
<form name=save method=post action=<?php echo $self; ?>>
<input type=hidden name=act value=SAVE />
<table width=700>
<?php 
$script = "";
foreach($defaults_questions as $d => $def) {

if($def['id']!=3) {

	$script.=chr(10)."$(\"#".$def['id']."\").click(function() { document.location.href = '$self?act=SAVE&".$def['code']."='+document.getElementById('".$def['code']."').value; });";
?>
	<tr>
		<th><?php echo $def['id']; ?></th>
		<td><span class=float>&nbsp;&nbsp;
<?php		echo "<select name=".$def['code']." id=".$def['code'].">";
	$options = $def['options'];
	//$options = explode("_",$options);
	foreach($options as $o=>$opt) {
		//$o = explode("|",$opt);
		echo "<option ".($setup_defaults[$def['code']]==$o ? " selected ":"")."value=".$o.">".decode($opt)."</option>";
	}
?>
			</select>
		<input type=button value=Save id=<?php echo $def['id']; ?> /></span><?php  if($def['code']=="POE") { echo "<span style='float: left;'>"; $me->displayIcon("info"); echo "</span>&nbsp;"; }  echo $def['descrip']; ?></td>
	</tr>
<?php 
} //not question 3
} //foreach defaults_questions ?>
<tr>
	<th>&nbsp;</th>
	<td colspan=3><input type=submit value="Save All" class=isubmit /></td>
</tr>
</table>
</form>
<div style="width: 700px;">
<?php $me->displayResult(array("info","If attachment of POE is allowed, please provide guidance to the user by completing the 'Glossary' section for the POE field in Setup > Headings > Departmental SDBIP / Top Layer SDBIP.  This guidance will be available to the user when they complete the Update form.")); ?>
</div>
	<script>
		<?php echo $script; ?>
	</script>
<?php 
goBack("setup.php","Back to Setup"); 
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>
</body>
</html>