<?php


/*****************************
JAVA BASED AM CHARTS 
*****************************/

//BAR GRAPH
function drawBarGraph($report_settings,$page_layout,$graph_title,$kpi_separate,$result_settings,$count,$chartData,$graph_stackType = "REG",$valueAxis_maximum = 0,$sub_chartData = array(),$my_lists = array(),$sub_ids = array(),$sub_count = array(),$display_grid=0) 
{

	$echo = "<script type=\"text/javascript\">
        var chart;
		var legend;
        var chartData = [".$chartData."];";
	if(count($sub_chartData) > 0) {
		$echo.= "var sub_chartData = new Array();";
		foreach($sub_chartData as $sc => $scd) {
				$echo.= chr(10)."sub_chartData[".$sc."] = [".implode(",",$scd)."];";
		} 
	}
        $echo.= "
		$(document).ready(function() {
            dir_chart = new AmCharts.AmPieChart();
			dir_chart.color = \"#000000\";
            dir_chart.dataProvider = chartData;
            dir_chart.titleField = \"value\";
			dir_chart.valueField = \"obj\";
			dir_chart.colorField = \"color\";
			dir_chart.labelText = \"[[percents]]%\";
			dir_chart.labelRadius = 20;
			dir_chart.angle = 40;
			dir_chart.depth3D = 15;
			dir_chart.hideLabelsPercent = 0;
			dir_chart.hoverAlpha = 0.5;
			dir_chart.startDuration = 0;
			dir_chart.radius = 150;
			dir_chart.marginBottom = 10;
			dir_chart.write(\"dir\");";

	if(count($sub_chartData) > 0) {

		$echo.="
			var valAxis = new AmCharts.ValueAxis();
			valAxis.stackType = \"".($graph_stackType=="REG" ? "regular" : "100%")."\";
			valAxis.gridAlpha = ".($display_grid==1 ? "0.2" : "0").";
			valAxis.axisAlpha = 0;
			valAxis.maximum = ".$valueAxis_maximum.";
			valAxis.color = \"#".($display_grid==1 ? "000000" : "ffffff")."\";";

		foreach($sub_chartData as $sc => $scd) {
			$echo.= "    
				sub_chart_".$sc." = new AmCharts.AmSerialChart();
				sub_chart_".$sc.".color = \"#ffffff\";
				sub_chart_".$sc.".dataProvider = sub_chartData[".$sc."];
				sub_chart_".$sc.".categoryField = \"text\";
				sub_chart_".$sc.".marginLeft = 47;
				sub_chart_".$sc.".marginTop = 10;
				sub_chart_".$sc.".marginBottom = 50;
				sub_chart_".$sc.".fontSize = 10;
				sub_chart_".$sc.".angle = 45;
				sub_chart_".$sc.".depth3D = 15;
				sub_chart_".$sc.".plotAreaBorderAlpha = ".($display_grid==1 ? "0.3" : "0").";
				sub_chart_".$sc.".rotate = false;

				sub_chart_".$sc.".addValueAxis(valAxis);

				sub_chart_".$sc.".categoryAxis.gridAlpha = ".($display_grid==1 ? "0.2" : "0").";
				sub_chart_".$sc.".categoryAxis.axisAlpha = 0;
				sub_chart_".$sc.".categoryAxis.color = \"#000000\";
				sub_chart_".$sc.".categoryAxis.fontSize = 7;
				sub_chart_".$sc.".categoryAxis.gridPosition = \"start\";
				";
			foreach($result_settings as $ky => $k) { 
				$echo.= "
				var graph_".$sc."_".$k['id']." = new AmCharts.AmGraph();
				graph_".$sc."_".$k['id'].".title = \"".$k['value']."\";
				graph_".$sc."_".$k['id'].".labelText=\"".($graph_stackType=="REG" ? "[[value]]" : "[[percents]]%")."\";
				graph_".$sc."_".$k['id'].".valueField = \"R".$k['id']."\";
				graph_".$sc."_".$k['id'].".type = \"column\";
				graph_".$sc."_".$k['id'].".lineAlpha = 0;
				graph_".$sc."_".$k['id'].".fillAlphas = 1;
				graph_".$sc."_".$k['id'].".lineColor = \"".$k['color']."\";
				graph_".$sc."_".$k['id'].".balloonText = \"[[value]] kpis\";
				sub_chart_".$sc.".addGraph(graph_".$sc."_".$k['id'].");";
			}
			$echo.= "sub_chart_".$sc.".write(\"sub_".$sc."\");";
		}	//end foreach sub chart 
	} //end if group 
			
	$echo.= "		});
			</script>
			
	<div align=center>
		<table class=noborder>
		<!-- MAIN GRAPH -->
			<tr class=no-highlight>
				<td colspan=2 class=\"center noborder\"><span style=\"font-weight: bold;font-size:10pt;\">".$graph_title['main']."</span></td>
			</tr><tr class=no-highlight>
				<td width=50% class=\"right noborder\">
					<div id=\"dir\" style=\"width:460px; height:280px; background-color:#ffffff;float:right\"></div>
				</td>
				<td width=50% class=\"middle noborder\"><table>
					<tbody>";

				if($kpi_separate['NM']=="EXCLUDE") { unset($result_settings[0]); }
				if($kpi_separate['MET']=="COMBINE") { unset($result_settings[4]); unset($result_settings[5]); }
				foreach($result_settings as $ky => $k) {
					$echo.= "
						<tr>
							<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
							".$k['value']."</td>
							<td class=center style=\"font-weight: bold;\">".(isset($count[$k['text']]) ? "<a href=# onclick=\"goToReport($ky,'ALL',0,'".base64_encode(serialize($_REQUEST))."')\">".$count[$k['text']]." (".number_format(($count[$k['text']]/array_sum($count)*100),2)."%)" : "-")."</a></td>
						</tr>";
				}
				$echo.= "
				</tbody>
				<tfoot>
					<tr>
						<td class=right rowspan=2 style=\"background-color: #dddddd;\"><b>Total:</b></td>
						<td class=\"center\" style=\"background-color: #555555; color: #ffffff; font-weight: bold;\">".array_sum($count)." (100%)</td>
					</tr>
				</tfoot>
				</table></td>
			</tr>";
	if(count($sub_chartData)>0) {
		$echo.="
		<!-- SUB GRAPHS -->
			<tr class=no-highlight ".($page_layout=="LAND" ? "style=\"page-break-before: always;\"" : "").">
				<td class=\"center noborder\" colspan=2><span style=\"font-weight: bold;font-size: 10pt\">".$graph_title['sub']."</span>
			</tr>";
		foreach($sub_chartData as $sc => $scd) { 
				$graph_width = count($scd)*90 + 50; 
				$list_ids = $sub_ids[$sc];
		$echo.="
			<tr class=no-highlight>
				<td colspan=2 class=\"center noborder\">
					<div id=\"sub_".$sc."\" style=\"width:".$graph_width."px; height:270px; background-color:#ffffff; margin: 0 auto;\"></div>
				</td>
			</tr><tr class=no-highlight>
				<td colspan=2 class=noborder>

				
				
					<div align=center><table>
							<thead>
								<tr class=no-highlight>
									<td style=\"border-top-color: #ffffff; border-left-color: #ffffff; border-right-color: #ffffff;\"></td>";
									foreach($my_lists as $s_id => $s) {
										if(in_array($s_id, $list_ids)) {
											$echo.= "<th width=100 style=\"font-size: 7pt;\">".$s['value']."</th>";
										}
									}
		$echo.="
								</tr>
							</thead>
							<tbody>";
								foreach($result_settings as $ky => $k) {
									$echo.= "
										<tr>
											<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
											".$k['value']."</td>";
									foreach($my_lists as $s_id => $s) {
										if(in_array($s_id, $list_ids)) { 
											$echo.= "
											<td class=center>".(isset($sub_count[$s_id][$k['text']]) ? "<a href=# onclick=\"goToReport($ky,'".$report_settings['groupby']."',".$s_id.",'".base64_encode(serialize($_REQUEST))."')\">".$sub_count[$s_id][$k['text']]." (".number_format(($sub_count[$s_id][$k['text']]/array_sum($sub_count[$s_id])*100),2)."%)" : "-")."</a></td>"; 
										}
									}
									echo "</tr>";
								}
		$echo.="
							</tbody>
							<tfoot>
								<tr>
									<td class=right rowspan=2 style=\"background-color: #dddddd;\"><b>Total:</b></td>";
									foreach($my_lists as $s_id => $s) {
										if(in_array($s_id, $list_ids)) { 
											$echo.= "
											<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? array_sum($sub_count[$s_id]) : "-")."
											 (".(isset($sub_count[$s_id]) ? number_format((array_sum($sub_count[$s_id])/array_sum($count)*100),2)."%" : "-").")</b></td>";
										}
									}
		$echo.="
								</tr>
							</tfoot>
					</table></div>
				
				
				
				</td>
			</tr>";
		}	//foreach sub_chartdata
	} //if subchartdata
	$echo.="
	</table></div>";

	echo $echo;
} //function drawBarGraph















function drawPieGraph($report_settings,$page_layout,$graph_title,$kpi_separate,$result_settings,$count,$chartData,$graph_stackType = "ABOVE",$sub_chartData = array(),$my_lists = array(),$sub_ids = array(),$sub_count = array())
{
$row_break = $page_layout=="LAND" ? 4 : 3;
$line_break = 2;
	switch($page_layout) {
		case "LAND":
			$tdl = 1;
			$cradius = 90;
			$cwidth = 10;
			break;
		case "PORT":
		default:
			$tdl = 3;
			$cradius = 100;
			$cwidth = 30;
			break;
	}
	$cwidth+= $cradius * 2;
	$cheight = $cwidth;

	$echo = "<script type=\"text/javascript\">
        var chart;
		var legend;
        var chartData = [".$chartData."];";
	if(count($sub_chartData) > 0) {
		//$echo.= "var sub_chartData = new Array();";
		foreach($sub_chartData as $sc => $scd) {
				$echo.= chr(10)."var sub_chartData_".$sc." = [".$scd."];";
		} 
	}
        $echo.= "
		$(document).ready(function() {
            main_chart = new AmCharts.AmPieChart();
			main_chart.color = \"#ffffff\";
            main_chart.dataProvider = chartData;
            main_chart.titleField = \"value\";
			main_chart.valueField = \"obj\";
			main_chart.colorField = \"color\";
			main_chart.labelText = \"[[percents]]%\";
			main_chart.labelRadius = -25;
			main_chart.angle = 10;
			main_chart.depth3D = 10;
			main_chart.hideLabelsPercent = 0;
			main_chart.hoverAlpha = 0.5;
			main_chart.startDuration = 0;
			main_chart.radius = ".$cradius.";
			main_chart.marginBottom = 30;
			main_chart.write(\"main\");";

	if(count($sub_chartData) > 0) {
		foreach($sub_chartData as $sc => $scd) {
			$echo.= "
				sub_chart_".$sc." = new AmCharts.AmPieChart();
				sub_chart_".$sc.".color = \"#ffffff\";
				sub_chart_".$sc.".dataProvider = sub_chartData_".$sc.";
				sub_chart_".$sc.".titleField = \"value\";
				sub_chart_".$sc.".valueField = \"obj\";
				sub_chart_".$sc.".colorField = \"color\";
				sub_chart_".$sc.".labelText = \"[[percents]]%\";
				sub_chart_".$sc.".labelRadius = -25;
				sub_chart_".$sc.".angle = 10;
				sub_chart_".$sc.".depth3D = 10;
				sub_chart_".$sc.".hideLabelsPercent = 0;
				sub_chart_".$sc.".hoverAlpha = 0.5;
				sub_chart_".$sc.".startDuration = 0;
				sub_chart_".$sc.".radius = ".$cradius.";
				sub_chart_".$sc.".marginBottom = 30;
				sub_chart_".$sc.".write(\"sub_".$sc."\");";
		}
	}
	$echo.= "		});
	function goReport(result,fld,id) {
		var url = '".$report_settings['path']."?page_id=generate&act=GENERATE&src=GRAPH&r_from=".$report_settings['r_from']."&r_to=".$report_settings['r_to']."&groupby=".$report_settings['groupby']."&filter[result]='+result;
		if(fld!=\"ALL\" && !isNaN(parseInt(id))) {
			url = url + '&filter[' + fld + '][]=' + id;
		}
		//alert(url);
		document.location.href = url;
	}
			</script>
			
	<div align=center>
		<table class=noborder>
		<!-- MAIN GRAPH -->
			<tr class=no-highlight><td class=\"center noborder\"><table class=noborder ".($graph_stackType=="ABOVE" ? "width=100%" : "").">
			<tr class=no-highlight>
				<td class=\"center noborder\" colspan=".($graph_stackType=="ABOVE" ? "2" : "1")."><span style=\"font-weight: bold;font-size:10pt;\">".$graph_title['main']."</span></td>
			</tr><tr class=no-highlight>
				<td ".($graph_stackType=="ABOVE" ? "width=50% class=\"right " : "class=\"center ")." noborder\">
					
					<div id=\"main\" style=\"width:".$cwidth."px; height:".$cheight."px; background-color:#ffffff;".($graph_stackType=="ABOVE" ? "float:right" : "margin: 0 auto;")."\"></div>
				".($graph_stackType=="ABOVE" ? "</td><td width=50% class=\"middle noborder\"><table>" : "<table style=\"margin: 0 auto;\">")."
				
					<tbody>";

				if($kpi_separate['NM']=="EXCLUDE") { unset($result_settings[0]); }
				if($kpi_separate['MET']=="COMBINE") { unset($result_settings[4]); unset($result_settings[5]); }
				foreach($result_settings as $ky => $k) {
					$echo.= "
						<tr>
							<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
							".$k['value']."</td>
							<td class=center style=\"font-weight: bold;\">".(isset($count[$k['text']]) ? "<a href=# onclick=\"goToReport($ky,'ALL',0,'".base64_encode(serialize($_REQUEST))."')\">".$count[$k['text']]." (".number_format(($count[$k['text']]/array_sum($count)*100),2)."%)" : "-")."</a></td>
						</tr>";
				}
				$echo.= "
				</tbody>
				<!-- <tfoot>
					<tr>
						<td class=right rowspan=2 style=\"background-color: #dddddd;\"><b>Total:</b></td>
						<td class=\"center\" style=\"background-color: #555555; color: #ffffff; font-weight: bold;\">".array_sum($count)." (100%)</td>
					</tr>
				</tfoot> -->
				</table></td></tr></table></td>";
		if($graph_stackType=="ABOVE") {
			$echo.="</tr>";
			$lines = 1;
			$rows = 0;
		} else {
			$lines = 0;
			$rows = 1;
		}
	if(count($sub_chartData)>0) {
		$echo.="
		<!-- SUB GRAPHS -->
			".($graph_stackType=="ABOVE" ? "<tr class=no-highlight >
				<td class=\"center noborder\"><span style=\"font-weight: bold;font-size: 10pt\">".$graph_title['sub']."</span>
			</tr><tr class=no-highlight>" : "")."<td class=\"center noborder\"><table class=noborder width=100%><tr class=no-highlight>";
		foreach($sub_chartData as $sc => $scd) { $s_id = $sc;
			$rows++; 
			if($rows>$row_break) { 
				$lines++; 
				if($lines>=$line_break) { $lb = "page-break-before: always;"; $lines = 0; } else { $lb = ""; }
				$echo.="</tr></table></td></tr><tr class=no-highlight><td class=\"center noborder\" ".($graph_stackType!="ABOVE" ? "colspan=2" : "")."><table style=\"".$lb."\" class=noborder width=100%><tr class=no-highlight>"; 
				$rows = 1; 
			}
			$echo.= "
				<td class=\"center noborder\">
					".$sub_ids[$sc]."
					<div id=\"sub_".$sc."\" style=\"width:".$cwidth."px; height:".$cheight."px; background-color:#ffffff;margin: 0 auto;\"></div>
					<table style=\"margin: 0 auto;\">
					<tbody>";

				foreach($result_settings as $ky => $k) {
					$echo.= "
						<tr>
							<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
							".$k['value']."</td>
							<td class=center style=\"font-weight: bold;\">".(isset($sub_count[$s_id][$k['text']]) ? "<a href=# onclick=\"goToReport($ky,'".$report_settings['groupby']."',".$s_id.",'".base64_encode(serialize($_REQUEST))."')\">".$sub_count[$s_id][$k['text']]." (".number_format(($sub_count[$s_id][$k['text']]/array_sum($sub_count[$s_id])*100),2)."%)" : "-")."</a></td>
						</tr>";
				}
				$echo.= "
				</tbody>
				<!-- <tfoot>
					<tr>
						<td class=right rowspan=2 style=\"background-color: #dddddd;\"><b>Total:</b></td>
						<td class=\"center\" style=\"background-color: #555555; color: #ffffff; font-weight: bold;\">".array_sum($sub_count[$s_id])." (100%)</td>
					</tr>
				</tfoot> -->
				</table>
				</td>
			";
		}
		$echo.="</tr></table></td></tr></table>";
	} //if subchartdata
	$echo.="
	</table></div>";

	echo $echo;
}


























/*****************************
FLASH BASED AMCHARTS
*****************************/


function drawFlashMainPie($title,$values,$type="single",$g = "main") {
global $result_settings;
	if($g=="main") {
		require_once "../library/amcharts2/php/AmPieChart.php"; 
		AmChart::$swfObjectPath = "swfobject.js";
		AmChart::$libraryPath = "/library/amcharts2/ampie/";
		AmChart::$jsPath = "/library/amcharts2/php/AmCharts.js";
		AmChart::$jQueryPath = "jquery.js";
		AmChart::$loadJQuery = false;
	}
	$chart = new AmPieChart("pie_".$g);
	foreach($result_settings as $r) {
		$chart->addSlice($r['text'],$r['value'],(isset($values[$r['text']]) ? $values[$r['text']] : 0),array("color" => $r['color']));
	}

	if($type=="multi") {
		$chart->setConfigAll(array(
			"width" => 200,
			"height" =>200,
			"font" => "Verdana",
			"text_size" => 10,
			"decimals_separator" => ".",
			"pie.y" => "50%",
			"pie.x"=>"50%",
			"pie.radius" => 95,
			"pie.height" => 10,
			"pie.angle" => 5,
			"pie.inner_radius" => 0,
			"animation.pull_out_on_click" => false,
			"background.border_alpha" => 0,
			"data_labels.radius" => "-25%",
			"data_labels.text_color" => "0xFFFFFF",
			"data_labels.show" => "<![CDATA[{percents}%]]>",
			/*"export_as_image.file" => '/library/amcharts2/ampie/export.php',
			"export_as_image.color" => '0xCC0001',
			"export_as_image.alpha" => '50',*/
			"legend.enabled" => "false",
			"legend.max_columns" => 1,
			"legend.text_size" => 9,
			"legend.spacing" => 3,
			"legend.x" => "35%",
			"balloon.enabled" => "true",
			"balloon.alpha" => 80,
			"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125; (&#123;percents&#125;%)&#93;&#93;>"
		));
	} else {
		$chart->setConfigAll(array(
			"width" => 540,
			"height" =>290,
			"font" => "Verdana",
			"text_size" => 10,
			"decimals_separator" => ".",
			"pie.y" => "50%",
			"pie.x"=>"50%",
			"pie.radius" => 150,
			"pie.height" => 15,
			"pie.angle" => 25,
			"pie.inner_radius" => 0,
			"animation.pull_out_on_click" => false,
			"background.border_alpha" => 0,
			"data_labels.radius" => "20%",
			"data_labels.text_color" => "0x000000",
			"data_labels.show" => "<![CDATA[{percents}%]]>",
			/*"export_as_image.file" => '/library/amcharts2/ampie/export.php',
			"export_as_image.color" => '0xCC0001',
			"export_as_image.alpha" => '50',*/
			"legend.enabled" => "false",
			"legend.max_columns" => 1,
			"legend.text_size" => 9,
			"legend.spacing" => 3,
			"legend.x" => "35%",
			"balloon.enabled" => "true",
			"balloon.alpha" => 80,
			"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125; (&#123;percents&#125;%)&#93;&#93;>"
		));
	}

	$echo = "<span class=b ".($g=="main" ? "style=\"font-size: 9pt\"" : "").">".$title."</span><br />";
	$echo.= html_entity_decode($chart->getCode());
	echo $echo;
}


function drawFlashBar($title,$group,$values,$graph_ref = 0,$graph_type = "PERC",$display_grid=0) {
	global $result_settings;
//arrPrint($values);
//arrPrint($result_settings);
if($graph_ref==0) {
		require("../library/amcharts2/php/AmBarChart.php");
		AmChart::$swfObjectPath = "swfobject.js";
		AmChart::$libraryPath = "../library/amcharts2/amcolumn/";
		AmChart::$jsPath = "../library/amcharts2/php/AmCharts.js";
		AmChart::$jQueryPath = "jquery.js";
		AmChart::$loadJQuery = false;
}
	$chart = new AmBarChart("bar_".$graph_ref);

	$gcount = 0;
	foreach($group as $g) {
		if(isset($values[$g['id']])) {
			$chart->addSerie($g['id'], decode($g['value']));
			$gcount++;
		}
	}

	foreach($result_settings as $r) {
		$kpires = array();
		foreach($group as $g) { 
			if(isset($values[$g['id']][$r['text']]) && $values[$g['id']][$r['text']]>0) { 
				$kpires[$g['id']] = $values[$g['id']][$r['text']]; 
			} 
		}//arrPrint($kpires);
		$chart->addGraph($r['text'], $r['value'], $kpires, array("color" => $r['color']));
	}

	$chart->setConfigAll(array(
		"width" => ($gcount*80)+10,
		"height" => 290,
		"depth" => 10,
		"angle" => 30,
		"font" => "Verdana",
		"text_size" => 9,
		"column.type" => ($graph_type=="REG" ? "stacked" : "100% stacked"),
		"column.width" => 85,
		"column.data_labels" => "<!".chartEncode("[CDATA[{".($graph_type=="REG" ? "value}" : "percents}%")."]]").">",
		"column.data_labels_text_color" => "#ffffff",
		"column.data_labels_text_size" => 10,
		"column.spacing"=>5,
		"decimals_separator" => ".",
		"plot_area.margins.top" => 10,
		"plot_area.margins.right" => 20,
		"plot_area.margins.left" => $display_grid==1 ? 45 : 20,
		"plot_area.margins.bottom" => 70,
		"background.border_alpha" => 0,
		"plot_area.border_alpha" => $display_grid==1 ? 20 : 0,
		"grid.category.alpha" => $display_grid==1 ? 20 : 0,
		"grid.value.alpha" => $display_grid==1 ? 20 : 0,
		"axes.category.alpha" => 0,
		"axes.value.alpha" => 0,
		"values.category.text_size" => 9,
		"values.value.enabled" => $display_grid==1 ? "true" : "false",
		"values.value.min" => 0,
		//"values.value.max" => 250,
//		"values.category.rotate" => 90,
		"legend.enabled" => "false",
		/*"legend.y" => 70,
		"legend.x" => $legend_x,
		"legend.width" => 120,
		"legend.border_alpha" => 0,
		"legend.text_size" => 11,*/
//		"legend.text_size" => 8,
//		"legend.spacing" => 1,
//		"legend.margins" => 3,
//		"legend.align" => "center",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!".chartEncode("[CDATA[{title}: {value} of {total} ]]").">"
/*		"labels.label.x" => $label_x,
		"labels.label.y" => 15,
		"labels.label.text_size" => 14,
		"labels.label.text" => "<!".chartEncode("[CDATA[ <b>".$chart_title."</b> ]]").">",*/
		//"export_as_image.file" => "lib/amcharts2/amcolumn/export.php",
		//"export_as_image.target" => "_blank",
		//"export_as_image.color" => "0x232323",
		//"export_as_image.alpha" => 40
		//"column.spacing" => 20,
		//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"
	));

	$echo = "<span class=b>".$title."</span>";
	$echo.= html_entity_decode($chart->getCode());

	
	//$echo.= "<div style=\"border: 2px solid #cdcdcd; width: 650px; height: 390px; background-color: #ccccff;\">BAR GRAPH HERE</div>";
	echo $echo;
}




	function createFlashBarGraph($group,$g,$graph_title,$sub_count,$graph_type="PERC",$display_grid=0) {
			echo "		</td>
					</tr>
					<tr style=\"page-break-before: always;\">
						<td class=\"center noborder\">";
					drawFlashBar($graph_title['sub'],$group,$sub_count,$g,$graph_type,$display_grid);
			echo "		</td>
					</tr>
					<tr>
						<td class=noborder>";
					drawFlashLegend("SUB",$graph_title,$sub_count,$group);

	}



	function createComputerBarGraph($group,$g,$graph_title,$values,$graph_type="PERC",$display_grid=0) {
		global $report_settings;
		echo "</td>";
		if($g>0) {
			$colspan = 2;
			$show_main = false;
			echo "
					</tr>
					<tr>";
		} else {
			$colspan=1;
			$show_main = true;
		}
		echo "				<td class=\"center noborder\" colspan=".$colspan.">";
					drawFlashBar($graph_title['sub'],$group,$values['sub'],$g,$graph_type,$display_grid);
			echo "		</td>
					</tr>
					<tr>
						<td class=noborder colspan=2>";
					drawOnscreenLegend($graph_title,$values,$group,$show_main);

	}
	

function drawFlashLegend($type,$graph_title,$values,$group) {
	global $result_settings;
	$echo = "";

	//$width=100/(count($values['sub'])+2);
	$width = 100;
	
	$rowspan = ($type=="MAIN") ? 1 : 2;
	
	$echo.="
	<div align=center><table style=\"margin-top: 10px\" class=legend>
		<tr>
			<td class=head1 width=".($width+50)." rowspan=".$rowspan."></td>";
		if($type=="MAIN") {
			$echo.= "<td class=head1 width=".$width." rowspan=".$rowspan.">".$graph_title['main']."</td>";
		} else {
			$echo.= "<td class=head1 colspan=".count($group).">".$graph_title['sub']."</td>";
		}
	$echo.= "
		</tr>";
	if($type!="MAIN") {
		$echo.= "
		<tr>";
		foreach($group as $g) {
			if(isset($values[$g['id']])) {
				$echo.= "<td width=".$width." class=head2>".$g['value']."</td>";
			}
		}
		$echo.= "</tr>"; //arrPrint($result_settings);
	}
	foreach($result_settings as $r) {	
		$echo.= "<tr>
					<td class=res><img src='/library/amcharts2/".strtoupper(substr($r['color'],1,6)).".gif' width=10 height=10>&nbsp;".$r['value']."</td>";
		if($type=="MAIN") {
			$echo.="
				<td class=center>".(isset($values[$r['text']]) ? $values[$r['text']]." (".round(($values[$r['text']]/array_sum($values)*100),1)."%)" : "-")."</td>";
		} else {
			foreach($group as $g) {
				if(isset($values[$g['id']])) {
					$echo.= "<td class=center>".(isset($values[$g['id']][$r['text']]) ? $values[$g['id']][$r['text']]." (".round($values[$g['id']][$r['text']]/array_sum($values[$g['id']])*100,1)."%)" : "-")."</td>";
				}
			}
		}
		$echo.= "</tr>";
	}
	$echo.= "<tr>
				<td class=\"res right\">Total:</td>";
	if($type=="MAIN") {
		$echo.="		<td class=\"b center\">".array_sum($values)."</td>";
	} else {
		foreach($group as $g) {
			if(isset($values[$g['id']])) {
				$echo.= "<td class=\"b center\">".array_sum($values[$g['id']])."</td>";
			}
		}
	}
	$echo.= "</tr>";
	$echo.= "</table></div>"; 
	echo $echo;
}







function drawOnscreenLegend($graph_title,$values,$group,$show_main=true) {
	global $result_settings;
	global $report_settings;
	$echo = "";

	//$width=100/(count($values['sub'])+2);
	$width = 100;
	//if($show_main) { } else { $row}
	$echo.="
	<div align=center><table style=\"margin-top: 10px\" class=legend>
		<tr>
			<th width=".($width+50)." rowspan=2></th>";
	if($show_main) {
		$echo.= "	<th width=".$width." rowspan=2>".$graph_title['main']."</th>";
	}
	$echo.= "	<th colspan=".count($group).">".$graph_title['sub']."</th>
		</tr>
		<tr>";
		foreach($group as $g) {
			if(isset($values['sub'][$g['id']])) {
				$echo.= "<th class=th2 width=".$width." >".$g['value']."</td>";
			}
		}
	$echo.= "</tr>"; //arrPrint($result_settings);
	foreach($result_settings as $r) {	
		$echo.= "<tr>
					<td class=res><img src='/library/amcharts2/".strtoupper(substr($r['color'],1,6)).".gif' width=10 height=10>&nbsp;".$r['value']."</td>";
		if($show_main) { $echo.= chr(10)."	<td class=center>".(isset($values['main'][$r['text']]) ? "<a href=# onclick=\"goToReport(".$r['id'].",'".$report_settings['groupby']."','ALL','".base64_encode(serialize($_REQUEST))."')\">".$values['main'][$r['text']]." (".round(($values['main'][$r['text']]/array_sum($values['main'])*100),1)."%)</a>" : "-")."</td>"; }
		foreach($group as $g) {
			if(isset($values['sub'][$g['id']])) {
				$echo.= "<td class=center>".(isset($values['sub'][$g['id']][$r['text']]) ? "<a href=# onclick=\"goToReport(".$r['id'].",'".$report_settings['groupby']."',".$g['id'].",'".base64_encode(serialize($_REQUEST))."')\">".$values['sub'][$g['id']][$r['text']]." (".round($values['sub'][$g['id']][$r['text']]/array_sum($values['sub'][$g['id']])*100,1)."%)</a>" : "-")."</td>";
			}
		}
		$echo.= "</tr>";
	}
	$echo.= "<tr>
				<td class=\"total right\">Total:</td>";
	if($show_main) { $echo.= "			<td class=\"total center\">".array_sum($values['main'])."</td>"; }
		foreach($group as $g) {
			if(isset($values['sub'][$g['id']])) {
				$echo.= "<td class=\"total center\">".array_sum($values['sub'][$g['id']])."</td>";
			}
		}
	$echo.= "</tr>";
	$echo.= "</table></div>";
	echo $echo;
}



function drawPieLegend($values,$show_link = false,$g=0) {
	global $result_settings;
	global $report_settings;
	$echo = "<table class=legend style=\"margin: 0 auto;margin-top: 10px\">";
	foreach($result_settings as $r) {
		if($show_link) { 
			$url = "<a href=# onclick=\"goToReport(".$r['id'].",'".$report_settings['groupby']."',".$g.",'".base64_encode(serialize($_REQUEST))."')\">";
		} else {
			$url = "";
		}
		$echo.= "<tr><td class=res><img src='/library/amcharts2/".strtoupper(substr($r['color'],1,6)).".gif' width=10 height=10>&nbsp;".$r['value']."</td>
		<td class=center>".(isset($values[$r['text']]) ? $url.$values[$r['text']]." (".round(($values[$r['text']]/array_sum($values))*100,1)."%)</a>" : "-")."</td></tr>";
	}	
		$echo.= "<tr><td class=\"total right\">Total:</td>
		<td class=\"total center\">".array_sum($values)."</td></tr>";
	$echo.= "</table>";
	echo $echo;
	
}




function chartEncode($str) {

	//$str = code($str);

	$str = str_replace("&","&#38;",$str);
	$str = str_replace("\"","&#34;",$str);
	
	$str = str_replace("[","&#91;",$str);
	$str = str_replace("]","&#93;",$str);

	$str = str_replace("{","&#123;",$str);
	$str = str_replace("}","&#125;",$str);

	return $str;
}


function drawLegendStyle() {
echo "
	<style type=text/css>
		/** STYLE FOR LEGEND **/
		table.legend td { font-size: 7pt; }
		table td .head1 {
			text-align: center;
			font-weight: bold;
			vertical-align: middle;
			font-size: 7pt;
			color: #000000;
		}
		table td .head2 {
			font-style: italic;
			text-align: center;
			font-weight: normal;
			vertical-align: bottom;
			font-size: 7pt;
			color: #000000;
		}
		table td .res {
			font-weight: bold;
			vertical-align: middle;
			font-size: 6.5pt;
			color: #000000;
		}
		table tbody th {
			font-size: 7.5pt;
			text-align: center;
		}
		table tbody th.th2 {
			font-size: 7.5pt;
			font-style: italic;
		}
		table td .total {
			background-color: #cdcdcd;
			font-weight: bold;
			vertical-align: middle;
			font-size: 7pt;
			color: #000000;
		}
		</style>";
}


?>