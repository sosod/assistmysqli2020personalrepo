<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unbilled Advocates - Please bill</b></h1>
<?php
//GET DATA
$uaid = $_POST['uaid'];
$claimtkid = $tkid;
$claimclient = $_POST['claimclient'];
$claimclientname = $_POST['claimclientname'];
$claimmatter = $_POST['claimmatter'];
$claimmattername = $_POST['claimmattername'];
$claimcomment = $_POST['claimcomment'];
$claimurgencyid = $_POST['claimurgencyid'];
//FORMAT VARIABLES
$day = date("d",$today);
$mon = date("n",$today);
$year = date("Y",$today);
$claimlistdate = mktime(0,0,0,$mon,$day,$year);
$claimclient = str_replace("'","&#39",$claimclient);
$claimmatter = str_replace("'","&#39",$claimmatter);
$claimclientname = str_replace("'","&#39",$claimclientname);
$claimmattername = str_replace("'","&#39",$claimmattername);
$claimcomment = str_replace("'","&#39",$claimcomment);

//ADD CLAIM RECORD
$sql = "INSERT INTO assist_".$cmpcode."_ua_claim SET ";
$sql .= "claimuaid = ".$uaid.", ";
$sql .= "claimtkid = '".$claimtkid."', ";
$sql .= "claimclient = '".$claimclient."', ";
$sql .= "claimclientname = '".$claimclientname."', ";
$sql .= "claimmatter = '".$claimmatter."', ";
$sql .= "claimmattername = '".$claimmattername."', ";
$sql .= "claimcomment = '".$claimcomment."', ";
$sql .= "claimurgencyid = ".$claimurgencyid.", ";
$sql .= "claimdate = '".$today."', ";
$sql .= "claimuserid = '".$tkid."', ";
$sql .= "claimlistdate = '".$claimlistdate."'";
include("inc_db_con.php");
    //Update transaction log
    $tsql = str_replace("'","|",$sql);
    $trans = "Unbilled Advocate ".$uaid." claimed: ".$sql2;
    $tref = "UA";
    include("inc_transaction_log.php");

//UPDATE DEPOSIT TABLE
$sql = "UPDATE assist_".$cmpcode."_ua_unbilled SET uaclaimyn = 'Y' WHERE uaid = ".$uaid;
include("inc_db_con.php");

//GET INFORMATION FOR EMAIL
    $sql = "SELECT * FROM assist_".$cmpcode."_ua_unbilled u, assist_".$cmpcode."_ua_list_advocates a, assist_".$cmpcode."_ua_list_directors d, assist_".$cmpcode."_timekeep t WHERE d.tkid = t.tkid AND u.uaclaimyn = 'N' AND u.uaadvocateid = a.id AND u.uadirectorid = d.id AND uaid = ".$uaid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $uainvdate = date("d F Y",$row['uddepdate']);
        $uaadvocate = $row['value'];
        $uafilenum = $row['uafilenum'];
        $uainvnum = $row['uainvnum'];
        $uadescrip = $row['uadescrip'];
        $uadirector = $row['tkname']." ".$row['tksurname'];
        $uaamount = $row['uaamount'];
    mysql_close();
/*    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$claimtkid."'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $claimtkname = $row['tkname']." ".$row['tksurname'];
    mysql_close();
*/
    $claimtkname = $tkname;
    $sql = "SELECT * FROM assist_".$cmpcode."_ua_list_urgency WHERE id = ".$claimurgencyid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $claimurgency = $row['value'];
    mysql_close();

//CHECK EMAIL SETTING
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'UA' AND refid = 1";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r == 0)
{
    $uaemail = "N";
}
else
{
    $row = mysql_fetch_array($rs);
    $uaemail = $row['value'];
}

if($uaemail != "N") //IF EMAIL SETTING IS NOT NO
{
    if($uaemail == "A") //IF EMAIL SETTING IS ADMIN GET EMAIL ADDRESSES FOR ALL ADMINS
    {
        $sql = "SELECT t.tkemail FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ua_list_users u WHERE t.tkid = u.tkid AND u.yn = 'Y'";
        include("inc_db_con.php");
        $r = mysql_num_rows($rs);
        if($r > 1)
        {
            $row = mysql_fetch_array($rs);
            $to = $row['tkemail'];
            while($row = mysql_fetch_array($rs))
            {
                $to .= ", ".$row['tkemail'];
            }
        }
        else
        {
            $row = mysql_fetch_array($rs);
            $to = $row['tkemail'];
        }
        mysql_close();
    }
    else    //ELSE EMAIL SETTING IS NOT NO OR ADMIN THEN IT IS A SPECIFIED EMAIL ADDRESS
    {
        $to = $uaemail;
    }
    //SET EMAIL VARIABLES
    $comment = str_replace(chr(10),"<br>",$claimcomment);
    $subject = "Unbilled Advocates - Please bill...";
    $message = "<font face=arial>";
    $message = $message."<p>A request has been made to bill an Unbilled Advocate amount.</p>";
    $message = $message."<p><b>The details are:</b><br><small>";
    $message = $message."<b>Invoice Date:</b> ".$uainvdate."<br>";
    $message = $message."<b>Advocate:</b> ".$uaadvocate."<br>";
    $message = $message."<b>File number:</b> ".$uafilenum."<br>";
    $message = $message."<b>Invoice number:</b> ".$uainvnum."<br>";
    $message = $message."<b>Fee-Earner:</b> ".$uadirector."<br>";
    $message = $message."<b>Description:</b> ".$uadescrip."<br>";
    $message = $message."<b>Amount:</b> R ".number_format($uaamount,2)."</p>";
    $message = $message."</small><p><b>The claim details are:</b><br><small>";
    $message = $message."<b>Claimed by:</b> ".$claimtkname."<br>";
    $message = $message."<b>Client number:</b> ".$claimclient."<br>";
    $message = $message."<b>Client name:</b> ".$claimclientname."<br>";
    $message = $message."<b>Matter number:</b> ".$claimmatter."<br>";
    $message = $message."<b>Matter name:</b> ".$claimmattername."<br>";
    $message = $message."<b>Urgency:</b> ".$claimurgency."<br>";
    $message = $message."<b>Comment:</b> ".$comment."</p>";
    $message = $message."<p>To view a report of all Unbilled Advocate amounts claimed today please log on to Ignite Assist at <b><a href=http://assist.ignite4u.co.za style='color: #cc0001'>http://assist.ignite4u.co.za</a></b>.</p>";
    $message = $message."</small></font>";
    include("../assist_from.php");
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    //SEND ADMIN NOTIFICATION EMAIL
    mail($to,$subject,$message,$header);
        //Add transaction log
        $trans = "Unbilled Advocate ".$uaid." claim email sent to: ".$to;
        $tref = "UA";
        $tsql = $message;
        include("inc_transaction_log.php");
}

echo("<p>Bill request processed successfully.</p><p>Thank you.</p>");
?>
<p>&nbsp;</p>
</body>

</html>
