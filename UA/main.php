<?php
include("inc_ignite.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function claimDep(id) {
    document.location.href = "main_claim.php?i="+id;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Unbilled Advocates</b></h1><p>&nbsp;</p>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ua_unbilled u, assist_".$cmpcode."_ua_list_advocates a, assist_".$cmpcode."_ua_list_directors d, assist_".$cmpcode."_timekeep t WHERE d.tkid = t.tkid AND u.uaclaimyn = 'N' AND u.uaadvocateid = a.id AND u.uadirectorid = d.id ORDER BY t.tkname, t.tksurname, u.uafilenum, u.uasort";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r == 0)
{
    echo("<p>There are no unbilled advocates.&nbsp;</p>");
}
else
{
    ?>
    <table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Date Invoiced</td>
        <td class=tdheader>Advocate</td>
        <td class=tdheader>File<br>Number</td>
        <td class=tdheader>Invoice<br>Number</td>
        <td class=tdheader>Fee-Earner</td>
        <td class=tdheader>Description</td>
        <td class=tdheader>Amount</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
        <?php
        $totamt = 0;
        while($row = mysql_fetch_array($rs))
        {
            $totamt = $totamt+$row['uaamount'];
            ?>
            <tr>
                <td class=tdgeneral><?php echo(date("d M Y",$row['uainvdate'])); ?>&nbsp;</td>
                <td class=tdgeneral><?php echo($row['value']); ?>&nbsp;&nbsp;</td>
                <td class=tdgeneral align=center><?php echo($row['uafilenum']); ?>&nbsp;</td>
                <td class=tdgeneral align=center><?php echo($row['uainvnum']); ?>&nbsp;</td>
                <td class=tdgeneral><?php echo($row['tkname']." ".$row['tksurname']); ?>&nbsp;&nbsp;&nbsp;</td>
                <td class=tdgeneral><?php echo($row['uadescrip']); ?>&nbsp;&nbsp;&nbsp;</td>
                <td class=tdgeneral align=right>&nbsp;&nbsp;R <?php echo(number_format($row['uaamount'],2)); ?>&nbsp;</td>
                <td class=tdheader>&nbsp;<input type=button value="Claim " onclick="claimDep(<?php echo($row['uaid']); ?>)">&nbsp;</td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td class=tdheader style="text-align:right" colspan=6>Total amount unbilled:</td>
            <td class=tdheader align=right>&nbsp;R&nbsp;<?php echo(number_format($totamt,2)); ?>&nbsp;</td>
            <td class=tdheader>&nbsp;</td>
        </tr>
    </table>
    <?php
}
mysql_close();
?>

</body>

</html>
