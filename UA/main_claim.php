<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var client = me.claimclient.value;
    var matter = me.claimmatter.value;
    var clientn = me.claimclientname.value;
    var mattern = me.claimmattername.value;
    var urgencyid = me.claimurgencyid.value
    if((client.length == 0 && matter.length == 0 && clientn.length == 0 && mattern.length == 0) || (urgencyid == "X"))
    {
        alert("Please fill in all the required fields.");
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unbilled Advocates - Please bill</b></h1>
<?php
$uaid = $_GET['i'];
$sql = "SELECT * FROM assist_".$cmpcode."_ua_unbilled u, assist_".$cmpcode."_ua_list_advocates a, assist_".$cmpcode."_ua_list_directors d, assist_".$cmpcode."_timekeep t WHERE d.tkid = t.tkid AND u.uaclaimyn = 'N' AND u.uaadvocateid = a.id AND u.uadirectorid = d.id AND uaid = ".$uaid;
include("inc_db_con.php");
$u = mysql_num_rows($rs);
if($u != 1)
{
    echo("<p>An error has occured. Please go back and try again.</p>");
    $err = "Y";
    echo("<p>&nbsp;</p><p><img src=/pics/tri_left.gif align=absmiddle > <a href=main.php class=grey>Go back</a></p>");

}
else
{
    $uarow = mysql_fetch_array($rs);
    $err = "N";
}

if($err == "N")
{
?>
<form name=claim method=post action=main_claim_process.php language=jscript onsubmit="return Validate(this);">
<input type=hidden name=uaid value=<?php echo($uaid); ?>>
<table border="0" id="table2" cellspacing="0" cellpadding="15" bordercolor=006600>
	<tr>
		<td class=tdgeneral>

		<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td colspan="2" valign=top class=tdheader>Billing Details</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Claimed by:</b></td>
		<td class=tdgeneral>
<?php
/*
echo("<select name=claimtkid>");
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000' ORDER BY tkname, tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    echo("<option");
    if($row['tkid']==$tkid)
    {
        echo(" selected");
    }
    echo(" value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
}
mysql_close();
echo("</select>");
*/
echo($tkname);
?>
        </td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Client number:</b></td>
		<td class=tdgeneral><input type=text name=claimclient maxlength=50>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Client name:</b></td>
		<td class=tdgeneral><input type=text name=claimclientname maxlength=100>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Matter number:</b></td>
		<td class=tdgeneral><input type=text name=claimmatter maxlength=50>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Matter name:</b></td>
		<td class=tdgeneral><input type=text name=claimmattername maxlength=100>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral valign=top><b>Comment:</b></td>
		<td class=tdgeneral><textarea name=claimcomment rows=4 cols=30></textarea></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Urgency:</b></td>
		<td class=tdgeneral><select name=claimurgencyid><option selected value=X>--- SELECT ---</option>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_ua_list_urgency WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        echo("<option value=".$row['id'].">".$row['value']."</option>");
    }
    mysql_close();
    ?>
        </select></td>
	</tr>
	<tr>
		<td colspan="2"><input type=submit value=" Bill "> <input type=button value=Cancel onclick="history.back();">&nbsp;</td>
	</tr>
</table>


		</td>
		<td valign=top>
		<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td colspan="2" class=tdheader>Deposit Details</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Invoice Date:</b></td>
		<td class=tdgeneral><?php echo(date("d F Y",$uarow['uainvdate'])); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Advocate:</b></td>
		<td class=tdgeneral><?php echo($uarow['value']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>File number:</b></td>
		<td class=tdgeneral><?php echo($uarow['uafilenum']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Invoice number:&nbsp;</b></td>
		<td class=tdgeneral><?php echo($uarow['uainvnum']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Fee-Earner:&nbsp;</b></td>
		<td class=tdgeneral><?php echo($uarow['tkname']." ".$uarow['tksurname']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Description:</b></td>
		<td class=tdgeneral><?php echo($uarow['uadescrip']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Amount:</b></td>
		<td class=tdgeneral>R <?php echo(number_format($uarow['uaamount'],2)); ?>&nbsp;</td>
	</tr>
	</table>

		</td>
	</tr>
</table></form>
<?php
}
?>

</body>

</html>
