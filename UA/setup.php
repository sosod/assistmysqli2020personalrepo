<?php
    include("inc_ignite.php");

$result = "";
$uaadm = $_POST['uaadm'];
$uaem = $_POST['uaemail'];
$uaval = $_POST['uaval'];
$a = $_POST['a'];
if(strlen($uaadm) > 0 && $a == "admin")
{
    $sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'UA' AND refid = 0";
    include("inc_db_con.php");
    $u = mysql_num_rows($rs);
    if($u == 0)
    {
        $sql2 = "INSERT INTO assist_".$cmpcode."_setup SET ref = 'UA', refid = 0, value = '".$uaadm."', comment = 'Setup administrator', field = ''";
    }
    else
    {
        $sql2 = "UPDATE assist_".$cmpcode."_setup SET value = '".$uaadm."' WHERE ref = 'UA' AND refid = 0";
    }
    mysql_close();
    $sql = $sql2;
    include("inc_db_con.php");
    $sql = "";
    $sql = "SELECT * FROM assist_".$cmpcode."_ua_list_users WHERE tkid = '".$uaadm."' AND yn = 'Y'";
    include("inc_db_con.php");
    $u = mysql_fetch_array($rs);
    if($u == 0)
    {
        $err = "Y";
        $sql = "INSERT INTO assist_".$cmpcode."_ua_list_users SET tkid = '".$uaadm."', yn = 'Y'";
    }
    else
    {
        $err = "N";
        $sql = "";
    }
    mysql_close();
    if($err == "Y")
    {
        include("inc_db_con.php");
    }
    
    $result = "<p><i>Unbilled Advocates Setup Administrator update complete.</i></p>";
    
}
else
{
    if($a == "email")
    {
        if($uaem == "Y")
        {
            $uavalue = $uaval;
        }
        else
        {
            $uavalue = $uaem;
        }
        if(strlen($uavalue) == 0)
        {
            $result = "<p><b>ERROR:</b> If you say 'Yes' to receiving emails then please enter a valid email address in the box provided.<br>Update not done.</p>";
        }
        else
        {
            $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$uavalue."' WHERE ref = 'UA' AND refid = 1";
            include("inc_db_con.php");
            $result = "<p><i>Email receiving update complete.</i></p>";
        }
    }
}

    include("inc_uaadmin.php");
    
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function udEmail() {
    var uaemail = document.email.uaemail.value;
    var target = document.getElementById('toggle');
    if(uaemail == "Y")
    {
        target.style.display = "inline";
        document.email.uaval.value = "";
    }
    else
    {
        target.style.display = "none";
    }
}
</script>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unbilled Advocates - Setup</b></h1>
<?php echo($result); ?>
<ul>
    <form name=admin method=post action=setup.php>
        <li>Unbilled Advocates Setup Administrator: <select name=uaadm>
            <?php
                if($udadmin == "0000")
                {
                    echo("<option selected value=0000>Ignite Assist Administrator</option>");
                }
                else
                {
                    echo("<option value=0000>Ignite Assist Administrator</option>");
                }
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m WHERE m.usrtkid = t.tkid AND t.tkstatus = 1 AND m.usrmodref = 'UA' AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $tk = $row['tkname']." ".$row['tksurname'];
                    if($id == $uaadmin)
                    {
                        echo("<option selected value=".$id.">".$tk."</option>");
                    }
                    else
                    {
                        echo("<option value=".$id.">".$tk."</option>");
                    }
                }
                mysql_close();
            ?>
        </select>&nbsp;<input type=hidden name=a value=admin><input type=submit value=Update></li>
    </form>
    <form name=email method=post action=setup.php>
        <li>Receive an email when deposits are claimed?:<br><select name=uaemail onchange=udEmail()>
            <?php
                if($uaemail == "N")
                {
                    echo("<option selected value=N>No</option>");
                }
                else
                {
                    echo("<option value=N>No</option>");
                }
                if($uaemail == "A")
                {
                    echo("<option selected value=A>Yes - Admins</option>");
                }
                else
                {
                    echo("<option value=A>Yes - Admins</option>");
                }
                if($uaemail != "N" && $uaemail != "A")
                {
                    echo("<option selected value=Y>Yes</option>");
                }
                else
                {
                    echo("<option value=Y>Yes</option>");
                }
            ?>
        </select><span id=toggle>&nbsp;Email: <input type=text name=uaval value=<?php echo($uaemail); ?>></span><input type=hidden name=a value=email>&nbsp;<input type=submit value=Update></li>
    </form>
    <li><a href=setup_advocates.php>Advocates</a></li>
    <li><a href=setup_directors.php>Fee-Earners</a></li>
    <li><a href=setup_users.php>Module Administrators</a></li>
</ul>
<?php
$helpfile = "../help/UA_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
<script language=JavaScript>
var uaemail = document.email.uaemail.value;
var target = document.getElementById('toggle');
if(uaemail == "Y")
{
    target.style.display = "inline";
}
else
{
    target.style.display = "none";
}
</script>
</body>

</html>
