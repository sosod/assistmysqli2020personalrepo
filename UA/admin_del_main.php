<?php
include("inc_ignite.php");

$a = $_GET['a'];
$i = $_GET['i'];

if(strlen($a) > 0 && strlen($i) > 0)
{
    $sql = "UPDATE assist_".$cmpcode."_ua_unbilled SET uaclaimyn = 'D' WHERE uaid = ".$i;
    include("inc_db_con.php");
        $tsql = $sql;
        $tref = "UA";
        $trans = "Deleted Unbilled Advocates record ".$i;
        include("inc_transaction_log.php");
}

$s = $_GET['s'];
if(strlen($s) == 0)
{
    $s = "a";
    $sort = "a.value, t.tkname, t.tksurname, u.uasort";
}
else
{
    switch($s)
    {
        case "id":
            $sort = "u.uainvdate, a.value, t.tkname, t.tksurname";
            break;
        case "a":
            $sort = "a.value, t.tkname, t.tksurname, u.uasort";
            break;
        case "fn":
            $sort = "u.uafilenum, t.tkname, t.tksurname, u.uasort";
            break;
        case "in":
            $sort = "u.uainvnum, t.tkname, t.tksurname, a.value, u.uasort";
            break;
        case "f":
            $sort = "t.tkname, t.tksurname, a.value, u.uasort";
            break;
        case "dp":
            $sort = "u.uadescrip, t.tkname, t.tksurname, u.uasort";
            break;
        default:
            $s = "a";
            $sort = "a.value, t.tkname, t.tksurname, u.uasort";
            break;
    }
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delDep(id) {
    if(confirm("Are you sure you wish to delete unbilled amount "+id+"?")==true)
    {
        var s = document.sort.s.value;
        document.location.href = "admin_del_main.php?a=del&i="+id+"&s="+s;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Unbilled Advocates</b></h1>
<form name=sort><input type=hidden name=s value=<?php echo($s);?>></form>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ua_unbilled u, assist_".$cmpcode."_ua_list_advocates a, assist_".$cmpcode."_ua_list_directors d, assist_".$cmpcode."_timekeep t WHERE d.tkid = t.tkid AND u.uaclaimyn = 'N' AND u.uaadvocateid = a.id AND u.uadirectorid = d.id ORDER BY ".$sort;
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r == 0)
{
    echo("<p>There are no unbilled advocates.&nbsp;</p>");
}
else
{
    ?>
    <table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Ref</td>
        <td class=tdheader><?php if($s!="id"){echo("<a href=admin_del_main.php?s=id style=\"color: #FFFFFF\">");}?>Date Invoiced</a></td>
        <td class=tdheader><?php if($s!="a"){echo("<a href=admin_del_main.php?s=a style=\"color: #FFFFFF\">");}?>Advocate</a></td>
        <td class=tdheader><?php if($s!="fn"){echo("<a href=admin_del_main.php?s=fn style=\"color: #FFFFFF\">");}?>File<br>Number</a></td>
        <td class=tdheader><?php if($s!="in"){echo("<a href=admin_del_main.php?s=in style=\"color: #FFFFFF\">");}?>Invoice<br>Number</a></td>
        <td class=tdheader><?php if($s!="f"){echo("<a href=admin_del_main.php?s=f style=\"color: #FFFFFF\">");}?>Fee-Earner</a></td>
        <td class=tdheader><?php if($s!="dp"){echo("<a href=admin_del_main.php?s=dp style=\"color: #FFFFFF\">");}?>Description</a></td>
        <td class=tdheader>Amount</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
        <?php
        $totamt = 0;
        while($row = mysql_fetch_array($rs))
        {
            $totamt = $totamt+$row['uaamount'];
            ?>
            <tr>
                <td class=tdgeneral align=center><?php echo($row['uaid']); ?></td>
                <td class=tdgeneral><?php echo(date("d M Y",$row['uainvdate'])); ?>&nbsp;</td>
                <td class=tdgeneral><?php echo($row['value']); ?>&nbsp;&nbsp;</td>
                <td class=tdgeneral align=center><?php echo($row['uafilenum']); ?>&nbsp;</td>
                <td class=tdgeneral align=center><?php echo($row['uainvnum']); ?>&nbsp;</td>
                <td class=tdgeneral><?php echo($row['tkname']." ".$row['tksurname']); ?>&nbsp;&nbsp;&nbsp;</td>
                <td class=tdgeneral><?php echo($row['uadescrip']); ?>&nbsp;&nbsp;&nbsp;</td>
                <td class=tdgeneral align=right>&nbsp;&nbsp;R <?php echo(number_format($row['uaamount'],2)); ?>&nbsp;</td>
                <td class=tdheader>&nbsp;<input type=button value="Del " onclick="delDep(<?php echo($row['uaid']); ?>)">&nbsp;</td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td class=tdheader style="text-align:right" colspan=7>Total amount unbilled:</td>
            <td class=tdheader align=right>&nbsp;R&nbsp;<?php echo(number_format($totamt,2)); ?>&nbsp;</td>
            <td class=tdheader>&nbsp;</td>
        </tr>
    </table>
    <?php
}
mysql_close();
?>

</body>

</html>
