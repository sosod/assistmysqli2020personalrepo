<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<a name=top></a>
<h1 class=fc><b>Unbilled Advocate - Admin: Add amounts</b></h1>
<?php
    $err = "N";
    $sql = "SELECT * FROM assist_".$cmpcode."_ua_list_advocates WHERE yn = 'Y' ORDER BY value";
    include("inc_db_con.php");
        $b = mysql_num_rows($rs);
        if($b == 0)
        {
            $err = "Y";
        }
        else
        {
            $err = "N";
        }
    mysql_close();

if($err == "Y")
{
    echo("<p>No Advocates have been set up.  Please set up Advocates before adding any unbilled amounts.</p>");
}
else
{
?>
<form name=add method=post action=admin_add_process.php>
<table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Date Invoiced<br><i><small>(DD MMM YYYY)</small></i></td>
        <td class=tdheader>Advocate</td>
        <td class=tdheader>File<br>Number</td>
        <td class=tdheader>Invoice<br>Number</td>
        <td class=tdheader>Fee-Earner</td>
        <td class=tdheader>Description</td>
        <td class=tdheader>Amount<br><i><small>(Numbers only)</small></i></td>
    </tr>
<?php
for($u=1;$u<11;$u++)
{
?>
    <tr>
        <td class=tdgeneral><input type=text size=2 name=uaday[]><select name=uamon[]>
            <?php
                $nmon = date("n");
                $months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                $m = 0;
                $m2 = 1;
                while($m<=11)
                {
                    if($m2 == $nmon)
                    {
                        echo("<option selected value=".$m2.">".$months[$m]."</option>");
                    }
                    else
                    {
                        echo("<option value=".$m2.">".$months[$m]."</option>");
                    }
                    $m++;
                    $m2++;
                }
            ?>
        </select><input type=text size=4 name=uayear[] value=<?php echo(date("Y"));?>></td>
        <td class=tdgeneral><select name=uaadvocateid[]><option selected value=X>--- SELECT ---</option>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_ua_list_advocates WHERE yn = 'Y' ORDER BY value";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option value=".$row['id'].">".$row['value']."</option>");
            }
            mysql_close();
        ?>
            </select></td>
        <td class=tdgeneral><input type=text name=uafilenum[] size=5></td>
        <td class=tdgeneral><input type=text name=uainvnum[] size=5></td>
        <td class=tdgeneral><select name=uadirectorid[]><option selected value=X>--- SELECT ---</option>
        <?php
            $sql = "SELECT t.tkid, t.tkname, t.tksurname, d.id FROM assist_".$cmpcode."_ua_list_directors d, assist_".$cmpcode."_timekeep t WHERE d.yn = 'Y' AND t.tkid = d.tkid ORDER BY t.tkname, t.tksurname";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option value=".$row['id'].">".$row['tkname']." ".$row['tksurname']."</option>");
            }
            mysql_close();
        ?>
            </select></td>
        <td class=tdgeneral><input type=text maxlength=50 size=30 name=uadescrip[]></td>
        <td class=tdgeneral>R <input type=text size=10 name=uarand[] style="text-align: right">.<input type=text size=2 name=uacents[] value=00></td>
    </tr>
<?php
}
?>
    <tr>
        <td class=tdgeneral colspan=7><input type=submit value=Add> <input type=reset value=Reset><Br>
        <i>Note: In order for an unbilled amount to be added, <u>all</u> the fields in the relevant row must be completed (see below).</td>
    </tr>
    <tr>
        <td class=tdheader colspan=7>Example</td>
    </tr>
    <tr>
        <td class=tdgeneral><input type=text size=2 name=uad value=1><select name=uam>
            <?php
                echo("<option selected value=1>Jan</option>");
            ?>
        </select><input type=text size=4 name=uay value=<?php echo(date("Y"));?>></td>
        <td class=tdgeneral><select name=uaa>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_ua_list_advocates WHERE yn = 'Y' ORDER BY value";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo("<option selected value=".$row['id'].">".$row['value']."</option>");
            mysql_close();
        ?>
            </select></td>
        <td class=tdgeneral><input type=text name=uaf value=123456 size=5></td>
        <td class=tdgeneral><input type=text name=uai value=987654 size=5></td>
        <td class=tdgeneral><select name=uad>
        <?php
            $sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_ua_list_directors d, assist_".$cmpcode."_timekeep t WHERE d.yn = 'Y' AND t.tkid = d.tkid ORDER BY t.tkname, t.tksurname";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
            mysql_close();
        ?>
            </select></td>
        <td class=tdgeneral><input type=text maxlength=50 size=30 name=uah value="Description here"></td>
        <td class=tdgeneral>R <input type=text size=10 name=uar value=10000 style="text-align: right">.<input type=text size=2 name=uac value=00></td>
    </tr>
</table>
</form>
<?php
}
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>

</html>
