var valid8 = "false";
var charU = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I','J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W','X', 'Y', 'Z');
var charL = new Array('a', 'b', 'c', 'd', 'e','f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's','t', 'u', 'v', 'w', 'x', 'y', 'z');
var charN = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

	function checkMe(p) {
		var u = false;
		var l = false;
		var n = false;
		var x = true;
		var c = '';
		if(p.length<6) { 
			return false;
		} else {
			for(var i=0;i<p.length;i++) {
				c = p.charAt(i);
				if($.inArray(c, charU)>=0) {
					u = true;
				} else if($.inArray(c, charL)>=0) {
					l = true;
				} else if($.inArray(c, charN)>=0) {
					n = true;
				} else {
					x = false;
				}
			}
			if(u && l && n) {
				return x;
			} else {
				return false;
			}
		}
	}


function pwd(pn,po)
{
    var chkU = "N";
    var chkL = "N";
    var chkN = "N";
    var chkS = "N";
    if(po.length < 6 || po.length > 20)
    {
        alert("Your old password is invalid.\n\nPlease try again.");
    }
    else
    {
        if(pn.length < 6 || pn.length > 20)
        {
            alert("Your new password is invalid.\n\nPlease try again.");
        }
        else
        {
            var pnl = pn.length;
            var ii;
            var pchk;
            var chkM;
            var x;
            for(i=0;i<pnl;i++)
            {
                chkM = "N";
                ii = i+1;
                pchk = pn.substring(i,ii);
                if(chkM == "N")
                {
                    for(x in charU)
                    {
                        if(pchk == charU[x])
                        {
                            chkU = "Y";
                            chkM = "Y";
                            x = charU.length + 10;
                        }
                    }
                }
                if(chkM == "N")
                {
                    for(x in charL)
                    {
                        if(pchk == charL[x])
                        {
                            chkL = "Y";
                            chkM = "Y";
                            x = charL.length + 10;
                        }
                    }
                }
                if(chkM == "N")
                {
                    for(x in charN)
                    {
                        if(pchk == charN[x])
                        {
                            chkN = "Y";
                            chkM = "Y";
                            x = charN.length + 10;
                        }
                    }
                }
                if(chkM == "N")
                {
                    chkS = "Y";
                    i = pnl + 10;
                }
//                alert(chkU+chkL+chkN+chkS);
            }
            if(chkU == "Y" && chkL == "Y" && chkN == "Y" && chkS == "N")
            {
                valid8 = "true";
            }
            else
            {
                alert("Your password is invalid.\n\nPlease ensure that it meets the\npassword requirements as indicated.");
            }
        }
    }
    return valid8;
}
