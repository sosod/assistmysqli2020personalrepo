<?php
include 'inc_head.php';

$var = $_REQUEST;
//arrPrint($var);
if(isset($var['act'])) {
	$cateid = $_REQUEST['cateid'];
	switch($var['act']) {
		case "SAVE_COMM":
			$result = saveComment($var);
			break;
		case "DEL_COMM":
			$result = deleteComment($var);
			break;
		case "EDIT_COMM":
			$result = editComment($var);
			break;
		default:
				//$result[0] = "error";
				//$result[1] = "Action function for <b>".$var['act']."</b> not set.";
			break;
	}
} else {
	$cateid = $_REQUEST['c'];
	$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
	//$result[0] = "error";
	//$result[1] = "Nothing done. ACT NOT SET";
}	


$sort = isset($_REQUEST['s']) ? $_REQUEST['s']:"";
$f = isset($_REQUEST['f']) ? $_REQUEST['f']  :"";
$find1 = $helper->decode($f);
$find = explode(" ",$find1);

//if(strlen($sort)==0 || ($sort != "d" && $sort != "c")) { $sort = "c"; }
if(strlen($sort)>3) { // == "d") {
    if($sort=="doctitle") {
        $orderby = "doctitle ASC, docdate DESC";
    } else {
        $orderby = $sort." DESC, doctitle ASC";
    }
} else {
    $orderby = "doctitle ASC, docdate DESC";
}

if(strlen($orderby)==0) { $orderby = "doctitle ASC, docdate DESC"; }

$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cateid;
//include("inc_db_con.php");
$cate = $helper->mysql_fetch_one($sql);
//mysql_close();
$cate['catetitle2'] = '';
if($helper->checkIntRef($cate['catesubid'])) {
	$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cate['catesubid'];
	//include("inc_db_con.php");
	$row = $helper->mysql_fetch_one($sql);
	//mysql_close($con);
	if(strlen($row['catetitle'])>0) {
		$cate['catetitle2'] = $row['catetitle']; 
	}
}
if(strlen($cate['catetitle2']) > 0 ) 
	$catetitle = $cate['catetitle2'];
else
	$catetitle = $cate['catetitle'];

?>
<script language=JavaScript>
function deldoc(d) {
    if(confirm("Are you sure you wish to delete document "+d+"?")==true)
    {
        document.location.href = "admin_maintain_doc_delete.php?d="+d;
    }
}
function editdoc(d) {
        document.location.href = "admin_maintain_doc_edit.php?d="+d;
}
function sortby() {
    var c = <?php echo($cateid); ?>;
    var s = document.getElementById('sort').value;
    document.location.href = "admin_maintain_doc.php?c="+c+"&s="+s;
}
</script>
<style type=text/css>
table {
    border-collapse: collapse;
    border: 1px solid #ffffff;
}
table td {
    border: 1px solid #ffffff;
}
h3 {
	margin-top: -5px;
}
</style>
<script type="text/javascript" src="lib/dialog.js"></script>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> Maintain >> <?php echo $catetitle; ?></b></h1>
<?php if($catetitle!=$cate['catetitle']) { echo "<h2>".$cate['catetitle']."</h2>"; } else { echo ""; } ?>
<div id=dispres><?php $helper->displayResult($result); ?></div>
<table width="100%" style="padding: 0px;"><tr><td width=48%>
<h3>Documents<span class=float><input type=button value="Upload a document" onclick="document.location.href = 'admin_upload.php?c=<?php echo($cateid); ?>';"></span></h3>
<form name=veiw action=admin_maintain_doc.php method=get><input type=hidden name=c value=<?php echo($cateid); ?>>
    <table cellpadding=5 cellspacing=0 width=100% style="margin-bottom: 10px;" class=showborder>
        <tr>
            <td width=40% style="border-right: 0px;">Sort by: <select id=sort><option selected value=doctitle>Document Title</option><option value=docdate>Document Date</option><option value=docadddate>Date Added</option><option value=docmoddate>Date Modified</option></select> <input type=button value=Go onclick="sortby();">&nbsp;</td>
            <td width=20% style="border-right: 0px; border-left: 0px;" class=center><?php if(strlen($find1)>0) { ?><input type=button value="All Documents" onclick="document.location.href = 'admin_maintain_doc.php?c=<?php echo($cateid); ?>';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=40% class=right style="border-left: 0px;"><!-- Search: <input type=hidden name=s value=0><input type=text width=15 name=f> <input type=submit value=" Go "> --></td>
        </tr>
    </table></form>
<?php
$mtable = "N";

$sql = "SELECT m.*, icon , dl.value as list
		FROM assist_".$cmpcode."_".$moduledb."_content m
		INNER JOIN assist_".$cmpcode."_".$moduledb."_list_doctype dt
		  ON m.doctypeid = dt.id 
		LEFT OUTER JOIN assist_".$cmpcode."_".$moduledb."_list_type dl
		  ON m.doclistid = dl.id
		WHERE 
		 m.doccateid = ".$cateid." 
		AND (m.docyn = 'Y' || m.docyn = 'U' || m.docyn = 'D') 
		ORDER BY ".$orderby;
//include("inc_db_con.php");
$m = $helper->db_get_num_rows($sql);

if($m > 0)
{
        $rows = $helper->mysql_fetch_all($sql);
        //while($row = mysql_fetch_assoc($rs)) {
        foreach ($rows as $row){
        $docid = $row['docid'];
        $floc2 = "..".$row['doclocation'];
        if(file_exists($floc2))
        {
            if($mtable == "N")
            {
            ?>
            <?php
                echo("<table width=100%>");
                $mtable = "Y";
            }   //IF THE TABLE HEADING HAS BEEN CREATED
            //include("inc_tr.php");
			echo "<tr>";
            echo("        <th width=40>".$row['docid']."</th>");
            echo("        <td><a href='view_dl.php?d=$docid'>");
            $icon = "/pics/icons/".$row['icon'];
            $icon2 = "../pics/icons/".$row['icon'];
                echo("<img src=$icon style=\"vertical-align: middle; margin: 2px 4px 2px 4px;border-width:0px\">");
            if(strlen($row['doctitle']) > 0)
            {
                echo($row['doctitle']);
            }
            else    //IF THERE IS TEXT IN THE TOC
            {
                echo("Untitled");
            }
            echo("</a>"." (".number_format(filesize($floc2)/1024,0)."kb)");
			if($row['docyn']=="D" || $row['docyn']=="U" || $row['docyn']=="O"){
				$mdr = $row['docmodref'];
				$sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '$mdr'";
				//$rws = getRS($sql);
				$result2 = $helper->mysql_fetch_one($sql);
				$store_name=$result2['modtext'];
				switch($row['docyn']){
					case "O":	
						//Doc has been deleted from a Task
						echo $helper->getDisplayIconAsDiv("info","name='tooltip-deletedTask' module='$store_name'");
						//Doc has been uploaded through an Update
						echo $helper->getDisplayIconAsDiv("info","name='tooltip-updatedTask' module='$store_name'");					
						break;
					case "D":
						//Doc has been deleted from a Task
						echo $helper->getDisplayIconAsDiv("info","name='tooltip-deletedTask' module='$store_name'");
						break;
					case "U":
						//Doc has been uploaded through an Update
						echo $helper->getDisplayIconAsDiv("info","name='tooltip-updatedTask' module='$store_name'");					
						break;
					default:
						break;
				}	
			}
            if(strlen($row['doccontent'])>0)
            {
            	$row['doccontent'] = stripslashes($row['doccontent']);
                $content = strFn("str_replace",$row['doccontent'],chr(10),"<br>");
                echo("<ul style=\"margin: 0 0 0 35;\"><span style=\"font-style:italic;font-size:7pt;\">".$content."</span></ul>");
            }
			$type = isset($row['list']) && strlen($row['list'])>0 ? $row['list'] : "";
			echo "<div style=\"margin-left: 20px; font-size: 7pt;\">".$type."</div>";
			
            echo("</td>");
            echo("        <td width=185 class=right style=\"font-style: italic; font-size: 7pt;\"><span style=\"font-size: 8pt;font-style: normal;\">&nbsp;".date("d F Y",$row['docdate'])."</span>");
            $d1 = $row['docadddate'];
            $d2 = $row['docmoddate'];
                echo("<br />Added on ".date("d M Y H:i",$row['docadddate'])."");
            $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['docadduser']."'";
            //include("inc_db_con2.php");
                if($helper->db_get_num_rows($sql2)>0)
                {
                    $tkrow = $helper->mysql_fetch_one($sql2);
                    $aname = $tkrow['tkname']." ".$tkrow['tksurname'];
                } else {
                    $aname = "Unknown";
                }
            //mysql_close($con2);
                echo("<br />by ".$aname."");
            if($d1!=$d2 && $d2 > $d1) {
                echo("<br />Modified on ".date("d M Y H:i",$row['docmoddate'])."");
            $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['docmoduser']."'";
            //include("inc_db_con2.php");
                if($helper->db_get_num_rows($sql2)>0)
                {
                    $tkrow = $helper->mysql_fetch_one($sql2);
                    $dname = $tkrow['tkname']." ".$tkrow['tksurname'];
                } else {
                    $dname = "Unknown";
                }
            //mysql_close($con2);
                echo("<br />by ".$dname."");
            }
            echo("</td>");
            ?><td class="right top" width=60><input type=button value=" Edit " onclick="editdoc(<?php echo($row['docid']); ?>);"> <input type=button value=Delete onclick="deldoc(<?php echo($row['docid']); ?>);"></td><?php
            echo("    </tr>");
        } else {  //IF THE FILE EXISTS
			$m--;
		}
    }   //WHILE THERE ARE ROWS TO READ
}   //IF THERE ARE ROWS TO READ
//mysql_close();
if($mtable == "Y")
{
    echo("</table>");
}
//else
if($m<=0)
{
    echo("<p>No documents to display.</p>");
}
echo "<span class=float>"; $helper->displayGoBack("",""); echo "</span>";
		$helper->displayGoUp("#top","Back to Top");
?>
</td><td width=4%>&nbsp;</td><td width=48%>
<?php adminComments($cateid); ?>
</td></tr></table>
<?php drawCommentDialog("admin_maintain_doc.php",$cateid); ?>
<script type=text/javascript>
	$(function() {   
		$("a").hover(
			function() { $(this).css("color","#FE9900"); },
			function() { $(this).css("color","#000099"); }
		);
	<?php if(isset($abc) && $abc == "USE THIS FUNCTION?" && isset($result) && $result[0]=="ok") { ?>
		setTimeout(function() {         
			$("#dispres").hide('blind', {}, 500)     
		}, 1500); 
<?php } ?>

		$("[name='tooltip-deletedTask']").each(function(){
			mod = $(this).attr("module");
			$(this).attr("title","This item was deleted from the "+mod+" module");
		});
		$("[name='tooltip-updatedTask']").each(function(){
			$(this).css("background-image","url(../library/jquery/css/images/ui-icons_000099_256x240.png)");
			//$(this).attr("background-image","../library/jquery/css/images/ui-icons_000099_256x240.png");
			mod = $(this).attr("module");
			$(this).attr("title","This item was uploaded during an Update in the "+mod+" module");
		});
	
	});
</script>
</body>
</html>
