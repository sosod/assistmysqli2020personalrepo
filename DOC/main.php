<?php
require_once 'inc_head.php';

$type = isset($_GET['t']) ? $_GET['t'] : "";
$f = isset($_GET['f']) ? $_GET['f'] : "";
if(strtoupper($type) == "V") {
} else {
    if($access['startpage'] != "main.php" && strlen($access['startpage'])>0) {
        ?>
        <script type=text/javascript>
        var url = '<?php echo($access['startpage']); ?>';
        document.location.href = url;
        </script>
        <?php
    }
}

$start = isset($_GET['s']) ? $_GET['s'] : "";

if($access['view']=="Y" || $access['docadmin'] == "Y") {
    $sqlcate = "FROM assist_".$cmpcode."_".$moduledb."_categories c WHERE c.cateyn = 'Y' AND c.catesubid = 0 ";
} else {
    $sqlcate = "FROM assist_".$cmpcode."_".$moduledb."_categories c, ".$dbref."_categories_users cu  WHERE c.cateyn = 'Y' AND c.catesubid = 0 ";
    $sqlcate.= "AND c.cateid = cu.cucateid AND cu.cuyn = 'Y' AND cu.cutype = 'VIEW' AND cu.cutkid = '$tkid' ";
}

$find1 = $helper->decode($f);
$find = explode(" ",$find1);
if(strlen($start)==0 || (!is_numeric($start) && $start != "e")) { $start = 0; }
$limit = 20;
$sql = "SELECT count(catetitle) as ct ".$sqlcate;
foreach($find as $needle)
{
        $needle = str_replace("'","&#39",$needle);
        $sql.= " AND c.catetitle LIKE '%".$needle."%'";
}
$sql.=" ORDER BY catetitle";
//include("inc_db_con.php");
    $mr = $helper->mysql_fetch_one($sql);
//mysql_close($con);
$max = $mr['ct'];
$pages = ceil($max/$limit);
if(!is_numeric($start)) {
    $start = $max - ($max - ($limit*($pages-1)));
}
$page = ($start / $limit) + 1;
?>
<script type=text/javascript>
function goNext(s,f) {
    f = escape(f);
    document.location.href = "main.php?t=V&s="+s+"&f="+f;
}
</script>
<style type=text/css>
table td {
	border-color: #ffffff;
}

#tbl_filter, #tbl_filter td {
	border-color: #ababab;
}
</style>
<h1>View</h1>
<form name=veiw action=view.php method=get><input type=hidden name=c value=0>
    <table width=650 style="margin-bottom: 10px;" id=tbl_filter>
        <tr>
            <td width=250 style="border-right: 0px;"><input type=button id=b1 value="|<" onclick="goNext(0,'<?php echo($find1)?>');"> <input type=button id=b2 value="<" onclick="goNext(<?php echo($start-$limit); ?>,'<?php echo($find1)?>');"> Page <?php echo($page); ?>/<?php echo($pages); ?> <input type=button value=">" id=b3 onclick="goNext(<?php echo($start+$limit); ?>,'<?php echo($find1)?>');"> <input type=button value=">|" id=b4 onclick="goNext('e','<?php echo($find1)?>');"></td>
            <td width=150 style="border-right: 0px; border-left: 0px;" align=center><?php if(strlen($find1)>0) { ?><input type=button value="All Categories" onclick="document.location.href = 'main.php?t=V';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=250 align=right style="border-left: 0px;text-align:right">
            	Search: <input type=hidden name=s value=0><input type=text width=15 name=f><br /><input type=checkbox value=1 name=inc_subs id=inc_subs checked /> <label for=inc_subs class=i style='font-size:75%'>Include subfolders? </label> <input type=submit value=" Go ">
            	<div style='position:absolute; top:0px;right:0px;'><button id=btn_search>Advanced Search</button></div>
            </td>
        </tr>
    </table></form>
    <script type="text/javascript" >
    	$(function() {
    		$("#btn_search").button().hide().click(function(e) { e.preventDefault(); document.location.href = 'search.php?cate_id=<?php echo $cateid; ?>';});
    	});
    </script>
<?php
$sql = "SELECT * ".$sqlcate;
foreach($find as $needle)
{
    $needle = str_replace("'","&#39",$needle);
    $sql.= " AND c.catetitle LIKE '%".$needle."%'";
}
$sql.=" ORDER BY catetitle LIMIT ".$start.", ".$limit;
//include("inc_db_con.php");
$c = $helper->db_get_num_rows($sql);
if($c == 0)
{
    echo("<p>There are no documents to display.</p>");
}
else
{
    ?>
    <table width=650 style="border-color: #ffffff;">
    <?php
    $ref = $start;
    $rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){
	$sql2 = "SELECT count(docid) as dc FROM assist_".$cmpcode."_".$moduledb."_content m WHERE doccateid = ".$row['cateid']." AND (docyn = 'Y' OR docyn ='D')";
	//include("inc_db_con2.php");
		$row2 = $helper->mysql_fetch_one($sql2);
		$dc = $row2['dc'];
	//mysql_close($con2);
	if($dc>0) {
		$icon = "/pics/icons/folder_full.gif";
	} else {
		$icon = "/pics/icons/folder_empty.gif";
	}
        $ref++;
        $url = "view.php?c=".$row['cateid'];
        
        ?>
		<tr>
            <td style=""><a href="<?php echo($url); ?>" style="text-decoration:underline;color: #000000"><img src="<?php echo($icon); ?>" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;"><?php echo($row['catetitle']); ?></a></td>
        </tr>
        <?php
    }
    ?>
    </table>
    <?php
}
//mysql_close();
?>
<p>&nbsp;</p>
<script type=text/javascript>
$(document).ready(function() {
	$("a").hover(
		function(){ $(this).css("color","#FE9900"); },
		function(){ $(this).css("color","#000000"); }
	);
});
<?php if($start==0) {?>document.getElementById('b1').disabled = true; document.getElementById('b2').disabled = true; <?php } ?>
<?php if($start == ($max - ($max - ($limit*($pages-1)))) || $pages == 0) { ?>document.getElementById('b3').disabled = true; document.getElementById('b4').disabled = true;<?php } ?>
</script>
</body>

</html>
