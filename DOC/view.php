<?php
require_once 'inc_head.php';

$db = new ASSIST_DB();

//arrPrint($_REQUEST);
$cateid = isset($_REQUEST['c']) ? $_REQUEST['c'] : 0;
$inc_subs = isset($_REQUEST['inc_subs']) ? $_REQUEST['inc_subs'] : 0;
$sort = isset($_REQUEST['s']) ? $_REQUEST['s'] : "";
$f = isset($_REQUEST['f']) ? $_REQUEST['f'] : "";
$find1 = $helper->decode($f);
if(strlen($find1)>0) {
	$find = explode(" ",$find1);
} else {
	$find = false;
}

/*if(strlen($sort)==0 || ($sort != "d" && $sort != "c")) { $sort = "c"; }
if($sort == "d") {
    $orderby = "docdate DESC, doctitle ASC";
} else {
    $orderby = "doctitle ASC, docdate DESC";
}*/
//if(strlen($sort)==0 || ($sort != "d" && $sort != "c")) { $sort = "c"; }
if(strlen($sort)>3) { // == "d") {
    if($sort=="doctitle") {
        $orderby = "doctitle ASC, docdate DESC";
    } else {
        $orderby = $sort." DESC, doctitle ASC";
    }
} else {
    $orderby = "doctitle ASC, docdate DESC";
}


if(strlen($orderby)==0) { $orderby = "doctitle ASC, docdate DESC"; }
if($cateid>0) {
	$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cateid;
	$cate = $helper->mysql_fetch_one($sql);
	$cate['catetitle2'] = '';
}
$catesubid = isset($cate['catesubid']) ? $cate['catesubid']:0;
if($helper->checkIntRef($catesubid)) {
	$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cate['catesubid'];
//	$rs = getRS($sql);
//	$row = mysql_fetch_array($rs);
//	mysql_close($con);
	$row = $helper->mysql_fetch_one($sql);
	if(strlen($row['catetitle'])>0) {
		$cate['catetitle2'] = $row['catetitle']; 
	}
}
$catetitle2 = isset($cate['catetitle2']) ? $cate['catetitle2'] : "";
if(strlen($catetitle2) > 0 ) {
	$catetitle = $catetitle2;
}else {
	$catetitle = $catetitle2;
}
$cate_title = isset($cate['catetitle']) ? $cate['catetitle'] : "";
//mysql_close();
?>

<h1><a href=main.php?t=v class=breadcrumb>View</a><?php echo ($cateid>0) ? ">> $catetitle " : ""; ?></h1>

<?php if($catetitle!=$cate_title) { echo "<span style=\"float: left; margin-top: -15px;\"><h2>".$cate_title."</h2>"; } else { echo "<span style=\"float: left;\">"; } ?>
<form name=veiw action=view.php method=get><input type=hidden name=c value=<?php echo($cateid); ?>>
    <table cellpadding=5 cellspacing=0 width=650 style="margin-bottom: 10px;">
        <tr>
            <td width=250 style="border-right: 0px;vertical-align:middle;">Sort by: <select id=sort><option selected value=doctitle>Document Title</option><option value=docdate>Document Date</option><option value=docadddate>Date Added</option><option value=docmoddate>Date Modified</option></select> <input type=button value=Go onclick="sortby();">&nbsp;</td>
            <td width=150 style="border-right: 0px; border-left: 0px;vertical-align:middle;" align=center><?php if(strlen($find1)>0) { ?><input type=button value="All Documents" onclick="document.location.href = 'view.php?c=<?php echo($cateid); ?>';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=250 align=right style="border-left: 0px;text-align:right">
            	Search: <input type=hidden name=s value=0><input type=text width=15 name=f><br /><input type=checkbox value=1 name=inc_subs id=inc_subs checked /> <label for=inc_subs class=i style='font-size:75%'>Include subfolders? </label> <input type=submit value=" Go ">
            	<div style='position:absolute; top:0px;right:0px;'><button id=btn_search>Advanced Search</button></div>
            </td>
        </tr>
    </table></form>
    <script type="text/javascript" >
    	$(function() {
    		$("#btn_search").button().hide().click(function(e) { e.preventDefault(); document.location.href = 'search.php?cate_id=<?php echo $cateid; ?>';});
    	});
    </script>
<?php

//if search is happening
	//get all folders & sub folders irrespective of search
	//display folders that meet search requirement
	//get all documents that meet search requirements & are in the subfolders
//else
	//get all folders that are child to current folder
	//display all folders
	//display content of current folder


//Set categories array to current folder
$categories = array($cateid);
//collection of all sub folder ids to facilitate searching for documents
//echo "<P>:".$cateid.":</p>";
$doc_categories = array($cateid);
//complete list of all categories to assist with naming
$all_categories = array();
//categories that meet the search criteria for display purposes
$search_categories = array();

$search_words = $find;//explode(" ",$find);
if($cateid>0) {
	if($find!==false && is_array($find) && count($find)>0 && $inc_subs==true) {
		//While the sub categories exist, keep looking for subfolders
		while(count($categories)>0) {
			$sql = "SELECT c.* 
					FROM assist_".$cmpcode."_".$moduledb."_categories c 
					WHERE cateyn = 'Y' AND catesubid IN (".implode(",",$categories).") ORDER BY catetitle";
			$cate = $db->mysql_fetch_all_by_id($sql,"cateid");
			if(count($categories)>0) {
				//get keys from the latest categories	
				$categories = array_keys($cate);
				//add categories to the complete list
				$all_categories = $all_categories + $cate;
		
				//add categories keys to the document list
				$doc_categories = array_merge($doc_categories, $categories);
		//arrPrint($doc_categories);
				//loop through categories 
				foreach($cate as $ci => $c) {
					$title = $c['catetitle'];
					//check if title contains the search terms
					$found = true;
					foreach($search_words as $s) {
						if($found && stripos($title,$s)===false) {
							$found = false;
							break;
						}
					}
					if($found) {
						//set the folder name to include full path
						$catesubid = isset($c['catesubid']) ? $c['catesubid'] : "";
						while($catesubid!="X") {
							if(isset($all_categories[$catesubid])) {
								$x = $all_categories[$catesubid];
								$xtitle = $x['catetitle'];
								$c['catetitle'] = $xtitle." / ".$c['catetitle'];
								$catesubid = ($x['catesubid']!=$cateid) ? $x['catesubid'] : "X";
							} else {
								$catesubid = "X";
							}
						}
						//add folder to list to display
						$search_categories[$ci] = $c;
					}
				}
			} else {
				$categories = array();
			}
		}
		
	} else {
		//get immediate sub folders with simple search
		$sql = "SELECT c.*, count(d.docid) as dc 
				FROM assist_".$cmpcode."_".$moduledb."_categories c
				 LEFT OUTER JOIN assist_".$cmpcode."_".$moduledb."_content d
				 ON d.doccateid = c.cateid
				WHERE c.cateyn = 'Y' AND c.catesubid = $cateid ";
		if($find!==false && is_array($find) && count($find)>0) {
			foreach($find as $needle) {
			    $needle = str_replace("'","&#39",$needle);
			    $sql.= " AND c.catetitle LIKE '%".$needle."%'";
			}
		}
		$sql.=" GROUP BY c.cateid 
				ORDER BY c.catetitle";
		$search_categories = $db->mysql_fetch_all_by_id($sql,"cateid");

	}
} else {
	if($access['view']=="Y" || $access['docadmin'] == "Y") {
	    $sqlcate = "FROM assist_".$cmpcode."_".$moduledb."_categories c WHERE c.cateyn = 'Y' AND c.catesubid = 0 ";
	} else {
	    $sqlcate = "FROM assist_".$cmpcode."_".$moduledb."_categories c, ".$dbref."_categories_users cu  WHERE c.cateyn = 'Y' AND c.catesubid = 0 ";
	    $sqlcate.= "AND c.cateid = cu.cucateid AND cu.cuyn = 'Y' AND cu.cutype = 'VIEW' AND cu.cutkid = '$tkid' ";
	}
	$sql = "SELECT * ".$sqlcate;
	$sql.=" ORDER BY catetitle";
	$user_access_categories = $db->mysql_fetch_all_by_id($sql,"cateid");
//arrPrint($user_access_categories);
	$all_categories = $user_access_categories;
	$categories = array_keys($user_access_categories);
//arrPrint($categories);
	if($find!==false && is_array($find) && count($find)>0 && $inc_subs==true) {
		//While the sub categories exist, keep looking for subfolders
		while(count($categories)>0) {
			$sql = "SELECT c.* 
					FROM assist_".$cmpcode."_".$moduledb."_categories c 
					WHERE cateyn = 'Y' AND catesubid IN (".implode(",",$categories).") ORDER BY catetitle";
			$cate = $db->mysql_fetch_all_by_id($sql,"cateid");
//arrPrint($cate);
			if(count($cate)>0) {
				//get keys from the latest categories	
				$categories = array_keys($cate);
				//add categories to the complete list
				$all_categories = $all_categories + $cate;
//arrPrint($all_categories);		
				//add categories keys to the document list
				$doc_categories = array_merge($doc_categories, $categories);
		
				//loop through categories 
				foreach($cate as $ci => $c) {
					$title = $c['catetitle'];
					//check if title contains the search terms
					$found = true;
					foreach($search_words as $s) {
						if($found && stripos($title,$s)===false) {
							$found = false;
							break;
						}
					}
					if($found) {
						//set the folder name to include full path
						$catesubid = $c['catesubid'];
						while($catesubid!="X") {
							if(isset($all_categories[$catesubid])) {
								$x = $all_categories[$catesubid];
								$xtitle = $x['catetitle'];
								$c['catetitle'] = $xtitle." / ".$c['catetitle'];
								$catesubid = ($x['catesubid']!=$cateid) ? $x['catesubid'] : "X";
							} else {
								$catesubid = "X";
							}
						}
						//add folder to list to display
						$search_categories[$ci] = $c;
					}
				}
			} else {
				$categories = array();
			}
		}
	} else {
		$search_categories = $user_access_categories;
	}

}
//$rs = getRS($sql);
//arrPrint($search_categories);
?>
    <table width=650 style="border-color: #ffffff;margin:-5 5 10 5;">
    <?php
    $start = isset($start) ? $start  :0;
    $ref = $start;
    //while($row = mysql_fetch_array($rs))
    foreach($search_categories as $row) {
	$sql2 = "SELECT count(docid) as dc FROM assist_".$cmpcode."_".$moduledb."_content m WHERE doccateid = ".$row['cateid']." AND (docyn = 'Y' OR docyn = 'U')";
	/*if($find!==false && is_array($find) && count($find)>0) {
		foreach($find as $needle) {
		    $needle = str_replace("'","&#39",$needle);
		    $sql.= " AND (d.doctitle LIKE '%".$needle."%' OR d.doccontent LIKE '%".$needle."%') ";
		}
	}*/
//	include("inc_db_con2.php");
//		$row2 = mysql_fetch_array($rs2);
		$row2 = $helper->mysql_fetch_one($sql2);
		$dc = $row2['dc'];
//	mysql_close($con2);
	if($dc>0) {
		$icon = "/pics/icons/folder_full.gif";
	} else {
		$icon = "/pics/icons/folder_empty.gif";
	}
        $ref++;
        $url = "view.php?c=".$row['cateid'];
        
        ?>
		<tr>
            <td style="border-color:#ffffff;"><a href="<?php echo($url); ?>" style="text-decoration:underline;color: #000000"><img src="<?php echo($icon); ?>" style="border-width:0px;vertical-align: middle;margin: 2px 4px 2px 10px;"><?php echo($row['catetitle']); ?></a></td>
        </tr>
        <?php
    }
    ?>
    </table>
<?php
//mysql_close();
//arrPrint($doc_categories);
if( ($cateid==0 && $find!==false && is_array($find) && count($find)>0) || $cateid>0) {
	
	$sql = "SELECT m.*, t.icon, CONCAT_WS(' ',addu.tkname, addu.tksurname) as addname , l.value as list
			FROM ".$dbref."_content m
			INNER JOIN ".$dbref."_list_doctype t
			ON m.doctypeid = t.id 
			LEFT OUTER JOIN ".$dbref."_list_type l
			ON l.id = m.doclistid
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep addu
			ON addu.tkid = m.docadduser
			WHERE m.doccateid IN (".implode(",",$doc_categories).") AND (m.docyn IN ('Y','U','D')) 
			";
			$where = "";
			if($find!==false && is_array($find) && count($find) >0) {
				$w = array();
				$jf= implode("|_|",$find);
				$jf = str_replace("'","&#39",$jf); //str_replace($search, $replace, $subject)
				$needle = explode("|_|",$jf);
				$w[] = "m.doctitle LIKE '%".implode("%' AND m.doctitle LIKE '%",$needle)."%'";
				$w[] = "m.doccontent LIKE '%".implode("%' AND m.doccontent LIKE '%",$needle)."%'";
				//foreach($find as $needle) {
				//	$needle = str_replace("'","&#39",$needle);
				//	
				//}
				$where = " AND (".implode(" OR ",$w).")";
			}
			
	$sql.= $where." 
			ORDER BY ".$orderby;
			//echo $sql;
	//$rs = getRS($sql);
	$m = $helper->db_get_num_rows($sql);
	$rows = $helper->mysql_fetch_all($sql);
	
	$mtable = "N";
	if($m > 0)
	{
	    //while($row = mysql_fetch_array($rs)) {
        foreach ($rows as $row){
	        $docid = $row['docid'];
	        $floc2 = "..".$row['doclocation'];
	        if(file_exists($floc2)) {
	            if($mtable == "N") {
	                echo "<table width=650 id=tbl_doc>";
	                $mtable = "Y";
	            }   //IF THE TABLE HEADING HAS BEEN CREATED
				echo "<tr>";
	            echo "        <td>";
				if($find!==false && is_array($find) && count($find)>0){
						$catesubid = $row['doccateid']; 
						while($catesubid!="X") {
							if(isset($all_categories[$catesubid])) {
								$x = $all_categories[$catesubid];
								$xtitle = $x['catetitle']; 
								$row['catetitle'] = $xtitle.(isset($row['catetitle'])?" / ".$row['catetitle']:"");
								$catesubid = ($x['catesubid']!=$cateid) ? $x['catesubid'] : "X";
							} else {
								$catesubid = "X";
							}
						}
						$folder_icon = ASSIST_HELPER::drawStatus("folder-open","black");
					echo "<div style='font-size:75%;padding-left:25px;' class=i>".$folder_icon.$row['catetitle']."</div>";
				}
	            echo "<a href='view_dl.php?d=$docid'>";
	            $icon = "/pics/icons/".$row['icon'];
	            $icon2 = "../pics/icons/".$row['icon'];
	            if(file_exists($icon2)) {
	                echo "<img src=$icon style=\"vertical-align: middle; margin: 2 4 2 10;border-width:1px; border-color: #FFFFFF;\" class=a>";
	            }
	            if(strlen($row['doctitle']) > 0) {
	                echo $row['doctitle'];
	            } else {    //IF THERE IS TEXT IN THE TOC
	                echo "Untitled" ;
	            }
	            echo "</a>"." (".number_format(filesize($floc2)/1024,0)."kb)";
				$type = isset($row['list']) && strlen($row['list'])>0 ? $row['list'] : "";
	            if(strlen($row['doccontent'])>0) {
	                $content = strFn("str_replace",$row['doccontent'],chr(10),"<br>");
	                echo "<ul style=\"margin: 0 0 0 35;\"><span style=\"font-style:italic;font-size:8pt;\">".$content."</span></ul>";
	            }
				echo "<div style=\"margin-left: 20px; font-size: 7pt;\">".$type."</div>";
	            echo "</td>";
	            echo "        <td width=185 style=\"text-align: right; font-size: 7pt; font-style: italic\">";
				echo "&nbsp;".date("d F Y",$row['docdate'])."";
				$exp_date = isset($row['docexpdate']) ? $row['docexpdate'] :"0000-00-00";
				if(strlen($exp_date)>0 && $exp_date!="0000-00-00" && !empty($exp_date) && !is_null($exp_date)) {
	                echo "<br />Expiration: ".date("d M Y",strtotime($exp_date." 00:00:00"))."";
				}
	            $d1 = $row['docadddate'];
	            $d2 = $row['docmoddate'];
	                echo "<br />Added on ".date("d M Y H:i",$row['docadddate'])."";
	
				$aname = isset($row['addname']) && strlen($row['addname']) ? $row['addname'] : "Unknown";
	                echo "<br />by ".$aname."";
	            if($d1!=$d2 && $d2 > $d1) {
	                echo "<br />Modified on ".date("d M Y H:i",$row['docmoddate'])."";
	            $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['docmoduser']."'";
	            //$rs2 = getRS($sql2);
	                if($helper->db_get_num_rows($sql2)>0) {
	                    $tkrow = $helper->mysql_fetch_one($sql2);
	                    $dname = $tkrow['tkname']." ".$tkrow['tksurname'];
	                } else {
	                    $dname = "Unknown";
	                }
	                echo "<br />by ".$dname."";
	            }
	            echo "</td>";
	            echo "    </tr>";
	        }   //IF THE FILE EXISTS
	    }   //WHILE THERE ARE ROWS TO READ
	}   //IF THERE ARE ROWS TO READ
	//mysql_close();
	if($mtable == "Y") {
	    echo "</table>";
	} else {
	    echo "<p>No documents to display.</p>";
	}

    $helper->displayGoBack("","");
}
?>
</span>
<script type=text/javascript>
$(document).ready(function() {
	$("#tbl_doc, #tbl_doc td").addClass("noborder");
	$("a").hover(
		function(){ $(this).css("color","#FE9900"); },
		function(){ $(this).css("color","#000000"); }
	);
	$(".a").hover(
		function(){ $(this).css("border-color","#FE9900"); },
		function(){ $(this).css("border-color","#FFFFFF"); }
	);
	$("div.ui-state-black").css({"margin":"0px 5px 0px 0px","float":"left"});
});
function sortby() {
    var c = <?php echo($cateid); ?>;
    var s = document.getElementById('sort').value;
    document.location.href = "view.php?c="+c+"&s="+s;
}
</script>
<?php
viewComments($cateid);
?>
</body>
</html>
