<?php
//error_reporting(-1);
class DOC extends ASSIST_MODULE_HELPER {
		
	protected $tablename;

	public function __construct($tablename="") {
		if(strlen($tablename)>0){
			$this->tablename = strtolower($tablename);
		}else{
			//$this->tablename = "doc";
			$this->tablename = strtolower($_SESSION['modref']);
		}
		//echo "Constructing DOC with tablename: ".$this->tablename;	
		parent::__construct();
	}
	
	
	public function addDoc($doctitle,$doccontents,$docdate,$docyn,$doccateid,$doclistid,$doctypeid,$docfile){
		if(strlen($doctitle)>0 && checkIntRef($doccateid) && checkIntRef($doctypeid) && strlen($docdate)>0 && strlen($docfile) > 0){
			//format date	
			$docdt = strtotime($docdate." 12:00:00");
			//fill in missing values
			$docadddate = strtotime();
    		$docadduser = $this->getUserID();
			//first sql load
      		$sql = "INSERT INTO ".$this->getDBRef()."_content (doctitle, doccontent, doccateid, docdate, doclocation, docfilename, doctypeid, docyn, docadduser, docadddate, docmoduser, docmoddate, doclistid) VALUES ";
        	$sql.= "('$doctitle','$doccontents',$doccateid,$docdt,'','$docfile',$doctypeid,'$docyn','$docadduser',$docadddate,'$docadduser',$docadddate, $doclistid)";
        	$docID = $this->db_insert($sql);
			if($this->checkIntRef($docID)){
				return array("ok",$docID);
			}else{
				return array("error","SQL insert failed! DOC/class/doc.php - see 19-20");
			}
		}else{
			return array("error",array("invalid parameters:"=>array($doctitle,$doccontents,$docdate,$docyn,$doccateid,$doclistid,$doctypeid,$docfile)));
		} 
	}
	
	public function addActionDoc($actName,$actTitle,$actID,$docfile,$tmpname){
		$step = 1;
		echo("<P>$step. Getting document details...");  $step++;	
		$cmpcode = $this->getCmpCode();
		$modref = $this->getModRef();	
		//Checking file name is set, and title of Task is set	
		if(strlen($actTitle)>0 && strlen($docfile) > 0){
			//Cutting the filename if it exceeds 26 chars - will be inserted into contents section	
			$doctitle = "Attachment from ".ucfirst($actName)." ".$actID." - '";	
            if(strlen($actTitle) > 50){
            	$doctitle .= preg_replace('/\s+?(\S+)?$/', '', substr($actTitle, 0, 26)) . "...";
            }else{
            	$doctitle .= $actTitle;
            }
        	$doctitle .= "'";
        	$doctitle = mysqli_real_escape_string($this->getConn(),$doctitle);
			$doccontents = "Attachment from ".ucfirst($actName)." $actID - '$docfile'";
        	$doccontents = mysqli_real_escape_string($this->getConn(),$doccontents);
        	//Checking if category (folder) exists - if not, creating one (for all attachments in this module)
			$catCheck = $this->checkCategory($actName);
			$doccateid = $catCheck > 0 ? $catCheck : $this->addCategory($actName);
			if(!$doccateid > 0){
				return array("error","SQL insert failed upon category creation! DOC/class/doc.php - see 45");
			}
			//Setting defaults (List '1' and Type 'other')
			$docyn = "T";	
			$doctypeid = 7;
			$doclistid = 1;	
			//fill in today's date (as a timestamp) and current user
			$docadddate = time();
    		$docadduser = $this->getUserID();
			echo("Done.</p><p>$step. Creating temporary document..."); $step++;
			//sql load
      		$sql = "INSERT INTO assist_".$cmpcode."_".$this->tablename."_content (doctitle, doccontent, doccateid, docdate, doclocation, docfilename, doctypeid, docyn, docadduser, docadddate, docmoduser, docmoddate, doclistid, docmodref, docactionlink) VALUES ";
        	$sql.= "('$doctitle','$doccontents',$doccateid,$docadddate,'','$docfile',$doctypeid,'$docyn','$docadduser',$docadddate,'$docadduser',$docadddate, $doclistid,'".$modref."', $actID)";
        	$docID = $this->db_insert($sql);
			if($this->checkIntRef($docID)){
				//Store the file in an appropriate location
				//Location: /files/$cmpcode/$moduledb/docdate(yyyy)/docdate(mm)/docid_docdate(yyyymmdd)_today(yyyymmddhhiiss).ext
		        $doc = explode(".",$docfile);
		        $docc = count($doc)-1;
		        $docext = $doc[$docc];
		        $path = "".$this->tablename."/".date("Y",$docadddate)."/".date("m",$docadddate);
				$this->checkFolder($path);
		        $doclocation = "/files/".$cmpcode."/".$path."/".$docID."_".date("Ymd",$docadddate)."_".date("YmdHis",$docadddate).".".$docext;

		        //$doclocation = "/files/".$cmpcode."/".strtoupper($this->tablename)."/".date("Y",$docadddate)."/".date("m",$docadddate)."/".$docID."_".date("Ymd",$docadddate)."_".date("YmdHis",$docadddate).".".$docext;
				echo("Done.</p> <p>$step. Uploading document..."); $step++;
		        //upload document
/*		        $chkloc = "../files/".$cmpcode;
		        if(!is_dir($chkloc)) { mkdir($chkloc); }
		        $chkloc = "../files/".$cmpcode."/".strtoupper($this->tablename);
		        if(!is_dir($chkloc)) { mkdir($chkloc); }
		        $chkloc = "../files/".$cmpcode."/".strtoupper($this->tablename)."/".date("Y",$docadddate);
		        if(!is_dir($chkloc)) { mkdir($chkloc); }
		        $chkloc = "../files/".$cmpcode."/".strtoupper($this->tablename)."/".date("Y",$docadddate)."/".date("m",$docadddate);
		        if(!is_dir($chkloc)) { mkdir($chkloc); }*/
		        copy($tmpname, "..".$doclocation);
				echo("Done.</p> <p>$step. Updating document..."); $step++;
				//Update the database with the new location of the file
		        $sql = "UPDATE assist_".$cmpcode."_".$this->tablename."_content SET doclocation = '$doclocation', docyn = 'Y' WHERE docid = $docID";
				$result1 = $this->db_update($sql);
				if($this->checkIntRef($result1)){
					echo("Done.</p>");
					//echo "<br>Insert ID: $docID";
					return array("ok",$docID);
				}else{
					echo("FAILED. Please contact your Assist Administrator, and try to show them this screen!</p>");	
					return array("error","SQL insert failed upon document location update! DOC/class/doc.php - see 90");
				}
			}else{
				return array("error","SQL insert failed! DOC/class/doc.php - see 19-20");
			}
		}else{
			return array("error",array("invalid parameters:"=>array($doctitle,$doccontents,$docdate,$docyn,$doccateid,$doclistid,$doctypeid,$docfile)));
		} 
	}
	
	public function addActionDocWithMeta($actName,$actTitle,$actID,$docfile,$tmpname,$metaData,$update=false){
		$step = 1;
		echo("<P>$step. Getting document details...");  $step++;	
		$cmpcode = $this->getCmpCode();
		$modref = $this->getModRef();	
		//Checking file name is set, and title of Task is set	
		if(strlen($actTitle)>0 && strlen($docfile) > 0){
			if(strlen($metaData['doctitle'])>0){
				$doctitle = $metaData['doctitle'];
			}else{
				//Cutting the filename if it exceeds 26 chars - will be inserted into contents section	
				$doctitle = "Attachment from ".ucfirst($actName)." ".$actID." - ";	
	            if(strlen($actTitle) > 50){
	            	$doctitle .= preg_replace('/\s+?(\S+)?$/', '', substr($actTitle, 0, 26)) . "...";
	            }else{
	            	$doctitle .= $actTitle;
	            }
			}
        	$doctitle = mysqli_real_escape_string($this->getConn(),$doctitle);
				
			$doccontentsPrefix = "Attachment from ".ucfirst($actName)." $actID - '$docfile'";
			if(strlen($metaData['doccontents'])>0){
				$doccontents = $metaData['doccontents']." [".stripslashes(mysqli_real_escape_string($this->getConn(),$doccontentsPrefix))."]";
			}else{
	        	$doccontents = stripslashes(mysqli_real_escape_string($this->getConn(),$doccontentsPrefix));
			}
        	$doccontents = mysqli_real_escape_string($this->getConn(),$doccontents);
			
			if(strlen($metaData['doccateid'])>0 && $metaData['doccateid'] != "X"){
				$doccateid = $metaData['doccateid'];
			}else{
	        	//Checking if category (folder) exists - if not, creating one (for all attachments in this module)
				$catCheck = $this->checkCategory($actName);
				$doccateid = $catCheck > 0 ? $catCheck : $this->addCategory($actName);
				if(!$doccateid > 0){
					return array("error","SQL insert failed upon category creation! DOC/class/doc.php - see 45");
				}
			}
			
			$docyn = "T";	

			//Setting defaults (List '1' and Type 'other')
			if(strlen($metaData['doctypeid'])>0){
				$doctypeid = $metaData['doctypeid'];
			}else{
				$doctypeid = 7;
			}
			
			if(strlen($metaData['doclistid'])>0){
				$doclistid = $metaData['doclistid'];
			}else{
				$doclistid = 1;	
			}

			if(strlen($metaData['docdate'])>0){
				$docdate = strtotime($metaData['docdate']);
			}else{
				$docdate = time();	
			}
			//fill in today's date (as a timestamp) and current user
			$docadddate = time();
    		$docadduser = $this->getUserID();
			echo("Done.</p><p>$step. Creating temporary document..."); $step++;
			//sql load
      		$sql = "INSERT INTO assist_".$cmpcode."_".$this->tablename."_content (doctitle, doccontent, doccateid, docdate, doclocation, docfilename, doctypeid, docyn, docadduser, docadddate, docmoduser, docmoddate, doclistid, docmodref, docactionlink) VALUES ";
        	$sql.= "('$doctitle','$doccontents',$doccateid,$docdate,'','$docfile',$doctypeid,'$docyn','$docadduser',$docadddate,'$docadduser',$docadddate, $doclistid,'".$modref."', $actID)";
        	//echo $sql;
        	$docID = $this->db_insert($sql);
			if($this->checkIntRef($docID)){
				//Store the file in an appropriate location
				//Location: /files/$cmpcode/$moduledb/docdate(yyyy)/docdate(mm)/docid_docdate(yyyymmdd)_today(yyyymmddhhiiss).ext
		        $doc = explode(".",$docfile);
		        $docc = count($doc)-1;
		        $docext = $doc[$docc];
		        $path = "".$this->tablename."/".date("Y",$docadddate)."/".date("m",$docadddate);
				$this->checkFolder($path);
		        $doclocation = "/files/".$cmpcode."/".$path."/".$docID."_".date("Ymd",$docadddate)."_".date("YmdHis",$docadddate).".".$docext;
				echo("Done.</p> <p>$step. Uploading document..."); $step++;
		        //upload document
		        /*$chkloc = "../files/".$cmpcode;
		        if(!is_dir($chkloc)) { mkdir($chkloc); }
		        $chkloc = "../files/".$cmpcode."/".strtoupper($this->tablename);
		        if(!is_dir($chkloc)) { mkdir($chkloc); }
		        $chkloc = "../files/".$cmpcode."/".strtoupper($this->tablename)."/".date("Y",$docadddate);
		        if(!is_dir($chkloc)) { mkdir($chkloc); }
		        $chkloc = "../files/".$cmpcode."/".strtoupper($this->tablename)."/".date("Y",$docadddate)."/".date("m",$docadddate);
		        if(!is_dir($chkloc)) { mkdir($chkloc); }*/
		        
		        copy($tmpname, "..".$doclocation);
				echo("Done.</p> <p>$step. Updating document..."); $step++;
				//Update the database with the new location of the file
				$yn = $update ? "U" : "Y";
		        $sql = "UPDATE assist_".$cmpcode."_".$this->tablename."_content SET doclocation = '$doclocation', docyn = '$yn' WHERE docid = $docID";
				$result1 = $this->db_update($sql);
				if($this->checkIntRef($result1)){
					echo("Done.</p>");
					//echo "<br>Insert ID: $docID";
					return array("ok",$docID);
				}else{
					echo("FAILED. Please contact your Assist Administrator, and try to show them this screen!</p>");	
					return array("error","SQL insert failed upon document location update! DOC/class/doc.php - see 90");
				}
			}else{
				return array("error","SQL insert failed! DOC/class/doc.php - see 19-20");
			}
		}else{
			return array("error",array("invalid parameters:"=>array($doctitle,$doccontents,$docdate,$docyn,$doccateid,$doclistid,$doctypeid,$docfile,$metaData)));
		} 
	}
	
	public function deleteActionDoc($id,$modref){
		$step = 1;
		$cmpcode = $this->getCmpCode();
		//echo("<p>$step. Updating table 'assist_".$cmpcode."_".strtolower($modref)."_content'... ");  $step++;	
		
		$sql = "UPDATE assist_".$cmpcode."_".strtolower($modref)."_content SET docyn = 'D' WHERE docid = $id";
		$result = $this->db_update($sql);
		if($this->checkIntRef($result)){
			//echo("Done ($result).</p><p>$step. Getting associated task... ");  $step++;
			$sql = "SELECT doclocation, docactionlink, docfilename FROM assist_".$cmpcode."_".strtolower($modref)."_content WHERE docid = '$id'";
			$result = $this->mysql_fetch_one($sql);
			if(isset($result['docactionlink'])){
				//echo("Done.</p><p>$step. Inserting a log... ");  $step++;
				$actionID = $result['docactionlink'];
				$filename = $result['docfilename'];
				
				//Get the corresponding Task's details
				$action = new ACTION();
				$task = $action->getAction($actionID);
				$logdate = time();
				$modTexts = $this->getDocInstancesForSelect();
				$username = $this->getUserName();
				$sql = "INSERT INTO ".$this->getDBRef()."_log 
						(logid, logdate, logtkid, logupdate, logstatusid, logstate, logactdate, logemail, logsubmittkid, logtaskid, logtasktkid, logtype) 
						VALUES 
						(null, $logdate, '".$this->getUserID()."', '<i>$username</i> deleted attachment \"".$filename."\" (stored in the ".$modTexts[$modref]." module.)', ".$task['taskstatusid'].", ".$task['taskstate'].", $logdate, 'N', '".$this->getUserID()."', ".$actionID.", '', 'E')";
				$logResult = $this->db_insert($sql);
				
				//Returning result of log addition			
				if($this->checkIntRef($logResult)){
					//echo("Done.</p>");
					//echo "<br>Insert ID: $logResult";
					return true;
				}else{
					echo "FAILED!</p><br>'$sql'";
					return;
				}
					
			}else{
				echo "FAILED!</p><br>'$sql'";
				return;
			}
		}else{
			echo "FAILED!</p><br>'$sql'";
			return;
		}
		return $sql;
// 			
		// //sql extract current location
		// $sql = "SELECT doclocation, docactionlink, docfilename FROM assist_".$cmpcode."_".strtolower($modref)."_content WHERE docid = '$id'";
		// $result = $this->mysql_fetch_one($sql);
		// $actionID = $result['docactionlink'];
		// $filename = $result['docfilename'];
		// $result = $result['doclocation'];
		// $old = "../..".$result;
		// echo("Done ($result).</p><p>$step. Processing new location... ");  $step++;
// 		
		// //remove last element - containing date, original name
		// $result = explode("/",$result,6);
		// //echo "<br>".$old."<br>";
		// $final = array_pop($result);
		// array_shift($result);
		// array_push($result,"deleted",$final);
		// $result = "/".implode("/",$result);
		// $oldResult = explode("/",$result,-1);
		// $oldResult = $_SERVER['DOCUMENT_ROOT']."/".implode("/",$oldResult)."/";
		// echo "<br>new delete location: ".$oldResult."<br>";
		// return;
		// //$this->checkDeleteLocation($oldResult);
		// //$this->listServerVars();
		// echo $this->createPath($oldResult)."<br>";
		// ////SSIST_HELPER::arrPrint($oldResult);
		// echo("Done ($result).</p><p>$step. Updating database... ");  $step++;
// 		
		// //sql update
  		// $sql = "UPDATE assist_".$cmpcode."_".$this->tablename."_content SET ";
    	// $sql.= "docyn = 'N', ";
    	// $sql.= "doclocation = '$result' ";
    	// $sql.= "WHERE docid = '$id'";
		// //echo "<br>".$sql."<br>";
		// //echo("Done.</p>");
		// //return;
    	// $docID = $this->db_update($sql);
//     	
    	// //Checking database success, moving file to deleted folder
		// if($this->checkIntRef($docID)){
			// echo("Done.</p><p>$step. Moving file... ");  $step++;
			// $result = "../..".$result;
			// echo("<br>These are the file paths:<br>$old<br>$result<br>");
			// if(file_exists($old)) {
				// $this->recurse_copy($old,$result);
				// if(file_exists($result)) {
					// unlink($old);
				// }else{
					// echo("FAILED. Copy to deleted folder (".$result.") failed. Thing doesn't exist.</p>");
					// return array("error","File copy (delete) failed");
				// }
			// }else{
				// echo("FAILED. Old file not found at (".$old.")</p>");
				// return array("error","Old file not found");
			// }	
			// echo("Done.</p><p>$step. Saving log ...");  $step++;
// 			
			// //Get the corresponding Task's details
			// $action = new ACTION();
			// $task = $action->getAction($actionID);
			// $logdate = time();
			// $sql = "INSERT INTO ".$dbref."_log 
					// (logid, logdate, logtkid, logupdate, logstatusid, logstate, logactdate, logemail, logsubmittkid, logtaskid, logtasktkid, logtype) 
					// VALUES 
					// (null, $logdate, '".$this->getUserID()."', 'Deleted attachment: ".$filename." from Document Management System', ".$task['taskstatusid'].", ".$task['taskstate'].", $logdate, 'N', '".$this->getUserID()."', ".$actionID.", '', 'E')";
			// $logResult = $this->db_insert($sql);
// 			
			// //Returning result of log addition			
			// if($this->checkIntRef($logResult)){
				// echo("Done.</p>");
				// echo "<br>Insert ID: $logResult";
				// return array("ok",$logResult);
			// }else{
				// echo("FAILED.</p>");	
				// return array("error","SQL insert failed upon Log Entry! DOC/class/doc.php - see 166");
			// }
		// }else{
			// echo("FAILED.</p>");
			// return array("error","SQL update failed! DOC/class/doc.php - see 136");
		// }
	}
	
	
	public function checkCategory($title){
		$sql = "SELECT cateid FROM assist_".$this->getCmpCode()."_".$this->tablename."_categories WHERE LOWER(catetitle) = '".strtolower($title)."'";
		//return $sql;
		$result = $this->mysql_fetch_one_value($sql, "cateid");
		//return $result;
		if($this->checkIntRef($result) && $result > 0){
			return $result;
		}else{
			return false;
		}
	}
	
	public function addCategory($title){
		$sql = "INSERT INTO assist_".$this->getCmpCode()."_".$this->tablename."_categories SET 
		catetitle = '".ucwords($title)."',
		cateyn = 'Y',
		catesubid = 0,
		cateowner = '".$this->getUserID()."'";
		$r = $this->db_insert($sql);
		if($this->checkIntRef($r)){
			return $r;
		}else{
			return false;			
		}
	}
	
	public function checkDocsForAction(){
		$sql = "SELECT docactionlink, count(docid) FROM assist_".$this->getCmpCode()."_".$this->tablename."_content WHERE docactionlink > 0 AND docyn = 'Y' GROUP BY docactionlink";
		$result = $this->mysql_fetch_all_fld($sql, "docactionlink");
		return $result;
	}
	
	public function checkAllDocsForAction($formatted=false){
		$activeModules = $this->getDocInstancesModRefs();
		////SSIST_HELPER::arrPrint($activeModules);
		if(count($activeModules)>0){
		$cmp = $this->getCmpCode();
			$total = array();
			foreach($activeModules as $val){
				$sql = "SELECT docactionlink as taskid, count(docid) as 'count(id)' FROM assist_".$cmp."_".strtolower($val)."_content WHERE docactionlink > 0 AND docyn = 'Y' GROUP BY docactionlink";
				$result = $this->mysql_fetch_all_fld($sql, "taskid");
				if(!$formatted){
					$total[$val]=$result;					
				}else{
					$total = $total + $result;
				}
			}
		}
		return $total;
	}
	
	public function checkDeleteLocation($chkloc=""){
		if(strlen($chkloc) <= 0){
			$cmpcode = $this->getCmpCode();
			$modref = $this->getModRef();	
			$chkloc = "../../files/".$cmpcode."/".strtoupper($this->tablename)."/deleted/";			
		}	
        if(!is_dir($chkloc)) { mkdir($chkloc,null,true); }
	}
	
	public function listServerVars(){
		$indicesServer = array('PHP_SELF', 
		'argv', 
		'argc', 
		'GATEWAY_INTERFACE', 
		'SERVER_ADDR', 
		'SERVER_NAME', 
		'SERVER_SOFTWARE', 
		'SERVER_PROTOCOL', 
		'REQUEST_METHOD', 
		'REQUEST_TIME', 
		'REQUEST_TIME_FLOAT', 
		'QUERY_STRING', 
		'DOCUMENT_ROOT', 
		'HTTP_ACCEPT', 
		'HTTP_ACCEPT_CHARSET', 
		'HTTP_ACCEPT_ENCODING', 
		'HTTP_ACCEPT_LANGUAGE', 
		'HTTP_CONNECTION', 
		'HTTP_HOST', 
		'HTTP_REFERER', 
		'HTTP_USER_AGENT', 
		'HTTPS', 
		'REMOTE_ADDR', 
		'REMOTE_HOST', 
		'REMOTE_PORT', 
		'REMOTE_USER', 
		'REDIRECT_REMOTE_USER', 
		'SCRIPT_FILENAME', 
		'SERVER_ADMIN', 
		'SERVER_PORT', 
		'SERVER_SIGNATURE', 
		'PATH_TRANSLATED', 
		'SCRIPT_NAME', 
		'REQUEST_URI', 
		'PHP_AUTH_DIGEST', 
		'PHP_AUTH_USER', 
		'PHP_AUTH_PW', 
		'AUTH_TYPE', 
		'PATH_INFO', 
		'ORIG_PATH_INFO') ; 
		
		echo '<table cellpadding="10">' ; 
		foreach ($indicesServer as $arg) { 
		    if (isset($_SERVER[$arg])) { 
		        echo '<tr><td>'.$arg.'</td><td>' . $_SERVER[$arg] . '</td></tr>' ; 
		    } 
		    else { 
		        echo '<tr><td>'.$arg.'</td><td>-</td></tr>' ; 
		    } 
		} 
		echo '</table>' ; 
	}
	
	/** 
	 * recursively create a long directory path
	 */
	public function createPath($path) {
	    if (is_dir($path)) return true;
	    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
	    $return = $this->createPath($prev_path);
	    return ($return && is_writable($prev_path)) ? mkdir($path,0775,true) : false;
	}
	
	public function getDocsForAction($taskid){
		$activeModules = $this->getDocInstancesModRefs();
		//echo "We have ".count($activeModules)." active";
		////SSIST_HELPER::arrPrint($activeModules);
		if(count($activeModules)>0){
			$total = array();	
			foreach($activeModules as $val){				
				$sql = "SELECT docid as id, docactionlink as taskid, docfilename as original_filename, doclocation as system_filename, docmodref as file_location, docyn FROM assist_".$this->getCmpCode()."_".strtolower($val)."_content WHERE docactionlink = $taskid AND (docyn = 'Y' || docyn = 'N')";
				//$sql = "SELECT d.docid as id, d.docactionlink as taskid, d.docfilename as original_filename, d.doclocation as system_filename, d.docmodref as file_location, m.modtext FROM assist_".$this->getCmpCode()."_".strtolower($val)."_content d INNER JOIN assist_menu_modules m ON d.docmodref = m.modref WHERE docactionlink = $taskid AND docyn = 'Y'";
				$result = $this->mysql_fetch_all($sql);
				if(count($result)>0){
					$sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '$val'";
					$result2 = $this->mysql_fetch_one_value($sql, "modtext");
					foreach($result as $k=>$r){
						$result[$k]['storage_name']=$result2;							
						$result[$k]['storage_module']=$val;						
					}
				}
				$total = array_merge($total,$result);
			}	
			return $total;
		}else{
			return array();
		}
	}

	function stripslash(&$key,$value){
		if($key == "doccontent"){
			$value = stripslashes($value);
		}
	}
	
	public function getHistoryDocsForAction($taskid,$logTableName){
		$activeModules = $this->getDocInstancesModRefs();
		if(count($activeModules)>0){
			$total = array();	
			foreach($activeModules as $val){				
				//echo "<br>";
				//echo $val . "<br>";
				//echo $logTableName . "<br>";
				$sql = "SELECT docid as id, 
						docactionlink as taskid, 
						docfilename as original_filename, 
						doclocation as system_filename, 
						docmodref as file_location, 
						docmoddate, 
						docadddate, 
						docyn, 
						l.logdate, 
						l.logid 
						FROM assist_".$this->getCmpCode()."_".strtolower($val)."_content a ";
				$sql .= "LEFT JOIN ".$logTableName." l 
						ON l.logtaskid = a.docactionlink 
						WHERE (a.docyn = 'U' OR a.docyn = 'O') 
						AND (ABS(a.docadddate - l.logdate) < 100) 
						AND a.docactionlink = '".$taskid."'
				";					
				//echo $sql;
				$result = $this->mysql_fetch_all_by_id2($sql, "logid", "id");
				if(count($result)>0){
					$sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '$val'";
					$result2 = $this->mysql_fetch_one_value($sql, "modtext");
					foreach($result as $lg => $did){
						foreach($did as $key => $v){
							$result[$lg][$key]['storage_name']=$result2;							
							$result[$lg][$key]['storage_module']=$val;														
						}
					}
				}
				////SSIST_HELPER::arrPrint($result);
				if(count($total)==0){
					$total = $result;
				}else{						
					if(count($result)>0){
						foreach($result as $lg => $did){
							if(isset($total[$lg])){
								foreach($did as $docid=>$data){
									if(!isset($total[$lg][$docid])){
										$total[$lg][$docid] = $data;										
									}else{
										//double of same ID in different doc modules!
										$total[$lg][$docid.$data['system_filename']] = $data;
									}	
								}
							}else{
								$total[$lg] = $did;
							}
						}
					}
				}
			}
			//echo "<br>TOTAL array<br>";	
			////SSIST_HELPER::arrPrint($total);
			return $total;
		}else{
			return array();
		}
		// $result = $this->mysql_fetch_all($sql);
		// return $result;
// 		
		// //$sql = "SELECT d.docid as id, d.docactionlink as taskid, d.docfilename as original_filename, d.doclocation as system_filename, d.docmodref as file_location, m.modtext FROM assist_".$this->getCmpCode()."_".strtolower($val)."_content d INNER JOIN assist_menu_modules m ON d.docmodref = m.modref WHERE docactionlink = $taskid AND docyn = 'Y'";
		// if(count($result)>0){
			// $sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '$val'";
			// $result2 = $this->mysql_fetch_one_value($sql, "modtext");
			// foreach($result as $k=>$r){
				// $result[$k]['storage_name']=$result2;							
				// $result[$k]['storage_module']=$val;						
			// }
		// }
		// $total = array_merge($total,$result);
		// return $total;
	}
		
	public function getDocLocationForAction($docid){
		$sql = "SELECT docid, doclocation, docfilename as original_filename FROM assist_".$this->getCmpCode()."_".$this->tablename."_content WHERE docid = $docid AND docyn = 'Y'";
		$result = $this->mysql_fetch_one($sql);
		return $result;
	}
	
	//All credit to gimmicklessgpt@gmail.com - via php.net manual on copying
	public function recurse_copy($src,$dst) { 
	    $dir = opendir($src);
		$counter = 0; 
	    @mkdir($dst); 
	    while(false !== ( $file = readdir($dir)) && $counter < 256) {
	        $counter ++;	 
	        if (( $file != '.' ) && ( $file != '..' )) { 
	            if ( is_dir($src . '/' . $file) ) { 
	                $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	            else { 
	                copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	        } 
	    } 
	    closedir($dir); 
	} 
	
	public function getDocInstances(){
		$sql = "SELECT * FROM assist_menu_modules WHERE modlocation = 'DOC' AND modyn = 'Y'";
		$result = $this->mysql_fetch_all($sql);
		return $result;			
		// if(count($result)>1){
		// }
		// else{
			// return array($result[0]['modref']=>$result[0]['modtext']);
		// }
	}
	
	public function getDocInstancesForSelect(){
		$inst = $this->getDocInstances();
		if(count($inst)>0){
			foreach($inst as $val){
				$list[$val['modref']]=$val['modtext'];
			}
			return $list;
		}else{
			return array();
		}
	}
	
	public function getDocInstancesModRefs(){
		$sql = "SELECT modref FROM assist_menu_modules WHERE modlocation = 'DOC'";
		//$result = $this->mysql_fetch_all_by_id($sql, "modref");
		$result = $this->mysql_fetch_all_by_value($sql, "modref");
		return $result;
	}
	
	public function getSetup($item){
		if($item == "catedisplay"){
			$sql = "SELECT * FROM assist_".$this->getCmpCode()."_setup WHERE ref = '".$this->tablename."' AND refid = 1";
			$res = $this->mysql_fetch_one($sql);
			//if(strlen($res['value'])>0) {
			if(count($res)>0) {
				$catedisplay = $res['value'];
			}else{
				$catedisplay = 20;
			}
			return $catedisplay;
		}else{
			return array("error","you fed in ".$item);
		}
	}
	
	public function getTypes(){
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_".$this->tablename."_list_type WHERE yn = 'Y'";
        $type = $this->mysql_fetch_all($sql);
		if(count($type)>0){
			return $type;
		}else{
			//Make a default
			$sql = "INSERT INTO assist_".$this->getCmpCode()."_".$this->tablename."_list_type SET yn = 'Y', value = 'Default Type'";
			$id = $this->db_insert($sql);
			if($this->checkIntRef($id)){
				$sql = "SELECT * FROM assist_".$this->getCmpCode()."_".$this->tablename."_list_type WHERE yn = 'Y'";
        		$type = $this->mysql_fetch_all($sql);
				return $type;
			}else{
				return array("error"=>"failed to make default");
			}
		}
	}
	
	function subFunction($sub,$level,$title,$access,$tkid,$my_access,$catid,$categories) {
		////SSIST_HELPER::arrPrint(array($sub,$level,$title,$access,$tkid,$my_access,$catid,$categories));	
		////SSIST_HELPER::arrPrint($sub);	
		$catedisplay = $this->getSetup("catedisplay");	
		$echo = "";
		foreach($sub as $c) {
			if($access['docadmin']=="Y" || $c['cateowner']==$tkid || isset($my_access[$c['cateid']])) {
				$text = (strlen($title)>0 ? $title." >> " : "").$c['catetitle'];
				$echo.= "<option ".($c['cateid']==$catid ? "selected" : "")." value=".$c['cateid'].">".$text."</option>";
			}
			$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
			if(count($sub)>0) {
				$cut = $catedisplay;
				$sub_title = $title.(strlen($title)>0 ? " >> " : "").substr($this->decode($c['catetitle']),0,$cut).(strlen($this->decode($c['catetitle']))>$cut ? "..." : "");
				$echo.= $this->subFunction($sub,$level+1,$sub_title,$access,$tkid,$my_access,$catid,$categories);
			}
		}
		return $echo;
	}

	public function getDocForm($data){
		$sqlC = 0;	
		//cats
		$catid = 'X';
		$categories = array();
		$cate_count = 0;
		$cate_owner = array();
		$tkid = $_SESSION['tid'];
		$dbref = "assist_".$this->getCmpCode()."_".$this->tablename;
		//Cats
		$sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
		$seeql = $sql;
		$res = $this->mysql_fetch_all($sql);
		foreach($res as $row) {
			$categories[$row['catesubid']][] = $row;
			$cate_count++;
			if($row['cateowner']==$tkid) {
				$cate_owner[$row['cateid']] = "Y";
			}
		}	
		//User Access
		$sql2 = "SELECT * FROM ".$dbref."_list_users WHERE tkid = '$tkid' AND yn = 'Y' ORDER BY id DESC LIMIT 1";
		//echo $sqlC.". ->  ".$sql2."  <- <br>"; $sqlC++;
		$access = $this->mysql_fetch_one($sql2);
		$my_access = array();
		if($access['docadmin']!="Y") {
			$sql = "SELECT * FROM ".$dbref."_categories_users 
					INNER JOIN ".$dbref."_categories
					  ON cucateid = cateid AND cateyn = 'Y'
					WHERE cuyn = 'Y' AND cutkid = '$tkid' AND (cutype = 'UP' OR cutype = 'MAIN')";
			//echo $sqlC.". ->  ".$sql."  <- <br>"; $sqlC++;
			$res = $this->mysql_fetch_all($sql);
			foreach($res as $row) {
				$my_access[$row['cucateid']] = "Y";
				if(isset($cate_owner[$row['cucateid']])) { unset($cate_owner[$row['cucateid']]); }
			}
			if(count($cate_owner)>0) {
				foreach($cate_owner as $i => $y) {
					$my_access[$i] = $y;
				}
			}
			$total_cate = count($my_access);
		} else {
			$total_cate = $cate_count;
		}
		//HTML form
		$xtra = "";
		//$xtra = $sqlC.". ->  ".$seeql."  <- <br>"; $sqlC++;
		$echo = "<form><table id=tbl_upload class=form><tbody>
				    <tr>
			        	<th>Document Title:*</th>
				        <td>".$xtra."<input class='i_am_required' type=text name=doctitle size=50 maxlength=200></td>
				    </tr>
				    <tr>
				        <th>Document Contents:</th>
				        <td><textarea name=doccontents rows=3 cols=40></textarea></td>
				    </tr>
				    <tr>
				        <th>Category:</th>
				        <td><select name=doccateid>            
							<option selected value=X>--- SELECT ---</option>";
							$echo.= $this->subFunction($categories[0],0,'',$access,$tkid,$my_access,$catid,$categories);
		$echo.= "					
						</select></td>
				    </tr>
				    <tr>
				        <th>Document Type:*</th>
				        <td><select class='i_am_required' name=doclistid>
							<option value=0 selected>Unspecified</option>";
							//$sql = 'SELECT * FROM assist_'.$this->getCmpCode().'_'.$this->tablename.'_list_type WHERE yn = "Y" ORDER BY value';
							//$res = $this->mysql_fetch_all($sql);
							$res = $this->getTypes();
							foreach($res as $row) {
								$echo.= "<option value=".$row['id'].">".$row['value']."</option>";
							}
		$eecho = '';
		//$eecho = $sqlC.'. ->  '.$sql.'  <- <br>'; $sqlC++;
		$echo.= "
						</select></td>
				    </tr>
				    <tr>
				        <th>Document Date:*</th>
				        ".$eecho."
				        <td><input type=text name=docdate readonly=readonly class='jdate2012 i_am_required' value=".date('d-M-Y')."></td>
				    </tr>
				    <tr>
				        <th>File Name:</th>
				        <td>".$data['fl']."</td>
				    </tr>
				    <tr>
				        <th>Document Format:</th>
				        <td><select name=doctypeid>
				            <option selected value=X>--- SELECT ---</option>";
				            $sql = "SELECT * FROM assist_".$this->getCmpCode()."_".$this->tablename."_list_doctype WHERE yn = 'Y'";
				            //echo $sqlC.". ->  ".$sql."  <- <br>"; $sqlC++;
				            $doctype = $this->mysql_fetch_all($sql);
			                foreach($doctype as $row) {
			                    $id = $row['id'];
			                    $val = $row['value'];
			                    $sel = $val == "Other" ? " selected " : "";
			                    $echo.= "<option $sel value=$id> $val </option>";
			                }
		$echo.= "
				        </select></td>
				    </tr>
				</tbody></table></form>
				<br>
				*<em> this is required information</em>";
		return $echo;
	}
}



?>