<?php
include 'inc_head.php';

$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$limit = 20;	//to get from profile
$displayed = $limit;
$eliminated = $start;
$needle = isset($_REQUEST['needle']) ? $_REQUEST['needle'] : "";

function subFunction($sub,$level,$title,&$eliminated,&$displayed) {
	global $categories;
	global $access;
	global $my_access;
	global $tkid;
	global $needle;
	global $catedisplay,$helper;
	$catedisplay = !isset($catedisplay) || !$helper->checkIntRef($catedisplay) ? 20 : $catedisplay;
	//$echo = "<P>START FUNCTION - $title: $start :: $displayed";
    $echo = "";
if($displayed>0) {
	foreach($sub as $c) {
		$c['catetitle'] = $helper->decode($c['catetitle']);
		if($displayed>0 && ($access['docadmin']=="Y" || $c['cateowner']==$tkid || isset($my_access[$c['cateid']]))) {
			if($eliminated>0) {
				$eliminated--;
			} else {
				/*$echo.= "<span style=\"".
				($access['docadmin']=="Y" ? " font-weight: bold;" :"").
				($c['cateowner']==$tkid ? " text-decoration: underline;" :"").
				(isset($my_access[$c['cateid']]) ? " color: #009900;" :"")."\">";
				//$echo.= "LINK!! ";*/
				$text = (strlen($title)>0 ? $title." >> " : "").$c['catetitle'];
				$chk = true;
				if(isset($_REQUEST['needle']) && strlen($needle)>0) {
					$arrNeedle = explode(" ",$needle);
					foreach($arrNeedle as $a) {
						if(strstr(strtolower($helper->decode($text)),strtolower($helper->decode($a)))===false) {
							$chk = false;
							break;
						}
					}
				}
				if($chk) {
					$echo .= chr(10)."<tr>
								<th class=th".$level."a>";
					$echo.= $c['cateid'];
					$echo.= "</th><td>";
					$echo.= $text; //."</span></p>";
					$echo.= "
						<td class=center>".(isset($c['owner']) && strlen($c['owner'])>0 ? $c['owner'] : "<span class=idelete>Unknown</span>")."</td>
						<td class=center><input type=button value=\"Category\" class=".$c['cateid']." /> <input type=button value=\"Content\" class=".$c['cateid']." /></td>
						</tr>";
					$displayed--;
					unset($my_access[$c['cateid']]);
				}
			}
		}
		//$echo .="<P>".$c['cateid']." :: ".$eliminated." :: ".$displayed;
		if($displayed > 0) {
			$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
			if(count($sub)>0) {
				$nexttitle = (strlen($title)>0 ? $title." >> " : "").(strlen($c['catetitle'])>$catedisplay ? substr($c['catetitle'],0,$catedisplay)."..." : $c['catetitle']);
				$echo.=subFunction($sub,$level+1,$nexttitle,$eliminated,$displayed);
			}
		} else {
			break;
		}
	}
}
	return $echo;
}

/** GET CATEGORIES **/
//CHECK FOR SUBS WITH DELETED PARENTS
$sql = "SELECT c.cateid FROM `".$dbref."_categories` c
		INNER JOIN ".$dbref."_categories p
		ON p.cateid = c.catesubid AND p.cateyn = 'N'
		WHERE c.cateyn = 'Y'";
$deleted_childs = $helper->mysql_fetch_fld_one($sql,"cateid");


$categories = array();
$cate_count = 0;
$cate_owner = array();

$sql = "SELECT c.*, CONCAT_WS(' ',tk.tkname,tk.tksurname) as owner FROM ".$dbref."_categories c
		LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
		ON tk.tkid = c.cateowner AND tk.tkstatus = 1
		WHERE c.cateyn = 'Y' ORDER BY c.catetitle";
//$rs = getRS($sql);
$rows = $helper->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
	if(!in_array($row['cateid'],$deleted_childs)) {
		$categories[$row['catesubid']][] = $row;
		$cate_count++;
		if($row['cateowner']==$tkid) {
			$cate_owner[$row['cateid']] = "Y";
		}
	}
}
//mysql_close();

/** USER ACCESS **/
$my_access = array();
if($access['docadmin']!="Y") {
	$sql = "SELECT * FROM ".$dbref."_categories_users 
			INNER JOIN ".$dbref."_categories
			  ON cucateid = cateid AND cateyn = 'Y'
			WHERE cuyn = 'Y' AND cutkid = '$tkid' AND cutype = 'MAIN'";
	//$rs = getRS($sql);
    $rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		$my_access[$row['cucateid']] = "Y";
		if(isset($cate_owner[$row['cucateid']])) { unset($cate_owner[$row['cucateid']]); }
	}
	//mysql_close();
	if(count($cate_owner)>0) {
		foreach($cate_owner as $i => $y) {
			$my_access[$i] = $y;
		}
	}
	$total_cate = count($my_access);
} else {
	$total_cate = $cate_count;
}
if(strlen($needle)>0) { $limit = $total_cate; $displayed = $limit; }
?>
<style type=text/css>
#tbl_page td, #tbl_page input { font-size: 7pt; }
.th0a { background-color: #000099; }
.th1a { background-color: #2222aa; }
.th2a { background-color: #4444bb; }
.th3a { background-color: #6666cc; }
.th4a { background-color: #9999dd; }
.th5a { background-color: #bbbbee; } /*color: #000000; }*/
.th6a { background-color: #ccccff; } /*color: #000000; }*/
</style>
<a name=top></a><h1><a href=admin.php class=breadcrumb >Admin</a> >> Maintain</h1>
<?php
$helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
if(strlen($needle)>0) {
	echo "<h2>Searching for: \"".$needle."\"</h2>";
}
?>
<table width=100% id=tbl_page style="margin-bottom: 10px;">
	<tr><?php 
	$end = ($total_cate%$limit)>0 ? $total_cate-($total_cate%$limit) : $total_cate-$limit;
	echo "
		<td class=center><span style=\"float: left;\" id=spn_page>
			<input type=button id=begin value=\"|<\" s=0 ".($start==0 ? "disabled=disabled" : "")." />
			<input type=button id=prev value=\"<\" s=".($start-$limit)." ".($start==0 ? "disabled=disabled" : "")." />
			Page ".(ceil($start/$limit)+1)." of ".ceil($total_cate/$limit)."
			<input type=button id=next value=\">\" s=".($start+$limit)." ".($start+$limit<$total_cate ? "" : "disabled=disabled")." />
			<input type=button id=end value=\">|\" s=".($end)." ".($start+$limit<$total_cate ? "" : "disabled=disabled")." />
		</span>
		".(strlen($needle)>0 ? "<input type=button value=\"Clear search\" id=clear />" : "")."
		<!-- <span style=\"float: right;\" id=spn_search><input type=text  /> <input type=button value=Search /></span> -->
		</td>";
	?> </tr>
</table>
<table width=100% id=tbl_cate>
	<tr>
		<th width=40>Ref</th>
		<th >Category</th>
		<th >Category&nbsp;Owner</th>
		<th width=150>Edit</th>
	</tr>
<?php
$level = 0;
echo subFunction($categories[$level],$level,"",$eliminated,$displayed);
?>
</table>
<?php /*echo "<P>";
if($start>0) {
	echo "<a href=admin_maintain.php?start=".($start-$limit).">";
}
echo "<-- Back</a>  Page ".(ceil($start/$limit)+1)." of ".ceil($total_cate/$limit)."  ";
if($start+$limit<$total_cate) {
	echo " <a href=admin_maintain.php?start=".($start+$limit).">";
}
echo "Next --></a>";*/
?>
<script type=text/javascript>
	$(document).ready(function() {
		$("table th").addClass("center");
		$("table td").addClass("middle");
		var needle = "<?php echo $needle; ?>";
		$("#tbl_cate input:button").click(function() {
			var id = $(this).attr("class");
			switch($(this).attr("value")) {
			case "Category":
				var url = "admin_maintain_cate.php?c="+id;
				break;
			case "Content":
				var url = "admin_maintain_doc.php?c="+id;
				break;
			default:
				var url = "error";
			}
			if(url=="error") {
				alert("An error occurred determining your requested action.\n\nPlease reload the page and try again.");
			} else {
				document.location.href = url;
			}
		});
		$("#tbl_page #spn_page input:button").click(function() {
			url = "admin_maintain.php?start="+$(this).attr("s")+"&needle="+needle;
			document.location.href = url;
		});
		$("#tbl_page #clear").click(function() {
			needle = "";
			$("#tbl_page #begin").trigger("click");
		});
		$("#tbl_page #spn_search input:button").click(function() {
			var needle = $("#tbl_page #spn_search input:text").val();
			url = "admin_maintain.php?start=0&needle="+escape(needle);
			document.location.href = url;
		});
	});
</script>
</body></html>