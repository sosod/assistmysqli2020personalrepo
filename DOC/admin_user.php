<?php
    require 'inc_head.php';
	
	
$action = isset($_REQUEST['act']) ? $_REQUEST['act'] : "LIST";
if(isset($_REQUEST['i']) && strlen($_REQUEST['i'])>=4) {
	$user_id = $_REQUEST['i'];
} else {
	$action = "LIST";
}


	$header_row = "<tr>
				<th>Ref</th>
				<th>Category</th>
				<th width=80>Owner</th>
				<th width=80>View</th>
				<th width=80>Maintain</th>
				<th width=80>Upload/<br />Comment</th>
			</tr>";

	$rows = 0;
			
function subFunction($sub,$level,&$rows) {
	global $categories;
	global $user_id;
	global $header_row;
    $echo = "";
	foreach($sub as $c) {
				$text = "";
				for($i=0;$i<$level;$i++) {
					$text.="&nbsp;&nbsp;&nbsp;";
				}
				if($level > 0) { $text.= "-&nbsp;"; }
				$text.= $c['catetitle'];
				$chk = true;
				if($chk) {
					$echo.= chr(10)."<tr>
								<th>";
					$echo.= $c['cateid'];
					$echo.= "</th><td>";
					$echo.= $text; //."</span></p>";
					$echo.= "</td>
							<td class=center>".($c['cateowner']==$user_id ? "<span class=isubmit>Yes</span>" : "")."</td>
							<td class=center>".($level==0?"<input type=checkbox name=view[] ".($c['view']>0 ? "checked" : "")." value=".$c['cateid']." class=view />":"-")."</td>
							<td class=center><input type=checkbox name=main[] ".($c['main']>0 ? "checked" : "")." value=".$c['cateid']." class=main /></td>
							<td class=center><input type=checkbox name=up[] ".($c['up']>0 ? "checked" : "")." value=".$c['cateid']." class=up /></td>
						</tr>";
					$rows++;
					if($rows==20) {
						$echo.=$header_row;
						$rows = 0;
					}
				}
			$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
			if(count($sub)>0) {
				$echo.=subFunction($sub,$level+1,$rows);
			}
	}
	return $echo;
}





switch($action) {
case "EDIT":
	/** GET USER DETAIL **/
	$sql = "SELECT u . * , CONCAT_WS( ' ', t.tkname, t.tksurname ) as uname
			FROM `".$dbref."_list_users` u
			INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = u.tkid
			AND t.tkstatus =1
			WHERE u.yn = 'Y'
			AND u.tkid = '".$user_id."'
			ORDER BY id DESC";
	$user = $helper->mysql_fetch_one($sql);
	$user['docadmin'] = $user['docadmin']=="Y" ? "Yes" : "No";
	$user['addcate'] = $user['addcate']=="Y" ? "Yes" : "No";
	$user['view'] = $user['view']=="Y" ? "Yes" : "No";
	if($user['delcate']=="A") { $user['delcate'] = "All"; } elseif($user['delcate']=="O") { $user['delcate'] = "Own"; } else { $user['delcate'] = "No"; }
	/** GET CATEGORIES **/
	/*$sql = "SELECT * FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catesubid, catetitle";
	$categories = array();
	$rs = getRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$categories[$catesubid][] = $row;
	}
	mysql_close();
	/** GET CATEGORY ACCESS **/
	$sql = "SELECT c.cateid as cateid
			, c.catetitle as catetitle
			, c.cateowner as cateowner
			, c.catesubid as parent
			, count(v.cutype) as view
			, count(m.cutype) as main
			, count(u.cutype) as up
			FROM ".$dbref."_categories c
			LEFT OUTER JOIN ".$dbref."_categories_users v
			ON v.cucateid = c.cateid AND v.cutkid = '".$user_id."' AND v.cutype = 'VIEW' AND v.cuyn = 'Y'
			LEFT OUTER JOIN ".$dbref."_categories_users m
			ON m.cucateid = c.cateid AND m.cutkid = '".$user_id."' AND m.cutype = 'MAIN' AND m.cuyn = 'Y'
			LEFT OUTER JOIN ".$dbref."_categories_users u
			ON u.cucateid = c.cateid AND u.cutkid = '".$user_id."' AND u.cutype = 'UP' AND u.cuyn = 'Y'
			WHERE cateyn = 'Y' GROUP BY c.cateid ORDER BY catesubid, catetitle";
	$categories = array();
	//$rs = getRS($sql);
    $rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		$categories[$row['parent']][$row['cateid']] = $row;
	}
	//mysql_close();

?>
	<a name=top></a><h1><a href=admin.php class=breadcrumb>Admin</a> >> User Access >> <?php echo $user['uname']; ?></h1>
<?php
	echo "<h2>User Access as per Setup</h2>
		<table id=tbl_setup><thead>
			<tr>
				<th width=25%>Document<br />Admin</th>
				<th width=25%>Create</th>
				<th width=25%>Delete</th>
				<th width=25%>View All</th>
			</tr>
		</thead><tbody>
			<tr>
				<td class=center>".$user['docadmin']."</td>
				<td class=center>".$user['addcate']."</td>
				<td class=center>".$user['delcate']."</td>
				<td class=center>".$user['view']."</td>
			</tr>
		</tbody></table>
		<p><span class=iinform>Please Note:</span><ul>
			<li> If Document Admin is set to Yes, all settings below are ignored and the user automatically has complete access to all categories.</li>
			<li>If View All is set to Yes, all settings in the View columns below are ignored and the user can automatically see all categories on the View page.</li>
			<li>If Delete is set to All then the user can delete any category to which they have Maintain access.</li>
			<li>If Delete is set to Own, then the user can only delete categories where they have been set as the Category Owner.</li></ul></p>
		<form id=frm_edit method=post action=admin_user.php>
		<input type=hidden name=i value=".$user_id." />
		<input type=hidden name=act value=SAVE />
		<h2>User Access per Category</h2>
		<p><span class=iinform>Please Note:</span><ul>
			<li>Where the user is the category owner, they automatically have Maintain access.</li>
			<li>If View is checked, the user automatically receives View access for all sub-categories.</li>
			<li>If Maintain is checked, the user automatically has Upload/Comment access.</li>
			</ul></p>
		<table id=tbl_container><tr><td>
		<table id=tbl_cate>
			<tr>
				<td style=\"border-color: #ffffff;\" colspan=2><img src=/pics/tri_down.gif style=\"vertical-align: middle\" /> <a href=#bottom>Go to bottom</a></td>
				<td style=\"border-color: #ffffff;\">&nbsp;</td>
				<td style=\"border-color: #ffffff; font-size: 6.5pt;\" class=center><span id=view class=\"u color check\" style=\"cursor:hand;\">Check All</span><br /><span id=view class=\"u color uncheck\" style=\"cursor:hand;\">Uncheck All</span></td>
				<td style=\"border-color: #ffffff; font-size: 6.5pt;\" class=center><span id=main class=\"u color check\" style=\"cursor:hand;\">Check All</span><br /><span id=main class=\"u color uncheck\" style=\"cursor:hand;\">Uncheck All</span></td>
				<td style=\"border-color: #ffffff; font-size: 6.5pt;\" class=center><span id=up class=\"u color check\" style=\"cursor:hand;\">Check All</span><br /><span id=up class=\"u color uncheck\" style=\"cursor:hand;\">Uncheck All</span></td>
			</tr>
		".$header_row;
		echo subFunction($categories[0],0,$rows);
	echo "</table>
	</td></tr>
	<tr><td><table width=100% id=tbl_save><tr><td class=center><a name=bottom></a><span style=\"float: left;\"><img src=/pics/tri_up.gif style=\"vertical-align: middle;\" /> <a href=#top>Go to top</a></span><input type=submit value=\"Save Changes\" class=isubmit /> <input type=reset /></td></tr></table>
	</td></tr></table>
	</form>";
	?>

	<script type=text/javascript>
$(document).ready(function() {	 	
	$("#tbl_container, #tbl_container td").addClass("noborder");
	$("#tbl_cate, #tbl_cate td").removeClass("noborder");
	$("#tbl_cate th").addClass("center");
	$("#tbl_save input:reset").click(function() {
		document.location.href = document.location.href;
	});
	$("#tbl_cate .check").click(function() {
		var i = $(this).attr("id");
		$("#tbl_cate ."+i).attr("checked","checked");
	});
	$("#tbl_cate .uncheck").click(function() {
		var i = $(this).attr("id");
		$("#tbl_cate ."+i).attr("checked","");
	});
});
	</script>
	<?php
	break;
	
	
	
	
	
	
	
case "SAVE":
	//arrPrint($_REQUEST);
?>
	<h1><a href=admin.php class=breadcrumb>Admin</a> >> User Access</h1>
<?php
	$helper->JSdisplayResultPrep("");
	echo "<script type=text/javascript>JSdisplayResult('info','info','Processing...');</script>";
	$sql = "UPDATE ".$dbref."_categories_users SET cuyn = 'N' WHERE cutkid = '".$user_id."'";
	//echo $sql;
    $helper->db_update($sql);
	$lsql = $sql;
	$type = array("view","main","up");
	
	foreach($type as $t) {
		$access = $_REQUEST[$t];
		//echo "<h2>".$t."</h2>";arrPrint($access);
		if(count($access)>0) {
			$sql = "UPDATE ".$dbref."_categories_users SET cuyn = 'Y' WHERE cutkid = '".$user_id."' AND cutype = '".strtoupper($t)."' AND cucateid IN (".implode(",",$access).")"; //echo $sql;
			$lsql.= chr(10).$sql;
            $helper->db_update($sql);
			$sql = "SELECT cucateid FROM ".$dbref."_categories_users WHERE cuyn = 'Y' AND cutkid = '".$user_id."' AND cutype = '".strtoupper($t)."'";
			$got = $helper->mysql_fetch_fld_one($sql,"cucateid"); //arrPrint($got);
			$new = array_diff($access,$got); //arrPrint($new);
			if(count($new)>0) { 
				$sql = "INSERT INTO ".$dbref."_categories_users (cucateid, cutkid, cutype, cuyn)
					SELECT cateid, '".$user_id."', '".strtoupper($t)."','Y' FROM ".$dbref."_categories WHERE cateyn = 'Y' AND cateid IN (".implode(",",$new).")"; 
//					echo $sql;
                $helper->db_insert($sql);
				$lsql.=chr(10).$sql;
			}
			
		}
	}
	logAct("Updated user access for user ".$user_id,$lsql,"USER",$user_id);
	echo "<script type=text/javascript>
			JSdisplayResult('ok','ok','Update complete.');
			document.location.href = 'admin_user.php?act=LIST&r[]=ok&r[]=Update complete.';
		</script>";
    $helper->displayGoBack("admin.php","Back to Admin");
	
	
	
	
	
	break;
	
	
	
	
	
	
case "LIST":
default:
	/** GET USER LIST **/
?>
	<h1><a href=admin.php class=breadcrumb>Admin</a> >> User Access</h1>
<?php
    $helper->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
	$users = array();
	$sql = "SELECT u . * , CONCAT_WS( ' ', t.tkname, t.tksurname ) as uname
			FROM `".$dbref."_list_users` u
			INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = u.tkid
			AND t.tkstatus =1
			WHERE u.yn = 'Y'
			ORDER BY uname";
	//$rs = getRS($sql);
    $rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		$users[$row['tkid']] = $row;
	} 
	//mysql_close();

	$cate = array();
	/** CATEGORY OWNERS **/
	$sql = "SELECT cateowner, count(cateid) as cc FROM ".$dbref."_categories WHERE cateyn = 'Y' GROUP BY cateowner";
	$cate['owner'] = $helper->mysql_fetch_fld2_one($sql,"cateowner","cc");
	/** CATEGORY ACCESS **/
	$type = array("VIEW","MAIN","UP");
	foreach($type as $t) {
		$sql = "SELECT cutkid, count(cucateid) as cc FROM ".$dbref."_categories_users INNER JOIN ".$dbref."_categories ON cateid = cucateid AND cateyn = 'Y' WHERE cuyn = 'Y' AND cutype = '$t' GROUP BY cutkid";
		$cate[$t] = $helper->mysql_fetch_fld2_one($sql,"cutkid","cc");
	}

	?>
	<table id=tbl_list><thead>
		<tr>
			<th rowspan=2>User<br />ID</th>
			<th rowspan=2>User</th>
			<th colspan=3>Categories</th>
			<th rowspan=2></th>
		</tr>
		<tr>
			<th width=80>View</th>
			<th width=80>Maintain</th>
			<th width=80>Upload Only</th>
		</tr>
	</thead><tbody>
	<?php 
	foreach($users as $i => $u) {
		echo "<tr>
				<th class=center>".$i."</th>
				<td style=\"padding-right: 10px;\">".$u['uname']."</td>";
			foreach($type as $t) {
				echo "<td class=center>".(isset($cate[$t][$i]) ? $cate[$t][$i] : "-")."</td>";
			}
		echo "	<td class=center><input type=button value=Edit id=\"".$i."\" /></td>
			</tr>";
	} ?>
	</tbody></table>
	<script type=text/javascript>
	$(document).ready(function() {
		$("#tbl_list input:button").click(function() {
			var i = $(this).attr("id");
			url = "admin_user.php?act=EDIT&i="+i;
			document.location.href = url;
		});
	});
	</script>
<?php
}	//end switch $action
?>
</body>
</html>
