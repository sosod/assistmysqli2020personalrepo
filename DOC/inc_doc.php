<?php
//error_reporting(-1);
$today = $helper->getToday();
function OdisplayResult($result) {
    if(count($result)>0) {
        echo("<div class=\"ui-widget\">");
        if($result[0]=="check")
        {
            echo("<div class=\"ui-state-highlight ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">");
            echo("<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>");
        }
        else
        {
            echo("<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">");
            echo("<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>");
        }
        echo($result[1]."</p></div></div>");
    }
}

function logAct($laction,$lsql,$ltype,$ref) {
	global $cmpcode;
	global $tkid;
	global $moduledb;
	global $today,$helper;

	$laction = $helper->code($laction);
	$lsql = $helper->code($lsql);

	$sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_log VALUES ";
	$sql.= "(null,'$laction','$lsql',$today,'$tkid','$ltype',$ref)";
	//include("inc_db_con.php");
    $helper->db_insert($sql);

}

function getCategory($cateid) {
    global $cmpcode;
    global $dbref,$helper;
    $cate = array();
    if($helper->checkIntRef($cateid)) {
        $sql = "SELECT * FROM ".$dbref."_categories WHERE cateid = $cateid ";
        //include("inc_db_con.php");
            $cate = $helper->mysql_fetch_one($sql);
        //mysql_close($con);
    }
    return $cate;
}

function saveComment($var) {
	global $cmpcode;
	global $dbref;
	global $today;
	global $tkid,$helper;
	$result = array();
		if(isset($var['cateid']) && $helper->checkIntRef($var['cateid'])) {
			if(isset($var['commtext']) && strlen($var['commtext'])>0) {
				$cateid = $var['cateid'];
				$commtext = $helper->code($var['commtext']);
				$sql = "INSERT INTO ".$dbref."_categories_comments 
							(commcateid, commtext, commadduser, commadddate, commmoduser, commmoddate, active)
						VALUES
							($cateid,'$commtext','$tkid',$today,'',0,true)";
				$i = $helper->db_insert($sql);
					if($helper->checkIntRef($i)) {
						$commid = $i;//mysql_insert_id();
						logAct("Added comment",$sql,"COMM_ADD",$commid);
						$result[0] = "ok";
						$result[1] = "Comment saved.";
					} else {
						$result[0] = "error";
						$result[1] = "An error occurred while trying to save the comment.  Please try again.";
					}
			} else {
				$result[0] = "error";
				$result[1] = "Please enter the comment text.";
			}
		} else {
			$result[0] = "error";
			$result[1] = "Please ensure that the category.";
		}
	return $result;
}

function editComment($var) {
	global $cmpcode;
	global $dbref;
	global $today;
	global $tkid,$helper;
	$result = array();
			if(isset($var['commid']) && $helper->checkIntRef($var['commid']) && isset($var['commtext']) && strlen($var['commtext'])>0) {
				$commid = $var['commid'];
				$commtext = $helper->code($var['commtext']);
				$sql = "UPDATE ".$dbref."_categories_comments SET commtext = '$commtext', commmoddate = $today, commmoduser = '$tkid' WHERE commid = ".$commid;
				$mar = $helper->db_update($sql);
				if($mar>0) {
					logAct("Edited comment",$sql,"COMM_EDIT",$commid);
					$result[0] = "ok";
					$result[1] = "Comment edited.";
				} else {
					$result[0] = "error";
					$result[1] = "An error occurred while trying to edit the comment.  No change was made to the database.  Please try again.";
				}
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred while trying to edit the comment.  Please try again.";
			}
	return $result;
}

function deleteComment($var) {
	global $cmpcode;
	global $dbref,$helper;
	$result = array();
			if(isset($var['commid']) && $helper->checkIntRef($var['commid'])) {
				$sql = "UPDATE ".$dbref."_categories_comments SET active = false WHERE commid = ".$var['commid'];
				//include("inc_db_con.php");
				if($helper->db_update($sql)>0) {
					logAct("Deleted comment",$sql,"COMM_DEL",$var['commid']);
					$result[0] = "ok";
					$result[1] = "Comment deleted.";
				} else {
					$result[0] = "error";
					$result[1] = "An error occurred while trying to delete the comment.  No change was made to the database.  Please try again.";
				}
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred while trying to delete the comment.  Please try again.";
			}
	return $result;
}


function drawCommentDialog($path,$cateid) {
	echo chr(10)."<div id=\"dialog\" title=\"Add Comment\">";
		echo "<form name=addcomm method=post action=".$path.">";
			echo "<input type=hidden name=act id=act value=SAVE_COMM />";
			echo "<input type=hidden name=cateid id=cateid value=\"$cateid\" />";
			echo "<input type=hidden name=commid id=commid value=0 />";
			echo "<p><textarea name=commtext id=commtext rows=10 cols=60></textarea></p>";
			echo "<p><input type=submit value=Save class=isubmit /> <input type=button value=Cancel id=cancel /></p>";
		echo "</form>";
	echo "</div>";
	echo chr(10)."<div id=\"del_dialog\" title=\"Delete Comment\">";
		echo "<form name=delcomm method=post action=".$path.">";
			echo "<input type=hidden name=act id=del_act value=DEL_COMM />";
			echo "<input type=hidden name=cateid id=del_cateid value=\"$cateid\" />";
			echo "<input type=hidden name=commid id=del_commid value=0 />";
			echo "<p>Are you sure you wish to delete this comment:</p>";
			echo "<p style=\"border: 1px solid #ababab;\"><label id=del_commtext for=del_commid style=\"font-style: italic;\"></label></p>";
			echo "<p><input type=submit value=Yes id=del_yes /> <input type=button value=No id=del_no /></p>";
		echo "</form>";
	echo "</div>";
}

function drawGeneralCommentDialog($path) {
	global $cmpcode, $tkid;
	global $dbref;
	global $access;
	global $categories;

	
	
//	$cates = array();
//	$subs = array();
	
	/*$sql = "SELECT *, cateyn as yn FROM ".$dbref."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs))
		{
			$subid = $row['catesubid'];
			$cateid = $row['cateid'];
			if($subid!=0) {
				$subs[$subid]['yn'] = "Y";
				$subs[$subid][$cateid] = $row;
			} else {
				$cates[$cateid] = $row;
			}
		}
	//mysql_close($con);*/
	
	echo chr(10)."<div id=\"dialog\" title=\"Add Comment\">";
		echo "<form name=addcomm method=post action=".$path.">";
			echo "<input type=hidden name=act id=act value=SAVE_COMM />";
			echo "<p>Category:<br /><select name=cateid><option selected value=X>--- SELECT ---</option>";
			/*foreach($cates as $c) {
				$id = $c['cateid'];
				echo "<option value=".$id.">".$c['catetitle']."</option>";
				if($subs[$id]['yn'] == "Y" && count($subs[$id]) > 1) {
					foreach($subs[$id] as $key => $s) {
						if($key != "yn") {
							echo "<option value=".$s['cateid'].">   - ".$s['catetitle']."</option>";
						}
					}
				}
			}*/
			echo optionFunction($categories[0],0,"");
			echo "</select></p>";
			echo "<p>Comment:<br /><textarea name=commtext id=commtext rows=10 cols=60></textarea></p>";
			echo "<p><input type=submit value=Save class=isubmit /> <input type=button value=Cancel id=cancel /></p>";
		echo "</form>";
	echo "</div>";
}


function viewComments($cateid) {
	global $cmpcode;
	global $dbref,$helper;
	
	$sql = "SELECT * FROM ".$dbref."_categories_comments WHERE active = true AND commcateid = $cateid ORDER BY commid DESC";
	//include("inc_db_con.php");
		$mnr = $helper->db_get_num_rows($sql);
		$rows = $helper->mysql_fetch_all($sql);
		$comments = array();
		//while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){$comments[] = $row; }
	//mysql_close($con);
	if($mnr>0 && count($comments) > 0) {
		echo chr(10)."<span style=\"float: right; padding-right: 10px; margin-top: -15px;\">";
		echo chr(10)."<h2>Comments</h2>";
		$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as user FROM assist_".$cmpcode."_timekeep WHERE tkid IN (
			SELECT DISTINCT commadduser FROM ".$dbref."_categories_comments WHERE active = true AND commcateid = $cateid ORDER BY commid DESC
			)";
		//include("inc_db_con.php");
			$commusers = array();
        $rows = $helper->mysql_fetch_all($sql);
			//while($u = mysql_fetch_assoc($rs)) {
        foreach ($rows as $u){
				$commusers[$u['tkid']] = $u;
			}
		//mysql_close($con);
		echo "<table cellpadding=3 cellspacing=0 class=showborder>";
			echo "<tr>";
				echo "<th width=100>Added</th>";
				echo "<th width=400>Comment</th>";
			echo "</tr>";
		foreach($comments as $comm) {
			$commid = $comm['commid'];
			echo chr(10)."<tr>";
				echo "<td class=showborder style=\"vertical-align: top; text-align: center\">".date("d M Y",$comm['commadddate'])."<br />".date("H:i",$comm['commadddate']).(isset($commusers[$comm['commadduser']]) ? "<br />by ".$commusers[$comm['commadduser']]['user'] : "")."</td>";
				echo "<td class=showborder style=\"vertical-align: top;\"><input type=hidden name=commid[] id=id_".$commid." value=\"$commid\" /><label id=text_".$commid." for=id_".$commid.">".str_replace(chr(10),"<br />",$comm['commtext'])."</label></td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "</span>";
	}
}

function adminComments($cateid) {
	global $cmpcode;
	global $dbref,$helper;
	echo chr(10)."<h3>Comments";
	echo "<span class=float><input type=button value=\"Add Comment\" id=add /></span>";
	echo "</h3>";
	$sql1 = "SELECT * FROM ".$dbref."_categories_comments WHERE active = true AND commcateid = $cateid ORDER BY commid DESC";
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as user FROM assist_".$cmpcode."_timekeep WHERE tkid IN (
		SELECT DISTINCT commadduser FROM ".$dbref."_categories_comments WHERE active = true AND commcateid = $cateid ORDER BY commid DESC
		)";
    $rows = $helper->mysql_fetch_all($sql);
	//include("inc_db_con.php");
		$commusers = array();
		//while($u = mysql_fetch_assoc($rs)) {
    foreach ($rows as $u){
			$commusers[$u['tkid']] = $u;
		}
	//mysql_close($con);
	$sql = $sql1;
    $rows = $helper->mysql_fetch_all($sql);
	//include("inc_db_con.php");
		if($helper->db_get_num_rows($sql)>0) {
			echo "<table width=\"100%\" class=showborder>";
				echo "<tr>";
					echo "<th width=100>Added</th>";
					echo "<th>Comment</th>";
					echo "<th width=70>&nbsp;</th>";
				echo "</tr>";
			//while($comm = mysql_fetch_assoc($rs)) {
            foreach ($rows as $comm){
				$commid = $comm['commid'];
				echo chr(10)."<tr>";
					echo "<td class=showborder style=\"vertical-align: top; text-align: center\">".date("d M Y",$comm['commadddate'])."<br />".date("H:i",$comm['commadddate']).(isset($commusers[$comm['commadduser']]) ? "<br />by ".$commusers[$comm['commadduser']]['user'] : "")."</td>";
					echo "<td class=showborder style=\"vertical-align: top;\"><input type=hidden name=commid[] id=id_".$commid." value=\"$commid\" /><label id=text_".$commid." for=id_".$commid.">".str_replace(chr(10),"<br />",$comm['commtext'])."</label></td>";
					echo "<td class=showborder style=\"vertical-align: top;text-align: center\"><input type=button value=Edit onclick=\"editComm($commid);\" /><br /><input type=button value=Delete id=del onclick=\"delComm(".$commid.");\" /></td>";
				echo "</tr>";
			}
			echo "</table>";
		} else {
			echo "<p>No comments to display.</p>";
		}
	//mysql_close($con);
}



function optionFunction($sub,$level,$title) {
	global $categories;
	global $access;
	global $my_access;
	global $tkid;

	//arrPrint($sub);

	foreach($sub as $c) {
		if($access['docadmin']=="Y" || $c['cateowner']==$tkid || isset($my_access[$c['cateid']])) {
				$text = (strlen($title)>0 ? $title." >> " : "").$c['catetitle'];
				$echo.= "<option value=".$c['cateid'].">".$text."</option>"; //."</span></p>";
		}
		$sub = isset($categories[$c['cateid']]) ? $categories[$c['cateid']] : array();
		if(count($sub)>0) {
			$echo.=optionFunction($sub,$level+1,(strlen($title)>0 ? $title." >> " : "").$c['catetitle']);
		}
	}
	return $echo;
}



















/*******************************************
             ACTIVITY LOGGING
*******************************************/

function logTransaction($type,$l,$lsql) {
global $dbref;
global $helper;


switch($type) {
case "SETUP":
	$sql = "INSERT INTO ".$dbref."_setup_log (
		id,
		date,
		tkid,
		tkname,
		section,
		ref,
		action,
		field,
		transaction,
		old,
		new,
		active,
		lsql
	) VALUES (
		null,
		now(),
		'".$_SESSION['tid']."',
		'".$_SESSION['tkn']."',
		'".$l['section']."',
		".$l['ref'].",
		'".$l['action']."',
		'".$l['field']."',
		'".$helper->code($l['transaction'])."',
		'".$l['old']."',
		'".$l['new']."',
		1,
		'".$helper->code($lsql)."'
	)";
	break;
case "ACTIVITY":
	$sql = "INSERT INTO ".$dbref."_activity_log (
		id,
		date,
		tkid,
		tkname,
		section,
		ref,
		cateid,
		action,
		field,
		transaction,
		old,
		new,
		active,
		lsql
	) VALUES (
		null,
		now(),
		'".$_SESSION['tid']."',
		'".$_SESSION['tkn']."',
		'".$l['section']."',
		".$l['ref'].",
		".$l['cateid'].",
		'".$l['action']."',
		'".$l['field']."',
		'".$helper->code($l['transaction'])."',
		'".$l['old']."',
		'".$l['new']."',
		1,
		'".$helper->code($lsql)."'
	)";
	break;
}

if(isset($sql) && strlen($sql)>0) $helper->db_insert($sql);

}

?>