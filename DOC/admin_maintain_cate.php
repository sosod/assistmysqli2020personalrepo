<?php
    require 'inc_head.php';

/******************
	EDIT CATEGORY
		Purpose: To edit details captured during the creation process, on a per category basis.
		Created: Pre-2012
		Created by: Janet Currie
		Edited: 30 January 2012
		Edited by: Janet Currie
	USES:
		$levels_of_children   => Source: inc_ignite.php
****************/

//GET CATEGORY DETAILS
if(!isset($_REQUEST['c']) || !$helper->checkIntRef($_REQUEST['c'])) {
	die("<h2 class=red>Error</h2><p>An error occurred while trying to identify the category you with to edit.  <br />Please go back and try again.</p>");
} else {
	$cateid = $_REQUEST['c'];
	$sql = "SELECT * FROM ".$dbref."_categories WHERE cateid = $cateid AND cateyn = 'Y'";
	$cate = $helper->mysql_fetch_one($sql);

//GET CATEGORY STRUCTURE	
	$c = $cate['catesubid'];
	$parents = array();
	$parents[] = $c;
	while($c!=0) {
		$sql = "SELECT catesubid FROM ".$dbref."_categories WHERE cateid = $c";
		$x = $helper->mysql_fetch_one($sql);
		$c = $x['catesubid'];
		$parents[] = $c;
	}
	
}
unset($parents[count($parents)-1]);
$hierarchy = array();
for($p=count($parents)-1;$p>=0;$p--) {
	$hierarchy[] = $parents[$p];
}
//arrPrint($hierarchy);

//GET CATEGORY USER LIST
$cate_users = array('VIEW'=>array(),'MAIN'=>array(),'UP'=>array());
$sql = "SELECT * FROM ".$dbref."_categories_users 
		WHERE cucateid = $cateid AND cuyn = 'Y'";
	//$rs = getRS($sql);
$rows = $helper->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
	foreach ($rows as $row){
		$cate_users[$row['cutype']][] = $row['cutkid'];
	} 
//mysql_close();



//GET GENERIC USER LIST
$users = array();
$viewers = array();
$nonview = array();
$sql = "SELECT DISTINCT t.tkid, t.tkname, t.tksurname, l.view FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u, ".$dbref."_list_users l ";
$sql.= "WHERE t.tkid = u.usrtkid AND u.usrmodref = '".$tref."' AND t.tkstatus = 1 AND t.tkid <> '0000' AND t.tkid = l.tkid AND l.yn = 'Y' ";
$sql.= " ORDER BY t.tkname, t.tksurname";
    //include("inc_db_con.php");
    $u = 0;
    $v = 0;
    $n = 0;
$rows = $helper->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            $users[$id] = array($id,$val);
            //$u++;
            if($row['view']=="Y") {
                $viewers[$id] = array($id,$val);
               // $v++;
            } else {
                $nonview[$id] = array($id,$val);
               // $n++;
            }
        }
    //mysql_close($con);
//    print_r($users);
if(count($users)>10) { $sc = 10; } else { $sc = count($users); }
if(count($nonview)>10) { $nv = 10; } else { $nv = count($nonview); }

?>
<style type=text/css>
table th { vertical-align: top; }
table tr { height: 30px; }
table th.disabled { background-color: #999999; }
</style>
<h1><a href=admin.php class=breadcrumb>Admin</a> >> <a href=admin_maintain.php class=breadcrumb>Maintain</a> >> Edit Category</b></h1>
<?php if(isset($result)) { $helper->displayResult($result); } ?>
<form id=frm_edit name=editcate method=post action=admin_maintain_cate_process.php>
<input type=hidden name=cateid value=<?php echo $cateid; ?> />
<input type=hidden name=act id=act value=EDIT />
<h2>Category Details</h2>
<p><span class=iinform>Please note:</span> All fields are required. Where information is missing, the field will be highlighted in red.</p>
<table width=600><tbody>
    <tr height=30>
        <th width=150 style="text-align:left;">Category Name:</th>
        <td><input type=text name=catetitle value="<?php echo $helper->decode($cate['catetitle']); ?>" size=50 maxlength=200 id=ct></td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category Structure:</th>
        <td><div id=child class=b><span id=spn_parent>Parent: <select name=catesubid[0] id=parent>
            <option selected value=0>None</option>
<?php
    $sql = "SELECT * FROM ".$dbref."_categories WHERE catesubid = 0 AND cateyn = 'Y' AND cateid <> $cateid ORDER BY catetitle";
    $categories = $helper->mysql_fetch_all($sql);
        foreach($categories as $row)
        {
            $id = $row['cateid'];
            $val = $row['catetitle'];
            echo("<option ".(isset($hierarchy[0]) && $helper->checkIntRef($hierarchy[0]) && $hierarchy[0]==$id ? "selected" : "")." value='$id'>$val</option>");
        }
    //mysql_close($con);
?>
        </select></span>
		<?php for($i=1;$i<=$levels_of_children;$i++) {
			echo chr(10)."<span id=spn_child_".$i." class=spn_child><br />Level ".$i.": <select name=catesubid[$i] id=child_".$i." class=child level=$i >";
			if((isset($hierarchy[$i]) && checkIntRef($hierarchy[$i])) || (isset($hierarchy[$i-1]) && $helper->checkIntRef($hierarchy[$i-1])) ) {
				echo "<option ".(!isset($hierarchy[$i]) || !checkIntRef($hierarchy[$i]) ? "selected" : "")." value=X>None</option>";
				$sql = "SELECT * FROM ".$dbref."_categories WHERE catesubid = ".$hierarchy[$i-1]." AND cateyn = 'Y' AND cateid <> ".$cateid." ORDER BY catetitle";
				$categories = $helper->mysql_fetch_all($sql);
					foreach($categories as $row) {
						$id = $row['cateid'];
						$val = $row['catetitle'];
						echo("<option ".(isset($hierarchy[$i]) && $helper->checkIntRef($hierarchy[$i]) && $hierarchy[$i]==$id ? "selected" : "")." value='$id'>$val</option>");
					}
				//mysql_close($con);
			} else {
				echo "<option selected value=X>None</option>";
			}
			echo "</select></span>";
		} ?>
		</div><span id=sub_text class=i style="font-size: 7pt;"><span class=iinform>Please note:</span> View Access (below) can only be set for categories without a parent category.  Any sub-category will automatically inherit its View Access settings from the parent category.</span>
		</td>
    </tr>
    <tr height=30>
        <th style="text-align:left;">Category owner:</th>
        <td><select name=cateowner id=cateowner><option value=X>Unspecified</option>
<?php
foreach($users as $u)
        {
			$id = $u[0];
			$val = $u[1];
            if($id == $cate['cateowner']) { $sel = "selected"; } else { $sel = ""; }
            echo("<option $sel value='$id'>$val</option>");
        }
?>
        </select></td>
    </tr>
	<tr>
		<th class=left>Sub-Categories:</th>
		<td><?php
			$sql = "SELECT c.cateid, c.catetitle, CONCAT_WS(' ',t.tkname,t.tksurname) as cateowner 
					FROM ".$dbref."_categories c 
					INNER JOIN assist_".$cmpcode."_timekeep t ON c.cateowner = t.tkid
					WHERE c.cateyn = 'Y' AND c.catesubid = $cateid
					ORDER BY c.catetitle";
			//$rs = getRS($sql);
            $rows = $helper->mysql_fetch_all($sql);
			$children = $helper->db_get_num_rows($sql);
			if($helper->db_get_num_rows($sql)>0) {
				echo "<ul>";
				//while($row = mysql_fetch_assoc($rs)) {
				foreach ($rows as $row){
					echo "<li>".$row['catetitle']." [".$row['cateowner']."]</li>";
				}
				echo "</ul><span class=i>Please Note: Categories with sub-categories cannot be deleted.</span>";
			} else {
				echo "This category does not have any sub-categories.";
			}
			unset($rs);
		?></td>
	</tr>
	</tbody></table>
<h2>User Access</h2>
<p><span class=iinform>Please Note:</span> <span id=note>Category Owner automatically receives complete access.</span></p>
	<table width=600 id=user_access ><tbody>
	<tr id=tr_auto>
		<th id=tr_auto width=150 class="left">Automatic View Access:</th>
		<td colspan=2><?php
				if(count($viewers)>0) {
					echo "<ul id=ul_view>";
					foreach($viewers as $v) {
						echo "<li style=\"color: #000000;\">".$v[1]."<input type=hidden name=view[] value=".$v[0]." /></li>";
					}
					echo "</ul>";
				} 
		?></td>
	</tr>
	<tr id=tr_view>
		<th id=th_view class="left">View Access:<br />&nbsp;<br /><span class=i style="font-weight: normal; font-size: 7pt">To select multiple users, hold down the CTRL key and left-click on the user names.</span></th>
		<td id=td_view style="border-right: 0px;"><?php
			if(count($nonview) > 0) {
				echo "	<select size=".$sc." multiple name=view[] id=sel_view>";
				foreach($nonview as $n) {
					echo "<option ".(in_array($n[0],$cate_users['VIEW']) ? "selected" : "")." value=".$n[0].">".$n[1]."</option>";
				}
				echo "	</select>";
			}
			?></td>
		<td class="middle center" style="border-left: 0px;">Users can view the category and<br />all associated documents.</td>
	</tr>
	<tr>
		<th class="left">Maintain Documents:<br />&nbsp;<br /><span class=i style="font-weight: normal; font-size: 7pt">To select multiple users, hold down the CTRL key and left-click on the user names.</span></th>
		<td style="border-right: 0px;"><?php
			if(count($users) > 0) {
				echo "	<select size=".$sc." multiple name=maintain[] id=sel_maintain>";
				foreach($users as $n) {
					echo "<option ".(in_array($n[0],$cate_users['MAIN']) ? "selected" : "")." value=".$n[0].">".$n[1]."</option>";
				}
				echo "	</select>";
			}
			?></td>
		<td class="middle center" style="border-left: 0px;">Users can access the category under<br />Admin and upload, edit & delete<br />documents within the category.</td>
	</tr>
	<tr>
		<th width=150 class="left">Upload Documents<br />/ Add Comments only:<br />&nbsp;<br /><span class=i style="font-weight: normal; font-size: 7pt">To select multiple users, hold down the CTRL key and left-click on the user names.</span></th>
		<td style="border-right: 0px;"><?php
			if(count($users) > 0) {
				echo "	<select size=".$sc." multiple name=upload[] id=sel_upload>";
				foreach($users as $n) {
					echo "<option ".(in_array($n[0],$cate_users['UP']) ? "selected" : "")." value=".$n[0].">".$n[1]."</option>";
				}
				echo "	</select>";
			}
			?></td>
		<td class="middle center" style="border-left: 0px;">Users can only upload<br />documents or add comments<br />to the category.</td>
	</tr>
	</tbody></table>
	<table width=600 style="margin-top: 20px"><tbody>
    <tr height=30>
        <td class=center><input type=submit value="Save Changes" class=isubmit /> <input type=reset> <?php if($children==0 && ($access['docadmin']=="Y" || $access['delcate']=="A" || ($access['delcate']=="O" && $tkid == $cate['cateowner']))) { ?><span class=float><input type=button value="Delete Category" class=idelete /></span><?php } ?></th>
    </tr>
</tbody></table>
</form>
<?php $helper->displayGoBack();  ?>
<script type=text/javascript>
function cateAjax(id,fld) {
		$.ajax({                                      
		  url: 'inc_ajax.php', 		  type: 'POST',		  data: "i="+id+"&act=CATE",		  dataType: 'json', 
		  success: function(rows) {
			for(var i in rows) {
				var row = rows[i];
				if(row['cateid']!=my_id) {
					$(fld).append("<option value="+row['cateid']+" cc="+row['cc']+">"+row['catetitle']+"</option>");
				}
			}
		  } 
		});
}
	var levels_of_children = <?php echo $levels_of_children; ?>;
	var my_id = <?php echo $cateid; ?>;
	var hierarchy = new Array();
	<?php 
	foreach($hierarchy as $h => $v) {
		echo chr(10)."   hierarchy[$h] = $v;";
	} echo chr(10);
	?>
$(function() {
	$("#frm_edit .idelete").click(function() {
		if(confirm("Are you sure you wish to delete this category?\n\nThis will also delete any documents within the category.")==true) {
			$("#frm_edit #act").val("DELETE");
			$("#frm_edit").submit();
		}
	});
	$("#frm_edit .isubmit").click(function() {
		var ct = $("#frm_edit #ct");
		var co = $("#frm_edit #cateowner");
		ct.removeClass("required");
		co.removeClass("required");
		var t = ct.val();
		var o = co.val();
		var err = false;
		if(t.length==0) { ct.addClass("required"); err = true; }
		if(o.length==0) { co.addClass("required"); err = true; }
		if(err) {
			alert("Please complete the missing information highlighted in red.");
		} else {
			$("#frm_edit #act").val("EDIT");
			$("#frm_edit").submit();
		}
	});
	$("#frm_edit input:reset").click(function() {
		document.location.href = document.location.href;
	});
//ON ACTIVITY ON PAGE
	if($("#cateowner").val()=="X") {
		$("#cateowner").addClass("required");
	}
	$("#cateowner").change(function() {
		if($("#cateowner").val()=="X") {
			$("#cateowner").addClass("required");
		} else {
			$("#cateowner").removeClass("required");
		}
	});
	$("h2").css("margin-bottom","-7px");
		//$("#child .spn_child").hide();
		$("#child .sub_text").css("font-weight","normal");
		$("#ct").focus();
		$("#parent").change(function() {
			var parent = $(this).val(); //alert(parent);
			if(parseInt(parent)==0) {	//NO PARENT = MAIN CATEGORY
				$("#user_access #td_view, #ul_view li").each(function() { 
					$(this).css("color","#000000");
				} );
				$("#user_access #sel_view").each(function() { 
					$(this).attr("disabled",""); 
				} );
				$("#user_access #th_view, #user_access #th_auto").each(function() { 
					$(this).removeClass("disabled"); 
				} );
				$("#user_access #tr_view, #user_access #tr_auto").each(function() { 
					$(this).show(); 
				} );
				//$(".child").hide();
				$("#child .child").empty();
				$("#child .child").append("<option selected value=X cc=0>None</option>");
				$("#child .spn_child").hide();
			} else {					//PARENT = COPY USER ACCESS
				$("#user_access #td_view, #ul_view li").each(function() { 
					$(this).css("color","#ababab");
				} );
				$("#user_access #sel_view").each(function() { $(this).attr("disabled","true"); } );
				$("#user_access #th_view, #user_access #th_auto").each(function() { 
					$(this).addClass("disabled"); 
				} );
				$("#user_access #tr_view, #user_access #tr_auto").each(function() { 
					$(this).hide(); 
				} );
				changeChild(1,1,parent);
			}
		});
		$("#child .child").change(function() {
			var cc = $(this).children("option:selected").attr("cc");
			var l = $(this).attr("level");
			l++;
			var v = $(this).val();
			changeChild(cc,l,v);
		});
	});
	function changeChild(cc,l,v) {
			if(l<=levels_of_children) {
				//clear any existing child selects
				for(var i = l; i<=levels_of_children; i++) {
					$("#child #child_"+i).empty();
					$("#child #child_"+i).append("<option selected value=X cc=0>None</option>");
					$("#child #spn_child_"+i).hide();
				}
				//display next level
				if(v!="X" && v!="0") {
					$("#child #spn_child_"+l).show();
				}
			}	
			//if current level has subs, use ajax to get subs
			if(cc>0 && v!="X") {
				cateAjax(v,"#child_"+l);
			}
	}
	$(function() {
	/* on document load, check each category child = display or not */
		var x;
		for(var i = 2; i<=levels_of_children; i++) {
			x = i-1;
			if($("#child_"+i).val() == "X" && $("#child_"+x).val()=="X") {
				$("#child #spn_child_"+i).hide();
			}
		}
	/* if parent selected, hide user access >> view option */
		if($("#parent").val()!=0 && $("#parent").val() != "X") {
				$("#user_access #td_view, #ul_view li").each(function() { 
					$(this).css("color","#ababab");
				} );
				$("#user_access #sel_view").each(function() { $(this).attr("disabled","true"); } );
				$("#user_access #th_view, #user_access #th_auto").each(function() { 
					$(this).addClass("disabled"); 
				} );
				$("#user_access #tr_view, #user_access #tr_auto").each(function() { 
					$(this).hide(); 
				} );
		}
	
	});

</script>
</body>
</html>
