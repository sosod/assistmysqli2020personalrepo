<?php
include 'inc_ignite.php';

switch($_REQUEST['act']) {
case "ADD":
	$val = $_REQUEST['value'];
	if(strlen($val)>0) {
		$val = $helper->code($val);
		$sql = "INSERT INTO ".$dbref."_list_type (value,yn) VALUES ('".$val."','Y')";
		$mnr = $helper->db_insert($sql);
		if($helper->checkIntRef($mnr)) {
			logAct($helper->code("Added Document Type '".$helper->decode($val)."' ($mnr)"),$sql,"S_TYPE",$mnr);
			$r0 = "ok";
			$r1 = "New Document Type \'$val\' created successfully.";
		} else {
			$r0 = "error";
			$r1 = "New Document Type was not created.";
		}
	} else {
		$r0 = "error";
		$r1 = "Ignite Assist was unable to complete your request.  Please try again.";
	}
	break;
case "EDIT":
	$id = $_REQUEST['i'];
	$val = $_REQUEST['value'];
	if(strlen($val)>0 && $helper->checkIntRef($id)) {
		$old = $helper->mysql_fetch_one("SELECT * FROM ".$dbref."_list_type WHERE id = $id");
		$val = $helper->code($val);
		$sql = "UPDATE ".$dbref."_list_type SET value = '".$val."' WHERE id = ".$id;
		$mnr = $helper->db_update($sql);
		if($helper->checkIntRef($mnr)) {
			logAct($helper->code("Updated Document Type $id to '".$helper->decode($val)."' from '".$helper->decode($old['value'])."'."),$sql,"S_TYPE",$id);
			$r0 = "ok";
			$r1 = "Document Type : ".$val." successfully updated.";
		} else {
			$r0 = "error";
			$r1 = "No change was found to be saved.";
		}
	} else {
		$r0 = "error";
		$r1 = "Ignite Assist was unable to complete your request.  Please try again.";
	}
	break;
case "DELETE":
	$id = $_REQUEST['i'];
	$val = $_REQUEST['value'];
	if($helper->checkIntRef($id)) {
		$old = $helper->mysql_fetch_one("SELECT * FROM ".$dbref."_list_type WHERE id = $id");
		$val = $helper->code($val);
		$sql = "UPDATE ".$dbref."_list_type SET yn = 'N' WHERE id = $id";
		$mnr = $helper->db_update($sql);
		if($helper->checkIntRef($mnr)) {
			logAct($helper->code("Deleted Document Type '".$helper->decode($old['value'])."' ($id)."),$sql,"S_TYPE",$id);
			$r0 = "ok";
			$r1 = "Document Type : ".$val." deleted successfully.";
		} else {
			$r0 = "info";
			$r1 = "Document Type could not be found to be deleted.";
		}
	} else {
		$r0 = "error";
		$r1 = "Ignite Assist was unable to complete your request.  Please try again.";
	}
	break;
default: 
		$r0 = "error";
		$r1 = "Ignite Assist was unable to complete your request.  Please try again.";
}

echo "<script type=text/javascript>
		document.location.href = 'setup_list_doctype.php?r[]=".$r0."&r[]=".$r1."';
		</script>";
?>