<?php
include 'inc_header.php';

?>
<h1>Server Database Activity Report - Transaction Log</h1>
<?php
$company = array();
$sql = "SELECT cmpcode, cmpname, cmpstatus FROM assist_company";
$rs = AgetRS($sql);
    while($row = mysql_fetch_array($rs))
    {
        $company[$row['cmpcode']] = $row;
    }
mysql_close();

$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);
$db_array = array();
$k = 0;
foreach($dbs as $db) {
	if($db!=$db_assist && substr($db,0,strlen($db_other))==$db_other) {
		$cc = strtoupper(substr($db,strlen($db_other)));
		if(isset($company[$cc])) {
			$cn = $company[$cc]['cmpname'];
			$cs = $company[$cc]['cmpstatus'];
		} else {
			$cn = "Invalid";
			$cs = "N/A";
		}
		switch($cs) {
		case "Y": $class = "isubmit"; $cs = "Available"; break;
		case "N": $class = "idelete"; $cs = "Closed"; break;
		case "N/A":
		default: $class = "iinform"; break;
		}
		$db_name = $db_other.strtolower($cc);
		$sql = "SHOW TABLES LIKE 'assist_".strtolower($cc)."_log'";
		$rs = getRS($sql);
		if(mysql_num_rows($rs) > 0) {
			$sql = "SELECT * FROM assist_".strtolower($cc)."_log ORDER BY id DESC LIMIT 1";
			$row = mysql_fetch_all($sql);
			if(!isset($row[0]) || !isset($row[0]['date']) || $row[0]['date'] < 1) {
				$activity = "No activity found.";
				$key = $k; $k++;
			} else {
				$activity = date("d M Y H:i:s",$row[0]['date']);
				$key = $row[0]['date'];
			}
		} else {
			$activity = "Error.  Log table not found.";
			$key = $k; $k++;
		}
		$db_array[$key] = array('cc'=>$cc,'cn'=>$cn,'cs'=>$cs,'activity'=>$activity,'class'=>$class);
	}
}
?>
<table>
<thead>
	<tr>
		<th>Database</th>
		<th>Company</th>
		<th>Company Status</th>
		<th>Last Activity</th>
	</tr>
</thead>
<tbody>
<?php

ksort($db_array);
foreach($db_array as $d) {
		echo "<tr>
				<td class=b>".$d['cc']."</td>
				<td>".$d['cn']."</td>
				<td class=\"".$d['class']." center\" >".$d['cs']."</td>
				<td>".$d['activity']."</td>
			</tr>";
}
?>
</tbody>
</table>
</body>
</html>