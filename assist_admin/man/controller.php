<?php
include "../../module/autoloader.php";

$setup = new MAN_SETUP();

$result = array();

switch($_REQUEST['act']) {
	case "add":
		$var = $_REQUEST;
		$tbl = $var['tbl'];
		unset($var['act']);
		unset($var['tbl']);
		$result = $setup->addObject($tbl,$var);
		break;
	case "edit":
		$var = $_REQUEST;
		$tbl = $var['tbl'];
		$id = $var['id'];
		unset($var['act']);
		unset($var['tbl']);
		unset($var['id']);
		$result = $setup->editObject($tbl,$id,$var);
		break;


}


echo json_encode($result);

?>