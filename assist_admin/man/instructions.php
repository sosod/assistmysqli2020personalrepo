<?php
include "header.php";


$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "edit";

//$mod_title = $me->getModTitle($modref);

if($act=="new") {
	$instructions = array();
	for($i=0;$i<5;$i++) {
		$instructions[] = array(
			'instruction'=>"",
			'instruction_notes'=>"",
			'picture'=>"",
		);
	}
	$hid = 0;
	$sid = 0;
	$mid = 0;
	$modloc = $_REQUEST['ml'];
	$modref = $_REQUEST['ml'];
	echo "<h1><a href=index.php class=breadcrumb>Help</a> >> New Instructions</h1>";
} else {
	$hid = $_REQUEST['hid'];
	$sid = $_REQUEST['sid'];
	$mid = $_REQUEST['mid'];
	$modref = $_REQUEST['mr'];
	$instructions = $man->getInstructions($_REQUEST['mid'],$_REQUEST['sid'],$_REQUEST['hid']);

	$x = $instructions[0];
	$modloc = $x['modloc'];
	$mod_title = $x['module_name'];

	echo "<h1><a href=index.php class=breadcrumb>Help</a> >> <a href=module.php?mr=".$modloc."&ml=".$modloc." class=breadcrumb>".$mod_title."</a> >> ".$x['section_name']."</h1>";
	echo "<h2>".$x['heading_name']."</h2>";

}

$mods = $man->getAllModules();
$secs = $man->getAllSections();
$heads = $man->getAllHeadings();

//$me->arrPrint($mods);

$me->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

echo "
<form name=frm_instruction action=instructions_process.php method=post>
	<input type=hidden name=mr value='".$modloc."' />
		<p>Module ID: <input type=hidden name=old_mid value='$mid' /><select name=mid>";
		foreach($mods as $m) {
			if($act=="new") {
				echo "<option ".($m['modloc'] == $modloc ? "selected" :"")." value=".$m['id'].">".$m['module_name']."</option>";
			} else {
				echo "<option ".($m['id'] == $mid ? "selected" :"")." value=".$m['id'].">".$m['module_name']."</option>";
			}
		}
echo "
</select>
		<p>Section ID: <input type=hidden name=old_sid value='$sid' /><select name=sid><option value=0 ".($act=="new" ? "selected" : "").">--- SELECT ---</option>";
		foreach($secs as $s) {
			echo "<option ".($s['id'] == $sid ? "selected" :"")." value=".$s['id'].">".$s['section_name']."</option>";
		}
echo "
</select>
		<p>Heading ID: <input type=hidden name=old_hid value='$hid' /><select name=hid><option value=0 ".($act=="new" ? "selected" : "").">--- SELECT ---</option>";
		foreach($heads as $h) {
			echo "<option ".($h['id'] == $hid ? "selected" :"")." value=".$h['id'].">".$h['heading_name']."</option>";
		}
echo "
</select>
		<p>First Sort Position: <input type=text name=sort value='".(isset($x['sort']) ? $x['sort'] : "1001.1")."' />
";


echo '<div class="contain">';
$i = 0;
echo "<table class=form>";
//if($act=="new") {
	foreach($instructions as $row) {
			$i++;
		echo "
		<tr>
			<th rowspan=3>$i</th>
				<td>Activity/Instruction:</td>
			<td colspan=2>";
				echo '<textarea cols=80 rows=3 name=instruction[] class=instruct>' . $me->decode($row['instruction']) . '</textarea>';
			echo "</td></tr><tr>
				<td>Note:</td>
				<td colspan=2>";
				echo '<textarea cols=120 rows=3 name=instruction_note[] class=instruct_note>' . $me->decode($row['instruction_notes']) . '</textarea>';
			echo "</td></tr><tr>
				<td>Picture:</td>
				<td>";
					echo'<img class="instruction_pic" id=img_'.$i.' src="../../man/pictures/' . $row['picture'] . '"/>';
			echo "</td><td>";
				echo "File Name: <input type=text value='".$row['picture']."' name=picture[] i=".$i." class=instruct_pic size=50 />";
			echo "</td>
		</tr>";
	}
//}
	echo "
	<tr id=add_new>
		<th></th>
		<td colspan=3 class=right>
			<input type=button value='Add Another Instruction Row' id=btn_add />
		</td>
	</tr>
</table>";
	echo "<input type=submit value='Save Changes' class=isubmit />";
echo'</div>
</form>
';

echo "<P>"; ASSIST_HELPER::goBack("module.php?ml=".$modloc."&mr=".$modloc); echo "</p>";
?>
<style type=text/css>

	.note{
		padding:7px;
		background-color:#fbf9ee;
		border:1px solid #fe9900;
		margin-top:10px;
		margin-bottom:10px;
		width:auto;
		display:inline-block;
	}
	
	.instruction_pic{
		margin-top:10px;
		margin-bottom:10px;
		align:center;
		max-width:95%;
	}
	
	.contain{
		background-color:#f8f8f8;
		width:75%;
		position:relative;
		margin-left:10%;
		margin-right:auto;
		padding:10px;
		border-radius:8px 8px 8px 8px;
	}
	
	.instruct {
	}
	.instruct_note {
		border: 1px solid #fe9900;
		color: #fe9900;
	}
	#frm_instruction p { font-weight: bold; }
</style>
<script type=text/javascript>
$(function() {
	var i = <?php echo $i++; ?>;
	$(".instruct_pic").blur(function() {
		var v = $(this).val(); //alert(v);
		var i = $(this).attr("i"); //alert(i);
		$("#img_"+i).prop("src","../../man/pictures/"+v);
	});
	$("#btn_add").click(function() {
		var i = $(".instruct_pic:last").attr("i"); i++; //alert(i);
		var new_row = "<tr><th rowspan=3>"+i+"</th><td colspan=2><textarea cols=80 rows=3 name=instruction[] class=instruct></textarea></td></tr><tr><td colspan=2><textarea cols=120 rows=3 name=instruction_note[] class=instruct_note></textarea></td></tr><tr><td><img class=instruction_pic id=img_"+i+" src='pictures/' /></td><td><input type=text value='' name=picture[] i="+i+" class=instruct_pic size=50 /></td></tr>";
		$("table.form tr:last").before(new_row);
		$(".instruct_pic:last").bind("blur",function() {
			var v = $(this).val(); //alert(v);
			var i = $(this).attr("i"); //alert(i);
			$("#img_"+i).prop("src","pictures/"+v);
		});
	});
});
</script>