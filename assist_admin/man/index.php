<?php
include("header.php");

//MESSAGE TO FLOAT AT BOTTOM RIGHT OF PAGE
//echo $me->getFloatingDisplay(array("info","If you require assistance which cannot be found within these manuals, please contact your Assist Administrator."),"BR"); 



$modules = $man->getModulesWithManuals();
$logos = $me->getModuleLogoDetails();

//$me->arrPrint($modules);

//$man_modules = $man->getModulesWithManuals();
//$me->arrPrint($man_modules);



?>


	<style>
	
	.icon{
		margin-top:5px;
		margin-bottom:5px;
	}
	 
	 
	.sortablefolders { 
		list-style-type: none; 
		margin: 0; padding: 0; 
		list-style-image: url(); 
	}
	.sortablefolders li {  
		margin: 3px 3px 3px 0; padding: 1px; float: left; 
		width: 130px; height: 130px;  
		text-align: center; vertical-align: middle; font-size: 1em; 
		background: url();
	}

.a_hover, a_hover a { 
	color: #FE9900; 
	border: 1px solid #FE9900; 
	cursor: pointer;
}
.no_hover { 
	border: 1px solid #ababab; 
}
.no_hover, .no_hover a { 
	color: #555555; 
}

	.sortablefolders li a {  
		text-decoration: none;
	}
	 
 
</style>
<script type=text/javascript>  
$(function() {
	$(".sortablefolders" ).sortable({});
	$(".sortablefolders" ).disableSelection();
	$(".sortablefolders li").hover(
		function(){ 
			$(this).removeClass("no_hover").addClass("a_hover"); 
			$(this).children("a").css("color","#FE9900"); 
		},
		function(){ 
			$(this).removeClass("a_hover").addClass("no_hover"); 
			$(this).children("a").css("color","#555555"); 
		}
	);
	$(".btn").click(function() {
		var i = $(this).prop("id");
		document.location.href = 'setup.php?act='+i;
	});
});
</script>

<h1>Help</h1>
<div style='width: 350px; margin: 0 auto; background-color: #fe9900;'>
<div style="width: 100px; margin: 10px; padding: 10px; display: inline; background-color: #cc0001;"><input type=button value=Modules id=mod class=btn /></div>
<div style="width: 100px; margin: 10px; padding: 10px; display: inline; background-color: #009900;"><input type=button value=Sections id=sec class=btn /></div>
<div style="width: 100px; margin: 10px; padding: 10px; display: inline; background-color: #000099;"><input type=button value=Headings id=head class=btn /></div>
</div>
<div id=div_sortable style='width: 75%; margin: 0 auto; '>

<?php
echo "<h2>Active Manuals</h2>";

echo "<ul class=sortablefolders>";

foreach($modules as $m) {
	$ml = $m['modloc'];
	$mr = $ml;
	$mt = $m['module_name'];
	//if(isset($man_modules[$ml])) {
	//	$url = "faq.php?mid=".$man_modules[$ml]['id'];
	//} else {
	//	$url = "pdf.php?loc=".$ml."&ref=".$m['modref'];
	//}
	$url = "module.php?ml=".$ml."&mr=".$mr;
	$img = $file_depth.$logos['path'];
	if(isset($logos['logos'][$ml])) {
		$img.= $logos['logos'][$ml];
	} else {
		$img.= $logos['default'];
	}
	echo "<li class=\"no_hover\"><a href=\"$url\"><img class=icon src=\"$img\" /><br />".$mt."</a></li>";
}


?>

</ul>
</div>

</body>
</html>