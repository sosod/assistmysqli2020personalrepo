<?php
include("header.php");

//$me->arrPrint($_REQUEST);
$modref = $_REQUEST['mr'];
$modloc = $_REQUEST['ml'];

$man_sections = $man->getSections($modloc);
$mod_title = $man_sections[0][0]['module_name'];

//$me->arrPrint($man_sections);

echo "<h1><a href=index.php class=breadcrumb>Help</a> >> ".$mod_title."</h1>";
$me->displayResult(array("info","Please note that while every effort has been made to maintain the accuracy and scope of these manuals, some methods may differ slightly from those applied on your Assist system.  Screenshots in these manuals may differ slightly from those found in your web browser. Lists and data have been captured from a demonstration database and will differ from those in your organisation. Certain headings and text fields may differ or be absent from those in the manuals, according to the Setup steps taken by your Assist Administrator."));



	//echo "Display NEW MANUALS HERE!! :-)";
	//$me->arrPrint($man_sections);
	echo "<input type=button value='Add New' id=add_new />";
	$firstRun = true;
	$section = "";
	$heading = "";
	echo "<div id='accordion'>";
		foreach($man_sections as $rows) {
		foreach($rows as $row3) {
			if($section != $row3['section_name']){
				if(!$firstRun){
					echo'</div>';
				}
				echo'<h3 class=h3_acc>' . $row3['section_name'] . '</h3><div>';
			}
			if($heading != $row3['heading_name']){
				echo '<p><a href="instructions.php?mr='.$modref.'&mid='.$row3['mid'].'&hid='.$row3['hid'].'&sid='.$row3['sid'].'">' . $row3['heading_name'] . '</a></p>';
			}
			$firstRun = false;
			$section = $row3['section_name'];
			$heading = $row3['heading_name'];
		}
		}
	echo'</div>';
	echo'</div>';	


echo "<p>"; ASSIST_HELPER::goBack("index.php"); echo "</p>";
?>
<script type=text/javascript>
$(function() {
	$( "#accordion" ).accordion({
		collapsible: true,
		active: false,
		heightStyle: "content"
	}).css({"width":"50%"});
	$("h3.h3_acc").css({"font-family":"Verdana"});
	$("#add_new").click(function() {
		document.location.href = 'instructions.php?act=new&ml=<?php echo $modloc; ?>';
	});
});
</script>