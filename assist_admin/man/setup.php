<?php
include("header.php");
$setup = new MAN_SETUP();

$act = $_REQUEST['act'];

?>
<h1><a href=index.php class=breadcrumb>Help</a> >> Setup</h1>
<?php
$setup->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());



switch($act) {
case "mod":
	$objects = $setup->getModulesList();
	$fields = array(
		'module_name'=>"Default Module Name",
		'modloc'=>"Folder",
	);
	echo "<h2>Edit Modules</h2>";
	break;
case "sec":
	$objects = $setup->getSectionsList();
	$fields = array(
		'section_name'=>"Section / Category",
	);
	echo "<h2>Edit Sections</h2>";
	break;
case "head":
	$objects = $setup->getHeadingsList();
	$fields = array(
		'heading_name'=>"Heading / Activity",
	);
	echo "<h2>Edit Headings</h2>";
	break;
default:
	echo "<p>Error!!  Can't figure out what you want to do.</p>";
}
?>
<table class=list>
	<tr><th>Ref</th><?php
	foreach($fields as $key => $f) {
		echo "<th>".$f."</th>";
	}
	?><th>In Use</th><th></th></tr>
	<tr><td></td><?php
	foreach($fields as $key => $f) {
		echo "<td><input type=text size=30 value='' name=".$key." /></td>";
	}
	?><td class=center></td><td><input type=button value=Add id=btn_add /></td></tr>
	<?php
	foreach($objects as $obj) {
		echo "<tr><td class='center b' id=td_".$obj['id']."_id>".$obj['id']."</td>";
		foreach($fields as $key => $f) {
			echo "<td id=td_".$obj['id']."_".$key.">".$setup->decode($obj[$key])."</td>";
		}
		echo "
		<td>".($me->drawStatus(($obj['c']>0)))."</td>
		<td><input type=button value=Edit class=btn_edit ref=".$obj['id']." /></td>";
		echo "</tr>";
	}
	?>
</table>
<p><?php $setup->goBack("index.php"); ?></p>
<script type=text/javascript>
$(function() {
	$("#btn_add").click(function() {
		var dta = "act=add&tbl=<?php echo $act; ?>";
		$("input:text").each(function() {
			dta = dta+"&"+$(this).prop("name")+"="+AssistString.code($(this).val());
		});
		var result = AssistHelper.doAjax("controller.php",dta);
		document.location.href = "setup.php?act=<?php echo $act; ?>&r[]="+result[0]+"&r[]="+result[1];
	});
});
</script>
<div id=dlg_edit title=Edit><form name=frm_edit>
<input type=hidden name=act value='edit' />
<input type=hidden name=tbl value='<?php echo $act; ?>' />
<input type=hidden name=id id=id value='' />
	<table class=form>
		<tr>
			<th>Ref:</th>
			<td id=td_id></td>
		</tr>
		<?php 
		foreach($fields as $key => $f) {
			echo "<tr><th>".$f.":</th><td><input type=text size=30 name=".$key." id=".$key." /></td></tr>";
		}
		?>
	</table>
</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dlg_edit").dialog({
		modal: true,
		autoOpen: false,
		width: "400px",
		buttons: [ { text: "Save", class: "isubmit", 
			click: function() { 
				var dta = AssistForm.serialize($("form[name=frm_edit]"));
				//alert(dta);
				var result = AssistHelper.doAjax("controller.php",dta);
				document.location.href = "setup.php?act=<?php echo $act; ?>&r[]="+result[0]+"&r[]="+result[1];
			}
		}]
	});
	$(".btn_edit").click(function() {
		var i = $(this).attr("ref");
		$("#td_id").html(i);
		$("#dlg_edit input:text, #dlg_edit input:hidden#id").each(function() {
			var k = $(this).prop("id"); 
			var v = $("#td_"+i+"_"+k).html(); 
			$(this).val(v);
		});
		$("#dlg_edit").dialog("open");
	});
});
</script>
</body>
</html>