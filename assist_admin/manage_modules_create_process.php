<?php
$cc = strtoupper($_REQUEST['cmpcode']);
$client = strtolower($_REQUEST['cmpcode']);
$mod_location = strtoupper($_REQUEST['modloc']);
$mod_text = $_REQUEST['modtext'];
$mod_ref = strtoupper($_REQUEST['modref']);
$max_users = $_REQUEST['max_users']*1;
$db_blank_other = $db_other;
echo "<h2>Create new Module</h2>";
$verify = array('client'=>true,'module'=>true);
//Validate client
echo "<ol>
	<li>Verifying that $cc is a valid Assist Client ... ";
	//Validate assist_company record
	$verify['client'] = isset($company[$cc]) ? true : false;
	$is_dev = false;
	if(!$verify['client']) {
		$verify['client'] = isset($dev_company[$cc]) ? true : false;
		$is_dev = true;
$db_prefix = "pwcza";
$db_name = $db_prefix."_i".strtolower($cc);
$db_user = "ignittps_ignite4";
$db_pwd = "ign92054u";
$db_assist = $db_prefix."_ignite4u";
$db_other = $db_prefix."_i";
//$db_help = "ignittps_help";
		//$devcon = mysql_connect("localhost",$db_user,$db_pwd);
		//mysql_select_db("dev_ignite4u",$devcon);
		//$devrs = mysql_query($sql,$devcon);
		//while($row = mysql_fetch_array($devrs)) {
			//$dev_company[$row['cmpcode']] = $row;
		//}
		//unset($devrs);
		
	}
	if($verify['client']) {
		$cmp = $is_dev ? $dev_company[$cc] : $company[$cc];
		//validate that database exists
		$db_name = $db_other.$client;
		if(!in_array($db_name,$dbs)) { $verify['client'] = false; }
		if(!$verify['client']) {
			echo "<span class=idelete>failure</span> - client $cc does not appear to be a valid client.  Please contact the Ignite Assist Helpdesk for assistance.</li>"; die();
		} else {
			echo "<span class=isubmit>success".($is_dev ? " - DEV CLIENT" : "")."</span>.</li>";
		}
		//validate that module does not already exist
		echo "<li>Verifying that $mod_ref is not a duplicate module for $cc ... ";
		$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$mod_ref."'";
		$rs = getRS($sql);
		if(mysql_num_rows($rs)!=0) { $verify['module'] = false; }
		if(!$verify['module']) {
			echo "<span class=idelete>failure</span> - $cc already has access to a module with reference $mod_ref .  It cannot be created again.</li>"; die();
		} else {
			echo "<span class=isubmit>success</span>.</li>";
		}
	}
if(($verify['client']+$verify['module'])==2) {
	//Create tables
		$db_name_client = $db_other.$client;
		$db_name_blank = $db_blank_other."blank";
		echo "<li>Creating database structure...";
			//get blank tables
		$db_name = $db_name_blank;
		$tables = array();
		//get tables for specific modules
		switch($mod_location) {
		case "PPH":
			$db_ref = "assist_".$client."_policies";
			$sql = "SELECT TABLE_NAME FROM information_schema.TABLES
				WHERE TABLE_SCHEMA = '".$db_name_blank."'";
			$sql.= " AND (TABLE_NAME LIKE 'assist_blank_policies%')";
			break;
		default:
			$db_ref = "assist_".$client."_".strtolower($mod_ref);
			if($mod_location=="MASTER") {
				$sql = "SHOW TABLES LIKE 'assist_blank_".strtolower($mod_ref)."_%'";
			} else {
				$sql = "SHOW TABLES LIKE 'assist_blank_".strtolower($mod_location)."_%'";
			}
			break;
		}
		echo $sql;
		$rs = getRS($sql);
			while( ($tables[] = mysql_fetch_row($rs) ) || array_pop($tables));
		unset($rs);
		echo "<p>client: ".$db_name_client;
		echo "<p>blank: ".$db_name_blank;
		echo "<pre>"; print_r($tables); echo "</pre>";
		$db_name = $db_name_client;
		foreach($tables as $tbl) {
			$old_tbl = $tbl[0];
			$etbl = explode("_",$old_tbl); 
if($etbl[2]!=strtolower($mod_location)) {
	//do nothing because this is the wrong module
	//[JC] for some reason assist_blank_modloc_% also returns assist_blank_modloc%
} else {
			//remove assist_blank_modloc
			unset($etbl[0]); unset($etbl[1]); unset($etbl[2]);
			$new_tbl = strtolower($db_ref.(count($etbl)>0 ? "_".implode("_",$etbl) : ""));
			//delete existing table (in case of interrupted creation process)
			$sql = "DROP TABLE IF EXISTS `".$db_name_client."`.`".$new_tbl."`";
			$rs = getRS($sql);
			unset($rs);
			//create table
			$sql = "CREATE TABLE `".$db_name_client."`.`".$new_tbl."` LIKE `".$db_name_blank."`.`".$old_tbl."`";
//			echo "<p>".$sql;
			$rs = getRS($sql);
			unset($rs);
			//insert fixed system data
			$sql = "INSERT INTO `".$db_name_client."`.`".$new_tbl."` SELECT * FROM `".$db_name_blank."`.`".$old_tbl."`";
			db_insert($sql);
//			echo "<p>".$sql;
}
		}
	//Special activities per module
	switch($mod_location) {
	//If module is COMPL then set AUTO_INCREMENT values where applicable
	case "COMPL":
		$sql = "SELECT TABLE_NAME, AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$db_name_blank."' AND TABLE_NAME LIKE 'assist_blank_".strtolower($mod_location)."%' AND AUTO_INCREMENT > 900"; 
		$db_name = "information_schema";
		$rs = getRS($sql);
		$auto_tables = array();
		while($row = mysql_fetch_assoc($rs)) {
			$auto_tables[] = $row;
		}
		unset($rs);
		
		$db_name = $db_name_client;
		foreach($auto_tables as $at) {
			$old_tbl = $at['TABLE_NAME'];
			$new_auto = $at['AUTO_INCREMENT'];
			if(is_numeric($new_auto)) {
				$etbl = explode("_",$old_tbl); unset($etbl[0]); unset($etbl[1]); unset($etbl[2]);
				$new_tbl = strtolower($db_ref."_".implode("_",$etbl));
				$sql = "ALTER TABLE `".$db_name_client."`.`".$new_tbl."` AUTO_INCREMENT = ".$new_auto;
				$rs = getRS($sql);
				unset($rs);
			}
		}
		break;
	case "UD":
	case "PPH":
		$db_name = "information_schema";
		$sql = "SELECT TABLE_NAME FROM information_schema.TABLES
				WHERE TABLE_SCHEMA = '".$db_name_blank."'";
			if($mod_location=="UD") {
				$sql.= " AND (TABLE_NAME LIKE 'assist_ud_%')";
			} else {
				$sql.= " AND (TABLE_NAME LIKE 'assist_policies%')";
			}
		$rs = getRS($sql);
			while( ($tables[] = mysql_fetch_row($rs) ) || array_pop($tables));
		unset($rs);
		$db_name = $db_name_client;
		foreach($tables as $tbl) {
			$new_tbl = $tbl[0];
			$old_tbl = $new_tbl;
			//delete existing table (in case of interrupted creation process)
			$sql = "DROP TABLE IF EXISTS `".$db_name_client."`.`".$new_tbl."`";
			$rs = getRS($sql);
			unset($rs);
			//create table
			$sql = "CREATE TABLE `".$db_name_client."`.`".$new_tbl."` LIKE `".$db_name_blank."`.`".$old_tbl."`";
			$rs = getRS($sql);
			unset($rs);
			//insert fixed system data
			$sql = "INSERT INTO `".$db_name_client."`.`".$new_tbl."` SELECT * FROM `".$db_name_blank."`.`".$old_tbl."`";
			db_insert($sql);
		}
		break;
	}
		echo "<span class=isubmit>done</span>.</li>";
		echo "<li>Creating folder structure...";
			checkFolder($mod_ref);
		echo "<span class=isubmit>done</span>.</li>";
	
	//Create menu item
		echo "<li>Creating the menu line item... ";
		//create assist_menu_modules record
		$sql = "INSERT INTO assist_menu_modules (modtext,modcontents,modlocation,modref,modyn,modadminyn) VALUES ('".$mod_text."','Y','".strtoupper($mod_location)."','".strtoupper($mod_ref)."','Y','Y')";
		$modmenuid = db_insert($sql);
		//create assist_cmpcode_menu_modules record
		$sql = "INSERT INTO assist_".strtolower($client)."_menu_modules (modmenuid,modcustom,moduserallowed,modusercount) VALUES ($modmenuid,'N',".$max_users.",0)";
		$modid = db_insert($sql);
		echo "<span class=isubmit>done</span>.</li>";
		
		echo "</ol>";
		//update assist_client_module_activity_log
		$sql = "INSERT INTO assist_client_module_activity_log VALUES (null, '".strtoupper($cc)."', '".strtoupper($mod_ref)."', '".strtoupper($mod_location)."', '".strtoupper($_SESSION['cc'])."', '".$_SESSION['tid']."', '".$_SESSION['tkn']."','C',now(),'',1)";
		$logid = Adb_insert($sql);

		if($err == 0) {
			echo "<P class=b>Creation process completed successfully.</p>
			<script type=text/javascript> 
//document.location.href = 'manage_modules.php?r[]=ok&r[]=".urlencode("".$mod_text." has been successfully created on client ".$cc.".")."';
document.location.href = 'manage_modules.php?r[]=ok&r[]=".urlencode("".$mod_text." has been successfully created on client ".$cc.".")."&r[]=".$cc."';
			</script>";
		} else {
			echo "<P class=b>Creation process completed with errors.  Please read the messages above.</p>";
		}
		
} else {
	die("<span class=idelete>Error on verification.</span>");
}
?>