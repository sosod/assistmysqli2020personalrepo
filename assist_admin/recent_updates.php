<?php
require_once '../header.php';
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");

?>
<style type=text/css>
.inactive td {
	background-color: #dedede;
	color: #555555;
}
</style>
<?php


echo "
<input type=button value=Add id=btn_add style='float:right' />
<h1>Recent Updates</h1>";

Assist_Helper::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$fields = array(
	'uid'		=> "ID",
	'udate'		=> "Date & Time",
	'umodule'	=> "Module",
	'utext'		=> "Update Message",
	'active'	=> "Status"
);
$form_fields = array(
	'uid'		=> "ID",
	'umodule'	=> "Module",
	'utext'		=> "Update Message",
	'udate'		=> "Date & Time",
	'active'	=> "Status"
);
$date_fld = "udate";
$id_fld = "uid";
$status_fld = "active";

$sql = "SELECT * FROM assist_updates ORDER BY udate DESC, uid DESC";
$updates = $db->mysql_fetch_all($sql);

echo "
<table>
	<tr>";
foreach($fields as $fld => $head) {
	echo "<th>".$head."</th>";
}	
echo "
		<th></th>
	</tr>";
foreach($updates as $u) {
	$status = $u[$status_fld];
	echo "<tr class='".($status==1 ? "active" : "inactive")."'>";
	foreach($fields as $fld => $head) {
		$v = $u[$fld];
		echo "<td ".( ($fld=="udate" || $fld=="umodule") ? "class=center width=100" : "").">";
		switch($fld) {
			case $status_fld:
				if($v==1) {
					echo "Display";
				} else {
					echo "Hidden";
				}
				break;
			case $date_fld:
				echo date("d-M-Y H:i",strtotime($v));
				break;
			default:
				echo str_ireplace(chr(10),"<br />",$v);
		}
		echo "</td>";
	}
	echo "<td class='center buttons'><input type=button value=Edit class=btn_edit ref=".$u[$id_fld]." /> ";
	if($status==1) {
		echo "<input type=button value='Hide' class=btn_hide ref=".$u[$id_fld]." />";
	} else {
		echo "<input type=button value='Restore' class=btn_restore ref=".$u[$id_fld]." />";
	}
	echo "</td></tr>";
}
echo "
</table>";



$form = "
<table class=form>";
foreach($form_fields as $fld => $f) {
	$form.="<tr><th>".$f.":</th><td>";
	switch($fld) {
		case $id_fld: $v = "<label for=uid id=lbl_uid>N/A</label>"; break;
		case $date_fld: $v = "<input type=text value='".date("Y-m-d H:i")."' size=20 class=datepicker name=".$date_fld." id=name=".$date_fld." />"; break;
		case $status_fld: $v = "<select name=".$status_fld." id=".$status_fld."><option value=1 selected>Display</option><option value=0>Hidden</option></select>"; break;
		case "utext":	$v = "<textarea rows=5 cols=60 name=".$fld." id=".$fld."></textarea>"; break;
		default:
			$v = "<input type=text value='' name=".$fld." id=".$fld." class=focus />";
			break;
	}
	$form.=$v."</td></tr>";
}
$form.= "
</table>
";

$add_form = "
<div id=dlg_add title='Add New Update'>
<h1>Add New Update</h1>
<form name=frm_add>
<input type=hidden name=action value=ADD />
".$form
."</form>
</div>
";
echo $add_form;

$edit_form = "
<div id=dlg_edit title='Edit Recent Update'>
<h1>Edit Recent Update</h1>
<form name=frm_edit>
<input type=hidden name=action value=EDIT />
<input type=hidden name=uid id=uid value='' />
".$form
."</form>
</div>
";
echo $edit_form;
?>
<script type=text/javascript>
$(function() {
	$("td.buttons").each(function() {
		$(this).children("input:button:first").next().css({"margin-top":"5px"});
	});
	$(".datepicker").datetimepicker({
						timeFormat: 'hh:mm',
						dateFormat: 'yy-mm-dd',
						showOn: 'both',
						buttonImage: '/library/jquery/css/calendar.gif',
						buttonImageOnly: true
		});
/*	$("#dlg_add").dialog({
			modal: true,
			autoOpen: false,
			width: "500px",
			buttons: {
				"Add": function() {
					var dta = AssistForm.serialize($("form[name=frm_add]"));
					//alert(dta);
					var r = AssistHelper.doAjax("recent_updates_process.php",dta);
					//alert(r[1]);
					//console.log(r);
					document.location.href = 'recent_updates.php?r[]='+r[0]+'&r[]='+r[1];
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});*/
	$("#dlg_add, #dlg_edit").dialog({
			modal: true,
			autoOpen: false,
			width: "500px"
		});
	$("#dlg_add").dialog("option", "buttons", {
				"Add": function() {
					var dta = AssistForm.serialize($("form[name=frm_add]"));
					//alert(dta);
					var r = AssistHelper.doAjax("recent_updates_process.php",dta);
					//alert(r[1]);
					//console.log(r);
					document.location.href = 'recent_updates.php?r[]='+r[0]+'&r[]='+r[1];
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		);
	$("#dlg_edit").dialog("option", "buttons", {
				"Save Changes": function() {
					var dta = AssistForm.serialize($("form[name=frm_edit]"));
					//alert(dta);
					var r = AssistHelper.doAjax("recent_updates_process.php",dta);
					//alert(r[1]);
					//console.log(r);
					document.location.href = 'recent_updates.php?r[]='+r[0]+'&r[]='+r[1];
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		);
	$("div[role=dialog] button:contains('Add'), div[role=dialog] button:contains('Save Changes')").css({"color": "#009900","border-color":"#009900"});
	$("#btn_add").button().css({"color":"#009900"}).click(function() {
		$("#dlg_add").dialog("open");
	});
	$(".btn_edit").click(function() {
		var i = $(this).attr("ref");
		var u = AssistHelper.doAjax("recent_updates_process.php","action=GET&id="+i);
		$.each(u,function(index,val) {
			$("#dlg_edit #"+index).val(val);
		});
		$("#dlg_edit .datepicker").val(u['udate']);
		$("#dlg_edit #lbl_uid").html(u['uid']);
		$("#dlg_edit").dialog("open");
	});
	$(".btn_hide").click(function() {
		var i = $(this).attr("ref");
		if(confirm("Are you sure you wish to hide Update #"+i+"?\n\nIt will no longer display on the login page.")==true) {
			var r = AssistHelper.doAjax("recent_updates_process.php","action=HIDE&id="+i);
			document.location.href = 'recent_updates.php?r[]='+r[0]+'&r[]='+r[1];
		}
	});
	$(".btn_restore").click(function() {
		var i = $(this).attr("ref");
		if(confirm("Are you sure you wish to restore Update #"+i+"?\n\nIt will then display on the login page.")==true) {
			var r = AssistHelper.doAjax("recent_updates_process.php","action=RESTORE&id="+i);
			document.location.href = 'recent_updates.php?r[]='+r[0]+'&r[]='+r[1];
		}
	});
});
</script>
</body>
</html>