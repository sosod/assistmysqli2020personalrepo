<?php
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");

$act = $_REQUEST['action'];
unset($_REQUEST['action']);
$result = array("error","Sorry, I can't identify the action you want to complete.");

switch($act) {
case "ADD":
	$insert_data = array();
	foreach($_REQUEST as $fld=>$v) {
		$insert_data[] = $fld." = '".$v."'";
	}
	$sql = "INSERT INTO assist_updates SET ".implode(", ",$insert_data);
	//$db->triggerError($sql);
	$i = $db->db_insert($sql);
	$db2 = new ASSIST_DB_REMOTE("master");
	$i2 = $db2->db_insert($sql);
	//$db->db_insert($sql);
	//$result[1] = $sql;
	$result = array("ok","New Update ".$i." (Remote: ".$i.") added successfully.");
	break;
case "GET":
	$sql = "SELECT * FROM assist_updates WHERE uid = ".$_REQUEST['id']." LIMIT 1";
	$result = $db->mysql_fetch_one($sql);
	if(strlen($result['udate'])>16) {
		$result['udate'] = substr($result['udate'],0,16);
	}
	break;
case "EDIT":
	$i = $_REQUEST['uid'];
	unset($_REQUEST['uid']);

	$update_data = array();
	foreach($_REQUEST as $fld=>$v) {
		$update_data[] = $fld." = '".$v."'";
	}
	$sql = "UPDATE assist_updates SET ".implode(", ",$update_data)." WHERE uid = ".$i;
	$db->db_update($sql);

	$result = array("ok","Recent Update ".$i." edited successfully.");
	break;
case "HIDE":
	$i = $_REQUEST['id'];
	$sql = "UPDATE assist_updates SET active = 0 WHERE uid = ".$i;
	$db->db_update($sql);
	$result = array("ok","Recent Update ".$i." hidden successfully.");
	break;
case "RESTORE":
	$i = $_REQUEST['id'];
	$sql = "UPDATE assist_updates SET active = 1 WHERE uid = ".$i;
	$db->db_update($sql);
	$result = array("ok","Recent Update ".$i." restored successfully.");
	break;
}

$result[1] = ASSIST_HELPER::code($result[1]);

echo json_encode($result);




?>