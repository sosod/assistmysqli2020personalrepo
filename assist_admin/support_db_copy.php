<?php
//AA-488 JC 12 Oct 2020 - Add option to copy DBs for support purposes
require_once("../module/autoloader.php");
$helper = new ASSIST_MODULE_HELPER();
$tkid = $helper->getUserID();
$cmpcode = $helper->getCmpCode();
$mdb = new ASSIST_DB("master");

ASSIST_HELPER::echoPageHeader("1.10.0", array(), array("/assist_jquery.css?".time(), "/assist3.css?".time()));

/** Variable Setup */
//Databases which can be copied into
$support_databases = $mdb->getAITSupportDatabases();

/** Get current Companies */
$companies = array();
$exclusion_databases = $mdb->getExclusionDatabases();
$exclusion_databases[] = "AIT0001";//add Action iT base database so that it can't be used for support database copying
$sql = "SELECT cmpcode, CONCAT(cmpname,' (',cmpcode,')') as cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode NOT IN ('".implode("','",$exclusion_databases)."') AND cmpcode NOT IN ('".implode("','",array_keys($support_databases))."') ORDER BY cmpname, cmpcode";
$companies = $mdb->mysql_fetch_value_by_id($sql,"cmpcode","cmpname");
/** Get active DBs */
$dbs = array();
$db_prefix = $mdb->getDBPrefix();
$sql = "SHOW DATABASES LIKE '".$db_prefix."%'";
$dbs_raw = $mdb->mysql_fetch_all($sql);
foreach($dbs_raw as $i=>$x) {
	foreach($x as $z => $y) {
		$dbs[] = $y;
	}
}

	//User Interface
echo "<h1>Copy Client Database for Support</h1>";
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

	//Container table
	?>
<table style="width:100%;margin: 0 auto; margin-top:50px; border:0px solid #fe9900" id=tbl_container>
	<tr>
		<td style='text-align: center; border:0px solid #ffffff' class=container>
			<table style="margin:0 auto" id="tbl_action">
				<tr>
					<td class="b center" width="50%">Source Database (Client)</td>
					<td class="b center" width="50%">Destination Database (Support)</td>
				</tr>
				<tr>
					<td class="center" width="50%"><select id="src_db">
							<option selected value="X">--- COMPANIES BY NAME ---</option><?php
							foreach($companies as $cc => $cmpname) {
								if(in_array(strtolower($db_prefix.$cc),$dbs)) {
									echo "<option value='".$cc."'>".$cmpname."</option>";
								}
							}
							?><option value="X">--- COMPANIES BY CODE ---</option><?php
							ksort($companies);
							foreach($companies as $cc => $cmpname) {
								if(in_array(strtolower($db_prefix.$cc),$dbs)) {
									echo "<option value='".$cc."'>".$cmpname."</option>";
								}
							}
							?></select></td>
					<td class="center" width="50%"><select id="dest_db"><?php
							$selected = false;
							foreach($support_databases as $cc => $database) {
								echo "<option ".(!$selected?"selected":"")." value='".$cc."'>".$database."</option>";
							}
							?></select></td>
				</tr>
				<tr>
					<td colspan="2" class="center"><button id="btn_copy">Copy</button></td>
				</tr>
				<tr>
					<td colspan="2" class="right"><button id="btn_export">Export</button>&nbsp;<button id="btn_export_helpdesk">Export Helpdesk</button></td>
				</tr>
			</table>
			<?php
			ASSIST_HELPER::displayResult(array("info","Pressing the Copy button will replace the existing Support (Destination) database with the selected Client (Source) database.  All user passwords will be reset to Assist123 and email addresses will be changed to developers@actionassist.co.za."));
			ASSIST_HELPER::displayResult(array("info","The Copy process can take a while, especially for larger clients, and your browser might indicate that the webpage has stopped/frozen.  Please let the process run for at least 15 minutes but if you are still waiting after that then contact Janet Currie (but leave this page open!)"));
			ASSIST_HELPER::displayResult(array("info","If you need an export of a database for local testing: (1) Copy the client DB to the relevant Support DB; (2) Click the Export button (with the relevant Support DB selected); (3) Go to <a href='https://assist.action4u.co.za/temp/SQL/index.php' target=_blank>https://assist.action4u.co.za/temp/SQL/index.php</a> to download the file;"))
			?>

<h2>Recent Copy Log</h2>
<table id="tbl_log" width=100%>
	<tr>
		<th>Support Database</th>
		<th>Source Database</th>
		<th>Copied On</th>
	</tr>
	<?php
	foreach($support_databases as $cc => $database) {
		$sql = "SELECT * FROM assist_support_copy_log WHERE dest_cc = '$cc' ORDER BY id DESC LIMIT 1";
		$row = $mdb->mysql_fetch_one($sql);
		if(isset($row['src_cc'])) {
			$src = isset($companies[$row['src_cc']]) ? $companies[$row['src_cc']] : $row['src_cc'];
		} else { $src = "N/A"; }
		?>
		<tr>
			<td class="b"><?php echo $database; ?></td>
			<td><?php echo $src; ?></td>
			<td><?php echo (isset($row['copy_date'])?$row['copy_date']:"N/A"); ?></td>
		</tr>
		<?php
	}
	?>
</table>
		</td>
	</tr>
</table>
<div id="dlg_confirm">
	<h3>Confirm</h3>
	<p>Please confirm that you want to copy:</p>
	<p class="center b" id="p_src">SOURCE</p>
	<p class="center">to</p>
	<p class="center b" id="p_dest">DESTINATION</p>
</div>
<script type="text/javascript">
	$(function() {
		$("#dlg_confirm").dialog({
			modal:true,
			autoOpen:false,
			buttons:[
					{ text: "Confirm & Continue", click: function() {
						var src_db = $("#src_db").val();
						var dest_db = $("#dest_db").val();
						var url = "support_db_copy_process.php?src="+src_db+"&dest="+dest_db;
						$( this ).dialog( "close" );
						AssistHelper.processing();
						document.location.href = url;
						//submitForm();
					}, class: 'ui-state-ok' },
					{ text: "Cancel & Start Over", click: function() {
						$( this ).dialog( "close" );
					}, class: 'ui-state-error' }
				]
		});
		$("#tbl_container").width($("#tbl_action").width()+30);
		$("#btn_copy").button({icons:{primary:"ui-icon-newwin"}}).removeClass("ui-state-default").addClass("ui-button-bold-green")
		.hover(function() {
			$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
		},function() {
			$(this).addClass("ui-button-bold-green").removeClass("ui-button-bold-orange");
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			if($("#src_db").val()=="X") {
				AssistHelper.finishedProcessing("error","Please select the Source Database.");
			} else {

				var src_name = $("#src_db option:selected").text();
				$("#p_src").html(src_name);
				var dest_name = $("#dest_db option:selected").text();
				$("#p_dest").html(dest_name);
				AssistHelper.closeProcessing();
				AssistHelper.hideDialogTitlebar("id","dlg_confirm");
				$("#dlg_confirm").dialog("open");
			}
		});
		$("#dest_db option:first").prop("selected","selected");
		$("#tbl_action td").css("padding","10px");
		$("#btn_export").button({icons:{primary:"ui-icon-arrowreturn-1-n"}}).removeClass("ui-state-default").addClass("ui-button-state-blue")
		.hover(function() {
			$(this).removeClass("ui-button-state-blue").addClass("ui-button-state-orange");
		},function() {
			$(this).addClass("ui-button-state-blue").removeClass("ui-button-state-orange");
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			//AssistHelper.finishedProcessing("error","Sorry, not working yet");
			if($("#dest_db").val()=="X" || $("#dest_db").val().length==0) {
				AssistHelper.finishedProcessing("error","Please select the Support Database.");
			} else {
				document.location.href = "support_db_export.php?cc="+$("#dest_db").val();
			}
		});
		$("#btn_export_helpdesk").button({icons:{primary:"ui-icon-arrowreturn-1-s"}}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.hover(function() {
			$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
		},function() {
			$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = "support_db_export.php?cc=helpdesk";
		});
	});
</script>