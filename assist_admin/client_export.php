<?php
require_once("../module/autoloader.php");
$attachment_modules = array("SDBP6","IKPI2","SDBP5B","SDBP5");
//$attachment_modules = array("SDBP5B");
$definitely_not_attachment_modules = array("PM3","PM4","PM5","PM6","PMSPS","JAL1","JAL2","MSCOA1","TD"		,"IDP3","EMP1","AQA","PDP");



	$me = new ASSIST_HELPER();
	ASSIST_HELPER::echoPageHeader("1.10.0", array(), array("/assist_jquery.css?".time(), "/assist3.css?".time()));
if(!isset($_REQUEST['cc']) || strtoupper($_REQUEST['cc'])=="X") {
	$mdb = new ASSIST_DB("master");
	$dbs = $mdb->getListOfAvailableDatabases();
	$dbs_key = array_flip($dbs);
	/* GET COMPANY LIST */
	$company = array();
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus IN ('S','Y') AND cmpcode <> 'BLANK' ORDER BY cmpcode";
	} else {
		$cmpcode = $_SESSION['cc'];
		$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus IN ('S','Y') AND cmpreseller = '".strtoupper($cmpcode)."' ORDER BY cmpcode";
	}
	$rows = $mdb->mysql_fetch_all($sql);
	foreach($rows as $row) {
		$company[$row['cmpcode']] = $row;
	}
	unset($rows);


	echo "<h1>Client Data Export</h1>
<form name='frm_client' action='client_export.php' method='post'>
		<p class='b'>Choose Client: <select name='cc'><option value='X'>--- SELECT ---</option>
		";
	foreach($company as $cc => $cmp) {
		if(in_array($mdb->getDBPrefix().strtolower($cc),$dbs)) {
			echo "<option value='".strtolower($cc)."'>".$cmp['cmpname']."</option>";
			unset($dbs_key[$mdb->getDBPrefix().strtolower($cc)]);
		}
	}
	//add back any existing DBs
	if(count($dbs_key)>0) {
		$db_prefix = $mdb->getDBPrefix();
		foreach($dbs_key as $test_db => $key) {
			if(strlen($test_db)==(strlen($db_prefix)+7)) {
				$cc = substr($test_db,-7);
				echo "<option value='".strtolower($cc)."'>".$test_db." [$cc]"."</option>";
			}
		}
	}
	echo "
</select> <input type='submit' value='Go'></p></form>";
	die();
}











/** ****************************************************************************************** **/


$client_cmpcode = $_REQUEST['cc'];
$me = new ASSIST_MODULE_HELPER("client",$client_cmpcode,false);

echo "<h1>Client Data Export (".strtoupper($client_cmpcode).")</h1>";

//get menu modules
$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modtext";
$menu = $me->mysql_fetch_all_by_id($sql,"modref");
$modules_not_processed = array();
//loop through and check for attachment modules
foreach($menu as $modref => $mod) {
	$modloc = $mod['modlocation'];
	if(in_array($modloc,$attachment_modules)) {
		$display_me = true;
		switch($modloc) {
			/* ******************************************************************************* */
			case "SDBP6":
			case "IKPI2":
				echo "<h2>".$mod['modtext']."</h2>";
				if($modloc != "IKPI2") {
					//Process TL
					//not needed for HES
//				echo "<h3>Top Layer</h3>";

					//Display headings for D / IP then process
					echo "<h3>Departmental</h3>";
				} else {
					echo "<h3>Individual Plan</h3>";
				}
				$me->setDBRef($modref, $client_cmpcode);
				$headings = array(
					'sdbip_name' => ($modloc == "IKPI2" ? "Plan" : "SDBIP")." Name",
					'kpi_id' => "System Ref",
					'kpi_ref' => "Internal Ref",
					'kpi_name' => "KPI Name",
					'end_date' => "Time Period",
					'attachment' => "Attachment Details",
				);
				$attachment_field = "kpir_attachment";
				$time_period_fld = "end_date";
				$sql = "SELECT sdbip_name, CONCAT('".$modref."/',kpi_sdbip_id) as sdbip_id,  CONCAT('D',kpi_id) as kpi_id, kpi_ref, kpi_name, kpir_time_id, end_date, kpir_attachment
					FROM `".$me->getDBRef()."_kpi` k
					INNER JOIN ".$me->getDBRef()."_kpi_results kr
					ON kpi_id = kpir_kpi_id 
					INNER JOIN ".$me->getDBRef()."_sdbip s
					ON kpi_sdbip_id = sdbip_id
					INNER JOIN ".$me->getDBRef()."_setup_time st
					ON kpir_time_id = st.id
					WHERE LENGTH(kpir_attachment)>6
					AND (kpi_status & 2) = 2
					ORDER BY kpi_sdbip_id, kpi_id, kpir_time_id";
				$rows = $me->mysql_fetch_all($sql);

				foreach($rows as $i => $row) {
					$attachment = unserialize(str_replace("|", '"', $row[$attachment_field]));
					if(count($attachment)>0) {
						$rows[$i]['attachment'] = $attachment;
					} else {
						unset($rows[$i]);
					}
				}
				$attachment_field = "attachment";

				break;
			/* ******************************************************************************* */
			case "SDBP5":
			case "SDBP5B":
			case "IKPI1":
				echo "<h2>".$mod['modtext']."</h2>";
				$me->setDBRef($modref, $client_cmpcode);
				$rows = array();
				$headings = array(
					'kpi_id' => "System Ref",
					'kpi_idpref' => "Internal Ref",
					'kpi_value' => "KPI Name",
					'end_date' => "Time Period",
					'attachment' => "Attachment Details",
				);
				$attachment_field = "kr_attachment";
				$time_period_fld = "end_date";
				$sql = "SELECT CONCAT('D',kpi_id) as kpi_id, kpi_idpref, kpi_value, kr_timeid, end_date, kr_attachment
					FROM `".$me->getDBRef()."_kpi` k
					INNER JOIN ".$me->getDBRef()."_kpi_results kr
					ON kpi_id = kr_kpiid 
					INNER JOIN ".$me->getDBRef()."_list_time st
					ON kr_timeid = st.id
					WHERE LENGTH(kr_attachment)>6
					AND (kpi_active = 1)
					ORDER BY kpi_id, kr_timeid";
				$rows = $me->mysql_fetch_all($sql);
				foreach($rows as $i => $row) {
					$attachment = null;
					$attachment = @unserialize($row[$attachment_field]);
					if($attachment===null) {
						echo "<p class=red>".$row[$attachment_field]."</p>";
					}
					if(count($attachment['attach'])>0) {
						$rows[$i]['attachment'] = $attachment['attach'];
					} else {
						unset($rows[$i]);
					}
				}
				$attachment_field = "attachment";
				break;
			/* ******************************************************************************* */
			default:
				$modules_not_processed[] = $modref;
				$display_me = false;
				$rows = array();
				break;
		}
		if($display_me && count($rows)>0) {
			echo "<table>
<tr>
";
			foreach($headings as $fld => $h) {
				if($fld == $attachment_field) {
					echo "<td class='b center'>Client Filename</td>";
					echo "<td class='b center'>Assist Filename</td>";
				} else {
					echo "<td class='b center'>".$h."</td>";
				}
			}
			echo "
</tr>";
			foreach($rows as $row) {
				$extraoutput = "";
				$attachment = $row[$attachment_field];
				if(count($attachment) > 0) {
					echo "<tr>";
					foreach($headings as $fld => $h) {
						echo "<td rowspan='".($fld == $attachment_field ? "1" : count($attachment))."'>";
						if($fld == $attachment_field) {
							$c = 0;
							foreach($attachment as $attach) {
								$e = $attach['original_filename']."</td><td>".$attach['system_filename']."</td>";
								if($c == 0) {
									echo $e;
								} else {
									$extraoutput .= "<tr><td>".$e."</tr>";
								}
								$c++;
							}
						} elseif($fld == $time_period_fld) {
							echo date("F Y", strtotime($row[$fld]));
						} else {
							echo $row[$fld];
						}
						echo "</td>";
					}
					echo "</tr>";
					echo $extraoutput;
				}
			}
			echo "</table>";
		}
	} else {
		$modules_not_processed[] = $modref;
	}
}
if(count($modules_not_processed)>0) {
	echo "<hr /><p class='b'>Modules not processed:</p><ul>";
	foreach($modules_not_processed as $mr) {
		if(!in_array($menu[$mr]['modlocation'],$definitely_not_attachment_modules)) {
			echo "<li>".$menu[$mr]['modtext']." [$mr / ".$menu[$mr]['modlocation']."]</li>";
		}
	}
	echo "</ul>";
}

?>