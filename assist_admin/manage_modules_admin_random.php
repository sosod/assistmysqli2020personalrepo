<?php
//error_reporting(-1);
$hex = array(
	'A'=>'41',
	'B'=>'42',
	'C'=>'43',
	'D'=>'44',
	'E'=>'45',
	'F'=>'46',
	'G'=>'47',
	'H'=>'48',
	'I'=>'49',
	'J'=>'4a',
	'K'=>'4b',
	'L'=>'4c',
	'M'=>'4d',
	'N'=>'4e',
	'O'=>'4f',
	'P'=>'50',
	'Q'=>'51',
	'R'=>'52',
	'S'=>'53',
	'T'=>'54',
	'U'=>'55',
	'V'=>'56',
	'W'=>'57',
	'X'=>'58',
	'Y'=>'59',
	'Z'=>'5a',
	'a'=>'61',
	'b'=>'62',
	'c'=>'63',
	'd'=>'64',
	'e'=>'65',
	'f'=>'66',
	'g'=>'67',
	'h'=>'68',
	'i'=>'69',
	'j'=>'6a',
	'k'=>'6b',
	'l'=>'6c',
	'm'=>'6d',
	'n'=>'6e',
	'o'=>'6f',
	'p'=>'70',
	'q'=>'71',
	'r'=>'72',
	's'=>'73',
	't'=>'74',
	'u'=>'75',
	'v'=>'76',
	'w'=>'77',
	'x'=>'78',
	'y'=>'79',
	'z'=>'7a',
	'0'=>'30',
	'1'=>'31',
	'2'=>'32',
	'3'=>'33',
	'4'=>'34',
	'5'=>'35',
	'6'=>'36',
	'7'=>'37',
	'8'=>'38',
	'9'=>'39'
);
if(isset($_REQUEST['cc'])) {
	$txt = $_REQUEST['cc'];
	$val = array();
	for($t=0;$t<strlen($txt);$t++) {
		$case = rand(0,1);
		if($case) {
			$v = strtoupper(substr($txt,$t,1));
		} else {
			$v = strtolower(substr($txt,$t,1));
		}
		$val[] = $v;
	}
	for($i=0;$i<4;$i++) {
		$val[] = rand(0,9);
	}
$result = array(0=>"ok",'text'=>implode("",$val),'hex'=>"");
	foreach($val as $v) {
		$result['hex'].=$hex[$v];
	}
	echo json_encode($result);
} else {
	$result = array(0=>"error",'text'=>"No CC given");
	echo json_encode($result);
}
?>