<?php
require_once("../module/autoloader.php");
$helper = new ASSIST_MODULE_HELPER();
$mdb = new ASSIST_DB("master");

ASSIST_HELPER::echoPageHeader("1.10.0", array(), array("/assist_jquery.css?".time(), "/assist3.css?".time()));


?>
<h1>Server Database Activity Report - User Activity Log</h1>
<p>This report displays date/time of the last entry in the user activity log which records when a user logs in or out or accesses a module on Ignite Assist. [Log added on 2 Oct 2011].</p>
<?php
$sql = "SELECT cmpcode, cmpname, cmpstatus FROM assist_company";
$company = $mdb->mysql_fetch_all_by_id($sql,"cmpcode");

//get array of databases
$dbs = array();
$db_prefix = $mdb->getDBPrefix();
$db_other = $db_prefix;
$db_assist = $mdb->getMasterName();
$sql = "SHOW DATABASES LIKE '".$db_prefix."%'";
$dbs_raw = $mdb->mysql_fetch_all($sql);
foreach($dbs_raw as $i=>$x) {
	foreach($x as $z => $y) {
		$dbs[] = $y;
	}
}

$db_array = array();
$k = 0;
foreach($dbs as $db) {
	if($db!=$db_assist && substr($db,0,strlen($db_other))==$db_other) {
		$cc = strtoupper(substr($db,strlen($db_other)));
		if(isset($company[$cc])) {
			$cn = $company[$cc]['cmpname'];
			$cs = $company[$cc]['cmpstatus'];
		} else {
			$cn = "Invalid";
			$cs = "N/A";
		}
		switch($cs) {
		case "Y": $class = "isubmit"; $cs = "Available"; break;
		case "N": $class = "idelete"; $cs = "Closed"; break;
		case "N/A":
		default: $class = "iinform"; break;
		}
		//$db_name = $db_other.strtolower($cc);
		$cdb = new ASSIST_DB("client",$cc);
		$sql = "SHOW TABLES LIKE 'assist_".strtolower($cc)."_timekeep_activity'";
		$test = $cdb->mysql_fetch_all($sql);
		if(count($test) > 0) {
			$sql = "SELECT * FROM assist_".strtolower($cc)."_timekeep_activity ORDER BY id DESC LIMIT 1";
			$row = $cdb->mysql_fetch_all($sql);
			if(!isset($row[0]) || !isset($row[0]['date']) || $row[0]['date'] < 1) {
				$sql = "SHOW TABLES LIKE 'assist_".strtolower($cc)."_log'";
				$test2 = $cdb->mysql_fetch_all($sql);
				if(count($test2) > 0) {
					$sql = "SELECT * FROM assist_".strtolower($cc)."_log ORDER BY id DESC LIMIT 1";
					$row = $cdb->mysql_fetch_all($sql);
					if(!isset($row[0]) || !isset($row[0]['date']) || $row[0]['date'] < 1) {
						$activity = "No activity found.";
						$key = $k; $k++;
					} else {
						$activity = date("d M Y H:i:s",$row[0]['date'])." [old log]";
						$key = $row[0]['date'];
					}
				} else {
					$activity = "No activity found.";
					$key = $k; $k++;
				}
			} else {
				$activity = date("d M Y H:i:s",strtotime($row[0]['date']));
				$key = strtotime($row[0]['date']);
			}
		} else {
			$sql = "SHOW TABLES LIKE 'assist_".strtolower($cc)."_log'";
			$test = $cdb->mysql_fetch_all($sql);
			if(count($test) > 0) {
				$sql = "SELECT * FROM assist_".strtolower($cc)."_log ORDER BY id DESC LIMIT 1";
				$row = $cdb->mysql_fetch_all($sql);
				if(!isset($row[0]) || !isset($row[0]['date']) || $row[0]['date'] < 1) {
					$activity = "No activity found.";
					$key = $k; $k++;
				} else {
					$activity = date("d M Y H:i:s",$row[0]['date'])." [old log]";
					$key = $row[0]['date'];
				}
			} else {
				$activity = "Error.  User Activity Log table not found.";
				$key = $k; $k++;
			}
		}
		$db_array[$key] = array('cc'=>$cc,'cn'=>$cn,'cs'=>$cs,'activity'=>$activity,'class'=>$class);
	}
}
?>
<table>
<thead>
	<tr>
		<th>Database</th>
		<th>Company</th>
		<th>Company Status</th>
		<th>Last Recorded Activity</th>
	</tr>
</thead>
<tbody>
<?php

ksort($db_array);
foreach($db_array as $d) {
		echo "<tr>
				<td class=b>".$d['cc']."</td>
				<td>".$d['cn']."</td>
				<td class=\"".$d['class']." center\" >".$d['cs']."</td>
				<td>".$d['activity']."</td>
			</tr>";
}
?>
</tbody>
</table>
</body>
</html>