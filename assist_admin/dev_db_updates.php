<?php
require_once '../header.php';
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");


?>
<h1>Ignite Assist Admin >> Module Management</h1>
<p><span class=iinform>Warning:</span> Only 1 SQL script can be submitted at a time.</p>
<?php
displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

	$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modtext";
	$modules = mysql_fetch_all($sql,"O","blank");
	$modules[] = array('modref'=>"AQA",'modlocation'=>"AQA",'modtext'=>"IAS Audit Query Assist");
	$modules[] = array('modref'=>"Master Setup",'modlocation'=>"S",'modtext'=>"Master Setup");
	$modules[] = array('modref'=>"GEN",'modlocation'=>"GEN",'modtext'=>"General / Structural");


	echo "<h2>Update Development Databases</h2>
	<form name=frm_dev action=dev_db_updates_process.php method=post>
	<input type=hidden name=act value='UPDATE_DEV' />
	<table id=tbl_dev class=form>
	<tr><th>Module:</th><td><select name=modloc id=modloc><option value=X selected>--- SELECT MODULE ---</option>";
	foreach($modules as $m) {
		echo "<option value=".$m['modlocation']." r=".$m['modref'].">".$m['modtext']."</option>";
	}
	echo "
	</select></td></tr>
	<tr><th>Table:</th><td><input type=text value='' name=db_table maxlength=100 size=50 /><br /> <span class=i>Don't include the company code or module ref e.g. 'assist_demo001_qry_query_register' becomes 'query_register'</span></td></tr>
	<tr><th>SQL:</th><td><textarea rows=10 cols=100 name=act_sql></textarea><br /><span class=i>Replace assist_cmpcode_modref with |#| (pipe-hash-pipe) in the table names e.g. 'UPDATE assist_demo001_qry_query_register SET...' becomes 'UPDATE |#|_query_register SET...'<br />Where there is no modref in the table name (e.g. assist_cmpcode_timekeep) replace the cmpcode with |cc| (pipe-cc-pipe) e.g. FROM assist_demo001_timekeep becomes FROM assist_|cc|_timekeep.</span></td></tr>
	<tr><td colspan=2 class=center><input type=button value='Update' class=isubmit /></td></tr>
	</table>
	</form>
	<script type=text/javascript>
	$(function() {
		$('form[name=frm_dev] input:button.isubmit').click(function() {
			var form = 'form[name=frm_dev]';
			var valid = true;
			var txt;
			$(form+' select, '+form+' input:text, '+form+' textarea').each(function() {
				$(this).removeClass('required');
				txt = $(this).val();
				if(txt.length==0) { txt = $(this).text(); }
				if(txt.length==0 || txt=='X' || txt=='0' || txt==0) {
					valid = false;
					$(this).addClass('required');
				}
			});
			if(!valid) {
				alert('Missing data.  All fields required.');
			} else {
				var me = $(form+' #modloc option:selected');
				if(confirm('You are about to update the \\n    '+$(form+' input:text[name=db_table]').val()+'\\ntable associated with \\n    '+me.text()+'.\\n\\nAre you sure you wish to continue?')==true) {
					$(form).submit();
				}
			}
		});
	});
	</script>

	";
echo "<h2>SQL Permitted</h2>
<h3>Record changes</h3>
<ul>
	<li>UPDATE</li>
	<li>INSERT</li>
	<li>DELETE</li>
</ul>
<h3>Structural changes</h3>
<ul>
	<li>CREATE TABLE</li>
	<li>ALTER TABLE ... ADD</li>
	<li>ALTER TABLE ... CHANGE</li>
	<li>ALTER TABLE ... DROP</li>
	<li>DROP TABLE</li>
</ul>";








?>




</body>

</html>
