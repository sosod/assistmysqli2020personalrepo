<?php
require_once '../header.php';
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");
$sql = "SELECT mod_loc, mod_generic_name FROM man_modules";
$generic_modules = $db->mysql_fetch_value_by_id($sql,"mod_loc","mod_generic_name");
$sql = "SELECT cd_demo_cmpcode as cmpcode FROM assist_company_demos WHERE cd_status = 2";
$company_demos = $db->mysql_fetch_value_by_id($sql,"cmpcode","cmpcode");
$support_databases = $db->getAITSupportDatabases();
//echo $db->getDBPrefix();

$db_name_prefix = $db->getDBPrefix();
$x = explode("_",$db_name_prefix);
$db_prefix = $x[0];
$db_initial = $x[1];
$ignore_databases = array("help","ignite4u","create","iblank");

$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix && !in_array($e[1],$ignore_databases)) {
		$dbs[] = substr($e[1],1,7);
	}
}

$folders = array();
$usage = array();
$user_usage = array();
$cc_usage = array();
//ASSIST_HELPER::arrPrint($dbs);

$sql = "SELECT LOWER(cmpcode) as id, cmpcode, cmpname, cmpreseller, cmp_is_demo, cmpstatus FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' ORDER BY cmpcode";
$companys = $db->mysql_fetch_all_by_id($sql, "id");

//ASSIST_HELPER::arrPrint($companys);

?>
<style type="text/css" >
	table td {
		vertical-align: top; 
	}
</style>
<h1>Module Usage Report (Active Modules only)</h1>
<?php
foreach($companys as $cc => $c) {
	if(!isset($company_demos[strtoupper($cc)]) && !isset($support_databases[strtoupper($cc)])) {
	$is_client = ($c['cmp_is_demo']==0 ? "Y" : "N");
/*	echo "
	<tr>
		<td>".strtoupper($cc)."</td>
		<td>".$c['cmpname']."</td>
		<td>".($c['cmp_is_demo']==0 ? "Client" : "Demo")."</td>
		<td>".$c['cmpreseller']."</td>
		<td>";*/
		if(in_array($cc,$dbs)) {
			$cdb = new ASSIST_DB("client", $cc);
			$modules = $cdb->mysql_fetch_all_by_id("SELECT * FROM assist_menu_modules ORDER BY modref", "modref");
			$sql = "SELECT usrmodref as modref, count(usrtkid) as c 
			FROM assist_".strtolower($cc)."_menu_modules_users
			INNER JOIN assist_".strtolower($cc)."_timekeep
			ON tkid = usrtkid AND tkstatus = 1
			GROUP BY usrmodref";
			$users = $cdb->mysql_fetch_value_by_id($sql, "modref", "c");
			unset($cdb);
			$locs = array();
			foreach($modules as $m) {
				if($m['modyn'] == "Y") {
					$mr = $m['modref'];
					$locs[] = $m['modlocation'];
					if(!isset($usage[$m['modlocation']])) {
						$usage[$m['modlocation']] = array('Y' => 0, 'N' => 0);
					}
					$usage[$m['modlocation']][$is_client]++;
					if(!isset($user_usage[$m['modlocation']])) {
						$user_usage[$m['modlocation']] = array('Y' => 0, 'N' => 0);
					}
					$user_usage[$m['modlocation']][$is_client] += $users[$mr];
					if(!isset($cc_usage[$m['modlocation']])) {
						$cc_usage[$m['modlocation']] = array('Y' => array(), 'N' => array());
					}
					$cc_usage[$m['modlocation']][$is_client][] = $cc;
				}
				/*	echo "
					<tr>
						<td>".$m['modlocation']."</td>
						<td>".$m['modref']."</td>
						<td>".$m['modtext']."</td>
						<td>".$m['modyn']."</td>
					</tr>";*/
			}
			$folders = array_merge($folders, $locs);
		}
//	echo "</table>";
		} else {
//			echo "ERROR! No database.";
		}
//	echo "
//		</td>
//	</tr>";
}

$folders = array_unique($folders);
sort($folders,SORT_STRING);

?>
</table>
<table><tr><th>ModLoc</th><th>Generic Name</th>
		<th width="100">Clients</th><th width="100">Client Users</th><th width="100">Demos</th><th width="100">Demo Users</th>
		<th>CCs</th>
	</tr>
	<?php
	foreach($folders as $f) {
		echo "
		<tr>
			<td class='b'>".$f."</td>
			<td>".(isset($generic_modules[$f])?$generic_modules[$f]:"?").(in_array($f,array("PMSPS","AQA","PP"))?"&nbsp;<span class=i>[IAS only]</span>":"")."</td>
			<td class='center'>".($usage[$f]['Y']>0?$usage[$f]['Y']:"-")."</td>
			<td class='center'>".($user_usage[$f]['Y']>0?$user_usage[$f]['Y']:"-")."</td>
			<td class='center'>".($usage[$f]['N']>0?$usage[$f]['N']:"-")."</td>
			<td class='center'>".($user_usage[$f]['N']>0?$user_usage[$f]['N']:"-")."</td>
			<td><span class='b'>Clients:&nbsp;</span>".implode("; ",$cc_usage[$f]['Y'])."
			<br /><span class='b'>Demos:&nbsp;</span>".implode("; ",$cc_usage[$f]['N'])."</td>
		</tr>
			";
	}
	?>
</table>
<?php 
//ASSIST_HELPER::arrPrint($folders);
?>
</body>
</html>