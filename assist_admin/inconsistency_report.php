<?php
require_once '../header.php';
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");

$db_prefix = "ignittps";
$db_name_prefix = $db_prefix."_i";

$ignore_databases = array("help","ignite4u","create","iblank");
$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix && !in_array($e[1],$ignore_databases)) {
		$dbs[] = substr($e[1],1,7);
	}
}

//ASSIST_HELPER::arrPrint($dbs);

$sql = "SELECT LOWER(cmpcode) as id, cmpcode, cmpname, cmpreseller, cmp_is_demo, cmpstatus FROM assist_company ORDER BY cmpcode";
$companys = $db->mysql_fetch_all_by_id($sql, "id");

//ASSIST_HELPER::arrPrint($companys);

$all = array_unique(array_merge($dbs, array_keys($companys)));
sort($all);
//ASSIST_HELPER::arrPrint($all);
echo "<table><tr><th width=75px>CMPCODE</th><th width=75px>DB</th><th width=75px>CMP Record</th><th width=75px>CMP Status</th><th width=75px>ERROR?</th></tr>";
foreach($all as $cc) {
	$in_db = in_array($cc,$dbs);
	$in_cmp = isset($companys[$cc]);
	$stat = ($in_cmp) ? $companys[$cc]['cmpstatus'] : "-";
	if((!$in_db && $in_cmp && $stat=="Y") || ($in_db && (!$in_cmp || $stat=="N"))) {
		$err = true;
	} else {
		$err = false;
	}
	echo "
	<tr>
		<td class='center b'>$cc</td>
		<td class='center b' ".(!$in_db ? "style='color: #cc0001;'" : "").">".($in_db ? "Y" : "N")."</td>
		<td class='center b' ".(!$in_cmp ? "style='color: #cc0001;'" : "").">".($in_cmp ? "Y" : "N")."</td>
		<td class='center b' ".($stat!="Y" ? "style='color: #cc0001;'" : "").">".($stat)."</td>
		<td class='center b' ".($err ? "style='background-color: #cc0001; color: #ffffff;'" : "").">".($err ? "ERROR" : "-")."</td>
	</tr>";
}
?>
</table>
</body>
</html>