<?php
include 'inc_header.php';
require_once("../module/autoloader.php");
error_reporting(-1);
$alphabet = isset($_REQUEST['character']) ? strtolower($_REQUEST['character']) : "a";

?>
<h1>Action iT User License Activity Report</h1>
<?php
$mdb = new ASSIST_DB("master");
$sql = "SELECT * FROM assist_company WHERE cmp_is_demo = 0 AND cmpstatus = 'Y'";
$non_demo_companies = $mdb->mysql_fetch_all_by_id($sql, "cmpcode");
unset($mdb);

$db_prefix = "ignittps";
$db_name_prefix = $db_prefix."_i";

$ignore_databases = array("help","ignite4u","create","iblank","compare1","icassist","iroadmap");
$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}
mysql_close($link);

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix && !in_array($e[1],$ignore_databases)) {
		$dbs[] = substr($e[1],1,7);
	}
}

//arrPrint($dbs);
//arrPrint($non_demo_companies);

$users = array();
$module_license_activity_logs = array();
$fc = 0;
foreach($dbs as $cmpcode) {
	if(isset($non_demo_companies[strtoupper($cmpcode)]) && ($alphabet=="all" || substr($cmpcode,0,1)==$alphabet)) {
//echo "<p>".$cmpcode;
		$cdb = new ASSIST_DB("client",$cmpcode);
		$sql = "SELECT *, '$cmpcode' as cc FROM assist_".strtolower($cmpcode)."_timekeep WHERE tkuser <> 'admin' AND tkuser <> 'support'";
		$u = $cdb->mysql_fetch_all($sql); //echo " = ".count($u);
//$fc +=count($u);
		$users = array_merge($users,$u);
		//echo "<P>".count($users);
		unset($cdb);
	}
}
//echo "<p class=b>".$fc."</p>";
$user_add = array(0=>array(),1=>array());
$user_term = array(0=>array(),1=>array());
$client_count = array();
$cc_processed = array();

$cc_users = array();
$final_count = array(
	'all'=>array(
		0=>0,1=>0,2=>0
	)
);

foreach($users as $u) {
	$cc = $u['cc'];
	$add = $u['tkadddate'];
	if(!isset($final_count[$cc])) { $final_count[$cc] = array(0=>0,1=>0,2=>0); }
	if(is_numeric($add)) {
		$final_count['all'][$u['tkstatus']]++;
		$final_count[$cc][$u['tkstatus']]++;
		$tks = (strlen($u['tkuser'])==0 || $u['tkstatus']==2) ? 2 : 1;
		$aY = date("Y",$add)*1;
		$aM = date("m",$add)*1;
//if($aY==1970) { echo "<P>".$u['cc'].":".$u['tkid']; }
		if(!isset($cc_users[$cc][$aY][$aM][$tks])) { $cc_users[$cc][$aY][$aM][$tks] = 0; }
		if(!in_array($u['cc'],$cc_processed)) {
			$client_count[$aY] = !isset($client_count[$aY]) ? array() : $client_count[$aY];
			$client_count[$aY][$aM] = !isset($client_count[$aY][$aM]) ? 0 : $client_count[$aY][$aM];
			$client_count[$aY][$aM]++;
			$cc_processed[] = $u['cc'];
		}
		$user_add[$tks][$aY] = !isset($user_add[$tks][$aY]) ? array() : $user_add[$tks][$aY];
		$user_add[$tks][$aY][$aM] = !isset($user_add[$tks][$aY][$aM]) ? 0 : $user_add[$tks][$aY][$aM];
		$user_add[$tks][$aY][$aM]++;
		$cc_users[$cc][$aY][$aM][$tks]++;
	
		if($u['tkstatus']==0 && strlen($u['tktermdate'])>0) {
			if(!isset($cc_users[$cc][$aY][$aM][0])) { $cc_users[$cc][$aY][$aM][0] = 0; }
			$term = $u['tktermdate'];
			$tY = date("Y",$term)*1;
			$tM = date("m",$term)*1;
			$user_term[$tks][$tY] = !isset($user_term[$tks][$tY]) ? array() : $user_term[$tks][$tY];
			$user_term[$tks][$tY][$tM] = !isset($user_term[$tks][$tY][$tM]) ? 0 : $user_term[$tks][$tY][$tM];
			$user_term[$tks][$tY][$tM]++;
			$cc_users[$cc][$aY][$aM][0]++;
		}
	}
}

$start_year = min(array_keys($user_add[1]));

?>
<table>
	<tr>
		<th></th>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			echo "<th colspan=12>".$y."</th>";
		}
		?>
	</tr>
	<tr>
		<th></th>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<th>".$m."</th>";
			}
		}
		?>
	</tr>
	<tr>
		<th colspan=<?php echo (((date("Y")-$start_year)+1)*12)+1; ?>>Active Users</th>
	</tr>
<?php $tks = 1; ?>
	<tr>
		<td class=b>
			Active User Additions
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Active User Removals
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_term[$tks][$y][$m]) ? "-".$user_term[$tks][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Change
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0) - (isset($user_term[$tks][$y][$m]) ? $user_term[$tks][$y][$m] : 0);
				echo "<td>".$change_for_period."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Total
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0) - (isset($user_term[$tks][$y][$m]) ? $user_term[$tks][$y][$m] : 0);
				$uc+=$change_for_period;
				echo "<td>".$uc."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<th colspan=<?php echo (((date("Y")-$start_year)+1)*12)+1; ?>>Active Users</th>
	</tr>
<?php $tks = 2; ?>
	<tr>
		<td class=b>
			Inactive User Additions
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Inactive User Removals
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Change
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0) - (isset($user_term[$tks][$y][$m]) ? $user_term[$tks][$y][$m] : 0);
				echo "<td>".$change_for_period."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Total
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[$tks][$y][$m]) ? $user_add[$tks][$y][$m] : 0) - (isset($user_term[$tks][$y][$m]) ? $user_term[$tks][$y][$m] : 0);
				$uc+=$change_for_period;
				echo "<td>".$uc."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<th colspan=<?php echo (((date("Y")-$start_year)+1)*12)+1; ?>>Clients</th>
	</tr>
	<tr>
		<td class=b>
			Client Changes by User table
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($client_count[$y][$m]) ? $client_count[$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
</table>
<?php

//echo "<p>User Add";
//arrPrint($user_add);
//echo "<p>User Term";
//arrPrint($user_term);
echo "<p>CC processed";
arrPrint($cc_processed);
//echo "<p>Client count";
//arrPrint($client_count);
echo "<p>Users per CC";
arrPrint($cc_users);
arrPrint($final_count);


?>
</body>
</html>