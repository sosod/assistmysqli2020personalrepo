<?php
include 'inc_header.php';
require_once("../module/autoloader.php");
error_reporting(-1);

?>
<h1>Action iT Client / User / License Activity Report</h1>
<?php
$mdb = new ASSIST_DB("master");
$sql = "SELECT * FROM assist_company WHERE cmp_is_demo = 0";
$non_demo_companies = $mdb->mysql_fetch_all_by_id($sql, "cmpcode");

$db_prefix = "adev";
$db_name_prefix = $db_prefix."_i";

$ignore_databases = array("help","ignite4u","create","iblank","compare1");
$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix && !in_array($e[1],$ignore_databases)) {
		$dbs[] = substr($e[1],1,7);
	}
}

//arrPrint($dbs);
//arrPrint($non_demo_companies);

$users = array();
$module_license_activity_logs = array();

foreach($dbs as $cmpcode) {
	if(isset($non_demo_companies[strtoupper($cmpcode)])) {
		$cdb = new ASSIST_DB("client",$cmpcode);
		$sql = "SELECT *, '$cmpcode' as cc FROM assist_".strtolower($cmpcode)."_timekeep WHERE tkuser <> 'admin' AND tkuser <> 'support' AND tkstatus < 2";
		$u = $cdb->mysql_fetch_all($sql);
		$users = array_merge($users,$u);
		echo "<P>".count($users);
		unset($cdb);
	}
}

$user_add = array(0=>array(),1=>array());
$user_term = array(0=>array(),1=>array());
$client_count = array();
$cc_processed = array();

foreach($users as $u) {
	$add = $u['tkadddate'];
	if(is_numeric($add)) {
		$tks = strlen($u['tkuser'])==0 ? 0 : 1;
		$aY = date("Y",$add);
		$aM = date("m",$add);
		if(!in_array($u['cc'],$cc_processed)) {
			$client_count[$aY] = !isset($client_count[$aY]) ? array() : $client_count[$aY];
			$client_count[$aY][$aM] = !isset($client_count[$aY][$aM]) ? 0 : $client_count[$aY][$aM];
			$client_count[$aY][$aM]++;
			$cc_processed[] = $u['cc'];
		}
		$user_add[$tks][$aY] = !isset($user_add[$tks][$aY]) ? array() : $user_add[$tks][$aY];
		$user_add[$tks][$aY][$aM] = !isset($user_add[$tks][$aY][$aM]) ? 0 : $user_add[$tks][$aY][$aM];
		$user_add[$tks][$aY][$aM]++;
	
		if($u['tkstatus']==0) {
			$term = $u['tktermdate'];
			$tY = date("Y",$term);
			$tM = date("m",$term);
			$user_term[$tks][$tY] = !isset($user_term[$tks][$tY]) ? array() : $user_term[$tks][$tY];
			$user_term[$tks][$tY][$tM] = !isset($user_term[$tks][$tY][$tM]) ? 0 : $user_term[$tks][$tY][$tM];
			$user_term[$tks][$tY][$tM]++;
		}
	}
}

arrPrint($user_add);
arrPrint($user_term);
arrPrint($cc_processed);
arrPrint($client_count);

$start_year = min(array_keys($user_add[1]));

?>
<table>
	<tr>
		<th></th>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			echo "<th colspan=12>".$y."</th>";
		}
		?>
	</tr>
	<tr>
		<th></th>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<th>".$m."</th>";
			}
		}
		?>
	</tr>
	<tr>
		<th colspan=<?php echo (((date("Y")-$start_year)+1)*12)+1; ?>>Active Users</th>
	</tr>
	<tr>
		<td class=b>
			Active User Additions
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_add[1][$y][$m]) ? $user_add[1][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Active User Removals
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_term[1][$y][$m]) ? "-".$user_term[1][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Change
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[1][$y][$m]) ? $user_add[1][$y][$m] : 0) - (isset($user_term[1][$y][$m]) ? $user_term[1][$y][$m] : 0);
				echo "<td>".$change_for_period."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Total
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[1][$y][$m]) ? $user_add[1][$y][$m] : 0) - (isset($user_term[1][$y][$m]) ? $user_term[1][$y][$m] : 0);
				$uc+=$change_for_period;
				echo "<td>".$uc."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<th colspan=<?php echo (((date("Y")-$start_year)+1)*12)+1; ?>>Active Users</th>
	</tr>
	<tr>
		<td class=b>
			Inactive User Additions
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_add[0][$y][$m]) ? $user_add[0][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Inactive User Removals
		</td>
		<?php
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				echo "<td>".(isset($user_add[0][$y][$m]) ? $user_add[0][$y][$m] : 0)."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Change
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[0][$y][$m]) ? $user_add[0][$y][$m] : 0) - (isset($user_term[0][$y][$m]) ? $user_term[0][$y][$m] : 0);
				echo "<td>".$change_for_period."</td>";
			}
		}
		?>
	</tr>
	<tr>
		<td class=b>
			Total
		</td>
		<?php
		$uc = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++) {
				$change_for_period = (isset($user_add[0][$y][$m]) ? $user_add[0][$y][$m] : 0) - (isset($user_term[0][$y][$m]) ? $user_term[0][$y][$m] : 0);
				$uc+=$change_for_period;
				echo "<td>".$uc."</td>";
			}
		}
		?>
	</tr>
</table>

</body>
</html>