<?php
include 'inc_header.php';
//error_reporting(-1);
//print_r($_REQUEST);


?>
<h1>Client Module Activity Report - User Activity Log</h1>
<p>This report displays date/time of the most recent user access for a particular module as well as the number of times a module has been accessed per month as recorded in the user activity log [since 2 Oct 2011].</p>

<?php

if(!isset($_REQUEST['cc']) || strlen($_REQUEST['cc'])==0) {
	$company = array();
	$sql = "SELECT cmpcode, cmpname, cmpstatus FROM assist_company";
	$rs = AgetRS($sql);
	    while($row = mysql_fetch_array($rs)) {
		        $company[$row['cmpcode']] = $row;
	    }
	mysql_close();
	
	$dbs = array();
	$link = mysql_connect("localhost",$db_user,$db_pwd);
	$db_list = mysql_list_dbs($link);
	while ($row = mysql_fetch_object($db_list)) {
		$d = $row->Database;
		$dbs[] = $d;
	}
	mysql_close($link);
	
	//arrPrint($dbs);
	echo "<p>select company: <select name=cc id=sel_cc><option selected value=X>---SELECT---</option>";
	foreach($company as $cc=>$cmp) {
		if(in_array(strtolower($db_other.$cc),$dbs)) {
			echo "<option value=".$cc.">".strtoupper($cc).": ".$cmp['cmpname']." [".($cmp['cmpstatus']=="Y" ? "Active" : "Disabled")."]</option>";
		}
	}
	echo "</select></p>
	<p><input type=button value=Go id=btn_go /></p>
	<script type=text/javascript>
	$(function() {
		$('#btn_go').click(function() {
			var x = $('#sel_cc').val();
			if(x=='X') {
				alert('Please select a company first');
			} else {
				document.location.href = 'it_mod_report_user.php?cc='+x;
			}
		});
	});
	</script>";
	
} else {
	
	$r_cc = $_REQUEST['cc'];
	
	
	$system_default_modules = array(
		'MAN'=>"Manuals (New)",
		'USERMAN'=>"Manuals (Old)",
		'ACTION_DASHBOARD'=>"Action Dashboard",
		'USER_PROFILE'=>"User Profile",
		'CHANGEPASS'=>"Change Password",
	);
	
	
	
	
	$company = array();
	$sql = "SELECT cmpcode, cmpname, cmpstatus FROM assist_company WHERE cmpcode = '".strtoupper($r_cc)."'";
	$rs = AgetRS($sql);
	    while($row = mysql_fetch_array($rs)) {
	    	if(strtolower($row['cmpcode'])==strtolower($r_cc)) {
		        $company[$row['cmpcode']] = $row;
	    	}
	    }
	mysql_close();
	$sql = "SELECT client_modref as modref, activity_date FROM assist_client_module_activity_log WHERE client_cmpcode = '".strtoupper($r_cc)."' AND status = 1 AND activity_type = 'C' ORDER BY activity_date DESC";
	$rs = AgetRS($sql);
	    while($row = mysql_fetch_assoc($rs)) {
	    	if(!isset($company[$row['cmpcode']]['mods'][$row['modref']])) {
				$company[strtoupper($r_cc)]['mods'][$row['modref']] = $row;
	    	}
	    }
	mysql_close();
	
	$dbs = array();
	$link = mysql_connect("localhost",$db_user,$db_pwd);
	$db_list = mysql_list_dbs($link);
	while ($row = mysql_fetch_object($db_list)) {
		$d = $row->Database;
		if($d==$db_other.strtolower($r_cc)) {
		     $dbs[] = $d;
		}
	}
	mysql_close($link);
	$db_array = array();
	$k = 0;
	foreach($dbs as $db) {
		if($db!=$db_assist && substr($db,0,strlen($db_other))==$db_other) {
			$cc = strtoupper(substr($db,strlen($db_other)));
			if(isset($company[$cc])) {
				$cn = $company[$cc]['cmpname'];
				$cs = $company[$cc]['cmpstatus'];
			} else {
				$cn = "Invalid";
				$cs = "N/A";
			}
			switch($cs) {
			case "Y": $class = "isubmit"; $cs = "Available"; break;
			case "N": $class = "idelete"; $cs = "Closed"; break;
			case "N/A":
			default: $class = "iinform"; break;
			}
			$db_name = $db_other.strtolower($cc);
			$sql = "SELECT * FROM assist_menu_modules ORDER BY modtext";
			$rs = getRS($sql);
			$mods = array();
			while($row = mysql_fetch_assoc($rs)) {
				$mods[strtoupper($row['modref'])] = $row;
				$mods[strtoupper($row['modref'])]['count'] = array();
			}
			$mods['IN'] = array('modtext'=>"User Login",'count'=>array());
			unset($rs);
			$sql = "SHOW TABLES LIKE 'assist_".strtolower($cc)."_timekeep_activity'";
			$rs = getRS($sql);
			if(mysql_num_rows($rs) > 0) {
				$sql = "SELECT * FROM assist_".strtolower($cc)."_timekeep_activity WHERE tkid <> '0000' AND tkname NOT LIKE '%Support' ORDER BY id DESC";
				$row = mysql_fetch_all($sql);
				$db_array[$cc] = $row;
				foreach($row as $r) {
					if($r['action']!="OUT") {
						$y = date("Y",strtotime($r['date']));
						$m = date("m",strtotime($r['date']));
						@$mods[strtoupper($r['action'])]['count'][$y][$m]++;
						if(!isset($mods[strtoupper($r['action'])]['last'])) {
							$mods[strtoupper($r['action'])]['last'] = $r['date'];
						}
						if(!isset($mods[strtoupper($r['action'])]['modtext']) && isset($system_default_modules[strtoupper($r['action'])])) {
							$mods[strtoupper($r['action'])]['modtext'] = $system_default_modules[strtoupper($r['action'])];
							$mods[strtoupper($r['action'])]['modyn'] = "SYS";
						} elseif(!isset($mods[strtoupper($r['action'])]['modtext']) && !isset($system_default_modules[strtoupper($r['action'])])) {
							$mods[strtoupper($r['action'])]['modtext'] = strtoupper($r['action']);
							$mods[strtoupper($r['action'])]['modyn'] = "ERR";
						}
					}
				}
				$total = $mods['IN'];
				unset($mods['IN']);
			} else {
				$activity = "Error.  User Activity Log table not found.";
				$key = $k; $k++;
				$db_array[$cc] = $activity;
			}
		}
	}
	
	//echo "<pre>"; print_r($mods); echo "</pre>";
	//echo "<pre>"; print_r($db_array); echo "</pre>";
	


uasort($mods,function(array $a, array $b) {
	return strcmp($a['modtext'], $b['modtext']);
});
?>
<table id=tbl_count>
	<thead>
		<tr>
			<th rowspan=3>Module</th>
			<th rowspan=3>Status</th>
			<th rowspan=3>Created**</th>
			<th rowspan=3>Last accessed</th>
			<th colspan=<?php echo count($total); ?>>User Access Count per Month</th>
		</tr>
		<tr>
		<?php 
		foreach($total['count'] as $ty => $t) {
			echo "<th colspan=".count($t).">".$ty."</th>";
		}
		?>
		</tr>
		<tr>
		<?php 
		foreach($total['count'] as $ty => $t) {
			foreach($t as $tm => $tc) {
				echo "<th>".date("M",mktime(12,0,0,$tm,1,date("Y")))."</th>";
			}
		}
		?>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($mods as $mr => $md) {
			$mc = "";
			if(isset($company[strtoupper($r_cc)]['mods'][$mr])) {
				$mc = $company[strtoupper($r_cc)]['mods'][$mr]['activity_date'];
			}
			if(isset($md['modyn'])) {
				switch($md['modyn']) {
					case "N":
						$ms = "<span class=grey>Disabled</span>";
						break;
					case "Y":
						$ms = "<span class=isubmit>Active</span>";
						break;
					case "SYS":
						$ms = "<span class=iinform>System</span>";
						break;
					default:
						$ms = "<span class=idelete>UNKNOWN</span>";
						break;
				}
			} else {
				$ms = "<span class=idelete>UNKNOWN</span>";
			}
			echo "
			<tr>
				<td class=b>".(isset($md['modtext']) ? $md['modtext']."<br /><small>[$mr]</small>" : $mr)."</td>
				<td class=center>".$ms."</td>
				<td class=center>".$mc."</td>
				<td>".(isset($md['last']) ? $md['last'] : "Never")."</td>
			";
			foreach($total['count'] as $ty => $t) {
				foreach($t as $tm => $tc) {
					echo "<td class=center>";
					if(isset($md['count'][$ty][$tm])) {
						echo $md['count'][$ty][$tm];
					} else {
						echo "-";
					}
					echo "</td>";
				}
			}
			echo "</tr>";
		}
		$md = $total;
		echo "
		<tr>
			<td class=b colspan=3>User Logins<br /><small>[IN]</small></td>
			<td>".(isset($md['last']) ? $md['last'] : "Never")."</td>";
			foreach($total['count'] as $ty => $t) {
				foreach($t as $tm => $tc) {
					echo "<td class=center>";
					if(isset($md['count'][$ty][$tm])) {
						echo $md['count'][$ty][$tm];
					} else {
						echo "-";
					}
					echo "</td>";
				}
			}
		echo "</tr>";
		?>
	</tbody>
</table>
<p>** The creation date for each module is only available for modules created after 1 July 2012.</p>
<script type="text/javascript" >
	$(function() {
		//colspan of the "access count" cell
		var tc = $("#tbl_count thead tr:eq(2) th").length;
		$("#tbl_count thead tr:eq(0) th:last").attr("colspan",tc);
	});
</script>


<?php


} //end if for if company code selected from drop down


?>

</body>
</html>