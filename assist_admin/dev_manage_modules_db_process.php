<?php
echo "<h2>Create New Client Database</h2><ol>";

$cc = strtoupper($_REQUEST['cmpcode']);
$client = strtolower($_REQUEST['cmpcode']);


//Validate that client is not duplicate
echo "<li>Validating that client code '$cc' is not a duplicate... ";
	$sql = "SELECT * FROM assist_company WHERE cmpcode = '$cc'";
	$rs = AgetRS($sql);
	$mnr = mysql_num_rows($rs);
	unset($rs);
	if($mnr>0) {
		echo "<span class=idelete>failure</span> - Client code '$cc' appears to already exist on Ignite Assist."; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
//Validate that database does not exist.
echo "<li>Validating that database does not already exist with this company code... ";
	$db_name_client = $db_other.$client;
	if(in_array($db_name_client,$dbs)) {
		echo "<span class=idelete>failure</span> - Database $db_name_client already exists on the server."; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
//Create blank database
/*
echo "<li>Creating blank database '$db_name_client'... ";
	//$sql = "CREATE DATABASE IF NOT EXISTS `".$db_name_client."`";
	//$rs = getRS($sql);
	//unset($rs);
	echo "<span class=idelete>WARNING!</span> - DB creation can't be done by dev user!</li>";
//Get/Copy base tables from blank database
echo "<li>Creating base tables... ";
	$db_name_blank = $db_other."blank";
	$db_name = $db_name_blank;
	//getting modules list in blank so that script knows which tables to ignore.
	//get modules only > 10 so that system required modules are ignored (e.g. user dir);
	$sql = "SELECT modlocation FROM assist_menu_modules WHERE modid > 10";
	$mods = mysql_fetch_fld_one($sql,"modlocation");
	//arrPrint($mods)
	//get table names from info schema for blank db
	$sql = "SELECT TABLE_NAME FROM information_schema.TABLES
			WHERE TABLE_SCHEMA = '".$db_name_blank."' AND ( (";
$sql_arr = array();
		foreach($mods as $m) {
			$sql_arr[]= "TABLE_NAME NOT LIKE '%_".strtolower($m)."_%'";
		}
$sql.= implode(" AND ",$sql_arr);
		$sql.= " AND TABLE_NAME NOT LIKE 'assist_policies_%'";
		$sql.= " AND TABLE_NAME NOT LIKE 'assist_ud_%' 
			) OR (
				TABLE_NAME LIKE '%_udf%'
			)
		)";
//echo $sql;
	$tables = mysql_fetch_fld_one($sql,"TABLE_NAME");
	//arrPrint($tables);
	$db_name = $db_name_client;
	$db_ref = "assist_".$client;
	foreach($tables as $old_tbl) {
		//$old_tbl = $tbl[0];
		$etbl = explode("_",$old_tbl); 
		if($etbl[1]=="blank") {
			unset($etbl[0]); unset($etbl[1]);
			$new_tbl = strtolower($db_ref."_".implode("_",$etbl));
		} else {
			$new_tbl = strtolower(implode("_",$etbl));
		}
		//delete existing table (in case of interrupted creation process)
		$sql = "DROP TABLE IF EXISTS `".$db_name_client."`.`".$new_tbl."`";
		$rs = getRS($sql);
		unset($rs);
		//create table
		$sql = "CREATE TABLE `".$db_name_client."`.`".$new_tbl."` LIKE `".$db_name_blank."`.`".$old_tbl."`";
		$rs = getRS($sql);
		unset($rs);
		//insert fixed system data
		if($old_tbl!="assist_menu_modules") {
			$sql = "INSERT INTO `".$db_name_client."`.`".$new_tbl."` SELECT * FROM `".$db_name_blank."`.`".$old_tbl."`";
		} else {
			$sql = "INSERT INTO `".$db_name_client."`.`".$new_tbl."` SELECT * FROM `".$db_name_blank."`.`".$old_tbl."` WHERE modid <= 10";
		}
		db_insert($sql);
	}
	echo "done!</li>";*/
echo "<h3 class=idelete>WARNING!</h3>
<p>All db / table actions cannot be done by dev user.";

//Create files/folder
echo "<li>Creating client files folder... ";
	checkFolder('reports');
	echo "done!</li>";
//Create record in master company table
echo "<li>Creating record in company master table... ";
	$cmpname = code($_REQUEST['cmpname']);
	$cmpdispname = $_REQUEST['cmpdispname'];
	$cmpadminpwd = $_REQUEST['cmpadminpwd'];
	$cmplogo = $_REQUEST['cmplogo'];
	$cmpadminemail = $_REQUEST['cmpadminemail'];
	$cmpadmin = decode($_REQUEST['cmpadmin']);
	$cmpreseller = $_REQUEST['cmpreseller'];
	$cmp_is_demo = $_REQUEST['cmp_is_demo'];
	$cmp_is_csi = $_REQUEST['cmp_is_csi'];
	$cmp_bill_hosting = $_REQUEST['cmp_bill_hosting'];
	$sql = "INSERT INTO assist_company (cmpcode, cmpname, cmpdispname, cmpstatus, cmpadminpwd, cmplogo, cmpadminemail, cmpadmin, cmplryn, cmpfolder, cmpreseller, cmp_is_demo, cmp_is_csi, cmp_bill_hosting, cmp_price_class) VALUES ('$cc','$cmpname','$cmpdispname','Y','$cmpadminpwd','$cmplogo','$cmpadminemail','$cmpadmin','N',0,'$cmpreseller',$cmp_is_demo, $cmp_is_csi, $cmp_bill_hosting,'P')";
	Adb_insert($sql);
	echo "done!</li>";
	
echo "<P class=b>Creation process completed successfully.</p><script type=text/javascript> document.location.href = 'manage_modules.php?r[]=ok&r[]=".urlencode("New client ".$cmpname." has been successfully created.")."';</script>";

?>