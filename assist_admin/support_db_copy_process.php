<?php
require_once("../module/autoloader.php");
$db = new ASSIST_MODULE_HELPER();
$tkid = $db->getUserID();
$cmpcode = $db->getCmpCode();
$mdb = new ASSIST_DB("master");
ASSIST_HELPER::echoPageHeader("1.10.0", array(), array("/assist_jquery.css?".time(), "/assist3.css?".time()));


/** Get current Companies */
$companies = array();
//don't worry about exclusion or support databases here - previous page will have filtered them out BUT ADD THEM BACK IN IF CALLING THIS PAGE FROM ELSEWHERE!!
$sql = "SELECT cmpcode, CONCAT(cmpname,' (',cmpcode,')') as cmpname FROM assist_company WHERE cmpstatus = 'Y' ORDER BY cmpname, cmpcode";
$companies = $mdb->mysql_fetch_value_by_id($sql,"cmpcode","cmpname");
/** Get active DBs */
$dbs = array();
$db_prefix = $mdb->getDBPrefix();
$sql = "SHOW DATABASES LIKE '".$db_prefix."%'";
$dbs_raw = $mdb->mysql_fetch_all($sql);
foreach($dbs_raw as $i=>$x) {
	foreach($x as $z => $y) {
		$dbs[] = $y;
	}
}


//error_reporting(-1);
echo "<h2>Copy Database</h2><ol>";

$src_cc = ($_REQUEST['src']);
$src_db = strtolower($db_prefix.$src_cc);
$dest_cc = ($_REQUEST['dest']);
$dest_db = strtolower($db_prefix.$dest_cc);
//preset some variables for checks further down (from JC's old DB copy function)
$rename_tbl = true;
$drop_dest = "YES";
$is_drop_dest = true;
$_REQUEST['reset_pwd'] = "Y";

//arrPrint($_REQUEST);
//echo "<h3>".$rename_tbl." :: ".($rename_tbl===true)."</h3>";

//Validate that source exists
echo "<li>Validating that source database for '$src_cc' exists... ";
	if(!in_array($src_db,$dbs)) {
		echo "
<span class=idelete>failure</span> - Source database for '$src_cc' does not appear to exist.  Please contact Janet Currie of Action iT.
<script type='text/javascript'>
document.location.href = 'support_db_copy.php?r[]=error&r[]=".urlencode("Source database does not appear to exist.  Please contact Janet Currie of Action iT")."';
</script>"; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
//Validate that destination does not exist
echo "<li>Preparing destination database to be replaced... ";
	if(in_array($dest_db,$dbs)) {
		if($is_drop_dest) {
			$sql = "DROP DATABASE ".$dest_db;
			$db->db_query($sql);
			echo "<span class=isubmit>success</span>.</li>";
		} else {
			echo "</li></ul>
			<form name=db_copy method=post action=manage_modules.php>
				<input type=hidden name=act value='COPY_DB' />
				<input type=hidden name=src_db value='$src_db' />
				<input type=hidden name=dest_db value='$dest_db' />
				<input type=hidden name=drop_dest value='NO' />
				<input type=hidden name=rename_tbl value='$rename_tbl' />
				<div class=center>
					<h2 class=idelete>Database $dest_db exists</h2>
					<p>Do you want to drop it?</p>
					<p><input type=button name=drop_dest_yes value=Yes class=idelete />  <input type=button name=drop_dest_no value=No class=isubmit /></p>
				</div>
			</form>
			<script type=text/javascript>
			$(function() {
				$('input:button').click(function() {
					var v = $(this).val();
					if(v=='Yes') {
						$('input:hidden[name=drop_dest]').val('YES');
					} else {
						$('input:hidden[name=act]').val('VIEW');
					}
					$('form[name=db_copy]').submit();
				});
			});
			</script>
			";
			die();
		}
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
//Create blank database
echo "<li>Creating destination database for '$dest_cc'... ";
	$sql = "CREATE DATABASE IF NOT EXISTS `".$dest_db."`";
	$db->db_query($sql);
	echo "<span class='isubmit'>done!</span></li>";
//Get/Copy base tables from blank database
echo "<li>Creating tables... <span class='iinform'>Please be patient - this can take a while for larger clients.  If you're still waiting after 15 minutes, please contact Janet Currie of Action iT (but leave this page open!)</span>... ";
	$sql = "SELECT TABLE_NAME FROM information_schema.TABLES
			WHERE TABLE_SCHEMA = '".$src_db."'";
	$tables = $db->mysql_fetch_fld_one($sql,"TABLE_NAME");
//	arrPrint($tables);
	$d = explode("_",$dest_db);
	$d_cmpcode = strtolower(substr($d[1],1,strlen($d[1])));
	$dest = new ASSIST_DB($d_cmpcode);
	$db_ref = "assist_".$d_cmpcode; 
	$s = explode("_",$src_db);
	$s_cmpcode = strtolower(substr($s[1],1,strlen($s[1])));
	$src = new ASSIST_DB($s_cmpcode);
	//echo "<li class=b>".$db_ref."</li>";
	foreach($tables as $old_tbl) {
		//echo "<li>".$old_tbl;
if(stripos($old_tbl,"assist_dra0001_sp12b_")===false) {
		if($rename_tbl===true) {
			//echo " ::> <span class=isubmit>RENAMING!!!</span>";
			$etbl = explode("_",$old_tbl); 
			//echo "  ::>  ".$etbl[1]."  ::>  ".$s_cmpcode;
			if($etbl[1]==$s_cmpcode) {
				unset($etbl[0]); unset($etbl[1]);
				$new_tbl = strtolower($db_ref."_".implode("_",$etbl));
			} else {
				$new_tbl = strtolower(implode("_",$etbl));
			}
		} else {
			$new_tbl = $old_tbl;
		}
		//echo "  ::>  ".$new_tbl."</li>";
		//delete existing table (in case of interrupted creation process)
		$sql = "DROP TABLE IF EXISTS `".$dest_db."`.`".$new_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_query($sql);
		//create table
		$sql = "CREATE TABLE `".$dest_db."`.`".$new_tbl."` LIKE `".$src_db."`.`".$old_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_query($sql);
		//insert data
		$sql = "INSERT INTO `".$dest_db."`.`".$new_tbl."` SELECT * FROM `".$src_db."`.`".$old_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_insert($sql);
}
	}
	echo "<span class='isubmit'>done!</span></li>";
//Reset passwords
echo "<li>Resetting user passwords / emails for Dev... ";
if(isset($_REQUEST['reset_pwd']) && strtoupper($_REQUEST['reset_pwd'])=="Y") {
	if($rename_tbl===true) { $reset_tbl = "assist_".$d_cmpcode."_timekeep"; } else { $reset_tbl = "assist_".$s_cmpcode."_timekeep"; }
	$dest->db_update("UPDATE `".$dest_db."`.`".$reset_tbl."` SET tkemail = 'developers@actionassist.co.za', tkpwd = '417373697374313233'");
	echo "<span class='isubmit'>done!</span></li>";
} else {
	echo "NO</li>";
}
//Reset passwords
echo "<li>Killing any existing Support / Admin user logins... ";
	if($rename_tbl===true) { $timekeep_tbl = "assist_".$d_cmpcode."_timekeep"; } else { $timekeep_tbl = "assist_".$s_cmpcode."_timekeep"; }
	//get support user id
	$sql = "SELECT tkid FROM `".$dest_db."`.`".$timekeep_tbl."` WHERE tkuser = 'support'";
	$support_tkid = $dest->mysql_fetch_one_value($sql,"tkid");
	//insert record to logout support user
	$sql = "INSERT INTO `".$dest_db."`.`".$timekeep_tbl."_activity` VALUES (null, now(), '$support_tkid', 'Support DB Copy Logout','OUT',1,'','')";
	$dest->db_insert($sql);
	//insert record to logout admin user
	$sql = "INSERT INTO `".$dest_db."`.`".$timekeep_tbl."_activity` VALUES (null, now(), '0000', 'Support DB Copy Logout','OUT',1,'','')";
	$dest->db_insert($sql);
	echo "<span class='isubmit'>done!</span></li>";

//Log source database in assist_company_profile
echo "<li>Logging source database so that you can remember where this came from... ";
	//insert record to logout admin user
	$sql = "INSERT INTO `".$dest_db."`.`assist_company_profile` (cmpcode, cmpname, cmpadmin) VALUES ('$src_cc','".$companies[$src_cc]."','".date("Y-m-d H:i:s")."')";
	$dest->db_insert($sql);
	$sql = "INSERT INTO assist_support_copy_log VALUES (null, '$src_cc','$dest_cc',now(),'$tkid','$cmpcode')";
	$mdb->db_insert($sql);
	echo "<span class='isubmit'>done!</span></li>";

if(!isset($_REQUEST['src']) || $_REQUEST['src']!="CURL") {
	echo "<P class='isubmit'>Copy process completed successfully.</p>
	<script type=text/javascript>  
	document.location.href = 'support_db_copy.php?r[]=ok&r[]=".urlencode("$src_cc has been successfully copied to $dest_cc ")."';
	</script>";
} else {
	echo "<P class='b isubmit'>Copy process completed successfully.</p>";
}
//echo "<p>end of page</p>";
?>