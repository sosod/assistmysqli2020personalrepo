<?php
//error_reporting(-1);
if(isset($_GET['token'])) {
	@session_start();
	$_SESSION['cc'] = "iassist";
	$_SESSION['tid']= "0001";
	$_SESSION['tkn'] = "Assist Server";
	$now = strtotime(date("d F Y H:i:s")); 
	$_SESSION['session_timeout']['expiration'] = $now+3600;


	$token = explode(" ",$_REQUEST['token']);
	$_REQUEST['act'] = $token[0];
	$_REQUEST['src_db'] = $token[1];
	$_REQUEST['dest_db'] = $token[2];
	$_REQUEST['rename_tbl'] = "Y";
	$_REQUEST['drop_dest'] = "YES";
	$_REQUEST['reset_pwd'] = "N";
	$_REQUEST['src'] = "CURL";

} else {

require_once '../header.php';
}

require_once('../library/class/autoload.php');
//error_reporting(-1);



$action = isset($_REQUEST['act']) && strlen($_REQUEST['act'])>0 ? $_REQUEST['act'] : "VIEW";






$db = new ASSIST_DB("master");
$db_user = $db->getDBUser();
$db_pwd = $db->getDBPwd();



/* GET COMPANY LIST */
$company = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' ORDER BY cmpcode";
} else {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpreseller = '".strtoupper($cmpcode)."' ORDER BY cmpcode";
}
/*$rs = AgetRS($sql); //echo $sql;
    while($row = mysql_fetch_array($rs))
    {
        $company[$row['cmpcode']] = $row;
    }
unset($rs);
*/
$company = $db->mysql_fetch_all_by_id($sql,"cmpcode");
//echo "<pre>"; print_r($company); echo "</pre>";

/* GET DEV COMPANY LIST */
/*$dev_company = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' ORDER BY cmpcode";
	$devcon = mysql_connect("localhost",$db_user,$db_pwd);
	mysql_select_db("dev_ignite4u",$devcon);
	$devrs = mysql_query($sql,$devcon);
	while($row = mysql_fetch_array($devrs)) {
		$dev_company[$row['cmpcode']] = $row;

	}
	unset($devrs);

}
//echo "<pre>"; print_r($dev_company); echo "</pre>";
/* GET RESELLER INFO */
$resellers = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname 
			FROM `assist_company` c1
			INNER JOIN assist_reseller r
			  ON r.cmpcode = c1.cmpcode AND r.active = 1
			WHERE c1.cmpstatus = 'Y'
			ORDER BY c1.cmpcode";
/*
	$rs = AgetRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$resellers[$row['cmpcode']] = $row;
	}
	unset($rs);*/
	$resellers = $db->mysql_fetch_all_by_id($sql,"cmpcode");
} else {
	$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>"SELECT");
}


$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}

?>
<style type=text/css>
    select {
        font-family: Lucida Console, Arial;
        font-size: 10pt;
    }
	ul li { 
		font-weight: bold;
	}
	.tbl_create, .tbl_create td { border: 1px solid #fe9900; }
</style>
<h1>Assist Admin >> Module Management</h1>
<?php 
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$action = isset($_REQUEST['act']) && strlen($_REQUEST['act'])>0 ? $_REQUEST['act'] : "VIEW";

switch($action) {
case "CREATE_MODULE":
	//arrPrint($_REQUEST);
	include("manage_modules_create_process.php");
	break;
case "CREATE_DB":
	include("manage_modules_db_process.php");
	break;
case "COPY_DB":
	include("manage_modules_db_copy.php");
	break;
case "DEV_USER":
	$src = strtolower($_REQUEST['src_db']);
	$admire = isset($_REQUEST['admire']) ? strtoupper($_REQUEST['admire']) : "N";
	if(strlen($src)>5) {
		$src = explode("_",$src);
		$sql = "GRANT SELECT , INSERT , UPDATE , DELETE , CREATE , DROP , ALTER ON  `".$src[0]."\_".$src[1]."` . * TO  'dev_ignite'@'localhost';";
		$db->db_query($sql);
		if($admire=="Y") {
			$sql = "GRANT SELECT , INSERT , UPDATE , DELETE , CREATE , DROP , ALTER ON  `".$src[0]."\_".$src[1]."` . * TO  'admire'@'localhost';";
			$db->db_query($sql);
		}
		echo "<script type=text/javascript>document.location.href = 'manage_modules.php?r[]=ok&r[]=".urlencode("Dev users have been given access to ".$_REQUEST['src_db'])."';</script>";
	} else {
		echo "<script type=text/javascript>document.location.href = 'manage_modules.php?r[]=error&r[]=".urlencode("Invalid database name given.")."';</script>";
	}
	break;
case "VIEW":
default:
	$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modtext";
	$modules = mysql_fetch_all($sql,"O","blank");

	echo "<h2>Load New Module</h2>
	<form name=frm_create action=manage_modules.php method=post>
	<input type=hidden name=act value='CREATE_MODULE' />
	<table class=tbl_create>
	<tr><td class=b>Database:</td><td><select name=cmpcode><option value=X select>--- SELECT DATABASE ---</option>";
	foreach($company as $c) {
		$cmpcode = strtolower($c['cmpcode']);
		if (in_array($db_other.$cmpcode, $dbs, true)) {
			echo "<option value='".$c['cmpcode']."' src=live>".$c['cmpcode'].": ".$c['cmpname']."</option>";
		}
	}
	if(count($dev_company)>0) {
		echo "<option value=X select>--- SELECT DEV DATABASE ---</option>";
		foreach($dev_company as $c) {
			$cmpcode = strtolower($c['cmpcode']);
			if (in_array("dev_i".$cmpcode, $dbs, true)) {
				echo "<option value='".$c['cmpcode']."' src=dev>".$c['cmpcode'].": ".$c['cmpname']."</option>";
			}
		}
	}
	echo "
	</select></td></tr>
	<tr>
		<td class=b>Source:</td><td><input type=text id=src name=src value='' /></td>
	</tr>
	<tr><td class=b>Base Module:</td><td><select name=modloc><option value=X selected>--- SELECT MODULE ---</option>";
	foreach($modules as $m) {
		echo "<option value=".$m['modlocation']." r=".$m['modref'].">".$m['modtext']."</option>";
	}
	echo "
	</select></td></tr>
	<tr><td class=b>Module Title:</td><td><input type=text value='' name=modtext maxlength=100 size=50 /></td></tr>
	<tr><td class=b>Module Ref:</td><td><input type=text value='' name=modref maxlength=10 size=15 /></td></tr>
	<tr><td class=b>Max. Users:</td><td><input type=text value='-1' name=max_users maxlength=5 size=5 /></td></tr>
	<tr><td colspan=2 class=center><input type=button value='Create Module' class=isubmit /></td></tr>
	</table>
	</form>
	<div style='width:700px;' class=float>
		<h3>Existing DB Modules</h3>
		<table id=tbl_existing_mods><tr><td>TBC</td></tr></table>
	</div> 
	<script type=text/javascript>
	$(function() {
		$('form[name=frm_create] select[name=modloc]').change(function() {
				var form = 'form[name=frm_create]';
				var me = 'select[name=modloc]';
			if($(this).val() != 'X') {
				var m = $(form+' '+me+' option:selected').text();
				$(form+' input[name=modtext]').val(m);
				var r = $(form+' '+me+' option:selected').attr('r');
				$(form+' input[name=modref]').val(r);
			} else {
				$(form+' input[name=modtext]').val('');
				$(form+' input[name=modref]').val('');
			}
		});
		$('form[name=frm_create] select[name=cmpcode]').change(function() {
				var form = 'form[name=frm_create]';
				var me = 'select[name=cmpcode]';
			var s = $(form+' '+me+' option:selected').attr('src');
			$(form+' input:text[name=src]').val(s);
		});
		$('form[name=frm_create] input:button.isubmit').click(function() {
			var form = 'form[name=frm_create]';
			var cc = $(form+' select[name=cmpcode]').val();
			var ml = $(form+' select[name=modloc]').val(); 
			var mt = $(form+' input[name=modtext]').val();
			var mr = $(form+' input[name=modref]').val();
			if(cc.length==0 || cc == 'X' || ml.length == 0 || ml == 'X' || mt.length == 0 || mr.length == 0) {
				alert('Missing data.  All fields required.');
			} else {
				$(form).submit();
			}
		});
//to speed up loading of EMP1
		$('form[name=frm_create] select[name=modloc]').val('SDBP6');
		$('form[name=frm_create] select[name=modloc]').trigger('change');
		$('form[name=frm_create] input[name=modref]').val('SDBIP6');";

if(isset($_REQUEST['r'][2]) && strlen($_REQUEST['r'][2])>0) {
	echo "
		$('form[name=frm_create] select[name=cmpcode]').val('".$_REQUEST['r'][2]."');
		//$('form[name=frm_create] select[name=cmpcode] option:selected').next().attr('selected', 'selected');
		$('form[name=frm_create] select[name=cmpcode]').trigger('change');
	";
}
echo "
	});
	</script>
	<h2>Create New Client Database</h2>
	<form name=frm_db action=manage_modules.php method=post>
	<input type=hidden name=act value='CREATE_DB' />
	<input type=hidden name=cmpstatus value = 'Y' />
	<table class=tbl_create>
	<tr><td class=b>Company Code:</td><td><input type=text value='' name=cmpcode maxlength=7 size=10 id=db_cc /></td></tr>
	<tr><td class=b>Company Name:</td><td><input type=text value='' name=cmpname maxlength=100 size=25 /></td></tr>
	<tr><td class=b>Company Display Name:</td><td><textarea name=cmpdispname cols=40 rows=3></textarea></td></tr>
	<tr><td class=b>Admin Name:</td><td><input type=text value='' name=cmpadmin maxlength=100 size=25 /></td></tr>
	<tr><td class=b>Admin Password (hex):</td><td><input type=text value='' name=cmpadminpwd id=db_adminpwd maxlength=100 size=25 /> <input type=button value=Generate id=btn_admin_pwd  /> <label id=db_adminpwdtext></label></td></tr>
	<tr><td class=b>Admin Email:</td><td><input type=text value='' name=cmpadminemail maxlength=100 size=50 /></td></tr>
	<tr><td class=b>Support Password (hex):</td><td><input type=text value='' name=supp_pwd maxlength=100 size=25 /></td>
		<td rowspan=8>Action Assist: sUpP1920 (7355705031393230)
		<br />UWC Assist: sUpP2122 (7355705032313232)
		<br />Ignite Assist: sUpP753 (73557050373533)
		<br />Imani Assist: sUpP9131 (7355705039313331)
		<br />Demo: Assist123 (417373697374313233)
		<br />REK: sUpP185 (73557050313835)
		<br />ARMS: sUpP1181 (7355705031313831)
		<br />PwC: sUpP1623 (7355705031363233)

	</td></tr>
	<tr><td class=b>Support Email:</td><td><input type=text value='' name=supp_email maxlength=150 size=50 /></td></tr>
	<tr><td class=b>Logo:</td><td><input type=text value='blank.gif' name=cmplogo maxlength=100 size=25 /></td></tr>
	<tr><td class=b>Reseller:</td><td><select name=cmpreseller><option value=X selected>--- SELECT RESELLER ---</option>";
	foreach($resellers as $rkey => $r) {
		echo "<option ".($rkey=="DEMORES" ? "selected" : "")." value=$rkey>".$rkey.": ".$r['cmpname']."</option>";
	}
	echo "
	</select></td></tr>
	<tr><td class=b>Is Demo?:</td><td><select name=cmp_is_demo><option selected value=0>No</option><option value=1>Yes</option></select></td></tr>
	<tr><td class=b>Is CSI?:</td><td><select name=cmp_is_csi><option selected value=0>No</option><option value=1>Yes</option></select></td></tr>
	<tr><td class=b>Bill Hosting?:</td><td><select name=cmp_bill_hosting><option selected value=1>Yes</option><option value=0>No</option></select></td></tr>
	<tr><td class=b>Site:</td><td><input type=text name=cmp_site size=25 value='".$_SERVER['SERVER_NAME']."' /></td></tr>
	<tr><td colspan=2 class=center><input type=button value='Create Database' class=isubmit /></td></tr>
	</table>
	</form>
	<script type=text/javascript>
	$(function() {
		$('form[name=frm_db] input:button.isubmit').click(function() {
			var form = 'form[name=frm_db]';
			$(form).submit();
		});
		$('form[name=frm_db] #btn_admin_pwd').click(function() {
			var cc = $('form[name=frm_db] #db_cc').val();
			cc = cc.substr(0,3);
			var r = AssistHelper.doAjax('manage_modules_admin_random.php','cc='+cc);
			if(r[0]=='ok') {
				$('form[name=frm_db] #db_adminpwd').val(r['hex']);
				$('form[name=frm_db] #db_adminpwdtext').text(r['text']);
			} else {
				alert(r['text']);
			}
		});
	});
	</script>
	
	<h2>Copy Database</h2>
	<form name=frm_db_copy action=manage_modules.php method=post>
	<input type=hidden name=act value='COPY_DB' />
	<table class=tbl_create>
	<tr><td class=b>Source:</td><td><select name=src_db><option selected value=X>--- SELECT ---</option>";
	foreach($dbs as $d) {
		echo "<option ".($d=="pwcza_itrain00" ? "selected" : "")." value=$d>$d</option>";
	}
	echo "
	</select></td></tr>
	<tr><td class=b>Destination:</td><td><input type=text value='".(isset($_REQUEST['cc']) ? "pwcza_i".strtolower($_REQUEST['cc']) : "")."' name=dest_db maxlength=100 size=25 /></td></tr>
	<tr><td class=b>Rename tables?:</td><td><select name=rename_tbl><option selected value=Y>Yes</option><option value=N>No</option></td></tr>
	<tr><td class=b>Reset passwords/email to Dev?:</td><td><select name=reset_pwd><option value=Y>Yes</option><option selected value=N>No</option></td></tr>
	<tr><td colspan=2 class=center><input type=button value='Copy Database' class=isubmit /></td></tr>
	</table>
	</form>
	<script type=text/javascript>
	$(function() {
		$('form[name=frm_db_copy] input:button.isubmit').click(function() {
			var form = 'form[name=frm_db_copy]';
			$(form).submit();
		});
	});
	</script>


	<h2>Dev User DB Access</h2>
	<form name=frm_db_dev_user action=manage_modules.php method=post>
	<input type=hidden name=act value='DEV_USER' />
	<table class=tbl_create>
	<tr><td class=b>Source:</td><td><select name=src_db><option selected value=X>--- SELECT ---</option>";
	foreach($dbs as $d) {
		echo "<option value=$d>$d</option>";
	}
	echo "
	</select></td></tr>
	<tr><td class=b>Admire:</td><td><select name=admire><option selected value=N>No</option><option value=Y>Yes</option></select></td></tr>
	<tr><td colspan=2 class=center><input type=button value='Grant Access' class=isubmit /></td></tr>
	</table>
	</form>
	<script type=text/javascript>
	$(function() {
		$('form[name=frm_db_dev_user] input:button.isubmit').click(function() {
			var form = 'form[name=frm_db_dev_user]';
			$(form).submit();
		});
	});
	</script>

	";
	
	



	break;
}	//end switch of action




?>




</body>

</html>
