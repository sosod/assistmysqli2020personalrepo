<?php
require_once '../header.php';
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");
//echo $db->getDBPrefix();

$db_name_prefix = $db->getDBPrefix();
$x = explode("_",$db_name_prefix);
$db_prefix = $x[0];
$db_initial = $x[1];
$ignore_databases = array("help","ignite4u","create","iblank");

$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix && !in_array($e[1],$ignore_databases)) {
		$dbs[] = substr($e[1],1,7);
	}
}

$folders = array();
$usage = array();
//ASSIST_HELPER::arrPrint($dbs);

$sql = "SELECT LOWER(cmpcode) as id, cmpcode, cmpname, cmpreseller, cmp_is_demo, cmpstatus FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' ORDER BY cmpcode";
$companys = $db->mysql_fetch_all_by_id($sql, "id");

//ASSIST_HELPER::arrPrint($companys);

?>
<style type="text/css" >
	table td {
		vertical-align: top; 
	}
</style>
<h1>Module Usage Report</h1>
<table>
	<tr>
		<th>Cmpcode</th>
		<th>Company Name</th>
		<th>Is Demo</th>
		<th>Reseller</th>
		<th>Modules</th>
	</tr>

<?php
foreach($companys as $cc => $c) {
	echo "
	<tr>
		<td>".strtoupper($cc)."</td>
		<td>".$c['cmpname']."</td>
		<td>".($c['cmp_is_demo']==0 ? "Client" : "Demo")."</td>
		<td>".$c['cmpreseller']."</td>
		<td>";
		if(in_array($cc,$dbs)) {
	$cdb = new ASSIST_DB("client",$cc);
	$modules = $cdb->mysql_fetch_all_by_id("SELECT * FROM assist_menu_modules ORDER BY modref","modref");
	unset($cdb);
	$locs = array();
	echo "
	<table>
		<tr><th>Location</th><th>Ref</th><th>Name</th><th>Status</th></tr>";
	foreach($modules as $m) {
		$locs[] = $m['modlocation'];
		if(!isset($usage[$m['modlocation']])) { $usage[$m['modlocation']] = array('Y'=>0,'N'=>0); }
		$usage[$m['modlocation']][$m['modyn']]++;
		echo "
		<tr>
			<td>".$m['modlocation']."</td>
			<td>".$m['modref']."</td>
			<td>".$m['modtext']."</td>
			<td>".$m['modyn']."</td>
		</tr>";
	}
	$folders = array_merge($folders,$locs);
	echo "</table>";
		} else {
			echo "ERROR! No database.";
		}
	echo "
		</td>
	</tr>";
}

$folders = array_unique($folders);
sort($folders,SORT_STRING);

/*
$all = array_unique(array_merge($dbs, array_keys($companys)));
sort($all);
//ASSIST_HELPER::arrPrint($all);
echo "<table><tr><th width=75px>CMPCODE</th><th width=75px>DB</th><th width=75px>CMP Record</th><th width=75px>CMP Status</th><th width=75px>ERROR?</th></tr>";
foreach($all as $cc) {
	$in_db = in_array($cc,$dbs);
	$in_cmp = isset($companys[$cc]);
	$stat = ($in_cmp) ? $companys[$cc]['cmpstatus'] : "-";
	if((!$in_db && $in_cmp && $stat=="Y") || ($in_db && (!$in_cmp || $stat=="N"))) {
		$err = true;
	} else {
		$err = false;
	}
	echo "
	<tr>
		<td class='center b'>$cc</td>
		<td class='center b' ".(!$in_db ? "style='color: #cc0001;'" : "").">".($in_db ? "Y" : "N")."</td>
		<td class='center b' ".(!$in_cmp ? "style='color: #cc0001;'" : "").">".($in_cmp ? "Y" : "N")."</td>
		<td class='center b' ".($stat!="Y" ? "style='color: #cc0001;'" : "").">".($stat)."</td>
		<td class='center b' ".($err ? "style='background-color: #cc0001; color: #ffffff;'" : "").">".($err ? "ERROR" : "-")."</td>
	</tr>";
}*/
?>
</table>
<h3>Modules In Usage</h3>
<table><tr><td>Location</td><td>Active</td><td>Inactive</td></tr>
	<?php
	foreach($folders as $f) {
		echo "<tr><td>".$f."</td><td ".($usage[$f]['Y']>0 ? "class='center isubmit'" : "class=center").">".$usage[$f]['Y']."</td><td ".($usage[$f]['N']>0 ? "class='center idelete'" : "class=center").">".$usage[$f]['N']."</td></tr>";
	}
	?>
</table>
<?php 
//ASSIST_HELPER::arrPrint($folders);
?>
</body>
</html>