<?php 
//error_reporting(-1);
echo "<h2>Copy Database</h2><ol>";

$src_db = strtolower($_REQUEST['src_db']);
$dest_db = strtolower($_REQUEST['dest_db']);
$rename_tbl = strtoupper($_REQUEST['rename_tbl'])=="Y" || $_REQUEST['rename_tbl']==1 ? true : false;
$drop_dest = isset($_REQUEST['drop_dest']) ? strtoupper($_REQUEST['drop_dest']) : "NO";
$is_drop_dest = isset($_REQUEST['drop_dest']) ? true : false;

//arrPrint($_REQUEST);
//echo "<h3>".$rename_tbl." :: ".($rename_tbl===true)."</h3>";

//Validate that source exists
echo "<li>Validating that source database '$src_db' exists... ";
	if(!in_array($src_db,$dbs)) {
		echo "<span class=idelete>failure</span> - Source '$src_db' does not appear to exist."; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
//Validate that destination does not exist
echo "<li>Validating that database does not already exist... ";
	if(in_array($dest_db,$dbs)) {
		if($is_drop_dest) {
			$sql = "DROP DATABASE ".$dest_db;
			$db->db_query($sql);
			echo "<span class=isubmit>success</span> - Database $dest_db has been dropped.</li>";
		} else {
			echo "</li></ul>
			<form name=db_copy method=post action=manage_modules.php>
				<input type=hidden name=act value='COPY_DB' />
				<input type=hidden name=src_db value='$src_db' />
				<input type=hidden name=dest_db value='$dest_db' />
				<input type=hidden name=drop_dest value='NO' />
				<input type=hidden name=rename_tbl value='$rename_tbl' />
				<div class=center>
					<h2 class=idelete>Database $dest_db exists</h2>
					<p>Do you want to drop it?</p>
					<p><input type=button name=drop_dest_yes value=Yes class=idelete />  <input type=button name=drop_dest_no value=No class=isubmit /></p>
				</div>
			</form>
			<script type=text/javascript>
			$(function() {
				$('input:button').click(function() {
					var v = $(this).val();
					if(v=='Yes') {
						$('input:hidden[name=drop_dest]').val('YES');
					} else {
						$('input:hidden[name=act]').val('VIEW');
					}
					$('form[name=db_copy]').submit();
				});
			});
			</script>
			";
			die();
		}
	} else {
		echo "<span class=isubmit>success</span> - $dest_db does not exist.</li>";
	}
//Create blank database
echo "<li>Creating blank database '$dest_db'... ";
	$sql = "CREATE DATABASE IF NOT EXISTS `".$dest_db."`";
	$db->db_query($sql);
	echo "done!</li>";
//Get/Copy base tables from blank database
echo "<li>Creating tables... ";
	$sql = "SELECT TABLE_NAME FROM information_schema.TABLES
			WHERE TABLE_SCHEMA = '".$src_db."'";
	$tables = $db->mysql_fetch_fld_one($sql,"TABLE_NAME");
//	arrPrint($tables);
	$d = explode("_",$dest_db);
	$d_cmpcode = strtolower(substr($d[1],1,strlen($d[1])));
	$dest = new ASSIST_DB($d_cmpcode);
	$db_ref = "assist_".$d_cmpcode; 
	$s = explode("_",$src_db);
	$s_cmpcode = strtolower(substr($s[1],1,strlen($s[1])));
	$src = new ASSIST_DB($s_cmpcode);
	//echo "<li class=b>".$db_ref."</li>";
	foreach($tables as $old_tbl) {
		echo "<li>".$old_tbl;
if(stripos($old_tbl,"assist_dra0001_sp12b_")===false) {
		if($rename_tbl===true) {
			//echo " ::> <span class=isubmit>RENAMING!!!</span>";
			$etbl = explode("_",$old_tbl); 
			//echo "  ::>  ".$etbl[1]."  ::>  ".$s_cmpcode;
			if($etbl[1]==$s_cmpcode) {
				unset($etbl[0]); unset($etbl[1]);
				$new_tbl = strtolower($db_ref."_".implode("_",$etbl));
			} else {
				$new_tbl = strtolower(implode("_",$etbl));
			}
		} else {
			$new_tbl = $old_tbl;
		}
		echo "  ::>  ".$new_tbl."</li>";
		//delete existing table (in case of interrupted creation process)
		$sql = "DROP TABLE IF EXISTS `".$dest_db."`.`".$new_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_query($sql);
		//create table
		$sql = "CREATE TABLE `".$dest_db."`.`".$new_tbl."` LIKE `".$src_db."`.`".$old_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_query($sql);
		//insert data
		$sql = "INSERT INTO `".$dest_db."`.`".$new_tbl."` SELECT * FROM `".$src_db."`.`".$old_tbl."`";
		//echo "<li>".$sql."</li>";
		$dest->db_insert($sql);
}
	}
	echo "done!</li>";
//Reset passwords
echo "<li>Reset user passwords / emails for Dev?... ";
if(isset($_REQUEST['reset_pwd']) && strtoupper($_REQUEST['reset_pwd'])=="Y") {
	echo "Yes...";
	if($rename_tbl===true) { $reset_tbl = "assist_".$d_cmpcode."_timekeep"; } else { $reset_tbl = "assist_".$s_cmpcode."_timekeep"; }
	$dest->db_update("UPDATE `".$dest_db."`.`".$reset_tbl."` SET tkemail = 'actionit.actionassist@gmail.com', tkpwd = '417373697374313233'");
	echo "done!</li>";
} else {
	echo "NO</li>";
}
if(!isset($_REQUEST['src']) || $_REQUEST['src']!="CURL") {
	echo "<P class=b>Copy process completed successfully.</p><script type=text/javascript>  document.location.href = 'manage_modules.php?r[]=ok&r[]=".urlencode("$src_db has been successfully copied to $dest_db ")."';</script>";
} else {
	echo "<P class=b>Copy process completed successfully.</p>";
}
//echo "<p>end of page</p>";
?>