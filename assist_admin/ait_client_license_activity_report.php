<?php
include 'inc_header.php';
require_once("../module/autoloader.php");
error_reporting(-1);
$alphabet = isset($_REQUEST['character']) ? strtolower($_REQUEST['character']) : "a";
?>
<h1>Action iT Module License Activity Report</h1>
<?php
$mdb = new ASSIST_DB("master");

$sql = "SELECT * FROM assist_company WHERE cmp_is_demo = 0 AND cmpstatus = 'Y'";
$non_demo_companies = $mdb->mysql_fetch_all_by_id($sql, "cmpcode");
//arrPrint($non_demo_companies);

$sql = "SELECT * FROM assist_billing_report_company_history WHERE by_cmpcode = 'IASSIST' ORDER BY date_drawn DESC, cmpcode ASC";
$billing_logs = $mdb->mysql_fetch_all($sql);

$cc_lic = array();
$cc2_lic = array();
$mod_count = array();
$cc_count = array();

foreach($billing_logs as $b) {
	$cc = $b['cmpcode'];
	if(isset($non_demo_companies[$cc])) {
		$d = strtotime($b['date_drawn']);
		$dY = date("Y",$d)*1;
		$dM = date("m",$d)*1;
	//	echo "<p class=b>".$b['cmpcode']." = > ".$b['date_drawn']."</p>";
		$details = unserialize($b['details']);
		if(!isset($cc_lic[$dY][$dM][$cc])) {
			$cc_lic[$dY][$dM][$cc] = $details['total_lic']; 
			if(!isset($mod_count[$dY][$dM])) {
				$mod_count[$dY][$dM] = $details['mod_count']; 
			} else {
				$mod_count[$dY][$dM]+= $details['mod_count']; 
			}
			if(!isset($cc_count[$dY][$dM])) {
				$cc_count[$dY][$dM] = 1; 
			} else {
				$cc_count[$dY][$dM]+= 1; 
			}

		}
		if(!isset($cc2_lic[$cc][$dY][$dM])) {
			$cc2_lic[$cc][$dY][$dM] = $details['total_lic']; 
		}
	}
		//arrPrint($details);
//	$mods = unserialize($b['modules']);
//	arrPrint($mods);
}

$licences = array();
foreach($cc_lic as $bY => $l) {
	$licences[$bY] = array();
	foreach($l as $bM => $x) {
		$licences[$bY][$bM] = 0;
		foreach($x as $b) {
			$licences[$bY][$bM] += $b;
		}
	}
}


$start_year = min(array_keys($licences));

?>

<table>
	<tr>
		<th>Month</th>
		<th>license count</th>
		<th>module count</th>
		<th>client count</th>
	</tr>
		<?php
		$start = 0;
		for($y=$start_year;$y<=date("Y");$y++) {
			for($m=1;$m<=12;$m++){
				$start = (isset($licences[$y][$m]) ? $licences[$y][$m] : $start);
				$mod = (isset($mod_count[$y][$m]) ? $mod_count[$y][$m] : 0);
				$cl = (isset($cc_count[$y][$m]) ? $cc_count[$y][$m] : 0);
				echo "
				<tr>
					<td>".date("M Y",mktime(12,0,0,$m,1,$y))."</td>
					<td>".$start."</td>
					<td>".$mod."</td>
					<td>".$cl."</td>
				</tr>
				";
			}
		}
		?>
</table>


<p>&nbsp;</p>

<table>
	<tr>
		<th></th>
	<?php
	for($y=2012;$y<=2016;$y++) {
		echo "<th colspan=12>".$y."</th>";
	}
	?>
	</tr>
	<tr>
		<th></th>
	<?php
	for($y=2012;$y<=2016;$y++) {
		for($m=1;$m<=12;$m++) {
			echo "<th>".date("M Y",mktime(12,0,0,$m,1,$y))."</th>";
		}
	}
	?>
	</tr>
	<?php
	foreach($cc2_lic as $cc => $lic) {
		echo "
		<tr>
		<td class=b>".$cc."</td>";
		for($y=2012;$y<=2016;$y++) {
			for($m=1;$m<=12;$m++) {
				$d = isset($lic[$y][$m]) ? $lic[$y][$m] : "-";
				echo "<td>".$d."</td>";
			}
		}
		echo "</tr>";
	}
	?>
</table>

<?php

arrPrint($licences);
arrPrint($cc_lic);

arrPrint($cc_count);

arrPrint($mod_count);



/*$sql = "SELECT * FROM assist_company WHERE cmp_is_demo = 0";
$non_demo_companies = $mdb->mysql_fetch_all_by_id($sql, "cmpcode");
unset($mdb);

$db_prefix = "adev";
$db_name_prefix = $db_prefix."_i";

$ignore_databases = array("help","ignite4u","create","iblank","compare1","icassist");
$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}
mysql_close($link);

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix && !in_array($e[1],$ignore_databases)) {
		$dbs[] = substr($e[1],1,7);
	}
}

//arrPrint($dbs);
//arrPrint($non_demo_companies);

$users = array();
$module_license_activity_logs = array();

$ias_modules = array("PMSPS","PP","AQA","TD");

foreach($dbs as $cmpcode) {
	if(isset($non_demo_companies[strtoupper($cmpcode)]) && substr($cmpcode,0,1)==$alphabet) {
		$cdb = new ASSIST_DB("client",$cmpcode);
		$sql = "SELECT *, '$cmpcode' as cc FROM assist_".strtolower($cmpcode)."_timekeep WHERE tkuser <> 'admin' AND tkuser <> 'support'";
		$u = $cdb->mysql_fetch_all_by_id($sql,"tkid");
		$sql = "SELECT * FROM assist_".strtolower($cmpcode)."_timekeep_log WHERE lsql LIKE '%menu_modules_users%' ORDER BY id DESC";
		$logs = $cdb->mysql_fetch_all_fld2($sql, "ref", "id");
		$sql = "SELECT * FROM assist_".strtolower($cmpcode)."_menu_modules_users WHERE usrmodref NOT IN ('".implode("','",$ias_modules)."')";
		$mmu = $cdb->mysql_fetch_array_by_id($sql, "usrtkid", "usrmodref");
		unset($cdb);
	}
}

*/
?>
</body>
</html>