<?php
require_once '../header.php';
require_once('../library/class/autoload.php');
$db = new ASSIST_DB("master");
$db_prefix = "dev";
$db_name_prefix = $db_prefix."_i";

$action_types = array(
	'DATA' => array('UPDATE','INSERT','DELETE'),
	'STRUCTURE' => array('CREATE','ALTER','DROP'),
);
$ignore_databases = array("help","ignite4u","create");
$dbs = array();
$raw_dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $raw_dbs[] = $row->Database;
}

foreach($raw_dbs as $d) {
	$e =explode("_",$d);
	if($e[0]==$db_prefix) {
		$dbs[] = $d;
	}
}

$ah = new ASSIST_HELPER();
//$ah->arrPrint($dbs);

?>
<h1>Ignite Assist Admin >> Development Database Update</h1>
<?php


$data['insertdate'] = date("d M Y");
$data['inserttime'] = date("H:i:s");
$data['insertuser'] = $_SESSION['tid'];
$data+=$_REQUEST;
$modloc = $data['modloc'];
$table = $data['db_table'];

$data['act_sql'] = str_replace("\r\n","  ",$data['act_sql']);

$filename = $db_prefix."_db_changes.txt";
$echo = (!file_exists($filename) ? implode(chr(9),array_keys($data)) : "").chr(10).implode(chr(9),$data);
			$file = fopen($filename,"a");
			fwrite($file,$echo);
			fclose($file);
			
		$modloc = $data['modloc'];
		$table = $data['db_table'];
			
echo "<table><tr><th>Database</th><th>Company Code</th><th>Action</th></tr>";
foreach($dbs as $d) {
	$x = explode("_",$d);
	$x = $x[1];
	if(!in_array($x,$ignore_databases)) {
		$cc = substr($x,-(strlen($x)-1));  $cc = strtolower($cc);
		echo "<tr><td>$d</td><td>$cc</td><td><ul>";
		$cdb = new ASSIST_DB("other",$db_name_prefix.$cc);
//		$mm = $cdb->mysql_fetch_all("SELECT * FROM assist_menu_modules WHERE modlocation = '$modloc'");
if($modloc!="S" && $modloc !="GEN") {
		$mm = $cdb->mysql_fetch_all("SELECT * FROM assist_menu_modules WHERE modlocation = '$modloc'");
} elseif($modloc=="S") {
		$mm = array(array('modtext'=>"Master Setup",'modlocation'=>"S",'modref'=>"MASTER"));
} else {
		$mm = array(array('modtext'=>"General / Structural",'modlocation'=>"GEN",'modref'=>"GEN"));
}
		if(count($mm)>0) {
			echo "<li>Module $modloc found on the database</li>";
			foreach($mm as $m) {
				$modref = $cc=="blank" ? strtolower($modloc) : strtolower($m['modref']);
				echo "<li>Processing module: ".$m['modtext']." (".$modref.")<ul>";
//				$dbref = strtolower("assist_".$cc."_".$modref);
//				$tbl = $dbref."_".strtolower($table);
				if($modloc=="GEN") {
					$dbref = "";
					$tbl = $dbref.strtolower(str_replace("|cc|",$cc,$table));
				} else {
					$dbref = strtolower("assist_".$cc."_".$modref);
					$tbl = $dbref."_".strtolower($table);
				}
				$t = $cdb->mysql_fetch_all("SHOW TABLES LIKE '$tbl'");
				$sql = stripslashes($data['act_sql']);
				$sql = str_replace("|#|",$dbref,$sql);
				$sql = str_replace("|cc|",$cc,$sql);
				$sql = trim($sql);
				while(strpos($sql,"  ")!==false) {
					$sql = str_replace("  "," ",$sql);
				}
				$s = explode(" ",$sql);
				$s[0] = strtoupper($s[0]);
				$act_type = in_array(strtoupper($s[0]),$action_types['STRUCTURE']) ? "STRUCTURE" : "DATA";
				if(count($t)>0) {
					if($act_type=="STRUCTURE" && $s[0]=="CREATE") {
						echo "<li class=idelete>Can't create $tbl - it already exists</li>";
					} else {
						echo "<li>Table $tbl found.</li>";
						echo "<li>SQL to be run: ".$sql."</li>";
						if($act_type=="DATA") {
							if($s[0]=="INSERT") {
								$mar = $cdb->db_insert($sql);
								if($mar>0) {
									echo "<li class=isubmit>Row(s) inserted.  Last insert ID: ".$mar.".</li>";
								} else {
									echo "<li class=idelete>No rows inserted.</li>";
								}
							} else {
								$mar = $cdb->db_update($sql);
								if($mar>0) {
									echo "<li class=isubmit>".$mar." rows affected.</li>";
								} else {
									echo "<li class=idelete>No rows affected.</li>";
								}
							}
						} else {	//structure
							$act = false;
							$fld = str_replace("`","",$s[4]);
							if($s[0]=="ALTER") {
								$cols = $cdb->mysql_fetch_all("SHOW COLUMNS FROM ".$tbl." WHERE Field LIKE '".$fld."'");
								switch($s[3]) {
									case "ADD":
										if(count($cols)>0) {
											echo "<li class=idelete>Can't add new column $fld - it already exists.</li>";
										} else {
											$act = true;
										}
										break;
									case "CHANGE":
										if(count($cols)>0) {
											$act = true;
										} else {
											echo "<li class=idelete>Can't change column $fld - it doesn't exist.</li>";
										}
										break;
									case "DROP":
										if(count($cols)>0) {
											$act = true;
										} else {
											echo "<li class=idelete>Can't change column $fld - it doesn't exist.</li>";
										}
										break;
									default: $act = true; break;
								}
							} elseif($s[0]=="DROP") {
								$act = true;
							}
							if($act) {
								//run the sql script!!
								$rs = $cdb->db_query($sql); unset($rs);
								echo "<li class=isubmit>SQL run!</li>";
							}
						}
					}
				} else {
					if($act_type=="STRUCTURE" && $s[0]=="CREATE") {
						echo "<li>SQL: ".$sql."</li>";
						$rs = $cdb->db_query(stripslashes($sql)); unset($rs);
						echo "<li class=isubmit>SQL run!</li>";
					} else {
						echo "<li class=idelete>Table $tbl not found!</li>";
					}
				}
				echo "</ul></li>";
			}
		} else {
			echo "<li class=iinform>No module $modloc found.</li>";
		}
		echo "</ul></td></tr>";
	}
}
echo "</table>";
$ah->displayGoBack("dev_db_updates.php");

?>




</body>

</html>
