<?php
    include("inc_ignite.php");
    include("inc_polref.php");
    
    function printr($array = null){
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }
    function getPolId($str){
           $priKeys = array();
           $sql = "SELECT ap.polid FROM assist_policies ap WHERE ap.polid LIKE '$str%'";
           include("inc_db_con.php");
           while($row = mysql_fetch_array($rs)){
               $keyArr = explode("-",$row['polid']);
               array_push($priKeys,$keyArr[1]);
           }
           //sort them in reverse order:the larget is the first
           rsort($priKeys);
           $polid = $priKeys[0] + 1;
           return $polid;
    }
    function generatePoliciesPriKey($polref){
           switch ($polref){
               case 'PPA':
                   $polid = getPolId('ACC');
                    $polid = 'ACC-'.($polid < 10 ? 0 : "").$polid;
                    break;
               case 'PPH':
                   $polid = getPolId('HR');
                   $polid = 'HR-'.($polid < 10 ? 0 : "").$polid;
                   break;
               case 'PPI':
                   $polid = getPolId('IT');
                   $polid = 'IT-'.($polid < 10 ? 0 : "" ).$polid;
                   break;
           }
           return $polid;
    }
    function checkPolicyDuplicates($polref,$poltitle){
        $sql = "SELECT assist_policies.polid,assist_policies.polref,assist_policies.poltitle
                    FROM assist_policies WHERE assist_policies.polref='$polref' AND assist_policies.poltitle='$poltitle'";
        $cmpcode = $_SESSION['cc'];
        include("inc_db_con.php");
        if(mysql_num_rows($rs) > 0){
            $row = mysql_fetch_array($rs);
            $sql = "DELETE FROM assist_".$cmpcode."_policies WHERE polassistid='$polref'";
            include("inc_db_con.php");
            return $row['polid'];
        }
        return false;
    }
    function addObjectives($polid){
        $cmpcode = $_SESSION['cc'];
        foreach($_POST['objtext'] as $key => $obj){
                       $objorder++;
                       $sql = "INSERT INTO assist_policies_objectives
                                              (objpolid,objorder,objtext)
                                        VALUES('$polid',$objorder,'$obj')";
                      
                       include("inc_db_con.php");
                       $lastInsertID = mysql_insert_id();
                       $sql = "INSERT INTO assist_".$cmpcode."_policies_objectives (objassistid,objyn) VALUES ('$lastInsertID','Y')";
                       include("inc_db_con.php");
        }
    }
    function addActionLine($polid){
        error_reporting(E_ALL);
        $cmpcode = $_SESSION['cc'];
       // print_r($cmpcode);
       // print_r($polid);
        $sql = "SELECT lineorder FROM assist_policies_lines WHERE linepolid='$polid' ORDER BY lineorder DESC LIMIT 1";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $lineorder = $row['lineorder'] + 1;
        foreach($_POST['linetext'] as $key => $line){
                        $lineorder++;
                        $sql = "INSERT INTO assist_policies_lines (linepolid,lineorder,linetext) VALUES ('$polid',$lineorder,'$line')";
                        include("inc_db_con.php");
                        $lastInsertID = mysql_insert_id();
                        $sql = "INSERT INTO assist_".$cmpcode."_policies_lines (lineassistid,lineyn) VALUES ('$lastInsertID','Y')";
                        include("inc_db_con.php");
        }
    }
    if(!empty($_POST)){
        $objorder = 0;
        $polref = $_POST['polref'];
        $polsubcateid = $_POST['polsubcateid'];
        
        $polid = null;
        if(isset($_POST['polid'])){
            $polid = $_POST['polid'];
            //just add new objectives and action lines if any.
            if(isset($_POST['objtext'])){
                    addObjectives($polid);
            }
            if(isset($_POST['linetext'])){
                    addActionLine($polid);
            }
        }else{
            if(isset($_POST['policytitle'])){
                $poltitle = $_POST['policytitle'];
            }
            //check if the same policy is not already in the system
            $polidKey = checkPolicyDuplicates($polref,$poltitle);
            if(!$polidKey){
               //generate new primary key for assist_policies
                $polid = generatePoliciesPriKey($polref);
                $poladdate = time();
                $poladddate = time();
                $polorder = 0;
                $sql = "INSERT INTO assist_policies
                                        (polid,polref,poltitle,polsubcateid,pollines,polorder,poladddate,polmoddate)
                                 VALUES ('$polid','$polref','$poltitle',$polsubcateid,0,$polorder,'$poladddate','$poladddate')";
                include("inc_db_con.php");
                $sql = "INSERT INTO assist_".$cmpcode."_policies
                                 (polassistid,polyn,poleffectdate,polpubyn) VALUES
                                 ('$polid','N','$poladddate','N')";
                include("inc_db_con.php");
                if(isset($_POST['objtext'])){
                    addObjectives($polid);
                }if(isset($_POST['linetext'])){
                    addActionLine($polid);
                }
            }
           
        }
        
       
    }
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<style type="text/css">
    #targCont table,#targCont td{
        border: none;
    }
</style>
</head>
<script language=JavaScript>
    function createXMLHttpRequestObject(){
				var xmlHttp;
				try{
					xmlHttp = new XMLHttpRequest();
				}catch(e){
					var xmlHttpVersions = new Array('MSXML2.XMLHTTP.6.0','MSXML2.XMLHTTP.5.0',
												'MSXML2.XMLHTTP.4.0','MSXML2.XMLHTTP.3.0','MSXML2.XMLHTTP','Microsoft.XMLHTTP');
					for(var i = 0; i < xmlHttpVersions.length && !xmlHttp; i++){
						try{
							xmlHttp = new ActiveXObject(xmlHttpVersions[i]);
						}catch(e){}
					}
				}
				if(!xmlHttp){
					alert("Error creating the xmlHttpRequestObject");
				}else{
					return xmlHttp;
				}
			}
			var xmlHttp = createXMLHttpRequestObject();
			function process(){
                            var url = "subcat_options.php";
                            var modulemenu = document.getElementById('polref');
                            var polref = modulemenu.options[modulemenu.selectedIndex].value;
                            var params = "polref="+polref;
				if(xmlHttp){
					try{
						xmlHttp.open("POST",url, true);
                                                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                xmlHttp.setRequestHeader("Content-length", params.length);
                                                xmlHttp.setRequestHeader("Connection", "close");
						xmlHttp.onreadystatechange = handleRequestStateChange;
						xmlHttp.send(params);
					}catch(e){
						alert("Can't connect to server:"+ e.toString());
					}
				}
			}
                        function getPolicies(){
                            var url = "get_policies.php";                            
                            var modulemenu = document.getElementById('polref');                           
                            var polref = modulemenu.options[modulemenu.selectedIndex].value;
                            var subcatemenu = document.getElementById("polsubcateid");
                            
                            var polsubcateid = subcatemenu.options[subcatemenu.selectedIndex].value;
                            var params = "polref="+polref+"&polsubcateid="+polsubcateid;
				if(xmlHttp){
					try{
						xmlHttp.open("POST",url, true);
                                                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                xmlHttp.setRequestHeader("Content-length", params.length);
                                                xmlHttp.setRequestHeader("Connection", "close");
						xmlHttp.onreadystatechange = handleRequest;
						xmlHttp.send(params);
					}catch(e){
						alert("Can't connect to server:"+ e.toString());
					}
				}
			}
                        function handleRequest(){
                            if (xmlHttp.readyState == 4){
					if (xmlHttp.status == 200){
						try{
							handleResponse();
						}catch(e){
							alert("Error reading the response:"+e.toString());
						}
					}else {
						alert("There was a problem retrieving the data:"+toString());
					}
				}
                        }
                        function handleResponse(){
                            var xmlResponse = xmlHttp.responseXML;
                            //alert(xmlResponse);
                            if(!xmlResponse || !xmlResponse.documentElement)
                                    throw("Invalid XML structure:\n"+xmlHttp.responseText);
                           
                            var rootNodeName = xmlResponse.documentElement.nodeName;
                            if(rootNodeName == "parseerror")
                                    throw("Invalid XML structure");
                            xmlRoot = xmlResponse.documentElement;
                            var options = xmlRoot.getElementsByTagName("option");
                            var html = '<select name="polid" id="polid"><option value=-1>--Select Policy--</option>';
                           
                            for(var i=0;i<options.length;i++){
                                var node = options.item(i);
                                html += '<option value='+node.getAttribute('polid')+'>'+node.firstChild.data+'</option>';
                            }
                            html += '</select>';
                            var doc = document.getElementById('policyChoice');
                           // alert(html);
                            doc.innerHTML = html;
                        }
			function handleRequestStateChange(){
				if (xmlHttp.readyState == 4){
					if (xmlHttp.status == 200){
						try{
							handleServerResponse();
						}catch(e){
							alert("Error reading the response:"+e.toString());
						}
					}else {
						alert("There was a problem retrieving the data:"+toString());
					}
				}
			}

			function handleServerResponse(){
				var xmlResponse = xmlHttp.responseXML;
                                //alert(xmlResponse);
				if (!xmlResponse || !xmlResponse.documentElement)
					throw("Invalid XML structure:\n" + xmlHttp.responseText);
				var rootNodeName = xmlResponse.documentElement.nodeName;
				if (rootNodeName == "parsererror")
					throw("Invalid XML structure");
				xmlRoot = xmlResponse.documentElement;
				var options = xmlRoot.getElementsByTagName("option");
				var html = '<select name="polsubcateid" id="polsubcateid" onchange="defaultlinks()"><option value=-1></option>';
				for(var i=0;i<options.length;i++){
					var node = options.item(i);
					html += '<option value='+node.getAttribute('value')+'>'+node.firstChild.data+'</option>';
				}
				html += '</select>';
				var doc = document.getElementById('target');
				doc.innerHTML = html;
			}
                        function addNewPolicy(){
                          document.getElementById("policyChoice").innerHTML = 'Enter Policy Title:<br />'+
                                        '<input type="text" name="policytitle" id="policytitle" size="80" />';
                        }
                        function existingPolicy(){
                            getPolicies();
                        }
                        function objTextArea(){
                          var rowNumber = parseInt(document.getElementById("rowNumber").value);
                          if(rowNumber > 0){
                                var tar = rowNumber - 1;
                                var targetEle = document.getElementById("obj_"+tar);
                                var parent = targetEle.parentNode;
                                var newDiv = document.createElement("div");
                                newDiv.setAttribute("id", "obj_"+rowNumber);
                                newDiv.setAttribute("style","font-family:Verdana,Arial,Helvetica,sans-serif;font-size: 8pt;");
                                newDiv.innerHTML = 'Objective Line '+ (rowNumber+1) +'<br /><textarea cols="80" rows="1" name="objtext[]" ></textarea>';
                                parent.insertBefore(newDiv,targetEle);
                            }else {
                                var html = '<div id=obj_'+rowNumber+'>Objective Line '+(rowNumber+1)+'<br /><textarea cols="80" rows="1" name="objtext[]" ></textarea></div>';
                                document.getElementById("objectivestext").innerHTML = html;
                            }
                            rowNumber = rowNumber + 1;
                            document.getElementById("rowNumber").value = rowNumber;
                        }
                        function lineTextArea(){
                            var lineRowNumber = parseInt(document.getElementById("lineRowNumber").value);
                          if(lineRowNumber > 0){
                                var tar = lineRowNumber - 1;
                                var targetEle = document.getElementById("line_"+tar);
                                var parent = targetEle.parentNode;
                                var newDiv = document.createElement("div");
                                newDiv.setAttribute("id", "line_"+lineRowNumber);
                                newDiv.setAttribute("style","font-family:Verdana,Arial,Helvetica,sans-serif;font-size: 8pt;");
                                newDiv.innerHTML = 'Action Line '+ (lineRowNumber+1) +'<br /><textarea cols="80" rows="1" name="linetext[]" ></textarea>';
                                parent.insertBefore(newDiv,targetEle);
                            }else {
                                var html = '<div id=line_'+lineRowNumber+' style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;">Action Line '+(lineRowNumber+1)+'<br /><textarea cols="80" rows="1" name="linetext[]" ></textarea></div>';
                                document.getElementById("linestext").innerHTML = html;
                            }
                            lineRowNumber = lineRowNumber + 1;
                            document.getElementById("lineRowNumber").value = lineRowNumber;
                        }
                        function createObjective(){
                            //var parts = lineRowNumber.substr("line_",0,5);
                            //lineRowNumber = parts[1];
                           var lineRowNumber = parseInt(document.getElementById("lineRowNumber").value);
                            var rowNumber = parseInt(document.getElementById("rowNumber").value);
                            if(lineRowNumber != 0){
                                document.getElementById("objectivestext").innerHTML ="";
                            }
                            if(rowNumber == 0){
                                rowNumber = rowNumber + 1;
                                var html = '<div id="'+rowNumber+'"><textarea cols="80" rows="1" name="objtext[]" ></textarea></div>';
                                document.getElementById("objectivestext").innerHTML = html;
                                document.getElementById("lineRowNumber").value = 0;
                            }else {
                                var newDiv = document.createElement("div");
                                var targetEle = document.getElementById(rowNumber);
                                rowNumber = rowNumber + 1;
                                newDiv.setAttribute("id", rowNumber);
                                newDiv.innerHTML = '<textarea cols="80" rows="1" name="objtext[]" ></textarea>';
                                var parent = document.getElementById("objectivestext");

                                parent.insertBefore(newDiv,targetEle);
                            }
                            document.getElementById("rowNumber").value = rowNumber;
                        }
                        function createActionLines(){
                            var rowNumber = parseInt(document.getElementById("rowNumber").value);
                            if(rowNumber != 0){
                                 document.getElementById("linestext").innerHTML ="";
                                 document.getElementById("rowNumber").value = 0;
                            }
                            var lineRowNumber = parseInt(document.getElementById("lineRowNumber").value);                            
                            if(lineRowNumber == 0){
                                lineRowNumber = lineRowNumber + 1;
                                var html = '<div id="'+lineRowNumber+'"><textarea cols="80" rows="1" name="linetext[]" ></textarea></div>';
                                document.getElementById("linestext").innerHTML = html;
                            }else {
                                var newDiv = document.createElement("div");
                                var targetEle = document.getElementById(lineRowNumber);
                                lineRowNumber = lineRowNumber + 1;
                                newDiv.setAttribute("id", lineRowNumber);
                                newDiv.innerHTML = '<textarea cols="80" rows="2" name="linetext[]" ></textarea>';
                                var parent = document.getElementById("linestext");
                                parent.insertBefore(newDiv,targetEle);
                            }
                            document.getElementById("lineRowNumber").value = lineRowNumber;
                        }
                        function defaultlinks(){                          
                            document.getElementById("policyChoice").innerHTML='<table cellpadding="5">'+
                                '<tr>'+
                                    '<td id="policytitletd"><a href="javascript:addNewPolicy()">New Policy</a></td>'+
                                    '<td id="existingpolicy"><a href="javascript:existingPolicy()">Existing Policy</a></td>'+
                                '</tr>'+
                            '</table>';
                            document.getElementById("objectivestext").innerHTML = null;
                            document.getElementById("linestext").innerHTML = null;
                            document.getElementById("rowNumber").value = 0;
                            document.getElementById("lineRowNumber").value = 0;
                        }
                        function checkForm(){
                            var message = '';

                            var polref = document.getElementById("polref").value;
                            var polsubcateid = document.getElementById("polsubcateid").value;
                            if(polref == '-1'){
                                message += 'Please select the relevent module.\n';
                            }
                            if(polsubcateid == '-1'){
                                message += 'Please select the relevent subcategory.\n';
                            }
                            if(!document.forms['policies_procedures'].elements['policytitle'] && !document.forms['policies_procedures'].elements['polid']){
                                message += 'Please click on New Policy or Existing Policy\n';
                            }
                            if(document.forms['policies_procedures'].elements['policytitle'] && document.forms['policies_procedures'].elements['policytitle'].value == ''){
                                message += 'Policy title cannot be left empty.\n';
                            }
                            if(document.forms['policies_procedures'].elements['polid'] && document.forms['policies_procedures'].elements['polid'].value == '-1'){
                              //if(document.forms['policies_procedures'].elements['polid'].value == '-1'){
                                    message += 'Please select a policy.\n';
                               }
                            if(message != ''){
                                alert(message);
                                return false;
                            }
                            return true;
                        }
</script>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
        <?php
            $url = 'http://'.$_SERVER['SERVER_NAME'].dirname($_SERVER['REQUEST_URI']).'/admin_preview_policy.php';
        ?>
	<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
		<h1 class=fc>Policies and Procedures: <?php echo($polheading); ?></h1>
                <form onsubmit="return checkForm()" id="policies_procedures" name="policies_procedures" method="post" action="<?php echo($_SERVER['PHP_SELF']);?>"  >
                    <table cellpadding="5px">
                        <tr>
                            <!--<td class="tdheaderl">Title</td>
                            <td ><input type="text" name="poltitle" size="50"/></td>-->
                        </tr>
                        <tr>
                        <td class="tdheaderl">Module</td>
                        <?php
                            $sql = "Select m.modref,m.modtext from assist_menu_modules m Where modtext LIKE 'Policies%' AND modref LIKE 'PP%'order by modtext asc";
                            include("inc_db_con.php");
                        ?>
                        <td >

                            <select  id="polref" name="polref" onchange="process()">
                                <option value="-1">Select Module</option>
                                <option value="PPH">Human Resources</option>
                                <option value="PPA">Finance</option>
                                <option value="PPI">Information Technology</option>
                               
                            </select>
                        </td>
                        </tr>
                        <tr>
                            <?php
                                $sql = "Select s.subid, s.subtitle from assist_policies_subcategories s order by s.subtitle asc";
                                include("inc_db_con.php");
                            ?>
                            <td  class="tdheaderl">Sub Category</td>
                            <td>
                                <div id="target">
                                    <select  name="polsubcateid" id="polsubcateid" onchange="defaultlinks()">
                                    <option value="-1">Sub-Category</option>
                                    <?php while($row = mysql_fetch_array($rs)) {?>
                                        <option value="<?php echo($row['subid']);?>" ><?php echo($row['subtitle']);?></option>
                                    <?php }?>
                                </select>
                                </div>
                            </td>
                        </tr>
                        
                    </table>
                    <br />
                    <div id="targCont">
                        <div id="policyChoice">
                            <table cellpadding="5">
                                <tr>
                                    <td id="policytitletd"><a href="javascript:addNewPolicy()">New Policy</a></td>
                                    <td id="existingpolicy"><a href="javascript:existingPolicy()">Existing Policy</a></td>
                                </tr>
                            </table>
                        </div>
                        <div id="objectivestext" ></div>
                        <div id="linestext" ></div>
                        <table>
                            <tr id="opBtns">
                                <td><input type="button" name="addObj" id="addObj" value="Add Objective" onclick="objTextArea()"/>
                                <input type="button" name="addAction" id="addAction" value="Add Action" onclick="lineTextArea()" />
                                <input type="submit" name="saveBtn" id="saveBtn" value="Save" /></td>
                            </tr>
                        </table>
                        
                    </div>
                    <input type="hidden" name="rowNumber" id="rowNumber" value="0" />
                    <input type="hidden" name="lineRowNumber" id="lineRowNumber" value="0" />
                </form>

	</body>
</html>
