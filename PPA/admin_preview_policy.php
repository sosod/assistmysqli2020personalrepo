<?php
include("inc_ignite.php");
$c = $_GET['c'];
include("inc_polref.php");

$a = $_POST['a'];
$adm = $_POST['adm'];
$sql = "SELECT cmplogo FROM assist_company_profile WHERE cmpcode = '$cmpcode' AND cmpstatus = 'Y'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$cmplogo = $row['cmplogo'];

if($a == "adm" && strlen($adm) > 3)
{
    $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$adm."' WHERE ref = '".$polref."' AND refid = 0";
    include("inc_db_con.php");
        $tsql = $sql;
        $tref = $polref;
        $trans = "Updated ".$polref." setup administrator.";
        include("inc_transaction_log.php");
}
include("inc_padmin.php");
global $uploadfile,$data;
if(isset($_POST) && $_POST['accept'] == 'Accept'){
    global $uploadfile,$polsubcateid;
    $polsubcateid = $_POST['polsubcateid'];
    $polref = $_POST['polref'];
    //print_r($polref);die;
    $uploadfile = $_POST['uploadfile'];
    
    function getPolId($str){
            $priKeys = array();
           $sql = "SELECT ap.polid FROM assist_policies ap WHERE ap.polid LIKE '$str%'";
           //echo $sql;die;
           include("inc_db_con.php");
           while($row = mysql_fetch_array($rs)){
               $keyArr = explode("-",$row['polid']);
               array_push($priKeys,$keyArr[1]);
           }
           rsort($priKeys);
           $polid = $priKeys[0] + 1;
           //echo $polid;die;
           return $polid;
       }
    function generatePoliciesPriKey(){
           global $polref;
           switch ($polref){
               case 'PPA':
                   $polid = getPolId('ACC');
                    $polid = 'ACC-'.($polid < 10 ? 0 : "").$polid;
                    break;
               case 'PPH':
                   $polid = getPolId('HR');
                   $polid = 'HR-'.($polid < 10 ? 0 : "").$polid;
                   break;
               case 'PPI':
                   $polid = getPolId('IT');
                   $polid = 'IT-'.($polid < 10 ? 0 : "" ).$polid;
                   break;
           }
           return $polid;
    }
    /**
        * checks if the policy about to be added to the system,
        * is not already under that module.
        * select record from assist_policies where polref=@polref and poltitle=@poltitle.
        * if number of records is greater than 1 return true otherwise false.
        * @return boolean
        */
       function checkPolicyDuplicates($polref){
            global $uploadfile;
            $cmpcode = $_SESSION['cc'];
            $fp = fopen($uploadfile,'r');
            while(($line = fgetcsv($fp)) !== FALSE){
                if(strtolower($line[0]) == '[policytitle]'){
                    break;
                }
            }
            $line = fgetcsv($fp);
            $poltitle = $line[0];
            fclose($fp);
           $sql = "SELECT assist_policies.polid,assist_policies.polref,assist_policies.poltitle
                    FROM assist_policies WHERE assist_policies.polref='$polref' AND assist_policies.poltitle='$poltitle'";
           include("inc_db_con.php");
           if(mysql_num_rows($rs) > 0){
               $row = mysql_fetch_array($rs);
               $sql = "DELETE FROM assist_".$cmpcode."_policies WHERE polassistid='$polref'";
               include("inc_db_con.php");               
               return $row['polid'];
           }
           return false;
       }
       /*
        *makes sure that there are no duplicates of each policy's objectives.
        *
        */
       function checkPolicyObj($polid){
           $objassistidArr = array();
           $cmpcode = $_SESSION['cc'];
           //get all the primary keys of objectives for a particluar policy, identified by $polid
           $sql = "SELECT objid FROM assist_policies_objectives WHERE assist_policies_objectives.objpolid='$polid'";
           include("inc_db_con.php");
           //push all the ids into an array.
           while($row = mysql_fetch_array($rs)){
               $objassistidArr[] = $row['objid'];
           }
           //delete
           $sql = "DELETE FROM assist_policies_objectives WHERE assist_policies_objectives.objpolid='$polid'";
           include("inc_db_con.php");
           $objassistidStr = implode(",",$objassistidArr);
           if(count($objassistidArr) > 0){
               $sql = "DELETE FROM assist_".$cmpcode."_policies_objectives WHERE objassistid IN ($objassistidStr)";
               include("inc_db_con.php");
           }
    }
    function checkPolicyLines($polid){
        $cmpcode = $_SESSION['cc'];
        $sql = "SELECT lineid FROM assist_policies_lines WHERE linepolid='$polid'";
        include("inc_db_con.php");
        while($row = mysql_fetch_array($rs)){
               $lineassistidArr[] = $row['lineid'];
        }
        $sql = "DELETE FROM assist_policies_lines WHERE linepolid='$polid'";
        include("inc_db_con.php");
        $lineassistidStr = implode(",",$lineassistidArr);
        if(count($lineassistidArr) > 0){
            $sql = "DELETE FROM assist_".$cmpcode."_policies_lines WHERE lineassistid IN ($lineassistidStr)";           
            include("inc_db_con.php");
        }
    }
    function saveData(){
        global $uploadfile,$polref,$polsubcateid;
        $objorder = 0;
        $lineorder = 0;
        $polorder = 0;
        $fp = fopen($uploadfile,'r');
       
        $ext = substr($uploadfile, strrpos($uploadfile, '.') + 1);
        $poladddate = $poladddate = time();
        $cmpcode = $_SESSION['cc'];
        $polidKey = checkPolicyDuplicates($polref);
        if($polidKey){
            checkPolicyObj($polidKey);
            checkPolicyLines($polidKey);
            $polid = $polidKey;
        }else{
            $polid = generatePoliciesPriKey();
        }

        while(!feof($fp)){
            $line = fgetcsv($fp);
            $item = $line[0];            
            if(!empty($item)){
                if(strtolower($item) == '[policytitle]') {
                    $line = fgetcsv($fp);
                    if(!empty($line[0])){
                        $poltitle = $line[0];
                        if($polidKey){
                            $sql = "UPDATE assist_policies SET  polref='$polref',
                                                                poltitle='$poltitle',
                                                                polsubcateid='$polsubcateid',
                                                                poladddate='$poladddate',
                                                                polfile='$ext'
                                                                WHERE polid='$polid'";
                            include("inc_db_con.php");
                        }else {
                            $polorder++;
                            $sql = "INSERT INTO assist_policies
                                        (polid,polref,poltitle,polsubcateid,pollines,polorder,poladddate,polmoddate,polfile)
                                 VALUES ('$polid','$polref','$poltitle',$polsubcateid,0,$polorder,$poladddate,$poladddate,'$ext')";
                          //echo $sql."<br />";
                            include("inc_db_con.php");
                            //make a corresponding entry in assist_[cmpcode]_policies
                            $sql = "INSERT INTO assist_".$cmpcode."_policies
                                 (polassistid,polyn,poleffectdate,polpubyn) VALUES
                                 ('$polid','N','$poladddate','N')";

                            include("inc_db_con.php");
                         }
                    }
                 }else if(strtolower($item) == '[objectives]'){
                            $cmpcode = $_SESSION['cc'];
                            while(($line = fgetcsv($fp)) !== FALSE  && strtolower($line[0]) != '[actions]'){
                                $item = $line[0];
                                if(!empty($item)){
                                    $objorder++;
                                    $item = addslashes($item);
                                    $sql = "INSERT INTO assist_policies_objectives
                                              (objpolid,objorder,objtext)
                                        VALUES('$polid',$objorder,'$item')";
                                    include("inc_db_con.php");
                                     $sql = "SELECT LAST_INSERT_ID() LastID";
                                    include("inc_db_con.php");
                                    $row = mysql_fetch_array($rs);
                                    $lastInsertID = $row['LastID'];
                                    $sql = "INSERT INTO assist_".$cmpcode."_policies_objectives (objassistid,objyn) VALUES ('$lastInsertID','Y')";
                                    include("inc_db_con.php");
                                }
                            }
                            if(strtolower($line[0]) == '[actions]'){
                                $actionsBeg = true;
                            }
                 }else if(strtolower($line[0]) == '[actions]' || $actionsBeg){
                            if(strtolower($line[0]) == '[actions]'){
                                $line = fgetcsv($fp);
                                $item = $line[0];
                            }
                            if(!empty($item)){
                                $lineorder++;
                                $item = addslashes($item);
                                $sql = "INSERT INTO assist_policies_lines (linepolid,lineorder,linetext) VALUES ('$polid',$lineorder,'$item')";
                                include("inc_db_con.php");
                                $sql = "SELECT LAST_INSERT_ID() LastID";
                                include("inc_db_con.php");
                                $row = mysql_fetch_array($rs);
                                $lastInsertID = $row['LastID'];
                                $sql = "INSERT INTO assist_".$cmpcode."_policies_lines (lineassistid,lineyn) VALUES ('$lastInsertID','Y')";
                                include("inc_db_con.php");
                            }
                 }
             }
         }
         fclose($fp);
    }
 saveData();
 echo "<script type='text/javascript'>window.close();</script>";
}else if(isset($_POST) && $_POST['uploadBtn'] == 'Preview Policy'){
    //print_r($_POST);
    $polid = $_POST['polref'];
    $polsubcateid = $_POST['polsubcateid'];
    function uploadFile(){
           global $uploadfile;
           $uploadfile = '../files/Dev1234/'.$_FILES['policy_file']['name'];
           
           if($_FILES['policy_file']['type'] == 'application/vnd.ms-excel'){
               if(move_uploaded_file($_FILES['policy_file']['tmp_name'], $uploadfile)){                   
                    return true;
                }
           }else {
               return false;
           }
           return false;
   }
    function getPolicyData(){
           global $uploadfile,$data;
          
           $actionsBeg = false;
           $objectives = array();
           $actions = array();
           $data = array('module'=>'','policytitle'=>'','objectives'=>array(),'actions'=>array());
           $fp = fopen($uploadfile,'r');
           //echo "uploadfile:".$uploadfile;
           $line = fgetcsv($fp);
           $data['module'] = $line[0];
           while(($line = fgetcsv($fp)) !== FALSE){
               if(!empty($line[0])){
                    if(strtolower($line[0]) == '[policytitle]'){
                        $line = fgetcsv($fp);
                        $data['policytitle'] = $line[0];
                    }else if(strtolower($line[0]) == '[objectives]'){
                        while(($line = fgetcsv($fp)) !== FALSE   && strtolower($line[0]) != '[actions]'){
                            if(!empty($line[0]))//makes sure no empty lines are picked up as objectives
                                $objectives[] = $line[0];
                        }
                        if($line[0] == '[actions]'){
                            $actionsBeg = true;
                        }
                    }else if($actionsBeg){
                        //echo print_r($line)."<br />";
                        $actions[] = $line[0];
                        while(($line = fgetcsv($fp)) !== FALSE){
                            if(!empty($line[0]))//this line makes sure no empty lines are picked up as actions.
                                $actions[] = $line[0];
                        }
                    }
               }
           }
           $data['objectives'] = $objectives;
           $data['actions'] = $actions;
          
    }

    function readAndPrint(){
           global $uploadfile,$data;
           $polref = $_POST['polref'];
           //print_r($_POST);
           $polsubcateid = $_POST['polsubcateid'];
           $pollines = 0;//??
           $polorder = 0;//??
           $actionsBeg = false;
           $poladddate = time();
          if(uploadFile()){
            getPolicyData();
          }else{
            $data['error']='An error has occurred! You can only upload CSV files';
          }
           
     }
     readAndPrint();
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<script type="text/javascript">
    function checkTarget(obj){
        var fr = document.getElementById("childwindowForm");
 
        return true;
    }
</script>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin='0' leftmargin='5' bottommargin='0' rightmargin='5'>
   
        <?php if(isset($data['error'])){?>
          <?php echo $data['error']  ;?>
        <?php }else { ?>
        
   
    <table cellpadding="3" cellspacing="0" border="1" colspan="3" width="600px">
        <tr>
            <td valign="top" colspan="3" class="tdgeneral">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td class="tdgeneral">
                            <img height="75" src="/pics/logos/<?php echo($cmplogo);?>" />
                        </td>
                        <td align="right" valign="bottom" class="tdgeneral"><b><font color="#000000"><?php echo($data['module']);?></font></b></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
			<td width="100" valign="top" class="tdgeneral"><b>Policy:</b></td>
			<td valign="top" colspan="3" class="tdgeneral"><b><?php echo($data['policytitle']);?></b></td>
		</tr>
         
        <?php if(!empty($data['objectives'])){?>
            <tr>
                <td valign="top"  class="tdgeneral" rowspan="<?php echo(count($data['objectives'])+1);?>"><b>Objective:</b></td>
            </tr>
        <?php }?>

        <?php foreach($data['objectives'] as $key => $line){?>
            <tr>
                <td valign="top"><input type="checkbox" checked="checked"  /></td>
                <td> <?php echo $line;?></td>
               
            </tr>
        <?php }?>
        <tr>
            <td valign="top" class="tdgeneral" rowspan="<?php echo(count($data['actions'])+1);?>">
                <b>Actions:</b>
            </td>
		</tr>
		<?php foreach($data['actions'] as $key => $line){?>
            <tr>
				<td valign="top"><input type="checkbox" checked="checked"  /></td>
				<td><?php  echo $line;?></td>
		<?php }?>
            </tr>
            <tr>
				<td><b>Effective date:</b></td>
				<td colspan="2"><?php echo(date("j M Y"));?></td>
			</tr>
			<tr>
				<?php $url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];?>
				<td colspan="3">
					<form method="post" action="<?php echo($url);?>" name="childwindowForm" id="childwindowForm" onsubmit="return checkTarget(this)" target="_self">
						<input type="button" onclick="window.print()" value="Print"/>
						<input type="hidden" name="polref" value="<?php echo($_POST['polref']);?>"/>
						<input type="hidden" name="polsubcateid" value="<?php echo($polsubcateid);?>" />
						<input type="hidden" name="uploadfile" value="<?php echo($uploadfile);?>" />
						<input type="submit" name="accept" value="Accept" />
						<input type="button" name="reject" value="Reject" onclick="window.close()"/>
					</form>
				</td>
			</tr>
    </table>
    <?php }?>
</body>

</html>
