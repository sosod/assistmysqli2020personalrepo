<?php
include("inc_ignite.php");
function deleteObjectives($objpolid){
    $cmpcode = $_SESSION['cc'];
    $sql = "SELECT objid FROM assist_policies_objectives WHERE objpolid='$objpolid'";
    include("inc_db_con.php");
    $objs = "(";
    $i = 0;
    $num_rows = mysql_num_rows($rs);
    while($row = mysql_fetch_array($rs)){
        if($num_rows == 1){
            $objs .= $row['objid'];
        }else{
             $objs .= $row['objid'].",";
        }
        $i++;
    }
     if($num_rows > 1)
        $objs = substr($objs,0,-1);
    $objs .= ")";
    if($num_rows > 0)
        $sql = "DELETE FROM assist_".$cmpcode."_policies_objectives WHERE objassistid IN $objs";
    include("inc_db_con.php");
    $sql = "DELETE FROM assist_policies_objectives WHERE objpolid='$objpolid'";
    include("inc_db_con.php");
}
function deleteActions($linepolid){
    $cmpcode = $_SESSION['cc'];
    $lines = "(";
    $i = 0;
    $sql = "SELECT lineid FROM assist_policies_lines WHERE linepolid='$linepolid'";
    include("inc_db_con.php");
    $num_rows = mysql_num_rows($rs);
    while($row = mysql_fetch_array($rs)){
        if($num_rows == 1){
            $lines .= $row['lineid'];
        }else{
             $lines .= $row['lineid'].",";
        }
        $i++;
    }
    if($num_rows > 1)
        $lines = substr($lines,0,-1);
    $lines .= ")";
    if($num_rows > 0)
        $sql = "DELETE FROM assist_".$cmpcode."_policies_lines WHERE lineassistid IN $lines";
    include("inc_db_con.php");
    $sql = "DELETE FROM assist_policies_lines WHERE linepolid='$linepolid'";
    include("inc_db_con.php");
}
function deleteOneObj($objid){
    $cmpcode = $_SESSION['cc'];
    $sql = "DELETE FROM assist_".$cmpcode."_policies_objectives WHERE objassistid=$objid";
    include("inc_db_con.php");
    $sql = "DELETE FROM assist_policies_objectives WHERE objid=$objid";
    include("inc_db_con.php");
}
function deleteOneLine($lineid){
    $cmpcode = $_SESSION['cc'];
    $sql = "DELETE FROM assist_".$cmpcode."_policies_lines WHERE lineassistid=$lineid";
    include("inc_db_con.php");
    $sql = "DELETE FROM assist_policies_lines WHERE lineid=$lineid";
    include("inc_db_con.php");
}
if(isset($_GET['action'])){   
    $cmpcode = $_SESSION['cc'];
    $polid = $_GET['i'];
    $action = $_GET['action'];
    switch($action){
        case 'deletePolicy':            
            $sql = "SELECT polassistid FROM assist_".$cmpcode."_policies WHERE polid=$polid";
            include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $polassistid = $row['polassistid'];
            $sql = "DELETE FROM assist_".$cmpcode."_policies WHERE polid=$polid";
            include("inc_db_con.php");
            $sql = "DELETE FROM assist_policies WHERE polid='$polassistid'";
            include("inc_db_con.php");
            //delete policy objectives
            deleteObjectives($polassistid);
            //delete policy lines
            deleteActions($polassistid);
            echo "<script type='text/javascript'>document.location.href='admin_main.php'</script>";
            break;
        case 'deleteObjective':
            deleteOneObj($_GET['objid']);
            //echo "<script type='text/javascript'>document.location.href='admin_main.php?i=$polid'</script>";
            break;
        case 'deleteLine':
            deleteOneLine($_GET['lineid']);
            //echo "<script type='text/javascript'>document.location.href='admin_main.php?i=$polid'</script>";
            break;
    }
    
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
    
function changeTickObj(obj,oid,pid) {
    if(obj.checked == true)
    {
        oyn = "Y";
    }
    else
    {
        oyn = "N";
    }
    document.location.href = "admin_view_obj.php?i="+oid+"&v="+oyn+"&p="+pid;
}
function changeTickLines(obj,oid,pid) {
    if(obj.checked == true)
    {
        oyn = "Y";
    }
    else
    {
        oyn = "N";
    }
    document.location.href = "admin_view_lines.php?i="+oid+"&v="+oyn+"&p="+pid;
}
var baseText = null;

function createXMLHttpRequestObject(){
    var xmlHttp;
    try{
        xmlHttp = new XMLHttpRequest();
    }catch(e){
        var xmlHttpVersions = new Array('MSXML2.XMLHTTP.6.0','MSXML2.XMLHTTP.5.0',
                'MSXML2.XMLHTTP.4.0','MSXML2.XMLHTTP.3.0','MSXML2.XMLHTTP','Microsoft.XMLHTTP');
        for(var i = 0; i < xmlHttpVersions.length && !xmlHttp; i++){
               try{
                    xmlHttp = new ActiveXObject(xmlHttpVersions[i]);
		}catch(e){}
	}
    }
    if(!xmlHttp){
        alert("Error creating the xmlHttpRequestObject");
    }else{
        return xmlHttp;
    }
}
var xmlHttp = createXMLHttpRequestObject();

function process(lineid,linetype){
    var cmpcode = document.getElementById('cmpcode').value;
    var url = "admin_view_edit_line.php";
    var params = "linetype="+linetype+"&cmpcode="+cmpcode+"&editPolicy=editPolicy&lineid="+lineid;
    if(xmlHttp){
        try{
	xmlHttp.open("POST",url, true);
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlHttp.setRequestHeader("Content-length", params.length);
        xmlHttp.setRequestHeader("Connection", "close");
	xmlHttp.onreadystatechange = handleRequestStateChange;
	xmlHttp.send(params);
	}catch(e){
	alert("Can't connect to server:"+ e.toString());
	}
    }
}
function handleRequestStateChange(){
if (xmlHttp.readyState == 4){
    if (xmlHttp.status == 200){
        try{
            handleServerResponse();
	}catch(e){
	alert("Error reading the response:"+e.toString());
	}
    }else {
        alert("There was a problem retrieving the data:"+toString());
	}
    }
}
function handleRequestStateChangeUpdate(){
if (xmlHttp.readyState == 4){
    if (xmlHttp.status == 200){
        try{
            handleUpdateResponse();
	}catch(e){
	alert("Error reading the response:"+e.toString());
	}
    }else {
        alert("There was a problem retrieving the data:"+toString());
	}
    }
}
function handleServerResponse(){
    var xmlResponse = xmlHttp.responseXML;
    //alert(xmlResponse);
    if (!xmlResponse || !xmlResponse.documentElement)
    throw("Invalid XML structure:\n" + xmlHttp.responseText);
    var rootNodeName = xmlResponse.documentElement.nodeName;
    if (rootNodeName == "parsererror")
    throw("Invalid XML structure");
    xmlRoot = xmlResponse.documentElement;

    var lineNodes = xmlRoot.getElementsByTagName("line");
    //there is only
    var lineNode = lineNodes.item(0);
    var linetype = lineNode.getAttribute('linetype');
    var html ='<form method="post" name="" action="" onsubmit="return false;" >'+
                '<table style="border:none;">'+
                '<tr><td style="border:none;">'+
                '<textarea cols="80" rows="5" id="lineTxt">'+lineNode.firstChild.data+'</textarea>'+
                '</td>'+
                '</tr><tr><td style="border:none;">'+
                '<input type="hidden" id="lineid" name="lineid" value="'+lineNode.getAttribute('id')+'" />'+
                '<input type="hidden" id="linetype" name="linetype" value="'+lineNode.getAttribute('linetype')+'" />'+
                '<button name="" value="" onclick="saveAction();"/>Save</button><button name="closeBtn" value="Close" onClick="hidePopup();">Close</button>'
                '</td></tr></table>'+                
                '</form>';
    var doc = document.getElementById('popupcontent');
    doc.innerHTML = html;
}
function handleUpdateResponse(){
    var xmlResponse = xmlHttp.responseXML;
    if (!xmlResponse || !xmlResponse.documentElement)
    throw("Invalid XML structure:\n" + xmlHttp.responseText);
    var rootNodeName = xmlResponse.documentElement.nodeName;
    if (rootNodeName == "parsererror")
    throw("Invalid XML structure");
    xmlRoot = xmlResponse.documentElement;
    var valueNode = xmlRoot.getElementsByTagName("value");
    var value = valueNode.item(0);
    var updateStatus = value.getAttribute("status");
    var tdline = value.getAttribute("actionline");
    var lineContent = value.firstChild.data;
    if(updateStatus == 'true'){
        var tdCont = document.getElementById(tdline);
        tdCont.innerHTML = lineContent;
    }else{
        
    }
}
function showPopup(lineid,linetype){
    process(lineid,linetype);    
   var popUp = document.getElementById("popupcontent");
   
   var w = 600;
   var h = 150;
  
   popUp.style.top = "500px";
   popUp.style.left = "500px";
   popUp.style.width = w + "px";
   popUp.style.height = h + "px";

   if (baseText == null) baseText = popUp.innerHTML;
   popUp.innerHTML = baseText +
      "<div id=\"statusbar\"><button onclick=\"saveAction();\" />Save</button><button onclick=\"hidePopup();\">Close</button></div>";

   var sbar = document.getElementById("statusbar");
   sbar.style.marginTop = (parseInt(h)-40) + "px";
   popUp.style.visibility = "visible";
}
function postData(linetype){
    var lineTxt = document.getElementById("lineTxt").value;
    var cmpcode = document.getElementById('cmpcode').value;
   
    var lineid = document.getElementById('lineid').value;
   
    var url = "admin_view_edit_line.php";
    var params = "linetype="+linetype+"&lineTxt="+lineTxt+"&cmpcode="+cmpcode+"&lineid="+lineid+"&postData=postData";
    if(xmlHttp){
        try{
	xmlHttp.open("POST",url, true);
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.setRequestHeader("Content-length", params.length);
        xmlHttp.setRequestHeader("Connection", "close");
	xmlHttp.onreadystatechange = handleRequestStateChangeUpdate;
	xmlHttp.send(params);
	}catch(e){
            alert("Can't connect to server:"+ e.toString());
	}
    }
}
function saveAction(){
    var linetype = document.getElementById("linetype").value;
   postData(linetype);
}
function hidePopup(){
   var popUp = document.getElementById("popupcontent");
   popUp.style.visibility = "hidden";
}
function popEditLine(){
    var html = '';
    
}
function deleteObjective(objid,polid){
    var response = confirm("Are you sure you wish to delete this objective?");
    if(response){
        document.location.href='admin_view.php?action=deleteObjective&objid='+objid+'&i='+polid;
    }else {
       document.location.href='admin_view.php?i='+polid;
    }
}
function deleteAction(lineid,polid){
     var response = confirm("Are you sure you wish to delete this line?");
    if(response){
        document.location.href='admin_view.php?action=deleteLine&lineid='+lineid+'&i='+polid;
    }else {
       document.location.href='admin_view.php?i='+polid;
    }
}
function deletePolicy(polid){
    if(polid > 0){
        var response = confirm("This will delete all the objectives and action line texts within this policy.Click Ok to proceed.")
        if(response){
            document.location.href='admin_view.php?action=deletePolicy&i='+polid;
        }else {
            document.localtion.href='admin_view.php?i='+polid;
        }
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<style type="text/css">

    #popupcontent{
   position: absolute;
   visibility: hidden;
   overflow: hidden;
   border:1px solid #CCC;
   background-color:#F9F9F9;
   border:1px solid #333;
   padding:5px;
  
  
}
</style>
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
    <!--<a href="#" onclick="showPopup(300,200);" >Open popup</a>-->
    <div id="popupcontent"></div>
<?php
/*if(!isset($_GET['m'])){
    $m = $_GET['m'];
    switch ($m){
        case 'PHP':
            $sql = "SELECT "
            break;
        case 'PPA':
            break;
        case 'PPI':
            break;
    }
    $pid = $_GET['i'];
}*/
$pid = $_GET['i'];
$display = $_GET['d'];
$c = $_GET['c'];
$cmpcode = $_SESSION['cc'];
if($c == "change")
{
    echo("<p>Update completed.</p>");
}

$sql = "SELECT * FROM assist_".$cmpcode."_policies_variables";
include("inc_db_con.php");
$v = 0;
while($row = mysql_fetch_array($rs))
{
    $field[$v] = $row['field'];
    $var[$field[$v]] = $row['value'];
    $v = $v + 1;
}
mysql_close();

$sql = "SELECT p.polid, p.poltitle, a.poleffectdate, s.subtitle, t.toptitle";
$sql .= " FROM assist_policies p, assist_".$cmpcode."_policies a, assist_policies_subcategories s, assist_policies_topcategories t";
$sql .= " WHERE a.polassistid = p.polid";
$sql .= " AND p.polsubcateid = s.subid";
$sql .= " AND t.topid = s.subtopid";
$sql .= " AND a.polid = ".$pid;

include("inc_db_con.php");
$p = mysql_num_rows($rs);
if($p > 0)
{
    $perr = "N";
    $row = mysql_fetch_array($rs);
    $ptitle = $row['poltitle'];
    $peffectdate = $row['poleffectdate'];
    $subtitle = $row['subtitle'];
    $toptitle = $row['toptitle'];
    $polid = $row['polid'];
}
else
{
    $perr = "Y";
}
mysql_close();
$cmplogo2 = "<img src=/pics/logos/".$cmplogo." height=75>";
if($perr == "N")
{
?>
<p><b>Warning:</b> Any text that appears in red is a policy variable that must be completed on the Setup > Variables page.</p>
<table border="1" id="table1" cellspacing="0" cellpadding="3" width=600>
	<tr>
		<td class="tdgeneral" colspan=4 valign=top>
                    <table style="border:none;" border='0' cellpadding='0' cellspacing='0' width='100%'>
                        <tr>
                            <td class=tdgeneral style="border:none;"><b><?php echo($cmplogo2);?></b></td>
                            <td style="border:none;" class="tdgeneral" align=right valign=bottom>
                                    <b><font color=000000><?php echo($toptitle); ?> Policies for</b>
                                    <br><h1 style="margin-bottom: 2px; margin-top: 0px; color: 000000"><?php echo($cmpname);?></h1>
                            </td>

                        </tr>
                    </table>
		</td>
        </tr>
	<tr>
		<td class="tdgeneral" valign=top width=100><b>Policy:</b></td>
		<td class="tdgeneral" valign=top colspan=2><b><?php echo $ptitle;?></b></td>
                <?php if(strtoupper($cmpcode)=="DEV1234"){?>
                <td><a href='javascript:editPolicy()'>Edit</a>&nbsp;&nbsp;&nbsp;<a href='javascript:deletePolicy("<?php echo($pid);?>")'>Delete</a></td>
                <?php }else{?>
                    <td>&nbsp;</td>
                <?php }?>
	</tr>
<?php
$sql = "SELECT a.objid, a.objyn, o.objtext,o.objid AS objidassist FROM assist_policies_objectives o, assist_".$cmpcode."_policies_objectives a";
$sql .= " WHERE o.objpolid = '".$polid."'";
$sql .= " AND a.objassistid = o.objid";
//$sql .= " AND a.objyn = 'Y'";
$sql .= " ORDER BY objorder ";
include("inc_db_con.php");
$o = mysql_num_rows($rs);
if($o > 0)
{
    $row = mysql_fetch_array($rs);
    $otext = $row['objtext'];
    $objyn = $row['objyn'];
    $objid = $row['objid'];
    $objidassist = $row['objidassist'];
    $v2 = 0;
    for($v2=0;$v2<=$v;$v2++)
    {
        $fld = "$".$field[$v2];
        $var2 = $var[$field[$v2]];
        $text1 = explode($fld,$otext);
        $otext = implode($var2,$text1);
    }
?>
	<tr>
		<?php echo("<td class=tdgeneral valign=top rowspan=".$o."><b>Objective:</b></td>"); ?>
		<td class="tdgeneral" valign=top><?php echo("<input type=checkbox ");
        if($objyn == "Y")
        {
            echo("checked ");
        }
        echo(" name=obj onclick=changeTickObj(this,'".$objid."',".$pid.");>");
        ?></td>
		<td class="tdgeneral" valign=top id="action_<?php echo($objidassist);?>"><?php echo($otext); ?></td>
               <?php  if(strtoupper($cmpcode)=="DEV1234"){?>
                <td><a href='javascript:showPopup("<?php echo($objidassist);?>","objective")'>Edit</a>&nbsp;&nbsp;&nbsp;<a href='javascript:deleteObjective(<?php echo($objidassist);?>,<?php echo($pid);?>)'>Delete</a></td>
                <?php }else {?>
                <td>&nbsp;</td>
                <?php }?>
                
	</tr>
    <?php
    while($row = mysql_fetch_array($rs))
    {
        $otext = $row['objtext'];
        $objyn = $row['objyn'];
        $objid = $row['objid'];
        $objidassist = $row['objidassist'];
        $v2 = 0;
        for($v2=0;$v2<=$v;$v2++)
        {
            $fld = "$".$field[$v2];
            $var2 = $var[$field[$v2]];
            $text1 = explode($fld,$otext);
            $otext = implode($var2,$text1);
        }
    	echo("<tr>");
		echo("<td class=tdgeneral valign=top><input type=checkbox ");
        if($objyn == "Y")
        {
            echo("checked ");
        }
        echo(" name=obj onclick=changeTickObj(this,'".$objid."',".$pid.");></td>");
		echo("<td class=tdgeneral id='action_$objidassist'>".$otext."</td>");
                if(strtoupper($cmpcode)=="DEV1234")
                    echo ("<td><a href='javascript:showPopup(\"$objidassist\",\"objective\")'>Edit</a>&nbsp;&nbsp;&nbsp;<a href='javascript:deleteObjective($objidassist,$pid)'>Delete</a></td>");
                else
                    echo ("<td>&nbsp;</td>");
		
    }
}
mysql_close();


$sql = "SELECT l.lineid AS lineassistid,l.linetext, a.lineyn, a.lineid FROM assist_policies_lines l, assist_".$cmpcode."_policies_lines a";
$sql .= " WHERE l.linepolid = '".$polid."'";
$sql .= " AND a.lineassistid = l.lineid";
//$sql .= " AND a.lineyn = 'Y'";
$sql .= " ORDER BY lineorder ";
//echo $sql;
include("inc_db_con.php");
$l = mysql_num_rows($rs);
if($l > 0)
{
    $row = mysql_fetch_array($rs);
    $ltext = $row['linetext'];
    $lyn = $row['lineyn'];
    $lineid = $row['lineid'];
    $lineassistid = $row['lineassistid'];
    $v2 = 0;
    for($v2=0;$v2<=$v;$v2++)
    {
        $fld = "$".$field[$v2];
        $var2 = $var[$field[$v2]];
        $text1 = explode($fld,$ltext);
        $ltext = implode($var2,$text1);
    }
?>
	<tr>
		<?php echo("<td class=tdgeneral valign=top rowspan=".$l."><b>Actions:</b></td>"); ?>
		<td class="tdgeneral" valign=top><?php echo("<input type=checkbox ");
        if($lyn == "Y")
        {
            echo("checked ");
        }
        echo(" name=lines onclick=changeTickLines(this,'".$lineid."',".$pid.");>");
        ?></td>
		<td class="tdgeneral" valign=top id="action_<?php echo ($lineassistid);?>"><?php echo($ltext); ?></td>
                <?php if(strtoupper($cmpcode)=="DEV1234"){?>
                <td><a href='javascript:showPopup("<?php echo($lineassistid);?>","line")'>Edit</a>&nbsp;&nbsp;&nbsp;<a href='javascript:deleteAction(<?php echo($lineassistid);?>,<?php echo($pid);?>)'>Delete</a></td>
                <?php }else{?>
                <td>&nbsp;</td>
                <?php }?>
	</tr>
    <?php
    while($row = mysql_fetch_array($rs))
    {
        $ltext = $row['linetext'];
        $lyn = $row['lineyn'];
        $lineid = $row['lineid'];
        $lineassistid = $row['lineassistid'];
        $v2 = 0;
        for($v2=0;$v2<=$v;$v2++)
        {
            $fld = "$".$field[$v2];
            $var2 = $var[$field[$v2]];
            //print_r($var)."<br /><br />";
            $text1 = explode($fld,$ltext);
            $ltext = implode($var2,$text1);
           // print_r($text1)."<br /><br />";
            //echo $var2.":".'<br />';
        }
       
    	echo("<tr>");
		echo("<td class=tdgeneral valign=top><input type=checkbox ");
        if($lyn == "Y")
        {
            echo("checked ");
        }
        echo(" name=lines onclick=changeTickLines(this,'".$lineid."',".$pid.");></td>");
		echo("<td class=tdgeneral id='action_".$lineassistid."'>".$ltext."</td>");
                if(strtoupper($cmpcode)=="DEV1234"){
                    echo ("<td><a href='javascript:showPopup(\"$lineassistid\",\"line\")'>Edit</a>&nbsp;&nbsp;&nbsp;<a href='javascript:deleteAction($lineassistid,$pid)'>Delete</a></td>");
                }else
                    {
                     echo ("<td>&nbsp;</td>");
                    }
		echo("</tr>");
    }
}
mysql_close();
?>
	<tr>
		<td class="tdgeneral" valign=top><b>Effective date:</b></td>
		<td class="tdgeneral" valign=top colspan=2><?php echo(date("d M Y",$peffectdate)); ?>&nbsp;</td>
	</tr>
        <input type="hidden" id="cmpcode" name="cmpcode" value="<?php echo($cmpcode);?>" />
</table>
<?php
}   //IF P > 0
else
{
    echo("<p>Error.  Please go back and try again.</p>");
}   //IF P > 0
if($display != "print" || $c != "change")
{
    include("inc_back.php");
}
?>


</body></html>
