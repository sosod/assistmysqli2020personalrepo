<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Edit"),
);
$page = array("edit");
$get_udf_link_headings = false;
require_once("inc_header.php");

echo "<p>Processing...</p>";

$taskid = $_REQUEST['i'];
$action = $_REQUEST['act'];

if($action=="DEL") {
	$log = array();
	$log['logstatusid'] = 2;
	$log['logupdate'] = "Task was deleted by ".$tkname;
	$log['logdate'] = $today;
	$log['logtkid'] = "";
	$log['logstate'] = "-100";
	$log['logactdate'] = $today;
	$log['logemail'] = "N";
	$log['logsubmittkid'] = $tkid;
	$log['logtaskid'] = $taskid;
	$log['logtasktkid'] = "";
	$log['logtype'] = "D";
	
	$s = array();
	foreach($log as $fld => $v) { $s[] = $fld." = '".$v."'"; }
	$sql = "INSERT INTO ".$dbref."_log SET ".implode(", ",$s);
	$helper->db_insert($sql);
	$sql = "UPDATE ".$dbref."_task SET taskstatusid = ".$log['logstatusid'].", taskstate = ".$log['logstate']." WHERE taskid = ".$taskid;
	$helper->db_update($sql);
} else {
	die("An error occurred while trying to delete the task. Please go back and try again.");
}


//    echo "<script type='text/javascript'>document.location.href='view.php';</script>";
	if(isset($_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'])) {
		$url = "view_list.php?get_details=1";
	} else {
		$url = "view.php";
	}
	echo "<script type='text/javascript'>document.location.href='$url';</script>";

	
	
?>