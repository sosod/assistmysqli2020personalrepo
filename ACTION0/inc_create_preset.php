<?php
$preset = array();
$sql = "SELECT * FROM ".$dbref."_list_preset WHERE src = 'S' AND srcid = ".$logstatusid." AND active = true ";
include("inc_db_con.php");
$mnr = mysql_num_rows($rs);
while($row = mysql_fetch_array($rs)) {
	$preset[] = $row;
}
mysql_close($con);
if(count($preset)>0) {
	foreach($preset as $p) {
		//add new task
		if($p['owner']=="S") { $p['owner']=$preset_task_owner; } elseif($p['owner']=="U") { $p['owner']= $tkid; }
		if(!checkIntRef($p['urgency']) || !$p['urgency']) { $p['urgency'] = $taskurgencyid; }
		if(!checkIntRef($p['topic']) || !$p['topic']) { $p['topic'] = $tasktopicid; }
		$sql = "INSERT INTO ".$dbref."_task SET 
			tasktkid = '',
			taskurgencyid = ".((!checkIntRef($p['urgency']) || strlen($p['urgency'])==0) ? 2 : $p['urgency']).",
			tasktopicid = ".$p['topic'].",
			taskaction = '".$p['action']."',
			taskdeliver = '".$p['deliver']."',
			taskstatusid = 4,
			taskstate = 0,
			taskdeadline = ".($today+($p['deadline']*86400)).",
			taskadddate = $today,
			taskadduser = '".$p['owner']."'
		";
		//include("inc_db_con.php");
		$new_preset_taskid = db_insert($sql); //mysql_insert_id();
		$new_taskid = $new_preset_taskid;
		//add log for new task
		$sql = "INSERT INTO ".$dbref."_log SET 
			logdate = $today,
			logtkid = '$tkid',
			logupdate = 'New task added.".chr(10)."Task instructions: ".$p['action']."',
			logstatusid = 4,
			logstate = 0,
			logemail = 'Y',
			logsubmittkid = '$tkid',
			logtaskid = $new_taskid,
			logtasktkid = ''
		";
		include("inc_db_con.php");
		//add recipients for new task
		$new_preset_recipients = explode("_",$p['recipients']);
		if(count($new_preset_recipients)>0) {
			$sql = "INSERT INTO ".$dbref."_task_recipients (taskid, tasktkid) VALUES ($new_preset_taskid,'".implode("'),($new_preset_taskid,'",$new_preset_recipients)."')";
			include("inc_db_con.php");
		}
		//add attachments for new task
		if($p['attachments']=="Y" && count($preset_filename)>0 ) {
			$sql = "INSERT INTO ".$dbref."_task_attachments (taskid,logid,original_filename,system_filename) VALUES ";
			$file_sql = array();
			foreach($preset_filename as $f) {
				$file_sql[] = "($new_preset_taskid,0,'".$f['o']."','".$f['s']."')";
			}
			$sql.= " ".implode(", ",$file_sql);
			include("inc_db_con.php");
		}
		//add link
		if($p['link']==true) {
			$sql = "INSERT INTO ".$dbref."_task_preset SET source_taskid = $preset_task_id, preset_taskid = $new_taskid, active = true";
			include("inc_db_con.php");
		}
		//UDFs
		if($p['udf']==true) {
			$udf = array();
			$sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = $preset_task_id AND udfref = '$modref'";
			include("inc_db_con.php");
				while($row = mysql_fetch_array($rs)) {
					$udf[] = "(".$row['udfindex'].",'".$row['udfvalue']."',".$new_taskid.",'".$row['udfref']."')";
				}
			mysql_close($con);
			if(count($udf)>0) {
				$sql = "INSERT INTO assist_".$cmpcode."_udf (udfindex, udfvalue, udfnum, udfref) VALUES ".implode(",",$udf);
				include("inc_db_con.php");
			}
		}
		
		//send emails to new task recipients	
		$subject = "";
		$message = "";
		$to = "";
		$from = "";
		$header = "";
		$sql = "SELECT DISTINCT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid IN ('".implode("','",$new_preset_recipients)."') AND tkstatus = 1";
		include("inc_db_con.php");
			$toemails = array();
			while($row = mysql_fetch_array($rs)) {
				$toemails[] = $row['tkemail'];
			}
		mysql_close($con);
		$to = implode(",",$toemails);
		$sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$p['owner']."'";
		include("inc_db_con.php");
			$row = mysql_fetch_array($rs);
			if(!isset($row['tkemail']) || strlen($row['tkemail'])==0) {
				$from = "no-reply@ignite4u.co.za";
			} else {
				$from = $row['tkemail'];
			}
		mysql_close($con);
		$subject = "New Preset $actname [Ref: #".$new_taskid."] on Ignite Assist";

		//GET TASK DETAILS
		$sqls['urgency'] = "SELECT * FROM ".$dbref."_list_urgency WHERE id = ".(checkIntRef($p['urgency']) ? $p['urgency'] : $task['taskurgencyid']);
		$sqls['topic'] = "SELECT * FROM ".$dbref."_list_topic WHERE id = ".(checkIntRef($p['topic']) ? $p['topic'] : $task['tasktopicid']);
		$sqls['oldstatus'] = "SELECT * FROM ".$dbref."_list_status WHERE pkey = $logstatusid ";
		foreach($sqls as $key => $sql) {
			//echo "<P>".$key." :: ".$p[$key]." >> ".$sql;
			include("inc_db_con.php");
				$row = mysql_fetch_array($rs);
			mysql_close($con);
			$p[$key] = (isset($row['value']) && strlen($row['value']) > 0) ? $row['value'] : "N/A";
			//echo "<br />".$p[$key]." || "; print_r($row);
		}
		
		$message = $actname." #".$preset_task_id." has been updated to '".$p['oldstatus']."' by ".$tkname." resulting in a preset ".$actname." being created and assigned to you.\n";
		$message.= "\nThe details of the preset $actname are as follows:\n";
		$message.= "Prority: ".ASSIST_HELPER::decode($p['urgency'])."\n";
		$message.= "Topic: ".ASSIST_HELPER::decode($p['topic'])."\n";
		$message.= "$actname Instructions:\n\t".str_replace(chr(10),"\n\t",ASSIST_HELPER::decode($p['action']))."\n";
		if(strlen($p['deliver']) >0 ) {
			$message .= "$actname Deliverables:\n\t".str_replace(chr(10),"\n\t",ASSIST_HELPER::decode($p['deliver']))."\n";
		}
		$message .= "Deadline: ".date("d F Y",($today+($p['deadline']*86400)))."\n";
		$message .= "Please log onto Ignite Assist in order to update this ".strtolower($actname).".\n";

		$headers = 'From: '.$userFrom.' <'.$from.'>' . "\r\n";
		$headers.= 'Reply-to: '.$userFrom.' <'.$from.'>' . "\r\n";
		//mail($to,$subject,$message,$headers);
        $mail = new ASSIST_EMAIL($to,$subject,$message);
        $mail->sendEmail();

		//add new log to source
        $sql = "INSERT INTO ".$dbref."_log SET ";
        $sql.= "logdate = '".$today."', ";
        $sql.= "logtkid = '".$tkid."', ";
        $sql.= "logupdate = 'Preset task #".$new_taskid."', ";
        $sql.= "logstatusid = '".$logstatusid."', ";
        $sql.= "logstate = ".$logstate.", ";
        $sql.= "logemail = 'Y', ";
        $sql.= "logsubmittkid = '".$tkid."', ";
        $sql.= "logtaskid = '".$taskid."', ";
        $sql.= "logtasktkid = '".$task['tasktkid']."'";
        //include("inc_db_con.php");

		
	}
}
?>