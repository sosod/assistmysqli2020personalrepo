<?php

class ACTION0 extends ASSIST_MODULE_HELPER {
	/*************
	 * CONSTANTS
	 */
	const OBJECT_NAME = "action";
	const OBJECT_TYPE = "UA";
	public function __construct() {
		parent::__construct();
	}
	public function getEmptyTableNotice() {
		return "No ".self::OBJECT_NAME."s found to display.";
	}

	static public function getObjectType() {
		return self::OBJECT_TYPE;
	}
}



?>