<?php

include("inc_ignite.php");



//GET TOPIC ID
$tid = $_GET['i'];
$helper->getPageHeader("1.10.0");
include("inc_style.php")
?>

<!--<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>-->
<script language=JavaScript>
function Validate() {
    var newval = document.upd.newval.value;
    var oldval = document.upd.oldval.value;

    if(newval == oldval)
    {
        alert("The updated topic is the same as the old topic.\n\nPlease enter an updated topic.");
        return false;
    }
    else
    {
        if(newval.length > 0)
        {
            return true;
        }
        else
        {
            alert("You've entered a blank updated topic.\n\nIf you wish to delete this topic, please click the 'Delete' button else please enter an updated topic.");
            return false;
        }
    }
}

function delTop(id) {
    if(confirm("Are you sure you want to delete this topic?"))
    {
        document.location.href = "setup_topic_edit_delete.php?i="+id;
    }
}
</script>
<!--<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php /*include("inc_style.php"); */?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>-->
<h1 class=fc><b><?php echo $modtext;?> >> Setup - Update Topics</b></h1>
<p>&nbsp;</p>
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader width=175>Old Topic</td>
        <td class=tdheader width=175>Updated Topic</td>
    </tr>
<form name=upd method=post action=setup_topic_edit_update.php onsubmit="return Validate();" language=jscript>
<?php
//GET TOPIC DETAILS AND DISPLAY
$sql = "SELECT * FROM ".$dbref."_list_topic WHERE id = ".$tid;
//include("inc_db_con.php");
$row = $helper->mysql_fetch_one($sql);
?>
    <tr>
        <td class=tdgeneral><?php echo($row['value']); ?>&nbsp;&nbsp;<input type=hidden name=oldval value="<?php echo($row['value']); ?>"><input type=hidden name=id value=<?php echo($tid); ?>></td>
        <td class=tdgeneral><input type=text name=newval size=30 value="<?php echo($row['value']); ?>"></td>
    </tr>
<?php

//mysql_close();
?>
    <tr>
        <td class=tdgeneral colspan=2><input type=submit value=Update> <input type=button value=Delete onclick="delTop(<?php echo($tid);?>)"></td>
    </tr>
</table>
</form>
</body>

</html>
