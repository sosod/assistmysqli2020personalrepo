<?php
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('url'=>"setup_udf.php",'txt'=>"User Defined Fields (UDFs)"),
	array('txt'=>"Edit List"),
);
$page = array("setup","udf");
$get_udf_link_headings = true;
require_once 'inc_header.php';



$id = $_REQUEST['id']; 
if(!$am->checkIntRef($id)) {
	echo "<script type=text/javascript>
			document.location.href = 'setup_udf.php?r[]=error&r[]=An error occurred while trying to access the UDF list.  Please try again.';
		</script>";
}
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiid = ".$id;
$uindex = $am->mysql_fetch_one($sql);
if(count($uindex)==0 || !isset($uindex['udfivalue'])) {
	echo "<script type=text/javascript>
			document.location.href = 'setup_udf.php?r[]=error&r[]=An error occurred while trying to access the UDF ".$id.".  Please try again.';
		</script>";
}
echo "<h2>".$uindex['udfivalue']."</h2>";

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

//JSdisplayResultPrep();
 ?>
<form name=addlist style="margin-bottom: 20px;">
<table>
    <tr>
        <th>Ref</th>
        <th>List Item</th>
        <th></th>
    </tr>
	<tr>
        <th><input type=hidden name=udfvid value=0 />&nbsp;</th>
        <td><input type=text size=50 maxlength=50 name=udfvvalue /></td>
        <td class=center><input type=button value=Add class=isubmit f=addlist act=ADD /></td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$id." AND udfvyn = 'Y' ORDER BY udfvvalue";
$items = $am->mysql_fetch_all($sql);
foreach($items as $row) {
	echo "<tr>
        <th>".$row['udfvid']."</th>
        <td>".$row['udfvvalue']."</td>
        <td class=center><input type=button value=Edit id=".$row['udfvid']." class=edit_item /></td>
    </tr>";
}
?>
</table>
</form>
<script type=text/javascript>
$(function() {
var udfvindex = <?php echo $id; ?>;
	<?php
	if(isset($_REQUEST['r'])) {
		$r = $_REQUEST['r'];
		//echo "JSdisplayResult('".$r[0]."','".$r[0]."','".decode($r[1])."');";
	}
	?>
$("form").keypress(function(e){ 
    if (e.which == 13) { 
       var frm = $(this).prop("name");
	   $("form[name="+frm+"] input:button.isubmit").trigger("click");
	   return false;
	   
   } 
});
	$("form[name=addlist] input:text[name=udfvvalue]").focus();
	$("input:button.isubmit").click(function() {
		var form = "form[name="+$(this).attr("f")+"]";
		var act = $(this).attr("act");
		var v = $(form+" input:text[name=udfvvalue]").val();
		$(form+" input:text[name=udfvvalue]").removeClass();
		if(v.length==0) {
			$(form+" input:text[name=udfvvalue]").addClass("required");
			JSdisplayResult("error","error","Please complete all fields.");
		} else {
			var i = $(form+" input:hidden[name=udfvid]").val();
			var p = "act="+act+"&udfvvalue="+escape(v)+"&udfvindex="+udfvindex+"&udfvid="+i;
			ajax_setupUDFListItem(form,act,p);
		}
	});
});
</script>
<?php 
$log_sql = "SELECT date as log_date, tkname as log_user, transaction as log_action FROM assist_".$cmpcode."_udf_log WHERE section='$id' ORDER BY id DESC";
$log_result = $am->mysql_fetch_all($log_sql);
$log_flds = array('date'=>"log_date",'user'=>"log_user",'action'=>"log_action");
$am->displayAuditLog($log_result,$log_flds);
?>
<div id=dlg_edit title="Edit List Item">
<form name=editlist>
	<table id=tbl_edit>
		<tr>	
			<th>Reference:</th>
			<td><label id=lbl_id for=id>###</label><input type=hidden name=udfvid value="0" id=id /></td>
		</tr>
		<tr>	
			<th>List Item:</th>
			<td><input type=text name=udfvvalue size=50 maxlength=50 value="" /></td>
		</tr>
		<tr>	
			<th></th>
			<td><input type=button value="Save Changes" class=isubmit f=editlist act=EDIT /></td>
		</tr>
	</table>
	<p class=float><input type=button value=Delete class=idelete f=editlist act=DELETE vindex=<?php echo $id; ?> /></p>
</form>
</div>
<script type=text/javascript>
$(function() {
var udfvindex = <?php echo $id; ?>;
	$("#dlg_edit").dialog({
		width: 500,
		height: 200,
		modal: true,
		autoOpen: false,
		close: function() {
			JSdisplayResult("","","");
		}
	});
	$("#tbl_edit th").addClass("left");
	$("form[name=addlist] input:button.edit_item").click(function() {
		var i = $(this).prop("id");
		var form = "form[name=editlist]";
		$("#dlg_edit #lbl_id").prop("innerText",i);
		$("#dlg_edit #id").val(i);
		ajax_setupUDFListItem(form,"pop_editform","act=getListItem&vid="+i);
	});
	$("form[name=editlist] input:button.idelete").click(function() {
		var form = "form[name=editlist]";
		var act = "DELETE";
		if(confirm("Are you sure you wish to DELETE this UDF List Item?")==true) {
			var i = $(form+" input:hidden[name=udfvid]").val();
			var p = "act="+act+"&udfvindex="+udfvindex+"&udfvid="+i;
			ajax_setupUDFListItem(form,act,p);
		}
	});
});
</script>
</body>

</html>
