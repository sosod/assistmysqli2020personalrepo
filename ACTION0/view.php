<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	
);
$page = array("update");
$get_udf_link_headings = false;
require_once 'inc_header.php';
//arrPrint($_SESSION);
$tact = getUserAccess();
$actname.="s";

//GET LIST OF STATUSES
$status = $helper->mysql_fetch_all_fld("SELECT * FROM ".$dbref."_list_status WHERE id <> 'CN' AND yn <> 'N' ORDER BY sort ","pkey");

$actions = array();
$table_width = 400;
//GET MY ACTIONS
$sql = "SELECT s.pkey, count(t.taskid) as tcount
		FROM ".$dbref."_task t
		INNER JOIN ".$dbref."_list_status s
		  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
		INNER JOIN ".$dbref."_task_recipients tr
		  ON tr.taskid = t.taskid 
		  AND tr.tasktkid = '".$tkid."'
		GROUP BY s.pkey";
$actions['MY'] = $helper->mysql_fetch_fld2_one($sql,"pkey","tcount");
//GET MY ASSIGNED ACTIONS
$sql = "SELECT s.pkey, count(t.taskid) as tcount
		FROM ".$dbref."_task t
		INNER JOIN ".$dbref."_list_status s
		  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
		WHERE t.taskadduser = '".$tkid."'
		GROUP BY s.pkey";
$actions['OWN'] = $helper->mysql_fetch_fld2_one($sql,"pkey","tcount");
//IF TACT > 20 GET ALL ACTIONS
if($tact>20) {
	$table_width+=200;
	$sql = "SELECT s.pkey, count(t.taskid) as tcount
			FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_list_status s
			  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
			GROUP BY s.pkey";
	$actions['ALL'] = $helper->mysql_fetch_fld2_one($sql,"pkey","tcount");
}

function displayList($act,$count) {
	global $status;
	$echo = "<ul>";
		foreach($status as $key => $row) {
			$echo.= "<li>".(isset($count[$key]) ? "<a href=view_list.php?act=".$act."&s=".$key.">" : "").$row['value']." (".(isset($count[$key]) ? $count[$key] : "0").")</a></li>";
		}
	$echo.= "	</ul>";
	return $echo;
}

echo "<table width=".$table_width.">
	<tr>
		<th width=200>My $actname</th>
		<th width=200>$actname I Assigned</th>"
		.($tact>20 ? "<th width=200>All $actname</th>" : "")
	."</tr>
	<tr>
		<td class=top>"
			.displayList("MY",$actions['MY'])
		."
			<p class=float><a href=search.php?s=0&act=MY>Advanced Search</a></p>
		</td>
		<td class=top>"
			.displayList("OWN",$actions['OWN'])
		."
			<p class=float><a href=search.php?s=0&act=OWN>Advanced Search</a></p>
		</td>".($tact>20 ? "
		<td class=top>"
			.displayList("ALL",$actions['ALL'])
		."
			<p class=float><a href=search.php?s=0&act=ALL>Advanced Search</a></p>
		</td>" : "")
	."</tr>
	</table>";

?>
<script type=text/javascript>
	$(function() {
		$("tr").off("mouseenter mouseleave");
		$("table").css("margin-left","20px");
	});
</script>



</body>

</html>
