<?php include("inc_ignite.php");

$helper->getPageHeader("1.10.0");
include("inc_style.php");
 ?>

<!--<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php /*include("inc_style.php"); */?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>-->
<h1 class=fc><b><?php echo $modtext;?>: Setup - Update Status - Edit</b></h1>
<p>&nbsp;</p>
<?php
//GET NEW TOPIC DETAILS
$txt = $_POST['edittxt'];
$stt = $_POST['editstate'];
$pkey = $_POST['pkey'];
$act = $_POST['act'];
if(strlen($stt)==0 || !is_numeric($stt)) { $stt=0; }
//FORMAT
$txt = $helper->code($txt);

$sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey = ".$pkey;
/*include("inc_db_con.php");
    $rowold = mysql_fetch_array($rs);
mysql_close();*/
$rowold = $helper->mysql_fetch_one($sql);


if($act=="DEL") {
	//Edit TOPIC
	$sql = "UPDATE ".$dbref."_list_status SET";
	$sql.= " yn = 'N'";
	$sql.= " WHERE pkey = ".$pkey;
	$helper->db_update($sql);
		//LOG DETAILS
		$told = "UPDATE ".$dbref."_list_status SET yn = 'Y'";
		$told.= " WHERE pkey = ".$pkey;
		$tsql = $sql;
		$trans = "Deleted status ".$pkey.".";
		$tref = "$modref";
		//include("inc_transaction_log.php");
} else {
	//Edit TOPIC
	$sql = "UPDATE ".$dbref."_list_status SET";
	$sql.= " id = ''";
	$sql.= ", value = '".$txt."'";
	$sql.= ", heading = '".$txt."'";
	$sql.= ", state = '".$stt."'";
	$sql.= ", yn = 'Y'";
	$sql.= " WHERE pkey = ".$pkey;
	$helper->db_update($sql);
		//LOG DETAILS
		$told = "UPDATE ".$dbref."_list_status SET";
		$told.= " id = ''";
		$told.= ", value = '".$rowold['value']."'";
		$told.= ", heading = '".$rowold['value']."'";
		$told.= ", state = '".$rowold['state']."'";
		$told.= ", yn = 'Y'";
		$told.= " WHERE pkey = ".$pkey;
		$tsql = $sql;
		$trans = "Edited status ".$pkey.".";
		$tref = "$modref";
		//include("inc_transaction_log.php");
}
//insert log
$sql = "INSERT INTO ".$dbref."_log SET ";
$sql .= "logdate = '".$today."', ";
$sql .= "logtkid = '".$tkid."', ";
$sql .= "logupdate = '".$trans."', ";
$sql .= "logstatusid = '', ";
$sql .= "logstate = 0, ";
$sql .= "logactdate = '".$today."', ";
$sql .= "logemail = 'N', ";
$sql .= "logsubmittkid = '".$tkid."', ";
$sql .= "logtaskid = '', ";
$sql .= "logtasktkid = '',";
$sql .= "logtype = 'E',";
$sql .= "logobjecttype = '".ACTION0::getObjectType()."'";
//include("inc_db_con.php");
$helper->db_insert($sql);
//SET FORM FOR REDIRECT
echo("<form name=add><input type=hidden name=txt value='".urlencode($trans)."'></form>");

?>
<script language=JavaScript>
//IF REDIRECT FORM EXISTS THEN UPDATE IS DONE - SEND ON TO SETUP_STATUS PAGE
var txt = document.add.txt.value;
if(txt.length>0)
    document.location.href="setup_status.php?r[]=ok&r[]="+txt;
</script>
</body>

</html>
