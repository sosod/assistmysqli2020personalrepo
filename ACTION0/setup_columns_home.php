<?php 

$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('txt'=>"List Columns"),
);
$page = array("setup","columns");
$get_udf_link_headings = true;
require_once 'inc_header.php';

if(isset($_REQUEST['r'])) { $am->displayResult($_REQUEST['r']); }

$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : die("<p>An error has occurred.  Please go back and try again.</p>");
switch($section) {
case "HOME": $title = "Home Page Action List"; break;
default: die("<p>An error has occurred.  Please go back and try again.</p>");
}

updateUDFDisplayList();

$result = array();

//SAVE CHANGES
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE" && isset($_REQUEST['display'])) {
	$am->db_update("UPDATE ".$dbref."_list_display SET home_page = 'N' WHERE field <> 'taskdeadline'");
	if(count($_REQUEST['display'])>0) {
		$am->db_update("UPDATE ".$dbref."_list_display SET home_page = 'Y' WHERE field IN ('".implode("','",$_REQUEST['display'])."')");
	}
	$tsql = $sql;
	$trans = "Updated home page columns listing";
	$tref = $_SESSION['modref'];
	//PERFORM TRANSACTION LOG UPDATE
	//include("inc_transaction_log.php");
	$result = array("ok","Changes saved.");
}






$sql_start = "SELECT a.*, u.udfivalue
				FROM `".$dbref."_list_display` a
				LEFT OUTER JOIN assist_".$cmpcode."_udfindex u
				ON a.field = u.udfiid
				AND u.udfiyn = 'Y'
				WHERE a.yn = 'Y'
				AND ( u.udfiobject = 'action' OR a.type = 'S' ) ";




$display = array();
	$sql = $sql_start." AND a.field <> 'tasktkid' ORDER BY mydisp DESC, mysort ASC, id";
	//echo $sql;
	$r = $am->mysql_fetch_all($sql);
	foreach($r as $row) {
		if($row['type']=="U") { $row['headingfull'] = $row['udfivalue']; }
		$display[$row['field']] = $row;
	}
	unset($rs);

$am->displayResult($result);
?>
<p><span class=iinform>Please note:</span><ul>
<li>Changes made on this page will affect the home page action list for ALL users.</li>
<li>The columns are ordered based on the order chosen on the List Columns >> My Actions page.</li>
</ul></p>
<form name=frm_save action=setup_columns_home.php method=post>
<input type=hidden name=act value=SAVE />
<input type=hidden name=section value=HOME />
<table id=tbl_list style='margin-left: 15px; margin-top: 15px;'>
	<tr>
		<th>Field</th>
		<th>Display?</th>
	</tr>
<?php
$d = $display['taskdeadline'];
	echo "<tr>
			<td>".$d['headingfull']."</td>
			<td class=center>";
	if($d['field']=="taskdeadline") {
		echo "Required";
		echo "<input type=checkbox name=start value=0 checked id=start />";
	}
	echo "</td>
		</tr>";
unset($display['taskdeadline']);
$s = $display['taskstatusid'];
unset($display['taskstatusid']);
foreach($display as $d) {
	echo "<tr>
			<td id=td_".$d['field'].">".$d['headingfull']."</td>
			<td class=center>";
	if($d['field']=="taskdeadline") {
		echo "Required";
	} else {
		echo "<input type=checkbox name=display[] value=".$d['field']." ".($d['home_page']=="Y" ? "checked=checked" : "")." id=chk_".$d['field']." />";
	}
	echo "</td>
		</tr>";
}
$d = $s;
	echo "<tr>
			<td>".$d['headingfull']."</td>
			<td class=center>";
		echo "Required";
	echo "</td>
		</tr>";
?>
	<tr>
		<td colspan=2 class=center><input type=submit value=Save class=isubmit /></td>
	</tr>
</table>
</form>

<h2>Demo</h2>
<table id=tbl_demo width=49%>
	<tr id=tr_demo_head>
	</tr>
	<tr id=tr_demo_action>
	</tr>
</table>

<script type=text/javascript>
$(document).ready(function() {
	$("#h_title").append(" >> <?php echo $title; ?>");
	$("input:checkbox").click(function() {
		var v= "";
		$("#tr_demo_head").empty().append("<th>Ref</th><th>Deadline</th>");
		$("#tr_demo_action").empty().append("<th>123</th><td class=center><span class=today>Due today</span></td>");
		$("input:checkbox").each(function() {
			//alert($(this).val() + " :: "+$(this).prop("checked"));
			if($(this).prop("checked") && $(this).prop("id")!="start") {
				v = $("#td_"+$(this).val()).prop("innerHTML");
				$("#tr_demo_head").append("<th>"+v+"</th>");
				$("#tr_demo_action").append("<td>"+v+" text here</td>");
			}
		});
		$("#tr_demo_head").append("<th>Status</th><th></th>");
		$("#tr_demo_action").append("<td>New task (0%)</td><td><input type=button value=View /></td>");
	});
	$("#start").trigger("click").hide();
});
</script>
	</body>
</html>
