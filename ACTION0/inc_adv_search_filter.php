<?php
//SEARCH SETTINGS
$modref = $am->getModRef();
$search_where = array("t.taskstatusid <> 2");
if(isset($_REQUEST['search']['src']) && $_REQUEST['search']['src']=="SERIAL") {
	$src_var = unserialize($_REQUEST['search']['value']);
} else {	
	$src_var = $_REQUEST;
}

//taskadddate
$fld = "taskadddate";
if(strlen($src_var[$fld]['from'])>0 && strlen($src_var[$fld]['to'])>0) {
	$from = $src_var[$fld]['from'];
	$from = strtotime($from." 00:00:00");
	$to = $src_var[$fld]['to'];
	$to = strtotime($to." 23:59:59");
	$search_where[] = "($fld >= $from AND $fld <= $to)";
} elseif( (strlen($src_var[$fld]['from']) + strlen($src_var[$fld]['to'])) > 0) {
	$v = strlen($src_var[$fld]['from'])>0 ? $src_var[$fld]['from'] : $src_var[$fld]['to'];
	$from = strtotime($v." 00:00:00");
	$to = strtotime($v." 23:59:59");
	$search_where[] = "($fld >= $from AND $fld <= $to)";
}
//taskadduser
$fld = "taskadduser";
if(isset($src_var[$fld]) && count($src_var[$fld])>0) {
	$search_where[] = "($fld IN ('".implode("','",$src_var[$fld])."'))";
}
//tasktkid
$fld = "tasktkid";
$attach = array();
if(isset($src_var[$fld]) && count($src_var[$fld])>0) {
	$tkid_tasks = $am->mysql_fetch_fld_one("SELECT DISTINCT taskid FROM ".$dbref."_task_recipients WHERE tasktkid IN ('".implode("','",$src_var[$fld])."')","taskid");
	$tkid_tasks[] = "0";
	$search_where[] = "(t.taskid IN (".implode(",",$tkid_tasks).") )";
}

//tasktopicid
$fld = "tasktopicid";
if(isset($src_var[$fld]) && count($src_var[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$src_var[$fld])."))";
}
//taskurgencyid
$fld = "taskurgencyid";
if(isset($src_var[$fld]) && count($src_var[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$src_var[$fld])."))";
}
//taskstatusid
$fld = "taskstatusid";
if(isset($src_var[$fld]) && count($src_var[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$src_var[$fld])."))";
}
//taskdeadline
$fld = "taskdeadline";
if(strlen($src_var[$fld]['from'])>0 && strlen($src_var[$fld]['to'])>0 && $src_var[$fld]['from']!=$src_var[$fld]['to']) {
	$from = $src_var[$fld]['from'];
	$from = strtotime($from);
	$to = $src_var[$fld]['to'];
	$to = strtotime($to);
	$search_where[] = "($fld >= $from AND $fld <= $to)";
} elseif( (strlen($src_var[$fld]['from']) + strlen($src_var[$fld]['to'])) > 0) {
	$v = strlen($src_var[$fld]['from'])>0 ? $src_var[$fld]['from'] : $src_var[$fld]['to'];
	$from = strtotime($v);
	$search_where[] = "($fld = $from)";
}
//taskaction
$fld = "taskaction";
if(isset($src_var[$fld]) && strlen($src_var[$fld]['search'])>0) {
	$v = $am->code($src_var[$fld]['search']);
	$v = explode(' ',$v);
	switch($src_var[$fld]['type']) {
	case "ANY":
		$search_where[] = "($fld LIKE '%".implode("%' OR $fld LIKE '%",$v)."%')";
		break;
	case "ALL":
		$search_where[] = "($fld LIKE '%".implode("%' AND $fld LIKE '%",$v)."%')";
		break;
	case "EXACT":
		$search_where[] = "($fld LIKE '%".$am->code($src_var[$fld]['search'])."%')";
		break;
	}
}
//taskdeliver
$fld = "taskdeliver";
if(isset($src_var[$fld]) && strlen($src_var[$fld]['search'])>0) {
	$v = $am->code($src_var[$fld]['search']);
	$v = explode(' ',$v);
	switch($src_var[$fld]['type']) {
	case "ANY":
		$search_where[] = "($fld LIKE '%".implode("%' OR $fld LIKE '%",$v)."%')";
		break;
	case "ALL":
		$search_where[] = "($fld LIKE '%".implode("%' AND $fld LIKE '%",$v)."%')";
		break;
	case "EXACT":
		$search_where[] = "($fld LIKE '%".$am->code($src_var[$fld]['search'])."%')";
		break;
	}
}
//udfs
//echo "<p class=b>--- START UDFS --- </p>";
$fld = "udf";
$udf = array(); 
if(isset($src_var[$fld])) {
	$var = $src_var[$fld];
	foreach($udf_index['action']['ids'] as $i) {
		if(isset($var[$i])) {
			$u = $udf_index['action']['index'][$i];
			$v = $var[$i];
			switch($u['udfilist']) {
			case "Y":
				if(count($v)>0) {
			$udf[] = 0;
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							LEFT OUTER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$modref."'
							  AND u.udfindex = $i
							WHERE ";
					if($v[0]==0) {
						$sql.= " u.udfnum is NULL OR ";
					}
					$sql.= " u.udfvalue IN (".implode(",",$v).")";
					$ids = $am->mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "T":
			case "M":
				if(strlen($v['search'])>0) {
			$udf[] = 0;
					$sql = "";
					$vs = $am->code($v['search']);
					$vs = explode(' ',$vs);
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$modref."'
							  AND u.udfindex = $i
							WHERE ";
					switch($v['type']) {
					case "ANY":
						$sql.= "(u.udfvalue LIKE '%".implode("%' OR u.udfvalue LIKE '%",$vs)."%')";
						break;
					case "ALL":
						$sql.= "(u.udfvalue LIKE '%".implode("%' AND u.udfvalue LIKE '%",$vs)."%')";
						break;
					case "EXACT":
						$sql.= "(u.udfvalue LIKE '%".$am->code($v['search'])."%')";
						break;
					}
					$ids = $am->mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "D":
				if(strlen($v['from'])>0 || strlen($v['to'])>0) {
			$udf[] = 0;
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$modref."'
							  AND u.udfindex = $i
							WHERE ";
					if($v['from']!=$v['to'] && $v['from']!='' && $v['to']!='') {
						$from = $v['from'];
						$from = strtotime($from);
						$to = $v['to'];
						$to = strtotime($to);
						$sql.= "(u.udfvalue >= $from AND u.udfvalue <= $to)";
					} else {
						$from = strtotime(strlen($v['from'])>0 ? $v['from'] : $v['to']);
						$sql.= "(u.udfvalue = $from)";
					}
					$ids = $am->mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "N":
				if(strlen($v['from'])>0 || strlen($v['to'])>0) {
			$udf[] = 0;
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$modref."'
							  AND u.udfindex = $i
							WHERE ";
					if($v['from']!=$v['to'] && $v['from']!='' && $v['to']!='') {
						$from = $v['from'];
						$to = $v['to'];
						$sql.= "(u.udfvalue >= $from AND u.udfvalue <= $to)";
					} else {
						$from = strlen($v['from'])>0 ? $v['from'] : $v['to'];
						$sql.= "(u.udfvalue = $from)";
					}
					$ids = $am->mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			}
		}
	}
	$udf = array_unique($udf,SORT_NUMERIC);
	if(count($udf)>0) {
		$search_where[] = "(t.taskid IN (".implode(",",$udf).") )";
	}
}
//echo "<P class=b>--- END UDF ---</p>";
//attach
$fld = "attach";
$attach = array();
if(isset($src_var[$fld]) && $src_var[$fld]!="X") {
	$attach[] = 0;
	$attach_tasks = $am->mysql_fetch_fld_one("SELECT taskid FROM ".$dbref."_task_attachments WHERE taskid > 0","taskid");
	$attach_logs = $am->mysql_fetch_fld_one("SELECT l.logtaskid FROM ".$dbref."_log l INNER JOIN ".$dbref."_task_attachments a ON a.logid = l.logid","logtaskid");
	$attach = $attach_tasks;
	foreach($attach_logs as $a) {
		if(!in_array($a,$attach)) { $attach[] = $a; }
	}
	if(count($attach)>0) {
		$search_where[] = "(t.taskid ".($src_var[$fld]==0 ? "NOT" : "")." IN (".implode(",",$attach).") )";
	}
}

$search_where = implode(" AND ",$search_where);
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "";
if($act=="OWN" && strlen($search_where)>0) { $search_where = " AND ".$search_where; }






/*//taskadddate
$fld = "taskadddate";
if(strlen($_REQUEST[$fld]['from'])>0 && strlen($_REQUEST[$fld]['to'])>0) {
	$from = $_REQUEST[$fld]['from'];
	$from = strtotime($from." 00:00:00");
	$to = $_REQUEST[$fld]['to'];
	$to = strtotime($to." 23:59:59");
	$search_where[] = "($fld >= $from AND $fld <= $to)";
} elseif( (strlen($_REQUEST[$fld]['from']) + strlen($_REQUEST[$fld]['to'])) > 0) {
	$v = strlen($_REQUEST[$fld]['from'])>0 ? $_REQUEST[$fld]['from'] : $_REQUEST[$fld]['to'];
	$from = strtotime($v." 00:00:00");
	$to = strtotime($v." 23:59:59");
	$search_where[] = "($fld >= $from AND $fld <= $to)";
}
//taskadduser
$fld = "taskadduser";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN ('".implode("','",$_REQUEST[$fld])."'))";
}
//tasktkid
$fld = "tasktkid";
$attach = array();
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$tkid_tasks = mysql_fetch_fld_one("SELECT DISTINCT taskid FROM ".$dbref."_task_recipients WHERE tasktkid IN ('".implode("','",$_REQUEST[$fld])."')","taskid");
	$tkid_tasks[] = "0";
	$search_where[] = "(t.taskid IN (".implode(",",$tkid_tasks).") )";
}

//tasktopicid
$fld = "tasktopicid";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$_REQUEST[$fld])."))";
}
//taskurgencyid
$fld = "taskurgencyid";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$_REQUEST[$fld])."))";
}
//taskstatusid
$fld = "taskstatusid";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$_REQUEST[$fld])."))";
}
//taskdeadline
$fld = "taskdeadline";
if(strlen($_REQUEST[$fld]['from'])>0 && strlen($_REQUEST[$fld]['to'])>0 && $_REQUEST[$fld]['from']!=$_REQUEST[$fld]['to']) {
	$from = $_REQUEST[$fld]['from'];
	$from = strtotime($from);
	$to = $_REQUEST[$fld]['to'];
	$to = strtotime($to);
	$search_where[] = "($fld >= $from AND $fld <= $to)";
} elseif( (strlen($_REQUEST[$fld]['from']) + strlen($_REQUEST[$fld]['to'])) > 0) {
	$v = strlen($_REQUEST[$fld]['from'])>0 ? $_REQUEST[$fld]['from'] : $_REQUEST[$fld]['to'];
	$from = strtotime($v);
	$search_where[] = "($fld = $from)";
}
//taskaction
$fld = "taskaction";
if(isset($_REQUEST[$fld]) && strlen($_REQUEST[$fld]['search'])>0) {
	$v = code($_REQUEST[$fld]['search']);
	$v = explode(' ',$v);
	switch($_REQUEST[$fld]['type']) {
	case "ANY":
		$search_where[] = "($fld LIKE '%".implode("%' OR $fld LIKE '%",$v)."%')";
		break;
	case "ALL":
		$search_where[] = "($fld LIKE '%".implode("%' AND $fld LIKE '%",$v)."%')";
		break;
	case "EXACT":
		$search_where[] = "($fld LIKE '%".code($_REQUEST[$fld]['search'])."%')";
		break;
	}
}
//taskdeliver
$fld = "taskdeliver";
if(isset($_REQUEST[$fld]) && strlen($_REQUEST[$fld]['search'])>0) {
	$v = code($_REQUEST[$fld]['search']);
	$v = explode(' ',$v);
	switch($_REQUEST[$fld]['type']) {
	case "ANY":
		$search_where[] = "($fld LIKE '%".implode("%' OR $fld LIKE '%",$v)."%')";
		break;
	case "ALL":
		$search_where[] = "($fld LIKE '%".implode("%' AND $fld LIKE '%",$v)."%')";
		break;
	case "EXACT":
		$search_where[] = "($fld LIKE '%".code($_REQUEST[$fld]['search'])."%')";
		break;
	}
}
//udfs
//echo "<p class=b>--- START UDFS --- </p>";
$fld = "udf";
$udf = array(); 
if(isset($_REQUEST[$fld])) {
	$var = $_REQUEST[$fld];
	foreach($udf_index['action']['ids'] as $i) {
		if(isset($var[$i])) {
			$u = $udf_index['action']['index'][$i];
			$v = $var[$i];
			switch($u['udfilist']) {
			case "Y":
				if(count($v)>0) {
			$udf[] = 0;
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							LEFT OUTER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					if($v[0]==0) {
						$sql.= " u.udfnum is NULL OR ";
					}
					$sql.= " u.udfvalue IN (".implode(",",$v).")";
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "T":
			case "M":
				if(strlen($v['search'])>0) {
			$udf[] = 0;
					$sql = "";
					$vs = code($v['search']);
					$vs = explode(' ',$vs);
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					switch($v['type']) {
					case "ANY":
						$sql.= "(u.udfvalue LIKE '%".implode("%' OR u.udfvalue LIKE '%",$vs)."%')";
						break;
					case "ALL":
						$sql.= "(u.udfvalue LIKE '%".implode("%' AND u.udfvalue LIKE '%",$vs)."%')";
						break;
					case "EXACT":
						$sql.= "(u.udfvalue LIKE '%".code($v['search'])."%')";
						break;
					}
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "D":
				if(strlen($v['from'])>0 || strlen($v['to'])>0) {
			$udf[] = 0;
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					if($v['from']!=$v['to'] && $v['from']!='' && $v['to']!='') {
						$from = $v['from'];
						$from = strtotime($from);
						$to = $v['to'];
						$to = strtotime($to);
						$sql.= "(u.udfvalue >= $from AND u.udfvalue <= $to)";
					} else {
						$from = strtotime(strlen($v['from'])>0 ? $v['from'] : $v['to']);
						$sql.= "(u.udfvalue = $from)";
					}
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "N":
				if(strlen($v['from'])>0 || strlen($v['to'])>0) {
			$udf[] = 0;
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					if($v['from']!=$v['to'] && $v['from']!='' && $v['to']!='') {
						$from = $v['from'];
						$to = $v['to'];
						$sql.= "(u.udfvalue >= $from AND u.udfvalue <= $to)";
					} else {
						$from = strlen($v['from'])>0 ? $v['from'] : $v['to'];
						$sql.= "(u.udfvalue = $from)";
					}
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			}
		}
	}
	$udf = array_unique($udf,SORT_NUMERIC);
	if(count($udf)>0) {
		$search_where[] = "(t.taskid IN (".implode(",",$udf).") )";
	}
}
//echo "<P class=b>--- END UDF ---</p>";
//attach
$fld = "attach";
$attach = array();
if(isset($_REQUEST[$fld]) && $_REQUEST[$fld]!="X") {
	$attach[] = 0;
	$attach_tasks = mysql_fetch_fld_one("SELECT taskid FROM ".$dbref."_task_attachments WHERE taskid > 0","taskid");
	$attach_logs = mysql_fetch_fld_one("SELECT l.logtaskid FROM ".$dbref."_log l INNER JOIN ".$dbref."_task_attachments a ON a.logid = l.logid","logtaskid");
	$attach = $attach_tasks;
	foreach($attach_logs as $a) {
		if(!in_array($a,$attach)) { $attach[] = $a; }
	}
	if(count($attach)>0) {
		$search_where[] = "(t.taskid ".($_REQUEST[$fld]==0 ? "NOT" : "")." IN (".implode(",",$attach).") )";
	}
}

$search_where = implode(" AND ",$search_where);
if($_REQUEST['act']=="OWN" && strlen($search_where)>0) { $search_where = " AND ".$search_where; }
*/
?>