<?php include("inc_header.php");



$sql = "DESCRIBE ".$dbref."_list_topic";
$rows = $am->mysql_fetch_all($sql);
$topiclen = 0;
foreach($rows as $row) {
		if($row=="value")
		{
			$topiclen = strFn('substr',$row[1],8,3);
			if(strpos($topiclen,')')>0) { $topiclen = strFn('substr',$topiclen,0,2); }
		}
	}
//mysql_close($con);
if(!is_numeric($topiclen)) { $topiclen = 45; } else { $topiclen = $topiclen - 10; }
 ?>
<script language=JavaScript>
function Validate() {
var addtxt = document.add.addtxt.value;
if(addtxt.length > 0)
    return true;
else
    return false;
}

function editTop(id,val) {
    while(val.indexOf("_")>0)
    {
        val = val.replace("_"," ");
    }
    while(val.indexOf("|")>0)
    {
        val = val.replace("|","'");
    }
    if(confirm("Warning: Editing a topic will affect all tasks that are associated with this topic.\n\nAre you sure you wish to continue editing '"+val+"'?") == true)
    {
        document.location.href = "setup_topic_edit.php?i="+id;
    }
}


</script>

<h1><a href=setup.php class=breadcrumb>Setup</a> >> Defaults >> Update Topics</b></h1>
<?php if(isset($_REQUEST['r'])) { $result = $_REQUEST['r']; $result[1] = stripslashes($result[1]); $am->displayResult($result); } else { echo "<P>&nbsp;</P>"; } ?>
<table>
    <tr>
        <th>ID</th>
        <th>Topic</th>
        <th>&nbsp;</th>
    </tr>
<form name=add method=post action=setup_topic_add.php onsubmit="return Validate();" language=jscript>
    <tr>
        <th>&nbsp;</th>
        <td><input type=text name=addtxt maxlength=<?php echo $topiclen; ?> size=40></td>
        <td><input type=submit value=Add /></td>
    </tr>
</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
$topics = $am->mysql_fetch_all($sql);
//include("inc_db_con.php");
//while($row = mysql_fetch_array($rs))
foreach($topics as $row) {
    $val = $row['value'];
    $valarr = explode(" ",$val);
    $val = implode("_",$valarr);
    $valarr = explode("&#39",$val);
    $val = implode("|",$valarr);
?>
    <tr>
        <th><?php echo($row['id']); ?></th>
        <td><?php echo($row['value']); ?></td>
        <td><?php echo("<input type=button value=Edit onclick=editTop(".$row["id"].",'".$val."') />"); ?></td>
    </tr>
<?php
}
//mysql_close();
?>
</table>
</body>

</html>
