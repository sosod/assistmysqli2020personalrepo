<?php
				$head = array();	$blank_action = array();
				$head['ref'] = array('text'=>"Ref", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['ref'] = 0;
				$head['taskdeadline'] = array('text'=>"Deadline", 'long'=>false, 'deadline'=>true, 'type'=>"S");
				$blank_action['taskdeadline'] = "";
				$sql = "SELECT type, field, headingfull FROM ".$dbref."_list_display WHERE yn = 'Y' AND home_page = 'Y' AND field NOT IN ('taskdeadline','taskstatusid') ORDER BY allsort";
				$head1 = $my_assist->mysql_fetch_all($sql);
				$udf_head = $my_assist->mysql_fetch_all_fld("SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".$modref."'","udfiid");
				$udfs = array();
				foreach($head1 as $h) {
					$f = $h['field'];
					$blank_action[$f] = "";
					$head[$f] = array(
						'text'=>$my_assist->decode($h['type']=="S" ? $h['headingfull'] : $udf_head[$f]['udfivalue']),
						'long'=> ( ($f=="taskaction" || $f=="taskdeliver" || ($h['type']=="U" && $udf_head[$f]['udfilist']=="M") ) ? true : false),
						'deadline'=> ( ($h['type']=="U" && $udf_head[$f]['udfilist']=="D") || $f == "taskadddate" ? true : false),
						'type'=> $h['type']=="S" ? "S" : $udf_head[$f]['udfilist'],
					);
					if($h['type']=="U") { $udfs[] = $f; }
				}
				$head['taskstatusid'] = array('text'=>"Status", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['taskstatusid'] = "";
				$head['link'] = array('text'=>"", 'long'=>false, 'deadline'=>false,'type'=>"S");
				$blank_action['link'] = "";
//arrPrint($head);
				$actions = array();
				
				$sql = "SELECT t.taskid 
						, t.taskaction
						, t.taskdeliver
						, t.taskdeadline
						, t.taskstate
						, t.taskadddate
						, top.value as tasktopicid
						, stat.value as taskstatusid
						, urg.value as taskurgencyid
						, CONCAT_WS(' ',tka.tkname, tka.tksurname) as taskadduser
						FROM ".$dbref."_task t
						INNER JOIN ".$dbref."_list_topic top
						ON t.tasktopicid = top.id
						INNER JOIN ".$dbref."_list_status stat
						ON t.taskstatusid = stat.pkey
						INNER JOIN ".$dbref."_list_urgency urg
						ON t.taskurgencyid = urg.id
						INNER JOIN ".$dbref."_task_recipients tr
						ON t.taskid = tr.taskid AND tr.tasktkid = '$tkid'
						INNER JOIN assist_".$cmpcode."_timekeep tka
						ON t.taskadduser = tka.tkid
						WHERE t.taskdeadline < ".($today + 3600*24*$next_due)."
						AND t.taskstatusid > 2 AND t.taskstatusid <> 5 ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.taskdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.taskdeadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
				$tasks = $my_assist->mysql_fetch_all_fld($sql,"taskid");
				
				if(count($udfs)>0 && count($tasks)>0) {
					$keys = array_keys($tasks);
					$sql = "SELECT * FROM  `assist_".$cmpcode."_udf` 
							WHERE udfindex IN (".implode(",",$udfs).") 
							AND udfref =  '".$modref."'
							AND udfnum IN (".implode(",",$keys).") ";
					$uv = $my_assist->mysql_fetch_all_fld2($sql,"udfnum","udfindex");
					$ul = $my_assist->mysql_fetch_all_fld2("SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex IN (".implode(",",$udfs).")","udfvindex","udfvid");
				}
				
				
				foreach($tasks as $obj_id => $t) {
					$actions[$obj_id] = $blank_action;
					foreach($head as $f => $h) {
						if($h['type']=="S") {
							$v = isset($t[$f]) ? $t[$f] :"";
							switch($f) {
							case "ref":
								$v = $obj_id;
								break;
							case "taskstatusid":
								$v = $v.($v!="On-going" ? " (".$t['taskstate']."%)" : "");
								break;
							case "taskdeadline":
									if($t['taskstatusid']!="On-going") {
										$deaddiff = $today - $v;
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0 && $deaddiff > -(3600*24*7)) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$v = "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$v = "<span>Due in $diffdays $days</span>";
										} elseif($diffdays==0) {
											$v = "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											$v = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									} else {
										$v = "";
									}
								break;
							case "link":
								$v = "update.php?i=".$obj_id;
								break;
							case "taskadddate":
								$v = date("d-M-Y H:i",$v); break;
							default:
								$v = $v;
								break;
							}
						} else {
							$v = isset($uv[$obj_id][$f]) ? $uv[$obj_id][$f]['udfvalue'] : "";
							switch($h['type']) {
							case "Y":	$v = isset($ul[$f][$v]) ? $ul[$f][$v]['udfvvalue'] : "";	break;
							case "N":	$v = format_number($v,2,".",",");	break;
							default:	$v = $v;	break;
							}
						}
						$actions[$obj_id][$f] = $v;
					}
				}
				//arrPrint($actions);

				/*$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'taskdeadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'tasktopicid'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'taskstatusid'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'taskaction'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'taskdeliver'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);
				$sql = "SELECT field, headingshort FROM ".$dbref."_list_display WHERE yn = 'Y' AND field in ('tasktopicid','taskstatusid','taskaction','taskdeliver') ORDER BY mysort";
				$rs = getRS($sql);
					while($row = mysql_fetch_array($rs)) {
						$head[$row['field']]['text'] = $row['headingshort'];
					}
				unset($rs);
unset($head['taskdeliver']);

				$actions = array();
				
					$sql = "SELECT t.*, tkname, tksurname, top.value as topic, stat.value as status ";
					$sql.= "FROM ".$dbref."_task t";
					$sql.= ", ".$dbref."_task_recipients tr";
					$sql.= ", ".$dbref."_list_topic top";
					$sql.= ", ".$dbref."_list_status stat";
					$sql.= ", assist_".$cmpcode."_timekeep ";
					$sql.= "WHERE t.taskadduser = tkid ";
					$sql.= "AND tr.tasktkid = '$tkid' AND tr.taskid = t.taskid ";
					$sql.= "AND t.tasktopicid = top.id ";
					$sql.= "AND t.taskstatusid = stat.pkey ";
					$sql.= "AND t.taskstatusid > 2 AND t.taskdeadline <= ($today + 3600*24*7) ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.taskdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.taskdeadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
					$rs = getRS($sql);
						$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
						while($task = mysql_fetch_array($rs)) {
							$obj_id = $task['taskid'];
							$actions[$obj_id] = array(
								'ref'=>$obj_id,
								'taskdeadline'=>"",
								'tasktopicid'=>"",
								'taskstatusid'=>"",
								'taskaction'=>"",
								'taskdeliver'=>"",
								'link'=>""
							);
									if($task['taskstatusid']!=5) {
										$deaddiff = $today - $task['taskdeadline'];
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$actions[$obj_id]['taskdeadline'] = "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff==0) {
											$actions[$obj_id]['taskdeadline'] = "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											$actions[$obj_id]['taskdeadline'] = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									}
								foreach($head as $fld => $h) {
									if(!in_array($fld,array("ref","link","taskdeadline"))) {
										$val = $task[$fld];
										switch($fld) {
											case "taskstatusid":
												$val = $task['status'];
												if($task['taskstate']>=0) { $val.="<br />(".$task['taskstate']."%)"; }
												break;
											case "tasktopicid":
												$val = $task['topic'];
												break;
											case "taskadduser":
												$val = $task['tkname']." ".$task['tksurname'];
												break;
											default:
												$val = decode($val);
												//if(strlen($val)>100) { $val = strFn("substr",$val,0,75)."..."; }
												$val = str_replace(chr(10),"<br />",$val);
												break;
										}
										$actions[$obj_id][$fld] = $val;
									}
								}
							//On button click, call function viewTask & pass variables ($mod, $modloc, page_address)
							$actions[$obj_id]['link'] = "update.php?i=".$task['taskid'];
						}	//while end
					unset($rs);
				

/*
				$head = array('tasktopicid'=>array(),'taskstatusid'=>array(),'taskaction'=>array(),'taskdeliver'=>array());
				$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND field in ('tasktopicid','taskstatusid','taskaction','taskdeliver') ORDER BY mysort";
				$rs = getRS($sql);
					while($row = mysql_fetch_array($rs)) {
						$head[$row['field']] = $row;
					}
				mysql_close();
				echo "<table cellpadding=3 cellspacing=0  width='100%'>";
					echo "<tr>";
						echo "<th>Ref</th>";
						echo "<th>Deadline</th>";
						foreach($head as $h) {
							echo "<th>".$h['headingshort']."</th>";
						}
						echo "<th>&nbsp;</th>";
					echo "</tr>";
					$sql = "SELECT t.*, tkname, tksurname, top.value as topic, stat.value as status ";
					$sql.= "FROM ".$dbref."_task t";
					$sql.= ", ".$dbref."_task_recipients tr";
					$sql.= ", ".$dbref."_list_topic top";
					$sql.= ", ".$dbref."_list_status stat";
					$sql.= ", assist_".$cmpcode."_timekeep ";
					$sql.= "WHERE t.taskadduser = tkid ";
					$sql.= "AND tr.tasktkid = '$tkid' AND tr.taskid = t.taskid ";
					$sql.= "AND t.tasktopicid = top.id ";
					$sql.= "AND t.taskstatusid = stat.pkey ";
					$sql.= "AND t.taskstatusid > 2 AND t.taskdeadline <= ($today + 3600*24*7) ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.taskdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.taskdeadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
					$rs = getRS($sql);
					if(mysql_num_rows($rs)==0) {
						echo "<tr>";
							echo "<td colspan=".(count($head)+3).">Nothing due for the next 7 days.</td>";
						echo "</tr>";
					} else {
						$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
						while($task = mysql_fetch_array($rs)) {
							echo "<tr>";
								echo "<th>".$task['taskid']."</th>";
								echo "<td>";
									if($task['taskstatusid']!=5) {
										$deaddiff = $today - $task['taskdeadline'];
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											echo "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff==0) {
											echo "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											echo "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									}
								echo"</td>";
								foreach($head as $h) {
									$fld = $h['field'];
									$val = $task[$fld];
									$style = "";
									switch($fld) {
										case "taskstatusid":
											$val = $task['status'];
											if($task['taskstate']>=0) { $val.="<br />(".$task['taskstate']."%)"; }
											break;
										case "tasktopicid":
											$val = $task['topic'];
											break;
										case "taskadduser":
											$val = $task['tkname']." ".$task['tksurname'];
											break;
										default:
											$val = decode($val);
											if(strlen($val)>100) { $val = strFn("substr",$val,0,75)."..."; }
											$val = str_replace(chr(10),"<br />",$val);
											break;
									}
									echo "<td style=\"".$style."\">".$val."</td>";
								}
							//On button click, call function viewTask & pass variables ($mod, $modloc, page_address)
							echo "<td class=\"center middle\"><input type=button value=View onclick=\"viewTask('$mod','$modloc','update.php?i=".$task['taskid']."');\"></td>";
							echo "</tr>";
						}	//while end
					}	//end mnr if
					mysql_close();
				echo "</table>";
*/
?>