<?php
$title = array(
	array('url'=>"new.php",'txt'=>"New"),
);
$redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";
$page = array("new");
$get_udf_link_headings = false;

//print_r($_SESSION);

require_once("inc_header.php");
//arrPrint($_SESSION);
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();

//GET USER ACCESS - IF 20 THEN NEW TASKS ONLY TO SELF ELSE CAN ASSIGN TO OTHERS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."' AND yn='Y'";
//include("inc_db_con.php");
//$row = mysql_fetch_array($rs);
$row = $am->mysql_fetch_one($sql);
$tact = isset($row['act']) ? $row['act'] : 20;
//mysql_close();
?>
        <script type ="text/javascript">
            $(function() {
                $("#datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('.date-type').datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                })
            });

        function Validate(tform){
            var form = document.forms['newtask'];
            var count = 0;
            var message = '';
            for(var i = 0; i < form.tasktkid.length;i++){
                if(form.tasktkid.options[i].selected){
                    count++;
                }
            }
            if(count == 0){
                if(form.elements['tact']){
                    var tact = parseInt(form.tact.value);
                    if(isNaN(tact)){
                        message += "Please assign this task to at least 1 user.\n";
                    }
                }else{
                    message += "Please assign this task to at least 1 user.\n";
                }

            }
            if(tform.taskurgencyid.value == "X"){
                message += "Please select the task priority.\n";
            }
            if(tform.taskstatusid.value == "X"){
                message += "Please select task status.\n";
            }
            if(tform.datepicker.value == ""){
                message += "Please select task deadline date.\n";
            }
            if(tform.tasktopicid.value == "X"){
                message += "Please select task topic.\n";
            }
            if(tform.taskaction.value == ""){
                message += "Please enter task details.\n"
            }
            if(message != '')
            {
                alert(message);
                return false;
            }
            return true;
        }
    </script>
<?php $am->displayResult($result); ?>
<p><input type=button value="Import <?php echo $actname; ?>" onclick="document.location.href = 'new_multiple.php';"></p>
        <form name=newtask method=post action=new_process.php enctype="multipart/form-data">
        	<input type=hidden name=redirect value="<?php echo $redirect; ?>" />
            <table id=tbl_action class="form">
                <tr>
                    <th>Assigned By:</th>
                    <td><?php echo($tkname); ?><input type=hidden size=5 name=taskadduser value=<?php echo($tkid); ?>></td>
                </tr>
                <tr>
                    <th id=th_tasktkid>Assign To:</th>
                    <td>
                        <?php 
/* INVEST1 TWEAK:
Added: March 2012
If the database is INVEST1 and it is not the investment admin logged in, then tasks can only be assigned to the investment admin.
*/
if(strtoupper($_SESSION['cc'])=="INVEST1" && $_SESSION['tid']!=$mod_admin) {
	$sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep WHERE tkid = $mod_admin";
	$row = $am->mysql_fetch_one($sql);
	$mod_name = $row['name'];
	echo $mod_name."<input type=hidden name=tasktkid[] value='$mod_admin' />";
} else {
                        if($tact == 20) { //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
                            echo($tkname."<input type=hidden size=5 name=tasktkid[] value=".$tkid." id=tasktkid>");
                           echo("<input type='hidden' name='tact' id='tact' value='$tact' />");
                        }
                        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
                        {
                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
									FROM assist_".$cmpcode."_timekeep t
									INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
									  ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($modref)."'
									LEFT OUTER JOIN ".$dbref."_list_access a
									  ON t.tkid = a.tkid AND a.yn = 'Y'
									WHERE t.tkid <> '0000' AND t.tkuser <> 'support' AND t.tkstatus = 1
									ORDER BY t.tkname, t.tksurname";  
                            //$rs = getRS($sql);
							$rs = $am->mysql_fetch_all($sql);
                            $size = $am->db_get_num_rows($sql);
							//if($size>1) {
                            ?>
                        <select  id="tasktkid" class=i_am_required name="tasktkid[]" multiple="multiple" size="<?php echo($size > 10 ? 10 : $size);?>">
                            <!--<option value=X>--- SELECT ---</option>-->
                                <?php
                                foreach($rs as $row) {
                                    echo("<option ".($size==1 ? "selected" : "")." name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                unset($rs);
                                ?>
                        </select><br /><i>Ctrl + left click to select multiple users</i><?php
							/*} else {
								$row = mysql_fetch_assoc($rs);
								echo $row['name']."<input type=hidden name=tasktkid[] value='".$row['tkid']."' />";
							}*/
                        }
}	//invest1 tweak
                        ?>
                    </td>
                </tr>
                <tr>
                    <th id=th_tasktopicid>Topic:</th>
                    <td>
                        <select name="tasktopicid" id=tasktopicid class=i_am_required>
                            <option value=0>--- SELECT ---</option>
<?php
                            $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
							$rs = $am->mysql_fetch_all($sql);
							foreach($rs as $row) {
                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                            }
                            unset($rs);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskurgencyid>Priority:</th>
                    <td>
                        <select name="taskurgencyid" class=i_am_required>
                            <option value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
							$rs = $am->mysql_fetch_all($sql);
							foreach($rs as $row) {
                                echo("<option ".($row['id']== 2 ? "selected='selected'" : '')." value=".$row['id'].">".$row['value']."</option>");
                            }
                            unset($rs);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskstatusid>Status:</th>
                    <td>
                        <select name="taskstatusid"  class=i_am_required>
                            <option value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM ".$dbref."_list_status WHERE (pkey = 4 OR pkey = 5) AND yn = 'Y' ORDER BY sort";
								$rs = $am->mysql_fetch_all($sql);
								foreach($rs as $row) {
                                if($row['pkey'] == "4") {
                                    echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                }
                                else {
                                    echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                }
                            }
                            unset($rs);
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskdeadline>Deadline:</th>
                    <td><input type=text size=15 class='jdate2012 i_am_required' name=taskdeadline readonly=readonly /></td>
                </tr>
                <tr>
                    <th id=th_taskaction><?php echo ucfirst($actname);?> Instructions:</th>
                    <td><textarea rows="7" name="taskaction" cols="50" class=i_am_required></textarea></td>
                </tr>
                <tr>
                    <th><?php echo ucfirst($actname);?> Deliverables:</th>
                    <td><textarea rows="7" name="taskdeliver" cols="50"></textarea></td>
                </tr>

<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' AND (udfiobject = 'action' OR udfiobject = '') ORDER BY udfisort, udfivalue";
$rs = $am->mysql_fetch_all($sql);
foreach($rs as $row) {
	$class = $row['udfiobject']." ".(strlen($row['udfilinkfield'])>0 ? $row['udfilinkfield']." ".$row['udfilinkref'] : "");
	echo "<tr class='udf $class'>
		<th>".$row['udfivalue'].":</th>
		<td>";
	switch($row['udfilist']) {
	case "Y":
		echo "<select name=".$row['udfiid']."><option selected value=0>---SELECT---</option>";
		$sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
		$rs = $am->mysql_fetch_all($sql2);
		foreach($rs as $row2) {
			echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
		}
		unset($rs2);
		echo "</select>";
		break;
	case "T":
		echo "<input type=text name=".$row['udfiid']." size=50 />";
		break;
	case "M":
		echo "<textarea name=".$row['udfiid']." rows=5 cols=40></textarea>";
		break;
	case "D":
		echo "<input class='jdate2012'  type='text' name='".$row['udfiid']."' size='15' readonly='readonly' />";
		break;
	case "N":
		 echo "<input type='text' name='".$row['udfiid']."' size='15' class=numb />&nbsp;<br /><span class=i style='font-size: 7pt;'>Only numeric values are allowed.</span>";
		break;
	default:
		echo "<input type=text name=".$row['udfiid']." size='50'>";
		break;
	}
    echo "	</td>
	</tr>";
}
unset($rs);
?>
                <tr>
                    <th>Attach Document(s):</th>
                    <td>
						<div id=docs><input type="file" name="attachments[]" id="attachment" size="30" /></div>
						<a href="javascript:void(0)" id="attachlink" style='margin-top: 5px;'>Attach another file</a>
					</td>
                </tr>
                <tr>
					<th>Send Email:</th>
					<td>
						<input type="checkbox" name="sendEmail" id="sendEmail" value="1" />
						<i>Tick to send <?php echo strtolower($actname);?> details to yourself.</i>
                    </td>
				</tr>
                <tr>
					<th></th>
                    <td>
                        <input type="button" value="Submit" class=isubmit />
                        <input type="reset" value="Reset" name="B2" />
					</td>
                </tr>
            </table>
        </form>
<script type=text/javascript>
$(function() {
	$("th").addClass("left").addClass("top");
	$("td").addClass("top");
	
	$("#tasktopicid").change(function() {
		var v = $(this).val();
		$(".tasktopicid").each(function() {
			if($(this).hasClass(v)) {	
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	$("#tasktopicid").trigger("change");
	
	$("input:text.numb").keyup(function() {
		var v = $(this).val();
		if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
			//$(this).css("background-color","#ffcccc").css("border","1px solid #cc0001");
			$(this).addClass("required");
		} else {
			$(this).removeClass();
		}
	});
	
	$('#attachlink').click(function(){
		$("#docs").append("<br /><input type=file name=attachments[] size=30 style='margin-top: 5px;' />");
	});
	
	$("form[name=newtask] input:button.isubmit").click(function() {
		var form = "form[name=newtask]";
		var err = new Array();
		var my_tag = "";
		var name = "";
		var val = "";
		var fld = "";
		$(".i_am_required").removeClass("required");
		$(".i_am_required").each(function() {
			my_tag = $(this).get(0).tagName; //attr("tagName");
			name = $(this).attr("name");
			val = $(this).val();
			if(my_tag.toUpperCase()=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(!val || val.length==0 || (my_tag=="SELECT" && (val=="X" || val=="0"))) {
				if(name=="tasktkid[]") { name="tasktkid"; }
				fld = $("#th_"+name).text();
				if(fld.charAt(fld.length-1)==":") {
					fld = fld.substr(0,fld.length-1);
				}
				err.push("- "+fld);
				$(this).addClass("required");
			}
		});
		if(err.length > 0) {
			alert("Please complete the required fields as highlighted:\n"+err.join("\n"));
		} else {
			$(form).submit();
		}
	});
	$("select.i_am_required").change(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if(!(!val || val=="X" || val=="0")) {
				$(this).removeClass("required");
			}
		}
	});
	$("textarea.i_am_required, input:text.i_am_required").keyup(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if($(this).attr("tagName")=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(val.length>0) {
				$(this).removeClass("required");
			}	
		}
	});
});
</script>
    </body>
</html>

