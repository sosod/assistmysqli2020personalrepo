<?php
    include("inc_ignite.php");



?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/lib/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: 20px"><b><?php echo $modtext;?>: Statistics</b></h1>

<h2 class=fc>Summary:
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr>
		<td class="tdheader">Person</td>
		<td class="tdheader">Num.</td>
		<td class="tdheader">% of total</td>
	</tr>
<?php
$htot = 0;
$h = 0;
//GET LIST OF USERS WITH TOTAL COUNT AND DISPLAY SUMMARY
$sql = "SELECT t.tkid, t.tkname, t.tksurname, count(c.taskid) as cc";
$sql .= " FROM ".$dbref."_task c, assist_".$cmpcode."_timekeep t";
$sql .= " WHERE c.taskstatusid <> 'CN' AND c.tasktkid = t.tkid";
$sql .= " GROUP BY t.tkid, t.tkname, t.tksurname ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $sid[$h] = $row['tkid'];
    $sval[$h] = $row['tkname']." ".$row['tksurname'];
    $scount[$h] = $row['cc'];
    $h++;
    $htot = $htot + $row['cc'];
}
mysql_close();
?>
<?php
$h2 = 0;
for($h2=0;$h2<$h;$h2++)
{
?>
	<tr>
		<td class="tdgeneral"><?php echo($sval[$h2]); ?>&nbsp;</td>
		<td class="tdgeneral" align=center><?php echo($scount[$h2]); ?></td>
		<td class="tdgeneral" align=right><?php echo(number_format(($scount[$h2]/$htot)*100,2));?>%</td>
	</tr>
<?php
}
?>
	<tr>
		<td class="tdgeneral"><b>Total</b></td>
		<td class="tdgeneral" align=center><b><?php echo($htot); ?></b></td>
		<td class="tdgeneral" align=right><b>100%</b></td>
	</tr>
</table>

<h2 class=fc><b>Details:</b>
<table border="1" id="table2" cellspacing="0" cellpadding="3">
<?php
//GET LIST OF TASK STATUS'
$p = 0;
$sql = "SELECT s.id, s.value FROM ".$dbref."_list_status s WHERE s.yn = 'Y' AND s.id <> 'CN' ORDER BY s.sort";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $st[$row['id']] = $row['value'];
    $stid[$p] = $row['id'];
    $p++;
}
mysql_close();

//GET COUNT OF OVERDUE TASKS PER USER
$sql = "SELECT c.tasktkid, count(c.taskid) as cc FROM ".$dbref."_task c";
$sql .= " WHERE c.taskstatusid <> 'CN' AND c.taskstatusid <> 'CL' AND c.taskdeadline < '".$today."'";
$sql .= " GROUP BY c.tasktkid";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $sover[$row['tasktkid']] = $row['cc'];
}
mysql_close();

$h2 = 0;
for($h2=0;$h2<$h;$h2++) //FOR EACH USER
{
$tcount = "";
    if($h2 > 0)
    {
        ?>
        <tr>
            <td colspan="3" class="tdgeneral" ><img src=../pics/blank.gif height=1></td>
        </tr>
        <?php
    }
    ?>
	<tr>
		<td colspan="3" class="tdheaderl"><?php echo($sval[$h2].": ".$scount[$h2]." (".number_format(($scount[$h2]/$htot)*100,2)."% of total)");?>&nbsp;</td>
	</tr>
    <?php
    //GET COUNT OF TASKS PER STATUS FOR EACH USER AND DISPLAY
$sql = " SELECT s.id, s.value, count(c.taskid) as cc";
$sql .= " FROM ".$dbref."_task c, ".$dbref."_list_status s";
$sql .= " WHERE s.id = c.taskstatusid AND s.id <> 'CN' AND c.tasktkid = '".$sid[$h2]."'";
$sql .= " AND c.taskid NOT IN (SELECT taskid FROM ".$dbref."_task c, ".$dbref."_list_status s";
$sql .= " WHERE s.id = c.taskstatusid AND s.id <> 'CN' AND s.id <> 'CL' AND c.tasktkid = '".$sid[$h2]."' AND c.taskdeadline < '".$today."')";
$sql .= " GROUP BY s.id, s.value ORDER BY s.sort";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $tcount[$row['id']] = $row['cc'];
    }
    mysql_close();

    foreach($stid as $t)
    {
        if($tcount[$t] > 0)
        {
        }
        else
        {
            $tcount[$t] = 0;
        }
        ?>
    	<tr>
            <td class="tdgeneral">&nbsp;&nbsp;&nbsp;<?php echo($st[$t]); ?>&nbsp;</td>
            <td class="tdgeneral" align=center><?php echo($tcount[$t]); ?>&nbsp;</td>
            <td class="tdgeneral" align=right><?php echo(number_format(($tcount[$t]/$scount[$h2])*100,2)); ?>%&nbsp;</td>
        </tr>
        <?php
    }
    //DISPLAY OVERDUE TASK COUNT
    if($sover[$sid[$h2]] > 0)
    {
    }
    else
    {
        $sover[$sid[$h2]] = 0;
    }
        ?>
    	<tr>
            <td class="tdgeneral">&nbsp;&nbsp;&nbsp;Overdue&nbsp;</td>
            <td class="tdgeneral" align=center><?php echo($sover[$sid[$h2]]); ?>&nbsp;</td>
            <td class="tdgeneral" align=right><?php echo(number_format(($sover[$sid[$h2]]/$scount[$h2])*100,2)); ?>%&nbsp;</td>
        </tr>
        <?php
}
?>
</table>

</body>

</html>
