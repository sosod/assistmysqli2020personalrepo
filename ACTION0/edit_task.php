<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Edit"),
);
$page = array("edit");
$get_udf_link_headings = false;
$scripts = array('edit_task.js');
require_once("inc_header.php");

$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
$taskid = $_REQUEST['taskid'];
if($am->checkIntRef($taskid)) {
	$sql = "SELECT t.*, CONCAT_WS(' ',tk.tkname, tk.tksurname) as adduser
			FROM ".$dbref."_task t
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			ON tk.tkid = t.taskadduser
			WHERE t.taskid = $taskid";
	$task = $am->mysql_fetch_one($sql);
	if(!is_array($task) || count($task) == 0) {
		die("<p>An error has occurred while trying to access the ".$actname." you wish to edit.  Please go back and try again.");
	} else {
		$tasktkid = $am->mysql_fetch_fld_one("SELECT * FROM ".$dbref."_task_recipients WHERE taskid = $taskid","tasktkid");
	}
} else {
	die("<p>An error has occurred while trying to determine the ".$actname." you wish to edit.  Please go back and try again.");
}

//GET USER ACCESS - IF 20 THEN NEW TASKS ONLY TO SELF ELSE CAN ASSIGN TO OTHERS
$sql = "SELECT * FROM ".$dbref."_list_access WHERE tkid = '".$tkid."'";
	$row = $am->mysql_fetch_one($sql);
$tact = $row['act'];
?>
        <script type ="text/javascript">
        function Validate(tform){
            var form = document.forms['newtask'];
            var count = 0;
            var message = '';
            for(var i = 0; i < form.tasktkid.length;i++){
                if(form.tasktkid.options[i].selected){
                    count++;
                }
            }
            if(count == 0){
                if(form.elements['tact']){
                    var tact = parseInt(form.tact.value);
                    if(isNaN(tact)){
                        message += "Please assign this task to at least 1 user.\n";
                    }
                }else{
                    message += "Please assign this task to at least 1 user.\n";
                }

            }
            if(tform.taskurgencyid.value == "X"){
                message += "Please select the task priority.\n";
            }
            if(tform.taskstatusid.value == "X"){
                message += "Please select task status.\n";
            }
            if(tform.datepicker.value == ""){
                message += "Please select task deadline date.\n";
            }
            if(tform.tasktopicid.value == "X"){
                message += "Please select task topic.\n";
            }
            if(tform.taskaction.value == ""){
                message += "Please enter task details.\n"
            }
            if(message != '')
            {
                alert(message);
                return false;
            }
            return true;
        }
    </script>
<?php $am->displayResult($result); ?>
        <form name=edittask method=post action=edit_process.php enctype="multipart/form-data">
            <table id=tbl_action class="form">
                <tr>
                    <th>Reference:</th>
                    <td><?php echo($taskid); ?><input type=hidden name=taskid value="<?php echo($taskid); ?>" id=taskid /></td>
                </tr>
                <tr>
                    <th>Created By:</th>
                    <td><?php echo($task['adduser']); ?><input type=hidden name=taskadduser value=<?php echo($task['taskadduser']); ?>></td>
                </tr>
                <tr>
                    <th>Created On:</th>
                    <td><?php echo(date("d-M-Y H:i:s",$task['taskadddate'])); ?></td>
                </tr>
                <tr>
                    <th id=th_tasktkid>Assigned To:</th>
                    <td>
                        <?php
/* INVEST1 TWEAK:
Added: March 2012
If the database is INVEST1 and it is not the investment admin logged in, then tasks can only be assigned to the investment admin.
*/
if(strtoupper($_SESSION['cc'])=="INVEST1" && $_SESSION['tid']!=$mod_admin) {
	$sql = "SELECT CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep WHERE tkid = $mod_admin";
	$row = $am->mysql_fetch_one($sql);
	$mod_name = $row['name'];
	echo $mod_name."<input type=hidden name=tasktkid[] value='$mod_admin' />";
} else {
                        if($tact == 20) { //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
                            echo($tkname."<input type=hidden size=5 name=tasktkid[] value=".$tkid." id=tasktkid>");
                           echo("<input type='hidden' name='tact' id='tact' value='$tact' />");
                        }
                        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
                        {
                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
									FROM assist_".$cmpcode."_timekeep t
									INNER JOIN assist_".$cmpcode."_menu_modules_users mmu
									  ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($modref)."'
									LEFT OUTER JOIN ".$dbref."_list_access a
									  ON t.tkid = a.tkid AND a.yn = 'Y'
									WHERE t.tkid <> '0000' AND t.tkuser <> 'support' AND t.tkstatus = 1
									ORDER BY t.tkname, t.tksurname";
                            //$rs = getRS($sql);
							$rs = $am->mysql_fetch_all($sql);
                            $size = $am->db_get_num_rows($sql);
                            ?>
                        <select  id="tasktkid" name="tasktkid[]" multiple="multiple" size="<?php echo($size > 10 ? 10 : $size);?>" class=i_am_required>
                                <?php
                                foreach($rs as $row) {
                                    echo("<option ".(in_array($row['tkid'],$tasktkid) ? "selected" : "")." name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                unset($rs);
                                ?>
                        </select>
						<br /><span class=note>Ctrl + left click to select multiple users</span>
    <?php
                        }
}
                        ?>
                    </td>
                </tr>
                <tr>
                    <th id=th_tasktopicid>Topic:</th>
                    <td>
                        <select name="tasktopicid" id=tasktopicid class=i_am_required>
                            <option selected value=0>--- SELECT ---</option><?php
                            $sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn = 'Y' ORDER BY value";
                            $topics = $am->mysql_fetch_all($sql);
							foreach($topics as $row) {
                                echo "<option value=".$row['id'].">".$row['value']."</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskurgencyid>Priority:</th>
                    <td>
                        <select name="taskurgencyid" id=taskurgencyid class=i_am_required>
                            <option selected value=0>--- SELECT ---</option><?php
							$sql = "SELECT * FROM ".$dbref."_list_urgency ORDER BY sort";
                            $urgency = $am->mysql_fetch_all($sql);
							foreach($urgency as $row) {
                                echo "<option value=".$row['id'].">".$row['value']."</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskstatusid>Status:</th>
                    <td>
                        <select name="taskstatusid" id=taskstatusid class=i_am_required>
                            <option selected value=X>--- SELECT ---</option><?php
							$sql = "SELECT * FROM ".$dbref."_list_status WHERE id != 'CN' AND yn = 'Y' ORDER BY sort";
							$status = $am->mysql_fetch_all($sql);
                            foreach($status as $row) {
                                    echo "<option value=".$row['pkey'].">".$row['value']."</option>";
                            }
						?></select>
                    </td>
                </tr>
                <tr>
                    <th id=th_taskdeadline>Deadline:</th>
                    <td><input type=text size=15 name="datepicker" class='jdate2012 i_am_required' readonly="readonly" value='<?php echo date("d-M-Y",$task['taskdeadline']); ?>' /></td>
                </tr>
                <tr>
                    <th id=th_taskaction><?php echo ucfirst($actname);?> Instructions:</th>
                    <td><textarea rows="5" name="taskaction" cols="35" class=i_am_required><?php echo $am->decode($task['taskaction']); ?></textarea></td>
                </tr>
                <tr>
                    <th><?php echo ucfirst($actname);?> Deliverables:</th>
                    <td><textarea rows="5" name="taskdeliver" cols="35"><?php echo $am->decode($task['taskdeliver']); ?></textarea></td>
                </tr>

<?php
//$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' AND (udfiobject = 'action' OR udfiobject = '') ORDER BY udfisort, udfivalue";
//$rs = getRS($sql);
$sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfref = '".$_SESSION['modref']."' AND udfnum = $taskid";
$my_udfs = $am->mysql_fetch_all_fld($sql,"udfindex");

foreach($udf_index['action']['index'] as $row) {
//while($row = mysql_fetch_array($rs)) {
	$class = $row['udfiobject']." ".(strlen($row['udfilinkfield'])>0 ? $row['udfilinkfield']." ".$row['udfilinkref'] : "");
	$u = isset($my_udfs[$row['udfiid']]) ? $my_udfs[$row['udfiid']] : array();
	$udfvalue = isset($u['udfvalue']) ? $u['udfvalue'] : "";
	echo "<tr class='udf $class'>
		<th>".$row['udfivalue'].":</th>
		<td>";
	switch($row['udfilist']) {
	case "Y":
		echo "<select name=".$row['udfiid']."><option ".(!isset($udfvalue) || !$am->checkIntRef($udfvalue) ? "selected" : "")." value=0>---SELECT---</option>";
		$sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
		$rs = $am->mysql_fetch_all($sql);
		foreach($rs as $row2) {ASSIST_HELPER::arrPrint($row2);
			$udfvvalue = isset($row2['udfvalue']) ? $row2['udfvalue'] : "";
			$udfvid = isset($row2['udfid']) ? $row2['udfid'] : "";
			echo "<option ".(isset($udfvalue) && $am->checkIntRef($udfvalue) && $udfvalue == $row2['udfvid'] ? "selected" : "")." value=".$udfvid.">".$udfvvalue."</option>";
		}
		unset($rs);
		echo "</select>";
		break;
	case "T":
		echo "<input type=text name=".isset($row['udfiid']) ? $row['udfiid'] :''." size=50 value='".$am->decode($udfvalue)."' />";
		break;
	case "M":
		echo "<textarea name=".$row['udfiid']." rows=5 cols=40>".$am->decode($udfvalue)."</textarea>";
		break;
	case "D":
		$val = "";
		if(isset($udfvalue) && strlen($udfvalue)>0) {
			if(is_numeric($udfvalue)) {
				$val = date("d-M-Y",$udfvalue);
			} else {	
				$val = date("d-M-Y",strtotime($udfvalue));
			}
		}
		echo "<input class='jdate2012'  type='text' name='".$row['udfiid']."' size='15' readonly='readonly' value='".$val."' />";
		break;
	case "N":
		 echo "<input type='text' name='".$row['udfiid']."' size='15' class=numb value='".$am->decode($udfvalue)."' />&nbsp;<br /><span class=i style='font-size: 7pt;'>Only numeric values are allowed.</span>";
		break;
	default:
		echo "<input type=text name=".$row['udfiid']." size='50' value='".$am->decode($udfvalue)."' />";
		break;
	}
    echo "	</td>
	</tr>";
}
unset($rs);
?>
                <tr>
                    <th>Attach Document(s):</th>
                    <td><table id=tbl_attach width=100%><?php
							$sql = "SELECT * FROM  ".$dbref."_task_attachments AS att WHERE att.file_location <> 'deleted' AND att.taskid = ".$taskid;
							$attach = $am->mysql_fetch_all($sql);
							if(count($attach)>0) {
								foreach($attach as $row) {
									$taskid = $row['taskid'];
									$taskattach_id = $row['id'];
									$file = "../files/$cmpcode/".$_SESSION['modref']."/".(strlen($row['file_location'])>0 ? $row['file_location']."/" : "").$row['system_filename'];
									if(file_exists($file)) {
										echo "<tr class=hover id=tr_".$row['id']."><td>+ <a href='download2.php?i=".$row['id']."' id=a_".$row['id'].">".$row['original_filename']."</a> <input type=button name='".$row['original_filename']."' value=Delete id=".$taskid." attachment_id='".$taskattach_id."' class=float /></td></tr>";
									}
								}
							}
						?>
						<tr><td><input type="file" name="attachments[]" id="attachment" size="30" /></td></tr>
						</table>
						<a href="javascript:void(0)" id="attachlink" style='margin-top: 5px;'>Attach another file</a>
					</td>
                </tr>
<!--                <tr>
					<th>Send Email:</th>
					<td>
						<input type="checkbox" name="sendEmail" id="sendEmail" value="1" />
						<i>Tick to send <?php echo strtolower($actname);?> details to yourself.</i>
                    </td>
				</tr> -->
                <tr>
					<th></th>
                    <td>
                        <input type="button" value="Submit" class=isubmit />
                        <input type="reset" value="Reset" name="B2" />
						<span class=float><input type=button value="Delete Task" class=idelete /></span>
					</td>
                </tr>
            </table>
        </form>
<script type=text/javascript>
    $(document).ready(function() {
	//FORM PREPARATION
	$("th").addClass("left").addClass("top");
	$("td").addClass("top");
	$("tr").off("mouseenter mouseleave");
	$(".note").css("color","#fe9900").css("font-style","italic").css("font-size","6.5pt");
	$("#tasktopicid").val(<?php echo $task['tasktopicid']; ?>);
	$("#taskurgencyid").val(<?php echo $task['taskurgencyid']; ?>);
	$("#taskstatusid").val(<?php echo $task['taskstatusid']; ?>);
	$("#tbl_attach tr.hover").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$("#tbl_attach, #tbl_attach td").addClass("noborder");
	//$("#tbl_attach").css("margin-bottom","10px");
	
	$("#tasktopicid").change(function() {
		var v = $(this).val();
		$(".tasktopicid").each(function() {
			if($(this).hasClass(v)) {	
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
	$("#tasktopicid").trigger("change");
	
	$("input:text.numb").keyup(function() {
		var v = $(this).val();
		if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
			//$(this).css("background-color","#ffcccc").css("border","1px solid #cc0001");
			$(this).addClass("required");
		} else {
			$(this).removeClass();
		}
	});
	
	$("#tbl_attach input:button").click(function() {
		//var i = $(this).attr("id");
        var taskid = $(this).attr("id");
        var attachment_id = $(this).attr("attachment_id");
        var file_name = $(this).attr("name");
		//var f = $("#tbl_attach #a_"+i).attr("innerText");
        var answer = confirm(`Are you sure you wish to completely delete file '${file_name}'? Click Ok to proceed otherwise cancel.`);
        if(answer){
            document.location.href = "delete_attachment.php?taskid="+taskid+"&attachment_id="+attachment_id;
        }
        return false;
		/*if(confirm("Are you sure you wish to delete attachment '"+name+"'?")===true) {
			ajax_editAction("DEL_FILE","d="+i);
		}*/
	});
	
	$('#attachlink').click(function(){
		$("#tbl_attach").append("<tr><td class=noborder><input type=file name=attachments[] size=30 style='margin-top: 0px;' /></td></tr>");
	});
	
	
	$("form[name=edittask] input:button.isubmit").click(function() {
		var form = "form[name=edittask]";
		var err = new Array();
		var tag = "";
		var name = "";
		var val = "";
		var fld = "";
		$(".i_am_required").removeClass("required");
		$(".i_am_required").each(function() {
			tag = $(this).get(0).tagName; //.attr("tagName");
			name = $(this).attr("name");
			val = $(this).val();
			if(tag.toUpperCase()=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(!val || val.length==0 || (tag=="SELECT" && (val=="X" || val=="0"))) {
				if(name=="tasktkid[]") { name="tasktkid"; }
				fld = $("#th_"+name).text();
				if(fld.charAt(fld.length-1)==":") {
					fld = fld.substr(0,fld.length-1);
				}
				err.push("- "+fld);
				$(this).addClass("required");
			}
		});
		if(err.length > 0) {
			alert("Please complete the required fields as highlighted:\n"+err.join("\n"));
		} else {
			$(form).submit();
		}
	});
	$("select.i_am_required").change(function() {
		if($(this).hasClass("required")) {
			var val = $(this).val();
			if(!(!val || val=="X" || val=="0")) {
				$(this).removeClass("required");
			}
		}
	});
	$("textarea.i_am_required, input:text.i_am_required").keyup(function() {
		if($(this).hasClass("required")) {
			val = $(this).val();
			if(tag.toUpperCase()=="TEXTAREA") {
				val = val+$(this).text();
			}
			if(val.length>0) {
				$(this).removeClass("required");
			}	
		}
	});	
	
	$("input:button.idelete").click(function() {
		var t = $("#taskid").val();
		if(!isNaN(parseInt(t))) {
			if(confirm("Are you sure you wish to DELETE this task?")==true) {
				document.location.href = 'edit_task_delete.php?act=DEL&i='+t;
			}
		} else {
			alert("An error occurred and this task cannot be deleted.\n\nPlease reload the page and try again.");
		}
	});

});

</script>
    </body>
</html>

