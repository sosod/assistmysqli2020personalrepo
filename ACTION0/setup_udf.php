<?php 
//include("inc_ignite.php");
//error_reporting(-1);
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup.php",'txt'=>"Defaults"),
	array('txt'=>"User Defined Fields (UDFs)"),
);
$page = array("setup","udf");
$scripts = array("setup_udf.js");
$get_udf_link_headings = true;
require_once 'inc_header.php';
//error_reporting(-1);

$topics = array();
$sql = "SELECT * FROM ".$dbref."_list_topic WHERE yn ='Y' ORDER BY value";
$topics = $am->mysql_fetch_all_fld($sql,"id");

$status = array();
$sql = "SELECT pkey as id, value, yn FROM ".$dbref."_list_status WHERE yn ='Y' AND id NOT IN ('CN','NW') ORDER BY sort, value";
$status = $am->mysql_fetch_all_fld($sql,"id");

//  $ah->JSdisplayResult();
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<form name="addudf" action="setup_udf_new_process.php" method=post enctype="multipart/form-data">
<input type=hidden name=action value=ADD />
<input type=hidden name=udfiref value='<?php echo strtoupper($modref); ?>' />
<table>
    <tr>
        <th>Ref</th>
        <th>Field Name</th>
        <th>Field Format</th>
        <th>Linked to...</th>
<!--		<th>Required</th>
        <th>Default Value</th> -->
        <th></th>
    </tr>
	<!-- ADD NEW UDF FORM -->
	<tr>
        <th><input type=hidden name=udfiid value=0 /></th>
        <td><input type=text name='udfivalue' /></td>
        <td><select name='udfilist' f='addudf'><?php
			foreach($udf_options['udfilist'] as $ui => $u) {
				echo "<option ".($ui=="Y" ? "selected" : "")." value=".$ui.">".$u['txt']."</option>";
			}
		?></select></td>
        <td>
			<select name='object_link' f='addudf'><?php
				foreach($udf_options['udfiobject'] as $uai => $ua) {
					echo "<option l=0 value=".$uai." class='$uai'>".$ua."</option>";
					foreach($udf_options['udfilinkfield'][$uai] as $ub) {
						echo "<option l=$ub value=".$uai."_".$ub." class='$uai'>".$ua." (Specific ".$udf_options['udfilinkfield']['headings'][$ub]['display'].")</option>";
					}
				}
			?></select>
			<span id=spn_udfilinkref><br /><select name='udfilinkref'></select></span>
			<!-- <span id=spn_remember><br /><input type=checkbox> Remember recent value?</span> -->
		</td>
<!--		<td><select name=udfirequired><option value=0 selected>No</option><option value=1>Yes</option></select></td>
        <td><input type=text name=udfidefault size=20 /> <input type=button value=Clear name=btn_clear_date f=addudf /></td> -->
        <td class=center><input type=hidden name=udfirequired value=0 /><input type=hidden name=udfidefault value='' /><input type=button value=Add class='isubmit' name='btn_add' /></td>
	</tr>
	<!-- END ADD FORM -->
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref' AND udfiyn = 'Y' ORDER BY udfisort";
$udfs = $am->mysql_fetch_all($sql);
if(count($udfs)==0) {
        ?>
        <tr>
            <td colspan=7>No UDFs to display</td>
        </tr>
        <?php
} else {
	foreach($udfs as $u) {
		$udf_object = strlen($u['udfiobject'])>0 ? $u['udfiobject'] : "action";
		$udf_link_field = $u['udfilinkfield'];
		$udf_link_ref = $u['udfilinkref'];
		$link_to = $udf_options['udfiobject'][$udf_object];
		if(strlen($udf_link_field)>0 && in_array($udf_link_field,$udf_options['udfilinkfield'][$udf_object])) {
			if(isset($udf_options['udfilinkfield']['headings'][$udf_link_field])) {
				$link_to.= " - ".$udf_options['udfilinkfield']['headings'][$udf_link_field]['display'];
			} else {
				$link_to.= " - ".$udf_link_field;
			}
			if($am->checkIntRef($udf_link_ref) && isset($udf_options['udfilinkfield']['tables'][$udf_link_field])) {
				switch($udf_link_field) {
				case "tasktopicid":
					$link_to.= "<br />(".$topics[$udf_link_ref]['value'].")";
					break;
				case "taskstatusid":
					$link_to.= "<br />(".$status[$udf_link_ref]['value'].")";
					break;
				}
			}
		}
		/*if($udf_object=="update") {
			$link_to.="<br />[Remember value]";
		}*/
		echo "
			<tr>
				<th class=top>".$u['udfiid']."</th>
				<td class=b>".$u['udfivalue']."</td>
				<td class=center>".$udf_options['udfilist'][$u['udfilist']]['txt']."".($u['udfilist']=="Y" ? "<br /><input type=button value='Edit List' id=".$u['udfiid']." class='edit_list' />" : "")."</td>
				<td class=center>".$link_to."</td>
			<!--	<td class=center>".$udf_options['udfirequired'][$u['udfirequired']]."</td>
				<td class=".$udf_options['udfilist'][$u['udfilist']]['align'].">".$u['udfidefault']."</td> -->
				<td class=center><input type=button value='Edit UDF' id=".$u['udfiid']." l='".$udf_link_field."' class='edit' /></td>
			</tr>
		";
	}
}
?>
</table>
</form>
<?php
	if(isset($_REQUEST['r'])) {
		$r = $_REQUEST['r'];
		$ah->JSdisplayResult($r[0],$r[0],$ah->decode($r[1]));
		//echo "JSdisplayResult('".$r[0]."','".$r[0]."','".decode($r[1])."');";
	}
	?>
<script type=text/javascript>
		var links = new Array();
		links['tasktopicid'] = new Array();
		<?php
		foreach($topics as $i => $l) {
			echo "
			links['tasktopicid'][$i] = '".$l['value']."';";
		}
		?>
		links['taskstatusid'] = new Array();
		<?php
		foreach($status as $i => $l) {
			echo "
			links['taskstatusid'][$i] = '".$l['value']."';";
		}
		?>
	function setUDFilinkref(form,fld) {
		for(i in links[fld]) {
			$(form+" select[name=udfilinkref]").append("<option value="+i+">"+links[fld][i]+"</option>");
		}
	}
$(function() {
	$("select[name=udfilistX]").change(function() {
		var form = "form[name="+$(this).prop("f")+"]";
		$(form+" input:text[name=udfidefault]").show();
		$(form+" input:text[name=udfidefault]").prop("size","20");
		$(form+" input:text[name=udfidefault]").datepicker("destroy");
		$(form+" input:button[name=btn_clear_date]").hide();
		switch($(this).val()) {
		case "D":
			$(form+" input:button[name=btn_clear_date]").show();
			$(form+" input:text[name=udfidefault]").prop("size","10");
			$(form+" input:text[name=udfidefault]").datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: 'both',
				buttonImage: '../library/jquery/css/calendar.gif',
				buttonImageOnly: true,
				dateFormat: 'yy-mm-dd'
			});
			break;
		case "Y":
			$(form+" input:text[name=udfidefault]").hide();
		}
	});
	$("select[name=object_link]").change(function() {
		var form = "form[name="+$(this).attr("f")+"]"; 
		var me = $(form+" select[name=object_link] option:selected").attr("l"); 
		/*if($(form+" select[name=object_link] option:selected").hasClass("update")) {
			$(form+" #spn_remember").show();
		} else {
			$(form+" #spn_remember").hide();
		}*/
		//$(form+" select[name=udfilinkref]").show();
		$(form+" #spn_udfilinkref").show();
		$(form+" select[name=udfilinkref] option").remove();
		if(isNaN(parseInt(me)) || parseInt(me)!=0) {
			//ajax_setupUDF(form,"pop_udfilinkref","act=getList&t="+me);
			setUDFilinkref(form,me);
		} else {
			$(form+" #spn_udfilinkref").hide();
		}
	});
	$("input:button[name=btn_clear_date]").click(function() {
		var form = $(this).attr("f");
		form = "form[name="+form+"]";
		$(form+" input:text[name=udfidefault]").val("");
	});

	//ADD FORM FUNCTIONS
	//$("form[name=addudf] input:text[name=udfidefault]").hide();
	$("form[name=addudf] select[name=udfilist]").trigger("change");
	$("form[name=addudf] select[name=object_link]").trigger("change");
	$("form[name=addudf] input:button[name=btn_add]").click(function() {
		JSdisplayResult("info","info","Processing... Please wait.");
		var res = Setup_udf.validateForm("form[name=addudf]","ADD");
		if(res.length>0) {
            JSdisplayResult(res[0],res[0],res[1]);
            return false;
		}
        //$("form[name=addudf]").submit();

	});
	
	$("input:button.edit_list").click(function() {
		var i = $(this).prop("id");
		document.location.href = "setup_udf_list.php?id="+i;
	});
	
	//$("form[name=addudf] select[name=object_link] option").each(function() {
		//if($(this).hasClass("action")) {
			//$(this).prop("disabled",!$(this).hasClass("action"));
		//}
	//});
});
</script>


<?php
$log_sql = "SELECT date as log_date, tkname as log_user, transaction as log_action FROM assist_".$cmpcode."_udf_log WHERE section='MAIN' ORDER BY id DESC";
$log = $am->mysql_fetch_all($log_sql);
$log_flds = array('date'=>"log_date",'user'=>"log_user",'action'=>"log_action");
$am->displayAuditLog($log,$log_flds);
?>

<div id=dlg_edit title="Edit UDF">
	<h1>Edit UDF</h1>
	<form name='editudf'>
	<table id=tbl_edit width="95%">
		<tr>
			<th>Ref:</th>
			<td><label id=lbl_udfiid for=udfiid>###</label><input type=hidden name=udfiid id=udfiid value='0' /></td>
		</tr>
		<tr>
			<th>Field Name:</th>
			<td><input type=text name=udfivalue /></td>
		</tr>
		<tr>
			<th>Field Format:</th>
			<td><select name=udfilist f=editudf><?php
			foreach($udf_options['udfilist'] as $ui => $u) {
				echo "<option ".($ui=="Y" ? "selected" : "")." value=".$ui.">".$u['txt']."</option>";
			}
			?></select></td>
		</tr>
		<tr>
			<th>Link:</th>
			<td>
				<select name='object_link' f='editudf'><?php
					foreach($udf_options['udfiobject'] as $uai => $ua) {
						echo "<option l=0 value=".$uai." class=$uai>".$ua."</option>";
						foreach($udf_options['udfilinkfield'][$uai] as $ub) {
							echo "<option l=$ub value=".$uai."_".$ub." class=$uai>".$ua." (Specific ".$udf_options['udfilinkfield']['headings'][$ub]['display'].")</option>";
						}
					}
				?></select>
				<span id=spn_udfilinkref><br /><select name=udfilinkref></select></span>
			</td>
		</tr>
		<!-- <tr>
			<th>Required:</th>
			<td><select name=udfirequired><option value=0 selected>No</option><option value=1>Yes</option></select></td>
		</tr>
		<tr>
			<th>Default Value:</th>
			<td><input type=text name=udfidefault /> <input type=button value=Clear name=btn_clear_date f=editudf /></td>
		</tr> -->
		<tr>
			<th></th>
			<td><input type=button value="Save Changes" class=isubmit name=btn_edit /></td>
		</tr>
	</table>
	<p class=float><input type=button value="Delete UDF" class=idelete /></p>
	</form>
</div>
<script type=text/javascript>
//var udfiobject = new Array();
//var udfiobject_keys = new Array();
//var udfilink = new Array();
//var udfilink_keys = new Array();
<?php
/*$uo = 0;
foreach($udf_options['udfiobject'] as $uai => $ua) {
	echo chr(10)."udfiobject[$uo] = '$ua';
	udfiobject_keys[$uo] = '$uai';
	udfilink[$uo] = new Array();
	udfilink_keys[$uo] = new Array()"; 
	foreach($udf_options['udfilinkfield'][$uai] as $ub) {
	//	echo "<option l=$ub value=".$uai."_".$ub.">".$ua." (Specific ".$udf_options['udfilinkfield']['headings'][$ub]['display'].")</option>";
	}
	$uo++;
}*/
?>
	$(function() {
		$("#tbl_edit th").addClass("left");
		$("#dlg_edit").dialog({
			modal: true,
			width: 500,
			height: 350,
			autoOpen: false
		});
		$(".edit").click(function() {
			var form = "form[name=editudf]";
			var id = $(this).prop("id");
			$(form+" #lbl_udfiid").prop("innerHTML",id);
			$(form+" input:hidden[name=udfiid]").val(id);
			Setup_udf.ajax_setupUDF(form,"pop_editudf","act=getUDF&i="+id);
			$("#dlg_edit").dialog("open");
		});
		$("#tbl_edit input:button[name=btn_edit]").click(function() {
			JSdisplayResult("info","info","Processing... Please wait.");
			var res = Setup_udf.validateForm("form[name=editudf]","EDIT");
			if(res.length>0) {
				JSdisplayResult(res[0],res[0],res[1]);
				return false;
			}
		});
		$("#dlg_edit input:button.idelete").click(function() {	
			if(confirm("Are you sure you wish to DELETE this UDF?")==true) {
				var id = $("form[name=editudf] input:hidden[name=udfiid]").val();
				Setup_udf.ajax_setupUDF("form[name=editudf]","DELETE","act=DELETE&i="+id);
			} 
		});
	});
</script>
</body>

</html>
