<?php
include("inc_ignite.php");
$helper->displayPageHeader();
?>
<h1 class=fc style="margin-bottom: -10px"><b><?php echo $modtext;?>: Setup - Create UDF</b></h1>
<p>&nbsp;</p>
<?php

if(isset($_POST['required']) && $_POST['required'] == 'on')
{
    $required = 1;
}else
{
    $required = 0;
}
//GET DETAILS
$value = isset($_POST['udfivalue']) ? $_POST['udfivalue'] : "";
$list = isset($_POST['udfilist']) ? $_POST['udfilist'] : "";
$default = isset($_POST['default']) ? $_POST['default'] :"";
$ref = "$modref";
$custom = "N";
$sort = 0;
$yn = "Y";

//format
$value = $helper->code($value);

//GET SORT
$sql = "SELECT max(udfisort) as isort FROM assist_".$cmpcode."_udfindex WHERE udfiref = '$modref'";
//include("inc_db_con.php");
$row = $helper->mysql_fetch_one($sql);//mysql_fetch_array($rs);
//mysql_close();
$sort = $row['isort']++;

if($sort==0 || strlen($sort)==0)
{
    $sort = 1;
}

//ADD TO UDFINDEX
$sql = "INSERT INTO assist_".$cmpcode."_udfindex SET ";
$sql.= "  udfivalue = '".$value."'";
$sql.= ", udfilist = '".$list."'";
$sql.= ", udfiref = '".$ref."'";
$sql.= ", udficustom = '".$custom."'";
$sql.= ", udfisort = ".$sort."";
$sql.= ", udfiyn = '".$yn."'";
$sql.= ", udfirequired = $required";
$sql.= ", udfidefault = '$default'";
//include("inc_db_con.php");
$udfiid = $helper->db_insert($sql);//mysql_insert_id();
$tref = "$modref";
$trans = "Created new UDF ".$udfiid;
$tsql = $sql;
//include("inc_transaction_log.php");
$sql = "INSERT INTO ".$dbref."_list_display SET ";
$sql.= "  headingfull = '".$value."'";
$sql.= ",  headingshort = '".$value."'";
$sql.= ",  type = 'U'";
$sql.= ",  field = '".$udfiid."'";
$sql.= ",  mydisp = 'N'";
$sql.= ",  myyn = 'Y'";
$sql.= ",  owndisp = 'N'";
$sql.= ",  ownyn = 'Y'";
$sql.= ",  alldisp = 'N'";
$sql.= ",  allyn = 'Y'";
$sql.= ",  allsort = '0'";
$sql.= ",  ownsort = '0'";
$sql.= ",  mysort = '0'";
$sql.= ", yn = '".$yn."'";
//include("inc_db_con.php");
$udfiid = $helper->db_insert($sql);//mysql_insert_id();
$tref = "$modref";
$trans = "Created new UDF ".$udfiid;
$tsql = $sql;
//include("inc_transaction_log.php");

echo("<p>UDF '".$value."' has been created.</p>");
echo("<p>&nbsp;</p><p><a href=# onclick=\"document.location.href = 'setup.php'\" class=grey><img src=/pics/tri_left.gif align=absmiddle border=0></a> <a href=# onclick=\"document.location.href = 'setup.php'\" class=grey>Go Back to Setup</a></p>");

?>


