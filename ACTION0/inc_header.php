<?php
//require_once '../inc_db_conn.php';
//require_once '../inc_codehelper.php';
require_once "inc_ignite.php"; 
require_once "inc.php";
require_once 'inc_ta.php';
$report_save_path = "reports";




date_default_timezone_set('Africa/Johannesburg');
$ah = new ASSIST_HELPER();
$adb = new ASSIST_DB("client");
//error_reporting(-1);

//UDF DEFAULTS
$udf_options = array(
	'udfilist' => array(
		'Y' => array('txt'=>"List",'type'=>"select",'align'=>"left"),
		'T' => array('txt'=>"Small Text",'type'=>"text",'align'=>"left"),
		'M' => array('txt'=>"Large Text",'type'=>"textarea",'align'=>"left"),
		'N' => array('txt'=>"Number",'type'=>"number",'align'=>"right"),
		'D' => array('txt'=>"Date",'type'=>"date",'align'=>"center"),
	),
	'udfirequired' => array(
		0 => "No",
		1 => "Yes",
	),
	'udfiobject' => array(
		'action' => $actname,
		'update' => "Update",
	),
	'udfilinkfield' => array(
		'all'		=> array("tasktopicid","taskstatusid"),
		'action'	=> array("tasktopicid"),
		'update'	=> array("tasktopicid","taskstatusid"),
		'headings'	=> array(),
		'tables'	=> array(
			'tasktopicid'	=> array(
				'tbl'		=> "list_topic",
				'id'			=> "pkey",
				'value'			=> "value",
				'active_fld'	=> "yn",
				'active'		=> "Y",
			),
			'taskstatusid'	=> array(
				'tbl'		=> "list_status",
				'id'			=> "id",
				'value'			=> "value",
				'active_fld'	=> "yn",
				'active'		=> "Y",
			),
		),
	),
);
if(isset($get_udf_link_headings) && $get_udf_link_headings) {
	$sql = "SELECT headingshort as display, field FROM ".$dbref."_list_display WHERE yn = 'Y' AND field IN ('".implode("','",$udf_options['udfilinkfield']['all'])."')";
		$udf_options['udfilinkfield']['headings'] = $am->mysql_fetch_all_fld($sql,"field");
}
//GET VALID UDFS
$udf_index = array('action'=>array('ids'=>array(),'index'=>array()),'update'=>array('ids'=>array(),'index'=>array()));
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$_SESSION['modref']."' AND udfiyn = 'Y'";
$r = $am->mysql_fetch_all($sql);
foreach($r as $row) {
	$obj = strlen($row['udfiobject'])>0 ? $row['udfiobject'] : "action";
	$udf_index[$obj]['ids'][] = $row['udfiid'];
	$udf_index[$obj]['index'][$row['udfiid']] = $row;
}
//unset($rs);

$headings = array();
$headings['action'] = array(
	'taskadddate'	=> "Created On",
	'taskadduser'	=> "Assigned By",
	'tasktkid'		=> "Assigned To",
	'tasktopicid'	=> "Topic",
	'taskurgencyid'	=> "Priority",
	'taskstatusid'	=> "Status",
	'taskdeadline'	=> "Deadline",
	'taskaction'	=> ucfirst($actname)." Instructions",
	'taskdeliver'	=> ucfirst($actname)." Deliverables",
	'taskattach'	=> "Attachment Details"
);
$headings['update'] = array(
	'logdate'		=> "Date logged",
	'logtkid'		=> "Logged By",
	'logupdate'		=> "Message",
	'logstatusid'	=> "Status",
	'logactdate'	=> "Date of Activity",
	'logattach'		=> "Attachment details",
);

if(!isset($page) || implode("_",$page)!="report_process") {
	ASSIST_HELPER::echoPageHeader("1.10.0",array(),array());



if(!isset($page)) {
	$page = explode("/",$_SERVER['PHP_SELF']);
	$page = $page[count($page)-1];
	if(strpos($page,".php")!==false) {
		$page = substr($page,0,-4);
	}
	$page = explode("_",$page);
}
$available_menu = array('setup');

if(in_array($page[0],$available_menu)) {
	$menu = array();
	switch($page[0]) {
	case "setup":
		$menu['defaults'] = array(
			'id'=>"defaults",
			'url'=>"setup.php",
			'active'=>(!isset($page[1]) || $page[1]!="access" ? true : false),
			'display'=>"Defaults"
		);
		$menu['user'] = array(
			'id'=>"user",
			'url'=>"setup_access.php",
			'active'=>(isset($page[1]) && $page[1]=="access" ? true : false),
			'display'=>"User Access"
		);
		break;
	}
	$level = 1;
	$am->drawNavButtons($menu);
	//echoNavigation($level,$menu);
}	//end if in available menu

if(isset($title)) {
	echo "<h1 id=h_title>";
	$new_title = array();
	foreach($title as $t) {
		$nt = "";
		if(isset($t['url']) && strlen($t['url'])>0) {
			$nt.="<a href='".$t['url']."' class=breadcrumb>";
		}
		$nt.=$t['txt']."</a>";
		$new_title[] = $nt;
	}
	echo implode(" >> ",$new_title);
	echo "</h1>";
}
	$am->JSdisplayResultPrep($t="");
}	//end not if report_process
include("inc_css.php");
include("inc_js.php");

?>