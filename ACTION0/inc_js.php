
<?php
$check_js = array("action");
$me = $_SERVER['PHP_SELF'];
$me = explode("/",$me);
$me = explode(".",$me[count($me)-1]);
$check_js[] = $me[0];
$me = explode("_",$me[0]);
$me = $me[0];
$check_js[] = $me;
foreach($check_js as $me) {
    if(file_exists('js/'.$me.'.js')) {
        ?>
        <script type ="text/javascript" src="<?php echo "js/".$me.".js";?>"></script>
        <?php
    }
}
?>
<script type=text/javascript>
$(function() {
	$("tr").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$(".datetime").datepicker({
					timeFormat: 'hh:mm',
					dateFormat: 'dd-M-yy',
					maxDate: new Date(),
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    changeMonth:true,
                    changeYear:true		
				});

});
function goNext(url,s,f) {
    f = escape(f);
    document.location.href = url+"s="+s+"&f="+f;
}
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
</script>