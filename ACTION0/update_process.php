<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$get_udf_link_headings = false;

$redirect_me = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";

require_once 'inc_header.php';
//arrPrint($_REQUEST);
echo "<p>Starting update...";

//arrPrint($_REQUEST);
$logdate = $today;
$logtkid = $tkid;
$logupdate = $am->code($_REQUEST['logupdate']);
$logstatusid = $_REQUEST['logstatusid'];
$logstate = $_REQUEST['logstatusid']==1 ? 100 : $_REQUEST['logstate'];
$logactdate = strtotime($_REQUEST['logactdate']);
$logemail = "Y";
$logsubmittkid = $tkid;
$logtaskid = $_REQUEST['logtaskid'];
$logtasktkid = "";
$logtype = "U";
echo "<p>1. Updating log.</p>";
$sql = "INSERT INTO ".$dbref."_log 
		(logid, logdate, logtkid, logupdate, logstatusid, logstate, logactdate, logemail, logsubmittkid, logtaskid, logtasktkid, logtype,logobjecttype) 
		VALUES 
		(null, $logdate, '$logtkid', '$logupdate', $logstatusid, $logstate, $logactdate, '$logemail', '$logsubmittkid', $logtaskid, '$logtasktkid', '$logtype','view_update')";
//echo "<P>".$sql;
$logid = $am->db_insert($sql);
//echo "<P>".$logid;
$sql = "UPDATE ".$dbref."_task SET taskstatusid = $logstatusid , taskstate = $logstate WHERE taskid = $logtaskid ";
$am->db_update($sql);

//echo "<h3>udfs</h3>";
echo "<p>2. Updating User Defined Fields.</p>";
$udftypes = unserialize($_REQUEST['udf_types']);
$udf_email = array();
$udf = isset($_REQUEST['udf']) ? $_REQUEST['udf'] : array();
foreach($udf as $key => $r) {
	$udfs = array();
	if(strlen($r)>0 && (!in_array($key,$udftypes['Y']) || $am->checkIntRef($r))) {
		if(in_array($key,$udftypes['D'])) { $r = strtotime($r); }
		$udfs[$key] = "(null,$key,'".$am->code($r)."',$logid,'".$_SESSION['modref']."')";
	}
	if(count($udfs)>0) {
		$sql = "INSERT INTO assist_".$cmpcode."_udf VALUES ".implode(",",$udfs);
		$am->db_insert($sql);
		//echo "<P>".$sql;
	}
	if(strlen($r)>0) {	$udf_email[$key] = $r; }
}



echo "<P>3. Uploading attachments.</p>";
//arrPrint($_FILES);
$path = $_SESSION['modref']."/update";
$am->checkFolder($path);
$path = "../files/".$cmpcode."/".$path;
$attach_count = 0;
foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
	if(strlen($tmp_name)>0 && $_FILES['attachments']['error'][$key] == 0) {
		$original_filename = $_FILES['attachments']['name'][$key];
		$parts = explode(".", $original_filename);
		$file = $parts[0];
		$ext = $parts[count($parts)-1];
		
		$sql = "INSERT INTO ".$dbref."_task_attachments 
				(taskid,logid,original_filename,system_filename,file_location) 
				VALUES 
				(0,$logid,'$original_filename','','update')";
		$docid = $am->db_insert($sql);
		//echo "<P>".$sql;
		$system_filename = $logid."_".$docid."_".date("YmdHis").".".$ext;
		$full_path = $path."/".$system_filename;
		//echo "  =>  ".$full_path;
		move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
		$attach_count+= $am->db_update("UPDATE ".$dbref."_task_attachments SET system_filename = '$system_filename' WHERE logid = $logid AND id = $docid");
	}
}



$task = getTask($logtaskid);


/*
$sql = "SELECT tasktkid as id, tkemail, CONCAT_WS(' ',tkname,tksurname) as name
		FROM ".$dbref."_task_recipients
		INNER JOIN assist_".$cmpcode."_timekeep ON tkid = tasktkid AND tkstatus = 1 AND tkemail <>  ''
		WHERE taskid = ".$logtaskid;
$rs = getRS($sql);
$recipients = array('ids'=>array(),'rec'=>array());
while($row = mysql_fetch_assoc($rs)) {
	$recipients['ids'][] = $row['id'];
	$recipients['rec'][$row['id']] = decode($row['name'])." <".$row['tkemail'].">";
}
unset($rs);
arrPrint($recipients);
if(count($recipients['ids'])>1) {// || ( count($recipients)==1 && !in_array($task['taskadduser'],$recipients['ids']) )) {
*/



$recipients = array('ids'=>array(),'rec'=>array());
//get task owner details
if($task['taskadduser']!=$tkid) {
	$sql = "SELECT tkid, tkemail, CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep WHERE tkid = ".$task['taskadduser']." AND tkstatus = 1 AND tkemail <> ''";
	$row = $am->mysql_fetch_one($sql);
	if(isset($row['tkemail'])) {
		$recipients['rec'][] = $row['name']." <".$row['tkemail'].">";
		$recipients['ids'][] = $row['tkid'];
	}
}
//get other recipient details
$sql = "SELECT tkid, tkemail, CONCAT_WS(' ',tkname,tksurname) as name
		FROM assist_".$cmpcode."_timekeep 
		INNER JOIN ".$dbref."_task_recipients 
		  ON tkid = tasktkid
		  AND taskid = ".$logtaskid."
		  AND tasktkid <> '".$tkid."'
		  AND tasktkid <> '".$task['taskadduser']."'
		WHERE tkstatus = 1 AND tkemail <> ''";
$rows = $am->mysql_fetch_all($sql);
if(count($rows)>0) {
	foreach($rows as $row) {
		$recipients['rec'][] = $row['name']." <".$row['tkemail'].">";
		$recipients['ids'][] = $row['tkid'];
	}
}

//arrPrint($recipients);

if(count($recipients['rec'])>0) {
	echo "<P>4. Sending notifications</p>";
	$userFrom = $am->mysql_fetch_one("SELECT CONCAT(tkname,' ',tksurname,' <',tkemail,'>') as userfrom FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tkid."' AND tkstatus = 1 AND tkemail <> ''");
	if(isset($userFrom['userfrom']) && strlen($userFrom['userfrom'])>0) {
		$userFrom = $userFrom['userfrom'];
		if(strpos($userFrom,"igniteassist.co.za")!==false) {
			$userFrom = "no-reply@ignite4u.co.za";
		}
	} else {
		$userFrom = "no-reply@ignite4u.co.za";
	}
	$sql = "SELECT * 
			FROM assist_".$cmpcode."_udfindex
			 LEFT OUTER JOIN assist_".$cmpcode."_udf
			 ON udfindex = udfiid 
			 AND udfref = '".strtoupper($_SESSION['modref'])."'
			 AND udfnum = $logtaskid
			WHERE udfiobject = 'action'
			AND udfiyn = 'Y'
			AND udfiref = '".strtoupper($_SESSION['modref'])."'
			AND (
			  udfilinkfield = '' 
			  OR (udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid'].")
			)
			ORDER BY udfisort, udfivalue";
	$task_udfs = $am->mysql_fetch_all($sql);
	$udf_lists = getUDFListItems("");


	$subject = $am->decode($_SESSION['modtext']." $actname $logtaskid Updated");
$message = $_SESSION['modtext']." $actname $logtaskid has been updated by ".$_SESSION['tkn'].".\n
Update details
Message: ".str_replace(chr(10),chr(10)."   ",$am->decode($logupdate))."
Status: ".$task['status']." (".$logstate."%) 
Date of Activity: ".date("d-M-Y H:i",$logactdate);
	foreach($udf_email as $key => $r) {
		$message.="\n".$udf_index['update']['index'][$key]['udfivalue'].": ";
		switch($udf_index['update']['index'][$key]['udfilist']) {
		case "N": $message.=number_format($r,2,".",","); break;
		case "D": $message.= (strlen($r)>0 && is_numeric($r) ? date("d-M-Y",$r) : $r); break;
		case "Y": $message.=(isset($udf_lists[$key][$r]) ? $am->decode($udf_lists[$key][$r]['udfvvalue']) : "[Unspecified]"); break;
		default: $message.=$r;
		}
	}
$message.=($attach_count>0 ? "\n".$attach_count." file(s) were attached to this update.  Please access the $actname on Assist in order to view the attachment(s)." : "")."

$actname details
Created: ".date("d-M-Y H:i:s",$task['taskadddate'])."
Topic: ".$task['topic']."
Deadline: ".date("d-M-Y",$task['taskdeadline'])."
Instructions: ".str_replace(chr(10),chr(10)."   ",$am->decode($task['taskaction']))."
Deliverables: ".str_replace(chr(10),chr(10)."   ",$am->decode($task['taskdeliver']));
	foreach($task_udfs as $tu) {
		$message.="\n".$tu['udfivalue'].": ";
		if(strlen($tu['udfvalue'])==0 || ($tu['udfilist']=="Y" && !$am->checkIntRef($tu['udfvalue']))) {
			$message.="[Unspecified]";
		} else {
			$r = $tu['udfvalue'];
			$key = $tu['udfindex'];
			switch($tu['udfilist']) {
			case "N": $message.=number_format($r,2,".",","); break;
			case "D": $message.= (strlen($r)>0 && is_numeric($r) ? date("d-M-Y",$r) : $r); break;
			case "Y": $message.=(isset($udf_lists[$key][$r]) ? $am->decode($udf_lists[$key][$r]['udfvvalue']) : "[Unspecified]"); break;
			default: $message.=$r;
			}
		}
	}
$message.="

Please log onto Assist to view the full history of this $actname.";
	//echo "<P>".$subject."</p><P>".str_replace("\n","<br />",$message);
	//if(strtoupper($cmpcode)=="IASSIST") { 
		//don't send the email while clearing bug tracker list
	//} else {
$userFrom = "no-reply@ignite4u.co.za";
		//mail(implode(", ",$recipients['rec']),$subject,$message,"From: no-reply@ignite4u.co.za");
    $mail = new ASSIST_EMAIL(implode(", ",$recipients['rec']),$subject,$message);
    $mail->sendEmail();
	//}
}
echo "<P>Update complete!</p>";
if(isset($_REQUEST['src']) && strlen($_REQUEST['src'])>0) {
	if($redirect_me=="dashboard" || $_REQUEST['src']=="home") {
		echo "
		<script type=text/javascript>
			parent.header.$('#backHome').trigger('click');
			//$('#backHome', window.parent.header.document).click();
		</script>";
	} else {
		echo "<script type=text/javascript>document.location.href = 'view_list.php?get_details=1';</script>";
		//echo "<script type=text/javascript>document.location.href = '".$_REQUEST['src']."';</script>";
	}
}


?>