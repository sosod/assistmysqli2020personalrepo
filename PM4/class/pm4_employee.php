<?php
/**
 * To manage the link between EMPLOYEE Assist and PM4
 *
 * Created on: 1 January 2016
 * Authors: Janet Currie
 *
 */

class PM4_EMPLOYEE extends PM4 {

	private $employee_modref = "EMP";


    public function __construct() {
		parent::__construct();
    }


	/**
	 * Get list of employees who can have scorecards created
	 */
	public function getAvailableEmployeesForCreate() {
		$local_modref = $this->getModRef();
		$data = array();
		//get list of current scorecards which are not deactivated - either active or pending
		$scdObject = new PM4_SCORECARD();
		$active_scorecards = $scdObject->getAllEmployeesAndJobsWithCurrentScorecards();
		//get list of active users with menu access
		$userObj = new PM4_USERACCESS();
		$users = $userObj->getMenuUsersFormattedForSelect();
		//get list of non-system employees && active user employees with menu access who aren't in the above list
		$empObj = new EMP1_INTERNAL($this->employee_modref);
		$employees = $empObj->getAllCurrentEmployeesWithStatusAndJobs();
		//loop through employees and
		foreach($employees as $tkid => $jobs) {
			foreach($jobs as $job_id => $job_detail) {
				//remove any users with active scorecards
				if(isset($active_scorecards[$tkid][$job_id])) {
					//ignore job
				} else {
					//remove any active users without menu access
					if($job_detail['is_system_user']==1 && !isset($users[$tkid])) {
						//ignore user
					} else {
						if(isset($data[$tkid])) {
							$data[$tkid]['job'][] = $job_id;
						} else {
							$data[$tkid] = array(
								'tkid'=>$tkid,
								'emp_id'=>$job_detail['emp_id'],
								'name'=>$job_detail['emp_name'],
								'is_system_user'=>$job_detail['is_system_user'],
								'job'=>array($job_id),
							);
						}
					}
				}
			}
		}
		$this->setDBRef($local_modref);
/*
		echo "<h3 class=red>active scorecards</h3>";
		ASSIST_HELPER::arrPrint($active_scorecards);
		echo "<h3 class=red>users</h3>";
		ASSIST_HELPER::arrPrint($users);
		echo "<h3 class=red>employees</h3>";
		ASSIST_HELPER::arrPrint($employees);
*/
		return $data;
	}





	public function getJobsFormattedForSelect($var) {
		$local_modref = $this->getModRef();

		$job_id = explode("|",$var['job_id']);
		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getListOfJobs($job_id);

		$this->setDBRef($local_modref);

		return $data;
	}





	public function getJobDetail($job_id) {
		$local_modref = $this->getModRef();

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getJobDetail($job_id);

		$this->setDBRef($local_modref);

		return $data;
	}






	public function getMyStaffJobs($type) {
		$local_modref = $this->getModRef();

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		$data = $empObject->getMyStaffJobs($type);

		$this->setDBRef($local_modref);

		return $data;
	}

	public function getAllJobTitlesByJobIDFormattedForSelect($job_id_list=array()) {
		$local_modref = $this->getModRef();

		$empObject = new EMP1_INTERNAL($this->employee_modref);
		//$data = $empObject->getAllJobTitlesFormattedForSelect();
		$data = $empObject->getListOfJobs($job_id_list);
		$this->setDBRef($local_modref);

		return $data;
	}

	/*
	 * TSHEGO REPORT CODE STARTS HERE*/

    public function getAllJobTitlesFormattedForSelect() {
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getAllJobTitlesFormattedForSelect();

        $this->setDBRef($local_modref);

        return $data;
    }

    public function getAllDepartmentsFormattedForSelect() {
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getAllDepartmentsFormattedForSelect();

        $this->setDBRef($local_modref);

        return $data;
    }

    public function getAllJobLevelsFormattedForSelect() {
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getAllJobLevelsFormattedForSelect();

        $this->setDBRef($local_modref);

        return $data;
    }

    public function getEmployee($emp_id) {
        $local_modref = $this->getModRef();

        $empObject = new EMP1_INTERNAL($this->employee_modref);
        $data = $empObject->getEmployee($emp_id);

        $this->setDBRef($local_modref);

        return $data;
    }




	public function __destruct() {
		parent::__destruct();
	}

}


?>