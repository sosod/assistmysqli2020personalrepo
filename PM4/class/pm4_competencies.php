<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class PM4_COMPETENCIES extends PM4 {

    public $treeview_datastructure;

    public function __construct() {
        parent::__construct();
    }

    public function getCompetencyTreeViewDataStructureByCompetencyID($competency_id){
        $competency = $this->getCompetencyByID($competency_id);
        $this->treeview_datastructure = $competency;
        $this->addProficienciesToTreeViewDataStructure($competency_id);
        $this->addCompetencyLevelsToTreeViewDataStructure($competency_id);

        return $this->treeview_datastructure;
    }

    public function getCompetencyByID($competency_id){
        $list_id = 'competencies';
        $listObj = new PM4_LIST($list_id);
        $list_data = $listObj->getListItemsForSetup();
        $competency = array();
        $competency[$competency_id] = $list_data[$competency_id];
        return $competency;
    }

    public function addProficienciesToTreeViewDataStructure($competency_id){
        $proficiencies = $this->getAllProficiencies();

        foreach($proficiencies as $key => $val){
            $this->treeview_datastructure[$competency_id]['proficiencies'][$key] = $val;
        }
    }

    public function getAllProficiencies(){
        $list_id = 'proficiencies';
        $listObj = new PM4_LIST($list_id);
        $list_data = $listObj->getListItems();
        ksort($list_data);
        return $list_data;
    }

    public function addCompetencyLevelsToTreeViewDataStructure($competency_id){
        $competency_levels = $this->getCompetencyLevelsByCompetencyID($competency_id);
        foreach($competency_levels as $key => $val){
            $proficiency_id = $val['proficiency_id'];
            $this->treeview_datastructure[$competency_id]['proficiencies'][$proficiency_id]['competency_levels'][$key] = $val;
        }
    }


    public function getCompetencyLevelsByCompetencyID($competency_id){
        $list_id = 'competencies_levels';
        $listObj = new PM4_LIST($list_id);
        $field_name = 'competency_id';
        $field_value = $competency_id;
        $list_data = $listObj->getListItemsByFieldID($field_name, $field_value);
        return $list_data;
    }

    public function editCompetencyLevels($var){
        $object_id = $var['object_id'];
        unset($var['object_id']);

        $list_vars = array();
        $list_vars['id'] = $object_id;

        foreach($var as $key => $val){
            $lv_key = ($key == 'proficiency' ? $key . '_id' : $key);
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies_levels';
        $listObj = new PM4_LIST($list_id);

        return $listObj->editObject($list_vars);

    }

    public function addCompetencyLevel($var){
        unset($var['object_id']);

        $list_vars = array();
        foreach($var as $key => $val){
            $lv_key = ($key == 'proficiency' || $key == 'competency' ? $key . '_id' : $key);
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies_levels';
        $listObj = new PM4_LIST($list_id);

        return $listObj->addObject($list_vars);

    }

    public function editCompetency($var){
        $object_id = $var['object_id'];
        unset($var['object_id']);

        $list_vars = array();
        $list_vars['id'] = $object_id;

        foreach($var as $key => $val){
            $lv_key = $key;
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies';
        $listObj = new PM4_LIST($list_id);

        return $listObj->editObject($list_vars);

    }

    public function addCompetency($var){
        unset($var['object_id']);
        unset($var['list_id']);

        $list_vars = array();
        foreach($var as $key => $val){
            $lv_key = $key;
            $lv_val = urldecode($val);
            if($lv_val != 'null'){
                $list_vars[$lv_key] = $lv_val;
            }

        }

        $list_id = 'competencies';
        $listObj = new PM4_LIST($list_id);

        return $listObj->addObject($list_vars);

    }

}


?>