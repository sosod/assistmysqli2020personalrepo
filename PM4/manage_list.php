<?php
//$page_action = "SELF";//PM4_TRIGGER::SELF_ASSESS;

/******
 * Requirements:
 * page_action = SELF || MOD || FINAL
 * page_section = self || moderation || final
 */

require_once("inc_header.php");

//end();
$scdObj = new PM4_SCORECARD();
$assessObj = new PM4_ASSESSMENT();
$triggerObj = new PM4_TRIGGER();

$incomplete_triggers = $triggerObj->getTriggersForCompletion($page_action);

$complete_triggers = $triggerObj->getCompletedTriggers($page_action);

$trigger_headings = array("in_progress"=>"Outstanding |ASSESSMENTS|",'complete'=>"Completed |ASSESSMENTS|");
$all_triggers = array('in_progress'=>$incomplete_triggers,'complete'=>$complete_triggers);


$scd_object_names = $scdObj->getObjectName($scdObj->getMyObjectType(),true);
$scd_object_name = $scdObj->getObjectName($scdObj->getMyObjectType());

$asmt_object_names = $assessObj->getObjectName($assessObj->getMyObjectType(),true);
$asmt_object_name = $assessObj->getObjectName($assessObj->getMyObjectType());


foreach($trigger_headings as $section_type => $section_heading) {
	$section_heading = $scdObj->replaceAllNames($section_heading);
	$triggers = $all_triggers[$section_type];
	$active_objects = $triggers['objects'];
	$summary_details = $triggers['details'];

	echo "<h2>".$section_heading."</h2>";

	?>
	<table class=list id=tbl_list_<?php echo $section_type; ?>>
		<tr>
			<th>Ref</th>
			<?php if($page_action != $triggerObj->getSelfType()) { ?>
				<th>Employee</th><?php } ?>
			<th>Assessment Period</th>
			<th>Organisational<Br/>KPIs</th>
			<th>Individual<br/>KPIs</th>
			<th>Organisational<Br/>Top Layer KPIs</th>
			<th>Individual<br/>Top Layer KPIs</th>
			<th>Organisationl<br/>Projects</th>
			<th>Individual<br/>Projects</th>
			<th>Core<br/>Competencies</th>
			<th>Function<br/>Activities</th>
			<th>Deadline</th>
			<th>Status</th>
			<th></th>
		</tr>
		<?php
		if(count($active_objects) > 0) {
			foreach($active_objects as $pa) {
				$pa['count'] = $summary_details[$pa['parent_id']]['count'];
				echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			".($page_action != $triggerObj->getSelfType() ? "<td>".$pa['employee']."</td>" : "")."
			<td class='' >".$pa['period']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
			<td class=center>
				".$pa['deadline']."
			</td>
			<td class=center>
				".$pa['status']."
			</td>
			<td class=center>
				".($pa['can_continue'] ? "<button class=btn_continue obj_id=".$pa['id'].">Complete</button>" : "<button class=btn_view obj_id=".$pa['id'].">View</button>")."
			</td>
		</tr>";
			}
		}
		else {
			echo "
	<tr>
		<td></td><td colspan=12>No ".$asmt_object_names." available.</td>
	</tr>";
		}

		?>
	</table>

	<?php
}
?>

<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");
	
	$(".btn_view").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "manage_<?php echo $page_section; ?>_view.php?view_type=<?php echo $page_action; ?>&obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});
	
	$(".btn_continue").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).click(function() {
		document.location.href = "manage_<?php echo $page_section; ?>_details.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

});
</script>