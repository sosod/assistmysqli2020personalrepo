<?php

// clone of "risk_versus_control.php"

$period_id = $_REQUEST['filter']['period_id'];
unset($_REQUEST['filter']['period_id']);
$filters = $_REQUEST['filter'];

$scdObj = new PM4_SCORECARD();
$assessObj = new PM4_ASSESSMENT();
$assessments = $assessObj->getAssessmentForStatusReportByPeriod($period_id, $filters);

//Employee Fields\
$emp_obj = new PM4_EMPLOYEE();
$job_titles = $emp_obj->getAllJobTitlesFormattedForSelect();
$departments = $emp_obj->getAllDepartmentsFormattedForSelect();
$job_levels = $emp_obj->getAllJobLevelsFormattedForSelect();


//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($assessments);
//echo '</pre>';

$new_count = 0;
$triggered_count = 0;
$Inactive_count = 0;
?>
<table class=list id=tbl_list>
    <tr>
        <th>Ref</th>
        <!-- EMPLOYEE DETAILS - BEGIN -->
        <th>Employee</th>
        <th>Job Title</th>
        <th>Department</th>
        <th>Manager</th>
        <th>Job Level</th>
        <!-- EMPLOYEE DETAILS - END -->

        <!-- LINES - BEGIN -->
        <th>Organisational<Br />KPIs</th>
        <th>Individual<br />KPIs</th>
        <th>Organisational<Br />Top Layer KPIs</th>
        <th>Individual<br />Top Layer KPIs</th>
        <th>Organisationl<br />Projects</th>
        <th>Individual<br />Projects</th>
        <th>Core<br />Competencies</th>
        <th><?php echo $scdObj->getObjectName(PM4_ASSESSMENT::OBJECT_TYPE,true); ?></th>
        <!-- LINES - END -->


        <!-- STATUS DETAILS - BEGIN -->
        <th>Activation Status</th>
        <th>Activated On</th>

        <th>Evaluation Status</th>
        <th>Evaluated On</th>
        <!-- STATUS DETAILS - END -->
    </tr>
    <?php if(count($assessments)>0) { ?>
        <?php foreach($assessments as $pa) { ?>
            <tr>
                <td class='center b'><?php echo $pa['asmt_id'] ?></td>
                <!-- EMPLOYEE DETAILS - BEGIN -->
                <td emp_tkid="<?php echo $pa['scd_tkid'] ?>"><?php echo $pa['employee'] ?></td>
                <td><?php echo ((int)$pa['job_id'] != 0 ? $job_titles[$pa['job_id']] : $emp_obj->getUnspecified()) ?></td>
                <td><?php echo $departments[$pa['job_dept']] ?></td>
                <td><?php echo $job_titles[$pa['job_manager_title']] ?></td>
                <td><?php echo ((int)$pa['job_level'] != 0 ? $job_levels[$pa['job_level']] : $emp_obj->getUnspecified()) ?></td>
                <!-- EMPLOYEE DETAILS - END -->

                <!-- LINES - BEGIN -->
                <td><?php echo (isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)?></td>
                <td><?php echo (isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)?></td>
                <td><?php echo (isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)?></td>
                <td><?php echo (isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)?></td>
                <td><?php echo (isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)?></td>
                <td><?php echo (isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)?></td>
                <td><?php echo (isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)?></td>
                <td class=center><?php echo (isset($pa['assessments']) ? $pa['assessments'] : 0) ?></td>
                <!-- LINES - END -->

                <!-- STATUS DETAILS - BEGIN -->
                <td class=center><?php echo $pa['status'] ?></td>
                <td class=center><?php echo $pa['status_last_update'] ?></td>

                <td class=center><?php echo $pa['eval_status'] ?></td>
                <td class=center><?php echo $pa['eval_last_update'] ?></td>
                <!-- STATUS DETAILS - END -->
            </tr>

            <?php
                if($pa['status'] == 'New'){
                    $new_count++;
                }elseif($pa['status'] == 'Triggered'){
                    $triggered_count++;
                }else{
                    $Inactive_count++;
                }
            ?>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td></td><td colspan=21>No Objects available.</td>
        </tr>
    <?php } ?>
</table>

<script type="text/javascript" >
    $(function() {
        $("#tbl_list td").addClass("center");
        $("#tbl_list tr").find("td:eq(1)").removeClass("center");

    });
</script>

<h3>Overall Summary of Results</h3>
<table class="summary">
    <tr>
        <td style="color: #FFFFFF; text-align: center; background-color: #000077;">N</td>
        <td>New</td>
        <td style="text-align:right"><?php echo $new_count ?></td>
    </tr>
    <tr>
        <td style="color: #FFFFFF; text-align: center; background-color: #009900;">T</td>
        <td style="width: 200px;">Triggered</td>
        <td style="text-align:right"><?php echo $triggered_count ?></td>
    </tr>
    <tr>
        <td style="color: #FFFFFF; text-align: center; background-color: #CC0001;">IA</td>
        <td>Inactive</td>
        <td style="text-align:right"><?php echo $Inactive_count ?></td></tr>
    <tr>
        <td></td>
        <td style="font-weight: bold; text-align: right;">Total:</td>
        <td style="font-weight: bold; text-align: right;"><?php echo count($assessments) ?></td>
    </tr>
</table>
