<?php

$page_action = "view";

require_once("inc_header.php");

//$userObj = new PM4_USERACCESS();
//$users = $userObj->getMenuUsersFormattedForSelect();
//$user_ids = array_keys($users);

$scdObj = new PM4_SCORECARD();
$active_objects = $scdObj->getMyObjects();
//$active_users = $scdObj->getUsersWithActiveAssessments();

//$number_of_steps_in_create_process = $scdObj->getNumberOfSteps();

//$employees = array_diff($user_ids, $active_users);

$scd_object_names = $scdObj->getObjectName($scdObj->getMyObjectType(),true);
$scd_object_name = $scdObj->getObjectName($scdObj->getMyObjectType());


//ASSIST_HELPER::arrPrint($pending_assessments);

?>
<table class=list id=tbl_list>
	<tr>
		<th>Ref</th>
		<th>Employee</th>
		<th>Organisational<Br />KPIs</th>
		<th>Individual<br />KPIs</th>
		<th>Organisational<Br />Top Layer KPIs</th>
		<th>Individual<br />Top Layer KPIs</th>
		<th>Organisationl<br />Projects</th>
		<th>Individual<br />Projects</th>
		<th>Core<br />Competencies</th>
		<th>Status</th>
		<th></th>
	</tr>
<?php
if(count($active_objects)>0) {
	foreach($active_objects as $pa) {
		//$pa['count'][$type][$mod_type]
		echo "
		<tr>
			<td class='center b'>".$pa['ref']."</td>
			<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
			<td class=center>
				".$pa['status']."
			</td>
			<td class=center>
				<button class=btn_view obj_id=".$pa['obj_id'].">View</button>
			</td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td></td><td colspan=10>No ".$scd_object_names." available.</td>
	</tr>";
}

?>
</table>
<script type="text/javascript">
$(function() {
	$("#tbl_list td").addClass("center");
	$("#tbl_list tr").find("td:eq(1)").removeClass("center");

	$(".btn_view").button({
		icons: {primary: "ui-icon-newwin"},
	}).click(function(e) {
		e.preventDefault();
		document.location.href = "manage_view_object.php?obj_id="+$(this).attr("obj_id");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});


});
</script>