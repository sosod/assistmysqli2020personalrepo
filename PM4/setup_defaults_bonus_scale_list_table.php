<?php

//$page_action = "trigger";

require_once("inc_header.php");


$scdObj = new PM4_SCORECARD();
$scorecards = $scdObj->getObjectsForTrigger();


$list_id = 'bonus_scale';

$listObj = new PM4_LIST($list_id);
//echo $listObj->getListTable();
$fields = $listObj->getFieldNames();
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
$items = $listObj->getListTable();
$list_data = $listObj->getListItemsForSetup();
$cat_sups = array();



ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


if($list_id == "competencies"){
    $information = "Choosing a Competency requires setting up Competency Categories first. ";
    $information .= "";
    $information .= "To Edit Competencies, or Add Competency Levels, click the \"Open\" button ";
    echo ASSIST_HELPER::getFloatingDisplay(array("info",$information));
    $listObj2 = new PM4_LIST("competency_category");
    $cat_list_tems = $listObj2->getListItems();
    foreach($cat_list_tems as $list => $val){
        if($val['status'] == RGSTR2::SYSTEM_DEFAULT || $val['status'] == RGSTR2::ACTIVE){
            $cat_sups[$val['id']]=$val['name'];
        }
    }
    if(count($cat_sups)==0) {
        ASSIST_HELPER::displayResult(array("error","There are currently no Competency Categories available.  Add / Edits may only be made if there are Competency Categories available.  Please visit the Setup > Defaults > Lists > Competency Categories page first."));
    }
}elseif($list_id=="bonus_scale") {
	$cat_sups = array(3=>"3",5=>"5");
}
?>
<h2><?php echo $listObj->replaceObjectNames("|bonusscale|"); ?></h2>
<form name=frm_list>
    <input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id; ?>" />
    <table class=list id=tbl_list>
        <tr>
            <?php foreach($fields as $fld=>$val){ ?>
                <th><?php echo $val ?></th>
            <?php } ?>
            <th></th>
        </tr>

        <!--  Add Form TAKEN FROM setup_defaults_lists.php  -->
        <?php echo "<tr id=\"add_row\">"; ?>
        <?php foreach($fields as $fld=>$name){ ?>
            <?php
            echo"<td >";
            //echo $fld;
            if($fld == "status"){
                echo "<div class=center>Active</div><input type=\"hidden\" name='$fld' id='$fld' value=" . PM4::ACTIVE . " />";
            }else if($fld == "list_num" || $fld == "category" || $fld == "divisor"){
                $js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld, 'options'=>$cat_sups,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0"),'req'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
            }else if($fld == "colour"){
                echo "<input type=\"hidden\" name='$fld' id=\"add_colour\" value=\"#FFFFFF\" />";
                $js.= $displayObject->drawFormField($types[$fld], array('id'=>'clr_'.$fld, 'name'=>$fld,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
            }else{
                $js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld,'require_me'=>(!isset($required_fields[$fld]) || $required_fields[$fld]==true ? "1" : "0")));
            }
            echo"</td>";
            ?>
        <?php } ?>
        <?php echo "<td class=center><input type=button name=btn_add value=Add /></td></tr>"; ?>
        <!--  Add Form TAKEN FROM setup_defaults_lists.php  -->

        <?php foreach($list_data as $key=>$val){ ?>
            <tr id="tr_<?php echo $val['id'] ?>" <?php echo ((($val['status'] & PM4::INACTIVE)==PM4::INACTIVE) ? "class=inact" : "") ?> >
                <?php
                foreach($fields as $fld=>$head){
                    $valkey = $fld;
                    $valval = $val[$fld];
                    switch($valkey){
                        case "sort":
                            break;
                        case "status":
                            if(($valval & PM4::SYSTEM_DEFAULT)==PM4::SYSTEM_DEFAULT){
                                echo "<td sys=1 class=\"center\">System Default";
                            }else if(($valval & PM4::ACTIVE)==PM4::ACTIVE){
                                echo "<td class=\"center\">Active";
                            }else if(($valval & PM4::INACTIVE)==PM4::INACTIVE){
                                echo "<td class=\"center\">Inactive";
                            }else{
                                echo "<td class=\"center\">";
                            }

                            echo "</td>";
                            break;
                        case "list_num":
                        case "category":
                            if(isset($cat_sups[$valval])){
                                echo "<td fld=\"". $valkey ."\">". $cat_sups[$valval]."</td>";
                            } else {
                                echo "<td fld=\"". $valkey ."\">".$helper->getUnspecified()."</td>";
                            }
                            break;
                        case "colour":
                            echo "<td style=\"background-color:".$valval."\" fld=\"". $valkey ."\"></td>";
                            break;
                        default:
                            echo "<td fld=\"". $valkey ."\">".$valval."</td>";
                    }
                }
                ?>

                <?php if(($val['status']& PM4::ACTIVE)==PM4::ACTIVE){ ?>
                    <td class=center><button class=btn_open bonus_scale_id="<?php echo $val['id'] ?>">Open</button></td>
                <?php }else{ ?>
                    <td class=center><button class=btn_open bonus_scale_id="<?php echo $val['id'] ?>">Open</button></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <?php
        //    if(count($scorecards)>0) {
        //        foreach($scorecards as $pa) {
        //            //$pa['count'][$type][$mod_type]
        //            echo "
        //		<tr>
        //			<td class='center b'>".$pa['ref']."</td>
        //			<td class='' emp_tkid='".$pa['employee_tkid']."'>".$pa['employee']."</td>
        //			<td>".(isset($pa['count']['KPI']['O']) ? $pa['count']['KPI']['O'] : 0)."</td>
        //			<td>".(isset($pa['count']['KPI']['I']) ? $pa['count']['KPI']['I'] : 0)."</td>
        //			<td>".(isset($pa['count']['TOP']['O']) ? $pa['count']['TOP']['O'] : 0)."</td>
        //			<td>".(isset($pa['count']['TOP']['I']) ? $pa['count']['TOP']['I'] : 0)."</td>
        //			<td>".(isset($pa['count']['PROJ']['O']) ? $pa['count']['PROJ']['O'] : 0)."</td>
        //			<td>".(isset($pa['count']['PROJ']['I']) ? $pa['count']['PROJ']['I'] : 0)."</td>
        //			<td>".(isset($pa['count']['CC']['O']) ? $pa['count']['CC']['O'] : 0)."</td>
        //			<td>".(isset($pa['count']['JAL']['O']) ? $pa['count']['JAL']['O'] : 0)."</td>
        //			<td class=center>".(isset($pa['assessments']) ? $pa['assessments'] : 0)."</td>
        //			<td class=center>
        //			<button class=btn_open obj_id=".$pa['obj_id'].">Open</button>
        //			</td>
        //		</tr>";
        //        }
        //    } else {
        //        echo "
        //	<tr>
        //		<td></td><td colspan=7>No ".$scdObj->getObjectName($scdObj->getObjectType(),true)." available.</td>
        //	</tr>";
        //    }

        ?>
        <tr>
            <td colspan="6"><?php $js.= $displayObject->drawPageFooter($helper->getGoBack("setup_defaults.php"),"setup",array('section'=>"BS")); ?></td>
        </tr>
    </table>
</form>
<?php



?>

<script type="text/javascript">
    $(function() {
        <?php
        /*****************
         * echo stored $js strings here
         */
        echo  $js ;
        ?>

        $("input[name='btn_add']").button({
            icons: {primary: "ui-icon-disk"},
        }).click(function() {
            var okay = true;

            $("#master_list tr#add_row").find("input:text, textarea, select").removeClass("required").each(function(){
                var require_me = $(this).attr("require_me");
                require_me = (require_me==true || require_me=="1" || require_me==1) ? true : false;
                if(require_me==true && $(this).val().length==0) {
                    okay = false
                    $(this).addClass("required");
                }
            });

            if($("#list_num").val() == "X" || $("#list_num").val() == "0"){
                okay = false;
                $("#list_num").addClass("required");
            }
            if($("#category").val() == "X" || $("#category").val() == "0"){
                okay = false;
                $("#category").addClass("required");
            }

            if(okay != true){
                alert("Please fill in all of the required fields as highlighted.");
            }else{
                AssistHelper.processing();
                var dta = AssistForm.serialize($("form[name=frm_list]"));
                var result = AssistHelper.doAjax("inc_controller.php?action=Lists.SimpleAdd",dta);
                //console.log(result);
                //alert(result);
                if(result[0]=="ok") {
                    document.location.href = "setup_defaults_bonus_scale_list_table.php?l=bonus_scale" +"&r[0]="+result[0]+"&r[1]="+result[1];
                    //		alert("complete! WP");
                } else {
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }
            }
        });

        $("#tbl_list td").addClass("center");
        $("#tbl_list tr").find("td:eq(1)").removeClass("center");

        $(".btn_open").button({
            icons: {primary: "ui-icon-newwin"},
        }).click(function(e) {
            e.preventDefault();
            document.location.href = "setup_defaults_bonus.php?bonus_scale_id="+$(this).attr("bonus_scale_id");
        }).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
        });
    });
</script>