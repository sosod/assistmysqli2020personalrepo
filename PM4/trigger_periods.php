<?php
require_once("inc_header.php");


$apObj = new PM4_PERIOD();
$periods = $apObj->getAllAssessmentPeriods();
$available_periods = count($periods)>0?true:false;


ASSIST_HELPER::displayResult(array("info","Under development"));


?>
<table class=list>
	<tr>
		 <th>Ref</th>
		 <th>Name</th>
		 <th>Start Date</th>
		 <th>End Date</th>
		 <th>Status</th>
		 <th></th>
	</tr>
	<?php
	
	if($available_periods) {
		foreach($periods as $pi => $p) {
			echo "
			<tr>
				 <td>".$pi."</td>
				 <td>".$p['name']."</td>
				 <td>".date("d M Y",strtotime($p['start_date']))."</td>
				 <td>".date("d M Y",strtotime($p['end_date']))."</td>
				 <td>".$p['status']."</td>
				 <td><!-- <button>Edit</button> --></td>
			</tr>";
		}
	} else {
		echo "
		<tr>
			<td colspan=6>No periods available.</td>
		</tr>";
	}
	
	?>
</table>