<table id=ed_frm class=form>
    <?php foreach($fields as $fld => $name){ ?>
        <tr>
            <th><?php echo $name; ?></th>
            <td fld=edit_td_<?php echo $fld; ?> id="edit_td_<?php echo $fld; ?>">
                <?php if($fld == "status"){; ?>
                    Active
                <?php }else if($fld == "category"){ ?>
                    <?php echo (isset($cat_sups) ? $cat_sups[$list_data[$object_id][$fld]] : $list_data[$object_id][$fld]); ?>
                <?php }else if($fld == "competency" || $fld == "proficiency"){ ?>
                    <?php
                        $field_name = $fld . '_id';

                        if($fld == "competency" && isset($competency)){
                            $field_value = $competency[$competency_id]['name'];
                        }elseif($fld == "proficiency" && isset($proficiencies)){
                            $field_value = $proficiencies[$competency_levels[$object_id][$field_name]]['name'];
                        }else{
                            $field_value = 'N/A';
                        }
                        echo $field_value;
                    ?>
                <?php }else{ ?>
                    <?php echo $list_data[$object_id][$fld]; ?>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    <?php
    ?>
</table>