<?php
/**
 * FOR DISPLAYING ASSESSMENT RESULTS
 *
 * Required variables:
 * $page_action = "SELF" || "MOD" || "FINAL" || "REPORT"
 * $page_section = self || moderation || final || "report"
 * $user_ids = array()
 * $assess_id
 * $onscreen_display=array()
 */
//require_once("inc_header.php");
//error_reporting(-1);
//$create_type = "PROJ";
//$create_name = "Project";


$head = $headingObject->getScoreHeadingsForDisplay("KPI");

$triggerObj = new PM4_TRIGGER();
$assessObj = new PM4_ASSESSMENT();
$error = false;

$weighted_score_rounding_precision = $triggerObj->getWeightedScoreRoundingPrecision();

$self_name = $triggerObj->getTypeName($triggerObj->getSelfType());
$mod_name = $triggerObj->getTypeName($triggerObj->getModeratorType());
$moderator_name = $triggerObj->replaceAllNames("|MODERATOR|");
$moderators_name = $triggerObj->replaceAllNames("|MODERATORS|");
$final_name = $triggerObj->getTypeName($triggerObj->getFinalReviewType());


$assessment_status = $triggerObj->getTriggersByParentID($assess_id);
$assessment_status = $assessment_status[$assess_id];
//	ASSIST_HELPER::arrPrint($assessment_status);
$final_is_complete = false;


$layout_js = "";
$options = array('id'=>"display_self",'name'=>"display_self");
$self_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$self_button['js'];
$options = array('id'=>"display_mod",'name'=>"display_mod");
$mod_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$mod_button['js'];
$options = array('id'=>"display_final",'name'=>"display_final");
$final_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$final_button['js'];


?>
	<div id=div_status>
		<h5><?php echo $triggerObj->replaceAllNames("|ASSESSMENT| Status & Display"); ?></h5>
		<table id="tbl_status_display">
			<tr class="b" id="tr_head">
				<td></td>
				<td>Status</td>
				<td class="no-print">Display</td>
			</tr>
			<tr>
				<td class=b><?php echo $self_name; ?>:</td>
				<td><?php
					$type = $triggerObj->getSelfType();
					if(isset($assessment_status[$type]['status'])) {
						echo ASSIST_HELPER::displayIconAsDiv($assessment_status[$type]['status_icon'])." ".$assessment_status[$type]['status'];
					} else {
						echo ASSIST_HELPER::getDisplayIconAsDiv("warn")." Not yet triggered";
					}
					?></td>
				<td><?php echo $self_button['display']; ?></td>
			</tr>
			<tr>
				<td class=b><?php echo $mod_name; ?>:</td>
				<td><?php
					$type = $triggerObj->getModeratorType();
					if(isset($assessment_status[$type]) && count($assessment_status[$type])>0) {
						$status_echo = array();
						if(count($assessment_status[$type])==1) {
							foreach($assessment_status[$type] as $mod_id => $mod) {
								$status_echo[] = ASSIST_HELPER::getDisplayIconAsDiv($mod['status_icon'])." ".$mod['status'];
							}
						} else {
							foreach($assessment_status[$type] as $mod_id => $mod) {
								$status_echo[] = $moderator_name." ".$mod_id.": ".ASSIST_HELPER::getDisplayIconAsDiv($mod['status_icon'])." ".$mod['status'];
							}
						}
						echo implode("<br />",$status_echo);//echo $assessment_status[$type]['status'];
					} else {
						echo ASSIST_HELPER::getDisplayIconAsDiv("warn")." Not yet triggered";
					}
					?></td>
				<td><?php echo $mod_button['display']; ?></td>
			</tr>
			<tr>
				<td class=b><?php echo $final_name; ?>:</td>
				<td><?php
					$type = $triggerObj->getFinalReviewType();
					if(isset($assessment_status[$type]['status'])) {
						$final_is_complete = $assessment_status[$type]['is_complete'];
						echo ASSIST_HELPER::displayIconAsDiv($assessment_status[$type]['status_icon'])." ".$assessment_status[$type]['status'];
					} else {
						echo ASSIST_HELPER::getDisplayIconAsDiv("warn")." Not yet triggered";
					}
					?></td>
				<td><?php echo $final_button['display']; ?></td>
			</tr>
		</table>
	</div>
<?php


$secondary_page_section = !isset($secondary_page_section) ? false : $secondary_page_section;

$object_ids = $assessObj->getRelatedIDs($assess_id,$page_action,$user_ids,$secondary_page_section);
if(!isset($object_ids['scorecard'])) {
	if($page_action==$triggerObj->getReportType()) {
		$assessObj->displayResult(array("error","Assessment not yet completed."));
		$error = true;
	} else {
		$assessObj->displayResult(array("error","An error occurred while getting the scoresheet information for assessment ".$assess_id.".  Please reload the page and try again."));
		exit();
	}
}


?><!--
<div id=div_layout>
	<h5>Display</h5>
	<table>
		<tr>
			<td class=b><?php echo $self_name; ?>:</td>
			<td></td>
		</tr>
		<tr>
			<td class=b><?php echo $mod_name; ?>:</td>
			<td></td>
		</tr>
		<tr>
			<td class=b><?php echo $final_name; ?>:</td>
			<td></td>
		</tr>
	</table>
</div>
-->
<?php



if(!$error) {


$scd_id = $object_ids['scorecard'];
$trigger_id = $object_ids['trigger'];
$period_id = $object_ids['period'];

$scdObj = new PM4_SCORECARD();
$sources = $scdObj->getKPASources();
$scorecard = $scdObj->getAObjectSummary($scd_id);

//ASSIST_HELPER::arrPrint($scorecard);

$bonus_scale_id = $scorecard['bonus_scale_id'];

$sdbp5Obj = new SDBP5B_PMS();
//$kpas = $sdbp5Obj->getKPAs($scdObj->getPrimaryKPASource());
$full_kpas = $sdbp5Obj->getKPAs($sources,true,$scdObj->getPrimaryKPASource());
$kpas = $full_kpas['list'];


$scoreObj = new PM4_SCORE();
$scores = $scoreObj->getScoresForCompletingAssessment($trigger_id);


$echo = array(
	'start'=>$scdObj->getAssessmentScoreHeading($scd_id),
	'end'=>"",
	'form'=>array(
		'start'=>"",
		'end'=>""
	),
	'kpa'=>"",
	'cc'=>"",
	'jal'=>"",
	'bonus'=>"",
	'dashboard'=>"",
	'js'=>"",
);

$echo['js'].="
<script type='text/javascript'>
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.99;
	var ifrHeight = (dlgHeight-50)*0.99;

	var validScore = [\"1\",\"2\",\"3\",\"4\",\"5\"];
	var validDecimal = [\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"0\"];
	var validPoint = ";
	$echo['js'].="\"";
	$echo['js'].=".";
	$echo['js'].="\"";
	$echo['js'].=";";
//above 4 lines done as multiline to avoid PhPStorm giving an error.  Original code: var validPoint = \".\";



if(!isset($onscreen_display) || !is_array($onscreen_display) || count($onscreen_display)==0) {
	$onscreen_display = array();
	switch($page_action) {
		case $triggerObj->getSelfType():
		case $triggerObj->getModeratorType():
		case $triggerObj->getFinalReviewType():
			$onscreen_display = array(
				'form'=>true,
				'kpa'=>true,
				'cc'=>true,
				'jal'=>true,
				'bonus'=>false,
				'dashboard'=>false,
				'js'=>true,
			);
			break;
		case $triggerObj->getReportType():
		case $triggerObj->getViewType():
		default:
			$onscreen_display = array(
				'form'=>false,
				'kpa'=>(isset($display_kpa) ? $display_kpa : true),
				'cc'=>(isset($display_cc) ? $display_cc : true),
				'jal'=>(isset($display_jal) ? $display_jal : true),
				'bonus'=>(isset($display_bonus) ? $display_bonus : true),
				'dashboard'=>(isset($display_dashboard) ? $display_dashboard : true),
				'js'=>true,
			);
			break;
	}
}





if($page_action!=$triggerObj->getSelfType()) {
	$self_scores = $triggerObj->getSelfScoresByAssessmentID($assess_id);
}

if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType()) {
	$mod_scores = $triggerObj->getModeratorScoresByAssessmentID($assess_id);
	$mod_averages = $mod_scores['average'];
	unset($mod_scores['average']);
}
//ASSIST_HELPER::ArrPrint($mod_scores);
unset($head['main']['kpi_natkpaid']);


$time_periods = $assessObj->getTimePeriodsForAssessment($period_id,$assess_id);
$tp = array_keys($time_periods);
$max = $tp[count($tp)-1];
$time_ids = array();
for($t=1;$t<=$max;$t++) { $time_ids[] = $t; }




	$echo['form']['start'].= "
	<form name=frm_assessment>
		<input type=hidden name=assess_id value=\"$assess_id\" />
		<input type=hidden name=trigger_id value=\"$trigger_id\" id=trigger />
		<input type=hidden name=trigger_type value=\"$page_action\" />
		<input type=hidden name=existing_scores value=\"".count($scores)."\" />
		<input type=hidden name=post_action id=post_action value=\"PENDING\" />
	";

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());





















$lineObj = new PM4_LINE();
$lines = $lineObj->getFullLinesByType($scd_id, true, $time_ids);
$weights = $lineObj->getLineWeights($scd_id);
$kpa_weights = $lineObj->getKPAWeightsByType($scd_id);

	$types = array("KPI", "PROJ", "TOP");
	$names = array('KPI' => "KPIs", 'TOP' => "Top Layer KPIs", 'PROJ' => "Projects");

$display_kpa = false;
	foreach($kpas as $k => $a) {
		foreach($types as $kt) {
			if(isset($lines[$k][$kt]) && count($lines[$k][$kt])) {
				$display_kpa = true;
			}
		}
	}

$colspan = 0;
$scoring_span = 2;

if($display_kpa) {

	$echo['kpa'] .= "
<h3>KPAs</h3>
<table id=tbl_kpa class=list>";
	$table_heading = "
	<tr>
		<th rowspan=2>Ref</th>";
	foreach($head['main'] as $fld => $name) {
		$table_heading .= "<th rowspan=2>".$name."</th>";
		$colspan++;
	}
	$table_heading .= "<th rowspan=2>KPA<br />Weight</th>
		<th rowspan=2>Overall<br />Weight</th>";
	$colspan += 2;
	foreach($time_periods as $ti => $tp) {
		/*	$table_heading.="<th colspan=".count($head['results']).">".$tp."</th>";
			$colspan++;*/
	}
	$table_heading .= "<th colspan=".count($head['results']).">Year To $tp</th>";
	$colspan++;

	//if($page_action!=$triggerObj->getSelfType()) {
	$table_heading .= "<th colspan=2 class=self_title>".$self_name."</th>";
	//}
	/*if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
		$table_heading.="<th colspan=".(count($mod_scores)).">Moderators</th><th rowspan=2>Average Moderators Score</th>";
		$scoring_span++;
		$colspan++;
	}
	if($page_action==$triggerObj->getViewType()) {
	*/
	$table_heading .= "<th colspan=2 class=mod_title>".$mod_name."</th>";
	//$scoring_span++;
	//$colspan++;
	//}
	//if($page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType()) {
	$table_heading .= "
			<th colspan=2 class=final_title>".$final_name."</th>
			<th rowspan=2>KPA Weighted Score</th>
			<th rowspan=2>Overall Weighted Score</th>";
	/*} else {
		$table_heading.="
		<th rowspan=2>Score</th>
		<th rowspan=2>Comment</th>";
	}*/
	$table_heading .= "</tr>
	<tr>";
	$colspan += 2;
	/*foreach($time_periods as $ti => $tp) {
		foreach($head['results'] as $fld => $h) {
			$table_heading.="<th >".$h."</th>";
			$colspan++;
		}
	}*/
	foreach($head['results'] as $fld => $h) {
		$table_heading .= "<th >".$h."</th>";
		$colspan++;
		$scoring_span++;
	}
	if($page_action != $triggerObj->getSelfType()) {
		$table_heading .= "<th class=self_title>Score</th><th class=self_title>Comment</th>";
		$table_heading .= "<th class=mod_title>Score</th><th class=mod_title>Comment</th>";
		$colspan += 2;
		$colspan += 2;
		$scoring_span += 2;
		$scoring_span += 2;
	}
	/*if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
		foreach($mod_scores as $mt){
			$table_heading.="<th>Moderator ".$mt['id']."</th>";
			$colspan++;
			$scoring_span++;
		}
	}*/
	//if($page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType()) {
	$table_heading .= "<th class=final_title>Score</th><th class=final_title>Comment</th>";
	$colspan += 2;
	$scoring_span += 2;
	//}
	$table_heading .= "
	</tr>
	<tbody>
	";

	$grand_tot = 0;

	$total_score = array();

	$default_score = $triggerObj->getDefaultScore();

	foreach($kpas as $k => $a) {
		foreach($types as $kt) {
			$tot = 0;
			$kpa_score = 0;
			$kpa_o_score = 0;
			if(isset($lines[$k][$kt]) && count($lines[$k][$kt])) {
				$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
				$echo['kpa'] .= $table_heading."<tr class=subth><td colspan=".$colspan.">".$a." - ".$names[$kt]."</td></tr>";

				$objects = $lines[$k][$kt]; //ASSIST_HELPER::arrPrint($objects);
				foreach($objects as $i => $obj) {
					$is_scored = ($obj['results']['YTD']['r'] > 0) && $obj['active'] == 1;
					$is_deleted = $obj['active'] != 1;
					$tr_class = "";
					$note = "";
					if($is_deleted) {
						$tr_class = "inactive";
						$note = "Deleted at source.";
					}
					elseif(!$is_scored) {
						$tr_class = "disabled";
						$note = "Not applicable for this period.";
					}
					$echo['kpa'] .= "
			<tr class='".$tr_class."'>
				<td class='b center'><span class=spn_ref>".$obj['ref']."</span><br />[Line: ".$lineObj->getRefTag().$i."]<br /><button style='margin-top: 5px' class=btn_view obj_id=".$i.">View</button></td>";
					foreach($head['main'] as $fld => $name) {
						$str = $obj[$fld];
						if(strlen($str) > 100) {
							$str = "<span id=spn_".$fld.$i."_full title='".$str."'>".substr(ASSIST_HELPER::decode($str), 0, 100)."...</span><span class='expander orange' id=spn_".$fld.$i." action=open>&nbsp;[+]</span>";
						}
						$echo['kpa'] .= "<td>".$str."</td>";
					}
					$w = (isset($weights[$i]) ? $weights[$i] : 1);
					$kpaw = round(($w / $kw) * 100, $weighted_score_rounding_precision);
					$echo['kpa'] .= "
				<td id=td_".$k.$kt."_".$i." class=right>".$kpaw."%</td>
				<td class=right><div style='width:60px'>".$w."%</div></td>";
					$ti = "YTD";
					foreach($head['results'] as $fld => $h) {
						if($fld == "kr_result") {
							$echo['kpa'] .= "<td ><div style='text-align: center; padding: 3px; background-color: ".$obj['results'][$ti]['color']."; color: #FFFFFF'>".$obj['results'][$ti]['text']."</div></td>";
						}
						else {
							$echo['kpa'] .= "<td >".$obj['results'][$ti][$fld]."</td>";
						}
					}
					//if($page_action!=$triggerObj->getSelfType()) {
					if(!$is_scored) {
						$c = 0;
						$c = 2;// + 1;
						/*if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
							$c = 2+count($mod_scores) + 1;
						} elseif($page_action==$triggerObj->getViewType()) {
							$c = 2+1;
						} else {
							$c = 2+($page_action==$triggerObj->getModeratorType() ? 2 : 0);
						}*/
						$echo['kpa'] .= "<td class=center colspan=".$c.">";
						$echo['kpa'] .= $note;
						$echo['kpa'] .= "</td>";
					}
					else {
						$echo['kpa'] .= "
				<td class='center self_col'>".(isset($self_scores[$i]) && $self_scores[$i]['score'] > 0 ? $self_scores[$i]['score'] : "N/A")."</td>
				<td class=self_col>".(isset($self_scores[$i]) ? $self_scores[$i]['comment'] : "N/A")."</td>";
					}
					//}
					//if($is_scored && ($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType())) {
//			if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
					$moderator_comments = array();
					foreach($mod_scores as $mt) {
						//			$echo['kpa'].= "<td class=center>".(isset($mt['scores'][$i]) && ($mt['scores'][$i]['score'])>0 ? $mt['scores'][$i]['score'] : "N/A")."</td>";
						if(isset($mt['scores'][$i]['comment']) && strlen($mt['scores'][$i]['comment']) > 0) {
							$moderator_comments[] = $mt['scores'][$i]['comment'];
						}
					}
					if(count($moderator_comments) > 0) {
						shuffle($moderator_comments);
						$mod_comm = "<ul><li>".implode("</li><li>", $moderator_comments)."</li></ul>";
					}
					else {
						$mod_comm = "";
					}

//			}
//display average mod score + comments
					$echo['kpa'] .= "
			<td class='center mod_col'>".(isset($mod_averages[$i]) ? round($mod_averages[$i], $weighted_score_rounding_precision) : "N/A")."</td>
			<td class='mod_col'>".$mod_comm."</td>";
					//}

					if($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
						if(!$is_scored) {
							$scores[$i] = array('score'   => $default_score,
												'comment' => "Default value applied.",);
						}
						$k_score = (isset($scores[$i]) ? round(($scores[$i]['score'] * $kpaw / 100), $weighted_score_rounding_precision) : 0);
						$kpa_score += $k_score;
						$o_score = (isset($scores[$i]) ? round(($scores[$i]['score'] * $w / 100), $weighted_score_rounding_precision) : 0);
						//$total_score+=$o_score;
						$kpa_o_score += $o_score;
						$echo['kpa'] .= "
				<td class='center final_col'>".(isset($scores[$i]) ? $scores[$i]['score'] : "")."</td>
				<td class='final_col'>".(isset($scores[$i]) ? $scores[$i]['comment'] : "")."</td>
				<td class=center>".$k_score."</td>
				<td class=center>".$o_score."</td>
			";
					}
					else {
						if($is_scored) {
							$echo['kpa'] .= "
					<td class=right><input type=text name=kpa_score[$i] class=txt_score size=5 value='".(isset($scores[$i]) ? $scores[$i]['score'] : "")."' /> <input type=hidden name=score_id[$i] value='".(isset($scores[$i]) ? $scores[$i]['id'] : 0)."' /></td>
					<td class=right><textarea rows=3 cols=40 class=txt_comment name=kpa_comment[$i]>".(isset($scores[$i]) ? $scores[$i]['comment'] : "")."</textarea></td>
				";
						}
						elseif($page_action == $triggerObj->getFinalReviewType()) {
							$echo['kpa'] .= "
					<td class=right>".$default_score."</td>
					<td class=right>Default value applied.</td>
				";
						}
						elseif($page_action == $triggerObj->getSelfType()) {
							$echo['kpa'] .= "<td class=center colspan=2>".$note."</td>";
						}
					}
					$echo['kpa'] .= "</tr>";
					$tot += (isset($weights[$i]) ? $weights[$i] : 1);
				}//end foreach
				$total_score[$kt][$k] = $kpa_o_score;
				//$total_score[$a." - ".$names[$kt]] = $kpa_o_score;
				$echo['kpa'] .= "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main']) + 1).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class=right>100%</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".$tot."%</th>";
				if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
					$scor_span = $scoring_span - 2;
					$echo['kpa'] .= "
				<th class=right colspan=".($scor_span).">Total Score:</th>
				<th class=center>".$kpa_score."</th>
				<th class=center>".$kpa_o_score."</th>
				";
				}
				else {
					$echo['kpa'] .= "
				<th class=right colspan=".($scoring_span + count($head['results']) * count($time_periods))."></th>";
				}
				$echo['kpa'] .= "
		</tr>";
			}
			$grand_tot += $tot;
		}
	}

	$t_score = 0;
	foreach($total_score as $ky => $ts) {
		$t_score += array_sum($ts);
	}

	$echo['kpa'] .= "
	</tbody>
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=".(1 + count($head['main'])).">Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot>".$grand_tot."%</th>";
	if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
		$scor_span = $scoring_span - 1;
		$echo['kpa'] .= "
			<th class=right colspan='".($scor_span)."'>Total Score:</th>
			<th class=center>".($t_score)."</th>";
	}
	else {
		$echo['kpa'] .= "<th class=right colspan='".($scoring_span + count($head['results']) * count($time_periods))."'></th>";
	}
	$echo['kpa'] .= "
		</tr>
	</tfoot>
</table>";


} else {
	//no kpa lines to display
}









$sources = $scdObj->getJALSources();


$primary_source = $scdObj->getPrimaryJALSource();

	$jalObj = new JAL1_PMS();
	$functions = $jalObj->getKPAs($primary_source);
	$head = $jalObj->getHeadings($primary_source,true,false);
	unset($head['main']['activity_kpi_link']); //$jalObj->arrPrint($head['main']);
	unset($head['main']['activity_subfunction_id']);
$create_names = "Activities";
$create_name = "Activity";
$create_type = "JAL";
$lines = $lineObj->getFullLinesForJAL($scd_id);
//$scdObj->arrPrint($lines);
$weights = $lineObj->getLineWeights($scd_id);

$c=0;
$types = array("JAL",);
$display_jal = false;
if(count($lines)>0 && count($functions)>0) {
	foreach($functions as $k => $a) {
			if(isset($lines[$k]) && count($lines[$k])>0) {
				$objects = $lines[$k];
				$display_jal = true;
				foreach($objects as $i => $obj) {
					$c++;
				}
			}
	}
}
if($c>0) {
	$blank = false;
	$default_weight = round(100/$c,$weighted_score_rounding_precision);
} else {
	$blank = true;
	$default_weight = 0;
}
$names = array('JAL'=>"Activities");


$total_jal_score = array();

if($display_jal) {
$echo['jal'].= "
<h3>Function Activities</h3>
<table id=tbl_jal class=list>
";

if($page_action==$triggerObj->getModeratorType() || $page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType()) {
	$rowspan=2;
} else {
	$rowspan=1;
}


$grand_tot = 0;
$visible=false;
$kt = "JAL";
foreach($functions as $k => $a) {
	$tot = 0;
	$kpa_score = 0;
	$kpa_o_score = 0;

	if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
		$scoring_span = 2;
		$colspan=0;
		$objects = $lines[$k][$kt];
		$visible = true;
		$kw = isset($kpa_weights[$k][$kt]) && !($kpa_weights[$k][$kt]<0) ? $kpa_weights[$k][$kt] : count($objects)*$default_weight;
$echo['jal'].="
		<tr>";
			foreach($head['main'] as $fld=>$name) {
				$class="";
				if($page_action!==$triggerObj->getReportType() || !(strpos($fld,"baseline")>0 || strpos($fld,"frequency")>0)) {
					$echo['jal'].="<th rowspan=2 $class >".$name."</th>";
				$colspan++;
			}
			}
$echo['jal'].="
			<th rowspan=2>Function<br />Weight</th>
			<th rowspan=2>Overall<br />Weight</th>";
	$colspan+=2;
		//if($page_action!=$triggerObj->getSelfType()) {
			$echo['jal'].= "<th colspan=2 class=self_title>".$self_name."</th>";
			$colspan+=2;
			$scoring_span+=2;
		//}
		/*if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
			$echo['jal'].= "<th colspan=".(count($mod_scores)).">Moderators</th><th rowspan=2>Average<br />Moderators<br />Score</th>";
			$colspan++;
			$scoring_span++;
		} elseif($page_action==$triggerObj->getViewType()) {
		*/	$echo['jal'].= "<th colspan=2 class=mod_title>".$mod_name."</th>";
			$colspan+=2;
			$scoring_span+=2;
		//}
		/*if($page_action==$triggerObj->getFinalReviewType()) {
			$echo['jal'].= "
			<th colspan=2>Final Review</th>
			";
		} elseif($page_action==$triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
		*/	$echo['jal'].= "
			<th colspan=2 class=final_title>".$final_name."</th>
			<th rowspan=2>Function Weighted Score</th>
			<th rowspan=2>Overall Weighted Score</th>
			";
			$scoring_span+=2;
		/*} else {
			$echo['jal'].= "
			<th rowspan=".$rowspan.">Score</th>
			<th rowspan=".$rowspan.">Comment</th>
			";
		}
*/
$echo['jal'].="
		</tr>";

	if($page_action!=$triggerObj->getSelfType()) {
		$echo['jal'].= "
		<tr>
			<th class=self_title>Score</th>
			<th class=self_title>Comment</th>";
		$echo['jal'].= "
			<th class=mod_title>Score</th>
			<th class=mod_title>Comment</th>";
		/*if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
			foreach($mod_scores as $mt){
				$echo['jal'].= "<th>Moderator ".$mt['id']."</th>";
				$colspan++;
			}
		}
		 *
		 */
		if($page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType()) {
			$echo['jal'].= "
			<th class=final_title>Score</th>
			<th class=final_title>Comment</th>";
		}
				$echo['jal'].= "
		</tr>";
	}
	$total_cc_score = 0;

		$echo['jal'].="<tr class=subth><td colspan=".($colspan+$scoring_span).">".$a."</td></tr>";
		foreach($objects as $i => $obj) {
			$is_scored = true;
			if($obj['active']==1 && $obj['activity_kpi_link']=="Yes") { $obj['active']=2; $is_scored = false; $note = "Scored in SDBIP"; }
			if($obj['active']==0) { $is_scored = false; $note ="Deleted at source";}
$echo['jal'].="
			<tr class=".($obj['active']!=1 ? "inactive" : "").">
			";
				foreach($head['main'] as $fld=>$name) {
					$class="";
					if($page_action!==$triggerObj->getReportType() || !(strpos($fld,"baseline")>0 || strpos($fld,"frequency")>0)) {
					$echo['jal'].="
						<td $class >
						".$obj[$fld].(
						$fld=="ref" && $obj['active']==0 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
						)."
						</td>";
				}
				}
				$w = (isset($weights[$i]) && !($weights[$i]<0) ? $weights[$i] : $default_weight);
				$tot+=$w;
			$kpaw = ($kw>0 ? round(($w/$kw)*100,$weighted_score_rounding_precision):0);
			$echo['jal'].="
				<td id=td_".$k.$kt."_".$i." class='jal_line_perc right'>".(count($objects)>1 ? $kpaw : "100.00")."%</td>
				<td class=right>".$w."%</td>";
			if($page_action!=$triggerObj->getSelfType()) {
				if(!$is_scored) {
					$c = 0;
					if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
						$c = 2+count($mod_scores) + 1;
					} elseif($page_action==$triggerObj->getViewType()) {
						$c = 2+1;
					} else {
						$c = 2+($page_action==$triggerObj->getModeratorType() ? 2 : 0);
					}
					$echo['jal'].= "<td class=center colspan=".$c.">";
						$echo['jal'].= $note;
					$echo['jal'].= "</td>";
				} else {
					$echo['jal'].= "
						<td class='center self_col'>".(isset($self_scores[$i]) && $self_scores[$i]['score']>0 ? $self_scores[$i]['score'] : "N/A")."</td>
						<td class=self_col>".(isset($self_scores[$i]) ? $self_scores[$i]['comment'] : "N/A")."</td>";
				}
			}
			if($is_scored && ($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType())) {
				if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
					foreach($mod_scores as $mt){
						//$echo['jal'].= "<td class=center>".(isset($mt['scores'][$i]) && ($mt['scores'][$i]['score'])>0 ? $mt['scores'][$i]['score'] : "N/A")."</td>";
					}
				}
				$echo['jal'].= "<td class='center mod_col'>".(isset($mod_averages[$i]) ? round($mod_averages[$i],$weighted_score_rounding_precision) : "N/A")."</td>";
				$moderator_comments = array();
				foreach($mod_scores as $mt){
					//			$echo['kpa'].= "<td class=center>".(isset($mt['scores'][$i]) && ($mt['scores'][$i]['score'])>0 ? $mt['scores'][$i]['score'] : "N/A")."</td>";
					if(isset($mt['scores'][$i]['comment']) && strlen($mt['scores'][$i]['comment'])>0) {
						$moderator_comments[] = $mt['scores'][$i]['comment'];
					}
				}
				if(count($moderator_comments)>0) {
					shuffle($moderator_comments);
					$mod_comm = "<ul><li>".implode("</li><li>",$moderator_comments)."</li></ul>";
				} else {
					$mod_comm = "";
				}

//			}
//display average mod score + comments
				$echo['jal'].= "
			<td class='mod_col'>".$mod_comm."</td>";
				//}

			}

			if($page_action==$triggerObj->getReportType() || $page_action==$triggerObj->getViewType()) {
				if(!$is_scored) {
					$scores[$i] = array(
						'score'=>$default_score,
						'comment'=>"Default value applied.",
					);
				}
				$k_score = (isset($scores[$i]) ? round(($scores[$i]['score']*$kpaw/100),$weighted_score_rounding_precision) : 0);
				$kpa_score+=$k_score;
				$o_score = (isset($scores[$i]) ? round(($scores[$i]['score']*$w/100),$weighted_score_rounding_precision) : 0);
				//$total_score+=$o_score;
				$kpa_o_score+=$o_score;
				$echo['jal'].= "
					<td class='center final_col'>".(isset($scores[$i]) ? $scores[$i]['score'] : "")."</td>
					<td class='final_col'>".(isset($scores[$i]) ? $scores[$i]['comment'] : "")."</td>
					<td class=center>".$k_score."</td>
					<td class=center>".$o_score."</td>
				";
			} else {
				if($is_scored) {
					$echo['jal'].= "
						<td class=right><input type=text name=jal_score[$i] class=txt_score size=5 value='".(isset($scores[$i]) ? $scores[$i]['score'] : "")."' /> <input type=hidden name=score_id[$i] value='".(isset($scores[$i]) ? $scores[$i]['id'] : 0)."' /></td>
						<td class=right><textarea rows=3 cols=40 class=txt_comment name=jal_comment[$i]>".(isset($scores[$i]) ? $scores[$i]['comment'] : "")."</textarea></td>
					";
				} elseif($page_action==$triggerObj->getFinalReviewType()) {
					$echo['jal'].= "
						<td class=right>".$default_score."</td>
						<td class=right>Default value applied.</td>
					";
				} elseif($page_action==$triggerObj->getSelfType()) {
					$echo['jal'].= "<td class=center colspan=2>".$note."</td>";
				}
			}
			$echo['jal'].= "</tr>";
		}
		$total_jal_score[$names[$kt]][$k] = $kpa_o_score;
		$echo['jal'].= "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])-($page_action==$triggerObj->getReportType()?2:0)).">Total ".$a." Weight:</th>
			<th class=right>100%</th>
			<th class='right kpa_tot'>".$tot."%</th>";
		if(in_array($page_action,array($triggerObj->getReportType(),$triggerObj->getViewType()))) {
			$scor_span=$scoring_span-2;
			$echo['jal'].= "
				<th class=right colspan=".($scor_span).">Total Score:</th>
				<th class=center>".$kpa_score."</th>
				<th class=center>".$kpa_o_score."</th>
				";
		} else {
			$echo['jal'].= "
				<th class=right colspan=".($scoring_span)."></th>";
		}
		$echo['jal'].= "
		</tr>";
		$grand_tot+=$tot;
	}
}


$echo['jal'].="
	<tfoot>
		<tr class=gtotal>
			<th class='right b' colspan=".(count($head['main'])-($page_action==$triggerObj->getReportType()?2:0)).">Total Weight:</th>
			<th class=right>-</th>
			<th class=right id=th_gtot>".$grand_tot."%</th>";
		if(in_array($page_action,array($triggerObj->getReportType(),$triggerObj->getViewType()))) {
			$scor_span = $scoring_span-1;
			$echo['jal'].= "
			<th class=right colspan='".($scor_span)."'>Total Score:</th>
			<th class=center>".array_sum($total_jal_score[$names[$kt]])."</th>";
		} else {
			$echo['jal'].= "<th class=right colspan='".($scoring_span)."'></th>";
		}
	$echo['jal'].= "
		</tr>
	</tfoot>

</table>
";
} else {	//if display_jal==true
}























	$total_cc_score = 0;
//$listObj = new PM4_LIST("competencies");
//$objects = $listObj->getListItems();
	$listObj = new PM4_LIST("competency_category");
	$category_objects = $listObj->getActiveListItemsFormattedForSelect();
	$listObj->changeListType("competencies");
	$competency_objects = $listObj->getListItemsGroupedByParent($listObj->getParentField());

	$lines = $lineObj->getLineSrcIDs($scd_id, $lineObj->getModRef(),"CC");

$ccsObj = new PM4_CC_SCORE();
$ccs_progress = $ccsObj->getProgressOfScoring($trigger_id);
$display_cc = false;
foreach($category_objects as $cate_id => $cate) {
	$objects = isset($competency_objects[$cate_id]) ? $competency_objects[$cate_id] : array();
	if(count($objects)>0) {
		foreach($objects as $i => $obj) {
			if(isset($lines[$i])) {
				$display_cc = true;
				break;
			}
		}
	}
}

//ASSIST_HELPER::arrPrint($scores);
if($display_cc) {
	$echo['cc'] .= "
<h3>Core Competencies</h3>
";

	if($page_action == $triggerObj->getModeratorType() || $page_action == $triggerObj->getFinalReviewType() || $page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
		$rowspan = 2;
	}
	else {
		$rowspan = 1;
	}
	$echo['cc'] .= "
<table class=list id=tbl_cc>";
	$colspan = 3;
	$cate_colspan = 6;
	$echo['cc'] .= "
	<tr>
		<th rowspan=".$rowspan.">Ref</th>
		<th rowspan=".$rowspan.">Core Competency</th>
		<th rowspan=".$rowspan.">Description</th>
		<th rowspan=".$rowspan.">Weight</th>";
	//if($page_action!=$triggerObj->getSelfType()) {
	$echo['cc'] .= "<th colspan=2 class=self_title>".$self_name."</th>";
	$colspan += 2;
	$cate_colspan += 2;
	$echo['cc'] .= "<th colspan=2 class=mod_title>".$mod_name."</th>";
	$colspan += 2;
	$cate_colspan += 2;
	/*}
	if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
		$echo['cc'].= "<th colspan=".(count($mod_scores)).">Moderators</th><th rowspan=2>Average<br />Moderators<br />Score</th>";
		$colspan++;
	} elseif($page_action==$triggerObj->getViewType()) {
		$echo['cc'].= "<th rowspan=2>Average<br />Moderators<br />Score</th>";
		$colspan++;
	}
	if($page_action==$triggerObj->getFinalReviewType()) {
		$echo['cc'].= "
		<th colspan=2>Final Review</th>
		";
	} elseif($page_action==$triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
	*/
	$echo['cc'] .= "
			<th colspan=2 class=final_title>".$final_name."</th>
			<th rowspan=2>Weighted Score</th>
			";
	$cate_colspan++;
	/*} else {
		$echo['cc'].= "
		<th rowspan=".$rowspan.">Score</th>
		<th rowspan=".$rowspan.">Comment</th>
		";
	}*/
	$echo['cc'] .= "
	</tr>";
	if($page_action != $triggerObj->getSelfType()) {
		$echo['cc'] .= "
		<tr>
			<th class=self_title>Score</th>
			<th class=self_title>Comment</th>
			<th class=mod_title>Score</th>
			<th class=mod_title>Comment</th>
			";
		/*if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
			foreach($mod_scores as $mt){
				$echo['cc'].= "<th>Moderator ".$mt['id']."</th>";
				$colspan++;
			}
		}*/
		if($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
			$echo['cc'] .= "
			<th class=final_title>Score</th>
			<th class=final_title>Comment</th>";
		}
		$echo['cc'] .= "
		</tr>";
	}
	$tot = 0;
	foreach($category_objects as $cate_id => $cate) {
		$objects = $competency_objects[$cate_id];
		if(count($objects) > 0) {
			$echo['cc'] .= "<tr class=subth><td colspan='".$cate_colspan."'>$cate</td></tr>";
			foreach($objects as $i => $obj) {
				if(isset($lines[$i])) {
					$li = $lines[$i];
					$w = isset($weights[$li]) ? $weights[$li] : 1;
					$tot += $w;
					$echo['cc'] .= "
			<tr cc_id=".$i." line_id=".$li.">
				<td>".$lineObj->getCCRefTag().$obj['id']." [Line: ".$lineObj->getRefTag().$li."]</td>
				<td>".$obj['name']."</td>
				<td>".$obj['description']."</td>
				<td class=right>".$w."%</td>";
					if($page_action != $triggerObj->getSelfType()) {
						$echo['cc'] .= "
						<td class='center self_col'>".(isset($self_scores[$li]) && $self_scores[$li]['score'] > 0 ? $self_scores[$li]['score'] : "N/A")."</td>
						<td class=self_col>".(isset($self_scores[$li]) ? $self_scores[$li]['comment'] : "N/A")."</td>";
						//if($page_action==$triggerObj->getFinalReviewType() || $page_action==$triggerObj->getReportType()) {
						$moderator_comments = array();
						foreach($mod_scores as $mt) {
							//$echo['cc'].= "<td class=center>".(isset($mt['scores'][$li]) && ($mt['scores'][$li]['score'])>0 ? $mt['scores'][$li]['score'] : "N/A")."</td>";
							if(isset($mt['scores'][$i]['comment']) && strlen($mt['scores'][$i]['comment']) > 0) {
								$moderator_comments[] = $mt['scores'][$i]['comment'];
							}
						}
						if(count($moderator_comments) > 0) {
							shuffle($moderator_comments);
							$mod_comm = "<ul><li>".implode("</li><li>", $moderator_comments)."</li></ul>";
						}
						else {
							$mod_comm = "";
						}
						$echo['cc'] .= "
								<td class='center mod_col'>".(isset($mod_averages[$li]) ? round($mod_averages[$li], $weighted_score_rounding_precision) : "N/A")."</td>
								<td class=mod_col>".$mod_comm."</td>";
						/*} elseif($page_action==$triggerObj->getViewType()) {
							$echo['cc'].= "<td class=center>".(isset($mod_averages[$li]) ? round($mod_averages[$li],2) : "N/A")."</td>";
						}*/
					}
					if($page_action == $triggerObj->getReportType() || $page_action == $triggerObj->getViewType()) {
						if(isset($scores[$li]['score'])) {
							$o_score = round(($scores[$li]['score'] * $w / 100), $weighted_score_rounding_precision);
						}
						else {
							$o_score = 0;
						}

						$total_cc_score += $o_score;
						$echo['cc'] .= "
						<td class='center final_col'>".(isset($scores[$li]) ? $scores[$li]['score'] : "")."</td>
						<td class=final_col>".(isset($scores[$li]) ? $scores[$li]['comment'] : "")."</td>
						<td class=center>".(isset($scores[$li]) ? $o_score : "")."</td>
					</tr>";
					}
					else {
						if(isset($scores[$li]) && $scores[$li]['score'] > 0) {
							$s = $scores[$li]['score'];
						}
						elseif(isset($ccs_progress[$li]) && count($ccs_progress[$li]) > 0) {
							$s = "TBC";
						}
						else {
							$s = "";
						}
						$echo['cc'] .= "
						<td class=center>
							<input type=text disabled=disabled class=display_score id=display_cc_".$i." name=display_cc_score[$li] size=5 value='$s' />
							<br /><button class=btn_cc_score style='margin-top:3px'>Score</button>
							<input type=hidden name=cc_score[$li] id=cc_score_".$i." size=5 value='".(isset($scores[$li]) ? $scores[$li]['score'] : "")."' />
							<input type=hidden name=score_id[$li] value='".(isset($scores[$li]) ? $scores[$li]['id'] : "")."' />
						</td>
						<td><textarea rows=3 cols=40 name=cc_comment[$li]>".(isset($scores[$li]) ? $scores[$li]['comment'] : "")."</textarea></td>
					</tr>";
					}
				}
			}
		}
	}
	$echo['cc'] .= "
	<tr class=total>
		<td colspan=2></td>
		<td class=right>Total weight:</td>
		<td class=right id=td_tot>".$tot."%</td>";
	if(in_array($page_action, array($triggerObj->getReportType(), $triggerObj->getViewType()))) {
		$echo['cc'] .= "<td colspan='".($colspan - 1)."' class=right>Total Score:</td><td class=center>".$total_cc_score."</td>";
	}
	else {
		$echo['cc'] .= "<td colspan='$colspan'></td>";
	}
	$echo['cc'] .= "
	</tr>
</table>
<input type=hidden id=cc_score_saved value='na' />
<div id=dlg_cc_scoring>
	<iframe id=ifr_cc_scoring></iframe>
</div>

";

} else {
	//No CC's found to display
}

























if($page_action!=$triggerObj->getReportType() && $page_action!=$triggerObj->getViewType()) {
	$echo['form']['end'].= "<p class=center><button id=btn_save_pending>Save & Continue Later</button>&nbsp;&nbsp;&nbsp;<button id=btn_save_submit>Save & Finalise</button></p>";
} elseif($final_is_complete!==true){
	$echo['dashboard'].= "<p>&nbsp;</p>".ASSIST_HELPER::getDisplayResult(array("info","Please note that the Performance Dashboard with the final scores and Bonus calculation will only be available once the $final_name has been completed."));
} else {
	$bonusperc = $assessObj->getBonusRatingTable($bonus_scale_id);
	$echo['bonus'].= "
	<div class=float>
		".$bonusperc."
	</div>";

	$echo['dashboard'].="
	<div>
	<h3>Performance Dashboard</h3>
	";
		$component = $displayObject->getComponentWeightingForScoring($scd_id,$scorecard,
			isset($total_score)?$total_score:array(),
			isset($total_jal_score[$names['JAL']]) ? $total_jal_score[$names['JAL']] : array(),
			isset($total_cc_score)?$total_cc_score:array());
	$echo['dashboard'].=$component['display'];
	$echo['js'].=$component['js'];

	$echo['dashboard'].="
	</div>
			";
}





$echo['form']['end'].= "

	</form>

	";
$echo['end'].= "



	<div id=dlg_view title='View'>
		<iframe id=ifr_view style='border: 0px solid #cc0001;padding:0px;margin:0px;'>

		</iframe>
</div>
";






//ASSIST_HELPER::arrPrint($_REQUEST);


	$echo['js'].= "
	$('#ifr_view').css({'width':ifrWidth+'px','height':ifrHeight+'px'});
	$('#dlg_view').dialog({
		modal: true,
		autoOpen: false,
		width: dlgWidth,
		height: dlgHeight,
		close: function() {
			var r = AssistHelper.doAjax('inc_iframe_session_fix.php','action=FIX');
			$('#ifr_view').prop('src','').html('');
		}
	});

	//CC scoring
	$('#ifr_cc_scoring').css({'width':'100%','height':ifrHeight+'px','border':'1px solid #ffffff'});
	$('#dlg_cc_scoring').dialog({
		modal: true,
		autoOpen: false,
		width: dlgWidth,
		height: dlgHeight,
		beforeClose: function() {
			if($('#cc_score_saved').val()=='na') {
				if(confirm('Are you sure you wish to close? Any changes since your last save will be lost!')==true) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		},
		close: function() {
			$('#ifr_cc_scoring').prop('src','').html('');
			// un-lock scroll position
			var html = jQuery('html');
			//var scrollPosition = html.data('scroll-position');
			html.css('overflow', html.data('previous-overflow'));
			//window.scrollTo(scrollPosition[0], scrollPosition[1])
		}
	});

	$('.btn_cc_score').button().click(function(e) {
		e.preventDefault();
		var title = $(this).parent().parent().find('td:eq(1)').html()+' Scoring';
		var cc_id = $(this).parent().parent().attr('cc_id');
		var line_id = $(this).parent().parent().attr('line_id');
		var trigger_id = $('#trigger').val();
		var dta = 'cc_id='+cc_id+'&line='+line_id+'&trigger='+trigger_id;
		$('#dlg_cc_scoring').dialog({'title':title});
		$('#ifr_cc_scoring').prop('src','manage_cc_scoresheet.php?'+dta);
		$('#cc_score_saved').val('na');

		var scrollPosition = [
		  self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
		  self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
		];
		var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
		html.data('scroll-position', scrollPosition);
		html.data('previous-overflow', html.css('overflow'));
		html.css('overflow', 'hidden');
		//window.scrollTo(scrollPosition[0], scrollPosition[1]);




		$('#dlg_cc_scoring').dialog('open');
	}).css({'font-size':'75%'});



	$('table.noborder, table.noborder td').css('border','0px');




	$('#div_status').addClass('float').css({'position':'relative','bottom':'50px','border':'1px solid #fe9900','border-radius':'5px','padding':'5px'}).find('h5').addClass('orange').css('margin-bottom','10px');
	$('#div_status').find('table').css('margin-right','10px');






	$('.txt_score').blur(function() {
		var r = validateScore('check',$(this));
		if(r[0]) {
			alert(r[1]);
		}
	});
	function validateScore(action,$"."obj) {
		$"."obj.removeClass('required');
		var val = $"."obj.val();
		var err = false;
		var errMsg = '';
		//if len = 1
		if(val.length==1) {
			//validate only 1, 2, 3, 4, 5
			//allow 0 if still pending
			if($.inArray(val,validScore)<0 || (val==0 && $('#post_action').val()=='SUBMIT')) {
				err = true;
				errMsg = 'Invalid score';
			}
		//else if len = 0
		} else if(val.length==0) {
			//required field
			if($('#post_action').val()!='PENDING') {
				err = true;
				errMsg = 'All scores are required';
			}
		//else if len = 2 || > 4
		} else if(val.length==2 || val.length > 4) {
			//error - invalid number
			err = true;
			err = true;
			errMsg = 'Invalid number format.  Only 1 - 5 with 2 decimal places are permitted.';
			errMsg = 'Invalid number format.  Only 1 - 5 with 2 decimal places are permitted.';
		//else
		} else {
			if($.inArray(x,validScore)==4 && (val.charAt(2)!='0' && val.charAt(3)!='0')) {
				err = true;
				errMsg = 'Invalid score.  Maximum score permitted is '+validScore[4]+'.00.';
			} else {
				var x = '';
				var start = '';
				for(i=0;i<val.length;i++) {
					x = val.charAt(i);
					switch(i) {
						case 0:
							//validate that 1st char is 1, 2, 3, 4, 5
							start = x;
							if($.inArray(x,validScore)<0) {
								err = true;
								errMsg = 'Invalid score.  Only 1 - 5 with 2 decimal places are permitted.';
							}
							break;
						case 1:
							//validate that 2nd char is '.'
							if(x!=validPoint) {
								err = true;
								errMsg = 'Invalid decimal point marker (2nd character).';
							}
							break;
						case 2:
						case 3:
							//validate that 3rd char is 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
							//validate that 4th char, if present, is 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
							if($.inArray(x,validDecimal)<0) {
								err = true;
								errMsg = 'Invalid score.  Only 1 - 5 with 2 decimal places are permitted.';
							}
							break;
					}
					if(err) {
						break;
					}
				}
			}
		}
		if(err) {
			$"."obj.addClass('required');
		}
		return [err,errMsg];
	}



	$('.btn_view').button({
		icons: {primary: 'ui-icon-newwin'},
	}).click(function(e) {
		e.preventDefault();
		var obj_id = $(this).attr('obj_id');
		var ref = $(this).parent().find('span.spn_ref').html();
		//console.log(':'+obj_id+':');
		//console.log(':'+ref+':');
		$('#dlg_view').dialog('option','title','View: '+ref);
		$('#ifr_view').prop('src','view_line.php?width='+ifrWidth+'&height='+ifrHeight+'&obj_id='+obj_id);
		$('#dlg_view').dialog('open');
	}).children('.ui-button-text').css({'padding-top':'4px','padding-bottom':'4px','padding-left':'25px','font-size':'80%'
	});



	$('#btn_save_submit').button({
		icons: {primary: 'ui-icon-disk', secondary: 'ui-icon-check'},
	}).click(function(e) {
		e.preventDefault();
		if(confirm('This will mark the Assessment as complete and you will no longer be able to make any changes.  Are you sure you wish to continue?')==true) {
			$('#post_action').val('SUBMIT');
			processForm('SUBMIT');
		}
	}).removeClass('ui-state-default').addClass('ui-button-state-ok').css({'color':'#009900','border':'1px solid #009900'
	});

	$('#btn_save_pending').button({
		icons: {primary: 'ui-icon-disk'},
	}).click(function(e) {
		e.preventDefault();
		$('#post_action').val('PENDING');
		processForm('PENDING');
	}).removeClass('ui-state-default').addClass('ui-button-state-info').css({'color':'#fe9900','border':'1px solid #fe9900'
	});

	function processForm(post_action) {
		AssistHelper.processing();
		var err = false;
		var errs = [];
		var errMsg = '';
		if(post_action=='SUBMIT') {
			$('.txt_score').each(function() {
				var r = validateScore('form',$(this));
				if(r[0]==true) {
					err = true;
				}
			});
		}
		if(err) {
			errMsg = 'There are invalid scores entered. Only 1 - 5 with 2 decimal places are permitted. Please review the scores highlighted.';
			AssistHelper.finishedProcessing('error',errMsg);
		} else {
			var dta = AssistForm.serialize($('form[name=frm_assessment]'));
			console.log(dta);
			var result = AssistHelper.doAjax('inc_controller_assessment.php?action=Trigger.saveScore',dta);
			if(result[0]=='ok') {
				var url = 'manage_".$page_section.".php?r[]=ok&r[]='+result[1];
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}



	$('.expander').css('cursor','pointer').click(function() {
		var i = $(this).prop('id');
		var a = $(this).attr('action');

		var t = $('#'+i+'_full').html();
		var r = $('#'+i+'_full').prop('title');

		$('#'+i+'_full').html(r);
		$('#'+i+'_full').prop('title',t);

		if(a=='open') {
			$(this).html('&nbsp;[-]');
			$(this).attr('action','close');
		} else {
			$(this).html('&nbsp;[+]');
			$(this).attr('action','open');
		}
	});


	$('#div_layout').addClass('no-print').addClass('float').css({'position':'relative','bottom':'50px','border':'1px solid #fe9900','border-radius':'5px','padding':'5px'}).find('h5').addClass('orange').css('margin-bottom','10px');
	$('#div_layout').find('table').css('margin-right','10px');


	$('#tbl_status_display #tr_head td').addClass('b center');
	$('#tbl_status_display td').css('vertical-align','middle').css('padding','5px');
	$('#tbl_status_display, #tbl_status_display td').addClass('noborder');
	$('#tbl_status_display tr').find('td:last').addClass('no-print');
	$('#tbl_status_display tr').find('td:not(:last)').css('padding-right','20px');
	$('#div_status').css('padding','15px');

".$layout_js."

	$('#display_self_yes').click(function() {
		if(!($('.self_title:first').is(':visible'))) {
			$('.self_title').show();
			$('.self_col').show();
			resetColspan('SHOW');
		}
	});
	$('#display_self_no').click(function() {
		if(($('.self_title:first').is(':visible'))) {
			$('.self_title').hide();
			$('.self_col').hide();
			resetColspan('HIDE');
		}
	});
	$('#display_mod_yes').click(function() {
		if(!($('.mod_title:first').is(':visible'))) {
			$('.mod_title').show();
			$('.mod_col').show();
			resetColspan('SHOW');
		}
	});
	$('#display_mod_no').click(function() {
		if(($('.mod_title:first').is(':visible'))) {
			$('.mod_title').hide();
			$('.mod_col').hide();
			resetColspan('HIDE');
		}
	});

	$('#display_final_yes').click(function() {
		if(!($('.final_title:first').is(':visible'))) {
			$('.final_title').show();
			$('.final_col').show();
			resetColspan('SHOW');
		}
	});
	$('#display_final_no').click(function() {
		if($('.final_title:first').is(':visible')) {
			$('.final_title').hide();
			$('.final_col').hide();
			resetColspan('HIDE');
		}
	});


	function resetColspan(act) {
".($display_kpa?"
		//KPA
		var c = $('#tbl_kpa tr:eq(3)').find('td:visible').length;
		$('#tbl_kpa').find('tr.subth').find('td').prop('colspan',c);
		var t = $('#tbl_kpa').find('tr.total:first').find('th:eq(3)').prop('colspan');
		if(act=='SHOW') { t=t+2; } else { t=t-2; }
		$('#tbl_kpa').find('tr.total').find('th:eq(3)').prop('colspan',t);
		$('#tbl_kpa').find('tr.gtotal').find('th:eq(3)').prop('colspan',t+1);
":"")."
".($display_jal?"
		//JAL
		c = $('#tbl_jal tr:eq(3)').find('td:visible').length;
		$('#tbl_jal').find('tr.subth').find('td').prop('colspan',c);
		t = $('#tbl_jal').find('tr.total:first').find('th:eq(3)').prop('colspan');
		if(act=='SHOW') { t=t+2; } else { t=t-2; }
		$('#tbl_jal').find('tr.total').find('th:eq(3)').prop('colspan',t);
		$('#tbl_jal').find('tr.gtotal').find('th:eq(3)').prop('colspan',t+1);
":"")."
".($display_cc?"
		//CC
		c = $('#tbl_cc tr:eq(4)').find('td:visible').length;
		$('#tbl_cc').find('tr.subth').find('td').prop('colspan',c);
		if($('#tbl_cc').find('tr.total').find('td:eq(3)').is(':visible')) {
			t = $('#tbl_cc').find('tr.total:first').find('td:eq(3)').prop('colspan');
			if(act=='SHOW') { t=t+2; } else { t=t-2; }
		} else {
			t = 2;
		}
		$('#tbl_cc').find('tr.total').find('td:eq(3)').prop('colspan',t);
		if(t==0) {
			$('#tbl_cc').find('tr.total').find('td:eq(3)').hide();
			$('#tbl_cc').find('tr.total').find('td:eq(1)').html('Total:');
		} else {
			$('#tbl_cc').find('tr.total').find('td:eq(3)').show();
			$('#tbl_cc').find('tr.total').find('td:eq(1)').html('Total Weight:');
		}
":"")."
	}

});

</script>
";

?>


<?php
echo $echo['start'];

if($onscreen_display['form']) {
	echo $echo['form']['start'];
}
if($onscreen_display['kpa']) {
	echo $echo['kpa'];
}
if($onscreen_display['jal']) {
	echo $echo['jal'];
}
if($onscreen_display['cc']) {
	echo $echo['cc'];
}
if($onscreen_display['bonus']) {
	echo $echo['bonus'];
}
if($onscreen_display['dashboard']) {
	echo $echo['dashboard'];
}
if($onscreen_display['form']) {
	echo $echo['form']['end'];
}
if($onscreen_display['js']) {
	echo $echo['js'];
}

echo $echo['end'];
}

//ASSIST_HELPER::arrPrint($_SESSION[$assessObj->getModRef()]);
?>