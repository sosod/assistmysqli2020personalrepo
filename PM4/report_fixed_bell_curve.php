<?php

$period_id = $_REQUEST['filter']['period_id'];
unset($_REQUEST['filter']['period_id']);
$filters = $_REQUEST['filter'];

$scdObj = new PM4_SCORECARD();
$assessObj = new PM4_ASSESSMENT();
$assessments = $assessObj->getAssessmentForPerformanceReportByPeriod($period_id, $filters);

//Employee Fields
$emp_obj = new PM4_EMPLOYEE();
$job_titles = $emp_obj->getAllJobTitlesFormattedForSelect();
$departments = $emp_obj->getAllDepartmentsFormattedForSelect();
$job_levels = $emp_obj->getAllJobLevelsFormattedForSelect();


//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($assessments);
//echo '</pre>';

$scores = array();
foreach($assessments as $key => $val){
    $self_score = round($val['self_score_average'], 1);
    $mod_score = round($val['moderation_score_average'], 1);
    $fin_score = round($val['final_score_average'], 1);
    $scores[] = $self_score;
    $scores[] = $mod_score;
    $scores[] = $fin_score;
}
$scores[] = 0;
$scores[] = 5;

sort($scores);

//echo '<pre style="font-size: 18px">';
//echo '<p>SCORES</p>';
//print_r($scores);
//echo '</pre>';

$new_score_array = array_unique($scores);

//echo '<pre style="font-size: 18px">';
//echo '<p>UNIQUE VALUES</p>';
//print_r($new_score_array);
//echo '</pre>';

$data = array();
foreach($new_score_array as $key => $val){
    $data[(string)$val]['score'] = $val;
}

//echo '<pre style="font-size: 18px">';
//echo '<p>DATA - BEFORE</p>';
//print_r($data);
//echo '</pre>';

foreach($assessments as $key => $val){
    $self_score = round($val['self_score_average'], 1);
    $mod_score = round($val['moderation_score_average'], 1);
    $fin_score = round($val['final_score_average'], 1);

    $data[(string)$self_score]['self_count'] = (isset($data[(string)$self_score]['self_count']) && is_numeric($data[(string)$self_score]['self_count']) ? $data[(string)$self_score]['self_count'] + 1 : 1 );
    $data[(string)$mod_score]['mod_count'] = (isset($data[(string)$mod_score]['mod_count']) && is_numeric($data[(string)$mod_score]['mod_count']) ? $data[(string)$mod_score]['mod_count'] + 1 : 1 );
    $data[(string)$fin_score]['final_count'] = (isset($data[(string)$fin_score]['final_count']) && is_numeric($data[(string)$fin_score]['final_count']) ? $data[(string)$fin_score]['final_count'] + 1 : 1 );
}

//echo '<pre style="font-size: 18px">';
//echo '<p>DATA - AFTER SORT OF</p>';
//print_r($data);
//echo '</pre>';

$chart_data = "";
foreach($data as $key => $val){
    if((float)$key == 0 || (float)$key == 5){
        $chart_data .= "{";
        $chart_data .= "\"score\": " . $key . ",";
        $chart_data .= "\"self_y\": " . (isset($val['self_count']) && is_numeric($val['self_count']) ? $val['self_count']  : "0") . ",";
        $chart_data .= "\"mod_y\": " . (isset($val['mod_count']) && is_numeric($val['mod_count']) ? $val['mod_count'] : "0") . ",";
        $chart_data .= "\"final_y\": " . (isset($val['final_count']) && is_numeric($val['final_count']) ? $val['final_count'] : "0") . ",";
        $chart_data .= "},";
    }else{
        $chart_data .= "{";
        $chart_data .= "\"score\": " . $key . ",";
        $chart_data .= (isset($val['self_count']) && is_numeric($val['self_count']) ? "\"self_y\": " . $val['self_count'] . "," : "");
        $chart_data .= (isset($val['mod_count']) && is_numeric($val['mod_count']) ? "\"mod_y\": " . $val['mod_count'] . "," : "");
        $chart_data .= (isset($val['final_count']) && is_numeric($val['final_count']) ? "\"final_y\": " . $val['final_count'] . "," : "");
        $chart_data .= "},";
    }

}

?>
<?php
$layout_js = "";
$options = array('id'=>"display_self",'name'=>"display_self");
$self_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$self_button['js'];
$options = array('id'=>"display_mod",'name'=>"display_mod");
$mod_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$mod_button['js'];
$options = array('id'=>"display_final",'name'=>"display_final");
$final_button = $displayObject->createFormField("BOOL_BUTTON", $options,1);
$layout_js.=$final_button['js'];
?>

<div id=div_layout>
    <h5>Display</h5>
    <input type="hidden" id="self_line" name="self_line" value="true">
    <input type="hidden" id="mod_line" name="mod_line" value="true">
    <input type="hidden" id="final_line" name="final_line" value="true">
    <table>
        <tr>
            <td class=b>Self:</td>
            <td><?php echo $self_button['display']; ?></td>
        </tr>
        <tr>
            <td class=b>Moderation:</td>
            <td><?php echo $mod_button['display']; ?></td>
        </tr>
        <tr>
            <td class=b>Final:</td>
            <td><?php echo $final_button['display']; ?></td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(function() {
        $('#div_layout').addClass('no-print').addClass('float').css({'position':'relative','bottom':'50px','border':'1px solid #fe9900','border-radius':'5px','padding':'5px'}).find('h5').addClass('orange').css('margin-bottom','10px');
        $('#div_layout').find('table').css('margin-right','10px');
        <?php echo $layout_js ?>

        $('#display_self_yes').click(function() {
            if($('#self_line').val() == 'false') {
                $('#self_line').val('true');
                //Then regenerate
                regenerateBellCurve();
            }
        });
        $('#display_self_no').click(function() {
            if($('#self_line').val() == 'true') {
                $('#self_line').val('false');
                //Then regenerate
                regenerateBellCurve();
            }
        });
        $('#display_mod_yes').click(function() {
            if($('#mod_line').val() == 'false') {
                $('#mod_line').val('true');
                //Then regenerate
                regenerateBellCurve();
            }
        });
        $('#display_mod_no').click(function() {
            if($('#mod_line').val() == 'true') {
                $('#mod_line').val('false');
                //Then regenerate
                regenerateBellCurve();
            }
        });

        $('#display_final_yes').click(function() {
            if($('#final_line').val() == 'false') {
                $('#final_line').val('true');
                //Then regenerate
                regenerateBellCurve();
            }
        });
        $('#display_final_no').click(function() {
            if($('#final_line').val() == 'true') {
                $('#final_line').val('false');
                //Then regenerate
                regenerateBellCurve();
            }
        });
    });
</script>

<?php $time = time(); ?>
<script src="common/amcharts.js?<?php echo $time; ?>" type="text/javascript"></script>
<script src="/library/amcharts3/xy.js?<?php echo $time; ?>" type="text/javascript"></script>
<script src="/library/amcharts3/serial.js?<?php echo $time; ?>" type="text/javascript"></script>
<script type="text/javascript" src="/library/amcharts3/themes/light.js?<?php echo $time; ?>"></script>

<div id="bell_curve_div" style="width:80%; height: 550px; background-color: #FFFFFF;"></div>

<script type="text/javascript">
    $(function() {
        AmCharts.makeChart("bell_curve_div", getGraphSettings());
    });

    function getGraphs() {

        var self_score_graph = {
            "balloonText": "<div style='margin:5px;'>Self Score:<b>[[x]]</b><br>Assessments:<b>[[y]]</b></div>",
            "title": "Self Score",
            "bullet": "round",
            "maxBulletSize": 25,
            "lineAlpha": 0.8,
            "lineThickness": 2,
            "lineColor": "#b0de09",
            "fillAlphas": 0,
            "type": "smoothedLine",
            "xField": "score",
            "yField": "self_y",
        };

        var mod_score_graph = {
            "balloonText": "<div style='margin:5px;'>Mod Score:<b>[[x]]</b><br>Assessments:<b>[[y]]</b></div>",
            "title": "Mod Score",
            "bullet": "round",
            "maxBulletSize": 25,
            "lineAlpha": 0.8,
            "lineThickness": 2,
            "lineColor": "#0000ff",
            "fillAlphas": 0,
            "type": "smoothedLine",
            "xField": "score",
            "yField": "mod_y",
        };

        var final_score_graph = {
            "balloonText": "<div style='margin:5px;'>Final Score:<b>[[x]]</b><br>Assessments:<b>[[y]]</b></div>",
            "title": "Final Score",
            "bullet": "round",
            "maxBulletSize": 25,
            "lineAlpha": 0.8,
            "lineThickness": 2,
            "lineColor": "#ff0000",
            "fillAlphas": 0,
            "type": "smoothedLine",
            "xField": "score",
            "yField": "final_y",
        };

        var graphs = [];

        if($('#self_line').val() == 'true'){
            graphs.push(self_score_graph)
        }
        if($('#mod_line').val() == 'true'){
            graphs.push(mod_score_graph)
        }
        if($('#final_line').val() == 'true'){
            graphs.push(final_score_graph)
        }

        return graphs;
    }

    function getGraphSettings() {
        var actual_bell_curve_settings = {
            "type": "xy",
            "pathToImages": "/library/amcharts3/images/",
            "theme": "light",
            "marginRight": 80,
            "dataDateFormat": "YYYY-MM-DD",
            "startDuration": 1.5,
            "trendLines": [],
            "graphs": getGraphs(),
            "valueAxes": [{
                "id": "ValueAxis-1",
                "axisAlpha": 0
            }, {
                "id": "ValueAxis-2",
                "axisAlpha": 0,
                "position": "bottom"
            }],
            "allLabels": [],
            "balloon": {},
            "titles": [{"text":"Bell Curves"}],
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": [<?php echo rtrim($chart_data,",") ?>],

            "export": {
                "enabled": true
            },

            "chartScrollbar": {
                "offset": 15,
                "scrollbarHeight": 5
            },

            "chartCursor": {
                "pan": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0
            }
        };

        var bell_curve_settings = {
            "type": "xy",
            "pathToImages": "/library/amcharts3/images/",
            "theme": "light",
            "marginRight": 80,
            "dataDateFormat": "YYYY-MM-DD",
            "startDuration": 1.5,
            "trendLines": [],
            "graphs": getGraphs(),
            "valueAxes": [{
                "id": "ValueAxis-1",
                "axisAlpha": 0
            }, {
                "id": "ValueAxis-2",
                "axisAlpha": 0,
                "position": "bottom"
            }],
            "allLabels": [],
            "balloon": {},
            "titles": [{"text":"Bell Curves"}],
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": [{
                "score": 0,
                "self_y": 0,
                "mod_y": 0,
                "final_y": 0
            }, {
                "score": 1,
                "self_y": 20,
                "mod_y": 9,
                "final_y": 11
            }, {
                "score": 2,
                "self_y": 30,
                "mod_y": 36,
                "final_y": 22
            }, {
                "score": 3,
                "self_y": 50,
                "mod_y": 47,
                "final_y": 62
            }, {
                "score": 4,
                "self_y": 10,
                "mod_y": 8,
                "final_y": 5
            }, {
                "score": 5,
                "self_y": 0,
                "mod_y": 0,
                "final_y": 0,
            }],

            "export": {
                "enabled": true
            },

            "chartScrollbar": {
                "offset": 15,
                "scrollbarHeight": 5
            },

            "chartCursor": {
                "pan": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0
            }
        };

        return actual_bell_curve_settings;
    }

    function regenerateBellCurve() {
        $('#bell_curve_div').empty();
        AmCharts.makeChart("bell_curve_div", getGraphSettings());
    }
</script>