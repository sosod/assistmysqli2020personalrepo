<?php
require_once("inc_header.php");

echo ASSIST_HELPER::getFloatingDisplay(array("info","Any blank headings will be replaced with the default terminology."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$head_section = array(
	"CONTRACT",
	"CONTRACT_UPDATE",
	"DELIVERABLE",
	"DELIVERABLE_UPDATE",
	"ACTION",
	"ACTION_UPDATE",
);

?>
<table class='tbl-container not-max'><tr><td>
<?php
foreach($head_section as $h_section) {
$headings = $headingObject->getRenameHeadings($h_section);
$o = explode("_",$h_section);
$object_type = $o[0];

?>
<h2><?php echo $headingObject->getObjectName($object_type).(isset($o[1]) && $o[1]=="UPDATE" ? " ".$menuObject->getAMenuNameByField("manage_update"):""); ?> Headings</h2>
<table class='tbl-container not-max'><tr><td>
<form name='frm_<?php echo $h_section; ?>'>
	<input type=hidden name=section value='<?php echo $h_section; ?>' />
<table class=tbl_headings id="<?php echo "tbl_".$h_section; ?>" sec='<?php echo $h_section; ?>'>
	<tr>
		<th width=30px rowspan=2>Ref</th>
		<th width=200px rowspan=2>Default<br />Terminology<br />[Field Type]</th>
		<th rowspan=2>Your<Br />Terminology</th>
		<th colspan=3>Include In List View</th>
		<th width=50px rowspan=2>Required<br />Field</th>
		<th width=50px rowspan=2></th>
	</tr>
	<tr class=th2>
		<th width=50px><?php echo $menuObject->getAMenuNameByField("new"); ?></th>
		<th width=50px><?php echo $menuObject->getAMenuNameByField("manage"); ?></th>
		<th width=50px><?php echo $menuObject->getAMenuNameByField("admin"); ?></th>
	</tr>
<?php
foreach($headings as $fld => $h) {
	echo "
	<tr>
		<td class=center>".$h['id']."</td>
		<td style='position:relative'>".$h['mdefault']." <br /><div style='margin-top: 10px;'>[".$headingObject->getHeadingTypeFormattedForClient($h['type'])."]</div></td>
		<td>";
		$js.= $displayObject->drawFormField("LRGVC",array('id'=>$fld,'name'=>"client[$fld]",'class'=>"heading_textarea",'orig'=>$h['client']),$h['client']);
	echo "</td>";
	if( ($h['status'] & PM4_HEADINGS::SYSTEM_DEFAULT) == PM4_HEADINGS::SYSTEM_DEFAULT) {
		echo "
			<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("ok")."</td>
			<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("ok")."</td>
			<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("ok")."</td>
		";
	} else {
		if($h['can_list']==1) {
			echo "
				<td>";
						$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=> $fld."_list_new",'name'=>"list_new[$fld]",'yes'=>1,'no'=>0,'form'=>"vertical",'extra'=>"boolButtonClick",'other'=>"orig=".$h['list_new']),$h['list_new']);
			echo "</td>
				<td>";
						$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=> $fld."_list_manage",'name'=>"list_manage[$fld]",'yes'=>1,'no'=>0,'form'=>"vertical",'extra'=>"boolButtonClick",'other'=>"orig=".$h['list_manage']),$h['list_manage']);
			echo "</td>
				<td>";
						$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=> $fld."_list_admin",'name'=>"list_admin[$fld]",'yes'=>1,'no'=>0,'form'=>"vertical",'extra'=>"boolButtonClick",'other'=>"orig=".$h['list_admin']),$h['list_admin']);
			echo "</td>";
		} else {
			echo "
				<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("error")."</td>
				<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("error")."</td>
				<td style='text-align: center; vertical-align: middle;'>".$helper->getDisplayIcon("error")."</td>
			";
		}
	}
	if($h['assist_required']==1 || ($h['status'] & PM4_HEADINGS::SYSTEM_DEFAULT) == PM4_HEADINGS::SYSTEM_DEFAULT) {
		echo "<td style='text-align: center; vertical-align: middle;'>";
		echo $helper->getDisplayIcon("ok");
	} else {
		echo "<td style=''>";
		$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=> $fld."_required",'name'=>"required[$fld]",'yes'=>1,'no'=>0,'form'=>"vertical",'extra'=>"boolButtonClick",'other'=>"orig=".$h['required']),$h['required']);
	}
	echo "</td>
		<td>
			<input type=hidden name=changes_".$fld." value='0' />
			<input type=hidden name=ref[$fld] value='".$h['id']."' />
			<input type=button value=Save class=btn_save />
		</td>
	</tr>";
}
?>
</table>
</form>
	</td></tr>
</table>
<?php
}
?>
</td></tr>
<tr>
	<td>&nbsp;</td>
</tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"HEAD"),$h_section); ?></td>
	</tr>
</table>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	//$("input:button").prop("disabled",true);
	$(".tbl_headings tr").not("tr:first").find("span.ui-icon").each(function() {
		if(!($(this).parents("button").length>0)) {
			$(this).css("margin","0 auto");
		}
	});
	$(".heading_textarea").prop("rows",3);
	$(".heading_textarea").prop("cols",30);


	$("textarea.heading_textarea").keyup(function() {
		if(AssistString.code($(this).val())!=$(this).attr('orig') && $(this).val()!=$(this).attr("orig")) {
			$(this).addClass("orange-border");
			//$(this).parent().parent().find("input:button").addClass("orange");
			//$(this).parent().parent().find("input:button").prop("disabled",false).addClass("orange");
		} else {
			$(this).removeClass("orange-border");
			//$(this).parent().parent().find("input:button").removeClass("orange");
		}
/*		var pi = $(this).parents("table").prop("id");
		var c = $("#"+pi+" .orange-border").length;
		if(c==0) {
			$("#"+pi+" tr:last input:button").val("No Changes to Save");
			$("#"+pi+" tr:last input:button").prop("disabled",true);
		} else { 
			$("#"+pi+" tr:last input:button").prop("disabled",false);
			var v = (c>1 ? "Save All "+c+" Changes" : "Save The "+c+" Change");
			$("#"+pi+" tr:last input:button").val(v);
		}*/
	});

	$(".btn_save_all, .btn_save").click(function() {
		var sec = $(this).parents("table").attr("sec");
		if($(this).hasClass("btn_save")) {
			var dta = "section="+sec;
			//var r = $(this).attr("ref");
			//var v = $("#"+r).val();
			//dta+="&"+r+"="+v;
			dta+= "&"+AssistForm.nonFormSerialize($(this).parent().parent());
		} else {
			$form = $("form[name=frm_"+sec+"]");
			var dta = AssistForm.serialize($form);
		}
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Headings.Edit",dta);
		if(result[0]=="ok" && !$(this).hasClass("btn_save")) {
			document.location.href = 'setup_defaults_headings.php?r[]='+result[0]+'&r[]='+result[1];
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	
	$(".tbl_headings").find("textarea.heading_textarea").each(function() {
		$(this).trigger("keyup");
	});
	

});
function boolButtonClick($me) {
	//alert($me.prop("id"));
	//$me.parent().addClass("orange-border");
//	$input = $me.parent().find("input:hidden");
//	if($input.val()!=$input.attr("orig")) {
//		$me.parent().parent().parent().find("input:hidden").val($me.parent().parent().parent().find("input:hidden").val()+1);
//	}
//	$me.parent().parent().parent().find("input:button").prop("disabled",false);
}
</script>