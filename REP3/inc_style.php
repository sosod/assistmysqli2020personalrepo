<?php
include("../styles/REP.php");
?>
<script language=JavaScript>
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
            var pbar = 0;
            var pb2 = 0;
			function incProg(pbar) {
                pbar = parseInt(pbar);
                lbl2.innerText=pbar+"%";
                pb2 = pbar * 2;
                document.getElementById('tbl3').width = pb2;
                document.getElementById('lbl2').innerText = pbar+'%';
			}

			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
</script>
<style type="text/css">
.blank { background-color: #ffffff; }
.tdhover { background-color: #e1e1e1; }
body { font-size: 62.5%; }
h1 { color: #000099; }
h2 { color: #000099; }
h3 { color: #000099; }
h4 { color: #000099; margin-bottom: 10px; text-decoration: underline;}
.noborder { border: 0px solid #ffffff; }
table {
    border-collapse: collapse;
    border: 1px solid #ababab;
}
table td {
    border: 1px solid #ababab;
	color: #000000;
	font-weight: normal;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 10pt;
}
table th {
    border: 1px solid #ffffff;
	color: #ffffff;
	text-align: center;
	background-color: #000099;
	font-weight: bold;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}
</style>
