<?php
include("inc_head.php");
//include("inc_admin.php");
?>
<h1><b><?php echo($moduletitle); ?>: Setup - User Access</b></h1>
<p>Processing...</p>
<?php
//GET VARIABLES PASSED BY FORM
$act = $_POST['act'];
$start = $_POST['s'];
if(!is_numeric($start) || strlen($start)==0) { $start = 0; }
$tid = $_POST['tid'];
$a = $_POST['a'];
$c = $_POST['c'];
$d = $_POST['d'];
$v = $_POST['v'];
if($act=="single")
{
                $tk = $tid;
                $uv = $v;
                $ur = $r;
                $ua = $a;
                $ud = $d;
                $uc = $c;
                $sql = "INSERT INTO ".$dbref."_list_users (tkid, yn, docadmin, addcate, delcate, view) VALUES ";
                $sql.= "('$tk','Y','$ua','$uc','$ud','$uv')";
                include("inc_db_con.php");
                    $tsql = $sql;
                    $trans = "Added user access for user ".$tk;
                    include("inc_transaction_log.php");
                $res = "User added to ".$modtitle;
}
else
{
    if($act == "all")
    {
        $tc = count($tid);
        $vc = count($v);
        $ac = count($a);
        $dc = count($d);
        $cc = count($c);
        $c = $tc+$dc+$vc+$ac+$cc;
        $c = $c/5;
        if($c!=$tc || $c != $dc || $c != $vc || $c != $ac || $c != $cc || $c != round($c))
        {
            $res = "ERROR - Incomplete data";
        }
        else
        {
            $steps = count($tid);
            for($i=0;$i<$steps;$i++)
            {
                $tk = $tid[$i];
                $uv = $v[$i];
                $ua = $a[$i];
                $ud = $d[$i];
                $uc = $c[$i];
                if($ua == "Y") { $ud = "A"; $uc = "Y"; $uv = "Y"; }
                $sql = "INSERT INTO ".$dbref."_list_users (tkid, yn, docadmin, addcate, delcate, view) VALUES ";
                $sql.= "('$tk','Y','$ud','$ua','$uc','$uv')";
                            include("inc_db_con.php");
                                $tsql = $sql;
                                $trans = "Added user access for user ".$tk;
                                include("inc_transaction_log.php");
            }
            $res = "Users added to ".$modtitle;
        }
    }
    else
    {
        $res = "ERROR - Unknown error.";
    }
}

?>
<script type=text/javascript>
var res = "<?php echo($res); ?>";
res = escape(res);
document.location.href = "setup_access.php?mr=<?php echo $tref; ?>&a=access&s=<?php echo($start); ?>&r="+res;
</script>
</body></html>
