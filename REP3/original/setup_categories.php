<?php
    include("inc_ignite.php");

$catetitle = $_POST["catetitle"];
$catematkid = $_POST["catematkid"];

if(strlen($catetitle) > 0)  //IF DATA HAS BEEN SUBMITTED VIA THE FORM
{
    $catetitle = htmlentities($catetitle,ENT_QUOTES,"ISO-8859-1");
    //CREATE NEW CATEGORY RECORD
    $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_categories SET ";
    $sql.= "catetitle = '".$catetitle."', ";
    $sql.= "catematkid = '".$catematkid."', ";
    $sql.= "cateyn = 'Y'";
    include("inc_db_con.php");
        //LOG THE NEW CATEGORY
        $tsql = $sql;
        //$tref = "MN";
        $trans = "Added new category: ".$catetitle;
        include("inc_transaction_log.php");
    $sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE catetitle = '".$catetitle."' AND catematkid = '".$catematkid."' AND cateyn = 'Y'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    $cateid = $row['cateid'];
    //CREATE VIEW ACCESS TO CATEGORY FOR ADMIN
    $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_list_users SET tkid = '".$catematkid."', cateid = ".$cateid.", yn = 'Y'";
    include("inc_db_con.php");
        //LOG THE VIEW ACCESS GRANTED TO THE NEW CATEGORY
        $tsql = $sql;
        //$tref = "MN";
        $trans = "Added view access for user ".$catematkid." to category ".$cateid;
        include("inc_transaction_log.php");
}

    include("inc_admin.php");
    
$start = $_GET['s'];
//echo($start);
if(strlen($catetitle)>0)
{
    $find1 = substr($catetitle,0,3);
    $find[0] = $find1;
}
else
{
    $find1 = html_entity_decode($_GET['f'],ENT_QUOTES,"ISO-8859-1");
    $find = explode(" ",$find1);
}
if(strlen($start)==0 || (!is_numeric($start) && $start != "e")) { $start = 0; }
//echo("-".$start);
$limit = 20;
$sql = "SELECT count(catetitle) as ct FROM assist_".$cmpcode."_".$moduledb."_categories c WHERE c.cateyn = 'Y' ";
foreach($find as $needle)
{
//    if($needle!= "\"")
//    {
        $needle = str_replace("'","&#39",$needle);
        $sql.= " AND c.catetitle LIKE '%".$needle."%'";
//    }
}
$sql.=" ORDER BY catetitle";
include("inc_db_con.php");
    $mr = mysql_fetch_array($rs);
mysql_close($con);
$max = $mr['ct'];
$pages = ceil($max/$limit);
//echo("-".$start);

if(!is_numeric($start)) {
    $start = $max - ($max - ($limit*($pages-1)));
}
//echo("-".$start);
$page = ($start / $limit) + 1;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delCate(c) {
    if(confirm("Are you sure you want to remove category "+c+"?\n\nNote that all documents associated with this category will be deleted.")==true)
    {
        //alert("delete");
        document.location.href = "setup_categories_delete.php?c="+c;
    }
    else
    {
        //alert("abort");
    }
}

function editCate(c) {
    if(confirm("Are you sure you want to edit category "+c+"?\n\nNote that all documents associated with this category will be affected.")==true)
    {
        //alert("edit");
        document.location.href = "setup_categories_edit.php?c="+c;
    }
    else
    {
        //alert("abort");
    }
}

function Validate(me) {

    var ct = me.catetitle.value;
    var ma = me.catematkid.value;
    
    if(ct.length == 0)
    {
        alert("Please enter a category title.");
    }
    else
    {
        if(ma == "X")
        {
            alert("Please select the administrator for this new category.");
        }
        else
        {
            return true;
        }
    }
    return false;
}
</script>
<script type=text/javascript>
function goNext(s,f) {
    f = escape(f);
    document.location.href = "setup_categories.php?s="+s+"&f="+f;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Setup - Categories</b></h1>
<form name=veiw action=setup_categories.php method=get>
    <table cellpadding=5 cellspacing=0 width=650 style="margin-bottom: 10px;">
        <tr>
            <td width=250 style="border-right: 0px;"><input type=button id=b1 value=" |< " onclick="goNext(0,'<?php echo($find1)?>');"> <input type=button id=b2 value=" < " onclick="goNext(<?php echo($start-$limit); ?>,'<?php echo($find1)?>');"> Page <?php echo($page); ?>/<?php echo($pages); ?> <input type=button value=" > " id=b3 onclick="goNext(<?php echo($start+$limit); ?>,'<?php echo($find1)?>');"> <input type=button value=" >| " id=b4 onclick="goNext('e','<?php echo($find1)?>');"></td>
            <td width=150 style="border-right: 0px; border-left: 0px;" align=center><?php if(strlen($find1)>0) { ?><input type=button value="All Categories" onclick="document.location.href = 'setup_categories.php';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=250 align=right style="border-left: 0px;">Search: <input type=hidden name=s value=0><input type=text width=15 name=f> <input type=submit value=" Go "></td>
        </tr>
    </table></form>
<form name=addcate action=setup_categories.php method=post onsubmit="return Validate(this);">
<table border=1 cellpadding=3 cellspacing=0 width=650>
    <tr height=28>
        <th width=40>Ref</th>
        <th>Category</th>
        <th width=60>&nbsp;</th>
    </tr>
<?php
//$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories c WHERE c.cateyn = 'Y' ";
foreach($find as $needle)
{
    $needle = str_replace("'","&#39",$needle);
    $sql.= " AND c.catetitle LIKE '%".$needle."%'";
}
$sql.=" ORDER BY catetitle LIMIT ".$start.", ".$limit;
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r > 0)
{
    $ccount = 0;
    while($row = mysql_fetch_array($rs))
    {
        $caterow[$ccount] = $row;
        $ccount++;
    }
}
mysql_close();

$ccount = 0;
if($r > 0)
{
    foreach($caterow as $cate)
    {
?>
    <tr>
        <td align=center><?php echo($cate['cateid']); ?></td>
        <td ><?php echo($cate['catetitle']); ?></td>
        <td align=center><?php echo("<input type=button value=Edit onclick=\"editCate(".$cate['cateid'].")\"> <input type=button value=Del onclick=\"delCate(".$cate['cateid'].")\">");?></td>
    </tr>
<?php
    }
}
?>
    <tr>
        <td class=tdgeneral valign=top align=center>&nbsp;</td>
        <td class=tdgeneral><input type=text name=catetitle size=30><input type=hidden name=catematkid value="0000"></td>
        <td class=tdgeneral><input type=submit value=Add></td>
    </tr>
</table>
</form>
<form name=veiw action=setup_categories.php method=get>
    <table cellpadding=5 cellspacing=0 width=650 style="margin-bottom: 10px;">
        <tr>
            <td width=250 style="border-right: 0px;"><input type=button id=b1b value=" |< " onclick="goNext(0,'<?php echo($find1)?>');"> <input type=button id=b2b value=" < " onclick="goNext(<?php echo($start-$limit); ?>,'<?php echo($find1)?>');"> Page <?php echo($page); ?>/<?php echo($pages); ?> <input type=button value=" > " id=b3b onclick="goNext(<?php echo($start+$limit); ?>,'<?php echo($find1)?>');"> <input type=button value=" >| " id=b4b onclick="goNext('e','<?php echo($find1)?>');"></td>
            <td width=150 style="border-right: 0px; border-left: 0px;" align=center><?php if(strlen($find1)>0) { ?><input type=button value="All Categories" onclick="document.location.href = 'setup_categories.php';"><?php } else { echo("&nbsp;"); } ?></td>
            <td width=250 align=right style="border-left: 0px;">Search: <input type=hidden name=s value=0><input type=text width=15 name=f> <input type=submit value=" Go "></td>
        </tr>
    </table></form>
<script type=text/javascript>
<?php if($start==0) {?>document.getElementById('b1').disabled = true; document.getElementById('b2').disabled = true; <?php } ?>
<?php if($start == ($max - ($max - ($limit*($pages-1)))) ) { ?>document.getElementById('b3').disabled = true; document.getElementById('b4').disabled = true;<?php } ?>
<?php if($start==0) {?>document.getElementById('b1b').disabled = true; document.getElementById('b2b').disabled = true; <?php } ?>
<?php if($start == ($max - ($max - ($limit*($pages-1)))) ) { ?>document.getElementById('b3b').disabled = true; document.getElementById('b4b').disabled = true;<?php } ?>
</script>
<?php
$helpfile = "../help/".$tref."_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</body>

</html>
