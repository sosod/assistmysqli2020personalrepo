<?php
include("inc_ignite.php");
include("inc_polref.php");
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc>Policies and Procedures: <?php echo($polheading); ?></h1>
<?php
$sql = "SELECT t.topid, t.toptitle, count(a.polyn) AS pol";
$sql .= " FROM assist_policies_topcategories t,";
$sql .= " assist_policies_subcategories s,";
$sql .= " assist_policies p,";
$sql .= " assist_".$cmpcode."_policies a";
$sql .= " WHERE t.topid = s.subtopid";
$sql .= " AND s.subid = p.polsubcateid";
$sql .= " AND p.polid = a.polassistid";
$sql .= " AND a.polyn = 'Y'";
$sql .= " AND a.polpubyn = 'Y'";
$sql .= " AND p.polref = '".$polref."'";
$sql .= " AND t.cateyn = 'Y'";
$sql .= " GROUP BY t.topid";
//echo $sql;
include("inc_db_con.php");
$t = mysql_num_rows($rs);
if($t > 0)
{
    $terr = "N";
    while($row = mysql_fetch_array($rs))
    {
        $pol = $row['pol'];
        if($pol > 0)
        {
            $tid .= "_".$row['topid'];
            $ttitle .= "_".$row['toptitle'];
        }
    }
    $tarrid = explode("_",$tid);
    $tarrtitle = explode("_",$ttitle);
}

else
{
    $terr = "Y";
}
mysql_close();
if($terr == "N")
{
    $t = 0;
    foreach($tarrid as $tid)
    {
        if(strlen($tid) > 0)
        {
            $ttitle = $tarrtitle[$t];
            //echo("<h3 class=fc>".$ttitle."</h3>");
            if($ttitle != "Templates")
            {
                $sql = "SELECT * FROM assist_policies_subcategories WHERE subtopid = ".$tid." ORDER BY suborder ASC";
                include("inc_db_con.php");
                $s = mysql_num_rows($rs);
                if($s > 0)
                {
                    $serr = "N";
                    while($row = mysql_fetch_array($rs))
                    {
                        $sid .= "_".$row['subid'];
                        $stitle .= "_".$row['subtitle'];
                    }
                    $sarrid = explode("_",$sid);
                    $sarrtitle = explode("_",$stitle);
                }
                else
                {
                    $serr = "Y";
                }
                mysql_close();
                $sid = "";
                $stitle = "";
                
                if($serr == "N")
                {
                    $s = 0;
                    foreach($sarrid as $sid)
                    {
                        if(strlen($sid) > 0)
                        {
                            $sql = "SELECT p.poltitle ptitle, p.polfile pfile, a.polid pid, p.polid aid FROM assist_policies p, assist_".$cmpcode."_policies a";
                            $sql .= " WHERE p.polsubcateid = ".$sid;
                            $sql .= " AND p.polid = a.polassistid";
                            $sql .= " AND a.polyn = 'Y'";
                            $sql .= " AND a.polpubyn = 'Y'";
                            $sql .= " ORDER BY p.polorder";
                            
                            include("inc_db_con.php");
                            $p = mysql_num_rows($rs);
                            if($p > 0)
                            {
                                $stitle = $sarrtitle[$s];
                                echo("<ul><li>".$stitle."</b></li><ul class=ul2>");
                                //echo("<ul>");
                                while($row = mysql_fetch_array($rs))
                                {
                                    $ptitle = $row['ptitle'];
                                    $pfile = $row['pfile'];
                                    $pid = $row['pid'];
                                    $aid = $row['aid'];
                                    switch($pfile)
                                    {
                                        case "csv":
                                        case "TXT":
                                            $url = "<a href=view.php?i=".$pid.">";
                                            break;
                                        case "PDF":
                                            $url = "<a href=files/".$aid.".pdf target=_blank>";
                                            break;
                                        default:
                                            $url = "";
                                            break;
                                    }
                                    echo("<li>".$url.$ptitle."</a></li>");
                                }
                                echo("</ul></ul>");
                            }
                            mysql_close();
                            
                        }
                        $sid = "";
                        $stitle = "";
                        $s = $s + 1;
                    }
                }
                else
                {
                    echo("<p>There are no policies and procedures to display.</p>");
                }
            }   //IF TTITLE != TEMPLATES
        }   //IF STRLEN TID > 0
        $t = $t + 1;
        $tid = "";
        $ttitle = "";
    }   //FOREACH TID
}   //IF TERR = N
else
{
    echo("<p>There are currently no policies and procedures available.</p>");
}

/*
$sql = "SELECT * FROM assist_policies_topcategories a WHERE topid = 3 AND cateyn = 'Y'";
include("inc_db_con.php");
$temp = mysql_num_rows($rs);
if($temp > 0)
{
    $tempyn = "Y";
}
else
{
    $tempyn = "N";
}

if($tempyn == "Y")
{
$sql = "SELECT t.toptitle, p.poltitle, p.polfile pfile, p.polorder, a.polid pid, p.polid aid FROM assist_policies_topcategories t, assist_policies_subcategories s, assist_policies p, assist_".$cmpcode."_policies a";
$sql .= " WHERE t.topid = s.subtopid";
$sql .= " AND s.subid = p.polsubcateid";
$sql .= " AND a.polassistid = p.polid";
$sql .= " AND a.polyn = 'Y'";
$sql .= " AND t.cateyn = 'Y'";
$sql .= " AND s.subtemplates = 'Y'";
$sql .= " ORDER BY t.toporder, p.polorder";
include("inc_db_con.php");
$t = mysql_num_rows($rs);
$tc = 0;
if($t > 0)
{
    $terr = "T";
    echo("<h3 class=fc>Templates</h3>");
    echo("<ul>");
    $toptitle1 = "";
    while($row = mysql_fetch_array($rs))
    {
        $toptitle2 = $row['toptitle'];
        $pfile = $row['pfile'];
        $aid = $row['aid'];
        $pid = $row['pid'];
        if($toptitle2 != $toptitle1)
        {
            $tc = $tc+1;
            if($tc > 1)
            {
                echo("</ul>");
            }
            echo("<li>".$toptitle2."</li><ul class=ul2>");
            $toptitle1 = $toptitle2;
            $toptitle2 = "";
        }
        switch($pfile)
        {
            case "TXT":
                $url = "<a href=view.php?i=".$pid.">";
                break;
            case "PDF":
                $url = "<a href=files/".$aid.".pdf target=_blank>";
                break;
            default:
                $url = "";
                break;
        }
        echo("<li>".$url.$row['poltitle']."</a></li>");
    }
    echo("</ul>");
}   //IF NO TEMPLATES FOUND

if($terr == "Y")
{
    echo("<p>There are no policies and procedures to display.</p>");
}
}*/
?>
</body></html>
