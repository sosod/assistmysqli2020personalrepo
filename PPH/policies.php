<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<base target="main">
</head>
<script type=text/javascript>
function restDef() {
	if(confirm("Are you sure you wish to restore ALL variables to their default setting?\nThis will affect all policies.")) {
		document.location.href = 'policies_process.php?act=Restore';
	} else {
	}
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<body topmargin=10 leftmargin=10 bottommargin=0 rightmargin=5>

<h1 class=fc style="margin-bottom: 20px"><b>Policies & Procedures: Variables</b></h1>
<form name=pol method=post action=policies_process.php>
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader>Variable</td>
        <td class=tdheader>Value</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_policies_variables p, assist_".$cmpcode."_policies_variables v WHERE p.varfield = v.field";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $comm = $row['varcomment'];
        $fld = $row['varfield'];
        $val = $row['value'];
        echo("<tr>");
        echo("<td class=tdgeneral>".$comm."<input type=hidden name=fld[] value=\"".$fld."\"></td>");
        $valsub = substr($val,0,5);
        if(strtolower($valsub) == "<font")
        {
            $val2 = "";
        }
        else
        {
            $val2 = $val;
        }
        echo("<td class=tdgeneral><input type=text name=val[] size=30 value=\"".$val2."\"><input type=hidden name=valold[] value=\"".$val."\"></td>");
        echo("</tr>");
    }
mysql_close();
?>
    <tr>
        <td class=tdgeneral colspan=2><input type=submit value="Update All"> <input type=reset> <input type=button value="Restore to Default" onclick="restDef();"></td>
    </tr>
</table>
</form>
<?php $urlback = "setup.php"; ?>
<p style="margin-top: 30px"><a href="<?php echo($urlback); ?>"><img src=/pics/tri_left.gif align=absmiddle border=0><span style="text-decoration: none;"> </span>Go Back</a></p>
</body>

</html>
