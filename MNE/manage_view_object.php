<?php
include("inc_header.php");

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
$object_id = $_REQUEST['object_id'];

switch($object_type) {
	case "CONTRACT":
		$child_type = "DELIVERABLE";
		$childObject = new CNTRCT_DELIVERABLE();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getDeliverableObjectName(true);
		break;
	case "DELIVERABLE":
		$child_type = "ACTION";
		$childObject = new CNTRCT_ACTION();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName(true);
		break;
	case "ACTION":
		$child_redirect = "";
		break;
}

$logObject = new CNTRCT_LOG(strtolower($object_type));
$audit_log = $logObject->getObject(array('object_id'=>$object_id));
$audit_log = $audit_log[0];



?>
<button id=btn_add_comment>Add Comment</button>
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, !($object_type=="CONTRACT"),false,($object_type=="CONTRACT")); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Activity Log</h2><?php 
			echo $audit_log;
		?></td>
	</tr>
	<?php if(isset($childObject)) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("open"),'class'=>"btn_view"),$child_type,$child_options); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
var url = "<?php echo $child_redirect; ?>";
$(function() {
	<?php 
	echo $js; 
	?>
	$("#view_all").val("View Complete "+window.contract_object_name);
	$("input:button.btn_view").click(function() {
		tableButtonClick($(this));
	});
	$("#view_all").click(function(){
		AssistHelper.processing();
		if(confirm("Please be patient while the page loads.  It can take some time depending on the amount of information.")==true) {
			document.location.href = "manage_view_all.php?object_id=<?php echo $object_id; ?>";
		} else {
			AssistHelper.closeProcessing();
		}
	});
	
	$("#btn_add_comment").button({
		icons: {primary: "ui-icon-comment"},
	}).css({"position":"absolute","top":"10px","right":"10px"}).removeClass("ui-state-default").addClass("ui-button-state-ok")
	.click(function(e) {
		e.preventDefault();
		$("<div />",{id:"dlg_comment"}).dialog({modal: true});
	});
	
});
function tableButtonClick($me) {
	var i = $me.attr("ref");
	url = url+i;
	document.location.href = url;
}
</script>
<?php
//markTime("end");
?>