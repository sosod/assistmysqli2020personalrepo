<?php
$display_navigation_buttons = false;
include("inc_header.php");
$s = $_REQUEST['s'];
$parent_object_id = $_REQUEST['parent_id'];
$child_object_id = $_REQUEST['child_id'];
switch ($s) {
	case 'CON':
		$page_redirect_path = "manage_assurance_contract_add.php?object_id=".$parent_object_id."&";
		$parent_object_type = "";
		$childObject = new CNTRCT_ASSURANCE_CONTRACT();
		$child_redirect = "new_deliverable_edit.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getObjectName("CONTRACT")." Assurance";
		$parentObject = "";
		$child_object_type = "CONTRACT_ASSURANCE";
		break;
	
	case 'DEL':
		$page_redirect_path = "manage_assurance_deliverable_add.php?object_id=".$parent_object_id."&";
		$parent_object_type = "DELIVERABLE";
		$childObject = new CNTRCT_ASSURANCE_DELIVERABLE();
		$child_redirect = "new_deliverable_edit.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getObjectName("DELIVERABLE")." Assurance";
		$parentObject = new CNTRCT_DELIVERABLE($parent_object_id);
		$child_object_type = "DELIVERABLE_ASSURANCE";
		
		break;
	case 'ACT':
		$page_redirect_path = "manage_assurance_action_add.php?object_id=".$parent_object_id."&";
		$parent_object_type = "ACTION";
		$childObject = new CNTRCT_ASSURANCE_ACTION();
		$child_redirect = "new_deliverable_edit.php?object_type=ACTION&object_id=";
		$child_name = $helper->getObjectName("ACTION")." Assurance";
		$parentObject = new CNTRCT_ACTION($parent_object_id);
		$child_object_type = "ACTION_ASSURANCE";
		
		break;
	
	default:
		echo "BUSTED FRAME - you sent '".implode(", ",$_REQUEST)."'. Try again";
		break;
}
?>
		<div id='edit_content' name='edcon'>
		<?php 
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id,"Edit", $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?>
		</div>
	
<script type=text/javascript>
	<?php echo $js; ?>
	document.defaultView.parent.finished('<?php echo $child_object_id; ?>');
</script>