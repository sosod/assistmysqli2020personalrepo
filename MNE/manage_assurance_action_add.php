<?php
require_once("inc_header.php");

$parent_object_type = "ACTION";
$parent_object_id = $_REQUEST['object_id'];
$page_section = "MANAGE";
$page_action = "UPDATE";
$page_redirect_path = "manage_assurance_action_add.php?object_id=".$parent_object_id."&";
switch($parent_object_type) {
	case "CONTRACT":
		$childObject = new CNTRCT_ASSURANCE_CONTRACT();
		$child_objects = $childObject->getAObject($parent_object_id);
//		ASSIST_HELPER::arrPrint($child_objects);
		$child_redirect = "new_deliverable_edit.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getObjectName("CONTRACT")." Assurance";
		$parentObject = new CNTRCT_CONTRACT($parent_object_id);
		$child_object_type = "CONTRACT_ASSURANCE";
		$child_object_id = 0;
		//$parent_id_name = $childObject->getParentFieldName();
		//$parent_id = 0;
		break;
	case "DELIVERABLE":
		$childObject = new CNTRCT_ASSURANCE_DELIVERABLE();
		$child_objects = $childObject->getAObject($parent_object_id);
//		ASSIST_HELPER::arrPrint($child_objects);
		$child_redirect = "new_deliverable_edit.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getObjectName("DELIVERABLE")." Assurance";
		$parentObject = new CNTRCT_DELIVERABLE($parent_object_id);
		$child_object_type = "DELIVERABLE_ASSURANCE";
		$child_object_id = 0;
		break;
	case "ACTION":
		$childObject = new CNTRCT_ASSURANCE_ACTION();
		$child_objects = $childObject->getAObject($parent_object_id);
//		ASSIST_HELPER::arrPrint($child_objects);
		$child_redirect = "new_action_edit.php?object_type=ACTION&object_id=";
		$child_name = $helper->getObjectName("ACTION")." Assurance";
		$parentObject = new CNTRCT_ACTION($parent_object_id);
		$child_object_type = "ACTION_ASSURANCE";
		$child_object_id = 0;
		break;
}

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class=tbl-container>
	<tr>
		<td width=47%>
			<?php 
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="CONTRACT"));
			$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : ""; 
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>New <?php echo $child_name; ?></h2><?php 
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id,"Add", $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?></td>
	</tr>
	<?php if(count($child_objects)>0) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php //$js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_edit")); ?>
			<?php $js.=$displayObject->drawListTable($child_objects,array()); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<div id='edit_dialog' style='display:none;'>
	
</div>
<iframe style='width:0;height:0' id='edit_frame' src='manage_assurance_edit_frame.php?s=ACT&object_id=<?php echo $parent_object_id; ?>'>

</iframe>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>
	
	$("input:button.btn_edit").click(function() {
		$("#edit_dialog").show().dialog({
			width:"575px", 
			position: { my: "center", at: "top", of: document }
		});
		var i = $(this).attr("ref");
		var result = AssistHelper.doAjax("inc_controller.php?action=ACTION_ASSURANCE.Get&id="+i);
		var date = result.aa_date_tested;
//		console.log(result);
		var iframe = document.getElementById("edit_frame");
		var content = iframe.contentDocument || iframe.contentWindow.document;
		$(content);
		var editer = $(content).find("div[name='edcon']");
		$("#edit_dialog").append(editer);
		$("#edit_dialog #aa_response").val(result.aa_response);
		$("#edit_dialog #aa_date_tested").val(date).datepicker({
			dateFormat:"yy-mm-dd",
			defaultDate:date,
		});
		$("#edit_dialog [name='object_id']").val(result.aa_id);
		$inputs = $("#edit_dialog td :last").find("input");
		var firstrun = true;
		$inputs.each(function(){
			if($(this).prop("class")=="close_dlg"){
				firstrun = false;
			}
		});
		if(firstrun){
			$("#edit_dialog td :last").append("<input type='button' value='Cancel' class='close_dlg' id='close_"+i+"'></input>");
		}
		//console.log($(this).prop("id"));
	});

	$(document).on("click",".close_dlg", function(){
		$("#edit_dialog").dialog("close");
	});
	
	$(document).on("click","#btn_save",function(){
		//alert("hi");
		//console.log($(this).siblings(":hidden").val());
		var data = AssistForm.serialize($(this).parents("form"));
		var result = AssistHelper.doAjax("inc_controller.php?action=ACTION_ASSURANCE.Edit",data);
		//console.log(result);
	});
	
});
</script>
