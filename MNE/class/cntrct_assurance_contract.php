<?php
/**
 * To manage the ASSURANCE function
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class CNTRCT_ASSURANCE_CONTRACT extends CNTRCT_ASSURANCE {
    
	protected $object_id = 0;
	protected $object_details = array();
    
    //protected $progress_status_field = "_status_id";
	protected $status_field = "_status";
	protected $id_field = "ca_id";
	protected $parent_field = "ca_contract_id";
	//protected $name_field = "_name";
	//protected $deadline_field = "_planned_date_completed";
	//protected $owner_field = "_owner_id";
	//protected $manager_field = "_manager";
	//protected $authoriser_field = "_authoriser";
	protected $attachment_field = "ca_attachment";
	//protected $progress_field = "_progress";
	
    /*************
     * CONSTANTS
     */
    //const OBJECT_TYPE = "ASSURANCE";
    const TABLE = "contract_assurance";
    const TABLE_FLD = "ca";
    const REFTAG = "CCAS";
	//const LOG_TABLE = "assurance";
	/**
	 * STATUS CONSTANTS
	 */
	//const CONFIRMED = 32;
	//const ACTIVATED = 64;
	/**
	 * ASSESSMENT TYPE CONSTANTS
	 */
	//const QUALITATIVE = 1024;
	//const QUANTITATIVE = 2048;
	//const OTHER = 4096;
    
    public function __construct($object_id=0) {
        parent::__construct();
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
    }
    
	public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
    //public function getMyObjectType() { return self::OBJECT_TYPE; }
    //public function getMyLogTable() { return self::LOG_TABLE; }
	
	/*******************************
	 * CONTROLLER functions
	 */
	//public function addObject($var) { - parent class
	public function addObject($var,$fld=""){
		$fld = $this->getTableField();
		return parent::addObject($var,$fld);
	}


	//public function editObject($var,$attach=array()) { - parent class
	

	
    /*************************************
     * GET functions
     * */
     public function getObject($id){
     	$id = $id['id'];
     	$sql = "SELECT * FROM ".$this->getTableName()." WHERE ca_id = $id";
		return $this->mysql_fetch_one($sql);
     }
     
	 public function getAObject($id=0,$options=array()){
	 	//return $this->getMyList("ASSURANCE","MANAGE");
	 	$head = new CNTRCT_HEADINGS();
		$headarr = $head->getMainObjectHeadings("ASSURANCE","LIST","",$this->getTableField()."_"); 
		echo $this->getTableField();
		//foreach($headarr as $key=>$val){
			foreach($headarr as $index=>$item){
				$data['head'][$index]=$item;
			}
		//}		
		$rows = $this->getRawListObject($id);
	//	ASSIST_HELPER::arrPrint($headarr);
	//	ASSIST_HELPER::arrPrint($rows);
		//Sanitize rows data for display
		$data = $this->formatRowDisplay(count($rows), $rows, $data, $this->getIDFieldName(), $head, $this->getRefTag(), $this->getStatusFieldName(), array('limit'=>false,'pagelimit'=>"",'start'=>0));
	//	ASSIST_HELPER::arrPrint($data);
		/*
		foreach($rows as $key=>$val){
			foreach($val as $index=>$item){
				if($index == "ca_contract_id" ||$index == "ca_status" ||$index == "ca_insertuser" ||$index == "ca_insertdate"){
					unset($rows[$key][$index]);
				}else if($index == "ca_attachment" && strlen($item) > 0){
					$attach = unserialize($item);
					if($attach[0]['status'] == CNTRCT_LOG::ACTIVE){
						$attach = $attach[0];
						$link="<a href='../files/".$this->getCmpCode()."/CONTRACT/assurance/".$attach['system_filename']."'>".$attach['original_filename']."</a>";
						$rows[$key][$index]=$link;
					}
				}
			}
		}
		foreach($rows as $key=>$val){
			$rows[$val['ca_id']]=$val;
			unset($rows[$key]);
		}
		$data['rows']=$rows;
		*/
		return $data;
	 	return $this->getDetailedObject("ASSURANCE", $id,$options);
	 }
	 
	 	public function getRawUpdateObject($id){
		return $this->getRawObject($id);
	}
	
	/***
	 * Returns an unformatted array of an object 
	 */
	 
	public function getRawObject($obj_id) {
		$tmp = is_numeric($obj_id)?" AND ca_id = ".$obj_id:"";
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE (ca_status & ".CNTRCT_LOG::ACTIVE.")=".CNTRCT_LOG::ACTIVE.$tmp;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	
	public function getRawListObject($obj_id="") {
		$tmp = is_numeric($obj_id)?" AND ca_contract_id = ".$obj_id:"";
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE (ca_status & ".CNTRCT_LOG::ACTIVE.")=".CNTRCT_LOG::ACTIVE.$tmp;
		$data = $this->mysql_fetch_all($sql);
		
		return $data;
	}
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}


?>