<?php
  
class CNTRCT_FINANCE_DRAWDOWN extends CNTRCT_FINANCE {
	
	protected $table_name = "_finance_drawdown";
	protected $field_prefix = "fd";
	
	protected $ref_tag = "CFD";
	
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $status_field = "_status";
	protected $primary_finance_field = "_name";
	protected $secondary_finance_field = "_comment";
	protected $extra_finance_field = "_perc_completion";
	protected $sort_field = "_name";
	
	protected $del_table_name = "_deliverable_drawdown";
	protected $del_field_prefix = "dd";
	protected $del_id_field = "_id";
	protected $del_object_field = "_del_id";
	protected $del_parent_field = "_fd_id";
	protected $del_status_field = "_status";
	protected $del_primary_finance_field = "";
	protected $del_secondary_finance_field = "";
	protected $del_extra_finance_field = "";
	
	protected $non_edit_fields = array();
	
	protected $js = "";
	
	protected $object_type = "FINANCE_DRAWDOWN";

	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getSortFieldName() { return $this->getTableField().$this->sort_field; }
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function getObject() {
		return array("error","function not programmed");
	}
	public function addObject($var) {
		$deliverables = $var['dd'];
		unset($var['dd']);
		$return = $this->addMyObject($var);
		if($return[0]=="ok") {
			$id = $return['object_id'];
			if(count($deliverables)>0) {
				$sql_array = array();
				foreach($deliverables as $d) {
					$sql_array[] = "(null, ".$d.", ".$id.", ".CNTRCT::ACTIVE.",now(),'".$this->getUserID()."')"; 
				}
				$sql = "INSERT INTO ".$this->getDeliverableTableName()." VALUES ".implode(",",$sql_array);
				$this->db_insert($sql);
			}
		}
		return $return;
	}
	public function editObject($var) {
			
		//return array("error",serialize($var));
		
		$id = $var['object_id'];
		$deliverables = $var['dd'];
		unset($var['dd']);
		$return = $this->editMyObject($var);
		//return array("error",serialize($deliverables));
		if($return[0]=="ok" || $return[0]=="info") {
			/****
			 * Get previous deliverables
			 * check for old dels not in current list
			 * check for new dels not in old list
			 * LOGG!!
			 */
			$m = 0;
			$old = $this->getDeliverablesForEdit($id); 
			$new_active = array();
			$restored = array();
			$new_inactive = array();
			foreach($deliverables as $d) {
				if(!isset($old['all'])) {
					$new_active[] = $d;
				} elseif(!isset($old['active'])) {
					$restored[] = $d;
				}
			}
			foreach($old['active'] as $a => $o) {
				if(!in_array($a,$deliverables)) {
					$new_inactive[] = $a;
				}
			}
			//return array("error",serialize($new_inactive));
			$changes = array(
				'user'=>$this->getUserName(),
				'deliverables'=>array(),
			);
			if(count($new_active)>0) {
				foreach($new_active as $d) {
					$x = $this->addDeliverableToDrawdown($id,$d);
					if($x>0) {
						$m++;
						$changes['deliverables'][] = "|deliverable| ".$this->getDelRefTag().$d." added to |".$this->getMyObjectName()."| ".$this->getRefTag().$id; 
					}
				}
			}
			if(count($restored)>0) {
				foreach($restored as $d) {
					$m+= $this->reactivateDeliverableToDrawdown($id,$d);
					$changes['deliverables'][] = "|deliverable| ".$this->getDelRefTag().$d." restored to |".$this->getMyObjectName()."| ".$this->getRefTag().$id;
				}
			}
			if(count($new_inactive)>0) {
				foreach($new_inactive as $d) {
					$m+= $this->deactivateDeliverableFromDrawdown($id,$d);
					$changes['deliverables'][] = "|deliverable| ".$this->getDelRefTag().$d." removed from |".$this->getMyObjectName()."| ".$this->getRefTag().$id;
				}
			}
			if($m>0) {
				if(count($changes['deliverables'])>0) {
					$log_var = array(
						'object_id'	=> $id,
						'changes'	=> $changes,
						'log_type'	=> CNTRCT_LOG::EDIT,
						'section'	=> $this->getMyObjectName(),
					);
					$this->addActivityLog($this->getMyLogTable(), $log_var);
				}
				$return[0] = "ok";
				$return[1] = "Changes saved successfully.";
			}
		}
		return $return;
	}
	//	public function deleteObject($var) {
	//	return array("error","function not programmed");
	//}
	
	
	public function getAllItems($contract_id=0, $heading=true) {
		$sql = "SELECT ".$this->getIDFieldName()." as id
						, ".$this->getPrimaryFinanceFieldName()." as name
						, ".$this->getSecondaryFinanceFieldName()." as comment
						, ".$this->getExtraFinanceFieldName()." as extra
						, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag 
				FROM ".$this->getTableName()."  
				WHERE ".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE." = ".CNTRCT::ACTIVE."
				AND ".$this->getParentFieldName()." = ".$contract_id." 
				ORDER BY ".$this->getSortFieldName();
		$result = $this->mysql_fetch_all_by_id($sql, "id");
		if($heading) {
			$displayObject = new CNTRCT_DISPLAY_FINANCE();
			foreach($result as $key => $r) {
				if($r['extra']>0) {
					$p = $displayObject->getDataField("PERC",$r['extra']);
					$result[$key]['name'] = $r['name']." (".$p['display'].")";
				}
			}
		}
		return $result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*******************************************
	 * DELIVERABLE Functions
	 */

	public function getDeliverableDrawDowns($contract_id=0,$del_ids=array()) {
		$delObject = new CNTRCT_DELIVERABLE();
		$result = array("del"=>array(),"dd"=>array());
		$sql = "SELECT DD.".$this->getDeliverableTableField()."_".$delObject->getIDFieldName()." as del, DD.".$this->getDeliverableParentFieldName()." as dd 
				FROM ".$this->getDeliverableTableName()." DD
				INNER JOIN ".$delObject->getTableName()." D 
				  ON D.".$delObject->getIDFieldName()." = DD.".$this->getDeliverableTableField()."_".$delObject->getIDFieldName()." 
				  ".($contract_id > 0 ? "AND D.".$delObject->getParentFieldName()." = ".$contract_id : "")."  
				WHERE DD.".$this->getDeliverableStatusFieldName()." & ".CNTRCT::ACTIVE." = ".CNTRCT::ACTIVE." 
				".(count($del_ids)>0 ? "AND DD.".$this->getDeliverableTableField()."_".$delObject->getIDFieldName()." IN (".implode(",",$del_ids).") " : "")."
				ORDER BY D.".$delObject->getDeadlineField().", D.".$delObject->getIDFieldName();
		$all = $this->mysql_fetch_all($sql);
		foreach($all as $row) {
			$result['del'][$row['del']] = $row['dd'];
			$result['dd'][$row['dd']][] = $row['del'];
		}
		return $result;
	}
	
	
	public function getDeliverablesForEdit($object_id) {
		$sql = "SELECT dd_id as id, dd_del_id as del_id, dd_status as status 
				FROM ".$this->getDeliverableTableName()."
				WHERE dd_fd_id = ".$object_id."
				ORDER BY dd_id DESC";
		$dd = $this->mysql_fetch_all($sql);
		$data = array(
			'active'=>array(),
			'inactive'=>array(),
			'deleted'=>array(),
			'all'=>array(),
		);
		foreach($dd as $d) {
			$di = $d['del_id'];
			if(!isset($data['all'][$di])) {
				$data['all'][$di] = $d;
				if(($d['status'] & CNTRCT::INACTIVE) == CNTRCT::INACTIVE) {
					$data['inactive'][$di] = $d;
				} elseif(($d['status'] & CNTRCT::DELETED) == CNTRCT::DELETED) {
					$data['deleted'][$di] = $d;
				} else {
					$data['active'][$di] = $d;
				}
			}
		}
		return $data;
	}
	
	
	private function addDeliverableToDrawdown($object_id,$del_id) {
		$sql = "INSERT INTO ".$this->getDeliverableTableName()."
				SET dd_del_id = $del_id ,
					dd_fd_id = $object_id ,
					dd_status = ".CNTRCT::ACTIVE." ,
					dd_insertdate = now() ,
					dd_insertuser = '".$this->getUserID()."' 
				";
		return $this->db_insert($sql);
	}
	
	private function deactivateDeliverableFromDrawdown($object_id,$del_id) {
		$sql = "UPDATE ".$this->getDeliverableTableName()."
				SET dd_status = ".CNTRCT::INACTIVE." 
				WHERE dd_del_id = $del_id 
				  AND dd_fd_id = $object_id 
				";
		return $this->db_update($sql);
	}
	
	private function reactivateDeliverableToDrawdown($object_id,$del_id) {
		$sql = "UPDATE ".$this->getDeliverableTableName()."
				SET dd_status = ".CNTRCT::ACTIVE." 
				WHERE dd_del_id = $del_id 
				  AND dd_fd_id = $object_id 
				";
		return $this->db_update($sql);
	}
	
	private function deleteDeliverablesWithDrawdown($object_id) {
		$sql = "UPDATE ".$this->getDeliverableTableName()."
				SET dd_status = dd_status + ".CNTRCT::DELETED." 
				WHERE dd_fd_id = $object_id 
				  AND ( dd_status & ".CNTRCT::DELETED." ) <> ".CNTRCT::DELETED." 
				";
		return $this->db_update($sql);
	}
	
	
		
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>