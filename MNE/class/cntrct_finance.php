<?php
  
class CNTRCT_FINANCE extends CNTRCT_HELPER {
	
	//protected $table_name = "_";				//set in child class
	//protected $field_prefix = "f";			//set in child class
	//protected $ref_tag = "CF";				//set in child class
	
	//protected $id_field = "_id";	//set in child class
	//protected $parent_field = "_contract";	//set in child class
	//protected $status_field = "_status";		//set in child class
	//protected $primary_finance_field = "_amount";	//set in child class

	//protected $del_table_name = "_";				//set in child class
	//protected $del_field_prefix = "f";			//set in child class
	//protected $del_parent_field = "_contract";	//set in child class
	//protected $del_status_field = "_status";		//set in child class
	//protected $del_primary_finance_field = "_amount";	//set in child class
	
	//protected $object_type = "FINANCE_";			//set in child class
	
		
	//protected $js = "";						//set in child class
	
	protected $attachment_field = "_attachment";
	protected $del_reftag = "";
	protected $del_object_field = "_del_id";
	

	protected $del_headings = array();
	
	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
		$this->id_field = $this->getTableField().$this->id_field;
		$this->parent_field = $this->getTableField().$this->parent_field;
		$this->status_field = $this->getTableField().$this->status_field;
		$this->primary_finance_field = $this->getTableField().$this->primary_finance_field;
		$this->secondary_finance_field = $this->getTableField().$this->secondary_finance_field;
		$this->extra_finance_field = $this->getTableField().$this->extra_finance_field;
		$this->attachment_field = $this->getTableField().$this->attachment_field;
		
		$this->del_object_field = $this->getDeliverableTableField().$this->del_object_field;
		$this->del_id_field = $this->getDeliverableTableField().$this->del_id_field;
		$this->del_parent_field = $this->getDeliverableTableField().$this->del_parent_field;
		$this->del_status_field = $this->getDeliverableTableField().$this->del_status_field;
		$this->del_primary_finance_field = $this->getDeliverableTableField().$this->del_primary_finance_field;
		if($this->del_secondary_finance_field!==false || strlen($this->del_secondary_finance_field)>0) {
			$this->del_secondary_finance_field = $this->getDeliverableTableField().$this->del_secondary_finance_field;
		}
		if($this->del_extra_finance_field!==false || strlen($this->del_extra_finance_field)>0) {
			$this->del_extra_finance_field = $this->getDeliverableTableField().$this->del_extra_finance_field;
		}
		$this->del_headings[]= $this->getMyObjectType();
		
		$delObject = new CNTRCT_DELIVERABLE();
		$this->del_reftag = $delObject->getRefTag();
		unset($delObject);
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	//set in child classes
	/*public function getObject() {
		return array("error","parent function not programmed");
	}
	public function addObject() {
		return array("error","parent function not programmed");
	}
	public function editObject() {
		return array("error","parent function not programmed");
	}
	public function deleteObject() {
		return array("error","parent function not programmed");
	}*/
	
	public function addMyObject($var) {
		unset($var['attachments']);
		unset($var['object_id']); //remove incorrect deliverable_id value from add form
		$var[$this->getParentFieldName()] = $var['contract_id'];
		unset($var['contract_id']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		if($this->getMyObjectName()=="INCOME") {// || $this->getMyObjectName()=="EXPENSE") {
			$var[$this->primary_finance_field] = $var[$this->secondary_finance_field] + $var[$this->extra_finance_field];
		}
		$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$import_data = $this->convertArrayToSQL($var);

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$import_data;
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CREATE,
				'section'	=> $this->getMyObjectName(),
			);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","An unexpected error occurred.  Please try again.");
	}
	
	public function editMyObject($var,$attach=array()) {
		//return array("error",serialize($var));
		$id = $var['object_id'];
		unset($var['attachments']);
		unset($var['object_id']); //remove incorrect deliverable_id value from add form
		//$var[$this->getParentFieldName()] = $var['contract_id'];
		unset($var['contract_id']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		if($this->getMyObjectName()=="INCOME") {// || $this->getMyObjectName()=="EXPENSE") {
			$var[$this->primary_finance_field] = $var[$this->secondary_finance_field] + $var[$this->extra_finance_field];
		}
		//$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		//$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		//$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$update_data = $this->convertArrayToSQL($var);

		//GET OLD RECORD
		//$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$old = $this->getRawObject($id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$update_data." WHERE ".$this->getIDFieldName()." = ".$id;
		$mar = $this->db_update($sql);
		if($mar>0 || count($attach)>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			foreach($var as $fld => $v) {
				if($old[$fld]!=$v || $h['type']=="HEADING") {
					$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type'=>"TEXT");
					if(in_array($h['type'],array("LIST","MASTER","USER","OWNER","DEL_TYPE","DELIVERABLE"))) {
						$list_items = array();
						$ids = array($v,$old[$fld]); 
						switch($h['type']) {
							case "LIST":
								$listObject = new CNTRCT_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$userObject = new CNTRCT_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break; 
							case "OWNER":
								$ownerObject = new CNTRCT_CONTRACT_OWNER();
								$list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new CNTRCT_MASTER($head['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break; 
							case "DELIVERABLE":
								$delObject = new CNTRCT_DELIVERABLE();
								$list_items = $delObject->getDeliverableNamesForSubs($ids);
								break; 
							case "DEL_TYPE":
								$delObject = new CNTRCT_DELIVERABLE();
								$list_items = $delObject->getDeliverableTypes($ids);
								break; 
						} 
						unset($listObject);
						$changes[$fld] = array('to'=>$list_items[$v],'from'=>$list_items[$old[$fld]], 'raw'=>array('to'=>$v,'from'=>$old[$fld]));
					} else {
						$changes[$fld] = array('to'=>$v,'from'=>$old[$fld]);
					}
				//} else {
					//$changes[$fld] = "no change";
				}
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getAttachmentFieldName()] = $x;
			
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::EDIT,
				'section'	=> $this->getMyObjectName(),
			);
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully edited.",
				2=> array($log_var,$this->getMyObjectType()),
				3=> $old,
			);
			//$result[0]="error";
			//$result[1] = serialize($changes);
			return $result;
		} else {
			$result = array("info","No changes found to be saved.","",$old);
			return $result;
		}
		return array("error","An unexpected error occurred.  Please try again.");
	}	
	
	public function deleteObject($var) {
		$id = $var['object_id'];
		$raw = $this->getRawObject($id);
		$old_status = $raw[$this->getStatusFieldName()];
		$new_status = $old_status - CNTRCT::ACTIVE + CNTRCT::DELETED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$id;
		$mar = $this->db_update($sql);
		$sql = "SELECT ".$this->getDeliverableIDFieldName()." as id FROM ".$this->getDeliverableTableName()." WHERE ".$this->getDeliverableParentFieldName()." = ".$id." AND ".$this->getDeliverableStatusFieldName()." = ".CNTRCT::ACTIVE;
		$dels = $this->mysql_fetch_all_by_value($sql, "id");
		if($this->getMyObjectName()=="DRAWDOWN") {
			$this->deleteDeliverablesWithDrawdown($id);
		} elseif($dels>0) {
			$sql = "UPDATE ".$this->getDeliverableTableName()." SET ".$this->getDeliverableStatusFieldName()." = ".CNTRCT::DELETED." WHERE ".$this->getDeliverableIDFieldName()." IN (".implode(",",$dels).")";
			$mar+=$this->db_update($sql);
		}
		if($mar>0) {
			$changes= array(
					'response'=>"|".$this->getMyObjectName()."| deleted.",
					'system'=>array(
						'status'=>array('from'=>$old_status,'to'=>$new_status),
						'deliverable'=>$dels,
					),
					'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::DELETE,
				'section'	=> $this->getMyObjectName(),
			);
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully deleted.",
				2=> array($log_var,$this->getMyObjectType()),
				3=> $raw,
			);
		} else {
			$result = array("info","No changes were made.");
		}
		return $result;
	}
	
	
	
	
	/***************************
	 * GET functions
	 */
	public function hasTarget() { return false; }	//needed for centralisation of display class
	public function hasDeadline() { return false; }	//needed for centralisation of display class
	
	public function getMyObjectType() { return $this->object_type; }
	public function getMySecondaryObjectType() { return $this->secondary_object_type; }
	public function getMyObjectName() { return str_ireplace("FINANCE_","",$this->object_type); }
	public function getLogTable() { return CNTRCT_FINANCE::LOG_TABLE; }
	public function getMyLogTable() { return CNTRCT_FINANCE::LOG_TABLE; }
	
	public function getRefTag() { return $this->ref_tag; }
	public function getDelRefTag() { return $this->del_reftag; }

	public function getTableName() { return $this->getDBRef().$this->table_name; }
	public function getTableField() { return $this->field_prefix; }

	public function getIDFieldName() {		return $this->id_field; 	}
	public function getParentFieldName() {		return $this->parent_field; 	}
	public function getStatusFieldName() {		return $this->status_field; 	}
	public function getPrimaryFinanceFieldName() {		return $this->primary_finance_field; 	}
	public function getSecondaryFinanceFieldName() {		return $this->secondary_finance_field; 	}
	public function getExtraFinanceFieldName() {		return $this->extra_finance_field; 	}

	public function getDeliverableTableName() { return $this->getDBRef().$this->del_table_name; }
	public function getDeliverableTableField() { return $this->del_field_prefix; }

	public function getDeliverableObjectIDFieldName() {		return $this->del_object_field; 	}
	public function getDeliverableIDFieldName() {		return $this->del_id_field; 	}
	public function getDeliverableParentFieldName() {		return $this->del_parent_field; 	}
	public function getDeliverableStatusFieldName() {		return $this->del_status_field; 	}
	public function getDeliverablePrimaryFinanceFieldName() {		return $this->del_primary_finance_field; 	}
	public function getDeliverableSecondaryFinanceFieldName() {		return $this->del_secondary_finance_field; 	}
	public function getDeliverableExtraFinanceFieldName() {		return $this->del_extra_finance_field; 	}
	public function getDeliverableFinanceFields() {			return $this->del_headings;		}

	
	public function getExtraObjectFormJS() {	return $this->js; }
	
	public function getNonEditFields() { return $this->non_edit_fields; }
	public function checkEditStatusOfField($f) { return in_array($f,$this->non_edit_fields); }
	
	
	
	public function getActiveStatusSQL($t="",$tbl_field_prefix="") {
		//Contracts where 
			//status = active and
			//status <> deleted
			//echo "<H2>".$t." = ".$tbl_field_prefix."</h2>";
		return $this->getStatusSQL("ALL",$t,false,false,$tbl_field_prefix);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Function to get list of records for EDIT list table
	 * @param (Int) $parent_object_id = contract id
	 * @param (String) $section = which section of the module: MANAGE || ADMIN
	 * 
	 * @return (array) ('head'=>array of headings,'rows'=>array of records,'paging'=>paging array) 
	 */
	public function getListObjects($parent_object_id,$section="MANAGE") {
		$displayObject = new CNTRCT_DISPLAY_FINANCE();
		$listObject = new CNTRCT_LIST();
		
		$ref_tag = $this->getRefTag();
		$data = array(
			'head'=>array(),
			'rows'=>array(),
			'paging'=>array(
				'totalrows' => 0,
				'totalpages' => 0,
				'currentpage' => 0,
				'pagelimit' => 0,
				'first' => 0,
				'prev' => 0,
				'next' => 0,
				'last' => 0,			
			)
		);
		
		//HEADINGS
		$headObject = new CNTRCT_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(),"LIST",$section);
		$data['head'] = $this->replaceObjectNames($headings);
		
		//SQL
		$sql = "SELECT O.* ";
		$left_joins = array();
		foreach($headings as $fld=>$head) {
				$lj_tblref = "O";
				if($head['type']=="LIST") {
					$listObject->changeListType($head['list_table']);
					$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
										AND (".$head['list_table'].".status & ".CNTRCT::DELETED.") <> ".CNTRCT::DELETED;
				} elseif($head['type']=="MASTER") {
					$tbl = $head['list_table'];
					$masterObject = new CNTRCT_MASTER($fld);
					$fy = $masterObject->getFields();
					$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
				} elseif($head['type']=="OWNER") {
					$ownerObject = new CNTRCT_CONTRACT_OWNER();
					$tbl = $head['list_table'];
					$dir_tbl = $fld."_parent";
					$sub_tbl = $fld."_child";
					$own_tbl = $fld;
					$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." ON ".$lj_tblref.".".$fld." = ".$sub_tbl.".subid";
					$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
					$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." AS ".$own_tbl." ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
				}
		}
		$sql.= " FROM ".$this->getTableName()." O 
				".(count($left_joins)>0 ? implode(" ",$left_joins) : "")."
				WHERE O.".$this->getParentFieldName()." = ".$parent_object_id." 
				AND (O.".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		
		//FORMATTING
		foreach($rows as $key => $r) {
			$row = array();
			foreach($headings as $fld=>$head) {
				$type = $head['type'];
						if($headObject->isListField($head['type'])) {
							$row[$fld] = $r[$head['list_table']];
						} elseif($this->isDateField($fld) || $head['type']=="DATE") {
							$row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
						} else {
							$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
						}
			}
			$data['rows'][$key] = $row;
		}
		
		
		
		//END
		return $data;
	}
		
	
	
	
	
	
	
	/**
	 * Function to return raw data from the database
	 * @param (Int) $parent_object_id = contract id
	 * @param (String) $result_format = DEFAULT (as is from the database) || "SUMMARY" (SUM of primary finance field)
	 * 
	 * @return (Array)  mysql_fetch_all
	 */
	public function getRawRows($parent_object_id,$result_format = "DEFAULT", $get_secondary=false) {
		switch($result_format) {
			case "SUMMARY":
				if(!$get_secondary) {
					$sql = "SELECT IF(SUM(".$this->getPrimaryFinanceFieldName().")<>0,SUM(".$this->getPrimaryFinanceFieldName()."),0) as ".$this->getPrimaryFinanceFieldName();
				} else {
					$sql = "SELECT IF(SUM(".$this->getSecondaryFinanceFieldName().")<>0,SUM(".$this->getSecondaryFinanceFieldName()."),0) as ".$this->getSecondaryFinanceFieldName();
				}
				$sql_postfix = "";
				break;
			case "DEFAULT":
			default:
				$sql = "SELECT * ";
				$sql_postfix = "";
		}
		$sql.= " FROM ".$this->getTableName()
				." WHERE ".$this->getParentFieldName()." = ".$parent_object_id
				." AND (".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." ".$sql_postfix;
		$data = $this->mysql_fetch_all($sql);
		return $data;
	}
	
	
	public function getRawObject($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()
				." WHERE ".$this->getIDFieldName()." = ".$object_id
				." AND (".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	
	
	/****************************
	 * DELIVERABLE RELATED FUNCTIONS
	 */
	
	/**
	 * Function to return raw data from the database
	 * @param (Int) $parent_object_id = contract id
	 * @param (String) $result_format = DEFAULT (as is from the database) || "SUMMARY" (SUM of primary finance field)
	 * 
	 * @return (Array)  mysql_fetch_all
	 */
	public function getDeliverableRawRows($parent_object_id,$result_format = "DEFAULT", $get_for_secondary=false) {
		switch($result_format) {
			case "SUMMARY":
				if($get_for_secondary) {
					$sql = "SELECT IF(SUM(".$this->getDeliverableSecondaryFinanceFieldName().")>0,SUM(".$this->getDeliverableSecondaryFinanceFieldName()."),0) as ".$this->getDeliverableSecondaryFinanceFieldName();
				} else {
					$sql = "SELECT IF(SUM(".$this->getDeliverablePrimaryFinanceFieldName().")>0,SUM(".$this->getDeliverablePrimaryFinanceFieldName()."),0) as ".$this->getDeliverablePrimaryFinanceFieldName();
				}
				$sql_postfix = "";
				break;
			case "DEFAULT":
			default:
				$sql = "SELECT del.* ";
				$sql_postfix = "";
		}
		$delObject = new CNTRCT_DELIVERABLE();
		$sql.= " FROM ".$this->getDeliverableTableName()." as del 
				 INNER JOIN ".$this->getTableName()." as b
				     ON del.".$this->getDeliverableParentFieldName()." = b.".$this->getIDFieldName()."
				     AND b.".$this->getParentFieldName()." = ".$parent_object_id
				." INNER JOIN ".$delObject->getTableName()." as obj_del 
				  ON del.".$this->getDeliverableTableField()."_".$delObject->getIDFieldName()." = obj_del.".$delObject->getIDFieldName()
				  ." AND obj_del.".$delObject->getParentFieldName()." = b.".$this->getParentFieldName()
				." WHERE (b.".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." AND (del.".$this->getDeliverableStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." AND (obj_del.".$delObject->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." ".$sql_postfix; //echo $sql;
		$data = $this->mysql_fetch_all($sql);
		return $data;
	}
	 	
	
	










	/** 
	 * Function to determine the data to be displayed in the Finance Summary table at a deliverable level
	 * @param (INT) $parent_object_id = Contract ID
	 * 
	 * @return (Array)  
	 */
	public function getDeliverableSummaryData($parent_object_id,$is_edit=false,$edit_id=0,$get_edit_values=false) {
		$delObject = new CNTRCT_DELIVERABLE();
		$sql = "
			SELECT D.del_id as id, SUM(DF.".$this->getDeliverablePrimaryFinanceFieldName().") as ".$this->getDeliverablePrimaryFinanceFieldName()." 
			".(strlen($this->getDeliverableSecondaryFinanceFieldName())>0 ? ", SUM(DF.".$this->getDeliverableSecondaryFinanceFieldName().") as ".$this->getDeliverableSecondaryFinanceFieldName()."" : "")."
			FROM `".$this->getDeliverableTableName()."` DF
			INNER JOIN ".$delObject->getTableName()." D
			  ON D.".$delObject->getIDFieldName()." = DF.".$this->getDeliverableTableField()."_".$delObject->getIDFieldName()." 
			  AND D.".$delObject->getParentFieldName()." = ".$parent_object_id." 
			INNER JOIN ".$this->getTableName()." F
			  ON DF.".$this->getDeliverableParentFieldName()." = F.".$this->getIDFieldName()."
			WHERE 
			  ".$this->getActiveStatusSQL("DF",$this->getDeliverableTableField())."
			AND
			  ".$this->getActiveStatusSQL("F")."
			AND
			  ".$delObject->getActiveStatusSQL("D")."
			".(
				$is_edit ? " AND ".$this->getIDFieldName()." ".($get_edit_values ? "=" : "<>")." ".$edit_id : ""
			)."
			GROUP BY D.".$delObject->getIDFieldName()."
		";
		
		return $this->mysql_fetch_all_by_id($sql, "id");
	}

	






	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>