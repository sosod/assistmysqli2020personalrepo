<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class CNTRCT_DELIVERABLE_DRAWDOWN extends CNTRCT {
    /*************
     * CONSTANTS
     */
    const TABLE = "deliverable_drawdown";
    const TABLE_FLD = "dd";
	
	
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}
	
    public function getTableField() { return self::TABLE_FLD; }
	
	public function addObject() {

		return $id;
	}

	public function editObject() {

		return $mar;
	}
	
	public function deactivateObject() {

		return $mar;
	}
	
	
	
	
	

	public function getObjectByParentID( ) {

	}
	public function getObjectForSelectByParentID( ) {


	}
	public function getRawObjectByParentID( ) {


	}

}  
?>