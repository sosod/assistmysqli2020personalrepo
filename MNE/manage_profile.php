<?php
require_once("inc_header.php");

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
$save_activity = $helper->getActivityName("SAVE");
$active_name = $helper->getObjectName("ACTION");
$active_names = $helper->getObjectName("ACTION",true);
$log_object = new CNTRCT_LOG();
$pro_object = new CNTRCT_PROFILE();

$sess_pro = $_SESSION[$helper->getModRef()]['profile'];

$findb = "assist_".$helper->getCmpCode()."_master_financialyears";
$sql = "SELECT * FROM $findb WHERE (status && ".CNTRCT::ACTIVE." = ".CNTRCT::ACTIVE.")";
$years = $helper->mysql_fetch_all($sql);

$strt_dy = $pro_object->getProfileFnYrStartDay();

?>

<div id="hint_box" style="padding:8px; width:170px; background-color:#25bb25; border:1px solid #66cf66; display:none; position:fixed; top:25px; right:10px; z-index:99;">
	<h2 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:normal; color:white">Information</h2>
	<p style="font-family:Verdana, Arial, Helvetica, sans-serif;"></p>
</div>

<table class='tbl-container'>
	<table class='form'>
		<tr>
			<th>Financial Year Start Day:</th>
			<td>
			<form id='yr_strt_frm'>
				<?php
					//drawFormField(type, then options, then value)
					$js.= $displayObject->drawFormField("DATE",array("name"=>"yr_strt","id"=>"yr_strt_pck","options"=>array("dateFormat"=>"'dd-M'","changeYear"=>"false")),$strt_dy);
				?>
			</form>
			</td>
			<td>
				<input type=button id='yr_strt_btn' value='<?php echo $save_activity; ?>' />
			</td>
		</tr>
		<tr>
			<th>Default Financial Year:</th>
			<form id='yr_frm'>
			<td>
				<select name='financial_year' id='yr_sel'>
					<?php
						foreach($years as $key){
							echo"<option ";
							echo isset($sess_pro['financial_year'])&&$sess_pro['financial_year']==$key['id']?"selected":"" ;
							echo" value=".$key['id'].">".$key['value']."</option>";
						}
					?>
				</select>
			</td>
			<td>
				<input type=button id='yr_btn' value='<?php echo $save_activity; ?>' />
			</td>
			</form>
		</tr>
		<tr>
			<th>Preferred List Length:</th>
			<form id='lst_frm'>
			<td>
				<select name='table_rows' id='lst_sel'>
					<?php
						for($i=0;$i<16;$i++){
							echo"<option ";
							echo isset($sess_pro['table_rows'])&&$sess_pro['table_rows']==$i?"selected":"" ;
							echo " value= $i > $i </option>";
						}
					?>
				</select> Items
			</td>
			<td>
				<input type=button id='lst_btn' value='<?php echo $save_activity; ?>' />
			</td>
			</form>
		</tr>
		<tr>
			<th>Personal Notifications:</th>
			<td>Set up interactive reminder emails</td>
			<td><a href='manage_profile_notifications.php'><input id='prsnl_btn' hint='Intelligent notifications and regular reminders about system events' type=button value='Set up' /></a></td>
		</tr>
		<tr>
			<th></th>
			<td colspan=2><input style='float:right' id='prsnl_btn_3' type=button hint='This will save the Financial Year start day, the default Financial Year, and the List Length together. Personal Notifications are saved separately.' value='Save All' /></td>
		</tr>
	</table>
</table>
<?php $js.= $displayObject->drawPageFooter("","userprofile",array("section"=>'basic')); ?>

<style></style>

<script type="text/javascript">
	$("#yr_strt_btn").click(function(){
		var dta = $("#yr_strt_frm").serialize();
	//	console.log(dta);
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Approve", dta);
		if(result[0] != "ok"){
        	AssistHelper.finishedProcessing(result[0],result[1]);
		}else{
			document.location.href = "manage_profile.php?r[0]="+result[0]+"&r[1]="+result[1];
		}
	//	console.log(result);
	});
	
	$("#yr_btn").click(function(){
		var dta = $("#yr_frm").serialize();
	//	console.log(dta);
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Approve", dta);
		if(result[0] != "ok"){
        	AssistHelper.finishedProcessing(result[0],result[1]);
		}else{
			document.location.href = "manage_profile.php?r[0]="+result[0]+"&r[1]="+result[1];
		}
	//	console.log(result);
	});
	
	$("#lst_btn").click(function(){
		var dta = $("#lst_frm").serialize();
//		console.log(dta);
//		console.log($("#lst_frm"));
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Approve", dta);
//		console.log(result);
		if(result[0] != "ok"){
        	AssistHelper.finishedProcessing(result[0],result[1]);
		}else{
			document.location.href = "manage_profile.php?r[0]="+result[0]+"&r[1]="+result[1];
		}
	});
	
	$("#prsnl_btn_3").click(function(){
		var dta = $("#yr_frm, #lst_frm, #yr_strt_frm").serialize();
//		console.log(dta);
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Approve", dta);
//		console.log(result);
		if(result[0] != "ok"){
	    	AssistHelper.finishedProcessing(result[0],result[1]);
		}else{
			document.location.href = "manage_profile.php?r[0]="+result[0]+"&r[1]="+result[1];
		}
		console.log(result);
	});
	
	$("* [type='button']").hoverIntent({
		over:function(){
			var hint = $(this).attr("hint");
			if(hint){
				AssistHelper.showHelp(hint);
				//$("#hint_box").css("box-shadow", "2px 2px 5px #777777");
			}
		}, 
		out:function(){
			AssistHelper.hideHelp(30);
		},
		timeout: 50,
		interval: 130,
		sensitivity: 4
	}).click(function(){
			AssistHelper.hideHelp(5);
	});

<?php
	echo $js;
?>
</script>