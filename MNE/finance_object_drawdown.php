<?php

$tab = strtoupper($_REQUEST['tab']);

$form_object_type = "FINANCE_".$tab;
$object_type = $tab;

$formObject = new CNTRCT_FINANCE_DRAWDOWN();

$form_object_id = 0;
$parent_object_type = "CONTRACT";
$parentObject = new CNTRCT_CONTRACT();
$parent_object_id = $object_id;
$page_action = "ADD";
$page_redirect_path = $page_redirect_path."&tab=".strtolower($tab)."&";

$edit_list_objects = $formObject->getListObjects($parent_object_id);

$listObject = new CNTRCT_LIST("deliverable_category");
$cate_items = $listObject->getListItems();
if(!isset($cate_items[0])) {
	$unspecified_item = array(array('id'=>0,'name'=>$helper->getUnspecified()));
	$cate_items = array_merge($unspecified_item,$cate_items);
}
//$helper->arrPrint($cate_items);

$delObject = new CNTRCT_DELIVERABLE();
$add_deliverables = $delObject->getCategoryGroupedOrderedObjects($parent_object_id);
//$del_ids = array_keys($add_deliverables['main']);
$heading_fields = $delObject->getSummaryFields();
$hf = array_flip($heading_fields);
unset($heading_fields[$hf['del_category_id']]);

$form_items = $formObject->getAllItems($parent_object_id);
$del_drawdowns = $formObject->getDeliverableDrawDowns($parent_object_id);

$headObject = new CNTRCT_HEADINGS();
$del_headings = $headObject->getMainObjectHeadings("DELIVERABLE","ALL","MANAGE","",true,$heading_fields);
$del_headings = $del_headings['rows'];


//$delObject->arrPrint($heading_fields);
?>
<div id="accordion" class=accordion>
	<h3 id=head_initiate next_index=1>Create</h3>
	<div id=initiate>
		<?php ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); ?>
		<table class=tbl-container>
			<tr>
				<td width=48%>
					<?php
					$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					$js.=$contract['js'];
					//$summary_table = $displayObject->getSummaryTable($form_object_type, $formObject, $parent_object_id);
					//echo $summary_table['display'];
					//$js.=$summary_table['js'];
					?>
				</td>
				<td width=4%>
				</td>
				<td width=48%><h2><?php echo $helper->getActivityName("add")." ".$helper->getObjectName($form_object_type); ?></h2>
					<?php
					$form_display = $displayObject->getFinanceObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path);
					echo $form_display['display'];
					$js.=$form_display['js'];
					$end_display = $form_display['end_display'];

					echo "<h2>".$helper->getDeliverableObjectName()."</h2>
					
					<table id=tbl_finance_drawdown_deliverables class=drawdown-list width=100%>
						<tr>";
					foreach($del_headings as $fld => $h) {
						echo "<th>".$h['name']."</th>";
					}
					echo "<th>".$helper->getObjectName($object_type)."</th></tr>";
					foreach($cate_items as $ci => $cate) {
						if(isset($add_deliverables['main'][$ci]) && count($add_deliverables['main'][$ci]) > 0) {
							echo "
								<tr>
									<th class=th2>".$cate['name']."</th>
								</tr>
									";
							foreach($add_deliverables['main'][$ci] as $i => $del) {
								//if($del['del_type']=="DEL") {
									echo "
									<tr>
										<td class='center b'>".$del['reftag']."</td>
										<td>".$del['name']."</td>
										<td class=center>".$del['responsible_person']."</td>
										<td class=center>".$displayObject->getDateForDisplay($del['deadline'])."</td>
										<td class=center>".(isset($del_drawdowns['del'][$i]) ? $form_items[$del_drawdowns['del'][$i]]['name'] : "<input type=checkbox name=dd[] value=".$i." />" )."</td>
									</tr>";
									if(isset($add_deliverables['subs'][$i]) && count($add_deliverables['subs'][$i])) {
										foreach($add_deliverables['subs'][$i] as $si => $sub) {
											echo "
											<tr class=sub>
												<td class='center b'>".$sub['reftag']."</td>
												<td>".$sub['name']."</td>
												<td class=center>".$sub['responsible_person']."</td>
												<td class=center>".$displayObject->getDateForDisplay($sub['deadline'])."</td>
												<td class=center>-</td>
											</tr>";
										}
									}
								/*} else {
									echo "
									<tr>
										<td class=b>".$del['reftag'].": ".$del['name']."</td>
										<td>0.00</td>
										<td>0.00</td>
										<td>0.00</td>
									</tr>";
									if(isset($add_deliverables[$i])) {
										foreach($add_deliverables[$i] as $ci => $cdel) {
											echo "
											<tr>
												<td class=i>&nbsp;&nbsp;+&nbsp;".$cdel['reftag'].": ".$cdel['name']."</td>
												<td>0.00</td>
												<td><input type=text value=0 size=15 /></td>
												<td>0.00</td>
											</tr>";
										}
									}
								}*/
							}
						}
					}
					echo "</table>".$end_display; 
?>
				</td>
			</tr>
			<tr>
				<td colspan=3>

				</td>
			</tr>
		</table>
	</div>

	<h3 id=head_edit next_index=0>Edit</h3>
	<div id=edit>
		<table class=tbl-container>
			<tr>
				<td width=48%>
					<?php
					//$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					//$js.=$contract['js'];
					?>

				</td>
				<td width=4%>
				</td>
				<td width=48%><?php
					//echo $summary_table['display'];
				?></td>
			</tr>
			<tr>
				<td colspan=3>
					<?php
					$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list"));
					?>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id=dlg_child class=dlg-child title="Edit">
		<iframe id=ifr_form_display style="border:0px solid #FFFFFF;" src="">
			
		</iframe>
</div>

<script type="text/javascript">
$(function() {	
	//$("h3#head_edit, div#edit").hide();
	$('#accordion').accordion({
		active: 0,
		event: false,
		heightStyle: 'fill'
	}).children('h3').css({'font-family':'Verdana','font-weight':'bold','font-size':'110%','line-height':'125%'});
	$('h3').click(function() {
		if($(this).parent().prop('id')=='accordion') {
			var next_index = $(this).attr('next_index');
			$relatedContent = $(this).next();
			if($relatedContent.is(':visible')) {
				if(next_index==0) {
					$("#accordion").accordion("option","active",0);
				} else {
					$("#accordion").accordion("option","active",1);
				}
			} else {
				if(next_index==0) {
					$("#accordion").accordion("option","active",1);
				} else {
					$("#accordion").accordion("option","active",0);
				}
			}
		}
	});
	$("table#tbl_finance_drawdown_deliverables th.th2").prop("colspan",$("table#tbl_finance_drawdown_deliverables tr:first th").length).css("text-align","left");
	$("table#tbl_finance_drawdown_deliverables tr.sub td").css("background-color","#efefef");

	
	var my_window = AssistHelper.getWindowSize();
	//alert(my_window['height']);
	//if(my_window['width']>1000) {
		//var my_width = 950;
	//} else {
		var my_width = my_window['width']*0.9;
	//}
	var my_height = my_window['height']-50;	

	var my_screen = AssistString.substr($("div#initiate").css("width"),0,-2)*1;
	var table_width = (AssistString.substr($("table.tbl_finance_deliverables:first").css("width"),0,-2))*1;
	var form_width = my_screen*0.47;
	if(form_width>table_width) {
		var div_width = form_width;
	} else {
		var div_width = table_width+3;
	}

	
	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			if($(this).prop("id") == "dlg_view_child") {
				$(this).dialog('option','width',my_view_width);
				$(this).children("iframe:first").css("width",(my_view_width-30)+"px");
			} else {
				$(this).dialog('option','width',my_width);
				$(this).children("iframe:first").css("width",(my_width-20)+"px");
			}
			$(this).dialog('option','height',my_height);
			$(this).children("iframe:first").css("height",(my_height-50)+"px");
		},
	});	
		
	$(".btn_list").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		$dlg = $("#dlg_child");



		var i = $(this).attr("ref");
		var dta = "object_type=DELIVERABLE&object_id="+i;
		var act = "edit";
		var obj = "deliverable";
		var heading = "<?php echo $helper->getActivityName("edit"); ?>";
		var url = "finance_object_form.php?display_type=dialog&object_id=<?php echo $parent_object_id; ?>&tab=<?php echo $tab; ?>&form_action=edit&edit_object_id="+i;
		
		//$dlg.prop("title","<?php $helper->getActivityName("edit"); ?>");
		$dlg.dialog("option","title",heading);
console.log(url);
		$dlg.find("iframe:first").prop("src",url);
		//$dlg.dialog("open");
		//AssistHelper.closeProcessing();
	});
	
	
	
	
});
</script>