<?php
$page_redirect_path = "new_confirm.php?";
include("inc_header.php");


$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
$object_id = $_REQUEST['object_id'];

switch($object_type) {
	case "CONTRACT":
		$myObject = new CNTRCT_CONTRACT();
		$childObject = new CNTRCT_DELIVERABLE();
		//$child_objects = $childObject->getObject(array('type'=>"ALL",'section'=>"NEW",'contract_id'=>$object_id,'limit'=>false,'page'=>"CONFIRM")); 
		$child_objects = $childObject->getOrderedObjects($object_id);
		$child_redirect = "manage_view_object.php?object_id=";
		$child_name = $helper->getDeliverableObjectName(true);
		break;
}
/*	case "DELIVERABLE":
		$myObject = new CNTRCT_DELIVERABLE();
		$childObject = new CNTRCT_ACTION();
		$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'deliverable_id'=>$object_id)); 
		$child_redirect = "new_confirm_details.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName(true);
		break;
	case "ACTION":
		$myObject = new CNTRCT_ACTION();
		$child_redirect = "";
		break;
*/

?>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,false,array('value'=>"View",'class'=>"btn_view contract")); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<?php if($object_type == "CONTRACT") { ?>
			<h2>Summary of <?php echo $helper->getContractObjectName()." ".$myObject->getRefTag().$object_id; 
				$summ = $myObject->getSummary($object_id);
				//ASSIST_HELPER::arrPrint($summ); 
				?></h2>
			<table id=tbl_summ>
				<tr>
					<th width=200px></th>
					<th width=100px>Count</th>
					<th width=100px>Progress</th>
				</tr>
				<tr>
					<th>Deliverables:</th>
					<td><?php echo $summ['DEL']['deliverable']; ?></td>
					<td class=right><?php echo ASSIST_HELPER::format_percent($summ['DEL']['del_prog']); ?></td>
				</tr><tr>
					<th>Deliverable Actions:</th>
					<td><?php echo $summ['DEL']['action']; ?></td>
					<td class=right><?php echo ASSIST_HELPER::format_percent($summ['DEL']['action_prog']); ?></td>
				</tr><tr>
					<th>Parent Deliverables:</th>
					<td><?php echo $summ['MAIN']['deliverable']; ?></td>
					<td class=right>N/A</td>
				</tr><tr>
					<th>Sub Deliverables:</th>
					<td><?php echo $summ['SUB']['deliverable']; ?></td>
					<td class=right><?php echo ASSIST_HELPER::format_percent($summ['SUB']['del_prog']); ?></td>
				</tr><tr>
					<th>Sub Deliverable Actions:</th>
					<td><?php echo $summ['SUB']['action']; ?></td>
					<td class=right><?php echo ASSIST_HELPER::format_percent($summ['SUB']['action_prog']); ?></td>
				</tr>
			</table>
			<?php } ?>
		</td>
	</tr>
	<?php  
	if(isset($childObject)) { ?>
	<tr>
		<td colspan=3>
			<hr />
			<h2><?php echo $child_name; ?></h2>
			<hr />
			<?php 	$gchildObject = new CNTRCT_ACTION();
					$gchild_redirect = "new_confirm_details.php?object_type=ACTION&object_id=";
					$gchild_name = $helper->getActionObjectName(true);

			foreach($child_objects[0] as $key=>$item){
				//1 detail display per deliverable
				$js.=$displayObject->drawDetailedView("DELIVERABLE", $key, false,false,false,($item['del_type']!="MAIN"?array('value'=>"View",'class'=>"btn_view deliverable"):array()));
				if($item['del_type'] !== "MAIN"){
					echo "<h3 style='margin-bottom: 5px;'>".$gchildObject->getObjectName("ACTION",true)."</h3>";
					$gchild_objects = $gchildObject->getObject(array('type'=>"ALL",'section'=>"MANAGE",'deliverable_id'=>$key,'limit'=>false,'page'=>"ALL")); 
					$js.=$displayObject->drawListTable($gchild_objects,array('value'=>"View",'class'=>"btn_view action",'pager'=>false));
				}else{
					if(isset($child_objects[$key])){
						foreach($child_objects[$key] as $key2=>$item){
							$js.=$displayObject->drawDetailedView("DELIVERABLE", $key2, false, true,false,array('value'=>"View",'class'=>"btn_view deliverable"));
							echo "<h3 class='sub_head'>".$gchildObject->getObjectName("ACTION",true)."</h3>";
							$gchild_objects = $gchildObject->getObject(array('type'=>"ALL",'section'=>"MANAGE",'deliverable_id'=>$key2,'limit'=>false,'page'=>"ALL")); 
							$js.=$displayObject->drawListTable($gchild_objects,array('value'=>"View",'class'=>"btn_view action",'pager'=>false), "", array(), true);
						}
					}
				}
			}	
					//All actions of said deliverable
/*
					foreach($gchild_objects['rows'] as $index=>$item){
						//$js.=$displayObject2->drawDetailedView("ACTION", $index, false);
					}
	
 * 
 */				
			?>
		</td>
	</tr>
	<?php }  ?>
</table>
<script type="text/javascript">
var url = "<?php echo $child_redirect; ?>";
$(function() {
	<?php echo $js; ?>
	$("h2").css({"margin-top":"10px","margin-bottom":"5px"});
	$("#tbl_summ th").addClass("th2");
	$("#tbl_summ tr:first th:first").css("background-color","#ffffff");
	$("#tbl_summ tr:gt(0) th").css("text-align","left");
	$("#tbl_summ tr:gt(0)").each(function() { $(this).find("td:eq(0)").css("text-align","center"); });
	$("input:button.btn_view").click(function() {
		var i = $(this).attr("ref");
		url = url+i;
		if($(this).hasClass("action")){
			url = url + "&object_type=ACTION";
		}else if($(this).hasClass("deliverable")){
			url = url + "&object_type=DELIVERABLE";
		}else{
			url = url + "&object_type=CONTRACT";
		}
		document.location.href = url;
	});
});
</script>