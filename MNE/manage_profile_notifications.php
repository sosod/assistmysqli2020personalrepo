<?php
require_once("inc_header.php");
echo"<h2>Personal Notifications</h2>";
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
$save_activity = $helper->getActivityName("SAVE");
$edit_activity = $helper->getActivityName("EDIT");
$add_activity = $helper->getActivityName("ADD");
$deact_activity = $helper->getActivityName("DEACTIVATE");
$react_activity = $helper->getActivityName("RESTORE");
$delete_activity = $helper->getActivityName("DELETE");
$active_name = $helper->getObjectName("ACTION");
$deliverable_name = $helper->getObjectName("DELIVERABLE");
$contract_name = $helper->getObjectName("CONTRACT");
$active_names = $helper->getObjectName("ACTION",true);
$log_object = new CNTRCT_LOG();
$pro_object = new CNTRCT_PROFILE(array(),array(),array(),array(),array(),false,"RULE");

$data = $pro_object->getNotes();

$note_whats = $pro_object->getNoteWhats();
$note_whens = $pro_object->getNoteWhens();
$note_everies = $pro_object->getNoteEveries();
//ASSIST_HELPER::arrPrint($note_whats);
//ASSIST_HELPER::arrPrint($note_whens);
//ASSIST_HELPER::arrPrint($note_everies);
?>
<!--- Hint-box ---->
<div id="hint_box" 
style="padding:8px; width:170px; background-color:#25bb25; border:1px solid #66cf66; display:none; position:fixed; top:25px; right:10px; z-index:99;">
	<h2 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:normal; color:white">Information</h2>
	<p style="font-family:Verdana, Arial, Helvetica, sans-serif;"></p>
</div>

<table class=form>
		<form id='prsnl_frm_2'>
			<tr>
				<th>
					ID
				</th>
				<th>
					Name
				</th>
				<th>
					Section
				</th>
				<th>
					What
				</th>
				<th>
					When
				</th>
				<th>
					Frequency
				</th>
				<th>
					Active
				</th>
				<th>
					Created (<a href='#'>Log</a>)
				</th>
				<th>
					
				</th>
			</tr>
				<?php
					foreach($data as $thing){
						if($thing['un_status']==CNTRCT::INACTIVE){
							echo"<tr style='background-color:#e6e6e6'>";
						}else{
							echo"<tr>";
						}
						foreach($thing as $key=>$val)
							if($key !== "un_tkid" && $key !== "un_insertuser" && $key !== "un_custom"){
								$custom=isset($thing['un_custom'])&&strlen($thing['un_custom'])>0?$thing['un_custom']:"false";
								if($key == "un_section" ||  $key == "un_when"){
									echo"<td num='$val'>".$pro_object->getSections($key,$val)."</td>";
								}else if($key == "un_what"){
									echo"<td num='$val'>".$pro_object->getSections($key,$val,$thing['un_section'])."</td>";
								}else if($key == "un_every"){
									if($pro_object->getSections($key,$val,$thing['un_when']) == "Custom"){
										echo"<td num='$val' cust='$custom' >".$custom."</td>";
									}else{
										echo"<td num='$val' cust='$custom' >".$pro_object->getSections($key,$val,$thing['un_when'])."</td>";
									}
								}else if($key == "un_status"){
									switch($val){
										case CNTRCT::ACTIVE:
											echo"<td style='color:#25bb25'>Active</td>";
											break;
										case CNTRCT::INACTIVE:
											echo"<td style='color:#dd1920'>Inactive</td>";
											break;
									}
								}else{
									echo"<td>".$val."</td>";
								}
								$tbl[$thing['un_id']][]=$val;
							}
						if($thing['un_status']==CNTRCT::INACTIVE){
							echo"<td><input type=button id='react_".$thing['un_id']."' class='react_btn' value='".$react_activity."' /></td>";
						}else{
							echo"<td><input type=button id='edit_".$thing['un_id']."' class='edit_btn' value='".$edit_activity."' /></td>";
						}
						echo"</tr>";
					}
				?>
			<tr>
				<td style='background-color:#eeeeee'></td>
				<td colspan=8><input style='float:right' type=button id='add_btn' value='<?php echo $add_activity; ?> personal reminder' /></td>
			</tr>
		</form>
	</table>
	
<div id='edit_frm' style='display:none'>
	<table>
		<?php
	//		ASSIST_HELPER::arrPrint($tbl);
			foreach($tbl as $key=>$val){
				echo"<form id='sect_sel_".$key."'><table id='t_".$key."'>
						<tr><th>ID</th><td>$val[0]<input type='hidden' name='id' value='$val[0]' /></td></tr>
						<tr><th>Name</th><td><input type='text' name='name' autofocus onclick='$(this).select()' value='$val[1]' /></td></tr>
						<tr><th>Section</th><td><select class='sect_sel' name='sect_sel'>";
				echo"	
							<option value=0>".$helper->getObjectName("ACTION")."</option>
							<option value=1>".$helper->getObjectName("DELIVERABLE")."</option>
							<option value=2>".$helper->getObjectName("CONTRACT")."</option>
					";
				echo"	</select></td></tr>
						<tr><th>What</th><td><select class='what_sel' name='what_sel'>";
				foreach($note_whats as $key=>$val){
					foreach($val as $index=>$item){
						echo"<option class='".$key."' value=".$index.">".$item."</option>";
					}
				}
				echo"		
						</select></td></tr>
						<tr><th>When</th><td><select class='when_sel' name='when_sel'>";
						
				foreach($note_whens as $key=>$val){
						echo"<option value=".$key.">".$val."</option>";
				}
						echo"</select></td></tr>
						<tr class='when_proc'><th>On every</th><td><select class='every_sel' name='every_sel'>";
						
				foreach($note_everies as $key=>$val){
						foreach($val as $index=>$thing){
							echo"<option class=".$key." value=".$index.">".$thing."</option>";
						}	
				}						
						echo"</select></td></tr><tr class='year_proc'><th>On every</th><td><div class='datepick'></div><input hidden type='text' name='edit_date' class='edit_date' /></td></tr>
					</table></form>";
			}
		?>
	</table>
</div>	

<div id='add_div' style='display:none'>
	<table>
				<form id='add_frm'><table id='t_add'>
						<tr><th>ID</th><td>Automatic</td></tr>
						<tr><th>Name</th><td><input type='text' name='name' value=''  /></td></tr>
						<tr><th>Section</th><td><select class='sect_sel' name='sect_sel'>";
					<?php
					echo"
							<option value=0>".$helper->getObjectName("ACTION")."</option>
							<option value=1>".$helper->getObjectName("DELIVERABLE")."</option>
							<option value=2>".$helper->getObjectName("CONTRACT")."</option>
					";
				echo"	</select></td></tr>
						<tr><th>What</th><td><select class='what_sel' name='what_sel'>";
				foreach($note_whats as $key=>$val){
					foreach($val as $index=>$item){
						echo"<option class='".$key."' value=".$index.">".$item."</option>";
					}
				}
				echo"		
						</select></td></tr>
						<tr><th>When</th><td><select class='when_sel' name='when_sel'>";
						
				foreach($note_whens as $key=>$val){
						echo"<option value=".$key.">".$val."</option>";
				}
						echo"</select></td></tr>
						<tr class='when_proc'><th>On every</th><td><select class='every_sel' name='every_sel'>";
						
				foreach($note_everies as $key=>$val){
						foreach($val as $index=>$thing){
							echo"<option class=".$key." value=".$index.">".$thing."</option>";
						}	
				}						
						echo"</select></td></tr><tr class='year_proc'><th>On every</th><td><div class='datepick'></div><input hidden type='text' name='add_date' id='add_date' /></td></tr>
					</table></form>";
			
		?>
	</table>
</div>	
<?php $js.= $helper->drawPageFooter($helper->getGoBack('manage_profile.php'),"userprofile",array("section"=>'note')); ?>
<div id="react_div" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to <?php echo $react_activity; ?> this notification? It will be triggered appropriately and you will receive notification emails.</p>
</div>
<div id="deact_div" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to <?php echo $deact_activity; ?> this notification? It will no longer send you notification emails, but you can <?php echo $react_activity; ?> it.</p>
</div>
<div id="delete_div" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to <?php echo $delete_activity; ?> this notification? It will be destroyed, and will no longer send you notification emails.</p>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".when_proc").hide();		
		$(".year_proc").hide();		
	});
	
	$(".react_btn").click(function(){
		var rid = $(this).prop("id");
		var id = rid.split("_");
		var i = id[1];
		$("#react_div").show().dialog({
	      resizable: false,
	      modal: true,
	      width: 450,
	      buttons: {
	        '<?php echo $react_activity; ?>': function() {
	        	AssistHelper.processing();
				var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Restore&id="+i);
				if(result[0] != "ok"){
		        	AssistHelper.finishedProcessing(result[0],result[1]);
				}else{
					document.location.href = "manage_profile_notifications.php?r[0]="+result[0]+"&r[1]="+result[1];
				}
		//		console.log(result);
	          $(this).dialog("close");
	        },
	        Cancel: function() {
	          $(this).dialog("close");
	        }
	      }
	    });
	});
	
	$("#add_btn").click(function(){
		$("#add_div .datepick").datepicker({
			altField: "#add_date",
			altFormat: "yy-mm-dd",
		});
		$(".year_proc").hide();
		$(".what_sel option").each(function(){
			var oid = $(this).prop("class");
			if(oid != "0"){
				$(this).hide();
			}else{
				$(this).show();
				if($(this).val()=='0'){
					$(this).attr("selected","selected");
				}
			}
		});
		$("#add_div").dialog({
			height: 495,
		    width: 488,
		    modal: true,
		    resizable: false,
		    title: 'Create a Notification',
		    buttons: {
		      '<?php echo $add_activity; ?>': function(){
				AssistHelper.processing();
		    	var serial = $("#add_frm").serialize();
				var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Add", serial);
				if(result[0] != "ok"){
		        	AssistHelper.finishedProcessing(result[0],result[1]);
				}else{
					document.location.href = "manage_profile_notifications.php?r[0]="+result[0]+"&r[1]="+result[1];
				}
				//console.log(result);
		        $(this).dialog( "close" );
				//console.log(serial);
		      },
		      Cancel: function() {
		        $(this).dialog( "close" );
		      }
		    }
		});
		AssistHelper.formatDialogButtons($("#add_div"),0,AssistHelper.getGreenCSS());
	});
	
	$(".edit_btn").click(function(){
		var edit_id = $(this).prop("id");
		var id = edit_id.split("_");
		var i = id[1];
		$("#t_"+i+" .datepick").datepicker({
			altField: ".edit_date",
			altFormat: "yy-mm-dd",
		});
		$(".year_proc").hide();	
		$("#edit_frm table").each(function(){
			var table_id = $(this).prop("id");
			var tab_id = table_id.split("_");
			var tab_i = tab_id[1];
			if(tab_i != i){
				$(this).hide();
			}else{
				$(this).show();
			}			
		});
		$("#t_"+i+" .every_sel").attr("size","5");
		//Make the currently selected values show up
		var tds = $(this).parent("td").siblings();
		var old_data = [];
		var custom_date = [];
		tds.each(function(){
			$td = parseInt($(this).attr("num"));
			var jesus = $(this).attr("cust");
			if(isNaN($td)){
				old_data.push($(this)[0].innerHTML);
			}else{
				old_data.push($td);
			}
			if(jesus != undefined && jesus != 'false'){
				custom_date.push(jesus);
			}
		});
	//	console.log(custom_date);
		$("#t_"+i+" .sect_sel").val(old_data[2]);
		//Hide and show the appropriate options for the 'what' select, based on the 'section' selected
		$("#t_"+i+" .what_sel option").each(function(){
			if($(this).attr("class")==old_data[2]){
		//	console.log("This item's class is equal to the value in 'Section' column in table: class "+$(this).attr("class"));
				$(this).show();
					//+1?
				var value=parseInt(old_data[3]);
			//	console.log(value);
				if($(this).val()==value){
					$(this).attr("selected","selected");
				}else{
					$(this).removeAttr("selected");
				}
			}else{
				$(this).hide();
			}
		});
		$("#t_"+i+" .when_sel").val(old_data[4]);
		if(old_data[4] != 0){
			$("#t_"+i+" .when_proc").show();
			if(old_data[4] == 6){
				$(".year_proc").show();
				$("#t_"+i+" .when_proc").hide();
				var fdate = new Date(custom_date[0]);
			//	console.log(fdate);
				$("#t_"+i+" .datepick").datepicker( "setDate", fdate );
			}else{
				$(".year_proc").hide();
			}
		}else{
			$("#t_"+i+" .when_proc").hide();
		}
		//Hide and show the appropriate options for the 'on every' (frequency) select, based on the 'when' selected
		$("#t_"+i+" .every_sel option").each(function(){
			$(this).show();
			if($(this).attr("class")==old_data[4]){
				if($(this).val()==old_data[5]){
					$(this).attr("selected","selected");
				}else{
					$(this).removeAttr("selected");
				}
			}else{
				$(this).hide();
			}
		});
		
	
		$("#edit_frm").dialog({
			height: 495,
		    width: 488,
		    modal: true,
		    resizable: false,
		    title: 'Edit Notification '+i,
		    buttons: {
		      '<?php echo $edit_activity; ?>': function(){
				AssistHelper.processing();
		    	var serial = $("#sect_sel_"+i).serialize();
				var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Edit", serial);
				if(result[0] != "ok"){
		        	AssistHelper.finishedProcessing(result[0],result[1]);
				}else{
					document.location.href = "manage_profile_notifications.php?r[0]="+result[0]+"&r[1]="+result[1];
				}
		//		console.log(serial);
		//		console.log(result);
		        $(this).dialog( "close" );
		      },
		      '<?php echo $deact_activity; ?>': function(){
		      		$("#deact_div").show().dialog({
				      resizable: false,
				      modal: true,
				      width: 450,
				      buttons: {
				        '<?php echo $deact_activity; ?>': function() {
							AssistHelper.processing();
							var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Deactivate&id="+i);
							if(result[0] != "ok"){
					        	AssistHelper.finishedProcessing(result[0],result[1]);
							}else{
								document.location.href = "manage_profile_notifications.php?r[0]="+result[0]+"&r[1]="+result[1];
							}
							console.log(result);
				          $(this).dialog("close");
				        },
				        Cancel: function() {
				          $(this).dialog("close");
				        }
				      }
				    });
				AssistHelper.formatDialogButtons($("#deact_div"),0,AssistHelper.getNoteCSS());
		       // $(this).dialog( "close" );
		      },
		      '<?php echo $delete_activity; ?>': function(){
				$("#delete_div").show().dialog({
				      resizable: false,
				      modal: true,
				      width: 450,
				      buttons: {
				        '<?php echo $delete_activity; ?>': function() {
							AssistHelper.processing();
							var result = AssistHelper.doAjax("inc_controller.php?action=Profile.Delete&id="+i);
							if(result[0] != "ok"){
					        	AssistHelper.finishedProcessing(result[0],result[1]);
							}else{
								document.location.href = "manage_profile_notifications.php?r[0]="+result[0]+"&r[1]="+result[1];
							}
				//			console.log(result);
				          $(this).dialog("close");
				        },
				        Cancel: function() {
				          $(this).dialog("close");
				        }
				      }
				    });
				AssistHelper.formatDialogButtons($("#delete_div"),0,AssistHelper.getRedCSS());
		      //  $(this).dialog( "close" );
		      },
		      Cancel: function() {
		        $(this).dialog( "close" );
		        resetSelects();
		      },
		    }
		});
		AssistHelper.formatDialogButtons($("#edit_frm"),0,AssistHelper.getGreenCSS());
		AssistHelper.formatDialogButtons($("#edit_frm"),1,AssistHelper.getNoteCSS());
		AssistHelper.formatDialogButtons($("#edit_frm"),2,AssistHelper.getRedCSS());
	});
	
	
	$(".sect_sel").change(function(){
		var id = $(this).val();
		$(".what_sel option").each(function(){
			var oid = $(this).prop("class");
			if(oid != id){
				$(this).hide();
			}else{
				$(this).show();
				if($(this).val()=='1'){
					$(this).attr("selected","selected");
				}else{
					$(this).removeAttr("selected");
				}
			}
		});
	//	alert(id);
	});

	$(".when_sel").change(function(){
		var id = $(this).val();
		var pid = $(this).parents("table").prop("id");
		var hint = "This setting allows you to choose a custom day of the year on which you will be reminded about the appropriate tasks on Action Assist. Note that if you choose a day like February 29th, the notifications will only be sent in leap years!";
		//alert(pid);
		if(id != "0"){
			//Could be softened to count the number of options with a certain class, and if zero - to hide the when_proc row
			$("#"+pid+" .when_proc").show();
			if(id == "6"){
				$(".when_proc").hide();			
				$(".year_proc").show();
				$(".datepick").datepicker("refresh");
				AssistHelper.showHelp(hint);
				setTimeout(function(){AssistHelper.hideHelp(30);}, 8500);
			}else{
				$(".year_proc").hide();
			}
		}else{
			$("#"+pid+" .year_proc").hide();
			$(".when_proc").hide();			
		}
		$("#"+pid+" .every_sel").attr("size","5");
		$("#"+pid+" .every_sel option").each(function(){
			var oid = $(this).prop("class");
			//console.log(oid);
			if(oid != id){
				$(this).hide();
			}else{
				$(this).show();
				if($(this).val()=='0'){
					$(this).attr("selected","selected");
					//$(this).css("color","orange");
				}else{
					$(this).removeAttr("selected");
				}
			}
		});
	});
	
		
	//Reset the values when closing a dialog
	function resetSelects(){
		$("select option").each(function(){
			$(this).removeAttr("selected").show();
		});
	}
	
	<?php echo $js; ?>
	
	$("* [type='button']").hoverIntent({
		over:function(){
			var hint = $(this).attr("hint");
			if(hint){
				AssistHelper.showHelp(hint);
				//$("#hint_box").css("box-shadow", "2px 2px 5px #777777");
			}
		}, 
		out:function(){
			AssistHelper.hideHelp(30);
		},
		timeout: 50,
		interval: 130,
		sensitivity: 4
	}).click(function(){
			AssistHelper.hideHelp(5);
	});
	
</script>
<style>
.suggested_hover_btn{
	border: 1px solid #364DA1;
	background: #dadada url(images/ui-bg_glass_75_dadada_1x400.png) 50% 50% repeat-x;
	font-weight: normal;
	color: #364DA1;
}
.suggested_gradient_general{
	/* Fallback */
	background: rgb(109,179,242);
	background: linear-gradient(to bottom, rgba(109,179,242,1) 0%,rgba(84,163,238,1) 50%,rgba(54,144,240,1) 51%,rgba(30,105,222,1) 100%);
}
</style>