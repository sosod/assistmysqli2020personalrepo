<?php
error_reporting(-1);
//Heading ("glossary") == auto from inc_header
require_once("inc_header.php");
$me = new ASSIST_HELPER();
$mod = new ASSIST_MODULE_HELPER();
$db = new ASSIST_DB();
$dbref = $mod->getDBRef()."_";
$listObj = new CNTRCT_LIST();

//Fetch the headings from the glossary
$sql = "SELECT * FROM ".$dbref."glossary WHERE field = 9999";
$custom_cats = $db->mysql_fetch_all_by_id($sql,"category");

//ASSIST_HELPER::arrPrint($custom_cats);

//SYSTEM DEFINED HEADINGS
	//Fetch all the info
	$sql = "SELECT * FROM ".$dbref."setup_headings";
	$cats = $db->mysql_fetch_all_by_id($sql,"h_field");
	
	//Split the glossary into headings
	foreach($cats as $key=>$val){
		if(stripos($val['h_section'],'CONTRACT')!==false){
			$gloss['contract'][$key]=$val;
		}else if(stripos($val['h_section'],'CONTRACT_UPDATE')!==false){
			$gloss['contract_update'][$key]=$val;
		}else if(stripos($val['h_section'],'DELIVERABLE')!==false){
			$gloss['deliverable'][$key]=$val;
		}else if(stripos($val['h_section'],'DELIVERABLE_UPDATE')!==false){
			$gloss['deliverable_update'][$key]=$val;
		}else if(stripos($val['h_section'],'ACTION')!==false){
			$gloss['action'][$key]=$val;
		}else if(stripos($val['h_section'],'ACTION_UPDATE')!==false){
			$gloss['action_update'][$key]=$val;
		}else if(stripos($val['h_section'],'ASSESSMENT')!==false){
			$gloss['assessment'][$key]=$val;
		}else if(stripos($val['h_section'],'CONTRACT_ASSURANCE')!==false){
			$gloss['contract_assurance'][$key]=$val;
		}else if(stripos($val['h_section'],'DELIVERABLE_ASSURANCE')!==false){
			$gloss['deliverable_assurance'][$key]=$val;
		}else if(stripos($val['h_section'],'ACTION_ASSURANCE')!==false){
			$gloss['action_assurance'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_BUDGET')!==false){
			$gloss['finance_budget'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_DELIVERABLE_BUDGET')!==false){
			$gloss['finance_deliverable_budget'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_ADJUSTMENT')!==false){
			$gloss['finance_adjustment'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_DELIVERABLE_ADJUSTMENT')!==false){
			$gloss['finance_deliverable_adjustment'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_INCOME')!==false){
			$gloss['finance_income'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_DELIVERABLE_INCOME')!==false){
			$gloss['finance_deliverable_income'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_EXPENSE')!==false){
			$gloss['finance_expense'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_DELIVERABLE_EXPENSE')!==false){
			$gloss['finance_deliverable_expense'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_RETENTION')!==false){
			$gloss['finance_retention'][$key]=$val;
		}else if(stripos($val['h_section'],'FINANCE_DELIVERABLE_RETENTION')!==false){
			$gloss['finance_deliverable_retention'][$key]=$val;
		}
	}

//ASSIST_HELPER::arrPrint($gloss);
	
	//Loop through all headings, and if an h_parent_id is found, put it in a seperate array for option listing later	
	foreach($gloss as $key=>$data){
		//Save keys for anchor bar
		$name = ucwords(str_ireplace("_"," ",$key));
		$anchor[]="<a style='text-decoration:none' href='#$key'>".$name."</a>";	
		foreach($data as $heading=>$val){
			if($val['h_parent_id']>0){
				$children[$val['h_parent_id']][]=$val;
				unset($gloss[$key][$heading]);
			}
		}
	}
	//Draw anchor bar
	echo implode(" | ",$anchor);
	
//	ASSIST_HELPER::arrPrint($children);
	//Draw one big heading per category
	foreach($gloss as $key=>$val){
		$name = ucwords(str_ireplace("_"," ",$key));
		echo "<h2><a name=".$key."></a>".$name."</h2><br>";
		//Check the custom glossary table for a blurb, display it if found
		if(isset($custom_cats[$key])){
			echo "<p>".$custom_cats[$key]['purpose']."</p>";
			//echo "<tr><th>&nbsp;</th><td>".$custom_cats[$key]['purpose']."</td></tr>";
		}
		echo "<table class='form'>";
		//Display a heading for the table
		if(count($val)>0){
			//echo "<tr><th>System Term</th><th>Custom Term</th><th>Explanation</th></tr>";
		}
		//Display each of the terms
		foreach($val as $index=>$item){
			$number = $item['h_id'];
			//echo "<tr><th>".$item['h_default']."</th><td>".$item['h_client']."</td></tr>";
			echo "<tr><th>".$item['h_client']."</th><td>";
				if(isset($item['h_system_glossary'])){
					echo "<p><em>".$item['h_system_glossary']."</em></p>";
				}
				if($item['h_type'] == "LIST"){
					if(strlen($item['h_table'])>0){
						$listObj->changeListType($item['h_table']);
						$opts = $listObj->getActiveListItemsFormattedForSelect();
						//ASSIST_HELPER::arrPrint($opts);
						echo "<p>Available options:</p><ul>";
						if(isset($opts['options'])){
							$opts = $opts['options'];
						}
						foreach($opts as $o){
							echo "<li>".$o."</li>";
						}
						echo "</ul>";
					}
					if(strlen($item['h_glossary'])>0){
						echo "<p class='orange'>".$item['h_glossary']."</p>";
					}
				}else if($item['h_type'] == "DEL_TYPE" || $item['h_type'] == "OWNER"){
					if(strlen($item['h_table'])>0){
						$listObj->changeListType($item['h_table']);
						switch($item['h_type']) {
								case "OWNER":
									$listObject = new CNTRCT_CONTRACT_OWNER();
									$opts = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "DEL_TYPE":
									$listObject = new CNTRCT();
									$opts = $listObject->getAllDeliverableTypesForGloss();
									ASSIST_HELPER::arrPrint($opts);
									break; 
							}
						echo "<p>Available options:</p><ul>";
						if(isset($opts['options'])){
							$opts = $opts['options'];
						}
						if(count($opts)==0){
							$opts[]="No options have been set up by the administrator.";
						}
						foreach($opts as $o){
							echo "<li>".$o."</li>";
						}
						echo "</ul>";
					}
					if(strlen($item['h_glossary'])>0){
						echo "<p class='orange'>".$item['h_glossary']."</p>";
					}
				}else if($item['h_type'] == "USER"){
					echo"<p>This field contains a list of all the active users on the Assist System.</p>";
					if(strlen($item['h_glossary'])>0){
						echo "<p class='orange'>".$item['h_glossary']."</p>";
					}
				}else if($item['h_type'] == "MASTER" && $item['h_table'] == "financial_years"){
					echo "<p>This field is comprised of a list of the organisation's financial years as captured by the Assist Administrator within the Assist Master Setup function.</p>";
					if(strlen($item['h_glossary'])>0){
						echo "<p class='orange'>".$item['h_glossary']."</p>";
					}	
				}else if(strlen($item['h_glossary'])>0){
					echo "<p class='orange'>".$item['h_glossary']."</p>";
				}else if(strlen($item['h_system_glossary'])==0){
					echo"<p>No custom explanation available.</p>";
				}
					
				if(isset($children[$number])){
				echo "<p>Available options:</p><ul>";
				foreach($children[$number] as $ch){
					$gl = strlen($ch['h_glossary'])>0?" - <span class='orange'>".$ch['h_glossary']."</span>":"";
					$sgl = strlen($ch['h_system_glossary'])>0?" - <em>".$ch['h_system_glossary']."</em>":"";
					echo "<li>".$ch['h_client'].$gl.$sgl."</li>";
				}
				echo "</ul>";
				}
				
				
			
			echo "</td></tr>";
		}
		echo "</table>";
	//Back to top
	echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
	echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
	}
?>

<style>
	.top_line{
		text-decoration:none;
	}
</style>