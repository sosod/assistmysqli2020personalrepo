<?php






$parent_object_id = $object_id;
$details = array();
$budObject = new CNTRCT_FINANCE_BUDGET();
$details['budget'] = $budObject->getSummaryData($parent_object_id);
$details['deliverable']['budget'] = $budObject->getDeliverableSummaryData($parent_object_id);
$adjObject = new CNTRCT_FINANCE_ADJUSTMENT();
$details['adjustment'] = $adjObject->getSummaryData($parent_object_id);
$details['deliverable']['adjustment'] = $adjObject->getDeliverableSummaryData($parent_object_id);
$expObject = new CNTRCT_FINANCE_EXPENSE();
$details['expense'] = $expObject->getSummaryData($parent_object_id);
$details['deliverable']['expense'] = $expObject->getDeliverableSummaryData($parent_object_id);
$incObject = new CNTRCT_FINANCE_INCOME();
$details['income'] = $incObject->getSummaryData($parent_object_id);
$details['deliverable']['income'] = $incObject->getDeliverableSummaryData($parent_object_id);
$retObject = new CNTRCT_FINANCE_RETENTION();
$details['retention'] = $retObject->getSummaryData($parent_object_id);
$details['deliverable']['retention'] = $retObject->getDeliverableSummaryData($parent_object_id);

//ASSIST_HELPER::arrPrint($details['deliverable']['expense']);

$summary = array(
	'original_budget'=>array(
		'total'=>$details['budget']['rows']['total'],
		'allocated'=>$details['budget']['rows']['deliverables'],
		'unallocated'=>$details['budget']['rows']['unallocated'],  
	),
	'adjustments'=>array(
		'total'=>$details['adjustment']['rows']['total'],
		'allocated'=>$details['adjustment']['rows']['deliverables'],
		'unallocated'=>$details['adjustment']['rows']['unallocated'],  
	),
	'total_budget'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
	'expenses'=>array(
		'total'=>$details['expense']['rows']['total'],
		'allocated'=>$details['expense']['rows']['deliverables'],
		'unallocated'=>$details['expense']['rows']['unallocated'],  
	),
	'unspent_budget'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
	'retention_held'=>array(
		'total'=>$details['expense']['rows']['retention'],
		'allocated'=>$details['expense']['rows']['retention_deliverables'],
		'unallocated'=>$details['expense']['rows']['retention_unallocated'],  
	),
	'retention_paid'=>array(
		'total'=>$details['retention']['rows']['total'],
		'allocated'=>$details['retention']['rows']['deliverables'],
		'unallocated'=>$details['retention']['rows']['unallocated'],  
	),
	'retention_outstanding'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
	'income_received'=>array(
		'total'=>$details['income']['rows']['total'],
		'allocated'=>$details['income']['rows']['deliverables'],
		'unallocated'=>$details['income']['rows']['unallocated'],  
	),
	'unfunded_budget'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
);

$columns = array("total","allocated","unallocated");

foreach($columns as $c) {
	$summary['total_budget'][$c] = $summary['original_budget'][$c] + $summary['adjustments'][$c];
	$summary['unspent_budget'][$c] = $summary['total_budget'][$c] - $summary['expenses'][$c];
	$summary['unfunded_budget'][$c] = $summary['total_budget'][$c] - $summary['income_received'][$c];
	$summary['retention_outstanding'][$c] = $summary['retention_held'][$c] - $summary['retention_paid'][$c];
}

foreach($summary as $key => $s) {
	foreach($columns as $c) {
		$summary[$key][$c] = $displayObject->getCurrencyForDisplay($s[$c],false);
	}
}


$delObject = new CNTRCT_DELIVERABLE();
$add_deliverables = $delObject->getOrderedObjects($parent_object_id);
//$helper->arrPrint($add_deliverables);

$ddObject = new CNTRCT_FINANCE_DRAWDOWN();
$drawdowns = $ddObject->getAllItems($object_id);
$deliverable_drawdowns = $ddObject->getDeliverableDrawDowns();
if(!isset($drawdowns[0])) {
	$unspecified_dd = array(array('id'=>0,'name'=>$helper->getUnspecified(),'comment'=>"",'reftag'=>""));
	$drawdowns = $drawdowns + $unspecified_dd;
}
$deliverable_drawdowns['dd'][0] = array();
foreach($add_deliverables[0] as $i=>$d) {
	if(!isset($deliverable_drawdowns['del'][$i])) {
		$deliverable_drawdowns['dd'][0][] = $i;
	}
}



?>
<table class=tbl-container>
	<tr>
		<td width=48%>
			<?php
			$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
			echo $contract['display'];
			$js.=$contract['js'];
			?>
		</td>
		<td width=4%>
			
		</td>
		<td width=48%>
			<h2>Finance Summary</h2>
			<table id=tbl_finance>
				<tr>
					<th width=25% style='background-color: #FFFFFF; border-bottom: 1px solid #ababab;'></th>
					<th width=25%>Total</th>
					<th width=25%>Allocated to Deliverables</th>
					<th width=25%>Unallocated</th>
				</tr>
				<?php $field = "original_budget"; ?>
				<tr>
					<td class=th>Original Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class='right'><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "adjustments"; ?>
				<tr>
					<td class=th>+ Adjustments</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "total_budget"; ?>
				<tr class=total>
					<td class=th>= Total Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<tr class=divider>
					<td colspan=4></td>
				</tr>
				<?php $field = "total_budget"; ?>
				<tr>
					<td class=th>Total Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "income_received"; ?>
				<tr>
					<td class=th>- Income Received</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "unfunded_budget"; ?>
				<tr class=total>
					<td class=th>= Unfunded Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<tr class=divider>
					<td colspan=4></td>
				</tr>
				<?php $field = "total_budget"; ?>
				<tr>
					<td class=th>Total Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "expenses"; ?>
				<tr>
					<td class=th>- Expenses (incl. Retention)</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "unspent_budget"; ?>
				<tr class=total>
					<td class=th>= Budget Remaining</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<tr class=divider>
					<td colspan=4></td>
				</tr>
				<?php $field = "retention_held"; ?>
				<tr>
					<td class=th>Retention Held</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "retention_paid"; ?>
				<tr>
					<td class=th>- Retention Paid</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "retention_outstanding"; ?>
				<tr class=total>
					<td class=th>= Retention Balance</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=3><div id=div_finance_deliverables style='margin: 0 auto;'>
					<?php
					echo "<h2>".$helper->getDeliverableObjectName()."</h2>";
					
					//$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list"));
					?>
					<table id=tbl_finance_deliverables class=finance-list>
						<tr>
							<th></th>
							<th>Deadline</th>
							<th>Responsible Person</th>
							<th>Action Progress</th>
							<th><?php echo $helper->getObjectName("budget"); ?></th>
							<th><?php echo $helper->getObjectName("adjustment"); ?></th>
							<th>Total <?php echo $helper->getObjectName("budget"); ?></th>
							<th></th>
							<th><?php echo $helper->getObjectName("income"); ?></th>
							<th>Unfunded <?php echo $helper->getObjectName("budget"); ?></th>
							<th></th>
							<th><?php echo $helper->getObjectName("expense"); ?></th>
							<th>Unspent <?php echo $helper->getObjectName("budget"); ?></th>
							<th></th>
							<th><?php echo $helper->getObjectName("retention"); ?> Held</th>
							<th><?php echo $helper->getObjectName("retention"); ?> Paid</th>
							<th><?php echo $helper->getObjectName("retention"); ?> Outstanding</th>
						</tr>
						<?php
					foreach($drawdowns as $di => $dd) {
						if(isset($deliverable_drawdowns['dd'][$di]) && count($deliverable_drawdowns['dd'][$di])>0) {
							echo "
							<tr>
								<th class=th2>".$dd['name']."</th>
							</tr>
							";
							foreach($deliverable_drawdowns['dd'][$di] as $deli) {
								$deld = $add_deliverables[0][$deli];
								$children = array();
								$dbudget = 0;
								$dadj = 0;
								$dinc = 0;
								$dexp = 0;
								$dret = 0;
								$dheld = 0;
								if($deld['del_type']=="MAIN") {
									$children = array_keys($add_deliverables[$deli]);
									foreach($children as $fci) {
										$fi = $fci;
										$do = $budObject;
										$df = "budget";
										$dbudget+=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$do = $adjObject;
										$df = "adjustment";
										$dadj+=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$do = $incObject;
										$df = "income";
										$dinc+=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$do = $expObject;
										$df = "expense";
										$dexp+=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$dheld+=isset($details['deliverable'][$df][$fi][$do->getDeliverableSecondaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverableSecondaryFinanceFieldName()] : 0;
										$do = $retObject;
										$df = "retention";
										$dret+=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
									}
								} else {
										$fi = $deli;
										$do = $budObject;
										$df = "budget";
										$dbudget+=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$do = $adjObject;
										$df = "adjustment";
										$dadj=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$do = $incObject;
										$df = "income";
										$dinc=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$do = $expObject;
										$df = "expense";
										$dexp=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										$dheld=isset($details['deliverable'][$df][$fi][$do->getDeliverableSecondaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverableSecondaryFinanceFieldName()] : 0;
										$do = $retObject;
										$df = "retention";
										$dret=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
								}
								$d_tot_budg = $dbudget + $dadj;
									echo "
									<tr>
										<td class=b>".$deld['reftag'].": ".$deld['name']."</td>
										<td >".$displayObject->getDateForDisplay($deld['deadline'])."</td>
										<td >".$deld['responsible_person']."</td>
										<td >".$displayObject->getPercentageForDisplay($deld['action_progress'])."</td>
										<td>".$displayObject->getCurrencyForDisplay($dbudget,false)."</td>
										<td>".$displayObject->getCurrencyForDisplay($dadj,false)."</td>
										<td class=b>".$displayObject->getCurrencyForDisplay($d_tot_budg,false)."</td>
										<td style='background-color:#dedede;'></td>
										<td>".$displayObject->getCurrencyForDisplay($dinc,false)."</td>
										<td class=b>".$displayObject->getCurrencyForDisplay(($d_tot_budg-$dinc),false)."</td>
										<td style='background-color:#dedede;'></td>
										<td>".$displayObject->getCurrencyForDisplay($dexp,false)."</td>
										<td class='b'>".$displayObject->getCurrencyForDisplay(($d_tot_budg-$dexp),false)."</td>
										<td style='background-color:#dedede;'></td>
										<td>".$displayObject->getCurrencyForDisplay($dheld,false)."</td>
										<td>".$displayObject->getCurrencyForDisplay($dret,false)."</td>
										<td class='b'>".$displayObject->getCurrencyForDisplay(($dheld-$dret),false)."</td>
									</tr>";
								if($deld['del_type']!="DEL") {
									if(isset($add_deliverables[$deli])) {
										foreach($add_deliverables[$deli] as $ci => $cdel) {
											$fi = $ci;
											$do = $budObject;
											$df = "budget";
											$dbudget=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
											$do = $adjObject;
											$df = "adjustment";
											$dadj=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
											$d_tot_budg = $dbudget + $dadj;
											$do = $incObject;
											$df = "income";
											$dinc=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
											$do = $expObject;
											$df = "expense";
											$dexp=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
											$dheld=isset($details['deliverable'][$df][$fi][$do->getDeliverableSecondaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverableSecondaryFinanceFieldName()] : 0;
											$do = $retObject;
											$df = "retention";
											$dret=isset($details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()]) ? $details['deliverable'][$df][$fi][$do->getDeliverablePrimaryFinanceFieldName()] : 0;
										echo "
											<tr class=i style='background-color: #f5f5f5;'>
												<td>&nbsp;&nbsp;+&nbsp;".$cdel['reftag'].": ".$cdel['name']."</td>
												<td >".$displayObject->getDateForDisplay($cdel['deadline'])."</td>
												<td >".$cdel['responsible_person']."</td>
												<td >".$displayObject->getPercentageForDisplay($cdel['action_progress'])."</td>
												<td>".$displayObject->getCurrencyForDisplay($dbudget,false)."</td>
												<td>".$displayObject->getCurrencyForDisplay($dadj,false)."</td>
												<td class=b>".$displayObject->getCurrencyForDisplay($d_tot_budg,false)."</td>
												<td style='background-color:#dedede;'></td>
												<td>".$displayObject->getCurrencyForDisplay($dinc,false)."</td>
												<td class=b>".$displayObject->getCurrencyForDisplay(($d_tot_budg-$dinc),false)."</td>
												<td style='background-color:#dedede;'></td>
												<td>".$displayObject->getCurrencyForDisplay($dexp,false)."</td>
												<td class='b'>".$displayObject->getCurrencyForDisplay(($d_tot_budg-$dexp),false)."</td>
												<td style='background-color:#dedede;'></td>
												<td>".$displayObject->getCurrencyForDisplay($dheld,false)."</td>
												<td>".$displayObject->getCurrencyForDisplay($dret,false)."</td>
												<td class='b'>".$displayObject->getCurrencyForDisplay(($dheld-$dret),false)."</td>
											</tr>";
										}
									}
								}
							//}
							}
						}
					}
						?>
					</table>
				</div>
		</td>
	</tr>
</table>
<style type="text/css">
	td.th {
		font-weight: bold;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
	tr.total td.red {
		color: #CC0001;
	}
	tr.divider td {
		height: 10px;
		border-right: 1px solid #ffffff;
		border-left: 1px solid #ffffff;
	}
</style>
<script type="text/javascript">
	$(function() {
		
		$("table#tbl_finance_deliverables tr:first th").each(function() {
			var t = $(this).html();
			$(this).html(AssistString.str_replace(' ','<br />',t));
		});
				
		$("table.finance-list tr:gt(0)").find("td:eq(3)").removeClass("right").addClass("center");
		$("table.finance-list tr:gt(0)").find("td:lt(3)").removeClass("right").addClass("left");
		var table_width = (AssistString.substr($("#tbl_finance_deliverables").css("width"),0,-2))*1;
		$("#div_finance_deliverables").css("width",(table_width+3)+"px");
		var x = $("table#tbl_finance_deliverables tr:first th").length;
		$("table#tbl_finance_deliverables tr th.th2").prop("colspan",x).css("text-align","left");
		
		
		$("table#tbl_finance tr.total td").each(function() {
			var t = $(this).html();
			//console.log(t.charAt(0)+" = "+(t.charAt(0)=="-"));
			if(t.charAt(0)=="-") {
				$(this).addClass("red");
			}
		});
		
		$("table#tbl_finance_deliverables tr:gt(0) td.b:gt(0)").each(function() {
			var t = $(this).html();
			//console.log(t.charAt(0)+" = "+(t.charAt(0)=="-"));
			if(t.charAt(0)=="-") {
				$(this).addClass("red");
			}
		});
		

	});
</script>