/**
 * Copyright 2014 Action Assist
 * @authors Duncan Cosser, Janet Currie
 */
//Set the defaults (page, finyear)
		var section = "deliverable";
		var ajaxString = "";
		if(section == "deliverable"){
			ajaxString = "contract_id";
		}else if(section == "contract"){
			ajaxString = "contract_financial_year_id";
		}
		window.pagenum = 1;
		var year = $("#yearpicker").prop("value");
		refreshContract(year);
		
		
		//-----------------------------------------Page change handlers---------------------------------------------//
		$("#pages_list").change(function(){
			var year = $("#yearpicker").prop("value");
			window.pagenum = $(this).val();
			refreshContract(year);
		});
		
		$("#show_all").click(function(){
			var year = $("#yearpicker").prop("value");
			window.pagenum = -9;
			refreshContract(year);
		});
		
		$("#show_all_dyn").click(function(){
			var year = $("#yearpicker").prop("value");
			window.pagenum = -9;
			refreshContract(year);
		});
		
		$("#page_first").click(function(){
			var year = $("#yearpicker").prop("value");
			window.pagenum = 1;
			refreshContract(year);
		});
		
		$("#page_back").click(function(){
			var year = $("#yearpicker").prop("value");
			if(window.pagenum <= 1){
				window.pagenum = 1;
			}else{
				window.pagenum --;
			}
			refreshContract(year);
		});
		
		$("#page_next").click(function(){
			var year = $("#yearpicker").prop("value");
			if(window.pagenum >= window.total_pages){
				window.pagenum = window.total_pages;
			}else if(window.pagenum == -9){
				window.pagenum = 2;
			}else{
				window.pagenum ++;
			}
			refreshContract(year);
		});
		
		$("#page_last").click(function(){
			var year = $("#yearpicker").prop("value");
			window.pagenum = window.total_pages;
			refreshContract(year);
		});

		//-----------------------------------------Updating List Table---------------------------------------------//		
		function refreshContract(year){
		//Clearing the page selector
			$("#pages_list").empty();
			
			var result = AssistHelper.doAjax("inc_controller.php?action="+section+".Get","type=LIST&section=NEW&"+ajaxString+"="+year);
			if(window.pagenum == -9){
				var result_chunk = result;
				$("#pages_list").append("<option value=1>1</option>");
			}else{
			var pagenum = window.pagenum - 1;
		//Count number of contracts returned
			var con_total = result.length;
			var row_height = 42;
			var rows_total = row_height * con_total;
			var window_dimensions = AssistHelper.getWindowSize();
			var view_height = window_dimensions['height'];
		//	console.log(window_dimensions);
		//Distance from top of screen to bottom of header row in the table = 250px
			var row_space = view_height - 250;
			
			if(rows_total >= row_space){
				var allowed = Math.floor(row_space / 42);
				var tocut = con_total - allowed;
				var total_pages = Math.ceil(con_total/allowed);
				window.total_pages = total_pages;
				var page_contents = Array();
				for(var i=0; i<total_pages; i++){
					var j = i + 1;
					page_contents[i]=result.slice(i*allowed, (1+i)*allowed);
					$("#pages_list").append("<option value="+j+">"+j+"</option>");
				}
				var result_chunk = page_contents[pagenum];
				//console.log(page_contents);
				
		//		result.splice(-tocut, tocut);
			}else{
				$("#pages_list").append("<option value=1>1</option>");
				var result_chunk = result;
			}
//alert(view_size['height']);
//			alert("con_total"+con_total+" rows_total"+rows_total+" row_space"+row_space+" allowed"+allowed);
			
			//Calculate how to fit the data into pages based on screen size
//			result.splice(-24,24);
//			alert("spliced length:"+result.length);
			}
			$("#pages_list").val(pagenum+1);


		//-----------------------------------------Populating List Table---------------------------------------------//
			
			//Clear the table
			$(".con_details").each(function(){
				$(this).remove();
			});
		
			//For each contract
			for(var prop in result_chunk){
				var zing = result_chunk[prop];
				var sect = "del";
				
				//Add a row for the contract details
				$("#hidden_row").after("<tr class=con_details id="+section+"_"+zing[sect+'_id']+"></tr>");
				
				//For each setup heading
				$("#head_list th").each(function(){
					
					//Loop through this contract's indexes
					for(var index in zing){
						
						//If the contract's index matches this setup heading, fill a cell with the contract's data at this index
						if(index == $(this).prop("id")){
							$("#"+section+"_"+zing[sect+'_id']).append("<td>"+zing[index]+"</td>");
						}
					}
				});
				
				//Summon an 'Add Deliverable' button
				$("#"+section+"_"+zing[sect+'_id']).append("<td><input type='button' class='"+section+"_edit' id='"+zing[sect+'_id']+"' value='Add Action' /></td>");
				
				//Breathe life into the button
				$('.'+section+'_edit').each(function(){
					var id = parseInt($(this).prop("id"));
					$(this).click(function(){
						document.location.href='new_action_add_action.php?deliverable_id='+id;
					});
				});
			}
			
			//'Show all' button at the end of the last item
			if(! $("#show_all_row").length){
				$(".con_details").last().after("<tr id='show_all_row' style='display:none'></tr>");
			}
			if(window.pagenum != -9){
				$("#show_all_row").html("<td colspan='42'><input type='image' src='../pics/tri_down.gif' id=show_all_dyn /></td>");
				$("#show_all_row").show();
			}else{
				$("#show_all_row").hide();
			}
	}