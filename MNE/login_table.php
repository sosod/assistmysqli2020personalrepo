<?php
/* requires $tdb as object of ASSIST_DB class => created in main/tables.php */
				/*$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'deadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'risk_action'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'action_owner'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'deliverable'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'progress'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'action_status'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);*/
$future = strtotime(date("d F Y",($today+($next_due*24*3600))));

$myObject = new CNTRCT_ACTION();
$my_field_name = $myObject->getTableField();
$data = $myObject->getObject(array('type'=>"FRONTPAGE_TABLE",'deadline'=>$future,'limit'=>(isset($action_profile['field2']) ? $action_profile['field2'] : 20)));
$headings2 = $data['head'];
$rows = $data['rows'];
$headObject = new CNTRCT_HEADINGS();
$headings = $headObject->getMainObjectHeadings("ACTION","LIST","DASHBOARD");
$head = array();
foreach($headings2 as $fld => $h) {
	if(isset($headings[$fld]) || substr($fld,0,strlen($my_field_name))!=$my_field_name) {
		$head[$fld] = array(
			'text'=>$h['name'],
			'long'=>($h['type']=="TEXT"),
			'deadline'=>($fld==$myObject->getDeadlineFieldName()),
		);
	}
}
$head['link'] = array('text'=>"",'long'=>false,'deadline'=>false);
$actions = array();
foreach($rows as $id => $r) { //arrPrint($r);
	$actions[$id] = array();
	foreach($head as $fld=> $h){
		if($fld!="link"){
			if(is_array($r[$fld])) {
				$actions[$id][$fld] = $r[$fld]['display']; //(isset($r[$fld]['display']) ? $r[$fld]['display'] : $r[$fld]);
			} else {
				$actions[$id][$fld] = $r[$fld]; //(isset($r[$fld]['display']) ? $r[$fld]['display'] : $r[$fld]);
			}
		}
	}
	$actions[$id]['link'] = "manage_update_action_object.php?page_redirect=home|object_id=".$id;
	
}
//arrPrint($head);
/*
$sql = "SELECT name, client_terminology FROM ".$dbref."_header_names WHERE type != 'risk'";
$rs = $tdb->db_query($sql);
	while($row = mysql_fetch_array($rs)) {
		if(isset($head[$row['name']])) {
			$head[$row['name']]['text'] = $row['client_terminology'];
		}
	}
unset($rs);

	$sql = "SELECT 	DISTINCT(RA.id) ,
			RA.action,
			RA.deliverable,
			RA.deadline,
			RA.progress,
			RA.status AS statusid,
			RAS.name as status,
			RAS.color as statuscolor,
			CONCAT(TK.tkname,' ',TK.tksurname) AS owner,
			RA.action_owner
			FROM  ".$dbref."_actions RA 
			LEFT JOIN ".$dbref."_action_status RAS ON RA.status = RAS.id			
			LEFT JOIN assist_".$cmpcode."_timekeep TK ON  TK.tkid  = RA.action_owner 
			WHERE RA.action_owner = '$tkid' AND RA.deadline <= ($today + 3600*24*$next_due)  	
			ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " STR_TO_DATE(RA.deadline,'%d-%M-%Y') DESC "; break;
						default:
						case "dead_asc":	$sql.= " STR_TO_DATE(RA.deadline,'%d-%M-%Y') ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20); //echo $sql;
	$tasks = $tdb->mysql_fetch_all_fld($sql,"id");
	
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));	
	foreach($tasks as $id => $task) {
		$deadline = "";
	    $stattusid = $task['statusid'];		
		if($stattusid !== "3") {
			$deaddiff = $today - strtotime($task['deadline']);
			$diffdays = floor($deaddiff/(3600*24));
			if($deaddiff<0) {
				$diffdays*=-1;
				$days = $diffdays > 1 ? "days" : "day";
				$deadline =  "<span class='soon'>Due in $diffdays $days</span>";
			} elseif($deaddiff==0) {
				$deadline =  "<span class='today'><b>Due today</b></span>";
			} else {
				$days = $diffdays > 1 ? "days" : "day";
				$deadline = "<span class='overdue'><b>$diffdays $days</b> overdue</span>";
			}			

			$actions[$id] = array();
			$actions[$id]['ref'] = $id;
			$actions[$id]['deadline'] = $deadline;
			$actions[$id]['risk_action'] = $task['action'];
			$actions[$id]['action_owner'] = $task['owner'];
			$actions[$id]['deliverable'] = $task['deliverable'];
			$actions[$id]['progress'] = ($task['progress'] == "" ? 0 : $task['progress'])."%";
			$actions[$id]['action_status'] = ($task['status'] == "" ? "New" : $task['status']);
			$actions[$id]['link'] = "manage/update_action.php?id=".$task['id'];
		}
	}
*/
?>