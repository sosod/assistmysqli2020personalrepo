<?php
$_REQUEST['object_type'] = "FINANCE";

include("inc_header.php"); 

$object_type = $_REQUEST['object_type'];
$myObject = new CNTRCT_FINANCE();

$child_object_id = $_REQUEST['object_id'];
$object_id = $child_object_id;	//needed for generic_object_update_form
$child_object_type = "CONTRACT";

$page_action = "ADD";
$page_redirect = "manage_finance_object.php?object_id=".$child_object_id."&";


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($child_object_type, $child_object_id, true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Add New <?php echo $helper->getObjectName("FINANCE"); ?></h2><?php
			include("common/generic_object_update_form.php");
			$js= $helper->drawPageFooter("",strtolower($object_type),"parent_id=".$child_object_id);
?>
		</td>
	</tr>
	<tr>
		<td colspan=3>
			<?php
			$page_section = "MANAGE";
			$page_action = "EDIT"; 
				include("common/generic_list_page.php"); 
			?>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	//echo $js; 
	
	?>
});
</script>