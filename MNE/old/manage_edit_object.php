<?php
include("inc_header.php");

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

switch($object_type) {
	case "CONTRACT":
		$myObject = new CNTRCT_CONTRACT($object_id);
		
		break;
	case "DELIVERABLE":
		$childObject = new CNTRCT_DELIVERABLE($object_id);		
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new CNTRCT_CONTRACT($parent_id);
		$parent_object_type = "CONTRACT";
		break;
	case "ACTION":
		$childObject = new CNTRCT_ACTION($object_id);		
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new CNTRCT_DELIVERABLE($parent_id);
		$parent_object_type = "DELIVERABLE";
		break;
}
?>
<table class=tbl-container>
	<tr>
	<?php if($object_type!="CONTRACT") { ?>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($parent_object_type, $parent_id, false); ?></td>
		<td width=5%>&nbsp;</td>
	<?php } ?>
		<td width=48%><h2><?php echo $helper->getObjectName($object_type); ?> Details</h2><?php 
			include("common/generic_object_form.php");
			$js.= $helper->drawPageFooter("",strtolower($object_type),"object_id=".$object_id."&log_type=".CNTRCT_LOG::EDIT);
		?></td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>
});
</script>