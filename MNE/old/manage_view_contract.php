<?php
include("inc_header.php");

$logObject = new CNTRCT_LOG("contract");
$contract_id = $_REQUEST['contract_id'];

?>
<table class=tbl-container>
	<tr>
		<td width=50%><h2><?php echo $helper->getContractObjectName(); ?> Details</h2><?php include("common/contract_details.php"); ?></td>
		<td width=50%><h2>Activity Log</h2><?php 
			$audit_log = $logObject->getObject(array('object_id'=>$contract_id));
			echo $audit_log[0];
			//$js.=$helper->drawPageFooter($helper->getGoBack(),"contract",array('object_id'=>$contract_id)); 
		?></td>
	</tr>
	<tr>
		<td colspan=2>
			<h2><?php echo $helper->getDeliverableObjectName(true); ?></h2>
			<?php
			$delObject = new CNTRCT_DELIVERABLE();
			$del_objects = $delObject->getObject(array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$contract_id)); 
			?>
			<table width=100%>
				<tr>
					<?php
					foreach($del_objects['head'] as $fld=>$head) {
						echo "<th>".$head['name']."</th>";
					}
					?>
					<th></th>
				</tr>
				<?php
				foreach($del_objects['rows'] as $id => $obj) {
					echo "<tr>";
					foreach($obj as $fld=>$val) {
						echo "<td>".$val."</td>";
					}
					echo "<td class=center><input type=button value=View ref=".$id." class=btn_view /></td>";
					echo "</tr>";
				}
				?>
			</table>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php echo $js; ?>
	$("input:button.btn_view").click(function() {
		var i = $(this).attr("ref");
		document.location.href = 'manage_view_deliverable.php?deliverable_id='+i;
	});
});
</script>