<?php

class MODULE_QUERY_REPORT_OBJECT extends MODULE_QUERY_REPORT {

	//CONSTRUCT
	private $date_format = "STRING";
	private $my_class_name = "MODULE_QUERY_REPORT_OBJECT";
	private $my_quick_class_name = "MODULE_QUERY_QUICK_REPORT";
	const REFTAG = Risk::REFTAG;
	const REPORTFILENAME = "query";
	const OBJECT_DEADLINE = "query_deadline_date";
	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Query Assist: Query Report";
	private $multilist_results;
	private $multitext_separator = ";";
	
	private $backupdata = array();
	
	


	public function __construct($p) {	
		//echo "<P>MODULE_COMPL_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}
	
	protected function getFieldDetails() {

		$headers = new Naming();
		$x = $headers->getReportingNaming("query");
		unset($x['attachements']);
		//$this->arrPrint($x);
		$search = array('query_item','query_description','query_background','query_type','query_category','query_owner','query_status','query_progress');
		$replace = array('id','description','background','type','category','sub_id','status','action_progress');
		$y = json_encode($x);
		$z = json_decode(str_replace($search,$replace,$y),true); 
		$this->titles = $z;
		$this->titles['date_completed'] = "Action Completed Date";
		$this->titles['result'] = "Result By Action Progress";
		$this->allowchoose = array(
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'id' => false,
			'date_completed'=>false,
			'action_progress' => false,
		);
		$this->types = array(
			'id' => "REF",
			'query_reference' => "TEXT",
			'description' => "TEXT",
			'background' => "TEXT",
			'query_source' => "LIST",
			'type' => "LIST",
			'category' => "LIST",
			'financial_exposure' => "LIST",
			'monetary_implication' => "TEXT",
			'risk_level' => "LIST",
			'risk_type' => "LIST",
			'risk_detail' => "TEXT",
			'finding' => "MULTITEXT",
			'internal_control_deficiency' => "MULTITEXT",
			'recommendation' => "MULTITEXT",
			'client_response' => "MULTITEXT",
			'auditor_conclusion' => "MULTITEXT",
			'sub_id' => "LIST",
			'financial_year' => "LIST",
			'query_date' => "DATE",
			'query_deadline_date' => "DATE",
			'status' => "LIST",
			'action_progress' => "PERC",
			'date_completed'=>"DATE",
			'result'=>"RESULT",
		);
		
		$this->default_data = array();
		$this->data = array(
		);
		$this->allowgroupby = array(
		);
		$this->allowsortby = array(
			'id' => true,
			'query_reference' => true,
			'description' => true,
			'background' => true,
			'type' => false,
			'category' => false,
			'financial_exposure' => false,
			'monetary_implication' => true,
			'risk_level' => false,
			'risk_type' => false,
			'risk_detail' => true,
			'finding' => true,
			'internal_control_deficiency' => true,
			'recommendation' => true,
			'client_response' => true,
			'auditor_conclusion' => true,
			'sub_id' => false,
			'financial_year' => false,
			'query_date' => true,
			'query_deadline_date' => true,
			'status' => false,
			'action_progress' => false,
			'date_completed'=>false,
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array(
			'query_reference' => 1,
			'query_date' => 2,
			'description' => 3,
			'background' => 4,
			'monetary_implication' => 5,
			'risk_detail' => 6,
			'finding' => 7,
			'internal_control_deficiency' => 8,
			'recommendation' => 9,
			'client_response' => 10,
			'auditor_conclusion' => 11,
			'query_deadline_date' => 12,
			'id' => 14,
		);
	
	}
	
	protected function getFieldData() {
		$this->data = array(
			'query_source' 				=> $this->getClassData("QuerySource"),
			'type' 				=> $this->getClassData("QueryType"),
			'category'			=> $this->getClassData("QueryCategory"),
			'financial_exposure'=> $this->getClassData("FinancialExposure"),
			'risk_level'		=> $this->getClassData("RiskLevel"),
			'risk_type'			=> $this->getClassData("RiskType"),
			'sub_id'		=> $this->getClassData("QueryOwner"),
			'financial_year'	=> $this->getFinancialYears(),
			'status'	 		=> $this->getClassData("QueryStatus"),
			'result'			=> $this->getResultOptions(),
		);
		$this->default_data = array();
		
		//$this->arrPrint($this->data['sub_id']);
	}
	private function getClassData($fld) {
		$data = array();
		switch($fld) {
			case "QuerySource":			$me = new QuerySource("","","","");	break;
			case "QueryType":			$me = new QueryType("","","","");	break;
			case "QueryCategory":		$me = new RiskCategory( "", "", "", "" );	break;
			case "FinancialExposure":	$me = new FinancialExposure( "", "", "", "", "" );	break;
			case "RiskLevel":			$me = new RiskLevel( "", "", "", "", "" );	break;
			case "RiskType":			$me = new RiskType( "", "", "", "", "" );	break;
			case "QueryOwner":			$me = new Directorate( "", "", "", "", "" );	break;
			case "QueryStatus":			$me = new RiskStatus( "", "", "", "" );	break;
		}
		$d = $me->getReportList();
		$data = $this->listSort($d);
		return $data;
	}


	
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(self::REPORTFILENAME);
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(self::REPORTFILENAME);
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r['id'];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$action_progress = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,"id");
			if( (isset($_REQUEST['columns']['action_progress']) && strtoupper($_REQUEST['columns']['action_progress'])=="ON") || (isset($_REQUEST['columns']['result']) && strtoupper($_REQUEST['columns']['result'])=="ON") ) {
				$action_progress = $this->getActionProgress($db,array_keys($rows));
			}
			foreach($rows as $key => $r) {
				$my_result = isset($action_progress[$key]) ?  ($action_progress[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
				$r['action_progress'] = $my_result['action_progress'];
				$r['date_completed'] = ($my_result['action_progress']<100 ? "" : $my_result['date_completed']);
				$r['result'] = $this->getCompliance($my_result,$r[self::OBJECT_DEADLINE]);
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r['id']] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								$d = $d;
								break;
							case "LIST":
							case "TEXTLIST":
							case "MULTILIST":
								$d2 = $d;
								if(strlen($d)==0) {
									$d = "Unspecified";
								} else {
									$x = explode(",",$d);
									$d = "";
									$z = array();
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										} elseif(isset($this->backupdata[$i][$a])) {
											$z[] = $this->backupdata[$i][$a];
										} 
									}
									$d = count($z)>0 ? implode(", ",$z) : "Unspecified";//$d2;//"Unknown";
								}
								//$d.=" [".$d2."]";
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r['id']][$i] = stripslashes($d);
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap,$d) {
		$result = "notCompletedAndOverdue";
		$deadline = strtotime($d);
		$p = $ap['action_progress'];
		$dc = strtotime($ap['date_completed']);
		if($p<100) {	//incomplete
			if(strtotime(date("d F Y")) > $deadline) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		} else {		//complete
			if($dc < $deadline) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$deadline) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		}
		return $result;
		
	}
	
	public function getActionProgress($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT U.*, A.risk_id 
			FROM ".$db->getDBRef()."_actions_update U 
			INNER JOIN ".$db->getDBRef()."_actions A 
				ON U.action_id = A.id 
			WHERE ".Riskaction::getStatusSQLForWhere("A")." 
			AND A.risk_id IN (".implode(",",$objects).") 
			AND A.progress > 0 
			ORDER BY A.risk_id ASC
			, U.action_id ASC
			, U.insertdate DESC"; 
			$rows = $db->mysql_fetch_all($sql); 
			$updates = array();
			foreach($rows as $r) {
				$a = $r['risk_id'];
				$c = unserialize(base64_decode($r['changes']));  
				if(isset($c['action_progress'])) {
					if(isset($c['date_completed']['to']) && strlen($c['date_completed']['to'])>0 && strtotime($c['date_completed']['to'])>86401) {
						$dc = date("d F Y H:i:s",strtotime($c['date_completed']['to']));
					} else {
						$dc = date("d F Y H:i:s",strtotime($r['insertdate']));
					}
					if(!isset($updates[$a])) {
						$updates[$a] = $dc;
					} elseif(strtotime($updates[$a])<strtotime($dc)) {
						$updates[$a] = $dc;
					}
				}
			}

			$sql = "SELECT
			A.risk_id as object
			, Q.".self::OBJECT_DEADLINE."
			, count(A.id) as count
			, sum(A.progress) as total
			, avg(A.progress) as average
			, max(A.action_on) as last_date
			FROM `".$db->getDBRef()."_actions` A
			INNER JOIN ".$db->getDBRef()."_query_register Q
			ON A.risk_id = Q.id
			WHERE A.risk_id IN (".implode(",",$objects).")
			AND A.active = 1
			group by A.risk_id";
			$res = $db->mysql_fetch_all_fld($sql,"object");		

			foreach($res as $d => $rs) {
				$r = "N/A";
				if(isset($updates[$d])) { 
					$r = $updates[$d];
				}
				if($r!="N/A" && strtotime($r)>strtotime($rs['last_date'])) {
					$dc = strtotime($r);
				} else {
					$dc = strtotime($rs['last_date']);
				}
				$result[$d] = array(
					'action_progress'=>$rs['average'],
					'date_completed'=>date("d M Y",$dc)
				);
			}
			
			
		}
		return $result;
	}
	
	

	protected function setSQL($db,$filter=array()) {
		$x = "Q";	//object alias in SQL
		$sql_status = Risk::getStatusSQLForWhere($x);
		$sql = "";
		$sql = "
		SELECT ".$x.".*
		FROM ".$db->getDBRef()."_query_register ".$x." 
		WHERE ".$sql_status; //echo $sql;
		//$this->arrPrint($_REQUEST);
		if(!isset($this->titles['id'])) { $this->getFieldDetails(); }
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; 
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY ".$x.".id ";
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						/*case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;*/
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$a = "";
					switch($i) {
/*						case '': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$a = $this->report->getFilterSql("D",$t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;*/
						case 'action_progress':	
						case 'date_completed':
						case 'result':			
							break;	//do nothing; filter applied in row processing
						default:				
							$a = $this->report->getFilterSql($x,$t,$f,$ft,$i);
					}
					if(strlen($a)>0) { $where[] = $a; }
				}
			}
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		if(count($having)>0) {
			$sql.= $sql_group;
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		
		$sql.=$this->getSortBySql();

		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array();
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				switch($s) {
					case "query_date":
					case "query_deadline_date":
						$sort_by[] = "STR_TO_DATE(".$s.",'%d-%b-%Y')";
						break;
					default:
						$sort_by[] = $s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	

}

?>