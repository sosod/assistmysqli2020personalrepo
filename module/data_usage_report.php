<?php
/********
required fields:
- $me = new module class()
- $ignore_folders = array()
- $folder_depth = "../";
 *********/

//error_reporting(-1);

$adb = new ASSIST_DB("master");
$cmpcode = $me->getCmpCode();


$ts = 0;
$fn = 0;
$folder = $folder_depth."files/".$cmpcode."/".strtoupper($_SESSION['modref']);
$totalsize = $me->getFolderSize($folder,$ignore_folders);


if($totalsize[2]==0) {
    $folder = $folder_depth."files/".$cmpcode."/".strtolower($_SESSION['modref']);
    $totalsize = $me->getFolderSize($folder,$ignore_folders);
}


$folder.="/";




?>
    <h1><?php echo $page_title; ?></h1>
    <h2>Module Specific Data Usage</h2>
    <table cellpadding=3 cellspacing=0 width=500>
        <tr>
            <th>Folder</th>
            <th>Num. of Files</th>
            <th>File Size (Bytes)</th>
        </tr>
        <?php
        $ret=$me->displayFolderSize($totalsize,$folder,$me->getModTitle());
        $ts+= $ret[0];
        $fn+= $ret[1];
        ?>
        <tr>
            <th style="text-align:right;">Total in Mega-Bytes:</th>
            <th style="text-align:right;">&nbsp;</th>
            <th style="text-align:right;"><?php echo $me->formatBytes($ts,2); ?></th>
        </tr>
    </table>
    <p>&nbsp;</p>
    <h2>Overall Data Usage</h2>
<?php
/* GET DOCUMENT STORAGE MODULES */
$docmods = array();
$doccode = array();
$sql = "SELECT * FROM assist_docmodules WHERE yn = 'Y' ORDER BY doclocation";
/*$rs = $adb->db_query($sql);
	while($row = mysql_fetch_array($rs)) {*/
$rows = $adb->mysql_fetch_all($sql);
foreach($rows as $row) {
    $docmods[$row['doclocation']] = $row;
    $doccode[] = $row['doclocation'];
}
unset($rs);
unset($adb);


$folder = $folder_depth."files/".$me->getCmpCode();
$f=0;
if(is_dir($folder)) {	//if valid files folder
    $sql = "SELECT * FROM assist_menu_modules WHERE (modyn = 'Y' OR modadminyn = 'Y')";
    //$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','",$doccode)."') AND (modyn = 'Y' OR modadminyn = 'Y')";
    $modules = $me->mysql_fetch_all_by_id($sql,"modref");
    if(count($modules)>0) {	//if active doc modules
        echo "
					<h3>Active Data Storage Modules</h3>
					<ol>
					";
        foreach($modules as $key=>$mod) {
            if(in_array($mod['modlocation'],$doccode)) {
                echo "<li>".$mod['modtext']."</li>";
                $f++;
            }
            $modules[strtolower($key)] = $mod;
        }
        $fre = 25 * $f;
        echo "
					</ol>
					<p class=b>Total MB included in subscription fee: ".$fre." MB</p>
					<h3>Data Storage In Use</h3>";
        $fre*=1024;
        $fre*=1024;
        $ts = 0;
        $folder.="/";
        echo "
					<table width=500>
						<tr>
							<th>Folder</th>
							<th>Num. of Files</th>
							<th>File Size (MB)</th>
						</tr>";
        $totalsize = $me->getFolderSize($folder);// arrPrint($totalsize);
        $folda = $me->getCmpName();
        $size = $totalsize[1];
        $count = $totalsize[2];
        $total = $size;
        echo "
							<tr>
								<td class=b>".$folda."</td>
								<td class=center>".$count."</td>
								<td class=right>".$me->formatBytes($size,2)."</td>
							</tr>";
        unset($totalsize[0]);
        unset($totalsize[1]);
        unset($totalsize[2]);
        foreach($totalsize as $tf) {
            $foldx = $tf[0];
            $size = $tf[1];
            $count = $tf[2];
            unset($tf[0]);
            unset($tf[1]);
            unset($tf[2]);
            if(count($tf)>0) {
                foreach($tf as $tf2) {
                    $tf3 = calcFolders($tf2);
                    $size+=$tf3['size'];
                    $count+=$tf3['count'];
                }
            }
            if($count>0 || $size>0) {
                $foldz = explode("/",$foldx);
                $mr = array_pop($foldz);
                $fold = $folda." > ".(isset($modules[$mr]) ? $modules[$mr]['modtext'] : $mr);
                echo "
							<tr>
								<td class=b>".$fold."</td>
								<td class=center>".$count."</td>
								<td class=right>".$me->formatBytes($size,2)."</td>
							</tr>";
            }
            $total+=$size;
        }
        $diff = $total - ($fre);
        echo "
						<tr>
							<th class=right>Total Data Used:</th>
							<th class=right>&nbsp;</th>
							<th class=right>".$me->formatBytes($total,2)."</th>
						</tr>
						<tr>
							<td class='b right'>Less MB included in subscription fee:</th>
							<td class=right>&nbsp;</td>
							<td class=right>-".$me->formatBytes($fre,2)."</td>
						</tr>
						<tr>
							<th class=right>Total Billable Data:</th>
							<th class=right>&nbsp;</th>
							<th class=right>".(($diff)>0?$me->formatBytes(($diff),2) : "-")."</th>
						</tr>
					</table>";
    } else {	//if active doc modules - else
        echo "<p>No active document modules exist for this client.</p>";
    }	//if active doc modules - end
} else {	//if valid files folder - else
    echo "<p><span class=idelete>ERROR:</span> No data storage folder exists. Please contact your business partner immediately to ensure that a folder is created.</p>";
}	//if valid files folder - end




function calcFolders($f) {
    $res = array('size'=>0,'count'=>0);
    unset($f[0]);
    $res['size'] = $f[1];
    unset($f[1]);
    $res['count'] = $f[2];
    unset($f[2]);
    if(count($f)>0) {
        foreach($f as $a) {
            $r = calcFolders($a);
            $res['size']+=$r['size'];
            $res['count']+=$r['count'];
        }
    }
    return $res;
}

?>