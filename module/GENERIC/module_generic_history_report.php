<?php

class MODULE_GENERIC_HISTORY_REPORT extends ASSIST {

	public function __construct() {
		parent::__construct();
	}

	public function displayPage() {
		$this->preparePage();
		$reports = $this->getList();
		$report = new ASSIST_REPORT_HISTORY();
		$report->drawTable($reports);
	}

}


?>