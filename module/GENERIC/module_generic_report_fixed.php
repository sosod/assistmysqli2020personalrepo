<?php

class MODULE_GENERIC_REPORT_FIXED extends ASSIST {

	private $act;
	
	private $reports = array();
	private $use_filter = false;
	private $filter = array();
	
	protected $reporting_data;
	protected $class_name;

	public function __construct($p,$d="DATETIME",$cn) {
		parent::__construct();
		$this->act = isset($_REQUEST['act']) ? strtoupper($_REQUEST['act']) : "GENERATOR";
		$this->class_name = $cn;
	}

	public function displayPage() {	
		switch($this->act) {
			case "DRAW":
				$this->prepareReport();
				$report = new ASSIST_REPORT_FIXED_DRAW($this->reporting_data);
				if($this->reporting_data['settings']['manualPageTitle']===true) {
					$report->setReportTitle($this->getReportTitle());
				}
				$report->drawPage();
				break;
			case "GENERATOR":
			default:
				$this->prepareGenerator();
				$report = new ASSIST_REPORT_FIXED_GENERATOR($this->reports, $this->use_filter, $this->filter,$this->class_name);
				$report->drawPage();
		}
	}
	
	protected function setReport($name, $description, $format, $code) {
		$this->reports[$code] = array(
			'name'=>$name,
			'descrip'=>$description,
			'format'=>$format,
			'code'=>$code,
		);
	}
	
	protected function setFilter($id,$name) {
		$this->filter[$id] = $name;
	}
	
	protected function setUseFilter($f) { $this->use_filter = $f; }
	
	
}

?>