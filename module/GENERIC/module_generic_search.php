<?php

class MODULE_GENERIC_SEARCH extends ASSIST {

	protected $report;
	protected $vars;
	private $act;
	protected $page = "";
	protected $module_date_format = "DATETIME";
	private $class_name;
	private $quick_class_name;
	
	protected $defaults = array(
		'allowchoose' => true,
		'default_selected' => true,
		'allowfilter' => true,
		'type' => "TEXT",
		'data' => "",
		'default_data' => "",
		'allowgroupby' => true,
		'allowsortby' => true,
		'sortposition' => 0,
	);
	
	

	public function __construct($p,$d="DATETIME",$cn,$qcn) {
		//echo "<P>Module_GENERIC_REPORT.__construct()</p>";
		parent::__construct();
		$this->module_date_format = $d;
		$this->page = $p;
		$this->class_name=$cn;
		$this->quick_class_name=$qcn;
		$this->vars = $_REQUEST;
		$this->act = isset($_REQUEST['act']) ? strtoupper($_REQUEST['act']) : "GENERATOR";

	}
	
	public function fixedReport() {
		$this->report = new ASSIST_SEARCH_DRAW();
	}

	public function displayPage() {	
		//$this->arrPrint($_REQUEST);
		//echo "<P>MODULE_GENERIC_REPORT.displayPage()</p>";
		switch($this->act) {
		case "GENERATE":
		//echo "generating";
			$this->report = new ASSIST_SEARCH_DRAW_TABLE();
			$this->report->setDBDateFormat($this->module_date_format);
			$this->prepareOutput();
			$this->setFields();
			break;
		case "DRAW":	
			$this->report = new ASSIST_SEARCH_DRAW_TABLE();	
			$this->prepareDraw();
			break;
		default:
		case "GENERATOR":
			$this->report = new ASSIST_SEARCH_GENERATOR($this->page,$this->class_name,$this->quick_class_name);
			$this->report->setDBDateFormat($this->module_date_format);
			$this->prepareGenerator();
			$this->setFields();
			break;
		}
		$this->report->drawPage($this->act);
	}
	
}

?>