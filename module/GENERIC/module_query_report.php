<?php

class MODULE_QUERY_REPORT extends MODULE_GENERIC_REPORT {

	protected $folder;
	//GET FIELD DETAILS
	protected $titles;
	protected $allowchoose;
	protected $default_selected;
	protected $allowfilter;
	protected $types;
	protected $default_data;
	protected $data;
	protected $allowgroupby;
	protected $allowsortby;
	protected $default_sort;
	protected $sortposition;

	public function __construct($p,$d,$c,$q) {
		parent::__construct($p,$d,$c,$q);
	}

	
	
	protected function prepareGenerator() {
		//echo "<P>module_query_report.prepareGenerator</p>";
		if(isset($_REQUEST['quick_id'])) {
			$db = new ASSIST_DB("client");
			$quick = $db->mysql_fetch_one("SELECT * FROM ".$db->getDBRef()."_quick_report WHERE id = ".$_REQUEST['quick_id']);
			$this->report->setQuickReport($quick['id'],$quick['name'],$quick['description'],$quick['insertuser'],$quick['insertdate'],unserialize($quick['report']));
		}
		$this->getFieldDetails();
		$this->getFieldData();
		$this->displayPageHeading();
	}
	
	public function displayPageHeading() {
		//echo "<p>module_query_report.displayPageHeading</p>";
		//Navigation buttons & page title
		$header = array(0=>"",1=>"",2=>"");
		$location = explode("_",$this->page);
		$m0 = new Menu();
		$m0_menu = $m0->getAMenu("report");	
		$header[0] = "<a href='/".$_SESSION['modlocation']."/report/' class=breadcrumb>".($m0_menu['client_name'] == "" ? $m0_menu['name'] : $m0_menu['client_name'])."</a>";
		$h = array(1=>"report");
		if(isset($location[1])) { $h[2] = $location[0]; }
		foreach($h as $k=>$i) {
			$m_menu = $m0->getSubMenu($i);
			$menu = array();
			foreach($m_menu as $key => $m) {
				$m['url'] = "/".$_SESSION['modlocation']."/".$this->folder."/".$m['folder'].".php";
				$m['display'] = ($m['client_name'] == "" ? $m['name'] : $m['client_name']);
				if($k==1) { $l = $location[$k-1]; } else { $l = $this->page; } 
				$m['active'] = ($m['folder'] == $l ? true : false);
				if($m['active']===true) { $header[$k]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
				$menu[$key] = $m;
			}
			if(count($menu)>0) {
				$this->drawNavButtons($menu);
			}
		}
		foreach($header as $key => $h) { if(strlen($h)==0) { unset($header[$key]); } }
		echo "<h1>".implode(" >> ",$header)."</h1>";
	}
	
	public function setFolder($f) {
		$this->folder = $f;
	}
	
	public function getFinancialYears() {
		$data = array();
		$me = new FinancialYear("","","");
		$d = $me->getReportList();
		$data = $this->listSort($d); 
		return $data;
	}
	public function getQueries() {
		$data = array();
		if(!isset($this->data['financial_year']) || count($this->data['financial_year'])==0) { $this->data['financial_year'] = $this->getFinancialYears(); }
		$db = new ASSIST_DB("client");
		$sql = "SELECT Q.id, Q.description as name, Q.financial_year 
				FROM ".$db->getDBRef()."_query_register Q
				LEFT OUTER JOIN ".$db->getDBRef()."_financial_years FY
				ON Q.financial_year = FY.id AND FY.active = 1
				WHERE ".Risk::getStatusSQLForWhere("Q")."
				ORDER BY Q.description, STR_TO_DATE(FY.end_date,'%d-%b-%Y')";
		$objects = $db->mysql_fetch_all_fld($sql,"id");

		foreach($objects as $lid => $l) {
			$txt = "";
			if(!isset($_REQUEST['act']) || $_REQUEST['act']=="GENERATOR") {
				$txt = "[".MODULE_QUERY_REPORT_OBJECT::REFTAG.$lid."] ";
			}
			$x = stripslashes($l['name']);
			if(strlen($x)>75) {
				$x = substr($x,0,75)."...";
			}
			$txt.= $x;
			
			if((!isset($_REQUEST['act']) || $_REQUEST['act']=="GENERATOR") && isset($this->data['financial_year'][$l['financial_year']])) {
				$txt.= " (Financial Year: ".$this->data['financial_year'][$l['financial_year']].")";
			}
			$data[$lid] = $txt;
		}
		return $data;
	}
	
	protected function listSort($d) {
		$data = array();
		$d2 = array();
		foreach($d as $e) {
			if($e['id']==1 && (strtoupper($e['name'])=="UNSPECIFIED" || strlen($e['name'])==0) ) {
				$data[$e['id']] = "[Unspecified]";
			} else {
				$d2[$e['id']] = $e['name'];
			}
		}
		natcasesort($d2);
		foreach($d2 as $e=>$f) { $data[$e] = $f; }	
		return $data;
	}
	
	public function getResultOptions() {
		$data = array();
		foreach($this->result_categories as $key => $r) {
			$data[$key] = $r['text'];
		} 
		return $data;
	}

	public function getResultSettings() {
		return $this->result_categories;
	}
	
	protected function setFields() {
		foreach($this->titles as $i => $t) {	
			$ac = isset($this->allowchoose[$i]) ? $this->allowchoose[$i] : $this->defaults['allowchoose'];	
			$df = isset($this->default_selected[$i]) ? $this->default_selected[$i] : $this->defaults['default_selected'];
			$af = isset($this->allowfilter[$i]) ? $this->allowfilter[$i] : $this->defaults['allowfilter'];
			$dt = isset($this->types[$i]) ? $this->types[$i] : $this->defaults['type'];
			$do = isset($this->data[$i]) ? $this->data[$i] : $this->defaults['data'];
			$dd = isset($this->default_data[$i]) ? $this->default_data[$i] : $this->defaults['default_data'];
			$ag = isset($this->allowgroupby[$i]) ? $this->allowgroupby[$i] : $this->defaults['allowgroupby'];
			$as = isset($this->allowsortby[$i]) ? $this->allowsortby[$i] : $this->defaults['allowsortby'];
			$sp = isset($this->sortposition[$i]) ? $this->sortposition[$i] : $this->default_sort;
			$this->report->addField($i,$t,$ac,$df,$af,$dt,$do,$dd,$ag,$as,$sp);
			$this->default_sort++;
		}
	}
	
	
	public function getSQL($db,$filter) {
		return $this->setSQL($db,$filter);
	}
	
	

}

?>