<?php

class MODULE_RGSTR_REPORT extends MODULE_GENERIC_REPORT {

	protected $folder;
	//GET FIELD DETAILS
	protected $titles;
	protected $allowchoose;
	protected $default_selected;
	protected $allowfilter;
	protected $types;
	protected $default_data;
	protected $data;
	protected $allowgroupby;
	protected $allowsortby;
	protected $default_sort;
	protected $sortposition;
	protected $db;
	
	protected $o = "O";
	protected $a = "A";

	public function __construct($p,$d,$c,$q) {
		parent::__construct($p,$d,$c,$q);
		$this->db = new ASSIST_DB("client");
	}

	
	
	protected function prepareGenerator() {
		//echo "<P>module_query_report.prepareGenerator</p>";
		$this->report->disableUpdateLogOption("AUDIT");
		if(isset($_REQUEST['quick_id'])) {
			$db = new ASSIST_DB("client");
			$quick = $db->mysql_fetch_one("SELECT * FROM ".$db->getDBRef()."_quick_report WHERE id = ".$_REQUEST['quick_id']);
			$this->report->setQuickReport($quick['id'],$quick['name'],$quick['description'],$quick['insertuser'],$quick['insertdate'],unserialize($quick['report']));
		}
		$this->getFieldDetails();
		$this->getFieldData();
		$this->displayPageHeading();
		//$this->report->disableQuickReports();
	}
	
	public function displayPageHeading() {
		//echo "<p>module_query_report.displayPageHeading</p>";
		//Navigation buttons & page title
		$header = array(0=>"",1=>"",2=>"");
		$location = explode("_",$this->page);
		$m0 = new Menu();
		$m0_menu = $m0->getAMenu("report");	//$this->arrPrint($m0_menu);
		$header[0] = "<a href='/".$_SESSION['modlocation']."/report/' class=breadcrumb>".($m0_menu['client_name'] == "" ? $m0_menu['name'] : $m0_menu['client_name'])."</a>";
		$h = array(1=>"report");
		if(isset($location[1])) { $h[2] = $location[0]; } //$this->arrPrint($h);
		foreach($h as $k=>$i) {
			$m_menu = $m0->getSubMenu($i); //$this->arrPrint($m_menu);
			$menu = array();
			foreach($m_menu as $key => $m) {
				$m['url'] = "/".$_SESSION['modlocation']."/".$this->folder."/".$m['folder'].".php";
				$m['display'] = ($m['client_name'] == "" ? $m['name'] : $m['client_name']);
				if($k==1) { $l = $location[$k-1]; } else { $l = $this->page; } 
				$m['active'] = ($m['folder'] == $l ? true : false);
				if($m['active']===true) { $header[$k]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
				$menu[$key] = $m;
			}
			if(count($menu)>0) {
				$this->drawNavButtons($menu); //$this->arrPrint($menu);
			}
		}
		foreach($header as $key => $h) { if(strlen($h)==0) { unset($header[$key]); } }
		echo "<h1>".implode(" >> ",$header)."</h1>";
	}
	
	public function setFolder($f) {
		$this->folder = $f;
	}
	
	public function getFinancialYears() {
		$data = array();
		$me = new Assist_Master_FinancialYears();
		$table = $this->db->getDBRef()."_risk_register";
		$fld = "financial_year_id";
		$d = $me->getLinked($table,$fld,"#.id as id, #.value as name");
		$data = $this->listSort($d); 
		return $data;
	}
	public function getObjects() {
		$data = array();
		if(!isset($this->data['financial_year_id']) || count($this->data['financial_year_id'])==0) { $this->data['financial_year_id'] = $this->getFinancialYears(); }
		$db = new ASSIST_DB("client");
		$sql = "SELECT O.id, O.description as name, O.financial_year_id 
				FROM ".$db->getDBRef()."_risk_register O
				LEFT OUTER JOIN ".strtolower("assist_".$this->getCmpCode()."_".ASSIST_MASTER_FINANCIALYEARS::TABLE)." FY
				ON O.financial_year_id = FY.".ASSIST_MASTER_FINANCIALYEARS::ID_FLD." AND FY.".ASSIST_MASTER_FINANCIALYEARS::STATUS_FLD." & ".ASSIST_MASTER_FINANCIALYEARS::ACTIVE." = ".ASSIST_MASTER_FINANCIALYEARS::ACTIVE."
				WHERE ".Risk::getStatusSQLForWhere("O")."
				ORDER BY O.description, FY.end_date";
		$objects = $db->mysql_fetch_all_fld($sql,"id");

		foreach($objects as $lid => $l) {
			$txt = "";
			$x = stripslashes($l['name']);
			if(!isset($_REQUEST['act']) || $_REQUEST['act']=="GENERATOR") {
				$txt = "[".MODULE_RGSTR_REPORT_OBJECT::REFTAG.$lid."] ";
				if(strlen($x)>75) {
					$x = substr($x,0,75)."...";
				}
				$txt.= $x;
				if(isset($this->data['financial_year_id'][$l['financial_year_id']]) && $this->data['financial_year_id'][$l['financial_year_id']]!="[Unspecified]") {
					$txt.= " (Financial Year: ".$this->data['financial_year_id'][$l['financial_year_id']].")";
				}
			} else {
				$txt = $x." [".MODULE_RGSTR_REPORT_OBJECT::REFTAG.$lid."]";
			}
			$data[$lid] = $txt;
		}
		return $data;
	}
	
	protected function listSort($d,$sort=true) {
		$unspecified = false;
		$data = array();
		$d2 = array();
		foreach($d as $e) {
			if($e['id']==1 && (strtoupper($e['name'])=="UNSPECIFIED" || strlen($e['name'])==0) ) {
				$data[$e['id']] = "[Unspecified]";
				$unspecified = true;
			} else {
				$d2[$e['id']] = $e['name'];
			}
		}
		if($sort) {	natcasesort($d2);	}
		if(!$unspecified) { $data[0] = "[Unspecified]"; }
		foreach($d2 as $e=>$f) { $data[$e] = $f; }	
		return $data;
	}
	
	public function getResultOptions() {
		$data = array();
		foreach($this->result_categories as $key => $r) {
			$data[$key] = $r['text'];
		} 
		return $data;
	}

	public function getResultSettings() {
		return $this->result_categories;
	}
	
	protected function setFields() {
		foreach($this->titles as $i => $t) {	
			$ac = isset($this->allowchoose[$i]) ? $this->allowchoose[$i] : $this->defaults['allowchoose'];	
			$df = isset($this->default_selected[$i]) ? $this->default_selected[$i] : $this->defaults['default_selected'];
			$af = isset($this->allowfilter[$i]) ? $this->allowfilter[$i] : $this->defaults['allowfilter'];
			$dt = isset($this->types[$i]) ? $this->types[$i] : $this->defaults['type'];
			$do = isset($this->data[$i]) ? $this->data[$i] : $this->defaults['data'];
			$dd = isset($this->default_data[$i]) ? $this->default_data[$i] : $this->defaults['default_data'];
			$ag = isset($this->allowgroupby[$i]) ? $this->allowgroupby[$i] : $this->defaults['allowgroupby'];
			$as = isset($this->allowsortby[$i]) ? $this->allowsortby[$i] : $this->defaults['allowsortby'];
			$sp = isset($this->sortposition[$i]) ? $this->sortposition[$i] : $this->default_sort;
			$this->report->addField($i,$t,$ac,$df,$af,$dt,$do,$dd,$ag,$as,$sp);
			$this->default_sort++;
		}
	}
	
	
	public function getSQL($db,$filter) {
		return $this->setSQL($db,$filter);
	}
	
	
	
	
	
	
	protected function getResponses($tbl,$id,$objects) {
		$result = array();
		$filter = strtoupper($_REQUEST['update_filter']); 
		if(count($objects)>0) {
			$sql = "SELECT * FROM ".$this->db->getDBRef()."_".$tbl."_update WHERE $id IN (".implode(",",$objects).") ORDER BY $id ASC, insertdate DESC";
			$a = $this->db->mysql_fetch_all($sql);
			$logs = array();
			foreach($a as $b) {
				$logs[$b[$id]][] = $b;
			}
			foreach($logs as $i => $x) {
				foreach($x as $l) {
					if( ($filter=="LAST" && !isset($result[$i])) || $filter!="LAST") {
						$l['changes'] = base64_decode($l['changes']);
						$c = unserialize($l['changes']);
						if( (isset($c['response']) && strlen($c['response'])>0) || (isset($c['attachments']) && count($c['attachments'])>0) || (isset($c['action_status']) && strlen($c['action_status'])>0) ) {
							$txt = "";
							$d = strtotime($l['insertdate']);
							if(isset($c['response']) && strlen($c['response'])>0) {
								$txt = stripslashes(" - ".$c['response']);
							}
							if(isset($c['attachments']) && count($c['attachments'])>0) {
								$at = array();
								foreach($c['attachments'] as $a) {
									if(strtoupper(substr($a,0,11))=="ATTACHMENT ") {
										$a = substr($a,11,strlen($a));
									}
									if(strtoupper(substr($a,-15))==" HAS BEEN ADDED") {
										$a = substr($a,0,-15);
									}
									$at[] = stripslashes($a);
								}
								if(count($at)>0) {
									$attach = true;
									$txt.= (strlen($txt)>0 ? " " : " - ")."[Attachment".(count($at)>0 ? "s" : "").": ".implode(", ",$at)."]";
								} else {
									$attach = false;
								}
							} else {
								$attach = false;
							}
							//$this->arrPrint($c);
							/*if(isset($c['action_status']) && trim(strtoupper($c['action_status']))=="ACTION HAS BEEN APPROVED") {
								if(strlen($txt)==0 && $filter!="LAST") {
									$txt.= (strlen($txt)>0 ? " " : " - ").(strlen($txt)>0 && !$attach ? "[" : "").strip_tags($c['action_status']).(strlen($txt)>0 && !$attach ? "]" : "");
								}
							}*/
							//$txt.="::::".$c['action_status'].":::";
							if(strlen($txt)>0) {
								$result[$i][] = $txt.stripslashes(" (Logged by ".$c['user']." on ".date("d-M-Y",$d)." at ".date("H:i",$d).")");
							}
						}
					}
				}
			}
		}
		return $result;
	}
		
		
	
	

}

?>