<?php

class MODULE_RGSTR_REPORT_OBJECT extends MODULE_RGSTR_REPORT {

	//CONSTRUCT
	private $date_format = "STRING";
	private $my_class_name = "MODULE_RGSTR_REPORT_OBJECT";
	private $my_quick_class_name = "MODULE_RGSTR_QUICK_REPORT";
	const REFTAG = Risk::REFTAG;
	const REPORTFILENAME = "rgstr";
	const OBJECT_DEADLINE = "deadline";	//object deadline is = max(action.deadline)
	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
		'noDeadline' => array("text" => "No Action Deadline", "code" => "noDeadline", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = ": Report";
	private $multilist_results;
	private $multitext_separator = ";";
	
	private $backupdata = array();
	
	private $rating_flds = array("inherent_risk_exposure","residual_risk");
	private $rating_values = array();
	private $rating_data = array();
	


	public function __construct($p) {	
		//echo "<P>MODULE_***_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
		$this->default_report_title = $this->getModTitle().$this->default_report_title;
	}
	
	
	
	
	/**************
	field list:
	Array
	(
		[risk_item] => Risk Item
		[risk_type] => Risk Type					=> type
		[risk_category] => Risk Category			=> category
		[risk_level] => Risk Level					=> level
		[risk_description] => Risk Description
		[risk_background] => Risk Background
		[impact] => Impact
		[impact_rating] => Impact Rating
		[likelihood] => Likelihoods
		[likelihood_rating] => Likelihood Ratings
		[inherent_risk_exposure] => Inherent risk Exposure
		[inherent_risk_rating] => Inherent risk rating
		[current_controls] => Current Controls
		[actual_financial_exposure] => Actual Financial Exposure
		[financial_exposure] => Financial Exposure
		[percieved_control_effective] => Perceived Control Effectiveness
		[action_progress] => Action Progress
		[control_rating] => Control Rating			=> control_effectiveness_rating
		[kpi_ref] => KPI Ref
		[residual_risk] => Residual Risk
		[residual_risk_exposure] => Residual Risk exposure
		[risk_owner] => Directorate
		[risk_status] => Risk Status
		[attachements] => Attachments
		[financial_year] => Financial Year
		[rbap_ref] => RBAP Ref
		[cause_of_risk] => Cause of risk
		[reasoning_for_mitigation] => Reasoning for mitigation
	)
	**************/
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}
	
	protected function getFieldDetails() {

		$headers = new Naming();
		$x = $headers->getReportingNaming("risk");
//		$this->arrPrint($x);
		unset($x['attachements']);
		$search = array('risk_item','risk_description','risk_background','risk_category','risk_owner','risk_status','percieved_control_effective','financial_year','risk_type','risk_level','control_rating');
		$replace = array('id','description','background','category','sub_id','status','percieved_control_effectiveness','financial_year_id','type','level','control_effectiveness_rating');
		$y = json_encode($x);
		$z = json_decode(str_replace($search,$replace,$y),true); 
//		$this->arrPrint($z);
		$this->titles = $z;
		$this->titles['update_log'] = "Response";
		$this->titles['date_completed'] = "Action Completed Date";
		$this->titles['result'] = "Result By Action Progress";
		$this->allowchoose = array(
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'id' => false,
			'impact_rating' => false,
			'likelihood_rating' => false,
			'inherent_risk_rating' => false,
			'control_effectiveness_rating' => false,
			'residual_risk_exposure' => false,
			'update_log'=>false,
			'date_completed'=>false,
			'action_progress' => false,
		);
		$this->types = array(
			'id' => "REF",
			'type' => "LIST",
			'category' => "LIST",
			'level' => "LIST",
			'description' => "TEXT",
			'background' => "TEXT",
			'impact' => "LIST",
			'impact_rating' => "TEXT",
			'likelihood' => "LIST",
			'likelihood_rating' => "TEXT",
			'inherent_risk_exposure' => "LIST",
			'inherent_risk_rating' => "TEXT",
			'current_controls' => "MULTITEXT",
			'actual_financial_exposure' => "NUM",
			'financial_exposure' => "LIST",
			'percieved_control_effectiveness' => "LIST",
			'control_effectiveness_rating' => "TEXT",
			'kpi_ref' => "TEXT",
			'residual_risk' => "LIST",
			'residual_risk_exposure' => "TEXT",
			'sub_id' => "LIST",
			'status' => "LIST",
			'financial_year_id' => "LIST",
			'rbap_ref' => "TEXT",
			'cause_of_risk' => "TEXT",
			'reasoning_for_mitigation' => "TEXT",
			'action_progress' => "PERC",
			'update_log'=>"LOG",
			'date_completed'=>"DATE",
			'result'=>"RESULT",
		);
		
		$this->default_data = array();
		$this->data = array(
		);
		$this->allowgroupby = array(
			'id' => false,
			'description' => false,
			'background' => false,
			'impact_rating' => false,
			'likelihood_rating' => false,
			'inherent_risk_rating' => false,
			'financial_exposure' => false,
			'percieved_control_effectiveness' => false,
			'residual_risk_exposure' => false,
			'rbap_ref' => false,
			'date_completed'=>false,
			'result'=>false,
		);
		$this->allowsortby = array(
			'id' => true,
			'type' => false,
			'category' => false,
			'level' => false,
			'description' => true,
			'background' => true,
			'impact' => false,
			'impact_rating' => true,
			'likelihood' => false,
			'likelihood_rating' => true,
			'inherent_risk_exposure' => false,
			'inherent_risk_rating' => true,
			'current_controls' => false,
			'actual_financial_exposure' => true,
			'financial_exposure' => false,
			'percieved_control_effectiveness' => false,
			'control_effectiveness_rating' => true,
			'kpi_ref' => true,
			'residual_risk' => false,
			'residual_risk_exposure' => true,
			'sub_id' => false,
			'status' => false,
			'financial_year_id' => false,
			'rbap_ref' => true,
			'cause_of_risk' => true,
			'reasoning_for_mitigation' => true,
			'action_progress' => false,
			'update_log'=>false,
			'date_completed'=>false,
			'result'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array(
			'id'=>1,
			'description' => 2,
			'background' => 3,
			'actual_financial_exposure' => 4,
			'kpi_ref' => 5,
			'rbap_ref' => 6,
			'cause_of_risk' => 7,
			'reasoning_for_mitigation' => 8,
		);

	}
	
	protected function getFieldData() {
		$this->data = array(
			'impact' 			=> $this->getClassData("Impact"),
				//'impact_rating' => "LIST",
			'likelihood' 		=> $this->getClassData("Likelihood"),
				//'likelihood_rating' => "LIST",
			'inherent_risk_exposure' =>  $this->getClassData("InherentRisk","inherent_risk_exposure"),
				//'inherent_risk_rating' => "LIST",
			'percieved_control_effectiveness' =>	$this->getClassData("PCE"),
				//'control_rating' => "LIST",
			'residual_risk' 	=> $this->getClassData("ResidualRisk","residual_risk"),
				//'residual_risk_exposure' => "LIST",
			'category'			=> $this->getClassData("Category"),
			'financial_exposure'=> $this->getClassData("FinancialExposure"),
			'level'		=> $this->getClassData("RiskLevel"),
			'type'			=> $this->getClassData("RiskType"),
			'sub_id'			=> $this->getClassData("Owner"),
			'financial_year_id'	=> $this->getFinancialYears(),
			'status'	 		=> $this->getClassData("Status"),
			'result'			=> $this->getResultOptions(),
		);
		$this->default_data = array();
		
		//$this->arrPrint($this->data['sub_id']);
	}
	private function getClassData($fld,$rf="") {
		$data = array();
		$sort = true;
		$rating = (strlen($rf)>0);
		switch($fld) {
			case "PCE":					$me = new ControlEffectiveness(  );		$sort = false; break;
			case "Impact":				$me = new Impact(  );		$sort = false; break;
			case "Likelihood":			$me = new Likelihood(  );	$sort = false; break;
			case "InherentRisk":		$me = new InherentRisk(  );	$sort = false; break;
			case "ResidualRisk":		$me = new ResidualRisk(  );	$sort = false; break;
			case "Category":			$me = new RiskCategory( "", "", "", "" );	break;
			case "FinancialExposure":	$me = new FinancialExposure( "", "", "", "", "" );	break;
			case "RiskLevel":			$me = new RiskLevel( "", "", "", "", "" );	break;
			case "RiskType":			$me = new RiskType( "", "", "", "", "" );	break;
			case "Owner":				$me = new Directorate( "", "", "", "", "" );	break;
			case "Status":				$me = new RiskStatus( "", "", "", "" );	$sort = false;	break;
		}
		$d = $me->getReportList((!isset($_REQUEST['act']) || $_REQUEST['act']=="GENERATOR" ? "" : "OUT" ));
		$data = $this->listSort($d,$sort);
		if($rating) { $this->rating_values[$rf] = $d; }
		return $data;
	}


	
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(self::REPORTFILENAME);
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(self::REPORTFILENAME);
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r['id'];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$action_progress = array();
		
		$sql = $this->setSQL();
		if(strlen($sql)>0) {
			$rows = $this->db->mysql_fetch_all_fld($sql,"id");
			if( (isset($_REQUEST['columns']['action_progress']) && strtoupper($_REQUEST['columns']['action_progress'])=="ON") || (isset($_REQUEST['columns']['result']) && strtoupper($_REQUEST['columns']['result'])=="ON") ) {
				$action_progress = $this->getActionProgress(array_keys($rows));
			}
			if(isset($_REQUEST['columns']['update_log'])) {
				$response = $this->getResponses("risk","risk_id",array_keys($rows));
			}
			foreach($this->rating_flds as $rf) {
				if(isset($_REQUEST['columns'][$rf])) {
					$this->rating_data[$rf] = array();
					$this->rating_data[$rf] = $this->getRatingData($rf);
				}
			}
			//$this->arrPrint($this->rating_flds);
			//$this->arrPrint($this->rating_values);
			//$this->arrPrint($this->rating_data);
			foreach($rows as $key => $r) {
				$my_result = isset($action_progress[$key]) ?  ($action_progress[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
				$r['action_progress'] = $my_result['action_progress'];
				$r['date_completed'] = ($my_result['action_progress']<100 ? "" : $my_result['date_completed']);
				$r['result'] = $this->getCompliance($my_result,(isset($my_result[self::OBJECT_DEADLINE])?$my_result[self::OBJECT_DEADLINE]:""));
				$r['update_log'] = isset($response[$key]) ? $response[$key] : array();
				foreach($this->rating_flds as $rf) {
					if(isset($_REQUEST['columns'][$rf])) {
						$irr = ceil($r[$rf]);
						$ir = isset($this->rating_data[$rf][$irr]) ? $this->rating_data[$rf][$irr] : 0;
						$r[$rf] = $ir;
					}
				}
				
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r['id']] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								$d = $d;
								break;
							case "LIST":
							case "TEXTLIST":
							case "MULTILIST":
								$d2 = $d;
								if(strlen($d)==0) {
									$d = "Unspecified";
								} else {
									$x = explode(",",$d);
									$d = "";
									$z = array();
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										} elseif(isset($this->backupdata[$i][$a])) {
											$z[] = $this->backupdata[$i][$a];
										} 
									}
									$d = count($z)>0 ? implode(", ",$z) : "Unspecified";//$d2;//"Unknown";
								}
								//$d.=" [".$d2."]";
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							//if(is_array(($d))) { echo $i; }
							//$final_rows[$r['id']][$i] = stripslashes($d);
							if($type!="LOG") {
								$final_rows[$r['id']][$i] = stripslashes($d);
							} else {
								$final_rows[$r['id']][$i] = $d;
							}
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		
		//$this->arrPrint($final_rows);
		
		return $final_rows;
	
	}
	
	public function getRatingData($fld) {
		$rv = $this->rating_values[$fld]; //echo $fld; $this->arrPrint($rv);
		$d = array();
		foreach($rv as $r) {
			for($i=$r['rating_from'];$i<=$r['rating_to'];$i++) { $d[$i] = $r['id']; }
		}
		return $d;
	}
	
	public function getCompliance($ap,$d) {
		$result = "notCompletedAndOverdue";
		if(strlen($d) > 0) {
			$deadline = strtotime($d);
			$p = $ap['action_progress'];
			$dc = strtotime($ap['date_completed']);
			if($p<100) {	//incomplete
				if(strtotime(date("d F Y")) > $deadline) {
					$result = "notCompletedAndOverdue";
				} else {
					$result = "notCompletedAndNotOverdue";
				}
			} else {		//complete
				if($dc < $deadline) {
					$result = "completedBeforeDeadline";
				} elseif($dc>$deadline) {
					$result = "completedAfterDeadline";
				} else {
					$result = "completedOnDeadlineDate";
				}
			}
		} else {
			$result = "noDeadline";
		}
		return $result;
		
	}
	
	public function getActionProgress($objects) {
		$a = $this->a;
		$o = $this->o;
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT U.*, ".$a.".risk_id 
			FROM ".$this->db->getDBRef()."_actions_update U 
			INNER JOIN ".$this->db->getDBRef()."_actions ".$a." 
				ON U.action_id = ".$a.".id 
			WHERE ".Riskaction::getStatusSQLForWhere($a)." 
			AND ".$a.".risk_id IN (".implode(",",$objects).") 
			AND ".$a.".progress > 0 
			ORDER BY ".$a.".risk_id ASC
			, U.action_id ASC
			, U.insertdate DESC"; 
			$rows = $this->db->mysql_fetch_all($sql); 
			$updates = array();
			foreach($rows as $r) {
				$i = $r['risk_id'];
				$c = unserialize(base64_decode($r['changes']));  
				if(isset($c['action_progress'])) {
					if(isset($c['date_completed']['to']) && strlen($c['date_completed']['to'])>0 && strtotime($c['date_completed']['to'])>86401) {
						$dc = date("d F Y H:i:s",strtotime($c['date_completed']['to']));
					} else {
						$dc = date("d F Y H:i:s",strtotime($r['insertdate']));
					}
					if(!isset($updates[$i])) {
						$updates[$i] = $dc;
					} elseif(strtotime($updates[$i])<strtotime($dc)) {
						$updates[$i] = $dc;
					}
				}
			}

			$sql = "SELECT
			".$a.".risk_id as object
			, max(STR_TO_DATE(".$a.".deadline,'%d-%b-%Y')) as deadline
			, count(".$a.".id) as count
			, sum(".$a.".progress) as total
			, avg(".$a.".progress) as average
			, max(".$a.".action_on) as last_date
			FROM `".$this->db->getDBRef()."_actions` ".$a."
			INNER JOIN ".$this->db->getDBRef()."_risk_register ".$o."
			ON ".$a.".risk_id = ".$o.".id
			WHERE ".$a.".risk_id IN (".implode(",",$objects).")
			AND ".$a.".active = 1
			group by ".$a.".risk_id";
			$res = $this->db->mysql_fetch_all_fld($sql,"object");		

			foreach($res as $d => $rs) {
				$r = "N/A";
				if(isset($updates[$d])) { 
					$r = $updates[$d];
				}
				if($r!="N/A" && strtotime($r)>strtotime($rs['last_date'])) {
					$dc = strtotime($r);
				} else {
					$dc = strtotime($rs['last_date']);
				}
				$result[$d] = array(
					'action_progress'=>$rs['average'],
					'date_completed'=>date("d M Y",$dc),
					'deadline'=>date("d M Y",strtotime($rs['deadline']." 23:59:59"))
				);
			}
			
			
		}
		return $result;
	}
	
	

	protected function setSQL($filter=array()) {
		$x = $this->o;	//object alias in SQL
		$a = $this->a;	//action alias in SQL
		$sql_status = Risk::getStatusSQLForWhere($x);
		$sql = "";
		$sql = "
		SELECT ".$x.".*
		FROM ".$this->db->getDBRef()."_risk_register ".$x." 
		WHERE ".$sql_status; //echo $sql;
		//$this->arrPrint($_REQUEST);
		if(!isset($this->titles['id'])) { $this->getFieldDetails(); }
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; 
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY ".$x.".id ";
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						/*case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;*/
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$s = "";
					switch($i) {
/*						case '': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$s = $this->report->getFilterSql("D",$t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;*/
						case 'action_progress':	
						case 'date_completed':
						case 'result':			
							break;	//do nothing; filter applied in row processing
						default:				
							$s = $this->report->getFilterSql($x,$t,$f,$ft,$i);
					}
					if(strlen($s)>0) { $where[] = $s; }
				}
			}
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		if(count($having)>0) {
			$sql.= $sql_group;
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		
		$sql.=$this->getSortBySql();

		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$o = $this->o;
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array();
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				switch($s) {
					case "risk_date":
					case "risk_deadline_date":
						$sort_by[] = "STR_TO_DATE(".$o.".".$s.",'%d-%b-%Y')";
						break;
					case "inherent_risk_exposure":
					case "inherent_risk_rating":
					case "residual_risk":
					case "residual_risk_exposure":
					case "control_effectiveness_rating":
					case "control_rating":
						$sort_by[] = "CAST(".$o.".".$s." AS DECIMAL(5,2))";
						break;
					default:
						$sort_by[] = $o.".".$s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	

}

?>