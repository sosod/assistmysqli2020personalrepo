<?php

class MODULE_COMPL_REPORT_FIXED extends MODULE_GENERIC_REPORT_FIXED {

	//CONSTRUCT
	private $page;
	private $folder = "report";
	private $date_format = "STRING";
	private $my_class_name = "MODULE_COMPL_REPORT_FIXED";
	
	private $my_reports;
	private $my_filters;
	private $use_a_filter = true;
	
	private $compl_report;
	
	private $active_report_title;

	public function __construct($p) {	
		$this->page = $p;
		parent::__construct($p,$this->date_format,$this->my_class_name);
		
		$legislative = $this->getCmpCode()=="eay999d" ? "Contract" : "Legislative";
		$legislative2 = $this->getCmpCode()=="eay999d" ? "Contract" : "Legislation";
		$this->my_reports = array(
			1=>array(
					'id'=>1,
					'name'=>"Compliance Analysis Dashboard",
					'descrip'=>"Overview of $legislative and Deliverable Compliance measured by Action Progress and Action Compliance measured by Action Status",
					'format'=>"Pie",
					'code'=>"dashboard",
				),
			2=> array(
					'id'=>2,
					'name'=>"$legislative2 Compliance Status (Action Progress)",
					'descrip'=>"$legislative Compliance per Action measured by Action Progress",
					'format'=>"Bar",
					'code'=>"legislation_compliance_status",
				),
			3=>array(
					'id'=>3,
					'name'=>"$legislative2 Compliance Status (Deliverable Status)",
					'descrip'=>"$legislative Compliance per Deliverable measured by Deliverable Status",
					'format'=>"Bar",
					'code'=>"legistion_compliance_status_per_deliverable",
				),
			4=>array(
					'id'=>4,
					'name'=>"Action Compliance per Person",
					'descrip'=>"Action Compliance per Person measured by Action Status",
					'format'=>"Bar",
					'code'=>"action_progress_per_person",
				),
			5=>array(
					'id'=>5,
					'name'=>"Deliverable Compliance Status per Reporting Category (Action Progress)",
					'descrip'=>"Deliverable Compliance Status per Reporting Category measured by Action Progress",
					'format'=>"Bar",
					'code'=>"deliverable_compliance_per_reporting_category",
				),
		);
		
		$this->compl_report = new MODULE_COMPL_REPORT($this->page,$this->date_format,$this->my_class_name,"");
		$this->compl_report->setFolder($this->folder);
		$this->my_filters = $this->compl_report->getFinancialYears();
		
	}
	
	
	protected function prepareGenerator() {
		$this->drawPageHeader();

		foreach($this->my_reports as $r) {
			$this->setReport($r['name'],$r['descrip'],$r['format'],$r['code']);
		}
		
		foreach($this->my_filters as $key=>$f) {
			$this->setFilter($key."_ALL",$f." (All Legislations)");
			$this->setFilter($key."_CLIENT",$f." (Client Created Legislations)");
			$this->setFilter($key."_MASTER",$f." (Imported Master Legislations)");
		}
		
		$this->setUseFilter($this->use_a_filter);
		
		
	}
	
	protected function drawPageHeader() {
		$this->compl_report->displayPageHeading();
	}
	
	protected function prepareReport() {
		//$this->drawPageHeader();
		
		$report = $_REQUEST['code'];
		$rprt = explode("_",$report);
		$report = "";
		foreach($rprt as $r) { $report.=ucwords($r); }
		
		$f = explode("_",$_REQUEST['filter']);
		$financial_year = $f[0];
		$legislation_type = isset($f[1]) ? $f[1] : "ALL";
		
		foreach($this->my_reports as $r) {
			if($r['code']==$_REQUEST['code']) {
				$this->active_report_title = $r['name'];
				break;
			}
		}
		
		$my_function = "self::get".$report;
		$this->reporting_data = call_user_func($my_function,$financial_year,$legislation_type);
		
	}
	
	protected function getReportTitle() { return $this->active_report_title; }
	
	
	private function getDashboard($f,$t) { 
		$data = array(
					'settings'=>array(
							'layout'=>"P",
							'hasMain'=>true,
							'hasSub'=>false,
							'manualPageTitle'=>true,
						),
					'legislation'=>array(),'deliverable'=>array(),'action'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "legislation";
		$leg = new MODULE_COMPL_REPORT_LEGISLATION($_REQUEST['page']);
		$leg->fixedReport();
		$data[$graph]['graph_title'] = "Legislation Compliance Summary (Action Progress)";
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $leg->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$options = array('financial_year'=>array($f),'filter_legislation_type'=>$t);
		if($f=="ANY") { unset($options['financial_year']); }
		
		$lsql = $leg->getSQL($db,$options); 
		$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		$lkeys = array_keys($lrows);
		$lap = $leg->getActionProgress($db,array_keys($lrows));  //$this->arrPrint($lap);
		foreach($lrows as $key => $r) {
			$my_action_progress = isset($lap[$key]) ?  ($lap[$key]) : array();
			$res = $leg->getCompliance($my_action_progress);
			$data[$graph]['data'][$res]++;
		}


		//DELIVERABLES
		$graph = "deliverable";
		$del = new MODULE_COMPL_REPORT_DELIVERABLE($_REQUEST['page']);
		$del->fixedReport();
		$data[$graph]['graph_title'] = "Deliverable Compliance Summary (Action Progress)";
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $del->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$dsql = $del->getSQL($db,$options);
		//echo $dsql;
		$drows = $db->mysql_fetch_all_fld($dsql,"id");
		$dkeys = array_keys($drows);
		$dap = $del->getActionProgress($db,array_keys($drows));
		foreach($drows as $key => $r) {
			$my_result = isset($dap[$key]) ?  ($dap[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
			$my_result['deliverable_status'] = $r['deliverable_status'];
			$res = $del->getCompliance($my_result,$r['legislation_deadline']);
			$data[$graph]['data'][$res]++;
		}

		//ACTIONS
		$graph = "action";
		$a = new MODULE_COMPL_REPORT_ACTION($_REQUEST['page']);
		$a->fixedReport();
		$data[$graph]['graph_title'] = "Action Compliance Summary (Action Status)";
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "PIE";
		$data[$graph]['result_options'] = $a->getResultSettings();
		$data[$graph]['data'] = array();
		foreach($data[$graph]['result_options'] as $key => $r) {
			$data[$graph]['data'][$key] = 0;
		}
		$asql = $a->getSQL($db,$options);
		$arows = $db->mysql_fetch_all_fld($asql,"id");
		$akeys = array_keys($arows);
		$aap = $a->getActionProgress($db,array_keys($arows));
		foreach($arows as $key => $r) {
			$my_result = isset($aap[$key]) ?  ($aap[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
			$res = $a->getCompliance($my_result,$r['deadline'],$r['status']);
			$data[$graph]['data'][$res]++;
		}
//$this->arrPrint($data);
		return $data;
	}
	
	private function getLegislationComplianceStatus($f,$t) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'legislation'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "legislation";
		$leg = new MODULE_COMPL_REPORT_LEGISLATION($_REQUEST['page']);
		$act = new MODULE_COMPL_REPORT_ACTION($_REQUEST['page']);
		$leg->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $act->getResultSettings();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get legislations for given financial year
		$options = array('financial_year'=>array($f),'filter_legislation_type'=>$t);
		if($f=="ANY") { unset($options['financial_year']); }
		$lsql = $leg->getSQL($db,$options);
		$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		$lkeys = array_keys($lrows);
		//set default results
		foreach($lkeys as $k) {
			$data[$graph]['subs'][$k] = $lrows[$k]['name'];
			foreach($data[$graph]['result_options'] as $key => $r) {
				$data[$graph]['data'][$k][$key] = 0;
			}
		}
		//get associated action ids => returns a.id, l.id as leg, a.deadline, a.status
		$lact = $leg->getLegislationActions($db,$lkeys);
		//get action progress & date completed for associated actions
		$aap = $act->getActionProgress($db,array_keys($lact));
		foreach($lact as $key => $r) {
			$my_result = isset($aap[$key]) ?  ($aap[$key]) : array('action_progress'=>0,'date_completed'=>"N/A",'status'=>1);
			$res = $act->getCompliance($my_result,$r['deadline'],$r['status']);
			$data[$graph]['data'][$r['leg']][$res]++;
		}
//		$this->arrPrint($data);
		return $data;
	}
	
	private function getLegistionComplianceStatusPerDeliverable($f,$t) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'legislation'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "legislation";
		$leg = new MODULE_COMPL_REPORT_LEGISLATION($_REQUEST['page']);
		$del = new MODULE_COMPL_REPORT_DELIVERABLE($_REQUEST['page']);
		$leg->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $del->getDeliverableStatusOptions();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get legislations for given financial year
		$options = array('financial_year'=>array($f),'filter_legislation_type'=>$t);
		if($f=="ANY") { unset($options['financial_year']); }
		$lsql = $leg->getSQL($db,$options);
		$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		$lkeys = array_keys($lrows);
		//set default results
		foreach($lkeys as $k) {
			$data[$graph]['subs'][$k] = $lrows[$k]['name'];
			foreach($data[$graph]['result_options'] as $key => $r) {
				$data[$graph]['data'][$k][$key] = 0;
			}
		}
		//get associated deliverables and status => returns d.id as id, l.id as leg, d.deliverable_status as status
		$ldel = $leg->getLegislationDeliverables($db,$lkeys);
		foreach($ldel as $key => $r) {
			$data[$graph]['data'][$r['leg']][$r['status']]++;
		}
//if($db->isSupportUser()) { echo "<pre>"; print_r($data); echo "</pre>"; }
		return $data;	
	}
	
	private function getActionProgressPerPerson($f,$t) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'action'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "action";
		$leg = new MODULE_COMPL_REPORT_LEGISLATION($_REQUEST['page']);
		$act = new MODULE_COMPL_REPORT_ACTION($_REQUEST['page']);
		$leg->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $act->getResultSettings();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get actions for given financial year
		$options = array('financial_year'=>array($f),'filter_legislation_type'=>$t);
		if($f=="ANY") { unset($options['financial_year']); }
				
		$lsql = $leg->getSQL($db,$options);
		$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		$lkeys = array_keys($lrows);
		//get associated action ids => returns a.id, l.id as leg, a.deadline, a.status
		$lact = $leg->getLegislationActions($db,$lkeys,"a.owner");
		$subs = $act->getActionOwners();
		//set default results
		foreach($subs as $k=>$a) {
			$data[$graph]['subs'][$k] = $a;
			foreach($data[$graph]['result_options'] as $key => $r) {
				$data[$graph]['data'][$k][$key] = 0;
			}
		}
		//get action progress & date completed for associated actions
		$aap = $act->getActionProgress($db,array_keys($lact));
		foreach($lact as $key => $r) {
			if(isset($data[$graph]['data'][$r['owner']])) {
				$my_result = isset($aap[$key]) ?  ($aap[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
				$res = $act->getCompliance($my_result,$r['deadline'],$r['status']);
				$data[$graph]['data'][$r['owner']][$res]++;
			}
		}
//		$this->arrPrint($data);
		return $data;
	}
	
	private function getDeliverableCompliancePerReportingCategory($f,$t) { 
		$data = array();
		$data = array(
					'settings'=>array(
							'manualPageTitle'=>false,
							'layout'=>"L",
							'hasMain'=>true,
							'hasSub'=>true,
							'bar'=>array(
								'format'=>"100",
								'grid'=>false,
							),
						),
					'legislation'=>array());
		$db = new ASSIST_DB("client");
		//LEGISLATION
		$graph = "legislation";
		$leg = new MODULE_COMPL_REPORT_LEGISLATION($_REQUEST['page']);
		$del = new MODULE_COMPL_REPORT_DELIVERABLE($_REQUEST['page']);
		$leg->fixedReport();
		$data[$graph]['page_title'] = $this->getReportTitle();
		$b = "";
		if(isset($t) && in_array($t,array("CLIENT","MASTER"))) {
			$b = "displaying ".($t=="CLIENT" ? "Client Created" : "Imported Master")." Legislations<br />";
		}
		$data[$graph]['blurb'] = $b.($f!="ANY" ? "for Financial Year: ".$this->my_filters[$f] : "");
		$data[$graph]['type'] = "BAR";
		$data[$graph]['result_options'] = $del->getResultSettings();
		$data[$graph]['data'] = array();
		$data[$graph]['subs'] = array();
		//get legislations for given financial year
		$options = array('financial_year'=>array($f),'filter_legislation_type'=>$t);
		if($f=="ANY") { unset($options['financial_year']); }
				
		$lsql = $leg->getSQL($db,$options);
		$lrows = $db->mysql_fetch_all_fld($lsql,"id");
		$lkeys = array_keys($lrows);
		$ldel = $leg->getLegislationDeliverables($db,$lkeys,"d.reporting_category");
		$subs = $del->getReportingCategory();
		//set default results
		foreach($subs as $k=>$a) {
			$data[$graph]['subs'][$k] = $a;
			foreach($data[$graph]['result_options'] as $key => $r) {
				$data[$graph]['data'][$k][$key] = 0;
			}
		}
		$dap = $del->getActionProgress($db,array_keys($ldel));
		foreach($ldel as $key => $r) {
			if(isset($data[$graph]['data'][$r['reporting_category']])) {
				$my_result = isset($dap[$key]) ?  ($dap[$key]) : array('action_progress'=>0,'date_completed'=>"N/A");
				$res = $del->getCompliance($my_result,$r['legislation_deadline']);
				$data[$graph]['data'][$r['reporting_category']][$res]++;
			}
		}
		return $data;	
	}
	

	

}

?>