<?php

class MODULE_COMPL_REPORT_LEGISLATION extends MODULE_COMPL_REPORT {

	//CONSTRUCT
	private $date_format = "STRING";
	private $my_class_name = "MODULE_COMPL_REPORT_LEGISLATION";
	private $my_quick_class_name = "MODULE_COMPL_QUICK_REPORT";
	const REFTAG = "L";
	

	
	protected $result_categories = array(
			'compliant'=>array("text" => "Compliant", "code" => "compliant", "color" => "green"),
			'notcompliant'=> array("text" => "Not Compliant", "code" => "notcompliant", "color" => "red"),
			'unableToComply' => array("text" => "Unable To Comply", "code" => "unableToComply", "color" => "charcoal"),
		);

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Compliance Assist: Legislation Report";
	
	
	


	public function __construct($p) {	
		//echo "<P>MODULE_COMPL_REPORT_LEGISLATION.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	
	protected function prepareGenerator() {
		parent::prepareGenerator();
		/*if(isset($_REQUEST['quick_id'])) {
			$db = new ASSIST_DB("client");
			$quick = $db->mysql_fetch_one("SELECT * FROM ".$db->getDBRef()."_quick_report WHERE id = ".$_REQUEST['quick_id']);
			$this->report->setQuickReport($quick['id'],$quick['name'],$quick['description'],$quick['insertuser'],$quick['insertdate'],unserialize($quick['report']));
		}
		$this->getFieldDetails();
		$this->getFieldData();
		//Navigation buttons & page title
		$header = array(0=>"",1=>"",2=>"");
		$m0 = new Menu();
		$m0_menu = $m0->getMenu("AND parent_id = 0 AND name='report'");
		$key = array_keys($m0_menu); $key = $key[0];
		$m0_menu = $m0_menu[$key];
		$header[0] = "<a href='/".$_SESSION['modlocation']."/report/' class=breadcrumb>".($m0_menu['client_terminology'] == "" ? $m0_menu['ignite_terminology'] : $m0_menu['client_terminology'])."</a>";
		$m1 = new UpperMenu($this->folder, $this->page);
		$m1_menu = $m1->createMenu();
		$menu = array();
		foreach($m1_menu as $key => $m) {
			$m['url'] = "/".$_SESSION['modlocation']."/".$this->folder."/".$m['url'];
			if($m['active']=="Y") { $header[1]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
			$menu[$key] = $m;
		}
		$this->drawNavButtons($menu);
		$m2 = new LowerMenu($this->folder, $this->page);
		$m2_menu = $m2->createMenu();
		$menu = array();
		foreach($m2_menu as $key => $m) {
			$m['url'] = "/".$_SESSION['modlocation']."/".$this->folder."/".$m['url'];
			if($m['active']=="Y") { $header[2]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
			$menu[$key] = $m;
		}
		$this->drawNavButtons($menu);
		echo "<h1>".implode(" >> ",$header)."</h1>";*/
	}


	protected function getFieldDetails() {
		//echo "<P>COMPL.getFieldDetails()</p>";

		$this->titles['id'] = "Ref";
		$headers = new LegislationNaming();
		$x = $headers->getHeaderList();
		unset($x['id']);
		$this->titles+=$x;
//		$this->titles['authorisers'] = "Legislation Authorisers";
//		$this->titles['admins'] = "Legislation Administrators";
		$this->titles['result'] = "Compliance Status";
		$this->titles['filter_legislation_type'] = "Legislation Source";
		$this->allowchoose = array(
			'business_patner'		=> false,
			'filter_legislation_type'=>false,
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'id'					=> false,
			'business_patner'		=> false,
			'action_progress'		=> false,
		);
		$this->types = array(
			'id' 					=> "REF",
			'name' 					=> "TEXT",
			'financial_year' 		=> "LIST",
			'reference' 			=> "TEXT",
			'type' 					=> "LIST",
			'category' 				=> "TEXTLIST",
			'organisation_size' 	=> "TEXTLIST",
			'legislation_date' 		=> "DATE",
			'legislation_number' 	=> "TEXT",
			'organisation_type' 	=> "TEXTLIST",
			'organisation_capacity'	=> "TEXTLIST",
			'hyperlink_to_act'		=> "LINK",
			'business_patner'		=> "LIST",
			'action_progress'		=> "PERC",
			'status' 				=> "LIST",
			'autorisers'			=> "USER",
			'admins'				=> "USER",
			'result'				=> "RESULT",
			'filter_legislation_type'=>"SMLLIST",
		);
		
		
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array(
			'id'=>false,
			'name'=>false,
			'financial_year'=>false,
			'reference'=>false,
			'legislation_date'=>false,
			'legislation_number'=>false,
			'hyperlink_to_act'=>false,
			'business_patner'=>false,
			'action_progress'=>false,
			'filter_legislation_type'=>false,
		);
		$this->allowsortby = array(
			'financial_year'=>false,
			'hyper_link_to_act'=>false,
			'business_patner'=>false,
			'action_progress'=>false,
			'result'=>false,
			'category' 				=> false,
			'organisation_size' 	=> false,
			'organisation_type' 	=> false,
			'organisation_capacity'	=> false,
			'legislation_status'	=> false,
			'filter_legislation_type'=>false,
		);
		$this->default_sort = 100;
		$this->sortposition = array(
			'id' 					=> 1,
			'name' 					=> 2,
			'financial_year' 		=> 3,
			'reference' 			=> 4,
			'type' 					=> 5,
			'category' 				=> 6,
			'organisation_size' 	=> 7,
			'legislation_date' 		=> 8,
			'legislation_number' 	=> 9,
			'organisation_type' 	=> 10,
			'organisation_capacity'	=> 11,
			'hyperlink_to_act'		=> 12,
			'business_patner'		=> 13,
			'action_progress'		=> 14,
			'status' 				=> 15,
		);
	
	}
	
	protected function getFieldData() {
		$this->data = array(
			'financial_year' 		=> $this->getFinancialYears(),
			'type'					=> $this->getLegTypes(),
			'category'				=> $this->getCategories(),
			'organisation_size'		=> $this->getOrgSizes(),
			'organisation_type'		=> $this->getOrgTypes(),
			'organisation_capacity'	=> $this->getOrgCapacities(),
			'business_patner'		=> array(),
			'status'				=> $this->getLegStatus(),
			'result'				=> $this->getResultOptions(),
			'filter_legislation_type'=> array('ALL'=>"All Legislations",'CLIENT'=>"Client Created Legislations",'MASTER'=>"Imported Master Legislations"),
		);
		$this->default_data = array('filter_legislation_type'=>"ALL");
	}
		

	private function getLegTypes() {
		$data = array();
		$me = new LegislationType();
		$d = $me->getAll();
		$data = $this->listSort($d);
		return $data;
	}

	private function getCategories() {
		$data = array();
		$me = new LegislationCategory();
		$d = $me->getAll();
		$data = $this->listSort($d);
		return $data;
	}

	private function getOrgSizes() {
		$data = array();
		$me = new OrganisationSize();
		$d = $me->getAll();
		$data = $this->listSort($d);
		return $data;
	}

	private function getOrgTypes() {
		$data = array();
		$me = new LegislationOrganisationType();
		$d = $me->getAll();
		$data = $this->listSort($d);
		return $data;
	}

	private function getOrgCapacities() {
		$data = array();
		$me = new OrganisationCapacity();
		$d = $me->getAll();
		$data = $this->listSort($d);
		return $data;
	}

	private function getLegStatus() {
		$data = array();
		$me = new Legislationstatus();
		$d = $me->getAll();
		foreach($d as $e) {
			$data[$e['id']] = $e['name'];
		}
		return $data;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName("legislation");
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName("legislation");
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r['id'];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,"id");
			if( (isset($_REQUEST['columns']['action_progress']) && strtoupper($_REQUEST['columns']['action_progress'])=="ON") || (isset($_REQUEST['columns']['result']) && strtoupper($_REQUEST['columns']['result'])=="ON") ) {
				$action_progress = $this->getActionProgress($db,array_keys($rows));
			}
			foreach($rows as $key => $r) {
				$my_action_progress = isset($action_progress[$key]) ?  ($action_progress[$key]) : array();
				$r['action_progress'] = $my_action_progress['average'];
				$r['result'] = $this->getCompliance($my_action_progress);
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r['id']] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								$d = $d;
								break;
							case "LIST":
							case "TEXTLIST":
								$d2 = $d;
								$x = explode(",",$d);
								$d = "";
								$z = array();
								if($i=="status") {
									$z[] = (!isset($this->data[$i][$a])) ? $this->data[$i][1] : $this->data[$i][$a];	//default to new for unknown status
								} else {
									foreach($x as $a) {
										if(isset($this->data[$i][$a])) {
											$z[] = $this->data[$i][$a];
										}
									}
								}
								$d = count($z)>0 ? implode(", ",$z) : "Unspecified";
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r['id']][$i] = stripslashes($d);
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap) { //$this->arrPrint($ap);
		$p = isset($ap['average']) ? $ap['average'] : 0;
		$s = isset($ap['action_status']) ? $ap['action_status'] : 1;
		if($s==0) {
			$result = "unableToComply";
		} elseif($p>=100) {
			$result = "compliant";
		} else {
			$result = "notcompliant";
		}
		
		return $result;
		
	}
	
	public function getActionProgress($db,$legislations) {
		$result = array();
		if(count($legislations)>0) {
			$sql = "SELECT l.id, l.name, l.financial_year, COUNT( a.id ) as actions, SUM( a.progress ) as progress, MIN(a.status) as action_status, AVG(a.progress) as average
			FROM  `".$db->getDBRef()."_legislation` l
			INNER JOIN ".$db->getDBRef()."_deliverable d ON d.legislation = l.id
			INNER JOIN ".$db->getDBRef()."_action a ON a.deliverable_id = d.id
			WHERE ".Legislation::getStatusSQLForWhere("l")." AND ".Deliverable::getStatusSQLForWhere("d")." AND ".Action::getStatusSQLForWhere("a")."
			AND l.id IN (".implode(",",$legislations).")
			GROUP BY l.id
			"; //echo $sql;		
			$result = $db->mysql_fetch_all_fld($sql,"id");
		}
		return $result;
	}

	public function getLegislationDeliverables($db,$legislations,$extra="") {
		$result = array();
		if(count($legislations)>0) {
			$sql = "SELECT d.id as id, l.id as leg, d.deliverable_status as status, d.legislation_deadline ".(strlen($extra)>0 ? ", ".$extra : "")."
			FROM  `".$db->getDBRef()."_legislation` l
			INNER JOIN ".$db->getDBRef()."_deliverable d ON d.legislation = l.id
			WHERE ".Legislation::getStatusSQLForWhere("l")." AND ".Deliverable::getStatusSQLForWhere("d")."
			AND l.id IN (".implode(",",$legislations).")
			"; //echo $sql;		
			$result = $db->mysql_fetch_all_fld($sql,"id");
		}
		return $result;
	}
	public function getLegislationActions($db,$legislations,$extra="") {
		$result = array();
		if(count($legislations)>0) {
			$sql = "SELECT a.id as id, l.id as leg, a.deadline, a.status ".(strlen($extra)>0 ? ", ".$extra : "")."
			FROM  `".$db->getDBRef()."_legislation` l
			INNER JOIN ".$db->getDBRef()."_deliverable d ON d.legislation = l.id
			INNER JOIN ".$db->getDBRef()."_action a ON a.deliverable_id = d.id
			WHERE ".Legislation::getStatusSQLForWhere("l")." AND ".Deliverable::getStatusSQLForWhere("d")." AND ".Action::getStatusSQLForWhere("a")."
			AND l.id IN (".implode(",",$legislations).")
			"; //echo $sql;		
			$result = $db->mysql_fetch_all_fld($sql,"id");
		}
		return $result;
	}
	
	
	protected function setSQL($db,$filter=array()) {
		$sql = "";
		$sql = "SELECT L.* FROM ".$db->getDBRef()."_legislation L WHERE ".Legislation::getStatusSQLForWhere("L"); //echo $sql;
		//$this->arrPrint($_REQUEST);
		if(!isset($this->titles['id'])) { $this->getFieldDetails(); } 
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; //$this->arrPrint($filter);
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$s = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
				$a = "";
				switch($i) {
					case 'category': 				
					case 'organisation_size': 	
					case 'organisation_type': 	
					case 'organisation_capacity':	
						if(count($f)>0 && $f[0]!="X") {
							$a = $this->report->getFilterSql("L",$t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
						}
						break;
					case 'action_progress':	break;	//do nothing; filter applied in row processing
					case 'result':			break;	//do nothing; filter applied in row processing
					case 'filter_legislation_type':			break;	//do nothing; filter applied later by calling sourceLegislation() function
					default:				
						$a =  $this->report->getFilterSql("L",$t,$f,$ft,$i);
				}
				if(strlen($a)>0) { $s[] = $a; }
			}
		}
		//$this->arrPrint($s);
		if(count($s)>0) {
			$sql.= " AND ".implode(" AND ",$s);
		}
		
		$sql.=$this->sourceLegislation("L",$filters);
		
		$sql.=$this->getSortBySql();
		
		//ASSIST_HELPER::arrPrint($filter);
		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		if(is_array($sort) && count($sort)>0) {
			$sf = array_flip($sort);
			$ld = $sf['legislation_date'];
			$sort[$ld] = "STR_TO_DATE(legislation_date,'%d-%b-%Y')";
			$sql = " ORDER BY ".implode(",",$sort);
		}
		return $sql;
	}
	

}

?>