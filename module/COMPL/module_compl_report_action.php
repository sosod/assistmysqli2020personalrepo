<?php

class MODULE_COMPL_REPORT_ACTION extends MODULE_COMPL_REPORT {

	//CONSTRUCT
	private $date_format = "STRING";
	private $my_class_name = "MODULE_COMPL_REPORT_ACTION";
	private $my_quick_class_name = "MODULE_COMPL_QUICK_REPORT";
	private $update_log_options = array("LAST","ALL");
	const REFTAG = "A";
	const REPORT_FILENAME = "action";

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'unableToComply' => array("text" => "Unable To Comply", "code" => "unableToComply", "color" => "charcoal"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),
	

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Compliance Assist: Action Report";
	private $multilist_results;
	
	private $backupdata = array();
	
	


	public function __construct($p) {	
		//echo "<P>MODULE_COMPL_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "report";
		//$this->arrPrint($_REQUEST);
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}
	
	protected function getFieldDetails() {

/**** HEADINGS AS AT 9 FEBRUARY 2013
    [deadline] => Action Deadline Date
    [action_deliverable] => Action Deliverables
    [id] => Action Reference
    [reminder] => Reminder Date
    [status] => Action Status
    [action] => Action Required
    [owner] => Action Owner
    [progress] => Action Progress
****************/

	
	
		$headers = new ActionNaming();
		$this->titles['id'] = "Ref";
		$this->titles['financial_year'] = "Financial Year";
		$this->titles['legislation_id'] = $this->getCmpCode()=="eay999d" ? "Contract" : "Legislation";
		$this->titles['deliverable_id'] = "Deliverable";
		$x = $headers->getHeaderList();
		unset($x['id']);
		$this->titles+=$x;
		//$this->arrPrint($this->titles);
		unset($this->titles['action_ref']);
		unset($this->titles['action_deadline_date']);
		unset($this->titles['action_reminder']);
		$this->titles['update_log'] = "Response";
		$this->titles['date_completed'] = "Completed Date";
		$this->titles['result'] = "Compliance Status";
		$this->titles['filter_legislation_type'] = "Legislation Source";
		$this->allowchoose = array(
			'filter_legislation_type'=>false,
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'id'=>false,
			'progress'=>false,
			'date_completed'=>false,
			'update_log'=>false,
		);
		$this->types = array(
			'financial_year'		=> "LIST",
			'legislation_id'		=> "LIST",
			'deliverable_id'		=> "LIST",
			'id' 					=> "REF",
			'progress'				=> "PERC",
			'result'				=> "RESULT",
			'date_completed'		=> "DATE",
			'deadline' 				=> "DATE",
			'action_deliverable' 	=> "TEXT",
			'reminder' 				=> "DATE",
			'status' 				=> "LIST",
			'action' 				=> "TEXT",
			'owner' 				=> "LIST",
			'filter_legislation_type'=>"SMLLIST",
			'update_log'				=>	"LOG",
		);
		
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array(
			'id'					=> false,
			'progress'				=> false,
			'filter_legislation_type'=>false,
			'update_log'				=> false,
		);
		$this->allowsortby = array(
			'financial_year'		=> false,
			'result'				=> false,
			'date_completed'		=> false,
			'status' 				=> false,
			'owner' 				=> false,
			'filter_legislation_type'=>false,
			'update_log'				=> false,
		);
		$this->default_sort = 100;
		$this->sortposition = array(
			'id' 					=> 1,
			'financial_year'		=> 2,
			'legislation_id'		=> 3,
			'deliverable_id'		=> 4,
			'progress'				=> 5,
			'result'				=> 6,
			'date_completed'		=> 7,
			'deadline' 				=> 8,
			'action_deliverable' 	=> 9,
			'reminder' 				=> 10,
			'status' 				=> 11,
			'action' 				=> 12,
			'owner' 				=> 13,
		);
	
	}
	
	protected function getFieldData() {
		$this->data = array(
			'financial_year'		=> $this->getFinancialYears(),
			'legislation_id'		=> $this->getLegislations(),
			'deliverable_id'		=> $this->getDeliverables(),
			'result'				=> $this->getResultOptions(),
			'status'				=> $this->getStatusOptions(),
			'owner'					=> $this->getActionOwners(),
			'filter_legislation_type'=> array('ALL'=>"All Legislations",'CLIENT'=>"Client Created Legislations",'MASTER'=>"Imported Master Legislations"),
		);
		$this->default_data = array('filter_legislation_type'=>"ALL");
		//$this->arrPrint($this->data);
	}


	
	private function getStatusOptions() {
		$data = array();
		$me = new Actionstatus();
		$d = $me->getAll();
		foreach($d as $e) {
			$data[$e['id']] = $e['name'];
		}
		return $data;
	}
	public function getActionOwners() {
		$data = array();
		$db = new ASSIST_DB("client");
		$sql = "SELECT DISTINCT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as name
				FROM assist_".$_SESSION['cc']."_timekeep tk
				INNER JOIN ".$db->getDBRef()."_action A
				ON tk.tkid = A.owner
				WHERE tk.tkstatus = 1
				AND ".Action::getStatusSQLForWhere("A")."
				ORDER BY tk.tkname, tk.tksurname";
		$data = $db->mysql_fetch_fld2_one($sql,"id","name");
		return $data;
	}
	


	
	
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(self::REPORT_FILENAME);
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(self::REPORT_FILENAME);
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r['id'];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,"id");
			//echo "<P>".implode(",",array_keys($rows))."</p>";
			if( (isset($_REQUEST['columns']['progress']) && strtoupper($_REQUEST['columns']['progress'])=="ON") || (isset($_REQUEST['columns']['result']) && strtoupper($_REQUEST['columns']['result'])=="ON") ) {
				$action_progress = $this->getActionProgress($db,array_keys($rows));
			}
			if(isset($_REQUEST['columns']['update_log'])) {
				$response = $this->getResponses("action","action_id",$db,array_keys($rows));
			}
			foreach($rows as $key => $r) {
				$status = $r['status'];
				$my_result = isset($action_progress[$key]) ?  ($action_progress[$key]) : array('progress'=>0,'date_completed'=>"N/A");
				$r['date_completed'] = ($my_result['progress']<100 ? "" : $my_result['date_completed']);
				$r['result'] = $this->getCompliance($my_result,$r['deadline'],$r['status']);
				$r['update_log'] = isset($response[$key]) ? $response[$key] : array();
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r['id']] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								$d = $d;
								break;
							case "LIST":
							case "TEXTLIST":
							case "MULTILIST":
									$d2 = $d;
									if(strlen($d)==0) {
										$d = "Unspecified";
									} else {
										$x = explode(",",$d);
										$d = "";
										$z = array();
										foreach($x as $a) {
											if(isset($this->data[$i][$a])) {
												$z[] = $this->data[$i][$a];
											} elseif(isset($this->backupdata[$i][$a])) {
												$z[] = $this->backupdata[$i][$a];
											} 
										}
										$d = count($z)>0 ? implode(", ",$z) : "Unspecified";//$d2;//"Unknown";
									}
									//$d.=" [".$d2."]";
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							if($i=="status" && $r['progress']=="100" && $status==3) {
								$s = $r['actionstatus'];
								$aa = Action::AWAITING_APPROVAL;
								$ap = Action::APPROVED;
								if( ($s & $aa) == $aa) {
									$d.=" (Awaiting Approval)";
								} elseif( ($s & $ap) == $ap) {
									$d.=" (Approved)";
								}
							}
							if($type!="LOG") {
								$final_rows[$r['id']][$i] = stripslashes($d);
							} else {
								$final_rows[$r['id']][$i] = $d;
							}
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap,$d,$s) {
		$result = "notCompletedAndOverdue";
		$deadline = strtotime($d);
		$p = $ap['progress'];
		$dc = strtotime($ap['date_completed']);
		if($s==0) {
			$result = "unableToComply";
		} elseif($p<100 || $s<>3) {	//incomplete
			if(strtotime(date("d F Y")) > $deadline) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		} else {		//complete
			if($dc < $deadline) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$deadline) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		}
		
		return $result;
		
	}
	
	public function getActionProgress($db,$objects,$key_field="id") {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT U.*
			FROM ".$db->getDBRef()."_action_update U 
			INNER JOIN ".$db->getDBRef()."_action A 
				ON U.action_id = A.id 
			WHERE ".Action::getStatusSQLForWhere("A")." 
			AND A.id IN (".implode(",",$objects).") 
			AND (
			  str_to_date(date_completed, '%d-%b-%Y') is null
			OR
			  str_to_date(date_completed, '%d-%b-%Y') = 0
			)
			AND A.progress > 0 
			ORDER BY 
			 U.action_id ASC
			, U.insertdate DESC";
			$rows = $db->mysql_fetch_all($sql);
			$updates = array();
			foreach($rows as $r) {
				$a = $r['action_id'];
				$c = unserialize(base64_decode($r['changes']));
				if(isset($c['progress'])) {
					if(isset($c['date_completed']['to']) && strlen($c['date_completed']['to'])>0 && strtotime($c['date_completed']['to'])>86401) {
						$dc = date("d F Y H:i:s",strtotime($c['date_completed']['to']));
					} else {
						$dc = date("d F Y H:i:s",strtotime($r['insertdate']));
					}
					if(!isset($updates[$a])) {
						$updates[$a] = $dc;
					} elseif(strtotime($updates[$a])<strtotime($dc)) {
						$updates[$a] = $dc;
					}
				}
			}

			$sql = "SELECT
			A.id 
			, A.deadline
			, (A.progress) as average
			, (STR_TO_DATE(A.date_completed,'%d-%b-%Y')) as last_date
			".(strlen($key_field)>0 && $key_field !="id" ? ",".$key_field : "")."
			FROM `".$db->getDBRef()."_action` A
			WHERE A.id IN (".implode(",",$objects).")";
			$res = $db->mysql_fetch_all_fld($sql,"id");		
			
			foreach($res as $d => $rs) {
				if($key_field!="id") {
					$k = $rs[$key_field];
				} else {
					$k = $d;
				}
				$r = "N/A";
				if(isset($updates[$d])) { 
					$r = $updates[$d];
				}
				if($r!="N/A" && strtotime($r)>strtotime($rs['last_date'])) {
					$dc = strtotime($r);
				} else {
					$dc = strtotime($rs['last_date']);
				}
				$result[$k] = array(
					'progress'=>$rs['average'],
					'date_completed'=>date("d M Y",$dc)
				);
			}
			
			
		}
		return $result;
	}

	protected function setSQL($db,$filter=array()) {
		$sql = "";
		$sql = "
		SELECT A.*
		, D.id as deliverable_id
		, L.financial_year
		, L.id as legislation_id
		FROM ".$db->getDBRef()."_action A
		INNER JOIN ".$db->getDBRef()."_deliverable D 
		ON A.deliverable_id = D.id
		INNER JOIN ".$db->getDBRef()."_legislation L
		ON L.id = D.legislation
		WHERE ".Action::getStatusSQLForWhere("A")." AND ".Deliverable::getStatusSQLForWhere("D")." AND ".Legislation::getStatusSQLForWhere("L"); //echo $sql;
		//$this->arrPrint($_REQUEST);
		if(!isset($this->titles['id'])) { $this->getFieldDetails(); }
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter'];
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY A.id ";
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						/*case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;*/
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$a = "";
					switch($i) {
/*						case 'sub_event': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$a = $this->report->getFilterSql($t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;*/
						case "financial_year":
							$a = $this->report->getFilterSql("L",$t,$f,$ft,$i);
							break;
						case "deliverable_id":
							$a = $this->report->getFilterSql("D",$t,$f,$ft,"id");
							break;
						case "legislation_id":
							$a = $this->report->getFilterSql("L",$t,$f,$ft,"id");
							break;
						case 'progress':	break;	//do nothing; filter applied in row processing
						case 'result':		break;	//do nothing; filter applied in row processing
						case 'filter_legislation_type':		break;	//do nothing; filter applied later
						default:				
							$a = $this->report->getFilterSql("A",$t,$f,$ft,$i);
					}
					if(strlen($a)>0) { $where[] = $a; }
				}
			}
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		$sql.=$this->sourceLegislation("L",$filters);
		if(count($having)>0) {
			$sql.= $sql_group;
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		$sql.=$this->getSortBySql();
		
//		echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array();
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				switch($s) {
					case "deadline":
					case "reminder":
						$sort_by[] = "STR_TO_DATE(A.".$s.",'%d-%b-%Y')";
						break;
					case "legislation_id":
						$sort_by[] = "L.name";
						break;
					case "deliverable_id":
						$sort_by[] = "D.short_description";
						break;
					default:
						$sort_by[] = $s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	

}













/*
SELECT A.* , D.id as deliverable_id , L.financial_year , L.id as legislation_id FROM assist_kny0009_compl_action A INNER JOIN assist_kny0009_compl_deliverable D ON A.deliverable_id = D.id 
INNER JOIN assist_kny0009_compl_legislation L ON L.id = D.legislation 
WHERE (A.actionstatus & 2 <> 2 AND ( A.actionstatus & 512 = 512 OR A.actionstatus & 4096 = 4096 OR A.actionstatus & 128 = 128 ) ) 
AND ( ( D.status & 4096 = 4096 OR D.status & 8192 = 8192 OR D.status & 16384 = 16384 OR D.status & 2048 = 2048 ) ) 
AND ( L.legislation_status & 1024 = 1024 OR L.legislation_status & 2048 = 2048 ) AND ( STR_TO_DATE(A.reminder,'%d-%b-%Y') >= CAST('10 February 2013 00:00:01' AS DATETIME) AND STR_TO_DATE(A.reminder,'%d-%b-%Y') <= CAST('11 February 2013 00:00:29' AS DATETIME) ) AND ( A.status = 1 OR A.status = 2 ) ORDER BY id,L.name,D.short_description,progress,STR_TO_DATE(A.deadline,'%d-%b-%Y'),action_deliverable,reminder,action
*/


?>