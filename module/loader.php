<?php
/**
 * This file has been retired.  If you encounter a module that calls this file, please redirect it to ../module/autoloader.php
 */
@ini_set(default_charset, "");
class Loader {

	public static function autoload( $class )
	{
//if($_SESSION['cc']=="test987") { echo "<P>".$class; }
		$c = explode("_",$class);
		switch(strtoupper($c[0])) {
			//$path = "../library/class/";
			//self::getAssistClass($class,$path);
			//break;
		case "MODULE":
			$path = "".strtoupper($c[1])."/";
			self::getAssistClass($class,$path);
			break;
		case "ASSIST":
		default:
			$path = $_SESSION['modlocation'];
			self::getModuleClass( $path, $class );
		}
	}
	
	public static function getModuleClass( $modloc, $filename ) {
		$found = false;
		
		$paths = array(
			"",
			"class/",
			$modloc."/",
			$modloc."/class/",
			"library/class/",
			"library/dbconnect/",
			"library/database/",
		);
		
		foreach($paths as $p) {
			if(!$found) {
				$f = $p.strtolower($filename).".php";
				for($i=0;$i<=3;$i++) {
					if($i>0) { $f = "../".$f; }
					if(file_exists($f)) {
						require_once($f);
						$found = true;
						$i=4;
					}
				}
			}
		}
		
	}
	
	public static function getAssistClass($class,$path) {	
//		echo "<P>::".$path.$class.".php::</p>";
		if(file_exists($path.strtolower($class).".php")) { 
			require_once($path.strtolower($class).".php");
		}
	}
	
}


?>