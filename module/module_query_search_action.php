<?php

class MODULE_QUERY_SEARCH_ACTION extends MODULE_QUERY_SEARCH {

	//CONSTRUCT
	private $date_format = "STRING";
	private $my_class_name = "MODULE_QUERY_SEARCH_ACTION";
	//private $my_quick_class_name = "MODULE_QUERY_QUICK_REPORT";
	const REFTAG = Riskaction::REFTAG;
	const DEFAULT_REPORT_FILENAME = "action";

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),
	

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = ": Search Results";
	private $multilist_results;
	
	private $backupdata = array();
	
	


	public function __construct($p) {	
		//echo "<P>MODULE_COMPL_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name);//,$this->my_quick_class_name);
		$this->folder = "search";
		$this->default_report_title = $this->getModTitle().$this->default_report_title;
		//$this->arrPrint($_REQUEST);
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}
	
	protected function getFieldDetails() {

/**** HEADINGS AS AT 9 FEBRUARY 2013
    [action_status] => Action Status	 => status
    [query_action] => Query Action		 => action
    [query_comments] => Query Comments	 => ? [unset]
    [deliverable] => Deliverable		 => deliverable
    [action_comments] => Action Comments => ? [unset]
    [action_owner] => Action Owner		 => action_owner
    [timescale] => Timescale			 => timescale
    [deadline] => Deadline				 => deadline
    [progress] => Progress				 => progress
    [assurance] => Assurance			 => [unset]
    [signoff] => Sign Off				 => [unset]
    [action_ref] => Reference			 => id [Ref]
	
AFTER ADJUSTMENT	
    [id] => Ref
    [financial_year] => Financial Year
    [query_id] => Query 
    [status] => Action Status
    [action] => Query Action
    [deliverable] => Deliverable
    [action_owner] => Action Owner
    [timescale] => Timescale
    [deadline] => Deadline
    [progress] => Progress
	
	
****************/

		$headers = new Naming();
	
		$this->titles['id'] = "Ref";
		$this->titles['financial_year'] = "Financial Year";
		$this->titles['query_id'] = "Query";


		$x = $headers->getReportingNaming("action");
		$unset_headers = array("action_ref","assurance","signoff","query_comments","action_comments");
		foreach($unset_headers as $h) { unset($x[$h]); }
		$search = array('query_action','action_status');
		$replace = array('action','status');
		$y = json_encode($x);
		$z = json_decode(str_replace($search,$replace,$y),true); 

		$this->titles+=$z;
		$this->titles['date_completed'] = "Completed Date";
		//$this->titles['result'] = "Result";
		$this->allowchoose = array(
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'id'=>false,
			'progress'=>false,
			'date_completed'=>false,
		);
		$this->types = array(
			'financial_year'		=> "LIST",
			'query_id'				=> "LIST",
			'id' 					=> "REF",
			'progress'				=> "PERC",
			'result'				=> "RESULT",
			'date_completed'		=> "DATE",
			'deadline' 				=> "DATE",
			'deliverable' 			=> "TEXT",
			'status' 				=> "LIST",
			'action' 				=> "TEXT",
			'action_owner' 			=> "LIST",
			'timescale'				=> "SMLTEXT",
		);
		
		$this->default_data = array();
		$this->data = array();
		$this->allowgroupby = array(
			'id'					=> false,
			'progress'				=> false,
			'action'				=> false,
			'deliverable'			=> false,
		);
		$this->allowsortby = array(
			'financial_year'		=> false,
			'result'				=> false,
			'date_completed'		=> false,
			'status' 				=> false,
			'action_owner' 			=> false,
		);
		$this->default_sort = 100;
		$this->sortposition = array(
			'id' 					=> 1,
			'financial_year'		=> 2,
			'query_id'				=> 3,
			'progress'				=> 5,
			'result'				=> 6,
			'date_completed'		=> 7,
			'deadline' 				=> 8,
			'deliverable' 			=> 9,
			'status' 				=> 11,
			'action' 				=> 12,
			'action_owner' 			=> 13,
			'timescale' 			=> 14,
		);
	
	}
	
	protected function getFieldData() {
		$this->data = array(
			'financial_year'		=> $this->getFinancialYears(),
			'query_id'				=> $this->getQueries(),
			'result'				=> $this->getResultOptions(),
			'status'				=> $this->getClassData("ActionStatus"), //$this->getStatusOptions(),
			'action_owner'			=> $this->getActionOwners(),
		);
		$this->default_data = array();
		//$this->arrPrint($this->data);
	}


	private function getClassData($fld) {
		$data = array();
		switch($fld) {
			case "ActionStatus":		$me = new ActionStatus( "", "", "" );	break;
		}
		$d = $me->getReportList();
		$data = $this->listSort($d);
		return $data;
	}
	
	public function getActionOwners() {
		$data = array();
		$db = new ASSIST_DB("client");
		$sql = "SELECT DISTINCT tk.tkid as id, CONCAT_WS(' ',tk.tkname,tk.tksurname) as name
				FROM assist_".$_SESSION['cc']."_timekeep tk
				INNER JOIN ".$db->getDBRef()."_actions A
				ON tk.tkid = A.action_owner
				INNER JOIN ".$db->getDBRef()."_query_register Q
				ON A.risk_id = Q.id
				WHERE ".Riskaction::getStatusSQLForWhere("A")."
				AND ".Risk::getStatusSQLForWhere("Q")."
				ORDER BY tk.tkname, tk.tksurname";
		$data = $db->mysql_fetch_fld2_one($sql,"id","name");
		return $data;
	}
	


	
	
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->getPageHeading("Results"));
		$this->report->setReportFileName(self::DEFAULT_REPORT_FILENAME);
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(self::DEFAULT_REPORT_FILENAME);
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby;
		if($groupby!="X" && isset($this->data[$groupby])) {
			foreach($this->data[$groupby] as $key => $t) {
				$this->groups[$key] = $this->blankGroup($key,$t);
			}
		} else {
			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {
		$i = $r['id'];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					$this->group_rows[$k][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$db = new ASSIST_DB();
		$sql = $this->setSQL($db);
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,"id");
			//echo "<P>".implode(",",array_keys($rows))."</p>";
			if( (isset($_REQUEST['columns']['date_completed']) && strtoupper($_REQUEST['columns']['date_completed'])=="ON") || (isset($_REQUEST['columns']['result']) && strtoupper($_REQUEST['columns']['result'])=="ON") ) {
				$date_completed = $this->getDateCompleted($db,array_keys($rows));
			}
			foreach($rows as $key => $r) {
				$r['date_completed'] = ($r['progress']<100 || !isset($date_completed[$key])) ? "" : $date_completed[$key];
				$r['result'] = $this->getCompliance(array('progress'=>$r['progress'],'date_completed'=>$r['date_completed']),$r['deadline'],$r['status']);
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r['id']] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								$d = $d;
								break;
							case "LIST":
							case "TEXTLIST":
							case "MULTILIST":
									$d2 = $d;
									if(strlen($d)==0) {
										$d = "Unspecified";
									} else {
										$x = explode(",",$d);
										$d = "";
										$z = array();
										foreach($x as $a) {
											if(isset($this->data[$i][$a])) {
												$z[] = $this->data[$i][$a];
											} elseif(isset($this->backupdata[$i][$a])) {
												$z[] = $this->backupdata[$i][$a];
											} 
										}
										$d = count($z)>0 ? implode(", ",$z) : "Unspecified";//$d2;//"Unknown";
									}
									//$d.=" [".$d2."]";
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LINK":
								$d = "<a href=".$d.">".$d."</a>";
								break;
							case "TEXT":
							default:
								$d = $d;
								break;
							}
							$final_rows[$r['id']][$i] = stripslashes($d);
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap,$d,$s) {
		$result = "notCompletedAndOverdue";
		$deadline = strtotime($d);
		$p = $ap['progress'];
		$dc = strtotime($ap['date_completed']);
		if($p<100 || $s<>3) {	//incomplete
			if(strtotime(date("d F Y")) > $deadline) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		} else {		//complete
			if($dc!=0 && $dc < $deadline) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$deadline) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		}
		
		return $result;
		
	}
	
	public function getDateCompleted($db,$objects,$key_field="id") {
		$result = array();
		if(count($objects)>0) {
		
			//Check for completed actions with action_on dates
			$sql = "SELECT A.id, A.action_on
					FROM ".$db->getDBRef()."_actions A
					WHERE A.id IN (".implode(",",$objects).")
					AND ".Riskaction::getStatusSQLForWhere("A")."
					AND A.progress = 100
					AND A.action_on <> 0
					ORDER BY A.id";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $r) {
				$result[$r['id']] = date("d-M-Y",strtotime($r['action_on']));
			}
			
			$result_keys = array_keys($result);
			sort($result_keys,SORT_NUMERIC);
			sort($objects,SORT_NUMERIC);
			//check for completed actions without action_on dates
			if(count($result) != count($objects) || $result_keys!=$objects) {
				$sql = "SELECT AU.* 
						FROM ".$db->getDBRef()."_actions_update AU 
						INNER JOIN ".$db->getDBRef()."_actions A
						ON A.id = AU.action_id AND A.progress = 100
						WHERE AU.action_id IN (".implode(",",$objects).")
						".(count($result_keys)>0 ? "AND AU.action_id NOT IN (".implode(",",$result_keys).")" : "")."
						ORDER BY insertdate DESC";
				$data = $db->mysql_fetch_all($sql);
				foreach($data as $r) {
					if(!isset($result[$r['action_id']])) {
						$result[$r['action_id']] = date("d-M-Y",strtotime($r['insertdate']));
					}
				}
			}
		}
		return $result;
	}
	
	
	protected function setSQL($db,$filter=array()) {
		$sql = "";
		$sql = "
		SELECT A.*
		, Q.id as query_id
		, Q.financial_year
		FROM ".$db->getDBRef()."_actions A
		INNER JOIN ".$db->getDBRef()."_query_register Q 
		ON A.risk_id = Q.id
		WHERE ".Riskaction::getStatusSQLForWhere("A")." AND ".Risk::getStatusSQLForWhere("Q"); //echo $sql;
		//$this->arrPrint($_REQUEST);
		if(!isset($this->titles['id'])) { $this->getFieldDetails(); }
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter'];
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY A.id ";
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						/*case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;*/
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$a = "";
					switch($i) {
/*						case 'sub_event': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$a = $this->report->getFilterSql($t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;*/
						case "financial_year":
							$a = $this->report->getFilterSql("Q",$t,$f,$ft,$i);
							break;
						case "query_id":
							$a = $this->report->getFilterSql("Q",$t,$f,$ft,"id");
							break;
						case 'progress':	break;	//do nothing; filter applied in row processing
						case 'result':			break;	//do nothing; filter applied in row processing
						default:				
							$a = $this->report->getFilterSql("A",$t,$f,$ft,$i);
					}
					if(strlen($a)>0) { $where[] = $a; }
				}
			}
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		if(count($having)>0) {
			$sql.= $sql_group;
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		
		$sql.=$this->getSortBySql();
		
		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array();
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				switch($s) {
					case "deadline":
					case "reminder":
						$sort_by[] = "STR_TO_DATE(A.".$s.",'%d-%b-%Y')";
						break;
					case "query_id":
						$sort_by[] = "Q.description";
						break;
					default:
						$sort_by[] = $s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	

}













/*
SELECT A.* , D.id as deliverable_id , L.financial_year , L.id as legislation_id FROM assist_kny0009_compl_action A INNER JOIN assist_kny0009_compl_deliverable D ON A.deliverable_id = D.id 
INNER JOIN assist_kny0009_compl_legislation L ON L.id = D.legislation 
WHERE (A.actionstatus & 2 <> 2 AND ( A.actionstatus & 512 = 512 OR A.actionstatus & 4096 = 4096 OR A.actionstatus & 128 = 128 ) ) 
AND ( ( D.status & 4096 = 4096 OR D.status & 8192 = 8192 OR D.status & 16384 = 16384 OR D.status & 2048 = 2048 ) ) 
AND ( L.legislation_status & 1024 = 1024 OR L.legislation_status & 2048 = 2048 ) AND ( STR_TO_DATE(A.reminder,'%d-%b-%Y') >= CAST('10 February 2013 00:00:01' AS DATETIME) AND STR_TO_DATE(A.reminder,'%d-%b-%Y') <= CAST('11 February 2013 00:00:29' AS DATETIME) ) AND ( A.status = 1 OR A.status = 2 ) ORDER BY id,L.name,D.short_description,progress,STR_TO_DATE(A.deadline,'%d-%b-%Y'),action_deliverable,reminder,action
*/


?>