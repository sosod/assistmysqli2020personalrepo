<?php
/**
 * AA-488 JC 12 Oct 2020
 * This file is the current Loader file.  If you encounter a module that doesn't call this file, please redirect it here.
 */
@ini_set(default_charset, "");
class Loader {

	public static function autoload( $class )
	{
 //echo "<P>".$class; 
		$c = explode("_",$class);
		switch(strtoupper($c[0])) {
			//$path = "../library/class/";
			//self::getAssistClass($class,$path);
			//break;
		case "MODULE":
			$path = "".strtoupper($c[1])."/";
			self::getAssistClass($class,$path);
			break;
		case "ASSIST":
		default:
			$path = isset($_SESSION['modlocation']) ? $_SESSION['modlocation'] : "";
			self::getModuleClass( $path, $class );
		}
	}
	
	public static function getModuleClass( $modloc, $filename ) {
		$found = false;
		
		$paths = array(
			"",
			"class/",
			"library/class/",
			"library/dbconnect/",
			"library/database/",
		);
		if(strlen($modloc)>0) {
			$paths[] = $modloc."/";
			$paths[] = $modloc."/class/";
			$paths[] = strtolower($modloc."/");
			$paths[] = strtolower($modloc."/class/");
		}
		$e = explode("_",$filename);
		if(isset($e[0])){
			$paths[] = strtoupper($e[0])."/class/";
			$paths[] = strtolower($e[0]."/class/");
			$paths[] = strtoupper($e[0])."/";
			$paths[] = strtolower($e[0]."/");
		}		
		foreach($paths as $p) {
			if(!$found) {
				$f = $p.strtolower($filename).".php";
//if($_SERVER['REMOTE_ADDR']=="105.233.76.67") { echo "<P>".$f."</p>"; }
				for($i=0;$i<=3;$i++) {
					if($i>0) { $f = "../".$f; }
					if(file_exists($f)) {
						require_once($f);
						$found = true;
						$i=4;
					}
				}
			}
		}
		
	}
	
	public static function getAssistClass($class,$path) {	
//		echo "<P>::".$path.$class.".php::</p>";
		if(file_exists($path.strtolower($class).".php")) { 
			require_once($path.strtolower($class).".php");
		}
	}
	
}

@session_start();
spl_autoload_register("Loader::autoload");


/*


// Our custom error handler
function nettuts_error_handler($number, $message, $file, $line, $vars)

{

	if(strpos($message,"ignoring session_start")===false 
		&& strpos($message,"mysql_close()")===false 
		&& strpos($message,"default_charset")===false
		&& strpos($message,"Declaration of ASSIST_TK::getUserEmail()")===false
	) {

    $email = "
        <p>An error ($number) occurred on line
        <strong>$line</strong> and in the <strong>file: $file.</strong>
        <p> $message </p>";

    $email .= "<pre>" . print_r($vars, 1) . "</pre>";

    	if($number!=8192) {
//	    $emailObject = new ASSIST_EMAIL("actionit.actionassist@gmail.com","ASSIST01 PHP Error",$email);
//		$emailObject->sendEmail();
$header="Content-type: text/html; charset=us-ascii";
mail("actionit.actionassist@gmail.com","ASSIST01 PHP Error",$email,$header);

    }
}
}

// We should use our custom function to handle errors.
set_error_handler('nettuts_error_handler');

register_shutdown_function( "fatal_handler" );

function fatal_handler() {
    $errfile = "unknown file";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;

    $error = error_get_last();

    if( $error !== NULL && $error['type']!=8192) {
        $errno   = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr  = $error["message"];
if(strpos($errstr,"ignoring session_start")===false 
	&& strpos($errstr,"mysql_close()")===false 
	&& strpos($errstr,"default_charset")===false
	&& strpos($errstr,"Declaration of ASSIST_TK::getUserEmail()")===false
) {
            $email = "
        <p>A <strong>fatal</strong> error ($errno) occurred on line
        <strong>$errline</strong> and in the <strong>file: $errfile.</strong>
        <p> $errstr </p>";

    $email .= "<pre>" . print_r($_REQUEST, 1) . "</pre>";
    $email .= "<pre>" . print_r($_SESSION, 1) . "</pre>";
$header="Content-type: text/html; charset=us-ascii";
	mail("actionit.actionassist@gmail.com","ASSIST01 PHP Fatal Error",$email,$header);
    }
}
}

*/

?>