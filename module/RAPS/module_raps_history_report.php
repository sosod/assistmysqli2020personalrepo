<?php

class MODULE_RAPS_HISTORY_REPORT extends MODULE_GENERIC_HISTORY_REPORT {

	protected $object_id = 0;
	protected $page;
	protected $folder;
	protected $my_class_name = "MODULE_RAPS_HISTORY_REPORT";
	private $db;
	protected $modReportObj;

	public function __construct($p) {
		parent::__construct();
		$this->modReportObj = new MODULE_RAPS_REPORT($p,"","","");
		$this->page = $p;
		$this->folder = "report";
		$this->db = new ASSIST_DB("client");
	}

/*	public function deleteReport($id) {
		$sql = "UPDATE ".$this->db->getDBRef()."_quick_report SET status = 0 WHERE id = $id";
		$mar = $this->db->db_update($sql);
		return array("ok","Quick Report ".$id." has been deleted successfully.");
	}
*/
/*	
	public function updateReport($id,$name,$descrip,$report) {
		$sql = "UPDATE ".$this->db->getDBRef()."_quick_report SET
					name = '$name',
					description = '$descrip',
					report = '$report'
				WHERE 
					id = $id";
		$mar = $this->db->db_update($sql);
		return array("ok","Quick Report ".$id." has been updated successfully.");
	}
*/

	public function createReport($sys,$down,$type) {
		$sys = $this->getReportPath()."/".$sys;
		$sql = "INSERT INTO ".$this->db->getDBRef()."_report_history 
				(system_name,download_name,rep_type,status,insertuser,insertdate) 
				VALUES ('$sys','$down','$type',2,'".$this->getUserID()."',now())";
		$id = $this->db->db_insert($sql);
		$this->object_id = $id;
		//return array("ok","New Quick Report ".$id." has been created successfully.");
		return $id;
	}

	public function markReportGenerationComplete($usr="",$id="") {
		if(strlen($id)==0) { $id = $this->object_id; }
		if($id>0) {
			$sql = "UPDATE ".$this->db->getDBRef()."_report_history SET status = 1, download_name = '$usr' WHERE id = $id ";
			$this->db->db_update($sql);
		}
	}

	public function getList() {
		$results = array();
		$sql = "SELECT *, insertdate as moddate FROM ".$this->db->getDBRef()."_report_history WHERE status >=1 AND insertuser = '".$this->getUserID()."' AND insertdate > DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY insertdate DESC";
		$rows = $this->db->mysql_fetch_all_fld($sql,"id");
		
		return $rows;
	}
	
	public function howManyReportsInProgressForThisUser() {
		return $this->howManyReportsInProgress(true);
	}
	
	public function howManyReportsInProgress($this_user=true) {
		$sql = "SELECT count(id) as c FROM ".$this->db->getDBRef()."_report_history WHERE status >1 ".($this_user===true ? "AND insertuser = '".$this->getUserID()."'" : "")." AND insertdate > DATE_SUB(NOW(), INTERVAL 6 HOUR) ORDER BY insertdate DESC";
		$row = $this->db->mysql_fetch_one($sql);
		return $row['c'];
	}
	
	public function preparePage() {
		/*$header = array(0=>"",1=>"");
		$m0 = new Menu();
		$m0_menu = $m0->getMenu("AND parent_id = 0 AND name='report'");
		$key = array_keys($m0_menu); $key = $key[0];
		$m0_menu = $m0_menu[$key];
		$header[0] = "<a href='/".$_SESSION['modlocation']."/report/' class=breadcrumb>".($m0_menu['client_terminology'] == "" ? $m0_menu['ignite_terminology'] : $m0_menu['client_terminology'])."</a>";
		$m1 = new UpperMenu($this->folder, $this->page);
		$m1_menu = $m1->createMenu();
		$menu = array();
		foreach($m1_menu as $key => $m) {
			$m['url'] = "/".$_SESSION['modlocation']."/".$this->folder."/".$m['url'];
			if($m['active']=="Y") { $header[1]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
			$menu[$key] = $m;
		}
		$this->drawNavButtons($menu);
		echo "<h1>".implode(" >> ",$header)."</h1>";*/
//		echo "quick report";
		/*$nav = array(
			"report"=>array('id'=>"report",'url'=>"/".$_SESSION['modlocation']."/report.php",'active'=>($this->page=="report"),'display'=>"Report Generator"),
			"quick"=>array('id'=>"quick",'url'=>"/".$_SESSION['modlocation']."/report_quick.php",'active'=>($this->page=="quick"),'display'=>"Quick Report"),
			"graph"=>array('id'=>"graph",'url'=>"/".$_SESSION['modlocation']."/report_graph.php",'active'=>false,'display'=>"Graph Generator"),
		);*/
		//echo $this->page;
		$nav = $this->modReportObj->getReportNavButtons();
		$this->drawNavButtons($nav);
		echo "<h1>Complaints Assist >> Report >> Report History</h1>";
	}
	
}



?>