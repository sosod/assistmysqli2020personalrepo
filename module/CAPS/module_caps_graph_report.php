<?php

class MODULE_CAPS_GRAPH_REPORT extends MODULE_CAPS_REPORT {

	private $me;
	
	//CONSTRUCT
	private $date_format = "INT";
	private $my_class_name = "MODULE_CAPS_GRAPH_REPORT";
	private $my_quick_class_name = "";//"MODULE_CAPS_QUICK_REPORT";
	const REFTAG = CAPS::REFTAG;
	const REPORTFILENAME = "complaints";
	const OBJECT_DEADLINE = "calldate";
	const OBJECT_ID = "callid";

	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Complaints Assist Report";
	private $multilist_results;
	private $multitext_separator = ";";
	
	private $backupdata = array();
	
	


	public function __construct($p) {	
		//echo "<P>MODULE_COMPL_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "";
	}
	
	
	
	
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	protected function prepareGenerator() {
		parent::prepareGenerator();
	}
	
	protected function getFieldDetails() {
		$this->me = new CAPS();
		$t = $this->me->getAllHeadings(); 
		foreach($t as $i=>$j) {
			if($i=="portfolioid") { $this->titles['deptid'] = $this->getTerm("DEPT"); }
			$this->titles['res'.$i] = $j;
		}
		/*array(
			'resid'=>"ID",
			'resref'=>"Resolution Reference",
			'restkid'=>"Assigned To",
			'resportfolioid'=>"Portfolio",
			'resurgencyid'=>"Priority",
			'resmeetingdate'=>"Council Meeting Date",
			'resdeadline'=>"Deadline",
			'resaction'=>"Instructions",
			'resnotes'=>"Additional Notes",
			'resstatusid'=>"Status",
			'resstate'=>"Progress",
			'resadddate'=>"Date Assigned",*/
		$this->titles['date_completed']="Date Completed";
		//$this->titles['update_log']="Update History";
		$this->titles['result']="Result";
		$this->allowchoose = array(
		);
		$this->default_selected = array();
		$this->allowfilter = array(
			'resid' => false,
			'date_completed'=>false,
			'resstate' => false,
			'update_log'=>false,
			'resattachment'=>false,
		);
		$this->types = array(
			'resid'=>"REF",
			'resref'=>"SMLTEXT",
			'restkid'=>"LIST",
			'resactionowner'=>"LIST",
			'deptid'=>"LIST",
			'resportfolioid'=>"LIST",
			'resurgencyid'=>"LIST",
			'resmeetingdate'=>"DATE",
			'resdeadline'=>"DATE",
			'resaction'=>"TEXT",
			'resnotes'=>"TEXT",
			'resstatusid'=>"LIST",
			'resstate'=>"PERC",
			'resadddate'=>"DATE",
			'resadduser'=>"LIST",
			'resattachment'=>"ATTACH",
			'date_completed'=>"DATE",
			'update_log'=>"LOG",
			'result'=>"RESULT",
		);
		$this->default_data = array();
		$this->data = array(
		);
		$this->allowgroupby = array(
			'resid'=>false,
			'resref'=>false,
			'resstate'=>false,
			'update_log'=>false,
			'resadddate'=>false,
			'resattachment'=>false,
			//'date_completed'=>false,
			'result'=>false,
		);
/*		$this->allowsortby = array(
			'resid'=>true,
			'resref'=>true,
			'restkid'=>true,
			'resactionowner'=>true,
			'deptid'=>true,
			'resportfolioid'=>true,
			'resurgencyid'=>true,
			'resmeetingdate'=>true,
			'resdeadline'=>true,
			'resaction'=>true,
			'resnotes'=>true,
			'resstatusid'=>true,
			'resstate'=>true,
			'resadddate'=>true,
			'resadduser'=>true,
			'date_completed'=>false,
			'result'=>false,
			'update_log'=>false,
		);
/*		$this->default_sort = 100;
		$this->sortposition = array(
			'resid'=>99,
			'resref'=>1,
			'restkid'=>2,
			'resactionowner'=>3,
			'deptid'=>4,
			'resportfolioid'=>6,
			'resurgencyid'=>7,
			'resmeetingdate'=>8,
			'resdeadline'=>9,
			'resaction'=>10,
			'resnotes'=>11,
			'resstatusid'=>12,
			'resstate'=>13,
			'resadddate'=>14,
			'resadddate'=>15,
		);
*/		
		foreach($this->types as $key => $t) {
			if(in_array($t,$this->bad_graph_types)) {
				$this->allowfilter[$key] = false;
				$this->allowgroupby[$key] = false;
			}
		}
		
	}
	
	protected function getFieldData() {
		$this->data = array(
			'restkid'		=>	$this->getOptionData("UserList"),
			'resactionowner'=>	$this->getOptionData("ActionUserList"),
			'resadduser'	=>	$this->getOptionData("AddUserList"),
			'deptid'		=>	$this->getOptionData("Dept"),
			'resportfolioid'=>	$this->getOptionData("Portfolio"),
			'resurgencyid'	=>	$this->getOptionData("Urgency"),
			'resstatusid'	=>	$this->getOptionData("Status"),
			'result'		=>	$this->getResultOptions(),
		);
		$this->default_data = array();
	}

	private function getOptionData($fld) {
		$data = array();
		$db = new ASSIST_DB("client");
		$db->setDBRef("CAPS");
		switch($fld) {
			case "Status":			$d = $this->me->getUsedStatus(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_status WHERE yn = 'Y' AND state >=0 ORDER BY sort"; break;
			case "AddUserList": 	$d = $this->me->getUsedAddUsers(); 				break; //$sql = "SELECT DISTINCT tkid as id, CONCAT(tkname,' ',tksurname) as name FROM assist_".$db->getCmpCode()."_timekeep INNER JOIN ".$db->getDBRef()."_res ON tkid = resadduser AND resstatusid <> 'CN' ORDER BY tkname, tksurname"; break;
			case "UserList": 		$d = $this->me->getUsedResponsiblePersons(); 	break; //$sql = "SELECT DISTINCT tkid as id, CONCAT(tkname,' ',tksurname) as name FROM assist_".$db->getCmpCode()."_timekeep INNER JOIN ".$db->getDBRef()."_res ON tkid = restkid AND resstatusid <> 'CN' ORDER BY tkname, tksurname"; break;
			case "ActionUserList": 	$d = $this->me->getUsedActionOwners(); 			break; //$sql = "SELECT DISTINCT tkid as id, CONCAT(tkname,' ',tksurname) as name FROM assist_".$db->getCmpCode()."_timekeep INNER JOIN ".$db->getDBRef()."_res ON tkid = resactionowner AND resstatusid <> 'CN' ORDER BY tkname, tksurname"; break;
			case "Dept": 			$d = $this->me->getUsedDepartments(); 			break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_portfolio WHERE yn = 'Y' ORDER BY value"; break;
			case "Portfolio": 		$d = $this->me->getUsedPortfolios(); 			break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_portfolio WHERE yn = 'Y' ORDER BY value"; break;
			case "Urgency": 		$d = $this->me->getUsedUrgency(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort"; break;
		}
		//$d = $db->mysql_fetch_all($sql);
		$e = array();
		foreach($d as $i => $v) {
			$e[$i] = array('id'=>$i,'name'=>$v);
		}
		$data = $this->listSort($e);
		return $data;
	}
	
	
	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
		//ASSIST_HELPER::arrPrint($_REQUEST);
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		//echo $this->groupby;
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		$this->report->setReportFileName(self::REPORTFILENAME);
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings($this->generateBlurbFromFilter());
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(self::REPORTFILENAME);
	}
	
	private function generateBlurbFromFilter() {
		//$this->arrPrint($_REQUEST);
		$b = array();
		$g = "";
		foreach($_REQUEST['filter'] as $key => $f) {
			switch($this->types[$key]) {
				case "DATE":
					if(strlen($f['from'])>0 && strlen($f['to'])>0) {
						$b[] = $this->titles[$key]." is from ".$f['from']." to ".$f['to'];
					} elseif(strlen($f['from'])>0 || strlen($f['to'])>0) {
						$b[] = $this->titles[$key]." is on ".(strlen($f['from'])>0 ? $f['from'] : $f['to']);
					}
					break;
				case "LIST":
					$fx = array();
					foreach($f as $fd) {
						if(strlen($fd)>0 && $fd!="X") {
							$fx[] = $this->data[$key][$fd];
						}
					}
					if(count($fx)>0) {
						$b[] = $this->titles[$key]." is ".implode(" or ",$fx);
					}
					break;
			}
		}
		return (count($b)>0 ? implode(" and<br />",$b)."<br />g" : "G")."rouped by ".$this->titles[$_REQUEST['group_by']];
	}
	
	private function setGroups() {
		$groupby = $this->groupby; //echo $groupby;
		if($groupby!="X") {
			if(in_array($this->types[$groupby],array("LIST")) && isset($this->data[$groupby])) {
				foreach($this->data[$groupby] as $key => $t) {
					$this->groups[$this->processTextForGroup($t)] = $this->blankGroup($this->processTextForGroup($t),$t);
				}
			} elseif(in_array($this->types[$groupby],array("RESULT")) && isset($this->data[$groupby])) {
				foreach($this->data[$groupby] as $key => $t) {
					$this->groups[$this->processTextForGroup($key)] = $this->blankGroup($this->processTextForGroup($key),$t);
				}
			}
//		} else {
//			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {  //ASSIST_HELPER::arrPrint($r);
		$i = $r[self::OBJECT_ID];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					switch($this->types[$groupby]) {
						case "DATE":	
							$x = $k;  
							if(!isset($this->groups[$x])) { $this->groups[$x] = $this->blankGroup($x,date("d F Y",$k)); }
							break;
						case "LIST":
						case "RESULT":
							$x = $this->processTextForGroup($k); break;
						case "TEXT":
							$x = $this->processTextForGroup($k); 
							if(!isset($this->groups[$x])) { $this->groups[$x] = $this->blankGroup($x,$k); }
							break;
						default: $x = $k; break;
					}
					$this->group_rows[$x][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$action_progress = array();
		$db = $this->me;//new ASSIST_DB();
		$sql = $this->setSQL($db);
		if(strlen($sql)>0) {
			$rows = $db->mysql_fetch_all_fld($sql,self::OBJECT_ID);
			$date_completed = $this->getDateCompleted($db,array_keys($rows)); //$this->arrPrint($_REQUEST['columns']);
			$logs = isset($_REQUEST['columns']['update_log']) ? $this->getUpdateLog($db,array_keys($rows)) : array();
			$attachments = isset($_REQUEST['columns']['resattachment']) ? $this->getResAttachments($db,array_keys($rows)) : array();
			foreach($rows as $key => $r) {
				$r['date_completed'] = ($r['resstate']<100 || !isset($date_completed[$key]) ? "" : $date_completed[$key]);
				$r['update_log'] = isset($logs[$key]) ? $logs[$key] : "";
				$r['resattachment'] = isset($attachments[$key]) ? $attachments[$key] : "";
				$r['result'] = $this->getCompliance(array('action_progress'=>$r['resstate'],'date_completed'=>$r['date_completed']),$r[self::OBJECT_DEADLINE]);
				if(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result']))) {
					$final_rows[$r[self::OBJECT_ID]] = $r;
					foreach($this->titles as $i => $t) {
						if(isset($r[$i]) && isset($this->types[$i])) {
							$d = $r[$i];
							$type = $this->types[$i];
							switch($type) {
							case "REF":
								$d = self::REFTAG.$d;
								break;
							case "DATE":
								if($d=="0" || (is_numeric($d) && date("d-M-Y",$d)=="01-Jan-1970")) {
									$d = "";
								} else {
									$d = is_numeric($d) && date("d-M-Y",$d)!="01-Jan-1970" ? date("d-M-Y",$d) : $d; 
								}
								break;
							case "PERC":
								$d = number_format($d,2)."%";
								break;
							case "LOG":
							case "ATTACH":
								$d = $d;
								break;
							case "LIST": case "MULTILIST":
							case "TEXT": case "SMLTEXT": case "MULTITEXT":
							default:
								$d = stripslashes($d);
								break;
							}
							$final_rows[$r[self::OBJECT_ID]][$i] = $d;
						}
					}
					$this->allocateToAGroup($r);
				}
			}
		}
		//echo $this->groupby;
		//ASSIST_HELPER::arrPrint($final_rows);
		//ASSIST_HELPER::arrPrint($this->group_rows);
		//$this->arrPrint($this->groups);
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap,$d) {
		$result = "notCompletedAndOverdue";
		$deadline = $d;
		$p = $ap['action_progress'];
		$dc = $ap['date_completed'];
		if($p<100) {	//incomplete
			if(strtotime(date("d F Y")) > $deadline) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		} else {		//complete
			if($dc < $deadline) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$deadline) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		}
		return $result;
		
	}
	
	public function getDateCompleted($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT logactdate, logdate, logresid FROM ".$db->getDBRef()."_log WHERE logstatusid = 'CL' ORDER BY logactdate DESC, logdate DESC, logresid ASC";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $l) {
				$d = $l['logresid'];
				if(!isset($result[$d])) {
					$x = $l['logactdate']>0 ? $l['logactdate'] : $l['logdate'];
					$result[$d] = $x;
				}
			}
		}
		return $result;
	}
	
	public function getUpdateLog($db,$objects) {
		$result = array();
		$rejects = array(
			"Resolution attachment deleted",
			"New resolution added",
			"Resolution id |id| edited",
		);
		$filter = strtoupper($_REQUEST['update_filter']); 
		if(count($objects)>0) {
			$sql = "SELECT logresid, logupdate, logactdate, logdate, CONCAT(tkname,' ',tksurname) as name , logaction
					FROM ".$db->getDBRef()."_log 
					INNER JOIN assist_".$db->getCmpCode()."_timekeep 
					ON tkid = logtkid 
					WHERE logresid IN (".implode(",",$objects).") 
					AND logstatusid <> 'CN'
					ORDER BY logid DESC";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $r) {
				$i = $r['logresid'];
				$ok = true;
				if(($filter=="LAST" && !isset($result[$i]) && ($r['logaction']=="U" || strlen($r['logaction'])==0)) || ($filter=="ALL" && ($r['logaction']=="U" || strlen($r['logaction'])==0)) || $filter=="AUDIT") {
					$l = strip_tags($this->decode($r['logupdate']));
					if($filter!="AUDIT") {
						foreach($rejects as $j) {
							$j = str_replace("|id|",$i,$j);
							if(substr($l,0,strlen($j))==$j) { $ok=false; }
						}
					}
					if($ok) {
						$result[$i][] = " - ".$l." (".(date("d-M-Y",$r['logactdate']*1)!=date("d-M-Y",$r['logdate']*1) && $r['logactdate']*1>0 ? "Action performed on: ".date("d-M-Y",$r['logactdate']*1)."; ":"")."Updated by ".$this->decode($r['name'])." on ".date("d-M-Y H:i",$r['logdate']).".)";//." (Updated by ".$this->decode($r['name'])" on ".date("d-M-Y H:i",$r['logdate']).")";
					}
				}
			}
		}
		return $result;
	}

	public function getResAttachments($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT resid, original_filename 
					FROM ".$db->getDBRef()."_attachments
					WHERE resid IN (".implode(",",$objects).") 
					ORDER BY id DESC";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $r) {
				$i = $r['resid'];
				$result[$i][] = " - ".$r['original_filename'];
			}
		}
		//$this->arrPrint($result);
		return $result;
	}
	
	
	protected function setSQL($db,$filter=array()) {
		$x = "R";	//object alias in SQL
		$sql_status = "(".$x.".resstatusid <> 'CN')";
		$sql = "";
		
		//Preparation
		$flds = array_keys($this->titles);
		$mod_flds = array(
			'resstatusid'=>array('fld'=>"S.value as resstatusid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_status S ON S.id = ".$x.".resstatusid"),
			'resurgencyid'=>array('fld'=>"U.value as resurgencyid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_urgency U ON U.id = ".$x.".resurgencyid"),
			'deptid'=>array('fld'=>"D.value as deptid",'table'=>""),
			'resportfolioid'=>array('fld'=>"CONCAT(D.value,' - ',P.value) as resportfolioid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_portfolio P ON P.id = ".$x.".resportfolioid INNER JOIN ".$db->getDepartmentDBRef()." D ON P.deptid = D.id"),
			'resadduser'=>array('fld'=>"CONCAT(AUT.tkname,' ',AUT.tksurname) as resadduser",'table'=>"INNER JOIN assist_".$db->getCmpCode()."_timekeep AUT ON AUT.tkid = ".$x.".resadduser"),
			'restkid'=>array('fld'=>"CONCAT(T.tkname,' ',T.tksurname) as restkid",'table'=>"INNER JOIN assist_".$db->getCmpCode()."_timekeep T ON T.tkid = ".$x.".restkid"),
			'resactionowner'=>array('fld'=>"CONCAT(AT.tkname,' ',AT.tksurname) as resactionowner",'table'=>"INNER JOIN assist_".$db->getCmpCode()."_timekeep AT ON AT.tkid = ".$x.".resactionowner"),
			'date_completed'=>array('fld'=>"",'table'=>""),
			'update_log'=>array('fld'=>"",'table'=>""),
			'result'=>array('fld'=>"",'table'=>""),
		);
		
		//Base sql
		$sql = " SELECT ";
		$sql_flds = array();
		$sql_tables = array();
		foreach($flds as $f) {
			if(isset($mod_flds[$f])) {
				if(strlen($mod_flds[$f]['fld'])>0) {
					$sql_flds[] = $mod_flds[$f]['fld'];
				}
				if(strlen($mod_flds[$f]['table'])>0) {
					$sql_tables[] = $mod_flds[$f]['table'];
				}
			} elseif(in_array($f,array("resid","resstate","resdeadline")) || !isset($this->allowgroupby[$f]) || $this->allowgroupby[$f]!=false ) {
				$sql_flds[] = $x.".".$f;
			}
		}
		$sql.=implode(", ",$sql_flds);
		$sql.=" FROM ".$db->getDBRef()."_res ".$x." ".implode(" ",$sql_tables);
		$sql.= " WHERE ".$sql_status; //echo $sql;
		
		//Filters
		if(!isset($this->titles[self::OBJECT_ID])) { $this->getFieldDetails(); }	//needed for fixed reports
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; 
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY ".$x.".id ";
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						/*case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;*/
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$a = "";
					switch($i) {
/*						case '': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$a = $this->report->getFilterSql("D",$t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;*/
						case 'resstate':	
						case 'date_completed':
						case 'result':			
						case 'resattachment':			
							break;	//do nothing; filter applied in row processing
							break;
						default:				
							if($i=="deptid") { $R = "P"; } else { $R = $x; }
							$a = $this->report->getFilterSql($R,$t,$f,$ft,$i);
					}
					if(strlen($a)>0) { $where[] = $a; }
				}
			}
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		if(count($having)>0) {
			$sql.= $sql_group;
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		
		$sql.=$this->getSortBySql();

		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array();
		$mod_flds = array(
			'resstatusid'=>"S.value",
			'resurgencyid'=>"U.value",
			'deptid'=>"D.value",
			'resportfolioid'=>"P.value",
			'resadduser'=>"AUT.tkname, AUT.tksurname",
			'restkid'=>"T.tkname, T.tksurname",
			'resactionowner'=>"AT.tkname, AT.tksurname",
			'date_completed'=>"",
			'result'=>"",
		);
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				if(isset($mod_flds[$s])) {
					if(strlen($mod_flds[$s])>0) { 
						$sort_by[] = $mod_flds[$s];
					}
				} elseif($s!="resattachment") {
					$sort_by[] = $s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	

}

?>