<?php

class MODULE_CAPS1B_REPORT extends MODULE_GENERIC_REPORT {

	protected $folder;
	//GET FIELD DETAILS
	protected $titles = array();
	protected $allowchoose = array();
	protected $default_selected = array();
	protected $allowfilter = array();
	protected $types = array();
	protected $default_data = array();
	protected $data = array();
	protected $allowgroupby = array();
	protected $allowsortby = array();
	protected $default_sort=100;
	protected $sortposition = array();
	
	
	
	

	public function __construct($p,$d,$c,$q) {
		parent::__construct($p,$d,$c,$q);
	}

	
	
	protected function prepareGenerator() {
		//echo "<P>module_query_report.prepareGenerator</p>";
		if($this->page=="graph") {
			$this->report->disableQuickReports();
			$this->report->disableFieldChooser();
			$this->report->enableGraphGrouping();
			$this->report->disableExcel();
			$this->report->disableFormatChooser();
		} elseif(isset($_REQUEST['quick_id'])) {
			$db = new ASSIST_DB("client");
			$quick = $db->mysql_fetch_one("SELECT * FROM ".$db->getDBRef()."_quick_report WHERE id = ".$_REQUEST['quick_id']);
			$this->report->setQuickReport($quick['id'],$quick['name'],$quick['description'],$quick['insertuser'],$quick['insertdate'],unserialize($quick['report']));
		}
		$this->getFieldDetails(); 
		$this->getFieldData();
		$this->displayPageHeading();
		$this->report->activateHistoryReport();
		
	}
	
	public function getReportNavButtons() {
		$nav = array(
			"report"=>array('id'=>"report",'url'=>"/".$_SESSION['modlocation']."/report.php",'active'=>($this->page=="report"),'display'=>"Report Generator"),
			"quick"=>array('id'=>"quick",'url'=>"/".$_SESSION['modlocation']."/report_quick.php",'active'=>($this->page=="quick"),'display'=>"Quick Report"),
			"history"=>array('id'=>"history",'url'=>"/".$_SESSION['modlocation']."/report_history.php",'active'=>($this->page=="history"),'display'=>"Report History"),
		);
		return $nav;
	}
	
	public function displayPageHeading() {
		/*//echo "<p>module_query_report.displayPageHeading</p>";
		//Navigation buttons & page title
		$header = array(0=>"",1=>"",2=>"");
		$location = explode("_",$this->page);
		$m0 = new Menu();
		$m0_menu = $m0->getAMenu("report");	
		$header[0] = "<a href='/".$_SESSION['modlocation']."/report/' class=breadcrumb>".($m0_menu['client_name'] == "" ? $m0_menu['name'] : $m0_menu['client_name'])."</a>";
		$h = array(1=>"report");
		if(isset($location[1])) { $h[2] = $location[0]; }
		foreach($h as $k=>$i) {
			$m_menu = $m0->getSubMenu($i);
			$menu = array();
			foreach($m_menu as $key => $m) {
				$m['url'] = "/".$_SESSION['modlocation']."/".$this->folder."/".$m['folder'].".php";
				$m['display'] = ($m['client_name'] == "" ? $m['name'] : $m['client_name']);
				if($k==1) { $l = $location[$k-1]; } else { $l = $this->page; } 
				$m['active'] = ($m['folder'] == $l ? true : false);
				if($m['active']===true) { $header[$k]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
				$menu[$key] = $m;
			}
			if(count($menu)>0) {
				$this->drawNavButtons($menu);
			}
		}
		foreach($header as $key => $h) { if(strlen($h)==0) { unset($header[$key]); } }
		echo "<h1>".implode(" >> ",$header)."</h1>";*/
		/*$nav = array(
			"report"=>array('id'=>"report",'url'=>"/".$_SESSION['modlocation']."/report.php",'active'=>($this->page=="report"),'display'=>"Report Generator"),
			"quick"=>array('id'=>"quick",'url'=>"/".$_SESSION['modlocation']."/report_quick.php",'active'=>($this->page=="quick"),'display'=>"Quick Report"),
			"history"=>array('id'=>"history",'url'=>"/".$_SESSION['modlocation']."/report_history.php",'active'=>($this->page=="history"),'display'=>"Report History"),
		);*/
		$nav = $this->getReportNavButtons();
			//"graph"=>array('id'=>"graph",'url'=>"/".$_SESSION['modlocation']."/report_graph.php",'active'=>($this->page=="graph"),'display'=>"Graph Generator"),
		//echo $this->page;
		$this->drawNavButtons($nav);
		switch($this->page) {
			case "graph":	$pt = "Graph Generator";	break;
			case "quick":	$pt = "Quick Report";	break;
			case "history":	$pt = "Report History";	break;
			case "report":
			default:
				$pt = "Report Generator";
				break;
		}
		echo "<h1>Complaints Assist >> Report >> ".($pt)."</h1>";
	}
	
	public function setFolder($f) {
		$this->folder = $f;
	}
	
protected function processTextForGroup($t) {
	$x = str_replace(" ","",$t); 
	$x = str_replace(chr(10),"",$x);
	return strtolower($x);
}
	
	protected function listSort($d) {
		$data = array();
		$d2 = array();
		foreach($d as $e) {
			if($e['id']==1 && (strtoupper($e['name'])=="UNSPECIFIED" || strlen($e['name'])==0) ) {
				$data[$e['id']] = "[Unspecified]";
			} else {
				$d2[$e['id']] = $e['name'];
			}
		}
		natcasesort($d2);
		foreach($d2 as $e=>$f) { $data[$e] = $f; }	
		return $data;
	}
	
	public function getResultOptions() {
		$data = array();
		foreach($this->result_categories as $key => $r) {
			$data[$key] = $r['text'];
		} 
		return $data;
	}

	public function getResultSettings() {
		return $this->result_categories;
	}
	
	protected function setFields() {
		
		foreach($this->titles as $i => $t) {	
			$ac = isset($this->allowchoose[$i]) ? $this->allowchoose[$i] : $this->defaults['allowchoose'];	
			$df = isset($this->default_selected[$i]) ? $this->default_selected[$i] : $this->defaults['default_selected'];
			$af = isset($this->allowfilter[$i]) ? $this->allowfilter[$i] : $this->defaults['allowfilter'];
			$dt = isset($this->types[$i]) ? $this->types[$i] : $this->defaults['type'];
			$do = isset($this->data[$i]) ? $this->data[$i] : $this->defaults['data'];
			$dd = isset($this->default_data[$i]) ? $this->default_data[$i] : $this->defaults['default_data'];
			$ag = isset($this->allowgroupby[$i]) ? $this->allowgroupby[$i] : $this->defaults['allowgroupby'];
			$as = isset($this->allowsortby[$i]) ? $this->allowsortby[$i] : $this->defaults['allowsortby'];
			$sp = isset($this->sortposition[$i]) ? $this->sortposition[$i] : $this->default_sort;
			$this->report->addField($i,$t,$ac,$df,$af,$dt,$do,$dd,$ag,$as,$sp);
			$this->default_sort++;
			
		}
	}
	
	
	public function getSQL($db,$filter) {
		return $this->setSQL($db,$filter);
	}
	
	

}

?>