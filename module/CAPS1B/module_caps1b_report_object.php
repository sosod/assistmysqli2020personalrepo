<?php

class MODULE_CAPS1B_REPORT_OBJECT extends MODULE_CAPS1B_REPORT {

	private $me;
	
	//CONSTRUCT
	private $date_format = "INT";
	private $my_class_name = "MODULE_CAPS1B_REPORT_OBJECT";
	private $my_quick_class_name = "MODULE_CAPS1B_QUICK_REPORT";
	private $my_history_class_name = "MODULE_CAPS1B_HISTORY_REPORT";
	const REFTAG = CAPS1B::REFTAG;
	const REPORTFILENAME = "complaints";
	const OBJECT_DEADLINE = "calldeadline";
	const OBJECT_ID = "callid";

	private $udf_filter = array();
	private $udf_data = array();
	
	protected $result_categories = array(
		'completedBeforeDeadline' => array("text" => "Completed Before Deadline Date", "code" => "completedBeforeDeadline", "color" => "blue"),
		'completedOnDeadlineDate' => array("text" => "Completed On Deadline Date", "code" => "completedOnDeadlineDate", "color" => "green"),
		'completedAfterDeadline' => array("text" => "Completed After Deadline", "code" => "completedAfterDeadline", "color" => "orange"),
		'notCompletedAndOverdue' => array("text" => "Not Completed And Overdue", "code" => "notCompletedAndOverdue", "color" => "red"),
		'notCompletedAndNotOverdue' => array("text" => "Not Completed And Not Overdue", "code" => "notCompletedAndNotOverdue", "color" => "grey"),
	);
//		'inactive' => array("text" => "Inactive due to Event Status", "code" => "inactive", "color" => "white"),

	//OUTPUT OPTIONS
	private $act;
	private $groups = array();
	private $group_rows = array();
	private $default_report_title = "Complaints Assist Report";
	private $multilist_results;
	private $multitext_separator = ";";
	
	private $backupdata = array();
	
	protected $historyObj;
	protected $history_report_id = 0;
	
	
public $start = 0;

	public function __construct($p) {
		$this->start = time();	
		//echo "<P>MODULE_COMPL_REPORT_DELIVERABLE.__construct()</p>";
		parent::__construct($p,$this->date_format,$this->my_class_name,$this->my_quick_class_name);
		$this->folder = "";
		$this->historyObj = new MODULE_CAPS_HISTORY_REPORT($p);
		$this->history_reports_running = $this->historyObj->howManyReportsInProgressForThisUser();
		////$this->markTime("end construct");
	}
	
	
	
	
public function markTime($s) {
	$start =$this->start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	//echo '<p>'.$s.' => '.$total_time.' seconds.';
}
	
	
	
	
	
	
	/*** FUNCTIONS REQUIRED TO SETUP ASSIST_REPORT_GENERATOR **/
	protected function prepareGenerator() {
		parent::prepareGenerator();
		//$this->markTime("end preparegenerator");
	}
	
	protected function getFieldDetails() {
		$this->me = new CAPS1B();
		$t = $this->me->getAllHeadings();
		$this->titles = $t; 
		//ASSIST_HELPER::arrPrint($this->titles);
		$this->titles['duration']="Duration";
		$this->titles['calldeadline']="Deadline for Completion";
		$this->titles['date_completed']="Date Completed";
		$this->titles['update_log']="Update History";
		$this->titles['logattachment']="Update Attachment";
		$this->titles['result']="Result";
		$this->titles['callid']="System Ref";
		$this->allowchoose = array(
			'callid'=>false,
		);
		$this->default_selected = array();
		$this->types = $this->me->getHeadingTypes();
		$this->types['duration']="TIME";
		$this->types['calldeadline']="DATETIME";
		$this->types['date_completed']="DATETIME";
		$this->types['update_log']="TEXT";
		$this->types['logattachment']="ATTACH";
		$this->types['result']="RESULT";
		$this->types['callid']="REF";
		$this->default_data = array();
		$this->data = array(
		);
		$this->allowgroupby = array(
			'callid'=>false,
			'callref'=>false,
			'update_log'=>false,
			'callattachment'=>false,
			'logattachment'=>false,
			'calldeadline'=>false,
			'duration'=>false,
		);
			//'date_completed'=>false,
		/*$this->allowsortby = array(
			'callstatusid'=>false,
			'callurgencyid'=>false,
			'calldeptid'=>false,
			'callareaid'=>false,
			'calltypeid'=>false,
		);*/
		$this->sortposition = array();
		$s = 1;
		foreach($this->titles as $key => $t) {
			if(!is_numeric($key) || !in_array($this->types[$key],array("UDFDATE"))) {
				$this->allowfilter[$key] = true;
			} else {
				$this->allowfilter[$key] = false;
			}
			if(!is_numeric($key)) {
				$this->allowsortby[$key] = true;
			} else {
				$this->allowsortby[$key] = false;
			}
			$this->sortposition[$key] = $s;
			$s++;
		}
		
		$this->allowsortby['callstatusid']=false;
			$this->allowsortby['callurgencyid']=false;
			$this->allowsortby['calldeptid']=false;
			$this->allowsortby['callareaid']=false;
			$this->allowsortby['calltypeid']=false;
		
		$this->allowsortby['logattachment']=false;
		$this->allowsortby['callattachment']=false;
		$this->allowsortby['duration']=false;
		$this->allowsortby['callid']=false;
		$this->allowsortby['date_completed']=false;
		$this->allowsortby['calldeadline']=false;
		$this->allowsortby['result']=false;
		$this->allowsortby['update_log']=false;
		$this->allowfilter['duration']=false;
		$this->allowfilter['callid']=false;
		$this->allowfilter['callref'] = false;
		//$this->allowfilter['date_completed']=false;
		$this->allowfilter['calldeadline']=false;
		$this->allowfilter['update_log']=false;
		$this->default_sort = 100;
		
		//$this->markTime("end getfielddetails");
	}
	
	protected function getFieldData() {
		$this->default_data = array();
		$this->data = array(
			'calltkid'	=>	$this->getOptionData("AddUserList"),
			'callrespid'=>	$this->getOptionData("Portfolio"),
			'callurgencyid'	=>	$this->getOptionData("Urgency"),
			'callstatusid'	=>	$this->getOptionData("Status"),
			'callareaid'	=>	$this->getOptionData("Area"),
			'calldeptid'	=>	$this->getOptionData("Dept"),
			'calltypeid'	=>	$this->getOptionData("Type"),
			'result'		=>	$this->getResultOptions(),
		);
		/*foreach($this->types as $key => $type) {
			if($type=="UDFLIST") {
				$this->data[$key] = $this->listSort($this->me->getUDFData($key));
			}
		}*/
		//$this->markTime("end getfielddata");
	}

	private function getOptionData($fld) {
		$data = array();
		$db = new ASSIST_DB("client");
		$db->setDBRef("CAPS");
		switch($fld) {
			case "Status":			$d = $this->me->getUsedStatus(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_status WHERE yn = 'Y' AND state >=0 ORDER BY sort"; break;
			case "AddUserList": 	$d = $this->me->getUsedAddUsers(); 				break; //$sql = "SELECT DISTINCT tkid as id, CONCAT(tkname,' ',tksurname) as name FROM assist_".$db->getCmpCode()."_timekeep INNER JOIN ".$db->getDBRef()."_res ON tkid = resadduser AND resstatusid <> 'CN' ORDER BY tkname, tksurname"; break;
			//case "UserList": 		$d = $this->me->getUsedResponsiblePersons(); 	break; //$sql = "SELECT DISTINCT tkid as id, CONCAT(tkname,' ',tksurname) as name FROM assist_".$db->getCmpCode()."_timekeep INNER JOIN ".$db->getDBRef()."_res ON tkid = restkid AND resstatusid <> 'CN' ORDER BY tkname, tksurname"; break;
			case "Portfolio": 		$d = $this->me->getUsedPortfolios(); 			break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_portfolio WHERE yn = 'Y' ORDER BY value"; break;
			case "Urgency": 		$d = $this->me->getUsedUrgency(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort"; break;
			case "Area": 		$d = $this->me->getUsedArea(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort"; break;
			case "Dept": 		$d = $this->me->getUsedDept(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort"; break;
			case "Type": 		$d = $this->me->getUsedType(); 				break; //$sql = "SELECT id as id, value as name FROM ".$db->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort"; break;
		}
		$e = array();
		foreach($d as $i => $v) {
			$e[$i] = array('id'=>$i,'name'=>$v);
		}
		$data = $this->listSort($e);
		return $data;
	}

	
	
	
	/***** FUNCTIONS FOR OUTPUT OF RESULTS ***/
	protected function prepareOutput() {
			$this->report->setReportFileName(self::REPORTFILENAME);
			$this->report->createSysFileName();
			$this->report->createUsrFileName();
			$sys = $this->report->getSysFileName();
			$this->report->setHistoryObj($this->historyObj);
			$this->history_report_id = $this->historyObj->createReport($sys, "", $this->report->getOutputExt());
		
		
		//ASSIST_HELPER::arrPrint($_REQUEST);
		$this->getFieldDetails();
		$this->getFieldData();
		$this->groupby = isset($_REQUEST['group_by']) && strlen($_REQUEST['group_by'])>0 ? $_REQUEST['group_by'] : "X";
		//echo $this->groupby;
		$this->setGroups();
		$rows = $this->getRows();
		$this->report->setReportTitle($this->default_report_title);
		foreach($this->result_categories as $key=>$r) {
			$this->report->setResultCategory($key,$r['text'],$r['color']);
		}
		$this->report->setRows($rows);
		foreach($this->groups as $key=>$g) {
			$this->report->setGroup($key,$g['text'],isset($this->group_rows[$key]) ? $this->group_rows[$key] : array());
		}
		$this->report->prepareSettings();
		//$this->markTime("end prepareoutput");
	/*
	 */
	}
	
	protected function prepareDraw() {
		$this->report->setReportFileName(self::REPORTFILENAME);
	}
	
	
	private function setGroups() {
		$groupby = $this->groupby; //echo $groupby;
		if($groupby!="X") {
			if(in_array($this->types[$groupby],array("LIST")) && isset($this->data[$groupby])) {
				foreach($this->data[$groupby] as $key => $t) {
					$this->groups[$this->processTextForGroup($t)] = $this->blankGroup($this->processTextForGroup($t),$t);
				}
			} elseif(in_array($this->types[$groupby],array("RESULT")) && isset($this->data[$groupby])) {
				foreach($this->data[$groupby] as $key => $t) {
					$this->groups[$this->processTextForGroup($key)] = $this->blankGroup($this->processTextForGroup($key),$t);
				}
			}
//		} else {
//			$this->groupby = "X";
		}
		$this->groups['X'] = $this->blankGroup("X","Unknown Group");
		//$this->arrPrint($this->groups);
		
	}
	
	private function blankGroup($id,$t) {
		return array('id'=>$id, 'text'=>stripslashes($t), 'rows'=>array());
	}
	
	private function allocateToAGroup($r) {  //ASSIST_HELPER::arrPrint($r);
		$i = $r[self::OBJECT_ID];
		$groupby = $this->groupby;
		if($groupby=="X") {
			$this->group_rows['X'][] = $i;
		} else {
			$g = explode(",",$r[$groupby]);
			if(count($g)==0) {
				$this->group_rows['X'][] = $i;
			} else {
				foreach($g as $k) {
					switch($this->types[$groupby]) {
						case "DATE":	
						case "DATETIME":
							if(is_numeric($k)) { $k = date("d F Y",$k); } 
							$x = $k; 
							if(!isset($this->groups[$x])) { $this->groups[$x] = $this->blankGroup($x,$k); }
							break;
						case "LIST":
						case "RESULT":
							$x = $this->processTextForGroup($k); break;
						case "TEXT":
							$x = $this->processTextForGroup($k); 
							if(!isset($this->groups[$x])) { $this->groups[$x] = $this->blankGroup($x,$k); }
							break;
						default: $x = $k; break;
					}
					$this->group_rows[$x][] = $i;
				}
			}
		}
	}
	
	
	
	
	
	
	private function getRows() {
		$rows = array();	//$this->arrPrint($_REQUEST['filter']);
		$final_rows = array();
		$action_progress = array();
		$db = $this->me;//new ASSIST_DB();
		$sql = $this->setSQL($db);
		//echo "<p>line 311:".$_REQUEST['filter']['result'].":";
		if(strlen($sql)>0) {
			$rows = array(); //echo $sql;
			$rows = $db->mysql_fetch_all_fld($sql,self::OBJECT_ID);
			if(count($rows)>0){
				//$udf_keys = $this->getUDFRows(array_keys($rows));
				$udf_keys = array(); //stripped udf functionality for speed
				$date_completed = $this->getDateCompleted($db,array_keys($rows)); //$this->arrPrint($_REQUEST['columns']);
				//$attachments = $this->getResAttachments($db,array_keys($rows)); //$this->arrPrint($_REQUEST['columns']);
				$logs = isset($_REQUEST['columns']['update_log']) ? $this->getUpdateLog($db,array_keys($rows)) : array();
				$attachments = isset($_REQUEST['columns']['callattachment']) ? $this->getResAttachments($db,array_keys($rows)) : array();
				$log_attachments = isset($_REQUEST['columns']['logattachment']) ? $this->getLogAttachments($db,array_keys($rows)) : array();
			} else {
				$udf_keys = array();
				$date_completed = array();
				$logs = array();
				$attachments = array();
				$log_attachments = array();
			}
			$from = 0;
			$to = 0;
	/**/
			if(isset($_REQUEST['filter']['date_completed']) && $_REQUEST['filter']['date_completed']['use']=="1") {
				$from = isset($_REQUEST['filter']['date_completed']['from']) ? $_REQUEST['filter']['date_completed']['from'] : "";
				$to = isset($_REQUEST['filter']['date_completed']['to']) ? $_REQUEST['filter']['date_completed']['to'] : "";
				$from = strlen($from)==0 && strlen($to)>0 ? $to : $from;
				$to = strlen($to)==0 && strlen($from)>0 ? $from : $to;
				$from = strlen($from)>0 ? strtotime($from." 00:00:00") : 0;
				$to = strlen($to)>0 ? strtotime($to." 23:59:59") : 0;
			}
			foreach($rows as $key => $r) {
				//if(in_array($key,$udf_keys)) {
					$r['date_completed'] = ($r['callstate']<>100 || !isset($date_completed[$key]) ? "" : $date_completed[$key]);
					$r['update_log'] = isset($logs[$key]) ? $logs[$key] : "";
					$r['callattachment'] = isset($attachments[$key]) ? $attachments[$key] : "";
					$r['logattachment'] = isset($log_attachments[$key]) ? $log_attachments[$key] : "";
					$r['result'] = $this->getCompliance(array('action_progress'=>($r['callstate']),'date_completed'=>$r['date_completed']),$r[self::OBJECT_DEADLINE]);
//					echo "<P>".$key." :: ".$r['callattachment']." :: count(".count($r['callattachment']).") :: isset(".isset($_REQUEST['filter']['callattachment']).") :: value(".($_REQUEST['filter']['callattachment']).") :: ALL(".(($_REQUEST['filter']['callattachment'])=="ALL").") :: 0(".(($_REQUEST['filter']['callattachment'])*1==0).") :: 1(".(($_REQUEST['filter']['callattachment'])*1==1).")";
					if(
						(!isset($_REQUEST['filter']['result']) || ($_REQUEST['filter']['result'][0]=="X" || in_array($r['result'],$_REQUEST['filter']['result'])))
						&&
						(
							!isset($_REQUEST['filter']['date_completed'])
							 || 
							($_REQUEST['filter']['date_completed']['use']=="ALL")
							 || 
							($_REQUEST['filter']['date_completed']['use']=="X" && $r['date_completed']=="") 
							 || 
							($_REQUEST['filter']['date_completed']['use']=="1" && ($from+$to)>0 && $r['date_completed']>=$from && $r['date_completed']<=$to)
						)
						&&
						(
							!isset($_REQUEST['filter']['callattachment'])
							 || 
							($_REQUEST['filter']['callattachment']=="ALL")
							 || 
							($_REQUEST['filter']['callattachment']*1==0 && (!is_array($r['callattachment']) || count($r['callattachment'])==0))
							 || 
							($_REQUEST['filter']['callattachment']*1==1 && is_array($r['callattachment']) && count($r['callattachment'])>0)
						)
						&&
						(
							!isset($_REQUEST['filter']['logattachment'])
							 || 
							($_REQUEST['filter']['logattachment']=="ALL")
							 || 
							($_REQUEST['filter']['logattachment']*1==0 && count($r['logattachment'])==0)
							 || 
							($_REQUEST['filter']['logattachment']*1==1 && count($r['logattachment'])>0)
						)
					) {
						$final_rows[$r[self::OBJECT_ID]] = $r;
						$c = $r[self::OBJECT_ID];
						foreach($this->titles as $i => $t) { //echo "<h2>".$i."</h2>";
							if((is_numeric($i) || isset($r[$i])) && isset($this->types[$i])) {
								$d = isset($r[$i]) ? $r[$i] : "";//$this->udf_data[$c][$i];
								$type = $this->types[$i];
								$is_udf = false;
								if(substr($type,0,3)=="UDF") {
									$is_udf = true;
								}
								switch($type) {
								case "REF":
									$d = self::REFTAG.$d;
									break;
								case "DATE":
									if($d=="0" || (is_numeric($d) && date("d-M-Y",$d)=="01-Jan-1970")) {
										$d = "";
									} else {
										$d = is_numeric($d) && date("d-M-Y",$d)!="01-Jan-1970" ? date("d-M-Y",$d) : $d; 
									}
									break;
								case "DATETIME":
									if($d=="0" || (is_numeric($d) && date("d-M-Y",$d)=="01-Jan-1970")) {
										$d = "";
									} else {
										$d = is_numeric($d) && date("d-M-Y",$d)!="01-Jan-1970" ? date("d-M-Y H:i",$d) : $d; 
									}
									break;
								case "TIME":
									if($r['callstate']==100) {
										$d = $r['date_completed'] - $r['calldate'];
									//} else {
									}
									if($d>86399) {
										$days = floor($d/86400);
										$d = $d - ($days*86400);
									} else {
										$days = 0;
									}
									if($d>3599) {
										$hours = floor($d/3600);
										$d = $d - ($hours*3600);
									} else {
										$hours = 0;
									}
									if($d>59) {
										$mins = floor($d/60);
										$sec = $d - ($mins*60);
									} else {
										$mins = 0;
										$sec = $d;
									}
									$d = ($days>0?$days." day":"");
									$d.= ($days>1?"s ":" ");
									$d.= ($hours<10?"0":"").$hours.":".($mins<10?"0":"").$mins.":".($sec<10?"0":"").$sec;
									//$d = ($days>0?$days." day".($days>1?"s":"")." ":"").date("H:i:s",$d)." ($d)";
									break;
								case "PERC":
									$d = number_format($d,2)."%";
									break;
								case "LOG":
								case "ATTACH":
									$d = $d;
									break;
								case "LIST":
									if(in_array($i, array("calltkid","callrespid"))) {
										$ia = false;
										if(is_array($d)) { $ia = true; $d = json_encode($d); }
										$d = stripslashes($d);
										if($ia) { $d = json_decode($d); }
									} else {
										if(strlen($d)>0 && $d!="0" && $d*1!=0) {
											$d = isset($this->data[$i][$d]) ? $this->data[$i][$d] : "Not available ($d)";
										} else {
											$d = $this->getUnspecified();
										}
									}
									break; 
								case "UDFTEXT": case "UDFNUM": case "UDFDATE": case "UDFLIST":
									$d = isset($this->udf_data[$r[self::OBJECT_ID]][$i]) ? $this->udf_data[$r[self::OBJECT_ID]][$i] : "";
									//break;
								case "MULTILIST":
								case "TEXT": case "SMLTEXT": case "MULTITEXT":
								default:
									$ia = false;
									if(is_array($d)) { $ia = true; $d = json_encode($d); }
									$d = stripslashes($d);
									if($ia) { $d = json_decode($d); }
									break;
								}
								$final_rows[$r[self::OBJECT_ID]][$i] = $d;
								$r[$i] = $d;
							}
						}
						$this->allocateToAGroup($r);
					}
				//}
			}
	 /**/
		}
		//echo $this->groupby;
		//$this->arrPrint($this->data);
		//ASSIST_HELPER::arrPrint($final_rows);
		//ASSIST_HELPER::arrPrint($this->group_rows);
		//$this->arrPrint($this->groups);
		return $final_rows;
	
	}
	
	
	public function getCompliance($ap,$d) {
		$result = "notCompletedAndOverdue";
		$deadline = $d;
		$p = $ap['action_progress'];
		$dc = $ap['date_completed'];
		if($p<100) {	//incomplete
			if(strtotime(date("d F Y")) > $deadline) {
				$result = "notCompletedAndOverdue";
			} else {
				$result = "notCompletedAndNotOverdue";
			}
		} else {		//complete
			if($dc < $deadline) {
				$result = "completedBeforeDeadline";
			} elseif($dc>$deadline) {
				$result = "completedAfterDeadline";
			} else {
				$result = "completedOnDeadlineDate";
			}
		}
		return $result;
		
	}
	
	public function getDateCompleted($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT logdateact as logactdate, logdate, logcallid FROM ".$db->getDBRef()."_log WHERE logstatusid = 4 ORDER BY logdateact DESC, logdate DESC, logcallid ASC";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $l) {
				$d = $l['logcallid'];
				if(!isset($result[$d])) {
					$x = $l['logactdate']>0 ? $l['logactdate'] : $l['logdate'];
					$result[$d] = $x;
				}
			}
		}
		//ASSIST_HELPER::arrPrint($data);
		//ASSIST_HELPER::arrPrint($result);
		return $result;
	}
	
	public function getUpdateLog($db,$objects) {
		$result = array();
		$rejects = array(
			"Complaints attachment deleted",
			"New complaint added",
			"Complaint id |id| edited",
		);
		$filter = strtoupper($_REQUEST['update_filter']); 
		if(count($objects)>0) {
			$sql = "SELECT logcallid, logadmintext as logupdate, IF(logdateact=0,logdate,logdateact) as logactdate, logdate, CONCAT(tkname,' ',tksurname) as name , logutype as logaction
					FROM ".$db->getDBRef()."_log 
					INNER JOIN assist_".$db->getCmpCode()."_timekeep 
					ON tkid = logtkid 
					WHERE logcallid IN (".implode(",",$objects).") 
					AND logstatusid <> 5
					AND logutype <> 'N'
					".($filter=="AUDIT"?"":"AND logutype IN ('A','U')")."
					ORDER BY IF(logdateact=0,logdate,logdateact) DESC"; //echo $sql;
			$data = $db->mysql_fetch_all($sql);  //ASSIST_HELPER::arrPrint($data);
			foreach($data as $r) {
				$i = $r['logcallid'];
				$ok = true;
				if(($filter=="LAST" && !isset($result[$i]) && (in_array($r['logaction'],array("U","A")) || strlen($r['logaction'])==0)) || ($filter=="ALL" && (in_array($r['logaction'],array("U","A")) || strlen($r['logaction'])==0)) || $filter=="AUDIT") {
					$l = strip_tags($this->decode($r['logupdate']));
					if($filter!="AUDIT") {
						foreach($rejects as $j) {
							$j = str_replace("|id|",$i,$j);
							if(substr($l,0,strlen($j))==$j) { $ok=false; }
						}
					}
					if($ok) {
						$result[$i][] = " - ".$l
										." (".(date("d-M-Y",$r['logactdate']*1)!=date("d-M-Y",$r['logdate']*1) && $r['logactdate']*1>0 ? "Action performed on: ".date("d-M-Y",$r['logactdate']*1)."; ":"")
										.($r['submit_user_id']==$r['user_id'] ? "Updated by ".$this->decode($r['name']) : "Updated by ".$this->decode($r['submit_name'])." on behalf of ".$this->decode($r['name']) )
										." on ".date("d-M-Y H:i",$r['logdate']).".)";
										//." (Updated by ".$this->decode($r['name'])" on ".date("d-M-Y H:i",$r['logdate']).")";

//						$result[$i][] = " - ".$l." (".(date("d-M-Y",$r['logactdate']*1)!=date("d-M-Y",$r['logdate']*1) && $r['logactdate']*1>0 ? "Action performed on: ".date("d-M-Y",$r['logactdate']*1)."; ":"")."Updated by ".$this->decode($r['name'])." on ".date("d-M-Y H:i",$r['logdate']).".)";//." (Updated by ".$this->decode($r['name'])" on ".date("d-M-Y H:i",$r['logdate']).")";
					}
				}
			}
		}
		return $result;
	}

	public function getResAttachments($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT callid, original_filename 
					FROM ".$db->getDBRef()."_attachments
					WHERE callid IN (".implode(",",$objects).") 
					ORDER BY id DESC";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $r) {
				$i = $r['callid'];
				$result[$i][] = " - ".$r['original_filename'];
			}
		}
		//$this->arrPrint($result);
		return $result;
	}
	public function getLogAttachments($db,$objects) {
		$result = array();
		if(count($objects)>0) {
			$sql = "SELECT a.logid, l.logcallid, a.original_filename 
					FROM ".$db->getDBRef()."_attachments a
					INNER JOIN ".$db->getDBRef()."_log l
					ON l.logid = a.logid
					AND l.logcallid IN (".implode(",",$objects).") 
					ORDER BY id DESC";
			$data = $db->mysql_fetch_all($sql);
			foreach($data as $r) {
				$i = $r['logcallid'];
				$result[$i][] = " - ".$r['original_filename'];
			}
		}
		//$this->arrPrint($result);
		return $result;
	}
	
	private function getUDFRows($keys,$filter=array()) {
		$udfs = array();
		foreach($this->titles as $key => $t) {
			if(is_numeric($key)) {
				$udfs[$key] = $t;
			}
		}
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; //ASSIST_HELPER::arrPrint($filters);
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		
		$data = array();
		
		
		
		foreach($udfs as $i => $t) {
			if(isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				$R = "U";
				$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
				$where = $this->report->getFilterSql($R,$t,$f,$ft,"udfvalue");
				if(strlen($where)>0) {
					$this->udf_filter[$i] = true; 
				}
			} else {
				$where = "";
			}
			//echo "<P>".$i.": ".$where.":";
			if($t=="UDFLIST") {
				$sql = "SELECT U.udfnum as callid, V.udfvvalue as value, U.udfindex as id , U.udfvalue as raw_value
						FROM assist_".$this->me->getCmpCode()."_udf U
						INNER JOIN assist_".$this->me->getCmpCode()."_udfvalue V
						ON U.udfvalue = V.udfvid
						WHERE U.udfref = '".$this->me->getModRef()."' AND U.udfindex = $i AND udfnum IN (".implode(",",$keys).") ".(strlen($where)>0 ? " AND ".$where : "");
			} else {
				$sql = "SELECT udfnum as callid, udfvalue as value, udfindex as id, U.udfvalue as raw_value FROM assist_".$this->me->getCmpCode()."_udf U
						WHERE U.udfref = '".$this->me->getModRef()."' AND U.udfindex = $i AND udfnum IN (".implode(",",$keys).") ".(strlen($where)>0 ? " AND ".$where : "");
			}
			$data[$i] = $this->me->mysql_fetch_all_by_id($sql, "callid"); 
			
		}
		//ASSIST_HELPER::arrPrint($data);
		
		$udf_keys = array();
		if(count($this->udf_filter)>0) {
			$filter_started = false;
			$is_blank = false;
			/*foreach($this->udf_filter as $i=>$f) {
				if($f==true && $is_blank!==true) {
					if(count($data[$i])==0) {
						$udf_keys = array();
						$is_blank = true;
					} elseif($filter_started==false) {
						$udf_keys = array_keys($data[$i]);
					} else {
						//$udf_keys = array_merge($udf_keys,array_keys($data[$i]));
						//foreach($data[$i] as $ci => $d) {
						//	
						//}
					}
				}
				if($is_blank==true) {
					break;
				}
			}*/
			foreach($keys as $ci) {
				$meets_filters = true;
				foreach($this->udf_filter as $i=>$f) {
					if($f==true) {
						if(!isset($data[$i][$ci]) || $data[$i][$ci]['value']=="") {
							$meets_filters = false;
							break;
						}
					}
				}
				if($meets_filters) {
					$udf_keys[] = $ci;
				}
			}
		} else {
			$udf_keys = $keys;
		}
		if(count($udf_keys)>0) {
			foreach($udf_keys as $callid) {
				$this->udf_data[$callid] = array();
				foreach($udfs as $i => $t) {
					$this->udf_data[$callid][$i] = isset($data[$i][$callid]['value']) ? $data[$i][$callid]['value'] : "";
				}
			}
		}
		//ASSIST_HELPER::arrPrint($udf_keys);
		return $udf_keys;
	}
	
	protected function setSQL($db,$filter=array(),$udf_keys=array()) {
		$x = "R";	//object alias in SQL
		$sql_status = "(".$x.".callstatusid <> 5)";
		$sql = "";
		
		//Preparation
		
		$flds = array_keys($this->titles);
			//'deptid'=>array('fld'=>"D.value as deptid",'table'=>""),
			//'resportfolioid'=>array('fld'=>"CONCAT(D.value,' - ',P.value) as resportfolioid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_portfolio P ON P.id = ".$x.".resportfolioid INNER JOIN ".$db->getDepartmentDBRef()." D ON P.deptid = D.id"),
			//'resadduser'=>array('fld'=>"CONCAT(AUT.tkname,' ',AUT.tksurname) as resadduser",'table'=>"INNER JOIN assist_".$db->getCmpCode()."_timekeep AUT ON AUT.tkid = ".$x.".resadduser"),
			//'resactionowner'=>array('fld'=>"CONCAT(AT.tkname,' ',AT.tksurname) as resactionowner",'table'=>"INNER JOIN assist_".$db->getCmpCode()."_timekeep AT ON AT.tkid = ".$x.".resactionowner"),
			//'callconaddress'=>array('fld'=>"CONCAT(".$x.".callconaddress1,'; ',".$x.".callconaddress2,'; ',".$x.".callconaddress3) as callconaddress",'table'=>""),
			$now = strtotime(date("d F Y H:i:s"));
			//$now = 1415009999;
		/*	'callstatusid'=>array('fld'=>"S.value as callstatusid, IF(callstatusid=4,100,50) as callstate",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_status S ON S.id = ".$x.".callstatusid"),
			'callurgencyid'=>array('fld'=>"U.value as callurgencyid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_urgency U ON U.id = ".$x.".callurgencyid"),
			'callareaid'=>array('fld'=>"AR.value as callareaid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_area AR ON AR.id = ".$x.".callareaid"),
			'calldeptid'=>array('fld'=>"DT.value as calldeptid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_dept DT ON DT.id = ".$x.".calldeptid"),
			'calltypeid'=>array('fld'=>"ET.value as calltypeid",'table'=>"LEFT JOIN ".$db->getDBRef()."_list_electype ET ON ET.id = ".$x.".calltypeid"),
		*/$mod_flds = array(
			'callrespid'=>array('fld'=>"CONCAT(p.value,' (',IF(ptk.tkname IS NULL,'Unspecified User',CONCAT(ptk.tkname,' ',ptk.tksurname)),')') as callrespid",'table'=>"LEFT OUTER JOIN ".$db->getDBRef()."_list_respondant p ON p.id = ".$x.".callrespid LEFT OUTER JOIN assist_".$db->getCmpCode()."_timekeep ptk ON p.admin = ptk.tkid"),
			'calltkid'=>array('fld'=>"CONCAT(T.tkname,' ',T.tksurname) as calltkid",'table'=>"INNER JOIN assist_".$db->getCmpCode()."_timekeep T ON T.tkid = ".$x.".calltkid"),
			'callconaddress'=>array('fld'=>"CONCAT(".$x.".callconaddress1,IF(LENGTH(".$x.".callconaddress2)>0,CONCAT('; ',".$x.".callconaddress2),''),IF(LENGTH(".$x.".callconaddress3)>0,CONCAT('; ',".$x.".callconaddress3),'')) as callconaddress",'table'=>""),
			'calldeadline'=>array('fld'=>"calldate + (3600*24) as calldeadline",'table'=>""),
			'duration'=>array('fld'=>"$now - calldate as duration",'table'=>""),
			'date_completed'=>array('fld'=>"",'table'=>""),
			'callstatusid'=>array('fld'=>"callstatusid, IF(callstatusid=4,100,50) as callstate",'table'=>""),
			'update_log'=>array('fld'=>"",'table'=>""),
			'result'=>array('fld'=>"",'table'=>""),
			'callattachment'=>array('fld'=>"",'table'=>""),
			'logattachment'=>array('fld'=>"",'table'=>""),
		);
			//'callattachment'=>array('fld'=>"COUNT(ca.callid) as callattachment",'table'=>"LEFT OUTER JOIN ".$db->getDBRef()."_attachments ca ON ".$x.".callid = ca.callid"),
//			'logattachment'=>array('fld'=>"COUNT(la.logid) as logattachment",'table'=>"LEFT OUTER JOIN ".$db->getDBRef()."_log l ON l.logcallid = ".$x.".callid AND l.logutype IN ('A','U') LEFT OUTER JOIN ".$db->getDBRef()."_attachments la ON l.logid = la.logid"),
		
		//Base sql
		$sql = " SELECT ";
		$sql_flds = array();
		$sql_tables = array();
		foreach($flds as $f) {
			if(isset($mod_flds[$f])) {
				if(strlen($mod_flds[$f]['fld'])>0) {
					$sql_flds[] = $mod_flds[$f]['fld'];
				}
				if(strlen($mod_flds[$f]['table'])>0) {
					$sql_tables[] = $mod_flds[$f]['table'];
				}
			} elseif(!is_numeric($f)) {
				$sql_flds[] = $x.".".$f;
			}
		}
		$sql.=implode(", ",$sql_flds);
		$sql.=" FROM ".$db->getDBRef()."_call ".$x." ".implode(" ",$sql_tables);
		$sql.= " WHERE ".$sql_status; //echo $sql;
		
		//Filters
		if(!isset($this->titles[self::OBJECT_ID])) { $this->getFieldDetails(); }	//needed for fixed reports
		$filters = count($filter)>0 ? $filter : $_REQUEST['filter']; //ASSIST_HELPER::arrPrint($filters);
		$filter_types = isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array();
		$where = array();
		$sql_group = " GROUP BY ".$x.".".self::OBJECT_ID;
		$having = array();
		foreach($this->titles as $i => $t) {
			//echo "<P>".$i;
			if( !is_numeric($i) && (!isset($this->allowfilter[$i]) || $this->allowfilter[$i]===true ) && isset($filters[$i])) {
				$t = $this->types[$i];
				$f = $filters[$i];
				if($t=="MULTILIST") {
					$c = array();
					switch($i) {
						/*case "accountable_person":
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								foreach($f as $h) {
									$c[] = " CONCAT(',', GROUP_CONCAT( DISTINCT AD.accountableperson_id SEPARATOR  ',' ) ,  ',' ) LIKE  '%,".$h.",%' ";
								}
							}
							break;*/
					}
					if(count($c)>0) {
						$having[] = " ( ".implode(" OR ",$c)." ) ";
					}
				} else {
					$ft = isset($filter_types[$i]) ? $filter_types[$i] : "";
					$a = "";
					switch($i) {
/*						case '': 				//textlist
							if(count($f)>0 && $f[0]!="X" && $f[0]!=0) {
								$a = $this->report->getFilterSql("D",$t,implode(",",$f),"ANY",$i,array('separator'=>",",'sql_separator'=>","));
							}
							break;*/
						case 'callstate':	
						case 'date_completed':
						case 'result':			
							break;	//do nothing; filter applied in row processing
						/*case 'callattachment':
						case 'logattachment':
							$ll = "COUNT(".($i=="logattachment" ? "la.logid" : "ca.callid").")";
							if(isset($filters[$i])) {
								switch($filters[$i]) {
									case "ALL":
										break;
									case "1":
										$having[] = $ll.">0";
										break;
									case "0":
										$having[] = $ll."=0";
										break;
								}
							}
							break;
						 * 
						 */	
						case "callconaddress":
							if($i=="deptid") { $R = "P"; } else { $R = $x; }
							$as = array();
							for($ca=1;$ca<=3;$ca++) {
								$sas = $this->report->getFilterSql($R,$t,$f,$ft,$i.$ca);
								if(strlen($sas)>0) { $as[] = $sas; }
							}
							if(count($as)>0) {
								$a = "((".implode(") OR (",$as)."))";
							}
							break;		
						default:				
							if($i=="deptid") { $R = "P"; } else { $R = $x; }
							$a = $this->report->getFilterSql($R,$t,$f,$ft,$i);
					}
					if(strlen($a)>0) { $where[] = $a; }
				}
			}
		}
		if(count($this->udf_filter)>0) {
			if(count($udf_keys)==0) {
				$udf_keys[] = 0;
			}
			$where[] = $x.".callid IN (".implode(",",$udf_keys).")";
		}
		//$this->arrPrint($s);
		if(count($where)>0) {
			$sql.= " AND ".implode(" AND ",$where);
		}
		$sql.= $sql_group;
		if(count($having)>0) {
			$sql.= " HAVING ".implode(" AND ",$having); 
		}
		
		$sql.=$this->getSortBySql();//." LIMIT 10";

		//echo $sql;
		return $sql;
	}
	
	protected function getSortBySql() {
		$sql = "";
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array();
		$sort_by = array(self::OBJECT_ID);
			//'deptid'=>"D.value",
			//'resportfolioid'=>"P.value",
			//'resadduser'=>"AUT.tkname, AUT.tksurname",
			//'resactionowner'=>"AT.tkname, AT.tksurname",
			//'callstatusid'=>"S.value",
			//'callurgencyid'=>"U.value",
		$mod_flds = array(
			'calltkid'=>"T.tkname, T.tksurname",
			'callrespid'=>"p.value, ptk.tkname, ptk.tksurname",
			'date_completed'=>"",
			'result'=>"",
		);
		if(is_array($sort) && count($sort)>0) {
			foreach($sort as $s) {
				if(isset($mod_flds[$s])) {
					if(strlen($mod_flds[$s])>0) { 
						$sort_by[] = $mod_flds[$s];
					}
				} elseif($s!="callattachment" && $s!=self::OBJECT_ID) {
					$sort_by[] = $s;
				}
			}
		}
		if(count($sort_by)>0) {
			$sql = " ORDER BY ".implode(",",$sort_by);
		}
		return $sql;
	}
	

}

?>