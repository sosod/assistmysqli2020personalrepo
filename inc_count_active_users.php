<?php
//echo session_save_path();
$session_files = scandir(session_save_path());
//print_r($session_files);
$session_now = strtotime(date("d F Y H:i:s"));
$total_sessions = count($session_files);
$session_count = 1;	//start at 1 to include current user (not yet logged in therefore size = 0
//$dead_count = 0;
//$time_count = 0;
foreach($session_files as $f) {
	if($f!="." && $f!=".." ) {
		$fn = session_save_path()."/".$f;
		$session_size = filesize($fn);
		$lastmod = filemtime($fn);
		$age = $session_now-$lastmod;
		if($session_size>0 && $age<3600) {
			$session_count++;
		//} elseif($size==0) {
		//	$dead_count++;
		//} else {
		//	$time_count++;
		}
	} else {
		$total_sessions-=1;
	}
}
if($session_count==0) { $session_count++; }
echo $session_count." active user".($session_count>1 ? "s" : "")." currently logged in.";
?>