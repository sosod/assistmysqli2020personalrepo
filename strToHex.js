var charArray = new Array(' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-','.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';','<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I','J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W','X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e','f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's','t', 'u', 'v', 'w', 'x', 'y', 'z');

var hex_digits = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f');

function charToByte(c)
{
	var i;
	for(i=0; i < charArray.length; i++) {
		if(c == charArray[i]) return i+32;
	}
	return 0;
}

function byteToHex(n)
{
	return hex_digits[(n >> 4) & 0xf] + hex_digits[n & 0xf];
}

function strToHex(char_str)
{
	var hex_str = "";
	var i, n;
	for(i=0; i < char_str.length; i++) {
		n = charToByte(char_str.charAt(i));
		if(n != 0) {
			hex_str += byteToHex(n);
		}
	}
	return hex_str;
}
