<?php
require_once("inc_header.php");
$page_redirect = "new.php";
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$all_users_who_are_not_employees = array();
$all_users_who_are_not_employees['active'] = $empObject->getListOfUsersWhoAreNotEmployees("active");
$all_users_who_are_not_employees['inactive'] = $empObject->getListOfUsersWhoAreNotEmployees("inactive");


?>
<style>
    html, body {
        overflow-y: hidden;
    }

    #div_container {
        overflow-y: scroll;
        width: 100%;

    }

    #new-convert-active {
        color: transparent;
        -webkit-animation: flash linear 0.3s;
        animation: flash linear 0.3s;

    }

    @-webkit-keyframes flash {
        0% {
            opacity: 0;
        }
        90% {
            opacity: 1;
            color: #fff;
        }

    }

    #new-nonuser {
        color: transparent;
        -webkit-animation: flash linear 0.3s;
        animation: flash linear 0.3s;

    }

    @-webkit-keyframes flash {
        0% {
            opacity: 0;
        }
        90% {
            opacity: 1;
            color: #fff;
        }

    }
</style>


<div id=div_container>
	<div id="tabs">
		<ul>
			<li><a href="#new-convert-active"><span>Convert Single Active <?php echo $empObject->getObjectName("USER"); ?></span></a></li>
			<li><a href="#new-multiple-active"><span>Convert Multiple Active <?php echo $empObject->getObjectName("USERS"); ?></span></a></li>
			<li><a href="#new-convert-inactive"><span>Convert Single Inactive <?php echo $empObject->getObjectName("USER"); ?></span></a></li>
			<li><a href="#new-multiple-inactive"><span>Convert Multiple Inactive <?php echo $empObject->getObjectName("USERS"); ?></span></a></li>
			<li><a href="#new-nonuser"><span>New Non <?php echo $empObject->getObjectName("USER")." ".$empObject->getObjectName("EMPLOYEE"); ?></span></a></li>
		</ul>
		<div id="new-convert-active">
			<?php
			$user_type = "active";
			if(count($all_users_who_are_not_employees[$user_type]) > 0) {
				$action = "single";
				include("new_tab_multiple.php");
			} else {
				ASSIST_HELPER::displayResult(array("error", "No users available to be converted."));
			}
			?>
		</div>
		<div id="new-multiple-active">
			<?php
			if(count($all_users_who_are_not_employees[$user_type]) > 0) {
				$action = "multiple";
				//include("new_tab_multiple.php");
			} else {
				ASSIST_HELPER::displayResult(array("error", "No users available to be converted."));
			}
			?>
		</div>
		<div id="new-convert-inactive">
			<?php
			$user_type = "inactive";
			if(count($all_users_who_are_not_employees[$user_type]) > 0) {
				$action = "single";
				include("new_tab_multiple.php");
			} else {
				ASSIST_HELPER::displayResult(array("error", "No users available to be converted."));
			}
			?>
		</div>
		<div id="new-multiple-inactive">
			<?php
			$user_type = "inactive";
			if(count($all_users_who_are_not_employees[$user_type]) > 0) {
				$action = "multiple";
				include("new_tab_multiple.php");
			} else {
				ASSIST_HELPER::displayResult(array("error", "No users available to be converted."));
			}
			?>
		</div>
		<div id="new-nonuser" style="overflow: auto;">

			<?php include("new_tab_nonuser.php"); ?>
		</div>
	</div>
</div>
<div id=dlg_form title="Convert User">
	<iframe id=ifr_form></iframe>
</div>
<!-- styling for the new_tab_multiple.php page -->
<style type="text/css">
    div.div-column-active {
        display: inline-block;
    }

    div.div-column-inactive {
        display: inline-block;
    }
</style>

<script type="text/javascript">
	var url = 'new.php?';
	var tab_name = "new-convert-active";
	$(function () {

		$("#tabs").tabs({
			heightStyle: "fit",
			activate: function (event, ui) {
				tab_name = AssistString.substr(ui.newPanel.selector, 1, 50);
			}
		});
		resizeContainer();
		$("#tabs").position("Fixed");

		/* function to update display of tabs if window is resized */
		function resizeContainer() {
			var containerPosition = $("#div_container").position();
			var screenSettings = AssistHelper.getWindowSize();
			let container_height_to_fill_screen = (screenSettings.height - containerPosition.top) - 10;
			$("#div_container").height(container_height_to_fill_screen);
			$("#tabs").tabs("refresh");
		}

		$('[href="#new-multiple-active"]').closest('li').hide();
		$('[href="#new-multiple-inactive"]').closest('li').hide();
		<?php
		$user_type = "inactive";
		if(count($all_users_who_are_not_employees[$user_type]) == 0) {
		?>
		$('[href="#new-convert-inactive"]').closest('li').hide();
		<?php
		}

		if(isset($_REQUEST['tab'])) {
			switch($_REQUEST['tab']) {
				case "last":
					echo "
					var index = $(\"#tabs a[href='#new-nonuser']\").parent().index();
					$(\"#tabs\").tabs('option','active',index);
					";
					break;
				default:
					echo "
					var index = $(\"#tabs a[href='#".$_REQUEST['tab']."']\").parent().index();
					$(\"#tabs\").tabs('option','active',index);
					";
			}
		}

		?>

		/***
		 * Code for tab content - new_tab_multiple.php
		 */
		//dialog to display add form
		$("#dlg_form").dialog({
			autoOpen: false,
			modal: true
		});
		resizeDialog();

		function resizeDialog() {

			var windowSize = getWindowSize(); //console.log(windowSize);
			var dialogWidth = windowSize['width'] < 1000 ? windowSize['width'] - 50 : windowSize['width'] - 150;
			var dialogHeight = windowSize['height'] < 1000 ? windowSize['height'] - 50 : windowSize['height'] - 150;

			//dialog to display add form
			$("#dlg_form").dialog("option", "width", dialogWidth);
			$("#dlg_form").dialog("option", "height", dialogHeight);
			$("#ifr_form").css("width", (dialogWidth - 20) + "px").css("height", (dialogHeight - 50) + "px");

		}


		//button to move multiple user selection to step 2
		$("#btn_multiple_step2").button().click(function (e) {
			e.preventDefault();
		});
		//button to select single user for conversion
		$(".btn_convert").button().click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var tkid = $(this).attr("tkid");
			var type = $(this).attr("user_type");
			$("#dlg_form").dialog("option", "title", "Convert " + type + " user");
			$("#ifr_form").prop("src", "new_form.php?user_type=" + type + "&tkid=" + tkid);

			//for development purposes only - live version must call open from inside iframe
			//$("#dlg_form").dialog("open");
		});


		/*
			//formatting of table displaying all users
			var td_name_width = 0;
			$(".td_name_active").each(function() {
				if($(this).width()>td_name_width) {
					td_name_width = $(this).width();
				}
			});
			$(".td_name_active").width(td_name_width+5);
			var table_width = $(".tbl-list-active:first").width();
			$(".div-column-active").width(table_width+15);

			var td_name_width = 0;
			$(".td_name_inactive").each(function() {
				if($(this).width()>td_name_width) {
					td_name_width = $(this).width();
				}
			});
			$(".td_name_inactive").width(td_name_width+5);
			var table_width = $(".tbl-list-inactive:first").width();
			$(".div-column-inactive").width(table_width+15);
		*/
		/**
		 * END CODE FOR TAB CONTENT - new_tab_multiple.php
		 */

		//Code to update the size of the tabs & dialog & iframe on window resize
		$(window).resize(function () {
			resizeContainer();
			resizeDialog();
		});


	});

	//function to open form dialog - to be called by form page
	function openUpdateDialog() {
		$(function () {
			AssistHelper.closeProcessing();
			$("#dlg_form").dialog("open");
		});
	}

	function dialogFinished(icon, result) {
		var u = makeURL();
		if (icon == "ok") {
			document.location.href = u + 'r[]=ok&r[]=' + result;
		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon, result);
		}
	}

	function makeURL() {
		return url + "tab=" + tab_name + "&";
	}
</script>