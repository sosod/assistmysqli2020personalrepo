<?php
$output_type = isset($_REQUEST['display']) ? $_REQUEST['display'] : "onscreen";
if($output_type != "onscreen") {
	$no_header_output = true;
}
require_once("inc_header.php");


$all_users_who_are_not_employees = $empObject->getListOfUsersWhoAreNotEmployees("all");

$employees = array(
	'head' => array(
		'id' => "User ID",
		'name' => "Name",
		'status' => "User Status"
	)
);

if($output_type == "export-plain" || $output_type == "export-format") {
	if($output_type == "export-plain") {
		$_REQUEST['output'] = "csv";
	} else {
		$_REQUEST['output'] = "excel";
	}
	$reportObject = new ASSIST_REPORT_DRAW_TABLE();
	$reportObject->setReportTitle("Employee / User Exception Report");
	$reportObject->setReportFileName($empObject->getModRef()."_exception_report");
	foreach($employees['head'] as $fld => $head) {
		$reportObject->addField($fld, $head);
	}
	$fld = "status";
	foreach($all_users_who_are_not_employees as $i => $emp) {
		$all_users_who_are_not_employees[$i][$fld] = $empObject->getUserStatusOptions($emp[$fld], false);
	}
	$reportObject->setRows($all_users_who_are_not_employees);
	$reportObject->setGroup("X", "Blank", array_keys($all_users_who_are_not_employees));
	$columns = array();
	foreach($employees['head'] as $fld => $head) {
		$columns[$fld] = "on";
	}
	$_REQUEST['columns'] = $columns;
	$_REQUEST['group_by'] = "X";
	$reportObject->prepareSettings();
	$reportObject->drawPage("GENERATE");


} else {

	?>
	<h2>Employee / User Exception Report</h2>
	<table>
		<tr>
			<?php
			foreach($employees['head'] as $fld => $head) {
				echo "<th>".$head."</th>";
			}
			?>
		</tr>
		<?php
		foreach($all_users_who_are_not_employees as $i => $emp) {
			echo "
		<tr>";
			foreach($employees['head'] as $fld => $head) {
				if($fld == "status") {
					$emp[$fld] = $empObject->getUserStatusOptions($emp[$fld]);
				}
				echo "
			<td>".$emp[$fld]."</td>";
			}
			echo "</tr>";
		}
		?>
	</table>
	<?php
	echo "<P class=i>Report Generated: ".date("d F Y H:i:s")."</P>";

}
?>