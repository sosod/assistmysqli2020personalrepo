<?php
/**
 * [SD] AA-643 EMP1 - Add search function to list pages
 * @var EMP1_DISPLAY $displayObject - from header file
 * @var EMP1 $helper - from header file
 * @var EMP1_HEADINGS $headingObject - from header file
 */

require_once("inc_header.php");
$page_reload = $_SERVER['PHP_SELF'];
$title = "Manage";
$is_view_page = false;
$is_edit_page = true;
$js = "";

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
if($page == "manage_edit.php"){
	$auto_select = "edit";
}else{
	$auto_select = "view";
}

//LIKE wildcards https://www.w3schools.com/sql/sql_like.asp
function drawField($section, $fld, $head) {
	global $user_details;
	global $full_user_details;
	global $can_enter_employee_details;
	global $displayObject;
	global $is_edit_page, $is_view_page, $is_add_page, $convert_user, $is_active_employee;
	global $emp_timekeep_field_map;
	global $helper;
	global $employee_user_changeover_timestamp;
	$js = "";
	if(($section == "USER" )) {
		if($fld == "tkname" || $fld == "tksurname") {
			echo "<input type='text' name='".$fld."' id='".$fld."' value=''  />";
		}
	} else {
		$prop = array(
			'id' => $fld,
			'name' => $fld,
		);
		$prop['req'] = isset($head['required']) ? $head['required'] : false;


		switch($head['type']) {
			case "EMPLOYEE":
				$listObject = new EMP1_EMPLOYEE();
				$select_data = $listObject->getListOfEmployeesFormattedForSelect();
				$list_options = array('S' => $listObject->getSelfManagerOption());
				foreach($select_data as $key => $name) {
					$list_options[$key] = $name;
				}
				$prop['options'] = $list_options;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld]) && strlen($user_details[$emp_timekeep_field_map[$fld]]) > 0) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
				} else {
					$js .= $displayObject->drawFormField("LIST", $prop, $value);
				}
				if($fld == "job_manager") {
					if(!$is_edit_page && !$is_view_page && $convert_user && isset($user_details['tkadddate']) && $user_details['tkadddate'] < $employee_user_changeover_timestamp && isset($user_details['tkmanager_name']) && !is_null($user_details['tkmanager_name'])) {
						echo "<br />[Previous Manager Listed in Users: <span class=u>" . $user_details['tkmanager_name'] . "</span>]";
					}
				}
				break;
			case "LIST":
				$listObject = new EMP1_LIST($head['list_table']);
				$list_options = $listObject->getActiveListItemsFormattedForSelect();
				$prop['options'] = $list_options;
				$prop['req'] = isset($head['required']) ? $head['required'] : false;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}

				$js .= $displayObject->drawFormField("LIST", $prop, $value);

				break;
			case "MASTER":
				$class = "ASSIST_MASTER_" . strtoupper($head['list_table']);
				$listObject = new $class();
				$list_options = $listObject->getActiveItemsFormattedForSelect();
				$prop['options'] = $list_options;
				$prop['req'] = isset($head['required']) ? $head['required'] : false;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}

				if($is_edit_page) {
					$val = $value;
					$value = "";
					foreach($list_options as $key => $v) {
						if(strtolower($v) == strtolower($val)) {
							$value = $key;
							break;
						}
					}
				}
				$js .= $displayObject->drawFormField("LIST", $prop, $value);

				break;
			case "USER_STATUS":
				echo $user_details['tkstatus'];
				break;
			case "EMP_STATUS":
				echo $user_details['emp_status'];
				break;
			case "DATE":
				$prop['options'] = array();
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
					if(strlen($value) > 0 && $value != "0000-00-00") {
						$value = date("d-M-Y", strtotime($value));
					} else {
						$value = "";
					}
				} elseif($convert_user && (isset($emp_timekeep_field_map[$fld]) || isset($user_details[$fld]))) {
					$value = isset($emp_timekeep_field_map[$fld]) ? $user_details[$emp_timekeep_field_map[$fld]] : $user_details[$fld];
					if(ASSIST_HELPER::checkIntRef($value)) {
						$value = date("d-M-Y", $value);
					}
				}

				$js .= $displayObject->drawFormField($head['type'], $prop, $value);

				break;
			case "ATTACH":
				$value = $head['default_value'];
				if($is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				}

				$prop['allow_multiple'] = false;
				//soft code this field
				// $prop['action'] = $section.".".$act; //The value for action is passed on the (EDIT and ADD document) script.
				//Url where user must be redirected
				//$prop['page_direct'] = "manage_edit_object.php?object_id=".$full_user_details['emp_id']."&documentsort=doc_id&";
				$js .= $displayObject->drawFormField($head['type'], $prop, $value);


				break;
			default:
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}

				$js .= $displayObject->drawFormField($head['type'], $prop, $value);

				break;
		}

	}
	return $js;
}
$sections = array("USER","EMPLOYEE","JOB");

//Get headings
$headings = array();
$headings['USER'] = $headingObject->getMainObjectHeadings("USER");
$headings['EMPLOYEE'] = $headingObject->getMainObjectHeadings("EMPLOYEE");
$headings['JOB'] = $headingObject->getMainObjectHeadings("JOB");

$userObject = new EMP1_USERACCESS();
$my_user_access = $userObject->getMyUserAccess();
$display_edit_button = false;
if($my_user_access['edit'] == true) {
	$display_edit_button = true;
}
?>
<h1 id="h_title"><a href='manage_view.php' class='breadcrumb'>Manage </a> >> Advanced Search</h1><br>
<form name='advanced_search' method='post'>
    <input type=hidden name='search_type' value='advanced' />
    <h2>1. Select filter you wish to apply</h2>

    <div id="filters" style="margin-left: 20px;margin-right: 20px; width: 55%;">
        <table class=form id="tbl_search" width="99%" >
			<?php
			foreach($sections as $section) {
				$heading_section = strtolower($section)."_heading";

				?>
                <tr>
                    <th class=th-head><?php if (isset($headings[$section]['rows'][$heading_section])) {echo  $headings[$section]['rows'][$heading_section]['name'];}else{echo "";} ?></th>
                </tr>
				<?php

				echo "<tr>";
				echo "<td><table  style='margin: -5px -5px; width: 101%;table-layout: fixed;'>";
				if(!isset($headings[$section]['rows'][$heading_section])) {
					$headings[$section]['rows'][$heading_section] = array();
				} //error mitigation
				$main_heading = $headings[$section]['rows'][$heading_section];
				if(!isset($main_heading['id'])) {
					$main_heading['id'] = array();
				}
				$main_H_id = $main_heading['id']; //causes error if not a variable

				if(is_array($headings[$section]['sub'][$main_H_id]) || is_object($headings[$section]['sub'][$main_H_id]))
				{
					foreach($headings[$section]['sub'][$main_heading['id']] as $id => $head) {
						$fld = $head['field'];
						if($fld != "emp_status"  && ($fld !="tktitle" && $fld !="user_contact" && $fld !="tkidnum" && $fld !="tkstatus")) {
							if($head['type'] != "TEXT" && $head['type'] != "DATE" && $head['type'] != "NUM" && $head['type'] != "EMPLOYEE" ) {
								?>
                                <tr id=tr_<?php echo $fld; ?>>
                                    <th style='background-color:#000098; color: white;'><?php echo $head['name'] . ":" ; ?></th>
                                    <td class=user-td-details>
										<?php
										if($head['type'] != "HEAD") {
											$js .= drawField($section, $fld, $head);
										} else {
											$sub_heading = $head;
											echo "<table id=tbl_" . $head['field'] . " class=noborder>";
											foreach($headings[$section]['sub'][$sub_heading['id']] as $sub_id => $sub_head) {
												echo "<tr><td class=b>" . $sub_head['name'] . ":</td><td>";
												$options = array('id' => $sub_head['field'],
													'name' => $sub_head['field'] ,);
												$js .= $displayObject->drawFormField($sub_head['type'], $options, $sub_head['default_value']);

												echo "</td></tr>";
											}
											echo "</table>";
										}
										?>
                                    </td>
                                </tr>
								<?php
							}
						}

					}//foreach($headings[$section]['sub'][$main_heading['id']] as $id => $head) {
				}//if (is_array($headings[$section]['sub'][$main_H_id]) || is_object($headings[$section]['sub'][$main_H_id]))
				echo "</table></td>";
				echo "</tr>";

			}//foreach(sections as section)

			?>

        </table>
        <table style='width: 99%;table-layout: fixed;'>
            <tr>
                <th>Search Output :</th>
                <td>
					<?php
                    $page_options = $display_edit_button == true  ? array("view"=>"View","edit"=>"Edit") : array("view"=>"View");
                    $js .= $displayObject->drawFormField("LIST", array("name"=>"search_output","id"=>"search_output","options"=>$page_options,"req"=>1),$auto_select);?>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input type="button" id="search_btn" value="Search" class=isubmit />
                    <input type="reset" value="Reset" name="B2" />
                </td>
            </tr>
        </table>
    </div>

</form>
<div id="dlg_confirm_page"></div>
<script type=text/javascript>

	$(function() {
		<?php echo $js;?>
		$("th").addClass("left").addClass("top").css({"min-width":"200px"});
		$("td").addClass("top");
		$("tr").off("mouseenter mouseleave");
		$(".note").css("color","#fe9900").css("font-style","italic").css("font-size","6.5pt");

		$("input:text.numb").keyup(function() {
			var v = $(this).val();
			if(v.length>0 && !(!isNaN(parseFloat(v)) && isFinite(v))) {
				$(this).addClass("required");
			} else {
				$(this).removeClass();
			}
		});

		/**
		 * Page confirm dialog
		 */
		$("#search_btn").click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var $form = $("form[name=advanced_search]");
			var output = $("#search_output").val();
			if (output == "edit"){
				$form.prop("action","manage_edit.php");
			}else {
				$form.prop("action","manage_view.php");
			}

			var err = false;
			$form.find("select").each(function () {

				$(this).removeClass("required");
				if ($(this).attr("id") == "search_output") {
					if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
						if ($(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0) {
							err = true;
							$(this).addClass("required");
						}
					}
				}else {

					if ($(this).val() == "X"){
						$(this).val("");
					}
				}
			});
			if (err) {
				AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
			} else {
				$form.submit();
			}


		});



	});
	jQuery(window).load(function () {
		/**
		 * @type {*|jQuery|HTMLElement}
         * comment: after page has loaded set all list to the first value so that it gets an value of either 0 or X (--SELECT--)
		 */
		var $form = $("form[name=advanced_search]");
		$form.find("select").each(function () {
			if ($(this).attr("id") != "search_output") {
				$(this).find('option:eq(0)').attr('selected', true);
			}
		});

	});
</script>
<?php $helper->displayGoBack(); ?>
