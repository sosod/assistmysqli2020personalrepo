<?php

//get list of users to be displayed from all_users array generated on parent new.php page
$users_who_are_not_employees = $all_users_who_are_not_employees[$user_type];


$number_of_users = count($users_who_are_not_employees);
$max_columns_per_screen = 4;
if($number_of_users < 10) {
	$max_columns_per_screen = 1;
} elseif($number_of_users < 20) {
	$max_columns_per_screen = 2;
} elseif($number_of_users < 30) {
	$max_columns_per_screen = 3;
}


//start of new_tab_multiple code - ALL VARIABLES WILL BE RESET BY NEXT CALL OF PAGE
$number_of_users_per_column = ceil(count($users_who_are_not_employees) / $max_columns_per_screen);


echo "<h2>Convert ".ucfirst($action)." User".($action != "single" ? "s" : "")." to Employee".($action != "single" ? "s" : "")."</h2>
	<h3>Step 1: Select User".($action != "single" ? "s" : "")." to be converted</h3>";

for($columns = 1; $columns <= $max_columns_per_screen; $columns++) {
	?>
	<div class="div-column div-column-<?php echo $user_type; ?>">
		<table class="tbl-list tbl-list-<?php echo $user_type; ?>">
			<tr>
				<th class=td_id>ID</th>
				<th class="td_name td_name_<?php echo $user_type; ?>">Name</th>
				<th></th>
			</tr>
			<?php
			$array_position_to_move_to = ($columns - 1) * $number_of_users_per_column + 1;
			$c = 0;
			foreach($users_who_are_not_employees as $id => $name) {
				$c++;
				if($c < $array_position_to_move_to) {

				} else {
					echo "
					<tr>
						<td class='b center'>$id</td>
						<td>$name</td>";
					if($action == "single") {
						echo "<td><button class=btn_convert tkid='$id' user_type='$user_type'>Convert</button></td>";
					} else {
						echo "<td><input type=checkbox class=chk_multiple value=Y name=user[$id] /></td>";
					}
					echo "
					</tr>";
				}
				if($c >= ($number_of_users_per_column * $columns)) {
					break;
				}
			}
			if($columns == $max_columns_per_screen && $c < ($number_of_users_per_column * $columns)) {
				for($x = $c; $c < ($number_of_users_per_column * $columns); $c++) {
					echo "
					<tr>
						<td class='b center' style='height:25px'>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>";
				}
			}
			?>
		</table>
	</div>
	<?php
}
//for multiple user selection, display the button to move to the next step
if($action != "single") {
	echo "<p><button id=btn_multiple_step2>Continue to Step 2</button></p>";
}
?>
	<script type="text/javascript">
		//all script is managed from new.php page as this page is called multiple times
	</script>
<?php


?>