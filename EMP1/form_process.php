<?php
/************
 * Purpose of this page is to receive form submissions and test resulting class function calls that would normally be handled by the inc_controller
 * This page will not be called by the live version
 */


require_once("inc_header.php");


/**
 * 16 Jan - testing adding a new employee so assuming $action sent to inc_controller = EMPLOYEE.ADD
 */
echo "<h3>Testing function call of EMP1_JOB.addObject</h3>";

$object = new EMP1_JOB();
$result = $object->editObject($_REQUEST);

ASSIST_HELPER::arrPrint($result);

echo "<hr />
<h3>_REQUEST data submitted</h3>";

ASSIST_HELPER::arrPrint($_REQUEST);
?>