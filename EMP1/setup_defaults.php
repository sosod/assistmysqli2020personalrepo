<?php
/**
 * from inc_header
 * @var EMP1_HEADINGS $headingObject
 */
require_once("inc_header.php");


$list_names = $headingObject->getStandardListHeadings();
//ASSIST_HELPER::arrPrint($list_names);


?>
<style type="text/css">
    #div_left, #div_right {
        padding: 0px 20px 20px 20px;
        width: 45%;
        height: 100%;
    }

    #div_container {
        padding: 0px 10px 0px 10px;
    }
</style>
<div id=div_container>
	<div id=div_right>
		<h2>Lists</h2>
		<table class="form middle">
			<?php
			foreach($list_names as $key => $val) {
				//[SD] 10-Aug-2021 #AA-671 EMP1 - Job title vs Acting As vs Manager (mention that AA & Man are covered by JobTitle list)
				$additional_info = "";
				switch($key){
					case "jobtitle":
						//did not use displayresult() since it echo's the div's, that displays info above the div_container.
						$additional_info = "<br><div class=\"ui-state-info ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em; width: 80%; \"><p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><span id=spn_displayResultText style=''>Please note the Job title list is also used in the Acting as and Manager list.</span></p></div></div>";
						break;
				}
				echo "	<tr>
	        					<th width=200px>".$val.":</th>
	        					<td>Configure the ".$val." list items.<span class=float><input type=button value=Configure class=btn_list id=".$key." /></span>".$additional_info."</td>
	    					</tr>";
			}
			?>

		</table>
	</div>
	<div id=div_left>
		<h2>Module Settings</h2>
		<table class="form middle">
			<tr>
				<th width=200px>Naming:</th>
				<td>Configure the naming convention for the various objects and activities.<span class=float><input type=button value=Configure class=btn_setup id="names"></span></td>
			</tr>
			<tr>
				<th width=200px>Menus:</th>
				<td>Configure the naming convention for the menus.<span class=float><input type=button value=Configure class=btn_setup id="menu"></span></td>
			</tr>
			<tr>
				<th>Headings:</th>
				<td>Configure the naming convention for the field headings.<span class=float><input type=button value=Configure class=btn_setup id="headings"></span></td>
			</tr><!--
	    <tr>
	        <th>List Columns:</th>
	        <td>Configure the columns to be displayed on the list pages.<span class=float><input type=button value=Configure class=btn_setup id=columns /></span></td>
	    </tr> -->
		</table>
		<!-- <h2>Module Reports</h2>
		<table class="form middle" >
			<tr>
				<th width=200px>User Access:</th>
				<td>View a summary of all User Access settings.<span class=float><input type=button value=Configure class=btn_report id=report_useraccess /></span></td>
			</tr>
			<tr>
				<th width=200px>Data Storage:</th>
				<td>View a report of the Data Usage by the module.<span class=float><input type=button value=Configure class=btn_report id=report_data /></span></td>
			</tr>
		</table> -->

	</div>
</div> <!-- end div container -->
<script type=text/javascript>
	$(function () {
		$("h2").css("margin-top", "15px");

		$("input:button").button().css("font-size", "75%");
		$("input:button.btn_setup").click(function () {
			document.location.href = "setup_defaults_" + $(this).prop("id") + ".php";
		});
		$("input:button.btn_list").click(function () {
			document.location.href = "setup_defaults_lists.php?l=" + $(this).prop("id");
		});
		$("input:button.btn_report").click(function () {
			document.location.href = "setup_defaults_" + $(this).prop("id") + ".php";
		});
		$("#div_right").css("float", "right");
		$("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
		$("table.form").css({"width": "100%"});
		$(window).resize(function () {
			var x = 0;
			var y = 0;
			$("#div_right").children().each(function () {
				x += AssistString.substr($(this).css("height"), 0, -2) * 1;
			});
			$("#div_left").children().each(function () {
				y += AssistString.substr($(this).css("height"), 0, -2) * 1;
			});
			if (x > y) {
				var z = x;
			} else {
				var z = y;
			}
			$("#div_container").css("height", (z + 50) + "px");
		});
		$(window).trigger("resize");
	});
</script>

