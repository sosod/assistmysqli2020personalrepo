<?php
$output_type = isset($_REQUEST['display']) ? $_REQUEST['display'] : "onscreen";
if($output_type != "onscreen") {
	$no_header_output = true;
}

require_once("inc_header.php");

$employees = $empObject->getList("REPORT_".$output_type, array(), "INACTIVE");

$logObject = new EMP1_LOG($empObject->getMyLogTable());
$logs = $logObject->getAuditLogsForReport(array('log_type' => EMP1_LOG::EDIT));
//ASSIST_HELPER::arrPrint($employees);
//ASSIST_HELPER::arrPrint($logs);


if($output_type == "export-plain" || $output_type == "export-format") {
	if($output_type == "export-plain") {
		$_REQUEST['output'] = "csv";
	} else {
		$_REQUEST['output'] = "excel";
	}
	$reportObject = new ASSIST_REPORT_DRAW_TABLE();
	$reportObject->setReportTitle("Terminated Employees Report");
	$reportObject->setReportFileName($empObject->getModRef()."_terminated_employees");
	foreach($employees['head'] as $fld => $head) {
		$reportObject->addField($fld, $head['name']);
	}
	$fld = "status";
	foreach($employees['rows'] as $i => $emp) {
		if(isset($logs[$i]) && count($logs[$i])) {
			$term_date = "";
			foreach($logs[$i] as $ld => $log) {
				//if tkstatus is in the changes log then assume changes made to the status of the user
				if(isset($log['changes']['tkstatus']['raw'])) {
					$l = $log['changes']['tkstatus']['raw'];
					if((($l['from'] & EMP1::ACTIVE) == EMP1::ACTIVE) && (($l['to'] & EMP1::INACTIVE) == EMP1::INACTIVE)) {
						$term_date = $ld;
						//don't break after finding termination log just in case user was restored and terminated again later
					}
				}
			}
			if(strlen($term_date) == 0) {
				$employees['rows'][$i]['emp_termination_date'] = $empObject->getUnspecified();
			} else {
				$employees['rows'][$i]['emp_termination_date'] = date("d M Y H:i:s", $term_date);
			}
		} else {
			$employees['rows'][$i]['emp_termination_date'] = $empObject->getUnspecified();
		}
	}
	$reportObject->setRows($employees['rows']);
	$reportObject->setGroup("X", "Blank", array_keys($employees['rows']));
	$columns = array();
	foreach($employees['head'] as $fld => $head) {
		$columns[$fld] = "on";
	}
	$_REQUEST['columns'] = $columns;
	$_REQUEST['group_by'] = "X";
	$reportObject->prepareSettings();
	$reportObject->drawPage("GENERATE");


} else {

	?>
	<h2>Terminated Employees Report</h2>
	<table>
		<tr>
			<?php
			foreach($employees['head'] as $fld => $head) {
				echo "<th>".$head['name']."</th>";
			}
			?>
		</tr>
		<?php
		foreach($employees['rows'] as $i => $emp) {
			//find termination date
			if(isset($logs[$i]) && count($logs[$i])) {
				$term_date = "";
				foreach($logs[$i] as $ld => $log) {
					//if tkstatus is in the changes log then assume changes made to the status of the user
					if(isset($log['changes']['tkstatus']['raw'])) {
						$l = $log['changes']['tkstatus']['raw'];
						if((($l['from'] & EMP1::ACTIVE) == EMP1::ACTIVE) && (($l['to'] & EMP1::INACTIVE) == EMP1::INACTIVE)) {
							$term_date = $ld;
							//don't break after finding termination log just in case user was restored and terminated again later
						}
					}
				}
				if(strlen($term_date) == 0) {
					$emp['emp_termination_date'] = $empObject->getUnspecified();
				} else {
					$emp['emp_termination_date'] = date("d M Y H:i:s", $term_date);
				}
			} else {
				$emp['emp_termination_date'] = $empObject->getUnspecified();
			}

			echo "
		<tr>";
			foreach($employees['head'] as $fld => $head) {
				echo "
			<td>".$emp[$fld]."</td>";
			}
			echo "</tr>";
		}
		?>
	</table>
	<?php
	echo "<P class=i>Report Generated: ".date("d F Y H:i:s")."</P>";
}
?>