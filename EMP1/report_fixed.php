<?php
require_once("inc_header.php");


?>
<table class=form style="margin-top: 20px">
	<tr>
		<th>Employee Register:</th>
		<td>Active Employee Details Report (regardless of Assist User status).</td>
		<?php $report_name = "report_fixed_register_allusers"; ?>
		<td>
			<button class=btn-generate id='<?php echo $report_name; ?>'>Generate</button>&nbsp;<button class=btn-export-format id='<?php echo $report_name; ?>'>Export (Formatted)</button>&nbsp;<button class=btn-export-plain id='<?php echo $report_name; ?>'>Export (Plain text)</button>
		</td>
	</tr>
	<tr>
		<th>Employee Register (Assist Users):</th>
		<td>Employee Details Report of those employees who are also Active Assist Users.</td>
		<?php $report_name = "report_fixed_register_activeusers"; ?>
		<td>
			<button class=btn-generate id='<?php echo $report_name; ?>'>Generate</button>&nbsp;<button class=btn-export-format id='<?php echo $report_name; ?>'>Export (Formatted)</button>&nbsp;<button class=btn-export-plain id='<?php echo $report_name; ?>'>Export (Plain text)</button>
		</td>
	</tr>
	<tr>
		<th>Employee Register (Non-Assist Users):</th>
		<td>Employee Details Report of those employees who are not Active Assist Users.</td>
		<?php $report_name = "report_fixed_register_inactiveusers"; ?>
		<td>
			<button class=btn-generate id='<?php echo $report_name; ?>'>Generate</button>&nbsp;<button class=btn-export-format id='<?php echo $report_name; ?>'>Export (Formatted)</button>&nbsp;<button class=btn-export-plain id='<?php echo $report_name; ?>'>Export (Plain text)</button>
		</td>
	</tr>
	<tr>
		<th>Terminated Employees:</th>
		<td>Terminated Employee Details Report (regardless of Assist User status).</td>
		<?php $report_name = "report_fixed_terminated"; ?>
		<td>
			<button class=btn-generate id='<?php echo $report_name; ?>'>Generate</button>&nbsp;<button class=btn-export-format id='<?php echo $report_name; ?>'>Export (Formatted)</button>&nbsp;<button class=btn-export-plain id='<?php echo $report_name; ?>'>Export (Plain text)</button>
		</td>
	</tr>
	<tr>
		<th>Exception Report:</th>
		<td>Detail Report of Assist Users who are not Employees.</td>
		<td>
			<button class=btn-generate id=report_fixed_exception>Generate</button>&nbsp;<button class=btn-export-format id=report_fixed_exception>Export (Formatted)</button>&nbsp;<button class=btn-export-plain id=report_fixed_exception>Export (Plain text)</button>
		</td>
	</tr>
</table>
<style type="text/css">
    table.form th {
        vertical-align: middle;
        padding: 5px 10px 5px 5px;
    }

    table.form td {
        vertical-align: middle;
        padding: 7px;
    }
</style>
<script type=text/javascript>
	$(function () {
		$("button.btn-generate").button({
			icons: {primary: "ui-icon-script"}
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
			.hover(function () {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-grey");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
			})
			.click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				var i = $(this).prop("id");
				document.location.href = i + ".php?display=onscreen";
			});
		$("button.btn-export-format").button({
			icons: {primary: "ui-icon-newwin"}
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
			.hover(function () {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-grey");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
			})
			.click(function (e) {
				e.preventDefault();
				var i = $(this).prop("id");
				document.location.href = i + ".php?display=export-format";
			});
		$("button.btn-export-plain").button({
			icons: {primary: "ui-icon-newwin"}
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
			.hover(function () {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-grey");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
			})
			.click(function (e) {
				e.preventDefault();
				var i = $(this).prop("id");
				document.location.href = i + ".php?display=export-plain";
			});
	});
</script>