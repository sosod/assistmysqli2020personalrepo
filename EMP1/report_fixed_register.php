<?php
$output_type = isset($_REQUEST['display']) ? $_REQUEST['display'] : "onscreen";
if($output_type != "onscreen") {
	$no_header_output = true;
}

require_once("inc_header.php");

$employees = $empObject->getList("MANAGE_".$output_type, array('user_filter' => $assist_user_status));


switch(strtolower($assist_user_status)) {
	case "all":
		$report_title = "Employee Register";
		break;
	case "active":
		$report_title = "Employee Register (Assist Users)";
		break;
	case "inactive":
		$report_title = "Employee Register (Non-Assist Users)";
		break;
}


if($output_type == "export-plain" || $output_type == "export-format") {
	if($output_type == "export-plain") {
		$_REQUEST['output'] = "csv";
	} else {
		$_REQUEST['output'] = "excel";
	}
	$reportObject = new ASSIST_REPORT_DRAW_TABLE();
	$reportObject->setReportTitle($report_title);
	$reportObject->setReportFileName($empObject->getModRef()."_".strtolower($assist_user_status)."users_employees");
	foreach($employees['head'] as $fld => $head) {
		$reportObject->addField($fld, $head['name']);
	}
	$fld = "status";
	foreach($employees['rows'] as $i => $emp) {

	}
	$reportObject->setRows($employees['rows']);
	$reportObject->setGroup("X", "Blank", array_keys($employees['rows']));
	$columns = array();
	foreach($employees['head'] as $fld => $head) {
		$columns[$fld] = "on";
	}
	$_REQUEST['columns'] = $columns;
	$_REQUEST['group_by'] = "X";
	$reportObject->prepareSettings();
	$reportObject->drawPage("GENERATE");


} else {

	echo "<h2>".$report_title."</h2>";
	?>
	<table>
		<tr>
			<?php
			foreach($employees['head'] as $fld => $head) {
				echo "<th>".$head['name']."</th>";
			}
			?>
		</tr>
		<?php
		foreach($employees['rows'] as $i => $emp) {
			echo "
		<tr>";
			foreach($employees['head'] as $fld => $head) {
				$fld = isset($emp[$fld]) ? $emp[$fld] : "";
				echo "
			<td>".$fld."</td>";
			}
			echo "</tr>";
		}
		?>
	</table>
	<?php
	echo "<P class=i>Report Generated: ".date("d F Y H:i:s")."</P>";
}
?>