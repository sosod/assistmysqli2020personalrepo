<?php

$is_add_page = true;
$convert_user = false;
$page_src = "tab";
$not_allowed_if_integrated = true;
$remove_tabs = true;

echo "<h2>New Non ".$empObject->getObjectName("USER")." ".$empObject->getObjectName("EMPLOYEE")."</h2>";


if(isset($not_allowed_if_integrated) && $not_allowed_if_integrated == true && $empObject->isCompanyHRIntegrated() == true) {
	ASSIST_HELPER::displayResult(array("error", "This function is not available as automatic HR Integration is active."));
	//die();
} else {

	include("common/form_object.php");
}

?>