<?php

/*****
 * Used for Add / Edit of employees
 *
 * REQUIRES:
 *    $is_edit_page = TRUE/FALSE
 * OR
 *    $is_add_page = TRUE/FALSE
 *    $convert_user = TRUE/FALSE
 *
 */
$is_edit_page = true;


$page_redirect_self = "manage_edit_object.php?a=reload";
foreach($_REQUEST as $key => $r) {
	$page_redirect_self .= "&".$key."=".$r;
}


function drawField($section, $fld, $head) {
	global $user_details;
	global $full_user_details;

	global $displayObject;
	global $is_edit_page, $is_view_page, $is_add_page, $convert_user, $is_active_employee;
	global $emp_timekeep_field_map;
	global $helper;
	$js = "";

	$prop = array(
		'id' => $fld,
		'name' => $fld,
	);
	$prop['req'] = isset($head['required']) ? $head['required'] : false;
	switch($head['type']) {
		case "EMPLOYEE":
			$listObject = new EMP1_EMPLOYEE();
			$select_data = $listObject->getListOfEmployeesFormattedForSelect();
			$list_options = array('S' => $listObject->getSelfManagerOption());
			foreach($select_data as $key => $name) {
				$list_options[$key] = $name;
			}
			$prop['options'] = $list_options;
			$value = $head['default_value'];
			if($is_edit_page || $is_view_page) {
				if(isset($user_details[$fld])) {
					$value = $user_details[$fld];
				} elseif(isset($full_user_details[$fld])) {
					$value = $full_user_details[$fld];
				}
			} elseif($convert_user && isset($emp_timekeep_field_map[$fld]) && strlen($user_details[$emp_timekeep_field_map[$fld]]) > 0) {
				$value = $user_details[$emp_timekeep_field_map[$fld]];
			}
			if($is_view_page) {
				$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
			} else {
				$js .= $displayObject->drawFormField("LIST", $prop, $value);
			}
			if($fld == "job_manager") {
				if(!$is_edit_page && !$is_view_page && $convert_user && isset($user_details['tkadddate']) && $user_details['tkadddate'] < $employee_user_changeover_timestamp && isset($user_details['tkmanager_name']) && !is_null($user_details['tkmanager_name'])) {
					echo "<br />[Previous Manager Listed in Users: <span class=u>".$user_details['tkmanager_name']."</span>]";
				}
			}
			break;
		case "LIST":
			$listObject = new EMP1_LIST($head['list_table']);
			$list_options = $listObject->getActiveListItemsFormattedForSelect();
			$prop['options'] = $list_options;
			$prop['req'] = isset($head['required']) ? $head['required'] : false;
			$value = $head['default_value'];
			if($is_edit_page || $is_view_page) {
				if(isset($user_details[$fld])) {
					$value = $user_details[$fld];
				} elseif(isset($full_user_details[$fld])) {
					$value = $full_user_details[$fld];
				}
			} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
				$value = $user_details[$emp_timekeep_field_map[$fld]];
			}
			if($is_view_page) {
				$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
			} else {
				$js .= $displayObject->drawFormField("LIST", $prop, $value);
			}
			break;
		case "MASTER":
			$class = "ASSIST_MASTER_".strtoupper($head['list_table']);
			$listObject = new $class();
			$list_options = $listObject->getActiveItemsFormattedForSelect();
			$prop['options'] = $list_options;
			$prop['req'] = isset($head['required']) ? $head['required'] : false;
			$value = $head['default_value'];
			if($is_edit_page || $is_view_page) {
				if(isset($user_details[$fld])) {
					$value = $user_details[$fld];
				} elseif(isset($full_user_details[$fld])) {
					$value = $full_user_details[$fld];
				}
			} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
				$value = $user_details[$emp_timekeep_field_map[$fld]];
			}
			if($is_view_page) {
				$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
			} else {
				if($is_edit_page) {
					$val = $value;
					$value = "";
					foreach($list_options as $key => $v) {
						if(strtolower($v) == strtolower($val)) {
							$value = $key;
							break;
						}
					}
				}
				$js .= $displayObject->drawFormField("LIST", $prop, $value);
			}
			break;
		case "USER_STATUS":
			echo $user_details['tkstatus'];
			break;
		case "EMP_STATUS":
			echo $user_details['emp_status'];
			break;
		case "DATE":
			$prop['options'] = array();
			$value = $head['default_value'];
			if($is_edit_page || $is_view_page) {
				if(isset($user_details[$fld])) {
					$value = $user_details[$fld];
				} elseif(isset($full_user_details[$fld])) {
					$value = $full_user_details[$fld];
				}
				if(strlen($value) > 0 && $value != "0000-00-00") {
					$value = date("d-M-Y", strtotime($value));
				} else {
					$value = "";
				}
			} elseif($convert_user && (isset($emp_timekeep_field_map[$fld]) || isset($user_details[$fld]))) {
				$value = isset($emp_timekeep_field_map[$fld]) ? $user_details[$emp_timekeep_field_map[$fld]] : $user_details[$fld];
				if(ASSIST_HELPER::checkIntRef($value)) {
					$value = date("d-M-Y", $value);
				}
			}
			if($is_view_page) {
				$js .= $displayObject->drawDataField("DATE", $value);
			} else {
				$js .= $displayObject->drawFormField($head['type'], $prop, $value);
			}
			break;
		default:
			$value = $head['default_value'];
			if($is_edit_page || $is_view_page) {
				if(isset($user_details[$fld])) {
					$value = $user_details[$fld];
				} elseif(isset($full_user_details[$fld])) {
					$value = $full_user_details[$fld];
				}
			} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
				$value = $user_details[$emp_timekeep_field_map[$fld]];
			}
			if($is_view_page) {
				$js .= $displayObject->drawDataField($head['type'], $value);
			} else {
				$js .= $displayObject->drawFormField($head['type'], $prop, $value);
			}
			break;
	}

	return $js;
}


//IF convert_user IS NOT SET THEN ASSUME THAT YOU ARE ADDING A NON-USER OR EDITING AN EXISTING USER
if(!isset($convert_user)) {
	$convert_user = false;
}
if(!isset($is_view_page)) {
	$is_view_page = false;
}
//If is_edit_page is not set then assume you are adding a new employee
if(!isset($is_edit_page)) {
	if(!isset($is_add_page)) {
		$is_add_page = true;
		$is_edit_page = false;
	} elseif($is_add_page) {
		$is_edit_page = false;
	} else {
		$is_edit_page = true;
	}
} else {
	if($is_edit_page || $is_view_page) {
		$is_add_page = false;
	} else {
		$is_add_page = true;
	}
}

if($is_add_page && !isset($page_redirect)) {
	$page_redirect = "manage_edit_object.php?object_id=".$object_id."&";
}

/**
 * Set booleans to decide which buttons to display at the end of the form
 */
$display_terminate_button = false;
$display_convert_button = false;
$display_new_button = false;
$display_edit_button = false;

/**
 * Get details to display in the form
 * Set the buttons
 */
$is_active_employee = true;
$full_user_details = array();
if($is_edit_page || $is_view_page) {
	if($is_edit_page) {

		$save_action = "EDIT";
		$form_type = "EDIT";
	} else {
		$save_action = "VIEW";
		$save_action = "VIEW";
	}
	$page_direct = "manage_edit.php?";
	if(!isset($object_id)) {
		$object_id = $_REQUEST['object_id'];
	}
	$full_user_details = $empObject->getRawEmployeeDetailsByEmployeeID($object_id, false);
	$user_id = $full_user_details[$empObject->getParentFieldName()];
	$user_details = $empObject->getFormattedOriginalUserDetails($user_id, $is_add_page);
	if($full_user_details['user_type'] == "active") {
		$is_active_user = true;
	} else {
		$is_active_user = false;
	}
	if(($full_user_details['emp_status'] & EMP1_EMPLOYEE::ACTIVE) == EMP1_EMPLOYEE::ACTIVE) {
		$is_active_employee = true;
	} else {
		$is_active_employee = false;
	}
	$user_details['emp_status'] = $empObject->getEmployeeStatusOptions($full_user_details['emp_status']);
	//echo ":::".$is_active_user.$full_user_details['user_type']."::::";
} elseif($convert_user) {
	$page_direct = "dialog";
	$save_action = "ADD";
	if($user_type == "active") {
		$form_type = "CONVERT";
		$page_redirect = "new.php?tab=new-convert-active&";
	} else {
		$page_redirect = "new.php?tab=new-convert-inactive&";
		$form_type = "CONVERT_INACTIVE";
	}
	$display_convert_button = true;
	$user_details = $empObject->getFormattedOriginalUserDetails($user_id, $is_add_page);
	$object_id = "";
} else {
	$save_action = "ADD";
	$page_direct = "new.php?tab=last&";
	$form_type = "ADD";
	$display_new_button = true;
	$object_id = "";
	$user_details = $empObject->getDefaultUserDetails();
	$user_id = "";
}
$emp_timekeep_field_map = $headingObject->getTimekeepFieldMap();
$employee_user_changeover_timestamp = $empObject->getModuleChangeoverTimestamp();

$headings['JOB'] = $headingObject->getMainObjectHeadings("JOB");

?>

<?php
if($is_edit_page) {
	//ASSIST_HELPER::arrPrint($job_headings);
	$section = "JOB";
	$job_head = $headings[$section]['rows']['job_heading'];
	$job_headings = $headings[$section]['sub'][$job_head['id']];
	?>
	<div id=div_job title="Add Job">
		<form name=frm_edit_job>
			<input type=hidden name=form_action id=form_action value='ADD' class='no-check'/>
			<input type=hidden id=new_job name=new_job value='1' class='no-check'/>
			<input type=hidden id=job_id name=job_id value='0' class='no-check'/>
			<input type=hidden id=job_empid name=job_empid value='<?php echo $full_user_details['emp_id']; ?>' class='no-check'/>
			<Table id=tbl_job_form class=form>
				<tr id=tr_edit_ref>
					<th>Ref:</th>
					<td>#</td>
				</tr>
				<?php
				foreach($job_headings as $h_id => $head) {
					echo "
			<tr id=tr_edit_".$head['field'].">
				<th>".$head['name'].($head['required'] == true ? "*" : "").":</th>
				<td>";
					$js .= drawField("JOB", $head['field'], $head);
					echo "</td>
			</tr>
			";
				}
				?>
				<tr>
					<th></th>
					<td>
						<button id=btn_save_job>Save Changes</button>

					</td>
				</tr>
			</Table>
			<div id=div_warn>
				<?php
				ASSIST_HELPER::displayResult(array("warn",
					"Warning: Adding a new Job will automatically close the previous job, if the start date for the new contract falls after the start date of the previous contract, and register a new contract for this Employee.  This will impact the assessment of any related Scorecards in the Individual Performance module."));
				?>
			</div>


		</form>
	</div>

	<?php
}
?>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		//table formatting
		$("table.form").find("th:not(.th-head)").addClass("thsub2");
		$("table.form").find("th.th-head").prop("colspan", 2);
		//$("table.form").find("td:not(.td-user-details):not(.td-buttons)").prop("colspan",2);
		$("#tbl_job_form th").css({"background-color": "#000098", "color": "white"});
		$("#tbl_details").width($("#tbl_details").width() + 100);
		$("#tbl_user_contact, #tbl_user_contact td").css("border", "0px solid #ffffff");
		//$("#div_warn").hide();


		//hide container table & first level cells
		$("#tbl_container").addClass("noborder").find("tr").each(function () {
			$(this).find("td:first:not(.not-noborder)").addClass("noborder");
		});
		//edit page buttons


		$("#btn_save_job").button({
			icons: {primary: "ui-icon-disk"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var $form = $("form[name=frm_edit_job]");
			var err = false;
			$form.find("select").each(function () {
				//console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
				$(this).removeClass("required");
				if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
					if ($(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0) {
						err = true;
						$(this).addClass("required");
					}
				}
			});
			$form.find("input:text, textarea").each(function () {
				//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
				$(this).removeClass("required");
				if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
					if ($(this).val() == "" || $(this).val().length == 0) {
						err = true;
						$(this).addClass("required");
					}
				}
			});
			if (err) {
				AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
			} else {
				var dta = AssistForm.serialize($form);
				var result = AssistHelper.doAjax("inc_controller.php?action=JOB." + $("#div_job #form_action").val(), dta);
				if (result[0] == "ok") {
					// var url = "manage_edit.php?";
					dialogFinished(result[0], result[1]);
					//AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
				} else {
					//var obj = "<?php echo $object_id;?>";
					//var url ='manage_edit_object.php?object_id='+obj+'&r[]=ok&r[]=' + result;
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			}
			//DEV PURPOSES ONLY - hide on live
			//$form.prop("action","form_process.php");
			//$form.prop("method","post");
			//$form.submit();

		}).removeClass("ui-state-default").addClass("ui-button-bold-green");


		$("td.missing-mileage").each(function () {
			$(this).find("div:first").css({
				"border": "1px solid #cc0001",
				"border-radius": "5px",
				"padding": "5px"
			}).addClass("red").addClass("red-back").addClass("red-border");
		});

	});
	//when called by dialog, page must open parent dialog
	<?php if(isset($page_src) && $page_src == "dialog") { ?>
	$(function () {
		window.parent.openUpdateDialog();
	});
	<?php } ?>

	<?php
	if($is_edit_page) {
	?>
	function dialogFinished(icon, result) {
		if (icon == "ok") {

			window.parent.$('#restoreEmp').click(); //button triggered is on the form object page before the script.

		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon, result);


		}
	}
	<?php
	}
	?>

</script>

