<?php
require_once("../../module/autoloader.php");

$section = $_REQUEST['section'];
$name_divider = ":";
$headingObject = new EMP1_HEADINGS();

$original_headings = array();

$sections = array("USER","EMPLOYEE","JOB");
if($section == "EMPLOYEE") { // If employee then we know that it will also need job fields
	foreach($sections as $sec) {
		$headings_raw = $headingObject->getMainObjectHeadings($sec, "DETAILS", "MANAGE");
		array_push($original_headings,$headings_raw);
	}
}

$headings = array('rows'=>array());

/*switch($section) {
	case "SUB":
		$headings['rows']['sub_function_id'] = array(
			'id'=>0,
			'field'=>"sub_function_id",
			'name'=>"Parent |function|",
			'section'=>"SUB",
			'type'=>"OBJECT",
			'list_table'=>"FUNCTION",
			'max'=>0,
			'parent_id'=>0,
			'parent_link'=>"",
			'default_value'=>"",
			'required'=>1,
			'help'=>"",
		);
		break;
}*/
$has_results = false;
foreach($original_headings as $field => $all_original_headings) {
	foreach($all_original_headings['sub'] as $fld => $data) {
		foreach($data as $inner_fld => $head) {
			$headings['sub'][$head['field']] = $head;
		}
	}
}

$data = array(
	0=>array(),
	1=>array(),
);

foreach($headings['sub'] as $fld => $head) {
	if($head['type']!="REF" && $head['type']!="ATTACH" && $head['type']!="USER_STATUS" && $head['type']!="EMP_STATUS" && $head['type']!="HEAD" && $head['field']!="job_end" && $head['field']!="Job_newcontract " && $head['field']!="ATTACH" && $head['field']!="ATTACH") {
		$name = $headingObject->replaceAllNames($head['name']);
		if($head['type'] != "REF" && $head['type'] != "COMPETENCY") {
			$data[0][] = str_replace('"', "'", ASSIST_HELPER::decode($name)).($head['required'] == 1 ? "*" : "");
			switch($head['type']) {
				case "MEDVC":
				case "LRGVC":
					$data[1][] = "Max characters: ".$head['max'];
					break;
				case "BOOL":
					$data[1][] = "Yes/No";
					break;
				case "NUM":
				case "TARGET":
					$data[1][] = "Numbers only";
					break;
				case "TEXT":
				case "LIST":
				default:
					$data[1][] = "";
					break;
			}
		}
	}
}

$fdata = "\"".implode("\",\"",$data[0])."\"\r\n\"".implode("\",\"",$data[1])."\"";

$cmpcode = $headingObject->getCmpCode();
$modref = $headingObject->getModRef();
$today = time();

//WRITE DATA TO FILE
$filename = "../../files/".$cmpcode."/".$modref."_template_".date("Ymd_Hi",$today).".csv";
$newfilename = "template.csv";
$file = fopen($filename,"w");
fwrite($file,$fdata."\n");
fclose($file);
//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$newfilename.'"');
readfile($filename);


?>