<?php
/**
 * @var EMP1_EMPLOYEE $me - from import.php
 * @var int $max_rows_to_import - from import.php
 * @var string $section - from calling page
 * @var EMP1_HEADINGS $headingObject - from inc_header
 * @var array $headings - from import.php => array('rows'=>array());
 * @var string $redirect_url - from import.php
 */

//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($_FILES);


//record output of import page
/**
 * NOT WORKING - JC 2018-07-15
 *
function recordOutput($buffer) {
	global $object_type;
	$helper = new ASSIST_MODULE_HELPER();
	$output = $helper->getPageHeader("1.10.0",$scripts,array("/assist_jquery.css","/assist3.css"));
	$output.=$buffer;

	$cmpcode = $helper->getCmpCode();
	$modref = $helper->getModRef();

	//WRITE DATA TO FILE
	$filename = "../files/".$cmpcode."/".$modref."/import/".strtolower($object_type)."_step3_".date("Ymd_His").".html";
	$file = fopen($filename,"w");
	fwrite($file,$output."\n");
	fclose($file);


	return $buffer;
}





/* Handle the uploading of the file */
/* ----------------------------------------------------------------------------------------------------------------------- */
$ext = "";
$error = "";
//Test that a valid file has been imported
$valid_file = true;
if(!isset($_FILES['import_file']['name']) || strlen($_FILES['import_file']['name'])==0) {
	$valid_file = false;
	$error = "No import file found.  Please go back and try again.";
} else {
	$name = $_FILES['import_file']['name'];
	$test = explode(".",$name);
	if(count($test)<2) {
		$error = "Invalid file name - Assist could not detect a valid file extension.  Please go back and try again.";
		$valid_file = false;
	} else {
		$ext = array_pop($test);
		if(strtoupper($ext)!="CSV") {
			$error = "Invalid file type - only Comma Separated/Delimited Values (CSV) files permitted.  Please go back and try again.";
			$valid_file = false;
		}
	}
}
//test file size before trying to upload
if($valid_file===true) {
	$max_file_size_allowed = $me->getMaxUploadFileSize();
	if($_FILES['import_file']['size']*1>$max_file_size_allowed*1) {
			$error = "Invalid file size - the file you have tried to upload is too big.  Please try again with a smaller file - only files up to ".$me->formatBytes($max_file_size_allowed,null,0)." are permitted.";
			$valid_file = false;
	}
}









//if error then display message and die
if($valid_file!==true) {
	ASSIST_HELPER::displayResult(array("error",$error));
	die();
}



//Upload the import file
$folder = $me->getAttachmentImportFolder();
$save_folder = $me->getSaveFolder();
$full_folder = $me->checkFolder($folder,1,true);
$file_name = $section."_".date("YmdHis");
$ext = ".".strtolower($ext);

//test file name is unique
$c = 1;
$test_name = $file_name.$ext;
while(file_exists($full_folder."/".$test_name)) {
	$file_name.="_".$c;
	$c++;
	$test_name = $file_name.$ext;
}
$file_name.=$ext;


//if you've gotten this far, then upload the file
if(move_uploaded_file($_FILES['import_file']['tmp_name'], $full_folder."/".$file_name)) {
	echo "";
	/*echo "
	<p><span class='b green'>Success!</span>  The file ". basename( $_FILES['mscoa_import_file']['name']). " has been uploaded.</p>
	<p>Assist is now processing the import file. Please do not close the window while this is happening...</p>";*/
} else {
	ASSIST_HELPER::displayResult(array("error","There was an unexpected error while trying to upload your file.  Please try again."));
	die();
}



/* ----------------------------------------------------------------------------------------------------------------------- */
/* now to process the import file into the list */
/* ----------------------------------------------------------------------------------------------------------------------- */

//read the uploaded file
$import = fopen($full_folder."/".$file_name,"r");
$data = array();
while(!feof($import)) {
	$tmpdata = fgetcsv($import);
	if(count($tmpdata)>0) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($import);

//test that data exists in the file - check for more than 2 lines (2 lines = heading and sub-heading)
if(count($data)<3) {
	ASSIST_HELPER::displayResult(array("error","No data was found to be imported.  Please check that your file is not empty."));
	die();
}

//remove heading & definition rows
unset($data[0]);
unset($data[1]);

//check for too many lines (limit set in parent page)
if(count($data)>$max_rows_to_import) {
	ASSIST_HELPER::displayResult(array("error","Your file has too much data in it to be processed in a single upload.  Please break up your file into multiple smaller files of no more than $max_rows_to_import rows and try again."));
	die();
}

//prepare variables before going through document
//$headings = $myObject->getHeadingsForImport(); ---- provided by parent page
$has_data = false;
$processed_data = array();
$errors = array();
$status_count = array(
	'rows'=>array('ok'=>0,'warn'=>0,'error'=>0),
	'fields'=>array('ok'=>0,'warn'=>0,'error'=>0),
);

$import_data = array();
$import_results_data = array();
$import_ref = $file_name.".".$me->getUserID();



$all_list_items = array();
foreach($headings['sub'] as $fld => $head) {
	$fld_type = $head['type'];
	if($headingObject->isListField($fld_type)) {
		$list_table = $head['list_table'];
		$okay = true;
		$extra_info = "";
		switch($fld_type) {
			case "MULTILIST": $is_multi = true;
			case "LIST":
				$listObject = new EMP1_LIST($list_table);
				break;
			case "MASTER":
				$class = "ASSIST_MASTER_" . strtoupper($list_table);
				$listObject = new $class();
				break;
			case "EMPLOYEE":
				$listObject = new EMP1_EMPLOYEE();
				break;

			default:
				$okay = false;
				$listObject = null;
				break;
		}
		//$list_items = $listObject->getActiveListItemsFormattedForImportProcessing($extra_info);
		if($fld_type == "MASTER"){
			$valid_items = $listObject->getActiveItemsFormattedForSelect($extra_info);
			$items = array();
			foreach($valid_items as $field => $val) {
				$items[strtolower($val)] = $field;
			}
			$all_list_items[$list_table] = $items;
			//$list_items = $items;
		}elseif($fld_type == "EMPLOYEE"){
			$select_data = $listObject->getListOfEmployeesFormattedForSelect();
			$list_options = array(strtolower(trim(strtolower(str_replace(" ","",$listObject->getSelfManagerOption()))))=>'S');
			foreach($select_data as $key => $name) {
				$trimmed_name = strtolower(trim(strtolower(str_replace(" ","",$name))));
				$list_options[$trimmed_name] = $key;
			}
			$all_list_items[$fld_type] = $list_options;

		}else {
			$valid_items = $listObject->getActiveListItemsFormattedForSelect($extra_info);
			$list_items = $listObject->getActiveListItemsFormattedForImportProcessing($valid_items);
			$all_list_items[$list_table] = $list_items;

		}
		/*if($okay) {
			$list_items = $listObject->formatListForImportProcessing($valid_items);
			$all_list_items[$list_table] = $list_items;
			unset($listObject);
		}*/
	}

}

//this is used to mark the existence of a manager(by name) without first loading them on the system
$temp_incoming_users_storage = array();

//process the import data
foreach($data as $key => $row) {
	$row = $me->removeBlanksFromArray($row,false,true);
	if(count($row)>0) {
		$has_data = true;
		$cell = 0;
		$processed_data[$key] = array(
			'row'=>$key+1,
			'import_status' => array(
				'final_icon'=>"ok"
			),
			'import_results'=>array(),
		);
		$has_error = false;
		$row_error = false;
		$row_warning = false;
		$targets = array();

		$already_exists = array();
		//process headings for main object import
		foreach($headings['sub'] as $fld => $head) {//echo $fld ; ASSIST_HELPER::arrPrint($head);
			$do_not_save_var = false;
			$is_required = $head['required']==true;
			$max_characters = $head['type']=="TEXT"?"65000":$head['max'];
			$fld_type = $head['type'];
			$fld_name = $head['name'];

			$value = isset($row[$cell]) ? $row[$cell] : "";
			$value = ASSIST_HELPER::code($value,"ISO-8859-1");
			
			//as it loops, we're storing all users to mark as promise that they will be on the system.
			//this is used to mark the existence of a manager(by name) without first loading them on the system
			$new_user_name = strtolower(trim(strtolower(str_replace(" ","",$data[$key][1]."".$data[$key][2]))));
			$temp_incoming_users_storage[$key] = $new_user_name;

			$processed_data[$key][$fld] = $value;
			$has_error = false;

			$listObj = new EMP1_EMPLOYEE();
			$all_users = $listObj->getListOfEmployeesFormattedForSelect();

			if($is_required && strlen($value)==0) {
				$has_error = true;
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - $fld_name is a required field.";
			} elseif($fld_type=="BOOL" || $fld_type=="BOOL_BUTTON") {
				if(strlen($value)==0) {
					$value="No";
				}
				$value = trim(strtoupper($value));
				if($value!="YES" && $value!="NO") {
					$has_error = true;
					$errors[$key][$fld] = "error";
					$status_count['fields']['error']++;
					$processed_data[$key]['import_status']['final_icon'] = "error";
					$processed_data[$key]['import_status'][$fld] = "Error - Invalid Yes/No answer.";
				} elseif($value=="YES") {
					$value = 1;
				} else {
					$value = 0;
				}
			} elseif($headingObject->isTextField($fld_type) && strlen($value)>$max_characters) {
				$has_error = true;
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - Text exceeds maximum allowed length.  Text length is ".strlen($value)." while the maximum allowed is ".$max_characters.".";
			} elseif($headingObject->isListField($fld_type)) {
				if(strlen($value) > 0) {
					$is_multi = false;
					$code_me = false;
					$list_table = $head['list_table'];
					$extra_info = array();
					switch($fld_type) {
						case "MULTILIST":
						case "MULTISEGMENT":
							$is_multi = true;
							break;
						case "MULTIOBJECT":
							$is_multi = true;
						case "OBJECT":
							if(strpos($list_table, "|") !== false) {
								$lon = explode("|", $list_table);
								$list_table = $lon[0];
								$extra_info = $lon[1];
							}
							break;
					}

					if(!isset($all_list_items[$list_table]) && $fld_type !== "EMPLOYEE") { //manage(by name) is type EMPLOYEE not LIST. So it has it its own validator below
						switch($fld_type) {
							case "MULTILIST":
							case "LIST":
								$listObject = new EMP1_LIST($list_table);
								break;
							case "MASTER":
								$class = "ASSIST_MASTER_" . strtoupper($list_table);
								$listObject = new $class();
								break;

						}
						if($fld_type == "MASTER"){
							$valid_items = $listObject->getActiveItemsFormattedForSelect($extra_info);
							$items = array();
							foreach($valid_items as $field => $val) {
								$items[strtolower($val)] = $field;
							}
							$list_items = $items;

						}else {
							$valid_items = $listObject->getActiveListItemsFormattedForImportProcessing($extra_info);
							$list_items = $valid_items;
						}


						$all_list_items[$list_table] = $list_items;
						unset($listObject);


					}elseif($fld_type == "EMPLOYEE" && !isset($all_list_items[$fld_type])){
						$listObject = new EMP1_EMPLOYEE();
						$select_data = $listObject->getListOfEmployeesFormattedForSelect();
						$list_options = array(strtolower(trim(strtolower(str_replace(" ","",$listObject->getSelfManagerOption()))))=>'S');
						foreach($select_data as $user_key => $name) {
							$trimmed_name = strtolower(trim(strtolower(str_replace(" ","",$name))));
							$list_options[$trimmed_name] = $user_key;
						}
						$all_list_items[$fld_type] = $list_options;
						$list_items = $all_list_items;
					}else {
						if($fld_type == "EMPLOYEE"){ //manage(by name) is type EMPLOYEE not LIST
							$list_items = $all_list_items[$fld_type];
						}else{
							$list_items = $all_list_items[$list_table];
						}
					}

					if($is_multi) {
						$test_values = explode(";", $value);
						$result = array();
						foreach($test_values as $test) {
							if(strlen($test) > 0) {
								$test = trim(strtolower(str_replace(" ", "", ($code_me ? ASSIST_HELPER::code($test) : $test))));
								if(!isset($list_items[$test])) {
									$has_error = true;
									break;
								} else {
									$result[] = $list_items[$test];
								}
							}
						}
						if(!$has_error) {
							$value = implode(";", $result);
						}
					} else {
						$test_value = strtolower(str_replace(" ", "", (($code_me ? ASSIST_HELPER::code($value) : $value))));
						if(!isset($list_items[$test_value])) {
							if($fld_type == "EMPLOYEE" ){
								//since employee ($test_value) in $list_items[$test_value] is not set, check if on the temporal storage to see if this user exists and remove error trigger
								if(in_array($test_value,$temp_incoming_users_storage)){
									$has_error = false;
								}else{
									$has_error = true;
								}
							}else{
								$has_error = true;
							}

						} else {
							$value = $list_items[$test_value];
						}

					}
					if($has_error) {
						$errors[$key][$fld] = "error";
						$status_count['fields']['error']++;
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - List item \"".$value."\" not found.";
					}
				} else {
					//Taken this isListField check as it is happening inside an existing isListField check. AA-655 [JC] 16 August 2021
	//				if($headingObject->isListField($fld_type)) {
	//				}else {
	//				}
					//update data for display
						$value = $headingObject->getUnspecified();
					$processed_data[$key][$fld] = $value;
					$value = 0;
				}
			}elseif($fld_type=="NUM" || $fld_type=="TARGET") { //ASSUME TARGET IS REQUIRED TO BE A NUMBER so handle initial steps the same
				//if blank then default to 0 to save lazy user effort
				if(strlen(trim($value))==0) {
					$value="0";
					//update data for display to match final product
					$processed_data[$key][$fld] = $value;
				}
				//test for errors
				$test_values = array("1","2","3","4","5","6","7","8","9","0","-",".");
				$err_char = "";
				$test_err = false;
				$found_decimal = false;
				for($x=0;$x<strlen($value);$x++) {
					$char = $value[$x];
					if(!in_array($char,$test_values) || ($char=="." && $found_decimal==true)) {
						$has_error = true;
						$test_err = true;
						$err_char.= $char;
					}
					if($char==".") { $found_decimal=true; }
				}
				if($test_err) {
					$errors[$key]['results'][$fld] = "error";
					$status_count['fields']['error']++;
					$processed_data[$key]['import_status']['final_icon'] = "error";
					@$processed_data[$key]['import_status'][$fld].= "Error - Invalid number \"$value\" due to character(s) \"$err_char\", only unformatted numbers permitted.";
				} elseif($fld_type=="TARGET") { 				//if no errors then do target specific handling
					$f = explode("_",$fld);
					$fld_fld = $f[0];	//keep the fld var the same for display purposes but get the actual field
					$time_id = $f[1];
					$targets[$fld_fld][$time_id]=$value;
					$do_not_save_var = true;
				}
			}elseif(isset($all_users) && array_key_exists($key,$temp_incoming_users_storage) ){//checks to see if user being imported exists and inform user if so
				$new_user_name = strtolower(trim(strtolower(str_replace(" ","",$data[$key][1]."".$data[$key][2]))));



				foreach($all_users as $key_fld => $existing_name) {
					$assist_user = strtolower(trim(strtolower(str_replace(" ","",ASSIST_HELPER::decode($existing_name)))));
					if($new_user_name == $assist_user && !array_key_exists($new_user_name,$already_exists)){
						$already_exists[$new_user_name] = $key;
						$row_warning = true;
						$errors[$key]['results'][$fld] = "info";
						$status_count['fields']['error']++;
						$processed_data[$key]['import_status']['final_icon'] = "info";
						@$processed_data[$key]['import_status'][$fld].= "Warning - \"$existing_name\" already exists.";
					}
				}

			}

			if(!$has_error) {
				$errors[$key][$fld] = "";
				$status_count['fields']['ok']++;
				//only send the value to the SQL if it hasn't been handled by the TARGET step
				if($do_not_save_var==false) {
					$import_data[$key][$fld] = $value; //ASSIST_HELPER::code($value);
				}
			} else {
				$row_error = true;
			}
			$cell++;
		}
		//ASSIST_HELPER::arrPrint($temp_incoming_users_storage);

		if(!$row_error) {
			$status_count['rows']['ok']++;
			$import_data[$key]['import_key']=$key;
			$import_data[$key]['import_ref']=$import_ref;
			$import_data[$key]['import_results']=base64_encode(serialize($targets));
			$import_data[$key]['import_status']=0;
			$import_data[$key]['import_user']=$me->getUserID();
			$import_data[$key]['import_date']=date("Y-m-d H:i:s");
			$processed_data[$key]['import_status']['final_icon'] = "ok";
			$processed_data[$key]['import_status']['final_result'] = "Ok to import";
		}else {
			$status_count['rows']['error']++;
		}
		if($row_warning == true) {
			$status_count['rows']['ok']++;
			$import_data[$key]['import_key']=$key;
			$import_data[$key]['import_ref']=$import_ref;
			$import_data[$key]['import_results']=base64_encode(serialize($targets));
			$import_data[$key]['import_status']=0;
			$import_data[$key]['import_user']=$me->getUserID();
			$import_data[$key]['import_date']=date("Y-m-d H:i:s");
			$processed_data[$key]['import_status']['final_icon'] = "info";
			$processed_data[$key]['import_status']['final_result'] = "Ok to import";
		}


	}
}
if($has_data!==true) {
	ASSIST_HELPER::displayResult(array("error","The file appears to be empty.  Please try again.  Remember that the import process ignores the first 2 rows as they are assumed to be headings."));
	die();
}


/* If all okay then load to temp table to facilitate quick accept import step 4 */
if($status_count['rows']['error']==0) {
	foreach($import_data as $key => $row) {
		$sql = "INSERT INTO ".$me->getTableName()."_temp SET ".$me->convertArrayToSQLForSave($row);
		//echo "<P>".$sql."</p>";
		$me->db_insert($sql);
	}
}



/*
 * DATA VALIDATION during loop (DEVELOPMENT)

echo "<hr />";
 echo "end";
echo "<hr />";
ASSIST_HELPER::arrPrint($import_data);
echo "<hr />";
ASSIST_HELPER::arrPrint($processed_data);
echo "<hr />";
ASSIST_HELPER::arrPrint($status_count);
echo "<hr />";

 */
$colspan = 1;
$rowspan = 1;


?>
<table id=tbl_import style=''>
	<tr>
		<th rowspan='<?php echo $rowspan; ?>'>&nbsp;</th>
		<th rowspan='<?php echo $rowspan; ?>'>Row</th>
		<?php
		foreach($headings['sub'] as $fld => $head) {
			echo "<th rowspan=".$rowspan.">".$head['name']."</th>";
		}
				?>
		<th rowspan='<?php echo $rowspan; ?>'>Import Status</th>
	</tr>
	<?php


foreach($processed_data as $key => $row) {
	$final_icon = $row['import_status']['final_icon'];
	unset($row['import_status']['final_icon']);
		echo "
		<tr>
			<td><div class='div_message final_".$final_icon."'>".ASSIST_HELPER::getDisplayIconAsDiv($final_icon)."</div></td>
			<td class=center>".$row['row']."</td>";
	foreach($headings['sub'] as $fld => $head) {
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			";
	}

		echo "
			<td><div class='div_message final_".$final_icon."'>".implode("<br />",$row['import_status'])."</div></td>
		</tr>
		";
}
	?>
</table>
<div id=div_result style='width:500px;margin:10px auto;padding:10px;'>
	<p class='center b'>Import Result</p>
	<p>
		<?php
		if($status_count['rows']['error']>0) {
			echo "<p>There are ".$status_count['rows']['error']." rows in an error state".($status_count['rows']['warn']>0?" and ".$status_count['rows']['warn']." rows with warnings":"").".  The Import cannot be finalised.  Please correct the errors and try again.</p>
			<p class='b center'>WARNING: NO LINE ITEMS HAVE BEEN IMPORTED.";
			echo "<br>";
			ASSIST_HELPER::goBack();
		} else {
			if($status_count['rows']['warn']>0) {
				echo "<p>There are ".$status_count['rows']['warn']." rows with warnings.  The Import can be finalised however the warnings should be reviewed.</p>";
			} else {
				echo "<p class='center'>All ".$status_count['rows']['ok']." rows are ok to Import.</p>";
			}
			echo "
			<form name=frm_save method=post action=".$redirect_url.">
			<input type=hidden name=action value='STEP4' />
			<input type=hidden name=section value='$section' />
			<input type=hidden name=import_ref value='$import_ref' />
			<p class=center>
			<button id=btn_save>Accept Import</button>
			</p>
			<p class=center style='margin-top:10px'>
			<button id=btn_cancel>Cancel Import</button>
			</p>
						</form>
			";
		}
		?>
	</p>

</div>
<script type="text/javascript">
	$(function() {
		<?php
		if($status_count['rows']['error']>0) {
			echo "$(\"#div_result\").addClass(\"ui-state-error\");";
		} elseif($status_count['rows']['warn']>0) {
			echo "$(\"#div_result\").addClass(\"ui-state-info\");";
		} else {
			echo "$(\"#div_result\").addClass(\"ui-state-ok\").css(\"background\",\"#ffffff\");";
		}
		?>

		$("#tbl_import").css({"height":"100%"});
		$("#tbl_import td").css({"padding":"0px"});
		$("#tbl_import div.div_message").css({"margin":"1px","padding":"2px","height":"87%"});
		$("#tbl_import div.error").addClass("ui-state-error");
		$("#tbl_import div.warn").addClass("ui-state-info");
		$("#tbl_import div.final_ok").addClass("ui-state-ok");
		$("#tbl_import div.final_warn").addClass("ui-state-info");
		$("#tbl_import div.final_error").addClass("ui-state-error");

		$("#btn_save").button({
			icons:{primary:"ui-icon-check"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green")
		.css("margin","0 auto")
		.hover(function(){
			$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
		},function() {
			$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
		})
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			$("form[name=frm_save]").submit();
		});
		$("#btn_cancel").button({
			icons:{primary:"ui-icon-closethick"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
		.css("margin","0 auto")
		.hover(function(){
			$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
		},function() {
			$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
		})
		.click(function(e) {
			e.preventDefault();
			history.back();
		});
	});
</script>
<?php





//$all_vars = get_defined_vars();
//unset($all_vars['_SESSION']);
//unset($all_vars['_SERVER']);
//ASSIST_HELPER::arrPrint($all_vars);
?>