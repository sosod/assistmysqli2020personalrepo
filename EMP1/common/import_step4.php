<?php
/**
 * @var EMP1_EMPLOYEE $me - from import.php
 * @var int $max_rows_to_import - from import.php
 * @var string $section - from calling page
 * @var EMP1_HEADINGS $headingObject - from inc_header
 * @var array $headings - from import.php => array('rows'=>array());
 * @var string $redirect_url - from import.php
 */
//ASSIST_HELPER::arrPrint($_REQUEST);

//Get data from temporary table
$import_ref = $_REQUEST['import_ref'];
$sql = "SELECT * FROM ".$me->getTableName()."_temp WHERE import_ref = '".$import_ref."'";
$data = $me->mysql_fetch_all_by_id($sql,"import_key");



//setup variables
$copy_fields = array();
foreach($headings['sub'] as $fld => $head) {
	if($head['type']!="REF" && $head['type']!="ATTACH" && $head['type']!="USER_STATUS" && $head['type']!="EMP_STATUS" && $head['type']!="HEAD" && $head['field']!="job_end" && $head['field']!="Job_newcontract" ) {
		$copy_fields[] = $fld;
	}
}
//ASSIST_HELPER::arrPrint($copy_fields);
//process data
if(count($data)>0) {
	echo "<p class=b>Importing:<ul>";
	//loop through each row from temp table and save to live table
	foreach($data as $key => $row) {
		//add one to the key to match the row number in excel (starts at 1) where key comes from array key (starts at 0)
		echo "<li>Row ".($key+1)."...";
		//get data for new list item
		$import_data = array();

		if($me->getParentFieldName()!==false && strlen($me->getParentFieldName())>0) {
			//parent id - handled in step 3 instead
			$import_data[$me->getParentFieldName()] = $row[$me->getParentFieldName()];
		}
		foreach($copy_fields as $fld) {
			$import_data[$fld] = $row[$fld];
		}
		//process results
		if(strlen($row['import_results'])>5) {	//if there is something to decode
			$results = unserialize(base64_decode($row['import_results']));
			if(is_array($results) && count($results)>0) {
				foreach($results as $k => $r) {
					$import_data[$k] = $r;
				}
			}
		}
		$import_data['form_type'] = "ADD";
		$import_data['job_newcontract'] = 1;

		//send data to EMP1_*->addObject for processing
		$result = $me->addObject($import_data);
		if($result[0]=="ok") {
			echo " done.</li>";
		} else {
			echo "<span class=red>Error: ".$result[1]."</span></li>";
		}
	}//end foreach data as row
	echo "</ul>
	";
//update temp table to acknowledge successful import
$sql = "UPDATE ".$me->getTableName()."_temp SET import_status = 1 WHERE import_ref = '".$import_ref."'";
$me->db_update($sql);

	echo "
	<script type=text/javascript>
	$(function() {
		AssistHelper.processing();
		AssistHelper.finishedProcessingWithRedirect('ok','Import completed successfully','".$redirect_url."');
	});
	</script>";

} else {
	ASSIST_HELPER::displayResult(array("error","No data found to import - the holding table does not contain any data for reference: ".$import_ref.".  Please try again otherwise contact your Assist Administrator."));

}
?>