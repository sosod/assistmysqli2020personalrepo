<?php
//ASSIST_HELPER::ArrPrint($_SESSION);

/*****
 * Used for Add / Edit of employees
 *
 * REQUIRES:
 *    $is_edit_page = TRUE/FALSE
 * OR
 *    $is_add_page = TRUE/FALSE
 *    $convert_user = TRUE/FALSE
 *
 */
$cont_height = 500;
$doc_available = true;
$original_filename = array();
$js = "";
function drawField($section, $fld, $head) {
	global $user_details;
	global $full_user_details;
	global $can_enter_employee_details;
	global $displayObject;
	global $is_edit_page, $is_view_page, $is_add_page, $convert_user, $is_active_employee;
	global $emp_timekeep_field_map;
	global $helper;
	global $employee_user_changeover_timestamp;
	$js = "";
	if(($section == "USER" && !$can_enter_employee_details)) {
		echo isset($user_details[$fld]) ? $user_details[$fld] : "";
		if($fld == "tkname" || $fld == "tksurname") {
			echo "<input type=hidden name='$fld' value=\"".$user_details[$fld]."\"  />";
		}
	} else {
		$prop = array(
			'id' => $fld,
			'name' => $fld,
		);
		$prop['req'] = isset($head['required']) ? $head['required'] : false;
		switch($head['type']) {
			case "EMPLOYEE":
				$listObject = new EMP1_EMPLOYEE();
				$select_data = $listObject->getListOfEmployeesFormattedForSelect();
				$list_options = array('S' => $listObject->getSelfManagerOption());
				foreach($select_data as $key => $name) {
					$list_options[$key] = $name;
				}
				$prop['options'] = $list_options;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld]) && strlen($user_details[$emp_timekeep_field_map[$fld]]) > 0) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
				} else {
					$js .= $displayObject->drawFormField("LIST", $prop, $value);
				}
				if($fld == "job_manager") {
					if(!$is_edit_page && !$is_view_page && $convert_user && isset($user_details['tkadddate']) && $user_details['tkadddate'] < $employee_user_changeover_timestamp && isset($user_details['tkmanager_name']) && !is_null($user_details['tkmanager_name'])) {
						echo "<br />[Previous Manager Listed in Users: <span class=u>".$user_details['tkmanager_name']."</span>]";
					}
				}
				break;
			case "LIST":
				$listObject = new EMP1_LIST($head['list_table']);
				$list_options = $listObject->getActiveListItemsFormattedForSelect();
				$prop['options'] = $list_options;
				$prop['req'] = isset($head['required']) ? $head['required'] : false;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
				} else {
					$js .= $displayObject->drawFormField("LIST", $prop, $value);
				}
				break;
			case "MASTER":
				$class = "ASSIST_MASTER_".strtoupper($head['list_table']);
				$listObject = new $class();
				$list_options = $listObject->getActiveItemsFormattedForSelect();
				$prop['options'] = $list_options;
				$prop['req'] = isset($head['required']) ? $head['required'] : false;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
				} else {
					if($is_edit_page) {
						$val = $value;
						$value = "";
						foreach($list_options as $key => $v) {
							if(strtolower($v) == strtolower($val)) {
								$value = $key;
								break;
							}
						}
					}
					$js .= $displayObject->drawFormField("LIST", $prop, $value);
				}
				break;
			case "USER_STATUS":
				echo $user_details['tkstatus'];
				break;
			case "EMP_STATUS":
				echo $user_details['emp_status'];
				break;
			case "DATE":
				$prop['options'] = array();
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
					if(strlen($value) > 0 && $value != "0000-00-00") {
						$value = date("d-M-Y", strtotime($value));
					} else {
						$value = "";
					}
				} elseif($convert_user && (isset($emp_timekeep_field_map[$fld]) || isset($user_details[$fld]))) {
					$value = isset($emp_timekeep_field_map[$fld]) ? $user_details[$emp_timekeep_field_map[$fld]] : $user_details[$fld];
					if(ASSIST_HELPER::checkIntRef($value)) {
						$value = date("d-M-Y", $value);
					}
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("DATE", $value);
				} else {
					$js .= $displayObject->drawFormField($head['type'], $prop, $value);
				}
				break;
			case "ATTACH":
				$value = $head['default_value'];
				if($is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				}
				if($is_view_page || !$is_active_employee) {

					$js .= $displayObject->drawDataField($head['type'], $value);
				} else {
					$prop['allow_multiple'] = false;
					//soft code this field
					// $prop['action'] = $section.".".$act; //The value for action is passed on the (EDIT and ADD document) script.
					//Url where user must be redirected
					//$prop['page_direct'] = "manage_edit_object.php?object_id=".$full_user_details['emp_id']."&documentsort=doc_id&";
					$js .= $displayObject->drawFormField($head['type'], $prop, $value);

				}
				break;
			default:
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField($head['type'], $value);
				} else {
					$js .= $displayObject->drawFormField($head['type'], $prop, $value);
				}
				break;
		}
	}
	return $js;
}

if(!isset($_REQUEST['documentsort'])) {
	$_REQUEST['documentsort'] = "doc_id";
}
if(!isset($docSort)) {
	$docSort = $_REQUEST['documentsort'];
}

//IF convert_user IS NOT SET THEN ASSUME THAT YOU ARE ADDING A NON-USER OR EDITING AN EXISTING USER
if(!isset($convert_user)) {
	$convert_user = false;
}
if(!isset($is_view_page)) {
	$is_view_page = false;
}
//If is_edit_page is not set then assume you are adding a new employee
if(!isset($is_edit_page)) {
	if(!isset($is_add_page)) {
		$is_add_page = true;
		$is_edit_page = false;
	} elseif($is_add_page) {
		$is_edit_page = false;
	} else {
		$is_edit_page = true;
	}
} else {
	if($is_edit_page || $is_view_page) {
		$is_add_page = false;
	} else {
		$is_add_page = true;
	}
}

if($is_add_page && !isset($page_redirect)) {
	$page_redirect = "new.php";
}

/**
 * Set booleans to decide which buttons to display at the end of the form
 */
$display_terminate_button = false;
$display_convert_button = false;
$display_new_button = false;
$display_edit_button = false;
$edit_access = false;

/**
 * Get details to display in the form
 * Set the buttons
 */
$is_active_employee = true;
$full_user_details = array();
if($is_edit_page || $is_view_page) {
	if($is_edit_page) {
		$userObject = new EMP1_USERACCESS();
		$my_user_access = $userObject->getMyUserAccess();
		if($my_user_access['edit'] == true) {
			$display_edit_button = true;
		}
		if($my_user_access['edit_document'] == true) {
			$display_edit_button = true;
			$edit_access = true;
		}
		if($my_user_access['terminate'] == true) {
			$display_terminate_button = true;
		}
		$save_action = "EDIT";
		$form_type = "EDIT";
	} else {
		$save_action = "VIEW";
		$form_type = "VIEW";

	}
	$page_direct = "manage_edit.php?";
	if(!isset($object_id)) {
		$object_id = $_REQUEST['object_id'];
	}
	$full_user_details = $empObject->getRawEmployeeDetailsByEmployeeID($object_id, false);
	$user_id = $full_user_details[$empObject->getParentFieldName()];
	$user_details = $empObject->getFormattedOriginalUserDetails($user_id, $is_add_page);
	if($full_user_details['user_type'] == "active") {
		$is_active_user = true;
	} else {
		$is_active_user = false;
	}
	if(($full_user_details['emp_status'] & EMP1_EMPLOYEE::ACTIVE) == EMP1_EMPLOYEE::ACTIVE) {
		$is_active_employee = true;
	} else {
		$is_active_employee = false;
	}
	$user_details['emp_status'] = $empObject->getEmployeeStatusOptions($full_user_details['emp_status']);
	//echo ":::".$is_active_user.$full_user_details['user_type']."::::";
} elseif($convert_user) {
	$page_direct = "dialog";
	$save_action = "ADD";
	if($user_type == "active") {
		$form_type = "CONVERT";
		$page_redirect = "new.php?tab=new-convert-active&";
	} else {
		$page_redirect = "new.php?tab=new-convert-inactive&";
		$form_type = "CONVERT_INACTIVE";
	}
	$display_convert_button = true;
	$user_details = $empObject->getFormattedOriginalUserDetails($user_id, $is_add_page);
	$object_id = "";
} else {
	$save_action = "ADD";
	$page_direct = "new.php?tab=last&";
	$form_type = "ADD";
	$display_new_button = true;
	$object_id = "";
	$user_details = $empObject->getDefaultUserDetails();
	$user_id = "";
}
$emp_timekeep_field_map = $headingObject->getTimekeepFieldMap();
$employee_user_changeover_timestamp = $empObject->getModuleChangeoverTimestamp();

//$empObject->arrPrint($full_user_details);
//$empObject->arrPrint($user_details);
//$empObject->arrPrint($emp_timekeep_field_map);


/**
 * CHECK IF Employee Details must be input fields or displayed
 *
 * add page + convert user = display text from user_details (both for active & inactive users)
 * edit page + active user = display text from user_details
 * otherwise input fields (assume Add NEW or EDIT non-user employee)
 **/
if(($is_add_page && $convert_user) || ($is_edit_page && $is_active_user) || $is_view_page) {
	$can_enter_employee_details = false;
} else {
	$can_enter_employee_details = true;
}

//Get headings
$headings = array();
$headings['USER'] = $headingObject->getMainObjectHeadings("USER");
$headings['EMPLOYEE'] = $headingObject->getMainObjectHeadings("EMPLOYEE");
$headings['JOB'] = $headingObject->getMainObjectHeadings("JOB");
$headings['DOCUMENT'] = $headingObject->getMainObjectHeadings("DOCUMENT");
$headings['NOTES'] = $headingObject->getMainObjectHeadings("NOTES");

//Add SYSTEM REF field
$s = $headings['USER']['sub'][1];
$t = $s[0];
$t['id'] = 0;
$t['field'] = "tkid";
$t['name'] = "System Ref";
$t['type'] = "REF";
$t['required'] = 0;
$headings['USER']['sub'][1] = array(0 => $t);//+$s;
foreach($s as $r) {
	$headings['USER']['sub'][1][] = $r;
}



$sections = array("USER", "EMPLOYEE", "JOB", "DOCUMENT", "NOTES");

//AA-444 - Remove IAS limit to manager by name - now applicable to all clients for PM6
//$display_manager_by_name = false;
//if($empObject->isIASClient()) {
$display_manager_by_name = true;
//}


if($is_edit_page) {
	$page_redirect_self = "manage_edit_object.php?object_id=".$full_user_details['emp_id']."&documentsort=doc_id&";
}


//Prep for HR Integration
if($convert_user && $save_action == "ADD" && $empObject->isCompanyHRIntegrated() == true) {
	ASSIST_HELPER::displayResult(array("info", "Automatic HR Integration is active, users can not be converted, only linked to existing Employees added via the HR Integration"));

	/**
	 * Add matching function here to match active users with inactive user employees.
	 *    Matching can be pre-done by matching ID Number (tkidnum)
	 */
	/* ------------ remove these lines after completing this function ---------*/
	$emailObject = new ASSIST_EMAIL("faranath@gmail.com", "Alert: EMP1 Convert attempted", "EMP1 > New > Convert was attempted on HR Integrated client: ".$empObject->getCmpCode()." by ".$empObject->getUserName(), "TEXT", "janet@actionassist.co.za");
	$emailObject->sendEmail();
	echo "<p>To be programmed</p>";
	/* ------------ END: remove these lines after completing this function ---------*/

} else {


//If a manage page passes a remove_tab variable, it will hide the tabs
if($remove_tabs == false)
{ ?>

	<style>

        /*Splash screen(loading screen) when page load is not ready*/
        #hideAll {
            position: fixed;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            background-color: white;
            z-index: 99; /* Higher than anything else in the document */

        }

        /* Set height of body and the document to 100% to enable "full page tabs" */
        body, html {
            height: 100%;
            margin: 0;
            overflow-y: hidden;
            overflow-x: hidden;
        }

        .anchor {
            position: initial;
            width: 99.5%;
            background: linear-gradient(to top, #bbbbbb, #d1d1d1, #d1d1d1, #e2e2e2);
            border: 1px solid #9a9d9f;
            border-radius: 5px;
            padding: 1px 3px 0px;
            margin: 2px 0px;
            height: 30px;

        }

        ul {
            padding: 0.4rem 0.1rem; /* Puts the list on the anchor div */
        }

        /* Style tab links */
        .tablink {
            display: inline;

            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            min-width: 100px; /*Minim width for each tab button*/
            background-color: #efefef;
            color: #525557;
            border: 1px solid #bec9db;
            border-bottom-color: #9a9d9f;
            outline: #0a36e7;
            cursor: pointer;
            padding: 5px 10px;
            font-size: 12px;

            margin: 2px -1px;
            /* -webkit-box-shadow: #3f3f4f;
             box-shadow:
                     12px 12px 16px 0 rgba(0, 0, 0,0.2),
                     -8px -8px 12px 0 rgba(255, 255, 255, 0.3);*/


        }

        #div_container {
            border-radius: 3px;
            border: 1px solid #0000cc;


        }


        .tablink:hover {
            color: #c77405;
            background-color: #efefef;
            border: 1px solid #c77405;
            border-bottom-color: #9a9d9f;

        }

        #activetab {
            color: #0000cc;
            border: 1px solid #0000cc;
            border-bottom-color: #efefef;
        }

        /* Style the tab content (and add height:100% for full page content) */
        .tabcontent {
            color: white;
            display: none;
            padding: 40px 30px;
            /*height: auto;*/
            color: transparent;
            -webkit-animation: flash linear 0.3s;
            animation: flash linear 0.3s;

        }

        @-webkit-keyframes flash {
            0% {
                opacity: 0;
            }
            90% {
                opacity: 1;
                color: #fff;
            }

        }


        }


        #employee_details {
            background-color: -moz-default-background-color;
        }

        #Job_details {
            background-color: -moz-default-background-color;
        }

        #Documents {
            background-color: -moz-default-background-color;
        }

        #Notes {
            background-color: -moz-default-background-color;
        }

        #Consequence Management {
            background-color: -moz-default-background-color;
        }

        #PDP {
            background-color: -moz-default-background-color;
        }

        #Scorecard {
            background-color: -moz-default-background-color;
        }

        #Performance Dashboard {
            background-color: -moz-default-background-color;
        }
	</style>
	<div style="display: none" id="hideAll">
		<div style="margin: 1% 2%;"><?php ASSIST_HELPER::displayResult(array("info", "Page loading...")); ?></div>
	</div> <!--HIDES ALL THESE DIV'S WHEN PAGE IS NOT READY-->

	<div id=div_container>
		<!-- <div class="tab">-->
		<div class="anchor">
			<ul>

				<li class="tablink" onclick="openPage('employee_details', this, 'white')" id="defaultOpen"><span><?php echo $empObject->getObjectName("EMPLOYEE"); ?> Details</span></li>
				<li class="tablink" onclick="openPage('Job_details', this, 'white')"><span><?php echo $empObject->getObjectName("JOB"); ?> History</span></li>
				<li class="tablink" onclick="openPage('Documents', this, 'white')"><span>Documents</span></li>
				<li class="tablink" onclick="openPage('Notes', this, 'white')"><span>Notes</span></li>
				<li class="tablink" onclick="openPage('Performance Dashboard', this, 'white')"><span>Performance Dashboard</span></li>
				<li class="tablink" onclick="openPage('Scorecard', this, 'white')"><span>Scorecard</span></li>
				<li class="tablink" onclick="openPage('PDP', this, 'white')"><span>PDP</span></li>
				<li class="tablink" onclick="openPage('Consequence Management', this, 'white')"><span>Consequence Management</span></li>
			</ul>
		</div>

		<div class="content">
			<div id="employee_details" class="tabcontent"><!--======================================EMP Div=====================================================-->


				<?php
				} ?>

				<form name=frm_new class="form">
					<input type=hidden name=form_type id=form_type value='<?php echo $form_type; ?>'/>
					<input type=hidden name=original_tkid id=original_tkid value='<?php echo $user_id; ?>'/>
					<input type=hidden name=object_id id=object_id value='<?php echo $object_id; ?>'/>
					<?php
					if(!$display_manager_by_name) {
						echo "<input type=hidden name=job_manager id=job_manager value='' />";
					}
					?>
					<table id=tbl_container>
						<tr>
							<td>
								<?php if($is_edit_page) {
									echo "<h2 style='margin-top:0px'>Employee Details</h2>";
								} ?>
								<table class=form id=tbl_details>
									<?php
									//preparing fields to be added.
									$emp_details_sections = array("USER","EMPLOYEE");
									//remove tabs is set on the NEW page(new_tab_nonuser)
									if($remove_tabs == true){$emp_details_sections = array("USER","EMPLOYEE","JOB");} //if remove tabs is true, include JOB fields
									foreach($sections as $section) {
										if(in_array($section,$emp_details_sections)) {
											$heading_section = strtolower($section)."_heading";
											?>

											<!-- =======================Employee Details Header==========  <tr>

                                <th class=th-head><?php //echo $headings[$section]['rows'][$heading_section]['name']; ?></th>
                            </tr>=========================================-->
											<?php
											if(!isset($headings[$section]['rows'][$heading_section])) {
												$headings[$section]['rows'][$heading_section] = array();
											} //error mitigation
											$main_heading = $headings[$section]['rows'][$heading_section];
											if(!isset($main_heading['id'])) {
												$main_heading['id'] = array();
											}
											$main_H_id = $main_heading['id']; //causes error if not a variable
											if(is_array($headings[$section]['sub'][$main_H_id]) || is_object($headings[$section]['sub'][$main_H_id])) {
												foreach($headings[$section]['sub'][$main_heading['id']] as $id => $head) {
													$fld = $head['field'];
													if(($fld != "job_manager" || $display_manager_by_name) && $fld != "job_end") {
														echo "
			<tr id=tr_".$fld.">
				<th style='background-color:#000098; color: white;'>".$head['name'].":".($head['required'] == 1 ? "*" : "")."</th>
				<td class=user-td-details>";
														if($head['type'] != "HEAD") {
															$js .= drawField($section, $fld, $head);
														} else {
															$sub_heading = $head;
															echo "
					<table id=tbl_".$head['field']." class=noborder>";
															foreach($headings[$section]['sub'][$sub_heading['id']] as $sub_id => $sub_head) {
																echo "<tr><td class=b>".$sub_head['name'].":</td><td>";
																if(!$can_enter_employee_details || !$is_active_employee) {
																	echo $user_details[$sub_head['field']];
																} else {
																	$options = array('id' => $sub_head['field'],
																		'name' => $sub_head['field'],);
																	$js .= $displayObject->drawFormField($sub_head['type'], $options, ($is_edit_page ? $user_details[$sub_head['field']] : $sub_head['default_value']));
																}
																echo "</td></tr>";
															}
															echo "</table>";
														}
														echo "</td>
			</tr>";
													}
												}
											}
										}

									}


									?>


									<!--	<td rowspan=4 class=td-extra>REQUEST CHANGE OF DETAILS:<br />Only if active user<br />Sends notification to Any user with "Admin Super User" privileges & Support User<br /><textarea></textarea></td> -->


									<!--

									<tr>
										<th>Reason for Changing Job Details:</th>
										<td><select>
											<option value=NEW>Converting User to Employee (New)</option>
											<option value=EDIT>Existing details are incorrect (Edit)</option>
											<option value=JOB>Employee has changed Jobs within the Organisation (Update)</option>
											</select>
											<span class=dev-notes>IF EDIT: Existing details are incorrect (= update current details) OR Employee has changed jobs within the organisation (= mark current details as only applicable up to date below = impact individual performance)</span></td>
									</tr>
									-->
									<?php if(($display_convert_button || $display_new_button || $display_edit_button || $display_terminate_button)) { ?>
										<tr>
											<td colspan=3 class='td-buttons right'>
												<?php if($display_convert_button) { ?>
													<button id=btn_save_convert class=btn_save>Save
													New <?php echo $empObject->getObjectName("EMPLOYEE"); ?></button><?php } ?>
												<?php if($display_new_button) { ?>
													<button id=btn_save_new class=btn_save>Save
													New <?php echo $empObject->getObjectName("EMPLOYEE"); ?></button><?php } ?>
												<?php
												if($is_active_employee) {
													if($display_edit_button) { ?>
														<button id=btn_save_edit class=btn_save>
														Save <?php echo $empObject->getObjectName("EMPLOYEE"); ?>
														Changes</button><?php }
													if($display_terminate_button) { ?>
														<button id=btn_terminate class=btn_terminate>
															Terminate <?php echo $empObject->getObjectName("EMPLOYEE"); ?></button>
													<?php }
												} else {
													?>
													<button id=btn_save_edit class=btn_restore>
													Restore <?php echo $empObject->getObjectName("EMPLOYEE"); ?></button><?php
												} ?>
												<!-- <input type=checkbox />Terminate Assist User (this will lock the user and notify Assist Admin) -->
											</td>
										</tr>
									<?php } ?>


								</table>


				</form>

				<?php //commented out because when this and the Job log are both displayed it doesn't work on the test site, only 1 works at a time so i had to make a choice.
				if($is_edit_page || $is_view_page) {
					$lg = array();
					$lg['log_emp_id'] = $object_id;
					$lg['log_object_type'] = "EMPLOYEE";
					$js .= $displayObject->drawPageFooter("", $empObject->getMyLogTable(), $lg, $id);
				}
				?>


				</td>

				</tr>


				<!--//This is where the job info was taken-->
				</table>
				</form>

			</div><!--======================================end EMP Div=====================================================-->
			<!-- The end and the start of a new tab (tabcontent)-->


			<?php
			if($remove_tabs == false)
			{ ?>

			<div id="Job_details" class="tabcontent"><!--======================================Job Div=====================================================-->


				<?php
				}
				?>


				<table id=tbl_details class="table_Jobs">

					<!--<form name = frm_new style="border: none;" >-->

					<?php

					if($is_edit_page || $is_view_page) {
						$section = "JOB";
						$job_head = $headings[$section]['rows']['job_heading'];
						$job_headings = $headings[$section]['sub'][$job_head['id']];

						$show_button = true;
						echo "
	    
	    <tr >
	    
	    <td style=\"border: hidden;\">
	   
	    ".(!$is_view_page && $is_active_employee ? "<button id=btn_add class=float >Add</button>" : ($user_details['emp_status'] == 0 & !$is_view_page ? ASSIST_HELPER::displayResult(array("info", "Please note: Terminated Employee. | Cannot Edit nor Add a job.")) : " "))."
	<h2>".$job_head['name']."</h2>

	<table id=tbl_job class='list'>
    	
		<tr>
			<th>Ref</th>";
						foreach($job_headings as $h_id => $head) {
							$name = $head['name'];
							if(substr(strtolower($name), 0, 4) == "job ") {
								$name = substr_replace($name, "<br />", 3, 1);//(" ","<br />",$name,)
							} else {
								$pos = strpos($name, " ");
								if($pos !== false) {
									$name = substr_replace($name, "<br />", $pos, 1);
								}
							}
							echo "<th>".$name."</th>";
						}
						echo "
			".($show_button && !$is_view_page && $is_active_employee ? "<th></th>" : "")."
		</tr>";

						$jobObject = new EMP1_JOB();
						$jobs = $jobObject->getJobFullDetailByEmployeeID($full_user_details['emp_id']);

						//notes by JC - 16 August 2018
						//display warning if job dates overlap anywhere
						//COMPLETE ON 23 AUGUST [JC] - display warning if gap in dates (see trip logger app for an example on how to handle)
						$job_start_date = "";
						$job_end_date = "";
						foreach($jobs as $job_id => $job) {
							if($job['is_current_contract'] == true) {
								//don't check end date for current contract
							} else {
								//check if current end date is within 1 day of prev start date
								if($job['job_end'] != "0000-00-00") {            //required error trap for non-current jobs with no end date
									$job_end_date = $job['job_end'];
									$validate_end = date("Y-m-d", strtotime($job_end_date." 12:00:00") + (12 * 60 * 60));
									if($job_start_date != $validate_end) {
										echo "
				<tr>
					<td colspan='".(count($job_headings) + 2)."' class='missing-mileage'><div class=div-missing-mileage>".ASSIST_HELPER::getDisplayIconAsDiv("ui-icon-alert", "", "red")."<span class=b>Warning:</span> Missing dates not allocated to a job: ".date("d F Y", strtotime($validate_end))." - ".date("d F Y", strtotime($job_start_date))."</div></td>
				</tr>
				";
									}
								}
							}
							$job_start_date = $job['job_start'];
							if($job['is_active'] == true) {
								$tr_class = "";
								$button = "<button class=btn_edit job_id=".$job_id." is_current_contract=".$job['is_current_contract'].">Edit</button>";
							} else {
								$tr_class = "inactive";
								$button = "<button class=btn_restore job_id=".$job_id." is_current_contract=".$job['is_current_contract'].">Restore</button>";
							}
							echo "
	<tr>
		<td class='b center not-noborder'>".$job['ref']."</td>
	";
							foreach($job_headings as $h_id => $head) {
								$fld = $head['field'];
								echo "<td>";
								if(isset($job[$fld])) {
									switch($head['type']) {
										case "LIST":
											if(is_null($job[$fld])) {
												echo $jobObject->getUnspecified();
											} else {
												echo $job[$fld];
											}
											break;
										case "DATE":
											$v = $job[$fld];
											if(strlen($v) == 0 || $v == "0000-00-00" || date("d F Y", strtotime($v)) == "01 January 1970") {
												if($fld == "job_end") {
													echo "N/A";
												} else {
													echo $jobObject->getUnspecified();
												}
											} else {
												echo date("d F Y", strtotime($v));
											}
											break;
										default:
											echo str_replace(chr(10), "<br />", $job[$fld]);
											break;
									}
								} else {
									switch($head['type']) {
										case "LIST":
											echo $jobObject->getUnspecified();
											break;
										default:
											//echo $fld;
											break;
									}
								}
								echo "</td>";
							}
							echo ($show_button && !$is_view_page && $is_active_employee ? "<td>".$button."</td>" : "")."</tr>";
						}

						echo "
	</table>
	";
//ASSIST_HELPER::arrPrint($jobs);
//ASSIST_HELPER::arrPrint($job_headings);
						if($is_edit_page || $is_view_page) {
							$lg = array();
							$lg['log_emp_id'] = $object_id;
							$lg['log_object_type'] = "JOB ";
							$js .= $displayObject->drawPageFooter("", $jobObject->getMyLogTable(), $lg, $job_id);
						}

					}

					?>


					</td>

					</tr>

				</Table>


				<?php
				if($remove_tabs == false)
				{

				?>

			</div><!--ends the job tab--><!--======================================End Job Div=====================================================-->


			<!--====================================== Documents Div=====================================================-->
			<div class="tabcontent" id="Documents">

				<?php /*ASSIST_HELPER::displayResult(array("info", "Under Development"."<img src='common/construction-caution-svgrepo-com.svg' style='height: 30px ; width: 40px; float: right;'>")); */
				?>


				<table id=tbl_details class="table_document">

					<!--<form name = frm_new style="border: none;" >-->

					<?php
					// ASSIST_HELPER::arrPrint($_REQUEST);


					$DocumentObject = new EMP1_DOCUMENT();
					$docs = $DocumentObject->getDocFullDetailByEmployeeID($full_user_details['emp_id'], $docSort);
					$number_of_documents = count($docs);
					if($is_edit_page || $is_view_page) {
						$section = "DOCUMENT";
						$doc_head = $headings[$section]['rows']['doc_heading'];
						$doc_headings = $headings[$section]['sub'][$doc_head['id']];
						$show_button = true;
						if($is_edit_page) {
							$whichPage = "manage_edit_object.php";
						} else {
							$whichPage = "manage_view_object.php";
						}
						echo "
	    
	    <tr >
	    <form name=docsort action='.$whichPage.' method=post>
    <table cellpadding=5 cellspacing=0 width=550 class='dsort'>
        <tr>
            <td width=250 style=\"border - right: 0px;\">Sort by: <select id=sort><option value=doc_date>Document Date</option><option value=doc_type>Document Type</option><option value=doc_title>Document Title</option><option value=doc_id>Upload Order</option></select>
             <input type=button value=Go onclick=\"sortby();\">&nbsp;</td>
        </tr>
    </table>
    </form>
	    <td style=\"border: hidden;\">
	   
	   ".(!$is_view_page && $is_active_employee && $edit_access == true ? " " : (!$is_active_employee && !$is_view_page ? ASSIST_HELPER::displayResult(array("info", "Please note: Inactive or Terminated Employee. | Cannot Edit nor Add a Document.")) : (!$is_view_page && $edit_access == false && $is_active_employee ? ASSIST_HELPER::displayResult(array("info", "Please note: You do not have edit access. | Cannot Edit nor Add a Document.")) : " ")))."
<h2>".$doc_head['name']." </h2>

	<table id=tbl_document class='list'>
	
    	
		<tr>
		 
		
			<th>Ref</th>";
						foreach($doc_headings as $h_id => $head) {
							$name = $head['name'];
							if($name != "Document" && $name != "Document Expires") {
								if(substr(strtolower($name), 0, 8) == "document") {
									$name = substr_replace($name, "<br />", 8, 1);//(" ","<br />",$name,)
								} else {
									$pos = strpos($name, " ");
									if($pos !== false) {
										$name = substr_replace($name, "<br />", $pos, 1);
									}
								}
								echo "<th>".$name."</th>";
							}
						}
						echo "
			".($show_button && $is_active_employee && !empty($docs) ? "<th>".(!$is_view_page && $is_active_employee && $edit_access == true ? "<button id=btn_add_doc  class=float >Add</button>" : "")."</th>" : "")."
		
		</tr>";


						/*ASSIST_HELPER::arrPrint( $docs);
						ASSIST_HELPER::arrPrint( $doc_headings);*/
						if(empty($docs)) {
							echo "<tr>


<td style='min-width: 650px' colspan='6'>".$DocumentObject->getEmptyTableNotice()."
".(!$is_view_page && $is_active_employee && $edit_access == true ? "<button id=btn_add_doc  class=float >Add</button>" : "")."
</td><!--//Document Status-->
</tr>";
						} else {

							$attach_button = "";
							$number_of_empty_rows = 0;
							foreach($docs as $doc_id => $doc) {

								//this is to check if the attachment is not empty
								if($is_view_page) {
									$attach_button = $displayObject->getAttachForDisplay($doc['attachments'], false, 'DOCUMENT', $doc['doc_id'], true);
									//https://actionassist.myjetbrains.com/youtrack/issue/AA-406#focus=Comments-4-505.0-0
									if(empty($doc['attachments']) || $doc['attachments'] == "") {
										$doc_available = false;
									}
								} else {
									$original_filename_string = $displayObject->getAttachForDisplay($doc['attachments'], false, 'DOCUMENT', $doc['doc_id'], false);
									$attach_button = $original_filename_string;

								}
								//only display rows that have an attachment
								if(isset($doc['attachments']) && strlen($doc['attachments']) > 10 && !empty($attach_button)) {
									if($doc['is_active'] == true) {
										$tr_class = "";
										$button1 = "";
										$button2 = "";
										if($is_view_page) {
											$js .= $displayObject->getAttachmentDownloadJS('DOCUMENT', $doc['doc_id']);
											//$js .= $something_else['js'];
											//$button1 .= " <button class=btn_download_doc doc_id=".$doc_id." >Download</button>";
											$attach_button .= "<button class='btn_view_doc button_class'  doc_id=".$doc_id." >View</button>";
											$lg['log_object_id'] = $doc_id;
										} else {
											//$button1 = "<button class=btn_view doc_id=" . $doc_id . " >View</button>";
											$button1 = ($edit_access == true) ? "<button class=btn_edit_doc  doc_id=".$doc_id." >Edit</button>" : "-";
											$original_filename[$doc['doc_id']] = $original_filename_string;
											$lg['log_object_id'] = $doc_id;
											unset($attach_button);
											$js .= $displayObject->getAttachmentDownloadJS('DOCUMENT', $doc['doc_id']);

										}
									} else {
										$tr_class = "inactive";
										//$button = "<button class=btn_restore doc_id=".$job_id." is_current_contract=".$job['is_current_contract'].">Restore</button>";
										$button = "";
									}
									echo " <tr><td class='b center not-noborder' style='vertical-align: middle;'>".$doc['ref']."</td>";
									foreach($doc_headings as $h_id => $head) {
										$fld = $head['field'];
										if($fld != "attachments" && $fld != "doc_expires") {
											echo "<td style='vertical-align: middle;'>";
											if(isset($doc[$fld])) {
												switch($head['type']) {
													case "LIST":
														if(is_null($doc[$fld])) {
															echo $DocumentObject->getUnspecified();
														} else {
															echo ASSIST_HELPER::decode($doc[$fld]);
														}
														break;
													case "DATE":
														$v = $doc[$fld];
														if(strlen($v) == 0 || $v == "0000-00-00" || date("d F Y", strtotime($v)) == "01 January 1970") {
															if($fld == "doc_date") {
																echo "N/A";
															} else {
																echo $DocumentObject->getUnspecified();
															}
														} else {
															echo date("d F Y", strtotime($v));
														}
														break;
													case "DOC_STATUS":
														if(is_null($doc[$fld])) {
															echo $DocumentObject->getUnspecified();
														} else {
															date_default_timezone_set('Africa/Johannesburg');
															$date = strtotime(date('d F Y', time()));//today's date
															$exp = strtotime($doc['doc_expires']); //converts string to time so that you can compare the dates
															if($exp >= $date) {
																echo "<span style='color: #4d9622'><b> Active </b></span>";

															} else {
																if($doc['doc_expires'] == "0000-00-00" || $doc['doc_expires'] == "1970-01-01") {
																	echo "<span style='color: #4d9622'><b> Active </b></span>";
																} else {
																	echo "<span style='color: #900000'><b> Expired</b></span>";
																}
															}

														}
														break;
													default:
														echo str_replace(chr(10), "<br />", ASSIST_HELPER::decode($doc[$fld]));
														break;
												}
											} else {
												switch($head['type']) {
													case "LIST":
														echo $DocumentObject->getUnspecified();
														break;
													default:
														//echo $fld;
														break;
												}
											}

											echo "</td>";
										}
									}

									echo ($show_button && $is_active_employee ? "<td class='download_button_width' >".(isset($attach_button) ? $attach_button : '')." ".$button1."</td>" : "")."</tr>";

								} else {
									$number_of_empty_rows++;
								}

							}
							//if all documents have been rejected not to appear
							if($number_of_empty_rows == $number_of_documents) {
								echo "
								<tr>
								<td style='min-width: 650px' colspan='7'> You have ".$number_of_empty_rows." saved documents that have unreadable attachments.<br> Please inform your helpdesk for further assistance.<br>
								</td><!--//Document Status-->
								</tr>";
							}
						}

						echo "   
    
	</table>
	";
//ASSIST_HELPER::arrPrint($jobs);
//ASSIST_HELPER::arrPrint($job_headings);


					}

					?>


					</td>

					</tr>

				</Table>

			</div>
			<!--======================================End Documents=====================================================-->

			<!--====================================== Notes Div=====================================================-->
			<div class="tabcontent" id="Notes">
				<?php /*ASSIST_HELPER::displayResult(array("info", "Under Development"."<img src='common/construction-caution-svgrepo-com.svg' style='height: 30px ; width: 40px; float: right;'>")); */
				?>

				<?php
				// ASSIST_HELPER::arrPrint($_REQUEST);


				$noteObject = new EMP1_NOTE();
				$empId = $full_user_details['emp_id'];
				if($is_view_page) {
					$notes = $noteObject->getNoteFullDetailByEmployeeID($empId, true);
				} else {
					$notes = $noteObject->getNoteFullDetailByEmployeeID($empId, false);
				}
				$number_of_notes = count($notes);
				if($is_edit_page || $is_view_page) {
					$section = "NOTES";
					$note_head = $headings[$section]['rows']['note_heading'];
					$note_headings = $headings[$section]['sub'][$note_head['id']];
					$show_button = true;
					echo "
	    
	    <tr >
	    
	    <td style=\"border: hidden;\">
	   ".($is_edit_page ? '<div style="width: 100px;">'.ASSIST_HELPER::displayResult(array('info', 'Please note: Deleted notes are available to be restored before midnight and will be permanently deleted at midnight. ')).'</div>' : "")."
	   ".($is_view_page && $is_active_employee ? " " : (!$is_active_employee && !$is_view_page ? ASSIST_HELPER::displayResult(array("info", "Please note: Inactive or Terminated Employee. | Cannot Edit nor Add a Note.")) : " "))."
	   
	<h2>".$note_head['name']."</h2>

	<table id=tbl_note class='list'>
    	
		<tr>
			<!--<th>Ref</th>-->";
					foreach($note_headings as $h_id => $head) {
						$name = $head['name'];
						if($name != "Notes") {
							if(substr(strtolower($name), 0, 4) == "note") {
								$name = substr_replace($name, "<br />", 8, 1);//(" ","<br />",$name,)
							} else {
								$pos = strpos($name, " ");
								if($pos !== false) {
									$name = substr_replace($name, "<br />", $pos, 1);
								}
							}
							echo "<th>".$name."</th>";
						}
					}
					echo "
			".($show_button && $is_active_employee && !empty($notes) && $is_edit_page ? "<th>

 ".(!$is_view_page && $is_active_employee ? "<button id=btn_add_note  class=float >Add</button>" : "")."


</th>" : "")."
		</tr>";


					/*ASSIST_HELPER::arrPrint( $docs);
					ASSIST_HELPER::arrPrint( $doc_headings);*/
					if(empty($notes)) {
						echo "<tr>
<!--<td>DOC</td>-->
<!--//Notes added date-->
<td colspan='2' style='min-width: 450px'>".$noteObject->getEmptyTableNotice()."
".(!$is_view_page && $is_active_employee ? "<button id=btn_add_note  class=float >Add</button>" : "")."
</td><!--//Notes comment-->

</tr>";
					} else {

						$number_of_empty_note_rows = 0;
						foreach($notes as $note_id => $note) {
							date_default_timezone_set('Africa/Johannesburg');
							$deletetime = strtotime($note['note_deletetime']);
							$today = strtotime(date("Y-m-d H:i:s", time()));//today's date
							if($deletetime > $today || $note['note_deletetime'] == "NULL") {
								if(!isset($button2)) {
									$button2 = "";
								}
								if($note['note_status'] == EMP1::ACTIVE) {
									$tr_class = "";
									$button1 = "";
									$button2 = "";
									if($is_view_page) {
										/*$button2 = "<button class=btn_view_doc note_id=".$note_id." >View</button>";*/
										$button2 = " ";
									} else {
										//$button1 = "<button class=btn_view doc_id=" . $doc_id . " >View</button>";
										$button1 = "<button class=btn_edit_note note_id=".$note_id." >Edit</button>";

										$lg['log_object_id'] = $note_id;


									}
								} elseif($note['note_status'] !== EMP1::ACTIVE) {
									$tr_class = "";
									$button3 = "";
									//$button = "<button class=btn_restore doc_id=".$job_id." is_current_contract=".$job['is_current_contract'].">Restore</button>";

									if($is_view_page) {

										$button2 = " ";
									} else {

										$button1 = "<button id=btn_save_edit class=btn_restore_note note_id=".$note_id.">Restore Note</button>";


									}
								}
								//echo " <tr><td class='b center not-noborder'>".$note['ref']."</td>";
								foreach($note_headings as $h_id => $head) {
									$fld = $head['field'];
									if($fld != "note_heading") {
										echo "<td  style='max-width:600px;min-width:200px; height: auto; vertical-align: middle;' >";
										if(isset($note[$fld])) {
											switch($head['type']) {
												case "LIST":
													if(is_null($note[$fld])) {
														echo $noteObject->getUnspecified();
													} else {
														echo ASSIST_HELPER::decode($note[$fld]);
													}
													break;
												case "DATE":
												case "DATETIME":
													$v = $note[$fld];
													if(strlen($v) == 0 || $v == "0000-00-00" || date("d F Y", strtotime($v)) == "01 January 1970") {
														if($fld == "note_insertdate") {
															echo "N/A";
														} else {
															echo $noteObject->getUnspecified();
														}
													} else {
														echo date("d F Y H:i", strtotime($v))."</br>"." by ".$note["note_insertuser"];
													}
													break;
												default:
													echo str_replace(chr(10), "<br />", ASSIST_HELPER::decode($note[$fld]));
													break;
											}
										} else {
											switch($head['type']) {
												case "LIST":
													echo $noteObject->getUnspecified();
													break;
												default:
													//echo $fld;
													break;
											}
										}

										echo "</td>";
									}
								}
								echo ($show_button && $is_active_employee && $is_edit_page ? "<td class='download_button_width' style='vertical-align: 50%;'>".$button1." ".$button2."</td>" : " ")."</tr>";

							} else {
								$number_of_empty_note_rows++;
							}
						}
						if($number_of_empty_note_rows == $number_of_notes) {
							echo "
								<tr>
								<td style='min-width: 650px' colspan='3'>".$noteObject->getEmptyTableNotice()."
								</td><!--//Document Status-->
								</tr>";
						}
					}

					echo " 
	</table>
	";
//ASSIST_HELPER::arrPrint($jobs);
//ASSIST_HELPER::arrPrint($job_headings);


				}

				?>


				</td>

				</tr>

				</Table>
			</div>

			<!--======================================End Notes=====================================================-->

			<div class="tabcontent" id="Performance Dashboard">

				<?php ASSIST_HELPER::displayResult(array("info", "Under Development"."<img src='common/construction-caution-svgrepo-com.svg' style='height: 30px ; width: 40px; float: right;'>")); ?>


			</div>
			<!--======================================End Performance Dashboard=====================================================-->
			<div class="tabcontent" id="Scorecard">

				<?php ASSIST_HELPER::displayResult(array("info", "Under Development"."<img src='common/construction-caution-svgrepo-com.svg' style='height: 30px ; width: 40px; float: right;'>")); ?>


			</div>
			<!--======================================End Scorecard=====================================================-->

			<div class="tabcontent" id="PDP">

				<?php ASSIST_HELPER::displayResult(array("info", "Under Development"."<img src='common/construction-caution-svgrepo-com.svg' style='height: 30px ; width: 40px; float: right;'>")); ?>


			</div>
			<!--======================================End PDP=====================================================-->

			<div class="tabcontent" id="Consequence Management">

				<?php ASSIST_HELPER::displayResult(array("info", "Under Development"."<img src='common/construction-caution-svgrepo-com.svg' style='height: 30px ; width: 40px; float: right;'>")); ?>


			</div>
		</div>
		<!--======================================End Consequence Management=====================================================-->
		<!-- </div> // ends the tabs-->


	</div><!--//remove this div tag when adding new tabs (container)-->

	<?php
}
	?>

	<div style="display: none" id="hideAll"> <!--HIDES ALL THESE DIV'S WHEN PAGE IS NOT READY-->
		<?php
		if($is_view_page) {
			//ASSIST_HELPER::arrPrint($doc_headings);
			?>


			<div id=div_view_doc title="Add Document">
				<form name=form_view_doc method="post">
					<input type=hidden name=form_action_view id=form_action value='VIEW' class='no-check'/>
					<input type=hidden id=doc_id name=doc_id value='<?php if(ASSIST_HELPER::checkIntRef($doc_id) == true) {
						echo $doc_id;
					} else {
						$doc_id = 0;
						echo $doc_id;
					} ?>' class='no-check'/>
					<input type=hidden id=doc_empid name=doc_empid value='<?php echo $full_user_details['emp_id']; ?>' class='no-check'/>
					<table id=tbl_doc_view class=view>


						<thead>
						<tr id=tr_view_ref>
							<td colspan="4" style="background-color: white; ">
								<center><img src="" alt="" id="attachments" width="25px" height="25px"> <u><span id="doc_title" style="vertical-align: 50%; "></u></span>&nbsp; <span id="doc_size" style="vertical-align: 50%; "></span></center>
							</td> <!--//call span must be the number of headers coming in-->

						</tr>
						<tr>
							<td colspan="4" class="doc_desc"><b><span id="doc_content">Description:</span></b></td>

						</tr>

						</thead>
						<tbody>
						<tr>
							<td class="no-check">Document type:</td>
							<td id="doc_type"></td>
							<td class="no-check">Document Date:</td>
							<td id="doc_date"></td>
						</tr>
						<tr>
							<td class="no-check">Status:</td>
							<td id="doc_status"></td>
							<td class="no-check">Added on:</td>
							<td id="doc_insertdate"></td>
						</tr>
						<tr>
							<td class="no-check">Expires</td>
							<td id="doc_expires"></td>
							<td class="no-check">By:</td>
							<td id="doc_insertuser"></td>
						</tr>
						<tr>
							<td class="no-check"></td>
							<td class="no-check"></td>
							<td class="no-check">Modified on:</td>
							<td id="doc_modifieddate"></td>
						</tr>
						<tr>
							<td class="no-check"></td>
							<td class="no-check"></td>
							<td class="no-check">By:</td>
							<td id="doc_lastmoduser"></td>
						</tr>


						</tbody>

					</table>

					<button id="btn_doc_dialog_cancel">Close</button>

				</form>


			</div>


			<?php
		}
		?>


		<?php
		if($is_edit_page) {
			//ASSIST_HELPER::arrPrint($job_headings);
			if(!isset($note_id)) {
				$note_id = 0;
			}
			?>
			<div id=div_note title="Add Note">
				<form name=frm_edit_note method="post">
					<input type=hidden name=form_action id=form_action value='ADD' class='no-check'/>
					<input type=hidden id=note_id name=note_id value='<?php if(ASSIST_HELPER::checkIntRef($note_id) == true) {
						echo $note_id;
					} else {
						$note_id = 0;
						echo $note_id;
					} ?>' class='no-check'/>
					<input type=hidden id=note_empid name=note_empid value='<?php echo $full_user_details['emp_id']; ?>' class='no-check'/>
					<Table id=tbl_note_form class=form style="border: hidden">
						<!-- <tr id=tr_edit_ref>
							 <th>Ref:</th>
							 <td>#</td>
						 </tr>-->
						<?php
						foreach($note_headings as $h_id => $head) {
							if($head['field'] == "note_comment") {
								echo "
			    <tr id=tr_edit_".$head['field']." style='border: hidden;'>
				<!--<th style='background-color:#000098; color: white;'>".$head['name'].($head['required'] == true ? "*" : "").":</th>-->
				Comment:<td style='border: hidden;'>";
								$js .= drawField("NOTES", $head['field'], $head);
								echo "<td>
			    </tr><tr style='border: hidden;'><td style='border: hidden;'><span class='countdown'></span></td></tr>
			";
							}
						}
						?>
						<tr style='border: hidden;'>

							<td style='border: hidden;'>

								<button id=btn_save_note>Save</button>
								<button id=btn_delete_note>Delete</button>
								<button id=btn_note_dialog_cancel>Cancel</button>


							</td>

						</tr>
					</Table>


				</form>
				<div class="div_note_log">
					<?php
					$lg = array();
					$lg['log_emp_id'] = $object_id;
					$lg['log_object_type'] = "NOTES";

					$js .= $displayObject->drawPageFooter("", $noteObject->getMyLogTable(), $lg, $note_id);

					?>
				</div>
			</div>
			<?php
		}
		?>
		<?php
		if($is_edit_page) {
			//ASSIST_HELPER::arrPrint($doc_headings);
			if(!isset($doc_id)) {
				$doc_id = 0;
			}
			?>
			<div class="doc_cont">
				<div id=div_doc title="Add Document">

					<form name=frm_edit_doc language="jscript" enctype="multipart/form-data" method="post">
						<input type=hidden name="form_action" id="form_action" value='ADD' class='no-check'/>
						<input type=hidden name="page_direct" id="page_direct" value='dialog' class='no-check'/>
						<input type=hidden id=doc_id name=doc_id value='<?php if(ASSIST_HELPER::checkIntRef($doc_id) == true) {
							echo $doc_id;
						} else {
							$doc_id = 0;
							echo $doc_id;
						} ?>' class='no-check'/>
						<input type=hidden id=doc_empid name=doc_empid value='<?php echo $full_user_details['emp_id']; ?>' class='no-check'/>
						<Table id=tbl_doc_view class=form>
							<tr id=tr_edit_ref>
								<th>Ref:</th>
								<td>#</td>
							</tr>
							<?php
							foreach($doc_headings as $h_id => $head) {
								$name = $head['name'];
								if($name != "Document Status") {
									echo "
			<tr id=tr_edit_".$head['field'].">
				<th>".$head['name'].($head['required'] == true ? "*" : "").":</th>
				<td>";

									$js .= drawField("DOCUMENT", $head['field'], $head);
									echo "</td>
			</tr>
			";
								}
							}
							?>
							<tr>
								<th></th>
								<td>
									<button id=btn_save_doc>Save Changes</button>
									<button id=btn_delete_doc>Delete</button>
									<button id=btn_doc2_dialog_cancel>Cancel</button>

								</td>


							</tr>
						</Table>


					</form>
					<?php
					echo '<br><div class="div_view_doc_log" >';

					$lg = array();
					$lg['log_emp_id'] = $object_id;
					$lg['log_object_type'] = "DOCUMENT";

					$js .= $displayObject->drawPageFooter("", $docObject->getMyLogTable(), $lg, $doc_id);

					echo '</div>';

					?>

				</div>
			</div>

			<?php
		}
		?>
	</div>


	<?php
	if($is_edit_page) {
		//ASSIST_HELPER::arrPrint($job_headings);
		?>
		<div id=div_job title="Add Job">
			<form name=frm_edit_job method="post">
				<input type=hidden name=form_action id=form_action value='ADD' class='no-check'/>
				<input type=hidden id=job_id name=job_id value='<?php if(ASSIST_HELPER::checkIntRef($job_id) == true) {
					echo $job_id;
				} else {
					$job_id = 0;
					echo $job_id;
				} ?>' class='no-check'/>
				<input type=hidden id=job_empid name=job_empid value='<?php echo $full_user_details['emp_id']; ?>' class='no-check'/>
				<Table id=tbl_job_form class=form>
					<tr id=tr_edit_ref>
						<th>Ref:</th>
						<td>#</td>
					</tr>
					<?php
					foreach($job_headings as $h_id => $head) {
						echo "
			<tr id=tr_edit_".$head['field'].">
				<th style='background-color:#000098; color: white;'>".$head['name'].($head['required'] == true ? "*" : "").":</th>
				<td>";
						$js .= drawField("JOB", $head['field'], $head);
						echo "</td>
			</tr>
			";
					}
					?>
					<tr>
						<th></th>
						<td>
							<button id=btn_save_job>Save Changes</button>
							<button id=btn_delete_job>Delete</button>
						</td>
					</tr>
				</Table>
				<div id=div_warn>
					<?php
					ASSIST_HELPER::displayResult(array("warn",
						"Warning: Adding a new Job will automatically close the previous job, if the start date for the new contract falls after the start date of the previous contract, and register a new contract for this Employee.  This will impact the assessment of any related Scorecards in the Individual Performance module."));
					?>
				</div>

				<button id=btn_dialog_cancel>Cancel</button>
			</form>
		</div>
		<?php
	}
	?>
	<?php
	if($is_edit_page || !$is_active_employee) {

		?>
		<div id="dlg_job">
			<iframe id=ifr_job_form src="./job_form.php?object_id='<?php echo $object_id; ?>'" style="border:none; max-width: 700px; height: 600px; max-height:630px" title="Add Job"></iframe>

		</div>
		<button id="restoreEmp" style="visibility: hidden"></button> <!--//button triggered from add_job_form.php-->
		<button id="priorJobRestoreEmp" style="visibility: hidden"></button><!--//button triggered from add_job_form.php-->
		<?php
	}
	?>


	<!--------------------------------Start of the tab script------------------------------------------->

	<?php
	if($remove_tabs == false) {
		?>
		<script>
			document.getElementById("hideAll").style.display = "block"; <!--HIDES ALL  DIV'S WHEN PAGE IS NOT READY (FULLY LOADED)-->

			window.onload = function () {
				document.getElementById("hideAll").style.display = "none";<!--SHOWS ALL HIDDEN DIV'S WHEN PAGE IS READY (FULLY LOADED)-->
			}

			$("#div_doc").css({"display": "none"});
			$("#div_note").css({"display": "none"});
			// $("#div_doc").css({"visibility":"hidden"});
			// $("#div_note").css({"visibility":"hidden"});

			// Get the element with id="defaultOpen" and click on it
			//Do not break this fragile code please!
			var pge = localStorage.getItem("openTab");
			if (pge != null || pge != " ") {
				document.getElementById("defaultOpen").click();

				var ment = document.getElementById("defaultOpen")
				ment.removeAttribute("id");
				var Elem = document.getElementsByTagName("li");
				for (var i = 0; i < Elem.length; i++) {
					if (Elem[i].textContent == pge) {
						Elem[i].setAttribute("id", "defaultOpen");
						document.getElementById("defaultOpen").click();
						break;
					}
				}

			} else {
				document.getElementById("defaultOpen").click();
			}


			function openPage(pageName, elmnt, color) {
				// Hide all elements with class="tabcontent" by default */
				let i, tabcontent, tablinks;
				tabcontent = document.getElementsByClassName("tabcontent");
				for (i = 0; i < tabcontent.length; i++) {
					tabcontent[i].style.display = "none";
				}

				// Remove the background color of all tablinks/buttons
				tablinks = document.getElementsByClassName("tablink");
				for (i = 0; i < tablinks.length; i++) {
					tablinks[i].style.backgroundColor = "";
					tablinks[i].style.color = "";
					tablinks[i].style.borderColor = "";
					tablinks[i].style.borderBottomColor = "";
				}
				//stores locally the pressed tab
				var tempPage;
				if (typeof (Storage) !== "undefined") {
					// Store
					localStorage.setItem("openTab", pageName);
					// Retrieve
					tempPage = localStorage.getItem("openTab");
				} else {
					tempPage = pageName;
				}
				// Show the specific tab content

				document.getElementById(tempPage).style.display = "block";

				// Add the specific color to the button used to open the tab content

				elmnt.style.backgroundColor = color;
				elmnt.style.color = "#0000cc";
				elmnt.style.borderColor = "#0000cc";
				elmnt.style.borderBottomColor = "white";

				var offset = $("#div_container").offset();
				$(window).resize(function () {
					// var state = $("#btn_hide_headings").attr("state");
					var scr = AssistHelper.getWindowSize();
					//if(state=="hide") {
					// var space_available_for_tabs = scr.height-5;
					// var tab_height = space_available_for_tabs<350 ? 350 : (space_available_for_tabs-10);
					// } else {
					var space_available_for_tabs = scr.height - offset.top;
					var tab_height = space_available_for_tabs < 350 ? 350 + Math.floor(offset.top) : (space_available_for_tabs - 10);
					//  }
					//$("#content").css({"height":"300px"});

					$("#div_container").css("height", tab_height + "px");
					$(".content").css({"height": (tab_height - 40) + "px", "overflow": "auto"});


				});
				$(window).trigger("resize");


			}

			//Document sorter
			function sortby() {
				var s = document.getElementById('sort').value;
				var editPage = "<?php echo $is_edit_page; ?>";
				var obj = "<?php echo $object_id?>";
				if (editPage == true) {
					document.location.href = "manage_edit_object.php?object_id=" + obj + "&documentsort=" + s;
				} else {
					document.location.href = "manage_view_object.php?object_id=" + obj + "&documentsort=" + s;
				}
			}


		</script>
		<?php
	}
	?>


	<!-------------------------end of the tab script------------------------------------------>


	<!----------------------END OF Job form-------------------------->
	<!----------------------SCRIPT-------------------------->

	<script type="text/javascript">


		document.onreadystatechange = function () {

			if (document.readyState == "complete") {

				$(function () {
					<?php echo $js; ?>
					//table formatting

					$("table.form").find("th:not(.th-head)").addClass("thsub2");
					$("table.form").find("th.th-head").prop("colspan", 2);
					$("#tbl_job_form th").css({"background-color": "#000098", "color": "white"});
					//$("table.form").find("td:not(.td-user-details):not(.td-buttons)").prop("colspan",2);
					$("#tbl_doc_view th").css({"background-color": "#000098", "color": "white"});
					$("#tbl_details").width($("#tbl_details").width() + 100);
					$("#tbl_user_contact, #tbl_user_contact td").css("border", "0px solid #ffffff");

					$(".doc_cont").css({"width": "23342 px"});
					let v = "<?php echo $is_view_page; ?>";
					if (v == true) {
						$(".download_button_width").css({"min-width": "350px", "vertical-align": "middle", "display": "flex", "border-bottom": "hidden", "border-left": "hidden", "border-right": "hidden"});
					}


					//note textarea character counter
					/* $('#note_comment').attr("maxlength", "255");

					 function updateCountdown() {
						 // 255 is the max message length
						 var remaining = 255 - jQuery('#note_comment').val().length;
						 jQuery('.countdown').text(remaining + ' characters remaining.');
					 }

					 jQuery(document).ready(function ($) {
						 updateCountdown();
						 $('#note_comment').change(updateCountdown);
						 $('#note_comment').keyup(updateCountdown);
					 });*/
					//dialog to display add form
					$("#dlg_job").dialog({
						autoOpen: false,
						modal: true
					});
					$("#dlg_job").dialog("close");
					resizeDialog();

					function resizeDialog() {

						var windowSize = getWindowSize(); //console.log(windowSize);
						var dialogWidth = windowSize['width'] < 1000 ? windowSize['width'] - 50 : windowSize['width'] - 150;
						var dialogHeight = windowSize['height'] < 1000 ? windowSize['height'] - 50 : windowSize['height'] - 150;

						//dialog to display add form
						$("#dlg_job").dialog("option", "width", dialogWidth);
						$("#dlg_job").dialog("option", "height", dialogHeight);
						$("#ifr_job_form").css("width", (dialogWidth - 20) + "px").css("height", (dialogHeight - 50) + "px");

					}

					$(window).resize(function () {

						resizeDialog();
					});

					function openUpdateDialog() {
						$(function () {
							AssistHelper.closeProcessing();
							$("#dlg_job").dialog("option", "title", "Add Job");
							$("#dlg_job").dialog("open");
							$("#dlg_job").dialog({
								width: 730
							});
						});
					}


					$(".btn_save")
						.button()
						.removeClass("ui-button-default").addClass("ui-button-bold-green")
						.hover(function () {
							$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
						}, function () {
							$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
						})
						.click(function (e) {
							e.preventDefault();
							$form = $("form[name=frm_new]");
							//LIVE CODE - uncomment after development
							var page_action = "<?php echo "EMPLOYEE.".$save_action; ?>";
							var page_direct = "<?php echo $page_direct; ?>";
							EMP1Helper.processObjectForm($form, page_action, page_direct);

							//DEV code - needed to test controller called functions
							//WARNING: No form validation is taking place!
							//$form.prop("method","POST");
							//$form.prop("action","form_process.php");
							//$form.submit();
						});
					$(".btn_restore")
						.button()
						.removeClass("ui-button-default").addClass("ui-button-bold-green")
						.hover(function () {
							$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
						}, function () {
							$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
						})
						.click(function (e) {
							e.preventDefault();

							$("#div_job form").hide();
							$("#div_job").append("<div id='msg_restore'><b>Do you want to add a new job or restore the prior job ?</b></div>");
							$("#div_job").dialog("option", "title", "Restore Employee");
							$("#div_job").dialog("open");
							$("#div_job").dialog({
								width: 250,
								height: 150,
								buttons: {
									'Add New Job': function () {
										//do something
										//$(this).dialog('close');
										openUpdateDialog();
									},
									'Restore job': function () {
										//$(this).dialog('close');
										$('#priorJobRestoreEmp').click();
									}
								}, close: function () {
									var objectId = "<?php echo $object_id;?>";
									window.location.href = "manage_edit_object.php?object_id=" + objectId + "&";
								}
							});


							//DEV code - needed to test controller called functions
							//WARNING: No form validation is taking place!
							//$form.prop("method","POST");
							//$form.prop("action","form_process.php");
							//$form.submit();
						});
//buttons triggered from the external job form script[add_job_form.php]
					$("#restoreEmp").button()
						.removeClass("ui-button-default").addClass("ui-button-bold-green")
						.hover(function () {
							$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
						}, function () {
							$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
						})
						.click(function (e) {
							e.preventDefault();
							restoreEmp();
						});
//buttons triggered from the external job form script [add_job_form.php]
					$("#priorJobRestoreEmp").button()
						.removeClass("ui-button-default").addClass("ui-button-bold-green")
						.hover(function () {
							$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
						}, function () {
							$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
						})
						.click(function (e) {
							e.preventDefault();
							priorJobrestoreEmp();
						});

//This function restores employee when triggered.
					function restoreEmp() {
						$(function () {
							$form = $("form[name=frm_new]");
							//LIVE CODE - uncomment after development
							var page_action = "EMPLOYEE.RESTORE";
							var page_direct = "<?php echo $page_direct; ?>";
							EMP1Helper.processObjectForm($form, page_action, page_direct);
						})
					}

//This function restarts contract and restores employee when triggered.
					function priorJobrestoreEmp() {
						$(function () {
							$form = $("form[name=frm_new]");
							//LIVE CODE - uncomment after development
							var page_action = "EMPLOYEE.RESTORE&restartcontract=true";
							var page_direct = "<?php echo $page_direct; ?>";
							EMP1Helper.processObjectForm($form, page_action, page_direct);
						})
					}

					$(".btn_terminate")
						.button({
							icons: {primary: "ui-icon-trash"}
						})
						.addClass("ui-button-minor-default")
						.hover(function () {
							$(this).addClass("ui-button-minor-red").removeClass("ui-button-minor-default")
						}, function () {
							$(this).removeClass("ui-button-minor-red").addClass("ui-button-minor-default")
						})
						.click(function (e) {
							e.preventDefault();
							if (confirm("Are you sure you wish to terminate this employee?")) {
								$form = $("form[name=frm_new]");
								//LIVE CODE - uncomment after development
								var page_action = "<?php echo "EMPLOYEE.TERMINATE"; ?>";
								var page_direct = "<?php echo $page_direct; ?>";
								EMP1Helper.processObjectForm($form, page_action, page_direct);

								//DEV code - needed to test controller called functions
								//WARNING: No form validation is taking place!
								//$form.prop("method","POST");
								//$form.prop("action","form_process.php");
								//$form.submit();
							}
						});


					//hide container table & first level cells
					$("#tbl_container").addClass("noborder").find("tr").each(function () {
						$(this).find("td:first:not(.not-noborder)").addClass("noborder");
					});
					//edit page buttons
					$("#btn_add").button({
						icons: {primary: "ui-icon-plus"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						$("#btn_delete_job").hide();
						$("#div_warn").show();
						$("#div_job #form_action").val("ADD");
						$("#div_job #job_id").val("0");
						$("#div_job #tr_edit_ref td:first").html("#");
						$("#div_job").find("input, textarea").not(".no-check").each(function () {
							$(this).val("");
							$(this).removeClass("required");
						});
						$("#div_job").find("select").not(".no-check").each(function () {
							$(this).val($(this).find("option:first").val());
							$(this).removeClass("required");
						});
						$("#tr_edit_job_end").hide();
						AssistHelper.closeProcessing();
						$("#div_job").dialog("option", "title", "Add Job");
						$("#div_job").dialog("open");
					}).removeClass("ui-state-default").addClass("ui-button-bold-green").css({
						"position": "relative",
						"bottom": "-20px"
					});
//add note
					let TopUp1 = "<?php echo(empty($notes) ? "-60px" : "-40px") ?>"; //DO NOT REMOVE, THIS HELPS ALIGN THE ADD BUTTON
					$("#btn_add_note").button({
						icons: {primary: "ui-icon-plus"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						$(".div_note_log").hide();
						$("#div_note").show();
						$("#btn_delete_note").hide();
						$("#div_warn").hide();
						$("#div_note #form_action").val("ADD");
						$("#div_note #note_id").val("0");

						$("#div_note").find("textarea").not(".no-check").each(function () {
							$(this).val("");
							$(this).removeClass("required");
						});
						$("#div_note").dialog({
							// autoOpen : false, modal : true, show : "blind", hide : "blind"
							modal: true, closeOnEscape: false,
							width: "auto"

						});
						AssistHelper.closeProcessing();
						$("#div_note").dialog("option", "title", "Add Note");
						$("#div_note").dialog("open");
					}).removeClass("ui-state-default").addClass("ui-button-bold-green").css({
						"position": "relative",
						"top": TopUp1
					});

//add document
					let TopUp2 = "<?php echo(empty($docs) ? "-95px" : "-40px") ?>"; //DO NOT REMOVE, THIS HELPS ALIGN THE ADD BUTTON
					$("#btn_add_doc").button({
						icons: {primary: "ui-icon-plus"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						//$("#div_doc #tr_edit_doc_type td").find("select").attr("disabled", false);
						$(".div_view_doc_log").hide();
						$("#btn_delete_doc").hide();
						$("#div_doc #tr_edit_attachments td").find("input:file").show();//.attr("disabled", false); //shows row hidden by btn_edit_doc
						$("#div_doc #tr_edit_attachments td").find(".flename").hide();
						$("#div_doc #form_action").val("ADD");
						$("#div_doc #doc_id").val("0");
						$("#div_doc #action").val("DOCUMENT.ADD");//This value is passed to the drawField function as activity
						$("#div_doc #tr_edit_ref td:first").html("#");
						$("#div_doc").find("input:text, textarea").not(".no-check").each(function () {
							$(this).val("");
							$(this).removeClass("required");
						});
						$("#div_doc").find("select").not(".no-check").each(function () {
							$(this).val($(this).find("option:first").val());
							$(this).removeClass("required");
						});
						$("#div_doc").dialog({
							// autoOpen : false, modal : true, show : "blind", hide : "blind"
							modal: true, closeOnEscape: false,
							width: "auto"
						});
						AssistHelper.closeProcessing();
						$("#div_doc").dialog("option", "title", "Add Document");
						$("#div_doc").dialog("open");
					}).removeClass("ui-state-default").addClass("ui-button-bold-green").css({
						"position": "relative",
						"top": TopUp2
					});

					$("#div_view_doc").hide();
					/*var doctble = $("#tbl_document th").width();*/
//DOCUMENT TABLE MAX WIDTH
					$("#tbl_document").css({"max-width": "1300px"});

					$(".doc_desc").css({"background-color": "#F1F1F1"});
					$("#tbl_doc_view").css({"width": "100%"}); //broadens the view document table
//view document

					$(".btn_view_doc").button({
						icons: {primary: "ui-icon-newwin"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();//doesn't wanna work for some reason
						var dta = "obj_id=" + $(this).attr("doc_id");
						var doc = AssistHelper.doAjax("inc_controller.php?action=DOCUMENT.getRawObject", dta);


						// this initializes the dialog (and uses some common options that I do)
						var w = $("#tbl_doc_view").width() + 500;
						$("#div_view_doc").dialog({
							// autoOpen : false, modal : true, show : "blind", hide : "blind"
							modal: true, closeOnEscape: false,
							width: w + "px"

						});
						$("#div_view_doc").find("img").not(".no-check").attr("src", "");
						$("#tbl_doc_view td").css({"border": "0px solid #ffffff"});
						$("#div_view_doc #form_action_view").val("VIEW");
						let divEle = "<?php $doc_id = isset($doc_id) ? $doc_id : 0; echo $doc_id;?>";
						let noteLogState = $("#" + divEle + "_div_audit_log").attr('state');
						if (noteLogState == "show") {
							$(".div_view_doc_log").css({"width": "auto", "height": "auto"});//Don't remove ,defaults the width and height of the log footer
						}

						$("#div_view_doc #doc_id").val($(this).attr("doc_id"));
						$("#div_view_doc").find(" td, th ,span ,img").not(".no-check").each(function () {

							var i = $(this).prop("id");
							$(this).html(doc[i]);
							if (i == "doc_content") {
								if (doc[i] !== null && doc[i] !== '') {
									$(this).html(AssistString.decode(doc[i]))
								}
							}
							$(this).removeClass("required");

							if ($("#div_view_doc").find("img").not(".no-check")) {//if statement is to exclude the footer DisplayActivityLog
								$(this).prop("src", doc['attachments']);
								$(this).prop("alt", doc['attachments']);
							}


						});

						var i = 0;
						var dta = 'activity="VIEW"&object_id="' + $(this).attr("doc_id") + '"&i=' + i;
						var fsize = AssistHelper.doAjax("inc_controller.php?action=DOCUMENT.GET_ATTACH_SIZE", dta); //gets file size of selected document
						var string = fsize.includes("error");
						if (string) {
							fsize = "There is a problem reading the document, Please contact your administrator.";
						}
						if ($("#div_view_doc").find("span").not(".no-check").each(function () {
								var i = $(this).prop("id");
								if (i == "doc_size") {
									$(this).html("(" + fsize + ")");
								}

							}
						)) ;

						AssistHelper.closeProcessing();
						$("#div_view_doc").dialog("option", "title", "View Document");
						$("#div_view_doc").dialog("open");
					}).removeClass("ui-state-default").addClass("ui-state-default").css({
						"position": "relative",
						"float": "right"
					});


//job edit
					$(".btn_edit").button({
						icons: {primary: "ui-icon-pencil"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						var dta = "obj_id=" + $(this).attr("job_id");
						var job = AssistHelper.doAjax("inc_controller.php?action=JOB.getRawObject", dta);
						var is_current_contract = $(this).attr("is_current_contract");

						$("#div_job #tr_edit_ref td:first").html($(this).closest("tr").find("td:first").html());
						if (is_current_contract == false) {
							$("#btn_delete_job").show();
							$("#tr_edit_job_end").show();
						} else {
							$("#btn_delete_job").hide();
							$("#tr_edit_job_end").hide();
						}
						$("#div_warn").hide();
						$("#div_job #job_id").val($(this).attr("job_id"));
						$("#div_job").find("input, select, textarea").not(".no-check").each(function () {
							var i = $(this).prop("id");
							$(this).val(job[i]);
							$(this).removeClass("required");
						});
						$("#div_job #form_action").val("EDIT");
						AssistHelper.closeProcessing();
						$("#div_job").dialog("option", "title", "Edit Job");
						$("#div_job").dialog("open");
					}).removeClass("ui-state-default").addClass("ui-button-minor-grey");


//edit document
					$(".btn_edit_doc").button({
						icons: {primary: "ui-icon-pencil"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						// $("#div_doc").css({"display":"block"});
						var fileName = <?php echo json_encode($original_filename); ?>;
						var dta = "obj_id=" + $(this).attr("doc_id");
						var id = $(this).attr("doc_id");
						var doc = AssistHelper.doAjax("inc_controller.php?action=DOCUMENT.getRawObject", dta);

						$("#div_doc").dialog({
							// autoOpen : false, modal : true, show : "blind", hide : "blind"
							modal: true, closeOnEscape: false,
							width: "auto",

						});
						$(".div_view_doc_log").show();//shows the log when when edit is pressed. Hides it when add doc is clicked.

						let access = "<?php echo $edit_access; ?>";
						if (access == true) {
							$("#div_doc #btn_delete_doc").show();
						}

						$("#div_doc #tr_edit_ref td:first").html($(this).closest("tr").find("td:first").html());

						$("#div_doc #doc_id").val($(this).attr("doc_id"));
						$("#div_doc #form_action").val("EDIT");
						$("#div_doc #action").val("DOCUMENT.EDIT"); //This value add the prop[action],
						$("#div_doc #tr_edit_attachments td").find("input:file").hide();//.attr("disabled", true); //user cannnot edit file but can edit other fields
						$("#div_doc #tr_edit_attachments td").find('.flename').show();
						$("#div_doc #tr_edit_attachments td").find('.flename').remove();
						$("#div_doc #tr_edit_attachments td").append("<td class=flename>" + fileName[id] + "</td>")							// $("#div_doc #tr_edit_doc_type td").find("select").hide();

						$("#div_doc").dialog("option", "title", "Edit Document");
						$("#div_doc").dialog("open");
						AssistHelper.closeProcessing();
						$("#div_doc").find("input, select, textarea").not(".no-check").each(function () {
							var i = $(this).prop("id");
							if (i == "doc_type") {
								i = "doc_typeNum"
							}
							$(this).val(doc[i]);
							if (i == "doc_content" || i == "doc_title") {
								if (doc[i] !== null && doc[i] !== '') {
									$(this).val(AssistString.decode(doc[i]))
								}
							}
							$(this).removeClass("required");
						});


					}).removeClass("ui-state-default").addClass("ui-button-minor-grey");
//edit note
					$(".btn_edit_note").button({
						icons: {primary: "ui-icon-pencil"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						//$("#div_note").css({"display":"block"});
						$(".div_note_log").show();
						$("#btn_delete_note").show();
						var dta = "obj_id=" + $(this).attr("note_id");
						var note = AssistHelper.doAjax("inc_controller.php?action=NOTE.getRawObject", dta);
						if (note['note_comment']) {
							note['note_comment'] = AssistString.decode(note['note_comment'])
						}
						//var note = AssistHelper.decode(result);
						//var w = $("#tbl_note_form").width() + 100;

						$("#div_note").dialog({
							// autoOpen : false, modal : true, show : "blind", hide : "blind"
							modal: true, closeOnEscape: false,
							width: "auto",


						});


						//$("#div_note #tr_edit_ref td:first").html($(this).closest("tr").find("td:first").html());

						let divEl = "<?php $note_id = isset($note_id) ? $note_id : 0;  echo $note_id;?>";
						let noteLogState = $("#" + divEl + "_div_audit_log").attr('state');
						if (noteLogState == "show") {
							$(".div_note_log").css({"width": "auto", "height": "auto"});//Don't remove ,defaults the width and height of the log footer
						}
						$("#div_warn").hide();
						$("#div_note #note_id").val($(this).attr("note_id"));
						$("#div_note").find("textarea").not(".no-check").each(function () {
							var i = $(this).prop("id");
							$(this).val(AssistString.decode(note[i]));
							$(this).removeClass("required");
						});
						$("#div_note #form_action").val("EDIT");
						AssistHelper.closeProcessing();
						$("#div_note").dialog("option", "title", "Edit Note");
						$("#div_note").dialog("open");
					}).removeClass("ui-state-default").addClass("ui-button-minor-grey");


//restore note
					$(".btn_restore_note")
						.button()
						.removeClass("ui-button-default").addClass("ui-button-bold-green")
						.hover(function () {
							$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
						}, function () {
							$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
						})
						.click(function (e) {
							e.preventDefault();
							AssistHelper.processing();
							let empId = "<?php $full_user_details['emp_id'] = isset($full_user_details['emp_id']) ? $full_user_details['emp_id'] : 0; echo $full_user_details['emp_id']; ?>";
							var dta = "note_id_from_button=" + $(this).attr("note_id")+ "&note_empid="+empId;
							var result = AssistHelper.doAjax("inc_controller.php?action=NOTE.RESTORE", dta);
							//reload the page

							var url = "<?php echo isset($page_redirect_self) ? $page_redirect_self : $page_redirect; ?>";

							if (typeof (result[0]) !== "undefined") {
								AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
							}
							//DEV code - needed to test controller called functions
							//WARNING: No form validation is taking place!
							//$form.prop("method","POST");
							//$form.prop("action","form_process.php");
							//$form.submit();
						});
//cancel job
					$("#btn_dialog_cancel").button({
						icons: {primary: "ui-icon-closethick"}
					}).click(function (e) {
						e.preventDefault();
						$("#div_job").dialog("close");
					}).removeClass("ui-state-default").addClass("ui-button-minor-grey").addClass("float").css("margin-top", "10px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
						});
					//cancel note
					$("#btn_note_dialog_cancel").button({
						icons: {primary: "ui-icon-closethick"}
					}).click(function (e) {
						e.preventDefault();
						$("#div_note").dialog("close");
					}).removeClass("ui-state-default").addClass("ui-button-minor-grey")//.addClass("float").css("margin-top", "10px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
						});

//document dialog cancel
					$("#btn_doc_dialog_cancel").button({
						icons: {primary: "ui-icon-closethick"}
					}).click(function (e) {
						e.preventDefault();
						$("#div_view_doc").dialog("close");
					}).removeClass("ui-state-default").addClass("ui-button-minor-grey").css("margin-top", "10px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
						});

					//doc2
					$("#btn_doc2_dialog_cancel").button({
						icons: {primary: "ui-icon-closethick"}
					}).click(function (e) {
						e.preventDefault();
						$("#div_doc").dialog("close");
					}).removeClass("ui-state-default").addClass("ui-button-minor-grey")//.addClass("float").css("margin-top", "10px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
						});


					var w = $("#tbl_job_form").width() + 50;
					$("#div_job").dialog({
						width: w + "px",
						modal: true,
						autoOpen: false
					});
//save job
					$("#btn_save_job").button({
						icons: {primary: "ui-icon-disk"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						var $form = $("form[name=frm_edit_job]");
						var err = false;
						$form.find("select").each(function () {
							//console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
							$(this).removeClass("required");
							if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								if ($(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0) {
									err = true;
									$(this).addClass("required");
								}
							}
						});
						$form.find("input:text, textarea").each(function () {
							//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
							$(this).removeClass("required");
							if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								if ($(this).val() == "" || $(this).val().length == 0) {
									err = true;
									$(this).addClass("required");
								}
							}
						});
						if (err) {
							AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
						} else {
							var dta = AssistForm.serialize($form);
							var result = AssistHelper.doAjax("inc_controller.php?action=JOB." + $("#div_job #form_action").val(), dta);
							if (typeof (result[0]) !== "undefined") {
								if (result[0] == "ok") {
									var url = "<?php echo $_SERVER["REQUEST_URI"]; ?>";
									AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
								} else {
									//AssistHelper.finishedProcessing(result[0], result[1]);
								}
							}
						}
						//DEV PURPOSES ONLY - hide on live
						//$form.prop("action","form_process.php");
						//$form.prop("method","post");
						//$form.submit();

					}).removeClass("ui-state-default").addClass("ui-button-bold-green");


//save note
					$("#btn_save_note").button({
						icons: {primary: "ui-icon-disk"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						var $form = $("form[name=frm_edit_note]");
						var err = false;
						/*  $form.find("select").each(function() {
							  //console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
							  $(this).removeClass("required");
							  if(($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								  if($(this).val() == "X"  || $(this).val() == "0" || $(this).val() == 0) {
									  err = true;
									  $(this).addClass("required");
								  }
							  }
						  });*/
						$form.find("input:text, textarea").each(function () {
							//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
							$(this).removeClass("required");
							if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								if ($(this).val() == "" || $(this).val().length == 0) {
									err = true;
									$(this).addClass("required");
								}
							}
						});
						if (err) {
							AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
						} else {
							var dta = AssistForm.serialize($form);
							var result = AssistHelper.doAjax("inc_controller.php?action=NOTE." + $("#div_note #form_action").val(), dta);
							if (result[0] == "ok") {
								var url = "<?php echo $_SERVER["REQUEST_URI"]; ?>";
								AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
							} else {
								AssistHelper.finishedProcessing(result[0], result[1]);
							}
						}
						//DEV PURPOSES ONLY - hide on live
						//$form.prop("action","form_process.php");
						//$form.prop("method","post");
						//$form.submit();

					}).removeClass("ui-state-default").addClass("ui-button-bold-green");

//save document
					$("#btn_save_doc").button({
						icons: {primary: "ui-icon-disk"}
					}).click(function (e) {
						e.preventDefault();
						AssistHelper.processing();
						var $form = $("form[name=frm_edit_doc]");

						var err = false;
						$form.find("select").each(function () {
							//console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
							$(this).removeClass("required");
							if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								if (this.selectedIndex == 0 || $(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0 || $(this).val() == "" || $(this).val() == "SELECT") {
									err = true;
									$(this).addClass("required");
								}
							}
						});
						$form.find("input:text,input:file, textarea").each(function () {
							//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
							$(this).removeClass("required");
							if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								if ($(this).val() == "" || $(this).val().length == 0) {
									err = true;
									$(this).addClass("required");
								}
							}
						});
						$form.find("input:file").each(function () {
							$(this).removeClass("required");
							if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
								if ($(this).val() == "" || $(this).val().length == 0) {
									err = true;
									$(this).addClass("required");
								}
							}
						});
						if (err) {
							AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
						} else {


							var result = EMP1Helper.processObjectFormWithAttachment($form);
							//console.log(AssistForm.serialize($form));
							//var result = AssistHelper.processObjectFormWithAttachment("inc_controller.php?action=DOCUMENT." +$("#div_doc #form_action").val(),dta);
							if (typeof (result[0]) !== "undefined") { //error mitigation
								if (result[0] == "ok") {
									var url = "<?php echo $_SERVER["REQUEST_URI"]; ?>";
									AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
								} else {
									AssistHelper.finishedProcessing(result[0], result[1]);
								}
							}
						}
						//DEV PURPOSES ONLY - hide on live
						//$form.prop("action","form_process.php");
						//$form.prop("method","post");
						//$form.submit();

					}).removeClass("ui-state-default").addClass("ui-button-bold-green");


//delete Job

					$("#btn_delete_job").button({
						icons: {primary: "ui-icon-trash"}
					}).click(function (e) {
						e.preventDefault();
						var ref = $("#div_job #tr_edit_ref td:first").html();
						var i = $("#div_job #job_id").val();
						$("<div />", {
							id: "dlg_confirm",
							html: "<p id=p_focus>Are you sure you wish to delete " + ref + "?</p>"
						}).dialog({
							modal: true,
							buttons: [
								{
									text: "Confirm & Continue", click: function () {
										$(this).dialog("close");
										AssistHelper.processing();
										//do the delete
										var result = AssistHelper.doAjax("inc_controller.php?action=JOB.DELETE", "job_id=" + i);
										//reload the page
										var url = "<?php echo isset($page_redirect_self) ? $page_redirect_self : $page_redirect; ?>";
										if (typeof (result[0]) !== "undefined") {
											AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
										}
									}, class: 'ui-button-state-grey'
									, icons: {primary: "ui-icon-trash"}
								},
								{
									text: "Cancel", click: function () {
										$(this).dialog("destroy");
									}, class: 'ui-button-minor-grey'
									, icons: {primary: "ui-icon-closethick"}
								}
							]
						});
						AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
						$("#p_focus").focus();


					}).removeClass("ui-state-default").addClass("ui-button-minor-grey").css("margin-left", "20px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-red");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-red");
						});
//delete document

					$("#btn_delete_doc").button({
						icons: {primary: "ui-icon-trash"}
					}).click(function (e) {
						e.preventDefault();
						var ref = $("#div_doc #tr_edit_ref td:first").html();
						var i = $("#div_doc #doc_id").val();
						var j = $("#div_doc #doc_empid").val()
						$("<div />", {
							id: "dlg_confirm",
							html: "<p id=p_focus>Are you sure you wish to delete " + ref + "?<br><br> Deleted documents cannot be restored " +
								"& will have to be re-uploaded if they are deleted.</p>"
						}).dialog({
							modal: true,
							buttons: [
								{
									text: "Confirm & Continue", click: function () {
										$(this).dialog("close");
										AssistHelper.processing();
										//do the delete
										var result = AssistHelper.doAjax("inc_controller.php?action=DOCUMENT.DELETE", "doc_id=" + i + "&doc_empid=" + j);
										var ref = '0';
										var dta = 'activity=EDIT&object_id=' + i + '&i=' + ref;
										//deletes the  document , moving it to the deleted folder
										var res = AssistHelper.doAjax('inc_controller.php?action=DOCUMENT.DELETE_ATTACH', dta);

										if (typeof (result[0]) !== "undefined") {
											if (res[0] == "ok") {
												//reload the page
												var url = "<?php echo isset($page_redirect_self) ? $page_redirect_self : $page_redirect; ?>";
												AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
											}
										}
									}, class: 'ui-button-state-grey'
									, icons: {primary: "ui-icon-trash"}
								},
								{
									text: "Cancel", click: function () {
										$(this).dialog("destroy");
									}, class: 'ui-button-minor-grey'
									, icons: {primary: "ui-icon-closethick"}
								}
							]
						});
						AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
						$("#p_focus").focus();


					}).removeClass("ui-state-default").addClass("ui-button-minor-grey").css("margin-left", "20px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-red");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-red");
						});

//Delete note
					$("#btn_delete_note").button({
						icons: {primary: "ui-icon-trash"}
					}).click(function (e) {
						e.preventDefault();
						// var ref = $("#div_note #tr_edit_ref td:first").html();
						var i = $("#div_note #note_id").val();
						var j = $("#div_note #note_empid").val();
						$("<div />", {
							id: "dlg_confirm",
							html: "<p id=p_focus>Are you sure you wish to delete this note?</p>"
						}).dialog({
							modal: true,
							buttons: [
								{
									text: "Confirm & Continue", click: function () {
										$(this).dialog("close");
										AssistHelper.processing();
										//do the delete
										var result = AssistHelper.doAjax("inc_controller.php?action=NOTE.DELETE", "note_id=" + i + "&note_empid=" + j);

										//reload the page
										var url = "<?php echo isset($page_redirect_self) ? $page_redirect_self : $page_redirect; ?>";
										if (typeof (result[0]) !== "undefined") {
											AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
										}
									}, class: 'ui-button-state-grey'
									, icons: {primary: "ui-icon-trash"}
								},
								{
									text: "Cancel", click: function () {
										$(this).dialog("destroy");
									}, class: 'ui-button-minor-grey'
									, icons: {primary: "ui-icon-closethick"}
								}
							]
						});
						AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
						$("#p_focus").focus();


					}).removeClass("ui-state-default").addClass("ui-button-minor-grey").css("margin-left", "20px")
						.hover(function () {
							$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-red");
						}, function () {
							$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-red");
						});

					$("td.missing-mileage").each(function () {
						$(this).find("div:first").css({
							"border": "1px solid #cc0001",
							"border-radius": "5px",
							"padding": "5px"
						}).addClass("red").addClass("red-back").addClass("red-border");
					});


				});


				//When a person clicks on a new tab, it will scroll up
				$(".tablink").click(function () {
					$('#content').scrollTop(0);
				});
				$(".tablink").click(function () {
					$('body, html').animate({scrollTop: 0}, 'slow');
				});
				$('#content').scroll(function () {
					if ($(this).scrollTop() > 100) {
						$("#content").css('visibility', 'visible');
					} else {
						$("#content").css('visibility', 'hidden');
					}
				});


			} else {
				AssistHelper.processing();
				AssistHelper.closeProcessing();

			}
		}

		function dialogFinished(icon, result) {
			if (typeof (icon) !== "undefined") {
				if (icon == "ok") {
					AssistHelper.processing();
					var url = "<?php echo $_SERVER["REQUEST_URI"]; ?>";
					AssistHelper.finishedProcessingWithRedirect(icon, result, url);

				} else {
					AssistHelper.processing();
					AssistHelper.finishedProcessing(icon, result);
				}
			}
		}
	</script>
	<?php
}
?>