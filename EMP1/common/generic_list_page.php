<?php
/*******
 * Pre-requisites:
 * $page_section = MANAGE || ADMIN
 * $page_action = UPDATE || EDIT
 *
 *  -[SD] AA-643 EMP1 - Add search function to list pages
 * $search_name from manage_view.php and manage_edit.php
 * $search_type
 */


if(!isset($_REQUEST['object_type'])) {
	if($page_action == "MANAGE") {
		$_REQUEST['object_type'] = "EMPLOYEE";
	} else {
		$_REQUEST['object_type'] = "EMPLOYEE";
	}
}
$object_type = $_REQUEST['object_type'];
$my_page = strtolower($_REQUEST['object_type']);

require_once("inc_header.php");

if(isset($not_allowed_if_integrated) && $not_allowed_if_integrated == true && $empObject->isCompanyHRIntegrated() == true) {
	ASSIST_HELPER::displayResult(array("error", "This function is not available as automatic HR Integration is active."));
	die();
}
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


if($add_button) {
	$add_button_label = $helper->replaceAllNames($add_button_label);
}


$page_type = "LIST";
if(!isset($button_label) || strlen($button_label) == 0) {
	$button_label = $helper->getActivityName("open");
} elseif($button_label == "breadcrumbs") {
	$button_label = implode(" ", $menu['breadcrumbs']);
} elseif(substr($button_label, 0, 1) == "|" && substr($button_label, -1) == "|") {
	$button_label2 = $helper->getActivityName(substr($button_label, 1, -1));
	if(strlen($button_label2) == 0 || $button_label2 == false) {
		$button_label = $helper->getObjectName(substr($button_label, 1, -1));
	} else {
		$button_label = $helper->replaceAllNames($button_label2);
	}
}

if($page_section == "NEW") {
	$page_action .= ($page_action != "CONFIRM" && substr($page_action, 0, 8) != "ACTIVATE" ? "_".$child_object_type : "");
} elseif($page_action != "VIEW") {
	$page_action .= "_".$object_type;
}
if(isset($page_direct)) {
	$page_direct = strtolower($page_direct).(substr($page_direct, -1) != "&" ? "?" : "")."object_id=";
} elseif($page_section != "NEW") {
	$page_direct = strtolower($page_section)."_".strtolower($page_action)."_object.php?object_id=";
} else {
	$pa = substr($page_action, 0, strpos($page_action, "_"));
	$page_direct = strtolower($page_section)."_".strtolower($object_type)."_".strtolower($pa)."_object.php?object_id=";
}

$js .= " var page_direct = '".$page_direct."';
";
$class_name = "EMP1_".$object_type;
$myObject = new $class_name();
if(isset($options)) {
	$options = array(
			'type' => $page_type,
			'section' => $page_section,
			'page' => $page_action,
		) + $options;
} else {
	$options = array(
		'type' => $page_type,
		'section' => $page_section,
		'page' => $page_action,
	);
}
/**
 *  [SD] AA-643 EMP1 - Add search function to list pages
 *  comment: The values below come from the manage_view.php and manage_edit.php page
 */
if(($search_name !="") || ($search_type != "")  ){
	$options['search']['search_type'] = $search_type;
	if($search_type == 'quick'){
		//quick search
		$options['search']['search_name'] = $search_name;
	}else if($search_type == 'advanced' && isset($_REQUEST['search_data'])){
		//advanced search

		//1.validate the fields to make sure only the correct data from the $_REQUEST array is passed.
		$sections = array("USER","EMPLOYEE","JOB");
            //Get headings
		$headings = array();
		$headingObject = new EMP1_HEADINGS();
		$headings['USER'] = $headingObject->getMainObjectHeadings("USER");
		$headings['EMPLOYEE'] = $headingObject->getMainObjectHeadings("EMPLOYEE");
		$headings['JOB'] = $headingObject->getMainObjectHeadings("JOB");
		$options['search']['headings'] = $headings; //pass down headings to use when validating the correct data to pull
		$options['search']['sections'] = $sections;//pass down sections to sort headings
		foreach($sections as $section) {
			$heading_section = strtolower($section)."_heading";
			$main_heading = $headings[$section]['rows'][$heading_section];
			foreach($headings[$section]['sub'][$main_heading['id']] as $id => $head) {
				$heading_fld = $head['field'];
				//2. prepare the $options['search'] array to receive data to pass down to getCurrentListOfEmployees()
				foreach($_REQUEST['search_data'] as $search_field => $val) {
				    if(($heading_fld == $search_field) && !empty($val)) {
					    //if the fields that come from the advanced page are the same as the fields from the assist_***_emp_setup_headings table, proceed storage.
					    $options['search'][$search_field] = $val;
					    unset($_REQUEST['search_data'][$search_field]); //free memory
				    }
				}
			}
		}

	}
}

$options['start'] = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$options['limit'] = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : "25";


$button = array('value' => $helper->replaceAllNames($button_label), 'class' => "btn_list", 'type' => "button");
if(isset($button_icon)) {
	$button['icon'] = $button_icon;
}
//$button['pager'] = false;
//disable paging temporarily
//$button = false;
//ASSIST_HELPER::arrPrint($options);

$js .= $displayObject->drawListTable($myObject->getObject($options), $button, $object_type, $options, false, array((isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : "")));


$js .= "
		$(\"button.btn_list\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";


?>
<script type=text/javascript>
	var url = "<?php echo $page_direct; ?>";
	var section = "<?php echo $object_type; ?>";

	$(function () {
		<?php echo $js; ?>
		/**
		 *  [SD] AA-643 EMP1 - Add search function to list pages
		 *  comment: When an advanced search is made the clear button should appear. this button appears for quick search as well.
         *  located : getPaging() EMP1_DISPLAY
		 */
		var search_type = "<?php echo $search_type; ?>";
		if(search_type == "advanced" || search_type == "quick"){
			$('#clear_search').show();
			$('#clear_search').focus();
		}else{
			$('#clear_search').hide();
		}
	});

	function tableButtonClick($me) {
		<?php
		if(isset($alternative_button_click_function) && $alternative_button_click_function !== false && strlen($alternative_button_click_function) > 0) {
			echo $alternative_button_click_function."($"."me);";
		} else {
			echo "
			AssistHelper.processing();
				var r = $"."me.attr(\"ref\");
				document.location.href = url+r;
				";
		}
		?>

	}

	function formatTableButton($me) {
		$me.button({
			icons: {primary: "ui-icon-<?php echo isset($button_icon) ? $button_icon : "newwin"; ?>"},
		}).children(".ui-button-text").css({"font-size": "80%"}).click(function (e) {
			e.preventDefault();

		}); //.removeClass(\"ui-state-default\").addClass(\"ui-state-info\").css(\"color\",\"#fe9900\")

	}
</script>