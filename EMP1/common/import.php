<?php
/**
 * @var EMP1_HEADING $headingObject - from inc_header
 * @var string $section - from calling page
 */
$page_section = "ADMIN";
$page_action = "EDIT";
$original_url = "admin_import.php";
$redirect_url = "admin_import_".strtolower($section).".php";
$is_add_page = true;
$is_edit_page = TRUE;
$is_view_page = true;
$list_view_page = "common/import_list_view.php";

$sections = array("USER","EMPLOYEE","JOB");

$max_rows_to_import = 500;
$name_divider = ":";
require_once("inc_header.php");

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "STEP0";

$original_headings = array();

if($section == "EMPLOYEE") { // If employee then we know that it will also need job fields

	foreach($sections as $sec) {
		$headings_raw = $headingObject->getMainObjectHeadings($sec, "DETAILS", "MANAGE");
		array_push($original_headings,$headings_raw);
	}
}

$headings = array('rows'=>array());

/*switch($section) {
	case "SUB":
		$headings['rows']['sub_function_id'] = array(
			'id'=>0,
			'field'=>"sub_function_id",
			'name'=>"Parent |function|",
			'section'=>"SUB",
			'type'=>"OBJECT",
			'list_table'=>"FUNCTION",
			'max'=>0,
			'parent_id'=>0,
			'parent_link'=>"",
			'default_value'=>"",
			'required'=>1,
			'help'=>"",
		);
		break;
	case "ACTIVITY":
		$headings['rows']['activity_subfunction_id'] = array(
			'id'=>0,
			'field'=>"activity_subfunction_id",
			'name'=>"Parent |function| $name_divider |sub|",
			'section'=>"SUB",
			'type'=>"OBJECT",
			'list_table'=>"SUB",
			'max'=>0,
			'parent_id'=>0,
			'parent_link'=>"",
			'default_value'=>"",
			'required'=>1,
			'help'=>"",
		);
		break;
}*/
$has_results = false;
foreach($original_headings as $field => $all_original_headings) {
	foreach($all_original_headings['sub'] as $fld => $data) {
		foreach($data as $inner_fld => $head) {
			if($head['type']!="REF" && $head['type']!="ATTACH" && $head['type']!="USER_STATUS" && $head['type']!="EMP_STATUS" && $head['type']!="HEAD" && $head['field']!="job_end" && $head['field']!="Job_newcontract") {
				$headings['sub'][$head['field']] = $head;
			}
		}
	}
}
//ASSIST_HELPER::arrPrint($headings);
/*if($action=="STEP0") {
	echo "<div class='float' style='padding:5px; margin:5px; border:1px solid #000099'><p class='b i'>Switch to:</p>";
	foreach($sections as $s) {
		if($s != $section) {
			echo "<P><button id='btn_".$s."' section='$s' class='btn-switch'>".$headingObject->replaceAllNames("|".$s."|")."</button></p>";
		}
	}
	echo "</div>";
}*/

echo "<h2>".$headingObject->replaceAllNames("|".$section."|")."</h2>";


switch($section) {
	case "EMPLOYEE":
		$me = new EMP1_EMPLOYEE();
		break;

}

$headings['sub'] = $me->replaceAllNames($headings['sub']);

switch($action) {

//Step 1 = generate template
	case "STEP1":
		//handled by import_generate.php
		break;


//Step 2 = populate template - done by user
	case "STEP2":
		break;


//Step 3 = import template for validation
	case "STEP3":
		include("import_step3.php");
		break;


//Step 4 = finalise import
	case "STEP4":
		include("import_step4.php");
		break;


//Step 0 = starting point
	case "STEP0":
	default:

		echo "
<form name=frm_import method=post action=".$redirect_url." enctype=\"multipart/form-data\">
<input type=hidden name=section value='$section' />
<input type=hidden name=action value='STEP3' />
<table width=550px class=form>
	<tr>
		<th width=60px>Step 1:</th>
		<td>Generate template (if not done already).
		<span class=float><button id=btn_step0_generate class=btn-step0>".$me->replaceActivityNames("|download|")."</button></span></td>
	</tr>
	<tr>
		<th>Step 2:</th>
		<td>Populate the template.</td>
	</tr>
	<tr>
		<th>Step 3:</th>
		<td>Import the template.<br />&nbsp;
		<span class=float><input type=file name=import_file value='' /> <button id=btn_step0_import class=btn-step0>".$me->replaceActivityNames("|import|")."</button></span></td>
	</tr>
	<tr>
		<th>Step 4:</th>
		<td>Finalise and Accept the Import.  The line items will not be imported until the \"Accept\" button is clicked.</td>
	</tr>

</table>
</form>
<h3>Guidelines on Using the Template</h3>
<ul>
<li class=red>WARNING: It is advisable NOT to make any changes to the Headings between generating the template in Step 1 and finalising the import in Step 3.  Changes to the Headings can change the structure of the template which will cause the process to fail.</li>
<li>The template is in comma-separated values (CSV) format.  If using Windows, please ensure that your computer's Regional Settings (also known as Region) has the following settings:<ul>
		<li>Decimal: . (period / full stop)</li>
		<li>List separator: , (comma)</li>
		<li>Thousand separator: \" \" (space)</li>
	</ul></li>
</li>
<li>The columns of the template must be in the order given below.</li>
<li>The first 2 rows will be ignored (assumed to be heading rows).</li>
<li>It is advisable to import no more than 500 rows at a time, to reduce the risk of the process taking too long and logging you out before it is complete.</li>
<li>Valid list items can be found <a href=#lists>here</a>.</li>
<li>It is advisable to import no more than $max_rows_to_import rows at a time, to reduce the risk of the process taking too long and logging you out before it is complete.</li>
</ul>
<h4>Template Columns</h4>
<table class='form'>";

echo ASSIST_HELPER::getFloatingDisplay(array("info","Please note when populating the import template, managers must be loaded according to the organisational structure."));
$str = "A";
$lists = array();
foreach($headings['sub'] as $fld => $head) {
	if($head['type']!="REF" && $head['type']!="ATTACH" && $head['type']!="USER_STATUS" && $head['type']!="EMP_STATUS" && $head['type']!="HEAD" && $head['field']!="job_end" && $head['field']!="Job_newcontract") {
		if($headingObject->isListField($head['type'])) {
			$list_table = !empty($head['list_table']) ? $head['list_table'] : $head['type'];
			if(!isset($lists[$head['list_table']])) {
				$lists[$list_table] = $head;
			} else {
				$lists[$list_table]['name'] .= " & ".$head['name'];
			}
		}
		$help = $headingObject->replaceAllNames($head['help'])."  ";
		switch($head['type']) {
			case "BOOL":
				$help.="Yes / No.";
				break;
			case "NUM":
			case "TARGET":
				$help .= (strlen($help) > 0 ? " " : "")."Numbers only, unformatted and without target types i.e. '1123456.78' not '1,123,456.78' or '78.5' not '78.5%'.";
				break;
			case "TEXT":
				$help .= (strlen($help) > 0 ? " " : "")."Unlimited text.";
				break;
			case "MEDVC":
			case "LRGVC":
				$help .= (strlen($help) > 0 ? " " : "")."Max characters: ".$head['max'].".";
				break;
			case "LIST":
				$help .= (strlen($help) > 0 ? " " : "")."A valid list item from the ".$head['name']." list.  See below for a complete set of valid items.";
				break;
		}
		if($head['required'] == true) {
			$help .= (strlen($help) > 0 ? " " : "")."<span class=red>This field is required.</span>";
		}
		echo "
		<tr>
			<th width=80px>Column ".$str.":</th>
			<td class=b>".$headingObject->replaceAllNames($head['name'])."</td>
			<td>".$help."</td>
		</tr>
		";
		$str++;
	}
}
echo "
</table>
";

if(count($lists)>0) {
	echo "
<h3 id=lists>List Items</h3>
";
	foreach($lists as $table => $list) {
		$type = $list['type'];
		$lot = $table;
		$list_name = $headingObject->replaceAllNames($list['name']);
		$button_name = $headingObject->getActivityName("open");
		$export_button_name = $headingObject->getActivityName("export");
		echo "<div style='width:350px;' id='div_".$lot."' class='div_segment' list_name='".addslashes($list_name)."' list_type='".$type."' list_table='".$lot."' my_btn='btn_".$lot."_open'>
		<h3 class='object_title'>".$list_name."</h3>
		<p class='right'><button class='btn_open' id='btn_".$lot."_open'>".$button_name."</button>&nbsp;<button class='btn_export' id='btn_".$lot."_export'>".$export_button_name."</button></p>
	</div>";
	}
}
	echo "<br>";
	ASSIST_HELPER::goBack();

?>
<div id=div_lists>
	<iframe id=ifr_lists></iframe>
</div>
	<?php
	break;
}

?>
<script type="text/javascript">
	$(function () {
//STEP 0 CODE
		$(".btn-step0").button()
			.removeClass("ui-state-default")
			.addClass("ui-button-bold-grey")
			.hover(
				function () {
					$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
				},
				function () {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey");
				}
			);
		$("#btn_step0_import").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-n"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
				$("form[name=frm_import]").submit();
		});
		$("#btn_step0_generate").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-s"}
		}).click(function (e) {
			e.preventDefault();
			document.location.href = "common/import_generate.php?section=<?php echo $section; ?>";
		});


//switch to buttons
		$(".btn-switch").button()
			.removeClass("ui-state-default").addClass("ui-button-state-blue")
			.hover(function(){
				$(this).removeClass("ui-button-state-blue").addClass("ui-button-state-orange");
			},function(){
				$(this).addClass("ui-button-state-blue").removeClass("ui-button-state-orange");
			}).click(function(e) {
				e.preventDefault();
				var s = $(this).attr("section");
				document.location.href = "admin_import_"+s.toLowerCase()+".php";
		});


//list section code
		var scr_dimensions = AssistHelper.getWindowSize();
		var div_h = 0;
		var tbl_h = 0;
		var dlg_width=0;
		var dlg_height=0;
		if(scr_dimensions.width>700) {
			dlg_width=700;
		} else {
			dlg_width=scr_dimensions.width*0.75;
		}
		if(scr_dimensions.height>700) {
			dlg_height=700;
		} else {
			dlg_height=scr_dimensions.height*0.75;
		}
		$("#div_lists").dialog({
			modal: true,
			autoOpen: false,
			width:dlg_width,
			height:dlg_height,
			close: function() {
				$("#ifr_lists").prop("src","");
			}
		});
		$("div.div_segment").button().click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			//$("#ifr_lists").html("Loading...");
			var list_type = $(this).attr("list_type");
			var table = $(this).attr("list_table");
			var name = $(this).attr("list_name");
			var url = '<?php echo $list_view_page; ?>?t='+table+'&n='+name+'&y='+list_type;
			$("#div_lists").dialog("option","title",name);
			$("#ifr_lists").prop("width",dlg_width-35).prop("height",dlg_height-50).prop("src",url).css("border","1px solid #FFFFFF");
			window.setTimeout(openDialog,3000);
		});
		$("div.div_segment").hover(
			function() {
				$me = $(this).find("#btn_"+$(this).attr("list_table")+"_open");
				$me.addClass("ui-button-state-orange").removeClass("ui-button-state-blue");
				$(this).find("h3").addClass("orange");
				$(this).addClass("orange-border").removeClass("light-blue-border");
			},
			function() {
				$me = $(this).find("#btn_"+$(this).attr("list_table")+"_open");
				$me.removeClass("ui-button-state-orange").addClass("ui-button-state-blue");
				$(this).find("h3").removeClass("orange");
				$(this).removeClass("orange-border").addClass("light-blue-border");
			}
		).css({"margin":"10px","background":"url()"});//,"border-color":"#0099FF"});
		$("div.div_segment").find("h3").css("font-family","Tahoma");

		$("button.btn_open").button({
			icons:{primary:"ui-icon-newwin"}
		}).removeClass("ui-state-default").addClass("ui-button-state-blue")
		.hover(
			function() {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-blue");
				$me = $(this).closest("div");
				$me.find("h3").addClass("orange");
				$me.addClass("orange-border").removeClass("light-blue-border");
			},
			function() {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-blue");
				$me = $(this).closest("div");
				$me.find("h3").removeClass("orange");
				$me.removeClass("orange-border").addClass("light-blue-border");
			}
		);
		$("button.btn_open").click(function(e) {
			e.preventDefault();
			$(this).parent("div.div_segment").trigger("click");
		});
		$("button.btn_export").button({
			icons:{primary:"ui-icon-arrowreturnthick-1-s"}
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
		.hover(
			function() {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-grey");
				$me = $(this).closest("div");
				$me.find("h3").addClass("orange");
				$me.addClass("orange-border").removeClass("light-blue-border");
			},
			function() {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
				$me = $(this).closest("div");
				$me.find("h3").removeClass("orange");
				$me.removeClass("orange-border").addClass("light-blue-border");
			}
		).click(function(e) {
			e.preventDefault();
			e.stopPropagation();	//don't trigger the parent div click event
			AssistHelper.processing();
			var list_type = $(this).closest("div").attr("list_type");
			var table = $(this).closest("div").attr("list_table");
			var name = $(this).closest("div").attr("list_name");
			var url = '<?php echo $list_view_page; ?>?page=export&t='+table+'&n='+name+'&y='+list_type;
			document.location.href = url;
			AssistHelper.closeProcessing();
		});
		function formatButtons() {
			$("button.xbutton").children(".ui-button-text").css({"font-size":"80%","padding":"4px","padding-left":"24px"});
			$("button.xbutton").css({"margin":"2px"});
		}
		formatButtons();

		$("#tbl_container, #tbl_container td").css("border","1px solid #ffffff");
		$(".div_segment").addClass("light-blue-border")//.css("border","1px solid #9999ff");



});
		function openDialog() {
			$(function() {
				$("#div_lists").dialog("open");
				AssistHelper.closeProcessing();
			});
		}
</script>