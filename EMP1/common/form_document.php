<?php
if(!isset($modref)) {
	$modref = "emp";
}
$DocumentObject = new EMP1_DOCUMENT($object_id, $modref);
$headings['DOCUMENT'] = $headingObject->getMainObjectHeadings("DOCUMENT");
//$full_user_details = array();
$full_user_details = $empObject->getRawEmployeeDetailsByEmployeeID($object_id, false);
$user_id = $full_user_details[$empObject->getParentFieldName()];
$user_details = $empObject->getFormattedOriginalUserDetails($user_id, $is_add_page);
$section = "DOCUMENT";
$doc_head = $headings[$section]['rows']['doc_heading'];
$doc_headings = $headings[$section]['sub'][$doc_head['id']];
$js = "";
global $is_edit_page, $is_view_page, $is_add_page, $convert_user, $is_active_employee;
global $full_user_details;
$is_active_employee = true;
function drawField($section, $fld, $head) {
	global $user_details;
	global $full_user_details;
	global $can_enter_employee_details;
	global $displayObject;
	global $is_edit_page, $is_view_page, $is_add_page, $convert_user, $is_active_employee;
	global $emp_timekeep_field_map;
	global $helper;
	$js = "";
	if(($section == "USER" && !$can_enter_employee_details)) {
		echo isset($user_details[$fld]) ? $user_details[$fld] : "";
		if($fld == "tkname" || $fld == "tksurname") {
			echo "<input type=hidden name='$fld' value=\"".$user_details[$fld]."\"  />";
		}
	} else {
		$prop = array(
			'id' => $fld,
			'name' => $fld,
		);
		$prop['req'] = isset($head['required']) ? $head['required'] : false;
		switch($head['type']) {

			case "LIST":
				$listObject = new EMP1_LIST($head['list_table']);
				$list_options = $listObject->getActiveListItemsFormattedForSelect();
				$prop['options'] = $list_options;
				$prop['req'] = isset($head['required']) ? $head['required'] : false;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
				} else {
					$js .= $displayObject->drawFormField("LIST", $prop, $value);
				}
				break;
			case "MASTER":
				$class = "ASSIST_MASTER_".strtoupper($head['list_table']);
				$listObject = new $class();
				$list_options = $listObject->getActiveItemsFormattedForSelect();
				$prop['options'] = $list_options;
				$prop['req'] = isset($head['required']) ? $head['required'] : false;
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("TEXT", (isset($list_options[$value]) ? $list_options[$value] : $helper->getUnspecified()));
				} else {
					if($is_edit_page) {
						$val = $value;
						$value = "";
						foreach($list_options as $key => $v) {
							if(strtolower($v) == strtolower($val)) {
								$value = $key;
								break;
							}
						}
					}
					$js .= $displayObject->drawFormField("LIST", $prop, $value);
				}
				break;
			case "DATE":
				$prop['options'] = array();
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
					if(strlen($value) > 0 && $value != "0000-00-00") {
						$value = date("d-M-Y", strtotime($value));
					} else {
						$value = "";
					}
				} elseif($convert_user && (isset($emp_timekeep_field_map[$fld]) || isset($user_details[$fld]))) {
					$value = isset($emp_timekeep_field_map[$fld]) ? $user_details[$emp_timekeep_field_map[$fld]] : $user_details[$fld];
					if(ASSIST_HELPER::checkIntRef($value)) {
						$value = date("d-M-Y", $value);
					}
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField("DATE", $value);
				} else {
					$js .= $displayObject->drawFormField($head['type'], $prop, $value);
				}
				break;
			default:
				$value = $head['default_value'];
				if($is_edit_page || $is_view_page) {
					if(isset($user_details[$fld])) {
						$value = $user_details[$fld];
					} elseif(isset($full_user_details[$fld])) {
						$value = $full_user_details[$fld];
					}
				} elseif($convert_user && isset($emp_timekeep_field_map[$fld])) {
					$value = $user_details[$emp_timekeep_field_map[$fld]];
				}
				if($is_view_page || !$is_active_employee) {
					$js .= $displayObject->drawDataField($head['type'], $value);
				} else {
					$js .= $displayObject->drawFormField($head['type'], $prop, $value);
				}
				break;
		}
	}
	return $js;
}

if(!isset($is_edit_page)) {
	if(!isset($is_add_page)) {
		$is_add_page = true;
		$is_edit_page = false;
	} elseif($is_add_page) {
		$is_edit_page = false;
	} else {
		$is_edit_page = true;
	}
} else {
	if($is_edit_page || $is_view_page) {
		$is_add_page = false;
	} else {
		$is_add_page = true;
	}
}


$doc_id = "";
if($is_edit_page) {
	//ASSIST_HELPER::arrPrint($job_headings);
	$docs = $DocumentObject->getDocFullDetailByEmployeeID($full_user_details['emp_id']);

	$doc_id = $docs['doc_id'];
	?>
	<div id=div_view_doc>

		<form name=form_view_doc>
			<input type=hidden name=form_action_view id=form_action value='ADD' class='no-check'/>
			<input type=hidden id=doc_id name=doc_id value='<?php if(ASSIST_HELPER::checkIntRef($doc_id) == true) {
				echo $doc_id;
			} else {
				$doc_id = 0;
				echo $doc_id;
			} ?>' class='no-check'/>
			<input type=hidden id=doc_empid name=doc_empid value='<?php echo !isset($full_user_details['emp_id']) ? $full_user_details['emp_id'] : 0; ?>' class='no-check'/>
			<Table id=tbl_doc_view class=form>

				<?php
				foreach($doc_headings as $h_id => $head) {
					$name = $head['name'];
					if($name != "Document Status") {
						echo "
			<tr id=tr_edit_".$head['field'].">
				<th>".$head['name'].($head['required'] == true ? "*" : "").":</th>
				<td>";
						$js .= drawField("DOCUMENT", $head['field'], $head);
						echo "</td>
			</tr>
			";
					}
				}
				?>
				<tr>
					<th></th>
					<td>
						<button id=btn_save_doc>Save Changes</button>
						<button id=btn_delete_doc>Delete</button>
						<button id=btn_dialog_cancel>Cancel</button>
						<button id=btn_edit_doc>Edit</button>
					</td>
				</tr>
			</Table>
			<!--<div id=div_warn>
                <?php
			/*                ASSIST_HELPER::displayResult(array("warn",
								"Warning: Adding a new Job will automatically close the previous job, if the start date for the new contract falls after the start date of the previous contract, and register a new contract for this Employee.  This will impact the assessment of any related Scorecards in the Individual Performance module."));
							*/ ?>
            </div>-->


		</form>
		<button id=btn_edit_doc>Edit</button>
	</div>
	<?php
}
?>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$(document).ready(function () {
			$("#tbl_doc_view th").css({"background-color": "#000098", "color": "white"});
			$("#btn_delete_doc").hide();
			$("#div_warn").hide();
			$("#div_view_doc #form_action").val("ADD");
			$("#div_view_doc #doc_id").val("0");
			$("#div_view_doc #tr_edit_ref td:first").html("#");
			$("#div_view_doc").find("input, textarea").not(".no-check").each(function () {
				$(this).val("");
				$(this).removeClass("required");
			});
			$("#div_view_doc").find("select").not(".no-check").each(function () {
				$(this).val($(this).find("option:first").val());
				$(this).removeClass("required");
			});


		});


		$("#btn_save_job").button({
			icons: {primary: "ui-icon-disk"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var $form = $("form[name=frm_edit_job]");
			var err = false;
			$form.find("select").each(function () {
				//console.log($(this).prop("id")+" = "+(($(this).attr("req")==true || parseInt($(this).attr("req"))==1)?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="X" || $(this).val()=="0" || $(this).val()==0)?"requirement test fail":"pass"));
				$(this).removeClass("required");
				if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
					if ($(this).val() == "X" || $(this).val() == "0" || $(this).val() == 0) {
						err = true;
						$(this).addClass("required");
					}
				}
			});
			$form.find("input:text, textarea").each(function () {
				//console.log($(this).prop("id")+" = "+((($(this).attr("req")==true || parseInt($(this).attr("req"))==1) && $(this).is(":visible"))?"is_required":"not req")+" = "+$(this).val()+" = "+(($(this).val()=="" || $(this).val().length=="")?"requirement test fail":"pass"));
				$(this).removeClass("required");
				if (($(this).attr("req") == true || parseInt($(this).attr("req")) == 1) && $(this).is(":visible")) {
					if ($(this).val() == "" || $(this).val().length == 0) {
						err = true;
						$(this).addClass("required");
					}
				}
			});
			if (err) {
				AssistHelper.finishedProcessing("error", "Please completed the required fields highlighted in red.");
			} else {
				var dta = AssistForm.serialize($form);
				var result = AssistHelper.doAjax("inc_controller.php?action=JOB." + $("#div_job #form_action").val(), dta);
				if (result[0] == "ok") {
					var url = "<?php echo $_SERVER["REQUEST_URI"]; ?>";
					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			}
			//DEV PURPOSES ONLY - hide on live
			//$form.prop("action","form_process.php");
			//$form.prop("method","post");
			//$form.submit();

		}).removeClass("ui-state-default").addClass("ui-button-bold-green");
	.
		hover(function () {
			$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
		}, function () {
			$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
		});

		$("#btn_edit_doc").button({
			icons: {primary: "ui-icon-pencil"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();//doesn't wanna work for some reason
			var dta = "obj_id=" + <?php echo $object_id ?>;
			var doc = AssistHelper.doAjax("inc_controller.php?action=DOCUMENT.getRawObject", dta)

			$("#div_view_doc #doc_id").val(<?php echo $doc_id ?>);
			$("#div_view_doc").find("input, select, textarea").not(".no-check").each(function () {
				var i = $(this).prop("id");
				$(this).html(doc[i]);
				$(this).removeClass("required");
				$("#div_view_doc #form_action").val("EDIT");
				AssistHelper.closeProcessing();
				/*  $("#div_job").dialog("option", "title", "Edit Job");
				  $("#div_job").dialog("open");*/
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey");


			$("#btn_delete_doc").button({
				icons: {primary: "ui-icon-trash"}
			}).click(function (e) {
				e.preventDefault();
				var ref = $("#div_view_doc #tr_edit_ref td:first").html();
				var i = $("#div_view_doc #doc_id").val();
				$("<div />", {
					id: "dlg_confirm",
					html: "<p id=p_focus>Are you sure you wish to delete " + ref + "?</p>"
				}).dialog({
					modal: true,
					buttons: [
						{
							text: "Confirm & Continue", click: function () {
								$(this).dialog("close");
								AssistHelper.processing();
								//do the delete
								var result = AssistHelper.doAjax("inc_controller.php?action=JOB.DELETE", "job_id=" + i);
								//reload the page
								var url = "<?php echo isset($page_redirect_self) ? $page_redirect_self : $page_redirect; ?>";
								AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
							}, class: 'ui-button-state-grey'
							, icons: {primary: "ui-icon-trash"}
						},
						{
							text: "Cancel", click: function () {
								$(this).dialog("destroy");
							}, class: 'ui-button-minor-grey'
							, icons: {primary: "ui-icon-closethick"}
						}
					]
				});

				$("#btn_add_doc").button({
					icons: {primary: "ui-icon-plus"}
				}).click(function (e) {
					e.preventDefault();
					AssistHelper.processing();
					$("#btn_delete_doc").hide();
					$("#div_warn").show();
					$("#div_view_doc #form_action").val("ADD");
					$("#div_view_doc #job_id").val("0");
					$("#div_view_doc #tr_edit_ref td:first").html("#");
					$("#div_view_doc").find("input, textarea").not(".no-check").each(function () {
						$(this).val("");
						$(this).removeClass("required");
					});
					$("#div_view_doc").find("select").not(".no-check").each(function () {
						$(this).val($(this).find("option:first").val());
						$(this).removeClass("required");
					});
					$("#tr_edit_job_end").hide();
					AssistHelper.closeProcessing();
					$("#div_view_doc").dialog("option", "title", "Add Document");
					$("#div_view_doc").dialog("open");
				}).removeClass("ui-state-default").addClass("ui-button-bold-green").css({
					"position": "relative",
					"bottom": "-20px"
				});

				$(".btn_save")
					.button()
					.removeClass("ui-button-default").addClass("ui-button-bold-green")
					.hover(function () {
						$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange")
					}, function () {
						$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green")
					})
					.click(function (e) {
						e.preventDefault();
						$form = $("form[name=frm_new]");
						//LIVE CODE - uncomment after development
						var page_action = "<?php echo "EMPLOYEE.".$save_action; ?>";
						var page_direct = "<?php echo $page_direct; ?>";
						EMP1Helper.processObjectForm($form, page_action, page_direct);

						//DEV code - needed to test controller called functions
						//WARNING: No form validation is taking place!
						//$form.prop("method","POST");
						//$form.prop("action","form_process.php");
						//$form.submit();
					});
				var offset = $("#div_view_doc").offset();
				$(window).resize(function () {
					// var state = $("#btn_hide_headings").attr("state");
					var scr = AssistHelper.getWindowSize();
					//if(state=="hide") {
					// var space_available_for_tabs = scr.height-5;
					// var tab_height = space_available_for_tabs<350 ? 350 : (space_available_for_tabs-10);
					// } else {
					var space_available_for_tabs = scr.height - offset.top;
					var tab_height = space_available_for_tabs < 350 ? 350 + Math.floor(offset.top) : (space_available_for_tabs - 100);
					//  }
					//$("#content").css({"height":"300px"});

					$("#div_view_doc").css("height", tab_height + "px");
					$("#div_view_doc").css({"height": (tab_height - 250) + "px", "overflow": "auto"});


				});
				$(window).trigger("resize");

			});
</script>