<?php
require_once("../../module/autoloader.php");
$today = time();
$me = new EMP1_HELPER();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$page_type = isset($_REQUEST['page']) ? $_REQUEST['page'] : "onscreen";

$table = $_REQUEST['t'];
$name = $_REQUEST['n'];
$type = $_REQUEST['y'];
$extra_info = "";
	switch($type) {
		case "MASTER":
			$class = "ASSIST_MASTER_" . strtoupper($table);
			$processObject = new $class();
			break;
		case "MULTILIST":
		case "LIST":
			$processObject = new EMP1_LIST($table);
			break;
		case "EMPLOYEE":
			$processObject = new EMP1_EMPLOYEE();
			break;
	}
	if($type == "MASTER"){
		$valid_items = $processObject->getActiveItemsFormattedForSelect($extra_info);
	}elseif($type == "EMPLOYEE") {
		$select_data = $processObject->getListOfEmployeesFormattedForSelect();
		$valid_items = array('S'=>$processObject->getSelfManagerOption());
		foreach($select_data as $key => $user) {
			$valid_items[$key] = $user ;
		}
	}else{
		$valid_items = $processObject->getActiveListItemsFormattedForSelect($extra_info);
	}


if($page_type=="onscreen"){
	ASSIST_HELPER::echoPageHeader();
	echo "<h1>".$name."</h1>";
	if(count($valid_items)>0) {
		echo "<table>";
		foreach($valid_items as $i => $item) {
			echo "<tr><td>$item</td></tr>";
		}
		echo "</table>";
	} else {
		echo "<p>No valid list items available.</p>";
	}
} else {
	$fdata = "\"".$name."\"\r\n\"\"\r\n";
	foreach($valid_items as $i => $item) {
		$fdata.= "\"".ASSIST_HELPER::decode($item)."\"\r\n";
	}

	//WRITE DATA TO FILE
	$filename = "../../files/".$cmpcode."/".$modref."_".strtolower($table)."_list_".date("Ymd_Hi",$today).".csv";
	$newfilename = strtolower($table)."_list_".date("YmdHis",$today).".csv";
	$file = fopen($filename,"w");
	fwrite($file,$fdata."\n");
	fclose($file);
	//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
	header('Content-type: text/plain');
	header('Content-Disposition: attachment; filename="'.$newfilename.'"');
	readfile($filename);






}
?>