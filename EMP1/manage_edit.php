<?php
$page_section = "MANAGE";
$page_action = "EDIT";
$add_button = false;
$button_label = "|edit|";
$button_icon = "pencil";
$_REQUEST['object_type'] = "EMPLOYEE";
$not_allowed_if_integrated = true;

/**
 *  [SD] AA-643 EMP1 - Add search function to list pages
 *  comment: The values below come from the advance_search.php page
 */

$search_type = isset($_REQUEST['search_type']) ? $_REQUEST['search_type'] :"";
$search_name = isset($_REQUEST['search_name']) ? $_REQUEST['search_name'] :"";

if($search_type == "advanced") { //confirmation that data is coming from the advanced search page
	// if confirmed, stored $_REQUEST values so that they can be properly filtered according to the assist_***_emp_setup_headings table
	foreach($_REQUEST as $field => $val) {
		$_REQUEST['search_data'][$field] = $val;
	}
}
include("common/generic_list_page.php");


?>
