<?php
require_once("inc_header.php");
if(!isset($_REQUEST['documentsort'])) {
	$_REQUEST['documentsort'] = "doc_id";
}
$docSort = $_REQUEST['documentsort'];
if(empty($docSort)) {
	if($_REQUEST['r[]'] != "ok" || !isset($_REQUEST['r[]'])) { //if page is not redirected clear storage
		echo "<script>localStorage.clear();</script>";//clearing storage used for tab tracking -----PLEASE DO NOT REMOVE
	}
}
$is_edit_page = true;
$object_id = $_REQUEST['object_id'];
$remove_tabs = false;
$page_redirect_self = "manage_edit_object.php?a=reload";
foreach($_REQUEST as $key => $r) {
	$page_redirect_self .= "&".$key."=".$r;
}

include("common/form_object.php");


?>