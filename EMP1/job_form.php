<?php
$object_id = isset($_REQUEST['object_id']) ? $_REQUEST['object_id'] : '';
$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "dialog";
if($display_type == "dialog") {
	$no_page_heading = true;
	$page_redirect_path = "dialog";
} else {
	$page_redirect_path = "manage_edit_object.php?object_id=".$object_id."&";
}
include("inc_header.php");

include("common/add_job_form.php");