<?php


class EMP1_INTERNAL extends EMP1 {

	private $modref;

	public function __construct($modref) {
		parent::__construct($modref);
		$this->modref = $modref;
	}

	public function getAllCurrentEmployeesFormattedForSelect() {
		return array();
	}

	public function getAllCurrentEmployeesWithStatusAndJobs() {
		$sql = "SELECT emp_id
				, job_id
				, job_start
				, job_end
				, tkid
				, CONCAT(tkname,' ',tksurname) as emp_name
				, IF(emp_status & ".EMP1::ASSIST_USER." = ".EMP1::ASSIST_USER.",1,0) as is_system_user
				FROM ".$this->getDBRef()."_employee
				INNER JOIN ".$this->getDBRef()."_job ON job_empid = emp_id
				INNER JOIN assist_".$this->getCmpCode()."_timekeep ON tkid = emp_tkid
				WHERE tkstatus > 0 AND (emp_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
				ORDER BY tkname, tksurname";
		$data = $this->mysql_fetch_all_by_id2($sql, "tkid", "job_id");
		return $data;
	}

	public function getCurrentEmployeesWithStatusAndCurrentJobDetails() {
		$sql = "SELECT emp.emp_id
				, tk.tkid
				, CONCAT(tk.tkname,' ',tk.tksurname) as emp_name
				, IF(emp.emp_status & ".EMP1::ASSIST_USER." = ".EMP1::ASSIST_USER.",1,0) as is_system_user
				, job.*
				FROM ".$this->getDBRef()."_employee emp
				INNER JOIN assist_".$this->getCmpCode()."_timekeep tk ON tkid = emp_tkid
				LEFT OUTER JOIN ".$this->getDBRef()."_job job ON job_empid = emp_id
				AND (job.job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE." 
				AND (job.job_status & ".EMP1::JOB_CURRENT_CONTRACT.") = ".EMP1::JOB_CURRENT_CONTRACT."
				WHERE tkstatus > 0 AND (emp_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
				ORDER BY tkname, tksurname";
		$data = $this->mysql_fetch_all_by_id($sql, "tkid");
		return $data;
	}

	public function getListOfJobs($job_ids) {
		$jobObject = new EMP1_JOB(0, $this->modref);
		return $jobObject->getJobsByID($job_ids);
	}


	public function getJobDetail($job_id) {
		$jobObject = new EMP1_JOB($job_id, $this->modref);
		return $jobObject->getJobDetail($job_id);
	}

	public function getJobsForFinancialYearByUserID($start_date, $end_date, $tkid) {
		$end_date = strtotime($end_date." 23:59:59");
		$start_date = strtotime($start_date." 00:00:00");
		$empObject = new EMP1_EMPLOYEE(0, $this->modref);
		$employee = $empObject->getRawEmployeeDetailsByUserID($tkid, false);
		$emp_id = isset($employee[$empObject->getIDFieldName()]) ? $employee[$empObject->getIDFieldName()] : false; 	//catch situations where person might be user but not employee [AA-510] JC
		if($emp_id!==false) {
			$jobObject = new EMP1_JOB(0, $this->modref);
			$rows = $jobObject->getJobFullDetailByEmployeeID($emp_id);
			unset($jobObject);
			$jobs = $this->filterJobsForSpecificFinancialYear($rows, $start_date, $end_date);
		} else {
			$jobs = false;
		}
		return $jobs;
	}

	public function filterJobsForSpecificFinancialYear($rows, $start_date, $end_date) {
		//assumes start & end date of financial year is in UNIX timestamp format - double check and convert if necessary
		if(strpos($start_date, "-") !== false) {
			$start_date = strtotime($start_date);
		}
		if(strpos($end_date, "-") !== false) {
			$end_date = strtotime($end_date);
		}
		$jobs = array();
		foreach($rows as $row_id => $row) {
			//only take jobs which are applicable to the financial year
			//job_start <= fin_year_end AND
			//job_end = 0/1970 OR job_end >= fin_year_start
			$job_start = strtotime($row['job_start']." 00:00:00");
			if($row['job_end'] == "0000-00-00" || $row['job_end'] == "1970-01-01") {
				$job_end = false;
			} else {
				$job_end = strtotime($row['job_end']." 23:59:59");
			}
			if($job_start <= $end_date) {
				if($job_end === false || $job_end >= $start_date) {
					$job_sort_id = $job_start;
					while(isset($jobs[$job_sort_id])) {
						$job_sort_id++;
					}
					$jobs[$job_sort_id] = $row;
				}
			}
		}
		ksort($jobs);
		return $jobs;
	}

	public function getMyStaffJobs($type) {
		$data = array(
			'job_title' => $this->getUnspecified(),
			'staff_job_ids' => array(),
		);
		$empObject = new EMP1_EMPLOYEE(0, $this->modref);
		$my_details = $empObject->getRawEmployeeDetails();
		$jobObject = new EMP1_JOB(0, $this->modref);
		$titles = array();
		$listObject = new EMP1_LIST("jobtitle", $this->modref);
		if($type == "mine") {
			$field = "job_title";
		} else {
			$field = "job_acting";
		}
		if($my_details[$field] != 0) {
			$title_id = $my_details[$field];
			$data['staff_job_ids'] = $jobObject->getJobIDsByManager($my_details[$field]);
		}

		//get name of job titles for display purposes
		$title = $listObject->getAListItemName($title_id);
		$data['job_title'] = $title;

		return $data;
	}

	public function getAllStaffWithJobsForCurrentFinancialYear($start_date, $end_date, $filter="") {
		$empObject = new EMP1_EMPLOYEE(0, $this->modref);
		$jobObject = new EMP1_JOB(0, $this->modref);

		$data = $jobObject->getJobFullDetails($filter, true);

		$data = $this->filterJobsForSpecificFinancialYear($data, $start_date, $end_date);
		return $data;
	}

	public function getDirectStaffDetailsByUserID($tkid, $start_date, $end_date, $active_users = true, $non_users = true) {
		//1 Get my details
		$my_jobs_for_fin_year = $this->getJobsForFinancialYearByUserID($start_date, $end_date, $tkid);
//echo "<hr /><h1 class='red'>Helllloooo $tkid from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";

		if(!is_array($my_jobs_for_fin_year) || count($my_jobs_for_fin_year) == 0) {
			return array();
		} else {
			//TODO - getDirectStaff - consider how to handle multiple jobs in a year? AA-404 JC 12 July 2020
			//assume last job (if more than one)
			$keys = array_keys($my_jobs_for_fin_year);
			$row_id = $keys[count($keys) - 1];
			$my_job = $my_jobs_for_fin_year[$row_id];
			$my_job_title = $my_job['job_title_id'];
			//2 Check if I'm the only user of the job title
			$empObject = new EMP1_EMPLOYEE(0, $this->modref);
			$jobObject = new EMP1_JOB(0, $this->modref);
			$job_id = $my_job[$jobObject->getIDFieldName()];
			$filter = "|J|.".$jobObject->getTableField()."_title = ".$my_job_title." AND |J|.".$jobObject->getIDFieldName()." <> ".$job_id;
			$all_jobs = $jobObject->getJobFullDetails($filter);
			$end_date = strtotime($end_date." 23:59:59");    //convert end_date to timestamp - filterJobsForSpecificFinancialYear assumes all dates are timestamps
			if(count($all_jobs) > 0) {
				$all_jobs_filter_for_fin_year = $this->filterJobsForSpecificFinancialYear($all_jobs, $start_date, $end_date);
			} else {
				$all_jobs_filter_for_fin_year = array();
			}
			if(count($all_jobs_filter_for_fin_year) > 0) {
				$job_title_filter = false;
			} else {
				$job_title_filter = true;
			}
			//3 get employees where I'm the manager or (my job title is the manager + no manager set OR manager is not active user)
			/**
			 * (
			 *    job.job_manager = 'my_user_id' OR
			 *    (job.job_manager_title = my_job_title AND
			 *        (job.job_manager = ''
			 *            OR job.job_manager NOT IN (
			 *                SELECT emp_tkid FROM dbref()_employee INNER JOIN assist_cc_timekeep ON emp_tkid = tkid AND tkstatus = 1 WHERE ((emp_status & ACTIVE) = ACTIVE) AND ((emp_status & USER) = USER)
			 *            )
			 *        )
			 *    )
			 * )
			 */
			$job_table_field = $jobObject->getTableField();
			$emp_table_field = $empObject->getTableField();
			$user_status_filter = "";
			//WARNING!  Requires that employee info be included so that employee table is referenced
			if($active_users === true && $non_users === true) {
				//no filter needed as all users are then included
			} elseif($active_users === true) {
				$user_status_filter = "((|EMP|.".$emp_table_field."_status & ".EMP1::ASSIST_USER.") = ".EMP1::ASSIST_USER.") AND ";
			} else {
				//assume non_user==true - filter for anyone without ASSIST_USER status
				$user_status_filter = "((|EMP|.".$emp_table_field."_status & ".EMP1::ASSIST_USER.") <> ".EMP1::ASSIST_USER.") AND ";
			}
			$filter = $user_status_filter." (|J|.".$job_table_field."_manager = '".$tkid."'";
			if($job_title_filter) {
				$filter .= " OR (
							|J|.".$job_table_field."_manager_title = $my_job_title 
							AND (
								|J|.".$job_table_field."_manager = ''
								OR |J|.".$job_table_field."_manager NOT IN (
									SELECT ".$emp_table_field."_tkid FROM ".$empObject->getTableName()." 
									INNER JOIN ".$empObject->getUserTableName()." ON ".$emp_table_field."_tkid = tkid AND tkstatus = 1 WHERE ((".$emp_table_field."_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE.") AND ((".$emp_table_field."_status & ".EMP1::ASSIST_USER.") = ".EMP1::ASSIST_USER.")
								)
							)
						)";
			}
			$filter .= ")";
			//WARNING - Must include employee_info just in case user_status_filter is needed as this requires the employee table - function should auto include if EMP or TK is found but just in case
			$staff_jobs = $jobObject->getJobFullDetails($filter, true);

			$data = array('jobs' => array(), 'employee_tkids' => array());
//				echo "<hr /><h1 class='red'>Found staff jobs ".count($staff_jobs)."  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
			if(count($staff_jobs) > 0) {

				$staff_jobs_filter_for_fin_year = $this->filterJobsForSpecificFinancialYear($staff_jobs, $start_date, $end_date);
				if(count($staff_jobs_filter_for_fin_year)) {
//				echo "<hr /><h1 class='red'>Filtered staff jobs ".count($staff_jobs_filter_for_fin_year)."  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
					$data['jobs'] = $staff_jobs_filter_for_fin_year;
					foreach($staff_jobs_filter_for_fin_year as $job_id => $job) {
						$data['employee_tkids'][$job['user_id']] = $job_id;
					}
				}
			}
			return $data;
		}
	}


	/*
	public function getFilters($include_blanks=false) {
		$filters = array();
		$parentObject = new EMP1_FUNCTION(0,$this->modref);
		$parent_objects = $parentObject->getOrderedObjects();
		if(count($parent_objects)>0) {
			$childObject = new EMP1_SUBFUNCTION(0,$this->modref);
			$child_objects = $childObject->getOrderedObjects(array_keys($parent_objects));

			if(count($child_objects)>0) {
				$activityObject = new EMP1_ACTIVITY(0,$this->modref);
				$activity_objects = $activityObject->getOrderedObjects(array_keys($parent_objects),0,$this->modref);
			}
			foreach($parent_objects as $p_id => $p) {
				if(isset($child_objects[$p_id]) && count($child_objects[$p_id])) {
					$f = $p['name'].": ";
					foreach($child_objects[$p_id] as $c_id => $c) {
						if((isset($activity_objects[$c_id]) && count($activity_objects[$c_id])>0) || $include_blanks) {
							$count = isset($activity_objects[$c_id]) ? count($activity_objects[$c_id]) : 0;
							$filters[$c_id] = $f.$c['name']." (".$count." record".($count!=1?"s":"").")";
						}
					}
				}
			}
		}
		return $filters;
	}






	public function getLinesByFilter($filter) {
		$data = array(
			'head'=>$this->getHeadings(),
			'rows'=>$this->getRows($filter),
		);
		return $data;
	}



	public function getLinesByID($id) {
		$data = array(
			'head'=>$this->getHeadings(),
			'rows'=>$this->getRows(0,$id),
		);
		return $data;
	}




	public function getHeadings() {
		$headObject = new EMP1_HEADINGS($this->modref);
		$headings = $headObject->getHeadingsForInternal();

		$data = array();
		foreach($headings as $fld => $h) {
			$data[$fld] = $h['name'];
		}
		return $data;
	}



	public function getRows($filter=0,$id=array()) {
		$parentObject = new EMP1_FUNCTION(0,$this->modref);
		if($this->checkIntRef($filter)) {
			$children = $parentObject->getOrderedObjectsByChild($filter);
		} else {
			$children = $parentObject->getOrderedObjectsByActivity($id);
		}
		return $children;
	}
	*/

	public function getAllDepartmentsFormattedForSelect() {
		$list_items = $this->getAllDepartments();
		return $this->listItemsFormattedForSelect($list_items);

	}

	public function getAllJobLevelsFormattedForSelect() {
		$list_items = $this->getAllJobLevels();
		return $this->listItemsFormattedForSelect($list_items);

	}

	public function getAllJobTitlesFormattedForSelect() {
		$list_items = $this->getAllJobTitles();
		return $this->listItemsFormattedForSelect($list_items);

	}

	public function getAllJobTitles() {
		$list_name = 'job_title';
		return $this->getListItems($list_name);
	}

	public function getAllDepartments() {
		$list_name = 'department';
		return $this->getListItems($list_name);
	}

	public function getAllJobLevels() {
		$list_name = 'job_level';
		return $this->getListItems($list_name);
	}

	private function getListItems($list_name) {
		$listObject = new EMP1_LIST($list_name, $this->modref);
		$list_items = $listObject->getActiveListItems();
		return $list_items;
	}

	private function listItemsFormattedForSelect($list_items) {
		$list_items_formatted_for_select = array();
		foreach($list_items as $key => $val) {
			$list_items_formatted_for_select[$key] = $val['name'];
		}

		return $list_items_formatted_for_select;
	}

	public function getEmployee($emp_id) {
		$empObject = new EMP1_EMPLOYEE(0, $this->modref);
		return $empObject->getRawEmployeeDetailsByEmployeeID($emp_id);
	}

	public function __destruct() {
		parent::__destruct();
	}

}


?>