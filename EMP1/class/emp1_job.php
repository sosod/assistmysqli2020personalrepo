<?php
/**
 * To manage the JOB object
 *
 * Created on: January 2018
 * Authors: Janet Currie
 *
 */

class EMP1_JOB extends EMP1 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_empid";
	protected $name_field = "_description";
	protected $attachment_field = "_attachment";

	protected $has_attachment = false;

	protected $ref_tag = "JOB";
	private $modref;
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "JOB";
	const OBJECT_NAME = "job";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "job";
	const TABLE_FLD = "job";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

	public function __construct($obj_id = 0, $modref = "") {
		$this->modref = $modref;
		parent::__construct($modref);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		//$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		//if($obj_id>0) {
		//	$this->object_id = $obj_id;
		//	$this->object_details = $this->getAObject($obj_id);
		//}
		$this->object_form_extra_js = "";
	}


	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_NAME;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}


	public function getRawObject($obj_id, $format_date = true) {
		if(is_array($obj_id)) {
			if(isset($obj_id['obj_id'])) {
				$obj_id = $obj_id['obj_id'];
			} else {
				return array();
			}
		}
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$row = $this->mysql_fetch_one($sql);
		if($format_date) {
			$row['job_start'] = date("d-M-Y", strtotime($row['job_start']));
			$row['job_end'] = $row['job_end'] != "0000-00-00" ? date("d-M-Y", strtotime($row['job_end'])) : $row['job_end'];
		}
		return $row;
	}


	public function getJobDetail($job_id = false) {
		if($job_id === false || (is_array($job_id) && count($job_id) == 0)) {
			return array();
		} else {
			$sql = "SELECT job_id
					, job_title.name as job_title
					, tk.tkid as job_manager_tkid
					, tk.tkname as job_manager_name
					, tk.tksurname as job_manager_surname
					, job_manager_title.name as job_manager_title
					, job_manager_title.id as job_manager_title_id
					, job_dept.name as job_dept
					, job_start
					FROM ".$this->getTableName()." job
					LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle job_title
					  ON job.job_title = job_title.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle job_manager_title
					  ON job.job_manager_title = job_manager_title.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_department job_dept
					  ON job.job_dept = job_dept.id
					LEFT OUTER JOIN ".$this->getUserTableName()." tk
					  ON tk.tkid = job.job_manager
					WHERE ".$this->getActiveStatusSQL("job");
			if(is_array($job_id)) {
				$sql .= " AND job.".$this->getIDFieldName()." IN (".implode(",", $job_id).")";
			} else {
				$sql .= " AND job.".$this->getIDFieldName()." = $job_id ";
			}
			$sql .= " ORDER BY job_start DESC, job_id DESC ";
			$final_data = $this->mysql_fetch_one($sql);
			if(is_null($final_data['job_manager_tkid']) || empty($final_data['job_manager_tkid']) || strlen($final_data['job_manager_tkid']) == 0) {
				//manager not explicitly set so get based on job title
				$manager_job_title = $final_data['job_manager_title_id'];
				//#AA-617 - PM5 Create process breaks if no manager job title is set [JC] 21 May 2021
				if(!is_null($manager_job_title) && !empty($manager_job_title) && strlen($manager_job_title)>0 && $manager_job_title!==0 ) {
					$sql = "SELECT TK.tkid, TK.tkname, TK.tksurname
						FROM ".$this->getTableName()." J
						INNER JOIN ".$this->getDBRef()."_employee E
						ON J.".$this->getParentFieldName()." = E.emp_id
						INNER JOIN ".$this->getUserTableName()." TK
						ON E.emp_tkid = TK.tkid
						WHERE job_title = ".$manager_job_title." 
						AND ((job_status & ".EMP1::JOB_CURRENT_CONTRACT.") = ".EMP1::JOB_CURRENT_CONTRACT.")
						AND ((job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE.")
						AND ((emp_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE.")
						AND TK.tkstatus = 1
						ORDER BY job_start ASC
						";
					$man = $this->mysql_fetch_one($sql);
					$final_data['job_manager_tkid'] = $man['tkid'];
					$final_data['job_manager_name'] = $man['tkname'];
					$final_data['job_manager_surname'] = $man['tksurname'];
				} else {
					$final_data['job_manager_tkid'] = false;
					$final_data['job_manager_name'] = "Self / No Manager";
					$final_data['job_manager_surname'] = "";
				}
			}
			return $final_data;
		}
	}


	public function getJobFullDetailByEmployeeID($emp_id = false) {
		if($emp_id === false || (is_array($emp_id) && count(ASSIST_HELPER::removeBlanksFromArray($emp_id)) == 0)) {
			return array();
		} else {
			$filter = "";
			if(is_array($emp_id)) {
				$filter .= "|J|.".$this->getParentFieldName()." IN (".implode(",", $emp_id).")";
			} else {
				$filter .= "|J|.".$this->getParentFieldName()." = $emp_id ";
			}
			return $this->getJobFullDetails($filter);
		}
	}

	public function getJobFullDetails($filter = "", $include_employee_info = false) {
		$filter_replace = array(
			'|J|' => "job",
			'|JT|' => "job_title",
			'|D|' => "job_dept",
			'|L|' => "job_loc",
			'|AA|' => "job_acting",
			'|EMP|' => "emp",
			'|TK|' => "tk",
		);
		foreach($filter_replace as $search => $replace) {
			//IF |EMP| or |TK| is found in the filter then force the inclusion of Employee info so that the employee/timekeep tables are included
			if(($replace == "emp" || $replace == "tk") && strpos($filter, $search) !== false) {
				$include_employee_info = true;
			}
			$filter = str_replace($search, $replace, $filter);
		}

		$sql = "SELECT job_id
					, CONCAT('".$this->getRefTag()."',job_id) as ref
				, job.job_empid as emp_id
					, job_title.name as job_title
				, job.job_title as job_title_id
					, job_manager_title.name as job_manager_title
					, job_acting.name as job_acting
					, job_dept.name as job_dept
					, job_loc.name as job_loc
					, job_level.name as job_level
					, job_skill.name as job_skill
					, job_team.name as job_team
					, job_leveltwo.name as job_leveltwo
					, CONCAT(job_manager.tkname, ' ', job_manager.tksurname) as job_manager
					, job_start
					, job_end
					, job_description
					, job_score
					, job_status
					, IF( ((job_status & ".EMP1::JOB_CURRENT_CONTRACT.") = ".EMP1::JOB_CURRENT_CONTRACT."),1,0) as is_current_contract
				, IF( ((job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."),1,0) as is_active";
		if($include_employee_info) {
			$sql .= ", emp.emp_tkid as user_id, CONCAT(tk.tkname, ' ', tk.tksurname) as user_name";
			$sql .= ", IF((emp.emp_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE.",1,0) as employee_status";
			$extra_table = "
				INNER JOIN ".$this->getDBRef()."_employee emp ON job.job_empid = emp.emp_id
				INNER JOIN ".$this->getUserTableName()." tk ON emp.emp_tkid = tk.tkid
			";
		}
		$sql .= "
					FROM ".$this->getTableName()." job
					LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle job_acting
					  ON job.job_acting = job_acting.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle job_title
					  ON job.job_title = job_title.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle job_manager_title
					  ON job.job_manager_title = job_manager_title.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_department job_dept
					  ON job.job_dept = job_dept.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_location job_loc
					  ON job.job_loc = job_loc.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_level job_level
					  ON job.job_level = job_level.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_skill job_skill
					  ON job.job_skill = job_skill.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_team job_team
					  ON job.job_team = job_team.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_leveltwo job_leveltwo
					  ON job.job_leveltwo = job_leveltwo.id
					LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep job_manager
					  ON job.job_manager = job_manager.tkid
				".(isset($extra_table) ? $extra_table : "")."
				WHERE ".$this->getActiveStatusSQL("job")
			.(strlen($filter) > 0 ? " AND ".$filter : "");
		$sql .= " ORDER BY job_start DESC, job_status ASC, job_id DESC ";    //include status sort so that current job status always sorts top
		$final_data = $this->mysql_fetch_all_by_id($sql, "job_id");
		return $final_data;
	}


	/**
	 * Get job ids related to specific manager job title
	 * Required by INTERNAL class
	 */
	public function getJobIDsByManager($manager_id) {
		if($manager_id === false || $manager_id == 0 || strlen($manager_id) == 0 || (is_array($manager_id) && count(ASSIST_HELPER::removeBlanksFromArray($manager_id)) == 0)) {
			return array();
		} else {
			$data = array();
			$empObject = new EMP1_EMPLOYEE(0, $this->modref);
			$sql = "SELECT
						job.job_id
					FROM ".$this->getTableName()." job
					INNER JOIN ".$empObject->getTableName()." E
					  ON job.".$this->getParentFieldName()." = E.".$empObject->getIDFieldName()."
					WHERE ".$this->getActiveStatusSQL("job")." AND ".$empObject->getActiveStatusSQL("E");
			if(is_array($manager_id)) {
				$sql .= " AND job.".$this->getTableField()."_manager_title IN (".implode(",", ASSIST_HELPER::removeBlanksFromArray($manager_id)).")";
			} else {
				$sql .= " AND job.".$this->getTableField()."_manager_title = $manager_id ";
			}
			$sql .= "";
			$data = $this->mysql_fetch_all_by_value($sql, "job_id");
			return $data;
		}
	}




	/*******************************************************
	 * Functions to replace old functions as per the note below [JC - 14 August 2018]
	 */
	//add JOB for new employee - don't worry about job change reason & end date of previous job
	public function addObjectForNewEmployee($var, $headings = array(), $log = true) {
		//get headings to be populated
		if(count($headings) > 0) {
			$headObject = new EMP1_HEADINGS();
			$headings = $headObject->getMainObjectHeadings("JOB");
		}
		$job_header = $headings['rows']['job_heading'];
		$headings = $headings['sub'][$job_header['id']];

		//set default fields - assume status of job is current contract
		$insert_data = array(
			self::TABLE_FLD.'_empid' => $var['emp_id'],
			self::TABLE_FLD.'_end' => "0000-00-00",                //job_end = 0 as it is assumed to be a current contract & field should not be present in add form
			self::TABLE_FLD.'_status' => EMP1::ACTIVE + EMP1::JOB_CURRENT_CONTRACT,
			self::TABLE_FLD.'_insertdate' => date("Y-m-d H:i:s"),
			self::TABLE_FLD.'_insertuser' => $this->getUserID(),
			self::TABLE_FLD.'_start' => date("Y-m-d H:i:s"),
		);

		//get fields
		foreach($headings as $h_id => $head) {
			$fld = $head['field'];
			if(!isset($insert_data[$fld])) {
				if(isset($var[$fld])) {
					$v = $var[$fld];
					switch($head['type']) {
						case "DATE":
							if(strlen($v) > 0) {
								$v = date("Y-m-d", strtotime($v));
							}
							break;
					}
				} else {
					$v = 0;
				}
				$insert_data[$fld] = $v;
			}
		}
		//save
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
		$job_id = $this->db_insert($sql);
		if($job_id !== false && !is_null($job_id) && $job_id > 0) {
			//save log (not needed if adding new job for edited employee)
			if($log) {
				//LOG CREATION!!!!
				$changes = array(
					'response' => "New |".self::OBJECT_NAME."| ".$this->getRefTag().$job_id." created.",
					'user' => $this->getUserName(),
				);
				$log_var = array(
					'object_id' => $job_id,
					'emp_id' => $var['emp_id'],
					'object_type' => $this->getMyObjectType(),
					'changes' => $changes,
					'log_type' => EMP1_LOG::CREATE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			}
			$result = array('result' => true, 'comment' => "Adding Job successful.", 'object_id' => $job_id);
		} else {
			$result = array('result' => false, 'comment' => "Adding Job failed.");
		}
		return $result;
	}

	//edit an existing job record
	public function editObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		unset($var['form_action']);
		$job_id = $var['job_id'];
		unset($var['job_id']);
		$emp_id = $var['job_empid'];
		unset($var['job_empid']);

		$insert_data = array();

		$original = $this->getRawObject($job_id);
		if(($original['job_status'] & EMP1::JOB_CURRENT_CONTRACT) == EMP1::JOB_CURRENT_CONTRACT) {
			$is_current = true;
		} else {
			$is_current = false;
		}
		//create copy to use for updating user table if required
		$new_record = $original;

		//ASSIST_HELPER::arrPrint($var);
		//ASSIST_HELPER::arrPrint($original);

		//PROCESS - JC 16 August 2018
		$job_start_changed = false;
		//1. check that end > start - if not then make end = start as end must fall on a later date than start
		if(isset($var['job_end'])) {
			if($is_current) {
				//unset job_end because not applicable to current contract
				unset($var['job_end']);
			} else {
				//check that end > start
				if(strtotime($var['job_end']) > strtotime($var['job_start'])) {
					//do nothing - end is correctly after the start
				} else {
					$var['job_end'] = $var['job_start'];
				}
			}

		}
		//2. check all fields for changes
		foreach($var as $fld => $v) {
			if($v != $original[$fld]) {
				if($fld == "job_start" || $fld == "job_end") {
					$v = date("Y-m-d", strtotime($v));
					//make a note if the start date changed so that the status can be checked
					if($fld == "job_start") {
						$job_start_changed = true;
					}
				}
				//A_H code not required as the data is passed via AssistForm.serialize() js function which automatically codes
				//Issue spotted when testing AA-444 as job_description was being coded twice before saving to the DB [JC] 13 July 2020
				$insert_data[$fld] = $v;//ASSIST_HELPER::code($v);
				$new_record[$fld] = $v;//ASSIST_HELPER::code($v);
			}
		}
		//3. if start date changed
		$end_previous_current_contract = false;
		$make_new_current = false;
		if($job_start_changed === true) {
			$job_start = strtotime($var['job_start']);
			$make_prev_new_current = false;
			//3.1 if not current contract:
			if($is_current !== true) {
				//a. get current contract
				$current_contract_record = $this->getLatestJobsPerEmployee($emp_id);
				$current_contract_record = $current_contract_record[$emp_id];
				//b. check start vs current start
				$current_start = strtotime($current_contract_record['job_start']);
				//c. if start > current start
				if($job_start > $current_start) {
					//i.  end current
					$end_previous_current_contract = true;
					$this->endContract($emp_id, $current_contract_record['job_id'], date("d F Y", $job_start));
					//ii. change edited job to current & delete end date
					$insert_data['job_status'] = EMP1::ACTIVE + EMP1::JOB_CURRENT_CONTRACT;
					$insert_data['job_end'] = "0000-00-00";
				}
			} else {
				//3.2. if is current contract
				//a. check for other records with later start dates
				$sql = "SELECT *
					FROM ".$this->getTableName()."
					WHERE job_start > '".date("Y-m-d", $job_start)."'
					AND (job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
					AND ".$this->getParentFieldName()." = ".$emp_id."
					AND ".$this->getIDFieldName()." <> ".$job_id."
					ORDER BY job_start ASC";
				$count = $this->db_get_num_rows($sql);
				if($count == 0) {
					//do nothing because no jobs exist with a later start date so this job remains the current contract
				} else {
					$prev = $this->mysql_fetch_one($sql);
					$prev_start = $prev['job_start'];
					//i. end edited job and set end date to prev start - 1
					$insert_data['job_status'] = EMP1::ACTIVE + EMP1::JOB_ENDED_CONTRACT;
					$insert_data['job_end'] = date("Y-m-d", strtotime($prev_start) - 12 * 60 * 60);
					//ii.  find last contract to make new current
					$sql = "SELECT *
						FROM ".$this->getTableName()."
						WHERE job_start > '".date("Y-m-d", $job_start)."'
						AND (job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
						AND ".$this->getParentFieldName()." = ".$emp_id."
						AND ".$this->getIDFieldName()." <> ".$job_id."
						ORDER BY job_start DESC";
					$new_current = $this->mysql_fetch_one($sql);
					if(isset($new_current['job_id'])) {
						$new_record = $new_current;
						$make_new_current = true;
						$this->restartContract($emp_id, $new_current['job_id']);
					}
				}
			}
		}


		if(count($insert_data) > 0) {
			//if there are changes then save to the database
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$job_id." AND ".$this->getParentFieldName()." = ".$emp_id;
			$mar = $this->db_update($sql);
			//log the changes
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$job_id." edited.",
				'user' => $this->getUserName(),
			);
			foreach($insert_data as $fld => $new_raw) {
				if($fld == "job_status") {
					if(($new_raw & EMP1::JOB_ENDED_CONTRACT) == EMP1::JOB_ENDED_CONTRACT) {
						$changes['log_display_comment'] = "Job status changed to 'Ended' due to change in start date";
					} else {
						$changes['log_display_comment'] = "Job status changed to 'Current' due to change in start date";
					}
				} elseif($fld == "job_start") {
					$new = date("d-M-Y", strtotime($new_raw));
					$old = date("d-M-Y", strtotime($original[$fld]));
					$changes[$fld] = array(
						'to' => $new,
						'from' => $old,
						'raw' => array(
							'to' => $new_raw,
							'from' => $original[$fld]
						)
					);
				} elseif($fld == "job_end") {
					$new = $new_raw == "0000-00-00" ? $this->getUnspecified() : date("d-M-Y", strtotime($new_raw));
					$old = $original[$fld] == "0000-00-00" ? $this->getUnspecified() : date("d-M-Y", strtotime($original[$fld]));
					$changes[$fld] = array(
						'to' => $new,
						'from' => $old,
						'raw' => array(
							'to' => $new_raw,
							'from' => $original[$fld]
						)
					);
				} elseif($fld == "job_description" || $fld == "job_score") {
					$changes[$fld] = array(
						'to' => $new_raw,
						'from' => $original[$fld],
						'raw' => array(
							'to' => $new_raw,
							'from' => $original[$fld]
						)
					);
				} elseif($fld == "job_manager") {
					$old_raw = $original[$fld];
					$new = $new_raw == 0 || strlen($new_raw) < 4 ? $this->getUnspecified() : $this->getAUserName($new_raw);
					$old = $old_raw == 0 || strlen($old_raw) < 4 ? $this->getUnspecified() : $this->getAUserName($old_raw);
					$changes[$fld] = array(
						'to' => $new,
						'from' => $old,
						'raw' => array(
							'to' => $new_raw,
							'from' => $old_raw
						)
					);
				} else {
					$old_raw = $original[$fld];
					$listObject = new EMP1_LIST($fld);
					$items = $listObject->getAListItemName(array($new_raw, $old_raw));
					$new = isset($items[$new_raw]) && $new_raw != 0 ? $items[$new_raw] : $this->getUnspecified();
					$old = isset($items[$old_raw]) && $old_raw != 0 ? $items[$old_raw] : $this->getUnspecified();

					$changes[$fld] = array(
						'to' => $new,
						'from' => $old,
						'raw' => array(
							'to' => $new_raw,
							'from' => $old_raw
						)
					);
				}
			}
			$log_var = array(
				'object_id' => $job_id,
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			//if changes were made to the previous current job then log the changes & update employee data in user table
			if($end_previous_current_contract == true) {
				$changes = array(
					'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$current_contract_record['job_id']." ended due to change of start date for |".self::OBJECT_NAME."| ".$this->getRefTag().$job_id.".",
					'user' => $this->getUserName(),
				);
				$log_var = array(
					'object_id' => $current_contract_record['job_id'],
					'emp_id' => $emp_id,
					'object_type' => $this->getMyObjectType(),
					'changes' => $changes,
					'log_type' => EMP1_LOG::INACTIVE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
				//if job is new current then send changes to timekeep table for old PMSPS
				$empObject = new EMP1_EMPLOYEE();
				$emp_details = $empObject->getRawEmployeeDetailsByEmployeeID($emp_id);
				$tkid = $emp_details['emp_tkid'];
				$empObject->sendEmployeeDataToUsersTable($tkid, $new_record);
			}
			if($make_new_current == true) {
				$changes = array(
					'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$new_current['job_id']." set as new current |".self::OBJECT_NAME."| due to change of start date for |".self::OBJECT_NAME."| ".$this->getRefTag().$job_id.".",
					'user' => $this->getUserName(),
					'job_end' => array(
						'to' => $this->getUnspecified(),
						'from' => date("d-M-Y", strtotime($new_current['job_end'])),
						'raw' => array(
							'to' => "0000-00-00",
							'from' => $new_current['job_end'],
						)
					)
				);
				$log_var = array(
					'object_id' => $new_current['job_id'],
					'emp_id' => $emp_id,
					'object_type' => $this->getMyObjectType(),
					'changes' => $changes,
					'log_type' => EMP1_LOG::INACTIVE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
				//if job is new current then send changes to timekeep table for old PMSPS
				$empObject = new EMP1_EMPLOYEE();
				$emp_details = $empObject->getRawEmployeeDetailsByEmployeeID($emp_id);
				$tkid = $emp_details['emp_tkid'];
				$empObject->sendEmployeeDataToUsersTable($tkid, $new_record);
			}
			if(!$end_previous_current_contract && !$make_new_current && $is_current) {
				//if current has not changed && this is current, then send changes to timkeep table for old PMPS
				$empObject = new EMP1_EMPLOYEE();
				$emp_details = $empObject->getRawEmployeeDetailsByEmployeeID($emp_id);
				$tkid = $emp_details['emp_tkid'];
				//get latest details as updated above
				$new_record = $this->getRawObject($job_id);
				$empObject->sendEmployeeDataToUsersTable($tkid, $new_record);
			}
			//result
			$result = array("ok", "".$this->getObjectName(self::OBJECT_NAME)." ".$this->getRefTag().$job_id." edited successfully.", 'object_id' => $job_id);
		} else {
			$result = array("info", "No changes found to be saved.", 'object_id' => $job_id);
		}
		return $result;

	}

	//new function - add new job record after converting employee
	public function addObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		unset($var['form_action']);
		unset($var['job_id']);
		$emp_id = $var['job_empid'];
		unset($var['job_empid']);
		unset($var['job_end']);
		$add_new_job = !isset($var['new_job']) ? null : $var['new_job']; //$var['new_job'] value comes from add_job_form.php when adding a new job of terminated emp.
		unset($var['new_job']);
		//set default fields
		$insert_data = array(
			self::TABLE_FLD.'_empid' => $emp_id,
			self::TABLE_FLD.'_status' => EMP1::ACTIVE,
			self::TABLE_FLD.'_insertdate' => date("Y-m-d H:i:s"),
			self::TABLE_FLD.'_insertuser' => $this->getUserID(),
		);

		//get current contract record
		if(isset($emp_id) && $add_new_job == null) {
			$current_contract_record = $this->getLatestJobsPerEmployee($emp_id);
			$current_contract_record = $current_contract_record[$emp_id];
		}

		//if latest jobs don't return a job_id then try Recent ended jobs
		if(!isset($current_contract_record['job_id']) || $current_contract_record['job_id'] == null || $current_contract_record['job_id'] == "") {
			$current_contract_record = $this->getRecentEndedJobsPerEmployee($emp_id);
			$current_contract_record = $current_contract_record[$emp_id];
		}

		//ASSIST_HELPER::arrPrint($current_contract_record);
		if(!isset($current_contract_record['job_start'])) {
			$old_job_start = "0000-00-00";
		} else {
			$old_job_start = $current_contract_record['job_start'];
		}
		if(!isset($var['job_start'])) {
			$new_job_start = "0000-00-00";
		} else {
			$new_job_start = $var['job_start'];
		}
		$old_start = strtotime($old_job_start);
		$new_start = strtotime($new_job_start);
		//if new start date falls after old start date, end the old contract
		if(isset($old_job_start) && $new_start > $old_start && $add_new_job == null) {
			$end_previous_contract = true;
			$this->endContract($emp_id, $current_contract_record['job_id'], date("Y-m-d", $new_start));
			$insert_data[self::TABLE_FLD.'_status'] += EMP1::JOB_CURRENT_CONTRACT;
			$insert_data[self::TABLE_FLD.'_end'] = "0000-00-00";
			$prev_end = false;
		} elseif(isset($old_job_start) && $new_start > $old_start && $add_new_job !== null) {
			$end_previous_contract = false;
			//$this->endContract($emp_id,$current_contract_record['job_id'], date("Y-m-d",$new_start));
			$insert_data[self::TABLE_FLD.'_status'] += EMP1::JOB_CURRENT_CONTRACT;
			$insert_data[self::TABLE_FLD.'_end'] = "0000-00-00";
			$prev_end = false;
		} else {
			$end_previous_contract = false;
			$insert_data[self::TABLE_FLD.'_status'] += EMP1::JOB_ENDED_CONTRACT;
			//check if contract exists with same start date - if yes, default new contract end date to same as start date
			$sql = "SELECT *
					FROM ".$this->getTableName()."
					WHERE job_start = '".date("Y-m-d", $new_start)."'
					AND (job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
					AND ".$this->getParentFieldName()." = ".$emp_id."
					ORDER BY job_start ASC";
			$count = $this->db_get_num_rows($sql);
			if($count > 0) {
				$prev_end = false;
				$insert_data[self::TABLE_FLD.'_end'] = $new_start;
			} else {
				//CHECK WHICH END DATE TO USE
				$sql = "SELECT *
						FROM ".$this->getTableName()."
						WHERE job_start > '".date("Y-m-d", $new_start)."'
						AND (job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
						AND ".$this->getParentFieldName()." = ".$emp_id."
						ORDER BY job_start ASC";
				$old = $this->mysql_fetch_one($sql);
				$new_end = date("Y-m-d", strtotime($old['job_start']) - (12 * 60 * 60));
				$insert_data[self::TABLE_FLD.'_end'] = isset($old['job_start']) ? $new_end : "0000-00-00";
				//check if any other contract needs to have its end date changed
				$sql = "SELECT *
						FROM ".$this->getTableName()."
						WHERE (job_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."
						AND ".$this->getParentFieldName()." = ".$emp_id."
						AND job_end = '".$new_end."'
						ORDER BY job_start ASC";
				$prev = $this->mysql_fetch_one($sql);
				if(isset($prev['job_id'])) {
					$prev_end = date("Y-m-d", strtotime($var['job_start']) - (12 * 60 * 60));
					$this->endContract($emp_id, $prev['job_id'], $var['job_start']);
				} else {
					$prev_end = false;
				}
			}
		}

		foreach($var as $fld => $v) {
			if($fld == "job_start" || $fld == "job_end") {
				$v = date("Y-m-d", strtotime($v));
			}
			if(!isset($insert_data[$fld])) {
				$insert_data[$fld] = $v;
			}
		}

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
		$job_id = $this->db_insert($sql);
		$changes = array(
			'response' => "New |".self::OBJECT_NAME."| ".$this->getRefTag().$job_id." created.",
			'user' => $this->getUserName(),
		);
		$log_var = array(
			'object_id' => $job_id,
			'emp_id' => $emp_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => EMP1_LOG::CREATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

		//if changes were made to the previous current job then log the changes
		if($end_previous_contract == true) {
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$current_contract_record['job_id']." ended due to addition of new contract ".$this->getRefTag().$job_id.".",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $current_contract_record['job_id'],
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::INACTIVE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			//if job is new current then send changes to timekeep table for old PMSPS
			$empObject = new EMP1_EMPLOYEE();
			$emp_details = $empObject->getRawEmployeeDetailsByEmployeeID($emp_id);
			$tkid = $emp_details['emp_tkid'];
			$empObject->sendEmployeeDataToUsersTable($tkid, $var);
		}
		//if a previously ended contract had it's end date changed then log changes
		if($prev_end !== false) {
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$prev['job_id']." end date changed due to addition of new contract ".$this->getRefTag().$job_id.".",
				'user' => $this->getUserName(),
				'job_end' => array(
					'to' => $prev_end,
					'from' => $prev['job_end'],
				),
			);
			$log_var = array(
				'object_id' => $prev['job_id'],
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::INACTIVE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		}


		$result = array("ok", $this->getObjectName(self::OBJECT_NAME)." ".$this->getRefTag().$job_id." added successfully.", 'object_id' => $job_id);


		return $result;

	}

	//new function - end previous current contract due to addition of new current job
	public function endContract($emp_id, $job_id, $start_date) {
		//set end date to start date - 12 hours
		$end_date = date("Y-m-d", strtotime($start_date) - (12 * 60 * 60));
		$sql = "UPDATE ".$this->getTableName()." SET
				job_end = '".$end_date."',
				job_status = ".(EMP1::ACTIVE + EMP1::JOB_ENDED_CONTRACT)."
				WHERE ".$this->getIDFieldName()." = ".$job_id;
		$mar = $this->db_update($sql);
	}

	//new function - restart previously ended contract due to edit of start date on current contract
	public function restartContract($emp_id, $job_id) {
		$sql = "UPDATE ".$this->getTableName()." SET
				job_end = '0000-00-00',
				job_status = ".(EMP1::ACTIVE + EMP1::JOB_CURRENT_CONTRACT)."
				WHERE ".$this->getIDFieldName()." = ".$job_id;
		$mar = $this->db_update($sql);
	}


	//new function - delete job record after converting employee
	public function deleteObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		$job_id = $var['job_id'];
		if(ASSIST_HELPER::checkIntRef($job_id)) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$this->getStatusFieldName()." - ".self::ACTIVE." + ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$job_id;
			$mar = $this->db_update($sql);
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$job_id." deleted.",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $job_id,
				'emp_id' => $var['emp_id'],
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			$result = array("ok", $this->replaceAllNames($changes['response']));
		} else {
			$result = array("error", "Invalid ".$this->getObjectName($this->getMyObjectType()));
		}

		return $result;

	}

	//new function - restore job record previously deleted
	public function restoreObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		$job_id = $var['job_id'];
		if(ASSIST_HELPER::checkIntRef($job_id)) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$this->getStatusFieldName()." + ".self::ACTIVE." - ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$job_id;
			$mar = $this->db_update($sql);
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$job_id." restored.",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $job_id,
				'emp_id' => $var['emp_id'],
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::RESTORE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			$result = array("ok", $this->replaceAllNames($changes['response']));
		} else {
			$result = array("error", "Invalid ".$this->getObjectName($this->getMyObjectType()));
		}

		return $result;

	}

	//get the current  contract record for employees
	public function getLatestJobsPerEmployee($emp_id = false) {
		if($emp_id === false || (is_array($emp_id) && count(ASSIST_HELPER::removeBlanksFromArray($emp_id)) == 0)) {
			return array();
		} else {
			//Get active jobs which are marked as current contracts
			$sql = "SELECT * FROM ".$this->getTableName()." job
					WHERE ".$this->getActiveStatusSQL("job")." AND (".$this->getStatusFieldName()." & ".EMP1::JOB_CURRENT_CONTRACT.") = ".EMP1::JOB_CURRENT_CONTRACT;
			if(is_array($emp_id)) {
				$sql .= " AND ".$this->getParentFieldName()." IN (".implode(",", $emp_id).")";
			} elseif($emp_id !== false) {
				$sql .= " AND ".$this->getParentFieldName()." = $emp_id ";
			}
			$sql .= " ORDER BY job_start DESC, job_id DESC ";
			$raw_data = $this->mysql_fetch_all($sql);
			//FINAL DATA TO BE RETURNED IS ALWAYS IN FORMAT array(emp_id=>job_data_in_array)
			$final_data = array();
			if(count($raw_data) > 0) {
				//if no employee id was submitted OR an array that is not empty was submitted
				if($emp_id === false || (is_array($emp_id) && count($emp_id) > 0)) {
					foreach($raw_data as $i => $row) {
						$key = $row[$this->getParentFieldName()];
						if(!isset($final_data[$key])) {
							$final_data[$key] = $row;
						}
					}
					//else if an employee id was submitted and it wasn't a blank array - assume a single integer and return the first row (latest start date)
				} elseif($emp_id !== false && !is_array($emp_id)) {
					$final_data[$emp_id] = $raw_data[0];
				}
			}

			return $final_data;
		}
	}


	public function getJobsByID($job_id = false) {
		if($job_id === false || (is_array($job_id) && count($job_id) == 0)) {
			return array();
		} else {
			$sql = "SELECT job_id, jt.name as job_title, job_start, dept.name as dept
					FROM ".$this->getTableName()." job
					LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle jt
					  ON job_title = jt.id
					LEFT OUTER JOIN ".$this->getDBRef()."_list_department dept
					  ON job_dept = dept.id
					WHERE ".$this->getActiveStatusSQL("job");
			if(is_array($job_id)) {
				$sql .= " AND ".$this->getIDFieldName()." IN (".implode(",", $job_id).")";
			} else {
				$sql .= " AND ".$this->getIDFieldName()." = $job_id ";
			}
			$sql .= " ORDER BY job_start DESC, job_id DESC ";
			$raw_data = $this->mysql_fetch_all_by_id($sql, "job_id");
			$final_data = array();
			foreach($raw_data as $job_id => $job) {
				//AA-581 - Added option to display department if job title is not available so that Individual Performance doesn't end up with random blank cells if JT is blank
				$is_job_title_null = (is_null($job['job_title'])||$job['job_title']==false||strlen($job['job_title'])==0);
				$is_dept_null = (is_null($job['dept'])||$job['dept']==false||strlen($job['dept'])==0);
				if($is_job_title_null) {
					if($is_dept_null) {
						$val = $this->getUnspecified();
					} else {
						$val = $job['dept'];
					}
				} else {
					$val =$job['job_title'];
				}
				if(strlen($job['job_start']) > 0 && $job['job_start'] != "0000-00-00" && date("Y-m-d", strtotime($job['job_start'])) != "1970-01-01") {
					$val .= " (Started on: ".date("d F Y", strtotime($job['job_start'])).")";
				}
				$final_data[$job_id] = $val;
			}
			return $final_data;
		}
	}

	// function 2 (used by Employee termination) get the current  contract record for employees
	public function getLatestJobs($emp_id = false) {
		if($emp_id === false || (is_array($emp_id) && count(ASSIST_HELPER::removeBlanksFromArray($emp_id)) == 0)) {
			return array();
		} else {
			//Get active jobs which are marked as current contracts
			$sql = "SELECT * FROM ".$this->getTableName()." job
					WHERE ".$this->getActiveStatusSQL("job")." AND (".$this->getStatusFieldName()." & ".EMP1::JOB_CURRENT_CONTRACT."+".EMP1::ACTIVE.") = ".EMP1::JOB_CURRENT_CONTRACT."+".EMP1::ACTIVE;
			if(is_array($emp_id)) {
				$sql .= " AND ".$this->getParentFieldName()." IN (".implode(",", $emp_id).")";
			} elseif($emp_id !== false) {
				$sql .= " AND ".$this->getParentFieldName()." = $emp_id ";
			}
			$sql .= " ORDER BY job_start DESC, job_id DESC ";
			$raw_data = $this->mysql_fetch_all($sql);
			//FINAL DATA TO BE RETURNED IS ALWAYS IN FORMAT array(emp_id=>job_data_in_array)
			$final_data = array();
			if(count($raw_data) > 0) {
				//if no employee id was submitted OR an array that is not empty was submitted
				if($emp_id === false || (is_array($emp_id) && count($emp_id) > 0)) {
					foreach($raw_data as $i => $row) {
						$key = $row[$this->getParentFieldName()];
						if(!isset($final_data[$key])) {
							$final_data[$key] = $row;
						}
					}
					//else if an employee id was submitted and it wasn't a blank array - assume a single integer and return the first row (latest start date)
				} elseif($emp_id !== false && !is_array($emp_id)) {
					$final_data[$emp_id] = $raw_data[0];
				}
			}

			return $final_data;
		}
	}

	//get the ended contract record for employees
	public function getRecentEndedJobsPerEmployee($emp_id = false) {
		if($emp_id === false || (is_array($emp_id) && count(ASSIST_HELPER::removeBlanksFromArray($emp_id)) == 0)) {
			return array();
		} else {
			//Get active jobs which are marked as current contracts
			$sql = "SELECT * FROM ".$this->getTableName()." job
				WHERE (".$this->getStatusFieldName()." & ".EMP1::JOB_ENDED_CONTRACT."+".EMP1::ACTIVE.") = ".EMP1::JOB_ENDED_CONTRACT."+".EMP1::ACTIVE;
			if(is_array($emp_id)) {
				$sql .= " AND ".$this->getParentFieldName()." IN (".implode(",", $emp_id).")";
			} elseif($emp_id !== false) {
				$sql .= " AND ".$this->getParentFieldName()." = $emp_id ";
			}
			$sql .= " ORDER BY job_start DESC, job_id DESC ";
			$raw_data = $this->mysql_fetch_all($sql);
			//FINAL DATA TO BE RETURNED IS ALWAYS IN FORMAT array(emp_id=>job_data_in_array)
			$final_data = array();
			if(count($raw_data) > 0) {
				//if no employee id was submitted OR an array that is not empty was submitted
				if($emp_id === false || (is_array($emp_id) && count($emp_id) > 0)) {
					foreach($raw_data as $i => $row) {
						$key = $row[$this->getParentFieldName()];
						if(!isset($final_data[$key])) {
							$final_data[$key] = $row;
						}
					}
					//else if an employee id was submitted and it wasn't a blank array - assume a single integer and return the first row (latest start date)
				} elseif($emp_id !== false && !is_array($emp_id)) {
					$final_data[$emp_id] = $raw_data[0];
				}
			}

			return $final_data;
		}
	}

	/**
	 * End function changes due to job handling change on 14 August
	 */


}


/******************************************
 * OLD FUNCTIONS BEFORE CHANGE OF JOB HANDLING [JC - 14 Aug 2018]
 * //add JOB for new employee - don't worry about job change reason & end date of previous job
 * public function addObjectForNewEmployee($var,$headings=array(),$log=true) {
 * //get headings to be populated
 * if(count($headings)>0) {
 * $headObject = new EMP1_HEADINGS();
 * $headings = $headObject->getMainObjectHeadings("JOB");
 * }
 * $job_header = $headings['rows']['job_heading'];
 * $headings = $headings['sub'][$job_header['id']];
 *
 * //set default fields
 * $insert_data = array(
 * self::TABLE_FLD.'_empid' => $var['emp_id'],
 * self::TABLE_FLD.'_status' => self::ACTIVE,
 * self::TABLE_FLD.'_insertdate' => date("Y-m-d H:i:s"),
 * self::TABLE_FLD.'_insertuser' => $this->getUserID(),
 * );
 *
 * //get fields
 * foreach($headings as $h_id => $head) {
 * $fld = $head['field'];
 * if(!isset($insert_data[$fld])) {
 * if(isset($var[$fld])) {
 * $v = $var[$fld];
 * switch($head['type']) {
 * case "DATE":
 * if(strlen($v)>0) {
 * $v = date("Y-m-d",strtotime($v));
 * }
 * break;
 * }
 * } else {
 * $v = "";
 * }
 * $insert_data[$fld] = $v;
 * }
 * }
 * //save
 * $sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
 * $job_id = $this->db_insert($sql);
 * if($job_id!==false && !is_null($job_id) && $job_id>0) {
 * //save log (not needed if adding new job for edited employee)
 * if($log) {
 * //LOG CREATION!!!!
 * $changes = array(
 * 'response'=>"New |".self::OBJECT_NAME."| ".$this->getRefTag().$job_id." created.",
 * 'user'=>$this->getUserName(),
 * );
 * $log_var = array(
 * 'object_id'        => $job_id,
 * 'emp_id'        => $var['emp_id'],
 * 'object_type'    => $this->getMyObjectType(),
 * 'changes'        => $changes,
 * 'log_type'        => EMP1_LOG::CREATE,
 * );
 * $this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
 * }
 * $result = array('result'=>true,'comment'=>"Adding Job successful.",'object_id'=>$job_id);
 * } else {
 * $result = array('result'=>false,'comment'=>"Adding Job failed.");
 * }
 * return $result;
 * }
 *
 *
 * public function editObject($var,$headings=array()) {
 * $result = array("error","Nothing done.");
 *
 * //get headings to be populated
 * if(count($headings)>0) {
 * $headObject = new EMP1_HEADINGS();
 * $headings = $headObject->getMainObjectHeadings("JOB");
 * }
 * $job_header = $headings['rows']['job_heading'];
 * $headings = $headings['sub'][$job_header['id']];
 *
 * $emp_id = $var['emp_id'];
 *
 * $old_data = $this->getLatestJobsPerEmployee($emp_id);
 * $old_data = $old_data[key($old_data)];
 *
 * $update_changes = array();
 * foreach($headings as $h_id => $head) {
 * $fld = $head['field'];
 * $new = $var[$fld];
 * $old = $old_data[$fld];
 *
 * if($new!=$old) {
 * switch($head['type']) {
 * case "EMPLOYEE":
 * $listObject = new EMP1_EMPLOYEE();
 * $items = $listObject->getListOfEmployeesFormattedForSelect();
 * $to = isset($items[$new]) ? $items[$new] : $this->getUnspecified();
 * $from = isset($items[$old]) ? $items[$old] : $this->getUnspecified();
 * break;
 * case "LIST":
 * $listObject = new EMP1_LIST($head['list_table']);
 * $items = $listObject->getAllListItemsFormattedForSelect();
 * $to = isset($items[$new]) ? $items[$new] : $this->getUnspecified();
 * $from = isset($items[$old]) ? $items[$old] : $this->getUnspecified();
 * break;
 * case "MASTER":
 * $class = "ASSIST_MASTER_".strtoupper($head['list_table']);
 * $listObject = new $class();
 * $items = $listObject->getAllItemsFormattedForSelect();
 * $to = isset($items[$new]) ? $items[$new] : $this->getUnspecified();
 * $from = isset($items[$old]) ? $items[$old] : $this->getUnspecified();
 * break;
 * case "DATE":
 * if(strlen($new)>0) {
 * $new = date("Y-m-d",strtotime($new));
 * $to = date("d-M-Y",strtotime($new));
 * } else {
 * $to = $this->getUnspecified();
 * $new = "0000-00-00";
 * }
 * if(strlen($old)==0 || $old == "0000-00-00" || is_null($old)) {
 * $from = $this->getUnspecified();
 * } else {
 * $from = date("d-M-Y",strtotime($old));
 * }
 * break;
 * default:
 * $to = $new;
 * $from = $old;
 * break;
 * }
 *
 * $update_changes[$fld] = array(
 * 'to'=>$to,
 * 'from'=>$from,
 * 'raw'=>array(
 * 'to'=>$new,
 * 'from'=>$old,
 * )
 * );
 * }
 * }
 * //if there are changes then create new job
 * if(count($update_changes)>0) {
 * $add_result = $this->addObjectForNewEmployee($var,$headings,$log=false);
 * if($add_result['result']==true) {
 * $object_id = $add_result['object_id'];
 * //log
 * $changes = array(
 * 'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." edited.",
 * 'user'=>$this->getUserName(),
 * );
 * $changes = array_merge($changes,$update_changes);
 * $log_var = array(
 * 'object_id'        => $object_id,
 * 'emp_id'        => $emp_id,
 * 'object_type'    => $this->getMyObjectType(),
 * 'changes'        => $changes,
 * 'log_type'        => EMP1_LOG::EDIT,
 * );
 * $this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
 * $result = array("ok",$changes['response']);
 * } else {
 * $result = array("error","An error occurred.  No changes were saved.");
 * }
 * } else {
 * $result = array("info","No changes to be saved.");
 * }
 * return $result;
 *
 * }
 *
 *
 * public function getLatestJobsPerEmployee($emp_id=false) {
 * if($emp_id===false || (is_array($emp_id) && count($emp_id)==0)) {
 * return array();
 * } else {
 * $sql = "SELECT * FROM ".$this->getTableName()."
 * WHERE ".$this->getActiveStatusSQL();
 * if(is_array($emp_id)) {
 * $sql.=" AND ".$this->getParentFieldName()." IN (".implode(",",$emp_id).")";
 * } else {
 * $sql.=" AND ".$this->getParentFieldName()." = $emp_id ";
 * }
 * $sql.=" ORDER BY job_start DESC, job_id DESC ";
 * $raw_data = $this->mysql_fetch_all($sql);
 * //FINAL DATA TO BE RETURNED IS ALWAYS IN FORMAT array(emp_id=>job_data_in_array)
 * $final_data = array();
 * if(count($raw_data)>0) {
 * //if no employee id was submitted OR an array that is not empty was submitted
 * if($emp_id===false || (is_array($emp_id) && count($emp_id)>0)) {
 * foreach($raw_data as $i => $row) {
 * $key = $row[$this->getParentFieldName()];
 * if(!isset($final_data[$key])) {
 * $final_data[$key] = $row;
 * }
 * }
 * //else if an employee id was submitted and it wasn't a blank array - assume a single integer
 * } elseif($emp_id!==false && !(is_array($emp_id) && count($emp_id)==0)) {
 * $final_data[$emp_id] = $raw_data[0];
 * }
 * }
 *
 * return $final_data;
 * }
 * }
 *
 *
 * public function getJobsByID($job_id=false) {
 * if($job_id===false || (is_array($job_id) && count($job_id)==0)) {
 * return array();
 * } else {
 * $sql = "SELECT job_id, name as job_title, job_start
 * FROM ".$this->getTableName()."
 * LEFT OUTER JOIN ".$this->getDBRef()."_list_jobtitle
 * ON job_title = id
 * WHERE ".$this->getActiveStatusSQL();
 * if(is_array($job_id)) {
 * $sql.=" AND ".$this->getIDFieldName()." IN (".implode(",",$job_id).")";
 * } else {
 * $sql.=" AND ".$this->getIDFieldName()." = $job_id ";
 * }
 * $sql.=" AND job_newcontract = 1
 * ORDER BY job_start DESC, job_id DESC ";
 * $raw_data = $this->mysql_fetch_all_by_id($sql,"job_id");
 * $final_data = array();
 * foreach($raw_data as $job_id => $job) {
 * $val = $job['job_title'];
 * if(strlen($job['job_start'])>0 && $job['job_start']!="0000-00-00" && date("Y-m-d",strtotime($job['job_start']))!="1970-01-01") {
 * $val.= " (Started on: ".date("d F Y",strtotime($job['job_start'])).")";
 * }
 * $final_data[$job_id] = $val;
 * }
 * return $final_data;
 * }
 * }
 */


?>