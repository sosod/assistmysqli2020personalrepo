<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 *
 *
 * LOG TYPE:
 *    A=Approve
 *    C=Create
 *    D=Delete
 *    E=Edit
 *  F=conFirm
 *    L=unLock
 *    N=decliNe
 *    R=Restore
 *    T=deacTivate
 *    U=Update
 *  V=actiVate
 *
 */

class EMP1_LOG extends EMP1 {
	private $log;
	private $log_table;
	private $field_prefix;
	private $fields;
	private $has_status;
	private $status_table;
	private $is_setup_log;

	private $headings;
	private $replace_headings;
	private $headObject;
	private $displayObject;

	const APPROVE = "A";
	const CREATE = "C";
	const DELETE = "D";
	const EDIT = "E";
	const CONFIRM = "F";
	const UNDOCONFIRM = "FU";
	const UNLOCK = "L";
	const DECLINE = "N";
	const RESTORE = "R";
	const DEACTIVATE = "T";
	const UPDATE = "U";
	const ACTIVATE = "V";
	const UNDOACTIVATE = "VU";

	public function __construct($log_table = "", $modref = "") {
		parent::__construct($modref);
		if(strlen($log_table) > 0) {
			$this->setLogTable($log_table);
		}
		$this->headObject = new EMP1_HEADINGS($modref);
		$this->displayObject = new EMP1_DISPLAY($modref);
	}

	public function setLogTable($log_table) {
		$this->log = $log_table;
		$this->log_table = $log_table;
		$this->setLogDetails();
	}

	/**********************************
	 * CONTROLLER functions
	 */
	public function getObject($var) {
		return array($this->getAuditLogHTML($var));
		//return array("getobject",serialize($var));
	}

	public function addObject($var) {
		foreach($this->fields as $fld => $table_fld) {
			switch($fld) {
				case "insertuser":
					if(!isset($var['insertuser'])) {
						$var['insertuser'] = $this->getUserID();
					}
					break;
				case "insertusername":
					if(!isset($var['insertusername'])) {
						$var['insertusername'] = $this->getUserName();
					}
					break;
				case "insertdate":
					if(!isset($var['insertdate'])) {
						$var['insertdate'] = date("Y-m-d H:i:s");
					}
					break;
				case "changes":
					if(isset($var['changes'])) {
						$var['changes'] = $this->encodeLogArray($var['changes']);
					}
					break;
				case "attachment":
					if(isset($var['attachment'])) {
						$var['attachment'] = $this->encodeLogArray($var['attachment']);
					}
					break;
			}
		}
		$data = $this->convertGenericToSpecific($var);
		$insert_data = $this->convertArrayToSQL($data);
		$sql = "INSERT INTO ".$this->log_table." SET ".$insert_data;
//		return $sql;
		$result = $this->db_insert($sql);
		return $result;
	}


	/**
	 * Function to convert changes array into something that can be displayed in a table or in email
	 * @param type = Output type
	 * @param c
	 * @param html_tags = allow html tags in text
	 * @param var = calling info - needed to generate headings for Setup Lists log
	 */
	public function processAuditLogs($type, $c, $html_tags = false, $var = array()) {
		$changes = array();
		$this->prepAuditLogHeadings($var);
		$headObject = $this->headObject;
		$displayObject = $this->displayObject;
		$headings = $this->headings; //return array(serialize($this->headings));
		$replace_headings = $this->replace_headings; //$this->arrPrint($headings);
		foreach($c as $fld => $x) {
			$name = "";
			if($fld == "log_display_comment") {
				$name = "";
				$type = "TEXT";
			} elseif(substr($fld, 0, 1) == "|" && substr($fld, -1) == "|") {
				//$a = $this->replaceObjectNames(array($fld));
				$name = $fld;
				$type = "TEXT";
			} elseif(!$this->is_setup_log && isset($headings[$fld])) {
				$name = $headings[$fld]['name'];
				$type = $headings[$fld]['type'];
				if($headObject->isListField($type)) {
					$type = "TEXT";
				}
			} else {
				if($fld != "response") {
					$nm = array();
					$n = explode(" ", $fld);
					$type = "TEXT";
					foreach($n as $f) {
						if(substr($f, 0, 1) == "|" && substr($f, -1) == "|") {
							$nm[] = $f;
						} else {
							$nm[] = isset($headings[$f]['name']) ? $headings[$f]['name'] : "|".$f."|";
							if(isset($headings[$f]['type']) && !in_array($headings[$f]['type'], array("TEXT", "LIST"))) {
								$type = $headings[$f]['type'];
							}
						}
					}
					$name = implode(" ", $nm);
				} elseif(!isset($headings[$fld]) || $type == "HTML") {
					$name = "";
				} else {
					$name = isset($headings[$fld]['name']) ? $headings[$fld]['name'] : "|".$fld."|";
				}
			}
			//$name.=":".substr($name,1,1).":";
			if(substr($name, 0, 1) == "!") {
				$name = ucwords(str_ireplace("_", " ", $fld));
			}
			if($fld == "log_display_comment") {
				//if($type!="NOTIFICATION") {
				$changes[] = $x;
				//}
				/*} elseif($fld=="contract_assess_status") {
						$AlistObject = new EMP1_LIST("deliverable_status");
						$Alist_items = $AlistObject->getActiveListItemsFormattedForSelect("id <> 1");
						unset($AlistObject);
					foreach($x as $key => $z) {
						$n = $name.": ".$Alist_items[$key];
						foreach($z as $y=>$a) {
							if(strlen($a)==0) {
								$z[$y] = $this->getUnspecified();
							} else {
								$z[$y] = $displayObject->getDataFieldNoJS("BOOL", $a,array('html'=>$html_tags));
							}
						}
						if($html_tags) {
							$changes[] = "<span class=u>".$n."</span> was changed to <span class=i>".$z['to']."</span> from <span class=i>".$z['from']."</span>";
						} else {
							$changes[] = "".$n." was changed to ".$z['to']." from ".$z['from']."";
						}
					}*/
			} elseif($type == "ATTACH") {
				if(is_array($x)) {
					foreach($x as $r) {
						$changes[] = $r;
					}
				} else {
					$changes[] = $x;
				}
			} elseif(is_array($x)) {
				unset($x['raw']);
				if(isset($x['to']) || isset($x['from'])) {
					$changes[] = $this->processArrayLog($x, $html_tags, $type, $name, $displayObject);
				} else {
					foreach($x as $y => $z) {
						$changes[] = $this->processArrayLog($z, $html_tags, $type, $name." ".$y, $displayObject);
					}
				}
			} elseif((($fld == "response") && ($name == "" || count($c) == 1 || $this->log == "list")) || $fld == "status") {
				$changes[] = $x;
			} else {
				//$changes[] = $type;
				$changes[] = $name.": ".$displayObject->getDataFieldNoJS($type, $x, array('html' => $html_tags));
			}
		}
		$changes = $this->replaceHeadingNames($replace_headings, $changes);
		$changes = $this->replaceAllNames($changes);
		$change = $this->stripPipes($changes);
		return $changes;
	}

	private function processArrayLog($x, $html_tags, $type, $name, $displayObject) {
		if(count($x) > 0) {
			foreach($x as $y => $z) {
				if(strlen($z) == 0) {
					$x[$y] = $this->getUnspecified();
				} else {
					$x[$y] = $displayObject->getDataFieldNoJS($type, $z, array('html' => $html_tags));
				}
			}
			if($html_tags) {
				return "<span class=u>".$name."</span> was changed to <span class=i>".$x['to']."</span> from <span class=i>".$x['from']."</span>";
			} else {
				return "".$name." was changed to ".$x['to']." from ".$x['from']."";
			}
		}
		return "Error";
	}


	/***********************************
	 * GET functions
	 */
	public function getAuditLogs($var) {
		if(isset($var['log_type']) && $var['log_type'] == "E") {
			$var['log_type'] = array(false, EMP1_LOG::UPDATE);
		}
		$data = $this->convertGenericToSpecific($var);
		$where = $this->convertArrayToSQL($data, " AND ");
		if($this->has_status) {
			$sql = "SELECT L.*, IF(LENGTH(S.client_name)>0,S.client_name,S.default_name) as status FROM ".$this->log_table." L LEFT OUTER JOIN ".$this->status_table." S ON S.id = L.".$this->fields['status_id']." ".(strlen($where) > 0 ? " WHERE  ".$where : "")." ORDER BY ".$this->field_prefix."id DESC";
		} else {
			$sql = "SELECT * FROM ".$this->log_table.(strlen($where) > 0 ? " WHERE  ".$where : "")." ORDER BY ".$this->field_prefix."id DESC";
		}
		$result = $this->mysql_fetch_all($sql);
		$c = $this->fields['changes'];
		$d = $this->fields['insertdate'];
		$rows = array();
		foreach($result as $res) {
			$changes = $this->decodeLogArray($res[$c]);
			unset($changes['system']);
			$user = $changes['user'];
			unset($changes['user']);
			if($this->has_status && !is_null($res['status'])) {
				$p = $this->fields['progress'];
				$status = $res['status']." (".$res[$p]."%)";
			} else {
				$status = "";
			}
			$rows[] = array(
				'insertdate' => date("d-M-Y H:i", strtotime($res[$d])),
				'user' => $user,
				'changes' => $changes,
				'status' => $status,
			);
		}
		return $rows;
	}

	public function getAuditLogsForReport($var) {
		if(isset($var['log_type']) && $var['log_type'] == "E") {
			$var['log_type'] = array(false, EMP1_LOG::UPDATE);
		}
		$data = $this->convertGenericToSpecific($var);
		$where = $this->convertArrayToSQL($data, " AND ");
		if($this->has_status) {
			$sql = "SELECT L.*, IF(LENGTH(S.client_name)>0,S.client_name,S.default_name) as status FROM ".$this->log_table." L LEFT OUTER JOIN ".$this->status_table." S ON S.id = L.".$this->fields['status_id']." ".(strlen($where) > 0 ? " WHERE  ".$where : "")." ORDER BY ".$this->field_prefix."id DESC";
		} else {
			$sql = "SELECT * FROM ".$this->log_table.(strlen($where) > 0 ? " WHERE  ".$where : "")." ORDER BY ".$this->field_prefix."id DESC";
		}
		$result = $this->mysql_fetch_all($sql); //ASSIST_HELPER::arrPrint($result);
		$c = $this->fields['changes'];
		$d = $this->fields['insertdate'];
		$rows = array();
		foreach($result as $res) {
			$object_id = $res[$this->fields['object_id']];
			$changes = $this->decodeLogArray($res[$c]);
			unset($changes['system']);
			$user = isset($changes['user']) ? $changes['user'] : "";
			unset($changes['user']);
			if($this->has_status && !is_null($res['status'])) {
				$p = $this->fields['progress'];
				$status = $res['status']." (".$res[$p]."%)";
			} else {
				$status = "";
			}
			$rows[$object_id][strtotime($res[$d])] = array(
				'insertdate' => date("d-M-Y H:i", strtotime($res[$d])),
				'user' => $user,
				'changes' => $changes,
				'status' => $status,
			);
		}
		return $rows;
	}

	public function getAuditLogHTML($var) {
		$echo = "";
		$displayObject = $this->displayObject; //new EMP1_DISPLAY();
		$rows = array();
		$rows = $this->getAuditLogs($var);
		$echo .= "
		<table class=tbl_audit_log>
			<tr>
				<th class=th_audit_log_date width=100px>Date</th>
				<th class=th_audit_log_user width=100px>User</th>
				<th class=th_audit_log_changes>Changes</th>
				".($this->has_status ? "<th class=th_audit_log_status width=100px>Status</th>" : "")."
			</tr>";
		if(count($rows) > 0) {
			foreach($rows as $r) {
				$c = $r['changes'];
				$changes = $this->processAuditLogs("HTML", $c, true, $var); //return $changes;
				//$changes = array();
				$echo .= "
				<tr>
					<td class=center>".$r['insertdate']."</td>
					<td class=center>".$r['user']."</td>
					<td>".implode("<br />", $changes)."</td>
					".($this->has_status ? "<td class=center>".$r['status']."</td>" : "")."
				</tr>
				";
			}
		} else {
			$colspan = 3 + ($this->has_status ? 1 : 0);
			$echo .= "
				<tr>
					<td colspan=".$colspan.">No Activity Logs found. </td>
				</tr>
				";
		}
		$echo .= "</table>";
		return $echo;
	}
	/***********************************
	 * SET functions
	 */
	/**********************************
	 * PROTECTED functions
	 */
	/***********************************
	 * PRIVATE functions
	 */
	/**
	 * Function to set $this->headings and $this->replace_headings for use to convert raw logs into user-readable format
	 */
	private function prepAuditLogHeadings($var = array()) {
		if(count($this->headings) == 0 || count($this->replace_headings) == 0) {
			$headings = array();
			$replace_headings = array();
			switch($this->log) {
				case "list":
					$listObj = new EMP1_LIST($var['section']);
					$headings = $listObj->getFieldNames();
					$replace_headings = $headings;
					break;
				case "user":
					$userObj = new EMP1_USERACCESS();
					$headings = $userObj->getUserAccessFields();
					$replace_headings = $headings;
					break;
				case "preferences":
					$prefObj = new EMP1_SETUP_PREFERENCES();
					$headings = $prefObj->getQuestionForLogs();
					$replace_headings = $headings;
					break;
				case "setup":
					if($var['section'] == "MENU") {
						$menuObj = new EMP1_MENU();
						$headings = $menuObj->getDefaultMenuNamesForLog();
						$replace_headings = $headings;
					} elseif($var['section'] == "HEAD") {
						//$headObj = new EMP1_HEADINGS();
						$headings = $this->headObject->getDefaultHeadingNamesForLog();
						$replace_headings = $headings;
					}
					break;
				default:
					//$headObject = new EMP1_HEADINGS();
					$headings = $this->headObject->getHeadingsForLog();
					$replace_headings = array();
					foreach($headings as $fld => $h) {
						$replace_headings[$fld] = $h['name'];
					}
					break;
			}
			$this->headings = $headings;
			$this->replace_headings = $replace_headings;
		}
	}

	/**
	 * Encode given array field for storage in database
	 */
	private function encodeLogArray($a) {
		return base64_encode(serialize($a));
	}

	/**
	 * Decode given array from the database for processing
	 */
	private function decodeLogArray($a) {
		return unserialize(base64_decode($a));
	}

	/**
	 * Convert generic field names to log specific field names
	 */
	public function convertGenericToSpecific($var) {
		$data = array();
		foreach($var as $key => $x) {
			if(isset($this->fields[$key])) {
				$data[$this->fields[$key]] = $x;
			} else {
				$data[$key] = $x;
			}
		}
		return $data;
	}

	/**
	 * Set the details of the log table
	 */
	private function setLogDetails() {
		$this->is_setup_log = false;
		switch($this->log_table) {
			case "setup":
			case "list":
			case "preferences":
			case "notifications":
			case "user":
			case $this->getDBRef()."_setup_log":
				$this->log_table = $this->getDBRef()."_setup_log";
				$this->has_status = false;
				$this->field_prefix = "sl_";
				$this->fields = array(
					'ref' => $this->field_prefix."id",
					'section' => $this->field_prefix."section",
					'object_id' => $this->field_prefix."item_id",
					'insertuser' => $this->field_prefix."insertuser",
					'insertusername' => $this->field_prefix."insertusername",
					'insertdate' => $this->field_prefix."insertdate",
					'changes' => $this->field_prefix."changes",
					'log_type' => $this->field_prefix."log_type",
				);
				$this->is_setup_log = true;
				break;
			case "userprofile":
			case $this->getDBRef()."_userprofile_log":
				$this->log_table = $this->getDBRef()."_userprofile_log";
				$this->has_status = false;
				$this->field_prefix = "upl_";
				$this->fields = array(
					'ref' => $this->field_prefix."id",
					'section' => $this->field_prefix."section",
					'object_id' => $this->field_prefix."item_id",
					'insertuser' => $this->field_prefix."insertuser",
					'insertdate' => $this->field_prefix."insertdate",
					'changes' => $this->field_prefix."changes",
					'log_type' => $this->field_prefix."log_type",
				);
				break;
			case "object":
			case $this->getDBRef()."_object_log":
				$this->log_table = $this->getDBRef()."_object_log";
				$this->has_status = false;
				$this->status_table = false;
				$this->field_prefix = "log_";
				$this->fields = array(
					'ref' => $this->field_prefix."id",
					'object_id' => $this->field_prefix."object_id",
					'emp_id' => $this->field_prefix."emp_id",
					'object_type' => $this->field_prefix."object_type",
					'insertuser' => $this->field_prefix."insertuser",
					'insertdate' => $this->field_prefix."insertdate",
					'changes' => $this->field_prefix."changes",
					'attachment' => $this->field_prefix."attachment",
					'log_type' => $this->field_prefix."log_type",
				);
				break;
			case "assurance":
			case $this->getDBRef()."_assurance_log":
				$this->log_table = $this->getDBRef()."_assurance_log";
				$this->has_status = false;
				$this->field_prefix = "asl_";
				$this->fields = array(
					'ref' => $this->field_prefix."id",
					'section' => $this->field_prefix."section",
					'object_id' => $this->field_prefix."item_id",
					'changes' => $this->field_prefix."changes",
					'insertuser' => $this->field_prefix."insertuser",
					'insertdate' => $this->field_prefix."insertdate",
					'attachment' => $this->field_prefix."attachment",
					'log_type' => $this->field_prefix."log_type",
				);
				break;
		}
	}


}

?>