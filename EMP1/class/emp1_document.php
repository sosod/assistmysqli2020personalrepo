<?php
/**
 * Class EMP1_DOCUMENT
 * Purpose: To manage the Documents tab in the EMP1/Employee Assist module
 * Author: Sondelani Dumalisile
 * Date Created: 10 Jan 2021
 * YouTrack: AA-406
 */

class EMP1_DOCUMENT extends EMP1 {
	protected $object_id = 0;
	protected $ref_tag = "DOC";
	protected $status_field = "_status";
	protected $parent_field = "_empid";
	protected $id_field = "_id";
	protected $attachment_field = "attachments";
	protected $progress_field = "_progress";
	private $modref;
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "DOCUMENT";
	const OBJECT_NAME = "document";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "document";
	const TABLE_FLD = "doc";
	const REFTAG = "ERROR/CONST/REFTAG";
	const LOG_TABLE = "object";
	public $filterExt;
	public $icon;
	public $lastModDate;
	public $lastModUser;


	public function __construct($obj_id = 0, $modref = "") {
		$this->modref = $modref;
		parent::__construct($modref);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;

		$this->object_form_extra_js = "";
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getEmptyTableNotice() {
		return "No ".self::OBJECT_NAME."s found to display.";
	}


	public function getDocFullDetailByEmployeeID($emp_id = false, $srt) {
		if($emp_id === false || (is_array($emp_id) && count(ASSIST_HELPER::removeBlanksFromArray($emp_id)) == 0)) {
			return array();
		} else {
			$sql = "SELECT doc_id
					, CONCAT('".$this->getRefTag()."',doc_id) as ref
					, doc_title as doc_title
					, doc_content as doc_content
					, doctype.value as doc_type
					, doc_date as doc_date
					, doc_expires as doc_expires
					, doc_status as doc_status
					, attachments as attachments
					, doc_insertdate as doc_insertdate
					, doc_modifieddate as doc_modifieddate
					, doc_insertuser as doc_insertuser
					, doc_lastmoduser  as doc_lastmoduser 
					, IF( ((doc_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."),1,0) as is_active
					FROM ".$this->getTableName()." doc
					LEFT OUTER JOIN ".$this->getDBRef()."_list_doctype doctype
					  ON doc.doc_type = doctype.id
					WHERE ".$this->getActiveStatusSQL("doc");
			if(is_array($emp_id)) {
				$sql .= " AND doc.".$this->getParentFieldName()." IN (".implode(",", $emp_id).")";
			} else {
				$sql .= " AND doc.".$this->getParentFieldName()." = $emp_id ";
			}
			if(isset($srt) && !empty($srt)) { //if the sort value is passed

				$sql .= " ORDER BY ".$srt." DESC, doc_id DESC";

			} else {
				$sql .= " ORDER BY doc_status ASC, doc_id DESC ";    //include status sort so that latest doc status always sorts top
			}

			$final_data = $this->mysql_fetch_all_by_id($sql, "doc_id");

			return $final_data;
		}
	}


	public function getRawObject($obj_id, $format_date = true) {
		if(is_array($obj_id)) {
			if(isset($obj_id['obj_id'])) {
				$obj_id = $obj_id['obj_id'];
			} else {
				return array();
			}
		}
		$sql = "SELECT * ,doc.doc_type as doc_typeNum, doctype.value as doc_type , CONCAT(docuser.tkname,' ', docuser.tksurname) as doc_insertuser, CONCAT(moduser.tkname,' ', moduser.tksurname) as doc_lastmoduser FROM ".$this->getTableName()." doc LEFT OUTER JOIN ".$this->getDBRef()."_list_doctype doctype
					  ON doc.doc_type = doctype.id LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep docuser
					  ON doc.doc_insertuser = docuser.tkid LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep moduser
					  ON doc.doc_lastmoduser = moduser.tkid WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$row = $this->mysql_fetch_one($sql);
		if(isset($row['doc_title']) && isset($row['doc_content'])) {
			$row['doc_title'] = ASSIST_HELPER::decode($row['doc_title']);
			$row['doc_content'] = ASSIST_HELPER::decode($row['doc_content']);
		}

		if($format_date) {
			$row['doc_insertdate'] = date("d F Y", strtotime($row['doc_insertdate']));
			if(isset($row['doc_expires'])) {
				$row['doc_expires'] = date("d F Y", strtotime($row['doc_expires']));
				if($row['doc_expires'] == "1970-01-01" || $row['doc_expires'] == "0000-00-00") {
					$row['doc_expires'] = "0000-00-00";
				}
			}
			$row['doc_modifieddate'] = date("d F Y", strtotime($row['doc_modifieddate']));
			$row['doc_date'] = date("d F Y", strtotime($row['doc_date']));
		}
		//for view document
		if(isset($row['doc_expires'])) {
			switch($row['doc_status']) {
				case 2: //if doc not deleted
					date_default_timezone_set('Africa/Johannesburg');
					$date = strtotime(date('d F Y', time()));//today's date
					$exp = strtotime($row['doc_expires']); //converts string to time so that you can compare the dates
					if($exp >= $date) {

						$row['doc_status'] = "<span style='color: #4d9622'><b> Active </b></span>";

					} else {
						if($exp == strtotime("0000-00-00") || $exp == strtotime("1970-01-01")) {
							$row['doc_status'] = "<span style='color: #4d9622'><b> Active </b></span>";
						} else {
							$row['doc_status'] = "<span style='color: #900000'><b> Expired</b></span>";
						}
					}
					break;
				default:
					$row['doc_status'] = "<span style='color: #900000'><b> Document inactive</b></span>";
					break;
			}
		}
		$row['attachments'] = "";
		if(isset($row['doc_type'])) {

			switch(strtolower($row['doc_type'])) {
				case "microsoft word" :
					$ext = "doc";
					break;
				case "microsoft excel" :
					$ext = "xls";
					break;
				case "microsoft powerPoint" :
					$ext = "pptx";
					break;
				case "pdf" :
					$ext = "pdf";
					break;
				case "image" :
					$ext = "jpg";
					break;
				case "compressed file (zip)" :
					$ext = "zip";
					break;
				case "internet file" :
					$ext = "html";
					break;
				case "tiff" :
					$ext = "tiff";
					break;
				case "email" :
					$ext = "eml";
					break;
				case "other" :
				default:
					$ext = "ics";
					break;
			}
			$disObject = new EMP1_DISPLAY();
			$final = $disObject->getAttachIconPathByExt($ext);
			$row['attachments'] = $final; //inserts the new image path

		}

		return $row;
	}

	public function drawPageFooter($left = "", $log_table = "", $var = "", $log_id = "") {
		$data = $this->getPageFooter($left, $log_table, $var, $log_id);
		echo $data['display'];
		return $data['js'];
	}


	public function editObject($var) {
		$result = array("error", "Nothing done.");


		unset($var['form_action']);
		$doc_id = $var['doc_id'];
		unset($var['doc_id']);
		$emp_id = $var['doc_empid'];
		unset($var['doc_empid']);


		$insert_data = array(
			self::TABLE_FLD.'_modifieddate' => date("Y-m-d H:i:s"),
			self::TABLE_FLD.'_lastmoduser' => $this->getUserID()
		);

		$original = $this->getRawObject($doc_id);


		foreach($var as $fld => $v) {
			if($fld != "attachments" && $fld != "doc_type") { //fields that must not be changed
				if($v != $original[$fld]) {
					if($fld == "doc_date" || $fld == "doc_expires") {
						$v = date("Y-m-d", strtotime($v));
						//make a note if the start date changed so that the status can be checked

					} elseif($fld == "doc_type") {
						if(empty($v) || $v == "" || $v == null) {
							$v = 0;
						}
					} elseif($fld == "doc_content") {
						$insert_data[$fld] = ASSIST_HELPER::code($v);
					} elseif($fld == "doc_title") {
						$insert_data[$fld] = ASSIST_HELPER::code($v);
					}

					$insert_data[$fld] = ASSIST_HELPER::code($v);
					$new_record[$fld] = ASSIST_HELPER::code($v);
				}
			}
		}
		//for document view.
		/*foreach($original as $fld => $data) {
			if ($fld == "doc_modifieddate") {
				date_default_timezone_set('Africa/Johannesburg');
				$date = date('d F Y', time());//today's date
				$insert_data[$fld] = ASSIST_HELPER::code($date); //last modified date
			}elseif ($fld == "doc_lastmoduser"){
				$insert_data[$fld] = $emp_id; //last user to modify
			}

		}*/


		if(count($insert_data) > 0) {
			//if there are changes then save to the database
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$doc_id." AND ".$this->getParentFieldName()." = ".$emp_id;
			$mar = $this->db_update($sql);
			//log the changes
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$doc_id." edited.",
				'user' => $this->getUserName(),
			);
			foreach($insert_data as $fld => $new_raw) {
				if($fld != "doc_modifieddate") {
					if($fld == "doc_status") {
						if(($new_raw & EMP1::ACTIVE) == EMP1::ACTIVE) {
							$changes['log_display_comment'] = "Document status changed to 'Active'";
						} else {
							$changes['log_display_comment'] = "Document status changed to 'Inactive'";
						}
					} elseif($fld == "doc_date") {
						$new = date("d-M-Y", strtotime($new_raw));
						$old = date("d-M-Y", strtotime($original[$fld]));
						$changes[$fld] = array(
							'to' => $new,
							'from' => $old,
							'raw' => array(
								'to' => $new_raw,
								'from' => $original[$fld]
							)
						);
					} elseif($fld == "doc_expires") {
						$new = $new_raw == "0000-00-00" ? $this->getUnspecified() : date("d-M-Y", strtotime($new_raw));
						$old = $original[$fld] == "0000-00-00" ? $this->getUnspecified() : date("d-M-Y", strtotime($original[$fld]));
						$changes[$fld] = array(
							'to' => $new,
							'from' => $old,
							'raw' => array(
								'to' => $new_raw,
								'from' => $original[$fld]
							)
						);
					} elseif($fld == "doc_type") {
						$changes[$fld] = array(
							'to' => $new_raw,
							'from' => $original[$fld],
							'raw' => array(
								'to' => $new_raw,
								'from' => $original[$fld]
							)
						);
					} elseif($fld == "doc_title") {
						$changes[$fld] = array(
							'to' => $new_raw,
							'from' => $original[$fld],
							'raw' => array(
								'to' => $new_raw,
								'from' => $original[$fld]
							)
						);
					} elseif($fld == "doc_content") {
						$changes[$fld] = array(
							'to' => $new_raw,
							'from' => $original[$fld],
							'raw' => array(
								'to' => $new_raw,
								'from' => $original[$fld]
							)
						);
					} elseif($fld == "doc_lastmoduser") {
						//left here so it isn't used as a list
					} else {
						$old_raw = $original[$fld];
						$listObject = new EMP1_LIST($fld);
						$items = $listObject->getAListItemName(array($new_raw, $old_raw));
						$new = isset($items[$new_raw]) && $new_raw != 0 ? $items[$new_raw] : $this->getUnspecified();
						$old = isset($items[$old_raw]) && $old_raw != 0 ? $items[$old_raw] : $this->getUnspecified();

						$changes[$fld] = array(
							'to' => $new,
							'from' => $old,
							'raw' => array(
								'to' => $new_raw,
								'from' => $old_raw
							)
						);
					}
				}
			}
			$log_var = array(
				'object_id' => $doc_id,
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			//if changes were made to the previous current job then log the changes & update employee data in user table

			//result
			$result = array("ok", "".$this->getObjectName(self::OBJECT_NAME)." ".$this->getRefTag().$doc_id." edited successfully.", 'object_id' => $doc_id);
		} else {
			$result = array("info", "No changes found to be saved.", 'object_id' => $doc_id);
		}
		return $result;

	}

	public function addObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		unset($var['form_action']);
		$doc_id = $var['doc_id'];
		unset($var['doc_id']);
		$doc_empid = $var['doc_empid'];
		unset($var['doc_empid']);


		//set default fields
		$insert_data = array(
			self::TABLE_FLD.'_empid' => $doc_empid,
			self::TABLE_FLD.'_status' => EMP1::ACTIVE,
			self::TABLE_FLD.'_insertdate' => date("Y-m-d H:i:s"),
			self::TABLE_FLD.'_insertuser' => $this->getUserID(),
			self::TABLE_FLD.'_lastmoduser' => $this->getUserID(),
			self::TABLE_FLD.'_modifieddate' => date("Y-m-d H:i:s")
		);


		foreach($var as $fld => $v) {
			if($fld == "doc_date") {
				$v = date("Y-m-d", strtotime($v));
			} elseif($fld == "doc_insertdate" || $fld == "doc_modifieddate") {
				$v = date("Y-m-d h:m", strtotime($v));
			} elseif($fld == "doc_content") {
				$insert_data[$fld] = ASSIST_HELPER::code($v);
			} elseif($fld == "doc_title") {
				$insert_data[$fld] = ASSIST_HELPER::code($v);
			}
			if($fld == "doc_expires") {
				if(!isset($v) || $v == "") {
					$insert_data[$fld] = "0000-00-00";
				} else {
					$v = date("Y-m-d", strtotime($v));
				}
			}
			if(!isset($insert_data[$fld])) {
				$insert_data[$fld] = ASSIST_HELPER::code($v);
			}
		}

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
		$doc_id = $this->db_insert($sql);
		$changes = array(
			'response' => "New |".self::OBJECT_NAME."| ".$this->getRefTag().$doc_id." created.",
			'user' => $this->getUserName(),
		);
		$log_var = array(
			'object_id' => $doc_id,
			'emp_id' => $doc_empid,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => EMP1_LOG::CREATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);


		$result = array("ok", $this->getObjectName(self::OBJECT_NAME)." ".$this->getRefTag().$doc_id." added successfully.", 'object_id' => $doc_id);


		return $result;

	}

//new function - delete job record after converting employee
	public function deleteObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");
		$doc_empid = $var['doc_empid'];
		unset($var['doc_empid']);
		$doc_id = $var['doc_id'];
		unset($var['doc_id']);
		if(ASSIST_HELPER::checkIntRef($doc_id)) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$this->getStatusFieldName()." - ".self::ACTIVE." + ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$doc_id;
			$mar = $this->db_update($sql);
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$doc_id." deleted.",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $doc_id,
				'emp_id' => $doc_empid,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			$result = array("ok", $this->replaceAllNames($changes['response']));
		} else {
			$result = array("error", "Invalid ".$this->getObjectName($this->getMyObjectType()));
		}

		return $result;

	}


}