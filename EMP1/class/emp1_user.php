<?php
/**
 * To manage the USER object within the EMPLOYEE module
 *
 * Created on: January 2018
 * Authors: Janet Currie
 *
 */

class EMP1_USER extends EMP1 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_empid";
	protected $name_field = "_description";
	protected $attachment_field = "_attachment";

	protected $has_attachment = false;

	protected $ref_tag = "TK";
	private $modref;
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "USER";
	const OBJECT_NAME = "user";
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "employee";
	const TABLE_FLD = "emp";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

	public function __construct($obj_id = 0, $modref = "") {
		$this->modref = $modref;
		parent::__construct($modref);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		//$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		//if($obj_id>0) {
		//	$this->object_id = $obj_id;
		//	$this->object_details = $this->getAObject($obj_id);
		//}
		$this->object_form_extra_js = "";
	}


	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_NAME;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}


	/********
	 * Called from EMP1_EMPLOYEE
	 * To add a non-user employee with some job details (dept etc) needed for old PMSPS module to continue working
	 */
	public function addInactiveUser($var) {
		//convert title into text for timekeep table
		$listObject = new ASSIST_MASTER_PPLTITLE();
		$item = $listObject->getObjectNameByID($var['tktitle']);
		//prep variables for submission
		$insert_data = array(
			'tktitle' => $item,
			'tkname' => $var['tkname'],
			'tksurname' => $var['tksurname'],
			'tkdept' => $var['job_dept'],
			'tkdesig' => $var['job_title'],
			'tkloc' => $var['job_loc'],
			'tkmanager' => $var['job_manager'],
			'tkempnum' => $var['emp_num'],
			'tkempdate' => strtotime($var['emp_date']),
			'tktel' => $var['tktel'],
			'tkmobile' => $var['tkmobile'],
			'tkfax' => $var['tkfax'],
			'tkemail' => $var['tkemail'],
			'tkstatus' => 2,
			'tkadddate' => strtotime(date("d F Y H:i:s")),
			'tkadduser' => $this->getUserID(),
		);
		//get last id
		$sql = "SELECT max(tkid*1) as last_id FROM ".$this->getUserTableName();
		$last_id = $this->mysql_fetch_one_value($sql, "last_id");
		$next_id = $last_id + 1;
		//set tkid
		$insert_data['tkid'] = ($next_id < 1000 ? "0" : "").($next_id < 100 ? "0" : "").($next_id < 10 ? "0" : "").$next_id;
		//submit
		$sql = "INSERT INTO ".$this->getUserTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
		$i = $this->db_insert($sql);

		if(!is_null($i) && $i !== false) {
			//LOG!!!!!

			//LOG CREATION!!!!
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$insert_data['tkid']." created due to creation of related ".$this->getObjectName("EMPLOYEE").".",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $insert_data['tkid'],
				'emp_id' => "0",
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);


			$result = array('result' => true, 'tkid' => $insert_data['tkid']);
		} else {
			$result = array('result' => false, 'tkid' => "");
		}
		return $result;
	}

	public function getAddResultsForActiveUser($var) {
		$result = array('result' => true, 'tkid' => $var['original_tkid']);
		return $result;
	}


	public function editObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		$tkid = $var['tkid'];
		$emp_id = $var['emp_id'];

		$old_data = $this->getCurrentUserDetails($tkid);
		if($old_data['tkstatus'] == 2) {
			//get headings to be populated
			if(count($headings) > 0) {
				$headObject = new EMP1_HEADINGS();
				$headings = $headObject->getMainObjectHeadings("USER");
			}
			$user_header = $headings['rows']['user_heading'];
			$headings = $headings['sub'][$user_header['id']];

			$update_data = array();
			$update_changes = array();
			foreach($headings as $h_id => $head) {
				$fld = $head['field'];
				if(isset($var[$fld]) && isset($old_data[$fld])) {
					if($fld == "tktitle") {
						$listObject = new ASSIST_MASTER_PPLTITLE();
						$new = $listObject->getObjectNameByID($var['tktitle']);
					} else {
						$new = $var[$fld];
					}
					$old = $old_data[$fld];
					if($new != $old) {
						$update_data[$fld] = $new;
						$update_changes[$fld] = array(
							'to' => $new,
							'from' => $old,
							'raw' => array(
								'to' => $new,
								'from' => $old,
							),
						);
					}
				}
			}
			if(count($update_data) > 0) {
				$sql = "UPDATE ".$this->getUserTableName()." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE tkid = '$tkid'";
				$this->db_update($sql);
				//log
				$changes = array(
					'response' => "|".self::OBJECT_NAME."| ".$tkid." edited.",
					'user' => $this->getUserName(),
				);
				$changes = array_merge($changes, $update_changes);
				$log_var = array(
					'object_id' => $tkid,
					'emp_id' => $emp_id,
					'object_type' => $this->getMyObjectType(),
					'changes' => $changes,
					'log_type' => EMP1_LOG::EDIT,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

				$result = array("ok", "Changes made.");
			} else {
				$result = array("info", "No changes.");
			}

		} else {
			$result = array("info", "Active user.  Changes can't be made.");
		}


		return $result;
	}


//Function required to accommodate IAS PMS module that looks in timekeep table for employees
	public function terminateObject($var) {
		$result = array("error", "Nothing done.");

		$tkid = $var['tkid'];
		$emp_id = $var['emp_id'];

		$old_data = $this->getCurrentUserDetails($tkid);
		if($old_data['tkstatus'] == 2) {

			$sql = "UPDATE ".$this->getUserTableName()." SET tkstatus = 0, tktermdate = '".time()."' WHERE tkid = '$tkid'";
			$this->db_update($sql);
			//log
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$tkid." terminated.",
				'user' => $this->getUserName(),
				'tkstatus' => array(
					'to' => "Terminated",
					'from' => "Inactive",
					'raw' => array(
						'to' => 0,
						'from' => 2
					)
				),
			);
			$log_var = array(
				'object_id' => $tkid,
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			$result = array("ok", "User terminated.");

		} else {
			$result = array("info", "Active user.  Changes can't be made.");
		}


		return $result;
	}


//Function required to accommodate IAS PMS module that looks in timekeep table for employees
	public function restoreObject($var) {
		$result = array("error", "Nothing done.");

		$tkid = $var['tkid'];
		$emp_id = $var['emp_id'];

		$old_data = $this->getCurrentUserDetails($tkid);
		if($old_data['tkstatus'] == 4 || $old_data['tkstatus'] == 0) {

			$sql = "UPDATE ".$this->getUserTableName()." SET tkstatus = 2 WHERE tkid = '$tkid'";
			$this->db_update($sql);
			//log
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$tkid." restored.",
				'user' => $this->getUserName(),
				'tkstatus' => array(
					'to' => "Inactive User",
					'from' => "Terminated",
					'raw' => array(
						'to' => 2,
						'from' => $old_data['tkstatus'],
					)
				),
			);
			$log_var = array(
				'object_id' => $tkid,
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

			$result = array("ok", "User restored.");

		} else {
			$result = array("info", "Active user.  Changes can't be made.");
		}


		return $result;
	}


	public function getCurrentUserDetails($tkid) {
		$sql = "SELECT * FROM ".$this->getUserTableName()." WHERE tkid = '".$tkid."'";
		return $this->mysql_fetch_one($sql);
	}


	//function to add menu_modules_users record for non-system/Inactive users on Ignite clients
	public function addPMSPSRecord($user_id) {
		//get menu_modules record to get modid
		$sql = "SELECT * FROM assist_menu_modules WHERE modlocation = 'PMSPS'";
		$menu = $this->mysql_fetch_one($sql);
		if(isset($menu['modid']) && ASSIST_HELPER::checkIntRef($menu['modid'])) {
			$modid = $menu['modid'];
			//get cc_menu_modules record to get mmid
			$sql = "SELECT * FROM assist_".strtolower($this->getCmpCode())."_menu_modules WHERE modmenuid = ".$modid;
			$module = $this->mysql_fetch_one($sql);
			if(isset($module['modid']) && ASSIST_HELPER::checkIntRef($module['modid'])) {
				$mmid = $module['modid'];
				//check for existing record
				$sql = "SELECT * FROM assist_".strtolower($this->getCmpCode())."_menu_modules_users WHERE usrmodref = 'PMSPS' AND usrtkid = '$user_id'";
				$mmu = $this->mysql_fetch_all($sql);
				if(count($mmu) == 0) {
					//insert into cc_menu_modules_users
					$sql = "INSERT INTO assist_".strtolower($this->getCmpCode())."_menu_modules_users (usrmodid, usrtkid, usrmodref) VALUES ($mmid , '$user_id' , 'PMSPS')";
					$this->db_insert($sql);
				}
			}
		}
	}


}


?>