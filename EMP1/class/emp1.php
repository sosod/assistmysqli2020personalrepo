<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class EMP1 extends EMP1_HELPER {
	/***
	 * Module Wide variables
	 */

	private $employee_user_module_change_date = "21 January 2018 06:00:00";
	private $employee_user_module_change_timestamp = 0;

	protected $object_form_extra_js = "";

	protected $deliverable_types = array();

	protected $copy_function_protected_fields = array("contract_have_subdeliverables", "del_type", "del_parent_id");
	protected $copy_function_protected_heading_types = array("ATTACH", "DEL_TYPE");

	protected $has_target = false;

	protected $has_secondary_parent = false;

	/**
	 * Module Wide Constants
	 * */
	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;

	//const AWAITING_APPROVAL = 16;
	//const APPROVED = 128;

	//const STRATEGY_DRAFT = 2048;
	//const STRATEGY_FINAL = 4096;

	const ASSIST_USER = 256;
	const NON_USER = 512;

	const JOB_CURRENT_CONTRACT = 1024;
	const JOB_ENDED_CONTRACT = 2048;


	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref = "") {
		$this->employee_user_module_change_timestamp = strtotime($this->employee_user_module_change_date);
		parent::__construct($modref);
	}


//Get the date, in UNIX format, of when the change from Users to Employee was made on the Assist System (globally)
	public function getModuleChangeoverTimestamp() {
		return $this->employee_user_module_change_timestamp;
	}

	public function getMySecondaryParentObjectType() {
		if($this->has_secondary_parent) {
			return self::SECONDARY_PARENT_OBJECT_TYPE;
		}
		return false;
	}


	/*********************
	 * MODULE OBJECT functions
	 * for CONTRACT, DELIVERABLE AND ACTION
	 */
	public function getObject($var) { //$this->arrPrint($var);
		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				//unset($options['page']);
				$data = $this->getList($var['section'], $options,'',array());
				if($var['type'] == "LIST" || $var['type'] == "ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data, $this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details) > 0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'], $var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'], $var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}

	public function getStatusFieldName() {
		return $this->status_field;
	}

	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}

	public function getProgressFieldName() {
		return $this->progress_field;
	}

	public function getIDFieldName() {
		return $this->id_field;
	}

	public function getParentFieldName() {
		return $this->parent_field;
	}

	public function getSecondaryParentFieldName() {
		return $this->secondary_parent_field;
	}

	public function getNameFieldName() {
		return $this->name_field;
	}

	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}

	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}

	public function getOwnerFieldName() {
		return $this->owner_field;
	}

	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}

	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}

	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}

	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}

	public function getPageLimit() {
		$profileObject = new EMP1_PROFILE();
		return $profileObject->getProfileTableLength();
	}

	public function getDateOptions($fld) {
		if(stripos($fld, "action_on") !== FALSE) {
			$fld = "action_on";
		}
		switch($fld) {
			case "action_on":
				return array('maxDate' => "'+0D'");
				break;
			default:
				return array();
		}
	}

	public function getExtraObjectFormJS() {
		return $this->object_form_extra_js;
	}

	public function getAllDeliverableTypes() {
		return $this->deliverable_types;
	}

	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key => $t) {
			$arr[] = $t;
		}
		return $arr;
	}

	public function getDeliverableTypes($contract_id) {
		$contractObject = new EMP1_CONTRACT($contract_id);
		if($contractObject->doIHaveSubDeliverables() == FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types;
	}

	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt, $ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE", $options);
		//$this->arrPrint($data);
		$result = array(
			'past' => 0,
			'present' => 0,
			'future' => 0,
		);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z < $now) {
				$result['past']++; //echo "past";
			} elseif($z > $now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE", $options);
		return $data;
	}


	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getActiveObjectsFormattedForSelect($options = array()) { //$this->arrPrint($options);
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE." ";
		if(count($options) > 0) {
			foreach($options as $key => $val) {
				switch($key) {
					case "id":
						$sql .= " AND ".$this->getIDFieldName()." = ".$val;
						break;
					case "parent":
						$sql .= " AND ".$this->getParentFieldName()." = ".$val;
						break;
				}
			}
		} //echo $sql;
		$sql .= "
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		return $items;
	}

	/**
	 * Get list of active objects ready to populate a SELECT element
	 */
	public function getAllObjectsFormattedForSelect() {
		$sql = "SELECT
				  ".$this->getIDFieldName()." as id
				  , CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED."
				ORDER BY ".$this->getNameFieldName()."
				";
		$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		return $items;
	}
	/**
	 * needed for centralised call in importing;
	 */
	public function getActiveListItemsFormattedForImportProcessing($options=array()) {
		$items = $this->getActiveListItemsFormattedForSelect();
		$data = array();
		foreach($items as $id => $name) {
			$name = trim(strtolower(str_replace(" ","",$name)));
			$data[$name] = $id;
		}
		return $data;
	}

	public function getMyList($obj_type, $section, $options = array()) {
		//echo "<P>object: ".$obj_type;
		//echo "<P>section: ".$section;
		//echo "<P>options: "; $this->arrPrint($options);
		if(isset($options['page'])) {
			$page = strtoupper($options['page']);
			unset($options['page']);
		} else {
			$page = "DEFAULT";
		}
		$trigger_rows = true;
		if(isset($options['trigger'])) {
			$trigger_rows = $options['trigger'];
			unset($options['trigger']);
		}
		//echo $page;
		if($page == "LOGIN_STATS" || $page == "LOGIN_TABLE") {
			$future_deadline = $options['deadline'];
			unset($options['deadline']);
			if(!isset($options['limit'])) {
				$options['limit'] = false;
			}
		}
		$compact_details = ($page == "CONFIRM" || $page == "ALL");
		if(isset($options['limit']) && ($options['limit'] === false || $options['limit'] == "ALL")) {
			$pagelimit = false;
			$start = 0;
			$limit = false;
			unset($options['start']);
			unset($options['limit']);
		} else {
			$pagelimit = $this->getPageLimit();
			$start = isset($options['start']) ? $options['start'] : 0;
			unset($options['start']);
			$limit = isset($options['limit']) ? $options['limit'] : $pagelimit;
			unset($options['limit']);
			if($start < 0) {
				$start = 0;
				$limit = false;
			}
		}
		$order_by = ""; //still to be processed
		$left_joins = array();

		$headObject = new EMP1_HEADINGS();
		$final_data = array('head' => array(), 'rows' => array());

		//set up variables
		$idp_headings = $headObject->getMainObjectHeadings($obj_type, ($obj_type == "FUNCTION" ? "LIST" : "FULL"), $section);
		switch($obj_type) {

			case "EMPLOYEE":
				$idpObject = new EMP1_EMPLOYEE();
				break;
		}
		$idp_tblref = $idpObject->getMyObjectType();
		$idp_table = $idpObject->getTableName();
		$idp_id = $idp_tblref.".".$idpObject->getIDFieldName();
		$idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();
		$idp_status_fld = false;//$idp_tblref.".".$idpObject->getProgressStatusFieldName();
		$idp_name = $idp_tblref.".".$idpObject->getNameFieldName();
		$idp_deadline = false;//$idpObject->getDeadlineFieldName();
		$idp_field = $idp_tblref.".".$idpObject->getTableField();

		$where = $idpObject->getActiveStatusSQL($idp_tblref);
		//if type is something other than contract
		if($obj_type != "EMPLOYEE") {
			switch($obj_type) {

				case "EMPLOYEE":
					$id_fld = $idpObject->getIDFieldName();
					$sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
					$from = " $idp_table $idp_tblref
					";
					$final_data['head'] = $idp_headings;
					$where_tblref = $idp_tblref;
					$where_deadline = $idp_deadline;
					$where_order = $idp_name;
					$ref_tag = $idpObject->getRefTag();
					if($page == "LOGIN_TABLE") {
						$order_by = $idp_name;
					}
					$where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
					$group_by = "";
					break;

			}
		} else {
			$id_fld = $idpObject->getIDFieldName();
			$sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
			$from = " $idp_table $idp_tblref
			";
			$final_data['head'] = $idp_headings;
			$where_tblref = $idp_tblref;
			$where_deadline = $idp_deadline;
			$where_order = $idp_name;
			$ref_tag = $idpObject->getRefTag();
			if($page == "LOGIN_TABLE") {
				$order_by = $idp_name;
			}
			$where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
			$group_by = "";
		}
//ASSIST_HELPER::arrPrint($final_data);
		if(count($options) > 0) {
			foreach($options as $key => $filter) {
				if(substr($key, 0, 3) != substr($this->getTableField(), 0, 3) && strrpos($key, ".") === FALSE) {
					$key = $this->getTableField()."_".$key;
				}
				$where .= " AND ".(strrpos($key, ".") === FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
			}
		}


		$all_headings = array_merge($idp_headings);

		$listObject = new EMP1_LIST();

		//$list_headings = $headObject->getAllListHeadings();
		//$list_tables = $listObject->getAllListTables(array_keys($list_headings));


		foreach($all_headings as $fld => $head) {
			$lj_tblref = $head['section'];
			if($head['type'] == "LIST") {
				$listObject->changeListType($head['list_table']);
				$sql .= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])."
										AS ".$head['list_table']."
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
										AND (".$head['list_table'].".status & ".EMP1::DELETED.") <> ".EMP1::DELETED;
			} elseif($head['type'] == "MASTER") {
				$tbl = $head['list_table'];
				$masterObject = new EMP1_MASTER($fld);
				$fy = $masterObject->getFields();
				$sql .= ", ".$fld.".".$fy['name']." as ".$tbl;
				$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
			} elseif($head['type'] == "USER") {
				$sql .= ", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
				$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
			}
		}
		$sql .= " FROM ".$from.implode(" ", $left_joins);
		$sql .= " WHERE ".$where;
		switch($section) {
			case "ACTIVE":
				$sql .= " AND ".$idpObject->getActiveStatusSQL($idp_tblref);
				break;
			case "NEW":
				if($page == "ACTIVATE_WAITING") {
					$sql .= " AND ".$idpObject->getActivationStatusSQL($idp_tblref);
				} elseif($page == "ACTIVATE_DONE") {
					$sql .= " AND ".$idpObject->getReportingStatusSQL($idp_tblref);
				} else {
					$sql .= " AND ".$idpObject->getNewStatusSQL($idp_tblref);
				}
				break;
			case "MANAGE":
				$sql .= " AND ".$idpObject->getReportingStatusSQL($idp_tblref);
				break;
			case "REPORT":
			case "SEARCH":
				$sql .= " AND ".$idpObject->getReportingStatusSQL($idp_tblref);
				break;
		}
		//$sql.= isset($parent_id) ? " AND ".$this->getParentFieldName()." = ".$parent_id : "";
		$sql .= isset($group_by) && strlen($group_by) > 0 ? $group_by : "";
		$mnr = $this->db_get_num_rows($sql);
		if($trigger_rows == true) {
			$start = ($start != false && is_numeric($start) ? $start : "0");
			$sql .= (strlen($order_by) > 0 ? " ORDER BY ".$order_by : " ORDER BY ".$where_order).($limit !== false ? " LIMIT ".$start." , $limit " : "");
			//echo $sql;
			//return array($sql);
			$rows = $this->mysql_fetch_all_by_id($sql, $id_fld);
			$all_my_lists = array();
			foreach($all_headings as $fld => $head) {
				switch($head['type']) {
					case "MULTILIST":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$listObject = new EMP1_LIST($head['list_table']);
							$all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
							unset($listObject);
						}
						//check each row and replace as needed
						foreach($rows as $key => $r) {
							$val = explode(";", $r[$fld]);
							$rows[$key][$fld] = array();
							foreach($val as $x) {
								if(isset($all_my_lists[$fld][$x])) {
									$rows[$key][$fld][] = $all_my_lists[$fld][$x];
								}
							}
							if(count($rows[$key][$fld]) > 0) {
								$rows[$key][$fld] = implode("; ", $rows[$key][$fld]);
							} else {
								$rows[$key][$fld] = $this->getUnspecified();
							}
						}
						break;
					case "OBJECT":
						//get list items if not already available
						if(!isset($all_my_lists[$fld])) {
							$list_object_name = $head['list_table'];
							$listObject = new $list_object_name();
							$all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect();
							unset($listObject);
						}
						//check each row and replace as needed
						foreach($rows as $key => $r) {
							$x = $r[$fld];
							if(isset($all_my_lists[$fld][$x])) {
								$rows[$key][$fld] = $all_my_lists[$fld][$x];
							} else {
								$rows[$key][$fld] = $this->getUnspecified();
							}
						}
						break;
					case "COMPETENCY_PROFICIENCY":
						$cpObject = new EMP1_COMPETENCY_PROFICIENCY();
						$cp_rows = $cpObject->getRecordsForListDisplay(array_keys($rows));
						foreach($rows as $key => $r) {
							$v = "<ul><li>".implode("</li><li>", $cp_rows[$key])."</li></ul>";
							$rows[$key][$fld] = $v;
						}
						break;
				}
			}

			$final_data = $this->formatRowDisplay($mnr, $rows, $final_data, $id_fld, $headObject, $ref_tag, false, array('limit' => $limit, 'pagelimit' => $pagelimit, 'start' => $start));
		} else {
			$final_data['rows'] = array();
		}
		$final_data['head'] = $this->replaceObjectNames($final_data['head']);

		return $final_data;
	}

	function formatRowDisplay($mnr, $rows, $final_data, $id_fld, $headObject, $ref_tag, $status_id_fld, $paging) {
		$limit = $paging['limit'];
		$pagelimit = $paging['pagelimit'];
		$start = $paging['start'];
		//ASSIST_HELPER::arrPrint($rows);
		$keys = array_keys($rows);
		$displayObject = new EMP1_DISPLAY();
		foreach($rows as $r) {
			$row = array();
			$i = $r[$id_fld];
			foreach($final_data['head'] as $fld => $head) {
				if($head['parent_id'] == 0) {
					if($headObject->isListField($head['type']) && $head['type'] != "DELIVERABLE") {
						$row[$fld] = strlen($r[$head['list_table']]) > 0 ? $r[$head['list_table']] : $this->getUnspecified();
						if($status_id_fld !== false && $fld == $status_id_fld) {
							if(($r['my_status'] & EMP1::AWAITING_APPROVAL) == EMP1::AWAITING_APPROVAL) {
								$row[$fld] .= " (Awaiting approval)";
							} elseif(($r['my_status'] & EMP1::APPROVED) == EMP1::APPROVED) {
								$row[$fld] .= " (Approved)";
							}
						}
					} elseif($this->isDateField($fld)) {
						$row[$fld] = $displayObject->getDataField("DATE", $r[$fld], array('include_time' => false));
					} else {
						//$row[$fld] = $r[$fld];
						$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld], array('right' => true, 'html' => true, 'reftag' => $ref_tag));
					}
				}
			}
			$final_data['rows'][$i] = $row;
			//$mnr++;
		}

		if($mnr == 0 || $limit === false) {
			$totalpages = 1;
			$currentpage = 1;
		} else {
			$totalpages = round(($mnr / $pagelimit), 0);
			$totalpages += (($totalpages * $pagelimit) >= $mnr ? 0 : 1);
			if($start == 0) {
				$currentpage = 1;
			} else {
				$currentpage = round($start / $pagelimit, 0);
				$currentpage += (($currentpage * $pagelimit) > $start ? 0 : 1);
			}
		}
		$final_data['paging'] = array(
			'totalrows' => $mnr,
			'totalpages' => $totalpages,
			'currentpage' => $currentpage,
			'pagelimit' => $pagelimit,
			'first' => ($start == 0 ? false : 0),
			'prev' => ($start == 0 ? false : ($start - $pagelimit)),
			'next' => ($totalpages == $currentpage ? false : ($start + $pagelimit)),
			'last' => ($currentpage == $totalpages ? false : ($totalpages - 1) * $pagelimit),
		);
		return $final_data;
	}


	//public function getDetailedObject($obj_type,$id,$options=array()) {
	//die("getDetailedObject error");
	//}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}



	/***********************************
	 * GET functions
	 */

	/**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
	public function drawPageFooter($left = "", $log_table = "", $var = "") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
		echo "WRONG PAGE FOOTER";
		return "";
	}

	/**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
	public function getPageFooter($left = "", $log_table = "", $var = "") {
		/*    	$echo = "";
				$js = "";
				if(is_array($var)) {
					$x = $var;
					$d = array();
					unset($var);
					foreach($x as $f => $v) {
						$d[] = $f."=".$v;
					}
					$var = implode("&",$d);
				}
				$echo = "
				<table width=100% class=tbl-subcontainer><tr>
					<td width=50%>".$left."</td>
					<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
						<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
					</span>" : "")."</td>
				</tr></table><div id=div_audit_log></div>";
				if(strlen($log_table)>0){
					$js = "
					$(\"#disp_audit_log\").click(function() {
						var state = $(this).attr('state');
						if(state==\"show\"){
							$(this).find('img').prop('src','/pics/tri_down.gif');
							$(this).attr('state','hide');
							$(\"#div_audit_log\").html(\"\");
							$(\"#log_txt\").html('Display Activity Log');
						} else {
							$(this).find('img').prop('src','/pics/tri_up.gif');
							$(this).attr('state','show');
							var dta = '".$var."&log_table='+$(this).attr('table');
							var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
							$(\"#div_audit_log\").html(result[0]);
							$(\"#log_txt\").html('Hide Activity Log');
						}

					});
					";
				}
				$data = array('display'=>$echo,'js'=>$js);
				return $data;
		 **/
		return array('display' => "WRONG PAGE FOOTER", 'js' => "");
	}

	public function drawActivityLog() {

	}




	/*********************************************
	 * SET/UPDATE functions
	 */
	//public function addActivityLog($log_table,$var) {
	//	$logObject = new EMP1_LOG($log_table);
	//	$logObject->addObject($var);
	//}


	public function notify($data, $type, $object) {
		$noteObj = new EMP1_NOTIFICATION($object);
		$result = $noteObj->prepareNote($data, $type, $object);
		return $result;
	}


	public function editMyObject($var, $extra_logs = array()) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$headObject = new EMP1_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(), "FORM");
		$mar = 0;
		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld => $v) {
			if(isset($headings['rows'][$fld])) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type'] == "DATE") {
					if(strlen($v) > 0) {
						$insert_data[$fld] = date("Y-m-d", strtotime($v));
					}
				} elseif(in_array($headings['rows'][$fld]['type'], array("TARGET", "COMPETENCY"))) {
					//unset($var[$fld]);
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}


		$old = $this->getRawObject($object_id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

		if($mar > 0 || count($extra_logs) > 0) {
			$changes = array(
				'user' => $this->getUserName(),
			);
			foreach($insert_data as $fld => $v) {
				$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type' => "TEXT");
				if($old[$fld] != $v || $h['type'] == "HEADING") {
					if(in_array($h['type'], array("LIST", "MASTER", "USER"))) {
						$list_items = array();
						$ids = array($v, $old[$fld]);
						switch($h['type']) {
							case "LIST":
								$listObject = new EMP1_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$userObject = new EMP1_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new EMP1_MASTER($head['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break;
						}

						unset($listObject);
						$changes[$fld] = array('to' => $list_items[$v], 'from' => $list_items[$old[$fld]], 'raw' => array('to' => $v, 'from' => $old[$fld]));
					} else {
						$changes[$fld] = array('to' => $v, 'from' => $old[$fld]);
					}
				}
			}
			$changes = array_merge($changes, $extra_logs);
			$log_var = array(
				'object_id' => $object_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok", $this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been updated successfully.", array($log_var, $this->getMyObjectType()));
		}
		return array("info", "No changes were found to be saved.  Please try again.");
	}










	/********************************************
	 * PROTECTED functions
	 */


	/*******************************************
	 * PRIVATE functions
	 */


	public function __destruct() {
		parent::__destruct();
	}


}


?>