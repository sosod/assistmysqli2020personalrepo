<?php
/**
 * Class EMP1_NOTE
 * Purpose: To manage the Notes tab in the EMP1/Employee Assist module
 * Author: Sondelani Dumalisile
 * Date Created: 10 Jan 2021
 * YouTrack: AA-406
 */

class EMP1_NOTE extends EMP1 {
	protected $object_id = 0;
	protected $ref_tag = "N";
	protected $status_field = "_status";
	protected $parent_field = "_empid";
	protected $id_field = "_id";
	private $modref;
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "NOTES";
	const OBJECT_NAME = "notes";
	const OBJECT_NAME2 = "Note";//used on log response
	const PARENT_OBJECT_TYPE = null;

	const TABLE = "notes";
	const TABLE_FLD = "note";
	const REFTAG = "ERROR/CONST/REFTAG";
	const LOG_TABLE = "object";

	public function __construct($obj_id = 0, $modref = "") {
		$this->modref = $modref;
		parent::__construct($modref);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		//$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		//if($obj_id>0) {
		//	$this->object_id = $obj_id;
		//	$this->object_details = $this->getAObject($obj_id);
		//}
		$this->object_form_extra_js = "";
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getEmptyTableNotice() {
		return "No ".self::OBJECT_NAME." found to display.";
	}

	public function getNoteFullDetailByEmployeeID($emp_id = false, $ActiveNotes = false) {
		if($emp_id === false || (is_array($emp_id) && count(ASSIST_HELPER::removeBlanksFromArray($emp_id)) == 0)) {
			return array();
		} else {
			$sql = "SELECT note_id
					, CONCAT('".$this->getRefTag()."',note_id) as ref
					, note_comment as note_comment
					, note_insertdate as note_insertdate
					, note_status as note_status
					, note_deletetime as note_deletetime
					, CONCAT(user.tkname,' ',user.tksurname) as note_insertuser
					, IF( ((note_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE."),1,0) as is_active
					FROM ".$this->getTableName()." notes
					LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep user
					  ON notes.note_insertuser = user.tkid
					WHERE "; //.$this->getActiveStatusSQL("notes")
			if($ActiveNotes == true) {
				$sql .= $this->getActiveStatusSQL("notes");
				if(is_array($emp_id)) {
					$sql .= " AND ".$this->getParentFieldName()." IN (".implode(",", $emp_id).")";
				} else {
					$sql .= " AND ".$this->getParentFieldName()." = $emp_id ";
				}
			} else {
				if(is_array($emp_id)) {
					$sql .= $this->getParentFieldName()." IN (".implode(",", $emp_id).")";//AND - was removed
					$sql .= " AND note_status <>".EMP1::DELETED;
				} else {
					$sql .= $this->getParentFieldName()." = $emp_id ";//AND - was removed
					$sql .= " AND note_status <>".EMP1::DELETED;
				}
			}
			$sql .= " ORDER BY note_insertdate DESC, note_id ASC ;";    //include status sort so that latest doc status always sorts top


			$final_data = $this->mysql_fetch_all_by_id($sql, "note_id");

			return $final_data;
		}
	}


	public function getRawObject($obj_id, $format_date = true) {
		if(is_array($obj_id)) {
			if(isset($obj_id['obj_id'])) {
				$obj_id = $obj_id['obj_id'];
			} else {
				return array();
			}
		}
		$sql = "SELECT * , CONCAT(user.tkname,' ',user.tksurname) as note_insertuser FROM ".$this->getTableName()." notes LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep user
					  ON notes.note_insertuser = user.tkid WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$row = $this->mysql_fetch_one($sql);


		if($format_date) {

			$row['note_insertdate'] = date(" d M Y H:i ", strtotime($row['note_insertdate']));
		}


		return $row;
	}


	public function editObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		unset($var['form_action']);
		$note_id = $var['note_id'];
		unset($var['note_id']);
		$emp_id = $var['note_empid'];
		unset($var['note_empid']);

		$insert_data = array();

		$original = $this->getRawObject($note_id);
		if(($original['note_status'] & EMP1::ACTIVE) == EMP1::ACTIVE) {
			$is_current = true;
		} else {
			$is_current = false;
		}
		foreach($var as $fld => $v) {
			if($v != $original[$fld]) {
				if($fld == "note_insertdate") {
					$v = date("Y-m-d", strtotime($v));
					//Create a note if the insert date changed

				} elseif($fld == "note_comment") {
					$insert_data[$fld] = ASSIST_HELPER::code($v);
				}
				$insert_data[$fld] = ASSIST_HELPER::code($v);
				//$new_record[$fld] =  ASSIST_HELPER::code($v);
			}
		}


		if(count($insert_data) > 0) {
			//if there are changes then save to the database
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE ".$this->getIDFieldName()." = ".$note_id." AND ".$this->getParentFieldName()." = ".$emp_id;
			$mar = $this->db_update($sql);
			//log the changes
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$note_id." edited.",
				'user' => $this->getUserName(),
			);
			foreach($insert_data as $fld => $new_raw) {
				if($fld == "note_status") {
					if(($new_raw & EMP1::ACTIVE) == EMP1::ACTIVE) {
						$changes['log_display_comment'] = "Note status changed to 'Active'";
					} else {
						$changes['log_display_comment'] = "Note status changed to 'Inactive'";
					}
				} elseif($fld == "note_comment") {
					$changes[$fld] = array(
						'to' => $new_raw,
						'from' => $original[$fld],
						'raw' => array(
							'to' => $new_raw,
							'from' => $original[$fld]
						)
					);
				} elseif($fld == "job_manager") {

				} else {
					$old_raw = $original[$fld];
					$listObject = new EMP1_LIST($fld);
					$items = $listObject->getAListItemName(array($new_raw, $old_raw));
					$new = isset($items[$new_raw]) && $new_raw != 0 ? $items[$new_raw] : $this->getUnspecified();
					$old = isset($items[$old_raw]) && $old_raw != 0 ? $items[$old_raw] : $this->getUnspecified();

					$changes[$fld] = array(
						'to' => $new,
						'from' => $old,
						'raw' => array(
							'to' => $new_raw,
							'from' => $old_raw
						)
					);
				}
			}
			$log_var = array(
				'object_id' => $note_id,
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			//if changes were made to the previous current job then log the changes & update employee data in user table


			//result
			$result = array("ok", "".$this->getObjectName(self::OBJECT_NAME)." ".$this->getRefTag().$note_id." edited successfully.", 'object_id' => $note_id);
		} else {
			$result = array("info", "No changes found to be saved.", 'object_id' => $note_id);
		}
		return $result;

	}

//new function - add new job record after converting employee
	public function addObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		unset($var['form_action']);
		unset($var['note_id']);
		$emp_id = $var['note_empid'];
		unset($var['note_empid']);


		//set default fields
		$insert_data = array(
			self::TABLE_FLD.'_empid' => $emp_id,
			self::TABLE_FLD.'_status' => EMP1::ACTIVE,
			self::TABLE_FLD.'_insertdate' => date("Y-m-d H:i:s"),
			self::TABLE_FLD.'_insertuser' => $this->getUserID(),
			self::TABLE_FLD.'_deletetime' => "NULL" //AT MIDNIGHT NOTES ARE DELETED <> NULL (not equal to null)
		);


		foreach($var as $fld => $v) {
			if($fld == "note_insertdate") {
				$v = date("Y-m-d h:i:s", strtotime($v));
			} elseif($fld == "note_comment") {
				$insert_data[$fld] = ASSIST_HELPER::code($v);
			}
			if(!isset($insert_data[$fld])) {
				$insert_data[$fld] = ASSIST_HELPER::code($v);
			}
		}

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
		$note_id = $this->db_insert($sql);
		$changes = array(
			'response' => "New |".self::OBJECT_NAME."| ".$this->getRefTag().$note_id." created.",
			'user' => $this->getUserName(),
		);
		$log_var = array(
			'object_id' => $note_id,
			'emp_id' => $emp_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => EMP1_LOG::CREATE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

		//if changes were made to the previous current job then log the changes


		$result = array("ok", $this->getObjectName(self::OBJECT_NAME2)." ".$this->getRefTag().$note_id." added successfully.", 'object_id' => $note_id);


		return $result;

	}

	//new function - delete note record
	public function deleteObject($var, $headings = array()) {
		$result = array("error", "Nothing done.");

		$note_id = $var['note_id'];
		unset($var['note_id']);
		$emp_id = $var['note_empid'];
		unset($var['note_empid']);
		date_default_timezone_set('Africa/Johannesburg');
		$midnight = strtotime("tomorrow 00:00:00");
		$deleteDate = date("Y-m-d H:i:s", $midnight);
		if(ASSIST_HELPER::checkIntRef($note_id)) {
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$this->getStatusFieldName()." - ".self::ACTIVE." + ".self::INACTIVE." ,note_deletetime ='".$deleteDate."' WHERE ".$this->getIDFieldName()." = ".$note_id;
			$mar = $this->db_update($sql);
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$note_id." deleted.",
				'user' => $this->getUserName(),
			);
			$log_var = array(
				'object_id' => $note_id,
				'emp_id' => $emp_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			$result = array("ok", $this->replaceAllNames($changes['response']));
		} else {
			$result = array("error", "Invalid ".$this->getObjectName($this->getMyObjectType()));
		}

		return $result;

	}

	public function restoreObject($var) {
		$result = array("error", "Error");

		$note_id = $var['note_id_from_button'];
		unset($var['note_id_from_button']);
		$emp_id = $var['note_empid'];
		unset($var['note_empid']);

		$old_data = $this->getRawObject($note_id, true);

		//$old_status = $old_data[$this->getStatusFieldName()];
		// $new_status = self::ACTIVE;

		$deleteDate = "NULL";
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$this->getStatusFieldName()." - ".self::INACTIVE." + ".self::ACTIVE." ,note_deletetime ='".$deleteDate."' WHERE ".$this->getIDFieldName()." = ".$note_id;
		$this->db_update($sql);

		$changes = array(
			'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$note_id." restored.",
			'user' => $this->getUserName(),
			/*'status'=>array(
				'to'=>"Active",
				'from'=>"Deleted",
				'raw'=>array(
					'to'=>$new_status,
					'from'=>$old_status,
				)
			),*/
		);
		$log_var = array(
			'object_id' => $note_id,
			'emp_id' => $emp_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => EMP1_LOG::RESTORE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array("ok", $this->replaceObjectNames($changes['response']));

		return $result;
	}

}