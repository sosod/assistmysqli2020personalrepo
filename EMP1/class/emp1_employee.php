<?php
/**
 * To manage the ACTION object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class EMP1_EMPLOYEE extends EMP1 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_tkid";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";

	protected $has_attachment = false;

	protected $ref_tag = "EMP";
	private $modref;

	private $default_user_details = array('tkstatus' => "Non-user", 'emp_status' => "New Active Employee");
	/*************
	 * CONSTANTS
	 */
	const OBJECT_TYPE = "EMPLOYEE";
	const OBJECT_NAME = "employee";
	const PARENT_OBJECT_TYPE = "USER";

	const TABLE = "employee";
	const TABLE_FLD = "emp";
	const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "object";

	public function __construct($obj_id = 0, $modref = "") {
		$this->modref = $modref;
		parent::__construct($modref);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		//$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		//if($obj_id>0) {
		//	$this->object_id = $obj_id;
		//	$this->object_details = $this->getAObject($obj_id);
		//}
		$this->object_form_extra_js = "";
	}

	/**********************
	 * Function to add a new Non-User Employee
	 * 1. Create Inactive User
	 * 2. Create Employee & link to Inactive User
	 * 3. Create Job & link to Employee
	 */
	public function addObject($var) {
		$result = array("error", serialize($var));
		$add_action = $var['form_type'];

		//If action = ADD then a new Inactive user needs to be created
		$userObject = new EMP1_USER();
		if($add_action == "ADD") {
			$emp_status = self::NON_USER;
			//1. Create user
			$user_result = $userObject->addInactiveUser($var);
		} else {
			if($add_action == "CONVERT_INACTIVE") {
				$emp_status = self::NON_USER;
			} else {
				$emp_status = self::ASSIST_USER;
			}
			$user_result = $userObject->getAddResultsForActiveUser($var);
		}
		//2. If User successfully created, create employee
		if($user_result['result'] === true && strlen($user_result['tkid']) > 0) {
			$var['tkid'] = $user_result['tkid'];
			//GET EMPLOYEE HEADINGS
			$headObject = new EMP1_HEADINGS();
			//get all headings so that JOB portion can be sent to JOB's Add Object function without requiring another DB call
			$full_headings = $headObject->getMainObjectHeadings("");
			$employee_heading = $full_headings['rows']['employee_heading'];
			$headings = $full_headings['sub'][$employee_heading['id']];
			//GET EMPLOYEE DATA
			$insert_data = array(
				self::TABLE_FLD.'_tkid' => $var['tkid'],
				self::TABLE_FLD.'_status' => self::ACTIVE + $emp_status,
				self::TABLE_FLD.'_insertdate' => date("Y-m-d H:i:s"),
				self::TABLE_FLD.'_insertuser' => $this->getUserID()
			);
			foreach($headings as $h_id => $head) {
				$fld = $head['field'];
				if(!isset($insert_data[$fld])) {
					if(isset($var[$fld])) {
						$v = $var[$fld];
						switch($head['type']) {
							case "DATE":
								if(strlen($v) > 0) {
									$v = date("Y-m-d", strtotime($v));
								}
								break;
						}
					} else {
						$v = "0";
					}
					$insert_data[$fld] = $v;
				}
			}
			//CREATE EMPLOYEE
			$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
			$emp_id = $this->db_insert($sql);
			if($emp_id !== false && $emp_id > 0) {
				$var['emp_id'] = $emp_id;
				//LOG CREATION!!!!
				$changes = array(
					'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$emp_id." created".(($emp_status & self::ASSIST_USER) == self::ASSIST_USER ? " from ".$this->getObjectName("USER")." '".$var['tkid']."'" : "").".",
					'user' => $this->getUserName(),
				);
				$log_var = array(
					'object_id' => $emp_id, //id of the objecxt you working on
					'emp_id' => $emp_id,//id of the object parent
					'object_type' => $this->getMyObjectType(),
					'changes' => $changes,
					'log_type' => EMP1_LOG::CREATE,
				);
				$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);

				//3. If Employee successfully created, create job
				$jobObject = new EMP1_JOB();
				$job_result = $jobObject->addObjectForNewEmployee($var, $full_headings);
				if($job_result['result'] === true) {
					if($add_action != "ADD") {
						//Update timekeep table with any changes (maintain old PMSPS module)
						//Not needed for new inactive users - this has been already handled in the create process
						$this->sendEmployeeDataToUsersTable($user_result['tkid'], $var);
					}
					//Everything successful = return ok
					$result = array("ok", "New ".$this->getObjectName("EMPLOYEE")." '".$var['tkname']." ".$var['tksurname']."' successfully created.", "ok");
				} else {
					//USER & EMPLOYEE successful but JOB failed - direct user to Manage > Edit function
					$result = array("ok", "New ".$this->getObjectName("EMPLOYEE")." '".$var['tkname']." ".$var['tksurname']."' successfully created BUT an error occured while trying to add their ".$this->getObjectName("JOB")." details.  Please use the Edit function to add their ".$this->getObjectName("JOB")." details.", "warn");
				}
				if($this->isIASClient() && strlen($user_result['tkid']) > 0) {
					$userObject->addPMSPSRecord($user_result['tkid']);
				}
			} else {
				//USER success but EMPLOYEE failed - redirect user to New > Convert Inactive User
				$result = array("ok", "New ".$this->getObjectName("EMPLOYEE")." '".$var['tkname']." ".$var['tksurname']."' successfully created BUT an error occured while trying to add their ".$this->getObjectName("EMPLOYEE")." and ".$this->getObjectName("JOB")." details.  Please use the New > Convert Inactive User function to add the missing details.", "warn");
			}
		} else {
			//USER failed
			$result = array("error", "An error occurred while trying to add the new ".$this->getObjectName("EMPLOYEE").".  Please try again.");
		}

		//Return result
		return $result;
	}


	public function editObject($var) {
		$result = array("error", serialize($var));
		$var['tkid'] = $var['original_tkid'];
		$tkid = $var['tkid'];
		$object_id = $var['object_id'];
		$var['emp_id'] = $object_id;
		$save_result = array(
			'USER' => "error",
			'EMPLOYEE' => "error",
		);

		//get headings to poll fields
		$headObject = new EMP1_HEADINGS();
		//get all headings so that JOB portion can be sent to JOB's Add Object function without requiring another DB call
		$full_headings = $headObject->getMainObjectHeadings("");

		$employee_heading = $full_headings['rows']['employee_heading'];
		$headings = $full_headings['sub'][$employee_heading['id']];

		//start with saving user changes
		$userObject = new EMP1_USER();
		$user_result = $userObject->editObject($var, $full_headings);
		$save_result['USER'] = $user_result[0];

		if($user_result[0] == "ok" || $user_result[0] == "info") {
			//continue with save process
			//get current employee details
			$old_data = $this->getRawEmployeeDetailsByEmployeeID($object_id);
			//compare current details with submitted details & if different save change & log it
			$update_data = array();
			$update_changes = array();
			foreach($headings as $h_id => $head) {
				$fld = $head['field'];
				if(!isset($update_data[$fld]) && isset($var[$fld])) {
					$new = $var[$fld];
					$old = isset($old_data[$fld]) ? $old_data[$fld] : "";
					if($old != $new) {
						switch($head['type']) {
							case "EMPLOYEE":
								$items = $this->getListOfEmployeesFormattedForSelect();
								$to = isset($items[$new]) ? $items[$new] : $this->getUnspecified;
								$from = isset($items[$old]) ? $items[$old] : $this->getUnspecified;
								break;
							case "LIST":
								$listObject = new EMP1_LIST($head['list_table']);
								$items = $listObject->getAllListItemsFormattedForSelect();
								$to = isset($items[$new]) ? $items[$new] : $this->getUnspecified;
								$from = isset($items[$old]) ? $items[$old] : $this->getUnspecified;
								break;
							case "MASTER":
								$class = "ASSIST_MASTER_".strtoupper($head['list_table']);
								$listObject = new $class();
								$items = $listObject->getAllItemsFormattedForSelect();
								$to = isset($items[$new]) ? $items[$new] : $this->getUnspecified;
								$from = isset($items[$old]) ? $items[$old] : $this->getUnspecified;
								break;
							case "DATE":
								if(strlen($new) > 0) {
									$new = date("Y-m-d", strtotime($new));
									$to = date("d-M-Y", strtotime($new));
								} else {
									$to = $this->getUnspecified();
									$new = "0000-00-00";
								}
								if(strlen($old) == 0 || $old == "0000-00-00" || is_null($old)) {
									$from = $this->getUnspecified();
								} else {
									$from = date("d-M-Y", strtotime($old));
								}
								break;
							default:
								$to = $new;
								$from = $old;
								break;
						}
						$update_data[$fld] = $new;
						$update_changes[$fld] = array(
							'to' => $to,
							'from' => $from,
							'raw' => array(
								'to' => $new,
								'from' => $old
							)
						);
					}
				}
			}
			if(count($update_data) > 0) {
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
				$mar = $this->db_update($sql);
				if($mar > 0) {
					$save_result['EMPLOYEE'] = "ok";
					//LOG !!!!
					$changes = array(
						'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." edited.",
						'user' => $this->getUserName(),
					);
					$changes = array_merge($changes, $update_changes);
					$log_var = array(
						'object_id' => $object_id,
						'emp_id' => $object_id,
						'object_type' => $this->getMyObjectType(),
						'changes' => $changes,
						'log_type' => EMP1_LOG::EDIT,
					);
					$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
				} else {
					$save_result['EMPLOYEE'] = "info";
				}
			} else {
				$save_result['EMPLOYEE'] = "info";
			}

			/*
			 * No longer save changes to JOB - handled separately
						//save changes to JOB object
						$jobObject = new EMP1_JOB();
						$job_result = $jobObject->editObject($var,$full_headings);
						$save_result['JOB'] = $job_result[0];
						//If there was a change then submit changes to users table to maintain old PMSPS module
						if($save_result['JOB']=="ok") {
						}
			*/
			if($save_result['USER'] != "error" && $save_result['EMPLOYEE'] != "error") {// && $save_result['JOB']!="error") {
				$this->sendEmployeeDataToUsersTable($tkid, $var);
				$result = array("ok", "".$this->getObjectName("EMPLOYEE")." '".$var['tkname']." ".$var['tksurname']."' successfully edited.");
			} else {
				$result = array("error", "An error occurred while trying to update the details.");
			}

		} else {
			//assume error on USER edit so don't continue
			$result = array("error", "An error occurred while trying to update the details.");
		}


		//Return result
		return $result;
	}


	public function makeInactiveUser($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE emp_tkid = '$object_id'";
		$old = $this->mysql_fetch_one($sql);
		if(is_null($old) || !is_array($old) || count($old) == 0) {
			return true;
		} else {
			$old_status = $old[$this->getStatusFieldName()];
			$new_status = self::ACTIVE + self::NON_USER;
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".($new_status)." WHERE emp_tkid = '$object_id'";
			$this->db_update($sql);
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." converted to Non-User Employee.",
				'user' => $this->getUserName(),
				'tkstatus' => array(
					'to' => "Non-User Employee",
					'from' => "Active Assist User",
					'raw' => array(
						'to' => $new_status,
						'from' => $old_status,
					)
				),
			);
			$log_var = array(
				'object_id' => $object_id,
				'emp_id' => $object_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			return true;
		}

	}


	public function makeActiveUser($object_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE emp_tkid = '$object_id'";
		$old = $this->mysql_fetch_one($sql);
		if(is_null($old) || !is_array($old) || count($old) == 0) {
			return true;
		} else {
			$old_status = $old[$this->getStatusFieldName()];
			$new_status = EMP1::ACTIVE + EMP1::ASSIST_USER;
			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".($new_status)." WHERE emp_tkid = '$object_id'";
			$this->db_update($sql);
			$changes = array(
				'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." converted to Active Assist User.",
				'user' => $this->getUserName(),
				'tkstatus' => array(
					'to' => "Active Assist User",
					'From' => "Non-User Employee",
					'raw' => array(
						'to' => $new_status,
						'from' => $old_status,
					)
				),
			);
			$log_var = array(
				'object_id' => $object_id,
				'emp_id' => $object_id,
				'object_type' => $this->getMyObjectType(),
				'changes' => $changes,
				'log_type' => EMP1_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
			return true;
		}

	}


	public function restoreObject($var) {
		$result = array("error", "Error");
		$var['tkid'] = $var['original_tkid'];
		$tkid = $var['tkid'];
		$object_id = $var['object_id'];
		$var['emp_id'] = $object_id;
		if(isset($var['restartcontract'])) {
			$restart_Contract = $var['restartcontract'];
		} else {
			$restart_Contract = false;
		}
		$old_data = $this->getRawEmployeeDetailsByEmployeeID($object_id);

		//TERMINATING EMPLOYEE MUST NOT AFFECT USER STATUS UNLESS CLIENT HAS OLD IAS PMS MODULE
		//see comment on terminateObject on why this check has been removed - JC 3 July 2018
		//$is_ias_client = $this->isIASClient();
		//if($is_ias_client) {
		$userObject = new EMP1_USER();
		$user_result = $userObject->restoreObject($var);
		//}

		$old_status = $old_data[$this->getStatusFieldName()];
		$new_status = $old_status - self::INACTIVE + self::ACTIVE;

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		if(isset($restart_Contract) && $restart_Contract == true) {
			$userJob = new EMP1_JOB();
			$ended_contract_record = $userJob->getRecentEndedJobsPerEmployee($object_id);
			$ended_contract_record = $ended_contract_record[$object_id];
			$old_start = strtotime($ended_contract_record['job_start']);
			$new_start = strtotime("0000-00-00");
			$userJob->restartContract($object_id, $ended_contract_record['job_id']);

		}

		$changes = array(
			'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." restored.",
			'user' => $this->getUserName(),
			'tkstatus' => array(
				'to' => "Active",
				'from' => "Terminated",
				'raw' => array(
					'to' => $new_status,
					'from' => $old_status,
				)
			),
		);
		if(isset($restart_Contract) & $restart_Contract == true) {
			$changes['log_display_comment'] = "Job status changed to 'Current' due to change in start date";
		}

		$log_var = array(
			'object_id' => $object_id,
			'emp_id' => $object_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => EMP1_LOG::RESTORE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array("ok", $this->replaceObjectNames($changes['response']));

		return $result;
	}


	public function terminateObject($var) {
		$result = array("error", "Error");
		$var['tkid'] = $var['original_tkid'];
		$tkid = $var['tkid'];
		$object_id = $var['object_id'];
		$var['emp_id'] = $object_id;

		$old_data = $this->getRawEmployeeDetailsByEmployeeID($object_id);

		//TERMINATING EMPLOYEE MUST NOT AFFECT USER STATUS UNLESS CLIENT HAS OLD IAS PMS MODULE
		//Check for IAS PMS module has been removed on 3 July 2018 by JC due to helpdesk request IGN0001/R838 where TK > Manage > Users > Non-Users section not clear enough for clients on status of non-users & IAS PMS module not picking up tkstatus = 4 as terminated
		//EMP1_USER won't act is tkstatus = 1
		//$is_ias_client = $this->isIASClient();
		//if($is_ias_client) {
		$userObject = new EMP1_USER();
		$user_result = $userObject->terminateObject($var);
		//}

		$old_status = $old_data[$this->getStatusFieldName()];
		$new_status = $old_status - self::ACTIVE + self::INACTIVE;

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$this->db_update($sql);

		//get current contract record
		$userJob = new EMP1_JOB();
		$current_contract_record = $userJob->getLatestJobs($object_id);
		$current_contract_record = $current_contract_record[$object_id];
		date_default_timezone_set('Africa/Johannesburg');
		$old_start = strtotime($current_contract_record['job_start']);
		$new_start = strtotime(date("Y-m-d", time()));

		if(isset($current_contract_record['job_id']) || $current_contract_record['job_id'] != "") {
			$userJob->endContract($object_id, $current_contract_record['job_id'], date("Y-m-d", $new_start));
		}

		$changes = array(
			'response' => "|".self::OBJECT_NAME."| ".$this->getRefTag().$object_id." terminated.",
			'user' => $this->getUserName(),
			'tkstatus' => array(
				'to' => "Terminated",
				'from' => "Active",
				'raw' => array(
					'to' => $new_status,
					'from' => $old_status,
				)
			),
		);
		$changes['log_display_comment'] = "Job status changed to 'Ended' due to terminated employee.";
		$log_var = array(
			'object_id' => $object_id,
			'emp_id' => $object_id,
			'object_type' => $this->getMyObjectType(),
			'changes' => $changes,
			'log_type' => EMP1_LOG::DELETE,
		);
		$this->addActivityLog(strtolower($this->getMyLogTable()), $log_var);
		$result = array("ok", $this->replaceObjectNames($changes['response']));

		return $result;
	}


	/*******************
	 * Function to update the old timekeep fields to ensure continuation of old PMSPS module
	 */
	public function sendEmployeeDataToUsersTable($tkid, $var) {
		$headObject = new EMP1_HEADINGS();
		$timekeep_map = $headObject->getTimekeepFieldMap();
		$timekeep_map = array_flip($timekeep_map);

		$insert_data = array();

		foreach($timekeep_map as $tk_fld => $emp_fld) {
			if(isset($var[$emp_fld])) {
				if($emp_fld == "emp_date") {
					$var[$emp_fld] = strtotime($var[$emp_fld]);
				}
				$insert_data[$tk_fld] = $var[$emp_fld];
			}
		}

		$sql = "UPDATE ".$this->getUserTableName()." SET ".$this->convertArrayToSQLForSave($insert_data)." WHERE tkid = '$tkid'";
		$this->db_update($sql);
		return true;
	}


	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}

	public function getTableField() {
		return self::TABLE_FLD;
	}

	public function getDefaultUserDetails() {
		return $this->default_user_details;
	}

	public function getRefTag() {
		return $this->ref_tag;
	}

	public function getMyObjectType() {
		return self::OBJECT_TYPE;
	}

	public function getMyObjectName() {
		return self::OBJECT_NAME;
	}

	public function getMyParentObjectType() {
		return self::PARENT_OBJECT_TYPE;
	}

	public function getMyLogTable() {
		return self::LOG_TABLE;
	}

	public function getSelfManagerOption() {
		return "[Self / No Manager]";
	}


	public function getRawEmployeeDetails($emp_id = false, $tkid = "", $include_job_details = true) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ";
		if($emp_id != false) {
			$sql .= $this->getIDFieldName()." = $emp_id ";
		} else {
			if(strlen($tkid) == 0) {
				$tkid = $this->getUserID();
			}
			$sql .= $this->getParentFieldName()." = '".$tkid."' ";
		}
		$data = $this->mysql_fetch_one($sql);
		if(($data[$this->getStatusFieldName()] & self::ASSIST_USER) == self::ASSIST_USER) {
			$data['user_type'] = "active";
		} else {
			$data['user_type'] = "inactive";
		}
		$emp_id = isset($data[$this->getIDFieldName()]) ? $data[$this->getIDFieldName()] : false;	//catch situations where person might be user but not employee [AA-510] JC
		if($include_job_details && $emp_id!==false) {
			$jobObject = new EMP1_JOB(0, $this->modref);
			$job = $jobObject->getLatestJobsPerEmployee($emp_id);
			if(count($job) > 0) {
				$data = array_merge($data, $job[key($job)]);
			}
		}
		return $data;
	}

	public function getRawEmployeeDetailsByUserID($tkid, $include_job_details = true) {
		return $this->getRawEmployeeDetails(false, $tkid, $include_job_details);
	}

	public function getRawEmployeeDetailsByEmployeeID($emp_id, $include_job_details = true) {
		return $this->getRawEmployeeDetails($emp_id, "", $include_job_details);
	}

	public function getList($section, $options = array(), $status_filter = "ACTIVE", $where_filter = array()) {
		if(strpos($section, "_") === false) {
			$status_format = true;
		} else {
			$s = explode("_", $section);
			$section = $s[0];
			$display = $s[1];
			if($display == "onscreen") {
				$status_format = true;
			} else {
				$status_format = false;
			}
		}

		$data = array(
			'head' => array(),
			'rows' => array(),
			'paging' => array(),
		);

		if(!isset($options['start'])) {
			$options['start'] = 0;
		}
		if(!isset($options['limit'])) {
			$options['limit'] = FALSE;
		}
		if(!isset($options['user_filter'])) {
			$where_filter['user_filter'] = "all";
		} else {
			$where_filter['user_filter'] = $options['user_filter'];
		}
		$options_search = isset($options['search']) ? $options['search'] : array();
		$is_edit = isset($options['page']) && $options['page'] == "EDIT_EMPLOYEE" ? true : false;
		//$raw_data = $this->getCurrentListOfEmployees($options['start'],$options['limit']);
		$emp_data = $this->getEmployeeListData($options['start'], $options['limit'], $is_edit, $status_filter, $where_filter,$options_search);
		$raw_data = $emp_data['rows'];
		$jobObject = new EMP1_JOB(0, $this->modref);
		$raw_jobs = $jobObject->getLatestJobsPerEmployee(array_keys($raw_data));
		$headObject = new EMP1_HEADINGS($this->modref);
		$data['head'] = $headObject->getMainObjectHeadings("", "LIST", $section);
		if($section == "REPORT") {
			$data['head']['emp_user_type'] = array(
				'id' => 9999999,
				'field' => "emp_user_type",
				'name' => "User Type",
				'section' => "EMPLOYEE",
				'type' => "EMP_USER_TYPE",
				'list_table' => "",
				'max' => 0,
				'parent_id' => 99999,
				'parent_link' => "",
				'default_value' => "",
				'required' => 0,
				'help' => "",
			);
			if($status_filter == "INACTIVE") {
				$data['head']['emp_termination_date'] = array(
					'id' => 99999999,
					'field' => "emp_termination_date",
					'name' => "Termination Date",
					'section' => "EMPLOYEE",
					'type' => "LOG_DATE",
					'list_table' => "",
					'max' => 0,
					'parent_id' => 999999,
					'parent_link' => "",
					'default_value' => "",
					'required' => 0,
					'help' => "",
				);
			}
		}
		if(!isset($data['head']['emp_tkid'])) {
			$a = array(
				'emp_tkid' => array(
					'id' => 0,
					'field' => "emp_tkid",
					'name' => "Ref",
					'section' => self::OBJECT_TYPE,
					'type' => "REF",
				)
			);
			$data['head'] = array_merge($a, $data['head']);
		}
		$lists = array();
		foreach($data['head'] as $fld => $head) {
			switch($head['type']) {
				case "LIST":
					$listObject = new EMP1_LIST($head['list_table'], $this->modref);
					$lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
					break;
				/*case "MASTER":
					$class = "ASSIST_MASTER_".strtoupper($head['list_table']);
					$listObject = new $class();
					$lists[$fld] = $listObject->getAllItemsFormattedForSelect();
					break;*/
				case "EMPLOYEE":
					$lists[$fld] = array('S' => $this->getSelfManagerOption()) + $this->getListOfEmployeesFormattedForSelect();
					break;
				default:
					break;
			}
		}

		foreach($raw_data as $key => $row) {
			$emp_id = $row[$this->getIDFieldName()];
			$emp_status = $row[$this->getStatusFieldName()];
			if(isset($raw_jobs[$emp_id])) {
				$row = array_merge($row, $raw_jobs[$emp_id]);
			}
			foreach($data['head'] as $fld => $head) {
				if(isset($lists[$fld])) {
					$list = $lists[$fld];
					if(isset($row[$fld]) && isset($list[$row[$fld]])) {
						$row[$fld] = $list[$row[$fld]];
					} else {
						$row[$fld] = $this->getUnspecified();
					}
				} else {
					switch($head['type']) {
						case "DATE":
							if(!is_null($row[$fld]) && $row[$fld] != "0000-00-00" && strtotime($row[$fld]) != 0) {
								$row[$fld] = date("d-M-Y", strtotime($row[$fld]));
							} else {
								$row[$fld] = $this->getUnspecified();
							}
							break;
						case "USER_STATUS":
							//if($status_filter=="INACTIVE" && ($emp_status & EMP1::NON_USER)==EMP1::NON_USER) {
							//	$row[$fld] = $this->getUserStatusOptions(0);
							//} else {
							$row[$fld] = $this->getUserStatusOptions($row[$fld], $status_format);
							//}
							break;
						case "EMP_STATUS":
							$row[$fld] = $this->getEmployeeStatusOptions($row[$fld], $status_format);
							break;
						case "EMP_USER_TYPE":
							$v = $this->getUnspecified();
							if(($emp_status & EMP1::NON_USER) == EMP1::NON_USER) {
								$v = $this->getUserStatusOptions(2, $status_format);
							} elseif(($emp_status & self::ASSIST_USER) == self::ASSIST_USER) {
								$v = $this->getUserStatusOptions(1, $status_format);
							}
							$row[$fld] = $v;
							break;
					}
				}
			}
			$data['rows'][$key] = $row;
		}
		$rows_per_page = $options['limit'];
		$data['paging'] = array(
			'totalpages' => $emp_data['total_pages'],
			'totalrows' => $emp_data['total_rows'],
			'currentpage' => $emp_data['page'],
			'pagelimit' => $rows_per_page,
			'first' => (($emp_data['page'] == 1) ? false : 0),
			'last' => 4,
			'next' => (($emp_data['total_pages'] == $emp_data['page']) ? false : $options['start'] + $rows_per_page),
			'prev' => (($emp_data['page'] == 1) ? false : $options['start'] - $rows_per_page),
		);
		return $data;
	}

	private function getEmployeeListData($start, $limit, $is_edit = false, $status_filter = "ACTIVE", $where_filter = array(),$search) {
		$where_sql = "";
		if(count($where_filter) > 0) {
			foreach($where_filter as $key => $v) {
				switch($key) {
					case "user_filter":
						if($v == "active") {
							$where_sql = " AND TK.tkstatus = 1 ";
							$user_status_filter = "ACTIVE";
						} elseif($v == "inactive") {
							$where_sql = " AND TK.tkstatus > 1 ";
							$user_status_filter = "INACTIVE";
						} else {
							//assume $v = all => don't filter
							$user_status_filter = "ALL";
						}
						break;
				}
			}
		}

		$data = array(
			'rows' => $this->getCurrentListOfEmployees($start, $limit, $is_edit, $status_filter, $user_status_filter,$search),
		);
		/**
		 *  [SD] AA-643 EMP1 - Add search function to list pages
		 *  comment: The search data below come from the generic_list_page.php page
		 */
		$search_type = isset($search['search_type']) ? $search['search_type'] : "";unset($search['search_type']);
		$search_name = isset($search['search_name']) ? $search['search_name'] : "";unset($search['search_name']);
		$headings = isset($search['headings']) ? $search['headings'] : array();unset($search['headings']);
		$sections = isset($search['sections']) ? $search['sections'] : array();unset($search['sections']);

		$sql = "SELECT count(E.emp_id) as total_rows
				FROM ".$this->getTableName()." E
				INNER JOIN  ".$this->getUserTableName()." TK
				ON TK.tkid = E.emp_tkid
				WHERE ";
		if(isset($search_type) && $search_type == "advanced") {
			//when search is happening and is advanced, overwrites the existing original $sql query
			$jobObj = new EMP1_JOB();
			$sql = "SELECT count(E.emp_id) as total_rows
				FROM ".$this->getTableName()." E
				INNER JOIN  ".$this->getUserTableName()." TK
				ON TK.tkid = E.emp_tkid
				INNER JOIN  " . $jobObj->getTableName() . " J
				ON J.job_empid = E.emp_id
				WHERE ";
		}
		switch($status_filter) {
			case "INACTIVE":
				$sql .= " (E.".$this->getStatusFieldName()." & ".self::INACTIVE.") = ".self::INACTIVE;
				break;
			case "ACTIVE":
			default:
				$sql .= " (E.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
							".($is_edit && !isset($search_type)? "OR (E.".$this->getStatusFieldName()." & ".self::INACTIVE.") = ".self::INACTIVE."" : "");
				break;
		}


		if($search_type != "" || $search_name != ""){
			if($search_type == "quick"){

				$nameArray = explode(" ", $search_name);
				foreach($nameArray as $name => $val){
					$nameArray[$name] = "'%".$val."%'";
				}
				$likes = implode(" ",$nameArray);
				$sql .= " AND TK.tkname LIKE ".$likes." OR TK.tksurname LIKE ".$likes;

			}elseif($search_type == "advanced"){
				$glue ="";

				//validate the fields to make sure only the correct data from the $_REQUEST array is passed.
				foreach($sections as $section) {
					$tbl_placeholder = "J";
					switch($section){
						case "JOB": $tbl_placeholder = "J";break;
						case "USER": $tbl_placeholder = "TK";break;
						case "EMPLOYEE": $tbl_placeholder = "E";break;
					}
					$heading_section = strtolower($section)."_heading";
					$main_heading = $headings[$section]['rows'][$heading_section];
					foreach($headings[$section]['sub'][$main_heading['id']] as $id => $head) {
						$heading_fld = $head['field'];
						foreach($search as $fld => $val) {
							if(($heading_fld == $fld) && !empty($val)) {
								if(!is_array($val)) { //pay attention to fields DB fields that are strings and int


									if(ASSIST_HELPER::checkIntRef($val)) {
										$glue .= " AND (" . $tbl_placeholder . "." . $fld . " = " . $val . ")";
									}else{
										$glue .= " AND (" . $tbl_placeholder . "." . $fld . " = '" . $val . "' OR " . $tbl_placeholder . "." . $fld . " LIKE '%".$val."%')";
									}

								} else {
									//If you will be using multiselect you can out your values here

								}
								unset($search[$fld]);
							}

						}
					}
				}
				$sql .= $glue;
			}
		}


		$sql .= $where_sql."
				ORDER BY TK.tkname, TK.tksurname";
		$total_rows = $this->mysql_fetch_one($sql);
		$total_rows = $total_rows['total_rows'];
		$data['total_rows'] = $total_rows;
		$data['total_pages'] = $limit !== false ? ceil($total_rows / $limit) : 1;
		$data['page'] = $limit !== false ? ($start / $limit) + 1 : 1;
		return $data;
	}

	public function getCurrentListOfEmployees($start, $limit, $is_edit = false, $status_filter = "ACTIVE", $user_status_filter = "ALL",$search) {
		/**
		 *  [SD] AA-643 EMP1 - Add search function to list pages
		 *  comment: The search data below come from the generic_list_page.php page
		 */
		$search_type = isset($search['search_type']) ? $search['search_type'] : "";unset($search['search_type']);
		$search_name = isset($search['search_name']) ? $search['search_name'] : "";unset($search['search_name']);
		$headings = isset($search['headings']) ? $search['headings'] : array();unset($search['headings']);
		$sections = isset($search['sections']) ? $search['sections'] : array();unset($search['sections']);


		//when no search is happening
		$sql = "SELECT E.*, TK.*
				FROM " . $this->getTableName() . " E
				INNER JOIN  " . $this->getUserTableName() . " TK
				ON TK.tkid = E.emp_tkid
				WHERE";

			//if you added more sections, make sure you include an inner join for the new section below
		if(isset($search_type) && $search_type == "advanced") {
			//when search is happening and is advanced, overwrites the existing original $sql query
			$jobObj = new EMP1_JOB();
			$sql = "SELECT E.*, TK.*
				FROM " . $this->getTableName() . " E
				INNER JOIN  " . $this->getUserTableName() . " TK
				ON TK.tkid = E.emp_tkid
				INNER JOIN  " . $jobObj->getTableName() . " J
				ON J.job_empid = E.emp_id
				WHERE";
		}
		switch($status_filter) {
			case "INACTIVE":
				$sql .= " (E.".$this->getStatusFieldName()." & ".self::INACTIVE.") = ".self::INACTIVE;
				break;
			case "ACTIVE":
			default:
				$sql .= " (E.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
							".($is_edit && !isset($search_type)? "OR (E.".$this->getStatusFieldName()." & ".self::INACTIVE.") = ".self::INACTIVE."" : "");
				break;
		}


		if($search_type != "" || $search_name != ""){
			if($search_type == "quick"){

				$nameArray = explode(" ", $search_name);
				foreach($nameArray as $name => $val){
					$nameArray[$name] = "'%".$val."%'";
				}
				$likes = implode(" ",$nameArray);
				$sql .= " AND TK.tkname LIKE ".$likes." OR TK.tksurname LIKE ".$likes;

			}elseif($search_type == "advanced"){
				$glue ="";

				//validate the fields to make sure only the correct data from the $_REQUEST array is passed.
				foreach($sections as $section) {
					$tbl_placeholder = "J";
					switch($section){
						case "JOB": $tbl_placeholder = "J";break;
						case "USER": $tbl_placeholder = "TK";break;
						case "EMPLOYEE": $tbl_placeholder = "E";break;
					}
					$heading_section = strtolower($section)."_heading";
					$main_heading = $headings[$section]['rows'][$heading_section];
					foreach($headings[$section]['sub'][$main_heading['id']] as $id => $head) {
						$heading_fld = $head['field'];
						foreach($search as $fld => $val) {
							if(($heading_fld == $fld) && !empty($val)) {
								if(!is_array($val)) { //pay attention to fields DB fields that are strings and int

									if(ASSIST_HELPER::checkIntRef($val)) {
										$glue .= " AND (" . $tbl_placeholder . "." . $fld . " = " . $val . ")";
									}else{
										$glue .= " AND (" . $tbl_placeholder . "." . $fld . " = '" . $val . "' OR " . $tbl_placeholder . "." . $fld . " LIKE '%".$val."%')";
									}

								} else {
									//If you will be using multi selects you can put your values here

								}
								unset($search[$fld]);
							}

						}
					}
				}
				$sql .= $glue;
			}
		}
		switch($user_status_filter) {
			case "ACTIVE":
				$sql .= " AND TK.tkstatus = 1 ";
				break;
			case "INACTIVE":
				$sql .= " AND TK.tkstatus > 1 ";
				break;
			case "ALL":
			default:
				//do nothing
				break;
		}

		$sql .= " ORDER BY TK.tkname, TK.tksurname
				".($limit !== false ? "LIMIT ".$start.", ".$limit : "");
		return $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
	}

	public function getListOfEmployeesFormattedForSelect() {
		return $this->getListOfUsersWhoAreEmployees("all");
	}


	public function getListOfUsersWhoAreNotEmployees($user_type = "active") {
		if($user_type == "active") {
			$user_filter = " TK.tkstatus = 1 ";
		} elseif($user_type == "all") {
			$user_filter = " TK.tkstatus > 0 ";
		} else {
			$user_filter = " TK.tkstatus = 2 ";
		}
		$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name, TK.tkid as id, TK.tkstatus as status
				FROM ".$this->getUserTableName()." TK
				WHERE $user_filter
				AND TK.tkid NOT IN (
					SELECT E.emp_tkid
					FROM ".$this->getTableName()." E
					WHERE ".$this->getActiveStatusSQL("E")."
				) ORDER BY TK.tkname, TK.tksurname";
		if($user_type == "all") {
			return $this->mysql_fetch_all_by_id($sql, "id");
		} else {
			return $this->mysql_fetch_value_by_id($sql, "id", "name");
		}
	}

	public function getListOfUsersWhoAreEmployees($user_type = "active") {
		$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name, TK.tkid as id
				FROM ".$this->getTableName()." E
				INNER JOIN ".$this->getUserTableName()." TK
				ON TK.tkid = E.emp_tkid
				WHERE TK.tkstatus ";
		switch($user_type) {
			case "active":
				$sql .= " = 1 ";
				break;
			case "inactive":
				$sql .= " = 2 ";
				break;
			default:
				$sql .= " > 0 ";
				break;
		}
		$sql .= "
				AND ".$this->getActiveStatusSQL("E")."
				 ORDER BY TK.tkname, TK.tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
	}


	public function getFormattedOriginalUserDetails($user_id, $set_job_start_to_emp_date = false) {
		$sql = "SELECT TK.*, IF(ISNULL(M.tkid),NULL,CONCAT(M.tkname,' ',M.tksurname)) as tkmanager_name ";
		$sql .= " FROM ".$this->getUserTableName()." TK
				LEFT OUTER JOIN ".$this->getUserTableName()." M
				ON M.tkid = TK.tkmanager
				WHERE TK.tkid = '".$user_id."'";
		$data = $this->mysql_fetch_one($sql);

		/*****
		 * Status options:
		 *
		 * Active Assist User - can login
		 * Active Assist User (Admin Locked) - can normally login but account access has been temporarily suspended by Admin
		 * Active Assist User (Password Locked) - can normally login but account access has been temporarily suspended due to invalid password attempts
		 * Non-Assist User - no login privileges, requires Active Assist User in direct management chain to act as proxy for completing Assessments
		 * Terminated Assist User - was Active but now Terminated (will be treated like Non-Assist User in Individual Performance if still Active Employee)
		 *
		 */

		$data['raw_tkstatus'] = $data['tkstatus'];
		$status = isset($data['tkstatus'])? $data['tkstatus'] :"";
		$data['tkstatus'] = $this->getUserStatusOptions($status);
		$data['emp_status'] = "Active Employee";
		if($set_job_start_to_emp_date === true) {
			$data['job_start'] = $data['tkempdate'];
		}
		return $data;
	}


	public function getUserStatusOptions($user_status, $include_html = true) {
		switch($user_status) {
			case "1":
				return ($include_html ? $this->getDisplayIconAsDiv("ok") : "")."Active Assist user";
				break;
			case "2":    //normal inactive user status
			case "4":    //terminated inactive user status for IAS clients
				return ($include_html ? $this->getDisplayIconAsDiv("ui-icon-circle-minus", "", "fe9900") : "")."Non-Assist User";
				break;
			case "0":
				return ($include_html ? $this->getDisplayIconAsDiv("error") : "")."Terminated user";
				break;
		}
		return $this->getUnspecified();
	}

	public function getEmployeeStatusOptions($status, $include_html = true) {
		if(($status & self::ACTIVE) == self::ACTIVE) {
			return ($include_html ? $this->getDisplayIconAsDiv("ok") : "")."Active ".$this->getMyObjectName();
		} elseif(($status & self::INACTIVE) == self::INACTIVE) {
			return ($include_html ? $this->getDisplayIconAsDiv("error") : "")."Terminated ".$this->getMyObjectName();
		} else {
			return $this->getUnspecified();
		}
	}
}


?>