<?php
$scripts    = array( 'jquery.ui.query.js','menu.js',  );
$styles     = array( 'colorpicker.css' );
$page_title = "Edit Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#risk").query({assuranceQuery:true, assuranceActions:true, page:"assurance", section:"manage"});	
	});
</script>
<div id="risk"></div>

