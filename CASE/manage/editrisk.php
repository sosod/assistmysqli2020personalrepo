<?php
$scripts = array( 'query.js', 'menu.js','ajaxfileupload.js', 'ignite.textcounter.js'  );
$styles = array( );
$page_title = "Edit Query";
require_once("../inc/header.php");
//require_once("controller.php");
$risk 		 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$rsk 		 = $risk -> getARisk( $_REQUEST['id']);
$riskStatus  = $risk -> getRiskUpdate( $rsk['id'] );
$statusId 	 = ((empty($riskStatus) || !isset($riskStatus )) ? $rsk['status'] : $riskStatus['status'] );
$type 		 = new RiskType( "", "", "", "" );
$riskTypes	 = $type-> getActiveRiskType();	
$typeOb 	 = new QueryType( "", "", "", "" );
$types	 	 = $typeOb-> getActiveRiskType();	
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$fin 		 = new FinancialExposure( "", "", "", "", "");
$finExposure = $fin -> getFinancialExposure() ;
$rkl 		 = new RiskLevel( "", "", "", "", "","");
$rLevel 	 = $rkl -> getLevel() ;
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUsers() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
$dire 		 = new Directorate("", "", "", "");
$direct      = $dire -> getDirectorate();
$sourceObj     = new QuerySource();
$query_sources = $sourceObj -> getQuerySources();
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<script>
$(function(){
	$("table#edit_risk_table").find("th").css({"text-align":"left"})
});
</script>
<?php JSdisplayResultObj(""); ?>
<div style="clear:both;">
        <form id="edit-risk-form" method="post" enctype="multipart/form-data">
        <table align="left" border="1" id="edit_risk_table">
       		 <tr>
                    <th><?php nameFields('query_item','Risk Item'); ?>:</th>
                    <td>#<?php echo $rsk['id'] ?></td>
                  </tr>
                  <tr>
                    <th><?php nameFields('query_reference','Query Reference'); ?>:</th>
                    <td><input type="text" id="query_reference" name="query_reference" value="<?php echo $rsk['query_reference'] ?>" /></td>
                  </tr>    
                  <tr>
                    <th><?php nameFields('query_source','Query Source'); ?>:</th>
                    <td>
                        <select id="query_source" name="query_source">
                            <option value="">--query source--</option>
                            <?php
                             foreach($query_sources as $q_index => $q_source)
                             {
                            ?>
                              <option value="<?php echo $q_source['id']; ?>" <?php echo ($q_source['id'] == $rsk['query_source'] ? "selected='selected'" : "") ?>><?php echo $q_source['name']; ?></option>
                            <?php
                             }
                            ?>
                        </select>         
                    </td>
                  </tr>                                      
                  <tr>
                    <th><?php nameFields('query_type','Query Type'); ?>:</th>
                    <td>
                        <select id="query_type" name="query_type">
                            <option value="">--risk type--</option>
                            <?php
								foreach($types as $type ){
							?>
                            <option value="<?php echo $type['id'] ?>"
                            	<?php
									if( $type['id'] == $rsk['type'] ) 
									{
										?>
                                        selected="selected"
                                        <?php
									}
								?>
                            ><?php echo $type['name']; ?></option>
							<?php	
							}
							?>
                        </select>
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('query_category','Query category'); ?>:</th>
                    <td>
                        <select id="query_category" name="query_category">
                            <option value="">--query category--</option>
                           <?php
								foreach($categories as $category ){
							?>
                            <option value="<?php echo $category['id']; ?>"
                            	<?php
									if( $category['id'] == $rsk['category'] ) 
									{
										?>
                                        selected="selected"
                                        <?php
									} ?>
									><?php echo $category['name']; ?></option>
							<?php	
							}
							?>
                        </select>    
                    </td>
                  </tr>
			      <tr>
			        <th><?php nameFields('query_date','Query Date'); ?>:</th>
			        <td><input type="text" name="query_date" id="query_date" class="datepicker" value="<?php echo $rsk['query_date']; ?>" readonly="readonly" /></td>
			      </tr>                   
                  <tr>
                    <th><?php nameFields('query_description','Query Description'); ?>:</th>
                    <td><textarea name="query_description" id="query_description" rows="7" cols="35"><?php echo $rsk['description']; ?></textarea></td>
                  </tr>
                  <tr>
                    <th><?php nameFields('query_background','Background Of Query'); ?>:</th>
                    <td><textarea name="query_background" id="query_background" rows="7" cols="35"><?php echo $rsk['background']; ?></textarea></td>
                  </tr>
     			<tr>
			        <th><?php nameFields('query_owner','Directorate responsible'); ?>:</th>
			        <td>
			        <select id="directorate" name="directorate" class="responsibility">
			                <option value="">--directorate--</option>
			                <?php 
			                	foreach( $direct as $k => $dir ){
			                ?>
				                <option value="<?php echo $dir['subid']; ?>"
				                <?php 
				                if( $dir['subid'] == $rsk['sub_id']){
				                	?>
				                	selected="selected"
				                <?php 
			                	}
				                ?> 
				               ><?php echo $dir['dirtxt']; ?></option>
			                <?php 
			                	}
			                ?>
			         </select>        
			        </td>
       		   </tr>                  
		       <tr>
			      	<th><?php nameFields('query_deadline_date','Query Deadline Date'); ?>:</th>
			      	<td><input type="text" name="query_deadline_date" id="query_deadline_date" class="datepicker" value="<?php echo $rsk['query_deadline_date']; ?>" readonly="readonly" /></td>
		      </tr>                 
<tr class="more_details">
        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>
        	<select id='financial_exposure' name='financial_exposure'>
            	<option value="">--financial exposure--</option>
                <?php
					foreach($finExposure as $key => $finExp){
				?>
                <option value='<?php echo $finExp['id']; ?>'
				<?php
                    if( $finExp['id'] == $rsk['financial_exposure'] ) 
                    {
                        ?>
                        selected="selected"
                        <?php
                    } ?>                
                ><?php echo $finExp['name']; ?></option>
                <?php
				}
				?>
        	</select>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('monetary_implication','Monetary Implication'); ?>:</th>
        <td>
        	<textarea id='monetary_implication' name='monetary_implication' class="textcounter"><?php echo $rsk['monetary_implication'] ?></textarea>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
        <td>
        <select id="risk_level" name="risk_level">
                <option value="">--risk level--</option>
                <?php
					foreach($rLevel as $key => $rskLevel){
				?>
                <option value="<?php echo $rskLevel['id']; ?>"
				<?php
                    if( $rskLevel['id'] == $rsk['risk_level'] ) 
                    {
                        ?>
                        selected="selected"
                        <?php
                    } ?>                    
                ><?php echo $rskLevel['name']; ?></option>
                <?php
				}
				?>               
         </select>            
        </td>
      </tr>      
      <tr class="more_details">
        <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
        <td>
        <select id="risk_type" name="risk_type">
                <option value="">--risk type--</option>
                <?php
					foreach($riskTypes as $key => $risk_type){
				?>
                <option value="<?php echo $risk_type['id']; ?>"
				<?php
                    if( $risk_type['id'] == $rsk['risk_type'] ) 
                    {
                        ?>
                        selected="selected"
                        <?php
                    } ?>                       
                ><?php echo $risk_type['name']; ?></option>
                <?php
				}
				?>                  
         </select>            
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('risk_detail','Risk Detail'); ?>:</th>
        <td>       
        	<textarea rows="5" cols="35" id='risk_detail' name='risk_detail'><?php echo $rsk['risk_detail'] ?></textarea>
      </tr>      
      <tr class="more_details">
        <th><?php nameFields('finding','Add Finding'); ?>:</th>
        <td>
        	 <?php 
        	 	$findings = split("__", $rsk['finding']);
        	 	if(!empty($findings)) {
        	 		foreach( $findings as $f => $fVal ) {
        	 ?>
        		<p><textarea class="find" id='finding' name='finding'><?php echo $fVal; ?></textarea></p>
        	<?php
        	 		} 
        	 	}
        	?>
            <input type="submit" name="add_another_finding" id="add_another_finding" value="Add Another"  /> 
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('internal_control_deficiency','Internal Control Deficiency'); ?>:</th>
        <td>
			<?php 
         	$internalControl = split("__", $rsk['internal_control_deficiency']);
         	if(!empty($internalControl)) {
         		foreach( $internalControl as $f => $icVal ) {
         	?>
         		<p>
            		<textarea class="internalcontrol" id="internal_control_deficiency" name="internal_control_deficiency"><?php echo $icVal; ?></textarea>
            	</p>
        	<?php
         			} 
         		}
        	?> 
        	<input type="submit" name="add_another_internalcontrol" id="add_another_internalcontrol" value="Add Another"  />                 
        </td>
      </tr> 
      <tr class="more_details">
        <th><?php nameFields('recommendation','Add Recommendation'); ?>:</th>
        <td>
        	<?php 
        		$recomendations = split("__", $rsk['recommendation']);
        		if(!empty($recomendations)){
        			foreach($recomendations as $r => $rVal){
        	?>
	            <p><textarea class="recommend" id='recommendation' name='recommendation'><?php echo $rVal; ?></textarea></p>
            <?php 
        			}
        		}
            ?>
            <input type="submit" name="add_another_recommendation" id="add_another_recommendation" value="Add Another"  />         
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('client_response','Add Client Response'); ?>:</th>
        <td>
		        <?php 
		        	$cresponse = split("__", $rsk['client_response']);
		        	if( !empty($cresponse)){
		        		foreach($cresponse as $c => $cVal){
		        ?>
 	       			<p><textarea class="clientresponse" id='client_response' name='client_response'><?php echo $cVal; ?></textarea></p>
	        	<?php 
		        	}
	        	}
	        	?>
        	<input type="submit" name="add_another_clientresponse" id="add_another_clientresponse" value="Add Another"  />                 
        </td>
      </tr>  
      <tr class="more_details">
        <th><?php nameFields('auditor_conclusion','Auditor\'s Conclusion'); ?>:</th>
        <td>					        <?php 
	        	$aUresponse = split("__", $rsk['auditor_conclusion']);
	        	if( !empty($aUresponse)){
	        		foreach($aUresponse as $c => $aVal){
	        ?>
        			<p><textarea class="auditorconclusion" id="auditor_conclusion" name="auditor_conclusion"><?php echo $aVal; ?></textarea></p>
        	<?php 
	        	}
        	}
        	?>
        	<input type="submit" name="add_another_auditorconclusion" id="add_another_auditorconclusion" value="Add Another"  />                 
        </td>
      </tr>   
      <tr>
        	<th>Attachment:</th>
            <td>
                <input id="query_attachment_<?php echo $rsk['id']; ?>" name="query_attachment_<?php echo $rsk['id']; ?>" type="file" class="upload_query" />                            
                <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                  <small>You can attach more than 1 file.</small>
                </p>
                <?php
                   Attachment::displayAttachmentList($rsk['attachment'], 'query');
                ?>
            </td>
   </tr>
                  <tr>
					<td></td>
                  	<td align="right">
                    	<input type="submit" name="edit_risk" id="edit_risk" value="Save Changes " class="isubmit" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="delete_risk" id="delete_risk" value="Delete Query" class="idelete" />
                        &nbsp;&nbsp;
                        <span style='float:right'><input type="submit" name="more_detail" id="more_detail" value="More Detail" class="iinform" /></span>
                        <input type="hidden" name="riskid" value="<?php echo $rsk['id']; ?>" id="riskid" /> 
                        <input type="hidden" name="risk_id" value="<?php echo $rsk['id']; ?>" id="risk_id" class="logid" /> 
                   </td>
                  </tr>
                  <tr>
                  	<td align="left" class="noborder">
                    	<?php displayGoBack("",""); ?>
                    </td>
                  	<td align="right" class="noborder">
                    	<?php displayAuditLogLink("risk_edits" , false); ?>
                    </td>                    
                  </tr>
               </table>
           </form>
      </div>
     <div id="editLogs" style="clear:both;">
  </div>
