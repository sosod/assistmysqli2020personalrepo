<?php
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "default";
switch($act) {



default:
?>
<P>In order to generate the CSV files necessary for the import, simply go to each worksheet in the template and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'. <br /><span class=b>
PLEASE ENSURE THAT THE COLUMNS MATCH THOSE GIVEN BELOW!!!</span></p>

<table class=form width=75%>
	<tr>
		<th width=100px>Step 1:</th>
		<td>If you haven't already generated the CSV from the Excel template, create one by clicking the Generate button.
		<input type=button value=Generate class=float style='padding-left: 10px' id=generate />
		</td>
	</tr>
	<tr>
		<th>Step 2:</th>
		<td>
		<span class='float right' style='padding-left: 10px;'>
			<input type=file name=import_file /> <p><input type=button value='Start Import Process' class=isubmit id=start /></p>
		</span>
		Import the populated template by selecting the file and clicking the Start button.<br />The first 2 rows of the template are assumed to be header rows and will be ignored.
</td>
	</tr>
	<tr>
		<th>Step 3:</th>
		<td>Review the Import result displayed on the next page.
			<div style="width: 75%; margin-top: 10px"><span class=u>Note:</span> <ul>
				<li>If any fatal errors exist (highlighted in red), correct them in the CSV template and repeat Step 2.  Fatal errors are errors that prevent the import process from being completed and must be corrected.</li>
				<li>If any non-fatal errors exist (highlighted in orange), review them and correct in the CSV template if necessary and repeat Step 2.  These error won't prevent the import process from being completed but might indicate a possible error in the import template.</li>
				<li>If no fatal errors exist, click the green "Accept" button at the bottom of the page to finalise the Import process. No data will be imported until you click the Accept button!</li>
			</ul></div>
		</td>
	</tr>
	<tr>
		<th>Reset:</th>
		<td>If you need to delete all the data already imported in this section, you can use the Reset option.<input type=button value=Reset class='float idelete' id=reset />
			<div style="width:75%"><?php $me->displayResult(array("error",$warning)); ?></div>
		</td>
	</tr>
</table>
<?php 
$me->displayGoBack("index.php");

/* GET HEADER NAMES */
//$sql = "SELECT name, IF(client_terminology='',ignite_terminology, client_terminology) as heading FROM ".$db->getDBRef()."_header_names";
$names = $query->getAllHeaderNames(); //$db->mysql_fetch_value_by_id($sql,"name","heading");
/* GET IMPORT TEMPLATE */
$data = $query->readImportTemplate($key);
/* DISPLAY DEFAULT COLUMN STRUCTURE */	
echo "
<h2>Template Column Structure</h2>
<table class=list>
	<tr>
		<th>Excel<br />Column</th>
		<th>Column Name</th>
		<th>Content Guidance</th>
	</tr>";
$cell = "A";
foreach($data[0] as $index => $d) {
	echo "
	<tr>
		<th>".$cell."</th>
		<td>".(isset($names[$d]) ? $names[$d] : $d)."</td>
		<td class=i>".($data[1][$index])."</td>
	</tr>";
	$cell++;
}
echo "
</table>";	
?>
<script type=text/javascript>
$(function() {
	$("#generate").click(function() {
		document.location.href = 'import_generate.php?key=<?php echo $key; ?>';
	});
});
</script>
<?php 

}	//end switch($act)

?>