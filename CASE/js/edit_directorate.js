// JavaScript Document
$(function(){
		   
	$("#edit_directorate").click(function(){
		var message  	 = $("#directorate_message");
		var d_shortcode  = $("#directorate_shortcode").val();
		var d_department = $("#dr_department").val();
		var d_funtion 	 = $("#dept_function").val();
		var d_user		 = $("#dr_users_respo :selected");
	
		if( d_shortcode == "") {
			message.html("Please enter the short code for this directorate");
			return false;
		} else if( d_department == "") {
			message.html("Please enter the department name for this directorate");
			return false;			
		} else if( d_funtion == "" ) {
			message.html("Please enter the department function for this directorate");
			return false;			
		} else if( d_user.val() == "" ) {
			message.html("Please select user responsible for this directorate");
			return false;			
		} else {
			$.post( "controller.php?action=updateDirectorate", 
				   {
					   id			: $("#directorate_id").val(),
					   shortcode	: d_shortcode,
					   dpt			: d_department,
					   fxn			: d_funtion,
					   user			: d_user.val()
					 },
				   function( retData ) {
						if( retData == 1 ) {
							message.html("Directorate successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Directorate not changed");
						} else {
							message.html("Error updating the control directorate");
						}					   
			}, "json")
		}
		return false;								  
	})
		 
	$("#change_directorate").click(function() {
		var message  = $("#directorate_message");											
		$.post( "controller.php?action=changeDireStatus", 
			   {
			      id	 : $("#directorate_id").val(),
				  status : $("#directorate_status :selected").val()
			  }, function( deactData ) {
			if( deactData == 1){
				message.html("Directorate successifully updated ..").animate({opacity:"0.0"},4000);
			} else if( deactData == 0){
				message.html("No change was made to the directorate structure statuc").animate({opacity:"0.0"},4000);
			} else {
				message.html("Error updating , please try again ").animate({opacity:"0.0"},4000);
			}										 
		})									
		return false;												
	});	 
		 
	$("#cancel_directorate").click(function(){
		history.back();
		return false;									  
	})
		  
});