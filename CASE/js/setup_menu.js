// JavaScript Document
var menuValues = {};

$(function(){
	$("#container").append($("<table />",{id:"menu_contents" , border:"1"})
		.append($("<tr />")
			.append($("<th />",{html:"Ref"}))
			.append($("<th />",{html:"Query Assit Terminology"}))
			.append($("<th />",{html:"&lt;Client&gt; Terminology"}))
			.append($("<th />",{html:""}))			
		)						 
	);
	
	$.get( "controller.php?action=getAllMenu", function( menuData ) {
		$.each( menuData, function( index , menu ) {
			$("#menu_contents")
			.append($("<tr />",{id:"tr_"+menu.folder})
				.append($("<td />",{html:menu.id}))
				.append($("<td />",{html:menu.name, title:menu.description}))
				.append($("<td />")
					.append($("<input />",{
							type : "text",
							name : "menu_"+menu.folder,
							id	 : "menu_"+menu.folder,
							value: ((menu.client_name == "" || menu.client_name == null) ?  menu.name : menu.client_name ),
							size : "50"
							}))		  
				)	
				.append($("<td />")
					.append($("<input />",{type:"submit", id:"save_"+menu.folder, name:"save_"+menu.folder, value:"Update" }))
				)
			)
			
			$("#menu_"+menu.folder).live( "focus", function(){
				$("#tr_"+menu.folder).addClass("tdhover")							   
			});
			
			$("#menu_"+menu.folder).live( "focusout", function(){
				$("#tr_"+menu.folder).removeClass("tdhover")							   
			});
			
			$("#save_"+menu.folder).live( "click" , function() {	
				jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
					$.post("controller.php?action=menu" , {
					   name : menu.folder,
					   value : $("#menu_"+menu.folder).val() 
					   } , function( retData  ) {
						   if( retData == 1 ) {
							   jsDisplayResult("ok", "ok", "Menu change successfully updated . . . ");   
						   } else if( retData == 0 ){
							   jsDisplayResult("info", "info", "No change has been made to the menu  . . . ");   	
						   } else {
							   jsDisplayResult("error", "error", "Error saving changes to the menu item  . . . ");   	   
						   }
					});
				return false;
			})
			
		});
	},"json");	
	

})