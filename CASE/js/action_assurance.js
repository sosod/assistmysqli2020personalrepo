// JavaScript Document
$(function(){

	$.get("controller.php?action=getRisk",{ id : $("#risk_id").val() }, function( riskData ){
			$("#risk_action_assurance")
			.append($("<tr />")
				.append($("<th />",{html:"Query Item:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.id}))		  				
			)
			.append($("<tr />")
				.append($("<th />",{html:"Query Type:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.type}))		  				
			)
			.append($("<tr />")
				.append($("<th />",{html:"Query Category:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.category}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Query description:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.description}))		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Background of the query:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.background}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Responsibility of the risk:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.risk_owner}))		  		  
			)
			//.append($("<tr />")
				//.append($("<th />",{html:"UDF's:"}))		  
				//.append($("<td />",{html:""}))		  		  
			//)
	}, "json")
	
	/*$.get( "controller.php?action=getActions", {id : $("#risk_id").val() }, function( actionsData ){																		 
		$.each( actionsData , function( index, val){
			$("#list_risk_ations")
				.append($("<tr />")
					.append($("<td />",{html:val.id}))
					.append($("<td />",{html:val.action}))
					.append($("<td />",{html:val.status}))
					.append($("<td />",{html:val.progress}))
					.append($("<td />",{html:val.tkname+" "+val.tksurname+"-"+val.value}))
					.append($("<td />")
						.append($("<input />",{type:"submit", name:"assurance_"+val.id, id:"assurance_"+val.id, value:"Assurance"}))		
					)					
				)
				
				$("#assurance_"+val.id).live( "click", function(){
					document.location.href = "assurance_action.php?id="+val.id;
					return false;								  
				});
		});
	},"json")*/
	
});
