// JavaScript Document
$(function(){
		   
	/**
		get all the risk categories
	**/
	$("#risk_type").live("change", function(){								
		$("#risk_category").html("");
		$("#risk_category").append($("<option />",{value:"", text:"--risk category--"}))
		$.get( "controller.php/?action=getTypeCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#risk_category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});
	/**
		Get the risk impact ratings
	**/
	$("#risk_impact").live( "change" ,function(){
		$("#risk_impact_rating").html("");
		$("#risk_impact_rating").append($("<option />",{value:"", text:"--impact rating--"}))
		$.get( "controller.php?action=getImpactRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#risk_impact_rating")
					.append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#risk_impact_rating")
					.append($("<option />",{value:i, text: i}))	
				}
			}
		}, "json");
		return false;
	});	
	/**
		Get the risk likelihood ratings
	**/
	$("#risk_likelihood").live( "change" ,function(){
		$("#risk_likelihood_rating").html("");
		$("#risk_likelihood_rating").append($("<option />",{value:"", text:"--likelihood rating--"}))
		$.get( "controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#risk_likelihood_rating")
					.append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#risk_likelihood_rating")
					.append($("<option />",{value:i, text:i}))	
				}
			}
		}, "json");
		return false;
	});	
	/**
		Get the risk control effectiveness ratings
	**/
	$("#risk_percieved_control").live( "change" ,function(){
		$("#risk_control_effectiveness").html("")														 
		$("#risk_control_effectiveness").append($("<option />",{text:"--control effectiveness rating--"}))
		$.get( "controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
				$("#risk_control_effectiveness")
					.append($("<option />",{value:data.rating, text:data.rating }))
		}, "json");
		return false;
	});	
	
		
	$("#add_another_control").click( function(){
		$(this)
		.parent()						  
		.prepend($("<br />"))
		.prepend($("<input />",{type:"text", name:"control" }).addClass("controls"))
		return false;									  
	});
	
	/**
		Add another browse button to attach files
	**/
	$("#attach_another").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	
});