$(function(){
	RiskManager.getUsers();
	RiskManager.get();
	
	$("#save").click(function(){
		RiskManager.save();
		return false;
	})
	
	$("#update_risk_manager").click(function() {
		RiskManager.update();
		return false;
	})
	
	$("#cancel_changes").live("click", function(){
		history.back();
		return false;										
	});
	
});


RiskManager		= {
		
	get				: function()
	{
		$.getJSON("controller.php?action=getRiskManagers", function( riskMangerData ) {
			if( $.isEmptyObject(riskMangerData) ) {
				$("#risk_manager_table")
				 .append($("<tr />")
				   .append($("<td />",{colspan:"4", html:"No risk manger has been set yet"}))		   
				 )
			} else {
				$.each( riskMangerData, function( index, manager) {
					RiskManager.display( manager );
				});
			}
		});
	} ,
	
	display			: function( manager )
	{
	  	$("#risk_manager_table")
		 .append($("<tr />")
			.append($("<td />",{html:manager.tkid}))
			.append($("<td />",{html:manager.tkname+" "+manager.tksurname}))
			.append($("<td />")
				.append($("<input />",{type:"button", id:"edit_"+manager.tkid, value:"Edit"}))		  
			 )					
		  )
		 $("#edit_"+manager.tkid).live("click", function(){
			document.location.href = "edit_risk_manager.php?id="+manager.tkid;												 
			return false;
		 });
	} ,
	
	getUsers		: function()
	{
		/**
			Get all the users 
		**/
		$.get("controller.php?action=getUsers", function( data ) {		
			$.each( data, function( index, val) {
				$("#risk_manager_users")
					.append($("<option />",{text:val.tkname+"  "+val.tksurname, value:val.tkid}))
			});											   
		},"json")
	} , 
	
	save			: function()
	{
		var message = $("#risk_manager_message")								 
		var user  	= $("#risk_manager_users :selected");
		
		if( user == "" ) {
			jsDisplayResult("error", "error", "Please select a user to set as query manger");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving  . . .  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=setRiskManager", { 
				   	id		: user.val() ,
					status  : 1 
				}, function( retData ) {
					if( retData == 1 ){																		
						jsDisplayResult("ok", "ok", user.text()+" has been set as query manger");
						$("#risk_manager_users option[value='']").attr("selected", "selected");
					  	$("#risk_manager_table")
						 .append($("<tr />")
							.append($("<td />",{html:user.val()}))
							.append($("<td />",{html:user.text()}))
							.append($("<td />")
								.append($("<input />",{type:"button", id:"edit_"+user.val(), value:"Edit"}))		  
							 )					
						  )
						 $("#edit_"+user.val()).live("click", function(){
							document.location.href = "edit_risk_manager.php?id="+user.val();												 
							return false;
						 });
						 
					} else if( retData == 0 ) {
						jsDisplayResult("info", "info", "There was no change made to this user");
						
					} else {
						jsDisplayResult("error", "error", "There was an error saving user details , please try again");
					}
			},"json")		
		}
	} ,
	
	
	update			: function()
	{
		var message = $("#edit_manager_message");		
		jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
		$.post( "controller.php?action=setRiskManager", { 
			   id	   : $("#userid").val() ,  
			   status  : $("#query_manager :selected").val()	
			   }, function( retData ) {
			if( retData == 1 ){																		
				jsDisplayResult("ok", "ok", "Query manager updated");	
			} else if( retData == 0 ) {
				jsDisplayResult("error", "error", "There was no change made to this user");
			} else {
				jsDisplayResult("error", "error", "There was an error updating user details , please try again");
			}
		},"json")		
		
	}
	
	
		
};