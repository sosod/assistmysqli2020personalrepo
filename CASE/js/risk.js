// JavaScript Document
$(function(){ 
	//set the with of the select to be almost uniform	   
	$("table#risk_new").find("select").css({"width":"200px"})	   
	//set the table th text to align to the left
	$("table#risk_new").find("th").css({"text-align":"left"})

  //======================================================================================================================	
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#risk_type").live("change", function() {
		$("#risk_category").html("");
		$("#risk_category").append($("<option />",{value:"", text:"--risk category--"}))											
		$.get( "controller.php/?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#risk_category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});
	//================================================================================================================
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getActiveImpact", function( data ) {
		var impactRating = [];
		$.each( data ,function( index, val ) {
			$("#risk_impact")
			.append($("<option />",{text:val.assessment, value:val.id}));
		})			
	},"json");
	//================================================================================================================	
	/**
		Get the risk impact ratings
	**/
	$("#risk_impact").live( "change" ,function(){
		$("#risk_impact_rating").html("");
		$("#risk_impact_rating").append($("<option />",{value:"", text:"--impact rating--"}))
		$.get( "controller.php?action=getImpactRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#risk_impact_rating")
					.append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#risk_impact_rating")
					.append($("<option />",{value:i, text: i}))	
				}
			}
		}, "json");
		return false;
	});
	//================================================================================================================	
	/**
		get all the risk likelihoods
	**/
	$.get( "controller.php/?action=getLikelihood", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_likelihood")
			.append($("<option />",{text:val.assessment, value:val.id}));
		})											 
	},"json");
	//================================================================================================================	
	/**
		Get the risk likelihood ratings
	**/
	$("#risk_likelihood").live( "change" ,function(){
		$("#risk_likelihood_rating").html("");
		$("#risk_likelihood_rating").append($("<option />",{value:"", text:"--likelihood rating--"}))
		$.get( "controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#risk_likelihood_rating")
					.append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#risk_likelihood_rating")
					.append($("<option />",{value:i, text:i}))	
				}
			}
		}, "json");
		return false;
	});	
	//================================================================================================================	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getControl", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_percieved_control")
			.append($("<option />",{text:val.effectiveness, value:val.id}));
		})											 
	},"json");	
	//================================================================================================================	
	/**
		Get the risk control effectiveness ratings
	**/
	$("#risk_percieved_control").live( "change" ,function(){
		$.get( "controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
				$("#risk_control_effectiveness")
					.append($("<option />",{value:data.rating, text:data.rating }))
		}, "json");
		return false;
	});		
	//================================================================================================================	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_user_responsible")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
			
			$("#action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})											 
	},"json");	
	//================================================================================================================	
	$.get("controller.php?action=getDirectorate", function( direData ){
	$.each( direData ,function( index, directorate ) {
			$("#directorate")
			.append($("<option />",{text:directorate.dirtxt	, value:directorate.subid}));
		})
	}, "json");
	/*================================================================================================================	
	$.get("controller.php?action=getSubDirectorate", function( subdireData ){													  
		$.each( subdireData ,function( index, subdirectorate ) {
			$("#subdirectorate")
			.append($("<option />",{text:subdirectorate.shortcode, value:subdirectorate.tkid}));
		})
	}, "json");
	*///================================================================================================================
	$("#add_another_control").click( function(){
		$(this)
		.parent()
		.prepend($("<br />"))
		.prepend($("<input />",{type:"text", name:"control" }).addClass("controls"))
		return false;									  
	});
	//================================================================================================================	
	/**
		Add another browse button to attach files
	**/
	$("#another_attach").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	//================================================================================================================
	$("#upload").click(function(){
				/**
			Ajax file uploading
		**/

		/**
			End ajax file upload
		**/
		return false;
	});
	//================================================================================================================
	/**
		Add New Risk
	**/
	$("#add_risk").click(function() {

		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#risk_type :selected").val()
		var r_category 				= $("#risk_category :selected").val()
		var r_description 			= $("#risk_description").val()
		var r_background			= $("#risk_background").val();	
		var r_impact 				= $("#risk_impact :selected").val();
		var r_impact_rating			= $("#risk_impact_rating").val();
		var r_likelihood 			= $("#risk_likelihood :selected").val();
		var r_likelihood_rating 	= $("#risk_likelihood_rating").val();
		var r_percieved_control		= $("#risk_percieved_control :selected").val();
		var r_control_effectiveness = $("#risk_control_effectiveness").val();
		//var r_user_responsible 		= $("#risk_user_responsible").val();
		var r_directorate 			= $("#directorate :selected").val();
		//var r_subdirectorate 		= $("#subdirectorate").val()
	
		
		var r_responsiblity = "";
		$(".responsibility").each( function( index, val){
			if( $(this).val() == "" ){
				message.html("Please select the one responsible ");
				return false;
			} else {
				r_responsiblity  = $(this).val();	
			}									
		});
		var r_attachements = "";
		ajaxFileUpload();
		$(".controls").each( function( index, val){
			if( index > 0 ) {
				r_currentcontrols += ","+$(this).val();
			}else{
				r_currentcontrols = $(this).val();	
			}
		});

		if( r_type == "" ) {
			message.html( "Please select the risk type for this risk " )
			return false;
		} else if( r_category == "" ) {
			message.html( "Please select the risk category for this risk " )
			return false;
		} else if( r_description == "" ) {
			message.html( "Please enter the risk description for this risk " )
			return false;
		} else if( r_background == "" ) {
			message.html( "Please enter the risk background for this risk " )
			return false;
		} else if( r_impact == "" ) {
			message.html( "Please select the risk impact for this risk " )
			return false;
		} else if( r_likelihood == "" ) {
			message.html( "Please select the risk likelihood for this risk " )
			return false;
		} else if( r_currentcontrols == "" ) {
			message.html( "Please enter the risk current controls for this risk " )
			return false;
		} else if( r_percieved_control == "" ) {
			message.html( "Please enter the risk percieved control effectiveness for this risk " )
			return false;
		}  else {
			message.html( "Saving risk...  <img src='../images/loaderA32.gif' />" )
			$.post( "controller.php?action=newRisk",
			  {
				type 			 	  : r_type ,
				type_text			  : $("#risk_type :selected").text(),
				category		 	  : r_category ,
				category_text		  : $("#risk_category :selected").text(),				
				description 	 	  : r_description ,
			    background		 	  : r_background ,
		 		impact			 	  : r_impact ,
				impact_text			  : $("#risk_impact :selected").text(),					
				impact_rating	 	  : r_impact_rating ,	
				likelihood 		 	  : r_likelihood ,
				likelihood_text		  : $("#risk_likelihood :selected").text(),				
				likelihood_rating	  : r_likelihood_rating , 
				currentcontrols	 	  : r_currentcontrols ,
				percieved_control	  : r_percieved_control ,
				percieved_control_text: $("#risk_percieved_control :selected").text(),					
				control_effectiveness :	r_control_effectiveness ,
				user_responsible 	  : r_directorate ,
				user_responsible_text : $("#directorate :selected").text(),					
				attachment			  : r_attachements
			  } ,
			  function( data ) {
				 message.html("")				  
				 if( ! $.isEmptyObject( data.success ) ) {
					
					$("#riskid").val( data )
					$("#idrisk").html( data )					
					$("select#risk_type option[value='']").attr("selected", "selected");
					$("select#risk_impact option[value='']").attr("selected", "selected");
					$("select#risk_likelihood_rating option[value='']").attr("selected", "selected");											
					$("select#risk_percieved_control option[value='']").attr("selected", "selected");$("select#risk_impact_rating option[value='']").attr("selected", "selected");					
					$("select#risk_category option[value='']").attr("selected", "selected");
					$("select#risk_likelihood option[value='']").attr("selected", "selected");
					
					$("select#risk_control_effectiveness option[value='']").attr("selected", "selected");						
					$("select#directorate option[value='']").attr("selected", "selected");					
					$("#risk_description").val("");
					$("#risk_background").val("");
					$(".controls").val("");	
					$.each( data.success , function( index, val ) {
						message.addClass("ui-state-highlight").append( val+"<br />" );
					});
					$.each( data.error , function( i, valu ) {
						message.addClass("ui-state-error").append( valu+"<br />" );
					});
				} else {
					$.each( data.error , function( i, valu ) {
						message.addClass("ui-state-error").append( "<br />"+valu+"<br />" );
					});
				}
			  } ,
			 "json");
		}				  
		return false;						  
	});
	//================================================================================================================		
	/**
		Show form to add action for this risk 
	**/
	$("#show_add_action").click(function(){
		if( $("#riskid").val() == "") {
			$("#risk_message").html("There was no risk enter associated with this action")	
			return false;			
		} else {
			document.location.href = "action.php";
		}
		//$("#actions_table").fadeIn();	
		return false;
	});
	/**
		Adding action
	**/
	$("#add_action").click( function() {	
		var message 		= $("#riskaction_message")
		var rs_action 		= $("#action").val()
		var rs_action_owner = $("#action_owner :selected").val()		
		var rs_deliverable  = $("#deliverable").val()
		var rs_timescale    = $("#timescale").val()		
		var rs_deadline     = $("#deadline").val()
		var rs_remindon     = $("#remindon").val()	
		
		 if( rs_action == "" ) {
			message.html("Please enter the action ")	
			return false;
		} else if( rs_action_owner == "" ) {
			message.html("Please select the action owner for this risk ")	
			return false;				
		} else if( rs_deliverable == "" ) {
			message.html("Please enter the deliverable for this action ")	
			return false;				
		} else if( rs_timescale == "" ) {
			message.html("Please enter the time scale for this action ")	
			return false;					
		} else if( rs_deadline == "" ) {
			message.html("Please enter the deadline for this action ")	
			return false;					
		} else if( rs_remindon == "" ) {
			message.html("Please enter the remond on for this action ")	
			return false;					
		} else {
			message.html( "<img src='../images/loaderA32.gif' />" )
			$.post( "controller.php?action=newRiskAction" , 
				   {
					   	id 				: $("#riskid").val(),
						r_action 		: rs_action ,
						action_owner    : rs_action_owner ,
						deliverable     : rs_deliverable ,
						timescale       : rs_timescale ,
						deadline     	: rs_deadline ,
						remindon     	: rs_remindon
				   } ,
				   function( retData ) {
					  if( retData > 0 ) {
					   $("#action").val("")
					   $("#deliverable").val("")
					   $("#timescale").val("")
					   $("#deadline").val("")
					   $("#remindon").val("")
					   $("select#action_owner option[value='']").attr("selected", "selected");
					   
					  $("#actions_table")
					  .append($("<tr />")
						.append($("<td />",{html:retData}))
						.append($("<td />",{html:rs_action}))
						.append($("<td />",{html:$("#action_owner :selected").text()}))
						.append($("<td />",{html:rs_deliverable}))
						.append($("<td />",{html:rs_timescale}))
						.append($("<td />",{html:rs_deadline}))
						.append($("<td />",{html:rs_remindon}))	
						.append($("<td />")
							.append($("<input />",{type:"submit", value:"Edit", id:"edit_action_"+retData}))
							.append($("<input />",{type:"submit", value:"Del", id:"delete_action_"+retData}))							
						)	
						//.append($("<td />")
							//.append($("<input />",{type:"submit", value:"InActivate", id:"deactivate_action_"+retData}))							
						//)							
					   )
						message.html( "Risk Action Saved" )	.animate({
							opacity:"0.76"
						},5000).html( "" )
										
						$("#edit_action_"+data).live( "click", function(){
							document.location.href = "../actions/edit_action.php?id="+retData;									
							return false;									   
						});
						
						$("#delete_action_"+retData).live( "click", function(){
							if(confirm("Are you sure you want to delete this action")){
								$.post("controller.php?action=deleteAction",{ id : retData}, function( del ){
									if( del == 1 ) {
										message.html( "Risk action deleted" )																								
									} else if( del == 0 ){
										message.html( "Risk action already deactivated" )	
									} else {
										message.html( "Error deleting risk action" )	
									}
								},"json");
							}
							//document.location.href = "../actions/action_edit.php?id="+$("#riskid").val();									
							return false;									   
						});			
						
						$("#activate_action_"+retData).live( "click", function(){
							$.post("controller.php?action=activateAction",{ id : retData}, function( act ){
								if( act == 1) {
									message.html( "Risk action activated" );
								} else if( act == 0) {
									message.html( "Risk action is already active" );
								} else {
									message.html( "Errod activating risk action" );
								}														
							},"json");																			
   							return false;									   
						});	

						$("#deactivate_action_"+retData).live( "click", function(){
							$.post("controller.php?action=deleteAction",{ id : retData}, function( deact ){
								if( deact == 1) {	
									message.html( "Risk deactivated" );																
								} else  if( deact == 0 ){
									message.html( "Risk action is already deactivated" )
								} else {
									message.html( "Error deactivating risk action " )
								}
							},"json");																			  
							return false;									   
						});	
						
					  } else {
						 if( !$.isEmptyObject( retData ) ) {
							message.html("Error : "+retData);  
						 } else {
							message.html("There was an error saving risk , please try again");  
						 }
					  }
				   } ,
			"json");
		}
		return false;	
	});
	$("#btn").live( "", function(){					 
	})
});

	function ajaxFileUpload()
	{
		$("#loading")
		.ajaxStart(function(){
			$(this).show();
		})
		.ajaxComplete(function(){
			$(this).hide();
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=saveAttachment',
				secureuri	  : false,
				fileElementId : 'file_upload',
				dataType	  : 'json',
				success       : function (data, status)
				{
					$("#loading").html( data )
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(" Eroor fron respinse => "+data.error);
						}else
						{
							alert(data.msg);
						}
					}
				},
				error: function (data, status, e)
				{
					alert("Ajax error => "+e);
				}
			}
		)
		return false;

	}