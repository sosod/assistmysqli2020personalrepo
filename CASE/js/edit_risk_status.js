// JavaScript Document
$(function(){


	$("#edit_status").click(function(){

		var risk_status = $("#status").val();
		var client_term = $("#client_term").val();
		var color 		= $("#risktype_color").val(); 
		var message 	= $("#edit_risk_status_message");
		
		if	( risk_status == "" ) {
			message.html("Please enter risk status ");
			return false;
		} else if ( client_term == "") {
			message.html("Please enter cleint risk status terminology ");
			return false;	
		} else {
			$.post( "controller.php?action=editRiskStatus" , { 
				   id					: $("#risk_status_id").val(),
				   name					: risk_status,
				   client_terminology   : client_term, 
				   color				: color
			}, function( retStat ) {
				if( retStat == 1 ) {
					message.html("Status successifully updated .. ");						
				} else if( retStat == 0 ){
					message.html("Status not changed");
				} else {
					message.html("Error updating the staus");
				}
			},"json");
		}
		return false;									 
	})
	
	$("#cancel_status").click(function(){
		history.back();
		return false;								   
	})
	
});