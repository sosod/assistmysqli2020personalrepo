$(function(){
	
	$(".textcounter").textcounter({maxLength:100});
	if( $("#risk_status_id").val() == undefined)
	{
		RiskStatus.get();
	}
	
	$("#add_status").click(function(){
		RiskStatus.save();
		return false;
	});
	
	$("#edit_status").click(function(){
		RiskStatus.edit();
		return false;
	});	
	
	
	$("#save_status").click(function(){
		RiskStatus.change();
		return false;
	})	
	
});

var RiskStatus		= {
		
		get				: function()
		{
			$.getJSON("controller.php?action=getRiskstatus", function( data ) {
				$.each( data , function( index, val) {					
					RiskStatus.display( val );
				});
			});
	
		} , 
		
		save			: function()
		{
			var risk_status = $("#status").val();
			var client_term = $("#client_term").val();
			var color 		= $("#risktype_color").val(); 
			if	( risk_status == "" ) {
				jsDisplayResult("error", "error", "Please enter query status");
				return false;
			} else if ( client_term == "") {
				jsDisplayResult("error", "error", "Please enter client terminology");
				return false;	
			} else {
				jsDisplayResult("info", "info", "Saving . . .  <img src='../images/loaderA32.gif' />");			
				$.post("controller.php?action=riskstatus" , { 
					   name					: risk_status,
					   client_terminology   : client_term, 
					   color				: color
				}, function( response ) {
					 if ( response.error ) {
						 jsDisplayResult("error", "error", response.text ); 
					 } else {
						 $("#status").val("");
						 $("#client_term").val("");
						 $("#risktype_color").val("66FF00");
						jsDisplayResult("ok", "ok", response.text);
						var data = { id:response.id, name:risk_status, client_terminology:client_term , color:color, active:1  };
						RiskStatus.display( data ); 
					 }
				},"json");
			}
		} , 
		
		edit			: function()
		{
			var risk_status = $("#status").val();
			var client_term = $("#client_term").val();
			var color 		= $("#risktype_color").val(); 
			
			if	( risk_status == "" ) {
				jsDisplayResult("error", "error", "Please enter query status");
				return false;
			} else if ( client_term == "") {
				jsDisplayResult("error", "error", "Please enter client terminology");
				return false;	
			} else {
				jsDisplayResult("info", "info", "Saving . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=editRiskStatus" , { 
					   id					: $("#risk_status_id").val(),
					   name					: risk_status,
					   client_terminology   : client_term, 
					   color				: color
				}, function( retStat ) {
					if( retStat == 1 ) {
						jsDisplayResult("ok", "ok", "Status successfully updated . . . ");						
					} else if( retStat == 0 ){
						jsDisplayResult("info", "info", "No change was made to the query status . . . ");
					} else {
						jsDisplayResult("error", "error", "Error updating the staus. . ");
					}
				},"json");
			}	
		} , 
		
		change			: function()
		{
			jsDisplayResult("info", "info", "Updating . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=changestatus",
			{ 
				   	id		: $("#risk_status_id").val(),
					active 	: $("#status_status :selected").val()
			}, function( response ) {
				if(response.error) {
					jsDisplayResult("error", "error", response.text );
				} else {
					jsDisplayResult("ok", "ok", response.text );
				}		   
			},"json");	 
		} ,
		
		display			: function( val )
		{
			$("#risk_status_table")
			.append($("<tr />",{id:"row_"+val.id})
				.append($("<td />",{html:val.id}))		  
				.append($("<td />",{html:val.name}))		  
				.append($("<td />",{html:val.client_terminology}))		  
				.append($("<td />")
				  .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))		
				)
				.append($("<td />")
				  .append($("<input />",{type:"button", value:"Edit", id:"edit_status_"+val.id, name:"edit_status_"+val.id}))		  						
				  .append($("<input />",{type:"button", value:"Del", id:(val.defaults == 1 ? "del__"+val.id : "del_status_"+val.id), name:"del_status_"+val.id}))			
				)
				.append($("<td />", { html:"<b>"+(val.active == 1 ? "Active"  : "Inactive")+"</b>"}))
				.append($("<td />")
				   .append($("<input />",{type:"button", id:(val.defaults == 1 ? "change__"+val.id : "change_"+val.id), name:"change_"+val.id, value:"Change Status"}))
				)
			)
			
			if(val.defaults == 1)
			{
			    $("#del__"+val.id).attr("disabled", "disabled");
			    $("#change__"+val.id).attr("disabled", "disabled");
			}
			
			
			$("#edit_status_"+val.id).live("click" ,function(){
				document.location.href = "edit_risk_status.php?id="+val.id;
				return false;					 
			});
			
			$("#del_status_"+val.id).live("click" ,function(){
				if( confirm(" Are you sure you want to delete this risk status ")) {
					jsDisplayResult("info", "info", "Inactivating status  ...  <img src='../images/loaderA32.gif' />");
					$.post("controller.php?action=changestatus", {
						active 	: 2,
						id 		: val.id
					}, function( response ) {
						if( response.error ){
							jsDisplayResult("error", "error", response.text );
						} else{
							jsDisplayResult("ok", "ok", response.text );
							$("#row_"+val.id).fadeOut();
						}		   
					},"json");				
				}
				return false;											 
			});
			
			
			$("#change_"+val.id).live("click", function(){
				document.location.href = "risk_status_status.php?id="+val.id;
				return false;			
			});
			
		}		
}
