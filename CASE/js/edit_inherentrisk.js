// JavaScript Document
$(function(){
		   
	$("#edit_inherentrisk").click(function(){
		var message 	= $("#inherentrisk_message")						
		var rating_from = $("#inherent_rating_from").val();
		var rating_to 	= $("#inherent_rating_to").val();
		var magnitude	= $("#inherent_risk_magnitude").val();
		var response	= $("#inherent_response").val();
		var color 		= $("#inherent_color").val();
		
		if ( rating_from == "") {
			message.html("Please enter the rating feom for this impact")
			return false;
		} else if( rating_to == "" ) {
			message.html("Please enter the rating to for this impact");
			return false;
		} else if ( magnitude == "" ) {
			message.html("Please enter the asssessment for this impact");
			return false;
		} else if( response == "" ) {
			message.html("Please enter the definition for this impact");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=updateInherentRisk",
				   {
					   id 		: $("#inherentrisk_id").val(), 
					   from		: rating_from,
					   to		: rating_to,
					   magnitude: magnitude,
					   response : response,
					   clr		: color
				}
				   , function( retData  ) {
						if( retData == 1 ) {
							message.html("Inherent risk successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Inherent risk not changed");
						} else {
							message.html("Error updating the inherent risk");
						}
			},"json")
		}
		return false;			   
	});
	
	$("#change_inherentrisk").click(function() {
		var message 	= $("#inherentrisk_message")												 
		$.post( "controller.php?action=changeInherent", 
			   {
				   id	  : $("#inherentrisk_id").val(),
				   status : $("#inherent_status :selected").val()
				}, function( deactData ) {
			if( deactData == 1){
				message.html("Inherent risk deactivated").animate({opacity:"0.0"},4000);
			} else if( deactData == 0){
				message.html("No change was made to the inherent riskstatus ").animate({opacity:"0.0"},4000);
			} else {
				message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
			}										 
		})									
		return false;									 
	});
		
	$("#cancel_inherentrisk").click(function(){
		history.back();
		return false;								   
	});
});