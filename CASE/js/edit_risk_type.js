// JavaScript Document
$(function(){
		
		$("#edit_risktype").click(function(){
		var message   	= $("#edit_risk_type");
		var shortcode 	= $("#risktype_shortcode").val();
		var type 		= $("#risktype").val();
		var description	= $("#risktype_descri").val();
		
		if ( shortcode == "" ) {
			message.html("Please the short code for this risk type")
			return false;
		} else if( type == "") {
			message.html("Please enter the risk type")
			return false;
		} else {
			$.post( "controller.php?action=updateRiskType", 
		   { 
			   id			: $("#risktype_id").val(), 
			   shortcode	: shortcode, 
			   name			: type, 
			   description	: description 
		   }, function( retData ) {
				if( retData == 1 ) {
					message.html("Risk type successfully updated .. ");
				} else if( retData == 0 ){
					message.html("Risk type not changed");
				} else {
					message.html("Error updating the risk type");
				}
			});
		}
		return false;
		});

	$("#cancel_risktype").click(function(){
		history.back();
		return false;								   
	})
	

})