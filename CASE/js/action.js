// JavaScript Document
var actionData = {};
$(function(){
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "dd-M-yy"
		});						   
	});	
	
	$.get("controller.php?action=getActions", {id : $("#risk_id").val() }, function( data ){
											
		var message = $("#risk_action_message");	
		if( $.isEmptyObject( data ) ) {
			$("#action_table")
			  .append($("<tr />")
				.append($("<td />",{html:"There were no actions found", colspan:"7"}))		
			   )	
		} else {
		  $.each( data , function( index ,val ) {
			$("#action_table")
			.append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.action}))
				.append($("<td />",{html:val.tkname+" "+val.tksurname}))				
				.append($("<td />",{html:(val.status == "" ? "New" : val.status)}))
				.append($("<td />",{html:(val.progress == "" ? "0" : val.progress)+"%"}))
				.append($("<td />",{html:""}))
				.append($("<td />")
					.append($("<input />",{type:"submit", value:"Edit", id:"edit_"+val.id, name:"edit_"+val.id}))
				)
			)
			
			$("#edit_"+val.id).live( "click", function(){
				document.location.href = "edit_action.php?id="+val.id;
				return false;
			});
			$("#delete_"+val.id).live( "click", function(){
				if(confirm("Are you sure you want to delete this action ")){								 				
					$.post("controller.php?action=deleteAction", { id : val.id }, function( ret ){
						$("#tr_"+val.id).fadeOut();
						message.addClass("ui-state-highlight").html( "Action deleted" )											  
					});
				}
				return false;
			});
			$("#update_action_table")
			.append($("<tr />")
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.action}))
				.append($("<td />",{html:val.tkname+" "+val.tksurname}))				
				.append($("<td />",{html:(val.status == "" ? "New" : val.status)}))
				.append($("<td />",{html:(val.progress == "" ? "0" : val.progress)+" % "}))
				.append($("<td />",{html:""}))
				.append($("<td />")
				   .append($("<input />",{
							 type 	: (val.isUpdate == "No" ? "hidden" : "submit"),
							 value	: "Update",
							 id		: "update_"+val.id,
							 name	: "update_"+val.id
							 }))					
				)
			)
			$("#update_"+val.id).live( "click", function(){
				document.location.href = "update_action.php?id="+val.id;
				return false;
			});
		  });	
		}
		$("#action_table")
		.append($("<tr />")
			.append($("<td />",{colspan:"7", align:"right"})
				.append($("<input />",{type:"submit", name:"add_action", id:"add_action", value:"Add New"}))		  
			)		  
		)
		$("#update_action_table")
		.append($("<tr />")
			.append($("<td />",{colspan:"7", align:"left"})
				.append($("<a />",{href:"javascript:history.back()", html:"Go Back "}))		  
			)		  
		 )
		$("#action_table")
		.append($("<tr />")
			.append($("<td />",{colspan:"7", align:"left"})
				.append($("<a />",{href:"javascript:history.back()", html:"Go Back "}))		  
			)		  
		 )		
	}, "json");
	
	$("#add_action").live( "click", function(){
		document.location.href = "add_action.php?id="+$("#risk_id").val();			 								 
		return false;								
	});

	$("#close").live("click", function(){
		$("#add_new_action").hide();		
		return false;
	});
	

	
	
});