// JavaScript Document
//View Risk
$(function(){

	$("input:button.show_dashboard").click(function(){
		var report = $(this).prop("id");
		var extra = "";
		switch(report) {
			case "report_dashboard_dir":
				var filter = $('#filter_report_dashboard_dir').val();
				extra = "&filter="+filter;
				break;
			case "report_dashboard_actions_person":
				var filter = $('#filter_report_dashboard_actions_person').val();
				extra = "&filter="+filter;
				break;
			case "report_dashboard_actions":
			case "report_dashboard":
			default:
				extra = "";
		}
		document.location.href = "dashboard.php?report="+report+extra;
		return false;
	});

	$("#show_dashboard_dir").click(function(){
		var filter = $('#filter_dashboard_dir').val();
		document.location.href = "dashboard_dir.php?filter="+filter;
		return false;
	});
	
	$("#show_queryVsStatus").click(function(){
		document.location.href = "query_vs_status.php";
		return false;
	});

	$("#show_queryVsRisk").click(function(){
		document.location.href = "query_vs_risk_type.php";
		return false;
	});
	
	$("#show_querisVsDepartment").click(function(){
		document.location.href = "query_vs_department.php";
		return false;
	});	

	$("#show_riskregister").click(function(){
		document.location.href = "fixed_report.php";
		return false;
	});		
	
	
});
