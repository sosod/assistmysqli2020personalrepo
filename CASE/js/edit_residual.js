// JavaScript Document
$(function(){
	
	$("#edit_residual").click(function(){
									   
		var message 		= $("#residual_risk_message")						
		var residual_from 	= $("#residual_risk_from").val();
		var residual_to 	= $("#residual_risk_to").val();
		var residual		= $("#residual_magnitude").val();
		var response  		= $("#residual_response").val();
		var color 			= $("#residual_color").val();
	
		if ( residual_from == "") {
			message.html("Please enter the rating from for this residual risk")
			return false;
		} else if( residual_to == "" ) {
			message.html("Please enter the rating to for this residual risk");
			return false;
		} else if ( residual == "" ) {
			message.html("Please enter the magnitude for this residual risk");
			return false;
		} else if( response == "" ) {
			message.html("Please enter the response for this residual risk");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=updateResidualRisk", 
				   {
					   id		: $("#residual_id").val(),
					   from		: residual_from,
					   to		: residual_to,
					   magn		: residual,
					   resp		: response,
					   clr		: color
					}
				   , function( retData ) {
						if( retData == 1 ) {
							message.html("Residual risk successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Residual risk not changed");
						} else {
							message.html("Error updating the residual risk");
						}
			},"json")	
		}
		return false;	 
	});
	
	$("#change_residual").click(function(){
		var message = $("#residual_risk_message")											 
		$.post( "controller.php?action=changeResidualStatus", 
			   {
				   id  		: $("#residual_id").val(),
				   status   : $("#residual_status :selected").val() 	   
				}, function( deactData ) {
			if( deactData == 1){
				message.html("Residual risk status changed").animate({opacity:"0.0"},4000);
			} else if( deactData == 0){
				message.html("No change was made to the residual risk").animate({opacity:"0.0"},4000);
			} else {
				message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
			}									 
		})									
		return false;											 
	});
	
	$("#cancel_residual").click(function(){
		history.back();
		return false;		 
	});
});