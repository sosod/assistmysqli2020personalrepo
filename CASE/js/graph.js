// JavaScript Document
$(function(){
	
		$("#inherent_vs_residual").click(function(){
			$("#bar").empty();		

			$.post("controller.php?action=residualVsInherent", function( residualInherent ){	
				var dataArray  = [];																		
				var exisLabels = [];
				$.each( residualInherent , function( index, values){
					dataArray.push([values.residual_risk,values.inherent_risk_rating]);
					exisLabels.push( values.id );
				});			
				var api = new jGCharts.Api();
				jQuery('<img>')
				.attr('src', api.make( {
									  	data 		: dataArray,
										axis_labels : exisLabels
										} ) )
				.appendTo("#bar");												  								
			},"json");
			return false;									  
		});

		$("#exposure_vs_type").click(function(){
			$("#bar").empty();											  
			var api = new jGCharts.Api();
			jQuery('<img>')
			.attr('src', api.make( {data : [[153, 60, 52],[113, 70, 60], [120, 80, 40]]} ) )
			.appendTo("#bar");												  
			return false;									  
		});
		
		$("#exposure_vs_category").click(function(){	
			$("#bar").empty();												  
			var api = new jGCharts.Api();
			jQuery('<img>')
			.attr('src', api.make( {data : [[153, 60, 52],[113, 70, 60], [120, 80, 40]]} ) )
			.appendTo("#bar");												  
			return false;									  
		});
		
		$("#exposure_vs_person").click(function(){
			$("#bar").empty();												
			var api = new jGCharts.Api();
			jQuery('<img>')
			.attr('src', api.make( {data : [[153, 60, 52],[113, 70, 60], [120, 80, 40]]} ) )
			.appendTo("#bar");												  
			return false;									  
		});	
});