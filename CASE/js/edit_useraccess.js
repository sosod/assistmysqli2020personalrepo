// JavaScript Document
$(function(){

	/**
		Adding new user access
	**/
	$("#save_user").click(function() {
		var message 	 	= $("#useraccess_message").slideDown();
		var module_admin 	= $("#module_admin :selected");
		var create_risks 	= $("#create_risk :selected");	
		var create_actions  = $("#create_actions :selected");
		var view_all 		= $("#view_all :selected");		
		var edit_all 		= $("#edit_all :selected");
		var reports 		= $("#reports :selected");	
		var assurance 		= $("#assurance :selected");
		var setup 			= $("#setup :selected");	
		
		$.post( "controller.php?action=updateUserAccess", 
		{
		  user		: $("#user_id").val(),
		  modadmin	: module_admin.val(), 
		  cr_risk	: create_risks.val(),
		  cr_actions: create_actions.val(),
		  v_all		: view_all.val(),
		  e_all		: edit_all.val(),
		  reports	: reports.val(),
		  assurance : assurance.val(),
		  setup		: setup.val()
		},
		function( retData ) {
			if( retData == 1 ) {
				message.html("User setting succefully updated");	
				history.back();
			} else if( retData == 0){
				message.html("There was no change to user setting");	
			} else {
				message.html("There was an error trying to save the user");		
			}
		},"json" )	
		return false;							  
	});
	
	$("#cancel_changes").click(function(){
		history.back();
		return false;
	});
	
});