// JavaScript Document
$(function(){
	/**
		get all the impacts
	**/
	$("#control_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getControl", function( data ) {
		var message = $("#control_message")
		message.html("");
		$.each( data , function( index, val) {
			$("#control_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.shortcode}))			  
			  .append($("<td />",{html:val.effectiveness}))
			  .append($("<td />",{html:val.qualification_criteria}))
			  .append($("<td />",{html:val.rating}))
			  .append($("<td />",{css:{"background-color":"#"+val.color}, html:val.color}))
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"ctrledit_"+val.id, name:"ctrledit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"ctrldel_"+val.id, name:"ctrldel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"ctrlact_"+val.id, name:"ctrlact_"+val.id}))	
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ),value:"Inactivate", id:"ctrldeact_"+val.id, name:"ctrldeact_"+val.id}))							
			   )
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))
			   )
			)
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_control_status.php?id="+val.id;
				return false;										  
			});			
			$("#ctrledit_"+val.id).live( "click", function() {
				document.location.href = "edit_control.php?id="+val.id;
				return false;										  
			});
			$("#ctrldel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this percieved control effectiveness")) {								  
					$.post( "controller.php?action=changeControl",
						   { id 	: val.id,
						   	 status : 0
						   }, function( delData ) {
						if( delData == 1){
							message.html("Control effectveness deleted").animate({opacity:"0.0"},4000);
							$("#tr_"+val.id).fadeOut();
						} else if( delData == 0){
							message.html("No change was made to the control effectveness ").animate({opacity:"0.0"},4000);
						} else {
							message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
						}										 
					});
				}
				return false;										  
			});
			$("#ctrlact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=ctrlactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						message.html("Control effectveness deactivated").animate({opacity:"0.0"},4000);
					} else if( actData == 0){
						message.html("No change was made to the control effectveness").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}										 
				})															 
				return false;										  
			});
		});
	}, "json")
	
	/**
		Adding a new control effectiveness
	**/
	$("#add_control").click( function() {							  
		var message 		= $("#control_message")
		var ctrl_shortcode	= $("#control_shortcode").val();		
		var control_effect	= $("#control_effect").val();
		var quali_critera 	= $("#quali_critera").val();
		var control_rating	= $("#control_rating").val();
		var color 			= $("#control_color").val();
		if ( ctrl_shortcode == "") {
			message.html("Please enter the short code for this control").show()
			return false;
		} else if( control_effect == "" ) {
			message.html("Please enter the control effectiveness ");
			return false;
		} else if ( quali_critera == "" ) {
			message.html("Please enter the qualification criteria for this control");
			return false;
		} else if( control_rating == "" ) {
			message.html("Please enter the qualifaction criteria for this control");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=newControl",
				   {
					  ctrl			: control_effect,
					  qual			: quali_critera,
					  rating		: control_rating,
					  clr			: color,
					  ctrl_shortcode: ctrl_shortcode
					}
				   , function( retData ) {
					if( retData > 0 ) {   
						$("#control_shortcode").val("")
						$("#control_effect").val("")
						$("#quali_critera").val("")
						$("#control_rating").val("")
						message.html("Control Effectiveness save successifully ....");
					
						$("#control_table")
						.append($("<tr />")
						  .append($("<td />",{html:retData}))
						  .append($("<td />",{html:ctrl_shortcode}))
						  .append($("<td />",{html:control_effect}))
						  .append($("<td />",{html:quali_critera}))
						  .append($("<td />",{html:control_rating}))				  
						  .append($("<td />",{css:{"background-color":"#"+color},html:color }))
						  .append($("<td />")
							.append($("<input />",{type:"submit", value:"Edit", id:"ctrledit_"+retData, name:"ctrledit_"+retData}))	
							.append($("<input />",{type:"submit", value:"Del", id:"ctrldel", name:"ctrldel"}))					
						   )
						  .append($("<td />",{html:"<b>Active</b>"})
							//.append($("<input />",{type:"submit", value:"Activate", id:"ctrlact", name:"ctrlact"}))	
							//.append($("<input />",{type:"submit",value:"Inactivate", id:"ctrlact", name:"ctrlact"}))							
						   )
						  .append($("<td />")
							.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))		
						   )
						)
							$("#change_"+retData).live( "click", function() {
								document.location.href = "change_control_status.php?id="+retData;
								return false;										  
							});			
							$("#ctrledit_"+retData).live( "click", function() {
								document.location.href = "edit_control.php?id="+retData;
								return false;										  
							});						
					} else {
						message.html("There was an error saving , please try again")
					}
				
			},"json")	
		}
		return false;							 
	});	
})