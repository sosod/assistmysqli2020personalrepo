// JavaScript Document
$(function(){
	/**
		get all the impacts
	**/
	$("#impact_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getImpact", function( data ) {
		$("#impact_message").html("");
		var message = $("#impact_message");
		$.each( data , function( index, val) {
			$("#impact_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.assessment}))
			  .append($("<td />",{html:val.definition}))
			  .append($("<td />",{css:{"background-color":"#"+val.color}, html:val.color }))
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"impactedit_"+val.id, name:"impactedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"impactde_"+val.id, name:"impactde_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"impactact_"+val.id, name:"impactact_"+val.id}))	
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ),value:"Inactivate", id:"impactdeact_"+val.id, name:"impactdeact_"+val.id}))							
			   )
			 .append($("<td />")
				.append($("<input />",{type:"submit", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		   
				)
			)
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_impact_status.php?id="+val.id;
				return false;										  
			});
			
			$("#impactedit_"+val.id).live( "click", function() {
				document.location.href = "edit_impact.php?id="+val.id;
				return false;										  
			});
			$("#impactde_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this impact ")) {												   
					$.post( "controller.php?action=changeImpstatus", 
						   {
							   	id		:	val.id ,
								status  : 0
								
								}, function( impdeData ) {
						if( impdeData == 1){
							message.html("Impact deactivated").animate({opacity:"0.0"},4000);
							$("#tr_"+val.id).fadeOut();
						} else if( impdeData == 0){
							message.html("No change was made to the impact ").animate({opacity:"0.0"},4000);
						} else {
							message.html("Error updating, please try again ").animate({opacity:"0.0"},4000);
						}									 
					});
				}
				return false;										  
			});
			$("#impactact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=impactactivate", {id:val.id}, function( ipmactData ) {
					if( ipmactData == 1){
						message.html("Impact activated").animate({opacity:"0.0"},4000);
					} else if( ipmactData == 0){
						message.html("No change was made to the impact ").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error updating , please try again ").animate({opacity:"0.0"},4000);
					}										 
				})															 
				return false;										  
			});
			$("#impactdeact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=impactdeactivate", {id:val.id}, function( ipmdeactData ) {
					if( ipmdeactData == 1){
						message.html("Impact deactivated").animate({opacity:"0.0"},4000);
					} else if( ipmdeactData == 0){
						message.html("No change was made to the impact ").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}						 
				})									
				return false;										  
			});
		});
	}, "json")
	
	/**
		Adding a new impact
	**/
	$("#add_impact").click( function() {
		var message 	= $("#impact_message")						
		var rating_from = $("#imprating_from").val();
		var rating_to 	= $("#imprating_to").val();
		var assessment	= $("#impact_assessment").val();
		var definition  = $("#impact_definition").val();
		var color 		= $("#impact_color").val();
	
		if ( rating_from == "") {
			message.html("Please enter the rating feom for this impact")
			return false;
		} else if( rating_to == "" ) {
			message.html("Please enter the rating to for this impact");
			return false;
		} else if ( assessment == "" ) {
			message.html("Please enter the asssessment for this impact");
			return false;
		} else if( definition == "" ) {
			message.html("Please enter the definition for this impact");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=newImpact", 
				   {
					  from		: rating_from,
					  to		: rating_to,
					  assmnt	: assessment,
					  dfn		: definition,
					  clr		: color
					}
				   , function( retData ) {
				if ( retData > 0 ) {
					$("#imprating_from").val("");
					$("#imprating_to").val("")
					$("#impact_assessment").val("")
					$("#impact_definition").val("")
					message.html("Impact saved ")
					$("#impact_table")
					.append($("<tr />")
					  .append($("<td />",{html:retData}))
					  .append($("<td />",{html:rating_from+" to "+rating_to}))
					  .append($("<td />",{html:assessment}))
					  .append($("<td />",{html:definition}))
					  .append($("<td />",{css:{"background-color":"#"+color}, html:color }))
					  .append($("<td />")
						.append($("<input />",{type:"submit", value:"Edit", id:"impactedit_"+retData, name:"impactedit_"+retData}))	
						.append($("<input />",{type:"submit", value:"Del", id:"impactdel_"+retData, name:"impactdel_"+retData}))					
					   )
					  .append($("<td />",{html:"<b>Active</b>"})
						//.append($("<input />",{type:"submit", value:"Activate", id:"impactact", name:"impactact"}))	
						//.append($("<input />",{type:"submit",value:"Inactivate", id:"impactdeact", name:"impactdeact"}))							
					   )	
					  .append($("<tr />")
						.append($("<td />")
						  .append($("<input />",{type:"button", id:"change_"+retData, name:"change_"+retData, value:"Change status"}))		  
						)		
					  )
					)
					
					$("#change_"+retData).live( "click", function() {
						document.location.href = "change_impact_status.php?id="+retData;
						return false;										  
					});
					
					$("#impactedit_"+retData).live( "click", function() {
						document.location.href = "edit_impact.php?id="+retData;
						return false;										  
					});					
				} else {
					message.html("There was an error saving the impact")
				}
			},"json")	
		}
		return false;							 
	});	
});