// JavaScript Document
//View Risk
$(function(){
	
	$("#show_inherentVsresidual").click(function(){
		document.location.href = "inherentVsResidual.php";		
		return false;
	});
		
	$("#show_inherentVsresidualperType").click(function(){
		document.location.href = "graphPerType.php";		
		return false;
	});

	$("#show_inherentVsresidualperCategory").click(function(){
		document.location.href = "graphPerCategory.php";		
		return false;
	});
	
	$("#show_inherentVsresidualperPerson").click(function(){
		document.location.href = "graphPerPerson.php";		
		return false;
	});
		

	Quick.get();	   
});

Quick	 = {
		
	get		: function(){
	
		$.getJSON("controller.php?action=getQuick", function( response ){
			$.each( response , function( index, quick){
				$("#quick_reports").append($("<tr />",{id:"tr_"+quick.id})
					.append($("<td />",{html:quick.id}))
					.append($("<td />",{html:quick.name}))
					.append($("<td />",{html:quick.description}))
					.append($("<td />",{html:"<center><b>On Screen</b><center>"}))
					.append($("<td />")
					   .append($("<input />",{
						   					type 	: "button",
						   					name	: "generate_"+quick.id,
						   					id		: "generate_"+quick.id,
						   					value	: "Generate"
					   }))
					)
					.append($("<td />")
						.append($("<input />",{
											type	: "button",
											name	: "edit_"+quick.id,
											id		: ($("#userId").val() == quick.insertuser ? "edit_"+quick.id : "edit__"+quick.id),
											value	: "Edit "
						}))
					)
				   .append($("<td />")
						.append($("<input />",{
											type	: "button",
											name	: "del_"+quick.id,
											id		: ($("#userId").val() == quick.insertuser ? "del_"+quick.id : "del__"+quick.id),
											value	: "Del"
						}))
				   )
				)	
			
			    if($("#userId").val() !== quick.insertuser)
			    {
			       $("#del__"+quick.id).attr("disabled", "disabled");
			       $("#edit__"+quick.id).attr("disabled", "disabled");
			    }
					
			    $("#generate_"+quick.id).click(function(){
			        document.location.href = "process_report.php?action=generate&id="+quick.id;
			        return false;
			    });
				
			    $("#edit_"+quick.id).click(function(){
			        document.location.href = "edit_quick_report.php?action=edit_quick&id="+quick.id;
			        return false;
			    });
					
			    $("#del_"+quick.id).click(function(){
			    	if(confirm("Are you sure you want to delete this report")){
			    		jsDisplayResult("info", "info", "Updating query...  <img src='../images/loaderA32.gif' />");
			    		$.post("controller.php?action=deleteQuickReport", { id : quick.id } ,function( response ){
							if( response.error )
							{
								jsDisplayResult("error", "error", response.text );
							} else {
								jsDisplayResult("ok", "ok", response.text );
								$("#tr_"+quick.id).fadeOut();
							}
			    		},"json") 		
			    		
			    	}  	
			        return false;
			    });
								    
				
			})	
		});	
	}


}
