// JavaScript Document
$(function(){
		   	
	$("#edit_glossary").live("click", function(){	
		var message  	= $("#glossary_message");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			message.html("Please select category")
			return false;
		} else if ( terminology == "" ) {
			message.html("Please enter the termilogy")
			return false;
		} else if ( explanation == "" ){
			message.html("Please enter the explanation")
			return false;
		} else  {
			
			$.post("controller.php?function=updateGlossary",
			  {
				id 			: $("#glossaryid").val(),
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData == 1 ) {
					message.html("Glossary term saved");
				} else if( retData == 0 ) {
					message.html("No change were made to the glossary");
				} else {
					message.html("Error saving the glossary term ");	
				}
			});
		 }
				return false;
			})


	$("#delete_glossary").click(function() {
		var message  	= $("#glossary_message");										
		if(confirm("Are you sure you want to delete this glossary item ")){
			$.post("controller.php?function=deleteGlossary",{ id : $("#glossaryid").val() }, function( retData ) {
				if( retData == 1 ){
					message.html("Glossary deleted");	
				} else {
					message.html("There was an error deleting glossary")	;
				}
			})	
		}								
		return false;								 
	});
	
	$("#cancel_glossary").live("click", function(){
		$("#new_term_table").fadeOut();										 
		return false;										 
	});
})