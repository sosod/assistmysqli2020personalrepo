<?php
$riskheadings = array();
$riskheadings['id'] = array(
	'display' => "Query Item",
	'type' => "X",
	'color'=> "N",
	'field' => "id"
);
$nogroup = array("id","insertdate","monetary_implication", "query_background","query_description","risk_detail","recommendation","client_response","query_reference", "query_date");
$nofilter = array("id");
$nosort = $nofilter;
unset($nosort[0]);

$risk_header_ids = array(1,4,7,8,11,14,16,18,20,22,31,34,32,33,25,5);
$risk_head_settings = array(
	4 => array('name'=>"type",'color'=>"N",'type'=>"L",'table'=>"types",'table_field'=>"name"),
	7 => array('name'=>"category",'color'=>"N",'type'=>"L",'table'=>"categories",'table_field'=>"name"),
	8 => array('name'=>"description",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	11 => array('name'=>"background",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	14 => array('name'=>"monetary_implication",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	16 => array('name'=>"risk_detail",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	18 => array('name'=>"finding",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	20 => array('name'=>"recommendation",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	22 => array('name'=>"client_response",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	31 => array('name'=>"query_reference",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	34 => array('name'=>"risk_type",'color'=>"N",'type'=>"L",'table'=>"risk_types",'table_field'=>"name"),
	32 => array('name'=>"financial_exposure",'color'=>"N",'type'=>"F",'table'=>"financial_exposure",'table_field'=>"name"),
	33 => array('name'=>"risk_level",'color'=>"N",'type'=>"L",'table'=>"level",'table_field'=>"name"),
	25 => array('name'=>"query_date",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
//	, 5 => array('name'=>"risk_owner",'color'=>"N",'type'=>"L",'table'=>"",'table_field'=>"value")
);
$sql = "SELECT * FROM ".$dbref."_header_names WHERE active = 1 ORDER BY ordernumber";
include("../../inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	if(isset($risk_head_settings[$row['id']])) {
		$rhs = $risk_head_settings[$row['id']];
		$riskheadings[$rhs['name']] = array(
			'display' => $row['client_terminology'],
			'type' => $rhs['type'],
			'color' => $rhs['color'],
			'field' => $rhs['name'],
			'table' => $rhs['table'],
			'table_field' => $rhs['table_field']
		);
	}
}
mysql_close($con);
$riskheadings['status'] = array(
	'display' => "Query Status",
	'type' => "L",
	'color'=> "N",
	'field' => "status",
	'table' => "status",
	'table_field' => "client_terminology"
);
$risk_head_settings[99] = array('name'=>"query_status",'color'=>"N",'type'=>"L",'table'=>"status",'table_field'=>"client_terminology");
$riskheadings['insertdate'] = array(
	'display' => "Query Creation Date",
	'type' => "D",
	'color'=> "N",
	'field' => "insertdate"
);
/*$riskheadings['risk_update'] = array(
	'display' => "Risk Update",
	'type' => "U",
	'color'=> "N",
	'field' => "risk_update"
);*/



$head = array_merge($riskheadings);
?>