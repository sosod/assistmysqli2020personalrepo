<?php
/**
	Reports , this generates reports
	* @package	 : Report
	* @author 	 : admire<admire@trafficsynergy.com>
	* @copyright : 
**/
class Report extends  DBConnect
{

	protected $generateArray= array();
	
	protected $sql 			=  "";
	
	protected $where		=  "";
	
	protected $from 		=  "";
	
	protected $field		= "";
	
	protected $groupby		= "";
	
	protected $_errors 		= array();	
	
	protected $fieldTable  = array( 
						 "risk_item" => array(
							"table" 		=> "",
							"prefix" 		=> "RR",
							"joinField" 	=> "",
							"joinWith"		=> "RR",
							"name"			=> "id", 	
							"searchField" 	=> "id"								
						),
						"control_effectiveness" => array(
							"table"	 		=> "control_effectiveness",
							"prefix" 		=> "CE",
							"joinField" 	=> "",
							"name"			=> "color", 
							"joinWith"		=> "RR",														
							"searchField" 	=> "id"								
						),
						 "risk_type" => array(
							"table"		 	=> "types",
							"prefix" 	 	=> "RT",
							"joinField"	 	=> "type",
							"joinWith"		=> "RR",							
							"as"			=> "risktype",
							"name"			=> "name",
							"searchField" 	=> "id"							
						),
						"risk_owner" => array(
							"table"	 		=> "timekeep",
							"prefix" 		=> "TK", 
							"joinField" 	=> "risk_owner",
							"as"			=> "RiskOwner",
							"name"			=> "tkname",
							"joinBy"		=> "tkid",
							"joinWith"		=> "RR",																
							"searchField"   => "tkid" 										
						),								
						"risk_category" => array(
							"table"	 		=> "categories",
							"prefix" 		=> "RC",		
							"joinField" 	=> "category",
							"joinWith"		=> "RR",							
							"name" 			=> "name",		
							"as"			=> "catergory",					
							"searchField"   => "id"																
						),								
						"risk_comments" => array(
							"table"	 		=> "risk_update",
							"prefix" 		=> "RR",
							"joinField" 	=> "",
							"joinWith"		=> "RR",																		
							"name" 			=> "response",													 		
							"searchField" 	=> "response"								
						),								
						"risk_description" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinField" 	=> "",	
							"name" 			=> "description",																	
							"joinWith"		=> "RR",	
							"searchField" 	=> "description"																					
						),								
						"risk_action" => array(
							"table"		  => "actions",
							"prefix" 	  => "RA",
							"joinField"   => "id",
							"name"		  => "action",
							"joinBy"	  => "risk_id",
							"joinWith"	  => "RR",														
							"searchField" => "action"							
						),
						"action_comments" => array(
							"table"	 		=> "actions_update",
							"prefix" 		=> "RAU" ,
							"name"		    => "description",							
							"joinBy"	 	=> "action_id",
							"as"			=> "action_comments",							
							"joinField" 	=> "id",								 		
							"joinWith"		=> "RA",
							"join"			=> "INNER",	
							"searchField"   => "description"																											
						),
						"action_status" => array(
							"table"	 	   => "action_status",
							"prefix" 	   => "RAS",
							"joinField"    => "status",
							"joinWith"	   => "RA",							
							"as"		   => "action_status",
							"name"		   => "name",			
							"searchField"  => "name"													
						),	
						"risk_background" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinField" 	=> "",	
							"name"		    => "background",																
							"joinWith"		=> "RR",	
							"searchField"  => "background" 																					
						),
						"deliverable" => array(
							"table"	 		=> "",
							"prefix" 		=> "RA",
							"joinField" 	=> "",
							"joinWith"		=> "RR",														
							"name"			=> "deliverable",
							"searchField"  => "deliverable"															
						),
						"impact" => array(
							"table"	 		=> "impact",
							"prefix" 		=> "IM",
							"joinField" 	=> "impact",
							"joinWith"		=> "RR",														
							"name"			=> "assessment",
							"searchField"   => "id"							
						),								
						"action_owner" => array(
							"table"	 		=> "timekeep",
							"prefix" 		=> "TKA",
							"joinField"		=> "action_owner",
							"joinWith"		=> "RA",														
							"as"			=> "ActionOwner",
							"name"			=> "tkname",
							"joinBy"		=> "tkid",
							"searchField"   => "tkid"								
						),								
						"impact_rating" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinField"		=> "",
							"joinWith"		=> "RR",														
							"name"			=> "impact_rating",							
							"searchField"  => "impact_rating"							
						),								
						"timescale" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinWith"		=> "RR",														
							"joinField" 	=> "",
							"name"			=> "time_scale",		
							"searchField"   => "time_scale"																													
						),								
						"likelihood" => array(
							"table"	 		=> "likelihood",
							"prefix" 		=> "LK",
							"joinField"		=> "likelihood",
							"joinWith"		=> "RR",							
							"as"			=> "likelihood",
							"name"			=> "assessment",
							"searchField"   => "id"															
						),
						"deadline" => array(
							"table"		   => "",
							"prefix" 	   => "RA",
							"joinField"	   => "",
							"joinWith"	   => "RR",							
							"name"		   => "deadline",
							"searchField"  => "id" 							
						),															
						"progress" => array(
							"table"	 	 	=> "actions_update",
							"prefix" 	 	=> "RUP",
							"joinBy"		=> "action_id",
							"joinField"	 	=> "id",
							"joinWith"		=> "RA",							
							"name"			=> "progress",		
							"searchField" 	=> "progress"										
						),								
						"current_controls" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinField"		=> "",
							"joinWith"		=> "RR",							
							"name"			=> "current_controls",														 		
							"searchField" 	=> "current_controls"							
						),								
						"signoff" => array(
							"table"	 		=> "",
							"prefix" 		=> "RAU" ,
							"name"		    => "deadline",							
							"as"			=> "signoff",
							"joinBy"	 	=> "",
							"joinField" 	=> "",								 		
							"joinWith"		=> "",				
							"searchField" 	=> "signoff"
						),
						"percieved_control_effective" => array(
							"table"	 		=> "control_effectiveness",
							"prefix" 		=> "RCE",
							"joinField"		=> "percieved_control_effectiveness",	
							"joinWith"		=> "RR",							
							"name" 			=> "effectiveness", 								
							"searchField" 	=> "id"							
						),
						"control_rating" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinField"		=> "",	
							"joinWith"		=> "RR",							
							"name" 			=> "control_effectiveness_rating",
							"searchField" 	=> "control_effectiveness_rating"							 														
						),
						"from_date" => array(
							"table"	 		=> "",
							"prefix" 		=> "RR",
							"joinField" 	=> "",	
							"joinWith"		=> "RR",							
							"name"		  	=> "insertdate",
							"searchField" 	=> "insertdate"																				 		
						),
						"to_date" => array(
							"table"	 		=> "",
							"joinField"		=> "",
							"prefix" 		=> "RR",
							"joinWith"		=> "RR",									
							"name"			=> "insertdate",
							"searchField" 	=> "insertdate"																				
						),								
						"risk_status" => array(
							"table"	 	   => "status",
							"prefix" 	   => "RS",
							"joinField"    => "status",
							"joinWith"		=> "RR",							
							"as"		   => "status",
							"name"		   => "name",									
							"searchField"  => "id" 								
						),												
						"risk_comments" => array(
							"table"	 	   => "",
							"prefix" 	   => "RR",
							"joinField"    => "",
							"joinWith"	   => "RR",							
							"name"		   => "",
							"searchField"  => "response" 								
						),								
						"action" => array(
							"table"	 	  => "actions",
							"prefix" 	  => "RA", 
							"joinField"   => "",
							"joinWith"	  => "RR",								
							"searchField" => "id",
							"name"		  => "action"
						),								
						"likelihood_rating" => array(
							"table"	 	=> "",
							"prefix" 	=> "RR",
							"joinField"	=> "",
							"joinWith"	=> "RR",							
							"name"		=> "likelihood_rating" ,
							"searchField"  => "likelihood_rating"									
						),								
						"assurance" => array(
							"table"	 	=> "",
							"prefix" 	=> "A",		
							"joinField" => "id",
							"joinWith"	=> "RR",							
							"name"		=> "",
							"searchField"  => "id"													
						)																											
			);	
	

	function __construct()
	{
		parent::__construct();
	}

/*	function generateReport( $mainArray )
	{
		$this -> generateArray = $mainArray;
		$headers = array();
		$where = ""; 
		$this -> from = $_SESSION['dbref']."_risk_register RR ";
		
		foreach( $this -> generateArray  as $field => $arrayValue ) {
		// get the table prrefixes for the company
		
		 //$headers[] = $this -> _createHeaders( $field ); 
		 
		// $this -> field = $this -> _createFields( $arrayValue );
		$dbref = "";
		if( $arrayValue['tablePrefix'] == "TK" || $arrayValue['tablePrefix'] == "TKA" ) {
			$dbref = "assist_".$_SESSION['cc'];
		} else {
			$dbref = $_SESSION['dbref'];
		}
		// get the field to search for in the database table
		$join = "";
		if(isset($arrayValue['searchArray'])){
			if( is_array($arrayValue['searchArray'])  && !empty($arrayValue['searchArray']) ) {
				foreach( $arrayValue['searchArray'] as $key => $value ){
					$this -> where .= $arrayValue['tablePrefix'].".".$value." LIKE '".$arrayValue['value']."' OR ";
					$join = $value;
					
					// grouping 
					if( isset($arrayValue['grouping']) && !empty($arrayValue['grouping']) ){
						if($arrayValue['grouping'] == "Yes") {
							$this -> groupby = " GROUP BY ".$arrayValue['tablePrefix'].".".$value;
						}
					}
				}
			} else {
				//$where .= $arrayValue['tablePrefix'].".".$field." LIKE '".$arrayValue['value']."' OR";
			}
		}

		// the fields to return 
		$this -> field   .= ($arrayValue['returnField'] == "" ? "" : $arrayValue['tablePrefix'].".".$arrayValue['returnField']." ".(!empty($arrayValue['AS']) && isset($arrayValue['AS']) ? " AS ".$arrayValue['AS']   : "")." , ");
		
		if( $arrayValue['returnField'] == "" ) {
			
		} else {
			$headers[] =  ucwords( str_replace( "_", " ",(!empty($arrayValue['AS']) && isset($arrayValue['AS']) ? $arrayValue['AS']   : $arrayValue['returnField']) ) );
		}
		// this is condition to search
		// tables to search and the joins
		$this -> from    .= 
		($arrayValue['table'] == "" ? "" : "LEFT JOIN ".$dbref."_".$arrayValue['table']." ".$arrayValue['tablePrefix']." ON ".$arrayValue['tablePrefix'].".".($join == "" ? "id" : $join)." = RR.".($arrayValue['join'] == "" ? $field : $arrayValue['join']))."\r\n\n";
		
		
	
		
		}
		
		$this -> field  = rtrim( $this -> field, " ," );
		$this -> where  = rtrim( $this -> where, " OR" );
		$this -> sql    = "SELECT ".$this -> field. " FROM ".$this -> from." WHERE ".($this -> where == "" ? 1 : $this -> where)." ".$this -> groupby;
		echo trim($this -> sql);
		//print_r($headers);
		$response = $this -> get( $this -> sql );
		return array("headers" => $headers, "data" => $response);
		
		}	*/
	
		function _createHeaders( $id  ) {
			$heading  = array();
			$convertToDbField = array( 
									"risk_item"   		=>  "riskitem",
									"risk_type"	  		=>  "risktype",
									"risk_owner"		=>  "riskowner",
									"risk_category" 	=>  "riskcategory",
									"risk_description"	=>  "riskdescription",
									"risk_background"	=>  "background"
 								 );
			$heading[] = ucwords( str_replace( "_", " ", $id ) );
			return $heading;
		}
		
		function getField( $name ) {
			//echo "From name => ".$name." => ";
			$fields = ""; 
			foreach( $this ->fieldTable as $key => $value ) {
			 if( trim($name) == trim($key) ) {
				if( $value['name'] !== "")	{				
					if( isset($value['as']) ) {
						$fields = $value['prefix'].".".$value['name']." AS ".$value['as'].",\r\n";
					} else {
						$fields = $value['prefix'].".".$value['name'].",\r\n";
					}
				}
			 }	
		  }		  
		    //echo "Field => ".$fields."\r\n\n";
			return $fields;
		} 
		
		function getGrouping( $name ) {
			$grouping = "";
				foreach( $this ->fieldTable as $key => $groupValue ) {
				 if( trim($name) == trim($key) ) {
					if( $groupValue['name'] !== "")	{				
						$grouping = "GROUP BY ".$groupValue['prefix'].".".$groupValue['searchField'];
					}
				 }	
			  }		
			return $grouping;		
		}
		
		function _createFrom( ) {
		
		
		}
		/*
			Createe the where string , from  the values entered
		*/
		function getWhere( $postedVaules ) {
			$where  = "";
			$valueS = ""; 
			foreach( $postedVaules as $inx => $valueArray ) {
			  if( $valueArray['value'] !== "" || !empty($value['value']) ) {
				$where  .= $this -> _getFieldToSearch( ltrim($valueArray['name'], "_" ))." = '".$valueArray['value']."' AND ";
				$valueS .= $valueArray['value']."";			  
			  }	
			}
			if( empty($valueS) && $valueS == "" ) {	
				//echo $valueS."WHERE 1";		
				return 1;
			} else {
				//echo $valueS."WHERE ".rtrim($where, " AND ")."<br />";
				return rtrim($where, " AND ")."\r\n";
			}
		}
		/*
		Fetches the report headers , 
		*/
		function getHeaders( ){
			$response = $this -> get(" SELECT * FROM ".$_SESSION['dbref']."_header_names ");
			$headerss = array();
			foreach( $response as $key => $headArray ) {
				$headers[$headArray['name']]  =  $headArray['client_terminology'];
			}
			return $headers;
		}
		/*			
		 Creates the table prefix for the field to be searched
		*/
		function _getFieldToSearch( $item ) {
			$stringField  = "";
			$tablePrefix  = $this -> fieldTable;

			if( isset( $tablePrefix[$item] ) && !empty( $tablePrefix[$item]) ) {
				$stringField = $tablePrefix[$item]['prefix'].".".$tablePrefix[$item]['searchField'];
			}
			return $stringField;
		}
		
		function getTableFrom( $field ) {
			$from = "";
			foreach( $this -> fieldTable as $key => $tableValue ) {
			 if( $field == $key ) {
			 	if( $tableValue['table'] !== "" ) {
			  		if( $tableValue['prefix'] == "TK" || $tableValue['prefix'] == "TKA" ) {
					
			  			$from = " \r\n LEFT JOIN assist_".$_SESSION['cc']."_".$tableValue['table']." ".$tableValue['prefix']." ON ".$tableValue['prefix'].".".(isset($tableValue['joinBy']) ? $tableValue['joinBy'] : "id")." = ".$tableValue['joinWith'].".".$tableValue['joinField'];
					} else {
						$from = " \r\n LEFT JOIN ".$_SESSION['dbref']."_".$tableValue['table']." ".$tableValue['prefix']." ON ".$tableValue['prefix'].".".(isset($tableValue['joinBy']) ? $tableValue['joinBy'] : "id")."= ".$tableValue['joinWith'].".".$tableValue['joinField'];
					}
			  	}
			  }	
			}
			return $from;
		}	
	
		
		function generateReport( $fields, $where , $from, $groupby ) {
			$this -> sql = "SELECT ".$fields."\r\n FROM ".$from."\r\n WHERE ".$where."\r\n ".$groupby;
			$response = $this -> get( $this -> sql );
			//echo $this -> sql."\r\n";
			return $response;			
		}
		
		function residualVsInherent()
		{
			$response = $this -> get("SELECT id, inherent_risk_rating, residual_risk FROM ".$_SESSION['dbref']."_risk_register");
			return $response;
		}
		
}
?>