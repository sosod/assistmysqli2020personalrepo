<?php
/**
	* @package 	: columns
	
**/
class QueryColumns extends Naming
{

	protected $id;
	
	protected $name;
	
	protected $client_terminology;
	
	protected $ordernumber;
	
	protected $active;
	
	function __construct($section = "")
	{
		parent::__construct($section);
	}
		
	function getNaming($optionStr =  "") {
		$response = $this -> get("SELECT id, name, client_terminology, ordernumber, active FROM #_header_names 
		                          WHERE type = 'query' ".$this -> optionSql." 
						          ORDER BY ordernumber ASC"
						        );
        $response = array_merge($response, array());
		return $response;	
	}
}
?>
