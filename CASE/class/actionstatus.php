<?php
/**
	* Action Status
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 ,Ignite Assist Risk Module
**/
class ActionStatus extends DBConnect
{
	
	protected $risk_status;
	
	protected $client_term;
	
	protected $color;
	
	function __construct( $risk_status, $client_term, $color)
	{
		$this -> risk_status = trim($risk_status);
		$this -> client_term = trim($client_term);
		$this -> color 		 = trim($color);
		parent::__construct();
	}
	
	function saveStatus()
	{
		$insert_data = array( 
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color,
						"insertuser"    	 => $_SESSION['tid'],						 
						);
		$response = $this -> insert( "action_status" , $insert_data );
		return $this -> insertedId();		
	}

	function getStatus()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE status & 2 <> 2" );
		echo json_encode( $response );		
	}	

	function getStatuses()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE status & 2 <> 2" );
		return $response;		
	}	
	
	function getOrderedStatuses()
	{
	   $statuses = $this -> getStatuses();
	   $tmp      = array();
	   $s_list   = array();
	   foreach($statuses as $s_index => $s_val)
	   {
	      if($s_val['id'] == 3)
	      { 
	        $tmp[$s_val['id']]    = $s_val;
	      } else {
	        $s_list[$s_val['id']] = $s_val;
	      }
	   }   
	   $status_list = array_merge($s_list, $tmp);
	   return $status_list;
	}
	
	function getAStatus( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE id = '".$id."'" );
		return $response;		
	}	

	function updateStatus( $id )
	{
		$insertdata = array(
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color,
						"insertuser"    	 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "action_status");
		$response = $this -> update( "action_status", $insertdata , "id=$id");
		return $response;
	}
	
	function changeActionStatus( $id, $status)
	{
        $logsObj = new Logs();
        $data = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "action_status");
		$updatedata = array(
						'status' 		=> ($status == 0 ? 0 : $data['status'] + $status),
						"insertuser"    => $_SESSION['tid'],						
		);

        
		$response = $this -> update( 'action_status', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateStatus( $id )
	{
        $logsObj   = new Logs();
        $data      = $this->getAStatus( $id );
		$update_data = array(
					'status' 		 => $data['status'] + $status,
					"insertuser"     => $_SESSION['tid'],						
		);

        $logsObj -> setParameters( $_POST, $data, "action_status");
		$response  = $this -> update( 'action_status', $update_data, "id=$id" );
		echo $response;
	}

	function getReportList() {
		return $this->getStatuses();
	}

}
?>