<?php

class QUERY_IMPORT extends QUERY {

	protected $lists = array(
		'action_owner'			=> array(
			'canImport' => false,
		),
		'action_status'			=> array(
			'canImport' => false,
		),
		'financial_exposure'			=> array(
			'canImport' => false,
		),
		'financial_year'			=> array(
			'canImport' => true,
		),
		'query_owner'			=> array(
			'canImport' => true,
		),
		'query_source'			=> array(
			'canImport' => true,
		),
		'query_status'			=> array(
			'canImport' => false,
		),
		'query_type'			=> array(
			'canImport' => true,
		),
		'risk_level'			=> array(
			'canImport' => true,
		),
		'risk_type'			=> array(
			'canImport' => true,
		),
		'query_category'			=> array(
			'canImport' => true,
		),
	);




	public function __construct() {
		parent::__construct();
	}





	/* GET IMPORT TEMPLATE - FOR SUPPORT > IMPORT */
	function readImportTemplate($file) {
		//$file = strtolower($key)."_template.csv";
		$import = fopen($file,"r");
		$data = array();
		while(!feof($import)) {
			$tmpdata = fgetcsv($import);
			if(count($tmpdata)>1) {
				$data[] = $tmpdata;
			}
			$tmpdata = array();
		}
		fclose($import);
		return $data;
	}
	
	
	function getListsForImport() {
		$l = array();
		foreach($this->lists as $key => $i) {
			if($i['canImport']) {
				$l[] = $key;
			}
		}
		return $l;
	}
	
	function getListData($list,$activeOnly=false) {
		/****** RETURNED ARRAY ********
		$data = array(
			[id] => array(
				'id'=>[id],
				'value'=> [name],
				'active'=>[1=in use,2=deactivated],
			)
		);
		*******************************/
		$data = array('rows'=>array(),'values'=>array());
		$sql = "";
		switch($list) {
			case "financial_year":
				$s = "SELECT id, start_date, end_date, active FROM ".$this->getDBRef()."_financial_years WHERE ".($activeOnly ? "active = ".FINANCIALYEAR::ACTIVE : "active <> ".FINANCIALYEAR::DELETED)." ORDER BY STR_TO_DATE(start_date, '%d-%M-%Y')";
				$rows = $this->mysql_fetch_all_by_id($s,"id");
				$data['rows'] = $rows;
				foreach($rows as $i=>$r) {
					$data['values']['financial_year'][strtotime($r['start_date'])."_".strtotime($r['end_date'])] = $r['id'];
					$data['values']['start'][strtotime($r['start_date'])] = $r['id'];
					$data['values']['end'][strtotime($r['end_date'])] = $r['id'];
					$data['rows'][$i]['value'] = $r['start_date']." - ".$r['end_date'];
				}
				break;
			case "query_owner":
				$s = "SELECT dirid, dirtxt, D.active as diractive, subid, subtxt, S.active as subactive, subdirid FROM ".$this->getDBRef()."_dir D INNER JOIN ".$this->getDBRef()."_dirsub S ON subdirid = dirid WHERE S.active = 1 AND D.active = 1 ORDER BY dirsort, subsort";
				$rows = $this->mysql_fetch_all($s);
				$data['rows']['dir'] = array();
				$data['rows']['dirsub'] = array();
				$data['values']['dir'] = array();
				$data['values']['dirsub'] = array();
				$data['values']['query_owner'] = array();
				foreach($rows as $r) {
					$d = $r['dirtxt'];
					$data['rows']['dir'][$r['dirid']] = array(
						'id'	=> $r['dirid'],
						'value'	=> $this->decode($r['dirtxt']),
						'active'=> $r['diractive'],
					);
					$data['values']['dir'][$this->stripStringForComparison($d)] = $r['dirid'];
					$s = $r['subtxt'];
					$data['rows']['dirsub'][$r['subid']] = array(
						'id'	=> $r['subid'],
						'value'	=> $this->decode($r['subtxt']),
						'active'=> $r['subactive'],
						'dirid'=> $r['subdirid'],
					);
					$data['values']['dirsub'][$this->stripStringForComparison($s)] = $r['subid'];
					$data['values']['query_owner'][$this->stripStringForComparison($d." - ".$s)] = $r['subid'];
				}
				break;
			case "query_source":
				$sql = "SELECT id, name as value, status as active FROM ".$this->getDBRef()."_".$list." WHERE ".($activeOnly ? "status = ".QUERYSOURCE::ACTIVE : "status <> ".QUERYSOURCE::DELETED)." ORDER BY name";
				break;
			case "financial_exposure":
				$sql = "SELECT id, name as value, status as active FROM ".$this->getDBRef()."_".$list." WHERE ".($activeOnly ? "status = ".FINANCIALEXPOSURE::ACTIVE : "status <> ".FINANCIALEXPOSURE::DELETED)." ORDER BY name";
				break;
			case "financial_exposure":
				$sql = "SELECT id, name as value, status as active FROM ".$this->getDBRef()."_".$list." WHERE ".($activeOnly ? "status = ".FINANCIALEXPOSURE::ACTIVE : "status <> ".FINANCIALEXPOSURE::DELETED)." ORDER BY name";
				break;
			case "query_type":
				$sql = "SELECT id, name as value, active FROM ".$this->getDBRef()."_types WHERE ".($activeOnly ? "active = ".QUERYTYPE::ACTIVE : "active <> ".QUERYTYPE::DELETED)." ORDER BY name";
				break;
			case "risk_type":
				$sql = "SELECT id, name as value, active FROM ".$this->getDBRef()."_risk_types WHERE ".($activeOnly ? "active = ".RISKTYPE::ACTIVE : "active <> ".RISKTYPE::DELETED)." ORDER BY name";
				break;
			case "risk_level":
				$sql = "SELECT id, name as value, status as active FROM ".$this->getDBRef()."_level WHERE ".($activeOnly ? "status = ".RISKLEVEL::ACTIVE : "status <> ".RISKLEVEL::DELETED)." ORDER BY name";
				break;
			case "query_category":
				$sql = "SELECT id, name as value, active FROM ".$this->getDBRef()."_categories WHERE ".($activeOnly ? "active = ".RISKCATEGORY::ACTIVE : "active <> ".RISKCATEGORY::DELETED)." ORDER BY name";
				break;
			case "action_owner":
				$sql = "SELECT TK.tkid as id, CONCAT(TK.tkname,' ',TK.tksurname) as value, 1 as active 
						FROM assist_".$this->getCmpCode()."_timekeep TK
						INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users mmu
						ON mmu.usrtkid = TK.tkid AND UPPER(mmu.usrmodref) = '".strtoupper($this->getModRef())."'
						WHERE TK.tkstatus = 1
						ORDER BY TK.tkname, TK.tksurname";
				break;
		}
		if(strlen($sql)>0) {
			$rows = $this->mysql_fetch_all_by_id($sql,"id");
			$data['rows'] = $rows;
			foreach($rows as $r) {
				$data['values'][$this->stripStringForComparison($r['value'])] = $r['id'];
			}
		}
	
		return $data;
	}
	
	function getIDsInUse($key) {
		$sql = "SELECT id FROM ".$this->getDBRef()."_".($key=="OBJECT" ? "query_register" : "actions");
		return $this->mysql_fetch_all_by_value($sql,"id");
	}
	
	
	function getObjects() {
		$sql = "SELECT id, description, query_reference FROM ".$this->getDBRef()."_query_register WHERE active & ".Risk::ACTIVE." = ".Risk::ACTIVE." AND active & ".Risk::DELETED." <> ".Risk::DELETED;
		return $this->mysql_fetch_all_by_id($sql,"id");
	}
	
	
}

?>