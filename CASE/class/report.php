<?php
/**
	Reports , this generates reports
	* @package	 : Report
	* @author 	 : admire<admire@trafficsynergy.com>
	* @copyright : 
**/
class Report extends  DBConnect
{

	protected $generateArray= array();
	
	protected $sql 			=  "";
	
	protected $where		=  "";
	
	protected $from 		=  "";
	
	protected $fields		= "";
	
	protected $groupby		= "";
	
	protected $_errors 		= array();

    protected $actionFields = "";
    
    private $headers        = array();


	function __construct()
	{
		parent::__construct();
	}
	
	function generateReport($reportdata = array())
	{
	   $reportinfo = array();
	   if(!isset($reportdata) && empty($reportdata))
	   {
	     $reportdata = $_REQUEST;
	   }
	   $field_str = $this -> _get_fields($reportdata['header']);
	   $where_str = $this -> _get_where($reportdata['values'], $reportdata['match']); 
        $group_str = $this -> _get_groupby($reportdata['group_by']);
        $sort_str  = $this -> _get_sortby($reportdata['sort']);	   
	   $query     = "SELECT ".$field_str." FROM ".$_SESSION['dbref']."_query_register QR 
	                 LEFT JOIN ".$_SESSION['dbref']."_types QT ON QT.id = QR.type
	                 LEFT JOIN ".$_SESSION['dbref']."_categories QC ON QC.id = QR.category
	                 LEFT JOIN ".$_SESSION['dbref']."_risk_types QRT ON QRT.id = QR.risk_type
	                 LEFT JOIN ".$_SESSION['dbref']."_level QRL ON QRL.id = QR.risk_level 
	                 LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FIN ON FIN.id = QR.financial_exposure
	                 LEFT JOIN ".$_SESSION['dbref']."_status QS ON QS.id = QR.status
	                 LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id 
	                 INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
	                 WHERE 1 ".$where_str." ";
	   if(!empty($group_str))
	   {
	     $query .= $group_str;
	   }
	   $query  .= $sort_str;
	   $results = $this -> get($query);
	   $data    = array('headers' => $reportdata['header'], 'actionheaders' => array(), 'data' => $results);
	   return $data;
	}
	
	private function _get_fields($fields)
	{

        $fields_matching = array(
                                 "query_item" 	    => "QR.id AS query_item" ,
                                 "query_type" 	    => "QT.name AS query_type" ,
                                 "query_category"     => "QC.name AS query_category" ,
                                 "query_description"  => "QR.description" ,
                                 "query_background"   => "QR.background" ,
                                 "query_status"  	    => "QS.name AS query_status",
                                 "financial_exposure" => "FIN.name AS financial_exposure",
                                 "risk_type"		    => "QRT.name AS risk_type",
                                 "risk_level"	    => "QRL.name AS risk_level",
                                 "query_owner"        => "D.dirtxt  AS query_owner"
          );
	   $field_str = "";
        $headersObj  = new Naming();
        $headers     = $headersObj -> getReportingNaming();
	   foreach($fields as $field => $val)
	   {
	      if($field == "status")
	      {   
	         $field_str                     .= "QS.name AS query_status ,";
	         $this -> headers['query_status'] = $headers['query_status'];
	      } else if($field == "financial_exposure") {   
	         $field_str                      .= "FIN.name AS financial_exposure ,";
              $this -> headers['financial_exposure'] = $headers['financial_exposure'];
	      } else if($field == "risk_type") {   
	         $field_str                   .= "QRT.name AS risk_type ,";
	         $this -> headers['risk_type'] = $headers['risk_type'];
	      } else if($field == "risk_level") {   
	         $field_str                    .= "QRL.name AS risk_level ,";
	         $this -> headers['risk_level'] = $headers['risk_level'];
	      } else if($field == "query_owner") {   
	         $field_str                     .= "D.dirtxt AS query_owner ,";
	         $this -> headers['query_owner'] = $headers['query_owner'];
	      } else if($field == "category") {   
	         $field_str                        .= "QC.name AS query_category ,";
	         $this -> headers['query_category'] = $headers['query_category'];
	      } else if($field == "type") {   
	         $field_str                    .= "QT.name AS query_type ,";
	         $this -> headers['query_type'] = $headers['query_type'];
	      } else {
	        if(isset($headers['query_'.$field]))
	        {
	           $field_str  .= " QR.".$field." ,";
	           $this -> headers[$field] = $headers['query_'.$field];
	        } else {
	           $field_str  .= " QR.".$field." ,";
	           $this -> headers[$field] = ucwords(str_replace('_', ' ',$field));	          
	        }
	      }
	   }
	   $field_str = rtrim($field_str, ",");
	   return $field_str;
	}
	
	function _get_where($values, $matches)
	{
	  $where_str = "";
	  $date_str  = "";
	  foreach($values  as $key => $value)
	  {
	     if(substr($key, -4) == "date")
	     {
	         if(!empty($values['from_query_date']) && !empty($values['to_query_date']))
	         {
                  $date_str = " AND QR.query_date BETWEEN ".$values['from_query_date']." AND ".$values['to_query_date']." \r\n";
	         }
	     } else if(is_array($value))  {
	        $multiple_str = "";
             foreach($value as $index => $key_val)
             {
               if($key_val !== "all")
               {
                  $multiple_str .=  " QR.".$key." = '".$key_val."' OR"; 
               }
             }                 
             if(!empty($multiple_str))
             {
                $where_str .= " AND (".rtrim($multiple_str, "OR").")  \r\n";
             }
	     } else if(!empty($value)) {
            if(isset($matches[$key]))
            {
			if($matches[$key] == "all")
			{
		        $where_str  .= " AND QR.".$key." LIKE '%".$value."%' \r\n";
			} else if($matches[$key] == "any") {
		        $where_str .= " AND QR.".$key." LIKE '".$value."' \r\n";
			} else if($matches[$key] == "exact") {
			  $where_str .= " AND QR.".$key." = '".$value."' \r\n";
			}
            } else {
               $where_str .= " AND QR.".$key." = '".$value."' \r\n";
            }
	     }
	  }
	  $where_str =  $where_str." ".$date_str;
	  return $where_str;
	}
	
	function _get_groupby($groupby = "")
	{
	   $group_str = "";
	   if(!empty($groupby))
	   {
	      $group_str = " GROUP BY QR.".rtrim($$groupby, "_");
	   }
	   return $group_str;
	}
	
	function _get_sortby($sortarray = array())
	{
	   $sort_str = " ORDER BY ";
	   foreach($sortarray as $index => $key)
	   {
	     $sort_str .= " QR.".str_replace('__', '', $key)." ,";
	   }
	   $sort_str = rtrim($sort_str, ",");
	   return $sort_str;
	}
	
	
		function _generateHeaders( $headers )
		{

			$queryFields = array(
								  "query_item" 		  => "QR.id AS query_item" ,
								  "query_type" 		  => "QT.name AS query_type" ,
								  "query_category"    => "QC.name AS query_category" ,
								  "query_description" => "QR.description" ,
								  "query_background"  => "QR.background" ,
								  "query_status"  	  => "QS.name AS query_status",
								  "financial_exposure"=> "FIN.name AS financial_exposure",
								  "risk_type"		  => "QRT.name AS risk_type",
								  "risk_level"		  => "QRL.name AS risk_level",
                                  "query_owner"       => "D.dirtxt  AS query_owner"
								);
		
		
			$headersObj  = new Naming();
			$headerNames = $headersObj -> getNamingConversion();
			$headerData  = array("query_item" => "Query Item");
			$fields 	 = "QR.id AS query_item ,";
			foreach($headers as $key => $values)
			{
				if( array_key_exists($key ,$headerNames) )
				{
					$fields .=  (isset($queryFields[$key]) ? $queryFields[$key] : " QR.".$key).",";
					$headerData[$key] = $headerNames[$key];
				}
			}		
			$this->fields = rtrim($fields, ",");
			return $headerData;
		}
		
	
	/*
	function generateReport()
	{
		$headers = $this -> _generateHeaders( $_REQUEST['header']);
		$where   = $this -> _generateWhereQuery( $_REQUEST['values']);
          $actionHeaders = $this->_getActionHeaders($_REQUEST['aheader']);
		$join 	 = $this -> joinQuery();
		$group 	 = $this -> _createGroup($_REQUEST['group_by']);
		$sort 	 = $this -> _createSort($_REQUEST['sort']);
		$sql  = "SELECT ".$this->fields." FROM ".$_SESSION['dbref']."_query_register QR \r\n";
		$sql .= $join;
		$sql .= "WHERE 1 \r\n";
		$sql .= $where;	
		$sql .= $group;	
		$sql .= $sort;
		
		//echo str_replace("\r\n", "<br />", $sql)."<br />";			
		$results = $this->get($sql);
            $finalResults = $this->getQueryActions( $results );
		$data = array("headers" =>$headers, "actionheaders" => $actionHeaders, "data" => $finalResults);
		return $data; 
	}
	*/

	
		function _generateWhereQuery( $keyValues )
		{
			$matches = $_REQUEST['match'];		
			$where = " ";
            $dateWhere = "";

		    if( isset($keyValues['from_query_date']) && isset($keyValues['to_query_date']))
            {	
            	
				$dateRange = "BETWEEN '".$keyValues['from_query_date']."' AND '".$keyValues['to_query_date']."' ";
            	$dateWhere = " OR QR.query_date ".$dateRange."\r\n";  
            }
			foreach($keyValues as $key => $value){
				if($value !==  ""){
					if( $key == "from_query_date" || $key == "to_query_date"){
						continue;
					} else if($value !== "all"){
							
							if(isset($matches[$key]))
							{
								if($matches[$key] == "all")
								{
									$where .= " AND QR.".$key." LIKE '%".$value."%' \r\n";
								} else if($matches[$key] == "any"){
									$where .= " AND QR.".$key." LIKE '".$value."' \r\n";
								} else if($matches[$key] == "exact"){
									$where .= " AND QR.".$key." = '".$value."' \r\n";
								}
							} else {
								if( is_array($value))
								{
									$value = implode(",",$value);
									if( $value == "all"){
										
									} else {
										$where .= " AND QR.".$key." IN (".$value.") \r\n";
									}
								} else {
								   $where .= " AND QR.".$key." = '".$value."' \r\n";
								}							
							}
						}
				}
			}
			
			return  $where." ".$dateWhere;
		}
		
		function joinQuery()
		{
			$innerJoins = "";
				$innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_types QT ON QT.id = QR.type \r\n";
				$innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_categories QC ON QC.id = QR.category \r\n";
				$innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_risk_types QRT ON QRT.id = QR.risk_type \r\n";
				$innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_level QRL ON QRL.id = QR.risk_level \r\n";
				$innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FIN ON FIN.id = QR.financial_exposure \r\n";
				$innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_status QS ON QS.id = QR.status \r\n";
                $innerJoins .= " LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id \r\n";
				$innerJoins .= " INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid \r\n";
			return $innerJoins;
		}
		
		function _createGroup( $groupField )
		{
			$group = "";
			if($groupField !== ""){
				$group = " GROUP BY QR.".rtrim($groupField,"_")."\r\n";
			}
			return $group;
		}
		
		function _createSort( $sortBy )
		{	
			$sort = "";
			if($sortBy == ""){
				$sort = "";
			} else {
				$sort = " ORDER BY ";
				
				foreach($sortBy as $index => $field)
				{
					$sort .= "QR.".ltrim($field, "__").",";
				}
				$sort = rtrim($sort,",");
			}
			return $sort;
		}

        function _getActionHeaders( $headers )
        {
            $queryFields = array(
                                "action_status"	  => "ASS.name AS action_status",
					            "action_owner"	  => "CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner",
                        );


			$headersObj  = new Naming();
			$headerNames = $headersObj -> getActionColums();
			$headerData  = array("query_item" => "Action Item");
			$fields 	 = "A.id AS action_item ,";
			foreach($headers as $key => $values)
			{
				if( array_key_exists($key ,$headerNames) )
				{
					$fields .=  (isset($queryFields[$key]) ? $queryFields[$key] : " A.".$key).",";
					$headerData[$key] = $headerNames[$key];
				}
			}
			$this->actionFields = rtrim($fields, ",");
			return $headerData;
        }

        function getQueryActions( $queries )
        {
            $queryActions = array();
            foreach($queries as $key => $query)
            {
                //echo $key;
                $actions = $this->getActions( $query['query_item']);
                $queryActions[$query['query_item']] = $query;

                if(!empty($actions))
                {
                    $queryActions[$query['query_item']]['actions'] = $actions;
                }
            }
            return $queryActions;
        }

        function getActions( $id )
        {
            $results = $this->get("SELECT ".$this->actionFields."
                                   FROM ".$_SESSION['dbref']."_actions A
                                   LEFT JOIN ".$_SESSION['dbref']."_action_status ASS ON A.status = ASS.id
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.action_owner 
                                   WHERE risk_id = $id");
            return $results;
        }


		function getDbFields()
		{
			$queryFields     = $this -> describe("query_register");
			$typeFields      = array("id", "name", "shortcode", "description");
			$riskTypeFields  = array("id", "name", "shortcode", "description");
			$categoryFields  = array("id", "name", "shortcode", "description");
			$finExsposureFields  = array("id", "exp_from", "exp_to", "name", "definition");
			$riskLevelFields = array("id", "name", "shortcode", "description");;
			$statusFields    = array("id", "name", "client_terminology");
			
			$fields = array(
							"query_register"  		=> array("fields" => $queryFields ,   	"fld" => "QR", 				  "table" => "query_register"),
							"query_type"  			=> array("fields" => "id", 		"fld" => "QT", 		  "table" => "types", 			     "join" => "QR.type = QT.id"),
							"query_category"  		=> array("fields" => "id", 	"fld" => "QC",     "table" => "categories" , 	     "join" => "QR.category = QC.id" ),
							"financial_exposure" 	=> array("fields" => "id", "fld" => "FIN", "table" => "financial_exposure" , "join" => "QR.financial_exposure = FIN.id" ),
							"risk_level" 			=> array("fields" => "id" ,	"fld" => "QRL" ,        "table" => "level" , 			 "join" => " QR.level = QRL.id"),
							"risk_type" 			=> array("fields" => "id" ,	"fld" => "QRT" , 		   "table" => "risk_types" ,    	 "join" => " QR.level = QRT.id"),
							"query_status"  		=> array("fields" => "id", 	"fld" => "QS", 	   "table" => "status" , 			 "join" => " QR.status = QS.id ")
							);
			return $fields;
		}

		
		function queryVsStatus()
		{
			$response = $this->get("SELECT count(QR.id) AS queries, QS.name AS status, QR.description 
								    FROM #_query_register QR
								    INNER JOIN #_status QS ON QS.id = QR.status
									WHERE ".Risk::getStatusSQLForWhere("QR")." 
									GROUP BY QR.status");
			return $response;			
		}
		
		function queryVsRiskType()
		{
			$results = $this->get("SELECT count(QR.id) AS queries, RT.name AS risk_type, QR.description 
									FROM #_query_register QR
									LEFT JOIN #_risk_types RT ON RT.id = QR.risk_type  
									WHERE ".Risk::getStatusSQLForWhere("QR")."
									GROUP BY QR.risk_type");
            $data   = array();
            $colors = array('FF0F00', 'FF6600', 'FF9E01', 'FCD202', 'F8FF01', 'B0DE09', '04D215', '0D8ECF');
            foreach($results as $r_index => $r_val)
            {
              if($r_val['risk_type'] == "")
              {
                $data[$r_index]['risk_type']        = "Unspecified";
                $data[$r_index]['description'] = "Unspecified";
                $data[$r_index]['queries']     = $r_val['queries'];
                $data[$r_index]['color']       = "#ababab";
              } else {
                $data[$r_index]['risk_type']   = $r_val['risk_type'];
                $data[$r_index]['queries']     = $r_val['queries'];
                $data[$r_index]['description'] = $r_val['description'];
                $data[$r_index]['color']       = "#".(isset($colors[$r_index]) ? $colors[$r_index] : "0D8ECF");
              }
            }
			return $data;			
		}

		function queryVsDepartment()
		{
			$defaultObj = new Defaults();
			$defaults   = $defaultObj -> getDefaults();
			$toDir 	  = true;
			$toSub	  = false;
			foreach($defaults as $index => $value)
			{
				if($value['name'] == "risk_is_to_directorate" && $value['value'] == 1)
				{
					$toDir = true;
				}
				
				if($value['name'] == "risk_is_to_subdirectorate" && $value['value'] == 1)
				{
					$toSub = true;
					$toDir = false;
				}			
			}
			/*
			if($toDir)
			{	
				$response = $this->get("SELECT count(QR.id) AS queries, D.dirtxt  AS department, QR.description 
										FROM #_query_register QR
										INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id
										INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
										WHERE ".Risk::getStatusSQLForWhere("QR")."
										GROUP BY QR.sub_id");
			} else if( $toSub ){	
				$response = $this->get("SELECT count(QR.id) AS queries, D.dirtxt  AS department, QR.description 
										FROM #_query_register QR
										INNER JOIN ".$_SESSION['dbref']."_dir DR ON DR.subid = QR.sub_id
										INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.dirid = DS.subdirid
										WHERE ".Risk::getStatusSQLForWhere("QR")."
										GROUP BY QR.sub_id");
			}
		    */
            $results = $this -> get("SELECT COUNT(QR.id) AS queries, DS.subtxt AS department
                                     FROM #_query_register QR 
                                     INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id
                                     INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                     WHERE ".Risk::getStatusSQLForWhere("QR")." GROUP BY QR.sub_id
                                   ");	
            $data   = array();
            foreach($results as $r_index => $result)
            {
               $data[$r_index]['queries']    = $result['queries'];
               $data[$r_index]['department'] = str_replace(" ", "\r\n", $result['department']);
            }				
			return $data;			
		}		
		
		function _residualVsInherent()
		{
			$response = $this -> get("SELECT id, inherent_risk_rating, residual_risk FROM ".$_SESSION['dbref']."_risk_register");
			return $response;
		}
		
		function _getRiskType() 
		{
			$response = $this -> get( "SELECT 
										RT.name, 
										SUM( inherent_risk_exposure ) AS sumInherent,
										SUM( residual_risk ) AS sumResidual,
										AVG( inherent_risk_exposure ) AS avgInherent,
										AVG( residual_risk ) AS avgResidual
										FROM ".$_SESSION['dbref']."_risk_register RR 
										INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type 
										WHERE 1 GROUP BY RR.type 									
										" );
			
			return $response;
		}
		
		
		function _getRiskCategory() 
		{
		
			$response = $this -> get( "SELECT 
										RC.name , 
										SUM( inherent_risk_exposure ) AS sumInherent ,
										SUM( residual_risk ) AS sumResidual,
										AVG( inherent_risk_exposure ) AS avgInherent,
										AVG( residual_risk )	AS avgResidual
										FROM ".$_SESSION['dbref']."_risk_register RR
										INNER JOIN ".$_SESSION['dbref']."_categories RC ON RR.category = RC.id
										WHERE 1 GROUP By RR.category																													
									" );
			return $response;
		}		
		
		function _getRiskPerson() 
		{
			$response = $this -> get( "SELECT 
										TK.tkname, TK.tksurname , 
										SUM( inherent_risk_exposure ) AS sumInherent,
										SUM( residual_risk ) AS sumResidual,
										AVG( inherent_risk_exposure ) AS avgInherent,
										AVG( residual_risk ) AS avgResidual	
										FROM ".$_SESSION['dbref']."_risk_register RR
										LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON RR.risk_owner = TK.tkid																													
										WHERE 1 GROUP BY RR.risk_owner
									" );
			return $response;
		}				
		

    function updateQuickReport( $id )
    {
        $name 		 = $_REQUEST['report_name'];
        $description = $_REQUEST['report_description']; 
        //unset($_REQUEST['report_name']);
        unset($_REQUEST['save_quick_report']);
        $updatedata = array("name" =>$name, "description" => $description,"report" => base64_encode(serialize($_REQUEST)), "insertuser" => $_SESSION['tid'] );
        if($this->update("quick_report", $updatedata, "id=$id"))
        {
        	echo "<div class='ui-widget ui-icon-check ui-state-ok' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Report updated successfully</div>";
        } else {
            echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Error updating the report</div>";
        }
    }

    function saveQuickReport()
    {
        $name 		 = $_REQUEST['report_name'];
        $description = $_REQUEST['report_description']; 
        //unset($_REQUEST['report_name']);
        unset($_REQUEST['save_quick_report']);
        $updatedata = array("name"           =>$name, 
                            "description"    => $description, 
                            "report"         => base64_encode(serialize($_REQUEST)), 
                            "insertuser"     => $_SESSION['tid']
                           );
        if($this->insert("quick_report", $updatedata))
        {
        	echo "<div class='ui-widget ui-icon-check ui-state-ok' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Report saved successfully</div>";
        } else {
            echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Error saving the report</div>";
        }
    }

    function getQuickReports( $start, $limit )
    {
        $results = $this->get("SELECT id, name, description, insertuser FROM ".$_SESSION['dbref']."_quick_report WHERE status & 1 = 1 ORDER BY id DESC LIMIT $start, $limit");
        return $results;
    }

    function totalQuickReports()
    {
        $results = $this->getRow("SELECT COUNT(*) AS total FROM ".$_SESSION['dbref']."_quick_report WHERE status & 1 = 1 ");
        return $results['total'];    	 	
    }
    
    function getQuickData( $id )
    {
        $results = $this->getRow("SELECT report FROM ".$_SESSION['dbref']."_quick_report WHERE id = $id");
        return $results['report'];
    }
      
    
    function deleteQuick( $id )
    {
    	$response = $this->update("quick_report", array("status" => 0), "id=$id");    	
    	return $response;
    }
    
    function generateFixedReport()
    {
    	$response  = $this->get(" 
							SELECT 
							DISTINCT(RR.id) AS query_item,
							RR.description AS query_description,
							RR.background AS query_background,
							RR.monetary_implication,
							RR.risk_detail,
							RRT.name AS risk_type,
							RR.finding, 
							RR.recommendation,
							RR.client_response , 
							RR.query_reference,
							RR.query_date,
							RR.query_deadline_date,
							RR.internal_control_deficiency,
							RR.auditor_conclusion,
							FE.name AS financial_exposure,
							RL.name AS risk_level,
							RC.name as query_category, 
							RC.description as cat_descr,
							RT.name as query_type,
							RT.shortcode as type_code, 
							D.dirtxt  AS query_owner,
							RR.sub_id,
							RS.name AS query_status,
							RS.color AS query_status_color,
							CONCAT(FC.start_date, ' - ', FC.end_date) AS financial_year
							FROM ".$_SESSION['dbref']."_query_register RR 
							LEFT JOIN ".$_SESSION['dbref']."_financial_years FC ON FC.id = RR.financial_year
							LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = RR.risk_level	
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE ".Risk::getStatusSQLForWhere("RR")."
							");
    	return $response;
    }
    
    function prepareReport( $data )
    {
    	
    }
    
    function displayHtmlReport( $data )
    {
    	   ob_start();   
	     echo '<link rel="stylesheet" href="/assist.css" type="text/css" />';
    		echo "<table>";
    		echo "<tr>";
    		$headersCount = count($data['headers']);
    		$additionals = array("finding", "recommendation", "client_response", "auditor_conclusion", "internal_control_deficiency");
    		
    		
    		foreach($this -> headers as $index => $head)
    		{
    			echo "<th>".$head."</th>";	    			
    		}
    		/*if(isset($data['actionheaders']) && !empty($data['actionheaders']))
    		{
        	  foreach( $data['actionheaders'] as $index => $head)
    		  {
    			echo "<th>".$head."</th>";	    			
    		  }
    		}*/    		
    		echo "</tr>";
          if(isset($data['data']) && !empty($data['data']))
          {
         		foreach($data['data'] as $index => $dataArr)
         		{
         			echo "<tr>";
				     foreach( $dataArr as $key => $val)
				     {
					     if(!is_array($val)){
						     if( in_array($key, $additionals))
						     {
							     echo "<td>".str_replace("__", ", ", $val)."</td>";	
						     } else {
							     echo "<td>".$val."</td>";
						     }	
					     }				
				     }
				     /*if(isset($dataArr['actions']))
				     {
					     echo  "<tr>";
					     foreach($dataArr as $index => $value){
			                 if(is_array($value) && !empty($value)){
			                     foreach($value as $k => $v){
			                         echo "<tr>";
			                         echo "<td colspan='".$headersCount."'></td>";
			                         foreach($v as $i => $val){
			                           echo  "<td>".($i == "progress" ? $val."%" : $val)."</td>";
			                         }
			                         echo "</tr>";
			                     }
			                 }
					     }
					     echo  "</tr>";
				     }*/
				     echo "</tr>";
         		}
          } else {
             echo "<tr><td colspan='".$headersCount."'>No records found for the selected filters</td></tr>";
          }
    		echo "<table>";
    	ob_end_flush();
    }
    
    function displayCsvReport( $report )
    {
 		$headers       = $report['headers'];
	    $headersCount  = count($headers);
		$data 	       = $report['data'];
	    $actionHeads   = $report['actionheaders'];   	
	    $string   = $_SESSION['cn']."\r\n";
		$string  .=  $_REQUEST['report_title']."\r\n";
		$string .= "\r\n";
		foreach($headers as $key => $heads){
			$string .= "".$heads.", ";
		}
	    foreach($actionHeads as $k => $head){
			$string .= $head.",";
		}
		foreach($data as $keys => $valueArr){
			$string .= "\r\n";
			foreach($valueArr as $index => $value){
				if(is_array($value)){} else{
					$string .= str_replace(",", "", $value).",";
				}
			}
	        if(isset($valueArr['actions'])){
			$string .= "\r\n";
			foreach($valueArr as $index => $value){
	            if(is_array($value) && !empty($value)){
	                foreach($value as $k => $v){
	                   $string .= "\r\n";
	                   		for( $x = 0; $x < $headersCount; $x++){
	                    		$string .= ","; //<td colspan='".$headersCount."'></td>";
	                   		}
	                   	  	foreach($v as $i => $val){
	                      		$string .= str_replace(",", " ", ($i == "progress" ? $val."%" : $val ) ).",";
	                    	}
	                    $string .= "";
	                }
	            }
			}
			$string .= "";
	        } else {
	        }
			$string .= "";
		}
		$string .= "";
	    $filename =  "report_".date("YmdHis").".csv";
	    $fp       =  fopen($filename, "w+");
	    fwrite($fp, $string);
	    fclose($fp);
	    header('Content-Type: application/csv'); 
	    header("Content-length: " . filesize($filename)); 
	    header('Content-Disposition: attachment; filename="' . $filename . '"'); 
	    readfile( $filename );
	    unlink($filename);
	    exit();	
    }
    
     public static function completedOnDeadline($deadlineDate, $actionOn)
     {
          $_deadlineDate = date("dFy", strtotime($deadlineDate));
          $_actionOn     = date("dFy", strtotime($actionOn));
          if($_deadlineDate == $_actionOn)
          {
            return TRUE;
          } else {
            return FALSE;
          }
     }
     
     public static function completedBeforeLegislationDeadline($legislationDeadline, $actionOn)
     {
        if(strtotime($legislationDeadline) > strtotime($actionOn))
        {
           return TRUE; 
        } else {
           return FALSE;
        }
     }
     
     public static function completedAfterLegislationDeadline($legislationDeadline, $actionOn)
     {
        if(strtotime($legislationDeadline." 23:59:59") >= strtotime($actionOn))
        {
          return FALSE; 
        } else {
           return TRUE;
        }
     }     
     
     public static function notCompletedAndOverdue($legislationDeadline, $actionOn)
     {
          if(strtotime($legislationDeadline." 23:59:59") > strtotime(date("dFy")))
          {
             return FALSE;
          } else {
             return TRUE;  
          }
     }	
	
     public function getActionProgressStatus($financial_year = "")
     {
          $actionObj           = new RiskAction("", "", "", "", "", "", "", "", "");
          $usersObj            = new UserAccess("", "", "", "", "", "", "", "", "");
          $users               = $usersObj -> getUsers();
          $usersActionProgress = array();
          $optionSql           = "";
          $userList            = array();
          foreach($users as $uIndex => $user)
          {
             //store actions completed on the deadline date for this user
             $actionCompletedOnDeadline         = 0;
             //store action completed before the deadline date for this user
             $actionCompletedBeforeDeadline     = 0;
             //store action completed after deadline date for this user
             $actionCompletedAfterDeadline      = 0;
             //store actions not completed and are overdue
             $actionsNotCompletedAndOverdue     = 0;
             //store actions not completed and not overdue
             $actionsNotCompletedAndNotOverdude = 0;
             $optionSql                         = " AND L.financial_year = '".$financial_year."'";
             $optionSql                         = " AND A.action_owner = '".$user['tkid']."' ";
             $actions                           = $actionObj -> filterActions("A.*", $optionSql);  
             $userList[$user['tkid']]           = $user['user'];             
             if(!empty($actions))
             {
               foreach($actions as $aIndex => $action)
               {
                  if($action['status'] == 3 && $action['progress'] == "100")
                  {
                       if(self::completedOnDeadline($action['deadline'], $action['date_completed']))
                       {
                         $actionCompletedOnDeadline += 1;
                       } else if(self::completedBeforeLegislationDeadline($action['deadline'], $action['action_on'])){
                         $actionCompletedBeforeDeadline += 1; 
                       } else if(self::completedAfterLegislationDeadline($action['deadline'], $action['action_on'])){
                         $actionCompletedAfterDeadline += 1;
                       }
                  } else {
                      if(self::notCompletedAndOverdue($action['deadline'], $action['action_on']))
                      {
                         $actionsNotCompletedAndOverdue += 1;
                      } else {
                         $actionsNotCompletedAndNotOverdude += 1;
                      }
                  }
               }
              $usersActionProgress[$user['tkid']] = array(
                                            "completedBeforeDeadline"   => $actionCompletedBeforeDeadline,
                                            "completedOnDeadlineDate"   => $actionCompletedOnDeadline,
                                            "completedAfterDeadline"    => $actionCompletedAfterDeadline,
                                            "notCompletedAndOverdue"    => $actionsNotCompletedAndOverdue,
                                            "notCompletedAndNotOverdue" => $actionsNotCompletedAndNotOverdude             
                                           );                              
             }            
          }
          $data = array("users" => $userList, "progressStatus" => $usersActionProgress);          
          return $data;
     }         
}
?>
