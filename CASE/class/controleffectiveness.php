<?php
/**	
	* @package 	: Control Effectiveness
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist 
**/
class ControlEffectiveness extends DBConnect
{
	protected $shortcode;
	
	protected $effectiveness;
	
	protected $qualification_criteria;
	
	protected $rating;
	
	protected $color;
	
	function __construct( $shortcode, $effectiveness, $qualification_criteria, $rating,  $color )
	{
		$this -> shortcode				 = trim($shortcode);
		$this -> effectiveness 			 = trim($effectiveness);
		$this -> qualification_criteria  = trim($qualification_criteria);
		$this -> rating  				 = trim($rating);
		$this -> color 		 			 = trim($color);						
		parent::__construct();
	}
	
	function saveControlEffectiveness()
	{
		$insert_data = array( 
						"shortcode" 			  => $this -> shortcode,
						"effectiveness"   		  => $this -> effectiveness,
						"qualification_criteria"  => $this -> qualification_criteria,
						"rating"  				  => $this -> rating,
						"color"       			  => $this -> color,
						);
		$response = $this -> insert( "control_effectiveness" , $insert_data );
		echo $this -> insertedId();		
	}		
	
	function updateControlEffectiveness( $id )
	{
		$insert_data = array( 
						"shortcode" 			  => $this -> shortcode,
						"effectiveness"   		  => $this -> effectiveness,
						"qualification_criteria"  => $this -> qualification_criteria,
						"rating"  				  => $this -> rating,
						"color"       			  => $this -> color,
						);
		$response = $this -> update( "control_effectiveness" , $insert_data, "id=$id" );
		return $response;	
	}			
				
	function getControlEffectiveness()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_control_effectiveness WHERE 1" );
		return $response;
	}
			
	function getAControlEffectiveness( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_control_effectiveness WHERE id = $id" );
		return $response;
	}
	
	function getControlRating( $id )
	{
		$response = $this -> get( "SELECT rating
								   FROM ".$_SESSION['dbref']."_control_effectiveness 
								   WHERE id = '".$id."' " );
		foreach($response as $row ) {
			$rating 	= $row['rating'];
		} 
		
		echo json_encode( array( "rating" => $rating ) );
	}	
	

	function updateControlEffectivenessStatus( $id, $status )
	{
		$updatedata = array(
						'active' => $status
		);
		$response = $this -> update( 'control_effectiveness', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateControlEffectiveness( $id )
	{
		$update_data = array(
						'active' => "1"
		);
		$response = $this -> update( 'control_effectiveness', $update_data, "id=$id" );
		echo $response;
	}
}
?>