<?php
class UserNotification extends DBConnect
{

	protected $id;
	
	protected $notification;
	
	protected $risk_id;
	
	protected $action_id;
	
	protected $recieve_email;
	
	protected $recieve_when;
	
	protected $recieve_what;
	
	protected $user_id;
	
	protected $recieve_day;
	
	protected $active;
	
	protected $inserdate;
	
	function __construct() 
	{
	    parent::__construct();
		$this -> notification  = (isset($_POST['notification']) ? $_POST['notification'] : "");
		$this -> risk_id	   = (isset($_POST['risk_id']) ? $_POST['risk_id'] : "");
		$this -> action_id	   = (isset($_POST['action_id']) ? $_POST['action_id'] : "");
		$this -> recieve_email = (isset($_POST['recieve_email']) ? $_POST['recieve_email'] : "");
		$this -> recieve_what  = (isset($_POST['recieve_what']) ? $_POST['recieve_what'] : "");
		$this -> recieve_when  = (isset($_POST['recieve_when']) ? $_POST['recieve_when'] : "");		
		$this -> user_id	   = (isset($_SESSION['tid']) ? $_SESSION['tid'] : "");	
		$this -> recieve_day   = (isset($_POST['recieve_day']) ? $_POST['recieve_day'] : "");					
	}
	
	function getNotifications()
	{
		$response = $this -> get("SELECT * FROM ".$_SESSION['dbref']."_user_notifications 
		                          WHERE user_id = ".$this -> user_id." AND active = 1");
		                          
		$data               = array();
		$notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
		                            'due_this_week'            => 'Actions due this week',
		                            'overdue_or_due_this_week' => 'Actions overdue or due this week',
		                            'due_today'                => 'Actions due today',
		                            'overdue_or_due_today'     =>  'Actions overdue or due today'
		                            );
		$recieve_day        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');
		if(!empty($response))
		{
		   foreach($response as $index => $result)
		   {
		      $data[$result['id']]['recieve']       = $result['recieve_email'];
		      $data[$result['id']]['what']          = $result['recieve_what'];
		      $data[$result['id']]['when']          = $result['recieve_when'];
		      $data[$result['id']]['day']           = $result['recieve_day'];
		      $data[$result['id']]['active']        = $result['active'];
		      $data[$result['id']]['id']            = $result['id'];
		      $data[$result['id']]['recieve_email'] = ($result['recieve_email'] == 0 ? "No" : "Yes");
		      $data[$result['id']]['recieve_what']  = $notification_types[$result['recieve_what']];
		      if($result['recieve_when'] == "weekly")
		      {
		        $when_str   = "Weekly";
		        if(isset($recieve_day[$result['recieve_day']]))
		        {
		            $when_str  .= "<em> on ".$recieve_day[$result['recieve_day']]."</em>";    
		        }
		        $data[$result['id']]['recieve_when'] = $when_str;
		      } else {
		        $data[$result['id']]['recieve_when']  = "Daily";
		      }
		      
		      $data[$result['id']]['status']        = ($result['active'] == 0 ? "Inactive" : "Active");
		   }		
		}                          
		return $data;		
	}
	
	function getNotitication($id)
	{
		$result = $this -> getRow("SELECT * FROM ".$_SESSION['dbref']."_user_notifications 
		                             WHERE id = ".$id."
		                          ");
		return $result;           
	}
	
	function saveNotifications()
	{
		$insert_data = array(
				'notification' 		=> $this -> notification,
				'risk_id' 			=> $this -> risk_id,
				'action_id' 		=> $this -> action_id,
				'recieve_email' 	=> $this -> recieve_email,
				'recieve_what' 		=> $this -> recieve_what,
				'recieve_when' 		=> $this -> recieve_when,				
				'user_id'			=> $this -> user_id,
				'recieve_day'		=> $this -> recieve_day,
				"insertuser"  		=> $_SESSION['tid'],				
		);
		$response = $this -> insert( 'user_notifications', $insert_data );
		return $this -> insertedId();
	}
	
		
	function getAProfile( $id )
	{
		$results = $this -> getRow("SELECT * FROM ".$_SESSION['dbref']."_user_notifications WHERE id = '".$id."'");
		return $results;
	}
	
	function updateNotification($id)
	{
	    $updatedata         = array();
	    $changes            = array();
	    $notification       = $this -> getNotitication($id);
		$notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
		                            'due_this_week'            => 'Actions due this week',
		                            'overdue_or_due_this_week' => 'Actions overdue or due this week',
		                            'due_today'                => 'Actions due today',
		                            'overdue_or_due_today'     =>  'Actions overdue or due today'
		                            );
		$recieve_day        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');
	    if((int)$notification['recieve_email'] != (int)$this -> recieve_email)
	    {
	        $to                 = ((int)$this -> recieve_email == 0 ? "No" : "Yes");
	        $from               = ((int)$notification['recieve_email'] == 0 ? "No" : "Yes");
	        $changes['recieve'] = "Recieve email changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_email'] = $this -> recieve_email;
	    }
	    if($notification['recieve_when'] != $this -> recieve_when)
	    {
            if($notification['recieve_when'] == "weekly")
            {
               $when_str   = "Weekly";
               if(isset($recieve_day[$notification['recieve_day']]))
               {
                  $when_str  .= "<em> on ".$recieve_day[$notification['recieve_day']]."</em>";    
               }
               $from = $when_str;
            } else {
               $from  = "Daily";
            }
            if($this -> recieve_when == "weekly")
            {
               $when_str   = "Weekly";
               if(isset($recieve_day[$this -> recieve_day]))
               {
                  $when_str  .= "<em> on ".$recieve_day[$this -> recieve_day]."</em>";    
               }
               $to = $when_str;
            } else {
               $to  = "Daily";
            }
	        $changes['recieve_when']    = "Recieve when changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_when'] = $this -> recieve_when;
	    }	    
	    if($notification['recieve_day'] != $this -> recieve_day)
	    {
            $from = $to = "";
            if(isset($recieve_day[$notification['recieve_day']]))
            {
              $from  = $recieve_day[$notification['recieve_day']];    
            }
            if(isset($recieve_day[$this -> recieve_day]))
            {
              $to  = $recieve_day[$this -> recieve_day];    
            }            
	        $changes['recieve_day']    = "Recieve day changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_day'] = $this -> recieve_day;
	    }
	    if($notification['recieve_what'] != $this -> recieve_what)
	    {
	        $to                         = $notification_types[$this -> recieve_what];
	        $from                       = $notification_types[$notification['recieve_what']];
	        $changes['recieve_what']    = "Recieve what changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_what'] = $this -> recieve_what;
	    }   
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['notification_id'] = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['ref_']           = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insertuser']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $this -> insert('user_notifications_logs', $updates);
	       $res += $this -> insertedId(); 
	    }

		$res = $this -> update('user_notifications', $updatedata, "id=$id" );
		return $res;
	}
	
	function deleteNotification( $id )
	{
	    $updatedata         = array();
	    $changes            = array();
	    $notification       = $this -> getNotitication($id);	    
	    if(isset($_POST['active']))
	    {
	        if($_POST['active'] != $notification['active'])
	        {
	            if($_POST['active'] == 0)
	            {
	                $changes['active_']    = "Notification setting deleted \r\n\n";
	                $updatedata['active']  = $_POST['active'];
	            }
	        }	    
	    } 
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['notification_id'] = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['ref_']           = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insertuser']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $this -> insert('user_notifications_logs', $updates);
	       $res += $this -> insertedId(); 
	    }
		$res = $this -> update('user_notifications', $updatedata, "id=$id" );
		return $res;
	}
	
}
?>
