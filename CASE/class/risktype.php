<?php
/**
	* Risk types for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class RiskType extends DBConnect
{
	
	protected $shortcode;
	
	protected $type;
	
	protected $description;
	
	protected $name;
	
	const ACTIVE = 1;
	const DELETED = 2;
	const INACTIVE = 0;
	
	
	function __construct( $shortcode, $name, $description, $type)
	{
		$this -> shortcode 	 = trim($shortcode);
		$this -> name 		 = trim($name);
		$this -> description = trim($description);
		$this -> type        = trim($type);		
		parent::__construct();
	}
	
	function saveType($echo=true)
	{
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"insertuser"  => $_SESSION['tid'],
						);
		$response = $this -> insert( "risk_types" , $insert_data );
		if($echo) { echo $this -> insertedId();		}
	}
	
	function getType()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_risk_types WHERE active & 2 <> 2" );
		return $response ;		
	}	
	
	function getARiskType( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_risk_types WHERE id = '".$id."'" );
		return $response ;		
	}	
	/**
		Get used risk types , so they cannot be deleted
	**/
	function getUsedTypes()
	{	
		$response = $this -> get("SELECT type FROM ".$_SESSION['dbref']."_risk_register GROUP BY type");
		return $response;
	}
	/**
		Fetches all active risk types
	**/
	function getActiveRiskType()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_risk_types WHERE active = 1" );
		return $response ;		
	}		
			
	function updateType( $id )
	{
		$insertdata = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "risk_types");						
		$response = $this -> update( "risk_types", $insertdata , "id=$id");
		return $response;
	}
	
	function changeTypeStatus( $id, $status )
	{
		$updatedata = array(
						'active' 	  => $status,
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "risk_types");			
		$response = $this -> update( 'risk_types', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateType( $id )
	{
		$update_data = array(
						'active' 	  => "1",
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "risk_types");			
		$response = $this -> update( 'risk_types', $update_data, "id=$id" );
		echo $response;
	}

	function getReportList()
	{
		$response = $this -> get( "SELECT L.id, L.name
								   FROM ".$_SESSION['dbref']."_risk_types L 
								   LEFT JOIN ".$_SESSION['dbref']."_query_register Q
								   ON Q.risk_type = L.id WHERE L.active & 2 <> 2
								 " );
		return $response;
	}

	

}
?>