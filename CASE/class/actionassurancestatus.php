<?php
class ActionAssuranceStatus extends DBConnect
{
    
    function __construct()
    {
        parent::__construct();
    }    
    
    function getAll($options = array())
    {
        $results = $this -> get("SELECT * FROM #_action_assurance_status WHERE status & 2 <> 2");
        return $results;    
    }
    
    function getAssuranceStatus($option_sql = "")
    {
       $result = $this -> getRow("SELECT * FROM #_action_assurance_status WHERE 1 $option_sql");
       return $result;
    }
    
    function updateAssurance($id, $data)
    {
        $actionstatus = $this -> getAssuranceStatus(" AND id = '".$id."' ");
        $_changes     = array();
        if(!empty($actionstatus))
        {
            foreach($actionstatus as $field => $val)
            {
               if(isset($data[$field]))
               {
                  if($data[$field] !== $val)
                  {
                    $_changes[$field] = array('from' => $val, 'to' => $data[$field]);
                  }
               }
            }            
        }
        if(!empty($_changes))
        {
           $changes['ref']        = "Action Assurance Status Ref #".$id;
           $changes['user']       = $_SESSION['tkn'];
           $changes               = array_merge($changes, $_changes);
           $updates['changes']    = base64_encode(serialize($changes));
           $updates['ref_id']     = $id;
           $updates['insertuser'] = $_SESSION['tid'];
           $res = $this -> insert("action_assurance_status_logs", $updates);
        }    
        
        $res = $this -> updateData("action_assurance_status", $data, array('id' => $id));
        return $this -> affected_rows();
    }
    
    function save($data)
    {
       $data['insertuser'] = $_SESSION['tid'];
       $this -> insert("action_assurance_status", $data);
       return $this -> insertedId();
    }
}



