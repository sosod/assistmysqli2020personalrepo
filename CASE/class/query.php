<?php

class QUERY extends ASSIST_MODULE_HELPER {
	//1 => Created in NEW, Not yet ACTIVATED
	//8 => Created in IMPORT, Not yet ACTIVATED
	//1 + 4 = 5 => Created in NEW and ACTIVATED to MANAGE
	//1 + 4 + 8 = 13 => Created in IMPORT and ACTIVATED to MANAGE
	//2 => Created in NEW and DELETED before ACTIVATED
	//2 + 8 = 10 => Created in IMPORT and DELETED before ACTIVATED
	//2 + 4 = 6 => Created in NEW and DELETED after ACTIVATED
	//2 + 4 + 8 = 14 => Created in IMPORT and DELETED after ACTIVATED
	const ACTIVE 	= 1; //In Use
	const DELETED   = 2; //Deleted
	const ACTIVATED = 4; //Activated from NEW into MANAGE
	const IMPORTED 	= 8; //Created using the Import function
	
	const REFTAG    = "Q";

	protected $object_fields = array(
		'id'	=> array(
			'type'=>"REF",
			'default'=>"",
			'required'=>true,
			'db'=>"id",
		),
		'financial_year'	=> array(
			'type'=>"LIST",
			'default'=>"0",
			'required'=>false,
			'db'=>"financial_year",
		),
		'query_type'	=> array(
			'type'=>"LIST",
			'default'=>"0",
			'required'=>false,
			'db'=>"type",
		),
		'query_source'	=> array(
			'type'=>"LIST",
			'default'=>"0",
			'required'=>false,
			'db'=>"query_source",
		),
		'query_category'	=> array(
			'type'=>"LIST",
			'default'=>"0",
			'required'=>false,
			'db'=>"category",
		),
		'query_description'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>true,
			'db'=>"description",
		),
		'query_background'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"background",
		),
		'financial_exposure'	=> array(
			'type'=>"LIST",
			'default'=>"0",
			'required'=>false,
			'db'=>"financial_exposure",
		),
		'monetary_implication'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"monetary_implication",
		),
		'risk_detail'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"risk_detail",
		),
		'risk_type'	=> array(
			'type'=>"LIST",
			'default'=>"0",
			'required'=>false,
			'db'=>"risk_type",
		),
		'finding'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"finding",
		),
		'inherent_qry_exposure'	=> array(
			'type'=>"IGNORE",
			'default'=>"",
			'required'=>false,
			'db'=>"inherent_qry_exposure",
		),
		'inherent_qry_rating'	=> array(
			'type'=>"IGNORE",
			'default'=>"",
			'required'=>false,
			'db'=>"inherent_qry_rating",
		),
		'recommendation'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"recommendation",
		),
		'client_response'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"client_response",
		),
		'internal_control_deficiency'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"internal_control_deficiency",
		),
		'auditor_conclusion'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"auditor_conclusion",
		),
		'query_reference'	=> array(
			'type'=>"VC",
			'length'=>20,
			'default'=>"",
			'required'=>false,
			'db'=>"query_reference",
		),
		'query_deadline_date'	=> array(
			'type'=>"DATE",
			'default'=>"",
			'required'=>true,
			'db'=>"query_deadline_date",
		),
		'remind_on'	=> array(
			'type'=>"DATE",
			'default'=>"0000-00-00",
			'required'=>false,
			'db'=>"remind_on",
		),
		'residual_qry_exposure'	=> array(
			'type'=>"IGNORE",
			'default'=>"",
			'required'=>false,
			'db'=>"residual_qry_exposure",
		),
		'risk_owner'	=> array(
			'type'=>"IGNORE",
			'default'=>"",
			'required'=>false,
			'db'=>"risk_owner",
		),
		'query_owner'	=> array(
			'type'=>"LIST",
			'default'=>"",
			'header_name'=>"query_owner",
			'required'=>true,
			'db'=>"sub_id",
		),
		'risk_level'	=> array(
			'type'=>"LIST",
			'default'=>"",
			'required'=>false,
			'db'=>"risk_level",
		),
		'query_date'	=> array(
			'type'=>"DATE",
			'default'=>"",
			'required'=>true,
			'db'=>"query_date",
		),
		'status'	=> array(
			'type'=>"LIST",
			'default'=>"1",
			'required'=>true,
			'db'=>"status",
		),
		'udf'	=> array(
			'type'=>"IGNORE",
			'default'=>"",
			'required'=>false,
			'db'=>"udf",
		),
		'attachment'	=> array(
			'type'=>"ATTACH",
			'default'=>"",
			'required'=>false,
			'db'=>"attachment",
		),
		'active'	=> array(
			'type'=>"STATUS",
			'default'=>"8",
			'required'=>true,
			'db'=>"active",
		),
		'insertdate'	=> array(
			'type'=>"TIMESTAMP",
			'default'=>"",
			'required'=>true,
			'db'=>"insertdate",
		),
		'insertuser'	=> array(
			'type'=>"USER",
			'default'=>"",
			'required'=>true,
			'db'=>"insertuser",
		),
	);

	
	protected $action_fields = array(
		
		'risk_id'	=> array(
			'type'=>"OBJECT",
			'default'=>"",
			'required'=>true,
			'db'=>"risk_id",
		),
		'query_action'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>true,
			'db'=>"action",
		),
		'action_owner'	=> array(
			'type'=>"LIST",
			'default'=>"",
			'required'=>true,
			'db'=>"action_owner",
		),
		'deliverable'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"deliverable",
		),
		'timescale'	=> array(
			'type'=>"TEXT",
			'default'=>"",
			'required'=>false,
			'db'=>"timescale",
		),
		'deadline'	=> array(
			'type'=>"DATE",
			'default'=>"",
			'required'=>true,
			'db'=>"deadline",
		),
		'remindon'	=> array(
			'type'=>"DATE",
			'default'=>"",
			'required'=>false,
			'db'=>"remindon",
		),
		
		'query_description'=>array('type'=>"IGNORE"),
		'query_reference'=>array('type'=>"IGNORE"),
	);

	public function __construct() {
		parent::__construct();
	}

	/* GET HEADER NAMES */
	function getAllHeaderNames() {
		$sql = "SELECT name, IF(client_terminology='',ignite_terminology, client_terminology) as heading FROM ".$this->getDBRef()."_header_names";
		$names = $this->mysql_fetch_value_by_id($sql,"name","heading");
		
		return $names;
	}
	
	function getObjectFields($key="OBJECT") { 
		if($key=="OBJECT") {
			return $this->object_fields; 
		} else {
			return $this->action_fields; 
		}
	}


	
}

?>