<?php
$scripts = array( 'menu.js', 'setup_logs.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");

//error_reporting(-1);

$nameObj = new Naming();
$names = array_merge($nameObj->getReportingNaming("query"),$nameObj->getReportingNaming("action"));
//echo "<pre>"; print_r($names); echo "</pre>";
?>
<script language="javascript">
	$(function(){
		$(".setup_defaults").click(function() {
			document.location.href = this.id+".php";
			return false;		  
		});
		$("table#link_to_page").find("th").css({"text-align":"left"})
	});
</script>
<table border="1" width="60%" cellpadding="5" cellspacing="0" id="link_to_page">
	<tr>
    <tr>
    	<th>Menu and Headings:</th>
        <td>Define the Menu headings to be displayed in the module</td>
        <td><input type="button" name="menu_heading" id="menu_heading" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Naming Convention:</th>
        <td>Define the Naming convention of the fields for the module </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php echo $names['financial_year']; ?>:</th>
        <td>Configure the <?php echo $names['financial_year']."s"; ?></td>
        <td><input type="button" name="financial_year" id="financial_year" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php echo $names['query_status']; ?>:</th>
        <td>Configure the <?php echo $names['query_status']; ?></td>
        <td><input type="button" name="risk_status" id="risk_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php echo $names['action_status']; ?>:</th>
        <td>Configure the <?php echo $names['action_status']; ?></td>
        <td><input type="button" name="action_status" id="action_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php echo $names['query_type']; ?>:</th>
        <td>Configure the <?php echo $names['query_type']; ?></td>
        <td><input type="button" name="query_type" id="query_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php echo $names['query_source']; ?>:</th>
        <td>Configure the <?php echo $names['query_source']; ?></td>
        <td><input type="button" name="query_source" id="query_source" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th><?php echo $names['query_category']; ?>:</th>
        <td>Configure the <?php echo $names['query_category']; ?></td>
        <td><input type="button" name="risk_categories" id="risk_categories" value="Configure" class="setup_defaults"  /></td>
    </tr>	
    <tr>
    	<th><?php echo $names['risk_type']; ?>:</th>
        <td>Configure the <?php echo $names['risk_type']; ?></td>
        <td><input type="button" name="risk_type" id="risk_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th><?php echo $names['risk_level']; ?>:</th>
        <td>Configure the <?php echo $names['risk_level']; ?></td>
        <td><input type="button" name="risk_level" id="risk_level" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th><?php echo $names['financial_exposure']; ?>:</th>
        <td>Configure the <?php echo $names['financial_exposure']; ?></td>
        <td><input type="button" name="financial_exposure" id="financial_exposure" value="Configure" class="setup_defaults"  /></td>
    </tr>  
   <tr>
    	<th>Action Assurance Status:</th>
        <td>Define the Action Assurance Statuses</td>
        <td><input type="button" name="action_assurance_status" id="action_assurance_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Query Assurance Status:</th>
        <td>Define the Query Assurance Status</td>
        <td>
           <input type="button" name="query_assurance_status" id="query_assurance_status" value="Configure" class="setup_defaults"  />
        </td>
    </tr>         
    <tr>
    	<th><?php echo $names['query_owner']; ?>:</th>
        <td>Configure the <?php echo $names['query_owner']; ?> Structure and responsibilities </td>
        <td><input type="button" name="setup_dir" id="setup_dir" value="Configure" class="setup_defaults"  /></td>
    </tr>	
<!--    <tr>
    	<th>Module Defaults:</th>
        <td>Configure the Module Defaults</td>
        <td><input type="button" name="default_setting" id="default_setting" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
    <tr>
    	<th>Glossary:</th>
        <td>Configure the data to be shown on the Glossary</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>List Columns:</th>
        <td>Configure which columns display on the Manage pages</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Data Report:</th>
        <td>View the data usage for this module</td>
        <td><input type="button" name="datareport" id="datareport" value="View" class="setup_defaults"  /></td>
    </tr>   
	
</table>
