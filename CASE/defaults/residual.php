<?php
$scripts = array( 'residual_risk.js','menu.js', 'jscolor.js');
$styles = array();
$page_title = "Set Up Risk residual risks";
require_once("../inc/header.php");
?>
<div>
<form method="post" name="form-residual-risk">
<div id="residual_risk_message"></div>
<table border="1" id="residual_risk_table">
  <tr>
    <th>ID</th>
    <th>Risk Rating</th>
    <th>Residual Risk magnitude</th>
    <th>Response</th>    
    <th>Colour assigned to risk rating</th>
    <th></th>    
    <th>Status</th>        
    <th></th>    
  </tr>
  <tr>
    <td>#</td>
    <td>
    	<input type="text" name="residual_risk_from" id="residual_risk_from" size="3" />
        to
        <input type="text" name="residual_risk_to" id="residual_risk_to" size="3" /></td>
    <td><input type="text" name="residual_magnitude" id="residual_magnitude" /></td>
    <td><textarea id="residual_response" name="residual_response"></textarea></td>
    <td><input type="text" name="residual_color" id="residual_color"  class="color" value="abf207" /> </td>
    <td><input type="submit" name="add_residual" id="add_residual" value="Add" /></td>
    <td></td>
    <td></td>        
  </tr>
</table>
</form>
</div>

