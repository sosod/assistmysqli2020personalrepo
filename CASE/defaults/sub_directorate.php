<?php
$scripts = array( 'sub_directorate.js','menu.js');
$styles = array( );
$page_title = "Set Up Risk sub directorate structure";
require_once("../inc/header.php");
?>
<div>
<form method="post" name="sub-directorate-form" id="sub-directorate-form">
<div id="subdir_message"></div>
<table border="1" id="sub_directorate_table">
  <tr>
    <th>ID</th>
    <th>Short Code</th>
    <th>Department</th>
    <th>Division</th>    
    <th>Responsible for risk</th>
    <th></th>    
    <th>Status</th> 
    <th></th>            
  </tr>
  <tr>
    <td>#</td>
    <td><input type="text" name="sub_shortcode" id="sub_shortcode"  /></td>
    <td>
        <select id="subdir_dpt" name="subdir_dpt">
        	<option>--department--</option>
        </select>
    </td>
    <td><input type="text" name="subdir_division" id="subdir_division" /></td>
    <td>
    	<select id="subdir_user" name="subdir_user">
        	<option>--user--</option>
        </select>
    </td>
    <td><input type="submit" name="add_subdir" id="add_subdir" value="Add" /></td>
    <td></td>
    <td></td>        
  </tr>
</table>
</form>
</div>
