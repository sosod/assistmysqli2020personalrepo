<?php
$scripts = array( 'defaults.js','menu.js', 'setup_logs.js');
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Set Up Risk residual risks";
require_once("../inc/header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
  <tr>
    <td colspan="2" class="noborder">
		<form method="post" id="defaults-form" name="defaults-form">
		<table border="1" id="defaults_table">
		  <tr>
		    <td><h4>Ref</h4></td>
		    <td><h4>Defaults to be applied</h4></td>
		    <td><h4>To be applied</h4></td>
		    <td><h4>Action</h4></td>
		  </tr>
		</table>
		</form>    
    </td>
  </tr>
  <tr>
    <td class="noborder"><?php displayGoBack("",""); ?></td>
    <td class="noborder"><?php displayAuditLogLink("defaults_logs", true)?></td>
  </tr>
</table>


