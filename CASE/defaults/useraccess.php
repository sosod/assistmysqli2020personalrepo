<?php
$scripts = array( 'useraccess.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "User Access";
require_once("../inc/header.php");
require_once("../../library/class/assist_helper.php");
?>
<div>
<form method="post" id="useraccess-form" name="useraccess-form">
<div id="useraccess_message" class="message"></div>
<table border="1" id="useraccess_table">
  <tr>
    <th>Ref</th>
    <th>User</th>
    <th>Module<br />Admin</th>
    <th>Create<br />Queries</th>
    <!-- <th>Create Actions</th> -->
    <th>Update<br />All</th>
    <th>Edit All</th>
    <th>Reports</th>
    <th>Assurance</th>
    <th>Setup</th>
    <th>Action</th>
  </tr>
  <tr>
    <td>#</td>
    <td>
		<select name="userselect" id="userselect">
        	<option value="">--- SELECT ---</option>
        </select>
    </td>
    <td>
    	<select id="module_admin" name="module_admin">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td>
    	 <select id="create_risks">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <!-- <td>
    	 <select id="create_actions" name="create_actions">
        	<option>--create actions--</option>
            <option value="1" selected="selected">Yes</option>
            <option value="0">No</option>            
        </select>
    </td> -->    
    <td>
    	 <select id="view_all" name="view_all">
            <option value="1" selected="selected">Yes</option>
            <option value="0">No</option>            
        </select>
    </td>
    <td>
    	<select id="edit_all" name="edit_all">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td>
    	 <select id="reports" name="reports">
            <option value="1" selected="selected">Yes</option>
            <option value="0">No</option>            
        </select>
    </td>
    <td>
    	 <select id="assurance" name="assurance">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>    
    <td>
    	 <select id="usersetup" name="usersetup">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td><input type="submit" value="Add" id="setup_access" name="setup_access" /></td>
  </tr>
</table>
<div><?php displayGoBack("",""); ?></div>
</form>
</div>
<script type=text/javascript>
$(function() {
	var tr = 0;
	$("#useraccess_table tr").each(function() {
		$(this).children().css("text-align","center");
	});
	//$("#useraccess_table tr:first").next().children().css("text-align","center");
	//$("#useraccess_table tr:first").next().children().css("text-align","center");
});
</script>