<?php
$scripts = array( 'impact.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Set Up Risk Impacts";
require_once("../inc/header.php");
?>
<script language="javascript">

</script>
<div>
<form id="impact-form" name="impact-form">
<div id="impact_message"></div>
<br /><br /><br />
<table border="1" id="impact_table">
  <tr>
    <th width="18">ID</th>
    <th width="100">Rating</th>
    <th width="149">Assessment</th>
    <th width="161">Definition</th>
    <th width="144">Color assigned to impact</th>
    <th width="79"></th>
    <th width="63">Status</th>    
    <th width="8"></th>
  </tr>
  <tr>
    <td>#</td>
    <td>
   		<input type="text" name="imprating_from" id="imprating_from" size="2" /> 
   	to
   		 <input type="text" name="imprating_to" id="imprating_to" size="2" />
    </td>
    <td><input type="text" name="impact_assessment" id="impact_assessment" /></td>
    <td><textarea id="impact_definition" name="impact_definition"></textarea></td>
    <td><input type="text" name="impact_color" id="impact_color" class="color" value="e2ddcf" /></td>
    <td><input type="submit" name="add_impact" id="add_impact" value="Add" /></td>
    <td></td>
    <td></td>
  </tr>
</table>
</form>
</div>

