<?php
$scripts = array( 'udf.js','menu.js' );
$styles = array();
$page_title = "User Defined Fields";
require_once("../inc/header.php");
?>
<div>
<div id="udf_message" class="message"></div>
<table width="50%">
	<tr>
	<th>Field Name</th>
	<th>Field Type</th> 
	<th>Required</th>
	<th>Default Value</th>
    <th></th>                
    </tr>
    <tr>
    	<td>
        	<input type="button" id="add_new_udf" name="add_new_udf" value="Add New" />
        </td>
    </tr>
    <tr>
    	<td colspan="5" align="left"><?php displayGoBack("",""); ?></td>
    </tr>
</table>
</div>