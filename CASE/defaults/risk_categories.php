<?php
$scripts = array( 'risk_category.js','menu.js', 'setup_logs.js', 'ignite.textcounter.js' );
$styles = array();
$page_title = "Set Up Risk Categories";
require_once("../inc/header.php");
?>
<div>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="risk-category-form" name="risk-category-form">
			<table border="1" id="riskcategory_table">
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th>Query Category</th>
			    <th>Description of Query Category</th>
			    <!-- <th>Query Type</th> -->
			    <th></th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			  <tr>
				    <td>#</td>
				    <td><input type="text" name="category_shortcode" id="category_shortcode" /></td>
				    <td>
				    	<textarea name="risk_category" id="risk_category" class="textcounter"></textarea>
				    </td>
				    <td>
					   	<textarea class="mainbox" id="category_description" name="category_description"></textarea>
				   </td>
				   <!--
				   <td>
				    <select name="category_risktype" id="category_risktype">
				    	<option value="">--Select query type--</option>
				    </select>
				    </td>
				    -->
				    <td><input type="submit" name="add_riskcategory" id="add_riskcategory" value="Add" /></td>
				    <td></td>   
				    <td></td> 
			 	 </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("",""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("categories_logs", true) ?></td>
	</tr>
</table>
</div>

