<?php
$scripts = array( 'risk_category.js','menu.js', 'ignite.textcounter.js');
$styles = array();
$page_title = "Edit Risk Status";
require_once("../inc/header.php");
$riskcat 	= new RiskCategory( "", "", "", "" );
$category 	= $riskcat  -> getACategory( $_REQUEST['id']);
?>
    <div>
 <?php JSdisplayResultObj(""); ?>   
    <form id="edit-risk-category-form" name="edit-risk-category-form" method="post">
    <div id="editriskcategory_message"></div>
    <table border="1" id="edit_riskcategory_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Category Status:</th>
        <td>
        	<select id="category_status" name="category_status">
            	<option value="1" <?php if($category['active']==1) {?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if($category['active']==0) {?> selected="selected" <?php } ?>>Inactive</option>                
            </select>
        </td>
      </tr>
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="risk_category_id" id="risk_category_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="change_riskcategory" id="change_riskcategory" value="Save Changes" class="isubmit" />
        <input type="submit" name="cancel_riskcategory" id="cancel_riskcategory" value="Cancel" class="idelete" />
        </td>
      </tr>
      <tr>
         <td class="noborder" align="left"><?php displayGoBack("/QUERY/defaults/risk_categories.php",""); ?></td>
         <td class="noborder"></td>
      </tr>
    </table>
    </form>
    </div>

