<?php
$scripts = array( 'edit_subdir.js','menu.js');
$styles = array( );
$page_title = "Edit Sub Directorate";
require_once("../inc/header.php");
$subdir 		= new SubDirectorate( "", "", "" ,"");
$subdirectorate = $subdir -> getASubDirectorate( $_REQUEST['id'] );
?>

    <div>
    <form id="change-subdirectorate-form" name="change-subdirectorate-form" method="post">
    <div id="subdirectorate_message"></div>
    <table border="1" id="change_subdirectorate_table">
    <tr>
    	 <th>Ref</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
          <tr>
      	<th>Subdirectorate Status</th>
        <td>
       		<select name="subdir_status" id="subdir_status">
            	<option value="1" <?php if($subdirectorate['active']==1) { ?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if($subdirectorate['active']==0) { ?> selected="selected" <?php } ?>>InActive</option>
            </select>
        </td>
      </tr>
     <tr>
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="subdir_id" id="subdir_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="change_subdir" id="change_subdir" value="Save Changes" />
        <input type="submit" name="cancel_subdir" id="cancel_subdir" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td align="left" colspan="2">
        	<?php displayGoBack("",""); ?>
        </td>
        </tr>
    </table>
    </form>
    </div>
