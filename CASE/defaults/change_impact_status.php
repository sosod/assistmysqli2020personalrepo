<?php
$scripts = array( 'edit_impact.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit Impact";
require_once("../inc/header.php");
$imp 	= new Impact( "", "", "", "", "");
$impact =  $imp -> getAImpact( $_REQUEST['id'] ) ;
?>
<div>
<form id="change-impact-form" name="change-impact-form">
<div id="impact_message"></div>
<table border="1" id="impact_table">
  <tr>
  	
    <th>ID</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Impact Status</th>
    <td>
    	<select id="impact_status">
        	<option value="1" <?php if($impact['active']==1) { ?> selected <?php } ?> >Active</option>
            <option value="0" <?php if($impact['active']==0) { ?> selected <?php } ?> >InActive</option>
        </select>
	</td>
    </tr>
    <tr>
      <th>&nbsp;</th>
    <td>
    <input type="hidden" name="impact_id" id="impact_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="change_impact" id="change_impact" value="Edit" />
    <input type="submit" name="cancel_impact" id="cancel_impact" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td colspan="2" align="left"><?php displayGoBack("",""); ?></td>
  </tr>
</table>
</form>
</div>
