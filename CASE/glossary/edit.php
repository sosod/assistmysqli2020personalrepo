<?php
$scripts = array( 'edit_glossary.js');
$styles = array( 'colorpicker.css' );
$page_title = "Edit glossary";
require_once("../inc/header.php");

$glosssary = new Glossary();
$gloss 	   = $glosssary -> getAGlossary( $_REQUEST['id']); 
?>
<div id="glossary_message"></div>
<table id="new_term_table" align="left">
	<tr>
    	<td>Category</td>
        <td>
 		<input type="text" name="category" id="category" value="<?php echo $gloss['category']; ?>" />
        </td>
    </tr>
	<tr>
    	<td>Teminology</td>
        <td>
			<input type="text" name="terminolgy" id="terminolgy" value="<?php echo $gloss['terminology']; ?>" />
        </td>
    </tr>    
	<tr>
    	<td>Explanation</td>
        <td>
			<textarea name="explanation" id="explanation"><?php echo $gloss['explanation']; ?></textarea>
        </td>
    </tr>   
    <tr>
    	<td align="right">
        <?php displayGoBack("",""); ?>
        </td>
        <td>
        <input type="hidden" name="glossaryid" id="glossaryid" value="<?php echo $_REQUEST['id']; ?>" />
        <input type="submit" name="edit_glossary" id="edit_glossary" value="Save Changes" />
        <input type="submit" name="delete_glossary" id="delete_glossary" value="Delete" />
        </td>
    </tr>     
</table>