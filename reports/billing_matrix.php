<?php 
//error_reporting(-1);
//$fixed_date = strtotime('25 July 2012 01:00:00');
require_once '../header.php';
if(!isset($folder)) { $folder = "files/"; }
ob_start();
$cc = isset($_REQUEST['cc']) ? $_REQUEST['cc'] : $_SESSION['cc'];
$tki = isset($_REQUEST['tki']) ? $_REQUEST['tki'] : $_SESSION['tid'];

$multi_mods = array('ACTION','DOC');
$multi_mods_names = array('ACTION'=>"Task Assist",'DOC'=>"Document Management");
?>
<style type=text/css>
//	table.noborder, td.noborder { border: 1px solid #009900; }
	table, table td, table th {
		border: 1px solid #555555;
	}
	p.note {
		font-style: italic;
		margin: -10px 0px 5px 10px;
		font-size: 7pt;
	}
	
	table.c_detail td.head, table.c_cost td.head {
		font-weight: bold;
	}
	table.c_detail, table.c_detail td {
		border: 0px solid #ffffff;
		vertical-align: bottom;
	}
	table.c_modules {
		margin-top: 15px;
	}
	table.c_modules tfoot td, table.c_cost tfoot td { 
		font-weight: bold; 
		background-color: #dedede;
	}
	table.c_modules th {
		background-color: #cccccc;
		color: #000000;
		border: 1px solid #555555;
	}
	table.c_modules tbody td.foot {
		font-weight: bold;
		background-color: #dedede;
	}
	table.c_modules tbody th.res {
		font-weight: bold;
		text-align: left;
		background-color: #efefef;
	}
	
	
	
	h2.matrix, h3 {
		font-size:11pt;
		font-family:Arial;
		margin-top:0px;
	}
	h3 {
		margin-top: 20px;
		margin-bottom: 5px;
	}
	p.matrix{
		font-style: italic;
		font-size:7pt;
		font-family:Arial;
		margin:-15px 0px 0px 5px;
	}
	table.matrix {
		border: 1px solid #cdcdcd;
	}
	table.matrix td {
		padding:1px 3px 1px 10px;
		font-size:7pt;
		color: #777777;
		border: 1px solid #cdcdcd;
	}
	table.matrix th {
		padding:2px 4px 2px 4px;
		font-size: 7pt;
		background-color:#ababab;
		color: #777777;
		border: 1px solid #cdcdcd;
	}
	table.matrix th.res {
		padding:2px 3px 2px 5px;
		font-size:7pt;
		background-color:#dedede;
		color: #777777;
		text-align:left;
		border: 1px solid #cdcdcd;
	}
	
	table.res_sum td, table.res_sum th {
		font-size: 7.5pt;
	}
	
	table.res_sum tfoot td, table.res_sum tbody td.foot {
		font-weight: bold;
		background-color: #dedede;
	}
	table.res_sum tbody td.head {
		font-weight: bold;
		background-color: #ccccff;
		font-size: 8pt;
	}
	table.res_sum tbody td.total {
		font-weight: bold;
		background-color: #dedede;
		text-align: right;
	}
	table.res_sum tfoot td.total, table.res_sum tbody td.foot.total{
		font-weight: bold;
		background-color: #ababab;
		text-align: right;
	}
	
	div p {
		font-size: 6.5pt;
		line-height: 7.5pt;
		margin: 0 0 0 0;
		margin-top: 5px;
		margin-left: 1em;
		text-indent: -1em;
	}
</style>

<?php
/* GET NON-BILL MODULE INFO */
$sql = "SELECT * FROM assist_billing_rules WHERE active = 1";
$results = mysql_fetch_all($sql,"A");
$non_bill = array();
foreach($results as $r) {
	if(!isset($non_bill[$r['cmpcode']])) { $non_bill[$r['cmpcode']] = array(); }
	if(strlen($r['modlocation'])>0) {
		$non_bill[$r['cmpcode']][] = $r['modlocation'];
	}
	if(strlen($r['modref'])>0) {
		$non_bill[$r['cmpcode']][] = $r['modref'];
	}
}

/* GET BILLING MATRIX INFO FROM DB */
$billing_class = array();
$billing_class = mysql_fetch_all_fld("SELECT * FROM assist_billing_class WHERE class_active = 1","class_id","A"); // arrPrint($billing_class);

$billing_category = array();
$billing_category = mysql_fetch_all_fld("SELECT * FROM assist_billing_category WHERE cate_active = 1","cate_id","A"); // arrPrint($billing_category);
$billing_fees = array();
foreach($billing_category as $bc) {
	$c = $bc['cate_id'];
	$billing_fees[$c] = array(
		'H' => array(),
		'M' => array()
	);
	foreach($billing_class as $cl) {
		$cli = $cl['class_id'];
		$billing_fees[$c]['H'][$cli] = 0;
	}
}


$bf = mysql_fetch_all("SELECT * FROM assist_billing_fees WHERE fee_active = 1 ORDER BY fee_type, fee_mods","A");
foreach($bf as $b) {
	$c = $b['fee_cate_id'];
	$cli = $b['fee_class_id'];
	$t = $b['fee_type'];
	$v = $b['fee_value'];
	if($t=="H") {
		$billing_fees[$c]['H'][$cli] = $v;
	} else {
		$m = $b['fee_mods'];
		if($m<0) {
			if(!isset($billing_fees[$c]['M']['max'])) {
				$billing_fees[$c]['M']['max'] = array(
					'title'=>"",
					'mods'=>-1,
					'class'=>array()
				);
			}
			$billing_fees[$c]['M']['max']['class'][$cli] = $v;
		} else {
			if(!isset($billing_fees[$c]['M'][$m])) {
				$billing_fees[$c]['M'][$m] = array(
					'title'=>$m." Module".($m>1 ? "s" : ""),
					'mods'=>$m,
					'class'=>array()
				);
			}
			$billing_fees[$c]['M'][$m]['class'][$cli] = $v;
		}
	}
}
foreach($billing_category as $c => $bc) {
	$m = count($billing_fees[$c]['M'])-1;
	$t = "> ".$m." Modules";
	$billing_fees[$c]['M']['max']['title'] = $t;
	$bm = $billing_fees[$c]['M']['max'];
	unset($billing_fees[$c]['M']['max']);
	$billing_fees[$c]['M']['max'] = $bm;
}
//arrPrint($billing_fees);

/* FUNCTION TO DRAW TABLE WITH BILLING MATRIX */
function drawMatrix() {
	global $billing_class, $billing_category, $billing_fees;
	
	$valid_date = mysql_fetch_one("SELECT MAX( fee_date_added ) AS valid_date
			FROM  `assist_billing_fees` 
			WHERE fee_active =1","A");
	$valid_date = strtotime($valid_date['valid_date']);

	$echo = "<h2 class=matrix>Pricing Matrix</h2>
	<p class=matrix>Valid as at ".date("d F Y",$valid_date)."</p>
	<table class=matrix><thead>
			<tr>
				<th></th>";
		foreach($billing_class as $ci=>$bc) {
			$echo.= "<th width=53>".$bc['class_name'].($ci!="P" ? "<sup>~</sup>" : "")."</th>";
		}
	$echo.= "	</tr>
		</thead><tbody>";
		foreach($billing_category as $cate_id => $cate) {
			$echo .="
			<tr>
				<th class=res colspan=".(1+count($billing_class)).">".$cate['cate_name']." (".($cate['cate_users_max']>0 ? ($cate['cate_users_min']." - ".$cate['cate_users_max']) : "> ".$cate['cate_users_min'])." Users)</th>
			</tr>";
			$billing = $billing_fees[$cate_id];
			//Hosting row
			$echo .="
			<tr>
				<td class=b>Hosting & Backup fee&nbsp;</td>";
				foreach($billing_class as $ci => $bc) {
					$echo .="<td class=center>".($ci=="P" ? $billing['H'][$ci] : "")."</td>";
				}
			$echo .="</tr>";
			//Modules
			foreach($billing['M'] as $bf) {
				$echo .="
				<tr>
					<td class=b>".$bf['title']."</td>";
					foreach($billing_class as $ci => $bc) {
						$echo .="<td class=center>".($ci=="P" ? $bf['class'][$ci] : "-")."</td>";
					}
				$echo .="</tr>";
			}
		}

	$echo .="</tbody></table>";

	return $echo;
}

//$non_bill_modules = array("PMSPS","PP","AQA");


/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);


$company = array();
$multiple_resellers = false;
$fields = "c1.cmpcode, c1.cmpreseller, c1.cmpname, c1.cmpstatus, c1.cmp_price_class, c1.cmp_is_csi, c1.cmp_bill_hosting";
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" */
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') AND c1.cmp_price_class <> 'X'";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT $fields FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S')
				WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') AND c1.cmp_is_demo = 0 AND c1.cmp_price_class <> 'X' ".($res[1]!="ALLCMP" && $res[1]!="DEMO" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
		$multiple_resellers = true;
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND c1.cmpcode NOT IN ('IASSIST','BLANK') AND (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') AND c1.cmp_is_demo = 0 AND c1.cmp_price_class <> 'X' ORDER BY c1.cmpreseller, c1.cmpcode, c1.cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}
$total = array();
$history = array();
//DISPLAY COMPANIES
foreach($resellers as $rcode => $res) {
	$res_company = $company[$rcode];
	$res_total = array('mods'=>array(),'cmp_count'=>0,'cmp'=>array());
	foreach($res_company as $cmp) {
		$hist = array('modules'=>array(),'details'=>array());
		$cmpcode = strtolower($cmp['cmpcode']);
		if(in_array($db_other.$cmpcode,$dbs)) {
			$rep_date = isset($fixed_date) ? $fixed_date : $today;
			$has_pmsps = false;
			$cmp_type = $cmp['cmp_is_csi']==1 ? "non" : "bill";
			$cmp_bill_hosting = $cmp['cmp_bill_hosting']==1 ? true : false;
			$res_total['cmp_count']++;
			echo "
			<h1 ".(count($total)>0 ? "style='page-break-before: always;'" : "").">".$cmp['cmpname']." (".$cmp['cmpcode'].")</h1>
			<p class=note>Report generated on ".date("d F Y",$rep_date)." at ".date("H:i",$rep_date).".</p>
			<table class=noborder width=100%><tr>
				<td class=noborder>
				";
			//GET MODULE / LICENSE INFO
			$sql = "SELECT m.modref, m.modlocation, m.modtext, m.mod_billable , count(DISTINCT usrtkid) as lic 
					FROM assist_menu_modules m 
					LEFT OUTER JOIN assist_".$cmpcode."_menu_modules_users mu 
						ON mu.usrmodref = m.modref 
						AND mu.usrtkid IN (SELECT tkid FROM assist_".$cmpcode."_timekeep t WHERE t.tkstatus = 1 AND t.tkuser <> 'support' AND t.tkuser <> 'admin')
					WHERE m.modyn = 'Y' AND m.modref <> 'TD' GROUP BY m.modref ORDER BY m.modtext";
			/*$sql = "SELECT m.modref, m.modlocation, m.modtext, m.mod_billable , count(usrtkid) as lic
					FROM assist_menu_modules m 
					LEFT OUTER JOIN assist_".$cmpcode."_menu_modules_users mu
					  ON mu.usrmodref = m.modref
					LEFT OUTER JOIN assist_".$cmpcode."_timekeep t
					  ON mu.usrtkid = t.tkid AND t.tkstatus = 1 AND t.tkuser <> 'support' AND t.tkuser <> 'admin' ".(isset($fixed_date) ? "AND CONVERT(t.tkadddate,SIGNED) < ".strtotime($fixed_date) : "")."
					WHERE m.modyn = 'Y' 
					  AND m.modref <> 'TD'
					GROUP BY m.modref
					ORDER BY m.modtext"; //echo $sql;*/
			$modules = mysql_fetch_all($sql,"O",$cmpcode);
			/*$sql = "SELECT count(m.mod_billable) as mc
					FROM assist_menu_modules m 
					WHERE m.modyn = 'Y' 
					  AND m.modref <> 'TD'
					  AND m.mod_billable = 1
					GROUP BY m.mod_billable";
			$mc = mysql_fetch_one_fld($sql,"mc","O",$cmpcode);*/
			$mod_count = 0;
			$lic_count = 0;
			$module_output = array('non'=>array(),'bill'=>array());
			foreach($modules as $m_key=>$m) {
				if($m['modlocation']=="PMSPS") { 
					$has_pmsps = true; 
					$sql = "SELECT m.modref, m.modlocation, m.modtext, m.mod_billable , count(DISTINCT usrtkid) as lic 
						FROM assist_menu_modules m 
						LEFT OUTER JOIN assist_".$cmpcode."_menu_modules_users mu 
							ON mu.usrmodref = m.modref 
							AND mu.usrtkid IN (SELECT tkid FROM assist_".$cmpcode."_timekeep t WHERE t.tkstatus > 0 AND t.tkuser <> 'support' AND t.tkuser <> 'admin')
						WHERE m.modyn = 'Y' AND m.modref <> 'TD' AND m.modref = 'PMSPS' GROUP BY m.modref ORDER BY m.modtext";
					$row =mysql_fetch_one($sql,"O",$cmpcode);
					$m['lic'] = $row['lic'];
					$modules[$m_key]['lic'] = $row['lic'];
				}
				$mo = array();
				$mc = (int)$m['mod_billable'];
				/*$mc = 0;
				$mc+= in_array($m['modref'],$non_bill['ALL']) ? 1 : 0;
				$mc+= in_array($m['modlocation'],$non_bill['ALL']) ? 1 : 0;
				$mc+= in_array($m['modlocation'],$non_bill[strtoupper($cmpcode)]) ? 1 : 0;
				$mc+= in_array($m['modref'],$non_bill[strtoupper($cmpcode)]) ? 1 : 0;*/
				//if($mc!=0) {
if($m['lic']>0) {
					$mod_count+=$mc;
					$lic_count+=$mc!=0 ? $m['lic'] : 0;
} else {
	$mc = 0;
}
					$mo['ref'] = $mc!=0 ? $mod_count : "#";
					$ml = in_array($m['modlocation'],$multi_mods) ? $m['modlocation'] : $m['modref'];
					$mn = in_array($m['modlocation'],$multi_mods) ? $multi_mods_names[$ml] : $m['modtext'];
				$bill_type = $mc!=0 ? "bill" : "non";
					if(!isset($res_total['mods'][$cmp_type][$bill_type][$ml])) {
						$res_total['mods'][$cmp_type][$bill_type][$ml] = array(
							'name'=>$mn,
							'clients'=>0,
							'total_lic'=>0,
							'total_lic_fee'=>0,
						);
					}
				//} else {
				//	$mo['ref'] = "#";
				//	$mo['lic'] = "N/A";
				//}
				$mo['lic'] = $m['lic'];
				$mo['name'] = $m['modtext'];
				$mo['loc'] = $ml;
				$mo['billable'] = $m['mod_billable'];
				$module_output[$bill_type][] = $mo;
			}
			//arrPrint($billing_category);
			$cmp_bill_class = $cmp['cmp_price_class'];
			$lic_ave = $lic_count/$mod_count;
			//if($has_pmsps) { 
				//$bc = $billing_category[3];
//			} else
			if($lic_ave >0) {
				foreach($billing_category as $bc) {
					if($lic_ave < $bc['cate_users_max']) {
						break;
					}
				}
			} else {
				$bc = $billing_category[1];
			}
			$cmp_cate_id = $bc['cate_id'];
if($has_pmsps) {
	$hosting = $cmp_bill_hosting ? $billing_fees[3]['H'][$cmp_bill_class] : 0;
} else {
	$hosting = $cmp_bill_hosting ? $billing_fees[$cmp_cate_id]['H'][$cmp_bill_class] : 0;
}
			if($mod_count==0) {
				$lic_fee = $billing_fees[$cmp_cate_id]['M'][1]['class'][$cmp_bill_class];
			} elseif(isset($billing_fees[$cmp_cate_id]['M'][$mod_count])) {
				$lic_fee = $billing_fees[$cmp_cate_id]['M'][$mod_count]['class'][$cmp_bill_class];
			} else {
				$lic_fee = $billing_fees[$cmp_cate_id]['M']['max']['class'][$cmp_bill_class];
			}
			$hist['details'] = array(
				'reseller'				=> $rcode,
				'package'				=> $cmp_bill_class,
				'billable_type'			=> $cmp_type,
				'mod_count'				=> $mod_count,
				'total_lic'				=> $lic_count,
				'ave_lic'				=> number_format($lic_ave,2),
				'pricing_cate'			=> $bc['cate_id'],
				'host_fee_per_month'	=> $hosting,
				'lic_fee_per_month'		=> $lic_fee,
			);
			echo "<table class=c_detail>
				<tr>
					<td class=head>Reseller:</td><td>".$res['cmpname']."</td>
				</tr>
				<tr>
					<td class='head'>Status:</td><td>".($res['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active")."</td>
				</tr>
				<!--<tr>
					<td class=head>Package:</td><td>".$billing_class[$cmp_bill_class]['class_name']." (".$cmp_bill_class.")</td>
				</tr> -->
				<tr>
					<td class=head>Billable Client:</td><td>".($cmp_type=="bill" ? "Yes" : "No")."</td>
				</tr>
				<!--<tr>
					<td colspan=2></td>
				</tr><sup>+</sup> -->
				<tr>
					<td class=head>Billable Module Count:</td><td>$mod_count</td>
				</tr><tr>
					<td class=head>Total Billable Licenses:</td><td>$lic_count</td>
				</tr>
				<!-- <tr>
					<td class=head>Average Billable Licenses:</td><td>".number_format($lic_ave,2)."</td>
				</tr><tr>
					<td class=head>Pricing Category:</td><td>".$bc['cate_name']."</td>
				</tr><tr>
					<td class=head>Hosting Fee<br />per month".($has_pmsps ? "^" : "").":*</td><td>".($cmp_bill_hosting ? "R ".number_format($hosting,2,".",",") : "N/A")."</td>
				</tr><tr>
					<td class=head>License Fee per<br />billable license per month:*</td><td>R ".number_format($lic_fee,2,".",",")."</td>
				</tr> -->
			</table>";
			echo "<table class=c_modules>
			<thead>
				<tr>
					<th>#</th>
					<th>Module Name</th>
					<th>Number of<br />Licenses in Use</th>
					<!-- <th>License Fee*</th> -->
				</tr>
			</thead><tbody>";
			$type = array("bill","non");
			$bill_lic_total = 0;
			foreach($type as $bill_type) {
				if(count($module_output[$bill_type])>0) {
					$title = $bill_type=="bill" ? "Billable Modules" : "Non-Billable Modules";
					$lic_total = 0;
					$mod_lic_count = 0;
					echo "<tr><th class=res colspan=4>".$title."</th></tr>";
					foreach($module_output[$bill_type] as $m) {
						$mod_lic_fee = $m['lic']*$lic_fee;
						$lic_total+=$mod_lic_fee;
						$mod_lic_count+=$m['lic'];
						$m['lic_fee'] = $mod_lic_fee;
						$hist['modules'][] = $m;
						echo "
						<tr>
							<td class=b>".$m['ref']."</td>
							<td>".$m['name']."</td>
							<td class=center>".$m['lic']."</td>
							<!-- <td class=right>".($bill_type=="bill" ? "R ".number_format($mod_lic_fee,2,".",",") : "N/A")."</td> -->
						</tr>
						";
						$ml = $m['loc'];
						if(isset($res_total['mods'][$cmp_type][$bill_type][$ml])) {
							$res_total['mods'][$cmp_type][$bill_type][$ml]['clients']++;
							$res_total['mods'][$cmp_type][$bill_type][$ml]['total_lic']+=$m['lic'];
							$res_total['mods'][$cmp_type][$bill_type][$ml]['total_lic_fee']+=$mod_lic_fee;
						}
					}
					if($bill_type == "bill") { $bill_lic_total = $lic_total; }
					echo "
					<tr>
						<td colspan=2 class='foot right'>Total:</td>
						<td class='foot center'>$mod_lic_count</td>
						<!-- <td class='foot right'>".($bill_type=="bill" ? "R ".number_format($lic_total,2,".",",") : "N/A")."</td> -->
					</tr>";
				}
			}
			echo "</tbody>
			</table>";
			/*echo "<h3>Total Billable ".($cmp_type!="non" ? "Cost" : "Donation")."*</h3>
			<table class=c_cost width=250px>
			<tbody>
				<tr>
					<td class=head>Hosting Fee:*</td><td class=right>".($cmp_bill_hosting===true ? "R ".number_format($hosting,2,".",",") : "N/A")."</td>
				</tr><tr>
					<td class=head>License Fee:*</td><td class=right>R ".number_format($bill_lic_total,2,".",",")."</td>
				</tr>
			</tbody><tfoot>
				<tr>
					<td class='right'>Total:*</td><td class=right>R ".number_format(($hosting+$bill_lic_total),2,".",",")."</td>
				</tr>
			</tfoot>
			</table>";
			echo "";
			echo "</td>
				<td class=noborder><div class=float style='width: 315px'>".drawMatrix()."
				<h2 class=matrix style='margin-top: 10px; margin-bottom: -5px;'>Notes:</h2 >
				<p>* All prices exclude VAT, SMSs and Data Storage.</p>
				<p>+ Only billable Action iT (Pty) Ltd modules are included in the module and license count.</p>
				<p>^ If subscribed to Performance Assist hosted on behalf of Ignite Advisory Services (Pty) Ltd, the Premium Category 3 Hosting & Backup fee automatically applies.</p>
				<p>~ Base and Extended pricing options are currently under development.  More information will become available in the future.</p></div></td>
			</tr></table>";
			*/
			$total[] = $cmpcode;
			$res_total['cmp'][$cmp_type][$cmpcode] = array(
				'cmpcode'=>strtoupper($cmpcode),
				'cmpname'=>$cmp['cmpname'],
				'cmpname'=>$cmp['cmpname'],
				'details'=>$hist['details']
			);
			$res_total['cmp'][$cmp_type][$cmpcode]['details']['total_lic_fee'] = round($bill_lic_total,2);
			$res_total['cmp'][$cmp_type][$cmpcode]['details']['total_cost'] = round($bill_lic_total+$hosting,2);
			//INSERT INTO assist_billing_report_company_history (cmpcode, by_cmpcode, by_tkid, date_drawn, details, modules)
			$history[] = "('".strtoupper($cmpcode)."', '".strtoupper($cc)."', '$tki', now(), '".serialize($hist['details'])."', '".serialize($hist['modules'])."'";
		} //if valid db
	} //end foreach res_company
	$summary = array();
	//arrPrint($res_total);
$rep_date = isset($fixed_date) ? $fixed_date : $today;
	if($res_total['cmp_count']>0) {
			$summary['client']['non'] = array(
				'mod_count'				=> 0,
				'total_lic'				=> 0,
				'ave_lic'				=> 0,
				'host_fee_per_month'	=> 0,
				'lic_fee_per_month'		=> 0,
				'total_lic_fee'			=> 0,
				'total_cost'			=> 0,
			);
			$summary['client']['bill'] = array(
				'mod_count'				=> 0,
				'total_lic'				=> 0,
				'ave_lic'				=> 0,
				'host_fee_per_month'	=> 0,
				'lic_fee_per_month'		=> 0,
				'total_lic_fee'			=> 0,
				'total_cost'			=> 0,
			);
			$summary['mods']['bill'] = array(
				'clients'		=> 0,
				'total_lic'		=> 0,
				'total_lic_fee'	=> 0,
			);
			$summary['mods']['non'] = array(
				'clients'		=> 0,
				'total_lic'		=> 0,
				'total_lic_fee'	=> 0,
			);
			echo "
			<h1 style='page-break-before: always;'>Summary for Reseller: ".$res['cmpname']." (".$rcode.")</h1>
			<p class=note>Report generated on ".date("d F Y",$rep_date)." at ".date("H:i",$rep_date).".</p>
			<h2>Summary per Client</h2>
			<table class=res_sum>
				<thead>
					<tr>
						<th colspan=1>Client</th>
						<th>Billable Module<br />Count</th>
						<th>Total Billable<br />Licenses</th>
						<th>Average Billable<br />Licenses</th>
						<th>Pricing<br />Category</th>
						<th>Billable Hosting<br />Fee*</th>
						<th>Billable License Fee<br />Per License*</th>
						<th>Total Billable<br />License Fee*</th>
						<th>Total Billable<br />Cost*</th>
					</tr>
				</thead><tbody>";
			$type_cmp = array("bill","non");//$type = "bill";
			foreach($type_cmp as $type) {
				if(count($res_total['cmp'][$type])>0) {
					echo "<tr><td class=head colspan=9>".($type=="non" ? "Non-" : "")."Billable Clients</td></tr>";
					foreach($res_total['cmp'][$type] as $cmpcode => $cmp) {
						$cmpcode = strtoupper($cmpcode);
						$summary['client'][$type]['mod_count']+=$cmp['details']['mod_count'];
						$summary['client'][$type]['total_lic']+=$cmp['details']['total_lic'];
						$summary['client'][$type]['host_fee_per_month']+=$cmp['details']['host_fee_per_month'];
						$summary['client'][$type]['total_lic_fee']+=$cmp['details']['total_lic_fee'];
						$summary['client'][$type]['total_cost']+=$cmp['details']['total_cost'];
						echo "
						<tr>
							<!-- <td class='center b'>$cmpcode</td> -->
							<td class=b>".$cmp['cmpname']." ($cmpcode)</td>
								<td class=center>".$cmp['details']['mod_count']."</td>
								<td class=center>".$cmp['details']['total_lic']."</td>
								<td class=center>".$cmp['details']['ave_lic']."</td>
								<td class=center>".$cmp['details']['pricing_cate']."</td>
								<td class=right>".number_format($cmp['details']['host_fee_per_month'],2)."</td>
								<td class=right>".number_format($cmp['details']['lic_fee_per_month'],2)."</td>
								<td class=right>".number_format($cmp['details']['total_lic_fee'],2)."</td>
								<td class=total>".number_format($cmp['details']['total_cost'],2)."</td>
						</tr>
						";
					}
					echo "
							<tr>
								<td colspan=1 class='foot right'>".($type=="non" ? "Non-" : "")."Billable Total:</td>
								<td class='foot center'></td>
								<td class='foot center'>".$summary['client'][$type]['total_lic']."</td>
								<td class='foot center'></td>
								<td class='foot center'></td>
								<td class='foot right'>".number_format($summary['client'][$type]['host_fee_per_month'],2,".",",")."</td>
								<td class='foot right'></td>
								<td class='foot right'>".number_format($summary['client'][$type]['total_lic_fee'],2,".",",")."</td>
								<td class='foot total'>".number_format($summary['client'][$type]['total_cost'],2,".",",")."</td>
							</tr>";
				}
			}
			echo "</tbody>
			</table>
			<h2>Summary per Module</h2>
			<table class=res_sum>
				<thead>
					<tr>
						<th>Module</th>
						<th>Clients</th>
						<th>Total<br />Licenses</th>
						<th>Total License<br />Fee*</th>
						<!-- <th>Average<br />License Fee*</th> -->
					</tr>
				</thead><tbody>";
			$type_cmp = array("bill","non");
			$type_mod = array("bill","non");
			foreach($type_cmp as $cmp_type) {
				$cmp_title = ($cmp_type=="bill" ? "Billable " : "Non-Billable")." Clients";
				foreach($type_mod as $type) {
					if(count($res_total['mods'][$cmp_type][$type])>0) {
						$mod_title = $type=="bill" ? "Billable Modules" : "Non-Billable Modules";
						echo "<tr><td class=head colspan=5>".$mod_title." for ".$cmp_title."</td></tr>";
						foreach($res_total['mods'][$cmp_type][$type] as $m) {
							$summary['mods'][$cmp_type][$type]['clients']+=$m['clients'];
							$summary['mods'][$cmp_type][$type]['total_lic']+=$m['total_lic'];
							$summary['mods'][$cmp_type][$type]['total_lic_fee']+=$m['total_lic_fee'];
							echo "
							<tr>
								<td class=b>".$m['name']."</td>
								<td class=center>".$m['clients']."</td>
								<td class=center>".$m['total_lic']."</td>
								<td class=total>".($type=="bill" ? number_format($m['total_lic_fee'],2) : "N/A")."</td>
							<!--	<td class=right>".(number_format(($m['total_lic_fee']/$m['total_lic']),2))."</td> -->
							</tr>";
						}
						echo "
							<tr>
								<td class='foot right'>Total ".$mod_title." for ".$cmp_title.":</td>
								<td class='foot'></td>
								<td class='foot center'>".$summary['mods'][$cmp_type][$type]['total_lic']."</td>
								<td class='foot total'>".($type=="bill" ? number_format($summary['mods'][$cmp_type][$type]['total_lic_fee'],2) : "N/A")."</td>
							<!--	<td class='foot right'>".number_format($summary['mods'][$cmp_type][$type]['total_lic_fee']/$summary['mods'][$cmp_type][$type]['total_lic'],2)."</td> -->
							</tr>";
					}
				}
			}
			echo "</tbody></table>";
			$btype = "bill";
			$ntype = "non";
		echo "<h2>Grand Total</h2>
			<table class=res_sum>
			<thead>
				<tr>
					<th></th>
					<th>Billable<br />Clients</th>
					<th width=10></th>
					<th>Non-Billable<br />Clients</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class='' width=100>Hosting fee:*</td>
					<td class=right style='padding-left: 30px;'>".number_format($summary['client'][$btype]['host_fee_per_month'],2,".",",")."</td>
					<td width=10></td>
					<td class=right style='padding-left: 30px;'>".number_format($summary['client'][$ntype]['host_fee_per_month'],2,".",",")."</td>
				</tr>
				<tr>
					<td class=''>License fee:*</td>
					<td class=right style='padding-left: 30px;'>".number_format($summary['client'][$btype]['total_lic_fee'],2)."</td>
					<td width=10></td>
					<td class=right style='padding-left: 30px;'>".number_format($summary['client'][$ntype]['total_lic_fee'],2)."</td>
				</tr>
			</tbody><tfoot>
				<tr>
					<td class=right>Total:*</td>
					<td class=right style='padding-left: 30px;'>".number_format(($summary['client'][$btype]['host_fee_per_month']+$summary['client'][$btype]['total_lic_fee']),2)."</td>
					<td width=10></td>
					<td class=right style='padding-left: 30px;'>".number_format(($summary['client'][$ntype]['host_fee_per_month']+$summary['client'][$ntype]['total_lic_fee']),2)."</td>
				</tr>
			</tfoot>
			</table>
			<p>* Please note that all prices exclude VAT, SMSs and Data Storage.</p>
			";
	}
} //end foreach reseller
//arrPrint($res_total);
?>
</body></html>
<?php
$output = ob_get_contents();
ob_end_flush();
$output = getOutputHeaderWithNoHTTPS().$output;

		$filename = $folder."billing_".date("Ymd")."_".date("His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$output."\n");
        fclose($file);



$sql = "INSERT INTO assist_billing_report_history (date_drawn, drawn_by, filename, request) VALUES (now(), '".strtoupper($_SESSION['cc'])."', '$filename', '".serialize($_REQUEST)."')";
$hist_id = Adb_insert($sql);

$a_sql = "INSERT INTO assist_billing_report_company_history (cmpcode, by_cmpcode, by_tkid, date_drawn, details, modules, hist_id) VALUES ";
$b_sql = array();
foreach($history as $h) {
	$b_sql[] = $h;
	if(count($b_sql)>=20) {
		$sql = $a_sql.implode(",".$hist_id."), ",$b_sql).",".$hist_id.")";
		Adb_insert($sql);
		$b_sql = array();
	}
}
if(count($b_sql)>0) {
	$sql = $a_sql.implode(",".$hist_id."), ",$b_sql).",".$hist_id.")";
	Adb_insert($sql);
	$b_sql = array();
}

?>