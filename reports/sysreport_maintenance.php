<?php
error_reporting(0);

include("../module/autoloader.php");

$interval = 18;

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader();

//$sql = "SELECT * FROM assist_backup WHERE status = 2 AND datetime > DATE_SUB(CURDATE(), INTERVAL 1 YEAR) ORDER BY datetime DESC";
$d = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),1,date("Y")-1));

$sql = "SELECT * FROM assist_maintenance_log WHERE status & 2 = 2 AND start_time > DATE_SUB(CURRENT_DATE(), INTERVAL $interval MONTH) AND start_time < CURRENT_DATE() ORDER BY start_time DESC";
$logs = $db->mysql_fetch_all($sql);


?>
<h1>System Maintenance Log Report</h1>
<table>
	<tr>
		<th>Maintenance<br />Type</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Duration</th>
	</tr>
<?php
	foreach($logs as $log) {
		$duration = strtotime($log['end_time']) - strtotime($log['start_time']);
		$duration = round(($duration / 60),2);
		echo "
		<tr>
			<td>".ucwords(strtolower($log['maintenance_type']))."</td>
			<td>".date("d M Y H:i",strtotime($log['start_time']))."</td>
			<td>".date("d M Y H:i",strtotime($log['end_time']))."</td>
			<td>".($duration)." minutes</td>
		</tr>";
	}
?>
</table>
<p class=i style='font-size: 100%'>Report drawn on <?php echo date("d F Y"); ?> at <?php echo date("H:i:s"); ?>.</p>
<script type="text/javascript">
$(function() {
});
</script>