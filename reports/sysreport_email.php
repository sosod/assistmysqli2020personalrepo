<?php
require_once("../module/autoloader.php");
$emailObject = new ASSIST_EMAIL();
$me = new ASSIST_MODULE_HELPER();
$db = new ASSIST_DB("master");
$me->displayPageHeaderJQ();
$cc = strtolower($me->getCmpCode());
?>
<style type="text/css" >
	@media print
	{
		.no-print, .no-print *
		{
			display: none !important;
		}
	}
</style>
<h1>System Email Log Report</h1>
<p class="no-print"><input type=button value=Filter id=btn_filter /></p>
<?php


$sql = "SELECT MIN(`dt`) as mindate FROM `assist_email_log_internal` L WHERE cmpcode = '".$cc."'";
$first_date = $db->mysql_fetch_one_value($sql,"mindate");
if(strlen($first_date)==0 || strtotime($first_date)==0) {
	$min_date = date("Y").", ".date("m").", ".date("d");
} else {
	$min_date = date("Y",strtotime($first_date)).", ".date("m",strtotime($first_date)).", ".date("d",strtotime($first_date));
}

$sql = "SELECT * FROM assist_menu_modules ORDER BY modtext";
$modules = $me->mysql_fetch_all_by_id($sql,"modref");
$system_modules = $me->getAdminSystemMenu();
$modules['TK'] = array(
		'modref'=>"TK",
		'modtext'=>$system_modules['TK']
);


$sql = "SELECT DISTINCT modref FROM `assist_email_log_internal` L WHERE cmpcode = '".$cc."'";
$mods_with_emails = $db->mysql_fetch_all_by_value($sql,"modref");


	$show_msg = isset($_REQUEST['show_msg']) && $_REQUEST['show_msg']=="Y" ? true : false;
$logs = array();
$pwd_records = array();
if(isset($_REQUEST['action']) && $_REQUEST['action']=="GENERATE") {
	$generate_report=true;


	$sql = "SELECT tkid as id, CONCAT(tkname,' ',tksurname) as name FROM assist_".$cc."_timekeep";
	$users = $me->mysql_fetch_value_by_id($sql, "id", "name");

	$modref = $_REQUEST['modref'];
	$date_from = $_REQUEST['from'];
	$date_to = $_REQUEST['to'];

	$sql = "SELECT * FROM assist_email_log_internal WHERE cmpcode = '".$cc."'";
	if($modref!="X") {
		$sql.= " AND modref = '".$modref."'";
	}
	if(strlen($date_from)>0) {
		$sql.= " AND dt >= '".date("Y-m-d",strtotime($date_from))." 00:00:00'";
	}
	if(strlen($date_to)>0) {
		$sql.= " AND dt <= '".date("Y-m-d",strtotime($date_to))." 23:59:59'";
	}
	$logs = $db->mysql_fetch_all($sql);
/* put on hold - struggling to match smtp_ids with processed list
	$log_ids = array_keys($logs);

	$sql = "SELECT smtp_id, final_result FROM assist_email_log_sendgrid_processed WHERE smtp_id IN ('<".implode(">','<",$log_ids).">')";
	$log_results = $db->mysql_fetch_value_by_id($sql, "smtp_id", "final_result");
*/
} else {
	$generate_report=false;




}
//ASSIST_HELPER::arrPrint($logs);
//ASSIST_HELPER::arrPrint($pwd_records);

?>
<table id=tbl_log>
	<tr>
		<th>Date Sent</th>
		<th>Module</th>
		<th>Recipient Email</th>
		<!-- <th>Recipient User</th> -->
		<th>Subject</th>
		<?php if($show_msg) { echo "<th>Message</th>"; } ?>
<!--		<th>Result</th>-->
	</tr>
	<?php
	if(count($logs)>0) {
		foreach($logs as $smtp_id => $eml) {
			/*if($eml['tkid']=="AUTO") {
				$rec_tkid = "User ID not logged";
			} else {
				$rec_tkid = $eml['rec_tkid'];
				if(strlen($rec_tkid) == 0 || $rec_tkid * 1 == 0) {
					$rec_tkid = "User ID not logged";
				} else {
					if(strlen($rec_tkid) == 1) {
						$rec_tkid = "000".$rec_tkid;
					} elseif(strlen($rec_tkid) == 2) {
						$rec_tkid = "00".$rec_tkid;
					} elseif(strlen($rec_tkid) == 3) {
						$rec_tkid = "0".$rec_tkid;
					}
					if(isset($users[$rec_tkid])) {
						$rec_tkid = $users[$rec_tkid];
					} else {
						$rec_tkid = "Unknown User ($rec_tkid)";
					}
				}
			}*/
			if($show_msg) {
				if($eml['modref']!="TK") {
					$m = base64_decode($eml['body']);
					if(strpos($m,"<html>")===false) {
						$m = str_replace(chr(10),"<br />",$m);
					}
					$message = $m;
				} else {
					$message = "<i>Message content withheld for security reasons.</i>";
				}
			}
			echo "
	<tr>
		<td>".$eml['dt']."</td>
		<td>".$modules[$eml['modref']]['modtext']."</td>
		<td>".base64_decode($eml['recipients'])."</td>
		<td>".base64_decode($eml['subject'])."</td>";
		if($show_msg) {
			echo "<td>".$message."</td>";
		}
	echo "</tr>";
//		<td>".$rec_tkid."</td>
//		<td>".(isset($log_results['<'.$smtp_id.'>'])?$log_results['<'.$smtp_id.'>']:"N/A")."</td>
		}



	} elseif($generate_report) {
		echo "
		<tr>
			<td colspan=11>No logs were found for the filter settings you selected.</td>
		</tr>";
	} else {
		echo "
		<tr>
			<td colspan=11>Please apply a filter in order to view the Activity Logs.</td>
		</tr>";
	}
	?>
</table>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>
<div id=div_filter title=Filter>
	<form name=frm_filter>
		<input type="hidden" name=action value=GENERATE />
		<table id=tbl_filter class=form width="400px">
			<tr>
				<th>Module:</th>
				<td><select id="modref" name="modref">
<option value="X" selected><?php echo $me->getUnspecified(); ?></option>
						<?php
						foreach($modules as $mod) {
							if(in_array($mod['modref'],$mods_with_emails)) {
								echo "<option value='".$mod['modref']."'>".$mod['modtext']."</option>";
							}
						}
						?>
					</select></td>
			</tr>
			<tr>
				<th>Date:</th>
				<td>From <input type=text class="datepicker" size=12 name=from value="<?php echo date("d-M-Y",strtotime("1 July ".(date("Y")-1)." 12:00:00")-365*24*60*60); ?>" /> to <input type=text name=to class="datepicker" value='<?php echo date("d-M-Y"); ?>' size=12 /></td>
			</tr>
			<tr>
				<th>Message:</th>
				<td><input type="checkbox" value="Y" name="show_msg" id="show_msg" /> Show message content</td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
	$(function(){
		$("#div_filter").dialog({
			modal:true,
			width:"500px",
			autoOpen:<?php echo ($generate_report ? "false" : "true"); ?>,
			buttons:[{
				text:"Apply",
				click:function(){
					AssistHelper.processing();
					document.location.href = 'sysreport_email.php?'+AssistForm.serialize($("form[name=frm_filter]"));
					$(this).dialog("close");
				}
			}]
		}).dialog("widget").find(".ui-dialog-buttonpane button").eq(0).css(AssistHelper.getGreenCSS()).end();
		$("#div_filter .datepicker").datepicker({
			showOn: 'both',
			buttonImage: '/library/jquery/css/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'dd-M-yy',
			changeMonth:true,
			changeYear:true,
			maxDate: 0,
			minDate: new Date(<?php echo $min_date; ?>)
		});
		$("#btn_filter").button().css(AssistHelper.getGreenCSS()).css("font-size","80%").click(function(){
			$("#div_filter").dialog("open");
			$("#div_filter input").blur();

		});
		$("#modref").change(function() {
			var v = $("#modref").val();
			if(v=="TK") {
				$("#show_msg").prop("checked","").prop("disabled",true).parent().css("color","#ababab").css("background-color","#cccccc");
			} else {
				$("#show_msg").prop("disabled",false).parent().css("color","#000").css("background-color","");
			}
		});
	});
</script>
</body>

</html>
