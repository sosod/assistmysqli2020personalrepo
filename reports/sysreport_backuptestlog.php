<?php
//error_reporting(-1);

include("../module/autoloader.php");

$interval = 18;

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader();

//$sql = "SELECT * FROM assist_backup WHERE status = 2 AND datetime > DATE_SUB(CURDATE(), INTERVAL 1 YEAR) ORDER BY datetime DESC";
$d = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),1,date("Y")-1));

//$sql = "SELECT * FROM assist_backup_test WHERE status & 2 = 2 AND datetime > DATE('$d') ORDER BY datetime DESC";
$sql = "SELECT * FROM assist_backup_test WHERE status & 2 = 2 AND datetime > DATE_SUB(CURRENT_DATE(), INTERVAL $interval MONTH) ORDER BY datetime DESC";
$logs = $db->mysql_fetch_all($sql);


?>
<h1>System Backup Restoration Test Report</h1>
<p>Action iT Assist backup restoration test log for the previous <?php echo $interval; ?> months as per the Action iT Assist Backup Policy (<a href='Action iT Assist Backup Policy 2018-07-01 published.pdf' target=_blank>Link</a>).</p>
<table>
	<tr>
		<th colspan=3>Backup Restoration Tested</th>
		<th colspan=2>Test Details</th>
	</tr>
	<tr>
		<th width=20%>Server Tested</th>
		<th width=20%>Date of Backup</th>
		<th width=20%>Type of Backup</th>
		<th width=20%>Date of Test</th>
		<th width=20%>Test Result</th>
	</tr>
<?php
	foreach($logs as $log) {
		switch(trim(strtolower($log['result']))) {
			case "ok":
				$icon = "ok";
				$log['message'] = "Successful";
				break;
			case "info":
				$icon = "info";
				$log['message'] = "Warnings";
				break;
			default:
				$icon = "error";
				$log['message'] = "Errors";
				break;
		}
		echo "
		<tr>
			<td>".date("d M Y",strtotime($log['date_tested']))."</td>
			<td>".$helper->getServerName()."</td>
			<td>".ucwords(strtolower($log['backup_type']))." (".ucwords(strtolower($log['backup_freq'])).")</td>
			<td>".date("d M Y",strtotime($log['datetime']))."</td>
			<td>".($helper->getDisplayIcon($icon))." <span>".($log['message'])."</span></td>
		</tr>";
	}
?>
</table>
<p class=i style='font-size: 100%'>Report drawn on <?php echo date("d F Y"); ?> at <?php echo date("H:i:s"); ?>.</p>
<script type="text/javascript">
$(function() {
	$("span").css("display","inline");
	//$("span").css("border","1px dashed #fe9900");
	$("td").find("span:first").css("padding-left","13px");
});
</script>