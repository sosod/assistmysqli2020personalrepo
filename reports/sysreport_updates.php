<?php
//error_reporting(-1);

include("../module/autoloader.php");

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader();

//$my_date = strtotime(date("d F Y H:i:s"));

//first day of the month
$day = 1;
//current month rolled back 6 months
$month = date("m")-6;
//year = current year rolled back to last year & rolled back an additional year if month roll back went below 0
$year = date("Y")-1+(($month<0) ? (-1) : 0);
//if month roll back went below 0 then add back 12 months to get correct month for 18 months from today
$month = ($month<0) ? $month+12 : $month;

$d = date("Y-m-d H:i:s",mktime(0,0,0,$month,$day,$year));
//echo $d;
$sql = "SELECT * FROM assist_updates WHERE active>0 AND udate > DATE('$d') ORDER BY udate DESC"; //echo $sql;
$logs = $db->mysql_fetch_all($sql);

//$helper->arrPrint($logs);

?>
<h1>System Updates Report</h1>
<p class=i>This report indicates the updates made to the system during the past 18 months.</p>
<table>
	<tr>
		<th>Date</th>
		<th>Module</th>
		<th>Update</th>
	</tr>
<?php
if(count($logs)>0) {
	foreach($logs as $log) {
		echo "
		<tr>
			<td>".date("d M Y",strtotime($log['udate']))."</td>
			<td>".$log['umodule']."</td>
			<td>"."<span>".str_replace(chr(10),"<br />",$log['utext'])."</span></td>
		</tr>";
	}
} else {
	echo "
	<tr>
		<td colspan=3>No update logs found to display.</td>
	</tr>";
}
	
?>
</table>
<p>Report drawn at <?php echo date("d F Y H:i"); ?>.</p>
<script type="text/javascript">
$(function() {
});
</script>