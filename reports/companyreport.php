<?php 
require_once '../header.php';
/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);


$totact = 0;
$totinact = 0;


?>
<style type=text/css>
.reseller {
	background-color: #555555;
	color: #ffffff;
	padding: 5px;
}
h2.res { color: #555555; }
th.res { background-color: #555555; text-align: left; }

</style>
<h1>Module License Report</h1> 
<p style="margin-top: -10px;"><i><small>Report drawn at <?php echo(date("d F Y H:i")); ?></small></i></p>
<p>Note: This summary automatically excludes the User Directory module and the Assist Administrator and Support user.</p>
<?php
$company = array();
$multiple_resellers = false;
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" */
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT cmpcode, cmpreseller, cmpname, cmpstatus FROM assist_company WHERE cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S')";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S')
				WHERE (c1.cmpstatus = 'Y'  OR c1.cmpstatus = 'S') ".($res[1]!="ALLCMP"&&$res[1]!="DEMO" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
		$multiple_resellers = true;
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT cmpcode, cmpreseller, cmpname, cmpstatus FROM assist_company WHERE cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND cmpcode NOT IN ('IASSIST','BLANK') ORDER BY cmpreseller, cmpcode, cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}

//DISPLAY COMPANIES
$total = array('cmpcount'=>0,'act'=>0,'inact'=>0);
foreach($resellers as $rcode => $res) {
	if($multiple_resellers) {
		echo "
		<h2 class=reseller>".$res['cmpname']."</h2>";
	}
	
	$res_company = $company[$rcode];
	$res_act = 0;
	$res_inact = 0;
	$res_cmpcount = 0;
	foreach($res_company as $cmp) {
		$act = 0;
		$inact = 0;
		$cmpcode = strtolower($cmp['cmpcode']);
		if(in_array($db_other.$cmpcode,$dbs)) {
			$res_cmpcount++;
			//GET MODULES
			$modules = array();
			$inactive = $rcode=="IGN0001" ? true : false;
			$sql = "SELECT modtext, modref, modlocation, tkstatus, count(DISTINCT usrtkid) as c 
					FROM assist_menu_modules 
					INNER JOIN assist_".$cmpcode."_menu_modules_users  
					  ON usrmodref = modref AND usrmodref <> 'TD'  
					INNER JOIN assist_".$cmpcode."_timekeep 
					  ON usrtkid = tkid AND tkstatus <> 0 AND tkid <> '0000' AND (tkuser <> 'support') 
					WHERE modyn ='Y' OR modadminyn = 'Y' 
					GROUP BY modref, tkstatus ";
			$rs = mysql_getRS($sql,"O",$cmpcode);
				while($row = mysql_fetch_array($rs)) { 
					$modref = $row['modref'];
					$modules[$modref]['modref'] = $modref;
					$modules[$modref]['modtext'] = $row['modtext'];
					if($row['tkstatus']!=2 || $modref=="PMSPS") {
						$modules[$modref]['tk'.$row['tkstatus']] = $row['c'];
						if($modref=="PMSPS") { $inactive=true; }
					}
				}
			unset($rs);
			
			echo "<h2>".$cmp['cmpname']." (".strtoupper($cmpcode).")</h2>
			<p class=i style='margin-top: -5px;font-size:85%;line-height:110%'>Reseller: ".$res['cmpname']." | Company Status: ".($cmp['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active")."</p>
			<table width=650>
				<tr>
					<th>Module</th>
					<th>Licenses<br />Available</th>
					<th>Licenses<br />In Use</th>
					".($inactive ? "<th>Active<br />Users</th><th>Inactive<br />Users</th>" :"")."
					<th>Remaining<br>Licenses</th>
				</tr>";
			//GET AVAILABLE LICENSES
			$lic = array();
			$sql = "SELECT m.modref, mm.moduserallowed FROM assist_menu_modules m INNER JOIN assist_".$cmpcode."_menu_modules mm ON m.modid = mm.modmenuid WHERE modyn = 'Y' OR modadminyn = 'Y'";
			$rs = mysql_getRS($sql,"O",$cmpcode);
			while($row = mysql_fetch_array($rs)) {
				$lic[$row['modref']] = $row['moduserallowed'];
			}
			unset($rs);

			foreach($modules as $mod) {
				$modtext = $mod['modtext'];
				$modref = $mod['modref'];
				$totact+=$mod['tk1'];
				$totinact+=$mod['tk2'];
				$act+=$mod['tk1'];
				$inact+=$mod['tk2'];

				if(isset($lic[$modref])) {
					$modlic = $lic[$modref];
					if($modlic > 0) {
						$remlic = $modlic - ($mod['tk1']+$mod['tk2']);
						if($remlic < 0) { $remlic = "<div class=ui-state-error>ERROR</div>"; }
					} else {
						$modlic = "Unlimited";
						$remlic = "Unlimited";
					}
				} else {
					$modlic = "Unlimited";
					$remlic = "Unlimited";
				}
				
				echo "
				<tr>
					<td>$modtext</td>
					<td class=center>$modlic</td>
					<td class=center>".($mod['tk1']+$mod['tk2'])."</td>
					".($inactive ? "<td class=center>".($mod['tk1'])."</td><td class=center>".(isset($mod['tk2']) ? $mod['tk2'] : "-")."</td>" : "")."
					<td class=center>$remlic</td>
				</tr>";	
			}
			echo "
			<tr>
				<td class=b>Total Licences:</td>
				<td></td>
				<td class='b center'>".number_format($act+$inact,0)."</td>
				".($inactive ? "<td class='b center'>".number_format($act,0)."</td><td class='b center'>".number_format($inact,0)."</td>" : "")."
				<td></td>
			</tr>
			</table>";
			$res_act+=$act; $res_inact+=$inact;
		} //if valid db
	} //end foreach res_company
	echo "
	<h2 class=res>Total for ".$res['cmpname']."</h2>
	<table width=250>
		<tr>
			<th class=res width=175>Active Companies:</td>
			<td width=75 class=res>".number_format($res_cmpcount,0)."</td>
		</tr>".($inactive ? "<tr>
			<th class=res>Active Licenses:</td>
			<td>".number_format($res_act,0)."</td>
		</tr><tr>
			<th class=res>Inactive Licenses:</td>
			<td>".number_format($res_inact,0)."</td>
		</tr>" : "")."<tr>
			<th class=res>Total Licenses In Use:</td>
			<td class=b>".number_format($res_act+$res_inact,0)."</td>
		</tr>
	</table>";
	$total['cmpcount']+=$res_cmpcount;
	$total['act']+=$res_act;
	$total['inact']+=$res_inact;
}//end foreach reseller
if(count($resellers)>1) {
	echo "
	<h2 class=reseller>Grand Total</h2>
	<table width=250>
		<tr>
			<th class=left width=175>Active Companies:</td>
			<td width=75>".number_format($total['cmpcount'],0)."</td>
		</tr><tr>
			<th class=left>Active Licenses:</td>
			<td>".number_format($total['act'],0)."</td>
		</tr><tr>
			<th class=left>Inactive Licenses:</td>
			<td>".number_format($total['inact'],0)."</td>
		</tr><tr>
			<th class=left>Total Licenses In Use:</td>
			<td class=b>".number_format($total['act']+$total['inact'],0)."</td>
		</tr>
	</table>";
}
?>
<p>&nbsp;</p>
</body>

</html>
