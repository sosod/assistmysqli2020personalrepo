<?php
//error_reporting(-1);

include("../module/autoloader.php");

$interval = 18;
$max_days_to_display = 545;
$now = strtotime(date("d F Y")." 23:59:59");//-(24*60*60);
//--start date moved below stats_dates calculation to match up start with max stats date; //$start_date = $now-($max_days_to_display*24*60*60);//strtotime("13 October 2016 12:00:00");

//7 days, 1 month, 3 months, 6 months, 1 year, 18 months
$stats = array(
	'7'=>array(),
	'30'=>array(),
	'90'=>array(),
	'180'=>array(),
	'365'=>array(),
	'545'=>array(),
);
$stats_names = array(
	'7'=>"Past 7 days",
	'30'=>"Past month",
	'90'=>"Past 3 months",
	'180'=>"Past 6 months",
	'365'=>"Past year",
	'545'=>"Past 18 months",
);
$stats_dates = array(
	'7'=>0,
	'30'=>0,
	'90'=>0,
	'180'=>0,
	'365'=>0,
	'545'=>0,
);

foreach($stats as $key => $s) {
	$stats[$key] = array(
		'outages'=>0,
		'downtime'=>0,
	);
	switch($key) {
		case 30:
		case 90:
		case 180:
			$months = $key/30;
			$dt = mktime(date("H",$now),date("i",$now),date("s",$now),date("n",$now)-$months,date("j",$now),date("Y",$now))+1;
			$stats_dates[$key] = $dt;
			break;
		case 545:
			$months = 6;
			$year = date("Y",$now)-1;
			$dt = mktime(date("H",$now),date("i",$now),date("s",$now),date("n",$now)-$months,date("j",$now),$year)+1;
			$stats_dates[$key] = $dt;
			break;
		default:	//7 & 365
			$stats_dates[$key] = $now-($key*24*60*60)+1;
			break;
	}
}

$start_date = $stats_dates[$max_days_to_display];

$me = new ASSIST();

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader();

$device = "Assist (ActionAssist)";
$server_name = "ASSIST01";

//$sql = "SELECT * FROM assist_backup WHERE status = 2 AND datetime > DATE_SUB(CURDATE(), INTERVAL 1 YEAR) ORDER BY datetime DESC";
$d = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),1,date("Y")-1));

//$sql = "SELECT * FROM assist_alertra_checks_log WHERE dt > DATE_SUB(CURRENT_DATE(), INTERVAL $interval MONTH) ORDER BY dt DESC";
//$sql = "SELECT * FROM assist_alertra_checks_log WHERE outages > 0 ORDER BY dt DESC";

//$logs = $db->mysql_fetch_all_by_id($sql,"dt");

$sql = "SELECT * FROM assist_alertra_log WHERE (log_device = '$device' OR log_device = '$server_name') ORDER BY log_date ASC";
$rows = $db->mysql_fetch_all($sql);
//ASSIST_HELPER::arrPrint($rows);
//die();
$logs = array();
$down = 0;
$up = 0;
foreach($rows as $r) {
// //	$l = unserialize($r['log_data']);
	$l = $r['log_data'];
	//old log format
	if(strpos($l,"EventTimestamp")!==false) {
		$l2 = explode("EventTimestamp",$l);
		$l3 = explode('"',$l2[1]);
//ASSIST_HELPER::arrPrint($l3);
		$et = $l3[2];
// //	$et = $l['EventTimestamp'];
		$dt = explode(" ",$et);
		$d = explode("/",$dt[0]);
		$mon = $d[0];
		$day = $d[1];
		$year = $d[2];
		$edt = strtotime($year."/".$mon."/".$day." ".$dt[1]);
	} else {
	//new log format
		$l2 = unserialize($l);
		$edt = $l2['ts'];
	}
	while(isset($logs[$edt])) {
		$edt++;
	}
	$logs[$edt] = $r['log_type'];
}

//ASSIST_HELPER::arrPrint($logs); die("end test");
$final = array();
$current_status = "UP";
$down_time = 0;
foreach($logs as $ts => $new_status) {
	if($new_status == $current_status) {
		//do nothing
	} else {
		switch($new_status) {
			case "DOWN":
				$down_time = $ts;
				$current_status = $new_status;
				break;
			case "UP":
				if($down_time>0) {
					$seconds_down = $ts - $down_time;
					$my_time = date("Y-m-d",$ts);
					if(isset($final[$my_time])) {
						$final[$my_time]['outages']++;
						$final[$my_time]['down']+=$seconds_down;
					} else {
						$final[$my_time] = array(
							'outages'=>1,
							'down'=>$seconds_down,
						);
					}
					$down_time = 0;
					$current_status = $new_status;
				}
				break;
			default:
				break;
		}
	}
}

//ASSIST_HELPER::arrPrint($final);
// echo $now." = ".date("Y-m-d",$now);
//die();
?>
<h1>System Uptime / Outages Report</h1>
<ul>
	<li>Since 13 October 2016, the <?php echo $me->getSiteName(); ?> system has been remotely monitored by <a href="http://www.alertra.com">Alertra</a>, an independent website monitoring service.</li>
	<li>Alertra uses multiple monitoring nodes situated in various locations in North America, Europe and Asia/Pacific regions to continuously check on the status of the <?php echo $me->getSiteName(); ?> system.  Each region has multiple monitoring nodes to minimise any risk of failure on the Alertra side and to increase the probabilty of early detection of any failure of the <?php echo $me->getSiteName(); ?> system.</li>
	<li>In the event that 3 consecutive monitoring node checks fail an outage event is declared and Action iT IT Personnel are immediately notified of the failure so that it can be attended to and resolved as quickly as possible.  </li>
	<li>In the event of an outage, Alertra will automatically update the System Uptime / Outages Report as soon as the Assist system is back up and is responding to checks from the Alertra monitoring nodes.  This happens automatically without any action required by Action iT IT personnel.</li>
</ul>
<p>Please note that this report does <u>not</u> include scheduled downtime due to system maintenance.  It only includes unscheduled outages.</p>
<table id=tbl_stats class=float>
	<tr>
		<th>Uptime Statistics</th>
		<th>Outages</th>
		<th>Downtime</th>
		<th>Uptime</th>
	</tr>
<?php
foreach($stats_names as $key => $n) {
	echo "<tr>
			<td class=b>".$n." [Since: ".date("d M Y",$stats_dates[$key])."]</td>
			<td id=td_".$key."_out></td>
			<td id=td_".$key."_down></td>
			<td id=td_".$key."_up></td>
		</tr>";
}
?>
</table>
<table>
	<tr>
		<th width=25%>Date</th>
		<th width=25%>Outages</th>
		<th width=25%>Downtime</th>
		<th width=25%>Uptime</th>
	</tr>
<?php
$decimal_places = 3;
	$total_time = 24*60;
	$c = 0;
	$total_down = 0;
	$total_up = 0;
	$total_out = 0;
//	for($x=$start_date;$x<=$now;$x+=(60*60*24)) {
	for($x=$now;$x>=$start_date;$x-=(60*60*24)) {
		//$outages = isset($logs[date("Y-m-d",$x)]['outages']) ? $logs[date("Y-m-d",$x)]['outages'] : "0";
		//$outage_time = isset($logs[date("Y-m-d",$x)]['outages_minutes']) ? $logs[date("Y-m-d",$x)]['outages_minutes'] : 0;
		//$down_time = round(($outage_time / $total_time * 100),2);
		$outages = isset($final[date("Y-m-d",$x)]) ? $final[date("Y-m-d",$x)]['outages'] : 0;
		$down_time = isset($final[date("Y-m-d",$x)]) ? $final[date("Y-m-d",$x)]['down'] : 0;		//in seconds
		$up_time = (((24*60*60)-$down_time)/(24*60*60))*100; //100;//(100-$down_time);
		if($up_time!=100) {
			$up_time = round($up_time,$decimal_places);
		}
		if($outages==0) {
			$icon = "ok";
		} else {
			$icon = "error";
		}

		$down_sec = $down_time%60;
		$down_min = round(($down_time-$down_sec)/60,0);
		if($down_min>60) {
			$down_min2 = $down_min%60;
			$down_hr = round(($down_min-($down_min2))/60,0);
			$down_format = $down_hr."h ".$down_min2."m ".$down_sec."s";
		} else {
			$down_format = $down_min."m ".$down_sec."s";
		}


		$c++;
		$total_down+=$down_time;
		$total_up+=$up_time;
		$total_out+=$outages;
		echo "
		<tr>
			<td>".date("d M Y",$x)."</td>
			<td class=center>"." <span>".$outages."</span></td>
			<td class=right>".($outages>0?$helper->getDisplayIcon($icon):"")." <span>".$down_format."</span></td>
			<td class=right>".($outages==0?$helper->getDisplayIcon($icon):"")." <span>".$up_time."%</span></td>
		</tr>";

		//stats
		foreach($stats as $key => $s) {
			if($x>$stats_dates[$key]) {
				$stats[$key]['outages']+=$outages;
				$stats[$key]['downtime']+=$down_time;
			}
		}

	}
	//$total_down = round($total_down/$c,2);
	$total_up = (((24*60*60*$c)-$total_down)/(24*60*60*$c))*100;//0;//100-$total_down;
	if($total_up!=100) {
		$total_up = round($total_up,$decimal_places);
	}

	$total_down_sec = $total_down%60;
	$total_down_min = round(($total_down-$total_down_sec)/60,0);
	$total_down = $total_down_min."m ".$total_down_sec."s";
		if($total_down_min>60) {
			$total_down_min2 = $total_down_min%60;
			$total_down_hr = round(($total_down_min-($total_down_min2))/60,0);
			$total_down = $total_down_hr."h ".$total_down_min2."m ".$total_down_sec."s";
		} else {
//			$down_format = $down_min."m ".$down_sec."s";
		}

	echo "
	<tr class=b>
		<td class=right>Total / Average:</td>
		<td class=center>$total_out</td>
		<td class=right>".$total_down."</td>
		<td class=right>".$total_up."%</td>
	</tr>";
	/*foreach($logs as $log) {
		$total_time = 24*60;
		$outage_time = $log['outages_minutes'];
		$down_time = $outage_time / $total_time * 100;
		$up_time = ($total_time - $outage_time) / $total_time * 100;
		echo "
		<tr>
			<td>".date("d M Y",strtotime($log['dt']))."</td>
			<td>".$log['outages']."</td>
			<td>".$down_time."%</td>
			<td>".$up_time."%</td>
		</tr>";
	}*/

?>
</table>
<p class=i style='font-size: 100%'>Report drawn on <?php echo date("d F Y"); ?> at <?php echo date("H:i:s"); ?>.</p>
<script type="text/javascript">
$(function() {
	$("span").css("display","inline");
	//$("span").css("border","1px dashed #fe9900");
	$("td").find("span:first").css("padding-left","13px");
	$("#tbl_stats tr td").not(".b").addClass("center");
<?php
foreach($stats as $key => $s) {
	$total_down = $s['downtime'];
	$total_down_sec = $total_down%60;
	$total_down_min = round(($total_down-$total_down_sec)/60,0);
	if($total_down_min>60) {
		$total_down_min2 = $total_down_min%60;
		$total_down_hr = round(($total_down_min-($total_down_min2))/60,0);
		$total_down = $total_down_hr."h ".$total_down_min2."m ".$total_down_sec."s";
	} else {
		$total_down = $total_down_min."m ".$total_down_sec."s";
	}
	if($s['downtime']==0) {
		$total_up = "100%";
	} else {
		$total_time = $now-$stats_dates[$key];
		$total_up = round(100-($s['downtime']/$total_time*100),$decimal_places)."%";
	}
	echo "$('#td_".$key."_out').html('".$s['outages']."');";
	echo "$('#td_".$key."_down').html('".$total_down."');";
	echo "$('#td_".$key."_up').html('".$total_up."');";

}
?>
});
</script>

<?php
//ASSIST_HELPER::arrPrint($stats);
?>