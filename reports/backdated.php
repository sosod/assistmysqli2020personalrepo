<?php 
error_reporting(-1);
$fixed_date = strtotime('1 August 2013 00:00:00');
require_once '../header.php';
require_once '../module/autoloader.php';
if(!isset($folder)) { $folder = "files/"; }
//ob_start();
$cc = isset($_REQUEST['cc']) ? $_REQUEST['cc'] : $_SESSION['cc'];
$tki = isset($_REQUEST['tki']) ? $_REQUEST['tki'] : $_SESSION['tid'];

$multi_mods = array('ACTION','DOC');
$multi_mods_names = array('ACTION'=>"Task Assist",'DOC'=>"Document Management");
?>
<style type=text/css>
//	table.noborder, td.noborder { border: 1px solid #009900; }
	table, table td, table th {
		border: 1px solid #555555;
	}
	p.note {
		font-style: italic;
		margin: -13px 0px 5px 10px;
		font-size: 7pt;
	}
	
	table.c_detail td.head, table.c_cost td.head {
		font-weight: bold;
	}
	table.c_detail, table.c_detail td {
		border: 0px solid #ffffff;
		vertical-align: bottom;
	}
	table.c_modules {
		margin-top: 15px;
	}
	table.c_modules tfoot td, table.c_cost tfoot td { 
		font-weight: bold; 
		background-color: #dedede;
	}
	table.c_modules th {
		background-color: #cccccc;
		color: #000000;
		border: 1px solid #555555;
	}
	table.c_modules tbody td.foot {
		font-weight: bold;
		background-color: #dedede;
	}
	table.c_modules tbody th.res {
		font-weight: bold;
		text-align: left;
		background-color: #efefef;
	}
	
	
	
	h2.matrix, h3 {
		font-size:11pt;
		font-family:Arial;
		margin-top:0px;
	}
	h3 {
		margin-top: 20px;
		margin-bottom: 5px;
	}
	p.matrix{
		font-style: italic;
		font-size:7pt;
		font-family:Arial;
		margin:-15px 0px 0px 5px;
	}
	table.matrix {
		border: 1px solid #cdcdcd;
	}
	table.matrix td {
		padding:1px 3px 1px 10px;
		font-size:7pt;
		color: #777777;
		border: 1px solid #cdcdcd;
	}
	table.matrix th {
		padding:2px 4px 2px 4px;
		font-size: 7pt;
		background-color:#ababab;
		color: #777777;
		border: 1px solid #cdcdcd;
	}
	table.matrix th.res {
		padding:2px 3px 2px 5px;
		font-size:7pt;
		background-color:#dedede;
		color: #777777;
		text-align:left;
		border: 1px solid #cdcdcd;
	}
	
	table.res_sum td, table.res_sum th {
		font-size: 7.5pt;
	}
	
	table.res_sum tfoot td, table.res_sum tbody td.foot {
		font-weight: bold;
		background-color: #dedede;
	}
	table.res_sum tbody td.head {
		font-weight: bold;
		background-color: #ccccff;
		font-size: 8pt;
	}
	table.res_sum tbody td.total {
		font-weight: bold;
		background-color: #dedede;
		text-align: right;
	}
	table.res_sum tfoot td.total, table.res_sum tbody td.foot.total{
		font-weight: bold;
		background-color: #ababab;
		text-align: right;
	}
	
	div p {
		font-size: 6.5pt;
		line-height: 7.5pt;
		margin: 0 0 0 0;
		margin-top: 5px;
		margin-left: 1em;
		text-indent: -1em;
	}
</style>

<?php

/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);


$company = array();
$multiple_resellers = false;
$fields = "c1.cmpcode, c1.cmpreseller, c1.cmpname, c1.cmp_price_class, c1.cmp_is_csi, c1.cmp_bill_hosting";
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" */
$_REQUEST['cmpcode'] = "ALL_ALLCMP";
$res = explode("_",$_REQUEST['cmpcode']);
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT $fields FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND c2.cmpstatus = 'Y'
				WHERE c1.cmpstatus = 'Y' AND c1.cmp_is_demo = 0 AND c1.cmp_price_class <> 'X' ".($res[1]!="ALLCMP" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
		$multiple_resellers = true;
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND c1.cmpcode NOT IN ('IASSIST','BLANK') AND c1.cmpstatus = 'Y' AND c1.cmp_is_demo = 0 AND c1.cmp_price_class <> 'X' ORDER BY c1.cmpreseller, c1.cmpcode, c1.cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");

$total = array();
$history = array();
//DISPLAY COMPANIES
echo "<table><tr><th>Company</th><th>Total Historical Users</th><th>Active PMSPS Users</th><th>Inactive PMSPS Users</th></tr>";
foreach($resellers as $rcode => $res) {
	$res_company = $company[$rcode];
	echo "<tr><td colspan=4>".$rcode."</td></tr>";
	$res_total = array('mods'=>array(),'cmp_count'=>0,'cmp'=>array());
	foreach($res_company as $cmp) {
		$hist = array('modules'=>array(),'details'=>array());
		$cmpcode = strtolower($cmp['cmpcode']);
		if(in_array($db_other.$cmpcode,$dbs)) {
			$rep_date = isset($fixed_date) ? $fixed_date : $today;
//			$has_pmsps = false;
//			$cmp_type = $cmp['cmp_is_csi']==1 ? "non" : "bill";
//			$cmp_bill_hosting = $cmp['cmp_bill_hosting']==1 ? true : false;
//			$res_total['cmp_count']++;
			/*echo "
			<h1 ".(count($total)>0 ? "style='page-break-before: always;'" : "").">".$cmp['cmpname']." (".$cmp['cmpcode'].")</h1>
			<p class=note>Report generated on ".date("d F Y",$rep_date)." at ".date("H:i",$rep_date).".</p>
			<table class=noborder width=100%><tr>
				<td class=noborder>
				";*/
$db = new ASSIST_DB("client",$cmpcode);
$sql = "SELECT tkid, tkstatus, tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid <> '0000' AND tkuser <> 'support' AND (CONVERT(tkadddate,SIGNED) < ".$fixed_date." OR (tkstatus = 0 aND CONVERT(tktermdate,SIGNED) > ".$fixed_date." ))";
$users = $db->mysql_fetch_all_by_id($sql,"tkid");
//echo "<P>Total users at history date: ".count($users)."</p>";
$sql = "SELECT * FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = 'PMSPS' ORDER BY usrtkid";
$menu = $db->mysql_fetch_all_by_id($sql,"usrtkid");
//echo "<P>Total PMSPS users today: ".count($menu)."</p>";
$sql = "SELECT id, ref, action, old, new FROM assist_".$cmpcode."_timekeep_log  WHERE (action = 'G' AND new = 'PMSPS') OR (action = 'R' AND old = 'PMSPS')";
$l = $db->mysql_fetch_all($sql);
$logs = array();
foreach($l as $x) {
	$logs[$x['ref']][$x['id']] = $x;
}
$pu = array(1=>0,2=>0);
foreach($users as $i => $u) {
	if($u['tkstatus'] == 0) {
		if(strlen($u['tkemail'])>0) {
			$u['tkstatus'] = 1;
		} else {
			$u['tkstatus'] = 2;
		}
	}
	//arrPrint($u);
	$tu = $u['tkstatus'];
	$ti = $i*1;
	if(!isset($logs[$ti])) {
		if(isset($menu[$i])) {
			$pu[$tu]++;
	//		echo "<P class=isubmit>IS PMSPS</p>";
		} else {
//			echo "<P class=idelete>NOT PMSPS</p>";
		}
	} else {
		$l = $logs[$ti];
		krsort($l);
		foreach($l as $x) {
			if($x['action'] == "G") {
				$pms = false;
			} else {
				$pms = true;
			}
		}
		if($pms) {
			$pu[$tu]++;
		//	echo "<P class=isubmit>IS PMSPS</p>";
		} else {
	//		echo "<P class=idelete>NOT PMSPS</p>";
		}
	}
}
//arrPrint($pu);
echo "<tr><th class=left>".strtoupper($cmpcode)."</th><td class=center>".count($users)."</td><td class=center>".$pu[1]."</td><td class=center>".$pu[2]."</td></tr>";
		} //if valid db
	} //end foreach res_company
} //end foreach reseller
//arrPrint($res_total);
?>
</body></html>
<?php
//$output = ob_get_contents();
//ob_end_flush();
//$output = getOutputHeaderWithNoHTTPS().$output;


?>