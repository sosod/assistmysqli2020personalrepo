<?php 
require_once '../header.php';

if(!isset($report_folder)) { $report_folder = "files/"; }

$all_details = false;
if(isset($_REQUEST['jc']) && $_REQUEST['jc']=="Y") { $all_details = true; }

ob_start();



/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);


/* GET DOCUMENT STORAGE MODULES */
$docmods = array();
$doccode = array();
$sql = "SELECT * FROM assist_docmodules WHERE yn = 'Y' ORDER BY doclocation";
$rs = mysql_getRS($sql,"A");
	while($row = mysql_fetch_array($rs)) {
		$docmods[$row['doclocation']] = $row;
		$doccode[] = $row['doclocation'];
	}
unset($rs);



?>
<style type=text/css>
tr.reseller td {
	background-color: #555555;
	color: #ffffff;
	font-weight: bold;
}
tr.res_total td {
	background-color: #cccccc;
	color: #000000;
	font-weight: bold;
	text-align: right;
}
.res { color: #555555; }
table.res th { background-color: #555555; }
.total { 
	font-weight: bold;
	text-align: right;
	border-top: 2px solid #000099;
	border-bottom: 2px solid #000099;
}
.alt { background-color: #e7e7ff; }
tr.demo td { color: #777777; font-style:italic;}
</style>
<h1>Data Storage Report</h1> 
<p style="margin-top: -10px;"><i><small>Report drawn at <?php echo(date("d F Y H:i")); ?></small></i></p>
<p>* Note: This report ignores CSV and HTML files generated automatically by the system.</p>
<?php
$company = array();
$multiple_resellers = false;
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" */
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT cmpcode, cmpreseller, cmpname, cmpstatus, cmp_is_demo FROM assist_company WHERE cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname, c1.cmpstatus, c1.cmp_is_demo FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S'";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") { 
		$multiple_resellers = true;
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname, c1.cmpstatus, c1.cmp_is_demo FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S')
				WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') ".($res[1]!="ALLCMP"&&$res[1]!="DEMO" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT cmpcode, cmpreseller, cmpname, cmpstatus, cmp_is_demo FROM assist_company WHERE cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND cmpcode NOT IN ('IASSIST','BLANK') AND (cmpstatus = 'Y' OR cmpstatus = 'S') ORDER BY cmpreseller, cmpcode, cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}



//DISPLAY COMPANIES
echo "<table id=tbl_report><thead>
<tr>
	<th colspan=3>Company</th>".
($all_details ? "<th class=mb>System Use</th>" : "")."
	<th class=mb>Billable Storage<br />in Use*</th>
	<!-- <th class=mb>MB Included<br />in License Fee</th>
	<th class=mb>Billable<br />Storage</th> -->
</tr></thead><tbody>";
$grand_total = array('used'=>0,'free'=>0,'bill'=>0,'sys'=>0);
foreach($resellers as $rcode => $res) {
	if($multiple_resellers) {
		echo "
		<tr class=reseller>
			<td>".$rcode."</td>
			<td colspan=1>".$res['cmpname']."</td>
			<td>".($res['cmpstatus']=="S"?"<span class='red'>Suspended</span>":"Active")."</td>
			<td colspan='".($all_details?"2":"1")."'></td>
		</tr>";
	}
	$res_company = $company[$rcode];
	$res_total = array('used'=>0,'free'=>0,'bill'=>0,'sys'=>0);
	foreach($res_company as $ckey => $cmp) {
		$cc = strtolower($ckey);
		if(in_array($db_other.$cc,$dbs)) {
			$class = $cmp['cmp_is_demo']==1?"demo":"";
			$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','",$doccode)."') AND (modyn = 'Y' OR modadminyn = 'Y')";// echo $sql;
			$modules = mysql_fetch_all($sql,"O",$cc);
			$free = count($modules)*25*1024*1024;
			echo "
			<tr class='$class'>
				<td class='cmp right'>".$ckey."</td>
				<td class=cmp>".$cmp['cmpname']."</td>
				<td class=cmp>".($cmp['cmpstatus']=="S"?"<span class='red'>Suspended</span>":"Active").($cmp['cmp_is_demo']==1?" | Demo":"")."</td>
				";
			$folder = "../files/".$cc;
			if(is_dir($folder)) {	//if valid files folder
					$totalsize = getFolderSize($folder,$all_details);
					$ret=displayFolderSize($totalsize,$folder,"",false,$all_details); 
//echo "<td><pre>"; print_r($ret); echo "</pre></td>";
					$sizeinuse = $ret[0];
					$syssizeinuse = $ret[2];
					$res_total['sys']+=$syssizeinuse;
					$grand_total['sys']+=$syssizeinuse;
					$res_total['used']+=$sizeinuse;
					$grand_total['used']+=$sizeinuse;
					$res_total['free']+=$free;
					$grand_total['free']+=$free;
					$billable = ($sizeinuse - $free)>0 ? $sizeinuse - $free : 0;
					$res_total['bill']+=$billable;
					$grand_total['bill']+=$billable;
if($all_details) {
					echo "<td class=right>".formatBytes($syssizeinuse,2)."</td>";
}
					echo "<td class=right>".formatBytes($sizeinuse,2)."</td>";
					//echo "<td class=right>-".formatBytes($free,2)."</td>";
					//echo "<td class=right>".formatBytes($billable,2)."</td>";
			} else {	//if valid files folder - else
				echo "<td colspan=".($all_details?"2":"1")." class=i>No files folder exists for this client.</td>";
			}	//if valid files folder - end 
			echo "</tr>";
		}	//if valid db
	} //end foreach res_company
	if($multiple_resellers) {
		echo "
		<tr class=res_total>
			<td colspan=3>Total for: ".$res['cmpname']."</td>".
			($all_details ? "<td>".formatBytes($res_total['sys'],2)."</td>" : "")."
			<td>".formatBytes($res_total['used'],2)."</td>
			<!-- <td>&nbsp;</td>
			<td>".formatBytes($res_total['bill'],2)."</td> -->
		</tr>";
	}
}	//end foreach reseller
echo "</tbody>
<tfoot>
	<tr>
		<th class=right colspan=3>Total:</th>".
			($all_details ? "<td class=total>".formatBytes($grand_total['sys'],2)."</td>" : "")."
		<td class=total>".formatBytes($grand_total['used'],2)."</td>
		<!-- <td class=total>&nbsp;</td>
		<td class=total>".formatBytes($grand_total['bill'],2)."</td> -->
	</tr>
</tfoot>
</table>";
?>
<p>&nbsp;</p>
<script type=text/javascript>
$(function() {
	$("h2.reseller").not(":first").css("page-break-before","always");
	$("th.mb").attr("width","100px");
	//$("#tbl_report tbody tr").not(".reseller").not(".res_total").hover(addClass("alt");
});
</script>
</body>

</html>
<?php
$output = ob_get_contents();
ob_end_flush();
$output = getOutputHeaderWithNoHTTPS().$output;

		$filename = $report_folder."data_summary_".date("Ymd")."_".date("His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$output."\n");
        fclose($file);



//$sql = "INSERT INTO assist_billing_report_history (date_drawn, drawn_by, report_type, filename, request) VALUES (now(), '".strtoupper($_SESSION['cc'])."', 'DATA_SUM', '$filename', '".serialize($_REQUEST)."')";
$sql = "INSERT INTO assist_billing_report_history (date_drawn, drawn_by, report_type, filename, request) VALUES (now(), '".(isset($_REQUEST['src']) && strlen($_REQUEST['src'])>0 ? strtoupper($_REQUEST['src']) : strtoupper($_SESSION['cc']))."', 'DATA_SUM', '$filename', '".serialize($_REQUEST)."')";
$hist_id = Adb_insert($sql);

?>