<?php 
require_once '../header.php';

if(!isset($report_folder)) { $report_folder = "files/"; }

ob_start();



/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);


/* GET DOCUMENT STORAGE MODULES */
$docmods = array();
$doccode = array();
$sql = "SELECT * FROM assist_docmodules WHERE yn = 'Y' ORDER BY doclocation";
$rs = mysql_getRS($sql,"A");
	while($row = mysql_fetch_array($rs)) {
		$docmods[$row['doclocation']] = $row;
		$doccode[] = $row['doclocation'];
	}
unset($rs);


?>
<style type=text/css>
.reseller {
	background-color: #555555;
	color: #ffffff;
	padding: 5px;
	font-size: 18pt;
	line-height: 22pt;
}
.res { color: #555555; }
table.res th { background-color: #555555; }
h2.demo, h3.demo, p.demo, table.demo td { color: #999999; }
table.demo th { background-color: #999999;}
</style>
<h1>Data Storage Report</h1> 
<p style="margin-top: -10px;"><i><small>Report drawn at <?php echo(date("d F Y H:i")); ?></small></i></p>
<p>Note: This report ignores CSV and HTML files generated automatically by the system.  These are typically stored in STASKS, import(s) and export(s) folders.</p>
<?php
$company = array();
$multiple_resellers = false;
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" */
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT cmpcode, cmpreseller, cmpname, cmpstatus, cmp_is_demo FROM assist_company WHERE cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname, c1.cmpstatus, c1.cmp_is_demo FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S'";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname, c1.cmpstatus, c1.cmp_is_demo FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S')
				WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') ".($res[1]!="ALLCMP"&&$res[1]!="DEMO" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
		$multiple_resellers = true;
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT cmpcode, cmpreseller, cmpname, cmpstatus, cmp_is_demo FROM assist_company WHERE cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND cmpcode NOT IN ('IASSIST','BLANK') AND (cmpstatus = 'Y' OR cmpstatus = 'S') ORDER BY cmpreseller, cmpcode, cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}


//DISPLAY COMPANIES
foreach($resellers as $rcode => $res) {
	if($multiple_resellers) {
		echo "
		<h2 class=reseller>".$res['cmpname']."</h2>";
	}
	$res_company = $company[$rcode];
	$res_total = array('used'=>0,'free'=>0,'bill'=>0);
	foreach($res_company as $ckey => $cmp) {
		$cc = strtolower($ckey);
		if(in_array($db_other.$cc,$dbs)) {
			if($cmp['cmp_is_demo']==1) {
				$class = "demo";
			} else {
				$class = "";
			}
			$fre = 0;
			$f = 0;
			echo "	<h2 class='cmp $class'>".$cmp['cmpname']." (".$ckey.")</h2>
	<p class=i style='margin-top: -5px;font-size:85%;line-height:110%' class='$class'>Company Status: ".($cmp['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active").($cmp['cmp_is_demo']==1?" | Demo":"")."</p>";
			$folder = "../files/".$cc;
			if(is_dir($folder)) {	//if valid files folder
				$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','",$doccode)."') AND (modyn = 'Y' OR modadminyn = 'Y')";
				$modules = mysql_fetch_all($sql,"O",$cc);
				if(count($modules)>0) {	//if active doc modules
					echo "
					<h3 class='$class'>Active Data Storage Modules</h3>
					<table cellpadding=5 width=300  class='$class'>
						<tr><th>System Ref</th><th>Client Ref</th><th>Module Name</th></tr>";
							foreach($modules as $mod) {
								echo "
								<tr>
									<td width=50 class='b center'>".$mod['modlocation']."</td>
									<td width=50 class='b center'>".$mod['modref']."</td>
									<td>".$mod['modtext']."</td>
								</tr>";
								$f++;
							}
						$fre = 25 * $f;
//						<tr>
//							<td colspan=3 class='b right'>Total MB included: ".$fre." MB</td>
//						</tr>
						echo "
					</table>";
				} else {	//if active doc modules - else
					echo "<p>No active document modules exist for this client.</p>";
				}	//if active doc modules - end
					echo "<h3  class='$class'>Data Storage In Use</h3>";
					$ts = 0;
					$totalsize = getFolderSize($folder);
					$folder.="/";
					echo "
					<table width=500 class='$class'>
						<tr>
							<th>Folder</th>
							<th>Num. of Files</th>
							<th>File Size (MB)</th>
						</tr>";
					$ret=displayFolderSize($totalsize,$folder,$moduletitle);
					$ts = $ret[0];
					$fn = $ret[1];
					$res_total['used']+=$ts;
					$res_total['free']+=($fre*1024*1024);
					$diff = $ts - ($fre*1024*1024);
					$res_total['bill']+= ($diff>0 ? $diff : 0);
					echo "
						<tr>
							<th class=right>Total Usage in Mega-Bytes:</th>
							<th class=right>&nbsp;</th>
							<th class=right>".formatBytes($ts,2)."</th>
						</tr>
					</table>";
//						<tr>
//							<td class='b right'>Less MB included in license fee:</th>
//							<td class=right>&nbsp;</td>
//							<td class=right>-".formatBytes($fre*1024*1024,2)."</td>
//						</tr>
//						<tr>
//							<th class=right>Total in Mega-Bytes:</th>
//							<th class=right>&nbsp;</th>
//							<th class=right>".formatBytes(($ts-($fre*1024*1024)),2)."</th>
//						</tr>
			} else {	//if valid files folder - else
				echo "<p><span class=idelete>ERROR:</span> No files folder exists for this client. Please contact Action iT immediately to ensure that a folder is created for this client.</p>";
			}	//if valid files folder - end 
		}	//if valid db
	} //end foreach res_company
	echo "
	<h2 class=res>Total for ".$res['cmpname']."</h2>
	<table width=300 class=res>
		<tr>
			<th class=left>Storage in Use:</td>
			<td class=right>".formatBytes($res_total['used'],2)."</td>
		</tr>
		<tr>
			<th class=left>Billable Storage:</td>
			<td class=right>".formatBytes($res_total['bill'],2)."</td>
		</tr>
	</table>";
}	//end foreach reseller
?>
<p>&nbsp;</p>
<script type=text/javascript>
$(function() {
	$("h2.reseller").not(":first").css("page-break-before","always");
});
</script>
</body>

</html>
<?php
$output = ob_get_contents();
ob_end_flush();
$output = getOutputHeaderWithNoHTTPS().$output;

		$filename = $report_folder."data_detail_".date("Ymd")."_".date("His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$output."\n");
        fclose($file);



$sql = "INSERT INTO assist_billing_report_history (date_drawn, drawn_by, report_type, filename, request) VALUES (now(), '".(isset($_REQUEST['src']) && strlen($_REQUEST['src'])>0 ? strtoupper($_REQUEST['src']) : strtoupper($_SESSION['cc']))."', 'DATA_DETAIL', '$filename', '".serialize($_REQUEST)."')";
$hist_id = Adb_insert($sql);

?>