<?php 
	$ts = 0;
	$totalsize = getFolderSize($folder);
	$folder.="/";
?>
<table cellpadding=3 cellspacing=0 width=500>
	<tr>
		<th>Folder</th>
		<th>Num. of Files</th>
		<th>File Size (MB)</th>
	</tr>
<?php 	$ret=displayFolderSize($totalsize,$folder,$moduletitle); 
		$ts = $ret[0];
		$fn = $ret[1];
?>
<!--	<tr>
        <td class=fc style="font-weight:bold;text-align:right;">Total:</th>
		<td class=fc style="font-weight:bold;text-align:center;"><?php echo $fn; ?></td>
		<td class=fc style="font-weight:bold;text-align:right;"><?php echo number_format($ts,0)." B"; ?></td>
    </tr> -->
	<tr>
        <th style="text-align:right;">Sub-Total in Mega-Bytes:</th>
        <th style="text-align:right;">&nbsp;</th>
		<th style="text-align:right;"><?php echo formatBytes($ts,2); ?></th>
    </tr>
	<tr>
        <td class=fc style="font-weight:bold;text-align:right;">Less MB included in license fee:</th>
        <td style="text-align:right;">&nbsp;</td>
		<td style="text-align:right;"><?php echo number_format($fre,0)." MB"; ?></td>
    </tr>
	<tr>
        <th style="text-align:right;">Total in Mega-Bytes:</th>
        <th style="text-align:right;">&nbsp;</th>
		<th style="text-align:right;"><?php echo formatBytes(($ts-($fre*1024*1024)),2); ?></th>
    </tr>
</table>
