<?php
require_once '../header.php';
//error_reporting(-1);
/* GET COMPANY LIST */
$company = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE (cmpstatus = 'Y' OR cmpstatus = 'S') AND cmpcode <> 'BLANK' AND cmp_is_demo = 0 ORDER BY cmpreseller, cmpcode";
} else {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE (cmpstatus = 'Y' OR cmpstatus = 'S') AND cmpreseller = '".strtoupper($cmpcode)."' AND cmp_is_demo = 0 ORDER BY cmpcode";
}
$rs = AgetRS($sql);
    while($row = mysql_fetch_array($rs))
    {
        $company[$row['cmpreseller']][] = $row;
    }
unset($rs);

/* GET RESELLER INFO */
$resellers = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S')
			WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S')";
	$rs = AgetRS($sql);
	while($row = mysql_fetch_assoc($rs)) {
		$resellers[$row['cmpcode']] = $row;
	}
	unset($rs);
	$multiple_resellers = true;
} else {
	$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>"SELECT");
	$multiple_Resellers = false;
}

/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);
?>
<style type=text/css>
th { text-align: left; vertical-align: top; }
</style>
<script type=text/javascript>

function userReport() {
	var cc = document.getElementById('cc').value;
	if(cc.length > 1) {
		document.location.href = 'report_user_export.php?c='+cc;
	}
}
</script>
<h1>Assist Reports</h1>
<form name=reportgen id=rg action=companyreport.php method=post><input type=hidden name=src value='<?php echo $_SESSION['cc']; ?>' />
<table cellpadding=3>
	<tr>
		<th>Company:</th>
		<td><select name=cmpcode id=sel_cmp><option selected summary=1 value=ALL_ALLCMP>All companies</option><?php 
if(strtoupper($_SESSION['cc'])=="IASSIST") {
		echo "<option value=ALL_DEMO summary=1>All companies incl Demos</option>";
}
		foreach($resellers as $rcc => $res) {
			if($multiple_resellers) { echo "<option value=ALL_".$rcc." summary=1>--- ".$res['cmpname']." (All) ---</option>"; }
			foreach($company[$rcc] as $cmp) {
				echo "<option value=".$cmp['cmpcode']." summary=0>".$cmp['cmpcode'].": ".$cmp['cmpname']."</option>";
			}
		}

?></select></td>
	</tr>
	<tr>
		<th>Report:</th>
		<td>
			<input type=radio name=rep value=MOD id=ml checked > <label for=ml>Module License Report</label>
			<span class=summary><br /><input type=radio name=rep value=MOD_SUM id=mls class=summary > <label for=mls>License Summary Report <?php if(isset($abc) && $_SESSION['cc']=="iassist" && $_SESSION['tid']=="0001") { ?> as at <input type=text size=10 class=jdate2012 value=<?php echo date("d-M-Y"); ?> name=aa /> <?php } ?></label></span>
			<br /><input type=radio name=rep value=DATA id=ds > <label for=ds>Data Storage Report</label>
			<span class=summary><br /><input type=radio name=rep value=DATA_SUM id=dss class=summary > <label for=dss>Data Storage Summary Report</label> <?php if($_SESSION['cc']=="iassist" && $_SESSION['tid']=="0001") { echo "<br /><input type=checkbox name=jc value=Y checked /> Including excluded system files"; } ?></span>
			<br /><input type=radio name=rep value=BILL id=bl > <label for=bl>Billing Report</label>
			<?php if(strtoupper($_SESSION['cc']) == "IASSIST") { ?>
			<span class=summary><br /><input type=radio name=rep value=UC_SUM id=ucr class=summary ><label for=ucr>User Count Report</label></span>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td colspan=2><input type=submit value=Generate></td>
	</tr>
</table>
</form>
<?php if($abc == "123" && ($_SESSION['tid'] == "0001" || $_SESSION['tid'] == "0002")) { ?>
<p><b>User Report:</b> <select id=cc><option value=X selected>--- SELECT ---</option>
<?php
		foreach($company['IASSIST'] as $row)
		{
			echo "<option value=".$row['cmpcode'].">".$row['cmpcode'].": ".$row['cmpname']."</option>";
		}
?>
</select> <input type=button value=Go onclick="userReport();" /></p>
<?php } ?>
<script type=text/javascript>
$(function() {
	$("th").addClass("left");
	$("#sel_cmp").change(function() {
		$opt = $("#sel_cmp option:selected");
		$rad = $("input[name=rep]:checked");
		//alert($rad.attr("id")+" > "+$opt.attr("summary"));
		$rad.trigger("click");
		if(parseInt($opt.attr("summary"))==1) {
			$("span.summary").show();
		} else {
//			if($rad.hasClass("summary")) { $("#ml").attr("checked","true").trigger("click"); }
			$("span.summary").hide();
		}
		//alert($rad.attr("id")+" :: "+$("form[name=reportgen]").attr("action"));
	});
	$("input:radio").click(function() {
		var i = $(this).attr("id");
		var url = "";
		var aa = "";
		switch(i) {
		case "ml":	url = "companyreport.php";	break;
		case "ucr":	url = "usercountreport.php";	break;
		case "mls":	
			aa = "";//$("input:text[name=aa]").val(); 
			url = "user_summary.php?aa="+aa;	
			break;
		case "ds":	url = "data_report.php";	break;
		case "dss":	url = "data_summary.php";	break;
		case "bl":	url = "billing_matrix.php";	break;
		}
		//alert(i+" :-: "+url);
		$("form[name=reportgen]").attr("action",url);
	});
	$("#sel_cmp").trigger("change");
});

</script>
<?php
/* SECTION ONLY FOR IASSIST USERS */ 
if(strtoupper($_SESSION['cc']) == "IASSIST" && $_SESSION['tid']=="0001") {
	echo "
	<h2>Additional Reports (Action iT only)</h2>
	<ul>
		<!-- <li><a href=/assist_admin/it_db_report_log.php>Server Database Activity Report - Transaction log</a><br /><span class=i>(This is the old transaction log which is being phased out and which only reflects activity on pre-2011 modules)</span></li> -->
		<li><a href=/assist_admin/it_db_report_user.php>Server Database Activity Report - User Login/Logout log</a> (As from 2 October 2011)</li>
		<li><a href=/assist_admin/it_mod_report_user.php>Client Module Activity Report - User Module Access log</a> (As from 2 October 2011)</li>
		<li><a href=/assist_admin/it_db_first_user.php>Client Database Creation Date By User Add Date</a></li>
		<li>&nbsp;</li>
		<li><a href=/assist_admin/ait_client_user_activity_report.php>Action iT User License Activity report</a></li>
		<li><a href=/assist_admin/ait_client_license_activity_report.php>Action iT Module License Activity report</a></li>
	</ul>";
}


?>

</body>

</html>
