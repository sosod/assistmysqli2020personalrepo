<?php
/************************
 * Page added 30 October 2016 by JC
 * Grants Admin users access to system reports
 */
require_once("../module/autoloader.php");

$me = new ASSIST();
$me->echoPageHeader();


?>
<h1>System Reports</h1>
<!-- container table --><table width=760px class=noborder><tr><td class=noborder>
<table class=form width=750px>
    <tr>
        <th>System Backup Report:</th>
        <td>Report on the status of the automated system backups <span class=float><input type=button value=" View " id="sysreport_backuplog"></span></td>
    </tr>
	<tr>
		<th>Backup Restoration Test Report:</th>
		<td>Report on the status of the resoration tests done on the system backups <span class=float><input type=button value=" View " id="sysreport_backuptestlog"></span></td>
	</tr>
	<tr>
		<th>Maintenance Log Report:</th>
		<td>Report on the system maintenance time scheduled. <span class=float><input type=button value=" View " id="sysreport_maintenance"></span></td>
	</tr>
    <tr>
        <th>System Updates Log:</th>
        <td>Report on the updates made to the Assist system <span class=float><input type=button value=" View " id="sysreport_updates"></span></td>
    </tr><!--
	<tr>
		<th>Email Log:</th>
		<td>Report on the emails sent to users by the Assist system <span class=float><input type=button value=" View " id="sysreport_email"></span></td>
	</tr> -->
	<tr>
		<th>System Uptime / Outages Report:*</th>
        <td>Report on the uptime and outages of the Assist system <span class=float><input type=button value=" View " id="sysreport_uptime"></span></td>
	</tr>
	<tr>
		<th>System / Module Version Report:</th>
        <td>Report on current version number of the Assist System as well as any modules currently in use. <span class=float><input type=button value=" View " id="sysreport_versions"></span></td>
	</tr>
</tr>
</table>

<h4>*System Uptime/Outages</h4>
<ul>
	<li>Since 13 October 2016, the <?php echo $me->getSiteName(); ?> system has been remotely monitored by <a href="http://www.alertra.com">Alertra</a>, an independent website monitoring service.</li>
	<li>Alertra uses multiple monitoring nodes situated in various locations in North America, Europe and Asia/Pacific regions to continuously check on the status of the <?php echo $me->getSiteName(); ?> system.  Each region has multiple monitoring nodes to minimise any risk of failure on the Alertra side and to increase the probabilty of early detection of any failure of the <?php echo $me->getSiteName(); ?> system.</li> 
	<li>In the event that 3 consecutive monitoring node checks fail an outage event is declared and Action iT IT Personnel are immediately notified of the failure so that it can be attended to and resolved as quickly as possible.  </li>
	<li>In the event of an outage, Alertra will automatically update the System Uptime / Outages Report as soon as the Assist system is back up and is responding to checks from the Alertra monitoring nodes.  This happens automatically without any action required by Action iT IT personnel.</li>
</ul>
</td></tr></table>
<script type=text/javascript>
$(document).ready(function() {
	$("table th").addClass("left");
	
	$("input:button").click(function() {
		AssistHelper.processing();
		var i = $(this).prop("id");
		document.location.href = i+".php";
		if(i=="report_user_export") {
			AssistHelper.closeProcessing();
		}
	});
});
</script>

</body></html>