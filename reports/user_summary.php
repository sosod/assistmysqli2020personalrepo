<?php 
require_once "../header.php";

function nf($v) {
	return ($v>0 ? number_format($v,0) : "-");
}

//error_reporting(-1);
$as_at = isset($_REQUEST['aa']) && strlen($_REQUEST['aa'])>0 ? strtotime($_REQUEST['aa']." 23:59:59") : strtotime(date("d F Y H:i:s"));

$doc_modules = array('REP','MN','TMP','TMP2','REP3','REP2','AUDMT','OPP','COSEC','ACC','INV','PAY','PAYU','HRA');
$action_modules = array('TA');
$ud_modules = array('UD','UDB');
$pstl_modules = array('PSTL1','PSTL2');
$sdp12_modules = array('SDP12','SP12B');
$risk_modules = array('RISK','RBER','RCED','RMAT','RSWA','RWES');
$dash_modules = array('DHP10','DASH','DHP11','DHP12','DHP13','DSH10','DSH11','DSH12','DSH13','DSHBD','DSHP');
$master_modules = array('CUSTOMER','SUPPLIER','EMPLOYEE');
$modules = array();
$modules['ACTION'] = array('id'=>'ACTION','value'=>"Action Management");
$modules['MDOC'] = array('id'=>"MDOC",'value'=>"Module Document Management");
$modules['DOC'] = array('id'=>"DOC",'value'=>"Document Management (v2)");
$modules['DOC1'] = array('id'=>"DOC",'value'=>"Document Management (v1)");
$modules['SDBIP'] = array('id'=>"SDBIP",'value'=>"SDBIP 2008/2009");
$modules['SDBP2'] = array('id'=>"",'value'=>"SDBIP 2009/2010");
$modules['SDP10'] = array('id'=>"",'value'=>"SDBIP 2010/2011");
$modules['SDP11'] = array('id'=>"",'value'=>"SDBIP 2011/2012");
$modules['SDP12'] = array('id'=>"",'value'=>"SDBIP 2012/2013");
$modules['SDP13'] = array('id'=>"",'value'=>"SDBIP 2013/2014");
$modules['SDP14'] = array('id'=>"",'value'=>"SDBIP 2014/2015");
$modules['SDP15'] = array('id'=>"",'value'=>"SDBIP 2015/2016");
$modules['SDP16'] = array('id'=>"",'value'=>"SDBIP 2016/2017");
$modules['SDP17'] = array('id'=>"",'value'=>"SDBIP 2017/2018");
$modules['SDP18'] = array('id'=>"",'value'=>"SDBIP 2018/2019");
$modules['AQA'] = array('id'=>"",'value'=>"Audit Query");
$modules['AUDIT'] = array('id'=>"",'value'=>"Audit Query (v1)");
$modules['CAPS'] = array('id'=>"",'value'=>"Complaints");
$modules['PMSPS'] = array('id'=>"",'value'=>"Performance [Public]");
$modules['PMS'] = array('id'=>"",'value'=>"Performance [Private]");
$modules['NB'] = array('id'=>"",'value'=>"Noticeboard");
$modules['DASH'] = array('id'=>"",'value'=>"Dashboard Assist Variations");
//$modules['DHP10'] = array('id'=>"",'value'=>"Dashboard 2010/2011 [Public]");
//$modules['DASH'] = array('id'=>"",'value'=>"Dashboard Assist");
$modules['RISK'] = array('id'=>"",'value'=>"Risk Assist");
$modules['HP'] = array('id'=>"",'value'=>"Help Assist");
$modules['RAPS'] = array('id'=>"",'value'=>"Resolutions Assist");
$modules['UD'] = array('id'=>"",'value'=>"Unclaimed Deposits");
$modules['PP'] = array('id'=>"",'value'=>"Project Prioritisation");
$modules['PPH'] = array('id'=>"",'value'=>"Policies & Procedures");
$modules['PSTL'] = array('id'=>"",'value'=>"Pastel");
//$modref = "DHP11"; $modtxt = "Dashboard 2011/2012";
//$modules[$modref] = array('id'=>"",'value'=>$modtxt);
//$modref = "DHP12"; $modtxt = "Dashboard 2012/2013";
//$modules[$modref] = array('id'=>"",'value'=>$modtxt);
//$modref = "DHP13"; $modtxt = "Dashboard 2013/2014";
//$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "QRY"; $modtxt = "Query Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "SLA"; $modtxt = "Contract/SLA Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "TVL"; $modtxt = "Travel Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "TVLA"; $modtxt = "Travel Assist for Agents";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "COMPL"; $modtxt = "Compliance Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "CUSTOMER"; $modtxt = "Customer Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "EMPLOYEE"; $modtxt = "Employee Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "SUPPLIER"; $modtxt = "Supplier Assist";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);






$modref = "A"; $modtxt = "Associates Online";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "HX"; $modtxt = "Hosted Exchange";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "admin"; $modtxt = "Ignite Assist Admin";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);
$modref = "repor"; $modtxt = "Ignite Assist Reports";
$modules[$modref] = array('id'=>"",'value'=>$modtxt);

foreach($modules as $key => $m) {
	$modules[$key]['id'] = $key;
	$modules[$key]['total'] = 0;
}
ksort($modules);
//$cmpcode = $_REQUEST['cmpcode'];
//if(strlen($cmpcode)<6) { $cmpcode = ""; } 

/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);



?>
<style type=text/css>
.c { text-align: center; }
.a { border-left: 1px solid #000099; }
.t { font-style: none; font-weight: bold; }
.gt { font-weight: bold; font-style: normal; font-size: 10pt; }
.total { font-weight: bold; border-bottom: 2px solid #000099; border-top: 2px solid #000099; }
.alt { background-color: #e7e7ff; }
.res { background-color: #555555; font-weight: bold; color: #ffffff; }
.gt.res { text-decoration: underline; }
</style>

<h1>License Summary</h1> 
<p style="margin-top: -5px;"><i>Report drawn based on the users as at <u><?php echo(date("d F Y H:i",$as_at)); ?></u></i></p>
<p>Note: This summary automatically excludes the User Directory module and the Assist Administrator and Support user.</p>
<?php
/*$company = array();
$inactive = false;
$multiple_resellers = false;
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" *//*
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT cmpcode, cmpreseller, cmpname FROM assist_company WHERE cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND c2.cmpstatus = 'Y' AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE c1.cmpstatus = 'Y'";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		$multiple_resellers = true;
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND c2.cmpstatus = 'Y'
				WHERE c1.cmpstatus = 'Y' ".($res[1]!="ALLCMP" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT cmpcode, cmpreseller, cmpname FROM assist_company WHERE cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND cmpcode NOT IN ('IASSIST','BLANK') ORDER BY cmpreseller, cmpcode, cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}
*/

//error_reporting(-1);

$inactive = false;
$company = array();
$multiple_resellers = false;
$fields = "c1.cmpcode, c1.cmpreseller, c1.cmpname, c1.cmp_price_class, c1.cmp_is_csi, c1.cmp_bill_hosting, c1.cmpstatus";
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_DEMO" || "ALL_ABC1234" || "ABC1234" */
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE c1.cmpstatus = 'Y' AND c1.cmp_price_class <> 'X'";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT $fields FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') 
				WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S')  AND ".($res[1]!="DEMO" ? "c1.cmp_is_demo = 0 AND " : "")." c1.cmp_price_class <> 'X' ".($res[1]!="ALLCMP" && $res[1]!="DEMO" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A"); //echo $sql;
		$multiple_resellers = true;
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND c1.cmpcode NOT IN ('IASSIST','BLANK') AND (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S')  AND ".($_REQUEST['cmpcode']!="ALL_DEMO" ? "c1.cmp_is_demo = 0 AND " : "")." c1.cmp_price_class <> 'X' ORDER BY c1.cmpreseller, c1.cmpcode, c1.cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A"); 
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}
//arrPrint($resellers);
//arrPrint($company);






/* GET COUNTS */
$mods = array();
$counts = array();
foreach($resellers as $rkey => $res) {
	$counts['res'][$rkey] = array();
	foreach($company[$rkey] as $ckey => $cmp) {
		$cc = strtolower($ckey);
		$counts[$cc] = array();
		if(in_array($db_other.$cc,$dbs)) {
			//GET TOTAL USERS FROM TIMEKEEP TABLE GROUP BY TKSTATUS
			$sql = "SELECT tkstatus, count(tkid) as c FROM assist_".$cc."_timekeep 
					WHERE tkstatus <> 0 AND (tkuser <> 'support' AND tksurname <> 'Support') 
					AND tkadddate < $as_at AND (tktermdate = 0 OR tktermdate > $as_at)
					GROUP BY tkstatus";
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				$company[$rkey][$ckey]['tk'.$row['tkstatus']] = $row['c'];
				$resellers[$rkey]['users']['tk'.$row['tkstatus']] = isset($resellers[$rkey]['users']['tk'.$row['tkstatus']]) ? $resellers[$rkey]['users']['tk'.$row['tkstatus']]+$row['c'] : $row['c'];
			}
			unset($rs);
			//GET MODULES AND COUNT USERS PER MODULE
			$sql = "SELECT modref, modlocation, tkstatus, count(DISTINCT usrtkid) as c 
					FROM assist_menu_modules 
					INNER JOIN assist_".$cc."_menu_modules_users 
					  ON usrmodref = modref AND usrmodref <> 'TD'  
					INNER JOIN  assist_".$cc."_timekeep 
					  ON usrtkid = tkid AND tkstatus <> 0 AND tkid <> '0000' AND tkuser <> 'support' AND tkadddate < $as_at AND (tktermdate = 0 OR tktermdate > $as_at)
					WHERE modyn ='Y' OR modadminyn = 'Y' 
					GROUP BY modref, tkstatus "; //echo $cc=="ove0002" ? $sql : "";
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				$modref = $row['modref'];
				$modlocation = $row['modlocation'];
				if($modlocation=="DOC") {
					$modref = "DOC";
				} elseif(in_array($modref,$doc_modules)) { 
					$modref = 'DOC1'; 
				} elseif(in_array($modref,$action_modules) || $modlocation == "ACTION") { 
					$modref = "ACTION"; 
				} elseif(in_array($modref,$ud_modules)) { 
					$modref = "UD"; 
				} elseif(in_array($modref,$pstl_modules)) { 
					$modref = "PSTL"; 
				} elseif(in_array($modref,$sdp12_modules)) { 
					$modref = "SDP12"; 
				} elseif(in_array($modref,$risk_modules)) { 
					$modref = "RISK"; 
				} elseif(in_array($modref,$dash_modules)) { 
					$modref = "DASH"; 
				} 
				$counts[$cc][$modref][$row['tkstatus']]+=$row['c'];
				$counts['res'][$rkey][$modref][$row['tkstatus']]+=$row['c'];
				if(!in_array($modref,$mods)) { $mods[] = $modref; }
				$modules[$modref][$row['tkstatus']]['total']+=$row['c'];
			}
			unset($rs);
		} else {
			unset($company[$rkey][$ckey]);
		}	//end if in db list
	}	//end foreach company[rkey]
}	//end foreach reseller
sort($mods);

if(in_array("PMSPS",$mods)) { $inactive = true; }

/* DISPLAY REPORT */
if($inactive) { $span = 2; }  else { $span = 1; }
echo "
<table id=tbl_report><thead>
	<tr>
	<th rowspan=$span colspan=3>Company</th>
	<th colspan=$span>Registered<br />Users</th>";
	foreach($mods as $m) {
		echo "<th ".($m=="PMSPS"?"colspan=2 rowspan=1":"rowspan=$span").">";
		if(isset($modules[$m]['value']) ) { 
			echo $modules[$m]['value'];
		} else {
			echo $m;
		}
		echo "</th>";
	}
	echo "	<th colspan=$span>Total</th>
	<th rowspan=$span>Grand&nbsp;Total</th>
	</tr>";
	if($span > 1) {
		echo "<tr>
			<th>Active</th>
			<th>Inactive</th>";
			if(in_array("PMSPS",$mods)) { echo "<th>Active</th><th>Inactive</th>"; }
		echo "
			<th>Active</th>
			<th>Inactive</th>
		</tr>";
	}
	echo "
	</thead><tbody>
	";

	$ultimate_total = array();
	$tr = 0;
	$totalact = 0; $totalinact = 0;
foreach($resellers as $rkey => $res) {
	$res_total = array(1=>0, 2=>0);
	if($multiple_resellers) {
		echo "
		<tr class=res>
			<td class=res>".$rkey."</td>
			<td class=res>".$res['cmpname']."</td>
			<td class=res>".($res['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active")."</td>
				<td class='res c'>".nf($res['users']['tk1'])."</td>
				".($inactive ? "<td class='res c'>".nf($res['users']['tk2'])."</td>" : "");
				foreach($mods as $m) {
					$j = $m=="PMSPS" ? 3  : 2;
					for($i=1;$i<$j;$i++) {
						$zyx = isset($counts['res'][$rkey][$m][$i]) ? $counts['res'][$rkey][$m][$i] : 0;
						echo "<td class='res c'>".nf($zyx)."</td>";
						$res_total[$i]+=$zyx;
					}
				}
				echo "
					<td class='c a t res'>".nf($res_total[1])."</td>
					".($inactive ? "<td class='c t res'>".nf($res_total[2])."</td>" : "")."
					<td class='c a gt res'>".nf(array_sum($res_total))."</td>
				</tr>";
	}
	foreach($company[$rkey] as $ckey => $cmp) {
		$cc = strtolower($ckey);
		if(in_array($db_other.$cc,$dbs)) {
			$total = array(1=>0,2=>0,3=>0);
			echo "<tr height=50 class=cmp>";
			$totalact+=$cmp['tk1'];
			$totalinact += isset($cmp['tk2']) ? $cmp['tk2'] : 0;
			echo "
			<td class=b>".$ckey."</td>
			<td class=b>".$cmp['cmpname']."</td>
			<td >".($cmp['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active")."</td>
			<td class='b c'>".nf($cmp['tk1'])."</td>
			".($inactive ? "<td class='b c'>".nf(isset($cmp['tk2']) ? $cmp['tk2'] : 0)."</td>" : "");
			foreach($mods as $m) {
				$j = $m=="PMSPS" ? 3  : 2;
				for($i=1;$i<$j;$i++) {
					$v = isset($counts[$cc][$m][$i]) ? $counts[$cc][$m][$i] : 0;
					echo "<td class=c>".nf($v)."</td>";
					$total[$i] += $v;
				}
			}
			echo "
				<td class='c a t '>".nf($total[1])."</td>
				".($inactive ? "<td class='c t'>".nf($total[2])."</td>" : "")."
				<td class='c a gt'>".nf(array_sum($total))."</td>";
			$ultimate_total[1]+=$total[1];
			$ultimate_total[2]+=$total[2];
			echo "</tr>";
		}
	}
}
	
	echo "</tbody><tfoot>
	<tr>
		<th class=right colspan=3>TOTAL:</th>";
$class = "total c";
	echo "<td class=\"$class\">&nbsp;".nf($totalact)."&nbsp;</td>";
	if($inactive) { echo "<td class=\"$class\">&nbsp;".nf($totalinact)."&nbsp;</td>"; }

	foreach($mods as $m) {
		$j = $m=="PMSPS" ? 3  : 2;
		for($i=1;$i<$j;$i++) {
			echo "<td class='total c'>".nf($modules[$m][$i]['total'])."</td>";
		}
	} 
	echo "
	<th>&nbsp;".nf($ultimate_total[1])."&nbsp;</th>
	".($inactive ? "<th>&nbsp;".nf($ultimate_total[2])."&nbsp;</th>" : "" )."
	<th>&nbsp;<u>".nf(array_sum($ultimate_total))."</u>&nbsp;</th>
	</tr></tfoot>
</table>";
?>
<p>&nbsp;</p>
<script type=text/javascript>
$(function() {
	$("#tbl_report tbody tr.cmp:odd").addClass("alt");
});
</script>
</body></html>