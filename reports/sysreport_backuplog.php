<?php
//error_reporting(-1);

include("../module/autoloader.php");

$interval = 18;

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader();

//$sql = "SELECT * FROM assist_backup WHERE status = 2 AND datetime > DATE_SUB(CURDATE(), INTERVAL 1 YEAR) ORDER BY datetime DESC";
$d = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),1,date("Y")-1));

$sql = "SELECT * FROM assist_backup WHERE status & 2 = 2 AND datetime > DATE_SUB(CURRENT_DATE(), INTERVAL $interval MONTH) ORDER BY datetime DESC";
$logs = $db->mysql_fetch_all($sql);




?>
<h1>System Backup Status Report</h1>
<p>Action iT backup log for the previous <?php echo $interval; ?> months as per Action iT Assist Backup Policy (<a href='Action iT Assist Backup Policy 2018-07-01 published.pdf' target=_blank>Link</a>)</p>
<table>
	<tr>
		<th>Date</th>
		<th>Server</th>
		<th>Backup<br />Frequency</th>
		<th>Backup<br />Type</th>
		<th>Message</th>
	</tr>
<?php
	foreach($logs as $log) {
		switch(trim(strtolower($log['result']))) {
			case "successful":
				$icon = "ok";
				break;
			case "warnings":
				$icon = "info";
				break;
			default:
				$icon = "error";
				break;
		}
		echo "
		<tr>
			<td>".date("d M Y",strtotime($log['datetime']))."</td>
			<td>".$helper->getServerName()."</td>
			<td>".ucwords(strtolower($log['backup_freq']))."</td>
			<td>".ucwords(strtolower($log['backup_type']))."</td>
			<td>".($helper->getDisplayIcon($icon))." <span>".($log['message'])."</span></td>
		</tr>";
	}
?>
</table>
<p class=i style='font-size: 100%'>Report drawn on <?php echo date("d F Y"); ?> at <?php echo date("H:i:s"); ?>.</p>
<script type="text/javascript">
$(function() {
	$("span").css("display","inline");
	//$("span").css("border","1px dashed #fe9900");
	$("td").find("span:first").css("padding-left","13px");
});
</script>