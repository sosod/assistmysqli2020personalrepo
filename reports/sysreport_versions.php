<?php
//error_reporting(-1);

include("../module/autoloader.php");

$interval = 18;
$max_days_to_display = 545;
$today = time(); //strtotime("20 May 2020");//   //used for dev testing of buttons
$now = isset($_REQUEST['dt']) ? strtotime($_REQUEST['dt']." 23:59:59") : strtotime(date("d F Y",$today)." 23:59:59")-(24*60*60);
$displaying_today = isset($_REQUEST['dt'])?false:true;
//Calculate previous financial year
if(date("m",$today)>6) {
	$prev_fin_year = mktime(null,null,null,6,30,date("Y",$today));
} else {
	$prev_fin_year = mktime(null,null,null,6,30,date("Y",$today)-1);
}

$me = new ASSIST_MODULE_HELPER();

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader("1.10.0",array(),array("/assist3.css?".time()));

if($displaying_today) {
	$button = "<button id='btn_history'>Display Versions as at ".date("d F Y",$prev_fin_year)."</button>";
} else {
	$button = "<button id='btn_today'>Display Versions as of yesterday</button>";
}

echo "<h1>System / Module Version Report</h1>";
echo "<p class='i'>This report currently displays the version numbers as at ".date("d F Y",$now).".</p><span class='float'>$button</span>";

$sql = "SELECT mod_code, max(yt_issue) as yt_issue, max(yt_resolve_date) as yt_resolve_date, count(yt_issue) as yt_count FROM assist_module_issues WHERE yt_resolve_date < '".date("Y-m-d",$now)."' GROUP BY mod_code ORDER BY mod_code";
$rows = $db->mysql_fetch_all_by_id($sql,"mod_code");

$sql = "SELECT modref as mod_ref, modlocation as mod_code, modtext as mod_name FROM assist_menu_modules WHERE modyn = 'Y' AND modlocation<>'PM6' ORDER BY modtext";
$menu = $me->mysql_fetch_all_by_id($sql,"mod_code");

//menu tidying up


$always_include = array("ASSIST3"=>"Assist System", "HD2"=>"HelpDesk");
$always_ignore = array("admin","admin2","reports","CMPL","DTM","EXTERNAL","PM");

$ias_modules = array("PMSPS","PP","AQA");

echo "<table><tr><th>Assist Module Ref</th><th>Client Module Ref</th><th>Client Module Name</th><th>Version Number</th><th>Last Modified Date</th></tr>";


foreach($always_include as $mod_code=>$mod_name) {
	$version_row = isset($rows[$mod_code]) ? $rows[$mod_code] : false;
	$version = $version_row===false?$mod_code.".0.0":$mod_code.".".$version_row['yt_issue'].".".$version_row['yt_count'];
	$version_date = $version_row===false?"-":date("d F Y",strtotime($version_row['yt_resolve_date']));
	echo "<tr><td class='center'>$mod_code</td><td class='center'>-</td><td>$mod_name</td><td class='right'>".$version."</td><td class='right'>".$version_date."</td></tr>";
	unset($rows[$mod_code]);
}


foreach($menu as $mod_code => $m) {
	if(!in_array($mod_code, $ias_modules)) {
		$mod_name = $m['mod_name'];
		$mod_ref = $m['mod_ref'];
		$version_row = isset($rows[$mod_code]) ? $rows[$mod_code] : false;
		$version = $version_row === false ? "-" : $mod_code.".".$version_row['yt_issue'].".".$version_row['yt_count'];
		$version_date = $version_row === false ? "-" : date("d F Y", strtotime($version_row['yt_resolve_date']));
		echo "<tr><td class='center'>$mod_code</td><td class='center'>$mod_ref</td><td>".$mod_name.($version_row===false?"*":"")."</td><td class='right'>".$version."</td><td class='right'>".$version_date."</td></tr>";
	}
}



?>
</table>
<p class=i style='font-size: 100%'>* indicates modules which are no longer under development and which pre-date the current version tracking system.</p>
<p class=i style='font-size: 100%'>Report drawn on <?php echo date("d F Y"); ?> at <?php echo date("H:i:s"); ?>.</p>
<?php
//ASSIST_HELPER::arrPrint($menu);
//ASSIST_HELPER::arrPrint($rows);
//die();
?>
<script type="text/javascript">
$(function() {
	$("#btn_history").button().addClass("no-print").click(function() {
		document.location.href = "sysreport_versions.php?dt=<?php echo "".date("Y-m-d",$prev_fin_year)."";?>";
	});
	$("#btn_today").button().addClass("no-print").click(function() {
		document.location.href = "sysreport_versions.php";
	});
});
</script>
