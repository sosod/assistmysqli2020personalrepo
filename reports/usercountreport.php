<?php 
require_once "../header.php";

function nf($v) {
	//return ($v>0 ? number_format($v,0) : "-");
	return $v;
}

//error_reporting(-1);
/* GET VALID DATABASES */
$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}
mysql_close($link);

//error_reporting(-1);

?>
<style type=text/css>
.c { text-align: center; }
.a { border-left: 1px solid #000099; }
.t { font-style: none; font-weight: bold; }
.gt { font-weight: bold; font-style: normal; font-size: 10pt; }
.total { font-weight: bold; border-bottom: 2px solid #000099; border-top: 2px solid #000099; }
.alt { background-color: #e7e7ff; }
.res { background-color: #555555; font-weight: bold; color: #ffffff; }
.gt.res { text-decoration: underline; }
</style>

<h1>User Count Summary</h1> 
<p style="margin-top: -5px;"><i>Report drawn based on the users as at <u><?php echo(date("d F Y H:i")); ?></u></i></p>
<p>Note: This summary automatically excludes the User Directory module, Ignite Advisory Services modules, and the Assist Administrator and Assist Support users.</p>
<?php
$inactive = false;
$company = array();
$multiple_resellers = false;
$fields = "c1.cmpcode, c1.cmpreseller, c1.cmpname, c1.cmpstatus, c1.cmp_price_class, c1.cmp_is_csi, c1.cmp_bill_hosting";
/* SCENARIOS: $_REQUEST['cmpcode']== "ALL_ALLCMP" || "ALL_ABC1234" || "ABC1234" */
if(!isset($_REQUEST['cmpcode']) || strlen($_REQUEST['cmpcode'])<7) {
	//If no valid company code, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
} elseif(strlen($_REQUEST['cmpcode'])==7) {
	//If single company selected
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S') AND c2.cmpcode = '".strtoupper($_REQUEST['cmpcode'])."'
			WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') AND c1.cmp_price_class <> 'X'";
	$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
} elseif(substr(strtoupper($_REQUEST['cmpcode']),0,4)=="ALL_") {
	//If display all companies or all resellers clients
	$res = explode("_",$_REQUEST['cmpcode']);
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		//get all resellers clients for IASSIST
		$sql = "SELECT DISTINCT $fields FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND (c2.cmpstatus = 'Y' OR c2.cmpstatus = 'S')
				WHERE (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') AND c1.cmp_is_demo = 0 AND c1.cmp_price_class <> 'X' ".($res[1]!="ALLCMP"&&$res[1]!="DEMO" ? " AND c1.cmpcode = '".$res[1]."'" : "");
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
		$multiple_resellers = true;
	} else {
		//get only my clients
		$res[1] = strtoupper($_SESSION['cc']);	//update $res[1] in case res[1] = "ALLCMP";
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>$_SESSION['ia_cmp_name']);
	}
	$sql = "SELECT $fields FROM assist_company c1 WHERE c1.cmpreseller IN ('".strtoupper(implode("','",array_keys($resellers)))."') AND c1.cmpcode NOT IN ('IASSIST','BLANK') AND (c1.cmpstatus = 'Y' OR c1.cmpstatus = 'S') AND c1.cmp_is_demo = 0 AND c1.cmp_price_class <> 'X' ORDER BY c1.cmpreseller, c1.cmpcode, c1.cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");
} else {
	//no valid scenario, then die
	die("<p>An error occurred while trying to draw the report.  Please try again.</p>");
}






/* GET COUNTS */
$activity = array();
$all_counts = array();
$all_inact_counts = array();
$counts = array();
$non_counts = array();
$ias_counts = array();
$ias_inact_counts = array();
$cmp_counts = array();
$cmp_counts['res'] = array();
$all_cmp_counts = array();
$all_cmp_counts['res'] = array();
$all_inact_cmp_counts = array();
$all_inact_cmp_counts['res'] = array();
$non_cmp_counts = array();
$non_cmp_counts['res'] = array();
$ias_cmp_counts = array();
$ias_cmp_counts['res'] = array();
$ias_inact_cmp_counts = array();
$ias_inact_cmp_counts['res'] = array();
foreach($resellers as $rkey => $res) {
	$cmp_counts['res'][$rkey] = array();
	$non_cmp_counts['res'][$rkey] = array();
	$ias_cmp_counts['res'][$rkey] = array();
	foreach($company[$rkey] as $ckey => $cmp) {
		$cc = strtolower($ckey);
		//$counts[$cc] = array();
		if(in_array($db_other.$cc,$dbs)) {
			//GET TOTAL USERS FROM TIMEKEEP TABLE GROUP BY TKSTATUS
			$sql = "SELECT tkstatus, count(tkid) as c FROM assist_".$cc."_timekeep 
					WHERE tkstatus <> 0 AND tkuser <> 'support' AND tkuser <> 'admin' 
					GROUP BY tkstatus";
//AND tkid IN (SELECT DISTINCT usrtkid FROM assist_".$cc."_menu_modules_users WHERE usrmodref NOT IN ('PMSPS','PP','AQA'))
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($resellers[$rkey]['users']['tk1'])) { $resellers[$rkey]['users']['tk1'] = 0; }
				if(!isset($resellers[$rkey]['users']['tk2'])) { $resellers[$rkey]['users']['tk2'] = 0; }
				if(!isset($company[$rkey][$ckey]['tk1'])) { $company[$rkey][$ckey]['tk1'] = 0;} 
				if(!isset($company[$rkey][$ckey]['tk2'])) { $company[$rkey][$ckey]['tk2'] = 0;} 
				$company[$rkey][$ckey]['tk'.$row['tkstatus']] = $row['c'];
				$resellers[$rkey]['users']['tk'.$row['tkstatus']] += $row['c'];
			}
			unset($rs);
			//Get last user activity
			$sql = "SELECT a.date as adate FROM assist_".$cc."_timekeep_activity a ORDER BY a.id DESC LIMIT 1";
			$rs = mysql_getRS($sql,"O",$cc);
				$row = mysql_fetch_array($rs);
				$activity[$cc] = date("d M Y H:i",strtotime($row['adate']));
			unset($rs);
	//COUNT ACTIVE USERS PER BILLABLE MODULE
			$sql = "SELECT tk.tkid, COUNT(DISTINCT mmu.usrmodref) as mc
					FROM `assist_".$cc."_timekeep` tk 
					INNER JOIN assist_".$cc."_menu_modules_users mmu
					  ON mmu.usrtkid = tk.tkid
					  AND mmu.usrmodref NOT IN ('PMSPS','PP','AQA','TD')
					INNER JOIN assist_menu_modules m
					  ON m.modref = mmu.usrmodref
					  AND m.mod_billable = 1
					  AND m.modyn = 'Y'
					  AND m.modref NOT IN ('TD','PMSPS','PP','AQA')
					WHERE tk.tkuser <> 'support' AND tk.tkuser <> 'admin' AND tkstatus = 1
					GROUP BY tk.tkid
					ORDER BY COUNT(DISTINCT mmu.usrmodref)"; //echo $cc=="ove0002" ? $sql : "";
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($counts[$row['mc']][$cc])) {
					$counts[$row['mc']][$cc] = 1;
				} else {
					$counts[$row['mc']][$cc] += 1;
				}
				if(!isset($cmp_counts['res'][$rkey][$row['mc']])) {
					$cmp_counts['res'][$rkey][$row['mc']]=1;
				} else {
					$cmp_counts['res'][$rkey][$row['mc']]+=1;
				}
				if(!isset($cmp_counts[$cc])) {
					$cmp_counts[$cc]=1;
				} else {
					$cmp_counts[$cc]+=1;
				}
				/*$modref = $row['modref'];
				$modlocation = $row['modlocation'];
				if($modlocation=="DOC") {
					$modref = "DOC";
				} elseif(in_array($modref,$doc_modules)) { 
					$modref = 'DOC1'; 
				} elseif(in_array($modref,$action_modules) || $modlocation == "ACTION") { 
					$modref = "ACTION"; 
				} elseif(in_array($modref,$ud_modules)) { 
					$modref = "UD"; 
				} elseif(in_array($modref,$pstl_modules)) { 
					$modref = "PSTL"; 
				} elseif(in_array($modref,$sdp12_modules)) { 
					$modref = "SDP12"; 
				} elseif(in_array($modref,$risk_modules)) { 
					$modref = "RISK"; 
				} elseif(in_array($modref,$dash_modules)) { 
					$modref = "DASH"; 
				} 
				$counts[$cc][$modref][$row['tkstatus']]+=$row['c'];
				$counts['res'][$rkey][$modref][$row['tkstatus']]+=$row['c'];
				if(!in_array($modref,$mods)) { $mods[] = $modref; }
				$modules[$modref][$row['tkstatus']]['total']+=$row['c'];*/
			}
			unset($rs);
	//COUNT ACTIVE USERS PER NON-BILLABLE MODULE
			$sql = "SELECT tk.tkid, COUNT(DISTINCT mmu.usrmodref) as mc
					FROM `assist_".$cc."_timekeep` tk 
					INNER JOIN assist_".$cc."_menu_modules_users mmu
					  ON mmu.usrtkid = tk.tkid
					  AND mmu.usrmodref NOT IN ('PMSPS','PP','AQA','TD')
					INNER JOIN assist_menu_modules m
					  ON m.modref = mmu.usrmodref
					  AND m.mod_billable = 0
					  AND m.modyn = 'Y'
					  AND m.modref NOT IN ('TD','PMSPS','PP','AQA')
					  AND (m.modtext NOT LIKE 'Dashboard%' OR (m.modtext LIKE 'Dashboard%' AND m.modid = (SELECT max(modid) FROM assist_menu_modules WHERE modtext LIKE 'Dashboard%')))
					  AND m.modtext NOT LIKE 'SDBIP%'
					WHERE tk.tkuser <> 'support' AND tk.tkuser <> 'admin' AND tkstatus = 1
					GROUP BY tk.tkid
					ORDER BY COUNT(DISTINCT mmu.usrmodref)"; 

//echo $cc=="ove0002" ? $sql : "";
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($non_counts[$row['mc']][$cc])) {
					$non_counts[$row['mc']][$cc] = 1;
				} else {
					$non_counts[$row['mc']][$cc] += 1;
				}
				if(!isset($non_cmp_counts['res'][$rkey][$row['mc']])) {
					$non_cmp_counts['res'][$rkey][$row['mc']]=1;
				} else {
					$non_cmp_counts['res'][$rkey][$row['mc']]+=1;
				}
				if(!isset($non_cmp_counts[$cc])) {
					$non_cmp_counts[$cc]=1;
				} else {
					$non_cmp_counts[$cc]+=1;
				}
			}
			unset($rs);
	//COUNT ACTIVE USERS PER IAS MODULE
			$sql = "SELECT tk.tkid, COUNT(DISTINCT mmu.usrmodref) as mc
					FROM `assist_".$cc."_timekeep` tk 
					INNER JOIN assist_".$cc."_menu_modules_users mmu
					  ON mmu.usrtkid = tk.tkid
					  AND mmu.usrmodref IN ('PMSPS','PP','AQA')
					INNER JOIN assist_menu_modules m
					  ON m.modref = mmu.usrmodref
					  AND m.modyn = 'Y'
					  AND m.modref IN ('PMSPS','PP','AQA')
					WHERE tk.tkuser <> 'support' AND tk.tkuser <> 'admin' AND tkstatus = 1
					GROUP BY tk.tkid
					ORDER BY COUNT(DISTINCT mmu.usrmodref)"; //echo $cc=="ove0002" ? $sql : "";
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($ias_counts[$row['mc']][$cc])) {
					$ias_counts[$row['mc']][$cc] = 1;
				} else {
					$ias_counts[$row['mc']][$cc] += 1;
				}
				if(!isset($ias_cmp_counts['res'][$rkey][$row['mc']])) {
					$ias_cmp_counts['res'][$rkey][$row['mc']]=1;
				} else {
					$ias_cmp_counts['res'][$rkey][$row['mc']]+=1;
				}
				if(!isset($ias_cmp_counts[$cc])) {
					$ias_cmp_counts[$cc]=1;
				} else {
					$ias_cmp_counts[$cc]+=1;
				}
			}
			unset($rs);
	//COUNT INACTIVE USERS PER IAS MODULE
			$sql = "SELECT tk.tkid, COUNT(DISTINCT mmu.usrmodref) as mc
					FROM `assist_".$cc."_timekeep` tk 
					INNER JOIN assist_".$cc."_menu_modules_users mmu
					  ON mmu.usrtkid = tk.tkid
					  AND mmu.usrmodref IN ('PMSPS','PP','AQA')
					INNER JOIN assist_menu_modules m
					  ON m.modref = mmu.usrmodref
					  AND m.modyn = 'Y'
					  AND m.modref IN ('PMSPS','PP','AQA')
					WHERE tk.tkuser <> 'support' AND tk.tkuser <> 'admin' AND tkstatus = 2
					GROUP BY tk.tkid
					ORDER BY COUNT(DISTINCT mmu.usrmodref)"; //echo $cc=="ove0002" ? $sql : "";
			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($ias_inact_counts[$row['mc']][$cc])) {
					$ias_inact_counts[$row['mc']][$cc] = 1;
				} else {
					$ias_inact_counts[$row['mc']][$cc] += 1;
				}
				if(!isset($ias_inact_cmp_counts['res'][$rkey][$row['mc']])) {
					$ias_inact_cmp_counts['res'][$rkey][$row['mc']]=1;
				} else {
					$ias_inact_cmp_counts['res'][$rkey][$row['mc']]+=1;
				}
				if(!isset($ias_inact_cmp_counts[$cc])) {
					$ias_inact_cmp_counts[$cc]=1;
				} else {
					$ias_inact_cmp_counts[$cc]+=1;
				}
			}
			unset($rs);
	//COUNT ACTIVE USERS PER BILL+IAS MODULES
			$sql = "SELECT tk.tkid, COUNT(DISTINCT mmu.usrmodref) as mc
					FROM `assist_".$cc."_timekeep` tk 
					INNER JOIN assist_".$cc."_menu_modules_users mmu
					  ON mmu.usrtkid = tk.tkid
					  AND mmu.usrmodref NOT IN ('TD')
					INNER JOIN assist_menu_modules m
					  ON m.modref = mmu.usrmodref
					  AND (m.mod_billable = 1 OR m.modref IN ('PMSPS','PP','AQA'))
					  AND m.modyn = 'Y'
					  AND m.modref NOT IN ('TD')
					WHERE tk.tkuser <> 'support' AND tk.tkuser <> 'admin' AND tkstatus = 1
					GROUP BY tk.tkid
					ORDER BY COUNT(DISTINCT mmu.usrmodref)"; 

			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($all_counts[$row['mc']][$cc])) {
					$all_counts[$row['mc']][$cc] = 1;
				} else {
					$all_counts[$row['mc']][$cc] += 1;
				}
				if(!isset($all_cmp_counts['res'][$rkey][$row['mc']])) {
					$all_cmp_counts['res'][$rkey][$row['mc']]=1;
				} else {
					$all_cmp_counts['res'][$rkey][$row['mc']]+=1;
				}
				if(!isset($all_cmp_counts[$cc])) {
					$all_cmp_counts[$cc]=1;
				} else {
					$all_cmp_counts[$cc]+=1;
				}
			}
			unset($rs);
	//COUNT INACTIVE USERS PER BILLABLE+IAS MODULES
			$sql = "SELECT tk.tkid, COUNT(DISTINCT mmu.usrmodref) as mc
					FROM `assist_".$cc."_timekeep` tk 
					INNER JOIN assist_".$cc."_menu_modules_users mmu
					  ON mmu.usrtkid = tk.tkid
					  AND mmu.usrmodref NOT IN ('TD')
					INNER JOIN assist_menu_modules m
					  ON m.modref = mmu.usrmodref
					  AND (m.modref IN ('PMSPS','PP','AQA'))
					  AND m.modyn = 'Y'
					  AND m.modref NOT IN ('TD')
					WHERE tk.tkuser <> 'support' AND tk.tkuser <> 'admin' AND tkstatus = 2
					GROUP BY tk.tkid
					ORDER BY COUNT(DISTINCT mmu.usrmodref)"; 

			$rs = mysql_getRS($sql,"O",$cc);
			while($row = mysql_fetch_array($rs)) {
				if(!isset($all_inact_counts[$row['mc']][$cc])) {
					$all_inact_counts[$row['mc']][$cc] = 1;
				} else {
					$all_inact_counts[$row['mc']][$cc] += 1;
				}
				if(!isset($all_inact_cmp_counts['res'][$rkey][$row['mc']])) {
					$all_inact_cmp_counts['res'][$rkey][$row['mc']]=1;
				} else {
					$all_inact_cmp_counts['res'][$rkey][$row['mc']]+=1;
				}
				if(!isset($all_inact_cmp_counts[$cc])) {
					$all_inact_cmp_counts[$cc]=1;
				} else {
					$all_inact_cmp_counts[$cc]+=1;
				}
			}
			unset($rs);			
		} else {
			unset($company[$rkey][$ckey]);
		}	//end if in db list
	}	//end foreach company[rkey]
}	//end foreach reseller
sort($mods);

ksort($counts);

//echo "<pre>"; print_r($counts); echo "</pre>";
//echo "<pre>"; print_r($all_counts); echo "</pre>";

//if(in_array("PMSPS",$mods)) {
	 $inactive = true; 
//}

	$ultimate_total = array('bill'=>0);
	$all_ultimate_total = array('bill'=>0);
	$all_inact_ultimate_total = array('bill'=>0);
	$non_ultimate_total = array('bill'=>0);
	$ias_ultimate_total = array('bill'=>0);
	$ias_inact_ultimate_total = array('bill'=>0);
/* DISPLAY REPORT */
if($inactive) { $span = 2; }  else { $span = 1; }
echo "
<table id=tbl_report><thead>
	<tr>
	<th rowspan=$span colspan=3>Company</th>
	<th colspan=$span>Total<br />Registered<br />Users</th>
	<th rowspan=$span>Total<br />Billable<br />Users</th>
	<th colspan=".(count($counts) > 0 ? count($counts) : 1).">Billable Module Count<br /><span style='font-weight: normal; font-size: 75%' class=i>Heading = Number of modules;<br />Value = No. of ACTIVE users with access to the given no. of modules</span></th>
	<th colspan=".(count($non_counts) > 0 ? count($non_counts) : 1).">Non-Billable Module Count<br /><span style='font-weight: normal; font-size: 75%' class=i>Heading = Number of modules;<br />Value = No. of ACTIVE users with access to the given no. of modules</span></th>
	<th colspan=".(count($ias_counts) > 0 ? count($ias_counts) : 1).">Ignite Advisory Services Module Count for Active users<br /><span style='font-weight: normal; font-size: 75%' class=i>Heading = Number of modules;<br />Value = No. of ACTIVE users with access to the given no. of modules</span></th>
	<th colspan=".(count($ias_inact_counts) > 0 ? count($ias_inact_counts) : 1).">Ignite Advisory Services Module Count for Inactive users<br /><span style='font-weight: normal; font-size: 75%' class=i>Heading = Number of modules;<br />Value = No. of INACTIVE users with access to the given no. of modules</span></th>
	<th colspan=".(count($all_counts) > 0 ? count($all_counts) : 1).">Billable & IAS Module Count<br /><span style='font-weight: normal; font-size: 75%' class=i>Heading = Number of modules;<br />Value = No. of ACTIVE users with access to the given no. of modules</span></th>
	<th colspan=".(count($all_inact_counts) > 0 ? count($all_inact_counts) : 1).">Billable & IAS Module Count<br /><span style='font-weight: normal; font-size: 75%' class=i>Heading = Number of modules;<br />Value = No. of INACTIVE users with access to the given no. of modules</span></th>
	<th rowspan=$span>Last User<br />Activity</th>";
	/*foreach($mods as $m) {
		echo "<th ".($m=="PMSPS"?"colspan=2 rowspan=1":"rowspan=$span").">";
		if(isset($modules[$m]['value']) ) { 
			echo $modules[$m]['value'];
		} else {
			echo $m;
		}
		echo "</th>";
	}*/
	echo "	
	</tr>";
	if($span > 1) {
		echo "<tr>
			<th>Active</th>
			<th>Inactive</th>";
		if(count($counts)>0) {
			foreach($counts as $key => $mc) {
				$ultimate_total[$key] = 0;
				echo "<th>$key</th>";
			}
		} else {
			echo "<th>N/A</th>";
		}
		if(count($non_counts)>0) {
			foreach($non_counts as $key => $mc) {
				$non_ultimate_total[$key] = 0;
				echo "<th>$key</th>";
			}
		} else {
			echo "<th>N/A</th>";
		}
		if(count($ias_counts)>0) {
			foreach($ias_counts as $key => $mc) {
				$ias_ultimate_total[$key] = 0;
				echo "<th>$key</th>";
			}
		} else {
			echo "<th>N/A</th>";
		}
		if(count($ias_inact_counts)>0) {
			foreach($ias_inact_counts as $key => $mc) {
				$ias_inact_ultimate_total[$key] = 0;
				echo "<th>$key</th>";
			}
		} else {
			echo "<th>N/A</th>";
		}
		if(count($all_counts)>0) {
			foreach($all_counts as $key => $mc) {
				$all_ultimate_total[$key] = 0;
				echo "<th>$key</th>";
			}
		} else {
			echo "<th>N/A</th>";
		}
		
		if(count($all_inact_counts)>0) {
			foreach($all_inact_counts as $key => $mc) {
				$all_inact_ultimate_total[$key] = 0;
				echo "<th>$key</th>";
			}
		} else {
			echo "<th>N/A</th>";
		}
		
		echo "
		</tr>";
	}
	echo "
	</thead><tbody>
	";

	$tr = 0;
	$totalact = 0; $totalinact = 0;
foreach($resellers as $rkey => $res) {
	$res_total = array(1=>0, 2=>0);
	if($multiple_resellers) {
		echo "
		<tr class=res>
			<td class=res>".$rkey."</td>
			<td class=res>".$res['cmpname']."</td>
			<td class=res>".($res['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active")."</td>
				<td class='res c'>".nf($res['users']['tk1'])."</td>
				".($inactive ? "<td class='res c'>".nf($res['users']['tk2'])."</td>" : "")
				."<td class='res b c'>".(isset($cmp_counts['res'][$rkey]) ? array_sum($cmp_counts['res'][$rkey]) : nf(0))."</td>";
				$ultimate_total['bill'] += (isset($cmp_counts['res'][$rkey]) ? array_sum($cmp_counts['res'][$rkey]) : 0);
				$all_ultimate_total['bill'] += (isset($all_cmp_counts['res'][$rkey]) ? array_sum($all_cmp_counts['res'][$rkey]) : 0);
				$all_inact_ultimate_total['bill'] += (isset($all_inact_cmp_counts['res'][$rkey]) ? array_sum($all_inact_cmp_counts['res'][$rkey]) : 0);
				$non_ultimate_total['bill'] += (isset($cmp_counts['res'][$rkey]) ? array_sum($cmp_counts['res'][$rkey]) : 0);
				$ias_ultimate_total['bill'] += (isset($cmp_counts['res'][$rkey]) ? array_sum($cmp_counts['res'][$rkey]) : 0);
				$ias_inact_ultimate_total['bill'] += (isset($cmp_counts['res'][$rkey]) ? array_sum($cmp_counts['res'][$rkey]) : 0);
				if(count($counts) > 0) {
					foreach($counts as $key => $c) {
						echo "<td class='res c'>".(isset($cmp_counts['res'][$rkey][$key]) ? $cmp_counts['res'][$rkey][$key] : nf(0))."</td>";
						$ultimate_total[$key]+=(isset($cmp_counts['res'][$rkey][$key]) ? $cmp_counts['res'][$rkey][$key] : 0);
					}
				} else {
					echo "<td class='res c'>N/A</td>";
				}
				if(count($counts) > 0) {
					foreach($non_counts as $key => $c) {
						echo "<td class='res c'>".(isset($non_cmp_counts['res'][$rkey][$key]) ? $non_cmp_counts['res'][$rkey][$key] : nf(0))."</td>";
						$non_ultimate_total[$key]+=(isset($non_cmp_counts['res'][$rkey][$key]) ? $non_cmp_counts['res'][$rkey][$key] : 0);
					}
				} else {
					echo "<td class='res c'>N/A</td>";
				}
				if(count($ias_counts) > 0) {
					foreach($ias_counts as $key => $c) {
						echo "<td class='res c'>".(isset($ias_cmp_counts['res'][$rkey][$key]) ? $ias_cmp_counts['res'][$rkey][$key] : nf(0))."</td>";
						$ias_ultimate_total[$key]+=(isset($ias_cmp_counts['res'][$rkey][$key]) ? $ias_cmp_counts['res'][$rkey][$key] : 0);
					}
				} else {
					echo "<td class='res c'>N/A</td>";
				}
				if(count($ias_inact_counts) > 0) {
					foreach($ias_inact_counts as $key => $c) {
						echo "<td class='res c'>".(isset($ias_inact_cmp_counts['res'][$rkey][$key]) ? $ias_inact_cmp_counts['res'][$rkey][$key] : nf(0))."</td>";
						$ias_inact_ultimate_total[$key]+=(isset($ias_inact_cmp_counts['res'][$rkey][$key]) ? $ias_inact_cmp_counts['res'][$rkey][$key] : 0);
					}
				} else {
					echo "<td class='res c'>N/A</td>";
				}
				if(count($all_counts) > 0) {
					foreach($all_counts as $key => $c) {
						echo "<td class='res c'>".(isset($all_cmp_counts['res'][$rkey][$key]) ? $all_cmp_counts['res'][$rkey][$key] : nf(0))."</td>";
						$all_ultimate_total[$key]+=(isset($all_cmp_counts['res'][$rkey][$key]) ? $all_cmp_counts['res'][$rkey][$key] : 0);
					}
				} else {
					echo "<td class='res c'>N/A</td>";
				}
				if(count($all_inact_counts) > 0) {
					foreach($all_inact_counts as $key => $c) {
						echo "<td class='res c'>".(isset($all_inact_cmp_counts['res'][$rkey][$key]) ? $all_inact_cmp_counts['res'][$rkey][$key] : nf(0))."</td>";
						$all_inact_ultimate_total[$key]+=(isset($all_inact_cmp_counts['res'][$rkey][$key]) ? $all_inact_cmp_counts['res'][$rkey][$key] : 0);
					}
				} else {
					echo "<td class='res c'>N/A</td>";
				}
				
				echo "<td></td>";
				/*foreach($mods as $m) {
					$j = $m=="PMSPS" ? 3  : 2;
					for($i=1;$i<$j;$i++) {
						echo "<td class='res c'>".nf($counts['res'][$rkey][$m][$i])."</td>";
						$res_total[$i]+=$counts['res'][$rkey][$m][$i];
					}
				}*/
				/*echo "
					<td class='c a t res'>".nf($res_total[1])."</td>
					".($inactive ? "<td class='c t res'>".nf($res_total[2])."</td>" : "")."
					<td class='c a gt res'>".nf(array_sum($res_total))."</td>
				</tr>";*/
	}
	foreach($company[$rkey] as $ckey => $cmp) {
		$cc = strtolower($ckey);
		if(in_array($db_other.$cc,$dbs)) {
			$total = array(1=>0,2=>0,3=>0);
			echo "<tr height=50 class=cmp>";
			$totalact+=$cmp['tk1'];
			$totalinact += isset($cmp['tk2']) ? $cmp['tk2'] : 0;
			echo "
			<td class=b>".$ckey."</td>
			<td class=b>".$cmp['cmpname']."</td>
			<td class=b>".($cmp['cmpstatus']=="S"?"<span class=red>Suspended</span>":"Active")."</td>
			<td class='b c'>".nf($cmp['tk1'])."</td>
			".($inactive ? "<td class='b c'>".nf(isset($cmp['tk2']) ? $cmp['tk2'] : 0)."</td>" : "")
			."<td class='b c'>".(isset($cmp_counts[$cc]) ? $cmp_counts[$cc] : nf(0))."</td>";
			if(count($counts)>0) {
				foreach($counts as $key => $c) {
					echo "<td class=c>".(isset($c[$cc]) ? $c[$cc] : nf(0))."</td>";
				}
			} else {
				echo "<td class=c>N/A</td>";
			}
			if(count($non_counts)>0) {
				foreach($non_counts as $key => $c) {
					echo "<td class=c>".(isset($c[$cc]) ? $c[$cc] : nf(0))."</td>";
				}
			} else {
				echo "<td class=c>N/A</td>";
			}
			if(count($ias_counts)>0) {
				foreach($ias_counts as $key => $c) {
					echo "<td class=c>".(isset($c[$cc]) ? $c[$cc] : nf(0))."</td>";
				}
			} else {
				echo "<td class=c>N/A</td>";
			}
			if(count($ias_inact_counts)>0) {
				foreach($ias_inact_counts as $key => $c) {
					echo "<td class=c>".(isset($c[$cc]) ? $c[$cc] : nf(0))."</td>";
				}
			} else {
				echo "<td class=c>N/A</td>";
			}
			if(count($all_counts)>0) {
				foreach($all_counts as $key => $c) {
					echo "<td class=c>".(isset($c[$cc]) ? $c[$cc] : nf(0))."</td>";
				}
			} else {
				echo "<td class=c>N/A</td>";
			}
			if(count($all_inact_counts)>0) {
				foreach($all_inact_counts as $key => $c) {
					echo "<td class=c>".(isset($c[$cc]) ? $c[$cc] : nf(0))."</td>";
				}
			} else {
				echo "<td class=c>N/A</td>";
			}
						
			echo "<td>".$activity[$cc]."</td>";
			/*foreach($mods as $m) {
				$j = $m=="PMSPS" ? 3  : 2;
				for($i=1;$i<$j;$i++) {
					$v = isset($counts[$cc][$m][$i]) ? $counts[$cc][$m][$i] : 0;
					echo "<td class=c>".nf($v)."</td>";
					$total[$i] += $v;
				}
			}*/
			/*echo "
				<td class='c a t '>".nf($total[1])."</td>
				".($inactive ? "<td class='c t'>".nf($total[2])."</td>" : "")."
				<td class='c a gt'>".nf(array_sum($total))."</td>";
			$ultimate_total[1]+=$total[1];
			$ultimate_total[2]+=$total[2];*/
			echo "</tr>";
		}
	}
}
	
	echo "</tbody><tfoot>
	<tr>
		<th class=right colspan=3>TOTAL:</th>";
$class = "total c";
	echo "<td class=\"$class\">&nbsp;".nf($totalact)."&nbsp;</td>";
	if($inactive) { echo "<td class=\"$class\">&nbsp;".nf($totalinact)."&nbsp;</td>"; }
	echo "<td class=\"$class\">&nbsp;".nf($ultimate_total['bill'])."&nbsp;</td>";
	if(count($counts)>0) {
		foreach($counts as $key=>$c){
			echo "<td class=\"$class\">&nbsp;".nf($ultimate_total[$key])."&nbsp;</td>";
		}
	} else {
		echo "<td class=\"$class\">N/A</td>";
	}
	if(count($non_counts)>0) {
		foreach($non_counts as $key=>$c){
			echo "<td class=\"$class\">&nbsp;".nf($non_ultimate_total[$key])."&nbsp;</td>";
		}
	} else {
		echo "<td class=\"$class\">N/A</td>";
	}
	if(count($ias_counts)>0) {
		foreach($ias_counts as $key=>$c){
			echo "<td class=\"$class\">&nbsp;".nf($ias_ultimate_total[$key])."&nbsp;</td>";
		}
	} else {
		echo "<td class=\"$class\">N/A</td>";
	}
	if(count($ias_inact_counts)>0) {
		foreach($ias_inact_counts as $key=>$c){
			echo "<td class=\"$class\">&nbsp;".nf($ias_inact_ultimate_total[$key])."&nbsp;</td>";
		}
	} else {
		echo "<td class=\"$class\">N/A</td>";
	}
	if(count($all_counts)>0) {
		foreach($all_counts as $key=>$c){
			echo "<td class=\"$class\">&nbsp;".nf($all_ultimate_total[$key])."&nbsp;</td>";
		}
	} else {
		echo "<td class=\"$class\">N/A</td>";
	}
	if(count($all_inact_counts)>0) {
		foreach($all_inact_counts as $key=>$c){
			echo "<td class=\"$class\">&nbsp;".nf($all_inact_ultimate_total[$key])."&nbsp;</td>";
		}
	} else {
		echo "<td class=\"$class\">N/A</td>";
	}

	echo "<td class=\"$class\">&nbsp;&nbsp;</td>";
	/*foreach($mods as $m) {
		$j = $m=="PMSPS" ? 3  : 2;
		for($i=1;$i<$j;$i++) {
			echo "<td class='total c'>".nf($modules[$m][$i]['total'])."</td>";
		}
	} */
	/*echo "
	<th>&nbsp;".nf($ultimate_total[1])."&nbsp;</th>
	".($inactive ? "<th>&nbsp;".nf($ultimate_total[2])."&nbsp;</th>" : "" )."
	<th>&nbsp;<u>".nf(array_sum($ultimate_total))."</u>&nbsp;</th>";*/
	echo "</tr></tfoot>
</table>";
?>
<p>&nbsp;</p>
<script type=text/javascript>
$(function() {
	$("#tbl_report tbody tr.cmp:odd").addClass("alt");
});
</script>
</body></html>