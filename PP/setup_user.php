<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $user_setup = new UserSetup();
  $user_setup->init( $_REQUEST );
  $user_setup->save();
	
	ViewHelper::set_session_message( "setup_users", "User setup successfully saved." );
  ViewHelper::redirect( "setup_users.php" );
}

$user_setup = new UserSetup();
$user_setup->load_from_user_id( $_REQUEST["user_id"] );
if( $user_setup->is_transient() )
{
  $user_setup->user_id = $_REQUEST["user_id"];
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Users" => "setup_users.php", $user_setup->get_user()->get_full_name() => null ) ); ?>

<? ViewHelper::validation( "frmUserSetup" ); ?>

<form id="frmUserSetup" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Module administrator</th>
      <td><?= ViewHelper::select( "is_mod_admin", ViewHelper::get_bool_select_list(), $user_setup->is_mod_admin, true, false ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $user_setup->id ?>" />
  <input type="hidden" name="user_id" value="<?= $user_setup->user_id ?>" />      
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_users.php" ) ); ?>

<? include("inc_footer.php"); ?>