<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $category = new Category();
  $category->init( $_REQUEST );
  $category->save();
	
	ViewHelper::set_session_message( "setup_categories", "Category successfully saved." );
  ViewHelper::redirect( "setup_categories.php" );
}

$category = new Category( $_REQUEST["id"] );

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => "setup_categories.php", $category->get_display_name() => null ) ); ?>

<? ViewHelper::validation( "frmCategory" ); ?>

<form id="frmCategory" method="post" >  
  <table class="form" cellspacing="0">
		<tr>
      <th>Name</th>
      <td><?= ViewHelper::text( "name", $category->name, true, false, "width:200px;" ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $category->id ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_categories.php" ) ); ?>

<? include("inc_footer.php"); ?>