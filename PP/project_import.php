<?
include("inc_ignite.php");

$test = $_REQUEST["test"];
$strict = $_REQUEST["strict"];

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
	echo "<p>TEST MODE = " . ( $test ? "ON" : "OFF" ) . "</p>";
	
	$target = "../files/" . get_company_code() . "/" . date("YmdHis") . "_" . basename( $_FILES['importFile']['name'] );
	
	echo "<p>TARGET FILE = " . $target . "</p>";
	
	if( move_uploaded_file( $_FILES['importFile']['tmp_name'], $target ) )
  {
  	echo "<p>The file ". basename( $_FILES['importFile']['name']). " has been successfully uploaded" . "</p>";
   
    $file_handle = fopen( $target, "r" );
    
    $cnt = 0;
		
		$cnt_saved = 0;
		$cnt_failed = 0;
		$cnt_invalid = 0;
		
    $doneWithHeader = false;
    
    while( !feof( $file_handle ) ) 
    {
      $record = fgetcsv( $file_handle );
      
      if( $doneWithHeader == false )
      {
        $doneWithHeader = true;
				
        continue;
      }
      
      //echo "<br/>" . var_export( $record, true );
      
			$validRecord = validateRecord( $record, $cnt );
			
      if( $validRecord )
      {
      	$project = new Project();
        $project->name = $record[2];
				$project->source = ( $strict == false && exists( $record[1] == false ) ? " " : $record[1] );
				$project->year = $record[4]; 
				$project->description = ( $strict == false && exists( $record[11] == false ) ? " " : $record[11] ); 
				$project->ward_id = getWard( $record[3] )->id; 
				$project->department_id = getDepartment( $record[0] )->id; 
				$project->value = $record[5]; 
				$project->weight = 0; 
				$project->status = "identified"; 
				$project->budget_1 = $record[6]; 
				$project->budget_2 = $record[7]; 
				$project->budget_3 = $record[8]; 
				$project->budget_4 = $record[9]; 
				$project->budget_5 = $record[10]; 
				$project->kpi_number = null; 
				$project->created_by_id = get_logged_in_user_id(); 
				$project->created_on = date( 'Y-m-d H:i:s' ); 
				$project->approved_by_id = null; 
				$project->approved_on = null;
				
				if( $test == false )
				{
				  $project->save();	
					
					if( exists( $project->id ) && $project->id != 0 )
					{
            echo "<p style='color:#070;'>Line " . ($cnt + 2) . ": Project, '" . $project->name . "', saved [" . $project->id . "].";
						$cnt_saved++;						
					}
					else
					{
						echo "<p style='color:#F60;'>Line " . ($cnt + 2) . ": Project, '" . $project->name . "', not saved. " . mysql_error(get_connection());
						$cnt_failed++;
					}
				}
				else
				{
					echo "<p style='color:#070;'>Line " . ($cnt + 2) . ": Project, '" . $project->name . "', processed.";
				}
      }  
			else
			{
				echo "<p style='color:#c00;'>Line " . ($cnt + 2) . ": Project data invalid</p>";
				$cnt_invalid++;
			}
			
			
			$cnt++;
    }
    
    echo "<p>" . $cnt . " records processed in total.</p>";
		echo "<p>" . $cnt_saved . " records saved.</p>";
		echo "<p>" . $cnt_failed . " records failed.</p>";
		echo "<p>" . $cnt_invalid . " records invalid.</p>";
    
    fclose( $file_handle );
  }
}

function validateRecord( $record, $cnt )
{
	global $strict;
	
  $hasErrors = false;
  
  if( count( $record ) != 12 )
  {
    printError( "Column count does not match expected count. Expected 12, found " . count( $record ), $cnt );
    $hasErrors = true;
  }
  
  /* RECORD[0] - VALIDATE DEPARTMENT */
  if( exists( $record[0] ) == false )
  {
    printError( "Missing value for column 'DEPARTMENT'", $cnt );
    $hasErrors = true;
  }
  else if( getDepartment( $record[0] ) == null )
  {
    printError( "Department name, '" . $record[0] . "', does not match any system department names", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[1] - VALIDATE SOURCE */
  if( $strict && exists( $record[1] ) == false )
  {
    printError( "Missing value for column 'SOURCE'", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[2] - VALIDATE PROJECT NAME */
  if( exists( $record[2] ) == false )
  {
    printError( "Missing value for column 'PROJECT NAME'", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[3] - VALIDATE WARD */
  if( exists( $record[3] ) == false )
  {
    printError( "Missing value for column 'WARD'", $cnt );
    $hasErrors = true;
  }
	else if( is_numeric( $record[3] ) == false )
	{
		printError( "WARD, '" . $record[3] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
	}
	else if( getWard( $record[3] ) == null )
  {
    printError( "WARD, '" . $record[3] . "', does not match any system wards", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[4] - VALIDATE START YEAR */
  if( exists( $record[4] ) == false )
  {
    printError( "Missing value for column 'START YEAR'", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[5] - VALIDATE TOTAL VALUE */
  if( exists( $record[5] ) == false )
  {
    printError( "Missing value for column 'TOTAL VALUE'", $cnt );
    $hasErrors = true;
  }
  else if( is_numeric( $record[5] ) == false )
  {
    printError( "TOTAL VALUE, '" . $record[5] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[6] - VALIDATE YEAR 1 */
  if( exists( $record[6] ) == false )
  {
    printError( "Missing value for column 'YEAR 1'", $cnt );
    $hasErrors = true;
  }
  else if( is_numeric( $record[6] ) == false )
  {
    printError( "YEAR 1, '" . $record[6] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[7] - VALIDATE YEAR 2 */
  if( exists( $record[7] ) == false )
  {
    printError( "Missing value for column 'YEAR 2'", $cnt );
    $hasErrors = true;
  }
  else if( is_numeric( $record[7] ) == false )
  {
    printError( "YEAR 2, '" . $record[7] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[8] - VALIDATE YEAR 3 */
  if( exists( $record[8] ) == false )
  {
    printError( "Missing value for column 'YEAR 3'", $cnt );
    $hasErrors = true;
  }
  else if( is_numeric( $record[8] ) == false )
  {
    printError( "YEAR 3, '" . $record[8] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[9] - VALIDATE YEAR 4 */
  if( exists( $record[9] ) == false )
  {
    printError( "Missing value for column 'YEAR 4'", $cnt );
    $hasErrors = true;
  }
  else if( is_numeric( $record[9] ) == false )
  {
    printError( "YEAR 4, '" . $record[9] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[10] - VALIDATE YEAR 5 */
  if( exists( $record[10] ) == false )
  {
    printError( "Missing value for column 'YEAR 5'", $cnt );
    $hasErrors = true;
  }
  else if( is_numeric( $record[10] ) == false )
  {
    printError( "YEAR 5, '" . $record[10] . "', is not a valid numeric value", $cnt );
    $hasErrors = true;
  }
	
	/* RECORD[11] - VALIDATE DESCRIPTION */
  if( $strict && exists( $record[11] ) == false )
  {
    printError( "Missing value for column 'DESCRIPTION'", $cnt );
    $hasErrors = true;
  }
  
  return $hasErrors == false;
}

function printError( $message, $cnt )
{
	global $test;
	
	if( $test )
	{
    echo "<p style='color:#c00;'>[ERROR] Line " . ($cnt + 2) . ": " . $message . "</p>";
	}
}

function getDepartment( $name )
{
  $department = new Department();
  $department->loadFromName( $name );
  
  if( exists( $department->id ) )
  {
    return $department;
  }
  
  return null;
}

function getWard( $number )
{
  $ward = new Ward();
  $ward->load_where( "number = " . $number );
  
  if( exists( $ward->id ) )
  {
    return $ward;
  }
  
  return null;
}

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Project Import" => null ) ); ?>

<? ViewHelper::validation( "frmProjectUpload" ); ?>

<form id="frmProjectUpload" enctype="multipart/form-data" action="project_import.php" method="post">
	<div>
    <label for="fldImportFile">Data File:</label>
		<input id="fldImportFile" name="importFile" type="file" class="required" />
	</div>
	<div>  
    <label for="fldTest">Test:</label>
    <input id="fldTest" name="test" type="checkbox" value="1" />
  </div>
	<div>  
    <label for="fldStrict">Strict:</label>
    <input id="fldStrict" name="strict" type="checkbox" value="1" />
  </div>
  <input type="submit" value="Submit" />
	<input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? include("inc_footer.php"); ?>