<?php
include("inc_ignite.php");

$type = get_param( "type" );

$ward_id = get_sql_param( "ward_id" );
$ward = new Ward( $ward_id );

$department_id = get_sql_param( "department_id" );
$department = new Department( $department_id );

$status = get_sql_param( "status" );
$year = get_sql_param( "year" );

$sql  = "SELECT p.id ";
$sql .= "FROM assist_".get_company_code()."_pp_project p ";
$sql .= "LEFT JOIN assist_".get_company_code()."_pp_ward w ON (p.ward_id = w.id) ";
$sql .= "LEFT JOIN assist_".get_company_code()."_list_dir d ON (p.department_id = d.dirid) ";
if( $type == "potential")
{
  $sql .= "WHERE p.status not in ('inprogress','completed') ";  
}
else if( $type == "approved")
{
  $sql .= "WHERE p.status not in ('identified','onhold') ";
}

if( exists($ward_id) )
{
  $sql .= "AND p.ward_id = " . quote($ward_id) . " ";
}

if( exists($department_id) )
{
  $sql .= "AND p.department_id = " . quote($department_id) . " ";
}       

if( exists($status) )
{
  $sql .= "AND p.status = " . quote($status) . " ";
}

if( exists($year) )
{
  $sql .= "AND p.year IN ( " . join( ",", db_budget_year_range( $year ) ) . " ) ";
}

$sql .= "ORDER BY p.weight desc ";

$project_ids = query_records( $sql );

$headers = array( "Name", "Source", "Year", "Description", "Ward", "Department", "Value", "Weight", "Status", "Budget 1", "Budget 2", "Budget 3", "Budget 4", "Budget 5", "KPI Number", "Criterias" );
$csv .= '"' . join( '","', str_replace( '"', '""', $headers ) ) . '"';
$csv .= "\015\012";

foreach( $project_ids as $project_id )
{
	$project = new Project( $project_id["id"] );
	
	foreach( $project->criterias as $criteria )
	{
		$criterias[] = $criteria->name . " (" . $criteria->weight . ")";
	}
	
	$row = array( $project->name, $project->source, $project->year, $project->description, $project->get_ward()->name, $project->get_department()->value, $project->value, $project->weight, ViewHelper::display_status($project->status), $project->budget_1, $project->budget_2, $project->budget_3, $project->budget_4, $project->budget_5, $project->kpi_number, join( ', ', str_replace( '"', '""', $criterias ) ) );
  $csv .= '"' . join( '","', str_replace( '"', '""', $row ) ) . '"';
	$csv .= "\015\012";
	
}

$size_in_bytes = strlen($csv);
header("Content-type: text/csv");
header("Content-disposition:  attachment; filename=" . $type . "_projects_" . date("Y-m-d").".csv; size=$size_in_bytes");
echo $csv; 
exit;
?>