<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $criteria = new Criteria();
  $criteria->init( $_REQUEST );
  $criteria->save();
	
	/* If the value of a criteria's weight field changes, 
	 * then we need to recalculate the weight field for all projects 
	 * that have this criteria attached to them */
	if( isset( $_REQUEST["id"] ) && $_REQUEST["old_weight"] != $criteria->weight )
	{
		$criteria->recalculate_project_weights();		
	}
	
	ViewHelper::set_session_message( "setup_criterias", "Criteria successfully saved." );
  ViewHelper::redirect( "setup_criterias.php?category_id={$criteria->category_id}" );
}

$criteria = new Criteria( $_REQUEST["id"] );

if( $criteria->is_transient() )
{
	$criteria->category_id = $_REQUEST["category_id"];
}

$category = new Category( $criteria->category_id );
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => "setup_categories.php", $category->name => null, "Criterias" => "setup_criterias.php?category_id={$category->id}", $criteria->get_display_name() => null ) ); ?>

<? ViewHelper::validation( "frmCriteria" ); ?>

<form id="frmCriteria" method="post" >  
  <table class="form" cellspacing="0">
		<tr>
      <th>Name</th>
      <td><?= ViewHelper::text( "name", $criteria->name, true, false, "width:200px;" ) ?></td>
    </tr>
		<tr>
      <th>Weight</th>
      <td><?= ViewHelper::select( "weight", ViewHelper::get_numeric_select_list( 1, 100 ), $criteria->weight, true, false ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $criteria->id ?>" />
	<input type="hidden" name="category_id" value="<?= $category->id ?>" />
	<input type="hidden" name="old_weight" value="<?= $criteria->weight ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_criterias.php?category_id={$category->id}" ) ); ?>

<? include("inc_footer.php"); ?>