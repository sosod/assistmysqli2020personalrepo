<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $ward = new Ward();
  $ward->init( $_REQUEST );
  $ward->save();
	
	ViewHelper::set_session_message( "setup_wards", "Ward successfully saved." );
  ViewHelper::redirect( "setup_wards.php" );
}

$ward = new Ward( $_REQUEST["id"] );

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Wards" => "setup_wards.php", $ward->get_display_name() => null ) ); ?>

<? ViewHelper::validation( "frmWard" ); ?>

<form id="frmWard" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Number</th>
      <td><?= ViewHelper::select( "number", ViewHelper::get_numeric_select_list( 1, 100 ), $ward->number, true, false ) ?></td>
    </tr>
		<tr>
      <th>Name</th>
      <td><?= ViewHelper::text( "name", $ward->name, true, false, "width:200px;" ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $ward->id ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_wards.php" ) ); ?>

<? include("inc_footer.php"); ?>