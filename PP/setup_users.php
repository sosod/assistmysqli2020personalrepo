<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Users" => null ) ); ?>

<? ViewHelper::get_session_message( "setup_users" ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:70%;">User</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
  foreach( User::get_records( false, "u.tkname, u.tksurname" ) as $obj )
  {
  ?>
  <tr>
    <td><?= $obj->get_full_name() ?></td>
    <td>
      <? ViewHelper::button( "setup_user.php?user_id={$obj->id}", "Configure" ); ?>
    </td>		
  </tr>
  <?
  }
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup.php" ) ); ?>

<?php include("inc_footer.php"); ?>