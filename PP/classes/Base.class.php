<?php
class Base
{
  private $table;
  private $fields;

  function __construct( $table, $fields, $id = false )
  {
    $this->table = $table;
    $this->fields = $fields;

		if( exists( $id ) )
		{
			$this->load( $id );
		}
  }

  function load( $id )
  {
    $sql = 'SELECT id, '.join( ",\n", $this->fields ).'
            FROM `assist_'.get_company_code().'_'.$this->table.'`
            WHERE id = '.$id;

    $this->init( get_record( get_resultset( $sql ) ) );
  }

	function load_where( $where )
  {
    $this->id = $id;

    $sql = 'SELECT id, '.join( ",\n", $this->fields ).'
            FROM `assist_'.get_company_code().'_'.$this->table.'`
            WHERE '.$where;

    $this->init( get_record( get_resultset( $sql ) ) );
  }

	function init( $data )
	{
    $this->id = $data['id'];

		foreach ( $this->fields as $field )
    {
      $this->$field = $data[$field];
    }
	}

  function save()
  {
    if( isset( $this->id ) && $this->id > 0 )
    {
      $this->update();
    }
    else
    {
      $this->insert();
    }
  }

  function update()
  {
    $sql = 'UPDATE `assist_'.get_company_code().'_'.$this->table.'`
            SET '.join( ",\n", $this->get_update_values() ).'
            WHERE id = '.$this->quote( $this->id );

	  execute_update( $sql );

		log_transaction( "UPDATE: " . get_class($this), $sql );
  }

  function insert()
  {
    $sql = 'INSERT INTO `assist_'.get_company_code().'_'.$this->table.'`
            ('.join( ",\n", $this->fields ).')
            VALUES
						('.join( ",\n", $this->get_insert_values() ).')';

    $this->id = execute_insert( $sql );

		log_transaction( "INSERT: " . get_class($this), $sql );
  }

  function delete()
  {
    $sql = 'DELETE FROM `assist_'.get_company_code().'_'.$this->table.'`
            WHERE id = '.$this->quote( $this->id );

    execute_update( $sql );

		log_transaction( "DELETE: " . get_class($this), $sql );
  }

	function get_records( $where = false, $order = "id asc" )
  {
  	$sql = 'SELECT id, '.join( ",\n", $this->fields ).'
            FROM `assist_'.get_company_code().'_'.$this->table.'`';

		if( $where )
		{
			$sql .= ' WHERE ' . $where;
		}

		$sql .= ' ORDER BY ' . $order;

    $rs = get_resultset( $sql );

    $records = array();

		$class = get_class($this);

    while( $record = get_record( $rs ) )
    {
      $obj = new $class;
      $obj->init( $record );

      $records[] = $obj;
    }

    return $records;
  }

  function get_insert_values()
  {
    $out = array();

    foreach ( $this->fields as $field )
    {
      $out[] = $this->quote( $this->$field );
    }

    return $out;
  }

  function get_update_values()
  {
    $out = array();

    foreach ( $this->fields as $field )
    {
      $out[] = $field.' = '.$this->quote( $this->$field );
    }

    return $out;
  }

  function quote( $value )
  {
    if( isset( $value ) )
    {
      if( is_int( $value ) )
      {
        return $value;
      }
      elseif( is_float( $value ) )
      {
        return sprintf( '%F', $value );
      }
			elseif( is_string( $value ) && strlen( $value ) == 0 )
			{
				return 'NULL';
			}

      return "'".addcslashes( $value, "\000\n\r\\'\"\032" )."'";
    }
    else
	  {
	    $value = 'NULL';
	  }

	  return $value;
  }

  function is_transient()
  {
    return isset( $this->id ) == false || $this->id == 0;
  }

  function is_persisted()
  {
    return $this->is_transient() === false;
  }

	function get_display_name()
  {
  	if( $this->is_transient() )
		{
      return "New";
		}
		else
	  {
	  	return $this->name;
		}
  }

	/*function to_string( $debug = true )
	{
	  foreach ( $this->fields as $field )
    {
    	if( $debug )
	    {
	      debug( $field.' = '.$this->$field );
	    }
	    else
	    {
	      echo $field.' = '.$this->$field;
	    }
		}
	}*/
}
?>
