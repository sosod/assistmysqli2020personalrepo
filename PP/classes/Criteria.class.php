<?php
  class Criteria extends Base
  {
    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = false )
	  {
	    $table = "pp_criteria";
	    $fields = array( "category_id", "name", "weight" );

	    parent::__construct( $table, $fields, $id );
	  }

		function recalculate_project_weights()
		{
      $project_id_rs = get_resultset( "SELECT project_id FROM assist_".get_company_code()."_pp_project_criteria WHERE criteria_id = " . $this->id );

			while( $record = get_record( $project_id_rs ) )
      {
        $sql = "UPDATE assist_".get_company_code()."_pp_project p SET weight = ( SELECT SUM(c.weight) FROM assist_".get_company_code()."_pp_criteria c JOIN assist_".get_company_code()."_pp_project_criteria pc ON pc.criteria_id = c.id WHERE pc.project_id = p.id ) WHERE p.id = " . $record["project_id"];
				execute_update( $sql );
			}
		}
  }
?>
