<?php
  class ProjectCriteria extends Base
  {
    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = false )
	  {
	    $table = "pp_project_criteria";
	    $fields = array( "project_id", "criteria_id" );

	    parent::__construct( $table, $fields, $id );
	  }
  }
?>
