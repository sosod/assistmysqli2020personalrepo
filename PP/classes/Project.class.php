<?php
require_once ( 'Base.class.php' );

class Project extends Base
{
	public $criterias = array();
	public $criteria_ids = array();


  function __construct( $id = false )
  {
    $table = "pp_project";
    $fields = array('name', 'source', 'year', 'description', 'ward_id', 'department_id', 'value', 'weight', 'status', 'budget_1', 'budget_2', 'budget_3', 'budget_4', 'budget_5', 'kpi_number', 'created_by_id', 'created_on', 'approved_by_id', 'approved_on');

    parent::__construct( $table, $fields, $id );
  }

	function load( $id )
  {
    parent::load( $id );

		/* Load criterias */
    $sql = 'SELECT c.id, c.name, c.weight
            FROM assist_'.get_company_code().'_pp_criteria c
						JOIN assist_'.get_company_code().'_pp_project_criteria pc ON pc.criteria_id = c.id
						WHERE pc.project_id = ' . $this->id;

    $sql .= ' ORDER BY c.weight desc';

    $rs = get_resultset( $sql );

    while( $record = get_record( $rs ) )
    {
      $criteria = new Criteria();
      $criteria->init( $record );

      $this->criterias[] = $criteria;
			$this->criteria_ids[] = $criteria->id;
    }
  }

	function insert()
  {
  	$this->status = "identified";

  	$this->created_by_id = get_logged_in_user_id();
		$this->created_on = date( 'Y-m-d H:i:s' );

		$this->approved_on = NULL;
		$this->approved_by_id = NULL;

    parent::insert();

		$this->save_criterias();
  }

	function update( $update_criterias = true )
  {
    parent::update();

		if( $update_criterias && count( $this->criteria_ids ) > 0 )
		{
			/* Delete existing criterias */
			$sql = "DELETE FROM assist_".get_company_code()."_pp_project_criteria WHERE project_id = " . $this->id;

			execute_update( $sql );

			/* Insert new criterias */
			$this->save_criterias();
		}
  }

	function save_criterias()
	{
		foreach( $this->criteria_ids as $criteria_id )
    {
      $project_criteria = new ProjectCriteria();
      $project_criteria->project_id = $this->id;
      $project_criteria->criteria_id = $criteria_id;

      $project_criteria->save();
    }

		/* Calculate the project total weight and update project */
		$sql = "UPDATE assist_".get_company_code()."_pp_project p SET weight = ( SELECT SUM(c.weight) FROM assist_".get_company_code()."_pp_criteria c JOIN assist_".get_company_code()."_pp_project_criteria pc ON pc.criteria_id = c.id WHERE pc.project_id = p.id ) WHERE p.id = " . $this->id;
    execute_update( $sql );
	}

	function has_criteria( $criteria_id )
	{
		foreach( $this->criteria_ids as $tmp_criteria_id )
		{
			if( $tmp_criteria_id == $criteria_id )
			{
				return true;
			}
		}

		return false;
	}

	function get_ward()
  {
    if( isset( $this->ward ) == false )
    {
      $this->ward = new Ward( $this->ward_id );
    }

    return $this->ward;
  }

	function get_department()
  {
    if( isset( $this->department ) == false )
    {
      $this->department = new Department( $this->department_id );
    }

    return $this->department;
  }
}
?>
