<?php
  class ViewHelper
  {
  	public static $ACTION_SAVE = "save";
    public static $ACTION_DELETE = "delete";
		
    public static $POTENTIAL_STATUSES = array( "identified" => "Identified",
					                                     "onhold" => "On Hold" );   

    public static $APPROVED_STATUSES = array( "inprogress" => "In Progress",
                                              "completed" => "Completed" ); 																							 
                                     
  	/******************************************************************************/
		/******************************* LIST FUNCTIONS *******************************/ 
		/******************************************************************************/
		public static function get_potential_status_select_list()
    {
      $list = array();  
  
      foreach( self::$POTENTIAL_STATUSES as $status => $desc )
      {
        $list[] = array( $status, $desc );    
      }
      
      return $list;      
    }
		
		public static function get_approved_status_select_list()
    {
      $list = array();  
  
      foreach( self::$APPROVED_STATUSES as $status => $desc )
      {
        $list[] = array( $status, $desc );    
      }
      
      return $list;      
    }
		
		public static function get_bool_select_list()
    {
      $list = array();  
      
      $list[] = array( "0", "No" );
      $list[] = array( "1", "Yes" );  
      
      return $list;
    } 
		
		public static function get_numeric_select_list( $low=0, $high=100 )
    {
      $list = array();  
      
      for( $i = $low; $i <= $high; $i++ )
      {
        $list[] = array( "$i", "$i" );  
      }  
      
      return $list;
    }
		
		public static function get_year_select_list( $existing_year )
    {
      $list = array();  
			
			if( isset( $existing_year ) )
			{
				$low = self::get_base_year( $existing_year );
				$high = ( (int)date("Y") ) + 5;    
			}
			else
			{
        $low = (int)date("Y");
				$high = ( (int)date("Y") ) + 5;
			}
      
      for( $i = $low; $i <= $high; $i++ )
      {
        $list[] = array( $i . "/" . ( $i + 1 ), $i . "/" . ( $i + 1 ) );  
      }  
      
      return $list;
    }
		
		public static function get_ward_select_list()
    {
      $list = array();
      
      $ward = new Ward();  
  
      foreach( $ward->get_records() as $record )
      {
        $list[] = array( $record->id, $record->name );    
      }
      
      return $list;      
    }
		
		public static function get_department_select_list()
    {
      $list = array();
      
      $department = new Department();  
  
      foreach( $department->get_records( "diryn = 'Y'", "dirtxt asc") as $record )
      {
        $list[] = array( $record->id, $record->value );    
      }
      
      return $list;      
    }
		
		public static function get_approved_eval_year_select_list()
    {
			$sql = "SELECT MIN(year) AS min, MAX(year) AS max FROM assist_" . get_company_code() . "_pp_project WHERE status = 'inprogress'";
			$record = get_record( get_resultset( $sql ) );
			
			$min = $record['min'];
			$max = $record['max'];
			
      $list = array();  
      
      if( isset( $min ) && isset( $max ) )
      {
        $low = ( (int)self::get_base_year( $min ) );
        $high = ( (int)self::get_base_year( $max ) ) + 5;    
      }
      else
      {
        $low = (int)date("Y");
        $high = $low + 5;
      }
      
      for( $i = $low; $i < $high; $i++ )
      {
        $list[] = array( $i . "/" . ( $i + 1 ), $i . "/" . ( $i + 1 ) );  
      }  
      
      return $list;
    }
		
		/******************************************************************************/
		/**************************** INTERNAL FUNCTIONS ******************************/ 
		/******************************************************************************/
    private static function get_base_year( $project_year )
    {
      return substr( $project_year, 0, 4 );
    }
		
    /******************************************************************************/
		/******************************* FLOW FUNCTIONS *******************************/ 
		/******************************************************************************/
    public static function redirect( $url )
    {
      header( "Location: $url" );
      exit();
    }
		
		/******************************************************************************/
		/****************************** MACRO FUNCTIONS *******************************/ 
		/******************************************************************************/
		public static function breadcrumbs( $breadcrumbs )
		{
      $header = "<h1 id=\"breadcrumbs\">";
      
      $cnt = 0;
      foreach( $breadcrumbs as $crumb => $url )
      {
        if( $cnt != 0 )
        {
          $header .= " &raquo; ";
        }
        
        if( exists( $url ) )
        {
          $header .= "<a href=\"{$url}\">{$crumb}</a>";        
        }
        else
        {
          $header .= $crumb;  
        }
        
        $cnt++;
      }
      
      $header .= "</h1>";
      
      echo $header;
		}
    
    public static function actions( $buttons, $is_active = true, $wrapper=true )
		{
			if( $wrapper )
		    $actions = "<div class=\"actions\">";
      
      foreach( $buttons as $name=>$url )
      {
        $actions .= "<input type=\"button\" value=\"{$name}\" onclick=\"redirect('{$url}');\" " . ($is_active == false ? "disabled=\"disabled\"" : "") . "/> ";
      }
      
			if( $wrapper )
        $actions .= "</div>";
      
      echo $actions;                 
    }
    
    public static function validation( $form, $errors = false )
		{
      echo "<script type=\"text/javascript\">$(document).ready(function(){ $(\"#{$form}\").validate(); }); </script>";
      
      if( $errors )
      {
        self::errors( $errors );
      }
    }
  
    public static function button( $url, $value, $is_active = true )
		{
		  if( $value == "Delete" )
      {
        echo "<input type=\"button\" onclick=\"confirm_delete('" . $url . "');\" value=\"" . $value ."\" " . ( $is_active == false ? "disabled=\"disabled\"" : "") . " />";	        
      }
      else
      {
        echo "<input type=\"button\" onclick=\"redirect('" . $url . "');\" value=\"" . $value ."\" " . ( $is_active == false ? "disabled=\"disabled\"" : "") . " />";	        
      }
		}
		
    public static function select( $name, $options, $value, $required, $empty_option = "Select from list ...", $id = false, $style = false )
    {
      echo "<select " . ($id ? "id=\"$id\" " : "") . "name=\"" . $name . "\" " . (($required) ? "class=\"required\"" : "") . (($style) ? "style=\"$style\"" : "") . " >";
      
      if( $empty_option )
      {
        echo "<option value=\"\">$empty_option</option>";         
      }
      
      foreach ($options as $option)
      {
        echo "<option value=\"" . htmlentities($option[0]) . "\" " . (($option[0] == $value) ? "selected=\"selected\"" : "") . ">" . $option[1] . "</option>";         
      }
      
      echo "</select>";  
    } 
		
		public static function text( $name, $value, $required = false, $id = false, $style = false )
    {
      echo "<input type=\"text\"" . ($id ? "id=\"$id\" " : "") . "name=\"" . $name . "\" " . (($required) ? "class=\"required\"" : "") . (($style) ? "style=\"$style\"" : "")  . " value=\"" . $value. "\" />";
    } 
		
		public static function textarea( $name, $value, $required = false, $cols="40", $rows="6", $id = false, $style = false )
    {
      echo "<textarea " . ($id ? "id=\"$id\" " : "") . "name=\"" . $name . "\" cols=\"" . $cols. "\" rows=\"" . $rows . "\" " . (($required) ? "class=\"required\"" : "") . (($style) ? "style=\"$style\"" : "")  . " />" . $value . "</textarea>" ;
    } 
		
		public static function submit( $value, $id = false, $style = false )
    {
      echo "<input type=\"submit\"" . ($id ? "id=\"$id\" " : "") . (($required) ? "class=\"required\"" : "") . (($style) ? "style=\"$style\"" : "")  . " value=\"" . $value. "\" />";
    } 
    
    public static function errors( $errors )
    {
      if( count( $errors ) > 0 ) 
      { 
        echo "<h2 class=\"error-messages-header\">Please correct the following errors:</h2>";
        echo "<ul class=\"error-messages\">";
        
        foreach( $errors as $error ) 
        {
          echo "<li>$error</li>";
        }
        
        echo "</ul>";
      }
    }
    
    public static function messages( $messages )
    {
      if( count( $messages ) > 0 ) 
      { 
        echo "<ul class=\"messages\">";
        
        foreach( $messages as $message ) 
        {
          echo "<li>$message</li>";
        }
        
        echo "</ul>";
      }
    }
    
    /******************************************************************************/
		/****************************** HELPER FUNCTIONS ******************************/ 
		/******************************************************************************/
    public static function display_bool( $value )
    {
      return $value ? "Yes" : "No";
    }
    
    public static function if_null( $value, $default )
    {
      return $value != null ? $value : $default;
    }
    
    public static function display_status( $status )
    {
    	$statuses = array_merge( self::$POTENTIAL_STATUSES, self::$APPROVED_STATUSES );
			
      return $statuses[ $status ];
    }
		
		public static function get_categories()
    {
      $category = new Category();
      
      return $category->get_records( false, "name" );  
    }
		
		public static function project_year( $project_year, $year_no )
		{
			$base_year = self::get_base_year( $project_year );
			
			return ( $base_year + $year_no - 1 ) . "/" . ( $base_year + $year_no ); 
		}
		
		/******************************************************************************/
    /******************************* COMMS FUNCTIONS ******************************/ 
    /******************************************************************************/
		public static function set_session_message( $id, $message )
    {
      $_SESSION["pp_message_" . $id] = $message;
    }
		
		public static function get_session_message( $id )
    {
    	$message = $_SESSION["pp_message_" . $id];
			$_SESSION["pp_message_" . $id] = null;
			
			if( isset( $message ) )
			{
				echo "<p class=\"message\">" . $message . "</p>";
			}
    }
  }
?>