<?php
  class Department
  {
  	/* NOTE - this class use to map list_dept but now maps list_dir instead -
  	 * at some stage the class name and code should probably be changed */

    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = false )
	  {
	    $this->table = "list_dir";
	    $this->fields = array( "dirtxt", "dirsort", "diryn" );

			if( exists( $id ) )
	    {
	      $this->load( $id );
	    }
	  }

		function load( $id )
    {
      $sql = 'SELECT dirid, '.join( ",\n", $this->fields ).'
              FROM `assist_'.get_company_code().'_'.$this->table.'`
              WHERE dirid = '.$id;

      $this->init( get_record( get_resultset( $sql ) ) );
    }

		function loadFromName( $name )
    {
      $sql = 'SELECT dirid, '.join( ",\n", $this->fields ).'
              FROM `assist_'.get_company_code().'_'.$this->table.'`
              WHERE dirtxt = "'.$name . '"';

      $this->init( get_record( get_resultset( $sql ) ) );
    }

		function init( $data )
	  {
	  	$this->id = $data['dirid'];
	    $this->dirid = $data['dirid'];
			$this->value = $data['dirtxt'];

	    foreach ( $this->fields as $field )
	    {
	      $this->$field = $data[$field];
	    }
	  }

		function get_records( $where = false, $order = "id asc" )
	  {
	    $sql = 'SELECT dirid, '.join( ",\n", $this->fields ).'
	            FROM `assist_'.get_company_code().'_'.$this->table.'`';

	    if( $where )
	    {
	      $sql .= ' WHERE ' . $where;
	    }

	    $sql .= ' ORDER BY ' . $order;

	    $rs = get_resultset( $sql );

	    $records = array();

	    while( $record = get_record( $rs ) )
	    {
	      $obj = new Department();
	      $obj->init( $record );

	      $records[] = $obj;
	    }

	    return $records;
	  }
  }
?>
