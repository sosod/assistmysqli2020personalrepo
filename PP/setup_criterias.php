<?
include("inc_ignite.php");
include("inc_header.php");

$category_id = $_REQUEST["category_id"];
$category = new Category( $category_id );
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => "setup_categories.php", $category->name => null, "Criterias" => null ) ); ?>

<? ViewHelper::get_session_message( "setup_criterias" ); ?>

<p class="extra-info">A maximum of 40 criteria may be created per category</p>

<table class="list" cellspacing="0">
  <tr>
		<th style="width:50%;">Name</th>
		<th style="width:20%;">Weight</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
	
	$criteria = new Criteria();
	$records = $criteria->get_records( "category_id = {$category_id}", "weight desc" );
	
	if( count( $records ) > 0 )
	{
	  foreach( $records as $obj )
	  {
  ?>
	  <tr>
	    <td><?= $obj->name ?></td>
			<td><?= $obj->weight ?></td>
	    <td>
	      <? ViewHelper::button( "setup_criteria.php?id={$obj->id}&category_id={$category_id}", "Configure" ); ?>
	    </td>		
	  </tr>
	<?
	  }
	}
	else
	{
	?>
	<tr>
    <td colspan="3">No records found.</td>
  </tr>
	<?	
	}
  ?>
</table>

<? 
  if( count( $records ) < 40 )
	{
	  ViewHelper::actions( array( "Back"=>"setup_categories.php", "New"=>"setup_criteria.php?category_id={$category_id}" ) );
	}
	else
	{
		ViewHelper::actions( array( "Back"=>"setup_categories.php" ) );
	}
?>

<?php include("inc_footer.php"); ?>