<?php
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => null ) ); ?>

<h2>Module configuration</h2>
<table cellspacing="0" class="info">
  <tr>
    <th style="width:120px;">Users</th>
    <td style="width:350px">Define the user's access and other module variables</td>
    <td class="action"><? ViewHelper::button( "setup_users.php", "Configure" ); ?></td>
  </tr>
  <tr>
    <th style="width:120px;">Wards</th>
    <td style="width:350px">Define the wards</td>
    <td class="action"><? ViewHelper::button( "setup_wards.php", "Configure" ); ?></td>
  </tr>
	<tr>
    <th style="width:120px;">Categories</th>
    <td style="width:350px">Define the categories and their criteria</td>
    <td class="action"><? ViewHelper::button( "setup_categories.php", "Configure" ); ?></td>
  </tr>
</table>
    
<?php include("inc_footer.php"); ?>