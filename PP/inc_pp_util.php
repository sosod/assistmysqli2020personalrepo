<?
function debug( $msg )
{ 
  $fd = fopen( "C:\www\hosts\assist.ignite4u.co.za\logs\PP.log", "a" );
  $str = "[".date( "Y/m/d h:i:s", mktime() )."] ".$msg;
  fwrite( $fd, $str."\n" );
  fclose( $fd );
}
    
/*********** VIEW HELPER FUNCTIONS **********/
function exists( $value )
{
  if( isset( $value ) && strlen( trim( $value ) ) != 0 )
  {
    return true;
  }
  
  return false;   
}

function redirect( $url )
{
  header("Location: $url");
  exit();
}

function get_param( $name )
{
  return $_REQUEST[ $name ];  
}

function get_sql_param( $name, $default = false )
{
	if( isset( $_REQUEST[ $name ] ) == false )
	{
		if( $default !== false )
		{
		  return $default;	
		}
		else
		{
			return null;
		}
	}
	
  return mysql_escape_string( $_REQUEST[ $name ] );  
}

function print_messages( $messages )
{
  if( count($messages) > 0 ) 
  { 
    echo "<ul class=\"error-messages\">";
    
    foreach( $messages as $message ) 
    {
      echo "<li>$message</li>";
    }
    
    echo "</ul>";
  }
} 
?>