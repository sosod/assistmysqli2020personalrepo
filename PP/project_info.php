<?
include("inc_ignite.php");

$type = $_REQUEST["type"];

$project = new Project( $_REQUEST["id"] );

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( ucfirst( $type ) . " Projects" => $type . "_projects.php", $project->get_display_name() => null ) ); ?>

<div class="wrapper">
	
	<div class="left-col">
		<table class="info full" cellspacing="0">
	    <tr>
	      <th>Name</th>
	      <td><?= $project->name ?></td>
	    </tr>
	    <tr>
	      <th>Source</th>
	      <td><?= $project->source ?></td>
	    </tr>
	    <tr>
	      <th>Year</th>
	      <td><?= $project->year ?></td>
	    </tr>
	    <tr>
	      <th>Description</th>
	      <td><?= $project->description ?></td>
	    </tr>
	    <tr>
	      <th>Ward</th>
	      <td><?= $project->get_ward()->name ?></td>
	    </tr>
	    <tr>
	      <th>Department</th>
	      <td><?= $project->get_department()->value ?></td>
	    </tr>
	    <tr>
        <th>Value</th>
        <td>R <?= $project->value ?></td>
      </tr>
			<tr>
        <th>Weight</th>
        <td><?= $project->weight ?></td>
      </tr>
	    <tr>
	      <th>Status</th>
	      <td><?= ViewHelper::display_status( $project->status ) ?></td>
	    </tr>
	  </table>
		
		<? if( $project->status != "identified" ){ ?>
		
		<table class="info full" cellspacing="0">
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 1 ) ?></th>
	      <td><?= $project->budget_1 ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 2 ) ?></th>
	      <td><?= $project->budget_2 ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 3 ) ?></th>
	      <td><?= $project->budget_3 ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 4 ) ?></th>
	      <td><?= $project->budget_4 ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 5 ) ?></th>
	      <td><?= $project->budget_5 ?></td>
	    </tr>
	    <tr>
	      <th>KPI Number</th>
	      <td><?= $project->kpi_number ?></td>
	    </tr>
	  </table>
		
		<? } ?>
	</div>
	<div class="right-col">
    <table class="info full" cellspacing="0">
	    <tr class="blank">
	      <td>
	        <h5>Criterias</h5>
	      </td>
	    </tr>
	    <? $index = 0;
	       foreach( $project->criterias as $criteria ){ ?>
	    <tr <?= $index % 2 == 1 ? "class=\"alt\"" : "" ?>>
	      <td><?= $criteria->name ?> (<?= $criteria->weight ?>)</td>
	    </tr>
	    <?   $index++;
	       } ?>
	  </table>
		
		<? if( $project->status != "completed" ){ ?>
			<? if( $user_setup->is_mod_admin && $project->status == "identified"){ ?>
	    <? ViewHelper::actions( array( "Edit" => "project.php?type=" . $type . "&id=" . $project->id, "Approve" => "project_approve.php?id=" . $project->id ) ) ?></td>
	    <? }else if( $user_setup->is_mod_admin && $project->status == "inprogress"){ ?>
			<? ViewHelper::actions( array( "Edit" => "project.php?type=" . $type . "&id=" . $project->id, "On hold" => "project_on_hold.php?id=" . $project->id, "Complete" => "project_complete.php?id=" . $project->id ) ) ?></td>
			<? } ?>
		<? } ?>
	</div>
	
</div>

<? ViewHelper::actions( array( "Back"=> $type . "_projects.php" ) ); ?>

<? include("inc_footer.php"); ?>