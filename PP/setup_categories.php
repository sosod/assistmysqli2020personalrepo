<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => null ) ); ?>

<? ViewHelper::get_session_message( "setup_categories" ); ?>

<p class="extra-info">A maximum of 10 categories may be created</p>

<table class="list" cellspacing="0">
  <tr>
		<th style="width:70%;">Name</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
	
	$category = new Category();
	$records = $category->get_records( false, "name" );
	
	if( count( $records ) > 0 )
	{
	  foreach( $records as $obj )
	  {
  ?>
	  <tr>
	    <td><?= $obj->name ?></td>
	    <td>
	      <? ViewHelper::button( "setup_category.php?id={$obj->id}", "Configure" ); ?>
				<? ViewHelper::button( "setup_criterias.php?category_id={$obj->id}", "Criteria" ); ?>
	    </td>		
	  </tr>
	<?
	  }
	}
	else
	{
	?>
	<tr>
    <td colspan="3">No records found.</td>
  </tr>
	<?	
	}
  ?>
</table>

<? 
  if( count( $records ) < 10 )
	{
	  ViewHelper::actions( array( "Back"=>"setup.php", "New"=>"setup_category.php" ) );
	}
	else
	{
		ViewHelper::actions( array( "Back"=>"setup.php" ) );
	}
?>

<?php include("inc_footer.php"); ?>