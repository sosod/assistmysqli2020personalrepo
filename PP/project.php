<?
include("inc_ignite.php");

$type = $_REQUEST["type"];

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $project = new Project( $_REQUEST["id"] );
  $project->init( $_REQUEST );
	
	$project->criteria_ids = $_REQUEST['criteria_ids']; 
	
  $project->save();
	
	ViewHelper::set_session_message( "projects", "Project successfully saved." );
  ViewHelper::redirect( $type . "_projects.php" );
}

$project = new Project( $_REQUEST["id"] );

$categories = ViewHelper::get_categories(); 

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( ucfirst( $type ) . " Projects" => $type . "_projects.php", $project->get_display_name() => null ) ); ?>

<? ViewHelper::validation( "frmProject" ); ?>

<form id="frmProject" action="project.php" method="post" class="two-column">  
  <div class="left-col">
  	<h5>Project Details</h5>
	  <table class="form full" cellspacing="0">
	    <tr>
	      <th>Name</th>
	      <td><? ViewHelper::text( "name", $project->name, true, false, "width:200px;" ) ?></td>
	    </tr>
			<tr>
	      <th>Source</th>
	      <td><? ViewHelper::text( "source", $project->source, true, false, "width:200px;" ) ?></td>
	    </tr>
	    <tr>
	      <th>Year</th>
	      <td><? ViewHelper::select( "year", ViewHelper::get_year_select_list( $project->year ), $project->year, true, false ) ?></td>
	    </tr>
			<tr>
	      <th>Description</th>
	      <td><? ViewHelper::textarea( "description", $project->description, true ) ?></td>
	    </tr>
			<tr>
	      <th>Ward</th>
	      <td><? ViewHelper::select( "ward_id", ViewHelper::get_ward_select_list(), $project->ward_id, true ) ?></td>
	    </tr>
			<tr>
	      <th>Department</th>
	      <td><? ViewHelper::select( "department_id", ViewHelper::get_department_select_list(), $project->department_id, true ) ?></td>
	    </tr>
			<tr>
	      <th>Value</th>
	      <td>R <? ViewHelper::text( "value", $project->value, true ) ?></td>
	    </tr>
	  </table>
	  
		<? if( $user_setup->is_mod_admin && $project->status == "inprogress" ){ ?>
		
		<table class="form full" cellspacing="0">
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 1 ) ?></th>
	      <td>R <? ViewHelper::text( "budget_1", $project->budget_1, true ) ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 2 ) ?></th>
	      <td>R <? ViewHelper::text( "budget_2", $project->budget_2, true ) ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 3 ) ?></th>
	      <td>R <? ViewHelper::text( "budget_3", $project->budget_3, true ) ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 4 ) ?></th>
	      <td>R <? ViewHelper::text( "budget_4", $project->budget_4, true ) ?></td>
	    </tr>
	    <tr>
	      <th>Budget <?= ViewHelper::project_year( $project->year, 5 ) ?></th>
	      <td>R <? ViewHelper::text( "budget_5", $project->budget_5, true ) ?></td>
	    </tr>
	    <tr>
	      <th>KPI Number</th>
	      <td><? ViewHelper::text( "kpi_number", $project->kpi_number, false ) ?></td>
	    </tr>
	  </table>
		<? } ?>
	</div>
	<div class="right-col">
  	<h5>Criteria</h5>
		<div id="accordion">
	  <? foreach( $categories as $category ){ ?>
		<h5><a href="#"><?= $category->name ?></a></h5>
	  <div>
	    <table class="form full" cellspacing="0">
	      <? $criterias = $category->get_criterias();
				   $index = 0;
	         foreach( $criterias as $criteria ){ ?>
	      <tr <?= $index % 2 == 1 ? "class=\"alt\"" : "" ?>>
	        <td><?= $criteria->name ?> (<?= $criteria->weight ?>)</td>
	        <td align="center"><input type="checkbox" name="criteria_ids[]" value="<?= $criteria->id ?>" <?= $project->has_criteria( $criteria->id ) ? "checked=\"checked\"" : "" ?> /></td>
	      </tr>
	      <?   $index++;
	         } ?>
	    </table>
	  </div>
	  <? } ?>
		</div>	
    <input type="submit" value="Save" style="margin-top:10px;"/>
	</div>
  <input type="hidden" name="id" value="<?= $project->id ?>" />
	<input type="hidden" name="type" value="<?= $type ?>" />
	
	<input type="hidden" name="status" value="<?= $project->status ?>" />
	<input type="hidden" name="created_by_id" value="<?= $project->created_by_id ?>" />
  <input type="hidden" name="created_on" value="<?= $project->created_on ?>" />
	<input type="hidden" name="approved_by_id" value="<?= $project->approved_by_id ?>" />
  <input type="hidden" name="approved_on" value="<?= $project->approved_on ?>" />
	
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>$type."_projects.php" ) ); ?>

<? include("inc_footer.php"); ?>