<?php
//session_start();

function __autoload($class_name) 
{
  require_once "classes/" . $class_name . '.class.php';  
}

require_once '../inc_session.php';
$logfile = "../logs/error_".$cmpcode.".txt";

include("inc_pp_util.php");
include("inc_pp_db.php");

/* Make a connection so that mysql_real_escape_string works */
get_connection();

/* LOAD THE LOGGED IN USER'S SETUP */
$user_setup = $_SESSION[ "pp_user_setup_" . $tkid ];

if( isset( $user_setup ) == false || $user_setup->user_id != $tkid )
{
  $user_setup = new UserSetup();
	$user_setup->load_from_user_id( $tkid );
  $_SESSION[ "pp_user_setup_" . $tkid ] = $user_setup;
} 
?>