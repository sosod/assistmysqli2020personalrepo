<?php
require ('lib/fpdf/IgnitePDF.class.php');

include("inc_ignite.php");

$type = get_param( "type" );

$ward_id = get_sql_param( "ward_id" );
$ward = new Ward( $ward_id );

$department_id = get_sql_param( "department_id" );
$department = new Department( $department_id );

$status = get_sql_param( "status" );
$year = get_sql_param( "year" );

$sql  = "SELECT p.id ";
$sql .= "FROM assist_".get_company_code()."_pp_project p ";
$sql .= "LEFT JOIN assist_".get_company_code()."_pp_ward w ON (p.ward_id = w.id) ";
$sql .= "LEFT JOIN assist_".get_company_code()."_list_dir d ON (p.department_id = d.dirid) ";
if( $type == "potential")
{
  $sql .= "WHERE p.status not in ('inprogress','completed') ";  
}
else if( $type == "approved")
{
  $sql .= "WHERE p.status not in ('identified','onhold') ";
}

if( exists($ward_id) )
{
  $sql .= "AND p.ward_id = " . quote($ward_id) . " ";
}

if( exists($department_id) )
{
  $sql .= "AND p.department_id = " . quote($department_id) . " ";
}       

if( exists($status) )
{
  $sql .= "AND p.status = " . quote($status) . " ";
}

if( exists($year) )
{
  $sql .= "AND p.year IN ( " . join( ",", db_budget_year_range( $year ) ) . " ) ";
}

$sql .= "ORDER BY p.weight desc ";

$project_ids = query_records( $sql );

$pdf = new IgnitePDF('L', 'pt', 'A4');
$pdf->AddPage();
$pdf->H1( ucfirst($type) . " Projects" );
$pdf->TableCaption( "Report filter", false );
$pdf->listTable( array("Ward", "Department", "Status", "Year"), array( array( isset( $ward->id ) ? $ward->name : "All wards", isset( $department->id ) ? $department->value : "All departments", exists( $status ) ? ViewHelper::display_status( $status ) : "All statuses", exists( $year ) ? $year : "All budget years" ) ), array( 195, 195, 195, 195 ) );
foreach( $project_ids as $project_id )
{
	$project = new Project( $project_id["id"] );
	
	$pdf->TableCaption( $project->name );
	
	$headers = array( "Source","Year","Description","Ward","Department","Value","Weight","Status" );
	$widths = array( 110,50,240,105,105,60,50,60 );
	$data = array( $project->source, $project->year, $project->description, $project->get_ward()->name, $project->get_department()->value, "R" . $project->value, $project->weight, ViewHelper::display_status( $project->status ) );
	
	$pdf->listTable( $headers, array( $data ), $widths );
	
	if( $project->status != "identified" )
	{
		$headers = array( "Budget " . ViewHelper::project_year( $project->year, 1 ),"Budget " . ViewHelper::project_year( $project->year, 2 ),"Budget " . ViewHelper::project_year( $project->year, 3 ),"Budget " . ViewHelper::project_year( $project->year, 4 ),"Budget " . ViewHelper::project_year( $project->year, 5 ), "KPI Number" );
	  $widths = array( 130,130,130,130,130,130 );
	  $data = array( "R".$project->budget_1, "R".$project->budget_2, "R".$project->budget_3, "R".$project->budget_4, "R".$project->budget_5, $project->kpi_number );
		
		$pdf->listTable( $headers, array( $data ), $widths );
	}
	
	foreach( $project->criterias as $criteria )
  {
    $criteria_names[] = $criteria->name . " (" . $criteria->weight . ")";
  }
  
  $pdf->infoTable( array( array( "Criterias", join( ",  ", $criteria_names ) ) ), array( 80, 700 ) );
}

$pdf->Output( $type . '_projects.pdf', 'I');

?>