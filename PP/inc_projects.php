<?php
$ward_id = get_sql_param( "ward_id" );
$status = get_sql_param( "status" );
$department_id = get_sql_param( "department_id" );
$year = get_sql_param( "year" );
$records_per_page = 10;
$page = get_sql_param( "page", 1 );
$total_records;
$total_pages;

$query_string = "ward_id=" . $ward_id . "&department_id=" . $department_id . "&status=" . $status . "&year=" . $year . "&";

$start = ( $page - 1 == 0 ) ? 0 : ( ( $page - 1 ) * $records_per_page );

$sql_select = "SELECT p.id, p.name, p.source, p.year, w.name AS ward, p.value, d.dirtxt AS department, p.weight, p.status ";
$sql_count  = "SELECT count(*) ";
$sql_from   = "FROM assist_".get_company_code()."_pp_project p ";
$sql_from  .= "LEFT JOIN assist_".get_company_code()."_pp_ward w ON (p.ward_id = w.id) ";
$sql_from  .= "LEFT JOIN assist_".get_company_code()."_list_dir d ON (p.department_id = d.dirid) ";

if( $type == "potential")
{
  $sql_where  = "WHERE p.status not in ('inprogress','completed') ";	
}
else if( $type == "approved")
{
	$sql_where  = "WHERE p.status not in ('identified','onhold') ";
}

if( exists($ward_id) )
{
  $sql_where .= "AND p.ward_id = " . quote($ward_id) . " ";
}

if( exists($department_id) )
{
  $sql_where .= "AND p.department_id = " . quote($department_id) . " ";
}				

if( exists($status) )
{
  $sql_where .= "AND p.status = " . quote($status) . " ";
}

if( exists($year) )
{
  $sql_where .= "AND p.year IN ( " . join( ",", db_budget_year_range( $year ) ) . " ) ";
}

$sql_order = "ORDER BY p.weight desc ";
$sql_limit = "LIMIT " . $start . "," . $records_per_page;

$projects = query_records(  $sql_select . $sql_from . $sql_where . $sql_order . $sql_limit );

$total_records = query_count( $sql_count . $sql_from . $sql_where );
$total_pages = floor( $total_records / $records_per_page );

if( $total_records % $records_per_page > 0 )
{
  $total_pages = $total_pages + 1;
}

if( $total_pages == 0 )
{
	$total_pages = 1;
}

$bottom_range = $page - 4;
if( $bottom_range < 1 )
{
  $bottom_range = 1;
}

$top_range = $bottom_range + 9;
if( $top_range > $total_pages )
{
  $top_range = $total_pages;
}

include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( $type_name => null ) ); ?>

<? ViewHelper::get_session_message( "projects" ); ?>

<? ViewHelper::actions( array( "Add new project"=>"project.php?type=".$type, "Download projects as PDF" => "projects_pdf.php?" . $query_string . "type=".$type, "Download projects as CSV" => "projects_csv.php?" . $query_string . "type=".$type ) ); ?>

<h5 class="boxHeader">Search for projects</h5>
<form action="<?= $type ?>_projects.php" method="post">
	<fieldset class="actions">
		<? ViewHelper::select( "ward_id", ViewHelper::get_ward_select_list(), $ward_id, false, "All wards" ) ?>
		<? ViewHelper::select( "department_id", ViewHelper::get_department_select_list(), $department_id, false, "All departments" ) ?>
		<? if( $type == "potential" ){ ?>
		<? ViewHelper::select( "status", ViewHelper::get_potential_status_select_list(), $status, false, "All statuses" ) ?>
		<? }else if( $type == "approved" ){ ?>
		<? ViewHelper::select( "status", ViewHelper::get_approved_status_select_list(), $status, false, "All statuses" ) ?>
		<? ViewHelper::select( "year", ViewHelper::get_approved_eval_year_select_list(), $year, false, "All budget years" ) ?>
		<? } ?>
		<? ViewHelper::submit( "Search" ) ?>
	</fieldset>
</form>

<table cellspacing="0" class="list big-gap-top">
	<tr>
		<th>Project</th>
		<th>Ward</th>
		<th>Department</th>
		<th>Year</th>
		<th>Estimate</th>
		<th>Weight</th>
		<th>Status</th>
		<th></th>
	</tr>
	<?
	  if( count($projects) > 0 )
		{ 
	    foreach( $projects as $project )
			{ 
	?>
	<tr>
		<td><?= $project['name'] ?></td>
		<td><?= $project['ward'] ?></td>
		<td><?= $project['department'] ?></td>
		<td><?= $project['year'] ?></td>
		<td>R<?= $project['value'] ?></td>
		<td><?= $project['weight'] ?></td>
		<td><?= ViewHelper::display_status( $project['status'] ) ?></td>
		<? if( $type == "potential" ){ ?>
		  <? if( $user_setup->is_mod_admin ){ ?>
        <td><? ViewHelper::actions( array( "View"=>"project_info.php?type=potential&id=" . $project['id'], "Edit"=>"project.php?type=potential&id=" . $project['id'], "Approve"=>"project_approve.php?id=" . $project['id'] ), true, false ); ?></td>			
			<? }else{ ?>
			  <td><? ViewHelper::actions( array( "View"=>"project_info.php?type=potential&id=" . $project['id'], "Edit"=>"project.php?type=potential&id=" . $project['id'] ), true, false ); ?></td>
			<? } ?>
		<? }else if( $type == "approved" ){ ?>
		  <? if( $user_setup->is_mod_admin ){ ?>
        <? if( $project["status"] != "completed" ){ ?>
	        <td><? ViewHelper::actions( array( "View"=>"project_info.php?type=approved&id=" . $project['id'], "Edit"=>"project.php?type=approved&id=" . $project['id'], "On hold"=>"project_on_hold.php?id=" . $project['id'], "Complete"=>"project_complete.php?id=" . $project['id'] ), true, false ); ?></td>  
	      <? }else{ ?>
	        <td><? ViewHelper::actions( array( "View"=>"project_info.php?type=approved&id=" . $project['id'] ), true, false ); ?></td>
	      <? } ?>     
      <? }else{ ?>
        <td><? ViewHelper::actions( array( "View"=>"project_info.php?type=approved&id=" . $project['id'] ), true, false ); ?></td>
      <? } ?>
		<? } ?>
	</tr>
	<?   }
		}
		else
		{
	?>
	<tr><td colspan="8">No projects found.</td></tr>
	<?}?>
</table>	

<div class="pager">
  <span class="overview">Page <?= $page ?> of <?= $total_pages ?></span>
 
  <? if( $bottom_range > 1 ){?>
  <span class="extend">...</span>
  <? } ?>
  
	<? if( $page > 1 ){?> 
  <a class="page prev" href="<?= $type ?>_projects.php?<?= $query_string ?>page=<?= $page-1 > 0 ? $page-1 : 1 ?>">&laquo;</a>
  <? } ?> 
  
	<? for( $i = $bottom_range; $i < $top_range+1; $i++ ){ ?>
    <? if( $page == $i ){ ?>
	  <span class="page current"><?= $i ?></span>  
	  <? }else{ ?>  
	  <a class="page" href="<?= $type ?>_projects.php?<?= $query_string ?>page=<?= $i ?>"><?= $i ?></a>
	  <? } ?>	
	<? } ?>
  
	<? if( $top_range < $total_pages ){?>
  <span class="extend">...</span>
  <? } ?>
	 
	<? if( $page < $total_pages ){?> 
  <a class="page next" href="<?= $type ?>_projects.php?<?= $query_string ?>page=<?= $page+1 ?>">&raquo;</a>
  <? } ?>
</div>

<?php include("inc_footer.php"); ?>