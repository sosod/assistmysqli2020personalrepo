function redirect( url )
{
  window.location.href = url;	
}

function confirm_delete( url )
{
	if( confirm( "Are you sure you want to delete this record? This action is permanent and cannot be undone." ) )
	{
	  redirect( url );
	}
	
	return false;
}
  
$(document).ready(function(){
  $("table.list tr").not(".heading,.info").mouseover(function(){$(this).addClass("over");}).mouseout(function(){$(this).removeClass("over");});
  $("table.list tr:even").not(".heading, .info").addClass("alt");
	/*
  $("table.info tr:even").addClass("alt");  
  $("table.form tr:even").addClass("alt");  
  $("table.form tr:last").addClass("actions");*/            
  
  $(".confirm-delete").click( function(){
    return confirm_delete( this.href );
  });
	
	/*
	$("#chx_all").click( function(){
    $("input.weight").attr("checked",true);
		$("#chx_none").attr("checked",false);
  });            
	
	$("#chx_none").click( function(){
    $("input.weight").attr("checked",false);
		$("#chx_all").attr("checked",false);
  });            
	*/
	
	$("#accordion").accordion();

});  