<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Wards" => null ) ); ?>

<? ViewHelper::get_session_message( "setup_wards" ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:20%;">Number</th>
		<th style="width:50%;">Name</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
	
	$ward = new Ward();
	$records = $ward->get_records();
	
	if( count( $records ) > 0 )
	{
	  foreach( $records as $obj )
	  {
  ?>
	  <tr>
	    <td><?= $obj->number ?></td>
			<td><?= $obj->name ?></td>
	    <td>
	      <? ViewHelper::button( "setup_ward.php?id={$obj->id}", "Configure" ); ?>
	    </td>		
	  </tr>
	<?
	  }
	}
	else
	{
	?>
	<tr>
    <td colspan="3">No records found.</td>
  </tr>
	<?	
	}
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup.php", "New"=>"setup_ward.php"  ) ); ?>

<?php include("inc_footer.php"); ?>