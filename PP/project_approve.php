<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $project = new Project( $_REQUEST["id"] );
  
	$project->budget_1 = get_sql_param( "budget_1" );
	$project->budget_2 = get_sql_param( "budget_2" );
	$project->budget_3 = get_sql_param( "budget_3" );
	$project->budget_4 = get_sql_param( "budget_4" );
	$project->budget_5 = get_sql_param( "budget_5" );
	$project->kpi_number = get_sql_param( "kpi_number" );
	$project->status = "inprogress";
	$project->approved_by_id = get_logged_in_user_id();
	$project->approved_on = date( 'Y-m-d H:i:s' );  
  
  $project->update( false );
  
  ViewHelper::set_session_message( "projects", "Project successfully approved." );
  ViewHelper::redirect( "potential_projects.php" );
}

$project = new Project( $_REQUEST["id"] );

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Potential Projects" => "potential_projects.php", $project->get_display_name() => "project_info.php?id=" . $project->id, "Approve" => null ) ); ?>

<div class="wrapper">
	
	<div class="left-col">
		<table class="info full" cellspacing="0">
	    <tr>
	      <th>Name</th>
	      <td><?= $project->name ?></td>
	    </tr>
	    <tr>
	      <th>Source</th>
	      <td><?= $project->source ?></td>
	    </tr>
	    <tr>
	      <th>Year</th>
	      <td><?= $project->year ?></td>
	    </tr>
	    <tr>
	      <th>Description</th>
	      <td><?= $project->description ?></td>
	    </tr>
	    <tr>
	      <th>Ward</th>
	      <td><?= $project->get_ward()->name ?></td>
	    </tr>
	    <tr>
	      <th>Department</th>
	      <td><?= $project->get_department()->value ?></td>
	    </tr>
	    <tr>
        <th>Value</th>
        <td>R <?= $project->value ?></td>
      </tr>
			<tr>
        <th>Weight</th>
        <td><?= $project->weight ?></td>
      </tr>
	    <tr>
	      <th>Status</th>
	      <td><?= ViewHelper::display_status( $project->status ) ?></td>
	    </tr>
	  </table>
		
		<table class="info full" cellspacing="0">
      <tr class="blank">
        <td>
          <h5>Criterias</h5>
        </td>
      </tr>
      <? $index = 0;
         foreach( $project->criterias as $criteria ){ ?>
      <tr <?= $index % 2 == 1 ? "class=\"alt\"" : "" ?>>
        <td><?= $criteria->name ?> (<?= $criteria->weight ?>)</td>
      </tr>
      <?   $index++;
         } ?>
    </table>    
		
	</div>
	<div class="right-col">
		
		<? ViewHelper::validation( "frmProject" ); ?>
    
		<form id="frmProject" method="post">  
		  <table class="form full" cellspacing="0">
		    <tr>
          <th>Budget <?= ViewHelper::project_year( $project->year, 1 ) ?></th>
          <td>R <? ViewHelper::text( "budget_1", $project->budget_1, true ) ?></td>
        </tr>
				<tr>
          <th>Budget <?= ViewHelper::project_year( $project->year, 2 ) ?></th>
          <td>R <? ViewHelper::text( "budget_2", $project->budget_2, true ) ?></td>
        </tr>
				<tr>
          <th>Budget <?= ViewHelper::project_year( $project->year, 3 ) ?></th>
          <td>R <? ViewHelper::text( "budget_3", $project->budget_3, true ) ?></td>
        </tr>
				<tr>
          <th>Budget <?= ViewHelper::project_year( $project->year, 4 ) ?></th>
          <td>R <? ViewHelper::text( "budget_4", $project->budget_4, true ) ?></td>
        </tr>
				<tr>
          <th>Budget <?= ViewHelper::project_year( $project->year, 5 ) ?></th>
          <td>R <? ViewHelper::text( "budget_5", $project->budget_5, true ) ?></td>
        </tr>
				<tr>
          <th>KPI Number</th>
          <td><? ViewHelper::text( "kpi_number", $project->kpi_number, false ) ?></td>
        </tr>
				<tr>
		      <td></td><td><input type="submit" value="Save" /></td>
		    </tr>
			</table>
			<input type="hidden" name="id" value="<?= $project->id ?>" />
      <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />	
		</form>
	</div>
	
</div>

<? ViewHelper::actions( array( "Back"=>"potential_projects.php" ) ); ?>

<? include("inc_footer.php"); ?>