<?php
require_once("inc_header.php");
$create_step = 3;
//$create_type = "PROJ";
//$create_name = "Project";

$scdObj = new SDBP5PM_SCORECARD();
$sources = $scdObj->getKPASources(); 
//$kpas = $assessObj->getKPAList();

$sdbp5Obj = new SDBP5_PMS();
$kpas = $sdbp5Obj->getKPAs($scdObj->getPrimaryKPASource());
$head = $sdbp5Obj->getHeadings($scdObj->getPrimaryKPASource(),true,false);

unset($head['main']['kpi_natkpaid']);

echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);
?>
<form name=frm_weights>
	<input type=hidden name=obj_id value="<?php echo $_REQUEST['obj_id']; ?>" />
<?php

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$lineObj = new SDBP5PM_LINE();
$lines = $lineObj->getFullLinesByType($_REQUEST['obj_id']);
$weights = $lineObj->getLineWeights($_REQUEST['obj_id']);
$kpa_weights = $lineObj->getKPAWeightsByType($_REQUEST['obj_id']);



?>
<table id=tbl_list class=list>
<thead>
	<tr>
		<?php
		foreach($head['main'] as $fld=>$name) {
			echo "<th>".$name."</th>";
		}
		?>
		<th title="Up to 2 decimal places allowed.">Overall<br />Weight*</th>
		<th>KPA<br />Weight</th>
	</tr>
</thead>
<tbody>
<?php
$grand_tot = 0;
$types = array("KPI","PROJ");
$names = array('KPI'=>"KPIs",'PROJ'=>"Projects");
foreach($kpas as $k => $a) {
foreach($types as $kt) {	
	$tot = 0;
	if(isset($lines[$k][$kt]) && count($lines[$k][$kt])>0) {
		$kw = isset($kpa_weights[$k][$kt]) ? $kpa_weights[$k][$kt] : count($lines[$k][$kt]);
		//echo "<h3>".$a."</h3>";
		echo "<tr class=subth><td colspan=".(count($head['main'])+2).">".$a." - ".$names[$kt]."</td></tr>";
		//ASSIST_HELPER::arrPrint($lines[$k]);
		$objects = $lines[$k][$kt];
		foreach($objects as $i => $obj) {
			echo "
			<tr  class=\"".($obj['active']==1 ? "" : "inactive")."\">";
				foreach($head['main'] as $fld=>$name) {
					echo "<td>".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."</td>";
				}
				$w = (isset($weights[$i]) ? $weights[$i] : 1);
			echo "
				<td class=right><div style='width:60px'><input type=text name=weight[".$i."] size=4 kpa=".$k." kt=".$kt." line_id=".$i." class='right txt_weight kpa".$k.$kt."' value='".$w."' />%</div></td>
				<td id=td_".$k.$kt."_".$i." class=right>".(count($objects)>1 ? ($kw>0 ? round(($w/$kw)*100,2):"0.00") : "100.00")."%</td>
			</tr>";
			$tot+=(isset($weights[$i]) ? $weights[$i] : 1);
		}
		echo "
		<tr class=total>
			<th class='right b' colspan=".(count($head['main'])).">Total ".$a." - ".$names[$kt]." Weight:</th>
			<th class='right kpa_tot' id=td_".$k.$kt.">".$tot."%</th>
			<th class=right>100%</th>
		</tr>";
	}
	$grand_tot+=$tot;
}
}  
?>
	</tbody>
	<tfoot>
		<tr class=gtotal><th class='right b' colspan=<?php echo (count($head['main'])); ?>>Total Weight:</th><th class=right id=th_gtot><?php echo $grand_tot; ?>%</th><th class=right>-</th></tr>
	</tfoot>
</table>
<p>* Up to 2 decimal places allowed.</p>
</form>

<p class=center><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Save & Next Step</button></p>






<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;
	
	$(".txt_weight").addClass("right").blur(function() {
		//update kpa total
		var k = $(this).attr("kpa");
		var t = $(this).attr("kt");
		var tot = 0;
		$(".kpa"+k+t).each(function() {
			tot+=parseFloat($(this).val());
		});
		$("#td_"+k+t).html(tot.toFixed(2)+"%");
		
		//update row totals
		var x = 0;
		$(".kpa"+k+t).each(function() {
			x=parseFloat($(this).val());
			$("#td_"+k+t+"_"+$(this).attr("line_id")).html(((x/tot)*100).toFixed(2)+"%");
		});
		
		//update grand total
		var gt = 0;
		$(".kpa_tot").each(function() {
			gt+=parseFloat(AssistString.substr($(this).html(),0,-1));
		});
		$("#th_gtot").html(gt.toFixed(2)+"%");
		if(gt!=100) {
			$("#th_gtot").css({"background-color":"#CC0001","color":"#FFFFFF"});
			$("#btn_next").removeClass("ui-button-state-ok").addClass("ui-state-default").css({"color":"","border":""}).prop("disabled","disabled");
		} else {
			$("#th_gtot").css({"background-color":"","color":""});
			$("#btn_next").prop("disabled","").removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"});
		}
	});
	
	
	
	
	
	
	$("table.noborder, table.noborder td").css("border","0px");

	$("#btn_next").button({
		icons: {primary: "ui-icon-disk", secondary: "ui-icon-arrowthick-1-e"},
	}).click(function() {
		var err = false;
		/*$("td.total_weight").each(function() {
			if(($(this).html()*1)!=100) {
				err = true;
			}
		});*/
		if(err) {
			alert("Not all KPA Weights total 100.  Please review the weights again.");
		} else {
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_weights]"));
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.saveWeights",dta);
			if(result[0]=="ok") {
				var url = "assessment_<?php echo $page_action; ?>_step<?php echo ($create_step+1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				//document.location.href = 
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
		//document.location.href = 'assessment_create_step3.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	});

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #cc0001"
	});

	$(".txt_weight:first").trigger("blur");
});

</script>