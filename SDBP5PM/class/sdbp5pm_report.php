<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class SDBP5PM_REPORT extends SDBP5PM {
	
	private $reports = array(
		'dashboard'		=> array(
			'title'=>"Performance Dashboard",
			'description'=>""
		),
		'eval_results'	=> array(
			'title'=>"Evaluation Results Report",
			'description'=>""
		),
		'perf_rating'	=> array(
			'title'=>"Performance Rating Report",
			'description'=>""
		),
		'bonus'			=> array(
			'title'=>"Bonus Percentage Report",
			'description'=>""
		),
		'activity'		=> array(
			'title'=>"Audit Log Report",
			'description'=>""
		),
		'perf_status'	=> array(
			'title'=>"Individual Performance Status Report",
			'description'=>""
		),
	);
    
	protected $object_id = "";
    /*
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	
	protected $formatted_db_name = "CONCAT(MONTHNAME(TS.end_date),IF(YEAR(TS.end_date)=YEAR(TE.end_date),'',CONCAT(' ',YEAR(TS.end_date))),' - ',MONTHNAME(TE.end_date), ' ',YEAR(TE.end_date))";
	protected $parent_field = "_assessid";
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
    const OBJECT_TYPE = "PERIOD";
    const TABLE = "periods";
    const TABLE_FLD = "prd";
    const REFTAG = "SP";
	const LOG_TABLE = "periods";
     */
	/**
	 * STATUS CONSTANTS
	 */
	//const SELF_COMPLETED = 1024;
	//const MOD_COMPLETED = 2048;
	//const FINAL_COMPLETED = 8192;
    
    public function __construct($object_id="") {
        parent::__construct();
/*
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if(strlen($object_id) > 0) {
	    	$this->object_id = $object_id;
		}
		$this->object_form_extra_js = "";
    }
    
	
	
	
	
	
	public function getListOfReports() { return $this->reports; }
	public function setReportID($i) { $this->object_id = $i; }
	
	public function getReportTitle($i="") {
		if(strlen($i)==0) { $i = $this->object_id; }
		if(strlen($i)==0) {
			return "No report selected.  Please go back and try again."; 
		} else {
			return $this->reports[$i]['title'];
		}
	}
	
	public function getReportData($var,$i="") {
		if(strlen($i)==0) { $i = $this->object_id; }
		if(strlen($i)==0) {
			$echo = "No report selected.  Please go back and try again."; 
		} else {
			$echo = "";
			
			switch($i) {
				case 'dashboard':
					$echo = $this->getDashboardReport($var);
					break;
				case 'eval_results':
					$echo = $this->getEvalResultsReport($var);
					break;
				case 'perf_rating':
					$echo = $this->getPerfRatingReport($var);
					break;
				case 'bonus':
					$echo = $this->getBonusReport($var);
					break;
				case 'activity':
					$echo = $this->getActivityReport($var);
					break;
				case 'perf_status':
					$echo = $this->getPerfStatusReport($var);
					break;
				default:
					$echo = "Unknown report ($i) selected.  Please go back and try again.";
					break;
			}
			
		}
		return $echo;
	}
	
	

	private function getDashboardReport($var) {
		$echo = "";
		return $echo;
	}
	

	private function getEvalResultsReport($var) {
		$echo = "";
		return $echo;
	}


	private function getPerfRatingReport($var) {
		$echo = "";
		return $echo;
	}


	private function getBonusReport($var) {
		$echo = "";
		return $echo;
	}


	private function getActivityReport($var) {
		$echo = "";
		return $echo;
	}


	private function getPerfStatusReport($var) {
		$echo = "";
		return $echo;
	}




	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*********************************************************************************
	 * DEFAULT FUNCTIONS - disabled for REPORT class
	 */
	
	/*******************************
	 * CONTROLLER functions
	 */
	/*
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		$sql = "hello";
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
		*/
	/*
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * 
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    


	public function getAObject($id=0,$options=array()) {
		$data = array();
		$rows = $this->getAssessmentPeriods(false,0,$id);
		return $data;
	}
	 * */
	
	
	
	/***
	 * Returns an unformatted array of an object 
	public function getRawObject($obj_id) {
		$sql = "SELECT *, prd_startid as start, prd_endid as end FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	 */
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	


}


?>