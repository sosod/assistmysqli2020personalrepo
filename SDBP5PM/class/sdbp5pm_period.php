<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class SDBP5PM_PERIOD extends SDBP5PM {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	
	protected $formatted_db_name = "CONCAT(MONTHNAME(TS.end_date),IF(YEAR(TS.end_date)=YEAR(TE.end_date),'',CONCAT(' ',YEAR(TS.end_date))),' - ',MONTHNAME(TE.end_date), ' ',YEAR(TE.end_date))";
    /*
	protected $parent_field = "_assessid";
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PERIOD";
    const TABLE = "periods";
    const TABLE_FLD = "prd";
    const REFTAG = "SP";
	const LOG_TABLE = "periods";
	/**
	 * STATUS CONSTANTS
	 */
	//const SELF_COMPLETED = 1024;
	//const MOD_COMPLETED = 2048;
	//const FINAL_COMPLETED = 8192;
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
/*		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	/*
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		$sql = "hello";
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
		*/
	/*
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    



	public function getPeriodsSQL($flds=array(),$tbls=array(),$where="",$active_only=true) {
		$tbl_fld = $this->getTableField();
		$sql = "SELECT P.*, ".$this->getIDFieldName()." as id
				, ".$this->formatted_db_name." as name
				, TS.end_date as start_date
				, TE.end_date as end_date 
				".(count($flds)>0 ? ", ".implode(",",$flds) : "")."
				FROM ".$this->getTableName()." P 
				INNER JOIN ".$this->getDBRef()."_list_time TS ON P.".$tbl_fld."_startid = TS.id
				INNER JOIN ".$this->getDBRef()."_list_time TE ON P.".$tbl_fld."_endid = TE.id 
				".(count($tbls)>0 ? " ".implode(" ",$tbls) : "")."";
		if(strlen($where)>0 || $active_only) {
			$sql.= " WHERE ".($active_only ? " (P.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE : " 1 ");
			$sql.=$where;
		}
		$sql.=" ORDER BY TS.start_date, TE.end_date";
		
		return $sql;
	}


	
    /**
	 * Returns a formatted list of the Periods
	 * @param not_in_use (BOOL)
	 * @param scd_id (INT) - for use with not_in_use
	 * @param obj_id (INT) - if to limit to only 1 period
	 * @return data (Array)
	 */
	public function getAssessmentPeriods($not_in_use=false,$scd_id=0,$obj_id=0) {
		/*$tbl_fld = $this->getTableField();
		$sql = "SELECT P.*, ".$this->getIDFieldName()." as id
				, ".$this->formatted_db_name." as name
				, TS.end_date as start_date
				, TE.end_date as end_date 
				FROM ".$this->getTableName()." P 
				INNER JOIN ".$this->getDBRef()."_list_time TS ON P.".$tbl_fld."_startid = TS.id
				INNER JOIN ".$this->getDBRef()."_list_time TE ON P.".$tbl_fld."_endid = TE.id ";
		if($obj_id==0) {
			$sql.= " WHERE (P.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		} else {
			$sql.= " WHERE ".$this->getIDFieldName()." = ".$obj_id;
		}
		$sql.=" ORDER BY TS.start_date, TE.end_date";*/
		/**
		 * if(not_in_use===true)
		 * 	filter for assess_id
		 */
		$where = "";
		 
		 
		$sql = $this->getPeriodsSQL(array(),array(),$where);
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		 
		 
		 
		return $data;
	}
	
	
	/***
	 * Get a list of Assessment periods formatted for use in a drop down.
	 * @param BOOL not_in_use = false 
	 * @param INT scd_id = 0
	 * @return ARRAY(id=>title)
	 */
	public function getAssessmentPeriodsForSelect($not_in_use=false,$scd_id=0) {
		$data = $this->getAssessmentPeriods($not_in_use,$scd_id);
		
		$list = array();
		foreach($data as $key => $d) {
			$list[$key] = $d['name'];
		}
		return $list;
	}
	
	public function getAllAssessmentPeriodsForSelect() {
		$where = "";
		 
		 
		$sql = $this->getPeriodsSQL(array(),array(),$where, false);
		$data = $this->mysql_fetch_all_by_id($sql, "id");
		
		$list = array();
		foreach($data as $key => $d) {
			$list[$key] = $d['name'];
		}
		return $list;
		
	}
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
    
	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options); 
	}
	*/
	public function getAObject($id=0,$options=array()) {
		$data = array();
		$rows = $this->getAssessmentPeriods(false,0,$id);
		return $data;
	}
	/*
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT *, prd_startid as start, prd_endid as end FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		return $data;
	}
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	public function getTimePeriodsByAssessmentPeriod($period_id=0) {
		$row = $this->getRawObject($period_id);
		$start_id = $row['start'];
		$end_id = $row['end'];
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id >= $start_id AND id <= $end_id ORDER BY start_date, end_date";
		$rows = $this->mysql_fetch_all_by_id($sql,"id");
		$data = array();
		foreach($rows as $key => $r) {
			$data[$key] = date("F Y",strtotime($r['end_date']));
		}
		return $data;
	}
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
        
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */

     
     
     
     
     
     
     
     
     
     
     
     
     

}


?>