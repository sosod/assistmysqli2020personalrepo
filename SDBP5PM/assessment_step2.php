<?php
//$page_action = create/edit required from calling page
require_once("inc_header.php");
$create_step = 2;
$create_type = "PROJ";
$create_name = "Project";

$scdObj = new SDBP5PM_SCORECARD();
$sources = $scdObj->getKPASources(); 
//$kpas = $assessObj->getKPAList();

$sdbp5Obj = new SDBP5_PMS();
$kpas = $sdbp5Obj->getKPAs($scdObj->getPrimaryKPASource());
$head = $sdbp5Obj->getHeadings($scdObj->getPrimaryKPASource(),true,false);

echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);

//ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table id=tbl_filter>
	<tr>
		<td>Source:</td>
		<td>
			<select id=sel_source>
				<option selected value=X>--- SELECT ---</option>
				<?php
				foreach($sources as $src) {
					echo "<option value=".$src['modref']." modloc='".$src['modlocation']."' >".$src['modtext']." (".$src['modref'].")</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Filter by:</td>
		<td><select id=sel_filter><option value=X selected>CHOOSE SOURCE ABOVE</option></select></td>
	</tr>
	<tr>
		<td></td>
		<td><button id=btn_filter>Go</button></td>
	</tr>
</table>
<h3>Existing <?php echo $create_name; ?>s</h3>
<?php
$lineObj = new SDBP5PM_LINE();
$lines = $lineObj->getAllLines($_REQUEST['obj_id'],$create_type);
$visible=false;
foreach($kpas as $k => $a) {
	if(isset($lines[$k])) {
		echo "<h4>".$a."</h4>";
		//ASSIST_HELPER::arrPrint($lines[$k]);
		$objects = $lines[$k];
		$visible = true;
		?>
	<table id=tbl_list_<?php echo $k; ?> class=list>
	<thead>
		<tr>
			<?php
			foreach($head['main'] as $fld=>$name) {
				echo "<th>".$name."</th>";
			}
			?>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach($objects as $i => $obj) {
			?>
			<tr class="<?php echo ($obj['active']==1 ? "" : "inactive"); ?>">
				<?php
				foreach($head['main'] as $fld=>$name) {
					echo "<td>".$obj[$fld].(
							$fld=="ref" && $obj['active']!=1 ? "<br /><span class=i style='font-size:80%'>[Deleted at Source]</span>" : ""
							)."</td>";
				}
				?>
				<td><button class=btn_del key=<?php echo $i; ?>>Delete</button></td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
		<?php
	} else {
	//	echo "<p>No KPIs assigned.</p>";
	}
} 
if(!$visible) {
	echo "<p>No ".$create_name."s selected.  To select ".$create_name."s, please select the appropriate filters above and click the 'Go' button.</p>";
} 
?>



<p class=center><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Next Step</button></p>





<div id=dlg_kpi title="Choose Projects">
	<iframe id=frm_kpi style='border:0px solid #ffffff;'>
		
	</iframe>
</div>

<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;
	
	$("#frm_kpi").css({"width":ifrWidth+"px","height":ifrHeight+"px"});
	
	$("table.noborder, table.noborder td").css("border","0px");
	$("#tbl_filter td:first").addClass("b");
	
	$("#sel_source").change(function() {
		if($(this).val()!="X") {
			AssistHelper.processing();
			dta = "type=<?php echo $create_type; ?>&modref="+$(this).val();
			//console.log(dta);
			r = AssistHelper.doAjax("inc_controller_assessment.php?action=SDBP5.getFilters",dta);
			if(r.subs!=null && r.subs!="undefined" && r.owners!=null && r.owners!="undefined") {
				if(r.subs.length>0 && r.owners.length>0) {
					$("#sel_filter option").remove();
					$("#sel_filter").append("<option value=X selected>--- SELECT FILTER ---</option>");
					$("#sel_filter").append("<option value=XS>--- Sub-Directorate ---</option>");
					var x = r.subs;
					for(i in x) {
						$("#sel_filter").append("<option filter_type=sub value="+x[i]['id']+">"+x[i]['value']+" ("+x[i]['c']+" Project"+(x[i]['c']!=1?"s":"")+")</option>");
					}
					$("#sel_filter").append("<option value=XO>--- KPI Owner ---</option>");
					x = r.owners;
					for(i in x) {
						$("#sel_filter").append("<option filter_type=owner value="+x[i]['id']+">"+x[i]['value']+" ("+x[i]['c']+" Project"+(x[i]['c']!=1?"s":"")+")</option>");
					}
				}
			}
			AssistHelper.closeProcessing();
		} else {
			$("#sel_filter option").remove();
			$("#sel_filter").append("<option value=X selected>CHOOSE SOURCE ABOVE</option>");
		}
	});
	
	$("#dlg_kpi").dialog({
		autoOpen: false,
		modal: true,
		width: dlgWidth,
		height: dlgHeight
	});
	
	$("#btn_filter").button({
		icons: {primary: "ui-icon-arrowreturnthick-1-e"},
	}).click(function() {
		AssistHelper.processing();
		var src = $("#sel_source").val();
		var ft = $("#sel_filter").val();
		if(src=="X" || ft=="X") {
			AssistHelper.finishedProcessing("error","Please select the filter.");
		} else {
			var ft_type = $("#sel_filter option:selected").attr("filter_type");
			$("#dlg_kpi #frm_kpi").prop("src","assessment_<?php echo strtolower($create_type); ?>.php?page_action=<?php echo $page_action; ?>&obj_id=<?php echo $_REQUEST['obj_id']; ?>&modref="+src+"&filter_type="+ft_type+"&filter_id="+ft);
		}
		//$("#dlg_kpi").dialog("open");
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});


	$(".btn_del").button({
		icons: {primary: "ui-icon-trash"},
	}).click(function() {
		var ref = ($(this).parent().parent().find("td:first").html());
		var key = $(this).attr("key");
		if(confirm("Are you sure you want to remove <?php echo $create_name; ?> "+ref+"?")==true) {
			AssistHelper.processing();
			var dta = "type=<?php echo $create_type; ?>&object_id="+key+"&ref="+ref;
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.Deactivate",dta);
			if(result[0]=="ok") {
				document.location.href = "assessment_<?php echo $page_action; ?>_step2.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>&r[]=ok&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"
	});

	$("#btn_next").button({
		icons: {secondary: "ui-icon-arrowthick-1-e"},
	}).click(function() {
		document.location.href = 'assessment_<?php echo $page_action; ?>_step3.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	});

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #cc0001"
	});

	
});
function openDialog() {
	$(function() {
		$("#dlg_kpi").dialog("open");
		AssistHelper.closeProcessing();
		//AssistHelper.hideDialogTitlebar("id","dlg_kpi");
	});
}
function reloadPage(r) {
	document.location.href = "assessment_<?php echo $page_action; ?>_step2.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>&r[]=ok&r[]="+r[1]; 
}
</script>