<?php
require_once("../module/autoloader.php");
$result = array("info","Sorry, I couldn't figure out what you wanted me to do.");

/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */
$my_action = explode(".",$_REQUEST['action']);
$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);
$page_direct = $_REQUEST['page_direct'];
unset($_REQUEST['page_direct']);













switch($class) {
	case "DISPLAY":
		$object = new SDBP5PM_DISPLAY();
		break;
	/**
	 * Log object
	 */
	case "LOG":
		$log_table = $_REQUEST['log_table'];
		unset($_REQUEST['log_table']);
		$object = new SDBP5PM_LOG($log_table);
		break;
	/**
	 * Setup objects
	 */
	case "CONTRACTOWNER":
		$object = new SDBP5PM_CONTRACT_OWNER();
		break;
	case "USERACCESS":
		$object = new SDBP5PM_USERACCESS();
		break;
	case "LISTS":
		$list_id = $_REQUEST['list_id'];
		unset($_REQUEST['list_id']);
		$object = new SDBP5PM_LIST($list_id);
		break;
	case "MENU":
		$object = new SDBP5PM_MENU();
		break;
	case "NAMES":
		$object = new SDBP5PM_NAMES();
		break;
	case "HEADINGS":
		$object = new SDBP5PM_HEADINGS();
		break;
	case "PROFILE":
		$object = new SDBP5PM_PROFILE();
		break;
	/**
	 * Module objects
	 */
	case "CONTRACT":
		$object = new SDBP5PM_CONTRACT();
		break;
	case "DELIVERABLE":
		$object = new SDBP5PM_DELIVERABLE();
		break;
	case "ACTION":
		$object = new SDBP5PM_ACTION();
		break;
	case "FINANCE":
		$object = new SDBP5PM_FINANCE();
		break;
	case "TEMPLATE":
		$object = new SDBP5PM_TEMPLATE();
		break;
}
$result[1].=":".$my_action.":";
if(isset($object)) {
	switch($activity) {
		case "GET_ATTACH":
			$object_id = $_REQUEST['object_id'];
			$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			$attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
			$folder = $object->getParentFolderPath();
			$old = $folder."/".$attach['location']."/".$attach['system_filename'];
			$new = $attach['original_filename'];
			$object->downloadFile3($old,$new);
			break;
	}
}

?>