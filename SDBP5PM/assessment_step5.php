<?php
require_once("inc_header.php");
$create_step = 5;
$create_type = "CC";
$create_name = "Core Competency";

$lineObj = new SDBP5PM_LINE();
$lines = $lineObj->getLineSrcIDs($_REQUEST['obj_id'], $lineObj->getModRef(),$create_type);
$weights = $lineObj->getLineWeights($_REQUEST['obj_id'],$create_type);

//ASSIST_HELPER::arrPrint($lines);
//ASSIST_HELPER::arrPrint($weights);

$listObj = new SDBP5PM_LIST("competencies");
//$kpas = $sdbp5Obj->getKPAs($assessObj->getPrimaryKPASource());
//$head = $sdbp5Obj->getHeadings($assessObj->getPrimaryKPASource(),true,false);
$objects = $listObj->getListItems();

$scdObj = new SDBP5PM_SCORECARD();
echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<form name=frm_cc>
	<input type=hidden name=obj_id value="<?php echo $_REQUEST['obj_id']; ?>" />
<table class=list id=tbl_list>
	<tr>
		<th>Core Competency</th>
		<th>Description</th>
		<th title="Up to 2 decimal places allowed.">Weight*</th>
	</tr>
	<?php
	$tot = 0;
	foreach($objects as $i => $obj) {
		if(isset($lines[$i])) {
			$li = $lines[$i];
			$w = isset($weights[$li]) ? $weights[$li] : 1;
			$tot+=$w;
			echo "
			<tr>
				<td>".$obj['name']."</td>
				<td>".$obj['description']."</td>
				<td><input type=text size=4 class=txt_weight name=weight[".$li."] value='".$w."' />%</td>
			</tr>";
		}
	}
	?>
	<tr class=total>
		<td></td>
		<td class=right>Total weight:</td>
		<td class=right id=td_tot><?php echo $tot; ?>%</td>
	</tr>
</table>
<p>* Up to 2 decimal places allowed.</p>
</form>
<?php
?>


<p class=center><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Save & Next</button></p>





<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;
	
	
	$("table.noborder, table.noborder td").css("border","0px");
	
	$("input:text").addClass("right");

	$("#btn_next").button({
		icons: {primary:"ui-icon-disk",secondary: "ui-icon-arrowthick-1-e"},
	}).click(function() {
		//document.location.href = 'assessment_create_step5.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_cc]"));
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.saveWeights",dta);
			if(result[0]=="ok") {
				var url = "assessment_<?php echo $page_action; ?>_step<?php echo ($create_step+1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	});

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		if(confirm("Are you sure you wish to go back?  Any changes made on this page will be lost.")==true) {
			document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #cc0001"
	});


	$(".txt_weight").blur(function() {
		var w = 0;
		$(".txt_weight").each(function() {
			w+=($(this).val()*1);
		});
		$("#td_tot").html(w.toFixed(2)+"%");
		if(w==100) {
			$("#td_tot").css({"background-color":"","color":""});
		} else {
			$("#td_tot").css({"background-color":"#CC0001","color":"#FFFFFF"});
		}
	});

	$(".txt_weight:first").trigger("blur");
});
</script>