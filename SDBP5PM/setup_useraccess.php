<?php
require_once("inc_header.php");

$userObject = new SDBP5PM_USERACCESS();
$user_access_fields = $userObject->getUserAccessFields();
$user_access_defaults = $userObject->getUserAccessDefaults();
$users_not_set = $userObject->getUsersWithNoAccess();
$active_users = $userObject->getActiveUsers();
$js = "";


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_add>
	<table class=list id=tbl_useraccess>
		<tr>
			<th></th>
			<th>User</th>
			<?php
			foreach($user_access_fields as $fld => $name){
				echo "<th>".str_replace(" ","<br />",$name)."</th>";
			}
			?>
			<th></th>
		</tr> 
	<?php if(count($users_not_set)>0) { ?>
		<tr>
			<td></td>
			<td><select name=ua_tkid><option value=X selected>--- SELECT ---</option><?php
			foreach($users_not_set as $id=>$name){
				echo "<option value='".$id."'>".$name."</option>";
			}
			?></select></td>
			<?php 
			foreach($user_access_fields as $fld=>$name){
				echo "<td>";
				$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>$fld,'yes'=>1,'no'=>0,'form'=>"vertical"),$user_access_defaults[$fld]);
				echo "</td>";
			} 
			?>
			<td><input type=submit value=Add id=btn_add /></td>
		</tr>
	<?php
	}
	foreach($active_users as $ua) {
		echo "
		<tr>
			<td>".$ua['tkid']."</td>
			<td>".$ua['name']."</td>";
		foreach($user_access_fields as $fld => $name) {
			echo "<td style='text-align: center'>".$helper->getDisplayIcon(($ua[$fld]==1?"ok":"error"))."</td>";
		}
		echo "
			<td><input type=button value=Edit class=btn_edit id='".$ua['ua_id']."' /></td>
		</tr>";
	}
	?>
	</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"user",array('section'=>"USER")); ?></td>
	</tr>
</table>
<script type="text/javascript">
	$(function() {
		<?php echo $js; ?>
		$("#tbl_useraccess tr").not("tr:first").next().find("span.ui-icon").css("margin","0 auto");
		//$("span.ui-icon").css("margin","0 auto");
		$("#btn_add").click(function(){
			if($("select[name=ua_tkid]").val()=="X") {
				alert("Please select a user.");
			} else {
				AssistHelper.processing();
				var dta = AssistForm.serialize($("form[name=frm_add]"));
				//console.log(dta);
				//alert(dta);
				var result = AssistHelper.doAjax("inc_controller.php?action=UserAccess.Add",dta);
				//console.log(result);
				if(result[0]=="ok") {
					document.location.href = "setup_useraccess.php?r[]="+result[0]+"&r[]="+result[1];
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
			return false;
		});
	});
</script>
<div id=dlg_edit title="Edit User">
	<h1>Edit: <span id=spn_edit></span></h1>
	<form name=frm_edit>
		<input type=hidden value="" id=ua_id name=ua_id />
		<table class=form width=400px id=tbl_edit>
<!--			<tr>
				<th width=40%>User:</th>
				<td width=60% id=td_user></td>
</tr> -->
			<?php 
			foreach($user_access_fields as $fld=>$name){
				echo "<tr><th>".str_ireplace(" ", "&nbsp;", $name).":</th>";
				echo "<td>";
				$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"edit_".$fld,'name'=>$fld,'yes'=>1,'no'=>0,'form'=>"horizontal"),$user_access_defaults[$fld]);
				echo "</td>
				</tr>";
			} 
			?>
		</table>
</div>
<script type="text/javascript">
	$(function() {
		$("#dlg_edit").dialog({
			modal:true,
			autoOpen:false,
			width: "450px",
			buttons:[{
				text:"Save Changes",
				click:function() {
					AssistHelper.processing();
					var dta = AssistForm.serialize($("form[name=frm_edit]"));
					var result = AssistHelper.doAjax("inc_controller.php?action=UserAccess.Edit",dta);
					if(result[0]=="ok") {
						document.location.href = "setup_useraccess.php?r[]="+result[0]+"&r[]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
				}
			},{
				text:"Cancel",
				click: function(){
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id","dlg_edit");
		AssistHelper.formatDialogButtons($("#dlg_edit"),0,AssistHelper.getDialogSaveCSS());
		AssistHelper.formatDialogButtons($("#dlg_edit"),1,AssistHelper.getCloseCSS());
		$(".btn_edit").click(function() {
			var i = $(this).prop("id");
			$("#dlg_edit #ua_id").val(i);
			var tk = $(this).parent().parent().find("td:eq(1)").html();
			//$("#dlg_edit #td_user").html(tk);
			$("#dlg_edit #spn_edit").html(tk);
			$("#dlg_edit").dialog("open");			
			var i = 0;
			$(this).parent().parent().find("td:gt(1)").each(function() {
				if($(this).find("span").hasClass("ui-icon-check")) {
					$("#tbl_edit tr:eq("+i+") td button.btn_yes").trigger("click");
				} else {
					$("#tbl_edit tr:eq("+i+") td button.btn_no").trigger("click");
				}
				i++;
			});
			AssistHelper.focusDialogButtons($("#dlg_edit"),1);
			AssistHelper.blurDialogButtons($("#dlg_edit"),1);
		});
	});
</script>