<?php



/**************
 * CODE TO MEASURE PAGE LOAD TIME 
 * ******************
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>'.$s.' => '.$total_time.' seconds.';
	
}
*/

//error_reporting(-1);

require_once ("../module/autoloader.php");
//marktime("autoloader");
/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/",$self);
$self = explode(".",end($self));
$page = $self[0];
if(isset($my_page)) {
	$page.="_".$my_page;
}
$navigation_path = explode("_",$page); //ASSIST_HELPER::arrPrint($navigation_path);
//marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */
$redirect = array(
	'assessment'		=> "assessment_create",
	'manage'			=> "manage_self",
	'admin'				=> "admin_self",
	'report'			=> "report_fixed",
	'setup'				=> "setup_defaults",
	'trigger'			=> "trigger_assessments",
);/*
    'new'				=> "new_contract_edit",
    'new_contract'		=> "new_contract_edit",
    'new_activate'		=> "new_activate_waiting",
    'manage'			=> "manage_view",
    'manage_update'		=> "manage_update_action",
    'manage_edit'		=> "manage_edit_deliverable",
    'manage_approve'	=> "manage_approve_action",
    'manage_assurance'	=> "manage_assurance_contract",
    'admin'				=> "admin_update",
    'admin_update'		=> "admin_update_action",
    'admin_template'	=> "admin_template_new",
    'report'			=> "report_generate_contract",
    'report_generate'	=> "report_generate_contract",
    'search'			=> "search_contract",
    'setup'				=> "setup_defaults"
);*/
if(isset($redirect[$page])) {
    header("Location:".$redirect[$page].".php");
}
//marktime("redirect");
/************
 * Process Navigation buttons and Breadcrumbs
 */
$menuObject = new SDBP5PM_MENU();
$menu = $menuObject->getPageMenu($page); 
 
//ASSIST_HELPER::arrPrint($menu);
 
//if($navigation_path[0]=="admin" && $navigation_path[1]=="update" && $menu['buttons'][1][0]['link']!="admin_update.php") {
//	header("Location:".$menu['buttons'][1][0]['link']);
//} elseif($navigation_path[0]=="new" && $navigation_path[1]=="contract" && $menu['buttons'][1][0]['link']!="new_contract.php") {
//	header("Location:".$menu['buttons'][1][0]['link']);
//}
 //marktime("menu");
  
/***********
 * Start general header
 */

$headingObject = new SDBP5PM_HEADINGS();
$displayObject = new SDBP5PM_DISPLAY();
$nameObject = new SDBP5PM_NAMES();
$helper = new SDBP5PM();

$s = array(
	"/library/js/assist.ui.paging.js",
	"/library/jquery-plugins/jscolor/jscolor.js",
	);
//	"/".$helper->getModLocation()."/common/SDBP5PMhelper.js",
//	"/".$helper->getModLocation()."/common/SDBP5PM.js",
//	"/".$helper->getModLocation()."/common/hoverIntent.js"
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

//$helper->arrPrint($_REQUEST);

$contract_object_name = $helper->getContractObjectName();
$deliverable_object_name = $helper->getDeliverableObjectName();
$action_object_name = $helper->getActionObjectName();
$finance_object_name = $helper->getObjectName("FINANCE");
//$template_object_name = $helper->getObjectName("TEMPLATE");

$js = ""; //to catch any js put out by the various display classes

//marktime("general");

/*********
 * Module-wide Javascript
 */
?>
<script type="text/javascript" >
$(document).ready(function() {
    window.contract_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($contract_object_name); ?>");
    window.deliverable_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($deliverable_object_name); ?>");
    window.action_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($action_object_name); ?>");
    window.finance_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($finance_object_name); ?>");
    

	//$("table.tbl-container, table.tbl-container td:first").addClass("noborder");
	//$("table.tbl-container tr").find("td:first").addClass("noborder");
	//$("table.tbl-subcontainer").parent("td").addClass("noborder");
	//$("table.sub_table").addClass("noborder").find("td").addClass("noborder");
	$("table.tbl-container").addClass("noborder");
	$("table.tbl-container:not(.not-max)").css("width","100%");
	$("table.tbl-container td").addClass("noborder");
	$("table.tbl-container").find("table:not(.tbl-container)").find("td").removeClass("noborder");
	$("table.th2").find("th").addClass("th2");
	$("table tr.th2").find("th").addClass("th2");
	$("table.tbl_audit_log").css("width","100%").find("td").removeClass("noborder");
	//$("h2").css("margin-top","0px");
	$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");



	//FINANCE specific formatting
	$("table.finance").addClass("form");
	$("table.finance th.DEFAULT").css({"background-color":"#FFFFFF","color":"#000000","border":"1px solid #ababab"});
	
	$("table.finance th.SUB_TOTAL").addClass("th2");
	$("table.finance td.SUB_TOTAL").css({"font-weight":"bold"});
	
	$("table.finance th.TOTAL").parent().addClass("total");
	//$("table.finance td.TOTAL").addClass("th2");
	
	$("table.finance-list tr:gt(0)").find("td:gt(0)").addClass("right");

	$("button.abutton").children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px"});

	//$("button.size_button").children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"});


	//TREE VIEW SETTINGS
	$("table.tbl-tree tr:eq(2)").find("td:first").prop("width","500px");

});
	function resizeButtons($btns) {
		$(function() {
			$btns.each(function() {
				$(this).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"});
			});
		});
	}


</script>
<style type="text/css">
	.noborder { border: 0px dashed #cc0001; }
	table.tbl_audit_log { width: 100%; }
	table.attach { border: 0px dashed #009900; }
	table.attach td { 
		border: 0px dashed #009900;
		vertical-align: middle;
		padding: 1px; 
	}
	.button_class, .ui-widget.button_class {
		font-size: 75%;
		padding: 1px;
	}
	
	tr.disabled td {
		color: #777777;
	}
	
	tr.subth td {
		background-color: #ccccff;
		font-weight: bold;
		color: #000099;
		font-size: 125%;
		line-height: 130%;
	}
	
	tr.total th {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
		border: 1px solid #ababab;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
	tr.gtotal th {
		background-color: #cdcdcd;
		font-weight: bold;
		color: #000099;
		border: 1px solid #ababab;
		font-size:125%;
		line-height: 130%;
	}
	.abutton {
		font-size: 90%;
		padding: 0px;
		font-weight: bold;
	}
	
	table.tbl-tree {
		border: 0px solid #fe9900;
	}
	table.tbl-tree th {
		border: 0px solid #fe9900;
		color: #000099;
		background-color: #ffffff;
		font-size: 150%;
		line-height: 155%;
		font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
	}
	table.tbl-tree td {
		border: 0px solid #fe9900;
	}
	table.tbl-tree td.cate-pos, table.tbl-tree tr.cate-pos td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
		font-size: 115%;
		background-color: #9494d4;
		color: #000000;
	}
	table.tbl-tree td.td-line-break, table.tbl-tree tr.td-line-break td {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
		font-size: 0.5em;
		line-height: 0.5em;
		padding: 1px;
		background-color: #FFFFFF;
	}

	table.tbl-tree td.grand-parent, table.tbl-tree tr.grand-parent td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
		font-size: 125%;
	}
	table.tbl-tree td.parent, table.tbl-tree tr.parent td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
	}
	table.tbl-tree td.sub-parent, table.tbl-tree tr.sub-parent td {
		font-weight: normal;
		text-decoration: none;
		font-style: italic;
	}
	table.tbl-tree td.child, table.tbl-tree tr.child td {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
	}
	.white-border {
		border-color: #FFFFFF;
	}
	
	table.tbl-tree tr.grand-parent td.td-button, table.tbl-tree tr.grand-parent td.count {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
	}
	
	button:disabled {
		cursor: not-allowed; 
	}
	
	.disabled-button {
		color: #777777;
		border: 1px solid #777777;
		background-color: #efefef;
	}
 
.ui-button-state-ok, .ui-widget-content .ui-button-state-ok, .ui-widget-header .ui-button-state-ok  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #009933;
	cursor: pointer; 
}
.ui-button-state-ok a, .ui-widget-content .ui-button-state-ok a,.ui-widget-header .ui-button-state-ok a { 
	color: #363636; 
	padding: 0px;
	cursor: pointer; 
}

.ui-button-state-info, .ui-widget-content .ui-button-state-info, .ui-widget-header .ui-button-state-info  {border: 0px solid #ffffff; background: url(); color: #fe9900; }
.ui-button-state-info a, .ui-widget-content .ui-button-state-info a,.ui-widget-header .ui-button-state-info a { color: #fe9900; }

.ui-button-state-default, .ui-widget-content .ui-button-state-default, .ui-widget-header .ui-button-state-default  {border: 1px solid #888888; background: url() ; color: #888888; }
.ui-ui-button-state-default a, .ui-widget-content .ui-button-state-default a,.ui-widget-header .ui-button-state-default a { color: #363636; }

.ui-state-error-button, .ui-widget-content .ui-state-error-button, .ui-widget-header .ui-state-error-button  {border: 0px solid #cc0001; background: url() ; color: #cd0a0a; }
.ui-state-error-button a, .ui-widget-content .ui-state-error-button a,.ui-widget-header .ui-state-error-button a { color: #363636; }


.ui-state-icon-button, .ui-widget-content .ui-state-icon-button, .ui-widget-header .ui-state-icon-button  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #363636;
	padding: 0px;
	margin: 0px;  
}
.ui-state-icon-button a, .ui-widget-content .ui-state-icon-button a,.ui-widget-header .ui-state-icon-button a { color: #363636; }

.ui-state-focus-icon-button {
	border: 0px solid #ffffff; 
	background: #ffffff; 
	color: #FE9900;
	padding: 0px; 
	margin: 0px;
}

.ui-button-state-error .ui-icon {
	background-image: url(/library/images/ui-icons_cc0001_256x240.png);
}
.ui-button-state-ok .ui-icon {
	background-image: url(/library/images/ui-icons_009900_256x240.png);
}
.ui-button-state-blue .ui-icon {
	background-image: url(/library/images/ui-icons_000099_256x240.png);
}
.ui-button-state-black .ui-icon {
	background-image: url(/library/images/ui-icons_222222_256x240.png);
}
.ui-button-state-info .ui-icon {
	background-image: url(/library/images/ui-icons_fe9900_256x240.png);
}

.ui-button-state-default .ui-icon {
	background-image: url(/library/images/ui-icons_888888_256x240.png);
}

	
</style>
<?php
//marktime("module wide");
/**********
 * Navigation buttons
 */
if(!isset($no_page_heading) || !$no_page_heading) {
	$active_button_label = $menuObject->drawPageTop($menu,(isset($display_navigation_buttons) ? $display_navigation_buttons : true));
}
 /*
$active_button_label = "";
if(!isset($display_navigation_buttons) || $display_navigation_buttons!==false) {
	foreach($menu['buttons'] as $level => $buttons) {
		//$helper->arrPrint($buttons);
	    echo $helper->generateNavigationButtons($buttons, $level);
		if($level==1) {
			foreach($buttons as $b) {
				if($b['active']==true) {
					$active_button_label = $b['name'];
					break;
				}
			}
		}
	}
}
//marktime("navigation buttons");
/**********
 * Breadcrumbs
 */ /*
$breadcrumbs = array();
    foreach($menu['breadcrumbs'] as $link=>$text) {
    	if(substr_count($link, ".php")>0) {
        	$breadcrumbs[] = "<a href='".$link."' class=breadcrumb>".$text."</a>";
		} else {
        	$breadcrumbs[] = "".$text."";
		}
    }
echo "<h1>".implode(" >> ",$breadcrumbs)."</h1>";
  */
//marktime("end header");
?>

<!------------Hint Box ------------>
<!-- <div id="hint_box" style="padding:8px; width:170px; box-shadow: 2px 2px 5px #333333; background-color:#25bb25; border:1px solid #66cf66; display:none; position:fixed; top:25px; right:10px; z-index:99;">
	<h2 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:normal; color:white">Information</h2>
	<p style="font-family:Verdana, Arial, Helvetica, sans-serif;"></p>
</div> -->
