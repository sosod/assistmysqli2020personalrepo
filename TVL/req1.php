<?php include("inc_ignite.php");

include("inc_tvl.php");

$t = $_POST['t'];
if(!checkIntRef($t))
{
    die("<h1 style=\"color: #cc0001;\">Error</h1><p>An error has occurred.  Please go back and try again.</p>");
}
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
    function Validate(me) {
        var tagent = me.tagent.value;
        if(tagent == "X")
        {
            alert("Please select a travel agent.");
        }
        else
        {
            var tauth = me.tauth.value;
            if(tauth == "X")
            {
                alert("Please select a travel authoriser.");
            }
            else
            {
                var tcount = me.tcount.value;
                var tcount2 = 0;
                var trav;
                var traveller;
                var fld;
                var tb = 1;
                for(tb=1;tb<=tcount;tb++)
                {
                    fld = "t"+tb;
                    trav = document.getElementById(fld);
                    traveller = trav.value;
                    if(traveller != "X")
                    {
                        tcount2++;
                    }
                }
                if(tcount2==0)
                {
                    alert("Please select at least one traveller.");
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request - Step 1</b></h1>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_tvl_list_agent WHERE yn = 'Y'";
include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $a = 0;
    while($row = mysql_fetch_array($rs))
    {
        $agent[$a] = $row;
        $a++;
    }
mysql_close();

if($mnr == 0)
{
    echo("<p>No travel agents have been set up.  Please contact your Travel Assist Administrator.</p>");
}
else
{
        $sql = "SELECT * FROM assist_".$cmpcode."_list_country WHERE id = ".$domctryid;
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
        mysql_close();
            $ctry = $row['value'];

?>
<form name=req1 method=post action=req1_process.php onsubmit="return Validate(this);" language=jscript>
<table border="1" cellspacing="0" cellpadding="5" width=80% style="border-collapse: collapse; border-style: solid;">
	<tr>
		<td class=tdgeneral width=150><b>Requested by:</b></td>
		<td class=tdgeneral><?php echo($tkname); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral width=150><b>Type of travel:</b></td>
		<td class=tdgeneral><select size="1" name="ttype">
        <option selected value=D>Domestic<?php
            if(strlen($ctry)>0)
            {
                echo(" (inside ".$ctry.")");
            }
        ?></option>
        <option value=I>International<?php
            if(strlen($ctry)>0)
            {
                echo(" (outside ".$ctry.")");
            }
        ?></option>
        </select></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Travel Agent:</b></td>
		<td class=tdgeneral>
		<?php
        if($a == 1)
        {
            $sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '".$agent[0]['cmpcode']."'";
            include("inc_db_con_assist.php");
                $row = mysql_fetch_array($rs);
            mysql_close();
            echo($row['cmpname']."<input type=hidden name=tagent value=".$agent[0]['cmpcode'].">");
        }
        else
        {
            ?>
            <select size="1" name="tagent"><option selected value=X>--- SELECT ---</option>
            <?php
            foreach($agent as $ta)
            {
                $sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '".$ta['cmpcode']."'";
                include("inc_db_con_assist.php");
                    $row = mysql_fetch_array($rs);
                mysql_close();
                echo("<option value=".$ta['cmpcode'].">".$row['cmpname']."</option>");
            }
            ?>
            </select>
        <?php
        }
        ?>
        </td>
	</tr>
<?php

if($authtype == "A")
{
    $sql = "SELECT value FROM assist_".$cmpcode."_tvl_policy WHERE questionid = 15";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    if($row['value'] == 27)
    {
?>
	<tr>
		<td class=tdgeneral><b>Travel Authoriser:</b></td>
		<td class=tdgeneral>
            <select size="1" name="tauth"><option selected value=X>--- SELECT ---</option>
            <?php
                $sql = "SELECT t.tkname, t.tksurname, t.tkid FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_tvl_list_authoriser a";
                $sql.= " WHERE t.tkid = a.value AND a.yn = 'Y' AND t.tkstatus = 1";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value='".$row['tkid']."'>".$row['tkname']." ".$row['tksurname']."</option>");
                    }
                mysql_close();
            ?>
            </select>
        </td>
	</tr>
<?php
    }
}
else
{
    echo("<input type=hidden name=tauth value=".$authtype.">");
}
?>
	<tr>
		<td class=tdgeneral rowspan=<?php echo($t); ?> valign=top><b>Traveller(s):</b><input type=hidden name=tcount value=<?php echo($t); ?>></td>
		<td class=tdgeneral><select size="1" id=t1 name="traveller[]"><option selected value=X>--- SELECT ---</option>
        <?php
            $sql = "SELECT t.tkname, t.tksurname, t.tkid FROM assist_".$cmpcode."_timekeep t, ";
            $sql.= "assist_".$cmpcode."_menu_modules_users u WHERE t.tkstatus = 1 AND t.tkid = u.usrtkid AND u.usrmodref = 'TVL' AND t.tkid = '".$tkid."'";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $val = $row['tkname']." ".$row['tksurname'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            mysql_close();
            $sql = "SELECT t.tkname, t.tksurname, t.tkid ";
            $sql.= "FROM assist_".$cmpcode."_timekeep t";
            $sql.= "   , assist_".$cmpcode."_menu_modules_users u";
            $sql.= "   , assist_".$cmpcode."_tvl_profile p";
            $sql.= "   , assist_".$cmpcode."_tvl_profile_proxy x";
            $sql.= " WHERE t.tkstatus <> 0";
            $sql.= "   AND t.tkid = u.usrtkid";
            $sql.= "   AND u.usrmodref = 'TVL'";
            $sql.= "   AND t.tkid = p.tkid";
            $sql.= "   AND p.id = x.profileid";
            $sql.= "   AND x.tkid = '".$tkid."'";
            $sql.= "   AND x.yn = 'Y'";
            $sql.= " ORDER BY t.tkname, t.tksurname";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $val = $row['tkname']." ".$row['tksurname'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            mysql_close();
        ?>
        </select></td>
	</tr>
    <?php
    if($t>0)
    {
        for($t2=2;$t2<=$t;$t2++)
        {
            ?>
	<tr>
		<td class=tdgeneral><select size="1" id=t<?php echo($t); ?> name="traveller[]"><option selected value=X>--- SELECT ---</option>
        <?php
            $sql = "SELECT t.tkname, t.tksurname, t.tkid FROM assist_".$cmpcode."_timekeep t, ";
            $sql.= "assist_".$cmpcode."_menu_modules_users u WHERE t.tkstatus = 1 AND t.tkid = u.usrtkid AND u.usrmodref = 'TVL' AND t.tkid = '".$tkid."'";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $val = $row['tkname']." ".$row['tksurname'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            mysql_close();
            $sql = "SELECT t.tkname, t.tksurname, t.tkid ";
            $sql.= "FROM assist_".$cmpcode."_timekeep t";
            $sql.= "   , assist_".$cmpcode."_menu_modules_users u";
            $sql.= "   , assist_".$cmpcode."_tvl_profile p";
            $sql.= "   , assist_".$cmpcode."_tvl_profile_proxy x";
            $sql.= " WHERE t.tkstatus <> 0";
            $sql.= "   AND t.tkid = u.usrtkid";
            $sql.= "   AND u.usrmodref = 'TVL'";
            $sql.= "   AND t.tkid = p.tkid";
            $sql.= "   AND p.id = x.profileid";
            $sql.= "   AND x.tkid = '".$tkid."'";
            $sql.= "   AND x.yn = 'Y'";
            $sql.= " ORDER BY t.tkname, t.tksurname";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $val = $row['tkname']." ".$row['tksurname'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            mysql_close();
        ?>

        </select></td>
	</tr>
            <?php
        }
    }
    ?>
    <tr>
        <td class=tdgeneral colspan=2><input type=submit value="Step 2 -->"></td>
    </tr>
</table>
</form>
<?php
}

?>

</body>

</html>
