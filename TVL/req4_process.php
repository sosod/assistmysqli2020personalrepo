<?php include("inc_ignite.php");

include("inc_tvl.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request Step 4 - Car Rental Details</b></h1>
<p>Processing... <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1></p>

<?php

$reqid = $_POST['reqid'];


if(strlen($reqid) > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
    include("inc_db_con.php");
        $reqrow = mysql_fetch_array($rs);
    mysql_close();

    if(count($reqrow)>0)
    {
        //GET DETAILS
        $tk = $_POST['tk'];
        $ttkid = $_POST['tkid'];
        $project = $_POST['project'];
        $company = $_POST['companyid'];
        $icity = $_POST['icity'];
        $icountry = $_POST['icountry'];
        $ocity = $_POST['ocity'];
        $ocountry = $_POST['ocountry'];
        $special = $_POST['special'];
        $iday = $_POST['iday'];
        $imon = $_POST['imon'];
        $iyear = $_POST['iyear'];
        $oday = $_POST['oday'];
        $omon = $_POST['omon'];
        $oyear = $_POST['oyear'];
        $typeid = $_POST['typeid'];
        $smoke = $_POST['smoke'];
        $act = $_POST['act'];
        $perc = 15;
        $percinc = 80 / ($tk);
        $percinc = $percinc / 2;
        ?>
        <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
        <?php
        //CHECK DETAILS
        for($t=0;$t<$tk;$t++)
        {
            $tt = $ttkid[$t];
            $p = $project[$t];  
            $c = $company[$t];
            $ici = $icity[$t];
            $icy = $icountry[$t];
            $oci = $ocity[$t];
            $ocy = $ocountry[$t];
            $s = $special[$t];
            $sm = $smoke[$t];
            $id = $iday[$t];
            $im = $imon[$t];
            $iy = $iyear[$t];
            $od = $oday[$t];
            $om = $omon[$t];
            $oy = $oyear[$t];
            $ty = $typeid[$t];
            if(strlen($tt) > 3 && $p!="X" && $c != "X" && strlen($c) > 0 && strlen($ici) > 0 && $icy != "X" && strlen($icy) > 0 && strlen($oci) > 0 && $ocy != "X" && strlen($ocy) > 0 && $id > 0 && $im > 0 && $iy > 1000 && $od > 0 && $om > 0 && $oy > 1000 && $ty != "X" && strlen($ty)> 0)
            {
                //FORMAT
                $s = htmlentities($s,ENT_QUOTES,"ISO-8859-1");
                $ici = htmlentities($ici,ENT_QUOTES,"ISO-8859-1");
                $oci = htmlentities($oci,ENT_QUOTES,"ISO-8859-1");
                $indate = mktime(12,0,0,$im,$id,$iy);
                $outdate = mktime(12,0,0,$om,$od,$oy);
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
                //INSERT
                $sql = "INSERT INTO assist_".$cmpcode."_tvl_req_car SET ";
                $sql.= "  reqid = ".$reqid;
                $sql.= ", tkid = '".$tt."'";
                $sql.= ", companyid = ".$c;
                $sql.= ", typeid = ".$ty;
                $sql.= ", pickdate = ".$indate;
                $sql.= ", pickcity = '".$ici."'";
                $sql.= ", pickcountry = ".$icy;
                $sql.= ", dropdate = ".$outdate;
                $sql.= ", dropcity = '".$oci."'";
                $sql.= ", dropcountry = ".$ocy;
                $sql.= ", special = '".$s."'";
                $sql.= ", smoking = '".$sm."'";
                $sql.= ", yn = 'Y'";
                $sql.= ", projectid = ".$p;
                $sql.= "  , status = 'P'";
                $sql.= "  , authtkid = ''";
                $sql.= "  , authdate = 0";
                include("inc_db_con.php");
//                echo("<p>".$sql);
                    $tref = "TVL";
                    $trans = "Added car rental record for travel request ".$reqid;
                    $tsql = $sql;
                    include("inc_transaction_log.php");
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
            else
            {
                $perc = $perc + $percinc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
        }
        //MOVE ON
                ?>
                <script language=JavaScript> lbl.innerText="100%"; </script>
                <?php
        if($act=="S")
        {
            if($reqrow['type'] == "D")
            {
                echo("<script language=JavaScript> document.location.href = \"req6.php?r=".$reqid."\"; </script>");
            }
            else
            {
                //CHECK POLICY ON FOREX
                $sql = "SELECT value FROM assist_".$cmpcode."_tvl_policy WHERE questionid = 40 AND yn = 'Y'";
                include("inc_db_con.php");
                    if(mysql_num_rows($rs)==0)
                    {
                        $forex = "Y";
                    }
                    else
                    {
                        $row = mysql_fetch_array($rs);
                        $forex = $row['value'];
                    }
                mysql_close();
                if($forex == "N")
                {
                    echo("<script language=JavaScript> document.location.href = \"req6.php?r=".$reqid."\"; </script>");
                }
                else
                {
                    echo("<script language=JavaScript> document.location.href = \"req5.php?r=".$reqid."\"; </script>");
                }
            }
        }
        else
        {
            echo("<script language=JavaScript> document.location.href = \"req4.php?r=".$reqid."\"; </script>");
        }
    }
    else
    {
        echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL4b).  Please go back and try again.</p>");
    }
}
else    //if strlen reqid > 0
{
    echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL4a).  Please go back and try again.</p>");
}

?>
</body>

</html>
