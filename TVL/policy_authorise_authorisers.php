<?php
include("inc_ignite.php");

include("inc_tvl.php");
$result = "<p>&nbsp;</p>";

$t = $_GET['t'];

    switch($t)
    {
        case "new":
            $title = "New Travel Authorisers";
            $sqlid = "AND field1 = 0 ";
            $tword = "new travel authorisers";
            break;
        case "upd":
            $title = "Updated Travel Authorisers";
            $sqlid = "AND field1 <> 0 ";
            $tword = "updates travel authorisers";
            break;
        default:
            $t = "new";
            $title = "New Travel Authorisers";
            $sqlid = "AND field1 = 0 ";
            $tword = "new list items";
            break;
    }

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var subBut = me.act.value;
    var chkcount = 0;
	for (var i=0;i<me.elements.length;i++)
	{
		var e=me.elements[i];
		if ((e.name != 'all') && (e.type=='checkbox') && (e.name!='notify'))
		{
            if(e.checked==true)
            {
                chkcount++;
            }
		}
	}
	if(chkcount>0)
	{
        if(confirm("Are you sure you with to "+subBut+" the "+chkcount+" item(s) you have ticked?")==true)
        {
            return true;
        }
    }
    else
    {
        alert("Please indicate which items you wish to "+subBut+" by ticking their checkboxes.");
    }
    return false;
}

function subMit(val) {
    document.aform.act.value = val;
    switch(val)
    {
        case "approve":
            var target2 = document.getElementById( 'but2' );
            target2.style.display = "none";
            var target1 = document.getElementById( 'but1' );
            target1.style.display = "inline";
            break;
        case "reject":
            var target2 = document.getElementById( 'but2' );
            target2.style.display = "inline";
            var target1 = document.getElementById( 'but1' );
            target1.style.display = "none";
            break;
    }
}

function checkAll(){
	for (var i=0;i<document.forms[0].elements.length;i++)
	{
		var e=document.forms[0].elements[i];
		if ((e.name != 'all') && (e.type=='checkbox') && (e.name!='notify'))
		{
			e.checked=document.forms[0].all.checked;
		}
	}
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Policy - Authorise Lists / <?php echo($title); ?></b></h1>
<?php echo($result); ?>
<form name=aform action=policy_authorise_authorisers_process.php method=post onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=t value=<?php echo($t); ?>>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdheader">Ref</td>
		<td class="tdheader">Added by</td>
		<td class="tdheader">Date Added</td>
		<td class="tdheader">Authoriser Added</td>
		<td class="tdheader">Department</td>
		<td class="tdheader"><input type=checkbox value=Y name=all onclick=checkAll()></td>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT intid, intuser, intdate, field2 value, field3 yn, field4 type, field5 loc, field6 account, tkname, tksurname FROM assist_".$cmpcode."_tvl_policy_interim, assist_".$cmpcode."_timekeep ";
    $sql .= "WHERE intstatus = 'P' ";
    $sql .= "AND inttable = 'tvl_list_authoriser' ";
    $sql .= "AND intuser = tkid ";
//    $sql .= "AND field4 = 'tu' ";
    $sql .= $sqlid;
    $sql .= "ORDER BY field4, intdate";
    include("inc_db_con.php");
if(mysql_num_rows($rs)>0)
{
    while($row = mysql_fetch_array($rs))
    {
        $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['value']."'";
        include("inc_db_con2.php");
            $tkrow = mysql_fetch_array($rs2);
        mysql_close($con2);
        $sql2 = "SELECT value FROM assist_".$cmpcode."_list_dept WHERE id = ".$row['loc'];
        include("inc_db_con2.php");
            $drow = mysql_fetch_array($rs2);
        mysql_close($con2);
?>
        <tr>
            <td class="tdgeneral"><?php echo($row['intid']); ?></td>
            <td class="tdgeneral"><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td class="tdgeneral"><?php echo(date("d-M-Y",$row['intdate'])); ?></td>
            <td class="tdgeneral"><B><?php echo($tkrow['tkname']." ".$tkrow['tksurname']); ?></b></td>
            <td class="tdgeneral"><?php echo($drow['value']); ?></td>
            <td class="tdheader"><input type=checkbox value=Y name=ref<?php echo($row['intid']);?>></td>
        </tr>
<?php
    }
}
else
{
    echo("<tr class=tdgeneral><td colspan=7>No ".$tword." awaiting approval.</td></tr>");
}
    mysql_close();
?>
<tr class=tdgeneral>
    <td colspan=7><input type=hidden name=act value=approve>
        <span id=but1><input type=submit name=approve value="Approve checked items"> <input type=button name=subbut value="Reject checked items" onclick=subMit('reject')></span>
        <span id=but2><input type=submit name=reject value="Submit rejection"> <input type=button name=subbut value="Cancel" onclick=subMit('approve')><br>
        Please indicate the reason for your rejection<Br>and click the "Submit rejection" button above:<br>
        <textarea name=rejreason cols=40 rows=5></textarea><br>
        <input type=checkbox name=notify value=Y> Notify Policy Administrators?</span>
    </td>
</tr>
</table>
</form>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=policy.php class=grey>Go back</a></p>
<script language=JavaScript>
    var target = document.getElementById( 'but2' );
    target.style.display = "none";

</script>
</body>

</html>
