<?php include("inc_ignite.php");

include("inc_tvl.php");

 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Policy - Allocation of Benefits</b></h1>
<form name=aob action=policy_setup_aob_process.php method=post>
<table border=1 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border-style: solid;">
<?php
$tr = 0;

$sql = "SELECT * FROM assist_list_orglevels WHERE yn = 'Y' ORDER BY sort DESC";
include("inc_db_con.php");
$o = 0;
    while($row = mysql_fetch_array($rs))
    {
        $o++;
        $org[$o] = $row;
    }
mysql_close();



?>
<tr class=tdheader>
    <td class=tdgeneral>&nbsp;</td>
    <?php
    foreach($org as $o)
    {
        $val = str_replace(" ","<br>",$o['value']);
        echo("<td class=tdgeneral><a title=\"".$o['comment']."\" style=\"text-decoration: underline; color: #FFFFFF\">".$val."</a></td>");
    }
    ?>
</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_tvl_policy_qindex WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
$c = 0;
    while($row = mysql_fetch_array($rs))
    {
        $c++;
        $cate[$c] = $row;
    }
mysql_close();

foreach($cate as $c)
{
    $sql = "SELECT q.id qid";
    $sql.= ", q.value qvalue";
    $sql.= ", q.type qtype";
    $sql.= ", p.value";
    $sql.= " FROM assist_".$cmpcode."_tvl_policy_questions q";
    $sql.= ", assist_".$cmpcode."_tvl_policy p";
    $sql.= " WHERE q.cateid = ".$c['id']." AND q.aob = 'Y' AND p.yn = 'Y' ";
    $sql.= "AND q.id = p.questionid ORDER BY q.sort";
    include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
        ?>
        <tr class=tdgeneral>
            <td colspan=8><h2 class=fc style="margin-top:2px; margin-bottom: 2px"><?php echo($c['value']); ?></h2></td>
        </tr>
        <?php
        while($row = mysql_fetch_array($rs))
        {
            ?>
            <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                <td class=tdgeneral><?php echo($row['qvalue']); ?></td>
                <?php
                foreach($org as $o)
                {
                    if($row['value']=="Y")
                    {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'Q' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value'] == "N")
                        {
                            ?>
                            <td class=tdgeneral align=center><select name=yn_q_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                            <?php
                        }
                        else
                        {
                            ?>
                            <td class=tdgeneral align=center><select name=yn_q_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                            <?php
                        }
                    }
                    else
                    {
                    ?>
                    <td class=tdgeneral align=center>No<input type=hidden name=yn_q_<?php echo($row['qid']); ?>[] value=N></td>
                    <?php
                    }
                }
                ?>
            </tr>
            <?php
            $tr++;
        }
        ?>
        <?php
    }
    mysql_close();
    $list = $c['list'];
    if(strlen($list)>0)
    {
        switch($list)
        {
            case "c":
                //Airline domestic - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Car Rental Companies allowed for domestic travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'cp' ";
                $sql .= "AND (a.loc = 'D' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LD' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_ld_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Airline domestic - Pending
                //Airline international - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Car Rental Companies allowed for international travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'cp' ";
                $sql .= "AND (a.loc = 'I' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LI' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_li_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Airline international - Pending
                //Class domestic - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Car types allowed for domestic travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'ct' ";
                $sql .= "AND (a.loc = 'D' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LD' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_ld_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Class domestic - Pending
                //Class international - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Car types allowed for international travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'ct' ";
                $sql .= "AND (a.loc = 'I' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LI' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_li_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Class international - Pending
                break;
            case "f":
                //Airline domestic - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Airlines allowed for domestic travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'fp' ";
                $sql .= "AND (a.loc = 'D' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LD' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_ld_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Airline domestic - Pending
                //Airline international - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Airlines allowed for international travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'fp' ";
                $sql .= "AND (a.loc = 'I' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LI' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_li_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Airline international - Pending
                //Class domestic - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Flight classes allowed for domestic travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'ft' ";
                $sql .= "AND (a.loc = 'D' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LD' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_ld_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Class domestic - Pending
                //Class international - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Flight classes allowed for international travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'ft' ";
                $sql .= "AND (a.loc = 'I' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LI' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_li_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Class international - Pending
                break;
            case "h":
                //Airline domestic - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Hotels allowed for domestic travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'hp' ";
                $sql .= "AND (a.loc = 'D' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LD' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_ld_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Airline domestic - Pending
                //Airline international - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Hotels allowed for international travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'hp' ";
                $sql .= "AND (a.loc = 'I' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LI' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_li_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Airline international - Pending
                //Class domestic - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Room types allowed for domestic travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'ht' ";
                $sql .= "AND (a.loc = 'D' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LD' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_ld_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_ld_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Class domestic - Pending
                //Class international - Authorised
                        ?>
                        <tr class=tdgeneral>
                            <td class=tdgeneral>Room types allowed for international travel:</td>
                            <?php
                           foreach($org as $o)
                            {
                                ?>
                                <td class=tdgeneral align=center>&nbsp;</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                $sql = "SELECT a.id qid, a.* FROM assist_".$cmpcode."_tvl_lists a ";
                $sql .= "WHERE a.yn = 'Y' ";
                $sql .= "AND a.type = 'ht' ";
                $sql .= "AND (a.loc = 'I' OR a.loc = 'B') ";
                $sql .= "ORDER BY a.value";
                include("inc_db_con.php");
                    $mnr = mysql_num_rows($rs);
                    while($row = mysql_fetch_array($rs))
                    {
                        ?>
                        <tr id=tr<?php echo($tr); ?> onmouseover="hovCSS('tr<?php echo($tr); ?>');"  onmouseout="hovCSS2('tr<?php echo($tr); ?>');">
                            <td class=tdgeneral><i>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo($row['value']); ?></i></td>
                            <?php
                            foreach($org as $o)
                            {
                                if($mnr > 1)
                                {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_aob WHERE yn = 'Y' AND type = 'LI' AND recordid = ".$row['qid']." AND orgid = ".$o['id'];
                        include("inc_db_con2.php");
                            $orow = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        if(strlen($orow['value'])==0 || $orow['value']=="N")
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option selected value=N>No</option><option value=Y>Yes</option></select></td>
                                    <?php
                        }
                        else
                        {
                                    ?>
                                    <td class=tdgeneral align=center><select name=yn_li_<?php echo($row['qid']); ?>[]><option value=N>No</option><option selected value=Y>Yes</option></select></td>
                                    <?php
                        }
                                }
                                else
                                {
                                    ?>
                                    <td class=tdgeneral align=center><input type=hidden name=yn_li_<?php echo($row['qid']); ?>[] value=Y>Yes</td>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $tr++;
                    }
                mysql_close();
                //Class international - Pending
                break;
        }
    }
}
?>
<tr class=tdgeneral>
    <td colspan=8><input type=submit value=Submit style="padding-left: 3px; padding-right: 2px"> <input type=reset value=Reset style="padding-left: 3px; padding-right: 2px;"></td>
</tr>

</table>
</form>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=policy.php class=grey>Go back</a></p>
</body>

</html>
