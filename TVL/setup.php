<?php
include("inc_ignite.php");

//GET SETUP ADMIN TKID
$ta0 = $_POST['O'];
//IF SETUP ADMIN TKID WAS SENT THEN PERFORM UPDATE
if(strlen($ta0) > 0)
{
    //GET CURRENT SETUP ADMIN TKID
    $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'TVL' AND refid = 0";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ta0z = $row['value'];
    mysql_close();
    $sql = "";
    //UPDATE ASSIST-SETUP TABLE
    $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0."' WHERE ref = 'TVL' AND refid = 0";
    include("inc_db_con.php");
        //Set transaction log values for this update
        $trans = "Updated Setup Administrator to ".$ta0;
    $sql = "";

    //PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
        $tsql = $sql;
        $told = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0z."' WHERE ref = 'TVL' AND refid = 0";
        $tref = 'TVL';
        include("inc_transaction_log.php");

    $result = "<p><i>Administrator update complete.</i></p>";
}
else
{
    $result = "<p>&nbsp;</p>";
}

include("inc_tvl.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Travel Assist: Setup</b></h1>
<?php echo($result); ?>
<ul>
<form name=update method=post action=setup.php>
<li>Setup Administrator: <select name=O>
<?php
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($setupadmn == "0000")
{
?>
    <option selected value=0000>Ignite Assist Administrator</option>
<?php
}
else
{
?>
    <option value=0000>Ignite Assist Administrator</option>
<?php
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = 'TVL' ";
$sql .= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tid = $row['tkid'];
    if($tid == $setupadmn)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
mysql_close();

?>
</select> <input type=submit value=Update style="padding-left: 1px"><br>&nbsp;</li>

<li>Domestic country: <select name=domctryid>
            <?php
                    echo("<option value=X>--- SELECT ---</option>");
                $sql = "SELECT * FROM assist_".$cmpcode."_list_country ORDER BY value";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $id = $row['id'];
                        $val = $row['value'];
                        echo("<option ");
                        if($id == $domctryid)
                        {
                            echo("selected ");
                        }
                        echo("value=".$id.">".$val."</option>");
                    }
                mysql_close();
            ?>
</select> <input type=button value=Update style="padding-left: 1px"><br>&nbsp;</li>

<li><a href=setup_policyadmin.php>Policy Administrators</a><br><i>These are the people responsible for updating and authorising the Travel Policy.</i><br>&nbsp;</li>
<li><a href=setup_projectadmin.php>Project Administrators</a><br><i>These are the people responsible for maintaining the projects under which travel is allowed.</i><br>&nbsp;</li>
<li><a href=setup_moduleadmin.php>Module Administrators</a><br><i>These are the people who can override Travel Authorisers.<br>&nbsp;</li>

</ul>
<?php
$helpfile = "../help/TVL_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</form>
</body>

</html>
