<?php include("inc_ignite.php");

include("inc_tvl.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request Step 3 - Accommodation Details</b></h1>
<p>Processing... <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1></p>

<?php

$reqid = $_POST['reqid'];


if(strlen($reqid) > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
    include("inc_db_con.php");
        $reqrow = mysql_fetch_array($rs);
    mysql_close();

    if(count($reqrow)>0)
    {
        //GET DETAILS
        $tk = $_POST['tk'];
        $ttkid = $_POST['tkid'];
        $project = $_POST['project'];
        $hotel = $_POST['hotel'];
        $hcity = $_POST['hcity'];
        $hcountry = $_POST['hcountry'];
        $special = $_POST['special'];
        $iday = $_POST['iday'];
        $imon = $_POST['imon'];
        $iyear = $_POST['iyear'];
        $oday = $_POST['oday'];
        $omon = $_POST['omon'];
        $oyear = $_POST['oyear'];
        $room = $_POST['room'];
        $smoke = $_POST['smoke'];
        $act = $_POST['act'];
        $perc = 15;
        $percinc = 80 / ($tk);
        $percinc = $percinc / 2;
        ?>
        <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
        <?php
        //CHECK DETAILS
        for($t=0;$t<$tk;$t++)
        {
            $tt = $ttkid[$t];
            $p = $project[$t];  
            $h = $hotel[$t];
            $hci = $hcity[$t];
            $hcy = $hcountry[$t];
            $s = $special[$t];
            $sm = $smoke[$t];
            $id = $iday[$t];
            $im = $imon[$t];
            $iy = $iyear[$t];
            $od = $oday[$t];
            $om = $omon[$t];
            $oy = $oyear[$t];
            $rm = $room[$t];
            if(strlen($tt) > 3 && $p!="X" && $h != "X" && strlen($h) > 0 && strlen($hci) > 0 && $hcy != "X" && strlen($hcy) > 0 && $id > 0 && $im > 0 && $iy > 1000 && $od > 0 && $om > 0 && $oy > 1000 && $rm != "X" && strlen($rm)> 0)
            {
                //FORMAT
                $s = htmlentities($s,ENT_QUOTES,"ISO-8859-1");
                $hci = htmlentities($hci,ENT_QUOTES,"ISO-8859-1");
                $indate = mktime(12,0,0,$im,$id,$iy);
                $outdate = mktime(12,0,0,$om,$od,$oy);
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
                //INSERT
                $sql = "INSERT INTO assist_".$cmpcode."_tvl_req_acc SET ";
                $sql.= "  reqid = ".$reqid;
                $sql.= ", tkid = '".$tt."'";
                $sql.= ", hotelid = ".$h;
                $sql.= ", roomid = ".$rm;
                $sql.= ", indate = ".$indate;
                $sql.= ", outdate = ".$outdate;
                $sql.= ", city = '".$hci."'";
                $sql.= ", countryid = ".$hcy;
                $sql.= ", special = '".$s."'";
                $sql.= ", smoking = '".$sm."'";
                $sql.= ", yn = 'Y'";
                $sql.= ", projectid = ".$p;
                $sql.= "  , status = 'P'";
                $sql.= "  , authtkid = ''";
                $sql.= "  , authdate = 0";
                include("inc_db_con.php");
//                echo("<p>".$sql);
                    $tref = "TVL";
                    $trans = "Added accommodation record for travel request ".$reqid;
                    $tsql = $sql;
                    include("inc_transaction_log.php");
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
            else
            {
                $perc = $perc + $percinc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
        }
        //MOVE ON
                ?>
                <script language=JavaScript> lbl.innerText="100%"; </script>
                <?php
        if($act=="S")
        {
            echo("<script language=JavaScript> document.location.href = \"req4.php?r=".$reqid."\"; </script>");
        }
        else
        {
            echo("<script language=JavaScript> document.location.href = \"req3.php?r=".$reqid."\"; </script>");
        }
    }
    else
    {
        echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL3b).  Please go back and try again.</p>");
    }
}
else    //if strlen reqid > 0
{
    echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL3a).  Please go back and try again.</p>");
}

?>
</body>

</html>
