<?php include("inc_ignite.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
s = 1;
function nextAuth() {
    var tbutt = "b"+s;
    s++;
    var target = document.getElementById(s);
    target.style.display = "block";
    var target = document.getElementById(tbutt);
    target.style.display = "none";
}

function Validate(me) {
    var pcode = me.pcode.value;
    var pvalue = me.pvalue.value;
    var valid8 = "true";
    var err = "Please provide the following information:";
    var fldgo = "";
    
    if(pcode.length == 0)
    {
        valid8 = "false";
        fldgo = "pcode";
        err = err + "\n - Project code;";
        document.getElementById('pcode').className = 'fldreq';
    }
    else
    {
        document.getElementById('pcode').className = 'fldgot';
    }
    if(pvalue.length == 0)
    {
        valid8 = "false";
        if(fldgo.length == 0)
        {
            fldgo = "pvalue";
        }
        err = err + "\n - Project name;";
        document.getElementById('pvalue').className = 'fldreq';
    }
    else
    {
        document.getElementById('pvalue').className = 'fldgot';
    }

    if(s>1)
    {
        var s2 = 1;
        var serr = "Y";
        s++;
        var pauth = "";
        var pfld = "";
        for(s2=1;s2<s;s2++)
        {
            pfld = "pa"+s2;
            pauth = document.getElementById(pfld).value;
            if(pauth.length != 0 && pauth != "X")
            {
                serr = "N";
            }
        }
        if(serr == "Y")
        {
            valid8 = "false";
            if(fldgo.length == 0)
            {
                fldgo = "pa1";
            }
            err = err + "\n - At least 1 Project Authoriser;";
            document.getElementById('pa1').className = 'fldreq';
        }
        s--;
    }
    else
    {
        var pauth = document.getElementById('pa1').value;
        if(pauth.length == 0 || pauth == "X")
        {
            valid8 = "false";
            if(fldgo.length == 0)
            {
                fldgo = "pa1";
            }
            err = err + "\n - At least 1 Project Authoriser;";
            document.getElementById('pa1').className = 'fldreq';
        }
    }

    if(valid8=="false")
    {
        alert(err);
        document.getElementById(fldgo).focus();
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

function editProject(i) {
    if(i>0)
    {
        document.location.href = "projects_edit.php?i="+i;
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist - Projects</b></h1>
                    <?php
                        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
                        $sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
                        $sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = 'TVL' ";
                        $sql .= "ORDER BY t.tkname, t.tksurname";
                        include("inc_db_con.php");
                            $ti = 0;
                            $tkusers = array();
                            while($row = mysql_fetch_array($rs))
                            {
                                $id = $row['tkid'];
                                $val = $row['tkname']." ".$row['tksurname'];
                                $tkauth[$id] = $val;
                                $tkusers[$ti]['id'] = $id;
                                $tkusers[$ti]['val'] = $val;
                                $ti++;
                            }
                        mysql_close();
                    ?>
<form name=projadd method=post action=projects_process.php onsubmit="return Validate(this);" language=jscript>
<table border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border-style: solid">
    <tr class=tdheader>
        <td>&nbsp;</td>
        <td>Project Code</td>
        <td>Project Name</td>
        <td>Authoriser(s)</td>
        <td>&nbsp;</td>
    </tr>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_projects WHERE yn = 'Y' AND adduser = '".$tkid."' ORDER BY value";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            include("inc_tr.php");
            ?>
                <td class=tdgeneral valign=top><?php echo($tr); ?>.</td>
                <td class=tdgeneral valign=top><?php echo($row['code']); ?></td>
                <td class=tdgeneral valign=top><?php echo($row['value']); ?></td>
                <td class=tdgeneral valign=top><ul style="margin-top: 0px; margin-bottom: 0px;">
                <?php
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_projects_authorisers WHERE projectid = ".$row['id']." AND yn = 'Y'";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            if(strlen($tkauth[$row2['tkid']])>0)
                            {
                                ?>
                                <li><?php echo($tkauth[$row2['tkid']]); ?></li>
                                <?php
                            }
                        }
                    mysql_close($con2);
                ?>
                </ul></td>
                <td class=tdgeneral valign=top><input type=button value=Edit onclick="editProject(<?php echo($row['id']); ?>)"></td>
            </tr>
            <?php
        }
    mysql_close();
    ?>
    
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral valign=top>&nbsp;</td>
        <td class=tdgeneral valign=top><input type=text maxlength=10 size=15 name=pcode id=pcode></td>
        <td class=tdgeneral valign=top><input type=text maxlength=225 size=50 name=pvalue id=pvalue></td>
        <td class=tdgeneral valign=top>
            <table border="0" cellspacing="0" cellpadding="2">
                <tr class=tdgeneral id=1>
                    <td align=right>&nbsp;&nbsp;1.</td>
                    <td><select name=pauth[] id=pa1><option value=X selected>--- SELECT ---</option>
                    <?php
                        for($i=0;$i<$ti;$i++)
                        {
                            echo("<option value=\"".$tkusers[$i]['id']."\">".$tkusers[$i]['val']."</option>");
                        }
                    ?>
                    </select></td>
                    <td><span id=b1><input type=button class=tvlbutton value=" + " onclick=nextAuth();></span></td>
                </tr>
            <?php
            $s = 2;
            $ts = 11;
            if($ti<11) { $ts = $ti+1; }
            for($s=2;$s<$ts;$s++)
            {
                ?>
                <tr class=tdgeneral id=<?php echo($s); ?>>
                    <td align=right><?php
                    echo($s); ?>.</td>
                    <td><select name=pauth[] id=pa<?php echo($s); ?>><option value=X selected>--- SELECT ---</option>
                    <?php
                        for($i=0;$i<$ti;$i++)
                        {
                            echo("<option value=\"".$tkusers[$i]['id']."\">".$tkusers[$i]['val']."</option>");
                        }
                    ?>
                    </select></td>
                    <td><?php if($s<$ti) { ?><span id=b<?php echo($s); ?>><input type=button name=abutt class=tvlbutton value=" + " onclick=nextAuth();></span><?php } else { echo("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"); } ?></td>
                </tr>
                <?php
            }
            ?>
            </table>
        </td>
        <td class=tdgeneral valign=top><input type=submit value=Add><input type=hidden name=ts id=ts value=<?php echo($ts); ?>></td>
    </tr>
</table>
<script language=JavaScript>
    var t = 2;
    var ts = document.getElementById('ts').value;
    for(t=2;t<ts;t++)
    {
        var targ = document.getElementById(t);
        targ.style.display = "none";
    }
</script>
</form>
</body>

</html>
