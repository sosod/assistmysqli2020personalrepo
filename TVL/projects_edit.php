<?php include("inc_ignite.php");

$pid = $_GET['i'];
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
s = 1;
function nextAuth() {
    var tbutt = "b"+s;
    s++;
    var target = document.getElementById(s);
    target.style.display = "block";
    var target = document.getElementById(tbutt);
    target.style.display = "none";
}

function Validate(me) {
    var pcode = me.pcode.value;
    var pvalue = me.pvalue.value;
    var valid8 = "true";
    var err = "Please provide the following information:";
    var fldgo = "";
    var t = me.t.value;
    
    if(pcode.length == 0)
    {
        valid8 = "false";
        fldgo = "pcode";
        err = err + "\n - Project code;";
        document.getElementById('pcode').className = 'fldreq';
    }
    else
    {
        document.getElementById('pcode').className = 'fldgot';
    }
    if(pvalue.length == 0)
    {
        valid8 = "false";
        if(fldgo.length == 0)
        {
            fldgo = "pvalue";
        }
        err = err + "\n - Project name;";
        document.getElementById('pvalue').className = 'fldreq';
    }
    else
    {
        document.getElementById('pvalue').className = 'fldgot';
    }

    var terr = "Y";
    var ta;
    var tauth;
    for(tb=1;tb<t;tb++)
    {
        ta = "t"+tb;
        tauth = document.getElementById(ta);
        if(!tauth.checked)
        {
            terr = "N";
        }
    }
    if(terr == "Y")
    {
        if(s>1)
        {
            var s2 = 1;
            var serr = "Y";
            s++;
            var pauth = "";
            var pfld = "";
            for(s2=1;s2<s;s2++)
            {
                pfld = "pa"+s2;
                pauth = document.getElementById(pfld).value;
                if(pauth.length != 0 && pauth != "X")
                {
                    serr = "N";
                }
            }
            if(serr == "Y")
            {
                valid8 = "false";
                if(fldgo.length == 0)
                {
                    fldgo = "pa1";
                }
                err = err + "\n - At least 1 Project Authoriser;";
                document.getElementById('pa1').className = 'fldreq';
            }
            else
            {
                document.getElementById('pa1').className = 'fldgot';
            }
            s--;
        }
        else
        {
            var pauth = document.getElementById('pa1').value;
            if(pauth.length == 0 || pauth == "X")
            {
                valid8 = "false";
                if(fldgo.length == 0)
                {
                    fldgo = "pa1";
                }
                err = err + "\n - At least 1 Project Authoriser;";
                document.getElementById('pa1').className = 'fldreq';
            }
            else
            {
                document.getElementById('pa1').className = 'fldgot';
            }
        }
    }

    if(valid8=="false")
    {
        alert(err);
        document.getElementById(fldgo).focus();
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

function delProject(d) {
    if(d > 0)
    {
        if(confirm("Are you sure you wish to delete this project?")==true)
        {
            document.location.href = "projects_delete_process.php?d="+d;
        }
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist - Projects</b></h1>
<form name=projadd method=post action=projects_edit_process.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=projectid value=<?php echo($pid); ?>>
<table border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border-style: solid">
    <tr class=tdheader>
        <td>Project Code</td>
        <td>Project Name</td>
        <td>Authoriser(s)</td>
    </tr>
    <tr>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_tvl_projects WHERE id = ".$pid;
        include("inc_db_con.php");
            $project = mysql_fetch_array($rs);
        mysql_close();
        ?>
        <td class=tdgeneral valign=top><input type=text maxlength=10 size=15 name=pcode id=pcode value="<?php echo($project['code']); ?>"></td>
        <td class=tdgeneral valign=top><input type=text maxlength=225 size=50 name=pvalue id=pvalue value="<?php echo($project['value']); ?>"></td>
        <td class=tdgeneral valign=top>
            <table border="0" cellspacing="0" cellpadding="2">
                    <?php
                    $t=1;
                        $sql2 = "SELECT t.tkid, t.tksurname, t.tkname ";
                        $sql2.= "FROM assist_".$cmpcode."_tvl_projects_authorisers a, assist_".$cmpcode."_timekeep t ";
                        $sql2.= "WHERE a.projectid = ".$pid." AND a.yn = 'Y' AND t.tkstatus = 1 AND a.tkid = t.tkid ORDER BY t.tkname, t.tksurname";
                        include("inc_db_con2.php");
                            while($row2 = mysql_fetch_array($rs2))
                            {
                                    ?>
                <?php include("inc_tr.php"); ?>
                    <td class=tdgeneral align=right>&nbsp;&nbsp;<?php echo($t); ?>.</td>
                    <td class=tdgeneral><?php echo($row2['tkname']." ".$row2['tksurname']); ?></td>
                    <td class=tdgeneral valign=middle><input type=checkbox value="<?php echo($row2['tkid']); ?>" name=tauth[] id=t<?php echo($t); ?>> <label for=x id=y class=fc><i><small>Remove?</small></i></label></td>
                </tr>
                                    <?php
                                    $t++;
                            }
                        mysql_close($con2);
                    ?><input type=hidden name=t value=<?php echo($t); ?>>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
        $sql.= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
        $sql.= "AND t.tkid = m.usrtkid AND m.usrmodref = 'TVL' ";
        $sql.= "AND t.tkid NOT IN (SELECT tkid FROM assist_".$cmpcode."_tvl_projects_authorisers a WHERE a.projectid = ".$pid." AND a.yn = 'Y') ";
        $sql.= "ORDER BY t.tkname, t.tksurname";
        include("inc_db_con.php");
            $ti = 0;
            $tkusers = array();
            while($row = mysql_fetch_array($rs))
            {
                $id = $row['tkid'];
                $val = $row['tkname']." ".$row['tksurname'];
                $tkusers[$ti]['id'] = $id;
                $tkusers[$ti]['val'] = $val;
                $ti++;
            }
        mysql_close();
        $ts = 0;
        if($ti > 0)
        {
        ?>
                <tr class=tdgeneral id=1>
                    <td align=right>&nbsp;&nbsp;<?php echo($t); ?>.</td>
                    <td><select name=pauth[] id=pa1><option value=X selected>--- SELECT ---</option>
                    <?php
                        for($i=0;$i<$ti;$i++)
                        {
                            echo("<option value=\"".$tkusers[$i]['id']."\">".$tkusers[$i]['val']."</option>");
                        }
                    ?>
                    </select></td>
                    <td><span id=b1><input type=button class=tvlbutton value=" + " onclick=nextAuth();></span></td>
                </tr>
            <?php
            $t++;
            $s = 2;
            $ts = 11;
            if($ti<11) { $ts = $ti+1; }
            for($s=2;$s<$ts;$s++)
            {
                ?>
                <tr class=tdgeneral id=<?php echo($s); ?>>
                    <td align=right><?php
                    echo($t); ?>.</td>
                    <td><select name=pauth[] id=pa<?php echo($s); ?>><option value=X selected>--- SELECT ---</option>
                    <?php
                        for($i=0;$i<$ti;$i++)
                        {
                            echo("<option value=\"".$tkusers[$i]['id']."\">".$tkusers[$i]['val']."</option>");
                        }
                    ?>
                    </select></td>
                    <td><?php if($s<$ti) { ?><span id=b<?php echo($s); ?>><input type=button name=abutt class=tvlbutton value=" + " onclick=nextAuth();></span><?php } else { echo("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"); } ?></td>
                </tr>
                <?php
                $t++;
            }
        } //if rows
        mysql_close();
            ?>
            </table>
        </td>
    </tr>
    <tr class=tdgeneral>
        <td class=tdgeneral colspan=3 valign=top><input type=submit value=Update> <input type=button value=Delete onclick="delProject(<?php echo($pid); ?>)"><input type=hidden name=ts id=ts value=<?php echo($ts); ?>></td>
    </tr>
</table>
<script language=JavaScript>
    var t = 2;
    var ts = document.getElementById('ts').value;
    for(t=2;t<ts;t++)
    {
        var targ = document.getElementById(t);
        targ.style.display = "none";
    }
</script>
</form>
</body>

</html>
