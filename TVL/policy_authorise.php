<?php include("inc_ignite.php");

include("inc_tvl.php");

 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var subBut = me.act.value;
    var chkcount = 0;
	for (var i=0;i<me.elements.length;i++)
	{
		var e=me.elements[i];
		if ((e.name != 'all') && (e.type=='checkbox') && (e.name!='notify'))
		{
            if(e.checked==true)
            {
                chkcount++;
            }
		}
	}
	if(chkcount>0)
	{
        if(confirm("Are you sure you with to "+subBut+" the "+chkcount+" item(s) you have ticked?")==true)
        {
            return true;
        }
    }
    else
    {
        alert("Please indicate which items you wish to "+subBut+" by ticking their checkboxes.");
    }
    return false;
}

function subMit(val) {
    document.aform.act.value = val;
    switch(val)
    {
        case "approve":
            var target2 = document.getElementById( 'but2' );
            target2.style.display = "none";
            var target1 = document.getElementById( 'but1' );
            target1.style.display = "inline";
            break;
        case "reject":
            var target2 = document.getElementById( 'but2' );
            target2.style.display = "inline";
            var target1 = document.getElementById( 'but1' );
            target1.style.display = "none";
            break;
    }
}

function checkAll(){
	for (var i=0;i<document.forms[0].elements.length;i++)
	{
		var e=document.forms[0].elements[i];
		if ((e.name != 'all') && (e.type=='checkbox') && (e.name!='notify'))
		{
			e.checked=document.forms[0].all.checked;
		}
	}
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Policy - Authorise</b></h1>
<form name=aform action=policy_authorise_process.php method=post onsubmit="return Validate(this);" language=jscript>
<table border=1 cellpadding=3 cellspacing=0>
    <tr class=tdheader>
        <td>Ref</td>
        <td>Added/Updated</td>
        <td>Policy Admin.</td>
        <td>Policy Section</td>
        <td>Policy Line Item</td>
        <td>Line Response</td>
        <td><input type=checkbox value=Y name=all onclick=checkAll()></td>
    </tr>
<?php
$sql = "SELECT i.intid";
$sql.= ", q.id qid";
$sql.= ", q.value qvalue";
$sql.= ", q.type qtype";
$sql.= ", i.field3 value";
$sql.= ", i.intdate";
$sql.= ", qi.value ivalue";
$sql.= ", t.tkname, t.tksurname ";
$sql.= ", i.field2";
$sql.= ", i.field4";
$sql.= ", i.field5";
$sql.= ", i.field6";
$sql.= ", i.field7";
$sql.= ", i.field8";
$sql.= ", i.field9 ";
$sql.= "FROM assist_".$cmpcode."_tvl_policy_interim i";
$sql.= ", assist_".$cmpcode."_tvl_policy_questions q";
$sql.= ", assist_".$cmpcode."_tvl_policy_qindex qi";
$sql.= ", assist_".$cmpcode."_timekeep t ";
$sql.= "WHERE ";
$sql.= "((inttable = 'tvl_policy' AND q.id = i.field2) ";
$sql.= "OR ";
$sql.= "(inttable = 'tvl_ccard' AND q.id = i.field10)) ";
$sql.= "AND i.field3 <> '' ";
$sql.= "AND intstatus = 'P' ";
$sql.= "AND q.cateid = qi.id ";
$sql.= "AND i.intuser = t.tkid ";
$sql.= "AND qi.yn = 'Y' AND q.yn = 'Y' ";
$sql.= "ORDER BY qi.sort, q.sort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)==0)
    {
        echo("<tr class=tdgeneral><td colspan=7>There are no policy line items awaiting authorisation.</td></tr>");
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
            $qtype = $row['qtype'];
            include("inc_tr.php");
            ?>
                <td class=tdgeneral valign=top class=tdheader><?php echo($row['intid']); ?></td>
                <td class=tdgeneral valign=top><?php echo(date("d-M-Y",$row['intdate'])); ?></td>
                <td class=tdgeneral valign=top><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
                <td class=tdgeneral valign=top><?php echo($row['ivalue']); ?></td>
            <?php
            if($qtype != "CC")
            {
            ?>
                <td class=tdgeneral valign=top><?php echo($row['qvalue']); ?></td>
                <td class=tdgeneral valign=top><?php
                    switch($row['qtype'])
                    {
                        case "YN":
                            if($row['value']=="Y")
                            {
                                echo("Yes");
                            }
                            else
                            {
                                if($row['value']=="No")
                                {
                                    echo("No");
                                }
                                else
                                {
                                    echo($row['value']);
                                }
                            }
                            break;
                        case "T":
                            echo($row['value']);
                            break;
                        case "DD":
                            $fld = explode("_",$row['value']);
if($fld[0]=="X") {
echo ("Unspecified");
} else {
                            $sql2 = "SELECT * FROM assist_".$cmpcode."_tvl_policy_qvalue WHERE questionid = ".$row['qid']." AND id = ".$fld[0];
                            include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                echo($row2['value']);
                            mysql_close($con2);
}
                            break;
                        default:
                            echo($row['value']);
                            break;
                    }
                    ?></td>
            <?php
            }
            else
            {
                ?>
                <td class=tdgeneral valign=top>Credit card details:</td><td>
                <Table border=0 cellpadding=2 cellspacing=0 width=100%>
                                            <tr class=tdgeneral >
                                <td valign=top>Card type:</i>
                                <?php
                                $sql2 = "SELECT v.udfvvalue FROM assist_".$cmpcode."_udfindex i, assist_".$cmpcode."_udfvalue v WHERE i.udfiid = v.udfvindex AND i.udfiref = 'TVL' AND i.udfisort = 1 AND i.udfiyn = 'Y' AND v.udfvid = ".$row['field2'];
                                include("inc_db_con2.php");
                                    $row2 = mysql_fetch_array($rs2);
                                    echo($row2['udfvvalue']);
                                mysql_close($con2);
                                ?>&nbsp;</td>
                            </tr>
                            <tr class=tdgeneral >
                                <td valign=top >Bank:</i> <?php echo($row['value']);?>&nbsp;</td>
                            </tr>
                            <tr class=tdgeneral >
                                <td valign=top>Card Status: <?php echo($row['field4']);?>&nbsp;</td>
                            </tr>
                            <tr class=tdgeneral >
                                <td valign=top>Card number:</i> <?php echo($row['field5']);?>&nbsp;</td>
                            </tr>
                            <tr class=tdgeneral >
                                <td valign=top>Cardholder's Name:&nbsp;<?php echo($row['field6']);?>&nbsp;</td>
                            </tr>
                            <tr class=tdgeneral >
                                <td valign=top >Expiry Date:</i> <?php echo(date("M Y",$row['field7']));?>&nbsp;</td>
                            </tr>
                            <tr class=tdgeneral >
                                <td valign=top >CVV number:</i> <?php echo($row['field8']);?></td>
                            </tr>
                </table></td>
                <?php
            }
            ?>
                <td class=tdheader valign=top><input type=checkbox name=ref<?php echo($row['intid']); ?> value=Y></td>
            </tr>
            <?php
        }
    }
mysql_close();
?>


<tr class=tdgeneral>
    <td colspan=7><input type=hidden name=act value=approve>
        <span id=but1><input type=submit name=approve value="Approve checked items"> <input type=button name=subbut value="Reject checked items" onclick=subMit('reject')></span>
        <span id=but2><input type=submit name=reject value="Submit rejection"> <input type=button name=subbut value="Cancel" onclick=subMit('approve')><br>
        Please indicate the reason for your rejection<Br>and click the "Submit rejection" button above:<br>
        <textarea name=rejreason cols=40 rows=5></textarea><br>
        <input type=checkbox name=notify value=Y> Notify Policy Administrators?</span>
    </td>
</tr>
</table>
</form>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=policy.php class=grey>Go back</a></p>
<script language=JavaScript>
    var target = document.getElementById( 'but2' );
    target.style.display = "none";

</script>
</body>

</html>
