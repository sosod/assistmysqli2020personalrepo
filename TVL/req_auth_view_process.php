<?php include("inc_ignite.php"); ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request - Authorisation</b></h1>
<p><label for=lbl2 id=lbl2b>Processing...</label> <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1><input type=hidden name=lbl2></p>
<?php
$reqid = $_POST['reqid'];
$f = $_POST['f'];
$h = $_POST['h'];
$c = $_POST['c'];
$x = $_POST['x'];
$n = $_POST['n'];
$act = $_POST['act'];
$response = $_POST['response'];

if(strlen($reqid)>0) {
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = $reqid";
    include("inc_db_con.php");
    if(mysql_num_rows($rs)>0) {
        $tvlreq = mysql_fetch_array($rs);
        $reqrow = $tvlreq;
    } else {
        die("An error has occurred.  Please go back and try again.");
    }
    mysql_close($con);
    if(strlen($tvlreq['adduser'])>3) {
        $sql = "SELECT tkname, tksurname, tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tvlreq['adduser']."' AND tkstatus = 1 AND tkemail <> ''";
        include("inc_db_con.php");
            if(mysql_num_rows($rs)>0) {
                $tkem = "Y";
                $tk = mysql_fetch_array($rs);
            } else {
                $tkem = "N";
            }
        mysql_close($con);
        $sql = "SELECT tkname, tksurname, tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tkid."'";
        include("inc_db_con.php");
                $authuser = mysql_fetch_array($rs);
        mysql_close($con);
    }
} else {
    die("An error has occurred.  Please go back and try again.");
}

//DISPLAY RESULTS FOR TESTING
/*echo("<P>".$reqid);
echo("<P>");
print_r($f);
echo("<P>");
print_r($h);
echo("<P>");
print_r($c);
echo("<P>");
print_r($x);
echo("<P>");
print_r($n);
echo("<P>".$act);
echo("<P>".$response);
*/
//FORMAT response
$response = htmlentities($response,ENT_QUOTES,"ISO-8859-1");
 ?>
<script language=JavaScript> lbl.innerText="10%"; </script>
<?php
//UPDATE FLIGHT
foreach($f as $fid)
{
    $sql = "UPDATE assist_".$cmpcode."_tvl_req_flight SET ";
    $sql.= "  status = '".$act."'";
    $sql.= ", authtkid = '".$tkid."'";
    $sql.= ", authdate = ".$today;
    $sql.= " WHERE id = ".$fid;
    include("inc_db_con.php");
        $tref = "TVL";
        $trans = "Updated status of flight record ".$fid." to ".$act;
        $tsql = $sql;
        include("inc_transaction_log.php");
}
 ?>
<script language=JavaScript> lbl.innerText="20%"; </script>
<?php
//UPDATE ACC
foreach($h as $hid)
{
    $sql = "UPDATE assist_".$cmpcode."_tvl_req_acc SET ";
    $sql.= "  status = '".$act."'";
    $sql.= ", authtkid = '".$tkid."'";
    $sql.= ", authdate = ".$today;
    $sql.= " WHERE id = ".$hid;
    include("inc_db_con.php");
        $tref = "TVL";
        $trans = "Updated status of accommodation record ".$hid." to ".$act;
        $tsql = $sql;
        include("inc_transaction_log.php");
}
 ?>
<script language=JavaScript> lbl.innerText="30%"; </script>
<?php
//UPDATE CAR
foreach($c as $cid)
{
    $sql = "UPDATE assist_".$cmpcode."_tvl_req_car SET ";
    $sql.= "  status = '".$act."'";
    $sql.= ", authtkid = '".$tkid."'";
    $sql.= ", authdate = ".$today;
    $sql.= " WHERE id = ".$cid;
    include("inc_db_con.php");
        $tref = "TVL";
        $trans = "Updated status of car rental record ".$cid." to ".$act;
        $tsql = $sql;
        include("inc_transaction_log.php");
}
 ?>
<script language=JavaScript> lbl.innerText="40%"; </script>
<?php
//UPDATE FOREX
foreach($x as $xid)
{
    $sql = "UPDATE assist_".$cmpcode."_tvl_req_forex SET ";
    $sql.= "  status = '".$act."'";
    $sql.= ", authtkid = '".$tkid."'";
    $sql.= ", authdate = ".$today;
    $sql.= " WHERE id = ".$xid;
    include("inc_db_con.php");
        $tref = "TVL";
        $trans = "Updated status of forex record ".$xid." to ".$act;
        $tsql = $sql;
        include("inc_transaction_log.php");
}
 ?>
<script language=JavaScript> lbl.innerText="50%"; </script>
<?php
//UPDATE ADD
foreach($n as $nid)
{
    $sql = "UPDATE assist_".$cmpcode."_tvl_req_traveller SET ";
    $sql.= "  status = '".$act."'";
    $sql.= ", authtkid = '".$tkid."'";
    $sql.= ", authdate = ".$today;
    $sql.= " WHERE id = ".$nid;
    include("inc_db_con.php");
        $tref = "TVL";
        $trans = "Updated status of traveller record ".$nid." to ".$act;
        $tsql = $sql;
        include("inc_transaction_log.php");
}
 ?>
<script language=JavaScript> lbl.innerText="60%"; </script>
<?php
 //UPDATE AUTH_RESPNSE
$lines = "";
if(count($f)>0)
{
    $lines = "f".implode("_f",$f);
}
if(count($h)>0)
{
    if(strlen($lines)>0)
    {
        $lines.= chr(10);
    }
    $lines.= "h".implode("_h",$h);
}
if(count($c)>0)
{
    if(strlen($lines)>0)
    {
        $lines.= chr(10);
    }
    $lines.= "c".implode("_c",$c);
}
if(count($x)>0)
{
    if(strlen($lines)>0)
    {
        $lines.= chr(10);
    }
    $lines.= "fx".implode("_fx",$x);
}
if(count($n)>0)
{
    if(strlen($lines)>0)
    {
        $lines.= chr(10);
    }
    $lines.= "n".implode("_n",$n);
}
 ?>
<script language=JavaScript> lbl.innerText="70%"; </script>
<?php
$sql = "INSERT INTO assist_".$cmpcode."_tvl_req_auth_response SET ";
$sql.= "reqid = ".$reqid;
$sql.= ", response = '".$response."'";
$sql.= ", authtkid = '".$tkid."'";
$sql.= ", authdate = ".$today;
$sql.= ", act = '".$act."'";
$sql.= ", records = '".$lines."'";
include("inc_db_con.php");
$respid = mysql_insert_id($con);
    $tref = "TVL";
    $trans = "Inserted response record ".$respid;
    $tsql = $sql;
    include("inc_transaction_log.php");
        if($tkem == "Y") {
        switch($act)
        {
        case "A":
            $subject = "Travel Request (".$tvlreq['ref'].") Approved";
            $message = "Your travel request ".$tvlreq['ref']." has been approved by ".$authuser['tkname']." ".$authuser['tksurname'].".";
            if(strlen($response)>0) {
                $message.= "\n\nThe reason given is:\n".$response;
            }
            $from = "no-reply@ignite4u.co.za";
            $to = $tk['tkemail'];
			if(strlen($to) > 0 && strpos($to,"@")>0) {
                $header = "From: ".$from;
                mail($to,$subject,$message,$header);
            }
            break;
        case "R":
            $subject = "Travel Request (".$tvlreq['ref'].") Rejected";
            $message = "Your travel request ".$tvlreq['ref']." has been rejected by ".$authuser['tkname']." ".$authuser['tksurname'].".";
            if(strlen($response)>0) {
                $message.= "\n\nThe reason given is:\n".$response;
            }
            $from = "no-reply@ignite4u.co.za";
            $to = $tk['tkemail'];
			if(strlen($to) > 0 && strpos($to,"@")>0) {
                $header = "From: ".$from;
                mail($to,$subject,$message,$header);
            }
            break;
        }
        }
 ?>
<script language=JavaScript> lbl.innerText="80%"; </script>
<?php
//CHECK STATUS OF REQUEST - all authorised/rejected?
    $c['a'] = 0;
    $c['r'] = 0;
    $c['p'] = 0;
    //flight
    $sql = "SELECT status, count(id) as c FROM assist_".$cmpcode."_tvl_req_flight WHERE yn = 'Y' AND reqid = ".$reqid." GROUP BY status";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $c[strtolower($row['status'])] = $c[strtolower($row['status'])]+$row['c'];
        }
    mysql_close();
    //acc
    $sql = "SELECT status, count(id) as c FROM assist_".$cmpcode."_tvl_req_acc WHERE yn = 'Y' AND reqid = ".$reqid." GROUP BY status";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $c[strtolower($row['status'])] = $c[strtolower($row['status'])]+$row['c'];
        }
    mysql_close();
    //car
    $sql = "SELECT status, count(id) as c FROM assist_".$cmpcode."_tvl_req_car WHERE yn = 'Y' AND reqid = ".$reqid." GROUP BY status";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $c[strtolower($row['status'])] = $c[strtolower($row['status'])]+$row['c'];
        }
    mysql_close();
    //forex
    $sql = "SELECT status, count(id) as c FROM assist_".$cmpcode."_tvl_req_forex WHERE yn = 'Y' AND reqid = ".$reqid." GROUP BY status";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $c[strtolower($row['status'])] = $c[strtolower($row['status'])]+$row['c'];
        }
    mysql_close();
    //add
    $sql = "SELECT status, count(id) as c FROM assist_".$cmpcode."_tvl_req_traveller WHERE reqid = ".$reqid." GROUP BY status";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $c[strtolower($row['status'])] = $c[strtolower($row['status'])]+$row['c'];
        }
    mysql_close();
 ?>
<script language=JavaScript> lbl.innerText="90%"; </script>
<?php
    if($c['p'] == 0 && $c['r'] == 0)
    {
        //UDPATE RECORD
        $sql = "UPDATE assist_".$cmpcode."_tvl_request SET status = 'A', authdate = '".$today."' WHERE id = ".$reqid;
        include("inc_db_con.php");
//echo("<p>".$sql);
            $tref = "TVL";
            $trans = "Updated status for record ".$reqid;
            $tsql = $sql;
            include("inc_transaction_log.php");

        //UPDATE TRAVEL AGENT
//        $sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
//        include("inc_db_con.php");
//            $reqrow = mysql_fetch_array($rs);
//echo("<p>".$sql);
//        mysql_close();
        $cmpcode3 = strtolower($reqrow['tagent']);
        //check travel agent setup - assign or claim?
        $agentaction = "A";
        if($agentaction == "C")
        {
        }
        else
        {
            $sql3 = "SELECT * FROM assist_".$cmpcode3."_tvla_list_agents WHERE cmpcode = '".strtoupper($cmpcode)."' AND pri = 'Y' AND tvltype = '".$reqrow['type']."' AND yn = 'Y'";
//echo("<p> $cmpcode3 - ".$sql3);
            include("inc_db_con3.php");
                $tmnr = mysql_num_rows($rs3);
                $tarow = mysql_fetch_array($rs3);
//echo("<p>2".$sql3);
            mysql_close();
            if($tmnr>0)
            {
                $agent = $tarow['tkid'];
                $sql3 = "SELECT tkname, tksurname, tkemail FROM assist_".$cmpcode3."_timekeep WHERE tkid = '$agent' AND tkstatus = 1";
                include("inc_db_con3.php");
                    $tkagent = mysql_fetch_array($rs3);
                mysql_close($con3);
            }
            else
            {
            }
        }
        //numtrav
        $sql = "SELECT * FROM assist_".$cmpcode."_tvl_req_traveller WHERE reqid = ".$reqid;
        include("inc_db_con.php");
            $numtrav = mysql_num_rows($rs);
//echo("<p>".$sql);
        mysql_close();
        //traveldate
        $traveldate = 0;
        $sql = "SELECT fdate FROM assist_".$cmpcode."_tvl_req_flight WHERE yn = 'Y' AND reqid = ".$reqid." ORDER BY fdate";
        include("inc_db_con.php");
            $travd = mysql_num_rows($rs);
            $row = mysql_fetch_array($rs);
            $traveldate = $row['fdate'];
//echo("<p>".$sql);
        mysql_close();
        if($travd == 0)
        {
            //check accommodation
            $sql = "SELECT indate FROM assist_".$cmpcode."_tvl_req_acc WHERE yn = 'Y' AND reqid = ".$reqid." ORDER BY indate";
            include("inc_db_con.php");
                $travd = mysql_num_rows($rs);
                $row = mysql_fetch_array($rs);
                $traveldate = $row['indate'];
//echo("<p>".$sql);
            mysql_close();
            if($travd == 0)
            {
                $sql = "SELECT pickdate FROM assist_".$cmpcode."_tvl_req_car WHERE yn = 'Y' AND reqid = ".$reqid." ORDER BY pickdate";
                include("inc_db_con.php");
                    $travd = mysql_num_rows($rs);
                    $row = mysql_fetch_array($rs);
                    $traveldate = $row['pickdate'];
//echo("<p>".$sql);
                mysql_close();
                if($travd == 0)
                {
                    $traveldate = $reqrow['adddate'];
                }
            }
        }
        $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$reqrow['adduser']."'";
        include("inc_db_con.php");
            $approw = mysql_fetch_array($rs);
        mysql_close();
        $appname = $approw['tkname']." ".$approw['tksurname'];

        $sql3 = "INSERT INTO assist_".$cmpcode3."_tvla_request SET ";
        $sql3.= "  ref = '".$reqrow['ref']."'";
        $sql3.= ", agent = '".$agent."'";
        $sql3.= ", status = 'N'";
        $sql3.= ", moddate = 0";
        $sql3.= ", moduser = '0000'";
        $sql3.= ", taskid = 0";
        $sql3.= ", cmpcode = '".strtoupper($cmpcode)."'";
        $sql3.= ", applicantid = '".$reqrow['adduser']."'";
        $sql3.= ", numtrav = ".$numtrav;
        $sql3.= ", ttype = '".$reqrow['type']."'";
        $sql3.= ", traveldate = ".$traveldate;
        $sql3.= ", agentdate = '0'";
        $sql3.= ", agentdate2 = 0";
        $sql3.= ", applicantname = '".$appname."'";
        $sql3.= ", appdate = ".$today;
        include("inc_db_con3.php");
            $tvlaid = mysql_insert_id();
//echo("<p>".$sql3);


//CHECK IF TASK EXISTS ON TRAVEL AGENTS' SYSTEM



        //CREATE TASK
        $sql3 = "SELECT * FROM assist_".$cmpcode3."_ta_list_topic WHERE value = 'Travel Requests' ORDER BY id DESC";
        include("inc_db_con3.php");
            $tatopic = mysql_num_rows($rs3);
            $tarow = mysql_fetch_array($rs3);
            $tasktopicid = $tarow['id'];
//echo("<p>".$sql3);
        mysql_close();
        if($tatopic==0)
        {
            $sql3 = "INSERT INTO assist_".$cmpcode3."_ta_list_topic SET value = 'Travel Requests', yn = 'N'";
            include("inc_db_con3.php");
                $tasktopicid = mysql_insert_id();
//echo("<p>".$sql3);
        }
        $sql3 = "INSERT INTO assist_".$cmpcode3."_ta_task SET ";
        $sql3.= "tasktkid = '".$agent."', ";
        $sql3.= "taskurgencyid = 2, ";
        $sql3.= "tasktopicid = ".$tasktopicid.", ";
        $sql3.= "taskaction = 'Make travel arrangements as per request ".$reqrow['ref']."', ";
        $sql3.= "taskdeliver = 'Travel arrangements completed.', ";
        $sql3.= "taskstatusid = '4', ";
        $sql3.= "taskstate = 0, ";
        $sql3.= "taskdeadline = '".$traveldate."', ";
        $sql3.= "taskadddate = '".$today."', ";
        $sql3.= "taskadduser = '".$agent."'";
        include("inc_db_con3.php");
            $taskid = mysql_insert_id();
	$sql3 = "INSERT INTO assist_".$cmpcode3."_ta_task_recipients (taskid, tasktkid) VALUES ($taskid,'$agent')";
	include("inc_db_con3.php");
	$logupdate = "New task added.".chr(10)."Task instructions: ".$taskaction.chr(10)."Task deliverables: ".$taskdeliver;
	$sql3 = "INSERT INTO assist_".$cmpcode3."_ta_log SET ";
	$sql3 .= "logdate = '".$today."', ";
	$sql3 .= "logtkid = '".$agent."', ";
	$sql3 .= "logupdate = '".$logupdate."', ";
	$sql3 .= "logstatusid = '4', ";
	$sql3 .= "logstate = 0, ";
	$sql3 .= "logemail = 'N', ";
	$sql3 .= "logsubmittkid = '".$agent."', ";
	$sql3 .= "logtaskid = '".$taskid."', ";
	$sql3 .= "logtasktkid = '".$agent."'";
	include("inc_db_con3.php");
//echo("<p>".$sql3);
        //ADD UDF RECORD
        $sql3 = "SELECT * FROM assist_".$cmpcode3."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
        include("inc_db_con3.php");
            $udf = mysql_num_rows($rs3);
            $udfrow = mysql_fetch_array($rs3);
        mysql_close();
//echo("<p>".$sql3);
        if($udf > 0 && $taskid > 0)
        {
            $sql3 = "INSERT INTO assist_".$cmpcode3."_udf SET";
            $sql3.= " udfindex = ".$udfrow['udfiid'];
            $sql3.= ", udfvalue = '".$reqrow['ref']."'";
            $sql3.= ", udfnum = ".$taskid;
            $sql3.= ", udfref = 'TATVL'";
            include("inc_db_con3.php");
//echo("<p>".$sql3);
        }
        //UPDATE TVLA RECORD
        $sql3 = "UPDATE assist_".$cmpcode3."_tvla_request SET taskid = ".$taskid." WHERE id = ".$tvlaid;
        include("inc_db_con3.php");
//echo("<p>".$sql3);
        //NOTIFY TRAVEL AGENT
        $to = $tkagent['tkemail'];
        if(strlen($to)>0 && strpos($to,"@")>0) {
            $sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '$cmpcode'";
            include("inc_db_con_assist.php");
                $cmp = mysql_fetch_array($rs);
            mysql_close($con);
            $subject = "New Travel Request - ".$tvlreq['ref'];
            $addname = $tk['tkname']." ".$tk['tksurname'];
            if(strlen($addname)>1) {
                $message = "A new travel request ".$tvlreq['ref']." has been submitted by ".$addname;
                if(strlen($cmp['cmpname'])>0) { $message.= " at ".$cmp['cmpname']; }
            } else {
                $message = "A new travel request ".$tvlreq['ref']." has been submitted";
                if(strlen($cmp['cmpname'])>0) { $message.= " by ".$cmp['cmpname']; }
            }
            $message.= ". \n\nPlease log onto Ignite Assist (http://assist.ignite4u.co.za) to review this request.";
            $from = "no-reply@ignite4u.co.za";
            $to = $tk['tkemail'];
			if(strlen($to) > 0 && strpos($to,"@")>0) {
                $header = "From: ".$from;
                mail($to,$subject,$message,$header);
            }
        }
        //NOTIFY APPLICANT
        if($tkem == "Y") {
            $subject = "Travel Request (".$tvlreq['ref'].") Submitted";
            $agentname = $tkagent['tkname']." ".$tkagent['tksurname'];
            if(strlen($agentname)>1) {
                $message = "Your travel request ".$tvlreq['ref']." has been submitted to your Travel Agent - ".$agentname.".";
            } else {
                $message = "Your travel request ".$tvlreq['ref']." has been submitted to the Travel Agent.";
            }
            $from = "no-reply@ignite4u.co.za";
            $to = $tk['tkemail'];
			if(strlen($to) > 0 && strpos($to,"@")>0) {
                $header = "From: ".$from;
                mail($to,$subject,$message,$header);
            }
        }
    } else {
    }
 ?>
<script language=JavaScript>
lbl.innerText="100%";
lbl2b.innerText = "Processing complete.";
document.location.href = "req_auth_list.php";
</script>

</body>

</html>
