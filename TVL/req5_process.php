<?php include("inc_ignite.php");

include("inc_tvl.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request Step 4 - Car Rental Details</b></h1>
<p>Processing... <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1></p>

<?php

$reqid = $_POST['reqid'];


if(strlen($reqid) > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
    include("inc_db_con.php");
        $reqrow = mysql_fetch_array($rs);
    mysql_close();

    if(count($reqrow)>0)
    {
        //GET DETAILS
        $tk = $_POST['tk'];
        $ttkid = $_POST['tkid'];
        $project = $_POST['project'];
        $currencysel = $_POST['currencysel'];
        $currency = $_POST['currency'];
        $amt = $_POST['amt'];
        $amttype = $_POST['amttype'];
        $deliver = $_POST['deliver'];
        $deliveraddress = $_POST['deliveraddress'];
        $act = $_POST['act'];
        $perc = 15;
        $percinc = 80 / ($tk);
        $percinc = $percinc / 2;
        ?>
        <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
        <?php
        //CHECK DETAILS
        for($t=0;$t<$tk;$t++)
        {
            $tt = $ttkid[$t];
            $p = $project[$t];  
            $cs = $currencysel[$t];
            $c = $currency[$t];
            $a = $amt[$t];
            $at = $amttype[$t];
            $d = $deliver[$t];
            $da = $deliveraddress[$t];
            
            if($cs == "X") { $cu = $c; } else { $cu = $cs; }
            
            if(strlen($tt) > 3 && $p!="X" && $cu != "X" && strlen($cu) > 0 && strlen($a) > 0 && is_numeric($a) && $at != "X" && strlen($at) > 0 && (($d == "N") || ($d == "Y" && strlen($da) > 0)))
            {
                //FORMAT
                if($cs == "X") { $cu = htmlentities($c,ENT_QUOTES,"ISO-8859-1"); } else { $cu = $cs; }
                $da = htmlentities($da,ENT_QUOTES,"ISO-8859-1");
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
                //INSERT
                $sql = "INSERT INTO assist_".$cmpcode."_tvl_req_forex SET ";
                $sql.= "  reqid = ".$reqid;
                $sql.= ", tkid = '".$tt."'";
                $sql.= ", currency = '".$cu."'";
                $sql.= ", amount = ".$a;
                $sql.= ", amounttype = '".$at."'";
                $sql.= ", deliver = '".$d."'";
                $sql.= ", deliveraddress = '".$da."'";
                $sql.= ", yn = 'Y'";
                $sql.= ", projectid = ".$p;
                $sql.= "  , status = 'P'";
                $sql.= "  , authtkid = ''";
                $sql.= "  , authdate = 0";
                include("inc_db_con.php");
//                echo("<p>".$sql);
                    $tref = "TVL";
                    $trans = "Added forex record for travel request ".$reqid;
                    $tsql = $sql;
                    include("inc_transaction_log.php");
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
            else
            {
                $perc = $perc + $percinc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
        }
        //MOVE ON
                ?>
                <script language=JavaScript> lbl.innerText="100%"; </script>
                <?php
        if($act=="S")
        {
            echo("<script language=JavaScript> document.location.href = \"req6.php?r=".$reqid."\"; </script>");
        }
        else
        {
            echo("<script language=JavaScript> document.location.href = \"req5.php?r=".$reqid."\"; </script>");
        }
    }
    else
    {
        echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL5b).  Please go back and try again.</p>");
    }
}
else    //if strlen reqid > 0
{
    echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL5a).  Please go back and try again.</p>");
}

?>
</body>

</html>
