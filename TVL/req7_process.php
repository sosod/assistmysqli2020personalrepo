<?php include("inc_ignite.php");

include("inc_tvl.php");

$reqid = $_POST['reqid'];

$sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
include("inc_db_con.php");
    $reqrow = mysql_fetch_array($rs);
mysql_close();

if($reqrow['type']=="D")
{
    $step = "Step 6";
}
else
{
    $step = "Step 7";
}
if($reqrow['type']=="I")
{
    $forex = "N";
    //CHECK POLICY ON FOREX
    $sql = "SELECT value FROM assist_".$cmpcode."_tvl_policy WHERE questionid = 40 AND yn = 'Y'";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)==0)
        {
            $forex = "Y";
        }
        else
        {
            $row = mysql_fetch_array($rs);
            $forex = $row['value'];
        }
    mysql_close();
    if($forex == "N")
    {
        $step = "Step 6";
    }
    else
    {
        $step = "Step 7";
    }
}
else
{
    $step = "Step 6";
}


 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request <?php echo($step); ?> - Confirmation</b></h1>
<p><label for="lbl1" id=lbl2>Processing...</label> <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1></p>
<?php
if(strlen($reqid)>0)
{
    if(count($reqrow)>0)
    {
        //GET ACTION - submit or cancel
        $act = $_POST['act'];
        ?>
        <script language=JavaScript>
            lbl.innerText="50%";
        </script>
        <?php

        //IF CANCEL
        if($act == "C")
        {
            $sql = "UPDATE assist_".$cmpcode."_tvl_request SET status = 'C' WHERE id = ".$reqid;
            include("inc_db_con.php");
//            echo($sql);
                $tref = "TVL";
                $trans = "Cancelled travel request ".$reqid;
                $tsql = $sql;
                include("inc_transaction_log.php");
            ?>
            <script language=JavaScript>
                lbl.innerText="";
                lbl2.innerText="Processing complete. Your travel request has been cancelled.";
            </script>
            <?php
        }
        else    //ELSE IF SUBMIT
        {
            $sql = "SELECT ref FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
            include("inc_db_con.php");
                $tvl = mysql_fetch_array($rs);
            mysql_close($con);
            //UPDATE REQUEST
            $sql = "UPDATE assist_".$cmpcode."_tvl_request SET status = 'S' WHERE id = ".$reqid;
            include("inc_db_con.php");
//            echo($sql);
                $tref = "TVL";
                $trans = "Submitted travel request ".$reqid;
                $tsql = $sql;
                include("inc_transaction_log.php");
            //GET ALL PROJECT IDS
            $p = 0;
            $projects = array();
            $sql = "SELECT DISTINCT projectid FROM assist_".$cmpcode."_tvl_req_traveller WHERE reqid = ".$reqid;
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $projects[$p] = $row['projectid'];
                    $p++;
                }
            mysql_close();
            $sql = "SELECT DISTINCT projectid FROM assist_".$cmpcode."_tvl_req_forex  WHERE reqid = ".$reqid;
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $projects[$p] = $row['projectid'];
                    $p++;
                }
            mysql_close();
            $sql = "SELECT DISTINCT projectid FROM assist_".$cmpcode."_tvl_req_flight WHERE reqid = ".$reqid;
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $projects[$p] = $row['projectid'];
                    $p++;
                }
            mysql_close();
            $sql = "SELECT DISTINCT projectid FROM assist_".$cmpcode."_tvl_req_car WHERE reqid = ".$reqid;
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $projects[$p] = $row['projectid'];
                    $p++;
                }
            mysql_close();
            $sql = "SELECT DISTINCT projectid FROM assist_".$cmpcode."_tvl_req_acc WHERE reqid = ".$reqid;
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $projects[$p] = $row['projectid'];
                    $p++;
                }
            mysql_close();
            $projects = array_unique($projects);
//            print_r($projects);
            //GET AUTHORISERS FOR PROJECTs
            $tkemails = array();
            $e = 0;
            foreach($projects as $pid)
            {
                $sql = "SELECT t.tkemail";
                $sql.= " FROM  assist_".$cmpcode."_tvl_projects_authorisers a";
                $sql.= "    ,  assist_".$cmpcode."_timekeep t";
                $sql.= " WHERE t.tkid = a.tkid";
                $sql.= "  AND  projectid = ".$pid;
                $sql.= "  AND  yn = 'Y'";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $tkemails[$e] = $row['tkemail'];
                        $e++;
                    }
                mysql_close();
            }
            //SEND EMAILS TO AUTHORISERS FOR PROJECTs
            $tkemails = array_unique($tkemails);
            //print_r($tkemails);
			$to = strFn("implode",$tkemails,",","");
			if(strlen($to) > 0 && strpos($to,"@")>0) {
	       		$subject = "Travel Request Awaiting Authorisation";
    			$message = "Travel request ".$tvl['ref']." is awaiting your authorisation.\n\nPlease log onto Ignite Assist (http://assist.ignite4u.co.za/) to review this request.";
                $from = "no-reply@ignite4u.co.za";
                $header = "From: ".$from;
                mail($to,$subject,$message,$header);
			}
            ?>
            <script language=JavaScript>
                lbl.innerText="";
                lbl2.innerText="Processing complete. The authoriser(s) for your selected project(s) have been notified.";
            </script>
            <?php
        }
    }
    else
    {
        echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL7b).  Please go back and try again.</p>");
    }
}
else
{
        echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL7a).  Please go back and try again.</p>");
}
?>
</body>

</html>
