<?php include("inc_ignite.php"); ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request - Step 1</b></h1>
<p>Processing... <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1></p>
<?php
//GET VALUES - 25%
    $ref = "";
    $adduser = $tkid;
    $adddate = $today;
    $authuser = "";
    $authdate = 0;
    $authassigned = $_POST['tauth'];
    $status = "P";
    $tagent = $_POST['tagent'];
    $tagentdate = 0;
    $type = $_POST['ttype'];
    $traveller = $_POST['traveller'];
    ?>
    <script language=JavaScript> lbl.innerText="25%"; </script>
    <?php
//CREATE REQUEST RECORD - 50%
    $sql = "INSERT INTO assist_".$cmpcode."_tvl_request ";
    $sql.= "SET ref = ''";
    $sql.= "  , adduser = '".$adduser."'";
    $sql.= "  , adddate = ".$adddate."";
    $sql.= "  , authuser = '".$authuser."'";
    $sql.= "  , authdate = ".$authdate."";
    $sql.= "  , authassigned = '".$authassigned."'";
    $sql.= "  , status = '".$status."'";
    $sql.= "  , tagent = '".$tagent."'";
    $sql.= "  , tagentdate = ".$tagentdate."";
    $sql.= "  , type = '".$type."'";
    include("inc_db_con.php");
    //    echo("<p>".$sql."</p>");
    ?>
    <script language=JavaScript> lbl.innerText="50%"; </script>
    <?php
//GET REQID - 75%
    $reqid = 0;
    $reqid = mysql_insert_id($con);
        $tref = "TVL";
        $trans = "Added new travel request ".$reqid;
        $tsql = $sql;
        include("inc_transaction_log.php");
    //    echo("<p>".$reqid."</p>");
    ?>
    <script language=JavaScript> lbl.innerText="75%"; </script>
    <?php
if($reqid > 0)
{
    $ref = date("Y")."/".strtoupper($cmpcode)."/".$reqid;
//FOR EACH TRAVELLER CREATE TRAVELLER RECORD - 100%
    //    echo("<P>".count($traveller));
    //    print_r($traveller);
    //    echo("</p>");
    $tb = 0;
    foreach($traveller as $trav)
    {
        $tb++;
        if($trav != "X")
        {
            $sql = "INSERT INTO assist_".$cmpcode."_tvl_req_traveller ";
            $sql.= "SET reqid = ".$reqid;
            $sql.= "  , tkid = '".$trav."'";
            $sql.= "  , special = ''";
            $sql.= "  , shuttle = 'N'";
            $sql.= "  , shuttleaddy = ''";
            $sql.= "  , parking = 'N'";
            $sql.= "  , projectid = 0";
            $sql.= "  , status = 'P'";
            $sql.= "  , authtkid = ''";
            $sql.= "  , authdate = 0";
            include("inc_db_con.php");
    //            echo("<p>".$sql."</p>");
                $tref = "TVL";
                $trans = "Added traveller ".$trav." to new travel request ".$reqid;
                $tsql = $sql;
                include("inc_transaction_log.php");
        }
        if($tb == count($traveller))
        {
            ?>
            <script language=JavaScript> lbl.innerText="90%"; </script>
            <?php
        }
    }
    //update ref
    $sql = "UPDATE assist_".$cmpcode."_tvl_request SET ref = '".$ref."' WHERE id = ".$reqid;
    include("inc_db_con.php");
                $tref = "TVL";
                $trans = "Updated new travel request ".$reqid." to reflect ref ".$ref;
                $tsql = $sql;
                include("inc_transaction_log.php");
//MOVE ON
    echo("<script language=JavaScript> document.location.href = \"req2.php?r=".$reqid."\"; </script>");
}
else
{
    echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error.  Please go back and try again.</p>");
}
?>

</body>

</html>
