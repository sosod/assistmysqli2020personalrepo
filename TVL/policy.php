<?php include("inc_ignite.php");

include("inc_tvl.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Policy</b></h1>
<?php
if($poladmin == "P" || $poladmin == "B")
{
    ?>
    <h2 class=fc>Setup Policy</h2>
    <ul>
    <li><a href=policy_setup.php><span class=fc style="text-decoration: none"><u>1. Setup policy</u></span></a></li>
            <?php
    if($poldate == "N")
    {
        echo("<li>Please complete (and authorise) step 1 before continuing.</li>");
    }
    else
    {
    ?>
    <li><u>2. Setup lists:</u><ul class=ul2>
        <li>Flight<ul>
            <li><a href=policy_setup_lists.php?t=ft><span class=fc style="text-decoration: none"><i>Class of travel</i></span></a></li>
            <li><a href=policy_setup_lists.php?t=fp><span class=fc style="text-decoration: none"><i>Airlines</i></span></a></li>
        </ul></li>
        <li>Accommodation<ul>
            <li><a href=policy_setup_lists.php?t=ht><span class=fc style="text-decoration: none"><i>Room Types</i></span></a></li>
            <li><a href=policy_setup_lists.php?t=hp><span class=fc style="text-decoration: none"><i>Hotels</i></span></a></li>
        </ul></li>
        <li>Transport<ul>
            <li><a href=policy_setup_lists.php?t=ct><span class=fc style="text-decoration: none"><i>Car Rental Types</i></span></a></li>
            <li><a href=policy_setup_lists.php?t=cp><span class=fc style="text-decoration: none"><i>Car Rental Companies</i></span></a></li>
        </ul></li>
<?php //        <li><a href=#><span class=fc style="text-decoration: none">-Travel Agents</span></a></li> ?>
    </ul></li>
        <?php
        $sql = "SELECT intid FROM assist_".$cmpcode."_tvl_policy_interim WHERE intstatus = 'P' AND inttable = 'tvl_policy_aob'";
        include("inc_db_con.php");
            $aobmnr = mysql_num_rows($rs);
        mysql_close();
        if($aobmnr==0)
        {
            ?>
            <li><a href=policy_setup_aob.php><span class=fc style="text-decoration: none"><u>3. Setup allocation of benefits</u></span></a></li>
            <?php
        }
    }
    ?>
    </ul>
    <p><i>Note: Any changes made to the travel policy are only effective once they have been approved.<br>
<?php //    If you have made changes to the travel policy and wish to have them approved,<br>please click on the button below to notify the Policy Authorisers.<br>
    //<input type=button value="-Notify Policy Authorisers"></i></p>
    ?><?php
}

if($poladmin == "A" || $poladmin == "B")
{
    ?>
    <h2 class=fc>Authorise Policy</h2>
    <ul>
    <?php
$sql = "SELECT i.intid";
$sql.= ", q.id qid";
$sql.= ", q.value qvalue";
$sql.= ", q.type qtype";
$sql.= ", i.field3 value";
$sql.= ", i.intdate";
$sql.= ", qi.value ivalue";
$sql.= ", t.tkname, t.tksurname ";
$sql.= ", i.field2";
$sql.= ", i.field4";
$sql.= ", i.field5";
$sql.= ", i.field6";
$sql.= ", i.field7";
$sql.= ", i.field8";
$sql.= ", i.field9 ";
$sql.= "FROM assist_".$cmpcode."_tvl_policy_interim i";
$sql.= ", assist_".$cmpcode."_tvl_policy_questions q";
$sql.= ", assist_".$cmpcode."_tvl_policy_qindex qi";
$sql.= ", assist_".$cmpcode."_timekeep t ";
$sql.= "WHERE ";
$sql.= "((inttable = 'tvl_policy' AND q.id = i.field2) ";
$sql.= "OR ";
$sql.= "(inttable = 'tvl_ccard' AND q.id = i.field10)) ";
$sql.= "AND i.field3 <> '' ";
$sql.= "AND intstatus = 'P' ";
$sql.= "AND q.cateid = qi.id ";
$sql.= "AND i.intuser = t.tkid ";
$sql.= "AND qi.yn = 'Y' AND q.yn = 'Y' ";
$sql.= "ORDER BY qi.sort, q.sort";
include("inc_db_con.php");
$ap = mysql_num_rows($rs);
mysql_close();
    ?>
        <li><?php if($ap>0) { echo("<a href=policy_authorise.php><span class=fc style=\"text-decoration: none\">"); } ?><u>1. Authorise policy</u></span></a><?php if($ap==0) { echo("&nbsp;<i>- No policy changes to authorise.</i>"); } ?></li>
            <?php
    if($poldate == "N")
    {
        echo("<li>Please authorise step 1 before continuing.</li>");
    }
    else
    {
        $sql = "SELECT intid, intuser, intdate, field2 value, field3 yn, field4 type, field5 loc, field6 account, tkname, tksurname FROM assist_".$cmpcode."_tvl_policy_interim, assist_".$cmpcode."_timekeep ";
        $sql.= "WHERE intstatus = 'P' ";
        $sql.= "AND inttable = 'tvl_lists' ";
        $sql.= "AND intuser = tkid ";
        $sql.= "AND field1 = 0 ";
        include("inc_db_con.php");
            $ni = mysql_num_rows($rs);
        mysql_close();
        $sql = "SELECT intid, intuser, intdate, field2 value, field3 yn, field4 type, field5 loc, field6 account, tkname, tksurname FROM assist_".$cmpcode."_tvl_policy_interim, assist_".$cmpcode."_timekeep ";
        $sql.= "WHERE intstatus = 'P' ";
        $sql.= "AND inttable = 'tvl_lists' ";
        $sql.= "AND intuser = tkid ";
        $sql.= "AND field1 <> 0 ";
        include("inc_db_con.php");
            $ui = mysql_num_rows($rs);
        mysql_close();
    ?>
        <li><u>2. Authorise lists:</u><ul class=ul2>
            <li><?php if($ni>0) { echo("<a href=policy_authorise_lists.php?t=new><span class=fc style=\"text-decoration: none\">"); } ?><i>New items</i></span></a><?php if($ni==0) { echo("&nbsp;<i>- No new items to authorise.</i>"); } ?></li>
            <li><?php if($ui>0) { echo("<a href=policy_authorise_lists.php?t=upd><span class=fc style=\"text-decoration: none\">"); } ?><i>Updated items</i></span></a><?php if($ui==0) { echo("&nbsp;<i>- No updated items to authorise.</i>"); } ?></li>
        </ul></li>
        <?php
        $sql = "SELECT intid FROM assist_".$cmpcode."_tvl_policy_interim WHERE intstatus = 'P' AND inttable = 'tvl_policy_aob'";
        include("inc_db_con.php");
            $aobmnr = mysql_num_rows($rs);
        mysql_close();
        if($aobmnr>0)
        {
        ?>
        <li><a href=policy_authorise_aob.php><span class=fc style="text-decoration: none"><u>3. Authorise allocation of benefits</u></span></a></li>
        <?php
        }
        else
        {
        ?>
        <li><u>3. Authorise allocation of benefits</u>&nbsp;<i> - No changes to authorise.</i></li>
        <?php
        }
    }
    ?>
    </ul>
    <?php
}

if($poladmin != "A" && $poladmin != "B" && $poladmin != "P")
{
    echo("<p>Error</p>");
}

?>
</ul>
</body>

</html>
