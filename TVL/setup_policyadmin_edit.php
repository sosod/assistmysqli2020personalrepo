<?php
include("inc_ignite.php");

include("inc_tvl.php");

$result = "<p>&nbsp;</p>";
//GET VARIABLES PASSED BY FORM
$aid = $_GET['id'];
$act = $_GET['t'];
$atype = $_GET['a'];

if($act == "e" && strlen($atype)==1 && strlen($aid) > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_list_admins WHERE id = ".$aid;
    include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
        $err = "N";
        $row = mysql_fetch_array($rs);
    }
    else
    {
        $err = "Y";
    }
    mysql_close();

    if($err == "N")
    {
        $sql = "UPDATE assist_".$cmpcode."_tvl_list_admins SET atype = '".$atype."' WHERE id = ".$aid;
        include("inc_db_con.php");
        //SET TRANSACTION LOG VALUES FOR UPDATE
        $tsql = $sql;
        $told =  "UPDATE assist_".$cmpcode."_tvl_list_admins SET atype = '".$row['type']."' WHERE id = ".$aid;
        $trans = "Policy administrator access changed for user ".$atkid.".";
    }
    else
    {
        $tsql = $sql;
        $told =  "";
        $trans = "Error - Policy administrator access not changed for user ".$atkid." as record ".$aid." not found.";
    }
    $tref = "TVL";
    include("inc_transaction_log.php");
    
    echo("<script language=JavaScript>document.location.href='setup_policyadmin.php';</script>");

}
else
{
    if($act == "d" && strlen($aid) > 0)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_tvl_list_admins WHERE id = ".$aid;
        include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $err = "N";
            $row = mysql_fetch_array($rs);
        }
        else
        {
            $err = "Y";
        }
        mysql_close();

        if($err == "N")
        {
            $sql = "UPDATE assist_".$cmpcode."_tvl_list_admins SET yn = 'N' WHERE id = ".$aid;
            include("inc_db_con.php");
            //SET TRANSACTION LOG VALUES FOR UPDATE
            $tsql = $sql;
            $told =  "UPDATE assist_".$cmpcode."_tvl_list_admins SET yn = 'Y' WHERE id = ".$aid;
            $trans = "Policy administrator access removed for user ".$atkid.".";
        }
        else
        {
            $tsql = $sql;
            $told =  "";
            $trans = "Error - Policy administrator access not removed for user ".$atkid." as record ".$aid." not found.";
        }
        $tref = "TVL";
        include("inc_transaction_log.php");

        echo("<script language=JavaScript>document.location.href='setup_policyadmin.php';</script>");

    }
}



?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function editUser(id) {
    //alert(id);
    var acc = document.edituser.atype.value;
    //alert(acc);
    document.location.href = "setup_policyadmin_edit.php?id="+id+"&t=e&a="+acc;
}

function delUser(id) {
    //CONFIRM DELETE ACTION
    if(confirm("Are you sure you wish to perform this action?"))
    {
        document.location.href = "setup_policyadmin_edit.php?id="+id+"&t=d";
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Setup - Policy Administrators</b></h1>
<p>&nbsp;</p>
<form name=edituser style="margin-top: 0px">
<table border="1" id="table1" cellspacing="0" cellpadding="4" width=500 style="border-collapse: collapse; border-style: solid;">
	<tr>
		<td class="tdheader" width=30>ID</td>
		<td class="tdheader" width=210>User</td>
		<td class="tdheader" width=260>Access</td>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.type, a.id ";
    $sql .= "FROM assist_".$cmpcode."_tvl_list_admins a, assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE a.yn = 'Y' ";
    $sql .= "AND t.tkid = a.tkid ";
    $sql .= "AND t.tkstatus = 1 ";
    $sql .= "AND a.yn = 'Y' ";
    $sql .= "AND a.id = ".$aid." ";
    $sql .= "AND (a.type = 'P' OR a.type = 'A' OR a.type = 'B') ";
    $sql .= "ORDER BY tkname, tksurname";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $psel['A'] = "";
        $psel['B'] = "";
        $psel['P'] = "";
        $psel[$row['type']] = "selected ";
        switch($row['type'])
        {
            case "A":
                $ptype = "Authorise policy changes only";
                break;
            case "B":
                $ptype = "Both make and authorise changes";
                break;
            case "P":
                $ptype = "Make changes to policy only";
                break;
            default:
                $ptype = "Make changes to policy only";
                break;
        }
?>
        <tr>
            <td class="tdgeneral"><?php echo($row['tkid']); ?></td>
            <td class="tdgeneral"><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td class="tdgeneral">
                <select name=atype>
                    <option <?php echo($psel['P']); ?> value=P>Make changes to policy only</option>
                    <option <?php echo($psel['A']); ?> value=A>Authorise policy changes only</option>
                    <option <?php echo($psel['B']); ?> value=B>Both make and authorise changes</option>
                </select>
            </td>
        </tr>
<?php
    }
    mysql_close();
?>
    <tr>
        <td class=tdgeneral colspan=3><input type=button  style="padding-left: 2px; padding-right: 1px" value=Edit onclick=editUser(<?php echo($aid);?>)> <input type=button value=Delete onclick=delUser(<?php echo($aid);?>) ></td>
    </tr>
</table>
</form>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=setup_policyadmin.php class=grey>Go back</a></p>
</body>

</html>
