<?php include("inc_ignite.php");

include("inc_tvl.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist: Travel Request Step 2 - Flight Details</b></h1>
<p>Processing... <label for="lbl1" id=lbl>0%</label><input type=hidden name=lbl1></p>

<?php

$reqid = $_POST['reqid'];


if(strlen($reqid) > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_tvl_request WHERE id = ".$reqid;
    include("inc_db_con.php");
        $reqrow = mysql_fetch_array($rs);
    mysql_close();

    if(count($reqrow)>0)
    {
        //GET DETAILS
        $tk = $_POST['tk'];
        $ttkid = $_POST['tkid'];
        $project = $_POST['project'];
        $airline = $_POST['airline'];
        $dcity = $_POST['dcity'];
        $dcountry = $_POST['dcountry'];
        $special = $_POST['special'];
        $fday = $_POST['fday'];
        $fmon = $_POST['fmon'];
        $fyear = $_POST['fyear'];
        $fhour = $_POST['fhour'];
        $fmin = $_POST['fmin'];
        $fdatetype = $_POST['fdatetype'];
        $class = $_POST['class'];
        $acity = $_POST['acity'];
        $acountry = $_POST['acountry'];
        $act = $_POST['act'];
        $perc = 15;
        $percinc = 80 / ($tk);
        $percinc = $percinc / 2;
        ?>
        <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
        <?php
        //CHECK DETAILS
        for($t=0;$t<$tk;$t++)
        {
            $tt = $ttkid[$t];
            $p = $project[$t];  
            $a = $airline[$t];  
            $dci = $dcity[$t];
            $dcy = $dcountry[$t];
            $s = $special[$t];
            $fd = $fday[$t];
            $fm = $fmon[$t];
            $fy = $fyear[$t];
            $fh = $fhour[$t];
            $fn = $fmin[$t];
            $fdt = $fdatetype[$t];
            $c = $class[$t];
            $aci = $acity[$t];
            $acy = $acountry[$t];
            if($p!="X" && $a != "X" && strlen($a) > 0 && strlen($dci) > 0 && $dcy != "X" && strlen($dcy) > 0 && $fd > 0 && $fm > 0 && $fyear > 1000 && $fh > 0 && $fh < 25 && $fn >= 0 && $fn < 61 && strlen($fdt) > 0 && $fdt != "X" && $c != "X" && strlen($c)> 0 && strlen($aci) > 0 && $acy != "X" && strlen($acy) > 0)
            {
                //FORMAT
                $s = htmlentities($s,ENT_QUOTES,"ISO-8859-1");
                $dci = htmlentities($dci,ENT_QUOTES,"ISO-8859-1");
                $fdate = mktime($fh,$fn,0,$fm,$fd,$fy);
                $aci = htmlentities($aci,ENT_QUOTES,"ISO-8859-1");
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
                //INSERT
                $sql = "INSERT INTO assist_".$cmpcode."_tvl_req_flight SET ";
                $sql.= "  reqid = ".$reqid;
                $sql.= ", tkid = '".$tt."'";
                $sql.= ", airlineid = ".$a;
                $sql.= ", classid = ".$c;
                $sql.= ", fdate = ".$fdate;
                $sql.= ", fdatetype = '".$fdt."'";
                $sql.= ", dcity = '".$dci."'";
                $sql.= ", dcountryid = ".$dcy;
                $sql.= ", special = '".$s."'";
                $sql.= ", acity = '".$aci."'";
                $sql.= ", acountryid = ".$acy;
                $sql.= ", yn = 'Y'";
                $sql.= ", projectid = ".$p;
                $sql.= "  , status = 'P'";
                $sql.= "  , authtkid = ''";
                $sql.= "  , authdate = 0";
                include("inc_db_con.php");
                //echo("<p>".$sql);
                    $tref = "TVL";
                    $trans = "Added flight record for travel request ".$reqid;
                    $tsql = $sql;
                    include("inc_transaction_log.php");
                $perc = $perc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
            else
            {
                $perc = $perc + $percinc + $percinc;
                ?>
                <script language=JavaScript> lbl.innerText="<?php echo($perc); ?>%"; </script>
                <?php
            }
        }
        //MOVE ON
                ?>
                <script language=JavaScript> lbl.innerText="100%"; </script>
                <?php
        if($act=="S")
        {
            echo("<script language=JavaScript> document.location.href = \"req3.php?r=".$reqid."\"; </script>");
        }
        else
        {
            echo("<script language=JavaScript> document.location.href = \"req2.php?r=".$reqid."\"; </script>");
        }
    }
    else
    {
        echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL2b).  Please go back and try again.</p>");
    }
}
else    //if strlen reqid > 0
{
    echo("<h2 class=fc>ERROR!</h2><p>Ignite Assist has encountered an error (error code: TVL2a).  Please go back and try again.</p>");
}

?>
</body>

</html>
