<?php

class STIME extends SDBP5B_HELPER {

	const ACTIVE = 1;

	private $time_settings;

	public function __construct() {
		parent::__construct();
		$this->time_settings = array(
			'TOP'=>array(3,6,9,12),
			'ALL'=>array(1,2,3,4,5,6,7,8,9,10,11,12),
		);
	}


	public function getAllTime($key="end") {
		$time = $this->getMyTime("ALL",$key);
		return $time;
	}


	public function getMyTime($section,$key="end") {
		$time_ids = $this->getTimeIDs($section);
		if(count($time_ids)>0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id IN (".implode(",",$time_ids).") AND active & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY start_date";
			$t = $this->mysql_fetch_all($sql);
			$time = $this->processTime($t,$key);
		} else {
			$time = array();
		}
		return $time;
	}

	public function getTopTime() {
		$t1 = $this->getAllTime();
		$time = array();
		foreach($t1 as $i => $t) {
			switch($t['id']) {
			case 3: case 6: case 9: case 12:
				$t['start'] = $start;
				$time[$i] = $t;
				break;
			case 1: case 4: case 7: case 10:
				$start = $t['start'];
				break;
			default:
				break;
			}
		}
		return $time;
	}



	private function getTimeIDs($s) {
		if(isset($this->time_settings[$s])) {
			return $this->time_settings[$s];
		} else {
			return $this->time_settings['ALL'];
		}
	}
	private function processTime($t,$key="end") {
		$time = array();
		foreach($t as $x) {
			$end = strtotime($x['end_date']);
			$start = strtotime($x['start_date']);
			$display = date("F Y",$end);
			if($key=="id") { $i = $x['id']; } else { $i = $end; }
			$time[$i] = array(
				'id'				=> $x['id'],
				'start'				=> $start,
				'end'				=> $end,
				'display'			=> $display,
				'active_primary'	=> ( ($x['active_primary'] & STIME::ACTIVE ) == STIME::ACTIVE),
				'active_secondary'	=> ( ($x['active_secondary'] & STIME::ACTIVE ) == STIME::ACTIVE),
				'active_tertiary'	=> (isset($x['active_tertiary']) ? ( ($x['active_tertiary'] & STIME::ACTIVE ) == STIME::ACTIVE) : false),
				'active_finance'	=> ( ($x['active_finance'] & STIME::ACTIVE ) == STIME::ACTIVE),
				'close_primary'		=> $x['close_primary'],
				'close_secondary'	=> $x['close_secondary'],
				'close_tertiary'	=> (isset($x['close_tertiary']) ? $x['close_tertiary'] : 0),
				'close_finance'		=> $x['close_finance'],
			);
		}
		return $time;
	}


	public function getOpenTime($category="",$options="") {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE start_date < now() AND active_".$category." & ".STIME::ACTIVE." = ".STIME::ACTIVE." ".(strlen($options)>0 ? " AND (".$options.")" : "");
		//echo $sql;
		$rows =  $this->mysql_fetch_all_by_id($sql,"id");
		$time = $this->processTime($rows,"id");
		return $time;
	}

	public function getTimeByID($t) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id = ".$t;
		$row[$t] = $this->mysql_fetch_one($sql);
		$ret = $this->processTime($row,"id");
		return $ret[$t];
	}

}


?>