<?php
/**
 * To manage the functions required by external modules
 *
 * Created on: 10 June 2018
 * Authors: Janet Currie
 *
 */

class SDBP5B_EXTERNAL extends ASSIST_MODULE_HELPER {

	private $local_modref;
	private $sourceObject;
	private $object_type;

    public function __construct($modref="",$object_type="",$create_source_object=false) {
        parent::__construct("client","",false,$modref);
		$this->local_modref = $modref;
		$this->object_type = $object_type;
		if(strlen($object_type)>0 && $create_source_object===true) {
			switch($object_type) {
				case SDBP5B_CAPITAL::OBJECT_TYPE:
					$this->sourceObject = new SDBP5B_CAPITAL($this->local_modref,true);
					break;
				case SDBP5B_TOP::OBJECT_TYPE:
					$this->sourceObject = new SDBP5B_TOP($this->local_modref,true);
					break;
				case SDBP5B_DEPT::OBJECT_TYPE:
					$this->sourceObject = new SDBP5B_DEPT($this->local_modref,true);
					break;
			}
		}
    }




	public function replaceExternalNames($a) {
		$idpObject = new SDBP5B($this->local_modref,true);
		$a = $idpObject->replaceAllNames($a);
		return $a;
	}





	public function getSourceRecords() {
		$records = $this->sourceObject->getRawObjectsForExternalModule();
		return $records;
	}


	public function getRefTag() {
    	return $this->sourceObject->getRefTag();
	}



	public function getListItemsForSelect() {
    	return $this->sourceObject->getListItemsForSelect();
	}























	public function __destruct() {
		parent::__destruct();
	}
}


?>