<?php

class TOP extends KPI {

	protected $section = "TOP";

	protected $section_head = "Top Layer SDBIP";

	protected $object_name = "KPI";
	protected $object_title = "Top Layer KPI";
	
	protected $table_field = "top_";
	protected $table = "top";
	protected $id_field = "top";
	protected $detail_link = "top_value";
	protected $results_table_field = "tr_";
	protected $target_field = "tr_target";
	protected $actual_field = "tr_actual";
	protected $wa_table_field = array('wards'=>"tw",'area'=>"ta","top_wards"=>"tw","top_area"=>"ta");
	protected $time_ids = array(3,6,9,12);
	protected $first_month = 3;
	protected $time_increment = 3;
	
	protected $absolute_required_fields;

	const REFTAG = "TL";
	
	const HEAD = "TOP";
	const RHEAD = "TOP_R";
	
	public function __construct() {
		parent::__construct(self::HEAD,self::RHEAD);
		$this->absolute_required_fields = array("top_dirid","dir","top_wards","top_area","top_ownerid","top_calctype","top_targettype");
	}

	public function saveUpdate($var,$old) {
		$obj_id = $var['obj_id'];
		$time_id = $var['time_id'];
		$section = $var['section'];
		$actual = $var['tr_actual'];
		$perf = $this->code($var['tr_perf']);
		$correct = $this->code($var['tr_correct']);
		$poe = isset($var['poe']) ? $this->code($var['poe']) : "";
		$attachment = array('poe'=>$poe,'attach'=>array());
			$attachment['attach'] = isset($old['attachment']['attach']) ? $old['attachment']['attach'] : array();
			foreach($var['new_attach'] as $key => $a) {
				if(!isset($attachment['attach'][$key])) {
					$attachment['attach'][$key] = $a;
				} else {
					$attachment['attach'][] = $a;
				}
			}

		$headings = $this->getModuleHeadings(array("TOP_R"));
		$object = $this->getObject($obj_id);
		$time = $this->getTime(array($time_id));
		$result = array(0=>"ok",1=>"Update to ".$this->section_details['code'].$obj_id." saved successfully.");
		//$old = $this->getUpdateRecord($obj_id,$time_id);
		
		//validate change
		if($old['tr_actual']!=$actual
			|| $old['tr_perf']!=$perf
			|| $old['tr_correct']!=$correct
			|| $old['attachment']['poe']!=$attachment['poe']
			|| $old['attachment']['attach']!=$attachment['attach']
		) {
			$tr_auto = (($old['tr_actual']!=$actual) ? "0" : $old['tr_auto']);
			//update result
			//$attachment['attach'] = isset($old['attachment']['attach']) ? $old['attachment']['attach'] : array();
			$attach = serialize($attachment);
			$lsql = "UPDATE ".$this->getDBRef()."_top_results SET
				tr_actual = ".$actual.",
				tr_perf = '".$perf."',
				tr_correct = '".$correct."',
				tr_attachment = '".$attach."',
				tr_update = 1,
				tr_auto = ".$tr_auto.",
				tr_updateuser = '".$this->getUserID()."',
				tr_updatedate = now()
				WHERE tr_topid = ".$obj_id." AND tr_timeid = ".$time_id;
			$mar = $this->db_update($lsql);
			//log update
			$log = array();
			if($old['tr_actual']!=$actual) {
				$fld = "tr_actual";
				$new = $actual;
				$n = $this->KPIresultDisplay($actual,$object['top_targettype']);
				$o = $this->KPIresultDisplay($old[$fld],$object['top_targettype']);
				$h = $headings['TOP_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['tr_perf']!=$perf) {
				$fld = "tr_perf";
				$new = $perf;
				$n = $this->decode($perf);
				$o = $this->decode($old[$fld]);
				$h = $headings['TOP_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['tr_correct']!=$correct) {
				$fld = "tr_correct";
				$new = $correct;
				$n = $this->decode($correct);
				$o = $this->decode($old[$fld]);
				$h = $headings['TOP_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['attachment']['poe']!=$attachment['poe']) {
				$fld = "tr_attachment_poe";
				$new = $poe;
				$n = $this->decode($poe);
				$o = $this->decode($old['attachment']['poe']);
				$h = $headings['TOP_R']['tr_attachment'];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old['attachment']['poe'],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['attachment']['attach']!=$attachment['attach']) {
				$fld = "tr_attachment_files";
				$new = $var['new_attach'];
				$new_files = array();
				foreach($new as $a => $x) { $new_files[] = $x['original_filename']; }
				$n = implode(", ",$new_files);
				$old_files = $old['attachment']['attach'];
				$h = $headings['TOP_R']['tr_attachment'];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>serialize($new),
					'old'=>serialize($old_files),
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['tr_auto']!=$tr_auto) {
				$fld = "tr_auto";
				$new = $tr_auto;
				$n = $tr_auto==0 ? "Locked" : "Unlocked";
				$o = $old['tr_auto']==0 ? "Locked" : "Unlocked";
				$h = "Auto Update";
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".($h)." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			foreach($log as $v) {
				$this->logChanges($section,$obj_id,$v,$this->code($lsql));
			}
		} else {
			$result[0] = "info";
			$result[1] = "No change found to be saved.";
		}
		//return result*/
		return $result;
	}
	
	public function saveUpdateAttachments($i,$t,$new,$old) {
		$headings = $this->getModuleHeadings(array("TOP_R"));
		$time = $this->getTime(array($t));
		$section = $this->section;
		
		$attach = array();
		$old_files = array();
		foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
		$new_files = array();
		foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }
		
		$update = array('poe'=>$old['poe'],'attach'=>$attach);
		$sql = "UPDATE ".$this->getDBRef()."_top_results SET tr_attachment = '".serialize($update)."' WHERE tr_topid = ".$i." AND tr_timeid = ".$t;
		$this->db_update($sql);
		
		$fld = "tr_attachment_files";
		$n = implode(", ",$new_files);
		$o = implode(", ",$old_files);
		$h = $headings['TOP_R']['tr_attachment'];
		$log = array(
			'fld'=>$fld,
			'new'=>serialize($new),
			'old'=>serialize($old['attach']),
			'act'=>"U",
			'YN'=>"Y",
			'timeid'=>$t,
			'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$t]['display_short'])
		);
		$this->logChanges($section,$i,$log,$this->code($sql));
	}
	
	
	public function deleteAttachment($kpi_id,$time_id,$attach_id) {
		$section = $this->section;
		$attach = $this->getAttachmentDetails($kpi_id,$time_id,$attach_id);
		//$this->arrPrint($attach);
		if($attach[0]===true) {
			$this->checkFolder($this->getAttachmentDeleteFolder());
			$old_path = "../../files/".$this->getCmpCode()."/".$this->getAttachFolder()."/".$attach['system_filename'];
			$new_path = "../../files/".$this->getCmpCode()."/".$this->getAttachmentDeleteFolder()."/".$section."_".date("YmdHis")."_".$attach['system_filename'];
			if(copy($old_path,$new_path)) {
				unlink($old_path);
			}
			$record = $attach[2]; //$this->getUpdateRecord($kpi_id,$time_id);
			$a = $record['attachment'];
			$old_attach = $a['attach'][$attach_id]['original_filename'];
			unset($a['attach'][$attach_id]);

if(isset($a['poe'])) {
	$a['poe'] = $this->code($a['poe']);
}

			$n = serialize($a);
			$sql = "UPDATE ".$this->getDBRef()."_top_results SET tr_attachment = '$n' WHERE tr_topid = ".$kpi_id." AND tr_timeid = ".$time_id." ";
			$this->db_update($sql);
			
			$headings = $this->getModuleHeadings(array("TOP_R"));
			$time = $this->getTime(array($time_id));
			
			$old = $record;
			$new = $a;
			
			$attach = array();
			$old_files = array();
			foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
			$new_files = array();
			foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }
			
			$fld = "tr_attachment_files";
			$n = implode(", ",$new_files);
			$o = implode(", ",$old_files);
			$h = $headings['TOP_R']['tr_attachment'];
			$log = array(
				'fld'=>$fld,
				'new'=>serialize($new),
				'old'=>serialize($old['attach']),
				'act'=>"U",
				'YN'=>"Y",
				'timeid'=>$time_id,
				'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '".$old_attach."' to time period ".$time[$time_id]['display_short'])
			);
			$this->logChanges($section,$kpi_id,$log,$this->code($sql));
			return true;
		} else {
			return false;
			
		}
		
	}
	
	
	
	function unlockAuto($obj_id,$time_id) {
		$time = $this->getTime(array($time_id));
		$old = $this->getUpdateRecord($obj_id,$time_id);

		$result = array("error","An error occurred while attempted to unlock Time Period ".$time[$time_id]['display_short']);

		$tr_auto = 1;
		$old_tr_auto = $old['tr_auto'];
		
		if($tr_auto!=$old_tr_auto) {
		
			$lsql = "UPDATE ".$this->getDBRef()."_top_results SET tr_auto = ".$tr_auto." WHERE tr_topid = ".$obj_id." AND tr_timeid = ".$time_id." ";
			$this->db_update($lsql);
		
			if($old['tr_auto']!=$tr_auto) {
				$fld = "tr_auto";
				$new = $tr_auto;
				$n = $tr_auto==0 ? "Locked" : "Unlocked";
				$o = $old_tr_auto==0 ? "Locked" : "Unlocked";
				$h = "Auto Update";
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".($h)." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			foreach($log as $v) {
				$this->logChanges($this->section,$obj_id,$v,$this->code($lsql));
			}
			
			$result = array("ok","Time Period ".$time[$time_id]['display_short']." unlocked for automatic updating.");
			
		} else {
			$result = array("info","Time period could not be unlocked.  Check that it has not already been unlocked.");
		}
		return $result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**********************************
		GET FUNCTIONS
	*************************************/
	
	public function getUpdateFormDetails($id) {
		$data = array();
		//$data = $this->getUpdateDetailsArray();
		$sql = "SELECT top_value, top_unit, top_poe, top_targettype, top_calctype, d.value as dir, ct.value as ct 
		, count(o.kpi_id) as assoc, GROUP_CONCAT(DISTINCT o.kpi_calctype ORDER BY o.kpi_calctype SEPARATOR '|') as assoc_calctype, GROUP_CONCAT(DISTINCT o.kpi_targettype ORDER BY o.kpi_targettype SEPARATOR '|') as assoc_targettype
		FROM ".$this->getDBRef()."_top k
				INNER JOIN ".$this->getDBRef()."_dir d
				  ON k.top_dirid = d.id
				INNER JOIN ".$this->getDBRef()."_list_calctype ct
				  ON ct.code = k.top_calctype
				LEFT OUTER JOIN ".$this->getDBRef()."_kpi o
				  ON o.kpi_topid = k.top_id
				WHERE top_id = ".$id;
		$row = $this->mysql_fetch_one($sql);
		$data['dir'] = ($row['dir']);
		$data['assoc'] = ($row['assoc']);
		$data['value'] = ($row['top_value']);
		$data['unit'] = ($row['top_unit']);
		$data['poe'] = ($row['top_poe']);
		$data['calctype'] = ($row['top_calctype']);
		$data['ct'] = ($row['ct'])." (".$data['calctype'].")";
		$data['targettype'] = ($row['top_targettype']);
		$data['target_formatting'] = array('prefix'=>"",'postfix'=>"");
		switch($data['targettype']) {
			case 1:
				//type = currency, add R as prefix
				$data['target_formatting']['prefix'] = "R&nbsp;";
				break;
			case 2:
				//type = percent, add % as postfix
				$data['target_formatting']['postfix'] = "&nbsp;%";
				break;
			case 3:
			default:
				//type = number, no formatting applied
				break;
		}

		//$sql = "SELECT * FROM ".$this->getDBRef()."_top_results WHERE tr_topid = ".$id." ORDER BY tr_timeid";
		
		$auto = "N";
		if($data['assoc']>0) {
			$auto_ct = explode("|",$row['assoc_calctype']);
			$auto_tt = explode("|",$row['assoc_targettype']);
			if((count($auto_ct)>1 || !in_array($row['top_calctype'],$auto_ct) || count($auto_tt)>1 || !in_array($row['top_targettype'],$auto_tt))) {
				$auto = "warn";
			} else {
				$auto = "Y";
			}
		}
		
		$sql = "SELECT kr.*, CONCAT(tk.tkname,' ',tk.tksurname) as updateuser 
				FROM ".$this->getDBRef()."_top_results kr
				LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep tk
				  ON tk.tkid = kr.tr_updateuser
				WHERE kr.tr_topid = ".$id." ORDER BY kr.tr_timeid";
		$results = $this->mysql_fetch_all_by_id($sql,"tr_timeid");     
		$targets = array();
		$actuals = array();
		for($ti=3;$ti<=12;$ti+=3) {
			if(isset($results[$ti])) {
				$r = $results[$ti];
/***************************************
* START OF HACK TO GET AROUND bug IN serialize FUNCTION RELATING TO " ' : ; CHARACTERS IN STRINGS
****************************************
if(strlen($r['tr_attachment'])>0) {
		//get kr_attachment string
	$w = $r['tr_attachment'];
		//replace double quotes " with a double pipe place holder
	$x = str_replace("&quot;","||",$w);
		//restore HTML tags
	$x = $this->decode($x);

		//break up the serialized array based on the semi-colon
	$y = explode(";",$x);
		//temp store the POE string portion
	$z = $y[1];
		//replace POE string portion with a blank string
	$y[1] = 's:0:""';
		//put serialized array back together
	$r['tr_attachment'] = implode(";",$y);
		//break up the POE temp string on the serialized " placeholders
	$x = explode('"',$z);
		//replace the double pipe with the original double quotes character
	$y = str_replace('||','"',$x[1]);
//	$y = $this->code($y);
		//unserialize the serialized array with blank POE string place holder
	$r['tr_attachment'] = unserialize($r['kr_attachment']);
		//replace the blank temp POE string with the original
	$r['tr_attachment']['poe'] = $y;
} else {
	$r['tr_attachment'] = array('poe'=>"",'attach'=>array());
}
/***************************************
* END OF serialize HACK
****************************************/


				$r['tr_attachment'] = $this->processPOEserializedArray($r['tr_attachment']);
				$data['results']['target'][$ti] = $r['tr_target'];			$targets[$ti] = $r['tr_target'];
				$data['results']['actual'][$ti] = $r['tr_actual'];			$actuals[$ti] = $r['tr_actual'];
				$data['results']['auto_update'][$ti] = $r['tr_auto'];
				$data['results']['perfcomm'][$ti] = ($r['tr_perf']);
				$data['results']['correct'][$ti] = ($r['tr_correct']);
				$data['results']['poe'][$ti] = (isset($r['tr_attachment']['poe']) ? $r['tr_attachment']['poe'] : "");
				$data['results']['attach'][$ti] = array();
				if(is_array($r['tr_attachment']['attach'])) {
					foreach($r['tr_attachment']['attach'] as $key=>$a) {
						$data['results']['attach'][$ti][$id."_".$ti."_".$key] = array(
							'original_filename'=>$a['original_filename'],
							'system_filename'=>$a['system_filename'],
						);
					}
				}
				$data['results']['update'][$ti] = $r['tr_update'];
				$data['results']['updateuser'][$ti] = ($r['tr_updateuser']=="IA" || $r['tr_updateuser']=="AUTO" ? "Auto Update" : $r['updateuser']);
				$data['results']['updatedate'][$ti] = $r['tr_updatedate'];
				$data['results']['auto'][$ti] = ($auto=="Y" && $r['tr_auto']==0 ? "lock" : $auto);
				$data['results']['tr_auto'][$ti] = ($r['tr_auto']);
			}
			$data['results']['r'][$ti] = $this->KPIcalcResult(array('target'=>$targets,'actual'=>$actuals),$data['calctype'],array(1,$ti),$ti);
			$data['results']['ytd_r'][$ti] = $this->KPIcalcResult(array('target'=>$targets,'actual'=>$actuals),$data['calctype'],array(1,$ti),"ALL");
			$data['results']['target'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['target'],$data['targettype']);
			$data['results']['actual'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['actual'],$data['targettype']);
			$data['results']['ytd_target'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['target'],$data['targettype']);
			$data['results']['ytd_actual'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['actual'],$data['targettype']);
		}
		return $data;
		//return array("abc");
	}
		

	
	
	
	
	
	

}



?>