<?php

class SDBP5_TOP extends SDBP5B_HELPER {

	private $sdbp5;
	private $section = "TOP";
	private $section_details;

	const DELETED = 0;
	const ACTIVE = 1;

	const REFTAG = "TL";

	const HEAD = "TOP";
	const RHEAD = "TOP_R";

	public function __construct() {
		parent::__construct();
		$this->sdbp5 = new SDBP5();
		$this->section_details = $this->sdbp5->getSection($this->section);
		$this->setAttachmentDownloadOptions("section=TOP");
		$this->setAttachmentDeleteOptions("section=TOP");
		$this->setHelperAttachmentFolder($this->getAttachFolder());
	}

	public function saveUpdate($var) {
		$obj_id = $var['obj_id'];
		$time_id = $var['time_id'];
		$section = $var['section'];
		$actual = $var['tr_actual'];
		$perf = $this->code($var['tr_perf']);
		$correct = $this->code($var['tr_correct']);
		$poe = isset($var['poe']) ? $this->code($var['poe']) : "";
		$attachment = array('poe'=>$poe,'attach'=>array());

		$headings = $this->getModuleHeadings(array("TOP_R"));
		$object = $this->getObject($obj_id);
		$time = $this->getTime(array($time_id));
		$result = array(0=>"ok",1=>"Update saved for ".$this->section_details['code'].$obj_id);
		$old = $this->getUpdateRecord($obj_id,$time_id);

		//validate change
		if($old['tr_actual']!=$actual
			|| $old['tr_perf']!=$perf
			|| $old['tr_correct']!=$correct
			|| $old['attachment']['poe']!=$attachment['poe']
		) {
			//update result
			$attachment['attach'] = isset($old['attachment']['attach']) ? $old['attachment']['attach'] : array();
			$attach = serialize($attachment);
			$lsql = "UPDATE ".$this->getDBRef()."_top_results SET
				tr_actual = ".$actual.",
				tr_perf = '".$perf."',
				tr_correct = '".$correct."',
				tr_attachment = '".$attach."'
				WHERE tr_topid = ".$obj_id." AND tr_timeid = ".$time_id;
			$mar = $this->db_update($lsql);
			//log update
			$log = array();
			if($old['tr_actual']!=$actual) {
				$fld = "tr_actual";
				$new = $actual;
				$n = $this->KPIresultDisplay($actual,$object['top_targettype']);
				$o = $this->KPIresultDisplay($old[$fld],$object['top_targettype']);
				$h = $headings['TOP_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['tr_perf']!=$perf) {
				$fld = "tr_perf";
				$new = $perf;
				$n = $this->decode($perf);
				$o = $this->decode($old[$fld]);
				$h = $headings['TOP_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['tr_correct']!=$correct) {
				$fld = "tr_correct";
				$new = $correct;
				$n = $this->decode($correct);
				$o = $this->decode($old[$fld]);
				$h = $headings['TOP_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['attachment']['poe']!=$attachment['poe']) {
				$fld = "tr_attachment_poe";
				$new = $poe;
				$n = $this->decode($poe);
				$o = $this->decode($old['attachment']['poe']);
				$h = $headings['TOP_R']['tr_attachment'];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old['attachment']['poe'],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			foreach($log as $v) {
				$this->logChanges($section,$obj_id,$v,$this->code($lsql));
			}
		} else {
			$result[0] = "info";
			$result[1] = "No change found to be saved.";
		}
		//return result*/
		return $result;
	}

	public function saveUpdateAttachments($i,$t,$new,$old) {
		$headings = $this->getModuleHeadings(array("TOP_R"));
		$time = $this->getTime(array($t));
		$section = $this->section;

		$attach = array();
		$old_files = array();
		foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
		$new_files = array();
		foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }

		$update = array('poe'=>$old['poe'],'attach'=>$attach);
		$sql = "UPDATE ".$this->getDBRef()."_top_results SET tr_attachment = '".serialize($update)."' WHERE tr_topid = ".$i." AND tr_timeid = ".$t;
		$this->db_update($sql);

		$fld = "tr_attachment_files";
		$n = implode(", ",$new_files);
		$o = implode(", ",$old_files);
		$h = $headings['TOP_R']['tr_attachment'];
		$log = array(
			'fld'=>$fld,
			'new'=>serialize($new),
			'old'=>serialize($old['attach']),
			'act'=>"U",
			'YN'=>"Y",
			'timeid'=>$t,
			'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$t]['display_short'])
		);
		$this->logChanges($section,$i,$log,$this->code($sql));
	}


	public function deleteAttachment($kpi_id,$time_id,$attach_id) {
		$section = $this->section;
		$attach = $this->getAttachmentDetails($kpi_id,$time_id,$attach_id);
		//$this->arrPrint($attach);
		if($attach[0]===true) {
			$this->checkFolder($this->getAttachmentDeleteFolder());
			$old_path = "../../files/".$this->getCmpCode()."/".$this->getAttachFolder()."/".$attach['system_filename'];
			$new_path = "../../files/".$this->getCmpCode()."/".$this->getAttachmentDeleteFolder()."/".$section."_".date("YmdHis")."_".$attach['system_filename'];
			if(copy($old_path,$new_path)) {
				unlink($old_path);
			}
			$record = $attach[2]; //$this->getUpdateRecord($kpi_id,$time_id);
			$a = $record['attachment'];
			unset($a['attach'][$attach_id]);
			$n = serialize($a);
			$sql = "UPDATE ".$this->getDBRef()."_top_results SET tr_attachment = '$n' WHERE tr_topid = ".$kpi_id." AND tr_timeid = ".$time_id." ";
			$this->db_update($sql);

			$headings = $this->getModuleHeadings(array("TOP_R"));
			$time = $this->getTime(array($time_id));

			$old = $record;
			$new = $a;

			$attach = array();
			$old_files = array();
			foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
			$new_files = array();
			foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }

			$fld = "tr_attachment_files";
			$n = implode(", ",$new_files);
			$o = implode(", ",$old_files);
			$h = $headings['TOP_R']['tr_attachment'];
			$log = array(
				'fld'=>$fld,
				'new'=>serialize($new),
				'old'=>serialize($old['attach']),
				'act'=>"U",
				'YN'=>"Y",
				'timeid'=>$t,
				'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$time_id]['display_short'])
			);
			$this->logChanges($section,$i,$log,$this->code($sql));

		}

	}




















































	/**********************************
		GET FUNCTIONS
	*************************************/

	public function getAttachFolder() {
		return $this->getModRef()."/".$this->section;
	}


	public function getObject($i) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_top WHERE top_id = $i";
		return $this->mysql_fetch_one($sql);
	}

	public function getUpdateFormDetails($id) {
		$data = $this->getUpdateDetailsArray();
		$sql = "SELECT top_value, top_unit, top_poe, top_targettype, top_calctype, d.value as dir, ct.value as ct , count(o.kpi_id) as assoc
				FROM ".$this->getDBRef()."_top k
				INNER JOIN ".$this->getDBRef()."_dir d
				  ON k.top_dirid = d.id
				INNER JOIN ".$this->getDBRef()."_list_calctype ct
				  ON ct.code = k.top_calctype
				LEFT OUTER JOIN ".$this->getDBRef()."_kpi o
				  ON o.kpi_topid = k.top_id
				WHERE top_id = ".$id;
		$row = $this->mysql_fetch_one($sql);
		$data['dir'] = $this->decode($row['dir']);
		$data['assoc'] = $this->decode($row['assoc']);
		$data['value'] = $this->decode($row['top_value']);
		$data['unit'] = $this->decode($row['top_unit']);
		$data['poe'] = $this->decode($row['top_poe']);
		$data['calctype'] = $this->decode($row['top_calctype']);
		$data['ct'] = $this->decode($row['ct'])." (".$data['calctype'].")";
		$data['targettype'] = $this->decode($row['top_targettype']);
		$sql = "SELECT * FROM ".$this->getDBRef()."_top_results WHERE tr_topid = ".$id." ORDER BY tr_timeid";
		$results = $this->mysql_fetch_all_by_id($sql,"tr_timeid");
		$targets = array();
		$actuals = array();
		for($ti=1;$ti<=12;$ti++) {
			if(isset($results[$ti])) {
				$r = $results[$ti];
//				$r['tr_attachment'] = unserialize($r['tr_attachment']);
/***************************************
* START OF HACK TO GET AROUND bug IN serialize FUNCTION RELATING TO " ' : ; CHARACTERS IN STRINGS
****************************************/
if(strlen($r['tr_attachment'])>0) {
		//get kr_attachment string
	$w = $r['tr_attachment'];
		//replace double quotes " with a double pipe place holder
	$x = str_replace("&quot;","||",$w);
		//restore HTML tags
	$x = $this->decode($x);

		//break up the serialized array based on the semi-colon
	$y = explode(";",$x);
		//temp store the POE string portion
	$z = $y[1];
		//replace POE string portion with a blank string
	$y[1] = 's:0:""';
		//put serialized array back together
	$r['tr_attachment'] = implode(";",$y);
		//break up the POE temp string on the serialized " placeholders
	$x = explode('"',$z);
		//replace the double pipe with the original double quotes character
	$y = str_replace('||','"',$x[1]);
//	$y = $this->code($y);
		//unserialize the serialized array with blank POE string place holder
	$r['tr_attachment'] = unserialize($r['kr_attachment']);
		//replace the blank temp POE string with the original
	$r['tr_attachment']['poe'] = $y;
} else {
	$r['tr_attachment'] = array('poe'=>"",'attach'=>array());
}
/***************************************
* END OF serialize HACK
****************************************/


				$data['results']['target'][$ti] = $r['tr_target'];
				$data['results']['actual'][$ti] = $r['tr_actual'];
				$data['results']['auto_update'][$ti] = $r['tr_auto'];
				$data['results']['perfcomm'][$ti] = $this->decode($r['tr_perf']);
				$data['results']['correct'][$ti] = $this->decode($r['tr_correct']);
				$data['results']['poe'][$ti] = $this->decode($r['tr_attachment']['poe']);
				$data['results']['attach'][$ti] = array();
				foreach($r['tr_attachment']['attach'] as $key=>$a) {
					$data['results']['attach'][$ti][$id."_".$ti."_".$key] = array(
						'original_filename'=>$a['original_filename'],
						'system_filename'=>$a['system_filename'],
					);
				}
			}
			$data['results']['r'][$ti] = $this->KPIcalcResult(array('target'=>$data['results']['target'],'actual'=>$data['results']['actual']),$data['calctype'],array(1,$ti),$ti);
			$data['results']['ytd_r'][$ti] = $this->KPIcalcResult(array('target'=>$data['results']['target'],'actual'=>$data['results']['actual']),$data['calctype'],array(1,$ti),"ALL");
			$data['results']['target'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['target'],$data['targettype']);
			$data['results']['actual'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['actual'],$data['targettype']);
			$data['results']['ytd_target'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['target'],$data['targettype']);
			$data['results']['ytd_actual'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['actual'],$data['targettype']);
		}
		return $data;
	}


	function getAttachmentDetails($kpi_id,$time_id,$attach_id) {
		$record = $this->getUpdateRecord($kpi_id,$time_id);
		$attachments = $record['attachment']['attach'];
		if(isset($attachments[$attach_id])) {
			$a = $attachments[$attach_id];
			$a[0] = true;
			$a[2] = $record;
			return $a;
		} else {
			return array(0=>false,1=>$attachments,2=>$record);
		}
	}




	public function getUpdateRecord($i,$t) {
		$data = array();
		$sql = "SELECT * FROM ".$this->getDBRef()."_top_results WHERE tr_topid = $i AND tr_timeid = $t ";
		$data = $this->mysql_fetch_one($sql);
		$p = $data['tr_attachment'];
		if(strlen($p)>0) {
			$p = unserialize($p);
		} else {
			$p = array('poe'=>"",'attach'=>array());
		}
		$data['attachment'] = $p;
		return $data;
	}


	function getAssociatedKPIs($i) {
		$sql = "SELECT kpi_id as id, kpi_value as name FROM ".$this->getDBRef()."_kpi WHERE kpi_topid = ".$i." AND kpi_active & ".SDBP5_DEPT::ACTIVE." = ".SDBP5_DEPT::ACTIVE;
		$kpis = $this->mysql_fetch_all($sql);

		$objects = array();

		foreach($kpis as $k) {
			$objects[$k['id']] = array('id'=>$k['id'],'name'=>$k['name'],'results'=>array());
		}

		$ids = array_keys($objects);

		if(count($ids)>0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_kpi_results WHERE kr_kpiid IN (".implode(",",$ids).") ORDER BY kr_kpiid, kr_timeid";
			$r = $this->mysql_fetch_all($sql);
			foreach($r as $x) {
				$i = $x['kr_kpiid'];
				$t = $x['kr_timeid'];
				$objects[$i]['results'][$t] = $x;
			}
		}

		return $objects;

	}






}



?>