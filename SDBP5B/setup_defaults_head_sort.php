<?php
global $helperObj;
$log_section = "HEAD_".$section;
$my_head = isset($mheadings[$section]) ? $mheadings[$section] : array();
foreach($unset as $u) {
	unset($my_head[$u]);
}


if(isset($_REQUEST['act']) && $_REQUEST['act'] == "SAVE") {
	$sql = "SELECT h.*, s.h_ignite, s.h_client FROM ".$dbref."_setup_headings_setup h INNER JOIN ".$dbref."_setup_headings s ON s.h_id = h.head_id WHERE h.section = '$section'";
	$olds = $helperObj->mysql_fetch_all_fld($sql,'field');
	$sort_req = isset($_REQUEST['sort']) ? $_REQUEST['sort']  : array();
	foreach($sort_req as $sort => $fld) {
		$c_required = "false";
		$c_sort = $sort;
		$c_list = "false";
		$glossary = "";
		//$f = $fld."_required";	if(isset($_REQUEST[$f]) && $_REQUEST[$f] == "on") { $c_required = "true"; }
		//$f = $fld."_list"; 		if(isset($_REQUEST[$f]) && $_REQUEST[$f] == "on") { $c_list = "true"; }
		//$f = $fld."_glossary";	$glossary = code($_REQUEST[$f]);
		$sql = "UPDATE ".$dbref."_setup_headings_setup SET c_sort = $c_sort WHERE section = '$section' AND field = '$fld'";
		$mar = $helperObj->db_update($sql);
		if($mar>0) {
			$change = array();
/*			//c_Required
			if( (!$olds[$fld]['c_required'] && $c_required != "false") || ($olds[$fld]['c_required'] && $c_required!="true") ) { 
				switch($c_required) {
					case "true": $change[] = " - Marked as a required field."; break;
					case "false": $change[] = " - Unmarked as a required field."; break;
				}
			}*/
			//c_sort
			if($olds[$fld]['c_sort'] != $c_sort) {
				$change[] = " - Changed display position to ".$c_sort." from ".$olds[$fld]['c_sort'].".";
			}
/*			//c_list
			if( (!$olds[$fld]['c_list'] && $c_list != "false") || ($olds[$fld]['c_list'] && $c_list!="true") ) { 
				switch($c_list) {
					case "true": $change[] = " - Added to List view."; break;
					case "false": $change[] = " - Removed from List view."; break;
				}
			}
			if($olds[$fld]['glossary']!=$glossary) {
				$change[] = " - Updated Glossary explanation.";
			}*/
			$text = "Updated ".$section_name." heading <i>".(strlen($olds[$fld]['h_client'])>0 ? $olds[$fld]['h_client'] : $olds[$fld]['h_ignite'])."</i>:<br />".implode("<br />",$change);
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$olds[$fld]['id'],
											'field'=>"",
											'text'=>$text,
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
		}
	}
	$mheadings = $helperObj->getModuleHeadings(array($section));
	$my_head = isset($mheadings[$section]) ? $mheadings[$section] : array();
    $helperObj->displayResult($result);
}
?>
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; float: left; margin-right: 10px; list-style-image: url(); width: 100% }
	#sortable li { margin: 0 5px 5px 5px; padding: 5px; }
	.ui-state-highlight { height: 15px; }
	li { cursor: hand; }
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({
			placeholder: "ui-state-highlight"
		}).disableSelection();
		$("tr").off("mouseenter mouseleave");
	});
	</script>
<form name=dept action=<?php echo $self; ?> method=post>
<input type=hidden name=page_id value=<?php echo $page_id; ?> />
<input type=hidden name=act value=SAVE />
<input type=hidden name=view value=SORT />
<table id=tbl_sortme>
<thead><tr><th>Headings</th></tr></thead>
<tbody><tr><td>
<ul id="sortable">
<?php 
	foreach($my_head as $key => $h) {
		echo "<li class='ui-state-default'><input type=hidden name=sort[] value='".$key."' />".$h['h_client']."</li>";
	}
?>
</ul>
</td></tr></tbody>
<tfoot><tr><td colspan=1 class=centre><input type=submit value="  Save  " class=isubmit /></td></tr></tfoot>
</table>
</form>
	<script>
	$(function() {
		//$( "#sortme tbody" ).sortable().disableSelection();
	});
	</script>
<?php 
goBack("setup.php","Back to Setup"); 
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>