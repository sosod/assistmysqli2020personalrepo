<?php 
include("inc/header.php"); 
global $helperObj;
$sections = array(
	'KPI'=>"Departmental SDBIP",
	'TOP'=>"Top Layer SDBIP",
	'CAP'=>"Capital Projects",
	'CF'=>"Monthly Cashflow",
	'RS'=>"Revenue By Source",
	'LIST'=>"Lists"
);

foreach($sections as $key => $sec) {
	$toc[] = "<a href=#".$key.">".$sec."</a>";
}

echo "<p style=\"margin-top: -10px; font-size: 7.5pt\"><a name=top></a>".implode(" | ",$toc)."</p>";


foreach($sections as $key => $sec) {
	echo "<h2><a name=".$key."></a>".$sec."</h2>";
	if($key!="LIST") {
		$gloss = 0;
		$echo = "";
			foreach($mheadings[$key] as $fld => $h) {
				if(strlen($h['glossary'])>0) {
					$gloss++;
					$echo .="<tr>";
						$echo.= "<th style=\"vertical-align: top;\" class=left>".$h['h_client'].":&nbsp;</td>";
						$echo.= "<td>".$h['glossary']."</td>";
					$echo.= "</tr>";
				}
			}
		if(!$gloss) {
			echo "<p>No definitions available.</p>";
		} else {
			echo "<table width=600px>";
			echo $echo;
			echo "</table>";
		}
		echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		if(in_array($key,array("KPI","TOP"))) {
			$mhead = $me->getModuleHeadings(array($key."_R"));
			$mhead = $mhead[$key."_R"];
			$gloss = 0;
			echo "<h3>".$sec.": Results</h3>";
			$echo = "";
			foreach($mhead as $fld => $h) {
				if(strlen($h['glossary'])>0) {
					$gloss++;
					$echo.= "<tr>";
						$echo.= "<th style=\"vertical-align: top;\" class=left>".$h['h_client'].":&nbsp;</td>";
						$echo.= "<td>".$h['glossary']."</td>";
					$echo.= "</tr>";
				}
			}
			if(!$gloss) {
				echo "<p>No definitions available.</p>";
			} else {
				echo "<table width=600px>";
				echo $echo;
				echo "</table>";
			}
			echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		}
		echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
	} else {
		$sql = "SELECT * FROM ".$dbref."_setup_headings WHERE h_type = 'LIST' AND h_table NOT IN ('dir','subdir','owner','targettype')";
		//$rs = getRS($sql);
        $rows = $helperObj->mysql_fetch_all($sql);
		//while($h = mysql_fetch_assoc($rs)) {
        foreach ($rows as $h){
			echo "<h3>".$h['h_client']."</h3>";
			?>
			<table>
				<tr>
					<th>Ref</th>
					<th>List Item</th>
					<?php if(in_array($h['h_table'],$code_lists)) { echo "<th>Code</th>"; } ?>
					<?php if($h['h_table']=="calctype") { echo "<th>Description</th>"; } ?>
				</tr>
				<?php 
				$rs2 = $helperObj->mysql_fetch_all("SELECT * FROM ".$dbref."_list_".$h['h_table']." WHERE active = true ORDER BY sort, value");
				//while($row = mysql_fetch_assoc($rs2)) {
            foreach ($rs2 as $row){
				?>
					<tr>
						<th><?php echo $row['id']; ?></th>
						<td><?php echo $row['value']; ?>&nbsp;&nbsp;</td>
						<?php if(in_array($h['h_table'],$code_lists)) { echo "<td class=centre>".$row['code']."</td>"; } ?>
					<?php if($h['h_table']=="calctype") { echo "<td>".$row['descrip']."</td>"; } ?>
					</tr>
				<?php
				}
				?>
			</table>
			<?php
			echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		}
	}
}

?>
<h3>KPI Results</h3>
<table>
	<tr>
		<th >KPI Result</th>
		<th >Code</th>
		<th >Explanation</th>
	</tr>
	<tr>
		<td>KPI Not Met</td>
		<td class=center><div style='background-color: #cc0001; color: #ffffff;'>&nbsp;R&nbsp;</div></td>
		<td>0% <= (Target &#247; Actual) < 75%</td>
	</tr>
	<tr>
		<td>KPI Almost Met</td>
		<td class=center><div style='background-color: #FE9900; color: #ffffff;'>&nbsp;O&nbsp;</div></td>
		<td>75% <= (Target &#247; Actual) < 100%</td>
	</tr>
	<tr>
		<td>KPI Met</td>
		<td class=center><div style='background-color: #009900; color: #ffffff;'>&nbsp;G&nbsp;</div></td>
		<td>(Target &#247; Actual) = 100%</td>
	</tr>
	<tr>
		<td>KPI Well Met</td>
		<td class=center><div style='background-color: #006600; color: #ffffff;'>&nbsp;G2&nbsp;</div></td>
		<td>100% < (Target &#247; Actual) < 150%</td>
	</tr>
	<tr>
		<td>KPI Extremely Well Met</td>
		<td class=center><div style='background-color: #000099; color: #ffffff;'>&nbsp;B&nbsp;</div></td>
		<td>150% <= (Target &#247; Actual)</td>
	</tr>
	<tr>
		<td>KPI Not Yet Measured</td>
		<td class=center><div style='background-color: #999999; color: #ffffff;'>&nbsp;N/A&nbsp;</div></td>
		<td>No target exists for the selected time period(s).</td>
	</tr>
</table>
<?php 
	echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
?>

</body>
</html>