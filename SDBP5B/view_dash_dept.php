<?php
	//GET DATA
		$object_sql = " ".$dbref."_".$table_tbl." o
			INNER JOIN ".$dbref."_subdir subdir ON ".$table_fld."subid = subdir.id AND subdir.active = true
			AND subdir.dirid = ".$graph_id."
			INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
			WHERE ".$table_fld."active = true ";
		
		$results_sql = "SELECT r.*, o.".$table_fld."subid as obj_subid, o.".$table_fld."calctype as obj_ct, o.".$table_fld."targettype as obj_tt FROM ".$dbref."_".$table_tbl."_results r
			INNER JOIN ".$dbref."_".$table_tbl." o ON r.".$r_table_fld.$table_id."id = o.".$table_fld."id AND o.".$table_fld."active = true
			WHERE ".$r_table_fld."timeid <= ".$filter_to." AND ".$r_table_fld.$table_id."id IN (SELECT o.".$table_fld."id FROM ".$object_sql.")";
		
		$results = $helperObj->mysql_fetch_all_fld2($results_sql,"".$r_table_fld.$table_id."id",$r_table_fld."timeid");
		
		$count = array();
		$sub_count = array();
	//CALCULATIONS
			foreach($results as $obj_id => $result_object) {
				$obj_tt = $result_object[$filter_from]['obj_tt'];
				$obj_ct = $result_object[$filter_from]['obj_ct'];
				$s_id = $result_object[$filter_from]['obj_subid'];
				$values = array('target'=>array(),'actual'=>array());
				for($ti=1;$ti<=$filter_to;$ti++) {
						$values['target'][$ti] = $result_object[$ti][$fld_target];
						$values['actual'][$ti] = $result_object[$ti][$fld_actual];
				}
				$r = KPIcalcResult($values,$obj_ct,array($filter_from,$filter_to),"ALL");
				if(!isset($count[$r['text']])) { $count[$r['text']] = 0; }
				if(!isset($sub_count[$s_id][$r['text']])) { $sub_count[$s_id][$r['text']] = 0; }
				$count[$r['text']]++;
				$sub_count[$s_id][$r['text']]++;
			}
		$total = array_sum($count);
		foreach($result_settings as $ky => $k) {
			$k['obj'] = isset($count[$k['text']]) ? $count[$k['text']] : 0;
			$val = array();
			foreach($k as $key => $s) {
				$val[] = $key.":\"".$s."\"";
			}
			$kr2[$ky] = implode(",",$val);
		}
		$chartData = "{".implode("},{",$kr2)."}";
		$sub_chartData = array();
		foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) {
			if(isset($sub_count[$s_id])) {
				$sval = explode(" ",$helperObj->decode($s['value']));
				$s_name = "";
				$s_n = "";
				foreach($sval as $sv) {
					if(strlen($s_n.$sv)>20) {
						$s_name.=$s_n."\\n";
						$s_n = "";
					}
					$s_n.=" ".$sv;
				}
				$s_name.=" ".$s_n;
				$cD = "{text:\"".$s_name."\"";
				foreach($result_settings as $ky => $k) {
					$cD.= ",R".$k['id'].":".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
				}
				$cD.= "}";
				$sub_chartData[] = $cD;
			}
		}
		$graph_width = count($sub_chartData)*100 + 100;
		?>
        <script type="text/javascript">
        var chart;
		var legend;
        var chartData = [<?php echo $chartData; ?>];
        var sub_chartData = [<?php echo implode(",",$sub_chartData) ?>];

         $(document).ready(function() {
            dir_chart = new AmCharts.AmPieChart();
			dir_chart.color = "#ffffff";
            dir_chart.dataProvider = chartData;
            dir_chart.titleField = "value";
			dir_chart.valueField = "obj";
			dir_chart.colorField = "color";
			dir_chart.labelText = "[[percents]]%";
			dir_chart.labelRadius = -25;
			dir_chart.angle = 10;
			dir_chart.depth3D = 15;
			dir_chart.hideLabelsPercent = 0;
			dir_chart.hoverAlpha = 0.5;
			dir_chart.startDuration = 0;
			dir_chart.radius = 150;
			dir_chart.marginBottom = 30;
			dir_chart.write("dir");

            sub_chart = new AmCharts.AmSerialChart();
			sub_chart.color = "#ffffff";
            sub_chart.dataProvider = sub_chartData;
            sub_chart.categoryField = "text";
            sub_chart.marginLeft = 47;
            sub_chart.marginTop = 30;
			sub_chart.marginBottom = 75;
			sub_chart.fontSize = 10;
			sub_chart.angle = 45;
			sub_chart.depth3D = 15;
			sub_chart.plotAreaBorderAlpha = 0.2;
			sub_chart.rotate = false;
 
		<?php foreach($result_settings as $ky => $k) { ?>
			var graph = new AmCharts.AmGraph();
			graph.title = "<?php echo $k['value']; ?>";
			graph.labelText="[[percents]]%";
			graph.valueField = "R<?php echo $k['id']; ?>";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "<?php echo $k['color']; ?>";
			graph.balloonText = "[[percents]]%";
			sub_chart.addGraph(graph);
		<?php } ?>
		
			var valAxis = new AmCharts.ValueAxis();
			valAxis.stackType = "regular";	//100%
			valAxis.gridAlpha = 0.1;
			valAxis.axisAlpha = 0;
			valAxis.color = "#000000";
			sub_chart.addValueAxis(valAxis);
 
			sub_chart.categoryAxis.gridAlpha = 0.1;
			sub_chart.categoryAxis.axisAlpha = 0;
			sub_chart.categoryAxis.color = "#000000";
			sub_chart.categoryAxis.fontSize = 7;
			//sub_chart.categoryAxis.labelRotation = 0;
			//sub_chart.categoryAxis.autoGridCount = true;
			//sub_chart.categoryAxis.gridCount = 1;
			sub_chart.categoryAxis.gridPosition = "start";
			
			sub_chart.write("sub");
		});
	    </script>
<div align=center>
	<table class=noborder><tr class=no-highlight>
		<td class="center noborder"><span style="font-weight: bold;"><?php echo $lists['dir'][$graph_id]['value']; ?></span>
			<div id="dir" style="width:400px; height:350px; background-color:#ffffff;"></div>
		</td>
		<td class="center noborder"><span style="font-weight: bold;"><?php echo $head_sub; ?></span>
			<div id="sub" style="width:<?php echo $graph_width; ?>px; height:400px; background-color:#ffffff;"></div>
		</td>
	</tr><tr class=no-highlight>
		<td colspan=2 class=noborder><div align=center><table>
		<thead>
			<tr>
				<td rowspan=2 style="border-top-color: #ffffff; border-left-color: #ffffff;"></td>
				<th rowspan=2><?php echo str_replace(" ","<br />",$lists['dir'][$graph_id]['value']); ?></th>
				<th colspan=<?php echo count($lists['subdir']['dir'][$graph_id]); ?>><?php echo $head_sub; ?></th>
			</tr>
			<tr>
	<?php
	foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) {
		echo "<th class=th2 style=\"font-size: 7pt;\">".str_replace(" ","<br />",$s['value'])."</th>";
	}
	?>
			</tr>
		</thead>
		<tbody>
	<?php 
	foreach($result_settings as $ky => $k) {
		echo "<tr>
				<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
				".$k['value']."</td>";
		echo "<td class=center style=\"font-weight: bold; background-color: #dddddd;\">".(isset($count[$k['text']]) ? $count[$k['text']]." (".number_format(($count[$k['text']]/array_sum($count)*100),2)."%)" : "-")."</td>";
		foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) {
			echo "<td class=center>".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']]." (".number_format(($sub_count[$s_id][$k['text']]/array_sum($sub_count[$s_id])*100),2)."%)" : "-")."</td>";
		}
		echo "</tr>";
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td class=right rowspan=2 style="background-color: #dddddd;"><b>Total:</b></td>
			<td class="center" style="background-color: #555555; color: #ffffff; font-weight: bold;"><?php echo array_sum($count); ?></td>
			<?php
			foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) {
				echo "<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? array_sum($sub_count[$s_id]) : "-")."</b></td>";
			}
			?>
		</tr>
		<tr>
			<td class="center" style="background-color: #555555; color: #ffffff; font-weight: bold;">100%</td>
			<?php
			foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) {
				echo "<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? number_format((array_sum($sub_count[$s_id])/array_sum($count)*100),2)."%" : "-")."</b></td>";
			}
			?>
		</tr>
	</tfoot>
		</table></div></td>
	</tr></table>
</div>
<?php $helperObj->displayGoBack("view_dash.php","Go Back"); ?>