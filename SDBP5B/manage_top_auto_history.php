<?php
include("inc/inc_cron.php");
global $helperObj;
?>
<h2>History of Automatic Updates</h2>
<p style="font-style: italic; margin-top: -10px;">As At <?php echo date("d M Y H:i"); ?></p>
<?php
$path = strtoupper($modref)."/STASKS/".$section;
$helperObj->checkFolder($path);
$path = "../files/$cmpcode/".$path."/";
$files = glob($path."*");
$files = array_reverse($files);
if(count($files)>0) {
	echo "<ol>";
	foreach($files as $f) {
		$fn = strFn("explode",$f,"/","");
		$file = $fn[count($fn)-1];
		$startdate = getFileDate($file);
		echo "<li><a href=# id=".$file." class=auto_hist>".date("d M Y H:i:s",$startdate)."</a>";
	}
	echo "</ol>";
} else {
	echo "<p>No history available.</p>";
}
?>
<script type=text/javascript>
$(function() {
	$("a.auto_hist").click(function() {
		i = $(this).attr("id");
		document.location.href = '<?php echo $path; ?>/'+i;
	});
});
</script>