<?php
/* Import Status */
$import = false;
$sql = "SELECT * FROM ".$dbref."_import_status";
//$rs = getRS($sql);
$import_status = array('LIST'=>false,'TOP'=>false,'CAP'=>false,'KPI'=>false,'CF'=>false,'RS'=>false);
//while($row = mysql_fetch_assoc($rs)) {
$rows = $helperObj->mysql_fetch_all($sql);
foreach ($rows as $row){
	if($row['status']) {
		$import_status[$row['value']] = true;
		if($row['value']=="LIST") { $import = true; }
	}
}
/* GET MODULE DEFAULTS */
$setup_defaults = getModuleDefaults();
if(!isset($get_time) || count($get_time)==0) {
	$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
}
$time = getTime($get_time,$get_open_time);
/* GET MODULES HEADINGS/TERMINOLOGY */
$get_headings = isset($get_headings) && is_array($get_headings) ? $get_headings : array("KPI","TOP","CAP","CF","RS");
$mheadings = getModuleHeadings($get_headings);
if(isset($mheadings['dir'])) {
    $head_dir = "Dir";
    if (array_key_exists(0,$mheadings['dir'])){
        $head_dir = strlen($mheadings['dir'][0]['h_client'])>0 ? $mheadings['dir'][0]['h_client'] : $mheadings['dir'][0]['h_ignite'];
    }
} else {
	$head_dir = "Dir";
}
if(isset($mheadings['KPI']['kpi_subid'])) {
	$h = $mheadings['KPI']['kpi_subid'];
	$head_sub = strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'];
} elseif(isset($mheadings['TOP']['top_subid'])) {
	$h = $mheadings['TOP']['top_subid'];
	$head_sub = strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'];
} elseif(isset($mheadings['CAP']['cap_subid'])) {
	$h = $mheadings['CAP']['cap_subid'];
	$head_sub = strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'];
} elseif(isset($mheadings['CF']['cf_subid'])) {
	$h = $mheadings['CF']['cf_subid'];
	$head_sub = strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'];
} else {
	$head_sub = "";
}

if(isset($mheadings['CF_R']['_7'])) {
	$mheadings['CF_R']['_7']['h_type'] = "PERC";
}
if(isset($mheadings['CF_R']['_11'])) {
	$mheadings['CF_R']['_11']['h_type'] = "PERC";
}
if(isset($mheadings['CF_R']['_15'])) {
	$mheadings['CF_R']['_15']['h_type'] = "PERC";
}

$wide_headings = array(
	"kpi_value","kpi_idpid","kpi_natoutcomeid","kpi_unit", "kpi_poe","kpi_mtas","kpi_capitalid","kpi_topid","kpi_risk",
	"top_risk","top_mtas","top_poe","top_natoutcomeid","top_idp","top_value","top_unit",
	"cap_name", "cap_descrip",
	"rs_value",
	"cf_value"
	);

/* MODULE LISTS */
//$sorted_lists = array("urgency","status","reslist1","reslist2");
$code_lists = array("munkpa","natkpa","kpitype","calctype","riskrating");
$list_types = array("LIST","WARDS","AREA","FUNDSRC","TEXTLIST");
$get_lists = isset($get_lists) ? $get_lists : true;
$get_active_lists = isset($get_active_lists) ? $get_active_lists : true;
if($get_lists) {
	$lists = array();
	if($section=="TOP" || $section=="CAP") {
		$mheadings[$section.'_F'] = array(
			'years'=>array(
				'h_type'=>"LIST",
				'h_table'=>"years",
			)
		);
	}
	foreach($mheadings as $sec) {
		foreach($sec as $key => $h) {
			if(in_array($h['h_type'],$list_types) && !isset($lists[$h['h_table']])) {
				switch($h['h_table']) {
					case "dir":
					case "subdir":
						$sql = "SELECT * FROM ".$dbref."_".$h['h_table']." WHERE active = true ORDER BY sort, value";
						break;
					default:
						$sql = "SELECT * FROM ".$dbref."_list_".$h['h_table']." ".($get_active_lists ? "WHERE active = true" : "")." ORDER BY ".($h['h_table']=="wards" ? "CAST(code as UNSIGNED), " : "")."sort, value";
						//echo $sql;
						break;
				}
				if($h['h_table']=="calctype") { $fld = "code"; } else { $fld = "id"; }
				if($h['h_table']=="subdir") {
					//$rs = getRS($sql);
                    $rs = $helperObj->mysql_fetch_all($sql);
					$data = array();
					//while($row = mysql_fetch_array($rs)) {
                    foreach ($rs as $row){
						$data['dir'][$row['dirid']][$row['id']] = $row;
						$data[$row['id']] = $row;
					}
				} else {
					//$data = mysql_fetch_alls($sql,$fld);
					$data = $helperObj->mysql_fetch_all_fld($sql,$fld);
				}
				$lists[$h['h_table']] = $data;
			}
		}
	}
}
//arrPrint($lists);

/* RESULT SETTINGS */
/*$result_settings = array(
	0 => array('id'=>0,'r'=>0,'value'=>"KPI Not Yet Measured"	,'text'=>"N/A"	,'style'=>"result0",'color'=>"#999999", 'glossary'=> "KPIs with no targets or actuals in the selected period."),
	1 => array('id'=>1,'r'=>1,'value'=>"KPI Not Met"			,'text'=>"X"	,'style'=>"result1",'color'=>"#CC0001", 'glossary'=> "0% >= Actual/Target < 75%"),
	2 => array('id'=>2,'r'=>2,'value'=>"KPI Almost Met"			,'text'=>"XX"	,'style'=>"result2",'color'=>"#FE9900", 'glossary'=> "75% >= Actual/Target < 100%"),
	3 => array('id'=>3,'r'=>3,'value'=>"KPI Met"				,'text'=>"XXX"	,'style'=>"result3",'color'=>"#009900", 'glossary'=> "Actual/Target = 100%"),
	4 => array('id'=>4,'r'=>4,'value'=>"KPI Well Met"			,'text'=>"Y"	,'style'=>"result4",'color'=>"#005500", 'glossary'=> "100% > Actual/Target < 150%"),
	5 => array('id'=>5,'r'=>5,'value'=>"KPI Extremely Well Met"	,'text'=>"Z"	,'style'=>"result5",'color'=>"#000077", 'glossary'=> "Actual/Target >= 150%"),
);*/
?>