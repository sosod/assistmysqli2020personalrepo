<?php 
$page_title = "Import Log";
$import_section = "LOG";
include("inc/header.php"); 
global $helperObj;

	$modref = strtoupper($modref);
	$ilf = $modref."/import/";
    $helperObj->checkFolder($ilf);
	$ilf.="import_log.csv";
	$import_log_file = fopen("../files/".$cmpcode."/".$ilf,"r");

            $data = array();
            while(!feof($import_log_file)) {
                $tmpdata = fgetcsv($import_log_file);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }

fclose($import_log_file);


//arrPrint($data);


?>
<table>
	<tr>
		<th>Date</th>
		<th>Page visited</th>
	</tr>
<?php
$char5_dont_display = array(".html","oaded"," MARK");
foreach($data as $log) {
	if(!in_array(substr($log['2'],-5),$char5_dont_display)) {
		echo "
	<tr>
		<td>".$log['0']."</td>
		<td>".$log['2']."</td>
	</tr>";
	}
}
?>
</table>
<p>Module version: SDBP5B.  Log report drawn as at <?php echo date("d F Y H:i"); ?>.</p>

</body>
</html>