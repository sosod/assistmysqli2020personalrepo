<?php
//error_reporting(-1);
$userObj = new SDBP5_User(true);
$active_time = $userObj->getActiveTimeField();

$importStatus = $userObj->getImportStatus();
$assurance_count = array();
if($importStatus['KPI']==1) {
	$kpiField = "KPI";
	$timeField = "kr";
	$canUpdate = $userObj->canUpdateDept(array("SUB","OWN"));
	if($canUpdate==false) {
		//check for assurance KPIs - in case user access at DIR level
			$sql = "SELECT count(kpi_id) as c, kr_assurance_status, kr_assurance_deadline, kr_timeid as t
					FROM ".$tdb->getDBRef()."_kpi
					INNER JOIN ".$tdb->getDBRef()."_kpi_results 
					ON kr_kpiid = kpi_id 
					WHERE kpi_active = 1 AND kr_assurance_status = 4 AND kr_assurance_updateuser = '".$userObj->getUserID()."'
					AND (
						kr_assurance_deadline = '0000-00-00' 
						OR (
							kr_assurance_deadline >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
							AND 
							kr_assurance_deadline <= DATE_ADD(NOW(), INTERVAL $next_due DAY)
						)
					)
					GROUP BY kr_assurance_status, kr_assurance_deadline, kr_timeid"; //echo $sql;
			$rows = $userObj->mysql_fetch_all($sql);
			foreach($rows as $r) {
				if($r['kr_assurance_deadline']=="0000-00-00") {
					$t = $r['t'];
					$tc = strtotime(date("d F Y",$open_time[$t]["close_".$active_time]));
				} else {
					$tc = strtotime(date("d F Y",strtotime($r['kr_assurance_deadline'])));
				}
				if($tc==$now) {
					$assurance_count['present']+=$r['c'];
				} elseif($tc<=$future) {
					$assurance_count['future']+=$r['c'];
				}
			}
		if(count($assurance_count)>0) { $canUpdate=true; }

	}

} elseif($importStatus['TOP']==1) {
	$kpiField = "TOP";
	$timeField = "tr";
	$canUpdate = $userObj->canUpdateTopLayer();
} else {
	$canUpdate = false;
}


if($importStatus['LIST']==1 && ($importStatus['KPI']==1 || $importStatus['TOP']==1)) {


	$headObj = new SDBP5_HEADINGS();
	$myHead = $headObj->getHeadings(array($kpiField,$kpiField."_R"));  //arrPrint($myHead);

					$head = array();
						$blank_action = array();
					$head['ref'] = array('text'=>"Ref", 'long'=>false, 'deadline'=>false, 'type'=>"S");
						$blank_action['ref'] = 0;
					$head['deadline'] = array('text'=>"Closure", 'long'=>false, 'deadline'=>false, 'type'=>"S", 'class'=>"center");
						$blank_action['deadline'] = "";
if($kpiField=="TOP") { $myHead['dir']['h_client']=$myHead[$kpiField]['top_dirid']['h_client']; }
					$head['dir'] = array('text'=>$myHead['dir']['h_client'], 'long'=>false, 'deadline'=>false, 'type'=>"S");
						$blank_action['dir'] = "";
					$head['value'] = array('text'=>$myHead[$kpiField][strtolower($kpiField).'_value']['h_client'], 'long'=>true, 'deadline'=>false, 'type'=>"S");
						$blank_action['value'] = "";
					$head['unit'] = array('text'=>$myHead[$kpiField][strtolower($kpiField).'_unit']['h_client'], 'long'=>true, 'deadline'=>false, 'type'=>"S");
						$blank_action['unit'] = "";
					$head['poe'] = array('text'=>$myHead[$kpiField][strtolower($kpiField).'_poe']['h_client'], 'long'=>true, 'deadline'=>false, 'type'=>"S");
						$blank_action['poe'] = "";
					$head['time'] = array('text'=>"Time Period", 'long'=>false, 'deadline'=>false, 'type'=>"S", 'class'=>"center");
						$blank_action['time'] = "";
					$head['target'] = array('text'=>$myHead[$kpiField.'_R'][$timeField.'_target']['h_client'], 'long'=>false, 'deadline'=>false, 'type'=>"S", 'class'=>"right");
						$blank_action['target'] = "";
					$head['link'] = array('text'=>"", 'long'=>false, 'deadline'=>false, 'type'=>"S");
						$blank_action['link'] = "manage_update.php?kpi=".$kpiField."|id=";
					
					$actions = array();




	$now = strtotime(date("d F Y",($today))); //echo date("d F Y H:i:s",$now);
	$future = strtotime(date("d F Y",($today+( ($next_due)*24*3600)))); //echo date("d F Y H:i:s",$future);

	/*****
	display kpis where user has departmental['update'] access
	and time periods are open and closure date falls before user profile next_due setting
	*****/

	//Read user access to determine time period activity - primary (default), secondary or tertiary
	if($canUpdate===true) {
		//Find open time periods WHERE closure date falls on or before $future 
		$timeObj = new STIME();
		$open_time = $timeObj->getOpenTime($active_time,"close_".$active_time." <= ".$future." AND close_".$active_time." > 0");
		$all_time = $timeObj->getAllTime("id");
		//arrPrint($open_time);
		
		
		//ASSURANCE KPIS
					$sql = "SELECT kpi_id as ref, kpi_value as value, kpi_poe as poe, kpi_unit as unit, kr_timeid as time, kr_target as target, CONCAT(d.value,' - ',s.value) as dir, kpi_targettype as targettype, kr_assurance_deadline as deadline
							FROM ".$tdb->getDBRef()."_kpi
							INNER JOIN ".$tdb->getDBRef()."_kpi_results 
							ON kr_kpiid = kpi_id  
							INNER JOIN ".$tdb->getDBRef()."_list_time t
							ON t.id = kr_timeid
							INNER JOIN ".$tdb->getDBRef()."_subdir s
							ON kpi_subid = s.id
							INNER JOIN ".$tdb->getDBRef()."_dir d
							ON s.dirid = d.id
							WHERE kpi_active = 1 AND kr_assurance_status = 4 AND kr_assurance_updateuser = '".$userObj->getUserID()."'
								AND (
									kr_assurance_deadline = '0000-00-00' 
									OR (
										kr_assurance_deadline >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
										AND 
										kr_assurance_deadline <= DATE_ADD(NOW(), INTERVAL $next_due DAY)
									)
								)
							ORDER BY "; 
							switch($action_profile['field3']) {
								case "dead_desc":	$sql.= " kr_assurance_deadline DESC, kr_timeid DESC, kpi_id "; break;
								default:
								case "dead_asc":	$sql.= " kr_assurance_deadline ASC, kr_timeid ASC, kpi_id "; break;
							}
							$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
		//echo $sql;
					$rows = $tdb->mysql_fetch_all($sql);
					foreach($rows as $r) {
						$i = $r['ref'];
						$a = $blank_action;
						foreach($blank_action as $key=>$b) {
							switch($key) {
								case "target":
									$a[$key] = SDBP5_HELPER::formatTarget($r[$key],$r['targettype'],0);
									break;
								case "deadline":
									//$a[$key] = date("d M Y",$open_time[$r['time']]['close_'.$active_time]);
									if($r['deadline']=="0000-00-00") {
										$a[$key] = date("d M Y",$open_time[$r['time']]['close_'.$active_time]);
									} else {
										$a[$key] = date("d M Y",strtotime($r['deadline']));
									}
									break;
								case "ref":
									$a[$key] = DEPT::REFTAG.$i;
									break;
								case "link":
									$a[$key].=$i."|time_id=".$r['time'];
									break;
								case "time":
									$a[$key] = $all_time[$r[$key]]['display'];
									break;
								default:
									$a[$key] = $r[$key];
									break;
							}
						}
						$actions[] = $a;
					}
				
		if(count($open_time)>0) {
			$admins = $userObj->getAdmins();
			if($importStatus['KPI']==1) {
				//If admin access exists - count KPIs where kpi_active = true, kpi_subid in admin list, kr_timeid in time list, kr_update = false, kr_target>0||kpi_calctype!="ZERO" GROUP BY kr_timeid
				$subs = array();
				if(isset($admins['SUB'])) {
					foreach($admins['SUB'] as $s => $v) {
						if($userObj->canUpdateSub($s)) {
							$subs[] = $s;
						}
					}
				}
				$own = array();
				if(isset($admins['OWN'])) {
					foreach($admins['OWN'] as $s => $v) {
						if($userObj->canUpdateOwn($s)) {
							$own[] = $s;
						}
					}
				}
				if(count($subs)>0 || count($own)>0) {
					$time_ids = array_keys($open_time);
					/*$sql = "SELECT kr_timeid as id, count(kpi_id) as c
							FROM ".$tdb->getDBRef()."_kpi
							INNER JOIN ".$tdb->getDBRef()."_kpi_results 
							ON kr_kpiid = kpi_id AND kr_timeid IN (".implode(",",$time_ids).")  AND kr_update = 0
							WHERE kpi_active = 1 AND kpi_subid IN (".implode(",",$subs).")
							AND (kpi_calctype = 'ZERO' OR kr_target>0)
							GROUP BY kr_timeid";*/
					$sql = "SELECT kpi_id as ref, kpi_value as value, kpi_poe as poe, kpi_unit as unit, kr_timeid as time, kr_target as target, CONCAT(d.value,' - ',s.value) as dir, kpi_targettype as targettype
							FROM ".$tdb->getDBRef()."_kpi
							INNER JOIN ".$tdb->getDBRef()."_kpi_results 
							ON kr_kpiid = kpi_id AND kr_timeid IN (".implode(",",$time_ids).")  AND kr_update = 0
							INNER JOIN ".$tdb->getDBRef()."_list_time t
							ON t.id = kr_timeid
							INNER JOIN ".$tdb->getDBRef()."_subdir s
							ON kpi_subid = s.id
							INNER JOIN ".$tdb->getDBRef()."_dir d
							ON s.dirid = d.id
							WHERE kpi_active = 1 AND (
								".(count($subs)>0 ? "kpi_subid IN (".implode(",",$subs).")" : "")."
								".(count($subs)>0 && count($own)>0 ? "	OR	" : "")."
								".(count($own)>0 ? "kpi_ownerid IN (".implode(",",$own).")" : "")."
							) AND (kpi_calctype = 'ZERO' OR kr_target>0)
							ORDER BY "; 
							switch($action_profile['field3']) {
								case "dead_desc":	$sql.= " t.close_".$active_time." DESC, kr_timeid DESC, kpi_id "; break;
								default:
								case "dead_asc":	$sql.= " t.close_".$active_time." ASC, kr_timeid ASC, kpi_id "; break;
							}
							$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
		//echo $sql;
					$rows = $tdb->mysql_fetch_all($sql);
					foreach($rows as $r) {
						$i = $r['ref'];
						$a = $blank_action;
						foreach($blank_action as $key=>$b) {
							switch($key) {
								case "target":
									$a[$key] = SDBP5_HELPER::formatTarget($r[$key],$r['targettype'],0);
									break;
								case "deadline":
									$a[$key] = date("d M Y",$open_time[$r['time']]['close_'.$active_time]);
									break;
								case "ref":
									$a[$key] = DEPT::REFTAG.$i;
									break;
								case "link":
									$a[$key].=$i."|time_id=".$r['time'];
									break;
								case "time":
									$a[$key] = $open_time[$r[$key]]['display'];
									break;
								default:
									$a[$key] = $r[$key];
									break;
							}
						}
						$actions[] = $a;
					}
				}
			} elseif($importStatus['TOP']==1) {
				//If admin access exists - count KPIs where kpi_active = true, kpi_subid in admin list, kr_timeid in time list, kr_update = false, kr_target>0||kpi_calctype!="ZERO" GROUP BY kr_timeid
				$subs = array();
				if(isset($admins['TOP'])) {
					foreach($admins['TOP'] as $s => $v) {
						if($userObj->canUpdateTop($s)) {
							$subs[] = $s;
						}
					}
				}
				$own = array();
				if(isset($admins['T_OWN'])) {
					foreach($admins['T_OWN'] as $s => $v) {
						if($userObj->canUpdateTopOwn($s)) {
							$own[] = $s;
						}
					}
				}
				if(count($subs)>0 || count($own)>0) {
					$time_ids = array_keys($open_time);
					$sql = "SELECT top_id as ref, top_value as value, top_poe as poe, top_unit as unit, tr_timeid as time, tr_target as target, CONCAT(d.value,'') as dir, top_targettype as targettype
							FROM ".$tdb->getDBRef()."_top
							INNER JOIN ".$tdb->getDBRef()."_top_results 
							ON tr_topid = top_id AND tr_timeid IN (".implode(",",$time_ids).")  AND tr_update = 0
							INNER JOIN ".$tdb->getDBRef()."_list_time t
							ON t.id = tr_timeid
							INNER JOIN ".$tdb->getDBRef()."_dir d
							ON top_dirid = d.id
							WHERE top_active = 1 AND (
								".(count($subs)>0 ? "top_dirid IN (".implode(",",$subs).")" : "")."
								".(count($subs)>0 && count($own)>0 ? "	OR	" : "")."
								".(count($own)>0 ? "top_ownerid IN (".implode(",",$own).")" : "")."
							) AND (top_calctype = 'ZERO' OR tr_target>0)
							ORDER BY "; 
							switch($action_profile['field3']) {
								case "dead_desc":	$sql.= " t.close_".$active_time." DESC, tr_timeid DESC, top_id "; break;
								default:
								case "dead_asc":	$sql.= " t.close_".$active_time." ASC, tr_timeid ASC, top_id "; break;
							}
							$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
		//echo $sql;
					$rows = $tdb->mysql_fetch_all($sql);
					foreach($rows as $r) {
						$i = $r['ref'];
						$a = $blank_action;
						foreach($blank_action as $key=>$b) {
							switch($key) {
								case "target":
									$a[$key] = SDBP5_HELPER::formatTarget($r[$key],$r['targettype'],0);
									break;
								case "deadline":
									$a[$key] = date("d M Y",$open_time[$r['time']]['close_'.$active_time]);
									break;
								case "ref":
									$a[$key] = TOP::REFTAG.$i;
									break;
								case "link":
									$a[$key].=$i."|time_id=".$r['time'];
									break;
								case "time":
									$a[$key] = $open_time[$r[$key]]['display'];
									break;
								default:
									$a[$key] = $r[$key];
									break;
							}
						}
						$actions[] = $a;
					}
				}
			}
		}
	}
}
//arrPrint($actions);











				/*
				$actions[1] = array();
				foreach($head as $k=>$x) {
					$actions[1][$k] = $x['text'];
				}
				$actions[1]['link'] = "manage_update.php?kpi_id=1";
				
				for($i=2;$i<=10;$i++) {
					$actions[$i] = $actions[1];
				}
				
/*				$sql = "SELECT type, field, headingfull FROM ".$dbref."_list_display WHERE yn = 'Y' AND home_page = 'Y' AND field NOT IN ('taskdeadline','taskstatusid') ORDER BY allsort";
				$head1 = mysql_fetch_all($sql);
				$udf_head = mysql_fetch_all_fld("SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".$modref."'","udfiid");
				$udfs = array();
				foreach($head1 as $h) {
					$f = $h['field'];
					$blank_action[$f] = "";
					$head[$f] = array(
						'text'=>decode($h['type']=="S" ? $h['headingfull'] : $udf_head[$f]['udfivalue']),
						'long'=> ( ($f=="taskaction" || $f=="taskdeliver" || ($h['type']=="U" && $udf_head[$f]['udfilist']=="M") ) ? true : false),
						'deadline'=> ( ($h['type']=="U" && $udf_head[$f]['udfilist']=="D") || $f == "taskadddate" ? true : false),
						'type'=> $h['type']=="S" ? "S" : $udf_head[$f]['udfilist'],
					);
					if($h['type']=="U") { $udfs[] = $f; }
				}
				$head['taskstatusid'] = array('text'=>"Status", 'long'=>false, 'deadline'=>false, 'type'=>"S");
				$blank_action['taskstatusid'] = "";
				$head['link'] = array('text'=>"", 'long'=>false, 'deadline'=>false,'type'=>"S");
				$blank_action['link'] = "";
//arrPrint($head);
				$actions = array();
				
				$sql = "SELECT t.taskid 
						, t.taskaction
						, t.taskdeliver
						, t.taskdeadline
						, t.taskstate
						, t.taskadddate
						, top.value as tasktopicid
						, stat.value as taskstatusid
						, urg.value as taskurgencyid
						, CONCAT_WS(' ',tka.tkname, tka.tksurname) as taskadduser
						FROM ".$dbref."_task t
						INNER JOIN ".$dbref."_list_topic top
						ON t.tasktopicid = top.id
						INNER JOIN ".$dbref."_list_status stat
						ON t.taskstatusid = stat.pkey
						INNER JOIN ".$dbref."_list_urgency urg
						ON t.taskurgencyid = urg.id
						INNER JOIN ".$dbref."_task_recipients tr
						ON t.taskid = tr.taskid AND tr.tasktkid = '$tkid'
						INNER JOIN assist_".$cmpcode."_timekeep tka
						ON t.taskadduser = tka.tkid
						WHERE t.taskdeadline < ".($today + 3600*24*$next_due)."
						AND t.taskstatusid > 2 AND t.taskstatusid <> 5 ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.taskdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.taskdeadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
				$tasks = mysql_fetch_all_fld($sql,"taskid");
				
				if(count($udfs)>0 && count($tasks)>0) {
					$keys = array_keys($tasks);
					$sql = "SELECT * FROM  `assist_".$cmpcode."_udf` 
							WHERE udfindex IN (".implode(",",$udfs).") 
							AND udfref =  '".$_SESSION['modref']."'
							AND udfnum IN (".implode(",",$keys).") ";
					$uv = mysql_fetch_all_fld2($sql,"udfnum","udfindex");
					$ul = mysql_fetch_all_fld2("SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex IN (".implode(",",$udfs).")","udfvindex","udfvid");
				}
				
				
				foreach($tasks as $obj_id => $t) {
					$actions[$obj_id] = $blank_action;
					foreach($head as $f => $h) {
						if($h['type']=="S") {
							$v = isset($t[$f]) ? $t[$f] : "";
							switch($f) {
							case "ref":
								$v = $obj_id;
								break;
							case "taskstatusid":
								$v = $v.($v!="On-going" ? " (".$t['taskstate']."%)" : "");
								break;
							case "taskdeadline":
									if($t['taskstatusid']!="On-going") {
										$deaddiff = $today - $v;
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0 && $deaddiff > -(3600*24*7)) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$v = "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$v = "<span>Due in $diffdays $days</span>";
										} elseif($diffdays==0) {
											$v = "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											$v = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									} else {
										$v = "";
									}
								break;
							case "link":
								$v = "update.php?i=".$obj_id;
								break;
							case "taskadddate":
								$v = date("d-M-Y H:i",$v); break;
							default:
								$v = $v;
								break;
							}
						} else {
							$v = isset($uv[$obj_id][$f]) ? $uv[$obj_id][$f]['udfvalue'] : "";
							switch($h['type']) {
							case "Y":	$v = isset($ul[$f][$v]) ? $ul[$f][$v]['udfvvalue'] : "";	break;
							case "N":	$v = format_number($v,2,".",",");	break;
							default:	$v = $v;	break;
							}
						}
						$actions[$obj_id][$f] = $v;
					}
				}
				//arrPrint($actions);


*/
?>