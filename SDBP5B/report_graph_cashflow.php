<?php
$section = "CF";
$get_lists = true; $get_active_lists = false;
include("inc/header.php"); 
include("inc/header_report.php"); 

//arrPrint($_REQUEST);

$generator = $_REQUEST;
if(!isset($generator['report_title']) || strlen($generator['report_title'])==0) { $generator['report_title'] = "Monthly Cashflow Report"; }
if(!isset($generator['from'])) { $generator['from'] = 1; }
if(!isset($generator['to'])) { $generator['to'] = $current_time_id; }
if($generator['from']>$generator['to']) { $x = $generator['to']; $generator['to'] = $generator['from']; $generator['from'] = $x; }
$valAxis = isset($_REQUEST['valAxis']) ? $_REQUEST['valAxis'] : "STD";

$filter_from = $generator['from'];
$filter_to = $generator['to'];
$x = isset($generator['dirsub_filter']) ? $generator['dirsub_filter'] : "ALL";


$report_settings = array();
$report_settings['r_from'] = $filter_from;
$report_settings['r_to'] = $filter_to;
$report_settings['groupby'] = "";
$report_settings['path'] = "report_cashflow.php";

if($x=="ALL") {
	$filter_who = array("X",0);
	$groupby = isset($generator['groupby']) ? $generator['groupby'] : "obj_dirid";
	$graph_title['main'] = $cmpname;
	$graph_title['sub'] = $head_dir;
	$report_settings['groupby'] = $groupby=="obj_dirid" ? "dir" : $groupby;
} else {
	$filter_who = explode("_",$x);
	if($filter_who[0]=="D") {
		$groupby = isset($generator['groupby']) ? $generator['groupby'] : "obj_subid";
		$graph_title['main'] = $lists['dir'][$filter_who[1]]['value'];
		$graph_title['sub'] = $head_sub;
		$report_settings['groupby'] = $groupby=="obj_subid" ? "kpi_subid" : $groupby;
	} else {
		$groupby = isset($generator['groupby']) ? $generator['groupby'] : "NA";
		$graph_title['main'] = $lists['subdir'][$filter_who[1]]['value'];
		$graph_title['sub'] = "";
		$report_settings['groupby']="X";
	}
}
$graph_id = $filter_who[1];



if(!isset($_REQUEST['do_group']) || $_REQUEST['do_group']=="N") { 	
	$groupby="NA";
	$graph_title['sub'] = "";
	$report_settings['groupby']="X";
}

switch($groupby) {
case "obj_dirid":	$graph_title['sub'] = $head_dir;									$r_sql = "dirid"; 		$group_list = $lists['dir']; break;
case "obj_subid":	$graph_title['sub'] = $head_sub;									$r_sql = "cf_subid"; 	$group_list = $lists['subdir']['dir'][$filter_who[1]]; break;
case "NA":			$graph_title['sub'] = "";											$r_sql = "cf_subid"; 	$group_list = array(); break;
default:			$graph_title['sub'] = $mheadings[$section][$groupby]['h_client']; 	$r_sql = $groupby; 		$group_list = $lists[$mheadings[$section][$groupby]['h_table']]; break;
}

$graph_type = array("BAR","REG");
//$page_layout = isset($generator['page_layout']) ? $generator['page_layout'] : "LAND";
$page_layout = "PORT";
//BAR GRAPH IS LIMITED TO 9 COLUMNS ELSE CATEGORY AXIS DOES NOT SHOW ALL VALUES
if($graph_type[0]=="BAR") {
	switch($page_layout) {
	case "PORT":
		$max[0] = 6;
		$max[1] = 6;
		break;
	case "LAND":
	default:
		$max[0] = 8;
		$max[1] = 8;
		break;
	}
} else {
	$max[0] = 1; $max[1] = 1;
}
$blurb = "";
if($generator['from']==$generator['to']) {
	$blurb = "<br />for the month of ".$time[$generator['from']]['display_full'];
} else {
	$blurb = "<br />for the months of ".$time[$generator['from']]['display_full']." to ".$time[$generator['to']]['display_full'];
}


?>
<style type=text/css>
table .legend { border: 1px solid #ffffff; margin: 5px 0px 10px 0px;}
table td .legend, .legend td { border: 1px solid #ffffff; }
table td { font-size: 7pt; }
//table th { font-size: 7pt; }
th.budget { background-color: #FE9900; }
th.actual { background-color: #009900; }
th.var { background-color: #444444; }
td.budget { text-align: right; background-color: #FFF5E6; }
td.actual { text-align: right; background-color: #E6FFE6; }
td.var { text-align: right; background-color: #E6E6E6; }
td.cs { text-align: right; font-weight: bold; }
tfoot td.budget { text-align: right; background-color: #FFED99; font-weight: bold; }
tfoot td.actual { text-align: right; background-color: #99FF99; font-weight: bold; }
tfoot td.var { text-align: right; background-color: #aaaaaa; font-weight: bold; }
tfoot td.cs {  text-align: right; background-color: #eeeeee; font-weight: bold; }
</style>
<h1 class=center style="margin-top: 0;margin-bottom: 0;"><?php echo $generator['report_title']; ?></h1>
<?php
		echo "<p class=\"center i\" style=\"margin-top: 0px; font-size: 7pt;\">Report drawn ".($groupby!="NA" ? "per ".$graph_title['sub']." " : "")."on ".date("d F Y")." at ".date("H:i").$blurb.".</p>";


function calculateVar($arr) {
	return ($arr['budget']-$arr['actual']);
}

function drawLegend($arr,$total) {
	global $cf_sections;
	$echo= "<div align=center><table class=legend>
							<tr>
								<th class=blank width=37%></th>
								<th class=budget width=21%>Budget</th>
								<th class=actual width=21%>Actual</th>
								<th class=var width=21%>Variance</th>
							</tr>";
foreach($cf_sections as $cs_id => $cs) {
	$echo.= "	<tr>
				<td class=cs>".$cs.":</td>
				<td class=budget>".number_format($arr[$cs_id]['budget'],2,".",",")."</td>
				<td class=actual>".number_format($arr[$cs_id]['actual'],2,".",",")."</td>
				<td class=var>".number_format(calculateVar($arr[$cs_id]),2,".",",")."</td>
			</tr>";
}
	$echo.= "	<tfoot><tr>
				<td class=cs>Total:</td>
				<td class=budget>".number_format($total['budget'],2,".",",")."</td>
				<td class=actual>".number_format($total['actual'],2,".",",")."</td>
				<td class=var>".number_format(calculateVar($total),2,".",",")."</td>
			</tr></tfoot>
		</table></div>";
	
	return $echo;
}

	//GET DATA
$cf_sections = array('rev'=>"Revenue",'opex'=>"Operational Expenditure",'capex'=>"Capital Expenditure");
		$dir = array();
		$sub = array();
		$results_sql = "SELECT ".$r_sql." as sub"; //, sum(cr_opex_4) as opex_budget, sum(cr_opex_5) as opex_actual";
		foreach($cf_sections as $cs_id => $cs) {
			$dir[$cs_id]=array('budget'=>0,'actual'=>0);
			$results_sql.= ", sum(cr_".$cs_id."_4) as ".$cs_id."_budget, sum(cr_".$cs_id."_5) as ".$cs_id."_actual";
		}
		$results_sql.= " FROM ".$dbref."_".$table_tbl."_results
						INNER JOIN ".$dbref."_".$table_tbl." ON cf_id = cr_cfid AND cf_active = true
						INNER JOIN ".$dbref."_subdir ON id = cf_subid AND active = true  
						".($filter_who[0]=="D" ? "AND dirid = ".$graph_id : "" )."
						".($filter_who[0]=="S" ? "AND id = ".$graph_id : "" )."
						WHERE cr_timeid <= ".$filter_to." 
						GROUP BY ".$r_sql;
						//echo $results_sql;
		$results = $helperObj->mysql_fetch_all_fld($results_sql,"sub");  //arrPrint($results);
	$sub_chartData = array();
	$dir_Data = array();
	$totals = array(
		'main'=>array(	
			'budget'=>0,
			'actual'=>0
		),
		'sub'=>array()
	);
	$max = 0;
	//arrPrint($group_list);
	if($groupby!="NA") {
		foreach($group_list as $g_id => $g) {
			if(isset($results[$g_id])) { 
				$sub[$g_id] = array();
				$totals['sub'][$g_id] = array(
					'budget'=>0,
					'actual'=>0
				);
				$g_res = $results[$g_id];

				foreach($cf_sections as $cs_id => $cs) {
					if($g_res[$cs_id.'_budget'] > $max) { $max = $g_res[$cs_id.'_budget']; }
					$max = $g_res[$cs_id.'_actual'] > $max ? $g_res[$cs_id.'_actual'] : $max;
					$sub[$g_id][$cs_id] = array(
						'budget'=>$g_res[$cs_id.'_budget'],
						'actual'=>$g_res[$cs_id.'_actual']
					);
						$dir[$cs_id]['actual']+= $g_res[$cs_id.'_actual'];
						$dir[$cs_id]['budget']+= $g_res[$cs_id.'_budget'];
					$sub_chartData[$g_id][$cs_id] = "{text:\"".$cs."\",budget:".number_format($g_res[$cs_id.'_budget'],2,".","").",actual:".number_format($g_res[$cs_id.'_actual'],2,".","")."}";
						$totals['sub'][$g_id]['budget']+=$g_res[$cs_id.'_budget'];
						$totals['sub'][$g_id]['actual']+=$g_res[$cs_id.'_actual'];
				}
			}
		}
		foreach($cf_sections as $cs_id => $cs) {
			$dir_Data[] = "{text:\"$cs\",budget:".number_format($dir[$cs_id]['budget'],0,".","").",actual:".number_format($dir[$cs_id]['actual'],0,".","")."}";
			//$totals['main']['budget']+=$dir[$cs_id]['budget'];
			//$totals['main']['actual']+=$dir[$cs_id]['actual'];
		}
	} else {
		foreach($cf_sections as $cs_id => $cs) {
			foreach($results as $i => $r) {
				$dir[$cs_id]['actual']+= $r[$cs_id.'_actual'];
				$dir[$cs_id]['budget']+= $r[$cs_id.'_budget'];
			}
			$dir_Data[] = "{text:\"$cs\",budget:".number_format($dir[$cs_id]['budget'],0,".","").",actual:".number_format($dir[$cs_id]['actual'],0,".","")."}";
		}
	}
	foreach($dir as $d) {	
		$totals['main']['budget']+=$d['budget'];
		$totals['main']['actual']+=$d['actual'];
	}
	//arrPrint($group_list);
	echo "<div align=center id=container>
			<table>
				<tr>
					<td>
						<h2>".$graph_title['main']."</h2>
						<div id=\"main\" style=\"width:550px; height:250px; background-color:#ffffff;\"></div>
						".drawLegend($dir,$totals['main'])."
					</td>
				</tr>
				";
	if(count($sub_chartData)>0 && $groupby!="NA") {
		foreach($sub_chartData as $s_id => $d) {
			echo chr(10)."<tr>
					<td><h3>".$group_list[$s_id]['value']."</h3><div id=\"sub_".$s_id."\" style=\"width:550px; height:250px; background-color:#ffffff;\"></div>".drawLegend($sub[$s_id],$totals['sub'][$s_id])."</td>
				</tr>";
		}
	}
	echo "</table></div>";
	//echo $max." :: ";
	$max*=1.1;
	$max = round($max,-(strlen(ceil($max))-2),PHP_ROUND_HALF_UP);
	//echo $max." :: ".round($max,-(strlen(ceil($max))-2),PHP_ROUND_HALF_UP)." :: ".(-(strlen(ceil($max))-2));
?>
<script type=text/javascript>
$(document).ready(function() {
	$("tr").off("mouseenter mouseleave");
	$("th, td").addClass("center");
	$("td .cs").removeClass("center");
	$("div").css("background-color","#ffffff");
	$("table, table td").css("border-color","#ffffff");
				var valAxis = new AmCharts.ValueAxis();
					valAxis.gridAlpha = 0.1;
					valAxis.axisAlpha = 0;
					valAxis.minimum = 0;
					valAxis.fontSize=8;
				var bgraph = new AmCharts.AmGraph();
					bgraph.title = "Budget";
					bgraph.valueField = "budget";
					bgraph.labelText = "[[value]]";
					bgraph.fontSize = 8;
					bgraph.type = "column";
					bgraph.lineAlpha = 0;
					bgraph.fillAlphas = 1;
					bgraph.lineColor = "#FE9900";
				var agraph = new AmCharts.AmGraph();
					agraph.title = "Actual";
					agraph.valueField = "actual";
					agraph.labelText = "[[value]]";
					agraph.fontSize = 8;
					agraph.type = "column";
					agraph.lineAlpha = 0;
					agraph.fillAlphas = 1;
					agraph.lineColor = "#009900";
					
		var main_chartData = [<?php echo implode(",",$dir_Data); ?>];
				main_chart = new AmCharts.AmSerialChart();
					main_chart.dataProvider = main_chartData;
					main_chart.categoryField = "text";
					main_chart.plotAreaBorderAlpha = 0.2;
					main_chart.angle = 45;
					main_chart.depth3D = 15;
					main_chart.columnSpacing = 1;
					main_chart.columnWidth = 0.9;
					main_chart.addGraph(bgraph);
					main_chart.addGraph(agraph);
					main_chart.addValueAxis(valAxis);
				main_chart.categoryAxis.gridAlpha = 0.1;
				main_chart.categoryAxis.axisAlpha = 0;
				main_chart.categoryAxis.gridPosition = "start";
				main_chart.write("main");

		var subValAxis = valAxis;
<?php if($valAxis=="STD") { ?>		
			subValAxis.maximum = <?php echo $max; ?>;
	<?php
	}
	
	foreach($sub_chartData as $s_id => $d) {
		echo chr(10).chr(10)."var sub_".$s_id."_chartData = [".implode(",",$d)."];
				sub_".$s_id."_chart = new AmCharts.AmSerialChart();
					sub_".$s_id."_chart.dataProvider = sub_".$s_id."_chartData;
					sub_".$s_id."_chart.categoryField = \"text\";
					sub_".$s_id."_chart.plotAreaBorderAlpha = 0.2;
					sub_".$s_id."_chart.angle = 45;
					sub_".$s_id."_chart.depth3D = 15;
					sub_".$s_id."_chart.columnSpacing = 1;
					sub_".$s_id."_chart.columnWidth = 0.9;
					sub_".$s_id."_chart.addGraph(bgraph);
					sub_".$s_id."_chart.addGraph(agraph);
					sub_".$s_id."_chart.addValueAxis(subValAxis);
				sub_".$s_id."_chart.categoryAxis.gridAlpha = 0.1;
				sub_".$s_id."_chart.categoryAxis.axisAlpha = 0;
				sub_".$s_id."_chart.categoryAxis.gridPosition = \"start\";
				
				sub_".$s_id."_chart.write(\"sub_".$s_id."\");
		";
	}
	?>
});
</script>
</body></html>