<?php 
//Filter settings
global $helperObj;
$filter = array();
	//WHEN
	if(isset($_REQUEST['filter_when'])) {
		$filter_when = $_REQUEST['filter_when'];
	} else {
		$filter_to = ($today < strtotime("2011-06-30 23:59:59")) ? 1 : ( (date("m",$today)>6) ? (date("m",$today)-6) : (date("m",$today)+6) );
		$filter_when = array($filter_to,$filter_to);
	}
	$filter['when'] = $filter_when;
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
	} else {
		$filter_who = array("","All");
	}
	$filter['who'] = $filter_who;
	//WHAT
	$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
	//DISPLAY
	$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";


//arrPrint($filter);


if(!$import_status[$section]) {
	die("<p>Monthly Cashflow has not yet been created.  Please try again later.</p>");
} else {


	if(isset($_REQUEST['act']) && strlen($_REQUEST['act'])>0) {
		//echo "abc";arrPrint($_REQUEST);
		include("manage_fin_cashflow_edit_budget.php");

	} else {
			echo "<h2>Edit All Budgets</h2>";
$time_all = getTime($get_time,false);
//			arrPrint($time);
		?>
		<form id=up action=manage_fin.php method=post enctype="multipart/form-data">
		<input type=hidden name=page_id value=<?php echo $page_id; ?> />
		<input type=hidden name=tab value=<?php echo $tab; ?> />
		<input type=hidden name=act value=REVIEW />
			<h4>Edit Process</h4>
				<ol>
					<li>Select time period to update: <select id=ti name=t><option selected value=X>--- SELECT MONTH ---</option><?php 
						foreach($time_all as $ti => $t) {
							if(($t['active'] && $t['active_finance']) ) {
								echo "<option value=$ti>".$t['display_full']."</option>";
							}
						}
					?></select>
					<li>Export project details: <input type=button value=Export id=export /><br /><span style="font-style: italic">Please ensure that all new Cashflow Line Items have been added using the "Create" tab before clicking the "Export" button. </span></li>
					<li>Update the BUDGETED financial details in the CSV document saved in step 1. </li>
					<li>Import updated information:  <input type=file name=ifile id=ifl /> <input type=button value=Import id=import /><br />
						Note: Once you have clicked the "Import" button, you will be given an opportunity to review the Import data before finalising the import.<br />
						Until you click the "Accept" button, on the next page, no information will be updated. </li>
				</ol>
			<h4>Import file information: </h4>
				<ul>
					<li>This process is only for updating the BUDGET results.  To change the actuals, please use the Update function.  To update any other field, please use the Edit function.</li>
					<li>The first two lines will be treated as header rows and will not be imported. </li>
					<li>The data needs to be in CSV format, seperated by commas, in the same layout as the export file generated in step 2. </li>
					<li>The system will upload the financials to the line items identified by the Reference in the first column. Any lines that do not contain a valid Reference number will be ignored during the upload. If you wish to add a capital project please use the "Create" tab. </li>
				</ul>
		</form>
		<?php
		//	$log_sql = "SELECT clog_date, CONCAT_WS(' ',tkname,tksurname) as clog_tkname, clog_transaction FROM ".$dbref."_capital_log INNER JOIN assist_".$cmpcode."_timekeep ON clog_tkid = tkid WHERE clog_capid = '0' AND clog_yn = 'Y' ORDER BY clog_date DESC";
		//	$log_flds = array('date'=>"clog_date",'user'=>"clog_tkname",'action'=>"clog_transaction");
		//displayAuditLog($log_sql,$log_flds);
		?>
		<script type=text/javascript>
			$(document).ready(function() {
				$("#export").click(function() {
					var t = $("#ti").val();
					$("#ti").removeClass("required");
					if(t!="X" && t.length>0 && !isNaN(parseInt(t))) {
						document.location.href = 'manage_fin_export.php?page_id=<?php echo $page_id; ?>_budget&t='+t;
					} else {
						$("#ti").addClass("required");
						alert("Please select the relevant time period in step 1.");
					}
				});
				$("#import").click(function() {
					var f = $("#ifl").val();
					var t = $("#ti").val();
					$("#ti, #ifl").removeClass("required");
					if(t!="X" && t.length>0 && !isNaN(parseInt(t)) && f.length>0) {
						$("#up").submit();
					} else {
						err = "";
						if(t=="X" || t.length==0 || isNaN(parseInt(t))) {
							$("#ti").addClass("required");
							err = err+"\n - Select the relevant time period";
						}
						if(f.length==0) {
							$("#ifl").addClass("required");
							err = err+"\n - Select the import file";
						}
						if(err.length>0) {
							alert("Please attend to the following errors: "+err);
						}
					}
				});
			});
		</script>
		<?php





		echo "<h2>Edit Individual Cashflow Line Items</h2>";

		$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
			INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT c.cf_subid FROM ".$dbref."_cashflow c
			INNER JOIN ".$dbref."_subdir subdir ON cf_subid = subdir.id AND subdir.active = true INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
			WHERE cf_active = true ) WHERE d.active = true";
		$valid_dir_sub = $helperObj->mysql_fetch_all_fld2($dir_sql,"dir","subdir");


		//drawViewFilter($who,$what,$when,$sel,$filter)
		$head = "<h2 style=\"margin-top: 0px;\">";
			$head.= $time[$filter['when'][0]]['display_full'];
		$head.="</h2>";
		//$filter['who'] = drawViewFilter(true,false,true,1,true,$filter,$head);
		//drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form)
		//drawPaging_noSearch($self,'',0,15,1,10,'Reference',$self);


		//SQL
		$object_sql = "SELECT c.* FROM ".$dbref."_cashflow c
		INNER JOIN ".$dbref."_subdir subdir ON cf_subid = subdir.id AND subdir.active = true
		".($filter['who'][0]=="D" && $helperObj->checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
		INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE cf_active = true ".($filter['who'][0]=="S" && $helperObj->checkIntRef($filter['who'][1]) ? " AND cf_subid = ".$filter['who'][1] : "");
		
		$results_sql = "SELECT r.* FROM ".$dbref."_cashflow_results r
		INNER JOIN ".$dbref."_cashflow c ON r.cr_cfid = c.cf_id AND c.cf_active = true
		WHERE cr_timeid <= ".$filter['when'][0]." AND cr_cfid IN (SELECT c.cf_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
		
		$results = $helperObj->mysql_fetch_all_fld2($results_sql,"cr_cfid","cr_timeid");
		$rheadings = "CF_R";
		

		
		$object_sql.= " ORDER BY dir.sort, subdir.sort, c.cf_sort";
		
	//	include("view_table.php");
		include("manage_fin_table.php");

	}
	
}	//if import_status['import_Section'] == false else	
?>