<?php 
$page_title = "SDBIP 2010/2011 Users";
include("inc/header.php");
global $helperObj;
$sdp10_user_access = array(
	'kpi'		=> "KPI",
	'finance'	=> "FIN",
	'toplayer'	=> "TL",
	'assurance'	=> "AP",
	'second'	=> "TPS",
);

function noUsersForm() {
	global $self;
	?>
	<form action=<?php echo $self; ?> method=post><input type=hidden name=act value=SAVE />
	<p>This client does not have SDBIP 2010/2011 users available.</p>
	<p>Click the button to open the SDBIP 2011/2012 so that users can be granted menu access via the USers > Edit function available to the Ignite Assist Administrator user.</p>
	<p style="margin-left: 20px"><input type=submit value=Open class=isubmit /></p>
	</form>
	<?php
}
function displayHead() {
	global $user_access_fields;
		echo "	<tr>
					<th rowspan=2>&nbsp;</th>
					<th rowspan=2>User</th>
					<th colspan=7>User Access</th>
				</tr>
				<tr>";
				foreach($user_access_fields as $u) {
					echo "<th>$u</th>";
				}
		echo "	</tr>";
}



$import_log_file = openLogImport();
//fdata = "date("Y-m-d H:i:s")","$_SERVER['REMOTE_ADDR']","action","$self","implode(",",$_REQUEST)"
logImport($import_log_file, "Open Support > Import > Users", $self);

	$tk_done = array();

$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$modref."' AND modyn = 'Y'";
$menu = $helperObj->mysql_fetch_one($sql);//(getRS($sql));
//mysql_close();

$sql = "SELECT modid FROM assist_".$cmpcode."_menu_modules WHERE modmenuid = '".$menu['modid']."'";
$m = $helperObj->mysql_fetch_one($sql);//mysql_fetch_assoc(getRS($sql));
$menu['mm'] = $m['modid'];
//mysql_close();

$sql = "SELECT * FROM assist_menu_modules WHERE modref = 'SDP10' AND modyn = 'Y'";
$sdp10menu = $helperObj->mysql_fetch_one($sql);//mysql_fetch_assoc(getRS($sql));
//mysql_close();
$m_sql="";
if(count($sdp10menu)>1) {
	$sql = "SELECT id, tkid, type FROM assist_".$cmpcode."_sdp10_list_admins WHERE yn = 'Y' AND ref = 0";
	$user_access = $helperObj->mysql_fetch_all_fld2($sql,"tkid","type");

	$sql = "SELECT mmu.*, CONCAT_WS(' ',t.tkname,t.tksurname) as tkn
			FROM assist_".$cmpcode."_menu_modules_users mmu
			INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = mmu.usrtkid AND t.tkstatus = 1
			WHERE mmu.usrmodref = 'SDP10'
			ORDER BY t.tkname, t.tksurname
			";
	$m_sql = $sql;
	//$rs = getRS($sql);
	$mnr = $helperObj->db_get_num_rows($sql);
} else {
	$mnr = 0;
}
	$sql = "SELECT * FROM ".$dbref."_user_access WHERE active = true";
	$current_access = $helperObj->mysql_fetch_all_fld($sql,"tkid");



if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
	/*** SAVE ***/
	$h = 1;
			if($mnr > 0) {
				$usrs = $_REQUEST['tkid'];
				echo "<table width=500><tr><th></th><th>User</th><th>Result</th></tr>"; 
				//while($row = mysql_fetch_assoc($rs)) {
				$rs = $helperObj->mysql_fetch_all($m_sql);
				foreach ($rs as $row){
					$u = $row['usrtkid'];
					if(!in_array($u,$tk_done)) {
						echo "<tr>";
							$sql = "INSERT INTO assist_".$cmpcode."_menu_modules_users (usrid, usrmodid, usrtkid, usrmodref) VALUES (null, ".$menu['mm'].", '$u','$modref')";
							$helperObj->db_insert($sql);
							if(isset($current_access[$u])) {
								echo "<td class=\"ui-state-info\"><span class=\"ui-icon ui-icon-info\" style=\"margin-right: .3em;\"></span></td>";
								echo "<td>".$row['tkn']."</td>";
								echo "<td >User already has access</td>";
							} elseif(!in_array($u,$usrs)) {
								echo "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
								echo "<td>".$row['tkn']."</td>";
								echo "<td >User not imported</td>";
							} else {
								$ua = $_REQUEST[$u];
							$h++;
								$sql = "INSERT INTO ".$dbref."_user_access SET tkid = '$u', active = true";
								foreach($user_access_fields as $i => $l) { $sql.=", ".$i." = ".$ua[$i]; }
							//$rs2 = getRS($sql);
								echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
								echo "<td>".$row['tkn']."</td>";
								echo "<td >";
								echo "User imported.";
								echo "</td>";
							}
						echo "</tr>";
						$tk_done[] = $u;
					}
				}
				//mysql_close();
			} //end if mnr>0
			$sql = "UPDATE assist_".$cmpcode."_menu_modules SET moduserallowed = '-1', modusercount = '$h' WHERE modid = ".$menu['mm'];
			$helperObj->db_update($sql);
			echo "<p>Module has been opened to allow for new users to be added.</p>";
} else {	//else if act==save
	/*** VIEW ***/
			if($mnr>0) {
				echo "<form action=".$self." method=post><input type=hidden name=act value=SAVE /><table>";
				displayHead();
				$h = 0; $h2 = 0; 
				//while($row = mysql_fetch_assoc($rs)) {
				$rs = $helperObj->mysql_fetch_all($m_sql);
				foreach ($rs as $row){
					if(!in_array($row['usrtkid'],$tk_done)) {
						$h++; $h2++;
						if(!isset($user_access[$row['usrtkid']])) { $ua = array(); } else { $ua = $user_access[$row['usrtkid']]; }
						echo "	<tr>
									<th>".(!isset($current_access[$row['usrtkid']]) ? "<input type=checkbox name=tkid[] checked value=\"".$row['usrtkid']."\" />" :"")."</th>
									<td>".$row['tkn']."</td>";
									if(isset($current_access[$row['usrtkid']])) {
										foreach($user_access_fields as $u => $uaf) {
											echo "<td class=center>";
												echo $current_access[$row['usrtkid']][$u] ? "<span style=\"background-color: #009900; color: #FFFFFF;\">Yes</span>" : "<span style=\"color: #CC0001;\">No</span>";
											echo "</td>";
										}
									} else {
										foreach($user_access_fields as $u => $uaf) {
											echo "<td class=center>";
											if(isset($sdp10_user_access[$u])) {
												$u2 = $sdp10_user_access[$u];
												//echo (isset($ua[$u2]) ? "Yes" : "No")."<input type=text name=\"".$row['usrtkid']."['".$u."']\" value=".(isset($ua[$u2]) ? "1" : "0")." />";
												echo "<select name=\"".$row['usrtkid']."[".$u."]\"><option value=0 style=\"color: #cc0001;\" ".(!isset($ua[$u2]) ? "selected":"").">No</option><option style=\"background-color: #009900; color: #FFFFFF;\" value=1 ".(isset($ua[$u2]) ? "selected":"").">Yes</option></select>";
											} else {
												echo "<select name=\"".$row['usrtkid']."[".$u."]\"><option value=0 style=\"color: #cc0001;\" selected>No</option><option style=\"background-color: #009900;color: #ffffff;\" value=1>Yes</option></select>";
											}
											echo "</td>";
										}
									}
						echo "	</tr>";
						$tk_done[] = $row['usrtkid'];
					}
					if($h>=15 && !($h2>($mnr-2))) { displayHead(); $h = 0; }
				}

					echo "	<tr><th>&nbsp;</th><td colspan=8><input type=submit value=\"Import Users\" class=isubmit /></td>
				</table>";
			//mysql_close();
			} else {
				noUsersForm();
			}

}  //end if act == save

fclose($import_log_file);

?>
</body>
</html>