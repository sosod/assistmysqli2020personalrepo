<?php
$section = "TOP";
$fld_name = "top_";
$fixed = "top_dirid";
$unset = array("top_calctype","top_targettype");
$result = array("ok","Top Layer SDBIP Headings updated.");
$my_head = $mheadings[$section];
foreach($unset as $u) {
	unset($my_head[$u]);
}


if(isset($_REQUEST['act']) && $_REQUEST['act'] == "SAVE") {
	foreach($_REQUEST['sort'] as $sort => $fld) {
		$c_required = "false";
		$c_sort = $sort;
		$c_list = "false";
		$glossary = "";
		$f = $fld."_required";	if(isset($_REQUEST[$f]) && $_REQUEST[$f] == "on") { $c_required = "true"; }
		$f = $fld."_list"; 		if(isset($_REQUEST[$f]) && $_REQUEST[$f] == "on") { $c_list = "true"; }
		$f = $fld."_glossary";	$glossary = ASSIST_HELPER::code($_REQUEST[$f]);
		$sql = "UPDATE ".$dbref."_setup_headings_setup SET c_required = $c_required , c_sort = $c_sort , c_list = $c_list , glossary = '$glossary' WHERE section = '$section' AND field = '$fld'";
		$mar = $helperObj->db_update($sql);
	}
	$mheadings = getModuleHeadings(array());
    $helperObj->displayResult($result);
}
?>
<form name=dept action='<?php echo $self; ?>' method=post>
<input type=hidden name=page_id value='<?php echo $page_id; ?>' />
<input type=hidden name=act value=SAVE />
<table id=sortme>
	<thead>
	<tr>
		<th>Heading</th>
		<th>Required</th>
		<th>List View</th>
		<th>Text for Glossary</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach($my_head as $key => $h) {
		echo "<tr style=\"cursor:hand; verical-align: top\">";
			echo "<td ><span class=\"ui-icon ui-icon-arrowthick-2-n-s\" style=\"float: left; margin-right: .3em;\"></span>";
			echo "<input type=hidden name=sort[] value=".$key." />".$h['h_client']."</td>";
			echo "<td class=centre>";
				if($h['i_required']) {
					echo "<div align=center><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></div>";
				} else {
					echo "<input type=checkbox name=".$key."_required ".($h['c_required'] ? "checked" : "")." />";
				}
			echo "</td>";
			echo "<td class=centre>";
				if($h['fixed'] || $key == $fixed) {
					echo "Fixed";
				} else {
					echo "<input type=checkbox name=".$key."_list ".($h['c_list'] ? "checked" : "")." />";
				}
			echo "</td>";
			echo "<td><textarea cols=60 rows=3 name=".$key."_glossary>".$helperObj->decode($h['glossary'])."</textarea></td>";
		echo "</tr>";
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan=4 class=centre>
				<input type=submit value="  Save  " />
			</td>
		</tr>
	</tfoot>
</table>
</form>
	<script>
	$(function() {
		$( "#sortme tbody" ).sortable().disableSelection();
	});
	</script>