<?php
global $helperObj;
$log_section = "HEAD";
/* Get defaults from $dbref_setup_defaults table */
$sql = "SELECT h_id, h_section, h_client, h_ignite FROM ".$dbref."_setup_headings INNER JOIN ".$dbref."_setup_headings_setup ON head_id = h_id WHERE h_active = true AND h_can_rename = true ORDER BY i_sort, h_id";
//$sql = "SELECT * FROM ".$dbref."_setup_headings WHERE h_active = true ORDER BY h_table";
$all_heads = array();
$save = array();
//$rs = getRS($sql);
$rs = $helperObj->mysql_fetch_all($sql);
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rs as $row){
	$all_heads[$row['h_section']][$row['h_id']] = $row;
	$save[$row['h_id']] = array('h_client'=>$row['h_client'],'h_ignite'=>$row['h_ignite'],'h_section'=>$row['h_section']);
}

$section = array(
	'ALL'=>array("General","These are lists which affect more than 1 section of the SDBIP."),
	'KPI'=>array("Departmental SDBIP",""),
	'TOP'=>array("Top Layer SDBIP",""),
	'CAP'=>array("Capital Projects",""),
	'CF'=>array("Monthly Cashflows",""),
	'RS'=>array("Revenue By Source","")
);

//arrPrint($save);
/* PROCESS SAVE */
$var = $_REQUEST;
//arrPrint($var);
if(isset($var['act']) && $var['act']=="SAVE") {
	$changes = array();
	foreach($save as $c => $v) {
		if(isset($var[$c])) {
			if(strlen($var[$c])>0) {
				if($var[$c] != $v['h_client']) { $changes[$c] = $var[$c]; }
			} else {
				$changes[$c] = $v['h_ignite'];
			}
		}
	}
	if(isset($changes) && count($changes)>0) {
		foreach($changes as $hid => $value) {
			$sql = "UPDATE ".$dbref."_setup_headings SET h_client = '".$helperObj->code($value)."' WHERE h_id = '$hid'";
			$mar = $helperObj->db_update($sql);
			if($mar>0) {
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$hid,
											'field'=>"h_client",
											'text'=>"Changed ".$save[$hid]['h_ignite']." to <i>".code($value)."</i> from <i>".$save[$hid]['h_client']."</i> (".$section[$save[$hid]['h_section']][0].").",
											'old'=>$save[$hid]['h_client'],
											'new'=>$value,
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
				$save[$hid]['h_client'] = $value;
			}
		}
		$result = array("ok","Changes saved.");
		$mheadings = getModuleHeadings(array());
	} else {
		$result = array("error","No changes found to save.");
	}

}


$helperObj->displayResult($result);
?>
<script>
$(document).ready(function(){
	//If a text field changes, then highlight the field
	$(":text").blur(function(){
		var fld = $(this).prop("name");
		if(fld.length > 0 && $(this).val() != $("#"+fld+"_old").val()) {
			$(this).addClass('ui-state-info');
		} else {
			$(this).removeClass('ui-state-info');
		}
	});
	//on reset, remove all highlighting
	$(":reset").click(function(){
		$(":text").removeClass('ui-state-info');
	});
	//single row save button click
	$(":button").click(function() {
		var fld = $(this).prop("id");
		var id = $(this).prop("name");
		document.location.href = '<?php echo $self; ?>?act=SAVE&'+fld+'='+escape($("#"+id).val());
	});
});
</script>
<div style="width:550px">
<ul>
	<li>Any changes will be highlighted in <span class=ui-state-info>orange</span> until they have been saved.</li>
	<li>Any blank fields will default back to the Default Terminology.</li>
	<li>You can either click the "Save All" button at the bottom of the page to save all the changes made, or you can use the "Save" button to save a single change.</li>
</ul>
</div>
<form name=head method=post action=<?php echo $self; ?> >
<input type=hidden name=act value=SAVE />
<?php
foreach($section as $s => $sec) {
	?>
	<h2><?php echo $sec[0]; ?></h2>
	<?php
		if(strlen($sec[1])>0) { echo "<P>".$sec[1]."</p>"; }
	?>
	<table width=510>
		<tr>
			<th>Default Terminology</th>
			<th>Client Terminology</th>
			<th>Action</th>
		</tr>
	<?php
   $all_heads_s = isset($all_heads[$s]) ? $all_heads[$s] : array();
    foreach($all_heads_s as $row) { ?>
		<tr>
			<td width=230><?php echo $row['h_ignite']; ?></td>
			<td width=230>
				<?php echo "<input type=text size=40 value=\"".$helperObj->decode($save[$row['h_id']]['h_client'])."\" name=\"".$row['h_id']."\" id=\"".$row['h_id']."\" />"; ?>
				<?php echo "<input type=hidden value=\"".$helperObj->decode($save[$row['h_id']]['h_client'])."\" id=\"".$row['h_id']."_old\"  /> "; ?>
			</td>
			<td class=centre width=50><?php echo "<input type=button value=Save id=\"".$row['h_id']."\" name=\"".$row['h_id']."\" />"; ?></td>
		</tr>
	<?php } ?>
	</table>
	<?php
}
?>
<p>&nbsp;</p>
<table width=510>
	<tr>
		<td class=centre>&nbsp;<input type=submit value="Save All" /> <input type=reset /></td>
	</tr>
</table>
</form>
<?php 
goBack("setup.php","Back to Setup"); 
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>