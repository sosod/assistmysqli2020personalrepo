<style type=text/css>
table th {
	border-left: 0px solid #000099;
	border-right: 0px solid #000099;
	border-top: 1px solid #000099;
}
table td {
	border-left: 0px solid #ffffff;
	border-right: 0px solid #ffffff;
}
</style>
<?php
/*****************
PAGE TO DISPLAY SETUP OF PLC PHASES
 **************
Preprequisite:
	$tbl => which list table to access
	$days=> make allowances for pre-determining days true/false (procurement)
	$required=> does table allow for required field true/false (not additional)
*****************/

if($page_id=="proc") {
	echo "<form action=$self method=post>
		<input type=hidden name=page_id value=".$page_id." />
		<input type=hidden name=act value=Q />
		<table width=300 class=noborder>
			<tr>
				<td class=noborder>".$plc_questions[4]['descrip']."&nbsp;&nbsp;<span class=float><select name=proc[4]>
					<option ".($plc_questions[4]['value']!="N" ? "selected" : "")." value=Y>Yes</option>
					<option ".($plc_questions[4]['value']=="N" ? "selected" : "")." value=N>No</option>
				</select></span></td>
			</tr>
			<tr>
				<td class=noborder>".$plc_questions[5]['descrip']."&nbsp;&nbsp;<span class=float><select name=proc[5]>
					<option ".($plc_questions[5]['value']!="N" ? "selected" : "")." value=Y>Yes</option>
					<option ".($plc_questions[5]['value']=="N" ? "selected" : "")." value=N>No</option>
				</select></span></td>
			</tr>
			<tr>
				<td class=noborder><input type=submit value=Save class=isubmit /><input type=reset /></td>
			</tr>
		</table>
		</form>";
}


/* Get phases from database */
$sql = "SELECT * FROM ".$dbref."_list_plc_".$tbl." WHERE yn IN ('Y','R') ORDER BY ".($tbl=="custom" ? "value" : "sort");
$phases = $helperObj->mysql_fetch_all_fld($sql,"id");
/* Add new phase */
echo "<P><input type=button value=\"Add new phase\" id=but_add /></p>";
/* Display phases in table */
echo "	<table id=phases>
			<tr>
				<th width=50>Ref</th>
				<th>Phase</th>";
if($required) { echo "				<th width=60>Required</th>";	}
if($days) { 
	for($d=1;$d<=4;$d++) {
		echo "				<th>Category ".$d."</th>";
	}
}
echo 			"<th width=65></th>
			</tr>";
foreach($phases as $pi => $pp) {
	if(isset($pp['type']) && $pp['type']!="S") {
		$pi = $pp['type'];
	}
	echo "	<tr height=27>
				<td class=\"center b\">$pi</td>
				<td id=val".$pi.">".$helperObj->decode($pp['value'])."</td>";
	if($required) { echo "			<td class=center id=req".$pi.">".($pp['yn']=="R" ? "Yes" : "No")."</td>";	}
if($days) { 
	for($d=1;$d<=4;$d++) {
		echo "				<td class=center id=d".$d."-".$pi.">".$pp['days'.$d]." day".($pp['days'.$d]>1?"s":"")."</th>";
	}
}
	echo "		<td class=right>".($helperObj->checkIntRef($pi) ? "<input type=button value=Edit id=$pi />" : "")."</td>
			</tr>";
}
echo "	</table>";

/* JQUERY */
$sort = 0;
if($tbl!="custom") {
	$sql = "SELECT max(sort) as sort FROM ".$dbref."_list_plc_".$tbl;
	$row = $helperObj->mysql_fetch_one($sql);
	$sort = $row['sort'];
}
?>
<div id=dialog_add title="Add new phase">
	<form id=frm_add action=<?php echo $self; ?> method=post>
		<input type=hidden name=page_id value="<?php echo $page_id; ?>" />
		<input type=hidden name=act value=NEW />
		<input type=hidden name=sort value=<?php echo $sort; ?> />
		<h2 style="margin-top: 5px; margin-bottom: 5px">Add New Phase</h2>
		<table id=tbl_add>
			<tr>
				<th>Phase:</th>
				<td><input type=text size=50 maxlength=100 id=valu name=val /></td>
			</tr>
<?php if($required) { ?>
			<tr>
				<th>Required:</th>
				<td><select name=req id=req>
					<option value=Y selected>No</option>
					<option value=R>Yes</option>
				</select></td>
			</tr>
<?php 
} else { 
	echo "<input type=hidden name=req id=req value=Y />"; 
} ?>
<?php if($days) { for($d=1;$d<=4;$d++) { ?>
			<tr>
				<th>Category <?php echo $d; ?>:</th>
				<td><input type=text size=5 value=1 name=<?php echo "days[".$d."]"; ?> /> days</td>
			</tr>
<?php 
	}
} ?>
			<tr>
				<th></th>
				<td><input type=button value=Save class=isubmit id=save /> <input type=reset /> <input type=button value=Cancel id=cancel /></td>
			</tr>
		</table>
	</form>
</div>
<div id=dialog_edit title="Edit phase">
	<form id=frm_edit action=<?php echo $self; ?> method=post>
		<input type=hidden name=page_id value="<?php echo $page_id; ?>" />
		<input type=hidden name=act value=EDIT />
		<h2 style="margin-top: 5px; margin-bottom: 5px">Edit Phase</h2>
		<table id=tbl_edit>
			<tr>
				<th>Ref:</th>
				<td><label for=i id=il></label><input type=hidden name=id value="" id=i /></td>
			</tr>
			<tr>
				<th>Phase:</th>
				<td><input type=text size=50 maxlength=100 id=valu name=val /></td>
			</tr>
<?php if($required) { ?>
			<tr>
				<th>Required:</th>
				<td><select name=req id=req>
					<option value=Y>No</option>
					<option value=R>Yes</option>
				</select></td>
			</tr>
<?php 
} else { 
	echo "<input type=hidden name=req id=req value=Y />"; 
} ?>
<?php if($days) { for($d=1;$d<=4;$d++) { ?>
			<tr>
				<th>Category <?php echo $d; ?>:</th>
				<td><input type=text size=5 value=1 <?php echo "name=days[".$d."] id=d".$d; ?> /> days</td>
			</tr>
<?php 
	}
} ?>
			<tr>
				<th></th>
				<td><input type=button value=Save class=isubmit id=save /> <input type=reset />  <input type=button value=Cancel id=cancel /> <span class=float><input type=button class=idelete value="Delete phase" id=delete /></td>
			</tr>
		</table>
	</form>
</div><script type=text/javascript>
$(document).ready(function() {
	$("#tbl_add th").addClass("left");
	$("#tbl_edit th").addClass("left");
	$("#dialog_add").dialog({
		autoOpen: false, 
		width: 500, 
		height: 300,
		modal: true
	});
	$("#dialog_edit").dialog({
		autoOpen: false, 
		width: 500, 
		height: 300,
		modal: true
	});

	$("#phases input:button").click(function() {
		var i = $(this).attr("id");
		var v = $("#phases #val"+i).html();
		var r = $("#phases #req"+i).html();
		if(r=="Yes") { r = "R"; } else { r = "Y"; }
		$("#dialog_edit").dialog("open");
		$("#tbl_edit #valu").val(v);
		$("#tbl_edit #req").val(r);
		$("#tbl_edit #i").val(i);
		$("#tbl_edit #il").html(i);
		<?php if($days) { 
			echo "var d = '';";
			for($d=1;$d<=4;$d++) {
			echo "d = $(\"#phases #d".$d."-\"+i).attr(\"innerText\");
			d = d.substr(0,d.indexOf(' '));
			$(\"#tbl_edit #d".$d."\").val(d);";
		} } ?>
	});
	$("#tbl_edit #save").click(function() {
		var v = $("#tbl_edit #valu").val();
		if(v.length>0) {
			$("#frm_edit").submit();
		} else {
			alert("Please enter the phase name.");
		}
	});
	$("#tbl_edit #delete").click(function() {
		if(confirm("Are you sure you wish to deactivate this phase?")) {
			var i = $("#tbl_edit #i").val();
			document.location.href = '<?php echo $self; ?>?page_id=<?php echo $page_id; ?>&act=DELETE&id='+i;
		}
	});
	
	$("#but_add").click(function() {
		$("#tbl_add #valu").val("");
		$("#tbl_add #req").val("N");
		$("#dialog_add").dialog("open");
		$("#tbl_add #valu").focus();
	});
	$("#tbl_add #cancel").click(function() {$("#dialog_add").dialog("close");});
	$("#tbl_add #save").click(function() {
		var v = $("#tbl_add #valu").val();
		if(v.length>0) {
			$("#frm_add").submit();
		} else {
			alert("Please enter the phase name.");
		}
	});
	
});
</script>