<?php 
if(!isset($_REQUEST['act']) || (isset($_REQUEST['tab']) && $_REQUEST['tab']=="create") || !in_array($_REQUEST['act'],array("REVIEW","SAVE"))) { 
	if(!isset($_REQUEST['act'])) { $_REQUEST['act'] = ""; }
	if(isset($_REQUEST['r'])) { echo "<div id=dispRes>"; $helperObj->displayResult($_REQUEST['r']); echo "</div>"; }
?>
<script type=text/javascript>
	$(function() {
		$("#tabs").tabs({
		});
	});
</script>

<DIV id=tabs>
	<UL>
		<LI id=tab_update><A href="#update">Update</A></LI>
		<LI id=tab_edit><A href="#edit">Edit</A></LI>
		<LI id=tab_create><A href="#create">Create</A></LI> 
	</UL>
	<DIV id=update>
		<?php if(!isset($tab) || $tab=="update") { include("manage_fin_".$page_id."_update.php"); } else { echo "Loading..."; } ?>
	</DIV>
	<DIV id=edit>
		<?php if(isset($tab) && $tab == "edit") { include("manage_fin_".$page_id."_edit.php"); } else { echo "Loading..."; } ?>
	</div>
	<DIV id=create>
		<?php if(isset($tab) && $tab == "create") { include("manage_fin_".$page_id."_create.php"); } else { echo "Loading..."; } ?>
	</DIV> 
</DIV>
<script type=text/javascript>
$(function() {
	$("#tabs").tabs( {active: <?php echo $tab_index; ?> });

	setTimeout(function () { $("#dispRes").attr("innerText","") }, 2500);

	$("#tabs").on( "tabsbeforeactivate", function(event, ui) {
				var url = "";
				switch(ui.newTab.prop("id")) {
					case "tab_update": url = '<?php echo $self."?page_id=".$page_id."&tab=update"; ?>'; break;
					case "tab_edit": url = '<?php echo $self."?page_id=".$page_id."&tab=edit"; ?>'; break;
					case "tab_create": url = '<?php echo $self."?page_id=".$page_id."&tab=create"; ?>'; break;
				}
				document.location.href = url;
			});

	
});
</script>
<?php } else {	//no action defined

include("manage_fin_".$page_id."_".$tab.".php");

}	//no action defined
?>