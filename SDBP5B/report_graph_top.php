<?php 
$section = "TOP";
$get_lists = true; $get_active_lists = false;
include("inc/header.php"); 
include("inc/header_report.php"); 
include("report_graph_amcharts.php");
global $helperObj;
//arrPrint($_REQUEST);
$current_time_id = (ceil($current_time_id/3)==$current_time_id/3) ? $current_time_id : ceil($current_time_id/3)*3;

$generator = $_REQUEST;
if(!isset($generator['report_title']) || strlen($generator['report_title'])==0) { $generator['report_title'] = "Top Layer SDBIP Report"; }
if(!isset($generator['from'])) { $generator['from'] = 1; }
if(!isset($generator['to'])) { $generator['to'] = $current_time_id; }
if($generator['from']>$generator['to']) { $x = $generator['to']; $generator['to'] = $generator['from']; $generator['from'] = $x; }
$display_grid = isset($_REQUEST['bar_grid']) && strlen($_REQUEST['bar_grid'])>0 ? $_REQUEST['bar_grid'] : 0;

$filter_from = $generator['from'];
$filter_to = $generator['to'];
$kpi_nm = isset($generator['kpi_notmeasured']) ? $generator['kpi_notmeasured'] : "EXCLUDE";//$kpi_nm = $generator['kpi_notmeasured'];
$kpi_met = isset($generator['kpi_met']) ? $generator['kpi_met'] : "SEPARATE";//$kpi_met = $generator['kpi_met'];
$x = isset($generator['dirsub_filter']) ? $generator['dirsub_filter'] : "ALL";//$x = $generator['dirsub_filter'];
$graph_display = isset($_REQUEST['display']) ? $_REQUEST['display'] : "ONS";
$report_settings = array();
$report_settings['r_from'] = $filter_from;
$report_settings['r_to'] = $filter_to;
$report_settings['groupby'] = "";
$report_settings['path'] = "report_top.php";

if($x=="ALL") {
	$filter_who = array("X",0);
	$groupby = isset($generator['groupby']) ? $generator['groupby'] : "obj_dirid";//$groupby = "obj_dirid";
	$graph_title['main'] = $cmpname;
	$graph_title['sub'] = $head_dir;
	$report_settings['groupby'] = $groupby=="obj_dirid" ? "top_dirid" : $groupby;
} else {
	$filter_who = explode("_",$x);
		$groupby = isset($generator['groupby']) ? $generator['groupby'] : "NA";//$groupby = "NA";
		$graph_title['main'] = $lists['dir'][$filter_who[1]]['value'];
		$graph_title['sub'] = "";
		$report_settings['groupby']="X";
}

if(!isset($_REQUEST['do_group']) || $_REQUEST['do_group']=="N") { 	
	$groupby="NA";
	$graph_title['sub'] = "";
	$report_settings['groupby']="X";
}

$graph_type = isset($generator['graph_type']) ? $generator['graph_type'] : "BAR_REG";
$graph_type = explode("_",$graph_type);
$page_layout = isset($generator['page_layout']) ? $generator['page_layout'] : "LAND";
//BAR GRAPH IS LIMITED TO 9 COLUMNS ELSE CATEGORY AXIS DOES NOT SHOW ALL VALUES
if($graph_type[0]=="BAR") {
	switch($page_layout) {
	case "PORT":
		$max[0] = 6;
		$max[1] = 6;
		break;
	case "LAND":
	default:
		$max[0] = 8;
		$max[1] = 8;
		break;
	}
} else {
	$max[0] = 1; $max[1] = 1;
}

switch($groupby) {
case "obj_dirid":	$graph_title['sub'] = $head_dir;	break;
//case "obj_subid":	$graph_title['sub'] = $head_sub;	break;
case "NA":			$graph_title['sub'] = "";	break;
default:			$graph_title['sub'] = $mheadings[$section][$groupby]['h_client']; break;
}

$blurb = "";
if($generator['from']==$generator['to']) {
	$blurb = "<br />for the month of ".$time[$generator['from']]['display_full'];
} else {
	$blurb = "<br />for the months of ".$time[$generator['from']]['display_full']." to ".$time[$generator['to']]['display_full'];
}


?>
<style type=text/css>
//.noborder { border: 2px dashed #fe9900; }
</style>
<h1 class=center style="margin-top: 0;margin-bottom: 0;"><?php echo $generator['report_title']; ?></h1>
<?php
		echo "<p class=\"center i\" style=\"margin-top: 0px; font-size: 7pt;\">Report drawn on ".date("d F Y")." at ".date("H:i").$blurb.".</p>";


	//GET DATA
		$object_sql = " ".$dbref."_".$table_tbl." o
			INNER JOIN ".$dbref."_dir dir ON ".$table_fld."dirid = dir.id AND dir.active = true";
		switch($filter_who[0]) {
		case "D":
			$object_sql.= " AND dir.id = ".$filter_who[1];
			break;
		}
		$object_sql.= "
			WHERE ".$table_fld."active = true ";
		
		$results_sql = "SELECT r.*, ".(!in_array($groupby,array("obj_subid","obj_dirid","NA")) ? "o.".$groupby."," : "")."dir.id as obj_dirid, o.".$table_fld."calctype as obj_ct, o.".$table_fld."targettype as obj_tt FROM ".$dbref."_".$table_tbl."_results r
			INNER JOIN ".$dbref."_".$table_tbl." o ON r.".$r_table_fld.$table_id."id = o.".$table_fld."id AND o.".$table_fld."active = true
			INNER JOIN ".$dbref."_dir dir ON o.".$table_fld."dirid = dir.id AND dir.active = true
			WHERE ".$r_table_fld."timeid >= ".$filter_from." AND ".$r_table_fld."timeid <= ".$filter_to." AND ".$r_table_fld.$table_id."id IN (SELECT o.".$table_fld."id FROM ".$object_sql.")";
		if(count($_REQUEST['repcate'])>0) {
			$repcate = array();
			foreach($_REQUEST['repcate'] as $r) {
				if($r=="0" || $r==0) {
					$repcate[] = "o.".$table_fld."repcate = '".$r."'";
				} else {
					$repcate[] = "o.".$table_fld."repcate LIKE ';".$r.";'";
				}
			}
			$results_sql.= " AND (".implode(" OR ",$repcate).")";
		}
		//echo $results_sql;
		$results = $helperObj->mysql_fetch_all_fld2($results_sql,"".$r_table_fld.$table_id."id",$r_table_fld."timeid");
		
		$count = array();
		$sub_count = array();
	//CALCULATIONS
			foreach($results as $obj_id => $result_object) {
				$obj_tt = $result_object[$filter_from]['obj_tt'];
				$obj_ct = $result_object[$filter_from]['obj_ct'];
				$values = array('target'=>array(),'actual'=>array());
				for($ti=3;$ti<=$filter_to;$ti+=3) {
						$values['target'][$ti] = isset($result_object[$ti][$fld_target]) ? $result_object[$ti][$fld_target] : 0;
						$values['actual'][$ti] = isset($result_object[$ti][$fld_actual]) ? $result_object[$ti][$fld_actual] : 0;
				}
				$r = KPIcalcResult($values,$obj_ct,array($filter_from,$filter_to),"ALL");
				if($kpi_met=="COMBINE" && $r['id']>3) { $r['text'] = "G"; }
				if($kpi_nm=="INCLUDE" || $r['id']>0) {
					if(!isset($count[$r['text']])) { $count[$r['text']] = 0; }
					$count[$r['text']]++;
					if($groupby!="NA") {
						$s_id = $result_object[$filter_from][$groupby];
						if(!isset($sub_count[$s_id][$r['text']])) { $sub_count[$s_id][$r['text']] = 0; }
						$sub_count[$s_id][$r['text']]++;
					}
				}
			}
		$total = array_sum($count);
	
include("report_graph_display.php");	
		
/*		
		foreach($result_settings as $ky => $k) {
			$k['obj'] = isset($count[$k['text']]) ? $count[$k['text']] : 0;
			$val = array();
			foreach($k as $key => $s) {
				$val[] = $key.":\"".$s."\"";
			}
			$kr2[$ky] = implode(",",$val);
		}
		$chartData = "{".implode("},{",$kr2)."}";
		$sub_chartData = array();
		$sub_ids = array();
		if($groupby!="NA") {
			switch($groupby) {
			case "obj_subid": case "obj_dirid":
				if($filter_who[0]=="X" ) { 
					$my_lists = $lists['dir']; 
				} else {
					$my_lists = $lists['subdir']['dir'][$filter_who[1]];
				}
				break;
			default:
				//arrPrint($mheadings[$section]);
				$my_lists = $lists[$mheadings[$section][$groupby]['h_table']];
			}
			//echo $groupby; arrPrint($my_lists);
			if($graph_type[0]=="PIE") {
				foreach($my_lists as $s_id => $s) { 
					if(isset($sub_count[$s_id])) { 
						$sub_ids[$s_id] = $s['value'];
						$kr2 = array();
						foreach($result_settings as $ky => $k) {
							$k['obj'] = isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0;
							$val = array();
							foreach($k as $key => $s) {
								$val[] = $key.":\"".$s."\"";
							}
							$kr2[$ky] = implode(",",$val);
						}
						$sub_chartData[$s_id] = "{".implode("},{",$kr2)."}";
					}
			
				}
			} else {
				$max_count = 0;
				$row_count = 0;
				$sub_row = 0;
				$max_kpis = 0;
				$valueAxis_maximum = 0;
				foreach($my_lists as $s_id => $s) { 
					if(isset($sub_count[$s_id])) { 
						$max_count++; if($max_count>$max[$row_count]) { $sub_row++; $sub_chartData[$sub_row] = array(); $sub_ids[$sub_row] = array(); $max_count = 1; $row_count = 1; }
						$sval = explode(" ",decode($s['value']));
						$s_name = "";
						$s_n = "";
						$sn_rows = 1;
						foreach($sval as $sv) {
							if(strlen($s_n.$sv)>18) {
								$s_name.=$s_n."\\n";
								$s_n = "";
								$sn_rows++;
							}
							$s_n.=" ".$sv;
							if($sn_rows>4) { $s_name = substr($s_name,0,-2); $s_n="..."; break; } //else {$s_n.=$sn_rows; }
						}
						$s_name.=" ".$s_n;
						if($sn_rows>3) { $s_name = "\\n".$s_name; }
						$cD = "{text:\"".$s_name."\"";
						foreach($result_settings as $ky => $k) {
							$cD.= ",R".$k['id'].":".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
							$max_kpis+=(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
						}
						$cD.= "}";
						$sub_chartData[$sub_row][] = $cD;
						$sub_ids[$sub_row][] = $s_id;
						$valueAxis_maximum = $max_kpis > $valueAxis_maximum ? $max_kpis : $valueAxis_maximum;	$max_kpis = 0;
					}
			
				}
			}
			
		}
	
if($graph_type[0]=="PIE") {
	drawPieGraph($report_settings,$page_layout,$graph_title,array('NM'=>$kpi_nm,'MET'=>$kpi_met),$result_settings,$count,$chartData,$graph_type[1],$sub_chartData,$my_lists,$sub_ids,$sub_count);
} else {
	drawBarGraph($report_settings,$page_layout,$graph_title,array('NM'=>$kpi_nm,'MET'=>$kpi_met),$result_settings,$count,$chartData,$graph_type[1],$valueAxis_maximum,$sub_chartData,$my_lists,$sub_ids,$sub_count);
}		
	*/


	?>
</body></html>