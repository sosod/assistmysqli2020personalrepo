<?php 
if(isset($_REQUEST['section'])) {
	$graph_section = $_REQUEST['section'];
	$graph_id = $_REQUEST['id'];
	switch($graph_section) {
	case 'D':	$section = "KPI";	$page_title = "Departmental SDBIP"; break;
	case 'T':	$section = "TOP";	$page_title = "Top Layer SDBIP"; break;
	case 'C':	$section = "CAP";	$page_title = "Capital Projects"; break;
	case 'M':	$section = "CF";	$page_title = "Monthly Cashflow"; break;
	}
} else {
	$section = "KPI";
}
$get_lists = true;
$get_active_lists = true;
$locked_column = false;
$amcharts = "java";

include("inc/header.php"); 


if(!isset($_REQUEST['act']) || $_REQUEST['act']=="SELECT") {
	$width=80;
	echo "<table width=700>
			<tr>
				<th>".$head_dir."</th>
				<th width=$width>Departmental<br />SDBIP</th>
				<th width=$width>Top Layer<br />SDBIP</th>
				<th width=$width>Capital<br />Projects</th>
				<th width=$width>Monthly<br />Cashflow</th>
		</tr>";
	$list_dir = isset($lists['dir']) ? $lists['dir'] : array();
	foreach($list_dir as $i => $l) {
		echo 	"<tr>
					<td style=\"font-weight: bold;\">".$l['value']."</td>
					<td class=center><input type=button value=View id=D_".$i." /></td>
					<td class=center><input type=button value=View id=T_".$i." /></td>
					<td class=center><input type=button value=View id=C_".$i." /></td>
					<td class=center><input type=button value=View id=M_".$i." /></td>
				</tr>";
	}
	echo "</table>";
?><script type=text/javascript>
$(function() {
	$("input:button").click(function() {
		var i = $(this).attr("id");
		var sec = i.substring(0,1);
		var id = i.substring(2);
		document.location.href = '<?php echo $self; ?>?act=VIEW&section='+sec+'&id='+id;
	});
});
</script><?php


} else {

	$filter_to = $current_time_id;
	$filter_from = ($graph_section=="T") ? 3 : 1;
	$filter_to = ($graph_section=="T") ? ceil($filter_to/3)*3 : $filter_to;

echo "<p style=\"font-style: italic;\">Year to date (".$time[$filter_from]['display_full']." - ".$time[$filter_to]['display_full'].") as at ".date("d F Y",$today)." at ".date("H:i:s",$today).".</p>";

	switch($graph_section) {
	case 'D':	include("view_dash_dept.php");		break;
	case 'T':	include("view_dash_top.php");		break;
	case 'C':	include("view_dash_capital.php");	break;
	case 'M':	include("view_dash_cashflow.php");	break;
	}

echo '
<script type=text/javascript>
$(function() {
	$("tr").off("mouseenter mouseleave");
});
</script>';

}
?>
</body>
</html>