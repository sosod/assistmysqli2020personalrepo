<?php 
$get_lists = true;
$get_active_lists = true;
$section = "KPI";
$page_title = "Projet Life Cycle";
$obj_id = $_REQUEST['id'];
$jquery_date = true;
include("inc/header.php"); 
global $helperObj;
if(!isset($_REQUEST['plc_kpiid']) || !$helperObj->checkIntRef($_REQUEST['plc_kpiid'])) {
	die($helperObj->displayResult(array("error","An error occurred.  Please go back and try again.")));
}

//arrPrint($_REQUEST);

/**********************************
********** FUNCTIONS **************
**********************************/

function calcNewValue(&$f_plc_values,$pp) {
				$m = date("m",strtotime($pp['end']));
				if($m>6) { $m-=6; } else { $m+=6; }
				if(!isset($f_plc_values['target'][$m])) { $f_plc_values['target'][$m] = 0; }
				$f_plc_values['target'][$m]+=$pp['perc'];
				if(isset($pp['done']) && $pp['done']=="Y" && strlen($pp['donedate'])>0) {
					$m = date("m",strtotime($pp['donedate']));
					if($m>6) { $m-=6; } else { $m+=6; }
					if(!isset($f_plc_values['actual'][$m])) { $f_plc_values['actual'][$m] = 0; }
					$f_plc_values['actual'][$m]+=$pp['perc'];
				}
}

function echoPhase($fld,$a) {
	$v = "";
	switch($fld) {
	case "use":
	case "done":
		if($a=="Y") { $v = "Yes"; } elseif($a=="R") { $v="Required"; } else { $v = "No"; }
		break;
	case "days":
		$v = $a." day".($a!=1?"s":"");
		break;
	case "start":
	case "end":
		$v = strtotime($a)!=0 ? date("d M Y",strtotime($a)) : "";
		break;
	case "donedate":
		$v = $a!=0 ? date("d M Y",$a) : "";
		break;
	case "perc":
		$v = $a." %";
		break;
	}
	
	return $v;
}

function echoPhaseRow($plc_id,$pp,$pi,$pt) {
	global $authorisation_required;
	global $dbref;
	if($pp['done']) {
		if(strlen($pp['donedate'])==0) { $pp['donedate'] = 0; } else { $pp['donedate']=strtotime($pp['donedate']); }
	} else {
		$pp['donedate'] = 0;
	}
	$sql = "";
				$fld = "use";
			echo "<td class=center>".echoPhase($fld,$pp[$fld])."</td>";
				$fld = "days";
			echo "<td class=center>".($pp['use']!="N" ? echoPhase($fld,$pp[$fld]) : "")."</td>";
				$fld = "start";
			echo "<td class=center>".($pp['use']!="N" ? echoPhase($fld,$pp[$fld]) : "")."</td>";
				$fld = "end";
			echo "<td class=center>".($pp['use']!="N" ? echoPhase($fld,$pp[$fld]) : "")."</td>";
				$fld = "perc";
			echo "<td class=center>".($pp['use']!="N" ? echoPhase($fld,$pp[$fld]) : "")."</td>";
				$fld = "done";
			echo "<td class=center>".($pp['use']!="N" ? echoPhase($fld,isset($pp[$fld]) ? $pp[$fld] : "N") : "")."</td>";
				$fld = "donedate";
			echo "<td class=center>".($pp['use']!="N" ? echoPhase($fld,$pp[$fld]) : "")."</td>";
		if($pp['use']!="N") {
			if($authorisation_required) {
			} else {
				$sql = "INSERT INTO ".$dbref."_kpi_plc_phases (pp_id,pp_plcid,pp_type,pp_phaseid, pp_days,pp_startdate,pp_enddate,pp_perc, pp_done,pp_donedate,pp_active) VALUES
						(null, ".$plc_id.", '$pt', '$pi', ".$pp['days'].", '".$pp['start']."', '".$pp['end']."', ".(isset($pp['done']) ? $pp['done'] : "N").", '".($pp['donedate']>0 ? date("Y-m-d",$pp['donedate']) : "")."',1)";
			}
 		}
	return $sql;
}


/********************************
****** GET DATABASE INFO ********
********************************/

$r_sql = "SELECT * FROM ".$dbref."_kpi_results WHERE kr_kpiid = ".$_REQUEST['plc_kpiid']." ORDER BY kr_timeid";
$results = $helperObj->mysql_fetch_all_fld($r_sql,"kr_timeid");

$sql = "SELECT * FROM ".$dbref."_kpi WHERE kpi_id = ".$_REQUEST['plc_kpiid']." AND kpi_active = true";
$object = $helperObj->mysql_fetch_all_fld($sql,"kpi_id");
$object = $object[$_REQUEST['plc_kpiid']];

$plc = array('std'=>array(),'proc'=>array(),'add'=>array());
$sql = "SELECT * FROM ".$dbref."_list_plc_std WHERE yn <> 'N' ORDER BY sort";
$p = $helperObj->mysql_fetch_all_fld($sql,"id");
$plc['std'] = $p;
$sql = "SELECT * FROM ".$dbref."_list_plc_proc WHERE yn <> 'N' ORDER BY sort";
$p = $helperObj->mysql_fetch_all_fld($sql,"id");
$plc['proc'] = $p;
$sql = "SELECT * FROM ".$dbref."_list_plc_custom WHERE yn <> 'N' ORDER BY value";
$p = $helperObj->mysql_fetch_all_fld($sql,"id");
$plc['add'] = $p;



/********************************
   *** CALCULATE NEW VALUES ***
********************************/
$plc_values = array('target'=>array(),'actual'=>array());
for($i=1;$i<13;$i++) {
	$plc_values['target'][$i] = 0;
	$plc_values['actual'][$i] = 0;
}
foreach($plc['std'] as $i => $p) { 
	switch($p['type']) {
	case "P": //PROCUREMENT PHASE
		foreach($plc['proc'] as $pi => $pd) {
			$pp = $_REQUEST['pp']['P_'.$pi];
			if($pp['use']!="N") {
				calcNewValue(&$plc_values,$pp);
			}
		}
		break;
	case "A":	//ADDITIONAL PHASE
		for($pc=0;$pc<10;$pc++) {
			$pp = $_REQUEST['pp']['A_'.$pc];
			if($pp['use']!="N") {
				calcNewValue(&$plc_values,$pp);
			}
		}
		break;
	case "S":	//NORMAL STANDARD PHASE
	default:
		$pp = $_REQUEST['pp']['S_'.$i];
		if($pp['use']!="N") {
			calcNewValue(&$plc_values,$pp);
		}
		break;
	}
}





/********************************
*** CALCULATE NEW KPI RESULTS ***
********************************/
$unauth_sql = array();  $auth_sql = array();
if($_REQUEST['plc_updatekpi']!="N") { 
	$kpi_results_echo = "<h2>KPI Results</h2>
	<table id=kpir>
	<thead>
		<tr>
			<th rowspan=3></th>
			<th colspan=6>Period Performance</th>
			<th colspan=6>Overall Performance</th>
		</tr>
		<tr>
			<th colspan=3>Original Values</th>
			<th colspan=3>New Values</th>
			<th colspan=3>Original Values</th>
			<th colspan=3>New Values</th>
		</tr>
		<tr>
			<th>Target</th>
			<th>Actual</th>
			<th>R</th>
			<th>Target</th>
			<th>Actual</th>
			<th>R</th>
			<th>Target</th>
			<th>Actual</th>
			<th>R</th>
			<th>Target</th>
			<th>Actual</th>
			<th>R</th>
		</tr>
	</thead><tbody>";

	$obj_ct = $object['kpi_calctype'];
	$values = array();
	$new_values = array();
	$new_target = 0;
	$new_actual = 0;
	foreach($time as $ti => $t) {
		$res_obj = $results[$ti];
		if($_REQUEST['plc_updatekpi']!="ALL") {
			$plc_values['target'][$ti] = $res_obj['kr_target'];
		} else {
			$new_target += $plc_values['target'][$ti];
			$plc_values['target'][$ti] = $new_target;
		}
		if($plc_values['actual'][$ti]>0) {
			$new_actual += $plc_values['actual'][$ti];
			$plc_values['actual'][$ti] = $new_actual;
		}
	}
	foreach($time as $ti => $t) {
		$res_obj = $results[$ti];
		$values['target'][$ti] = $res_obj['kr_target'];
		$values['actual'][$ti] = $res_obj['kr_actual'];
		$new_values['target'][$ti] = $plc_values['target'][$ti];
		$new_values['actual'][$ti] = $plc_values['actual'][$ti];
		$r = KPIcalcResult($values,$obj_ct,array(1,$ti),$ti);
		$r_all = KPIcalcResult($values,$obj_ct,array(1,$ti),"ALL");
		$new_r = KPIcalcResult($new_values,$obj_ct,array(1,$ti),$ti);
		$new_r_all = KPIcalcResult($new_values,$obj_ct,array(1,$ti),"ALL");
		$kpi_results_echo.= "<tr>";
			$kpi_results_echo.= "<th>".$t['display_full'].": </th>";
			//if(strtotime($t['start_date'])<$today) {
				$kpi_results_echo.= "<td>".$r['target']."%</td>";
				$kpi_results_echo.= "<td>".$r['actual']."%</td>";
				$kpi_results_echo.= "<td class=".$r['style'].">".$r['text']."</td>";

				$kpi_results_echo.= "<td>".$new_r['target']."%</td>";
				$kpi_results_echo.= "<td>".$new_r['actual']."%</td>";
				$kpi_results_echo.= "<td class=".$new_r['style'].">".$new_r['text']."</td>";
				
				$kpi_results_echo.= "<td>".$r_all['target']."%</td>";
				$kpi_results_echo.= "<td>".$r_all['actual']."%</td>";
				$kpi_results_echo.= "<td class=".$r_all['style'].">".$r_all['text']."</td>";

				$kpi_results_echo.= "<td>".$new_r_all['target']."%</td>";
				$kpi_results_echo.= "<td>".$new_r_all['actual']."%</td>";
				$kpi_results_echo.= "<td class=".$new_r_all['style'].">".$new_r_all['text']."</td>";
			//} else {
			//	for($m=1;$m<=12;$m++) { echo "<td></td>"; }
			//}
			$unauth_sql[] = "UPDATE ".$dbref."_kpi_results SET kr_actual = ".$new_values['actual'][$ti]." WHERE kr_kpiid = ".$_REQUEST['plc_kpiid']." AND kr_timeid = $ti";
		$kpi_results_echo.= "</tr>";
	}
	$kpi_results_echo.="</tbody>
						</table>";
						
	if($_REQUEST['plc_updatekpi']=="ALL" && count(array_diff_assoc($new_values['target'],$values['target']))>0) {
		$authorisation_required = true;
	} else {
		$authorisation_required = false;
	}
} else {
	$authorisation_required = false;
}




/********************************
      *** DISPLAY FORM ***
********************************/




?>
<form action=<?php echo $self; ?> method=post>
<table class="noborder"><tr class="no-highlight"><td class="noborder no-highlight">
<h2>PLC Details</h2>
<table id=details>
	<tr>
		<th>KPI #:</th>
		<td><?php echo $id_labels_all[$section].$_REQUEST['plc_kpiid']; ?></td>
	</tr>
	<tr>
		<th>Project Name:</th>
		<td><?php echo $_REQUEST['plc_name']; ?></td>
	</tr>
	<tr>
		<th>Project Description:</th>
		<td><?php echo $_REQUEST['plc_description']; ?></td>
	</tr>
	<tr>
		<th>Start Date:</th>
		<td><?php echo $_REQUEST['plc_startdate']; ?></td>
	</tr>
	<tr>
		<th>End Date:</th>
		<td><?php echo $_REQUEST['plc_enddate']; ?></td>
	</tr>
	<tr>
		<th>Use PLC to Update KPI Results?:</th>
		<td><?php switch($_REQUEST['plc_updatekpi']) { case "ACTUAL": echo "Actuals Only"; break; case "ALL": echo "Targets & Actuals (Requires authorisation)"; break; default: echo "No"; } ?></td>
	</tr>
</table>
<?php 
if($authorisation_required) {
} else {
	$sql = "INSERT INTO ".$dbref."_kpi_plc (plc_id, plc_name, plc_description, plc_kpiid, plc_startdate, plc_enddate, plc_proccate, plc_updatekpi, plc_active) VALUES
			(null,'".code($_REQUEST['plc_name'])."','".code($_REQUEST['plc_description'])."',".$_REQUEST['plc_kpiid'].",'".$_REQUEST['plc_startdate']."','".$_REQUEST['plc_enddate']."',".$_REQUEST['plc_proccate'].",'".$_REQUEST['plc_updatekpi']."',1)";
}
echo "<P>".$sql;
//$plc_id = db_insert($sql);
$plc_id = 0;


?>
<h2>PLC Phases</h2>
<table id=phases>
<thead>
	<tr>
		<th colspan=2>Phases</th>
		<th>     Use?    </th>
		<th> Calendar Days </th>
		<th>  Start Date  </th>
		<th>   End Date   </th>
		<th> % of Project </th>
		<th>  Completed?  </th>
		<th>Completion Date</th>
		<th>SQL</th>
	</tr>
</thead>
<tbody>
<?php 
$pid_array = array();
foreach($plc['std'] as $i => $p) { 
	if($p['type']=="P") {
		echo "<tr>
			<td class=phase rowspan=".count($plc['proc']).">".$p['value'].":
			<br /><span style=\"font-style: italic;\">Project Category: ".$_REQUEST['plc_proccate']."</span></td>";
		$pc = 0;
		foreach($plc['proc'] as $pi => $pd) {
			if($pc>0) { echo "<tr>"; }	$pc++;
			$pp = $_REQUEST['pp']['P_'.$pi];
			echo "<td class=\"phase proc\">".$pi." :: ".$pd['value'].":</td>";
			$sql = echoPhaseRow($plc_id,$pp,$pi,'P');
			echo "<td>$sql</td></tr>";
		}
	} elseif($p['type']=="A") {
		echo "<tr>
			<td class=phase rowspan=10>".$p['value'].":</td>";
		for($pc=0;$pc<10;$pc++) {
			$pp = $_REQUEST['pp']['A_'.$pc];
			$ppcust = $_REQUEST['custom_phase']['A_'.$pc];
			if($pc>0) { echo "<tr id=\"tr-A_".$pc."\">"; }
			echo "<td class=\"phase custom\">$pc :: ".(isset($ppcust['pp']) ? $ppcust['pp'] : "X")." = :".$ppcust['new'].":</td>";
			if(isset($ppcust['pp'])) { 
				if($ppcust['pp']=="add") {
					$sql = "INSERT INTO ".$dbref."_list_plc_custom (id, value, yn, dirid) VALUES (null, '".code($ppcust['new'])."', 'Y', 0)";
					//$pp_id = db_insert($sql);
					echo "<P>".$sql;
					$pp_id = $pc;
				} else {
					$pp_id = $ppcust['pp'];
				}
			} else {
				$pp_id = "X";
			}
			$sql = echoPhaseRow($plc_id,$pp,$pp_id,'A');
			echo "<td>$sql</td></tr>";
		}
	} else {
		$pp = $_REQUEST['pp']['S_'.$i];
		echo "<tr>
				<td class=phase colspan=2>".$i." :: ".$p['value'].":</td>";
		$sql = echoPhaseRow($plc_id,$pp,$i,'S');
		echo "<td>$sql</td></tr>";
	}
} ?>
</tbody>
</table>
<?php 
if($_REQUEST['plc_updatekpi']!="N") { 
	echo $kpi_results_echo;
	if($authorisation_required) {
		foreach($time as $ti => $t) {
			$auth_sql[] = "INSERT INTO ".$dbref."_kpi_plc_temp_results (r_id, r_tmpid, r_plcid, r_kpiid, r_timeid, r_target, r_actual, r_yn) VALUES (null, ".$tmp_id.", ".$plc_id.", ".$_REQUEST['plc_kpiid'].", $ti, ".$new_values['target'][$ti].", ".$new_values['actual'][$ti].", 'Y')";
		}
		//arrPrint($auth_sql);
	} else {
		//arrPrint($unauth_sql);
	}
} 
?>
<p>&nbsp;</p>
<table width=100%><tr><td class=center><input type=submit value=Submit class=isubmit /> <input type=reset /> <span class=float><input type=button value=Cancel class=idelete /></span></td></tr></table>
</td></tr></table>
</form>
<script type=text/javascript>
$(function() {
	/* ON LOAD */
	$("#details th").addClass("top left");
	$("#kpir tbody th").addClass("top left");
	$("#kpir tbody td").addClass("right");
});
</script>
</body>
</html>