<script type=text/javascript>
$(document).ready(function(){
	$("tr").off("mouseenter mouseleave");
});
</script>
<style type=text/css>
table th { vertical-align: top; }
table td {vertical-align: top; }
table th.th2 { vertical-align: top; border-color: #ababab; }
.green li { color: #009900; }
.red { color: #999999; }
.orange { color: #fe9900; }
</style>
<?php
global $helperObj;
//GET MENU_MODULES_USERS
$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as tkn FROM assist_".$cmpcode."_timekeep
		INNER JOIN assist_".$cmpcode."_menu_modules_users ON usrtkid = tkid AND usrmodref = '$modref'
		WHERE tkstatus = 1 ORDER BY tkname, tksurname";
$users = $helperObj->mysql_fetch_all_fld($sql,"tkid");
//GET USER_ACCESS
$sql = "SELECT * FROM ".$dbref."_user_access WHERE active = true";
$access = $helperObj->mysql_fetch_all_fld($sql,"tkid");
$access_codes = array(
	'module'=>"Module Administrator",
	'kpi'=>"KPI Administrator",
	'finance'=>"Finance Administrator",
	'toplayer'=>"Top Layer Administrator",
	'assurance'=>"Assurance Provider",
	'second'=>"Secondary User",
	'setup'=>"Setup",
);
//GET ADMINS
$admin_codes = array(
	'update'=>"Update",
	'edit'=>"Edit",
	'create'=>"Create",
	'approve'=>"Approve",
	'assuranceemails'=>"Assurance Notifications",
);
//DIR
$sql = "SELECT d.value, a.* FROM ".$dbref."_user_admins a
		INNER JOIN ".$dbref."_dir d ON d.id = a.ref AND d.active = true
		WHERE a.active = true AND a.type = '".$report_types['dir']."'
		ORDER BY d.sort";
$admin['DIR'] = $helperObj->mysql_fetch_all_fld2($sql,"tkid","id");
//SUB
if(isset($report_types['sub'])) {
	$sql = "SELECT d.value, a.* FROM ".$dbref."_user_admins a
		INNER JOIN ".$dbref."_subdir d ON d.id = a.ref AND d.active = true
		WHERE a.active = true AND a.type = '".$report_types['sub']."'
		ORDER BY d.sort";
	$admin['SUB'] = $helperObj->mysql_fetch_all_fld2($sql,"tkid","id");
}
//OWN
$sql = "SELECT d.value, a.* FROM ".$dbref."_user_admins a
		INNER JOIN ".$dbref."_list_owner d ON d.id = a.ref AND d.active = true
		WHERE a.active = true AND a.type = '".$report_types['own']."'
		ORDER BY d.sort";
$admin['OWN'] = $helperObj->mysql_fetch_all_fld2($sql,"tkid","id");




echo "<table>
	<tr>
		<th >Ref</th>
		<th >User</th>
		<th colspan=".(count($admin_codes)+1).">User Access / Admin Privileges</th>
	</tr>";
	foreach($users as $tk => $u) {
		$td = 0;
		$tc = 1;
		$tc += (isset($admin['DIR'][$tk]) ? count($admin['DIR'][$tk]) : 0) + ((isset($admin['DIR'][$tk]) && count($admin['DIR'][$tk])>0) ? 1 : 0);
		$tc += (isset($admin['SUB'][$tk]) ? count($admin['SUB'][$tk]) : 0) + ((isset($admin['SUB'][$tk]) && count($admin['SUB'][$tk])>0) ? 1 : 0);
		$tc += (isset($admin['OWN'][$tk]) ? count($admin['OWN'][$tk]) : 0) + ((isset($admin['OWN'][$tk]) && count($admin['OWN'][$tk])>0) ? 1 : 0);
		$tc -= (($page_type=="TOP" && isset($admin['DIR'][$tk]) && count($admin['DIR'][$tk])>0) ? 1 : 0);
		if( (isset($admin['DIR'][$tk]) && count($admin['DIR'][$tk])>0) || (isset($admin['SUB'][$tk]) && count($admin['SUB'][$tk])>0) || (isset($admin['OWN'][$tk]) && count($admin['OWN'][$tk])>0) ) {	$tc++; }
		echo "<tr>";
			echo "<th rowspan=$tc >".$tk."</th>";
			echo "<td rowspan=$tc style=\"font-weight: bold; padding-top: 5px;\"><span >".$u['tkn']."</span></td>";
			if(isset($access[$tk])) {
				echo "<td class=green colspan=".(count($admin_codes)+1).">";
					foreach($access_codes as $a => $c) {
						if($access[$tk][$a]) {
							if($td==0) { echo "<ul>"; }
							$td++;
							echo "<li>".$c."</li>";
						}
					}
					if($td==0) {
						echo "<span class=orange>No administrator access has been defined.</span>";
					} else {
						echo "</ul>";
					}
				echo "</td>";
			} else {
				$td++;
				echo "<td class=red colspan=".(count($admin_codes)+1).">No User Access has been defined.</td>";
			}
		echo "</tr>";
	if( (isset($admin['DIR'][$tk]) && count($admin['DIR'][$tk])>0) || (isset($admin['SUB'][$tk]) && count($admin['SUB'][$tk])>0) || (isset($admin['OWN'][$tk]) && count($admin['OWN'][$tk])>0) ) {
		echo "<tr>
			<th class=th2>".($page_type=="TOP" ? "Directorate" : "")."&nbsp;</th>";
			foreach($admin_codes as $ac) {
				echo "<th class=th2 width=50>".$ac."</th>";
			}
		echo "</tr>";
	}
		$type = "DIR";
		if(isset($admin[$type][$tk]) && count($admin[$type][$tk])>0) {
			if($page_type!="TOP") {
				echo "<tr>";
                $mhead_dir = array_key_exists(0,$mheadings['dir']) ? $mheadings['dir'][0] : array();
                $mhead_dir_h_client = isset($mhead_dir['h_client']) ? $mhead_dir['h_client'] : "";
                $mhead_dir_h_ignite = isset($mhead_dir['h_ignite']) ? $mhead_dir['h_ignite'] : "";
					echo "<td colspan=".(count($admin_codes)+1)." style=\"font-weight: bold;\">".(strlen($mhead_dir_h_client)>0 ? $mhead_dir_h_client : $mhead_dir_h_ignite)."</td>";
				echo "</tr>";
			}
			foreach($admin[$type][$tk] as $ad) {
				echo "<tr>";
					echo "<td>".$ad['value']."</td>";
					foreach($admin_codes as $a => $c) {
						echo "<td class=centre>";
							echo ($ad['act_'.$a]) ? "Yes" : "No";
						echo "</td>";
					}
				echo "</tr>";
			}
		}
		$type = "SUB";
		if(isset($admin[$type][$tk]) && count($admin[$type][$tk])>0) {
			echo "<tr>";
				echo "<td colspan=".(count($admin_codes)+1)." style=\"font-weight: bold;\">".(strlen($mheadings['KPI']['kpi_subid']['h_client'])>0 ? $mheadings['KPI']['kpi_subid']['h_client'] : $mheadings['KPI']['kpi_subid']['h_ignite'])."</td>";
			echo "</tr>";
			foreach($admin[$type][$tk] as $ad) {
				echo "<tr>";
					echo "<td>".$ad['value']."</td>";
					foreach($admin_codes as $a => $c) {
						echo "<td class=centre>";
							echo ($ad['act_'.$a]) ? "Yes" : "No";
						echo "</td>";
					}
				echo "</tr>";
			}
		}
		$type = "OWN";
		if(isset($admin[$type][$tk]) && count($admin[$type][$tk])>0) {
			echo "<tr>";
				echo "<td colspan=".(count($admin_codes)+1)." style=\"font-weight: bold;\">".(strlen($mheadings['KPI']['kpi_ownerid']['h_client'])>0 ? $mheadings['KPI']['kpi_ownerid']['h_client'] : $mheadings['KPI']['kpi_ownerid']['h_ignite'])."</td>";
			echo "</tr>";
			foreach($admin[$type][$tk] as $ad) {
				echo "<tr>";
					echo "<td>".$ad['value']."</td>";
					foreach($admin_codes as $a => $c) {
						echo "<td class=centre>";
							echo ($ad['act_'.$a]) ? "Yes" : "No";
						echo "</td>";
					}
				echo "</tr>";
			}
		}
	}
echo "</table>";
?>