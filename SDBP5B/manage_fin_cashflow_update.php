<?php $time_all = getTime($get_time,false);
global $helperObj;
switch($_REQUEST['act']) {
case "REVIEW":
case "SAVE":
	$total_changes = 0;
	$log_headings = array();
	$updated_fields = array("_5");
	$layout = array(
		'rev'=>array(	'_5'=>7,),
		'opex'=>array(	'_5'=>9,),
		'capex'=>array(	'_5'=>11,),
	);
	$perc = array("_7","_11","_15");
	//GET REQUEST DATA
	if(!isset($_REQUEST['t'])) { die($helperObj->displayResult(array("error","An error occurred while trying to determine the time period."))); }
	$time_id = $_REQUEST['t'];
	if($_REQUEST['act']=="REVIEW") {
		echo "<p>Please note: 
			<br />1. This function only updates the ACTUALS and does not affect the budget figures.
			<br />2. Any changes will be highlighted in green.  Any errors will be highlighted in red.</p>";
		echo $me->JSdisplayResultPrep("");
		echo "<script type=text/javascript>
			JSdisplayResult('info','info','Processing...');
		</script>";
	} else {
		echo $me->JSdisplayResultPrep("");
		echo "<script type=text/javascript>
			JSdisplayResult('info','info','Processing...');
		</script>";
	}
	echo "<form id=up method=post action=".$self.">
			<input type=hidden name=t value=".$time_id." />
			<input type=hidden name=page_id value=".$page_id." />
			<input type=hidden name=tab value=".$tab." />
			<input type=hidden name=act value=SAVE />";
	//CHECK FOR IMPORT FILE
		$err = false;
		$file_location = $modref."/import/".$section."/";
    $helperObj->checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$file_location;
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				die($helperObj->displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						die($helperObj->displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB.")));
						break;
					case 4:	
						die($helperObj->displayResult(array("error","No file found.  Please select a file to import.")));
						break;
					default: 
						die($helperObj->displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					die($helperObj->displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
				}
			}
		}
		//READ FILE
		if(!$err) {
			echo "<input type=hidden name=f value=\"".(isset($filen) ? $filen : $_REQUEST['f'])."\" />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);
		//GET DATA
			if(count($data)>3) {
				unset($data[0]); unset($data[1]); unset($data[2]);
				$new = array();
				$object_sql[0] = "SELECT subdir.value as list_cf_subid, dir.value as list_0, gfs.value as list_cf_gfsid, c.* ";
				$object_sql[1] = "FROM ".$dbref."_".$table_tbl." c
				INNER JOIN ".$dbref."_subdir subdir ON cf_subid = subdir.id AND subdir.active = true
				INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
				INNER JOIN ".$dbref."_list_gfs gfs ON cf_gfsid = gfs.id 
				WHERE cf_active = true ";
				$objects = $helperObj->mysql_fetch_all_fld(implode($object_sql),"cf_id");

				$results_sql = "SELECT r.* FROM ".$dbref."_".$table_tbl."_results r
				INNER JOIN ".$dbref."_".$table_tbl." c ON r.cr_cfid = c.cf_id AND c.cf_active = true";
				
				$results = $helperObj->mysql_fetch_all_fld2($results_sql,"cr_cfid","cr_timeid");
				//arrPrint($mheadings);
				echo "<table class=noborder><tr class=no-highlight><td class=noborder><table>
						<thead><tr>
							<th rowspan=2>Ref</th>
							<th rowspan=2>".$mheadings['dir'][0]['h_client']."</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'subid']['h_client']."</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'value']['h_client']."</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'vote']['h_client']."</th>";
					foreach($mheadings[$h_section] as $hfld => $h2) {
						echo "<th style=\"border-left: 2px solid #ffffff;\" colspan=".count($mheadings[$r_section]).">".$h2['h_client']."</th>";
					}
					echo "<th rowspan=2></th>";
					echo "</tr><tr>";
					foreach($mheadings[$h_section] as $hfld => $h2) {
						foreach($mheadings[$r_section] as $fld => $h) {
							if($fld == "target") { $style = "border-left: 2px solid #ffffff;"; } else { $style = ""; }
							if(!isset($log_headings['cr_'.$hfld.$fld])) { $log_headings['cr_'.$hfld.$fld] = $h2['h_client'].": ".$h['h_client']; }
							echo "<th style=\"$style\">".$h['h_client']."</th>";
							//echo "<th>".$log_headings['cr_'.$hfld.$fld]."</th>";
						}
					}
					echo "</tr></thead>
						<tbody>";
						$overall_change = true;
				foreach($data as $d => $di) {
					$err = false;
					if(strlen($di[0])>0) {
						$obj_id = substr($di[0],strlen($id_labels_all[$section]),strlen($di[0]));
						if(!isset($objects[$obj_id])) { $err = true; } else {	$obj = $objects[$obj_id];  $res = $results[$obj_id]; $new[$obj_id] = array(); }
						echo "<tr class=no-highlight>
							<th>".$id_labels_all[$section].$obj_id."</th>";
							if(!$err) {
								echo "<td>".$obj['list_0']."</td>";
								echo "<td>".$obj['list_'.$table_fld.'subid']."</td>";
								echo "<td>".$obj[$table_fld.'value']."</td>";
								echo "<td>".$obj[$table_fld.'vote']."</td>";
								$new_actuals = array();
								foreach($mheadings[$h_section] as $hfld => $hh) {
									$values = array();
									$change = false;
									foreach($mheadings[$r_section] as $fld => $h) {
										if($fld == "_1") { $style = "border-left: 2px solid #000099;"; } else { $style = ""; }
										$class = "";
										$val = 0;
										$db_fld = "cr_".$hfld.$fld;
										$db_ob	= "cr_".$hfld."_1";
										$db_ae	= "cr_".$hfld."_2";
										$db_vire= "cr_".$hfld."_3";
										$db_ab	= "cr_".$hfld."_4";
										$db_act	= "cr_".$hfld."_5";
										if(isset($layout[$hfld][$fld])) {
											$val = $di[$layout[$hfld][$fld]];
											if(!is_numeric($val)) {
												$change = false; $err = true; $class = "result1"; $overall_change = false;
											} else {
																				//$new_actuals[$hfld] = $val;
												if($val!=$res[$time_id][$db_fld]) {
													$change = true;
																		//$new[$obj_id][$db_fld] = $val;
													$class = "result3";
												}
											}
										} elseif($change) {
											if(!in_array($fld,array("_4","_8","_12"))) { $class = "time3"; } else { $class = ""; $val = $res[$time_id][$db_fld]; $new[$obj_id][$db_fld] = $val; }
											//$fld_i = substr($fld,1,2);
											switch($fld) {
											/*case "_4":	//monthly adjusted budget
												//$val = $res[$time_id]["cr_".$hfld.$ob] + $di[$layout[$hfld][$ae]] + $di[$layout[$hfld][$vire]];
												//$val = $res[$time_id][$db_ob] + $new[$obj_id][$db_ae] + $new[$obj_id][$db_vire];
												$val = $res[$time_id][$db_ab];
												break;*/
											case "_6":	//monthly variance
												$val = $new[$obj_id][$db_ab] - $new[$obj_id][$db_act];
												break;
											case "_7":	//monthly % spent
												if($new[$obj_id][$db_ab]==0 && $new[$obj_id][$db_act]==0) {
													$val = 0;
												} else {
													$val = ($new[$obj_id][$db_ab]>0) ? $new[$obj_id][$db_act] / $new[$obj_id][$db_ab] * 100 : 100;
												}
												break;
											case "_8":	//ytd adj budget
												/*for($i=1;$i<$time_id;$i++) {
													$val+=$res[$i][$db_ab];
												}
												$val+=$new[$obj_id][$db_ab];*/
												$ytd_ab = $res[$time_id][$db_fld];
												break;
											case "_9":
												/*if($time_id>1) {
													$val = $res[$time_id-1][$db_act];
												}*/
												for($i=1;$i<$time_id;$i++) {
													$val+=$res[$i][$db_act];
												}
												$val+=$new[$obj_id][$db_act];
												$ytd_act = $val;
												break;
											case "_10":	//ytd var
												$val = $ytd_ab - $ytd_act;
												break;
											case "_11":	//ytd %
												if($ytd_ab==0 && $ytd_act==0) {
													$val = 0;
												} else {
													$val = ($ytd_ab>0) ? $ytd_act/$ytd_ab*100 : 100;
												}
												break;
											case "_12":	//total adj budget
												/*for($i=1;$i<=12;$i++) {
													if($i==$time_id) {
														$val+=$new[$obj_id][$db_ab];
													} else {
														$val+=$res[$i][$db_ab];
													}
												}*/
												$tot_ab = $res[$time_id][$db_fld];
												break;
											case "_13":
												$val = $ytd_act;
												$tot_act = $val;
												break;
											case "_14":	//total var
												$val = $tot_ab - $tot_act;
												break;
											case "_15":	//total %
												if($tot_ab==0 && $tot_act==0) {
													$val = 0;
												} else {
													$val = ($tot_ab>0) ? $tot_act / $tot_ab * 100 : 100;
												}
												break;
											case "_1": default: $val = $res[$time_id][$db_fld]; break;
											}
										} else {
											$val = $res[$time_id][$db_fld];
										}
										if($class=="result1") {
											echo "<td class=\"$class right\" style=\"$style\">Invalid number</td>";
										} else {
											echo "<td class=\"$class\" style=\"text-align: right; $style\">".number_format($val,2).(in_array($fld,$perc) ? "%" : "")."</td>";
										}
										$new[$obj_id][$db_fld] = $val;
									}
								}
								$new[$obj_id]['change'] = !$err;
								$sql = "";
								if($new[$obj_id]['change']) {
									if($_REQUEST['act']=="SAVE") {
										$sql_fld = array();
										$log_changes = array();
										foreach($new[$obj_id] as $fld=>$v) {
											if($fld!="change" && !(strpos($fld,"budg")>0)) {
												$sql_fld[] = $fld." = ".number_format($v,4,".","");
												$log_changes[] = $log_headings[$fld]." = ".number_format($v,2).(strpos($fld,"perc")>0?"%":"");
											}
										}
										$sql = "UPDATE ".$dbref."_".$table_tbl."_results SET ".implode(", ",$sql_fld)." WHERE cr_cfid = ".$obj_id." AND cr_timeid = ".$time_id;
										$mar = 0;
										$mar = $helperObj->db_update($sql);
										$total_changes+=$mar;
										//echo "<td>$sql <br /> $mar </td>";
										if($mar>0) {
											$v = array(
												'fld' => "",
												'timeid' => $time_id,
												'text' => addslashes("Updated financials for ".$time[$time_id]['display_full']." as follows:<br /> - ".implode("<br /> - ",$log_changes)),
												'old' => "",
												'new' => addslashes($file),
												'act' => "U",
												'YN' => "Y"
											);
											logChanges($section,$obj_id,$v,addslashes($sql));
											//UPDATE YTD AND TOTAL CHANGES
												$future = array();
												$my_sec = array("rev","opex","capex");
												$val_act = array('rev'=>0,'opex'=>0,'capex'=>0);
												$val_budg = array('rev'=>0,'opex'=>0,'capex'=>0); 
												$val_tot_budg = array('rev'=>0,'opex'=>0,'capex'=>0);
												for($i=1;$i<=12;$i++) {
													foreach($my_sec as $f) {
														$val_act[$f]+= ( ($i==$time_id) ? $new[$obj_id]['cr_'.$f.'_5'] : $res[$i]['cr_'.$f.'_5'] );
														$val_budg[$f]+= ( ($i==$time_id) ? $new[$obj_id]['cr_'.$f.'_4'] : $res[$i]['cr_'.$f.'_4'] );
													
														if($i>$time_id) {
															$future[$i][$f]['actual'] = $val_act[$f];
															//$future[$i][$f]['ytd_target'] = $val_budg[$f];
															$future[$i][$f]['ytd_var'] = calcChange("var",$val_budg[$f],$val_act[$f]);
															$future[$i][$f]['ytd_perc'] = calcChange("perc",$val_budg[$f],$val_act[$f]);
														}
													}
												}
												$val_tot_budg = array(	
													'rev'=>$val_budg['rev'],
													'opex'=>$val_budg['opex'],
													'capex'=>$val_budg['capex'],
												);
												//echo "<td><pre>"; print_r($future);  echo "</td>";
												foreach($future as $i => $f) {
													$f['rev']['tot_var'] = calcChange("var",$val_tot_budg['rev'],$f['rev']['actual']);
													$f['rev']['tot_perc'] = calcChange("perc",$val_tot_budg['rev'],$f['rev']['actual']);
													$f['opex']['tot_var'] = calcChange("var",$val_tot_budg['opex'],$f['opex']['actual']);
													$f['opex']['tot_perc'] = calcChange("perc",$val_tot_budg['opex'],$f['opex']['actual']);
													$f['capex']['tot_var'] = calcChange("var",$val_tot_budg['capex'],$f['capex']['actual']);
													$f['capex']['tot_perc'] = calcChange("perc",$val_tot_budg['capex'],$f['capex']['actual']);
													//echo "<td><pre>"; print_r($f['rev']); echo "</td>";
													$sql2 = "UPDATE ".$dbref."_cashflow_results SET 
															cr_rev_9 = ".number_format($f['rev']['actual'],4,".","").",
															cr_rev_10 = ".number_format($f['rev']['ytd_var'],4,".","").",
															cr_rev_11 = ".number_format($f['rev']['ytd_perc'],4,".","").",
															cr_rev_13 = ".number_format($f['rev']['actual'],4,".","").",
															cr_rev_14 = ".number_format($f['rev']['tot_var'],4,".","").",
															cr_rev_15 = ".number_format($f['rev']['tot_perc'],4,".","").",
															cr_opex_9 = ".number_format($f['opex']['actual'],4,".","").",
															cr_opex_10 = ".number_format($f['opex']['ytd_var'],4,".","").",
															cr_opex_11 = ".number_format($f['opex']['ytd_perc'],4,".","").",
															cr_opex_13 = ".number_format($f['opex']['actual'],4,".","").",
															cr_opex_14 = ".number_format($f['opex']['tot_var'],4,".","").",
															cr_opex_15 = ".number_format($f['opex']['tot_perc'],4,".","").",
															cr_capex_9 = ".number_format($f['capex']['actual'],4,".","").",
															cr_capex_10 = ".number_format($f['capex']['ytd_var'],4,".","").",
															cr_capex_11 = ".number_format($f['capex']['ytd_perc'],4,".","").",
															cr_capex_13 = ".number_format($f['capex']['actual'],4,".","").",
															cr_capex_14 = ".number_format($f['capex']['tot_var'],4,".","").",
															cr_capex_15 = ".number_format($f['capex']['tot_perc'],4,".","")."
															WHERE cr_cfid = ".$obj_id." AND cr_timeid = ".$i;
													$mar2 = db_update($sql2);
													//echo "<td>".$mar2." :: ".$sql2."</td>";
													if($mar2>0) {
														$v = array(
															'fld' => "",
															'timeid' => $i,
															'text' => addslashes("Updated YTD and Total financials for ".$time_all[$i]['display_full']." due to change in ".$time[$time_id]['display_full']."."),//:<br /> - ".implode("<br /> - ",$log_changes)),
															'old' => serialize($res[$i]),
															'new' => serialize($f),
															'act' => "U",
															'YN' => "Y"
														);
														logChanges($section,$obj_id,$v,addslashes($sql2));
													}
												}
											echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> Success</td>";
										} else {
											echo "<td class=\"ui-state-info\"><span class=\"ui-icon ui-icon-info\" style=\"margin-right: .3em;\"></span> No update done</td>";
										}
									} else {
										echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									}
								} elseif($err) {
									echo "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
								} else {
									echo "<td></td>";
								}
								if($_REQUEST['act']=="SAVE") {
								}
								//echo "<td>$sql</td>";
							} else {
								echo "<td colspan=21>";
                                $helperObj->displayResult(array("error","Reference not found"));
								echo "</td>";
							}
						echo "</tr>";
					}	//strlen ref col
				}	//foreach data
				echo "</tbody>
				</table>";
				if($_REQUEST['act']=="REVIEW") {
					echo "<p>&nbsp;</p>";
					echo "<table width=100%><tr><td class=center><input type=submit value=\"Accept changes\" class=isubmit id=my_submit />
					".($overall_change===false ? "<br />There is an <span class=idelete>invalid number</span> in the import file.  Please review the file before trying again." : "")."
					</td></tr></table></td></tr></table>";
					echo "<script type=text/javascript>
						$(document).ready(function() {
							$('#display_result').hide();
							".($overall_change===false ? "$('#my_submit').attr('disabled','disabled');" : "")."
						});
					</script>";
				} else {
					if($total_changes>0) {
						$v = array(
							'fld' => "",
							'timeid' => $time_id,
							'text' => addslashes("Updated financials for ".$time[$time_id]['display_full']),
							'old' => "",
							'new' => addslashes($file),
							'act' => "U",
							'YN' => "Y"
						);
						logChanges($section,0,$v,"");
					?><script type=text/javascript> 
							$(document).ready(function() {
								//JSdisplayResult(result,icon,text)	result=ok||error||info  icon=ok||error||info||...
								JSdisplayResult("reset","reset","");
								JSdisplayResult("ok","ok","Update complete");
							});
						</script><?php
					} else {
						?><script type=text/javascript> 
							$(document).ready(function() {
								//JSdisplayResult(result,icon,text)	result=ok||error||info  icon=ok||error||info||...
								JSdisplayResult("info","info","Update complete.  No changes were found.");
							});
						</script><?php
					}
				}
                $helperObj->displayGoBack($self."?page_id=".$page_id."&tab=".$tab,"Go Back");
			} else {	//if count data>0
				die($helperObj->displayResult(array("error","No data found to import.")));
			}
		} else {
			die($helperObj->displayResult(array("error","An error occurred while trying to read the file.")));
		}
		//arrPrint($_SESSION);
	break;
default:
?>
<form id=up action=<?php echo $self; ?> method=post enctype="multipart/form-data">
<input type=hidden name=page_id value=<?php echo $page_id; ?> />
<input type=hidden name=tab value=<?php echo $tab; ?> />
<input type=hidden name=act value=REVIEW />
	<h4>Update Process</h4>
		<ol>
			<li>Select time period to update: <select id=ti name=t><option selected value=X>--- SELECT MONTH ---</option><?php 
				foreach($time as $ti => $t) {
					if($t['active'] && $t['active_finance']) {
						echo "<option value=$ti>".$t['display_full']."</option>";
					}
				}
			?></select>
			<li>Export project details: <input type=button value=Export id=export /><br /><span style="font-style: italic">Please ensure that all new line items have been added using the "Create" tab before clicking the "Export" button. </span></li>
			<li>Update the ACTUAL financial details in the CSV document saved in step 1. </li>
			<li>Import updated information:  <input type=file name=ifile id=ifl /> <input type=button value=Import id=import /><br />
				Note: Once you have clicked the "Import" button, you will be given an opportunity to review the Import data before finalising the import.<br />
				Until you click the "Accept" button, on the next page, no information will be updated. </li>
		</ol>
	<h4>Import file information: </h4>
		<ul>
			<li>This process is only for updating the ACTUAL results.  To change the budgets, please use the Edit function.</li>
			<li>The first two lines will be treated as header rows and will not be imported. </li>
			<li>The data needs to be in CSV format, seperated by commas, in the same layout as the export file generated in step 2. </li>
			<li>The system will upload the financials to the line items identified by the Reference in the first column. Any lines that do not contain a valid Reference number will be ignored during the upload. If you wish to add a line item please use the "Create" tab. </li>
		</ul>
</form>
<?php
	$log_sql = "SELECT flog_date, CONCAT_WS(' ',tkname,tksurname) as flog_tkname, flog_transaction FROM ".$dbref."_cashflow_log INNER JOIN assist_".$cmpcode."_timekeep ON flog_tkid = tkid WHERE flog_kpiid = '0' AND flog_yn = 'Y' ORDER BY flog_date DESC";
	$log_flds = array('date'=>"flog_date",'user'=>"flog_tkname",'action'=>"flog_transaction");
	$log = $helperObj->mysql_fetch_all($log_sql);
	$helperObj->displayAuditLog($log,$log_flds);
?>
<script type=text/javascript>
	$(document).ready(function() {
		$("#export").click(function() {
			var t = $("#ti").val();
			$("#ti").removeClass("required");
			if(t!="X" && t.length>0 && !isNaN(parseInt(t))) {
				document.location.href = 'manage_fin_export.php?page_id=<?php echo $page_id; ?>&t='+t;
			} else {
				$("#ti").addClass("required");
				alert("Please select the relevant time period in step 1.");
			}
		});
		$("#import").click(function() {
			var f = $("#ifl").val();
			var t = $("#ti").val();
			$("#ti, #ifl").removeClass("required");
			if(t!="X" && t.length>0 && !isNaN(parseInt(t)) && f.length>0) {
				$("#up").submit();
			} else {
				err = "";
				if(t=="X" || t.length==0 || isNaN(parseInt(t))) {
					$("#ti").addClass("required");
					err = err+"\n - Select the relevant time period";
				}
				if(f.length==0) {
					$("#ifl").addClass("required");
					err = err+"\n - Select the import file";
				}
				if(err.length>0) {
					alert("Please attend to the following errors: "+err);
				}
			}
		});
	});
</script>
<?php
	break;	//default
} 
?>