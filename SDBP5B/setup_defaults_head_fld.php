<?php
global $helperObj;
$log_section = "HEAD_".$section;
$mhead_sec = isset($mheadings[$section]) ? $mheadings[$section] : array() ;
$my_head = $mhead_sec;
foreach($unset as $u) {
	unset($my_head[$u]);
}
$sort_req = isset($_REQUEST['sort']) ? $_REQUEST['sort']  : array();
if(isset($_REQUEST['act']) && $_REQUEST['act'] == "SAVE") {
	$sql = "SELECT h.*, s.h_ignite, s.h_client, s.h_type FROM ".$dbref."_setup_headings_setup h INNER JOIN ".$dbref."_setup_headings s ON s.h_id = h.head_id WHERE h.section = '$section'";
	$olds = $helperObj->mysql_fetch_all_fld($sql,'field');
	foreach($sort_req as $sort => $fld) {
		$c_required = "false";
		$c_list = "false";
		$glossary = "";
		$c_maxlen = 0;
		$f = $fld."_required";	if(isset($_REQUEST[$f]) && $_REQUEST[$f] == "on") { $c_required = "true"; }
		$f = $fld."_list"; 		if(isset($_REQUEST[$f]) && $_REQUEST[$f] == "on") { $c_list = "true"; }
		$lf = "len_".$fld;		if(isset($_REQUEST[$lf]) && $_REQUEST[$lf]=="LIMIT") {
			$f = $fld."_maxlen";	$c_maxlen = isset($_REQUEST[$f]) ? (int)$_REQUEST[$f] : 0;
		}
		$f = $fld."_glossary";	$glossary = ASSIST_HELPER::code($_REQUEST[$f]);
		$sql = "UPDATE ".$dbref."_setup_headings_setup SET c_required = $c_required , c_list = $c_list , c_maxlen = $c_maxlen , glossary = '$glossary' WHERE section = '$section' AND field = '$fld'";
		$mar = $helperObj->db_update($sql);
		if($mar>0) {
			$change = array();
			//c_Required
			if( (!$olds[$fld]['c_required'] && $c_required != "false") || ($olds[$fld]['c_required'] && $c_required!="true") ) { 
				switch($c_required) {
					case "true": $change[] = " - Marked as a required field."; break;
					case "false": $change[] = " - Unmarked as a required field."; break;
				}
			}
			//c_list
			if( (!$olds[$fld]['c_list'] && $c_list != "false") || ($olds[$fld]['c_list'] && $c_list!="true") ) { 
				switch($c_list) {
					case "true": $change[] = " - Added to List view."; break;
					case "false": $change[] = " - Removed from List view."; break;
				}
			}
			//c_maxlen
			//echo "<P>".$olds[$fld]['h_client'].":: OLD:".$olds[$fld]['c_maxlen']." NEW:".$c_maxlen;
			$o_maxlen = (int)$olds[$fld]['c_maxlen'];
				//echo "::".($o_maxlen!=$c_maxlen)."::".$olds[$fld]['h_type'];
			if($o_maxlen!=$c_maxlen && in_array($olds[$fld]['h_type'],array("TEXT","VC"))) {
				if($o_maxlen<=0 && $c_maxlen > 0) {
					$change[] = " - Decreased maximum text length allowance to $c_maxlen characters from unlimited.";
				} elseif($c_maxlen<=0 && $o_maxlen > 0) {
					$change[] = " - Increased maximum text length allowance to unlimited from $o_maxlen characters.";
				} elseif($o_maxlen<$c_maxlen) {
					$change[] = " - Increased maximum text length allowance to $c_maxlen from $o_maxlen characters.";
				} elseif($o_maxlen>$c_maxlen) {
					$change[] = " - Decreased maximum text length allowance to $c_maxlen from $o_maxlen characters.";
				}
			}
			//glossary
			if($olds[$fld]['glossary']!=$glossary) {
				$change[] = " - Updated Glossary explanation.";
			}
			$text = "Updated ".$section_name." heading <i>".(strlen($olds[$fld]['h_client'])>0 ? $olds[$fld]['h_client'] : $olds[$fld]['h_ignite'])."</i>:<br />".implode("<br />",$change); //echo $text;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$olds[$fld]['id'],
											'field'=>"",
											'text'=>$text,
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
		}
	}
	$mheadings = $helperObj->getModuleHeadings(array($section));
	$my_head = isset($mheadings[$section]) ? $mheadings[$section] : array();
    $helperObj->displayResult($result);
} elseif(isset($_REQUEST['act']) && $_REQUEST['act'] == "SAVE_R") {
	$sql = "SELECT h.*, s.h_ignite, s.h_client, s.h_type FROM ".$dbref."_setup_headings_setup h INNER JOIN ".$dbref."_setup_headings s ON s.h_id = h.head_id WHERE h.section = '".$r_section."'";
	$olds = $helperObj->mysql_fetch_all_fld($sql,'field');
	foreach($sort_req as $sort => $fld) {
		$glossary = "";
		$f = $fld."_glossary";	$glossary = addslashes($helperObj->code($_REQUEST[$f]));
		$sql = "UPDATE ".$dbref."_setup_headings_setup SET glossary = '$glossary' WHERE section = '".$r_section."' AND field = '$fld'";
		$mar = $me->db_update($sql);
		if($mar>0) {
			$change = array();
			//glossary
			if($olds[$fld]['glossary']!=$glossary) {
				$change[] = " - Updated Glossary explanation.";
			}
			$text = "Updated ".$section_name." Result heading <i>".(strlen($olds[$fld]['h_client'])>0 ? $olds[$fld]['h_client'] : $olds[$fld]['h_ignite'])."</i>:<br />".implode("<br />",$change); //echo $text;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$olds[$fld]['id'],
											'field'=>"",
											'text'=>$text,
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,$helperObj->code($sql));
		}
	}
	//$mheadings = $me->getModuleHeadings(array($section));
	//$my_head = $mheadings[$section];
	$helperObj->displayResult($result);
}

//GET EXISTING MAXIMUM TEXT VALUES
if(isset($maxlen_sql)) {
	$existing_max = $helperObj->mysql_fetch_one($maxlen_sql);
} else {
	$existing_max = array();
}
?>
<style type=text/css>
.slider { margin: 5px; }
.ui-slider .ui-widget-header { background-image: none; background-color: #aaaaff; }
.ui-slider .ui-widget-header .ui-state-disabled { 
   background-color: #cccccc;
} 
</style>
<form name=dept action=<?php echo $self; ?> method=post>
<input type=hidden name=page_id value=<?php echo $page_id; ?> />
<input type=hidden name=act value=SAVE />
<input type=hidden name=view value=SET />
<table id=tbl_headings>
	<thead>
	<tr>
		<th>Heading</th>
		<th>Required</th>
		<th>List View</th>
		<th width=200>Text Length</th>
		<th>Text for Glossary</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach($my_head as $key => $h) {
		$h['i_maxlen']-=20;
		echo "<tr style=\"verical-align: top\">";
			echo "<td>
					<input type=hidden name=sort[] value=".$key." />".$h['h_client']."
				</td>";
			echo "<td class=centre>";
				if($h['i_required']) {
					echo "<div align=center><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></div>";
				} else {
					echo "<input type=checkbox name=".$key."_required ".($h['c_required'] ? "checked" : "")." />";
				}
			echo "</td>";
			echo "<td class=centre>";
				if($h['fixed'] || $key == $fixed) {
					echo "Fixed";
				} else {
					echo "<input type=checkbox name=".$key."_list ".($h['c_list'] ? "checked" : "")." />";
				}
			echo "</td>";
			if(in_array($h['h_type'],array("TEXT","VC"))) {
				echo "<td style='padding: 5px 5px 5px 5px;'>";
				$min = isset($existing_max[$key]) ? $existing_max[$key] : 10;
				if($min==0) { $min = 10; }
				$min = ceil($min/10) * 10;
				//echo $min." :: ";
				if($h['i_maxlen']<=0) { 
					echo "<input type=radio name=len_".$key." value=UNLIMIT ".($h['c_maxlen']<=0 ? "checked" : "")." key=".$key." /> &gt; 500 characters<br /><input type=radio name=len_".$key." value=LIMIT key=".$key." ".($h['c_maxlen']>0 ? "checked" : "")." /> ";
				} else {
					echo "<input type=hidden name=len_".$key." value=LIMIT key=".$key." class=hidden_radio />";
				}
				echo "<label id=lbl_len_".$key.">".($h['c_maxlen']>0 ? $h['c_maxlen'] : 500)."</label> characters <br />";
				echo "<div class=slider id=slider_".$key." key=$key imax=".$h['i_maxlen']." emin=".$min."></div>";
			} else {
				echo "<td class=center>N/A";
			}
			echo "<input type=hidden value='".$h['c_maxlen']."' name=".$key."_maxlen id=maxlen_".$key." size=3 /></td><td><textarea cols=60 rows=3 name=".$key."_glossary>".decode($h['glossary'])."</textarea></td>";
		echo "</tr>";
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan=5 class=centre>
				<input type=submit value="  Save  " />
			</td>
		</tr>
	</tfoot>
</table>
</form>
	<script>
	$(function() {
		$("#tbl_headings input:radio").click(function() {
			var $rad = $(this);
			var k = $rad.attr("key");
			var $sldr = $("#slider_"+k);
			switch($rad.val()) {
			case "LIMIT":
				$sldr.show();
				$sldr.slider("enable");
				$sldr.trigger("slidechange");
				break;
			case "UNLIMIT":
				$sldr.slider("disable");
				$sldr.hide();
				break;
			}
		});
		$("#tbl_headings input:radio, #tbl_headings input.hidden_radio").each(function() {
			var check = false;
			if($(this).hasClass("hidden_radio")) { check = true; } else { if($(this).attr("checked")) { check = true; } }
			if(check) { 
				var k = $(this).attr("key");
				var $sldr = $("#slider_"+k);
				var imax = parseInt($sldr.attr("imax"));
				if(imax<=0) { imax = 500; }
				var v = parseInt($("#maxlen_"+k).val());
				if(v<=0) { v = imax; }
				var min = parseInt($sldr.attr("emin"));
				if(min<=0) { min = 10; }
				$sldr.slider({
					range: 'min',
					value: v,
					min: min,
					max: imax,
					step: 10,
					slide: function(event, ui) { $(this).trigger("slidechange"); }
				});
				$(this).trigger("click"); 
			}
		});
		$(".slider").on( "slidechange", function(event, ui) {
			var $sldr = $(this);
				var $i = $("#maxlen_"+$sldr.attr("key"));
				var $l = $("#lbl_len_"+$sldr.attr("key"));
				var v = $sldr.slider("option","value");
				$i.val(v);
				$l.text(v);
		});
	});
	</script>
<?php 
if(in_array($section,array("KPI","TOP"))) {
	$r_section = $section."_R";
	$my_head = $helperObj->getModuleHeadings(array($r_section));
	$my_head = isset($my_head[$r_section]) ? $my_head[$r_section] : array();
?>
	<h2>Results</h2>
<form name=res action=<?php echo $self; ?> method=post>
<input type=hidden name=page_id value=<?php echo $page_id; ?> />
<input type=hidden name=act value=SAVE_R />
<input type=hidden name=view value=SET />
<table id=tbl_headings>
	<thead>
	<tr>
		<th>Heading</th>
		<th>Text for Glossary</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach($my_head as $key => $h) {
		echo "<tr style=\"verical-align: top\">";
			echo "<td>
					<input type=hidden name=sort[] value=".$key." />".$h['h_client']."
				</td>";
			echo "<td><textarea cols=60 rows=3 name=".$key."_glossary>".$helperObj->decode($h['glossary'])."</textarea></td>";
		echo "</tr>";
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan=5 class=centre>
				<input type=submit value="  Save  " />
			</td>
		</tr>
	</tfoot>
</table>
<?php
}
?>
<?php 
goBack("setup.php","Back to Setup"); 
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>