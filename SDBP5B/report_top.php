<?php 
$section = "TOP";
$get_lists = true;
if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") { $get_active_lists = false; } else { $get_active_lists = true; }
include("inc/header.php"); 

//arrPrint($_REQUEST);

switch($page_id) {
	case "generate":
	/* SETUP VARIABLES */
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",
		"dir",$table_fld."subid",
		$table_fld."annual",
		$table_fld."revised",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
		$table_fld."pmsref",
	);
	//$head0['dir'] = $mheadings['dir'][0];
    $head_top = isset($mheadings[$section]) ? $mheadings[$section] : array();
	$head = $head_top;


	$src = isset($_REQUEST['src']) ? $_REQUEST['src'] : "GENERATOR";
	if($src=="GRAPH" && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
		$data = unserialize(base64_decode($_REQUEST['dta']));
		//arrPrint($data);
		if(isset($data['dirsub_filter']) && strpos($data['dirsub_filter'],"_")!==false) {
			$ds_filter = explode("_",$data['dirsub_filter']);
			if($ds_filter[0]=="D") {
				$_REQUEST['filter']['dir'][0] = $ds_filter[1];
			} elseif($ds_filter[0]=="S") {
				$_REQUEST['filter']['kpi_subid'][0] = $ds_filter[1];
			}
		}
		$repcate = $data['repcate'];
		if(count($repcate)==0) {
			$repcate = array("ANY");
		}
		$_REQUEST['filter'][$table_fld."repcate"] = $repcate;
		$_REQUEST['fields'] = array(
            0 => "top_dirid",
            1 => "top_pmsref",
            2 => "top_gfsid",
            3 => "top_natoutcomeid",
            4 => "top_natkpaid",
            5 => "top_mtas",
            6 => "top_idp",
            7 => "top_munkpaid",
            8 => "top_value",
            9 => "top_unit",
            10 => "top_risk",
            11 => "top_wards",
            12 => "top_area",
            13 => "top_ownerid",
            14 => "top_baseline",
            15 => "top_poe",
            16 => "top_pyp",
            17 => "top_annual",
            18 => "top_revised",
            19 => "top_calctype",
            20 => "results",
            21 => "tr_perf",
            22 => "tr_correct",
            23 => "tr_dept",
            24 => "tr_dept_correct",
			25	=> "top_repcate",
		);
		//$_REQUEST['r_from'] => from graph report
		//$_REQUEST['r_to'] => from graph report
		$_REQUEST['r_calc'] = "captured";
		$_REQUEST['summary'] = "Y";
		//$_REQUEST['filter']['group_by_filter'] = array([0]=>#) => from graph where applicable
		//$_REQUEST['filter']['result'] = # => from graph
		//$_REQUEST['groupby'] => from graph
		$_REQUEST['filter']['top_wards'] = array("ANY");
		$_REQUEST['filter']['top_area'] = array("ANY");
		$_REQUEST['sort'] = array(
            0 => "top_dirid",
            1 => "top_pmsref",
            2 => "top_repkpi",
            3 => "top_gfsid",
            4 => "top_natoutcomeid",
            5 => "top_natkpaid",
            6 => "top_mtas",
            7 => "top_idp",
            8 => "top_munkpaid",
            9 => "top_value",
            10 => "top_unit",
            11 => "top_risk",
            12 => "top_ownerid",
            13 => "top_baseline",
            14 => "top_poe",
            15 => "top_pyp",
			16 => "top_id",
		);	
		$_REQUEST['output'] = "display";
		$_REQUEST['rhead'] = "";
	}
//arrPrint($_REQUEST);
	if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
			include("report_kpi_process.php");
		} else {
			include("report_kpi_generate.php");
		}
		break;
	case "quick":
		include("report_top_quick.php");
		break;
	default:
		echo "<p>An error occurred.  Please try again.</p>";
		break;
}


if(!(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE")) { echo "</body></html>"; }
?>