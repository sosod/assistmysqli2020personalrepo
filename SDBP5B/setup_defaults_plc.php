<?php 
$page_title = "Project Life Cycle";
$log_section = "PLC";
include("inc/header.php");
global $helperObj;
$result = array(); 

/* Get PLC defaults from $dbref_kpi_plc_setup table */
$sql = "SELECT * FROM ".$dbref."_kpi_plc_setup WHERE active = true ORDER BY id";
$plc_questions = array();
$plc_questions = $helperObj->mysql_fetch_all_fld($sql,'id');



if(isset($_REQUEST['page_id'])) {
/* Set table */
switch($page_id) {
	case "std":		$tbl = $page_id;	$days = false;	$required = true; 	$log_title = "standard";	break;
	case "proc":	$tbl = $page_id;	$days = true;	$required = true;	$log_title = "procurement";	break;
	case "add":		$tbl = "custom";	$days = false;	$required = false;	$log_title = "additional/custom";	break;
	default:		die("<P>An error has occurred.  Please go back and try again.</p>".displayGoBack("setup.php",""));
}
/* Save changes */
if(isset($_REQUEST['act'])) {
	$act = $_REQUEST['act'];
	if($act=="DELETE") {
		$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
		if($helperObj->checkIntRef($id)) {
			$sql = "UPDATE ".$dbref."_list_plc_".$tbl." SET yn = 'N' WHERE id = ".$id;
			$mar = $helperObj->db_update($sql);
			if($helperObj->checkIntRef($mar)) {
						$v_log = array(
							'section'=>$log_section,
							'ref'=>$id,
							'field'=>"yn",
							'text'=>"Deleted ".$log_title." phase $id.",
							'old'=>'',
							'new'=>'N',
							'YN'=>"Y"
						);
						logChanges('SETUP',0,$v_log,$helperObj->code($sql));
				$result = array("ok","Phase $id has been successfully deleted.");
			} else {
				$result = array("error","The phase could not be found to be deleted.");
			}
		} else {
			$result = array("error","An error occurred while deleting the phase.  Please try again.");
		}
	} else {
		switch($page_id) {
			case "std":
				$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
				$val = $_REQUEST['val'];
				$req = $_REQUEST['req'];
				$sort = isset($_REQUEST['sort']) && $helperObj->checkIntRef($_REQUEST['sort']) ? $_REQUEST['sort'] : 99;
				switch($act) {
				case "NEW":
					$sql = "INSERT INTO ".$dbref."_list_plc_std (id, value, yn, sort, type) VALUES (null, '".code($val)."', '$req',$sort,'S')";
					$id = $helperObj->db_insert($sql);
					if($helperObj->checkIntRef($id)) {
						$result = array("ok","New standard phase '$val' was successfully added (ref: $id)");
						$v_log = array(
							'section'=>$log_section,
							'ref'=>$id,
							'field'=>"value",
							'text'=>"Created new standard phase \'".$helperObj->code($val)."\'.",
							'old'=>'',
							'new'=>$helperObj->code($val),
							'YN'=>"Y"
						);
						logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					} else {
						$result = array("error","An error occurred and the new phase was not created.  Please try again.");
					}
					break;
				case "EDIT":
					if($helperObj->checkIntRef($id)) {
						$sql = "UPDATE ".$dbref."_list_plc_std SET value = '".$helperObj->code($val)."', yn = '$req' WHERE id = $id";
						$mar = $helperObj->db_update($sql);
						if($helperObj->checkIntRef($mar)) {
							$v_log = array(
								'section'=>$log_section,
								'ref'=>$id,
								'field'=>"value",
								'text'=>"Updated standard phase $id to \'".$helperObj->code($val)."\'.",
								'old'=>'',
								'new'=>$helperObj->code($val),
								'YN'=>"Y"
							);
							logChanges('SETUP',0,$v_log,$helperObj->code($sql));
							$result = array("ok","Phase $id has been successfully saved.");
						} else {
							$result = array("info","No change was found to be made.");
						}
					} else {
						$result = array("error","An error occurred and the changes could not be saved.  Please try again.");
					}
					break;
				default:
					break;
				}
				break;
			case "add":
				$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
				$val = $_REQUEST['val'];
				switch($act) {
				case "NEW":
					$sql = "INSERT INTO ".$dbref."_list_plc_custom (id, value, yn, dirid) VALUES (null, '".code($val)."', 'Y', 0)";
					$id = $helperObj->db_insert($sql);
					if($helperObj->checkIntRef($id)) {
						$result = array("ok","New additional/custom phase '$val' was successfully added (ref: $id)");
						$v_log = array(
							'section'=>$log_section,
							'ref'=>$id,
							'field'=>"value",
							'text'=>"Created new additional/custom phase \'".$helperObj->code($val)."\'.",
							'old'=>'',
							'new'=>$helperObj->code($val),
							'YN'=>"Y"
						);
						logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					} else {
						$result = array("error","An error occurred and the new phase was not created.  Please try again.");
					}
					break;
				case "EDIT":
					if($helperObj->checkIntRef($id)) {
						$sql = "UPDATE ".$dbref."_list_plc_custom SET value = '".$helperObj->code($val)."' WHERE id = $id";
						$mar = $helperObj->db_update($sql);
						if($helperObj->checkIntRef($mar)) {
							$result = array("ok","Phase $id has been successfully updated.");
							$v_log = array(
								'section'=>$log_section,
								'ref'=>$id,
								'field'=>"value",
								'text'=>"Updated additional/custom phase $id to \'".$helperObj->code($val)."\'.",
								'old'=>'',
								'new'=>$helperObj->code($val),
								'YN'=>"Y"
							);
							logChanges('SETUP',0,$v_log,$helperObj->code($sql));
						} else {
							$result = array("info","No change was found to be made.");
						}
					} else {
						$result = array("error","An error occurred and the changes could not be saved.  Please try again.");
					}
					break;
				default:
					break;
				}
				break;
			case "proc":
				$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
				$val = $_REQUEST['val'];
				$pdays = $_REQUEST['days'];
				$req = $_REQUEST['req'];
				$sort = isset($_REQUEST['sort']) && $helperObj->checkIntRef($_REQUEST['sort']) ? $_REQUEST['sort'] : 99;
				switch($act) {
				case "NEW":
					$sql = "INSERT INTO ".$dbref."_list_plc_".$tbl." (id, value, yn, sort,days1,days2,days3,days4) VALUES (null, '".$helperObj->code($val)."', '$req', 99";
					for($d=1;$d<=4;$d++) { $sql.= ",".($helperObj->checkIntRef($pdays[$d]) ? $pdays[$d] : 0); }
					$sql.=")";
					$id = $helperObj->db_insert($sql);
					if($helperObj->checkIntRef($id)) {
						$result = array("ok","New ".$log_title." phase '$val' was successfully added (ref: $id)");
						$v_log = array(
							'section'=>$log_section,
							'ref'=>$id,
							'field'=>"value",
							'text'=>"Created new ".$log_title." phase \'".$helperObj->code($val)."\'.",
							'old'=>'',
							'new'=>$helperObj->code($val."|_|".$req."|_|".implode("-|-",$pdays)),
							'YN'=>"Y"
						);
						logChanges('SETUP',0,$v_log,$helperObj->code($sql));
					} else {
						$result = array("error","An error occurred and the new phase was not created.  Please try again.");
					}
					break;
				case "EDIT":
					if(checkIntRef($id)) {
						$sql = "UPDATE ".$dbref."_list_plc_".$tbl." SET value = '".$helperObj->code($val)."', yn = '$req' ";
							for($d=1;$d<=4;$d++) { $sql.= ", days".$d." = ".($helperObj->checkIntRef($pdays[$d]) ? $pdays[$d] : 0); }
						$sql.=" WHERE id = $id";
						$mar = $helperObj->db_update($sql);
						if($helperObj->checkIntRef($mar)) {
							$result = array("ok","Phase $id has been successfully updated.");
							$v_log = array(
								'section'=>$log_section,
								'ref'=>$id,
								'field'=>"value",
								'text'=>"Updated ".$log_title." phase $id.",
								'old'=>'',
								'new'=>$helperObj->code($val."|_|".$req."|_|".implode("-|-",$pdays)),
								'YN'=>"Y"
							);
							logChanges('SETUP',0,$v_log,$helperObj->code($sql));
						} else {
							$result = array("info","No change was found to be made.");
						}
					} else {
						$result = array("error","An error occurred and the changes could not be saved.  Please try again.");
					}
					break;
				case "Q":
					$q = $_REQUEST['proc'];
					$r_txt = array();
					foreach($q as $i => $v) {
						if($v!=$plc_questions[$i]['value']) {
							$sql = "UPDATE ".$dbref."_kpi_plc_setup SET value = '$v' WHERE id = $i"; 
							$mar = $helperObj->db_update($sql);
							if($mar>0) {
								$plc_questions[$i]['value'] = $v;
								$r_txt[] = "Setting ".$plc_questions[$i]['descrip']." has been successfully updated.";
								$v_log = array(
									'section'=>$log_section,
									'ref'=>$i,
									'field'=>"value",
									'text'=>"Updated ".$plc_questions[$i]['descrip']." to $v.",
									'old'=>'',
									'new'=>$v,
									'YN'=>"Y"
								);
								logChanges('SETUP',0,$v_log,$helperObj->code($sql));
							}
						}
					}
					if(count($r_txt)>0) {
						$result = array("ok",implode("  ",$r_txt));
					}
					break;
				default:
					break;
				}
				break;
		}	//end switch page_id
	}	//endif act = delete
}
    $helperObj->displayResult($result);

/* Display table */
include("setup_defaults_plc_list.php");

    $helperObj->displayGoBack("","");

} else {	//pageid


/* Save */
$var = $_REQUEST;
if(isset($var['act']) && $var['act']=="SAVE") {
	$value = $var['value'];
	$id = $var['id'];
		if($helperObj->checkIntRef($id)) {
			$sql = "UPDATE ".$dbref."_kpi_plc_setup SET value = '".$helperObj->code($value)."' WHERE id = '$id'";
			$mar = $helperObj->db_update($sql);
			if($mar>0) {
				$v_log = array(
					'section'=>$log_section,
					'ref'=>$id,
					'field'=>"value",
					'text'=>"Updated \'".$plc_questions[$id]['descrip']."\' to ".$value." from ".$plc_questions[$id]['value'].".",
					'old'=>$plc_questions[$id]['value'],
					'new'=>$helperObj->code($value),
					'YN'=>"Y"
				);
				logChanges('SETUP',0,$v_log,$helperObj->code($sql));
				$plc_questions[$id]['value'] = $helperObj->code($value);
				$result = array("ok","Changes saved.");
			} else {
				$result = array("info","No changes found to save.");
			}
		} else {
			$result = array("error","An error occurred.  Please try again.");
		}
}

    $helperObj->displayResult($result);
/*$sql = "SELECT * FROM ".$dbref."_kpi_plc_setup WHERE active = true ORDER BY id";
$plc_questions = mysql_fetch_alls($sql,'id');*/

/* FUNCTIONS */
function drawQuestion($def) {
	echo "	<tr>
				<td>".$def['descrip']."&nbsp;&nbsp;<span class=float>
					<select id=sel".$def['id'].">
						<option value=Y ".($def['value']=="Y" ? "selected" : "").">Yes</option>
						<option value=N ".($def['value']!="Y" ? "selected" : "").">No</option>
					</select>
					<input type=button value=Save id=".$def['id']." />
				</span></td>
			</tr>";
}





/* Display questions */
?>
<table width=400>
	<tr>
		<th colspan=1>Standard Phases</th>
	</tr>
	<tr>
		<td>Configure Standard Phases&nbsp;&nbsp;<span class=float>
			<input type=button value=Configure id=std />
		</span></td>
	</tr>
	<tr>
		<th colspan=1>Procurement Phases</th>
	</tr>
<?php drawQuestion($plc_questions[2]); ?>
	<tr>
		<td>Configure Procurement Phases&nbsp;&nbsp;<span class=float>
			<input type=button value=Configure id=proc />
		</span></td>
	</tr>
	<tr>
		<th colspan=1>Additional/Custom Phases</th>
	</tr>
<?php drawQuestion($plc_questions[3]); ?>
	<tr>
		<td>Configure Additional Phases&nbsp;&nbsp;<span class=float>
			<input type=button value=Configure id=add />
		</span></td>
	</tr>
</table>
</form>
<script type=text/javascript>
	$(document).ready(function() {
		$("th").addClass("left");
		$("select").each(function() {
			var i = $(this).prop("id");
			i = i.substr(3,1);
			var v = $(this).val();
			if(v=="N") {
				switch(i) {
				case "2": $("#proc").prop("disabled","true"); break;
				case "3": $("#add").prop("disabled","true"); break;
				}
			}
		});
		$("input:button").click(function() {
			var v = $(this).val();
			var i = $(this).prop("id");
			if(v=="Configure") {
				document.location.href = '<?php echo $self; ?>?page_id='+i;
			} else if(v=="Save") {
				var s = $("#sel"+i).val();
				document.location.href = '<?php echo $self; ?>?act=SAVE&id='+i+'&value='+s;
			}
		});
	});
</script>
<?php 
goBack("setup.php","Back to Setup"); 

}	//end pageid


$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));
?>
</body>
</html>