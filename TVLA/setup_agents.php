<?php
include("inc_ignite.php");

include("inc_admin.php");

$result = "<p>&nbsp;</p>";
//GET VARIABLES PASSED BY FORM
$atkid = $_POST['atkid'];
$cc = explode("_|_",$_POST['cc']);
$tt = $_POST['tt'];
$pri = $_POST['pri'];

//echo("<P>".$atkid."-".$cc[0]."-".$tt."-".$pri);
//IF THE VARIABLES WERE PASSED CORRECTLY THEN PERFORM UPDATE
if($atkid != "X" && strlen($atkid)>0 && strlen($cc[0])>4 && $cc[0] != "X" && ($tt == "B" || $tt == "D" || $tt == "I") && ($pri == "Y" || $pri == "N"))
{
    //CHECK IF NEW USER EXISTS IN ACCESS TABLE ALREADY
    $sql = "SELECT * FROM assist_".$cmpcode."_tvla_list_agents WHERE tkid = '".$atkid."' AND yn = 'Y' AND tvltype = '".$tt."' AND cmpcode = '".$cc[0]."'";
    include("inc_db_con.php");
    $trow = mysql_num_rows($rs);
    if($trow > 0)
    {
        //IF THE USER WAS FOUND IN THE ACCESS TABLE - ERROR
        $err = "Y";
        $result = "<p>Error - the requested action could not be completed as the user already exists in Travel Assist as an agent for client ".$cc[0].", travel type ".$tt.".</p>";
        //SET TRANSACTION LOG VALUES
        $tsql = $sql;
        $trans = "Error - User ".$atkid." could not be added as they are already an agent for ".$cc[0].".";
    }
    else
    {
        //ELSE SET VARIABLE TO CONTINUE
        $err = "N";
    }
    mysql_close();
    //IF RECORD WAS NOT FOUND THEN PERFORM UPDATE
    if($err == "N")
    {
        $sql = "INSERT INTO assist_".$cmpcode."_tvla_list_agents ";
        $sql.= "SET tkid = '".$atkid."'";
        $sql.= "  , pri = '".$pri."'";
        $sql.= "  , yn = 'Y'";
        $sql.= "  , cmpcode = '".$cc[0]."'";
        $sql.= "  , tvltype = '".$tt."'";
        include("inc_db_con.php");
        //SET TRANSACTION LOG VALUES FOR UPDATE
        $tsql = $sql;
        $trans = "User ".$atkid." added to TVLA as an agent for ".$cc[0].".";
        $result = "<p>Success!  Agent added to Travel Assist.</p>";
    }
    //PERFORM TRANSACTION LOG UPDATE
    $tref = "TVLA";
    include("inc_transaction_log.php");
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delUser(id) {
    if(confirm("Are you sure you wish to perform this action?"))
    {
        document.location.href="setup_agents_edit.php?id="+id+"&t=d";
    }
}

cc = "";
priD = "N";
priI = "N";
cType = "D";

function changeCmp(me) {
    var c = me.value;
    var carr = c.split("_|_");
    cc = carr[0];
    cType = carr[1];
    priD = carr[2];
    priI = carr[3];
    var target = document.getElementById('t');
    var target2 = document.getElementById('p');
    switch(cType)
    {
        case "B":
            target.disabled = false;
            target2.disabled = false;
            target.value = "D";
            if(priD=="Y" || priI == "Y")
            {
                target2.value = "N";
//                target2.disabled = true;
            }
            else
            {
                target2.value = "Y";
            }
            break;
        case "D":
            target.value = "D";
            target.disabled = true;
            target2.disabled = false;
            if(priD=="Y")
            {
                target2.value = "N";
                target2.disabled = false;
            }
            else
            {
                target2.value = "Y";
            }
            break;
        case "I":
            target.value = "I";
            target.disabled = true;
            target2.disabled = false;
            if(priI=="Y")
            {
                target2.value = "N";
                target2.disabled = true;
            }
            else
            {
                target2.value = "Y";
            }
            break;
        default:
            target.disabled = false;
            target.value = "D";
            target2.disabled = false;
            target2.value = "Y";
            break;
    }
}

function Validate(me) {
    me.tt.disabled = false;
    me.pri.disabled = false;
    if(me.atkid.value != "X" && me.cc.value != "X")
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist (Agents): Setup - Assign Agents</b></h1>
<?php echo($result); ?>
<table border="1" id="table1" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-style: solid;">
	<tr>
		<td class="tdheader" width=30>Ref</td>
		<td class="tdheader">Agent</td>
		<td class="tdheader">Client</td>
		<td class="tdheader">Type</td>
		<td class="tdheader">Agent status*</td>
		<td class="tdheader" width=40>&nbsp;</td>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.id, a.cmpcode, a.tvltype, a.pri ";
    $sql .= "FROM assist_".$cmpcode."_tvla_list_agents a, assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE a.yn = 'Y' ";
    $sql .= "AND t.tkid = a.tkid ";
    $sql .= "AND t.tkstatus = 1 ";
    $sql .= "ORDER BY tkname, tksurname, cmpcode";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
?>
        <?php include("inc_tr.php"); ?>
            <td class="tdgeneral"><?php echo($row['id']); ?></td>
            <td class="tdgeneral"><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td class="tdgeneral"><?php $cmpc = $row['cmpcode'];
            $sql2 = "SELECT cmpname FROM assist_company WHERE cmpcode = '".$cmpc."'";
            include("inc_db_con_assist2.php");
                $row2 = mysql_fetch_array($rs2);
                echo($row2['cmpname']);
            mysql_close($con2);
            ?></td>
            <td class="tdgeneral"><?php if($row['tvltype']=="D") { echo("Domestic requests only"); } else { if($row['tvltype']=="I") { echo("International requests only"); } else { echo("Domestic and international requeusts"); } } ?></td>
            <td class="tdgeneral"><?php if($row['pri']=="Y") { echo("Primary agent"); } else { echo("Backup agent"); } ?></td>
            <td class="tdgeneral" align=center><input type=button value=Delete  onclick="delUser(<?php echo($row['id']); ?>)" style="padding-left: 1px"></td>
        </tr>
<?php
    }
    mysql_close();

//IF A USER HAS ACCESS TO TA BUT IS NOT IN THE ABOVE RESULT THEN DISPLAY THE ADD FORM
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql.= "WHERE t.tkid = m.usrtkid  ";
$sql.= "AND usrmodref = 'TVLA'  ";
$sql.= "ORDER BY tkname, tksurname";
include("inc_db_con.php");
$ta = mysql_num_rows($rs);
if($ta > 0)
{
?>
<form method=POST action=setup_agents.php onsubmit="return Validate(this);" language=jscript>
    <?php include("inc_tr.php"); ?>
		<td class="tdgeneral">&nbsp;</td>
		<td class="tdgeneral">
            <select name=atkid>
                <option selected value=X>--- SELECT ---</option>
                <?php
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
                    }
                ?>
            </select>
        </td>
        <td class=tdgeneral><select name=cc id=c onchange="changeCmp(this);"><option selected value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT c.cmpcode, c.cmpname, t.type FROM assist_company c, assist_tvl_agents t";
        $sql.= " WHERE t.ccmpcode = c.cmpcode";
        $sql.= " AND t.acmpcode = '".strtoupper($cmpcode)."'";
        $sql.= " AND t.yn = 'Y'";
        $sql.= " AND c.cmpstatus = 'Y'";
        $sql.= " ORDER BY cmpname";
        include("inc_db_con_assist.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option value=".$row['cmpcode']."_|_".$row['type']."_|_");
                $sql2 = "SELECT * FROM assist_".$cmpcode."_tvla_list_agents WHERE cmpcode = '".$row['cmpcode']."' AND yn = 'Y' AND pri = 'Y' AND (tvltype = 'D' OR tvltype = 'B')";
                include("inc_db_con2.php");
                    if(mysql_num_rows($rs2)>0)
                    {
                        echo("Y");
                    }
                    else
                    {
                        echo("N");
                    }
                mysql_close($con2);
                echo("_|_");
                $sql2 = "SELECT * FROM assist_".$cmpcode."_tvla_list_agents WHERE cmpcode = '".$row['cmpcode']."' AND yn = 'Y' AND pri = 'Y' AND (tvltype = 'I' OR tvltype = 'B')";
                include("inc_db_con2.php");
                    if(mysql_num_rows($rs2)>0)
                    {
                        echo("Y");
                    }
                    else
                    {
                        echo("N");
                    }
                mysql_close($con2);
                echo(">".$row['cmpname']."</option>");
            }
        mysql_close();
        ?>
        </select></td>
        <td class=tdgeneral><select name=tt id=t><?php //<option selected value=B>Domestic and international requests</option> ?>
        <option selected value=D>Domestic requests</option>
        <option value=I>International requests</option>
        </select></td>
        <td class=tdgeneral><select name=pri id=p><option selected value=Y>Primary agent</option>
        <option value=N>Backup agent</option></select>
		<td class="tdgeneral" align=center><input type=submit value=Add  style="padding-left: 1px; padding-right: 1px;"></td>
	</tr>
</form>
<?php
}
?>
</table>
<p><i>* The primary agent is the agent who, by default, receives the travel requests from the client.<br>
If the primary agent is not available, the backup agent can claim the request from the primary agent.<Br>
Note: Only 1 primary agent is allowed per client per travel type however there can be many backup agents.</i></p>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=setup.php class=grey>Go back</a></p>
</body>

</html>
