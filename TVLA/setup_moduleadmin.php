<?php
include("inc_ignite.php");

include("inc_admin.php");

$result = "<p>&nbsp;</p>";
//GET VARIABLES PASSED BY FORM
$atkid = $_POST['atkid'];
//IF THE VARIABLES WERE PASSED CORRECTLY THEN PERFORM UPDATE
if($atkid != "X" && strlen($atkid)>0)
{
    //CHECK IF NEW USER EXISTS IN ACCESS TABLE ALREADY
    $sql = "SELECT * FROM assist_".$cmpcode."_tvla_list_admins WHERE tkid = '".$atkid."' AND yn = 'Y'";
    include("inc_db_con.php");
    $trow = mysql_num_rows($rs);
    if($trow > 0)
    {
        //IF THE USER WAS FOUND IN THE ACCESS TABLE - ERROR
        $err = "Y";
        $result = "<p>Error - the requested action could not be completed as the user already exists in Travel Assist as a module administrator.</p>";
        //SET TRANSACTION LOG VALUES
        $tsql = $sql;
        $trans = "Error - User ".$atkid." could not be added to TVLA as user already had access.";
    }
    else
    {
        //ELSE SET VARIABLE TO CONTINUE
        $err = "N";
    }
    mysql_close();
    //IF RECORD WAS NOT FOUND THEN PERFORM UPDATE
    if($err == "N")
    {
        $sql = "INSERT INTO assist_".$cmpcode."_tvla_list_admins SET tkid = '".$atkid."', yn = 'Y'";
        include("inc_db_con.php");
        //SET TRANSACTION LOG VALUES FOR UPDATE
        $tsql = $sql;
        $trans = "User ".$atkid." added to TVLA.";
        $result = "<p>Success!  Module Administrator added to Travel Assist.</p>";
    }
    //PERFORM TRANSACTION LOG UPDATE
    $tref = "TVLA";
    include("inc_transaction_log.php");
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delUser(id) {
    if(confirm("Are you sure you wish to perform this action?"))
    {
        document.location.href="setup_moduleadmin_edit.php?id="+id+"&t=d";
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Travel Assist (Agents): Setup - Module Administrators</b></h1>
<?php echo($result); ?>
<table border="1" id="table1" cellspacing="0" cellpadding="4" width=290 style="border-collapse: collapse; border-style: solid;">
	<tr>
		<td class="tdheader" width=30>ID</td>
		<td class="tdheader" width=210>User</td>
		<td class="tdheader" width=50>&nbsp;</td>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.id ";
    $sql .= "FROM assist_".$cmpcode."_tvla_list_admins a, assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE a.yn = 'Y' ";
    $sql .= "AND t.tkid = a.tkid ";
    $sql .= "AND t.tkstatus = 1 ";
    $sql .= "ORDER BY tkname, tksurname";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
?>
        <?php include("inc_tr.php"); ?>
            <td class="tdgeneral"><?php echo($row['tkid']); ?></td>
            <td class="tdgeneral"><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td class="tdgeneral"><input type=button value=Delete  onclick="delUser(<?php echo($row['id']); ?>)" style="padding-left: 1px"></td>
        </tr>
<?php
    }
    mysql_close();

//IF A USER HAS ACCESS TO TA BUT IS NOT IN THE ABOVE RESULT THEN DISPLAY THE ADD FORM
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql.= "WHERE t.tkid = m.usrtkid  ";
$sql.= "AND usrmodref = 'TVLA'  ";
$sql.= "AND t.tkid NOT IN (SELECT tkid FROM assist_".$cmpcode."_tvla_list_admins ";
$sql.= "WHERE yn = 'Y') ";
$sql.= "ORDER BY tkname, tksurname";
include("inc_db_con.php");
$ta = mysql_num_rows($rs);
if($ta > 0)
{
?>
<form method=POST action=setup_moduleadmin.php>
    <?php include("inc_tr.php"); ?>
		<td class="tdgeneral">&nbsp;</td>
		<td class="tdgeneral">
            <select name=atkid>
                <option selected value=X>--- SELECT ---</option>
                <?php
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
                    }
                ?>
            </select>
        </td>
		<td class="tdgeneral"><input type=submit value=Add  style="padding-left: 7px; padding-right: 6px;"></td>
	</tr>
</form>
<?php
}
?>
</table>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=setup.php class=grey>Go back</a></p>
</body>

</html>
