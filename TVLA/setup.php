<?php
include("inc_ignite.php");

//GET SETUP ADMIN TKID
$ta0 = $_POST['O'];
//IF SETUP ADMIN TKID WAS SENT THEN PERFORM UPDATE
if(strlen($ta0) > 0)
{
    //GET CURRENT SETUP ADMIN TKID
    $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'TVLA' AND refid = 0";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ta0z = $row['value'];
    mysql_close();
    $sql = "";
    //UPDATE ASSIST-SETUP TABLE
    $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0."' WHERE ref = 'TVLA' AND refid = 0";
    include("inc_db_con.php");
        //Set transaction log values for this update
        $trans = "Updated Setup Administrator to ".$ta0;
    $sql = "";

    //PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUS;
        $tsql = $sql;
        $told = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0z."' WHERE ref = 'TVLA' AND refid = 0";
        $tref = 'TVLA';
        include("inc_transaction_log.php");

    $result = "<p><i>Administrator update complete.</i></p>";
}
else
{
    $result = "<p>&nbsp;</p>";
}

include("inc_admin.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Travel Assist (Agents): Setup</b></h1>
<?php echo($result); ?>
<ul>
<form name=update method=post action=setup.php>
<li>Setup Administrator: <select name=O>
<?php
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($setupadmn == "0000")
{
?>
    <option selected value=0000>Ignite Assist Administrator</option>
<?php
}
else
{
?>
    <option value=0000>Ignite Assist Administrator</option>
<?php
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = 'TVLA' ";
$sql .= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tid = $row['tkid'];
    if($tid == $setupadmn)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
mysql_close();

?>
</select> <input type=submit value=Update style="padding-left: 1px"><br>&nbsp;</li>

<li><a href=setup_moduleadmin.php>Module Administrators</a><br><i>These are the people who can see all travel requests and reassign them if necessary.</i><br>&nbsp;</li>

<?php if($requestaction == "C" && $abc == "xyz") { ?>
<li><u>Delay Notification:</u>
<br>Notify Module Administrators of requests not claimed: <select><option selected value=Y>Yes</option><option value=N>No</option></select>
<br>If yes, please indicate the age of the requests: <select>
<option selected>1 hour</option>
<?php
for($n=2;$n<19;$n++)
{
    echo("<option value=".$n.">".$n." hours</option>");
    if($n==12) { $n = 17; }
}
    echo("<option value=24>1 day</option>");
for($n=36;$n<121;$n++)
{
    $d = $n/24;
    echo("<option value=".$n.">".$d." days</option>");
    $n = $n+11;
}
    echo("<option value=168>1 week</option>");
?>
</select>
<br><input type=button value=Update>
<br>&nbsp;</li>
<?php }

if($requestaction == "A") {
?>
<li><a href=setup_agents.php>Assign Agents to Clients</a><br><i>Assign specific travel agents to deal with specific clients.</i><br>&nbsp;</li>
<?php } ?>
</ul>
<?php
$helpfile = "../help/TVL_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</form>
</body>

</html>
