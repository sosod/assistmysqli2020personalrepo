<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 *
 *
 * LOG TYPE:
 * 	A=Approve
 * 	C=Create
 * 	D=Delete
 * 	E=Edit
 *  F=conFirm
 * 	L=unLock
 * 	N=decliNe
 * 	R=Restore
 * 	T=deacTivate
 * 	U=Update
 *  V=actiVate
 *
 */

class PM3_LOG_OBJECT extends PM3 {
	private $log;
	private $log_table;
	private $field_prefix;
	private $fields;
	private $has_status;
	private $status_table;
	private $is_setup_log;



	private $headings;
	private $replace_headings;
	private $headObject;
	private $displayObject;

    const TABLE = "log";
    const TABLE_FLD = "log_";

	const APPROVE 	= "A";
	const CREATE	= "C";
	const DELETE 	= "D";
	const EDIT 		= "E";
	const CONFIRM	= "F";
	const UNLOCK 	= "L";
	const DECLINE 	= "N";
	const RESTORE 	= "R";
	const DEACTIVATE= "T";
	const UPDATE 	= "U";
	const ACTIVATE	= "V";

	public function __construct() {
		parent::__construct();

		//if(strlen($log_table)>0) {
		//	$this->setLogTable($log_table);
		//}
		//$this->headObject = new PM3_HEADINGS();
		//$this->displayObject = new PM3_DISPLAY();
	}


	/**********************************
	 * CONTROLLER functions
	 */
	public function getObject($var){
		return array($this->getAuditLogHTML($var));
		//return array("getobject",serialize($var));
	}

	/**
	 * ADD NEW LOG
	 * @param Array of insert variables
	 * @param		'object' => OBJECT_TYPE (Required)
	 * @param		'action' => const (Required)
	 * @param		'display'  => Text to display in the log table
	 * @param		'old'  => serialized array of previous object values
	 * @param		'new'  => serialized array of new object values
	 * @param	  At least 1 ID required
	 * @param		'scd_id'
	 * @param		'asmt_id'
	 * @param		'trgr_id'
	 * @param		'lne_id'
	 * @param		'scr_id'
	 * @param		'prd_id'
	 */
	public function addObject($var=array(),$object="",$action="",$display="",$old="",$new="",$scd_id=0,$asmt_id=0,$trgr_id=0,$lne_id=0,$scr_id=0,$prd_id=0){
		if(!isset($var['insertuser'])) { $var['insertuser'] = $this->getUserID(); }
		if(!isset($var['insertusername'])) { $var['insertusername'] = $this->getUserName(); }
		if(!isset($var['insertdate'])) { $var['insertdate'] = date("Y-m-d H:i:s"); }
		if(!isset($var['status'])) { $var['status'] = self::ACTIVE; }
		if(!isset($var['object'])) { $var['object'] = $object; }
		if(!isset($var['scd_id'])) { $var['scd_id'] = $scd_id; }
		if(!isset($var['asmt_id'])) { $var['asmt_id'] = $asmt_id; }
		if(!isset($var['trgr_id'])) { $var['trgr_id'] = $trgr_id; }
		if(!isset($var['lne_id'])) { $var['lne_id'] = $lne_id; }
		if(!isset($var['scr_id'])) { $var['scr_id'] = $scr_id; }
		if(!isset($var['prd_id'])) { $var['prd_id'] = $prd_id; }
		if(!isset($var['action'])) { $var['action'] = $action; }
		if(!isset($var['display'])) { $var['display'] = $display; }
		if(!isset($var['old'])) { $var['old'] = is_array($old) ? serialize($old) : $old; }
		if(!isset($var['new'])) { $var['new'] = is_array($new) ? serialize($new) : $new; }
		$data = $this->convertGenericToSpecific($var);
		$insert_data = $this->convertArrayToSQL($data);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$insert_data;
//		return $sql;
		$result = $this->db_insert($sql);
		return $result;
	}



	/***
	 * GENERIC FUNCTIONS
	 */
	public function convertGenericToSpecific($var) {
		$data = array();
		$tbl_fld = $this->getTableField();
		foreach($var as $key => $x) {
			$data[$tbl_fld.$key] = $x;
		}
		return $data;
	}

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }


	/********
	 * Functions to return log_action constants
	 */
	function getApproveLogAction() { return self::APPROVE; }
	function getCreateLogAction() { return self::CREATE; }
	function getAddLogAction() { return self::CREATE; }
	function getDeleteLogAction() { return self::DELETE; }
	function getEditLogAction() { return self::EDIT; }
	function getConfirmLogAction() { return self::CONFIRM; }
	function getUnlockLogAction() { return self::UNLOCK; }
	function getDeclineLogAction() { return self::DECLINE; }
	function getRestoreLogAction() { return self::RESTORE; }
	function getDeactivateLogAction() { return self::DEACTIVATE; }
	function getUpdateLogAction() { return self::UPDATE; }
	function getActivateLogAction() { return self::ACTIVATE; }

    public function getScorecardLogDetails($scd_id,$log_type) {
        if((is_array($scd_id) && count(ASSIST_HELPER::removeBlanksFromArray($scd_id))==0) || (!is_array($scd_id) && !ASSIST_HELPER::checkIntRef($scd_id))) {
            if(is_array($scd_id)) {
                return array();
            } else {
                return array('date'=>0,'user'=>"X");
            }
        } else {
            if(is_array($scd_id)) {
                $scd_id = ASSIST_HELPER::removeBlanksFromArray($scd_id);
                $sql = "SELECT log_scd_id as scd_id, log_insertdate as activation_date, log_insertusername as activation_user
						FROM ".$this->getTableName()."
						WHERE log_object = '".PM3_SCORECARD::OBJECT_TYPE."'
						AND log_scd_id IN (".implode(",",$scd_id).")
						AND log_action = '".$log_type."'
						ORDER BY log_insertdate ASC";
                $results = $this->mysql_fetch_all($sql);
                $details = array();
                foreach($results as $row) {
                    $s = $row['scd_id'];
                    $d = $row['activation_date'];
                    $u = $row['activation_user'];
                    $details[$s] = array('date'=>strtotime($d),'user'=>$u);
                }
            } else {
                $sql = "SELECT log_insertdate as activation_date, log_insertusername as activation_user
						FROM ".$this->getTableName()."
						WHERE log_object = '".PM3_SCORECARD::OBJECT_TYPE."'
						AND log_scd_id = $scd_id
						AND log_action = '".$log_type."'
						ORDER BY log_insertdate DESC LIMIT 1";
                $row = $this->mysql_fetch_one($sql);
                if(!isset($row['activation_date'])) {
                    $details = array('date'=>0,'user'=>"X");
                } else {
                    $date = $row['activation_date'];
                    if(strlen($date)==0 || is_null($date) || $date == null || $date == "NULL") {
                        $details = array('date'=>0,'user'=>"X");
                    } else {
                        $u = $row['activation_user'];
                        $details = array('date'=>strtotime($date),'user'=>$u);
                    }
                }
            }
            return $details;
        }
    }
}
?>