<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class PM3_SCORECARD_WEIGHT extends PM3 {
    
	protected $object_id = 0;
	protected $object_details = array();
    
	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_scdid";
    /*
	protected $name_field = "_value";
	protected $progress_status_field = "_statusid";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "SCDWEIGHT";
    const TABLE = "scorecards_weight";
    const TABLE_FLD = "sw";
    const REFTAG = "SW";
	const LOG_TABLE = "log";
	/**
	 * Type CONSTANTS
	 */
	/**
	 * STATUS CONSTANTS
	 */
    
    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
/*
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$result = array("error","addObject not programmed"); 
		/*
		
		unset($var['attachments']);
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		
		$trgr_id = $var['trigger'];
		$lne_id = $var['line'];
		$lvl_id = $var['level'];
		$value = $var['value'];
		$iu = $this->getUserID();
		$s = self::ACTIVE;
		
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ccs_trgrid = ".$trgr_id." AND ccs_lneid = ".$lne_id." AND ccs_lvlid = ".$lvl_id." AND ".$this->getStatusFieldName()." = ".self::ACTIVE;
		//get all records
		$old = $this->mysql_fetch_all($sql);
		
		if(count($old)==0) {
			$sql = "INSERT INTO ".$this->getTableName()." VALUES (null, $trgr_id, $lne_id, $lvl_id, $value, $s, '$iu', now())";
			$id = $this->db_insert($sql);
			$this->addLogRecord($id,"C","0",$value,$sql);
			$result = array("ok","Record added.");
		} elseif($old[0][$this->getTableField()."_value"]!=$value) {
			//update first record
			$old = $old[0];
			$sql = "UPDATE ".$this->getTableName()." SET ccs_value = ".$value." WHERE ".$this->getIDFieldName()." = ".$old[$this->getIDFieldName()];
			$mnr = $this->db_update($sql);
			$this->addLogRecord($old[$this->getIDFieldName()],"E",$old[$this->getNameFieldName()],$value,$sql);
			//LOG CHANGE
			$result = array("ok","Record updated.");
		} else {
			$result = array("info","No change.");
		}
		*/
		
		return $result;
	}

	private function addLogRecord($ccs_id,$type,$old,$new,$i_sql) {
		$ui = $this->getUserID();
		$i_sql = addslashes($i_sql);
		$sql = "INSERT INTO ".$this->getTableName()."_log VALUES (null,$ccs_id,'$type',$old,$new,'$i_sql','$ui',now())";
		$this->db_insert($sql);
	} 

	
	
	public function saveWeights($var) {
		$result = array("error","Weight saving unsuccessful.");
		
		$obj_id = $var['obj_id'];
		$old = $this->getRawObject(0,$obj_id);
		
		$insert_data = $var;
		unset($insert_data['obj_id']);
		if(count($old)==0 || !isset($old[$this->getIDFieldName()])) {
			//CREATE NEW RECORD
			$result = $this->createNewRecord($insert_data);
		} else {
			//UPDATE OLD RECORD
			$id = $old[$this->getIDFieldName()];
			if($this->checkIntRef($id)) {
				$result = $this->editRecord($id,$insert_data);
			} else {
				$result = $this->createNewRecord($insert_data);
			}
		}
		//LOGGING
		$old_row = $old; 
		unset($old);
		$old = array(
			'kpa' => $old_row[$this->getTableField().'_kpa'],
			'cc' => $old_row[$this->getTableField().'_cc'],
			'jal' => $old_row[$this->getTableField().'_jal'],
		);
		$new = array(
			'kpa' => $insert_data[$this->getTableField().'_kpa'],
			'cc' => $insert_data[$this->getTableField().'_cc'],
			'jal' => $insert_data[$this->getTableField().'_jal'],
		);
		$type = PM3_SCORECARD::OBJECT_TYPE;
		$transaction = "Set component weights";
		$parent_ids = array();
		$this->logMyAction($obj_id, "EDIT", $old, $new, $parent_ids, $type, $transaction);
		return $result;
	}
	
	private function createNewRecord($insert_data) {
			$insert_data[$this->getTableField().'_insertuser'] = $this->getUserID();
			$insert_data[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
			$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($insert_data);
			$this->db_insert($sql);
			$result = array("ok","Weights saved successfully");
			return $result;
	}
	
	private function editRecord($id,$update_data) {
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		$result = array("ok","Weights saved successfully");
		return $result;
	}
	
	/*

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}
	
	
	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
		
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type']; 
		$ref = $var['ref'];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			return array("ok",$type." ".$ref." successfully removed from this assessment.");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this assessment. Please reload the page and try again.");
		}
		
		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
	
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}
	
	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}	
	
	*/

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    
    
	
	
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
    
	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
	}
	
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id=0,$parent_id=0) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ";
		if($this->checkIntRef($obj_id)) {
			$sql.=$this->getIDFieldName()." = ".$obj_id;
		} else {
			$sql.=$this->getParentFieldName()." = ".$parent_id;
		} 
		$data = $this->mysql_fetch_one($sql);

		return $data;
	}
	
	
	
	public function getComponentWeightForAScorecard($scd_id) { 
		$row = $this->getRawObject(0,$scd_id);
		
		$data = array(
			'kpa'=>$row[$this->getTableField()."_kpa"],
			'cc'=>$row[$this->getTableField()."_cc"],
			'jal'=>$row[$this->getTableField()."_jal"],
		);
		
		return $data;
	}
	
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}	
*/	
	
	
	
	
	
	
	

	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a core competency
	 */ 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
        
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */



	private function logMyAction($id,$action,$old,$new,$parent_ids,$type,$transaction_comment = "") {
		$logObj = new PM3_LOG_OBJECT();
		switch($action) {
			case "EDIT":
				$action = $logObj->getEditLogAction();
				$display = $transaction_comment;
				break;
		}
		$logObj->addObject(
			$parent_ids,
			$type,
			$action,
			$display,
			$old,
			$new,
			$id
		);
		
	}


}


?>