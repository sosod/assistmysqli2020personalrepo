<?php
/**
 * To manage the CONTRACT object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class PM3_LINE extends PM3 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_scdid";
    /*
	protected $progress_status_field = "_status_id";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */

     //Weight to use for new row
   const NEW_WEIGHT = -1;

    const OBJECT_TYPE = "LINE";
    const PARENT_OBJECT_TYPE = "SCORECARD";
    const TABLE = "lines";
    const TABLE_FLD = "lne";
    const REFTAG = "SL";
	const LOG_TABLE = "lines";

	const CCREFTAG = "CC";
	/**
	 * STATUS CONSTANTS
	 */
	const CONFIRMED = 32;
	const ACTIVATED = 64;


    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
/*		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }

	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		$create_type = $var['create_type'];
		unset($var['create_type']);
		switch($create_type) {
			case "PROJ":
				return $this->addProject($var);
				break;
			case "JAL":
				return $this->addJAL($var);
				break;
			case "TOP":
				return $this->addTOP($var);
				break;
			case "KPI":
			default:
				return $this->addKPI($var);
				break;
		}
		return array("error","Sorry, I can't work out what you want me to add.");
	}
	/*

	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}


	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}
		*/
	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type'];
		$ref = $var['ref'];

				$sql = "SELECT  ".$this->getStatusFieldName()." as stat
						, ".$this->getParentFieldName()." as scd_id
						, ".self::TABLE_FLD."_srcmodref as srcmodref
						, ".self::TABLE_FLD."_srcid as srcid
						, ".self::TABLE_FLD."_srctype as srctype
						FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
				$old_stat = $this->mysql_fetch_one($sql);

		$sql = "UPDATE ".$this->getTableName()."
				SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.")
				WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			$scd_id = $old_stat['scd_id'];
			$ref = $old_stat['srcmodref']."/".$this->getTypeRefTag($old_stat['srctype']).$old_stat['scd_id'];
			$new = array($this->getStatusFieldName()=>($old_stat['stat'] - self::ACTIVE));
			$old = array($this->getStatusFieldName()=>$old_stat['stat']);
			$this->logMyAction($object_id, "DEACTIVATE", $old, $new,array('scd_id'=>$scd_id),"CC",$ref);

			return array("ok",$type." ".$ref." successfully removed from this ".$this->getObjectName(self::PARENT_OBJECT_TYPE).".");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this ".$this->getObjectName(self::PARENT_OBJECT_TYPE).". Please reload the page and try again.");
		}

		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}
	/*

	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}

	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];

		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}

	*/


    /*************************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getCCRefTag() { return self::CCREFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}


	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options);
	}
	*/
	public function getAObject($id=0,$options=array(),$include_module_folder=false) {
		$row = $this->getRawObject($id,$include_module_folder);
		$tbl_fld = $this->getTableField();
		$row['parent_id'] = $row[$this->getParentFieldName()];
		$row['srcmodref'] = $row[$tbl_fld."_srcmodref"];
		$row['srcid'] = $row[$tbl_fld."_srcid"];
		$row['srctype'] = $row[$tbl_fld."_srctype"];
		$row['weight'] = $row[$tbl_fld."_weight"];
		return $row;
	}
	/*
	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/


	public function getLines($parent_id,$type="KPI") {
		$data = array();

		$fld_name = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$parent_fld = $this->getParentFieldName();
		$status_fld = $this->getStatusFieldName();

		$sql = "SELECT ".$id_fld." as id, ".$fld_name."_srcmodref as modref, ".$fld_name."_srcid as srcid, ".$fld_name."_kpaid as kpa
				FROM ".$this->getTableName()."
				WHERE ".$parent_fld." = ".$parent_id."
				AND ".$fld_name."_srctype = '$type'
				AND (".$status_fld." & ".self::ACTIVE.") = ".self::ACTIVE;
		$lines = $this->mysql_fetch_all_by_id2($sql, "modref","id");
		/*
		 * $lines[modref][id] = array(id=>#,modref=>"SDP?",srcid=>#,kpa=>#)
		 */

		$sdbp5Obj = new SDBP5_PMS();
		foreach($lines as $modref=>$ls) {
			$var = array();
			$var['modref'] = $modref;
			$var['filter_type'] = "ID";
			$var['primary_source'] = $this->getPrimaryKPASource();
			$var['filter_id'] = array_keys($ls);
			$d = $sdbp5Obj->getKPIs($var,false);
			foreach($d['objects'] as $i=>$x) {
				$data[$x['kpa']][] = $x;
			}
		}

		return $data;
	}

	public function getLineSrcIDs($parent_id,$modref,$type="KPI",$active_only=true) {
		$tbl_fld = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$parent_fld = $this->getParentFieldName();
		$status_fld = $this->getStatusFieldName();

		$data = array();
		if($type=="CC") {
			$sql = "SELECT ".$tbl_fld."_srcid as srcid, ".$id_fld." as id
					FROM ".$this->getTableName()."
					WHERE ".$parent_fld." = ".$parent_id."
					AND ".$tbl_fld."_srctype = '$type'
					".($active_only ? "AND (".$status_fld." & ".self::ACTIVE.") = ".self::ACTIVE : "");
			$data = $this->mysql_fetch_value_by_id($sql, "srcid","id");
		} else {
			$sql = "SELECT ".$tbl_fld."_srcid as srcid
					FROM ".$this->getTableName()."
					WHERE ".$parent_fld." = ".$parent_id."
					AND ".$tbl_fld."_srctype = '$type'
					AND ".$tbl_fld."_srcmodref = '".$modref."'
					AND (".$status_fld." & ".self::ACTIVE.") = ".self::ACTIVE;
			$data = $this->mysql_fetch_all_by_value($sql, "srcid");
		}

		return $data;
	}


	public function getAllLines($parent_id,$type="KPI",$var=array(),$get_results=false,$time_ids=array()) {
		$tbl_fld = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$parent_fld = $this->getParentFieldName();
		$status_fld = $this->getStatusFieldName();
		//get all lines for the assessment by the type indicated
			//group results by module
		$data = array();
		$sql = "SELECT ".$id_fld." as id, ".$tbl_fld."_srcmodref as modref, ".$tbl_fld."_srcid as srcid, ".$tbl_fld."_kpaid as kpa
				FROM ".$this->getTableName()."
				WHERE ".$parent_fld." = ".$parent_id."
				AND ".$tbl_fld."_srctype = '$type'
				AND (".$status_fld." & ".self::ACTIVE.") = ".self::ACTIVE; //echo $sql;
		/*
		 * $lines[modref][id] = array(id=>#,modref=>"SDP?",srcid=>#,kpa=>#)
		 */
		$lines = $this->mysql_fetch_all_by_id2($sql, "modref","srcid");
		//$this->arrPrint($lines);
		//for each module
			//get the line details
		$moddata = array();
		if($type=="JAL") {
			$sdbp5Obj = new JAL1_PMS();
			$primary_source = $this->getPrimaryJALSource();
			$sources = $this->getJALSources();
		} else {
			$primary_source = $this->getPrimaryKPASource();
			$sdbp5Obj = new SDBP5B_PMS();
			$sources = $this->getKPASources();
		}
		$kpas = array();
		foreach($lines as $modref=>$ls) {
			$var = array();
			$var['modref'] = $modref;
			$var['filter_type'] = "ID";
			$var['primary_source'] = $primary_source;
			$var['filter_id'] = array_keys($ls);
			if($type=="PROJ") {
				$md = $sdbp5Obj->getProjects($var,false,$get_results,$time_ids);
			} elseif($type=="JAL") {
				$md = $sdbp5Obj->getObjects($var,false,$get_results,$time_ids);
			} elseif($type=="TOP") {
				$md = $sdbp5Obj->getTopKPIs($var,false,$get_results,$time_ids);
			} else {
				$md = $sdbp5Obj->getKPIs($var,false,$get_results,$time_ids);
			}
			$moddata[$modref] = $md['objects'];
		}
		//for each line
			//group by ORIGINAL kpa
		$full_kpas = $sdbp5Obj->getKPAs($sources,true,$primary_source);
		foreach($lines as $modref => $ls) {
			foreach($ls as $l) {
				$row = $moddata[$modref][$l['srcid']];
				$kpa_id = $l['kpa'];
				//If line comes from module other than primary source of KPAs, convert that KPA id to primary source KPA ID
				if($modref!=$primary_source) {
					$kpa_id = $full_kpas['convert'][$modref][$kpa_id];
				}
				$line_id = $l['id'];
				$row[$tbl_fld.'_kpaid'] = $kpa_id;
				$row[$id_fld] = $line_id;
				$data[$kpa_id][$line_id] = $row;
			}
		}
		return $data;
	}

	public function getLinesInUse($modref,$type,$keys) {
		$tbl_fld = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$parent_fld = $this->getParentFieldName();
		$status_fld = $this->getStatusFieldName();

		$data = array();
		$scdObj = new PM3_SCORECARD();
		$scd_tbl_fld = $scdObj->getTableField();
		$scd_id_fld = $scdObj->getIDFieldName();
		$scd_status_fld = $scdObj->getStatusFieldName();

		$sql = "SELECT ".$tbl_fld."_srcid as srcid, ".$scd_tbl_fld."_tkid as employee, CONCAT(TK.tkname,' ',TK.tksurname) as name
				FROM ".$this->getTableName()."
				INNER JOIN ".$scdObj->getTableName()."
				ON ".$scd_id_fld." = ".$parent_fld."
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK
				ON ".$scd_tbl_fld."_tkid = TK.tkid
				WHERE (".$scd_status_fld." & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (".$status_fld." & ".self::ACTIVE.") = ".self::ACTIVE."
				AND ".$tbl_fld."_srcmodref = '$modref'
				AND ".$tbl_fld."_srctype = '$type'
				";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$data[$r['srcid']][$r['employee']] = $r['name'];
		}
		return $data;
	}

	public function getFullLines($parent_id,$get_results = false, $time_ids= array()) {
		$data = array();
		$k = $this->getAllLines($parent_id,"KPI",array(),$get_results,$time_ids);
		$p = $this->getAllLines($parent_id,"PROJ",array(),$get_results,$time_ids);
		foreach($k as $kpa_id => $lines) {
			$data[$kpa_id] = $lines;
			if(isset($p[$kpa_id])) {
				$data[$kpa_id] = $data[$kpa_id] + $p[$kpa_id];
				unset($p[$kpa_id]);
			}
		}
		if(count($p)>0) {
			foreach($p as $kpa_id => $lines) {
				$data[$kpa_id] = $lines;
			}
		}
		return $data;
	}

	public function getFullLinesForJAL($parent_id) {
		return $this->getFullLinesByType($parent_id,false,array(),"JAL");
	}

	public function getFullLinesForAll($parent_id,$get_results=false,$time_ids=array()) {
		$result = array(
			'SDBIP'=>$this->getFullLinesByType($parent_id,$get_results,$time_ids,"SDBIP"),
			'JAL'=>$this->getFullLinesForJAL($parent_id)
		);
		return $result;
	}

	public function getFullLinesByType($parent_id,$get_results = false, $time_ids= array(),$src="SDBIP") {
		$data = array();
		if($src=="SDBIP" || $src=="ALL") {
			$k = $this->getAllLines($parent_id,"KPI",array(),$get_results,$time_ids);
			$t = $this->getAllLines($parent_id,"TOP",array(),$get_results,$time_ids);
			$p = $this->getAllLines($parent_id,"PROJ",array(),$get_results,$time_ids);
			foreach($k as $kpa_id => $lines) {
				$data[$kpa_id]['KPI'] = $lines;
				if(isset($t[$kpa_id])) {
					$data[$kpa_id]['TOP'] = $t[$kpa_id];
					unset($t[$kpa_id]);
				} else {
					$data[$kpa_id]['TOP'] = array();
				}
				if(isset($p[$kpa_id])) {
					$data[$kpa_id]['PROJ'] = $p[$kpa_id];
					unset($p[$kpa_id]);
				} else {
					$data[$kpa_id]['PROJ'] = array();
				}
			}
			if(count($t)>0) {
				foreach($t as $kpa_id => $lines) {
					$data[$kpa_id]['TOP'] = $lines;
				}
			}
			if(count($p)>0) {
				foreach($p as $kpa_id => $lines) {
					$data[$kpa_id]['PROJ'] = $lines;
				}
			}
		}
		if($src!="SDBIP" || $src=="ALL") {
			$j = $this->getAllLines($parent_id,"JAL",array(),false,array());
			foreach($j as $kpa_id => $lines) {
				$data[$kpa_id]['JAL'] = $lines;
			}
		}
		return $data;
	}

	public function getLineWeights($parent_id,$type="") {
		$tbl_fld = $this->getTableField();
		$parent_fld = $this->getParentFieldName();
		$id_fld = $this->getIDFieldName();
		$status_fld = $this->getStatusFieldName();
		$sql = "SELECT ".$id_fld.", ".$tbl_fld."_weight FROM ".$this->getTableName()."
				WHERE ".$parent_fld." = ".$parent_id."
				".(strlen($type)>0 ? " AND ".$tbl_fld."_srctype = '$type' " : "")."
				AND ".$status_fld." = ".self::ACTIVE; //echo $sql;
		return $this->mysql_fetch_value_by_id($sql, $id_fld, $tbl_fld."_weight");
	}
	public function getKPAWeights($parent_id) {
		$tbl_fld = $this->getTableField();
		$parent_fld = $this->getParentFieldName();
		$id_fld = $this->getIDFieldName();
		$status_fld = $this->getStatusFieldName();
		$sql = "SELECT ".$tbl_fld."_kpaid, SUM(".$tbl_fld."_weight) as ".$tbl_fld."_weight
				FROM ".$this->getTableName()."
				WHERE ".$parent_fld." = $parent_id AND ".$status_fld." = ".self::ACTIVE."
				GROUP BY ".$tbl_fld."_kpaid";
		return $this->mysql_fetch_value_by_id($sql, "".$tbl_fld."_kpaid", $tbl_fld."_weight");
	}
	public function getKPAWeightsByType($parent_id) {
		$tbl_fld = $this->getTableField();
		$parent_fld = $this->getParentFieldName();
		$id_fld = $this->getIDFieldName();
		$status_fld = $this->getStatusFieldName();
		$sql = "SELECT ".$tbl_fld."_kpaid as k, ".$tbl_fld."_srctype as t, SUM(".$tbl_fld."_weight) as w
				FROM ".$this->getTableName()."
				WHERE ".$parent_fld." = $parent_id AND ".$status_fld." = ".self::ACTIVE."
				GROUP BY ".$tbl_fld."_kpaid, ".$tbl_fld."_srctype";
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $r) {
			$data[$r['k']][$r['t']] = $r['w'];
		}
		return $data;
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id,$include_modloc=false) {
		if($include_modloc) {
			$sql = "SELECT L.*, mm.modlocation as module
					FROM ".$this->getTableName()." L
					LEFT OUTER JOIN assist_menu_modules mm
					ON mm.modref = L.lne_srcmodref
					WHERE L.".$this->getIDFieldName()." = ".$obj_id;
		} else {
			$sql = "SELECT L.*
					FROM ".$this->getTableName()." L WHERE L.".$this->getIDFieldName()." = ".$obj_id;
		}
		$data = $this->mysql_fetch_one($sql);

		return $data;
	}
	/*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}
*/








	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}



	/****
	 * functions to check on the status of a core competency
	 */
	public function coreCompInUse() {
		$tbl_fld = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$sql = "SELECT ".$tbl_fld."_srcid as srcid,
				count(".$id_fld.") as cc
				FROM ".$this->getTableName()."
				WHERE ".$tbl_fld."_srctype = 'CC'
				GROUP BY ".$tbl_fld."_srcid";
		$data = $this->mysql_fetch_value_by_id($sql, "srcid", "cc");
		return $data;
	}





    /***
     * SET / UPDATE Functions
     */

	public function addKPI($var) {
		$result = array("info","No KPIs selected.  Please try again.");
		if(count($var['obj'])>0) {
			$insertdata = array();
			$raw_insertdata = array();
			$raw_insertdata[self::TABLE_FLD.'_id'] = "null";
			$raw_insertdata[$this->getParentFieldName()] = $var['obj_id'];
			$scd_id = $var['obj_id'];
			$raw_insertdata[self::TABLE_FLD.'_srcmodref'] = "'".$var['modref']."'";
			$raw_insertdata[self::TABLE_FLD.'_srctype'] = "'KPI'";
			$raw_insertdata[self::TABLE_FLD.'_weight'] = self::NEW_WEIGHT;
			$raw_insertdata[self::TABLE_FLD.'_status'] = self::ACTIVE;
			$raw_insertdata[self::TABLE_FLD.'_insertuser'] = "'".$this->getUserID()."'";
			$raw_insertdata[self::TABLE_FLD.'_insertdate'] = "now()";

			foreach($var['obj'] as $src_id) {
				$insertdata[$src_id] = $raw_insertdata;
				$insertdata[$src_id][self::TABLE_FLD.'_kpaid'] = $var['kpa'][$src_id];
				$insertdata[$src_id][self::TABLE_FLD.'_srcid'] = $src_id;
			}

			$fields = array_keys($raw_insertdata);
			$fields[] = self::TABLE_FLD.'_kpaid';
			$fields[] = self::TABLE_FLD.'_srcid';

			if(count($insertdata)>0) {
				$sql0 = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				foreach($insertdata as $i) {
					$sql = $sql0."(".implode(",",$i).")";
					$id = $this->db_insert($sql);
					$this->logMyAction($id, "CREATE", "", ASSIST_HELPER::code(serialize($i)),array('scd_id'=>$scd_id),"KPI",$var['modref']."/".SDBP5_DEPT::REFTAG.$i[self::TABLE_FLD.'_srcid']);
				}
				$result = array("ok","KPIs added successfully.");
			}
		}
		return $result;
	}

	public function addTOP($var) {
		$result = array("info","No KPIs selected.  Please try again.");
		if(count($var['obj'])>0) {
			$insertdata = array();
			$raw_insertdata = array();
			$raw_insertdata[self::TABLE_FLD.'_id'] = "null";
			$raw_insertdata[$this->getParentFieldName()] = $var['obj_id'];
			$scd_id = $var['obj_id'];
			$raw_insertdata[self::TABLE_FLD.'_srcmodref'] = "'".$var['modref']."'";
			$raw_insertdata[self::TABLE_FLD.'_srctype'] = "'TOP'";
			$raw_insertdata[self::TABLE_FLD.'_weight'] = self::NEW_WEIGHT;
			$raw_insertdata[self::TABLE_FLD.'_status'] = self::ACTIVE;
			$raw_insertdata[self::TABLE_FLD.'_insertuser'] = "'".$this->getUserID()."'";
			$raw_insertdata[self::TABLE_FLD.'_insertdate'] = "now()";

			foreach($var['obj'] as $src_id) {
				$insertdata[$src_id] = $raw_insertdata;
				$insertdata[$src_id][self::TABLE_FLD.'_kpaid'] = $var['kpa'][$src_id];
				$insertdata[$src_id][self::TABLE_FLD.'_srcid'] = $src_id;
			}

			$fields = array_keys($raw_insertdata);
			$fields[] = self::TABLE_FLD.'_kpaid';
			$fields[] = self::TABLE_FLD.'_srcid';

			if(count($insertdata)>0) {
				$sql0 = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				foreach($insertdata as $i) {
					$sql = $sql0."(".implode(",",$i).")";
					$id = $this->db_insert($sql);
					$this->logMyAction($id, "CREATE", "", ASSIST_HELPER::code(serialize($i)),array('scd_id'=>$scd_id),"KPI",$var['modref']."/".SDBP5_DEPT::REFTAG.$i[self::TABLE_FLD.'_srcid']);
				}
				$result = array("ok","KPIs added successfully.");
			}
		}
		return $result;
	}



	public function addProject($var) {
		$result = array("info","No Projects selected.  Please try again.");
		if(count($var['obj'])>0) {
			$insertdata = array();
			$raw_insertdata = array();
			$raw_insertdata[self::TABLE_FLD.'_id'] = "null";
			$raw_insertdata[$this->getParentFieldName()] = $var['obj_id'];
			$scd_id = $var['obj_id'];
			$raw_insertdata[self::TABLE_FLD.'_srcmodref'] = "'".$var['modref']."'";
			$raw_insertdata[self::TABLE_FLD.'_srctype'] = "'PROJ'";
			$raw_insertdata[self::TABLE_FLD.'_weight'] = self::NEW_WEIGHT;
			$raw_insertdata[self::TABLE_FLD.'_status'] = self::ACTIVE;
			$raw_insertdata[self::TABLE_FLD.'_insertuser'] = "'".$this->getUserID()."'";
			$raw_insertdata[self::TABLE_FLD.'_insertdate'] = "now()";

			foreach($var['obj'] as $src_id) {
				$insertdata[$src_id] = $raw_insertdata;
				$insertdata[$src_id][self::TABLE_FLD.'_kpaid'] = $var['kpa'][$src_id];
				$insertdata[$src_id][self::TABLE_FLD.'_srcid'] = $src_id;
			}

			$fields = array_keys($raw_insertdata);
			$fields[] = self::TABLE_FLD.'_kpaid';
			$fields[] = self::TABLE_FLD.'_srcid';

			if(count($insertdata)>0) {
				/*$sql = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				$sarra = array();
				foreach($insertdata as $i) {
					$sarra[] = "(".implode(",",$i).")";
				}
				$sql.=implode(",",$sarra);
				$this->db_insert($sql);*/
				$sql0 = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				foreach($insertdata as $i) {
					$sql = $sql0."(".implode(",",$i).")";
					$id = $this->db_insert($sql);
					$this->logMyAction($id, "CREATE", "", ASSIST_HELPER::code(serialize($i)),array('scd_id'=>$scd_id),"PROJ",$var['modref']."/".SDBP5_CAPITAL::REFTAG.$i[self::TABLE_FLD.'_srcid']);
				}

				$result = array("ok","Projects added successfully.");
			}
		}
		return $result;
	}



	public function addJAL($var) {
		$result = array("info","No Activities selected.  Please try again.");
		if(count($var['obj'])>0) {
			$insertdata = array();
			$raw_insertdata = array();
			$raw_insertdata[self::TABLE_FLD.'_id'] = "null";
			$raw_insertdata[$this->getParentFieldName()] = $var['obj_id'];
			$scd_id = $var['obj_id'];
			$raw_insertdata[self::TABLE_FLD.'_srcmodref'] = "'".$var['modref']."'";
			$raw_insertdata[self::TABLE_FLD.'_srctype'] = "'JAL'";
			$raw_insertdata[self::TABLE_FLD.'_weight'] = self::NEW_WEIGHT;
			$raw_insertdata[self::TABLE_FLD.'_status'] = self::ACTIVE;
			$raw_insertdata[self::TABLE_FLD.'_insertuser'] = "'".$this->getUserID()."'";
			$raw_insertdata[self::TABLE_FLD.'_insertdate'] = "now()";

			foreach($var['obj'] as $src_id) {
				$insertdata[$src_id] = $raw_insertdata;
				$insertdata[$src_id][self::TABLE_FLD.'_kpaid'] = $var['kpa'][$src_id];
				$insertdata[$src_id][self::TABLE_FLD.'_srcid'] = $src_id;
			}

			$fields = array_keys($raw_insertdata);
			$fields[] = self::TABLE_FLD.'_kpaid';
			$fields[] = self::TABLE_FLD.'_srcid';

			if(count($insertdata)>0) {
				/*$sql = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				$sarra = array();
				foreach($insertdata as $i) {
					$sarra[] = "(".implode(",",$i).")";
				}
				$sql.=implode(",",$sarra);
				$this->db_insert($sql);*/
				$activityObject = new JAL1_ACTIVITY(0,$var['modref']);
				$sql0 = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				foreach($insertdata as $i) {
					$sql = $sql0."(".implode(",",$i).")";
					$id = $this->db_insert($sql);
					$this->logMyAction($id, "CREATE", "", ASSIST_HELPER::code(serialize($i)),array('scd_id'=>$scd_id),"JAL",$var['modref']."/".$activityObject->getRefTag().$i[self::TABLE_FLD.'_srcid']);
				}

				$result = array("ok","Activities added successfully.");
			}
		}
		return $result;
	}



	public function addCC($var) {
		$result = array("info","No Core Competencies selected.  Please try again.");
		if(count($var['cc'])>0) {
			$insertdata = array();
			$updatedata = array();
			$raw_insertdata = array();
			$raw_insertdata[self::TABLE_FLD.'_id'] = "null";
			$raw_insertdata[$this->getParentFieldName()] = $var['obj_id'];
			$scd_id = $var['obj_id'];
			$raw_insertdata[self::TABLE_FLD.'_srcmodref'] = "'".$this->getModRef()."'";
			$raw_insertdata[self::TABLE_FLD.'_srctype'] = "'CC'";
			$raw_insertdata[self::TABLE_FLD.'_weight'] = self::NEW_WEIGHT;
			$raw_insertdata[self::TABLE_FLD.'_status'] = self::ACTIVE;
			$raw_insertdata[self::TABLE_FLD.'_insertuser'] = "'".$this->getUserID()."'";
			$raw_insertdata[self::TABLE_FLD.'_insertdate'] = "now()";
			$raw_insertdata[self::TABLE_FLD.'_kpaid'] = 0;

			$lines = $this->getLineSrcIDs($var['obj_id'], $this->getModRef(),"CC",false);

			foreach($var['cc'] as $src_id) {
				if(isset($lines[$src_id])) {
					$updatedata[] = $lines[$src_id];
				} else {
					$insertdata[$src_id] = $raw_insertdata;
					$insertdata[$src_id][self::TABLE_FLD.'_srcid'] = $src_id;
				}
			}

			$fields = array_keys($raw_insertdata);
			$fields[] = self::TABLE_FLD.'_srcid';

			if(count($insertdata)>0) {
				/*$sql = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				$sarra = array();
				foreach($insertdata as $i) {
					$sarra[] = "(".implode(",",$i).")";
				}
				$sql.=implode(",",$sarra);
				$this->db_insert($sql);*/
				$sql0 = "INSERT INTO ".$this->getTableName()." (".implode(",",$fields).") VALUES ";
				foreach($insertdata as $i) {
					$sql = $sql0."(".implode(",",$i).")";
					$id = $this->db_insert($sql);
					$this->logMyAction($id, "CREATE", "", ASSIST_HELPER::code(serialize($i)),array('scd_id'=>$scd_id),"CC",$this->getModRef()."/".self::CCREFTAG.$i[self::TABLE_FLD.'_srcid']);
				}

				$result = array("ok","Competencies added successfully.");
			}
			if(count($updatedata)>0) {
				$sql = "SELECT ".$this->getIDFieldName()." as id, ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." IN (".implode(",",$updatedata).")";
				$old_stat = $this->mysql_fetch_value_by_id($sql, "id", "stat");
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." IN (".implode(",",$updatedata).")";
				$this->db_update($sql);
				foreach($updatedata as $id) {
					$new = array($this->getStatusFieldName()=>self::ACTIVE);
					$old = array($this->getStatusFieldName()=>$old_stat[$id]);
					$this->logMyAction($id, "RESTORE", $old, $new,array('scd_id'=>$scd_id),"CC",$this->getModRef()."/".self::CCREFTAG.$id);
				}
				$result = array("ok","Competencies added successfully.");
			}
		}
		return $result;
	}


    public function saveWeights($var) {
    	$parent_id = $var['obj_id'];
		$weights = $var['weight'];

		$parent_ids = array('scd_id'=>$parent_id);

		$sql = "SELECT ".$this->getTableField()."_weight as w, ".$this->getIDFieldName()." as i
				, ".$this->getTableField()."_srcmodref as sm
				, ".$this->getTableField()."_srcid as si
				, ".$this->getTableField()."_srctype as st
				FROM ".$this->getTableName()."
				WHERE ".$this->getIDFieldName()." IN (".implode(",",array_keys($weights)).")";
		$old_rows = $this->mysql_fetch_all_by_id($sql, 'i');
				//return array("info",$sql,$old);

		foreach($weights as $id => $w) {
			if(strlen($w)>0 && is_numeric($w)) {
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->getTableField()."_weight = $w WHERE ".$this->getIDFieldName()." = $id";
				$this->db_update($sql);
				$old = array($this->getTableField().'_weight'=>$old_rows[$id]['w']);
				$new = array($this->getTableField().'_weight'=>$w);
				$type = $old_rows[$id]['st'];
				$ref = $old_rows[$id]['sm']."/".$this->getTypeRefTag($type).$old_rows[$id]['si'];
				$transaction = $this->code("Edited weight for |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '$w' from '".$old_rows[$id]['w']."'");
				$this->logMyAction($id, "EDIT", $old, $new, $parent_ids, $type, $ref,$transaction);
			}
		}

		return array("ok","Weights saved successfully.");

    }








    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /***********************
     * PRIVATE functions: functions only for use within the class
     */



	private function getTypeName($t) {
		switch($t) {
			case "KPI": return "KPI"; break;
			case "PROJ": return "Project"; break;
			case "CC": return "Core Competency"; break;
		}
	}
	private function getTypeRefTag($t) {
		switch($t) {
			case "KPI": return SDBP5_DEPT::REFTAG; break;
			case "PROJ": return SDBP5_CAPITAL::REFTAG; break;
			case "CC": return self::CCREFTAG; break;
		}
	}


	private function logMyAction($id,$action,$old,$new,$parent_ids,$type,$ref,$transaction_comment = "") {
		$logObj = new PM3_LOG_OBJECT();
		$object_type = $this->getTypeName($type);
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				$display = "Linked $object_type $ref to |".PM3_SCORECARD::OBJECT_TYPE."| ".PM3_SCORECARD::REFTAG.$parent_ids['scd_id']." as |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "RESTORE":
				$action = $logObj->getRestoreLogAction();
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id." [".$ref."]";
				break;
			case "DEACTIVATE":
				$action = $logObj->getDeactivateLogAction();
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id." [".$ref."]";
				break;
			case "EDIT":
				$action = $logObj->getEditLogAction();
				$display = $transaction_comment;
				break;
		}
		$logObj->addObject(
			$parent_ids,
			$this->getMyObjectType(),
			$action,
			$display,
			$old,
			$new,
			0,
			0,
			0,
			$id
		);

	}





}


?>