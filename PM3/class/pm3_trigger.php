<?php
/**
 * To manage the CONTRACT object
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class PM3_TRIGGER extends PM3 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $id_field = "_id";
	protected $status_field = "_status";
	protected $parent_field = "_asmtid";
	protected $progress_status_field = "_statusid";

	protected $type_names = array(
		'SELF'=>"Self Assessment",
		'MOD'=>"Moderator Assessment",
		'FINAL'=>"Final Review"
	);


    /*
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "TRIGGER";
    const TABLE = "triggers";
    const TABLE_FLD = "trgr";
    const REFTAG = "ST";
	const LOG_TABLE = "triggers";
	/**
	 * Type CONSTANTS
	 */
	const SELF_ASSESS = "SELF";
	const MODERATOR_ASSESS = "MOD";
	const FINAL_ASSESS = "FINAL";
	const REPORT_ASSESS = "REPORT";
	const VIEW_ASSESS = "VIEW";
	/**
	 * STATUS CONSTANTS
	 */
	const NUUT = 1;
	const IN_PROGRESS = 2;
	const COMPLETED = 3;
	const CLOSED = 4;

    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
/*		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }

	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		switch($var['trigger_type']) {
			case $this->getSelfType(): return $this->addSelfTrigger($var); break;
			case $this->getModeratorType(): return $this->addModTrigger($var); break;
			case $this->getFinalReviewType(): return $this->addFinalTrigger($var); break;
		}
	}
	private function addSelfTrigger($var) {
		$sql = "INSERT INTO ".$this->getTableName()." SET
				trgr_asmtid = ".$var['assess_id']."
				, trgr_type = '".$var['trigger_type']."'
				, trgr_tkid = '".$var['tkid']."'
				, trgr_deadline = '".date("Y-m-d",strtotime($var['self_deadline']))."'
				, trgr_statusid = 1
				, trgr_status = ".self::ACTIVE."
				, trgr_insertuser = '".$this->getUserID()."'
				, trgr_insertdate = now()
		";
		$id = $this->db_insert($sql);
		if($id>0) {
			$asmtObj = new PM3_ASSESSMENT();
			$parent_ids = $asmtObj->getRelatedIDs($var['assess_id']);
			$new = array(
				'trgr_asmtid' => $var['assess_id'],
				'trgr_type' => $var['trigger_type'],
				'trgr_tkid' => $var['tkid'],
				'trgr_deadline' => date("Y-m-d",strtotime($var['self_deadline'])),
				'trgr_statusid' => 1,
			);
			$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>isset($parent_ids['scorecard']) ? $parent_ids['scorecard'] : 0, 'prd_id'=>isset($parent_ids['period']) ? $parent_ids['period'] : 0, 'trgr_id'=>$id);
			$this->logMyAction($var['trigger_type'],$id, "CREATE", "", $new, $ivar);


			//SEND EMAIL TO EMPLOYEE
			$tkObject = new ASSIST_TK($var['tkid']);
			if($tkObject->isActiveUser()) {
				$to_email = $tkObject->getObjectUserEmail();
				$to_name = $tkObject->getObjectUserName();
				$to = array('name'=>$to_name,'email'=>$to_email);
				$subject = "Self Assessment triggered";
$message = "Good Day ".$to_name."

Your Self Assessment (".$asmtObj->getRefTag().$var['assess_id'].") for ".$this->getModTitle()." has been triggered on ".$this->getSiteName()." by ".$this->getActualUserName().".

You have until ".$var['self_deadline']." to complete this Self Assessment.

In order to complete this assessment, please login on ".$this->getSiteName().".

Kind regards
".$this->getSiteName()."
";
$message = ASSIST_HELPER::decode($message);
				$emailObject = new ASSIST_EMAIL($to, $subject, $message);
				$emailObject->sendEmail();
			}



			return array("ok","New ".$this->getObjectName($this->getMyObjectType())." created successfully.");
		}
		return array("error","An error occurred.  Please reload the page and try again.");
	}

	private function addModTrigger($var) {
		$done = array();
		$done_id = array();
		$asmtObj = new PM3_ASSESSMENT();
		$employee = $asmtObj->getEmployeeBeingAssessed($var['assess_id']);
		$parent_ids = $asmtObj->getRelatedIDs($var['assess_id']);
		foreach($var['tkid'] as $i => $tkid) {
			if(strlen($tkid)>0 && $tkid!="X" && strlen($var['mod_deadline'][$i])>0 && !in_array($tkid,$done)) {
				$sql = "INSERT INTO ".$this->getTableName()." SET
						trgr_asmtid = ".$var['assess_id']."
						, trgr_type = '".$var['trigger_type']."_".$i."'
						, trgr_tkid = '".$tkid."'
						, trgr_deadline = '".date("Y-m-d",strtotime($var['mod_deadline'][$i]))."'
						, trgr_statusid = 1
						, trgr_status = ".self::ACTIVE."
						, trgr_insertuser = '".$this->getUserID()."'
						, trgr_insertdate = now()
				";
				$id = $this->db_insert($sql);
				$done[] = $tkid;
				$done_id[] = $id;
				$new = array(
					'trgr_type' => $var['trigger_type']."_".$i,
					'trgr_tkid' => $tkid,
					'trgr_deadline' => $var['mod_deadline'][$i],
					'trgr_statusid' => 1,
					'mod_id'=>$i,
				);
				$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>isset($parent_ids['scorecard']) ? $parent_ids['scorecard'] : 0, 'prd_id'=>isset($parent_ids['period']) ? $parent_ids['period'] : 0, 'trgr_id'=>$id);
				$this->logMyAction($var['trigger_type'] ,$id, "CREATE", "", $new, $ivar);

				//SEND EMAIL TO MODERATOR
				$tkObject = new ASSIST_TK($tkid);
				if($tkObject->isActiveUser()) {
					$to_email = $tkObject->getObjectUserEmail();
					$to_name = $tkObject->getObjectUserName();
					$to = array('name'=>$to_name,'email'=>$to_email);
					$subject = "Assessment Moderation triggered";
$message = "Good Day ".$to_name."

".$this->getActualUserName()." has assigned you as a Moderator on Assessment ".$asmtObj->getRefTag().$var['assess_id']." for ".$employee['name'].".

You have until ".$var['mod_deadline'][$i]." to complete this Assessment.

In order to complete this assessment, please login on ".$this->getSiteName().".

Kind regards
".$this->getSiteName()."
";
$message = ASSIST_HELPER::decode($message);
					$emailObject = new ASSIST_EMAIL($to, $subject, $message);
					$emailObject->sendEmail();
				}

			}
		}
		if(count($done)>0) {
			//close any open self assessments

			//get older details
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE trgr_asmtid = ".$var['assess_id']." AND trgr_statusid < 3 AND trgr_type = '".$this->getSelfType()."'";
			$self = $this->mysql_fetch_one($sql);
			if(isset($self[$this->getIDFieldName()])) {
				$sql = "UPDATE ".$this->getTableName()." SET trgr_statusid = 4, trgr_datecompleted = now() WHERE trgr_asmtid = ".$var['assess_id']." AND trgr_statusid < 3 AND trgr_type = '".$this->getSelfType()."'";
				$mar = $this->db_update($sql);
				$new = array(
					'trgr_datecompleted' => date("Y-m-d"),
					'trgr_statusid' => 4,
				);
				$old = $self;
				$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>isset($parent_ids['scorecard']) ? $parent_ids['scorecard'] : 0, 'prd_id'=>isset($parent_ids['period']) ? $parent_ids['period'] : 0, 'trgr_id'=>$self[$this->getIDFieldName()]);
				$this->logMyAction($this->getSelfType(),$self[$this->getIDFieldName()], "CLOSE", $old, $new, $ivar);
			}
		}
		if($id>0) {
			return array("ok","New ".$this->getObjectName($this->getMyObjectType())." created successfully.");
		}
		return array("error","An error occurred.  Please reload the page and try again.");
	}


	private function addFinalTrigger($var) {
		$sql = "INSERT INTO ".$this->getTableName()." SET
				trgr_asmtid = ".$var['assess_id']."
				, trgr_type = '".$var['trigger_type']."'
				, trgr_tkid = '".$var['final_tkid']."'
				, trgr_deadline = '".date("Y-m-d",strtotime($var['final_deadline']))."'
				, trgr_statusid = 1
				, trgr_status = ".self::ACTIVE."
				, trgr_insertuser = '".$this->getUserID()."'
				, trgr_insertdate = now()
		";
		$id = $this->db_insert($sql);
		if($id>0) {
			$asmtObj = new PM3_ASSESSMENT();
			$employee = $asmtObj->getEmployeeBeingAssessed($var['assess_id']);
			$parent_ids = $asmtObj->getRelatedIDs($var['assess_id']);
			$new = array(
				'trgr_asmtid' => $var['assess_id'],
				'trgr_type' => $var['trigger_type'],
				'trgr_tkid' => $var['final_tkid'],
				'trgr_deadline' => date("Y-m-d",strtotime($var['final_deadline'])),
				'trgr_statusid' => 1,
				'trgr_id'=>$id,
			);
			$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>isset($parent_ids['scorecard']) ? $parent_ids['scorecard'] : 0, 'prd_id'=>isset($parent_ids['period']) ? $parent_ids['period'] : 0, 'trgr_id'=>$id);
			$this->logMyAction($var['trigger_type'] ,$id, "CREATE", "", $new, $ivar);
			//Notify user
				$tkObject = new ASSIST_TK($var['final_tkid']);
				if($tkObject->isActiveUser()) {
					$to_email = $tkObject->getObjectUserEmail();
					$to_name = $tkObject->getObjectUserName();
					$to = array('name'=>$to_name,'email'=>$to_email);
					$subject = "Assessment Final Review triggered";
$message = "Good Day ".$to_name."

".($this->getActualUserName())." has assigned you to perform a Final Review on Assessment ".$asmtObj->getRefTag().$var['assess_id']." for ".$employee['name'].".

You have until ".$var['final_deadline']." to complete this Assessment.

In order to complete this assessment, please login on ".$this->getSiteName().".

Kind regards
".$this->getSiteName()."
";
$message = ASSIST_HELPER::decode($message);
					$emailObject = new ASSIST_EMAIL($to, $subject, $message);
					$emailObject->sendEmail();
				}
			//close any open moderators
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE trgr_asmtid = ".$var['assess_id']." AND trgr_statusid < 3 AND trgr_type LIKE '".$this->getModeratorType()."%'";
			$mod = $this->mysql_fetch_all($sql);
			if(count($mod)>0) {
				$sql = "UPDATE ".$this->getTableName()." SET trgr_statusid = 4, trgr_datecompleted = now() WHERE trgr_asmtid = ".$var['assess_id']." AND trgr_statusid < 3 AND trgr_type LIKE '".$this->getModeratorType()."%'";
				$this->db_update($sql);
				foreach($mod as $m) {
					$new = array(
						'trgr_datecompleted' => date("Y-m-d"),
						'trgr_statusid' => 4,
					);
					$old = $m;
					$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>isset($parent_ids['scorecard']) ? $parent_ids['scorecard'] : 0, 'prd_id'=>isset($parent_ids['period']) ? $parent_ids['period'] : 0, 'trgr_id'=>$m[$this->getIDFieldName()]);
					$this->logMyAction($this->getModeratorType(),$m[$this->getIDFieldName()], "CLOSE", $old, $new, $ivar);
				}
			}
			return array("ok","New ".$this->getObjectName($this->getMyObjectType())." created successfully.");
		}
		return array("error","An error occurred.  Please reload the page and try again.");
	}



	public function deleteObject($var) {
		$old_sql = "SELECT * FROM ".$this->getTableName()." WHERE
				trgr_asmtid = ".$var['assess_id']."
				AND trgr_type LIKE '".$var['type']."%'";
			$old_rows = $this->mysql_fetch_all_by_id($old_sql,$this->getIDFieldName());
		$sql = "UPDATE ".$this->getTableName()." SET
				trgr_status = ".self::INACTIVE."
				WHERE
				trgr_asmtid = ".$var['assess_id']."
				AND trgr_type LIKE '".$var['type']."%'
		";
		$mar = $this->db_update($sql);
		if(count($old_rows)>0) {
			$asmtObj = new PM3_ASSESSMENT();
			if($var['type']==$this->getSelfType()) {
				$asmtObj->deleteObject($var['assess_id']);
			}
			$parent_ids = $asmtObj->getRelatedIDs($var['assess_id']);
			if(!isset($parent_ids['scorecard'])) { $parent_ids['scorecard'] = 0; }
			if(!isset($parent_ids['period'])) { $parent_ids['period'] = 0; }
			$new = array(
				'trgr_asmtid' => $var['assess_id'],
				'trgr_type' => $var['type'],
				'trgr_status' => self::INACTIVE,
			);
			foreach($old_rows as $id => $old) {
				$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>isset($parent_ids['scorecard']) ? $parent_ids['scorecard'] : 0, 'prd_id'=>isset($parent_ids['period']) ? $parent_ids['period'] : 0, 'trgr_id'=>$id);
				$this->logMyAction($var['type'],$id, "DELETE", $old, $new, $ivar);
			}
			return array("ok","".$this->getObjectName($this->getMyObjectType())." deleted successfully.");
		}
		return array("error","An error occurred.  Please reload the page and try again.",$sql,$old_rows,$old_sql);
	}


	/*
	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}


	public function activateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		$type = $var['type'];
		$ref = $var['ref'];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		//return array("error",$sql);
		if($mar>0) {
			return array("ok",$type." ".$ref." successfully removed from this assessment.");
		} else {
			return array("info",$type." ".$ref." could not be found associated with this assessment. Please reload the page and try again.");
		}

		return array("error","An error occurred while trying to deactivate ".$type." ".$ref.". Please reload the page and try again.");
	}


	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}

	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];

		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}

	*/


    /*************************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }

    public function getSelfType() { return self::SELF_ASSESS; }
    public function getModeratorType() { return self::MODERATOR_ASSESS; }
    public function getFinalReviewType() { return self::FINAL_ASSESS; }
    public function getReportType() { return self::REPORT_ASSESS; }
    public function getViewType() { return self::VIEW_ASSESS; }

	public function getTypeName($x) { return $this->type_names[$x]; }

	public function getTriggersForCompletion($type,$tkid="") {
		if(strlen($tkid)==0) { $tkid = $this->getUserID(); }
		$data = array();
		$tbl_fld = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$status_fld = $this->getStatusFieldName();
		$parent_fld = $this->getParentFieldName();
		$progress_fld = $this->getProgressStatusFieldName();

		$prdObj = new PM3_PERIOD();
		$periods = $prdObj->getAssessmentPeriodsForSelect();

		$listObj = new PM3_LIST("status");
		$statuses = $listObj->getActiveListItemsFormattedForSelect();

		$assessObj = new PM3_ASSESSMENT();
		$assess_tbl_fld = $assessObj->getTableField();
		$assess_id_fld = $assessObj->getIDFieldName();
		$assess_parent_fld = $assessObj->getParentFieldName();
		$assess_status_fld = $assessObj->getStatusFieldName();

		$scdObj = new PM3_SCORECARD();
		$scd_id_fld = $scdObj->getIDFieldName();
		$scd_status_fld = $scdObj->getStatusFieldName();
		$scd_tbl_fld = $scdObj->getTableField();

		$active_bitwise = self::ACTIVE;

		$sql = "SELECT Tr.*, Tr.".$id_fld." as child_id
		, Tr.".$parent_fld." as id
		, Tr.".$progress_fld." as progress
		, CONCAT('".$assessObj->getRefTag()."',Tr.".$parent_fld.") as ref
		, Tr.".$tbl_fld."_deadline as deadline
		, Asmt.".$assess_tbl_fld."_periodid as period_id
		, Scd.".$scd_id_fld." as parent_id
		, Scd.".$scd_tbl_fld."_tkid as employee_tkid
		, CONCAT(TK.tkname, ' ',TK.tksurname) as employee
		FROM `".$this->getTableName()."` Tr
		INNER JOIN ".$assessObj->getTableName()." Asmt
		ON Tr.".$parent_fld." = Asmt.".$assess_id_fld."
		AND (Asmt.".$assess_status_fld." & ".$active_bitwise.") = ".$active_bitwise."
		INNER JOIN ".$scdObj->getTableName()." Scd
		ON Asmt.".$assess_parent_fld." = Scd.".$scd_id_fld."
		AND (Scd.".$scd_status_fld." & ".$active_bitwise.") = ".$active_bitwise."
		INNER JOIN ".$this->getUserTableName()." TK
		ON TK.tkid = Scd.".$scd_tbl_fld."_tkid
		WHERE `".$tbl_fld."_type` LIKE '".$type."%'
		AND `".$tbl_fld."_tkid` = '$tkid'
		AND (".$status_fld." & ".$active_bitwise.") = ".$active_bitwise."";

		$rows = $this->mysql_fetch_all($sql);

		$scorecard_keys = array();

		foreach($rows as $key => $r) {
			$data[$key] = $r;
			$data[$key]['period'] = $periods[$r['period_id']];
			$data[$key]['deadline'] = date("d-M-Y",strtotime($r['deadline']));
			$data[$key]['status'] = isset($statuses[$r[$tbl_fld."_statusid"]]) ? $statuses[$r[$tbl_fld."_statusid"]] : $this->getUnspecified();
			if(($r[$progress_fld]) >2) {
				$data[$key]['can_continue'] = false;
			} else {
				$data[$key]['can_continue'] = true;
			}
			if(!in_array($r['parent_id'],$scorecard_keys)) {
				$scorecard_keys[] = $r['parent_id'];
			}
		}
		$scorecard_details = $scdObj->getObjectsSummary($scorecard_keys,false);

		return array('objects'=>$data,'details'=>$scorecard_details);
	}


	/*************
	 * Get trigger details using the parent assessment id as the WHERE
	 */
	public function getTriggersByParentID($assessment_keys) {
		$tbl_fld = $this->getTableField();
		$listObj = new PM3_LIST("trigger_status");
		$status = $listObj->getActiveListItemsFormattedForSelect();
		$sql = "SELECT *
				, ".$this->getIDFieldName()." as id
				, ".$this->getParentFieldName()." as assess_id
				, ".$tbl_fld."_statusid as statusid
				, ".$tbl_fld."_deadline as deadline
				, ".$tbl_fld."_datecompleted as datecompleted
				, CONCAT(TK.tkname,' ',TK.tksurname) as user
				FROM ".$this->getTableName()."
				INNER JOIN ".$this->getUserTableName()." as TK
				ON TK.tkid = ".$this->getTableField()."_tkid
				WHERE (trgr_status & 2) = 2 AND ";
		if((is_array($assessment_keys) && count($assessment_keys)>0) || strlen($assessment_keys)>0) {
			if(is_array($assessment_keys)) {
				$sql.= " trgr_asmtid IN (".implode(",",$assessment_keys).")";
			} else {
				$sql.= " trgr_asmtid = $assessment_keys ";
			}
		}
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		$blank_data = array(
			$this->getSelfType()=>array(),
			$this->getModeratorType()=>array(),
			$this->getFinalReviewType()=>array(),
		);
		foreach($rows as $r) {
			$i = $r['assess_id'];
			if(!isset($data[$i])) { $data[$i] = $blank_data; }
			$r['ref'] = $this->getRefTag().$r['id'];
			$r['status'] = $status[$r['statusid']];
			$r['date'] = $r['deadline'].($r['statusid']>=3 ? " / ".$r['datecompleted'] : "");
			$type = $r['trgr_type'];
			$r['type'] = $type;
			if($type!=$this->getSelfType() && $type!=$this->getFinalReviewType()) {
				$t = explode("_",$type);
				$type = $t[0];
				$mod = $t[1];
				$r['mod'] = $mod;
				$data[$i][$type][$mod] = $r;
			} else {
				$mod = 0;
				$data[$i][$type] = $r;
			}
		}
		return $data;
	}




	public function getSelfScoresByAssessmentID($assess_id) {
		$sql = "SELECT ".$this->getIDFieldName()." as id FROM ".$this->getTableName()."
				WHERE ".$this->getParentFieldName()." = $assess_id
				AND (".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE."
				AND ".$this->getTableField()."_type = '".$this->getSelfType()."'
				ORDER BY ".$this->getIDFieldName()." DESC";
		$trigger_id = $this->mysql_fetch_one_value($sql, "id");

		$scoreObj = new PM3_SCORE();
		return $scoreObj->getScoresForCompletingAssessment($trigger_id);
	}

    public function getOverallSelfScoreAverageByAssessmentID($assess_id) {
        $self_scores = $this->getSelfScoresByAssessmentID($assess_id);

        $self_scores_array = array();
        foreach($self_scores as $key => $val){
            $self_scores_array[] = $val['score'];
        }

        if(isset($self_scores_array) && is_array($self_scores_array) && count($self_scores_array) > 0){
            $average = array_sum($self_scores_array)/count($self_scores_array);
        }else{
            $average = 0;
        }

        return $average;
    }

	public function getModeratorScoresByAssessmentID($assess_id) {
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getTableField()."_type as mod_id
				FROM ".$this->getTableName()."
				WHERE ".$this->getParentFieldName()." = $assess_id
				AND (".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE."
				AND ".$this->getTableField()."_type LIKE '".$this->getModeratorType()."%'
				ORDER BY ".$this->getIDFieldName()." ASC";
		$trigger_ids = $this->mysql_fetch_value_by_id($sql, "mod_id", "id");

		$scoreObj = new PM3_SCORE();
		$scores = array();
		foreach($trigger_ids as $key => $i) {
			$s = $scoreObj->getScoresForCompletingAssessment($i);
			$x = explode("_",$key);
			$scores[$key] = array(
				'scores'=>$s,
				'id'=>$x[1]
			);
		}

		$scores['average'] = $scoreObj->getAverageModeratorsScoreByAssessmentID($assess_id);


		return $scores;
	}

    public function getOverallModerationScoreAverageByAssessmentID($assess_id) {
        $moderator_scores = $this->getModeratorScoresByAssessmentID($assess_id);

        $moderator_scores_array = array();
        foreach($moderator_scores['average'] as $key => $val){
            $moderator_scores_array[] = $val;
        }

        if(isset($moderator_scores_array) && is_array($moderator_scores_array) && count($moderator_scores_array) > 0){
            $average = array_sum($moderator_scores_array)/count($moderator_scores_array);
        }else{
            $average = 0;
        }

        return $average;
    }

    public function getFinalScoresByAssessmentID($assess_id) {
        $sql = "SELECT ".$this->getIDFieldName()." as id FROM ".$this->getTableName()."
				WHERE ".$this->getParentFieldName()." = $assess_id
				AND (".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE."
				AND ".$this->getTableField()."_type = '".$this->getFinalReviewType()."'
				ORDER BY ".$this->getIDFieldName()." DESC";
        $trigger_id = $this->mysql_fetch_one_value($sql, "id");

        if($trigger_id !== null){
            $scoreObj = new PM3_SCORE();
            return $scoreObj->getScoresForCompletingAssessment($trigger_id);
        }else{
            return array();
        }
    }

    public function getOverallFinalScoreAverageByAssessmentID($assess_id) {
        $final_scores = $this->getFinalScoresByAssessmentID($assess_id);

        $final_scores_array = array();
        foreach($final_scores as $key => $val){
            $final_scores_array[] = $val['score'];
        }

        if(isset($final_scores_array) && is_array($final_scores_array) && count($final_scores_array) > 0){
            $average = array_sum($final_scores_array)/count($final_scores_array);
        }else{
            $average = 0;
        }

        return $average;
    }


	public function getEditObjectByParentID($var) {
		$type = $var['type'];
		$assess_id = $var['assess_id'];

		$tbl_fld = $this->getTableField();

		$assessObj = new PM3_ASSESSMENT();
		$assess_tbl = $assessObj->getTableName();
		$assess_id_fld = $assessObj->getIDFieldName();
		$assess_period = $assessObj->getNameFieldName();

		$sql = "SELECT *
				FROM ".$this->getTableName()."
				INNER JOIN $assess_tbl ON $assess_id_fld = ".$this->getParentFieldName()."
				WHERE ".$tbl_fld."_type = '$type'
				AND ".$this->getParentFieldName()." = ".$assess_id."
				AND (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		if($type!=$this->getModeratorType()) {
			$sql.= " ORDER BY ".$this->getIDFieldName()." DESC LIMIT 1";
		}
		$rows = $this->mysql_fetch_all($sql);

		$result = array();
		foreach($rows as $r) {
			$x = array();
			$x['id'] = $r[$this->getIDFieldName()];
			$x['employee_tkid'] = $r[$tbl_fld.'_tkid'];
			$x['period_id'] = $r[$assess_period];
			$x['type'] = $r[$tbl_fld.'_type'];
			$x['deadline'] = $r[$tbl_fld.'_deadline'];
			$x['deadline_formatted'] = date("d-M-Y",strtotime($r['trgr_deadline']));
			$x['raw'] = $r;
			$x['raw']['ass_name'] = $assess_period;
			$result[] = $x;
		}
		if($type!=$this->getModeratorType()) {
			$result = $result[0];
		}

		return $result;
	}

/*
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}


	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
	}

	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}*/



	/***
	 * Returns an unformatted array of an object
	 */
	/*public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);

		return $data;
	}
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}
*/








	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted
	 *//*
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 *//*
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 *//*
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}



	/****
	 * functions to check on the status of a core competency
	 */





    /***
     * SET / UPDATE Functions
     */









    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /***********************
     * PRIVATE functions: functions only for use within the class
     */


	public function saveScore($var) {
		$trigger_id = $var['trigger_id'];
		$trigger_type = $var['trigger_type'];
		$assess_id = $var['assess_id'];
		$post_action = $var['post_action'];
		$result = array("error","An error occurred.  Please try again.");

		$scoreObj = new PM3_SCORE();
		$ok = $scoreObj->saveScore($var);

		if($ok || $post_action=="SUBMIT") {
			if($post_action=="SUBMIT") {
				$trgr_statusid = 3;
				$trgr_datecompleted = date("Y-m-d");
			} else {
				$trgr_statusid = 2;
				$trgr_datecompleted = "0000-00-00";
			}
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$trigger_id;
			$old = $this->mysql_fetch_one($sql);

			$sql = "UPDATE ".$this->getTableName()." SET ".$this->getProgressStatusFieldName()." = ".$trgr_statusid." , ".$this->getTableField()."_datecompleted = '$trgr_datecompleted' WHERE ".$this->getIDFieldName()." = ".$trigger_id;
			$mar = $this->db_update($sql);
			//if($mar>0) {
				$asmtObj = new PM3_ASSESSMENT();
				$parent_ids = $asmtObj->getRelatedIDs($old[$this->getParentFieldName()],$trigger_type);
				$new = array(
					'trgr_statusid' => $trgr_statusid,
					'trgr_datecompleted' => $trgr_datecompleted,
				);
				if($trigger_type==$this->getModeratorType()) {
					$x = explode("_",$old['trgr_type']);
					$new['mod_id'] = $x[1];
				}
				$ivar = array('asmt_id'=>$var['assess_id'], 'scd_id'=>$parent_ids['scorecard'], 'prd_id'=>$parent_ids['period'], 'trgr_id'=>$trigger_id);
				$this->logMyAction($trigger_type,$trigger_id, "SCORE", $old, $new, $ivar);

				//Notify creator
				if($post_action=="SUBMIT") {
					$send_email = false;
					$tkObject = new ASSIST_TK($old[$this->getTableField().'_insertuser']);
					if($tkObject->isActiveUser()) {
						$to_email = $tkObject->getObjectUserEmail();
						$to_name = $tkObject->getObjectUserName();
						$to = array('name'=>$to_name,'email'=>$to_email);
						$send_email = true;
					} else {
						$userObject = new PM3_USERACCESS();
						$trigger_users = $userObject->getTriggerUsersFormattedForSelect();
						unset($trigger_users[$old[$this->getTableField().'_insertuser']]);
						if(count($trigger_users)>0) {

						}
					}
					if($send_email) {
						$subject = $this->getTypeName($trigger_type)." ".$asmtObj->getRefTag().$var['assess_id']." Completed";
$message = "Good Day ".$to_name."

".($this->getActualUserName())." has completed ".$this->getTypeName($trigger_type)." on Assessment ".$asmtObj->getRefTag().$var['assess_id']."

In order to review this assessment, please login on ".$this->getSiteName().".

Kind regards
".$this->getSiteName()."
";
						$message = ASSIST_HELPER::decode($message);
						$emailObject = new ASSIST_EMAIL($to, $subject, $message);
						$emailObject->sendEmail();
					}//endif send email = true
					$result = array("ok","Scores saved and finalised.");
				} else {//else if submit then notify creator
					$result = array("ok","Scores saved for later finalisation.");
				}//end if submit then notify creator
			//}
		} else {
			$result = array("ok","Scores saved for later finalisation.");
			//$result = array("info","No scores found to be saved.");
		}
		return $result;
	}



	/****
	 * functions to check on the status of a core competency
	 */
	public function triggerStatusInUse() {
		$tbl_fld = $this->getTableField();
		$id_fld = $this->getIDFieldName();
		$sql = "SELECT ".$tbl_fld."_statusid as id,
				count(".$id_fld.") as cc
				FROM ".$this->getTableName()."
				GROUP BY ".$tbl_fld."_statusid";
		$data = $this->mysql_fetch_value_by_id($sql, "id", "cc");
		return $data;
	}





/*	public function getTypeName($t) {
		switch($t) {
			case $this->getSelfType(): return "Self Assessment"; break;
			case $this->getModeratorType(): return "Moderation"; break;
			case $this->getFinalReviewType(): return "Final Review"; break;
		}
	}
  */
	public function getNextType($t) {
		switch($t) {
			case $this->getSelfType(): return $this->getModeratorType(); break;
			case $this->getModeratorType(): return $this->getFinalReviewType(); break;
			case $this->getFinalReviewType(): return false; break;
		}
	}

		private function logMyAction($type,$id,$action,$old,$new,$var) {
		$logObj = new PM3_LOG_OBJECT();
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				if($type == $this->getModeratorType()) {
					$display = "Triggered ".$this->getTypeName($type)." ".$this->getRefTag().$id." (Moderator ".$new['mod_id'].") for |".PM3_ASSESSMENT::OBJECT_TYPE."| ".PM3_ASSESSMENT::REFTAG.$var['asmt_id'].".";
				} else {
					$display = "Triggered ".$this->getTypeName($type)." ".$this->getRefTag().$id." for |".PM3_ASSESSMENT::OBJECT_TYPE."| ".PM3_ASSESSMENT::REFTAG.$var['asmt_id'].".";
				}
				break;
			case "CLOSE":
				$action = $logObj->getUpdateLogAction();
				$display = "Closed ".$this->getTypeName($type)." ".$this->getRefTag().$id." for |".PM3_ASSESSMENT::OBJECT_TYPE."| ".PM3_ASSESSMENT::REFTAG.$var['asmt_id']." due to triggering of ".$this->getTypeName($this->getNextType($type)).".";
				break;
			case "SCORE":
				$action = $logObj->getUpdateLogAction();
				$display = "Updated ".$this->getTypeName($type)." ".$this->getRefTag().$id." ".($type==$this->getModeratorType() ? "(Moderator ".$new['mod_id'].")" : "")." Scores for |".PM3_ASSESSMENT::OBJECT_TYPE."| ".PM3_ASSESSMENT::REFTAG.$var['asmt_id'].".";
				if($new['trgr_statusid']==3) {
					$display.=" ".$this->getTypeName($type)." ".$this->getRefTag().$id." is now complete.";
				}
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted ".$this->getTypeName($type)." ".$this->getRefTag().$id." for |".PM3_ASSESSMENT::OBJECT_TYPE."| ".PM3_ASSESSMENT::REFTAG.$var['asmt_id'];
				break;
			/*case "RESTORE":
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DEACTIVATE":
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REASSIGN":
				$display = "Reassigned |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '".$new[$fld]."' from '".$old[$fld]."'";
				break;*/
		}
		$logObj->addObject(
			$var,
			$this->getMyObjectType(),
			$action,
			ASSIST_HELPER::code($display),
			$old,
			$new,
			0,
			0,
			$id
		);

	}

}


?>