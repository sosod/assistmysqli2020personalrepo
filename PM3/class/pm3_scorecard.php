<?php
/**
 * To manage the ASSESSMENT object
 *
 * Created on: 1 January 2016
 * Authors: Janet Currie
 *
 */

class PM3_SCORECARD extends PM3 {

	protected $object_id = 0;
	protected $object_details = array();

	protected $id_field = "_id";
	protected $status_field = "_status";


    /*
	protected $progress_status_field = "_status_id";
	protected $parent_field = "";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	*/
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "SCORECARD";
    const TABLE = "scorecards";
    const TABLE_FLD = "scd";
    const REFTAG = "SCD";
	const LOG_TABLE = "scorecards";
	/**
	 * STATUS CONSTANTS
	 */
	const CONFIRMED = 32;
	const ACTIVATED = 64;


    public function __construct($object_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;

		/*$this->sources = array(
			array('active'=>true,'modref'=>"SDP15"),
			array('active'=>true,'modref'=>"SDP15I"),
		);*/

/*
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;*/
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "";
    }

public function getObjectType() { return self::OBJECT_TYPE; }

	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		//$sql = serialize($var);
		if(isset($var['step'])) {
			$result = array("error","Start of addObject function");
			$step = $var['step'];
			switch($step) {
				case 0:
				case "0":
					$employee_id = $var['employee_id'];
					$sql = "INSERT INTO ".$this->getTableName()." SET
							".self::TABLE_FLD."_tkid = '".$employee_id."',
							".self::TABLE_FLD."_comment = '',
							".self::TABLE_FLD."_status = ".self::ACTIVE.",
							".self::TABLE_FLD."_insertuser = '".$this->getUserID()."',
							".self::TABLE_FLD."_insertdate = now()";
					$id = $this->db_insert($sql);
					$result = array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$id." creation started successfully. Please continue with the next step.",'obj_id'=>$id);
					$insert_data = array(
							self::TABLE_FLD.'_tkid' => $employee_id,
							self::TABLE_FLD.'_status' => self::ACTIVE,
					);
					//$logObj = new PM3_LOG_OBJECT();
					//$logObj->addObject(array(),$this->getMyObjectType(),$logObj->getCreateLogAction(),"Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id,"",$insert_data,$id);
					$this->logMyAction($id, "CREATE", "", $insert_data);
					break;
				default:
					$sql = "hello";
					break;
			}
		} else {
			$result = array("error","Sorry, I couldn't work out where you were in the Create process.  Please try again.");
		}
		return $result;
	}
/*
	public function confirmObject($var) {
		$obj_id = $var['object_id'];
		return array("error","An error occurred while trying to confirm ".$this->getMyObjectName()." ".self::REFTAG.$obj_id.". Please reload the page and try again.");
	}

	*/

	//Move assessment from Create (pending) to Edit
	public function activateObject($var) {
		$object_id = $var['obj_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." + ".self::ACTIVATED.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => self::ACTIVATED,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => self::ACTIVE,
					);
				$this->logMyAction($object_id, "ACTIVATE", $old_data, $insert_data);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".$object_id." successfully activated.  It is now available for triggering.");
		}
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Undo activation of scorecard to make it available to edit
	public function undoActivation($object_id) {
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".self::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => self::ACTIVE,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => self::ACTIVATED + self::ACTIVE,
					);
				$this->logMyAction($object_id, "REVERSEACTIVATE", $old_data, $insert_data);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".$object_id." activation reversed.  It is now available for editing.");
		}
		return array("error","An error occurred while trying to activate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Deactivate an assessment which has been already triggered and therefore can't be deleted
	public function deactivateObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "stat");
		$uas = $as - self::ACTIVE + self::INACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$uas.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $uas,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $as,
					);
				$this->logMyAction($object_id, "DEACTIVATE", $old_data, $insert_data);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".$object_id." successfully deactivated.");
		}
		return array("error","An error occurred while trying to deactivate ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Restore a previously DEACTIVATED assessment
	public function restoreObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "stat");
		$uas = $as + self::ACTIVE - self::INACTIVE;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$uas.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $uas,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $as,
					);
				$this->logMyAction($object_id, "RESTORE", $old_data, $insert_data);
			return array("ok",$this->getObjectName($this->getMyObjectType())." ".$object_id." successfully restored.");
		}
		return array("error","An error occurred while trying to restore ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}


	//Delete an assessment which has NOT been triggered
	public function deleteObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getStatusFieldName()." as stat FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "stat");
		$uas = $as - self::ACTIVE + self::DELETED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$uas.") WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_status' => $uas,
					);
					$old_data = array(
							self::TABLE_FLD.'_status' => $as,
					);
				$this->logMyAction($object_id, "DELETE", $old_data, $insert_data);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".$object_id." successfully deleted.");
		}
		return array("error","An error occurred while trying to delete ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}

	//Assign an assessment to a different employee
	public function reassignObject($var) {
		$object_id = $var['obj_id'];
		$sql = "SELECT ".$this->getTableField()."_tkid as tkid FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$as = $this->mysql_fetch_one_value($sql, "tkid");
		$tkid = $var['employee'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getTableField()."_tkid = '".$tkid."' WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
					$insert_data = array(
							self::TABLE_FLD.'_tkid' => $tkid,
							'name' => $this->getAUserName($tkid),
					);
					$old_data = array(
							self::TABLE_FLD.'_tkid' => $as,
							'name' => $this->getAUserName($as),
					);
				$this->logMyAction($id, "EDIT", $old_data, $insert_data, "name");
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".$object_id." successfully reassigned.");
		}
		return array("error","An error occurred while trying to reassign ".$this->getMyObjectName()." ".self::REFTAG.$object_id.". Please reload the page and try again.");
	}



	/*


	public function updateObject($var) {
		$object_id = $var['object_id'];
		return array("error","An error occurred.  Please try again.");
	}

	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];

		$result =  array("error","An error occurred.  Please try again.");
		return $result;
	}


*/

    /*************************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }

/*	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}


	public function getList($section,$options=array()) {
		return $this->getMyList(self::OBJECT_TYPE, $section,$options);
	}

	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject(self::OBJECT_TYPE, $id,$options);
	}

	public function getSimpleDetails($id=0) {
		$obj = $this->getDetailedObject(self::OBJECT_TYPE, $id,array());
		$data = array(
			'parent_id'=>0,
			'id'=>$id,
			'name' => $obj['rows'][$this->getNameFieldName()]['display'],
			'reftag' => $obj['rows'][$this->getIDFieldName()],
			'deadline' => $obj['rows'][$this->getDeadlineFieldName()]['display'],
			'responsible_person'=>$obj['rows'][$this->getManagerFieldName()],
		);
		return $data;
	}





	/**
	 * Get assessments still to be activated
	 */
	public function getPendingObjects() {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") <> ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		return $this->getSpecificObjects($where);
	}
	private function getSpecificObjects($where) {
		$sql = "SELECT O.* , O.".$this->getIDFieldName()." as obj_id,
					CONCAT(TK.tkname,' ',TK.tksurname) as employee,
					TK.tkid as employee_tkid,
					CONCAT('".$this->getFullRefTag()."', O.".$this->getIDFieldName().") as ref
				FROM ".$this->getTableName()." O
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK
				  ON TK.tkid = O.".$this->getTableField()."_tkid
				WHERE $where
				ORDER BY TK.tkname, TK.tksurname
				";
		$objects = $this->mysql_fetch_all_by_id($sql,"obj_id");
		$object_keys = array_keys($objects);
		if(count($object_keys) >0 ) {
			$lineObj = new PM3_LINE();
			$line_fld = $lineObj->getTableField();
			$line_id_fld = $lineObj->getIDFieldName();
			$line_status_fld = $lineObj->getStatusFieldName();
			$line_parent_fld = $lineObj->getParentFieldName();
			$sql = "SELECT ".$line_parent_fld."
						, ".$line_fld."_srctype
						, count(".$line_id_fld.") as lc
						, IF(".$line_fld."_srcmodref LIKE '%I', 'I', 'O') as mod_type
					FROM `".$lineObj->getTableName()."`
					WHERE ".$line_status_fld." = ".self::ACTIVE."
					AND ".$line_parent_fld." IN (".implode(",",$object_keys).")
					GROUP BY ".$line_parent_fld.", ".$line_fld."_srcmodref, ".$line_fld."_srctype
					ORDER BY ".$line_parent_fld.", ".$line_fld."_srctype
					";
			$counts = $this->mysql_fetch_all($sql);
			foreach($counts as $c) {
				$obj_id = $c[$line_parent_fld];
				$type = $c[$line_fld.'_srctype'];
				$t = $c['lc'];
				$mod_type = $c['mod_type'];
				$objects[$obj_id]['count'][$type][$mod_type] = $t;
			}
		}
		return $objects;
	}


	public function getUsersWithActiveAssessments() {
		$sql = "SELECT DISTINCT ".$this->getTableField()."_tkid as tkid
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$all = $this->mysql_fetch_all_by_value($sql,"tkid");
		return $all;
	}


	public function getObjectsForEdit() {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
		foreach($objects as $key => $obj) {
			$s = $obj[$this->getStatusFieldName()];
			if( (($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
				if(isset($assessments[$key]) && $assessments[$key]>0) {
					$objects[$key]['can_edit'] = false;
					$objects[$key]['can_reassign'] = false;
					$objects[$key]['can_delete'] = false;
					$objects[$key]['can_deactivate'] = true;
					$objects[$key]['can_restore'] = false;
					$objects[$key]['status'] = "Triggered";
				} else {
					$objects[$key]['can_edit'] = true;
					$objects[$key]['can_reassign'] = true;
					$objects[$key]['can_delete'] = true;
					$objects[$key]['can_deactivate'] = false;
					$objects[$key]['can_restore'] = false;
					$objects[$key]['status'] = "New";
				}
			} else {
				$objects[$key]['can_edit'] = false;
				$objects[$key]['can_reassign'] = false;
				$objects[$key]['can_delete'] = false;
				$objects[$key]['can_deactivate'] = false;
				$objects[$key]['can_restore'] = false;
				$objects[$key]['can_restore'] = true;
				$objects[$key]['status'] = "Inactive";
			}
		}

		return $objects;
	}


		public function getMyObjects() {
$where = "";
		$is_support_user = $this->isSupportUser();
		$userObject = new PM3_USERACCESS();
		$can_view_all = $userObject->canIViewAll();
if(!($is_support_user || $can_view_all)) {
		$where.= " O.".$this->getTableField()."_tkid = '".$this->getUserID()."' AND ";
}
		$where.= "
				 (O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
        foreach($objects as $key => $obj) {
            $s = $obj[$this->getStatusFieldName()];
            $objects[$key]['status']  = $this->getMyScorecardStatusName($s,isset($assessments[$key])?$assessments[$key]:0);
        }

		return $objects;
	}

    public function getMyScorecardStatusName($s,$assessments) {
        if( (($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
            if($assessments>0) {
                return "Triggered";
            } else {
                return  "New";
            }
        } else {
            return "Inactive";
        }
    }

	public function getObjectsForTrigger() {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
		foreach($objects as $key => $obj) {
			$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
		}

		return $objects;
	}


	public function getObjectsForReport() {
		$where = "(O.".$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if(count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
		} else {
			$assessments = array();
		}
		foreach($objects as $key => $obj) {
			$s = $obj[$this->getStatusFieldName()];
			if( (($s & self::ACTIVE) == self::ACTIVE) && (($s & self::INACTIVE) != self::INACTIVE)) {
				if(isset($assessments[$key]) && $assessments[$key]>0) {
					$objects[$key]['status'] = "Triggered";
				} else {
					$objects[$key]['status'] = "New";
				}
			} else {
				$objects[$key]['status'] = "Inactive";
			}
			$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
		}

		return $objects;
	}


	public function getAObjectSummary($obj_id,$get_assessment_details=true) {
		$where = "
			 (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."";
			$where.= " AND O.".$this->getIDFieldName()." = ".$obj_id;
		$swObject = new PM3_SCORECARD_WEIGHT();
		$weights = $swObject->getComponentWeightForAScorecard($obj_id);
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if($get_assessment_details && count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
			foreach($objects as $key => $obj) {
				$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
			}
		}
		$objects[$obj_id]['weights'] = $weights;
		return $objects[$obj_id];
	}


	public function getObjectsSummary($obj_id,$get_assessment_details=true) {
		$where = "(O.".$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (O.".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."";
		if(is_array($obj_id) && count($obj_id)>0) {
			$where.= " AND O.".$this->getIDFieldName()." IN (".implode(",",$obj_id).")";
		} elseif(!is_array($obj_id)) {
			//$where.= " AND O.".$this->getIDFieldName()." = ".$obj_id;
		}
		$objects = $this->getSpecificObjects($where);
		$object_keys = array_keys($objects);
		if($get_assessment_details && count($object_keys)>0) {
			$assessments = $this->getAssessmentDetails($object_keys);
			foreach($objects as $key => $obj) {
				$objects[$key]['assessments'] = isset($assessments[$key]) ? $assessments[$key] : 0;
			}
		}
		//$objects['ids'] = $obj_id;
		return $objects;
	}

	private function getAssessmentDetails($object_keys) {
			$asmtObj = new PM3_ASSESSMENT();
			$asmt_id_fld = $asmtObj->getIDFieldName();
			$asmt_parent_fld = $asmtObj->getParentFieldName();
			$asmt_status_fld = $asmtObj->getStatusFieldName();
			$sql = "SELECT count(".$asmt_id_fld.") as c, ".$asmt_parent_fld." as obj_id
					FROM ".$asmtObj->getTableName()."
					WHERE ".$asmt_parent_fld." IN (".implode(",",$object_keys).") AND (".$asmt_status_fld." & ".self::ACTIVE.") = ".self::ACTIVE."
					GROUP BY ".$asmt_parent_fld."";
			return $this->mysql_fetch_value_by_id($sql, "obj_id","c");
	}



	public function getEmployee($scorecard_id) {
		$row = $this->getRawObject($scorecard_id);
		$tkid = $row[$this->getTableField().'_tkid'];
		$tkObject = new ASSIST_TK($tkid);
		$name = $tkObject->getObjectUserName();
		return array('id'=>$tkid,'name'=>$name);
	}


	/***
	 * Returns an unformatted array of an object
	 */

	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);

		return $data;
	}
	  /*
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		return $data;
	}









	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
			//status = activated
		//return $this->getStatusSQL("ALL",$t,false);
		$where = "(".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVATED.") = ".self::ACTIVATED."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::INACTIVE.") <> ".self::INACTIVE."
				AND (".(strlen($t)>0?$t.".":"").$this->getStatusFieldName()." & ".self::DELETED.") <> ".self::DELETED;

		return $where;
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}



	/****
	 * functions to check on the status of a contract
	 */
	/*public function coreCompInUse() {
		$sql = "SELECT line_srcid, count(line_id) as cc FROM ".$this->getTableName()." WHERE line_srctype = 'CC' GROUP BY line_srcid";
		$data = $this->mysql_fetch_value_by_id($sql, "line_srcid", "cc");
		return $data;
	}*/





    /***
     * SET / UPDATE Functions
     */










































    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /***********************
     * PRIVATE functions: functions only for use within the class
     */



  	/************
	 * Generate a standard heading for the creation of assessments
	 */
	public function getAssessmentCreateHeading($create_step,$obj_id,$page_action="create") {
		$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN ".$this->getTableName()." A ON A.".$this->getTableField()."_tkid = TK.tkid AND A.".$this->getIDFieldName()." = ".$obj_id;
		$name = $this->mysql_fetch_one($sql);
		if($page_action=="view") {
			$str = "
			<h3>".$name['name']." (".$this->getObjectName(self::OBJECT_TYPE).": ".($this->getFullRefTag().$obj_id).")</h3>";
		} else {
			$str = "
			<h2>Step ".$create_step.": ".$this->getStep($create_step)."</h2>
			<p class=b>".$name['name']." (".$this->getObjectName(self::OBJECT_TYPE).": ".($this->getFullRefTag().$obj_id).")</p>";
		}
		return $str;
	}

	public function getAssessmentScoreHeading($obj_id) {
		$sql = "SELECT CONCAT(TK.tkname,' ',TK.tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN ".$this->getTableName()." A ON A.".$this->getTableField()."_tkid = TK.tkid AND A.".$this->getIDFieldName()." = ".$obj_id;
		$name = $this->mysql_fetch_one($sql);
		$str = "
			<h2>".$name['name']."</h2>
			<p class=b>".$this->getObjectName(self::OBJECT_TYPE).": ".($this->getFullRefTag().$obj_id)."</p>";
		return $str;
	}














	private function logMyAction($id,$action,$old,$new,$fld="") {
		$logObj = new PM3_LOG_OBJECT();
		switch($action) {
			case "CREATE":
				$action = $logObj->getCreateLogAction();
				$display = "Created |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "ACTIVATE":
				$action = $logObj->getActivateLogAction();
				$display = "Activated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REVERSEACTIVATE":
				$action = $logObj->getActivateLogAction();
				$display = "Reversed previous activation of |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DELETE":
				$action = $logObj->getDeleteLogAction();
				$display = "Deleted |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "RESTORE":
				$action = $logObj->getRestoreLogAction();
				$display = "Restored |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "DEACTIVATE":
				$action = $logObj->getDeactivateLogAction();
				$display = "Deactivated |".$this->getMyObjectType()."| ".$this->getRefTag().$id;
				break;
			case "REASSIGN":
				$action = $logObj->getEditLogAction();
				$display = "Reassigned |".$this->getMyObjectType()."| ".$this->getRefTag().$id." to '".$new[$fld]."' from '".$old[$fld]."'";
				break;
		}
		$logObj->addObject(
			array(),
			$this->getMyObjectType(),
			$action,
			ASSIST_HELPER::code($display),
			$old,
			$new,
			$id
		);

	}

    public function determineLastActivationStatusUpdateDate($scd_id){
        $logObj = new PM3_LOG_OBJECT();
        $creation = $logObj->getScorecardLogDetails($scd_id, 'C');

        $activation_dates = array();

        if(isset($creation) && is_array($creation) && count($creation) > 0){
            $activation_dates[] = date("Y-m-d H:i:s", $creation['date']);;;
        }

        $activations = $logObj->getScorecardLogDetails($scd_id, 'V');

        if(isset($creation) && is_array($creation) && count($creation) > 0){
            $activation_dates[] = date("Y-m-d H:i:s", $activations['date']);;
        }

        if(count($activation_dates) > 0){
            usort($activation_dates, array('PM4', 'date_sort'));
            krsort($activation_dates);
            $activation_date = $activation_dates[0];

        }else{
            $activation_date = 'N/A';
        }

        return $activation_date;
    }
}


?>