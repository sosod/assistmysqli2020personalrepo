<?php
require_once("inc_header.php");

$me = new PM3();
$scale = $me->getCustomBonusScaleDirectlyFromDatabase(true);


?>
<h2>Bonus Scale</h2>
<?php
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_bonus>
<table id=tbl_bonus class=list>
	<tr>
		<th>Ref</th>
		<th>Bonus %</th>
		<th>Performance Bracket*</th>
		<th>In Use?</th>
	</tr>
<?php
$last_id = 0;
foreach($scale as $key => $q) {
	$bonus = $key."%";
	//$key = $q['id'];
	$last_id = $key;
	echo "
	<tr>
		<th>$key</th>
		<td class=center>$bonus</td>
		<td>";
		if($key==0) {
			echo "0.00%<input type=hidden name=start[$key] id=start_".$key." class=start_perf key=".$key." value=0 />";
		} else {
			$js.=$displayObject->drawFormField("SMLVC",array('id'=>"start_".$key,'name'=>"start[$key]",'class'=>"start_perf",'key'=>$key),number_format($q['start'],2));
		}
		echo "
		% to ";
		$js.=$displayObject->drawFormField("LABEL",array('id'=>"end_".$key,'name'=>"end[$key]",'class'=>"end_label"),"");
		echo "</td>
		<td>";
		$js.=$displayObject->drawFormField("BOOL_BUTTON",array('id'=>"status_".$key,'name'=>"status[$key]",'extra_act'=>"processStatusChange",'button_extra'=>"key='$key'"),($q['status']/2));
		echo "
		</td>
	</tr>";
}
?>
	<tr>
		<th></th>
		<td colspan=3 class=center><button class=saveform><?php echo $helper->getActivityName("save"); ?></button></td>
	</tr>
</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"BS")); ?></td>
	</tr>
</table>

<style type="text/css" >
	.saveform {
		font-size: 90%;
	}
	.saveform-active .ui-icon { background-image: url(/library/images/ui-icons_009900_256x240.png); }
	.saveform-active {
		border: 1px solid #009900;
		color: #009900;
		background-color: #ffffff;
		background-image: url();
	}
</style>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	var first_id = 0;
	var last_id = <?php echo $last_id; ?>;
	$("#tbl_bonus td").css("vertical-align","middle");
	$(".saveform").button({ icons: { primary: "ui-icon-disk" } })
		.addClass("saveform-active")
		.hover(function() { $(this).removeClass("saveform-active"); },function() { $(this).addClass("saveform-active"); })
		.click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_bonus]")); console.log(dta);
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=SetupBonus.Update",dta);
			console.log(result);
			result = result['result_message'];
			if(result[0]=="ok") {
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"setup_defaults.php");
				//document.location.href = "setup_defaults_bonus.php?r[]=ok&r[]="+result[1];
			//} else if(result[0]=="okerror") {
				//document.location.href = "setup_defaults_preferences.php?r[]=error&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		});
	$(".start_perf").blur(function() {
		//.prev(label) didn't work so resorted to using the key attr
		var key = $(this).attr("key")*1;
		var last_id = $(".start_perf:enabled:last").attr("key")*1;
		if(key==first_id) {
			//do nothing for the first one
		} else {
			var prev_key = key-1;
			var $prev_label = $("#end_"+prev_key);
			var $prev_text = $("#start_"+prev_key);
			var prev_disabled = $prev_text.attr("disabled");
			var is_disabled = (prev_disabled=="disabled" || prev_disabled==true);
			while(is_disabled==true) {
				prev_key--;
				$prev_label = $("#end_"+prev_key);
				$prev_text = $("#start_"+prev_key);
				prev_disabled = $prev_text.attr("disabled");
				is_disabled = (prev_disabled=="disabled" || prev_disabled==true);
			}
			if(prev_key==last_id) {
				$prev_label.html("onwards");
			} else {
				
				$prev_label.html((($(this).val()*1)-0.01).toFixed(2)+"%");
			}
		}
	});
	$(".start_perf").trigger("blur");
	$("#end_"+last_id).html("onwards");

	//replace "in use" buttons with "required" for 0% bonus - cannot be disabled
	$("#status_"+first_id+"_yes").parent().html("Required");
	//function to act on changes to "in use" option
	function processStatusChange($btn,act) {
		var key = $btn.attr("key");
		if(act=="no") {
			$("#start_"+key).prop("disabled","disabled").parent().parent().find("td").css("color","#ababab");
			$("#end_"+key).prop("disabled","disabled");
		} else {
			$("#start_"+key).prop("disabled","").parent().parent().find("td").css("color","");
		}
		$(".start_perf").trigger("blur");
	}

	$(".btn_no").each(function() {
		if($(this).attr("button_status")=="active") {
			processStatusChange($(this),"no");
		}
	});
});
</script>