<?php
require_once("inc_header.php");
$create_step = 5;
$create_type = "CC";
$create_name = "Core Competency";
$create_names = "Core Competencies";

$lineObj = new PM3_LINE();
$lines = $lineObj->getLineSrcIDs($_REQUEST['obj_id'], $lineObj->getModRef(),$create_type);

//ASSIST_HELPER::arrPrint($lines);

$listObj = new PM3_LIST("competencies");
//$kpas = $sdbp5Obj->getKPAs($assessObj->getPrimaryKPASource());
//$head = $sdbp5Obj->getHeadings($assessObj->getPrimaryKPASource(),true,false);
$objects = $listObj->getListItems();

$scdObj = new PM3_SCORECARD();
echo $scdObj->getAssessmentCreateHeading($create_step,$_REQUEST['obj_id']);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<form name=frm_cc>
	<input type=hidden name=obj_id value="<?php echo $_REQUEST['obj_id']; ?>" />
<table class=list id=tbl_list>
	<?php
	echo "
	<tr>
		<th></th>
		<th>Ref</th>
		<th>Core Competency</th>
		<th>Description</th>
		".(count($lines)>0 ? "<th></th>" : "")."
	</tr>";
	foreach($objects as $i => $obj) {
		$check = "";
		if(isset($lines[$i])) {
			$check = ASSIST_HELPER::getDisplayIconAsDiv("ok");
			$butt = "<button class=btn_delete key=".$lines[$i].">Delete</button>";
		}else{
			if(count($lines)==0) { $chkb = "checked"; } else { $chkb = ""; }
			$check = "<input type=checkbox name=cc[] value=".$i." $chkb />";
			$butt = "";
		}
		echo "
		<tr>
			<td class=center>$check</td>
			<td>".$obj['ref']."</td>
			<td>".$obj['name']."</td>
			<td>".$obj['description']."</td>
			".(count($lines)>0 ? "<td>$butt</td>" : "")."
		</tr>";
	}
	?>
</table>
</form>
<?php
?>


<p class=center><button id=btn_back>Back</button> &nbsp;&nbsp;&nbsp; <button id=btn_next>Save & Next</button></p>





<?php






//ASSIST_HELPER::arrPrint($_REQUEST);


$displayObject->echoAssessmentCreationStatus($create_step,$page_action,$_REQUEST['obj_id']);

?>
<script type="text/javascript">
$(function() {
	var scr = AssistHelper.getWindowSize();
	var dlgWidth = scr.width*0.95;
	var dlgHeight = (scr.height*0.95);
	var ifrWidth = dlgWidth*0.97;
	var ifrHeight = (dlgHeight-50)*0.97;
	
	//console.log($("input[type=checkbox]:checked").length);
	
	$("table.noborder, table.noborder td").css("border","0px");
	

	$("#btn_next").button({
		icons: {primary:"ui-icon-disk",secondary: "ui-icon-arrowthick-1-e"},
	}).click(function() {
		AssistHelper.processing();
		if($("input:checkbox").length>0) {
			if($("input[type=checkbox]:checked").length>0) {
				var dta = AssistForm.serialize($("form[name=frm_cc]"));
				var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.addCC",dta);
				if(result[0]=="ok") {
					var url = "assessment_<?php echo $page_action; ?>_step<?php echo ($create_step+1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			} else if($("button.btn_delete").length>0) {
				moveOn();
			} else {
				if(confirm("Are you sure you wish to move on without adding any <?php echo $create_names; ?>?")==true) {
					moveOn();
				} 
			}
		} else {
			moveOn();
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	});

	$("#btn_back").button({
		icons: {primary: "ui-icon-arrowthick-1-w"},
	}).click(function() {
		document.location.href = 'assessment_<?php echo $page_action; ?>_step<?php echo ($create_step-1); ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>';
	}).removeClass("ui-state-default").addClass("ui-button-state-error").css({"color":"#CC0001","border":"1px solid #cc0001"
	});

	$(".btn_delete").click(function(e) {
		e.preventDefault();
		var ref = ($(this).parent().parent().find("td:eq(1)").html());
		var key = $(this).attr("key");
		if(confirm("Are you sure you want to remove <?php echo $create_name; ?> "+ref+"?")==true) {
			AssistHelper.processing();
			var dta = "type=<?php echo $create_type; ?>&object_id="+key+"&ref="+ref;
			var result = AssistHelper.doAjax("inc_controller_assessment.php?action=Lines.Deactivate",dta);
			if(result[0]=="ok") {
				var url = "assessment_<?php echo $page_action; ?>_step<?php echo $create_step; ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	}).children(".ui-button-text").css({"padding-top":"4px","padding-bottom":"4px","padding-left":"25px","font-size":"80%"	
	});
});
function moveOn() {
	document.location.href = "assessment_<?php echo $page_action; ?>_step<?php echo $create_step+1; ?>.php?obj_id=<?php echo $_REQUEST['obj_id']; ?>";
}
</script>