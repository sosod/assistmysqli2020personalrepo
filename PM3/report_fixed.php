<?php
$page_section = "report";
require_once("inc_header.php");

$me = new PM3_REPORT();

if(isset($_REQUEST['i'])) {
	$report_id = $_REQUEST['i'];
	$me->setReportID($_REQUEST['i']);
	//$me->arrPrint($_REQUEST);
	echo "<h2>".$me->getReportTitle()."</h2>";

    if($report_id=="dashboard") {
        $period_id = $_REQUEST['filter']['period_id'];
        $assessObj = new PM3_ASSESSMENT();
        $assessments = $assessObj->getAssessmentByPeriod($period_id);
        $page_action = PM3_TRIGGER::REPORT_ASSESS;
        $display_kpa=false;
        $display_cc=false;
        $display_bonus = false;
        $display_js = false;
        $user_ids = false;

        foreach($assessments as $as) {
            echo "<h2>".$as['employee']."</h2>";
            $assess_id = $as[$assessObj->getIDFieldName()];


            include("common/scoresheet.php");
        }
        $me->arrPrint($assessments);
    }elseif($report_id=="perf_status"){
        include("report_fixed_perf_status.php");
    }elseif($report_id=="performance_by_score"){
        include("report_fixed_performance_by_score.php");
    }elseif($report_id=="bell_curve"){
        include("report_fixed_bell_curve.php");
    }




    ?>
<?php } else { ?>
    <?php
        $apObj = new PM3_PERIOD();
        $periods = $apObj->getAssessmentPeriodsForSelect();

        $available_periods = count($periods)>0?true:false;
        $reports = $me->getListOfReports();
        //$me->arrPrint($reports);
    ?>
	<table class=list>
		<tr>
			<th>Report</th>
			<th>Description</th>
			<th>Filter</th>
			<th></th>
		</tr>
        <?php foreach($reports as $i => $r) { ?>
        <tr>
            <td class=b><?php echo $r['title'] ?></td>
            <td><?php echo $r['description'] ?></td>
            <td>
                <?php if(!isset($r['filter']) || (isset($r['filter']) && $r['filter']!==false)) { ?>
                    <?php $selected = false; ?>
                    <select id=filter_<?php echo $i ?> f_type=period_id>
                        <?php foreach($periods as $pi => $p) { ?>
                            <option <?php echo (!$selected?"selected":"") ?> value="<?php echo $pi ?>"><?php echo $p ?></option>
                            <?php $selected = true; ?>
                        <?php } ?>
                    </select>
                <?php } else { ?>
                    <?php echo "N/A" ?>
                <?php } ?>
            </td>
            <td><button class='btn_generate size_button' id="<?php echo $i ?>">Generate</button></td>
        </tr>
        <?php } ?>
	</table>

	<script type=text/javascript>
		$(".btn_generate").button({
			icons: {primary: "ui-icon-script"},
		}).click(function() {
			AssistHelper.processing();
			var i = $(this).prop("id");
			var f = $("#filter_"+i).val();
			var ft = $("#filter_"+i).attr("f_type");
			document.location.href = 'report_fixed.php?i='+i+'&filter['+ft+']='+f;
		});
		resizeButtons($(".btn_generate"));
	</script>
	<?php
}
?>