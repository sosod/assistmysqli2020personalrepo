<?php
require_once("inc_header.php");

$head_name = $headingObject->getAHeadingNameByField("contract_owner_id");
$sub_name = $helper->getTerm('SUB');

$deptObject = new PM3_CONTRACT_OWNER();
$dept_list = $deptObject->getActiveMasterDepartmentsList();
$admins_list = $deptObject->getAllActiveAdmins();
$displayObject = new PM3_DISPLAY();
//ASSIST_HELPER::arrPrint($dept_list);

/**********************************
 * format data for the add/edit dialog forms
 */
$list_for_select = array(
	'dept'=>array(),
	'admin'=>array(),
);
foreach($dept_list as $key=>$row){
	$list_for_select['dept'][$key] = $row['name'];
}
$userObject = new PM3_USERACCESS();
$a = $userObject->getActiveUsers();
foreach($a as $key=>$row){
	$list_for_select['admin'][$key] = $row['name'];
}
/**
 * 
 ********************************************/

echo ASSIST_HELPER::getFloatingDisplay(array("info","To add a new ".$sub_name." please contact your Assist Administrator."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$parent_name = "";

?>
<table class=tbl-container><tr><td>
	<table>
		<tr>
			<th><?php echo $sub_name; ?></th>
			<th><?php echo $head_name; ?></th>
		</tr>
	<?php 
	foreach($dept_list as $key => $dept) {
		if($dept['parent']!=$parent_name) {
			$parent_name = $dept['parent'];
			echo "
			<tr>
				<th class='thsub left' colspan=3>".$parent_name."</th>
			</tr>";
		}
		?>
		<tr>
			<td class=b><?php echo $dept['child']; ?></td>
			<td style='padding: 0px;'><table width=100% class=sub_table style='margin: 0px;'>
				<tr>
					<th class=th2>Administrator</th>
					<th class=th2 width=50px>Create</th>
					<th class=th2 width=50px>Update</th>
					<th class=th2 width=50px>Edit</th>
					<th class=th2 width=50px>Approve</th>
					<th class=th2></th>
				</tr>
<?php
//ASSIST_HELPER::arrPrint($admins_list);
if(isset($admins_list[$key])) {
	foreach($admins_list[$key] as $tkey => $admin) {
		echo "
		<tr sect='".$key."' usr='".$admin['tkid']."'>
			<td>".$admin['name']."</td>
			<td class=center>";
				ASSIST_HELPER::displayIconAsDiv(($admin['status'] & PM3_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE)==PM3_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE); 
			echo "</td>
						<td class=center>";
				ASSIST_HELPER::displayIconAsDiv(($admin['status'] & PM3_CONTRACT_OWNER::CAN_UPDATE)==PM3_CONTRACT_OWNER::CAN_UPDATE); 
			echo "</td>
			<td class=center>";
				ASSIST_HELPER::displayIconAsDiv(($admin['status'] & PM3_CONTRACT_OWNER::CAN_EDIT)==PM3_CONTRACT_OWNER::CAN_EDIT);
			echo "</td>
			<td class=center>";
				ASSIST_HELPER::displayIconAsDiv(($admin['status'] & PM3_CONTRACT_OWNER::CAN_APPROVE)==PM3_CONTRACT_OWNER::CAN_APPROVE); 
			echo "</td>
			<td><input type=button class='btn_edit' value=Edit /></td>
		</tr>";
	}
}
?>
				<tr>
					<td colspan=5 class=right></td>
					<td><input type=button value=Add class=btn_add dept_id='<?php echo $key; ?>' dept='<?php echo $dept['name']; ?>'  /></td>
				</tr>
			</table></td>
		</tr>
	<?php	
	}
	
	?>
	</table>
</td></tr><tr><td>
	<?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"OWNER")); ?>
</td></tr>
</table>
<div id=dlg_add title="Add">
	<h2>Add New <?php echo $head_name; ?></h2>
	<form name=frm_add>
		<input type=hidden name=owner_subid id=dept_id value=0 />
		<table id=tbl_add class=form style='margin-left: 5px; margin-right: 20px'>
			<tr>
				<th width=150px><?php echo $sub_name; ?>:</th>
				<td id=td_add_dept></td>
			</tr>
			<tr>
				<th><?php echo $head_name; ?>:</th>
				<td><?php $js.= $displayObject->drawFormField("LIST",array('id'=>"add_tkid",'name'=>"owner_tkid",'options'=>$list_for_select['admin']),"X");?></td>
			</tr>
			<tr>
				<th>Create Deliverable Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_create",'name'=>"create",'yes'=>PM3_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE,'no'=>0),PM3_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE); ?></td>
			</tr>
			<tr>
				<th>Update Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_update",'name'=>"update",'yes'=>PM3_CONTRACT_OWNER::CAN_UPDATE,'no'=>0),PM3_CONTRACT_OWNER::CAN_UPDATE); ?></td>
			</tr>
			<tr>
				<th>Edit Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_edit",'name'=>"edit",'yes'=>PM3_CONTRACT_OWNER::CAN_EDIT,'no'=>0),PM3_CONTRACT_OWNER::CAN_EDIT); ?></td>
			</tr>
			<tr>
				<th>Approve Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_approve",'name'=>"approve",'yes'=>PM3_CONTRACT_OWNER::CAN_APPROVE,'no'=>0),PM3_CONTRACT_OWNER::CAN_APPROVE); ?></td>
			</tr>
		</table>
	</form>
</div>
<div id=dlg_edit title="Edit">
	<h2>Edit <?php echo $head_name; ?></h2>
	<form name=frm_edit>
		<input type=hidden name=owner_subid id=dept_id value=0 />
		<table id=tbl_add class=form style='margin-left: 5px; margin-right: 20px'>
			<tr>
				<th width=150px><?php echo $sub_name; ?>:</th>
				<td id=td_add_dept></td>
			</tr>
			<tr>
				<th><?php echo $head_name; ?>:</th>
				<td><?php $js.= $displayObject->drawFormField("LIST",array('id'=>"add_tkid",'name'=>"owner_tkid",'options'=>$list_for_select['admin']),"X");?></td>
			</tr>
			<tr>
				<th>Update Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_update",'name'=>"update",'yes'=>PM3_CONTRACT_OWNER::CAN_UPDATE,'no'=>0),PM3_CONTRACT_OWNER::CAN_UPDATE); ?></td>
			</tr>
			<tr>
				<th>Edit Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_edit",'name'=>"edit",'yes'=>PM3_CONTRACT_OWNER::CAN_EDIT,'no'=>0),PM3_CONTRACT_OWNER::CAN_EDIT); ?></td>
			</tr>
			<tr>
				<th>Approve Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_approve",'name'=>"approve",'yes'=>PM3_CONTRACT_OWNER::CAN_APPROVE,'no'=>0),PM3_CONTRACT_OWNER::CAN_APPROVE); ?></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	$("#dlg_add").dialog({
		autoOpen: false,
		modal: true,
		width: "auto",
		buttons: [{
			text:"Save New <?php echo $head_name; ?>",
			click:function() {
				AssistHelper.processing();
				if($("#add_tkid").val()=="X") {
					AssistHelper.finishedProcessing("error","Please select a user.");
				} else {
					var dta = AssistForm.serialize($("form[name=frm_add]"));
					//alert(dta);
					var result = AssistHelper.doAjax("inc_controller.php?action=ContractOwner.Add",dta);
					//console.log(result);
					if(result[0]=="kk") {
		//AssistHelper.finishedProcessing(result[0],result[1]);
						document.location.href = "setup_defaults_admins.php?r[]=ok&r[]="+result[1];
					} else {
						//AssistHelper.closeProcessing();
						AssistHelper.finishedProcessing(result[0],result[1]);
						//alert(result[1]);
					}
				}
			}
		},{
			text:"Cancel",
			click: function() {
				$(this).dialog("close");
			}
		}]
	});
	AssistHelper.hideDialogTitlebar("id","dlg_add");
    AssistHelper.formatDialogButtons($("#dlg_add"),0,AssistHelper.getDialogSaveCSS());
    AssistHelper.formatDialogButtons($("#dlg_add"),1,AssistHelper.getCloseCSS());

	$(".btn_add").click(function() {
		$("#dlg_add #add_tkid option").each(function(){
			$(this).prop("hidden",false);
		});
		var d = $(this).attr("dept_id");
		var n = $(this).attr("dept");
		$("#dlg_add #dept_id").val(d);
		$("#dlg_add #td_add_dept").html(n);
		$("#dlg_add #add_tkid").val("X");
		//Check if user is already an admin - exclude them from the select
		$usrs = $("#dlg_add #add_tkid");
		var existing = [];
		
		$("tr [sect='"+d+"']").each(function(){
			existing.push($(this).attr("usr"));
		});
		//console.log(existing);
		$("#dlg_add #add_tkid option").each(function(){
			var val = $(this).val();
			if(existing.indexOf(val) !== -1){
				$(this).prop("hidden",true);
			}
		});
		$("#dlg_add #add_update_yes").trigger("click");
		$("#dlg_add #add_edit_yes").trigger("click");
		$("#dlg_add #add_approve_yes").trigger("click");
		$("#dlg_add").dialog("open");
	});
	
	
	$("#dlg_edit").dialog({
		autoOpen: false,
		modal: true,
		width: "auto",
		buttons: [{
			text:"Save Changes",
			click:function() {
				AssistHelper.processing();
				var dta = AssistForm.serialize($("form[name=frm_edit]"));
				//alert(dta);
				var result = AssistHelper.doAjax("inc_controller.php?action=ContractOwner.Edit",dta);
				if(result[0]=="kk") {
					document.location.href = "setup_defaults_admins.php?r[]=ok&r[]="+result[1];
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		},{
			text:"Cancel",
			click: function() {
				$(this).dialog("close");
			}
		}]
	});
	AssistHelper.hideDialogTitlebar("id","dlg_add");
    AssistHelper.formatDialogButtons($("#dlg_add"),0,AssistHelper.getDialogSaveCSS());
    AssistHelper.formatDialogButtons($("#dlg_add"),1,AssistHelper.getCloseCSS());
	
	
	
	$(".btn_edit").click(function() {
		var i = $(this).attr("owner_id");
		var row = AssistHelper.doAjax("inc_controller.php?action=ContractOwner.Get","id="+i);
		$("#dlg_edit #owner_id").val(i);
		//$("#dlg_add #td_add_dept").html(n);
		//$("#dlg_add #add_tkid").val("X");
		//$("#dlg_add #add_update").trigger("click");
		//$("#dlg_add #add_edit").trigger("click");
		//$("#dlg_add #add_approve").trigger("click");
		$("#dlg_edit").dialog("open");
	});
		
	
});
</script>