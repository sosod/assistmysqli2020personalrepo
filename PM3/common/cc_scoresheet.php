<div id=div_level style='background-color:#FFFFFF; overflow:auto; margin:0px;width:100px'>
<?php
$text_placeholder = "";//"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin, tellus nec pretium molestie, diam augue tristique est, ut imperdiet risus metus non metus. Maecenas sem nibh, viverra ac dictum eget, hendrerit id neque. Donec ligula risus, vulputate nec commodo et, pellentesque et sapien. Donec varius, leo eget dapibus aliquet, lacus diam tincidunt velit, ac consectetur felis odio eget dui. Pellentesque vitae feugiat purus, a vehicula leo. Ut volutpat viverra tellus non venenatis. Proin in porta nisl. Nullam urna diam, tincidunt sit amet molestie non, scelerisque sed nunc.";
$js = isset($js) ? $js : "";
$cc_id = $_REQUEST['cc_id'];
$trigger = $_REQUEST['trigger'];
$line = $_REQUEST['line'];

$listObj = new PM3_LIST("competencies");
$object = $listObj->getAListItem($cc_id);
$listObj->changeListType("proficiencies");
$profs = $listObj->getListItems();

$ccsObj = new PM3_CC_SCORE();
$scores = $ccsObj->getScores($trigger,$line);


echo "
<h1>".$object['name']."</h1>
<p>".$object['description']."</p>
<input type=hidden name=cc_id id=cc_id value='".$cc_id."' />
<input type=hidden name=trigger id=trigger value='".$trigger."' />
<input type=hidden name=line id=line value='".$line."' />
";


//If there are no proficiencies then return an error
if(count($profs)==0) {
	ASSIST_HELPER::displayResult(array("error","Error: No Proficiencies are available.  Please create Proficiencies within the module's Setup function before continuing."));
	die();
}	//end if check if any proficiencies exist - process will DIE is error found.

$prof_ids = array_keys($profs);
$levelObj = new PM3_LEVEL();
$levels = $levelObj->getList($cc_id,$prof_ids);

//If there are no competency/proficiency levels then return an error
if(count($levels)==0) {
	ASSIST_HELPER::displayResult(array("error","Error: No Competency-Proficiency Achievement Levels are available.  Please create Levels within the module's Setup function before continuing."));
	die();
} else {
	foreach($profs as $p_id => $pl) {
		if(!isset($levels[$cc_id][$p_id]) || count($levels[$cc_id][$p_id])==0) {
			ASSIST_HELPER::displayResult(array("error","Error: Competency-Proficiency Achievement Levels are incomplete. All Competency / Proficiency combinations must have Achievement Levels.  Please create Levels within the module's Setup function before continuing."));
			die();
		}
	}
}

$score = 0;

//Check that levelObj->getListItems didn't return an error
if(!isset($levels[0]) || $levels[0]===true) {
	unset($levels[0]);	//remove 0 element just in case it is returned
	
	foreach($profs as $p_id => $p) {
		if($p['is_highest']==true) { $score = $p['score_achieved']; }
		echo "
		<div id=div_".$p_id." class='prof_div' style='' 
			is_highest='".$p['is_highest']."'
			is_lowest='".$p['is_lowest']."'
			score_achieved='".$p['score_achieved']."'
			score_failed='".$p['score_failed']."'
			>
			<h3>".$p['name']."</h3>
			<table id=tbl_".$p_id.">
				<tr>
					<td class='b center'>Achievement Level</td>
					<td class='b center'>Competent?</td>
				</tr>
			";
			foreach($levels[$cc_id][$p_id] as $l_id => $l) {
				if(isset($scores[$l_id])) {
					$val = $scores[$l_id];
				} else {
					$val = "";
				}
				
				$butt = $displayObject->createFormField("BOOL_BUTTON",array('id'=>"bool_".$cc_id."_".$p_id."_".$l_id,'extra_act'=>"checkLevelScore"),$val);
				$js.=$butt['js'];
				echo "
				<tr>
					<td>".$l." $text_placeholder <span style='font-size:75%' class=i>(".$levelObj->getRefTag().$l_id.")</span></td>
					<td class=center p_id=".$p_id." level_id=".$l_id.">".$butt['display']."<input type=hidden size=5 class=extra_status_check id=bool_".$cc_id."_".$p_id."_".$l_id."_status name=bool_".$cc_id."_".$p_id."_".$l_id."_status value='$val' /></td>
				</tr>";
			}
		echo "
				<tr>
					<td colspan=2 p_id=".$p_id.">
						".($p['is_lowest']!=true ? "<button class=btn_down_level>Down 1 Level</button>" : "")."
						".($p['is_highest']!=true ? "<button class='btn_up_level float'>Up 1 Level</button>" : "")."
					</td>
				</tr>
			</table>
			
		</div>";
	} //end foreach profs
	
}

//ASSIST_HELPER::arrPrint($profs);
//ASSIST_HELPER::arrPrint($levels);





?>
</div>
<div id=div_score style='  vertical-align: middle; position:absolute;top:0px;right:2px;width:120px; text-align: center;' >
<p class=b style='font-size: 150%'>Score:</p>
<table id=tbl_display_score style='margin: 0 auto;background-color: #EEEEFF;'>
	<tr>
		<td id=td_display_score width=75px height=75px style='padding: 5px; text-align: center; vertical-align: middle; font-size: 125%;' class='b blue-border'>In Progress</td>
	</tr>
</table>
<p style='font-size: 90%' class=i id=p_instruction>Please complete the form to the left to determine the score.</p>
<table id=tbl_save_btn style='background-color: #FFFFFF; position:absolute; bottom: 10px;  ' class=blue-border>
	<tr>
		<td width=100px height=40px style='padding: 5px; text-align: center; vertical-align: middle; background-color: #ffffff;' class='b blue-border'>
			
			<button id=btn_accept>Accept</button>
			<span id=spn_btn_ip_save><br /><span  style='font-size:75%;'>Last&nbsp;saved:<br /><label id=lbl_save for=btn_ip_save class=i style='font-weight:normal'>Never</label></span><br /><button id=btn_ip_save>Save Progress</button></span>
			<span id=spn_btn_close><button id=btn_close>Save & Close</button></span>
		</td>
	</tr>
</table>

</div>
<script type="text/javascript" >
	var ip_instruction = $("#p_instruction").html();
	var finished_instruction = "Scoring process complete.";
	var ip_score = $("#td_display_score").html();
	var score_achieved = <?php echo $score; ?>;
	var have_score = false;
$(function() {
	<?php echo $js; ?>
	//Hide all levels except the first one
	$("div.prof_div").each(function() {
		var is_highest = $(this).attr("is_highest");
		if(is_highest==0) {
			$(this).hide();
		}
	});
	//Hide the Accept button
	$("#btn_accept").hide();
	
	//Resize the elements so that the scoring bar is on the right and the levels on the left
	var scr = AssistHelper.getWindowSize();
	var div_score_width = parseInt(AssistString.substr($("#div_score").css("width"),0,-2));
	var available_width = parseInt(AssistString.substr($("#div_score").parent().css("width"),0,-2));
	var div_level_width = scr.width - div_score_width - 10;
	$("#div_level").css("width",div_level_width+"px");
	$("#div_level").css("height",scr.height+"px");
	$("#div_score").css("height",scr.height+"px");
	
	//remove the border from the buttons box in the scoring bar
	$("#tbl_save_btn").removeClass("blue-border").addClass("noborder").find("td").removeClass("blue-border").addClass("noborder");
	
	//Format the buttons
	$(".btn_up_level").button({
		icons:{primary:"ui-icon-arrowthick-1-n"}
	}).click(function(e) {
		e.preventDefault();
		moveBetweenLevels($(this),"up");
	}).removeClass("ui-state-default").addClass("ui-button-state-grey");
	
	$(".btn_down_level").button({
		icons:{primary:"ui-icon-arrowthick-1-s"}
	}).click(function(e) {
		e.preventDefault();
		moveBetweenLevels($(this),"down");
	}).removeClass("ui-state-default").addClass("ui-button-state-grey");
	
	$("#btn_accept").button({
		icons:{primary:"ui-icon-check"}
	}).click(function(e) {
		e.preventDefault();
		//Save progress
		saveProgress();
		//Close parent dialog
		closeWindow();
	}).removeClass("ui-state-default").addClass("ui-state-ok");
	
	$("#btn_ip_save").button({
		icons:{primary:"ui-icon-disk"}
	}).click(function(e) {
		e.preventDefault();
		//Save progress
		saveProgress();
	}).removeClass("ui-state-default").addClass("ui-state-ok");

	$("#btn_close").button({
		icons:{primary:"ui-icon-closethick"}
	}).click(function(e) {
		e.preventDefault();
		//Save progress
		saveProgress();
		//Close parent dialog
		closeWindow();
	});
	
	$("button").css("margin-top","5px");
	
});

//Save progress
function saveProgress() {
	$(function() {
		$("input:hidden").each(function() {
			//look for input:hidden elements associated with bool buttons - ignore "extra_status_check" class which is added to input:hidden elements used to confirm if save is necessary
			if(AssistString.substr($(this).prop("id"),0,4)=="bool" && !$(this).hasClass("extra_status_check")) {
				var v = $(this).val();
				var i = $(this).prop("id");
				
				if(v.length>0) {
					v = parseInt(v);
					//if input:hidden value = 1 = yes
					if(v==1) {
						var bi = i+"_yes";
						var act = "yes";
					//if input:hidden value = 0 = no
					} else if(v==0) {
						var bi = i+"_no";
						var act = "no";
					//otherwise return an error (false) to prevent further activity
					} else {
						var bi = false;
						var act = "";
					}
					//if no error then check if save is required
					if(bi!==false && act.length>0) {
						var $btn = $("#"+bi);
						//var fld = getRowID($btn); //-not required - it returns the same value as the input:hidden.prop('id')
						var status_check = $("#"+i+"_status").val();
						//Check if a save action is really necessary to prevent unnecessary database activity
						var do_save = false;
						if(status_check.length>0) {
							//an error occurred on a previous save attempt, so retry
							do_save = true;
						} else if(status_check.length==0) {
							//blank status_check value but input:hidden check says that there is a value, so save
							do_save = true; 
						} else if(isNaN(parseInt(status_check))) {
							//check for int failed, so retry save
							do_save = true; 
						} else if(parseInt(status_check)!=v) {
							//value of input:hidden does not match value of status_check, so save
							do_save = true;
						}
						if(do_save) {
							saveButtonStatus($btn,act,i);
						}
					}
				}
			}
			setLastSaved();
		});
		return true;
	});
}

//display the "Last saved" time
function setLastSaved() {
	var n = new Date();
	var t = n.toLocaleDateString()+" "+(n.getHours()<10?"0":"")+n.getHours()+":"+(n.getMinutes()<10?"0":"")+n.getMinutes()+":"+(n.getSeconds()<10?"0":"")+n.getSeconds();
	$("#lbl_save").text(t);
}

//Save a single button
function saveButtonStatus($btn,act,fld) {
	var trigger = $("#trigger").val();
	var line = $("#line").val();
	var level = $btn.parents("td").attr("level_id");
	var value = act=="yes" ? 1 : 0;
	
	var dta = "trigger="+trigger+"&line="+line+"&level="+level+"&value="+value; 
	var r = AssistHelper.doAjax("inc_controller_assessment.php?action=CC_SCORE.ADD",dta);
	//set the status_check field so that the save progress function knows whether or not to save this field
	if(r[0]=="ok") {
		$("#"+fld+"_status").val(value);
		return true;
	} else {
		$("#"+fld+"_status").val(r[0]);
		return false;
	}
}

//Close the dialog pop-up
function closeWindow() {
	$(function() {
		//Get CC_ID
		var i = $("#cc_id").val();
		//Get current score (## or IP)
		if(have_score == true) {
			var final_score = $("#td_display_score").html();
			//Send current_score to hidden field in parent window - cc_score_$i
			window.parent.$("#cc_score_"+i).val(final_score);
		} else {
			var final_score = "TBC";
			//NOTE: don't send current score if IP - will generate errors as not valid number
		}
		//Send current_score to display on parent window - display_cc_$i
		window.parent.$("#display_cc_"+i).val(final_score);
		//Set the marker so that the dialog knows it was saved before closing
		window.parent.$("#cc_score_saved").val("ok");
		//Close dialog box
		window.parent.$("#dlg_cc_scoring").dialog("close");
	});
}

//Process the scoring based on the BOOL button clicks
function checkLevelScore($btn,act) {
	$(function() {
		//get the row general ID based on the button's ID
		var fld = getRowID($btn);
		//Save button status
		saveButtonStatus($btn,act,fld);
		setLastSaved();
		
		//Process button value - do I need to move down a level or am I finished?
		var p_id = $btn.parents("td").attr("p_id");
		var $div = $("#div_"+p_id);
		
		if(act=="yes") {
			var all = true;
			$div.find(".btn_yes").each(function() {
					var v = $(this).attr('val');
					var fld = getRowID($(this));
					//var i = $(this).prop('id');
					//var f = AssistString.explode('_',i);
					//var act2 = AssistArray.array_pop(f);
					//var fld = AssistString.implode('_',f);
					var iv = $('#'+fld).val();
				if(iv.length==0 || isNaN(parseInt(iv)) || v != parseInt(iv)) {
					all = false;
				}
			});
			if(all==true) {
				var score = $div.attr("score_achieved");
				score_achieved = score;
				$("#td_display_score").html(score).css("font-size","300%");
				have_score = true;
				$("#btn_accept").show();
				$("#btn_ip_save").hide();
				$("#btn_close").hide();
				$("#p_instruction").html(finished_instruction);
			}
		} else {
			//alert("oops, move down a level");
			var score = $div.attr("score_failed");
			if($div.attr("is_lowest")==1) {
				//alert("lowest level failed = final score: "+score);
				score_achieved = score;
				have_score = true;
				$("#td_display_score").html(score).css("font-size","300%");
				$("#btn_accept").show();
				$("#btn_ip_save").hide();
				$("#btn_close").hide();
				$("#p_instruction").html(finished_instruction);
			} else {
				$("#td_display_score").html(ip_score).css("font-size","120%");
				have_score = false;
				$("#p_instruction").html(ip_instruction);
				$("#btn_accept").hide();
				$("#btn_ip_save").show();
				$("#btn_close").show();
				$div.hide();
				$("div.prof_div").each(function() {
					score_achieved = $(this).attr("score_achieved");
					if(score_achieved==score) {
						$(this).show();
						$(this).parent().scrollTop(0);
					}
				});			
			}
		}
	});
}

//Get the row's ID based on button's ID
function getRowID($btn) {
	var i = $btn.prop('id');
	var f = AssistString.explode('_',i);
	var act2 = AssistArray.array_pop(f);
	var fld = AssistString.implode('_',f);
	return fld;
}


//Process the scoring based on the BOOL button clicks
function moveBetweenLevels($btn,act) {
	$(function() {
			
		var p_id = $btn.parents("td").attr("p_id");
		var $div = $("#div_"+p_id);
		
		//down button shouldn't be available at the lowest level but catch it anyway
		if($div.attr("is_lowest")==1 && act=="down") {
			alert("Error: This is the lowest level.");
		//up button shouldn't be available at the highest level but catch it anyway
		} else if($div.attr("is_highest")==1 && act=="up") {
			alert("Error: This is the highest level.");
		} else {
			//get the next score level for up and the previous score level for down
			var score = act=="down" ? $div.attr("score_failed") : $div.attr("score_achieved");
			//hide the current div
			$div.hide();
			//loop through the divs to find one with the matching score
			var did_i_find_one = false; //bool to check that another level was found
			$("div.prof_div").each(function() {
				var check_score = act=="down" ? $(this).attr("score_achieved") : $(this).attr("score_failed");
				if(check_score==score) {
					$(this).show();
					$(this).parent().scrollTop(0);
					did_i_find_one = true;
					return false;
				}
			});	
			//if next div wasn't found then alert the user and redisplay the current div		
			if(did_i_find_one===false) {
				$div.show();
				alert("The next level wasn't found.  Please check the Proficiency levels in Setup.");
			}
		}
	});
}


<?php

if(count($scores)>0) {
	?>
//on page load move through all the buttons and check for level changes	
$(function() {
	$(".extra_status_check").each(function() {
		if($(this).val().length>0) {
			var p_id = $(this).parents("td").attr("p_id");
			var $div = $("#div_"+p_id);
			var act = parseInt($(this).val())==1 ? "yes" : "no";
			if(act=="yes") {
				var all = true;
				$div.find(".btn_yes").each(function() {
						var v = $(this).attr('val');
						var fld = getRowID($(this));
						//var i = $(this).prop('id');
						//var f = AssistString.explode('_',i);
						//var act2 = AssistArray.array_pop(f);
						//var fld = AssistString.implode('_',f);
						var iv = $('#'+fld).val();
					if(iv.length==0 || isNaN(parseInt(iv)) || v != parseInt(iv)) {
						all = false;
					}
				});
				if(all==true) {
					var score = $div.attr("score_achieved");
					score_achieved = score;
					$("#td_display_score").html(score).css("font-size","300%");
					have_score = true;
					$("#btn_accept").show();
					$("#btn_ip_save").hide();
					$("#btn_close").hide();
					$("#p_instruction").html(finished_instruction);
				}
			} else {
				//alert("oops, move down a level");
				var score = $div.attr("score_failed");
				if($div.attr("is_lowest")==1) {
					//alert("lowest level failed = final score: "+score);
					score_achieved = score;
					have_score = true;
					$("#td_display_score").html(score).css("font-size","300%");
					$("#btn_accept").show();
					$("#btn_ip_save").hide();
					$("#btn_close").hide();
					$("#p_instruction").html(finished_instruction);
				} else {
					$("#td_display_score").html(ip_score).css("font-size","120%");
					have_score = false;
					$("#p_instruction").html(ip_instruction);
					$("#btn_accept").hide();
					$("#btn_ip_save").show();
					$("#btn_close").show();
					$div.hide();
					$("div.prof_div").each(function() {
						score_achieved = $(this).attr("score_achieved");
						if(score_achieved==score) {
							$(this).show();
							$(this).parent().scrollTop(0);
						}
					});			
				}
			}
		}
	});
});
	
	
	<?php
}
?>


</script>

