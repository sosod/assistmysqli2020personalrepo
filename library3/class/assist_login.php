<?php
/** CLASS TO MANAGE LOGIN PROCESS
Created on: 2 October 2012
Created by: Janet Currie

Deployed to Dev: 3 October 2012


**/


class ASSIST_LOGIN extends ASSIST_HELPER {

	private $user = "";
	private $cc = "";
	private $pwd = "";
	
	private $sessid = "";
	
	private $mdb;
	private $cdb;
	
	private $login_error = 0;
	private $login_error_message = "";
	
	private $cmp_details = array();
	private $user_details = array();
	private $bp_details = array();
	private $site_details = array();
	private $is_reseller = 0;
	private $is_support = 0;
	
	private $screen = array('h'=>0,'w'=>0);
	private $next_page;
	
	private $default_error_message = "Invalid login details.  Please try again.<br /><br /><span class=i>Hint: Remember that passwords are CaSe SeNsItIvE.</span>";
	
	private $request = array();

	public function __construct($var,$valid8 = false) {
		$this->request = $var;
		if($valid8===true) { 
			$this->mdb = new ASSIST_DB("master");
			$this->validateLogin($var); 
		}
	}
	
	private function killMe($err=null) {
		$this->login_error += 1;
		if(strlen($this->login_error_message)==0) {
			$this->login_error_message = (!isset($err) || strlen($err)==0) ? $this->default_error_message : $err;
		}
		/*$result = array("error",);
		echo "
		<div title='Login Error' id=dlg_login_error>
			<h2>Login Error</h2>";
			$this->displayResult($result);
		echo "
		</div>
		<script type=text/javascript>
			$('#dlg_login_error').dialog({autoOpen:false});
		</script>";*/
	}
	
	function drawKillMe() {
		echo "
		<style type=text/css>
		#dlg_login_error p { font-size: 9pt; line-height: 11pt; font-style: Verdana; margin: 10px; }
		</style>
		<div title='Login Error' id=dlg_login_error>
			<p style='font-size: 14pt; font-weight: bold; color: #cc0001; line-height: 16pt'>Login Error</p>
			<p id=error_message>Invalid login details.</p>
			";
		echo "
		</div>
		<script type=text/javascript>
			$(function() {
				$('#dlg_login_error').dialog({
					autoOpen: false, 
					modal: true, 
					width: 400, 
					height: 200,
					buttons: {
						'Close': function() {
							$(this).dialog('close'); 
						}
					}
				});//.find('.ui-dialog').css({border:\"1px solid #CC0001\"});
				AssistHelper.formatDialogButtons($('#dlg_login_error'),0,AssistHelper.getGreenCSS());
				AssistHelper.formatDialogButtons($('#dlg_login_error'),0,{'padding':'0px','font-size':'75%'});
				AssistHelper.hideDialogTitlebar('id','dlg_login_error');
				//$('#dlg_login_error #btn_error_close').click(function(){ $('#dlg_login_error').dialog('close'); });
			});
			
			function executeKillMe(errmsg) {
				errmsg = (typeof errmsg === 'undefined' || errmsg.length==0) ? '".$this->default_error_message."' : errmsg;
				$('#dlg_login_error #error_message').attr('innerHTML',errmsg);
				$('#dlg_login_error').dialog('open');
			}
		</script>";
	}

	

	
	
	
/**VALIDATION FUNCTIONS**/
	private function validateLogin($var) {
	
		$this->user = isset($var['uid']) ? trim(strtolower($var['uid'])) : $this->killMe();
		$this->cc = isset($var['cc']) ? trim(strtolower($var['cc'])) : $this->killMe();
		$this->pwd = isset($var['pid']) ? $this->convertToHex($var['pid']) : $this->killMe();
		$this->screen['h'] = (isset($var['screen_h']) ? $var['screen_h'] : 0);
		$this->screen['w'] = (isset($var['screen_w']) ? $var['screen_w'] : 0);
		$this->sessid = $var['niosses'];
		//Validate Company & check for reseller
		if($this->login_error==0) { $this->validateCompany(); }
		//$this->login_error = 1;
		//$this->login_error_message = "company";

		//Validate User
		if($this->login_error==0) { $this->validateUser(); }

		//Validate Pwd
		if($this->login_error==0) { $this->validatePwd(); }
		
		//Validate Account Status
		if($this->login_error==0) { $this->validateAccountStatus(); }

		//Validate Login Window
		if($this->login_error==0) { $this->validateLoginWindow(); }
	
		if($this->login_error==0) { 
			//Is company a reseller?
				$this->isReseller(); 
			//Is user Support?
				$this->isSupport(); 
			//Set Session Variables
				$this->setSession();
			//determine the next page to go to
				$this->nextStep();
		}

	}
	private function validateCompany() {
		$sql = "SELECT * FROM assist_company WHERE cmpcode = '".strtoupper($this->cc)."' AND cmpstatus = 'Y'"; 
		$cmp_details = $this->mdb->mysql_fetch_one($sql);
		if(!isset($cmp_details['cmpname'])) { 
			$this->killMe();
		} else {
			$_SESSION['cc'] = strtolower($this->cc);
			$this->cdb = new ASSIST_DB("client",strtolower($this->cc));
			$this->cmp_details = $cmp_details;
			$this->bp_details = $this->getBPAdetails($this->cmp_details['cmpreseller']);
			$this->site_details = array(
				'name'=>$this->bp_details['site_name'],
				'logo'=>$this->bp_details['assist_logo'],
				'code'=>$this->bp_details['site_code'],
				'logo_link'=>"pics/bp_logos/".$this->bp_details['assist_logo'],
				'given_code'=>$this->request['site_code'],
			);
			//$this->login_error_message = serialize($this->bp_details);
		}
	}
	
	private function validateUser() {
		if($this->user=="admin") {
			$this->user_details = array(
				'tkid' => "0000",
				'tkn' => "Assist Administrator",
				'tkpwd' => $this->cmp_details['cmpadminpwd'],
				'tkstatus' => 1,
				'tkemail' => $this->cmp_details['cmpadminemail'],
				'tklogin' => 1,
				'tkpwdyn' => "N",
				'tklock' => "N",
				'tkloginwindow'=> "",
			);
		} else {
			$sql = "SELECT tkid, CONCAT(tkname, ' ', tksurname) as tkn, tkpwd, tkstatus, tkemail, tklogin, tkpwdyn, tklock, tkloginwindow FROM assist_".strtolower($this->cc)."_timekeep WHERE tkuser = '".$this->user."'";
			$rs = $this->cdb->db_get_num_rows($sql);
			if($rs!=1) {
				$this->login_error_message = "There is an error with your user account.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
				$this->killMe();
			} else {
				$one = $this->cdb->mysql_fetch_one($sql);
				$this->user_details = $one;
			}
		}
	}

	private function validatePwd() {
		if($this->pwd != $this->user_details['tkpwd']) {
			$this->killMe();
		}
	}

	private function validateAccountStatus() {
		if($this->user_details['tkstatus']==0) {
			$this->killMe();
		} elseif($this->user_details['tklock']=="Y") {
			$this->killMe();
			$this->login_error_message = "Your account has been locked.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
		}
	}

	private function validateLoginWindow() {
		if($this->user!="admin" && strtotime($this->user_details['tkloginwindow']) < $this->today && !($this->user_details['tklogin']>0)) {
			$this->killMe();
			$this->login_error_message = "Your first-time login window has expired and your account has been locked.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
		}
	}

	private function isReseller() {
		if($this->user!="admin" && $this->user!="support") {
			$sql = "SELECT action_reseller FROM assist_reseller WHERE cmpcode = '".$this->cc."' AND active = true";
			$rs = $this->mdb->db_query($sql);
			if(mysql_num_rows($rs)>0) {
				$r = mysql_fetch_assoc($rs);
				$this->is_reseller = $r['action_reseller'];
			}
		}
		//$this->killMe("resel");
	}

	private function isSupport() {
		if($this->user=="support") {
			$cmpcode = strtolower($this->cc);
			$tkid = $this->user_details['tkid'];
			$this->is_support = 1;
				$sql = "INSERT INTO assist_".strtolower($this->cc)."_menu_modules_users (usrmodid,usrmodref,usrtkid)
					SELECT mm.modid, m.modref, '".$tkid."' FROM assist_menu_modules m INNER JOIN  assist_".$cmpcode."_menu_modules mm ON m.modid = mm.modmenuid
					WHERE m.modyn = 'Y' AND m.modref NOT IN (select usrmodref from assist_".$cmpcode."_menu_modules_users WHERE usrtkid = '".$tkid."')";
			$this->cdb->db_insert($sql);
		}
	}

	private function getTerminology() {
		$sql = "SELECT field, value FROM assist_".strtolower($this->cc)."_setup WHERE ref = 'IA_TERM'";
		$ct = $this->cdb->mysql_fetch_fld2_one($sql,"field","value");
		if(count($ct)==0) {
			$sql2 = "INSERT INTO `assist_".strtolower($this->cc)."_setup` (`id`, `ref`, `refid`, `value`, `comment`, `field`) VALUES (null, 'IA_TERM', 0, 'Department', 'Name of Department', 'DEPT'), (null, 'IA_TERM', 0, 'Section', 'Name of Section', 'SUB')";
			$this->cdb->db_insert($sql2);
			$ct = $this->cdb->mysql_fetch_fld2_one($sql,"field","value");
		}
		return $ct;
	}

	private function setSession() {
		$_SESSION['cc'] = strtolower($this->cc);
		$_SESSION['bpa'] = $this->cmp_details['cmpreseller'];
		$_SESSION['tid'] = $this->user_details['tkid'];
		$_SESSION['tkn'] = $this->user_details['tkn'];
		$_SESSION['tku'] = $this->user;
		$_SESSION['ref'] = "main";
		$_SESSION['ia_cmp_reseller'] = $this->cmp_details['cmpreseller'];
		$_SESSION['ia_cmp_dispname'] = $this->cmp_details['cmpdispname'];
		$_SESSION['ia_cmp_name'] = $this->cmp_details['cmpname'];
		$_SESSION['ia_cmp_logo'] = $this->cmp_details['cmplogo'];
		$_SESSION['ia_reseller'] = $this->is_reseller;
		$_SESSION['ia_support'] = $this->is_support;
		$_SESSION['screen'] = array('height'=>$this->screen['h'],'width'=>$this->screen['w']);
		$_SESSION['cmp_terminology'] = $this->getTerminology();
		$_SESSION['DISPLAY_INFO']['site_code'] = $this->site_details['code'];
		$this->getUserProfile(strtolower($this->cc),$this->user_details['tkid']);
	}
	
	private function nextStep() {
		if($this->user=="admin" || ($this->user_details['tklogin']>0 && $this->user_details['tkpwdyn'] != "Y")) {
			$tkid = $this->user_details['tkid'];
			$cmpcode = strtolower($this->cc);
			
			//update the login count
			$sql = "UPDATE assist_".$cmpcode."_timekeep SET tklogin = tklogin + 1, tklogindate = '".strtotime(date("d F Y H:i:s"))."' WHERE tkid = '$tkid'";
			$this->cdb->db_update($sql);
			
			//log the activity
			$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) VALUES (null,now(),'$tkid','".$this->user_details['tkn']."','IN',1,'".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."')";
			$this->cdb->db_insert($sql);
			
			$this->next_page = "login.html";
		} else {
			if($this->user_details['tklogin']>0 && $this->user_details['tkpwdyn']=="Y") {
				$this->next_page = "password1.php";
			} else {
				$this->next_page = "terms_user.php";
			}
		}
	}
	
	public function getValidationResult() {
		/*$data = array(
			1,
			"errormessage",
			"nextpage"
		);*/
		//$data[0] = $this->login_error;
		//$data[1] = $this->login_error_message;
		//$data[2] = $this->next_page;
		$data = array(
			$this->login_error,
			$this->login_error_message,
			$this->next_page
		);
		return $data;
	}
	
	

	function __destruct() {
		
	}
}



?>