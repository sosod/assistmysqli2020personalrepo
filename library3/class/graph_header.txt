class ASSIST_GRAPH {
	public function __construct(int=0);			//Create instance, pass int variable for graph id else 0 will be used
	
	//GRAPH FORMATTING FUNCTIONS
	public function setGraphID(int) 			//Set the graph id to something else if more than 1 graph on the page
	public function setGraphAsPie()				//set the graph type to PIE	[default]
	public function setGraphAsBar()				//set the graph type to BAR
	public function setFormatAsJava()			//Use the JAVA based graphs	[default]
	public function setFormatAsFlash()			//Use the FLASH based graphs -> not yet working
	public function setLayoutAsPortrait() 		//Format the page for printing on Portrait
	public function setLayoutAsLandscape() 		//Format the page for printing on Landscape/widescreen [default]

	//GENERAL
	public function setHasMainGraph(bool) 		//Display a main/summary graph
	public function setHasSubGraph(bool)		//Display sub graphs
	public function setPageTitle(str)			//Set the page title to display; default=blank, if blank then no page title/report timestamp will display
	public function setBlurb(str)				//Set the blurb to add to the report timestamp, page title must be set to display blurb; default=blank
	public function setMainTitle(str)			//Set the heading for the main graph
	public function setSubTitle(str)			//Set the collective heading for the sub graphs
	public function setDrawHeader(bool)			//Set whether the page header (<html><head><body> etc) must display; default=false; if true then will include jquery js/css necessary to draw graphs
	
	//DATA SETTING FUNCTIONS -> minimum required before calling drawPage();
	public function setGraphCategories(array)	//Set the categories (legend); Input = array([#]=>array('code'=>char,'color'=>str||hex,'text'=>str)) => default colors available: red, orange, green, darkgreen, blue, grey
	public function setMainCount(array=array())	//Set the data to be displayed in main graph, if blank array is sent & has_sub_graph=true, then main count will be array_sum of counts for sub graphs; format: array([code]=>int,...)
		//required if has_sub_graph = true
	public function setSubList(array)			//Set the items to be displayed in the sub graphs; format: array([s_id]=>str,...)
	public function setSubCount(array)			//Set the data to be displayed in sub graphs if has_sub_graph = true; format: array([s_id]=>array([code]=>int,...),...)
	
	//PIE GRAPHS ONLY
	public function setMainGraphAbove()			//If there is a main/summary pie graph, display it above the sub graphs [default setting]
	public function setMainGraphInline()		//OR display it inline with the sub graphs
	
	//BAR GRAPH ONLY
	public function setBarGraphByPercent()		//Display the bars as 100% stacked [default]
	public function setBarGraphByNumber()		//OR display the bars as regular numbers
	public function setDisplayBarGrid(bool)		//Display the background grid [default=false]


	//GENERATE PAGE
	public function drawPage()

}