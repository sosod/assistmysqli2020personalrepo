<?php


class ASSIST_REPORT_FIXED_DRAW extends ASSIST_REPORT_FIXED {

	private $data;
	private $settings;
	private $report_title;

	/************** REQUIRED INPUT ***********************
	
	$data = Array(
		[settings] => Array (
            [layout] => "",						P* || L
            [hasMain] => true,					true || false
            [hasSub] => false,					true* || false
            [manualPageTitle] => "",
			[bar] => Array(
				[format] => "100",				100* || NUM
				[grid] => false,				true || false*
				
			),
        ),
		[graphX_id] => Array(
			[graph_title] => "",
			[blurb] => "",
			[type] => "PIE",					PIE* || BAR
			[result_options] => Array(
				[resultX_id] => Array(
					[text] => "",
					[code] => "resultX_id",
					[color]=> "",
				),
				.....
			),
			[data]=> Array(
				[resultX_id] => ##,
				.....
			)
		),
		.....
	);
	
	$data = array(
		'settings' => array (
			'layout' => "P",
			'hasMain' => true,
			'hasSub' => true,
			'manualPageTitle' => false,
			'bar' => array(
				'format' => "100",
				'grid'	=> false,
			),
		),
		'graphX_id' => array(
			'graph_title' => "",
			'blurb' => "",
			'type' => "",
			'result_options' => array(
				'resultX_id' => array(
					'text' => "",
					'code' => "resultX_id",
					'color'=> "",
				),
			),
			'data' => array(
				'subX_id' => array(
					'resultX_id' => ##,
				),
			),
			'subs' => array(
				'subX_id' => "Sub name",
			)
		)
	);
	
	********************************************************/
	
	public function __construct($d) {
		parent::__construct();
		$this->data = $d;
		//$this->arrPrint($this->data);
		$this->settings = $this->data['settings'];
		unset($this->data['settings']);
	}

	
	public function drawPage() {
		//echo "<p>ASSIST_REPORT_FIXED_DRAW.drawPage()!!!</p>";
		
		echo "
		<h1 class=center>".$this->report_title."</h1>
		";
		
		$g = 0;
		foreach($this->data as $key => $d) {
			$graph = new ASSIST_GRAPH($g);	$g++;
			if($d['type']=="PIE") {
				$graph->setGraphAsPie();
			} elseif($d['type']=="BAR") {
				$graph->setGraphAsBar();
				if(isset($this->settings['bar']['format']) && $this->settings['bar']['format'] == "NUM") {	//default = 100% stacked
					$graph->setBarGraphByNumber();
				}
				if(isset($this->settings['bar']['grid'])) {
					$graph->setDisplayBarGrid($this->settings['bar']['grid']);		//Display the background grid [default=false]
				}
			}
			if($this->settings['layout']=="L") {
				$graph->setLayoutAsLandscape();
			} else {
				$graph->setLayoutAsPortrait();
			}
			if(isset($d['page_title']) && strlen($d['page_title'])>0) { $graph->setPageTitle($d['page_title']); }
			if(isset($d['graph_title']) && strlen($d['graph_title'])>0) { $graph->setGraphTitle($d['graph_title']); }
			$graph->setBlurb($d['blurb']);
			$graph->setGraphCategories($d['result_options']);
			$graph->setHasSubGraph($this->settings['hasSub']);
			if($this->settings['hasSub']===false) {
				$graph->setMainCount($d['data']);
			} else {
				$graph->setSubList($d['subs']);			//Set the items to be displayed in the sub graphs; format: array([s_id]=>str,...)
				$graph->setSubCount($d['data']);			//Set the data to be displayed in sub graphs if has_sub_graph = true; format: array([s_id]=>array([code]=>int,...),...)
			}
			
			$graph->drawPage();
		}
	
	}
	
	public function setReportTitle($r="") { $this->report_title = $r; }


}
?>