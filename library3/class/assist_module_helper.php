<?php

/******* FILE ERROR CODES
UPLOAD_ERR_INI_SIZE : 1 : File size exceeds limit.
UPLOAD_ERR_FORM_SIZE : 2 : File size exceeds limit.
UPLOAD_ERR_PARTIAL : 3 : The uploaded file was only partially uploaded. Please try again before contacting your Business Partner with the following error code: FILE3.
UPLOAD_ERR_NO_FILE : 4 : No file was uploaded. Please check that the original file still exists and try again before contacting your Business Partner with the following error code: FILE4.
UPLOAD_ERR_NO_TMP_DIR : 6 : A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE6.
UPLOAD_ERR_CANT_WRITE : 7 :  A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE7.
UPLOAD_ERR_EXTENSION : 8 :   A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE8.
*/

class ASSIST_MODULE_HELPER extends ASSIST_DB {

	public $assist;
	public $assist_user;
	protected $cc;
	protected $user_id;
	protected $mod_ref;
	protected $attach_java_already_echoed = false;
	protected $attachment_default_content = "application/octet-stream";
	
	private $attachment_delete_ajax = false;
	private $attachment_delete_function = "";
	private $attachment_delete_link = "ajax/delete_attachment.php";
	private $attachment_delete_options = "";
	private $attachment_download_link = "ajax/download_attachment.php";
	private $attachment_download_options = "";
	private $attachment_folder = "";

	private $deleted_folder = "deleted";
	
	private $file_error_codes = array(
		1 => "File size exceeds limit.",
		2 => "File size exceeds limit.",
		3 => "The uploaded file was only partially uploaded. Please try again before contacting your Business Partner with the following error code: FILE[3].",
		4 => "No file was uploaded. Please check that the original file still exists and try again before contacting your Business Partner with the following error code: FILE[4].",
		6 => "A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[6].",
		7 => "A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[7].",
		8 => "A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[8].",
	);
	
	 protected $default_activity_names = array(
		'view'=>"View",
		'update'=>"Update",
		'updated'=>"Updated",
		'edit'=>"Edit",
		'edited'=>"Edited",
		'save'=>"Save",
		'approve'=>"Approve",
		'approved'=>"Approved",
		'approval'=>"Approval",
		'assess'=>"Assess",
		'add'=>"Add",
		'created'=>"Created",
		'confirm'=>"Confirm",
		'confirmed'=>"Confirmed",
		'activate'=>"Activate",
		'activated'=>"Activated",
		'activation'=>"Activation",
		'deactivate'=>"Deactivate",
		'deactivated'=>"Deactivated",
		'deactivation'=>"Deactivation",
		'unlock'=>"Unlock",
		'unlocked'=>"Unlocked",
		'open'=>"Open",
		'decline'=>"Decline",
		'declined'=>"Declined",
		'restore'=>"Restore",
		'delete'=>"Delete",
		'download'=>"Download",
		'upload'=>"Upload",
		'import'=>"Import",
		'export'=>"Export",
		'reset'=>"Reset",
		'review'=>"Review",
	);
	protected $activity_names = array();
	
	public function __construct($dbtype="client",$cc="",$autojob=false) {
		$this->assist = new ASSIST($autojob);
		$this->assist_user = new ASSIST_USER();
		if(strlen($cc)==0) {
			$this->cc = $this->assist->getCmpCode();
			$cc = $this->cc;
		} else {
			$this->cc = $cc;
		}
		parent::__construct($dbtype,$cc);
		$this->user_id = $this->assist->getUserID();
		$this->mod_ref = $this->assist->getModRef();


	}

	
	
	/** 
		$attach = array('unique_key'=>array('system_filename'=>src file,'original_filename'=>user file)
	**/
	function displayObjectAttachments($can_edit=false,$attach=array(),$folder="",$need_java=true) {
		echo $this->getObjectAttachmentDisplay($can_edit,$attach,$folder,$need_java);
	}
	function getObjectAttachmentDisplay($can_edit=false,$attach=array(),$loc="",$need_java=true) {
	//echo $loc;
		$echo = "";
			$delete_link = $this->getHelperAttachmentDeleteLink();
			$delete_options = $this->getHelperAttachmentDeleteOptions();
			$download_link = $this->getHelperAttachmentDownloadLink();
			$download_options = $this->getHelperAttachmentDownloadOptions();
			if(strlen($loc)==0) { $loc = $this->getHelperAttachmentFolder(); }
			$valid_files = false;
			$folder = "";
			$location = explode("/",$_SERVER["REQUEST_URI"]);
			$l = count($location)-2;
			for($f=0;$f<$l;$f++) {
				$folder.= "../";
			}
			$folder.="files/".$this->getCmpCode()."/".$loc;
			foreach($attach as $key=>$a) {
				$file = $folder."/".$a['system_filename'];
				$fname = $can_edit==true && strlen($a['original_filename'])>26 ? substr($a['original_filename'],0,25)."..." : $a['original_filename'];
				if(file_exists($file)) {
					if($valid_files===false) { $echo.= "<p class=b>Attached Files:</p><ul>"; }
					$valid_files = true;
					$echo.= "<li id=li_".$key."><a id=".$key." class='down_attach u' style='cursor: pointer' >".$fname."</a>".($can_edit==true ? "<span class='float red hand delete_attach' key=".$key.">Delete</span>" : "")."</li>";
				}
			}
			if($valid_files===true) {
				$echo.= "</ul>";
				if($this->attach_java_already_echoed!==true && $need_java===true) {
					$this->attach_java_already_echoed = true;
					$echo.= "
					<script type=text/javascript>
					//alert('attachment_form!');
					$(function() {
						$('a.down_attach').click(function() {  //alert('down_attach clicked!');
							var i = $(this).prop('id');  //alert(i);
							var url = '".(strlen($download_link)>0 ? $download_link : "ajax/download_attachment.php")."?id='+i".(strlen($download_options)>0 ? "+'&".$download_options."'" : "").";
							//alert('downloading '+i+' via \\n'+url);
							document.location.href = url;
						});";
						if($can_edit) {
						$echo.="
						$('span.delete_attach').hover(
							function(){ $(this).removeClass('red').addClass('orange'); $(this).parent().css({'background-color':'#ababab'}); },
							function(){ $(this).removeClass('orange').addClass('red'); $(this).parent().css({'background-color':''}); }
						).click(function() {
							var i = $(this).attr('key');
							if(confirm('Are you sure you wish to delete the attachment: \\n'+$('#'+i).text()+'? \\n \\n Warning: Any other changes made on this page will be lost.\\nThis action cannot be undone.')) {
						";
								if($this->getHelperAttachmentDeleteByAjax()===true && strlen($this->getHelperAttachmentDeleteFunction())>0) {
									$echo.="
										var index = i;
										var url = '".(strlen($delete_link)>0 ? $delete_link : "ajax/delete_attachment.php")."';
										//alert(url);
										var dta = 'id='+i".(strlen($delete_options)>0 ? "+'&".$delete_options."'" : "").";
										//alert(dta);
										".$this->getHelperAttachmentDeleteFunction()."(index,url,dta);
									";
								} else {
									$echo.="
										var url = '".(strlen($delete_link)>0 ? $delete_link : "ajax/delete_attachment.php")."?id='+i".(strlen($delete_options)>0 ? "+'&".$delete_options."'" : "").";
										document.location.href = url;
									";
								}
								$echo.="
							} //endif confirm delete
						});";
						}
					$echo.="
					});
					</script>";
				}
			}
		return $echo;
	}
	
	function getAttachmentJavaForDownload($can_edit=false,$need_script_tags=false) {
			$delete_link = $this->getHelperAttachmentDeleteLink();
			$delete_options = $this->getHelperAttachmentDeleteOptions();
			$download_link = $this->getHelperAttachmentDownloadLink();
			$download_options = $this->getHelperAttachmentDownloadOptions();
				$this->attach_java_already_echoed = true;
				$echo= ($need_script_tags===true ? "<script type=text/javascript>" : "")."
				//alert('attachment_form!');
					//function downloadAttachmentClickEvent(\$me) {  
						$('a.down_attach').live('click',function() { 
				//alert('down_attach clicked!');
							var i = $(this).prop('id');  //alert(i);
							var url = '".(strlen($download_link)>0 ? $download_link : "ajax/download_attachment.php")."?id='+i".(strlen($download_options)>0 ? "+'&".$download_options."'" : "").";
							//alert('downloading '+i+' via \\n'+url);
							document.location.href = url;
						});
					//}";
					$echo.="
					".($need_script_tags===true ? "</script>" : "");
		return $echo;
	}
	
	function displayAttachmentForm($next_step="") {
		echo $this->getAttachmentForm($next_step);
	}
	function getAttachmentForm($next_step="") {
		$echo= "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" style='margin-bottom: 5px;' /> <input type=button value=\"Clear\" class=cancel_attach  />
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			<input type=hidden name=next_step value='".$next_step."' id=next_step />
			<input type=hidden value='' name=result id=result />
			<input type=hidden value='' name=response id=response />
			".($_SESSION['cc']=="test987DEV" ? "<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"../common/test.php\" style=\"width:250px;height:250px;border:1px solid #999999;color:#fe9900\"></iframe>" : "<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #ffffff;color:#ffffff;\"></iframe>")."
			".$this->getDisplayResult(array("warn","No more than 10 files may be uploaded at once.<br />Total file size cannot exceed 10MB per update."))."
		<script type=text/javascript>
		$(function() {
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			var attachment_count = 1;
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<input type=button value=\"Clear\" class=cancel_attach />').bind('click',function() { handler($('#firstdoc input:button').index(this)); });
				});
				attachment_count++;
				if(attachment_count==10) { $('#attachlink').hide(); }
			});
			$('input:button.cancel_attach').click(function() {
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		});
		</script>";
		return $echo;
	}
	
	function resetAttachmentForm() {
		return "<input type=\"file\" name=\"attachments[]\" size=\"30\" style='margin-bottom: 5px;' /> <input type=button value=\"Clear\" class=cancel_attach  />";
	}	
	
	
	function downloadAttachment($full_path, $new_filename) {
		/*$size = filesize($full_path);
		@header('Content-Length: '.$size);
		//header('Content-Description: File Transfer');
		@header('Content-Type: application/octet-stream');
		@header('Content-Transfer-Encoding: binary');
		@header("Content-disposition: attachment; filename=".$new_filename);
		@readfile($full_path);*/
		$this->assist->downloadFile3($full_path,$new_filename,"application/octet-stream");
	}		
	
	
	
	/***********************
		SET FUNCTIONS 
	***********************/
	function setHelperAttachmentDeleteByAjax($a) { $this->attachment_delete_ajax = $a; }
	function setHelperAttachmentDeleteFunction($a) { $this->attachment_delete_function = $a; }
	function setHelperAttachmentDeleteLink($a) { $this->attachment_delete_link = $a; }
	function setHelperAttachmentDeleteOptions($a) { $this->attachment_delete_options = $a; }
	function setHelperAttachmentDownloadLink($a) { $this->attachment_download_link = $a; }
	function setHelperAttachmentDownloadOptions($a) { $this->attachment_download_options = $a; }
	function setHelperAttachmentFolder($a) { $this->attachment_folder = $a; }
	
	/***********************
		GET FUNCTIONS 
	***********************/
	function getAttachmentContent() { return $this->attachment_default_content; }
	function getHelperAttachmentDeleteByAjax() { return $this->attachment_delete_ajax; }
	function getHelperAttachmentDeleteFunction() { return $this->attachment_delete_function; }
	function getHelperAttachmentDeleteLink() { return $this->attachment_delete_link; }
	function getHelperAttachmentDeleteOptions() { return $this->attachment_delete_options; }
	function getHelperAttachmentDownloadLink() { return $this->attachment_download_link; }
	function getHelperAttachmentDownloadOptions() { return $this->attachment_download_options; }
	function getHelperAttachmentFolder() { return $this->attachment_folder; }
	function getGenericAttachmentDeletedFolder() { return $this->deleted_folder; }
	//file error codes
	function getFileErrorMsg($e) {
		if(isset($this->file_error_codes[$e])) { 
			return $this->file_error_codes[$e];
		} else {
			return "An unknown error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[".$e."].";
		}
	}
	







    /************
     * Reworking functions
     */
     
     
     /**
      * Function to convert small array of menu items into the formatted menu item needed for the ASSIST_HELPER::getNavButtons($menu)
      * 
      * @param (Array) arr = array with display, link, active
      * @param (int) level = tier of buttons 1 or 2
      *              needed:$menu[id] = array('id'=>id,'url'=>path_to,'active'=>(bool)checked_or_not,'display'=>heading); 
      * @return (HTML) code needed to echo onscreen to generate buttons 
      */
   public function generateNavigationButtons($buttons,$level) {
       $menu = array();
       foreach($buttons as $key => $a){
           $menu[$key] = array(
            'id'=>$a['id'],
            'url'=>$a['link'],
            'active'=>($a['active']==1),
            'display'=>$a['name'],
            'help'=>$a['help']
           );
       }
       $this->assist->setNavButtonsLevel($level);
       return $this->assist->getNavButtons($menu);
       
   }




	
	
	
	
	
	
	
	/******************************************************************************
	 * Activity name functions
	 ******************************************************************************/
	 
	 public function getActivityName($a) {
    	if(isset($this->activity_names[strtolower($a)])) {
    		return $this->activity_names[strtolower($a)];
		} elseif(isset($this->default_activity_names[strtolower($a)])) {
			return $this->default_activity_names[strtolower($a)];
		} else {
			return "";
		}
    }
	public function getAllActivityNames() {
		$a = array();
		foreach($this->default_activity_names as $key => $dan) {
			if(isset($this->activity_names[$key])) {
				$a[$key] = $this->activity_names[$key];
			} else {
				$a[$key] = $dan;
			}
		}
		return $a;
	}
	
    /**
	 * Function to replace |activity| with acitivty_name
	 */
    public function replaceActivityNames($arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
	        foreach($this->activity_names as $key => $on) {
	            $a = str_ireplace("|".$key."|",$on,$a);
	        }
	        $arr = json_decode($a,true);
		} else {
	        foreach($this->activity_names as $key => $on) {
	            $arr = str_ireplace("|".$key."|",$on,$arr);
	        }
		}
        return $arr;
    }
	
	
	
	
	
	
	





	



	/***********************
		ASSIST SHORTCUT FUNCTIONS 
	***********************/
	function displayPageHeader() {
		$this->assist->displayPageHeader();			//echos automatically
	}
	function displayPageHeaderJQ($jqv="1.10.0") {
		$this->assist->displayPageHeaderJQ($jqv);			//echos automatically
	}
	function displayResult($r) {
		$this->assist->displayResult($r);			//echos automatically
	}
	function getDisplayResult($r) {
		return $this->assist->getDisplayResult($r);
	}
	function JSdisplayResultPrep($t) {				//echos automatically
		$this->assist->JSdisplayResultPrep($t);
	}
	function getJSdisplayResultPrep($t) {
		return $this->assist->getJSdisplayResultPrep($t);
	}
	function displayIcon($r,$o="") {				//echo
		$this->assist->displayIcon($r,$o);
	}
	function getDisplayIcon($r,$o="") {				
		return $this->assist->getDisplayIcon($r,$o);
	}
	function getDisplayIconAsDiv($r,$o="") {				
		return $this->assist->getDisplayIconAsDiv($r,$o);
	}
	function code($r) {
		return $this->assist->code($r);
	}
	function decode($r) {
		return $this->assist->decode($r);
	}
	function arrPrint($a) {
		return $this->assist->arrPrint($a);
	}
	function stripStringForComparison($s) {
		return $this->assist->stripStringForComparison($s);
	}
	
	/**
	 * Function to convert array into string for SQL statement
	 * 
	 * @param (Array) var = array ('db field'=>"value")
	 * @param (String) join = glue for implode, default = , for insert
	 * @return (String) field = 'value', field2 = 'value2'
	 */
    function convertArrayToSQL($var,$join=",") {
		return $this->assist->convertArrayToSQL($var,$join);
    }
	
	function getAUserName($t) {
		return $this->assist->getTKDetails($t,$this->cc,"tkn");
	}
	function getAnEmail($t) {
		return $this->assist->getTKDetails($t,$this->cc,"tkemail");
	}
	function drawStatus($s) {
		return $this->assist->drawStatus($s);
	}
	function getUserName() {
		return $this->assist->getUserName();
	}
	function getUserID() {
		return $this->assist->getUserID();
	}
	function getToday() {
		return strtotime(date("d F Y H:i:s"));
	}
	function dateToInt($d) {
		return $this->assist->dateToInt($d);
	}
	function checkFolder($f) {
		return $this->assist->checkFolder($f);
	}
	function drawNavButtons($menu) {
		return $this->assist->drawNavButtons($menu);
	}
	
	function displayGoBack($a="",$b="Go Back") {
		return $this->assist->displayGoBack($a,$b);
	}
	
	function getGoBack($url="",$txt="") {
		return $this->assist->getGoBack($url,$txt);
	}
	
	function downloadFile2($folder, $old, $new, $content) {
		return $this->assist->downloadFile2($folder, $old, $new, $content);
	}

	function downloadFile3($old, $new) {
		return $this->assist->downloadFile3($old, $new);
	}

	function displayAuditLog($logs,$flds,$extra="") {
		return $this->assist->displayAuditLog($logs,$flds,$extra);
	}
	function isAdminUser() {
		return $this->assist->isAdminUser();
	}
	function isSupportUser() {
		return $this->assist->isSupportUser();
	}
	function formatBytes($b,$p = null) {
		return $this->assist->formatBytes($b,$p);
	}
	function checkIntRef($r) {
		return $this->assist->checkIntRef($r);
	}
	
	function getTerm($i,$plural=false){
		return $this->assist->getTerm($i,$plural);
	}
	
	function getFolderSize($folder,$ignore=array()) { return $this->assist->getFolderSize($folder,$ignore); }
	function displayFolderSize($totalsize,$folder,$moduletitle) { return $this->assist->displayFolderSize($totalsize,$folder,$moduletitle); }
	
	function getModTitle($ref="") { return $this->assist->getModTitle($ref); }
	function getModLocation() { return $this->assist->getModLocation(); }
	function getCmpName() { return $this->assist->getCmpName(); }
	function getSiteName() { return $this->assist->getSiteName(); }
	function getUnspecified() { return $this->assist->getUnspecified(); }
	
	/**
	 * Function to read main user profile to get default page length
	 */
	function mainProfileActionsToDisplay() { return $this->assist_user->mainProfileActionsToDisplay(); }
	
	
	
	
	
	/* generate the primary save path for an attachment */
	function getSaveFolder($depth=1) {
		$cmpcode = strtolower($this->getCmpCode());
		$path = "";
			$me = $_SERVER['PHP_SELF'];
			$myloc = explode("/",$me);
			if(strlen($myloc[0])==0) { unset($myloc[0]); }
			$mc = count($myloc) - 1;
			$path = "files/".$cmpcode."/".$path;
			$chk = explode("/",$path);
			$chkloc = "";
			for($m=1;$m<count($myloc);$m++) { $chkloc.="../"; }
			for($m=1;$m<$depth;$m++) { $chkloc.="../"; }
			foreach($chk as $c) {
				if(strlen($c)>0 && $c != "." && $c != "..") {
					$chkloc.= "/".$c;
				}
			}
		return $chkloc;
	}
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
	
}


?>