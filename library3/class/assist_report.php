<?php


class ASSIST_REPORT extends ASSIST {

	protected $db_date_format = "DATETIME";	/* What format are dates stored in the database?  VARCHAR / STRING || UNIX / NUMERIC || DATE / DATETIME */
	protected $defaults = array(
		'allowchoose' => true,
		'default_selected' => true,
		'allowfilter' => true,
		'type' => "TEXT",
		'data' => "",
		'default_data' => "",
		'allowgroupby' => true,
		'allowsortby' => true,
		'sortposition' => 0,
	);
	protected $vars;

// REPORT DETAILS
	protected $fields = array();				//array[id]=>('id'=>"",'heading'=>"")
	protected $columns = array();				//array[id]=>('id'=>"",'selected'=>true)
	protected $filters = array();				//array[id]=>('id'=>"",'type'=>"TEXT")
	protected $filter_types = array();		//array[id]=>('id'=>"",'data'=>""/array(),'default'=>"")
	protected $group_by = array();			//array[id]=>true
	protected $sort_by = array();				//array[id]=>true
	protected $sort_order = array();

	
	public function __construct() {
		//echo "<P>ASSIST_REPORT.__construct()</p>";
		parent::__construct();
		$this->vars = $_REQUEST;
	}

	
	/** SETTING FUNCTIONS **/
	public function setDBDateFormat($d) { $this->db_date_format = $d; }
	
	
	
	public function addField($id,$title,$allowchoose=true,$sel=true,$allowfilter=true,$type="TEXT",$data="",$def="",$allowgroupby=true,$allowsortby=true,$sortposition=0) {
		//echo "<P>Add field ::> ".$id;
		$this->fields[$id] = array(
			'id'		=>	$id,
			'heading'	=>	$title,
			'type'		=>	$type,
		);
		if($allowchoose===true) {
			$this->addColumn($id,$sel);
		}
		if($allowfilter===true) {
			$this->addFilter($id,$data,$def);
		}
		if($allowgroupby===true) {
			$this->addGroupBy($id);
		}
		if($allowsortby===true) {
			$this->addSortBy($id);
			$this->setSortPosition($id,$sortposition);
		}
	}
	
	protected function addFieldByArray($f) {
		$this->addField($f['id'],$f['title'],$f['allowchoose'],$f['default_selected'],$f['allowfilter'],$f['type'],$f['data'],$f['default_data'],$f['allowgroupby'],$f['allowsortby'],$f['sortposition']);
	}
	
	protected function addColumn($id,$sel=true) { 
		$this->columns[$id] = array(
			'id'=>$id,
			'selected'=>$sel
		);
	}
	
	protected function addFilter($id,$data,$def) {
		$this->filters[$id] = array(
			'id'=>$id,
			'data'=>$data,
			'default'=>$def,
		);
	}
	
	protected function addGroupBy($id) { $this->group_by[$id] = true; }
	protected function addSortBy($id) { $this->sort_by[$id] = true; }
	protected function setSortPosition($id,$pos) { $this->sort_order[$pos] = $id; }

}


?>