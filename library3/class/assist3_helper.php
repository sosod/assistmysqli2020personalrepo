<?php
/** ASSIST HELPER CLASS
Created on: 2 October 2012
Created by: Janet Currie
Updated on: 3 February 2013 [JC]
Updated on: 6 February 2013 [JC]


General functions to help Ignite Assist







**/
class ASSIST3_HELPER {


	protected $hex = array(
				'0' => '30','1' => '31','2' => '32','3' => '33','4' => '34','5' => '35','6' => '36','7' => '37','8' => '38','9' => '39',
				'A' => '41','B' => '42','C' => '43','D' => '44','E' => '45','F' => '46','G' => '47','H' => '48','I' => '49','J' => '4a',
				'K' => '4b','L' => '4c','M' => '4d','N' => '4e','O' => '4f','P' => '50','Q' => '51','R' => '52','S' => '53','T' => '54',
				'U' => '55','V' => '56','W' => '57','X' => '58','Y' => '59','Z' => '5a',
				'a' => '61','b' => '62','c' => '63','d' => '64','e' => '65','f' => '66','g' => '67','h' => '68','i' => '69','j' => '6a',
				'k' => '6b','l' => '6c','m' => '6d','n' => '6e','o' => '6f','p' => '70','q' => '71','r' => '72','s' => '73','t' => '74',
				'u' => '75','v' => '76','w' => '77','x' => '78','y' => '79','z' => '7a'
			);
	const UNSPECIFIED = "[Unspecified]";
	protected $unspecified = "[Unspecified]";
	public $today;	//a variable to store the current date & time in UNIX timestamp => for data/time comparisons
	protected $nav_buttons_level = 1;	
	
	protected $html_codes = array("b","i","u","li","ul","ol","p");
	
	function __construct() {
		$this->today = strtotime(date("Y-m-d H:i:s"));
	}
	
	public function getUnspecified() { return $this->unspecified; }
	
/* Convert a string to html codes for easy storage in db */
	static function code($str) {
		return htmlentities($str,ENT_QUOTES,"ISO-8859-1");
	}
	
/* Convert a html encoded string back to normal text before displaying in form field */
	static function decode($str) {
		return html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
	}

/* Convert a string with human [] html tags into PC <> html tags */
	function convertToHTML($str) {
		$str = $this->decode($str);
		foreach($this->html_codes as $c) {
			$str = str_replace("[".$c."]","<".$c.">",$str);
			$str = str_replace("[/".$c."]","</".$c.">",$str);
		}
		return $str;
	}

/* Convert a string with PC <> html tags into human [] html tags */
	function convertFromHTML($str) {
		$str = $this->decode($str);
		foreach($this->html_codes as $c) {
			$str = str_replace("<".$c.">","[".$c."]",$str);
			$str = str_replace("</".$c.">","[/".$c."]",$str);
		}
		return $str;
	}	
/* decodes string, removes all spaces and converts to lowercase for easy comparison with another string */
	function stripStringForComparison($s) {
		return str_replace(" ","",strtolower(ASSIST_HELPER::decode($s)));
	}




	
/* echo an array onscreen => development purposes */
	static function arrPrint($a) {
		echo "<pre>"; print_r($a); echo "</pre>";
	}
	
/* Check that a value is a valid PK => numeric & greater than 0 */
	static function checkIntRef($r) {
		if(is_null($r) || strlen($r)==0 || !is_numeric($r) || $r <=0) {
			return false;
		}
		return true;
	}
	
/* check that a folder exists in the client files/cmpcode folder & create if necessary */
	function checkFolder($path,$depth=1) {
		$cmpcode = $this->getCmpCode();
		if(strlen($cmpcode)>0) {
			$me = $_SERVER['PHP_SELF'];
			$myloc = explode("/",$me);
			if(strlen($myloc[0])==0) { unset($myloc[0]); }
			$mc = count($myloc) - 1;
			$path = "files/".$cmpcode."/".$path;
			$chk = explode("/",$path);
			$chkloc = "";
			for($m=1;$m<count($myloc);$m++) { $chkloc.="../"; }
			for($m=1;$m<$depth;$m++) { $chkloc.="../"; }
			foreach($chk as $c)
			{
				if(strlen($c)>0 && $c != "." && $c != "..") {
					$chkloc.= "/".$c;
					if(!is_dir($chkloc)) { mkdir($chkloc); }
				}
			}
		} else {
			die("An error has occurred.  Please go back and try again.");
		}
	}
	
	function saveEcho($folder, $filename, $echo, $depth=1) {
		$cmpcode = $this->getCmpCode();
			//CHECK EXISTANCE OF STORAGE LOCATION
			$this->checkFolder($folder);
			//WRITE DATA TO FILE
			$file_name = "";
			for($i=1;$i<=$depth;$i++) {
				$file_name.="../";
			}
			$file_name.= "files/".$cmpcode."/".$folder."/".$filename;
			$file = fopen($file_name,"w");
			fwrite($file,$echo."\n");
			fclose($file);
	}	
	function downloadFile2($folder, $old, $new, $content) {
		$cmpcode = $this->getCmpCode();
		header("Content-type: $content");
		header("Content-disposition: attachment; filename=$new");
		readfile("../files/".$cmpcode."/".$folder."/".$old);
	}	
	function downloadFile3($old, $new="", $content="application/octet-stream") {
		header("Content-type: $content");
		header("Expires: 0");
header("Cache-Control: ");// leave blank to avoid IE errors
header("Pragma: ");// leave blank to avoid IE errors
		if(strlen($new)>0) {
			header('Content-disposition: attachment; filename="'.$new.'"');
		} else {
			//header("Content-disposition: attachment; ");
		}
		header("Content-Transfer-Encoding: binary ");
		readfile($old);
	}	
	function getFilesLocation($path,$cc) {
		return "../files/".$cc."/".$path."/";
	}
	function downloadFile4($old, $new, $content="application/octet-stream",$isFile=true) {
		$new = (strlen($new)==0 ? $old : $new);
		$content = (strlen($content)==0 ? "application/octet-stream" : $content);
		
		header("Content-type: $content");
		header("Content-disposition: attachment;".(strlen($new)>0 ? " filename=$new" : ""));
		if($isFile) {
			readfile($old);
		} else {
			echo $old;
		}
	}	
	function generateCSVDownloadFile($data,$name) {
		//ORIGINALLY FROM: http://code.stephenmorley.org/php/creating-downloadable-csv-files/
		// output headers so that the file is downloaded rather than displayed
		header("Content-Type: text/csv; charset=utf-8");
		header("Content-Disposition: attachment; filename=$name ");

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		/* // output the column headings
		fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

		// fetch the data
		mysql_connect('localhost', 'username', 'password');
		mysql_select_db('database');
		$rows = mysql_query('SELECT field1,field2,field3 FROM table');

		// loop over the rows, outputting them
		while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);*/
		
		foreach($data as $d) {
			fputcsv($output,$d);
		}
		
	}
	
	
	
	
	
	
	
/** DATA REPORT CODE **/	

	function getFolderSize($folder,$ignore=array()) {
		$fnum = 0;
		$size = array(0=>$folder,1=>0,2=>0);
		$filename = glob($folder."/*");  //arrPrint($filename);
		$ignore[] = "deleted";
		$ignore[] = "reports";
		$files_to_ignore = array("csv","html","xml","Thumbs.db");
		foreach($filename as $file) {
			//echo "<P>".$file;
	//		$f = strFn("explode",$file,"/","");
	//		$f = $f[count($f)-1];
			$fname = explode("/",$file);
			$fname = array_pop($fname);
			if(is_dir($file)) {
				//echo "<br />is dir";
				$fcheck = true;
				foreach($ignore as $ig) {
					if(strtolower($fname)==strtolower($ig)) { //stripos($file,$ig)) {
						$fcheck = false;
						break;
					}
				}
				//echo "<br />fcheck: ".($fcheck?"true":"false");
				if($fcheck) {
					$size[$file] = $this->getFolderSize($file);
				}
			} else {
				//echo "<br />is file";
				$fcheck = true;
				foreach($files_to_ignore as $fig) {
					if(stripos($file,".".$fig) || strtolower($fname)==strtolower($fig)) {
						$fcheck = false;
						break;
					}
				}
				//echo "<br />fcheck: ".($fcheck?"true":"false");
				if($fcheck) {
					$size[1]+=filesize($file);
					$fnum++;
				}
				//echo "<br />fnum: ".$fnum;
			}
			$size[2] = $fnum;
		}
		return $size;
	}
	function displayFolderSize($s,$folder,$moduletitle,$display=true) {
		/*echo "<P>";
		print_r($s);
		echo "-<b>".$folder."</b></p>";*/
		$fn = 0;
		$ts = 0;
		$fs = strlen($folder);
		$f = $s[0];
		//$f = strFn("substr",$f,$fs,strlen($f));
		$f = substr($f,$fs,strlen($f));
		//$fx = explode("/",$f);
		//$f = array_pop($fx);
		$f = str_replace("/"," > ",$f);
		$z = $s[1];
		$ts+=$z;
		$n = $s[2];
		$fn+=$n;
		unset($s[0]);
		unset($s[1]);
		unset($s[2]);
	if($display) {
		echo "<tr>";
			echo "<td style=\"font-weight:bold;\">".$moduletitle.(strlen($f)>0 ? " > ":"").$f."</td>";
			echo "<td style=\"text-align:center;\">".$n."</td>";
			echo "<td style=\"text-align:right;\">".$this->formatBytes($z,2)."</td>";
		echo "</tr>";
	}
		foreach($s as $t) {
			if(is_array($t)) {
				$r=$this->displayFolderSize($t,$folder,$moduletitle,$display);
				$ts+=$r[0];
				$fn+=$r[1];
			}
		}
		$ret = array($ts,$fn);
		return $ret;
	}	
	

	
	
	
	
	
	
	
	
	
	
	
/* PHP code to display the result of an action in jquery widget */
	static function displayResult($result=array()) {
		if(count($result)>0) {
			echo("<div class=\"ui-widget\">");
			if($result[0]=="ok" || $result[0]=="check") {
				echo "<div class=\"ui-state-ok ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
				echo "<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="info") {
				echo "<div class=\"ui-state-info ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em; \">";
				echo "<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="warn") {
				echo "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
				echo "<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>";
			} else {
				echo "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
				echo "<p><span class=\"ui-icon ui-icon-closethick\" style=\"float: left; margin-right: .3em;\"></span>";
			}
			echo "<span id=spn_displayResultText style=''>".stripslashes($result[1])."</span></p></div></div>";
		}
	}
	static function getDisplayResult($result=array()) {
		$data = "";
		if(count($result)>0) {
			$data.="<div class=\"ui-widget\">";
			if($result[0]=="ok" || $result[0]=="check") {
				$data.= "<div class=\"ui-state-ok ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="info") {
				$data.= "<div class=\"ui-state-info ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="warn") {
				$data.= "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>";
			} else {
				$data.= "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-closethick\" style=\"float: left; margin-right: .3em;\"></span>";
			}
			$data.= "<span id=spn_displayResultText>".stripslashes($result[1])."</span></p></div></div>";
		}
		return $data;
	}
	
	static function getFloatingDisplay($result=array(),$position="TR",$size=300,$vertical=10,$horizontal=10) {
		switch($position) {
		case "BR":
			$pos = "bottom: ".$vertical."px; right: ".$horizontal."px;";
			break;
		case "BL":
			$pos = "bottom: ".$vertical."px; left: ".$horizontal."px;";
			break;
		case "TL":
			$pos = "top: ".$vertical."px; left: ".$horizontal."px;";
			break;
		case "TR":
		default:
			$pos = "top: ".$vertical."px; right: ".$horizontal."px;";
			break;
		}
		return "<div class=float style='width: ".$size."px; position: fixed; ".$pos."'>".ASSIST_HELPER::getDisplayResult($result)."</div>"; 
	}
	
	function getDisplayIcon($icon,$options="") {
		switch($icon) {
		case "ok":
			return "
				<span class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png);\" ".$options.">&nbsp;</span>
			";
			break;
		case "error":
			return "
				<span class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png);\" ".$options.">&nbsp;</span>
			";
			break;
		case "info":
			return "
				<span class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png);\" ".$options.">&nbsp;</span>
			";
			break;
		}
	}
	
	static function displayIcon($icon,$options="") {
		switch($icon) {
		case "ok":
			echo "
				<span class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); ".$options."\">&nbsp;</span>
			";
			break;
		case "error":
			echo "
				<span class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); ".$options."\">&nbsp;</span>
			";
			break;
		case "info":
			echo "
				<span class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png);\" ".$options.">&nbsp;</span>
			";
			break;
		}
	}

	static function displayIconAsDiv($icon,$options="") {
		echo self::getDisplayIconAsDiv($icon,$options);
	}
	static function getDisplayIconAsDiv($icon,$options="") {
		if($icon===true || $icon==true) { $icon="1"; } elseif($icon===false || $icon==false) {$icon="0";}
		switch($icon) {
		case "ok":
		case "1":
			return "
				<div class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "error":
		case "0":
			return "
				<div class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "info":
			return "
				<div class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>
			";
			break;
		}
	}

	static function JSdisplayResultPrep($txt="") {
		echo ASSIST_HELPER::getJSdisplayResultPrep($txt);
	}
	static function getJSdisplayResultPrep($txt) {
		return "
			<div class=\"ui-widget\">
				<div id=display_result class=\"ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
					<p>
						<span id=display_result_icon class=\"\" style=\"float: left; margin-right: .3em;\"></span>
						<span id=display_result_text>".$txt."</span>
					</p>
				</div>
			</div>
			<script type=text/javascript>
			function JSdisplayResult(result,icon,txt) { 
				if(result==\"reset\") {
					$(\"#display_result_text\").text('');
					$(\"#display_result_icon\").removeClass();
					$(\"#display_result\").removeClass();
							$(\"#display_result_icon\").removeClass(\"ui-icon-check\"); 
							$(\"#display_result_icon\").removeClass(\"ui-icon-info\"); 
							$(\"#display_result_icon\").removeClass(\"ui-icon-closethick\"); 
							$(\"#display_result\").removeClass(\"ui-state-error\");
							$(\"#display_result\").removeClass(\"ui-state-info\");
							$(\"#display_result\").removeClass(\"ui-state-ok\"); 
					$(\"div.ui-widget\").hide();
				} else if(result.length>0) {
					$(\"div.ui-widget\").show();
					$(\"#display_result_text\").text('');
					$(\"#display_result_icon\").removeClass().addClass(\"ui-icon\");
					$(\"#display_result\").removeClass();
					switch(icon) {
						case \"ok\":		$(\"#display_result_icon\").addClass(\"ui-icon-check\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-ok\"); break;
						case \"info\":	$(\"#display_result_icon\").addClass(\"ui-icon-info\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-info\"); break;
						case \"error\":	$(\"#display_result_icon\").addClass(\"ui-icon-closethick\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-error\"); break;
						case \"reset\":	
							$(\"#display_result_icon\").removeClass(\"ui-icon-check\"); 
							$(\"#display_result_icon\").removeClass(\"ui-icon-info\"); 
							$(\"#display_result_icon\").removeClass(\"ui-icon-closethick\"); 
							$(\"#display_result\").removeClass(\"ui-state-error\");
							$(\"#display_result\").removeClass(\"ui-state-info\");
							$(\"#display_result\").removeClass(\"ui-state-ok\"); 
							break;
						default:		$(\"#display_result_icon\").addClass(\"ui-icon-\"+icon); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-\"+result); break;
					}
					$(\"#display_result_text\").text(txt);
				} else {
					$(\"#display_result_text\").text('');
					$(\"#display_result_icon\").removeClass();
					$(\"#display_result\").removeClass();
					$(\"div.ui-widget\").hide();
				}
			}
			</script>";
	}	
	static function JSdisplayResult($result="",$icon="",$text="") {
		if(strlen($icon)==0) { $icon = $result; }
		$text = stripslashes($text);
		echo "<script type=text/javascript>JSdisplayResult('$result','$icon','$text');</script>";
	}
	
	
/* shortcut to generating options for a select from an array */
	function getOptions($arr,$sel="") {
		$echo = "";
		foreach($arr as $v => $a) {
			$echo.="<option value=$v ".($v==$sel ? "selected" : "").">$a</option>";
		}
		return $echo;
	}
	
/* PHP code to format & display an activity log */
	function displayAuditLog($logs,$flds,$extra="") {
			/* $flds = array('date'=>,'user'=>,'action'=>); */
		if(count($logs)>0) {
			echo "
			<table  width=707 class=noborder style='margin-top: 20px'>
				<tr>
					<td class=noborder width=35%>$extra</td>
					<td class='center noborder' width=30%><h3 class=log id=h3_log_title>Activity log</h3></td>
					<td class=noborder width=35%><span id=disp_audit_log style=\"cursor: hand;\" class=\"float color\"><img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span></span></td>
				</tr>
				<tr>
					<td colspan=3 class=noborder>
						<div id=my_audit_log>
								<table width=700 id=tbl_log>
									<tr>
										<th class=log>Date</th>
										<th class=log>User</th>
										<th class=log>Action</th>
									</tr>";
						foreach($logs as $l) {
							echo "<tr>";
								$a=0;
								foreach($flds as $f) {
									$a++;
									if($a==1) {
										if(!is_numeric($l[$f])) { $d = strtotime($l[$f]); } else { $d = $l[$f]; }
										echo "<td class=\"log centre\">".date("d M Y H:i",$d)."</td>";
									} else {
										echo "<td class=log>".stripslashes(decode($l[$f]))."</td>";
									}
								}
							echo "</tr>";
						}
					echo "</table>";
			echo "</div></td></tr></table>";
			echo "<script type=text/javascript>
					$(function() {
						$('#my_audit_log, #h3_log_title').hide();
						$(\"#disp_audit_log\").click(function() {
							if($('#my_audit_log').is(':visible')) {
								$(\"#my_audit_log, #h3_log_title\").hide();
								$(\"#log_txt\").attr(\"innerText\",\"Display Activity Log\");
								$(\"#log_pic\").attr(\"src\",\"/pics/tri_down.gif\");
							} else {
								$(\"#my_audit_log, #h3_log_title\").show();
								$(\"#log_txt\").attr(\"innerText\",\"Hide Activity Log\");
								$(\"#log_pic\").attr(\"src\",\"/pics/tri_up.gif\");
							}
						});
						$(\"#tbl_log a\").click(function() {
							var u = $(this).attr(\"id\");
							if(u.length>0)
								window.open(u, \"_blank\",\"status=no,toolbar=no,location=no,scrollbars=no,directories=no\");
						});
					});
				</script>";
		} elseif(strlen($extra)>0) {
			echo "
			<table  width=707 class=noborder style='margin-top: 20px'>
				<tr>
					<td class=noborder width=35%>$extra</td>
					<td class='center noborder' width=30%>&nbsp;</td>
					<td class=noborder width=35%>&nbsp;</td>
				</tr>
			</table>";
		}
	}

/* echo the navigation buttons onscreen */	
	function drawNavButtons($menu) { 
		/*** variables:
			$menu[id] = array('id'=>id,'url'=>path_to,'active'=>(bool)checked_or_not,'display'=>heading);
		***/
		echo $this->getNavButtons($menu);
	}
	function setNavButtonsLevel($l) {
		$this->nav_buttons_level = $l;
	}
/* generate navigation buttons */
	function getNavButtons($menu) {
		$echo = "";
		$id = "m".$this->nav_buttons_level;
		switch($this->nav_buttons_level) {
		case 1:
			$echo.= "<div id=".$id." style=\"font-size: 8pt; font-weight: bold;\">";
			foreach($menu as $m) {
				$key = $m['id'];
				$echo.= "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" ".($m['active']==true || $m['active']=="Y" ? "checked=checked": "")." ".( (isset($m['help']) && strlen($m['help'])>0) ? " help_text=\"".$m['help']."\" " : "")." />";
				$echo.= "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
			}
			$echo.= "</div>";
			break;
		case 2:
		default:
			$echo.= "<div id=".$id." style=\"padding-top: 5px; font-size: 7pt\">";
			foreach($menu as $m) {
				$key = $m['id'];
				$echo.= "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" ".($m['active']==true || $m['active']=="Y" ? "checked=checked" : "")." ".( (isset($m['help']) && strlen($m['help'])>0) ? " help_text=\"".$m['help']."\" " : "")." />";
				$echo.= "<label for=\"$key\" style=\"background: #fafaff url();margin-right: 3px;\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
			}
			$echo.= "</div>";
			break;
		}
		$echo.= "<script type=text/javascript>
			  $(function() {
				$(\"#".$id."\").buttonset();
				$(\"#".$id." input[type='radio']\").click( function() {
				  document.location.href = $(this).val();
				}).children('.ui-button-hover').hover(
					function() {
						var h = $(this).attr('id');
						alert(h);
						if(h.length>0) {
							AssistHelper.showHelp(h);
						}
					},
					function() {
						AssistHelper.hideHelp();
					}
				);
				
			  });
		</script>";
		$this->nav_buttons_level++;
		return $echo;
	}	//end drawNav($nav)	

/* get the Business PArtner contact details & store in session */	
	function getBPAdetails($bpa = "") {
		$bpa = strlen($bpa)>0 ? $bpa : $_SESSION['ia_cmp_reseller'];

		if(!isset($_SESSION['bpa_details']) || strlen($bpa)>0) {
			$db = new ASSIST_DB("master");
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r 
					INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
					WHERE r.cmpcode = '".strtoupper($bpa)."'";
			$bpa_details = $db->mysql_fetch_one($sql);
			unset($db);
				$bpa_con = explode("|",$bpa_details['help_contact']);
				$bpa_contact = array();
				foreach($bpa_con as $b) {
					$b2 = explode("_",$b);
					$bpa_contact[] = $b2[1]." (".strtolower($b2[0]).")";
				}
				$bpa_details['contact'] = $bpa_contact;
				$_SESSION['bpa_details'] = $bpa_details;
				if(isset($bpa_details['assist_logo']) && strlen($bpa_details['assist_logo'])>0) {
					$_SESSION['DISPLAY_INFO']['title_logo'] = "pics/bp_logos/".$bpa_details['assist_logo'];
				}
				if(isset($bpa_details['site_name']) && strlen($bpa_details['site_name'])>0) {
					$_SESSION['DISPLAY_INFO']['ignite_name'] = $bpa_details['site_name'];
				}
		} else {
			$bpa_details = $_SESSION['bpa_details'];
		}
		//$this->arrPrint($bpa_details);
		return $bpa_details;
	}
	function getReseller($cmpcode) {
		$db = new ASSIST_DB("master");
		$sql = "SELECT cmpreseller FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";
		
		$row = $db->mysql_fetch_one($sql);
		return $row['cmpreseller'];
	}

/* display go back triangle onscreen */
	function displayGoBack($url = "",$txt = "Go Back") {
		echo "<p>".$this->getGoBack($url,$txt)."</p>";
	}
	static function goBack($url="",$txt="Go Back") {
		if(strlen($url)>0) {
			echo "<a href=$url /><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a>";
		} else {
			echo "<a href=\"javascript:history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=\"javascript:history.back();\">Go Back</a>";
		}
	}

/* generate the go back triangle */
	function getGoBack($url = "",$txt = "Go Back") {
		if(strlen($txt)==0) { $txt = "Go Back"; }
		if(strlen($url)>0) {
			return "<a href=$url /><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a>";
		} else {
			return "<a href=\"javascript:history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=\"javascript:history.back();\">Go Back</a>";
		}
	}

/* generate & display the go up triangle */
	function displayGoUp($url, $txt = "Back to Top") {
		if(strlen($url)>0) {
			echo "<p><a href=$url /><img src=\"/pics/tri_up.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a></p>";
		}
	}

/* get home/frontpage user profile */
	function getUserProfile($cmpcode="",$tkid="") {
		$max_profile = 3; //last profile section reference
		if(strlen($cmpcode)==0 || strlen($tkid)==0) {
			$assist = new ASSIST();
			$cmpcode = $assist->getCmpCode();
			$tkid = $assist->getUserID();
		}
		$db = new ASSIST_DB("client",$cmpcode);
		$_SESSION['USER_PROFILE'] = array();
		$field5_serials = array(2,3);
		$sql = "SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE tkid = '$tkid' AND active = 1 ORDER BY section ASC";
		$profile = $db->mysql_fetch_all_fld($sql,"section");
		
		for($p=1;$p<=$max_profile;$p++) {
			if(isset($profile[$p])) {
				$prf = $profile[$p];
				if(in_array($p,$field5_serials)) {
					$f5 = $prf['field5'];
					$prf['field5'] = unserialize($f5);
				}
				$_SESSION['USER_PROFILE'][$p] = $prf;
			} else {
				switch($p) {
				case 1:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,			'tkid' => $tkid,			'active' => true,			'section' => 1,			'field1' => "",			'field2' => "15",			'field3' => "dead_asc",			'field4' => 7,			'field5' => "",
					);
					break;
				case 2:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,	'tkid' => $tkid,	'active' => true,	'section' => 2,	'field1' => "MIN",	'field2' => "",	'field3' => "",	'field4' => "",	'field5' => array(),
					);
					break;
				case 3:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,	'tkid' => $tkid,	'active' => true,	'section' => 3,	'field1' => "",	'field2' => "",	'field3' => "",	'field4' => "",	'field5' => array(),
					);
					break;
				default:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,	'tkid' => $tkid,	'active' => true,	'section' => $p,	'field1' => "",	'field2' => "",	'field3' => "",	'field4' => "",	'field5' => array(),
					);
					break;
				}
			}//endif profile exists for section
		}//end for each profile section
	} //end getUserProfile function

	
	
	
	static function getModuleLogoDetails() {
		$shortcut_details = array();
		$shortcut_details['path'] = "../pics/module/";
		$shortcut_details['logos'] = array(
			'CAPS'	=> "2013caps.png",
			'ACTION'=> "2013action.png",
			'JOBS'=> "2013action.png",
			'SDBP4'	=> "sdbp4.png",
			'SDBP5'	=> "2013sdbp5.png",
			'DOC'	=> "2013doc.png",
			'TD'	=> "td.png",
			'DSHP'	=> "dshp.png",
			'USER_MANUALS'	=> "manuals.png",
			'TK'	=> "td.png",
			'PM'	=> "pm.png",
			'COMPL'=>"2013compl.png",
			'QUERY'=>"2013query.png",
			'CASE'=>"2013query.png",
			'RGSTR'=>"2013rgstr.png",
			'RGSTR_ALT'=>"2014rgstr_alt.png",
			'RAPS'=>"2013raps.png",
			'S'=>"2013setup.png",
			'HP'=>"hp.png",
			'NB'=>"noticeboard2014.png",
			'CONTRACT'	=>	"contract2014.png",
			'SCORECARD'	=>	"scorecard2014.png",
		);
		$shortcut_details['default'] = "circle.png";
		$shortcut_details['new'] = "add_new.png";
		return $shortcut_details;
	}
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***************************************
			PAGE HEADERS
	****************************************/
	
	
	

	function displayPageHeader($jquery=true,$amcharts=true,$base_target="main",$top=0,$right=5,$bottom=0,$left=5) {
		$jquery_version = isset($_REQUEST['jquery_version']) && strlen($_REQUEST['jquery_version'])>0 ? $_REQUEST['jquery_version'] : "1.8.24";
		echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Ignite4u.co.za</title>
	
	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js"></script>' : '').'

	<link rel="stylesheet" href="/assist.css" type="text/css">
	<script type ="text/javascript" src="/assist.js"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js"></script>
	<script type ="text/javascript" src="/library/js/assistform.js"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js"></script>
</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
	}


	
	
	function displayPageHeaderJQ($jquery_version="1.10.0",$jquery=true,$amcharts=true,$base_target="main",$top=0,$right=5,$bottom=0,$left=5) {
		echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Ignite4u.co.za</title>
	
	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js"></script>' : '').'

	<link rel="stylesheet" href="/assist.css" type="text/css">
	<script type ="text/javascript" src="/assist.js"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js"></script>
	<script type ="text/javascript" src="/library/js/assistform.js"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js"></script>
</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
	}

	
	static function echoPageHeader($jquery_version="1.10.0",$scripts=array(),$css=array()) {
		$jquery=true; $amcharts=true; $base_target="main"; $top=0; $right=5; $bottom=0; $left=5;
		echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Ignite4u.co.za</title>
	
	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js"></script>' : '').'

	<link rel="stylesheet" href="/assist.css" type="text/css">
	<script type ="text/javascript" src="/assist.js"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js"></script>
	<script type ="text/javascript" src="/library/js/assistform.js"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js"></script>';
	if(is_array($scripts) && count($scripts)>0) {
		foreach($scripts as $s) {
			echo '
			<script type ="text/javascript" src="'.$s.'"></script>';
		}
	}
	if(is_array($css) && count($css)>0) {
		foreach($css as $c) {
			echo '
			<link rel="stylesheet" href="'.$c.'" type="text/css">';
		}
	}
echo '
	</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
	}



	/********
		FUNCTIONS TO FORMAT NUMBERS 
			********/
	const NUMBER_DEFAULT_TYPE = "FLOAT";
	const NUMBER_DECIMAL_PLACES = 2;
	const NUMBER_THOUSANDS = true;
	const NUMBER_CURRENCY_SYMBOL = "R";
	const NUMBER_PERCENT_SYMBOL = "%";
	const NUMBER_THOUSANDS_SEPARATOR = " ";

	static function format_number($n,$type=self::NUMBER_DEFAULT_TYPE,$dec=self::NUMBER_DECIMAL_PLACES,$thou=self::NUMBER_THOUSANDS,$symbol="") {
		if(!isset($types[$type])) {
			$types = array(
				'FLOAT'=>array("FLOAT","DOUBLE","CALC"),
				'INT'=>array("INT","WHOLE"),
				'CURRENCY'=>array("CURR","CURRENCY","RAND","RANDS"),
				'PERC'=>array("PERC"),
			);
			foreach($types as $key=>$arr) {
				if(in_array(strtoupper($type),$arr)) { $type = $key; break; }
			}
		}
		if($thou===true) { $thou_sep = self::NUMBER_THOUSANDS_SEPARATOR; } else { $thou_sep = ''; }
		switch($type) {
		case "INT":
			return number_format($n,0,'.',$thou_sep);
			break;
		case "FLOAT":
		case "PERC":
		case "CURRENCY":
		default:
			$x = number_format($n,$dec,'.',$thou_sep);
			if($type=="CURRENCY") { 
				$x = (strlen($symbol)==0 ? self::NUMBER_CURRENCY_SYMBOL : $symbol)." ".$x;
			} elseif($type=="PERC") {
				$x .=(strlen($symbol)==0 ? self::NUMBER_PERCENT_SYMBOL : $symbol);
			}
			return $x;
			break;
		}
	}
	
	static function format_percent($n,$d=2) { 
		return self::format_number($n,"PERC",$d); 
	}
	static function format_currency($n,$curr=self::NUMBER_CURRENCY_SYMBOL) { 
		return self::format_number($n,"CURRENCY",self::NUMBER_DECIMAL_PLACES,self::NUMBER_THOUSANDS,$curr); 
	}
	static function format_integer($n) { 
		return self::format_number($n,"INT"); 
	}
	static function format_float($n) { 
		return self::format_number($n); 
	}	
	
	
	/** END NUMBER FORMATTING FUNCTIONS **/
	
	
		
	function formatBytes($b,$p = null) {
		/**
		 * 
		 * @author Martin Sweeny
		 * @version 2010.0617
		 * 
		 * returns formatted number of bytes. 
		 * two parameters: the bytes and the precision (optional).
		 * if no precision is set, function will determine clean
		 * result automatically.
		 * 
		 **/
		$units = array("B","kB","MB","GB","TB","PB","EB","ZB","YB");
		$c=0;
		if(!$p && $p !== 0) {
			foreach($units as $k => $u) {
				if(($b / pow(1024,$k)) >= 1) {
					$r["bytes"] = $b / pow(1024,$k);
					$r["units"] = $u;
					$c++;
				}
			}
			return number_format($r["bytes"],2) . " " . $r["units"];
		} else {
			return number_format($b / pow(1024,$p),2) . " " . $units[$p];
		}
	}

	
	
	/** ticks and crosses **/
	static function drawStatus($s,$st="") {
		if($s===true || ":".$s.":"==":1:") { $s="Y"; } elseif($s===false || ":".$s.":"==":0:") { $s="N"; }
		switch($s) {
			case "Y":
				$icon = "circle-check"; 
				$style = "yes"; 
				break;
			case "info":
				$icon = "circle-minus";
				$style = "nothing";
				break;
			case "warn":
				$icon = "alert";
				$style = "fail";
				break;
			case "lock":
				$icon = "locked";
				$style = "nothing";
				break;
			case "unlock":
				$icon = "unlocked";
				$style = "yes";
				break;
			case "N":
				$style = "fail"; $icon = "circle-close"; 
				break;
			default:
				$icon = $s; 
				$style = (strlen($st)>0 ? $st : $s); 
				echo "else".$style; 
				break;
			}
		$return = "<div style='width: 16px; margin: 0 auto;' class='ui-widget ui-state-".$style."'><span class='ui-icon ui-icon-".$icon."'>&nbsp;&nbsp;</span></div>";
		return $return;
	}	

	static function dateToInt($d) {
		$d = strtotime($d);
		if(intval(date("H",$d))==0) {
			$d+=12*60*60;
		}
		return $d;
	}
	
	
		/**
	 * Function to convert array into string for SQL statement
	 * 
	 * @param (Array) var = array ('db field'=>"value")
	 * @param (String) join = glue for implode, default = , for insert
	 * @return (String) field = 'value', field2 = 'value2'
	 */
    public function convertArrayToSQL($var,$join=",") {
    	$insert_data = array();
		foreach($var as $fld => $v){
			$insert_data[] = $fld." = '".$v."'";
		}
		return implode(" ".$join." ",$insert_data);
    }
	
	
	
	
	/* function to convert a string to its hex value - used for passwords */
	public function convertToHex($pa,$error_value = "_") {
		$pb = "";
		for($a=0;$a<strlen($pa);$a++) {
			$alpha = substr($pa,$a,1);
			$pb.= isset($this->hex[$alpha]) ? $this->hex[$alpha] : $error_value;
		}		
		return $pb;
	}	
	
	
	
	function __destruct() {}
}


?>