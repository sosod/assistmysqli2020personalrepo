<?php
/*******************
Class to manage the generation of email notifications by modules on Assist.

Created by: Janet Currie
Created on: 25 August 2014
*********************/


class ASSIST_MODULE_EMAIL extends ASSIST_HELPER {

	private $assist;
	private $assist_email;
	private $email_type;
	
	private $object_name;
	private $ref;
	private $reftag;
	private $child_name;
	private $parent_name;
	private $gparent_name;
	private $activity_names;

	public function __construct($on="",$rt="",$modloc="") {
		parent::__construct();
		$this->assist = new ASSIST();
		$this->assist_email = new ASSIST_EMAIL();
		$this->helper = new ASSIST_HELPER();
		$this->object_name = $on;
		switch(strtoupper($on)){
			case "ACTION":
				$this->parent_name = "Deliverable";
				$this->gparent_name = "Contract";
				break;
			case "DELIVERABLE":
				$this->parent_name = "Contract";
				break;
			case "CONTRACT":
				$this->parent_name = $on;
				$this->child_name = "Deliverable";
				break;
			default:
				$this->parent_name = $on;
				$this->child_name = "Deliverable";
				break;
		}		
		switch (strtoupper($modloc)) {
			case 'CNTRCT':
				$help = new CNTRCT_HELPER();
				break;
			default:
				$help = new ASSIST_MODULE_HELPER();
				break;
		}
		$this->activity_names = $help->getAllActivityNames();
		$this->reftag = $rt;
		//mail("duncancosser@gmail.com","Assist mail from construct","MESSAGE from MODULE_EMAIL!");
	}
	public function changeObject($on="",$rt="") {
		if(strlen($on)>0) { $this->object_name = $on; }
		if(strlen($rt)>0) { $this->reftag = $rt; }
	}
	
	public function getActivityName($key){
		if(isset($this->activity_names[$key])){
			return $this->activity_names[$key];
		}else{
			return $key;
		}
	}
	
	
	
	
	
	
	/**************************
	sendNewEmail = when an object is created that DOES NOT REQUIRE Activation
	e.g. New Task / Resolution / Complaint and Risk/Query/Compliance/Contract object created in Admin
	-------------Variables-------------
	$ref = the system generated id of the object
	$t = array of user ids (tkid) of the object owners responsible for updating the object
	$c = array of user ids (tkid) of the owners of the parent object or module admin if applicable ELSE blank array
	$details = array of field headings/names and values e.g. array('Description'=>"Update accumulated leave days at the end of the month",'Deadline'=>"30-Aug-2014")
	$parent_details = array of SUMMARY of any parents up to the primary parent with the first array element is the object name of the parent, the second is the parent object ref (including reftag) and the third is an array of some of the parent object's details - enough to identify the parent object easily e.g. parent object ref (including reftag) and name/description field.
			NOTE: Parent details must include at a minimum:
				+ Financial Year if directly related
				+ System Ref with Reftag
				+ Name / Short description
				+ Owner / Responsible Person / Directorate
				+ Deadline date (if applicable)
		e.g. array(
				0=>array("Deliverable","D1",array('Ref'=>"D1",'Short Description'=>"All leave days must be documented.",'Deliverable Responsible Person'=>"Ian Technology",'Deliverable Deadline Date'=>"30-Sep-2014")),
				1=>array("Legislation","L1",array('Financial Year'=>"Year Ending 30 June 2015",'Ref'=>"L1",'Legislation Name'=>"Employment Equity Act"))
			);
			
	------------EXAMPLE OF RESULTING EMAIL FROM COMPLIANCE------------------
		Subject: New Action [A100]
		Message: 
			Mike Billing as created a new Action related to Deliverable D50 of Legislation L2
			
			The Action details are as follows:
			Action Ref: A100
			Action Required: The accounting officer of a municipality text text text text text.
			Action Deliverable: The accounting officer must submit, in writing, text text text text text.
			Action Owner: Carol Jetson
			Action Deadline: 30-Sep-2014
			Action Status: New
			Action Progress: 0%
			
			The related Deliverable details are as follows:
			Deliverable Ref: D50
			Deliverable Short Description: The accounting officer of a municipality text text text text text.
			Deliverable Responsible Person: Ian Technology
			Deliverable Legislative Deadline Date: 30-Sep-2014
			
			The related Legislation details are as follows:
			Financial Year: Year ending 30-June-2015 (2014-07-01 - 2015-06-30)
			Legislation Ref: L2
			Legislation Name: Municipal Financial Management Act
			
			Please log onto Assist in order to View or Update this Action.
	--------------------------------------------------------------------------
			
	*************************/
	
	public function sendNewEmail($ref,$t,$c,$details,$parent_details=array()) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "New ".$this->object_name." ".$this->reftag.$ref." [".$this->assist->getModTitle()."]";
			
			$parent = $this->getParentDetails($parent_details);
			
			$message = $this->assist->getUserName()." has created a new ".$this->object_name.$parent['related'].".

The ".$this->object_name." details are as follows:";
			foreach($details as $heading=>$value) {
				$message.="
-".$this->decode($heading).": ".$this->decode($value)."";
				}
				$message.=$this->decode($parent['message'])."
				
Please log onto Assist at https://assist.action4u.co.za in order to View or ".$this->getActivityName("update")." this ".$this->object_name." on ".$this->assist->getModTitle().".";

/*
//Testing recipients
foreach($emails['to'] as $key=>$val){
	$rec[] = implode(" - ",$val);
}
$message.="

Recipients:".implode(", ",$rec).".";
*/
  
			//$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setRecipient('duncancosser@gmail.com');
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	/**************************
	sendConfirmationEmail = when an object is Confirmed in New and ready for Activation
	
	-------------Variables-------------	
	$ref = the system generated id of the object
	$t = array of user ids (tkid) of the users responsible for activating the object
	$c = array of user ids (tkid) of the module admin if applicable
	$name = name/short description of the object 
			
	------------EXAMPLE OF RESULTING EMAIL FROM COMPLIANCE------------------
		Subject: New Legislation L2 - Ready for Activation
		Message: 
			Mike Billing has confirmed Legislation L2: Municipal Financial Management Act and it is now ready to be activated.
			
			Please log onto Assist in order to Activate this Legislation.
	--------------------------------------------------------------------------
			
	*************************/
	public function sendConfirmationEmail($ref,$t,$c) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "New ".$this->object_name." ".$this->reftag.$ref." - Ready for ".$this->getActivityName("activation")." [".$this->assist->getModTitle()."]";
			$message = $this->assist->getUserName()." has ".$this->getActivityName("confirmed")." ".$this->object_name." ".$this->reftag.$ref." and it is now ready to be ".$this->getActivityName("activated").".

Please log onto Assist at https://assist.action4u.co.za in order to ".$this->getActivityName("activate")." this ".$this->object_name." on ".$this->assist->getModTitle().".";
			//$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setRecipient('duncancosser@gmail.com');
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}





	
	/**************************
	sendActivationEmail = when an object is Activated in New and ready for Manage
	
	-------------Variables-------------	
	$ref = the system generated id of the object
	$t = array of user ids (tkid) of the object owners responsible for updating the object & any associated child objects
	$c = array of user ids (tkid) of the module admin if applicable & any associated responsible people not directly responsible for updating the object/child objects
	$name = name/short description of the object
	$child = object name of the child object(s) - IN PLURAL FORM
			
	------------EXAMPLE OF RESULTING EMAIL FROM COMPLIANCE------------------
		Subject: New Legislation [L2]
		Message: 
			Mike Billing has activated Legislation L2: Municipal Financial Management Act.
			
			There are Actions/Deliverables associated with this Legislation which have been assigned to you.
			
			Please log onto Assist in order to View or Update these Actions/Deliverables.
	--------------------------------------------------------------------------
			
	*************************/
	public function sendActivationEmail($ref,$t,$c,$name,$children) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "New ".$this->object_name." ".$this->reftag.$ref." [".$this->assist->getModTitle()."]";
			$message = $this->assist->getUserName()." has ".$this->getActivityName("activated")." ".$this->object_name." ".$this->reftag.$ref.": ".$name."
		
There are ".$children." associated with this ".$this->object_name." which have been assigned to you.

Please log onto Assist at https://assist.action4u.co.za in order to View or ".$this->getActivityName("update")." these ".$children." on ".$this->assist->getModTitle().".";
			//$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setRecipient('duncancosser@gmail.com');
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}

	/**
	 * Function is a clone of activation emails - just to tell the usr that the activation has been reversed
	 */

	public function sendDeactivationEmail($ref,$t,$c) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "New ".$this->object_name." ".$this->reftag.$ref." -  ".$this->getActivityName("deactivation")." [".$this->assist->getModTitle()."]";
			$message = $this->assist->getUserName()." has ".$this->getActivityName("deactivated")." ".$this->object_name." ".$this->reftag.$ref.". It is no longer avaliable in Manage and Admin.

Please log onto Assist at https://assist.action4u.co.za in order to ".$this->getActivityName("edit")." this ".$this->object_name." (or its associated ".$this->child_name."s) on ".$this->assist->getModTitle().".";
			//$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setRecipient('duncancosser@gmail.com');
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	/**************************
	sendEditEmail = when an object is edited in Manage/Admin
	
	-------------Variables-------------	
	$ref = the system generated id of the object EXCLUDING Reftag
	$t = array of user ids (tkid) of the object owners responsible for updating the object
	$c = array of user ids (tkid) of the owners of the parent object or module admin if applicable
	$changes = array of changes as per Activity Log e.g. array(0=>"Action Deadline changed to 30-Sep-2014 from 30-August-2014",1=>"Action Owner changed to Carol Jetson from Ian Technology")
	$details = array of ALL field headings/names [array index] and current values [array element] (after edit) e.g. array('Description'=>"Update accumulated leave days at the end of the month",'Deadline'=>"30-Sep-2014")
	$parent_details = array of SUMMARY of any parents up to the primary parent with the first array element is the object name of the parent, the second is the parent object ref (including reftag) and the third is an array of some of the parent object's details - enough to identify the parent object easily e.g. parent object ref (including reftag) and name/description field.
			NOTE: Parent details must include at a minimum:
				+ Financial Year if directly related
				+ System Ref with Reftag
				+ Name / Short description
				+ Owner / Responsible Person / Directorate
				+ Deadline date (if applicable)
		e.g. array(
				0=>array("Deliverable","D1",array('Ref'=>"D1",'Short Description'=>"All leave days must be documented.",'Deliverable Responsible Person'=>"Ian Technology",'Deliverable Deadline Date'=>"30-Sep-2014")),
				1=>array("Legislation","L1",array('Financial Year'=>"Year Ending 30 June 2015",'Ref'=>"L1",'Legislation Name'=>"Employment Equity Act"))
			);
			
			
	------------EXAMPLE OF RESULTING EMAIL FROM COMPLIANCE------------------
		To: Carol Jetson [role of Action Owner]
		CC: Ian Technology [role of Deliverable Responsible Person]
		Subject: Action A100 Edited
		Message: 
			Mike Billing has made changes to Action A100 related to Deliverable D50 of Legislation L2
			
			The changes are:
			- Action Deadline changed to 30-Sep-2014 from 30-Aug-2014
			- Action Owner changed to Carol Jetson from Ian Technology
			
			The Action details are as follows:
			Action Ref: A100
			Action Required: The accounting officer of a municipality text text text text text.
			Action Deliverable: The accounting officer must submit, in writing, text text text text text.
			Action Owner: Ian Technology
			Action Deadline: 30-Sep-2014
			Action Status: In Progress
			Action Progress: 10%
			
			The related Deliverable details are as follows:
			Deliverable Ref: D50
			Deliverable Short Description: The accounting officer of a municipality text text text text text.
			Deliverable Responsible Person: Ian Technology
			Deliverable Legislative Deadline Date: 30-Sep-2014
			
			The related Legislation details are as follows:
			Financial Year: Year ending 30-June-2015 (2014-07-01 - 2015-06-30)
			Legislation Ref: L2
			Legislation Name: Municipal Financial Management Act
			
			Please log onto Assist in order to View or Update this Action.
	--------------------------------------------------------------------------
			
	*************************/
	public function sendEditEmail($ref, $t, $c, $changes, $details, $parent_details=array()) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "".$this->object_name." ".$this->reftag.$ref." Edited [".$this->assist->getModTitle()."]";
			
			$parent = $this->getParentDetails($parent_details);
			
			$message = $this->assist->getUserName()." has made changes to ".$this->object_name." ".$this->reftag.$ref.$this->decode($parent['related']).".

The changes are:";
			foreach($changes as $c) {
				$message.="
- ".$this->decode($c)."";
			}
			$message.="
			
The ".$this->object_name." details are as follows:";
			foreach($details as $heading=>$value) {
				$message.="
-".$this->decode($heading).": ".$this->decode($value)."";
				}
				$message.=$this->decode($parent['message'])."
				
Please log onto Assist at https://assist.action4u.co.za in order to View this ".$this->object_name." on ".$this->assist->getModTitle().".";

			//Swap the first two assist email calls to send to real addresses.
			$this->assist_email->setRecipient('duncancosser@gmail.com');
//			$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			
		
			//return $emails['to'];
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**************************
	sendUpdateEmail = when an object is updated in Manage/Admin
	
	-------------Variables-------------	
	$ref = the system generated id of the object EXCLUDING Reftag
	$t = array of user ids (tkid) of the responsible person(s) of the parent object AND the object owner if the user updating is NOT the owner (i.e. updated in Admin) 
	$c = module admin if applicable
	$changes = array of updates as per Activity Log e.g. array(0=>"Added Response: Accumulated leave days calculated for 50% of employees.  Rest to follow by tomorrow.",1=>"Status changed to In Progress from New",2=>"Progress changes to 50% from 0%",3=>"Action On changed to 26-Aug-2014 from -")
	$details = array of ALL field headings/names [array index] and current values [array element] e.g. array('Description'=>"Update accumulated leave days at the end of the month",'Deadline'=>"30-Sep-2014")
	$parent_details = array of SUMMARY of any parents up to the primary parent with the first array element is the object name of the parent, the second is the parent object ref (including reftag) and the third is an array of some of the parent object's details - enough to identify the parent object easily e.g. parent object ref (including reftag) and name/description field.
			NOTE: Parent details must include at a minimum:
				+ Financial Year if directly related
				+ System Ref with Reftag
				+ Name / Short description
				+ Owner / Responsible Person / Directorate
				+ Deadline date (if applicable)
		e.g. array(
				0=>array("Deliverable","D1",array('Ref'=>"D1",'Short Description'=>"All leave days must be documented.",'Deliverable Responsible Person'=>"Ian Technology",'Deliverable Deadline Date'=>"30-Sep-2014")),
				1=>array("Legislation","L1",array('Financial Year'=>"Year Ending 30 June 2015",'Ref'=>"L1",'Legislation Name'=>"Employment Equity Act"))
			);
			
			
	------------EXAMPLE OF RESULTING EMAIL FROM COMPLIANCE------------------
		To: Ian Technology [role of Deliverable Responsible Person]; Carol Jetson [role of Action Owner]
		CC: 
		Subject: Action A100 Updated
		Message: 
			Mike Billing has made an update to Action A100 related to Deliverable D50 of Legislation L2
			
			The update is:
			- Added Response: Accumulated leave days calculated for 50% of employees.  Res to follow by tomorrow.
			- Status changed to In Progress from New
			- Progress changed to 50% from 0%
			- Action On changed to 26-Aug-2014 from -
			
			The Action details are as follows:
			Action Ref: A100
			Action Required: Update accumulated leave days at the end of the month
			Action Deliverable: Leave days updated on HR software
			Action Owner: Carol Jetson
			Action Deadline: 30-Sep-2014
			Action Status: In Progress
			Action Progress: 50%
			
			The related Deliverable details are as follows:
			Deliverable Ref: D50
			Deliverable Short Description: All employees shall have access to current details regarding their accumulated leave days at all times.
			Deliverable Responsible Person: Ian Technology
			Deliverable Legislative Deadline Date: 30-Sep-2014
			
			The related Legislation details are as follows:
			Financial Year: Year ending 30-June-2015 (2014-07-01 - 2015-06-30)
			Legislation Ref: L2
			Legislation Name: Employment Equity Act
			
			Please log onto Assist in order to View this Action.
	--------------------------------------------------------------------------
			
	*************************/	
	public function sendUpdateEmail($ref, $t, $c, $changes, $details, $parent_details=array(), $parent_formatted=array(),$gparent_formatted=array()) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "".$this->object_name." ".$this->reftag.$ref." ".$this->getActivityName("updated")." [".$this->assist->getModTitle()."]";
			$parent = $this->getParentDetails($parent_details);
			$message = $this->assist->getUserName()." has made an ".$this->getActivityName("update")." to ".$this->object_name." ".$this->reftag.$ref.$this->decode($parent['related']).".

The ".$this->getActivityName("update")." is:";
			foreach($changes as $c) {
				$message.="
- ".$this->decode($c)."";
			}
			$message.="
			
The ".$this->object_name." details are as follows:";
			foreach($details as $heading=>$value) {
				$message.="
-".$this->decode($heading).": ".$this->decode($value)."";
				}
				$message.=$this->decode($parent['message'])."
				
Please log onto Assist at https://assist.action4u.co.za in order to View this ".$this->object_name." on ".$this->assist->getModTitle().".";

/*
//Testing recipients
foreach($emails['to'] as $key=>$val){
	$rec[] = implode(",",$val);
}
$message.="
RECIPIENTS:".implode(",",$rec).".";

*/
			//email testing, change #1 to #0 when finished!	
//	$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setRecipient("duncancosser@gmail.com");
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			//mail('duncancosser@gmail.com', "IT WORKS OMG", 'lol.');
			return true;
	//		return $emails['to'];
		}
		return false;
	}
	
	
	
	
	
	
	
	
	/**************************
	sendDeleteEmail = when an object is deleted in Admin
	
	-------------Variables-------------	
	$ref = the system generated id of the object EXCLUDING Reftag
	$t = array of user ids (tkid) of the responsible person(s) of the parent object AND the object owner if the user updating is NOT the owner (i.e. updated in Admin) 
	$c = module admin if applicable
	$changes = array of updates as per Activity Log e.g. array(0=>"Added Response: Accumulated leave days calculated for 50% of employees.  Rest to follow by tomorrow.",1=>"Status changed to In Progress from New",2=>"Progress changes to 50% from 0%",3=>"Action On changed to 26-Aug-2014 from -")
	$details = array of ALL field headings/names [array index] and current values [array element] e.g. array('Description'=>"Update accumulated leave days at the end of the month",'Deadline'=>"30-Sep-2014")
	$parent_details = array of SUMMARY of any parents up to the primary parent with the first array element is the object name of the parent, the second is the parent object ref (including reftag) and the third is an array of some of the parent object's details - enough to identify the parent object easily e.g. parent object ref (including reftag) and name/description field.
			NOTE: Parent details must include at a minimum:
				+ Financial Year if directly related
				+ System Ref with Reftag
				+ Name / Short description
				+ Owner / Responsible Person / Directorate
				+ Deadline date (if applicable)
		e.g. array(
				0=>array("Deliverable","D1",array('Ref'=>"D1",'Short Description'=>"All leave days must be documented.",'Deliverable Responsible Person'=>"Ian Technology",'Deliverable Deadline Date'=>"30-Sep-2014")),
				1=>array("Legislation","L1",array('Financial Year'=>"Year Ending 30 June 2015",'Ref'=>"L1",'Legislation Name'=>"Employment Equity Act"))
			);
			
			
	------------EXAMPLE OF RESULTING EMAIL FROM COMPLIANCE------------------
		To: Ian Technology [role of Deliverable Responsible Person]; Carol Jetson [role of Action Owner]
		CC: 
		Subject: Deliverable D50 Deleted
		Message: 
			Deliverable D50, related to Legislation L2, has been deleted by Mike Billing.
			Financial Year: Year ending 30 June 2015
			Legislation Name: Employment Equity Act
			Deliverable Short Description: All employees shall have access to current details regarding their accumulated leave days at all times.
			
			The associated Actions also deleted are:
			- A100: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-Jul-2014]
			- A101: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-Aug-2014]
			- A102: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 30-Sep-2014]
			- A103: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-Oct-2014]
			- A104: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 30-Nov-2014]
			- A105: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-Dec-2014]
			- A106: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-Jan-2015]
			- A107: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 28-Feb-2015]
			- A108: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-Mar-2015]
			- A109: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 30-Apr-2015]
			- A110: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 31-May-2015]
			- A111: Update accumulated leave days by the end of the month [Assigned to Carol Jetson, Deadline: 30-Jun-2015]
			
			
			
		FULL OBJECT DELETION	
			--------PARENT EMAIL NOTIFICATION-------
			To: Legislation Responsible Person
			Message:
			Legislation L2 has been deleted by Mike Billing.  
			
			The Legislation Details are:
			Financial Year: Year ending 30-June-2015
			Legislation Name: Employment Equity Act
			
			The associated Deliverables also deleted are:
			- D1: this is my short description [Assigned to: Ian Technology]
			- D2: this is my short description [Assigned to: Carol Jetson]
			- D3: this is my short description [Assigned to: Carol Jetson]
			- D4: this is my short description [Assigned to: Ian Technology]
			- D5: this is my short description [Assigned to: Carol Jetson]
			- D6: this is my short description [Assigned to: Ian Technology]
			- D7: this is my short description [Assigned to: Carol Jetson]
			- D8: this is my short description [Assigned to: Ian Technology]
			- D9: this is my short description [Assigned to: Carol Jetson]
			
			All associated Actions have also been deleted.
			-----------------------------------------
			
			----------- MIDDLE CHILD EMAIL NOTIFICATION -----------
			To: Deliverable Responsible Person (1 per individual user NOT individual deliverable)
			Message:
			Legislation L2 has been deleted by Mike Billing.

			The Legislation Details are:
			Financial Year: Year ending 30-June-2015
			Legislation Name: Employment Equity Act
			
			The associated Deliverables for which you are responsible and which have also been deleted are:
			- D2: this is my short description 
			- D3: this is my short description 
			- D5: this is my short description 
			- D7: this is my short description 
			- D9: this is my short description 
			
			All associated Actions have also been deleted.
			----------------------------------------------------
			
			-------- LAST CHILD EMAIL -----------------
			To: Action Owner (1 per individual user NOT individual action)
			Message:
			Legislation L2 and all its associated Deliverables have been deleted by Mike Billing.
			
			The Legislation Details are:
			Financial Year: Year ending 30-June-2015
			Legislation Name: Employment Equity Act
			
			The associated Actions for which you are responsible and which have also deleted are:
			- A100: this is my action description [Deadline: 30-Apr-2015]
			- A101: this is my action description [Deadline: 30-Apr-2015] 
			- A102: this is my action description [Deadline: 30-Apr-2015]
			- A103: this is my action description [Deadline: 30-Apr-2015] 
			- A104: this is my action description [Deadline: 30-Apr-2015] 
			- A105: this is my action description [Deadline: 30-Apr-2015] 
			- A106: this is my action description [Deadline: 30-Apr-2015] 
			
			
			
			
			
			
			
			
		MIDDLE CHILD DELETED (Not parent)
			----------- MIDDLE CHILD EMAIL NOTIFICATION -----------
			To: Deliverable Responsible Person 
			Message:
			Deliverable D50 related to Legislation L2 has been deleted by Mike Billing.

			The Deliverable Details are:
			Financial Year: Year ending 30-June-2015
			Legislation Name: Employment Equity Act
			Deliverable Short Description: This is my short description
			
			The associated Actions which have also been deleted are:
			- A100: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015]
			- A101: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015] 
			- A102: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015]
			- A103: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015] 
			- A104: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015] 
			- A105: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015] 
			- A106: this is my action description [Assigned to: Carol Jetson, Deadline: 30-Apr-2015] 		
			----------------------------------------------------
			
			-------- LAST CHILD EMAIL -----------------
			To: Action Owner (1 per individual user NOT individual action)
			Message:
			Deliverable D50 has been deleted by Mike Billing.
			
			The Deliverable Details are:
			Financial Year: Year ending 30-June-2015
			Legislation Name: Employment Equity Act
			Deliverable Short Description: This is my short description
			Deliverable Responsible Person: Ian Technology
			
			The associated Actions for which you are responsible and which have also deleted are:
			- A100: this is my action description [Deadline: 30-Apr-2015]
			- A103: this is my action description [Deadline: 30-Apr-2015] 
			- A105: this is my action description [Deadline: 30-Apr-2015] 
			- A106: this is my action description [Deadline: 30-Apr-2015] 		
	--------------------------------------------------------------------------
			
	*************************/		
	public function sendDeleteEmail() {}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**************************
	sendApprovalRequestEmail = when an object is marked as completed and moves to waiting approval
	
	-------------Variables-------------	
	$ref = the system generated id of the object EXCLUDING Reftag
	$t = array of user ids (tkid) of the responsible person(s) of the parent object
	$c = module admin if applicable
	$parent_details = array of SUMMARY of any parents up to the primary parent with the first array element is the object name of the parent, the second is the parent object ref (including reftag) and the third is an array of some of the parent object's details - enough to identify the parent object easily e.g. parent object ref (including reftag) and name/description field.
			NOTE: Parent details must include at a minimum:
				+ Financial Year if directly related
				+ System Ref with Reftag
				+ Name / Short description
				+ Owner / Responsible Person / Directorate
				+ Deadline date (if applicable)
		e.g. array(
				0=>array("Deliverable","D1",array('Ref'=>"D1",'Short Description'=>"All leave days must be documented.",'Deliverable Responsible Person'=>"Ian Technology",'Deliverable Deadline Date'=>"30-Sep-2014")),
			);
	----------------------EXAMPLE EMAIL----------------------
	To: Parent responsible Person(s)
	CC: Module admin if applicable
	Subject: Action A100 Awaiting Approval [Compliance Assist]
	Message:
	Mike Billing has marked Action A100 as complete and it is now awaiting your review and approval.
	
	Please log onto Assist to review this Action on Compliance Assist.
	
	***************************/
	public function sendApprovalRequestEmail($ref,$t,$c) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "".$this->object_name." ".$this->reftag.$ref." Awaiting ".$this->getActivityName("approval")." [".$this->assist->getModTitle()."]";
			
			$message = $this->object_name." ".$this->reftag.$ref." has been marked as complete by ".$this->assist->getUserName()." and is now awaiting your review and ".$this->getActivityName("approval").".

Please log onto Assist at https://assist.action4u.co.za in order to review this ".$this->object_name." on ".$this->assist->getModTitle().".";
			//Swap the first two assist email calls to send to real addresses.
			//$this->assist_email->setRecipient($emails['to]);
			$this->assist_email->setRecipient('duncancosser@gmail.com');
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}
	
	
	/**
	*
	 * Function to send reminder email
	 * @param {int} user 	TKID of the user in question
	 * @return {array} response		Array containing the result ("OK/Error","Response Message")
	 */ 
	
	public function sendReminderEmail($ref,$t,$c) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "".$this->object_name." ".$this->reftag.$ref." Awaiting ".$this->getActivityName("approval")." [".$this->assist->getModTitle()."]";
			
			$message = $this->object_name." ".$this->reftag.$ref." has been marked as complete by ".$this->assist->getUserName()." and is now awaiting your review and ".$this->getActivityName("approval").".
Please log onto Assist at https://assist.action4u.co.za in order to review this ".$this->object_name." on ".$this->assist->getModTitle().".";
			$this->assist_email->setRecipient($emails['to']);
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**************************
	sendApproveEmail = when an object is approved
	
	-------------Variables-------------	
	$ref = the system generated id of the object EXCLUDING Reftag
	$t = array of user ids (tkid) of the responsible person(s) of the parent object AND the object owner if the user updating is NOT the owner (i.e. updated in Admin) 
	$c = module admin if applicable
	$msg = response captured on Approve screen
	$parent_details = array of SUMMARY of any parents up to the primary parent with the first array element is the object name of the parent, the second is the parent object ref (including reftag) and the third is an array of some of the parent object's details - enough to identify the parent object easily e.g. parent object ref (including reftag) and name/description field.
			NOTE: Parent details must include at a minimum:
				+ Financial Year if directly related
				+ System Ref with Reftag
				+ Name / Short description
				+ Owner / Responsible Person / Directorate
				+ Deadline date (if applicable)
		e.g. array(
				0=>array("Deliverable","D1",array('Ref'=>"D1",'Short Description'=>"All leave days must be documented.",'Deliverable Responsible Person'=>"Ian Technology",'Deliverable Deadline Date'=>"30-Sep-2014")),
			);
			
			
	----------------EXAMPLE EMAIL -------------------
	To: Action Owner
	CC: Parent Responsible Person
	Subject: Action A100 Approved/Declined/Unlocked [Complaince Assist]
	Message:
	Action A100 has been approved/declined/unlocked by Mike Billing with the message:
	My response here.
	
	The associated Deliverable is:
	Financial Year: Year ending 30 June 2014
	Legislation Ref: L2
	Legislation Name: Employment Equity Act
	Deliverable Ref: D50
	Deliverable Short Description: This is my description
	Deliverable Responsible Person: Ian Technology
	
	Please log onto Assist in order to View (or Update) this Action on Compliance Assist.
	
	***************************/
	public function sendApproveEmail($ref,$t,$c,$msg="",$parent_details=array()) {
		return $this->manageApprovalEmails("Approved",$ref,$t,$c,$msg,$parent_details);
	}
	public function sendDeclineEmail($ref,$t,$c,$msg="",$parent_details=array()) {
		return $this->manageApprovalEmails("Declined",$ref,$t,$c,$msg,$parent_details);
	}
	public function sendUnlockEmail($ref,$t,$c,$msg="",$parent_details=array()) {
		return $this->manageApprovalEmails("Unapproved",$ref,$t,$c,$msg,$parent_details);
	}
	
	public function manageApprovalEmails($action,$ref,$t,$c,$msg="",$parent_details=array()) {
		$emails = $this->getEmailAddresses($t,$c);
		if(count($emails['to'])>0) {
			$subject = "".$this->object_name." ".$this->reftag.$ref." ".$action." [".$this->assist->getModTitle()."]";
			
			//$parent = $this->getParentDetails($parent_details);
			
			$message = $this->object_name." ".$this->reftag.$ref." has been ".strtolower($action)." by ".$this->assist->getUserName().(strlen($msg)>0 ? " with the message:
			
	".$this->decode($msg)."." : "");
			if(strtolower($action) == "unapproved") {
$message.="

This means that the person who ".$this->getActivityName("approved")." your ".$this->object_name." (".$this->assist->getUserName().") has reversed their decision. The ".$this->object_name." will return to the Manage section (with 99% completion) where you can ".$this->getActivityName("update")." it and send it for ".$this->getActivityName("approval")." again.";
			}
			if(count($parent_details[2])>0) {
$message.="

The ".$parent_details[0]." details are as follows:";
				foreach($parent_details[2] as $heading=>$value) {
					$message.="
-".$this->decode($heading).": ".$this->decode($value)."";
				}
			}
				$message.="
			
Please log onto Assist at https://assist.action4u.co.za in order to View ".($action!="Approved" ? "or ".$this->getActivityName("updated") : "")."this ".$this->object_name." on ".$this->assist->getModTitle().".";
			//Uncomment the first assist email call to send to real addresses.
			//$this->assist_email->setRecipient($emails['to]);
			$this->assist_email->setRecipient('duncancosser@gmail.com');
			$this->assist_email->setSubject($subject);
			$this->assist_email->setBody($message);
			$this->assist_email->setCC($emails['cc']);
			$this->assist_email->setSender($emails['reply_to']);
			$this->assist_email->sendEmail();
			return true;
		}
		return false;
	}

















	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***********************************************
	GENERIC FUNCTIONS
	***********************************************/
	public function getEmailAddresses($t,$c) {
		$ids = array_merge($t,$c);
		$ids = array_merge($ids,array($this->assist->getUserID()));
		
		if(count($ids)>0) {
			$db = new ASSIST_DB();
			$sql = "SELECT tkid, CONCAT(tkname,' ',tksurname) as name, tkemail as email FROM assist_".$db->getCmpCode()."_timekeep WHERE tkid IN ('".implode("','",$ids)."')";
			$rows = $db->mysql_fetch_all($sql);
			$emails = array();
			foreach($rows as $r) {
				$emails[$r['tkid']] = array('name'=>$r['name'],'email'=>$r['email']);
			}
		}
		$to = array();
		foreach($t as $x) {
			$to[] = $emails[$x];
		}
		$cc = array();
		foreach($c as $x) {
			$cc[] = $emails[$x];
		}
		$reply_to = $emails[$this->assist->getUserID()];
		
		return array('to'=>$to,'cc'=>$cc,'reply_to'=>$reply_to);
		 
	}
	
	
	private function getParentDetails($parent_details) {
		
			if(count($parent_details)>0) {
				$last_parent = end($parent_details);
				$first_parent = reset($parent_details);
				$parent_relation = " related to ".$first_parent[0]." ".$first_parent[1].($first_parent!=$last_parent ? " of ".$last_parent[0]." ".$last_parent[1] : "");
				$parent_message = "";
				foreach($parent_details as $p) {
					$parent_message.="
					
The related ".$p[0]." details are as follows: ";
					foreach($p[2] as $h=>$v) {
						$parent_message.= "
-".$h.": ".$v;
					}
				}
			} else {
				$parent_relation = "";
				$parent_message = "";
			}
			//	$parent_relation = "";
			//$parent_message = chr(10).chr(10)."Parent details array check: ".chr(10).$this->arrayLoop($parent_details);
		return array('related'=>$parent_relation,'message'=>$parent_message);
		 
		 
	}
	function arrayLoop($a) {
		$str = "";
		foreach($a as $k=>$v) {
			$str.=chr(10).$k." = ";
			if(is_array($v)) {
				$str.="{Array};";
			} else {
				$str.=$v.";";
	
			}
		}
		return $str;
	}
	
	
}


?>