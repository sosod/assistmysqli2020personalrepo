<?php
/**
 * To manage the display of forms fields
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */

class ASSIST_MODULE_DISPLAY {
	
	private $js = array();
	
	private $max_select_option_length = 75;
	
	private $common_extensions = array(
		'doc'	=> "doc",
		'docx'	=> "doc",
		'rtf'	=> "doc",
		'xls'	=> "xls",
		'csv'	=> "xls",
		'xlsx'	=> "xls",
		'dot'	=> "doc",
		'doct'	=> "doc",
		'docm'	=> "doc",
		'xlst'	=> "xls",
		'xlt'	=> "xls",
		'xltm'	=> "xls",
		'xlm'	=> "xls",
		'ppt'	=> "ppt",
		'pptx'	=> "ppt",
		'pptm'	=> "ppt",
		'pdf'	=> "pdf",
		'msg'	=> "email",
		'htm'	=> "html",
		'html'	=> "html",
		'css'	=> "html",
		'js'	=> "html",
		'php'	=> "html",
		'jpg'	=> "image",
		'jpeg'	=> "image",
		'gif'	=> "image",
		'png'	=> "image",
		'bmp'	=> "image",
		'tif'	=> "tiff",
		'tiff'	=> "tiff",
		'zip'	=> "zip",
		'rar'	=> "zip",
		'txt'	=>"other",
	);
	private $file_icons = array(
		'doc'=>"word.gif",
		'other'=>"document.gif",
		'text'=>"document.gif",
		'email'=>"email.gif",
		'xls'=>"excel.gif",
		'html'=>"htm.gif",
		'image'=>"image.gif",
		'pdf'=>"pdf.gif",
		'ppt'=>"ppt.gif",
		'tiff'=>"tiff2.gif",
		'zip'=>"zip1.gif",
	); 
	
	private $activity_names = array();
	
	private $object_names = array();
    
    public function __construct($an=array(),$on=array()) {
    	$this->activity_names = $an;
		$this->object_names = $on;
    }
    
	/**
	 * Function to replace both |object| and |activity| with the current usage
	 */
	public function replaceAllNames($v) {
		$v = $this->replaceObjectNames($v);
		$v = $this->replaceActivityNames($v);
		return $v;
	}
    /**
	 * Function to replace |object| with object_name
	 */
    public function replaceObjectNames($v) {
    	if(is_array($v)) {
	        $a = json_encode($v);
		} else {
			$a = $v;
		}
	    foreach($this->object_names as $key => $on) {
	        $a = str_ireplace("|".$key."|",$on,$a);
	    }
		if(is_array($v)) {
			$v = json_decode($a,true);
		} else {
			$v = $a;
		}
        return $v;
    }  
	
	/**
	 * Function to replace |activity| with acitivty_name
	 */
	public function replaceActivityNames($v) {
		if(is_array($v)) {
			$a = json_encode($v);
		} else {
			$a = $v;
		}
		foreach($this->activity_names as $key => $on) {
			$a = str_ireplace("|".$key."|",$on,$a);
		}
		if(is_array($v)) {
			$v = json_decode($a,true);
		} else {
			$v = $a;
		}
		return $v;
	}
	
	
	
	/**********************************
	 * FORM INPUT FIELD
	 */
	
	
	
    /**
     * (ECHO) Displays a small input text field: use for short codes
     * @param (String) val = any value to be displayed
     * @param (Array) prop = any additional properties with property name as key and value as element
     * @param (INT) max = max length of text to be allowed, default = 20
     * @param (INT) size = size of input field, default = 10
     * 
     * @return (String) js = any related javascript that might need to be displayed on the calling page 
     */
    public function drawSmallInputText($val="",$prop=array(),$max=20,$size=10) {
        $d = $this->getSmallInputText($val,$prop,$max,$size);
        echo $d['display'];
        return $d['js'];
    }
    
    /**
     * (ECHO) Displays a medium input text field: use for reference numbers 
     * @param (String) val = any value to be displayed
     * @param (Array) prop = any additional properties with property name as key and value as element
     * @param (INT) max = max length of text to be allowed, default = 100
     * @param (INT) size = size of input field, default = 50
     * 
     * @return (String) js = any related javascript that might need to be displayed on the calling page 
     *      */
    public function drawMediumInputText($val="",$prop=array(),$max=100,$size=50) {
        $d = $this->getMediumInputText($val,$prop,$max,$size);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a large input text field: use for names
     */
    public function drawLargeInputText($val="",$prop=array(),$max=200,$size=100) {
        $d = $this->getLargeInputText($val,$prop,$max,$size);
        echo $d['display'];
        return $d['js'];
    }
    
    /**
     * Displays a date picker input
     */
    public function drawDatePicker($val="",$prop=array(),$options=array()) {
        $d = $this->getDatePicker($val,$prop,$options);
        echo $d['display'];
        return $d['js'];
    }
    
    /**
     * Displays a standard text area: use for unlimited TEXT fields
     */
    public function drawTextArea($val="",$prop=array(),$rows=5,$cols=50) {
        $d = $this->getTextArea($val,$prop,$rows,$cols);
        echo $d['display'];
        return $d['js'];
    }
    
    /**
     * Displays a text field with a text limiter associated: use for large text fields with a text limit
     */
    public function drawLimitedTextArea($val="",$prop=array(),$rows=5,$cols=50,$max=200) {
        $d = $this->getLimitedTextArea($val,$prop,$rows,$cols,$max);
        echo $d['display'];
        return $d['js'];
    }
    
    /**
     * Displays a drop down select field = single value
     */
    public function drawSelect($val="",$prop=array(),$items) {
        $d = $this->getSelect($val,$prop,$items);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a drop down select field = multiple value
     */
    public function drawMultipleSelect($val=array(),$prop=array(),$items,$show=0) {
        $d = $this->getMultipleSelect($val,$prop,$items,$show);
        echo $d['display'];
        return $d['js'];
    }
    
    /**
	 * Displays a yes/no form field
	 */
	public function drawBool($val="",$prop=array()){
		$d = $this->getBool($val,$prop);
		echo $d['display'];
		return $d['js'];
	}
    
    /**
	 * Displays a colour field
	 */
	public function drawColour($val="",$prop=array()){
		$d = $this->getColour($val,$prop);
		echo $d['display'];
		return $d['js'];
	}
    
    /**
	 * Displays a numerical rating field
	 */
	public function drawRating($val="",$prop=array()){
		$d = $this->getRating($val,$prop);
		echo $d['display'];
		return $d['js'];
	}
    
    /**
	 * Displays a numerical field which only permits numbers
	 */
	public function drawNumInputText($val="",$prop=array()){
		$d = $this->getNumInputText($val,$prop);
		echo $d['display'];
		return $d['js'];
	}
    
	/**
	 * Displays the attachment form
	 */
	public function drawAttachmentInput($val="",$prop=array()) {
		$d = $this->getAttachmentInput($val,$prop);
		echo $d['display'];
		return $d['js'];
	}
	
	
	
	
	/***************************************
	 * GET FUNCTIONS!!
	 */
    
	public function getLabel($val="",$prop=array()) {
		$data = array('display'=>"",'js'=>"");
		$data['display']="<label ".$this->convertPropToString($prop)." >".ASSIST_HELPER::decode($val)."</label>";
		return $data;
	}
    	public function getSmallInputText($val="",$prop=array(),$max=20,$size=10) {
		$max = $max>0 ? $max : 20;
		$size = $size>0 ? $size : 10;
		return $this->getInputText($val, $prop, $max, $size);
	}
    public function getMediumInputText($val="",$prop=array(),$max=100,$size=50) {
        $max = $max>0 ? $max : 100;
        $size = $size>0 ? $size : 50;
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getLargeInputText($val="",$prop=array(),$max=200,$size=100) {
        $max = $max>0 ? $max : 200;
        $size = $size>0 ? $size : 100;
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getColour($val="",$prop=array(),$max=20,$size=10) {
        $max = $max>0 ? $max : 20;
        $size = $size>0 ? $size : 10;
		$prop = array("colour"=>"true");
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getRating($val="",$prop=array(),$max=20,$size=10) {
        $max = $max>0 ? $max : 20;
        $size = $size>0 ? $size : 10;
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getNumInputText($val="",$prop=array(),$max=20,$size=10) {
        $max = $max>0 ? $max : 20;
        $size = $size>0 ? $size : 10;
		if(isset($prop['warn'])) {
			$display_warning = $prop['warn'];
			unset($prop['warn']);
		} else {
			$display_warning = true;
		}
		$prop['class']= (isset($prop['class']) ? $prop['class'] : "").($display_warning ? " display_warning" : "")." number-only";
		$js_alt = isset($prop['extra']) ? $prop['extra'] : "";
        $data = $this->getInputText($val, $prop, $max, $size);
		if(!isset($this->js['num']) || $this->js['num']!==true) {
			$this->js['num'] = true;
			$data['js'] = "
			$('.number-only').blur(function() {
				$(this).removeClass('required');
				var v = $(this).val();
				var num_arr = ['0','1','2','3','4','5','6','7','8','9','.','-'];
				var valid8 = true;
				for ( var i = 0; i < v.length; i++ ) {
					if($.inArray(v.charAt(i),num_arr)<0) {
						valid8=false;
					}
				}
				if(!valid8) {
					$(this).addClass('required');
					".(strlen($js_alt)>0 ? $js_alt."($(this));" : "")."
				}
			}).each(function() {
				if($(this).hasClass('display_warning')) {
					$(this).parent().append('<div class=\'orange i float\' style=\'width: 50%\'><small>Note: Only numbers (0-9), period (.) and dash (-) permitted</small></div>');
				} 
			});
			";
		}
		return $data;
    }
    public function getDatePicker($val="",$prop=array(),$options=array()) {
    	//echo "get date picker";
        $data = array('display'=>"",'js'=>"");
		if(isset($prop['custom_datepicker'])) {
			$custom_datepicker = $prop['custom_datepicker'];
			unset($prop['custom_datepicker']);
		} else {
        	$custom_datepicker ="datepicker";
		}
    	$prop['class'] = (isset($prop['class']) ? $prop['class'] : "")." ".$custom_datepicker;
		$data = $this->getInputText($val, $prop, 10, 12);
		if(!isset($this->js[$custom_datepicker]) || $this->js[$custom_datepicker]==false) {
			//default datepicker options
                    $options['showOn'] = !isset($options['showOn']) ? "'both'" : $options['showOn'];
                    $options['buttonImage'] = !isset($options['buttonImage']) ? "'/library/jquery/css/calendar.gif'" : $options['buttonImage'];
                    $options['buttonImageOnly'] = !isset($options['buttonImageOnly']) ? "true" : $options['buttonImageOnly'];
                    $options['dateFormat'] = !isset($options['dateFormat']) ? "'dd-M-yy'" : $options['dateFormat'];
                    $options['changeMonth'] = !isset($options['changeMonth']) ? "true" : $options['changeMonth'];
                    $options['changeYear'] = !isset($options['changeYear']) ? "true" : $options['changeYear'];
                    $options['minDate'] = !isset($options['minDate']) ? "null" : $options['minDate'];
                    $options['maxDate'] = !isset($options['maxDate']) ? "null" : $options['maxDate'];
                    			
			$data['js'] = "$('.".$custom_datepicker."').datepicker({".$this->convertPropToJS($options)."});";
			$this->js[$custom_datepicker] = true;
		}
        return $data;
    }
    public function getTextArea($val="",$prop=array(),$rows=5,$cols=50) {
        $data = array('display'=>"",'js'=>"");
		$data['display'] = "<textarea".$this->convertPropToString($prop)." rows=".$rows." cols=".$cols.">".ASSIST_HELPER::decode($val)."</textarea>";	
        return $data;
    }
    	/**
		 * JS FINETUNING TO BE DONE!!!
		 */
    public function getLimitedTextArea($val="",$prop=array(),$rows=5,$cols=50,$max=200) {
        $data = array('display'=>"",'js'=>"");
		$prop['class'] = (isset($prop['class']) ? $prop['class'] : "")." txt_char";
		$data['display'] = "<textarea ".$this->convertPropToString($prop)." rows=".$rows." cols=".$cols.">".ASSIST_HELPER::decode($val)."</textarea><br />
		<label id='lbl_".$prop['id']."' for='".$prop['id']."' max=".$max."><span class=spn_char>".$max."</span> characters remaining</label>";
		if(!isset($this->js['limitedtext']) || $this->js['limitedtext']==FALSE){
			$data['js'] = "$('textarea.txt_char').keyup(function(){
				var v = $(this).val();
				var l = v.length;
				var i = $(this).prop('id');
				var mxStr = $('#lbl_'+i).attr('max');
				var mx = parseInt(mxStr);
				window.rem = mx - l;
				$('#lbl_'+i).removeClass('idelete').children('span.spn_char').html(rem).removeClass('iinform').removeClass('isubmit');
				
				if(rem>10) {
					$('#lbl_'+i).children('span.spn_char').addClass('isubmit');
				} else if (rem <= 10 && rem > 0) {
					$('#lbl_'+i).children('span.spn_char').addClass('iinform');
				} else {
					$('#lbl_'+i).addClass('idelete');
				}
				
			});
			$('textarea.txt_char').blur(function(){
				$(this).trigger('keyup');
				var i = $(this).prop('id');
				var rem = $('#lbl_'+i).children('span.spn_char').html();
				if(rem < 0){
					alert('Your input is too long, and not all of it will be saved. Please stick to the character limit.');
				}
			});";
			$this->js['limitedtext'] = true;
		}	
        return $data;
    }
    public function getSelect($val="",$prop=array(),$items) {
    	//print_r($prop); print_r($items);
    	$required = isset($prop['req']) ? $prop['req']==1 : false;
    	$allow_unspecified = isset($prop['unspecified']) ? $prop['unspecified'] : true;
    	$option_parent_association = isset($prop['list_num']) ? $prop['list_num'] : array();
        $data = array('display'=>"",'js'=>"");
		$data['display'] = "<select ".$this->convertPropToString($prop).">".($required || !$allow_unspecified ? "<option value=X>--- SELECT ---</option>" : "<option value=0>".ASSIST_HELPER::UNSPECIFIED."</option>");
		foreach($items as $key => $i) {
			$s = $i;
			if(strlen($s)>$this->max_select_option_length) {
				$s = substr($s,0,$this->max_select_option_length-3)."...";
			}
			$data['display'].="<option value='".$key."' ".($key==$val ? "selected" : "")." full='".ASSIST_HELPER::code($i)."' list_num=".(isset($option_parent_association[$key]) ? $option_parent_association[$key] : "0").">".$s."</option>";
		}
		$data['display'].="</select>
		<div id=div_".$prop['id']." class='display_me ui-state-ok' style='width: 500px;margin: 5px;'></div>";
		if(!isset($this->js['select']) || $this->js['select']!==true) {
			$this->js['select']=true;
			$data['js'] = "
			$('div.display_me').hide();
			$('select').change(function(){
				var i = $(this).prop('id');
				var $"."opt = $(this).find(':selected');
				var txt = '';
				var alt = '';
				$"."opt.each(function() {
					if($(this).val()==0 || $(this).val()=='X') {
						
					} else {
						if($(this).attr('full')!=null) {
							alt = AssistString.stripslashes($(this).attr('full'));
							if(alt==$(this).text() || alt==AssistString.code($(this).text())) {
							} else {
								if(alt.length>0 && txt.length>0) { 
									txt+='<br />';
								} 
								txt+='+ '+alt;
							}
						}
					}
				});
				if(txt.length>0) {
					$('#div_'+i).show().html('<p>'+txt+'</p>');
				} else {
					$('#div_'+i).html('').hide();
				}
			});";
		}
        return $data;
    }
    public function getMultipleSelect($val=array(),$prop=array(),$items,$show=0) {
        $data = array('display'=>"",'js'=>"");
    	$required = isset($prop['req']) ? $prop['req']==1 : false;
    	$allow_unspecified = isset($prop['unspecified']) ? $prop['unspecified'] : true;
		$prop['class'] = (isset($prop['class']) ? $prop['class'] : "")." multi_select";
		if($show==0) {
			$show = count($items)>7 ? 8 : count($items)+1;
		}
		$data['display'] = "<select ".$this->convertPropToString($prop)." multiple size=".($show>0 ? $show : 10).">".($required || !$allow_unspecified ? "" : "<option value=0>".ASSIST_HELPER::UNSPECIFIED."</option>");
		foreach($items as $key => $i) {
			$data['display'].="<option value='".$key."' ".(in_array($key,$val) ? "selected" : "").">".$i."</option>";
		}
		$data['display'].="</select>";
		if(!isset($this->js['multi_select']) || $this->js['multi_select']==FALSE){
			$data['js'] = "
				$('select.multi_select').change(function() {
					var $"."me = $(this).find('option:selected:first');
					if($"."me.prop('value')=='0') {
						$(this).find('option:selected:gt(0)').prop('selected',false); 
					}
				});
			";
		}
        return $data;
    }

	/**
	 * Function to return the display and js needed for the Yes/No buttons
	 * @param $val = initial value (must match settings of $prop['yes'] or $prop['no'])
	 * @param (array) $prop = array of properties
	 * @param $prop['id'] = unique identifier (minimum requirement)
	 * @param $prop['name'] = form field name, defaults to id if not specified
	 * @param $prop['yes'] = form value for the yes button, defaults to 1 if not specified
	 * @param $prop['no'] = form value for the no button, defaults to 0 if not specified
	 * @param $prop['form'] = layout of the buttons, horizontal or vertical, default to horizontal if not specified
	 * @param $prop['extra'] = function name of additional function (on the calling page) to be called when a button is pressed, not required
	 * @param $prop['other'] = any additional attributes to be set for the hidden input field that holds the result of the button press (as string, not array)
	 * 
	 * @return array('display'=>HTML, 'js'=>JQuery)
	 */

    public function getBoolButton($val="",$prop=array()){
    	//echo "<br /> gBB VAL:".$val." ARR: "; print_r($prop);
    	if(!isset($prop['form'])) { $prop['form'] = "horizontal"; } else { $prop['form'] = strtolower($prop['form']); }
        $data = array('display'=>"",'js'=>"");
		$prefix = "";
		$midfix = "";
		$postfix = "";
		if($prop['form']=="vertical"){
			$prefix = "<div style='text-align: center; padding-left: 10px; padding-right: 10px;'>";
			$midfix = "<br />";
			$postfix = "</div>";
		} else {
			$prefix = "<div>";
			$midfix = "";
			$postfix = "</div>";
		}
		$prop['yes'] = (isset($prop['yes']) ? $prop['yes'] : 1);
		$prop['no'] = (isset($prop['no']) ? $prop['no'] : 0);
		$data['display'] = $prefix."
			<input type=hidden name=".(isset($prop['name']) ? $prop['name'] : $prop['id'])." id=".$prop['id']." value=".$val." ".(isset($prop['other']) ? $prop['other'] : "")." />
			<button class='bool_btn btn_yes' val=".$prop['yes']." id=".$prop['id']."_yes button_status=".($prop['yes']==$val?"active":"no").">Yes</button> ".$midfix."
			<button class='bool_btn btn_no' val=".$prop['no']." id=".$prop['id']."_no button_status=".($prop['no']==$val?"active":"no").">No</button>
		".$postfix."";
		if(!isset($this->js['bool_button']) || $this->js['bool_button']==FALSE){
			$data['js'] = "
			$(\"button.btn_yes\").button({
				icons: {
					primary: \"ui-icon-check\"
				}
			}).each(function() {
				if($(this).attr(\"button_status\")==\"active\") {
					$(this).addClass(\"ui-state-ok\");
				}
			})".($prop['form']=="vertical" ? ".css('margin-bottom','2px')":"").";
			$(\"button.btn_no\").button({
				icons: {
					primary: \"ui-icon-close\"
				}
			}).each(function() {
				if($(this).attr(\"button_status\")==\"active\") {
					$(this).addClass(\"ui-state-error\");
				}
			})".($prop['form']=="vertical" ? ".css('margin-top','2px')":"").";
			$('button.bool_btn').addClass('button_class').click(function(event) {
				event.preventDefault();
				var i = $(this).prop('id');
				var v = $(this).attr('val');
				var f = AssistString.explode('_',i);
				var act = AssistArray.array_pop(f);
				var fld = AssistString.implode('_',f);
				$('#'+fld).val(v);
				if(act=='yes'){
					$(this).addClass('ui-state-ok');
					$('#'+fld+'_no').removeClass('ui-state-error');
				} else {
					$(this).addClass('ui-state-error');
					$('#'+fld+'_yes').removeClass('ui-state-ok');
				}
				$(this).blur();
				".(isset($prop['extra']) ? $prop['extra']."($(this));" : "")."
				return false;
			});
			";
			$this->js['bool_button']=true;
		}
    	return $data;
    }


	public function getAttachmentInput($val="",$prop=array()) {
		$data = array('display'=>"<div id=div_attach_container style='width:350px;'>",'js'=>"");
		if(strlen($val)>0) {
			$data['display'].=$this->getAttachForDisplay($val,
															(isset($prop['can_edit']) ? $prop['can_edit'] : false),
															(isset($prop['object_type']) ? $prop['object_type'] : "X"),
															(isset($prop['object_id']) ? $prop['object_id'] : "0"),
															true,
															(isset($prop['page_activity']) ? $prop['page_activity'] : "VIEW")
														);
		}
		$data['display'].= "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" style='margin-bottom: 5px;' /> <button class=cancel_attach>".$this->activity_names["reset"]."</button>
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #ffffff;color:#ffffff;\"></iframe>
			".ASSIST_HELPER::getDisplayResult(array("warn","No more than 10 files may be uploaded at once.<br />Combined file size cannot exceed 10MB."))."
			<input type=hidden name=has_attachments id=has_attachments value=1 />
			<input type=hidden name=action value='".$prop['action']."' />
			<input type=hidden name=page_direct value='".$prop['page_direct']."' />
		";
		$data['js'].="
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			var attachment_count = 1;
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<button class=cancel_attach>".$this->activity_names["reset"]."</button>')
							.button({icons:{primary:'ui-icon-cancel'}})
							.addClass('button_class')
							.bind('click',function(e) { e.preventDefault(); handler($('#firstdoc input:button').index(this)); });
				});
				attachment_count++;
				if(attachment_count==10) { $('#attachlink').hide(); }
			});
			$('button.cancel_attach').button({icons:{primary:'ui-icon-cancel'}}).addClass('button_class').click(function(e) {
				e.preventDefault();
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		".($_SESSION['cc']=="testcon2" ? "$('#file_upload_target').css({'width':'250px','height':'250px','border':'1px dashed #009900'});" : "");
		$data['display'].="</div>";
		return $data;
	}



/*****************
 * PRIVATE functions - this class only
 */
    private function getInputText($val,$prop,$max,$size){
    	//echo "get input text val:*".$val."*";
        $data = array('display'=>"",'js'=>"");
    	if(isset($prop['colour']) && $prop['colour'] == "true"){
    		//echo "color";
	        $data['display']="<input style=\"border:1px solid grey\" class=\"color {notext:true}\" />";
    	}else{
    		//echo "else";
	        $data['display']="<input type=text value=\"".ASSIST_HELPER::decode($val)."\" maxlength='".$max."' size='".$size."' ";
	        $data['display'].=$this->convertPropToString($prop)." />";
			//echo ASSIST_HELPER::code($data['display']);
    	}
			
        //foreach($prop as $key=>$v){
        //    $data['display'].= " ".$key."='".$v."'";
        //}
        return $data;
    }
	private function convertPropToString($prop){
		$options = "";
		foreach($prop as $key=>$v){
			$options.=" ".$key."=\"".$v."\"";
		}
		return $options;
	}
	private function convertPropToJS($prop){
		$options = array();
		foreach($prop as $key=>$v){
			$options[] = $key.": ".$v."";
		}
		return implode(",",$options);
	}
    
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**********************************
	 * DATA DISPLAY FORMATTING
	 */
	
	/***
	 * (ECHO) DATE formatting
	 * @param (String) d = date in non-unix format
	 * @param (Bool*) include_time = include time in output true/false*
	 */
	public function drawDateForDisplay($d,$include_time=false) {
		echo $this->getDateForDisplay($d,$include_time);
	}
	/***
	 * (RETURN) DATE formatting
	 * @param (String) d = date in non-unix format
	 * @param (Bool*) include_time = include time in output true/false*
	 */
	public function getDateForDisplay($d,$include_time=false) {
		if($include_time) { $format = "d-M-Y H:i"; } else { $format = "d-M-Y"; }
		if(is_numeric($d)) {
			return date($format,$d);
		} elseif(strtotime($d)>0) {
			return date($format,strtotime($d));
		} else {
			return ASSIST_HELPER::UNSPECIFIED;
		}
	}
	
	/** 
	 * (ECHO) Currency formatting
	 * @param (Float) v = amount to be displayed
	 * @param (String*) symbol = prefix; default = "R"
	 */
	public function drawCurrencyForDisplay($v,$symbol = "R") {
		echo $this->getCurrencyForDisplay($v,$symbol = "R");
	}
	/** 
	 * (RETURN) Currency formatting
	 * @param (Float) v = amount to be displayed
	 * @param (String*) symbol = prefix; default = "R"
	 */
	public function getCurrencyForDisplay($v,$symbol="R") {
		$decimal = ".";
		$thou_sep = ",";
		return (strlen($symbol)>0 ? $symbol." " : "").number_format($v,2,$decimal,$thou_sep);
	}
	
	/** 
	 * (ECHO) Percentage formatting
	 * @param (Float) v = amount to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function drawPercentageForDisplay($v,$decimal=0) {
		echo $this->getPercentageForDisplay($v,$decimal);
	}
	/** 
	 * (RETURN) Percentage formatting
	 * @param (Float) v = amount to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function getPercentageForDisplay($v,$decimal=0) { 
		return number_format($v,$decimal)."%";
	}
	
	/** 
	 * (ECHO) Number/Integer formatting
	 * @param (Number) v = number to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function drawNumberForDisplay($v,$decimal=0) {
		echo $this->getNumberForDisplay($v,$decimal);
	}
	/** 
	 * (RETURN) Number/Integer formatting
	 * @param (Float) v = number to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function getNumberForDisplay($v,$decimal=0) {
		return number_format($v,$decimal);
	}
	
	
	/** 
	 * (ECHO) BOOL formatting
	 * @param (BOOL) v = value to be displayed = true/false or 1/0
	 */
	public function drawBoolForDisplay($v) {
		echo $this->getBoolForDisplay($v);
	}
	/** 
	 * (RETURN) BOOL formatting
	 * @param (BOOL) v = value to be displayed = true/false or 1/0
	 */
	public function getBoolForDisplay($v,$html_tags=true) {
		if($v==true) {
			return ($html_tags ? $this->getDisplayIconAsDiv("ok")." " : "")."Yes";
		} else {
			return ($html_tags ? $this->getDisplayIconAsDiv("0")." " : "")."No";
		}
	}
	
/*	public function getAttachForDisplay($val,$can_edit=false) {
		$me = new ASSIST_MODULE_HELPER();
		$folder = $me->getSaveFolder();
		$data = "";
		$y = unserialize($val);
		$z = array();
		foreach($y as $i => $x) {
			$filename = $folder."/".$x['location']."/".$x['system_filename'];
			if(file_exists($filename)) {
				$f = "<tr ref=".$i." class=down_attach><td>";
				$fn = $x['original_filename'];
				if(isset($x['ext']) && strlen($x['ext'])>0) {
					if(isset($this->common_extensions[$x['ext']])) {
						$f .= "<img src=/pics/icons/".$this->file_icons[$this->common_extensions[$x['ext']]]." />";
					} else {
						$f .= "<img src=/pics/icons/".$this->file_icons['other']." />";
						//Notify Action iT so that the icon can be identified
						mail("actionit.actionassist@gmail.com","Unknown file extension identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","From: no-reply@ignite4u.co.za");
					}
				}
				$f.="</td><td><a href=#>".$fn."</a></td>";
				if($can_edit) {
					$f.="<td class=delete_attach>delete</td>";
				}
				$z[] = $f."</tr>";
			}
		}
		if(count($z)>0) {
			$data = "<table class='attach not-max'>".implode("",$z)."</table>";
			
		}
		return $data;
	}
 */
	/**
	 * Function to generate code to display existing attachments
	 * @param (serializeArray*) val
	 * @param (BOOL) can_edit=false
	 * @param (Str) Object_type=X
	 * @param (Int) Object_id=0
	 * @param (Bool) buttons=true
	 * @param (Str) page_activity=VIEW
	 */
	public function getAttachForDisplay($val,$can_edit=false,$object_type="X",$object_id="0",$buttons=true,$activity="VIEW") {
		$dom_element_id = strtolower($object_type."_".$activity."_".$object_id);
		$amh = new ASSIST_MODULE_HELPER();
		$folder = $amh->getSaveFolder();
		$data = "";
		if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
		$y = unserialize($val);
		$z = array();
		if(is_array($y) && count($y)>0) {
			foreach($y as $i => $x) {
				$filename = $folder."/".$x['location']."/".$x['system_filename'];
				if((!isset($x['status']) || (($x['status'] & CNTRCT::ACTIVE) == CNTRCT::ACTIVE)) && file_exists($filename)) {
					$f = "<tr id=th_".$i."><td width=26px class=center>";
					$fn = $x['original_filename'];
					if(isset($x['ext']) && strlen($x['ext'])>0) {
						if(isset($this->common_extensions[$x['ext']])) {
							$f .= "<img src=/pics/icons/".$this->file_icons[$this->common_extensions[$x['ext']]]." />";
						} else {
							$f .= "<img src=/pics/icons/".$this->file_icons['other']." />";
							//Notify Action iT so that the icon can be identified
							mail("actionit.actionassist@gmail.com","Unknown file extension identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","From: no-reply@ignite4u.co.za");
						}
					}
					$f.="</td><td>".$fn."</td>";
					if($buttons) {
						$f.="<td width=30px><button ref='".$i."' class='button_class down_attach_".$dom_element_id."'>".$this->activity_names["download"]."</button></td>";
						if($can_edit) {
							$f.="<td width=30px><button ref='".$i."' class='button_class delete_attach_".$dom_element_id."'>".$this->activity_names["delete"]."</button></td>";
						}
					}
					$z[] = $f."</tr>";
				}
			}
		}
		if(count($z)>0) {
			$data = "<table id=tbl_display_attach class='attach' width=100% style='".($can_edit ? "margin-bottom: 20px;" : "")."'>".implode("",$z)."</table>";
			
		}
		return $data;
	}
 	
/*	public function getAttachmentDownloadJS($object_type,$object_id) {
		$js = "
		$('tr.down_attach').css('cursor','pointer').click(function() {
			//var i = $(this).attr('ref'); //alert(i);
			//var dta = 'object_id=".$object_id."&i='+i;
			//var result = AssistHelper.doAjax('inc_attachment_controller.php?action=".$object_type.".GET_ATTACH',dta);
			//document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
			//console.log(result);
		});
		$('tr.down_attach img, tr.down_attach a').click(function(event){
			event.preventDefault();
			var i = $(this).parent().parent().attr('ref');
			alert(i);
			//var dta = 'object_id=".$object_id."&i='+i;
			//document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
		});
		$('td.delete_attach').hover(
			function() {
				$(this).css('color','#fe9900');
			},
			function() {
				$(this).css('color','');
			}
		);
		";
		return $js;
	}
	*/
	public function getAttachmentDownloadJS($object_type,$object_id,$activity="VIEW") {
		$dom_element_id = strtolower($object_type."_".$activity."_".$object_id);
		if(!isset($this->js['attachment_down_delete_'.$dom_element_id]) || $this->js['attachment_down_delete_'.$dom_element_id]!==true) {
			$this->js['attachment_down_delete_'.$dom_element_id] = true;
			$js = "
			$('table.attach tr').hover(
				function() { $(this).children('td:eq(1)').addClass('trhover'); },
				function() { $(this).children('td:eq(1)').removeClass('trhover'); }
			);
			$('button.down_attach_".$dom_element_id."').button({
				icons: {primary: 'ui-icon-arrowthickstop-1-s'}
			}).hover(
				function() {
					$(this).addClass('ui-state-ok');
				},
				function() {
					$(this).removeClass('ui-state-ok');
				}
			).click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var dta = 'activity=".$activity."&object_id=".$object_id."&i='+i;
				document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
				$(this).blur();
			});
			$('button.delete_attach_".$dom_element_id."').button({
				icons: {primary: 'ui-icon-trash'}
			}).hover(
				function() {
					$(this).addClass('ui-state-error');
				},
				function() {
					$(this).removeClass('ui-state-error');
				}
			).click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var txt = $(this).parent().parent().children('td:eq(1)').html();
				var nm = $(this).parents('tr:eq(1)').children('th:first').html();
				if(confirm('Are you sure you wish to ".$this->activity_names["delete"]." '+nm+' '+txt)) {
					AssistHelper.processing();
					var dta = 'activity=".$activity."&object_id=".$object_id."&i='+i;
					var result = AssistHelper.doAjax('inc_controller.php?action=".$object_type.".DELETE_ATTACH',dta);
					console.log(result);
					if(result[0]=='ok') {
						$(this).parents('tr:eq(0)').hide();
					}
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			});
			";
		} else {
			$js = "";
		}
		return $js;
	}	
	
	
	
	/******
	 * Data display PRIVATE functions
	 */
	private function getDisplayIconAsDiv($icon,$options="") {
		if($icon===true || $icon==true) { $icon="1"; } elseif($icon===false || $icon==false) {$icon="0";}
		switch($icon) {
		case "ok":
		case "1":
			return "
				<div class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "error":
		case "0":
			return "
				<div class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "info":
			return "
				<div class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>
			";
			break;
		}
	}
	
	
	
	
	
	public function displayJSSettings() { ASSIST_HELPER::arrPrint($this->js); }
	
}



?>