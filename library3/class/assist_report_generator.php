<?php


class ASSIST3_REPORT_GENERATOR extends ASSIST_REPORT {
//VARIABLES FOR DISPLAY
	private $sections = array();
	private $script = "";
	private $filter_script = "";
	private $form_action = "";
	private $source_class = "";
	private $quick_class = "";
//VARIABLES TO STORE
	private $formats = array(
		'onscreen'=>"Onscreen display",
		'csv'=>"Microsoft Excel (Plain Text)",
		'excel'=>"Microsoft Excel (Formatted)*",
	);
	private $availableformats = array('onscreen'=>true,'csv'=>true,'excel'=>true);
	private $quick = true;
	private $has_clear_date_been_scripted = false;
	private $update_log_option = array('LAST'=>true,'ALL'=>true,'AUDIT'=>true);
	
	private $quick_report;
	private $a = 0;

	private $update_filter_options = array(
		'ALL'=>"Complete Update History",
		'LAST'=>"Most Recent Update",
		'AUDIT'=>"Complete Activity Log",
	);
	
	
	
	
	public function __construct($p,$sc,$qc) {
//		echo "<P>ASSIST_REPORT_GENERATE.__construct()</p>";
		parent::__construct();
		$this->form_action = $p;
		$this->source_class = $sc;
		$this->quick_class = $qc;
		$this->sections = array(	
			'fields'=>array(
				'title'=>"Select the information to be displayed in the report",
				'func'=>"drawColumnChooser",
			),
			'filters'=>array(
				'title'=>"Select the filter you wish to apply",
				'func'=>"drawFilters",
			),
			'sort'=>array(
				'title'=>"Choose your group and sort options",
				'func'=>"drawGroupSortOptions",
			),
			'output'=>array(
				'title'=>"Choose the document format of your report",
				'func'=>"drawFormatSelection",
			),
			'generate'=>array(
				'title'=>"Generate the report",
				'func'=>"drawGenerateButtons",
			),
			'quick'=>array(
				'title'=>"Save as Quick Report",
				'func'=>"drawSaveQuickReport",
			),
		);
		$this->quick_report = array(
			'id'=>0,
			'name'=>"",
			'descrip'=>"",
			'user'=>$this->getUserId(),
			'date'=>date("d F Y H:i:s"),
			'settings'=>array(),
			'existing'=>false
		);
	}
	
	
	
	
	
	//SETTINGS
	public function disableQuickReports() { $this->quick = false; }
	public function enableQuickReports() { $this->quick = true; }
	public function disableOnscreen() { $this->availableformats['onscreen'] = false; }
	public function enableOnscreen() { $this->availableformats['onscreen'] = true; }
	public function disableCSV() { $this->availableformats['csv'] = false; }
	public function enableCSV() { $this->availableformats['csv'] = true; }
	public function disableExcel() { $this->availableformats['excel'] = false; }
	public function enableExcel() { $this->availableformats['excel'] = true; }
	public function disableUpdateLogOption($o) { unset($this->update_log_option[$o]); }
	public function disableFieldChooser() { unset($this->sections['fields']); }
	public function disableFormatChooser() { unset($this->sections['output']); }
	public function enableGraphGrouping() { 
		$this->sections['sort']['title'] = "Choose your grouping option"; 
		$this->sections['sort']['func'] = "drawGraphGroupOptions"; 
	}
	
	public function setQuickReport($id,$name,$descrip,$user,$date,$settings) {
		$this->quick_report = array(
			'id'=>$id,
			'name'=>$name,
			'descrip'=>$descrip,
			'user'=>$user,
			'date'=>$date,
			'settings'=>$settings,
			'existing'=>true,
		);
	}	
	
	
	
	
	//VALIDATIONS
	public function displayUpdateLogOption($o) {
		if(isset($this->update_log_option[$o])) { // && $this->update_log_option[$o]===true) {
			return true;
		}
		return false;
	}
	
	
	
	
	

	public function drawPage($action) {
		//echo "<P>generate.drawPage()</p>";
		//Open Form
		//$this->arrPrint($this->quick_report['settings']); 
		echo "
		<style type=text/css>
		#sortable .ui-state-info { width: 60%; }
		body { font-size: 62.5%; }
		.progresslabel {    float: left;    margin-left: 5%;    margin-top: 5px;    font-weight: bold;    color: #ffffff; }
		#dlg_generate #progressbar .ui-progressbar-value { background-color: #009900; }
		.ok { color: #009900; border-color: #009900; }
		</style>
		<form name=frm_generator method=post action=/module/report_process.php>
		<input type=hidden name=act id=act value=GENERATE />
		<input type=hidden name=page id=page value=".$this->form_action." />
		<input type=hidden name=source_class id=source_class value=".$this->source_class." />
		";
		foreach($this->sections as $s) {
			$my_function = "self::".$s['func'];
			call_user_func($my_function,$s['title']);
		}
		//Close Form
		echo "</form>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<div id=dlg_generate title=Generating...>
			<p>Please be patient while the report generates.</p>
			<p>You will either be transferred to the report or prompted to save it once it has been generated.</p>
			<p class=i>Click \"OK\" once the report has been generated.</p>
			<div id=progressbar><div class=progresslabel>Processing...</div></div>
		</div>
		<script type=text/javascript>
		$(function() {
			$('form[name=frm_generator] label').css('cursor','hand');
			$('form[name=frm_generator] div').css({'margin-left':'20px','margin-right':'20px'});
			$('form[name=frm_generator] #div_sortable').css({'margin-left':'0px','margin-right':'0px'});
			$('#sortable').sortable({
				placeholder: \"ui-state-info\", 
				forcePlaceholderSize: true,
				forceHelperSize: true
			});
			$('#sortable').disableSelection();
			$('form[name=frm_generator] table.form th').css({'text-align':'left','vertical-align':'top'}).width(200);
			$('form[name=frm_generator] table.form').css('width','100%');
			$(\".datepicker\").datepicker({
				showOn: 'both',
				buttonImage: '../../library/jquery/css/calendar.gif',
				buttonImageOnly: true,
				dateFormat: 'd M yy',
				changeMonth:true,
				changeYear:true		
			});
			
			$(\"#btn_submit\").click(function() {
				$(\"#dlg_generate #progressbar\").progressbar({value: false});
				$(\"#dlg_generate\").show().dialog({
					buttons: [{ 
							text: 'OK',
							click: function() {
								$( this ).dialog('close');
							}
						}]
				}).dialog(\"widget\").find(\".ui-dialog-buttonpane button\").css({'color':'#009900','border-color':'#009900'}).end();

				$(\"form[name=frm_generator]\").submit();
			});
			
			".$this->script."
			$(\"#dlg_generate\").hide();
			".(isset($_REQUEST['quick_act']) && strtolower($_REQUEST['quick_act'])=="generate" ? "$(\"#btn_submit\").trigger(\"click\");" : "")."
		});
		</script>";
	}

	
	private function drawHeader($t) {
			$this->a++;
			echo "<h2>".$this->a.". ".$t."</h2>";
	}
	
	
	private function drawColumnChooser($title) {
		$this->drawHeader($title);
		$rows = ceil(count($this->columns)/3);
		$rows = $rows<5 ? 5 : $rows;
		echo "
		<div id=columnchooser>
			<table>
				<tr>
					<td>
						<table>
		";
		$r = 0;
		foreach($this->columns as $key => $c) {
			$r++;
			if($r>$rows) {
				echo "</table></td><td><table>";
				$r=1;
			}
			$s = false;
			if($this->quick_report['existing']===true) {
				if(isset($this->quick_report['settings']['columns'][$key]) && strtoupper($this->quick_report['settings']['columns'][$key])=="ON") { 
					$s = true; 
				}
			} elseif($c['selected']===true) {
				$s = true;
			}
			echo "
				<tr>
					<td><input type=checkbox name=columns[".$key."] ".($s===true ? "checked=checked" : "")." id=col_".$key." /></td>
					<td>";
			switch($key) {
			case "update_log":
				$uf = "ALL";
				if(isset($this->quick_report['settings']['update_filter']) && $this->displayUpdateLogOption(strtoupper($this->quick_report['settings']['update_filter']))) {
					$uf = strtoupper($this->quick_report['settings']['update_filter']);
				}
				echo "<select name=update_filter>";
				foreach($this->update_filter_options as $ukey=>$uval) {
					if($this->displayUpdateLogOption($ukey)) {
						echo "<option ".($uf==$ukey ? "selected" : "")." value=".$ukey.">".$uval."</option>";
					}
				}
				/*if($this->displayUpdateLogOption("ALL")) {
					echo "<option ".($uf=="ALL" ? "selected" : "")." value=ALL>Complete Update History</option>";
				}
				if($this->displayUpdateLogOption("LAST")) {
					echo "<option ".($uf=="LAST" ? "selected" : "")." value=LAST>Most Recent Update</option>";
				}
				if($this->displayUpdateLogOption("AUDIT")) {
					echo "<option ".($uf=="AUDIT" ? "selected" : "")." value=AUDIT>Complete Activity Log</option>";
				}*/
				echo "</select>";
				break;
			default:
				echo "<label for=col_".$key.">".$this->fields[$key]['heading']."</label>";
			}
			echo "	
					</td>
				</tr>
			";
		}
		echo "
						</table>
					</td>
				</tr>
			</table>
			<p><a href=# id=action_checkall>Check All</a> | <a href=# id=action_uncheckall>Uncheck All</a> | <a href=# id=action_invert>Invert Selection</a></p>
		</div>
		<script type=text/javascript>
		$(function() {
			$('form[name=frm_generator] #columnchooser table, form[name=frm_generator] #columnchooser table td').css('border-color','#FFFFFF');
			$('form[name=frm_generator] #columnchooser #action_uncheckall').click(function() {
				$('form[name=frm_generator] #columnchooser input:checkbox').prop('checked',false);
			});
			$('form[name=frm_generator] #columnchooser #action_checkall').click(function() {
				$('form[name=frm_generator] #columnchooser input:checkbox').prop('checked',true);
			});
			$('form[name=frm_generator] #columnchooser #action_invert').click(function() {
				$('form[name=frm_generator] #columnchooser input:checkbox').each(function() {
					if($(this).is(':checked')) {
						$(this).prop('checked',false);
					} else {
						$(this).prop('checked',true);
					}
				});
			});
		});
		</script>
		";
	}
	
	private function drawFilters($title) {
		$this->drawHeader($title);
		echo "
		<div id=filters>
			<table class=form>";
		foreach($this->filters as $key=>$f) {
			$type = $this->fields[$key]['type'];
			$filter_type = "self::draw".ucfirst($type)."Filter";
			echo "
				<tr>
					<th>".$this->fields[$key]['heading'].":</th>
					<td>".call_user_func(array($this,$filter_type),$key,$f['data'],$f['default'])."</td>
				</tr>
			";
		}
		echo "
			</table>
		</div>
		<script type=text/javascript>
		$(function() {
			".$this->filter_script."
		});
		</script>";
	}
	
	private function drawGroupSortOptions($title) {
		$this->drawHeader($title);
		$groupby = "X";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['group_by'])) {
			$groupby = $this->quick_report['settings']['group_by'];
		}
		echo "
		<div id=groupandsort>
			<table class=form>
				<tr>
					<th>Group by:</th>
					<td><select name=group_by><option ".($groupby=="X" ? "selected" : "")." value=X>No Grouping</option>";
		foreach($this->group_by as $g => $k) {
			if($k===true) {
				echo "<option ".($groupby==$g ? "selected" : "")." value=".$g.">".$this->fields[$g]['heading']."</option>";
			}
		}
		echo "
					</select></td>
				</tr><tr>
					<th>Sort by:</th>
					<td><ul id=sortable>";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['sort'])) {
			$sort = $this->quick_report['settings']['sort'];
		} else {
			ksort($this->sort_order);
			$sort = $this->sort_order;
		}
		//$a = $this->sort_order; ksort($a);
		//$this->arrPrint($a);
		foreach($sort as $s) {
			if($this->sort_by[$s]===true) {
				echo "<li class=\"ui-state-default\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;".$this->fields[$s]['heading']."<input type=hidden name=sort[] value='$s' /></li>";
			}
		}
		echo "
					</ul></td>
				</tr>
			</table>
		";
		
		echo "</div>
		<script type=text/javascript>
		$(function() {
			$('form[name=frm_generator] #groupandsort #sortable li').css('cursor','hand').css('width','60%');
		});
		</script>";
	}
	
	private function drawGraphGroupOptions($title) {
		$this->drawHeader($title);
		$groupby = "X";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['group_by'])) {
			$groupby = $this->quick_report['settings']['group_by'];
		}
		echo "
		<div id=groupandsort>
			<table class=form>
				<tr>
					<th>Group by:</th>
					<td><select name=group_by><option ".($groupby=="X" ? "selected" : "")." value=X>No Grouping</option>";
		foreach($this->group_by as $g => $k) {
			if($k===true) {
				echo "<option ".($groupby==$g ? "selected" : "")." value=".$g.">".$this->fields[$g]['heading']."</option>";
			}
		}
		echo "
					</select></td>
				</tr>
			</table>
		";
		
		echo "</div>
		<script type=text/javascript>
		$(function() {
			$('form[name=frm_generator] #groupandsort #sortable li').css('cursor','hand').css('width','60%');
		});
		</script>";
	}	
	
	
	
	private function drawFormatSelection($title) {
		$this->drawHeader($title);
		echo "<div id=documentformat>";
		foreach($this->formats as $key=>$f) {
			if($this->availableformats[$key]===true) {
				echo "<p><input type=radio name=output value=$key id=output_".$key." >&nbsp;<label for=output_".$key.">".$f."</label></p>";
			}
		}
		echo "</div>";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['output'])) {
			$this->script.="
				$('#output_".$this->quick_report['settings']['output']."').prop('checked','checked');
			";
		} else {
			$this->script.="
				$('#documentformat input:radio:eq(0)').attr('checked','checked');
			";
		}
	}
	
	private function drawGenerateButtons($title) {
		$this->drawHeader($title);
		echo "
		<div id=generate>
			<p><span class=b>Report Title:</span> <input type=text name=rhead value=\"".($this->quick_report['existing']===true ? $this->quick_report['settings']['rhead'] : "")."\" maxlength=100 size=70> <span class=i>(Displays at the top of the report.)</span>
			<p><input type=button value=\"Generate Report\" class=isubmit id=btn_submit>&nbsp;<input type=reset value=Reset></p>
			";
		//If quick reports are enabled then display option to save
		if($this->availableformats['excel']===true) {
			echo "
			<table width=700>
			<tr>
			<td style=\"font-size:8pt;border:1px solid #AAAAAA;\">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
			<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
			<li>When opening this document in Microsoft Excel <u>2007 or later</u>, you might receive the following warning message: <br />
			<span class=i>\"The file you are trying to open is in a different format than specified by the file extension.
			Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?\"</span><br />
			This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel. 
			It is safe to click on the \"Yes\" button to open the document.</li>
			</ol></td>
			</tr></table>";
		}
		echo "</div>";
	}
	
	private function drawSaveQuickReport($title) {
		if($this->quick===true) {
			$this->drawHeader($title);
			$dr = $this->getJSdisplayResultPrep("");
			$dr = explode("<script type=text/javascript>",$dr);
			$dr2 = substr($dr[1],0,strlen($dr[1])-9);
			echo "
			<div id=saveQuick><input type=hidden name=quick_act id=quick_act value='create' /><input type=hidden name=quick_class class=quick id=quick_class value='".$this->quick_class."' />
				<table class=form>
					<tr>
						<th>Report Reference:</th>
						<td><input type=hidden name=quick_id class=quick id=quick_id value='".$this->quick_report['id']."' /><label for=rid id=lbl_rid>".($this->quick_report['existing']===true ? $this->quick_report['id'] : "N/A")."</label></td>
					</tr>
					<tr>
						<th>Report Name:*</th>
						<td><input type=text name=quick_name id=quick_name class=quick size=50 value='".$this->quick_report['name']."' /></td>
					</tr>
					<tr>
						<th>Description:</th>
						<td><textarea name=quick_descrip id=quick_descrip class=quick cols=50 rows=4>".$this->quick_report['descrip']."</textarea></td>
					</tr>
					<tr>
						<th></th>
						<td>";
						if($this->quick_report['existing']===true) {
							echo "
							<input type=button value=\"Save Changes\" id=update class=quick>&nbsp;
							<input type=button value=\"Save As New Report\" id=create class=quick>&nbsp;
							<input type=button value=\"Delete Report\" id=delete class=quick>&nbsp;";
						} else {
							echo "<input type=button value=\"Save As New Report\" id=create class=quick>&nbsp;";
						}
			echo "
						</td>
					</tr>
				</table>
			</div>
			<div id=dlg_quick title='Processing Quick Report...'>
				".$dr[0]."
			</div>
			";
			$this->script.=$dr2."
			$('#dlg_quick').hide();
			$('#saveQuick input:button.quick').click(function() {
				var act = $(this).prop(\"id\");
				//alert(act);
				if($('#quick_name').val().length==0 && act != 'delete') {
					alert('Please enter a name for your Quick Report.');
				} else {
					$('#saveQuick #quick_act').val(act);
					//alert($('#saveQuick #quick_act').val());
					$('#dlg_quick').show();
					JSdisplayResult('info','info','Your Quick Report request is being processed.');
					$('#dlg_quick').dialog({
						modal: true,
						closeOnEscape: false,
						open: function(event, ui) { 
							$(\".ui-dialog-titlebar-close\", this.parentNode).hide(); 
						},
						buttons: [{ 
								text: 'OK',
								click: function() {
									$( this ).dialog('close');
								}
							}]
					}).dialog(\"widget\").find(\".ui-dialog-buttonpane button\").hide().end();
					$.ajax({
						type: 'POST',
						url: 'ajax_quick_process.php',
						data: $('form[name=frm_generator]').serialize(),
						success: quickSuccess,
						dataType: 'json'
					});
				}
			});
			function quickSuccess(data) {
				//alert(data[0]);
				//alert(data[1]);
				//$('#dlg_quick').show();
				JSdisplayResult('reset','reset','');
				JSdisplayResult(data[0],data[0],data[1]);
				$('#dlg_quick').dialog(\"widget\").find(\".ui-dialog-buttonpane button\").show().css({'color':'#009900','border-color':'#009900'}).end();
			}
			";
		}



	}
	
	
	
	
	/** FILTERS **/
	private function drawDateFilter($id,$data,$def) {
		$echo =""; 
		$def2 = "ALL";
		$data2["ALL"] = "Doesn't Matter";
		$data2["X"] = $this->getUnspecified();
		$data2["1"] = "";
		foreach($data2 as $key => $d) {
			$e[]= "<input type=radio name=filter[".$id."] ".( $def2 === $key  ? "checked" : "")." value=$key id=".$id."_".$key." /><label for=".$id."_".$key.">".$d."</label>";
		}
		$echo = implode("<br />",$e);
		
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['filter'][$id])) { $def = $this->quick_report['settings']['filter'][$id]; $def[0] = $def['from']; $def[1] = $def['to']; }
		$echo.= "From <input type=text size=10 readonly=readonly class=datepicker name=filter[".$id."][from] id=filter_".$id."_from value='";
		if(is_array($def)) {
			if(isset($def[0]) && strlen($def[0])>0) {
				$echo.=$this->formatDateForDisplay($def[0]);
			}
		} elseif(strlen($def)>0) {
			$echo.=$this->formatDateForDisplay($def);
		}
		$echo.="' /> to <input type=text size=10 readonly=readonly class=datepicker name=filter[".$id."][to] id=filter_".$id."_to value='";
		if(is_array($def)) {
			if(isset($def[1]) && strlen($def[1])>0) {
				$echo.=$this->formatDateForDisplay($def[1]);
			}
		} elseif(strlen($def)>0) {
			$echo.=$this->formatDateForDisplay($def);
		}
		$echo.="' />
		<input type=button value=Clear id=filter_".$id." class=clear_date />";
		$this->scriptClearDate();
		if(is_array($data)) {
			foreach(array("from","to") as $a) {
				if(strlen($data[0])>0) {
					$this->restrictDateFilter("min","#filter_".$id."_".$a,$data[0]);
				}
				if(strlen($data[1])>0) {
					$this->restrictDateFilter("max","#filter_".$id."_".$a,$data[1]);
				}
			}
		}
		return $echo;
	}
	private function formatDateForDisplay($d) {
		$echo = "";
		if($d=="today") {
			$echo = date("d M Y");
		} elseif($this->db_date_format=="STRING") {
			$echo = date("d M Y",strtotime($d));
		} elseif($this->db_date_format=="UNIX" && is_numeric($d)) {
			$echo = date("d M Y",$d);
		}
		return $echo;
	}
	private function restrictDateFilter($type,$fld_id,$d) {
			if($d=="today") {
				$d = time();
			} elseif($this->db_date_format=="STRING") {
				$d = strtotime($d);
			}
			$this->script.="
				$('form[name=frm_generator] #filters ".$fld_id."').datepicker(\"option\",\"".$type."Date\",new Date(\"".date("d F Y",$d)."\"));
			";
	}
	private function scriptClearDate() {
		if($this->has_clear_date_been_scripted===false) {
			$this->filter_script.="
			$('form[name=frm_generator] input:button.clear_date').click(function() {
				var i = $(this).prop('id'); 
				$('form[name=frm_generator] #filters #'+i+'_from').val('');
				$('form[name=frm_generator] #filters #'+i+'_to').val('');
			});
			";
			$this->has_clear_date_been_scripted = true;
		}
	}
	
	private function drawTextFilter($id,$data,$def) {
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['filter'][$id])) { 
			$def = $this->quick_report['settings']['filter'][$id]; 
			$deftype = isset($this->quick_report['settings']['filtertype'][$id]) ? $this->quick_report['settings']['filtertype'][$id] : "ANY";
		} else {
			$deftype = "ANY";
		}

		return "<input type=text size=50 name=filter[".$id."]"." id=filter_".$id." value='$def' /> <select name=filtertype[".$id."]><option ".($deftype=="ANY" ? "selected" : "")." value=ANY>Match any words</option><option ".($deftype=="ALL" ? "selected" : "")." value=ALL>Match all words</option><option ".($deftype=="EXACT" ? "selected" : "")." value=EXACT>Match exact phrase</option></select>";
	}
	
	private function drawNumFilter($id,$data,$def) {	return $this->drawTextFilter($id,$data,$def);	}
	private function drawRefFilter($id,$data,$def) {	return $this->drawTextFilter($id,$data,$def);	}
	private function drawUpdateFilter($id,$data,$def) {	return $this->drawTextFilter($id,$data,$def);	}
	private function drawSmltextFilter($id,$data,$def) {	return $this->drawTextFilter($id,$data,$def);	}
	private function drawLinkFilter($id,$data,$def) {	return $this->drawTextFilter($id,$data,$def);	}
	private function drawMultitextFilter($id,$data,$def) {	return $this->drawTextFilter($id,$data,$def);	}
	private function drawTextlistFilter($id,$data,$def) { return $this->drawListFilter($id,$data,$def); }
	private function drawMultilistFilter($id,$data,$def) { return $this->drawListFilter($id,$data,$def); }
	private function drawResultFilter($id,$data,$def) { return $this->drawListFilter($id,$data,$def); }
	private function drawListFilter($id,$data,$def) {
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['filter'][$id])) { $def = $this->quick_report['settings']['filter'][$id]; }
		if(!isset($def) || (!is_array($def) && (strlen($def)==0 || strtoupper($def)=="X")) || (is_array($def) && (strtoupper($def[0])=="X" || in_array("X",$def)))) { 
			$def = array("X"=>0); 
		} elseif(is_array($def)) {
			$def = array_flip($def);
		} elseif(!is_array($def)) {
			$def = array($def);
			$def = array_flip($def);
		}
		$size=11;
		$x = count($data)+2;
		if($x<$size) { $size = $x; }
		$echo =  "<select multiple name=filter[".$id."][] size=$size><option ".(isset($def["X"]) ? "selected"  : "")." value=X>--- ANY OPTION ---</option>";
		if(is_array($data)) {
			foreach($data as $key => $d) {
				$echo.= "<option ".( (is_array($def) && isset($def[$key]) ) ? "selected" : "")." value=$key>".$d."</option>";
			}
		}
		$echo.= "</select>";
//$echo.="<ul>";foreach($data as $k => $d) { $echo.="<li>".$k." => ".$d."<br />selected validation => ::".(( (is_array($def) && in_array($key,$def)) || (!is_array($def) && ($def===$key) || ((int)$def===$key) ) ))."</li>"; } $echo.="</ul>";
		return $echo;
	}
	
	
	private function drawSmllistFilter($id,$data,$def) {
		$echo = "";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['filter'][$id])) { $def = $this->quick_report['settings']['filter'][$id]; }
		$e = array();
		if(is_array($data)) {
			foreach($data as $key => $d) {
				$e[]= "<input type=radio name=filter[".$id."] ".( $def == $key  ? "checked" : "")." value=$key id=".$id."_".$key." /><label for=".$id."_".$key.">".$d."</label>";
			}
		}
		$echo = implode("<br />",$e);
		
//$echo.="<ul>";foreach($data as $k => $d) { $echo.="<li>".$k." => ".$d."<br />selected validation => ::".(( (is_array($def) && in_array($key,$def)) || (!is_array($def) && ($def===$key) || ((int)$def===$key) ) ))."</li>"; } $echo.="</ul>";
		return $echo;
	}
	
	private function drawBoolFilter($id,$data=array(),$def="ALL") {
		$def = strlen($def)==0 ? "ALL" : $def;
		$data["ALL"] = "Doesn't Matter";
		$data["X"] = $this->getUnspecified();
		$data["1"] = "Yes"; 
		$data["0"] = "No"; 
		$echo = "";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['filter'][$id])) { $def = $this->quick_report['settings']['filter'][$id]; }
		$e = array();
		if(is_array($data)) {
			foreach($data as $key => $d) {
				$e[]= "<input type=radio name=filter[".$id."] ".( $def === $key  ? "checked" : "")." value=$key id=".$id."_".$key." /><label for=".$id."_".$key.">".$d."</label>";
			}
		}
		$echo.= implode("&nbsp;&nbsp;&nbsp;",$e);
		
//$echo.="<ul>";foreach($data as $k => $d) { $echo.="<li>".$k." => ".$d."<br />selected validation => ::".(( (is_array($def) && in_array($key,$def)) || (!is_array($def) && ($def===$key) || ((int)$def===$key) ) ))."</li>"; } $echo.="</ul>";

		return $echo;
	}
		
	private function drawAttachFilter($id) {
		$def = "ALL";
		$data["ALL"] = "Doesn't Matter";
		$data[1] = "Must have attachments";
		$data[0] = "Must not have attachments";
		$echo = "";
		if($this->quick_report['existing']===true && isset($this->quick_report['settings']['filter'][$id])) { $def = $this->quick_report['settings']['filter'][$id]; }
		$e = array();
		if(is_array($data)) {
			foreach($data as $key => $d) {
				$e[]= "<input type=radio name=filter[".$id."] ".( $def === $key  ? "checked" : "")." value=$key id=".$id."_".$key." /><label for=".$id."_".$key.">".$d."</label>";
			}
		}
		$echo = implode("<br />",$e);
		
		return $echo;
	}
	
	
	
	
}


?>