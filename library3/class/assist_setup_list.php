<?php
/*******************************
For use in the management of MASTER SETUP lists on Ignite Assist
Created by: Janet Currie [JC]
Created on: 2 October 2012

Weightings: added on 2 October 2012 [JC]
Competencies: added on 2 October 2012 [JC]
Proficiencies: added on 2 October 2012 [JC]
*******************************/
class ASSIST_SETUP_LIST extends ASSIST {
	
	private $slist;		//which list is being worked on
	
	private $db;
	
	private $cmpcode = "";
	private $dbref = "";
	private $select_all = "";
	private $section = "";
	private $title = "";
	private $ref_fld = "id"; 
	private $val_fld = "value"; 
	private $status_fld = "status"; 
	private $delete_status = 0;
	private $can_sort = true; 
	private $sort_fld = ""; 
	private $sort_sql = ""; 
	private $get_old_sort = "";
	private $fields = array();
	private $structure = array();
	private $action = "C";
	private $validate_delete = false;
	
	private $list_items;
	private $list_titles = array(
		'weightings'=>"Weighting",
		'competencies'=>"Competency",
		'proficiencies'=>"Proficiency",
		'orgpositions'=>"Organisational Position",
		'joblevels'=>"Job Level",
		'jobtitles'=>"Job Title",
	);
		
	public function __construct($l="") {
		parent::__construct();
		$this->cmpcode = $this->getCmpCode();
		$this->db = new ASSIST_DB();
		$this->setList($l);
	}
/** SET FUNCTIONS **/
	function setList($l) {
	//echo $l;
		if(strlen($l)>0) {
			$this->title = isset($this->list_titles[$l]) ? $this->list_titles[$l] : "Unknown List";
			$this->slist = $l;
			switch($l) {
				case "weightings": $this->createWeightingsList(); break;
				case "competencies": $this->createCompetencyList(); break;
				case "proficiencies": $this->createProficiencyList(); break;
				case "orgpositions": $this->createOrganisationalPositionList(); break;
				case "joblevels": $this->createJobLevelsList(); break;
				case "jobtitles": $this->createJobTitlesList(); break;
				default: $this->createBlankList(); break;
			}
		} else {
			$this->createBlankList();
		}
	}
	function setAction($a) { $this->action = $a; }


/** GET FUNCTIONS **/
	function getTitle() { return $this->title; }
	function getDBref() { return $this->dbref; }
	function getList() { return $this->slist; }
	function getLogSection() { return $this->section; }
	function getRefFld() { return $this->ref_fld; }
	function getTitleFld() { return $this->val_fld; }
	function getStatusFld() { return $this->status_fld; }
	function CanISort() { return $this->can_sort; }
	function getSortFld() { return $this->sort_fld; }
	function getDeleteStatus() { return $this->delete_status; }
	function getFields() { return $this->fields; }
	function getStructure() { return $this->structure; }
	
	function getAllItemsForDisplay() {
		$sql = $this->select_all;
		$all = $this->db->mysql_fetch_all($sql);
		return $all;
	}
	function getAllItemsForSort() {
		$sort = $this->db->mysql_fetch_all($this->sort_sql);
		return $sort;
	}
	function getOneItem($id) {
		if($this->checkIntRef($id)) {
			$sql = "SELECT * FROM ".$this->dbref." WHERE ".$this->ref_fld." = ".$id;
			$one = $this->db->mysql_fetch_one($sql);
			foreach($one as $key=>$v) {
				$one[$key] = $this->decode($v);
			}
			return $one;
		} else {
			return false;
		}
		
	}
	function getOldSortItems() {
		if($this->canISort()===true && strlen($this->get_old_sort)>0) {
			$all = $this->db->mysql_fetch_all($this->get_old_sort);
			return $all;
		} else {
			return false;
		}
	}
	function getListItems($fld) {
		return $this->list_items[$fld];
	}
	function getListItemForDisplay($key,$i) {
		if($this->checkIntRef($i)) {
			switch($this->slist) {
				case "jobtitles": 
				case "joblevels": 
					$v = $this->list_items[$key][$i]; 
					break;
				case "weightings": 
				case "competencies": 
				case "proficiencies": 
				case "orgpositions": 
				default: 
					$v = ""; 
					break;
			}
			return $v;
		} else {
			return "";
		}
	}
	function getItemDetailsForEdit($id) {
		$data = $this->getOneItem($id);
		if($this->validate_delete===true) {
			switch($this->slist) {
				case "weightings": 		$d = $this->weightingsDeleteValidation($id); break;
				case "competencies": 	$d = $this->competencyDeleteValidation($id); break;
				case "proficiencies": 	$d = $this->proficiencyDeleteValidation($id); break;
				case "orgpositions": 	$d = $this->organisationalPositionDeleteValidation($id); break;
				case "joblevels": 		$d = $this->jobLevelsDeleteValidation($id); break;
				case "jobtitles": 		$d = $this->jobTitlesDeleteValidation($id); break;
				default: 				$d = $this->defaultDeleteValidation($id); break;
			}
		
			$data['can_be_deleted'] = $d[0];
			$data['del_msg'] = $d[1];
		} else {
			$data['can_be_deleted'] = "1";
			$data['del_msg'] = "";
		}
		return $data;
	}
	
	function getSQLSelectAll() { return $this->select_all; }
	function getSQLSort() { return $this->sort_sql; }
	
	
	
	
	
/** ACTION FUNCTIONS **/
	function addNewItem() {
		$err = false;
		$s = array();
		$l = array();
		$f = array();
		$this->action = "C";
		foreach($this->structure as $key => $a) {
			$f[] = $key;
			switch($a['src']) {
			case "DEFAULT":
				$s[] = $a['default'];
				break;
			case "FORM":
				$v = $this->code($_REQUEST[$key]);			$_REQUEST[$key] = $v;
				if(strlen($v)==0 && $a['required']) {
					$err = true;
					break;
				} else {
					$s[] = "'".$v."'";
					$l[] = $v;
				}
				break;
			}
		}
		if(!$err) {
			$sql = "INSERT INTO ".$this->dbref." (".implode(",",$f).") VALUES (".implode(",",$s).")";
			$id = $this->db->db_insert($sql);
			if($this->checkIntRef($id)) {
				$this->addItemToOldDB($id);
				$this->logMe($id,serialize($f),"Added new ".$this->title." $id: \'".$_REQUEST[$this->val_fld]."\'","",serialize($l),$sql,"master");
				$result = array("ok","New ".$this->title." '".$_REQUEST[$this->val_fld]."' has been added with reference $id.");
			} else {
				$result = array("error","New ".$this->title." was not added successfully.  Please try again.");
			}
		} else {
			$result = array("error","New ".$this->title." could not be added as there was missing information.  Please try again.");
		}
		return $result;
	}
	function editItem() {
		$err = false;
		$s = array();
		$l = array();
		$f = array();
		$this->action = "E";
		$changes = array();
		$id = $_REQUEST['id'];
		if($this->checkIntRef($id)) {
			$old = $this->db->mysql_fetch_one("SELECT * FROM ".$this->dbref." WHERE ".$this->ref_fld." = $id");
			foreach($this->structure as $key => $a) {
				switch($a['src']) {
				case "FORM":
					$v = $this->code($_REQUEST[$key]);		$_REQUEST[$key] = $v;
					if($v!=$old[$key]) {
						if(strlen($v)==0 && $a['required']) {
							$err = true;
							break;
						} else {
							$f[] = $key;
							$s[] = $key." = '".$v."'";
							$l[] = $v;
							if($a['type']=="LIST") {
								$ov = $this->getListItemForDisplay($key,$old[$key]);
								$nv = $this->getListItemForDisplay($key,$v);
							} else {
								$ov = $old[$key];
								$nv = $v;
							}
							$changes[] = "- Changed ".$a['title']." to '$nv' from '$ov'";
						}
					}
					break;
				}
			}
			if(!$err) {
				if(count($s)>0) {
					$sql = "UPDATE ".$this->dbref." SET ".implode(",",$s)." WHERE ".$this->ref_fld." = $id";
					$mar = $this->db->db_update($sql);
					if($mar>0) {
						$this->editItemInOldDB($id);
						$this->logMe($id,serialize($f),addslashes("Edited ".$this->title." $id:<br />".implode("<br />",$changes)),serialize($old),serialize($l),$sql,"master");
						$result = array("ok","".$this->title." $id '".$_REQUEST[$this->val_fld]."' has been saved successfully.");
					} else {
						$result = array("error","".$this->title." $id was not edited successfully.  Please try again.");
					}
				} else {
					$result = array("error","".$this->title." $id was not edited successfully - no changes were found.  Please try again.");
				}
			} else {
				$result = array("error","".$this->title." $id could not be edited as there was missing information.  Please try again.");
			}
		} else {
			$result = array("error","An error occurred while trying to edit the selected ".$this->title.".  Please try again.");
		}
		return $result;
	}
	function deleteItem() {
		$this->action = "D";
		$id = $_REQUEST['id'];
		if($this->checkIntRef($id)) {
			$old = $this->db->mysql_fetch_one("SELECT * FROM ".$this->dbref." WHERE ".$this->ref_fld." = $id");
			$sql = "UPDATE ".$this->dbref." SET ".$this->status_fld." = ".$this->delete_status." WHERE ".$this->ref_fld." = $id";
			$mar = $this->db->db_update($sql); 
			if($mar>0) { 
				$this->delItemInOldDB($id);
				$this->logMe($id,$this->status_fld,addslashes("Deleted ".$this->title." $id: '".$old[$this->val_fld]."'"),$old[$this->status_fld],$this->delete_status,$sql,"master");
				$result = array("ok","".$this->title." $id has been successfully deleted.");
			} else {
				$result = array("info","".$this->title." $id could not be deleted.  It is possible that it has already been deleted.");
			}
		} else {
			$result = array("error","An error occurred while trying to delete the selected ".$this->title.".  Please try again.");
		}
		return $result;
	}
	function changeSort() {
		$sort = $_REQUEST['sort'];
		if(count($sort)>0) {
			$old = $this->getOldSortItems();
			$sql = array();
			$new = array();
			foreach($sort as $s=>$i) {
				$new[] = $i." => ".$s;
				$sql[] = "UPDATE ".$this->dbref." SET ".$this->sort_fld." = $s WHERE ".$this->ref_fld." = $i";
			}
			foreach($sql as $s) {
				$mar = $this->db->db_update($s);
			}
			$this->logMe(0,$this->sort_fld,addslashes("".$this->title." items were rearranged."),serialize($new),serialize($old),serialize($sql));
			$result = array("ok","".$this->title." items have been successfully sorted.");
		} else {
			$result = array("error","The ".$this->title." items could not be sorted - there were no items to sort.");
		}
		return $result;
	}







/** LOGGING FUNCTIONS **/
	private function createLog($l,$table="") {
		$tkid = $this->getUserID();
		$cmpcode = $this->cmpcode;
		$tkn = $this->getUserName();
		if(!isset($table) || strlen($table)==0) { $table = "setup"; }
		$sql = "INSERT INTO assist_".$cmpcode."_".$table."_log 
				(id, date, tkid, tkname, section, ref, action, field, transaction, old, new, active, lsql)
				VALUES
				(null, now(), '$tkid', '$tkn', '".$l['sec']."', '".$l['ref']."', '".$l['action']."', '".$l['fld']."', '".$l['trans']."', '".$l['old']."', '".$l['new']."', ".$l['active'].", '".$l['sql']."')";
		$this->db->db_insert($sql);
	}
	function logMe($id,$fld,$trans,$old,$new,$sql,$table="setup",$active="true") {
		$section = $this->section;
		$action = $this->action;
				$l = array(
					'sec'=>		$section,
					'ref'=>		$id,
					'action'=>	$action,
					'fld'=> 	$fld,
					'trans'=> 	$trans,
					'old'=>		$old,
					'new'=>		$new,
					'sql'=> 	addslashes($sql),
					'active'=>	isset($active) && strlen($active)>0 ? $active : "true",
				);
		$this->createLog($l,$table);
	}
	function displaySetupActivityLog() {
		$log_sql = "SELECT l.date, l.tkname, l.transaction 
					FROM assist_".$this->cmpcode."_setup_log l
					WHERE l.active = true AND l.section = '".$this->getLogSection()."' 
					ORDER BY l.id DESC";
		$log_data = $this->db->mysql_fetch_all($log_sql);
		$this->displayAuditLog($log_data,array('date'=>"date",'user'=>"tkname",'action'=>"transaction"),$this->getGoBack("main.php"));
	}
	
	
	
	
	
	
	
	
	
	
/** DETAILS FOR SPECIFIC LISTS **/
	private function createBlankList() {
		$this->dbref = "assist_".$this->cmpcode."_master_list_";
		$this->select_all = "SELECT * FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value";
		$this->section = "SETUP";
		$this->ref_fld = "id"; 
		$this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = false;
		$this->can_sort = true; 
			$this->sort_fld = "sort"; 
			$this->sort_sql = "SELECT id as id, value as value FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value"; 
			$this->get_old_sort = "SELECT CONCAT(id,' => ',sort) as old FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value";
		$this->fields = array("id","value","status","sort");
		$this->list_items = array();
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Title",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>100,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'sort'=>array(
				'title'=>"Order",				'display'=>false,				'default'=>"9999",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);
	}
	private function createWeightingsList() {
	//echo "Creating a weighting list";
		$this->dbref = "assist_".$this->cmpcode."_master_list_weightings";
		$this->select_all = "SELECT * FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, weight, value";
		$this->section = "WGHT";
		$this->ref_fld = "id"; 
		$this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = false;
		$this->can_sort = true; 
			$this->sort_fld = "sort"; 
			$this->sort_sql = "SELECT id as id, CONCAT(value,' (',weight,')') as value FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, weight, value"; 
			$this->get_old_sort = "SELECT CONCAT(id,' => ',sort) as old FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, weight, value";
		$this->fields = array("id","value","description","weight","status","sort");
		$this->list_items = array();
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Title",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>100,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'description'=>array(
				'title'=>"Description",				'display'=>true,				'default'=>"",				'type'=>"TEXT",				'length'=>0,				'src'=>"FORM",				'required'=>false,				'cell'=>"TD",				'class'=>"",
			),
			'weight'=>array(
				'title'=>"Weight",				'display'=>true,				'default'=>"1",				'type'=>"NUM",				'length'=>11,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'sort'=>array(
				'title'=>"Order",				'display'=>false,				'default'=>"9999",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);
	}
	private function weightingsDeleteValidation($i) {
		return array("1","");
	}
	
	private function createCompetencyList() {
		$this->dbref = "assist_".$this->cmpcode."_master_competencies";
		$this->select_all = "SELECT * FROM ".$this->dbref." WHERE status = 1 ORDER BY value";
		$this->section = "COMP";
		$this->ref_fld = "id"; $this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = false;
		$this->can_sort = false; 
			$this->sort_fld = ""; 
			$this->sort_sql = ""; 
			$this->get_old_sort = "";
		$this->fields = array("id","value","description","status");
		$this->list_items = array();
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Competency",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>200,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'description'=>array(
				'title'=>"Description",				'display'=>true,				'default'=>"",				'type'=>"TEXT",				'length'=>0,				'src'=>"FORM",				'required'=>false,				'cell'=>"TD",				'class'=>"",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);

	}
	private function competencyDeleteValidation($i) {
		return array("1","");
	}
	
	private function createProficiencyList() {
		$this->dbref = "assist_".$this->cmpcode."_master_proficiencies";
		$this->select_all = "SELECT * FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value";
		$this->section = "PROF";
		$this->ref_fld = "id"; 
		$this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = false;
		$this->can_sort = true; 
			$this->sort_fld = "sort"; 
			$this->sort_sql = "SELECT id as id, value as value FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value"; 
			$this->get_old_sort = "SELECT CONCAT(id,' => ',sort) as old FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value";
		$this->fields = array("id","value","description","status","sort");
		$this->list_items = array();
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Title",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>200,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'description'=>array(
				'title'=>"Description",				'display'=>true,				'default'=>"",				'type'=>"TEXT",				'length'=>0,				'src'=>"FORM",				'required'=>false,				'cell'=>"TD",				'class'=>"",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'sort'=>array(
				'title'=>"Order",				'display'=>false,				'default'=>"9999",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);
	}
	private function proficiencyDeleteValidation($i) {
		return array("1","");
	}
	
	private function createOrganisationalPositionList() {
		$this->dbref = "assist_".$this->cmpcode."_master_list_orgposition";
		$this->select_all = "SELECT * FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, value";
		$this->section = "ORGPOS";
		$this->ref_fld = "id"; 
		$this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = true;
		$this->can_sort = true; 
			$this->sort_fld = "sort"; 
			$this->sort_sql = "SELECT id as id, CONCAT(orglevel,': ',value) as value FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, orglevel, value"; 
			$this->get_old_sort = "SELECT CONCAT(id,' => ',sort) as old FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, orglevel, value";
		$this->fields = array("id","value","orglevel","description","status","sort","assistid");
		$this->list_items = array();
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Title",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>100,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'orglevel'=>array(
				'title'=>"Organisational Level",				'display'=>true,				'default'=>"1",				'type'=>"NUM",				'length'=>10,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'description'=>array(
				'title'=>"Core Job Outputs",				'display'=>true,				'default'=>"",				'type'=>"TEXT",				'length'=>0,				'src'=>"FORM",				'required'=>false,				'cell'=>"TD",				'class'=>"",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'sort'=>array(
				'title'=>"Order",				'display'=>false,				'default'=>"9999",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'assistid'=>array(
				'title'=>"Ignite Assist Default",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);
	}
	private function organisationalPositionDeleteValidation($i) {
		$sql = "SELECT * FROM assist_".$this->cmpcode."_master_list_joblevel WHERE orgpositionid = $i AND status = 1";
		$mnr = $this->db->db_get_num_rows($sql);
		if($mnr>0) {
			$d = array("0","This ".$this->title." cannot be deleted as there is at least one ".$this->list_titles['joblevels']." currently associated with it.");
		} else {
			$d = array("1","");
		}
		return $d;
	}





/* JOB LEVELS */	
	private function createJobLevelsList() {
		$this->dbref = "assist_".$this->cmpcode."_master_list_joblevel";
		$this->select_all = "SELECT jl.id, CONCAT(op.value,' (Org. Level: ',op.orglevel,')') as orgpositionid, jl.joblevel, jl.value, jl.status, jl.sort, jl.assistid
								FROM ".$this->dbref." jl
								INNER JOIN assist_".$this->cmpcode."_master_list_orgposition op
								ON op.id = jl.orgpositionid AND op.status = 1
								WHERE jl.status = 1 ORDER BY jl.sort, op.sort, jl.joblevel, jl.value, op.orglevel, op.value";
		$this->section = "JOBLEV";
		$this->ref_fld = "id"; 
		$this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = true;
		$this->can_sort = true; 
			$this->sort_fld = "sort"; 
			$this->sort_sql = "SELECT jl.id, CONCAT(op.value,': ',jl.value,' (Level: ',joblevel,')') as value
								FROM ".$this->dbref." jl
								INNER JOIN assist_".$this->cmpcode."_master_list_orgposition op
								ON op.id = jl.orgpositionid AND op.status = 1
								WHERE jl.status = 1 ORDER BY jl.sort, op.sort, jl.joblevel, jl.value, op.orglevel, op.value"; 
			$this->get_old_sort = "SELECT CONCAT(id,' => ',sort) as old FROM ".$this->dbref." WHERE status = 1 ORDER BY sort, joblevel, value";
		$this->fields = array("id","value","joblevel","orgpositionid","status","sort","assistid");
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'orgpositionid'=>array(
				'title'=>"Organisational Position",				'display'=>true,				'default'=>"",				'type'=>"LIST",				'length'=>0,				'src'=>"FORM",				'required'=>false,				'cell'=>"TD",				'class'=>"",
			),
			'joblevel'=>array(
				'title'=>"Job Level",				'display'=>true,				'default'=>"1",				'type'=>"NUM",				'length'=>10,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Job Level Name",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>100,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'sort'=>array(
				'title'=>"Order",				'display'=>false,				'default'=>"9999",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'assistid'=>array(
				'title'=>"Ignite Assist Default",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);
		$this->list_items = array('orgpositionid'=>array());
		$sql = "SELECT id as id, CONCAT(op.value,' (Org. Level: ',op.orglevel,')') as value FROM assist_".$this->cmpcode."_master_list_orgposition op WHERE op.status = 1 ORDER BY op.sort, op.orglevel, op.value";
		$mylist = $this->db->mysql_fetch_fld2_one($sql,"id","value");
		$this->list_items['orgpositionid'] = $mylist;
	}
	private function jobLevelsDeleteValidation($i) {
		$sql = "SELECT * FROM assist_".$this->cmpcode."_master_list_jobtitle WHERE joblevelid = $i AND status = 1";
		$mnr = $this->db->db_get_num_rows($sql);
		if($mnr>0) {
			$d = array("0","This ".$this->title." cannot be deleted as there is at least one ".$this->list_titles['jobtitles']." currently associated with it.");
		} else {
			$d = array("1","");
		}
		return $d;
	}




	
	
	
/* JOB TITLES */	
	private function createJobTitlesList() {
		$this->dbref = "assist_".$this->cmpcode."_master_list_jobtitle";
		$this->select_all = "SELECT 
							  jt.id as id, 
							  jt.value as value,
							  CONCAT(op.value,': ',jl.value,' (Level: ',joblevel,')') as joblevelid,
							  jt.status as status,
							  jt.assistid as assistid
							FROM ".$this->dbref." jt
							LEFT OUTER JOIN assist_".$this->cmpcode."_master_list_joblevel jl
							ON jt.joblevelid = jl.id AND jl.status = 1
							LEFT OUTER JOIN assist_".$this->cmpcode."_master_list_orgposition op
							ON op.id = jl.orgpositionid AND op.status = 1
							WHERE jt.status = 1 
							ORDER BY jt.value, jl.sort, op.sort, jl.joblevel, jl.value, op.orglevel, op.value";
		$this->section = "JOBT";
		$this->ref_fld = "id"; 
		$this->val_fld = "value"; 
		$this->status_fld = "status"; 
		$this->delete_status = 0;
		$this->validate_delete = true;
		$this->can_sort = false; 
			$this->sort_fld = ""; 
			$this->sort_sql = ""; 
			$this->get_old_sort = "";
		$this->fields = array("id","value","joblevelid","status","assistid");
		$this->structure = array(
			'id'=>array(
				'title'=>"Ref",				'display'=>true,				'default'=>"NULL",				'type'=>"PK",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TH",				'class'=>"center",
			),
			'value'=>array(
				'title'=>"Job Title",				'display'=>true,				'default'=>"",				'type'=>"VC",				'length'=>120,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'joblevelid'=>array(
				'title'=>"Job Level",				'display'=>true,				'default'=>"1",				'type'=>"LIST",				'length'=>10,				'src'=>"FORM",				'required'=>true,				'cell'=>"TD",				'class'=>"",
			),
			'status'=>array(
				'title'=>"Status",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
			'assistid'=>array(
				'title'=>"Ignite Assist Default",				'display'=>false,				'default'=>"1",				'type'=>"",				'length'=>0,				'src'=>"DEFAULT",				'required'=>true,				'cell'=>"TD",				'class'=>"center",
			),
		);
		$this->list_items = array('joblevelid'=>array());
		$sql = "SELECT jl.id as id, CONCAT(op.value,': ',jl.value,' (Level: ',joblevel,')') as value
								FROM assist_".$this->cmpcode."_master_list_joblevel jl
								INNER JOIN assist_".$this->cmpcode."_master_list_orgposition op
								ON op.id = jl.orgpositionid AND op.status = 1
								WHERE jl.status = 1 ORDER BY jl.sort, op.sort, jl.joblevel, jl.value, op.orglevel, op.value";
		$mylist = $this->db->mysql_fetch_fld2_one($sql,"id","value");
		$this->list_items['joblevelid'] = $mylist;
	}
	private function jobTitlesDeleteValidation($i) {
		$sql = "SELECT * FROM assist_".$this->cmpcode."_timekeep WHERE tkdesig = '$i' AND tkstatus <> 0";
		$mnr = $this->db->db_get_num_rows($sql);
		if($mnr>0) {
			$d = array("0","This ".$this->title." cannot be deleted as there is at least one User currently associated with it.");
		} else {
			$d = array("1","");
		}
		return $d;
	}

	
	
	
	
	
	
	
	
	
/** FUNCTIONS NECESSARY TO UPDATE OLD DATABASE STRUCTURE **/	
	private function addItemToOldDB($id) {
		switch($this->slist) {
			case "jobtitles":
				$sql = "INSERT INTO assist_".$this->cmpcode."_list_jobtitle (id,value,assist,yn) VALUES ($id,'".$this->code($_REQUEST[$this->val_fld])."',".$this->structure['assistid']['default'].",'Y')";
				$this->db->db_insert($sql);
				$this->logMe($id,"","Added new ".$this->title." $id: \'".$_REQUEST[$this->val_fld]."\' (Added in Master Setup)","",serialize($_REQUEST),$sql,"setup","false");
				break;
			default:
				break;
		}
	
	
	
	
	}
	
	private function editItemInOldDB($id) {
		switch($this->slist) {
			case "jobtitles":
				$sql = "UPDATE assist_".$this->cmpcode."_list_jobtitle SET value = '".$this->code($_REQUEST[$this->val_fld])."' WHERE id = $id";
				$this->db->db_update($sql);
				$this->logMe($id,"","Edited ".$this->title." $id (Edited in Master Setup)\'",serialize($_REQUEST),"",$sql,"setup","false");
				break;
			default:
				break;
		}
	}
	
	
	private function delItemInOldDB($id) {
		switch($this->slist) {
			case "jobtitles":
				$sql = "UPDATE assist_".$this->cmpcode."_list_jobtitle SET yn = 'N' WHERE id = $id";
				$this->db->db_update($sql);
				$this->logMe($id,"","Deleted ".$this->title." $id (Deleted in Master Setup)\'",serialize($_REQUEST),"",$sql,"setup","false");
				break;
			default:
				break;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/** DESTROY OBJECT **/
	public function __desctruct() {
		parent::__destruct();
	}
}	//end class ASSIST_SETUP_LIST

?>