<?php 

class ASSIST_MASTER_FINANCIALYEARS extends ASSIST_MASTER {
	
	const TABLE = "master_financialyears";
	
	const ID_FLD = "id";
	const NAME_FLD = "value";
	const SORT_FLD = "start_date";
	const STATUS_FLD = "status";
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	
	public function __construct() {
		parent::__construct();
		$this->db_table = strtolower("assist_".$this->getCmpCode()."_".self::TABLE);
		$this->sort_by = array("start_date","end_date","value");
	}
	
	//[JC - 29 Oct 2013] old function - not sure where in use
	public function fetchAll() {
		$db = new ASSIST_DB("client");
		$sql = "SELECT * FROM ".$this->getTable()." WHERE ".self::STATUS_FLD." & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY ".self::SORT_FLD;
		return $db->mysql_fetch_all_fld($sql,self::ID_FLD);
	}
	
	
	
	
	
	
	public function getFld($f) { 
		switch(strtoupper($f)) {
			case "ID": return self::ID_FLD; break;
			case "SORT": return self::SORT_FLD; break;
			case "NAME": return self::NAME_FLD; break;
			case "STATUS": return self::STATUS_FLD; break;
			case "ACTIVE": return self::ACTIVE; break;
			case "TABLE": return self::TABLE; break;
		}
	}
	
}

?>