<?php

class ASSIST_EMAIL_SUMMARY extends ASSIST_EMAIL {

	private $objects;			//object[] = array('id'=>"display text",'id2'=>"display 2")
	private $headings;			//heading = array('id'=>"display text",'id2'=>"display 2")
	
	private $title1;
	private $title2;
	
	private $body;
	
	private $deadline_field;
	private $today;
	
	public function __construct($to = "", $subject = "", $cc = "", $bcc = "", $reply_to = "", $h1 = "", $h2 = "", $table_headings = array(), $table_rows = array(), $deadline_field = "") {
		parent::__construct($to, $subject, "", "HTML", $cc, $bcc, $reply_to);
		$this->today = strtotime(date("d F Y"));
		//echo "<br />".$deadline_field;
		$this->setDeadlineField($deadline_field);
		$this->setH1($h1);
		$this->setH2($h2);
		$this->setSummaryHeadings($table_headings);
		$this->setSummaryRows($table_rows);
		$this->createMessageBody();
	}

	public function setDeadlineField($f,$create_body=true) { 
		$this->deadline_field = $f;
		if($create_body) { $this->createMessageBody(); }
	}
	
	public function setH1($h,$create_body=true) { 
		$this->title1 = $h; 
		if($create_body) { $this->createMessageBody(); }
	}
	
	public function setH2($h,$create_body=true) { 
		$this->title2 = $h; 
		if($create_body) { $this->createMessageBody(); }
	} 

	public function setSummaryHeadings($headings,$create_body=true) { 
		$this->headings = $headings; 
		if($create_body) { $this->createMessageBody(); }
	}
	
	public function setSummaryRows($objects,$create_body=true) { 
		$this->objects = $objects; 
		if($create_body) { $this->createMessageBody(); }
	}
	
	private function createMessageBody() {
		$body = "";
		if(strlen($this->title1)>0) {
			$body.="<h1>".$this->title1."</h1>";
		}
		if(strlen($this->title2)>0) {
			$body.="<h2>".$this->title2."</h2>";
		}
		if(count($this->headings)==0 || count($this->objects)==0) {
			$body.="<p>There are no actions to display.</p>";
		} else {
			$body.="<table>
				<tr>";
			foreach($this->headings as $key => $h) {
				$body.="<th>".$h."</th>";
			}
			$body.="</tr>";
			foreach($this->objects as $o) {
				$body.="<tr>";
				foreach($this->headings as $key => $h) {
					$v = $o[$key];
					if($key==$this->deadline_field) {
						$t = strtotime($v);
						if($t<$this->today) {
							$v = "<span class=overdue>".$v."</span>";
						} elseif(date("d F Y",$t)==date("d F Y",$this->today)) {
							$v = "<span class=today>".$v."</span>";
						} elseif($t < ($this->today + 7*24*3600)) {
							$v = "<span class=soon>".$v."</span>";
						}
					}
					$body.="<td>".$v."</td>";
				}
				$body.="</tr>";
			}
			$body.= "</table>";
		}
		$this->body = $body;
		$this->setHTMLBody($this->body);
	}




}


?>