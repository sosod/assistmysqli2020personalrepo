<?php


class ASSIST_SEARCH_DRAW extends ASSIST_SEARCH {

	protected $output = "onscreen";
	protected $report_title;
	protected $groups = array();
	protected $group_rows = array();
	protected $group_results = array();
	protected $result_categories = array();
	protected $report_file_name = array();
	protected $color_codes = array();
	
	public function __construct() {
		//echo "<P>ASSIST_SEARCH_DRAW.__construct()</p>";
		parent::__construct();
		$this->report_file_name = array(
			'modref'=>strtolower($_SESSION['modref']),
			'section'=>"",
		);
		$this->output = isset($this->vars['output']) ? $this->vars['output'] : $this->output;
		$this->color_codes = array(
			'white'		=> array('color'=>"#FFFFFF",'code'=>""),
			'red'		=> array('color'=>"#CC0001",'code'=>"R"),
			'orange'	=> array('color'=>"#FE9900",'code'=>"O"),
			'green'		=> array('color'=>"#009900",'code'=>"G"),
			'darkgreen'	=> array('color'=>"#005500",'code'=>"G2"),
			'blue'		=> array('color'=>"#000077",'code'=>"B"),
			'maroon'	=> array('color'=>"#700000",'code'=>"X"),
			'grey'		=> array('color'=>"#999999",'code'=>"N/A"),
		);
		$this->color_codes['gray'] = $this->color_codes['grey'];
	}
	
	public function setReportTitle($drt) {
		$rt = (isset($_REQUEST['rhead']) && strlen($_REQUEST['rhead'])>0) ? $_REQUEST['rhead'] : $drt;
		$this->report_title = $rt;
	}
	protected function getReportTitle() { return $this->report_title; }
	
	public function setReportFileName($rn) { $this->report_file_name['section'] = $rn; } //$this->arrPrint($this->report_file_name); echo $rn.":"; }
	
	public function getFilterSql($tbl_id,$type,$filter,$filter_type,$field_name,$options=array()) {
		$separator = isset($options['separator']) ? $options['separator'] : " ";
		$sql_separator = isset($options['sql_separator']) ? $options['sql_separator'] : "";
		$date_format = isset($options['date_format']) ? $options['date_format'] : $this->db_date_format;
		
		$s = "";
			switch($type) {
				case "REF":
				case "TEXT":
				case "TEXTLIST":
				case "MULTITEXT":
					if(strlen($filter)>0) {
						switch($filter_type) {
						case "ANY":
						case "ALL":
							$f = explode($separator,$filter);
							$r = array();
							foreach($f as $a) {
								if(strlen($a)>0) {
									$r[] = $tbl_id.".".$field_name." LIKE '%".$sql_separator.strtolower($a).$sql_separator."%'";
								}
							}
							if($filter_type=="ALL") {
								$s = "( ".implode(" AND ",$r)." )";
							} else {
								$s = "( ".implode(" OR ",$r)." )";
							}
							break;
						case "EXACT":
							$s = $tbl_id.".".$field_name." LIKE '%".$sql_separator.strtolower($filter).$sql_separator."%'";
							break;
						}
					}
					break;
				case "DATE":
//					$this->arrPrint($filter);
					if(strlen($filter['from'])>0 || strlen($filter['to'])>0) {
						$from = strtotime(strlen($filter['from'])>0 ? $filter['from'] : $filter['to']);
						$to = strtotime(strlen($filter['to'])>0 ? $filter['to'] : $filter['from']);
						if($from > $to) {
							$x = $from; $from = $to; $to = $x;
						}
						$to = $to + (24*60*60-1);
						switch($this->db_date_format) {
							case "INT":
							case "UNIX_INTEGER":
								$s = "( ".$tbl_id.".".$field_name." >= ".$from." AND ".$tbl_id.".".$field_name." <= ".$to." )";
								break;
							case "STRING":
								$s = "( STR_TO_DATE(".$tbl_id.".".$field_name.",'%d-%b-%Y') >= STR_TO_DATE('".date("d-M-Y",$from)."','%d-%b-%Y') AND STR_TO_DATE(".$tbl_id.".".$field_name.",'%d-%b-%Y') <= STR_TO_DATE('".date("d-M-Y",$to)."','%d-%b-%Y') )";
								break;
							case "DATETIME":
							default:
								$s = "( ".$tbl_id.".".$field_name." >= CAST('".date("d F Y H:i:s",$from)."' AS DATETIME) AND ".$tbl_id.".".$field_name." <= CAST('".date("d F Y H:i:s",$to)."' AS DATETIME) )";
								break;
						}
					}
					break;
				case "LIST":
					$r = array();
					if(is_array($filter)) {
						if($filter[0]!="X") {
							foreach($filter as $f) {
								if(strlen($f)>0) {
									if(is_numeric($f)) {
										$r[] = $tbl_id.".".$field_name." = ".$f;
									} else {
										$r[] = $tbl_id.".".$field_name." = '".$f."'";
									}
								}
							}
						}
						if(count($r)>0) {
							$s = "( ".implode(" OR ",$r)." )";
						}
					} else {
						if($filter!="X") {
							if(is_numeric($filter)) {
								$s = $tbl_id.".".$field_name." = ".$filter;
							} else {
								$s = $tbl_id.".".$field_name." = '".$filter."'";
							}
						}
					}
					break;
				case "USER":
				case "NUM":
				case "PERC":
					break;
			}
		return $s;
	}
	
	
	public function setGroup($id,$group,$rows) { 
		$this->groups[$id] = $group;
		$this->group_rows[$id] = $rows;
		$this->group_results[$id] = array();
		foreach($this->result_categories as $key=>$r) {
			$this->group_results[$id][$key] = 0;
		}
	}
	
	public function setResultCategory($id,$heading,$color) {
		$this->result_categories[$id] = array(
			'id'=>$id,
			'heading'=>$heading,
			'color'=>$color,
		);
	}
	
	protected function outputReport($echo,$f) { //echo $f; $this->arrPrint($this->report_file_name);
		$result = "";
		if($f=="SAVE") {
			switch($this->output) {
			case "excel":
				$ext = ".xls"; $content = 'application/ms-excel'; 
				break;
			case "csv":
				$ext = ".csv"; $content = 'text/plain'; 
				break;
			case "onscreen":
			default:
				$ext = ".html"; $content = 'html'; 
				break;
			}
			$sys_filename = $this->report_file_name['modref']."_".$this->report_file_name['section']."_".date("YmdHis").$ext;
			$path = $this->getReportPath();
			$this->saveEcho($path,$sys_filename,$echo);
			$result = $sys_filename;
		} else {
			switch($this->output) {
			case "excel":
				$ext = ".xls"; $content = 'application/ms-excel'; 
				break;
			case "csv":
				$ext = ".csv"; $content = 'text/plain'; 
				break;
			case "onscreen":
			default:
				$ext = ".html"; $content = 'html'; 
				break;
			}
			$path = $this->getReportPath();
			if($this->output=="onscreen") {
				include($this->getFilesLocation($path,$this->getCmpCode()).$f);
			} else {
				$usr_filename = $this->report_file_name['section']."_report_".date("YmdHis").$ext;
				$this->downloadFile2($path, $f, $usr_filename, $content);
			}
			$result = true;
		}
		return $result;
	}
	
	protected function getResultCategory($id) {
		if(isset($this->result_categories[$id])) {
			$r = $this->result_categories[$id];
			$r['code']=(isset($this->color_codes[$r['color']]) ? $this->color_codes[$r['color']]['code'] : $this->color_codes['white']['code']);
			$r['color']=(isset($this->color_codes[$r['color']]) ? $this->color_codes[$r['color']]['color'] : $this->color_codes['white']['color']);
		} else {
			$r = array(
				'id'=>$id,
				'heading'=>$id,
				'color'=>"#FFFFFF",
				'code'=>$id,
			);
		}
		return $r;
	}
	
}


?>