<?php

class ASSIST_REPORT_QUICK extends ASSIST_REPORT {


	public function __construct() {
		parent::__construct();
	}

	public function drawTable($reports) {
		if(count($reports)==0) {
			echo "<P>There are no Quick Reports to display.</p>";
		} else {
			echo "
			<table class=list id=quickreports>
				<tr>
					<th>Ref</th>
					<th width=200>Name</th>
					<th width=250>Description</th>
					<th>Date</th>
					<th></th>
				</tr>";
			foreach($reports as $key => $r) {
				echo "
				<tr>
					<td class='center b'>".$key."</td>
					<td>".$r['name']."</td>
					<td>".$r['description']."</td>
					<td>".date("d M Y H:i",strtotime($r['moddate']))."</td>
					<td><input type=button value=Edit key=".$key." sc=".$r['source_class']." pg=".$r['page']." /> <input type=button value=Generate key=".$key." sc=".$r['source_class']." pg=".$r['page']." /></td>
				</tr>";
			}
			echo "
			</table>
			<script type=text/javascript>
				$(function() {
					$('#quickreports input:button').click(function() {
						var q = $(this).attr('key');
						var sc = $(this).attr('sc');
						var pg = $(this).attr('pg');
						var v = $(this).val();
						document.location.href = 'report.php?page='+pg+'&class='+sc+'&quick_id='+q+'&quick_act='+v;
					});
				});
			</script>";
		}
	}
	

}


?>