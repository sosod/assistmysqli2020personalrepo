<?php


class ASSIST_REPORT_FIXED_GENERATOR extends ASSIST_REPORT_FIXED {

	private $reports;
	private $use_filter;
	private $filters;


	public function __construct($r, $uf, $f) {
		parent::__construct();
		
		$this->reports = $r;
		$this->use_filter = $uf;
		$this->filters = $f;
	}

	
	public function drawPage() {
		echo "aaaaaaa		<table class=list id=report_list>";
		if($this->use_filter===true) {
			echo "
				<tr>
					<td colspan=5 class='b right'>Filter: <select id=filter><option value=ALL selected>All</option>";
			foreach($this->filters as $i=>$f) {
				echo "<option value=$i>$f</option>";
			}
			echo "</select></td>
				</tr>
			";
		}
	
		echo "
			<tr>
				<th>Ref</th>
				<Th>Name</th>
				<th>Description</th>
				<th>Format</th>
				<th></th>
			</tr>";
		$c = 1;
		foreach($this->reports as $key => $r) {
			echo "
			<tr>
				<td class='center b'>".$c."</td>
				<td>".$r['name']."</td>
				<td>".$r['descrip']."</td>
				<td>".$r['format']."</td>
				<td><input type=button value=Generate id=".$r['code']." /></td>
			</tr>";
			$c++;
		}
		echo "
		</table>
		
		<form name=frm_fixed action=report.php method=post>
			<input type=hidden name=act value='DRAW' />
			<input type=hidden name=page value='fixed' />
			<input type=hidden name=class value='MODULE_COMPL_REPORT_FIXED' />
			<input type=hidden name=filter value='ALL' />
			<input type=hidden name=code value=''/>
		</form>
		
		<script type=text/javascript>
		$(function () {
			$('input:button').button().click(function() {
				var f = $('#filter').val();
				var me = $(this).prop('id');
				if(f!='X') {//} && parseInt(f)!=0) {
					$('input[name=filter]').val(f);
					$('input[name=code]').val(me);
					$('form[name=frm_fixed]').submit();
				} else {
					alert('Please select a filter.');
				}
			}).css({'font-size':'7pt','padding':'3pt'});
			
		});
		</script>
		
		";
	
	
	}


}
?>