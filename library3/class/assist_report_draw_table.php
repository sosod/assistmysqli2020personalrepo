<?php


class ASSIST_REPORT_DRAW_TABLE extends ASSIST_REPORT_DRAW {
	private $settings = array();
	private $report_settings = array(
		'columns'		=> array(),
		'filter'		=> array(),
		'filtertype'	=> array(),
		'groupby'		=> array(),
		'sort'			=> array(),
	);
	private $rows = array();
	
	public function __construct() {
		//echo "<P>ASSIST_REPORT_DRAW_TABLE.__construct()</p>";
		parent::__construct();
	} 
	
	public function prepareSettings() {
	//echo "<p>assist_report_draw_table.preparesettings";
		$this->report_settings = array(
			'columns'		=> $_REQUEST['columns'],
			'filter'		=> $_REQUEST['filter'],
			'filtertype'	=> $_REQUEST['filtertype'],
			'groupby'		=> $_REQUEST['group_by'],
			'sort'			=> $_REQUEST['sort'],
		);
		//echo "<P>output format: ".$this->output;
		switch($this->output) {
			case "csv":
				$this->setFormatting("CSV");
				//$this->setNewline(chr(10));
				$this->setNewline(" ");
				$x = "\"".$this->getCmpName()."\"\r\n\"".$this->getReportTitle()."\"";
				$this->setStartPage($x);
				$this->setEndPage("\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"");
				$this->setStartGroupHeading("\"\"\r\n\"");
				$this->setStartSummaryHeading("\"\"\r\n\"");
				$this->setEndGroupHeading("\"\r\n");
				$this->setEndSummaryHeading("\"\r\n");
				$this->setStartTable("\r\n");
				$this->setStartSummaryTable("\r\n");
				$this->setEndTable("");
				$this->setStartTableHeader("");
				$this->setEndTableHeader("");
				$this->setStartTableBody("");
				$this->setEndTableBody("");
				$this->setStartRow("");
				$this->setEndRow("\r\n");
				$this->setStartCell("default","\"");
				$this->setStartCell("heading","\"");
				$this->setStartCell("result_heading","\"");
				$this->setStartCell("center","\"");
				$this->setStartCell("TEXT","\"");
				$this->setStartCell("NUM","\"");
				$this->setStartCell("PERC","\"");
				$this->setStartCell("LIST","\"");
				$this->setStartCell("SUM_TOTAL","\"");
				foreach($this->result_categories as $key => $rc) {
					$this->setStartCell("RESULT_".$key,"\"");
				}
				$this->setEndCell("default","\",");
				$this->setEndCell("heading","\",");				
				$this->setEndCell("result_heading","\",\"\",");	
				$this->setNoResult("\r\n\"There are no results to display.\"\r\n");			
				/*
				$this->settings['cell'][22]['a']['OVERALL'] = "\"";	//ptd normal cell
				$this->settings['cell'][22]['a']['NUM'] = "\""; //result cell
				$this->settings['cell'][22]['a']['DOUBLE'] = "\""; //result cell
				$this->settings['cell'][22]['a']['TEXT'] = "\""; //result cell
				
				*/
				break;
			case "excel":
				$this->setFormatting("XLS");
				$this->setNewline("<br />");
				$x = "
				<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">
					<head>
						<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
						<!--[if gte mso 9]>
							<xml>
							<x:ExcelWorkbook>
							<x:ExcelWorksheets>
							<x:ExcelWorksheet>
							<x:Name>Report</x:Name>
							<x:WorksheetOptions>
							<x:Panes>
							</x:Panes>
							</x:WorksheetOptions>
							</x:ExcelWorksheet>
							</x:ExcelWorksheets>
							</x:ExcelWorkbook>
							</xml>
						<![endif]-->
						<style>
							td { font-style: Calibri; font-size:11pt; } 
							.kpi { border-width: thin; border-color: #000000; border-style: solid; vertical-align: top; } 
							.kpi_r { border-width: thin; border-color: #000000; border-style: solid; text-align: center; color: #FFFFFF; vertical-align: top; } 
							.head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } 
							.title { font-size:20pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:underline; text-align: center; } 
							.title2 { font-size:16pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:none; text-align: center;} 
							.title3 { font-size:14pt; font-style: Calibri; color:#000099; font-weight:bold;} 
							.title4 { font-size:12pt; font-style: Calibri; color:#000099; font-weight:bold;}
							.summary {
								margin: 3px;
								line-height: 1.1em;
							}
						</style>
					</head>
					<body><table><tr><td class=title nowrap colspan=".(count($this->report_settings['columns'])+(isset($this->report_settings['columns']['result']) ? 1 : 0))." >".$this->getCmpName()."</td></tr>
					<tr><td class=title2 nowrap colspan=".(count($this->report_settings['columns'])+(isset($this->report_settings['columns']['result']) ? 1 : 0))." >".$this->getReportTitle()."</td></tr>
				";
				$this->setStartPage($x);
				$this->setEndPage("<tr></tr><tr><td></td><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>");
				$this->setStartGroupHeading("<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>");
				$this->setEndGroupHeading("</td></tr>");
				$this->setStartSummaryHeading("<tr><td width=50></td></tr><tr><td nowrap class=title4 rowspan=1>");
				$this->setEndSummaryHeading("</td></tr>");
				$this->setStartTable("");
				$this->setStartSummaryTable("");
				$this->setEndTable("");
				$this->setStartTableHeader("");
				$this->setEndTableHeader("");
				$this->setStartTableBody("");
				$this->setEndTableBody("");
				$this->setStartRow("<tr>");
				$this->setEndRow("</tr>");
				$this->setStartCell("default","<td class=kpi>");
				$this->setStartCell("heading","<td class=\"head\">");
				$this->setStartCell("result_heading","<td colspan=2 class=\"head\">");
				$this->setStartCell("center","<td class=kpi style='text-align:center'>");
				$this->setStartCell("TEXT","<td class=kpi style=''>");
				$this->setStartCell("NUM","<td class=kpi style='text-align:right;'>");
				$this->setStartCell("PERC","<td class=kpi style='text-align:right;'>");
				$this->setStartCell("LIST","<td class=kpi>");
				$this->setStartCell("SUM_TOTAL","<td class=kpi style='font-weight: bold; text-align: right;'>");
				foreach($this->result_categories as $key => $rc) {
					$this->setStartCell("RESULT_".$key,"<td class=kpi style='color: #FFFFFF; text-align: center; background-color: ".$this->color_codes[$rc['color']]['color'].";'>");
				}
				$this->setEndCell("default","</td>");
				$this->setEndCell("heading","</td>");			
				$this->setEndCell("result_heading","</td>");	
				$this->setNoResult("<tr><td class=kpi>There are no results to display.</td></tr>");		
				/*
				$this->settings['cell'][20]['a'] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
				$this->settings['cell'][22]['a']['OVERALL'] = "<td style=\"text-align: right;background-color: #eeeeee\" class=kpi>";	//ptd normal cell
				$this->settings['cell'][22]['a']['NUM'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
				$this->settings['cell'][22]['a']['DOUBLE'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
				$this->settings['cell'][22]['a']['TEXT'] = "<td class=kpi>"; //result cell
				*/
				break;
			case "onscreen";
			default:
				$this->setFormatting("HTML");
				$this->setNewline("<br />");
				$x = "
				<html>
					<head>
						<meta http-equiv=\"Content-Language\" content=\"en-za\">
						<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
						<title>www.Ignite4u.co.za</title>
					</head>
					<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\">
					<style type=text/css>
					table.summary, table.summary td {
						border: 2px solid #FFFFFF;
						margin: 3px;
						line-height: 1.1em;
					}
					</style>
					<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
					<h1 style=\"text-align: center;\">".$this->getCmpName()."</h1>
					<h2 style=\"text-align: center;\">".$this->getReportTitle()."</h2>
				";
				$this->setStartPage($x);
				$this->setEndPage("<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>");
				$this->setStartGroupHeading("<h3>");
				$this->setEndGroupHeading("</h3>");
				$this->setStartSummaryHeading("<h4>");
				$this->setEndSummaryHeading("</h4>");
				$this->setStartTable("<table width=100%>");
				$this->setStartSummaryTable("<table class=summary>");
				$this->setEndTable("</table>");
				$this->setStartTableHeader("<thead>");
				$this->setEndTableHeader("</thead>");
				$this->setStartTableBody("<tbody>");
				$this->setEndTableBody("</tbody>");
				$this->setStartRow("<tr>");
				$this->setEndRow("</tr>");
				$this->setStartCell("default","<td>");
				$this->setStartCell("heading","<th>");
				$this->setStartCell("result_heading","<th colspan=2>");
				$this->setStartCell("center","<td style='text-align:center'>");
				$this->setStartCell("right","<td style='text-align:right'>");
				//$this->setStartCell("TEXT","<td style='font-size: 7pt;'>");
				$this->setStartCell("LOG","<td style='font-size: 7pt;'>");
				$this->setStartCell("TEXT","<td style=''>");
				$this->setStartCell("NUM","<td style='text-align:right;'>");
				$this->setStartCell("PERC","<td style='text-align:right;'>");
				$this->setStartCell("LIST","<td>");
				$this->setStartCell("SUM_TOTAL","<td style='font-weight: bold; text-align: right;'>");
				foreach($this->result_categories as $key => $rc) {
					$this->setStartCell("RESULT_".$key,"<td style='color: #FFFFFF; text-align: center; background-color: ".$this->color_codes[$rc['color']]['color'].";'>");
				}
				$this->setEndCell("default","</td>");
				$this->setEndCell("heading","</th>");
				$this->setEndCell("result_heading","</th>");
				$this->setNoResult("<p>There are no results to display.</p>");
				break;
			}
		
		
	}
	
	
	

	public function setRows($r=array()) {
		
		$this->rows = $r;
	}
	
	function echoMe($r) {
		$str = "<ul>";
		foreach($r as $k=>$a) {
			$str.="<li>".$k."=>";
			if(is_array($a)) {
				$str.=$this->echoMe($a);
			} else {
				$str.=str_replace("<","[",str_replace(">","]",$a));
			}
			$str.="</li>";
		}
		$str.="</ul>";
		return $str;
	}
	
	public function displayColumnCheck($fld) {
		if(isset($this->columns[$fld]) && isset($this->report_settings['columns'][$fld]) && strtoupper($this->report_settings['columns'][$fld])=="ON") {
			return true;
		} 
		return false;
	}
	
	public function drawPage($action) {
		//echo "<P>draw page <P>resulting settings: "; $this->arrPrint($this->settings);
	
		if($action=="GENERATE") {
			$results = array();
			$hasResults = false;
		
			//echo $this->displayPageHeader()."<h2 class=red>Processing...</h2>";
		//$this->arrPrint($this->settings);
		
		//echo $this->echoMe($this->settings);
		
			$echo = "";
			$echo .=$this->startPage();
			$results['total'] = $this->createBlankResultArray();
			foreach($this->groups as $gkey=>$g) {
				if(count($this->group_rows[$gkey])>0) {
					$hasResults = true;
					if($gkey!="X") {
						$results[$gkey] = $this->createBlankResultArray();
						$echo.=$this->startGroupHeading().$g.$this->endGroupHeading();
					}
					$echo .=$this->startTable().$this->startTableHeader().$this->startRow();
							foreach($this->fields as $fld => $f) {
								if($this->displayColumnCheck($fld)) {
									$c = $fld=="result" ? "result_heading" : "heading";
									$echo .=$this->startCell($c).$f['heading'].$this->endCell($c);
								}
							}
					$echo .=$this->endRow().$this->endTableHeader().$this->startTableBody();
					foreach($this->group_rows[$gkey] as $gr) {
						$r = $this->rows[$gr];
						if($gkey!="X") { $results[$gkey][$r['result']]++; }
						if($this->displayColumnCheck("result") && isset($r['result'])) { $results['total'][$r['result']]++; }
						$echo .=$this->startRow();
							foreach($this->fields as $fld => $f) {
								if(isset($this->columns[$fld]) && isset($this->report_settings['columns'][$fld]) && strtoupper($this->report_settings['columns'][$fld])=="ON") {
									$d = $r[$fld];
									if($fld=="result") {
										$rc = $this->getResultCategory($d);
										$echo .=$this->startCell("RESULT_".$rc['id']).$rc['code'].$this->endCell().$this->startCell().$rc['heading'].$this->endCell();
									//} elseif(is_array($d)) {
									//	$t = $f['type'];
									//	$echo .=$this->startCell($t).(is_array($d) ? implode($this->newline(),$d) : "").$this->endCell($t);
									} else {
										$t = $f['type'];
										$e = (is_array($d) ? implode($this->newline(),$d) : str_replace(chr(10),$this->newline(),$d));
										//if(strlen($e)>75) {	$t = "TEXT"; }
										$echo.=$this->startCell($t);
										$echo.=$e;
											//$echo.=str_replace(chr(10),$this->newline(),$d);
										$echo.=$this->endCell($t);
									}
								}
							}
						$echo .=$this->endRow();
					}
					$echo.=$this->endTableBody().$this->endTable();
					if($gkey!="X" && $this->displayColumnCheck("result")) {	$echo.=$this->drawSummary($results[$gkey],$g);	}
				}
			}
			if(array_sum($results['total'])!=0 || $hasResults) { } else { $echo.=$this->noResult(); }
			if($this->displayColumnCheck("result") && array_sum($results['total'])>0) { $echo.=$this->drawSummary($results['total'],"total"); }
			$echo.=$this->endPage();
			$file = $this->outputReport($echo,"SAVE");
			//echo $echo; 
			//$this->arrPrint($this->rows);  $this->arrPrint($this->groups); 
			//echo "<script type=text/javascript>document.location.href = 'report_process.php?source_class=MODULE_COMPL_REPORT_LEGISLATION&page=generate_legislation&act=DRAW&f=".$file."&output=".$this->output."';</script>";
			$this->outputReport("",$file);
		} elseif($action=="DRAW") {
			//$filename = $_REQUEST['f'];
			header("Location:report.php?class=".$_REQUEST['source_class']."&page=".$_REQUEST['page']);
		}
	}
	
	public function createBlankResultArray() {
		$r = array();
		foreach($this->result_categories as $id=>$rc) {
			$r[$id] = 0;
		}
		return $r;
	}

	function drawSummary($values,$type="total") {
		$output = $this->output;
		$echo  = "";
		if($type=="total") {
			$echo = $this->startGroupHeading()."Overall Summary of Results".$this->endGroupHeading();
		} else {
			$echo = $this->startSummaryHeading()."Summary of Results: ".$type.$this->endSummaryHeading();
		}
		$echo.=$this->startSummaryTable();
		foreach($this->result_categories as $key=>$rc) {
			$echo.=$this->startRow()
				.$this->startCell("RESULT_".$rc['id'])
				." ".$this->color_codes[$rc['color']]['code']." "
				.$this->endCell()
				.$this->startCell()
				.$rc['heading']
				.$this->endCell()
				.$this->startCell("right")
				.$values[$key]
				.$this->endCell().$this->endRow();
		}
		$echo.=$this->startRow().$this->startCell().$this->endCell().$this->startCell("SUM_TOTAL")."Total:".$this->endCell().$this->startCell("SUM_TOTAL").array_sum($values).$this->endCell().$this->endRow().$this->endTable();

		return $echo;
	}

	
	
	
	private function startPage() { return $this->settings['page']['a']; }
	private function endPage() { return $this->settings['page']['z']; }
	private function startGroupHeading() { return $this->settings['group']['a']; }
	private function endGroupHeading() { return $this->settings['group']['z']; }
	private function startSummaryHeading() { return $this->settings['summary']['a']; }
	private function endSummaryHeading() { return $this->settings['summary']['z']; }
	private function startTable() { return $this->settings['table']['a']; }
	private function startSummaryTable() { return $this->settings['table']['summary']['a']; }
	private function endTable() { return $this->settings['table']['z']; }
	private function startTableHeader() { return $this->settings['thead']['a']; }
	private function endTableHeader() { return $this->settings['thead']['z']; }
	private function startTableBody() { return $this->settings['tbody']['a']; }
	private function endTableBody() { return $this->settings['tbody']['z']; }
	private function startRow() { return $this->settings['row']['a']; }
	private function endRow() { return $this->settings['row']['z']; }
	private function startCell($c="default") { 
		if(!isset($this->settings['cell'][$c])) {
			if(in_array($c,array("MULTITEXT","LOG","MULTILIST"))) {
				$c = "TEXT";
			} else {
				$c = "default"; 
			}
		}
		return $this->settings['cell'][$c]['a']; 
	}
	private function endCell($c="default") { 
		if(!isset($this->settings['cell'][$c]['z'])) { $c = "default"; }
		return $this->settings['cell'][$c]['z']; 
	}
	private function getFormatting() { return $this->settings['applied_formatting']; }
	private function newline() { return $this->settings['newline']; }
	private function noResult() { return $this->settings['noresult']; }

	//private function start() { return $this->settings['']['a']; }
	//private function end() { return $this->settings['']['z']; }
	
	private function setStartPage($a) { $this->settings['page']['a'] = $a; }
	private function setEndPage($z) { $this->settings['page']['z'] = $z; }
	private function setStartGroupHeading($a) { $this->settings['group']['a'] = $a; }
	private function setEndGroupHeading($z) { $this->settings['group']['z'] = $z; }
	private function setStartSummaryHeading($a) { $this->settings['summary']['a'] = $a; }
	private function setEndSummaryHeading($z) { $this->settings['summary']['z'] = $z; }
	private function setStartTable($a) { $this->settings['table']['a'] = $a; }
	private function setStartSummaryTable($a) { $this->settings['table']['summary']['a'] = $a; }
	private function setEndTable($z) { $this->settings['table']['z'] = $z; }
	private function setStartTableHeader($a) { $this->settings['thead']['a'] = $a; }
	private function setEndTableHeader($z) { $this->settings['thead']['z'] = $z; }
	private function setStartTableBody($a) { $this->settings['tbody']['a'] = $a; }
	private function setEndTableBody($z) { $this->settings['tbody']['z'] = $z; }
	private function setStartRow($a) { $this->settings['row']['a'] = $a; }
	private function setEndRow($z) { $this->settings['row']['z'] = $z; }
	private function setStartCell($c,$a) { 
		$this->settings['cell'][$c]['a'] = $a; 
	}
	private function setEndCell($c,$z) { $this->settings['cell'][$c]['z'] = $z; }
	private function setFormatting($a) { $this->settings['applied_formatting'] = $a; }
	private function setNewline($a) { $this->settings['newline'] = $a; }
	private function setNoResult($a) { $this->settings['noresult'] = $a; }
	
}


?>