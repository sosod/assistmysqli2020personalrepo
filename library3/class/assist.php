<?php
/** BASE ASSIST CLASS
Created on: 26 August 2012
Created by: Janet Currie


IN PROGRESS

**/


class ASSIST extends ASSIST_HELPER {

	private $mod_ref;
	
	private $cmpcode;
	protected $cmp_code;
	private $cmp_name;
	
	private $tkid;
	protected $user_id;
	private $tkname;
	protected $user_name;
	private $tkuser;
	private $user_login_name;
	private $user_email;

	private $modules = array(
		'jean'=>array("PMSPS","PMS","PP","AQA"),
		'admire'=>array("RGSTR","QUERY","CMPL","COMPL","SCORECARD","CONTRACT","SLA"),
		'action'=>array("ACTION","RGSTR","SLA","QUERY","RAPS","CAPS","COMPL","JOBS","SCORECARD","CONTRACT","CASEM","CNTRCT"),
		'contentsbar_have_no_title'=>array("TD"),
	);
	

	private $is_assist_help = array("IASSIST");
	private $report_save_path = "reports";
	private $delete_move_path = "deleted";
	
	private $modref;
	private $terms;
	private $ignite_name;
	private $title_logo;
	
	protected $cmp_db;
	

	
	public function __construct($autojob=false) {
		if(!$autojob) {
			//GET SESSION INFO
			$this->mod_ref = isset($_SESSION['ref']) ? $_SESSION['ref'] : "main";	//MODULE REFERENCE
			$this->cmp_code = isset($_SESSION['cc']) ? $_SESSION['cc'] : die($this->autoLogout()); //COMPANY CODE
			$this->cmp_name = isset($_SESSION['ia_cmp_name']) ? $_SESSION['ia_cmp_name'] : $this->getCMPNsess($this->cmp_code);	//CoMPany NAME
			$this->tkid = isset($_SESSION['tid']) ? $_SESSION['tid'] : die("tkid"); //die($this->autoLogout()); //USER ID
			$this->user_id = $this->tkid;
			if($this->tkid=="0000") {
				$this->tkuser = "admin";
				$this->tkname = "Assist Administrator";
				//$this->tkemail = 
			} else {
				$this->tkuser = isset($_SESSION['tku']) ? trim($_SESSION['tku']) : $this->getTKDetails($this->tkid,$this->cmpcode,"tkuser");	//TKUSER
				$this->tkname = isset($_SESSION['tkn']) ? trim($_SESSION['tkn']) : $this->getTKDetails($this->tkid,$this->cmpcode,"tkn");	//TKNAME
				$this->user_email = $this->getTKDetails($this->tkid,$this->cmpcode,"tkemail");
			}
			$this->user_name = $this->tkname;
			$this->user_login_name = $this->tkuser;
			
			if(!isset($_SESSION['cmp_terminology'])) {
				$sql = "SELECT field, value FROM assist_".$this->cmp_code."_setup WHERE ref = 'IA_TERM'";
				$cmp_db = new ASSIST_DB();
				$ct = $cmp_db->mysql_fetch_fld2_one($sql,"field","value");
				if(count($ct)==0) {
					$sql2 = "INSERT INTO `assist_blank_setup` (`id`, `ref`, `refid`, `value`, `comment`, `field`) VALUES (null, 'IA_TERM', 0, 'Department', 'Name of Department', 'DEPT'), (null, 'IA_TERM', 0, 'Section', 'Name of Section', 'SUB')";
					$cmp_db->db_insert($sql2);
					$ct = $cmp_db->mysql_fetch_fld2_one($sql,"field","value");
				}
				$_SESSION['cmp_terminology'] = $ct;
			}




			$this->terms = $_SESSION['cmp_terminology'];
			
			$this->ignite_name = isset($_SESSION['DISPLAY_INFO']['ignite_name']) ? $_SESSION['DISPLAY_INFO']['ignite_name'] : "Ignite Assist";
			$this->title_logo = isset($_SESSION['DISPLAY_INFO']['title_logo']) && file_exists($_SESSION['DISPLAY_INFO']['title_logo']) ? $_SESSION['DISPLAY_INFO']['title_logo'] : "pics/assist_logo_h75.jpg";
			$this->site_code = isset($_SESSION['DISPLAY_INFO']['site_code']) && strlen($_SESSION['DISPLAY_INFO']['site_code'])>0 ? $_SESSION['DISPLAY_INFO']['site_code'] : "igniteassist";
		}
	}

	function autoLogout() {
		echo "<html><head><title>www.ignite4u.co.za</title>
				<link rel='stylesheet' href='/assist.css' type='text/css'>
				<script type='text/javascript'>
				<!--
				function delayer(){
//					parent.location.href = 'index.html';
					parent.location.href = 'https://".$_SERVER['SERVER_NAME']."/index.php?".$this->getSiteCode()."';
				}
				//-->
				</script>
			</head>
			<body onLoad=\"setTimeout('delayer()', 3000)\">
			<h1>Notice</h1>
			<p>Your session on Action Assist has expired.  Please login again.</p>
			<p>You will be redirected to the Login page momentarily.  If your browser does not redirect you, <a href='http://assist.action4u.co.za/index.php?".$this->getSiteCode()."' target=_parent>click here</a>.</p>
			<script type=text/javascript>
				
			</script>
			</body></html>";
	}	
	
	function getTKDetails($id,$cc,$fld) {
		$cmp_db = new ASSIST_DB();
		if($fld=="tkn") { $fld2 = "CONCAT(tkname,' ',tksurname) as tkn"; } else { $fld2 = $fld; }
		$sql = "SELECT $fld2 FROM assist_".$this->cmp_code."_timekeep WHERE tkid = $id";
		$one = $cmp_db->mysql_fetch_one_fld($sql,$fld);
		$cmp_db->db_close();
		return $one;
	}

	function getCMPNsess($cmpcode) {
		$cmp_db = new ASSIST_DB("master");
		$sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";
		$cmprow = $cmp_db->mysql_fetch_one($sql);
		//$rs = AgetRS($sql);
			//$cmprow = mysql_fetch_assoc($rs);
		//mysql_close();
		$_SESSION['ia_cmp_name'] = $cmprow['cmpname'];
		return $cmprow['cmpname'];
	}

	
	function getUserID() {
		return $this->user_id;
	}
	function getLoginName() { return $this->tkuser; }
	function getUserName() {
		return $this->user_name;
	}
	function getUserEmail() {
		return $this->user_email;
	}
	function getCmpCode() {
		return $this->cmp_code;
	}
	function getCmpDisplayName() {
		return $_SESSION['ia_cmp_dispname'];
	}
	function getCmpLogo() {
		return $_SESSION['ia_cmp_logo'];
	}
	function getCmpName() {
		return $_SESSION['ia_cmp_name'];
	}
	function getModRef() {
		return $_SESSION['modref'];
	}
	function getModLocation() {
		return $_SESSION['modlocation'];
	}
	
	function isSupportUser() {
		if($this->tkuser == "support") 
			return true;
		else
			return false;
	}
	
	function isActiveReseller($c="") {
		return $_SESSION['ia_reseller'];
		//return false;
	}
	
	function isAssistHelp($c="") {
		if(strlen($c)==0) { $c = $this->getCmpCode(); }
		return in_array(strtoupper($c),$this->is_assist_help);
	}
	
	function isAdminUser() {
		if($this->tkuser == "admin" && $this->tkid == "0000")
			return true;
		else
			return false;
	}
	
	function getAdminSystemMenu() {
		$menu = array(
			'S'=>"Master Setup",
			'TK'=>"Users",
			'SMS'=>"SMS",
		);
		return $menu;
	}
	function getUserSystemMenu() {
		$menu = array(
			'user_profile'=>"My Profile",
			'changepass'=>"Change Password",
		);
		return $menu;
	}
	function getSystemMenu() {
		$menu = array(
			'userman'=>"User Manuals",
		);
		return $menu;
	}

	function getJeanModules() {
		return $this->modules['jean'];
	}
	
	function getContentsWithNoTitle() {
		return $this->modules['contentsbar_have_no_title'];
	}
	
	function getIgniteName() { return $this->getSiteName(); }	//Old function
	function getSiteName() { return $this->ignite_name; }
	function getIgniteTitleLogo() { return $this->title_logo; }
	function getSiteCode() { return $this->site_code; }
	
	function getReportPath() { return $this->report_save_path; }
	
	function getModTitle($ref="") {
		if(strlen($ref)==0) {
			return $_SESSION['modtext'];
		} else {
			$am = $this->getAdminSystemMenu();
			if(isset($am[$ref]) && $this->isAdminUser()) {
				return $am[$ref];
			} else {
				$sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '".$ref."'";
				$db = new ASSIST_DB("client");
				return $db->mysql_fetch_one_value($sql,"modtext");
			}
		}
	}
	function getTerm($i,$plural=false) {
		$t = $this->terms[$i];
		if($plural) {
			if(substr($t,-1)!="s") {
				$t.="s";
			} else {
				$t.="'";
			}
		}
		return $t; 
	}
	function getPasswordSecurityAnswers() {
		$me = $this->getUserID();
		$db = new ASSIST_DB();
		$sql = "SELECT tkpwdanswer1, tkpwdanswer2, tkpwdanswer3 FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$me."'";
		return $db->mysql_fetch_one($sql);
	}
	
	
	function setSessionDetails($modref,$module) {
			$_SESSION[$modref] = array();

			$_SESSION['modref'] = strtoupper($modref);
			$this->setSessionModuleRef($modref);
			$_SESSION['modtext'] = $module['modtext'];
			$_SESSION['modlocation'] = $module['modlocation'];
			$dbref = "assist_".$this->getCmpCode()."_".$modref;
			$_SESSION['dbref'] = strtolower($dbref);
	}
	function setSessionModuleRef($modref) {
		$_SESSION['ref'] = $modref;
	}
	function logUserActivity($modref) {
		$db = new ASSIST_DB();
		$sql = "INSERT INTO assist_".$this->getCmpCode()."_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) VALUES (null,now(),'".$this->getUserID()."','".$this->getUserName()."','$modref',1,'".$_SERVER["HTTP_USER_AGENT"]."','".$_SERVER['REMOTE_ADDR']."')";
		$db->db_insert($sql);
		unset($db);
	}
	function setModRef($mr) {
		$mr = strtolower($mr);
		$_SESSION['modref'] = $mr;
		$_SESSION['dbref'] = "assist_".$this->getCmpCode()."_".$mr;
	}
	
	
	
	
	function getMenuModules() {
		$data = array();
		$db = new ASSIST_DB();
		
		$sql = "SELECT mm.modref, mm.modlocation, mm.modtext 
				FROM assist_menu_modules mm 
				".($this->isAdminUser() ? "" : "INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users mmu
				  ON mm.modref = mmu.usrmodref
				  AND mmu.usrtkid = '".$this->getUserID()."'")."
				WHERE mm.modyn = 'Y' ".($this->isAdminUser() ? " OR mm.modadminyn = 'Y' " : "")." ORDER BY mm.modtext";
				
		$data = $db->mysql_fetch_all_by_id($sql,"modref");
		return $data;
	}
	
	
	function getOldUserManuals() {
		$adb = new ASSIST_DB("master");
		$data = array();
		$sql = "SELECT * FROM assist_manuals_docs WHERE docyn = 'Y' ".(($this->isAdminUser()) ? " OR docyn = 'A'" : "")." ORDER BY doctitle";
		$rows = $adb->mysql_fetch_all($sql);
		
			foreach($rows as $row) {
				$data['docs'][$row['docid']] = $row;
				if(strlen($row['docmodloc'])>0) {
					$data['location'][$row['docmodloc']][] = $row['docid'];
				}
				if(strlen($row['docmodref'])>0) {
					$data['ref'][$row['docmodref']][] = $row['docid'];
				}
			}
		return $data;
	}
	
	
	
	//PASSWORD FUNCTIONS
	function updateSecurityAnswers($user_id="", $answers) {
		$user_id = strlen($user_id)>0 ? $user_id : $this->getUserID();
		$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep SET ";
		$s = array();
		foreach($answers as $fld => $val) {
			$s[] = $fld." = '$val'";
		}
		$sql.= implode(", ",$s)." WHERE tkid = '".$user_id."'";
		$cdb = new ASSIST_DB();
		return $cdb->db_update($sql);
	}
	

	
	function __destruct() {
		
	}
}



?>