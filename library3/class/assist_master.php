<?php

class ASSIST_MASTER extends ASSIST {

	protected $db_table = "";
	protected $sort_by = array();

	public function __construct() {
		parent::__construct();
	}

	public function getTable() { return $this->db_table; }

	
	public function getSortBy($f="") {
		if(strlen($f)>0) {
			$f = $f.".";
		}
		return " ORDER BY ".$f.implode(", ".$f,$this->sort_by);
	}
	
	public function fetch($option="",$fields="",$ids="") {
		$db = new ASSIST_DB("client");
		$f = "fy";
		if($option=="active") {
			$option = " WHERE ".$f.".".$this->getFld("STATUS")." & ".$this->getFld("ACTIVE")." = ".$this->getFld("ACTIVE")." ";
		} elseif(strlen($option)>0) {
			$option = " WHERE ".$option;
		}
		if(is_array($ids)) {
			$option.=" AND ".$this->getFld("ID")." IN (".implode(",",$ids).") ";
		} elseif(strlen($ids)>0) {
			$option.=" AND ".$this->getFld("ID")." = ".$ids;
		}
		if(strlen($fields)==0) { 
			$fields = $f.".*";
		} else {
			$fields = str_replace("#",$f,$fields);
		}
		$sql = "SELECT ".$fields." FROM ".$this->getTable()." ".$f." ".$option." ".$this->getSortBy($f);
		return $db->mysql_fetch_all_fld($sql,$this->getFld("ID"));
	}

	public function getActive($ids="") {
		return $this->fetch("active","",$ids);
	}

	public function getAll() {
		return $this->fetch();
	}

	public function getLinked($table,$fld,$fields) {
		if(strlen($fields)==0) { $fields = "fy.*"; } else { $fields = str_replace("#","fy",$fields); }
		$db = new ASSIST_DB("client");
		$sql = "SELECT $fields FROM ".$this->getTable()." fy
				INNER JOIN ".$table." t
				ON t.".$fld." = fy.id
				 ".$this->getSortBy("fy");
		return $db->mysql_fetch_all_fld($sql,$this->getFld("ID"));
	}
	
	public function getActiveItemsFormattedForSelect() {
		
	}
	
	
	
	
}

?>