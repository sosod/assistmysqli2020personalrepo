<?php

CLASS ASSIST_SMS extends ASSIST_MODULE_HELPER{
	
	protected $db;
	protected $db_m;
	protected $modmailer;
	protected $mailer;
	private $sms_usr = array('user'=>"ignite",'password'=>"ign1t3sms");
	
	function __construct(){
		parent::__construct();
		$this->db = new ASSIST_DB();
		$this->db_m = new ASSIST_DB("master");
		$this->modmailer = new ASSIST_MODULE_EMAIL();
		$this->mailer = new ASSIST_EMAIL();
	}
	
	/**
	 * Function to get this company's default phone prefix --WRITE ME IF NECESSARY--
	 */
	
	/**
	 * Function to get this company's current SMS balance
	 * @return {int} SMS balance from the company's SMS usage table
	 */
	public function checkMyBalance(){
		$dbref = $this->db->getDBRef();
		$sms_bal_sql = "SELECT sms_balance FROM ".$dbref."_usage ORDER BY sms_datetime DESC LIMIT 1";
		$sms_bal = $this->db->mysql_fetch_one_value($sms_bal_sql, "sms_balance");
		return $sms_bal;
	}
	
	/**
	 * Function to fetch the current SMS balance for ASSIST via the bulkSMS API
	 */
	public function checkBalance(){
			$login = $this->sms_usr;
			$url = "http://bulksms.2way.co.za:5567/eapi/user/get_credits/1/1.1";
			$data = "username=".$login['user']."&password=".$login['password'];
			$params = array('http'      => array(
								'method'       => 'POST',
								'content'      => $data,
				      		)
						);
			$params['http']['header'] = 'Content-type:application/x-www-form-urlencoded';
			$response = @file_get_contents($url."?".$data, false, NULL);
			if ($response === false) {
				$credits = 0;
			} else {
				$result = explode("|",$response);
				if($result[0]==0) {
					$credits = $result[1];
				} else {
					$credits = 0;
				}
			}
		if(!is_numeric($credits*1)){
			return "Error checking balance";
		}else{
			return $credits;
		}
	}
	
	/**
	 * Function to process an SMS for sending to single or multiple recipients! Many at a time, or one at a time
	 * @param {string}	SMS message
	 * @param {string}  Recipient(s) - single string (10 digit number) or array of strings (10 digit numbers)
	 * @return {bool} Response from BulkSMS API on success/fail, or error if parameters were invalid
	 */
	public function sendPolySMS($sms,$recipients){
		if(strlen($sms)>0){
			if(is_array($recipients)){
				if(count($recipients)>0){
					$recipients = array_unique($recipients);
					foreach($recipients as $num){
						$send[$num]=$this->sendSMS($sms,$num);
						$result = json_encode($send);
					}
				}else{
					$result = "Empty recipient array fed in.";
				}
			}else{
				if(strlen($recipients)>0){
					$result = $this->sendSMS($sms,$recipients);
				}else{
					$result = "Blank recipient string fed in.";
				}
			}
		}else{
			$result = "Blank SMS string fed in.";
		}
		return $result;
	}
	
	/**
	 * Function to send an SMS! One at a time, please
	 * @param {string}	SMS message
	 * @param {string}  Recipient - single string (10 digit number)
	 * @return {bool} Response from BulkSMS API
	 */
	public function sendSMS($msg,$cell){
		$balance = $this->checkMyBalance();
		if($balance >= 1){
			//Tagline discount 25% - www.bulksms.com //
			$msg.= " www.bulksms.com";
			$login = $this->sms_usr;
			$url = "http://bulksms.2way.co.za/eapi/submission/send_sms/2/2.0";
			$data = "username=".$login['user']."&password=".$login['password']."&message=".urlencode($msg)."&msisdn=".urlencode($cell);
			$optional_headers = "Content-type:application/x-www-form-urlencoded";
			$params = array('http'      => array(
								'method'       => 'POST',
								'content'      => $data,
								)
							);
			if($optional_headers !== null){
				$params['http']['header'] = $optional_headers;
			}
			$ctx = stream_context_create($params);
			$response = @file_get_contents($url, false, $ctx);
			if($response === false){
				$response = "Problem reading data from $url, No status returned\n";
			}
			return $response;
		}else{
			return array(false,"SMS balance too low");
		}
	}
	
	/**
	 * Function to fetch an array of users' mobile numbers
	 */
	public function getMobilePhones($users){
		$sql = "SELECT tkid, tkphprefix as prefix, tkmobile as num FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid IN (".implode(",",$users).") AND tkstatus = 1";
		$result = $this->db->mysql_fetch_all_by_id($sql, "tkid");
		//return $result;
		if(count($users)==1){
			$number = $result[$users[0]]['num'];
			if(strlen($number) == 10){
				$pref = $result[$users[0]]['prefix'];
				$sliced = substr($number,1,9);
				$x_number = "+".$pref.$sliced;
				$result[$users[0]]['num']=$x_number;
			}
		}
		return $result;
	}
	
	/**
	 * Function to fetch a user's mobile number, birthday, and names to get an SMS ready for their birthday
	 */
	public function getBirthdaySMS($user){
		$sql = "SELECT tkname as name, tksurname as surname, tkphprefix as prefix, tkmobile as num, tkbdate as birthday FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = ".$user." AND tkstatus = 1";
		$result = $this->db->mysql_fetch_one($sql);
		return $result;
	}
	
	/**
	 * Function to check if rows exist in Setup table
	 */
	 public function checkSetup(){
		 $sql = "SELECT * FROM assist_".$this->getCmpCode()."_setup WHERE ref = 'SMS'";
		 $res = $this->db->mysql_fetch_all($sql);
		 if($res && count($res)>2){
		 	return true;
		 }else{
		 	$sql = "INSERT INTO assist_".$this->getCmpCode()."_setup SET ref='SMS', refid=0, value='0000', comment='Administrator'";
		 	$res1 = $this->db->db_insert($sql);
		 	$sql = "INSERT INTO assist_".$this->getCmpCode()."_setup SET ref='SMS', refid=1, value='1', comment='Use SMSs - Yes(1) or No(0)'";
		 	$res2 = $this->db->db_insert($sql);
		 	$sql = "INSERT INTO assist_".$this->getCmpCode()."_setup SET ref='SMS', refid=2, value='100', comment='Running low reminder'";
		 	$res3 = $this->db->db_insert($sql);
			if($this->checkIntRef($res1) && $this->checkIntRef($res2) && $this->checkIntRef($res3)){
			 	return true;
			}else{
				return array("error","1"=>$res1,"2"=>$res2,"3"=>$res3);
			}
		 }
	 }
	 
	 
	 /**
	  * Function to read the setup data from the assist_CMPCODE_setup table
	  */
	  public function readSetup(){
	  	$chk = $this->checkSetup();
		if($chk && !is_array($chk)){
			$sql = "SELECT * FROM assist_".$this->getCmpCode()."_setup WHERE ref = 'SMS'";
		}else{
			return array("Error - checkSetup failed"=>$chk);
		}
		return $this->db->mysql_fetch_all($sql);
	  }
	  
	  
	  /**
	   * Function for updating the setup rules
	   * @param {string} Feed in the tkid of the new admin user, if required
	   * @param {bool} Feed in 1 or 0 for 'Use SMSs - Yes/No'
	   * @return Update response
	   */
	  public function updateSetup($admin="", $use){
	  	$chk = $this->checkSetup();
		if($chk && !is_array($chk)){
			if(strlen($admin)>0){
				$sql = "UPDATE assist_".$this->getCmpCode()."_setup SET value = '$admin' WHERE ref = 'SMS' AND refid = 0";
				$res = $this->db->db_update($sql);
			}
			$sql = "UPDATE assist_".$this->getCmpCode()."_setup SET value = '$use' WHERE ref = 'SMS' AND refid = 1";
			$res = $this->db->db_update($sql);
			return array("ok","SMS rule updated successfully");
		}else{
			return array("Error - checkSetup failed"=>$chk);
		}
	  }
	  
	  /**
	   * Function for updating the reminder email limit
	   * @param {string} Feed in the number of SMSs you want to be the cutoff
	   * @return Update response
	   */
	  public function updateSetupLimits($lim){
	  	$chk = $this->checkSetup();
		if($chk && !is_array($chk)){
			$sql = "UPDATE assist_".$this->getCmpCode()."_setup SET value = '$lim' WHERE ref = 'SMS' AND refid = 2";
			$res = $this->db->db_update($sql);
			//return $sql;
			if($this->checkIntRef($res)){
				return array("ok","SMS reminder limit updated successfully");
			}else{
				return array("Error","SMS limit wasn't saved properly");
			}
		}else{
			return array("Error - checkSetup failed",$chk);
		}
	  }
	   
	   /**
	    * Function to say whether or not SMSs can be used within this company's system
	    */
	   public function canIUseSMS(){
			$sql = "SELECT value FROM assist_".$this->getCmpCode()."_setup WHERE ref = 'SMS' AND refid=1";
			$res = $this->db->mysql_fetch_one_value($sql, "value");
			return $res;
	   }
	   
	   /**
	    * Function to say whether or not a reference code can be used within this company's system
	    */
	   public function canIUseWord($word){
	   	$word = strtolower($this->getCmpCode()) . "-" . $word;
			$sql = "SELECT sms_ref FROM assist_".$this->getCmpCode()."_sms_usage WHERE sms_ref = '$word'";
			$res = $this->db->mysql_fetch_one_value($sql, "sms_ref");
			if(strlen($res)>0 && $res != false){
				return false;
			}else{
				return true;
			}
	   }

	   /**
	    * Function to check how many SMSs should be left before sending notification to buy more
	    */
	   public function checkLimits(){
			$sql = "SELECT value FROM assist_".$this->getCmpCode()."_setup WHERE ref = 'SMS' AND refid=2";
			$res = $this->db->mysql_fetch_one_value($sql, "value");
			return $res;
	   }
	   
	   public function updateTransaction($bundle,$seed){
	   		//Bundle processing
			$sql = "SELECT * FROM assist_sms_bundle WHERE bundle_id = $bundle";
			$bundles = $this->db_m->mysql_fetch_one($sql);
			$b_size = $bundles['bundle_size'];
			//Updating transaction table
			$sql = "SELECT sms_balance FROM assist_".$this->getCmpCode()."_sms_usage ORDER BY sms_datetime DESC LIMIT 1";
			$balance = $this->db->mysql_fetch_one_value($sql,"sms_balance");
			$sql = "INSERT INTO assist_".$this->getCmpCode()."_sms_usage SET sms_datetime = NOW(), sms_status = 2, sms_ref = '$seed', sms_in = $b_size, sms_out=0, sms_balance = $balance + $b_size";
			$result = $this->db->db_insert($sql);
			if($this->checkIntRef($result)){
				$result = array("ok","Transaction processed successfully. Your SMS balance has been updated.");
			}else{
				$result = array("error","Transaction processing failed.");
			}
			return $result;
	   }


	/**
	 * Function to alert assist that an order is waiting to be processed
	 * @param bundle {int}
	 * @param seed {string}
	 */
	   public function tempTransaction($bundle,$seed){
	   		//Bundle processing
			$sql = "SELECT * FROM assist_sms_bundle WHERE bundle_id = $bundle";
			$bundles = $this->db_m->mysql_fetch_one($sql);
			$b_size = $bundles['bundle_size']*0.1;
			//Updating transaction table
			$sql = "SELECT sms_balance FROM assist_".$this->getCmpCode()."_sms_usage ORDER BY sms_datetime DESC LIMIT 1";
			$balance = $this->db->mysql_fetch_one_value($sql,"sms_balance");
			$sql = "INSERT INTO assist_".$this->getCmpCode()."_sms_usage SET sms_datetime = NOW(), sms_status = 3, sms_ref = '$seed', sms_in = $b_size, sms_out=0, sms_balance = $balance + $b_size";
			$result = $this->db->db_insert($sql);
			$sql3 = "SELECT * FROM assist_sms_orders WHERE or_ref = '$seed'";
			$pay_ltr = $this->db_m->mysql_fetch_one($sql3);
			//return $pay_ltr;
			if($pay_ltr && count($pay_ltr)>0){
				$sql2 = "UPDATE assist_sms_orders SET or_date = NOW(), or_status=1 WHERE or_ref = '$seed'";
				$result2 = $this->db_m->db_update($sql2);
			}else{
				$sql2 = "INSERT INTO assist_sms_orders SET or_ref = '$seed', or_bundle = $bundle, or_date = NOW(), or_status=1";
				//return $sql2;
				$result2 = $this->db_m->db_insert($sql2);
			}
			if($this->checkIntRef($result) && $this->checkIntRef($result2)){
				$b_price = $bundles['bundle_price'];
				$b_size *=10;
				$subject = "SMS bundle ordered ($seed)";
				$message = "Someone has ordered a bundle of $b_size SMSs (R $b_price.00) on Assist (PoP has been attached). Their order's reference number is $seed.
Go to Assist at https://assist.ignite4u.co.za to process the order.";
				$this->mailer->setRecipient("duncancosser@gmail.com");
				$this->mailer->setSubject($subject);
				$this->mailer->setBody($message);
				//$this->mailer->setSender("no-reply@assist4u.co.za");
				$send = $this->mailer->sendEmail();
				if($send) {
					$result = array("ok","Transaction processed successfully. Your SMS balance will be updated within 48 hours. You have received a 10% advance on your bundle.");
				}else{
					$result = array("info","Transaction was processed successfully, but Assist could not be reached to notify them about the transaction. You have received a 10% advance on your bundle.");
				}
			}else{
				$result = array("error","Transaction processing failed: $result, $result2");
			}
			return $result;
	   }
	   
	   public function storeTransaction($bundle,$seed){
	   		$sql = "INSERT INTO assist_sms_orders SET or_ref = '$seed', or_bundle=$bundle, or_date = NOW(), or_status=2";
			$result = $this->db_m->db_insert($sql);
			if($this->checkIntRef($result)){
		   		$mail = $this->tempMail($bundle, $seed);
				if($mail){
					$result = array("ok","Transaction storage was successful. Use your code $seed when you are ready to attach Proof of Payment.");
				}else{
					$result = array("info","Transaction storage was successful, but mail generation failed.");
				}
			}else{
				$result = array("error","Transaction storage failed: $result");
			}
	   }
	   
	   /**
	    * Function to get the usage logs
	    */
	   public function getUsage($strip){
	   	$sql = "SELECT * FROM assist_".$this->getCmpCode()."_sms_usage";
	   	$result = $this->db->mysql_fetch_all_by_id($sql, "sms_id");
		if($strip){
			foreach($result as $key=>$val){
				if($val['sms_status'] == 1){
					$result[$key]['sms_status']="System";
				}else if($val['sms_status'] == 2){
					$result[$key]['sms_status']="Activated";
				}else if($val['sms_status'] == 3){
					$result[$key]['sms_status']="Awaiting Activation";
				}
			}
		}
		return $result;
	   }
	   
	   
	   /**
	    * INCOMPLETE FUNCTION VERY MUCH - Unused
	    */
	   public function updateLog(){
	   	$sql = "INSERT INTO assist_".$this->getCmpCode()."_sms_log SET log_tkid = '".$this->getUserID()."', ";
	   }
	   
	   /**
	    * Function to send a reminder email with your bundle reference code
	    * NEEDS: responsible person for SMS purchases
	    */
	    public function tempMail($bundle, $seed){
	    	//$emails = $this->modmailer->getEmailAddresses(array(),array());
	    	$emails = array("to"=>array("test","lol"));
	    	$sql = "SELECT * FROM assist_sms_bundle WHERE bundle_id = $bundle";
			$bundles = $this->db_m->mysql_fetch_one($sql);
			$b_size = $bundles['bundle_size'];
			$b_price = $bundles['bundle_price'];
		if(count($emails['to'])>0) {
			$subject = "SMS bundle order information ($seed)";
			
			$message = "You have ordered a bundle of $b_size SMSs for use on Action Assist. Your order's reference number is $seed.
			
Assist's banking details are as follows:
	Bank: Investec Bank Limited
	Branch: 100 Grayston Drive, Sandton
	Branch code: 580105
	Current Account number: 10011404453
			
Once you have transferred the funds for the SMS bundle (R $b_price.00) to the orders department, you will receive proof of payment.

Please log into Assist at https://assist.ignite4u.co.za to complete the order process when you have proof of payment.";
			//$this->mailer->setRecipient($emails['to']);
			$this->mailer->setRecipient("duncancosser@gmail.com");
			$this->mailer->setSubject($subject);
			$this->mailer->setBody($message);
			//$this->mailer->setSender($emails['reply_to']);
			//$this->mailer->setSender("no-reply@assist4u.co.za");
			$this->mailer->sendEmail();
			return array("ok","Order processed successfully");
		}
		return array("error","Order processing failed. No valid email address found.");
	    }

		/**
		 * Function to activate an SMS bundle which has been paid for
		 */
		public function activateBundle($ref){
			$cmp_code_arr = explode("-",$ref);
			$cmp_code = $cmp_code_arr[0];
			$sql = "UPDATE assist_sms_orders SET or_status = 9, or_date = NOW() WHERE or_ref = '$ref'";
			$res1 = $this->db_m->db_update($sql);
			$sql = "UPDATE assist_".$cmp_code."_sms_usage SET sms_in = (10 * sms_in), sms_balance = sms_balance +  (sms_in*0.9), sms_status = 2, sms_datetime = NOW() WHERE sms_ref = '$ref'";
			$db = new ASSIST_DB("client",$cmp_code);
			$res2 = $db->db_update($sql);
			if($this->checkIntRef($res1) && $this->checkIntRef($res2)){
				return array("ok","Activation of bundle $ref successful");
			}else{
				return array("error","Activation failed. Error A:".$res1.", Error B:".$res2);
			}
		}
		
		/**
		 * Function to send an SMS bundle as a gift
		 * @param {string} CMPCODE
		 * @param {int} ID of bundle to be sent
		 */
		public function giveBundle($cmpcode,$bundle){
			$sql = "SELECT * FROM assist_sms_bundle WHERE bundle_id = $bundle";
			$bundles = $this->db_m->mysql_fetch_one($sql);
			$b_size = $bundles['bundle_size'];
			$db = new ASSIST_DB("client",$cmpcode);
			$token = substr(md5(rand()), 0, 4);
			$word = "GIFT-".$token."-".$cmpcode;
			$sql = "SELECT sms_balance FROM assist_".$cmpcode."_sms_usage ORDER BY sms_datetime DESC LIMIT 1";
			$balance = $db->mysql_fetch_one_value($sql, "sms_balance");
			$sql = "INSERT INTO assist_".$cmpcode."_sms_usage SET sms_in = $b_size, sms_balance = $balance + $b_size, sms_datetime = NOW(), sms_status = 2, sms_ref = '$word'";
			$res = $db->db_insert($sql);
			if($this->checkIntRef($res)){
				return array("ok","Gift sent successfully - reference code: '".$word."'");
			}else{
				return array("error","Database error with sending gift: ".$res);
			}
		}
}

?>