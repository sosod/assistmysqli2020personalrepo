<?php
/* Email sending class for Ignite Assist
Created by: Janet Currie (c)
Created on: 12 September 2012
*/


class ASSIST_EMAIL {

/* VARIABLES */ 
	private $to;
	private $reply_to;
	private $from;
	private $default_from = "no-reply@ignite4u.co.za";
	private $header;
	private $subject;
	private $message;
	private $content_type;
	private $cc;
	private $bcc;
	
	private $status = array(
		'to' => false,
		'subject' => false,
		'body'=>false
	);

	public function __construct($to = "", $subject = "", $message = "", $content_type = "TEXT", $cc = "", $bcc = "", $reply_to = "") {
		$this->setRecipient($to);
		$this->setSubject($subject);
		$this->setBody($message);
		$this->setCC($cc);
		$this->setBCC($bcc);
		$this->setSender($reply_to);
		$this->setContentType($content_type);
	}
	
	function setRecipient($to) {
		$this->status['to'] = false;
		if(is_array($to)) {
			$rec = array();
			if(count($to)==2 && isset($to['name'])) {
				$this->to = $this->setNamedEmail($to);
				$this->status['to'] = true;
			} else {
				foreach($to as $t) {
					if(is_array($t)) {
						if(count($t)==2 && isset($t['name'])) {
							$rec[] = $this->setNamedEmail($t);
						}
					} else {
						$rec[] = $t;
					}
				}
				if(count($rec)>0) {
					$this->to = implode(",",$rec);
					$this->status['to'] = true;
				}
			}
		} else {
			$this->to = $to;
			$this->status['to'] = true;
		}
	}
	private function setNamedEmail($r) {
		$str = $r['name']." <".$r['email'].">";
		return $str;
	}
	
	function setSubject($sub="") {
		$this->subject = $sub;
		if(strlen($sub)>0) { $this->status['subject'] = true; } else { $this->status['subject'] = false; }
	}
	
	function setBody($body) {
		$this->message = $body;
		if(strlen($body)>0) { $this->status['body'] = true; } else { $this->status['body'] = false; }
	}
	
	function setHTMLBody($body) {

		$email = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\">
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>
		<style type=text/css>
			hr {
				color: #000099;
			}
			h1 {
				text-decoration: none;
				font-size: 16pt;
				line-height: 20pt;
				font-weight: bold;
				padding: 0 0 0 0;
				margin: 15 0 10 5;
				font-family: \"Comic Sans MS\", Arial, Helvetica, sans-serif;
				color: #000099;
				letter-spacing: 0.07em;
			}
			h2 {
				text-decoration: none;
				font-size: 14pt;
				line-height: 17pt;
				font-weight: bold;
				padding: 0 0 0 0;
				margin: 15 0 10 5;
				font-family: \"Comic Sans MS\", Arial, Helvetica, sans-serif;
				color: #000099;
				letter-spacing: 0.07em;
			}
			p {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 10pt;
				line-height: 12pt;
				color: #000000;
				margin: 10px 0px 10px;
				padding: 0px 0px 0px 5px;
			}
			table {
				border-collapse: collapse;
				border: solid 1px #ababab;
			}
			table td {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 8.5pt;
				line-height: 12pt;
				color: #000000;
				border: solid 1px #ababab;
				vertical-align: top;
				padding: 5px 5px 5px 5px;
				text-align: left;
			}
			table th {
				background-color: #000099;
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 8.5pt;
				text-align: center;
				font-weight: bold;
				line-height: 12pt;
				color: #ffffff;
				padding: 5px 5px 5px 5px;
				border: solid 1px #ffffff;
			}
			.overdue { color: #cc0001; }
			.today { color: #009900; }
			.soon { color: #FE9900; }
			body { 
				font-family: Verdana, Arial, Helvetica, sans-serif; 
				font-size: 10pt;
				line-height: 12pt;
			}
		</style>
		<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
		";	
		$email.= $body;
		$email.= "</body></html>";	
		//$filename = "Results/TESTEMAIL_".date("Ymd_His").".html";
       //$file = fopen($filename,"w");
       //fwrite($file,$email."\n");
       //fclose($file);
		$this->setBody($email);
	}
	
	function setContentType($ct) {
		$this->content_type = $ct;
		$this->setHeader();
	}
	
	function setCC($cc="") {
		$this->cc = $cc;
		$this->setHeader();
	}
	
	function setBCC($bcc="") {
		$this->bcc = $bcc;
		$this->setHeader();
	}
	
	function setSender($send) {
		if(is_array($send)) {
			if(count($send)==2 && isset($send['name']) && isset($send['email'])) {
				$this->reply_to = $this->setNamedEmail($send);
				$this->from = $this->setNamedEmail(array('name'=>$send['name'],'email'=>$this->default_from));
			} else {
				$this->reply_to = "";
				$this->from = $this->default_from;
			}
		} else {
			if(strlen($send)>0) {
				$this->reply_to = $send;
				$this->from = $this->default_from;
			} else {
				$this->reply_to = "";
				$this->from = $this->default_from;
			}
		}
		$this->setHeader();
	}
	
	function setHeader() {
		$header = "From: ".$this->from."\r\n";
		if(strlen($this->reply_to)>0) { 
			$header.="Reply-to: ".$this->reply_to."\r\n";
		}
		if(strlen($this->cc)>0) {
			$header.="CC: ".$this->cc."\r\n";
		}
		if(strlen($this->bcc)>0) {
			$header.="BCC: ".$this->bcc."\r\n";
		}
		switch($this->content_type) {
			case "HTML":
				$header.="Content-type: text/html; charset=us-ascii";
				$this->setHTMLBody($this->getBody());
				break;
			case "TEXT":
				$header.="";
				break;
			default:
				if(strlen($this->content_type)>0) {
					$header.=$this->content_type;
				}
				break;
		}
		$this->header = $header;
	}
	
	
	
	
	function getRecipient() {
		return $this->to;
	}
	
	function getSubject() {
		return $this->subject;
	}
	
	function getBody() {
		return $this->message;
	}
	
	function getContentType() {
		return $this->content_type;
	}
	
	function getCC() {
		return $this->cc;
	}
	
	function getBCC() {
		return $this->bcc;
	}
	
	function getSender() {
		return $this->reply_to;
	}
	
	function getHeader() {
		return $this->header;
	}
	
	
	
	function sendEmail() {
		if($this->status['to']==true && $this->status['subject']==true && $this->status['body']==true) {
			mail($this->to,$this->subject,$this->message,$this->header);
			return true;
		} else {
			return false;
		}
	}
	
	
	
	function __destruct() {
		
	} 

} //end class 

?>