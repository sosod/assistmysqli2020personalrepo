$.widget("ui.paging",{

	options : {
		url				: "inc_controller.php",
		page_options	: "",
		parent_id		: "paging_obj",
		table_id		: "list_view",
		firstbtn_id		: "firstbtn",
		prevbtn_id		: "prevbtn",
		nextbtn_id		: "nextbtn",
		lastbtn_id		: "lastbtn",
		select_id		: "page_picker",
		totalpages		: 1,
		totalrows		: 0,
		currentpage		: 1,
		firstrecord		: false,
		prevrecord		: false,
		nextrecord		: false,
		lastrecord		: false,
		pagelimit		: 20,
		paging_options	: []
	},
	
	_init : function() {
		var self = this;
		parent_id = self.options.parent_id;
			$('#'+parent_id+' button').css({'padding':'3px'});
			$('#'+parent_id+' button:first').button({icons:{primary:'ui-icon-arrowthickstop-1-w'},text:false})
				.next().button({icons:{primary:'ui-icon-arrowthick-1-w'},text:false})
				.next()
				.next().button({icons:{primary:'ui-icon-arrowthick-1-e'},text:false})
				.next().button({icons:{primary:'ui-icon-arrowthickstop-1-e'},text:false});
		self._preparePaging();
	},
	_preparePaging : function() {
		var self = this;
		self._setPagingRecords(self.options.paging_options);
		self._validateButtons();	
		$('#'+self.options.parent_id+' select.'+self.options.select_id).val(self.options.currentpage);		
	},
	_setPagingRecords : function(paging_options) {
		var self = this;
		if(paging_options.hasOwnProperty('totalpages')){
			self.options.totalpages = paging_options.totalpages;
		}
		if(paging_options.hasOwnProperty('totalrows')){
			self.options.totalrows = paging_options.totalrows;
		}
		if(paging_options.hasOwnProperty('currentpage')){
			self.options.currentpage = paging_options.currentpage;
		}
		if(paging_options.hasOwnProperty('first')){
			self.options.firstrecord = paging_options.first;
		}
		if(paging_options.hasOwnProperty('prev')){
			self.options.prevrecord = paging_options.prev;
		}
		if(paging_options.hasOwnProperty('next')){
			self.options.nextrecord = paging_options.next;
		}
		if(paging_options.hasOwnProperty('last')){
			self.options.lastrecord = paging_options.last;
		}
		if(paging_options.hasOwnProperty('pagelimit')){
			self.options.pagelimit = paging_options.pagelimit;
		}
	},
	
	_validateButtons : function(deactivate) {
		var self = this;
		if(self.options.totalpages==1 || deactivate===true) {
			$('#'+self.options.parent_id+' button').each(function() {
				self._deactivatePagingObject($(this));
			});
			if(deactivate!==true) {
				self._deactivatePagingObject($('#'+self.options.parent_id+' select.'+self.options.select_id));
			}
		} else {
			$('#'+self.options.parent_id+' button').each(function() {
				
				if($(this).hasClass(self.options.firstbtn_id)) {
					self._setPagingButtonStatus($(this),self.options.firstrecord);
				} else if($(this).hasClass(self.options.prevbtn_id)) {
					self._setPagingButtonStatus($(this),self.options.prevrecord);
				} else if($(this).hasClass(self.options.nextbtn_id)) {
					self._setPagingButtonStatus($(this),self.options.nextrecord);
				} else if($(this).hasClass(self.options.lastbtn_id)) {
					self._setPagingButtonStatus($(this),self.options.lastrecord);
				}
			});
		}
	},

	_setPagingButtonStatus : function($me,opt) {
		var self = this;
		if(opt===false || opt=="false") {
			self._deactivatePagingObject($me);
		} else {
			self._activatePagingObject($me);
		}
	},

	_activatePagingObject : function($me) {
		if($me.get(0).nodeName.toUpperCase()=="BUTTON") {
			$me.removeClass('ui-state-default').addClass('ui-widget-header').css('cursor','pointer');
		}
		$me.prop('disabled',false);
	},
	
	_deactivatePagingObject : function($me) {
		if($me.get(0).nodeName.toUpperCase()=="BUTTON") {
			$me.removeClass('ui-widget-header ui-state-focus ui-state-hover ui-state-active').addClass('ui-state-default').css('cursor','not-allowed');	
		}
		$me.prop('disabled',true);
	},
	
	buttonClick : function($me) { //alert("button click");
		var self = this;
		if($me.hasClass(self.options.firstbtn_id)) {
			self.processChange(self.options.firstrecord);
		} else if($me.hasClass(self.options.prevbtn_id)) {
			self.processChange(self.options.prevrecord);
		} else if($me.hasClass(self.options.nextbtn_id)) {
			self.processChange(self.options.nextrecord);
		} else if($me.hasClass(self.options.lastbtn_id)) {
			self.processChange(self.options.lastrecord);
		}
	},
	
	changePage : function(page) {
		var self = this;
		if(page!="ALL") {
			var start = (page-1)*self.options.pagelimit;
		} else {
			var start = -1;
		}
		self.processChange(start);
	},
	
	processChange : function(start) { //alert(start);
		var self = this;
		AssistHelper.processing();
		var dta = self.options.page_options+"&options[start]="+start;
		var url = self.options.url;
		//alert(url);
		//alert(dta);
		var result = AssistHelper.doAjax(url,dta);
		var display = result.display;
		//console.log(result);
		//alert(result.display);
		if(start>=0) {
			self.options.paging_options = result.paging;
			self._preparePaging();
		} else {
			self._validateButtons(true);
		}
		//$my_parent = $('#'+self.options.parent_id).parent().parent().parent().parent(); 
		//alert($my_parent.get(0).nodeName+" => "+$my_parent.prop("class"));
		$('table #'+self.options.table_id+' tr:gt(0)').remove();
		$('table #'+self.options.table_id+'').append(display);
		AssistHelper.closeProcessing();
		//$('table #'+self.options.table_id+' tr:gt(0)').each(function() {
		//	$(this).find("td:last").append($("<input />",{type:"button",value:"something"}));
		//});
		$('table #'+self.options.table_id).find("input:button").on("click",function() { 
			tableButtonClick($(this)); 
		});
	}
	
});