var AssistString = {
	
	code : function(string) {
		return this.htmlentities(string,"ENT_QUOTES");
	},
	
	//JS alternative to php.htmlentities :: http://phpjs.org/functions/htmlentities/
	htmlentities : function(string, quote_style, charset, double_encode) {
	  var hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style),
		symbol = '';
	  string = string == null ? '' : string + '';

	  if (!hash_map) {
		return false;
	  }

	  if (quote_style && quote_style === 'ENT_QUOTES') {
		hash_map["'"] = '&#039;';
	  }

	  if (!!double_encode || double_encode == null) {
		for (symbol in hash_map) {
		  if (hash_map.hasOwnProperty(symbol)) {
			string = string.split(symbol).join(hash_map[symbol]);
		  }
		}
	  } else {
		string = string.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g, function (ignore, text, entity) {
		  for (symbol in hash_map) {
			if (hash_map.hasOwnProperty(symbol)) {
			  text = text.split(symbol).join(hash_map[symbol]);
			}
		  }

		  return text + entity;
		});
	  }

	  return string;
	} ,
	//end htmlentities()

	decode : function(string) {
		return this.html_entity_decode(string);
	},
	
	//JS alternative to php.html_entity_decode :: http://phpjs.org/functions/html_entity_decode
	html_entity_decode : function(string, quote_style) {
	  var hash_map = {},
		symbol = '',
		tmp_str = '',
		entity = '';
	  tmp_str = string.toString();

	  if (false === (hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style))) {
		return false;
	  }

	  delete(hash_map['&']);
	  hash_map['&'] = '&amp;';

	  for (symbol in hash_map) {
		entity = hash_map[symbol];
		tmp_str = tmp_str.split(entity).join(symbol);
	  }
	  tmp_str = tmp_str.split('&#039;').join("'");

	  return tmp_str;
	  
	} ,		
	//end html_entity_decode()

	//Required for html entity functions :: http://phpjs.org/functions/get_html_translation_table
	get_html_translation_table : function(table, quote_style) {
	  var entities = {},
		hash_map = {},
		decimal;
	  var constMappingTable = {},
		constMappingQuoteStyle = {};
	  var useTable = {},
		useQuoteStyle = {};

	  // Translate arguments
	  constMappingTable[0] = 'HTML_SPECIALCHARS';
	  constMappingTable[1] = 'HTML_ENTITIES';
	  constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
	  constMappingQuoteStyle[2] = 'ENT_COMPAT';
	  constMappingQuoteStyle[3] = 'ENT_QUOTES';

	  useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
	  useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';

	  if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
		throw new Error("Table: " + useTable + ' not supported');
		// return false;
	  }

	  entities['38'] = '&amp;';
	  if (useTable === 'HTML_ENTITIES') {
		entities['160'] = '&nbsp;';
		entities['161'] = '&iexcl;';
		entities['162'] = '&cent;';
		entities['163'] = '&pound;';
		entities['164'] = '&curren;';
		entities['165'] = '&yen;';
		entities['166'] = '&brvbar;';
		entities['167'] = '&sect;';
		entities['168'] = '&uml;';
		entities['169'] = '&copy;';
		entities['170'] = '&ordf;';
		entities['171'] = '&laquo;';
		entities['172'] = '&not;';
		entities['173'] = '&shy;';
		entities['174'] = '&reg;';
		entities['175'] = '&macr;';
		entities['176'] = '&deg;';
		entities['177'] = '&plusmn;';
		entities['178'] = '&sup2;';
		entities['179'] = '&sup3;';
		entities['180'] = '&acute;';
		entities['181'] = '&micro;';
		entities['182'] = '&para;';
		entities['183'] = '&middot;';
		entities['184'] = '&cedil;';
		entities['185'] = '&sup1;';
		entities['186'] = '&ordm;';
		entities['187'] = '&raquo;';
		entities['188'] = '&frac14;';
		entities['189'] = '&frac12;';
		entities['190'] = '&frac34;';
		entities['191'] = '&iquest;';
		entities['192'] = '&Agrave;';
		entities['193'] = '&Aacute;';
		entities['194'] = '&Acirc;';
		entities['195'] = '&Atilde;';
		entities['196'] = '&Auml;';
		entities['197'] = '&Aring;';
		entities['198'] = '&AElig;';
		entities['199'] = '&Ccedil;';
		entities['200'] = '&Egrave;';
		entities['201'] = '&Eacute;';
		entities['202'] = '&Ecirc;';
		entities['203'] = '&Euml;';
		entities['204'] = '&Igrave;';
		entities['205'] = '&Iacute;';
		entities['206'] = '&Icirc;';
		entities['207'] = '&Iuml;';
		entities['208'] = '&ETH;';
		entities['209'] = '&Ntilde;';
		entities['210'] = '&Ograve;';
		entities['211'] = '&Oacute;';
		entities['212'] = '&Ocirc;';
		entities['213'] = '&Otilde;';
		entities['214'] = '&Ouml;';
		entities['215'] = '&times;';
		entities['216'] = '&Oslash;';
		entities['217'] = '&Ugrave;';
		entities['218'] = '&Uacute;';
		entities['219'] = '&Ucirc;';
		entities['220'] = '&Uuml;';
		entities['221'] = '&Yacute;';
		entities['222'] = '&THORN;';
		entities['223'] = '&szlig;';
		entities['224'] = '&agrave;';
		entities['225'] = '&aacute;';
		entities['226'] = '&acirc;';
		entities['227'] = '&atilde;';
		entities['228'] = '&auml;';
		entities['229'] = '&aring;';
		entities['230'] = '&aelig;';
		entities['231'] = '&ccedil;';
		entities['232'] = '&egrave;';
		entities['233'] = '&eacute;';
		entities['234'] = '&ecirc;';
		entities['235'] = '&euml;';
		entities['236'] = '&igrave;';
		entities['237'] = '&iacute;';
		entities['238'] = '&icirc;';
		entities['239'] = '&iuml;';
		entities['240'] = '&eth;';
		entities['241'] = '&ntilde;';
		entities['242'] = '&ograve;';
		entities['243'] = '&oacute;';
		entities['244'] = '&ocirc;';
		entities['245'] = '&otilde;';
		entities['246'] = '&ouml;';
		entities['247'] = '&divide;';
		entities['248'] = '&oslash;';
		entities['249'] = '&ugrave;';
		entities['250'] = '&uacute;';
		entities['251'] = '&ucirc;';
		entities['252'] = '&uuml;';
		entities['253'] = '&yacute;';
		entities['254'] = '&thorn;';
		entities['255'] = '&yuml;';
	  }

	  if (useQuoteStyle !== 'ENT_NOQUOTES') {
		entities['34'] = '&quot;';
	  }
	  if (useQuoteStyle === 'ENT_QUOTES') {
		entities['39'] = '&#39;';
	  }
	  entities['60'] = '&lt;';
	  entities['62'] = '&gt;';


	  // ascii decimals to real symbols
	  for (decimal in entities) {
		if (entities.hasOwnProperty(decimal)) {
		  hash_map[String.fromCharCode(decimal)] = entities[decimal];
		}
	  }

	  return hash_map;
	},
	//end get_html_translation_table

	
	stripslashes : function(str) {
	  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // +   improved by: Ates Goral (http://magnetiq.com)
	  // +      fixed by: Mick@el
	  // +   improved by: marrtins
	  // +   bugfixed by: Onno Marsman
	  // +   improved by: rezna
	  // +   input by: Rick Waldron
	  // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
	  // +   input by: Brant Messenger (http://www.brantmessenger.com/)
	  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
	  // *     example 1: stripslashes('Kevin\'s code');
	  // *     returns 1: "Kevin's code"
	  // *     example 2: stripslashes('Kevin\\\'s code');
	  // *     returns 2: "Kevin\'s code"
	  return (str + '').replace(/\\(.?)/g, function (s, n1) {
		switch (n1) {
		case '\\':
		  return '\\';
		case '0':
		  return '\u0000';
		case '':
		  return '';
		default:
		  return n1;
		}
	  });
	},

	
	substr : function (str, start, len) {
	  // Returns part of a string
	  //
	  // version: 909.322
	  // discuss at: http://phpjs.org/functions/substr
	  // +     original by: Martijn Wieringa
	  // +     bugfixed by: T.Wild
	  // +      tweaked by: Onno Marsman
	  // +      revised by: Theriault
	  // +      improved by: Brett Zamir (http://brett-zamir.me)
	  // %    note 1: Handles rare Unicode characters if 'unicode.semantics' ini (PHP6) is set to 'on'
	  // *       example 1: substr('abcdef', 0, -1);
	  // *       returns 1: 'abcde'
	  // *       example 2: substr(2, 0, -6);
	  // *       returns 2: false
	  // *       example 3: ini_set('unicode.semantics',  'on');
	  // *       example 3: substr('a\uD801\uDC00', 0, -1);
	  // *       returns 3: 'a'
	  // *       example 4: ini_set('unicode.semantics',  'on');
	  // *       example 4: substr('a\uD801\uDC00', 0, 2);
	  // *       returns 4: 'a\uD801\uDC00'
	  // *       example 5: ini_set('unicode.semantics',  'on');
	  // *       example 5: substr('a\uD801\uDC00', -1, 1);
	  // *       returns 5: '\uD801\uDC00'
	  // *       example 6: ini_set('unicode.semantics',  'on');
	  // *       example 6: substr('a\uD801\uDC00z\uD801\uDC00', -3, 2);
	  // *       returns 6: '\uD801\uDC00z'
	  // *       example 7: ini_set('unicode.semantics',  'on');
	  // *       example 7: substr('a\uD801\uDC00z\uD801\uDC00', -3, -1)
	  // *       returns 7: '\uD801\uDC00z'
	  // Add: (?) Use unicode.runtime_encoding (e.g., with string wrapped in "binary" or "Binary" class) to
	  // allow access of binary (see file_get_contents()) by: charCodeAt(x) & 0xFF (see https://developer.mozilla.org/En/Using_XMLHttpRequest ) or require conversion first?
	  var i = 0,
		allBMP = true,
		es = 0,
		el = 0,
		se = 0,
		ret = '';
	  str += '';
	  var end = str.length;

	  // BEGIN REDUNDANT
	  this.php_js = this.php_js || {};
	  this.php_js.ini = this.php_js.ini || {};
	  // END REDUNDANT
	  switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
	  case 'on':
		// Full-blown Unicode including non-Basic-Multilingual-Plane characters
		// strlen()
		for (i = 0; i < str.length; i++) {
		  if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
			allBMP = false;
			break;
		  }
		}

		if (!allBMP) {
		  if (start < 0) {
			for (i = end - 1, es = (start += end); i >= es; i--) {
			  if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
				start--;
				es--;
			  }
			}
		  } else {
			var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
			while ((surrogatePairs.exec(str)) != null) {
			  var li = surrogatePairs.lastIndex;
			  if (li - 2 < start) {
				start++;
			  } else {
				break;
			  }
			}
		  }

		  if (start >= end || start < 0) {
			return false;
		  }
		  if (len < 0) {
			for (i = end - 1, el = (end += len); i >= el; i--) {
			  if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
				end--;
				el--;
			  }
			}
			if (start > end) {
			  return false;
			}
			return str.slice(start, end);
		  } else {
			se = start + len;
			for (i = start; i < se; i++) {
			  ret += str.charAt(i);
			  if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
				se++; // Go one further, since one of the "characters" is part of a surrogate pair
			  }
			}
			return ret;
		  }
		  break;
		}
		// Fall-through
	  case 'off':
		// assumes there are no non-BMP characters;
		//    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
	  default:
		if (start < 0) {
		  start += end;
		}
		end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
		// PHP returns false if start does not fall within the string.
		// PHP returns false if the calculated end comes before the calculated start.
		// PHP returns an empty string if start and end are the same.
		// Otherwise, PHP returns the portion of the string from start to end.
		return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
	  }
	  return undefined; // Please Netbeans
	},

		
	str_replace : function (search, replace, subject, count) {
	  // http://kevin.vanzonneveld.net
	  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // +   improved by: Gabriel Paderni
	  // +   improved by: Philip Peterson
	  // +   improved by: Simon Willison (http://simonwillison.net)
	  // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	  // +   bugfixed by: Anton Ongson
	  // +      input by: Onno Marsman
	  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // +    tweaked by: Onno Marsman
	  // +      input by: Brett Zamir (http://brett-zamir.me)
	  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // +   input by: Oleg Eremeev
	  // +   improved by: Brett Zamir (http://brett-zamir.me)
	  // +   bugfixed by: Oleg Eremeev
	  // %          note 1: The count parameter must be passed as a string in order
	  // %          note 1:  to find a global variable in which the result will be given
	  // *     example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
	  // *     returns 1: 'Kevin.van.Zonneveld'
	  // *     example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
	  // *     returns 2: 'hemmo, mars'
	  var i = 0,
		j = 0,
		temp = '',
		repl = '',
		sl = 0,
		fl = 0,
		f = [].concat(search),
		r = [].concat(replace),
		s = subject,
		ra = Object.prototype.toString.call(r) === '[object Array]',
		sa = Object.prototype.toString.call(s) === '[object Array]';
	  s = [].concat(s);
	  if (count) {
		this.window[count] = 0;
	  }

	  for (i = 0, sl = s.length; i < sl; i++) {
		if (s[i] === '') {
		  continue;
		}
		for (j = 0, fl = f.length; j < fl; j++) {
		  temp = s[i] + '';
		  repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
		  s[i] = (temp).split(f[j]).join(repl);
		  if (count && s[i] !== temp) {
			this.window[count] += (temp.length - s[i].length) / f[j].length;
		  }
		}
	  }
	  return sa ? s : s[0];
	},
	
	
	implode : function (glue, pieces) {
	  //  discuss at: http://phpjs.org/functions/implode/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Waldo Malqui Silva
	  // improved by: Itsacon (http://www.itsacon.net/)
	  // bugfixed by: Brett Zamir (http://brett-zamir.me)
	  //   example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
	  //   returns 1: 'Kevin van Zonneveld'
	  //   example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
	  //   returns 2: 'Kevin van Zonneveld'

	  var i = '',
		retVal = '',
		tGlue = '';
	  if (arguments.length === 1) {
		pieces = glue;
		glue = '';
	  }
	  if (typeof pieces === 'object') {
		if (Object.prototype.toString.call(pieces) === '[object Array]') {
		  return pieces.join(glue);
		}
		for (i in pieces) {
		  retVal += tGlue + pieces[i];
		  tGlue = glue;
		}
		return retVal;
	  }
	  return pieces;
	},
	
	explode : function (delimiter, string, limit) {
	  //  discuss at: http://phpjs.org/functions/explode/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //   example 1: explode(' ', 'Kevin van Zonneveld');
	  //   returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}

	  if (arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined') return null;
	  if (delimiter === '' || delimiter === false || delimiter === null) return false;
	  if (typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string ===
		'object') {
		return {
		  0: ''
		};
	  }
	  if (delimiter === true) delimiter = '1';

	  // Here we go...
	  delimiter += '';
	  string += '';

	  var s = string.split(delimiter);

	  if (typeof limit === 'undefined') return s;

	  // Support for limit
	  if (limit === 0) limit = 1;

	  // Positive limit
	  if (limit > 0) {
		if (limit >= s.length) return s;
		return s.slice(0, limit - 1)
		  .concat([s.slice(limit - 1)
			.join(delimiter)
		  ]);
	  }

	  // Negative limit
	  if (-limit >= s.length) return [];

	  s.splice(s.length + limit);
	  return s;
	},

	
	
	
	stripos : function(f_haystack, f_needle, f_offset) {
		//  discuss at: http://phpjs.org/functions/stripos/
		//  original by: Martijn Wieringa
		//  revised by: Onno Marsman
		//   example 1: stripos('ABC', 'a');
		//   returns 1: 0

		var haystack = (f_haystack + '').toLowerCase();
		var needle = (f_needle + '').toLowerCase();
		var index = 0;

		if ((index = haystack.indexOf(needle, f_offset)) !== -1) {
			return index;
		}
		return false;
	},
	
	
	
	/***************
	  FUNCTIONS BY JANET CURRIE
	      ***************/
	get_human_html_codes : function() {
		var codes = new Array("b","i","u","li","ul","ol","p");
		return codes;
	},
	
	convert_to_html : function(str) {
		var codes = this.get_human_html_codes();
		str = this.decode(str);
		$.each(codes, function(index,c) {
			//str = AssistString.str_replace(c,index,str);
			str = AssistString.str_replace("["+c+"]","<"+c+">",str);
			str = AssistString.str_replace("[/"+c+"]","</"+c+">",str);
		});
		return str;
	},	
		  
	convert_from_html : function(str) {
		var codes = this.get_human_html_codes();
		str = this.decode(str);
		$.each(codes, function(index,c) {
			//str = AssistString.str_replace(c,index,str);
			str = AssistString.str_replace("<"+c+">","["+c+"]",str);
			str = AssistString.str_replace("</"+c+">","[/"+c+"]",str);
		});
		return str;
	}	
	
	
	
	
	
}