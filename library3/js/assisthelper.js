var AssistHelper = {
	isubmit : {"border":"1px solid #009900","color":"#009900"},
	dialogSaveCSS : {"border":"1px solid #009900","color":"#009900","position":"absolute","left":"10px"},
	idelete : {"border":"1px solid #CC0000","color":"#CC0000"},
	iclose	: {}, //"font-size":"75%","margin-left":"20px"
	idefault : {"border":"1px solid #D3D3D3","color":"#555555"},
	inote : {"border":"1px solid #fe9900","color":"#fe9900"},
	idisplayHidden : {"display":"none"},
	idisplayInline : {"display":"inline"},
	idisplayBlock : {"display":"block"},
	idisplayInherit : {"display":"inherit"},

	getDisplayCSS : function(val){
		switch(val){
			case "inline":
				return AssistHelper.idisplayInline;
				break;
				
			case "block":
				return AssistHelper.idisplayBlock;
				break;
				
			case "hidden":
				return AssistHelper.idisplayHidden;
				break;
			
			default:
				return AssistHelper.idisplayInherit;
				break;			
		}
	},
	
	getGreenCSS : function() {
		return AssistHelper.isubmit;
	},	

	getRedCSS : function() {
		return AssistHelper.idelete;
	},	
	
	getCloseCSS : function() {
		return AssistHelper.iclose;
	},	
	
	getDialogSaveCSS : function() {
		return AssistHelper.dialogSaveCSS;
	},
	
	getNoteCSS : function() {
		return AssistHelper.inote;
	},
	
	getDefaultCSS : function() {
		return AssistHelper.idefault;
	},
	
	processing : function() {
				$('<div />',{id:"dlg_msg",html:"<p class=center>Processing...</p><p class=center><img src='../../pics/ajax_loader_v2.gif' /></p>"})
				.dialog({modal:true,closeOnEscape:false});
				//$('.ui-dialog-titlebar').hide();
				AssistHelper.hideDialogTitlebar("id","dlg_msg");
	},
	closeProcessing : function() {
		$("#dlg_msg").dialog("destroy");
	},
	finishedProcessing : function(result,msg) {
		//AssistHelper.closeProcessing();
		//$('<div />',{id:"dlg_msg2",html:AssistHelper.getHTMLResult(result,msg,"")}).dialog({modal:true,buttons:[{ text: "Ok", click: function() { $(this).dialog("destroy"); 	} 	}]});
		$("#dlg_msg").html(AssistHelper.getHTMLResult(result,msg,"")).dialog("option","buttons",[{ text: "Ok", click: function() { $(this).dialog("destroy"); 	} 	}]);
		//$('.ui-dialog :button').focus();
	},
	
	hideDialogTitlebar : function(typ,name) {
		switch(typ.toLowerCase()) {
			case "id": $("#"+name).siblings("div.ui-dialog-titlebar").hide(); break;
			case "class": $("."+name).siblings("div.ui-dialog-titlebar").hide(); break;
			default:
				$(".ui-dialog-titlebar").hide();
		}
	},

	hideDialogCSS : function(typ,name) {
		switch(typ.toLowerCase()) {
			case "id": $("#"+name).css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"}); break;
			case "class": $("."+name).css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"}); break;
			default:
				$(".ui-dialog").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
		}
/*		if(id.length>0) {
			$("#"+id).css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
		} else if(clss.length>0) {
			$("."+clss).css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
		} else {
			$(".ui-dialog").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
		}*/
	},
	
	formatDialogButtons : function($obj,ref,newCSS) {
		$obj.dialog("widget").find(".ui-dialog-buttonpane button").eq(ref).css(newCSS);
	},
	hideDialogButtons : function($obj,ref){
		$obj.dialog("widget").find(".ui-dialog-buttonpane button").eq(ref).hide();
	},
	showDialogButtons : function($obj,ref){
		$obj.dialog("widget").find(".ui-dialog-buttonpane button").eq(ref).show();
	},
	focusDialogButtons : function($obj,ref) {
		$obj.dialog("widget").find(".ui-dialog-buttonpane button").eq(ref).focus();
	},
	blurDialogButtons : function($obj,ref) {
		$obj.dialog("widget").find(".ui-dialog-buttonpane button").eq(ref).blur();
	},
	getHTMLResult : function(format,str,icon) {
		var result = "";
			result+="<div class='ui-widget'>";
			if(format=="ok") {
				result+= "<div class='ui-state-ok ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-check' style='float: left; margin-right: .3em;'></span>";
			} else if(format=="info") {
				result+= "<div class='ui-state-info ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>";
			} else if(format=="warn" || format=="error") {
				result+= "<div class='ui-state-error ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>";
			} else {
				result+= "<div class='ui-state-"+format+" ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-"+icon+"' style='float: left; margin-right: .3em;'></span>";
			}
			result+= "<span id=spn_displayResultText>"+AssistString.stripslashes(str)+"</span></p></div></div>";
		return result;
	},

	getHTMLResultWithBorder : function(format,str,color) {
		switch(format) {
		case "ok": color = "#009900"; break;
		case "info": color = "#FE9900"; break;
		case "error": color = "#CC0000"; break;
		default: break;
		}
		var result = "";
			result+="<div class='ui-widget ui-corner-all' style='border: 1px solid "+color+"'>";
			if(format=="ok") {
				result+= "<div class='ui-state-ok ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-check' style='float: left; margin-right: .3em;'></span>";
			} else if(format=="info") {
				result+= "<div class='ui-state-info ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>";
			} else if(format=="warn" || format=="error" || format=="notice") {
				//if(format=="notice") {var icon = }
				result+= "<div class='ui-state-error ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>";
			} else {
				result+= "<div class='ui-state-"+format+" ui-corner-all' style='margin: 5px 0px 10px 0px; padding: 0 .3em;'><p><span class='ui-icon ui-icon-"+icon+"' style='float: left; margin-right: .3em;'></span>";
			}
			result+= "<span id=spn_displayResultText>"+AssistString.stripslashes(str)+"</span></p></div></div>";
		return result;
	},
	
	
	getWindowSize : function() {
		var viewportwidth;  
		var viewportheight;     
		// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight     
		if (typeof window.innerWidth != 'undefined')  {       
			viewportwidth = window.innerWidth;   
			viewportheight = window.innerHeight;  
		}    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)    
		else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)  {
			viewportwidth = document.documentElement.clientWidth;
			viewportheight = document.documentElement.clientHeight;  
		}     // older versions of IE     
		else {        
			viewportwidth = document.getElementsByTagName('body')[0].clientWidth;        
			viewportheight = document.getElementsByTagName('body')[0].clientHeight;  
		} 
		var ret = new Array();
		ret['width'] = viewportwidth;
		ret['height'] = viewportheight;
		return ret;
	},
	
	doAjax : function(url,dta) {
		var result;
		//alert("ajax");
		$.ajax({                                      
			url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
			success: function(d) { 
				//alert("d "+d[1]); 
				//console.log(d);
				result=d; 
				//return d;
			},
			error: function(d) { console.log(d); result=d; }
		});
		//alert("r "+result[1]);
		return result;
	},
	doAjaxAsync : function(url,dta) {
		var result;
		//alert("ajax");
		$.ajax({                                      
			url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: true,
			success: function(d) { 
				//alert("d "+d[1]); 
				//console.log(d);
				result=d; 
				//return d;
			},
			error: function(d) { console.log(d); result=d; }
		});
		//alert("r "+result[1]);
		return result;
	},	
	getDisplayIcon : function(icon,options) {
		switch(icon) {
			case "ok":
			case 1:
			case "1":
			case true:
				return "<span class=\"ui-icon ui-icon-check center\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); "+options+"\">&nbsp;</span>";
				break;
			case "error":
			case "0":
			case 0:
			case false:
				return "<span class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); "+options+"\">&nbsp;</span>";
				break;
			case "info":
				return "<span class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); "+options+"\">&nbsp;</span>";
				break;
		}
	},
	
	showHelp : function(help_text) {
		//$("#hint_box").hide("slide", {direction: "right"}, 20);
		$("#hint_box p").text(help_text);
		if(!$("#hint_box p").is("visible")){
			setTimeout(function(){
				$("#hint_box").show("slide", {direction: "right"}, 200);
			}, 600);
		}else{
				$("#hint_box").show();			
		}
		
	},
	hideHelp : function() {
		setTimeout(function(){
			$("#hint_box").hide("slide",{direction: "right"},100);
		}, 910);
		
	}
	
};