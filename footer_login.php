<?php
//require 'inc_session.php';
//$jean[] = "CAPS";

require_once "module/autoloader.php";
$me = new ASSIST();
$jean = $me->getJeanModules();
$tkname = $me->getUserName();

$site_code = isset($_SESSION['DISPLAY_INFO']['site_code']) ? $_SESSION['DISPLAY_INFO']['site_code'] : "actionassist";
$site_name = isset($_SESSION['DISPLAY_INFO']['ignite_name']) ? $_SESSION['DISPLAY_INFO']['ignite_name'] : "Action Assist";

/**
 * Modifications to accommodate / check adjustments to auto logout process as a result of auto saving while capturing PM6 scoresheets
 * AA-232 - JC - 16 March 2021
 */
if(isset($_SESSION['session_timeout']) && is_array($_SESSION['session_timeout'])) {
	$timeout = $_SESSION['session_timeout']['current'];
	//$timeout_setting = $_SESSION['session_timeout']['setting']*1000;
	$timeout_setting = $_SESSION['session_timeout']['setting'];
} else {
	$timeout = time();
	$timeout_setting = 3600; //1 hour * 60 minutes * 60 seconds
}

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Action4u.co.za</title>

	<base target="main">

	<link href="/library/jquery-ui-1.10.0/css/jquery-ui.css?1456142536" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery.min.js?1456142536"></script>
	<script type ="text/javascript" src="/library/jquery-ui-1.10.0/js/jquery-ui.min.js?1456142536"></script>

	<link rel="stylesheet" href="/assist.css?1456142536" type="text/css">
	<script type ="text/javascript" src="/assist.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assistform.js?1456142536"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js?1456142536"></script>
	<style>
		/* The snackbar - position it at the bottom and in the middle of the screen */
		#snackbarOnline{
			visibility: hidden; /* Hidden by default. Visible on click */
			min-width: 300px; /* Set a default minimum width */
			max-height: 9px;
			margin-left: -125px;  /*Divide value of min-width by 2 */
			/*background-color: #333;  Black background color */
			color: black;
			vertical-align: middle;
			height: 10px;
			background-color: #bfffbe; /* White text color */
			text-align: center; /* Centered text */
			border-radius: 7px; /* Rounded borders */
			padding: 10px; /* Padding */
			position: fixed; /* Sit on top of the screen */
			left: 50%; /* Center the snackbar */
			bottom: 0px; /* 30px from the bottom */
		}
		#snackbarOffline{
			visibility: hidden; /* Hidden by default. Visible on click */
			min-width: 550px; /* Set a default minimum width */
			max-height: 9px;
			margin-left: -125px;  /*Divide value of min-width by 2 */
			/*background-color: #333;  Black background color */
			color: black;
			vertical-align: middle;
			height: 10px;
			background-color: #e5dfbb; /* White text color */
			text-align: center; /* Centered text */
			border-radius: 7px; /* Rounded borders */
			padding: 10px; /* Padding */
			position: fixed; /* Sit on top of the screen */
			left: 40%; /* Center the snackbar */
			bottom: 0px; /* 30px from the bottom */
		}
		
		/* Show the snackbar when clicking on a button (class added with JavaScript) */
		#snackbarOffline.offline {
			visibility: visible; /* Show the snackbar */
			/* Add animation: Take 0.5 seconds to fade in and out the snackbar.
			However, delay the fade out process for 2.5 seconds */
			/*-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;*/
			animation: fadein 0.5s, fadeout 0.5s 2.5s;
		}
		#snackbarOnline.online {
			visibility: visible; /* Show the snackbar */
			/* Add animation: Take 0.5 seconds to fade in and out the snackbar.
			However, delay the fade out process for 2.5 seconds */
			/*-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;*/
			animation: fadein 0.5s, fadeout 0.5s 2.5s;
		}
		@keyframes fadein {
			from {bottom: 0; opacity: 0;}
			to {bottom: 5px; opacity: 1;}
		}
		
	</style>
	
</head>
<body topmargin=5 leftmargin=0 bottommargin=5 rightmargin=0>


<div id="snackbarOnline" class="ui-widget ui-state-ok ui-corner-all"><div style="width:100px;margin:0 auto;"><span class="ui-icon ui-icon-check" style="float: left;"></span><span id="spn_displayResultText">Back online.</span></div></div>
<div id="snackbarOffline" class="ui-widget ui-state-info ui-corner-all"><span class="ui-icon ui-icon-info" style="float: left;"></span><span id="spn_displayResultText">Your connection to Assist has been interrupted. Please check your internet connection.</span></div>



<script type=text/javascript>
	//INTERNET CONNECTION DETECTOR AA-543
	//30 January 2021 => Sondelani Dumalisile
	function hasNetwork(online){
		const  elementOffline = document.querySelector("#snackbarOffline");
		const  elementOnline = document.querySelector("#snackbarOnline");
		if(online){
			elementOffline.classList.remove("offline");
			elementOnline.classList.add("online");
			//elementOnline.innerText = "Online";
			// Hide message after 5s.
			setTimeout(() => { elementOnline.classList.remove("online"); }, 10000);
		}else{
			elementOnline.classList.remove("online");
			elementOffline.classList.add("offline");
			//elementOffline.innerText = "Offline";
			// Hide message after 5s.
			//setTimeout(() => { elementOffline.classList.remove("offline"); }, 10000);
		}
	}
	
	window.addEventListener("online", () => {
		// Show "Back online" message.
		hasNetwork(true);
	});
	window.addEventListener("offline", () => {
		// Show "Back online" message.
		hasNetwork(false);
	});
	
//CODE BELOW REPLACED CODE ABOVE ON 2014-08-31 => Coded by Duncan Cosser
function printPage() {
    var usr = navigator.userAgent;
    if(usr.search("Chrome") != -1)
    { //do this if user is in Chrome based browser
        window.parent.main.print();
    }
    else if(usr.search("Firefox") != -1)
    { //do this if user is in firefox
    	window.parent.main.print();
    }
    else
    { //otherwise do this
	   window.parent.main.focus();
	   window.print();
	}
}
//console.log('timout setting: <?php //echo $timeout_setting; ?>//');
//console.log('timeout: <?php //echo date("d F Y H:i:s",$timeout); ?>//');
/*
setTimeout(showTimeout,<?php //echo $timeout_setting; ?>);
*/
function showTimeout(rem) {
	$(function() {
		//console.log(rem);
		//window.open("timeout.php","TimeOut","width=300, height=300, toolbar=0, titlebar=1, scrollbars=0, menubar=0, status=0, location=0");
		var timeOutWin = window.open("timeout.php","TimeOut","width=300, height=300");
		timeOutWin.focus();
	});
}

var to = 0;
var timeout_check = setInterval(checkTimeout,60000);

function checkTimeout() {
	to+=60;
	//console.log(to);
	$(function() {
		var r = AssistHelper.doAjax("inc_timeout.php","action=check");
		//console.log(r);
		if(r[0]=="logout") {
			document.location.href = "logout.php?action=TIME";
			//alert("logout");
		} else if(r[0]=="valid") {
			var expire = r[2];
			// $("#spn_timeout_display").html("Auto Logout At: "+expire);
			var rem = r[1];
			if(rem < 45) {
				showTimeout(rem);
				clearInterval(timeout_check);
			} else if(rem < 90) {
				clearInterval(timeout_check);
				timeout_check = setInterval(checkTimeout,15000);
				//var t = (rem - 32)*1000;
				//setTimeout(showTimeout,t);
			}
		}
	});
}

$(function() {
	$("#spn_timeout").click(function() {
		top.main.$("<div />",{id:"dlg_timeout",html:""}).dialog({modal:true});
		top.main.$("#dlg_timeout").prepend($("<iframe />",{id:"ifr_timeout"}));
		top.main.$("#ifr_timeout").prop("src","timeout.php");
		top.main.$("#dlg_timeout").siblings("div.ui-dialog-titlebar").hide()
		window.parent.header.showOverlay();
	});

	//added on 2018-08-09 by JC to resolved problem of footer loading into main after clicking print_page link
	$("#a_print_img, #a_print").click(function(e) {
		e.preventDefault();
		printPage();
	})
});

</script>
<style type=text/css>
.bc { background-color: #000099; }
/*.noborder { border: 1px solid green; }*/
table td { vertical-align: middle; }
a, a:hover, a:visited { color: #ffffff; }
</style>

<table width="100%" cellpadding=0 cellspacing=0 class=noborder>
	<tr>
		<td class="bc noborder" height="25" style="text-align: left; color: #ffffff;">&nbsp;<a href=# id=a_print_img><img src=/pics/print.gif height=18 border=0 align=absmiddle></a> <a href=# style="color: #FFFFFF" id=a_print>Print page</a></td>
		<td class="bc noborder center i" height="25" style="color: #eeeeee; font-size: 6.5pt;">&nbsp;<?php
if(isset($_REQUEST['mr']) && in_array($_REQUEST['mr'],$jean)) {
	echo "This module is hosted on behalf of <a href=http://www.igniteconsult.co.za target=_blank>Ignite Advisory Services (Pty) Ltd</a>.";
} elseif($site_code!="actionassist") {
	echo "$site_name is hosted on the Action Assist platform owned by <a href=http://www.actionassist.co.za target=_blank>Action iT (Pty) Ltd</a>.";
} else {
	echo "Action Assist is owned by <a href=http://www.actionassist.co.za target=_blank>Action iT (Pty) Ltd</a>.";
} ?>&nbsp;</td>
		<td class="bc noborder" height="25" style="text-align: right; color: #ffffff;"><?php echo("You are currently logged in as: ".$tkname); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=logout.php target=main style="color: #ffffff;">Logout</a> <a href=logout.php target=main><img src=pics/close-icon.png style="border-width:0px;vertical-align:middle;" ></a>&nbsp;</td>
	</tr>
</table>
<!-- <span style="float: left;" class="i" id="spn_timeout_display">Auto Logout At: <?php echo date("d F Y H:i:s",$timeout+$timeout_setting); ?></span> -->
</body>

</html>
