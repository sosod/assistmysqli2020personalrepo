<?php
include("inc_ignite.php");
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<style type=text/css>
table th { background-color: #CC0001; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php if($cmpcode!="ign0001") { die("<p>You are not authorised to view this page.</p>"); } ?>
<h1>SDBIP 2010/2011 Upload Status</h1>
<table cellspacing="0" cellpadding="4">
<?php
$file = fopen("schedule.csv","r");
$data = array();
while(!feof($file)) {
	$tmpdata = fgetcsv($file);
	if(count($tmpdata)>1) { $data[] = $tmpdata;	}
	$tmpdata = array();
}
fclose($file);
?>
	<tr>
<?php 
	$row = $data[0];
	foreach($row as $r) {
		echo "<th>$r</th>";
	}
?>
	</tr>
<?php
for($d=1;$d<count($data);$d++) {
	echo "<tr>";
		$row = $data[$d];
		for($r=0;$r<count($row);$r++) {
			$t = $row[$r];
			switch($r) {
				case 0:
					echo "<td style=\"text-align:center;font-weight:bold;\">"; break;
				case 1:
					echo "<td>"; break;
				case 2:
					switch($t) {
						case "Delivered": echo "<td style=\"text-align:center;font-weight:bold;color:#FFFFFF;background-color:#006600\">"; break;
						case "Received": echo "<td style=\"text-align:center;font-weight:bold;color:#006600;\">"; break;
						default: echo "<td style=\"text-align:center;\">"; break;
					} break;
				default:
					if($r==(count($row)-1)) {
						echo "<td>";
					} else {
						echo "<td style=\"text-align:center;\">";
					}
					break;
			}
			echo "$t</td>";
		}
	echo "</tr>";
}
?>
</table>
</body>
</html>
