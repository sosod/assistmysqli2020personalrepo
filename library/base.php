<?php
abstract class Base{
	
	const DELETE 	= 2;
	const DEFAULTS  = 4;
	const ACTIVE 	= 1; 
	
	function __construct()
	{

		$db = new DBConnect();
	}
	

	abstract function save();
	abstract function edit();
	abstract function delete();
	abstract function fetch();
	abstract function fetchAll();
	
}