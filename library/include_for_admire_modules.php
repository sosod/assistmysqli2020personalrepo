<?php
/*require_once("/library/class/assist_helper.php");
require_once("/library/class/assist.php");
require_once("/library/class/assist_dbconn.php");
require_once("/library/class/assist_db.php");
require_once("/library/class/assist_root.php");
require_once("/library/class/assist_user.php");
require_once("/library/class/assist_module_helper.php");
*/

$generic_classes = array(
	"assist_helper",
	"assist",
	"assist_dbconn",
	"assist_db",
	"assist_root",
	"assist_user",
	"assist_module_helper",
	"assist_sms",
	"assist_email",
	"assist_email_summary",
	"assist_module_email",
	"assist_module_notification",
	"assist_module_user",
);

foreach($generic_classes as $filename) {
	getModuleClass($filename);
}


	function getModuleClass( $filename, $modloc="" ) {
		$found = false;
		
		$paths = array(
			"",
			"class/",
			"library/class/",
			"library/dbconnect/",
			"library/database/",
		);
		if(strlen($modloc)>0) {
			$paths[] = $modloc."/";
			$paths[] = $modloc."/class/";
			$paths[] = strtolower($modloc."/");
			$paths[] = strtolower($modloc."/class/");
		}
		
		foreach($paths as $p) { 
			if(!$found) {
				$f = $p.strtolower($filename).".php";
				for($i=0;$i<=3;$i++) {
					if($i>0) { $f = "../".$f; }       //echo "<p>".$f;
					if(file_exists($f)) {
						//echo " - file found";
						require_once($f);
						$found = true;
						$i=4;
					}
				}
			}
		}
		
	}



?>