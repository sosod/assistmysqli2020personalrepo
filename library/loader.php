<?php
/**
 * OUTDATED AUTOLOADER - If the module uses this file, please redirect it to module/autoloader.php - AA-488 JC 12 Oct 2020
 */
@ini_set(default_charset, "");
class Loader {

	public function autoload($class) {
		self::requireFileName( $class );
	}
	
	public static function requireFileName($filename) {
		if( file_exists( "../class/".strtolower($filename).".php" ) ){
			require_once( "../class/".strtolower($filename).".php" );
		} else if( file_exists( "../../".strtolower($filename).".php" ) ) {
			require_once("../../".strtolower($filename).".php");
		} else if(file_exists("../../library/dbconnect/".strtolower($filename).".php")) {
			require_once( "../../library/dbconnect/".strtolower($filename).".php" );		
		} else if(file_exists("../../library/database/".strtolower($filename).".php")) {
			require_once( "../../library/database/".strtolower($filename).".php" );		
		} else {
			require_once( "../../library/database/".strtolower($filename).".php" );		
		}
	}
	
}