<?php

/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/21/11
 * Time: 2:31 AM
 * To change this template use File | Settings | File Templates.
 */
class AdmireAuditlog extends DBConnect {

	protected $tableName;

	protected $postArr;

	protected $objectInContext;

	protected $setupId;

	protected $assist_helper;

	function __construct() {
		parent::__construct();
		$this->assist_helper = new ASSIST_MODULE_HELPER();
	}

	function setParameters($postArr, $context, $table) {
		$this->postArr = $postArr;
		$this->objectInContext = $context;
		$this->tableName = $table;
		$keyPost = key($_POST);
		$this->setupId = (isset($postArr['id']) ? isset($postArr['id']) : $_POST[$keyPost]);
		$this->saveLog();
	}

	function viewLogs($dataArr) {
		$table = substr($dataArr['table'], 6);
		$sql = "";
		if(isset($dataArr['field']) && !empty($dataArr['field'])) {
			$sql = " AND ".$dataArr['field']." = ".$dataArr['value'];
		}
		$this->tableName = $table;
		/**
		 * AA-559 Risk Assist - Cannot Edit Users under Setup
		 * Changes by Sondelani Dumalisile (09 March 2021)
		 * comment : Special character's decoding function switched from the Old DBconnect file to latest ASSIST_MODULE_HELPER
		 */
		/*$results = $this -> get("SELECT id, insertuser, insertdate, changes FROM #_".$table." WHERE 1 $sql
								 ORDER BY insertdate DESC
								");*/
		$hlp = new ASSIST_MODULE_HELPER();
		$results = $hlp->mysql_fetch_all("SELECT id, insertuser, insertdate, changes FROM ".$hlp->getDBRef()."_".$table." WHERE 1 $sql ORDER BY insertdate DESC");
		$changes = array();
		foreach($results as $key => $updateArr) {
			if($_SESSION['modlocation'] == "RGSTR" || $_SESSION['modlocation'] == "QUERY") {
				$x = unserialize(base64_decode($updateArr['changes']));
				switch($table) {
					case "useraccess_logs":
						//$changeArr1 = array();
						$x['user'] = $x['insertusername'];
						unset($x['insertusername']);
						unset($x['insertuser']);
						unset($x['action']);
						unset($x['user_record_id']);
						unset($x['datetime']);
						//$changeArr['user'] = $changeArr1['user'];
						break;
					case "actions_update":
						unset($x['timestamp']);
						break;
				}
				$updateArr['changes'] = base64_encode(serialize($x));
				$changeArr = $this->_extractChanges($updateArr);
			} else {
				$changeArr = $this->_extractChanges($updateArr);
			}
			//$changeArr['message'].="<br />modloc:".$_SESSION['modlocation'];
			$keyId = "";
			if(isset($updateArr['id'])) {
				$keyId = $updateArr['id'];
			} else if(isset($updateArr['setup_id'])) {
				$keyId = $updateArr['setup_id'];
			} else {
				$keyId = key($updateArr);
			}
			if(!empty($changeArr)) {
				$timestamped = strtotime($updateArr['insertdate']);
				$key = $keyId."".$updateArr['insertdate'];
				$date = date("d M Y H:i:s", $timestamped);
				$changes[$key] = array("ref" => $keyId,
					"date" => $date,
					"user" => $changeArr['user'],
					"changes" => $changeArr['message'],
					"status" => (isset($changeArr['status']) ? $changeArr['status'] : "")
				);
			}
		}
		return $changes;
	}

	function saveLog() {
		$changesData = $this->_processChanges();
		$insertdata = array();
		$insertdata['changes'] = serialize($changesData['changes']);
		$insertdata['setup_id'] = $this->setupId;
		$insertdata['insertuser'] = $_SESSION['tid'];
		$this->insert($this->tableName."_logs", $insertdata);
		return $this->insertedId();
	}

	function _processChanges() {
		$headers = new Naming();
		$headers->getHeaderNames();
		$changes = array();
		$changeMessage = $_SESSION['tkn']." has made the following changes \r\n\n";
		$changes['user'] = $changeMessage;
		foreach($this->objectInContext as $key => $value) {
			if(isset($this->postArr[$key]) || array_key_exists($key, $this->postArr)) {
				if($this->postArr[$key] != $value) {
					if($key == "status" || $key == "active") {
						$fromStatus = $this->_checkStatus($value);
						$toStatus = $this->_checkStatus($this->postArr[$key]);
						$changeMessage = $headers->setHeader($key)." changes to ".$toStatus." from ".$fromStatus."\r\n\n";
						$changes[$key] = array("from" => $fromStatus, "to" => $toStatus);
					} else {
						$changeMessage = $headers->setHeader($key)." changes to ".$this->postArr[$key]." from ".$value."\r\n\n";
						$changes[$key] = array("from" => $value, "to" => $this->postArr[$key]);
					}
				}
			}
		}
		return array("changes" => $changes, "message" => $changeMessage);
	}

	/* extract changes that have been saved in the audit log */
	function _extractChanges($updateArray) {
		$naming = new Naming();
		$user = "";
		$status = "";
		if($_SESSION['ref'] == "COMPL" && substr($this->tableName, 0, 6) == "action") {
			$naming->setType("action");
		}
		$naming->getHeaderNames();
		$changeMessage = "";
		if(isset($updateArray['changes']) && !empty($updateArray['changes'])) {
			$changesArr = @unserialize(base64_decode($updateArray['changes']));
			//get the user who made the change
			if(isset($changesArr['user'])) {
				$user = $changesArr['user'];
				//$changeMessage .= $changesArr['user']." <br /><br />";
				unset($changesArr['user']);
			}

			if(isset($changesArr['currentstatus'])) {
				$status = $changesArr['currentstatus'];
				unset($changesArr['currentstatus']);
			}
			//extract other changes
			if(isset($changesArr) && !empty($changesArr)) {
				$changeMessage = $this->_getChanges($changesArr, $naming);
			} else {
				$changeMessage = "";
			}
		}//AA-559 - code user so that special char's don't break ajax code. - 15 March 2021 - Sondelani Dumalisile - adjusted 16 March 2021 [JC]
		return array('user' => $this->assist_helper->code($user), 'status' => $status, 'message' => ($changeMessage));
	}

	function _getChanges($changesArr, $naming) {
		$changeMessage = "";
		foreach($changesArr as $index => $valArr) {
			//AA-559 - code all the values so that any special char's don't break ajax - 16 March 2021 [JC]
			if(is_array($valArr)) {
				foreach($valArr as $a => $aVal) {
					$valArr[$a] = $this->assist_helper->code($aVal);
				}
			} else {
				$valArr = $this->assist_helper->code($valArr);
			}
			if($index == "user") {
				continue;
			} else if($index == "attachment") {
				if(is_array($valArr)) {
					foreach($valArr as $a => $aVal) {
						$changeMessage .= $aVal."<br />";
					}
				} else {
					$changeMessage .= $valArr."<br />";
				}
			} else if($index == "response") {
				if(is_array($valArr)) {
					if(isset($valArr['to']) && isset($valArr['from'])) {
						$changeMessage .= $naming->setHeader($index)." was changed to <span class='change'><i>".$valArr['to']."</i></span> from <span><i>".$valArr['from']."</i></span><br />";
					} else {
						$changeMessage .= "<span class='change'><i>".$valArr."</i></span><br />";
					}
				} else {
					$changeMessage .= "Added response : <span class='change'><i>".$valArr."</i></span><br />";
				}
			} else {
				if(is_array($valArr)) {
					if(array_key_exists("to", $valArr) && array_key_exists("from", $valArr)) {
						$changeMessage .= $naming->setHeader($index)." was changed <b>to</b> <span class='change'><i>".$valArr['to']."</i></span> <b>from</b> <span class='change'><i>".$valArr['from']."</i></span><br />";
					} else {
						$changeMessage .= $this->_getChanges($valArr, $naming);
					}
				} else {
					$changeMessage .= "<span class='change'><i>".$valArr."</span></i><br />";
				}
			}
		}
		return $changeMessage;
	}


	function _checkStatus($value) {
		$status = "";
		if($value == 2) {
			$status = " deleted";
		} else if($value == 1) {
			$status = " activated";
		} else if($value == 0) {
			$status = " deactivated";
		}
		return $status;
	}

}
