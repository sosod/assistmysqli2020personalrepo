<?php
/**
 * OUTDATED AUTOLOADER - If the module uses this file, please redirect it to module/autoloader.php - AA-488 JC 12 Oct 2020
 */
@ini_set(default_charset, "");
class Loader {

	public static function autoload($class) {
 //echo "<P>".$class; 
		$c = explode("_",$class);
		switch(strtoupper($c[0])) {
			//$path = "../library/class/";
			//self::getAssistClass($class,$path);
			//break;
		case "MODULE":
			$path = "".strtoupper($c[1])."/";
			self::getAssistClass($class,$path);
			break;
		case "ASSIST":
		default:
			$path = isset($_SESSION['modlocation']) ? $_SESSION['modlocation'] : "";
			self::getModuleClass( $path, $class );
		}
	}
	
	public static function getModuleClass( $modloc, $filename ) {
		$found = false;
		
		$paths = array(
			"",
			"class/",
			"library/class/",
			"library/dbconnect/",
			"library/database/",
		);
		if(strlen($modloc)>0) {
			$paths[] = $modloc."/";
			$paths[] = $modloc."/class/";
			$paths[] = strtolower($modloc."/");
			$paths[] = strtolower($modloc."/class/");
		}
		$e = explode("_",$filename);
		if(isset($e[0])){
			$paths[] = strtoupper($e[0])."/class/";
			$paths[] = strtolower($e[0]."/class/");
			$paths[] = strtoupper($e[0])."/";
			$paths[] = strtolower($e[0]."/");
		}		
		foreach($paths as $p) {
			if(!$found) {
				$f = $p.strtolower($filename).".php";
//if($_SERVER['REMOTE_ADDR']=="105.233.76.67") { echo "<P>".$f."</p>"; }
				for($i=0;$i<=3;$i++) {
					if($i > 0) {
						$f = "../".$f;
					}
					if(file_exists($f)) {
						require_once($f);
						$found = true;
						$i=4;
					}
				}
			}
		}
		
	}
	
	public static function getAssistClass($class,$path) {	
//		echo "<P>::".$path.$class.".php::</p>";
		if(file_exists($path.strtolower($class).".php")) { 
			require_once($path.strtolower($class).".php");
		}
	}
	
}

@session_start();
spl_autoload_register("Loader::autoload");
?>