<?php
/* Base Database Connection class for connecting to Ignite Assist
Created by: Janet Currie (c)
Created on: 20 August 2012
*/


class ASSIST_DBCONN_REMOTE { //extends ASSIST_HELPER {

/* VARIABLES */

	/** these settings must be updated in the __construct() function 
	depending on the site that the class is running on **/
	//db name settings
	protected $db_prefix;
	protected $db_other;
	//login settings
	private $db_user;
	private $db_pwd;
	private $db_host;
	
	//database names
	private $db_client;		//this is the default database name based on the client currently logged in
	private $db_name;		//this is the actual database name used to connect to the mysql server => can be changed through setDBName($dbn)
	private $db_master;		//this is the name of the database with the assist_company and other master tables in it
	private $db_help;		//this is the name of the Ignite Help database
	private $db_helpdesk;		//name of new helpdesk database
	private $db_cassist;	//this is the name of the CASSIST database
	private $db_blank;		//this is the name of the blank database used to create new client dbs/modules
	private $db_type;		//this indicates whether the connection is to a client database or the master, help or cassist databases

	//error settings
	private $db_error_to;	
	private $db_error_from;
	private $db_error_cc;
		/* This is a constant to identify the site/computer that generated an error msg:
				JC_* => A development computer belonging Janet Currie
				AZ_* => A development computer belonging to Admire Zinamo
				LIVE => assist.ignite4u.co.za
				DEMO => demo.ignite4u.co.za
				DEV => dev.ignite4u.co.za
		*/
	private $site_src;
	
	//resources
	protected $db_resource;
	protected $db_ref;
	
	private $cmpcode = "";

/* MAIN FUNCTIONS */
	public function __construct($server="ASSIST02",$dbn="",$cc="") {
		//server specific values
		$this->site_src = $server;
		switch($server) {
			case "ASSIST01":
				$this->db_host = "197.97.241.115";
				$this->db_prefix = "ignittps";
				$this->db_error_from = "no-reply@assist.action4u.co.za";
				break;
			case "ASSIST02":
				$this->db_prefix = "pwcza";
				$this->db_host = "197.97.241.116";
				$this->db_error_from = "no-reply@pwc.assist.action4u.co.za";
				break;
			case "FINTEG":
				$this->db_prefix = "finteg";
				$this->db_host = "197.97.241.125";
				$this->db_error_from = "no-reply@integration.assist.action4u.co.za";
				break;
			case "DEV01":
			case "TEST":
				$this->db_prefix = "testdev";
				$this->db_host = "197.97.241.126";
				$this->db_error_from = "no-reply@action4udev.co.za";
				break;
			case "FINTEG_TEST":
				$this->db_prefix = "finteg";
				$this->db_host = "197.97.241.126";
				$this->db_error_from = "no-reply@action4udev.co.za";
				break;
			case "LOCAL":
			case "LOCAL_FINTEG":
				$this->db_prefix = "finteg";
				$this->db_host = "localhost";
				$this->db_error_from = "no-reply@action4udev.co.za";
				break;
			default:
				die("<h1>I don't know which server you are looking for!</h1>");
				break;
		}
		//Site-dependant values
		$cc = (strlen($cc)==0 && isset($_SESSION['cc']) ? strtolower($_SESSION['cc']) : strtolower($cc));
		$this->db_other = $this->db_prefix."_i";
		$this->db_user = "ait_remote";
		$this->db_pwd = "@cT10n1T4uA1T";
		//DB Names
		$this->db_master = $this->db_prefix."_ignite4u";
		$this->db_help = $this->db_prefix."_help";
		$this->db_cassist = $this->db_other."cassist";
		$this->db_blank = $this->db_other."blank";
		//Email addresses
		$this->db_error_to = "actionit.actionassist@gmail.com";
		$this->db_error_cc = "";
		//Connect
		$this->db_type = strtolower($dbn);
		switch(strtolower($dbn)) {
			case "master":	$dbn = $this->getMasterName();		break;
			case "cassist":	$dbn = $this->getCASSISTName();		break;
			case "help":	$dbn = $this->getHelpName();		break;
			case "other":	$dbn = $cc; break;
			case "client":
			default:
				$this->db_type = "client";
				$dbn = $this->db_other.$cc;
				$this->db_client = strtolower($dbn);
				$this->setDBRef();
				break;
		}
		$dbn = strtolower($dbn);
		$this->setDBName($dbn);
//		$this->db_connect();
		if(strlen($cc)>0) {
			$this->cmpcode = strtolower($cc);
		}
	}


    public function get_db_resource(){
        return $this->db_resource;
    }

	public function db_connect() {
		$this->db_close();
		
		try{
			if(!$this->db_resource = mysql_connect( $this->db_host, $this->db_user, $this->db_pwd, TRUE)) {
					throw new Exception(mysql_error());
			}
			if(!mysql_select_db($this->db_name, $this->db_resource)) {
					throw new Exception(mysql_error());
			}
			
		} catch ( Exception $e ){
			$this->sendError(mysql_error(),"CONN");
		}
	}
	public function db_close() {
		if(is_resource($this->db_resource)) {
			mysql_close($this->db_resource);
		}
	}
	
	
	
/* Functions to set variables */
	//Change the database name if needed => send blank to revert back to default client db name
	public function setDBName($dbn = "") {
		$this->db_name = strlen($dbn)==0 ? $this->db_client : $dbn;
		$this->db_connect();
	}
	//Set an additional email recipient to receive the error emails
	public function setEmailCC($em) {
		$this->db_error_cc = $em;
	}
	//Change the DBRef => send blank to revert to the default
	public function setDBRef($mr="",$cc="") {
		$cc = strlen($cc)==0 && isset($_SESSION['cc']) ? $_SESSION['cc'] : $cc;
		$cc = strlen($cc)==0 ? $this->cmpcode : $cc;
		$mr = strlen($mr)==0 && isset($_SESSION['modref']) ? $_SESSION['modref'] : $mr;
		$this->db_ref = strtolower("assist_".$cc."_".$mr);
		if(strtolower($cc)!=$this->cmpcode) {
			$dbn = $this->db_other.strtolower($cc);
			$this->setDBName($dbn);
			$this->db_connect();
			if(strlen($cc)>0) {
				$this->cmpcode = strtolower($cc);
			}
		}
	}
	
	
/* Functions to get variables */
	//Get database username
	public function getDBUser() { return $this->db_user; }
	//Get database user password
	public function getDBPwd() { return $this->db_pwd; }
	//Get DB Prefix - needed for admin functionality
	public function getRawDBPrefix() { return $this->db_prefix; }

	//Get the current database name
	public function getDBName() {
		return $this->db_name;
	}
	//Create a different database bame
	public function getOtherDBName($cc) {
		$odb = $this->db_other;
		$odb.=strtolower($cc);
		return $odb;
	}
	//Get the DBRef 'assist_cmpcode_modref'
	public function getDBRef() {
		return $this->db_ref;
	}
	//Get the database name for the CASSIST database
	public function getCASSISTName() {
		return $this->db_cassist;
	}
	//Get the database name for the Master/Main database
	public function getMasterName() {
		return $this->db_master;
	}
	//Get the database name for the Assist Help database
	public function getHelpName() {
		return $this->db_help;
	}
	//Get the database name for the Assist Helpdesk database - 16 September 2016 [TM]
	public function getHelpdeskName() {
		return $this->db_helpdesk;
	}

	//return the db connection resource
	public function getConn() {
//		$this->db_connect();
		return $this->db_resource;
	}
	public function getDBPrefix() {
		return $this->db_other;
	}
	public function getCmpCode() {
		return $this->cmpcode;
	}
	public function getModRef() {
		return strtoupper($_SESSION['modref']);
	}
	
	public function isClientDatabase() {
		return ($this->db_type == "client");
	}
	
	public function getUserID() { return $_SESSION['tid']; }
	
	public function isSupportUser() {
		if($_SESSION['tku'] == "support") 
			return true;
		else
			return false;
	}	




	/****
	 * generic table names
	 */
	public function getUserTableName() {
		return "assist_".strtolower($this->getCmpCode())."_timekeep";
	}


	protected function arrayToString($a,$i) {
		$s = array();
		$i++;
		foreach($a as $k=>$v) {
			if(is_array($v)) {
				$v = $this->arrayToString($v, $i);
			}
			$s[] = $k."=".$v."";
		}
		return "<Br />{".implode("; ",$s)."}";
	}

	
	//If there is an error -> send an email with the details
	//sendError(mysql_error(),$sql);
	protected function sendError($err,$sql) {
		$sqlerror = "<p>&nbsp;</p>
		<p><b>---ERROR DETAILS BEGIN---</b></p>
		<ul><li>Database error: ".$err."</li>
		<li>".$sql."</li>
		<li>".$this->db_name."</li>
		<li>Page: ".$_SERVER['PHP_SELF']."</li><li>Name: ".$_SERVER['SERVER_NAME']."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
		$sqlerror.="<p>--- SESSION DETAILS ---";
		$sqlerror.="<p>".serialize($_SESSION);
		/*foreach($_SESSION as $s => $v) {
			if(!is_array($v)) {
				$sqlerror.="<br />".$s." :: ".$v;
			} else {
				$sqlerror.="<br />".$s." :: ".implode("-",$v);
			}
		}*/
		$sqlerror.="<p>--- REQUEST DETAILS ---";
		foreach($_REQUEST as $r => $v) {
			$sqlerror.="<br />".$r." :: ".$v;
		}
		$sqlerror.="<p>".serialize($_REQUEST);
		$sqlerror.="<p>--- OTHER DETAILS ---";
			$sqlerror.="<br />HTTP USER AGENT :: ".$_SERVER['HTTP_USER_AGENT'];
		
		foreach($_SERVER['argv'] as $r => $v) {
			$sqlerror.="<br />".$r." :: ".$v;
		}
		$sqlerror.="<p>".serialize($_SERVER);
		$sqlerror.="<br />--- END ---</p>";
//		mail($this->db_error_to,"DB ".self::SITE_SRC." Error",$sqlerror,"From: ".$this->db_error_from.(strlen($this->db_error_cc)>0 ? "\r\nCC: ".$this->db_error_cc : "")."\r\nContent-type: text/html; charset=us-ascii");
		$src_path = explode("/",$_SERVER["REQUEST_URI"]);
		$path = ""; 
		if(count($src_path)<=2) { 
		} else { 
			$c = count($src_path)-2;
			for($i=1;$i<=$c;$i++) { $path.="../"; }
		}
		$path .= "logs/".date("Ymd_His")."_librarysql_error_log.html";
	        $file = fopen($path,"w");
       	 fwrite($file,$sqlerror."\n");
	        fclose($file);
//if($this->cc=="ait555d") { 
//echo $sqlerror; 
//echo "<pre>"; print_r($_SESSION); echo "</pre>";
//}
		die("<h1>Error</h1><p>Unfortunately Assist has encountered a fatal error and cannot continue.<br />An email has been sent to Action iT (Pty) Ltd notifying them of this error.</p>".$sqlerror);

	}



	function __destruct() {
		$this->db_close();
	}

}  //END assistDB class


?>
