<?php

class ASSIST_DB_REMOTE extends ASSIST_DBCONN_REMOTE {

	private $rs;
	private $oddchars = array ("‘", "’","–");
	private $replacechars = array ("&#039;", "&#039;","-");
 


//public: 
	public function __construct($dbn="",$cc="") {
		parent::__construct($dbn,$cc);
//		echo "db created";
	}

	//return the recordset resource
	public function db_query($sql) {
		//echo "<br>db_query function";
		$this->db_connect();
		//echo "<br>connected";
		$this->rs = mysql_query($sql,$this->getConn());
		//echo "<br>sql run";
//$this->sendError("test",$sql);
		if(!$this->rs) {
			//echo "<br>error!</p>";
			$this->sendError(mysql_error(),$sql);	
		}
		return $this->rs;
	}

	//run the insert sql and return the last insert_id()
	public function db_insert($sql) {
//		$sql = str_replace($this->oddchars,$this->replacechars,$sql);
		$sql = $this->convert_smart_quotes($sql);
		$rs = $this->db_query($sql);
		$mid = mysql_insert_id($this->getConn());
		$this->db_close();
		return $mid; //mysql_insert_id($this->getConn());
	}
	
	//run the update sql and return the number of rows affected
	public function db_update($sql) {
		//$sql = str_replace($this->oddchars,$this->replacechars,$sql);
		$sql = $this->convert_smart_quotes($sql);
		$rs = $this->db_query($sql);
		$mar = mysql_affected_rows($this->getConn());
		$this->db_close();
		return $mar; //mysql_affected_rows($this->getConn());
	}

	private function convert_smart_quotes($string) { 
		$string = $this->strip_bullets($string);
		$search = array(
			chr(145), 
			chr(146), 
			chr(147), 
			chr(148), 
			chr(151)
		);
		
		$replace = array(
			"&#039;", 
			"&#039;", 
			"&quot;", 
			"&quot;", 
			"-"
		);
		
		return str_replace($search, $replace, $string); 
	}

	private function strip_bullets($str) {
		$str = urlencode($str);
		$str = str_replace("%95","&bull;",$str);
		$str = str_replace("%09"," ",$str);
		$str = urldecode($str);
		return $str;
	}

	//return the number of rows
	public function db_get_num_rows($sql) {
		$rs = $this->db_query($sql);
		$mnr = mysql_num_rows($rs);
		$this->db_close();
		return $mnr; //mysql_num_rows($rs);
	}
	
	//return all rows in an array
	public function mysql_fetch_all($sql) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while(($resultArray[] = mysql_fetch_assoc($result)) || array_pop($resultArray));
		$this->db_close();
		return $resultArray;
	}
	
	//return all rows in an array using a given fld as the key
	public function mysql_fetch_all_fld($sql,$fld) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while($row = mysql_fetch_assoc($result)) {
			$resultArray[$row[$fld]] = $row;
		}
		$this->db_close();
		return $resultArray;
	}
	//return specific field in an array using a given fld as the key
	public function mysql_fetch_array_fld($sql,$fld,$fld2,$keepKey=false) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while($row = mysql_fetch_assoc($result)) {
			if($keepKey) {
				$resultArray[$row[$fld]][$row[$fld2]] = $row[$fld2];
			} else {
				$resultArray[$row[$fld]][] = $row[$fld2];
			}
		}
		$this->db_close();
		return $resultArray;
	}

	//return all rows in a 2D array using given flds as the keys
	public function mysql_fetch_all_fld2($sql,$fld1,$fld2) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while($row = mysql_fetch_assoc($result)) {
			$resultArray[$row[$fld1]][$row[$fld2]] = $row;
		}
		$this->db_close();
		return $resultArray;
	}

	//return only the 1st row
	public function mysql_fetch_one($sql) { //echo "assist_db mysqlfetch1: ".$sql;
		$result = $this->db_query($sql);
		$row = mysql_fetch_assoc($result);
		//echo "<pre>"; print_r($row); echo "</pre>";
		$this->db_close();
		//if(!is_array($row)) {
		//	return array();
		//} else {
			return $row;
		//}
	}

	//return the value of 1 fld in the first row
	public function mysql_fetch_one_fld($sql,$fld) {
		$result = $this->db_query($sql);
		$row = mysql_fetch_assoc($result);
		$value = $row[$fld];
		$this->db_close();
		return $value;
	}

	//return an array of all values held in given fld
	public function mysql_fetch_fld_one($sql,$fld) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while($row = mysql_fetch_assoc($result)) {
			$resultArray[] = $row[$fld];
		}
		$this->db_close();
		return $resultArray;
	}

	//return an array of the values held in a given fld using another fld as the key
	public function mysql_fetch_fld2_one($sql,$arrfld,$rowfld) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while($row = mysql_fetch_assoc($result)) {
			$resultArray[$row[$arrfld]] = $row[$rowfld];
		}
		$this->db_close();
		return $resultArray;
	}

	
	
	/** verify that a module exists on a client database **/
	function checkModuleExists($modref,$mod_table) {
		$module_exists = $this->isClientDatabase();
		if($module_exists===true) {
			$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".strtoupper($modref)."' AND (modyn = 'Y' OR modadminyn = 'Y')"; //echo $sql;
			$menu = $this->mysql_fetch_all($sql);
			if(!(count($menu)>0)) { $module_exists = false; }
			if(strlen($mod_table)>0) {
				$sql = "SHOW TABLES LIKE 'assist_".$this->getCmpCode()."_".strtolower($modref)."_".$mod_table."'";
				$tables = $this->mysql_fetch_all($sql);
				if(!(count($tables)>0)) { $module_exists = false; }
			}
		} //echo "::".$module_exists."::<br />";
		return $module_exists;
	}

	
	
	
	
    /**
     * Returns an array of a specific field as the element with another field as the key 
     * @param (SQL) sql = The SQL statement to run
     * @param (String) arrFld = The field name of the element that must be used for the key
     * @param (String) rowFld = The field name of the element that must be used for the value
     * @return (Array) row[arrFld] = rowFld
     */
	public function mysql_fetch_value_by_id($sql,$arrfld,$rowfld) {
		return $this->mysql_fetch_fld2_one($sql,$arrfld,$rowfld);
	}
    /**
     * Returns a single value from the first row
     * @param (SQL) sql = The SQL statement to run
     * @param (String) fld = The field name of the element that must be returned
     * @return (String)
     */
	public function mysql_fetch_one_value($sql,$fld) {
		return $this->mysql_fetch_one_fld($sql,$fld);
	}
    /**
     * Returns an array of records with the given $fld as the key
     * @param (SQL) sql = The SQL statement to run
     * @param (String) fld = The field name to use as the array key
     * @return (Array)
     */
	public function mysql_fetch_all_by_id($sql,$fld) {
		return $this->mysql_fetch_all_fld($sql,$fld);
	}
    /**
     * Returns an array of records with the given $fld as the key
     * @param (SQL) sql = The SQL statement to run
     * @param (String) arrFld = The field name to use as the array key
     * @param (String) valueFld = The field name to use as the array element
	 * @param (Bool) maintainKeyAssociation = use the valueFld as the array key within the arrFld array True/False (default = false)
     * @return (Array)
     */
	public function mysql_fetch_array_by_id($sql,$arrFld,$valueFld,$maintainKeyAssociation=false) {
		return $this->mysql_fetch_array_fld($sql,$arrFld,$valueFld,$maintainKeyAssociation);
	}
    /**
     * Returns a 2D array of records with the given $fld as the first level array key and the $fld2 as the key for the second level array
     * @param (SQL) sql = The SQL statement to run
     * @param (String) fld = The field name to use as the first-level array key
     * @param (String) fld = The field name to use as the second-level array key
     * @return (Array) $data[$fld][$fld2] = $row
     */
	public function mysql_fetch_all_by_id2($sql,$fld,$fld2) {
		return $this->mysql_fetch_all_fld2($sql,$fld,$fld2);
	}
    /**
     * Returns a 3D array of records with the given $fld as the first level array key and the $fld2 as the key for the second level array and $fld3 as the key for the third level
     * @param (SQL) sql = The SQL statement to run
     * @param (String) fld = The field name to use as the first-level array key
     * @param (String) fld = The field name to use as the second-level array key
     * @param (String) fld = The field name to use as the third-level array key
     * @return (Array) $data[$fld][$fld2] = $row
     */
	public function mysql_fetch_all_by_id3($sql,$fld,$fld2,$fld3) {
		$result = $this->db_query($sql);
		$resultArray = array();
		while($row = mysql_fetch_assoc($result)) {
			$resultArray[$row[$fld1]][$row[$fld2]][$row[$fld3]] = $row;
		}
		$this->db_close();
		return $resultArray;
	}

	public function mysql_fetch_all_by_value($sql,$fld) {
		return $this->mysql_fetch_fld_one($sql,$fld);
	}

	
	function __destruct() {
		parent::__destruct();
	}

}  //END assistDB class


?>