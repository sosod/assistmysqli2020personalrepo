<?php


class ASSIST_MODULE_USER extends ASSIST_MODULE_HELPER {


	private $table_name = "timekeep TK";
	private $id_field = "TK.tkid";
	private $name_field = "CONCAT(TK.tkname,' ',TK.tksurname)";

	public function __construct() {
		parent::__construct();
		$this->table_name = "assist_".$this->getCmpCode()."_".$this->table_name;
		
	}


	public function getTableName() { return $this->table_name; }



	/**
	 * Function to generate a user list for use in a specific module's report generator
	 */
	public function getItemsForReport($table,$fld,$options="") {
		$sql = "SELECT DISTINCT ".$this->id_field." as id, ".$this->name_field." as name
				FROM ".$this->getTableName()."
				INNER JOIN ".$table." O
				  ON O.".$fld." = ".$this->id_field."
				".(strlen($options)>0 ? "WHERE ".$options : "")."
				ORDER BY ".$this->name_field." 
				";
		return $this->mysql_fetch_value_by_id($sql,"id","name");
	}
	


	public function __destruct() {
		parent::__destruct();
	}


}



?>