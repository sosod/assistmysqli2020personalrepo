<?php

class ASSIST_REPORT_HISTORY extends ASSIST_REPORT {


	public function __construct() {
		parent::__construct();
	}

	public function drawTable($reports) {
		//ASSIST_HELPER::arrPrint($reports);
		//if(count($reports)==0) {
			//echo "<P>There is no Report History to display.</p>";
		//} else {
			echo "
			<table class=list id=historyreports>
				<tr>
					<th>Date</th>
					<th>File Name</th>
					<th>Type</th>
					<th>Status</th>
				</tr>";
			foreach($reports as $key => $r) {
				if($r['status']==2 || file_exists("../files/".strtolower($this->getCmpCode())."/".$r['system_name'])) {
					$path = "../files/".strtolower($this->getCmpCode())."/".$r['system_name'];
					$btn_text = "Download";
					$btn_class = "btn_down";
					if($r['status']==1 && $r['rep_type']!="HTML") {
						$file = strlen($r['download_name'])>0 ? $r['download_name'] : $r['system_name'];
					} else {
						$file = "N/A";
					}
					$format = $r['rep_type'];
					switch($r['rep_type']) {
						case "CSV":
							$type = "Microsoft Excel<br />(Plain text)";
							$p = explode("/",$path);
							$path = end($p);
							break;
						case "XLS":
							$type = "Microsoft Excel<br />(Formatted)";
							$p = explode("/",$path);
							$path = end($p);
							break;
						case "HTML":
						default:
							$btn_text = "Open";
							$btn_class = "btn_open";
							$type = "Onscreen";
							//$file = "N/A";
							$format = "";
							break;
					}
					if($r['status']==1) {
						$button = "<button id=$key class='btn_gen $btn_class ' link='$path' format='$format' usr='$file' >$btn_text</button>";
					} else {
						if(strtotime($r['moddate'])<(time()-(3600*6))) {
							$button = $this->getDisplayIconAsDiv("error")." <span >Failed</span>";
						} else {
							$button = $this->getDisplayIconAsDiv("info")." <span >Processing</span>";
						}
					}
					echo "
					<tr>
						<td class=center>".date("d M Y H:i",strtotime($r['moddate']))."</td>
						<td>".$file."</td>
						<td class=center>".$type."</td>
						<td class=center>".$button."</td>
					</tr>";
				}
			}
			echo "
			</table>
			<script type=text/javascript>
				$(function() {
					if(($('#historyreports').find('tr').length)==1) {
						$('#historyreports').append($('<tr />',{html:'<td colspan=4>There is no Report History to display.</td>'}));
					}
					$('#historyreports button.btn_down').button({
						icons: {
							primary: 'ui-icon-disk'
						}
					}).click(function(e) {
						e.preventDefault();
						var link = $(this).attr('link');
						var format = $(this).attr('format');
						var usr = $(this).attr('usr');
						//document.location.href = 'report.php?page='+pg+'&class='+sc+'&quick_id='+q+'&quick_act='+v;
						//alert(q);
						document.location.href = 'report_download.php?usr='+usr+'&sys='+link+'&f='+format;
					});
					$('#historyreports button.btn_open').button({
						icons: {
							primary: 'ui-icon-folder-open'
						}
					}).click(function(e) {
						e.preventDefault();
						var q = $(this).attr('link');
						document.location.href = q;
						//document.location.href = 'report.php?page='+pg+'&class='+sc+'&quick_id='+q+'&quick_act='+v;
						//alert(q);
					});
					$('#historyreports button.btn_gen').css({'background':'url()'})
					.children('.ui-button-text').css({'padding-top':'3px','padding-bottom':'3px','padding-left':'25px','font-size':'90%'});;
				});
			</script>";
		//}
	}
	

}


?>