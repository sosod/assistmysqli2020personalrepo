<?php

class ASSIST_ROOT {

	private $activity_names = array();
	private $object_names = array();

//DEFAULT ACTIVITY NAMES TEMP REMOVED for testing purposes
	protected $default_activity_names = array();/*
		'view'=>"View",
		'update'=>"Update",
		'updated'=>"Updated",
		'edit'=>"Edit",
		'edited'=>"Edited",
		'save'=>"Save",
		'approve'=>"Approve",
		'approved'=>"Approved",
		'approval'=>"Approval",
		'assess'=>"Assess",
		'add'=>"Add",
		'created'=>"Created",
		'confirm'=>"Confirm",
		'confirmed'=>"Confirmed",
		'unconfirm'=>"Undo Confirmation",
		'activate'=>"Activate",
		'activated'=>"Activated",
		'activation'=>"Activation",
		'deactivate'=>"Deactivate",
		'deactivated'=>"Deactivated",
		'deactivation'=>"Deactivation",
		'unlock'=>"Unlock",
		'unlocked'=>"Unlocked",
		'open'=>"Open",
		'decline'=>"Decline",
		'declined'=>"Declined",
		'restore'=>"Restore",
		'delete'=>"Delete",
		'download'=>"Download",
		'upload'=>"Upload",
		'import'=>"Import",
		'export'=>"Export",
		'reset'=>"Reset",
		'review'=>"Review",
		'copy'=>"Copy",
		'continue'=>"Continue",
		'build'=>"Build",
	);*/

	protected $default_object_names = array();




	/**
	 * STATUS CONSTANTS
	 */
	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;

	const CONFIRMED = 32;
	const ACTIVATED = 64;












	public function __construct($an = array(),$on = array()) {
		//if(count($an)==0) {
		//	$this->activity_names =
		//}
		$this->activity_names = $an;
		$this->object_names = $on;
	}





















	/****************************************
	 * General functions
	 */
	public function getModRef() {
		return strtoupper($_SESSION['modref']);
	}













	/**************************************
	 * Set functions
	 */
	public function setDefaultObjectNames($on) {
		$this->default_object_names = $on;
	}
	public function setObjectNames($names) {
		//set the object_names array to the given list
		$this->object_names = $names;
		//check for missing defaults and set those to default if necessary
		foreach($this->default_object_names as $key => $default) {
			if(!isset($this->object_names[$key])) {
				$this->object_names[$key] = $default;
			}
		}
	}
	public function setDefaultActivityNames($on) {
		$this->default_activity_names = $on;
	}
	public function setActivityNames($names) {
		//set the activity_names array to the given list
		$this->activity_names = $names;
		//check for missing defaults and set those to default if necessary
		foreach($this->default_activity_names as $key => $default) {
			if(!isset($this->activity_names[$key])) {
				$this->activity_names[$key] = $default;
			}
		}
	}

//menu names are included in object_names
	public function setMenuNames($names) {
		$names = $this->replaceAllNames($names);
		foreach($names as $key => $name) {
			//Don't overwrite any existing name that might come from the Objects - Object names take preference over Manu names
			if(!isset($this->object_names[$key])) {
				$this->object_names[$key] = $name;
			}
		}
	}

//additional object names are included in object_names
//this function is for names that might come from external sources
	public function setAdditionalObjectNames($names) {
		$names = $this->replaceAllNames($names);
		foreach($names as $key => $name) {
			//Don't overwrite any existing name that might come from the Objects - Object names take preference over Manu names
			if(!isset($this->object_names[$key])) {
				$this->object_names[$key] = $name;
			}
		}
	}


















	/**************************************
	 * Get functions
	 */
	public function getActivityName($a) {
		if(isset($this->activity_names[strtolower($a)])) {
			return $this->activity_names[strtolower($a)];
		} elseif(isset($this->default_activity_names[strtolower($a)])) {
			return $this->default_activity_names[strtolower($a)];
		} else {
			return ucwords($a);
		}
    }
	public function getAllActivityNames() {
		//$a = array();
		//foreach($this->default_activity_names as $key => $dan) {
		//	if(isset($this->activity_names[$key])) {
		//		$a[$key] = $this->activity_names[$key];
		//	} else {
		//		$a[$key] = $dan;
		//	}
		//}
		//return $a;
		return $this->activity_names;
	}


	public function displayObjectNames() { echo "<pre>"; print_r($this->object_names); echo "</pre>"; }

    public function getObjectName($o,$plural=false) {
    	$x = explode("_",$o);
		if(count($x)>1 && $x[0]=="TEMPLATE"){
			$o=$x[1];
		} elseif($x[0]=="FINANCE" && count($x)>1) {
			unset($x[0]);
			$o = implode("_",$x);
		}
		if(isset($this->object_names[strtolower($o)])) {
    		return $this->object_names[strtolower($o)].($plural?"s":"");
		} elseif(isset($this->default_object_names[strtolower($o)])) {
    		return $this->default_object_names[strtolower($o)].($plural?"s":"");
		} else {
			return ucwords($o);
		}
	}

	public function getAllObjectNames() {
		/*$a = array();
		foreach($this->default_object_names as $key => $dan) {
			if(isset($this->object_names[$key])) {
				$a[$key] = $this->object_names[$key];
			} else {
				$a[$key] = $dan;
			}
		}
		return $a;*/
		return $this->object_names;
	}






















	/*************************************
	 * Replacement functions
	 */


	/**
	 * Function to replace both |object| and |activity| with the current usage
	 */
	public function replaceAllNames($v) {
		//ASSIST_HELPER::arrPrint($this->object_names);
		//ASSIST_HELPER::arrPrint($this->activity_names);
		$v = $this->replaceObjectNames($v);
		$v = $this->replaceActivityNames($v);
		//$this->displayObjectNames();
		return $v;
	}
    /**
	 * Function to replace |object| with object_name
	 */
    public function replaceObjectNames($v) {
    	if(is_array($v)) {
	        $a = json_encode($v);
		} else {
			$a = $v;
		}
	    foreach($this->object_names as $key => $on) {
	        $a = str_ireplace("|".$key."|",$on,$a);
	    }
		if(is_array($v)) {
			$v = json_decode($a,true);
		} else {
			$v = $a;
		}
        return $v;
    }

	/**
	 * Function to replace |activity| with activty_name
	 */
	public function replaceActivityNames($v) {
		if(is_array($v)) {
			$a = json_encode($v);
		} else {
			$a = $v;
		}
		foreach($this->activity_names as $key => $on) {
			$a = str_ireplace("|".$key."|",$on,$a);
		}
		if(is_array($v)) {
			$v = json_decode($a,true);
		} else {
			$v = $a;
		}
		return $v;
	}


    public function replaceHeadingNames($head,$arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
	        foreach($head as $key => $on) {
	            $a = str_ireplace("|".$key."|",$on,$a);
	        }
	        $arr = json_decode($a,true);
		} else {
	        foreach($head as $key => $on) {
	            $arr = str_ireplace("|".$key."|",$on,$arr);
	        }
		}
        return $arr;
    }

    public function stripPipes($arr) {
    	if(is_array($arr)) {
	        $a = json_encode($arr);
            $a = str_ireplace("|","",$a);
	        $arr = json_decode($a,true);
		} else {
            $arr = str_ireplace("|","",$arr);
		}
        return $arr;
    }


}


?>