<?php
/** CLASS TO MANAGE LOGIN PROCESS - SECOND VALIDATION
Created on: 24 February 2016
Created by: Janet Currie

NOTE: PASSWORD AGE CHECKS ARE NOT CARRIED OUT ON 2nd VALIDATION LOGINS
 * 
 * 
**/


class ASSIST_LOGIN2 extends ASSIST_HELPER {
	
	private $request = array();
	private $is_second_validation = false;
	private $mdb;
	private $db;
	private $cdb;
	
	private $login_error = 0;
	private $login_error_message = "";
	
	private $user;
	private $cc;
	private $pwd;
	private $user_id;
	private $user_details;
	private $cc_details;
	
	private $user_type;
	
	private $system_settings;

	private $default_error_message = "Invalid login details.  Please try again.<br /><br /><span class=i>Hint: Remember that passwords are CaSe SeNsItIvE.</span>";

	const MASTER_ASSIST_DB_CC = "IASSIST";
	
	public function __construct($var,$valid8 = false) {
		$this->request = $var;
		$this->system_settings = unserialize(base64_decode($_SESSION['system_settings']));
		if(isset($var['is_second_validation'])) {
			$this->is_second_validation = $var['second_validation']*1==1 ? true : false; 
		}
		if($valid8===true) { 
			$this->mdb = new ASSIST_DB("master");
			$this->cdb = new ASSIST_DB();
			$this->validateLogin($var); 
		}
	}
	
	
	private function validateLogin($var) {
		$this->user = isset($var['uid2']) ? trim(strtolower($var['uid2'])) : $this->killMe();//"U".__LINE__);
		$this->cc = isset($var['cc2']) ? trim(strtolower($var['cc2'])) : $this->killMe();//"C".__LINE__);
		$this->pwd = isset($var['pid2']) ? $this->convertToHex($var['pid2']) : $this->killMe();//"P".__LINE__); //Removed ampersand conversion - replaced by encodeURIComponent on index [JC 2019-03-04 #AA-4]
		//check that not admin or support user name used
		if($this->user=="admin" || $this->user=="support") {
			$this->login_error++;
			$this->login_error_message = "Please use your personal credentials to login.";
		} else {
			$this->db = new ASSIST_DB();
			//if company code = already set company code
			if($this->cc == $_SESSION['cc']) {
				if($_SESSION['tku']=="support") {
					$this->killMe("You do not have permission to login as the Support user.  Please access Assist by using your personal credentials.");
				} else {
				$this->user_type = "CLIENT";
				//Validate company just in case they have been suspended - then only allow resellers & IASSIST to access as super users AA-341 JC
				$this->validateCompany();
				//leave $this->db to current cc
				//check valid user
				$this->validateUser();
				if($this->login_error==0) {
					//check is superuser
					$this->checkSuperUser();
					if($this->login_error==0) {
						//check valid password
						$this->validatePassword();
					}
				}
				}
			//elseif cc = IASSIST
			} elseif(strtoupper($this->cc)==self::MASTER_ASSIST_DB_CC) {
				$this->user_type = "IASSIST";
				//set $this->db to IASSIST
				$this->db->setDBRef("tk",$this->cc);
				//check valid user
				$this->validateUser();
				if($this->login_error==0) {
					//get company name for display purposes
					$sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '".strtoupper($this->cc)."'";
					$two = $this->mdb->mysql_fetch_one($sql);
					$this->cc_details['cmpname'] = $two['cmpname'];
					$this->cc_details['cc'] = $this->cc;
					
					//check is superuser
					$this->checkSuperUser();
					if($this->login_error==0) {
						//check valid password
						$this->validatePassword();
					}
				}
			//else
			} else {
				$this->user_type = "RESELLER";
				//check company code is reseller
				$this->validateReseller();
				if($this->login_error==0) {
					//set $this->db to reseller db
					$this->db->setDBRef("tk",$this->cc);
					//check valid user
					$this->validateUser();
					if($this->login_error==0) {
						//check is superuser
						$this->checkSuperUser();
						if($this->login_error==0) {
							//check valid password
							$this->validatePassword();
						}
					}
				}
			}	
			//if no errors
			if($this->login_error==0 && !isset($_SESSION['super_user'])){
				//log access attempt
				$this->logAccessAttempt(true);
				//update session details
				$_SESSION['tkn'].= " (".$this->user_details['tkn'];
				if($this->user_type=="RESELLER" || $this->user_type=="IASSIST") {
					$_SESSION['tkn'].= " of ".$this->cc_details['cmpname'];
				}
				$_SESSION['tkn'].= ")";
				$_SESSION['super_user'] = $this->user_details;
				$_SESSION['super_user']['cc'] = $this->cc_details;
				//set timeout
				$_SESSION['session_timeout']['expiration'] = strtotime(date("d F Y H:i:s"))+3600;//($this->system_settings['inact_duration']['setting']*60);
				$_SESSION['session_timeout']['current'] = strtotime(date("d F Y H:i:s"));
//				unset($_SESSION['system_settings']);
			}
				
		}
	}
	









	private function validateCompany() {
		$sql = "SELECT cmpname, cmpstatus FROM assist_company WHERE cmpcode = '".strtoupper($this->cc)."'";
		$two = $this->mdb->mysql_fetch_one($sql);
		if($two['cmpstatus']=="S") {
			$this->login_error_message = "The account for ".$two['cmpname']." has been suspended due to non-payment of subscription fees.";
			$this->killMe();
			return false;
		} elseif($two['cmpstatus']!="Y") {
			$this->login_error_message = "The account for ".strtoupper($this->cc)." has been closed.";
			$this->killMe();
			return false;
		} else {
			$this->cc_details['cmpname'] = $two['cmpname'];
			$this->cc_details['cc'] = $this->cc;
			return true;
		}
	}




	private function validateUser() {
		//$cmpcode = strtolower($_SESSION['cc']);
		$cmpcode = $this->cc;
			$sql = "SELECT tkid, CONCAT(tkname, ' ', tksurname) as tkn, tkpwd, tkstatus, tkemail, tklogin, tkpwdyn, tkpwddate, tklock, tklocktime, tkloginwindow 
					FROM assist_".$cmpcode."_timekeep 
					WHERE tkuser = '".$this->user."'";
			$rs = $this->db->db_get_num_rows($sql);
			if($rs!=1) {
				$activity_id = $this->logAccessAttempt(false,"USER");
				$this->logFailedUserName($this->user, $rs, $activity_id);
				$this->login_error_message = "There is an error with your user account.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
				$this->killMe();
			} else {
				$one = $this->db->mysql_fetch_one($sql);
				$this->user_details = $one;
				$this->user_id = $one['tkid'];
			}
	}

	private function checkSuperUser() {
		$cmpcode = $this->cc;
		$active = ASSIST_TK::ACTIVE;
		if($this->user_type=="CLIENT") {
			$stat = ASSIST_TK::SUPER_CAN_ADMIN;
		} elseif($this->user_type=="IASSIST") {
			$stat = ASSIST_TK::SUPER_CAN_RESELLER_CLIENT;
		} else {
			$stat = ASSIST_TK::SUPER_CAN_CLIENT;
		}
		$sql = "SELECT * FROM assist_".$cmpcode."_timekeep_superusers WHERE 
				tkid = '".$this->user_id."' AND 
				( status & $stat ) = $stat AND
				( status & $active ) = $active ";
		$x = $this->db->mysql_fetch_all($sql);
		$c = count($x);
		//$c = $this->db->db_get_num_rows($sql);
		if($c>0) {
			//return true;
		} else {
/*
			*/
			$activity_id = $this->logAccessAttempt(false,"PERM");
			$this->logFailedPermissions($this->user, $stat, $activity_id);
			$this->login_error_message = "You do not have permission to login as a super user.";
			$this->killMe();
			//return false;
		}
	}

	private function validatePassword() {
		if($this->pwd != $this->user_details['tkpwd']) {
			//$this->login_error_message = "Invalid password :: ".$this->pwd." : ".$this->user_details['tkpwd'];
			$this->killMe();
			
			$activity_id = $this->logAccessAttempt(false,"PWD");
			$this->logFailedPassword($this->user_details['tkid'],$this->pwd,$this->user_details['tkpwd'],$activity_id);
/*
			//Get system login counter setting
			$system_lockout_counter = $this->system_settings['att_number']['setting'];
			$system_lockout_counter_duration = $this->system_settings['att_counter_duration']['setting'];
			//Get login counter
			$counter = $this->getLoginAttemptCounter();
			//if login attempts >= system login setting
			if($counter >= $system_lockout_counter) {
				//lock user
				//$cmpcode = strtolower($_SESSION['cc']);
				$cmpcode = $this->cc;
				$sql = "UPDATE assist_".$cmpcode."_timekeep SET tklock = 'Y', tklocktime = now() WHERE tkid = '".$this->user_details['tkid']."'";
				$this->db->db_update($sql);
				//log lock
				$obj_id = $this->user_details['tkid'];
				$type = "L";
				$field = serialize(array("tklock","tklocktime"));
				$new = serialize(array("Y",date("Y-m-d H:i:s")));
				$old = serialize(array($this->user_details['tklock'],$this->user_details['tklocktime']));
				$this->logUserChange($obj_id,$type,$field,$new,$old,"Locked user due to $counter incorrect login attempt(s) within $system_lockout_counter_duration minutes in excess of the system setting of $system_lockout_counter incorrect login attempt(s).",$sql);
			}
*/
		}
	}


	private function validateReseller() {
		$sql = "SELECT cmpreseller FROM assist_company WHERE cmpcode = '".strtoupper($_SESSION['cc'])."'";
		$one = $this->mdb->mysql_fetch_one($sql);
		if(strtoupper($this->cc)!=$one['cmpreseller']) {
			$this->login_error_message = "You do not have permission to access '".strtoupper($_SESSION['cc'])."' as a super user.";
			$this->killMe();
			return false;
		} else {
			$sql = "SELECT cmpname, cmpstatus FROM assist_company WHERE cmpcode = '".strtoupper($this->cc)."'";
			$two = $this->mdb->mysql_fetch_one($sql);
			if($two['cmpstatus']=="S") {
				$this->login_error_message = "The account for ".$two['cmpname']." has been suspended due to non-payment of subscription fees.";
				$this->killMe();
				return false;
			} elseif($two['cmpstatus']!="Y") {
				$this->login_error_message = "The account for ".strtoupper($this->cc)." has been closed.";
				$this->killMe();
				return false;
			} else {
				$this->cc_details['cmpname'] = $two['cmpname'];
				$this->cc_details['cc'] = $this->cc;
				return true;
			}
		}
	}





	
	private function logAccessAttempt($success,$reason="") {
		if($success==false) {
			if($reason!="OUT"){
				$code = "IN_FAIL_".$reason;
			} else {
				$code = $reason;
			}
		} else {
			$code = "IN";
		}
		if(isset($_COOKIE['PHPSESSID']) && strlen($_COOKIE['PHPSESSID'])) {
			$unique_id = "PHPSESSID=".$_COOKIE['PHPSESSID'];
		} elseif(isset($_SERVER["HTTP_COOKIE"]) && strlen($_SERVER["HTTP_COOKIE"])) {
			$unique_id = $_SERVER["HTTP_COOKIE"];
		} else {
			$unique_id = "";
		}
		
		$tkid = isset($this->user_details['tkid']) ? $this->user_details['tkid'] : "X";
		$tkn = isset($this->user_details['tkn']) ? $this->user_details['tkn'] : "X";
		$cmpcode = strtolower($_SESSION['cc']);
		
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity_superusers (id, date, cmpcode, tkid, tkname, action, active,sessid,remote_addr) 
				VALUES (null,now(),'".$this->cc."','$tkid','$tkn','$code',1,'".$unique_id."|".substr($_SERVER['HTTP_USER_AGENT'],0,100)."','".$_SERVER['REMOTE_ADDR']."')";
		$id = $this->cdb->db_insert($sql);
		//if($reason=="SUPER_PWD" || $reason == "SUPER_USER") {
			return $id;		
		//}
	}

	private function logFailedPassword($tkid,$attempt,$stored,$activity_id) {
		$details = array(
			'attempt'=>$attempt,
			'attempt_raw'=>$this->convertFromHex($attempt),
			'stored'=>$stored,
			'stored_raw'=>$this->convertFromHex($stored),
		);
		$cmpcode = strtolower($_SESSION['cc']);
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity_details_superusers (cmpcode,tkid,datetime,activity_id,details) VALUES ('".$this->cc."','$tkid',now(),$activity_id,'".serialize($details)."')";
		$this->cdb->db_insert($sql);
		$this->logFailedAccessAttempt();
	}
	
	
	private function logFailedUserName($user,$num_rows,$activity_id) {
		$details = array(
			'user'=>$user,
			'rows'=>$num_rows,
		);
		$cmpcode = strtolower($_SESSION['cc']);
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity_details_superusers (cmpcode,tkid,datetime,activity_id,details) VALUES ('".$this->cc."','',now(),$activity_id,'".serialize($details)."')";
		$this->cdb->db_insert($sql);
		$this->logFailedAccessAttempt();
	}

	private function logFailedPermissions($user,$status,$activity_id) {
		$details = array(
			'user'=>$user,
			'failed_status'=>$status,
		);
		$cmpcode = strtolower($_SESSION['cc']);
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity_details_superusers (cmpcode,tkid,datetime,activity_id,details) VALUES ('".$this->cc."','',now(),$activity_id,'".serialize($details)."')";
		$this->cdb->db_insert($sql);
		$this->logFailedAccessAttempt();
	}

	
	private function logFailedAccessAttempt() {
		$success = false;
		$reason = "OUT";
		$code = "OUT";
		
		$tkid = $_SESSION['tid']; //isset($this->user_details['tkid']) ? $this->user_details['tkid'] : "X";
		$tkn = $_SESSION['tkn']; //isset($this->user_details['tkn']) ? $this->user_details['tkn'] : "X";
		$cmpcode = strtolower($_SESSION['cc']);
		if(isset($_COOKIE['PHPSESSID']) && strlen($_COOKIE['PHPSESSID'])) {
			$unique_id = "PHPSESSID=".$_COOKIE['PHPSESSID'];
		} elseif(isset($_SERVER["HTTP_COOKIE"]) && strlen($_SERVER["HTTP_COOKIE"])) {
			$unique_id = $_SERVER["HTTP_COOKIE"];
		} else {
			$unique_id = "";
		}
		
		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) 
				VALUES (null,now(),'$tkid','$tkn','$code',1,'".$unique_id."|".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."')";
		$id = $this->cdb->db_insert($sql);
	}




	
	public function getValidationResult() {
		//return array (
			//login_error
			//error message
			//next page
		$data = array(
			$this->login_error,
			$this->login_error_message,
			($this->login_error==0?"login.html":""),
		);
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private function killMe($err=null) {
		$this->login_error++;
		if(strlen($this->login_error_message)==0) {
			if(!isset($err) || strlen($err)==0) {
				if(isset($this->default_error_message)) {
					$this->login_error_message = $this->default_error_message;
				} else {
					$this->login_error_message = "Oops!  No Message to feed back.  Sorry.";
				}
		} else {
				$this->login_error_message = $err;
		}
	}
	}

		
		

		


		

	public function __destruct() {

		}

	}




?>