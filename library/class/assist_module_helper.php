<?php

/******* FILE ERROR CODES
UPLOAD_ERR_INI_SIZE : 1 : File size exceeds limit.
UPLOAD_ERR_FORM_SIZE : 2 : File size exceeds limit.
UPLOAD_ERR_PARTIAL : 3 : The uploaded file was only partially uploaded. Please try again before contacting your Business Partner with the following error code: FILE3.
UPLOAD_ERR_NO_FILE : 4 : No file was uploaded. Please check that the original file still exists and try again before contacting your Business Partner with the following error code: FILE4.
UPLOAD_ERR_NO_TMP_DIR : 6 : A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE6.
UPLOAD_ERR_CANT_WRITE : 7 :  A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE7.
UPLOAD_ERR_EXTENSION : 8 :   A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE8.
*/

class ASSIST_MODULE_HELPER extends ASSIST_DB {

	public $assist;
	public $assist_user;

	private $assist_root;

	protected $cc;
	protected $user_id;
	protected $mod_ref;
	protected $attach_java_already_echoed = false;
	protected $attachment_default_content = "application/octet-stream";

	private $attachment_delete_ajax = false;
	private $attachment_delete_function = "";
	private $attachment_delete_link = "ajax/delete_attachment.php";
	private $attachment_delete_options = "";
	private $attachment_download_link = "ajax/download_attachment.php";
	private $attachment_download_options = "";
	private $attachment_folder = "";

	private $deleted_folder = "deleted";

	private $file_error_codes = array(
		1 => "File size exceeds limit.",
		2 => "File size exceeds limit.",
		3 => "The uploaded file was only partially uploaded. Please try again before contacting your Business Partner with the following error code: FILE[3].",
		4 => "No file was uploaded. Please check that the original file still exists and try again before contacting your Business Partner with the following error code: FILE[4].",
		6 => "A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[6].",
		7 => "A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[7].",
		8 => "A system error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[8].",
	);

	private $start;

	/**
	 * Preset module reference required to link MSCOA1, IDP3 and SDBP6 modules
	 * Other copies of MSCOA1 can be run on the database, but only the one with MSCOA as the modref will link with IDP3 and SDBP6
	 * JC - 19 March 2018
	 */
	protected $mscoa_for_idp3_and_sdbp6_modref = "MSCOA";
	protected $mscoa_for_idp3_and_sdbp6_modcode = "MSCOA1";
	protected $mscoa_for_idp3_and_sdbp6_title = "mSCOA Library";
	protected $mscoa_for_idp3_and_sdbp6_title_default = "mSCOA Library";
	protected $mscoa_for_idp3_and_sdbp6_title_set = false;

	const BREADCRUMB_DIVIDER = ">>";

	const INTEGRATION_ACTIVE = 2;
	const INTEGRATION_TYPE_HR = "HR";

	public function __construct($dbtype="client",$cc="",$autojob=false,$modref="") {
		$this->start = time();
		$this->assist_root = new ASSIST_ROOT();
		$this->assist = new ASSIST($autojob);
		$this->assist_user = new ASSIST_USER();
		if(strlen($cc)==0) {
			$this->cc = $this->assist->getCmpCode();
			$cc = $this->cc;
		} else {
			$this->cc = $cc;
		}
		parent::__construct($dbtype,$cc);
		$this->user_id = $this->assist->getUserID();
		if(strlen($modref)==0) {
			$this->mod_ref = $this->assist->getModRef();
		} else {
			$this->setDBRef($modref);
			$this->mod_ref = $modref;
		}


	}


	function markTime($s) {
		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		$finish = $time;
		$total_time = round(($finish - $this->start), 4);
//		echo '<p>'.$s.' => '.$total_time.' seconds.';

	}

	/**
	 * Preset module reference required to link MSCOA1, IDP3 and SDBP6 modules
	 * Other copies of MSCOA1 can be run on the database, but only the one with MSCOA as the modref will link with IDP3 and SDBP6
	 * JC - 19 March 2018
	 */
	public function getMSCOAModRefForIDP3AndSDBP6() { return $this->mscoa_for_idp3_and_sdbp6_modref; }
	public function getMSCOAModCodeForIDP3AndSDBP6() { return $this->mscoa_for_idp3_and_sdbp6_modcode; }
	public function getMSCOAModRefForPMSuite() { return $this->mscoa_for_idp3_and_sdbp6_modref; }
	public function getMSCOAModCodeForPMSuite() { return $this->mscoa_for_idp3_and_sdbp6_modcode; }
	public function getMSCOAModuleNameForIDP3AndSDBP6() {
		return $this->getMSCOAModuleNameForPMSuite();
	}
	public function getMSCOAModuleNameForPMSuite() {
		if($this->mscoa_for_idp3_and_sdbp6_title_set) {
			return $this->mscoa_for_idp3_and_sdbp6_title;
		} else {
			$sql = "SELECT modtext as name FROM assist_menu_modules WHERE modref = '".$this->mscoa_for_idp3_and_sdbp6_modref."' AND modyn = 'Y' ORDER BY modid DESC LIMIT 1";
			$row = $this->mysql_fetch_one($sql);
			if(isset($row['name']) && strlen($row['name'])>0) {
				$this->mscoa_for_idp3_and_sdbp6_title = $row['name'];
				$this->mscoa_for_idp3_and_sdbp6_title_set = true;
			} else {
				$this->mscoa_for_idp3_and_sdbp6_title = $this->mscoa_for_idp3_and_sdbp6_title_default;
				$this->mscoa_for_idp3_and_sdbp6_title_set = true;
			}
		}
	}

	public function getBreadcrumbDivider() { return self::BREADCRUMB_DIVIDER; }

	public function createPageTitleBreadcrumb($title_array,$extra="") {
		return $extra.implode($extra." ".$this->getBreadcrumbDivider()." ".$extra,$title_array).$extra;
	}
	/**
	 * function createMenuTrailFromLastLink($link) where $link = "contents_first_second_third"
	 * Purpose: Convert final link in menu chain ("contents_first_second_third") into an array containing each level separately
	 *          "contents_first_second_third" => array("contents","contents_first","contents_first_second","contents_first_second_third")
	 *     This can then be used to generate a menu trail using createPageTitleBreadcrumb($title_array) to guide users on paths within a module
	 *     Also: use $object->replaceAllNames($object->createPageTitleBreadcrumb($object->createMenuTrailFromLastLink("contents_first_second_third"),"|")) to convert "contents_first_second_third" into Contents >> First Tier >> Second Tier >> Third Tier
	 */
	public function createMenuTrailFromLastLink($link) {
		//set variables
		$array = array();
		$x = "";
		//make sure that a link was sent
		if(strlen($link)>0) {
			$y = explode("_",$link);
			//set first level
			$x = $y[0];
			//Set the first level as a separate item ONLY if it is not set to "menu" - this is a restricted word used to indicate menu names within the assist_root->object_names variable
			if($x!="menu") {
				$array[] = $x;
			}
			unset($y[0]);
			//make sure that more levels are waiting to be processed
			if(count($y)>0) {
				//loop through remaining levels to build trail
				foreach($y as $z) {
					$x.="_".$z;
					$array[] = $x;
				}
			}
		}
		return $array;
	}

	/**
		$attach = array('unique_key'=>array('system_filename'=>src file,'original_filename'=>user file)
	**/
	function displayObjectAttachments($can_edit=false,$attach=array(),$folder="",$need_java=true) {
		echo $this->getObjectAttachmentDisplay($can_edit,$attach,$folder,$need_java);
	}
	function getObjectAttachmentDisplay($can_edit=false,$attach=array(),$loc="",$need_java=true) {
	//echo $loc;
		$echo = "";
			$delete_link = $this->getHelperAttachmentDeleteLink();
			$delete_options = $this->getHelperAttachmentDeleteOptions();
			$download_link = $this->getHelperAttachmentDownloadLink();
			$download_options = $this->getHelperAttachmentDownloadOptions();
			if(strlen($loc)==0) { $loc = $this->getHelperAttachmentFolder(); }
			$valid_files = false;
			$folder = "";
			$location = explode("/",$_SERVER["REQUEST_URI"]);
			$l = count($location)-2;
			for($f=0;$f<$l;$f++) {
				$folder.= "../";
			}
			$folder.="files/".$this->getCmpCode()."/".$loc;
			foreach($attach as $key=>$a) {
				$file = $folder."/".$a['system_filename'];
				$fname = $can_edit==true && strlen($a['original_filename'])>26 ? substr($a['original_filename'],0,25)."..." : $a['original_filename'];
				if(file_exists($file)) {
					if($valid_files===false) { $echo.= "<p class=b>Attached Files:</p><ul>"; }
					$valid_files = true;
					$echo.= "<li id=li_".$key."><a id=".$key." class='down_attach u' style='cursor: pointer' >".$fname."</a>".($can_edit==true ? "<span class='float red hand delete_attach' key=".$key.">Delete</span>" : "")."</li>";
				}
			}
			if($valid_files===true) {
				$echo.= "</ul>";
				if($this->attach_java_already_echoed!==true && $need_java===true) {
					$this->attach_java_already_echoed = true;
					$echo.= "
					<script type=text/javascript>
					//alert('attachment_form!');
					$(function() {
						$('a.down_attach').click(function() {  //alert('down_attach clicked!');
							//console.log($(this));
							var i = $(this).prop('id');  //alert(i);
							var url = '".(strlen($download_link)>0 ? $download_link : "ajax/download_attachment.php")."?id='+i".(strlen($download_options)>0 ? "+'&".$download_options."'" : "").";
							//alert('downloading '+i+' via \\n'+url);
							//console.log(url);
							document.location.href = url;
						});";
						if($can_edit) {
						$echo.="
						$('span.delete_attach').hover(
							function(){ $(this).removeClass('red').addClass('orange'); $(this).parent().css({'background-color':'#ababab'}); },
							function(){ $(this).removeClass('orange').addClass('red'); $(this).parent().css({'background-color':''}); }
						).click(function() {
							var i = $(this).attr('key');
							if(confirm('Are you sure you wish to delete the attachment: \\n'+$('#'+i).text()+'? \\n \\n Warning: Any other changes made on this page will be lost.\\nThis action cannot be undone.')) {
						";
								if($this->getHelperAttachmentDeleteByAjax()===true && strlen($this->getHelperAttachmentDeleteFunction())>0) {
									$echo.="
										var index = i;
										var url = '".(strlen($delete_link)>0 ? $delete_link : "ajax/delete_attachment.php")."';
										//alert(url);
										var dta = 'id='+i".(strlen($delete_options)>0 ? "+'&".$delete_options."'" : "").";
										//alert(dta);
										".$this->getHelperAttachmentDeleteFunction()."(index,url,dta);
									";
								} else {
									$echo.="
										var url = '".(strlen($delete_link)>0 ? $delete_link : "ajax/delete_attachment.php")."?id='+i".(strlen($delete_options)>0 ? "+'&".$delete_options."'" : "").";
										document.location.href = url;
									";
								}
								$echo.="
							} //endif confirm delete
						});";
						}
					$echo.="
					});
					</script>";
				}
			}
		return $echo;
	}

	function getAttachmentJavaForDownload($can_edit=false,$need_script_tags=false) {
			$delete_link = $this->getHelperAttachmentDeleteLink();
			$delete_options = $this->getHelperAttachmentDeleteOptions();
			$download_link = $this->getHelperAttachmentDownloadLink();
			$download_options = $this->getHelperAttachmentDownloadOptions();
				$this->attach_java_already_echoed = true;
				$echo= ($need_script_tags===true ? "<script type=text/javascript>" : "")."
				//alert('attachment_form!');
					//function downloadAttachmentClickEvent(\$me) {
						$('a.down_attach').live('click',function() {
							//console.log($(this));
				//alert('down_attach clicked!');
							var i = $(this).prop('id');  //alert(i);
							var url = '".(strlen($download_link)>0 ? $download_link : "ajax/download_attachment.php")."?id='+i".(strlen($download_options)>0 ? "+'&".$download_options."'" : "").";
							//alert('downloading '+i+' via \\n'+url);
							//console.log(url);
							document.location.href = url;
						});
					//}";
					$echo.="
					".($need_script_tags===true ? "</script>" : "");
		return $echo;
	}

	function getAttachmentJavaForDownload2($can_edit=false,$need_script_tags=false) {
			$delete_link = $this->getHelperAttachmentDeleteLink();
			$delete_options = $this->getHelperAttachmentDeleteOptions();
			$download_link = $this->getHelperAttachmentDownloadLink();
			$download_options = $this->getHelperAttachmentDownloadOptions();
				$this->attach_java_already_echoed = true;
				$echo= ($need_script_tags===true ? "<script type=text/javascript>" : "")."
						$('a.down_attach').click(function() {
							//console.log($(this));
							var i = $(this).prop('id');  //alert(i);
							var url = '".(strlen($download_link)>0 ? $download_link : "ajax/download_attachment.php")."?id='+i".(strlen($download_options)>0 ? "+'&".$download_options."'" : "").";
							//console.log(url);
							document.location.href = url;
						});
						$('body').on('click','a.down_attach',function() {
							//console.log($(this));
							var i = $(this).prop('id');  //alert(i);
							var url = '".(strlen($download_link)>0 ? $download_link : "ajax/download_attachment.php")."?id='+i".(strlen($download_options)>0 ? "+'&".$download_options."'" : "").";
							//console.log(url);
							document.location.href = url;
						});
											";
					$echo.="
					".($need_script_tags===true ? "</script>" : "");
		return $echo;
	}

	function displayAttachmentForm($next_step="") {
		echo $this->getAttachmentForm($next_step);
	}
	function getAttachmentForm($next_step="") {
		$http_host = explode(".",$_SERVER['HTTP_HOST']);
		$is_localhost = in_array("localhost",$http_host);
		$echo= "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" style='margin-bottom: 5px;' /> <input type=button value=\"Clear\" class=cancel_attach  />
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			<input type=hidden name=next_step value='".$next_step."' id=next_step />
			<input type=hidden value='' name=result id=result />
			<input type=hidden value='' name=response id=response />
			".($is_localhost ? "<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:500px;height:500px;border:1px solid #999999;color:#fe9900\"></iframe>" : "<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #000000;color:#009900;\"></iframe>")."
			".$this->getDisplayResult(array("warn","No more than 10 files may be uploaded at once.<br />Total file size cannot exceed 100MB per update."))."
		<script type=text/javascript>
		$(function() {
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			var attachment_count = 1;
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<input type=button value=\"Clear\" class=cancel_attach />').bind('click',function() { handler($('#firstdoc input:button').index(this)); });
				});
				attachment_count++;
				if(attachment_count==10) { $('#attachlink').hide(); }
			});
			$('input:button.cancel_attach').click(function() {
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		});
		</script>";
		return $echo;
	}

	function resetAttachmentForm() {
		return "<input type=\"file\" name=\"attachments[]\" size=\"30\" style='margin-bottom: 5px;' /> <input type=button value=\"Clear\" class=cancel_attach  />";
	}


	function downloadAttachment($full_path, $new_filename) {
		/*$size = filesize($full_path);
		@header('Content-Length: '.$size);
		//header('Content-Description: File Transfer');
		@header('Content-Type: application/octet-stream');
		@header('Content-Transfer-Encoding: binary');
		@header("Content-disposition: attachment; filename=".$new_filename);
		@readfile($full_path);*/
		$this->assist->downloadFile3($full_path,$new_filename,"application/octet-stream");
	}

	/**
	 * Function to strip out any odd characters from user generated file names that Linux / Database might not like [AA-506]
	 * @param $original_filename - string of full, unprocessed original file name as set by user, including file extension
	 */
	function normalizeOriginalFilenameForLinux($original_filename) {
		//remove any extra periods(.) so that only the ext period remains
		$o = explode(".",$original_filename);
		if(count($o)>1) {
			//get the extension from the last array element but limit it to a max of 10 characters to prevent SQL / code injection - file extensions should only be 3-4 chars anyway
			$ext = ".".substr($o[count($o)-1],0,10);
			unset($o[count($o)-1]);
		} else {
			$ext = "";
		}
		$original_filename = implode("_",$o);
		//trim all spaces from front & back and then replace any spaces in filenames with _
		$original_filename = str_replace(" ","_",trim($original_filename));
		//remove all illegal characters from file name -> ALLOWED CHAR: A-Z(case insensitive) 0-9 . - _ ( ) [No spaces allowed]
		$original_filename = preg_replace("/[^a-zA-Z0-9.\-\_]/","",$original_filename);
		//catch empty file names if required
		if(strlen($original_filename)==0) {
			$original_filename = "assist_attachment_".date("Ymd")."_".date("His");
		}
		//add file extension back but also pull out any invalid characters just in case
		$original_filename.=preg_replace("/[^a-zA-Z0-9.\-\_]/","",$ext);
		return $original_filename;
	}

	/**
	 * Function to create a standard system filename for saving client documents on Assist; Does NOT include file extension [AA-506]
	 * @param string $object_type - the object type within the module linked to the document, if not provided will be set to modref
	 * @param int $object_id - the id of hte specific object linked to the document
	 * @param false $extra - any extra info that might be useful in the the filename (e.g. specific update log or time period) - not required, leave as false to skip
	 * @param false $increment - if other documents are already linked to the specific object then give an increment to prevent duplicates (not required, can be added manually later if so desired, leave as false then)
	 * @return string
	 */
	function generateSystemFilename($object_type="",$object_id=0,$extra=false,$increment=false) {
		if(strlen($object_type)==0) { $object_type = $this->getModRef(); }
		$system_filename = $object_type.$object_id."_";
		$system_filename.=($extra!==false && strlen($extra)>0?$extra."_":"");
		$system_filename.=date("YmdHis");
		$system_filename.=($increment!==false?"_".$increment:"");
		return $system_filename;
	}

	/**
	 * Function to create a standard system filename for saving client documents on Assist; DOES include file extension [AA-506]
	 * @param string $original_filename - the original file name (or normalised original file name) from which the extension is obtained - don't pass only the ext, must be the full filename
	 * @param string $object_type - the object type within the module linked to the document, if not provided will be set to modref
	 * @param int $object_id - the id of hte specific object linked to the document
	 * @param false $extra - any extra info that might be useful in the the filename (e.g. specific update log or time period) - not required, leave as false to skip
	 * @param false $increment - if other documents are already linked to the specific object then give an increment to prevent duplicates (not required but recommended)
	 * @return string
	 */
	function generateSystemFilenameWithExtension($original_filename,$object_type="",$object_id=0,$extra=false,$increment=false) {
		$system_filename = $this->generateSystemFilename($object_type,$object_id,$extra,$increment);
		$o = explode(".",$original_filename);
		if(count($o)>1) {
			$ext = ".".$o[count($o)-1];
		} else {
			$ext = "";
		}
		return $system_filename.$ext;
	}

	/***********************
		SET FUNCTIONS
	***********************/
	function setHelperAttachmentDeleteByAjax($a) { $this->attachment_delete_ajax = $a; }
	function setHelperAttachmentDeleteFunction($a) { $this->attachment_delete_function = $a; }
	function setHelperAttachmentDeleteLink($a) { $this->attachment_delete_link = $a; }
	function setHelperAttachmentDeleteOptions($a) { $this->attachment_delete_options = $a; }
	function setHelperAttachmentDownloadLink($a) { $this->attachment_download_link = $a; }
	function setHelperAttachmentDownloadOptions($a) { $this->attachment_download_options = $a; }
	function setHelperAttachmentFolder($a) { $this->attachment_folder = $a; }

	/***********************
		GET FUNCTIONS
	***********************/
	function getAttachmentContent() { return $this->attachment_default_content; }
	function getHelperAttachmentDeleteByAjax() { return $this->attachment_delete_ajax; }
	function getHelperAttachmentDeleteFunction() { return $this->attachment_delete_function; }
	function getHelperAttachmentDeleteLink() { return $this->attachment_delete_link; }
	function getHelperAttachmentDeleteOptions() { return $this->attachment_delete_options; }
	function getHelperAttachmentDownloadLink() { return $this->attachment_download_link; }
	function getHelperAttachmentDownloadOptions() { return $this->attachment_download_options; }
	function getHelperAttachmentFolder() { return $this->attachment_folder; }
	function getGenericAttachmentDeletedFolder() { return $this->deleted_folder; }
	//file error codes
	function getFileErrorMsg($e) {
		if(isset($this->file_error_codes[$e])) {
			return $this->file_error_codes[$e];
		} else {
			return "An unknown error has occurred.  Please try again before contacting your Business Partner with the following error code: FILE[".$e."].";
		}
	}








    /************
     * Reworking functions
     */


     /**
      * Function to convert small array of menu items into the formatted menu item needed for the ASSIST_HELPER::getNavButtons($menu)
      *
      * @param (Array) arr = array with display, link, active
      * @param (int) level = tier of buttons 1 or 2
      *              needed:$menu[id] = array('id'=>id,'url'=>path_to,'active'=>(bool)checked_or_not,'display'=>heading);
      * @return (HTML) code needed to echo onscreen to generate buttons
      */
   public function generateNavigationButtons($buttons,$level) {
       $menu = array();
       foreach($buttons as $key => $a){
           $menu[$key] = array(
            'id'=>$a['id'],
            'url'=>$a['link'],
            'active'=>($a['active']==1),
            'display'=>$a['name'],
            'help'=>$a['help']
           );
       }
       $this->assist->setNavButtonsLevel($level);
       return $this->assist->getNavButtons($menu);

   }















	public function getUserTableName() {
		return "assist_".strtolower($this->getCmpCode())."_timekeep";
	}



























	/************************************************************************************
	 * INTEGRATION FUNCTIONS
	 */

	public function isCompanyHRIntegrated() {
		$cmpcode = strtoupper($this->getCmpCode());
		$mdb = new ASSIST_DB("master");
		$sql = "SELECT * 
				FROM integration_settings
				INNER JOIN integration_type ON set_typeid = type_id 
				WHERE set_cmpcode = '$cmpcode' 
				AND type_name = '".self::INTEGRATION_TYPE_HR."'
				AND set_status = ".self::INTEGRATION_ACTIVE;
		$rows = $mdb->mysql_fetch_all($sql);
		return count($rows)>0 ? true : false;
	}
























	/***********************
		ASSIST SHORTCUT FUNCTIONS
	***********************/
	function echoPageHeader($jquery_version="1.10.0",$scripts=array(),$css=array()) {
		$this->assist->echoPageHeader($jquery_version,$scripts,$css);			//echos automatically
	}
	function displayPageHeader() {
		$this->assist->displayPageHeader();			//echos automatically
	}
	function displayPageHeaderJQ($jqv="1.10.0") {
		$this->assist->displayPageHeaderJQ($jqv);			//echos automatically
	}
	function displayResult($r) {
		$this->assist->displayResult($r);			//echos automatically
	}
	function getDisplayResult($r) {
		return $this->assist->getDisplayResult($r);
	}
	function JSdisplayResultPrep($t) {				//echos automatically
		$this->assist->JSdisplayResultPrep($t);
	}
	function JSdisplayResultObj($txt="",$icon="") {				//echos automatically
		//USED BY ADMIRE MODULES
		$this->assist->JSdisplayResultObj($txt,$icon);
	}
	function displayAuditLogLink($txt="",$icon="") {				//echos automatically
		//USED BY ADMIRE MODULES
		$this->assist->displayAuditLogLink($txt,$icon);
	}

	function getJSdisplayResultPrep($t) {
		return $this->assist->getJSdisplayResultPrep($t);
	}
	function displayIcon($r,$o="") {				//echo
		$this->assist->displayIcon($r,$o);
	}
	function getDisplayIcon($r,$o="",$c="") {
		return $this->assist->getDisplayIcon($r,$o,$c);
	}
	function getDisplayIconAsDiv($r,$o="",$c="") {
		return $this->assist->getDisplayIconAsDiv($r,$o,$c);
	}
	function getDisplayStatusAsDiv($r,$o="") {
		return $this->assist->getDisplayStatusAsDiv($r,$o);
	}
	function code($r) {
		return $this->assist->code($r);
	}
	function decode($r) {
		return $this->assist->decode($r);
	}
	function arrPrint($a) {
		return $this->assist->arrPrint($a);
	}
	function stripStringForComparison($s) {
		return $this->assist->stripStringForComparison($s);
	}
	function cleanUpText($text) {
		return $this->assist->cleanUpText($text);
	}

	/**
	 * Function to convert array into string for SQL statement
	 *
	 * @param (Array) var = array ('db field'=>"value")
	 * @param (String) join = glue for implode, default = , for insert
	 * @return (String) field = 'value', field2 = 'value2'
	 */
    function convertArrayToSQL($var,$join=",") {
		return $this->assist->convertArrayToSQL($var,$join);
    }
    function convertArrayToSQLForSave($var,$join="") {
		return $this->assist->convertArrayToSQLForSave($var,$join);
    }
	function getBPAdetails($bpa="",$logo_bpa="") {
		return $this->assist->getBPAdetails($bpa,$logo_bpa);
	}
	function getCmpAdmin($cmpcode) {
		return $this->assist->getCmpAdmin($cmpcode);
	}
	function getAUserName($t) {
		return $this->assist->getTKDetails($t,$this->cc,"tkn");
	}
	function getMyName() {
		return $this->getAUserName($this->getUserID());
	}
	function getAnEmail($t,$cc="",$inc_name=false) {
		if(strlen($cc)==0) { $cc = $this->cc; }
		return $this->assist->getTKDetails($t,$cc,"tkemail",$inc_name);
	}
	function getUserEmail($t,$cc="",$inc_name=false) {
		return $this->getAnEmail($t,$cc,$inc_name);
	}
	function getUserRecord($t,$cc="") {
		return $this->assist->getUserRecord($t,$cc);
	}
	function getUserNameWithEmail($t,$cc="") {
		return $this->getAnEmail($t,$cc,true);
	}
	function getUserDetailsForEmail($t,$cc="") {
		$raw = $this->getAnEmail($t,$cc,true);
		$data = array(
			'email'=>$raw['email'],
			'name'=>$raw['tkn'],
			'tkid'=>$t
		);
		return $data;
	}
	function getTKList($current=true,$modref="") {
		return $this->assist->getTKList($current,$modref);
	}
	function drawStatus($s) {
		return $this->assist->drawStatus($s);
	}
	function getUserName() {
		return $this->assist->getUserName();
	}
	function getUserID() {
		return $this->assist->getUserID();
	}
	function getActualUserID() {
		return $this->assist->getActualUserID();
	}
	function getActualUserName() {
		return $this->assist->getActualUserName();
	}
	function getJeanModules() {
		return $this->assist->getJeanModules();
	}
	function doPasswordSecurityAnswersPassInspection() {
		return $this->assist->doPasswordSecurityAnswersPassInspection();
	}
	function haveISeenTheFPDYetToday() {
		return $this->assist->haveISeenTheFPDYetToday();
	}
	function IveSeenTheFPD() {
		$this->assist->IveSeenTheFPD();
	}
	function getToday() {
		return strtotime(date("d F Y H:i:s"));
	}
	function dateToInt($d) {
		return $this->assist->dateToInt($d);
	}
	function checkFolder($folder,$depth=1,$return=false,$cmpcode=false) {
		return $this->assist->checkFolder($folder,$depth,$return,$cmpcode);
	}
	function saveEcho($folder, $filename, $echo, $depth=1) {
		return $this->assist->saveEcho($folder, $filename, $echo, $depth);
	}
	function getFilesLocation($path,$cc) {
		return $this->assist->getFilesLocation($path,$cc);
	}
	function drawNavButtons($menu) {
		return $this->assist->drawNavButtons($menu);
	}

	function displayGoBack($a="",$b="Go Back") {
		return $this->assist->displayGoBack($a,$b);
	}
	function displayGoUp($a="",$b="Go Up") {
		return $this->assist->displayGoUp($a,$b);
	}
	function getGoBack($url="",$txt="",$include_js=false) {
		return $this->assist->getGoBack($url,$txt,$include_js);
	}

	function downloadFile2($folder, $old, $new, $content) {
		return $this->assist->downloadFile2($folder, $old, $new, $content);
	}

	function downloadFile3($old, $new) {
		return $this->assist->downloadFile3($old, $new);
	}

	function displayAuditLog($logs,$flds,$extra="") {
		return $this->assist->displayAuditLog($logs,$flds,$extra);
	}
	function isAdminUser($user_id="") {
		return $this->assist->isAdminUser($user_id);
	}
	function getAssistAdminName() { return $this->assist->getAssistAdminName(); }
	//Added option to test another user id (not currently logged in) for support user status - needed for PM6 #AA-676 [JC] 18 August 2021
	function isSupportUser($user_id="") {
		return $this->assist->isSupportUser($user_id);
	}
	function getSupportUserID() {
		return $this->assist->getSupportUserID();
	}
	function isAssistHelp() {
		return $this->assist->isAssistHelp();
	}
	function isIASClient() {
		return $this->assist->isIASClient();
	}
	function formatBytes($b,$p = null,$decimal=2) {
		return $this->assist->formatBytes($b,$p,$decimal);
	}
	function checkIntRef($r) {
		return $this->assist->checkIntRef($r);
	}
	function checkIntRefForWholeIntegersOnly($r) {
    	return $this->assist->checkIntRefForWholeIntegersOnly($r);
	}
	function removeBlanksFromArray($a,$numerical=true,$maintain_key=false){
		return $this->assist->removeBlanksFromArray($a,$numerical,$maintain_key);
	}
	function getTerm($i,$plural=false){
		return $this->assist->getTerm($i,$plural);
	}
	function convertToHex($a) {
		return $this->assist->convertToHex($a);
	}
	function convertFromHex($a) {
		return $this->assist->convertFromHex($a);
	}
	function getFolderSize($folder,$ignore=array()) { return $this->assist->getFolderSize($folder,$ignore); }
	function displayFolderSize($totalsize,$folder,$moduletitle) { return $this->assist->displayFolderSize($totalsize,$folder,$moduletitle); }
	function getReportPath() { return $this->assist->getReportPath(); }
	function getModTitle($ref="") { return $this->assist->getModTitle($ref); }
	function getModLocation() { return $this->assist->getModLocation(); }
	function getAModLocation($ref) { return $this->assist->getAModLocation($ref); }
	function setModRef($ref) { return $this->assist->setModRef($ref); }
	function getCmpName() { return $this->assist->getCmpName(); }
	function getSiteName() { return $this->assist->getSiteName(); }
	function getSiteCode() { return $this->assist->getSiteCode(); }
	function getUnspecified() { return $this->assist->getUnspecified(); }
	function isActiveReseller() { return $this->assist->isActiveReseller(); }
	function isDemoCompany() { return $this->assist->isDemoCompany(); }
	function getIgniteTitleLogo() { return $this->assist->getIgniteTitleLogo(); }
	function setSessionDetails($menuref,$menurow) { return $this->assist->setSessionDetails($menuref,$menurow); }
	function getSplitModules() { return $this->assist->getSplitModules(); }
	function getActionModules() { return $this->assist->getActionModules(); }
	function isSplitModules($modloc) { return $this->assist->isSplitModules($modloc); }
	function getFintegModules() { return $this->assist->getFintegModules(); }
	function isFintegModule($modloc) { return $this->assist->isFintegModule($modloc); }
	function isMdocModule($modloc){
	    return $this->assist->isMdocModule($modloc);
	}
	function isMasterLinkModule($modloc){
	    return $this->assist->isMasterLinkModule($modloc);
	}
    function getAllMenuModules($withUsers=FALSE){
        return $this->assist->getAllMenuModules($withUsers);
    }
    function getAdminSystemMenu() { return $this->assist->getAdminSystemMenu(); }
	/**
	 * Function to read main user profile to get default page length
	 */
	function mainProfileActionsToDisplay() { return $this->assist_user->mainProfileActionsToDisplay(); }
	function getUserProfile() { $this->assist->getUserProfile(); }
	function getPageHeader($jquery_version="1.10.0",$scripts=array(),$css=array()) {
		return $this->assist->getPageHeader($jquery_version,$scripts,$css);
	}
	function getFixedPageHeader() {
		return $this->assist->getFixedPageHeader();
	}


	/* generate the primary save path for an attachment */
	function getSaveFolder($depth=1) {
		$cmpcode = strtolower($this->getCmpCode());
		$path = "";
			$me = $_SERVER['PHP_SELF'];
			$myloc = explode("/",$me);
			if(strlen($myloc[0])==0) { unset($myloc[0]); }
			$mc = count($myloc) - 1;
			$path = "files/".$cmpcode."/".$path;
			$chk = explode("/",$path);
			$chkloc = "";
			for($m=1;$m<count($myloc);$m++) { $chkloc.="../"; }
			for($m=1;$m<$depth;$m++) { $chkloc.="../"; }
			foreach($chk as $c) {
				if(strlen($c)>0 && $c != "." && $c != "..") {
					$chkloc.= "/".$c;
				}
			}
		return $chkloc;
	}


	function getMaxUploadFileSize() {
		return $this->assist->getMaxUploadFileSize();
	}















	/********************************
	 * ASSIST_ROOT functions
	 */

	public function replaceAllNames($v) { return $this->assist_root->replaceAllNames($v); }
	public function replaceObjectNames($v) { return $this->assist_root->replaceObjectNames($v); }
	public function replaceActivityNames($v) { return $this->assist_root->replaceActivityNames($v); }
	public function replaceHeadingNames($head, $arr) { return $this->assist_root->replaceHeadingNames($head, $arr); }
	public function stripPipes($arr) { return $this->assist_root->stripPipes($arr); }
	public function getActivityName($a) { return $this->assist_root->getActivityName($a); }
	public function getObjectName($a,$plural=false) { return $this->assist_root->getObjectName($a,$plural); }
	public function displayObjectNames($a) { return $this->assist_root->displayObjectNames($a); }
	public function getAllActivityNames() { return $this->assist_root->getAllActivityNames(); }
	public function getAllObjectNames() { return $this->assist_root->getAllObjectNames(); }
	protected function setDefaultObjectNames($on) { $this->assist_root->setDefaultObjectNames($on); }
	protected function setObjectNames($names) { $this->assist_root->setObjectNames($names); }
	protected function setDefaultActivityNames($on) { $this->assist_root->setDefaultActivityNames($on); }
	protected function setActivityNames($names) { $this->assist_root->setActivityNames($names); }
	protected function setMenuNames($names) { $this->assist_root->setMenuNames($names); }
	protected function setAdditionalObjectNames($names) { $this->assist_root->setAdditionalObjectNames($names); }





	public function formatTextForComparison($text) {
		return $this->assist->formatTextForComparison($text);
	}


	public function formatTextForSorting($text) {
		return $this->assist->formatTextForSorting($text);
	}




	protected function isThisAnExtraAutoCompleteFieldName($fld) {
		$f = explode("_", $fld);
		if(is_array($f) && count($f) > 0 && $f[0] == "auto") {
			return true;
		}
		return false;
	}


	/*************************************************************************************************
	 * Old /inc_assist.php functions
	 */
	public function displayAuditLogOldStyle($sql,$flds,$extra) {
		if($sql!==false) {
			$logs = $this->mysql_fetch_all($sql);
		} else {
			$logs = array();
		}
		return $this->assist->displayAuditLogOldStyle($logs,$flds,$extra);
	}

	/**
	 * Function to create the array needed to run createLogOldStyle function #AA-663 [JC] 3 Aug 2021
	 * @param string $section - module section / object
	 * @param string $action - action taken (C=create, E=edit etc)
	 * @param string $id - id of record acted on
	 * @param string $fld - field changed
	 * @param string $trans - REQUIRED - description of action taken (displayed in audit log)
	 * @param string $old - original value
	 * @param string $new - new value
	 * @param string $sql - sql statement run
	 * @param string $table - full table name (including dbref) e.g. assist_cmpcode_modref_setup_log
	 */
	public function logMeOldStyle($section,$action,$id,$fld,$trans,$old,$new,$sql,$table) {
		$l = array(
			'sec'=>		$section,
			'ref'=>		$id,
			'action'=>	$action,
			'fld'=> 	$fld,
			'trans'=> 	$trans,
			'old'=>		$old,
			'new'=>		$new,
			'sql'=> 	addslashes($sql)
		);
		$this->createLogOldStyle($l,$table);
	}

	/**
	 * Function to create the log record in the database #AA-663 [JC] 3 Aug 2021
	 * @param array $l - array of details to be saved into the log table
	 * @param string $table - full table name (including dbref) e.g. assist_cmpcode_modref_setup_log
	 */
	public function createLogOldStyle($l,$table) {
		$tkid = $this->getUserID();
		$tkn = $this->getUserName();
		$sql = "INSERT INTO ".$table." 
				(id, date, tkid, tkname, section, ref, action, field, transaction, old, new, active, lsql)
				VALUES
				(null, now(), '$tkid', '$tkn', '".$l['sec']."', '".$l['ref']."', '".$l['action']."', '".$l['fld']."', '".$l['trans']."', '".$l['old']."', '".$l['new']."', true, '".$l['sql']."')";
		$this->db_insert($sql);
	}


	public function __destruct() {
		parent::__destruct();
	}

}


?>