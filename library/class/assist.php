<?php
/** BASE ASSIST CLASS
Created on: 26 August 2012
Created by: Janet Currie


IN PROGRESS

 * last edit:
 *
 * 	19/10/2015 14:00
 * 	DAC
 * 	Added list of MASTER-linked modules - $modules['masterLink']
 * 	Line: 42
 * 	Added function to check if a module is linked to MASTER modules - isMasterLinkModule()
 * 	Line: 233-5
 *
**/


class ASSIST extends ASSIST_HELPER {

	private $mod_ref;

	private $cmpcode;
	protected $cmp_code;
	private $cmp_name;

	private $tkid;
	protected $user_id;
	private $tkname;
	protected $user_name;
	private $tkuser;
	private $user_login_name;
	private $user_email;

	private $modules = array(
		'jean'=>array("PMSPS","PMS","PP","AQA"),
		'admire'=>array("RGSTR","QUERY","CMPL","COMPL","SCORECARD","CONTRACT","SLA"),
		'action'=>array("ACTION0","ACTION","CACTION","RGSTR","SLA","QUERY","RAPS","CAPS","COMPL","JOBS","SCORECARD","CONTRACT","CASEM","CNTRCT","RGSTR2","PDP","SDBP6","PDP1","SDBP5","SDBP5B","IKPI1","IKPI2","PM6","PDP2"),
		'mdoc'=>array("MASTER"),
		'masterLink'=>array("ACTION"),
		'contentsbar_have_no_title'=>array("TD"),
		'split'=>array("SDBP6","IKPI2"),
		'finteg'=>array("SDBP6"),
	);


	private $is_assist_help = array("IASSIST","PASSIST");
	private $report_save_path = "reports";
	private $delete_move_path = "deleted";

	private $modref;
	private $terms;
	private $ignite_name;
	private $title_logo;

	protected $cmp_db;

	protected $default_site_code = "actionassist";

	private $assist_admin_name = "Assist Administrator";



	public function __construct($autojob=false) {
		if(!$autojob) {
			//also used in library/assist_db->db_query function to reset inactivity timeout on long running scripts - AA-317 [JC] 3 Feb 2020
			if(isset($_SESSION['session_timeout']) && is_array($_SESSION['session_timeout'])) {
				$now = strtotime(date("d F Y H:i:s"));
				if($now<$_SESSION['session_timeout']['expiration']) {
					$_SESSION['session_timeout']['expiration'] = $now+$_SESSION['session_timeout']['setting'];
					$_SESSION['session_timeout']['current'] = $now;
				} else {
					die($this->autoLogout("TIMEOUT"));
				}
			}
			//GET SESSION INFO

			$this->cmp_code = isset($_SESSION['cc']) ? $_SESSION['cc'] : die($this->autoLogout("CC")); //COMPANY CODE
			$this->cmpcode = $this->cmp_code;
			$this->mod_ref = isset($_SESSION['ref']) ? $_SESSION['ref'] : "main";	//MODULE REFERENCE
			$this->cmp_name = isset($_SESSION['ia_cmp_name']) ? $_SESSION['ia_cmp_name'] : $this->getCMPNsess($this->cmp_code);	//CoMPany NAME
			$this->tkid = isset($_SESSION['tid']) ? $_SESSION['tid'] : die($this->autoLogout("TKI")); //die($this->autoLogout()); //USER ID
			$this->user_id = $this->tkid;
			if($this->tkid=="0000") {
				$this->tkuser = "admin";
				$this->tkname = isset($_SESSION['tkn']) ? trim($_SESSION['tkn']) : $this->getAssistAdminName();
				//$this->tkemail =
			} else {
				$this->tkuser = isset($_SESSION['tku']) ? trim($_SESSION['tku']) : $this->getTKDetails($this->tkid,$this->cmpcode,"tkuser");	//TKUSER
				$this->tkname = isset($_SESSION['tkn']) ? trim($_SESSION['tkn']) : $this->getTKDetails($this->tkid,$this->cmpcode,"tkn");	//TKNAME
				$this->user_email = $this->getTKDetails($this->tkid,$this->cmpcode,"tkemail");
			}
			$this->user_name = $this->tkname;
			$this->user_login_name = $this->tkuser;

			if(!isset($_SESSION['cmp_terminology'])) {
				$sql = "SELECT field, value FROM assist_".$this->cmp_code."_setup WHERE ref = 'IA_TERM'";
				$cmp_db = new ASSIST_DB();
				$ct = $cmp_db->mysql_fetch_fld2_one($sql,"field","value");
				if(count($ct)==0) {
					$sql2 = "INSERT INTO `assist_blank_setup` (`id`, `ref`, `refid`, `value`, `comment`, `field`) VALUES (null, 'IA_TERM', 0, 'Department', 'Name of Department', 'DEPT'), (null, 'IA_TERM', 0, 'Section', 'Name of Section', 'SUB')";
					$cmp_db->db_insert($sql2);
					$ct = $cmp_db->mysql_fetch_fld2_one($sql,"field","value");
				}
				$_SESSION['cmp_terminology'] = $ct;
			}




			$this->terms = $_SESSION['cmp_terminology'];

			$this->ignite_name = isset($_SESSION['DISPLAY_INFO']['ignite_name']) ? $_SESSION['DISPLAY_INFO']['ignite_name'] : "Action Assist";
			$this->title_logo = isset($_SESSION['DISPLAY_INFO']['title_logo']) && file_exists($_SESSION['DISPLAY_INFO']['title_logo']) ? $_SESSION['DISPLAY_INFO']['title_logo'] : "pics/assist_logo_h75.jpg";
			$this->site_code = isset($_SESSION['DISPLAY_INFO']['site_code']) && strlen($_SESSION['DISPLAY_INFO']['site_code'])>0 ? $_SESSION['DISPLAY_INFO']['site_code'] : "igniteassist";
		}
	}

	function autoLogout($code="") {
		if(isset($_REQUEST['check'])) {
			$url = (isset($_SERVER['HTTPS']) ? "https" : "http")."://".$_SERVER['SERVER_NAME']."/index.php";
		} else {
			$url = (isset($_SERVER['HTTPS']) ? "https" : "http")."://".$_SERVER['SERVER_NAME']."/logout.php?r=".base64_encode($code)."&check=Y";
		}
		echo "<html><head><title>www.Action4u.co.za</title>
				<link rel='stylesheet' href='/assist.css' type='text/css'>
				<script type='text/javascript'>
				<!--
				function delayer(){
					parent.location.href = '".$url."';
				}
				//-->
				</script>
			</head>
			<body onLoad=\"setTimeout('delayer()', 3000)\">
			<h1>Expiration Notice</h1>
			<p>Your session has expired.  Please login again.</p>
			<p>You will be redirected to the Login page momentarily.  If your browser does not redirect you, <a href='".$url."' target=_parent>click here</a>.</p>
			<script type=text/javascript>

			</script>
			</body></html>";
	}

	/**
	 * Function to get some user details from the database
	 * [JC - 27 April 2017] Added option to get the users' name as well as the selected field
	 *
	 * @param $id [STRING || ARRAY OF STRINGS] TKID of the user to be found
	 * @param $cc [STRING] Cmpcode of the client to which the TK belongs - needed as this function can be called before login process is complete
	 * @param $fld [STRING] TK Field to be looked up and returned
	 * @param $inc_name [BOOL] Include the User(s) name Yes/No
	 * @return $inc_name=true = array (tkid=> array( field + name )) ELSE if ID=ARRAY then array(tkid=>field) ELSE field
	 *
	 */

	function getTKDetails($id,$cc,$fld,$inc_name=false) {
		$cmp_db = new ASSIST_DB("client",$cc);
		if($fld=="tkn") { $fld2 = "CONCAT(tkname,' ',tksurname) as tkn"; } else { $fld2 = $fld; }
		if($inc_name && $fld!="tkn") { $fld2.= ", CONCAT(tkname,' ',tksurname) as tkn"; }
		if(is_array($id)) {
			if(count($id)==0) {
				//break here because a blank array has been sent
				return array();
			} else {
				//continue
			}
			$fld2.= ", tkid";
			$where = "tkid IN ('".implode("','",$id)."')";
		} else {
			$where = "tkid = '$id'";
		}
		$sql = "SELECT $fld2 FROM assist_".strtolower($cc)."_timekeep WHERE ".$where;
		if(is_array($id)) {
			if($inc_name) {
				$one = $cmp_db->mysql_fetch_all_by_id($sql, "tkid");
				if($fld=="tkemail") {
					foreach($one as $i => $x) {
						$one[$i]['email'] = $x['tkemail'];
					}
				}
			} else {
				$one = $cmp_db->mysql_fetch_value_by_id($sql, "tkid", $fld);
			}
		} else {
			if($inc_name) {
				$one = $cmp_db->mysql_fetch_one($sql);
				if($inc_name==true & $fld == "tkemail") {
					$one['email'] = $one['tkemail'];
				}
			} else {
				$one = $cmp_db->mysql_fetch_one_fld($sql,$fld);
			}
		}
		$cmp_db->db_close();
		return $one;
	}

	public function getUserRecord($t_id,$cc="") {
		$cmp_db = new ASSIST_DB("client",$cc);
		$where = false;
		if(is_array($t_id)) {
			$t_id = $this->removeBlanksFromArray($t_id,false);
			if(count($t_id)>0) {
				$where = "tkid IN ('".implode("','",$t_id)."')";
			}
		} else {
			$where = "tkid = '$t_id'";
		}
		if($where!==false) {
			$sql = "SELECT * FROM assist_".strtolower($cc)."_timekeep WHERE ".$where." ORDER BY tkname, tksurname";
			if(is_array($t_id)) {
				$one = $cmp_db->mysql_fetch_all_by_id($sql, "tkid");
			} else {
				$one = $cmp_db->mysql_fetch_one($sql);
			}
			$cmp_db->db_close();
			return $one;
		} else {
			return array();
		}
	}

	function getTKList($current=true,$modref="") {
		$cmp_db = new ASSIST_DB();
		$fld2 = "";
		$sql = "SELECT tkid as id, CONCAT(tkname,' ',tksurname) as value FROM assist_".$this->cmp_code."_timekeep ";
		if(strlen($modref)>0) {
			$sql.= " INNER JOIN assist_".$this->cmp_code."_menu_modules_users ON usrtkid = tkid AND usrmodref = '$modref' ";
		}
		if($current) {
			$sql.= " WHERE tkstatus = 1 ";
		}
		$sql.= " ORDER BY tkname, tksurname";
		$one = $cmp_db->mysql_fetch_array_by_id($sql, "id", "value");
		$cmp_db->db_close();
		return $one;
	}


	function getCMPNsess($cmpcode) {
		$cmp_db = new ASSIST_DB("master");
		$sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";
		$cmprow = $cmp_db->mysql_fetch_one($sql);
		//$rs = AgetRS($sql);
			//$cmprow = mysql_fetch_assoc($rs);
		//mysql_close();
		$_SESSION['ia_cmp_name'] = $cmprow['cmpname'];
		return $cmprow['cmpname'];
	}

	function getCmpAdmin($cmpcode) {
		$cmp_db = new ASSIST_DB("master");
		$sql = "SELECT cmpadmin, cmpadminemail FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";
		return $cmp_db->mysql_fetch_one($sql);
	}

	function getUserID() {
		return $this->user_id;
	}
	function getActualUserID() {
		return $this->user_id;
	}
	function getActualUserName() {
		return $this->user_name;
	}

	//Added option to get another user's login name - needed by isSupportUser #AA-676 [JC] 18 August 2021
	function getLoginName($user_id = "") {
		if(strlen($user_id) == 0) {
			return $this->tkuser;
		} else {
			$cmp_db = new ASSIST_DB();
			$sql = "SELECT tkuser FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$user_id."'";
			$row = $cmp_db->mysql_fetch_one($sql);
			unset($cmp_db);
			if(isset($row['tkuser']))
				return $row['tkuser'];
			else
				return false;
		}
	}
	function getUserName() {
		return $this->user_name;
	}
	function getUserEmail() {
		return $this->user_email;
	}
	function getCmpCode() {
		return $this->cmp_code;
	}
	function getCmpDisplayName() {
		return $_SESSION['ia_cmp_dispname'];
	}
	function getCmpLogo() {
		return $_SESSION['ia_cmp_logo'];
	}
	function getCmpName() {
		return $_SESSION['ia_cmp_name'];
	}
	function getModRef() {
		return isset($_SESSION['modref']) ? $_SESSION['modref'] : "";
	}
	function getModLocation() {
		return $_SESSION['modlocation'];
	}

	function doPasswordSecurityAnswersPassInspection() {
		if($_SESSION['pwd_security_check']=="passed") {
			return true;
		}
		return false;
	}
	function haveISeenTheFPDYetToday() {
		return $_SESSION['fpd_displayed'];
	}
	function IveSeenTheFPD() {
		$_SESSION['fpd_displayed'] = true;
	}

	//Added option to test a different user for Support user - needed for PM6 #AA-676 [JC] 18 August 2021
	function isSupportUser($user_id = "") {
		if(strlen($user_id) == 0 || $user_id == $this->tkid) {
		if($this->tkuser == "support")
			return true;
		else
			return false;
		} elseif($user_id == "0000") {
			return false; //0000 = admin user so can't be support user
		} else {
			$user = $this->getLoginName($user_id);
			if($user !== false && $user == "support") {
				return true;
			} else {
				return false;
			}
		}
	}
	function getSupportUserID() {
		$cmp_db = new ASSIST_DB();
		$sql = "SELECT * FROM assist_".$cmp_db->getCmpCode()."_timekeep WHERE tkuser = 'support'";
		$support = $cmp_db->mysql_fetch_one($sql);
		return $support['tkid'];
	}

	function isActiveReseller($c="") {
		return $_SESSION['ia_reseller'];
		//return false;
	}

	function isDemoCompany($c="") {
		if(strlen($c)==0) { $c = $this->getCmpCode(); }
		if(strtoupper($c)=="IASSIST" || strtoupper($c)=="DEMORES") {
			return false;
		} else {
			$check_character = substr($c, -1);
			if(!is_numeric($check_character)) {
				if($check_character=="t") {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}
	}

	function isAssistHelp($c="") {
		if(strlen($c)==0) { $c = $this->getCmpCode(); }
		return in_array(strtoupper($c),$this->is_assist_help);
	}

	function isAdminUser($test_user_id=false) {
		//if a test user id has not been sent then assume testing logged in user
		if($test_user_id===false) {
		if($this->tkuser == "admin" && $this->tkid == "0000")
			return true;
		else
			return false;
		} else {
			if($test_user_id=="0000") {
				return true;
			} else {
				return false;
			}
		}
	}
	function getAssistAdminName() { return $this->assist_admin_name; }

	function getAdminSystemMenu() {
		$menu = array(
			'S'=>"Master Setup",
			'TK'=>"Users",
			'reports'=>"System Reports",
			'dashboard_management'=>"Dashboard Management",
		);
//			'module_management'=>"Module Management",
		return $menu;
	}
	function getUserSystemMenu() {
		$menu = array(
			'user_profile'=>"My Profile",
			'changepass'=>"Change Password",
		);
		return $menu;
	}
	function getSystemMenu() {
		$menu = array(
			'userman'=>"User Manuals",
		);
		return $menu;
	}
	function getSplitModules() {
		return $this->modules['split'];
	}
	function getActionModules() {
		return $this->modules['action'];
	}
	function isSplitModule($modloc) {
		return in_array($modloc,$this->modules['split']);
	}
	function getFintegModules() {
		return $this->modules['finteg'];
	}
	function isFintegModule($modloc) {
		return in_array($modloc,$this->modules['finteg']);
	}
	function getJeanModules() {
		return $this->modules['jean'];
	}
	function isIASClient() {
		if(isset($_SESSION['bpa'])) {
			return strtoupper($_SESSION['bpa'])==strtoupper("IGN0001");
		} else {
			return false;
		}
	}
    function isMdocModule($modloc){
        return in_array($modloc,$this->modules['mdoc']);
    }

    function isMasterLinkModule($modloc){
        return in_array($modloc,$this->modules['masterLink']);
    }

	function getContentsWithNoTitle() {
		return $this->modules['contentsbar_have_no_title'];
	}

	function getIgniteName() { return $this->getSiteName(); }	//Old function
	function getSiteName() { return $this->ignite_name; }
	function getIgniteTitleLogo() { return $this->title_logo; }
	function getSiteCode() {
		$dsc = isset($_SESSION['DISPLAY_INFO']['site_code']) ? $_SESSION['DISPLAY_INFO']['site_code'] : $this->default_site_code;

		return isset($this->site_code) ? $this->site_code : $dsc;
	}

	function getReportPath() { return $this->report_save_path; }

	function getModTitle($ref="") {
		if(strlen($ref)==0) {
			return $_SESSION['modtext'];
		} else {
			$am = $this->getAdminSystemMenu();
			if(isset($am[$ref]) && $this->isAdminUser()) {
				return $am[$ref];
			} else {
				$sql = "SELECT modtext FROM assist_menu_modules WHERE modref = '".$ref."'";
				$db = new ASSIST_DB("client");
				return $db->mysql_fetch_one_value($sql,"modtext");
			}
		}
	}

	function getAModLocation($ref) {
		if(strlen($ref)==0) {
			return $_SESSION['modtext'];
		} else {
			$am = $this->getAdminSystemMenu();
			if(isset($am[$ref]) && $this->isAdminUser()) {
				return $am[$ref];
			} else {
				$sql = "SELECT modlocation FROM assist_menu_modules WHERE modref = '".$ref."'";
				$db = new ASSIST_DB("client");
				return $db->mysql_fetch_one_value($sql,"modlocation");
			}
		}
	}

	function getTerm($i,$plural=false) {
		$t = $this->terms[$i];
		if($plural) {
			if(substr($t,-1)!="s") {
				$t.="s";
			} else {
				$t.="'";
			}
		}
		return $t;
	}
	function getPasswordSecurityAnswers() {
		$me = $this->getUserID();
		$db = new ASSIST_DB();
		$sql = "SELECT tkpwdanswer1, tkpwdanswer2, tkpwdanswer3 FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$me."'";
		return $db->mysql_fetch_one($sql);
	}


	function setSessionDetails($modref,$module) {
			$_SESSION[$modref] = array();

			$_SESSION['modref'] = strtoupper($modref);
			$this->setSessionModuleRef($modref);
			$_SESSION['modtext'] = $module['modtext'];
			$_SESSION['modlocation'] = $module['modlocation'];
			$dbref = "assist_".$this->getCmpCode()."_".$modref;
			$_SESSION['dbref'] = strtolower($dbref);
	}
	function setSessionModuleRef($modref) {
		$_SESSION['ref'] = $modref;
	}
	function logUserActivity($modref) {
		$db = new ASSIST_DB();
		if(isset($_COOKIE['PHPSESSID']) && strlen($_COOKIE['PHPSESSID'])) {
			$unique_id = "PHPSESSID=".$_COOKIE['PHPSESSID'];
		} elseif(isset($_SERVER["HTTP_COOKIE"]) && strlen($_SERVER["HTTP_COOKIE"])) {
			$unique_id = $_SERVER["HTTP_COOKIE"];
		} else {
			$unique_id = "";
		}

		$sql = "INSERT INTO assist_".$this->getCmpCode()."_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) VALUES (null,now(),'".$this->getUserID()."','".$this->getUserName()."','$modref',1,':".($unique_id).":|".$_SERVER["HTTP_USER_AGENT"]."','".$_SERVER['REMOTE_ADDR']."')";
		$db->db_insert($sql);
		unset($db);
	}
	function setModRef($mr) {
		$mr = strtolower($mr);
		$_SESSION['modref'] = $mr;
		$_SESSION['dbref'] = "assist_".$this->getCmpCode()."_".$mr;
	}


	function getAllMenuModules($withUsers = FALSE) {
		$data = array();
		$db = new ASSIST_DB();

		$sql = "SELECT mm.modref, mm.modlocation, mm.modtext
				FROM assist_menu_modules mm
				WHERE mm.modyn = 'Y' ORDER BY mm.modtext";

		$data = $db->mysql_fetch_all_by_id($sql,"modref");
        if($withUsers){
            $ref = array_keys($data);
            if(count($ref)>0){
                $sql = "SELECT mmu.usrmodref AS modref, mmu.usrtkid AS tkid FROM assist_".$db->getCmpCode()."_menu_modules_users mmu
                        INNER JOIN assist_".$db->getCmpCode()."_timekeep as tk ON tk.tkid = mmu.usrtkid AND tk.tkstatus = 1
                        WHERE mmu.usrmodref IN ('".implode("', '",$ref)."')
                ";
                $users = $db->mysql_fetch_array_by_id($sql, "tkid", "modref");
                $data['users'] = $users;
            }
        }
		return $data;
	}


	function getMenuModules() {
		$data = array();
		$db = new ASSIST_DB();

		$sql = "SELECT mm.modref, mm.modlocation, mm.modtext
				FROM assist_menu_modules mm
				".($this->isAdminUser() ? "" : "INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users mmu
				  ON mm.modref = mmu.usrmodref
				  AND mmu.usrtkid = '".$this->getUserID()."'")."
				WHERE mm.modyn = 'Y' ".($this->isAdminUser() ? " OR mm.modadminyn = 'Y' " : "")." ORDER BY mm.modtext";

		$data = $db->mysql_fetch_all_by_id($sql,"modref");
		return $data;
	}

	function getModuleDataFromCode($code){
		$data = array();
		$db = new ASSIST_DB();
		$sql = "SELECT mm.modref, mm.modlocation, mm.modtext
				FROM assist_menu_modules mm
				WHERE mm.modyn = 'Y' AND mm.modref = '".$code."' ORDER BY mm.modtext";

		$data = $db->mysql_fetch_all_by_id($sql,"modref");
		return $data;
	}

	function getOldUserManuals() {
		$adb = new ASSIST_DB("master");
		$data = array();
		$sql = "SELECT * FROM assist_manuals_docs WHERE docyn = 'Y' ".(($this->isAdminUser()) ? " OR docyn = 'A'" : "")." ORDER BY doctitle";
		$rows = $adb->mysql_fetch_all($sql);

			foreach($rows as $row) {
				$data['docs'][$row['docid']] = $row;
				if(strlen($row['docmodloc'])>0) {
					$data['location'][$row['docmodloc']][] = $row['docid'];
				}
				if(strlen($row['docmodref'])>0) {
					$data['ref'][$row['docmodref']][] = $row['docid'];
				}
			}
		return $data;
	}



	//PASSWORD FUNCTIONS
	function updateSecurityAnswers($user_id="", $answers) {
		$user_id = strlen($user_id)>0 ? $user_id : $this->getUserID();
		$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep SET ";
		$s = array();
		foreach($answers as $fld => $val) {
			$s[] = $fld." = '$val'";
		}
		$sql.= implode(", ",$s)." WHERE tkid = '".$user_id."'";
		$cdb = new ASSIST_DB();
		return $cdb->db_update($sql);
	}






	/********************
	 * Business Partner functions
	 */
/*	function getBPAdetails() {
		$bpa = $_SESSION['ia_cmp_reseller'];
		$adb = new ASSIST_DB("master");

		if(!isset($_SESSION['bpa_details'])) {
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r
					INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
					WHERE r.cmpcode = '".strtoupper($bpa)."'";
			$bpa_details = $adb->mysql_fetch_one($sql);
			//	$bpa_details = mysql_fetch_assoc($rs);
				$bpa_con = explode("|",$bpa_details['help_contact']);
				$bpa_contact = array();
				foreach($bpa_con as $b) {
					$b2 = explode("_",$b);
					$bpa_contact[] = $b2[1]." (".strtolower($b2[0]).")";
				}
				$bpa_details['contact'] = $bpa_contact;
				$_SESSION['bpa_details'] = $bpa_details;
			unset($rs);
		} else {
			$bpa_details = $_SESSION['bpa_details'];
		}
		return $bpa_details;
	}
	*/






	function __destruct() {

	}
}



?>