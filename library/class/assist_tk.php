<?php

class ASSIST_TK extends ASSIST_MODULE_HELPER {

	const ACTIVE = 2;
	const INACTIVE = 4;

	const SUPER_NA = 1024;
	const SUPER_CAN_ADMIN = 2048;
	const SUPER_CAN_CLIENT = 4096;
	const SUPER_CAN_RESELLER_CLIENT = 8192;

	private $super_user_permissions = array(
		1024=>array('text'=>"No additional special permissions",'client'=>true,'reseller'=>true,'iassist'=>true,'form'=>false),
		2048=>array('text'=>"Can login as Admin",'client'=>true,'reseller'=>true,'iassist'=>true,'form'=>true),
		4096=>array('text'=>"Can login as Client Super User (Admin/Support)",'client'=>false,'reseller'=>true,'iassist'=>true,'form'=>true),
		8192=>array('text'=>"Can login as Partner Client Super User (Admin/Support)",'client'=>false,'reseller'=>false,'iassist'=>true,'form'=>true),
	);

	private $object_user_id;

	private $super_user_log_code = "SUPUSER";
	private $pwd_log_code = "PL_PWD";
	private $att_log_code = "PL_ATT";
	private $inact_log_code = "PL_INACT";

	private $password_security_questions = array(
		1=> "What was your mother's maiden name?",
		2=> "What was the colour of your first car?",
		3=> "What was the name of your high school?",
	);

	private $password_settings = array();

	private $password_complexity_options = array(
		16 => "Lower case character",
		32 => "Upper case character",
		64 => "Number",
		128 => "Special character",
	);




	private $password_change_reasons = array(
		'user'=>"User Request (from main menu)",
		'age'=>"Password expired",
		'new'=>"First time login",
		'reset'=>"Reset password by admin",
		'force'=>"Forced password reset by admin",
		'convert'=>"Convert Non-user Employee to Active Assist User",
	);
	private $password_change_reason;
	private $redirect_password_reasons = array("force","age","new");


	private $password_change_request_sources = array(
		'email'=>"Email",
		'tel'=>"Telephone",
		'visit'=>"In person",
		'help'=>"Helpdesk request",
		'msg'=>"SMS / Messenging apps",
		'other'=>"Other",
		'self'=>"Own password change",
	);


	private $table_name = "_timekeep";
	private $have_row = false;
	private $user_record;



	public function __construct($ui="",$get_row=false) {
		parent::__construct();
		$this->object_user_id = strlen($ui)==0 ? $this->getUserID() : $ui;
		$this->table_name = "assist_".$this->getCmpCode().$this->table_name;
		if($get_row) {
			$this->getUserRecord(false);
		}

	}

	public function getPasswordChangeRequestSources() { return $this->password_change_request_sources; }

	private function getTableName() {
		return $this->table_name;
	}

	private function setObjectUserID($ui="") {
		$this->object_user_id = strlen($ui)==0 ? $this->getUserID() : $ui;
		if($this->have_row) {
			$t = $this->getUserData("tkid");
			if($t!=$this->object_user_id) {
				$this->getUserRecord(false,true);
			}
		}
	}

	public function getUserRecord($return=true,$force_update=false) {
		if(!$this->have_row || $force_update==true) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE tkid = '".$this->object_user_id."'";
			$this->user_record = $this->mysql_fetch_one($sql);
			$this->have_row = true;
		}
		if($return) {
			return $this->user_record;
		}
	}
	private function getUserData($field) {
		if(!$this->have_row) {
			$this->getUserRecord(false);
		}
		return $this->user_record[$field];
	}

	public function getUserEmail($var=array()) {
		if(isset($var['tkid'])) {
			$this->setObjectUserID($var['tkid']);
		}
		$email = $this->getUserData("tkemail");
				//$email = $this->getAnEmail($var['tkid']);
		return $this->validateUserEmail($email);
	}
	public function getObjectUserEmail() {
		$email = $this->getUserData("tkemail");
		return $email;
	}
	public function validateUserEmail($email) {
		if(is_array($email)) { $email = $email['email']; }
		//$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		$allowed_local_chars = array(
			"!", "#", "$", "%", "&", "'", "*", "+", "-", "/", "=", "?", "^", "_", "`", "{", "|", "}", "~", ".",
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
			"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		);
		$allowed_domain_chars = array(
			"-", ".",
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
			"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		);
		$err = false;
		$em = explode("@",$email);
		if(count($em)!=2 || strlen($em[0])==0 || strlen($em[1])==0) {
			$err = true;
		} else {
			if(substr($em[0],0,1)==".") { $err = true; }
			if(substr($em[1],strlen($em[1])-1,1)==".") { $err = true; }
		}
		if(!$err) {
			for($i=0;$i<strlen($em[0]);$i++) {
				if(!in_array(strtolower(substr($em[0],$i,1)),$allowed_local_chars)) {
					$err = true;
					break;
				}
			}
		}
		if(!$err) {
			for($i=0;$i<strlen($em[1]);$i++) {
				if(!in_array(strtolower(substr($em[1],$i,1)),$allowed_domain_chars)) {
					$err = true;
					break;
				}
			}
		}

		if($err===false && filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			return array("ok","Notification will be sent to:<br />$email");
		} else {
			return array("error","Warning! Invalid email address! <br /> $email appears to be invalid.");
		}
	}




	public function getPasswordLoginLogs() {
		return $this->getLogs(array("PL_PWD","PL_ATT","PL_INACT"));
	}
	public function getPasswordLoginLogsHTML() {
		return $this->getLogHTML(array("PL_PWD","PL_ATT","PL_INACT"));
	}

	/**************************************
	 * Password functions
	 */
	public function getPasswordSettings($code = "") {
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_timekeep_setup WHERE code ".(strlen($code)>0 ? " = '$code'" : "LIKE 'pwd_%'");
		if(strlen($code)>0) {
			return $this->mysql_fetch_one($sql);
		}
		return $this->mysql_fetch_all_by_id($sql,"code");
	}
	public function getPasswordComplexityOptions() {
		return $this->password_complexity_options;
	}
	public function savePasswordSetting($var) {
		$code = $var['code'];
		$old = $this->getPasswordSettings($code);
		$result = $this->savePasswordLoginSetting($var,$this->pwd_log_code,$old);
		if($result[0]=="ok") {
			$result[1] = "Password Setting ".$result[1];
		}
		return $result;
	}

	/**************************************
	 * Login Attempts functions
	 */
	public function getAttemptSettings($code = "") {
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_timekeep_setup WHERE code ".(strlen($code)>0 ? " = '$code'" : "LIKE 'att_%'");
		if(strlen($code)>0) {
			return $this->mysql_fetch_one($sql);
		}
		return $this->mysql_fetch_all_by_id($sql,"code");
	}
	public function saveAttemptSetting($var) {
		$code = $var['code'];
		$old = $this->getAttemptSettings($code);
		$result = $this->savePasswordLoginSetting($var,$this->att_log_code,$old);
		if($result[0]=="ok") {
			$result[1] = "Login Attempt Setting ".$result[1];
		}
		return $result;
	}

	/**************************************
	 * Inactivity functions
	 */
	public function getInactivitySettings($code = "") {
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_timekeep_setup WHERE code ".(strlen($code)>0 ? " = '$code'" : "LIKE 'inact_%'");
		if(strlen($code)>0) {
			return $this->mysql_fetch_one($sql);
		}
		return $this->mysql_fetch_all_by_id($sql,"code");
	}
	public function saveInactivitySetting($var) {
		$code = $var['code'];
		$old = $this->getInactivitySettings($code);
		$result = $this->savePasswordLoginSetting($var,$this->inact_log_code,$old);
		if($result[0]=="ok") {
			$result[1] = "User Inactivity Setting ".$result[1];
		}
		return $result;
	}


	/**************
	 * General
	 */
	public function savePasswordLoginSetting($var,$log_code,$old) {
		$code = $var['code'];
		$normal = $var['normal_user'];
		$super = $var['super_user'];
		$change = array();
		if($normal!=$old['normal_user']) {
			$change[] = "normal_user = $normal";
		} else {
			$normal=false;
		}
		if($super!=$old['super_user']) {
			$change[] = "super_user = $super";
		} else {
			$super=false;
		}
		$super_users = $this->getSuperUsers(true,true);
		if(count($change)>0) {
			$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep_setup SET ".implode(", ",$change)." WHERE code = '$code' ";
			$this->db_update($sql);
			if($normal!==false) {
				//log - normal
				if($old['form_type']=="BOOL") {
					$old['normal_user'] = ($old['normal_user']==1 ? "Yes" : "No");
					$normal = ($normal==1 ? "Yes" : "No");
				}
				$this->addLog(0,$log_code,"E",$code."_normal","Changed Normal User setting for ".$old['name']." to '".$normal."' from '".$old['normal_user']."'",$old['normal_user'],$normal,$sql);
				if($code=="pwd_age") {
					$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep SET tkpwddate = now() WHERE ".(count($super_users)>0 ? "tkid NOT IN ('".implode("','",$super_users)."') AND " : "")." tkpwddate = '0000-00-00 00:00:00' AND tkstatus = 1";
					$this->db_update($sql);
				}
			}
			if($super!==false) {
				//log - super
				if($old['form_type']=="BOOL") {
					$old['super_user'] = ($old['super_user']==1 ? "Yes" : "No");
					$super = ($super==1 ? "Yes" : "No");
				}
				$this->addLog(0,$log_code,"E",$code."_super","Changed Super User setting for ".$old['name']." to '".$super."' from '".$old['super_user']."'",$old['super_user'],$super,$sql);
				if($code=="pwd_age") {
					$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep SET tkpwddate = now() WHERE ".(count($super_users)>0 ? "tkid IN ('".implode("','",$super_users)."') AND " : "")." tkpwddate = '0000-00-00 00:00:00' AND tkstatus = 1";
					$this->db_update($sql);
				}
			}
			//get log table html
			$html = $this->getLogHTML("PL_%");
			return array("ok","updated successfully.",$html);
		}
		return array("info","No changes were found to be saved.");
	}















	/*****************************************
	 * GENERAL FUNCTIONS
	 */

	public function isActiveUser() {
		$sql = "SELECT tkstatus FROM ".$this->getTableName()." WHERE tkid = '".$this->user_id."'";
		$row = $this->mysql_fetch_one($sql);
		$status = $row['tkstatus'];

		if($status == 1) {
			return true;
		}
		return false;
	}

	public function getObjectUserName() {
		return $this->getUserData("tkname")." ".$this->getUserData("tksurname");
	}














	/*****************************************
	 * Super User functions
	 */
	public function getSuperUserPermissionsFormatted($limited=true) {
		$permissions = array();
		if($limited==true) {
			$p = array();
			foreach($this->super_user_permissions as $key => $x) {
				if($x['form']==true) {
					if($x['client']==true) {
						$p[$key] = $x;
					} elseif($x['reseller']==true && $_SESSION['ia_reseller']==true) {
						$p[$key] = $x;
					} elseif($x['iassist']==true && strtolower($_SESSION['cc'])=="iassist") {
						$p[$key] = $x;
					}
				}
			}
		} else {
			$p = $this->super_user_permissions;
		}
		foreach($p as $key => $x) {
			$permissions[$key] = $x['text'];
		}
		return $permissions;
	}



	public function isSuperUser($tkid="") {
		if(strlen($tkid)==0) {
			$tkid = $this->user_id;
		}
		$sql = "SELECT count(id) as c FROM assist_".$this->getCmpCode()."_timekeep_superusers WHERE tkid = '$tkid' AND (status & ".self::ACTIVE.") = ".self::ACTIVE;
		$c = $this->mysql_fetch_one_value($sql, "c");

		if($c>0) {
			return true;
		}
		return false;
	}
	/**
	 * Return list of all Active users who don't have Super User permission
	 * @return (Array) array('tkid'=>"tkname tksurname")
	 */
	public function getAvailableUsersForSuperUser() {
		$sql = "SELECT TK.tkid as id, CONCAT(TK.tkname,' ',TK.tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep TK
				WHERE TK.tkstatus = 1
				AND TK.tkid NOT IN (SELECT tkid FROM assist_".$this->getCmpCode()."_timekeep_superusers)
				AND TK.tkuser <> 'support'
				AND TK.tkid <> '0000'
				ORDER BY TK.tkname, TK.tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
	}

	public function getSuperUsers($active_only=false,$id_only=false) {
		$sql = "SELECT SU.tkid as id, CONCAT(TK.tkname,' ',TK.tksurname) as name, SU.status
				FROM assist_".$this->getCmpCode()."_timekeep_superusers SU
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK
				  ON TK.tkid = SU.tkid AND TK.tkstatus = 1
				".($active_only ? " WHERE SU.status = ".self::ACTIVE : "")."
				ORDER BY TK.tkname, TK.tksurname";
		$results = $this->mysql_fetch_all_by_id($sql, "id");
		//$this->arrPrint($results);
		foreach($results as $rkey => $r) {
			$results[$rkey]['permissions'] = array();
			foreach($this->super_user_permissions as $key => $s) {
				if(($r['status'] & $key)==$key) {
					$results[$rkey]['permissions'][] = $key;
				}
			}
		}
		if($id_only) {
			return array_keys($results);
		} else {
		return $results;
		}
	}

	public function AddSuperUser($var) {
		$u = $var['tkid'];
		$p = isset($var['permissions']) ? $var['permissions'] : array();
		$p[] = self::ACTIVE;
		$status = array_sum($p);
		$sql = "INSERT INTO assist_".$this->getCmpCode()."_timekeep_superusers SET
				tkid = '$u',
				status = ".$status.",
				insertdate = now(),
				insertuser = '".$this->getActualUserID()."'
		";
		$this->db_insert($sql);
		$this->addLog($u,"SUPUSER","C","superuser","Added ".$this->getAUserName($u)." as a Super User","",self::ACTIVE,$sql);
		return array("ok","Super User added successfully.");
	}

	public function editSuperUser($var) {
		$u = $var['edit_tkid'];
		$old = $this->getSuperUserForEdit(array('tkid'=>$u));
		$p = isset($var['edit_permissions']) ? $var['edit_permissions'] : array();
		$p[] = self::ACTIVE;
		$status = array_sum($p);
		if($status != $old['status']) {
			$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep_superusers SET
					status = ".$status."
					WHERE tkid = '$u'
			";
			$this->db_insert($sql);

			$added = array();
			foreach($this->super_user_permissions as $key => $s) {
				if(($status & $key)==$key && !(($old['status']&$key)==$key)) {
					$added[] = $s['text'];
				}
			}
			$removed = array();
			foreach($this->super_user_permissions as $key => $s) {
				if(($old['status'] & $key)==$key && !(($status&$key)==$key)) {
					$removed[] = $s['text'];
				}
			}
			$transaction = "Edited ".$this->getAUserName($u).":";
			if(count($added)>0 || count($removed)>0) {
				if(count($added)>0) {
					$transaction.=" Added permission '".implode("' & '",$added)."';";
				}
				if(count($removed)>0) {
					$transaction.=" Removed permission '".implode("' & '",$removed)."';";
				}
			}
			//$transaction = ASSIST_HELPER::code($transaction);
			$this->addLog($u,"SUPUSER","E","superuser",$transaction,$old['status'],$status,$sql);
			return array("ok","Super User saved successfully.");
		} else {
			return array("info","No changes found to be saved.");
		}
	}

	public function deactivateSuperUser($var) {
		$u = $var['tkid'];
		$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep_superusers SET
				status = (status + ".self::INACTIVE." - ".self::ACTIVE.")
				WHERE tkid = '$u'";
		$this->db_update($sql);
		$this->addLog($u,"SUPUSER","D","superuser","Deactivated ".$this->getAUserName($u)." as a Super User",self::ACTIVE,self::INACTIVE,$sql);
		return array("ok","Super User deactivated successfully.");
	}

	public function restoreSuperUser($var) {
		$u = $var['tkid'];
		$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep_superusers SET
				status = (status - ".self::INACTIVE." + ".self::ACTIVE.")
				WHERE tkid = '$u'";
		$this->db_update($sql);
		$this->addLog($u,"SUPUSER","R","superuser","Restored ".$this->getAUserName($u)." as a Super User",self::INACTIVE,self::ACTIVE,$sql);
		return array("ok","Super User restored successfully.");
	}

	public function getSuperUserLogs() {
		return $this->getLogs("SUPUSER");
	}

	public function getSuperUserForEdit($var) {
		$u = $var['tkid'];
		$sql = "SELECT SU.tkid as id, CONCAT(TK.tkname,' ',TK.tksurname) as name, SU.status
				FROM assist_".$this->getCmpCode()."_timekeep_superusers SU
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK
				  ON TK.tkid = SU.tkid AND TK.tkstatus = 1
				WHERE SU.tkid = '$u'
				ORDER BY TK.tkname, TK.tksurname";
		$su = $this->mysql_fetch_one($sql);
		$su['permissions'] = array();
		foreach($this->super_user_permissions as $key => $s) {
			if(($su['status'] & $key)==$key) {
				$su['permissions'][] = $key;
			}
		}
		return $su;
	}






	/**************************************
	 * LOGGING
	 */
	public function addLog($id,$type,$act,$fld,$trans,$old,$new,$sql) {
		$sql2 = "INSERT INTO assist_".$this->getCmpCode()."_timekeep_log
				(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
				VALUES
				(now(),'".$this->getActualUserID()."','".$this->getActualUserName()."','".$type."','".$id."','".$act."','".$fld."','".ASSIST_HELPER::code($trans)."','".$old."','".$new."',1,'".base64_encode($sql)."')";
		$this->db_insert($sql2);
	}

	public function getLogs($type) {
		$sql = "SELECT date, transaction as activity FROM assist_".$this->getCmpCode()."_timekeep_log WHERE ";
		if(is_array($type)) {
			$sql.=" section IN ('".implode("','",$type)."') ";
		} elseif(strpos($type,"%")!==false) {
			$sql.=" section LIKE '$type' ";
		} else {
			$sql.= "section = '$type' ";
		}
		$sql.= "AND active = 1 ORDER BY date DESC";
		return $this->mysql_fetch_all($sql);
	}

	public function getLogHTML($type) {
		$logs = $this->getLogs($type);
		$html = "
		<h3>Activity Log</h3>
	<table width=500px>
		<tr>
			<th width=150px>Date</th>
			<th>Activity</th>
		</tr>";
		if(count($logs)>0) {
			foreach($logs as $l) {
				$html.= "
				<tr>
					<td class=center>".$l['date']."</td>
					<td>".$l['activity']."</td>
				</tr>";
			}
		} else {
			$html.= "<tr><td colspan=2>No activity logs found.</td></tr>";
		}
		$html.="</table>";
		return $html;
	}













	/******************************************
	 * Password Change Form Functions
	 */
	public function getLockedStatus($var) {
		$tkid = $var['tkid'];
		$sql = "SELECT * FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$tkid."'";
		$row = $this->mysql_fetch_one($sql);
		if($row['tklock']=="Y") {
			//manually locked by admin
			if($row['tklocktime']=="0000-00-00 00:00:00" || $row['tklocktime']=="0" || strlen($row['tklocktime'])==0 || strtotime($row['tklocktime'])==0) {
				return "admin_locked";
			} else { //auto locked by password failure
				return "pwd_locked";
			}
		} else {
			return "unlocked";
		}
	}
	public function setPasswordChangeReason($r) {
		if(strlen($r)==0) { $r = "user"; }
		$this->password_change_reason = $r;
	}
	public function getPasswordChangeReason() { return $this->password_change_reason; }
	private function isRedirectPasswordChangeReason() { if(in_array($this->getPasswordChangeReason(),$this->redirect_password_reasons)) { return true; } else { return false; } }

	public function doDisplaySecurityQuestions() {
		if($this->password_change_reason=="new" || $this->password_change_reason=="user") {
			return true;
		}
		return false;
	}

	public function isRootFolder() {
		if($this->password_change_reason=="new" || $this->password_change_reason=="force" || $this->password_change_reason=="age") {
			return true;
		}
		return false;
	}

	public function echoPasswordChangeForm($tkid="",$hsection=array(),$headings=array()) {
		$display = $this->getPasswordChangeForm($tkid,$hsection,$headings);
		echo $display['display'];
		return $display['js'];
	}

	public function getPasswordChangeForm($tkid="",$hsection=array(),$headings=array()) {
		$display = "";
		$js = "";
		$password_settings = $this->getPasswordSettings();
		$is_superuser = $this->isSuperUser(strlen($tkid)==0 ? "" : $tkid);
		$displayObject = new ASSIST_MODULE_DISPLAY();
		$new = $displayObject->getMediumInputText("",array('id'=>"new",'name'=>"new",'type'=>"password"),50,30);
		$repeat = $displayObject->getMediumInputText("",array('id'=>"repeat",'name'=>"repeat",'type'=>"password"),50,30);
		if($this->getPasswordChangeReason()=="reset") {
			//BUTTONS
			$prop=array('form'=>"horizontal",'id'=>"notify");
			$notify = $displayObject->getBoolButton(1,$prop);
			$notify['display']="<label id=lbl_notify for=notify class='right float'></label>".$notify['display'];
			$js.= $notify['js'];	//this must belong to first BOOL button generated
			$prop=array('form'=>"horizontal",'id'=>"id_validated");
			$user_identity_validated = $displayObject->getBoolButton(0,$prop);
			$prop=array('form'=>"horizontal",'id'=>"force");
			$force = $displayObject->getBoolButton(1,$prop);
			$prop=array('form'=>"horizontal",'id'=>"unlock");
			$unlock = $displayObject->getBoolButton(0,$prop);
			//SELECT
			$sql = "SELECT tk.tkid as id, CONCAT(tkname,' ',tksurname,IF(su.tkid IS NULL, '', '*')) as value FROM assist_".$this->getCmpCode()."_timekeep tk LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep_superusers su ON su.tkid = tk.tkid AND su.status & 2 = 2 WHERE tkstatus = 1 AND tkuser<>'admin' AND tkuser<>'support' ORDER BY tkname, tksurname";
			$items = $this->mysql_fetch_value_by_id($sql, "id", "value");
			$prop = array('id'=>"user_id",'unspecified'=>false);
			$prop['name'] = $prop['id'];
			$user_list = $displayObject->getSelect("",$prop,$items);
			$js.=$user_list['js'];
			$prop = array('id'=>"request_sources");
			$prop['name'] = $prop['id'];
			$change_source_items = $this->password_change_request_sources;
			unset($change_source_items['self']);
			$change_request_sources = $displayObject->getSelect("",$prop,$change_source_items);
			$change_request_sources['display'].=" <input type=text id=request_sources_other name=request_sources_other size=50 maxlength=75 />";
			$js.="
			$('#request_sources_other').hide();
			$('#request_sources').change(function() {
				if($(this).val()=='other') {
					$('#request_sources_other').show();
				} else {
					$('#request_sources_other').val('');
					$('#request_sources_other').hide();
				}
			});

			";
			$prop = array('id'=>"id_validation_sources",'unspecified'=>false);
			$prop['name'] = $prop['id']."[]";
			$validation_items = array();
			$not_available = array(
				"tkstatus",
			);
			foreach($hsection as $key => $s) {
				if($key!="sys") {
					foreach($headings[$key] as $hkey => $fld) {
						if(!in_array($hkey,$not_available)) {
							$validation_items[$hkey] = $fld['txt'];
						}
					}
				}
			}
			$security_questions = $this->getPasswordSecurityQuestions();
			foreach($security_questions as $qkey => $q) {
				$validation_items['security_'.$qkey] = "Security question: ".$q;
			}
			$id_validation_sources = $displayObject->getMultipleSelect(array(),$prop,$validation_items);
			//OTHER
			$current = array('display'=>"<input type=hidden value='0' name=current id=current />",'js'=>"");
			$password_settings_field = array("normal_user","super_user");
			$locked = array('display'=>"<input type=hidden value='N' name=locked id=locked />",'js'=>"");
		} else {
			$notify = array('display'=>"<input type=hidden value='N' name=notify id=notify />",'js'=>"");
			$unlock = array('display'=>"<input type=hidden value='N' name=unlock id=unlock />",'js'=>"");
			$locked = array('display'=>"<input type=hidden value='N' name=locked id=locked />",'js'=>"");
			$user_identity_validated = array('display'=>"<input type=hidden value='Y' name=id_validated id=id_validated />",'js'=>"");
			$force = array('display'=>"<input type=hidden value='N' name=force id=force />",'js'=>"");
			$user_list = array('display'=>"<input type=hidden value='".$this->getUserID()."' name=user_id id=user_id />");
			$change_request_sources = array('display'=>"<input type=hidden value='SELF' name=request_sources id=request_sources />",'js'=>"");
			$id_validation_sources = array('display'=>"<input type=hidden value='X' name=id_validation_sources id=id_validation_sources />",'js'=>"");
			if($this->getPasswordChangeReason()=="user") {
				$current = $displayObject->getMediumInputText("",array('id'=>"current",'name'=>"current"),50,30);
			} else {
				$current = array('display'=>"<input type=hidden value='0' name=current id=current />");
				$current['js'] = "";
			}
			if($is_superuser) {
				$password_settings_field = array("super_user");
			} else {
				$password_settings_field = array("normal_user");
			}
		}
		$reason_display = array('display'=>"<input type=hidden value='".$this->getPasswordChangeReason()."' name=reason id=reason />");
		$display = "
<form name=frm_change>
	".$reason_display['display']."
	";
	if($this->getPasswordChangeReason()!="reset") {
		$display.=$notify['display']
				.$force['display']
				.$user_list['display']
				.$change_request_sources['display']
				.$user_identity_validated['display']
				.$id_validation_sources['display']
				.$unlock['display']
				.$locked['display']
				;
	} else {
		$display.=$current['display']
				.$locked['display'];
	}
	$display.="
	<table id=tbl_change class=form>
		".($this->getPasswordChangeReason()=="reset" ? "
		<tr>
			<th>User:</th>
			<td>".$user_list['display']."</td>
		</tr>" : "")."
		".($this->getPasswordChangeReason()=="user" ? "
		<tr>
			<th>Current Password:</th>
			<td>".$current['display']."</td>
		</tr>" : "")."
		<tr>
			<th width=160px>New Password:</th>
			<td>".$new['display']."</td>
		</tr>
		<tr>
			<th>Repeat New Password:</th>
			<td>".$repeat['display']."</td>
		</tr>
		".($this->getPasswordChangeReason()=="reset" ? "
		<tr>
			<th>Source of Reset request:</th>
			<td>".$change_request_sources['display']."</td>
		</tr>
		<tr>
			<th>User identiy validated?:</th>
			<td>".$user_identity_validated['display']."</td>
		</tr>
		<tr>
			<th>Identity validated against:</th>
			<td>".$id_validation_sources['display']."</td>
		</tr>
		<tr id=tr_unlock>
			<th>Unlock locked user?:</th>
			<td><span id=spn_btn_locked>".$unlock['display']."</span><span id=spn_lbl_locked></span></td>
		</tr>
		<tr>
			<th>Notify user via email?:</th>
			<td>".$notify['display']."</td>
		</tr>
		<tr>
			<th>Force password change on next login?:</th>
			<td>".$force['display']."</td>
		</tr>
				" : "")."
		".($this->getPasswordChangeReason()!="new" ? "<tr>
			<th></th>
			<td><button id=btn_change class='btn_green btn_save'>Save</button></td>
		</tr>" : "")."
		<tr>
			<th>Requirements:</th>
			<td>".(count($password_settings_field)>1 ? "<ul>" : "");
foreach($password_settings_field as $psf) {
	if($this->getPasswordChangeReason()=="reset") {
		$x = explode("_",$psf);
		$display.="<li><span class=b>".($psf=="super_user"?"*":"").ucwords(implode(" ",$x))."</span><ol>";
	} elseif($psf == "super_user") {
		$display.="<p class=i style='margin-top:0px;padding-top:0px;'>As a Super User you have stricter restrictions on your password.</p><ul>";
	} else {
		$display.="<ul>";
	}
	foreach($password_settings as $code => $p) {
		$v = $p[$psf];
		switch($code) {
			case "pwd_history":
				$display.="<li>".$p['name'].": ";
				if($v>1) {
					$display.="Your ".$v." most recent passwords cannot be used.";
				} elseif($v>0) {
					$display.="Your most recent password cannot be used.";
				} else {
					$display.="No restriction on password reuse.";
				}
				$display.= "</li>";
				break;
			case "pwd_complexity":
				$display.="<li>".$p['name'].": ";
				if($v>0) {
					$display.=" Password must contain at least 1 UPPERcase letter, 1 lowercase letter, 1 number and a printable special character e.g. (~!@#$%^&*()_+-=[];'\\,./{}:\"|<>?).  Note that spaces and extended ASCII characters are not permitted.";
				} else {
					$display.=" Password must contain at least 1 UPPERcase letter, 1 lowercase letter and 1 number.";
				}
				$display.= "</li>";
				break;
			case "pwd_age":
				if($this->getPasswordChangeReason()!="reset") {
					$display.="<li>".$p['name'].": ";
					if($v>0) {
						$display.="Your password will expire every ".$v." ".$p['form_unit'];
					} else {
						$display.="Your password will not expire.";
					}
					$display.= "</li>";
				}
				break;
			case "pwd_length":
				$display.="<li>".$p['name'].": ";
				$display.= "Minimum of ".$v." ".$p['form_unit'];
				$display.= "</li>";
				break;
		}
		/*$display.="<li>".$code.":<ul>";
		foreach($p as $x => $z) {
			$display.="<li>".$x." => ".$z."</li>";
		}
		$display.="</ul></li>";*/
	}
	if($this->getPasswordChangeReason()=="reset") {
		$display.="</ol></li>";
	}
}
			$display.="</ul></td>
		</tr>
	</table>
</form>
		";
		$js.= "
		$('button#btn_change').click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			".($this->getPasswordChangeReason()!="new" ? $this->getChangePasswordJS() : "")."
		});

		".$notify['js']."
		".$force['js']."
		".$current['js']."
		".$new['js']."
		".$repeat['js']."
		$('button:not(.btn_save)').children('.ui-button-text').css({'padding-top':'3px','padding-bottom':'3px'});
		";
		return array('display'=>$display,'js'=>$js);
	}

	public function getChangePasswordJS() {
		$js= "
			var u = $('#user_id').val();
			var r = $('#reason').val();
			if(u=='X' || u==0 || u=='0' || u.length <4) {
				AssistHelper.finishedProcessing('error','Please select the user.');
			} else if(r=='user' && $('#current').val().length==0) {
				AssistHelper.finishedProcessing('error','Please enter your current password.');
			} else if($('#new').val()!=$('#repeat').val()) {
				$('#new').val('');
				$('#repeat').val('');
				AssistHelper.finishedProcessing('error','The two new passwords do not match.  Please try again.');
			} else {
				var dta = 'reason=".$this->getPasswordChangeReason()."&current='+AssistString.rawurlencode($('#current').val());
				dta += '&new='+AssistString.rawurlencode($('#new').val());
				dta += '&notify='+AssistString.rawurlencode($('#notify').val());
				dta += '&force='+AssistString.rawurlencode($('#force').val());
				dta += '&user_id='+AssistString.rawurlencode($('#user_id').val());";
				if($this->getPasswordChangeReason()=="reset"){
					$js.="
					dta += '&locked='+AssistString.rawurlencode($('#locked').val());
					dta += '&unlock='+AssistString.rawurlencode($('#unlock').val());
					var res = AssistHelper.doAjax('ajax_controller.php?act=SAVE_SESSION',AssistForm.serialize($('form[name=frm_change]')));
					//console.log(res);
					dta += '&key='+res[2];
					//full_form = AssistForm.serialize($('form[name=frm_change]'));
					//console.log($('form[name=frm_change]'));
					//console.log(full_form);
					//full_form = AssistString.code(full_form);
					//console.log(full_form);
					//full_form = AssistString.base64_encode(full_form);
					//console.log(full_form);
					//dta += '&full_form='+full_form;
					";
				}
				//console.log(dta);
				$js.="
				//var result = ['info','dev test'];
				var result = AssistHelper.doAjax('".($this->isRootFolder() ? "TK/" : "")."ajax_controller.php?act=ASSIST_TK.changePassword',dta);
				if(result[0]=='ok') { ";
				//if($this->getPasswordChangeReason()=="new") {
					$js.="
					//alert('Password change successful.');
					//document.location.href = 'login.html'";
				//} else
				if($this->isRedirectPasswordChangeReason()) {
					$js.="
					//alert('Password change successful.');
					//document.location.href = 'login.html'
					AssistHelper.finishedProcessingWithRedirect(result[0],result[1],'login.html');
					";
				} else {
					$js.="
					//document.location.href = 'password.php?r[]=ok&r[]='+result[1];
					AssistHelper.finishedProcessing(result[0],result[1]);
					$('form[name=frm_change] input:text, form[name=frm_change] input:password').val('');
					";
				}
			$js.="
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}";
		return $js;
	}

	public function changePassword($var) {
		$new = rawurldecode($var['new']);
		$tkid = isset($var['user_id']) ? $var['user_id'] : $this->getUserID();
		$reason = $var['reason'];
		$this->setPasswordChangeReason($reason);
		if($reason=="reset" || $reason=="convert") {
			$sql = "SELECT tkid, CONCAT(tkname, ' ', tksurname) as tkn, tkpwd, tkstatus, tkemail, tklogin, tkpwdyn, tkpwddate, tklock, tklocktime, tkloginwindow
			FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$tkid."'";
			$old_row = $this->mysql_fetch_one($sql);
			$current = $this->convertFromHex($old_row['tkpwd']);
		} else {
			$current = rawurldecode($var['current']);
		}

		$notify = $var['notify'];
		$force = $var['force'];
		//$full_form = isset($var['full_form']) ? $var['full_form'] : false;
		if($reason!="convert" && isset($_SESSION['TK']['stored']['PASSWORD_RESET'])) {
			$full_form = $_SESSION['TK']['stored']['PASSWORD_RESET'];
			unset($_SESSION['TK']['stored']['PASSWORD_RESET']);
		} else {
			$full_form = false;
		}
		$unlock = isset($var['unlock']) ? $var['unlock'] : 0;
		$locked = isset($var['locked']) ? $var['locked'] : "N";

		$is_superuser = $this->isSuperUser(strlen($tkid)==0 ? "" : $tkid);
		if($is_superuser) {
			$password_settings_field = "super_user";
		} else {
			$password_settings_field = "normal_user";
		}

		$err = array();
		$sql = "SELECT tkpwd, tkemail, tkname, tksurname, tkuser FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$tkid."'";
		//$p = $this->mysql_fetch_one_value($sql, "tkpwd");
		$old = $this->mysql_fetch_one($sql);
		$p = $old['tkpwd'];

		if($reason=="user" && $this->convertToHex($current)!=$p) {
			return array("error","Incorrect current password.  Please try again.");
		} else {
			$password_settings = $this->getPasswordSettings();
			$is_super = $this->isSuperUser();
			if($is_superuser) {
				$password_settings_field = "super_user";
			} else {
				$password_settings_field = "normal_user";
			}
			if($reason!="convert") {
				foreach($password_settings as $key => $p) {
					$v = $p[$password_settings_field];
					switch($key) {
						case "pwd_length":
							if(strlen($new)<$v) {
								$err[] = "- Password is too short.  It must be at least ".$v." characters in length.";
							}
							break;
						case "pwd_complexity":
							$validation = array(
								'lower'=>0,
								'upper'=>0,
								'digit'=>0,
								'punct'=>0,
							);
							for($i=0;$i<strlen($new);$i++) {
								$c = substr($new,$i,1);
								if(ctype_lower($c)) {
									$validation['lower']++;
								}
								if(ctype_upper($c)) {
									$validation['upper']++;
								}
								if(ctype_digit($c)) {
									$validation['digit']++;
								}
								if(ctype_punct($c)) {
									$validation['punct']++;
								}
							}
							if($validation['lower']==0){
								$err[] = "- Password must contain at least one lower case character.";
							}
							if($validation['upper']==0){
								$err[] = "- Password must contain at least one UPPER case character.";
							}
							if($validation['digit']==0){
								$err[] = "- Password must contain at least one numerical character (0-9).";
							}
							if($v>0 && $validation['punct']==0){
								$err[] = "- Password must contain at least one special character.";
							}
							if(array_sum($validation)!=strlen($new)) {
								$err[] = "- Password contains an invalid character.  Note that spaces and extended ASCII characters are not permitted.";
							}
							break;
						case "pwd_history":
							if($v>0) {
								$history = $this->getPasswordHistory($v,$tkid);
								if(in_array($this->convertToHex($new), $history)) {
									$err[] = "- Password cannot be repeated within $v password change".($v>1?"s":"").".";
								}
							}
							break;
					}
				}
			}
			if(count($err)>0) {
				return array("error","Password change error:<br />".implode("<br />",$err));
			} else {
				$cmpcode = $this->getCmpCode();
				$new_hex = $this->convertToHex($new);
				$current_hex = $this->convertToHex($current);
				//process change - force change if reason = reset
				$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep
				SET ".(($reason=="reset" && (($force*1)==1 || $force=="Y"))||$reason=="convert" ? "tkpwdyn = 'Y' " : "tkpwdyn = 'N' ")."
				, tkpwd = '".$new_hex."'
				, tkpwddate = ".($password_settings["pwd_age"][$password_settings_field]>0 ? "now()" : "'0000-00-00 00:00:00'")."
				".(($reason=="reset" && $locked=="Y" && ($unlock*1)==1)||$reason=="convert" ? ", tklock = 'N', tklocktime = '' " : "")."
				WHERE tkid = '".$tkid."'";
				$this->db_update($sql);

				//log change
				$this->logPasswordChange($tkid, $new_hex, $current_hex, $sql);
				$result = array("ok","Password successfully changed.");
				//NOTIFY if reason = reset & notify = 1
				if($reason=="reset" || $reason=="convert") {
					if($locked=="Y" && ($unlock*1)==1 && $reason!="convert") {
						//log unlocking
						$loginObj = new ASSIST_LOGIN(array(),false);
						$txt = "Unlocked user on password reset.";
						$old_locktime = "";
						$loginObj->logUserUnlock($tkid, $old_locktime, $txt, $sql,true,$this->getCmpCode());
					}
					if($full_form!==false) {
						//log complete form into db
						//$form = unserialize(ASSIST_HELPER::decode(base64_decode($full_form)));
						$form = $full_form;
						$full_form = base64_encode(serialize($form));
						$request_src = substr($form['request_sources']."_".$this->code($form['request_sources_other']),0,100);
						$validated = $form['id_validated'];
						$validated_against = implode("; ",$form['id_validation_sources']);
						$sql = "INSERT INTO assist_".$cmpcode."_timekeep_log_password SET
									acting_date = now(),
									acting_tid = '".$this->getUserID()."',
									acting_tkn = '".$this->getUserName()."',
									acting_reason = '".$this->getPasswordChangeReason()."',
									user_tid = '".$tkid."',
									user_was_pwd_locked = ".($locked=="Y" ? 1 : 0).",
									user_is_super = ".($is_superuser ? 1 : 0).",
									admin_super_user_details = '".serialize($_SESSION['super_user'])."',
									admin_request_src = '$request_src',
									admin_validated = '$validated',
									admin_validated_against = '$validated_against',
									admin_unlock_user = '".($locked=="Y" && (($unlock*1)==1 || $unlock=="Y") ? 1 : 0)."',
									admin_notify_user = '".((($notify*1)==1 || $notify=="Y") ? 1 : 0)."',
									admin_force_reset = '".((($force*1)==1 || $force=="Y") ? 1 : 0)."',
									full_form = '$full_form',
									pwd_settings = '".base64_encode(serialize($password_settings))."'
								";
						$this->db_insert($sql);
					}
					if(($notify*1)==1) {
						$to = $old['tkemail'];
						if(strlen($to)>0) {
							if($reason=="convert") {
								$to = array('name'=>$old['tkname']." ".$old['tksurname'],'email'=>$to);
								$site_name = $this->getSiteName();
								$site_code = $this->getSiteCode();
								$cmpadmin = $this->getCmpAdmin($cmpcode);
								$bpa_details = $this->getBPAdetails();
								$bpa_con = explode("|",$bpa_details['help_contact']);
								$bpa_contact = array();
								foreach($bpa_con as $b) {
									$b2 = explode("_",$b);
									$bpa_contact[] = $b2[1];
								}
								$subject = "Welcome to ".$site_name;
								$message = "Dear ".decode(trim($old['tkname'])." ".trim($old['tksurname'])).chr(10);
								$message.= " ".chr(10);
								$message.= "We would like to welcome you to ".$site_name.". ".chr(10);
								$message.= " ".chr(10);
								$message.= "Your login details are: ".chr(10);
								$message.= "User name: ".(decode($old['tkuser'])).chr(10);
								$message.= "Company ID: ".strtoupper($cmpcode).chr(10);
								$message.= "Password: ".$new.chr(10);
								$message.= "Please keep your login details confidential. ".chr(10);
								$message.= " ".chr(10);
								$message.= "To login go to: http://assist.action4u.co.za/?".$site_code." ".chr(10);
								$message.= "Please login within 7 days or your account will be locked. ".chr(10);
								$message.= " ".chr(10);
								$message.= "For assistance please contact: ".chr(10);
								$message.= "- Your Assist Administrator, ".$cmpadmin['cmpadmin'].(strlen($cmpadmin['cmpadminemail'])>0 ? ", at ".$cmpadmin['cmpadminemail'] : ",")." or ".chr(10);
//Removed Business Partner name to accommodate the separate reseller & support reseller [AA-534] JC
								$message.= "- Your ".$site_name." Business Partner at ".implode(" or ",$bpa_contact).". ".chr(10);
								$message.= " ".chr(10);
								$message.= "Kind regards ".chr(10);
//Removed Business Partner name to accommodate the separate reseller & support reseller [AA-534] JC
								$message.= "".$site_name." ".chr(10);

								$from = $bpa_details['help_email'];
								$header = "From: ".$bpa_details['cmpname']." <no-reply@action4u.co.za>\r\nReply-to: ".$bpa_details['cmpname']." <".$from.">\r\nCC: ".$from."\r\nBCC: webmaster@ignite4u.co.za";
								//mail($to,$subject,$message,$header);
								$mail = new ASSIST_EMAIL($to, $subject, $message);
								$mail->setSender(array('name'=>$bpa_details['cmpname'],'email'=>$from));
								$mail->setCC($from);
								$mail->setBCC("helpdesk_cc@actionassist.co.za");
								$mail->sendEmail();
								$result[1].=" An email was sent to ".$to['email']." with the new password.";


							} else {

								$to = array('name'=>$old['tkname']." ".$old['tksurname'],'email'=>$to);
								$site_name = $this->getSiteName();
								$site_code = $this->getSiteCode();
								$cmpadmin = $this->getCmpAdmin($cmpcode);
								$bpa_details = $this->getBPAdetails();
								$bpa_con = explode("|",$bpa_details['help_contact']);
								$bpa_contact = array();
								foreach($bpa_con as $b) {
									$b2 = explode("_",$b);
									$bpa_contact[] = $b2[1];
								}
								$subject = "Your ".$site_name." Password";
								$message = "Dear ".$old['tkname']." ".$old['tksurname'].chr(10);
								$message.= " ".chr(10);
								$message.= "Your ".$site_name." password has been changed by the Administrator. ".chr(10);
								$message.= " ".chr(10);
								$message.= "Your new password is: ".$new.chr(10);
								$message.= "Reminder: Your username is ".$old['tkuser']." and the company code is ".$cmpcode.".".chr(10);
								$message.= "Please keep your login details confidential. ".chr(10);
								$message.= " ".chr(10);
								$message.= "To login go to: http://assist.action4u.co.za/?".$site_code." ".chr(10);
								$message.= " ".chr(10);
								$message.= "For assistance please contact: ".chr(10);
								$message.= "- Your Assist Administrator, ".$cmpadmin['cmpadmin'].(strlen($cmpadmin['cmpadminemail'])>0 ? ", at ".$cmpadmin['cmpadminemail'] : ",")." or ".chr(10);
								$message.= "- Your ".$site_name." Business Partner, ".$bpa_details['cmpname'].", at ".implode(" or ",$bpa_contact).". ".chr(10);
								$message.= " ".chr(10);
								$message.= "Kind regards ".chr(10);
								$message.= "".$bpa_details['cmpname']." ".chr(10);
								$message.= " ".chr(10);

								$from = $bpa_details['help_email'];
								$header = "From: ".$bpa_details['cmpname']." <no-reply@action4u.co.za>\r\nReply-to: ".$bpa_details['cmpname']." <".$from.">\r\nCC: ".$from."\r\nBCC: helpdesk@actionassist.co.za";
								//mail($to,$subject,$message,$header);
								$mail = new ASSIST_EMAIL($to, $subject, $message);
								$mail->setSender(array('name'=>$bpa_details['cmpname'],'email'=>$from));
								$mail->setCC($from);
								$mail->setBCC("helpdesk_cc@actionassist.co.za");
								$mail->sendEmail();
								$result[1].=" An email was sent to ".$to['email']." with the new password.";
							}
						}
					}
				}
				//return result
				return $result;
			}
		}
		return array("info","An error ocurred.  Password change unsuccessful.");
	}




	private function logPasswordChange($user_id,$new,$old,$sql2) {
		$c = array(
			'type'=>strtoupper(substr($this->password_change_reason,0,1)),
			'fld'=>"tkpwd",
			'new'=>$new,
			'old'=>$old,
			'txt'=>"Password changed: ".$this->password_change_reasons[$this->getPasswordChangeReason()]
		);
		$sql = "INSERT INTO assist_".$this->cc."_timekeep_log
				(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
				VALUES
				(now(),'".$this->getUserID()."','".$this->getUserName()."','PWD','".$user_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql2)."')";
		$this->db_insert($sql);

	}

	private function getPasswordHistory($limit=0,$user_id="") {
		if($limit>0) {
			if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
			$sql = "SELECT new FROM assist_".$this->cc."_timekeep_log WHERE section = 'PWD' AND ref = '".$user_id."' LIMIT ".$limit;
			$rows = $this->mysql_fetch_all_by_value($sql,"new");
			return $rows;
		} else {
			return array();
		}
	}


	private function getPasswordSecurityQuestions() {
		return $this->password_security_questions;
	}

	public function echoPasswordSecurityQuestionForm($user_id="") {
		$display = $this->getPasswordSecurityQuestionForm($user_id);
		echo $display['display'];
		return $display['js'];
	}

	public function getPasswordSecurityQuestionForm($user_id="") {
		$display = "";
		$js = "";

		if($this->getPasswordChangeReason()=="new") {
			$old = array('tkpwdanswer1'=>"",'tkpwdanswer2'=>"",'tkpwdanswer3'=>"",);

		} else {
			if($this->getPasswordChangeReason()!="reset") {
				$user_id = $this->getUserID();
			}
			$sql = "SELECT tkpwdanswer1, tkpwdanswer2, tkpwdanswer3 FROM assist_".$this->getCmpCode()."_timekeep WHERE tkid = '".$user_id."'";
			$old = $this->mysql_fetch_one($sql);
		}


		$displayObject = new ASSIST_MODULE_DISPLAY();

		$security = array();
		if($this->getPasswordChangeReason()!="reset") {
			$security[1] = $displayObject->getMediumInputText($this->decode($old['tkpwdanswer1']),array('id'=>"tkpwdanswer1"));
			$security[2] = $displayObject->getMediumInputText($this->decode($old['tkpwdanswer2']),array('id'=>"tkpwdanswer2"));
			$security[3] = $displayObject->getMediumInputText($this->decode($old['tkpwdanswer3']),array('id'=>"tkpwdanswer3"));
		} else {
			$security[1]['display'] = $this->decode($old['tkpwdanswer1']);
			$security[2]['display'] = $this->decode($old['tkpwdanswer2']);
			$security[3]['display'] = $this->decode($old['tkpwdanswer3']);
		}

	/*
		$security[1]['text'] = "What was your mother's maiden name?";
		$security[2]['text'] = "What was the colour of your first car?";
		$security[3]['text'] = "What was the name of your high school?";
		*/
		$questions = $this->getPasswordSecurityQuestions();
		foreach($questions as $qk => $q) {
			$security[$qk]['text'] = $q;
		}

		if($this->getPasswordChangeReason()!="reset") {
			$display = "
			<form name=frm_security>
			<p>If you forget your password, you will be asked to answer one of the following questions in order to receive a new password. <br />Answers to at least 2 questions are required.</p>";
		}
		$display.="<table id=tbl_security>";
		for($i=1;$i<=3;$i++) {
			$display.="
			<tr>
				<td class=b>".$security[$i]['text'].":</td>
				<td>".$security[$i]['display']."</td>
			</tr>";
			$js.="
			".$security[$i]['js']."
			";
		}
		$display.="
			".($this->getPasswordChangeReason()!="new" && $this->getPasswordChangeReason()!="reset" ? "
			<tr>
				<td></td>
				<td><button id=btn_security class='btn_green btn_save'>Save</button></td>
			</tr>" : "")."
		</table>
		</form>
		";

		$js.="
		$('button#btn_security').click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			".($this->getPasswordChangeReason()!="new" ? $this->getChangeSecurityQuestionJS() : "")."
		});
		";

		return array('display'=>$display,'js'=>$js);
	}


	public function getChangeSecurityQuestionJS() {
		$js= "
			var a = $('#tkpwdanswer1').val();
			var b = $('#tkpwdanswer2').val();
			var c = $('#tkpwdanswer3').val();

			var d = 0;
			if(a.length>0) d++;
			if(b.length>0) d++;
			if(c.length>0) d++;

			if(d<2) {
				AssistHelper.finishedProcessing('error','Please provide answers to at least 2 questions.');
			} else {
				var dta = 'user_id=".$this->getUserID()."&reason=".$this->getPasswordChangeReason()."&tkpwdanswer1='+AssistString.rawurlencode(a)+'&tkpwdanswer2='+AssistString.rawurlencode(b)+'&tkpwdanswer3='+AssistString.rawurlencode(c);
				var result = AssistHelper.doAjax('".($this->isRootFolder() ? "TK/" : "")."ajax_controller.php?act=ASSIST_TK.saveSecurityQuestions',dta);
				if(result[0]=='ok') { ";
				if($this->getPasswordChangeReason()=="new") {
					$js.="
					//alert('Password change successful.');
					//document.location.href = 'login.html'";
				} else {
					$js.="
					//document.location.href = 'password.php?r[]=ok&r[]='+result[1];
					AssistHelper.finishedProcessing(result[0],result[1]);
					";
				}
			$js.="
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		";
		return $js;
	}



	public function saveSecurityQuestions($var) {
		$answer1 = rawurldecode($var['tkpwdanswer1']);
		$answer2 = rawurldecode($var['tkpwdanswer2']);
		$answer3 = rawurldecode($var['tkpwdanswer3']);
		$tkid = isset($var['user_id']) ? $var['user_id'] : $this->getUserID();
		$reason = $var['reason'];
		$this->setPasswordChangeReason($reason);

		$sql = "UPDATE assist_".$this->getCmpCode()."_timekeep SET
				tkpwdanswer1 = '".$this->code($answer1)."' ,
				tkpwdanswer2 = '".$this->code($answer2)."' ,
				tkpwdanswer3 = '".$this->code($answer3)."'
				WHERE tkid = '".$tkid."'";
		$this->db_update($sql);

		return array("ok","Security Question Answers updated successfully.");

	}



	public function echoNewUserPasswordSecurity() {
		$display = "
		<div class=center style='margin-top: 10px;'><button id=btn_save_all class='btn_green btn_save'>Save</button></div>
		";
		$js = $this->getNewUserPasswordSecurityJS();
		echo $display;
		return $js;
	}


	public function getNewUserPasswordSecurityJS() {
		$js = "
		$('button#btn_save_all').click(function(e) {
			e.preventDefault();

			AssistHelper.processing();

			var overall_result = 0;

			".$this->getChangePasswordJS()."

			var pwd_result = result;

			".$this->getChangeSecurityQuestionJS()."

			var sec_result = result;

			if(pwd_result[0]=='ok' && sec_result[0]=='ok') {
				alert('Password change successful.');
				document.location.href = 'login.html';
			}
		});
		";
		return $js;
	}




	public function generateRandomPassword() {
		$alpha = array("a","B","c","D","e","F","g","H","i","J","k","L","m","N","o","P","q","R","s","T","u","V","w","X","y","Z");
			$tkpwd = "";
			$i=1;
			while($i<8) {
				$p = rand(0,25);
				if(strlen($alpha[$p]) != 0) {
					$tkpwd = $tkpwd.$alpha[$p];
					$alpha[$p] = "";
					$i++;
				}
			}
		return array('pwd'=>$tkpwd,'hex'=>$this->convertToHex($tkpwd));
	}



	public function __destruct() {

	}

}


?>