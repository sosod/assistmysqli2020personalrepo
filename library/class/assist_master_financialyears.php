<?php

/**
 * Class ASSIST_MASTER_FINANCIALYEARS
 * Original Class for managing Master Setups > Financial Years
 * Contents moved to /S/class/S_FINANCIALYEARS as part of standardisation/centralisation
 * This class has been kept as a placeholder for the other modules that call it
 * AA-610 / AA-649 [JC] 17 July 2021
 */
class ASSIST_MASTER_FINANCIALYEARS extends S_FINANCIALYEARS {

	public function __construct() {
		parent::__construct();

	}


}

?>