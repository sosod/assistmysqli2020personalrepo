<?php
include_once "assist_dbconn.php";
include_once "assist_db.php";

/* Email sending class for Ignite Assist
Created by: Janet Currie (c)
Created on: 12 September 2012
*/


class ASSIST_EMAIL {

/* VARIABLES */
	private $to;
	private $raw_to = "";
	private $reply_to;
	private $raw_replyto = "";
	private $from;
	private $default_from = "no-reply";
	private $header;
	private $subject;
	private $message;
	private $content_type;
	private $raw_cc = "";
	private $cc;
	private $bcc;
	private $server;
	private $domain = "assist.action4u.co.za";
	private $message_id = "";

	private $modref = "";
	private $sender_user_id = "";
	private $recipient_user_id = "";
	private $sender_cmpcode = "";
	private $recipient_cmpcode = "";

	private $status = array(
		'to' => false,
		'subject' => false,
		'body'=>false
	);

	/**
	 * Class to generate, format and send emails from Assist
	 * Variables not required
	 *
	 * @param to = recipients
	 * @param subject
	 * @param message
	 * @param content_type = "TEXT" || "HTML"
	 * @param cc
	 * @param bcc - email only, no name/array
	 * @param reply_to
	 */
	public function __construct($to = "", $subject = "", $message = "", $content_type = "TEXT", $cc = "", $bcc = "", $reply_to = "") {
		$this->server = $_SERVER["SERVER_NAME"];
		if(strpos($this->server,"localhost")!==false) {
			//CHANGE NOTHING - $this->domain
		} elseif(strpos($this->server,"action4udev")!==false) {
			$this->domain = "action4udev.co.za";
		} else {
			$this->domain = $this->server;
		}
		$this->sender_user_id = (isset($_SESSION['tid']) ? $_SESSION['tid'] : "0000");
		$this->sender_cmpcode = (isset($_SESSION['cc']) ? $_SESSION['cc'] : "X");
		$this->default_from = $this->default_from."@".$this->domain;
		$this->setRecipient($to);
		$this->setSubject($subject);
		$this->setBody($message);
		$this->setCC($cc);
		$this->setBCC($bcc);
		$this->setSender($reply_to);
		$this->setContentType($content_type);
	}

	public function getEmailSendingDomain() { return $this->domain; }
	public function getSenderUserID() { return $this->sender_user_id; }
	public function getRecipientUserID() { return $this->recipient_user_id; }
	public function getSenderCmpCode() { return $this->sender_cmpcode; }

	public function setModRef($mr) { $this->modref = $mr; $this->setHeader(); }
	public function setSenderUserID($si) { $this->sender_user_id = $si; $this->setHeader(); }
	public function setSenderCmpCode($sc) { $this->sender_cmpcode = $sc; $this->setHeader(); }
	public function setRecipientUserID($si,$append=false) {
		if($append) {
			if(strlen($this->recipient_user_id)>0) { $this->recipient_user_id.=","; }
			$this->recipient_user_id.= is_array($si) ? implode(",",$si) : $si;
		} else {
			$this->recipient_user_id = is_array($si) ? implode(",",$si) : $si;
		}
	}
	public function setRecipientCmpCode($rc,$append=false) {
		if($append) {
			if(strlen($this->recipient_cmpcode)>0) { $this->recipient_cmpcode.=","; }
			$this->recipient_cmpcode.= is_array($rc) ? implode(",",$rc) : $rc;
		} else {
			$this->recipient_cmpcode = is_array($rc) ? implode(",",$rc) : $rc;
		}
	}

	function setRecipient($to) {
/*		$this->status['to'] = false;
		if(is_array($to)) {
			$rec = array();
			if(count($to)==2 && isset($to['name'])) {
				$this->to = $this->setNamedEmail($to);
				$this->raw_to = $to['email'];
				$this->status['to'] = true;
			} else {
				foreach($to as $t) {
					if(is_array($t)) {
						if(count($t)==2 && isset($t['name'])) {
							$rec[] = $this->setNamedEmail($t);
						}
					} else {
						$rec[] = $t;
					}
				}
				if(count($rec)>0) {
					$this->to = implode(",",$rec);
					$this->status['to'] = true;
				}
			}
		} else {
			$this->to = $to;
			$this->status['to'] = true;
		}*/
		$this->status['to'] = false;
		if(is_array($to)) {
			$rec = array();
			if(isset($to['name']) && isset($to['email'])) {
				/* Added option to set Recipient user id for DB logging by sending id as part of the $to - AA-540 JC 20 March 2021 */
				if(isset($to['tkid'])) {
					$this->setRecipientUserID($to['tkid']);
				}
				$this->to = $this->setNamedEmail($to);
				$this->raw_to = $to['email'];
				$this->status['to'] = true;
			} else {
				foreach($to as $t) {
					if(is_array($t)) {
						if(isset($t['name']) && isset($t['email'])) {
				/* Added option to set Recipient user id for DB logging by sending id as part of the $to - AA-540 JC 20 March 2021 */
							if(isset($t['tkid'])) {
								$this->setRecipientUserID($t['tkid']);
							}
							$rec[] = $this->setNamedEmail($t);
							$this->raw_to.=(strlen($this->raw_to)>0?"; ":"").$t['email'];
						}
					} else {
						$rec[] = $t;
						$this->raw_to.=(strlen($this->raw_to)>0?"; ":"").$t;
					}
				}
				if(count($rec)>0) {
					$this->to = implode(",",$rec);
					$this->status['to'] = true;
				}
			}
		} else {
			$this->to = $to;
			$this->raw_to = $to;
			$this->status['to'] = true;
		}
		$this->setHeader();

	}
	private function setNamedEmail($r) {
		$str = $r['name'];
		$str.= " <".$r['email'].">";
		return $str;
	}

	function setSubject($sub="") {
		$this->subject = $sub;
		if(strlen($sub)>0) { $this->status['subject'] = true; } else { $this->status['subject'] = false; }
	}

	function setBody($body) {
		$this->message = $body;
		if(strlen($body)>0) { $this->status['body'] = true; } else { $this->status['body'] = false; }
	}

	function setHTMLBody($body) {

		$email = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\">
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>
		<style type=text/css>
			hr {
				color: #000099;
			}
			h1 {
				text-decoration: none;
				font-size: 16pt;
				line-height: 20pt;
				font-weight: bold;
				padding: 0 0 0 0;
				margin: 15 0 10 5;
				font-family: \"Comic Sans MS\", Arial, Helvetica, sans-serif;
				color: #000099;
				letter-spacing: 0.07em;
			}
			h2 {
				text-decoration: none;
				font-size: 14pt;
				line-height: 17pt;
				font-weight: bold;
				padding: 0 0 0 0;
				margin: 15 0 10 5;
				font-family: \"Comic Sans MS\", Arial, Helvetica, sans-serif;
				color: #000099;
				letter-spacing: 0.07em;
			}
			p {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 10pt;
				line-height: 12pt;
				color: #000000;
				margin: 10px 0px 10px;
				padding: 0px 0px 0px 5px;
			}
			table {
				border-collapse: collapse;
				border: solid 1px #ababab;
			}
			table td {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 8.5pt;
				line-height: 12pt;
				color: #000000;
				border: solid 1px #ababab;
				vertical-align: top;
				padding: 5px 5px 5px 5px;
				text-align: left;
			}
			table th {
				background-color: #000099;
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 8.5pt;
				text-align: center;
				font-weight: bold;
				line-height: 12pt;
				color: #ffffff;
				padding: 5px 5px 5px 5px;
				border: solid 1px #ffffff;
			}
			.overdue { color: #cc0001; }
			.today { color: #009900; }
			.soon { color: #FE9900; }
			.b { font-weight: bold; }
			.u { text-decoration: underline; }
			.i { font-style: italic; }
			body {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 10pt;
				line-height: 12pt;
			}
		</style>
		<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
		";
		$email.= $body;
		$email.= "</body></html>";
		//$filename = "Results/TESTEMAIL_".date("Ymd_His").".html";
       //$file = fopen($filename,"w");
       //fwrite($file,$email."\n");
       //fclose($file);
		$this->setBody($email);
	}

	function setContentType($ct) {
		$this->content_type = $ct;
		$this->setHeader();
	}

	function setCC($to="") {
		//$this->cc = $cc;
		if(is_array($to)) {
			$rec = array();
			if(count($to)==2 && isset($to['name'])) {
				$this->cc = $this->setNamedEmail($to);
				$this->raw_cc = $to['email'];
			} else {
				foreach($to as $t) {
					if(is_array($t)) {
						if(count($t)==2 && isset($t['name'])) {
							$rec[] = $this->setNamedEmail($t);
							$this->raw_cc.=$t['email'];
						}
					} else {
						$rec[] = $t;
						$this->raw_cc.=$t;
					}
				}
				if(count($rec)>0) {
					$this->cc = implode(",",$rec);
				}
			}
		} else {
			$this->cc = $to;
			$this->raw_cc.=$to;
		}

		$this->setHeader();
	}

	function setBCC($bcc="") {
		$this->bcc = $bcc;
		$this->setHeader();
	}

	function setSender($send) {
		if($send!==false && is_array($send)) {
			if(count($send)==2 && isset($send['name']) && isset($send['email'])) {
				if(strlen($send['email'])==0) {
					$send['email'] = $this->default_from;
				}
				$this->reply_to = $this->setNamedEmail($send);
				$this->raw_replyto=$send['email'];
				$this->from = $this->setNamedEmail(array('name'=>$send['name'],'email'=>$this->default_from));
			} else {
				$this->reply_to = "";
				$this->raw_replyto="";
				$this->from = $this->default_from;
			}
		} else {
			if($send!==false && strlen($send)>0) {
				$this->reply_to = $send;
				$this->raw_replyto=$send;
				$this->from = $this->default_from;
			} else {
				$this->reply_to = "";
				$this->raw_replyto="";
				$this->from = $this->default_from;
			}
		}
		$this->setHeader();

/*
		if(is_array($send)) {
			if(count($send)==2 && isset($send['name']) && isset($send['email'])) {
				$this->reply_to = $this->setNamedEmail($send);
				$this->from = $this->setNamedEmail(array('name'=>$send['name'],'email'=>$this->default_from));
			} else {
				$this->reply_to = "";
				$this->from = $this->default_from;
			}
		} else {
			if(strlen($send)>0) {
				$this->reply_to = $send;
				$this->from = $this->default_from;
			} else {
				$this->reply_to = "";
				$this->from = $this->default_from;
			}
		}
		$this->setHeader();
*/
	}

	function setHeader() {
		$header = "From: ".$this->from."\r\n";
		if(strlen($this->reply_to)>0) {
			$header.="Reply-to: ".$this->reply_to."\r\n";
		}
		if(strlen($this->cc)>0) {
			$header.="CC: ".$this->cc."\r\n";
		}
		if(strlen($this->bcc)>0) {
			$header.="BCC: ".$this->bcc."\r\n";
		}
		$message_id = date("YmdHis").".".md5($this->raw_to.$this->raw_replyto)."@".$this->domain;
$this->message_id = $message_id;
		$header.="Message-ID: ".$message_id."\r\n";
		$header.="X-SMTPAPI: { \"unique_args\" : { \"user_id\": \"".$this->getSenderUserID()."\", \"cmpcode\": \"".$this->getSenderCmpCode()."\" , \"internal_smtp_id\" : \"$message_id\" , \"subject\" : \"".urlencode($this->getSubject())."\" , \"modref\" : \"".$this->getModref()."\"} }\r\n";
		$header.="X-Auto-Response-Suppress:OOF\r\nReturn-Path: <>\r\n";

		switch($this->content_type) {
			case "HTML":
				$header.="Content-type: text/html; charset=us-ascii";
				$this->setHTMLBody($this->getBody());
				break;
			case "TEXT":
				$header.="";
				break;
			default:
				if(strlen($this->content_type)>0) {
					$header.=$this->content_type;
				}
				break;
		}
		$this->header = $header;
	}


	function getModRef() { return isset($_SESSION['modref']) ? strtoupper($_SESSION['modref']) : "X"; }

	function getRecipient() {
		return $this->to;
	}

	function getSubject() {
		return $this->subject;
	}

	function getBody() {
		return $this->message;
	}

	function getContentType() {
		return $this->content_type;
	}

	function getCC() {
		return $this->cc;
	}

	function getBCC() {
		return $this->bcc;
	}

	function getSender() {
		return $this->reply_to;
	}

	function getHeader() {
		return $this->header;
	}



	function sendEmail() {
		$this->setHeader();
		if($this->status['to']==true && $this->status['subject']==true && $this->status['body']==true) {
			$original_message = $this->message;
			if(strpos($this->to,"actionassist.co.za")!==false || strpos($this->to,"igniteassist.co.za")!==false) {
				if($this->content_type=="HTML") {
					$this->message.="<p>&nbsp;</p><p>Site: ".(isset($_SERVER['HTTPS']) && ($_SERVER["HTTPS"]=="on" || $_SERVER["HTTPS"]==true) ? "https://" : "http://").$_SERVER["SERVER_NAME"]."</p>";
				} else {
					$this->message.=chr(10).chr(10)."Site: ".(isset($_SERVER['HTTPS']) && ($_SERVER["HTTPS"]=="on" || $_SERVER["HTTPS"]==true) ? "https://" : "http://").$_SERVER["SERVER_NAME"];
				}
			}
			$db = new ASSIST_DB("master");
			$sql = "INSERT INTO assist_email_log_internal VALUES (
					null
					, now()
					, '".$this->message_id."'
					, '".base64_encode($this->raw_to)."'
					,'".base64_encode($this->getHeader())."'
					,'".base64_encode($this->getSubject())."'
					,'".base64_encode($this->getBody())."'
					,'".$this->getSenderUserID()."'
					,'".$this->getRecipientUserID()."'
					,'".$this->getSenderCmpCode()."'
					,'".$this->getModRef()."'
					)";
			$db->db_insert($sql);

			mail($this->to,$this->subject,$this->message,$this->header);
			$this->message = $original_message;
			return true;
		} else {
			return false;
		}
	}



	function __destruct() {

	}

} //end class

?>