<?php
/** CLASS TO MANAGE LOGIN PROCESS
Created on: 2 October 2012
Created by: Janet Currie

Deployed to Dev: 3 October 2012

V3: June 2015

 **/


class ASSIST_LOGIN extends ASSIST_HELPER {

	//AA-341
	private $super_users = array("support","admin");

	private $user = "";
	private $cc = "";
	private $pwd = "";

	private $sessid = "";

	private $mdb;
	private $cdb;

	private $login_error = 0;
	private $login_error_message = "";
	private $require_second_validation = 0;
	private $redirect = 0;
	private $redirect_site = "";

	private $is_second_validation = false;

	private $cmp_details = array();
	private $user_details = array();
	private $bp_details = array();
	private $site_details = array();
	private $is_reseller = 0;
	private $is_support = 0;
	private $is_admin = false;

	private $system_settings = array();

	private $screen = array('h'=>0,'w'=>0);
	private $next_page;

	private $submit_data;

	private $default_error_message = "Invalid login details.  Please try again.<br /><br /><span class=i>Hint: Remember that passwords are CaSe SeNsItIvE.</span>";

	private $request = array();

	public function __construct($var,$valid8 = false) {
		$this->request = $var;
		if(isset($var['is_second_validation'])) {
			$this->is_second_validation = $var['second_validation']*1==1 ? true : false;
		}
		if($valid8===true) {
			$this->mdb = new ASSIST_DB("master");
			$this->validateLogin($var);
		}
	}

	private function killMe($err=null) {
		if(!is_null($err)) {
			$err = $this->getError($err);
		}
		$this->login_error += 1;
		if(strlen($this->login_error_message)==0) {
			$this->login_error_message = (!isset($err) || strlen($err)==0) ? $this->default_error_message : $err;
		}
		/*$result = array("error",);
		echo "
		<div title='Login Error' id=dlg_login_error>
			<h2>Login Error</h2>";
			$this->displayResult($result);
		echo "
		</div>
		<script type=text/javascript>
			$('#dlg_login_error').dialog({autoOpen:false});
		</script>";*/
	}

	private function getError($err) {
		$err = explode("|",$err);
		switch($err[0]) {
			case "suspended":
				$this->login_error_message = "The account of <i>".$err[1]."</i> has been suspended<br />due to <b>non-payment of fees</b>.  Please contact your Assist Administrator.</b>.";
				break;
			case "dead":
			case "closed":
				$this->login_error_message = "The account of ".$err[1]." has been closed.  <br />Your login details are no longer active.";
				break;
			case "U01":
			case "C02":
			case "P03":
			case "C04":
				$this->login_error_message = "Invalid login details.  Please try again. <br />[Error Code: ".$err[0]."]";
				break;
			default:
				$this->login_error_message = $err[0];
				break;
		}
		return null;
	}

	function drawKillMe() {
		echo "
		<style type=text/css>
		#dlg_login_error p { font-size: 9pt; line-height: 11pt; font-style: Verdana; margin: 10px; }
		</style>
		<div title='Login Error' id=dlg_login_error>
			<p style='font-size: 14pt; font-weight: bold; color: #cc0001; line-height: 16pt'>Login Error</p>
			<p id=error_message style='line-height: 150%'>Invalid login details.</p>
			";
		echo "
		</div>
		<script type=text/javascript>
			$(function() {
				$('#dlg_login_error').dialog({
					autoOpen: false, 
					modal: true, 
					width: 400, 
					height: 200,
					buttons: {
						'Close': function() {
							$(this).dialog('close'); 
						}
					}
				});//.find('.ui-dialog').css({border:\"1px solid #CC0001\"});
				AssistHelper.formatDialogButtons($('#dlg_login_error'),0,AssistHelper.getGreenCSS());
				AssistHelper.formatDialogButtons($('#dlg_login_error'),0,{'padding':'0px','font-size':'75%'});
				AssistHelper.hideDialogTitlebar('id','dlg_login_error');
				//$('#dlg_login_error #btn_error_close').click(function(){ $('#dlg_login_error').dialog('close'); });
			});
			
			function executeKillMe(errmsg) {
				errmsg = (errmsg === null || typeof errmsg === 'undefined' || errmsg.length==0) ? '".$this->default_error_message."' : errmsg;
				$('#dlg_login_error #error_message').attr('innerHTML',errmsg);
				$('#dlg_login_error').dialog('open');
			}
		</script>";
	}






	/**VALIDATION FUNCTIONS**/
	private function validateLogin($var) {
		$this->submit_data = $var;

		$this->user = isset($var['uid']) ? trim(strtolower($var['uid'])) : $this->killMe("U01");
		$this->cc = isset($var['cc']) ? trim(strtolower($var['cc'])) : $this->killMe("C02");
		$this->pwd = isset($var['pid']) ? $this->convertToHex($var['pid']) : $this->killMe("P03");	//Removed ampersand conversion - replaced by encodeURIComponent on index [JC 2019-03-04 #AA-4]
		$this->screen['h'] = (isset($var['screen_h']) ? $var['screen_h'] : 0);
		$this->screen['w'] = (isset($var['screen_w']) ? $var['screen_w'] : 0);
		$this->sessid = $var['niosses'];
		//Validate Company & check for reseller
		if($this->login_error==0) { $this->validateCompany(); }
		//$this->login_error = 1;
		//$this->login_error_message = "company";

		//Validate User
		if($this->login_error==0) { $this->validateUser(); }

		//Validate Active User
		if($this->login_error==0 && !$this->isAdmin()) {
			$this->validateActiveUser();
		}

		if($this->login_error==0) {
			$this->getSystemSettings();
		}

		//Validate Account Status = locked yes / no
		if($this->login_error==0 && !$this->isAdmin()) {
			$this->validateAccountStatus();
		}
		/*
*/
		//Validate Pwd
		if($this->login_error==0) { $this->validatePwd(); }

		//Is Admin or Support?  = Requires secondary validation
		if($this->login_error==0) {
			if($this->isSupport() || $this->isAdmin()) {
				/**
				 * JC - added this line of code to bypass second validation when using a dev site.
				 * The IF statement MUST BE DISABLED if testing anything to do with second validation!
				 * 11 March 2021
				 */
				if(strpos($_SERVER['HTTP_HOST'],"action4u.co.za")!==false) {
					//validate no more than 1 logged in
					$this->validateNumberLoggedIn();
				}
				if($this->login_error==0) {
					/**
					 * JC - added this line of code to bypass second validation when using a dev site.
					 * The IF statement MUST BE DISABLED if testing anything to do with second validation!
					 * 10 March 2021
					 */
					if(strpos($_SERVER['HTTP_HOST'],"action4u.co.za")!==false) {
						$this->require_second_validation = 1;
					}
					//$this->killMe();
					//$this->login_error_message = "second_validation";
					$this->isReseller();
					$this->setSession();
					$this->nextStep();
				}
			} else {
				//Validate Login Window
				$this->validateLoginWindow();
				if($this->login_error==0) {
					$this->checkPasswordAge();
				}
				if($this->login_error==0) {
					//Is company a reseller?
					$this->isReseller();
					//Is user Support?
					//$this->isSupport();
					//Set Session Variables
					$this->setSession();
					//determine the next page to go to - password age
					$this->nextStep();
				}
			}
		}
	}

	private function validateCompany() {
		$sql = "SELECT * FROM assist_company WHERE cmpcode = '".strtoupper($this->cc)."'";
		$cmp_details = $this->mdb->mysql_fetch_one($sql);
		if(!isset($cmp_details['cmpname'])) {
			$this->failCompany("BAD_CC:".$this->cc);
			$this->killMe("C04");
		} elseif($cmp_details['cmp_site']!=$_SERVER['HTTP_HOST']
			&& strpos($_SERVER['HTTP_HOST'],"localhost")===false
			&& strpos($_SERVER['HTTP_HOST'],"action4udev")===false
			&& (isset($_SERVER['APPLICATION_ENV']) && strpos($_SERVER['APPLICATION_ENV'],"development")===false)) {
			//} elseif($cmp_details['cmp_site']=="pwc.assist.action4u.co.za") {
			$this->login_error++;
			$raw_token = $this->request;
			$token = base64_encode(serialize($raw_token));
			$this->login_error_message = $token;
			$this->redirect = 1;
			$this->redirect_site = $cmp_details['cmp_site'];
			//Swapped S/!Y checks around so that S&!SuperUser can pass on to "else", so !Y check must include !S as well otherwise S will trigger !Y : AA-341 JC
		} elseif($cmp_details['cmpstatus']!="Y" && $cmp_details['cmpstatus']!="S") {
			$this->login_error++;
			$this->failCompany("DEAD");
			$this->killMe("dead|".$cmp_details['cmpname']." (".strtoupper($this->cc).")");
		} elseif($cmp_details['cmpstatus']=="S" && !in_array($this->user,$this->super_users)) {
			$this->login_error++;
			$this->failCompany("SUSPENDED");
			$this->killMe("suspended|".$cmp_details['cmpname']." (".strtoupper($this->cc).")");
		} else {
			$_SESSION['cc'] = strtolower($this->cc);
			$this->cdb = new ASSIST_DB("client",strtolower($this->cc));
			$this->cmp_details = $cmp_details;
			$this->bp_details = $this->getBPAdetails($this->cmp_details['cmpreseller'],$this->cmp_details['cmp_logo_reseller']);
			$this->site_details = array(
				'name'=>$this->bp_details['site_name'],
				'logo'=>$this->bp_details['assist_logo'],
				'code'=>$this->bp_details['site_code'],
				'logo_link'=>"pics/bp_logos/".$this->bp_details['assist_logo'],
				'given_code'=>$this->request['site_code'],
			);
			//$this->login_error_message = serialize($this->bp_details);
		}
	}

	private function failCompany($failure_step="") {
		$attempt_details = base64_encode(serialize($this->submit_data));
		$sql = "INSERT INTO assist_company_login_errors VALUES (null, now(), '$failure_step','$attempt_details')";
		$this->mdb->db_insert($sql);
	}

	private function validateUser() {
		if($this->user=="admin") {
			$this->is_admin = true;
			$this->user_details = array(
				'tkid' => "0000",
				'tkn' => "Assist Administrator",
				'tkpwd' => $this->cmp_details['cmpadminpwd'],
				'tkstatus' => 1,
				'tkemail' => $this->cmp_details['cmpadminemail'],
				'tklogin' => 1,
				'tkpwdyn' => "N",
				'tklock' => "N",
				'tklocktime'=>"",
				'tkloginwindow'=> "",
				'pwd_security_check'=>"passed",
			);
		} else {
			$sql = "SELECT tkid, CONCAT(tkname, ' ', tksurname) as tkn, tkpwd, tkstatus, tkemail, tklogin, tkpwdyn, tkpwddate, tklock, tklocktime, tkloginwindow, tkpwdanswer1 as pa1, tkpwdanswer2 as pa2, tkpwdanswer3 as pa3 FROM assist_".strtolower($this->cc)."_timekeep WHERE tkuser = '".$this->user."'";
			$rs = $this->cdb->db_get_num_rows($sql);
			if($rs!=1) {
				$activity_id = $this->logAccessAttempt(false,"USER");
				$this->logFailedUserName($this->user, $rs, $activity_id);
				$this->login_error_message = "There is an error with your user account.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
				$this->killMe();
			} else {
				$one = $this->cdb->mysql_fetch_one($sql);
				if($this->convertSecurity($one['pa1']) == $this->convertSecurity($one['pa2']) && $this->convertSecurity($one['pa2']) == $this->convertSecurity($one['pa3'])) {
					$one['pwd_security_check'] = "failed";
				} else {
					$one['pwd_security_check'] = "passed";
				}
				$this->user_details = $one;
				//$this->login_error_message = "There is an error with your user account.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".".serialize($one);
				//$this->killMe();
			}
		}
	}

	private function convertSecurity($s) {
		return str_ireplace(' ', '', strtolower($s));
	}

	private function validateActiveUser() {
		if($this->user_details['tkstatus']!=1) {
			$this->login_error_message = "Your account is not active.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
			//LOG
			$this->logAccessAttempt(false,"S_".$this->user_details['tkstatus']);
			$this->killMe();
		}
	}

	private function validateAccountStatus() {
		if($this->user_details['tklock']=="Y") {
			//check if tklock_time == 0
			if($this->user_details['tklocktime']=="0000-00-00 00:00:00" || $this->user_details['tklocktime']=="0" || strlen($this->user_details['tklocktime'])==0 || strtotime($this->user_details['tklocktime'])==0) {
				$this->login_error_message = "Your account has been locked.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
				$this->logAccessAttempt(false,"LOCKED");
				$this->killMe();
			} else {
				//get system lockout duration
				$system_lockout_duration = $this->system_settings['att_lockout_duration']['setting'];
				$duration = ($_SERVER['REQUEST_TIME'] - strtotime($this->user_details['tklocktime']))/60;  // ( now(seconds) - lockout time (seconds) ) / 60 = minutes
				$system_lockout_counter = $this->system_settings['att_number']['setting'];
				//if lockout has expired
				if($duration >= $system_lockout_duration) {
					//unlock user & reset lockout time
					$sql = "UPDATE assist_".$this->cc."_timekeep SET tklock = 'N', tklocktime = '' WHERE tkid = '".$this->user_details['tkid']."'";
					$this->cdb->db_update($sql);
					//log unlock
					$obj_id = $this->user_details['tkid'];
					$type = "L";
					$field = serialize(array("tklock","tklocktime"));
					$new = serialize(array("N","0"));
					$old = serialize(array($this->user_details['tklock'],$this->user_details['tklocktime']));
					$this->logUserChange($obj_id,$type,$field,$new,$old,"Unlocked user due to expiration of lockout duration of ".$system_lockout_duration.".",$sql);
				} else {
					$this->login_error_message = "Your account has been temporarily locked due to ".$system_lockout_counter." failed login attempts.<br />Please try again in $system_lockout_duration minutes.<br />For further assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
					$this->logAccessAttempt(false,"LOCKOUT");
					$this->killMe();
				}
			}
		}
	}

	private function validatePwd() {
		if($this->pwd != $this->user_details['tkpwd']) {
			$activity_id = $this->logAccessAttempt(false,"PWD");
			$this->logFailedPassword($this->user_details['tkid'],$this->pwd,$this->user_details['tkpwd'],$activity_id);
			$this->killMe();
			//Get system login counter setting
			$system_lockout_counter = $this->system_settings['att_number']['setting'];
			$system_lockout_counter_duration = $this->system_settings['att_counter_duration']['setting'];
			//Get login counter
			$counter = $this->getLoginAttemptCounter();
			//if login attempts >= system login setting
			if($counter >= $system_lockout_counter) {
				//lock user
				$sql = "UPDATE assist_".$this->cc."_timekeep SET tklock = 'Y', tklocktime = now() WHERE tkid = '".$this->user_details['tkid']."'";
				$this->cdb->db_update($sql);
				//log lock
				$obj_id = $this->user_details['tkid'];
				$type = "L";
				$field = serialize(array("tklock","tklocktime"));
				$new = serialize(array("Y",date("Y-m-d H:i:s")));
				$old = serialize(array($this->user_details['tklock'],$this->user_details['tklocktime']));
				$this->logUserChange($obj_id,$type,$field,$new,$old,"Locked user due to $counter incorrect login attempt(s) within $system_lockout_counter_duration minutes in excess of the system setting of $system_lockout_counter incorrect login attempt(s).",$sql);
			}
		}
	}

	private function validateNumberLoggedIn() {
		$sql = "SELECT * FROM `assist_".$this->cc."_timekeep_activity` WHERE `tkid` = '".$this->user_details['tkid']."' ORDER BY `date` DESC LIMIT 1";
		$one = $this->cdb->mysql_fetch_one($sql);

		if(substr($one['action'],0,3)=="OUT" || substr($one['action'],0,7)=="IN_FAIL") {
			return true;
		} else {
			$d = strtotime($one['date']);
			$now = strtotime(date("d F Y H:i:s"));
			$inact_timeout = $this->system_settings['inact_duration']['setting']*60;
			$diff = $now - $d;
			if($diff > $inact_timeout) {
				return true;
			} else {
				$this->login_error_message = "Only 1 ".($this->isAdmin() ? "Admin" : "Support")." super user can be logged in at any one time.";
				$this->killMe();
				return false;
			}
		}

	}

	private function validateLoginWindow() {
		if(!$this->isAdmin() && strtotime($this->user_details['tkloginwindow']) < $this->today && !($this->user_details['tklogin']>0)) {
			$this->login_error_message = "Your first-time login window has expired and your account has been locked.<br />For assistance, please contact your Business Partner, ".$this->bp_details['cmpname'].", at ".implode(" or ",$this->bp_details['contact']).".";
			//LOG failure
			$this->logAccessAttempt(false,"NEW");
			$this->killMe();
		}
	}

	private function checkPasswordAge() {
		if($this->user_details['tkpwdyn'] != "Y") {
			$system_password_age = $this->system_settings['pwd_age']['setting'];
			if($system_password_age>0) {
				$pwd_date = ($this->user_details['tkpwddate']);
				if(!$this->checkBlankDate($pwd_date)) {
					$pwd_date = strtotime($pwd_date);
					$now = $_SERVER["REQUEST_TIME"];
					$diff = ($now - $pwd_date)/(3600*24);
					if($diff>$system_password_age) {
						$this->user_details['tkpwdyn'] = "A";
					}
				} else {
					$sql = "UPDATE assist_".$this->cdb->getCmpCode()."_timekeep SET tkpwddate = now() WHERE tkid = '".$this->user_details['tkid']."'";
					$this->cdb->db_update($sql);
				}
			}
		}
	}

	private function isReseller() {
		//if($this->user!="admin" && $this->user!="support") {
		$sql = "SELECT action_reseller FROM assist_reseller WHERE cmpcode = '".strtoupper($this->cc)."' AND active = true";
		$rs = $this->mdb->db_query($sql);
		if(mysqli_num_rows($rs)>0) {
			$r = mysqli_fetch_assoc($rs);
			$this->is_reseller = $r['action_reseller'];
		}
		//}
		//$this->killMe("resel");
	}

	private function isSupport() {
		if($this->user=="support") {
			$cmpcode = strtolower($this->cc);
			$tkid = $this->user_details['tkid'];
			$this->is_support = 1;
			$sql = "INSERT INTO assist_".strtolower($this->cc)."_menu_modules_users (usrmodid,usrmodref,usrtkid)
					SELECT mm.modid, m.modref, '".$tkid."' FROM assist_menu_modules m INNER JOIN  assist_".$cmpcode."_menu_modules mm ON m.modid = mm.modmenuid
					WHERE m.modyn = 'Y' AND m.modref NOT IN (select usrmodref from assist_".$cmpcode."_menu_modules_users WHERE usrtkid = '".$tkid."')";
			$this->cdb->db_insert($sql);
			return true;
		} else {
			return false;
		}
	}

	private function isAdmin() {
		return $this->is_admin;
	}

	private function getTerminology() {
		$sql = "SELECT field, value FROM assist_".strtolower($this->cc)."_setup WHERE ref = 'IA_TERM'";
		$ct = $this->cdb->mysql_fetch_fld2_one($sql,"field","value");
		if(count($ct)==0) {
			$sql2 = "INSERT INTO `assist_".strtolower($this->cc)."_setup` (`id`, `ref`, `refid`, `value`, `comment`, `field`) VALUES (null, 'IA_TERM', 0, 'Department', 'Name of Department', 'DEPT'), (null, 'IA_TERM', 0, 'Section', 'Name of Section', 'SUB')";
			$this->cdb->db_insert($sql2);
			$ct = $this->cdb->mysql_fetch_fld2_one($sql,"field","value");
		}
		return $ct;
	}

	private function setSession() {
		$_SESSION['cc'] = strtolower($this->cc);
		$_SESSION['bpa'] = $this->cmp_details['cmpreseller'];
		$_SESSION['tid'] = $this->user_details['tkid'];
		$_SESSION['tkn'] = $this->user_details['tkn'];
		$_SESSION['original_tkn'] = $this->user_details['tkn'];
		$_SESSION['pwd_security_check'] = $this->user_details['pwd_security_check'];
		$_SESSION['fpd_displayed'] = false;
		$_SESSION['tku'] = $this->user;
		$_SESSION['ref'] = "main";
		$_SESSION['session_timeout']['setting'] = $this->system_settings['inact_duration']['setting']*60;
		if($this->isAdmin() || $this->isSupport()) {
			$_SESSION['session_timeout']['expiration'] = strtotime(date("d F Y H:i:s"))+(60*60); //set to 1 hour for default timeout until second validation kicks in
			$_SESSION['system_settings'] = base64_encode(serialize($this->system_settings));
		} else {
			$_SESSION['session_timeout']['expiration'] = strtotime(date("d F Y H:i:s"))+($this->system_settings['inact_duration']['setting']*60);
		}
		$_SESSION['session_timeout']['current'] = strtotime(date("d F Y H:i:s"));
		$_SESSION['ia_cmp_reseller'] = $this->cmp_details['cmpreseller'];
		$_SESSION['ia_cmp_logo_reseller'] = $this->cmp_details['cmp_logo_reseller'];
		$_SESSION['ia_cmp_dispname'] = $this->cmp_details['cmpdispname'];
		$_SESSION['ia_cmp_name'] = $this->cmp_details['cmpname'];
		$_SESSION['ia_cmp_logo'] = $this->cmp_details['cmplogo'];
		$_SESSION['ia_reseller'] = $this->is_reseller;
		$_SESSION['ia_support'] = $this->is_support;
		$_SESSION['screen'] = array('height'=>$this->screen['h'],'width'=>$this->screen['w']);
		$_SESSION['cmp_terminology'] = $this->getTerminology();
		$_SESSION['DISPLAY_INFO']['site_code'] = $this->site_details['code'];
		$this->getUserProfile(strtolower($this->cc),$this->user_details['tkid']);
	}

	private function nextStep() {
		$tkid = $this->user_details['tkid'];
		$cmpcode = strtolower($this->cc);
		//update the login count
		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tklogin = tklogin + 1, tklogindate = '".strtotime(date("d F Y H:i:s"))."' WHERE tkid = '$tkid'";
		$this->cdb->db_update($sql);
		$this->logAccessAttempt(true);
		if($this->isAdmin() || $this->isSupport() || ($this->user_details['tklogin']>0 && !in_array($this->user_details['tkpwdyn'],array("A","Y")))) {
			$this->next_page = "login.html";
		} else {
			if($this->user_details['tklogin']>0 && in_array($this->user_details['tkpwdyn'],array("A","Y"))) {
				$this->next_page = "password1.php?reason=".$this->user_details['tkpwdyn'];
			} else {
				$this->next_page = "terms_user.php";
			}
		}
	}

	public function getValidationResult() {
		/*$data = array(
			1,
			"errormessage",
			"nextpage"
		);*/
		//$data[0] = $this->login_error;
		//$data[1] = $this->login_error_message;
		//$data[2] = $this->next_page;
		$data = array(
			$this->login_error,
			$this->login_error_message,
			$this->next_page,
			$this->require_second_validation,
			$this->system_settings,
			$this->redirect,
			$this->redirect_site
		);
		return $data;
	}

	private function logAccessAttempt($success,$reason="") {
		if($success==false) {
			$code = "IN_FAIL_".$reason;
		} else {
			$code = "IN";
		}
		if(isset($_COOKIE['PHPSESSID']) && strlen($_COOKIE['PHPSESSID'])) {
			$unique_id = "PHPSESSID=".$_COOKIE['PHPSESSID'];
		} elseif(isset($_SERVER["HTTP_COOKIE"]) && strlen($_SERVER["HTTP_COOKIE"])) {
			$unique_id = $_SERVER["HTTP_COOKIE"];
		} else {
			$unique_id = "";
		}

		$tkid = isset($this->user_details['tkid']) ? $this->user_details['tkid'] : "X";
		$tkn = isset($this->user_details['tkn']) ? $this->user_details['tkn'] : "X";
		$cmpcode = strtolower($this->cc);

		$sql = "INSERT INTO assist_".$cmpcode."_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) 
				VALUES (null,now(),'$tkid','$tkn','$code',1,'".$unique_id."|".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."')";
		$id = $this->cdb->db_insert($sql);
		if($reason=="PWD" || $reason == "USER") {
			return $id;
		}
	}

	private function getLoginAttemptCounter() {
		$system_lockout_counter_duration = $this->system_settings['att_counter_duration']['setting'];
		$tkid = $this->user_details['tkid'];
		$cmpcode = strtolower($this->cc);

		/*		$sql = "SELECT count(id) as lc FROM assist_".$cmpcode."_timekeep_activity
						WHERE tkid = '$tkid'
						AND action = 'IN_FAIL_PWD'
						AND `date` > DATE_SUB(now(), INTERVAL '".$system_lockout_counter_duration.":0' MINUTE_SECOND)";
				$counter = $this->cdb->mysql_fetch_one_value($sql,"lc");
		*/
		$sql = "SELECT action FROM assist_".$cmpcode."_timekeep_activity
				WHERE tkid = '$tkid' 
				AND (action = 'IN_FAIL_PWD' OR action = 'IN') 
				AND `date` > DATE_SUB(now(), INTERVAL '".$system_lockout_counter_duration.":0' MINUTE_SECOND)
				ORDER BY `date` DESC ";
		//$counter = $this->cdb->mysql_fetch_one_value($sql,"lc");
		$rows = $this->cdb->mysql_fetch_all($sql);
		$counter = 0;
		foreach($rows as $r) {
			if($r['action']=="IN_FAIL_PWD") {
				$counter++;
			} elseif($r['action']=="IN") {
				break;
			}
		}


		return $counter;
	}


	public function logUserUnlock($obj_id,$old_locktime,$txt,$sql2,$create_cdb=false,$cc=""){
		$type = "L";
		$field = serialize(array("tklock","tklocktime"));
		$new = serialize(array("N","0"));
		$old = serialize(array("Y",$old_locktime));
		if($create_cdb) {
			$this->cdb = new ASSIST_DB("client",strtolower($cc));
			$this->cc = strtolower($cc);
		}
		$this->logUserChange($obj_id, $type, $field, $new, $old, $txt, $sql2);
	}

	private function logUserChange($obj_id,$type,$field,$new,$old,$txt,$sql2) {
		$c = array(
			'type'=>$type,
			'fld'=>$field,
			'new'=>$new,
			'old'=>$old,
			'txt'=>$txt
		);
		$sql = "INSERT INTO assist_".$this->cc."_timekeep_log 
				(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
				VALUES
				(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".addslashes($sql2)."')";
		$this->cdb->db_insert($sql);

	}

	private function logFailedPassword($tkid,$attempt,$stored,$activity_id) {
		$details = array(
			'attempt'=>$attempt,
			'attempt_raw'=>addslashes($this->convertFromHex($attempt)),
			'stored'=>$stored,
			'stored_raw'=> addslashes($this->convertFromHex($stored)),
		);
		$sql = "INSERT INTO assist_".$this->cc."_timekeep_activity_details (tkid,datetime,activity_id,details) VALUES ('$tkid',now(),$activity_id,'".serialize($details)."')";
		$this->cdb->db_insert($sql);
	}
	private function logFailedUserName($user,$num_rows,$activity_id) {
		$details = array(
			'user'=>$user,
			'rows'=>$num_rows,
		);
		$sql = "INSERT INTO assist_".$this->cc."_timekeep_activity_details (tkid,datetime,activity_id,details) VALUES ('',now(),$activity_id,'".serialize($details)."')";
		$this->cdb->db_insert($sql);
	}

	private function getSystemSettings() {
		if($this->isAdmin() || $this->isSupport()) {
			$c = 1;
		} else {
			$sql = "SELECT count(tkid) as c FROM assist_".$this->cdb->getCmpCode()."_timekeep_superusers WHERE tkid = '".$this->user_details['tkid']."' AND status = 2";
			$c = $this->cdb->mysql_fetch_one_value($sql, "c");
		}
		if($c>0) {
			$s = " super_user as setting";
		} else {
			$s = " normal_user as setting";
		}
		$sql = "SELECT *, $s FROM assist_".$this->cc."_timekeep_setup";
		$this->system_settings = $this->cdb->mysql_fetch_all_by_id($sql, "code");
	}


	private function checkBlankDate($d) {
		if($d=="0000-00-00 00:00:00" || $d=="0" || strlen($d)==0 || strtotime($d)==0) {
			return true;
		}
		return false;
	}



	function __destruct() {

	}
}



?>