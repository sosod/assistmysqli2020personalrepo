<?php 

class ASSIST_MASTER_BUSINESSSECTORS extends ASSIST_MASTER {
	const SECTION = "BUSSEC";
	const TABLE = "master_businesssectors";
	protected $title = "Business Sectors";
	
	const ID_FLD = "id";
	const NAME_FLD = "value";
	const SORT_FLD = "value";
	const STATUS_FLD = "status";
	
	const SELECT_NAME = "value as name";
	
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;
	
	const CAN_SORT = true;
	protected $fields = array();
	
	protected $db;
	
	public function __construct() {
		parent::__construct();
		$this->db_table = strtolower("assist_".$this->getCmpCode()."_".self::TABLE);
		$this->db = new ASSIST_DB("client");
		$this->sort_by = array("sort","value");
		$this->fields = array(
			'id'=>array(
				'type'=>"REF",
				'heading'=>"Ref",
				'field'=>"id",
			),
			'value'=>array(
				'type'=>"MEDTEXT",
				'heading'=>"Value",
				'field'=>"value",
				'default'=>"",
			),
			'status'=>array(
				'type'=>"STATUS",
				'heading'=>"Status",
				'field'=>"status",
				'default'=>2,
			),
		);
	}
	
	public function getObject($var) {
		$id = $var['object_id'];
		$sql = "SELECT * FROM ".$this->getTable()." WHERE ".$this->getFld("ID")." = ".$id;
		return $this->db->mysql_fetch_one($sql);
	}
	
	public function addObject($var) {
		foreach($var as $key => $v) { $var[$key] = $this->code($v); }
		$sql = "INSERT INTO ".$this->getTable()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db->db_insert($sql);
		if($id>0) {
			$transaction = "Added new ".$this->getTitle()." ".$id.": '".$this->decode($var[self::NAME_FLD])."'";
			$this->addLog(self::SECTION, $sql, $id, self::LOG_CREATE, array_keys($var), $transaction, "", $var);
			return array("ok","Successfully added".substr($transaction,5));
		}else {
			return array("error","An error occured.  Please reload the page and try again.");
		}
	}
	
	public function editObject($var) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$old = $this->db->mysql_fetch_one("SELECT * FROM ".$this->getTable()." WHERE ".$this->getFld("ID")." = ".$object_id);
		
		foreach($var as $key => $v) { $var[$key] = $this->code($v); }

		$changes = array();
		foreach($var as $key => $v) {
			if($v!=$old[$key]) {
				$changes['new'][$key] = $v;
				$changes['old'][$key] = $old[$key]; 
			}
		}
		
		if(count($changes)==0) {
			return array("info","No change was found to be saved.");
		} else {
			$sql = "UPDATE ".$this->getTable()." SET ".$this->convertArrayToSQL($var)." WHERE ".$this->getFld("ID")." = ".$object_id;
			$mnr = $this->db->db_update($sql);
			if($mnr>0) {
				$transaction = "Changed ".$this->getTitle()." ".$object_id." to '".$this->decode($changes['new'][self::NAME_FLD])."' from '".$this->decode($changes['old'][self::NAME_FLD])."'";
				$this->addLog(self::SECTION, $sql, $object_id, self::LOG_EDIT, array_keys($changes['new']), $transaction, $changes['old'], $changes['new']);
				return array("ok",$this->getTitle()." ".$object_id." successfully changed.");
			}
		}
		return array("error","An error occured.  Please reload the page and try again.");
	}

	public function deactivateObject($var) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$old = $this->db->mysql_fetch_one("SELECT * FROM ".$this->getTable()." WHERE ".$this->getFld("ID")." = ".$object_id);
		$changes = array();
		if($old[self::STATUS_FLD]!=self::INACTIVE) {
			$changes['new'][self::STATUS_FLD] = self::INACTIVE;
			$changes['old'][self::STATUS_FLD] = $old[self::STATUS_FLD];
		}
		
		if(count($changes)==0) {
			return array("info","No change was found to be saved.");
		} else {
			$sql = "UPDATE ".$this->getTable()." SET ".$this->getFld("STATUS")." = ".self::INACTIVE." WHERE ".$this->getFld("ID")." = ".$object_id;
			$mnr = $this->db->db_update($sql);
			if($mnr>0) {
				$transaction = "Deactivated ".$this->getTitle()." ".$object_id;
				$this->addLog(self::SECTION, $sql, $object_id, self::LOG_DEACTIVATE, array_keys($changes['new']), $transaction, $changes['old'], $changes['new']);
				return array("ok",$this->getTitle()." ".$object_id." successfully deactivated.");
			}
		}
		return array("error","An error occured.  Please reload the page and try again.");
	}
	

	public function restoreObject($var) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$old = $this->db->mysql_fetch_one("SELECT * FROM ".$this->getTable()." WHERE ".$this->getFld("ID")." = ".$object_id);
		$changes = array();
		if($old[self::STATUS_FLD]!=self::ACTIVE) {
			$changes['new'][self::STATUS_FLD] = self::ACTIVE;
			$changes['old'][self::STATUS_FLD] = $old[self::STATUS_FLD];
		}
		
		if(count($changes)==0) {
			return array("info","No change was found to be saved.");
		} else {
			$sql = "UPDATE ".$this->getTable()." SET ".$this->getFld("STATUS")." = ".self::ACTIVE." WHERE ".$this->getFld("ID")." = ".$object_id;
			$mnr = $this->db->db_update($sql);
			if($mnr>0) {
				$transaction = "Restored ".$this->getTitle()." ".$object_id;
				$this->addLog(self::SECTION, $sql, $object_id, self::LOG_RESTORE, array_keys($changes['new']), $transaction, $changes['old'], $changes['new']);
				return array("ok",$this->getTitle()." ".$object_id." successfully restored.");
			}
		}
		return array("error","An error occured.  Please reload the page and try again.");
	}











	public function getFld($f) { 
		switch(strtoupper($f)) {
			case "ID": return self::ID_FLD; break;
			case "SORT": return self::SORT_FLD; break;
			case "NAME": return self::NAME_FLD; break;
			case "STATUS": return self::STATUS_FLD; break;
			case "ACTIVE": return self::ACTIVE; break;
			case "INACTIVE": return self::INACTIVE; break;
			case "DELETED": return self::DELETED; break;
			case "TABLE": return self::TABLE; break;
		}
	}



	//[JC - 29 Oct 2013] old function - not sure where in use
	public function fetchAll() {
		$sql = "SELECT *, ".self::SELECT_NAME." FROM ".$this->getTable()." WHERE ".self::STATUS_FLD." & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY ".self::SORT_FLD;
		return $this->db->mysql_fetch_all_fld($sql,self::ID_FLD);
	}
	


	public function getStatusText($s) {
		switch($s) {
			case self::ACTIVE:
				return "Active";
				break;
			case self::INACTIVE:
				return "Inactive";
				break;
			case self::DELETED:
				return "Deleted";
				break;
		}
		return "<span class=required>ERROR</span>";
	}
	
}

?>