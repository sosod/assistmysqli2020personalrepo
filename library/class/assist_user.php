<?php
/** CLASS TO MANAGE USER PROCESSES
Created on: 7 August 2014
Created by: Janet Currie



	private $site_code;
	private $cmpcode;
	private $cmp_details = array();
	private $tkid;
	private $security_questions;
		//Array Password security questions
	private $default_page_length = 15;
	

	public function __construct($sc="")
	
	public function getSecurityQuestions() 
		//Return $security_questions array
	
	public function validateForgottenPasswordResetRequest($cmpcode,$user,$question,$answer) 
	
	private function validateCompany($cmpcode) 

	private function validateUser($user_name,$security_question,$security_answer)
	
	private function changePassword() 
	
	function getSiteName()

	function getCmpAdmin() 

	function getCmpReseller() 
	
	function getBPAdetails($bpa="") 
	
	public function mainProfileActionsToDisplay() 
		//Function to read main user profile
	
	function __destruct() 


**/


class ASSIST_USER extends ASSIST_HELPER {
	private $site_code;
	private $cmpcode;
	private $cmp_details = array();
	private $tkid;
	private $security_questions;
	private $default_page_length = 15;
	

	public function __construct($sc="") {
		$this->site_code = $sc;
		$this->security_questions = array(
			'1'=>"What was your mother's maiden name?",
			'2'=>"What was the colour of your first car?",
			'3'=>"What was the name of your high school?",
		);
	}
	
	/**
	 * Return $security_questions array
	 */
	public function getSecurityQuestions() {
		return $this->security_questions;
	}
	
	/**
	 * Validate user request to reset forgotten password using security question answer
	 * @param (String) $cmpcode
	 * @param (String) $user
	 * @param (Int) $question = Array index of selected security question
	 * @param (String) $answer = Security answer provided by user
	 * 
	 * @return (Array) $result 
	 */
	public function validateForgottenPasswordResetRequest($cmpcode,$user,$question,$answer) {
		$companyValid = $this->validateCompany($cmpcode);
		if($companyValid===true) {
			$userValid = $this->validateUser($user,$question,$answer);
			if($userValid===true) {
				$result = $this->changePassword();
				return $result;
			} else {
				return $userValid;
			}
		} else {
			return $companyValid;
		}
		return array("error","Validation failed.  Please try again.");
	}
	
	/**
	 * Verify company code provided by user
	 * @param (String) $cmpcode
	 * 
	 * @return Boolean true || Array
	 */
	private function validateCompany($cmpcode) {
		$result_error = array("error","Invalid Company Code. Please try again.");
		$db = new ASSIST_DB("master");
		$sql = "SELECT * FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";  $result_error[] = $sql; $result_error[] = $db->getDBName();
		$results = $db->mysql_fetch_all($sql); 
		if(count($results)==0) {
			$result_error[]="Assist: No company found.";
			return $result_error; 
		} elseif(count($results)>1) {
			$result_error[] = "Assist: More than 1 company record returned";
			return $result_error;
		} else {
			$r = $results[0];  
			if($r['cmpstatus']=="N") {
				$result_error[] = "Assist: Company status = N";
				return $result_error;
			} elseif($r['cmpstatus']==2) {
				return array("error","Your Assist platform has been suspended.  Please contact your Assist Administrator or Assist Business Partner for further assistance.");
			} else {
				$this->cmpcode = strtolower($cmpcode);
				$this->cmp_details = $r;
				return true;
			}
		}
		return $result_error;
	}
	
	/**
	 * Verify username provided by user
	 * @param (String) $user_name
	 * @param (Int) $security_question
	 * @param (String) $security_answer
	 */
	private function validateUser($user_name,$security_question,$security_answer) {
		$result_error = array("error","Unable to verify your user name.  Please try again.");
		$db = new ASSIST_DB("client",$this->cmpcode);
		$sql = "SELECT * FROM assist_".$this->cmpcode."_timekeep WHERE tkuser = '".strtolower($user_name)."'";
		$results = $db->mysql_fetch_all($sql);
		if(count($results)==0) {
			$result_error[] = "Assist: No user record found.";
			return $result_error;
		} elseif(count($results)>1) {
			$result_error[] = "Assist: More than 1 user record returned.";
			return $result_error;
		} else {
			$r = $results[0];
			if($r['tkstatus']==0) {
				return array("error","Your login has been cancelled/terminated.  Please contact your Assist Administrator or Assist Business Partner for further assistance.");
			} elseif($r['tklock']=="Y") {
				return array("error","Your login has been suspended/locked.  Please contact your Assist Administrator or Assist Business Partner for further assistance.");
			} else {
				$saved_answer = str_ireplace(" ","",strtolower($r['tkpwdanswer'.$security_question]));
				$test_answer = str_ireplace(" ","",strtolower($security_answer));
				$hex_save = $this->convertToHex($saved_answer,"");
				$hex_test = $this->convertToHex($test_answer,"");
				if($hex_test==$hex_save) {
					$this->tkid = $r['tkid'];
					return true;
				} else {
					return array("error","Invalid security question answer.  Please try again.");
				}
			}
		}
		return $result_error;
	}
	
	private function changePassword() {
			$alpha = array("a","B","c","D","e","F","g","H","i","J","k","L","m","N","o","P","q","R","s","T","u","V","w","X","y","Z");
			$alphahex = array("61","42","63","44","65","46","67","48","69","4a","6b","4c","6d","4e","6f","50","71","52","73","54","75","56","77","58","79","5a");
			$tkpwd = "";
			$tkpwdhex = "";
			$i=1;
			while($i<9) {
				$p = rand(0,25);
				if(strlen($alpha[$p]) != 0) {
					$tkpwd = $tkpwd.$alpha[$p];
					$tkpwdhex = $tkpwdhex.$alphahex[$p];
					$alpha[$p] = "";
					$i++;
				}
			}
		$db = new ASSIST_DB("client",$this->cmpcode);
		$sql = "SELECT * FROM assist_".$this->cmpcode."_timekeep WHERE tkid = '".$this->tkid."'";
		$tk = $db->mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$this->cmpcode."_timekeep SET tkpwd = '".$tkpwdhex."', tkpwdyn = 'Y' WHERE tkid = '".$this->tkid."'";
		$db->db_update($sql);
		
		$cmpadmin = $this->getCmpAdmin();
		$bpa_details = $this->getBPAdetails();
		$site_name = $this->getSiteName();
		$site_name = "Assist";
		$site_code = $this->site_code;
		
		$to = $tk['tkemail'];
		$subject = $site_name." Password Reset";
				$message = "Dear ".$tk['tkname']." ".$tk['tksurname'].chr(10);
				$message.= " ".chr(10);
				$message.= "Your ".$site_name." password has been changed due to a Forgotten Password Request. ".chr(10);
				$message.= " ".chr(10);
				$message.= "Your temporary password is: ".$tkpwd.chr(10);
				$message.= "Reminder: Your username is ".$tk['tkuser']." and your company code is ".strtoupper($this->cmpcode).".".chr(10);
				$message.= "Please keep your login details confidential. ".chr(10);
				$message.= " ".chr(10);
				$message.= "To login go to: http://assist.action4u.co.za/"." ".chr(10);
				$message.= " ".chr(10);
				$message.= "For further assistance please contact: ".chr(10);
				$message.= "- Your Assist Administrator, ".$cmpadmin['name'].(strlen($cmpadmin['email'])>0 ? ", at ".$cmpadmin['email'] : ",")." or ".chr(10);
				$message.= "- Your ".$site_name." Business Partner, ".$bpa_details['cmpname'].", at ".$bpa_details['contact_display'].". ".chr(10);
				$message.= " ".chr(10);
				$message.= "Kind regards ".chr(10);
				$message.= "".$site_name." ".chr(10);
				$message.= " ".chr(10);
		$email = new ASSIST_EMAIL($to,$subject,$message);
		$email->sendEmail();
		//Mask $to email address
		$x = strlen($to);
		$to = "*".substr($to,1,(stripos($to,"@")+5));
		if($x>=(strlen($to)*2)) { $x/=2; }
		for($i=strlen($to2);$i<=$x;$i++) { $to.="*"; }
		return array("ok","Your password has been reset.  An email has been sent to '".$to."' with a temporary password.  Should you not receive the email or if the email address is incorrect, please contact your Assist Administrator or Assist Business Partner.");
		
	}
	
	function getSiteName() {
		$site_code = $this->site_code;
		$mdb = new ASSIST_DB("master");
		
		$sql = "SELECT r.*, c.cmpname FROM assist_reseller r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode WHERE site_code = '".$site_code."' AND active = 1";
		$site = $mdb->mysql_fetch_one($sql);

		if(!isset($site['site_name'])) {
			$site_code = $default_site_code;
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode WHERE r.id = 1 ";
			$site = $mdb->mysql_fetch_one($sql);
		}

		return $site['site_name'];
	}

	function getCmpAdmin() {
		if(!isset($this->cmp_details['cmpadmin']) || strlen($this->cmp_details['cmpadmin'])==0) {
			$mdb = new ASSIST_DB("master");
			$sql = "SELECT cmpadmin as name, cmpadminemail as email FROM assist_company WHERE cmpcode = '".strtoupper($this->cmpcode)."'";
			$row = $mdb->mysql_fetch_one($sql);
		} else {
			$row = array('name'=>$this->cmp_details['cmpadmin'],'email'=>$this->cmp_details['cmpadminemail']);
		}
		return $row;
	}

	function getCmpReseller() {
		if(!isset($this->cmp_details['cmpreseller']) || strlen($this->cmp_details['cmpreseller'])==0) {
			$mdb = new ASSIST_DB("master");
			$sql = "SELECT cmpreseller as reseller FROM assist_company WHERE cmpcode = '".strtoupper($this->cmpcode)."'";
			$row = $mdb->mysql_fetch_one($sql);
			$bpa = $row['reseller'];
		} else {
			$bpa = $this->cmp_details['cmpreseller'];
		}
		return $bpa;
	}
	function getCmpLogoReseller() {
		if(!isset($this->cmp_details['cmp_logo_reseller']) || strlen($this->cmp_details['cmp_logo_reseller'])==0) {
			$mdb = new ASSIST_DB("master");
			$sql = "SELECT cmp_logo_reseller as reseller FROM assist_company WHERE cmpcode = '".strtoupper($this->cmpcode)."'";
			$row = $mdb->mysql_fetch_one($sql);
			$bpa = $row['reseller'];
		} else {
			$bpa = $this->cmp_details['cmp_logo_reseller'];
		}
		return $bpa;
	}
	
	function getBPAdetails($bpa="",$logo_bpa="") {
		$bpa = strlen($bpa)>0 ? $bpa : $this->getCmpReseller();
		$logo_bpa = strlen($logo_bpa)>0 ? $logo_bpa : $this->getCmpLogoReseller();
		$mdb = new ASSIST_DB("master");
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r 
					INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
					WHERE r.cmpcode = '".strtoupper($bpa)."'";
			//$rs = AgetRS($sql);
			//	$bpa_details = mysql_fetch_assoc($rs);
			$bpa_details = $mdb->mysql_fetch_one($sql);
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r 
					INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
					WHERE r.cmpcode = '".strtoupper($logo_bpa)."'";
			$logo_bpa_details = $mdb->mysql_fetch_one($sql);

				$bpa_con = explode("|",$bpa_details['help_contact']);
				$bpa_contact = array();
				foreach($bpa_con as $b) {
					$b2 = explode("_",$b);
					$bpa_contact[] = $b2[1]." (".strtolower($b2[0]).")";
				}
				$bpa_details['contact'] = $bpa_contact;
				$bpa_details['contact_display'] = implode(" or ",$bpa_contact);
				if($bpa!=$logo_bpa) {
					$bpa_details['site_code'] = $logo_bpa['site_code'];
					$bpa_details['site_name'] = $logo_bpa['site_name'];
					$bpa_details['assist_logo'] = $logo_bpa['assist_logo'];
				}
		return $bpa_details;
	}
	
	
	/** 
	 * Function to read main user profile
	 */
	public function mainProfileActionsToDisplay() {
		if(!isset($_SESSION['USER_PROFILE'])) {
			return $this->default_page_length;
		} else {
			$p = $_SESSION['USER_PROFILE'];
			if(!isset($p[1]) || !isset($p[1]['field2']) || !$this->checkIntRef($p[1]['field2'])) {
				return $this->default_page_length;
			} else {
				return $p[1]['field2'];
			}
		}
	}
	

	
	/*
	 * Function to read a user / users contact details
	 * REQUIRED BY:
	 * 		HELPDESK
	 * 
	 * REQUIRES:
	 * 		Existing required/included ASSIST_DB & ASSIST_DBCONN class 
	 * 
	 * @param REQUIRED	$ti 		String || Array		user id / ids to get data
	 * @param 			$cmpcode	String				company code to search if not logged in company
	 */
	public function getContactDetails($ti,$cmpcode="") {
		//If no Company Code sent then use currently logged in company to create database else use specified company
		if(strlen($cmpcode)==0) {
			$db = new ASSIST_DB();
			$cc = $db->getCmpCode();
		} else {
			$db = new ASSIST_DB("client",$cmpcode);
		}
		
		//Get Id, email address, cellphone, name & status of user(s) according to tkid submitted
		$sql = "SELECT 
				  tkid as id
				  , tkemail as email
				  , tkmobile as mobile
				  , CONCAT(tkname, ' ',tksurname) as name
				  , IF(tkstatus=1,1,0) as active  
				FROM assist_".$db->getCmpCode()."_timekeep 
				WHERE ";
		//Create SQL filter for user ids
		if(is_array($ti)) {
			$run = false;
			if(count($ti)>0) {
				$run = true;
				$where = "tkid IN ('".implode("','",$ti)."')";
			}
		} else {
			$run = false;
			if(count($ti)>0) {
				$run = true;
				$where = "tkid = '".$ti."'";
			}
		}
		
		//If valid user ids submitted then run sql
		if($run) {
			$sql.= $where;
			return $db->mysql_fetch_all_by_id($sql, "id");
		}
		
		return array();
		
	}
	
	
	
	function __destruct() {
	}
}



?>