<?php 

class ASSIST_MASTER_WEIGHTS extends ASSIST_MASTER {
	
	const TABLE = "master_weights";
	
	const ID_FLD = "id";
	const NAME_FLD = "name";
	const SORT_FLD = "rating";
	const STATUS_FLD = "status";
	
	const SELECT_NAME = "IF(LENGTH(client_name)>0,client_name,default_name) as name";
	
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;
	
	public function __construct() {
		parent::__construct();
		$this->db_table = strtolower("assist_".$this->getCmpCode()."_".self::TABLE);
		$this->sort_by = array("rating","name");
	}
	
	//[JC - 29 Oct 2013] old function - not sure where in use
	public function fetchAll() {
		$db = new ASSIST_DB("client");
		$sql = "SELECT *, ".self::SELECT_NAME." FROM ".$this->getTable()." WHERE ".self::STATUS_FLD." & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY ".self::SORT_FLD;
		return $db->mysql_fetch_all_fld($sql,self::ID_FLD);
	}
	
	
	
	
	
	
	public function getFld($f) { 
		switch(strtoupper($f)) {
			case "ID": return self::ID_FLD; break;
			case "SORT": return self::SORT_FLD; break;
			case "NAME": return self::NAME_FLD; break;
			case "STATUS": return self::STATUS_FLD; break;
			case "ACTIVE": return self::ACTIVE; break;
			case "TABLE": return self::TABLE; break;
		}
	}
	
}

?>