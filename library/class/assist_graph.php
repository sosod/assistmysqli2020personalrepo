<?php
/** public functions:
**/



class ASSIST_GRAPH {
	//graph settings
	private $graph_type;		//PIE || BAR
	private $graph_format;	 	//JAVA || FLASH
	private $page_layout;		//PORT || LAND
	private $graph_stack_type;	//ABOVE || INLINE -> where to display the main graph

	//Bar graph settings
	private $bar_stack_type;	//REG || PERC
	private $display_bar_grid;
	private $max;				//maximum bars to display per page
	private $sub_ids;
	
	//page structure settings
	private $draw_header;
	private $has_main_graph;
	private $main_graph_id;
	private $has_sub_graph;
	
	//data to be input to draw graphs
	private $graph_categories;	//array([id]=>array('id'=>id,'text'=>name,'code'=>short code,'color'=>hex code)
	private $count;				//numbers to build graph array('main'=>array([code]=>int),'sub'=>array([s_id]=>array([code]=>int)))
	private $sub_list;			//array of sub graphs: array[id] => graph name
	
	//titles to display
	private $page_title;
	private $graph_title;
	private $blurb;
	private $main_title;
	private $sub_title;

	//class data
	private $main_chart_data;		//used for java graphs
	private $sub_chart_data;		//used for java graphs
	private $color_codes;
	
	public function __construct($i=0) {
		$this->graph_type = "PIE";
		$this->graph_format = "JAVA";
		$this->graph_stack_type = "ABOVE";
		$this->bar_stack_type = "PERC";
		$this->page_layout = "LAND";
		$this->graph_categories = array();
		$this->has_main_graph = true;
		$this->has_sub_graph = true;
		$this->count = array('main'=>array(),'sub'=>array());
		$this->sub_list = array();
		$this->main_chart_data = "";
		$this->sub_chart_data = "";
		$this->color_codes = array(
			'red'		=> "#CC0001",
			'orange'	=> "#FE9900",
			'green'	=> "#009900",
			'darkgreen'	=> "#005500",
			'blue'		=> "#000077",
			'grey'		=> "#999999",
			'gray'		=> "#999999",
			'charcoal'		=> "#333333",
		);
		$this->page_title = "";
		$this->graph_title = "";
		$this->blurb = "";
		$this->main_title = "";
		$this->sub_title = "";
		$this->draw_header = false;
		$this->main_graph_id = $i;
		$this->display_bar_grid = false;
		$this->setMaxRows();
		$this->sub_ids = array();
	}
	public function setGraphID($i) {
		$this->main_graph_id = $i;
	}
	public function setGraphAsPie() {
		$this->graph_type = "PIE";
		$this->graph_stack_type = "ABOVE";
		$this->setMaxRows();
	}
	public function setGraphAsBar() {
		$this->graph_type = "BAR";
		$this->bar_stack_type = "PERC";
		$this->setMaxRows();
	}
	public function setFormatAsJava() {
		$this->graph_format = "JAVA";
		//$this->page_layout = "LAND";
	}
	public function setFormatAsFlash() {
		$this->graph_format = "FLASH";
		//$this->page_layout = "LAND";
	}
	public function setLayoutAsPortrait() {
		$this->page_layout = "PORT";
	}
	public function setLayoutAsLandscape() {
		$this->page_layout = "LAND";
	}
	public function setHasMainGraph($has) {
		$this->has_main_graph = $has===true ? true : false;
	}
	//PIE ONLY
	public function setMainGraphAbove() {
		$this->graph_stack_type = "ABOVE";
	}
	public function setMainGraphInline() {
		$this->graph_stack_type = "INLINE";
	}
	//BAR ONLY
	public function setBarGraphByPercent() {
		$this->bar_stack_type = "PERC";
	}
	public function setBarGraphByNumber() {
		$this->bar_stack_type = "REG";
	}
	public function setDisplayBarGrid($b) {
		$this->display_bar_grid = $b;
	}
	private function setMaxRows() {
		$max = array();
		if($this->graph_type=="BAR") {
			switch($this->page_layout) {
			case "PORT":
				$max[0] = 6;
				$max[1] = 6;
				break;
			case "LAND":
			default:
				$max[0] = 8;
				$max[1] = 8;
				break;
			}
		} else {
			$max[0] = 1; $max[1] = 1;
		}
		$this->max = $max;
	}
	//
	public function setHasSubGraph($has) {
		$this->has_sub_graph = $has===true ? true : false;
	}
	public function setGraphCategories($a) {
		$this->graph_categories = $a;
		$this->checkColorCode();
	}
	public function setPageTitle($t) {
		$this->page_title = $t;
	} 
	public function setGraphTitle($t) {
		$this->graph_title = $t;
	} 
	public function setBlurb($t) {
		$this->blurb = $t;
	}
	public function setMainTitle($t) {
		$this->main_title = $t;
	}
	public function setSubTitle($t) {
		$this->sub_title = $t;
	}
	public function setMainCount($count = array()) {
		if((count($count)==0 || array_sum($count)==0) && count($this->count['sub'])>0) {
			$count = array();
			foreach($this->count['sub'] as $s) {
				foreach($s as $i => $c) {
					if(!isset($count[$i])) { $count[$i]=0; }
					$count[$i]+=$c;
				}
			}
		}
		$this->count['main'] = $count;
	}
	public function setSubCount($count) {
		$this->count['sub'] = $count;
		if(array_sum($this->count['main'])==0) {
			$this->setMainCount(array());
		}
	}
	public function setSubList($s) {
		$this->sub_list = $s;
	}
	public function setDrawHeader($h=true) {
		$this->draw_header = $h;
	}
	
	private function checkColorCode() {
		foreach($this->graph_categories as $g_id => $g) {
			if(isset($this->color_codes[strtolower($g['color'])])) {
				$this->graph_categories[$g_id]['color'] = $this->color_codes[strtolower($g['color'])];
			}
		}
	}
	
	
	
	
	
	private function generateJavaPieChartData($count) {
		$kr2 = array();
		foreach($this->graph_categories as $ky => $k) {
			$k['obj'] = isset($count[$k['code']]) ? $count[$k['code']] : 0;
			$val = array();
			foreach($k as $key => $s) {
				$val[] = $key.":\"".$s."\"";
			}
			$kr2[$ky] = implode(",",$val);
		}
		return "{".implode("},{",$kr2)."}";
	}
	private function generateJavaPieSubChartData() {
		$sub_count = $this->count['sub'];
		foreach($this->sub_list as $s_id => $s) { 
			if(isset($sub_count[$s_id])) { 
				$sub_chartData[$s_id] = $this->generateJavaPieChartData($sub_count[$s_id]);
			}
		}
		return $sub_chartData;
	}
	private function generateJavaPieMainChartData() {
		$count = $this->count['main'];
		$chartData = $this->generateJavaPieChartData($count);
		return $chartData;
	}

	private function generateJavaBarChartData() {
		$sub_count = $this->count['sub'];
		$sub_chartData = array();
				$max_count = 0;
				$row_count = 0;
				$sub_row = 0;
				foreach($this->sub_list as $s_id => $s) { 
					if(isset($sub_count[$s_id]) && array_sum($sub_count[$s_id])>0) { 
						$max_count++; if($max_count>$this->max[$row_count]) { $sub_row++; $sub_chartData[$sub_row] = array(); $this->sub_ids[$sub_row] = array(); $max_count = 1; $row_count = 1; }
						$s = html_entity_decode($s, ENT_QUOTES, "ISO-8859-1");
						$sval = explode(" ",$s);
						$s_name = "";
						$s_n = "";
						$sn_rows = 1;
						foreach($sval as $sv) {
							if(strlen($s_n.$sv)>18) {
								$s_name.=$s_n."\\n";
								$s_n = "";
								$sn_rows++;
							}
							$s_n.=" ".$sv;
							if($sn_rows>4) { $s_name = substr($s_name,0,-2); $s_n="..."; break; } //else {$s_n.=$sn_rows; }
						}
						$s_name.=" ".$s_n;
						//if($sn_rows>3) { $s_name = "\\n".$s_name; }
						$cD = "{text:\"".$s_name."\"";
						foreach($this->graph_categories as $ky=>$k) {
							//$ky = $k['code'];
							$k['id'] = $ky;
							$cD.= ",R".$k['id'].":".(isset($sub_count[$s_id][$k['code']]) ? $sub_count[$s_id][$k['code']] : 0);
						}
						$cD.= "}";
						$sub_chartData[$sub_row][] = $cD;
						$this->sub_ids[$sub_row][] = $s_id;
					}
			
				}
		return $sub_chartData;
	}
	
	
	
	
	
	
	public function drawPage() {
		if($this->draw_header===true) {
			echo $this->drawHeader();
		}
		echo $this->drawCSS();
		if(strlen($this->page_title)>0 || strlen($this->graph_title)>0) {
			if(strlen($this->page_title)>0) {
				echo "<h1 class=center style='margin-top: 0;margin-bottom: 0;'>".$this->page_title."</h1>";
			}
			if(strlen($this->graph_title)>0) {
				echo "<h2 class=center style='margin-top: 0;margin-bottom: 0;'>".$this->graph_title."</h2>";
			}
			echo "<p class=\"center i\" style=\"margin-top: 0px; font-size: 7pt;\">Report drawn on ".date("d F Y")." at ".date("H:i")."<br />".(strlen($this->blurb)>0 ? $this->blurb."." : "")."</p>";
		}
		switch($this->graph_format) {
		case "JAVA":
			if($this->has_main_graph===true) {
				$this->main_chart_data = $this->generateJavaPieMainChartData();
			}
			if($this->has_sub_graph===true) {
				if($this->graph_type=="PIE") {
					$this->sub_chart_data = $this->generateJavaPieSubChartData();
				} else {
					$this->sub_chart_data = $this->generateJavaBarChartData();
				}
			}
			//echo "<pre>"; print_r($this->sub_chart_data); echo "</pre>";
			break;
		case "FLASH":
		default:
			break;
		}
		if($this->graph_format=="JAVA") {
			$echo = "<script type=\"text/javascript\">
				var chart;
				var legend;";
			if($this->has_main_graph===true && strlen($this->main_chart_data)>0) {
				$echo.= "var chartData_".$this->main_graph_id." = [".$this->main_chart_data."];";
			}
			if($this->has_sub_graph===true && count($this->sub_chart_data) > 0) {
				if($this->graph_type=="BAR") {
					$echo.="var sub_chartData = new Array();";
				}
				foreach($this->sub_chart_data as $sc => $scd) {
					if($this->graph_type=="PIE") {
						$echo.= chr(10)."var sub_chartData_".$sc." = [".$scd."];";
					} else {
						$echo.= chr(10)."
						sub_chartData[".$sc."] = [".implode(",",$scd)."];";
					}
				}	 
			}
			switch($this->graph_type) {
				case "PIE":	$this->drawJavaPiePage($echo);	break;
				case "BAR":	$this->drawJavaBarPage($echo);	break;
			}
		} else {
			switch($this->graph_type) {
				case "PIE":	$this->drawFlashPiePage();	break;
				case "BAR":	$this->drawFlashBarPage();	break;
			}
		}
	}
	
	
	
/*** FLASH GRAPHS ***/	
	private function drawFlashPiePage() {
		echo "drawing pies";
	}
	
	private function drawFlashBarPage() {
		echo "drawing bars";
	}
	
	
/*** JAVA GRAPHS ***/
	private function drawJavaPiePage($echo) {
		$row_break = $this->page_layout=="LAND" ? 4 : 3;
		$line_break = 2;
		switch($this->page_layout) {
			case "LAND":
				$tdl = 1;
				$cradius = 90;
				$cwidth = 10;
				break;
			case "PORT":
			default:
				$tdl = 3;
				$cradius = 100;
				$cwidth = 30;
				break;
		}
		$cwidth+= $cradius * 2;
		$cheight = $cwidth;

			$echo.= "
			$(document).ready(function() {";
		if($this->has_main_graph===true) {
			$echo.= "
				main_chart_".$this->main_graph_id." = new AmCharts.AmPieChart();
				main_chart_".$this->main_graph_id.".color = \"#ffffff\";
				main_chart_".$this->main_graph_id.".dataProvider = chartData_".$this->main_graph_id.";
				main_chart_".$this->main_graph_id.".titleField = \"text\";
				main_chart_".$this->main_graph_id.".valueField = \"obj\";
				main_chart_".$this->main_graph_id.".colorField = \"color\";
				main_chart_".$this->main_graph_id.".labelText = \"[[percents]]%\";
				main_chart_".$this->main_graph_id.".labelRadius = -25;
				main_chart_".$this->main_graph_id.".angle = 10;
				main_chart_".$this->main_graph_id.".depth3D = 10;
				main_chart_".$this->main_graph_id.".hideLabelsPercent = 5;
				main_chart_".$this->main_graph_id.".hoverAlpha = 0.5;
				main_chart_".$this->main_graph_id.".startDuration = 0;
				main_chart_".$this->main_graph_id.".radius = ".$cradius.";
				main_chart_".$this->main_graph_id.".marginBottom = 30;
				main_chart_".$this->main_graph_id.".write(\"main_".$this->main_graph_id."\");";
		}
		if($this->has_sub_graph===true && count($this->sub_chart_data) > 0) {
			foreach($this->sub_chart_data as $sc => $scd) {
				$echo.= "
					sub_chart_".$sc." = new AmCharts.AmPieChart();
					sub_chart_".$sc.".color = \"#ffffff\";
					sub_chart_".$sc.".dataProvider = sub_chartData_".$sc.";
					sub_chart_".$sc.".titleField = \"text\";
					sub_chart_".$sc.".valueField = \"obj\";
					sub_chart_".$sc.".colorField = \"color\";
					sub_chart_".$sc.".labelText = \"[[percents]]%\";
					sub_chart_".$sc.".labelRadius = -25;
					sub_chart_".$sc.".angle = 10;
					sub_chart_".$sc.".depth3D = 10;
					sub_chart_".$sc.".hideLabelsPercent = 5;
					sub_chart_".$sc.".hoverAlpha = 0.5;
					sub_chart_".$sc.".startDuration = 0;
					sub_chart_".$sc.".radius = ".$cradius.";
					sub_chart_".$sc.".marginBottom = 30;
					sub_chart_".$sc.".write(\"sub_".$this->main_graph_id."_".$sc."\");";
			}
		}
		$echo.= "		});
				</script>";
		$echo.= "	<div align=center>
		<table class=noborder>
		<tr class=no-highlight>";
		if($this->has_main_graph===true) {
			$echo.="
			
			<!-- MAIN GRAPH -->
				<td class=\"center noborder\"><table class=noborder ".($this->graph_stack_type=="ABOVE" ? "width=100%" : "").">
				<tr class=no-highlight>
					<td class=\"center noborder\" colspan=".($this->graph_stack_type=="ABOVE" ? "2" : "1")."><span style=\"font-weight: bold;font-size:10pt;\">".$this->main_title."</span></td>
				</tr><tr class=no-highlight>
					<td ".($this->graph_stack_type=="ABOVE" ? "".($this->has_sub_graph===true ? "width=50%" : "")." class=\"right " : "class=\"center ")." noborder\">
						
						<div id=\"main_".$this->main_graph_id."\" style=\"width:".$cwidth."px; height:".$cheight."px; background-color:#ffffff;".($this->graph_stack_type=="ABOVE" ? "float:right" : "margin: 0 auto;")."\"></div>
					".($this->graph_stack_type=="ABOVE" ? "</td><td ".($this->has_sub_graph===true ? "width=50%" : "")." class=\"middle noborder\"><table>" : "<table style=\"margin: 0 auto;\">")."
					
						<tbody>";
					foreach($this->graph_categories as $k) {
						$ky = $k['code'];
						$echo.= "
							<tr>
								<td style=\"font-weight: bold;\" ><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
								".str_replace(" ","&nbsp;",$k['text'])."</td>
								<td class=center style=\"font-weight: normal;\">".(isset($this->count['main'][$k['code']]) ? $this->count['main'][$k['code']]." (".number_format(($this->count['main'][$k['code']]/array_sum($this->count['main'])*100),2)."%)" : "-")."</a></td>
							</tr>";
					}
					$echo.= "
					</tbody>
					</table></td></tr></table></td>";
		}
		if($this->graph_stack_type=="ABOVE" || $this->has_main_graph===false) {
			if($this->has_main_graph===true) { $echo.="</tr>"; }
			$lines = 1;
			$rows = 0;
		} else {
			$lines = 0;
			$rows = 1;
		}
		if($this->has_sub_graph===true && count($this->sub_chart_data)>0) {
			$echo.="
			<!-- SUB GRAPHS -->
				".($this->graph_stack_type=="ABOVE" && strlen($this->sub_title)>0 ? "<tr class=no-highlight >
					<td class=\"center noborder\"><span style=\"font-weight: bold;font-size: 10pt\">".$this->sub_title."</span>
				</tr><tr class=no-highlight>" : "")."<td class=\"center noborder\"><table class=noborder width=100%><tr class=no-highlight>";
			foreach($this->sub_chart_data as $sc => $scd) { 
				$s_id = $sc;
				if(array_sum($this->count['sub'][$s_id])*100>0){
					$rows++; 
					if($rows>$row_break) { 
						$lines++; 
						if($lines>=$line_break) { $lb = "page-break-before: always;"; $lines = 0; } else { $lb = ""; }
						$echo.="</tr></table></td></tr><tr class=no-highlight><td class=\"center noborder\" ".($this->graph_stack_type!="ABOVE" ? "colspan=2" : "")."><table style=\"".$lb."\" class=noborder width=100%><tr class=no-highlight>"; 
						$rows = 1; 
					}
					$echo.= "
						<td class=\"center noborder\">
							".$this->sub_list[$sc]."
							<div id=\"sub_".$this->main_graph_id."_".$sc."\" style=\"width:".$cwidth."px; height:".$cheight."px; background-color:#ffffff;margin: 0 auto;\"></div>
							<table style=\"margin: 0 auto;\">
							<tbody>";

						foreach($this->graph_categories as $k) {
							$ky = $k['code'];
							$echo.= "
								<tr>
									<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
									".$k['text']."</td>
									<td class=center style=\"font-weight: normal;\">".(isset($this->count['sub'][$s_id][$k['code']]) ? "".$this->count['sub'][$s_id][$k['code']]." (".number_format(($this->count['sub'][$s_id][$k['code']]/array_sum($this->count['sub'][$s_id])*100),2)."%)" : "-")."</a></td>
								</tr>";
						}
						$echo.= "
						</tbody>
						</table>
						</td>
					";
				}
			}
			$echo.="</tr></table></td></tr></table>";
		} //if subchartdata
		$echo.="
		</table></div>";

		echo $echo;
		
	}
	
	private function drawJavaBarPage($echo) {
        $echo.= "
		$(document).ready(function() {";
		if($this->has_main_graph===true) {
            $echo.="
			main_chart_".$this->main_graph_id." = new AmCharts.AmPieChart();
			main_chart_".$this->main_graph_id.".color = \"#000000\";
            main_chart_".$this->main_graph_id.".dataProvider = chartData_".$this->main_graph_id.";
            main_chart_".$this->main_graph_id.".titleField = \"text\";
			main_chart_".$this->main_graph_id.".valueField = \"obj\";
			main_chart_".$this->main_graph_id.".colorField = \"color\";
			main_chart_".$this->main_graph_id.".labelText = \"[[percents]]%\";
			main_chart_".$this->main_graph_id.".labelRadius = 20;
			main_chart_".$this->main_graph_id.".angle = 40;
			main_chart_".$this->main_graph_id.".depth3D = 15;
			main_chart_".$this->main_graph_id.".hideLabelsPercent = 5;
			main_chart_".$this->main_graph_id.".hoverAlpha = 0.5;
			main_chart_".$this->main_graph_id.".startDuration = 0;
			main_chart_".$this->main_graph_id.".radius = 150;
			main_chart_".$this->main_graph_id.".marginBottom = 10;
			main_chart_".$this->main_graph_id.".write(\"main_".$this->main_graph_id."\");";
		}
		if($this->has_sub_graph===true && count($this->count['sub'])>0) {
			$echo.="
				var valAxis = new AmCharts.ValueAxis();
				valAxis.stackType = \"".($this->bar_stack_type=="REG" ? "regular" : "100%")."\";
				valAxis.gridAlpha = ".($this->display_bar_grid===true ? "0.2" : "0").";
				valAxis.axisAlpha = 0;
				valAxis.maximum = ".$this->getValueAxisMaximum().";
				valAxis.color = \"#".($this->display_bar_grid===true ? "000000" : "ffffff")."\";";

			foreach($this->sub_chart_data as $sc => $scd) {
				$echo.= "    
					sub_chart_".$sc." = new AmCharts.AmSerialChart();
					sub_chart_".$sc.".color = \"#ffffff\";
					sub_chart_".$sc.".dataProvider = sub_chartData[".$sc."];
					sub_chart_".$sc.".categoryField = \"text\";
					sub_chart_".$sc.".marginLeft = 47;
					sub_chart_".$sc.".marginTop = 10;
					sub_chart_".$sc.".marginBottom = 50;
					sub_chart_".$sc.".fontSize = 10;
					sub_chart_".$sc.".angle = 45;
					sub_chart_".$sc.".depth3D = 15;
					sub_chart_".$sc.".plotAreaBorderAlpha = ".($this->display_bar_grid===true ? "0.3" : "0").";
					sub_chart_".$sc.".rotate = false;

					sub_chart_".$sc.".addValueAxis(valAxis);

					sub_chart_".$sc.".categoryAxis.gridAlpha = ".($this->display_bar_grid===true ? "0.2" : "0").";
					sub_chart_".$sc.".categoryAxis.axisAlpha = 0;
					sub_chart_".$sc.".categoryAxis.color = \"#000000\";
					sub_chart_".$sc.".categoryAxis.fontSize = 7;
					sub_chart_".$sc.".categoryAxis.gridPosition = \"start\";
					";
				foreach($this->graph_categories as $ky => $k) { 
					//$ky = $k['code'];
					$k['id'] = $ky;
					$echo.= "
					var graph_".$sc."_".$k['id']." = new AmCharts.AmGraph();
					graph_".$sc."_".$k['id'].".title = \"".$k['text']."\";
					graph_".$sc."_".$k['id'].".labelText=\"".($this->bar_stack_type=="REG" ? "[[value]]" : "[[percents]]%")."\";
					graph_".$sc."_".$k['id'].".valueField = \"R".$k['id']."\";
					graph_".$sc."_".$k['id'].".type = \"column\";
					graph_".$sc."_".$k['id'].".lineAlpha = 0;
					graph_".$sc."_".$k['id'].".fillAlphas = 1;
					graph_".$sc."_".$k['id'].".lineColor = \"".$k['color']."\";
					graph_".$sc."_".$k['id'].".balloonText = \"[[value]]\";
					sub_chart_".$sc.".addGraph(graph_".$sc."_".$k['id'].");";
				}
				$echo.= "sub_chart_".$sc.".write(\"sub_".$this->main_graph_id."_".$sc."\");";
			}	//end foreach sub chart 
		}
		$echo.= "		});
				</script>
				
		<div align=center style='margin: 0 auto;'>
			<table class=noborder style='margin: 0 auto;'>
				<tr class=no-highlight>";
		if($this->has_main_graph===true) {
			$echo.= "<!-- MAIN GRAPH -->
				".(strlen($this->main_title)>0 ? "
					<td colspan=2 class=\"center noborder\"><span style=\"font-weight: bold;font-size:10pt;\">".$this->main_title."</span></td>
				</tr><tr class=no-highlight>" : "")."
					<td width=50% class=\"right noborder\">
						<div id=\"main_".$this->main_graph_id."\" style=\"width:460px; height:280px; background-color:#ffffff;float:right\"></div>
					</td>
					<td width=50% class=\"middle noborder\"><table>
						<tbody>";

					foreach($this->graph_categories as $k) {
						$ky = $k['code'];
						$echo.= "
							<tr>
								<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
								".$k['text']."</td>
								<td class=center style=\"font-weight: bold;\">".(isset($this->count['main'][$k['code']]) ? $this->count['main'][$k['code']]." (".number_format(($this->count['main'][$k['code']]/array_sum($this->count['main'])*100),2)."%)" : "-")."</a></td>
							</tr>";
					}
					$echo.= "
					</tbody>
					<tfoot>
						<tr>
							<td class=right rowspan=2 style=\"background-color: #dddddd;\"><b>Total:</b></td>
							<td class=\"center\" style=\"background-color: #555555; color: #ffffff; font-weight: bold;\">".array_sum($this->count['main'])." (100%)</td>
						</tr>
					</tfoot>
					</table></td>
				</tr>";
		}//is has main
		
		if($this->has_sub_graph===true && count($this->sub_chart_data)>0) {
			$echo.="
			<!-- SUB GRAPHS -->
				<tr class=no-highlight ".($this->page_layout=="LAND" ? "style=\"page-break-before: always;\"" : "").">
					<td class=\"center noborder\" colspan=2><span style=\"font-weight: bold;font-size: 10pt\">".$this->sub_title."</span>
				</tr>";
			foreach($this->sub_chart_data as $sc => $scd) { 
					$graph_width = count($scd)*90 + 50; 
					//$list_ids = $sub_ids[$sc];
					//$list_ids = array_keys($this->sub_list);
			$echo.="
				<tr class=no-highlight>
					<td colspan=2 class=\"center noborder\">
						<div id=\"sub_".$this->main_graph_id."_".$sc."\" style=\"width:".$graph_width."px; height:270px; background-color:#ffffff; margin: 0 auto;\"></div>
					</td>
				</tr><tr class=no-highlight>
					<td colspan=2 class=noborder>

					
					
						<div align=center><table>
								<thead>
									<tr class=no-highlight>
										<td style=\"border-top-color: #ffffff; border-left-color: #ffffff; border-right-color: #ffffff;\"></td>";
										foreach($this->sub_list as $s_id => $s) {
											if(isset($this->count['sub'][$s_id]) && array_sum($this->count['sub'][$s_id])>0 && in_array($s_id, $this->sub_ids[$sc])) {
//											if(in_array($s_id, $this->sub_ids)) {
												$echo.= "<th width=100 style=\"font-size: 7pt;\">".$s."</th>";
											}
										}
			$echo.="
									</tr>
								</thead>
								<tbody>";
									foreach($this->graph_categories as $k) {
										$ky = $k['code'];
										$echo.= "
											<tr>
												<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
												".$k['text']."</td>";
										foreach($this->sub_list as $s_id => $s) {
											if(isset($this->count['sub'][$s_id]) && array_sum($this->count['sub'][$s_id])>0 && in_array($s_id, $this->sub_ids[$sc])) {
											//if(in_array($s_id, $list_ids)) { 
												$echo.= "
												<td class=center>".(isset($this->count['sub'][$s_id][$k['code']]) ? $this->count['sub'][$s_id][$k['code']]." (".number_format(($this->count['sub'][$s_id][$k['code']]/array_sum($this->count['sub'][$s_id])*100),2)."%)" : "-")."</a></td>"; 
											}
										}
										echo "</tr>";
									}
			$echo.="
								</tbody>
								<tfoot>
									<tr>
										<td class=right rowspan=2 style=\"background-color: #dddddd;\"><b>Total:</b></td>";
										foreach($this->sub_list as $s_id => $s) {
											if(isset($this->count['sub'][$s_id]) && array_sum($this->count['sub'][$s_id])>0 && in_array($s_id, $this->sub_ids[$sc])) {
											//if(in_array($s_id, $list_ids)) { 
												$echo.= "
												<td class=center style=\"background-color: #dddddd;\"><b>".(isset($this->count['sub'][$s_id]) ? array_sum($this->count['sub'][$s_id]) : "-")."
												 (".(isset($this->count['sub'][$s_id]) ? number_format((array_sum($this->count['sub'][$s_id])/array_sum($this->count['main'])*100),2)."%" : "-").")</b></td>";
											}
										}
			$echo.="
									</tr>
								</tfoot>
						</table></div>
					
					
					
					</td>
				</tr>";
			}	//foreach sub_chartdata
		} //if subchartdata
		$echo.="
		</table></div>";

		echo $echo;

	}
	private function getValueAxisMaximum() {
		$valueAxis_maximum = 0;
		foreach($this->count['sub'] as $s) {
			$max_kpis = 0;
			foreach($s as $k) {
				$max_kpis+=$k;
			}
			$valueAxis_maximum = $max_kpis > $valueAxis_maximum ? $max_kpis : $valueAxis_maximum;	
		}
		return $valueAxis_maximum;
	}
	
	
	private function drawHeader() {
		echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">
				<html>
				<head>
				<meta http-equiv=\"Content-Language\" content=\"en-za\">
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">
				<title>www.Ignite4u.co.za</title>
					<script src=\"/library/amcharts/javascript/amcharts.js\" type=\"text/javascript\"></script>
					<script src=\"/library/amcharts/javascript/raphael.js\" type=\"text/javascript\"></script>        
						<script type =\"text/javascript\" src=\"/library/jquery/js/jquery.min.js\"></script>
						<script type =\"text/javascript\" src=\"/library/jquery/js/jquery-ui.min.js\"></script>
						<script type =\"text/javascript\" src=\"/library/jquery/js/jquery-ui-timepicker-addon.js\"></script>
						<link href=\"/library/jquery/css/jquery-ui.css\" rel=\"stylesheet\" type=\"text/css\" />
					<link rel=\"stylesheet\" href=\"/assist.css\" type=\"text/css\">
				</head>
				<base target=\"main\">
				<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";
	}
	private function drawCSS() {
		echo "
		<style type=text/css>
			table td { font-size: 7pt; }
			//table.noborder, table td.noborder { border: dashed 1px #009900; }	//development testing purposes only
		</style>";
	}
	
	
	public function __destruct() {
		unset($this);
	}

}


?>