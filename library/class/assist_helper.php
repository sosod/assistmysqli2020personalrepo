<?php
/** ASSIST HELPER CLASS
Created on: 2 October 2012
Created by: Janet Currie
Updated on: 3 February 2013 [JC]
Updated on: 6 February 2013 [JC]


General functions to help Ignite Assist







**/
class ASSIST_HELPER {



	protected $hex = array(
				'0' => '30','1' => '31','2' => '32','3' => '33','4' => '34','5' => '35','6' => '36','7' => '37','8' => '38','9' => '39',
				'A' => '41','B' => '42','C' => '43','D' => '44','E' => '45','F' => '46','G' => '47','H' => '48','I' => '49','J' => '4a',
				'K' => '4b','L' => '4c','M' => '4d','N' => '4e','O' => '4f','P' => '50','Q' => '51','R' => '52','S' => '53','T' => '54',
				'U' => '55','V' => '56','W' => '57','X' => '58','Y' => '59','Z' => '5a',
				'a' => '61','b' => '62','c' => '63','d' => '64','e' => '65','f' => '66','g' => '67','h' => '68','i' => '69','j' => '6a',
				'k' => '6b','l' => '6c','m' => '6d','n' => '6e','o' => '6f','p' => '70','q' => '71','r' => '72','s' => '73','t' => '74',
				'u' => '75','v' => '76','w' => '77','x' => '78','y' => '79','z' => '7a',
		'`'=>"60",
		'~'=>"7e",
		'!'=>"21",
		'@'=>"40",
		'#'=>"23",
		'$'=>"24",
		'%'=>"25",
		'^'=>"5e",
		'&'=>"26",
		'*'=>"2a",
		'('=>"28",
		')'=>"29",
		'_'=>"5f",
		'+'=>"2b",
		'-'=>"2d",
		'='=>"3d",
		'['=>"5b",
		']'=>"5d",
		';'=>"3b",
		'\''=>"27",
		'\\'=>"5c",
		','=>"2c",
		'.'=>"2e",
		'/'=>"2f",
		'{'=>"7b",
		'}'=>"7d",
		':'=>"3a",
		'"'=>"22",
		'|'=>"7c",
		'<'=>"3c",
		'>'=>"3e",
		'?'=>"3f",
			);
	const UNSPECIFIED = "[Unspecified]";
	protected $unspecified = "[Unspecified]";
	public $today;	//a variable to store the current date & time in UNIX timestamp => for data/time comparisons
	protected $nav_buttons_level = 1;

	protected $html_codes = array("b","i","u","li","ul","ol","p");

	const JOIN_FOR_MULTI_LISTS = ";";
	const JOIN_FOR_SQL_FOR_SAVE = ", ";

	function __construct() {
		$this->today = strtotime(date("Y-m-d H:i:s"));
	}

	public function getUnspecified() { return $this->unspecified; }

/* Convert a string to html codes for easy storage in db */
	static function code($str,$encoding="UTF-8") {
		$str = str_replace(chr(151), '-', $str); // emdash
		$str = str_replace(chr(150), '-', $str); // endash
		if($encoding=="UTF-8") {
			$str = utf8_encode($str);
		}
		$str = htmlentities($str,ENT_QUOTES,$encoding);
		//$str = htmlentities($str,ENT_QUOTES,"UTF-8");
		//$str = htmlentities($str,ENT_QUOTES,"ISO-8859-1");
        //$str = htmlentities($str, ENT_COMPAT, 'iso-8859-1');
        //$str = preg_replace('/&(.)(acute|cedil|circ|lig|grave|ring|tilde|uml);/', "$1", $str);
        //$str = str_replace("\x8226","&bull;",$str);//preg_replace('/\x8226/', '&bull;', $str);
        return $str;
	}

/* Convert a html encoded string back to normal text before displaying in form field */
	static function decode($str) {
		return html_entity_decode($str, ENT_QUOTES, "UTF-8");
		//return html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
	}

/* Convert a string with human [] html tags into PC <> html tags */
	function convertToHTML($str) {
		$str = $this->decode($str);
		foreach($this->html_codes as $c) {
			$str = str_replace("[".$c."]","<".$c.">",$str);
			$str = str_replace("[/".$c."]","</".$c.">",$str);
		}
		return $str;
	}

/* Convert a string with PC <> html tags into human [] html tags */
	function convertFromHTML($str) {
		$str = $this->decode($str);
		foreach($this->html_codes as $c) {
			$str = str_replace("<".$c.">","[".$c."]",$str);
			$str = str_replace("</".$c.">","[/".$c."]",$str);
		}
		return $str;
	}
/* decodes string, removes all spaces and converts to lowercase for easy comparison with another string */
	function stripStringForComparison($s) {
		return str_replace(" ","",strtolower(ASSIST_HELPER::decode(stripslashes($s))));
	}







/**
* get SERVER NAME 
**/
	public function getServerName() {
		if(strpos($_SERVER["HTTP_HOST"],"action4udev.co.za")!==false) {
			return "DEV01";
		} else {
			switch($_SERVER["HTTP_HOST"]) {
				case "pwc.assist.action4u.co.za":
					return "ASSIST02";
					break;
				case "integration.assist.action4u.co.za":
					return "FINTEG01";
					break;
				case "assist.action4u.co.za":
				case "ignite.assist.action4u.co.za":
				default:
					return "ASSIST01";
					break;
			}
		}
	}





/* echo an array of all variables onscreen => development purposes
 *     NOTE: send get_defined_vars() as $var in function call otherwise it only gets the variables set in the function and not on the calling page */
	static function varPrint($var,$request=false,$files=false,$session=false,$server=false,$cookie=false) {
		if(!$request) {
			unset($var['_REQUEST']);
			unset($var['_GET']);
			unset($var['_POST']);
		}
		if(!$files) {
			unset($var['_FILES']);
		}
		if(!$session) {
			unset($var['_SESSION']);
		}
		if(!$cookie) {
			unset($var['_COOKIE']);
		}
		if(!$server) {
			unset($var['_SERVER']);
		}
		ASSIST_HELPER::arrPrint($var);
	}
/* echo an array onscreen => development purposes */
	static function arrPrint($a) {
		if(!is_array($a)) {
			echo "<h3 class=red>NOT ARRAY: ".$a." :</h3>";
		} else {
			echo "<pre style='font-size:14pt'>"; print_r($a); echo "</pre>";
		}
	}

/* Check that a value is a valid PK => numeric & greater than 0 - does allow decimals : 6.3 = true */
	static function checkIntRef($r) {
		if(is_array($r) || is_null($r) || strlen($r)==0 || !is_numeric($r) || $r <=0) {
			return false;
		}
		return true;
	}

	/* Check that a value is a valid PK => numeric & greater than 0 & integer only - 6.3 = false */
	static function checkIntRefForWholeIntegersOnly($r) {
		if(is_array($r) || is_null($r) || strlen($r)==0 || !is_numeric($r) || $r <=0 || (int)$r!=($r*1)) {
			return false;
		}
		return true;
	}

/* Remove unwanted blank elements from array */
	static function removeBlanksFromArray($a,$numerical=true,$maintain_key_association=false) {
		$a2 = array();
		if(is_array($a) && count($a)>0) {
		foreach($a as $i => $b) {
			$check = false;
			if(is_array($b)) {
				$b = ASSIST_HELPER::removeBlanksFromArray($b,$numerical,$maintain_key_association);
				if(count($b)>0) {
					$check = true;
				}
			} else if($numerical && ASSIST_HELPER::checkIntRef($b)) {
				$check = true;
			} else if(!$numerical && !(is_null($b) || strlen($b)==0)) {
				$check = true;
			}
			if($check) {
				if($maintain_key_association) {
					$a2[$i] = $b;
				} else {
					$a2[] = $b;
					}
				}
			}
		}
		return $a2;
	}

/* check that a folder exists in the client files/cmpcode folder & create if necessary */
	function checkFolder($path,$depth=1,$return=false,$cmpcode=false) {
		if($cmpcode===false) {
		$cmpcode = $this->getCmpCode();
		}
		if(strlen($cmpcode)>0) {
			$me = $_SERVER['PHP_SELF'];
			$myloc = explode("/",$me);
			if(strlen($myloc[0])==0) { unset($myloc[0]); }
			$mc = count($myloc) - 1;
			$path = "files/".$cmpcode."/".$path;
			$chk = explode("/",$path);
			$chkloc = "";
			for($m=1;$m<count($myloc);$m++) { $chkloc.="../"; }
			for($m=1;$m<$depth;$m++) { $chkloc.="../"; }
			$final = $chkloc.$path;
			foreach($chk as $c)
			{
				if(strlen($c)>0 && $c != "." && $c != "..") {
					$chkloc.= "/".$c;
					if(!is_dir($chkloc)) { mkdir($chkloc); }
				}
			}
			if($return) {
				return $final;
			}
		} else {
			die("An error has occurred.  Please go back and try again.");
		}
	}

	function saveEcho($folder, $filename, $echo, $depth=1) {
		$cmpcode = $this->getCmpCode();
			//CHECK EXISTANCE OF STORAGE LOCATION
			$this->checkFolder($folder);
			//WRITE DATA TO FILE
			$file_name = "";
			for($i=1;$i<=$depth;$i++) {
				$file_name.="../";
			}
			$file_name.= "files/".$cmpcode."/".$folder."/".$filename;
			$file = fopen($file_name,"w");

			fwrite($file,$echo."\n");
			fclose($file);
	}
	function downloadFile2($folder, $old, $new, $content) {
		$cmpcode = $this->getCmpCode();
        $file_name = "../files/".$cmpcode."/".$folder."/".$old;

		header("Content-type: $content");
		header("Content-disposition: attachment; filename=$new");
		readfile($file_name);
	}
	function downloadFile3($old, $new="", $content="application/octet-stream") {
		ob_start();
		header("Content-type: $content");
		header("Expires: 0");
header("Cache-Control: ");// leave blank to avoid IE errors
header("Pragma: ");// leave blank to avoid IE errors
		if(strlen($new)>0) {
			header('Content-disposition: attachment; filename="'.$new.'"');
		} else {
			//header("Content-disposition: attachment; ");
		}
		header("Content-Transfer-Encoding: binary ");
		while (ob_get_level()) {
			ob_end_clean();
		}
		flush();
		readfile($old);
	}
	function getFilesLocation($path,$cc) {
		return "../files/".strtolower($cc)."/".$path."/";
	}
	function downloadFile4($old, $new, $content="application/octet-stream",$isFile=true) {
		$new = (strlen($new)==0 ? $old : $new);
		$content = (strlen($content)==0 ? "application/octet-stream" : $content);

		header("Content-type: $content");
		header("Content-disposition: attachment;".(strlen($new)>0 ? " filename=$new" : ""));
		if($isFile) {
			readfile($old);
		} else {
			echo $old;
		}
	}
	function generateCSVDownloadFile($data,$name) {
		//ORIGINALLY FROM: http://code.stephenmorley.org/php/creating-downloadable-csv-files/
		// output headers so that the file is downloaded rather than displayed
		header("Content-Type: text/csv; charset=utf-8");
		header("Content-Disposition: attachment; filename=$name ");

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		/* // output the column headings
		fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

		// fetch the data
		mysql_connect('localhost', 'username', 'password');
		mysql_select_db('database');
		$rows = mysql_query('SELECT field1,field2,field3 FROM table');

		// loop over the rows, outputting them
		while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);*/

		foreach($data as $d) {
			fputcsv($output,$d);
		}

	}







/** DATA REPORT CODE **/

	function getFolderSize($folder,$ignore=array()) {
		$fnum = 0;
		$size = array(0=>$folder,1=>0,2=>0);
		$filename = glob($folder."/*");  //arrPrint($filename);
		$ignore[] = "deleted";
		$ignore[] = "reports";
		$files_to_ignore = array("csv","html","xml","Thumbs.db");
		foreach($filename as $file) {
			//echo "<P>".$file;
	//		$f = strFn("explode",$file,"/","");
	//		$f = $f[count($f)-1];
			$fname = explode("/",$file);
			$fname = array_pop($fname);
			if(is_dir($file)) {
				//echo "<br />is dir";
				$fcheck = true;
				foreach($ignore as $ig) {
					if(strtolower($fname)==strtolower($ig)) { //stripos($file,$ig)) {
						$fcheck = false;
						break;
					}
				}
				//echo "<br />fcheck: ".($fcheck?"true":"false");
				if($fcheck) {
					$size[$file] = $this->getFolderSize($file);
				}
			} else {
				//echo "<br />is file";
				$fcheck = true;
				foreach($files_to_ignore as $fig) {
					if(stripos($file,".".$fig) || strtolower($fname)==strtolower($fig)) {
						$fcheck = false;
						break;
					}
				}
				//echo "<br />fcheck: ".($fcheck?"true":"false");
				if($fcheck) {
					$size[1]+=filesize($file);
					$fnum++;
				}
				//echo "<br />fnum: ".$fnum;
			}
			$size[2] = $fnum;
		}
		return $size;
	}
	function displayFolderSize($s,$folder,$moduletitle,$display=true) {
		/*echo "<P>";
		print_r($s);
		echo "-<b>".$folder."</b></p>";*/
		$fn = 0;
		$ts = 0;
		$fs = strlen($folder);
		$f = $s[0];
		//$f = strFn("substr",$f,$fs,strlen($f));
		$f = substr($f,$fs,strlen($f));
		//$fx = explode("/",$f);
		//$f = array_pop($fx);
		$f = str_replace("/"," > ",$f);
		$z = $s[1];
		$ts+=$z;
		$n = $s[2];
		$fn+=$n;
		unset($s[0]);
		unset($s[1]);
		unset($s[2]);
	if($display) {
		echo "<tr>";
			echo "<td style=\"font-weight:bold;\">".$moduletitle.(strlen($f)>0 ? " > ":"").$f."</td>";
			echo "<td style=\"text-align:center;\">".$n."</td>";
			echo "<td style=\"text-align:right;\">".$this->formatBytes($z,2)."</td>";
		echo "</tr>";
	}
		foreach($s as $t) {
			if(is_array($t)) {
				$r=$this->displayFolderSize($t,$folder,$moduletitle,$display);
				$ts+=$r[0];
				$fn+=$r[1];
			}
		}
		$ret = array($ts,$fn);
		return $ret;
	}


/* function to convert php style file size '5M' etc to actual bytes */
static function returnBytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= (1024 * 1024 * 1024); //1073741824
            break;
        case 'm':
            $val *= (1024 * 1024); //1048576
            break;
        case 'k':
            $val *= 1024;
            break;
    }

    return $val;
}

function getMaxUploadFileSize() {
	return self::returnBytes(ini_get('post_max_size'));
}









/* PHP code to display the result of an action in jquery widget */
	static function displayResult($result=array()) {
		if(count($result)>0) {
			echo("<div class=\"ui-widget\">");
			if($result[0]=="ok" || $result[0]=="check") {
				echo "<div class=\"ui-state-ok ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
				echo "<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="info") {
				echo "<div class=\"ui-state-info ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em; \">";
				echo "<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="warn") {
				echo "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
				echo "<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>";
			} else {
				echo "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">";
				echo "<p><span class=\"ui-icon ui-icon-closethick\" style=\"float: left; margin-right: .3em;\"></span>";
			}
			echo "<span id=spn_displayResultText style=''>".stripslashes($result[1])."</span></p></div></div>";
		}
	}
	static function getDisplayResult($result=array()) {
		$data = "";
		if(count($result)>0) {
			$data.="<div class=\"ui-widget\">";
			if($result[0]=="ok" || $result[0]=="check") {
				$data.= "<div class=\"ui-state-ok ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="info") {
				$data.= "<div class=\"ui-state-info ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>";
			} elseif($result[0]=="warn") {
				$data.= "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>";
			} else {
				$data.= "<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
						<p><span class=\"ui-icon ui-icon-closethick\" style=\"float: left; margin-right: .3em;\"></span>";
			}
			$data.= "<span id=spn_displayResultText>".stripslashes($result[1])."</span></p></div></div>";
		}
		return $data;
	}

	static function getFloatingDisplay($result=array(),$position="TR",$size=300,$vertical=10,$horizontal=10) {
		switch($position) {
		case "BR":
			$pos = "bottom: ".$vertical."px; right: ".$horizontal."px;";
			break;
		case "BL":
			$pos = "bottom: ".$vertical."px; left: ".$horizontal."px;";
			break;
		case "TL":
			$pos = "top: ".$vertical."px; left: ".$horizontal."px;";
			break;
		case "TR":
		default:
			$pos = "top: ".$vertical."px; right: ".$horizontal."px;";
			break;
		}
		return "<div class=float style='width: ".$size."px; position: fixed; ".$pos."'>".ASSIST_HELPER::getDisplayResult($result)."</div>";
	}

	function getDisplayIcon($icon,$options="",$color="") {
		switch($icon) {
		case "ok":
			return "				<span class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"009900")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		case "error":
			return "				<span class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"cc0001")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		case "info":
			return "				<span class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"fe9900")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		case "warn":
			return "				<span class=\"ui-icon ui-icon-alert\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"fe9900")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		case "person":
			return "				<span class=\"ui-icon ui-icon-person\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"000099")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		case "folder-open": //ui-icon-folder-open
			return "				<span class=\"ui-icon ui-icon-folder-open\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"222222")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		default:
			return "				<span class=\"ui-icon ".$icon."\" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"222222")."_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		}
	}

	static function displayIcon($icon,$options="") {
		switch($icon) {
		case "ok":
			echo "				<span class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); ".$options."\">&nbsp;</span>			";
			break;
		case "error":
			echo "				<span class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); ".$options."\">&nbsp;</span>			";
			break;
		case "info":
			echo "				<span class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png);\" ".$options.">&nbsp;</span>			";
			break;
		}
	}

	static function displayIconAsDiv($icon,$options="",$color="",$class="") {
		echo self::getDisplayIconAsDiv($icon,$options,$color,$class);
	}
	static function getDisplayIconAsDiv($icon,$options="",$color="",$class="") {
		if($icon===true || $icon=="true") { $icon="1"; } elseif($icon===false || $icon=="false") {$icon="0";}
		//icon check needed before colour check to catch original 3 icons with no colour given
		if($icon=="info" && strlen($color)==0) { $color = "orange"; }
		if($icon=="ok" && strlen($color)==0) { $color = "green"; }
		if($icon=="error" && strlen($color)==0) { $color = "red"; }
		//now check for colours
		$color = ( strlen($color)==0 ? "default" : $color );
		switch($color) {
			case "red":	$color="cc0001"; break;
			case "green":	$color="009900"; break;
			case "orange":	$color="fe9900"; break;
			case "blue":	$color="000099"; break;
			case "white":	$color="ffffff"; break;
			case "light-grey":	$color="888888"; break;
			case "default":
			case "grey":
			case "gray":
					$color="222222"; break;
			case "black":
					$color="000000"; break;
		}
		//create icon
		switch($icon) {
		case "ok":
		case "1":
			return "				<div class=\"ui-icon ui-icon-check $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>			";
			break;
		case "error":
		case "0":
			return "				<div class=\"ui-icon ui-icon-closethick $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>			";
			break;
		case "warn":
			return "				<div class=\"ui-icon ui-icon-alert $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>			";
			break;
		case "info":
			return "				<div class=\"ui-icon ui-icon-info $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"fe9900")."_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>			";
			break;
		case "flag":
			return "				<div class=\"ui-icon ui-icon-flag $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"cc0001")."_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>			";
			break;
		case "no-entry":
			return "				<div class=\"ui-icon ui-icon-circle-minus $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"cc0001")."_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>			";
			break;
		case "not-done":
			return "				<div class=\"ui-icon ui-icon-circle-close $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"cc0001")."_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>			";
			break;
		case "done":
			return "				<div class=\"ui-icon ui-icon-circle-check $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"cc0001")."_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>			";
			break;
		default:
			return "				<div class=\"ui-icon ".$icon." $class \" style=\"background-image: url(/library/jquery/css/images/ui-icons_".(strlen($color)>0?$color:"222222")."_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>			";
			break;
		}
	}

	static function JSdisplayResultPrep($txt="") {
		echo ASSIST_HELPER::getJSdisplayResultPrep($txt);
	}
	static function getJSdisplayResultPrep($txt) {
		return "
			<div class=\"ui-widget\">
				<div id=display_result class=\"ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">
					<p>
						<span id=display_result_icon class=\"\" style=\"float: left; margin-right: .3em;\"></span>
						<span id=display_result_text>".$txt."</span>
					</p>
				</div>
			</div>
			<script type=text/javascript>
			function JSdisplayResult(result,icon,txt) {
				if(result==\"reset\") {
					$(\"#display_result_text\").text('');
					$(\"#display_result_icon\").removeClass();
					$(\"#display_result\").removeClass();
							$(\"#display_result_icon\").removeClass(\"ui-icon-check\");
							$(\"#display_result_icon\").removeClass(\"ui-icon-info\");
							$(\"#display_result_icon\").removeClass(\"ui-icon-closethick\");
							$(\"#display_result\").removeClass(\"ui-state-error\");
							$(\"#display_result\").removeClass(\"ui-state-info\");
							$(\"#display_result\").removeClass(\"ui-state-ok\");
					$(\"div.ui-widget\").hide();
				} else if(result.length>0) {
					$(\"div.ui-widget\").show();
					$(\"#display_result_text\").text('');
					$(\"#display_result_icon\").removeClass().addClass(\"ui-icon\");
					$(\"#display_result\").removeClass();
					switch(icon) {
						case \"ok\":		$(\"#display_result_icon\").addClass(\"ui-icon-check\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-ok\"); break;
						case \"info\":	$(\"#display_result_icon\").addClass(\"ui-icon-info\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-info\"); break;
						case \"error\":	$(\"#display_result_icon\").addClass(\"ui-icon-closethick\"); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-error\"); break;
						case \"reset\":
							$(\"#display_result_icon\").removeClass(\"ui-icon-check\");
							$(\"#display_result_icon\").removeClass(\"ui-icon-info\");
							$(\"#display_result_icon\").removeClass(\"ui-icon-closethick\");
							$(\"#display_result\").removeClass(\"ui-state-error\");
							$(\"#display_result\").removeClass(\"ui-state-info\");
							$(\"#display_result\").removeClass(\"ui-state-ok\");
							break;
						default:		$(\"#display_result_icon\").addClass(\"ui-icon-\"+icon); 	$(\"#display_result\").addClass(\"ui-corner-all ui-state-\"+result); break;
					}
					$(\"#display_result_text\").text(txt);
				} else {
					$(\"#display_result_text\").text('');
					$(\"#display_result_icon\").removeClass();
					$(\"#display_result\").removeClass();
					$(\"div.ui-widget\").hide();
				}
			}
			</script>";
	}
	static function JSdisplayResult($result="",$icon="",$text="") {
		if(strlen($icon)==0) { $icon = $result; }
		$text = stripslashes($text);
		echo "<script type=text/javascript>JSdisplayResult('$result','$icon','$text');</script>";
	}


/* shortcut to generating options for a select from an array */
	function getOptions($arr,$sel="") {
		$echo = "";
		foreach($arr as $v => $a) {
			$echo.="<option value=$v ".($v==$sel ? "selected" : "").">$a</option>";
		}
		return $echo;
	}

/* PHP code to format & display an activity log */
	function displayAuditLog($logs,$flds,$extra="") {
			/* $flds = array('date'=>,'user'=>,'action'=>); */
		if(count($logs)>0) {
			echo "
			<table  width=707 class=noborder style='margin-top: 20px'>
				<tr>
					<td class=noborder width=35%>$extra</td>
					<td class='center noborder' width=30%><h3 class=log id=h3_log_title>Activity log</h3></td>
					<td class=noborder width=35%><span id=disp_audit_log style=\"cursor: hand;\" class=\"float color\"><img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span></span></td>
				</tr>
				<tr>
					<td colspan=3 class=noborder>
						<div id=my_audit_log>
								<table width=700 id=tbl_log>
									<tr>
										<th class=log>Date</th>
										<th class=log>User</th>
										<th class=log>Action</th>
									</tr>";
						foreach($logs as $l) {
							echo "<tr>";
								$a=0;
								foreach($flds as $f) {
									$a++;
									if($a==1) {
										if(!is_numeric($l[$f])) { $d = strtotime($l[$f]); } else { $d = $l[$f]; }
										echo "<td class=\"log centre\">".date("d M Y H:i",$d)."</td>";
									} else {
										echo "<td class=log>".stripslashes(decode($l[$f]))."</td>";
									}
								}
							echo "</tr>";
						}
					echo "</table>";
			echo "</div></td></tr></table>";
			echo "<script type=text/javascript>
					$(function() {
						$('#my_audit_log, #h3_log_title').hide();
						$(\"#disp_audit_log\").click(function() {
							if($('#my_audit_log').is(':visible')) {
								$(\"#my_audit_log, #h3_log_title\").hide();
								$(\"#log_txt\").attr(\"innerText\",\"Display Activity Log\");
								$(\"#log_pic\").attr(\"src\",\"/pics/tri_down.gif\");
							} else {
								$(\"#my_audit_log, #h3_log_title\").show();
								$(\"#log_txt\").attr(\"innerText\",\"Hide Activity Log\");
								$(\"#log_pic\").attr(\"src\",\"/pics/tri_up.gif\");
							}
						});
						$(\"#tbl_log a\").click(function() {
							var u = $(this).attr(\"id\");
							if(u.length>0)
								window.open(u, \"_blank\",\"status=no,toolbar=no,location=no,scrollbars=no,directories=no\");
						});
					});
				</script>";
		} elseif(strlen($extra)>0) {
			echo "
			<table  width=707 class=noborder style='margin-top: 20px'>
				<tr>
					<td class=noborder width=35%>$extra</td>
					<td class='center noborder' width=30%>&nbsp;</td>
					<td class=noborder width=35%>&nbsp;</td>
				</tr>
			</table>";
		}
	}

/* echo the navigation buttons onscreen */
	function drawNavButtons($menu) {
		/*** variables:
			$menu[id] = array('id'=>id,'url'=>path_to,'active'=>(bool)checked_or_not,'display'=>heading);
		***/
		echo $this->getNavButtons($menu);
	}
	function setNavButtonsLevel($l) {
		$this->nav_buttons_level = $l;
	}
/* generate navigation buttons */
	function getNavButtons($menu) {
		$echo = "";
		$id = "m".$this->nav_buttons_level;
		switch($this->nav_buttons_level) {
		case 1:
			$echo.= "<div id=".$id." class=nav-buttons style=\"font-size: 8pt; font-weight: bold;\">";
			break;
		case 2:
			$echo.= "<div id=".$id." class=nav-buttons style=\"padding-top: 5px; font-size: 7pt\">";
	/*		foreach($menu as $m) {
				$key = $m['id'];
				$echo.= "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" ".($m['active']==true || $m['active']=="Y" ? "checked=checked" : "")." ".( (isset($m['help']) && strlen($m['help'])>0) ? " help_text=\"".$m['help']."\" " : "")." />";
				$echo.= "<label for=\"$key\" style=\"background: #fafaff url();margin-right: 3px;\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
			}
			$echo.= "</div>";*/
			break;
		case 3:
		default:
			$echo.= "<div id=".$id." class=nav-buttons style=\"padding-top: 5px; font-size: 6.5pt; margin-left:10px\">";
		/*	foreach($menu as $m) {
				$key = $m['id'];
				$echo.= "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" ".($m['active']==true || $m['active']=="Y" ? "checked=checked" : "")." ".( (isset($m['help']) && strlen($m['help'])>0) ? " help_text=\"".$m['help']."\" " : "")." />";
				$echo.= "<label for=\"$key\" style=\"background: #fafaff url();margin-right: 3px;\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
			}
			$echo.= "</div>";*/
			break;
		}
			foreach($menu as $m) {
				$key = $m['id'];
				$echo.= "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" ".($m['active']==true || $m['active']=="Y" ? "checked=checked": "")." ".( (isset($m['help']) && strlen($m['help'])>0) ? " help_text=\"".$m['help']."\" " : "")." />";
				$echo.= "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
			}
			$echo.= "</div>";
		$echo.= "<script type=text/javascript>
			  $(function() {
				$(\"#".$id."\").buttonset();
				$(\"#".$id." input[type='radio']\").click( function() {
					AssistHelper.processingForNavigation();
				  document.location.href = $(this).val();
				});

			  });
		</script>";
		$this->nav_buttons_level++;
		return $echo;
	}	//end drawNav($nav)

/* get the Business PArtner contact details & store in session */
	function getBPAdetails($helpdesk_reseller="", $logo_reseller="") {
		$helpdesk_reseller = strlen($helpdesk_reseller)>0 ? $helpdesk_reseller : $_SESSION['ia_cmp_reseller'];
		$logo_reseller = strlen($logo_reseller)>0 ? $logo_reseller : $_SESSION['ia_cmp_logo_reseller'];

		if(!isset($_SESSION['bpa_details']) || strlen($helpdesk_reseller)>0) {
			$db = new ASSIST_DB("master");
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r
					INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
					WHERE r.cmpcode = '".strtoupper($helpdesk_reseller)."'";
			$helpdesk_bpa_details = $db->mysql_fetch_one($sql);
			$sql = "SELECT r.*, c.cmpname FROM assist_reseller r
					INNER JOIN assist_company c ON c.cmpcode = r.cmpcode
					WHERE r.cmpcode = '".strtoupper($logo_reseller)."'";
			$logo_bpa_details = $db->mysql_fetch_one($sql);
			unset($db);
				$bpa_con = explode("|",$helpdesk_bpa_details['help_contact']);
				$bpa_contact = array();
				foreach($bpa_con as $b) {
					$b2 = explode("_",$b);
					$bpa_contact[] = $b2[1]." (".strtolower($b2[0]).")";
				}

				$helpdesk_bpa_details['contact'] = $bpa_contact;
				$helpdesk_bpa_details['assist_logo'] = $logo_bpa_details['assist_logo'];
				$helpdesk_bpa_details['site_name'] = $logo_bpa_details['site_name'];
				$helpdesk_bpa_details['site_code'] = $logo_bpa_details['site_code'];
				$_SESSION['bpa_details'] = $helpdesk_bpa_details;
				$bpa_details = $helpdesk_bpa_details;
				if(isset($logo_bpa_details['assist_logo']) && strlen($logo_bpa_details['assist_logo'])>0) {
					$_SESSION['DISPLAY_INFO']['title_logo'] = "pics/bp_logos/".$logo_bpa_details['assist_logo'];
				}
				if(isset($logo_bpa_details['site_name']) && strlen($logo_bpa_details['site_name'])>0) {
					$_SESSION['DISPLAY_INFO']['ignite_name'] = $logo_bpa_details['site_name'];
				}
		} else {
			$bpa_details = $_SESSION['bpa_details'];
		}
		//$this->arrPrint($bpa_details);
		return $bpa_details;
	}
	function getReseller($cmpcode) {
		$db = new ASSIST_DB("master");
		$sql = "SELECT cmpreseller FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";

		$row = $db->mysql_fetch_one($sql);
		return $row['cmpreseller'];
	}

/* display go back triangle onscreen */
	function displayGoBack($url = "",$txt = "Go Back") {
		echo "<p>".$this->getGoBack($url,$txt)."</p>";
	}
	static function goBack($url="",$txt="Go Back") {
		if(strlen($url)>0) {
			echo "<a href=$url /><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a>";
		} else {
			echo "<a href=\"javascript:history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=\"javascript:history.back();\">Go Back</a>";
		}
	}

/* generate the go back triangle */
	function getGoBack($url = "",$txt = "Go Back",$include_js=false) {
		if(strlen($txt)==0) { $txt = "Go Back"; }
		if(strlen($url)>0) {
			$code = "<div id=div_go_back><a href=$url /><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a></div>";
			if($include_js) {
				$code.="
				<script type=text/javascript>
				$(function() {
					$('#div_go_back a').click(function() {
						AssistHelper.processing();
					});
				});
				</script>
				";
			}
		} else {
			$code = "<a href=\"javascript:history.back();\"><img src=\"/pics/tri_left.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=\"javascript:history.back();\">Go Back</a>";
		}
		return $code;
	}

/* generate & display the go up triangle */
	function displayGoUp($url, $txt = "Back to Top") {
		if(strlen($url)>0) {
			echo "<p><a href=$url /><img src=\"/pics/tri_up.gif\" style=\"vertical-align: middle; border: 0px;\"></a> <a href=$url>$txt</a></p>";
		}
	}

/* get home/frontpage user profile */
	function getUserProfile($cmpcode="",$tkid="") {
		$max_profile = 4; //last profile section reference
		if(strlen($cmpcode)==0 || strlen($tkid)==0) {
			$assist = new ASSIST();
			$cmpcode = $assist->getCmpCode();
			$tkid = $assist->getUserID();
		}
		$db = new ASSIST_DB("client",$cmpcode);
		$_SESSION['USER_PROFILE'] = array();
		$field5_serials = array(2,3);
		$sql = "SELECT * FROM assist_".$cmpcode."_timekeep_profile WHERE tkid = '$tkid' AND active = 1 ORDER BY section ASC";
		$profile = $db->mysql_fetch_all_fld($sql,"section");

		for($p=1;$p<=$max_profile;$p++) {
			if(isset($profile[$p])) {
				$prf = $profile[$p];
				if(in_array($p,$field5_serials)) {
					$f5 = $prf['field5'];
					$prf['field5'] = unserialize($f5);
				}
				$_SESSION['USER_PROFILE'][$p] = $prf;
			} else {
				switch($p) {
				case 1:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,			'tkid' => $tkid,			'active' => true,			'section' => 1,			'field1' => "",			'field2' => "15",			'field3' => "dead_asc",			'field4' => 7,			'field5' => "",
					);
					break;
				case 2:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,	'tkid' => $tkid,	'active' => true,	'section' => 2,	'field1' => "ALL",	'field2' => "",	'field3' => "",	'field4' => "",	'field5' => array(),
					);
					break;
				case 3:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,	'tkid' => $tkid,	'active' => true,	'section' => 3,	'field1' => "",	'field2' => "",	'field3' => "",	'field4' => "",	'field5' => array(),
					);
					break;
				default:
					$_SESSION['USER_PROFILE'][$p] = array(
						'id' => 0,	'tkid' => $tkid,	'active' => true,	'section' => $p,	'field1' => "",	'field2' => "",	'field3' => "",	'field4' => "",	'field5' => array(),
					);
					break;
				}
			}//endif profile exists for section
		}//end for each profile section
	} //end getUserProfile function




	static function getModuleLogoDetails() {
		$shortcut_details = array();
		$shortcut_details['path'] = "../pics/module/";
		$shortcut_details['logos'] = array(
			'CAPS'	=> "2018caps.png",
			'CAPS1B'	=> "2018caps.png",
			'ACTION'=> "2018action.png",
			'ACTION0'=> "2018action.png",
			'JOBS'=> "2018action.png",
			'SDBIP'	=> "sdbp4.png",
			'SDBP2'	=> "sdbp4.png",
			'SDBP3'	=> "sdbp4.png",
			'SDBP4'	=> "sdbp4.png",
			'SDBP5'	=> "2013sdbp5.png",
			'SDBP5B'	=> "2018sdbp5b.png",
			'SDBP6'	=> "2018sdbp5b.png",
			'SDBP5F'	=> "2019sdbp5f.png",
			'DOC'	=> "2018doc.png",
			'DOC0'	=> "2018doc.png",
			'TD'	=> "td.png",
			'DSHP'	=> "dshp.png",
			'USER_MANUALS'	=> "manuals.png",
			'TK'	=> "2018tk.png",
			'PM'	=> "pm.png",
			'SDBP5PM'	=> "2016sdbp5pm.png",
			'SDBP5BCPM'	=> "2016sdbp5pm.png",
			'PM3'	=> "2018pm3.png",
			'PM4'	=> "2018pm3.png",
			'PM5'	=> "2018pm3.png",
			'PM6'	=> "2018pm3.png",
			'IND_KPI'	=> "2016sdbp5i.png",
			'IKPI1'	=> "2016sdbp5i.png",
			'IKPI2'	=> "2016sdbp5i.png",
			'COMPL'=>"2018compl.png",
			'QUERY'=>"2018query.png",
			'CASE'=>"2013query.png",
			'RGSTR'=>"2013rgstr.png",
			'RGSTR2'=>"2018rgstr2.png",
			'RGSTR_ALT'=>"2014rgstr_alt.png",
			'RAPS'=>"2018raps.png",
			'S'=>"2018s.png",
			'HP'=>"hp.png",
			'NB'=>"noticeboard2014.png",
			'CONTRACT'	=>	"contract2014.png",
			'CNTRCT'	=>	"2018cntrct.png",
			'SCORECARD'	=>	"scorecard2014.png",
			'SMS'	=> "2014sms.png",
			'MASTER'	=> "2018master.png",
			'MDOC'	=> "2015doc1.png",
			'DTM'=>"dtm.png",
			'SAGE'=>"sage.png",
			'reports'=>"2016reports.png",
			'IDP1'=>"2016idp1b.png",
			'IDP2'=>"2016idp1b.png",
			'IDP3'=>"2018idp3.png",
			'JAL1'=>"2018jal1.png",
			'LDAL1'=>"2017ldal1.png",
			'AIT'=>"2020ait.jpg",
			'PDP'=>"2018pdp.png",
			'PDP2'=>"2018pdp.png",
			'helpdesk'=>"helpdesk.png",
			'EMP1'=>"2018emp1.png",
			'MSCOA1'=>"2018mscoa1.png",
			'module_management'=>"2019_modman.png",
			'dashboard_management'=>"2019_dashman.png",
		);
/*  REPLACED ON 2018-03-25 BY RK
		$shortcut_details['logos'] = array(
			'CAPS'	=> "2013caps.png",
			'CAPS1B'	=> "2013caps.png",
			'ACTION'=> "2013action.png",
			'ACTION0'=> "2013action.png",
			'JOBS'=> "2013action.png",
			'SDBIP'	=> "sdbp4.png",
			'SDBP2'	=> "sdbp4.png",
			'SDBP3'	=> "sdbp4.png",
			'SDBP4'	=> "sdbp4.png",
			'SDBP5'	=> "2013sdbp5.png",
			'SDBP5B'	=> "2013sdbp5.png",
			'SDBP5C'	=> "2013sdbp5.png",
			'SDBP6'	=> "2017sdbp6.png",
			'DOC'	=> "2013doc.png",
			'DOC0'	=> "2013doc.png",
			'TD'	=> "td.png",
			'DSHP'	=> "dshp.png",
			'USER_MANUALS'	=> "manuals.png",
			'TK'	=> "2015tk.png",
			'PM'	=> "pm.png",
			'SDBP5PM'	=> "2016sdbp5pm.png",
			'SDBP5BCPM'	=> "2016sdbp5pm.png",
			'PM3'	=> "2016sdbp5pm.png",
			'IND_KPI'	=> "2016sdbp5i.png",
			'COMPL'=>"2013compl.png",
			'QUERY'=>"2013query.png",
			'CASE'=>"2013query.png",
			'RGSTR'=>"2013rgstr.png",
			'RGSTR2'=>"2013rgstr.png",
			'RGSTR_ALT'=>"2014rgstr_alt.png",
			'RAPS'=>"2013raps.png",
			'S'=>"2013setup.png",
			'HP'=>"hp.png",
			'NB'=>"noticeboard2014.png",
			'CONTRACT'	=>	"contract2014.png",
			'CNTRCT'	=>	"2015cntrct.png",
			'SCORECARD'	=>	"scorecard2014.png",
			'SMS'	=> "2014sms.png",
			'MASTER'	=> "2015master2.png",
			'MDOC'	=> "2015doc1.png",
			'DTM'=>"dtm.png",
			'SAGE'=>"sage.png",
			'reports'=>"2016reports.png",
			'IDP1'=>"2016idp1b.png",
			'IDP2'=>"2016idp1b.png",
			'JAL1'=>"2017jal1.png",
			'LDAL1'=>"2017ldal1.png",
			'AIT'=>"ait.png",
			'PDP'=>"2017pdp.png",
			'helpdesk'=>"helpdesk.png",
			'EMP1'=>"2017emp1.png",
			'MSCOA1'=>"2018mscoa1.png",
			'IDP3'=>"2018_idp3.png",
		);
*/
		$shortcut_details['default'] = "circle.png";
		$shortcut_details['new'] = "add_new.png";
		return $shortcut_details;
	}






















	/***************************************
			PAGE HEADERS
	****************************************/




	function displayPageHeader($jquery=true,$amcharts=true,$base_target="main",$top=0,$right=5,$bottom=0,$left=5) {
		$jquery_version = isset($_REQUEST['jquery_version']) && strlen($_REQUEST['jquery_version'])>0 ? $_REQUEST['jquery_version'] : "1.8.24";
		$time = time();
		echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Action4u.co.za</title>

	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js?'.$time.'" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js?'.$time.'" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css?'.$time.'" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js?'.$time.'"></script>' : '').'

	<link rel="stylesheet" href="/assist.css?'.$time.'" type="text/css">
	<script type ="text/javascript" src="/assist.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistform.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js?'.$time.'"></script>
</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
	}




	function displayPageHeaderJQ($jquery_version="1.10.0",$jquery=true,$amcharts=true,$base_target="main",$top=0,$right=5,$bottom=0,$left=5) {
		$time = time();
		echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Action4u.co.za</title>

	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js?'.$time.'" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js?'.$time.'" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css?'.$time.'" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js?'.$time.'"></script>' : '').'

	<link rel="stylesheet" href="/assist.css?'.$time.'" type="text/css">
	<script type ="text/javascript" src="/assist.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistform.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js?'.$time.'"></script>
</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
	}


	static function echoPageHeader($jquery_version="1.10.0",$scripts=array(),$css=array()) {
		$time = time();
	    if($jquery_version == ""){$jquery_version="1.10.0";}
		$jquery_css_version = $jquery_version=="1.10.4" ? "1.10.0" : $jquery_version;
		$jquery=true; $amcharts=true; $base_target="main"; $top=0; $right=5; $bottom=0; $left=5;
		echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Action4u.co.za</title>

	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js?'.$time.'" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js?'.$time.'" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_css_version.'/css/jquery-ui.css?'.$time.'" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js?'.$time.'"></script>' : '').'

	<link rel="stylesheet" href="/assist.css?'.$time.'" type="text/css">
	<script type ="text/javascript" src="/assist.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistform.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/jquery.sessionTimeout.js?'.$time.'"></script>';
	if(is_array($scripts) && count($scripts)>0) {
		foreach($scripts as $s) {
			echo '
			<script type ="text/javascript" src="'.$s.'?'.$time.'"></script>';
		}
	}
	if(is_array($css) && count($css)>0) {
		foreach($css as $c) {
			echo '
			<link rel="stylesheet" href="'.$c.'" type="text/css">';
		}
	}
echo '
	</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
	}


//get page header for use in outputting buffer - linked to above echoPageHeader function
	public function getPageHeader($jquery_version="1.10.0",$scripts=array(),$css=array()) {
		$time = time();
	    if($jquery_version == ""){$jquery_version="1.10.0";}
		$jquery=true; $amcharts=true; $base_target="main"; $top=0; $right=5; $bottom=0; $left=5;
		$output = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Action4u.co.za</title>

	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="/library/amcharts/javascript/amcharts.js?'.$time.'" type="text/javascript"></script>
	<script src="/library/amcharts/javascript/raphael.js?'.$time.'" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css?'.$time.'" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js?'.$time.'"></script>' : '').'

	<link rel="stylesheet" href="/assist.css?'.$time.'" type="text/css">
	<script type ="text/javascript" src="/assist.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assisthelper.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assiststring.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistform.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/assistarray.js?'.$time.'"></script>
	<script type ="text/javascript" src="/library/js/jquery.sessionTimeout.js?'.$time.'"></script>';
	if(is_array($scripts) && count($scripts)>0) {
		foreach($scripts as $s) {
			$output.= '
			<script type ="text/javascript" src="'.$s.'"></script>';
		}
	}
	if(is_array($css) && count($css)>0) {
		foreach($css as $c) {
			$output.= '
			<link rel="stylesheet" href="'.$c.'" type="text/css">';
		}
	}
$output.='
	</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
return $output;
	}

//get page header for use in outputting buffer - linked to above echoPageHeader function
	public function getFixedPageHeader($jquery_version="1.10.0",$scripts=array(),$css=array()) {
		$time = time();
		if($jquery_version == ""){$jquery_version="1.10.0";}
		$jquery=true; $amcharts=true; $base_target="main"; $top=0; $right=5; $bottom=0; $left=5;
		$output = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Action4u.co.za</title>

	<base target="'.$base_target.'">

	'.($amcharts===true ? '<script src="https://assist.action4u.co.za/library/amcharts/javascript/amcharts.js?'.$time.'" type="text/javascript"></script>
	<script src="https://assist.action4u.co.za/library/amcharts/javascript/raphael.js?'.$time.'" type="text/javascript"></script>' : '').'

	'.($jquery===true ? '<link href="https://assist.action4u.co.za/library/jquery-ui-'.$jquery_version.'/css/jquery-ui.css?'.$time.'" rel="stylesheet" type="text/css"/>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/jquery-ui-'.$jquery_version.'/js/jquery.min.js?'.$time.'"></script>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/jquery-ui-'.$jquery_version.'/js/jquery-ui.min.js?'.$time.'"></script>' : '').'

	<link rel="stylesheet" href="https://assist.action4u.co.za/assist.css?'.$time.'" type="text/css">
	<script type ="text/javascript" src="https://assist.action4u.co.za/assist.js?'.$time.'"></script>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/js/assisthelper.js?'.$time.'"></script>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/js/assiststring.js?'.$time.'"></script>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/js/assistform.js?'.$time.'"></script>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/js/assistarray.js?'.$time.'"></script>
	<script type ="text/javascript" src="https://assist.action4u.co.za/library/js/jquery.sessionTimeout.js?'.$time.'"></script>';

		$output.='
	</head>
<body style="margin: '.$top.'px '.$right.'px '.$bottom.'px '.$left.'px;">
';
		return $output;
	}



	/********
		FUNCTIONS TO FORMAT NUMBERS
			********/
	const NUMBER_DEFAULT_TYPE = "FLOAT";
	const NUMBER_DECIMAL_PLACES = 2;
	const NUMBER_THOUSANDS = true;
	const NUMBER_CURRENCY_SYMBOL = "R";
	const NUMBER_PERCENT_SYMBOL = "%";
	const NUMBER_THOUSANDS_SEPARATOR = " ";

	static function format_number($n,$type=self::NUMBER_DEFAULT_TYPE,$dec=self::NUMBER_DECIMAL_PLACES,$thou=self::NUMBER_THOUSANDS,$symbol="") {
		if(!isset($types[$type])) {
			$types = array(
				'FLOAT'=>array("FLOAT","DOUBLE","CALC","RAW"),
				'INT'=>array("INT","WHOLE"),
				'CURRENCY'=>array("CURR","CURRENCY","RAND","RANDS"),
				'PERC'=>array("PERC"),
			);
			foreach($types as $key=>$arr) {
				if(in_array(strtoupper($type),$arr)) { $type = $key; break; }
			}
		}
		if($thou===true) { $thou_sep = self::NUMBER_THOUSANDS_SEPARATOR; } else { $thou_sep = ''; }
		if($dec===true) { $dec = self::NUMBER_DECIMAL_PLACES; } elseif($dec===false) { $dec = 0; }
		switch($type) {
		case "INT":
			return number_format($n,0,'.',$thou_sep);
			break;
		case "FLOAT":
		case "PERC":
		case "CURRENCY":
		default:
			$x = number_format($n,$dec,'.',$thou_sep);
			if($type=="CURRENCY") {
				$x = (strlen($symbol)==0 ? self::NUMBER_CURRENCY_SYMBOL : $symbol)." ".$x;
			} elseif($type=="PERC") {
				$x .=(strlen($symbol)==0 ? self::NUMBER_PERCENT_SYMBOL : $symbol);
			}
			return $x;
			break;
		}
	}

	static function format_percent($n,$d=2) {
		return self::format_number($n,"PERC",$d);
	}
	static function format_currency($n,$curr=self::NUMBER_CURRENCY_SYMBOL) {
		return self::format_number($n,"CURRENCY",self::NUMBER_DECIMAL_PLACES,self::NUMBER_THOUSANDS,$curr);
	}
	static function format_integer($n) {
		return self::format_number($n,"INT");
	}
	static function format_float($n) {
		return self::format_number($n);
	}


	/** END NUMBER FORMATTING FUNCTIONS **/



	function formatBytes($b,$p = null,$decimal=2) {
		/**
		 *
		 * @author Martin Sweeny
		 * @version 2010.0617
		 *
		 * returns formatted number of bytes.
		 * two parameters: the bytes and the precision (optional).
		 * if no precision is set, function will determine clean
		 * result automatically.
		 *
		 **/
		$units = array("B","kB","MB","GB","TB","PB","EB","ZB","YB");
		$c=0;
		if(!$p && $p !== 0) {
			foreach($units as $k => $u) {
				if(($b / pow(1024,$k)) >= 1) {
					$r["bytes"] = $b / pow(1024,$k);
					$r["units"] = $u;
					$c++;
				}
			}
			return number_format($r["bytes"],$decimal) . " " . $r["units"];
		} else {
			return number_format($b / pow(1024,$p),$decimal) . " " . $units[$p];
		}
	}



	/** ticks and crosses **/
	static function drawStatus($s,$st="") {
		if($s===true || ":".$s.":"==":1:") { $s="Y"; } elseif($s===false || ":".$s.":"==":0:") { $s="N"; }
		switch($s) {
			case "Y":
				$icon = "circle-check";
				$style = "yes";
				break;
			case "info":
				$icon = "circle-minus";
				$style = "nothing";
				break;
			case "info2":
				$icon = "info";
				$style = "nothing";
				break;
			case "warn":
				$icon = "alert";
				$style = "fail";
				break;
			case "pending":
				$icon = "alert";
				$style = "nothing";
				break;
			case "lock":
				$icon = "locked";
				$style = "nothing";
				break;
			case "unlock":
				$icon = "unlocked";
				$style = "yes";
				break;
			case "na":
				$icon = "circle-minus";
				$style = "grey";
				break;
			case "N":
				$style = "fail"; $icon = "circle-close";
				break;
			case "in_progress":
				$style = "nothing";
				$icon = "circle-plus";
				break;
			default:
				$icon = $s;
				$style = (strlen($st)>0 ? $st : $s);
				//echo "else".$style;
				break;
			}
		$return = "<div style='width: 16px; margin: 0 auto;' class='ui-widget ui-state-".$style."'><span class='ui-icon ui-icon-".$icon."'>&nbsp;&nbsp;</span></div>";
		return $return;
	}


	static function getDisplayStatusAsDiv($icon,$options="") {
		if($icon===true || $icon=="true") { $icon="1"; } elseif($icon===false || $icon=="false") {$icon="0";}
		switch($icon) {
		case "ok":
		case "1":
			return "
				<div class=\"ui-icon ui-icon-circle-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "error":
		case "0":
			return "
				<div class=\"ui-icon ui-icon-circle-close\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "info":
			return "
				<div class=\"ui-icon ui-icon-circle-minus\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>
			";
			break;
		case "in_progress":
			return "
				<div class=\"ui-icon ui-icon-circle-plus\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>
			";
			break;
		}
	}


	static function dateToInt($d) {
		$d = strtotime($d);
		if(intval(date("H",$d))==0) {
			$d+=12*60*60;
		}
		return $d;
	}


	/**
	 * Function to convert array into string for SQL statement
	 *
	 * @param (Array) var = array ('db field'=>"value")
	 * @param (String) join = glue for implode, default = , for insert
	 * @return (String) field = 'value', field2 = 'value2'
	 */
    public function convertArrayToSQL($var,$join=",") {
    	$insert_data = array();
		foreach($var as $fld => $v){
			$state = is_array($v) ? $v[0] : true;
			if(is_array($v)) { unset($v[0]); if(count($v)==1) { $v = $v[1]; } }
			if($state===true) {
				if(is_array($v)) {
					$insert_data[] = $fld." IN ('".implode("','",$v)."')";
				} else {
					$insert_data[] = $fld." = '".$v."'";
				}
			} else {
				if(is_array($v)) {
					$insert_data[] = $fld." NOT IN ('".implode("','",$v)."')";
				} else {
					$insert_data[] = $fld." <> '".$v."'";
				}
			}
		}
		return implode(" ".$join." ",$insert_data);
    }


	/**
	 * Simplified Function to convert array into string for SQL statement
	 *
	 * @param (Array) var = array ('db field'=>"value")
	 * @param (String) join = glue for implode, default = , for insert
	 * @return (String) field = 'value', field2 = 'value2'
	 */
    public function convertArrayToSQLForSave($var,$join="") {
    	if(strlen($join)==0) { $join = self::JOIN_FOR_SQL_FOR_SAVE; }
    	$insert_data = array();
		foreach($var as $fld => $v){
			if(is_array($v)) {
				$insert_data[] = $fld." = '".implode(self::JOIN_FOR_MULTI_LISTS,$v)."'";
			} else {
				$insert_data[] = $fld." = '".$v."'";
			}
		}
		return implode(" ".$join." ",$insert_data);
    }



	/* function to convert a string to its hex value - used for passwords */
	public function convertToHex($pa,$error_value = "_") {
		$pb = "";
		for($a=0;$a<strlen($pa);$a++) {
			$alpha = substr($pa,$a,1);
			$pb.= isset($this->hex[$alpha]) ? $this->hex[$alpha] : $error_value;
		}
		return $pb;
	}
	public function convertFromHex($pa,$error_value = "_") {
		$alpha = array_flip($this->hex);
		$pb = "";
		for($a=0;$a<strlen($pa);$a+=2) {
			$hex = substr($pa,$a,2);
			$pb.= isset($alpha[$hex]) ? $alpha[$hex] : $error_value;
		}
		return $pb;
	}

	public function formatTextForComparison($text) {
		$text = ASSIST_HELPER::decode($text);
		$text = strtolower($text);
		$text = str_replace(chr(10),'',$text);
		$text = str_replace(' ','',$text);
		$text = str_replace('?','',$text);
		return base64_encode($text);
	}

	public function formatTextForSorting($text) {
		$text = ASSIST_HELPER::decode($text);
		$text = strtolower($text);
		$text = str_replace(chr(10),'',$text);
		$text = str_replace(' ','',$text);
		$text = str_replace(chr(9),'',$text);
		$text = str_replace('?','',$text);
		return ($text);
	}

	public function cleanUpText($text) {
		$text = stripslashes($text);
		$text = ASSIST_HELPER::decode($text);
		$text = str_replace(chr(10),'',$text);
		$text = trim($text);
		return $text;
	}

	/*******************************************************************************************************************
	 * Historical functions from old /inc_assist.php
	 */
	function displayAuditLogOldStyle($logs,$flds,$extra) {
		/* $flds = array('date'=>,'user'=>,'action'=>); */

		echo "<table  width=707 class=noborder><tr><td class=noborder>$extra</td><td class=noborder>";
			if(count($logs)>0) {
		echo "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\"><img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span></span>";
		echo "</td></tr><tr><td colspan=2 class=noborder>";
		echo "<div id=my_audit_log>";
				echo "<h3 class=log>Activity log</h3>";
				echo "<table width=700 id=tbl_log>
						<tr>
							<th class=log>Date</th>
							<th class=log>User</th>
							<th class=log>Action</th>
						</tr>";
					foreach($logs as $l) {
						echo "<tr>";
							$a=0;
							foreach($flds as $f) {
								$a++;
								if($a==1) {
									if(!is_numeric($l[$f])) { $d = strtotime($l[$f]); } else { $d = $l[$f]; }
									echo "<td class=\"log centre\">".date("d M Y H:i",$d)."</td>";
								} else {
									echo "<td class=log>".stripslashes($this->decode($l[$f]))."</td>";
								}
							}
						echo "</tr>";
					}
				echo "</table></div>";
		echo "<script type=text/javascript>
				$(function() {
					$(\"#my_audit_log\").hide();
					$(\"#disp_audit_log\").click(function() {
						if($('#my_audit_log').is(':visible')) {
							$(\"#my_audit_log\").hide();
							$(\"#log_txt\").attr(\"innerText\",\"Display Activity Log\");
							$(\"#log_pic\").attr(\"src\",\"/pics/tri_down.gif\");
						} else {
							$(\"#my_audit_log\").show();
							$(\"#log_txt\").attr(\"innerText\",\"Hide Activity Log\");
							$(\"#log_pic\").attr(\"src\",\"/pics/tri_up.gif\");
						}
					});
					$(\"#tbl_log a\").click(function() {
						var u = $(this).attr(\"id\");
						if(u.length>0)
							window.open(u, \"_blank\",\"status=no,toolbar=no,location=no,scrollbars=no,directories=no\");
					});
				});
			</script>";
			}
		echo "</td></tr></table>";
	}


	function __destruct() {}
}


?>