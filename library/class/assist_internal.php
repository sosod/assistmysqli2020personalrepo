<?php
/**
 * TO MANAGE THE LINKS BETWEEN INTERNAL ASSIST MODULES
 * Originally created to manage the link between RGSTR2 and LDAL1 modules
 * 
 * Created By: JC
 * Created On: 30 August 2017
 */

class ASSIST_INTERNAL extends ASSIST_MODULE_HELPER {
	
	private $source_modules_by_type = array(
		'library'=>array("JAL1","LDAL1"),
		'sdbip'=>array("SDBP5","SDBP5B"),
	);
	
	private $source_type;
	private $sources = array();
	private $source_modref;
	private $sourceObject;
	
	public function __construct() {
		parent::__construct();
		if(isset($_SESSION[$_SESSION['modref']]['sources'])) {
			$this->sources = $_SESSION[$_SESSION['modref']]['sources'];
		}
	}
	
	
	public function setSourceType($type) {
		$this->source_type = $type;
	}
	
	public function getSources() {
		if(strlen($this->source_type)==0 || !isset($this->source_modules_by_type[$this->source_type])) {
			return array();
		} elseif(isset($_SESSION[$_SESSION['modref']]['sources'])) {
			$this->sources = $_SESSION[$_SESSION['modref']]['sources'];

            $rows =  $_SESSION[$_SESSION['modref']]['sources'];
            $data = array();
            foreach($rows as $r) {
                $data[$r['modref']] = $r['modtext'];
            }
            return $data;
		} else {
			$sql = "SELECT * FROM assist_menu_modules WHERE modlocation IN ('".implode("','",$this->source_modules_by_type[$this->source_type])."') ORDER BY modtext";
			$rows = $this->mysql_fetch_all_by_id($sql,"modref");
			$this->sources = $rows;
			$_SESSION[$_SESSION['modref']]['sources'] = $rows;
			$data = array();
			foreach($rows as $r) {
				$data[$r['modref']] = $r['modtext'];
			}
			return $data;
		}
	}
	
	
	public function setSource($source) {
		$this->source_modref = $source;
		$_SESSION['secondary_modref'] = $source;
		$class_name = $this->sources[$source]['modlocation']."_INTERNAL";
		$this->sourceObject = new $class_name($source);
	}
	
	public function getFilters($source,$include_blanks=false) {
		$this->setSource($source);
		$filters = array();
		$filters = $this->sourceObject->getFilters($include_blanks);
		return $filters;
	}

    public function getLegacyDataStructure($source) {
        $this->setSource($source);
        $data_structure = $this->sourceObject->getLegacyDataStructure();
        return $data_structure;
    }
	
	
	public function getLinesByFilter($filter) {
		return $this->sourceObject->getLinesByFilter($filter);
	}
	
	public function getLinesByID($modref,$id=array()) {
		$this->setSource($modref);
		return $this->sourceObject->getLinesByID($id);
	}

	
	public function __destruct() {
		parent::__destruct();
	}
	
}

?>