<?php
/* Base Database Connection class for connecting to Ignite Assist
Created by: Janet Currie (c)
Created on: 20 August 2012
*/
@ini_set('default_charset', "");
class ASSIST_DBCONN { //extends ASSIST_HELPER {

/* VARIABLES */
	private $server;
	/** these settings must be updated in the __construct() function
	depending on the site that the class is running on **/
	//db name settings
	protected $db_prefix;
	protected $db_other;
	//login settings
	private $db_user;
	private $db_pwd;
	private $db_host;

	//database names
	private $db_client;		//this is the default database name based on the client currently logged in
	private $db_name;		//this is the actual database name used to connect to the mysql server => can be changed through setDBName($dbn)
	private $db_master;		//this is the name of the database with the assist_company and other master tables in it
	private $db_help;		//this is the name of the Ignite Help database
	private $db_helpdesk;		//name of new helpdesk database
	private $db_sage_integ;		//this is the name of the Ignite Help database
	private $db_dtm;		//name of new dtm database
	private $db_cassist;	//this is the name of the CASSIST database
	private $db_blank;		//this is the name of the blank database used to create new client dbs/modules
	private $db_type;		//this indicates whether the connection is to a client database or the master, help or cassist databases

    //error settings
    private $db_error_to;
    private $db_error_from;
    private $db_error_cc;
    /* This is a constant to identify the site/computer that generated an error msg:
            JC_* => A development computer belonging Janet Currie
            AZ_* => A development computer belonging to Admire Zinamo
            LIVE => assist.ignite4u.co.za
            DEMO => demo.ignite4u.co.za
            DEV => dev.ignite4u.co.za
    */
    const SITE_SRC = "JC";

    //resources
    protected $db_resource;
    protected $db_ref;
    protected $db_modref;

    private $cmpcode = "";

/* MAIN FUNCTIONS */
	public function __construct($dbn="",$cc="") {
        //Site-dependant values
        $cc = (strlen($cc)==0 && isset($_SESSION['cc']) ? strtolower($_SESSION['cc']) : strtolower($cc));
        $this->db_prefix = "adev";
        $this->db_other = $this->db_prefix."_i";
        $this->db_user = ($this->isLocalRepository() ? "root" : "ignittps_ignite4");
        $this->db_pwd = ($this->isLocalRepository() ? "" : "ign92054u");
        $this->db_host = "localhost";
		$cc = (strlen($cc)==0 && isset($_SESSION['cc']) ? strtolower($_SESSION['cc']) : strtolower($cc));
		//DB Names
		$this->db_master = $this->db_prefix."_ignite4u";
		$this->db_help = $this->db_prefix."_help";
		$this->db_helpdesk = $this->db_prefix."_helpdesk";
		$this->db_sage_integ = $this->db_prefix."_sage_integration";
		$this->db_dtm = $this->db_prefix."_dtm";
		$this->db_cassist = $this->db_other."cassist";
		$this->db_blank = $this->db_other."blank";
		//Email addresses
		$this->db_error_to = "actionit.actionassist@gmail.com";
		//$this->db_error_from - in site specific checks
		$this->db_error_cc = "";
		//Connect
		$this->db_type = strtolower($dbn);
		switch(strtolower($dbn)) {
			case "master":	$dbn = $this->getMasterName();		break;
			case "cassist":	$dbn = $this->getCASSISTName();		break;
			case "help":	$dbn = $this->getHelpName();		break;
            case "finteg":
                $this->db_prefix = "finteg";
                $this->db_other = $this->db_prefix."_i";
                $dbn = $this->db_other.$cc;
                break;
			case "helpdesk":	
				$dbn = $this->getHelpdeskName();		
				$this->db_ref = "helpdesk";
				break;
			case "dtm":
				$dbn = $this->getDTMName();
				$this->db_ref = "dtm";
				break;
			case "sage":
				$this->db_prefix = "sage";
				$this->db_sage_integ = $this->db_prefix."_sage_integration";

				$dbn = $this->getSageIntegName();
				$this->db_ref = "sage";
				break;
			case "blank":	$dbn = $this->getBlankName();		break;
			case "other":	$dbn = $cc; break;
			case "client":
			default:
				$this->db_type = "client";
				$dbn = $this->db_other.$cc;
				$this->db_client = strtolower($dbn);
				$this->setDBRef();
				break;
		}
		$dbn = strtolower($dbn);
		$this->setDBName($dbn);
//		$this->db_connect();
		if(strlen($cc)>0) {
			$this->cmpcode = strtolower($cc);
		}
	}

    public function isLocalRepository(){
        $isLocalRepository = false;
        if(isset($_SERVER['APPLICATION_ENV']) && strpos($_SERVER['APPLICATION_ENV'],"development")!==false || $_SERVER['SERVER_NAME'] == 'centralized.action4u.localhost'){
            $isLocalRepository = true;
        }
        return $isLocalRepository;
    }

    public function get_db_resource(){
        return $this->db_resource;
    }

	public function db_connect() {
		//$this->db_close();//Had to comment this out because the system broke

		try{
			if(!$this->db_resource = mysqli_connect( $this->db_host, $this->db_user, $this->db_pwd)) {
					throw new Exception(mysqli_error($this->db_resource));
			}
			if(!mysqli_select_db($this->db_resource, $this->db_name)) {
					throw new Exception(mysqli_error($this->db_resource));
			}

		} catch ( Exception $e ){
			$this->sendError(mysqli_error($this->db_resource),"CONN");
		}
			if(isset($_SESSION['session_timeout']) && is_array($_SESSION['session_timeout']) && isset($_SESSION['session_timeout']['expiration']) && isset($_SESSION['session_timeout']['setting']) && isset($_SESSION['session_timeout']['current'])) {
				$now = time();
				if($now<$_SESSION['session_timeout']['expiration']) {
					$_SESSION['session_timeout']['expiration'] = $now+$_SESSION['session_timeout']['setting'];
					$_SESSION['session_timeout']['current'] = $now;
				} else {
					die($this->autoLogoutDB("TIMEOUT"));
				}
			}
	}
	public function db_close() {
		if(is_resource($this->db_resource)) {
			mysqli_close($this->db_resource);
		}
	}



/* Functions to set variables */
	//Change the database name if needed => send blank to revert back to default client db name
	public function setDBName($dbn = "") {
		$this->db_name = strlen($dbn)==0 ? $this->db_client : $dbn;
		$this->db_connect();
	}
	//Set an additional email recipient to receive the error emails
	public function setEmailCC($em) {
		$this->db_error_cc = $em;
	}
	//Change the DBRef => send blank to revert to the default
	public function setDBRef($mr="",$cc="") {
		$cc = strlen($cc)==0 && isset($_SESSION['cc']) ? $_SESSION['cc'] : $cc;
		$cc = strlen($cc)==0 ? $this->cmpcode : $cc;
		$original_mr = $mr;
		$mr = strlen($mr)==0 && isset($_SESSION['modref']) ? $_SESSION['modref'] : $mr;
		$this->db_ref = strtolower("assist_".$cc."_".$mr);
		if(strtolower($cc)!=$this->cmpcode) {
			$dbn = $this->db_other.strtolower($cc);
			$this->setDBName($dbn);
			$this->db_connect();
			if(strlen($cc)>0) {
				$this->cmpcode = strtolower($cc);
			}
		}
		$session_mr = "";
		if(isset($_SESSION['modref'])) {
			$session_mr = $_SESSION['modref'];
		} elseif(isset($_SESSION['ref'])) {
			$session_mr = $_SESSION['ref'];
		}

		if(strtolower($original_mr)!=$session_mr) {
			$this->db_modref = $original_mr;
		} else {
			$this->db_modref = $session_mr;
		}
	}
	//Alias for setDBRef (replace function in ASSIST class)
	public function setModRef($mr) {
		$this->setDBRef($mr);
	}
	
/* Functions to get variables */
	//Get database username
	public function getDBUser() { return $this->db_user; }
	//Get database user password
	public function getDBPwd() { return $this->db_pwd; }
	//Get DB Prefix - needed for admin functionality
	public function getRawDBPrefix() { return $this->db_prefix; }

	//Get the current database name
	public function getDBName() {
		return $this->db_name;
	}
	//Create a different database bame
	public function getOtherDBName($cc) {
		$odb = $this->db_other;
		$odb.=strtolower($cc);
		return $odb;
	}
	//Get the DBRef 'assist_cmpcode_modref'
	public function getDBRef() {
		return $this->db_ref;
	}
	//Get the database name for the CASSIST database
	public function getCASSISTName() {
		return $this->db_cassist;
	}
	//Get the database name for the Master/Main database
	public function getMasterName() {
		return $this->db_master;
	}
	//Get the database name for the Blank database
	public function getBlankName() {
		return $this->db_blank;
	}

	//Get the database name for the Assist Help database
	public function getHelpName() {
		return $this->db_help;
	}

	//Get the database name for the Assist Helpdesk database - 16 September 2016 [TM]
	public function getHelpdeskName() {
		return $this->db_helpdesk;
	}
    //Get the database name for the Assist DTM database - 07 March 2017 [TM]
    public function getDTMName() {
        return $this->db_dtm;
    }

	public function getSageIntegName() {
		return $this->db_sage_integ;
	}

	//return the db connection resource
	public function getConn() {
//		$this->db_connect();
		return $this->db_resource;
	}
	public function getDBPrefix() {
		return $this->db_other;
	}
	public function getCmpCode() {
		return $this->cmpcode;
	}
	public function getModRef() {
		if(isset($this->db_modref) && strlen($this->db_modref)>0) {
			return strtoupper($this->db_modref);
		} else {
		return strtoupper($_SESSION['modref']);
		}
	}
	public function getModTitle() {
		return ($_SESSION['modtext']);
	}
	
	public function isClientDatabase() {
		return ($this->db_type == "client");
	}
	
	public function getUserID() { return $_SESSION['tid']; }
	
	public function isSupportUser() {
		if($_SESSION['tku'] == "support") 
			return true;
		else
			return false;
	}	
	



	/***
     * Get list of actual databases on the server
     */
    public function getListOfAvailableDatabases() {
        $dbs = array();
        $link = mysqli_connect("localhost",$this->db_user,$this->db_pwd);
        $db_list =  mysqli_query($link, "SHOW DATABASES");
        while ($row = mysqli_fetch_object($db_list)) {
            $dbs[] = $row->Database;
        }
        return $dbs;
    }

	/**
	 * Function used to output the amount of memory used by a script - useful for testing report generators that handle huge datasets
	 */
	public function print_mem() {
		/* USED MEMORY */
			// Currently used memory
			$mem_usage = memory_get_usage(FALSE);
			// Peak memory usage
			$mem_peak = memory_get_peak_usage(FALSE);
			echo 'The script is now using: <strong>'.round($mem_usage / 1024).'KB</strong> of memory.<br>';
			echo 'Peak usage: <strong>'.round($mem_peak / 1024).'KB</strong> of memory.<br><br>';
		/* ALLOCATED MEMORY */
			// Currently used memory
			$mem_usage = memory_get_usage(TRUE);
			// Peak memory usage
			$mem_peak = memory_get_peak_usage(TRUE);
			echo 'The script has <u>allocated</u>: <strong>'.round($mem_usage / 1024).'KB</strong> of memory.<br>';
			echo 'Peak <u>allocated</u> usage: <strong>'.round($mem_peak / 1024).'KB</strong> of memory.<br><br>';
	}


	/****
	 * generic table names
	 */
	public function getUserTableName() {
		return "assist_".strtolower($this->getCmpCode())."_timekeep";
	}


	protected function arrayToString($a,$i) {
		$s = array();
		$i++;
		foreach($a as $k=>$v) {
			if(is_array($v)) {
				$v = $this->arrayToString($v, $i);
			}
			$s[] = $k."=".$v."";
		}
		return "<Br />{".implode("; ",$s)."}";
	}


	//If there is an error -> send an email with the details
	//sendError(mysql_error(),$sql);
	protected function sendError($err,$sql) {
		$sqlerror = "<p>&nbsp;</p>
		<p><b>---ERROR DETAILS BEGIN---</b></p>
		<ul><li>Database error: ".$err."</li>
		<li>".$sql."</li>
		<li>".$this->db_name."</li>
		<li>Page: ".$_SERVER['PHP_SELF']."</li><li>Name: ".$_SERVER['SERVER_NAME']."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
		$sqlerror.="<p>--- SESSION DETAILS ---";
		foreach($_SESSION as $s => $v) {
//			$sqlerror.="<br />".$s." :: ".$v;
			if(is_array($v)) {
				$sqlerror.=$this->arrayToString($v, 0);
			} else {
				$sqlerror.=$v;					
			}
		}
		$sqlerror.="<p>".serialize($_SESSION);
		$sqlerror.="<p>--- REQUEST DETAILS ---";
		foreach($_REQUEST as $r => $v) {
//			$sqlerror.="<br />".$r." :: ".$v;
			if(is_array($v)) {
				$sqlerror.=$this->arrayToString($v, 0);
			} else {
				$sqlerror.=$v;					
			}
		}
		$sqlerror.="<p>".serialize($_REQUEST);
		$sqlerror.="<p>--- OTHER DETAILS ---";
			$sqlerror.="<br />HTTP USER AGENT :: ".$_SERVER['HTTP_USER_AGENT'];
		foreach($_SERVER as $r => $v) {
//			$sqlerror.="<br />".$r." :: ".$v;
			if(is_array($v)) {
				$sqlerror.=$this->arrayToString($v, 0);
			} else {
				$sqlerror.=$v;					
			}
		}
		$sqlerror.="<p>".serialize($_SERVER);
		$sqlerror.="<br />--- END ---</p>";
//		mail($this->db_error_to,"DB ".self::SITE_SRC." Error",$sqlerror,"From: ".$this->db_error_from.(strlen($this->db_error_cc)>0 ? "\r\nCC: ".$this->db_error_cc : "")."\r\nContent-type: text/html; charset=us-ascii");
		$src_path = explode("/",$_SERVER["REQUEST_URI"]);
		$path = ""; 
		if(count($src_path)<=2) { 
		} else { 
			$c = count($src_path)-2;
			for($i=1;$i<=$c;$i++) { $path.="../"; }
		}
		$path .= "logs/".date("Ymd_His")."_librarysql_error_log.html";
	        $file = fopen($path,"w");
       	 fwrite($file,$sqlerror."\n");
	        fclose($file);
//if($this->cc=="ait555d") { 
//echo $sqlerror; 
//echo "<pre>"; print_r($_SESSION); echo "</pre>";
//}
		die("<h1>Error</h1><p>Unfortunately Assist has encountered a fatal error and cannot continue.<br />An email has been sent to Action iT (Pty) Ltd notifying them of this error.</p>".$sqlerror);

	}

	private function autoLogoutDB($code="") {
		echo "<html><head><title>www.ignite4u.co.za</title>
				<link rel='stylesheet' href='/assist.css' type='text/css'>
				<script type='text/javascript'>
				<!--
				function delayer(){
					parent.location.href = 'http://".$_SERVER['SERVER_NAME']."/logout.php?r=".base64_encode($code)."';
				}
				//-->
				</script>
			</head>
			<body onLoad=\"setTimeout('delayer()', 3000)\">
			<h1>Expiration Notice</h1>
			<p>Your session has expired.  Please login again.</p>
			<p>You will be redirected to the Login page momentarily.  If your browser does not redirect you, <a href='https://".$_SERVER['SERVER_NAME']."/logout.php?r=".base64_encode($code)."' target=_parent>click here</a>.</p>
			<script type=text/javascript>

			</script>
			</body></html>";
	}

	function __destruct() {
		$this->db_close();
	}

}  //END assistDB class


?>
