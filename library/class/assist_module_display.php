<?php
ini_set('precision', 19);
/**
 * To manage the display of forms fields
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 * 2019 Annual Maintenance #AA-297
 */

class ASSIST_MODULE_DISPLAY extends ASSIST_ROOT {

	private $js = array();

	private $max_select_option_length = 75;

	private $common_extensions = array(
		'doc'	=> "doc",
		'docx'	=> "doc",
		'rtf'	=> "doc",
		'dot'	=> "doc",
		'doct'	=> "doc",
		'docm'	=> "doc",
		'odt'	=> "doc",
		'ods'	=> "xls",
		'xls'	=> "xls",
		'csv'	=> "xls",
		'xlsx'	=> "xls",
		'xlst'	=> "xls",
		'xlt'	=> "xls",
		'xltm'	=> "xls",
		'xlm'	=> "xls",
		'xlsm'	=> "xls",
		'xlab'	=> "xls",
		'xlsb'	=> "xls",
		'xlam'	=> "xls",
		'ppt'	=> "ppt",
		'pptx'	=> "ppt",
		'pptm'	=> "ppt",
		'pdf'	=> "pdf",
		'msg'	=> "email",
		'eml'	=> "email",
		'gwi'	=> "email",
		'htm'	=> "html",
		'html'	=> "html",
		'css'	=> "html",
		'js'	=> "html",
		'php'	=> "html",
		'mht'	=> "html",
		'jpg'	=> "image",
		'jpeg'	=> "image",
		'gif'	=> "image",
		'png'	=> "image",
		'bmp'	=> "image",
		'jfif'	=> "image",
		'svg'	=> "image",
		'tif'	=> "tiff",
		'tiff'	=> "tiff",
		'zip'	=> "zip",
		'rar'	=> "zip",
		'mpp'	=> "project",
		'txt'	=>"text",
		'ics'	=>"other",
		'xps'	=> "other",
		'exe'	=> "other",
		'pub'	=> "pub",
		'download'=>"other",
		'dat'=>"other",
		'mp4'=>"other",
		'mp3'=>"other",
	);
	private $file_icons = array(
		'doc'=>"word2020.png",
		'other'=>"document.gif",
		'text'=>"text2020.png",
		'email'=>"email2020.png",
		'xls'=>"excel2020.png",
		'html'=>"html2020.png",
		'image'=>"image2020.png",
		'pdf'=>"pdf2020.png",
		'ppt'=>"ppt2020.png",
		'tiff'=>"tiff2.gif",
		'zip'=>"zip2020.png",
		'project'=>"project2020.png",
		'pub'=>"pub2020.png",
		'onenote'=>"onenote2020.png",
		'access'=>"access2020.png",
		'info'=>"infopath2020.png"
	);

	const ACTIVE = 2;

    public function __construct($an=array(),$on=array()) {
    	parent::__construct($an,$on);
    }






	/**********************************
	 * FORM INPUT FIELD
	 */



    /**
     * (ECHO) Displays a small input text field: use for short codes
     * @param (String) val = any value to be displayed
     * @param (Array) prop = any additional properties with property name as key and value as element
     * @param (INT) max = max length of text to be allowed, default = 20
     * @param (INT) size = size of input field, default = 10
     *
     * @return (String) js = any related javascript that might need to be displayed on the calling page
     */
    public function drawSmallInputText($val="",$prop=array(),$max=20,$size=10) {
        $d = $this->getSmallInputText($val,$prop,$max,$size);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * (ECHO) Displays a medium input text field: use for reference numbers
     * @param (String) val = any value to be displayed
     * @param (Array) prop = any additional properties with property name as key and value as element
     * @param (INT) max = max length of text to be allowed, default = 100
     * @param (INT) size = size of input field, default = 50
     *
     * @return (String) js = any related javascript that might need to be displayed on the calling page
     *      */
    public function drawMediumInputText($val="",$prop=array(),$max=100,$size=50) {
        $d = $this->getMediumInputText($val,$prop,$max,$size);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a large input text field: use for names
     */
    public function drawLargeInputText($val="",$prop=array(),$max=200,$size=100) {
        $d = $this->getLargeInputText($val,$prop,$max,$size);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a date picker input
     */
    public function drawDatePicker($val="",$prop=array(),$options=array()) {
        $d = $this->getDatePicker($val,$prop,$options);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a standard text area: use for unlimited TEXT fields
     */
    public function drawTextArea($val="",$prop=array(),$rows=5,$cols=50) {
        $d = $this->getTextArea($val,$prop,$rows,$cols);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a text field with a text limiter associated: use for large text fields with a text limit
     */
    public function drawLimitedTextArea($val="",$prop=array(),$rows=5,$cols=50,$max=200) {
        $d = $this->getLimitedTextArea($val,$prop,$rows,$cols,$max);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a drop down select field = single value
     */
    public function drawSelect($val="",$prop=array(),$items) {
        $d = $this->getSelect($val,$prop,$items);
        echo $d['display'];
        return $d['js'];
    }

    /**
     * Displays a drop down select field = multiple value
     */
    public function drawMultipleSelect($val=array(),$prop=array(),$items,$show=0) {
        $d = $this->getMultipleSelect($val,$prop,$items,$show);
        echo $d['display'];
        return $d['js'];
    }

	/**
	 * Displays an autocomplete field - single input only
	 * @param STRING $val - current value of field
	 * @param ARRAY $prop - any background properties
	 * @param ARRAY $items - list items to be selected from
	 * @echo STRING display - onscreen display
	 * @return STRING js - JavaScript/JQuery to be triggered on client side
	 */
	public function drawAutoComplete($val = "", $prop = array(), $items) {
		if(isset($prop['raw_id'])) {
			$id = $prop['raw_id'];
			unset($prop['raw_id']);
		} else {
			$id = 0;
		}
		$d = $this->rawGetAutoComplete($val, $id, $items, $prop);
		echo $d['display'];
		return $d['js'];
	}

    /**
	 * Displays a yes/no form field
	 */
	public function drawBool($val="",$prop=array()){
		$d = $this->getBoolButton($val,$prop);
		echo $d['display'];
		return $d['js'];
	}

    /**
	 * Displays a yes/no form field
	 */
	public function drawBoolButton($val="",$prop=array()){
		$d = $this->getBoolButton($val,$prop);
		echo $d['display'];
		return $d['js'];
	}

    /**
	 * Displays a colour field
	 */
	public function drawColour($val="",$prop=array()){
		$d = $this->getColour($val,$prop);
		echo $d['display'];
		return $d['js'];
	}

    /**
	 * Displays a numerical rating field
	 */
	public function drawRating($val="",$prop=array()){
		$d = $this->getRating($val,$prop);
		echo $d['display'];
		return $d['js'];
	}

    /**
	 * Displays a numerical field which only permits numbers
	 */
	public function drawNumInputText($val="",$prop=array()){
		$d = $this->getNumInputText($val,$prop);
		echo $d['display'];
		return $d['js'];
	}

	/**
	 * Displays the attachment form
	 */
	public function drawAttachmentInput($val="",$prop=array()) {
		$d = $this->getAttachmentInput($val,$prop);
		echo $d['display'];
		return $d['js'];
	}




	/***************************************
	 * GET FUNCTIONS!!
	 */



	/**
	 * Function to return the display and js needed for calculations on a form
	 * @param $val = initial value (must match settings of $prop['yes'] or $prop['no'])
	 * @param (array) $prop = array of properties
	 * @param $prop['id'] = unique identifier (minimum requirement)
	 * @param $prop['name'] = form field name, defaults to id if not specified
	 * @param $prop['yes'] = form value for the yes button, defaults to 1 if not specified
	 * @param $prop['no'] = form value for the no button, defaults to 0 if not specified
	 * @param $prop['form'] = layout of the buttons, horizontal or vertical, default to horizontal if not specified
	 * @param $prop['extra'] = function name of additional function (on the calling page) to be called when a button is pressed, not required
	 * @param $prop['other'] = any additional attributes to be set for the hidden input field that holds the result of the button press (as string, not array)
	 *
	 * @return array('display'=>HTML, 'js'=>JQuery)
	 */

	public function getCalculationForm($val="",$prop=array()){
		$form_name = "form[name=".$prop['form_name']."]";
		unset($prop['form_name']);
		$extras = $prop['extra'];
		$calculation_type = $prop['extra'][0];
		$format = $prop['extra'][count($prop['extra'])-1];
		unset($prop['extra'][count($prop['extra'])-1]);
		unset($prop['extra'][0]);
		$fields = $prop['extra'];
		unset($prop['extra']);

		switch($format) {
			//case "CURRENCY":
		}

		$data = $this->getLabel(ASSIST_HELPER::format_number($val),$prop);
		//$data['display'].=implode("-",$extras)." :: ".$calculation_type." :: ".$format." :: ".implode("-",$fields);
		//if(!isset($this->js['bool_button']) || $this->js['bool_button']==FALSE){
			$data['js'] = "
				$('".$form_name." #".implode(", ".$form_name." #",$fields)."').keyup(function() {
					var total = 0;
					var err = false;
			";
		switch($calculation_type) {
			case "SUM":
				$data['js'].= "
					$('".$form_name." #".implode(", ".$form_name." #",$fields)."').each(function() {
						if($(this).is('label')) {
							var val = AssistString.calcPureNumberString($(this).html(),true);
						} else {
							var val = $(this).val();
						}
						if(val.length==0) {
							total+=0;
							val = 0;
							if($(this).is('label')) {
								$(this).html(val);
							} else {
								$(this).val(val);
							}
						} else if($(this).hasClass('required') || isNaN(parseInt(val))) {
							total = 0;
							err = true;
						} else if(!err) {
							total += parseFloat(val)*1;
						}
					});
					";
			break;
			case "DIFF":
				$collection_object = $fields[1];
				$removing_object = $fields[2];
				$data['js'].= "
				//verify all the values are ready
					$('".$form_name." #".implode(", ".$form_name." #",$fields)."').each(function() {
						if($(this).is('label')) {
							var val = AssistString.calcPureNumberString($(this).html(),true);
						} else {
							var val = $(this).val();
						}
						if(val.length==0) {
							if($(this).is('label')) {
								$(this).html('0');
							} else {
								$(this).val('0');
							}
						} else if($(this).hasClass('required') || isNaN(parseFloat(val))) {
							total = 0;
							err = true;
						}
					});
					//perform calculation
					if(!err) {
						var $"."collection_object = $('".$form_name." #".$collection_object."');
						if($"."collection_object.is('label')) {
							var collection = parseFloat(AssistString.calcPureNumberString($"."collection_object.html(),true));
						} else {
							var collection = parseFloat($"."collection_object.val());
						}
						var $"."removing_object = $('".$form_name." #".$removing_object."');
						if($"."removing_object.is('label')) {
							var removing = parseFloat(AssistString.calcPureNumberString($"."removing_object.html(),true));
						} else {
							var removing = parseFloat($"."removing_object.val());
						}
						total = (collection*1) - (removing*1);
					}
				";
			break;
		}
		$data['js'].= "
				if(!err) {
					//alert(total);
					$('".$form_name." #".$prop['id']."').html(AssistString.formatNumber(total.toFixed(2)));
					if($('".$form_name." #".$prop['id']."').hasClass('calculate_next')) {
						$('".$form_name." #".$prop['id']."').trigger('keyup');
					}
				}
			});
			";
    	return $data;
    }


	public function getLabel($val="",$prop=array()) {
		$data = array('display'=>"",'js'=>"");
		$data['display']="<label ".$this->convertPropToString($prop)." >".ASSIST_HELPER::decode($val)."</label>";
		return $data;
	}
    public function getSmallInputText($val="",$prop=array(),$max=20,$size=10) {
		$max = $max>0 ? $max : 20;
		$size = $size>0 ? $size : 10;
		return $this->getInputText($val, $prop, $max, $size);
	}
    public function getMediumInputText($val="",$prop=array(),$max=100,$size=50) {
        $max = $max>0 ? $max : 100;
        $size = $size>0 ? $size : 50;
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getLargeInputText($val="",$prop=array(),$max=200,$size=100) {
        $max = $max>0 ? $max : 200;
        $size = $size>0 ? $size : 100;
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getColour($val="",$prop=array(),$max=20,$size=10) {
        $max = $max>0 ? $max : 20;
        $size = $size>0 ? $size : 10;
		$prop = array("colour"=>"true");
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getRating($val="",$prop=array(),$max=20,$size=10) {
        $max = $max>0 ? $max : 20;
        $size = $size>0 ? $size : 10;
        return $this->getInputText($val, $prop, $max, $size);
    }
    public function getNumInputText($val="",$prop=array(),$max=20,$size=10) {
		//function to hack around PHP's inability to manage large floats [JC] AA-690 3 Sep 2021
		$val = $this->processingBigNumbersForDisplay($val);

        $max = $max>0 ? $max : 20;
        $size = $size>0 ? $size : 10;
		if(isset($prop['warn'])) {
			$display_warning = $prop['warn'];
			unset($prop['warn']);
		} else {
			$display_warning = true;
		}
		$prop['class']= (isset($prop['class']) ? $prop['class'] : "").($display_warning ? " display_warning" : "")." number-only";
		$js_alt = isset($prop['extra']) ? $prop['extra'] : "";
        $data = $this->getInputText($val, $prop, $max, $size);
		if(!isset($this->js['num']) || $this->js['num']!==true) {
			$this->js['num'] = true;
			$data['js'] = "
			$('.number-only').blur(function() {
				$(this).removeClass('required');
				var v = $(this).val();
				var num_arr = ['0','1','2','3','4','5','6','7','8','9','.','-'];
				var valid8 = true;
				for ( var i = 0; i < v.length; i++ ) {
					if($.inArray(v.charAt(i),num_arr)<0) {
						valid8=false;
					}
				}
				if(!valid8) {
					$(this).addClass('required');
					".(strlen($js_alt)>0 ? $js_alt."($(this));" : "")."
				}
			}).each(function() {
				if($(this).hasClass('display_warning')) {
					$(this).parent().append('<div class=\'orange i float\' style=\'width: 50%\'><small>Note: Only numbers (0-9), period (.) and dash (-) permitted</small></div>');
				}
			});
			";
		}
		return $data;
    }
    public function getDatePicker($val="",$prop=array(),$options=array()) {
    	//echo "get date picker";
        $data = array('display'=>"",'js'=>"");
		if(isset($prop['custom_datepicker'])) {
			$custom_datepicker = $prop['custom_datepicker'];
			unset($prop['custom_datepicker']);
		} else {
        	$custom_datepicker ="datepicker";
		}
    	$prop['class'] = (isset($prop['class']) ? $prop['class'] : "")." ".$custom_datepicker;
    	$prop['autocomplete'] = "off";
    	if(isset($prop['onSelect'])) {
    		$on_select_js = $prop['onSelect'];
    		unset($prop['onSelect']);
    	} else {
    		$on_select_js = false;
    	}
		$data = $this->getInputText($val, $prop, 12, 12);
		if(!isset($this->js[$custom_datepicker]) || $this->js[$custom_datepicker]==false) {
			$data['js'] = "";
			//default datepicker options
                    $options['showOn'] = !isset($options['showOn']) ? "'both'" : $options['showOn'];
                    $options['buttonImage'] = !isset($options['buttonImage']) ? "'/library/jquery/css/calendar.gif'" : $options['buttonImage'];
                    $options['buttonImageOnly'] = !isset($options['buttonImageOnly']) ? "true" : $options['buttonImageOnly'];
                    if($options['buttonImage']===false) {
                    	unset($options['buttonImage']);
                    	unset($options['buttonImageOnly']);
                    	unset($options['showOn']);
					}
                    $options['dateFormat'] = !isset($options['dateFormat']) ? "'dd-M-yy'" : $options['dateFormat'];
                    $options['changeMonth'] = !isset($options['changeMonth']) ? "true" : $options['changeMonth'];
                    $options['changeYear'] = !isset($options['changeYear']) ? "true" : $options['changeYear'];
                    $options['minDate'] = !isset($options['minDate']) ? "null" : $options['minDate'];
                    $options['maxDate'] = !isset($options['maxDate']) ? "null" : $options['maxDate'];
			if(!isset($options['onSelect']) && $on_select_js == true) {
                    $options['onSelect'] = "function() {
                    	setPostDateSelectionFocus($(this));
                    }";
					$data['js'].="
function setPostDateSelectionFocus($"."me) {
	var selector = 'input:text, select, textarea, button';
	var $"."selectables = $"."me.closest(\"table\").find(selector);
	var found = false;
	var focus = false;
	$"."selectables.each(function() {
		if(!found) {
			if($(this).prop(\"id\")==$"."me.prop(\"id\")) {
				found = true;
				console.log($"."me.prop(\"id\"));
			}
		} else if(!focus){
			$(this).focus();
			if($(this).hasClass(\"datepicker\")) {
				$(this).datepicker(\"show\");
			}
			focus = true;
			console.log(\"focused on:\"+$(this).prop(\"id\"));
		}
	});

}					";
            }
			$data['js'].= "$('.".$custom_datepicker."').datepicker({".$this->convertPropToJS($options)."});";
		//echo $data['js'];
			$this->js[$custom_datepicker] = true;
		}
        return $data;
    }
    public function getTextArea($val="",$prop=array(),$rows=5,$cols=50) {
        $data = array('display'=>"",'js'=>"");
		$data['display'] = "<textarea".$this->convertPropToString($prop)." rows=".$rows." cols=".$cols.">".ASSIST_HELPER::decode($val)."</textarea>";
        return $data;
    }
    	/**
		 * JS FINETUNING TO BE DONE!!!
		 */
    public function getLimitedTextArea($val="",$prop=array(),$rows=5,$cols=50,$max=200) {
        $data = array('display'=>"",'js'=>"");
		$prop['class'] = (isset($prop['class']) ? $prop['class'] : "")." txt_char";
		if(isset($prop['rows']) && is_numeric($prop['rows']) && $prop['rows']>0) {
			$rows = $prop['rows'];
		}
		if(isset($prop['cols']) && is_numeric($prop['cols']) && $prop['cols']>0) {
			$cols = $prop['cols'];
		}
		if(isset($prop['max']) && is_numeric($prop['max']) && $prop['max']>0) {
			$max = $prop['max'];
		}
		$data['display'] = "<textarea ".$this->convertPropToString($prop)." rows=".$rows." cols=".$cols.">".ASSIST_HELPER::decode($val)."</textarea><br />
		<label id='lbl_".$prop['id']."' for='".$prop['id']."' max=".$max."><span class=spn_char>".$max."</span> characters remaining</label>";
		if(!isset($this->js['limitedtext']) || $this->js['limitedtext']==FALSE){
			$data['js'] = "$('textarea.txt_char').keyup(function(){
				var v = $(this).val();
				var l = v.length;
				var i = $(this).prop('id');
				var mxStr = $('#lbl_'+i).attr('max');
				var mx = parseInt(mxStr);
				window.rem = mx - l;
				$('#lbl_'+i).removeClass('idelete').children('span.spn_char').html(rem).removeClass('iinform').removeClass('isubmit');

				if(rem>10) {
					$('#lbl_'+i).children('span.spn_char').addClass('isubmit');
				} else if (rem <= 10 && rem > 0) {
					$('#lbl_'+i).children('span.spn_char').addClass('iinform');
				} else {
					$('#lbl_'+i).addClass('idelete');
				}

			});
			$('textarea.txt_char').blur(function(){
				$(this).trigger('keyup');
				var i = $(this).prop('id');
				var rem = $('#lbl_'+i).children('span.spn_char').html();
				if(rem < 0){
					alert('Your input is too long, and not all of it will be saved. Please stick to the character limit.');
				}
			});";
			$this->js['limitedtext'] = true;
		}
        return $data;
    }
    public function getSelect($val="",$prop=array(),$items) {
    	//print_r($prop); //print_r($items);
    	$required = false;
    	if(isset($prop['req'])) {
    		$required = ($prop['req']==true);
    	} elseif(isset($prop['required'])) {
    		$required = ($prop['required']==true);
    	}
    	$allow_unspecified = true;
    	if($required==true) {
    		$allow_unspecified = false;
    	} elseif(isset($prop['unspecified'])) {
    		$allow_unspecified = ($prop['unspecified']==true);
    	} elseif(isset($prop['allow_unspecified'])) {
    		$allow_unspecified = ($prop['allow_unspecified']==true);
    	}
    	$option_parent_association = isset($prop['list_num']) ? $prop['list_num'] : array();
    	$extra_padding = isset($prop['padding']) ? $prop['padding'] : false;
    	unset($prop['padding']);
    	if($extra_padding==true) {
    		$prop['style'] = "padding:5px;";
    	}
		if(isset($prop['alternative_display'])) {
			$alternative_display = $prop['alternative_display'];
			unset($prop['alternative_display']);
		} else {
			$alternative_display = false;
		}

        $data = array('display'=>"",'js'=>"");
		$data['display'] = "<select ".$this->convertPropToString($prop).">".($required || !$allow_unspecified ? "<option value=X>--- SELECT ---</option>" : "<option value=0>".ASSIST_HELPER::UNSPECIFIED."</option>");
		foreach($items as $key => $i) {
			if(is_array($i)) {
				$opt_prop = isset($i['prop']) ? $i['prop'] : array();
				$i = $i['value'];
			} else {
				$opt_prop = array();
			}
			$s = $i;
			if(strlen($s)>$this->max_select_option_length) {
				//echo "<h3>:".$alternative_display.":</h3>";
				if($alternative_display==true) {
					//echo "<P>".$s." => ";
					$s = substr($s,0,($this->max_select_option_length*0.20)).".....".substr($s,($this->max_select_option_length*-0.79));
					//echo $s;
				} else {
					//echo "<P>NOT ALTERNATIVE";
					$s = substr($s,0,$this->max_select_option_length-3)."...";
				}
			} else {
				//echo "<P>TOO SHORT";
			}
			$data['display'].="<option value='".$key."' ".($key==$val ? "selected" : "")." full='".ASSIST_HELPER::code($i)."' list_num=".(isset($option_parent_association[$key]) ? $option_parent_association[$key] : "0")." ".$this->convertPropToString($opt_prop)." >".$s."</option>";
		}
		$data['display'].="</select>
		<div id=div_".$prop['id']." class='display_me ui-state-ok' style='width: 500px;margin: 5px;'></div>";
		if(!isset($this->js['select']) || $this->js['select']!==true) {
			$this->js['select']=true;
			$data['js'] = "
			$('div.display_me').hide();
			$('select').change(function(){
				var i = $(this).prop('id');
				var $"."opt = $(this).find(':selected');
				var txt = '';
				var alt = '';
				//console.log('inside change 1 '+i+' = '+$(this).val());
				$"."opt.each(function() {
					if($(this).val()==0 || $(this).val()=='X') {

					} else {
						if($(this).attr('full')!=null) {
							alt = AssistString.stripslashes($(this).attr('full'));
							if(alt==$(this).text() || alt==AssistString.code($(this).text())) {
							} else {
								if(alt.length>0 && txt.length>0) {
									txt+='<br />';
								}
								txt+='+ '+alt;
							}
						}
					}
				});
				if(txt.length>0) {
					$('#div_'+i).show().html('<p>'+txt+'</p>');
				} else {
					$('#div_'+i).html('').hide();
				}
				//console.log('inside change 2 '+i+' = '+$(this).val());
			});";
		}

        return $data;
    }
    public function getMultipleSelect($val=array(),$prop=array(),$items,$show=0) {
        $data = array('display'=>"",'js'=>"");
    	$required = isset($prop['req']) ? $prop['req']==1 : false;
    	$allow_unspecified = isset($prop['unspecified']) ? $prop['unspecified'] : true;
		$prop['class'] = (isset($prop['class']) ? $prop['class'] : "")." multi_select";
		if($show==0) {
			$show = count($items)>7 ? 8 : count($items)+1;
		}
		if(!isset($prop['name'])) { $prop['name'] = $prop['id']; }
		$data['display'] = "<select ".$this->convertPropToString($prop)." multiple size=".($show>0 ? $show : 10).">".($required || !$allow_unspecified ? "" : "<option value=0>".ASSIST_HELPER::UNSPECIFIED."</option>");
		foreach($items as $key => $i) {
			$data['display'].="<option value='".$key."' ".(in_array($key,$val) ? "selected" : "").">".$i."</option>";
		}
		$data['display'].="</select>";
		if(!isset($this->js['multi_select']) || $this->js['multi_select']==FALSE){
			$data['js'] = "
				$('select.multi_select').change(function() {
					var $"."me = $(this).find('option:selected:first');
					if($"."me.prop('value')=='0') {
						$(this).find('option:selected:gt(0)').prop('selected',false);
					}
				});
			";
		}
        return $data;
    }

	public function getAutoComplete($val = "", $prop = array(), $items = array()) {
		if(isset($prop['raw_id'])) {
			$id = $prop['raw_id'];
			unset($prop['raw_id']);
		} else {
			$id = 0;
		}
		return $this->rawGetAutoComplete($val, $id, $items, $prop);
	}
	public function rawGetAutoComplete($val = "", $id = "", $items = array(), $prop = array(), $rows = 3, $cols = 100) {
		$data = array('display' => "", 'js' => "");
		$hidden_prop = array('class'=>"is-auto-raw");
		$display_info_message_on_load = isset($prop['display_info_message_on_load']) ? $prop['display_info_message_on_load'] : true;
		if(isset($prop['id'])) {
			$hidden_prop['id'] = $prop['id'];
			$prop['id'] = "auto_".$prop['id'];
		} elseif(isset($prop['name'])) {
			$hidden_prop['id'] = $prop['name'];
			$prop['id'] = "auto_".$prop['name'];
		}
		if(isset($prop['name'])) {
			$hidden_prop['name'] = $prop['name'];
			$prop['name'] = "auto_".$prop['name'];
		} else {
			$hidden_prop['name'] = $prop['id'];
			$prop['name'] = "auto_".$prop['id'];
		}
		if(isset($prop['req'])) {
			$hidden_prop['class'] = "is-auto-raw-".($prop['req']==true?"req":"no");
			unset($prop['req']);
		}
		$data['display'] = "<textarea ".$this->convertPropToString($prop)." rows=".$rows." cols=".$cols.">".ASSIST_HELPER::decode($val)."</textarea><div id='div_".$hidden_prop['id']."' style='width:500px' class='autocomplete-notice-div'><label for='".$prop['name']."' id='lbl_".$prop['name']."'></label></div>
<input ".$this->convertPropToString($hidden_prop)." type='hidden' value='$id' />";
		$var_name = "available_items_".$hidden_prop['id']."";
		$raw_name = $hidden_prop['id'];
		$div_name = "div_".$hidden_prop['id'];
		$data['js'] = "
			var $var_name = [";
						$list = array();
						foreach($items as $id => $label) {
							$list[] = "
							{id:'".$id."',label:'".(strpos($label, "'") !== false || strpos($label, '"') !== false ? ASSIST_HELPER::code($label) : $label)."'}
							";
						}
						$data['js'].=implode(",",$list)."];
						for(i in $var_name ) {
							var x = ".$var_name."[i];
							".$var_name."[i].id = AssistString.decode(x.id);
							".$var_name."[i].label = AssistString.decode(x.label);
						}
			
			$('#".$prop['id']."').autocomplete({
				minLength: 2,
				source: ".$var_name.",
				select: function(event,ui) {
					$(this).val(AssistString.decode(ui.item.label));
					$('#".$raw_name."').val(ui.item.id);
					".$prop['name']."_MESSAGES('ok');
					return false;
				},
				change: function(event,ui) {
					if(!ui.item) {
						$('#".$raw_name."').val('');
						if($('#auto_".$raw_name."').val().length==0) {
							".$prop['name']."_MESSAGES('info');
						} else {
							".$prop['name']."_MESSAGES('error');
						}
					} else {
						".$prop['name']."_MESSAGES('ok');
					}
				}
			});
			$('#".$div_name."').width($('#auto_".$raw_name."').width());
			if($('#".$raw_name."').val().length==0) {
			".($display_info_message_on_load?"	".$prop['name']."_MESSAGES('info');":"//setting was sent through to not display the info message on page load")."
			} else {
				".$prop['name']."_MESSAGES('ok');
			}
			function ".$prop['name']."_MESSAGES(msg) {
				switch(msg) {
				case 'error':
						$('#lbl_".$prop['name']."').html(AssistHelper.getHTMLResult('error','Invalid entry.  Please select from the available options (start typing a few letters in the text box and then select from the options that appear below).',''));
						break;
				case 'ok':
						$('#auto_".$raw_name."').removeClass('required');
						$('#lbl_".$prop['name']."').html(AssistHelper.getHTMLResult('ok','All okay.  Valid entry.',''));
						break;
				case 'info':
				default:
						$('#lbl_".$prop['name']."').html(AssistHelper.getHTMLResult('info','This is an autocomplete field.  Please select from the available options by typing a few letters in the text box and then selecting from the options that appear below.  Please note that typing the full text will not result in a successful entry - the correct option must be selected from the list of available options.',''));
				}
			}
		";

		return $data;
	}
	/**
	 * Function to return the display and js needed for the Yes/No buttons
	 * @param $val = initial value (must match settings of $prop['yes'] or $prop['no'])
	 * @param (array) $prop = array of properties
	 * @param $prop['id'] = unique identifier (minimum requirement)
	 * @param $prop['name'] = form field name, defaults to id if not specified
	 * @param $prop['yes'] = form value for the yes button, defaults to 1 if not specified
	 * @param $prop['no'] = form value for the no button, defaults to 0 if not specified
	 * @param $prop['form'] = layout of the buttons, horizontal or vertical, default to horizontal if not specified
	 * @param $prop['extra'] = function name of additional function (on the calling page) to be called when a button is pressed, not required
	 * @param $prop['other'] = any additional attributes to be set for the hidden input field that holds the result of the button press (as string, not array)
	 *
	 * @return array('display'=>HTML, 'js'=>JQuery)
	 */
//$val=1; $prop=array('form'=>"horizontal",'id'=>"notify");
    public function getBoolButton($val="",$prop=array()){
    	//echo "<br /> gBB VAL:".$val." ARR: "; print_r($prop);
    	if(!isset($prop['form'])) { $prop['form'] = "horizontal"; } else { $prop['form'] = strtolower($prop['form']); }
    	if(isset($prop['class']) && strlen($prop['class'])>0) { $class = $prop['class']; unset($prop['class']); } else { $class = ""; }
        $data = array('display'=>"",'js'=>"");
		$prefix = "";
		$midfix = "";
		$postfix = "";
		if($prop['form']=="vertical"){
			$prefix = "<div style='text-align: center; padding-left: 10px; padding-right: 10px;'>";
			$midfix = "<br />";
			$postfix = "</div>";
		} else {
			$prefix = "<div>";
			$midfix = "";
			$postfix = "</div>";
		}
		$prop['yes'] = (isset($prop['yes']) ? $prop['yes'] : "1");
		$prop['no'] = (isset($prop['no']) ? $prop['no'] : "0");
		$data['display'] = $prefix."
			<input type=hidden name=".(isset($prop['name']) ? $prop['name'] : $prop['id'])." id=".$prop['id']." value='".$val."' class='$class' ".(isset($prop['other']) ? $prop['other'] : "")." />
			<button class='bool_btn btn_yes' val=".$prop['yes']." id=".$prop['id']."_yes button_status=".($prop['yes']==$val?"active":"no")." ".(isset($prop['button_extra']) ? $prop['button_extra'] : "").">Yes</button> ".$midfix."
			<button class='bool_btn btn_no' val=".$prop['no']." id=".$prop['id']."_no button_status=".($prop['no']==$val?"active":"no")." ".(isset($prop['button_extra']) ? $prop['button_extra'] : "").">No</button>
		".$postfix."";
		if(isset($prop['small_size']) && $prop['small_size']==true) {
			//custom to each bool button drawn
			$data['js'].="
			$('button#".$prop['id']."_yes, button#".$prop['id']."_no').css({
				'font-size':'75%',
				'padding':'1px'
			});

			";
		}
		if(!isset($this->js['bool_button']) || $this->js['bool_button']==FALSE){
			$data['js'].= "
			$(\"button.btn_yes\").button({
				icons: {
					primary: \"ui-icon-check\"
				}
			}).each(function() {
				if($(this).attr(\"button_status\")==\"active\") {
					$(this).addClass(\"ui-state-ok\");
				}
			})".($prop['form']=="vertical" ? ".css('margin-bottom','2px')":"").";
			$(\"button.btn_no\").button({
				icons: {
					primary: \"ui-icon-close\"
				}
			}).each(function() {
				if($(this).attr(\"button_status\")==\"active\") {
					$(this).addClass(\"ui-state-error\");
				}
			})".($prop['form']=="vertical" ? ".css('margin-top','2px')":"").";



	function selectNextTabbableOrFocusable(selector){
		var selectables = $(selector);
		var current = $(':focus');
		var nextIndex = 0;
		var movementIndex = 1;
		if(current.hasClass('btn_yes')) { movementIndex++; }
		if(current.length === 1){
			var currentIndex = selectables.index(current);
			if(currentIndex + movementIndex < selectables.length){
				nextIndex = currentIndex + movementIndex;
			}
		}

		selectables.eq(nextIndex).focus();
	}


			$('button.bool_btn').addClass('button_class').click(function(event) {
				event.preventDefault();
				var i = $(this).prop('id');
				var v = $(this).attr('val');
				var f = AssistString.explode('_',i);
				var act = AssistArray.array_pop(f);
				var fld = AssistString.implode('_',f);
				$('#'+fld).val(v);
				if(act=='yes'){
					$(this).addClass('ui-state-ok');
					$('#'+fld+'_no').removeClass('ui-state-error');
				} else {
					$(this).addClass('ui-state-error');
					$('#'+fld+'_yes').removeClass('ui-state-ok');
				}
				//$(this).blur();
				".(isset($prop['select_next']) && $prop['select_next']==false ? "":"selectNextTabbableOrFocusable(':focusable');")."
				".(isset($prop['extra']) ? $prop['extra']."($(this));" : "")."
				".(isset($prop['extra_act']) ? $prop['extra_act']."($(this),act);" : "")."
				return false;
			});
			";
			$this->js['bool_button']=true;
		}
    	return $data;
    }



	/** Original function, not edited as assumed to be working in other modules but not working in SDBP6 [JC - 24 Sep 2018]
	 * 			New function added below for SDBP6
	 */
	public function getAttachmentInput($val="",$prop=array()) {
	    $allow_multiple = !isset($prop['allow_multiple']) ? true : $prop['allow_multiple'];
        $has_attachments = !isset($prop['has_attachments']) ? 0 : $prop['has_attachments'];
        $action = !isset($prop['action']) ? " " : $prop['action'];
        $page_direct = !isset($prop['page_direct']) ? "dialog" : $prop['page_direct'];
		$data = array('display'=>"<div id=div_attach_container style='width:350px;'>",'js'=>"");
		if((!is_array($val) && strlen($val)>0) || (is_array($val) && count($val)>0)) {
			$data['display'].=$this->getAttachForDisplay($val,
															(isset($prop['can_edit']) ? $prop['can_edit'] : false),
															(isset($prop['object_type']) ? $prop['object_type'] : "X"),
															(isset($prop['object_id']) ? $prop['object_id'] : "0"),
															(isset($prop['buttons']) ? $prop['buttons'] : true),
															(isset($prop['page_activity']) ? $prop['page_activity'] : "VIEW")
														);
		}
		$class = isset($prop['class']) ? " class='".$prop['class']."' " : "";
		//give the input an ID and tell it that it's required
		$id = ($class == " class='am_required1' " ? "id='firstdoc_input' req=1" : "");
		$class =
		$data['display'].= "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" ".$class." ".$id." style='margin-bottom: 5px;' />".($allow_multiple?"<button class=cancel_attach>".$this->getActivityName("reset")."</button>
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			".ASSIST_HELPER::getDisplayResult(array("info","No more than 10 files may be uploaded at once.<br />Combined file size cannot exceed 100MB.")):"</div>")."
			<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #000000;color:#000000;\"></iframe>
			<input type=hidden name=has_attachments id=has_attachments value=1 class='no-check'/>
			<input type=hidden name=action id='action' value='".$action."' class='no-check'/>
			<input type=hidden name=page_direct id='page_direct' value='".$page_direct."' class='no-check'/>
		";
		$data['js'].="
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			var attachment_count = 1;
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<button class=cancel_attach>".$this->getActivityName("reset")."</button>')
							.button({icons:{primary:'ui-icon-cancel'}})
							.addClass('button_class')
							.bind('click',function(e) { e.preventDefault(); handler($('#firstdoc input:button').index(this)); });
				});
				attachment_count++;
				if(attachment_count==10) { $('#attachlink').hide(); }
			});
			$('button.cancel_attach').button({icons:{primary:'ui-icon-cancel'}}).addClass('button_class').click(function(e) {
				e.preventDefault();
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		".($_SESSION['cc']=="testcon2" ? "$('#file_upload_target').css({'width':'250px','height':'250px','border':'1px dashed #009900'});" : "");
		$data['display'].="</div>";
		return $data;
	}

	public function getAttachmentInputIKPI2($val="",$prop=array()) {
		return $this->getAttachmentInputSDBP6($val,$prop);
	}
	/** SDBP6 version of module above - JC [24 Sep 2018] **/
	public function getAttachmentInputSDBP6($val="",$prop=array()) {
	    $allow_multiple = !isset($prop['allow_multiple']) ? true : $prop['allow_multiple'];
		$data = array('display'=>"<div id=div_attach_container style='width:350px;'>",'js'=>"");
		if((!is_array($val) && strlen($val)>0) || (is_array($val) && count($val)>0)) {
			$attach_display=$this->getAttachForDisplay($val,
														(isset($prop['can_edit']) ? $prop['can_edit'] : false),
														(isset($prop['object_type']) ? $prop['object_type'] : "X"),
														(isset($prop['object_id']) ? $prop['object_id'] : "0"),
														(isset($prop['buttons']) ? $prop['buttons'] : true),
														(isset($prop['page_activity']) ? $prop['page_activity'] : "VIEW")
													);
			$data['display'].=$attach_display;
		}
		$class = isset($prop['class']) ? " class='".$prop['class']."' " : "";
		//give the input an ID and tell it that it's required
		$id = ($class == " class='am_required1' " ? "id='firstdoc_input' req=1" : "");
		$class =
		$data['display'].= "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" ".$class." ".$id." style='margin-bottom: 5px;' />".($allow_multiple?"<button class=cancel_attach>".$this->getActivityName("reset")."</button>
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			".ASSIST_HELPER::getDisplayResult(array("info","No more than 10 files may be uploaded at once.<br />Combined file size cannot exceed 100MB.")):"</div>")."
			<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #000000;color:#000000;\"></iframe>
			<input type=hidden name=has_attachments id=has_attachments value=1 />
			<input type=hidden name=action value='".$prop['action']."' />
			<input type=hidden name=page_direct value='".$prop['page_direct']."' />
		";
		$data['js'].="
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			var attachment_count = 1;
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<button class=cancel_attach>".$this->getActivityName("reset")."</button>')
							.button({icons:{primary:'ui-icon-cancel'}})
							.addClass('button_class')
							.bind('click',function(e) { e.preventDefault(); handler($('#firstdoc input:button').index(this)); });
				});
				attachment_count++;
				if(attachment_count==10) { $('#attachlink').hide(); }
			});
			$('button.cancel_attach').button({icons:{primary:'ui-icon-cancel'}}).addClass('button_class').click(function(e) {
				e.preventDefault();
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		".($_SESSION['cc']=="testcon2" ? "$('#file_upload_target').css({'width':'250px','height':'250px','border':'1px dashed #009900'});" : "");
		$data['display'].="</div>";
		return $data;
	}


	public function getAttachmentInputOnly($val="",$prop=array()) {
	    $allow_multiple = !isset($prop['allow_multiple']) ? true : $prop['allow_multiple'];
		$data = array('display'=>"<div id=div_attach_container style='width:350px;'>",'js'=>"");
		$class = isset($prop['class']) ? " class='".$prop['class']."' " : "";
		//give the input an ID and tell it that it's required
		$id = ($class == " class='am_required1' " ? "id='firstdoc_input' req=1" : "");
		$class =
		$data['display'].= "
		<div id=\"firstdoc\">
				<input type=\"file\" name=\"attachments[]\" size=\"30\" ".$class." ".$id." style='margin-bottom: 5px;' />".($allow_multiple?"<button class=cancel_attach>".$this->getActivityName("reset")."</button>
			</div>
			<a href=\"javascript:void(0)\" id=\"attachlink\">Attach another file</a>
			".ASSIST_HELPER::getDisplayResult(array("warn","No more than 10 files may be uploaded at once.<br />Combined file size cannot exceed 100MB.")):"</div>")."
			<iframe id=\"file_upload_target\" name=\"file_upload_target\" src=\"\" style=\"width:0px;height:0px;border:0px solid #ffffff;color:#000000;\"></iframe>
			<input type=hidden name=has_attachments id=has_attachments value=1 />
			<input type=hidden name=action value='".$prop['action']."' />
			<input type=hidden name=page_direct value='".$prop['page_direct']."' />
		";
		$data['js'].="
			var attachment = '<input type=\"file\" name=attachments[] size=\"30\"  style=\"margin-bottom: 5px;\" />';
			var attachment_count = 1;
			$('#attachlink').click(function(){
				$('#firstdoc').append('<br/>'+attachment+' ').append(function() {
					return $('<button class=cancel_attach>".$this->getActivityName("reset")."</button>')
							.button({icons:{primary:'ui-icon-cancel'}})
							.addClass('button_class')
							.bind('click',function(e) { e.preventDefault(); handler($('#firstdoc input:button').index(this)); });
				});
				attachment_count++;
				if(attachment_count==10) { $('#attachlink').hide(); }
			});
			$('button.cancel_attach').button({icons:{primary:'ui-icon-cancel'}}).addClass('button_class').click(function(e) {
				e.preventDefault();
				handler($('#firstdoc input:button').index(this));
			});
			function handler(e) {
				$('#firstdoc input:file').eq(e).before(attachment).remove();
			}
		".($_SESSION['cc']=="testcon2" ? "$('#file_upload_target').css({'width':'250px','height':'250px','border':'1px dashed #009900'});" : "");
		$data['display'].="</div>";
		return $data;
	}



/*****************
 * PRIVATE functions - this class only
 */
    private function getInputText($val,$prop,$max,$size){
    	//echo "get input text val:*".$val."*";
        $data = array('display'=>"",'js'=>"");
    	if(isset($prop['colour']) && $prop['colour'] == "true"){
    		//echo "color";
	        $data['display']="<input style=\"border:1px solid grey\" class=\"color {notext:true}\" />";
    	}else{
    		//echo "else";
    		if(isset($prop['type'])) {
    			$type = $prop['type'];
				unset($prop['type']);
    		} else {
    			$type = "text";
    		}
	        $data['display']="<input type=".$type." value=\"".ASSIST_HELPER::decode($val)."\" maxlength='".$max."' size='".$size."' ";
	        //$data['display']="<input type=text value=\"".ASSIST_HELPER::decode($val)."\"  size='".$size."' ";
	        $data['display'].=$this->convertPropToString($prop)." />";
			//echo ASSIST_HELPER::code($data['display']);
    	}

        //foreach($prop as $key=>$v){
        //    $data['display'].= " ".$key."='".$v."'";
        //}
        return $data;
    }
	private function convertPropToString($prop){
		$options = "";
		foreach($prop as $key=>$v){
			$options.=" ".$key."=\"".$v."\"";
		}
		return $options;
	}
	private function convertPropToJS($prop){
		$options = array();
		foreach($prop as $key=>$v){
			$options[] = $key.": ".$v."";
		}
		return implode(",",$options);
	}




















	/**********************************
	 * DATA DISPLAY FORMATTING
	 */

	/***
	 * (ECHO) DATE formatting
	 * @param (String) d = date in non-unix format
	 * @param (Bool*) include_time = include time in output true/false*
	 */
	public function drawDateForDisplay($d,$include_time=false) {
		echo $this->getDateForDisplay($d,$include_time);
	}
	/***
	 * (RETURN) DATE formatting
	 * @param (String) d = date in non-unix format
	 * @param (Bool*) include_time = include time in output true/false*
	 */
	public function getDateForDisplay($d,$include_time=false) {
		if($include_time) { $format = "d-M-Y H:i"; } else { $format = "d-M-Y"; }
		if(is_numeric($d)) {
			return date($format,$d);
		} elseif(strtotime($d)>0) {
			return date($format,strtotime($d));
		} else {
			return ASSIST_HELPER::UNSPECIFIED;
		}
	}

	/**
	 * (ECHO) Currency formatting
	 * @param (Float) v = amount to be displayed
	 * @param (String*) symbol = prefix; default = "R"
	 */
	public function drawCurrencyForDisplay($v,$symbol = "R") {
		echo $this->getCurrencyForDisplay($v,$symbol = "R");
	}
	/**
	 * (RETURN) Currency formatting
	 * @param (Float) v = amount to be displayed
	 * @param (String*) symbol = prefix; default = "R"
	 */
	public function getCurrencyForDisplay($v,$symbol="") {
		$decimal = ".";
		$thou_sep = ",";
		$v*=1;
		return ($symbol!==false && strlen($symbol)>0 ? $symbol." " : "").number_format($v,2,$decimal,$thou_sep);
	}

	/**
	 * (ECHO) Percentage formatting
	 * @param (Float) v = amount to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function drawPercentageForDisplay($v,$decimal=0) {
		echo $this->getPercentageForDisplay($v,$decimal);
	}
	/**
	 * (RETURN) Percentage formatting
	 * @param (Float) v = amount to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function getPercentageForDisplay($v,$decimal=0) {
		$v*=1;
		return number_format($v,$decimal)."%";
	}

	/**
	 * (ECHO) Number/Integer formatting
	 * @param (Number) v = number to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function drawNumberForDisplay($v,$decimal=0) {
		echo $this->getNumberForDisplay($v,$decimal);
	}
	/**
	 * (RETURN) Number/Integer formatting
	 * @param (Float) v = number to be displayed
	 * @param (Int) decimal = number of decimal places
	 */
	public function getNumberForDisplay($v,$decimal=0) {
		$v*=1;
		return ASSIST_HELPER::format_number($v,($decimal>0?"FLOAT":"INT"),$decimal);
		//return number_format($v,$decimal);
	}


	/**
	 * (ECHO) BOOL formatting
	 * @param (BOOL) v = value to be displayed = true/false or 1/0
	 */
	public function drawBoolForDisplay($v) {
		echo $this->getBoolForDisplay($v);
	}
	/**
	 * (RETURN) BOOL formatting
	 * @param (BOOL) v = value to be displayed = true/false or 1/0
	 */
	public function getBoolForDisplay($v,$html_tags=true) {
		if($v==true) {
			return ($html_tags ? $this->getDisplayIconAsDiv("ok")." " : "")."Yes";
		} else {
			return ($html_tags ? $this->getDisplayIconAsDiv("0")." " : "")."No";
		}
	}

/*	public function getAttachForDisplay($val,$can_edit=false) {
		$me = new ASSIST_MODULE_HELPER();
		$folder = $me->getSaveFolder();
		$data = "";
		$y = unserialize($val);
		$z = array();
		foreach($y as $i => $x) {
			$filename = $folder."/".$x['location']."/".$x['system_filename'];
			if(file_exists($filename)) {
				$f = "<tr ref=".$i." class=down_attach><td>";
				$fn = $x['original_filename'];
				if(isset($x['ext']) && strlen($x['ext'])>0) {
					if(isset($this->common_extensions[$x['ext']])) {
						$f .= "<img src=/pics/icons/".$this->file_icons[$this->common_extensions[$x['ext']]]." />";
					} else {
						$f .= "<img src=/pics/icons/".$this->file_icons['other']." />";
						//Notify Action iT so that the icon can be identified
						mail("actionit.actionassist@gmail.com","Unknown file extension identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","From: no-reply@ignite4u.co.za");
					}
				}
				$f.="</td><td><a href=#>".$fn."</a></td>";
				if($can_edit) {
					$f.="<td class=delete_attach>delete</td>";
				}
				$z[] = $f."</tr>";
			}
		}
		if(count($z)>0) {
			$data = "<table class='attach not-max'>".implode("",$z)."</table>";

		}
		return $data;
	}
 */

	/**
	 * Function to generate code to display existing attachments
	 * @param (serializeArray*) val
	 * @param (Array) options
	 */
	 public function getAttachmentView($val,$options){
	 	$data = array('display'=>"<div id=div_attach_container style='width:350px;'>",'js'=>"");
		$data['display'].=$this->getAttachForDisplay($val,
															(false),
															(isset($options['object_type']) ? $options['object_type'] : "X"),
															(isset($options['object_id']) ? $options['object_id'] : "0"),
															true,
															(isset($options['page_activity']) ? $options['page_activity'] : "VIEW")
														);
		$data['js'] = $this->getAttachmentDownloadJS($options['object_type'],$options['object_id'],$options['page_activity']);
		return $data;
	 }
	 public function getAttachmentViewNoButton($val,$options){
	 	$data = array('display'=>"<div id=div_attach_container style=''>",'js'=>"");
		$data['display'].=$this->getAttachForDisplayNoButton($val,
															(false),
															(isset($options['object_type']) ? $options['object_type'] : "X"),
															(isset($options['object_id']) ? $options['object_id'] : "0"),
															(isset($options['buttons']) ? $options['buttons'] : true),
															(isset($options['page_activity']) ? $options['page_activity'] : "VIEW")
														);
		$data['js'] = $this->getAttachmentDownloadJSNoButton($options['object_type'],$options['object_id'],$options['page_activity'],(isset($options['js_activity']) ? $options['js_activity'] : "UPDATE"));
		return $data;
	 }


	/**
	 * Function to generate code to display existing attachments
	 * @param (serializeArray*) val
	 * @param (BOOL) can_edit=false
	 * @param (Str) Object_type=X
	 * @param (Int) Object_id=0
	 * @param (Bool) buttons=true
	 * @param (Str) page_activity=VIEW
	 */
	public function getAttachForDisplay($val,$can_edit=false,$object_type="X",$object_id="0",$buttons=true,$activity="VIEW") {
		$dom_element_id = strtolower($object_type."_".$activity."_".$object_id);
		$amh = new ASSIST_MODULE_HELPER();
		$folder = $amh->getSaveFolder();
		$data = "";
		if(!is_array($val)) {
			if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
			$y = unserialize($val);
			$z = array();
		} else {
			$y = false;
			$z = array();
		}
		//echo json_encode($y);
//		echo $dom_element_id;
		$c = 0;
		if(is_array($y) && count($y)>0) { //ASSIST_HELPER::arrPrint($y);
			foreach($y as $i => $x) {
				//error trap for blank $i that broke SDBP6/IKPI2 Attachment functions
				if(!ASSIST_HELPER::checkIntRef($i)) { $i = $c; }
				$filename = $folder."/".$x['location']."/".$x['system_filename'];
				//echo "<strong>".$filename."</strong>";
				if((!isset($x['status']) || (($x['status'] & self::ACTIVE) == self::ACTIVE)) && file_exists($filename)) {
					$f = "<tr id=th_".$i."><td width=26px class=center>";
					//$fn = $x['original_filename'];
                    $strFileName = substr($x['original_filename'], 0, 28); // cutting the string so it doesn't stretch the page.
                    $fn = $strFileName;                                                 //substr changes will affect the form_object.php page's documents tab table. The download button field
                    $lengthFileName = strlen($strFileName);
                    if ($lengthFileName > 27){
                        $fn.= "...";
                    }
					if(isset($x['ext']) && strlen($x['ext'])>0) {
						if(isset($this->common_extensions[strtolower($x['ext'])])) {
							$f .= "<img src=/pics/icons/".$this->file_icons[$this->common_extensions[strtolower($x['ext'])]]." />";
						} else {
							$f .= "<img src=/pics/icons/".$this->file_icons['other']." />";
							//Notify Action iT so that the icon can be identified
							$emailObject = new ASSIST_EMAIL("janet@actionassist.co.za","New File Extension Identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","HTML");
							$emailObject->sendEmail();
							//mail("actionit.actionassist@gmail.com","Unknown file extension identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","From: no-reply@ignite4u.co.za");
						}
					}
					$f.="</td><td>".$fn."</td>";
					if($buttons) {
						$f.="<td  class='right' ><button ref='".$i."' class='button_class down_attach_".$dom_element_id."'>".$this->getActivityName("download")."</button>";
						if($can_edit) {
							$f.="&nbsp;<button ref='".$i."' class='button_class delete_attach_".$dom_element_id."'>".$this->getActivityName("delete")."</button>";
						}
						$f.="</td>";
					}
					$z[] = $f."</tr>";
				}
				$c++;
			}
		}
		if(count($z)>0) {
			$data = "<table id=tbl_display_attach class='attach' width=85% style='".($can_edit ? "margin-bottom: 20px;" : "")."'>".implode("",$z)."</table>";

		}
		return $data;
	}
	public function getAttachIconPathByExt($ext) {
		return "/pics/icons/".$this->getAttachIconByExt($ext);
	}
	public function getAttachIconByExt($ext) {
		if(isset($this->common_extensions[strtolower($ext)])) {
			return $this->file_icons[$this->common_extensions[strtolower($ext)]];
		} else {
			//Notify Action iT so that the icon can be identified
			$emailObject = new ASSIST_EMAIL("janet@actionassist.co.za","New File Extension Identified",$ext." could not be found in the common file extensions of assist_module_display.","HTML");
			$emailObject->sendEmail();
			//return default "other" icon
			return $this->file_icons['other'];
		}
	}
/**
 * Option only for downloading of attachments (no delete) - displays link in file name rather than separate button - needed for SDBP6
 */
	public function getAttachForDisplayNoButton($val,$can_edit=false,$object_type="X",$object_id="0",$buttons=true,$activity="VIEW") {
		if($activity=="VIEW_EXTERNAL") {
			$url_activity = "VIEW";
		} else {
			$url_activity = $activity;
		}
		$dom_element_id = strtolower($object_type."_".$url_activity."_".$object_id);
		$amh = new ASSIST_MODULE_HELPER();
		$folder = $amh->getSaveFolder();
		$data = "";
		if(!is_array($val) && $val!==false) {
			if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
			$y = unserialize($val);
			$z = array();
		} else {
			$y = false;
			$z = array();
		}
		//echo json_encode($y);
		if(is_array($y) && count($y)>0) {
			foreach($y as $i => $x) {
				$filename = $folder."/".$x['location']."/".$x['system_filename'];
				//echo "<strong>".$filename."</strong>";
				if((!isset($x['status']) || (($x['status'] & self::ACTIVE) == self::ACTIVE)) && file_exists($filename)) {
					$f = "<tr id=th_".$i."><td width=26px class=center>";
					$fn = $x['original_filename'];
					if(isset($x['ext']) && strlen($x['ext'])>0) {
						if(isset($this->common_extensions[strtolower($x['ext'])])) {
							$f .= "<img src=/pics/icons/".$this->file_icons[$this->common_extensions[$x['ext']]]." />";
						} else {
							$f .= "<img src=/pics/icons/".$this->file_icons['other']." />";
							//Notify Action iT so that the icon can be identified
							$emailObject = new ASSIST_EMAIL("janet@actionassist.co.za","New File Extension Identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","HTML");
							$emailObject->sendEmail();
							//mail("actionit.actionassist@gmail.com","Unknown file extension identified",$x['ext']." could not be found in the common file extensions of assist_module_display.","From: no-reply@ignite4u.co.za");
						}
					}
					$link = "";
					if($buttons) {
						$link="<a href=# ref=".$i." class='down_attach_".$dom_element_id."'>";
					}
					$f.="</td><td>".$link.$fn."</a></td>";
					$z[] = $f."</tr>";
				}
			}
		}
		if(count($z)>0) {
			$data = "<table id=tbl_display_attach class='attach' width=100% style='".($can_edit ? "margin-bottom: 20px;" : "")."'>".implode("",$z)."</table>";

		}
		return $data;
	}
	public function getAttachForDisplayInList($val,$object_type="X",$object_id="0",$folder=false) {
		//$activity = "VIEW";
		if($folder===false) {
			$amh = new ASSIST_MODULE_HELPER();
			$folder = $amh->getSaveFolder();
			unset($amh);
		}
		$z = array();
		if(!is_array($val)) {
			if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
			$y = unserialize($val);
		} else {
			$y = $val;
		}
		unset($val);
		//echo json_encode($y);
		if(is_array($y) && count($y)>0) {
			foreach($y as $i => $x) {
				$filename = $folder."/".$x['location']."/".$x['system_filename'];
				//echo "<strong>".$filename."</strong>";
				if((!isset($x['status']) || (($x['status'] & self::ACTIVE) == self::ACTIVE)) && file_exists($filename)) {
					//no icons in list display so unnecessary code from object view pulled here
					$z[] = $x['original_filename'];
				}
			}
		}
		$data = "";
		if(count($z)>0) {
			$data = implode("<br />",$z);
		}
		return $data;
	}
/*	public function getAttachmentDownloadJS($object_type,$object_id) {
		$js = "
		$('tr.down_attach').css('cursor','pointer').click(function() {
			//var i = $(this).attr('ref'); //alert(i);
			//var dta = 'object_id=".$object_id."&i='+i;
			//var result = AssistHelper.doAjax('inc_attachment_controller.php?action=".$object_type.".GET_ATTACH',dta);
			//document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
			//console.log(result);
		});
		$('tr.down_attach img, tr.down_attach a').click(function(event){
			event.preventDefault();
			var i = $(this).parent().parent().attr('ref');
			alert(i);
			//var dta = 'object_id=".$object_id."&i='+i;
			//document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
		});
		$('td.delete_attach').hover(
			function() {
				$(this).css('color','#fe9900');
			},
			function() {
				$(this).css('color','');
			}
		);
		";
		return $js;
	}
	*/
	public function getAttachmentDownloadJS($object_type,$object_id,$activity="VIEW") {
		$dom_element_id = strtolower($object_type."_".$activity."_".$object_id);
		if(!isset($this->js['attachment_down_delete_'.$dom_element_id]) || $this->js['attachment_down_delete_'.$dom_element_id]!==true) {
			$this->js['attachment_down_delete_'.$dom_element_id] = true;
			$js = "
			/*$('table.attach tr').hover(
				function() { $(this).children('td:eq(1)').addClass('trhover'); },
				function() { $(this).children('td:eq(1)').removeClass('trhover'); }
			);*/
			$('button.down_attach_".$dom_element_id."').button({
				icons: {primary: 'ui-icon-arrowthickstop-1-s'}
			}).hover(
				function() {
					$(this).addClass('ui-state-ok');
				},
				function() {
					$(this).removeClass('ui-state-ok');
				}
			).click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var dta = 'activity=".$activity."&object_id=".$object_id."&i='+i;
				document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
				$(this).blur();
			});
			$('button.delete_attach_".$dom_element_id."').button({
				icons: {primary: 'ui-icon-trash'}
			}).hover(
				function() {
					$(this).addClass('ui-state-error');
				},
				function() {
					$(this).removeClass('ui-state-error');
				}
			).click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var txt = $(this).parent().parent().children('td:eq(1)').html();
				var nm = $(this).parents('tr:eq(1)').children('th:first').html();
				if(confirm('Are you sure you wish to ".$this->getActivityName("delete")." '+nm+' '+txt)) {
					AssistHelper.processing();
					var dta = 'activity=".$activity."&object_id=".$object_id."&i='+i;
					//console.log(dta);
					var result = AssistHelper.doAjax('inc_controller.php?action=".$object_type.".DELETE_ATTACH',dta);
					console.log(result);
					if(result[0]=='ok') {
						$(this).parents('tr:eq(0)').hide();
					}
					AssistHelper.finishedProcessing(result[0],result[1]);
					//AssistHelper.closeProcessing();
				}
			});
			";
		} else {
			$js = "";
		}
		return $js;
	}
	public function getAttachmentDownloadJS3($object_type,$object_id,$activity="VIEW") {
		$dom_element_id = strtolower($object_type."_".$activity."_".$object_id);
		if($activity!=="VIEW") {
			$view_element_id = strtolower($object_type."_view_".$object_id);
		} else {
			$view_element_id = $dom_element_id;
		}
		if(!isset($this->js['attachment_down_delete_'.$dom_element_id]) || $this->js['attachment_down_delete_'.$dom_element_id]!==true) {
			$this->js['attachment_down_delete_'.$dom_element_id] = true;
			$js = "
			/*$('table.attach tr').hover(
				function() { $(this).children('td:eq(1)').addClass('trhover'); },
				function() { $(this).children('td:eq(1)').removeClass('trhover'); }
			);*/
			$('button.down_attach_".$dom_element_id."').button({
				icons: {primary: 'ui-icon-arrowthickstop-1-s'}
			}).removeClass('ui-state-default').addClass('ui-button-minor-grey').hover(
				function() {
					$(this).removeClass('ui-button-minor-grey').addClass('ui-button-minor-green');
				},
				function() {
					$(this).removeClass('ui-button-minor-green').addClass('ui-button-minor-grey');
				}
			).click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var dta = 'activity=".$activity."&object_id=".$object_id."&i='+i;
				document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
				$(this).blur();
			});
			$('button.delete_attach_".$dom_element_id."').button({
				icons: {primary: 'ui-icon-trash'}
			}).removeClass('ui-state-default').addClass('ui-button-minor-grey').hover(
				function() {
					$(this).removeClass('ui-button-minor-grey').addClass('ui-button-minor-red');
				},
				function() {
					$(this).removeClass('ui-button-minor-red').addClass('ui-button-minor-grey');
				}
			).click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var txt = $(this).parent().parent().children('td:eq(1)').html();
				if($(this).parents('tr:eq(1)').children('th:first').children('span').length>0) {
					var nm = $(this).parents('tr:eq(1)').children('th:first').children('span:first').html();
				} else {
				var nm = $(this).parents('tr:eq(1)').children('th:first').html();
				}
				if(confirm('Are you sure you wish to ".strtolower($this->getActivityName("delete"))." '+nm+' '+txt+'?')) {
					AssistHelper.processing();
					var dta = 'activity=".$activity."&object_id=".$object_id."&i='+i;
					console.log(dta);
					var result = AssistHelper.doAjax('inc_controller.php?action=".$object_type.".DELETE_ATTACH',dta);
					console.log(result);
					if(result[0]=='ok') {
						$(this).parents('tr:eq(0)').hide();
						$('a.down_attach_".$view_element_id."').closest('tr').hide();
					}
					AssistHelper.finishedProcessing(result[0],result[1]);
					//AssistHelper.closeProcessing();
				}
			});
			";
		} else {
			$js = "";
		}
		return $js;
	}

	public function getAttachmentDownloadJSNoButton($object_type,$object_id,$activity="VIEW",$download_activity="UPDATE") {
		if($activity=="VIEW_EXTERNAL") {
			$local_activity = "VIEW";
			$is_external_call = 1;
		} else {
			$local_activity = $activity;
			$is_external_call = 0;
		}
		$dom_element_id = strtolower($object_type."_".$local_activity."_".$object_id);
		if(!isset($this->js['attachment_down_'.$dom_element_id]) || $this->js['attachment_down_'.$dom_element_id]!==true) {
			$this->js['attachment_down_'.$dom_element_id] = true;
			$js = "
			$('a.down_attach_".$dom_element_id."').click(function(e){
				e.preventDefault();
				var i = $(this).attr('ref');
				var dta = 'is_external_module_call=".$is_external_call."&activity=".$download_activity."&object_id=".$object_id."&i='+i;
				document.location.href = 'inc_attachment_controller.php?action=".$object_type.".GET_ATTACH&'+dta;
				$(this).blur();
			});
			";
		} else {
			$js = "";
		}
		return $js;
	}


	/******
	 * Data display PRIVATE functions
	 */
	private function getDisplayIconAsDiv($icon,$options="") {
		if($icon===true || $icon==true) { $icon="1"; } elseif($icon===false || $icon==false) {$icon="0";}
		switch($icon) {
		case "ok":
		case "1":
			return "
				<div class=\"ui-icon ui-icon-check\" style=\"background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "error":
		case "0":
			return "
				<div class=\"ui-icon ui-icon-closethick\" style=\"background-image: url(/library/jquery/css/images/ui-icons_cc0001_256x240.png); display:inline-block; overflow:visible;".$options."\">&nbsp;</div>
			";
			break;
		case "info":
			return "
				<div class=\"ui-icon ui-icon-info\" style=\"background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png); display:inline-block; overflow:visible\" ".$options.">&nbsp;</div>
			";
			break;
		}
	}





	public function displayJSSettings() { ASSIST_HELPER::arrPrint($this->js); }


	static function convertStringToHTML($str) {
		$str = str_replace(chr(10),'<br />',$str);
		return $str;
	}

	/**
	 * Used internally & externally by modules - keep static for easy reference
	 * AA-690 [JC] 3 Sep 2021
	 * @param $val
	 * @param bool $keep_raw = default=true - set to false to format the number for UI display / reporting
	 * @return string
	 */
	static function processingBigNumbersForDisplay($val,$keep_raw = true) {
		//hack for big float val handling to stop PHP from being non-precise when converting to float or *1'ing [JC] AA-660 3 Sep 2021
		//KEEP IT AS A STRING - AT NO STAGE CAN YOU CONVERT TO A NUMBER - PHP adds decimals and then doesn't round / number_format properly after that - KEEP IT AS A STRING
		if(strlen($val)>0) {
			//pull out non-numerical characters
			$val = preg_replace("/[^0-9.-]/", "", $val);
			//if there is a decimal place, then check the tail for excess trailing 0s and remove while keeping it as a string
			//This is to comply with AA-595 [JC] 19 April 2021
			//if there is a value and a decimal place exists....
			if(strlen($val) > 0 && strpos($val, ".") !== false) {
				$test = false;
				while(!$test) {
					//get the last character from the string
					$char = substr($val, strlen($val) - 1, 1);
					//if the character is a decimal place or a 0....
					if($char == "." || $char == "0" || $char == 0) {
						//strip the character from the string
						$val = substr($val, 0, -1);
						//if you've hit the decimal then there is nothing left to do so break out
						if($char == ".") {
							$test = true;
						}
					} else {
						//otherwise you've run into a non-0 or decimal character and must stop processing so break out
						$test = true;
					}
				}
			}
		}
		//if I do need to display my float in nice number formatting.... time to get sneaky [JC] AA-690 3 Sep 2021
		if(!$keep_raw) {
			$thousand_separator = ASSIST_HELPER::NUMBER_THOUSANDS_SEPARATOR;
			//separate front and back
			$a = explode(".",$val);
			//format the front part nicely
			$x = number_format($a[0],0,".",$thousand_separator);
			//string the front and back back together again
			//implicitly cast as a string
			$val = $x.(isset($a[1])?".".$a[1]:"").(isset($a[1])&&strlen($a[1])==1?"0":"");
		}
		return $val;
	}

}



?>