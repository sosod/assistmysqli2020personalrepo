<?php


class ASSIST_REPORT extends ASSIST {

	protected $db_date_format = "DATETIME";	/* What format are dates stored in the database?  VARCHAR / STRING || UNIX / NUMERIC || DATE / DATETIME */
	protected $defaults = array(
		'allowchoose' => true,
		'default_selected' => true,
		'allowfilter' => true,
		'type' => "TEXT",
		'data' => "",
		'default_data' => "",
		'allowgroupby' => true,
		'allowsortby' => true,
		'sortposition' => 0,
	);
	protected $vars;

// REPORT DETAILS
	protected $fields = array();				//array[id]=>('id'=>"",'heading'=>"")
	protected $columns = array();				//array[id]=>('id'=>"",'selected'=>true)
	protected $filters = array();				//array[id]=>('id'=>"",'type'=>"TEXT")
	protected $filter_types = array();		//array[id]=>('id'=>"",'data'=>""/array(),'default'=>"")
	protected $group_by = array();			//array[id]=>true
	protected $sort_by = array();				//array[id]=>true
	protected $sort_order = array();


	protected $result_field_column_selector_title;
	protected $result_field_columns;
    protected $result_field_time_periods;
    protected $result_field_value_types;
	protected $result_top_fields = array();
	protected $result_bottom_fields = array();
    protected $result_top_overall_fields = array();
    protected $result_bottom_overall_fields = array();
    protected $change_log_field = array();

	protected $has_history_report = false;
	protected $history_reports_running = 0;
	protected $max_history_reports_running = 2;

	
	public function __construct() {
		//echo "<P>ASSIST_REPORT.__construct()</p>";
		parent::__construct();
		$this->vars = $_REQUEST;
	}

	
	/** SETTING FUNCTIONS **/
	public function setDBDateFormat($d) { $this->db_date_format = $d; }
	public function activateHistoryReport() { $this->has_history_report = true; }
	public function hasHistoryReport() { return $this->has_history_report; }
	
	
	public function setReportsAlreadyRunning($h) { $this->history_reports_running = $h; }
	public function getHistoryReportsRunning() { return $this->history_reports_running; }
	public function isMaxHistoryReportsRunning() { return ($this->history_reports_running>=$this->max_history_reports_running); }
	
	
	public function addField($id,$title,$allowchoose=true,$sel=true,$allowfilter=true,$type="TEXT",$data="",$def="",$allowgroupby=true,$allowsortby=true,$sortposition=0) {
		//echo "<P>Add field ::> ".$id;
		$this->fields[$id] = array(
			'id'		=>	$id,
			'heading'	=>	$title,
			'type'		=>	$type,
		);
		if($allowchoose===true) {
			$this->addColumn($id,$sel);
		}
		if($allowfilter===true) {
			$this->addFilter($id,$data,$def);
		}
		if($allowgroupby===true) {
			$this->addGroupBy($id);
		}
		if($allowsortby===true) {
			$this->addSortBy($id);
			$this->setSortPosition($id,$sortposition);
		}
	}

    public function addResultFieldColumn($id,$title,$sel=true){
        $this->result_field_columns[$id] = array(
            'id'=>$id,
            'heading'=>$title,
            'selected'=>$sel
        );
    }

    public function setResultFieldColumnSelectionTitle($title){
        $this->result_field_column_selector_title = $title;
    }

    public function addResultFieldColumns($id,$title,$sel=true){
        $this->result_field_columns[$id] = array(
            'id'=>$id,
            'heading'=>$title,
            'selected'=>$sel
        );
    }

    public function addResultFieldTimePeriods($result_field_time_periods){
        $this->result_field_time_periods = $result_field_time_periods;
    }

    public function addResultFieldValueTypes($result_field_value_types){
        $this->result_field_value_types = $result_field_value_types;
    }

    public function addTopResultFields($field_id, $field_name){
        $this->result_top_fields[$field_id] = $field_name;
    }

    public function addBottomResultFields($field_id, $field_name){
        $this->result_bottom_fields[$field_id] = $field_name;
    }

    public function addTopOverallResultFields($field_id, $field_name){
        $this->result_top_overall_fields[$field_id] = $field_name;
    }

    public function addBottomOverallResultFields($field_id, $field_name){
        $this->result_bottom_overall_fields[$field_id] = $field_name;
    }

    public function addChangeLogField($field_id, $field_name){
        $this->change_log_field[$field_id] = $field_name;
    }
	
	protected function addFieldByArray($f) {
		$this->addField($f['id'],$f['title'],$f['allowchoose'],$f['default_selected'],$f['allowfilter'],$f['type'],$f['data'],$f['default_data'],$f['allowgroupby'],$f['allowsortby'],$f['sortposition']);
	}
	
	protected function addColumn($id,$sel=true) { 
		$this->columns[$id] = array(
			'id'=>$id,
			'selected'=>$sel
		);
	}
	
	protected function addFilter($id,$data,$def) {
		$this->filters[$id] = array(
			'id'=>$id,
			'data'=>$data,
			'default'=>$def,
		);
	}
	
	protected function addGroupBy($id) { $this->group_by[$id] = true; }
	protected function addSortBy($id) { $this->sort_by[$id] = true; }
	protected function setSortPosition($id,$pos) { $this->sort_order[$pos] = $id; }
	


}


?>