<?php


class ASSIST_REPORT_DRAW_TABLE extends ASSIST_REPORT_DRAW {
	private $settings = array();
	private $report_settings = array('columns'    => array(),
		'filter'		=> array(),
		'filtertype'	=> array(),
		'groupby'		=> array(),
									 'sort'       => array(),);
	private $rows = array();
	private $result_rows = array();
	private $overall_result_rows = array();
	private $change_log_rows = array();

	public function __construct() {
		//echo "<P>ASSIST_REPORT_DRAW_TABLE.__construct()</p>";
		parent::__construct();
	} 
	
	public function prepareSettings($results_headings=array()) {
	//echo "<p>assist_report_draw_table.preparesettings";
		$this->report_settings = array('columns'                => $_REQUEST['columns'],
			'filter'		=> (isset($_REQUEST['filter']) ? $_REQUEST['filter'] : array()),
			'filtertype'	=> (isset($_REQUEST['filtertype']) ? $_REQUEST['filtertype'] : array()),
			'groupby'		=> (isset($_REQUEST['groupby']) ? $_REQUEST['groupby'] : array()),
			'sort'			=> (isset($_REQUEST['sort']) ? $_REQUEST['sort'] : array()),
			'result_columns'=> (isset($_REQUEST['result_columns']) ? $_REQUEST['result_columns'] : array()),
									   'overall_result_columns' => (isset($_REQUEST['overall_result_columns']) ? $_REQUEST['overall_result_columns'] : array()),);
		//echo "<P>output format: ".$this->output;
		switch($this->output) {
			case "csv":
				$this->setFormatting("CSV");
				//$this->setNewline(chr(10));
				$this->setNewline(" ");
				$x = "\"".$this->getCmpName()."\"\r\n\"".$this->getReportTitle()."\"";
				$this->setStartPage($x);
				$this->setEndPage("\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"");
				$this->setStartGroupHeading("\"\"\r\n\"");
				$this->setStartSummaryHeading("\"\"\r\n\"");
				$this->setEndGroupHeading("\"\r\n");
				$this->setEndSummaryHeading("\"\r\n");
				$this->setStartTable("\r\n");
				$this->setStartSummaryTable("\r\n");
				$this->setEndTable("");
				$this->setStartTableHeader("");
				$this->setEndTableHeader("");
				$this->setStartTableBody("");
				$this->setEndTableBody("");
				$this->setStartRow("");
				$this->setEndRow("\r\n");
				$this->setStartCell("default","\"");
				$this->setStartCell("heading","\"");
				$this->setStartCell("result_heading","\"");
				$this->setStartCell("dbl_row_span_heading","\"");
				$this->setStartCell("result_col_span_heading","\"");
				$this->setStartCell("overall_result_col_span_heading","\"");
				$this->setStartCell("center","\"");
				$this->setStartCell("TEXT","\"");
				$this->setStartCell("NUM","\"");
				$this->setStartCell("PERC","\"");
				$this->setStartCell("LIST","\"");
				$this->setStartCell("SUM_TOTAL","\"");
				$this->setStartCell("overall","\"");
				foreach($this->result_categories as $key => $rc) {
					$this->setStartCell("RESULT_".$key,"\"");
				}
				$this->setEndCell("default","\",");
				$this->setEndCell("heading","\",");				
				$this->setEndCell("result_heading","\",\"\",");
				$closecell = "\",";
				for($r=1;$r<count($this->report_settings['result_columns']);$r++) {
					$closecell.="\"\",";
				}
				$this->setEndCell("dbl_row_span_heading","\",");
				$this->setEndCell("result_col_span_heading",$closecell);
				$this->setNoResult("\r\n\"There are no results to display.\"\r\n");
				/*
				$this->settings['cell'][22]['a']['OVERALL'] = "\"";	//ptd normal cell
				$this->settings['cell'][22]['a']['NUM'] = "\""; //result cell
				$this->settings['cell'][22]['a']['DOUBLE'] = "\""; //result cell
				$this->settings['cell'][22]['a']['TEXT'] = "\""; //result cell
				
				*/
				break;
			case "excel":
				$this->setFormatting("XLS");
				$this->setNewline("<br style=\"mso-data-placement:same-cell;\">");
				//<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">
				$x = "
				<html>
					<head>
						<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
						<!--[if gte mso 9]>
							<xml>
							<x:ExcelWorkbook>
							<x:ExcelWorksheets>
							<x:ExcelWorksheet>
							<x:Name>Report</x:Name>
							<x:WorksheetOptions>
							<x:Panes>
							</x:Panes>
							</x:WorksheetOptions>
							</x:ExcelWorksheet>
							</x:ExcelWorksheets>
							</x:ExcelWorkbook>
							</xml>
						<![endif]-->
						<style>
							td { font-style: Calibri; font-size:11pt; } 
							.kpi { border-width: thin; border-color: #000000; border-style: solid; vertical-align: top; } 
							.kpi_r { border-width: thin; border-color: #000000; border-style: solid; text-align: center; color: #FFFFFF; vertical-align: top; } 
							.head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } 
							.title { font-size:20pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:underline; text-align: center; } 
							.title2 { font-size:16pt; font-style: Calibri; color:#000099; font-weight:bold; text-decoration:none; text-align: center;} 
							.title3 { font-size:14pt; font-style: Calibri; color:#000099; font-weight:bold;} 
							.title4 { font-size:12pt; font-style: Calibri; color:#000099; font-weight:bold;}
							.summary {
								margin: 3px;
								line-height: 1.1em;
							}
						</style>
					</head>
					<body><table><tr><td class=title nowrap colspan=".(count($this->report_settings['columns'])+(isset($this->report_settings['columns']['result']) ? 1 : 0))." >".$this->getCmpName()."</td></tr>
					<tr><td class=title2 nowrap colspan=".(count($this->report_settings['columns'])+(isset($this->report_settings['columns']['result']) ? 1 : 0))." >".$this->getReportTitle()."</td></tr>
				";
				$this->setStartPage($x);
				$this->setEndPage("<tr></tr><tr><td></td><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>");
				$this->setStartGroupHeading("<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>");
				$this->setEndGroupHeading("</td></tr>");
				$this->setStartSummaryHeading("<tr><td width=50></td></tr><tr><td nowrap class=title4 rowspan=1>");
				$this->setEndSummaryHeading("</td></tr>");
				$this->setStartTable("");
				$this->setStartSummaryTable("");
				$this->setEndTable("");
				$this->setStartTableHeader("");
				$this->setEndTableHeader("");
				$this->setStartTableBody("");
				$this->setEndTableBody("");
				$this->setStartRow("<tr>");
				$this->setEndRow("</tr>");
				$this->setStartCell("default","<td class=kpi>");
				$this->setStartCell("heading","<td class=\"head\">");
				$this->setStartCell("result_heading","<td colspan=2 class=\"head\">");
				$this->setStartCell("dbl_row_span_heading","<td rowspan=2 class=\"head\">");
				$this->setStartCell("result_col_span_heading","<td colspan=".count($this->report_settings['result_columns'])." class=\"head\">");
                $this->setStartCell("overall_result_col_span_heading","<th colspan=".count($this->report_settings['overall_result_columns'])." class=\"head\">");
				$this->setStartCell("center","<td class=kpi style='text-align:center'>");
				$this->setStartCell("TEXT","<td class=kpi style=''>");
				$this->setStartCell("NUM","<td class=kpi style='text-align:right;'>");
				$this->setStartCell("PERC","<td class=kpi style='text-align:right;'>");
				$this->setStartCell("LIST","<td class=kpi>");
				$this->setStartCell("SUM_TOTAL","<td class=kpi style='font-weight: bold; text-align: right;'>");
				$this->setStartCell("overall","<td class=kpi style='font-weight: bold; text-align: right; background-color: #ddd'>");
				foreach($this->result_categories as $key => $rc) {
					$this->setStartCell("RESULT_".$key,"<td class=kpi style='color: #FFFFFF; text-align: center; background-color: ".$this->color_codes[$rc['color']]['color'].";'>");
				}
				$this->setEndCell("default","</td>");
				$this->setEndCell("heading","</td>");			
				$this->setEndCell("result_heading","</td>");
				$this->setEndCell("dbl_row_span_heading","</td>");
				$this->setEndCell("result_col_span_heading","</td>");
                $this->setEndCell("overall_result_col_span_heading","</th>");
				$this->setNoResult("<tr><td class=kpi>There are no results to display.</td></tr>");
				/*
				$this->settings['cell'][20]['a'] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
				$this->settings['cell'][22]['a']['OVERALL'] = "<td style=\"text-align: right;background-color: #eeeeee\" class=kpi>";	//ptd normal cell
				$this->settings['cell'][22]['a']['NUM'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
				$this->settings['cell'][22]['a']['DOUBLE'] = "<td class=kpi style=\"text-align: right;\">"; //result cell
				$this->settings['cell'][22]['a']['TEXT'] = "<td class=kpi>"; //result cell
				*/
				break;
			case "onscreen";
			default:
				$this->setFormatting("HTML");
				$this->setNewline("<br />");
				$x = "
				<html>
					<head>
						<meta http-equiv=\"Content-Language\" content=\"en-za\">
						<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
						<title>www.Action4u.co.za</title>
					</head>
					<link rel=\"stylesheet\" href=\"/assist.css?".time()."\" type=\"text/css\">
					<style type=text/css>
					table.summary, table.summary td {
						border: 2px solid #FFFFFF;
						margin: 3px;
						line-height: 1.1em;
					}
					</style>
					<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
					<h1 style=\"text-align: center;\">".$this->getCmpName()."</h1>
					<h2 style=\"text-align: center;\">".$this->getReportTitle()."</h2>
				";
				$this->setStartPage($x);
				$this->setEndPage("<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>");
				$this->setStartGroupHeading("<h3>");
				$this->setEndGroupHeading("</h3>");
				$this->setStartSummaryHeading("<h4>");
				$this->setEndSummaryHeading("</h4>");
				$this->setStartTable("<table width=100%>");
				$this->setStartSummaryTable("<table class=summary>");
				$this->setEndTable("</table>");
				$this->setStartTableHeader("<thead>");
				$this->setEndTableHeader("</thead>");
				$this->setStartTableBody("<tbody>");
				$this->setEndTableBody("</tbody>");
				$this->setStartRow("<tr>");
				$this->setEndRow("</tr>");
				$this->setStartCell("default","<td>");
				$this->setStartCell("heading","<th>");
				$this->setStartCell("result_heading","<th colspan=2>");
				$this->setStartCell("dbl_row_span_heading","<th rowspan=2>");
				$this->setStartCell("result_col_span_heading","<th colspan=".count($this->report_settings['result_columns']).">");
				$this->setStartCell("overall_result_col_span_heading","<th colspan=".count($this->report_settings['overall_result_columns']).">");
				$this->setStartCell("center","<td style='text-align:center'>");
				$this->setStartCell("right","<td style='text-align:right'>");
				//$this->setStartCell("TEXT","<td style='font-size: 7pt;'>");
				$this->setStartCell("LOG","<td style='font-size: 7pt;'>");
				$this->setStartCell("TEXT","<td style=''>");
				$this->setStartCell("NUM","<td style='text-align:right;'>");
				$this->setStartCell("PERC","<td style='text-align:right;'>");
				$this->setStartCell("LIST","<td>");
				$this->setStartCell("SUM_TOTAL","<td style='font-weight: bold; text-align: right;'>");
                $this->setStartCell("overall","<td class=kpi style='font-weight: bold; text-align: right; background-color: #ddd'>");
				foreach($this->result_categories as $key => $rc) {
					if(strlen($key)>0) {
//					echo "<h1>".$key."</h1>";
//					ASSIST_HELPER::arrPrint($rc);
					$this->setStartCell("RESULT_".$key,"<td style='color: #FFFFFF; text-align: center; background-color: ".$this->color_codes[$rc['color']]['color'].";'>");
					}
				}
				$this->setEndCell("default","</td>");
				$this->setEndCell("heading","</th>");
				$this->setEndCell("result_heading","</th>");
				$this->setEndCell("dbl_row_span_heading","</th>");
				$this->setEndCell("result_col_span_heading","</th>");
				$this->setEndCell("overall_result_col_span_heading","</th>");
				$this->setNoResult("<p>There are no results to display.</p>");
				break;
			}
		
		
	}
	
	
	public function setRows($r=array()) {
		
		$this->rows = $r;
	}

    public function setResultRows($r=array()) {
        $this->result_rows = $r;
    }

    public function setOverallResultRows($r=array()) {
        $this->overall_result_rows = $r;
    }

    public function setChangeLogRows($r=array()) {
        $this->change_log_rows = $r;
    }
	
	function echoMe($r) {
		$str = "<ul>";
		foreach($r as $k=>$a) {
			$str.="<li>".$k."=>";
			if(is_array($a)) {
				$str.=$this->echoMe($a);
			} else {
				$str.=str_replace("<","[",str_replace(">","]",$a));
			}
			$str.="</li>";
		}
		$str.="</ul>";
		return $str;
	}
	
	public function displayColumnCheck($fld) {
		if(isset($this->columns[$fld]) && isset($this->report_settings['columns'][$fld]) && strtoupper($this->report_settings['columns'][$fld])=="ON") {
			return true;
		} 
		return false;
	}
	
	public function drawPage($action,$version="module") {
		//echo "<P>draw page <P>resulting settings: "; $this->arrPrint($this->settings);
	//echo "<h1>cell settings available:</h1>";
//	foreach($this->settings as $c => $v) {
//		if($c == "cell") {
//			foreach($v as $x => $k) {
//				echo "<p>".$x.": ".$this->code($k['a'])."</p>";
//			}
//		}
//	}
//	$this->arrPrint($this->settings);
		if($action=="GENERATE") {
			$results = array();
			$hasResults = false;
			
			$echo = "";
			$echo .=$this->startPage();
			$results['total'] = $this->createBlankResultArray();

			foreach($this->groups as $gkey=>$g) {
				if(count($this->group_rows[$gkey])>0) {
					$hasResults = true;
					if($gkey != "X" || count($this->groups) > 1) {
						$results[$gkey] = $this->createBlankResultArray();
						$echo.=$this->startGroupHeading().$g.$this->endGroupHeading();
					}
					$echo .=$this->startTable().$this->startTableHeader().$this->startRow();
							foreach($this->fields as $fld => $f) {
								if($this->displayColumnCheck($fld)) {
								    if(!($this->resultFieldsExist())){
                                        $c = $fld=="result" ? "result_heading" : "heading";
                                        $echo .=$this->startCell($c).$f['heading'].$this->endCell($c);
                                    }else{
                                        $c = "dbl_row_span_heading";
                                        $echo .=$this->startCell($c).$f['heading'].$this->endCell($c);
                                    }
								}
							}

							if($this->resultFieldsExist()){
							    foreach($this->result_top_fields as $key => $val){
                                    $c = "result_col_span_heading";
                                    $echo .=$this->startCell($c).$val.$this->endCell($c);
                                }
                                //Overall Results Loop goes here
                                if($this->overallResultFieldsExist()){
                                    foreach($this->result_top_overall_fields as $key => $val){
                                        $c = "overall_result_col_span_heading";
                                        $echo .=$this->startCell($c).$val.$this->endCell($c);
                                    }
                                }
                                //Change Log Loop goes here
                                if($this->changeLogFieldExists()){
                                    foreach($this->change_log_field as $key => $val){
                                        $c = "dbl_row_span_heading";
                                        $echo .=$this->startCell($c).$val.$this->endCell($c);
                                    }
                                }
                                $echo .=$this->endRow();
                                $echo .=$this->startRow();
                                if($this->getOutputFormat()=="csv") {
									foreach($this->fields as $fld => $f) {
										if($this->displayColumnCheck($fld)) {
											if(!($this->resultFieldsExist())){
												$c = $fld=="result" ? "result_heading" : "heading";
												$echo .=$this->startCell($c).$this->endCell($c);
											}else{
												$c = "dbl_row_span_heading";
												$echo .=$this->startCell($c).$this->endCell($c);
											}
										}
									}
								}
                                foreach($this->result_top_fields as $key => $val){
                                    foreach($this->result_bottom_fields as $key2 => $val2){
                                        $field_id = $key2;
                                        if($this->displayThisResultField($field_id)){
                                            $c = "heading";
                                            $echo .=$this->startCell($c).$val2.$this->endCell($c);
                                        }
                                    }
                                }
                                //Overall Results Loop goes here
                                if($this->overallResultFieldsExist()){
                                    foreach($this->result_top_overall_fields as $key => $val){
                                        foreach($this->result_bottom_overall_fields as $key2 => $val2){
                                            $field_id = $key2;
                                            if($this->displayThisResultField($field_id)){
                                                $c = "heading";
                                                $echo .=$this->startCell($c).$val2.$this->endCell($c);
                                            }
                                        }
                                    }
                                }
                            }
					$echo .=$this->endRow().$this->endTableHeader().$this->startTableBody();
                    $summary_able_results_exist = false;
					foreach($this->group_rows[$gkey] as $gr) {
						$r = $this->rows[$gr];
						if(($gkey != "X" && isset($r['result'])) || ($gkey != "X" && $this->resultFieldsExist() && isset($r['result']))) {
							$results[$gkey][$r['result']]++;
						}
						if(($this->displayColumnCheck("result") || ($this->resultFieldsExist() && isset($r['result']))) && isset($r['result'])) {
							$results['total'][$r['result']]++;
							$summary_able_results_exist = true;
						}
						$echo .=$this->startRow();
                        foreach($this->fields as $fld => $f) {
                            if(isset($this->columns[$fld]) && isset($this->report_settings['columns'][$fld]) && strtoupper($this->report_settings['columns'][$fld])=="ON") {
                                $d = isset($r[$fld]) ? $r[$fld] : array();
                                if($fld=="result") {
                                    $rc = $this->getResultCategory($d);
                                    $echo .=$this->startCell("RESULT_".$rc['id']).$rc['code'].$this->endCell().$this->startCell().$rc['heading'].$this->endCell();
                                } else {
                                    $t = $f['type'];
//hack to replace html line breaks (added somewhere in module classes) with standard ascii chr(10) so that correct newline markers can be inserted depending on output format chosen
									if(is_array($d)) { 
										$d = ASSIST_HELPER::removeBlanksFromArray($d,false); 
										$e = implode("|--|",$d);
									} else {
										$e = $d;
									}
									$e = str_replace("<br />",chr(10),$e);
									if(is_array($d)) {
										$d = explode("|--|",$e);
									} else {
										$d = $e;
									}
                                    $e = (is_array($d) ? implode($this->newline(),$d) : str_replace(chr(10),$this->newline(),$d));
                                    $echo.=$this->startCell($t);
                                    $echo.=$e;//THIS IS THE INFORMATION
                                    $echo.=$this->endCell($t);
                                }
                            }
                        }

                        if($this->resultFieldsExist()){
                            foreach($this->result_top_fields as $key => $val){
                                foreach($this->result_bottom_fields as $key2 => $val2){
                                    $field_id = $key2;
                                    if($this->displayThisResultField($field_id)){
                                        $c = "centre";
                                        $field = $key2;
                                        if(substr($field,-7)=="_result") {
                                            $d = $this->result_rows[$gr][$key][$field];
                                            $rc = $this->getResultCategory($d);
                                            $echo .=$this->startCell("RESULT_".$rc['id']).$rc['code'].$this->endCell();
                                        } else {
//hack to replace html line breaks (added somewhere in module classes) with standard ascii chr(10) so that correct newline markers can be inserted depending on output format chosen
										if(isset($this->result_rows[$gr][$key][$field])) {
											$d = $this->result_rows[$gr][$key][$field];
											} else {
												$d = "";
											}
										if(is_array($d)) { 
											$d = ASSIST_HELPER::removeBlanksFromArray($d,false); 
											$e = implode("|--|",$d);
											} else {
												$e = $d;
											}
										$e = str_replace("<br />",chr(10),$e);
										if(is_array($d)) {
											$d = explode("|--|",$e);
											} else {
												$d = $e;
											}
										$e = (is_array($d) ? implode($this->newline(),$d) : str_replace(chr(10),$this->newline(),$d));
										$echo.=$this->startCell($c);
										$echo.=$e;//THIS IS THE INFORMATION
										$echo.=$this->endCell($c);

										/*	if(isset($this->result_rows[$gr][$key][$field]) && is_array($this->result_rows[$gr][$key][$field])) {
																		$this->result_rows[$gr][$key][$field] = ASSIST_HELPER::removeBlanksFromArray($this->result_rows[$gr][$key][$field]);
												$this->result_rows[$gr][$key][$field] = implode($this->newline(),$this->result_rows[$gr][$key][$field]);
											}
                                            $echo .= $this->startCell($c).(isset($this->result_rows[$gr][$key][$field]) ? $this->result_rows[$gr][$key][$field] : $this->getUnspecified()).$this->endCell($c);*/
                                        }
                                    }

                                }
                            }

                            //Overall Results Loop goes here
                            if($this->overallResultFieldsExist()){
                                foreach($this->result_top_overall_fields as $key => $val){
                                    foreach($this->result_bottom_overall_fields as $key2 => $val2){
                                        $field_id = $key2;
                                        if($this->displayThisResultField($field_id)){
                                            $c = "overall";
                                            $field = $key2;
                                            if(substr($field,-7)=="_result") {
                                                $d = $this->overall_result_rows[$gr][$key][$field];
                                                $rc = $this->getResultCategory($d);
                                                $echo .=$this->startCell("RESULT_".$rc['id']).$rc['code'].$this->endCell();
                                            } else {
                                                $echo .= $this->startCell($c).$this->overall_result_rows[$gr][$key][$field].$this->endCell($c);
                                            }
                                        }
                                    }
                                }
                            }
                            //Change Log Loop goes here
                            if($this->changeLogFieldExists()){
                                foreach($this->change_log_field as $key => $val){
                                    $c = "centre";
                                    $field = $key;
                                    $echo .= $this->startCell($c).$this->change_log_rows[$gr][$field].$this->endCell($c);
                                }
                            }
                        }
						$echo .=$this->endRow();
					}
					$echo.=$this->endTableBody().$this->endTable();
					if(($gkey != "X" && $this->displayColumnCheck("result")) || ($gkey != "X" && $this->resultFieldsExist() && $summary_able_results_exist === true)) {
						$echo .= $this->drawSummary($results[$gkey], $g);
					}
				}
				}
			if(array_sum($results['total']) != 0 || $hasResults) {
			} else {
				$echo .= $this->noResult();
			}

			if(($this->displayColumnCheck("result") || ($this->resultFieldsExist() && $summary_able_results_exist === true)) && array_sum($results['total']) > 0) {
				$echo .= $this->drawSummary($results['total'], "total");
			}
			$echo.=$this->endPage();
			$file = $this->outputReport($echo,"SAVE");
			//echo $echo; 
			//$this->arrPrint($this->rows);  $this->arrPrint($this->groups); 
			//echo "<script type=text/javascript>document.location.href = 'report_process.php?source_class=MODULE_COMPL_REPORT_LEGISLATION&page=generate_legislation&act=DRAW&f=".$file."&output=".$this->output."';</script>";
			$this->outputReport("",$file);
		} elseif($action=="DRAW") {
			//$filename = $_REQUEST['f'];
			header("Location:report.php?class=".$_REQUEST['source_class']."&page=".$_REQUEST['page']);
		}
	}

    private function resultFieldsExist() {
        $resultFieldsExist = false;
        if(isset($this->report_settings['result_columns']) && is_array($this->report_settings['result_columns']) && count($this->report_settings['result_columns']) > 0){
            $resultFieldsExist = true;
        }
        return $resultFieldsExist;
    }

    private function overallResultFieldsExist() {
        $overallResultFieldsExist = false;
        if(isset($this->result_top_overall_fields) && is_array($this->result_top_overall_fields) && count($this->result_top_overall_fields) > 0){
            $overallResultFieldsExist = true;
        }
        return $overallResultFieldsExist;
    }

    private function changeLogFieldExists() {
        $changeLogFieldsExist = false;
        if(isset($this->change_log_field) && is_array($this->change_log_field) && count($this->change_log_field) > 0){
            $changeLogFieldsExist = true;
        }
        return $changeLogFieldsExist;
    }

    private function displayThisResultField($field_id) {
	    $displayThisResultField = false;
	    if(isset($this->report_settings['result_columns'][$field_id]) /*&& $this->report_settings['result_columns'][$field_id] == 'ON'*/){
            $displayThisResultField = true;
        }
	    return $displayThisResultField;
    }
	
	public function createBlankResultArray() {
		$r = array();
		foreach($this->result_categories as $id=>$rc) {
			$r[$id] = 0;
		}
		return $r;
	}

	function drawSummary($values,$type="total") {
		$output = $this->output;
		$echo  = "";
		if($type=="total") {
			$echo = $this->startGroupHeading()."Overall Summary of Results".$this->endGroupHeading();
		} else {
			$echo = $this->startSummaryHeading()."Summary of Results: ".$type.$this->endSummaryHeading();
		}
		$echo.=$this->startSummaryTable();
		$description_isset = false;
		foreach($this->result_categories as $key=>$rc) {
            $description_isset = (isset($rc['description']) ? true : $description_isset );
			$echo .= $this->startRow().$this->startCell("RESULT_".$rc['id'])." ".$this->color_codes[$rc['color']]['code']." ".$this->endCell().$this->startCell().$rc['heading'].$this->endCell().($description_isset === true ? $this->startCell().$rc['description'].$this->endCell() : "").$this->startCell("right").$values[$key].$this->endCell().$this->endRow();
		}
		$echo.=$this->startRow().$this->startCell().$this->endCell().$this->startCell("SUM_TOTAL")."Total".($description_isset===true ? " KPIs" : "" ).":".$this->endCell().($description_isset===true ? $this->startCell().$this->endCell() : "" ).$this->startCell("SUM_TOTAL").array_sum($values).$this->endCell().$this->endRow().$this->endTable();

		return $echo;
	}

	
	private function startPage() {
		return $this->settings['page']['a'];
	}

	private function endPage() {
		return $this->settings['page']['z'];
	}

	private function startGroupHeading() {
		return $this->settings['group']['a'];
	}

	private function endGroupHeading() {
		return $this->settings['group']['z'];
	}

	private function startSummaryHeading() {
		return $this->settings['summary']['a'];
	}

	private function endSummaryHeading() {
		return $this->settings['summary']['z'];
	}

	private function startTable() {
		return $this->settings['table']['a'];
	}

	private function startSummaryTable() {
		return $this->settings['table']['summary']['a'];
	}

	private function endTable() {
		return $this->settings['table']['z'];
	}

	private function startTableHeader() {
		return $this->settings['thead']['a'];
	}

	private function endTableHeader() {
		return $this->settings['thead']['z'];
	}

	private function startTableBody() {
		return $this->settings['tbody']['a'];
	}

	private function endTableBody() {
		return $this->settings['tbody']['z'];
	}

	private function startRow() {
		return $this->settings['row']['a'];
	}
	
	private function endRow() {
		return $this->settings['row']['z'];
	}
	
	private function startCell($c="default") { 
		if(!isset($this->settings['cell'][$c])) {
			if(in_array($c,array("MULTITEXT","LOG","MULTILIST"))) {
				$c = "TEXT";
			} else {
				$c = "default"; 
			}
		}
		return $this->settings['cell'][$c]['a']; 
	}
	private function endCell($c="default") { 
		if(!isset($this->settings['cell'][$c]['z'])) {
			$c = "default";
		}
		return $this->settings['cell'][$c]['z']; 
	}

	private function getFormatting() {
		return $this->settings['applied_formatting'];
	}

	private function newline() {
		return $this->settings['newline'];
	}

	private function noResult() {
		return $this->settings['noresult'];
	}

	//private function start() { return $this->settings['']['a']; }
	//private function end() { return $this->settings['']['z']; }
	
	private function setStartPage($a) {
		$this->settings['page']['a'] = $a;
	}

	private function setEndPage($z) {
		$this->settings['page']['z'] = $z;
	}

	private function setStartGroupHeading($a) {
		$this->settings['group']['a'] = $a;
	}

	private function setEndGroupHeading($z) {
		$this->settings['group']['z'] = $z;
	}

	private function setStartSummaryHeading($a) {
		$this->settings['summary']['a'] = $a;
	}

	private function setEndSummaryHeading($z) {
		$this->settings['summary']['z'] = $z;
	}

	private function setStartTable($a) {
		$this->settings['table']['a'] = $a;
	}

	private function setStartSummaryTable($a) {
		$this->settings['table']['summary']['a'] = $a;
	}

	private function setEndTable($z) {
		$this->settings['table']['z'] = $z;
	}

	private function setStartTableHeader($a) {
		$this->settings['thead']['a'] = $a;
	}

	private function setEndTableHeader($z) {
		$this->settings['thead']['z'] = $z;
	}

	private function setStartTableBody($a) {
		$this->settings['tbody']['a'] = $a;
	}

	private function setEndTableBody($z) {
		$this->settings['tbody']['z'] = $z;
	}

	private function setStartRow($a) {
		$this->settings['row']['a'] = $a;
	}

	private function setEndRow($z) {
		$this->settings['row']['z'] = $z;
	}

	private function setStartCell($c,$a) { 
		$this->settings['cell'][$c]['a'] = $a; 
	}

	private function setEndCell($c, $z) {
		$this->settings['cell'][$c]['z'] = $z;
	}

	private function setFormatting($a) {
		$this->settings['applied_formatting'] = $a;
	}

	private function setNewline($a) {
		$this->settings['newline'] = $a;
	}

	private function setNoResult($a) {
		$this->settings['noresult'] = $a;
	}
	
}


?>