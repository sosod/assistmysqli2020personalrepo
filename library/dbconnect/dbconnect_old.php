<?php
require_once( "database.php" );
/** 
	connects to the database and create function to ececute queries
	@author 	: admire
**/
class DBConnect extends Database
{
	
	protected $db;
	public $helper;
	const ACTIVE   = 1;
	const DELETE   = 2;
	const DEFAULTS = 4;
	function __construct()
	{
		Database::getInstance();
		$this->helper = new ASSIST_DB();
        parent::__construct();
		//echo "Coming here to connect to the databse <br /><br />";
	}
	
    function query( $query )
	{
       $resource = mysqli_query( $this->get_db_resource(),$query ) or die(mysqli_error($this->get_db_resource()));
       return $resource;        
    }
	
	    
    function get( $query )
	{
	   $query = str_replace("#", "".$_SESSION['dbref']."", $query);
	   $data = $this->helper->mysql_fetch_all($query);
	   /*$resource = $this -> query( $query );
       $data = array();
	   if( is_resource( $resource) )
       while($result = mysqli_fetch_assoc( $resource )){
            $data[] = $result;
       }*/
       return $data;          
    }
	// show a tables colums
	function describe( $table )
	{
		$results = $this->get("DESCRIBE ".$_SESSION['dbref']."_".$table."");
		$columns  =  array();
		foreach( $results as $key => $value){
			$columns[] = $value['Field'];
		}
		return $columns;
	}

	function debug( $query )
	{
		echo str_replace("#", "".$_SESSION['dbref']."", $query);
	} 

    function getRow( $query ){	
	  	$query = str_replace("#", "".$_SESSION['dbref']."", $query);
        $resource = $this->helper->mysql_fetch_one($query);
		/*$resource = $this -> query( $query );
		if(is_resource($resource))
		return mysqli_fetch_assoc( $resource ) ;*/
        return $resource;
	}
	
	
    function getWhere( $table, $where )
	{
       $query = "SELECT * FROM ".$_SESSION['dbref']."_".$table." WHERE $where";
       $resource = $this -> query( $query );
       $data = array();
       while($result = mysqli_fetch_assoc( $resource )){
            $data[] = $result;
       }
       return $data;          
    }
	
	function insert( $table, $insert_array ) 
	{
		$query = "INSERT INTO ".$_SESSION['dbref']."_".$table." SET ";
		foreach( $insert_array as $field => $value ) {
			$query .= $field." = '".mysqli_real_escape_string($this->get_db_resource(), trim($value) )."' ," ;
		}
		$query = rtrim( $query , ',' ) ;
        $mar = $this->helper->db_insert($query);//$this -> query( $query );
        return $mar;//$this -> affected_rows();
	}
	
	function update( $table, $insert_array, $where ) 
	{
		$query = "UPDATE ".$_SESSION['dbref']."_".$table." SET ";
		foreach( $insert_array as $field => $value ) {
			$query .= $field." = '".mysqli_real_escape_string( $this->get_db_resource(),trim($value) )."' ," ;
		}
		$query = rtrim( $query, ',' ) ;
		$query .= "WHERE ".$where;
		//$this -> query( $query );
        $mar = $this->helper->db_update($query);
		return $mar;//$this -> affected_rows();
	}
    
	
	function insertForCompany( $table, $insert_array ) 
	{
		$query = "INSERT INTO assist_".$this -> cmpcode."_".$table." SET ";
		foreach( $insert_array as $field => $value ) {
			$query .= $field." = '".mysqli_real_escape_string($this->get_db_resource(), trim($value) )."' ," ;
		}
		$query = rtrim( $query , ',' ) ;
        $mar = $this->helper->db_insert($query);//$this -> query( $query );
        return $mar;//$this -> affected_rows();
	}
	
	function updateForCompany( $table, $insert_array, $where ) 
	{
		$query = "UPDATE assist_".$this -> cmpcode."_".$table." SET ";
		foreach( $insert_array as $field => $value ) {
			$query .= $field." = '".mysqli_real_escape_string($this->get_db_resource(), trim($value) )."' ," ;
		}
		$query = rtrim( $query, ',' ) ;
		$query .= "WHERE ".$where;
        $mar = $this->helper->db_update($query);//$this -> query( $query );
        return $mar;//$this -> affected_rows();
	}
		
    function affected_rows()
	{
	    $mar = mysqli_affected_rows($this->get_db_resource() );
	    $this->db_close();
        return $mar;
    }
	
	function insertedId()
	{
        $mid = mysqli_insert_id($this->get_db_resource());
        $this->db_close();
        return $mid;
	}
    public function db_close() {
        if(is_resource($this->get_db_resource())) {
            mysqli_close($this->get_db_resource());
        }
    }
}
?>