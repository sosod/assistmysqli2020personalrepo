<?php
/** ADMIRE ASSIST HELPER CLASS
Created on: 06 September 2021
Created by: Sondelani Dumalisile
Updated on: 06 September 2021 [SD]



General functions from root inc_assist.php used by Admires modules

 **/

class ADMIRE_ASSIST_HELPER {

	function __construct() {

	}
	function JSdisplayResultObj($txt="",$icon="") {
		//USED BY ADMIRE MODULES
		$output = '';
		$output .= '<div id="display_objresult">
		<p>
			<span id="display_result_icon"></span>
			<span id="display_result_text">';
		$output .= $txt;
		$output .= '</span>
		</p>
	</div>
	<script type="text/javascript">

			function jsDisplayResult( result, icon, text )
			{
				$("#display_result_text").html("");
				$("#display_objresult").attr("class", "").show();
				$("#display_result_icon").attr("class", "");
				//$("#display_objresult").addClass("message");			
				$("#display_objresult").addClass("ui-widget ui-state-"+result);
				$("#display_objresult").css({"margin":"5px 0px 10px 0px","padding":"0 .3em"});
				$("#display_result_icon").addClass("ui-icon");				
				if(text != "") {
					switch(icon) {
						case "ok":
							$("#display_result_icon").addClass("ui-icon-check"); 
							break;
						case "info":
							$("#display_result_icon").addClass("ui-icon-info"); 
							break;
						case "error":	
							$("#display_result_icon").addClass("ui-icon-closethick"); 
						break;
						default:
							$("#display_result_icon").addClass("ui-icon-"+icon); 
						break;
					}				
					$("#display_result_icon").css({"float":"left","margin-right":".3em"});
					$("#display_result_text").html( text );
					$("html, body").animate({
					    scrollTop: 0,
					    scrollLeft: 50
					}, 800);
				} else {
					window.location.hash = "#display_objresult";
					$("#display_result_text").html( "" );
				}

}';
		echo $output;
		if(strlen($icon)>0 && strlen($txt)>0) {
			echo "jsDisplayResult('".$icon."','".$icon."','".$txt."');";
		}
		echo "</script>";
	}

	function displayAuditLogLink( $table , $important) {
		echo "<span id=\"_disp_audit_log\" style=\"cursor: pointer;\" class=\"float color\"><a href=\"#\" id=\"table_$table\" class=\"audit_log\"><img src=\"/pics/tri_down.gif\" style=\"vertical-align: middle; border-width: 0px;\"><span style=\"text-decoration: underline;\">Display Activity Log</span></a></span>";
		echo "<input type=\"hidden\" name=\"importance\" id=\"importance\" value=\"$important\" />";
		echo '<script language="javascript">
			$(function(){				
				$(".audit_log").click(function(){
					var id   = this.id; 
					var data = {};
					data.field = $(".logid").attr("id");
					data.value = $(".logid").val();
					data.table = this.id;
					$("#"+this.id+" img").prop("src", "/pics/tri_up.gif");
					
					if( $("#audit_log_"+id).length == 0 || $("#audit_log_"+id).is(":hidden") ){		
						$("#"+this.id+" img").prop("src", "/pics/tri_up.gif");			
						$("#"+this.id+" span").text("Hide Activity Log");
						$("body").append($("<div />",{id:"audit_log_"+id}).css({"clear":"both"})
								   .append($("<table />",{id:"table_"+id}).css({"display":"block"}))
							)
					} else {
						$("#"+this.id+" img").prop("src", "/pics/tri_down.gif");
						$("#"+this.id+" span").text("Display Activity Log");
						$("#audit_log_"+id).remove();	
					}					
					$.post("../../library/process_log.php",{ data : data },function( auditlogData ){
						//$("#aduit_log_"+id).html("");
						//$("body").append( $("<div />",{id:"aduit_log_"+id}).css({"clear":"both"})
											//.append($("<table />",{id:"table_"+id}).css({"display":"block"}))
										//)										
						var importance  = $("#importance").val();
						
						$("#table_"+id).append($("<tr />")
							.append($("<th />",{html:"Date/Time"}).addClass((importance == true ? "log"  : "")))
							.append($("<th />",{html:"User"}).addClass((importance == true ? "log"  : "")))
							.append($("<th />",{html:"Change Log"}).addClass((importance == true ? "log"  : "")))
							.append($("<th />",{html:"Status"}).addClass((importance == true ? "log"  : "")).css({"display":(importance == true ? "none"  : "table-cell")}))
						)
						if( $.isEmptyObject(auditlogData) ){
							$("#table_"+id).addClass("noborder").append($("<tr />")
												.append($("<td />",{colspan:(importance == true ? 3  : 4), html:"No log yet"}))
							)
						} else {
							$.each( auditlogData, function( index, values ){
								$("#table_"+id).addClass("noborder").append($("<tr />")
									.append($("<td />",{html:values.date}))
									.append($("<td />",{html:values.user}))
									.append($("<td />",{html:values.changes}))
									.append($("<td />",{html:values.status}).css({"display":(importance == true ? "none"  : "table-cell")}))		
								)							
							});
						}

					},"json");
					return false;					
				});
			});
		</script>';
	}
}