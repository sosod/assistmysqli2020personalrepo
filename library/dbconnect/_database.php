<?php
/**
	Create the databse instance and connects to the databse
	@author 	: admire
**/
class Database
{
	private static $instance;
	/**
		Database resource
	**/
	protected $dbResource = NULL;
	/**
		Database host
	**/
	protected $dbHost 	  = "localhost"; 
	/**
		Database name
	**/
	protected $dbName 	  = ""; 
	/**
		Database password
	**/
	protected $dbPassword = "DevIgn1t3";
	/**
		Database username
	**/
	protected $dbUser     = "dev_ignite";
	
	private function __construct()
	{
		$this -> dbName = "dev_i".$_SESSION['cc'];
		try
		{
			if(!$this -> dbResource = mysql_connect( $this -> dbHost, $this -> dbUser, $this -> dbPassword))
			{
				throw new Exception("An error occured connecting to the databse ".mysql_error()."\r\n host => ".$this -> dbHost." user => ".$this -> dbUser." Password => ".$this -> dbPassword );
			}
			if( !mysql_select_db( $this -> dbName, $this -> dbResource) )
			{
				throw new Exception("An error selection the specified database ".mysql_error()."\r\n\n");
			}	
		} catch( Exception $e )
		{
			echo "Error => ".$e -> getMessage();
		}
	}
	
	//set the databse name
	function setDbName( $dbname = "" )
	{
		$this -> dbName = $dbname;
	}
	 
	 //set the username for the database
	function setDbUser( $dbuser = "" ) 
	{
		$this -> dbUser  = $dbuser;
	}
	
	//set the password of the database
	function setDbPassword( $dbpass = "" )
	{
		$this -> dbPassword = $dbpass;
	}
	//set the databse host
	function setDbHost( $dbhost = "" )
	{
		$this -> dbHost = $dbhost;
	}
	
	
	//create an instance of the database
	public static function getInstance()
	{
		if(! self::$instance ) {
			self::$instance = new Database();
		}
		return self::$instance;
	}
	
}

?>