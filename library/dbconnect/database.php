<?php
/**
	Create the databse instance and connects to the databse
	@author 	: admire
 *
 * Edited by : Sondelani Dumalisile - During standardisation
 * Comment : To allow new server compatibility, update depricated functions
 * Date: 13 May 2021
**/
if(strpos($_SERVER['HTTP_HOST'],"localhost")===false && strpos($_SERVER['HTTP_HOST'],"action4udev")===false) {
    error_reporting(0);
}else{error_reporting(-1);}
class Database
{
	private static $instance;
	/**
		Database resource
	**/
	protected $dbResource = NULL;
	/**
		Database host
	**/
	protected $dbHost 	  = "localhost"; 
	/**
		Database name
	**/
	protected $dbName 	  = ""; 
	/**
		Database password
	**/
	protected $dbPassword = "ign92054u";
	/**
		Database username
	**/
	protected $dbUser     = "ignittps_ignite4";
    protected $db_prefix;
	protected $db_error_to = "igniteassistdev@gmail.com";
	
	protected $db_error_from = "assist@ignite4u.co.za";
	
	public function __construct()
	{
        $this->db_prefix = ($this->isLocalRepository() ? "adev" : "testdev");
		$this -> dbName = $this->db_prefix."_i".$_SESSION['cc'];

        $this->dbUser = ($this->isLocalRepository() ? "root" : "ignittps_ignite4");
        $this->dbPassword = ($this->isLocalRepository() ? "" : "ign92054u");

		try
		{
			if(!$this -> dbResource = mysqli_connect( $this -> dbHost, $this -> dbUser, $this -> dbPassword))
			{
					mail($this->db_error_to,"DB CON error","An error occured connecting to the database ".mysqli_error($this->dbResource).chr(10)." host => ".$this -> dbHost." user => ".$this -> dbUser." Password => ".$this -> dbPassword ,"From: ".$this->db_error_from."\r\nContent-type: text/html; charset=us-ascii");
					throw new Exception("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered a fatal error and cannot continue.<br />An email has been sent to Ignite Reporting Systems (Pty) Ltd notifying them of this error.</p>");

			}
			if( !mysqli_select_db($this -> dbResource, $this -> dbName) )
			{
					mail($this->db_error_to,"DB error","Error selecting database ".mysqli_error($this->dbResource),"From: ".$this->db_error_from."\r\nContent-type: text/html; charset=us-ascii");
					throw new Exception("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered a fatal error and cannot continue.<br />An email has been sent to Ignite Reporting Systems (Pty) Ltd notifying them of this error.</p>");
			}	
		} catch( Exception $e )
		{
			echo $e -> getMessage();
			exit(0);
		}
	}
	
	//set the databse name
	function setDbName( $dbname = "" )
	{
		$this -> dbName = $dbname;
	}


    public function get_db_resource(){
        return $this->dbResource;
    }
	 
	 //set the username for the database
	function setDbUser( $dbuser = "" ) 
	{
		$this -> dbUser  = $dbuser;
	}
	
	//set the password of the database
	function setDbPassword( $dbpass = "" )
	{
		$this -> dbPassword = $dbpass;
	}
	//set the databse host
	function setDbHost( $dbhost = "" )
	{
		$this -> dbHost = $dbhost;
	}
	
	
	//create an instance of the database
	public static function getInstance()
	{
		if(! self::$instance ) {
			self::$instance = new Database();
		}
		return self::$instance;
	}

    public function isLocalRepository(){
        $isLocalRepository = false;
        if(isset($_SERVER['APPLICATION_ENV']) && strpos($_SERVER['APPLICATION_ENV'],"development")!==false){
            $isLocalRepository = true;
        }
        return $isLocalRepository;
    }
	
}

?>