var AssistForm = {

	serialize : function($form) {
		//alert("serializing the form!");
		//foreach form element: check if text input & encode if necessary
		$formchild = $form.find('textarea, input:text');
		$formchild.each(function() {
			var v = $(this).val();
			//alert($(this).prop("id")+" :: "+v);
			//v = AssistString.code(v);
			v = AssistString.code(v);
			v = v.replace(/\u20ac/g, '&euro;');
			//alert($(this).prop("id")+" :: "+v);
			$(this).val(v);
		});
		
		var data = $form.serialize(); 
		$formchild.each(function() {
			var v = $(this).val();
			//alert($(this).prop("id")+" :: "+v);
			v = v.replace('&euro;', '\u20ac');
			v = AssistString.decode(v);
			//alert($(this).prop("id")+" :: "+v);
			$(this).val(v);
		});
		//return serialised data
		return data; 
	},
	codeFormInputs : function($form) {
		//alert("serializing the form!");
		//foreach form element: check if text input & encode if necessary
		$formchild = $form.find('textarea, input:text');
		$formchild.each(function() {
			var v = $(this).val();
			//alert($(this).prop("id")+" :: "+v);
			//v = AssistString.code(v);
			v = AssistString.code(v);
			v = v.replace(/\u20ac/g, '&euro;');
			//alert($(this).prop("id")+" :: "+v);
			$(this).val(v);
		});
		
		return true; 
	},
	decodeFormInputs : function($form) {
		//alert("serializing the form!");
		//foreach form element: check if text input & encode if necessary
		$formchild = $form.find('textarea, input:text');
		$formchild.each(function() {
			var v = $(this).val();
			//alert($(this).prop("id")+" :: "+v);
			v = v.replace('&euro;', '\u20ac');
			v = AssistString.decode(v);
			//alert($(this).prop("id")+" :: "+v);
			$(this).val(v);
		});
		
		return true; 
	},
	
	cereallies : function($form2){
		return $form2;
	},

	nonFormSerialize : function($me) {
		//alert("serializing the form!");
		//foreach form element: check if text input & encode if necessary
		$codechild = $me.find('textarea, input:text');
		$inputchild = $me.find('textarea, input:text, input:hidden, select');
		$codechild.each(function() {
			var v = $(this).val();
			//alert($(this).prop("id")+" :: "+v);
			
			//v = AssistString.code(v);
			v = AssistString.code(v);
			v = v.replace(/\u20ac/g, '&euro;');
			//alert($(this).prop("id")+" :: "+v);
			$(this).val(v);
		});
		
		var data = "";
		$inputchild.each(function() {
			data+="&"+$(this).prop("name")+"="+encodeURIComponent($(this).val());
		});
		data = AssistString.substr(data,1);
		//var data = $form.serialize(); 
		$codechild.each(function() {
			var v = $(this).val();
			//alert($(this).prop("id")+" :: "+v);
			v = AssistString.decode(v);
			v = v.replace('&euro;', '\u20ac');
			//alert($(this).prop("id")+" :: "+v);
			$(this).val(v);
		});
		//return serialised data
		return data; 
	},

};