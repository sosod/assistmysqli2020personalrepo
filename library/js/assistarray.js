var AssistArray = {
    
	array_pop : function(inputArr) {
	  //  discuss at: http://phpjs.org/functions/array_pop/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //    input by: Brett Zamir (http://brett-zamir.me)
	  //    input by: Theriault
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Brett Zamir (http://brett-zamir.me)
	  //        note: While IE (and other browsers) support iterating an object's
	  //        note: own properties in order, if one attempts to add back properties
	  //        note: in IE, they may end up in their former position due to their position
	  //        note: being retained. So use of this function with "associative arrays"
	  //        note: (objects) may lead to unexpected behavior in an IE environment if
	  //        note: you add back properties with the same keys that you removed
	  //   example 1: array_pop([0,1,2]);
	  //   returns 1: 2
	  //   example 2: data = {firstName: 'Kevin', surName: 'van Zonneveld'};
	  //   example 2: lastElem = array_pop(data);
	  //   example 2: $result = data
	  //   returns 2: {firstName: 'Kevin'}
	
		var key = '',
		lastKey = '';
		
		if (inputArr.hasOwnProperty('length')) {
			// Indexed
			if (!inputArr.length) {
				// Done popping, are we?
				return null;
			}
			return inputArr.pop();
		} else {
			// Associative
			for (key in inputArr) {
				if (inputArr.hasOwnProperty(key)) {
					lastKey = key;
				}
			}
			if (lastKey) {
				var tmp = inputArr[lastKey];
				delete(inputArr[lastKey]);
				return tmp;
			} else {
				return null;
			}
		}
	},
	end : function(arr) {
	  //  discuss at: http://phpjs.org/functions/end/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Legaev Andrey
	  //  revised by: J A R
	  //  revised by: Brett Zamir (http://brett-zamir.me)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //        note: Uses global: php_js to store the array pointer
	  //   example 1: end({0: 'Kevin', 1: 'van', 2: 'Zonneveld'});
	  //   returns 1: 'Zonneveld'
	  //   example 2: end(['Kevin', 'van', 'Zonneveld']);
	  //   returns 2: 'Zonneveld'
	
		this.php_js = this.php_js || {};
		this.php_js.pointers = this.php_js.pointers || [];
		var indexOf = function(value) {
			for (var i = 0, length = this.length; i < length; i++) {
				if (this[i] === value) {
					return i;
				}
			}
			return -1;
		};
		// END REDUNDANT
		var pointers = this.php_js.pointers;
		if (!pointers.indexOf) {
			pointers.indexOf = indexOf;
		}
		if (pointers.indexOf(arr) === -1) {
			pointers.push(arr, 0);
		}
		var arrpos = pointers.indexOf(arr);
		if (Object.prototype.toString.call(arr) !== '[object Array]') {
			var ct = 0;
			var val;
			for (var k in arr) {
				ct++;
				val = arr[k];
			}
			if (ct === 0) {
				return false; // Empty
			}
			pointers[arrpos + 1] = ct - 1;
			return val;
		}
		if (arr.length === 0) {
			return false;
		}
		pointers[arrpos + 1] = arr.length - 1;
		return arr[pointers[arrpos + 1]];
	}

    
};