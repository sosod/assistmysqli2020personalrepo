<?php 
	/* SETUP VARIABLES */
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",
		"dir",$table_fld."subid",
		$table_fld."annual",
		$table_fld."revised",
		$table_fld."repcate",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
		$table_fld."pmsref",
	);
	//$head0['dir'] = $mheadings['dir'][0];
	$head = $mheadings[$section];


	$src = isset($_REQUEST['src']) ? $_REQUEST['src'] : "GENERATOR";
	if($src=="GRAPH" && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
		$_REQUEST['fields'] = array(
            0 => "top_dirid",
            1 => "top_pmsref",
            2 => "top_gfsid",
            3 => "top_natoutcomeid",
            4 => "top_natkpaid",
            5 => "top_mtas",
            6 => "top_idp",
            7 => "top_munkpaid",
            8 => "top_value",
            9 => "top_unit",
            10 => "top_risk",
            11 => "top_wards",
            12 => "top_area",
            13 => "top_ownerid",
            14 => "top_baseline",
            15 => "top_poe",
            16 => "top_pyp",
            17 => "top_annual",
            18 => "top_revised",
            19 => "top_calctype",
            20 => "results",
            21 => "tr_perf",
            22 => "tr_correct",
            23 => "tr_dept",
            24 => "tr_dept_correct",
		);
		//$_REQUEST['r_from'] => from graph report
		//$_REQUEST['r_to'] => from graph report
		$_REQUEST['r_calc'] = "ytd";
		$_REQUEST['summary'] = "Y";
		//$_REQUEST['filter']['group_by_filter'] = array([0]=>#) => from graph where applicable
		//$_REQUEST['filter']['result'] = # => from graph
		//$_REQUEST['groupby'] => from graph
		$_REQUEST['filter']['top_wards'] = array("ANY");
		$_REQUEST['filter']['top_area'] = array(1);
		$_REQUEST['sort'] = array(
            0 => "top_dirid",
            1 => "top_pmsref",
            2 => "top_repkpi",
            3 => "top_gfsid",
            4 => "top_natoutcomeid",
            5 => "top_natkpaid",
            6 => "top_mtas",
            7 => "top_idp",
            8 => "top_munkpaid",
            9 => "top_value",
            10 => "top_unit",
            11 => "top_risk",
            12 => "top_ownerid",
            13 => "top_baseline",
            14 => "top_poe",
            15 => "top_pyp",
		);	
		$_REQUEST['output'] = "display";
		$_REQUEST['rhead'] = "";
	}

	if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
			include("report_kpi_process.php");
		} else {
			include("report_kpi_generate.php");
		}	
?>