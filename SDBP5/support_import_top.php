<?php 
$page_title = "Top Layer";
$import_section = "TOP";
$get_headings = array("TOP","TOP_R","TOP_F");
$section = "TOP";
$get_lists = true;
include("support_import_headings.php");
include("inc/header.php"); 

$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > Top Layer", $self);

$valid_numbers = array("0","1","2","3","4","5","6","7","8","9",".");

//arrPrint($lists['years']);

//GET ALL LIST VALUES
$listoflists = array(
	'dir'=>				array('name'=>"Directorate",'data'=>array()),
	'subdir'=>			array('name'=>"Sub-Directorate",'data'=>array()),
	'list_gfs'=>		array('name'=>"GFS Classification",'data'=>array()),
	'list_munkpa'=>		array('name'=>"Municipal KPA",'data'=>array()),
	'list_natkpa'=>		array('name'=>"National KPA",'data'=>array()),
	'list_natoutcome'=>	array('name'=>"National Outcome",'data'=>array()),
	//'list_idp_kpi'=>	array('name'=>"Departmental SDBIP: IDP Objective",'data'=>array()),
	'list_idp_top'=>	array('name'=>"Top Layer: IDP Objective",'data'=>array()),
	'list_area'=>		array('name'=>"Area",'data'=>array()),
	'list_wards'=>		array('name'=>"Ward",'data'=>array()),
	'list_owner'=>		array('name'=>"KPI Owner / Driver",'data'=>array()),
	//'list_fundsource'=>	array('name'=>"Funding Source",'data'=>array()),
	'list_calctype'=>	array('name'=>"Calculation Type",'data'=>array()),
	'list_targettype'=>	array('name'=>"Target Type",'data'=>array()),
	'list_ndp'=>		array('name'=>"NDP Objectives",'data'=>array()),
	'list_pdo'=>		array('name'=>"Pre-Determined Objectives",'data'=>array()),
	'list_repcate'=>	array('name'=>"Reporting Category",'data'=>array()),
	'list_riskrating'=>	array('name'=>"Risk Rating",'data'=>array()),
);
$sql = "SELECT h_client, h_table FROM ".$dbref."_setup_headings WHERE h_type IN ('LIST','WARDS','AREA','FUNDSRC','TEXTLIST')";
$client_list_headings = mysql_fetch_all($sql);
foreach($client_list_headings as $chead) {
	$htbl = $chead['h_table'];
	$chv = $chead['h_client'];
	if(isset($listoflists[$htbl])) {
		$listoflists[$htbl]['name'] = $chv;
	} elseif(isset($listoflists['list_'.$htbl])) {
		$listoflists['list_'.$htbl]['name'] = $chv;
	}
}
$owner = array();
$wa = array(
	'top_wards'=>array(),
	'top_area'=>array(),
	'top_repcate'=>array(),
);
foreach($listoflists as $tbl => $l) {
	$rs = getRS("SELECT * FROM ".$dbref."_".$tbl." WHERE active = true");
	while($row = mysql_fetch_assoc($rs)) {
		if($tbl!="list_calctype") {
			$listoflists[$tbl]['data'][$row['id']] = $row;
		} else {
			$listoflists[$tbl]['data'][$row['code']] = $row;
		}
		switch($tbl) {
			case "list_owner": $owner[strtolower(decode($row['value']))] = $row['id']; break;
			case "list_wards": 
				if(strtolower($row['value'])=="all") {
					$wa['top_wards']['ALL'] = $row['id'];
				} else {
					$wa['top_wards'][$row['code']] = $row['id'];
				}
				break;
			case "list_area":	$wa['top_area'][$row['id']] = $row['id']; break;
			case "list_repcate":	$wa['top_repcate'][$row['id']] = $row['id']; break;
		} 
	}
}
//arrPrint($listoflists);
//arrPrint($wa['area']);
//arrPrint($listoflists['list_wards']);
//arrPrint($wa['wards']);
function valError($v,$e) {
	return $v."<br /><b>-> ".$e."</b>";
}
function valList($v,$e) {
	return $v."<br />=> ".$e;
}

$file_error = array("error","An error occurred while trying to import the file.  Please go back and try again.");
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VIEW";

$onscreen = "";

switch($act) {
	case "RESET":
		$r_sql = array();
		$r_sql[] = "TRUNCATE ".$dbref."_top";
		$r_sql[] = "TRUNCATE ".$dbref."_top_results";
		$r_sql[] = "TRUNCATE ".$dbref."_top_wards";
		$r_sql[] = "TRUNCATE ".$dbref."_top_area";
		$r_sql[] = "TRUNCATE ".$dbref."_top_forecast";
		foreach($r_sql as $sql) {	
			$mnr = db_update($sql);
		}
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = '".$import_section."'");
		$import_status[$import_section] = false;
		$result = array("ok","All $page_title tables successfully reset.");
		logImport($import_log_file, "Top Layer > Reset tables", $self);
		break;
}

?>		<style type=text/css>
		table th {
			vertical-align: top;
		}
		.dark-info {
			background: #FE9900 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		.dark-error {
			background: #CC0001 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		th.ignite { background-color: #555555; }
		th.doc { background-color: #999999; }
		</style>
<?php
logImport($import_log_file, "Top Layer > ".$act, $self);
$save_echo_filename = $import_section."_".date("YmdHis")."_".$act.".html";
switch($act) {
	case "VIEW": case "RESET": case "GENERATE":
		?>
		<script>
			$(function() {
				$(":button").click(function() {
					var id = $(this).prop("id");
					switch(id) {
						case "generate": document.location.href = 'support_import_generate.php?i=<?php echo $import_section; ?>'; break;
						case "reset": if(confirm("This will delete any existing data!\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=RESET'; } break;
					}
				});
				$("#isubmit:button").click(function() {
					var ifl = $("#ifl").prop("value");
					if(ifl.length > 0) 
						$("#list").submit();
					else
						alert("Please select a file for import.");
				});
			});
		</script>
		<?php displayResult($result); ?>
		<p>In order to generate the CSV files necessary for the import, simply go to each worksheet in the SDBIP document and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'.
		<br /><b>PLEASE ENSURE THAT THE COLUMNS MATCH THOSE AS GIVEN ON THE IMPORT PAGE FOR EACH SECTION!!!</b></p>
		<?php
$i = 1;
			?>
			<form id=list action="<?php echo $self; ?>" method=post enctype="multipart/form-data">
				<input type=hidden name=act value=PREVIEW />
				<table>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Generate the CSV file:
							<br />+ <input type=button value=Generate id=generate /> the CSV template here and copy the data into the sheet from the SDBIP template  OR
							<br />+ Open the SDBIP template, go to the Lists sheet, click File > Save As & change the File Type option to Comma-delimited (*.csv).
							<br />&nbsp;<br />Please ensure that the columns match up to those given below.&nbsp;
						</td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reset the tables to delete previously loaded information.&nbsp;<span class=float><input type=button value="Reset Tables" id=reset class=idelete /></span></td>
					</tr>
					<?php if(!$import_status['TOP']) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Select the CSV file and click the Import button. &nbsp;&nbsp;<input type=file name=ifile id=ifl />&nbsp;<span class=float><input type=button id=isubmit value=Import class=isubmit /></span></td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Review the information.  If the information is correct, click the Accept button.
						<br /><b>NO DATA WILL BE IMPORTED UNTIL THE FINAL ACCEPT BUTTON HAS BEEN CLICKED!!</b></td>
					</tr>
					<?php } ?>
				</table>
			</form>
			<?php displayGoBack("support_import.php");
			
			drawCSVColumnsGuidelines("top_id",$target_text);
			
		break;	//switch act case view/reset/generate
	case "PREVIEW":
		$onscreen.= "<h1 class=centre>$page_title Preview</h1><p class=centre>Please click the \"Accept\" button at the bottom of the page<br />in order to finalise the import of the list items.</p>";
		$onscreen.= "<p class=centre>Any errors highlighted in <span class=dark-error>red</span> are errors that prevent the Top Layer from importing (fatal errors).<br />The \"Accept\" button will not be available until all of these errors have been corrected.</p>";
		$onscreen.= "<p class=centre>Any kpi data cells highlighted in <span class=dark-info>orange</span> are where the value associated with the given list reference does not match the value given in the next cell.<br />These errors do not prevent the Top Layer from being imported (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Any kpi target cells highlighted in <span class=dark-info>orange</span> are where target type is percentage and the value in the cell is 1 or less which is potentially an incorrectly captured 100%.<br />These errors do not prevent the Top Layer from being imported (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Please remember, if you get any \"Too long\" errors, that any non-alphanumeric characters in text fields are converted a special code to allow them to be uploaded to the database.
		<br />This code is generally 6 characters in length and counts towards the overall length of the text field.</p>
		<p class=center><span class=iinform>Please note:</span> The top heading row displays the headings from the module as per Setup > Defaults > Headings.
		<br />The second heading row displays the headings from the import file.
		<br />Please ensure that they match up to ensure that the correct data is being imported into the correct field.</p>";
	case "ACCEPT":
		if($act!="PREVIEW") {
			$accept_text = "<h1 class=centre>Processing...</h1><p class=centre>The import is being processed.<br />Please be patient.  You will be redirected back to the Support >> Import page when the processing is complete.</p>";
			$onscreen.= $accept_text;
			echo $accept_text;
		}
		$onscreen.= "<form id=save method=post action=\"$self\">";
		$onscreen.= "<input type=hidden name=act value=ACCEPT />";
		$err = false;
		$file_location = $modref."/import/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$modref."/import/";
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				logImport($import_log_file, "Top Layer > ERROR > $file doesn't exist", $self);

				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						logImport($import_log_file, "Top Layer > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [Too big]", $self);
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						logImport($import_log_file, "Top Layer > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [No file found]", $self);
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						logImport($import_log_file, "Top Layer > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"], $self);
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					logImport($import_log_file, "Top Layer > ERROR > Invalid file extension - ".$ext, $self);
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi").".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
					logImport($import_log_file, "Top Layer > $file uploaded", $self);
				}
			}
		}
		//READ FILE
		if(!$err) {
			$onscreen.= "<input type=hidden name=f value=".(isset($filen) ? $filen : $_REQUEST['f'])." />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);

			if(count($data)>2) {
				$doc_headings = $data[0];
				unset($data[0]); unset($data[1]);
				$columns[0] = array("ID","Directorate","","Reporting KPI","PMS Ref","GFS Classification","","National Outcome","","National KPA","","MTAS Indicator","IDP Objective","","Municipal KPA","","KPI","Unit of Measurement","Risk","Wards","Area","Program Driver","Baseline","POE","Past Year Performance","KPI Calculation Type","","KPI Target Type","","Annual Target","Revised Target","","Q1","Q2","Q3","Q4","","2012/2013","2013/2014","2014/2015","2015/2016");
				$columns[1] = array("Ref","Dir Ref","Dir Name","40 characters","Y / N","GFS Ref","GFS Value","Ref","Value","Ref","Value","200 characters","Ref","Value","Ref","Value","200 characters","200 characters","200 characters","Mun Ref ;","Ignite Ref ;","Value","200 characters","200 characters","100 characters","Code","Value","Ref","Value","Number","Number","","Number","Number","Number","Number","","Number","Number","Number","Number");
				$colspanned[2] = array(1,5,7,9,11,13,15,17,22,32,34);
				$colspanned[3] = array();
				
				$listinfo = array(
					'top_dirid' 		=> array('tbl' => "dir", 'other' => 2),
					'top_gfsid' 		=> array('tbl' => "list_gfs", 'other' => 6),
					'top_natoutcomeid' 	=> array('tbl' => "list_natoutcome", 'other' => 8),
					'top_natkpaid' 		=> array('tbl' => "list_natkpa", 'other' => 10),
					'top_pdoid' 		=> array('tbl' => "list_pdo", 'other' => 12),
					'top_ndpid' 		=> array('tbl' => "list_ndp", 'other' => 14),
					'top_munkpaid' 		=> array('tbl' => "list_munkpa", 'other' => 18),
					'top_riskratingid'	=> array('tbl' => "list_riskrating", 'other' => 23),
					'top_idp' 			=> array('tbl' => "list_idp_top", 'other' => 16),
					'top_targettype'	=> array('tbl' => "list_targettype", 'other'=>35),
					'top_calctype'		=> array('tbl' => "list_calctype", 'other'=>33),
					'top_wards'			=> array('tbl' => "list_wards"),
					'top_area'			=> array('tbl' => "list_area"),
					'top_repcate'		=> array('tbl' => "list_repcate"),
				);
				$required = array(0,1,3,5,7,9,11,13,15,17,19,22,26,32,34,36);

/*$sql = "SELECT sh.h_client, shs.field, (shs.c_required + shs.i_required) AS req, c_maxlen
		FROM  ".$dbref."_setup_headings_setup shs
		INNER JOIN ".$dbref."_setup_headings sh ON sh.h_id = shs.head_id
		WHERE shs.section =  'TOP'";				
$client_headings = mysql_fetch_all_fld($sql,"field");*/ //arrPrint($mheadings);
				$onscreen.= "<table>";
					//$onscreen.= "<tr><th rowspan=2 style='vertical-align: middle;'> ? </th>";
/*		$cdata = array();
		for($i=0;$i<count($import_columns);$i++) {
			if(isset($cells[$i])) {
				$col = $import_columns[$i];
				$fld = isset($col['fld']) ? $col['fld'] : "";
				if(strlen($fld)==0) {
					$txt = "";
				} elseif($fld==strtolower($import_section)."_id") { 
					$txt = $col['txt']; 
				} elseif($i>=$forecast2_start) { 
					$txt = ($i%2==1 ? "" : $col['txt']);
				} elseif($i>=$forecast_start) { 
					$txt = $col['txt']." Forecast";
				} elseif($i>=$target_start) { 
					$txt = isset($col['txt']) && strlen($col['txt'])>0 ? $col['txt'] : "";
				} else {
					$txt = isset($mheadings[$import_section][$fld]['h_client']) ? $mheadings[$import_section][$fld]['h_client'] : "";
				}
				$cdata[]="<th class=ignite colspan=".(in_array($i,$colspanned) ? 2 : 1).">".$txt."</th>";
				//$cdata[] = "<th>".$txt.":".$i."</th>";
			} else {
				$txt = "";
			}
		}
		$onscreen.=implode("",$cdata);*/
		$onscreen.=getImportHeadings();
		/*foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 2 : 1;
							if(strlen($c)>0 || $cell>30) {
								if(isset($cells[$cell])) {
									$fld = $cells[$cell];
									if(isset($mheadings['TOP'][$fld]['h_client'])) {
										$c = $mheadings['TOP'][$fld]['h_client'];
									} elseif(isset($mheadings['TOP']['top_'.$fld]['h_client'])) {
										$c = $mheadings['TOP']['top_'.$fld]['h_client'];
									}
								}
								$onscreen.= "<th colspan=$colspan class=ignite >".$c." :: ".$fld."</th>";
							}
						}*/
/*					$onscreen.= "</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							$colspan = in_array($cell,$colspanned[2]) ? 2 : 1;
							if(strlen($c)>0 || $cell>30) {
								$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
							}
						}
					$onscreen.= "</tr>";*/
					
					$t_sql = array();
					$r_sql = array();
					$f_sql = array();
					$refs = array();
					$values = array();
					$all_valid = true;
					$error_count = 0; 
					$info_count = 0;
					foreach($data as $i => $d) {
						$targettype = 0;
						$valid = true;
						$row = array();
						$display = array();
						$val = array(); //echo "<P>".$i.":";
						foreach($cells as $c => $fld) {
							$v = trim($d[$c]); //echo "<br />".$c." => ".$fld." :: ".$v;
							$val[$c][0] = !isset($val[$c][0]) ? 1 : $val[$c][0];
							//DATA VALIDATION HERE
							switch($fld) {
								case "top_id":		//IGNITE REF
									$v = $v*1;
									if(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(in_array($v,$refs)) {	//duplicate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Duplicate reference");
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = $v;
										$refs[] = $v;
									}
									break;
								case "top_dirid":		//DIRECTORATE
								case "top_gfsid":
								case "top_natoutcomeid":
								case "top_natkpaid":
								case "top_pdoid":
								case "top_ndpid":
								case "top_munkpaid":
								case "top_riskratingid":
								case "top_idp":
								case "top_targettype":
								case "top_calctype":
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$targettype = ($fld=="top_targettype") ? 0 : $targettype;

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v) && $fld!="top_calctype") {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
										$targettype = ($fld=="top_targettype") ? $v : $targettype;
									}
									break;
								case "top_pmsref":		//PMS REF
								case "top_mtas":
								case "top_value":
								case "top_unit":
								case "top_risk":
								case "top_baseline":
								case "top_pyp":
								case "top_poe":
									$len = $mheadings['TOP'][$fld]['c_maxlen'];
									$type = $mheadings['TOP'][$fld]['h_type'];
									if(($mheadings['TOP'][$fld]['i_required']+$mheadings['TOP'][$fld]['c_required'])>0 && strlen(trim(code($v))) == 0) {			//required field
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(strlen(trim(code($v)))> $len && $type!="TEXT") {			//too long
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Too long: ".strlen(trim(code($v)))." chars (max ".$len." chars)");
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = trim(code($v));
									}
									break;
								case "top_repkpi": 	//reporting kpi?
									if(strtolower($v)!="yes" && strtolower($v)!="no") {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data");
									} else {
										switch(strtolower($v)) {
											case "yes":
												$row[$fld] = "true";
												break;
											case "no":
												$row[$fld] = "false";
												break;
										}
										$val[$c][1] = $v;
									}
									break; 
								case "top_ownerid":
									$v2 = strtolower($v);
									if(!isset($owner[$v2])) {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data");
									} else {
										$row[$fld] = $owner[$v2];
										$val[$c][1] = valList($v,$owner[$v2]." (".$listoflists['list_owner']['data'][$owner[$v2]]['value'].")");
									}
									break;
								case "top_annual":
								case "top_revised":
								case "target_1":
								case "target_2":
								case "target_3":
								case "target_4":
								case "forecast_1":
								case "forecast_2":
								case "forecast_3":
								case "forecast_4":
								case "forecast_5":
									$v = trim($v);
									if(!is_numeric($v) && strlen($v)>0) {	//invalid number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data: number only");
									} elseif($fld=="top_revised" && $v!=$row['top_annual']) {
										//$valid = 0; 
										$row[$fld] = $v;
										$info_count++;
										$val[$c][0] = 2;
										$val[$c][1] = valError($v,"Doesn't equal Annual Target");
									} else {
										$x = true;
										if(strlen($v)==0) {
											$v = 0;
										} else {
											$v2 = str_split(trim($v));
											foreach($v2 as $z) {
												if(!in_array($z,$valid_numbers)) {
													$x=false;
													break;
												}
											}
										}
										$row[$fld] = $v;
										$val[$c][1] = $v;
										if($targettype==2 && $v <=1 && $v>0) {
											$val[$c][0]=2; $info_count++;
										}
									}
									if($fld=="target_4" && $valid!=0) {
										//KPIcalcResult($values,$ct,$filter,$t)
										$calc = array(
											'target'=>array(3=>$row['target_1'],6=>$row['target_2'],9=>$row['target_3'],12=>$v),
											'actual'=>array(3=>0,6=>0,9=>0,12=>0)
										);
										$row_results = KPIcalcResult($calc,$row['top_calctype'],array(3,6,9,12),"ALL");
										$row['overall'] = $row_results;
										if($row_results['target']!=$row['top_annual']) {
											$val[$c+1][0] = 2; $info_count++;
											$val[$c+1][1] = valError($row_results['target'],"Calculated Overall doesn't match given Annual Target");
										} else {
											$val[$c+1][0] = 1;
											$val[$c+1][1] = "<div class='b center ui-state-ok'>".$row_results['target']."</div>";
										}
									}
									break;
								case "top_wards":
								case "top_area":
								case "top_repcate":
									if(strlen(trim($v))==0) {
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} else {
										$tbl = $listinfo[$fld]['tbl'];
										$row[$fld] = array();
										$val[$c][1] = "";
										$v = strtoupper($v);
										$va = explode(";",$v);
										if($fld=="top_wards" && in_array("ALL",$va)) {
											if(isset($wa[$fld]['ALL'])) {
												$row[$fld][] = $wa[$fld]['ALL'];
												$val[$c][1] = "All";
											} else {
												$val[$c][1] = valError("All","Not found");
												$valid = 0; $error_count++;
												$val[$c][0] = 0;
											}
										} else {
											foreach($va as $a) {
												$a = trim($a);
												if(!checkIntRef($a)) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Invalid reference - number only");
												} elseif(!isset($a,$wa[$fld][$a])) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Reference not found");
												} elseif(!checkIntRef($wa[$fld][$a])) {
													$valid = 0; $error_count++;
													$val[$c][0] = 0;
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").valError($a,"Invalid Ignite Reference found");
												} else {
													$row[$fld][] = $wa[$fld][$a];
													$val[$c][1].= (strlen($val[$c][1])>0 ? "<br />" : "").$a." => ".$listoflists[$tbl]['data'][$wa[$fld][$a]]['value'];
												}
											}
										}
									}
									break;
								default:
									$val[$c][1] = !isset($val[$c][1]) ? $v : $val[$c][1];
									$row[$fld] = $v;
							}
						}//foreach cell (fld)
						//arrPrint($val);
						//arrPrint($row);
						$val['a'] = $valid;
						$onscreen.= "<tr>";
							switch($val['a']) {
								case true:
									$onscreen.= "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									break;
								case false:
								default:
									$onscreen.= "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
							}
							//foreach($cells as $cell => $fld) {
							foreach($import_columns as $cell => $c) {
								if(isset($val[$cell])) {
									$v = $val[$cell];
									switch($v[0]) {
										case 0:
											$onscreen.= "<td class=\"dark-error\">";
											break;
										case 2:
											$onscreen.= "<td class=\"dark-info\">";
											break;
										case 1:
										default:
											$onscreen.= "<td>";
									}
									$onscreen.= $val[$cell][1];
									$onscreen.= "</td>";
								} else {
									$onscreen.= "<td></td>";
								}
							}
						$onscreen.= "</tr>";
						if($valid) {
							$values[$i] = $val;
							//INSERT INTO $dbref_top (top_id,top_dirid,'top_pmsref',top_repkpi,top_gfsid,top_natoutcomeid,top_natkpaid,'top_mtas',top_idp,top_munkpaid,'top_value','top_unit','top_risk',top_ownerid,'top_baseline','top_poe','top_pyp','top_calctype',top_targettype,top_annual,top_revised,active,top_pdoid,top_ndpid,top_riskratingid,top_repcate) VALUES ...
							$t_sql[] = "(".$row['top_id'].",".$row['top_dirid'].",'".$row['top_pmsref']."',".$row['top_repkpi'].",".$row['top_gfsid'].",".$row['top_natoutcomeid'].",".$row['top_natkpaid'].",'".$row['top_mtas']."',".$row['top_idp'].",".$row['top_munkpaid'].",'".$row['top_value']."','".$row['top_unit']."','".$row['top_risk']."',".$row['top_ownerid'].",'".$row['top_baseline']."','".$row['top_poe']."','".$row['top_pyp']."','".$row['top_calctype']."',".$row['top_targettype'].",".$row['top_annual'].",".$row['top_revised'].",true,".$row['top_pdoid'].",".$row['top_ndpid'].",".$row['top_riskratingid'].",';".implode(";",$row['top_repcate']).";')";
							//INSERT INTO $dbref_top_results (tr_id, tr_topid, tr_timeid, tr_target, tr_actual, tr_perf, tr_correct, tr_dept) VALUES ...
							$r_sql[] = "INSERT INTO ".$dbref."_top_results (tr_id, tr_topid, tr_timeid, tr_target, tr_actual, tr_perf, tr_correct, tr_dept) VALUES (null,".$row['top_id'].",3,".$row['target_1'].",0,'','',''), (null,".$row['top_id'].",6,".$row['target_2'].",0,'','',''), (null,".$row['top_id'].",9,".$row['target_3'].",0,'','',''), (null,".$row['top_id'].",12,".$row['target_4'].",0,'','','')";
							//INSERT INTO ".$dbref."_top_forecast (tf_id, tf_topid, tf_listid, tf_active, tf_value) VALUES...
							$r_sql[] = "INSERT INTO ".$dbref."_top_forecast (tf_id, tf_topid, tf_listid, tf_active, tf_value) VALUES (null,".$row['top_id'].",3,true,".$row['forecast_1']."), (null,".$row['top_id'].",4,true,".$row['forecast_2']."), (null,".$row['top_id'].",5,true,".$row['forecast_3']."), (null,".$row['top_id'].",6,true,".$row['forecast_4'].")";
							//INSERT INTO ".$dbref."_top_wards (tw_id, tw_topid, tw_listid, tw_active) VALUES...
							$r_sql[] = "INSERT INTO ".$dbref."_top_wards (tw_id, tw_topid, tw_listid, tw_active) VALUES	(null,".$row['top_id'].",".implode(",true),(null,".$row['top_id'].",",$row['top_wards']).",true)";
							//INSERT INTO ".$dbref."_top_wards (tw_id, tw_topid, tw_listid, tw_active) VALUES...
							$r_sql[] = "INSERT INTO ".$dbref."_top_area (ta_id, ta_topid, ta_listid, ta_active) VALUES	(null,".$row['top_id'].",".implode(",true),(null,".$row['top_id'].",",$row['top_area']).",true)";
						} else {
							$all_valid = false;
						}
					}//foreach data
					
				$onscreen.= "</table>";
				if($act!="ACCEPT") {
					$onscreen.= "<p class=centre>There are $error_count fatal error(s).<br />There are $info_count non-fatal error(s).</p>";
					$onscreen.= "<p class=centre><input type=submit value=\"Accept\" class=isubmit id=submit_me /> <input type=button id=reset_me  value=Reject class=idelete /><p>";
					if(!$all_valid || $error_count > 0) {
						$onscreen.= "<script>$(function() {
							$('#submit_me').prop('disabled',true);
							$('#submit_me').removeClass('isubmit');
							$('#reset_me').click(function() { document.location.href = 'support_import_top.php'; });
						});</script>";
					}
					//echo "<p class=centre>Warning: When you click the Accept button, only those list items with a <span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> will be imported.</p>";
					echo $onscreen;
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, "Top Layer > $act > ".$save_echo_filename, $self);
				} else {
					$z_sql = array();
					$z=0;
					$z_sql[$z] = "INSERT INTO ".$dbref."_top (top_id,top_dirid,top_pmsref,top_repkpi,top_gfsid,top_natoutcomeid,top_natkpaid,top_mtas,top_idp,top_munkpaid,top_value,top_unit,top_risk,top_ownerid,top_baseline,top_poe,top_pyp,top_calctype,top_targettype,top_annual,top_revised,top_active,top_pdoid,top_ndpid,top_riskratingid,top_repcate) VALUES ";
					$a = 0;
					foreach($t_sql as $t) {
						if($a>=50) {
							$z++;
							$z_sql[$z] = "INSERT INTO ".$dbref."_top (top_id,top_dirid,top_pmsref,top_repkpi,top_gfsid,top_natoutcomeid,top_natkpaid,top_mtas,top_idp,top_munkpaid,top_value,top_unit,top_risk,top_ownerid,top_baseline,top_poe,top_pyp,top_calctype,top_targettype,top_annual,top_revised,top_active,top_pdoid,top_ndpid,top_riskratingid,top_repcate) VALUES ";
							$a=0;
						} elseif($a>0) {
							$z_sql[$z].=",";
						}
						$z_sql[$z].=$t;
						$a++;
					}
					foreach($z_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.="<P>".$sql;
					}
					foreach($r_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.="<P>".$sql;
					}
					db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = '$import_section'");
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, "Top Layer > $act > ".$save_echo_filename, $self);
					echo "<script>document.location.href = 'support_import.php?r[]=ok&r[]=Top+Layer+imported.';</script>";
				}
				echo "</form>";
			} else {	//data count
				logImport($import_log_file, "Top Layer > ERROR > No info to upload [".$file."]", $self);
				die(displayResult(array("error","Couldn't find any information to import.")));
			}
			
		} else {	//if !err
			die(displayResult($file_error));
		}
		break;	//switch act case accept
}	//switch act


fclose($import_log_file);
?>

</body>
</html>