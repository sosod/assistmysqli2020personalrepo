<?php
include "inc.php";

$result = array(0=>0,1=>"Error");
$result['var'] = $_REQUEST;

$s = $_REQUEST['section'];
$section = $me->getSection($s);

$obj_id = $_REQUEST['id'];

$after_action = isset($_REQUEST['after_action']) ? stripslashes($_REQUEST['after_action']) : "";

switch($s) {
	case "KPI":	$object = new DEPT(); $result[0]++; break;
	case "TOP":	$object = new TOP(); $result[0]++; break;
	default: $result[1] = "Invalid Section given"; $object = new SDBP5(); break;
}
$drawObj = new DISPLAY();
$timeObj = new STIME();
$headObj = new HEADINGS();

//echo json_encode($result);

if($result[0]>0) {
	switch($_REQUEST['action']) {
		case "GET_UPDATE_DETAILS":
			$object->logUser();
			$result['ref'] = $section['code'].$obj_id;
			$result['data'] = $object->getAssuranceUpdateFormDetails($obj_id); //$object->getSectionHead();//
			$a = $result['data']['results']['attach'][$_REQUEST['time_id']];
			$result['attach_display'] = $object->getObjectAttachmentDisplay(true,$a);

			$kpi_head = $headObj->getHeadings($object->getHeadingCodes()); //arrPrint($kpi_head);
			$head = array_merge(array('dir'=>$kpi_head['dir']),$kpi_head[$object->getHeadCode()]);
			$rhead = $kpi_head[$object->getRHeadCode()];
			$kpi_time = $timeObj->getAllTime("id");
			$result['data']['time'] = $kpi_time;
			$kr = $result['data'];
			$i = DEPT::REFTAG.$obj_id;
			$kr['display_id'] = $i;
			$result['detail_display'] = $drawObj->getLiteKPI($object,$head,$rhead,$kpi_time,$kr,false,false,$_REQUEST['time_id']);
			$result[1] = "Details retrieved";
			$logs = $object->getAssuranceHistory($obj_id,$_REQUEST['time_id']);
			$result['log_display'] = $drawObj->getAssuranceHistoryHTML($logs);
			break;
		case "GET_ASSURANCE_HISTORY":
			$result['data']=$object->getAssuranceHistory($obj_id,$_REQUEST['time_id']);
			$result['html'] = $drawObj->getAssuranceHistoryHTML($result['data']);
			//$result['data'] = "any info?";
			$result[1] = "details retrieved";
			break;
		/*case "SAVE_UPDATE":	//original update action
			$result = $object->saveUpdate($_REQUEST);
			break;
		case "GET_ATTACH":
			//attachment id given in format KPIID_TIMEID_ARRAYID
			break;
		case "UPDATE_ATTACH":		//original update action
			$obj_id = $_REQUEST['obj_id'];
			$time_id = $_REQUEST['time_id'];
			$original_filename = "";
			$system_filename = "";
			if(isset($_FILES)) {
				$result[] = "files are set";
				$save_folder = $object->getAttachFolder();
				$me->checkFolder($save_folder);
				$result[] = $save_folder." folder check complete";
				$folder = "";
				$location = explode("/",$_SERVER["REQUEST_URI"]);
				$l = count($location)-2;
				for($f=0;$f<$l;$f++) {
					$folder.= "../";
				}
				$folder.="files/".$me->getCmpCode()."/".$save_folder;
				$result[] = $folder;
				$record = $object->getUpdateRecord($obj_id,$time_id);
				$new_attach = array();
				if(!isset($record['attachment']['attach']) || count($record['attachment']['attach'])==0) {
					$i = 0;
				} else {
					foreach($record['attachment']['attach'] as $i=>$a) {
						//
					}
					$i++;
				}
				$result[] = $i;
				foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
					$x = array('id'=>$key,'tmp_name'=>$tmp_name);
					$x['error'] = $_FILES['attachments']['error'][$key];
					if($_FILES['attachments']['error'][$key] == 0) {
						$original_filename = $_FILES['attachments']['name'][$key];
						$ext = substr($original_filename, strrpos($original_filename, '.') + 1);
						$parts = explode(".", $original_filename);
						$file = $parts[0];
						$system_filename = $obj_id."_".$time_id."_".date("YmdHis")."_".$i.".".$ext; 
						$x['sys'] = $system_filename;
						$full_path = $folder."/".$system_filename;
						$x['path'] = $full_path;
						$new_attach[$i] = array('system_filename'=>$system_filename,'original_filename'=>$original_filename);
						$x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
						$i++;
						$result[] = $x;
					}
					$result[] = $new_attach;
				}
				if($new_attach!=$record['attachment']['attach']) {
					$object->saveUpdateAttachments($obj_id,$time_id,$new_attach,$record['attachment']);
				}
			}
			if(isset($_REQUEST['after_action']) && strlen($_REQUEST['after_action'])>0) {
				echo $object->displayPageHeader()."
				<script type=text/javascript>
					$(function() { 
						".stripslashes($_REQUEST['after_action'])."
					});
				</script>";
			}
			break;
		/*** UPDATED UPDATE PROCESS WHICH DOES ATTACHMENTS AND DETAILS AT ONCE ***/
		//[JC] 2013-10-20/21 Response to R395 
		case "SAVE_ASSURANCE":
			//error_reporting(-1);
			$var = $_REQUEST;
//$object->arrPrint($_REQUEST);
			//Process attachment
			$obj_id = $var['obj_id'];
			$time_id = $var['time_id'];
			$original_filename = "";
			$system_filename = "";
			$new_attach = array();
			$record = $object->getAssuranceRecord($obj_id,$time_id);
			/*if(isset($_FILES)) {
				$result[] = "files are set";
				$save_folder = $object->getAttachFolder(true);
				$object->checkFolder($save_folder);
				$result[] = $save_folder." folder check complete";
				$folder = "";
				$location = explode("/",$_SERVER["REQUEST_URI"]);
				$l = count($location)-2;
				for($f=0;$f<$l;$f++) {
					$folder.= "../";
				}
				$folder.="files/".$me->getCmpCode()."/".$save_folder;
				$result[] = $folder;
				if(!isset($record['attachment']['attach']) || count($record['attachment']['attach'])==0) {
					$i = 0;
				} else {
					foreach($record['attachment']['attach'] as $i=>$a) {
						//
					}
					$i++;
				}
				$result[] = $i;
				$err = false;
				$err_msg = array();
				$result[] = $_FILES;
				foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
					$original_filename = $_FILES['attachments']['name'][$key];
					//if(strlen($tmp_name)>0 && strlen($original_filename)>0) {
						$result[] = $key." => processing: ".$tmp_name;
						$x = array('id'=>$key,'tmp_name'=>":".$tmp_name.":",'original'=>":".$original_filename.":");
						$x['error'] = $_FILES['attachments']['error'][$key];
						if($_FILES['attachments']['error'][$key] == 0) {
							//$ext = substr($original_filename, strrpos($original_filename, '.') + 1);
							$parts = explode(".", $original_filename);
							//$file = $parts[0];
							if(count($parts)>1) {
								$ext = ".".$parts[count($parts)-1];
							} else {
								$ext = "";
							}
							$system_filename = $obj_id."_".$time_id."_".date("YmdHis")."_".$i.$ext; 
							$x['sys'] = $system_filename;
							$full_path = $folder."/".$system_filename;
							$x['path'] = $full_path;
							$new_attach[$i] = array('system_filename'=>$system_filename,'original_filename'=>$original_filename);
							$x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
							$i++;
							$result[] = $x;
						} elseif($_FILES['attachments']['error'][$key] == 4) {// && strlen($tmp_name)>0 && strlen($original_filename)>0) {
							$result[] = $key." => no file uploaded";
						} else {
							$err = true;
							$result[] = $x;
							$err_msg[] = $original_filename." could not be uploaded: ".$object->getFileErrorMsg($x['error']);
						}
					//} else {
					//	$result[] = $key." => no file uploaded";
					//}
					$result[] = $new_attach;
				}
				//if($new_attach!=$record['attachment']['attach']) {
				//	$object->saveUpdateAttachments($obj_id,$time_id,$new_attach,$record['attachment']);
				//}
				
			}*/
			if($err || count($err_msg)>0) {
				//$result[] = implode("<br />",$err_msg);
				$r = array(0=>"error","No update could be done.  Please see the reason below:<br />".implode("<br />",$err_msg));
			} else {
				//$var['new_attach'] = $new_attach;
				$r = $object->saveAssuranceUpdate($var,$record);
				//$result[] = $r;
			}
			if($r[0]=="ok") {
				//$after_action = str_ireplace("UPDATERESPONSEMSG",$r[1],$after_action);
				$result = $r;
			} else {
				$result = $r;
				/*$after_action = "";
				$rmsg = str_replace(chr(10)," ",$me->getDisplayResult($r));
				$after_action = "
					window.parent.$('#dlg_msg').dialog('close');
					var response = '".$rmsg."';
					var r = '".ucfirst($r[0])."';
					window.parent.showDialog(r,response);
				";*/
			}
			break;
	}
} else {	//response if an error occurred when trying to create object
	//switch($_REQUEST['action']) {
	//	case "SAVE_UPD":
			$r = array(
				0=>"error",
				1=>"An error has occurred while trying to process your request."
			);
			$result = $r;
			$rmsg = str_replace(chr(10)," ",$me->getDisplayResult($r));
			$rmsg.='<div style="width:400px"><span class=b>Before trying again, please confirm that:</span><ul> <li>You are still logged in and, </li><li>If you are performing an update, that your attachments do not exceed the total file size limit (10MB)</li></ul></div>';
			$after_action = "
				window.parent.$('#dlg_msg').dialog('close');
				var response = '".$rmsg."';
				var r = '".ucfirst($r[0])."';
				window.parent.showDialog(r,response);
			";
		//break;
	//}
}


//if($_REQUEST['action']!="UPDATE_ATTACH") {
/*if(isset($after_action) && strlen($after_action)>0) {
	echo $object->displayPageHeader();
	echo "<h1>test</h1>";
	$object->arrPrint($result);
	echo "	
	<script type=text/javascript>
		$(function() {
			//window.parent.$('#dlg_msg').dialog('close'); 
			".$after_action."
		});
	</script>";
} else {*/
	echo json_encode($result);
//}




?>