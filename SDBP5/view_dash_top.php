<?php
	//GET DATA
		$object_sql = " ".$dbref."_".$table_tbl." o
			INNER JOIN ".$dbref."_dir dir ON o.".$table_fld."dirid = dir.id AND dir.active = true
			WHERE ".$table_fld."active = true AND o.".$table_fld."dirid = ".$graph_id;
		
		$results_sql = "SELECT r.*, o.".$table_fld."calctype as obj_ct, o.".$table_fld."targettype as obj_tt FROM ".$dbref."_".$table_tbl."_results r
			INNER JOIN ".$dbref."_".$table_tbl." o ON r.".$r_table_fld.$table_id."id = o.".$table_fld."id AND o.".$table_fld."active = true
			WHERE ".$r_table_fld."timeid <= ".$filter_to." AND ".$r_table_fld.$table_id."id IN (SELECT o.".$table_fld."id FROM ".$object_sql.")";
		
		$results = mysql_fetch_alls2($results_sql,"".$r_table_fld.$table_id."id",$r_table_fld."timeid");
		
		$count = array();
	//CALCULATIONS
			foreach($results as $obj_id => $result_object) {
				$obj_tt = $result_object[$filter_from]['obj_tt'];
				$obj_ct = $result_object[$filter_from]['obj_ct'];
				$values = array('target'=>array(),'actual'=>array());
				for($ti=3;$ti<=$filter_to;$ti+=3) {
						$values['target'][$ti] = $result_object[$ti][$fld_target];
						$values['actual'][$ti] = $result_object[$ti][$fld_actual];
				}
				$r = KPIcalcResult($values,$obj_ct,array($filter_from,$filter_to),"ALL");
				if(!isset($count[$r['text']])) { $count[$r['text']] = 0; }
				$count[$r['text']]++;
			}
		$total = array_sum($count);
		foreach($result_settings as $ky => $k) {
			$k['obj'] = isset($count[$k['text']]) ? $count[$k['text']] : 0;
			$val = array();
			foreach($k as $key => $s) {
				$val[] = $key.":\"".$s."\"";
			}
			$kr2[$ky] = implode(",",$val);
		}
		$chartData = "{".implode("},{",$kr2)."}";
		?>
        <script type="text/javascript">
        var chart;
		var legend;
        var chartData = [<?php echo $chartData; ?>];

         $(document).ready(function() {
            dir_chart = new AmCharts.AmPieChart();
			dir_chart.color = "#ffffff";
            dir_chart.dataProvider = chartData;
            dir_chart.titleField = "value";
			dir_chart.valueField = "obj";
			dir_chart.colorField = "color";
			dir_chart.labelText = "[[percents]]%";
			dir_chart.labelRadius = -25;
			dir_chart.angle = 10;
			dir_chart.depth3D = 15;
			dir_chart.hideLabelsPercent = 5;
			dir_chart.hoverAlpha = 0.5;
			dir_chart.startDuration = 0;
			dir_chart.radius = 150;
			dir_chart.marginBottom = 30;
			dir_chart.write("dir");

		});
	    </script>
<div align=center>
	<table class=noborder><tr class=no-highlight>
		<td class="center noborder"><span style="font-weight: bold;"><?php echo $lists['dir'][$graph_id]['value']; ?></span>
			<div id="dir" style="width:400px; height:350px; background-color:#ffffff; border: 0px solid #000000;;"></div>
		</td>
	</tr><tr class=no-highlight>
		<td colspan=2 class=noborder><div align=center><table>
	<?php 
	foreach($result_settings as $ky => $k) {
		echo "<tr>
				<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
				".$k['value']."</td>";
		echo "<td class=center style=\"font-weight: normal;\">".(isset($count[$k['text']]) ? $count[$k['text']]." (".number_format($count[$k['text']]/array_sum($count)*100,2)."%)" : "-")."</td>";
		echo "</tr>";
	}
	?>
	<tr>
		<td class=right colspan=1 style="background-color: #dddddd; font-weight: bold;">Total:</td>
		<td class=center style="background-color: #dddddd; font-weight: bold;"><?php echo array_sum($count); ?> (100%)</td>
	</tr>
		</table><br />&nbsp;</div></td>
	</tr></table>
</div>
<?php displayGoBack("view_dash.php","Go Back"); ?>