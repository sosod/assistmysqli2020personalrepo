<?php 
$var = $_REQUEST;
$log_section = "LIST";
$page_title = (isset($var['l']) && is_numeric($var['l']) && $var['l']>0) ? "<a href=".$_SERVER['PHP_SELF']." class=breadcrumb>" : "";
$page_title.= "Lists";
include("inc/header.php"); 

$structure = array(
	'munkpa' => array('code'),
	'natkpa' => array('code'),
	'riskrating' => array('code'),
	'kpitype' => array('code'),
	'kpiconcept' => array('code'),
	'idp_kpi' => array('munkpaid','natkpaid'),
	'wards' => array('code'),
	'repcate'=>array(),
);


if(isset($var['l']) && checkIntRef($var['l'])) {





	$list_id = $var['l'];
	$log_section.="_".$list_id;
	$sql = "SELECT * FROM ".$dbref."_setup_headings 
			WHERE h_type IN ( 'LIST' , 'WARDS','AREA','FUNDSRC','TEXTLIST')
			AND h_id = $list_id";
	$list = mysql_fetch_one($sql);
	//arrPrint($list);
	if(!isset($list['h_length']) || $list['h_length']==0) { $list['h_length']=200; } else { $list['h_length']-=50; }
		//$list = $list[0];
	$list_table = $list['h_table'];
	
	if($list_table=="idp_kpi") {
		$listoflists = array(
			'munkpaid' => array('name'=>"Municipal KPA",'tbl'=>"munkpa"),
			'natkpaid' => array('name'=>"National KPA",'tbl'=>"natkpa")
		);
		foreach($listoflists as $key => $l) {
			$sql = "SELECT * FROM ".$dbref."_list_".$l['tbl']." WHERE active = true ORDER BY value, code";
			$rs = getRS($sql);
			$data = array();
			while($row = mysql_fetch_assoc($rs)) {
				$data[$row['id']] = $row;
			}
			$listoflists[$key]['data'] = $data;
		}
	}
	echo "<h2>".$list['h_client']."</h2>";

	$sql = "SELECT * FROM ".$dbref."_list_".$list['h_table']." ORDER BY sort, value";
	$items = mysql_fetch_alls($sql,'id');
	
	if(isset($var['act'])) {
		switch($var['act']) {
			case "ADD":
				$value = $var['new'];
				if(strlen($value) > 0) {
					$s = array();
					$s[] = "value = '".code($value)."'";
					$s[] = "active = true";
					$s[] = "sort = 0";
					if(isset($structure[$list['h_table']])) {
						foreach($structure[$list['h_table']] as $f) {
							$s[] = $f." = '".code($_REQUEST[$f])."'";
						}
					}
					$sql = "INSERT INTO ".$dbref."_list_".$list['h_table']." SET ".implode(", ",$s);
					$myid = db_insert($sql);
					if(checkIntRef($myid)) {
						$result = array("ok","New ".$list['h_client']." item created.");
						$items[$myid] = array('id'=>$myid,'value'=>code($value),'active'=>1,'sort'=>0);
						//ACTIVITY LOG
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$myid,
											'field'=>"value",
											'text'=>"Added new list item <i>".code($value)."</i>.",
											'old'=>"",
											'new'=>code($value),
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					} else {
						$result = array("error","An error occurred.  No change was made. Please try again.");
					}
				} else {
					$result = array("info","Please enter the list item you wish to add in the text box provided.");
				}
				break;
			case "EDIT": 
				$value = code($var['edit']);
				$item_id = $var['i'];
				if(strlen($value)>0) {
					if(checkIntRef($item_id)) {
						$sql = "UPDATE ".$dbref."_list_".$list['h_table']." SET value = '$value' ";
						if(isset($structure[$list['h_table']])) {
							foreach($structure[$list['h_table']] as $f) {
								$sql.= ", ".$f." = '".code($_REQUEST[$f])."'";
							}
						}
						$sql.= "WHERE id = ".$item_id;
						$mar = db_update($sql);
						if($mar>0) {
							$result = array("ok","List item ".$item_id." has been changed.");
						//ACTIVITY LOG
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$item_id,
											'field'=>"value",
											'text'=>"Changed list item ".$item_id." to <i>".code($value)."</i> from <i>".$items[$item_id]['value']."</i>.",
											'old'=>$items[$item_id]['value'],
											'new'=>code($value),
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
							$items[$item_id]['value'] = code($value);
						} else {
							$result = array("info","No change could be found.  Please try again.");
						}
					} else {
						$result = array("error","An error occurred.  Unfortunately the list item could not be identified.  Please try again.");
					}
				} else {
					$result = array("info","The list item was blank.  No changes were made.");
				}
				break;
			case "DELETE":
				$item_id = $var['i'];
				if(checkIntRef($item_id)) {
					$sql = "UPDATE ".$dbref."_list_".$list['h_table']." SET active = false WHERE id = ".$item_id;
					$mar = db_update($sql);
					if($mar>0) {
						//ACTIVITY LOG
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$item_id,
											'field'=>"active",
											'text'=>"Deactivated list item ".$item_id.".",
											'old'=>"true",
											'new'=>"false",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
						$result = array("ok","List item ".$item_id." has been deactivated.");
						$items[$item_id]['active'] = 0;
					} else {
						$result = array("info","No change could be found.  Please try again.");
					}
				} else {
					$result = array("error","An error occurred.  Unfortunately the list item could not be identified.  Please try again.");
				}
				break;
			case "RESTORE":
				$item_id = $var['i'];
				if(checkIntRef($item_id)) {
					$sql = "UPDATE ".$dbref."_list_".$list['h_table']." SET active = true WHERE id = ".$item_id;
					$mar = db_update($sql);
					if($mar>0) {
						//ACTIVITY LOG
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$item_id,
											'field'=>"active",
											'text'=>"Restored list item ".$item_id.".",
											'old'=>"true",
											'new'=>"false",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
						$result = array("ok","List item ".$item_id." has been restored.");
						$items[$item_id]['active'] = 1;
					} else {
						$result = array("info","No change could be found.  Please try again.");
					}
				} else {
					$result = array("error","An error occurred.  Unfortunately the list item could not be identified.  Please try again.");
				}
				break;
		}
	}
	displayResult($result);
	
	$struct = isset($structure[$list['h_table']]) ? $structure[$list['h_table']] : array();
	
?>

<form name=list id=list>
<input type=hidden name=l value=<?php echo $var['l']; ?> />
<input type=hidden name=act value=ADD />
<table id=tbl_list>
	<tr>
		<th width=50px>Ref</th>
		<th width=400px>List Item</th>
<?php
		foreach($struct as $s) {
			switch($s) {
				case "code":
					$code = array();
					echo "<th width=100px>Short Code</th>";
					break;
				case "munkpaid":
				case "natkpaid":
					echo "<th>".$listoflists[$s]['name']."</th>";
					break;
			}
		}
		
echo "
		<th>Action</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<td><textarea name=new id=new cols=70 rows=4></textarea><br /><label for=new id=lbl_new>".$list['h_length']."</label> characters remaining</td>";
		
		foreach($struct as $s) {
			switch($s) {
				case "code":
					echo "<td class=center><input type=text value='' name=".$s." id=newc class=$s /></td>";
					break;
				case "munkpaid":
				case "natkpaid":
					echo "<td><select name=$s id=new".$s.">";
					echo "<option selected value=0>--- No association ---</option>";
					foreach($listoflists[$s]['data'] as $d) {
						echo "<option value=".$d['id'].">".$d['value']."</option>";
					}
					echo "</select></td>";
					break;
			}
		}
?>		
		<td class=centre><input type=button value=Add id=add class=isubmit /></td>
	</tr>
<?php foreach($items as $i) { ?>
	<tr>
		<?php echo "<th ".((!$i['active']) ? "style=\"background-color: #777777;\"" : "").">".$i['id']."</th>"; ?>
		<?php echo "<td ".((!$i['active']) ? "class=inact" : "").">".$i['value']."<input type=hidden id=V".$i['id']." value=\"".decode($i['value'])."\" /></td>"; ?>
		<?php
			foreach($struct as $s) {
				switch($s) {
					case "code":
						$code[] = $i['code'];
						echo "<td class='center ".((!$i['active']) ? "inact" : "")."'>".$i['code']."<input type=hidden id=C".$i['id']." value=\"".decode($i['code'])."\" /></td>";
						break;
					case "munkpaid":
					case "natkpaid":
						echo "<td ".((!$i['active']) ? "class=inact" : "").">".(isset($listoflists[$s]['data'][$i[$s]]) ? $listoflists[$s]['data'][$i[$s]]['value'] : "[No association]" )."<input type=hidden id=".$s."T".$i['id']." value=\"".(isset($listoflists[$s]['data'][$i[$s]]) ? $listoflists[$s]['data'][$i[$s]]['value'] : "No Association" )."\" /><input type=hidden id=".$s."I".$i['id']." value=\"".(isset($listoflists[$s]['data'][$i[$s]]) ? $listoflists[$s]['data'][$i[$s]]['id'] : "0" )."\" /></td>";
						break;
				}
			}
		?>
		<?php echo "<td  ".((!$i['active'])?"class=\"centre inact\"":"class=centre").">";
			if((($i['id']==1 && in_array(strtoupper($list['h_table']),array("WARDS","AREA"))) || ($i['id']==1 && strtoupper($i['value'])=="UNSPECIFIED"))) {
			} elseif($i['active']) {
				echo "<input type=button value=Edit id=".$i['id']." class=edit />";
			} else {
				echo "<input type=button value=Restore id=".$i['id']." class=restore />";
			}
		?></td>
	</tr>
<?php } ?>
</table>
</form>
<script>
$(document).ready(function() {
	var max = <?php echo $list['h_length']; ?>;
	$("textarea").keyup(function() {
		var i = $(this).prop("id");
		var t = $(this).val(); 
		if(t.length==0) {
			t = $(this).text();
		}
		var $lbl = $("#lbl_"+i);
		$lbl.removeClass("iinform").removeClass("idelete");
		if(t.length>=max) {
			$lbl.addClass("idelete");
			$(this).text(t.substring(0,max));
			$lbl.html(max-$(this).text().length);
		} else {
			var rem = max - t.length;
			$lbl.html(max-t.length);
			if(rem<10) { $lbl.addClass("iinform"); }
		}
	});
	$(".edit:button").click(function() {
		var id = $(this).prop("id");
		$("#i").val(id);
		var v = $("#V"+id).val();
		$("#edit").text(v);
		$("#edit").trigger("keyup");
		$("#ovl").html(v);
<?php foreach($struct as $s) { switch($s) {
	case "code":
	?>var c = $("#C"+id).val();
		$("#ecode").val(c);
		$("#oc").val(c);
		$("#ocl").html(c);<?php
		break;
	case "munkpaid":
	?>var mT = $("#munkpaidT"+id).val();
		var mI = $("#munkpaidI"+id).val();
		$("#emunkpaid").val(mI);
		$("#omunkpaidl").html(mT);<?php
		break;
	case "natkpaid":
	?>var nT = $("#natkpaidT"+id).val();
		var nI = $("#natkpaidI"+id).val();
		$("#enatkpaid").val(nI);
		$("#onatkpaidl").html(nT);<?php
		break;
} } ?>
		$("#dialog_edit").dialog("open");
	});
	<?php 
	if(count($struct)>0 && $list_table != "idp_kpi") {
	foreach($struct as $s) { switch($s) { 
		case "code":
	?>
	var code = new Array();
	<?php foreach($code as $c) { 
		echo chr(10)."code['".strtolower($c)."'] = 1;";
	} ?>
	$(".code:text").prop('size',10);
	$(".code:text").prop('maxlength',10);
	$("#add:button").click(function() {
		var c = $("#newc:text").val();
		if(code[c.toLowerCase()]==1) {
			alert("Duplicate code.\n\nPlease try again.");
		} else {
			$("#list").submit();
		}
	});
	$("#save").click(function() { 
		var c = $("#ecode:text").val();
		if(code[c.toLowerCase()]==1 && c.toLowerCase()!=$("#oc").val().toLowerCase()) {
			alert("Duplicate code.\n\nPlease try again.");
		} else {
			$("#edit_form").submit();
		}
	});

	<?php 
		break;
	} 	} 
	} else {
		?>$("#add:button").click(function() { $("#list").submit(); });
			$("#save").click(function() { $("#edit_form").submit(); });<?php
	}
	?>
	$("#new").focus();
	$(".restore:button").click(function() {
		if(confirm("Are you sure you wish to restore this list item?")==true) {
			var id = $(this).prop("id");
			document.location.href = '<?php echo $self; ?>?act=RESTORE&l=<?php echo $list_id; ?>&i='+id;
		}
	});
});
</script>

<?php
	goBack($self,"Back to Lists");

	
	
	$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
	displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));



/* EDIT DIALOG */	
?>


<div id=dialog_edit title="Edit List Item">
	<form name=edit id=edit_form action=<?php echo $self; ?> method=post style="margin-top: 10px;">
		<input type=hidden value=<?php echo $list_id; ?> name=l />
		<input type=hidden value="" name=i id=i />
		<input type=hidden value=EDIT name=act />
		<table>
			<tr>
				<th class=left>Original Value:</th>
				<td><input type=hidden id=ov /><label for=ov id=ovl></label></td>
			</tr>
			<tr>
				<th class=left>New Value:</th>
				<td><textarea id=edit name=edit cols=70 rows=4></textarea>
				<br /><label for=edit id=lbl_edit></label> characters remaining</td>
			</tr>
<?php
foreach($struct as $s) {
	switch($s) {
		case "code":
			echo "
			<tr>
				<th class=left>Original Code:</th>
				<td><input type=hidden id=oc /><label for=oc id=ocl></label></td>
			</tr>
			<tr>
				<th class=left>New Code:</th>
				<td><input type=text id=ecode name=code class=code value='' /></td>
			</tr>";
			break;
		case "munkpaid":
		case "natkpaid":
			echo "
			<tr>
				<th class=left>Original ".$listoflists[$s]['name'].":</th>
				<td><input type=hidden id=o".$s." /><label for=o".$s." id=o".$s."l></label></td>
			</tr>
			<tr>
				<th class=left>New <?php echo $listoflists[$s]['name']; ?>:</th>
				<td><select name=$s id=e".$s.">
						<option value=0>--- No association ---</option>";
					foreach($listoflists[$s]['data'] as $d) {
						echo "<option value=".$d['id'].">".$d['value']."</option>";
					}
				echo "
				</select></td>
			</tr>";
			break;
	}
}
?>
			<tr>
				<th>&nbsp;</th>
				<td><input type=button value="Save Changes" id=save name=but class=isubmit /> <span class=float><input type=button value=Deactivate id=delete /></span></td>
			</tr>
		</table>
		<p><input type=button value=Cancel id=cancel /></p>
	</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dialog_edit").dialog({
		autoOpen: false, 
		width: 650, 
		height: "auto",
		modal: true
	});
	$("#dialog_edit #cancel").click(function() { $("#dialog_edit").dialog("close"); });
	$("#dialog_edit #delete").addClass("idelete").click(function() {
		if(confirm("Are you sure you wish to deactivate this list item?")==true) {
			var id = $("#i").val();
			document.location.href = '<?php echo $self; ?>?act=DELETE&l=<?php echo $list_id; ?>&i='+id;
		}
	});

});
</script>


<?php

	
	
	
	
	
	
	
	
	
	
	
	
} else {	//DISPLAY SETUP >> DEFAULTS >> LISTS








$sql = "SELECT h_id as id, h_ignite as ignite, h_client as client, h_table as source FROM ".$dbref."_setup_headings 
		WHERE h_type IN ('LIST','AREA','WARDS','FUNDSRC','TEXTLIST')
		AND h_table NOT IN ('dir','calctype','targettype','subdir')
		ORDER BY h_client";
$lists = mysql_fetch_all($sql);
?>
<table width=650px>
<?php foreach($lists as $l) { ?>
	<tr>
		<?php if($l['ignite']=="IDP Objective" && $l['source']=="idp_kpi") {
			?><th class=left><?php echo $l['client']; ?>:<br /><span style="font-weight: normal; font-style: italic; font-size: 6.5pt;">(Departmental)</span></th>
			<td>Define the items for use in the Departmental <?php echo $l['client']; ?> list.<?php
		} elseif($l['ignite']=="IDP Objective" && $l['source']=="idp_top") {
			?><th class=left><?php echo $l['client']; ?>:<br /><span style="font-weight: normal; font-style: italic; font-size: 6.5pt;">(Top Layer)</span></th>
			<td>Define the items for use in the Top Layer <?php echo $l['client']; ?> list.<?php
		} else {
			?><th class=left><?php echo $l['client']; ?>:</th>
			<td>Define the list items for use in the <?php echo $l['client']; ?> list.<?php
		} ?>
			<span class=float><input type=button value=Configure class=config id=<?php echo $l['id']; ?> /></span></td>
	</tr>
<?php } ?>
</table>
<script>
$(document).ready(function() {
	//$(".config:button").prop("disabled",true);
	$(":button").click(function() {
		var id = $(this).prop("id");
		if(!isNaN(parseInt(id))) {
			document.location.href = '<?php echo $self; ?>?l='+id;
		}
	});
});
</script>
<?php 
	goback("setup.php","Back to Setup");
	
	
	
	
	
} //endif isset($var['l']) ?>
<script type=text/javascript>
$(function() {
	//$(":button").button().css("font-size","7pt");
});
</script>

</body>
</html>