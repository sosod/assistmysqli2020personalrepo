<?php
include("inc/inc_ignite.php"); 
include("inc/inc.php"); 

$page_id = $_REQUEST['page_id'];
$time_id = $_REQUEST['t'];
$t_sql = "SELECT * FROM ".$dbref."_list_time WHERE id = ".$time_id;
$time = mysql_fetch_alls($t_sql,"id");
$time = $time[$time_id];
$time['display_short'] = date("M Y",strtotime($time['end_date']));
$time['display_full'] = date("F Y",strtotime($time['end_date']));

switch($page_id) {
case "capital":
case "capital_budget":
	switch($page_id) {
	case "capital":
		$title = "Update";
		$rheadings = array("target","actual");
		break;
	case "capital_budget":
		$rheadings = array("target");
		$title = "Edit";
		break;
	}
	$fname = strtolower($title);
		$section = "CAP";
		$r_section = "CAP_R";
		$mheadings = getModuleHeadings(array($section,$section."_R"));
		$fdata = "\"Update worksheet for Capital Projects for time period ".$time['display_full']."\"\n";
		$fdata.= "\"Ref\"";
		foreach($mheadings as $sec => $h1) {
			foreach($h1 as $fld => $h) {
				if(($sec=="dir" || $sec == $section) && !in_array($h['h_type'],array("WARDS","AREA","FUNDSRC","DATE"))) {
					$fdata.= ",\"".decode($h['h_client'])."\"";
				} elseif($sec == $r_section && in_array($fld,$rheadings)) {
					if($fld == "target") { $fdata.= ",\"\""; }
					$fdata.= ",\"".decode($h['h_client'])."\"";
				}
			}
		}

		$object_sql[0] = "SELECT subdir.value as list_cap_subid, dir.value as list_0, gfs.value as list_cap_gfsid, c.* ";
		$object_sql[1] = "FROM ".$dbref."_capital c
		INNER JOIN ".$dbref."_subdir subdir ON cap_subid = subdir.id AND subdir.active = true
		INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		INNER JOIN ".$dbref."_list_gfs gfs ON cap_gfsid = gfs.id 
		WHERE cap_active = true ";

		$results_sql = "SELECT r.* FROM ".$dbref."_capital_results r
		INNER JOIN ".$dbref."_capital c ON r.cr_capid = c.cap_id AND c.cap_active = true
		WHERE cr_timeid = ".$time_id."";
		
		$results = mysql_fetch_alls($results_sql,"cr_capid");
		
		/*if(isset($mheadings[$section]['capital_wards'])) {
			$wards_sql = "SELECT cw_capid, id, value, code FROM ".$dbref."_capital_wards INNER JOIN ".$dbref."_list_wards ON cw_listid = id 
			WHERE cw_active = true AND cw_capid IN (SELECT c.cap_id ".$object_sql[1].")";
			$wa['capital_wards'] = mysql_fetch_alls2($wards_sql,"cw_capid","id");
		}
		if(isset($mheadings[$section]['capital_area'])) {
			$area_sql = "SELECT ca_capid, id, value, code FROM ".$dbref."_capital_area INNER JOIN ".$dbref."_list_area ON ca_listid = id 
			WHERE ca_active = true AND ca_capid IN (SELECT c.cap_id ".$object_sql[1].")";
			$wa['capital_area'] = mysql_fetch_alls2($area_sql,"ca_capid","id");
		}
		if(isset($mheadings[$section]['capital_fundsource'])) {
			$fundsrc_sql = "SELECT cs_capid, id, value, code FROM ".$dbref."_capital_fundsource INNER JOIN ".$dbref."_list_fundsource ON cs_listid = id 
			WHERE cs_active = true AND cs_capid IN (SELECT c.cap_id ".$object_sql[1].")";
			$wa['capital_fundsource'] = mysql_fetch_alls2($fundsrc_sql,"cs_capid","id");
		}*/
		$object_sql = implode(" ",$object_sql)." ORDER BY dir.sort, subdir.sort, cap_id";
		
		$objects = mysql_fetch_alls($object_sql,"cap_id");

		foreach($objects as $obj_id => $obj) {
			$fdata.="\n\"".$id_labels_all[$section].$obj_id."\"";
			foreach($mheadings as $sec => $h1) {
				foreach($h1 as $fld => $h) {
					if(($sec=="dir" || $sec == $section) && !in_array($h['h_type'],array("WARDS","AREA","FUNDSRC","DATE"))) {
						$fdata.=",\"";
						switch($h['h_type']) {
/*							case "WARDS":
							case "AREA":
							case "FUNDSRC":
								$d = array();
								foreach($wa[$fld][$obj_id] as $a => $b) {
									$d[] = isset($b['code']) && strlen($b['code'])>0 ? $b['code'] : $b['value'];
								}
								$fdata.=implode(";",$d);
								break;*/
							case "DATE":
								$fdata.=strlen($obj[$fld])>0 && $obj[$fld]>0 ? date("d M Y",$obj[$fld]) : "";
								break;
							case "LIST":
								$fdata.=decode($obj['list_'.$fld]);
								break;
							case "VC":
							case "TEXT":
							default:
								$fdata.= str_replace('"',"''",decode($obj[$fld]));
//								$fdata.= str_replace('"',"''",str_replace(',',';',decode($obj[$fld])));
								break;
						}
						$fdata.="\"";
					} elseif($sec == $r_section && in_array($fld,$rheadings)) {
						if($fld=="target") { $fdata.=",\"\""; }
						$fdata.=",\"".(isset($results[$obj_id]['cr_'.$fld]) ? $results[$obj_id]['cr_'.$fld] : "0")."\"";
					}
				}
			}
			
		}
	break;
case "cashflow":
case "cashflow_budget":
	switch($page_id) {
	case "cashflow":
		$title = "Update";
//		$rheadings = array("target","actual");
		$rheadings = array("_5");
		break;
	case "cashflow_budget":
//		$rheadings = array("target");
		$rheadings = array("_1","_2","_3");
		$title = "Edit";
		break;
	}
	$fname = strtolower($title);
		$section = "CF";
		$r_section = "CF_R";
		$h_section = "CF_H";
		$table_tbl = "cashflow";
		$mheadings = getModuleHeadings(array($section,$section."_H",$section."_R"));
		$mheadings[$r_section]["_5"]['h_client'] = "Actual";
		$fdata = "\"Update worksheet for Monthly Cashflow for time period ".$time['display_full']."\"\n";

		$fdata.= "\"\",\"\"";
		foreach($mheadings[$section] as $fld => $h) {
			$fdata.= ",\"\"";
		}
		foreach($mheadings[$h_section] as $fld => $h) {
			$fdata.=",\"\",\"".$h['h_client']."\"";
			for($i=1;$i<count($rheadings);$i++) {
				$fdata.=",\"\"";
			}
		}
		$fdata.= "\n\"Ref\",\"".$mheadings['dir'][0]['h_client']."\"";
		foreach($mheadings[$section] as $fld => $h) {
			$fdata.= ",\"".decode($h['h_client'])."\"";
		}
		for($i=1;$i<=count($mheadings[$h_section]);$i++) {
			foreach($mheadings[$r_section] as $fld => $h) {
				if(in_array($fld,$rheadings)) {
					if($fld == $rheadings[0]) { $fdata.= ",\"\""; }
					$fdata.= ",\"".decode($h['h_client'])."\"";
				}
			}
		}
		
		$object_sql[0] = "SELECT subdir.value as list_cf_subid, dir.value as list_0, gfs.value as list_cf_gfsid, c.* ";
		$object_sql[1] = "FROM ".$dbref."_cashflow c
		INNER JOIN ".$dbref."_subdir subdir ON cf_subid = subdir.id AND subdir.active = true
		INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		INNER JOIN ".$dbref."_list_gfs gfs ON cf_gfsid = gfs.id 
		WHERE cf_active = true ";

		$results_sql = "SELECT r.* FROM ".$dbref."_".$table_tbl."_results r
		INNER JOIN ".$dbref."_".$table_tbl." c ON r.cr_cfid = c.cf_id AND c.cf_active = true
		WHERE cr_timeid = ".$time_id."";
		
		$results = mysql_fetch_alls($results_sql,"cr_cfid");
		
		$object_sql = implode(" ",$object_sql)." ORDER BY dir.sort, subdir.sort, cf_id";
		
		$objects = mysql_fetch_alls($object_sql,"cf_id");

		foreach($objects as $obj_id => $obj) {
			$fdata.="\n\"".$id_labels_all[$section].$obj_id."\"";
			$fdata.=",\"".$obj['list_0']."\"";
			foreach($mheadings[$section] as $fld => $h) {
				$fdata.=",\"";
				switch($h['h_type']) {
					case "LIST":
						$fdata.=decode($obj['list_'.$fld]);
						break;
					case "VC":
					case "TEXT":
					default:
						$fdata.= decode($obj[$fld]);
						break;
				}
				$fdata.="\"";
			}
			foreach($mheadings[$h_section] as $hfld => $hh) {
				foreach($mheadings[$r_section] as $fld => $h) {
					if(in_array($fld,$rheadings)) {
						if($fld==$rheadings[0]) { $fdata.=",\"\""; }
						$db_fld = 'cr_'.$hfld.$fld;
						$fdata.=",\"".(isset($results[$obj_id][$db_fld]) ? $results[$obj_id][$db_fld] : $db_fld)."\"";
					}
				}
			}
		}
	break;
case "revbysrc":
	$fname = "update";
		$section = "RS";
		$r_section = "RS_R";
		$mheadings = getModuleHeadings(array($section,$section."_R"));
		$rheadings = array("rr_budget","rr_actual");
		$fdata = "\"Update worksheet for Revenue By Source for time period ".$time['display_full']."\"\n";
		$fdata.= "\"Ref\"";
		foreach($mheadings as $sec => $h1) {
			foreach($h1 as $fld => $h) {
				if($sec=="dir" || $sec == $section) {
					$fdata.= ",\"".decode($h['h_client'])."\"";
				} elseif($sec == $r_section && in_array($fld,$rheadings)) {
					if($fld == "rr_budget") { $fdata.= ",\"\""; }
					$fdata.= ",\"".decode($h['h_client'])."\"";
				}
			}
		}

	$object_sql = "SELECT r.* FROM ".$dbref."_revbysrc r
	WHERE r.rs_active = true ORDER BY r.rs_sort";
	
	$results_sql = "SELECT r.* FROM ".$dbref."_revbysrc_results r
	INNER JOIN ".$dbref."_revbysrc rs ON r.rr_rsid = rs.rs_id AND rs.rs_active = true
	WHERE rr_timeid <= ".$time_id;
	
	$results = mysql_fetch_alls($results_sql,"rr_rsid");
	$objects = mysql_fetch_alls($object_sql,"rs_id");
		foreach($objects as $obj_id => $obj) {
			$fdata.="\n\"".$id_labels_all[$section].$obj_id."\"";
			foreach($mheadings as $sec => $h1) {
				foreach($h1 as $fld => $h) {
					if($sec=="dir" || $sec == $section) {
						$fdata.= ",\"".decode($obj[$fld])."\"";
					} elseif($sec == $r_section && in_array($fld,$rheadings)) {
						if($fld == "rr_budget") { $fdata.= ",\"\""; }
						$fdata.=",\"".(isset($results[$obj_id][$fld]) ? $results[$obj_id][$fld] : "$fld")."\"";
					}
				}
			}
		}
	break;
}

$folder = $modref."/exports";
$old = $page_id."_".$fname."_".date("FY",strtotime($time['end_date']))."_".date("YmdHis").".csv";
saveEcho($folder, $old, $fdata);
$content = "text/plain";
$new = $page_id."_".$fname."_".date("FY",strtotime($time['end_date'])).".csv";
downloadFile2($folder, $old, $new, $content);
?>