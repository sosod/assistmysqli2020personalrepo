<?php
if(!isset($_REQUEST['page_action_view'])) {
	$_REQUEST['page_action_view'] = "GENERATOR";
}

//error_reporting(-1);

//ASSIST_HELPER::arrPrint($time);


switch($_REQUEST['page_action_view']) {
	
	case "GENERATOR":


	$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
				INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT k.kpi_subid FROM ".$dbref."_kpi k WHERE kpi_active = true ) 
				WHERE d.active = true
				";
				//needed for departmental specific assurance
				/*AND (
					d.id IN (SELECT ref FROM ".$dbref."_user_admins WHERE type = 'DIR' AND active = true AND tkid = '$tkid' AND act_".$page_id." = true) 
				OR 
					s.id IN (SELECT ref FROM ".$dbref."_user_admins WHERE type = 'SUB' AND active = true AND tkid = '$tkid' AND act_".$page_id." = true)
				)";*/
				//echo $dir_sql;
	$valid_dir_sub = mysql_fetch_alls2($dir_sql,"dir","subdir");
	//arrPrint($lists);

	
echo "
<form name=assurance_report method=post action=manage_assurance.php>
	<input type=hidden name=page_action_view value=GENERATE />
	<input type=hidden name=page_id value=review_kpi_report />
<table width=750px style='margin-bottom: 20px'><tr><td class=b>

";
	
		$dir = $lists['dir'];
		if(isset($lists['subdir']['dir'])) {
			$sub = $lists['subdir']['dir'];
		}
		echo "&nbsp;Who: <select name=filter[who] id=filter_who>
				<option value=ALL selected>".$_SESSION['ia_cmp_name']."</option>";
			foreach($dir as $d) {
				if(isset($valid_dir_sub[$d['id']])) {
					echo "<option value=D_".$d['id']." >".(strlen(decode($d['value']))>53 ? substr(decode($d['value']),0,53)."..." : $d['value'])."</option>";
					if(isset($lists['subdir']['dir'])) {
						foreach($sub[$d['id']] as $s) {
								echo "<option value=S_".$s['id']." >&nbsp;-&nbsp;".(strlen(decode($s['value']))>50 ? substr(decode($s['value']),0,50)."..." : $s['value'])."</option>";
						}
					}
				}
			}
		echo "</select>&nbsp;&nbsp;&nbsp;";



		$sel = array();
		$sel_set = false; 
		$x = time();
		foreach($time as $ti=>$t){
			$check = false;
			if($sel_set!==true && $ti<12) {
				if($time[$ti+1]['start_stamp']<$x && $time[$ti+1]['end_stamp']>$x) {
					$check = true;
					$sel_set = true;
				}
			} elseif($sel_set===false && $ti==12) {
				$check = true;
			}
			$sel[] = "<option ".($check ? "selected" : "")." value=".$ti.">".$t['display_short']."</option>";
		}

		echo "
	When: <select name=filter[when] id=filter_when>".implode("",$sel)."</select> <input type=hidden name=filter[what] id=filter_what value='all' />
	</td></tr></table>
</form>
<table width=750px>
	<tr>
		<th>Report</th>
		<th>Description</th>
		<th></th>
	</tr>
	<tr>
		<td class=b>All Assurance Statuses</td>
		<td>All KPIs irrespective of assurance status.</td>
		<td><input type=button value=Go id=btn_all report=all /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("Y")."</div>Signed-off</td>
		<td>All KPIs which have been reviewed and signed off.</td>
		<td><input type=button value=Go id=btn_ok report=ok /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("N")."</div>Rejected (All)</td>
		<td>All KPIs which have been reviewed and returned to the user.</td>
		<td><input type=button value=Go id=btn_no_all report=no_all /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("N")."</div>Rejected (No response)</td>
		<td>All KPIs which have been reviewed and returned to the user where the user has not yet responded.</td>
		<td><input type=button value=Go id=btn_no_no report=no_no /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("warn")."</div>Rejected (User responded)</td>
		<td>All KPIs which have been reviewed and returned to the user where the user has responded.</td>
		<td><input type=button value=Go id=btn_no_yes report=no_yes /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("info")."</div>Not Reviewed (All)</td>
		<td>All KPIs which have not yet been reviewed.</td>
		<td><input type=button value=Go id=btn_na_all report=na_all /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("info")."</div> Not Reviewed (Applicable)</td>
		<td>All KPIs which have not yet been reviewed but where an update is required.</td>
		<td><input type=button value=Go id=btn_na_do report=na_do /></td> 
	</tr>
	<tr>
		<td class=b><div class=float>".ASSIST_HELPER::drawStatus("info")."</div>Not Reviewed (Not Applicable)</td>
		<td>All KPIs which have not yet been reviewed which do not require an update.</td>
		<td><input type=button value=Go id=btn_na_ignore report=na_ignore /></td> 
	</tr>
</table>

<script type=text/javascript>
$(function() {
	$('input:button').click(function() {
		//document.location.href = 'manage_assurance.php?page_id=review_kpi_report&page_action_view=GENERATE';
		$"."form = $('form[name=assurance_report]');
		var rep = $(this).attr('report');
		$('#filter_what').val(rep);
		$"."form.submit();
	});
});
</script>

";
	
	
	
	break;
case "GENERATE":
$section = "KPI";
	include("assurance_review_report_table.php");


	break;
	
}


?>