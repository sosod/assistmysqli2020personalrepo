<?php 
$section = "KPI";
$get_lists = true;
$get_active_lists = false;
$locked_column = true;

include("inc/header.php"); 

//arrPrint($all_time);

//Filter settings
$filter = array();
	//WHEN
	if(isset($_REQUEST['filter_when'])) {
		$f_when = $_REQUEST['filter_when'];
		$filter_when = array(
			0=>($f_when[0]<=$f_when[1] ? $f_when[0] : $f_when[1]),
			1=>($f_when[0]<=$f_when[1] ? $f_when[1] : $f_when[0])
		);
	} else {
		$filter_to = $current_time_id;
		$filter_from = ($filter_to > 4) ? $filter_to - 2 : 1;
		$filter_when = array($filter_from,$filter_to);
	}
	$filter['when'] = $filter_when;
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
	} else {
		$filter_who = array("","All");
	}
	$filter['who'] = $filter_who;
	//WHAT
	$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
	//DISPLAY
	$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";


if(!$import_status[$section]) {
	die("<p>Departmental SDBIP has not yet been created.  Please try again later.</p>");
} else {

	$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
		INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT k.kpi_subid FROM ".$dbref."_kpi k
		INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE kpi_active = true ) WHERE d.active = true";
	$valid_dir_sub = mysql_fetch_alls2($dir_sql,"dir","subdir");
	
	//drawViewFilter($who,$what,$when,$sel,$filter)
	$head = "<h2 style=\"margin-top: 0px;\">";
	if($filter['when'][0]==$filter['when'][1]) {
		$head.= $time[$filter['when'][0]]['display_full'];
	} else {
		$head.= $time[$filter['when'][0]]['display_full']." - ".$time[$filter['when'][1]]['display_full'];
	}
	$head.="</h2>";
	$filter['who'] = drawViewFilter(true,true,true,2,true,$filter,$head);
	//drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form)
	//drawPaging_noSearch($self,'',0,15,1,10,'Reference',$self);

//arrPrint($filter);
	//SQL
/*	$object_sql = "SELECT k.* FROM ".$dbref."_kpi k
	INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true
	".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
	INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
	WHERE kpi_active = true ".($filter['who'][0]=="S" && checkIntRef($filter['who'][1]) ? " AND kpi_subid = ".$filter['who'][1] : "");
	if(isset($filter['what']) && strtolower($filter['what'])!="all") {
		switch($filter['what']) {
			case "op":	$object_sql.= " AND kpi_capitalid = 0 AND kpi_topid = 0"; break;
			case "cap":	$object_sql.= " AND kpi_capitalid > 0"; break;
			case "top":	$object_sql.= " AND kpi_topid > 0"; break;
		}
	}*/

	$object_sql = " ".$dbref."_kpi k
		INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true
		".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
		INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE kpi_active = true ".($filter['who'][0]=="S" && checkIntRef($filter['who'][1]) ? " AND kpi_subid = ".$filter['who'][1] : "");
		if(isset($filter['what']) && strtolower($filter['what'])!="all") {
			switch($filter['what']) {
				case "op":	$object_sql.= " AND kpi_capitalid = 0 AND kpi_topid = 0"; break;
				case "cap":	$object_sql.= " AND kpi_capitalid > 0"; break;
				case "top":	$object_sql.= " AND kpi_topid > 0"; break;
			}
		}

	
	$results_sql = "SELECT r.* FROM ".$dbref."_kpi_results r
		INNER JOIN ".$dbref."_kpi k ON r.kr_kpiid = k.kpi_id AND k.kpi_active = true
		WHERE kr_timeid <= ".$filter['when'][1]." AND kr_kpiid IN (SELECT k.kpi_id FROM ".$object_sql.")";
	
	$results = mysql_fetch_alls2($results_sql,"kr_kpiid","kr_timeid");
	
	if(isset($mheadings['KPI']['kpi_topid'])) {
		$top_sql = "SELECT t.top_id, t.top_value FROM ".$dbref."_top t
			WHERE t.top_id IN (SELECT k.kpi_topid FROM ".$object_sql.") AND t.top_active = true";
		$tops = mysql_fetch_alls($top_sql,"top_id");
	}
	if(isset($mheadings['KPI']['kpi_capitalid'])) {
		$cap_sql = "SELECT c.cap_id, c.cap_name FROM ".$dbref."_capital c
			WHERE c.cap_id IN (SELECT k.kpi_capitalid FROM ".$object_sql.") AND c.cap_active = true";
		$caps = mysql_fetch_alls($cap_sql,"cap_id");
	}
	if(isset($mheadings['KPI']['kpi_wards'])) {
		$wards_sql = "SELECT kw_kpiid, id, value, code FROM ".$dbref."_kpi_wards INNER JOIN ".$dbref."_list_wards ON kw_listid = id 
			WHERE kw_active = true AND kw_kpiid IN (SELECT k.kpi_id FROM ".$object_sql.")";
		$wa['kpi_wards'] = mysql_fetch_alls2($wards_sql,"kw_kpiid","id");
	}
	if(isset($mheadings['KPI']['kpi_area'])) {
		$area_sql = "SELECT ka_kpiid, id, value, code FROM ".$dbref."_kpi_area INNER JOIN ".$dbref."_list_area ON ka_listid = id 
			WHERE ka_active = true AND ka_kpiid IN (SELECT k.kpi_id FROM ".$object_sql.")";
		$wa['kpi_area'] = mysql_fetch_alls2($area_sql,"ka_kpiid","id");
	}

	$object_sql = "SELECT k.* FROM ".$object_sql;
	
	
	include("view_table.php");

	
	
}	//if import_status['import_Section'] == false else	
?>
</body>
</html>