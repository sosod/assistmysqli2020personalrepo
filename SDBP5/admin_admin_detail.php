<?php 
$get_lists = true;
$get_active_lists = true;
$section = "KPI";
$page_title = "Detail";
$obj_id = $_REQUEST['id'];
include("inc/header.php"); 

if(isset($_REQUEST['id']) && checkIntRef($obj_id)) {



/*** GET OBJECT DETAILS ***/

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "TEXTLIST":
			$fields[] = "k.".$fld." as o".$fld;
			$fields[] = "k.".$fld." as ".$fld;
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			$fields[] = "k.".$fld." as o".$fld;
			break;
		default: $fields[] = "k.".$fld;
	}
}

$object_sql = "SELECT dir.value, targettype.id as tt, calctype.code as ct, ".implode(", ",$fields)."
FROM ".$dbref."_kpi k
INNER JOIN ".$dbref."_subdir as subdir ON k.kpi_subid = subdir.id
INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " LEFT OUTER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE k.kpi_id = ".$obj_id;
$object = mysql_fetch_all($object_sql);
$object = $object[0];

$extras = array();
//WARDS
$fld = "kpi_wards";
$tbl = "wards";
$a = "w";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.k".$a."_listid AND b.k".$a."_active = true AND b.k".$a."_kpiid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql,"id");
//AREAS
$fld = "kpi_area";
$tbl = "area";
$a = "a";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.k".$a."_listid AND b.k".$a."_active = true AND b.k".$a."_kpiid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql, "id");



if($object['kpi_topid']>0) {
	$top_sql = "SELECT top_value FROM ".$dbref."_top WHERE top_id = ".$object['kpi_topid']." AND top_active = true";
	$o = array();
	$o = mysql_fetch_all($top_sql);
	if(isset($o[0])) {
		$object['kpi_topid'] = $o[0]['top_value']." [".$id_labels_all['TOP'].$object['kpi_topid']."]";
	} else {
		$object['kpi_topid'] = "N/A";
	}
} else {
	$object['kpi_topid'] = "N/A";
}
if($object['kpi_capitalid']>0) {
	$top_sql = "SELECT cap_name FROM ".$dbref."_capital WHERE cap_id = ".$object['kpi_capitalid']." AND cap_active = true";
	$o = array();
	$o = mysql_fetch_all($top_sql);
	if(isset($o[0])) {
		$object['kpi_capitalid_disp'] = $o[0]['cap_name']." [".$id_labels_all['CAP'].$object['kpi_capitalid']."]";
	} else {
		$object['kpi_capitalid_disp'] = "N/A";
		$object['kpi_capitalid'] = 0;
	}
} else {
	$object['kpi_capitalid_disp'] = "N/A";
	$object['kpi_capitalid'] = 0;
}

$result_sql = "SELECT * FROM ".$dbref."_kpi_results WHERE kr_kpiid = ".$obj_id." ORDER BY kr_timeid";
$results = mysql_fetch_alls($result_sql,"kr_timeid");


}//isset id

/** ACTION **/
if(isset($_REQUEST['act'])) {
echo "<p>Processing edit...</p>";
switch($_REQUEST['act']) {
case "SAVE":
	$updates = array();
	$trans = array();
	$changes = array();
	$sql = array();
	foreach($mheadings[$section] as $fld => $h) {
		$log_trans = "";
		switch($h['h_type']) {
			case "TOP":
//			case "CAP":
				$new = "Do nothing";
				$old = "Do nothing";
				$log_trans = "";
				break;
			case "WARDS":
			case "AREA":
				$s = array();
				$deleted = array();
				$added = array();
				$log_trans = "";
				$n = array();
				foreach($_REQUEST[$fld] as $i) {
					$n[] = $lists[$h['h_table']][$i]['value']." => ".(!isset($extras[$fld][$i]) ? "NEW" : "");
					if(!isset($extras[$fld][$i])) {
						$log_trans.="<br />- Added \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$lists[$h['h_table']][$i]['value']."\'";
						$added[] = $i;
					}
				}
				$new = implode("<br />",$n);
				$o = array();
				foreach($extras[$fld] as $i => $l) {
					$o[] = $l['value'];
					if(!in_array($i,$_REQUEST[$fld])) {
						$log_trans.="<br />- Deleted \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$l['value']."\'";
						$deleted[] = $i;
					}
				}
				$old = implode("<br />",$o);
				if(strlen($log_trans)>0) {
					$log_trans = "Updated ".$h['h_client'].":".$log_trans;
					if(count($deleted)>0) {
						$sa = "UPDATE ".$dbref."_".$fld." SET k".(substr($h['h_table'],0,1))."_active = false WHERE k".(substr($h['h_table'],0,1))."_kpiid = ".$obj_id." AND k".(substr($h['h_table'],0,1))."_listid IN (".implode(",",$deleted).")";
						db_update($sa);
						$s[] = $sa;
					}
					if(count($added)>0) {
						$sa = "INSERT INTO ".$dbref."_".$fld." (k".(substr($h['h_table'],0,1))."_kpiid, k".(substr($h['h_table'],0,1))."_listid, k".(substr($h['h_table'],0,1))."_active) VALUES (".$obj_id.",".implode(",true),(".$obj_id.",",$added).",true)";
						db_insert($sa);
						$s[] = $sa;
					}
					$v = array(
						'fld'=>$fld,
						'timeid'=>0,
						'text'=>$log_trans,
						'old'=>implode(",",$deleted),
						'new'=>implode(",",$added),
						'act'=>"E",
						'YN'=>"Y"
					); 
					logChanges($section,$obj_id,$v,code(implode(chr(10),$s)));
				}
				break;
			case "TEXTLIST":
				if(isset($_REQUEST[$fld]) && is_array($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
					$new = ";".implode(";",$_REQUEST[$fld]).";";
					$n = $_REQUEST[$fld];
					$nw = array();
					foreach($n as $i) {
						$nw[] = $lists[$h['h_table']][$i]['value'];
					}
					if(count($nw)>0) {
						$new_text = implode("; ",$nw);
					} else {
						$new = ";0;";
						$new_text = $unspecified;
					}
				} else {
					$new = ";0;";
					$new_text = $unspecified;
				}
	//			$old = isset($object['o'.$fld]) ? $object['o'.$fld] : ";0;";
				if(isset($object['o'.$fld]) && strlen($object['o'.$fld])>0 && $object['o'.$fld]!=";0;") { //&& is_array($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
					$old = $object['o'.$fld];
					$n = explode(";",$old);
					$nw = array();
					if(!in_array("0",$n)) {
						foreach($n as $i) {
							if(strlen($i)>0) {
								$nw[] = $lists[$h['h_table']][$i]['value'];
							}
						}
					}
					if(count($nw)>0) {
						$old_text = implode("; ",$nw);
					} else {
						$old = ";0;";
						$old_text = $unspecified;
					}
				} else {
					$old = ";0;";
					$old_text = $unspecified;
				}
				if($new!=$old) {
					$log_trans = addslashes("Updated ".$h['h_client']." to '".$new_text."' from '".$old_text."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
			case "CAP":
			case "LIST":
				$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
				if($h['h_type']=="LIST") {
					$old = isset($object['o'.$fld]) ? $object['o'.$fld] : "0";
				} else {
					$old = isset($object[$fld]) ? $object[$fld] : "0";
				}
				if($new!=$old) {
					if(in_array($h['h_type'],array("CAP","TOP"))) {
						if($old==0) {
							$log_trans = addslashes("Added link to ".$h['h_client']." '".$id_labels_all[$h['h_type']].$new."'");
						} elseif($new==0) {
							$log_trans = addslashes("Deleted link to ".$h['h_client']." '".$id_labels_all[$h['h_type']].$old."'");
						} else {
							$log_trans = addslashes("Changed link to ".$h['h_client']." to '".$id_labels_all[$h['h_type']].$new."' from '".$id_labels_all[$h['h_type']].$old."'");
						}
					} else {
						$log_trans = addslashes("Updated ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."' from '".$object[$fld]."'");
					}
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
			default:
				$new = isset($_REQUEST[$fld]) ? code($_REQUEST[$fld]) : "";
				$old = isset($object[$fld]) ? $object[$fld] : "";
				if($new!=$old) {
					$log_trans = addslashes("Updated ".$h['h_client']." to '".$new."' from '".$old."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
		}
		/*echo "<tr>
		<th>".$fld."</th>
		<td>".$h['h_type']."</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	if(count($changes)>0) {
		$sb = "UPDATE ".$dbref."_kpi SET ".implode(", ",$changes)." WHERE kpi_id = ".$obj_id;
		db_update($sb);
		foreach($trans as $fld => $lt) {
			$v = array(
				'fld'=>$fld,
				'timeid'=>0,
				'text'=>$lt['lt'],
				'old'=>$lt['o'],
				'new'=>$lt['n'],
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sb));
		}
	}
	foreach($_REQUEST['ti'] as $ti) {
		$trans = array();
		$result_changes = array();
		$log_trans = "";
		if(isset($_REQUEST['kr_target'][$ti]) && ($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") {
			$fld = "kr_target";
			$new = $_REQUEST['kr_target'][$ti];
			$old = $results[$ti]['kr_target'];
			if($new!=$old) {
				$log_trans = "Updated Target for ".$time[$ti]['display_full']." to \'$new\' from \'$old\'";
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$result_changes[] = "kr_target = $new";
			}
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Target</td>
			<td>".$new."</td>
			<td>".$old."</td>
			<th>".$log_trans."</th>
			</tr>";*/
			
		}
		$log_trans = "";
		if(isset($_REQUEST['kr_actual_'.$ti])) {
			$fld = "kr_actual";
			$new = $_REQUEST['kr_actual_'.$ti];
			$old = $results[$ti]['kr_actual'];
			if($new!=$old) {
				$log_trans = "Updated Actual for ".$time[$ti]['display_full']." to \'$new\' from \'$old\'";
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$result_changes[] = "kr_actual = $new";
			}
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Actual</td>
			<td>".$new."</td>
			<td>".$old."</td>
			<th>".$log_trans."</th>
			</tr>";*/
		}
		if(isset($_REQUEST['kr_perf_'.$ti])) {
			$fld = "kr_perf";
			$new = code($_REQUEST['kr_perf_'.$ti]);
			$old = $results[$ti]['kr_perf'];
			if($new!=$old) {
				$log_trans = "Updated Comment for ".$time[$ti]['display_full']." to \'$new\' from \'$old\'";
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$result_changes[] = "kr_perf = '$new'";
			}
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Actual</td>
			<td>".$new."</td>
			<td>".$old."</td>
			<th>".$log_trans."</th>
			</tr>";*/
		}
		if(isset($_REQUEST['kr_correct_'.$ti])) {
			$fld = "kr_correct";
			$new = code($_REQUEST['kr_correct_'.$ti]);
			$old = $results[$ti]['kr_correct'];
			if($new!=$old) {
				$log_trans = "Updated Corrective Measures for ".$time[$ti]['display_full']." to \'$new\' from \'$old\'";
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$result_changes[] = "kr_correct = '$new'";
			}
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Actual</td>
			<td>".$new."</td>
			<td>".$old."</td>
			<th>".$log_trans."</th>
			</tr>";*/
		}
		if(count($result_changes)>0) {
			$sc = "UPDATE ".$dbref."_kpi_results SET ".implode(", ",$result_changes)." WHERE kr_kpiid = ".$obj_id." AND kr_timeid = ".$ti;
			db_update($sc);
			foreach($trans as $fld => $lt) {
				$v = array(
					'fld'=>$fld,
					'timeid'=>$ti,
					'text'=>$lt['lt'],
					'old'=>$lt['o'],
					'new'=>$lt['n'],
					'act'=>"E",
					'YN'=>"Y"
				); 
				logChanges($section,$obj_id,$v,code($sc));
			}
		}
	}
	//echo "</table>";


	echo "	<script type=text/javascript>
				document.location.href = '".$base[0]."_".$base[1].".php?page_id=".$_REQUEST['page_id']."&r[]=ok&r[]=KPI+".$id_labels_all[$section].$obj_id."+has+been+successfully+updated.';
			</script>";
	break;	//$_REQUEST['act']=="SAVE"
case "DELETE":
	$obj_sql = "UPDATE ".$dbref."_kpi SET kpi_active = false WHERE kpi_id = ".$obj_id;
	$mar = db_update($obj_sql);
				$v = array(
					'fld'=>"kpi_active",
					'timeid'=>0,
					'text'=>"Deleted KPI ".$id_labels_all[$section].$obj_id,
					'old'=>1,
					'new'=>0,
					'act'=>"D",
					'YN'=>"Y"
				);
				logChanges($section,$obj_id,$v,code($obj_sql));
	echo "	<script type=text/javascript>
				document.location.href = '".$base[0]."_".$base[1].".php?page_id=".$_REQUEST['page_id']."&r[]=ok&r[]=KPI+".$id_labels_all[$section].$obj_id."+has+been+successfully+deleted.';
			</script>";
	break;
}	//end switch $_REQUEST['act']




} else {





	include("manage_table_detail.php");


	$log_sql = "SELECT klog_date, CONCAT_WS(' ',tkname,tksurname) as klog_tkname, klog_transaction FROM ".$dbref."_kpi_log INNER JOIN assist_".$cmpcode."_timekeep ON klog_tkid = tkid WHERE klog_kpiid = '".$obj_id."' AND klog_yn = 'Y' ORDER BY klog_id DESC";
	displayAuditLog($log_sql,array('date'=>"klog_date",'user'=>"klog_tkname",'action'=>"klog_transaction"));



}

?>