<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
			<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
		<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
	
	<link rel="stylesheet" href="/assist.css" type="text/css">
	<link rel="stylesheet" href="inc/main.css" type="text/css">
	<script type ="text/javascript" src="inc/main.js"></script>
</head>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<div id=m1 style="font-size: 8pt; font-weight: bold;"><input type="radio" id="dept" name="radiom1" value="view_dept.php"  /><label for="dept">&nbsp;&nbsp;Departmental SDBIP&nbsp;&nbsp;</label>&nbsp;&nbsp;<input type="radio" id="top" name="radiom1" value="view_top.php"  /><label for="top">&nbsp;&nbsp;Top Layer SDBIP&nbsp;&nbsp;</label>&nbsp;&nbsp;<input type="radio" id="capital" name="radiom1" value="view_capital.php"  /><label for="capital">&nbsp;&nbsp;Capital Projects&nbsp;&nbsp;</label>&nbsp;&nbsp;<input type="radio" id="cashflow" name="radiom1" value="view_cashflow.php"  /><label for="cashflow">&nbsp;&nbsp;Monthly Cashflow&nbsp;&nbsp;</label>&nbsp;&nbsp;<input type="radio" id="revbysrc" name="radiom1" value="view_revbysrc.php"  /><label for="revbysrc">&nbsp;&nbsp;Revenue By Source&nbsp;&nbsp;</label>&nbsp;&nbsp;<input type="radio" id="dash" name="radiom1" value="view_dash.php" checked=checked /><label for="dash">&nbsp;&nbsp;YTD Dashboard&nbsp;&nbsp;</label>&nbsp;&nbsp;</div><script type=text/javascript>
		  $(function() {
			$("#m1").buttonset();
			$("#m1 input[type='radio']").click( function() {
			  document.location.href = $(this).val();
			});
		  });
	</script><div id=m2 style="padding-top: 5px; font-size: 7pt"><input type="radio" id="deptytd" name="radiom2" value="view_dash.php?page_id=deptytd" checked=checked /><label for="deptytd" style="background: #fafaff url();margin-right: 3px;">&nbsp;&nbsp;Departmental SDBIP&nbsp;&nbsp;</label><input type="radio" id="topytd" name="radiom2" value="view_dash.php?page_id=topytd"  /><label for="topytd" style="background: #fafaff url();margin-right: 3px;">&nbsp;&nbsp;Top Layer SDBIP&nbsp;&nbsp;</label><input type="radio" id="capitalytd" name="radiom2" value="view_dash.php?page_id=capitalytd"  /><label for="capitalytd" style="background: #fafaff url();margin-right: 3px;">&nbsp;&nbsp;Capital Projects&nbsp;&nbsp;</label><input type="radio" id="cashflowytd" name="radiom2" value="view_dash.php?page_id=cashflowytd"  /><label for="cashflowytd" style="background: #fafaff url();margin-right: 3px;">&nbsp;&nbsp;Monthly Cashflow&nbsp;&nbsp;</label><input type="radio" id="revbysrcytd" name="radiom2" value="view_dash.php?page_id=revbysrcytd"  /><label for="revbysrcytd" style="background: #fafaff url();margin-right: 3px;">&nbsp;&nbsp;Revenue By Source&nbsp;&nbsp;</label></div><script type=text/javascript>
		  $(function() {
			$("#m2").buttonset();
			$("#m2 input[type='radio']").click( function() {
			  document.location.href = $(this).val();
			});
		  });
	</script><script>$(function() { 
			//$("#dash").button("option","disabled",true); 
		});</script><h1><a href=view.php class=breadcrumb>View</a> >> YTD Dashboard</a> >> Departmental SDBIP</a></h1>        <script type="text/javascript">

        var chart;

        var chartData = [{country:"USA",visits:4252},
					{country:"China",visits:1882},
					{country:"Japan",visits:1809},
					{country:"Germany",visits:1322},
					{country:"UK",visits:1122},
					{country:"France",visits:1114},
					{country:"India",visits:984},
					{country:"Spain",visits:711},
					{country:"Netherlands",visits:665},
					{country:"Russia",visits:580},
					{country:"South Korea",visits:443},
					{country:"Canada",visits:441},
					{country:"Brazil",visits:395},
					{country:"Italy",visits:386},
					{country:"Australia",visits:384},
					{country:"Taiwan",visits:338},
					{country:"Poland",visits:328}];

         window.onload = function() {
            // Pie chart
            var chart1 = new AmCharts.AmPieChart();
            chart1.dataProvider = chartData;

			chart1.valueField = "visits";
            chart1.titleField = "country";
            chart1.angle = 5;
            chart1.depth3D = 5;
            chart1.innerRadius = 10;
            chart1.groupPercent = 3;
            chart1.startDuration = 1.5;
            chart1.startRadius = 100;
            chart1.marginLeft = 0;
            chart1.marginRight = 0;
            chart1.labelWidth = 100;

			var legend = new AmCharts.AmLegend();
			chart1.addLegend(legend);

			chart1.write("chartdiv1");


            // Column chart
			var chart2 = new AmCharts.AmSerialChart();
			chart2.dataProvider = chartData;
			chart2.categoryField = "country";
			chart2.marginTop = 15;
            chart2.marginLeft = 55;
            chart2.marginRight = 15;
            chart2.marginBottom = 80;
            chart2.angle = 30;
            chart2.depth3D = 15;

			var catAxis = chart2.categoryAxis;
			catAxis.gridCount = chartData.length;
			catAxis.gridPosition = "start";
			catAxis.labelRotation = 90;

			var graph = new AmCharts.AmGraph();
			graph.balloonText = "[[category]]: [[value]]";
			graph.valueField = "visits"
			graph.type="column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 0.8;
			chart2.addGraph(graph);

			var chartCursor = new AmCharts.ChartCursor();
			chartCursor.zoomable = false;
			chartCursor.cursorAlpha = 0;
			chartCursor.categoryBalloonEnabled = false;
			chart2.addChartCursor(chartCursor);
			chart2.write('chartdiv2');
		}

	    </script>

		<div id="chartdiv1" style="width: 50%; float:left; height: 400px;"></div>
		<div id="chartdiv2" style="width: 50%; float:left; height: 400px;"></div>
</body>
</html>