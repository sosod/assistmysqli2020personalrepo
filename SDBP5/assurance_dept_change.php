<?php 
	/* SETUP VARIABLES */
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",
		"dir",$table_fld."subid",
		$table_fld."repcate",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
	);
	$head0['dir'] = $mheadings['dir'][0];
	$head = array_merge($head0,$mheadings[$section]);
	//arrPrint($mheadings[$r_section]);
	$mheadings[$r_section]['kr_assurance_status'] = array(
		'h_client' => "Assurance Status",
		'h_ignite' => "Assurance Status",
		'id' => 998,
		'head_id' => 998,
		'section' => "KPI_R",
		'fixed' => 0,
		'i_sort' => 999,
		'c_sort' => 999,
		'c_list' => 0,
		'i_required' => 0,
		'c_required' => 0,
		'i_maxlen' => 0,
		'c_maxlen' => 0,
		'field' => "kr_assurance_status",
		'active' => 1,
		'glossary' => "",
		'h_type' => "STAT",
		'h_table' => "",
        );
	$mheadings[$r_section]['assurance_logs'] = array(
		'h_client' => "Assurance Logs",
		'h_ignite' => "Assurance Logs",
		'id' => 998,
		'head_id' => 998,
		'section' => "KPI_R",
		'fixed' => 0,
		'i_sort' => 999,
		'c_sort' => 999,
		'c_list' => 0,
		'i_required' => 0,
		'c_required' => 0,
		'i_maxlen' => 0,
		'c_maxlen' => 0,
		'field' => "assurance_logs",
		'active' => 1,
		'glossary' => "",
		'h_type' => "LOG",
		'h_table' => "",
        );
/*					echo "<tr>
							<td><input type=checkbox checked name=\"fields[]\" value='assurance_status' /></td>
							<td>Assurance Status</td>
						</tr>";
					echo "<tr>
							<td><input type=checkbox checked name=\"fields[]\" value='assurance_logs' /></td>
							<td>Assurance Logs</td>
						</tr>";
	*/

	$src = isset($_REQUEST['src']) ? $_REQUEST['src'] : "GENERATOR";
	if($src=="GRAPH" && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
		$_REQUEST['fields'] = array(
				0 => "dir",
				1 => "kpi_subid",
				2 => "kpi_topid",
				3 => "kpi_capitalid",
				4 => "kpi_gfsid",
				5 => "kpi_idpref",
				6 => "kpi_natoutcomeid",
				7 => "kpi_idpid",
				8 => "kpi_natkpaid",
				9 => "kpi_munkpaid",
				10 => "kpi_mtas",
				11 => "kpi_value",
				12 => "kpi_unit",
				13 => "kpi_conceptid",
				14 => "kpi_typeid",
				15 => "kpi_riskref",
				16 => "kpi_risk",
				17 => "kpi_riskratingid",
				18 => "kpi_wards",
				19 => "kpi_area",
				20 => "kpi_ownerid",
				21 => "kpi_baseline",
				22 => "kpi_pyp",
				23 => "kpi_perfstd",
				24 => "kpi_poe",
				25 => "kpi_calctype",
				26 => "results",
				27 => "kr_perf",
				28 => "kr_correct",
			);
		//$_REQUEST['r_from'] => from graph report
		//$_REQUEST['r_to'] => from graph report
		$_REQUEST['r_calc'] = "ytd";
		$_REQUEST['summary'] = "Y";
		//$_REQUEST['filter']['group_by_filter'] = array([0]=>#) => from graph where applicable
		//$_REQUEST['filter']['result'] = # => from graph
		//$_REQUEST['groupby'] => from graph
		$_REQUEST['filter']['kpi_wards'] = array("ANY");
		$_REQUEST['filter']['kpi_area'] = array(1);
		$_REQUEST['sort'] = array(
				0 => "dir",
				1 => "kpi_gfsid",
				2 => "kpi_idpref",
				3 => "kpi_natoutcomeid",
				4 => "kpi_idpid",
				5 => "kpi_natkpaid",
				6 => "kpi_munkpaid",
				7 => "kpi_mtas",
				8 => "kpi_value",
				9 => "kpi_unit",
				10 => "kpi_conceptid",
				11 => "kpi_typeid",
				12 => "kpi_riskref",
				13 => "kpi_risk",
				14 => "kpi_riskratingid",
				15 => "kpi_ownerid",
				16 => "kpi_baseline",
				17 => "kpi_pyp",
				18 => "kpi_perfstd",
				19 => "kpi_poe",
			);	
		$_REQUEST['output'] = "display";
		$_REQUEST['rhead'] = "";
	}
//arrPrint($_REQUEST);
		if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
			include("report_kpi_process.php");
		} else {
			include("report_kpi_generate.php");
		}
?>