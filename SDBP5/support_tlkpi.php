<?php 
$section = "TOP";
include("inc/header.php"); 

if(isset($_REQUEST['act']) && $_REQUEST['act']=="RESTORE") {
	if(isset($_REQUEST['i']) && checkIntRef($_REQUEST['i'])) {
		$obj_id = $_REQUEST['i'];
		$sql = "UPDATE ".$dbref."_top SET top_active = true WHERE top_id = ".$obj_id;
		$mar = db_update($sql);
		if($mar>0) {
				$v = array(
					'fld'=>"top_active",
					'timeid'=>0,
					'text'=>"Restored KPI ".$id_labels_all[$section].$obj_id,
					'old'=>0,
					'new'=>1,
					'act'=>"R",
					'YN'=>"Y"
				);
				logChanges($section,$obj_id,$v,code($sql));
			$result = array("ok","KPI ".$id_labels_all[$section].$obj_id." was successfully restored.");
		} else {	
			$result = array("info","No change was detected to KPI ".$id_labels_all[$section].$obj_id.".  It has possibly already been restored.");
		}
	} else {
		$result = array("error","Invalid KPI Reference.  KPI was not restored.");
	}
}

if(isset($result)) {
	displayResult($result);
}

$log_sql = "SELECT l.*, CONCAT_WS(' ',tkname,tksurname) as luser FROM ".$dbref."_top_log l INNER JOIN assist_".$cmpcode."_timekeep ON tkid = tlog_tkid WHERE tlog_act = 'D' ORDER BY tlog_id DESC";
$rs = getRS($log_sql);
$logs = array();
while($row = mysql_fetch_assoc($rs)) {
	if(!isset($logs[$row['tlog_topid']])) { $logs[$row['tlog_topid']] = $row; }
}
mysql_close();
?>
<table>
<thead>
	<tr>
		<th>Ref</th>
		<th>Directorate</th>
		<th>KPI Name</th>
		<th>KPI Owner</th>
		<th>Audit Log</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
<?php
$sql = "SELECT k.*, o.value as owner , d.value as subdir
		FROM ".$dbref."_top k
		INNER JOIN ".$dbref."_list_owner o ON top_ownerid = o.id
		INNER JOIN ".$dbref."_dir d ON k.top_dirid = d.id
		WHERE top_active = false";
$objects = mysql_fetch_alls($sql,"top_id");
foreach($objects as $obj_id => $obj) {
	echo "	<tr>
				<th>".$id_labels_all[$section].$obj_id."</th>
				<td>".$obj['subdir']."</td>
				<td>".$obj['top_value']."</td>
				<td>".$obj['owner']."</td>
				<td>";
	if(isset($logs[$obj_id])) {
		echo "Deleted by ".$logs[$obj_id]['luser']."<br />on ".date("d M Y H:i:s",$logs[$obj_id]['tlog_date']);
	} else {
		echo "N/A";
	}
		echo "	</td>
				<td><input type=button value=Restore class=isubmit id=".$obj_id." /></td>
			</tr>";
}
?>
</tbody>
</table>
<script type=text/javascript>
	$(document).ready(function() {
		$(".isubmit").click(function() {
			id = $(this).attr("id");
			if(confirm("Are you sure you wish to restore KPI <?php echo $id_labels_all[$section]; ?>"+id+"?")==true) {
				document.location.href = '<?php echo $self; ?>?act=RESTORE&i='+id;
			}
		});
	});
</script>
</body>
</html>