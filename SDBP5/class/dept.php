<?php

class DEPT extends KPI {

	protected $section = "KPI";
	protected $assurance_section = "KAS";
	
	protected $section_head = "Departmental SDBIP";

	protected $object_name = "KPI";
	protected $object_title = "Departmental KPI";
	
	protected $table_field = "kpi_";
	protected $table = "kpi";
	protected $id_field = "kpi";
	protected $detail_link = "kpi_value";
	protected $results_table_field = "kr_";
	protected $target_field = "kr_target";
	protected $actual_field = "kr_actual";
	protected $assurance_table_field = "kas_";
	protected $wa_table_field = array('wards'=>"kw",'area'=>"ka","kpi_wards"=>"kw","kpi_area"=>"ka");
	protected $time_ids = array(1,2,3,4,5,6,7,8,9,10,11,12);
	protected $first_month = 1;
	protected $time_increment = 1;
	
	protected $absolute_required_fields;
	
	const REFTAG = "D";
	
	const HEAD = "KPI";
	const RHEAD = "KPI_R";
	
	
	public function __construct() {
		parent::__construct(self::HEAD,self::RHEAD);
		$this->absolute_required_fields = array("kpi_subid","dir","kpi_wards","kpi_area","kpi_ownerid","kpi_calctype","kpi_targettype");
	}

	
	
	
	
	
	
	
	/****************
	SAVE FUNCTIONS
	*****************/
	
	public function saveUpdate($var,$old) {

		$obj_id = $var['obj_id'];
		$time_id = $var['time_id'];
		$section = $var['section'];
		$actual = $var['kr_actual'];
		$perf = $this->code($var['kr_perf']);
		$correct = $this->code($var['kr_correct']);
		$poe = isset($var['poe']) ? $this->code($var['poe']) : "";
		$attachment = array('poe'=>$poe,'attach'=>array());
			$attachment['attach'] = isset($old['attachment']['attach']) ? $old['attachment']['attach'] : array();
			foreach($var['new_attach'] as $key => $a) {
				if(!isset($attachment['attach'][$key])) {
					$attachment['attach'][$key] = $a;
				} else {
					$attachment['attach'][] = $a;
				}
			}

		$headings = $this->getModuleHeadings(array("KPI_R"));
		$object = $this->getObject($obj_id);
		$time = $this->getTime(array($time_id));
		$result = array(0=>"ok",1=>"Update to ".$this->section_details['code'].$obj_id." saved successfully.");
		//$old = $this->getUpdateRecord($obj_id,$time_id);
		
		//validate change
		/*if($old['kr_actual']!=$actual
			|| $old['kr_perf']!=$perf
			|| $old['kr_correct']!=$correct
			|| $old['attachment']['poe']!=$attachment['poe']
		) {*/
		//DEVELOPMENT TESTING
	//if($actual>100) {
	//		$result[0] = "error";
	//		$result[1] = "Development testing!!!";
	//} else {
		if($old['kr_actual']!=$actual
			|| $old['kr_perf']!=$perf
			|| $old['kr_correct']!=$correct
			|| $old['attachment']['poe']!=$attachment['poe']
			|| $old['attachment']['attach']!=$attachment['attach']
		) {
			//update result
			$attach = serialize($attachment);
			$lsql = "UPDATE ".$this->getDBRef()."_kpi_results SET
				kr_actual = ".$actual.",
				kr_perf = '".$perf."',
				kr_correct = '".$correct."',
				kr_attachment = '".$attach."',
				kr_update = 1,
				kr_updateuser = '".$this->getUserID()."',
				kr_updatedate = now()
				".($old['kr_assurance_status']==KPI::ASSURANCE_REJECT ? ", kr_assurance_status=".KPI::ASSURANCE_UPDATED:"")."
				WHERE kr_kpiid = ".$obj_id." AND kr_timeid = ".$time_id;
			$mar = $this->db_update($lsql);
			//log update
			$log = array();
			if($old['kr_actual']!=$actual) {
				$fld = "kr_actual";
				$new = $actual;
				$n = $this->KPIresultDisplay($actual,$object['kpi_targettype']);
				$o = $this->KPIresultDisplay($old[$fld],$object['kpi_targettype']);
				$h = $headings['KPI_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['kr_perf']!=$perf) {
				$fld = "kr_perf";
				$new = $perf;
				$n = $this->decode($perf);
				$o = $this->decode($old[$fld]);
				$h = $headings['KPI_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['kr_correct']!=$correct) {
				$fld = "kr_correct";
				$new = $correct;
				$n = $this->decode($correct);
				$o = $this->decode($old[$fld]);
				$h = $headings['KPI_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['attachment']['poe']!=$attachment['poe']) {
				$fld = "kr_attachment_poe";
				$new = $poe;
				$n = $this->decode($poe);
				$o = $this->decode($old['attachment']['poe']);
				$h = $headings['KPI_R']['kr_attachment'];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old['attachment']['poe'],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['attachment']['attach']!=$attachment['attach']) {
				$fld = "kr_attachment_files";
				$new = $var['new_attach'];
				$new_files = array();
				foreach($new as $a => $x) { $new_files[] = $x['original_filename']; }
				$n = implode(", ",$new_files);
				$old_files = $old['attachment']['attach'];
				$h = $headings['KPI_R']['kr_attachment'];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>serialize($new),
					'old'=>serialize($old_files),
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$time_id]['display_short'])
				);
			}
			foreach($log as $v) {
				$this->logChanges($section,$obj_id,$v,$this->code($lsql));
			}
			
			//EMAIL NOTIFICATION TO ASSURANCE USER
			if($old['kr_assurance_status']==KPI::ASSURANCE_REJECT) {
				$assurance = $this->getAssuranceRecord($obj_id, $time_id);
				
				if(isset($assurance['kas_user']) && strlen($assurance['kas_user'])>0) {
					$to = array('name'=>$this->getAUserName($assurance['kas_user']),'email'=>$this->getAnEmail($assurance['kas_user']));
					$from = array('name'=>$this->getAUserName($this->getUserID()),'email'=>$this->getAnEmail($this->getUserID()));
					//$from = array('name'=>$this->getAUserName($this->getUserID()),'email'=>getAnEmail($this->getUserID()));
					//$to = "janet@actionassist.co.za";
					//$from = "helpdesk@actionassist.co.za";
					$subject = $this->getModTitle()." Assurance Review Response [".self::REFTAG.$obj_id."]";
					$message = "
					<p>".$from['name']." has updated ".$this->object_title." ".self::REFTAG.$obj_id." for time period ".$time[$time_id]['display_short']." in response to your Assurance Review.</p>
					<p>Please log onto Assist to review this update.</p>
					";
					//.serialize(array('name'=>$this->getAUserName($this->getUserID()),'email'=>getAnEmail($this->getUserID())));
					$content_type = "HTML";
					//if(strlen($to['email'])>0) {
						$emailObj = new ASSIST_EMAIL($to,$subject,$message,$content_type,"","",$from);
						$emailObj->sendEmail();
					//}
				}
			}
			
		} else {
			$result[0] = "info";
			$result[1] = "No change found to be saved.";
		}
	//}	//development purposes
		//return result*/
		return $result;
	}
	
	public function saveUpdateAttachments($i,$t,$new,$old) {
		$headings = $this->getModuleHeadings(array("KPI_R"));
		$time = $this->getTime(array($t));
		$section = $this->section;
		
		$attach = array();
		$old_files = array();
		foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
		$new_files = array();
		foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }
		
		$update = array('poe'=>$old['poe'],'attach'=>$attach);
		$sql = "UPDATE ".$this->getDBRef()."_kpi_results SET kr_attachment = '".serialize($update)."' WHERE kr_kpiid = ".$i." AND kr_timeid = ".$t;
		$this->db_update($sql);
		
		$fld = "kr_attachment_files";
		$n = implode(", ",$new_files);
		$o = implode(", ",$old_files);
		$h = $headings['KPI_R']['kr_attachment'];
		$log = array(
			'fld'=>$fld,
			'new'=>serialize($new),
			'old'=>serialize($old['attach']),
			'act'=>"U",
			'YN'=>"Y",
			'timeid'=>$t,
			'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$t]['display_short'])
		);
		$this->logChanges($section,$i,$log,$this->code($sql));
	}
	
	
	public function deleteAttachment($kpi_id,$time_id,$attach_id) {
		$section = $this->section;
		$attach = $this->getAttachmentDetails($kpi_id,$time_id,$attach_id);
		//$this->arrPrint($attach);
		if($attach[0]===true) {
			$this->checkFolder($this->getAttachmentDeleteFolder());
			$old_path = "../../files/".$this->getCmpCode()."/".$this->getAttachFolder()."/".$attach['system_filename'];
			$new_path = "../../files/".$this->getCmpCode()."/".$this->getAttachmentDeleteFolder()."/".$section."_".date("YmdHis")."_".$attach['system_filename'];
			if(copy($old_path,$new_path)) {
				unlink($old_path);
			}
			$record = $attach[2]; //$this->getUpdateRecord($kpi_id,$time_id);
			$a = $record['attachment'];
			$old_attach = $a['attach'][$attach_id]['original_filename'];

			unset($a['attach'][$attach_id]);
			$n = serialize($a);
			$sql = "UPDATE ".$this->getDBRef()."_kpi_results SET kr_attachment = '$n' WHERE kr_kpiid = ".$kpi_id." AND kr_timeid = ".$time_id." ";
			$this->db_update($sql);
			
			$headings = $this->getModuleHeadings(array("KPI_R"));
			$time = $this->getTime(array($time_id));
			
			$old = $record;
			$new = $a;
			
			$attach = array();
			$old_files = array();
			foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
			$new_files = array();
			foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }
			
			$fld = "kr_attachment_files";
			$n = implode(", ",$new_files);
			$o = implode(", ",$old_files);
			$h = $headings['KPI_R']['kr_attachment'];
			$log = array(
				'fld'=>$fld,
				'new'=>serialize($new),
				'old'=>serialize($old['attach']),
				'act'=>"U",
				'YN'=>"Y",
				'timeid'=>$time_id,
				'text'=>$this->code("Deleted ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '".$old_attach."' from time period ".$time[$time_id]['display_short'])
			);
			$this->logChanges($section,$kpi_id,$log,$this->code($sql));
			return true;
		} else {
			return false;
		}
		
	}
	
	
	
	
	
	
	public function saveAssuranceUpdate($var,$old) {

		$obj_id = $var['obj_id'];
		$time_id = $var['time_id'];
		$section = $var['section'];

		$object = $this->getObject($obj_id);
		$time = $this->getTime(array($time_id));
		$result = array(0=>"ok",1=>"Update to ".$this->section_details['code'].$obj_id." saved successfully.");
		
		$sign_off = $var['ass_signoff']==1 ? KPI::ASSURANCE_ACCEPT : KPI::ASSURANCE_REJECT;
		$response = $this->code($var['ass_response']);
		$user = $var['ass_updateuser'];
		//$deadline = strlen($var['ass_deadline'])==0 || $var['ass_deadline']=="X" ? "0000-00-00" : date("Y-m-d",strtotime($var['ass_deadline']));
		if($var['ass_timecheck']=="open" || $sign_off==KPI::ASSURANCE_ACCEPT) {
			$deadline = 0; 
		} else {
			$deadline = date("Y-m-d",strtotime($var['ass_deadline']));
		} 
		//$result[1].=":".$deadline.":".$var['ass_deadline'].":".$var['ass_timecheck'];
		/*
		$attachment = array();
			$attachment = isset($old['attachment']) ? $old['attachment'] : array();
			foreach($var['new_attach'] as $key => $a) {
				if(!isset($attachment[$key])) {
					$attachment[$key] = $a;
				} else {
					$attachment[] = $a;
				}
			}
		//$attachment = $attachment['attach'];

*/

		//$headings = $this->getModuleHeadings(array("KPI_R"));
		
		$attach = "";//serialize($attachment);
		if($old['kas_id']===false) {
			$lsql = "INSERT INTO ".$this->getDBRef()."_kpi_assurance SET
					kas_kpiid = ".$obj_id.",
					kas_timeid = ".$time_id.",
					kas_updateuser = '".$user."',
					kas_response = '".$response."',
					kas_result = ".$sign_off.",
					kas_deadline = '".$deadline."',
					kas_attachment = '".$attach."',
					kas_status = 1,
					kas_user = '".$this->getUserID()."',
					kas_date = now()
					";
			$mid = $this->db_insert($lsql);
			$rsql = "UPDATE ".$this->getDBRef()."_kpi_results SET 
						kr_assurance_status = ".$sign_off." ,
						kr_assurance_deadline = '".$deadline."',
						kr_assurance_updateuser = '".$user."'
					WHERE 
						kr_kpiid = ".$obj_id."
						AND
						kr_timeid = ".$time_id."
					";
			$this->db_update($rsql);



					$fld = "";
					$new = $mid;
					//$n = $sign_off==KPI::ASSURANCE_ACCEPT ? "Yes" : "No";
					//$o = $old[$fld]==KPI::ASSURANCE_ACCEPT ? "Yes" : "No";
					//$h = "Sign-off";//$headings['KPI_R'][$fld];
					$log = array(
						'fld'=>$fld,
						'new'=>$new,
						'old'=>"0",
						'act'=>"AC",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Added new Assurance review for time period ".$time[$time_id]['display_short'].". The update was ".($sign_off==KPI::ASSURANCE_REJECT?"not ":"")."signed-off.")
					);
					$this->logChanges($this->assurance_section,$obj_id,$log,$this->code($lsql));




		} elseif($old['kas_result']!=$sign_off
			|| $old['kas_response']!=$response
			|| $old['kas_updateuser']!=$user
			|| $old['kas_deadline']!=$deadline
			|| $old['attachment']!=$attachment
		) {
			//update result
				$lsql = "UPDATE ".$this->getDBRef()."_kpi_assurance SET
					kas_updateuser = '".$user."',
					kas_response = '".$response."',
					kas_result = ".$sign_off.",
					kas_deadline = '".$deadline."',
					kas_attachment = '".$attach."',
					kas_status = 1,
					kas_user = '".$this->getUserID()."',
					kas_date = now()
					WHERE kas_kpiid = ".$obj_id." AND kas_timeid = ".$time_id;
				$mar = $this->db_update($lsql);
				$rsql = "UPDATE ".$this->getDBRef()."_kpi_results SET 
						kr_assurance_status = ".$sign_off." ,
						kr_assurance_deadline = '".$deadline."',
						kr_assurance_updateuser = '".$user."'
						WHERE 
							kr_kpiid = ".$obj_id."
							AND
							kr_timeid = ".$time_id."
						";
				$this->db_update($rsql);
				//log update
				$log = array();
/*				if($old['kr_perf']!=$perf) {
					$fld = "kr_perf";
					$new = $perf;
					$n = $this->decode($perf);
					$o = $this->decode($old[$fld]);
					$h = $headings['KPI_R'][$fld];
					$log[$fld] = array(
						'fld'=>$fld,
						'new'=>$new,
						'old'=>$old[$fld],
						'act'=>"U",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
					);
				}*/
				if($old['kas_result']!=$sign_off) {
					$fld = "kas_result";
					$new = $sign_off;
					$n = $sign_off==KPI::ASSURANCE_ACCEPT ? "Yes" : "No";
					$o = $old[$fld]==KPI::ASSURANCE_ACCEPT ? "Yes" : "No";
					$h = "Sign-off";//$headings['KPI_R'][$fld];
					$log[$fld] = array(
						'fld'=>$fld,
						'new'=>$new,
						'old'=>$old[$fld],
						'act'=>"A",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Updated ".($h)." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
					);
				}
				if($old['kas_response']!=$response) {
					$fld = "kas_response";
					$new = $response;
					$n = $this->decode($response);
					$o = $this->decode($old[$fld]);
					$h = "Response";//$headings['KPI_R'][$fld];
					$log[$fld] = array(
						'fld'=>$fld,
						'new'=>$new,
						'old'=>$old[$fld],
						'act'=>"A",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Updated ".($h)." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
					);
				}
				if($old['kas_deadline']!=$deadline) {
					$fld = "kas_deadline";
					$new = $deadline;
					$n = $deadline=="0000-00-00" ? "N/A" : $deadline;
					$o = $old[$fld]=="0000-00-00" ? "N/A" : $old[$fld];
					$h = "Update Deadline";//$headings['KPI_R'][$fld];
					$log[$fld] = array(
						'fld'=>$fld,
						'new'=>$new,
						'old'=>$old[$fld],
						'act'=>"A",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Updated ".($h)." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
					);
				}
				if($old['kas_updateuser']!=$user) {
					$fld = "kas_updateuser";
					$new = $user;
					$n = $this->getAUserName($user);
					$o = $this->getAUserName($old[$fld]);
					$h = "Update Deadline";//$headings['KPI_R'][$fld];
					$log[$fld] = array(
						'fld'=>$fld,
						'new'=>$new,
						'old'=>$old[$fld],
						'act'=>"A",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Updated ".($h)." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
					);
				}
				/*if($old['attachment']!=$attachment) {
					$fld = "kas_attachment";
					$new = $var['new_attach'];
					$new_files = array();
					foreach($new as $a => $x) { $new_files[] = $x['original_filename']; }
					$n = implode(", ",$new_files);
					$old_files = $old['attachment'];
					$h = "Attachment";
					$log[$fld] = array(
						'fld'=>$fld,
						'new'=>serialize($new),
						'old'=>serialize($old_files),
						'act'=>"A",
						'YN'=>"Y",
						'timeid'=>$time_id,
						'text'=>$this->code("Added new Attachment(s) '$n' to time period ".$time[$time_id]['display_short'])
					);
				}*/
				foreach($log as $v) {
					$this->logChanges($this->assurance_section,$obj_id,$v,$this->code($lsql));
				}
		} else {
			$result[0] = "info";
			$result[1] = "No change found to be saved.";
		}
		
		
		//Notify recipient if rejected
		if($result[0]=="ok" && $sign_off==KPI::ASSURANCE_REJECT) {
			
			
			$update_deadline = "";
			if($var['ass_timecheck']=="open") {
				if($time[$time_id]['deadline'] < time()){
					$update_deadline = date("d F Y",$time[$time_id]['deadline']);
				} else {
					$update_deadline = false;
				}
			} else {
				$update_deadline = $var['ass_deadline'];
			}
			$response = $this->decode($response);
			$content_type = "HTML";
			$to = array('name'=>$this->getAUserName($user),'email'=>$this->getAnEmail($user));//recipient as per $user
			$from = array('name'=>$this->getAUserName($this->getUserId()),'email'=>$this->getAnEmail($this->getUserID()));//sender = user logged in
			$cc = "";//CC -> later version
			$bcc = "";//N/A
			$subject = $this->getModTitle()." Assurance Review [".self::REFTAG.$obj_id."]";//MODTITLE Assurance Review [KPI REF]
			$message = "
			<p>".$from['name']." has reviewed your update to ".$this->object_title." ".self::REFTAG."".$obj_id." for time period ".$time[$time_id]['display_short'].".</p>
			<p>The update has <span class='b u'>not</span> been signed-off with the following reason given:<br /><span class=i>".str_replace(chr(10),"<br />",$response)."</span></p>
			".($update_deadline!==false && $update_deadline != "0" && $update_deadline != "01 January 1970" && $update_deadline!="0000-00-00" ? "
			<p>You have until <span class=overdue>".date("d F Y",strtotime($update_deadline))."</span> to make the required changes.</p>
			" : "
			<p>Please log onto Assist to make the required changes.</p>
			")."
			<p>&nbsp;</p>
			<p class=i>Please note: KPIs under Assurance Review can only be updated from the Action List on the Frontpage Dashboard.
			<br />If you are unable to find the KPI on your Action List check your 'Days to Display' setting under your 'My Profile' (Main Menu) to confirm if the deadline falls within the given time frame.</p> 
			";
			
			$email = new ASSIST_EMAIL($to,$subject,$message,$content_type,$cc,$bcc,$from);
			$email->sendEmail();
		}
		
		
		
	//}	//development purposes
		//return result*/
		return $result;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**********************************
		GET FUNCTIONS
	***********************************/

	
	public function getUpdateFormDetails($id) {
		
		//$data = $this->getUpdateDetailsArray();
		$sql = "SELECT kpi_value, kpi_unit, kpi_poe, kpi_targettype, kpi_calctype, CONCAT(d.value,' - ',s.value) as dir, ct.value as ct 
				FROM ".$this->getDBRef()."_kpi k
				INNER JOIN ".$this->getDBRef()."_subdir s
				  ON k.kpi_subid = s.id
				INNER JOIN ".$this->getDBRef()."_dir d
				  ON s.dirid = d.id
				INNER JOIN ".$this->getDBRef()."_list_calctype ct
				  ON ct.code = k.kpi_calctype
				WHERE kpi_id = ".$id;
		$row = $this->mysql_fetch_one($sql);
		$data['obj_id'] = $id;
		$data['dir'] = ($row['dir']);
		$data['value'] = ($row['kpi_value']);
		$data['unit'] = ($row['kpi_unit']);
		$data['poe'] = ($row['kpi_poe']);
if($row['kpi_calctype']=="LASTR") {
	$row['kpi_calctype'] = "LASTREV";
	$row['ct'] = "Reverse Last Value (LASTREV)";
}
		$data['calctype'] = ($row['kpi_calctype']);
		$data['ct'] = ($row['ct'])." (".$data['calctype'].")";

		$data['targettype'] = ($row['kpi_targettype']);
		$sql = "SELECT kr.*, CONCAT(tk.tkname,' ',tk.tksurname) as updateuser 
				FROM ".$this->getDBRef()."_kpi_results kr
				LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep tk
				  ON tk.tkid = kr.kr_updateuser
				WHERE kr.kr_kpiid = ".$id." ORDER BY kr.kr_timeid";
		$results = $this->mysql_fetch_all_by_id($sql,"kr_timeid");  //if($_SERVER['REMOTE_ADDR']=="105.233.22.159") { arrPrint($results); }
		$targets = array();
		$actuals = array();
		for($ti=1;$ti<=12;$ti++) {
			if(isset($results[$ti])) {
				$r = $results[$ti];
				$r['kr_attachment'] = unserialize($r['kr_attachment']);  //if($_SERVER['REMOTE_ADDR']=="105.233.22.159") { echo $ti; arrPrint($r['kr_attachment']); }
				$data['results']['target'][$ti] = $r['kr_target'];		$targets[$ti] = $r['kr_target'];
				$data['results']['actual'][$ti] = $r['kr_actual'];		$actuals[$ti] = $r['kr_actual'];
				//$data['results']['perfcomm'][$ti] = $this->decode($r['kr_perf']);
				//$data['results']['correct'][$ti] = $this->decode($r['kr_correct']);
				//$data['results']['poe'][$ti] = $this->decode($r['kr_attachment']['poe']);
				$data['results']['perfcomm'][$ti] = ($r['kr_perf']);
				$data['results']['correct'][$ti] = ($r['kr_correct']);
				$data['results']['poe'][$ti] = (isset($r['kr_attachment']['poe']) ? ($r['kr_attachment']['poe']) : "");
				$data['results']['attach'][$ti] = array();
				if(is_array($r['kr_attachment']['attach'])) {
					foreach($r['kr_attachment']['attach'] as $key=>$a) {
						$data['results']['attach'][$ti][$id."_".$ti."_".$key] = array(
							'original_filename'=>$a['original_filename'],
							'system_filename'=>$a['system_filename'],
						);
					}
				}
				$data['results']['update'][$ti] = $r['kr_update'];
				$data['results']['updateuser'][$ti] = $r['updateuser'];
				$data['results']['updatedate'][$ti] = $r['kr_updatedate'];
				if($r['kr_assurance_status']==KPI::ASSURANCE_ACCEPT) {
					$data['results']['ass_status'][$ti] = "Y";
				} elseif($r['kr_assurance_status']==KPI::ASSURANCE_REJECT) {
					$data['results']['ass_status'][$ti] = "N";
				} elseif($r['kr_assurance_status']==KPI::ASSURANCE_UPDATED) {
					$data['results']['ass_status'][$ti] = "warn";
				} else {
					$data['results']['ass_status'][$ti] = "info";
				}
			} else {
					$data['results']['ass_status'][$ti] = "info";
			}
			$data['results']['r'][$ti] = $this->KPIcalcResult(array('target'=>$targets,'actual'=>$actuals),$data['calctype'],array(1,$ti),$ti);
			$data['results']['ytd_r'][$ti] = $this->KPIcalcResult(array('target'=>$targets,'actual'=>$actuals),$data['calctype'],array(1,$ti),"ALL");
			$data['results']['target'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['target']*1,$data['targettype']);
			$data['results']['actual'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['actual']*1,$data['targettype']);
			$data['results']['ytd_target'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['target']*1,$data['targettype']);
			$data['results']['ytd_actual'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['actual']*1,$data['targettype']);
		}
		
		//		$data = array("sbc");
		return $data;
	}
		









	public function getAssuranceUpdateFormDetails($id) {
		//$data = $this->getUpdateDetailsArray();
		$sql = "SELECT kpi_value, kpi_unit, kpi_poe, kpi_targettype, kpi_calctype, CONCAT(d.value,' - ',s.value) as dir, ct.value as ct 
				FROM ".$this->getDBRef()."_kpi k
				INNER JOIN ".$this->getDBRef()."_subdir s
				  ON k.kpi_subid = s.id
				INNER JOIN ".$this->getDBRef()."_dir d
				  ON s.dirid = d.id
				INNER JOIN ".$this->getDBRef()."_list_calctype ct
				  ON ct.code = k.kpi_calctype
				WHERE kpi_id = ".$id;
		$row = $this->mysql_fetch_one($sql);
		/*$data['dir'] = $this->decode($row['dir']);
		$data['value'] = $this->decode($row['kpi_value']);
		$data['unit'] = $this->decode($row['kpi_unit']);
		$data['poe'] = $this->decode($row['kpi_poe']);
		$data['calctype'] = $this->decode($row['kpi_calctype']);
		$data['ct'] = $this->decode($row['ct'])." (".$data['calctype'].")";
		$data['targettype'] = $this->decode($row['kpi_targettype']);*/
		$data['dir'] = ($row['dir']);
		$data['value'] = ($row['kpi_value']);
		$data['unit'] = ($row['kpi_unit']);
		$data['poe'] = ($row['kpi_poe']);
		$data['calctype'] = ($row['kpi_calctype']);
		$data['ct'] = ($row['ct'])." (".$data['calctype'].")";
		$data['targettype'] = ($row['kpi_targettype']);
		$sql = "SELECT kr.*, CONCAT(tk.tkname,' ',tk.tksurname) as updateuser 
				FROM ".$this->getDBRef()."_kpi_results kr
				LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep tk
				  ON tk.tkid = kr.kr_updateuser
				WHERE kr.kr_kpiid = ".$id." ORDER BY kr.kr_timeid";
		$results = $this->mysql_fetch_all_by_id($sql,"kr_timeid");  //if($_SERVER['REMOTE_ADDR']=="105.233.22.159") { arrPrint($results); }
		$targets = array();
		$actuals = array();
		for($ti=1;$ti<=12;$ti++) {
			if(isset($results[$ti])) {
				$r = $results[$ti];
/***************************************
* START OF HACK TO GET AROUND bug IN serialize FUNCTION RELATING TO " ' : ; CHARACTERS IN STRINGS
****************************************/
if(strlen($r['kr_attachment'])>0) {
		//get kr_attachment string
	$w = $r['kr_attachment'];
		//replace double quotes " with a double pipe place holder
	$x = str_replace("&quot;","||",$w);
		//restore HTML tags
	$x = $this->decode($x);

		//break up the serialized array based on the semi-colon
	$y = explode(";",$x);
		//temp store the POE string portion
	$z = $y[1];
		//replace POE string portion with a blank string
	$y[1] = 's:0:""';
		//put serialized array back together
	$r['kr_attachment'] = implode(";",$y);
		//break up the POE temp string on the serialized " placeholders
	$x = explode('"',$z);
		//replace the double pipe with the original double quotes character
	$y = str_replace('||','"',$x[1]);
//	$y = $this->code($y);
		//unserialize the serialized array with blank POE string place holder
	$r['kr_attachment'] = unserialize($r['kr_attachment']);
		//replace the blank temp POE string with the original
	$r['kr_attachment']['poe'] = $y;
} else {
	$r['kr_attachment'] = array('poe'=>"",'attach'=>array());
}
/***************************************
* END OF serialize HACK
****************************************/


				//$r['kr_attachment'] = unserialize($r['kr_attachment']);  //if($_SERVER['REMOTE_ADDR']=="105.233.22.159") { echo $ti; arrPrint($r['kr_attachment']); }
				$data['results']['target'][$ti] = $r['kr_target'];		$targets[$ti] = $r['kr_target'];
				$data['results']['actual'][$ti] = $r['kr_actual'];		$actuals[$ti] = $r['kr_actual'];
				//$data['results']['perfcomm'][$ti] = $this->decode($r['kr_perf']);
				//$data['results']['correct'][$ti] = $this->decode($r['kr_correct']);
				//$data['results']['poe'][$ti] = $this->decode($r['kr_attachment']['poe']);
				$data['results']['perfcomm'][$ti] = ($r['kr_perf']);
				$data['results']['correct'][$ti] = ($r['kr_correct']);
				$data['results']['poe'][$ti] = (isset($r['kr_attachment']['poe']) ? ($r['kr_attachment']['poe']) : "");
				$data['results']['attach'][$ti] = array();
				if(is_array($r['kr_attachment']['attach'])) {
					foreach($r['kr_attachment']['attach'] as $key=>$a) {
						$data['results']['attach'][$ti][$id."_".$ti."_".$key] = array(
							'original_filename'=>$a['original_filename'],
							'system_filename'=>$a['system_filename'],
						);
					}
				}
				$data['results']['update'][$ti] = $r['kr_update'];
				$data['results']['updateuser'][$ti] = $r['updateuser'];
				$data['results']['updateuser_id'][$ti] = $r['kr_updateuser'];
				$data['results']['updatedate'][$ti] = $r['kr_updatedate'];
				if($r['kr_assurance_status']==KPI::ASSURANCE_ACCEPT) {
					$data['results']['ass_status'][$ti] = "Y";
				} elseif($r['kr_assurance_status']==KPI::ASSURANCE_REJECT) {
					$data['results']['ass_status'][$ti] = "N";
				} elseif($r['kr_assurance_status']==KPI::ASSURANCE_UPDATED) {
					$data['results']['ass_status'][$ti] = "warn";
				} else {
					$data['results']['ass_status'][$ti] = "info";
				}
			} else {
					$data['results']['ass_status'][$ti] = "info";
						
			}
			$data['results']['r'][$ti] = $this->KPIcalcResult(array('target'=>$targets,'actual'=>$actuals),$data['calctype'],array(1,$ti),$ti);
			$data['results']['ytd_r'][$ti] = $this->KPIcalcResult(array('target'=>$targets,'actual'=>$actuals),$data['calctype'],array(1,$ti),"ALL");
			$data['results']['target'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['target']*1,$data['targettype']);
			$data['results']['actual'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['actual']*1,$data['targettype']);
			$data['results']['ytd_target'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['target']*1,$data['targettype']);
			$data['results']['ytd_actual'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['actual']*1,$data['targettype']);
		}
			
				//$data = array("sbc");
		return $data;
	}
		


	public function getAssuranceHistory($obj_id,$time_id) {
		//$data = array("help");
		$sql = "SELECT
				kas.kas_id as id
				, kas.kas_kpiid as obj_id
				, kas.kas_timeid as time_id
				, kas.kas_user as ass_userid
				, CONCAT(tku.tkname,' ',tku.tksurname) as ass_user
				, kas.kas_date as ass_date
				, kas.kas_result as sign_off
				, kas.kas_result as ass_result
				, kas.kas_response as response
				, kas.kas_attachment as attachment
				, kas.kas_updateuser as kr_userid
				, CONCAT(tkr.tkname,' ',tkr.tksurname) as kr_user
				, kas.kas_deadline as kr_deadline
				, kr.kr_assurance_status as update_status
				FROM ".$this->getDBRef()."_kpi_assurance kas
				INNER JOIN assist_".$this->getCmpCode()."_timekeep tku
				  ON tku.tkid = kas.kas_user 
				INNER JOIN assist_".$this->getCmpCode()."_timekeep tkr
				  ON tkr.tkid = kas.kas_updateuser
				INNER JOIN ".$this->getDBRef()."_kpi_results kr
				  ON kr.kr_kpiid = kas.kas_kpiid AND kr.kr_timeid = kas.kas_timeid
				WHERE kas.kas_kpiid = $obj_id AND kas.kas_timeid = $time_id 
				";
				//$data['final']['id']=0;
				//$data['final']['response'] = $sql;
		$row = $this->mysql_fetch_one($sql);
		
		$ra = unserialize($row['attachment']);
		unset($row['attachment']);
				if(is_array($ra)) {
					foreach($ra as $key=>$a) {
						$row['attachment'][$obj_id."_".$time_id."_".$key] = array(
							'original_filename'=>$a['original_filename'],
							'system_filename'=>$a['system_filename'],
						);
					}
				}
				
		
		$data['final'] = $row;
		if(isset($data['final']['sign_off'])) {
			if(($data['final']['sign_off'])==KPI::ASSURANCE_ACCEPT) {
				$data['final']['sign_off'] = "Yes";
			} elseif($data['final']['sign_off']==KPI::ASSURANCE_REJECT) {
				if($data['final']['update_status']==KPI::ASSURANCE_UPDATED) {
					$data['final']['sign_off'] = "No (Latest update pending review)";
				} else {
					$data['final']['sign_off'] = "No";
				}
			} elseif($data['final']['sign_off']==KPI::ASSURANCE_UPDATED) {
				$data['final']['sign_off'] = "No (Latest update pending review)";
			} else {
				$data['final']['sign_off'] = "Not available";
			}
		}
		
		$sql = "SELECT 
				klog_id as id
				, klog_tkid as log_tkid
				, CONCAT(tkname,' ',tksurname) as log_user
				, klog_date as log_date
				, klog_transaction as log_response
				FROM ".$this->getDBRef()."_kpi_log 
				INNER JOIN assist_".$this->getCmpCode()."_timekeep 
				  ON klog_tkid = tkid
				WHERE klog_kpiid = $obj_id AND klog_timeid = $time_id
				  AND klog_act IN ('A','AC')
				ORDER BY klog_id DESC
				";
		$rows = $this->mysql_fetch_all($sql);
		$d = array();
		foreach($rows as $r) {
			$d[$r['log_date']][] = $r['log_response'];
			if(!isset($data['log'][$r['log_date']])) $data['log'][$r['log_date']] = $r;
		}
		foreach($data['log'] as $i => $a) {
			$data['log'][$i]['log_response'] = "<ul><li>".implode("</li><li>",$d[$i])."</li></ul>";
			$data['log'][$i]['logs'] = $d[$i];
		}
		
		return $data;
	}












	
	public function getAssocID($s) {
		switch($s) {
			case "TOP": return "topid"; break;
			case "CAP": return "capitalid"; break;
		}
		return false;
	}
	

}



?>