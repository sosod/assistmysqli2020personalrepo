<?php

class SDBP5_TIME extends SDBP5_HELPER {

	const ACTIVE = 1;
	
	private $time_settings;

	public function __construct($cc="",$autojob=false) { 
	//echo "sdbp5_time created<br />";
		parent::__construct("client",$cc,$autojob);
		$this->time_settings = array(
			'TOP'=>array(3,6,9,12),
			'ALL'=>array(1,2,3,4,5,6,7,8,9,10,11,12),
		);
		//$this->arrPrint($this->time_settings);
	}

	
	public function getAllTime($key="end") {
		$time = $this->getMyTime("ALL",$key);
		return $time;
	}

	
	public function getMyTime($section,$key="end") {
		$time_ids = $this->getTimeIDs($section);
		if(count($time_ids)>0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id IN (".implode(",",$time_ids).") AND active & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY start_date";
			$t = $this->mysql_fetch_all($sql);
			$time = $this->procesself($t,$key);
		} else {
			$time = array();
		}
		return $time;
	}
	
	public function getTopTime() {
		$t1 = $this->getAllTime();
		$time = array();
		foreach($t1 as $i => $t) {
			switch($t['id']) {
			case 3: case 6: case 9: case 12:
				$t['start'] = $start;
				$time[$i] = $t;
				break;
			case 1: case 4: case 7: case 10:
				$start = $t['start'];
				break;
			default:
				break;
			}
		}
		return $time;
	}
	
	
	
	private function getTimeIDs($s) {
		if(isset($this->time_settings[$s])) {
			return $this->time_settings[$s];
		} else {
			return $this->time_settings['ALL'];
		}
	}
	private function procesself($t,$key="end") {
		$time = array();
		foreach($t as $x) {
			$end = strtotime($x['end_date']);
			$start = strtotime($x['start_date']);
			$display = date("F Y",$end);
			if($key=="id") { $i = $x['id']; } else { $i = $end; }
			$time[$i] = array(
				'id'				=> $x['id'],
				'start'				=> $start,
				'end'				=> $end,
				'display'			=> $display,
				'active_primary'	=> ( ($x['active_primary'] & self::ACTIVE ) == self::ACTIVE),
				'active_secondary'	=> ( ($x['active_secondary'] & self::ACTIVE ) == self::ACTIVE),
				'active_tertiary'	=> (isset($x['active_tertiary']) ? ( ($x['active_tertiary'] & self::ACTIVE ) == self::ACTIVE) : false),
				'active_finance'	=> ( ($x['active_finance'] & self::ACTIVE ) == self::ACTIVE),
				'close_primary'		=> $x['close_primary'],
				'close_secondary'	=> $x['close_secondary'],
				'close_tertiary'	=> (isset($x['close_tertiary']) ? $x['close_tertiary'] : 0),
				'close_finance'		=> $x['close_finance'],
				'rem_primary'		=> $x['rem_primary'],
				'rem_secondary'		=> $x['rem_secondary'],
				'rem_tertiary'		=> (isset($x['rem_tertiary']) ? $x['rem_tertiary'] : 0),
				'rem_finance'		=> $x['rem_finance'],
			);
		}
		return $time;
	}
	
	
	public function getOpenTime($category="",$options="") {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE active_".$category." & ".self::ACTIVE." = ".self::ACTIVE." ".(strlen($options)>0 ? " AND (".$options.")" : "");
		
		$rows =  $this->mysql_fetch_all_by_id($sql,"id");
		$time = $this->procesself($rows,"id");
		return $time;
	}
	
	public function getTimeByID($t) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id = ".$t;
		$row[$t] = $this->mysql_fetch_one($sql);
		$ret = $this->procesself($row,"id");
		return $ret[$t];
	}
	
	
	
	public function resetTime($flds,$id) {
		$sql = false;
		if(count($flds)>0 && $this->checkIntRef($id)) {
			$sql = "UPDATE ".$this->getDBRef()."_list_time SET ".implode("=0, ",$flds)."=0 WHERE id = ".$id;
			$this->db_update($sql);
		}
		return $sql;
	}
	
	
	
	
}


?>