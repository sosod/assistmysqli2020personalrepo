<?php

class DISPLAY extends SDBP5_HELPER {

	public function __construct() {
		parent::__construct();
	}

	
	
	
	
	
	
	public function drawLiteKPI($kpiObj,$head,$rhead,$time,$kr,$update_form=false,$not_assurance_form=true,$current_time_id=0) {
		echo $this->getLiteKPI($kpiObj,$head,$rhead,$time,$kr,$update_form,$not_assurance_form,$current_time_id);
	}
	
	
	
	public function getLiteKPI($kpiObj,$head,$rhead,$time,$kr,$update_form=false,$not_assurance_form=true,$current_time_id=0) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$i = $kr['display_id'];
		if(!isset($head['dir']) && isset($head['top_dirid'])) {
			$head['dir'] = $head['top_dirid'];
		}
		$echo = "";
		if(!$update_form && $not_assurance_form) {
			$echo .= "<h4>".$kr['value']." (".$i.")</h4>";
		}
		$echo.= "
		<table class=form width=".$width.">
			<tr>
				<th>Ref:</th>
				<td id=ref>".$i."</td>
			</tr>
			<tr>
				<th width=25%>".$head['dir']['h_client'].":</th>
				<td id=dir>".$kr['dir']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getValueField()]['h_client'].":</th>
				<td id=value>".$kr['value']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getUnitField()]['h_client'].":</th>
				<td id=unit>".$kr['unit']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getPOEField()]['h_client'].":</th>
				<td id=poe>".$kr['poe']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getCTField()]['h_client'].":</th>
				<td id=ct>".$kr['ct']."</td>
			</tr>
		</table>";
		if($update_form) {
			$echo.= "<h2>Current Results</h2>";
			$echo.=$this->getKPIResults($kpiObj,$rhead,$time,$kr,false,$kr['obj_id']);
		} elseif(!$not_assurance_form) {
			$echo.="<h2>Complete Results</h2>";
			$echo.=$this->getKPIResults($kpiObj,$rhead,$time,$kr,false,$kr['obj_id']);
			$echo.= "<h2>".$time[$current_time_id]['display']." Update</h2>";
			$echo.=$this->getKPIUpdateForView($kpiObj,$rhead,$time,$kr,$current_time_id);
		}
		/*if($not_assurance_form) {
			$echo.=$this->getKPIResults($kpiObj,$rhead,$time,$kr,false,$kr['obj_id']);
		} else {
			$echo.=$this->getKPIUpdateForView($kpiObj,$rhead,$time,$kr,$current_time_id);
		}*/
		return $echo;
	}
	
	
	
	public function drawKPIResults($kpiObj,$rhead,$time,$kr,$edit=false,$obj_id=0) {
		echo $this->getKPIResults($kpiObj,$rhead,$time,$kr,$edit,$obj_id);
	}
	
	public function getKPIResults($kpiObj,$rhead,$time,$kr,$edit=false,$obj_id=0) {
		//$kpiObj->arrPrint($kr);
		$echo = "";
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$width+=($edit ? 100 : 0);
		if(isset($GLOBALS['userObj'])) { $userObj = $GLOBALS['userObj']; } else { $userObj = new SDBP5_USER(); }
		$active_time_field = $userObj->getActiveTimeField();
		$canEdit = $edit;
		$isManage = $kpiObj->isManage();
		$isAdmin = $kpiObj->isAdmin();
		$target_type = $kr['targettype'];
		$echo.= "
		<table width=".$width." style='margin-top: 10px'>
			<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>	
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>
				<th rowspan=2>".$rhead[$kpiObj->getRPerfField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRCorrectField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRAttachField()]['h_client']."</th>
				".($edit ? "<th rowspan=2 width=100>Update Status</th>" : "")."
				".($edit && $kpiObj->isTop() ? "<th rowspan=2 width=100>Auto Update</th>" : "")."
				".(!$kpiObj->isTop() ? "<th rowspan=2 width=100>Assurance Status</th>" : "")."
			</tr><tr>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
			</tr>";
			foreach($time as $ti => $t) {
				$poe = "";
				$attach = "";
				$i = $t['id']; 
				if(($_SESSION['tid']=="0003" && strtolower($_SESSION['cc'])=="tza0001")) {
					$canEdit = $edit & true;
				}  else {
					$canEdit = ($edit && $t['active_'.$active_time_field]);
				}

				if($t['start']<$this->today) {
					if(strlen($kr['results']['poe'][$i])>0) { $poe = ($kr['results']['poe'][$i]); }
					$a = $kr['results']['attach'][$i];
					if(count($a)>0) {
						$attachments = array();
						foreach($a as $b=>$c) {
							$attachments[$b] = $c;
						}
						$attach = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder());//,$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
					} else {
						$attach = "";
					}
					if($kr['results']['target'][$i]>0 || $kr['calctype']=="ZERO" || $kr['results']['update'][$i]==true) {	
						if($kr['results']['update'][$i]==true) { 
							$us = "Y";
						} else {
							$us = "N";
						}
					} else {
						$us = "info";
					}
					$unlock = false;
					if($edit) {
						$update_status = $this->drawStatus($us);
						if($kr['results']['update'][$i]==true) {
							$update_status.="<p style='margin-top: 1px; margin-bottom: 0px;'>By ".$kr['results']['updateuser'][$i]." on ".date("d-M-Y",strtotime($kr['results']['updatedate'][$i]));
						}
						if($kpiObj->isTop()) {
							$auto_update = $kpiObj->drawStatus($kr['results']['auto'][$i]);
							switch($kr['results']['auto'][$i]) {
								case "Y": 		$auto_update.=""; break;
								case "warn": 	$auto_update.="Conflict with associated Departmental KPIs"; break;
								case "lock": 	$auto_update="<div style='cursor:pointer;' title='Click to Unlock' class=unlock_auto time_id=".$i." time_name='".$t['display']."' obj_id=".$obj_id.">".$auto_update."Locked for Manual Update</div>"; $unlock = true; break;
								case "N": 		$auto_update.="No associated Departmental KPIs"; break;
							}
						}
					}
					if(!$kpiObj->isTop()) {
						$ass_status = $kpiObj->drawStatus($kr['results']['ass_status'][$i]);
						//$ass_status = ":".$kr['results']['ass_status'][$i].":";
					}
					$echo.= "
					<tr id=tr_".$i.">
						<th class=left>".$t['display'].($canEdit ? "<input type=hidden name=ti[] value=$i />" : "")."</th>
						<td class=right>".( ($isAdmin && $canEdit) ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()."[$i]",true) : $kr['results']['target'][$i])."</td>
						<td class=right>".($kr['results']['actual'][$i])."</td>
						<td class='".$kr['results']['r'][$i]['style']."'>".$kr['results']['r'][$i]['text']."</td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right>".$kr['results']['ytd_actual'][$i]."</td>
						<td class='".$kr['results']['ytd_r'][$i]['style']."'>".$kr['results']['ytd_r'][$i]['text']."</td>
						<td>".($kr['results']['perfcomm'][$i])."</td>
						<td>".($kr['results']['correct'][$i])."</td>
						<td>".$poe.$attach."</td>
						".($edit ? "<td class=center>".$update_status."</td>" : "")."
						".($edit && $kpiObj->isTop() ? "<td class=center id=auto_".$i.">".$auto_update."</td>" : "")."
						".(!$kpiObj->isTop() ? "<td class='center assurance-view' obj_id=".$obj_id." time_id=".$i." id=ass_".$i.">".$ass_status."</td>" : "")."
					</tr>";
			} else {
					if($edit) {
						if($kpiObj->isTop()) {
							$auto_update = $kpiObj->drawStatus($kr['results']['auto'][$i]);
							switch($kr['results']['auto'][$i]) {
								case "Y": 		$auto_update.=""; break;
								case "warn": 	$auto_update.="Conflict with associated Departmental KPIs"; break;
								case "lock": 	$auto_update="<div style='cursor:pointer;' title='Click to Unlock' class=unlock_auto time_id=".$i." time_name='".$t['display']."' obj_id=>".$auto_update."Locked for Manual Update</div>"; $unlock = true; break;
								case "N": 		$auto_update.="No assoc. Departmental KPIs"; break;
							}
						}
					}
					$echo.= "
					<tr id=tr_".$i.">
						<th class=left>".$t['display'].($canEdit ? "<input type=hidden name=ti[] value=$i />" : "")."</th>
						<td class=right>".($isAdmin && $canEdit ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()."[$i]",true) : $kr['results']['target'][$i])."</td>
						<td class=right></td>
						<td class=''></td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td></td>
						<td></td>
						<td></td>
						".($edit ? "<td></td>" : "" )."
						".($edit && $kpiObj->isTop() ? "<td class=center id=auto_".$i.">".$auto_update."</td>" : "" )."
						".(!$kpiObj->isTop() ? "<td class='center assurance-view' obj_id=".$obj_id." time_id=".$i." id=ass_".$i."></td>" : "" )."
					</tr>";
			}
		}
/*
 */
		$echo .="
		</table>";
/*
		$echo .="
		<div id=dlg_assurance_log title='Assurance Review History'>
		</div>
		<script type=text/javascript>
			var winSize = getWindowSize();
			var dlgWidth = 400;
			var cenVert = winSize['height']/2-100;
			var cenHor = winSize['width']/2-dlgWidth/2;
			var assDlgWidth = 600;
			var assCenHor = winSize['width']/2-assDlgWidth/2;
			$(function() {
				$('#dlg_assurance_log').hide();
				
		";
		if($edit && $unlock) {
			$echo.="
				$('div.unlock_auto').click(function() {
					if(confirm('Are you sure you wish to unlock this KPI for '+$(this).attr('time_name')+'?')) {
						var ti = $(this).attr('time_id');
						var dta = 'section=TOP&action=UNLOCK_AUTO&id=".$obj_id."&time_id='+ti;
						var r = doAjax('controller/edit.php',dta);
						//change display object in cell to green unlocked icon with no text ->drawStatus('unlock')
						$('td #auto_'+ti).html(r['icon']);
						//Display confirmation result div
						//alert(r[1]);
						var response = r['msg'];
						$('<div />',{id:'response_msg', html:response}).dialog({
							autoOpen	: true,
							modal 		: true,
							title 		: r['title'],
							position	: [cenHor, cenVert],
							width 		: dlgWidth,
							height    	: 'auto',
							buttons		: [{ text: 'Ok', click: function() { 
												$('#response_msg').dialog('destroy'); 
												$('#response_msg').remove(); 
											} 
										}],
							});
					}
				});
			";
		}
		$echo.="
			});
			</script>
		";*/
		return $echo;
	}
	
	
	function KPIresultEdit($val,$tt,$fld,$annual=false,$hidden=false) {
		if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
		switch($tt) {
			case 1:	return "R <input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\"  />"; break;
			case 2:	return "<input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\"  /> %"; break;
			case 3: return "<input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\" />"; break;
			default: return $val; break;
		}
	}
	

	function KPIresultDisplay($val,$tt,$annual=false) {
		$ret = "";
		if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
		switch($tt) {
			case 1:	$ret = "R ".number_format($val,$decimal); break;
			case 2:	$ret = number_format($val,$decimal)."%"; break;
			case 3: $ret = number_format($val,$decimal); break;
			default: $ret = $val; break;
		}
		if($annual) {
			$ret.= "<input type=hidden name=hidden value=\"".$val."\" class=\"".($annual ? "annual_revised" : "")."\" />";
		}
		return $ret;
	}
	
	function KPIcommentEdit($val,$fld,$ti) {
		return "<textarea name=\"".$fld."_".$ti."\" rows=3 cols=40>".$val."</textarea>";
	}
	
	


	
	public function drawKPIUpdateForView($kpiObj,$rhead,$time,$kr,$current_time_id=0) {
		echo $this->getKPIUpdateForView($kpiObj,$rhead,$time,$kr,$current_time_id);
	}
	
	public function getKPIUpdateForView($kpiObj,$rhead,$time,$kr,$current_time_id=0) {
		$echo = false;
		$obj_id = 0;
		$echo = "";
		$i = $current_time_id;
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$target_type = $kr['targettype'];
		$poe = "";
		$attach = "";
			if(strlen($kr['results']['poe'][$i])>0) { $poe = ($kr['results']['poe'][$i]); }
			$a = $kr['results']['attach'][$i];
			if(count($a)>0) {
				$attachments = array();
				foreach($a as $b=>$c) {
					$attachments[$b] = $c;
				}
				$attach = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder());//,$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
				//$attach = implode($attachments);
			}
			if($kr['results']['target'][$i]>0 || $kr['calctype']=="ZERO" || $kr['results']['update'][$i]==true) {	
				if($kr['results']['update'][$i]==true) { 
					$us = "ok";
				} else {
					$us = "error";
				}
			} else {
				$us = "info";
			}
			$unlock = false;
			$update_status = $this->getDisplayStatusAsDiv($us);
			//if($kr['results']['update'][$i]==true) {
			if($us=="ok") {
				$update_status.="By ".$kr['results']['updateuser'][$i]." on ".date("d-M-Y",strtotime($kr['results']['updatedate'][$i]))."";
			} elseif($us=="error") {
			} else {
				
			}
		
		$echo.="
		<table width=".$width." style='margin-top: 10px' class=form>
			<tr>
				<th width=25%>".$rhead[$kpiObj->getRTargetField()]['h_client'].":</th>
				<td>".$kr['results']['target'][$current_time_id]."</td>
			</tr><tr>
				<th>".$rhead[$kpiObj->getRActualField()]['h_client'].":</th>
				<td>".$kr['results']['actual'][$current_time_id]."</td>
			</tr><tr>
				<th>Result:</th>
				<td><span class='".$kr['results']['r'][$current_time_id]['style']."'>&nbsp;&nbsp;</span> ".$kr['results']['r'][$current_time_id]['value']."</td>
			</tr><tr>
				<th>".$rhead[$kpiObj->getRPerfField()]['h_client'].":</th>
				<td>".($kr['results']['perfcomm'][$current_time_id])."</td>
			</tr><tr>
				<th>".$rhead[$kpiObj->getRCorrectField()]['h_client'].":</th>
				<td>".($kr['results']['correct'][$current_time_id])."</td>
			</tr><tr>
				<th>".$rhead[$kpiObj->getRAttachField()]['h_client'].":</th>
				<td>".$poe.$attach."</td>
			</tr><tr>
				<th>Update Status:</th>
				<td>".$update_status."</td>
			</tr>
		</table>
		";
/*		$width+=($edit ? 100 : 0);
		if(isset($GLOBALS['userObj'])) { $userObj = $GLOBALS['userObj']; } else { $userObj = new SDBP5_USER(); }
		$active_time_field = $userObj->getActiveTimeField();
		$canEdit = $edit;
		$isManage = $kpiObj->isManage();
		$isAdmin = $kpiObj->isAdmin();
		$target_type = $kr['targettype'];
		$echo.= "
		<table width=".$width." style='margin-top: 10px'>
			<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>	
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>
				<th rowspan=2>".$rhead[$kpiObj->getRPerfField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRCorrectField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRAttachField()]['h_client']."</th>
				".($edit ? "<th rowspan=2 width=100>Update Status</th>" : "")."
				".($edit && $kpiObj->isTop() ? "<th rowspan=2 width=100>Auto Update</th>" : "")."
			</tr><tr>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
			</tr>";
			foreach($time as $ti => $t) {
				$poe = "";
				$attach = "";
				$i = $t['id']; 
				$canEdit = ($edit && $t['active_'.$active_time_field]);
				if($t['start']<$this->today) {
					if(strlen($kr['results']['poe'][$i])>0) { $poe = ($kr['results']['poe'][$i]); }
					$a = $kr['results']['attach'][$i];
					if(count($a)>0) {
						$attachments = array();
						foreach($a as $b=>$c) {
							$attachments[$b] = $c;
						}
						$attach = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder());//,$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
					}
					if($kr['results']['target'][$i]>0 || $kr['calctype']=="ZERO" || $kr['results']['update'][$i]==true) {	
						if($kr['results']['update'][$i]==true) { 
							$us = "Y";
						} else {
							$us = "N";
						}
					} else {
						$us = "info";
					}
					$unlock = false;
					if($edit) {
						$update_status = $this->drawStatus($us);
						if($kr['results']['update'][$i]==true) {
							$update_status.="<p style='margin-top: 1px; margin-bottom: 0px;'>By ".$kr['results']['updateuser'][$i]." on ".date("d-M-Y",strtotime($kr['results']['updatedate'][$i]));
						}
						if($kpiObj->isTop()) {
							$auto_update = $kpiObj->drawStatus($kr['results']['auto'][$i]);
							switch($kr['results']['auto'][$i]) {
								case "Y": 		$auto_update.=""; break;
								case "warn": 	$auto_update.="Conflict with associated Departmental KPIs"; break;
								case "lock": 	$auto_update="<div style='cursor:pointer;' title='Click to Unlock' class=unlock_auto time_id=".$i." time_name='".$t['display']."' obj_id=".$obj_id.">".$auto_update."Locked for Manual Update</div>"; $unlock = true; break;
								case "N": 		$auto_update.="No associated Departmental KPIs"; break;
							}
						}
					}
					$echo.= "
					<tr id=tr_".$i.">
						<th class=left>".$t['display'].($canEdit ? "<input type=hidden name=ti[] value=$i />" : "")."</th>
						<td class=right>".( ($isAdmin && $canEdit) ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()."[$i]",true) : $kr['results']['target'][$i])."</td>
						<td class=right>".($kr['results']['actual'][$i])."</td>
						<td class='".$kr['results']['r'][$i]['style']."'>".$kr['results']['r'][$i]['text']."</td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right>".$kr['results']['ytd_actual'][$i]."</td>
						<td class='".$kr['results']['ytd_r'][$i]['style']."'>".$kr['results']['ytd_r'][$i]['text']."</td>
						<td>".($kr['results']['perfcomm'][$i])."</td>
						<td>".($kr['results']['correct'][$i])."</td>
						<td>".$poe.$attach."</td>
						".($edit ? "<td class=center>".$update_status."</td>" : "")."
						".($edit && $kpiObj->isTop() ? "<td class=center id=auto_".$i.">".$auto_update."</td>" : "")."
					</tr>";
			} else {
					if($edit) {
						if($kpiObj->isTop()) {
							$auto_update = $kpiObj->drawStatus($kr['results']['auto'][$i]);
							switch($kr['results']['auto'][$i]) {
								case "Y": 		$auto_update.=""; break;
								case "warn": 	$auto_update.="Conflict with associated Departmental KPIs"; break;
								case "lock": 	$auto_update="<div style='cursor:pointer;' title='Click to Unlock' class=unlock_auto time_id=".$i." time_name='".$t['display']."' obj_id=>".$auto_update."Locked for Manual Update</div>"; $unlock = true; break;
								case "N": 		$auto_update.="No assoc. Departmental KPIs"; break;
							}
						}
					}
					$echo.= "
					<tr id=tr_".$i.">
						<th class=left>".$t['display'].($canEdit ? "<input type=hidden name=ti[] value=$i />" : "")."</th>
						<td class=right>".($isAdmin && $canEdit ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()."[$i]",true) : $kr['results']['target'][$i])."</td>
						<td class=right></td>
						<td class=''></td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td></td>
						<td></td>
						<td></td>
						".($edit ? "<td></td>" : "" )."
						".($edit && $kpiObj->isTop() ? "<td class=center id=auto_".$i.">".$auto_update."</td>" : "" )."
					</tr>";
			}
		}
		$echo .="
		</table>";
		if($edit && $unlock) {
			$echo.="
			<script type=text/javascript>
			var winSize = getWindowSize();
			var dlgWidth = 400;
			var cenVert = winSize['height']/2-100;
			var cenHor = winSize['width']/2-dlgWidth/2;
			$(function() {
				$('div.unlock_auto').click(function() {
					if(confirm('Are you sure you wish to unlock this KPI for '+$(this).attr('time_name')+'?')) {
						var ti = $(this).attr('time_id');
						var dta = 'section=TOP&action=UNLOCK_AUTO&id=".$obj_id."&time_id='+ti;
						var r = doAjax('controller/edit.php',dta);
						//change display object in cell to green unlocked icon with no text ->drawStatus('unlock')
						$('td #auto_'+ti).html(r['icon']);
						//Display confirmation result div
						//alert(r[1]);
						var response = r['msg'];
						$('<div />',{id:'response_msg', html:response}).dialog({
							autoOpen	: true,
							modal 		: true,
							title 		: r['title'],
							position	: [cenHor, cenVert],
							width 		: dlgWidth,
							height    	: 'auto',
							buttons		: [{ text: 'Ok', click: function() { 
												$('#response_msg').dialog('destroy'); 
												$('#response_msg').remove(); 
											} 
										}],
							});
					}
				});
			});
			</script>";
		}*/
		return $echo;
	}












//ASSURANCE KPI
	public function drawAssuranceHistory($data) {
		return $this->getAssuranceHistoryHTML($data);
	}

	public function getAssuranceHistoryHTML($data) {
		if(isset($data['final']) && isset($data['final']['id'])) {
			$a = $data['final']['attachment'];
					//arrPrint($a);
					if(count($a)>0) {
						$attachments = array();
						foreach($a as $b=>$c) {
							$attachments[$b] = $c;
						}
						//arrPrint($attachments);
						$kpiObj = new DEPT();
						$kpiObj->setAttachmentDownloadOptions("section=".$kpiObj->getAssuranceSection());
						$attach = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder());//,$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
					}
			
		
			
			
			
			$echo = "
			<h3>Most Recent Review</h3>
			<table class=form>
				<tr>
					<th>Reviewed:</th>
					<td>By ".$data['final']['ass_user']." on ".date("d M Y H:i",strtotime($data['final']['ass_date']))."</td>
				</tr>
				<tr>
					<th>Sign-off:</th>
					<td>".$data['final']['sign_off']."</td>
				</tr>
				<tr>
					<th>Response:</th>
					<td>".str_replace(chr(10),"<br />",$data['final']['response'])."</td>
				</tr>
				<tr>
					<th>Assigned to:</th>
					<td>".$data['final']['kr_user']."</td>
				</tr>
				<tr>
					<th>Update deadline:</th>
					<td>".date("d F Y",strtotime($data['final']['kr_deadline']))."</td>
				</tr>
			<!--	<tr>
					<th>Attachment:</th>
					<td>".$attach."</td>
				</tr> -->
			</table>";
			if(count($data['log'])>0) {
				$echo.="
				<h3>Review Logs</h3>
				<table>
					<tr>
						<th>Date</th>
						<th>User</th>
						<th>Activity Log</th>
					</tr>";
				foreach($data['log'] as $d) {
					$echo.="
					<tr>
						<td class=center>".date("d M Y H:i",$d['log_date'])."</td>
						<td class=center>".$d['log_user']."</td>
						<td>".$d['log_response']."</td>
					</tr>
					";
				}
				$echo.="
				</table>";
			}
		} else {
			$echo = "No Assurance Review found.";
		}
		return $echo;
	}






























	
	//CAPITAL PROJECT
	
	public function drawLiteCapital($capObj,$head,$time,$cap_id) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$h = $head[CAPITAL::HEAD];
		$cap = $capObj->getLiteDetails($cap_id);
		echo "<h4>".$cap['cap_name']." (".CAPITAL::REFTAG.$cap_id.")</h4>
		<table class=form width=$width>
			<tr>
				<th width=25%>".$head['dir']['h_client'].":</th>
				<td>".$cap['dir']."</td>
			</tr>
			<tr>
				<th>".$h['cap_vote']['h_client'].":</th>
				<td>".$cap['cap_vote']."</td>
			</tr>
			<tr>
				<th>".$h['cap_descrip']['h_client'].":</th>
				<td>".$cap['cap_descrip']."</td>
			</tr>
		</table>
		";
		
		
		$this->drawCapitalResults($capObj,$head,$time,$capObj->getResults($cap_id));
	
	}
	
	
	public function drawCapitalResults($capObj,$head,$time,$cr) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$head1 = $head[CAPITAL::HHEAD];
		$head2 = $head[CAPITAL::RHEAD];
		echo "<table width=".$width." style='margin-top: 10px' >
			<tr>
				<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
			foreach($head1 as $fldh => $f) {
				echo "<th colspan=".count($head2).">".$f['h_client']."</th>";
			}
			echo "
			</tr>
			<tr>";
			foreach($head1 as $fldh => $f) {
				foreach($head2 as $fld => $h) {
					echo "<th>".$h['h_client']."</th>";
				}
			}
			echo "</tr>";
		foreach($time as $ti => $t) {
			$i = $t['id'];
			$c = $cr[$i];
			echo "
			<tr>
				<th class=left>".$t['display']."</th>";
				foreach($head1 as $fldh => $f) {
					foreach($head2 as $fldr => $h) {
						$fld = $fldh.$fldr;
						echo "<td class=right>".$capObj->formatResult($c[$fld],$fldr=="perc" ? "perc" : "number")."</td>";
					}
				}
			echo "
			</tr>";
		}

		echo "</table>";	
	
	}
	
	
	
	
}








/*	public function drawKPIResults($kpiObj,$rhead,$time,$kr,$edit=false) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		echo "
		<table width=".$width." style='margin-top: 10px'>
			<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>	
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>
				<th rowspan=2>".$rhead[$kpiObj->getRPerfField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRCorrectField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRAttachField()]['h_client']."</th>
			</tr><tr>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
			</tr>";
			foreach($time as $ti => $t) {
				$i = $t['id']; 
				if($t['start']<$this->today) {
					$z = array();
					if(strlen($kr['results']['poe'][$i])>0) { $z[] = str_replace(chr(10),"<br />",$kr['results']['poe'][$i]); }
					$a = $kr['results']['attach'][$i];
					//arrPrint($a);
					if(count($a)>0) {
						$attachments = array();
						foreach($a as $b=>$c) {
							$attachments[$b] = $c;
						}
						//arrPrint($attachments);
						$z[] = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder(),$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
					}
					$poe = implode("<br />",$z);
					echo "
					<tr id=tr_".$i.">
						<th class=left>".$t['display']."</th>
						<td class=right>".$kr['results']['target'][$i]."</td>
						<td class=right>".$kr['results']['actual'][$i]."</td>
						<td class='".$kr['results']['r'][$i]['style']."'>".$kr['results']['r'][$i]['text']."</td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right>".$kr['results']['ytd_actual'][$i]."</td>
						<td class='".$kr['results']['ytd_r'][$i]['style']."'>".$kr['results']['ytd_r'][$i]['text']."</td>
						<td>".$kr['results']['perfcomm'][$i]."</td>
						<td>".$kr['results']['correct'][$i]."</td>
						<td>".$poe."</td>
					</tr>";
			} else {
					echo "
					<tr id=tr_".$i.">
						<th class=left>".$t['display']."</th>
						<td class=right>".$kr['results']['target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>";
			}
		}
		echo "
		</table>";
	}
*/




?>