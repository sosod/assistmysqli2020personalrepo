<?php
	$scripts = array( 'graph.js','jgcharts.js','menu.js'  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../inc/header.php");

?>
<!-- <table border="1">
        	<tr>
            	<th colspan="2">Select the fielter you wish to apply</th>
            </tr>
            <tr>
            	<td>Risk Item</td>
                <td><input type="text" name="_risk_tem" id="_risk_tem" value=""  /></td>
            </tr>
             <tr>
            	<td>Risk Type</td>
                <td>
                	<select name="_risk_type" id="_risk_type">
                    	
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Risk Category</td>
                <td>
                	<select id="_risk_category" name="_risk_category">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Risk Description</td>
                <td>
                	<input type="text" name="_risk_description" id="_risk_description" value="" />
                </td>
            </tr>
            <tr>
            	<td>Background of the Risk</td>
                <td>
                	<input type="text" name="_risk_background" id="_risk_background" value=""  />
                </td>
            </tr>
            <tr>
            	<td>Impact</td>
                <td>
                	<select name="_impact" id="_impact">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Impact Rating</td>
                <td>
                	<select name="_impact_rating" id="_impact_rating">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Likelihood</td>
                <td>
                	<select name="_likelihood" id="_likelihood">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Likehood Rating</td>
                <td>
                	<select name="_likelihood_rating" id="_likelihood_rating">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Current Controls</td>
                <td>
                <input type="text" name="_current_controls" id="_current_controls"  />
                </td>
            </tr>
            <tr>
            	<td>Percieved Control Effectiveness</td>
                <td>
                	<select name="_percieved_control_effectiveness" id="_percieved_control_effectiveness">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Control Effectiveness Rating</td>
                <td>
                	<select name="_control_effectiveness_rating" id="_control_effectiveness_rating">
                    </select>
	                </td>
            </tr>
            <tr>
            	<td>Risk Owner</td>
                <td>
                	<select name="_risk_owner" id="_risk_owner">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Action Number</td>
                <td>
                	<select name="_action_number" id="_action_number">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Action</td>
                <td><input type="text" name="_action" id="_action" value=""  /></td>
            </tr>
            <tr>
            	<td>Deliverable</td>
                <td><input type="text" name="_deliverable" id="_deliverable"  /></td>
            </tr>
            <tr>
            	<td>Action Owner</td>
                <td>
                <select name="_action_owner" id="_action_owner">
                </select>
                </td>
            </tr>
            <tr>
            	<td>Time Scale</td>
                <td>
                	<input type="text" name="_timescale" id="_timescale" value=""  />
                </td>
            </tr>
            <tr>
            	<td>Deadline</td>
                <td><input type="text" name="_deadline" id="deadline" value=""  /></td>
            </tr>
            <tr>
            	<td>Sign-Off</td>
                <td><input type="text" id="_signoff" name="_signoff"  /></td>
            </tr>
            <tr>
            	<td>Progress</td>
                <td><input type="text" name="_progress" id="_progress" value=""  /></td>
            </tr>
            <tr>
            	<td>Assurance</td>
                <td><input type="text" name="_assurance" id="_assurance"  /></td>
            </tr> 
            <tr>
            	<td>Status</td>
                <td>
                <select name="_status" id="_status">
                </select>
                </td>
            </tr>
            <tr>
            	<td>Date Created</td>
                <td>
				<input type="radio" name="date" id="any_date" value="any_date" /> Any Date <br />
				<input type="radio" name="date" id="any_date" value="any_date" />  From 
                <input type="text" name="from_date" id="from_date" class="datepicker" /> 
                To  <input type="text" name="to_date" id="to_date" class="datepicker" />             
                </td>
            </tr>
            <tr>
            	<td>Date Updated</td>
                <td>
				<input type="radio" name="date" id="any_date" value="any_date" /> Any Date <br />
				<input type="radio" name="date" id="any_date" value="any_date" />  From 
                <input type="text" name="from_date" id="from_date" class="datepicker" /> 
                To  <input type="text" name="to_date" id="to_date" class="datepicker" />              
                </td>
            </tr>                                                                                    
            <tr>
            	<td>Deadline</td>
                <td>
				<input type="radio" name="date" id="any_date" value="any_date" /> Any Date <br />
				<input type="radio" name="date" id="any_date" value="any_date" />  From 
                <input type="text" name="from_date" id="from_date" class="datepicker" /> 
                To  <input type="text" name="to_date" id="to_date" class="datepicker" />              
                </td>
            </tr>   
             <tr>
            	<td>User defined Field ( UDFs )</td>
                <td>
				<input type="radio" name="date" id="any_date" value="any_date" /> Any Date <br />
				<input type="radio" name="date" id="any_date" value="any_date" />  From 
                <input type="text" name="from_date" id="from_date" class="datepicker" /> 
                To  <input type="text" name="to_date" id="to_date" class="datepicker" />              
                </td>
            </tr>                                                                                                                                                                                                                   
        </table>   
    </form>
    <form id="grouping-options-form" name="grouping-options-form">
    	<table id="grouping_table">
        	<tr>
            	<th colspan="2">Choose your group and sort options</th>
            </tr>
            <tr>
            	<td>Group By</td>
                <td>
                	<select name="group_by" id="group_by">
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Sort by</td>
                <td></td>
            </tr>
        </table>
     </form>
         	<table id="document_format_table">
        	<tr>
            	<th colspan="2">Choose the graph format of your report</th>
            </tr>
            <tr>
            	<td><input type="radio" id="column" name="column" value="column" />Column Chart</td>
            <tr>
            <tr>
            	<td><input type="radio" id="linechart" name="linechart" value="linechart" />Line Chart</td>
            <tr>
            <tr>
            	<td><input type="radio" id="barchart" name="barchart" value="barchart" />Bar  Chart</td>
            <tr>
            <tr>
            	<td><input type="radio" id="piechart" name="piechart" value="piechart" />Pie Chart</td>
            <tr>                                    
            	<td>Sort by</td>
                <td></td>
            </tr>
        </table>
            	<table id="generate_report_table">
        	<tr>
            	<th colspan="2">Generate the report</th>
            </tr>
            <tr>
            	<td>Report Title</td>
            	<td><input type="text" id="report_title" name="report_title" value="" /></td>
            <tr>
            <tr>
            	<td><input type="submit" name="generate_report" id="generate_report" value="Generate Report"  /></td>
            	<td><input type="reset" id="reset" name="reset" value="Reset" /></td>
            <tr>
            <tr>
            	<td>Report Name</td>
            	<td><input type="text" id="report_name" name="report_name" value="" /></td>
            <tr>
            <tr>
            	<td><input type="submit" id="save_quick_report" name="save_quick_report" value="Save as Quick Report" /> </td>
            	<td></td>
        </table>-->