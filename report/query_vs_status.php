<?php
$scripts = array(  'menu.js' );
$styles = array();
$page_title = "Report";
require_once("../inc/header.php");
?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<script>
	$(function(){
		$.post("controller.php?action=queryVSstatus", function( response ){
	        var chart;
			var legend;

	        var chartData = [{country:"Czech Republic",litres:156.90},
					{country:"Ireland",litres:131.10},
					{country:"Germany",litres:115.80},
					{country:"Australia",litres:109.90},
					{country:"Austria",litres:108.30},
					{country:"UK",litres:99.00},
					{country:"Belgium",litres:93.00}];

	            chart = new AmCharts.AmPieChart();
	            chart.dataProvider = response;
	            chart.titleField = "status";
				chart.valueField = "queries";
				chart.radius  	 = "40%";
				chart.colors     = ["#cc0001", "#fe9900", "#009900"];
				
				legend = new AmCharts.AmLegend();
				legend.align = "center";
				legend.markerType = "circle";
				chart.addLegend(legend);

				chart.write("chartdiv");			
		},"json");	
	});
</script>
<table width="100%" class="noborder">
  <tr>
   <td align="center" class="noborder">
   		<center>
   		<h1>
		  	<?php 
		  		echo $_SESSION['cn'];
		  	?>
  		</h1>
  		</center>
  	</td>
  </tr>
  <tr>
    <td colspan="2" class="noborder">
    	<center><div id="chartdiv" style="width:70%; height: 400px;"></div></center>
    </td>
  </tr>
  <tr>
  	<td colspan="2" class="noborder">
  		<center>
 			<?php echo "<font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>"; ?>
 		</center>
  	</td>
  </tr>  
  <tr>
    <td class="noborder"><?php displayGoBack("",""); ?></td>
    <td class="noborder"></td>
  </tr>
</table>

