<?php
//	require_once '../../inc_session.php';
include("../../library/class/assist_dbconn.php");
include("../../library/class/assist_db.php");
include("../../library/class/assist_helper.php");
$db = new ASSIST_DB("client");
$dbref = $db->getDBRef(); //"assist_".$_SESSION['cc']."_".strtolower($_SESSION['modref']);
//	error_reporting(-1);

$colours = array('#bb0000','#bb5f00','#00bb00','#0000bb','#dd0000','#dd6e00','#00dd00','#0000dd','#ff1111','#ff7811','#11ff11','#1111ff','#ff3333','#ff8733','#33ff33','#3333ff','#ff5555','#ffaa55','#55ff55','#5555ff','#ff7777','#ffbb77','#77ff77','#7777ff','#ff9999','#ffcc99','#99ff99','#9999ff','#990000','#995000','#009900','#000099');
$query_fields = array("category","type","risk_type","risk_level","financial_exposure","financial_year","subdirid");

	
switch($_REQUEST['report']) {
case "report_dashboard_dir":
	$object = "query";
	$objects = "queries";
	$object2 = "query(ies)";
	$title = "Query Analysis Dashboard Per Department";
	$classification_title = "Queries by Classification";
	$status_title = "Queries by Status";
	if(isset($_REQUEST['filter']) && strlen($_REQUEST['filter'])>0) {
		$filter = ASSIST_HELPER::checkIntRef($_REQUEST['filter']) ? $_REQUEST['filter'] : "NULL";
		$sql_filter = ASSIST_HELPER::checkIntRef($_REQUEST['filter']) ? "d.dirid = ".$_REQUEST['filter'] : "d.dirid IS NULL";
		if(ASSIST_HELPER::checkIntRef($_REQUEST['filter'])) {
			$sql = "SELECT dirtxt as text FROM ".$dbref."_dir WHERE dirid = $filter";
			$row = $db->mysql_fetch_one($sql);
			$blurb = "Report generated for Queries assigned to ".$row['text'].".";
		} else {
			$blurb = "Report generated for Queries with no owner.";
		}
	} else {
		$filter = "ALL";
		$sql_filter = "";
		$blurb = "Report generated for All Departments.";
	}
	$class_values_sql = "SELECT q.id, ".implode(", ",$query_fields).", AVG(a.progress) as act_avg
		FROM ".$dbref."_query_register q
		LEFT OUTER JOIN ".$dbref."_actions a
		ON a.risk_id = q.id AND a.active = 1
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE q.active = 1
		".($sql_filter!="" ? " AND ".$sql_filter : "")."
		GROUP BY q.id";
	$status_addressed_sql = "SELECT count(q.id) as c 
		FROM ".$dbref."_query_register q 
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE q.active = 1 AND q.status = 3
		".($sql_filter!="" ? " AND ".$sql_filter : "")."";
	$status_other_sql = "SELECT q.query_deadline_date as d, count(q.id) as c FROM ".$dbref."_query_register q
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE q.active = 1 AND q.status <> 3 
		".($sql_filter!="" ? " AND ".$sql_filter : "")."
		GROUP BY q.query_deadline_date";
	break;
case "report_dashboard_actions_person":
	$object = "action";
	$objects = "actions";
	$object2 = "action(s)";
	$title = "Action Analysis Dashboard Per Person";
	$classification_title = "Actions by Query Classification";
	$status_title = "Actions by Status";
	if(isset($_REQUEST['filter']) && strlen($_REQUEST['filter'])>0) {
		$filter = ASSIST_HELPER::checkIntRef($_REQUEST['filter']) ? $_REQUEST['filter'] : "NULL";
		$sql_filter = ASSIST_HELPER::checkIntRef($_REQUEST['filter']) ? "a.action_owner = '".$_REQUEST['filter']."'" : "(a.action_owner IS NULL OR a.action_owner = '')";
		if(ASSIST_HELPER::checkIntRef($_REQUEST['filter'])) {
			$sql = "SELECT CONCAT(tk.tkname, ' ', tk.tksurname) as text FROM assist_".$_SESSION['cc']."_timekeep tk WHERE tk.tkid = '".$filter."'";
			$row = $db->mysql_fetch_one($sql);
			$blurb = "Report generated for Actions assigned to ".$row['text'].".";
		} else {
			$blurb = "Report generated for Actions with no owner.";
		}
	} else {
		$filter = "ALL";
		$sql_filter = "";
		$blurb = "Report generated for All Action Owners.";
	}
	$class_values_sql = "SELECT a.id, ".implode(", ",$query_fields).", a.progress as act_avg, a.status
		FROM ".$dbref."_actions a
		LEFT OUTER JOIN ".$dbref."_query_register q
		ON a.risk_id = q.id 
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE q.active = 1 AND a.active = 1
		".($sql_filter!="" ? " AND ".$sql_filter : "")."
		";
	$status_addressed_sql = "SELECT count(a.id) as c FROM ".$dbref."_actions a WHERE a.active = 1 AND a.status = 3
		".($sql_filter!="" ? " AND ".$sql_filter : "")."";
	$status_other_sql = "SELECT a.deadline as d, count(a.id) as c FROM ".$dbref."_actions a WHERE a.active = 1 AND a.status <> 3 
		".($sql_filter!="" ? " AND ".$sql_filter : "")." GROUP BY deadline";
	break;
case "report_dashboard_actions":
	$object = "action";
	$objects = "actions";
	$object2 = "action(s)";
	$title = "Action Analysis Dashboard";
	$blurb = "";
	$classification_title = "Actions by Query Classification";
	$status_title = "Actions by Status";
	$class_values_sql = "SELECT a.id, ".implode(", ",$query_fields).", a.progress as act_avg, a.status
		FROM ".$dbref."_actions a
		LEFT OUTER JOIN ".$dbref."_query_register q
		ON a.risk_id = q.id 
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE q.active = 1 AND a.active = 1";
	$status_addressed_sql = "SELECT count(id) as c FROM ".$dbref."_actions WHERE active = 1 AND status = 3";
	$status_other_sql = "SELECT deadline as d, count(id) as c FROM ".$dbref."_actions WHERE active = 1 AND status <> 3 GROUP BY deadline";
	break;
case "report_dashboard":
default:
	$object = "query";
	$objects = "queries";
	$object2 = "query(ies)";
	$title = "Query Analysis Dashboard";
	$blurb = "";
	$classification_title = "Queries by Classification";
	$status_title = "Queries by Status";
	$class_values_sql = "SELECT q.id, ".implode(", ",$query_fields).", AVG(a.progress) as act_avg
		FROM ".$dbref."_query_register q
		LEFT OUTER JOIN ".$dbref."_actions a
		ON a.risk_id = q.id AND a.active = 1
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE q.active = 1
		GROUP BY q.id";
	$status_addressed_sql = "SELECT count(id) as c FROM ".$dbref."_query_register WHERE active = 1 AND status = 3";
	$status_other_sql = "SELECT query_deadline_date as d, count(id) as c FROM ".$dbref."_query_register WHERE active = 1 AND status <> 3 GROUP BY query_deadline_date";
}	

	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
	<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
	<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
	<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
		<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
	<link rel="stylesheet" href="/assist.css" type="text/css">
	<script type ="text/javascript" src="/assist.js"></script>
<style type=text/css>
#color-check td, #legend td, #legend, #legend2, #legend2 td, #tbl_graphs, #tbl_graphs td { border: 0px solid #ffffff; }
</style>
</head>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php	
	echo "<div class=center id=heading><h1>".$_SESSION['ia_cmp_name']."</h1>
	<h2>".$title."</h2>
	".(strlen($blurb)>0 ? "<p class=b style=\"font-size: bpt;\">".$blurb."</p>" : "")."
	<p class=i style=\"font-size: 7pt;\">Report generated on ".date("d M Y H:i:s")."</p>
	</div>";
?>
<table style="width:100%;" id=tbl_graphs><tr><td>
<div align=center>
<h2><?php echo $classification_title; ?></h2>
	<div id="colChart" style="width:700px; height:400px;"></div>

<?php
//GET HEADINGS
$sql = "SELECT * FROM ".$dbref."_header_names WHERE name IN ('query_category','query_type','risk_level','risk_type','financial_exposure','financial_year','query_owner')";
$headings = $db->mysql_fetch_all_fld($sql,"name");
	
$query = array();
$lists = array();
$lists['categories'] = array(
		'table'		=> "categories",
		'display'	=> (strlen($headings['query_category']['client_terminology'])>0 ? $headings['query_category']['client_terminology'] : $headings['query_category']['ignite_terminology']),
		'query'		=> "category",
		'fields' 	=> array(
			'id'		=> "id",
			'name'		=> "name",
			'status'	=> "active",
			'color'		=> "",
		),
		'data'		=> array()
	);
	$query['category'] = array();
$lists['types'] = array(
		'table'		=> "types",
		'display'	=> (strlen($headings['query_type']['client_terminology'])>0 ? $headings['query_type']['client_terminology'] : $headings['query_type']['ignite_terminology']),
		'query'		=> "type",
		'fields' 	=> array(
			'id'		=> "id",
			'name'		=> "name",
			'status'	=> "active",
			'color'		=> "",
		),
		'data'		=> array()
	);
	$query['type'] = array();
$lists['level'] = array(
		'table'		=> "level",
		'display'	=> (strlen($headings['risk_level']['client_terminology'])>0 ? $headings['risk_level']['client_terminology'] : $headings['risk_level']['ignite_terminology']),
		'query'		=> "risk_level",
		'fields' 	=> array(
			'id'		=> "id",
			'name'		=> "name",
			'status'	=> "status",
			'color'		=> "",
		),
		'data'		=> array()
	);
	$query['risk_level'] = array();
$lists['risk_types'] = array(
		'table'		=> "risk_types",
		'display'	=> (strlen($headings['risk_type']['client_terminology'])>0 ? $headings['risk_type']['client_terminology'] : $headings['risk_type']['ignite_terminology']),
		'query'		=> "risk_type",
		'fields' 	=> array(
			'id'		=> "id",
			'name'		=> "name",
			'status'	=> "active",
			'color'		=> "",
		),
		'data'		=> array()
	);
	$query['risk_type'] = array();
$lists['financial_exposure'] = array(
		'table'		=> "financial_exposure",
		'display'	=> (strlen($headings['financial_exposure']['client_terminology'])>0 ? $headings['financial_exposure']['client_terminology'] : $headings['financial_exposure']['ignite_terminology']),
		'query'		=> "financial_exposure",
		'fields' 	=> array(
			'id'		=> "id",
			'name'		=> "name",
			'status'	=> "status",
			'color'		=> "color",
		),
		'data'		=> array()
	);
	$query['financial_exposure'] = array();
$lists['financial_year'] = array(
		'table'		=> "financial_years",
		'display'	=> "Financial Year",
		'query'		=> "financial_year",
		'fields' 	=> array(
			'id'		=> "id",
			'name'		=> "last_day",
			'status'	=> "active",
			'color'	=>	"",
		),
		'data'		=> array()
	);
	$query['financial_year'] = array();
$lists['query_owner'] = array(
		'table'		=> "dir",
		'display'	=> (strlen($headings['query_owner']['client_terminology'])>0 ? $headings['query_owner']['client_terminology'] : $headings['query_owner']['ignite_terminology']),
		'query'		=> "subdirid",
		'fields' 	=> array(
			'id'		=> "dirid",
			'name'		=> "dirtxt",
			'status'	=> "active",
			'color'	=>	"",
		),
		'data'		=> array()
	);
	$query['query_owner'] = array();



$row_count = 0;

foreach($lists as $key=>$l) {
		$sql = "SELECT ".$l['fields']['id']." as id, ".$l['fields']['name']." as name ".(strlen($l['fields']['color'])>0 ? ", ".$l['fields']['color']." as color" : "")."
			FROM assist_".$_SESSION['cc']."_".strtolower($_SESSION['modref'])."_".$l['table']." 
			WHERE ".$l['fields']['status']." = 1
			ORDER BY ".$l['fields']['name'];
		$lists[$key]['data'] = $db->mysql_fetch_all_fld($sql,"id");
		$lists[$key]['data2'] = $db->mysql_fetch_all($sql);
		if(count($lists[$key]['data'])>$row_count) { $row_count = count($lists[$key]['data']); }
}
//print_r($lists);

//GET QUERIES
//$sql = "SELECT ".implode(", ",$query_fields)." FROM ".$dbref."_query_register WHERE active = 1";	
$sql = $class_values_sql;
$rs = $db->db_query($sql);
while($row = mysql_fetch_assoc($rs)) {
	foreach($query_fields as $qf) {
		if(is_null($row[$qf])) { $row[$qf]=0; }
		if(!isset($query[$qf][$row[$qf]])) { 
			$query[$qf][$row[$qf]] = array(
				'count' => 1,
				'avg' => (!is_null($row['act_avg']) ? $row['act_avg'] : 0),
			);
		} else { 
			$query[$qf][$row[$qf]]['count']++; 
			$query[$qf][$row[$qf]]['avg']+= (!is_null($row['act_avg']) ? $row['act_avg'] : 0);
		}
	}
}
unset($rs);
$chartData = array();
$graph_count = 0;
//arrPrint($query);

//Horizontal legend
echo "<table style=\"margin-top: 20px;\" id=legend>";
foreach($lists as $l) {
$color = 0;
$gc = 0;
$col_count = count($l['data']);
if($col_count > 8) {
	$col_count = ceil($col_count/2);
}
$cc = 0;
	echo "<tr><td></td>";
	echo "<td class=\"center b bottom\">Unspecified</td>";
	foreach($l['data'] as $d) {
		echo "<td class=\"center b bottom\">".stripslashes($d['name'])."</td>";
$cc++;
if($cc>=$col_count) { break; }
	}
$cc=0;
	echo "<tr style=\"background-color: #efefef; height: 40px;\">
			<td class=\"right b middle\">".$l['display'].":</td>";
			$cd = array();
$d['id'] = 0;
				if(isset($query[$l['query']][$d['id']])) { 
					$cd[] = "\"F".$gc."\":".$query[$l['query']][$d['id']]['count'];
					$qc = $query[$l['query']][$d['id']]['count'];
					$qa = $query[$l['query']][$d['id']]['avg'] / $qc;
					$qt = $qc==1 ? $object : $objects;
				} else {
					$qc = 0;
					$qa = 0;
					$qt = $objects;
				} 
$gc++;
				echo "<td class=\"center middle\">
					<div style=\"margin-bottom: -15px; padding: 2px 2px 2px 2px; background-color: #ababab; color: #000000;\">
						<span >".$qc." ".$qt."</span>
					</div>
					<br />
						<span class=i style=\"font-size: 6.5pt;\">".number_format($qa,2)."%&nbsp;completed</span>
				</td>";
			foreach($l['data'] as $d) {
				if(isset($query[$l['query']][$d['id']])) { 
					$cd[] = "\"F".$gc."\":".$query[$l['query']][$d['id']]['count'];
					$qc = $query[$l['query']][$d['id']]['count'];
					$qa = $query[$l['query']][$d['id']]['avg'] / $qc;
					$qt = $qc==1 ? $object : $objects;
				} else {
					$qc = 0;
					$qa = 0;
					$qt = $objects;
				} 
				$gc++;
				echo "<td class=\"center middle\">
					<div style=\"margin-bottom: -15px;padding: 2px 2px 2px 2px; background-color: ".$colours[$color]."; color: ".($color>=count($colours)-20 ? "#000000" : "#FFFFFF").";\">
						<span >".$qc." ".$qt."</span>
					</div>
					<br />
						<span class=i style=\"font-size: 6.5pt;\">".number_format($qa,2)."%&nbsp;completed</span>
				</td>";
				$color = ($color+1>=count($colours) ? 0 : $color+1);
				$cc++;
				if($cc>=$col_count && $cc != count($l['data'])) {
					echo "</tr><tr><td></td>";
					$cc2 = 0;
					foreach($l['data'] as $d2) {
						$cc2++;
						if($cc2>$col_count) { echo "<td class=\"center b bottom\">".stripslashes($d2['name'])."</td>"; }
					}
					echo "</tr><tr style=\"background-color: #efefef; height: 40px;\"><td style=\"background-color: #ffffff;\"></td>";
					$cc=-1;
				}
			}
	echo "</tr>
	<tr><td>&nbsp;</td></tr>";
	$chartData[$l['table']] = "{field:\"".str_replace(" ","\\n",$l['display'])."\",".implode(",",$cd)."}";
	if($gc>$graph_count) { $graph_count = $gc; }
}
echo "</table>";

//print "<pre>";
//     print_r($chartData);
//print "</pre>";
//exit();
$color = 0;
?>
        <script type="text/javascript">
$(document).ready(function() {
	$("#legend td").css("width","100px");
});
        var chart;
 
        var chartData = [<?php echo implode(",",$chartData); ?>];
 
         window.onload = function() {
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "field";
            chart.marginLeft = 47;
            chart.marginTop = 30;
//		chart.marginBottom = 100;
			chart.plotAreaBorderAlpha = 0.2;
			chart.angle = 30;
			chart.depth3D = 10;

			var graph = new AmCharts.AmGraph();
			graph.title = "F-0";
		//	graph.labelText="[[value]]";
			graph.valueField = "F0";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "#ababab";
			graph.balloonText = "[[value]] <?php echo $object2; ?>";
			chart.addGraph(graph);
<?php for($i=1;$i<=$graph_count;$i++) { ?>
			var graph = new AmCharts.AmGraph();
			graph.title = "F-<?php echo $i; ?>";
		//	graph.labelText="[[value]]";
			graph.valueField = "F<?php echo $i; ?>";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "<?php echo $colours[$color]; ?>";
			graph.balloonText = "[[value]] <?php echo $object2; ?>";
			chart.addGraph(graph);
<?php 
//				$color=($color+(count($colours)/4)>=count($colours) ? $color+(count($colours)/4)-count($colours)+1 : $color+(count($colours)/4));
$color = ($color+1>=count($colours) ? 0 : $color+1);

} 
?>

			var valAxis = new AmCharts.ValueAxis();
			valAxis.stackType = "100%";
			valAxis.gridAlpha = 0.1;
			valAxis.axisAlpha = 0;
			chart.addValueAxis(valAxis);
 
			var catAxis = chart.categoryAxis;
			catAxis.gridAlpha = 0.1;
			catAxis.axisAlpha = 0;
			catAxis.gridPosition = "start";
 
			/*var legend = new AmCharts.AmLegend();
			legend.position = "right";
			legend.borderAlpha = 0.2;
			legend.horizontalGap = 10;
			legend.switchType = "v";
			chart.addLegend(legend);*/


			chart.write("colChart");
		}
 
</script>


</td><td><div align=center>
<h2><?php echo $status_title; ?></h2>
<div id="pieChart" style="width:400px; height:350px;"></div>
<?php
// GET DATA
$query = array('done'=>0,'ip'=>0,'over'=>0);
//ADDRESSED :: status = 3
$sql = $status_addressed_sql;
$rs = $db->db_query($sql);
$row = mysql_fetch_assoc($rs);
unset($rs);
$query['done'] = $row['c'];
unset($row);
//IP OR OVERDUE
$sql = $status_other_sql;
$rs = $db->db_query($sql);
while($row = mysql_fetch_assoc($rs)) {
	if(strtotime($row['d'])<$today) {
		$query['over']+=$row['c'];
	} else {
		$query['ip']+=$row['c'];
	}
}
unset($rs);


?>
<script type=text/javascript>
        var dir_chart;
        var chartData2 = [
		{field:"Addressed",count:<?php echo $query['done']; ?>,color:"#009900"},
		{field:"In Progress",count:<?php echo $query['ip']; ?>,color:"#FE9900"},
		{field:"Overdue",count:<?php echo $query['over']; ?>,color:"#CC0001"}
	];

         $(document).ready(function() {
            dir_chart = new AmCharts.AmPieChart();
			dir_chart.color = "#ffffff";
			dir_chart.dataProvider = chartData2;
			dir_chart.titleField = "field";
			dir_chart.valueField = "count";
			dir_chart.colorField = "color";
			dir_chart.labelText = "[[percents]]%";
			dir_chart.labelRadius = -35;
			dir_chart.angle = 10;
			dir_chart.depth3D = 15;
			dir_chart.hideLabelsPercent = 5;
			dir_chart.hoverAlpha = 0.5;
			dir_chart.startDuration = 0;
			dir_chart.radius = 150;
			dir_chart.marginBottom = 30;
			dir_chart.write("pieChart");
	});

</script>
<table id=legend2>
	<tr>
		<td class="right b">Overdue:</td>
		<td width=90 class=center><div style="background-color: #CC0001; color: #ffffff;"><span ><?php echo $query['over']." ".($query['over']==1 ? $object : $objects); ?></span></div></td>
	</tr>
	<tr>
		<td class="right b">In Progress:</td>
		<td width=90 class=center><div style="background-color: #FE9900; color: #ffffff;"><span ><?php echo $query['ip']." ".($query['ip']==1 ? $object : $objects); ?></span></div></td>
	</tr>
	<tr>
		<td class="right b">Addressed:</td>
		<td width=90 class=center><div style="background-color: #009900; color: #ffffff;"><span ><?php echo $query['done']." ".($query['done']==1 ? $object : $objects); ?></span></div></td>
	</tr>
	<tr>
		<td class="b right">Total:</td>
		<td class="b center"><div style="background-color: #dedede;"><?php echo array_sum($query)." ".(array_sum($query)==1 ? $object : $objects); ?></div></td>
	</tr>
</table>

</div>
</td></tr>
</table>

</body></html>
