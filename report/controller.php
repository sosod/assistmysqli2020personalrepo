<?php
@session_start();
include_once("../inc/init.php");


switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {
	case "getRisk":
		$type = new Risk( "", "", "", "" ,"", "", "", "", "", "", "", "", "", "");
		echo json_encode( $type-> getAllRisk( (isset($_REQUEST['start']) ?  $_REQUEST['start'] : ""), (isset($_REQUEST['limit']) ? $_REQUEST['limit'] : "" ) ) );	
		break;							
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		echo json_encode( $finExp -> getFinancialExposure() );
		break;	
	case "getRiskLevel":
		$level = new Risklevel( "", "", "", "" );
		echo json_encode( $level-> getLevel() );	
		break;		
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;	
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;
	case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );	
		break;
    case "getQueryType":
		$type = new QueryType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );
		break;
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id'][0]) );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;		
	case "getCategories":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "getQueryStatuses":
		$riskstat = new RiskStatus( "", "", "", "" );
		echo json_encode( $riskstat->getStatus() );
		break;
 	case "getActionStatuses":
		$actstat = new ActionStatus( "", "", "", "" );
		echo $actstat ->getStatus() ;
		break;
	case "generateReport":
		$rep 		= new Report();
		$rep  -> generateReport();
	/*	$rep 		= new Report();
		$dbNames 	= $rep  -> getHeaders();
		$headers 	= array(); 
		$where 		= "";
		$fields		= "";
		$from 		= "";
		$groupby 	= "";
		$riskAction = "";
		if( is_array( $_REQUEST['headers'] ) ){
			foreach( $_REQUEST['headers'] as $key => $headerValue ) {
				//get the heading names to display in the report	
			echo "How about this ".$dbNames[$headerValue['name']]."\r\n\n";
	
				foreach( $dbNames as $ref => $value ) {
					//echo $headerValue['name']."\r\n\n";
					if( $headerValue['name'] == $ref ) {
						$headers[] = $value;
					}		
				} 
				//echo $headerValue['name']."\r\n";
				//$headers[] = $rep -> getHeaders( $headerValue['name'] );
				
				$fields .= $rep -> getField( $headerValue['name'] );
				$from 	.= $rep -> getTableFrom( $headerValue['name'] );
			}
		} 
		
		
		$where  = $rep -> getWhere( $_REQUEST['values'] );
				
		//create the group by if it has been chosen
		if( isset( $_REQUEST['grouping']) ) {
			$groupby = $rep -> getGrouping( rtrim( $_REQUEST['grouping'][0]['value'],"_") );
		}
		if($where !== 1 && !empty($_REQUEST['includeaction']) && empty($_REQUEST['noAction']) ) {
			$riskAction = "LEFT JOIN ".$_SESSION['dbref']."_actions RA ON RA.risk_id = RR.id";
		}
		$from 		  = $_SESSION['dbref']."_risk_register RR \r\n".$riskAction." ".$from;

		$response = $rep -> generateReport( rtrim(trim($fields),",") ,$where , $from ,$groupby );
		echo json_encode( array("headers" => $headers, "data" => $response) );
		*/
		break;
	case "_generateReport":
		$mainArray = array();
		$headers   = array();
		foreach( $_REQUEST['headers'] as $key => $nameValue ) {
		 foreach($_REQUEST['values'] as $val => $valuesArray ) {	 
		 	// if its a clicked check box ,, the get its value entered	
			if( ltrim($valuesArray['name'],"_") ==  $nameValue['name'] ) {
				
				$mainArray[$nameValue['name']] = array(	
															"value" 		=> $valuesArray['value'],
															"grouping"		=> "",
															"sortable"		=> "",
															"tablePrefix"	=> ""
													  );
				// if there is grouping , add the grouping key to the main array
				foreach( $_REQUEST['grouping'] as $key => $groupValue ) {	
					if( rtrim($groupValue['value'],"_") == $nameValue['name'] )	{
						$mainArray[$nameValue['name']]["grouping"] =  "Yes";
					}
					if( ltrim( $groupValue['value'],"__") == $nameValue['name'] ) {
						$mainArray[$nameValue['name']]["sortable"] =  "Yes";
					}
			   }
				// add table and the table prefix	
				if(array_key_exists( $nameValue['name'], $fieldTable ) && isset( $fieldTable[$nameValue['name']]['table'] ) ) {
					$headers[] = $nameValue['name'] ;
					$mainArray[$nameValue['name']]["searchArray"] =  (isset($fieldTable[$nameValue['name']]['searchField']) ? $fieldTable[$nameValue['name']]['searchField']  : "");
					
					if( isset($fieldTable[$nameValue['name']]['as']) && !empty($fieldTable[$nameValue['name']]['as']) ) {
					 $mainArray[$nameValue['name']]["AS"] =   $fieldTable[$nameValue['name']]['as'];	
					}
					$mainArray[$nameValue['name']]["returnField"] = $fieldTable[$nameValue['name']]['name'];
					$mainArray[$nameValue['name']]["table"]		  =  $fieldTable[$nameValue['name']]['table'];
					$mainArray[$nameValue['name']]["tablePrefix"] =  $fieldTable[$nameValue['name']]['prefix'];	
					$mainArray[$nameValue['name']]["join"] 		  =  $fieldTable[$nameValue['name']]['joinField'];								
				}						
				}

		  }
	    }
		$rep 		= new Report();
		$response 	= $rep -> generateReport( $mainArray );
		echo json_encode( $response );
		break;
		case "residualVsInherent":
		//$rep 		= new Report();
		//echo json_encode( $rep -> residualVsInherent() );
		break;	
		case "queryVSstatus":
				$rep 		= new Report();
				$result  	= $rep -> queryVsStatus();
				echo json_encode( $result ); 
		break;
		case "queryVSriskType":
				$rep 		= new Report();
				$result  	= $rep -> queryVsRiskType();
				echo json_encode( $result ); 
		break;	
		case "queryVSdepartment":
				$rep 		= new Report();
				$result  	= $rep -> queryVsDepartment();
				echo json_encode( $result ); 
		break;	
		case "getQuick":
			$qrep     = new Report();
			$qreports = $qrep -> getQuickReports( $_GET['start'], $_GET['limit'] );
			echo json_encode( array("quick"  => $qreports, "total" => $qrep -> totalQuickReports() ) );
		break;	
		case "deleteQuickReport":
			$rep     = new Report();
			$res = $rep -> deleteQuick( $_POST['id']);
			$response = array();
			if( $res == 1){
				$response = array("text" => "Quick report successfully deleted . . ", "error" => false);
			} else {
				$response = array("text" => "Error deleting the query . . ", "error" => true);				
			}
			echo json_encode($response);
		break;
		case "getSavedReport":
			$qrep     = new Report();
			$responseR = $qrep->getQuickData( $_GET['id'] );
			echo json_encode( unserialize( $responseR ) );
		break;
		
		}
?>
