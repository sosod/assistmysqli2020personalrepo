<?php
require_once( "../../library/dbconnect/dbconnect.php" );
include_once("../class/report.php");
include_once("../class/naming.php");
if( isset($_POST['generate_report']))
{	
	$repObj = new Report();
 	$report = $repObj -> generateReport();
 	switch( $_REQUEST['display'])
 	{
 		case "screen":
 			$repObj -> displayHtmlReport($report);
 			break;
 		case "excell":
 			$repObj -> displayCsvReport($report);
 			break;
 		case "pdf":
 			$repObj -> displayHtmlReport($report);
 			break;
 		default:
 			$repObj -> displayHtmlReport($report);
 	}
	exit(0);
} else {
	$scripts = array( 'generate.js','menu.js');
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../inc/header.php");
	$rep = new Report();
	if(isset($_POST['save_quick_report']))
	{
	    if($_POST['report_name'] == "") {
	        echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Please enter the report name</div>";
	    } else {
	        $rep -> saveQuickReport();
	    }
	}
	$naming 	= new Naming();
	$rowNames   = $naming -> rowLabel();
	function nameFields( $fieldId , $defaultName ){
		global $naming, $rowNames;
		if( isset( $rowNames[$fieldId] )) {
			echo $rowNames[$fieldId];
		}	else {
			echo $defaultName;
		}
	}
}
?>
<script>
$(function(){
	$("table#query_field_tobeincluded").find("th").css({"text-align":"left", "vertical-align":"top"});
	$("table#query_field_tobeincluded td").css({"border-color":"#FFFFFF"});
	$("table#query_field_tobeincluded td").css({"border-width":"0px"});	
	$("table#query_field_tobeincluded td").css({"border-style":"none"});
	$("table#query_field_tobeincluded td").css({"vertical-align":"top"});
	$("table#query_field_tobeincluded th").css({"margin-bottom":"10px"})	
});
</script>
<?php JSdisplayResultObj(""); ?>
<form action=""name="report-header-form" id="report-header-form" method="post">
<table id="query_field_tobeincluded" width="70%">
	<tr>
		<th colspan="3">1. Select the Query information to be displayed on the report</th>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[category]" value="1" id="query_category" class="queryf" /><?php nameFields('query_category','Query Category'); ?></td>
		<td><input type="checkbox" name="header[query_reference]" value="1" id="query_reference" class="queryf" /><?php  nameFields('query_reference','Query Reference'); ?></td>
		<td><input type="checkbox" name="header[type]" value="1" id="query_type" class="queryf" /><?php nameFields('query_type','Query Type'); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[background]" value="1" id="query_background" class="queryf" /><?php nameFields('query_background','Background Of Query'); ?></td>
		<td><input type="checkbox" name="header[query_deadline_date]" value="1" id="query_date" class="queryf" /><?php nameFields('query_date','Query Raised Date'); ?></td>
		<td><input type="checkbox" name="header[description]" value="1" id="query_description" class="queryf" ><?php nameFields('query_description','Query Description'); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[monetary_implication]" value="1" id="monetary_implication" class="queryf" /><?php nameFields('monetary_implication','Monetary Implication'); ?></td>
		<td><input type="checkbox" name="header[query_owner]" id="query_owner" value="1" class="queryf" /><?php nameFields('query_owner','Directorate responsible'); ?></td>
		<td><input type="checkbox" name="header[financial_exposure]" id="financial_exposure" value="1" class="queryf" /><?php nameFields('financial_exposure','Financial Exposure'); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[risk_detail]" value="1" id="risk_detail" class="queryf" /><?php nameFields('risk_detail','Risk Detail'); ?></td>
		<td><input type="checkbox" name="header[risk_level]" value="1" id="risk_level" class="queryf" /><?php nameFields('risk_level','Risk Level'); ?></td>
		<td><input type="checkbox" name="header[risk_type]" value="1" id="risk_type" class="queryf" /><?php nameFields('risk_type','Risk Type'); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[client_response]" value="1" id="client_response" class="queryf" /><?php nameFields('client_response','Add Client Response'); ?></td>
		<td><input type="checkbox" name="header[finding]" value="1" id="finding" class="queryf" /><?php nameFields('finding','Finding'); ?></td>
		<td><input type="checkbox" name="header[recommendation]" value="1" id="recommendation" class="queryf" /><?php nameFields('recommendation','Add Recommendation'); ?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="header[status]" value="1" id="query_status" class="queryf" />Query Status</td>
		<td><input type="checkbox" name="header[internal_control_deficiency]" id="internal_control_deficiency" value="1" id="finding" class="queryf" /><?php nameFields('internal_control_deficiency','Internal Control Deficiency'); ?></td>
		<td><input type="checkbox" name="header[auditor_conclusion]" value="1" id="auditor_conclusion" class="queryf" /><?php nameFields('auditor_conclusion','Auditor\'s Conclusion'); ?></td>
	</tr>
	<tr>
	<td colspan="3">
		<input type="button" value="Check All" id="r_checkAll" name="r_checkAll" />
		<input type="button" value="UnCheck All" id="r_uncheckAll" name="r_uncheckAll" />
		<input type="button" value="Invert" id="r_invert" name="r_invert" />
	</td>
	</tr>	
	<!--
	<tr>
		<th colspan="3">2. Select the Action Information to be displayed on the report</th>
	</tr>
	<tr>
		<td><input type="checkbox" name="aheader[action_item]" value="1" id="action_item" class="actionf">Action Item</td>
		<td><input type="checkbox" name="aheader[action_description]" value="1" id="action_description" class="actionf">Action Description</td>
		<td><input type="checkbox" name="aheader[deliverable]" value="1" id="deliverable" class="actionf" >Action Deliverable</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="aheader[action_owner]" value="1" id="action_owner" class="actionf">Action Owner</td>
		<td><input type="checkbox" name="aheader[timescale]" value="1" id="timescale" class="actionf"/>Action Time Scale</td>
		<td><input type="checkbox" name="aheader[deadline]" value="1" id="deadline" class="actionf" />Action Deadline</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="aheader[action_reminder]" value="1" id="action_reminder" class="actionf" />Action Reminder Date</td>
		<td><input type="checkbox" name="aheader[action_status]" id="action_status" value="1" class="actionf" />Action Status</td>
		<td><input type="checkbox" name="aheader[progress]" value="1" id="progress" class="actionf" />Action Progress</td>
	</tr>
	<tr>
	<td colspan="3">
		<input type="button" value="Check All" id="a_checkAll" name="a_checkAll" />
		<input type="button" value="UnCheck All" id="a_uncheckAll" name="a_uncheckAll" />
		<input type="button" value="Invert" id="a_invert" name="a_invert" />
	</td>
	</tr>    -->	
	<tr>
		<th colspan="3">2. Select the filters you wish to apply</th>
	</tr>
	<tr>
		<th><?php nameFields('query_reference','Query Reference'); ?>:</th>
		<td>
			<input type="text" name="values[query_reference]" id="_query_reference" value="" />
		</td>
		<td>
			<select name="match[query_reference]" id="match_query_reference" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th><?php nameFields('query_type','Query Type'); ?>:</th>
		<td>
			<select name="values[type][]" id="_type"  multiple="multiple">
			   <option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th><?php nameFields('query_category','Query Category'); ?>:</th>
		<td>
			<select id="_category" name="values[category][]" multiple="multiple">         
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th><?php nameFields('query_description','Query Description'); ?>:</th>
		<td>
			<textarea name="values[description]" id="_description"></textarea>
		</td>
		<td>
			<select name="match[description]" id="match_description" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>		
	</tr>
	<tr>
		<th><?php nameFields('query_background','Background Of Query'); ?>:</th>
		<td>
			<textarea name="values[background]" id="_background"></textarea>
		</td>
		<td>
			<select name="match[background]" id="match_background" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>
	</tr>
   <tr>
		<th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
		<td>
			<select id="_financial_exposure" name="values[financial_exposure][]" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th><?php nameFields('monetary_implication','Monetary Implication'); ?>:</th>
		<td>
			<textarea name="values[monetary_implication]" id="_monetary_implication"></textarea>
		</td>
		<td>
			<select id="match_monetary_implication" name="match[monetary_implication]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th><?php nameFields('risk_level','Risk Level'); ?>:</th>
		<td>
			<select name="values[risk_level][]" id="_risk_level" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th><?php nameFields('risk_type','Risk Type'); ?>:</th>
		<td>
			<select name="values[risk_type][]" id="_risk_type" multiple="multiple">                    
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th><?php nameFields('risk_detail','Risk Detail'); ?>:</th>
		<td>
			<textarea name="values[risk_detail]" id="_risk_detail"></textarea>
		</td>
		<td>
			<select id="match_risk_detail" name="match[risk_detail]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th><?php nameFields('finding','Add Finding'); ?>:</th>
		<td>
			<textarea name="values[finding]" id="_finding"></textarea>
		</td>
		<td>
			<select id="match_finding" name="match[finding]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th><?php nameFields('internal_control_deficiency','Internal Control Deficiency'); ?>:</th>
		<td>
			<textarea name="values[internal_control_deficiency]" id="_internal_control_deficiency"></textarea>
		</td>
		<td>
			<select id="match_internal_control_deficiency" name="match[internal_control_deficiency]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>	
	</tr>		
	<tr>
		<th><?php nameFields('recommendation','Add Recommendation'); ?>:</th>
		<td>
			<textarea name="values[recommendation]" id="_recommendation"></textarea>
		</td>
		<td>
			<select id="match_recommendation" name="match[recommendation]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th><?php nameFields('client_response','Add Client Response'); ?>:</th>
		<td>
			<textarea name="values[client_response]" id="_client_response"></textarea>
		</td>
		<td>
			<select id="match_client_response" name="match[client_response]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>	
	</tr>
	<tr>
		<th><?php nameFields('auditor_conclusion','Auditor\'s Conclusion'); ?>:</th>
		<td>
			<textarea name="values[auditor_conclusion]" id="_auditor_conclusion"></textarea>
		</td>
		<td>
			<select id="match_auditor_conclusion" name="match[auditor_conclusion]" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="exact">Match exact phrase</option>
			</select>
		</td>	
	</tr>		
	<tr>
		<th>Query Status:</th>
		<td>
			<select name="values[status][]" id="_status" multiple="multiple">                                       
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
		</tr>
	<tr>
		<th><?php nameFields('query_date','Query Raised Date'); ?>:</th>
		<td>
			From: <input type="text" name="values[from_query_date]" id="_from_query_date" value="" class="datepicker" readonly="readonly" />
		</td>
		<td>
         to : <input type="text" name="values[to_query_date]" id="_to_query_date" value="" class="datepicker" readonly="readonly"/>
		</td>
	</tr>		
	<tr>
		<th colspan="3">3. Choose your group and sort options</th>
	</tr>	
	<tr>
		<th>Group By:</th>
		<td colspan="3">
			<select name="group_by" id="group_by">
				<option value="">No grouping</option>
				<option value="type_">Query Type</option>
				<option value="category_">Query Category</option>	
				<option value="financial_exposure_">Financial Exposure</option>
				<option value="risk_level_">Risk Level</option>
				<option value="risk_type_">Risk Type</option>
			</select>
	  </td>
  </tr>
	<tr>
		<th>Sort By:</th>
		<td colspan="3">
			<ul id="sortable" style="list-style:none;">
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort"  id="__id"></span><input type=hidden name=sort[] value="__id">Query Item</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__type"></span><input type=hidden name=sort[] value="__type">Query Type</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__category"></span><input type=hidden name=sort[] value="__category">Query Category</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__description"></span><input type=hidden name=sort[] value="__description">Query Description</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__background"></span><input type=hidden name=sort[] value="__background">Query Background</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__financial_exposure"></span><input type=hidden name=sort[] value="__financial_exposure">Financial Exposure</li>
                <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__monetary_implication"></span><input type=hidden name=sort[] value="__monetary_implication">Monetary Implication</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__risk_level"></span><input type=hidden name=sort[] value="__risk_level">Risk Level</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__risk_type"></span><input type=hidden name=sort[] value="__risk_type">Risk Type</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__risk_detail"></span><input type=hidden name=sort[] value="__risk_detail">Risk Detail</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__finding"></span><input type=hidden name=sort[] value="__finding">Finding</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__recommendation"></span><input type=hidden name=sort[] value="__recommendation">Recommendation</li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__client_response"></span><input type=hidden name=sort[] value="__client_response">Client Response</li>
			</ul>
		</td>
	</tr>		
	<tr>
		<th colspan="3">4. Choose the document format of your report</th>
	</tr>	
	<tr>
		<td colspan="3"><input type="radio" id="onscreen" name="display" value="screen" checked="checked" />OnScreen Display</td>
	</tr>
	<!--
	<tr>
		<td colspan="3"><input type="radio" id="excell" name="display" value="excell" />Microsoft Excel( Plain Text)</td>
	</tr>
	<tr>
		<td colspan="3"><input type="radio" id="excell_formated" name="display" value="excell_formated" />Microsft Excel(Formatted)</td>
	</tr>
	<tr>
		<td colspan="3"><input type="radio" id="pdf" name="display" value="pdf" />Save to Pdf</td>
	</tr>
	-->	
	<tr>
		<th colspan="3">5. Generate the report</th>
	</tr>
	<tr>
		<th>Report Title:</th>
		<td><input type="text" id="report_title" name="report_title" value="" /></td>
        <td></td>
	</tr>
	<tr>
		<th></th>
		<td>
		<input type="submit" name="generate_report" id="generate_report" value="Generate Report"  />
		<input type="reset" id="reset" name="reset" value="Reset" />
		</td>
        <td></td>
	</tr>
	<tr>
		<th>Report Name:</th>
		<td><input type="text" id="report_name" name="report_name" value="" /></td>
        <td></td>
	</tr>
	<tr>
		<th>Description:</th>
		<td><textarea id="report_description" name="report_description" cols="30" rows="7"></textarea></td>
        <td></td>
	</tr>		
	<tr>
		<th></th>
		<td><input type="submit" id="save_quick_report" name="save_quick_report" value="Save as Quick Report"  /></td>
        <td></td>
    </tr>	
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">
* Please note the following with regards to the formatted Microsoft Excel report:<br />
<ol>
	<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
	<li>
		When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
		<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
		Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
		This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
		It is safe to click on the "Yes" button to open the document.
	</li>
</ol>
</td>
</tr>					
</table>
</form>
