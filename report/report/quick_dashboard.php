<?php
require_once("../inc/inc_ignite.php");
?><html><head>
<link rel="stylesheet" href="../../default.css" type="text/css" />
<link rel="stylesheet" href="../../styles/style_blue.css" type="text/css" />
<style type=text/css>
table {
	border-color: #ffffff;
}
table td {
	font-weight: normal;
	text-align: center;
	border-color: #ffffff;
}
</style>
</head><body>
<?php
error_reporting(-1);
function drawStackedBar($values,$chart_title,$layout,$required,$graph_ref,$series) {

	if($required=="Y") {
		require("../../lib/amcharts2-php/AmBarChart.php");
		AmChart::$swfObjectPath = "../../lib/amcharts2/swfobject.js";
		AmChart::$libraryPath = "../../lib/amcharts2/amcolumn/";
		AmChart::$jsPath = "../../lib/amcharts2-php/AmCharts.js";
		AmChart::$jQueryPath = "../../lib/amcharts2/amcolumn/jquery.js";
		AmChart::$loadJQuery = true;
	}
	
	$cwidth = 500;
	$cheight = 350;
	$cdepth = 15;
	$cangle = 45;
	$ccolwidth = 80;
	$style = "font-size: 7pt; line-height: 9pt; padding-right: 10px;";
	$cmargin_top = 20;
	$cmargin_bottom = 40;		
	$cmargin_right = 20;
	$cmargin_left = 20;		

	$chart = new AmBarChart("bar_".$graph_ref);
	//$chart->setTitle("<p class=".$title_class." style=\"margin: 10 0 0 0;\">".$chart_title."</p>");
		
	foreach($series as $s) {
		$chart->addSerie($s['id'], chartEncode(decode($s['value'])));
	}

	foreach($values as $v) { 
		$chart->addGraph($v['id'], $v['name'], $v['values'], array("color" => $v['color']));
	}

	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" => $cheight,
		"depth" => $cdepth,
		"angle" => $cangle,
		"font" => "Tahoma",
		"text_size" => 10,
		"column.type" => "100% stacked",
		"column.width" => $ccolwidth,
		"column.data_labels" => "<!".chartEncode("[CDATA[{percents}%]]").">",
		"column.data_labels_text_color" => "#ffffff",
		"decimals_separator" => ".",
		"plot_area.margins.top" => $cmargin_top,
		"plot_area.margins.right" => $cmargin_right,
		"plot_area.margins.left" => $cmargin_left,
		"plot_area.margins.bottom" => $cmargin_bottom,
		"background.border_alpha" => 0,
		"grid.category.alpha" => 0,
		"grid.value.alpha" => 0,
		"axes.category.alpha" => 0,
		"axes.value.alpha" => 0,
		"values.value.enabled" => "false",
		"legend.enabled" => "false",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!".chartEncode("[CDATA[{title}: {value} of {total} ]]").">"
	));

	echo html_entity_decode($chart->getCode());

}

function chartEncode($str) {

	$str = str_replace("&","&#38;",$str);
	$str = str_replace("\"","&#34;",$str);
	
	$str = str_replace("[","&#91;",$str);
	$str = str_replace("]","&#93;",$str);

	$str = str_replace("{","&#123;",$str);
	$str = str_replace("}","&#125;",$str);

	return $str;
}

/*** Title Page ***/
?>
<h1 style="text-align: center;"><?php echo $cmpname; ?></h1>
<h2 style="text-align: center;margin-top: -5px; margin-bottom: -5px;">Analysis of Risk Ratings</h2>
<p style="text-align: center;"><i><small>Report generated on <?php echo date("d M Y H:i"); ?>.</small></i></p>
<?php
/*** Get Data ***/
$data = array();
$cols = 0;
//IMPACT
$sql = "SELECT i.id, i.assessment as value, i.color, count(r.id) as rc FROM ".$dbref."_impact i LEFT JOIN ".$dbref."_risk_register r ON r.impact = i.id GROUP BY i.assessment ORDER BY i.rating_from ASC";
include("generate_db.php");
while($row = mysql_fetch_array($rs)) {
	$data['i'][] = $row;
} 
mysql_close($con);
$cols = count($data['i'])>$cols ? count($data['i']) : $cols;
//LIKELIHOOD
$likelihood = array();
$sql = "SELECT i.id, i.assessment as value, i.color, count(r.id) as rc FROM ".$dbref."_likelihood i LEFT JOIN ".$dbref."_risk_register r ON r.likelihood = i.id GROUP BY i.assessment ORDER BY i.rating_from ASC";
include("generate_db.php");
while($row = mysql_fetch_array($rs)) {
	$data['l'][] = $row;
} 
mysql_close($con);
$cols = count($data['l'])>$cols ? count($data['l']) : $cols;
//Inherent risk exposure
$sql = "SELECT i.id, i.magnitude as value, i.color, i.rating_from as ifrom, i.rating_to as ito FROM ".$dbref."_inherent_exposure i WHERE active = true ORDER BY i.rating_from ASC";
include("generate_db.php");
while($row = mysql_fetch_array($rs)) {
	$row['rc'] = 0;
	$data['e'][] = $row;
} 
mysql_close($con);
foreach($data['e'] as $key => $d) {
	$sql = "SELECT count(id) as rc FROM ".$dbref."_risk_register WHERE inherent_risk_exposure >=".$d['ifrom']." AND inherent_risk_exposure <= ".$d['ito'];
	include("generate_db.php");
	if(mysql_num_rows($rs)>0) {
		$row = mysql_fetch_array($rs);
		$data['e'][$key]['rc'] += $row['rc'];
	} else {
		$data['e'][$key]['rc'] += 0;
	}
}
$cols = count($data['e'])>$cols ? count($data['e']) : $cols;
//Control Effectiveness
$sql = "SELECT i.id, i.effectiveness as value, i.color, count(r.id) as rc FROM ".$dbref."_control_effectiveness i LEFT JOIN ".$dbref."_risk_register r ON r.percieved_control_effectiveness = i.id GROUP BY i.effectiveness ORDER BY i.rating ASC";
include("generate_db.php");
while($row = mysql_fetch_array($rs)) {
	$data['c'][] = $row;
} 
mysql_close($con);
$cols = count($data['c'])>$cols ? count($data['c']) : $cols;
//residual risk exposure
$sql = "SELECT i.id, i.magnitude as value, i.color, i.rating_from as ifrom, i.rating_to as ito FROM ".$dbref."_residual_exposure i WHERE active = true ORDER BY i.rating_from ASC";
include("generate_db.php");
while($row = mysql_fetch_array($rs)) {
	$row['rc'] = 0;
	$data['r'][] = $row;
} 
mysql_close($con);
foreach($data['r'] as $key => $d) {
	$sql = "SELECT count(id) as rc FROM ".$dbref."_risk_register WHERE residual_risk_exposure >=".$d['ifrom']." AND residual_risk_exposure <= ".$d['ito'];
	include("generate_db.php");
	if(mysql_num_rows($rs)>0) {
		$row = mysql_fetch_array($rs);
		$data['r'][$key]['rc'] += $row['rc'];
	} else {
		$data['r'][$key]['rc'] += 0;
	}
}
$cols = count($data['r'])>$cols ? count($data['r']) : $cols;



$graph_ref = "0";
$layout = 2;
$chart_title = "Graph Analysis";
$required = "Y";



	$series = array(
		'i' => array('id'=>"i",'value'=>"Impact"),
		'l' => array('id'=>"l",'value'=>"Likelihood"),
		'e' => array('id'=>"e",'value'=>"Inherent Risk Exposure"),
		'c' => array('id'=>"c",'value'=>"Control Effectiveness"),
		'r' => array('id'=>"r",'value'=>"Residual Risk Exposure")
	);
	$values = array();
	foreach($series as $s) {
		foreach($data[$s['id']] as $d) {
			$values[] = array('id'=>$s['id'].$d['id'], 'name'=>$d['value'], 'color'=>"#".$d['color'],
				'values'=>array($s['id']=>$d['rc'])
			);
		}
	}
echo "<div align=center>";
drawStackedBar($values,$chart_title,$layout,$required,$graph_ref,$series);
/*** LEGEND ***/


$width = 80;
echo "<table cellpadding=3 style=\"border:1px solid #ababab;\">";
foreach($series as $s) { 
$total = 0;
	echo chr(10)."<tr><td style=\"text-align: right;\">";
	echo "<table cellpadding=3>";
		echo chr(10)."<tr><td style=\"font-weight: bold; text-align: right; vertical-align: bottom; padding-bottom: 6px;\">".$s['value']."</td><td><table cellpadding=3>";
			echo chr(10)."<tr>";
			foreach($data[$s['id']] as $d) {
				echo "<td width=$width >".$d['value']."</td>";
			}
			//echo "<td width=$width ><b>Total</b></td>";
			echo "</tr>";
			echo chr(10)."<tr>";
			foreach($data[$s['id']] as $d) {
				echo "<td width=$width style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color'].";\">".$d['rc']."</div></td>";
				$total += $d['rc'];
			}
			//echo "<td width=$width><div style=\"border: 1px solid #dddddd;\"><b>".$total."</b></div></td>";
			echo "</tr>";
		echo chr(10)."</table></td></tr>";
	echo "</table>";
	echo "</td></tr>";
}
echo "</table>";





/*
echo "<table cellpadding=3 style=\"border:1px solid #ababab;\">";
foreach($series as $s) { 
	echo "<tr><td>";
		echo "<table cellpadding=3 style=\"margin-top: 10px;\">";
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				foreach($data[$s['id']] as $d) {
					echo "<td>".$d['value']."</td>";
				}
			echo "</tr>";
			echo "<tr>";
				echo "<td style=\"text-align: right;\">".$s['value']."</td>";
				foreach($data[$s['id']] as $d) {
					echo "<td style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color']."; \">".$d['rc']."</div></td>";
				}
			echo "</tr>";
		echo "</table>";
	echo "</td></tr>";
}
echo "</table>";


*/

/*
echo "<P>&nbsp;</p>";
echo "<table cellpadding=3>";
$a = 0;
foreach($series as $s) {
	if($a > 0) {
		echo "<tr><td colspan=".($cols+1).">&nbsp;</td></tr>";
	}
	echo "<tr>";
		echo "<td>&nbsp;</td>";
		foreach($data[$s['id']] as $d) {
			echo "<td >".$d['value']."</td>";
		}
		if(count($data[$s['id']])<$cols) {
			echo "<td colspan=".($cols-count($data[$s['id']])).">&nbsp;</td>";
		}
	echo "</tr>";
	echo "<tr>";
		echo "<td  style=\"text-align: right;\">".$s['value']."</td>";
		foreach($data[$s['id']] as $d) {
			//echo "<td width=".$width."% style=\"font-weight: normal;\"><span style=\"background-color: #".$d['color']."; \">&nbsp;&nbsp;&nbsp;</span> ".$d['rc']."</td>";
			echo "<td style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color']."; \">".$d['rc']."</div></td>";
		}
		if(count($data[$s['id']])<$cols) {
			echo "<td colspan=".($cols-count($data[$s['id']])).">&nbsp;</td>";
		}
	echo "</tr>";
	$a++;
}
echo "</table>";*/
echo "</div>";

?>
</body></html>