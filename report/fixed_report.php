<link href="/library/jquery/css/jquery-ui-date.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php
include_once("../inc/init.php");
//include_once("../class/dbconnect.php");
//include_once("../class/report.php");
//include_once("../class/naming.php");

$rep 	  = new Report();
$report   = $rep -> generateFixedReport();
$nm 	  = new Naming();
$headers                                = $nm -> sortedHeaders();
$q_headers['query_item']                  = $headers['query_item'];
$q_headers['query_reference']             = $headers['query_reference'];
$q_headers['query_description']           = $headers['query_description'];
$q_headers['query_background']            = $headers['query_background'];
$q_headers['query_type']                  = $headers['query_type'];
$q_headers['query_category']              = $headers['query_category'];
$q_headers['financial_exposure']          = $headers['financial_exposure'];
$q_headers['monetary_implication']        = $headers['monetary_implication'];
$q_headers['risk_level']                  = $headers['risk_level'];
$q_headers['risk_type']                   = $headers['risk_type'];
$q_headers['risk_detail']                 = $headers['risk_detail'];
$q_headers['finding']                     = $headers['finding'];
$q_headers['internal_control_deficiency'] = $headers['internal_control_deficiency'];
$q_headers['recommendation']              = $headers['recommendation'];
$q_headers['client_response']             = $headers['client_response'];
$q_headers['auditor_conclusion']          = $headers['auditor_conclusion'];
$q_headers['financial_year']              = $headers['financial_year'];
$q_headers['query_owner']                 = $headers['query_owner'];
$q_headers['query_date']                  = $headers['query_date'];
$q_headers['query_deadline_date']         = $headers['query_deadline_date'];
$q_headers['query_progress']              = $headers['query_progress'];
$qHeaders = array();
$fQuery   = array(); 

foreach($report as $key => $repArr)
{
   foreach($q_headers as $index => $head)
   {
       if($index == "query_progress")
       {
		 $act 		  = new RiskAction($repArr['query_item'], "","","", "", "", "", "", "");
	 	 $riskActions = $act->_getRiskActions();		
		 $fQuery[$key]['query_progress'] = round( (isset($riskActions['progress']) ? $riskActions['progress'] : 0), 2)."%";
		 $qHeaders[$index]  = $head;	
       } elseif(isset($repArr[$index]) || array_key_exists($index, $repArr))
	   {
		  $qHeaders[$index]  	= $head;
		  $fQuery[$key][$index] = $repArr[$index];
	   }	
	}
}

echo "<center><h1>Query Register Report for ".$_SESSION['ia_cmp_name']." as at ".date("d-M-Y")." </h1></center>";
echo "<table>";
echo "<tr>"; 
foreach($qHeaders as $key => $heads)
{
	echo  "<th>".$heads."</th>";
}

echo "</tr>";
foreach($fQuery as $q => $qArr)
{
	echo "<tr>";
		foreach($qArr as $i => $qVal){		
			echo "<td>".$qVal."</td>";
		}	
	echo "</tr>";
}
echo  "</table>";	
echo "<center><small><i><font size='2'>Report generated on ".date("d-M-Y")."  ".date("H:i:s")."</font></i></small></center>";
?>
