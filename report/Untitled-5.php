<?php
	$scripts = array( 'generate.js','menu.js',  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../inc/header.php");
?>
<script>
$(function(){
	$("table#report_values_table").find("th").css({"text-align":"left"});
	$("table#grouping_table").find("th").css({"text-align":"left"});
	$("table#generate_report_table").find("th").css({"text-align":"left"});	
});
</script>
<div id="searchBox">
<div id="generate_report_messsage" class="message"></div>
<form name="report-header-form" id="report-header-form" method="post">
<table id="query_field_tobeincluded">
	<tr>
		<th colspan="3" align="left">Select the Query information to be displayed on the report</th>
	</tr>
	<tr>
		<td><input type="checkbox" name="query_type" value="" id="query_type" >Query Type</td>
		<td><input type="checkbox" name="query_category" value="" id="risk_category" >Query Category</td>
		<td><input type="checkbox" name="query_description" value="" id="risk_description" >Query Description</td>
	</tr>
	<tr>ignittps_i
		<td><input type="checkbox" name="query_background" value="" id="risk_background" >Background of risk</td>
		<td><input type="checkbox" name="monetary_implication" value="" id="monetary_implication"/>
Monetary Implication
		</td>
		<td><input type="checkbox" name="risk_detail" value="" id="risk_detail" >Risk Detail</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="recommendation" value="" id="recommendation" >Recommendation</td>
		<td><input type="checkbox" name="financial_exposure" id="financial_exposure" value="" />Financial Exposure</td>
		<td><input type="checkbox" name="risk_level" value="" id="risk_level" >Risk Level</td>
	</tr>  
	<tr>
		<td><input type="checkbox" name="query_reference" value="" id="query_reference" >Query Reference</td>				
		<td><input type="checkbox" name="risk_type" value="" id="risk_type" >Risk Type</td>
		<td><input type="checkbox" name="query_status" value="" id="query_status" >Query Status</td>
	</tr>  
	<tr>
	<td colspan="3">
		<input type="button" value="Check All" id="r_checkAll" name="r_checkAll" />
		<input type="button" value="UnCheck All" id="r_uncheckAll" name="r_uncheckAll" />
		<input type="button" value="Invert" id="r_invert" name="r_invert" />
	</td>
	</tr>                
</table>
</form>
<form id="report-values-form" name="report-values-form">
<table border="1" id="report_values_table" >
	<tr>
		<td colspan="3"><h3>Select the filter you wish to apply</h3></th>
	</tr>
	<tr>
		<th>Query Reference:</th>
		<td>
			<input type="text" name="_query_reference" id="_query_reference" value="" />
		</td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Query Type:</th>
		<td>
			<select name="_query_type" id="_query_type"  multiple="multiple">
			   <option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Query Category:</th>
		<td>
			<select id="_query_category" name="_query_category" multiple="multiple">         
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Query Description:</th>
		<td>
			<input type="text" name="_query_description" id="_query_description" value="" />
		</td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>		
	</tr>
	<tr>
		<th>Background of the Query:</th>
		<td>
			<input type="text" name="_query_background" id="_query_background" value=""  />
		</td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>
	</tr>
   <tr>
		<th>Financial Exposure:</th>
		<td>
			<select id="_financial_exposure" name="_financial_exposure" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Risk Level:</th>
		<td>
			<select name="_risk_level" id="_risk_level" multiple="multiple">
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Risk Type:</th>
		<td>
			<select name="_risk_type" id="_risk_type" multiple="multiple">                    
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
	</tr>
	<tr>
		<th>Risk Detail:</th>
		<td><input type="text" name="_risk_detail" id="_risk_detail" value=""  /></td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th>Finding:</th>
		<td><input type="text" name="_add_finding" id="_add_finding" value=""  /></td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th>Recommendation:</th>
		<td><input type="text" name="_recommendation" id="_recommendation" value=""  /></td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th>Client Response:</th>
		<td><input type="text" name="_client_response" id="_client_response" value=""  /></td>
		<td>
			<select>
				<option value="any">Match any word</option>
				<option value="any">Match all words</option>
				<option value="any">Match excat phrase</option>
			</select>
		</td>	
	</tr>	
	<tr>
		<th>Query Status:</th>
		<td>
			<select name="_query_status" id="_query_status" multiple="multiple">                                       
				<option value="all" selected="selected">ALL</option>
			</select>
			<br /><i><small>Use CTRL key to select multiple options</small></i>
		</td>
		<td></td>
		</tr>
	<tr>
		<th>Query Date:</th>
		<td>
			From : <input type="text" name="_from_query_date" id="_from_query_date" value="" class="datepicker" />
			to <input type="text" name="_to_query_date" id="_to_query_date" value="" class="datepicker" />
		</td>
		<td></td>
	</tr>		
</table>   
</form>
<form id="grouping-options-form" name="grouping-options-form">
<table id="grouping_table" border="1">
	<tr>
		<td colspan="2"><h3>Choose your group and sort options</h3></th>
	</tr>
	<tr>
		<th width="65">Group By</th>
		<td width="371">
			<select name="group_by" id="group_by">
				<option value="">No grouping</option>
				<option value="query_type_">Query Type</option>
				<option value="query_category_">Query Category</option>	
				<option value="financial_exposure_">Financial Exposure</option>
				<option value="risk_level_">Risk Level</option>
				<option value="risk_type_">Risk Type</option>
			</select>
	  </td>
  </tr>
	<tr>
		<th>Sort by</td>
		<td>
			<ul id="sortable" style="list-style:none;">
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__query_item"></span>Query Item
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__query_type"></span>Query Type
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__query_category"></span>Query Category
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__query_description"></span>Query Description
				</li>				
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__query_background"></span>Query Background
				</li>								
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__financial_exposure"></span>Financial Exposure
				</li>												
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__monetary_implication"></span>Monetary Implication
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__risk_level"></span>Risk Level
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__risk_type"></span>Risk Type
				</li>				
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__risk_detail"></span>Risk Detail
				</li>								
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__add_finding"></span>Finding
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__recommendation"></span>Recommendation
				</li>
				<li class="ui-state-default">
					<span class="ui-icon ui-icon-arrowthick-2-n-s" id="__client_response"></span>Client Response
				</li>				
			</ul>
		</td>
	</tr>
</table>
</form>
<form id="document-format-form" name="document-format-form">
<table id="document_format_table" border="1">
	<tr>
		<td colspan="2"><h3>Choose the document format of your report</h3></th>
	</tr>
	<tr>
		<td colspan="2"><input type="radio" id="onscreen" name="display" value="screen" checked="checked" />OnScreen Display</td>
	<tr>
	<tr>
		<td colspan="2"><input type="radio" id="excell" name="display" value="excell" />Microsoft Excel( Plain Text)</td>
	<tr>
	<tr>
		<td colspan="2"><input type="radio" id="excell_formated" name="display" value="excell_formated" />Microsft Excel(Formatted)</td>
	<tr>
	<tr>
		<td colspan="2"><input type="radio" id="pdf" name="display" value="pdf" />Save to Pdf</td>
	</tr>
</table>
</form>
<form id="generate-report-form" name="generate-report-form">
<table id="generate_report_table" border="1">
	<tr>
		<td colspan="2"><h3>Generate the report</h3></th>
	</tr>
	<tr>
		<th>Report Title</td>
		<td><input type="text" id="report_title" name="report_title" value="" /></td>
	<tr>
	<tr>
		<th></th>
		<td>
		<input type="submit" name="generate_report" id="generate_report" value="Generate Report"  />
		<input type="reset" id="reset" name="reset" value="Reset" />
		</td>
	<tr>
	<tr>
		<th>Report Name</td>
		<td><input type="text" id="report_name" name="report_name" value="" /></td>
	<tr>
	<tr>
		<th></th>
		<td><input type="submit" id="save_quick_report" name="save_quick_report" value="Save as Quick Report"  /></td>
</table>
</form>
</div>
<div id="showResults"></div>