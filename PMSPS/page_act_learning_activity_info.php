<?
include("activate_inc.php");

$id = $_REQUEST["id"];

$learningActivity = new LearningActivity();
$learningActivity->loadFromId( $id );
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Learning Activity</h1>
    
    <? include("info_learning_activity.php"); ?>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_activate.php');" />
    </div>  
  </body>
</html>