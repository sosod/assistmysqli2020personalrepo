<? 
  printErrors( $errors ); 
  
  if( count( $errors ) == 0 )
  {
    if( $selectedUserStatus->getStatus() == "new" )
    {
?>
  <h5>To complete the activation, please request employer approval:</h5>
  <form id="frmRequestApproval" method="post">
    <input type="submit" value="Request approval" />
    <input type="hidden" name="action" value="request_approval" />
  </form>
<? 
    }
    else if( $selectedUserStatus->getStatus() == "pending" )
    {
?>
  <h5>To complete the activation employer approval is required.</h5>
<?
      //$showEmployerApprove = false;
      
      /*if( $selectedUser->getManagerId() == "S" && $selectedUser->getId() == getLoggedInUserId() )
      {
        $showEmployerApprove = true;
      }
      else if( $selectedUser->getManagerId() == getLoggedInUserId() )
      {
        $showEmployerApprove = true;
      }*/ 
      
      if( $selectedUser->isManagedBy( getLoggedInUserId() ) )
      {
?>  
  <form id="frmEmployerApproval" method="post">
    <input type="submit" value="Employer approval" />
    <input type="hidden" name="action" value="employer_approval" />
  </form>
<?     
      }
      else
      {
?>
  <p class="warning">You are not this user's manager and can therefore not approve the activation.</p>    
<?        
      }   
    }
    else if( $selectedUserStatus->getStatus() == "employer" )
    {
?>
  <h5>To complete the activation employee approval is required.</h5>
<?
      $showEmployeeApprove = false;
      
      if( $selectedUser->getId() == getLoggedInUserId() )
      {
        $showEmployeeApprove = true;
      }
      else if( $selectedUser->isManagedBy( getLoggedInUserId() ) )
      {
        $showEmployeeApprove = true;
      }
      
      if( $showEmployeeApprove )
      {
?>  
  <form id="frmEmployeeApproval" method="post">
    <input type="submit" value="Employee approval" />
    <input type="hidden" name="action" value="employee_approval" />
  </form>
<?     
      }
      else
      {
?>
  <p class="warning">You are not this user or this user's manager and can therefore not approve the activation.</p>    
<?        
      }   
    }
    else if( $selectedUserStatus->getStatus() == "employee" )
    {
?>
  <h5>Activation has been completed</h5>
<?
    }
  }      
    ?>