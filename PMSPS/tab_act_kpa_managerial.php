<? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
<div class="actions">
  <input type="button" value="New Managerial KPI" onclick="redirect('<?=$moduleType?>_activate_kpa_man.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="25%">Skill</th>
    <th width="10%">Level</th>
    <th width="20%">Measurement</th>
    <th width="20%">Comments</th>
    <? if( $useWeights ){ ?>
    <th width="5%">Weight</th>
    <? } ?>
    <th>Actions</th>
  </tr>
  <?
    if( isset( $manKPAs ) && count( $manKPAs ) > 0 )
    {
      foreach( $manKPAs as $kpa )
      {
  ?>
  <tr>
    <td><?= $kpa->getCoreManagerialSkill()->getSkill() ?></td>
    <td><?=$kpa->getSkillLevel()?> - <?= KPAManagerial::displaySkillLevel( $kpa->getSkillLevel() ) ?></td>
    <td><?= $kpa->getMeasurement() ?></td>
    <td><?= $kpa->getComments() ?></td>
    <? if( $useWeights ){ ?>
    <td><?= $kpa->getWeight() ?></td>
    <? } ?>
    <td>
      <input type="button" value="View" onclick="redirect('<?=$moduleType?>_activate_kpa_man_info.php?id=<?= $kpa->getId() ?>');" />
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_kpa_man.php?id=<?= $kpa->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_kpa_man.php', '<?= $kpa->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }

      if( $useWeights ){
  ?>
  <tr>
    <td colspan="4">&nbsp;</td>
    <td style="border-top:2px solid #333;border-bottom:2px solid #333;"><?= $manKPAWeightTotal ?></td>
    <td>&nbsp;</td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="5">No Managerial KPIs</td></tr>
  <?
    }
  ?>
</table>