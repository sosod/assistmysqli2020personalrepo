<?
  function getNumOfKPAEvaluations( $type, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, $symbol )
  {
    //debug("selectedYearId: " . $selectedYearId );
    //debug("selectedPeriod: " . $selectedPeriod );
    //debug("departmentId: " . $departmentId );
    //debug("jobLevel: " . $jobLevel );
    //debug("exists(dept): " . boolValue(exists($departmentId)) );

    $sql = "select count(es.id) AS numOfKPAs from assist_".getCompanyCode(). "_pmsps_" . EvaluationStatus::$TABLE_NAME . " es, " .
           " assist_".getCompanyCode()."_pmsps_" . UserSetup::$TABLE_NAME . " us, " .
           " assist_".getCompanyCode()."_" . User::$TABLE_NAME . " u " .
           " WHERE es.eval_year_id = ". $selectedYearId .
           " AND es.eval_period = ". $selectedPeriod .
           " AND es.type = '" . $type . "' " .
           " AND es.user_id = u.tkid " .
           " AND es.user_id = us.user_id " .
           " AND es.status = 'completed'";

    if( exists($departmentId) )
    {
      $sql .= " AND u.tkdept = " . $departmentId;
    }

    if( exists($jobLevel) )
    {
      $sql .= " AND us.job_level = " . $jobLevel;
    }

    if( exists($symbol) )
    {
      $sql .= " AND es.final_kpa_symbol = '" . $symbol . "'";
    }

    //debug( $sql );

    $record = getRecord( getResultSet( $sql ) );

    //debug("numOfKPAs " . $symbol . ": " . $record["numOfKPAs"] );

    return $record["numOfKPAs"];
  }

  $selectedYearId = $_REQUEST["selectedYearId"];
  $selectedPeriod = $_REQUEST["selectedPeriod"];
  $departmentId = $_REQUEST["departmentId"];
  $jobLevel = $_REQUEST["jobLevel"];

  $numOfEvaluations = getNumOfKPAEvaluations( $moduleType, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, null );

  $numOfLSymbols = getNumOfKPAEvaluations( $moduleType, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, "L" );
  $numOfESymbols = getNumOfKPAEvaluations( $moduleType, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, "E" );
  $numOfSSymbols = getNumOfKPAEvaluations( $moduleType, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, "S" );
  $numOfNSymbols = getNumOfKPAEvaluations( $moduleType, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, "N" );
  $numOfBSymbols = getNumOfKPAEvaluations( $moduleType, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, "B" );

  $percentageL = $numOfEvaluations != 0 ? $numOfLSymbols / $numOfEvaluations * 100 : 0;
  $percentageE = $numOfEvaluations != 0 ? $numOfESymbols / $numOfEvaluations * 100 : 0;
  $percentageS = $numOfEvaluations != 0 ? $numOfSSymbols / $numOfEvaluations * 100 : 0;
  $percentageN = $numOfEvaluations != 0 ? $numOfNSymbols / $numOfEvaluations * 100 : 0;
  $percentageB = $numOfEvaluations != 0 ? $numOfBSymbols / $numOfEvaluations * 100 : 0;

  $xml = '<?xml version="1.0" encoding="UTF-8"?>
          <chart>
          	<series>
          		<value xid="0">B</value>
          		<value xid="1">N</value>
          		<value xid="2">S</value>
          		<value xid="3">E</value>
          		<value xid="4">L</value>
          	</series>
          	<graphs>
          		<graph gid="1">
          			<value xid="0">10</value>
          			<value xid="1">15</value>
          			<value xid="2">60</value>
          			<value xid="3">12</value>
          			<value xid="4">3</value>
          		</graph>
          		<graph gid="2">';
  $xml .= '			<value xid="0">' . $percentageB . '</value>';
  $xml .= '			<value xid="1">' . $percentageN . '</value>';
  $xml .= '			<value xid="2">' . $percentageS . '</value>';
  $xml .= '			<value xid="3">' . $percentageE . '</value>';
  $xml .= '			<value xid="4">' . $percentageL . '</value>';
  $xml .= '   </graph>
          	</graphs>
          </chart>';

  //debug( "xml: " . $xml );
  echo $xml;
?>