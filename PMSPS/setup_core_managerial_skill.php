<?php
include("inc_ignite.php");


$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$coreManagerialSkill = new CoreManagerialSkill();

if( $action == "save" )
{
  $coreManagerialSkill->loadFromRequest( $_REQUEST );
  $coreManagerialSkill->save();

  redirect("setup_core_managerial_skills.php" );
}

if( exists( $id ) )
{
  $coreManagerialSkill->loadFromId( $id );
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript" src="/PMSPS/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/PMSPS/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">

    </script>
    <script type="text/javascript">
      var CKOptions =
      {
        height : '80px',
        width : '500px',
        toolbar : [
          ['Bold', 'Italic', 'NumberedList', 'BulletedList','Paste','PasteFromWord', 'Source' ]
        ]
      };

      $(document).ready(function()
      {
        //$("textarea[name='definition'],textarea[name='definitionBasic'],textarea[name='definitionCompetent'],textarea[name='definitionAdvanced'],textarea[name='definitionExpert']").ckeditor( CKOptions );
        $("#frmCoreManagerialSkill").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Contract KPI</h1>

    <form id="frmCoreManagerialSkill" method="post" >
      <table class="form full" cellspacing="0">
        <tr>
          <th>Skill</th>
          <td><input type="text" name="skill" value="<?= $coreManagerialSkill->getSkill() ?>" class="required"/></td>
        </tr>
				<tr>
          <th>Definition</th>
          <td><textarea name="definition" class="required" cols="40" rows="5"><?= $coreManagerialSkill->getDefinition() ?></textarea></td>
        </tr>
        <tr>
          <th>Basic Skill Definition</th>
          <td><textarea name="definitionBasic" cols="40" rows="5"><?= $coreManagerialSkill->getDefinitionBasic() ?></textarea></td>
        </tr>
        <tr>
          <th>Competent Skill Definition</th>
          <td><textarea name="definitionCompetent" cols="40" rows="5"><?= $coreManagerialSkill->getDefinitionCompetent() ?></textarea></td>
        </tr>
        <tr>
          <th>Advanced Skill Definition</th>
          <td><textarea name="definitionAdvanced"  cols="40" rows="5"><?= $coreManagerialSkill->getDefinitionAdvanced() ?></textarea></td>
        </tr>
        <tr>
          <th>Expert Skill Definition</th>
          <td><textarea name="definitionExpert"  cols="40" rows="5"><?= $coreManagerialSkill->getDefinitionExpert() ?></textarea></td>
        </tr>
				<tr>
          <th>Active</th>
          <td><?= select( "active", getYesNoSelectList(), $coreManagerialSkill->getActive(), true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $coreManagerialSkill->getId() ?>" />
      <input type="hidden" name="action" value="save" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_core_managerial_skills.php');" />
    </div>
  </body>
</html>