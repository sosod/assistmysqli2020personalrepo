<?php
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$kpa = new KPA();
$kpa->setKPAType( $_REQUEST["kpaType"] );

if( $action == "save" )
{
  $kpa->loadFromRequest( $_REQUEST );
  $kpa->save();

  redirect( $moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $kpa->setId( $id );
  $kpa->delete();

  redirect( $moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $kpa->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
          $("#frmKPA").validate({
              submitHandler: function(form) {

                  var msg = null;

              <? if( $useWeights ){ ?>
                  var $weight = $('#fldWeight');

                  if( $weight.val() == "" )
                  {
                      msg = "Weight is required.";
                  }

                  if( /^([0-9]+)(\.([0-9]{2}))?$/i.test( $weight.val() ) == false )
                  {
                      msg = "Please specify a valid weight.";
                  }

                  if( parseFloat( $weight.val() ) > 25 )
                  {
                      msg = "Weight may not be more than 25.";
                  }

                  if( msg != null )
                  {
                      $weight.after('<label for="fldWeight" generated="true" class="error">' + msg + '</label>');
                      return false;
                  }
                  <? } ?>

                  if( msg == null )
                  {
                      form.submit();
                  }
              }
          });
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: KPI</h1>

    <? $type = "perform"; ?>

    <form id="frmKPA" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Type *</th>
          <td><?= $kpa->getTypeDisplay() ?></td>
        </tr>
        <?php if( $kpa->getKPAType() == "career" && $moduleType == "perform" ){ ?>
        <tr>
          <th>SDBIP #:</th>
          <td><input type="text" name="sdbipNum" value="<?= $kpa->getSDBIPNum() ?>" /></td>
        </tr>
        <?php } ?>
        <tr>
          <th>Name: *</th>
          <td><input type="text" name="name" value="<?= $kpa->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Description: *</th>
          <td><textarea name="description" cols="50" rows="5" class="required"><?= $kpa->getDescription() ?></textarea></td>
        </tr>
        <tr>
          <th>Measurement: *</th>
          <td><textarea name="measurement" cols="50" rows="5" class="required"><?= $kpa->getMeasurement() ?></textarea></td>
        </tr>
        <? if( $useWeights ){ ?>
        <tr>
          <th>Weight:</th>
            <?php /* <td><?= select( "weight", getNumericSelectList(0,100), $kpa->getWeight(), true ) ?></td> */ ?>
            <td><input type="text" id="fldWeight" name="weight" value="<?= $kpa->getWeight() ?>" class="number" maxlength="4" size="4"/></td>
        </tr>
        <? } ?>
        <tr>
          <th>Outcome:</th>
          <td><textarea name="outcome" cols="50" rows="5"><?= $kpa->getOutcome() ?></textarea></td>
        </tr>
        <tr>
          <th>Portfolio of evidence:</th>
          <td><textarea name="evidence" cols="50" rows="5"><?= $kpa->getEvidence() ?></textarea></td>
        </tr>
        <tr>
          <th>Comments:</th>
          <td><textarea name="comments" cols="50" rows="5"><?= $kpa->getComments() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />
      <input type="hidden" name="id" value="<?= $kpa->getId() ?>" />
      <? if( $useWeights == false ){ ?>
      <input type="hidden" name="weight" value="0" />
      <? } ?>
      <input type="hidden" name="kpaType" value="<?= $kpa->getKPAType() ?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>
  </body>
</html>