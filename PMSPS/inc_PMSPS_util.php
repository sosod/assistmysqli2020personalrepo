<?
$CONST_MODULE_TYPES = array( "career" => "Performance Development", "perform" => "Performance Contract" );

function debug($msg)
{
  /*$fd = fopen("C:\www\hosts\assist.ignite4u.co.za\logs\PMSPS.log", "a");
  $str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg;
  fwrite($fd, $str . "\n");
  fclose($fd);*/
}

/*********** VIEW HELPER FUNCTIONS **********/
function getCompanyCode()
{
  global $cmpcode;

  return $cmpcode;
}

function getCompanyName()
{
  global $cmpname;

  return $cmpname;
}

function getLoggedInUserId()
{
  global $tkid;

  return $tkid;
}

function select( $name, $options, $value, $required, $id = false, $emptyOption = "Select from list ...", $style = false )
{
	debug("building select with: name = " . $name );

  echo "<select " . ($id ? "id=\"$id\" " : "") . "name=\"" . $name . "\" " . (($required) ? "class=\"required\"" : "") . (($style) ? "style=\"$style\"" : "") . " >";

  if( $emptyOption )
  {
    echo "<option value=\"\">$emptyOption</option>";
  }

  foreach ($options as $option)
  {
    echo "<option value=\"" . htmlentities($option[0]) . "\" " . (($option[0] == $value) ? "selected=\"selected\"" : "") . ">" . $option[1] . "</option>";
  }

  echo "</select>";
}

function multiselect( $name, $options, $required, $values )
{
  echo "<select name=\"" . $name . "\" " . (($required) ? "class=\"required\"" : "") . " multiple=\"multiple\" style=\"width:200px;\" >";

  foreach ($options as $option)
  {
    echo "<option value=\"" . htmlentities($option[0]) . "\" " . ( arrayContains( $values, $option[0] )  ? "selected=\"selected\"" : "") . ">" . $option[1] . "</option>";
  }

  echo "</select>";
}

function arrayContains( $array, $value )
{
  foreach( $array as $element )
  {
    if( $element == $value )
    {
      return true;
    }
  }

  return false;
}

function arrayStartWithContains( $array, $value )
{
  foreach( $array as $element )
  {
    if( strpos( $element, $value ) === 0 )
    {
      return true;
    }
  }

  return false;
}

function exists( $value )
{
	if( isset( $value ) )
	{
	  if( is_string( $value ) )
	  {
  	  $value = trim( $value );

  		if( strlen( $value ) != 0 )
  		{
  			return true;
  		}
	  }
	  else
	  {
	    return true;
	  }
	}

	return false;
}

function redirect($url)
{
  header("Location: $url");
  exit();
}

function getParam( $name )
{
  return $_REQUEST[ $name ];
}

function getSQLParam( $name )
{
  return mysql_real_escape_string( getParam( $name ) );
}

function boolValue( $value )
{
  return $value ? "Yes" : "No";
}

function printErrors( $errors )
{
  if( count($errors) > 0 )
  {
    echo "<h5>Please correct the following errors to continue:</h5>";
    echo "<ul class=\"error-messages\">";

    foreach( $errors as $error )
    {
      echo "<li>$error</li>";
    }

    echo "</ul>";
  }
}

function printMessages( $messages )
{
  if( count($messages) > 0 )
  {
    echo "<h5>Import results:</h5>";
    echo "<ul class=\"error-messages\">";

    foreach( $messages as $message )
    {
      echo "<li>$message</li>";
    }

    echo "</ul>";
  }
}

/* CAREER - EVALUATION */
function getPeriodStartDate( $evalPeriodType, $evalYear, $evalPeriod )
{
  $months = 0;

  if( $evalPeriodType == "quart" || $evalPeriodType == "quart_oral" )
  {
    $months = ($evalPeriod - 1) * 3;
  }
  else if( $evalPeriodType == "bi_annual" )
  {
    $months = ($evalPeriod - 1) * 6;
  }

  return date( "d M y ", strtotime( "+" . $months ." month", strtotime( $evalYear->getStart() ) ) );
}

function getPeriodEndDate( $evalPeriodType, $evalYear, $evalPeriod )
{
  $months = 0;

  if( $evalPeriodType == "quart" || $evalPeriodType == "quart_oral" )
  {
    $months = ($evalPeriod - 1) * 3 + 2;
  }
  else if( $evalPeriodType == "bi_annual" )
  {
    $months = ($evalPeriod - 1) * 6 + 5;
  }

  return date( "t M y ", strtotime( "+" . $months ." month", strtotime( $evalYear->getStart() ) ) );
}

function isDateInFuture( $date )
{
  $now = getdate();

  return strtotime( $date ) > $now[0];
}

/* EMAIL STUFF */
function getEmailTemplate($filename, $replacements ) {
  $template = '';
  if ($fp = fopen($filename, 'rb')) {
    while (!feof($fp)) {
      $template .= fread($fp, 1024);
    }
    fclose($fp);
  } else {
    return false;
  }

  foreach( $replacements as $replacement )
  {
    $template = str_replace( $replacement[0], $replacement[1], $template );
  }

  return $template;
}
?>