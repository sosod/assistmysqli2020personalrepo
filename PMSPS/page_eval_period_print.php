<?php
include("evaluate_inc.php");

$action = $_REQUEST["action"];

if( isset( $selectedUser ) == false || isset( $selectedYear ) == false || isset( $selectedPeriod ) == false )
{
 redirect($moduleType."_evaluate_period.php");
}

if( $moduleType != "contract" )
{
  $careerGoals = getCareerGoals( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
  $careerKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "career" );
  $bonusKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "bonus" );
}

$strongSkills = getEvaluationSkills( $moduleType, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod, "strong" );
$developmentSkills = getEvaluationSkills( $moduleType, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod, "development" );
$comments = getEvaluationComments( $moduleType, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

if( $moduleType == "career" )
{
  $jobLevelKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "job_level" );
  $uniqueKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "unique" );
}

if( $moduleType == "perform" || $moduleType == "contract" )
{
  $orgKPAs = getOrganisationalKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
  $manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
}

if( $action == "pdf" )
{
  require('fpdf/IgnitePDF.class.php');
  $pdf=new IgnitePDF('L','pt','A4');
  $pdf->AddPage();
  $pdf->H0( $moduleName . ' Evaluation' );
  $pdf->InlineText( "Year: ", 50, 16, "R", "B" );
  $pdf->InlineText( $selectedYear->getName(), 200, 16, "L", "" );
  $pdf->Ln();

  $pdf->InlineText( "Period: ", 50, 12, "R", "B" );
  $pdf->InlineText( getPeriodStartDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) . " - " . getPeriodEndDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ), 200, 12, "L", "" );
  $pdf->Ln();
  $pdf->Ln();

  /* PERSONAL INFORMATION */
  $pdf->TableHeading('Personal Information');

  $widths = array( 100, 380 );
  $rows = array( array( "Name  ", $selectedUser->getDecodedFullName() ), /*array( "Gender", $selectedUser->getGenderDisplay() ),*/
               /*array( "Race", $selectedUser->getRace() ),*/ array( "Department  ", $selectedUser->getDepartment() ), array( "Section  ", $selectedUser->getSection() ),
               array( "Job title  ", $selectedUser->getJobTitle() ), array( "Job level  ", $selectedUser->getJobLevel() ), array( "Job number  ", $selectedUser->getJobNumber() ),
               array( "Manager  ", $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ) );

  $pdf->infoTable( $rows, $widths );

  if( $moduleType != "contract" )
  {
    /* CAREER GOALS */
    $pdf->TableHeading('Career Goals');
    $headers = array( "Name", "Status", "Comments" );
    $widths = array( 200, 150, 430 );
    $rows = array();

    foreach( $careerGoals as $careerGoal )
    {
      $careerGoalEvaluation = new CareerGoalEvaluation();
      $careerGoalEvaluation->loadFor( $moduleType, $careerGoal->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

      $rows[] = array( $careerGoal->getName(),
                       $careerGoalEvaluation->getCompletedDisplay(),
                       $careerGoalEvaluation->getComments() );
    }

    $pdf->listTable( $headers, $rows, $widths, "L", -1, "No Career Goals" );
  }

  $pdf->AddPage();

  /* KPAS */
  $allRated = true;
  $totalMarks = 0;
  $totalPercentage = 0;
  $kpaCount = 0;

  if( $moduleType == "career")
  {
    $pdf->TableHeading('KPIs linked to job level');
    $headers = array( "KPI", "Description", "Measurement", "Rating", "Self Evaluation", "Comments", "Outcome", "Portfolio of evidence" );
    $widths = array( 70, 125, 125, 80, 80, 100, 100, 100 );
    $rows = array();

    foreach( $jobLevelKPAs as $kpa )
    {
      $kpaEvaluation = new KPAEvaluation();
      $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

      if( $kpaEvaluation->isTransient() )
      {
        $allRated = false;
      }

      $totalMarks += $kpaEvaluation->getRating();
      $kpaCount += 1;

      $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence() );
    }

    $pdf->listTable( $headers, $rows, $widths );

    $pdf->TableHeading('KPIs unique to this job');
    $headers = array( "KPI", "Description", "Measurement", "Rating", "Self Evaluation", "Comments", "Outcome", "Portfolio of evidence" );
    $widths = array( 70, 125, 125, 80, 80, 100, 100, 100 );
    $rows = array();

    foreach( $uniqueKPAs as $kpa )
    {
      $kpaEvaluation = new KPAEvaluation();
      $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

      if( $kpaEvaluation->isTransient() )
      {
        $allRated = false;
      }

      $totalMarks += $kpaEvaluation->getRating();
      $kpaCount += 1;

      $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence());
    }

    $pdf->listTable( $headers, $rows, $widths );
  }

  if( $moduleType != "contract")
  {
    $pdf->TableHeading('KPIs linked to career goals');
    $headers = $useWeights ? array( "KPI", "Description", "Measurement", "Rating", "Self Evaluation", "Weight", "Percentage", "Comments", "Outcome", "Portfolio of evidence") : array( "KPI", "Description", "Measurement", "Rating", "Self Evaluation", "Comments", "Outcome", "Portfolio of evidence");
    $widths = $useWeights ? array( 80, 120, 120, 55, 55, 55, 55, 80, 80, 80 ) : array( 70, 125, 125, 80, 80, 100, 100, 100 );
    $rows = array();

    foreach( $careerKPAs as $kpa )
    {
      $kpaEvaluation = new KPAEvaluation();
      $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

      if( $kpaEvaluation->isTransient() )
      {
        $allRated = false;
      }

      if( $useWeights )
      {
        $percentage = ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );
        $totalPercentage += $percentage;
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpa->getWeight(), $percentage."%", $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence());
      }
      else
      {
        $totalMarks += $kpaEvaluation->getRating();
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence());
      }

      $kpaCount += 1;
    }

    $pdf->listTable( $headers, $rows, $widths );

    $hasValidBonus = false;

    if( count( $bonusKPAs ) > 0 )
    {
      $rows = array();

      foreach( $bonusKPAs as $kpa )
      {
        $kpaEvaluation = new KPAEvaluation();
        $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

        if( $kpaEvaluation->getRating() == 0 )
        {
          continue;
        }

        if( $kpaEvaluation->isTransient() )
        {
          $allRated = false;
        }

        if( $useWeights )
        {
          $percentage = ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );
          $totalPercentage += $percentage;
          $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpa->getWeight(), $percentage."%", $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence());
        }
        else
        {
          $totalMarks += $kpaEvaluation->getRating();
          $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence());
        }

        $kpaCount += 1;
      }

      if( count( $rows ) > 0 )
      {
        $pdf->TableHeading('Additional work KPI');
        $headers = $useWeights ? array( "KPI", "Description", "Measurement", "Rating", "Self Evaluation", "Weight", "Percentage", "Comments", "Outcome", "Portfolio of evidence") : array( "KPI", "Description", "Measurement", "Rating", "Self Evaluation", "Comments", "Outcome", "Portfolio of evidence");
        $widths = $useWeights ? array( 80, 120, 120, 55, 55, 55, 55, 80, 80, 80 ) : array( 70, 125, 125, 80, 80, 100, 100, 100 );
        $pdf->listTable( $headers, $rows, $widths );
      }
    }
  }

  if( $moduleType == "perform" || $moduleType == "contract")
  {
    $pdf->TableHeading('Organisational KPIs');

    if( $useWeights )
    {
      $headers = array( "Group Num", "SDBIP KPI Num", "KPA", "Objective", "KPI", "Unit of Measurement", "Target Unit", "Target", "Rating", "Self Rating", "Weight", "%", "Comments", "Outcome", "Portfolio of evidence" );
      $widths = array( 30, 30, 55, 75, 75, 75, 60, 35, 30, 30, 30, 30, 75, 75, 75 );
    }
    else
    {
      $headers = array( "Group Num", "SDBIP KPI Num", "KPA", "Objective", "KPI", "Unit of Measurement", "Target Unit", "Target", "Rating", "Self Rating", "Comments", "Outcome", "Portfolio of evidence" );
      $widths = array( 35, 35, 60, 80, 80, 80, 80, 40, 40, 40, 70, 70, 70 );
    }

    $rows = array();

    $lastGroupNum = -1;

    foreach( $orgKPAs as $kpa )
    {
      $kpaEvaluation = new KPAEvaluation();
      $kpaEvaluation->loadForOrg( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

      if( $kpaEvaluation->isTransient() )
      {
        $allRated = false;
      }

      $rating = $kpaEvaluation->getRating();
      $selfRating = $kpaEvaluation->getSelfEvaluation();

      if( $kpa->getGroupNum() == $lastGroupNum )
      {
        $rating = "";
        $selfRating = "";
      }

      if( $useWeights )
      {
        $percentage = ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );
        $totalPercentage += $percentage;
        $rows[] = array( $kpa->getGroupNum(), $kpa->getSDBIPNum(), $kpa->getKPI(), $kpa->getObjective(), $kpa->getName(), $kpa->getMeasurement(), $kpa->getTargetUnit(), $kpa->getTargetForPeriod( $selectedPeriod ), $rating, $selfRating, $kpa->getWeight(), $percentage."%", $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $totalMarks += $kpaEvaluation->getRating();
        $rows[] = array( $kpa->getGroupNum(), $kpa->getSDBIPNum(), $kpa->getKPI(), $kpa->getObjective(), $kpa->getName(), $kpa->getMeasurement(), $kpa->getTargetUnit(), $kpa->getTargetForPeriod( $selectedPeriod ), $rating, $selfRating, $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence() );
      }

      $kpaCount += 1;
      $lastGroupNum = $kpa->getGroupNum();
    }

    $pdf->listTable( $headers, $rows, $widths );

    $pdf->TableHeading('Managerial KPIs');

    if( $useWeights )
    {
      $headers = array( "Core Skill", "Skill Level", "Measurement", "Rating", "Self Evaluation", "Weight", "Percentage", "Comments", "Outcome", "Portfolio of evidence" );
      $widths = array( 100, 60, 80, 70, 70, 50, 50, 100, 100, 100 );
    }
    else
    {
      $headers = array( "Core Skill", "Skill Level", "Measurement", "Rating", "Self Evaluation", "Comments", "Outcome", "Portfolio of evidence" );
      $widths = array( 100, 60, 120, 70, 70, 120, 120, 120 );
    }

    $rows = array();

    foreach( $manKPAs as $kpa )
    {
      $kpaEvaluation = new KPAEvaluation();
      $kpaEvaluation->loadForMan( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

      if( $kpaEvaluation->isTransient() )
      {
        $allRated = false;
      }

      if( $useWeights )
      {
        $percentage = ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );
        $totalPercentage += $percentage;
        $rows[] = array( $kpa->getName(), $kpa->getSkillLevel(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpa->getWeight(), $percentage."%", $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $totalMarks += $kpaEvaluation->getRating();
        $rows[] = array( $kpa->getName(), $kpa->getSkillLevel(), $kpa->getMeasurement(), $kpaEvaluation->getRating(), $kpaEvaluation->getSelfEvaluation(), $kpaEvaluation->getComments(), $kpa->getOutcome(), $kpa->getEvidence() );
      }

      $kpaCount += 1;
    }

    $pdf->listTable( $headers, $rows, $widths );
  }

  $pdf->SetFont('Arial','B',11);

  $pdf->Cell(130,21,"Final Evaluation Mark:",0,0);

  if( $allRated && $kpaCount > 0 )
  {
    if( $useWeights )
    {
      $pdf->Cell(100,21, $totalPercentage . "% - " . KPAEvaluation::bellCurveSymbolFromPercentage($totalPercentage ),0,0);
    }
    else
    {
      $pdf->Cell(100,21,number_format(round( ($totalMarks / $kpaCount), 1 ), 1, '.', '') . " - " . KPAEvaluation::bellCurveSymbol( $totalMarks / $kpaCount ),0,0);
    }
  }

  $pdf->AddPage();

  /* STRONG AREAS */
  $pdf->TableHeading('Strengths');
  $headers = array( "Message" );
  $widths = array( 780 );
  $rows = array();

  foreach( $strongSkills as $skill )
  {
    $rows[] = array( $skill->getMessage() );
  }

  for($i=1; $i <= 5 - count( $strongSkills ); $i++)
  {
    $rows[] = array( "" );
  }

  $pdf->listTable( $headers, $rows, $widths, "L", -1, "No Strengths" );

  /* DEVELOPMENT AREAS */
  $pdf->TableHeading('Development Areas');
  $headers = array( "Message" );
  $widths = array( 780 );
  $rows = array();

  foreach( $developmentSkills as $skill )
  {
    $rows[] = array( $skill->getMessage() );
  }

  for($i=1; $i <= 5 - count( $developmentSkills ); $i++)
  {
    $rows[] = array( "" );
  }

  $pdf->listTable( $headers, $rows, $widths, "L", -1, "No Development Areas" );

  /* COMMENTS */
  $pdf->TableHeading('Comments');
  $headers = array( "Message" );
  $widths = array( 780 );
  $rows = array();

  foreach( $comments as $comment )
  {
    $rows[] = array( $comment->getMessage() );
  }

  for($i=1; $i <= 5 - count( $comments ); $i++)
  {
    $rows[] = array( "" );
  }

  $pdf->listTable( $headers, $rows, $widths, "L", -1, "No Comments" );

  $pdf->AddPage();
  $pdf->Ln(50);
  $pdf->SetFont('Arial','',9);
  $pdf->Cell(300,21,"Signed and accepted by the Employee",'T',1);
  $pdf->Ln(20);
  $pdf->Cell(300,21,"Date",'T',1);
  $pdf->Ln(50);
  $pdf->Cell(300,21,"Signed and accepted by the Manager",'T',1);
  $pdf->Ln(20);
  $pdf->Cell(300,21,"Date",'T',1);

  $pdf->Output( $moduleType. '_eval_' . str_replace(" ", "_", strtolower($selectedUser->getDecodedFullName())) . '.pdf','I');

  exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName ?> :: Evaluation :: Print Preview</h1>

    <div class="print-preview">
    <h1><?=$moduleName?> Evaluation</h1>
    <h2>Personal Information</h2>
    <table class="print" cellspacing="0">
      <tr>
        <th width="20%">Name:</th>
        <td width="80%"><?= $selectedUser->getDecodedFullName() ?></td>
      </tr>
      <?php /*
      <tr>
        <th>Race:</th>
        <td><?= $selectedUser->getRace() ?></td>
      </tr>
      <tr>
        <th>Gender:</th>
        <td><?= $selectedUser->getGender() ?></td>
      </tr>
      */?>
      <tr>
        <th>Department:</th>
        <td><?= $selectedUser->getDepartment() ?></td>
      </tr>
      <tr>
        <th>Section:</th>
        <td><?= $selectedUser->getSection() ?></td>
      </tr>
      <tr>
        <th>Job title:</th>
        <td><?= $selectedUser->getJobTitle() ?></td>
      </tr>
      <tr>
        <th>Job level:</th>
        <td><?= $selectedUser->getJobLevel() ?></td>
      </tr>
      <tr>
        <th>Job number:</th>
        <td><?= $selectedUser->getJobNumber() ?></td>
      </tr>
      <tr>
        <th>Manager:</th>
        <td><?= $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ?></td>
      </tr>
      <tr>
        <th>Performance year:</th>
        <td><?= $selectedYear->getName() ?></td>
      </tr>
      <tr>
        <th>Period:</th>
        <td><?= getPeriodStartDate( getCareerCategory()->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?> - <?= getPeriodEndDate( getCareerCategory()->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></td>
      </tr>
    </table>

    <h2>Career Goals</h2>
    <table class="print" cellspacing="0">
      <tr>
        <th width="20%">Name</th>
        <th width="10%">Completed</th>
        <th width="55%">Comments</th>
        <th width="15%">Evaluated By</th>
      </tr>
    <?
      foreach( $careerGoals as $careerGoal )
      {
        $careerGoalEvaluation = new CareerGoalEvaluation();
        $careerGoalEvaluation->loadFor( $moduleType, $careerGoal->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
    ?>
    <tr>
      <td><?= $careerGoal->getName() ?></td>
      <td><?= $careerGoalEvaluation->isTransient() ? "Not Set" : $careerGoalEvaluation->getCompletedDisplay() ?></td>
      <td><?= $careerGoalEvaluation->isTransient() ? "Not Set" : $careerGoalEvaluation->getComments() ?></td>
      <td><?= $careerGoalEvaluation->isTransient() ? "Not Set" : $careerGoalEvaluation->getEvaluatedBy()->getDecodedFullName() ?></td>
    </tr>
    <?
      }
    ?>
    </table>

    <h2>Key Performance Areas</h2>

    <?
      $allRated = true;
      $totalMarks = 0;
      $kpaCount = 0;
    ?>

    <? if( $moduleType == "career") { ?>
    <h3>Key Performance Areas linked to Job Level</h3>
    <table class="print" cellspacing="0">
      <tr>
        <th width="15%">Name</th>
        <th width="30%">Measurement</th>
        <th width="8%">Rating</th>
				<th width="8%">Self Evaluation</th>
        <th width="24%">Comments</th>
        <th width="15%">Evaluated By</th>
      </tr>
    <?
      if( count($jobLevelKPAs) > 0 )
      {
        foreach( $jobLevelKPAs as $kpa )
        {
          $kpaEvaluation = new KPAEvaluation();
          $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

          if( $kpaEvaluation->isTransient() )
          {
            $allRated = false;
          }

          $totalMarks += $kpaEvaluation->getRating();
          $kpaCount += 1;
    ?>
      <tr>
        <td><?= $kpa->getName() ?></td>
        <td><?= $kpa->getMeasurement() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getRating() ?></td>
				<td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getSelfEvaluation() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getComments() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedBy()->getDecodedFullName() ?></td>
      </tr>
    <?
        }
      }
      else
      {
    ?>
      <tr><td colspan="5">No Key Performance Areas linked to Job Level</td></tr>
    <?
      }
    ?>
    </table>

    <h3>Key Performance Areas unique to this job</h3>
    <table class="print" cellspacing="0">
      <tr>
        <th width="15%">Name</th>
        <th width="30%">Measurement</th>
        <th width="8%">Rating</th>
        <th width="8%">Self Evaluation</th>
        <th width="24%">Comments</th>
        <th width="15%">Evaluated By</th>
      </tr>
    <?
      if( count($uniqueKPAs) > 0 )
      {
        foreach( $uniqueKPAs as $kpa )
        {
          $kpaEvaluation = new KPAEvaluation();
          $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

          if( $kpaEvaluation->isTransient() )
          {
            $allRated = false;
          }

          $totalMarks += $kpaEvaluation->getRating();
          $kpaCount += 1;
    ?>
      <tr>
        <td><?= $kpa->getName() ?></td>
        <td><?= $kpa->getMeasurement() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getRating() ?></td>
				<td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getSelfEvaluation() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getComments() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedBy()->getFullName() ?></td>
      </tr>
    <?
        }
      }
      else
      {
    ?>
      <tr><td colspan="5">No Key Performance Areas unique to this job</td></tr>
    <?
      }
    ?>
    </table>

    <? } ?>

    <h3>Key Performance Areas linked to career goals</h3>
    <table class="print" cellspacing="0">
      <tr>
        <th width="15%">Name</th>
        <th width="30%">Measurement</th>
        <th width="8%">Rating</th>
        <th width="8%">Self Evaluation</th>
        <th width="24%">Comments</th>
        <th width="15%">Evaluated By</th>
      </tr>
    <?
      if( count($careerKPAs) > 0 )
      {
        foreach( $careerKPAs as $kpa )
        {
          $kpaEvaluation = new KPAEvaluation();
          $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

          if( $kpaEvaluation->isTransient() )
          {
            $allRated = false;
          }

          $totalMarks += $kpaEvaluation->getRating();
          $kpaCount += 1;
    ?>
      <tr>
        <td><?= $kpa->getName() ?></td>
        <td><?= $kpa->getMeasurement() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getRating() ?></td>
				<td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getSelfEvaluation() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getComments() ?></td>
        <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedBy()->getDecodedFullName() ?></td>
      </tr>
    <?
        }
      }
      else
      {
    ?>
      <tr><td colspan="5">No Key Performance Areas linked to career goals</td></tr>
    <?
      }
    ?>
    </table>

    <? if( $moduleType == "perform") { ?>

      <h3>Organisational Key Performance Areas</h3>
      <table class="print" cellspacing="0">
        <tr>
        <th width="15%">Name</th>
        <th width="30%">Measurement</th>
        <th width="8%">Rating</th>
        <th width="8%">Self Evaluation</th>
        <th width="24%">Comments</th>
        <th width="15%">Evaluated By</th>
        </tr>
      <?
        if( count($orgKPAs) > 0 )
        {
          foreach( $orgKPAs as $kpa )
          {
            $kpaEvaluation = new KPAEvaluation();
            $kpaEvaluation->loadForOrg( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

            if( $kpaEvaluation->isTransient() )
            {
              $allRated = false;
            }

            $totalMarks += $kpaEvaluation->getRating();
            $kpaCount += 1;
      ?>
        <tr>
          <td><?= $kpa->getName() ?></td>
          <td><?= $kpa->getMeasurement() ?></td>
          <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getRating() ?></td>
					<td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getSelfEvaluation() ?></td>
          <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getComments() ?></td>
          <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedBy()->getDecodedFullName() ?></td>
        </tr>
      <?
          }
        }
        else
        {
      ?>
        <tr><td colspan="5">No Organisational Key Performance Areas</td></tr>
      <?
        }
      ?>
      </table>

      <h3>Managerial Key Performance Areas</h3>
      <table class="print" cellspacing="0">
        <tr>
          <th width="15%">Name</th>
	        <th width="30%">Measurement</th>
	        <th width="8%">Rating</th>
	        <th width="8%">Self Evaluation</th>
	        <th width="24%">Comments</th>
	        <th width="15%">Evaluated By</th>
        </tr>
      <?
        if( count($manKPAs) > 0 )
        {
          foreach( $manKPAs as $kpa )
          {
            $kpaEvaluation = new KPAEvaluation();
            $kpaEvaluation->loadForMan( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

            if( $kpaEvaluation->isTransient() )
            {
              $allRated = false;
            }

            $totalMarks += $kpaEvaluation->getRating();
            $kpaCount += 1;
      ?>
        <tr>
          <td><?= $kpa->getName() ?></td>
          <td><?= $kpa->getMeasurement() ?></td>
          <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getRating() ?></td>
					<td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getSelfEvaluation() ?></td>
          <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getComments() ?></td>
          <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedBy()->getDecodedFullName() ?></td>
        </tr>
      <?
          }
        }
        else
        {
      ?>
        <tr><td colspan="5">No Managerial Key Performance Areas</td></tr>
      <?
        }
      ?>
      </table>
    <? } ?>

    <h2>Final Evaluation Mark: <? if( $allRated && $kpaCount > 0 ){?> <?= number_format(round( ($totalMarks / $kpaCount), 1 ), 1, '.', '') ?> - <?= KPAEvaluation::bellCurveSymbol( $totalMarks / $kpaCount ) ?> <? }else{ ?> ? <? } ?></h2>

    <h2>Strengths</h2>
    <table class="print" cellspacing="0">
      <tr>
        <th width="85%">Message</th>
        <th width="15%">Created by</th>
      </tr>
      <?
        foreach( $strongSkills as $skill ) {
      ?>
      <tr>
        <td><?= $skill->getMessage() ?></td>
        <td><?= $skill->getCreatedBy()->getDecodedFullName() ?></td>
      </tr>
      <?
        }

        for($i=1; $i <= $maxStrongAreas - count($strongSkills); $i++) {
      ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <? } ?>
    </table>

    <h2>Development Areas</h2>
    <table class="print" cellspacing="0">
      <tr>
        <th width="85%">Message</th>
        <th width="15%">Created by</th>
      </tr>
      <?
        foreach( $developmentSkills as $skill ) {
      ?>
      <tr>
        <td><?= $skill->getMessage() ?></td>
        <td><?= $skill->getCreatedBy()->getFullName() ?></td>
      </tr>
      <?
        }

        for($i=1; $i <= $maxDevelopmentAreas - count($developmentSkills); $i++) {
      ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <? } ?>
    </table>

    <h2>Comments</h2>
    <table class="print" cellspacing="0">
      <tr>
        <th width="85%">Message</th>
        <th width="15%">Created by</th>
      </tr>
    <?
      foreach( $comments as $comment ) {
    ?>
    <tr>
      <td><?= $comment->getMessage() ?></td>
      <td><?= $comment->getCreatedBy()->getDecodedFullName() ?></td>
    </tr>
    <?
      }
      for($i=1; $i <= $maxComments - count($comments); $i++) {
    ?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <? } ?>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
		<p>___________________________________</p>
    <p>Signed and accepted by the Employee</p>
    <br/>
		<p>Date: ___________________________________</p>
    <br/>
    <br/>
    <br/>
		<p>___________________________________</p>
    <p>Signed and accepted by the Manager</p>
    <br/>
		<p>Date: ___________________________________</p>
		</div>
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_evaluate_period.php');" />
    </div>
  </body>
</html>

