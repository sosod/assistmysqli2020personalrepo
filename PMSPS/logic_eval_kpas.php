<?php
/* Ensure that the user has a 'bonus' KPA record
 * to allow them to add additional marks when evaluating a user */
$bonusKPA = getBonusKPA( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

if( isset( $bonusKPA ) == false )
{
  createBonusKPA( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
}


$kpas = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

$kpasWithEval = array();

foreach( $kpas as $kpa )
{
  $kpaEvaluation = new KPAEvaluation();
  $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

  if( $kpa->getKPAType() == "bonus" && $kpaEvaluation->getRating() == 0 )
  {
    continue;
  }

  if( $kpaEvaluation->isTransient() )
  {
    $allEvaluated = false;
  }

  if( $useWeights )
  {
    $totalPercentage += ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );
  }
  else
  {
    $totalMarks += $kpaEvaluation->getRating();
  }

  $kpasWithEval[] = array( $kpa, $kpaEvaluation );
}

$numOfKPAs = $numOfKPAs + count( $kpasWithEval );
?>
