<?php
include("report_inc.php");

$errors = array();

$process = $_REQUEST["process"];

if( isset( $selectedYear ) && isset( $process ) )
{

    $orderBy = $_REQUEST["orderBy"];
    if( exists( $orderBy ) == false )
    {
        $orderBy = "tkname";
    }
    $orderByDir = $_REQUEST["orderByDir"];
    if( exists( $orderByDir ) == false )
    {
        $orderByDir = "ASC";
    }

    $departmentId = $_REQUEST["departmentId"];
    $section = $_REQUEST["section"];
    $managerId = $_REQUEST["managerId"];
    $jobLevel = $_REQUEST["jobLevel"];


    /* BUILD UP THE SQL QUERY */
    $sql = " SELECT u.tkid, u.tkmanager, u.tkname, u.tksurname, us.section, us.job_level, d.value AS department, ".
        " (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_timekeep um WHERE um.tkid = u.tkmanager ) AS manager, ";

    if( $moduleType == "career" )
    {
        $sql .= " (SELECT IFNULL( (SUM(ke.rating) / (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa k, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_id = k.id AND k.user_id = u.tkid AND k.eval_year_id = ".$selectedYear->getId()." AND k.type = '".$moduleType."')), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) AS avg_score ";
    }
    else if( $moduleType == "perform" )
    {
        if( $useWeights )
        {
            $sql .= " (SELECT IFNULL( sum(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa k WHERE ke.user_id = u.tkid AND ke.kpa_id = k.id AND k.kpa_type='career' AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) + (SELECT IFNULL( sum(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa k WHERE ke.user_id = u.tkid AND ke.kpa_id = k.id AND k.kpa_type='bonus' AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) + (SELECT IFNULL( sum(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_managerial k WHERE ke.user_id = u.tkid AND ke.kpa_man_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) + ( SELECT IFNULL( sum(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_org k WHERE ke.user_id = u.tkid AND ke.kpa_org_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) AS total_percentage ";
        }
        else
        {
            $sql .= " (SELECT IFNULL( (SUM(ke.rating) / ( (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa k ,assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_id = k.id AND k.user_id = u.tkid AND k.eval_year_id = ".$selectedYear->getId()." AND k.type = '".$moduleType."')  + (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_managerial km, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_man_id = km.id AND km.user_id = u.tkid AND km.eval_year_id = ".$selectedYear->getId()." AND km.type = '".$moduleType."') + (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_org ko, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_org_id = ko.id AND ko.user_id = u.tkid AND ko.eval_year_id = ".$selectedYear->getId()." AND ko.type = '".$moduleType."') ) ), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) AS avg_score ";
        }
    }
    else if( $moduleType == "contract" )
    {
        if( $useWeights )
        {
            $sql .= " (SELECT IFNULL( sum(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_managerial k WHERE ke.user_id = u.tkid AND ke.kpa_man_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) + ( SELECT IFNULL( sum(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_org k WHERE ke.user_id = u.tkid AND ke.kpa_org_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) AS total_percentage ";
        }
        else
        {
            $sql .= " (SELECT IFNULL( (SUM(ke.rating) / ( (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_managerial km, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_man_id = km.id AND km.user_id = u.tkid AND km.eval_year_id = ".$selectedYear->getId()." AND km.type = '".$moduleType."') + (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_org ko, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_org_id = ko.id AND ko.user_id = u.tkid AND ko.eval_year_id = ".$selectedYear->getId()." AND ko.type = '".$moduleType."') ) ), 0) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.type = '".$moduleType."' ) AS avg_score ";
        }
    }

    $sql .= " FROM assist_".getCompanyCode()."_timekeep u, " .
        " assist_".getCompanyCode()."_pmsps_user_setup us, " .
        " assist_".getCompanyCode()."_list_dept d " .
        " WHERE us.user_id = u.tkid " .
        " AND us.category_id =  " . getTypeCategory( $moduleType )->getId() .
        " AND d.id = u.tkdept ";

    if( exists( $departmentId ) )
    {
        $sql .= " AND u.tkdept = " . $departmentId;
    }

    if( exists( $section ) )
    {
        $sql .= " AND us.section = '" . $section . "' ";
    }

    if( exists( $managerId ) )
    {
        $sql .= " AND u.tkmanager = '" . $managerId . "'";
    }

    if( exists( $jobLevel ) )
    {
        $sql .= " AND us.job_level = " . $jobLevel;
    }

    $sql .= " ORDER BY " . $orderBy . " " . $orderByDir;

    debug( $sql );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <? include("inc_head.php"); ?>
</head>
<body style="margin:0 5px 0 5px;">
<h1><?=$moduleName?> :: Reporting :: Final Score Report</h1>

<form action="<?=$moduleType?>_report_finalscore.php" id="frmUserSelect" method="get" >
    <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
        <tr>
            <td valign="middle">
                <strong>Evaluation year:</strong> <?= select( "selectedYearId", getEvalYearSelectList(), ( isset( $selectedYear ) ? $selectedYear->getId() : "" ), true ) ?>
            </td>
            <td valign="middle"><input type="submit" value="Select" /></td>
        </tr>
    </table>
    <input type="hidden" name="action" value="year_period_select" />
</form>
<hr/>

<?
if( isset( $selectedYear ) )
{
    ?>

    <h5>Report Filter:</h5>

    <form action="<?=$moduleType?>_report_finalscore.php" id="frmFilter" method="get" class="filter" style="width: 750px">
        <div>
            <label>Department</label>
            <?= select( "departmentId", getDepartmentSelectList(), $departmentId, false, false, "All" ) ?>
            <label>Section</label>
            <?= select( "section", getUserSectionSelectList(), $section, false, false, "All" ) ?>
        </div>
        <div>
            <label>Manager</label>
            <?= select( "managerId", getManagerSelectList(), $managerId, false, false, "All" ) ?>
            <label>Job level</label>
            <?= select( "jobLevel", getNumericSelectList(1,20), $jobLevel, false, false, "All" ) ?>
        </div>
        <div>
            <label>Order by</label>
            <?= select( "orderBy", getFishbowlOrderBySelectList(), $orderBy, false, false, false  ) ?>
            <?= select( "orderByDir", getOrderByDirectionSelectList(), $orderByDir, false, false, false ) ?>
            <input type="submit" value="View Report" />
        </div>
        <input type="hidden" name="process" value="true" />
    </form>

<?
    if(isset($process)) {
?>

    <table class="list full" cellspacing="0" style="margin-top:15px;">
        <tr>
            <th width="10%">User</th>
            <th width="5%">Job level</th>
            <th width="10%">Department</th>
            <th width="10%">Section</th>
            <th width="10%">Manager</th>
            <th width="5%"># Evaluations</th>
            <? if ($useWeights) { ?>
                <th width="7%">Total Percentage</th>
            <? } else { ?>
                <th width="7%">Avg Score</th>
            <? } ?>
            <th width="6%">Symbol</th>
        </tr>
        <?
        $data = getListRecords($sql);

        if (count($data) > 0) {
            foreach ($data as $record) {
                $num_completed_evaluations = getNumCompletedEvaluations($moduleType, $record["tkid"], $selectedYearId);
        ?>
        <tr data-tkid"=<?= $record["tkid"] ?>">
            <td><?= $record["tkname"] ?> <?= $record["tksurname"] ?></td>
            <td align="right"><?= $record["job_level"] ?></td>
            <td><?= $record["department"] ?></td>
            <td><?= $record["section"] ?></td>
            <td><?= $record["tkmanager"] == "S" ? "Self" : $record["manager"] ?></td>
            <td><?= $num_completed_evaluations ?></td>
            <? if ($useWeights) { ?>
            <td align="right"><?= exists($record["total_percentage"]) ? number_format(($record["total_percentage"] / $num_completed_evaluations), 1, '.', '') . "%" : "?" ?></td>
            <td align="right"><?= exists($record["total_percentage"]) ? KPAEvaluation::bellCurveSymbolFromPercentage($record["total_percentage"] / $num_completed_evaluations) : "?" ?></td>
            <? } else { ?>
            <td align="right"><?= exists($record["avg_score"]) ? number_format(round(($record["avg_score"]), 1), 1, '.', '') : "?" ?></td>
            <td align="right"><?= exists($record["avg_score"]) ? KPAEvaluation::bellCurveSymbol(($record["avg_score"])) : "?" ?></td>
            <? } ?>
      </tr>
      <?
          }
        } else {
            ?>
            <tr>
                <td colspan="8">No record match the selected filter options.</td>
            </tr>
        <?
        }
        ?>
    </table>
<?
    }else {
?>
    <p class="instruction" style="margin-top: 20px;">Please specify your report criteria in the filter above.</p>
<?
    }

}
else
{
    ?>
    <p class="instruction">Please select an evaluation year above to continue.</p>
<?
}
?>
</body>
</html>