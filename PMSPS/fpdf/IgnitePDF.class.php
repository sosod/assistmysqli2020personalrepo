<?php
require('fpdf/fpdf.php');
class IgnitePDF extends FPDF
{
  function Header()
  {
      //Select Arial bold 15
      $this->SetFont('Arial','',6);
      $this->Cell(0,10,'Private and Confidential',0,0,'C');
      //Line break
      $this->Ln(20);
  }

  function Footer()
  {
    //Go to 1.5 cm from bottom
    $this->SetY(-15);
    //Select Arial italic 8
    $this->SetFont('Arial','',6);
    //Print centered page number
    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
  }

  function printRow($data, $widths, $isTextList=false, $align='C', $fill = false )
  {
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
    {
      $nb=max($nb,$this->NbLines($widths[$i],$data[$i]));
    }
    $h=15*$nb;

    //Issue a page break first if needed
    $this->CheckPageBreak($h);

    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
      $w=$widths[$i];

      //Save the current position
      $x=$this->GetX();
      $y=$this->GetY();

      //Draw the border
      if( $isTextList == false )
      {
        $this->Rect($x,$y,$w,$h, $fill ? "DF" : "D");
      }

      //Print the text
      $align = $isTextList ? 'L' : $align;

      $this->MultiCell($w,15,$data[$i],0,$align );

      //Put the position to the right of the cell
      $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
  }


  function CheckPageBreak($h)
  {
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
    {
      $this->AddPage($this->CurOrientation);
    }
  }

  function NbLines($w,$txt)
  {
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
    {
      $w=$this->w-$this->rMargin-$this->x;
    }

    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);

    if($nb>0 and $s[$nb-1]=="\n")
    {
      $nb--;
    }

    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;

    while($i<$nb)
    {
      $c=$s[$i];
      if($c=="\n")
      {
        $i++;
        $sep=-1;
        $j=$i;
        $l=0;
        $nl++;
        continue;
      }

      if($c==' ')
      {
        $sep=$i;
      }

      $l+=$cw[$c];
      if($l>$wmax)
      {
        if($sep==-1)
        {
          if($i==$j)
          {
            $i++;
          }
        }
        else
        {
          $i=$sep+1;
        }

        $sep=-1;
        $j=$i;
        $l=0;
        $nl++;
      }
      else
      {
        $i++;
      }
    }

    return $nl;
  }

  function listTable( $headers, $rows, $widths, $align = 'L', $indent = -1, $empty=null )
  {
    if( $indent != -1 )
    {
      $this->SetX( $indent );
    }

    $this->SetFont('Arial','',7);
    $this->SetFillColor(222,222,222);
    $this->SetTextColor(0,0,0);
    //Headers
    $this->printRow( $headers, $widths, false, 'C', true );

    if( $indent != -1 )
    {
      $this->SetX( $indent );
    }
    $this->SetFont('Arial','',6);
    $this->SetTextColor(0,0,0);

    foreach( $rows as $row )
    {
      $this->printRow( $row, $widths, false, $align );
      if( $indent != -1 )
      {
        $this->SetX( $indent );
      }
    }

    if( count($rows) == 0 && $empty != null )
    {
      $this->printRow( array( $empty ), array( 780 ), false, $align );
    }

    $this->Cell( array_sum($widths), 10, '', 'T' );

    $this->Ln();
  }

  function infoTable( $rows, $widths )
  {
    for( $i = 0; $i < count( $rows ); $i++ )
    {
      $this->SetFont('Arial','B',7);
      $this->SetFillColor(222,222,222);
      $this->SetTextColor(0,0,0);
      $this->Cell( $widths[0], 15, $rows[ $i ][0], 1, 0, 'R', true );

      $this->SetFont('Arial','',6);
      $this->Cell( $widths[1], 15, $rows[ $i ][1], 1, 0 );
      $this->Ln();
    }

    //Closure line
    $this->Cell( array_sum($widths), 10, '', 'T' );
    $this->Ln();
  }

  function textList( $rows )
  {
    $widths_3col = array(20,30,450);
    $widths_4col = array(20,30,40,440);

    $this->SetFont('Arial','',10);
    $this->SetTextColor(0,0,0);

    foreach( $rows as $row )
    {
      $widths = $widths_3col;

      if( count( $row ) == 4 )
      {
        $widths = $widths_4col;
      }

      $this->printRow( $row, $widths, true );
    }

    //$this->Cell( array_sum($widths), 10, '', 'T' );
    $this->Ln();
  }

  function Heading( $text, $fontSize, $textAlign, $fontStyle, $spacing )
  {
    $this->SetFont('Arial',$fontStyle,$fontSize);
    $this->MultiCell(0,$fontSize+$spacing,$text,0,$textAlign);
    $this->Ln();
  }

  function H0( $text, $align = 'L', $fontStyle = 'B', $spacing = 6 )
  {
    $this->Heading( $text, 20, $align, $fontStyle, $spacing );
  }

  function H1( $text, $align = 'L', $fontStyle = 'B', $spacing = 6 )
  {
    $this->Heading( $text, 16, $align, $fontStyle, $spacing );
  }

  function H2( $text, $align = 'L', $fontStyle = 'B', $spacing = 6 )
  {
    $this->Heading( $text, 13, $align, $fontStyle, $spacing );
  }

  function H3( $text, $align = 'L', $fontStyle = 'B', $spacing = 6 )
  {
    $this->Heading( $text, 11, $align, $fontStyle, $spacing );
  }

  function TableHeading( $text )
  {
    $this->Ln();
    $this->SetFont('Arial','B',12);
    $this->MultiCell(0,14,$text,0,'L');
  }

  function TextLine( $text, $indent = 0, $fontSize = 10, $textAlign = "L", $fontStyle="" )
  {
    $this->SetX( $indent );
    $this->SetFont('Arial',$fontStyle,$fontSize);
    $this->MultiCell(0,$fontSize+6,$text,0,$textAlign);
  }

  function InlineText( $text, $width, $height, $align="L", $style="", $size=12 )
  {
    $this->SetFont('Arial',$style,$size);
    $this->Cell($width, $height, $text, 0, 0, $align, false, '');
  }
}
?>
