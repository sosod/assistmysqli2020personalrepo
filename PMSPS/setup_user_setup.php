<?php
include("inc_ignite.php");


$userId = $_REQUEST["userId"];
$action = $_REQUEST["action"];

$userSetup = new UserSetup();

if( $action == "save" )
{
  $userSetup->loadFromRequest( $_REQUEST );
  $userSetup->save();

  redirect("setup_user_setup_info.php?userId=" . $userId );
}

if( exists( $userId ) )
{
  $userSetup->loadFromUserId( $userId );
  if( $userSetup->isTransient() )
  {
    //set the user id so that the lazy loading getUser() function can get the user info
    $userSetup->setUserId( $userId );  
  }
}

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmUserSetup").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - User Configuration</h1>
    
    <form id="frmUserSetup" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>User</th>
          <td><?= $userSetup->getUser()->getFullName() ?></td>
        </tr>
        <tr>
          <th>Category</th>
          <td><?= select( "categoryId", getCategorySelectList(), $userSetup->getCategoryId(), true ) ?></td>
        </tr>
        <tr>
          <th>May Create</th>
          <td><?= select( "createPermission", getYesNoSelectList(), $userSetup->getCreatePermission(), true ) ?></td>
        </tr>
				<? /*
        <tr>
          <th>May Approve</th>
          <td><?= select( "approvePermission", getYesNoSelectList(), $userSetup->getApprovePermission(), true ) ?></td>
        </tr>
				*/ ?>
        <tr>
          <th>HR Manager</th>
          <td><?= select( "hrManager", getYesNoSelectList(), $userSetup->getHRManager(), true ) ?></td>
        </tr>
        <tr>
          <th>Section</th>
          <td><input type="text" name="section" value="<?= $userSetup->getSection() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Job level</th>
          <td><?= select( "jobLevel", getJobLevelSelectList(), $userSetup->getJobLevel(), true ) ?></td>
        </tr>
        <tr>
          <th>Job number</th>
          <td><input type="text" name="jobNumber" value="<?= $userSetup->getJobNumber() ?>" maxlength="20" class="required"/></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $userSetup->getId() ?>" />
      <input type="hidden" name="user_id" value="<?= $userId ?>" />
			<input type="hidden" name="approvePermission" value="0" />      
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_user_setup_info.php?userId=<?= $userId ?>');"/>
    </div>  
  </body>
</html>