<table class="info" cellspacing="0">
  <tr>
    <th>User:</th>
    <td><?= $selectedUser->getFullName() ?></td>
  </tr>
  <tr>
    <th>Evaluation year:</th>
    <td><?= $selectedYear->getName() ?></td>
  </tr>
  <tr>
    <th>Type</th>
    <td><?= $kpa->getTypeDisplay() ?></td>
  </tr>
  <?php if( $kpa->getKPAType() == "career" && $moduleType == "perform" ){ ?>
  <tr>
    <th>SDBIP #:</th>
    <td><?= $kpa->getSDBIPNum() ?></td>
  </tr>
  <?php } ?>
  <tr>
    <th>Name:</th>
    <td><?= $kpa->getName() ?></td>
  </tr>
  <tr>
    <th>Description:</th>
    <td><?= $kpa->getDescription() ?></td>
  </tr>
  <tr>
    <th>Measurement:</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <? if( $useWeights ){ ?>
  <tr>
    <th>Weight:</th>
    <td><?= $kpa->getWeight() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Outcome:</th>
    <td><?= $kpa->getOutcome() ?></td>
  </tr>
  <tr>
    <th>Porfolio of evidence:</th>
    <td><?= $kpa->getEvidence() ?></td>
  </tr>
  <tr>
    <th>Comments:</th>
    <td><?= $kpa->getComments() ?></td>
  </tr>
  <tr>
    <th>Created by</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on</th>
    <td><?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>