<div class="actions">
  <input type="button" value="New comment" onclick="redirect('<?=$moduleType?>_evaluate_comment.php')" <?if($disableBtn){?>disabled="disabled"<?}?>/>
</div>

<table class="list full" cellspacing="0">
  <tr>
    <th width="13%">Created on</th>
    <th width="17%">Created by</th>
    <th width="55%">Message</th>
    <th width="15%">Actions</th>
  </tr>
  <?
    $comments = getEvaluationComments( $moduleType, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );

    if( isset( $comments ) && count( $comments ) > 0 )
    {
      foreach( $comments as $comment ) {
  ?>
  <tr>
    <td><?= $comment->getCreatedOn() ?></td>
    <td><?= $comment->getCreatedBy()->getFullName() ?></td>
    <td><?= $comment->getMessage() ?></td>
    <td>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_evaluate_comment.php?id=<?= $comment->getId() ?>');" <?if($disableBtn){?>disabled="disabled"<?}?> />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_evaluate_comment.php', '<?= $comment->getId() ?>');" <?if($disableBtn){?>disabled="disabled"<?}?> />
    </td>
  </tr>
  <?
      }
    }
    else {
  ?>
  <tr><td colspan="4">No comments</td></tr>
  <?
    }
  ?>
</table>