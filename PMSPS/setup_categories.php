<?php
include("inc_ignite.php");

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Categories</h1>
    
    <table class="list" cellspacing="0">
      <tr>
        <th>Name</th>
        <th>Evaluation periods</th>
        <th>Use Weights</th>
        <th>Actions</th>
      </tr>
      <?
      $categories = getCategories();
      foreach($categories as $category )
      {
  	  ?>
      <tr>
        <td><?= $category->getName() ?></td>
        <td><?= $category->getEvalPeriodTypeDisplay() ?></td>        
        <td><?= boolValue( $category->getUseWeights() ) ?></td>        
        <td><input type="button" value="Edit" onclick="redirect('setup_category.php?id=<?= $category->getId() ?>');"></td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');" />
    </div>
  </body>
</html>