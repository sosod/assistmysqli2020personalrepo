<?php
if( $action == "complete_evaluation" )
{
  $selectedPeriodEvalStatus->setStatus("completed");
  $selectedPeriodEvalStatus->setFinalKPASymbol( $_REQUEST["finalKPASymbol"] );
  $selectedPeriodEvalStatus->setActionById( getLoggedInUserId() );
  $selectedPeriodEvalStatus->save();
  
  $_SESSION[$moduleType."_evaluate_sess_eval_status_" .getLoggedInUserId()] = $selectedPeriodEvalStatus;  
}

if( $action == "reset_status" )
{
  $selectedPeriodEvalStatus->setStatus("pending");
  $selectedPeriodEvalStatus->setActionById( getLoggedInUserId() );
  $selectedPeriodEvalStatus->save();
}
?>