<?php
include("report_inc.php");

$errors = array();

if( isset( $selectedYear ) && isset( $selectedPeriod ) )
{

  $orderBy = $_REQUEST["orderBy"];
  if( exists( $orderBy ) == false )
  {
    $orderBy = "tkname";
  }
  $orderByDir = $_REQUEST["orderByDir"];
  if( exists( $orderByDir ) == false )
  {
    $orderByDir = "ASC";
  }

  $departmentId = $_REQUEST["departmentId"];
  $section = $_REQUEST["section"];
  $managerId = $_REQUEST["managerId"];
  $jobLevel = $_REQUEST["jobLevel"];


  /* BUILD UP THE SQL QUERY */
  $sql = " SELECT u.*, " .
         " us.section, " .
         " us.job_level, " .
         " d.value AS department, ".
         " j.value AS job_title, " .
         " (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_timekeep um WHERE um.tkid = u.tkmanager ) AS manager, ";

   if( $moduleType == "career" )
   {
     $sql .= " (SELECT IFNULL( (SUM(ke.rating) / (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa k, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_id = k.id AND ke.eval_period = ".$selectedPeriod." AND ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' AND ke.rating != 0)), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND ke.type = '".$moduleType."' AND ke.rating != 0 ) AS avg_score, ";
   }
   else if( $moduleType == "perform" )
   {
     if( $useWeights )
     {
       $sql .= " (SELECT IFNULL( SUM(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa k WHERE ke.user_id = u.tkid AND ke.kpa_id = k.id AND k.kpa_type='career' AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' AND ke.rating != 0) + (SELECT IFNULL( SUM(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa k WHERE ke.user_id = u.tkid AND ke.kpa_id = k.id AND k.kpa_type='bonus' AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) + (SELECT IFNULL( SUM(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_managerial k WHERE ke.user_id = u.tkid AND ke.kpa_man_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) + ( SELECT IFNULL( SUM(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_org k WHERE ke.user_id = u.tkid AND ke.kpa_org_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) AS total_percentage, ";
     }
     else
     {
       $sql .= " (SELECT IFNULL( (SUM(ke.rating) / ( (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa k, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_id = k.id AND ke.eval_period = ".$selectedPeriod." AND ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' AND ke.rating != 0)  + (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_managerial km, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_man_id = km.id AND ke.eval_period = ".$selectedPeriod." AND km.user_id = u.tkid AND km.eval_year_id = ".$selectedYear->getId()." AND km.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) + (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_org ko, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_org_id = ko.id AND ke.eval_period = ".$selectedPeriod." AND ko.user_id = u.tkid AND ko.eval_year_id = ".$selectedYear->getId()." AND ko.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) ) ), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND ke.type = '".$moduleType."' AND ke.rating != 0 ) AS avg_score, ";
     }
   }
   else if( $moduleType == "contract" )
   {
     if( $useWeights )
     {
       $sql .= " (SELECT IFNULL( SUM(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_managerial k WHERE ke.user_id = u.tkid AND ke.kpa_man_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) + ( SELECT IFNULL( SUM(ke.rating/5*k.weight), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke, assist_".getCompanyCode()."_pmsps_kpa_org k WHERE ke.user_id = u.tkid AND ke.kpa_org_id = k.id AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND k.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) AS total_percentage, ";
     }
     else
     {
       $sql .= " (SELECT IFNULL( (SUM(ke.rating) / ( (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_managerial km, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_man_id = km.id AND ke.eval_period = ".$selectedPeriod." AND km.user_id = u.tkid AND km.eval_year_id = ".$selectedYear->getId()." AND km.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) + (SELECT COUNT(*) FROM assist_".getCompanyCode()."_pmsps_kpa_org ko, assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.kpa_org_id = ko.id AND ke.eval_period = ".$selectedPeriod." AND ko.user_id = u.tkid AND ko.eval_year_id = ".$selectedYear->getId()." AND ko.type = '".$moduleType."' AND ke.type = '".$moduleType."' ) ) ), 0 ) FROM assist_".getCompanyCode()."_pmsps_eval_kpa ke WHERE ke.user_id = u.tkid AND ke.eval_year_id = ".$selectedYear->getId()." AND ke.eval_period = ".$selectedPeriod." AND ke.type = '".$moduleType."' AND ke.rating != 0 ) AS avg_score, ";
     }
   }

   $sql .= " (SELECT es.final_kpa_symbol FROM assist_".getCompanyCode()."_pmsps_eval_status es WHERE es.user_id = u.tkid AND es.eval_year_id = ".$selectedYear->getId()." AND es.eval_period = ".$selectedPeriod." AND es.type = '".$moduleType."' ) AS final_kpa_symbol, " .
         " (SELECT es.comments FROM assist_".getCompanyCode()."_pmsps_eval_status es WHERE es.user_id = u.tkid AND es.eval_year_id = ".$selectedYear->getId()." AND es.eval_period = ".$selectedPeriod." AND es.type = '".$moduleType."' ) AS comments " .
         " FROM assist_".getCompanyCode()."_timekeep u, " .
         " assist_".getCompanyCode()."_menu_modules_users mmu, " .
         " assist_".getCompanyCode()."_pmsps_user_setup us, " .
         " assist_".getCompanyCode()."_list_dept d, " .
         " assist_".getCompanyCode()."_list_jobtitle j " .
         " WHERE us.user_id = u.tkid " .
         " AND u.tkid = mmu.usrtkid " .
         " AND mmu.usrmodref = 'PMSPS' " .
         " AND us.category_id =  " . getTypeCategory( $moduleType )->getId() .
         " AND d.id = u.tkdept " .
         " AND j.id = u.tkdesig ";

  if( exists( $departmentId ) )
  {
    $sql .= " AND u.tkdept = " . $departmentId;
  }

  if( exists( $section ) )
  {
    $sql .= " AND us.section = '" . $section . "' ";
  }

  if( exists( $managerId ) )
  {
    $sql .= " AND u.tkmanager = '" . $managerId . "'";
  }

  if( exists( $jobLevel ) )
  {
    $sql .= " AND us.job_level = " . $jobLevel;
  }

  $sql .= " ORDER BY " . $orderBy . " " . $orderByDir;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Reporting :: Fishbowl Report</h1>

    <form action="<?=$moduleType?>_report_fishbowl.php" id="frmUserSelect" method="get" >
      <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
        <tr>
          <td valign="middle">
           <strong>Evaluation year:</strong> <?= select( "selectedYearId", getEvalYearSelectList(), ( isset( $selectedYear ) ? $selectedYear->getId() : "" ), true ) ?>
           <strong>Evaluation period:</strong> <?= select( "selectedPeriod", getTypePeriodSelectList( $moduleType ), $selectedPeriod, true ) ?>
          </td>
          <td valign="middle"><input type="submit" value="Select" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="year_period_select" />
    </form>
    <hr/>

    <?
      if( isset( $selectedYear ) && isset( $selectedPeriod ) )
      {
    ?>

    <h5>Period <?= getPeriodStartDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?> to <?= getPeriodEndDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></h5>
    <br/>
    <h5>Report Filter:</h5>

    <form action="<?=$moduleType?>_report_fishbowl.php" id="frmFilter" method="get" class="filter" >
      <div>
        <label>Department</label>
        <?= select( "departmentId", getDepartmentSelectList(), $departmentId, false, false, "All" ) ?>
        <label>Section</label>
        <?= select( "section", getUserSectionSelectList(), $section, false, false, "All" ) ?>
      </div>
      <div>
        <label>Manager</label>
        <?= select( "managerId", getManagerSelectList(), $managerId, false, false, "All" ) ?>
        <label>Job level</label>
        <?= select( "jobLevel", getNumericSelectList(1,20), $jobLevel, false, false, "All" ) ?>
      </div>
      <div>
        <label>Order by</label>
        <?= select( "orderBy", getFishbowlOrderBySelectList(), $orderBy, false, false, false  ) ?>
        <?= select( "orderByDir", getOrderByDirectionSelectList(), $orderByDir, false, false, false ) ?>
        <input type="submit" value="Select" />
      </div>
    </form>

    <table class="list full" cellspacing="0" style="margin-top:15px;">
      <tr>
        <th width="12%">User</th>
        <th width="5%">Job level</th>
        <th width="11%">Department</th>
        <th width="10%">Section</th>
        <th width="12%">Manager</th>
        <? if( $useWeights ){ ?>
        <th width="7%">Total Percentage</th>
        <? }else{ ?>
        <th width="7%">Avg Score</th>
        <? } ?>
        <th width="6%">Symbol</th>
        <th width="7%">Final Symbol</th>
        <th width="25%">Comments</th>
        <th width="5%">Actions</th>
      </tr>
      <?
        $data = getListRecords( $sql );

        if( count($data) > 0 )
        {
          foreach( $data as $record )
          {
      ?>
      <tr>
        <td><?= $record["tkname"] ?> <?= $record["tksurname"] ?></td>
        <td align="right"><?= $record["job_level"] ?></td>
        <td><?= $record["department"] ?></td>
        <td><?= $record["section"] ?></td>
        <td><?= $record["tkmanager"] == "S" ? "Self" : $record["manager"] ?></td>

        <? if( $useWeights ){ ?>
        <td align="right"><?= exists($record["total_percentage"]) ? number_format($record["total_percentage"], 1, '.', '') . "%" : "?" ?></td>
        <td align="right"><?= exists($record["total_percentage"]) ? KPAEvaluation::bellCurveSymbolFromPercentage( $record["total_percentage"] ) : "?" ?></td>
        <? }else{ ?>
        <td align="right"><?= exists($record["avg_score"]) ? number_format(round($record["avg_score"] ,1 ), 1, '.', '') : "?" ?></td>
        <td align="right"><?= exists($record["avg_score"]) ? KPAEvaluation::bellCurveSymbol( $record["avg_score"] ) : "?" ?></td>
        <? } ?>
        <td align="right"><?= $record["final_kpa_symbol"] ?></td>
        <td><?= $record["comments"] ?></td>
        <td><? if( exists($record["avg_score"]) || exists($record["total_percentage"]) ){ ?><input type="button" value="Update" onclick="redirect('<?=$moduleType?>_report_fishbowl_update.php?userId=<?= $record['tkid'] ?>');"/><? } ?></td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr><td colspan="10">No record match the selected filter options.</td></tr>
      <?
        }
      ?>
    </table>
    <?


      }
      else
      {
    ?>
    <p class="instruction">Please select an evaluation year and period above to continue.</p>
    <?
      }
    ?>
  </body>
</html>