<? if( count( $jobFunctions ) < $maxJobFunctions && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New job function" onclick="redirect('<?=$moduleType?>_activate_job_function.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="30%">Name</th>
    <th width="50%">Description</th>
    <th width="20%">Actions</th>
  </tr>
  <?
    if( isset( $jobFunctions ) && count( $jobFunctions ) > 0 )
    {
      foreach( $jobFunctions as $jobFunction ) {
  ?>
  <tr>
    <td><?= $jobFunction->getName() ?></td>
    <td><?= $jobFunction->getDescription() ?></td>
    <td>
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_job_function.php?id=<?= $jobFunction->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_job_function.php', '<?= $jobFunction->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="4">No job functions</td></tr>
  <?
    }
  ?>
</table>