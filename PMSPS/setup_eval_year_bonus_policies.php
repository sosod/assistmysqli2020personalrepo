<?php
include("inc_ignite.php");


$evalYearId = $_REQUEST["evalYearId"];
$bonusPolicies = getBonusPolicies( $evalYearId );
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Year Bonus Policies</h1>
    
    <table class="list" cellspacing="0">
      <tr>
        <th>Name</th>
        <th>Percentage</th>
        <th>Lower limit</th>
        <th>Upper limit</th>
        <th>Actions</th>        
      </tr>
  	  <?
      foreach( $bonusPolicies as $bonusPolicy )
      {
  	  ?>
      <tr>
        <td><?= $bonusPolicy->getName() ?></td>
        <td><?= $bonusPolicy->getPercentage() ?>%</td>
        <td><?= $bonusPolicy->getLowerLimit() ?>%</td>
        <td><?= $bonusPolicy->getUpperLimit() ?>%</td>        
        <td>
          <input type="button" value="Edit" onclick="redirect('setup_eval_year_bonus_policy.php?id=<?= $bonusPolicy->getId() ?>');">
        </td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_eval_years.php');"/>
    </div>  
  </body>
</html>