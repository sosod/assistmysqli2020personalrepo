<?php
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$kpa = new KPAManagerial();

if( $action == "save" )
{
  $kpa->loadFromRequest( $_REQUEST );
  $kpa->save();

  redirect( $moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $kpa->setId( $id );
  $kpa->delete();

  redirect( $moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $kpa->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">

      function loadCoreSkillDefinition()
      {
        var $defTD = $("#core_man_skill_def");
        var val = $("select[name='coreManagerialSkillId']").val();

        if( val == "" )
        {
          $defTD.html("Select a core managerial skill");
        }
        else
        {
        	$.ajax(
        	{
        		url: "ajax_coreskill_definition.php",
            data: "coreManagerialSkillId=" + val,
            success: function( response )
            {
              $defTD.html( response );
            }
          });
        }
      }

      function loadSkillLevelDefinition()
      {
        var $defTD = $("#skill_level_def");
        var coreVal = $("select[name='coreManagerialSkillId']").val();
        var skillVal = $("select[name='skillLevel']").val();

        if( coreVal == "" )
        {
          $defTD.html("Select a core managerial skill");
        }
        else if( skillVal == "" )
        {
          $defTD.html("Select a skill level");
        }
        else
        {
        	$.ajax(
          {
            url: "ajax_coreskill_definition.php",
            data: "coreManagerialSkillId=" + coreVal + "&skillLevel=" + skillVal,
            success: function( response )
            {
              $defTD.html( response );
            }
          });
        }
      }

      function updateDefinitions()
      {
    	  loadCoreSkillDefinition();
    	  loadSkillLevelDefinition();
      }

      $(document).ready(function(){
          $("#frmKPA").validate({
              submitHandler: function(form) {

                  var msg = null;

              <? if( $useWeights ){ ?>
                  var $weight = $('#fldWeight');

                  if( $weight.val() == "" )
                  {
                      msg = "Weight is required.";
                  }

                  if( /^([0-9]+)(\.([0-9]{2}))?$/i.test( $weight.val() ) == false )
                  {
                      msg = "Please specify a valid weight.";
                  }

                  if( parseFloat( $weight.val() ) > 25 )
                  {
                      msg = "Weight may not be more than 25.";
                  }

                  if( msg != null )
                  {
                      $weight.after('<label for="fldWeight" generated="true" class="error">' + msg + '</label>');
                      return false;
                  }
                  <? } ?>

                  if( msg == null )
                  {
                      form.submit();
                  }
              }
          });
        $("select[name='coreManagerialSkillId']").change( updateDefinitions );
        $("select[name='skillLevel']").change( loadSkillLevelDefinition );
        updateDefinitions();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Managerial KPI</h1>

    <form id="frmKPA" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Core Managerial Skill</th>
          <td><?= select( "coreManagerialSkillId", getCoreManagerialSkillSelectList(), $kpa->getCoreManagerialSkillId(), true ) ?></td>
        </tr>
        <tr>
          <th>Core Managerial Skill Definition</th>
          <td id="core_man_skill_def"></td>
        </tr>
        <tr>
          <th>Skill Level</th>
          <td><?= select( "skillLevel", getSkillLevelSelectList(), $kpa->getSkillLevel(), true ) ?></td>
        </tr>
        <tr>
          <th>Skill Level Definition</th>
          <td id="skill_level_def"></td>
        </tr>
        <tr>
          <th>Measurement:</th>
          <td><textarea name="measurement" cols="50" rows="5"><?= $kpa->getMeasurement() ?></textarea></td>
        </tr>
        <tr>
          <th>Outcome:</th>
          <td><textarea name="outcome" cols="50" rows="5"><?= $kpa->getOutcome() ?></textarea></td>
        </tr>
        <tr>
          <th>Portfolio of evidence:</th>
          <td><textarea name="evidence" cols="50" rows="5"><?= $kpa->getEvidence() ?></textarea></td>
        </tr>
        <tr>
          <th>Comments:</th>
          <td><textarea name="comments" cols="50" rows="5"><?= $kpa->getComments()?></textarea></td>
        </tr>
        <? if( $useWeights ){ ?>
        <tr>
          <th>Weight:</th>
            <?php /* <td><?= select( "weight", getNumericSelectList(0,100), $kpa->getWeight(), true ) ?></td> */ ?>
            <td><input type="text" id="fldWeight" name="weight" value="<?= $kpa->getWeight() ?>" class="number" maxlength="4" size="4"/></td>
        </tr>
        <? } ?>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />
      <input type="hidden" name="id" value="<?= $kpa->getId() ?>" />
      <? if( $useWeights == false ){ ?>
      <input type="hidden" name="weight" value="0" />
      <? } ?>
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_activate.php');" />
    </div>
  </body>
</html>