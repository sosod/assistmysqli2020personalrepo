<?php
include("inc_ignite.php");

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - User Configuration</h1>
		
		<div class="actions">
			<input type="button" value="User Overview Report" onclick="redirect('setup_users_report.php');" />
		</div>
    
    <table class="list" cellspacing="0">
      <tr>
        <th>User</th>
        <th>Category</th>
        <th>Section</th>
        <th>Job level</th>
        <th>Actions</th>        
      </tr>
      <?
      $usersWithSetup = getUsersWithSetup();
      foreach( $usersWithSetup as $userWithSetup )
      {
        $user = $userWithSetup[0];
        $userSetup = $userWithSetup[1];
  	  ?>
      <tr>
        <td><?= $user->getFullName() ?></td>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getCategory()->getName() ?></td>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getSection() ?></td>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getJobLevel() ?></td>
        <td>
          <input type="button" value="User Setup" onclick="redirect('setup_user_setup_info.php?userId=<?= $user->getId() ?>');">
          <input type="button" value="User Access" onclick="redirect('setup_user_access_info.php?userId=<?= $user->getId() ?>');">
        </td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');" />
    </div>  
  </body>
</html>