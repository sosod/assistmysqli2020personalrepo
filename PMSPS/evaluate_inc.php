<?php

  if( $userSetup->isTransient() )
  {
    redirect("pmsps_warning.php?type=1");
  }
  //if( $userSetup->getCreatePermission() == false && $userSetup->getApprovePermission() == false )
  //{
  //  redirect("career_warning.php?type=3");
  //}

  /* *********************************************************************** */
  /* ************************ LOAD THE SELECTED USER *********************** */
  /* *********************************************************************** */
  $selectedUserId = $_SESSION[$moduleType."_evaluate_sess_selected_user_" .getLoggedInUserId()];

  if( exists( $selectedUserId ) )
  {
    $selectedUser = new User();
    $selectedUser->loadFromIdForActivation( $selectedUserId );
  }

  if( isset( $selectedUser ) == false || $_REQUEST["action"] == "user_year_select" )
  {
    /*if( $userSetup->getApprovePermission() == false ) //selected user = logged in user
    {
      $selectedUser = new User();
      $selectedUser->loadFromIdForActivation( getLoggedInUserId() );
    }
    else
    {*/
      if( exists( $_REQUEST["selectedUserId"] ) )
      {
        $selectedUser = new User();
        $selectedUser->loadFromIdForActivation( $_REQUEST["selectedUserId"] );
      }
    /*}*/

    if( isset( $selectedUser ) )
    {
      $_SESSION[$moduleType."_evaluate_sess_selected_user_" .getLoggedInUserId()] = $selectedUser->getId();
    }
    else
    {
      $selectedUser = null;
    }
  }

  /* *********************************************************************** */
  /* ************************ LOAD THE SELECTED YEAR *********************** */
  /* *********************************************************************** */
  $selectedYearId = $_SESSION[ $moduleType."_evaluate_sess_selected_year_" .getLoggedInUserId()];

  if( exists( $selectedYearId ) )
  {
    $selectedYear = new EvalYear();
    $selectedYear->loadFromId( $selectedYearId );
  }

  if( isset( $selectedYear ) == false || $_REQUEST["action"] == "user_year_select" )
  {
    if( exists( $_REQUEST["selectedYearId"] ) )
    {
      $selectedYear = new EvalYear();
      $selectedYear->loadFromId( $_REQUEST["selectedYearId"] );
    }

    if( isset( $selectedYear ) )
    {
      $_SESSION[$moduleType."_evaluate_sess_selected_year_" .getLoggedInUserId()] = $selectedYear->getId();
    }
    else
    {
      $selectedYear = null;
    }
  }

  /* *********************************************************************** */
  /* *********************** LOAD THE SELECTED PERIOD ********************** */
  /* *********************************************************************** */
  $selectedPeriod = $_SESSION[$moduleType."_evaluate_sess_selected_period_" .getLoggedInUserId()];

  if( isset( $selectedPeriod ) == false || $_REQUEST["action"] == "period_select" )
  {
    if( exists( $_REQUEST["selectedPeriod"] ) )
    {
      $selectedPeriod = $_REQUEST["selectedPeriod"];
    }
    else
    {
      $selectedPeriod = null;
    }

    $_SESSION[$moduleType."_evaluate_sess_selected_period_" .getLoggedInUserId()] = $selectedPeriod;
  }

  /* *********************************************************************** */
  /* ************** LOAD THE SELECTED USER EVALUATION STATUS *************** */
  /* *********************************************************************** */
  //$selectedPeriodEvalStatus = $_SESSION[$moduleType."_evaluate_sess_eval_status_" .getLoggedInUserId()];

  if( isset( $selectedUser ) == true &&
      isset( $selectedYear ) == true &&
      isset( $selectedPeriod ) == true )
  {
    /*if( isset( $selectedPeriodEvalStatus ) == false ||
        $selectedPeriodEvalStatus->getUserId() != $selectedUser->getId() ||
        $selectedPeriodEvalStatus->getEvalYearId() != $selectedYear->getId() ||
        $selectedPeriodEvalStatus->getEvalPeriod() != $selectedPeriod ||
        $_REQUEST["action"] == "complete_evaluation" )
    {*/
      $selectedPeriodEvalStatus = new EvaluationStatus();
      $selectedPeriodEvalStatus->loadFor( $moduleType, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
      /*$_SESSION[$moduleType."_evaluate_sess_eval_status_" .getLoggedInUserId()] = $selectedPeriodEvalStatus;
    }*/
  }
?>
