<?php
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Reporting</h1>

		<div class="option">
      <h2>User Status Report</h2>
      <div class="info">
        <p>View the activation and evaluation status of the users</p>
      </div>
      <input type="button" value="Select" onclick="redirect('<?=$moduleType?>_report_status.php');" />
    </div>

    <div class="option">
      <h2>Fishbowl Report</h2>
      <div class="info">
        <p>View all the user KPI evaluations</p>
      </div>
      <input type="button" value="Select" onclick="redirect('<?=$moduleType?>_report_fishbowl.php');" />
    </div>

    <div class="option">
      <h2>Bell Curve Graphs</h2>
      <div class="info">
        <p>View the KPI Bell Curve graphs for departments and job levels</p>
      </div>
      <input type="button" value="Select" onclick="redirect('<?=$moduleType?>_report_bellcurve.php');" />
    </div>

    <div class="option">
      <h2>Learning Activities</h2>
      <div class="info">
        <p>View all the user Learning activities</p>
      </div>
      <input type="button" value="Select" onclick="redirect('<?=$moduleType?>_report_learning.php');" />
    </div>

    <div class="option">
      <h2>Final Score Report</h2>
      <div class="info">
        <p>View all the average user score over all evaluation periods</p>
      </div>
      <input type="button" value="Select" onclick="redirect('<?=$moduleType?>_report_finalscore.php');" />
    </div>

  </body>
</html>