<? if( count( $uniqueKPAs ) < $maxUniqueKPAs && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New KPI unique to this job" onclick="redirect('<?=$moduleType?>_activate_kpa.php?kpaType=unique')" />
  <input type="button" value="Import" onclick="redirect('<?=$moduleType?>_activate_kpa_import.php?kpaType=unique')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="15%">Name</th>
    <th width="25%">Description</th>
    <th width="25%">Measurement</th>
    <th width="20%">Comments</th>
    <th width="15%">Actions</th>
  </tr>
  <?
    if( isset( $uniqueKPAs ) && count( $uniqueKPAs ) > 0 )
    {
      foreach( $uniqueKPAs as $kpa ) {
  ?>
  <tr>
    <td><?= $kpa->getName() ?></td>
    <td><?= $kpa->getDescription() ?></td>
    <td><?= $kpa->getMeasurement() ?></td>
    <td><?= $kpa->getComments() ?></td>
    <td>
      <input type="button" value="View" onclick="redirect('<?=$moduleType?>_activate_kpa_info.php?id=<?= $kpa->getId() ?>');" />
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_kpa.php?id=<?= $kpa->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_kpa.php', '<?= $kpa->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="5">No KPIs unqiue to this job</td></tr>
  <?
    }
  ?>
</table>