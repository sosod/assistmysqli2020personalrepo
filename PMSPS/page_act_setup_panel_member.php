<?php
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$panelMember = new PanelMember();

if( $action == "save" )
{
  $panelMember->loadFromRequest( $_REQUEST );
  $panelMember->save();

  redirect($moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $panelMember->setId( $id );
  $panelMember->delete();

  redirect( $moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $panelMember->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmPanelMember").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Panel Member</h1>

    <form id="frmPanelMember" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Panel Title:</th>
          <td><?= select( "panelTitleId", getPanelTitleSelectList(), $panelMember->getPanelTitleId(), true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?=$moduleType?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_activate.php');" />
    </div>
  </body>
</html>