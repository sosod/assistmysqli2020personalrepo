<?php
include("inc_ignite.php");
include("contract_inc.php");
include("activate_inc.php");

$errors = array();

$action = $_REQUEST["action"];

include("logic_act_status_change.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation</h1>

    <? include("frm_activate_user_year_select.php"); ?>

    <? if( isset( $selectedUser ) && isset( $selectedYear ) ){ ?>

    <? include("info_act_summary.php"); ?>

    <?
      if( $moduleType == "contract" )
      {
        $panelMembers = getPanelMembers( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
        if( count( $panelMembers ) == 0 )
        {
          $errors[] = "At least one evaluation panel member required.";
        }
      }
      $formalQualifications = getQualifications( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "formal" );
      $informalQualifications = getQualifications( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "informal" );
      $experiences = getExperiences( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $professionalBodies = getProfessionalBodies( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $orgKPAs = getOrganisationalKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $learningActivities = getLearningActivities( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $comments = getActivationComments( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

      if( $useWeights )
      {
        if( $selectedUserStatus->getOrgKPAPercentage() == 0 )
        {
          $errors[] = "Organisational KPI percentage not defined. Please define under setup tab.";
        }

        if( $selectedUserStatus->getManKPAPercentage() == 0 )
        {
          $errors[] = "Managerial KPI percentage not defined. Please define under setup tab.";
        }

        if( (int)($selectedUserStatus->getOrgKPAPercentage() + $selectedUserStatus->getManKPAPercentage()) != 100 )
        {
          $errors[] = "Organisational and Managerial KPI percentage not equal to 100%. Currently it is " . ($selectedUserStatus->getOrgKPAPercentage() + $selectedUserStatus->getManKPAPercentage()) . "%";
        }

        $orgKPAWeightTotal = 0;

        foreach( $orgKPAs as $kpa )
        {
          $orgKPAWeightTotal += $kpa->getWeight();
        }

        if( (int)$orgKPAWeightTotal != (int)$selectedUserStatus->getOrgKPAPercentage() )
        {
          $errors[] = "Organisational KPI weights do not add up to " . $selectedUserStatus->getOrgKPAPercentage() . "%. Currently it is " . $orgKPAWeightTotal . "%.";
        }

        $manKPAWeightTotal = 0;

        foreach( $manKPAs as $kpa )
        {
          $manKPAWeightTotal += $kpa->getWeight();
        }

        if( (int)$manKPAWeightTotal != (int)$selectedUserStatus->getManKPAPercentage() )
        {
          $errors[] = "Managerial KPI weights do not add up to " . $selectedUserStatus->getManKPAPercentage() . "%. Currently it is " . $manKPAWeightTotal . "%.";
        }
      }
    ?>

    <? include("frm_activate_approve.php"); ?>

    <div id="tabs" class="tab-group" style="margin-top:15px;">
      <ul>
        <li class="ui-tabs-nav-item"><a href="#act_setup">Setup</a></li>
        <li class="ui-tabs-nav-item"><a href="#qualifications">Qualifications</a></li>
        <? /* <li class="ui-tabs-nav-item"><a href="#job_functions">Job functions</a></li>
        <li class="ui-tabs-nav-item"><a href="#goals">Goals</a></li>
        <li class="ui-tabs-nav-item"><a href="#careerkpas">Career KPAs</a></li> */ ?>
        <li class="ui-tabs-nav-item"><a href="#orgkpas">Organisational KPIs</a></li>
        <li class="ui-tabs-nav-item"><a href="#managerialkpas">Managerial KPIs</a></li>
        <li class="ui-tabs-nav-item"><a href="#learning">Learning activities</a></li>
        <li class="ui-tabs-nav-item"><a href="#comments">Comments</a></li>
      </ul>
      <div id="act_setup">
        <? include("tab_act_setup.php"); ?>
      </div><!-- end act_setup -->
      <div id="qualifications">
        <div id="tabs" class="tab-group" style="margin-top:15px;">
          <ul>
            <li class="ui-tabs-nav-item"><a href="#formal">Formal</a></li>
            <li class="ui-tabs-nav-item"><a href="#informal">Informal</a></li>
            <li class="ui-tabs-nav-item"><a href="#experience">Experience</a></li>
            <li class="ui-tabs-nav-item"><a href="#profbodies">Professional Bodies</a></li>
          </ul>
          <div id="formal">
            <? include("tab_act_qualifications_formal.php"); ?>
          </div>
          <div id="informal">
            <? include("tab_act_qualifications_informal.php"); ?>
          </div>
          <div id="experience">
            <? include("tab_act_experience.php"); ?>
          </div>
          <div id="profbodies">
            <? include("tab_act_professional_body.php"); ?>
          </div>
        </div>

      </div><!-- end qualifications -->
      <? /* <div id="job_functions">
        <? include("tab_act_job_functions.php"); ?>
      </div><!-- end job functions -->
      <div id="goals">
        <? include("tab_act_career_goals.php"); ?>
      </div><!-- end goals -->
      <div id="careerkpas">
        <? include("tab_act_kpa_career.php"); ?>
      </div><!-- end careerkpas --> */ ?>
      <div id="orgkpas">
        <? include("tab_act_kpa_organisational.php"); ?>
      </div><!-- end orgkpas -->
      <div id="managerialkpas">
        <? include("tab_act_kpa_managerial.php"); ?>
      </div><!-- end managerialkpas -->
      <div id="learning">
        <? include("tab_act_learning_activities.php"); ?>
      </div><!-- end skills -->
      <div id="comments">
        <? include("tab_act_comments.php"); ?>
      </div><!-- end comments -->
    </div><!-- end tabs -->
    <?
    }
    else
    {
    ?>
    <p class="instruction">Please select a user and evaluation year above to continue.</p>
    <? } ?>
  </body>
</html>

