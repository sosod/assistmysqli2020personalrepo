<?php
include("inc_ignite.php");

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Contract KPIs</h1>
    
    <table class="list" cellspacing="0">
      <tr>
        <th>Name</th>
        <th>Actions</th>        
      </tr>
  	  <?
      foreach( ContractKPA::getList() as $contractKPA )
      {
  	  ?>
      <tr>
        <td><?= $contractKPA->getName() ?></td>
        <td>
          <input type="button" value="Edit" onclick="redirect('setup_contract_kpa.php?id=<?= $contractKPA->getId() ?>');">
          <input type="button" value="Delete" onclick="confirmDelete( 'setup_contract_kpa.php', '<?= $contractKPA->getId() ?>');" />
        </td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');"/>
      <input type="button" value="Add" onclick="redirect('setup_contract_kpa.php');">
    </div>  
  </body>
</html>