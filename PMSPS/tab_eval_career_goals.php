<div class="actions">
  <input type="button" value="Evaluate All" onclick="redirect('<?=$moduleType?>_evaluate_career_goals.php');" <?if($disableBtn){?>disabled="disabled"<?}?>/>          
</div>

<table class="list full" cellspacing="0">
  <tr>
    <th>Career Goal</th>
    <th>Completed</th>
    <th>Comments</th>
    <th>Evaluated On</th>
    <th>Actions</th>
  </tr>        
<? 
  foreach( $goalsWithEval as $careerGoalEval )
  {
    $careerGoal = $careerGoalEval[0];
    $careerGoalEvaluation = $careerGoalEval[1];
?>
  <tr>
    <td><?= $careerGoal->getName() ?></td>
    <td><?= $careerGoalEvaluation->isTransient() ? "Not Set" : $careerGoalEvaluation->getCompletedDisplay() ?></td>
    <td><?= $careerGoalEvaluation->isTransient() ? "Not Set" : $careerGoalEvaluation->getComments() ?></td>
    <td><?= $careerGoalEvaluation->isTransient() ? "Not Set" : $careerGoalEvaluation->getEvaluatedOn() ?></td>
    <td><input type="button" value="Evaluate" onclick="redirect('<?=$moduleType?>_evaluate_career_goal.php?careerGoalId=<?= $careerGoal->getId() ?>');" <?if($disableBtn){?>disabled="disabled"<?}?>/></td>
  </tr>      
<?
  }
?>
</table>