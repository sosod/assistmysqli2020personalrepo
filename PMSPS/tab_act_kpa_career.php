<? if( count( $careerKPAs ) < $maxCareerKPAs && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New Performance Development KPI" onclick="redirect('<?=$moduleType?>_activate_kpa.php?kpaType=career')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="15%">Name</th>
    <th width="25%">Description</th>
    <th width="25%">Measurement</th>
    <th width="20%">Comments</th>
    <? if( $moduleType == "perform" && $useWeights ){ ?>
    <th>Weight</th>
    <? } ?>
    <th width="15%">Actions</th>
  </tr>
  <?
    if( isset( $careerKPAs ) && count( $careerKPAs ) > 0 )
    {
      foreach( $careerKPAs as $kpa ) {
  ?>
  <tr>
    <td><?= $kpa->getName() ?></td>
    <td><?= $kpa->getDescription() ?></td>
    <td><?= $kpa->getMeasurement() ?></td>
    <td><?= $kpa->getComments() ?></td>
    <? if( $moduleType == "perform" && $useWeights ){ ?>
    <td><?= $kpa->getWeight() ?></td>
    <? } ?>
    <td>
      <input type="button" value="View" onclick="redirect('<?=$moduleType?>_activate_kpa_info.php?id=<?= $kpa->getId() ?>');" />
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_kpa.php?id=<?= $kpa->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_kpa.php', '<?= $kpa->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="<? if( $moduleType == "perform" && $useWeights ){ ?>6<? }else{?>5<? } ?>">No KPIs linked to career goals</td></tr>
  <?
    }
  ?>
</table>