<?php
$careerGoals = getCareerGoals( $moduleType, $selectedUser->getId(), $selectedYear->getId() ); 
      
$goalsWithEval = array();

$allEvaluated = true;      
    
foreach( $careerGoals as $careerGoal )
{
  $careerGoalEvaluation = new CareerGoalEvaluation();
  $careerGoalEvaluation->loadFor( $moduleType, $careerGoal->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
  
  if( $careerGoalEvaluation->isTransient() )
  {
    $allEvaluated = false;
  }  
  
  $goalsWithEval[] = array( $careerGoal, $careerGoalEvaluation );
}

if( $allEvaluated == false )
{
  $errors[] = "Not all Career Goals have been evaluated.";
}
?>
