<?php
  $moduleName = "Performance Development";
  $moduleType = "career";

  $maxFormalQualifications = 5;
  $maxInformalQualifications = 15;
  $maxExperiences = 5;
  $maxProfessionalBodies = 5;
  $maxJobFunctions = 5;
  $maxCareerGoals = 3;
  $maxJobLevelKPAs = 15;
  $maxUniqueKPAs = 10;
  $maxCareerKPAs = 3;
  $maxLearningActivities = 10;
  $maxStrongAreas = 5;
  $maxDevelopmentAreas = 5;
  $maxComments = 5;

  $useWeights = getTypeCategory( $moduleType )->getUseWeights();
?>