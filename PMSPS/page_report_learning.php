<?php
include("report_inc.php");

$errors = array();

if( isset( $selectedYear ) )
{
  $orderBy = $_REQUEST["orderBy"];
  if( exists( $orderBy ) == false )
  {
    $orderBy = "tkname";
  }  
  $orderByDir = $_REQUEST["orderByDir"];
  if( exists( $orderByDir ) == false )
  {
    $orderByDir = "ASC";
  }
  
  $userId = $_REQUEST["userId"];
  $activity = $_REQUEST["activity"];
  $departmentId = $_REQUEST["departmentId"];
  $section = $_REQUEST["section"];
  $supportPerson = $_REQUEST["supportPerson"];
  $jobLevel = $_REQUEST["jobLevel"];  
  $status = $_REQUEST["status"];    
  
  
  /* BUILD UP THE SQL QUERY */
  $sql = " SELECT l.*, " .
         "        u.*, " .
         "        us.section, " .
         "        us.job_level, " .  
         "        d.value AS department, " .  
         "        j.value AS job_title, " .
         "        (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_timekeep um WHERE um.tkid = u.tkmanager ) AS manager " .    
         " FROM assist_".getCompanyCode()."_pmsps_learning_activity l, " .  
         "      assist_".getCompanyCode()."_timekeep u, " .  
         "      assist_".getCompanyCode()."_pmsps_user_setup us, " .  
         "      assist_".getCompanyCode()."_list_dept d, " .  
         "      assist_".getCompanyCode()."_list_jobtitle j " .  
         " WHERE l.user_id = u.tkid " .
         " AND l.type = '" . $moduleType . "'" .
				 " AND l.eval_year_id = " . $selectedYear->getId() . 
         " AND us.user_id = l.user_id " .  
         " AND d.id = u.tkdept " .   
         " AND j.id = u.tkdesig ";
         
  if( exists( $userId ) )
  {
    $sql .= " AND l.user_id = '" . $userId . "'"; 
  }
  
  if( exists( $activity ) )
  {
    $sql .= " AND l.name = '" . $activity . "'"; 
  }
  
  if( exists( $departmentId ) )
  {
    $sql .= " AND u.tkdept = " . $departmentId; 
  }
  
  if( exists( $section ) )
  {
    $sql .= " AND us.section = '" . $section . "' "; 
  }         
  
  if( exists( $supportPerson ) )
  {
    $sql .= " AND l.support_person = '" . $supportPerson . "'"; 
  }
  
  if( exists( $jobLevel ) )
  {
    $sql .= " AND us.job_level = " . $jobLevel; 
  }
  
  if( exists( $status ) )
  {
    $sql .= " AND l.status = '" . $status . "'"; 
  }
  
  $sql .= " ORDER BY " . $orderBy . " " . $orderByDir;
  
  debug( $sql );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Reporting :: Learning Activities</h1>
    
    <form action="<?=$moduleType?>_report_learning.php" id="frmUserSelect" method="get" >
      <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
        <tr>
          <td valign="middle">
           <strong>Evaluation year:</strong> <?= select( "selectedYearId", getEvalYearSelectList(), ( isset( $selectedYear ) ? $selectedYear->getId() : "" ), true ) ?>
          </td>
          <td valign="middle"><input type="submit" value="Select" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="year_period_select" />
    </form>  
    <hr/>
    
    <?
      if( isset( $selectedYear ) )
      {
    ?>
    
    <h5>Report Filter:</h5>
    
    <form action="<?=$moduleType?>_report_learning.php" id="frmFilter" method="get" class="filter" >
      <div>
        <label>User</label>
        <?= select( "userId", getTypeUserSelectList( $moduleType ), $userId, false, false, "All" ) ?>
        <label>Activity</label>
        <?= select( "activity", getUniqueLearningActivitySelectList( $moduleType ), $activity, false, false, "All" ) ?>
      </div>
      <div>
        <label>Department</label>
        <?= select( "departmentId", getDepartmentSelectList(), $departmentId, false, false, "All" ) ?>
        <label>Section</label>
        <?= select( "section", getUserSectionSelectList(), $section, false, false, "All" ) ?>
      </div>
      <div> 
        <label>Support Person</label>
        <?= select( "supportPerson", getSupportPersonSelectList( $moduleType ), $supportPerson, false, false, "All" ) ?>
        <label>Job level</label>
        <?= select( "jobLevel", getNumericSelectList(1,20), $jobLevel, false, false, "All" ) ?>
        <label>Status</label>
        <?= select( "status", getLearningActivityStatusSelectList(), $status, false, false, "All" ) ?>        
      </div>  
      <div>    
        <label>Order by</label>
        <?= select( "orderBy", getLearningOrderBySelectList(), $orderBy, false, false, false  ) ?>
        <?= select( "orderByDir", getOrderByDirectionSelectList(), $orderByDir, false, false, false ) ?>
        <input type="submit" value="Select" />
      </div>
    </form>  
    
    <table class="list full" cellspacing="0" style="margin-top:15px;">
      <tr>
        <th width="10%">User</th>
        <th width="5%">Job level</th>
        <th width="10%">Department</th>
        <th width="10%">Section</th>
        <th width="5%">Priority</th>
        <th width="20%">Activity</th>
        <th width="20%">Training</th>
        <th width="10%">Support Person</th>
        <th width="5%">Status</th>        
        <th width="5%">Actions</th>
      </tr>
      <?
        $data = getListRecords( $sql );
        
        if( count($data) > 0 )
        {
          foreach( $data as $record ) 
          {
      ?>
      <tr>
        <td><?= $record["tkname"] ?> <?= $record["tksurname"] ?></td>
        <td align="right"><?= $record["job_level"] ?></td>
        <td><?= $record["department"] ?></td>
        <td><?= $record["section"] ?></td>
        <td align="right"><?= $record["priority"] ?></td>
        <td><?= $record["name"] ?></td>
        <td><?= $record["training"] ?></td>
        <td><?= $record["support_person"] ?></td>
        <td><?= LearningActivity::displayStatus( $record["status"] ) ?></td>
        <td><input type="button" value="View" onclick="redirect('<?=$moduleType?>_report_learning_activity.php?learningActivityId=<?= $record['id'] ?>');"/></td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr><td colspan="10">No record match the selected filter options.</td></tr>
      <?        
        }  
      ?>  
    </table>    
    <?
      }
      else
      {
    ?>
    <p class="instruction">Please select an evaluation year above to continue.</p> 
    <?
      }
    ?>
  </body>
</html>