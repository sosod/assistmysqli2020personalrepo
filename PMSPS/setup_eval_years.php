<?php
include("inc_ignite.php");

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Years</h1>

    <table class="list" cellspacing="0">
      <tr>
        <th>Name</th>
        <th>Start</th>
        <th>End</th>
        <th>Actions</th>
      </tr>
  	  <?
      $evalYears = getEvalYears();
      foreach( $evalYears as $evalYear )
      {
  	  ?>
      <tr>
        <td><?= $evalYear->getName() ?></td>
        <td><?= $evalYear->getStart() ?></td>
        <td><?= $evalYear->getEnd() ?></td>
        <td>
          <input type="button" value="Edit" onclick="redirect('setup_eval_year.php?id=<?= $evalYear->getId() ?>');">
          <input type="button" value="Bonus Policies" onclick="redirect('setup_eval_year_bonus_policies.php?evalYearId=<?= $evalYear->getId() ?>');">
          <input type="button" value="Copy Data" onclick="redirect('setup_eval_year_copy.php?evalYearId=<?= $evalYear->getId() ?>');">
        </td>
      </tr>
      <?
      }
      ?>
    </table>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');"/>
      <input type="button" value="Add" onclick="redirect('setup_eval_year.php');">
    </div>
  </body>
</html>