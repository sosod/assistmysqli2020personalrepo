<?php
include("inc_ignite.php");


$userId = $_REQUEST["userId"];

$userSetup = new UserSetup();
$userSetup->loadFromUserId( $userId ); 

if( $userSetup->isTransient() )
{
  //set the user id so that the lazy loading getUser() function can get the user info
  $userSetup->setUserId( $userId );  
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - User Configuration</h1>
    
    <table class="info" cellspacing="0">
      <tr>
        <th>User</th>
        <td><?= $userSetup->getUser()->getFullName() ?></td>
      </tr>
      <tr>
        <th>Category</th>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getCategory()->getName() ?></td>
      </tr>
      <tr>
        <th>May Create</th>
        <td><?= $userSetup->isTransient() ? "Not set" : boolValue( $userSetup->getCreatePermission() ) ?></td>
      </tr>
			<? /*
      <tr>
        <th>May Approve</th>
        <td><?= $userSetup->isTransient() ? "Not set" : boolValue( $userSetup->getApprovePermission() ) ?></td>
      </tr>
			*/ ?>
      <tr>
        <th>HR Manager</th>
        <td><?= $userSetup->isTransient() ? "Not set" : boolValue( $userSetup->getHRManager() ) ?></td>
      </tr>
      <tr>
        <th>Section</th>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getSection() ?></td>
      </tr>
      <tr>
        <th>Job level</th>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getJobLevel() ?></td>
      </tr>
      <tr>
        <th>Job number</th>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getJobNumber() ?></td>
      </tr>
    </table>
    
    <div class="actions" style="margin-bottom:20px;">
      <input type="button" value="Back" onclick="redirect('setup_users.php');" />
      <input type="button" value="Edit" onclick="redirect('setup_user_setup.php?userId=<?= $userSetup->getUserId() ?>');" />
    </div>
    
  </body>
</html>