<h5>Selected user and evaluation year:</h5>
<table class="info wide" cellspacing="0" style="margin-bottom:15px;">
  <tr>
    <th width="18%">User:</th>
    <td width="32%"><?= $selectedUser->getFullName() ?></td>
    <th width="18%">Manager:</th>
    <td width="32%"><?= $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ?></td>
  </tr>
  <tr>
    <th>Evaluation year:</th>
    <td><?= $selectedYear->getName() ?></td>
    <th>Status:</th>
    <td>
      <?= $selectedUserStatus->getStatusDisplay() ?>
      <?php if( $selectedUserStatus->getStatus() != "new" &&
                ( getLoggedInUser()->getUserSetup()->getHRManager() == true || getLoggedInUser()->isAssistSupportUser() ) ){ ?>
      <form id="frmResetStatus" method="post" onsubmit="return confirmResetStatus();">
        <input type="submit" value="Reset Status" />
        <input type="hidden" name="action" value="reset_status" />
      </form>
      <?php }?>
    </td>
  </tr>
  <tr>
    <th>Last Action by:</th>
    <td><?= $selectedUserStatus->getActionBy()->getFullName() ?></td>
    <th>Last Action on:</th>
    <td><?= $selectedUserStatus->getActionOn() ?></td>
  </tr>
  <tr>
    <th>Print:</th>
    <td colspan="3">
      <input type="button" value="Activation" onclick="redirect('<?=$moduleType?>_activate_print.php?action=pdf&ac=<?= time() ?>');" />
      <? if( $moduleType == "contract" ) {?>
      <br/>
      <!--<input type="button" value="Performance Agreement" onclick="redirect('<?=$moduleType?>_activate_print_contract.php?action=agreement&ac=<?= time() ?>');" />-->
      <input type="button" value="Annexure A" onclick="redirect('<?=$moduleType?>_activate_print_contract.php?action=annexure_a&ac=<?= time() ?>');" />
      <input type="button" value="Annexure B" onclick="redirect('<?=$moduleType?>_activate_print_contract.php?action=annexure_b&ac=<?= time() ?>');" />
      <? } ?>
    </td>
  </tr>
</table>