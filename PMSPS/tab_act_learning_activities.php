<? if( count( $learningActivities ) < $maxLearningActivities && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New learning activity" onclick="redirect('<?=$moduleType?>_activate_learning_activity.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="8%">Priority</th>
    <th width="25%">Name</th>
    <th width="37%">Training</th>
    <th width="15%">Support Person</th>
    <th width="15%">Actions</th>
  </tr>
  <?
    if( isset( $learningActivities ) && count( $learningActivities ) > 0 )
    {
      foreach( $learningActivities as $learningActivity ) {
  ?>
  <tr>
    <td><?= $learningActivity->getPriority() ?></td>
    <td><?= $learningActivity->getName() ?></td>
    <td><?= $learningActivity->getTraining() ?></td>
    <td><?= $learningActivity->getSupportPerson() ?></td>
    <td>
      <input type="button" value="View" onclick="redirect('<?=$moduleType?>_activate_learning_activity_info.php?id=<?= $learningActivity->getId() ?>');" />
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_learning_activity.php?id=<?= $learningActivity->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_learning_activity.php', '<?= $learningActivity->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="5">No learning activities</td></tr>
  <?
    }
  ?>
</table>