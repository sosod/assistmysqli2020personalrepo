<? if( count( $formalQualifications ) < $maxFormalQualifications && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New formal qualification" onclick="redirect('<?=$moduleType?>_activate_qualification.php?primaryType=formal')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="35%">Name</th>
    <th width="10%">Accredited</th>
    <th width="40%">Comments</th>
    <th width="15%">Actions</th>
  </tr>
  <?
    if( isset( $formalQualifications ) && count( $formalQualifications ) > 0 )
    {
      foreach( $formalQualifications as $qualification ) {
  ?>
  <tr>
    <td><?= $qualification->getName() ?></td>
    <td><?= boolValue( $qualification->getAccredited() ) ?></td>
    <td><?= $qualification->getComments() ?></td>
    <td>
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_qualification.php?id=<?= $qualification->getId() ?>&primaryType=formal');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_qualification.php', '<?= $qualification->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="4">No formal qualifications</td></tr>
  <?
    }
  ?>
</table>