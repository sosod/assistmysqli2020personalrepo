<?php
include("inc_ignite.php");
include("perform_inc.php");
include("evaluate_inc.php");

$errors = array();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Evaluation</h1>

    <? $evalPeriodType = getPerformCategory()->getEvalPeriodType(); ?>

    <? include("frm_eval_select_period.php"); ?>
  </body>
</html>