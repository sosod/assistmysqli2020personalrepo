<?php
$orgKPAs = getOrganisationalKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() ); 
      
$orgKPAsWithEval = array();

foreach( $orgKPAs as $kpa )
{
  $kpaEvaluation = new KPAEvaluation();
  $kpaEvaluation->loadForOrg( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
  
  if( $kpaEvaluation->isTransient() )
  {
    $allEvaluated = false;
  }
  
  if( $useWeights )
  {
    $totalPercentage += ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );    
  }
  else
  {
    $totalMarks += $kpaEvaluation->getRating();  
  }
  
  $orgKPAsWithEval[] = array( $kpa, $kpaEvaluation );
}  

$numOfKPAs = $numOfKPAs + count( $orgKPAs );
?>
