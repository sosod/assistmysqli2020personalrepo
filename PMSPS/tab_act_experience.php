<? if(count( $experiences ) < $maxExperiences && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New Experience" onclick="redirect('<?=$moduleType?>_activate_experience.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="20%">Type</th>
    <th width="20%">Obtained From</th>
    <th width="10%">Obtained On</th>
    <th width="10%">Number Of Years</th>
    <th width="30%">Description</th>
    <th width="10%">Actions</th>
  </tr>
  <?
    if( isset( $experiences ) && count( $experiences ) > 0 )
    {
      foreach( $experiences as $experience ) {
  ?>
  <tr>
    <td><?= $experience->getName() ?></td>
    <td><?= $experience->getObtainedFrom() ?></td>
    <td><?= $experience->getObtainedOn() ?></td>
    <td><?= $experience->getNumOfYears() ?></td>
    <td><?= $experience->getDescription() ?></td>
    <td>
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_experience.php?id=<?= $experience->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_experience.php', '<?= $experience->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="5">No Experiences</td></tr>
  <?
    }
  ?>
</table>