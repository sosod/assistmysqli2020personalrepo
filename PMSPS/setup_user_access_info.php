<?php
include("inc_ignite.php");


$userId = $_REQUEST["userId"];

$userAccess = new UserAccess();
$userAccess->loadFromUserId( $userId );
if( $userAccess->isTransient() )
{
  //set the user id so that the lazy loading getUser() function can get the user info
  $userAccess->setUserId( $userId );  
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - User Access</h1>
    
    <table class="info" cellspacing="0">
      <tr>
        <th>User</th>
        <td><?= $userAccess->getUser()->getFullName() ?></td>
      </tr>
      <tr>
        <th>Sections</th>
        <td>
          <? 
           if( count( $userAccess->getSections() ) > 0 )
           {
             foreach( $userAccess->getSections() as $section )
             { 
               echo UserAccess::displayAccessSection( $section ) . "<br/>";
             }
           }
           else
           {
             echo "No Access"; 
           }     
          ?>
        </td>
      </tr>
    </table>
    
    <div class="actions" style="margin-bottom:20px;">
      <input type="button" value="Back" onclick="redirect('setup_users.php');" />
      <input type="button" value="Edit" onclick="redirect('setup_user_access.php?userId=<?= $userAccess->getUserId() ?>');" />
    </div>
    
  </body>
</html>