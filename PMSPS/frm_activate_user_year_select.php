<form id="frmUserSelect" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle"><strong>User:</strong>
      <?
        /*if( $userSetup->getApprovePermission() )
        {
          echo select( "selectedUserId", getTypeUserSelectList( $moduleType ), ( isset( $selectedUser ) ? $selectedUser->getId() : "" ), true );
        }*/

				if( getLoggedInUser()->isManager() || getLoggedInUser()->isAssistSupportUser() || getLoggedInUser()->getUserSetup()->getHRManager() == true )
				{
					echo select( "selectedUserId", getLoggedInUser()->getManagedUsersSelectList($moduleType), ( isset( $selectedUser ) ? $selectedUser->getId() : "" ), true );
				}
        else
        {
          echo getLoggedInUser()->getFullName();
        }
      ?>
       <strong>Evaluation year:</strong> <?= select( "selectedYearId", getEvalYearSelectList(), ( isset( $selectedYear ) ? $selectedYear->getId() : "" ), true ) ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="action" value="user_year_select" />
</form>
<hr/>