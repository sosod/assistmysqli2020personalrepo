<?
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];
$experience = new Experience();

if( $action == "save" )
{
  $experience->loadFromRequest( $_REQUEST );
  $experience->save();

  redirect( $moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $experience->setId( $id );
  $experience->delete();

  redirect( $moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $experience->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmExperience").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Experience</h1>

    <form id="frmExperience" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Type:</th>
          <td><input type="text" name="name" value="<?= $experience->getName() ?>" class="required" maxlength="100"/></td>
        </tr>
        <tr>
          <th>Obtained From:</th>
          <td><input type="text" name="obtainedFrom" value="<?= $experience->getObtainedFrom() ?>" class="required" maxlength="100"/></td>
        </tr>
        <tr>
          <th>Obtained On:</th>
          <td><input type="text" name="obtainedOn" value="<?= $experience->getObtainedOn() ?>" readonly="readonly" class="required date-input"/></td>
        </tr>
        <tr>
          <th>Number of Years:</th>
          <td><?= select( "numOfYears", getExperienceYearsList(), $experience->getNumOfYears(), true ) ?></td>
        </tr>
        <tr>
          <th>Description:</th>
          <td><textarea name="description" cols="50" rows="5"><?= $experience->getDescription() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />
      <input type="hidden" name="id" value="<?= $experience->getId() ?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>
  </body>
</html>