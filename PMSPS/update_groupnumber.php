<?php
  include("inc_ignite.php");
?>
<html>
  <head>
    <style type="text/css">
      body{
        font-family:Arial;
        font-size:10px;
      }
    </style>
  </head>
  <body>
    <h1>Organisation KPI Group Number Update</h1>
    <?php
      //loop through categories
      $categories = getCategories();
      $eval_years = getEvalYears();
      foreach( $categories as $category )
      {
        $category_users = getCategoryUsers( $category->getId() );

        echo "<hr/>";
        echo "<h1>Processing category: " . $category->getName() . "</h1>";

        //loop through each eval year
        foreach( $eval_years as $eval_year )
        {
          echo "<h2>Processing evaluation year: " . $eval_year->getName() . "</h2>";

          //loop through each category user
          foreach( $category_users as $category_user )
          {
            echo "<h3>Processing user: " . $category_user->getFullName() . "</h3>";

            $type = getTypeFromCategoryId( $category->getId() );
            $kpas = getOrganisationalKPAs( $type, $category_user->getId(), $eval_year->getId() );

            echo "Found " . count( $kpas ) . " Organisational KPIs<br/>";

            $group_num = 1;

            //loop through each kpa
            foreach( $kpas as $kpa )
            {
              //set group number and save
              $kpa_update_sql = "UPDATE assist_" . $cmpcode . "_pmsps_kpa_org SET group_num = " . $group_num . " WHERE id = " . $kpa->getId();
              mysql_query( $kpa_update_sql, getConnection() );

              echo "&nbsp;&nbsp;-&nbsp;&nbsp;Set group number " . $group_num . " on KPI [" . $kpa->getId() . "]<br/>";

              $group_num++;
            }
          }
        }
      }
    ?>
  </body>
</html>