<?
include("evaluate_inc.php");

$careerGoalId = $_REQUEST["careerGoalId"];
$action = $_REQUEST["action"];

$careerGoal = new CareerGoal();
$careerGoal->loadFromId( $careerGoalId );

$careerGoalEvaluation = new CareerGoalEvaluation();

if( $action == "save" )
{
  $careerGoalEvaluation->loadFromRequest( $_REQUEST );
  $careerGoalEvaluation->save();
  
  redirect( $moduleType."_evaluate_period.php" );
}

$careerGoalEvaluation->loadFor( $moduleType, $careerGoalId, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmCareerGoal").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Evaluation :: Career Goal</h1>
    <form id="frmCareerGoal" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Name:</th>
          <td><?= $careerGoal->getName() ?></td>
        </tr>
        <tr>
          <th>Description:</th>
          <td><?= $careerGoal->getDescription() ?></td>
        </tr>
        <tr>
          <th>Completed: *</th>
          <td><?= select( "completed", getCompletedSelectList(), $careerGoalEvaluation->getCompleted(), true ) ?></td>
        </tr>
        <tr>
          <th>Comments:</th>
          <td><textarea name="comments" cols="50" rows="5"><?= $careerGoalEvaluation->getComments() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Evaluate" /></td>
        </tr>
      </table>
        
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?=$moduleType?>" />         
      <input type="hidden" name="id" value="<?= $careerGoalEvaluation->getId() ?>" />            
      <input type="hidden" name="careerGoalId" value="<?= $careerGoalId ?>" />                  
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="evalPeriod" value="<?= $selectedPeriod ?>" />      
      <input type="hidden" name="evaluatedById" value="<?= getLoggedInUserId() ?>" />
    </form>  
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_evaluate_period.php');" />
    </div>  
  </body>
</html>