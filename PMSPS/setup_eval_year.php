<?php
include("inc_ignite.php");


$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$evalYear = new EvalYear();

if( $action == "save" )
{
  $evalYear->loadFromRequest( $_REQUEST );
  $evalYear->save();
  
  if( exists( $_REQUEST["id"] ) == false )
  {
    /* load eval year to get the id, mysql_insert_id doesnt work ??? */
    $newEvalYear = new EvalYear();
    $newEvalYear->loadFor( $evalYear->getStart(), $evalYear->getEnd() );
    
    BonusPolicy::createDefaultPolicies( $newEvalYear->getId() );    
  }
  
  redirect("setup_eval_years.php" );
}

if( exists( $id ) )
{
  $evalYear->loadFromId( $id );
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmEvalYear").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Year</h1>
    
    <form id="frmEvalYear" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>Name</th>
          <td><input type="text" name="name" value="<?= $evalYear->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Start</th>
          <td><input type="text" name="start" value="<?= $evalYear->getStart() ?>" readonly="readonly" class="required date-input"/></td>
        </tr>
        <tr>
          <th>End</th>
          <td><input type="text" name="end" value="<?= $evalYear->getEnd() ?>" readonly="readonly" class="required date-input"/></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $evalYear->getId() ?>" />
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_eval_years.php');" />
    </div>  
  </body>
</html>