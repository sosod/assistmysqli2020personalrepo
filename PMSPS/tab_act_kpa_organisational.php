<? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
<div class="actions">
  <input type="button" value="New Organisational KPI" onclick="redirect('<?=$moduleType?>_activate_kpa_org.php')" />
  <input type="button" value="Import" onclick="redirect('<?=$moduleType?>_activate_kpa_org_import.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="5%">Group Num</th>
    <th width="5%">SDBIP KPI Num</th>
    <th width="17%">KPA</th>
    <th width="17%">Objective</th>
    <th width="10%">KPI</th>
    <th width="17%">Unit of Measurement</th>
    <th width="10%">Baseline</th>
    <? if( $useWeights ){ ?>
    <th width="5%">Weight</th>
    <? } ?>
    <th>Actions</th>
  </tr>
  <?
    if( isset( $orgKPAs ) && count( $orgKPAs ) > 0 )
    {
      foreach( $orgKPAs as $kpa )
      {
  ?>
  <tr>
    <td><?= $kpa->getGroupNum() ?></td>
    <td><?= $kpa->getSDBIPNum() ?></td>
    <td><?= $kpa->getKPI() ?></td>
    <td><?= $kpa->getObjective() ?></td>
    <td><?= $kpa->getName() ?></td>
    <td><?= $kpa->getMeasurement() ?></td>
    <td><?= $kpa->getBaseline() ?></td>
    <? if( $useWeights ){ ?>
    <td><?= $kpa->getWeight() ?></td>
    <? } ?>
    <td>
      <input type="button" value="View" onclick="redirect('<?=$moduleType?>_activate_kpa_org_info.php?id=<?= $kpa->getId() ?>');" />
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_kpa_org.php?id=<?= $kpa->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_kpa_org.php', '<?= $kpa->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }

      if( $useWeights ){
  ?>
  <tr>
    <td colspan="7">&nbsp;</td>
    <td style="border-top:2px solid #333;border-bottom:2px solid #333;"><?= $orgKPAWeightTotal ?></td>
    <td>&nbsp;</td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="<? if( $useWeights ){ ?>9<?php }else{?>8<?php }?>">No KPIs linked to organisational plan</td></tr>
  <?
    }
  ?>
</table>