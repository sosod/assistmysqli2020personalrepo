<?
include("evaluate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$learningActivity = new LearningActivity();

if( $action == "save" )
{
  $learningActivity->loadFromRequest( $_REQUEST );
  $learningActivity->save();
  
  redirect( $moduleType."_evaluate_period.php" );
}

if( exists( $id ) )
{
  $learningActivity->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmLearningActivity").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Evaluation :: Learning Activity</h1>
    <form id="frmLearningActivity" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Priority *</th>
          <td><?= select( "priority", getNumericSelectList(1,10), $learningActivity->getPriority(), true ) ?></td>
        </tr>
        <tr>
          <th>Name: *</th>
          <td><input type="text" name="name" value="<?= $learningActivity->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Suggested training: *</th>
          <td><textarea name="training" cols="50" rows="5" class="required"><?= $learningActivity->getTraining() ?></textarea></td>
        </tr>
        <tr>
          <th>Mode of delivery: *</th>
          <td><textarea name="deliveryMode" cols="50" rows="5" class="required"><?= $learningActivity->getDeliveryMode() ?></textarea></td>
        </tr>
        <tr>
          <th>Suggested time frames: *</th>
          <td><textarea name="timeFrames" cols="50" rows="5" class="required"><?= $learningActivity->getTimeFrames() ?></textarea></td>
        </tr>
        <?/*<tr>
          <th>Support person: *</th>
          <td><?= select( "supportPersonId", getManagerSelectList(), $learningActivity->getSupportPersonId(), true ) ?></td>
        </tr>*/?>
        <tr>
          <th>Support person: *</th>
          <td><input type="text" name="supportPerson" value="<?= $learningActivity->getSupportPerson() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Outcomes expected:</th>
          <td><textarea name="outcome" cols="50" rows="5"><?= $learningActivity->getOutcome() ?></textarea></td>
        </tr>
        <tr>
          <th>Work opportunity:</th>
          <td><textarea name="workOpportunity" cols="50" rows="5"><?= $learningActivity->getWorkOpportunity() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?=$moduleType?>" />            
      <input type="hidden" name="id" value="<?= $learningActivity->getId() ?>" />            
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>  
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_evaluate_period.php');" />
    </div>  
  </body>
</html>