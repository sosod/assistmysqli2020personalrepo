<? include("frm_eval_user_year_select.php") ?>

<? if( isset( $selectedUser ) && isset( $selectedYear ) ){ ?>

<form id="frmSelectPeriod" action="<?=$moduleType?>_evaluate_period.php" method="post">
<div class="option-group">
<?
   $numOfPeriods = 4;

   if( $evalPeriodType == "bi_annual" )
   {
     $numOfPeriods = 2;
   }

   $evalPeriodStatusses = getEvalPeriodStatusses( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

   if( count( $evalPeriodStatusses ) < $numOfPeriods )
   {
     $evalPeriodStatusses = createEvalPeriodStatusses( $moduleType, $evalPeriodType, $selectedUser->getId(), $selectedYear->getId(), $evalPeriodStatusses );
   }

   foreach( $evalPeriodStatusses as $periodStatus )
   {
     $startOfPeriod = getPeriodStartDate( $evalPeriodType, $selectedYear, $periodStatus->getEvalPeriod() );

     $disableBtn = false;

     if( isDateInFuture( $startOfPeriod ) )
     {
       $disableBtn = true;
     }
?>
  <div class="option">
    <h2>Period <?= $periodStatus->getEvalPeriod() ?></h2>
    <h5 style="margin-top:10px;"><?= $startOfPeriod ?> - <?= getPeriodEndDate( $evalPeriodType, $selectedYear, $periodStatus->getEvalPeriod() ) ?></h5>
    <div class="info">
      <strong>Status:</strong> <span><?= $periodStatus->getStatusDisplay() ?></span><br/>
      <strong>Review type:</strong> <span><?= $periodStatus->getReviewTypeDisplay() ?></span><br/>
      <strong>Locked:</strong> <span><?= boolValue( $periodStatus->getLocked() ) ?></span><br/>
    </div>
    <input type="button" value="Select" onclick="selectPeriod('<?= $periodStatus->getEvalPeriod() ?>');" <?if($disableBtn){?>disabled="disabled"<?}?>/>
  </div>
<?
   }
?>
<input id="select_period" type="hidden" name="selectedPeriod" value="0" />
<input type="hidden" name="action" value="period_select" />
</form>

</div>
<?
}
else
{
?>
<p class="instruction">Please select a user and evaluation year above to continue.</p>
<? } ?>