<?
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];
$primaryType = $_REQUEST["primaryType"];

$qualification = new Qualification();

if( $action == "save" )
{
  $qualification->loadFromRequest( $_REQUEST );
  $qualification->save();
  
  redirect( $moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $qualification->setId( $id );
  $qualification->delete();
  
  redirect( $moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $qualification->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmQualification").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: <?= Qualification::displayPrimaryType( $primaryType ) ?> Qualification</h1>

    <form id="frmQualification" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <? if( $primaryType == "informal" ){ ?>
        <tr>
          <th>Type:</th>
          <td><?= select( "secondaryType", getQualificationSecondaryTypeSelectList(), $qualification->getSecondaryType(), true ) ?></td>
        </tr>
        <? } ?>
        <tr>
          <th>Name:</th>
          <td><input type="text" name="name" value="<?= $qualification->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Accredited:</th>
          <td><?= select( "accredited", getYesNoSelectList(), $qualification->getAccredited(), true ) ?></td>
        </tr>
        <tr>
          <th>Comments:</th>
          <td><textarea name="comments" cols="50" rows="5" class="required"><?= $qualification->getComments() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />            
      <input type="hidden" name="id" value="<?= $qualification->getId() ?>" />            
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>  
  </body>
</html>