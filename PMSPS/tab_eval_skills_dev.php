<div class="actions">
  <input type="button" value="New development area" onclick="redirect('<?=$moduleType?>_evaluate_skill.php?skillType=development')" <?if($disableBtn){?>disabled="disabled"<?}?>/>
</div>

<table class="list full" cellspacing="0">
  <tr>
    <th>Created on</th>
    <th>Created by</th>
    <th>Message</th>
    <th>Actions</th>
  </tr>
  <?
    $skills = getEvaluationSkills( $moduleType, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod, "development" );
  
    if( isset( $skills ) && count( $skills ) > 0 )
    { 
      foreach( $skills as $skill ) {
  ?>
  <tr>
    <td><?= $skill->getCreatedOn() ?></td>
    <td><?= $skill->getCreatedBy()->getFullName() ?></td>
    <td><?= $skill->getMessage() ?></td>
    <td><input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_evaluate_skill.php?id=<?= $skill->getId() ?>&skillType=development')" <?if($disableBtn){?>disabled="disabled"<?}?>/></td>
  </tr>
  <?
      }
    }
    else {
  ?>
  <tr><td colspan="4">No development areas</td></tr> 
  <?
    }
  ?>
</table>