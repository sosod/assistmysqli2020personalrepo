<?php
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

if( $action == "save" )
{
  $selectedUserStatus->setCareerKPAPercentage($_REQUEST["careerKPAPercentage"]);
  $selectedUserStatus->setOrgKPAPercentage($_REQUEST["orgKPAPercentage"]);
  $selectedUserStatus->setManKPAPercentage($_REQUEST["manKPAPercentage"]);
  $selectedUserStatus->save();

  $_SESSION[$moduleType."_activate_sess_status_" .getLoggedInUserId()] = $selectedUserStatus->getId();

  redirect($moduleType."_activate.php" );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmUserStatus").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: KPA Percentages</h1>

    <form id="frmUserStatus" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <? if( $moduleType == "perform" ) { ?>
        <tr>
          <th>Career KPI Percentage:</th>
          <td><?= select( "careerKPAPercentage", getNumericSelectList(0,100), $selectedUserStatus->getCareerKPAPercentage(), true ) ?></td>
        </tr>
        <? }else{ ?>
				<input type="hidden" name="careerKPAPercentage" value="0" />
				<? } ?>
        <tr>
          <th>Organisational KPI Percentage:</th>
          <td><?= select( "orgKPAPercentage", getNumericSelectList(0,100), $selectedUserStatus->getOrgKPAPercentage(), true ) ?></td>
        </tr>
        <tr>
          <th>Managerial KPI Percentage:</th>
          <td><?= select( "manKPAPercentage", getNumericSelectList(0,100), $selectedUserStatus->getManKPAPercentage(), true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_activate.php');" />
    </div>
  </body>
</html>