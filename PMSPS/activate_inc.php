<?php
  if( $userSetup->isTransient() )
  {
    redirect("pmsps_warning.php?type=1");
  }

  if( $userSetup->getCreatePermission() == false /* && $userSetup->getApprovePermission() == false*/ )
  {
    redirect("pmsps_warning.php?type=2");
  }

  /* *********************************************************************** */
  /* ************************ LOAD THE SELECTED USER *********************** */
  /* *********************************************************************** */
  $selectedUserId = $_SESSION[$moduleType."_activate_sess_selected_user_" .getLoggedInUserId()];

  if( exists( $selectedUserId ) )
  {
    $selectedUser = new User();
    $selectedUser->loadFromIdForActivation( $selectedUserId );
  }

  if( isset( $selectedUser ) == false || $_REQUEST["action"] == "user_year_select" )
  {

    /*if( $userSetup->getApprovePermission() == false )*/
    //selected user = logged in user
    if( getLoggedInUser()->isManager() == false && getLoggedInUser()->isAssistSupportUser() == false && getLoggedInUser()->getUserSetup()->getHRManager() == false )
	{
      $selectedUser = new User();
      $selectedUser->loadFromIdForActivation( getLoggedInUserId() );
    }
    else
    {
      if( exists( $_REQUEST["selectedUserId"] ) )
      {



        $selectedUser = new User();
        $selectedUser->loadFromIdForActivation( $_REQUEST["selectedUserId"] );
      }
    }

    if( isset( $selectedUser ) )
    {
      $_SESSION[$moduleType."_activate_sess_selected_user_" .getLoggedInUserId()] = $selectedUser->getId();
    }
    else
    {
      $selectedUser = null;
    }
  }



  /* *********************************************************************** */
  /* ************************ LOAD THE SELECTED YEAR *********************** */
  /* *********************************************************************** */
  $selectedYearId = $_SESSION[ $moduleType."_activate_sess_selected_year_" .getLoggedInUserId()];

  if( exists( $selectedYearId ) )
  {
    $selectedYear = new EvalYear();
    $selectedYear->loadFromId( $selectedYearId );
  }

  if( isset( $selectedYear ) == false || $_REQUEST["action"] == "user_year_select" )
  {
    if( exists( $_REQUEST["selectedYearId"] ) )
    {
      $selectedYear = new EvalYear();
      $selectedYear->loadFromId( $_REQUEST["selectedYearId"] );
    }

    if( isset( $selectedYear ) )
    {
      $_SESSION[$moduleType."_activate_sess_selected_year_" .getLoggedInUserId()] = $selectedYear->getId();
    }
    else
    {
      $selectedYear = null;
    }
  }

  /* *********************************************************************** */
  /* *************** LOAD THE SELECTED USER ACTIVATION STATUS ************** */
  /* *********************************************************************** */
  $selectedUserStatusId = $_SESSION[$moduleType."_activate_sess_status_" .getLoggedInUserId()];

  if( exists( $selectedUserStatusId ) )
  {
    $selectedUserStatus = new ActivationStatus();
    $selectedUserStatus->loadFromId( $selectedUserStatusId );
  }

  if( isset( $selectedUser ) == true && isset( $selectedYear ) == true && ( isset( $selectedUserStatus ) == false || $selectedUserStatus->getUserId() != $selectedUser->getId() || $selectedUserStatus->getEvalYearId() != $selectedYear->getId() || $_REQUEST["action"] == "user_year_select" ) )
  {
      $selectedUserStatus = new ActivationStatus();
      $selectedUserStatus->loadFor( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

      /* if the obj doesn't exist we create it */
      if( $selectedUserStatus->isTransient() )
      {
        $selectedUserStatus->setUserId( $selectedUser->getId() );
        $selectedUserStatus->setEvalYearId( $selectedYear->getId() );
        $selectedUserStatus->setActionById( getLoggedInUserId() );
        $selectedUserStatus->setStatus( "new" );
        $selectedUserStatus->setType( $moduleType );
        $selectedUserStatus->setCareerKPAPercentage( 0 );
        $selectedUserStatus->setOrgKPAPercentage( 0 );
        $selectedUserStatus->setManKPAPercentage( 0 );

        $selectedUserStatus->save();

        /* reload from db, to get correct timestamps */
        $selectedUserStatus->loadFor( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      }

      $_SESSION[$moduleType."_activate_sess_status_" .getLoggedInUserId()] = $selectedUserStatus->getId();
  }

	$isManagedByLoggedInUser = false;

	if( $selectedUser != null )
	{
		$isManagedByLoggedInUser = $selectedUser->isManagedBy( getLoggedInUserId() ) ||
		                           getLoggedInUser()->getUserSetup()->getHRManager() == true ||
		                           getLoggedInUser()->isAssistSupportUser();
	}

?>
