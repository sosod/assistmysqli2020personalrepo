<h6>Personal Information</h6>
<? include("info_personal_info.php"); ?>

<? if( $useWeights ){ ?>
<h6>KPI Weight percentages</h6>
<div class="actions">
  <input type="button" value="Change percentages" onclick="redirect('<?=$moduleType?>_activate_setup_percentages.php')" />
</div>
<table class="info full" cellspacing="0">
  <tr>
    <? if( $moduleType == "perform" ){ ?>
    <th>Career KPI percentage:</th>
    <td><?= $selectedUserStatus->getCareerKPAPercentage() ?>%</td>
    <? } ?>
    <th>Organisational KPI percentage:</th>
    <td><?= $selectedUserStatus->getOrgKPAPercentage() ?>%</td>
    <th>Managerial KPI percentage:</th>
    <td><?= $selectedUserStatus->getManKPAPercentage() ?>%</td>
  </tr>
</table>
<? } ?>

<? if( $moduleType == "contract" ){ ?>
<h6>Evaluation panel members</h6>
<div class="actions">
  <input type="button" value="Add panel member" onclick="redirect('<?=$moduleType?>_activate_setup_panel_member.php')" />
</div>
<table class="list full" cellspacing="0">
  <tr>
    <th>Person</th>
    <th>Created by</th>
    <th>Created on</th>
    <th>Actions</th>
  </tr>
  <? foreach( $panelMembers as $panelMember ) {?>
  <tr>
    <td><?= $panelMember->getPanelTitle()->getName() ?></td>
    <td><?= $panelMember->getCreatedBy()->getFullName() ?></td>
    <td><?= $panelMember->getCreatedOn() ?></td>
    <td><input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_setup_panel_member.php', '<?= $panelMember->getId() ?>');" /></td>
  </tr>
  <? } ?>
</table>
<? } ?>