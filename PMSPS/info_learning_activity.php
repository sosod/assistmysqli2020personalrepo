<table class="info" cellspacing="0">
  <tr>
    <th>User:</th>
    <td><?= $learningActivity->getUser()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Evaluation year:</th>
    <td><?= $selectedYear->getName() ?></td>
  </tr>
  <tr>
    <th>Priority</th>
    <td><?= $learningActivity->getPriority() ?></td>
  </tr>
  <tr>
    <th>Name:</th>
    <td><?= $learningActivity->getName() ?></td>
  </tr>
  <tr>
    <th>Outcomes expected:</th>
    <td><?= $learningActivity->getOutcome() ?></td>
  </tr>
  <tr>
    <th>Suggested training:</th>
    <td><?= $learningActivity->getTraining() ?></td>
  </tr>
  <tr>
    <th>Mode of delivery:</th>
    <td><?= $learningActivity->getDeliveryMode() ?></td>
  </tr>
  <tr>
    <th>Suggested time frames:</th>
    <td><?= $learningActivity->getTimeFrames() ?></td>
  </tr>
  <tr>
    <th>Work opportunity:</th>
    <td><?= $learningActivity->getWorkOpportunity() ?></td>
  </tr>
  <tr>
    <th>Support person</th>
    <td><?= $learningActivity->getSupportPerson() ?></td>
  </tr>
  <tr>
    <th>Created by</th>
    <td><?= $learningActivity->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on</th>
    <td><?= $learningActivity->getCreatedOn() ?></td>
  </tr>
</table>