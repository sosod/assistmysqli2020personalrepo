<h5>Selected user, evaluation year and evaluation period:</h5>
<table class="info slim" cellspacing="0" style="margin-bottom:15px;">
  <tr>
    <th>User:</th>
    <td><?= $selectedUser->getFullName() ?></td>
    <th>Manager:</th>
    <td><?= $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ?></td>
  </tr>
  <tr>
    <th>Evaluation year:</th>
    <td><?= $selectedYear->getName() ?></td>
    <th>Evaluation Status:</th>
    <td>
      <?= $selectedPeriodEvalStatus->getStatusDisplay() ?>
      <?php if( $selectedPeriodEvalStatus->getStatus() == "completed" &&
                ( getLoggedInUser()->getUserSetup()->getHRManager() == true || getLoggedInUser()->isAssistSupportUser() ) ){ ?>
      <form id="frmResetStatus" method="post" onsubmit="return confirm('Are you sure you want to reset the status of this evaluation?')">
        <input type="submit" value="Reset Status" />
        <input type="hidden" name="action" value="reset_status" />
      </form>
      <?php }?>
    </td>
  </tr>  
  <tr>
    <th>Period start:</th>
    <td><?= getPeriodStartDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></td>        
    <th>Period end:</th>
    <td><?= getPeriodEndDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></td>
  </tr>
  <tr>
    <th>Print:</th>
    <td colspan="3">
      <input type="button" value="Evaluation" onclick="redirect('<?=$moduleType?>_evaluate_period_print.php?action=pdf');" />
    </td>        
  </tr>
</table>