<?php
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$comment = new ActivationComment();

if( $action == "save" )
{
  $comment->loadFromRequest( $_REQUEST );
  $comment->save();
  
  redirect($moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $comment->setId( $id );
  $comment->delete();
  
  redirect($moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $comment->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmComment").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Comment</h1>
    
    <form id="frmComment" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Message:</th>
          <td><textarea name="message" cols="50" rows="5" class="required"><?= $comment->getMessage() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?=$moduleType?>" />
      <input type="hidden" name="id" value="<?= $comment->getId() ?>" />       
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>  
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_activate.php');" />
    </div>  
  </body>
</html>