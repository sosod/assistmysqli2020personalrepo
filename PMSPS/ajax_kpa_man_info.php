<?php
include("inc_ignite.php");

$id = $_REQUEST["id"];
$moduleType = $_REQUEST["moduleType"];

$kpa = new KPAManagerial();
$kpa->loadFromId( $id );

$useWeights = getTypeCategory( $moduleType )->getUseWeights();

?>
<table class="info" cellspacing="0">
  <tr>
    <th>Core Managerial Skill:</th>
    <td><?= $kpa->getCoreManagerialSkill()->getSkill() ?></td>
  </tr>
  <tr>
    <th>Skill Definition:</th>
    <td><?= $kpa->getCoreManagerialSkill()->getDefinition() ?></td>
  </tr>
  <tr>
    <th>Skill Level:</th>
    <td><?=$kpa->getSkillLevel()?> - <?= KPAManagerial::displaySkillLevel( $kpa->getSkillLevel() ) ?></td>
  </tr>
  <tr>
    <th>Measurement:</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <tr>
    <th>Comments:</th>
    <td><?= $kpa->getComments()?></td>
  </tr>
  <? if( $useWeights ){ ?>
  <tr>
    <th>Weight:</th>
    <td><?= $kpa->getWeight() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Created by</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on</th>
    <td><?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>