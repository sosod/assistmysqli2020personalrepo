<?php
include("inc_ignite.php");
include("career_inc.php");
include("activate_inc.php");

$id = $_REQUEST["id"];

$kpa = new KPA();
$kpa->loadFromId( $id );
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: KPI</h1>
    
    <? include("info_kpa.php"); ?>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>  
  </body>
</html>