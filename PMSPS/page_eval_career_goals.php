<?

include("evaluate_inc.php");

$action = $_REQUEST["action"];
$careerGoals = getCareerGoals( $moduleType, $selectedUser->getId(), $selectedYear->getId() ); 

if( $action == "save" )
{
  foreach( $careerGoals as $careerGoal )
  {
    $careerGoalEvaluation = new CareerGoalEvaluation();
    $careerGoalEvaluation->loadFor( $moduleType, $careerGoal->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
    
    $careerGoalEvaluation->loadFromRequest( $_REQUEST );
    $careerGoalEvaluation->setCareerGoalId( $careerGoal->getId() );
    $careerGoalEvaluation->setCompleted( getSQLParam( $careerGoal->getId() . "_completed" ) );
    $careerGoalEvaluation->setComments( getSQLParam( $careerGoal->getId() . "_comments" ) );
    $careerGoalEvaluation->save();
  }  
  
  redirect( $moduleType."_evaluate_period.php" );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmCareerGoals").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Evaluation :: Career Goals</h1>
    
    <? include("info_eval_summary.php") ?>
    
    <form id="frmCareerGoals" method="post" >
      <table class="list" cellspacing="0">
        <tr>
          <th width="30%">Career Goal:</th>
          <th width="10%">Yes</th>
          <th width="10%">No</th>
          <th width="10%">Partial</th>                    
          <th width="40%">Comments:</th>
        </tr>
        <?
          foreach( $careerGoals as $careerGoal )
          {
            $careerGoalEvaluation = new CareerGoalEvaluation();
            $careerGoalEvaluation->loadFor( $moduleType, $careerGoal->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
        ?>
        <tr>
          <td><?= $careerGoal->getName() ?></td>
          <td align="center"><input type="radio" name="<?= $careerGoal->getId() ?>_completed" value="yes" <?= $careerGoalEvaluation->getCompleted() == "yes" ? "checked=\"checked\"" : ""?> /></td>
          <td align="center"><input type="radio" name="<?= $careerGoal->getId() ?>_completed" value="no" <?= $careerGoalEvaluation->isTransient() || $careerGoalEvaluation->getCompleted() == "no" ? "checked=\"checked\"" : ""?> /></td>
          <td align="center"><input type="radio" name="<?= $careerGoal->getId() ?>_completed" value="partial" <?= $careerGoalEvaluation->getCompleted() == "partial" ? "checked=\"checked\"" : ""?> /></td>
          <td><input type="text" name="<?= $careerGoal->getId() ?>_comments" value="<?= $careerGoalEvaluation->getComments() ?>" style="width:95%;"/></td>
        </tr>
        <?
          }
        ?>
        <tr class="info">
          <td colspan="5"><input type="submit" value="Evaluate" /></td>
        </tr>
      </table>
        
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?=$moduleType?>" />      
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="evalPeriod" value="<?= $selectedPeriod ?>" />      
      <input type="hidden" name="evaluatedById" value="<?= getLoggedInUserId() ?>" />
    </form>   
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_evaluate_period.php');" />
    </div>  
  </body>
</html>