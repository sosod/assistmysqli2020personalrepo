<?php
include("inc_ignite.php");

$evalYearId = $_REQUEST["evalYearId"];

$log = "";

if( $_POST["action"] == "save" )
{
  $selectedYear = $_POST["selectedYear"];

  //qualifications
  $log .= "<h3>Qualifications</h3>";

  $qualifications = getList( Qualification::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $qualifications as $record )
  {
    $qualification = new Qualification();
    $qualification->loadFromRecord( $record, true );
    $qualification->setId( null );
    $qualification->setEvalYearId( $evalYearId );
    $qualification->save();
  }

  $log .= "<p>" . count( $qualifications ) . " qualification records copied</p>";

  //experiences
  $log .= "<h3>Experience</h3>";

  $experiences = getList( Experience::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $experiences as $record )
  {
    $experience = new Experience();
    $experience->loadFromRecord( $record, true );
    $experience->setId( null );
    $experience->setEvalYearId( $evalYearId );
    $experience->save();
  }

  $log .= "<p>" . count( $experiences ) . " experience records copied</p>";

  //professional bodies
  $log .= "<h3>Professional Bodies</h3>";

  $professionalBodies = getList( ProfessionalBody::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $professionalBodies as $record )
  {
    $professionalBody = new ProfessionalBody();
    $professionalBody->loadFromRecord( $record, true );
    $professionalBody->setId( null );
    $professionalBody->setEvalYearId( $evalYearId );
    $professionalBody->save();
  }

  $log .= "<p>" . count( $professionalBodies ) . " professional body records copied</p>";

  //job functions
  $log .= "<h3>Job Functions</h3>";

  $jobFunctions = getList( JobFunction::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $jobFunctions as $record )
  {
    $jobFunction = new JobFunction();
    $jobFunction->loadFromRecord( $record, true );
    $jobFunction->setId( null );
    $jobFunction->setEvalYearId( $evalYearId );
    $jobFunction->save();
  }

  $log .= "<p>" . count( $jobFunctions ) . " job function records copied</p>";

  //career goals
  $log .= "<h3>Career Goals</h3>";

  $careerGoals = getList( CareerGoal::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $careerGoals as $record )
  {
    $careerGoal = new CareerGoal();
    $careerGoal->loadFromRecord( $record, true );
    $careerGoal->setId( null );
    $careerGoal->setEvalYearId( $evalYearId );
    $careerGoal->save();
  }

  $log .= "<p>" . count( $careerGoals ) . " career goals records copied</p>";

  //kpas
  $log .= "<h3>KPIs</h3>";

  $kpas = getList( KPA::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $kpas as $record )
  {
    $kpa = new KPA();
    $kpa->loadFromRecord( $record, true );
    $kpa->setId( null );
    $kpa->setEvalYearId( $evalYearId );
    $kpa->save();
  }

  $log .= "<p>" . count( $kpas ) . " kpa records copied</p>";

  //learning activities
  $log .= "<h3>Learning Activities</h3>";

  $learningActivities = getList( LearningActivity::$TABLE_NAME, "eval_year_id = " . $selectedYear );

  foreach( $learningActivities as $record )
  {
    $learningActivity = new LearningActivity();
    $learningActivity->loadFromRecord( $record, true );
    $learningActivity->setId( null );
    $learningActivity->setEvalYearId( $evalYearId );
    $learningActivity->save();
  }

  $log .= "<p>" . count( $learningActivities ) . " learning activity records copied</p>";
}

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Year Copy Data</h1>

    <?php if( $_POST["action"] == "save" ){
      echo $log;
    }else{?>

    <p>Choose an evaluation year below to copy from:</p>
    <form id="frmEvalYear" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>Evaluation year</th>
          <td><?= select( "selectedYear", getEvalYearSelectList(), $selectedYear, true ) ?> <input type="submit" value="Copy" /></td>
        </tr>
      </table>
      <input type="hidden" name="evalYearId" value="<?= $evalYearId ?>" />
      <input type="hidden" name="action" value="save" />
    </form>

    <?php }?>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_eval_years.php');"/>
    </div>
  </body>
</html>