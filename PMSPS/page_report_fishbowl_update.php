<?php
include("report_inc.php");

if( isset( $selectedYear ) == false && isset( $selectedPeriod ) == false )
{
  redirect($moduleType."_report_fishbowl.php");
}

$errors = array();

$action = $_REQUEST["action"];
$userId = $_REQUEST["userId"];

$evalStatus = new EvaluationStatus();
$evalStatus->loadFor( $moduleType, $userId, $selectedYear->getId(), $selectedPeriod );

if( $action == "save" )
{
  $finalKPASymbol = getSQLParam( "finalKPASymbol" );
  $comments = getSQLParam( "comments" );
    
  $evalStatus->updateFromReporting( $moduleType, $userId, $selectedYear->getId(), $selectedPeriod, $finalKPASymbol, $comments );
  
  redirect($moduleType."_report_fishbowl.php");
}    

$selectedUser = new User();
$selectedUser->loadFromId( $userId );

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Reporting :: Fishbowl Report :: Update</h1>
    
    <form id="frmCareerGoal" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Evaluation period:</th>
          <td><?= getPeriodStartDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?> to <?= getPeriodEndDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></td>
        </tr>
        <tr>
          <th>Final KPI Symbol: *</th>
          <td><?= select( "finalKPASymbol", getBellCurveSymbolSelectList(), $evalStatus->getFinalKPASymbol(), true ) ?></td>
        </tr>
        <tr>
          <th>Comments:</th>
          <td><textarea name="comments" cols="50" rows="5"><?= $evalStatus->getComments() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Update" /></td>
        </tr>
      </table>
        
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
    </form>  
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_report_fishbowl.php');" />
    </div>  
    
  </body>
</html>