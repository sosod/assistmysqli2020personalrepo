<?php
  $moduleName = "S57 Appointees";
  $moduleType = "contract";

  $maxFormalQualifications = 5;
  $maxInformalQualifications = 5;
  $maxExperiences = 5;
  $maxProfessionalBodies = 5;
  $maxLearningActivities = 5;
  $maxComments = 5;

  $useWeights = getTypeCategory( $moduleType )->getUseWeights();
?>