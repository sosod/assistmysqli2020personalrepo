<?php
include("inc_ignite.php");

$userId = $_REQUEST["userId"];
$action = $_REQUEST["action"];

$userAccess = new UserAccess();

if( $action == "save" )
{
  $userAccess->loadFromRequest( $_REQUEST );
  $userAccess->save();

  redirect("setup_user_access_info.php?userId=" . $userId );
}

if( exists( $userId ) )
{
  $userAccess->loadFromUserId( $userId );
  if( $userAccess->isTransient() )
  {
    //set the user id so that the lazy loading getUser() function can get the user info
    $userAccess->setUserId( $userId );  
  }
}

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmUserAccess").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - User Access</h1>
    
    <form id="frmUserAccess" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>User</th>
          <td><?= $userAccess->getUser()->getFullName() ?></td>
        </tr>
				<tr>
          <th>Sections</th>
          <td>
          <? foreach( UserAccess::$ACCESS_SECTIONS as $section => $description ){ ?>
	        <input type="checkbox" name="sections[]" value="<?= $section ?>" <?= arrayContains( $userAccess->getSections(), $section ) ? "checked=\"checked\"" : "" ?> /><?= $description ?><br/>
	        <? } ?>
          </td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="userId" value="<?= $userAccess->getUserId() ?>" />
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_user_access_info.php?userId=<?= $userId ?>');"/>
    </div>  
  </body>
</html>