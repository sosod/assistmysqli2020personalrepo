<?php
include("activate_inc.php");

$action = $_REQUEST["action"];
$kpaType = $_REQUEST["kpaType"];
$messages = array();

if( $action == "upload" )
{
  $target = "../files/" . getCompanyCode() . "/" . date("YmdHis") . "_";
  $target = $target . basename( $_FILES['importFile']['name']) ;

  if( move_uploaded_file( $_FILES['importFile']['tmp_name'], $target ) )
  {
    $messages[] = "The file ". basename( $_FILES['importFile']['name']). " has been successfully uploaded";

    $file_handle = fopen( $target, "r" );

    $cnt = 0;
    $doneWithHeader = false;

    while( !feof($file_handle) )
    {
      $record = fgetcsv($file_handle);

      if( $doneWithHeader == false )
      {
        $doneWithHeader = true;
        continue;
      }

      if( $record[0] != "" )
      {
        $kpa = new KPA();

        $kpa->setType( $moduleType );
        $kpa->setUserId( $selectedUser->getId() );
        $kpa->setEvalYearId( $selectedYear->getId() );
        $kpa->setCreatedById( getLoggedInUserId() );
        $kpa->setCreatedOn( getdate() );
        $kpa->setKPAType( $kpaType );

        $kpa->loadFromImport( $record );

        $kpa->save();

        $messages[] = "KPI, " . $kpa->getName() . ", processed.";

        $cnt++;
      }
    }

    $messages[] = $cnt . " KPIs processed in total.";

    fclose($file_handle);
  }
  else
  {
    $messages[] = "Sorry, there was a problem uploading your file.";
  }
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: KPI Import</h1>

    <h6>Import file requirements:</h6>
    <ul>
      <li>The first line in the file will be treated as the header row and will not be imported.</li>
      <li>The data needs to be in CSV format seperated by commas.</li>
      <li>The data needs to be in the following order: SDBIP KPI #, Name, Description, Measurement, Outcome, Evidence</li>
    </ul>

    <h5>Choose import file:</h5>
    <form enctype="multipart/form-data" action="" method="post">
      <input name="importFile" type="file" />
      <input type="submit" value="Submit" />
      <input type="hidden" name="action" value="upload" />
      <input type="hidden" name=kpaType" value="<?= $kpaType ?>" />
    </form>

    <?= printMessages( $messages ) ?>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>
  </body>
</html>
