<?
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];
$professionalBody = new ProfessionalBody();

if( $action == "save" )
{
  $professionalBody->loadFromRequest( $_REQUEST );
  $professionalBody->save();

  redirect( $moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $professionalBody->setId( $id );
  $professionalBody->delete();

  redirect( $moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $professionalBody->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmProfessionalBody").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Experience</h1>

    <form id="frmProfessionalBody" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Institution:</th>
          <td><input type="text" name="name" value="<?= $professionalBody->getName() ?>" class="required" maxlength="100"/></td>
        </tr>
        <tr>
          <th>Member Since:</th>
          <td><input type="text" name="memberSince" value="<?= $professionalBody->getMemberSince() ?>" readonly="readonly" class="required date-input"/></td>
        </tr>
        <tr>
          <th>Membership Level:</th>
          <td><input type="text" name="memberLevel" value="<?= $professionalBody->getMemberLevel() ?>" class="required" maxlength="100"/></td>
        </tr>
        <tr>
          <th>Description:</th>
          <td><textarea name="description" cols="50" rows="5"><?= $professionalBody->getDescription() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />
      <input type="hidden" name="id" value="<?= $professionalBody->getId() ?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>
  </body>
</html>