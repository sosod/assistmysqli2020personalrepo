<?php

$CONST_TABLE_PREFIX = "assist_${cmpcode}_pmsps";
/********** HELPER FUNCTIONS **********/
function setMessage( $msg )
{
  $_SESSION["msg"] = $msg;
}

function dbdebug($msg)
{
  //$fd = fopen("/opt/log/debug.log", "a");
  //$str = "[" . date("Y/m/d h:i:s", mktime()) . "] [" . $_SERVER['PHP_SELF'] . ":] " . $msg;
  //fwrite($fd, $str . "\n");
  //fclose($fd);
}

function nullIfEmpty( $value )
{
  if( isset( $value ) && strlen( trim( $value ) ) != 0 )
  {
     return $value;
  }

  return "NULL";
}

$loggedInUser = null;

function getLoggedInUser()
{
  global $loggedInUser;
  if( $loggedInUser == null )
  {
    $loggedInUser = new User();
    $loggedInUser->loadFromId( getLoggedInUserId() );
  }

  return $loggedInUser;
}

/********** CORE DB FUNCTIONS **********/
$conn = null;

function getConnection()
{
  global $conn;

  if( isset( $conn ) == false )
  {
    global $cmpcode;
    global $tkid;
    global $tkname;

    $conn = mysql_connect("localhost","ignittps_ignite4","ign92054u");
    if(!$conn)
    {
    	$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on sql: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
    	$fileloc = $_SERVER['PHP_SELF'];
      $to = "actionit.actionassist@gmail.com";
      $from = "no-reply@ignite4u.co.za";
      $subject = "Ignite Assist CON Error";
      $message = "<font face=arial size=2><p><b>Company:</b> ".$cmpcode."<br><b>TKID:</b> ".$tkid."<br><b>TK Name:</b> ".$tkname."</p>";
      $message .= "<p><b>File:</b> ".$fileloc."</p>";
    	$message .= $sqlerror;
      $message .= "</center></font>";
      $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
      mail($to,$subject,$message,$header);
    	die("<h1>Error getConnection()</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Advisory Services at <a href=mailto:assist@ignite4u.co.za>assist@ignite4u.co.za</a> with the error details.<br>Thank you.</p><p>");
    }

    $db = "ignittps_i".strtolower($cmpcode);
    mysql_select_db($db,$conn);
  }

  return $conn;
}

function getResultSet( $sql )
{
  global $cmpcode;
  global $tkid;
  global $tkname;

  $con = getConnection();

  $rs = mysql_query($sql,$con);

  if(!$rs)
  {
/*  	$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on sql: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
  	$fileloc = $_SERVER['PHP_SELF'];
    $to = "assist@ignite4u.co.za";
    $from = "assist@ignite4u.co.za";
    $subject = "Ignite Assist SQL50 Error";
    $message = "<font face=arial size=2><p><b>Company:</b> ".$cmpcode."<br><b>TKID:</b> ".$tkid."<br><b>TK Name:</b> ".$tkname."</p>";
    $message .= "<p><b>File:</b> ".$fileloc."</p>";
  	$message .= $sqlerror;
    $message .= "</center></font>";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail($to,$subject,$message,$header);*/
		$sqlerror = "<p>&nbsp;</p>
		<p><b>---ERROR DETAILS BEGIN---</b></p>
		<ul><li>Database error: ".mysql_error()."</li>
		<li>".$sql."</li>
		<li>".$cmpcode."</li>
		<li>Page: ".$_SERVER['PHP_SELF']."</li><li>Name: ".$_SERVER['SERVER_NAME']."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
		$sqlerror.="<p>--- SESSION DETAILS ---";
		$sqlerror.="<p>".serialize($_SESSION);
		/*foreach($_SESSION as $s => $v) {
			if(!is_array($v)) {
				$sqlerror.="<br />".$s." :: ".$v;
			} else {
				$sqlerror.="<br />".$s." :: ".implode("-",$v);
			}
		}*/
		$sqlerror.="<p>--- REQUEST DETAILS ---";
		foreach($_REQUEST as $r => $v) {
			$sqlerror.="<br />".$r." :: ".$v;
		}
		$sqlerror.="<p>".serialize($_REQUEST);
		$sqlerror.="<p>--- OTHER DETAILS ---";
			$sqlerror.="<br />HTTP USER AGENT :: ".$_SERVER['HTTP_USER_AGENT'];

		/*foreach($_SERVER['argv'] as $r => $v) {
			$sqlerror.="<br />".$r." :: ".$v;
		}*/
		$sqlerror.="<p>".serialize($_SERVER);
		$sqlerror.="<br />--- END ---</p>";
//		mail($this->db_error_to,"DB ".self::SITE_SRC." Error",$sqlerror,"From: ".$this->db_error_from.(strlen($this->db_error_cc)>0 ? "\r\nCC: ".$this->db_error_cc : "")."\r\nContent-type: text/html; charset=us-ascii");
		$src_path = explode("/",$_SERVER["REQUEST_URI"]);
		$path = "";
		if(count($src_path)<=2) {
		} else {
			$c = count($src_path)-2;
			for($i=1;$i<=$c;$i++) { $path.="../"; }
		}
		$path .= "logs/".date("Ymd_His")."_pmsps_sql_error_log.html";
	        $file = fopen($path,"w");
       	 fwrite($file,$sqlerror."\n");
	        fclose($file);

  	die("<h1>Error getResultSet()</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Assist notifying them of the error.<br>Thank you.</p><p>");
  }

  return $rs;
}

function updateRecord( $sql )
{
  return getResultSet( $sql );
}

function insertRecord( $sql )
{
  getResultSet( $sql );

  return mysql_insert_id();
}

function getRecord( $rs )
{
  return stripslashes_deep( mysql_fetch_array($rs) );
}

function stripslashes_deep($value)
{
  $value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);

  return $value;
}

function logTransaction( $message, $sql, $old = "" )
{
  global $cmpcode;
  global $tkid;
  global $tkname;
  global $today;

  $ref = "PMSPS";
  $sql = str_replace("'","|",$sql);
  $message = str_replace("'","&#39",$message);
  $old = str_replace("'","&#39",$old);

  $sqlLog = "INSERT INTO assist_".$cmpcode."_log SET date = '".$today."', tkid = '".$tkid."', ref = '".$ref."', transaction = '".$message."', tsql = '".$sql."', told = '".$old."'";

  insertRecord( $sqlLog );
}

/********** LIST FUNCTIONS **********/
function getListRecords( $sql )
{
  $rs = getResultSet( $sql );

  $records = array();
  while($record = getRecord($rs) )
  {
    $records[] = $record;
  }

  return $records;
}

function getList( $table, $restrict = false, $order_by = "id ASC" )
{
  global $CONST_TABLE_PREFIX;

  $sql = "SELECT * FROM ${CONST_TABLE_PREFIX}_${table} " . ( $restrict ? "WHERE " . $restrict : "" ) . " ORDER BY $order_by";

  debug( $sql );

  return getListRecords( $sql );
}

function getEvalYears()
{
  $records = getList( EvalYear::$TABLE_NAME );

  $results = array();

  foreach( $records as $record )
  {
    $evalYear = new EvalYear();
    $evalYear->loadFromRecord( $record );

    $results[] = $evalYear;
  }

  return $results;
}

function getPanelTitles()
{
  $records = getList( PanelTitle::$TABLE_NAME );

  $results = array();

  foreach( $records as $record )
  {
    $panelTitle = new PanelTitle();
    $panelTitle->loadFromRecord( $record );

    $results[] = $panelTitle;
  }

  return $results;
}

function getCategories()
{
  $records = getList( Category::$TABLE_NAME );

  $results = array();

  foreach( $records as $record )
  {
    $category = new Category();
    $category->loadFromRecord( $record );

    $results[] = $category;
  }

  return $results;
}

function getBonusPolicies( $evalYearId )
{
  $records = getList( BonusPolicy::$TABLE_NAME, "eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $bonusPolicy = new BonusPolicy();
    $bonusPolicy->loadFromRecord( $record );

    $results[] = $bonusPolicy;
  }

  return $results;
}

function getCategoryUsers( $categoryId )
{
	global $CONST_TABLE_PREFIX;
	global $cmpcode;

	$sql = "SELECT us.* FROM assist_" . $cmpcode . "_timekeep tk, " .$CONST_TABLE_PREFIX . "_" . UserSetup::$TABLE_NAME . " us, assist_".$cmpcode."_menu_modules_users mmu WHERE tk.tkid = mmu.usrtkid AND mmu.usrmodref = 'PMSPS' AND tk.tkid = us.user_id AND us.category_id = " . $categoryId . " ORDER BY tk.tkname ";

  $records = getListRecords( $sql );

  $results = array();

  foreach( $records as $record )
  {
    $user = new User();
    $user->loadFromId( $record["user_id"] );

    $results[] = $user;
  }

  return $results;
}

function getUsersWithPermission( $field )
{
  $records = getList( UserSetup::$TABLE_NAME, $field . " = true" );

  $results = array();

  foreach( $records as $record )
  {
    $user = new User();
    $user->loadFromId( $record["user_id"] );

    $results[] = $user;
  }

  return $results;
}

function getModuleUsers()
{
  $sql = "SELECT * FROM assist_".getCompanyCode()."_timekeep tk, assist_".getCompanyCode()."_menu_modules_users mu WHERE tk.tkid = mu.usrtkid AND mu.usrmodref = 'PMSPS' ORDER BY tk.tkname, tk.tksurname";
  $rs = getResultSet( $sql );

  $results = array();

  while($record = getRecord($rs) )
  {
    $user = new User();
    $user->loadFromRecord( $record );

    $results[] = $user;
  }

  return $results;
}

function getUsersWithSetup()
{
  $users = getModuleUsers();

  $usersWithSetup = array();

  foreach( $users as $user )
  {
    $userWithSetup[0] = $user;

    $userSetup = new UserSetup();
    $userSetup->loadFromUserId( $user->getId() );

    $userWithSetup[1] = $userSetup;

    $usersWithSetup[] = $userWithSetup;
  }

  return $usersWithSetup;
}

function getActivationComments( $type, $userId, $evalYearId )
{
  $records = getList( ActivationComment::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $comment = new ActivationComment();
    $comment->loadFromRecord( $record );

    $results[] = $comment;
  }

  return $results;
}

function getEvaluationComments( $type, $userId, $evalYearId, $evalPeriod )
{
  $records = getList( EvaluationComment::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId . " AND eval_period = " . $evalPeriod );

  $results = array();

  foreach( $records as $record )
  {
    $comment = new EvaluationComment();
    $comment->loadFromRecord( $record );

    $results[] = $comment;
  }

  return $results;
}

function getEvaluationSkills( $type, $userId, $evalYearId, $evalPeriod, $skillType )
{
  $records = getList( EvaluationSkill::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId . " AND eval_period = " . $evalPeriod . " AND skill_type = '" . $skillType . "'" );

  $results = array();

  foreach( $records as $record )
  {
    $skill = new EvaluationSkill();
    $skill->loadFromRecord( $record );

    $results[] = $skill;
  }

  return $results;
}

function getQualifications( $type, $userId, $evalYearId, $primaryType )
{
  $records = getList( Qualification::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId . " AND primary_type = '" . $primaryType . "'" );

  $results = array();

  foreach( $records as $record )
  {
    $qualification = new Qualification();
    $qualification->loadFromRecord( $record );

    $results[] = $qualification;
  }

  return $results;
}

function getExperiences( $type, $userId, $evalYearId )
{
  $records = getList( Experience::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $experience = new Experience();
    $experience->loadFromRecord( $record );

    $results[] = $experience;
  }

  return $results;
}

function getProfessionalBodies( $type, $userId, $evalYearId )
{
  $records = getList( ProfessionalBody::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $professionalBody = new ProfessionalBody();
    $professionalBody->loadFromRecord( $record );

    $results[] = $professionalBody;
  }

  return $results;
}

function getPanelMembers( $type, $userId, $evalYearId )
{
  $records = getList( PanelMember::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $panelMember = new PanelMember();
    $panelMember->loadFromRecord( $record );

    $results[] = $panelMember;
  }

  return $results;
}

function getJobFunctions( $type, $userId, $evalYearId )
{
  $records = getList( JobFunction::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $jobFunction = new JobFunction();
    $jobFunction->loadFromRecord( $record );

    $results[] = $jobFunction;
  }

  return $results;
}

function getCareerGoals( $type, $userId, $evalYearId )
{
  $records = getList( CareerGoal::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $careerGoal = new CareerGoal();
    $careerGoal->loadFromRecord( $record );

    $results[] = $careerGoal;
  }

  return $results;
}

function getBonusKPA( $type, $userId, $evalYearId )
{
  $restrict = "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId . " AND kpa_type = 'bonus'";

  $records = getList( KPA::$TABLE_NAME, $restrict );

  if( count( $records ) > 0 )
  {
    $kpa = new KPA();
    $kpa->loadFromRecord( $records[0] );

    return $kpa;
  }
}

function createBonusKPA( $type, $userId, $evalYearId )
{
  $kpa = new KPA();
  $kpa->setType( $type );
  $kpa->setUserId( $userId );
  $kpa->setEvalYearId( $evalYearId );
  $kpa->setCreatedById( getLoggedInUserId() );
  $kpa->setKPAType( "bonus" );
  $kpa->setName( "Additional Work" );
  $kpa->setDescription( "Additional work completed" );
  $kpa->setMeasurement( "" );
  $kpa->setComments( "" );
  $kpa->setOutcome( "" );
  $kpa->setEvidence( "" );
  $kpa->setWeight( 5 );

  $kpa->save();

  return $kpa;
}

function getKPAs( $type, $userId, $evalYearId, $kpaType = null )
{
  $restrict = "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId . ( isset($kpaType) ? " AND kpa_type = '" . $kpaType . "'" : "" );

  $records = getList( KPA::$TABLE_NAME, $restrict );

  $results = array();

  foreach( $records as $record )
  {
    $kpa = new KPA();
    $kpa->loadFromRecord( $record );

    $results[] = $kpa;
  }

  return $results;
}

function getOrganisationalKPAs( $type, $userId, $evalYearId )
{
  $restrict = "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId;
  $records = getList( KPAOrganisational::$TABLE_NAME, $restrict, "group_num ASC, weight DESC, sdbip_num ASC " );
  $results = array();
  foreach( $records as $record )
  {
    $kpa = new KPAOrganisational();
    $kpa->loadFromRecord( $record );

    $results[] = $kpa;
  }

  return $results;
}

function getMaxOrgKPAGroupNumber( $type, $userId, $evalYearId )
{
  global $CONST_TABLE_PREFIX;

  $sql = "SELECT max(group_num) FROM ${CONST_TABLE_PREFIX}_" . KPAOrganisational::$TABLE_NAME . " WHERE type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId;

  $rs = getResultSet( $sql );
  $record = getRecord( $rs );

  if( isset( $record ) == false || isset( $record[0] ) == false )
  {
    return 0;
  }

  return $record[0];
}

function getManagerialKPAs( $type, $userId, $evalYearId )
{
  $restrict = "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId;

  $records = getList( KPAManagerial::$TABLE_NAME, $restrict );

  $results = array();

  foreach( $records as $record )
  {
    $kpa = new KPAManagerial();
    $kpa->loadFromRecord( $record );

    $results[] = $kpa;
  }

  return $results;
}

function getLearningActivities( $type, $userId, $evalYearId )
{
  $records = getList( LearningActivity::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $learningActivity = new LearningActivity();
    $learningActivity->loadFromRecord( $record );

    $results[] = $learningActivity;
  }

  return $results;
}

function getLoggedInUserAccessSections()
{
  $userAccess = new UserAccess();
  $userAccess->loadFromUserId( getLoggedInUserId() );

  return $userAccess->getSections();
}

function getEvalPeriodStatusses( $type, $userId, $evalYearId )
{
  $records = getList( EvaluationStatus::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );

  $results = array();

  foreach( $records as $record )
  {
    $evalStatus = new EvaluationStatus();
    $evalStatus->loadFromRecord( $record );

    $results[] = $evalStatus;
  }

  return $results;
}

function getManagersUsers( $moduleType, $managerId )
{
  $sql = "SELECT * FROM assist_".getCompanyCode()."_timekeep, assist_".getCompanyCode()."_menu_modules_users, assist_".getCompanyCode()."_pmsps_user_setup us WHERE tkid = usrtkid AND usrmodref = 'PMSPS' AND ( tkmanager = '" . $managerId . "' OR ( tkmanager = 'S' AND tkid = '".getLoggedInUserId()."' ) ) AND us.user_id = tkid AND us.category_id = " . getTypeCategoryId( $moduleType );
  $rs = getResultSet( $sql );

  $results = array();

  while($record = getRecord($rs) )
  {
    $user = new User();
    $user->loadFromRecord( $record );

    $results[] = $user;
  }

  return $results;
}

function getDepartments( $departmentId = false )
{
  return getListRecords( "SELECT * FROM assist_" . getCompanyCode() . "_list_dept WHERE yn = 'Y' " . ( $departmentId ? " AND id = " . $departmentId : "" ) );
}

function getCoreManagerialSkills()
{
  $records = getList( CoreManagerialSkill::$TABLE_NAME, "active = 1" );

  $results = array();

  foreach( $records as $record )
  {
    $coreManagerialSkill = new CoreManagerialSkill();
    $coreManagerialSkill->loadFromRecord( $record );

    $results[] = $coreManagerialSkill;
  }

  return $results;
}
/*****************************************************************************/
/****************************** SELECT LISTS *********************************/
/*****************************************************************************/
function getEvalPeriodTypeSelectList()
{
  $list = array();

  foreach( Category::$EVAL_PERIOD_TYPES as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getCategorySelectList()
{
  $list = array();

  foreach( getCategories() as $category )
  {
    $list[] = array( $category->getId(), $category->getName() );
  }

  return $list;
}

function getPanelTitleSelectList()
{
  $list = array();

  foreach( getPanelTitles() as $panelTitle )
  {
    $list[] = array( $panelTitle->getId(), $panelTitle->getName() );
  }

  return $list;
}

function getYesNoSelectList()
{
  $list = array();

  $list[] = array( "0", "No" );
  $list[] = array( "1", "Yes" );

  return $list;
}

function getJobLevelSelectList()
{
  $list = array();

	for( $i = 0; $i <= 20; $i++ )
  {
    $list[] = array( "$i", "$i" );
  }

  $list[] = array( "C", "C" );

  return $list;
}

function getNumericSelectList( $low=0, $high=100 )
{
  $list = array();

  for( $i = $low; $i <= $high; $i++ )
  {
    $list[] = array( "$i", "$i" );
  }

  return $list;
}

function getSectionSelectList()
{
  $list = array();

  foreach( UserAccess::$ACCESS_SECTIONS as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getTypeUserSelectList( $type )
{
  $list = array();

  foreach( getCategoryUsers( getTypeCategory( $type )->getId() ) as $user )
  {
    $list[] = array( $user->getId(), $user->getFullName() );
  }

  return $list;
}

function getSupportPersonSelectList( $type )
{
  $list = array();

  foreach( getListRecords( "SELECT support_person FROM assist_" . getCompanyCode() . "_pmsps_" . LearningActivity::$TABLE_NAME . " WHERE type = '" . $type . "' GROUP BY support_person" ) as $record )
  {
    $list[] = array( $record["support_person"], $record["support_person"] );
  }

  return $list;
}

/*function getSupportPersonSelectList()
{
	return getManagerSelectList();

  $list = array();

  foreach( getUsersWithPermission( "approve_permission" ) as $user )
  {
    $list[] = array( $user->getId(), $user->getFullName() );
  }

  return $list;
}*/

function getEvalYearSelectList()
{
  $list = array();

  foreach( getEvalYears() as $evalYear )
  {
    $list[] = array( $evalYear->getId(), $evalYear->getName() );
  }

  return $list;
}

function getKPATypeSelectList()
{
  $list = array();

  foreach( KPA::$KPA_TYPES as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getCompletedSelectList()
{
  $list = array();

  foreach( CareerGoalEvaluation::$COMPLETED_OPTIONS as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getRatingSelectList()
{
  $list = array();

  foreach( KPAEvaluation::$RATING_OPTIONS as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getBellCurveSymbolSelectList()
{
  $list = array();

  foreach( KPAEvaluation::$BELL_CURVE_SYMBOLS as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getManagersUsersSelectList( $moduleType, $managerId )
{
  $list = array();

  foreach( getManagersUsers( $moduleType, $managerId ) as $user )
  {
    $list[] = array( $user->getId(), $user->getFullName() );
  }

  return $list;
}

function getTypePeriodSelectList( $type )
{
  $list = array();

  $category = getTypeCategory( $type );

  if( $category->getEvalPeriodType() == "quart" || $category->getEvalPeriodType() == "quart_oral" )
  {
    $numOfPeriods = 4;
  }
  else if( $category->getEvalPeriodType() == "bi_annual" )
  {
    $numOfPeriods = 2;
  }

  for( $i = 1; $i <= $numOfPeriods; $i++ )
  {
    $list[] = array( $i, "Period " . $i );
  }

  return $list;
}

function getCareerPeriodSelectList()
{
  return getTypePeriodSelectList( "career" );
}

function getFishbowlOrderBySelectList()
{
  $list = array();

  $list[] = array( "tkname", "User" );
  $list[] = array( "job_level", "Job level" );
  $list[] = array( "department", "Department" );
  $list[] = array( "section", "Section" );
  $list[] = array( "tkmanager", "Manager" );

  return $list;
}

function getLearningOrderBySelectList()
{
  $list = array();

  $list[] = array( "tkname", "User" );
  $list[] = array( "job_level", "Job level" );
  $list[] = array( "department", "Department" );
  $list[] = array( "section", "Section" );
  $list[] = array( "tkmanager", "Manager" );
  $list[] = array( "priority", "Priority" );
  $list[] = array( "status", "Status" );

  return $list;
}

function getActivationStatusReportOrderBySelectList()
{
  $list = array();

  $list[] = array( "tk.tkname", "User" );
  $list[] = array( "department", "Department" );
  $list[] = array( "tk.tkmanager", "Manager" );
	$list[] = array( "us.section", "Section" );

  return $list;
}

function getOrderByDirectionSelectList()
{
  $list = array();

  $list[] = array( "ASC", "Ascending" );
  $list[] = array( "DESC", "Descending" );

  return $list;
}

function getDepartmentSelectList()
{
  $list = array();

  foreach( getListRecords( "SELECT * FROM assist_" . getCompanyCode() . "_list_dept WHERE yn = 'Y'" ) as $record )
  {
    $list[] = array( $record["id"], $record["value"] );
  }

  return $list;
}

function getUserSectionSelectList()
{
  $list = array();

  foreach( getListRecords( "SELECT section FROM assist_" . getCompanyCode() . "_pmsps_user_setup GROUP BY section" ) as $record )
  {
    $list[] = array( $record["section"], $record["section"] );
  }

  return $list;
}

function getManagerSelectList()
{
  $list = array();

	foreach( getListRecords( "SELECT tkmanager FROM assist_".getCompanyCode()."_timekeep, assist_".getCompanyCode()."_menu_modules_users WHERE tkid = usrtkid AND usrmodref = 'PMSPS' AND tkmanager != 'S' group by tkmanager " ) as $record )
	{
	  $managerIds[] = $record["tkmanager"];
	}

	$managerIdsSql = "'".join("','", $managerIds)."'";

  foreach( getListRecords( "SELECT tkid, tkname, tksurname FROM assist_".getCompanyCode()."_timekeep WHERE tkid in (" . $managerIdsSql . ")" ) as $record )
  {
    $list[] = array( $record["tkid"], $record["tkname"] . " " . $record["tksurname"] );
  }

  return $list;
}

function getLearningActivityStatusSelectList()
{
  $list = array();

  foreach( LearningActivity::$STATUSSES as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getUniqueLearningActivitySelectList( $type )
{
  $list = array();

  foreach( getListRecords( "SELECT name FROM assist_" . getCompanyCode() . "_pmsps_" . LearningActivity::$TABLE_NAME . " WHERE type = '" . $type . "' GROUP BY name" ) as $record )
  {
    $list[] = array( $record["name"], $record["name"] );
  }

  return $list;
}

function getCoreManagerialSkillSelectList()
{
  $list = array();

  foreach( getCoreManagerialSkills() as $skill )
  {
    $list[] = array( $skill->getId(), $skill->getSkill() );
  }

  return $list;
}

function getSkillLevelSelectList()
{
  $list = array();

  foreach( KPAManagerial::$SKILL_LEVELS as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getQualificationSecondaryTypeSelectList()
{
  $list = array();

  foreach( Qualification::$SECONDARY_TYPES as $key => $description )
  {
    $list[] = array( $key, $description );
  }

  return $list;
}

function getExperienceYearsList( $low=0, $high=50 )
{
  $list = array();

  for( $i = $low; $i <= $high; $i += 0.5 )
  {
    $list[] = array( "$i", "$i" );
  }

  return $list;
}
/* *************************************************************************************** */
function getTypeCategory( $type )
{
  if( $type == "career" )
  {
    return getCareerCategory();
  }
  else if( $type == "perform" )
  {
    return getPerformCategory();
  }
  else if( $type == "contract" )
  {
    return getContractCategory();
  }
}

function getTypeCategoryId( $type )
{
  if( $type == "career" )
  {
    return 3;
  }
  else if( $type == "perform" )
  {
    return 2;
  }
  else if( $type == "contract" )
  {
    return 1;
  }
}

function getTypeFromCategoryId( $categoryId )
{
  if( $categoryId == 3 )
  {
    return "career";
  }
  else if( $categoryId == 2 )
  {
    return "perform";
  }
  else if( $categoryId == 1 )
  {
    return "contract";
  }
}

$careerCategory = null;
function getCareerCategory()
{
  global $careerCategory;

  if( $careerCategory == null )
  {
    $careerCategory = new Category();
    $careerCategory->loadFromId( 3 ); //id 3 = staff category
  }

  return $careerCategory;
}

function createEvalPeriodStatusses( $type, $evalPeriodType, $userId, $evalYearId, $existingStatusses=null )
{
  $statusses = array();

  if( $evalPeriodType == "quart" || $evalPeriodType == "quart_oral" )
  {
    $numOfPeriods = 4;
  }
  else if( $evalPeriodType == "bi_annual" )
  {
    $numOfPeriods = 2;
  }

  for( $i = 1; $i <= $numOfPeriods; $i++ )
  {
    $statusExist = false;

    if( isset($existingStatusses) )
    {
      foreach( $existingStatusses as $status )
      {
        if( $status->getEvalPeriod() == $i )
        {
          $statusses[] = $status;
          $statusExist = true;

          break;
        }
      }
    }

    if( $statusExist == false )
    {
      $evalPeriodStatus = new EvaluationStatus();
      $evalPeriodStatus->setType( $type );
      $evalPeriodStatus->setUserId( $userId );
      $evalPeriodStatus->setEvalYearId( $evalYearId );
      $evalPeriodStatus->setEvalPeriod( $i );
      $evalPeriodStatus->setEvalPeriodReviewType( $evalPeriodType == "quart_oral" ?  ( $i == 1 || $i == 3 ? "oral" : "formal" ) : "formal" );
      $evalPeriodStatus->setActionById( getLoggedInUserId() );
      $evalPeriodStatus->setStatus( "pending" );
      $evalPeriodStatus->setLocked( 0 );
      $evalPeriodStatus->save();

      $statusses[] = $evalPeriodStatus;
    }
  }

  return $statusses;
}

$performCategory = null;
function getPerformCategory()
{
  global $performCategory;

  if( $performCategory == null )
  {
    $performCategory = new Category();
    $performCategory->loadFromId( 2 ); //id 2 = managers category
  }

  return $performCategory;
}

$contractCategory = null;
function getContractCategory()
{
  global $contractCategory;

  if( $contractCategory == null )
  {
    $contractCategory = new Category();
    $contractCategory->loadFromId( 1 ); //id 1 = s57 category
  }

  return $contractCategory;
}
?>
