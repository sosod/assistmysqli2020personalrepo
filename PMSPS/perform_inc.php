<?php
  $moduleName = "Performance Agreement";
  $moduleType = "perform";

  $maxFormalQualifications = 5;
  $maxInformalQualifications = 15;
  $maxExperiences = 5;
  $maxProfessionalBodies = 5;
  $maxJobFunctions = 10;
  $maxCareerGoals = 3;
  $maxCareerKPAs = 3;
  $maxLearningActivities = 10;
  $maxComments = 5;

  $useWeights = getTypeCategory( $moduleType )->getUseWeights();
?>