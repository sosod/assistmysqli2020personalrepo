<?php
include("inc_ignite.php");


$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$category = new Category();

if( $action == "save" )
{
  $category->loadFromRequest( $_REQUEST );
  $category->save();  
  
  redirect("setup_categories.php");
}

if( exists( $id ) )
{
  $category->loadFromId( $id );
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmCategory").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Category</h1>
    
    <form id="frmCategory" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>Name</th>
          <td><input type="text" name="name" value="<?= $category->getName() ?>" readonly="readonly" class="required"/></td>
        </tr>
        <tr>
          <th>Evaluation period type</th>
          <td><?= select( "evalPeriodType", getEvalPeriodTypeSelectList(), $category->getEvalPeriodType(), true ) ?></td>
        </tr>
        <tr>
          <th>Use Weights</th>
          <td><?= select( "useWeights", getYesNoSelectList(), $category->getUseWeights(), true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $id ?>" />
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_categories.php');" />
    </div>  
  </body>
</html>