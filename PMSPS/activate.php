<?php
include("inc_ignite.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Activation</h1>
    
    <h5>Choose a section below</h5>
    <div class="option-group">
      <? 
        $userAccessSections = getLoggedInUserAccessSections();
        
        if( arrayContains( $userAccessSections, "activate_career" ) ){ 
      ?>
      <div class="option">
        <h2>Performance Development</h2>
        <p>Define the performance development KPIs, learning activities and goals</p>
        <input type="button" value="Select" onclick="redirect('career_activate.php')" />
      </div>
      <? } 
         if( arrayContains( $userAccessSections, "activate_perform" ) ){ 
      ?>      
      <div class="option">
        <h2>Performance Agreement</h2>
        <p>Define the performance agreement KPI targets, learning activities and goals</p>
        <input type="button" value="Select" onclick="redirect('perform_activate.php')" />
      </div>
      <? } 
      if( arrayContains( $userAccessSections, "activate_contract" ) ){ 
      ?>      
      <div class="option">
        <h2>S57 Appointees</h2>
        <p>Define the S57 appointees KPI targets, learning activities and goals</p>
        <input type="button" value="Select" onclick="redirect('contract_activate.php')" />
      </div>
      <? } 
      ?>
    </div>
  </body>
</html>