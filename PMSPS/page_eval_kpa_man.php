<?php
include("evaluate_inc.php");

$kpaId = $_REQUEST["kpaId"];
$action = $_REQUEST["action"];

$kpa = new KPAManagerial();
$kpa->loadFromId( $kpaId );

$kpaEvaluation = new KPAEvaluation();

if( $action == "save" )
{
  $kpaEvaluation->loadFromRequest( $_REQUEST );
  $kpaEvaluation->save();

  redirect( $moduleType."_evaluate_period.php" );
}

$kpaEvaluation->loadForMan( $moduleType, $kpaId, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmKPA").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Evaluation :: Managerial KPI</h1>
    <form id="frmKPA" method="post" >

      <? include("info_kpa_man.php"); ?>

      <table class="form" cellspacing="0">
        <tr>
          <th>Rating:</th>
          <td><?= select( "rating", getRatingSelectList(), $kpaEvaluation->getRating(), false ) ?></td>
        </tr>
				<tr>
          <th>Self Evaluation:</th>
          <td><?= select( "selfEvaluation", getRatingSelectList(), $kpaEvaluation->getSelfEvaluation(), false ) ?></td>
        </tr>
        <tr>
          <th>Comments:</th>
          <td><textarea name="comments" cols="50" rows="5"><?= $kpaEvaluation->getComments() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Evaluate" /></td>
        </tr>
      </table>

      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="id" value="<?= $kpaEvaluation->getId() ?>" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />
      <input type="hidden" name="kpaManId" value="<?= $kpaId ?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="evalPeriod" value="<?= $selectedPeriod ?>" />
      <input type="hidden" name="evaluatedById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_evaluate_period.php');" />
    </div>
  </body>
</html>