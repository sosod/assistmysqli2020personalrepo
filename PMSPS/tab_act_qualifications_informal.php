<? if( count( $informalQualifications ) < $maxInformalQualifications && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New informal qualification" onclick="redirect('<?=$moduleType?>_activate_qualification.php?primaryType=informal')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="25%">Name</th>
    <th width="10%">Type</th>
    <th width="10%">Accredited</th>
    <th width="40%">Comments</th>
    <th width="15%">Actions</th>
  </tr>
  <?
    if( isset( $informalQualifications ) && count( $informalQualifications ) > 0 )
    {
      foreach( $informalQualifications as $qualification ) {
  ?>
  <tr>
    <td><?= $qualification->getName() ?></td>
    <td><?= $qualification->getSecondaryTypeDisplay() ?></td>
    <td><?= boolValue( $qualification->getAccredited() ) ?></td>
    <td><?= $qualification->getComments() ?></td>
    <td>
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_qualification.php?id=<?= $qualification->getId() ?>&primaryType=informal');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_qualification.php', '<?= $qualification->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="4">No informal qualifications</td></tr>
  <?
    }
  ?>
</table>