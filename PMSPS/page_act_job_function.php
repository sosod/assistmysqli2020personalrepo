<?
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$jobFunction = new JobFunction();

if( $action == "save" )
{
  $jobFunction->loadFromRequest( $_REQUEST );
  $jobFunction->save();
  
  redirect($moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $jobFunction->setId( $id );
  $jobFunction->delete();
  
  redirect($moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $jobFunction->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmJobFunction").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Job Function</h1>
    
    <form id="frmJobFunction" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Name:</th>
          <td><input type="text" name="name" value="<?= $jobFunction->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Description:</th>
          <td><textarea name="description" cols="50" rows="5" class="required"><?= $jobFunction->getDescription() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />      
      <input type="hidden" name="id" value="<?= $jobFunction->getId() ?>" />      
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form> 
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_activate.php');" />
    </div>  
  </body>
</html>