<?php
include("report_inc.php");

$id = $_REQUEST["learningActivityId"];
$action = $_REQUEST["action"];

$learningActivity = new LearningActivity();

if( $action == "complete" )
{
  $learningActivity->markAsCompleted( $id );
  redirect($moduleType."_report_learning.php");
}  

$learningActivity->loadFromId( $id );
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Activation :: Learning Activity</h1>
    
    <? include("info_learning_activity.php"); ?>
    
    <form id="frmLearningActivity" method="post" >
      <input type="submit" value="Mark as completed" />
      <input type="hidden" name="id" value="<?= $id ?>"/>
      <input type="hidden" name="action" value="complete"/>      
    </form>  
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_report_learning.php');" />
    </div>  
  </body>
</html>