function redirect( url )
{
  window.location.href = url;	
}

function selectPeriod( period )
{
  $("#select_period" ).val( period );  
  $("#frmSelectPeriod").submit();
}

function confirmDelete( url, id )
{
	if( confirm("Are you sure you want to delete this record?") )
	{
	  redirect( url + "?action=delete&id=" + id );
	}
	
	return false;
}

function confirmResetStatus( url )
{
	if( confirm("Are you sure you want to reset the status of this activation?") )
	{
	  return true;
	}
	
	return false;
}
  
$(document).ready(function(){
  $(".date-input").datepicker({ dateFormat: 'yy-mm-dd', changeMonth:true, changeYear:true, yearRange: '-100:+10' });  
  $('.tab-group ul').tabs({ cookie: { expires: 30 } });
  $("table.list tr").not(".heading,.info").mouseover(function(){$(this).addClass("over");}).mouseout(function(){$(this).removeClass("over");});
  $("table.list tr:even").not(".heading, .info").addClass("alt");
  $("table.info tr:even").addClass("alt");  
  $("table.form tr:even").addClass("alt");  
  $("table.form tr:last").addClass("actions");    
  $('a[rel*=facebox]').facebox();
});  