<?php

  /* *********************************************************************** */
  /* ************************ LOAD THE SELECTED YEAR *********************** */
  /* *********************************************************************** */
  $selectedYearId = $_SESSION[$moduleType."report_sess_selected_year_" .getLoggedInUserId()];

  if( exists($selectedYearId) )
  {
    $selectedYear = new EvalYear();
    $selectedYear->loadFromId($selectedYearId);
  }

  if( isset( $selectedYear ) == false || $_REQUEST["action"] == "year_period_select" )
  {
    if( exists( $_REQUEST["selectedYearId"] ) )
    {
      $selectedYear = new EvalYear();
      $selectedYear->loadFromId( $_REQUEST["selectedYearId"] );
    }

    if( isset( $selectedYear ) )
    {
      $_SESSION[$moduleType."report_sess_selected_year_" .getLoggedInUserId()] = $selectedYear->getId();
    }
    else
    {
      $selectedYear = null;
    }
  }

  /* *********************************************************************** */
  /* *********************** LOAD THE SELECTED PERIOD ********************** */
  /* *********************************************************************** */
  $selectedPeriod = $_SESSION[$moduleType."report_sess_selected_period_" .getLoggedInUserId()];

  if( isset( $selectedPeriod ) == false || $_REQUEST["action"] == "year_period_select" )
  {
    if( exists( $_REQUEST["selectedPeriod"] ) )
    {
      $selectedPeriod = $_REQUEST["selectedPeriod"];
    }
    else
    {
      $selectedPeriod = null;
    }

    $_SESSION[$moduleType."report_sess_selected_period_" .getLoggedInUserId()] = $selectedPeriod;
  }

?>
