<?php
include("inc_ignite.php");
include("career_inc.php");
include("activate_inc.php");

$errors = array();

$action = $_REQUEST["action"];

include("logic_act_status_change.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation</h1>

    <? include("frm_activate_user_year_select.php"); ?>

    <? if( isset( $selectedUser ) && isset( $selectedYear ) ){ ?>

    <? include("info_act_summary.php"); ?>

    <?
      $formalQualifications = getQualifications( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "formal" );
      $informalQualifications = getQualifications( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "informal" );
      $experiences = getExperiences( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $professionalBodies = getProfessionalBodies( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $jobFunctions = getJobFunctions( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $careerGoals = getCareerGoals( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $jobLevelKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "job_level" );
      $uniqueKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "unique" );
      $careerKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "career" );
      $learningActivities = getLearningActivities( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
      $comments = getActivationComments( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

      if( count( $jobFunctions ) == 0 )
      {
        $errors[] = "At least one job function is required";
      }

			/**
			 *  @refer Task 197
			 *  @date 05/10/2009
			 *  Career goals should not be required
			 */
      /*if( count( $careerGoals ) == 0 )
      {
        $errors[] = "At least one career goal is required";
      }*/

      if( ( count( $jobLevelKPAs ) + count( $uniqueKPAs ) + count( $careerKPAs ) ) == 0 )
      {
        $errors[] = "At least one KPI is required";
      }
    ?>

    <? include("frm_activate_approve.php"); ?>

    <div id="tabs" class="tab-group" style="margin-top:15px;">
      <ul>
        <li class="ui-tabs-nav-item"><a href="#setup">Setup</a></li>
        <li class="ui-tabs-nav-item"><a href="#qualifications">Qualifications</a></li>
        <li class="ui-tabs-nav-item"><a href="#job_functions">Job functions</a></li>
        <li class="ui-tabs-nav-item"><a href="#goals">Career goals</a></li>
        <li class="ui-tabs-nav-item"><a href="#kpas">KPIs</a></li>
        <li class="ui-tabs-nav-item"><a href="#learning">Learning activities</a></li>
        <li class="ui-tabs-nav-item"><a href="#comments">Comments</a></li>
      </ul>
      <div id="setup">
        <? include("tab_act_setup.php"); ?>
      </div><!-- end setup -->
      <div id="qualifications">

        <div id="tabs" class="tab-group" style="margin-top:15px;">
          <ul>
            <li class="ui-tabs-nav-item"><a href="#formal">Formal</a></li>
            <li class="ui-tabs-nav-item"><a href="#informal">Informal</a></li>
            <li class="ui-tabs-nav-item"><a href="#experience">Experience</a></li>
            <li class="ui-tabs-nav-item"><a href="#profbodies">Professional Bodies</a></li>
          </ul>
          <div id="formal">
            <? include("tab_act_qualifications_formal.php"); ?>
          </div>
          <div id="informal">
            <? include("tab_act_qualifications_informal.php"); ?>
          </div>
          <div id="experience">
            <? include("tab_act_experience.php"); ?>
          </div>
          <div id="profbodies">
            <? include("tab_act_professional_body.php"); ?>
          </div>
        </div>

      </div><!-- end qualifications -->
      <div id="job_functions">
        <? include("tab_act_job_functions.php"); ?>
      </div><!-- end job functions -->
      <div id="goals">
        <? include("tab_act_career_goals.php"); ?>
      </div><!-- end goals -->
      <div id="kpas">
        <h6>KPIs linked to job level</h6>
        <? include("tab_act_kpa_job_level.php"); ?>
        <h6>KPIs unique to this job</h6>
        <? include("tab_act_kpa_unique.php"); ?>
        <h6>KPIs linked to career goals</h6>
        <? include("tab_act_kpa_career.php"); ?>
      </div><!-- end kpas -->
      <div id="learning">
        <? include("tab_act_learning_activities.php"); ?>
      </div><!-- end learning -->
      <div id="comments">
        <? include("tab_act_comments.php"); ?>
      </div><!-- end comments -->
    </div><!-- end tabs -->
    <?
    }
    else
    {
    ?>
    <p class="instruction">Please select a user and evaluation year above to continue.</p>
    <? } ?>
  </body>
</html>

