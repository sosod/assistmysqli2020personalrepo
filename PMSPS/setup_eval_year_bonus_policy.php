<?php
include("inc_ignite.php");


$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$bonusPolicy = new BonusPolicy();

if( $action == "save" )
{
  $bonusPolicy->loadFromRequest( $_REQUEST );
  $bonusPolicy->save();
  
  redirect("setup_eval_year_bonus_policies.php?evalYearId=" . $bonusPolicy->getEvalYearId() );
}

if( exists( $id ) )
{
  $bonusPolicy->loadFromId( $id );
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmBonusPolicy").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Year Bonus Policy</h1>
    
    <form id="frmEvalYear" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>Name</th>
          <td><input type="text" name="name" value="<?= $bonusPolicy->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Percentage</th>
          <td><?= select( "percentage", getNumericSelectList(0,100), $bonusPolicy->getPercentage(), true ) ?></td>
        </tr>
        <tr>
          <th>Lower limit</th>
          <td><?= select( "lowerLimit", getNumericSelectList(0,100), $bonusPolicy->getLowerLimit(), true ) ?></td>
        </tr>
        <tr>
          <th>Percentage</th>
          <td><?= select( "upperLimit", getNumericSelectList(0,100), $bonusPolicy->getUpperLimit(), true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $bonusPolicy->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $bonusPolicy->getEvalYearId() ?>" />
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_eval_years.php');" />
    </div>  
  </body>
</html>