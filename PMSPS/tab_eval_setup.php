<h6>Personal Information</h6>
<? include("info_personal_info.php"); ?>

<? if( $useWeights )
   {
     $userActivationStatus = new ActivationStatus();
     $userActivationStatus->loadFor( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
?>
<h6>KPI Weight percentages</h6>
<table class="info full" cellspacing="0">
  <tr>
    <? if( $moduleType == "perform" ){ ?>
    <th>Career KPI percentage:</th>
    <td><?= $userActivationStatus->getCareerKPAPercentage() ?>%</td>
    <? } ?>
    <th>Organisational KPI percentage:</th>
    <td><?= $userActivationStatus->getOrgKPAPercentage() ?>%</td>
    <th>Managerial KPI percentage:</th>
    <td><?= $userActivationStatus->getManKPAPercentage() ?>%</td>
  </tr>
</table>
<? } ?>

<? if( $moduleType == "contract" )
   {
     $panelMembers = getPanelMembers( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
?>
<h6>Evaluation panel members</h6>
<table class="list full" cellspacing="0">
  <tr>
    <th>Person</th>
    <th>Created by</th>
    <th>Created on</th>
  </tr>
  <? foreach( $panelMembers as $panelMember ) {?>
  <tr>
    <td><?= $panelMember->getPanelTitle()->getName() ?></td>
    <td><?= $panelMember->getCreatedBy()->getFullName() ?></td>
    <td><?= $panelMember->getCreatedOn() ?></td>
  </tr>
  <? } ?>
</table>
<? } ?>