<?php
  class EvalYear extends Base
  {
    public static $TABLE_NAME = "eval_year";
    public static $DISPLAY_NAME = "Evaluation year";    
    
    private $id;
    private $name;
    private $start;
    private $end;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFor( $start, $end )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "start = '".$start."' AND end = '".$end."'" );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setName( $values["name"] );
      $this->setStart( $values["start"] );
      $this->setEnd( $values["end"] );      
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      $this->setStart( mysql_real_escape_string( $values["start"] ) );
      $this->setEnd( mysql_real_escape_string( $values["end"] ) );      
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . 
               " SET name = '" . $this->name . "', start = '" . $this->start . "', end = '" . $this->end . "' " .
               " WHERE id = " . $this->id;
               
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (name,start,end) " .
               "VALUES ('" . $this->name . "', '" . $this->start . "', '" . $this->end . "' )";
        
        return parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    } 
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    
    public function setStart( $start ) {
      $this->start = $start;
    }
    
    public function getStart() {
      return $this->start;
    }
    
    public function setEnd( $end ) {
      $this->end = $end;
    }
    
    public function getEnd() {
      return $this->end;
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
    
  }
?>
