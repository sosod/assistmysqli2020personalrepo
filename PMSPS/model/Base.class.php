<?php
  class Base
  {
    public static $INSERT = "Insert";
    public static $UPDATE = "Update";

    /* DB FUNCTIONS */
    public function save( $type, $sql, $displayName )
    {
      debug( "sql = " . $sql );

      mysql_query( $sql, getConnection() );

      logTransaction( $type . ": " . $displayName, $sql );
    }

    public function delete( $sql, $displayName )
    {
      debug( "sql = " . $sql );

      mysql_query( $sql, getConnection() );

      logTransaction( "Delete: " . $displayName, $sql );
    }

    public function load( $table, $id )
    {
      if( exists( $id ) )
      {
        $sql  = "SELECT * FROM assist_".getCompanyCode()."_pmsps_${table} WHERE id = $id";
        $rs = mysql_query( $sql, getConnection() );

        debug($sql);

        return getRecord( $rs );
      }
    }

    public function loadWhere( $table, $where )
    {
      $sql  = "SELECT * FROM assist_".getCompanyCode()."_pmsps_${table} WHERE " . $where;
      $rs = mysql_query( $sql, getConnection() );

      return getRecord( $rs );
    }
  }
?>
