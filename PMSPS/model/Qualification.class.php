<?php
  class Qualification extends Base
  {
    public static $TABLE_NAME = "qualification";
    public static $DISPLAY_NAME = "Qualification";    
    public static $PRIMARY_TYPES = array("formal" => "Formal",
                                         "informal" => "Informal");     
    public static $SECONDARY_TYPES = array("course" => "Course",
                                           "skill" => "Skill");                                              
    
    private $id;
    private $type;
    private $userId;
    private $evalYearId;    
    private $createdById;
    private $createdBy;
    private $createdOn;
    private $primaryType;
    private $secondaryType;    
    private $name;
    private $accredited;
    private $comments;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values, $escape = false )
    {
      $this->setId( $escape ? mysql_real_escape_string( $values["id"] ) : $values["id"] );
      $this->setType( $escape ? mysql_real_escape_string( $values["type"] ) : $values["type"] );
      $this->setUserId( $escape ? mysql_real_escape_string( $values["user_id"] ) : $values["user_id"] );
      $this->setEvalYearId( $escape ? mysql_real_escape_string( $values["eval_year_id"] ) : $values["eval_year_id"] );
      $this->setCreatedById( $escape ? mysql_real_escape_string( $values["created_by_id"] ) : $values["created_by_id"] );      
      $this->setCreatedOn( $escape ? mysql_real_escape_string( $values["created_on"] ) : $values["created_on"] );            
      $this->setPrimaryType( $escape ? mysql_real_escape_string( $values["primary_type"] ) : $values["primary_type"] );
      $this->setSecondaryType( $escape ? mysql_real_escape_string( $values["secondary_type"] ) : $values["secondary_type"] );      
      $this->setName( $escape ? mysql_real_escape_string( $values["name"] ) : $values["name"] );      
      $this->setAccredited( $escape ? mysql_real_escape_string( $values["accredited"] ) : $values["accredited"] );      
      $this->setComments( $escape ? mysql_real_escape_string( $values["comments"] ) : $values["comments"] );                  
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );      
      $this->setCreatedOn( getdate() );            
      $this->setPrimaryType( mysql_real_escape_string($values["primaryType"] ) );
      $this->setSecondaryType( mysql_real_escape_string($values["secondaryType"] ) );
      $this->setName( mysql_real_escape_string($values["name"] ) );      
      $this->setAccredited( mysql_real_escape_string($values["accredited"] ) );      
      $this->setComments( mysql_real_escape_string($values["comments"] ) );                  
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET type = '" . $this->type . "', primary_type = '" . $this->primaryType . "', secondary_type = '" . $this->secondaryType . "', name = '" . $this->name . "', accredited = " . $this->accredited . ", comments = '" . $this->comments . "' " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, user_id, eval_year_id, created_by_id, created_on, primary_type, secondary_type, name, accredited, comments) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", '" . $this->createdById . "', now(), '" . $this->primaryType . "', '" . $this->secondaryType . "', '" . $this->name . "', " . $this->accredited . ", '" . $this->comments . "' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }  

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }
    
    public function getCreatedById() {
      return $this->createdById;
    }
    
    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }
    
    public function getCreatedOn() {
      return $this->createdOn;
    }
       
    public function setPrimaryType( $primaryType ) {
      $this->primaryType = $primaryType;
    }
    
    public function getPrimaryType() {
      return $this->primaryType;
    }
    
    public function setSecondaryType( $secondaryType ) {
      $this->secondaryType = $secondaryType;
    }
    
    public function getSecondaryType() {
      return $this->secondaryType;
    }
    
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    
    public function setAccredited( $accredited ) {
      $this->accredited = $accredited;
    }
    
    public function getAccredited() {
      return $this->accredited;
    }
    
    public function setComments( $comments ) {
      $this->comments = $comments;
    }
    
    public function getComments() {
      return $this->comments;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }
      
      return $this->createdBy; 
    }
    
    /* UTILITY METHODS */
    public function getPrimaryTypeDisplay()
    {
      return self::$PRIMARY_TYPES[ $this->primaryType ];
    }
    
    public function getSecondaryTypeDisplay()
    {
      return self::$SECONDARY_TYPES[ $this->secondaryType ];
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayPrimaryType( $primaryType )
    {
      return self::$PRIMARY_TYPES[ $primaryType ];
    }
    
    public static function displaySecondaryType( $secondaryType )
    {
      return self::$SECONDARY_TYPES[ $secondaryType ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
  }   
?>
