<?php
  class BonusPolicy extends Base
  {
    public static $TABLE_NAME = "bonus_policy";
    public static $DISPLAY_NAME = "Bonus Policy";    
    
    private $id;
    private $name;
    private $evalYearId;
    private $evalYear;
    private $percentage;
    private $lowerLimit;
    private $upperLimit;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setName( $values["name"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setPercentage( $values["percentage"] );
      $this->setLowerLimit( $values["lower_limit"] );
      $this->setUpperLimit( $values["upper_limit"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      $this->setEvalYearId( mysql_real_escape_string( $values["evalYearId"] ) );
      $this->setPercentage( mysql_real_escape_string( $values["percentage"] ) );
      $this->setLowerLimit( mysql_real_escape_string( $values["lowerLimit"] ) );
      $this->setUpperLimit( mysql_real_escape_string( $values["upperLimit"] ) );
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . 
               " SET  name = '" . $this->name . "', eval_year_id = '" . $this->evalYearId . "', percentage = " . $this->percentage .
               ", lower_limit = " . $this->lowerLimit . ", upper_limit = " . $this->upperLimit . 
               " WHERE id = " . $this->id;
               
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );   
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (name,eval_year_id,percentage,lower_limit,upper_limit) " .
               "VALUES ('" . $this->name . "', '" . $this->evalYearId . "', " . $this->percentage . ", " . $this->lowerLimit . ", " . $this->upperLimit . " )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
       
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setPercentage( $percentage ) {
      $this->percentage = $percentage;
    }
    
    public function getPercentage() {
      return $this->percentage;
    }
    
    public function setLowerLimit( $lowerLimit ) {
      $this->lowerLimit = $lowerLimit;
    }
    
    public function getLowerLimit() {
      return $this->lowerLimit;
    }
    
    public function setUpperLimit( $upperLimit ) {
      $this->upperLimit = $upperLimit;
    }
    
    public function getUpperLimit() {
      return $this->upperLimit;
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = new EvalYear();
        $this->evalYear->loadFromId( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
    
    public static function createDefaultPolicies( $evalYearId )
    {
      debug( "going to create default policies" );
      
      $bonusPolicy = new BonusPolicy();
      $bonusPolicy->setName( "Poor performance" );
      $bonusPolicy->setEvalYearId( $evalYearId );
      $bonusPolicy->setPercentage( 0 );
      $bonusPolicy->setLowerLimit( 0 );
      $bonusPolicy->setUpperLimit( 45 );
      $bonusPolicy->save();
      
      debug( "done with poor performance" );
      
      $bonusPolicy = new BonusPolicy();
      $bonusPolicy->setName( "Average performance" );
      $bonusPolicy->setEvalYearId( $evalYearId );
      $bonusPolicy->setPercentage( 5 );
      $bonusPolicy->setLowerLimit( 46 );
      $bonusPolicy->setUpperLimit( 55 );
      $bonusPolicy->save();
      
      debug( "done with avg performance" );
      
      $bonusPolicy = new BonusPolicy();
      $bonusPolicy->setName( "Fair performance" );
      $bonusPolicy->setEvalYearId( $evalYearId );
      $bonusPolicy->setPercentage( 8 );
      $bonusPolicy->setLowerLimit( 56 );
      $bonusPolicy->setUpperLimit( 65 );
      $bonusPolicy->save();
      
      debug( "done with fair performance" );
      
      $bonusPolicy = new BonusPolicy();
      $bonusPolicy->setName( "Good performance" );
      $bonusPolicy->setEvalYearId( $evalYearId );
      $bonusPolicy->setPercentage( 11 );
      $bonusPolicy->setLowerLimit( 66 );
      $bonusPolicy->setUpperLimit( 75 );
      $bonusPolicy->save();
      
      debug( "done with good performance" );
      
      $bonusPolicy = new BonusPolicy();
      $bonusPolicy->setName( "Excellent performance" );
      $bonusPolicy->setEvalYearId( $evalYearId );
      $bonusPolicy->setPercentage( 14 );
      $bonusPolicy->setLowerLimit( 76 );
      $bonusPolicy->setUpperLimit( 100 );
      $bonusPolicy->save();
      
      debug( "done with excellent performance" );
    }
  }   
?>
