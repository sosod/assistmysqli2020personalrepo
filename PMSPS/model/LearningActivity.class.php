<?php
  class LearningActivity extends Base
  {
    public static $TABLE_NAME = "learning_activity";
    public static $DISPLAY_NAME = "Learning Activity";  
    public static $STATUSSES = array("active" => "Active", 
                                 "completed" => "Completed");  
    
    private $id;
    private $type;
    private $user;
    private $userId;
    private $evalYear;
    private $evalYearId;    
    private $createdById;
    private $createdBy;
    private $createdOn;
    private $priority;
    private $name;
    private $outcome;
    private $training;
    private $delivery_mode;
    private $time_frames;        
    private $work_opportunity;
    /*private $supportPersonId;*/
    private $supportPerson;    
    private $status;        
        
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values, $escape = false )
    {
      $this->setId( $escape ? mysql_real_escape_string( $values["id"] ) : $values["id"] );
      $this->setType( $escape ? mysql_real_escape_string( $values["type"] ) : $values["type"] );
      $this->setUserId( $escape ? mysql_real_escape_string( $values["user_id"] ) : $values["user_id"] );
      $this->setEvalYearId( $escape ? mysql_real_escape_string( $values["eval_year_id"] ) : $values["eval_year_id"] );
      $this->setName( $escape ? mysql_real_escape_string( $values["name"] ) : $values["name"] );
      $this->setCreatedById( $escape ? mysql_real_escape_string( $values["created_by_id"] ) : $values["created_by_id"] );
      $this->setCreatedOn( $escape ? mysql_real_escape_string( $values["created_on"] ) : $values["created_on"] );
      $this->setPriority( $escape ? mysql_real_escape_string( $values["priority"] ) : $values["priority"] );
      $this->setOutcome( $escape ? mysql_real_escape_string( $values["outcome"] ) : $values["outcome"] );      
      $this->setTraining( $escape ? mysql_real_escape_string( $values["training"] ) : $values["training"] );      
      $this->setDeliveryMode( $escape ? mysql_real_escape_string( $values["delivery_mode"] ) : $values["delivery_mode"] );      
      $this->setTimeFrames( $escape ? mysql_real_escape_string( $values["time_frames"] ) : $values["time_frames"] );
      $this->setWorkOpportunity( $escape ? mysql_real_escape_string( $values["work_opportunity"] ) : $values["work_opportunity"] );
      $this->setSupportPerson( $escape ? mysql_real_escape_string( $values["support_person"] ) : $values["support_person"] );
      $this->setStatus( $escape ? mysql_real_escape_string( $values["status"] ) : $values["status"] );      
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );      
      $this->setCreatedOn( getdate() );            
      $this->setPriority( mysql_real_escape_string($values["priority"] ) );      
      $this->setName( mysql_real_escape_string($values["name"] ) );            
      $this->setOutcome( mysql_real_escape_string($values["outcome"] ) );      
      $this->setTraining( mysql_real_escape_string($values["training"] ) );      
      $this->setDeliveryMode( mysql_real_escape_string($values["deliveryMode"] ) );      
      $this->setTimeFrames( mysql_real_escape_string($values["timeFrames"] ) );      
      $this->setWorkOpportunity( mysql_real_escape_string($values["workOpportunity"] ) );      
      $this->setSupportPerson( mysql_real_escape_string($values["supportPerson"] ) );      
      $this->setStatus( mysql_real_escape_string($values["status"] ) );            
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET priority = " . $this->priority . ", name = '" . $this->name . "', outcome = '" . $this->outcome . "', training = '" . $this->training . "', delivery_mode = '" . $this->deliveryMode . "', time_frames = '" . $this->timeFrames . "', work_opportunity = '" . $this->workOpportunity . "', support_person = '" . $this->supportPerson . "' " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type,user_id, eval_year_id, created_by_id, created_on, priority, name, outcome, training, delivery_mode, time_frames, work_opportunity, support_person, status) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", '" . $this->createdById . "', now(), " . $this->priority . " ,'" . $this->name . "', '" . $this->outcome . "', '" . $this->training . "', '" . $this->deliveryMode . "', '" . $this->timeFrames . "', '" . $this->workOpportunity . "', '" . $this->supportPerson . "', 'active' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    public function markAsCompleted( $id )
    {
      $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
             " SET status = 'completed' " .
             " WHERE id = " . $id;
        
      parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }  

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }
    
    public function getCreatedById() {
      return $this->createdById;
    }
    
    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }
    
    public function getCreatedOn() {
      return $this->createdOn;
    }
    
    public function setPriority( $priority ) {
      $this->priority = $priority;
    }
    
    public function getPriority() {
      return $this->priority;
    }
    
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
       
    public function setOutcome( $outcome ) {
      $this->outcome = $outcome;
    }
    
    public function getOutcome() {
      return $this->outcome;
    }
    
    public function setTraining( $training ) {
      $this->training = $training;
    }
    
    public function getTraining() {
      return $this->training;
    }
    
    public function setDeliveryMode( $deliveryMode ) {
      $this->deliveryMode = $deliveryMode;
    }
    
    public function getDeliveryMode() {
      return $this->deliveryMode;
    }
    
    public function setTimeFrames( $timeFrames ) {
      $this->timeFrames = $timeFrames;
    }
    
    public function getTimeFrames() {
      return $this->timeFrames;
    }
    
    public function setWorkOpportunity( $workOpportunity ) {
      $this->workOpportunity = $workOpportunity;
    }
    
    public function getWorkOpportunity() {
      return $this->workOpportunity;
    }
    
    /*public function setSupportPersonId( $supportPersonId ) {
      $this->supportPersonId = $supportPersonId;
    }
    
    public function getSupportPersonId() {
      return $this->supportPersonId;
    }*/
    
    public function setStatus( $status ) {
      $this->status = $status;
    }
    
    public function getStatus() {
      return $this->status;
    }
    
    /* UTILITY METHODS */
    public function getStatusDisplay()
    {
      return self::$STATUSSES[ $this->status ];
    }
    
    /* STATIC UTILITY METHODS */
    public static function displayStatus( $status )
    {
      return self::$STATUSSES[ $status ];
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }
      
      return $this->createdBy; 
    }
    
    public function getSupportPerson() 
    {
      /*if( isset( $this->supportPerson ) == false )
      {
        $this->supportPerson = new User();
        $this->supportPerson->loadFromId( $this->supportPersonId );
      }*/
      
      return $this->supportPerson; 
    }
		
		public function setSupportPerson( $supportPerson ) 
		{
      $this->supportPerson = $supportPerson;
    }
    
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = new User();
        $this->user->loadFromId( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = new EvalYear();
        $this->evalYear->loadFromId( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
  }   
?>
