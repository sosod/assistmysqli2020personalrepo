<?php
  class ProfessionalBody extends Base
  {
    public static $TABLE_NAME = "professional_body";
    public static $DISPLAY_NAME = "Professional Body";

    private $id;
    private $type;
    private $userId;
    private $evalYearId;
    private $name;
    private $memberSince;
    private $memberLevel;
    private $description;
    private $createdById;
    private $createdBy;
    private $createdOn;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );

      $this->loadFromRecord( $values );
    }

    public function loadFromRecord( $values, $escape = false )
    {
      $this->setId( $escape ? mysql_real_escape_string( $values["id"] ) : $values["id"] );
      $this->setType( $escape ? mysql_real_escape_string( $values["type"] ) : $values["type"] );
      $this->setUserId( $escape ? mysql_real_escape_string( $values["user_id"] ) : $values["user_id"] );
      $this->setEvalYearId( $escape ? mysql_real_escape_string( $values["eval_year_id"] ) : $values["eval_year_id"] );
      $this->setName( $escape ? mysql_real_escape_string( $values["name"] ) : $values["name"] );
      $this->setMemberSince( $escape ? mysql_real_escape_string( $values["member_since"] ) : $values["member_since"] );
      $this->setMemberLevel( $escape ? mysql_real_escape_string( $values["member_level"] ) : $values["member_level"] );
      $this->setDescription( $escape ? mysql_real_escape_string( $values["description"] ) : $values["description"] );
      $this->setCreatedById( $escape ? mysql_real_escape_string( $values["created_by_id"] ) : $values["created_by_id"] );
      $this->setCreatedOn( $escape ? mysql_real_escape_string( $values["created_on"] ) : $values["created_on"] );
    }

    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      $this->setMemberSince( mysql_real_escape_string( $values["memberSince"] ) );
      $this->setMemberLevel( mysql_real_escape_string( $values["memberLevel"] ) );
      $this->setDescription( mysql_real_escape_string( $values["description"] ) );
      $this->setCreatedById( mysql_real_escape_string( $values["createdById"] ) );
      $this->setCreatedOn( getdate() );
    }

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET name = '" . $this->name . "', member_since = '" . $this->memberSince . "', member_level = '" . $this->memberLevel . "', description = '" . $this->description . "' " .
               " WHERE id = " . $this->id;

        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, user_id, eval_year_id, name, member_since, member_level, created_by_id, created_on, description ) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ",'" . $this->name . "', '" . $this->memberSince . "', '" . $this->memberLevel . "', '" . $this->createdById . "', now(), '" . $this->description . "' )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );
      }
    }

    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setType( $type ) {
      $this->type = $type;
    }

    public function getType() {
      return $this->type;
    }

    public function setUserId( $userId ) {
      $this->userId = $userId;
    }

    public function getUserId() {
      return $this->userId;
    }

    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }

    public function getEvalYearId() {
      return $this->evalYearId;
    }

    public function setName( $name ) {
      $this->name = $name;
    }

    public function getName() {
      return $this->name;
    }

    public function setMemberSince( $memberSince ) {
      $this->memberSince = $memberSince;
    }

    public function getMemberSince() {
      return $this->memberSince;
    }

    public function setMemberLevel( $memberLevel ) {
      $this->memberLevel = $memberLevel;
    }

    public function getMemberLevel() {
      return $this->memberLevel;
    }

    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }

    public function getCreatedById() {
      return $this->createdById;
    }

    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function setDescription( $description ) {
      $this->description = $description;
    }

    public function getDescription() {
      return $this->description;
    }

    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy()
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }

      return $this->createdBy;
    }

    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }
?>
