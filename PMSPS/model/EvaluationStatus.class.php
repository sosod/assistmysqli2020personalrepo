<?php
  class EvaluationStatus extends Base
  {
    public static $TABLE_NAME = "eval_status";
    public static $DISPLAY_NAME = "Evaluation Status";    
    public static $STATUSSES = array("pending" => "Pending Evaluation", 
                                     "completed" => "Completed Evaluation");    
                                     
    public static $REVIEW_TYPES = array("formal" => "Formal Evaluation", 
                                        "oral" => "Oral Evaluation");                                     
    
    private $id;
    private $type;
    private $userId;
    private $evalYearId;
    private $evalPeriod;
    private $evalPeriodReviewType;    
    private $actionById;
    private $actionBy;
    private $actionOn;
    private $status;
    private $locked;
    private $finalKPASymbol;
    private $comments;        
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFor( $type, $userId, $evalYearId, $evalPeriod  )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId. " AND eval_period = " . $evalPeriod );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setEvalPeriod( $values["eval_period"] );
      $this->setEvalPeriodReviewType( $values["eval_period_review_type"] );      
      $this->setActionById( $values["action_by_id"] );      
      $this->setActionOn( $values["action_on"] );            
      $this->setStatus( $values["status"] );
      $this->setLocked( $values["locked"] );
      $this->setFinalKPASymbol( $values["final_kpa_symbol"] );
      $this->setComments( $values["comments"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );      
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setEvalPeriod( mysql_real_escape_string($values["evalPeriod"] ) );            
      $this->setEvalPeriodReviewType( mysql_real_escape_string($values["evalPeriodReviewType"] ) );                  
      $this->setActionById( mysql_real_escape_string($values["actionById"] ) );      
      $this->setActionOn( getdate() );            
      $this->setStatus( mysql_real_escape_string($values["status"] ) );
      $this->setFinalKPASymbol( mysql_real_escape_string($values["final_kpa_symbol"] ) );
      $this->setComments( mysql_real_escape_string($values["comments"] ) );
    }  

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET status = '" . $this->status . "', locked = " . $this->locked . ", final_kpa_symbol = '" . $this->finalKPASymbol . "', action_by_id = '" . $this->actionById . "', action_on = now() " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, user_id, eval_year_id, eval_period, eval_period_review_type, action_by_id, action_on, status, locked) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", " . $this->evalPeriod . ", '" . $this->evalPeriodReviewType . "', '" . $this->actionById . "', now(), '" . $this->status . "', ". $this->locked ." )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    public function updateFromReporting( $type, $userId, $evalYearId, $evalPeriod, $finalKPASymbol, $comments )
    {
      
      $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
             " SET final_kpa_symbol = '" . $finalKPASymbol . "', comments = '" . $comments . "' " .
             " WHERE user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId . " AND eval_period = " . $evalPeriod. " AND type = '" . $type . "'";
      
      debug( $sql );
        
      parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }  

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setEvalPeriod( $evalPeriod ) {
      $this->evalPeriod = $evalPeriod;
    }
    
    public function getEvalPeriod() {
      return $this->evalPeriod;
    }
    
    public function setEvalPeriodReviewType( $evalPeriodReviewType ) {
      $this->evalPeriodReviewType = $evalPeriodReviewType;
    }
    
    public function getEvalPeriodReviewType() {
      return $this->evalPeriodReviewType;
    }
    
    public function setActionById( $actionById ) {
      $this->actionById = $actionById;
    }
    
    public function getActionById() {
      return $this->actionById;
    }
    
    public function setActionOn( $actionOn ) {
      $this->actionOn = $actionOn;
    }
    
    public function getActionOn() {
      return $this->actionOn;
    }
       
    public function setStatus( $status ) {
      $this->status = $status;
    }
    
    public function getStatus() {
      return $this->status;
    }
    
    public function setLocked( $locked ) {
      $this->locked = $locked;
    }
    
    public function getLocked() {
      return $this->locked;
    }
    
    public function setFinalKPASymbol( $finalKPASymbol ) {
      $this->finalKPASymbol = $finalKPASymbol;
    }
    
    public function getFinalKPASymbol() {
      return $this->finalKPASymbol;
    }
    
    public function setComments( $comments ) {
      $this->comments = $comments;
    }
    
    public function getComments() {
      return $this->comments;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getActionBy() 
    {
      if( isset( $this->actionBy ) == false )
      {
        $this->actionBy = new User();
        $this->actionBy->loadFromId( $this->actionById );
      }
      
      return $this->actionBy; 
    }
    
    /* UTILITY METHODS */
    public function getStatusDisplay()
    {
      return self::$STATUSSES[ $this->status ];
    }
    
    public function getReviewTypeDisplay()
    {
      return self::$REVIEW_TYPES[ $this->evalPeriodReviewType ];
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayStatus( $status )
    {
      return self::$STATUSSES[ $status ];
    }
    
    public static function displayReviewType( $type )
    {
      return self::$REVIEW_TYPES[ $type ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
    
    public function isEvaluationMutable()
    {
      return $this->status == "pending";
    }
  }   
?>
