<?php
  class User extends Base
  {
    public static $TABLE_NAME = "timekeep";
    public static $DISPLAY_NAME = "User";

    private $id; //tkid
    private $title; //tktitle
    private $firstName; //tkname
    private $lastName; //tksurname
    private $email; //tkemail
    private $status; //tkstatus
    private $department;
    private $section;
    private $jobTitle;
    private $jobLevel;
    private $jobNumber;
    private $managerId;
    private $managerEmail;
    private $manager;
    private $gender;
		private $username;

		/* LAZY LOAD MEMBERS */
		private $userSetup;
		private $managedUsers;


    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      if( exists( $id ) )
      {
        $sql  = "SELECT * FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME .
                " WHERE tkid = '" . $id . "' ";

        $rs = mysql_query( $sql, getConnection() );

        $values = getRecord( $rs );

        $this->loadFromRecord( $values );
      }
    }

    public function loadFromIdForActivation( $id )
    {
      if( exists( $id ) )
      {
        $sql  = "SELECT u.*, us.section, us.job_level, us.job_number, d.value AS department, j.value AS job_title, " .
                " (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " um WHERE um.tkid = u.tkmanager ) AS manager, " .
                " (SELECT um.tkemail FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " um WHERE um.tkid = u.tkmanager ) AS manager_email " .
                " FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " u, " .
                " assist_".getCompanyCode()."_pmsps_user_setup us, " .
                " assist_".getCompanyCode()."_list_dept d, " .
                " assist_".getCompanyCode()."_list_jobtitle j " .
                " WHERE tkid = '" . $id . "' " .
                " AND us.user_id = u.tkid " .
                " AND d.id = u.tkdept " .
                " AND j.id = u.tkdesig ";

        $rs = mysql_query( $sql, getConnection() );

        $values = getRecord( $rs );

        $this->loadFromRecord( $values );

        $this->setDepartment( $values["department"] );
        $this->setSection( $values["section"] );
        $this->setJobTitle( $values["job_title"] );
        $this->setJobLevel( $values["job_level"] );
        $this->setJobNumber( $values["job_number"] );
        $this->setManager( $values["manager"] );
        $this->setManagerEmail( $values["manager_email"] );
      }
    }

    public function loadFromRecord( $values )
    {
      $this->setId( $values["tkid"] );
      $this->setTitle( $values["tktitle"] );
      $this->setFirstName( $values["tkname"] );
      $this->setLastName( $values["tksurname"] );
      $this->setEmail( $values["tkemail"] );
			$this->setStatus( $values["tkstatus"] );
      $this->setManagerId( $values["tkmanager"] );
      $this->setGender( $values["tkgender"] );
			$this->setUsername( $values["tkuser"] );
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setTitle( $title ) {
      $this->title = $title;
    }

    public function getTitle() {
      return $this->title;
    }

    public function setFirstName( $firstName ) {
      $this->firstName = $firstName;
    }

    public function getFirstName() {
      return $this->firstName;
    }

    public function setLastName( $lastName ) {
      $this->lastName = $lastName;
    }

    public function getLastName() {
      return $this->lastName;
    }

    public function setEmail( $email ) {
      $this->email = $email;
    }

    public function getEmail() {
      return $this->email;
    }

		public function setStatus( $status ) {
      $this->status = $status;
    }

    public function getStatus() {
      return $this->status;
    }

		public function setUsername( $username ) {
      $this->username = $username;
    }

    public function getUsername() {
      return $this->username;
    }

    public function setDepartment( $department ) {
      $this->department = $department;
    }

    public function getDepartment() {
      return $this->department;
    }

    public function setSection( $section ) {
      $this->section = $section;
    }

    public function getSection() {
      return $this->section;
    }

    public function setJobTitle( $jobTitle ) {
      $this->jobTitle = $jobTitle;
    }

    public function getJobTitle() {
      return $this->jobTitle;
    }

    public function setJobLevel( $jobLevel ) {
      $this->jobLevel = $jobLevel;
    }

    public function getJobLevel() {
      return $this->jobLevel;
    }

    public function setJobNumber( $jobNumber ) {
      $this->jobNumber = $jobNumber;
    }

    public function getJobNumber() {
      return $this->jobNumber;
    }

    public function setManagerId( $managerId ) {
      $this->managerId = $managerId;
    }

    public function getManagerId() {
      return $this->managerId;
    }

    public function setManager( $manager ) {
      $this->manager = $manager;
    }

    public function getManager() {
      return $this->manager;
    }

    public function setManagerEmail( $managerEmail ) {
      $this->managerEmail = $managerEmail;
    }

    public function getManagerEmail() {
      return $this->managerEmail;
    }

    public function setGender( $gender ) {
      $this->gender = $gender;
    }

    public function getGender() {
      return $this->gender;
    }

    /* UTILITY METHODS */
		static function compare( $a, $b )
    {
      $al = strtolower( $a->getFullName() );
      $bl = strtolower( $b->getFullName() );
      if( $al == $bl )
			{
        return 0;
      }

      return ( $al > $bl ) ? +1 : -1;
    }

    public function getFullName()
    {
      return $this->firstName . " " . $this->lastName;
    }

    public function getDecodedFullName()
    {
      return html_entity_decode( $this->firstName . " " . $this->lastName, ENT_QUOTES, "ISO-8859-1" );
    }

    public function getGenderDisplay()
    {
      return $this->gender == "M" ? "Male" : "Female";
    }

		public function getUserSetup()
    {
      if( isset( $this->setup ) == false )
      {
        $this->userSetup = new UserSetup();
				$this->userSetup->loadFromUserId( $this->id );
      }

      return $this->userSetup;
    }

		public function isManager()
    {
      if( $this->managerId == "S" )
      {
        return true;
      }
      else
      {
        $rs = getResultSet( "SELECT COUNT(*) AS cnt FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " WHERE tkmanager = " . $this->id );

        $record = getRecord($rs);

        if( $record != null && $record["cnt"] > 0 )
        {
          return true;
        }
      }

      return false;
    }

    public function isAssistSupportUser()
    {
      return $this->username == "support";
    }

		public function getManagedUsers( $category_id = null )
		{
			if( $this->isAssistSupportUser() || $this->getUserSetup()->getHRManager() == true )
			{
				$this->managedUsers = getCategoryUsers( $category_id );
			}
			else
			{
			  if( $this->getUserSetup()->getCategoryId() == $category_id )
	      {
	        $this->managedUsers[ $this->id ] = $this;
	      }

	      $ids = array();
	      $ids[] = $this->id;

	      $this->recursiveGetManagedUsers( $this->managedUsers, $ids, $category_id );

	      usort($this->managedUsers, array("User", "compare"));
			}

			return $this->managedUsers;
		}

		public function recursiveGetManagedUsers( &$users, $manager_ids, $category_id = null )
    {
    	$sql = "SELECT tk.tkid, tk.tkname, tk.tksurname, us.category_id FROM assist_".getCompanyCode()."_timekeep tk, assist_".getCompanyCode()."_pmsps_user_setup us, assist_".getCompanyCode()."_menu_modules_users mmu WHERE tk.tkid = mmu.usrtkid AND mmu.usrmodref = 'PMSPS' AND us.user_id = tk.tkid AND tk.tkmanager in ('" . implode( "','", $manager_ids ) ."')";

      $managed_users = getListRecords( $sql );

      $new_ids = array();

			foreach( $managed_users as $user )
			{
			  $new_ids[] = $user["tkid"];

				if( isset($users) == false || array_key_exists( $user["tkid"], $users ) == false )
				{
					if( $category_id != null && $category_id == $user["category_id"] )
					{
						$userObj = new User();
						$userObj->loadFromRecord( $user );

				    $users[$user["tkid"]] = $userObj;
					}
				}
			}

			if( count( $new_ids ) > 0 )
			{
			   $this->recursiveGetManagedUsers( $users, $new_ids, $category_id );
			}
    }

		function getManagedUsersSelectList( $type )
		{
		  $list = array();

			$users = $this->getManagedUsers( getTypeCategoryId( $type ) );

		  foreach( $users as $user )
		  {
		    $list[] = array( $user->getId(), $user->getFullName() );
		  }

		  return $list;
		}

		public function isManagedBy( $manager_id )
		{
			if( $this->managerId == $manager_id || ( $this->managerId == "S" && $this->id == $manager_id ) )
			{
				return true;
			}
			else
			{
				return $this->recursiveIsManagedBy( $this->id, $manager_id );
			}
		}

		public function recursiveIsManagedBy( $user_id, $manager_id )
		{
			$record = getRecord( getResultSet( "SELECT tkid, tkmanager FROM assist_".getCompanyCode()."_timekeep WHERE tkid = '" . $user_id . "'" ) );

			if( isset( $record ) )
			{
				if( $record["tkmanager"] == $manager_id )
				{
				  return true;
				}
				else
				{
					if( $record["tkmanager"] == "S" )
					{
					  return false;
					}
					else
					{
						return $this->recursiveIsManagedBy( $record["tkmanager"], $manager_id );
					}
				}
			}

			return false;
		}
  }
?>
