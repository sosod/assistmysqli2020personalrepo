<?php
  class Experience extends Base
  {
    public static $TABLE_NAME = "experience";
    public static $DISPLAY_NAME = "Experience";

    private $id;
    private $type;
    private $userId;
    private $evalYearId;
    private $name;
    private $obtainedFrom;
    private $obtainedOn;
    private $numOfYears;
    private $description;
    private $createdById;
    private $createdBy;
    private $createdOn;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );

      $this->loadFromRecord( $values );
    }

    public function loadFromRecord( $values, $escape = false  )
    {
      $this->setId( $escape ? mysql_real_escape_string( $values["id"] ) : $values["id"] );
      $this->setType( $escape ? mysql_real_escape_string( $values["type"] ) : $values["type"] );
      $this->setUserId( $escape ? mysql_real_escape_string( $values["user_id"] ) : $values["user_id"] );
      $this->setEvalYearId( $escape ? mysql_real_escape_string( $values["eval_year_id"] ) : $values["eval_year_id"] );
      $this->setName( $escape ? mysql_real_escape_string( $values["name"] ) : $values["name"] );
      $this->setObtainedFrom( $escape ? mysql_real_escape_string( $values["obtained_from"] ) : $values["obtained_from"] );
      $this->setObtainedOn( $escape ? mysql_real_escape_string( $values["obtained_on"] ) : $values["obtained_on"] );
      $this->setNumOfYears( $escape ? mysql_real_escape_string( $values["num_of_years"] ) : $values["num_of_years"] );
      $this->setDescription( $escape ? mysql_real_escape_string( $values["description"] ) : $values["description"] );
      $this->setCreatedById( $escape ? mysql_real_escape_string( $values["created_by_id"] ) : $values["created_by_id"] );
      $this->setCreatedOn( $escape ? mysql_real_escape_string( $values["created_on"] ) : $values["created_on"] );
    }

    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      $this->setObtainedFrom( mysql_real_escape_string( $values["obtainedFrom"] ) );
      $this->setObtainedOn( mysql_real_escape_string( $values["obtainedOn"] ) );
      $this->setNumOfYears( mysql_real_escape_string( $values["numOfYears"] ) );
      $this->setDescription( mysql_real_escape_string( $values["description"] ) );
      $this->setCreatedById( mysql_real_escape_string( $values["createdById"] ) );
      $this->setCreatedOn( getdate() );
    }

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET name = '" . $this->name . "', obtained_from = '" . $this->obtainedFrom . "', obtained_on = '" . $this->obtainedOn . "', num_of_years = " . $this->numOfYears . ", description = '" . $this->description . "' " .
               " WHERE id = " . $this->id;

        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, user_id, eval_year_id, name, obtained_from, obtained_on, created_by_id, created_on, num_of_years, description ) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ",'" . $this->name . "', '" . $this->obtainedFrom . "', '" . $this->obtainedOn . "', '" . $this->createdById . "', now(), '" . $this->numOfYears . "', '" . $this->description . "' )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );
      }
    }

    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setType( $type ) {
      $this->type = $type;
    }

    public function getType() {
      return $this->type;
    }

    public function setUserId( $userId ) {
      $this->userId = $userId;
    }

    public function getUserId() {
      return $this->userId;
    }

    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }

    public function getEvalYearId() {
      return $this->evalYearId;
    }

    public function setName( $name ) {
      $this->name = $name;
    }

    public function getName() {
      return $this->name;
    }

    public function setObtainedFrom( $obtainedFrom ) {
      $this->obtainedFrom = $obtainedFrom;
    }

    public function getObtainedFrom() {
      return $this->obtainedFrom;
    }

    public function setObtainedOn( $obtainedOn ) {
      $this->obtainedOn = $obtainedOn;
    }

    public function getObtainedOn() {
      return $this->obtainedOn;
    }

    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }

    public function getCreatedById() {
      return $this->createdById;
    }

    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function setNumOfYears( $numOfYears ) {
      $this->numOfYears = $numOfYears;
    }

    public function getNumOfYears() {
      return $this->numOfYears;
    }

    public function setDescription( $description ) {
      $this->description = $description;
    }

    public function getDescription() {
      return $this->description;
    }

    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy()
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }

      return $this->createdBy;
    }

    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }
?>
