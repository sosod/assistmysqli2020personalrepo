<?php
  class EvaluationSkill extends Base
  {
    public static $TABLE_NAME = "eval_skill";
    public static $DISPLAY_NAME = "Evaluation Skill";    
    
    public static $SKILL_TYPES = array("strong" => "Strong area", 
                                        "development" => "Development Area");   
    
    private $id;
    private $type;
    private $userId;
    private $evalYearId;
    private $evalPeriod;        
    private $skillType;
    private $createdById;
    private $createdBy;
    private $createdOn;
    private $message;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );            
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setEvalPeriod( $values["eval_period"] );
      $this->setSkillType( $values["skill_type"] );      
      $this->setCreatedById( $values["created_by_id"] );      
      $this->setCreatedOn( $values["created_on"] );            
      $this->setMessage( $values["message"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );         
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setEvalPeriod( mysql_real_escape_string($values["evalPeriod"] ) );            
      $this->setSkillType( mysql_real_escape_string($values["skillType"] ) );                  
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );      
      $this->setCreatedOn( getdate() );            
      $this->setMessage( mysql_real_escape_string($values["message"] ) );
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET message = '" . $this->message . "' " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type,user_id, eval_year_id, eval_period, skill_type, created_by_id, created_on, message) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", " . $this->evalPeriod . ", '" . $this->skillType . "', '" . $this->createdById . "', now(), '" . $this->message . "' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setEvalPeriod( $evalPeriod ) {
      $this->evalPeriod = $evalPeriod;
    }
    
    public function getEvalPeriod() {
      return $this->evalPeriod;
    }
    
    public function setSkillType( $skillType ) {
      $this->skillType = $skillType;
    }
    
    public function getSkillType() {
      return $this->skillType;
    }
    
    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }
    
    public function getCreatedById() {
      return $this->createdById;
    }
    
    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }
    
    public function getCreatedOn() {
      return $this->createdOn;
    }
       
    public function setMessage( $message ) {
      $this->message = $message;
    }
    
    public function getMessage() {
      return $this->message;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }
      
      return $this->createdBy; 
    }
    
    /* UTILITY METHODS */
    public function getTypeDisplay()
    {
      return self::$SKILL_TYPES[ $this->type ];
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayType( $type )
    {
      return self::$SKILL_TYPES[ $type ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
  }   
?>
