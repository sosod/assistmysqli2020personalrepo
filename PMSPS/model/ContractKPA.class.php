<?php
  class ContractKPA extends Base
  {
    public static $TABLE_NAME = "contract_kpa";
    public static $DISPLAY_NAME = "Contract KPI";
    
    private $id;
    private $name;
    //private $active;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setName( $values["name"] );
      //$this->setActive( $values["active"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      //$this->setActive( mysql_real_escape_string( $values["active"] ) );
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . 
               " SET name = '" . $this->name . "' " .
               " WHERE id = " . $this->id;
               
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );   
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (name) " .
               "VALUES ('" . $this->name . "' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    } 
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    /*
    public function setActive( $active ) {
      $this->active = $active;
    }
    
    public function getActive() {
      return $this->active;
    }*/
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
    
    public static function getList()
    {
      $records = getList( self::$TABLE_NAME );
      
      $results = array();
  
      foreach( $records as $record )
      {
        $contractKPA = new ContractKPA();
        $contractKPA->loadFromRecord( $record );
        
        $results[] = $contractKPA;
      }
      
      return $results;
    }
  }
?>
