<?php
  class JobFunction extends Base
  {
    public static $TABLE_NAME = "job_function";
    public static $DISPLAY_NAME = "Job Function";    
    
    private $id;
    private $type;
    private $userId;
    private $evalYearId;    
    private $createdById;
    private $createdBy;
    private $createdOn;
    private $name;
    private $description;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values, $escape = false )
    {
      $this->setId( $escape ? mysql_real_escape_string( $values["id"] ) : $values["id"] );
      $this->setType( $escape ? mysql_real_escape_string( $values["type"] ) : $values["type"] );
      $this->setUserId( $escape ? mysql_real_escape_string( $values["user_id"] ) : $values["user_id"] );
      $this->setEvalYearId( $escape ? mysql_real_escape_string( $values["eval_year_id"] ) : $values["eval_year_id"] );
      $this->setName( $escape ? mysql_real_escape_string( $values["name"] ) : $values["name"] );
      $this->setDescription( $escape ? mysql_real_escape_string( $values["description"] ) : $values["description"] );    
      $this->setCreatedById( $escape ? mysql_real_escape_string( $values["created_by_id"] ) : $values["created_by_id"] );
      $this->setCreatedOn( $escape ? mysql_real_escape_string( $values["created_on"] ) : $values["created_on"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );      
      $this->setCreatedOn( getdate() );            
      $this->setName( mysql_real_escape_string($values["name"] ) );
      $this->setDescription( mysql_real_escape_string($values["description"] ) );      
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET name = '" . $this->name . "', description = '" . $this->description . "' " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type,user_id, eval_year_id, created_by_id, created_on, name, description) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", '" . $this->createdById . "', now(), '" . $this->name . "', '" . $this->description . "' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }  

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }
    
    public function getCreatedById() {
      return $this->createdById;
    }
    
    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }
    
    public function getCreatedOn() {
      return $this->createdOn;
    }
       
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    
    public function setDescription( $description ) {
      $this->description = $description;
    }
    
    public function getDescription() {
      return $this->description;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }
      
      return $this->createdBy; 
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }   
?>
