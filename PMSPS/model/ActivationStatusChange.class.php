<?php
  class ActivationStatusChange extends Base
  {
    public static $TABLE_NAME = "act_status_change";
    public static $DISPLAY_NAME = "Activation Status change";    
    public static $STATUSSES = array("new" => "New",
                                     "pending" => "Pending Approval", 
                                     "employer" => "Approved - Employer",
                                     "employee" => "Approved - Employee");    
    
    private $id;
    private $statusId;
    private $changedById;
    private $changedBy;
    private $changedOn;
    private $oldStatus;
    private $newStatus;    
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFor( $setupId )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "status_id = " . $setupId );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setStatusId( $values["status_id"] );
      $this->setChangedById( $values["changed_by_id"] );      
      $this->setChangedOn( $values["changed_on"] );            
      $this->setOldStatus( $values["old_status"] );
      $this->setNewStatus( $values["new_status"] );      
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setStatusId( mysql_real_escape_string($values["statusId"] ) );
      $this->setChangedById( mysql_real_escape_string($values["changedById"] ) );      
      $this->setChangedOn( getdate() );            
      $this->setOldStatus( mysql_real_escape_string($values["oldStatus"] ) );
      $this->setNewStatus( mysql_real_escape_string($values["newStatus"] ) );      
    }  

    /* DB FUNCTIONS */
    public function save()
    {
      $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (status_id, changed_by_id, changed_on, old_status, new_status) " .
             "VALUES (" . $this->statusId . ", '" . $this->changedById . "', now(), '" . $this->oldStatus . "', '" . $this->newStatus . "' )";
      
      parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
    }
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setStatusId( $statusId ) {
      $this->statusId = $statusId;
    }
    
    public function getStatusId() {
      return $this->statusId;
    }
    
    public function setChangedById( $changedById ) {
      $this->changedById = $changedById;
    }
    
    public function getChangedById() {
      return $this->changedById;
    }
    
    public function setChangedOn( $changedOn ) {
      $this->changedOn = $changedOn;
    }
    
    public function getChangedOn() {
      return $this->changedOn;
    }
       
    public function setOldStatus( $oldStatus ) {
      $this->oldStatus = $oldStatus;
    }
    
    public function getOldStatus() {
      return $this->oldStatus;
    }
    
    public function setNewStatus( $newStatus ) {
      $this->newStatus = $newStatus;
    }
    
    public function getNewStatus() {
      return $this->newStatus;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getChangedBy() 
    {
      if( isset( $this->changedBy ) == false )
      {
        $this->changedBy = new User();
        $this->changedBy->loadFromId( $this->changedById );
      }
      
      return $this->actionBy; 
    }
    
    /* UTILITY METHODS */
    public function getOldStatusDisplay()
    {
      return self::$STATUSSES[ $this->oldStatus ];
    }  
    
    public function getNewStatusDisplay()
    {
      return self::$STATUSSES[ $this->newStatus ];
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayStatus( $status )
    {
      return self::$STATUSSES[ $status ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
    
  }   
?>
