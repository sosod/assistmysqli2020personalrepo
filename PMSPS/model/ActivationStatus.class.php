<?php
  class ActivationStatus extends Base
  {
    public static $TABLE_NAME = "act_status";
    public static $DISPLAY_NAME = "Activation Status";    
    public static $STATUSSES = array("new" => "New",
                                     "pending" => "Pending Approval", 
                                     "employer" => "Approved - Employer",
                                     "employee" => "Approved - Employee");    
    
    private $id;
    private $type;
    private $userId;
    private $evalYearId;    
    private $actionById;
    private $actionBy;
    private $actionOn;
    private $status;
    private $careerKPAPercentage = 0;
    private $orgKPAPercentage = 0;
    private $manKPAPercentage = 0;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFor( $type, $userId, $evalYearId )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "type = '" . $type . "' AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setActionById( $values["action_by_id"] );      
      $this->setActionOn( $values["action_on"] );            
      $this->setStatus( $values["status"] );
      $this->setCareerKPAPercentage( $values["career_kpa_percentage"] );
      $this->setOrgKPAPercentage( $values["org_kpa_percentage"] );
      $this->setManKPAPercentage( $values["man_kpa_percentage"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setActionById( mysql_real_escape_string($values["actionById"] ) );      
      $this->setActionOn( getdate() );            
      $this->setStatus( mysql_real_escape_string($values["status"] ) );
      $this->setCareerKPAPercentage( mysql_real_escape_string( $values["careerKPAPercentage"] ) );
      $this->setOrgKPAPercentage( mysql_real_escape_string( $values["orgKPAPercentage"] ) );
      $this->setManKPAPercentage( mysql_real_escape_string( $values["manKPAPercentage"] ) );
    }  

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET status = '" . $this->status . "', career_kpa_percentage = " . $this->careerKPAPercentage . ", org_kpa_percentage = " . $this->orgKPAPercentage . ", man_kpa_percentage = " . $this->manKPAPercentage . ", action_by_id = '" . $this->actionById . "', action_on = now() " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, user_id, eval_year_id, action_by_id, action_on, status, career_kpa_percentage, org_kpa_percentage, man_kpa_percentage) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", '" . $this->actionById . "', now(), '" . $this->status . "', ".$this->careerKPAPercentage.", ".$this->orgKPAPercentage.", ".$this->manKPAPercentage." )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }  

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setActionById( $actionById ) {
      $this->actionById = $actionById;
    }
    
    public function getActionById() {
      return $this->actionById;
    }
    
    public function setActionOn( $actionOn ) {
      $this->actionOn = $actionOn;
    }
    
    public function getActionOn() {
      return $this->actionOn;
    }
       
    public function setStatus( $status ) {
      $this->status = $status;
    }
    
    public function getStatus() {
      return $this->status;
    }
    
    public function setCareerKPAPercentage( $careerKPAPercentage ) {
      $this->careerKPAPercentage = $careerKPAPercentage;
    }
    
    public function getCareerKPAPercentage() {
      return $this->careerKPAPercentage;
    }
    
    public function setOrgKPAPercentage( $orgKPAPercentage ) {
      $this->orgKPAPercentage = $orgKPAPercentage;
    }
    
    public function getOrgKPAPercentage() {
      return $this->orgKPAPercentage;
    }
    
    public function setManKPAPercentage( $manKPAPercentage ) {
      $this->manKPAPercentage = $manKPAPercentage;
    }
    
    public function getManKPAPercentage() {
      return $this->manKPAPercentage;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getActionBy() 
    {
      if( isset( $this->actionBy ) == false )
      {
        $this->actionBy = new User();
        $this->actionBy->loadFromId( $this->actionById );
      }
      
      return $this->actionBy; 
    }
    
    /* UTILITY METHODS */
    public function getStatusDisplay()
    {
      return self::$STATUSSES[ $this->status ];
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayStatus( $status )
    {
      return self::$STATUSSES[ $status ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
    
    public function isActivationMutable()
    {
      return $this->status == "new";
    }
  }   
?>
