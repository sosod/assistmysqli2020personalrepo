<?php
  class Category extends Base
  {
    public static $TABLE_NAME = "category";
    public static $DISPLAY_NAME = "Category";    
    public static $EVAL_PERIOD_TYPES = array("quart_oral" => "Quarterly (Alt Oral)",
                                             "bi_annual" => "Bi-Annually", 
                                             "quart" => "Quarterly");
    
    private $id;
    private $name;
    private $evalPeriodType;
    private $useWeights;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setName( $values["name"] );
      $this->setEvalPeriodType( $values["eval_period_type"] );
      $this->setUseWeights( $values["use_weights"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      $this->setEvalPeriodType( mysql_real_escape_string( $values["evalPeriodType"] ) );
      $this->setUseWeights( mysql_real_escape_string( $values["useWeights"] ) );
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . 
               " SET name = '" . $this->name . "', eval_period_type = '" . $this->evalPeriodType . "', use_weights = " . $this->useWeights . 
               " WHERE id = " . $this->id;
               
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );   
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (name,eval_period_type,useWeights) " .
               "VALUES ('" . $this->name . "', '" . $this->evalPeriodType . "', " . $this->useWeights . " )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
       
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    
    public function setEvalPeriodType( $evalPeriodType ) {
      $this->evalPeriodType = $evalPeriodType;
    }
    
    public function getEvalPeriodType() {
      return $this->evalPeriodType;
    }
    
    public function setUseWeights( $useWeights ) {
      $this->useWeights = $useWeights;
    }
    
    public function getUseWeights() {
      return $this->useWeights;
    }
    
    /* UTILITY METHODS */
    public function getEvalPeriodTypeDisplay()
    {
      return self::$EVAL_PERIOD_TYPES[ $this->evalPeriodType ];
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayEvalPeriodType( $type )
    {
      return self::$EVAL_PERIOD_TYPES[ $type ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }   
?>
