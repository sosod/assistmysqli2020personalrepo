<?php
  class CareerGoalEvaluation extends Base
  {
    public static $TABLE_NAME = "eval_career_goal";
    public static $DISPLAY_NAME = "Career Goal Evaluation";    
    public static $COMPLETED_OPTIONS = array("yes" => "Yes", 
                                              "no" => "No",
                                              "partial" => "Partial");    
    
    private $id;
    private $type;
    private $careerGoalId;
    private $userId;
    private $evalYearId;
    private $evalPeriod;
    private $completed;
    private $comments;
    private $evaluatedById;
    private $evaluatedBy;
    private $evaluatedOn;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFor( $type, $careerGoalId, $userId, $evalYearId, $evalPeriod )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "type = '" . $type . "' AND career_goal_id = " . $careerGoalId . " AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId. " AND eval_period = " . $evalPeriod );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );      
      $this->setCareerGoalId( $values["career_goal_id"] );
      $this->setUserId( $values["user_id"] );      
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setEvalPeriod( $values["eval_period"] );
      $this->setCompleted( $values["completed"] );      
      $this->setEvaluatedById( $values["evaluated_by_id"] );      
      $this->setEvaluatedOn( $values["evaluated_on"] );            
      $this->setComments( $values["comments"] );
    }
    
    public function loadFromRequest( $values )
    {
      if( $this->isTransient() )
      {
        $this->setId( mysql_real_escape_string( $values["id"] ) );
      }

      $this->setType( mysql_real_escape_string($values["type"] ) );       
      $this->setCareerGoalId( mysql_real_escape_string($values["careerGoalId"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );      
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setEvalPeriod( mysql_real_escape_string($values["evalPeriod"] ) );            
      $this->setCompleted( mysql_real_escape_string($values["completed"] ) );                  
      $this->setEvaluatedById( mysql_real_escape_string($values["evaluatedById"] ) );      
      $this->setEvaluatedOn( getdate() );            
      $this->setComments( mysql_real_escape_string($values["comments"] ) );
    }  

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET completed = '" . $this->completed . "', comments = '" . $this->comments . "', evaluated_by_id = '" . $this->evaluatedById . "', evaluated_on = now() " .
               " WHERE id = " . $this->id;
        
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );       
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, career_goal_id, user_id, eval_year_id, eval_period, completed, evaluated_by_id, evaluated_on, comments) " .
               "VALUES ('" . $this->type . "', " . $this->careerGoalId . ", '" . $this->userId . "', " . $this->evalYearId . ", " . $this->evalPeriod . ", '" . $this->completed . "', '" . $this->evaluatedById . "', now(), '" . $this->comments . "' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }  

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setCareerGoalId( $careerGoalId ) {
      $this->careerGoalId = $careerGoalId;
    }
    
    public function getCareerGoalId() {
      return $this->careerGoalId;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setEvalPeriod( $evalPeriod ) {
      $this->evalPeriod = $evalPeriod;
    }
    
    public function getEvalPeriod() {
      return $this->evalPeriod;
    }
    
    public function setCompleted( $completed ) {
      $this->completed = $completed;
    }
    
    public function getCompleted() {
      return $this->completed;
    }
    
    public function setEvaluatedById( $evaluatedById ) {
      $this->evaluatedById = $evaluatedById;
    }
    
    public function getEvaluatedById() {
      return $this->evaluatedById;
    }
    
    public function setEvaluatedOn( $evaluatedOn ) {
      $this->evaluatedOn = $evaluatedOn;
    }
    
    public function getEvaluatedOn() {
      return $this->evaluatedOn;
    }
       
    public function setComments( $comments ) {
      $this->comments = $comments;
    }
    
    public function getComments() {
      return $this->comments;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getEvaluatedBy() 
    {
      if( isset( $this->evaluatedBy ) == false )
      {
        $this->evaluatedBy = new User();
        $this->evaluatedBy->loadFromId( $this->evaluatedById );
      }
      
      return $this->evaluatedBy; 
    }
    
    /* UTILITY METHODS */
    public function getCompletedDisplay()
    {
      return self::$COMPLETED_OPTIONS[ $this->completed ];
    }
    
    /* STATIC UTILITY METHODS */
    public static function displayCompleted( $completed )
    {
      return self::$COMPLETED_OPTIONS[ $completed ];
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
  }   
?>
