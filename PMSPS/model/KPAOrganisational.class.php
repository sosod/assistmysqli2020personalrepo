<?php
  class KPAOrganisational extends Base
  {
    public static $TABLE_NAME = "kpa_org";
    public static $DISPLAY_NAME = "Organisational KPI";

    private $id;
    private $type;
    private $userId;
    private $evalYearId;
    private $createdById;
    private $createdBy;
    private $createdOn;
    private $name;
    private $objective;
    private $kpi;
    private $measurement;
    private $baseline;
    private $target_unit;
    private $target1;
    private $target2;
    private $target3;
    private $target4;
    private $weight;
    private $comments;
    private $groupNum;
    private $sdbipNum;
    private $outcome;
    private $evidence;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );

      $this->loadFromRecord( $values );
    }

    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setCreatedById( $values["created_by_id"] );
      $this->setCreatedOn( $values["created_on"] );
      $this->setName( $values["name"] );
      $this->setObjective( $values["objective"] );
      $this->setKPI( $values["kpi"] );
      $this->setMeasurement( $values["measurement"] );
      $this->setBaseline( $values["baseline"] );
      $this->setTargetUnit( $values["target_unit"] );
      $this->setTarget1( $values["target1"] );
      $this->setTarget2( $values["target2"] );
      $this->setTarget3( $values["target3"] );
      $this->setTarget4( $values["target4"] );
      $this->setWeight( $values["weight"] );
      $this->setComments( $values["comments"] );
      $this->setGroupNum( $values["group_num"] );
      $this->setSDBIPNum( $values["sdbip_num"] );
      $this->setOutcome( $values["outcome"] );
      $this->setEvidence( $values["evidence"] );

    }

    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );
      $this->setCreatedOn( getdate() );
      $this->setName( mysql_real_escape_string($values["name"] ) );
      $this->setObjective( mysql_real_escape_string($values["objective"] ) );
      $this->setKPI( mysql_real_escape_string($values["kpi"] ) );
      $this->setMeasurement( mysql_real_escape_string($values["measurement"] ) );
      $this->setBaseline( mysql_real_escape_string($values["baseline"] ) );
      $this->setTargetUnit( mysql_real_escape_string($values["targetUnit"] ) );
      $this->setTarget1( mysql_real_escape_string($values["target1"] ) );
      $this->setTarget2( mysql_real_escape_string($values["target2"] ) );
      $this->setTarget3( mysql_real_escape_string($values["target3"] ) );
      $this->setTarget4( mysql_real_escape_string($values["target4"] ) );
      $this->setWeight( mysql_real_escape_string($values["weight"] ) );
      $this->setComments( mysql_real_escape_string($values["comments"] ) );
      $this->setGroupNum( mysql_real_escape_string($values["groupNum"] ) );
      $this->setSDBIPNum( mysql_real_escape_string($values["sdbipNum"] ) );
      $this->setOutcome( mysql_real_escape_string($values["outcome"]) );
      $this->setEvidence( mysql_real_escape_string($values["evidence"]) );
    }

    public function loadFromImport( $values )
    {
      $this->setSDBIPNum( mysql_real_escape_string($values[0] ) );
      $this->setName( mysql_real_escape_string($values[3] ) );
      $this->setObjective( mysql_real_escape_string($values[2] ) );
      $this->setKPI( mysql_real_escape_string($values[1] ) );
      $this->setMeasurement( mysql_real_escape_string($values[4] ) );
      $this->setBaseline( mysql_real_escape_string($values[5] ) );
      $this->setTargetUnit( mysql_real_escape_string($values[6] ) );
      $this->setTarget1( mysql_real_escape_string($values[7] ) );
      $this->setTarget2( mysql_real_escape_string($values[8] ) );
      $this->setTarget3( mysql_real_escape_string($values[9] ) );
      $this->setTarget4( mysql_real_escape_string($values[10] ) );
      $this->setWeight( mysql_real_escape_string($values[11] ) );
      $this->setComments( mysql_real_escape_string($values[12] ) );
    }

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET name = '" . $this->name . "', objective = '" . $this->objective . "', kpi = '" . $this->kpi . "', measurement = '" . $this->measurement . "', " .
               " baseline = '" . $this->baseline . "', target_unit = '" . $this->targetUnit . "', target1 = " . nullIfEmpty($this->target1) . ", target2 = " . nullIfEmpty($this->target2) . ", " .
               " target3 = " . nullIfEmpty($this->target3) . ", target4 = " . nullIfEmpty($this->target4) . ", weight = " . nullIfEmpty($this->weight) . ", comments = '" . $this->comments . "', " .
               " group_num = " . $this->groupNum . ", sdbip_num = " . nullIfEmpty($this->sdbipNum) . ", outcome = '" . $this->outcome . "', evidence = '" . $this->evidence . "' WHERE id = " . $this->id;

        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " (type, user_id, eval_year_id, created_by_id, created_on, name, objective, kpi, " .
               " measurement, baseline, target_unit, target1, target2, target3, target4, weight, comments, group_num, sdbip_num, outcome, evidence ) " .
               "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", '" . $this->createdById . "', now(), " .
               " '" . $this->name . "', '" . $this->objective . "', '" . $this->kpi . "', '" . $this->measurement . "', '" . $this->baseline . "', " .
               " '" . $this->targetUnit . "', " . nullIfEmpty($this->target1) . ", " . nullIfEmpty($this->target2) . ", " . nullIfEmpty($this->target3) . ", " . nullIfEmpty($this->target4) . ", " .
               " " . nullIfEmpty($this->weight) . ", '" . $this->comments . "', " . $this->groupNum . ", " . nullIfEmpty($this->sdbipNum) . ", '" . $this->outcome . "', '" . $this->evidence . "' )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );
      }
    }

    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setType( $type ) {
      $this->type = $type;
    }

    public function getType() {
      return $this->type;
    }

    public function setUserId( $userId ) {
      $this->userId = $userId;
    }

    public function getUserId() {
      return $this->userId;
    }

    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }

    public function getEvalYearId() {
      return $this->evalYearId;
    }

    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }

    public function getCreatedById() {
      return $this->createdById;
    }

    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function setName( $name ) {
      $this->name = $name;
    }

    public function getName() {
      return $this->name;
    }

    public function setObjective( $objective ) {
      $this->objective = $objective;
    }

    public function getObjective() {
      return $this->objective;
    }

    public function setKPI( $kpi ) {
      $this->kpi = $kpi;
    }

    public function getKPI() {
      return $this->kpi;
    }

    public function setMeasurement( $measurement ) {
      return $this->measurement = $measurement;
    }

    public function getMeasurement() {
      return $this->measurement;
    }

    public function setBaseline( $baseline ) {
      $this->baseline = $baseline;
    }

    public function getBaseline() {
      return $this->baseline;
    }

    public function setTargetUnit( $targetUnit ) {
      $this->targetUnit = $targetUnit;
    }

    public function getTargetUnit() {
      return $this->targetUnit;
    }

    public function setTarget1( $target1 ) {
      $this->target1 = $target1;
    }

    public function getTarget1() {
      return $this->target1;
    }

    public function setTarget2( $target2 ) {
      $this->target2 = $target2;
    }

    public function getTarget2() {
      return $this->target2;
    }

    public function setTarget3( $target3 ) {
      $this->target3 = $target3;
    }

    public function getTarget3() {
      return $this->target3;
    }

    public function setTarget4( $target4 ) {
      $this->target4 = $target4;
    }

    public function getTarget4() {
      return $this->target4;
    }

    public function getTargetForPeriod( $period )
    {
      if( $period == 1 )
      {
        return $this->target1;
      }
      else if( $period == 2 )
      {
        return $this->target2;
      }
      else if( $period == 3 )
      {
        return $this->target3;
      }
      else if( $period == 4 )
      {
        return $this->target4;
      }
    }

    public function setWeight( $weight ) {
      $this->weight = $weight;
    }

    public function getWeight() {
      return $this->weight;
    }

    public function setComments( $comments ) {
      $this->comments = $comments;
    }

    public function getGroupNum() {
      return $this->groupNum;
    }

    public function setGroupNum( $groupNum ) {
      $this->groupNum = $groupNum;
    }

    public function getSDBIPNum() {
      return $this->sdbipNum;
    }

    public function setSDBIPNum( $sdbipNum ) {
      $this->sdbipNum = $sdbipNum;
    }

    public function getComments() {
      return $this->comments;
    }

    public function setOutcome( $outcome ) {
      $this->outcome = $outcome;
    }

    public function getOutcome() {
      return $this->outcome;
    }

    public function setEvidence( $evidence ) {
      $this->evidence = $evidence;
    }

    public function getEvidence() {
      return $this->evidence;
    }

    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy()
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }

      return $this->createdBy;
    }

    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }
?>
