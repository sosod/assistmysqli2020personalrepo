<?php
  class PanelMember extends Base
  {
    public static $TABLE_NAME = "panel_member";
    public static $DISPLAY_NAME = "Panel Member";    
    
    private $id;
    private $type;
    private $userId;
    private $evalYearId;    
    private $panelTitleId;
    private $panelTitle;
    private $createdById;
    private $createdBy;
    private $createdOn;
    
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );      
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setPanelTitleId( $values["panel_title_id"] );
      $this->setCreatedById( $values["created_by_id"] );      
      $this->setCreatedOn( $values["created_on"] );            
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );      
      $this->setPanelTitleId( mysql_real_escape_string($values["panelTitleId"] ) );
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );      
      $this->setCreatedOn( getdate() );            
    }  
    
    public function save()
    {
      $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, user_id, eval_year_id, panel_title_id, created_by_id, created_on) " .
             "VALUES ('" . $this->type . "', '" . $this->userId . "', " . $this->evalYearId . ", " . $this->panelTitleId . ", '" . $this->createdById . "', now() )";
      
      parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
    }
    
    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );    
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setType( $type ) {
      $this->type = $type;
    }
    
    public function getType() {
      return $this->type;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEvalYearId() {
      return $this->evalYearId;
    }
    
    public function setPanelTitleId( $panelTitleId ) {
      $this->panelTitleId = $panelTitleId;
    }
    
    public function getPanelTitleId() {
      return $this->panelTitleId;
    }
    
    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }
    
    public function getCreatedById() {
      return $this->createdById;
    }
    
    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }
    
    public function getCreatedOn() {
      return $this->createdOn;
    }
       
    public function setMessage( $message ) {
      $this->message = $message;
    }
    
    public function getMessage() {
      return $this->message;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getPanelTitle() 
    {
      if( isset( $this->panelTitle ) == false )
      {
        $this->panelTitle = new PanelTitle();
        $this->panelTitle->loadFromId( $this->panelTitleId );
      }
      
      return $this->panelTitle; 
    }
    
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }
      
      return $this->createdBy; 
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }   
?>
