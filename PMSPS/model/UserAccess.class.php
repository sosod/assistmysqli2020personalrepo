<?php
  class UserAccess extends Base
  {
    public static $TABLE_NAME = "user_access";
    public static $DISPLAY_NAME = "User access";    
    public static $ACCESS_SECTIONS = array("setup" => "Setup",
                                           "activate_career" => "Performance Development - Activation",
                                           "evaluate_career" => "Performance Development - Evaluation",
                                           "reports_career" => "Performance Development - Reporting",
                                           "activate_perform" => "Performance Agreement - Activation",
                                           "evaluate_perform" => "Performance Agreement - Evaluation",
                                           "reports_perform" => "Performance Agreement - Reporting",
                                           "activate_contract" => "S57 Appointees - Activation",
                                           "evaluate_contract" => "S57 Appointees - Evaluation",
                                           "reports_contract" => "S57 Appointees - Reporting");
    
    private $id;
    private $userId;
    private $sections;
    
    /* LOAD FUNCTIONS */
    public function loadFromUserId( $userId )
    {
      $records = getListRecords( "SELECT * FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE user_id = '" . $userId . "'" );
      
      $this->sections = array();
      
      foreach( $records as $record )
      {
        $this->setUserId( $record["user_id"] );
        $this->sections[] = $record["section"];
      }  
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setUserId( mysql_real_escape_string( $values["userId"] ) );
      $this->setSections( $values["sections"] );               
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE user_id = '" . $this->userId . "'";
      parent::delete( $sql, self::$DISPLAY_NAME );        

      foreach( $this->sections as $section )
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (user_id,section) " .
               "VALUES ('" . $this->userId . "', '" . $section . "' )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );           
      }  
    } 
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setSections( $sections ) {
      $this->sections = $sections;
    }
    
    public function getSections() {
      return $this->sections;
    }
    
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = new User();
        $this->user->loadFromId( $this->userId );
      }
      
      return $this->user; 
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
    
    /* STATIC UTILITY METHODS */
    public static function displayAccessSection( $section )
    {
      return self::$ACCESS_SECTIONS[ $section ];
    }
  }
?>
