<?php
  class PanelTitle extends Base
  {
    public static $TABLE_NAME = "panel_title";
    public static $DISPLAY_NAME = "Panel Title";    
    
    private $id;
    private $name;
    private $active;
    
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setName( $values["name"] );
      $this->setActive( $values["active"] );
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setName( mysql_real_escape_string( $values["name"] ) );
      $this->setActive( mysql_real_escape_string( $values["active"] ) );
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . 
               " SET name = '" . $this->name . "', active = " . $this->active . 
               " WHERE id = " . $this->id;
               
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );   
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (name,active) " .
               "VALUES ('" . $this->name . "', " . $this->active . " )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    } 
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setName( $name ) {
      $this->name = $name;
    }
    
    public function getName() {
      return $this->name;
    }
    
    public function setActive( $active ) {
      $this->active = $active;
    }
    
    public function getActive() {
      return $this->active;
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }
?>
