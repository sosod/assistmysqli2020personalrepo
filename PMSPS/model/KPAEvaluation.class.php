<?php
  class KPAEvaluation extends Base
  {
    public static $TABLE_NAME = "eval_kpa";
    public static $DISPLAY_NAME = "KPI Evaluation";
    public static $RATING_OPTIONS = array("1" => "1",
                                          "2" => "2",
                                          "3" => "3",
                                          "4" => "4",
                                          "5" => "5");

    public static $BELL_CURVE_SYMBOLS = array("B" => "B",
                                          "N" => "N",
                                          "S" => "S",
                                          "E" => "E",
                                          "L" => "L");

    public static $KPA_TYPE_BASIC = "basic";
    public static $KPA_TYPE_ORG = "org";
    public static $KPA_TYPE_MAN = "man";
    public static $KPA_TYPE_BONUS = "bonus";

    private $id;
    private $type;
    private $kpaId;
    private $kpaOrgId;
    private $kpaManId;
    private $userId;
    private $evalYearId;
    private $evalPeriod;
    private $rating;
    private $selfEvaluation;
		private $comments;
    private $evaluatedById;
    private $evaluatedBy;
    private $evaluatedOn;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );

      $this->loadFromRecord( $values );
    }

    public function loadForBasic( $type, $kpaId, $userId, $evalYearId, $evalPeriod )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "type = '" . $type . "' AND kpa_id = " . $kpaId . " AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId. " AND eval_period = " . $evalPeriod );

      $this->loadFromRecord( $values );
    }

    public function loadForOrg( $type, $kpaId, $userId, $evalYearId, $evalPeriod )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "type = '" . $type . "' AND kpa_org_id = " . $kpaId . " AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId. " AND eval_period = " . $evalPeriod );

      $this->loadFromRecord( $values );
    }

    public function loadForMan( $type, $kpaId, $userId, $evalYearId, $evalPeriod )
    {
      $values = parent::loadWhere( self::$TABLE_NAME, "type = '" . $type . "' AND kpa_man_id = " . $kpaId . " AND user_id = '" . $userId . "' AND eval_year_id = " . $evalYearId. " AND eval_period = " . $evalPeriod );

      $this->loadFromRecord( $values );
    }

    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );
      $this->setKPAId( $values["kpa_id"] );
      $this->setKPAOrgId( $values["kpa_org_id"] );
      $this->setKPAManId( $values["kpa_man_id"] );
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setEvalPeriod( $values["eval_period"] );
      $this->setRating( $values["rating"] );
      $this->setEvaluatedById( $values["evaluated_by_id"] );
      $this->setEvaluatedOn( $values["evaluated_on"] );
      $this->setSelfEvaluation( $values["self_evaluation"] );
			$this->setComments( $values["comments"] );
    }

    public function loadFromRequest( $values )
    {
      if( $this->isTransient() )
      {
        $this->setId( mysql_real_escape_string( $values["id"] ) );
      }
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setKPAId( mysql_real_escape_string( $values["kpaId"] ) );
      $this->setKPAOrgId( mysql_real_escape_string( $values["kpaOrgId"] ) );
      $this->setKPAManId( mysql_real_escape_string( $values["kpaManId"] ) );
      $this->setUserId( mysql_real_escape_string( $values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string( $values["evalYearId"] ) );
      $this->setEvalPeriod( mysql_real_escape_string( $values["evalPeriod"] ) );
      $this->setRating( mysql_real_escape_string( $values["rating"] ) );
      $this->setEvaluatedById( mysql_real_escape_string( $values["evaluatedById"] ) );
      $this->setEvaluatedOn( getdate() );
      $this->setSelfEvaluation( mysql_real_escape_string( $values["selfEvaluation"] ) );
			$this->setComments( mysql_real_escape_string( $values["comments"] ) );
    }

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET rating = " . nullIfEmpty($this->rating) . ", self_evaluation = " .  nullIfEmpty( $this->selfEvaluation ) . ", comments = '" . $this->comments . "', evaluated_by_id = '" . $this->evaluatedById . "', evaluated_on = now() " .
               " WHERE id = " . $this->id;

        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, kpa_id, kpa_org_id, kpa_man_id, user_id, eval_year_id, eval_period, rating, evaluated_by_id, evaluated_on, comments, self_evaluation) " .
               "VALUES ('" . $this->type . "', " . nullIfEmpty($this->kpaId) . ", " . nullIfEmpty($this->kpaOrgId) . ", " . nullIfEmpty($this->kpaManId) . ", '" . $this->userId . "', " . $this->evalYearId . ", " . $this->evalPeriod . ", " . nullIfEmpty($this->rating) . ", '" . $this->evaluatedById . "', now(), '" . $this->comments . "', " .  nullIfEmpty($this->selfEvaluation). " )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );
      }
    }

    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setType( $type ) {
      $this->type = $type;
    }

    public function getType() {
      return $this->type;
    }

    public function setKPAId( $kpaId ) {
      $this->kpaId = $kpaId;
    }

    public function getKPAId() {
      return $this->kpaId;
    }

    public function setKPAOrgId( $kpaOrgId ) {
      $this->kpaOrgId = $kpaOrgId;
    }

    public function getKPAOrgId() {
      return $this->kpaOrgId;
    }

    public function setKPAManId( $kpaManId ) {
      $this->kpaManId = $kpaManId;
    }

    public function getKPAManId() {
      return $this->kpaManId;
    }

    public function setUserId( $userId ) {
      $this->userId = $userId;
    }

    public function getUserId() {
      return $this->userId;
    }

    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }

    public function getEvalYearId() {
      return $this->evalYearId;
    }

    public function setEvalPeriod( $evalPeriod ) {
      $this->evalPeriod = $evalPeriod;
    }

    public function getEvalPeriod() {
      return $this->evalPeriod;
    }

    public function setRating( $rating ) {
      $this->rating = $rating;
    }

    public function getRating() {
      return $this->rating;
    }

    public function setEvaluatedById( $evaluatedById ) {
      $this->evaluatedById = $evaluatedById;
    }

    public function getEvaluatedById() {
      return $this->evaluatedById;
    }

    public function setEvaluatedOn( $evaluatedOn ) {
      $this->evaluatedOn = $evaluatedOn;
    }

    public function getEvaluatedOn() {
      return $this->evaluatedOn;
    }

    public function setComments( $comments ) {
      $this->comments = $comments;
    }

    public function getComments() {
      return $this->comments;
    }

		public function setSelfEvaluation( $selfEvaluation ) {
      $this->selfEvaluation = $selfEvaluation;
    }

    public function getSelfEvaluation() {
      return $this->selfEvaluation;
    }

    /* LAZY LOAD FUNCTIONS */
    public function getEvaluatedBy()
    {
      if( isset( $this->evaluatedBy ) == false )
      {
        $this->evaluatedBy = new User();
        $this->evaluatedBy->loadFromId( $this->evaluatedById );
      }

      return $this->evaluatedBy;
    }

    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }

  public static function bellCurveSymbol( $average )
    {

      $average = round( $average, 1 );

      if( $average <= 1.4 )
      {
        return "B";
      }
      else if( $average <= 2.4 )
      {
        return "N";
      }
      else if( $average <= 3.4 )
      {
        return "S";
      }
      else if( $average <= 4.4 )
      {
        return "E";
      }
      else
      {
        return "L";
      }
    }

    public static function bellCurveSymbolFromPercentage( $percentage )
    {
      $percentage = round( $percentage, 0 );

      if( $percentage <= 40 )
      {
        return "B";
      }
      else if( $percentage <= 55 )
      {
        return "N";
      }
      else if( $percentage <= 70 )
      {
        return "S";
      }
      else if( $percentage <= 85 )
      {
        return "E";
      }
      else
      {
        return "L";
      }
    }
  }
?>
