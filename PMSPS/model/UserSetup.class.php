<?php
  class UserSetup extends Base
  {
    public static $TABLE_NAME = "user_setup";
    public static $DISPLAY_NAME = "User setup";  
    
    private $id;
    private $userId;
    private $user;
    private $categoryId;
    private $category;
    private $createPermission;
    private $approvePermission;    
    private $section;
    private $jobLevel;
    private $hrManager;
    private $jobNumber;
            
    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromUserId( $userId )
    {
      debug("loading setup for user id = " . $userId);
      
      $values = parent::loadWhere( self::$TABLE_NAME, "user_id = '" . $userId . "'" );
      
      $this->loadFromRecord( $values );
    }
    
    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setUserId( $values["user_id"] );
      $this->setCategoryId( $values["category_id"] );
      $this->setCreatePermission( $values["create_permission"] );      
      $this->setApprovePermission( $values["approve_permission"] );         
      $this->setSection( $values["section"] );               
      $this->setJobLevel( $values["job_level"] );
      $this->setHRManager( $values["hr_manager"] );      
      $this->setJobNumber( $values["job_number"] );  
    }
    
    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setUserId( mysql_real_escape_string( $values["userId"] ) );
      $this->setCategoryId( mysql_real_escape_string( $values["categoryId"] ) );
      $this->setCreatePermission( mysql_real_escape_string( $values["createPermission"] ) );      
      $this->setApprovePermission( mysql_real_escape_string( $values["approvePermission"] ) );         
      $this->setSection( mysql_real_escape_string( $values["section"] ) );               
      $this->setJobLevel( mysql_real_escape_string( $values["jobLevel"] ) );
      $this->setHRManager( mysql_real_escape_string( $values["hrManager"] ) ); 
      $this->setJobNumber( mysql_real_escape_string( $values["jobNumber"] ) );                   
    }  
    
    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . 
               " SET user_id = '" . $this->userId . "', category_id = " . $this->categoryId . ", create_permission = " . $this->createPermission . ", " . 
               " approve_permission = " . $this->approvePermission . ", section = '" . $this->section . "', job_level = '" . $this->jobLevel . "', hr_manager = " . $this->hrManager . ", " . 
               " job_number = '" . $this->jobNumber . "'" .
               " WHERE id = " . $this->id;
               
        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );   
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (user_id,category_id,create_permission,approve_permission,section,job_level,hr_manager,job_number) " .
               "VALUES ('" . $this->userId . "', " . $this->categoryId . ", " . $this->createPermission . ", " . $this->approvePermission . ", '" . $this->section . "', '" . $this->jobLevel . "', " . $this->hrManager . ", '" . $this->jobNumber . "' )";
        
        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );       
      }
    } 
    
    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }
    
    public function getId() {
      return $this->id;
    }
    
    public function setUserId( $userId ) {
      $this->userId = $userId;
    }
    
    public function getUserId() {
      return $this->userId;
    }
    
    public function setCategoryId( $categoryId ) {
      $this->categoryId = $categoryId;
    }
    
    public function getCategoryId() {
      return $this->categoryId;
    }
    
    public function setCreatePermission( $createPermission ) {
      $this->createPermission = $createPermission;
    }
    
    public function getCreatePermission() {
      return $this->createPermission;
    }
    
    public function setApprovePermission( $approvePermission ) {
      $this->approvePermission = $approvePermission;
    }
    
    public function getApprovePermission() {
      return $this->approvePermission;
    }
    
    public function setSection( $section ) {
      $this->section = $section;
    }
    
    public function getSection() {
      return $this->section;
    }
    
    public function setJobLevel( $jobLevel ) {
      $this->jobLevel = $jobLevel;
    }
    
    public function getJobLevel() {
      return $this->jobLevel;
    }
    
    public function setHRManager( $hrManager ) {
      $this->hrManager = $hrManager;
    }
    
    public function getHRManager() {
      return $this->hrManager;
    }
    
    public function setJobNumber( $jobNumber ) {
      $this->jobNumber = $jobNumber;
    }
    
    public function getJobNumber() {
      return $this->jobNumber;
    }
    
    /* LAZY LOAD FUNCTIONS */
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = new User();
        $this->user->loadFromId( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getCategory() 
    {
      if( isset( $this->category ) == false )
      {
        $this->category = new Category();
        $this->category->loadFromId( $this->categoryId );
      }
      
      return $this->category; 
    }
    
    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }  
  }
?>
