<?php
  class KPAManagerial extends Base
  {
    public static $TABLE_NAME = "kpa_managerial";
    public static $DISPLAY_NAME = "Managerial KPI";
    public static $SKILL_LEVELS = array("E" => "Expert",
                                        "A" => "Advanced",
                                        "C" => "Competent",
                                        "B" => "Basic");

    private $id;
    private $type;
    private $userId;
    private $evalYearId;
    private $createdById;
    private $createdBy;
    private $createdOn;
    private $coreManagerialSkillId;
    private $coreManagerialSkill;
    private $skillLevel;
    private $measurement;
    private $comments;
    private $weight;
    private $outcome;
    private $evidence;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );

      $this->loadFromRecord( $values );
    }

    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setType( $values["type"] );
      $this->setCoreManagerialSkillId( $values["core_managerial_skill_id"] );
      $this->setUserId( $values["user_id"] );
      $this->setEvalYearId( $values["eval_year_id"] );
      $this->setCreatedById( $values["created_by_id"] );
      $this->setCreatedOn( $values["created_on"] );
      $this->setSkillLevel( $values["skill_level"] );
      $this->setMeasurement( $values["measurement"] );
      $this->setComments( $values["comments"] );
      $this->setWeight( $values["weight"] );
      $this->setOutcome( $values["outcome"] );
      $this->setEvidence( $values["evidence"] );
    }

    public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string($values["id"] ) );
      $this->setType( mysql_real_escape_string($values["type"] ) );
      $this->setCoreManagerialSkillId( mysql_real_escape_string($values["coreManagerialSkillId"] ) );
      $this->setUserId( mysql_real_escape_string($values["userId"] ) );
      $this->setEvalYearId( mysql_real_escape_string($values["evalYearId"] ) );
      $this->setCreatedById( mysql_real_escape_string($values["createdById"] ) );
      $this->setCreatedOn( getdate() );
      $this->setSkillLevel( mysql_real_escape_string($values["skillLevel"] ) );
      $this->setMeasurement( mysql_real_escape_string($values["measurement"] ) );
      $this->setComments( mysql_real_escape_string($values["comments"] ) );
      $this->setWeight( mysql_real_escape_string($values["weight"] ) );
      $this->setOutcome( mysql_real_escape_string($values["outcome"]) );
      $this->setEvidence( mysql_real_escape_string($values["evidence"]) );
    }

    /* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET skill_level = '" . $this->skillLevel . "', measurement = '" . $this->measurement . "', comments = '" . $this->comments . "', weight = " . $this->weight . ", outcome = '" . $this->outcome . "', evidence = '" . $this->evidence . "' " .
               " WHERE id = " . $this->id;

        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (type, core_managerial_skill_id, user_id, eval_year_id, created_by_id, created_on, skill_level, measurement, comments, weight, outcome, evidence) " .
               "VALUES ('" . $this->type . "', '" . $this->coreManagerialSkillId . "', '" . $this->userId . "', " . $this->evalYearId . ", '" . $this->createdById . "', now(), '" . $this->skillLevel . "', '" . $this->measurement . "', '" . $this->comments . "', " . $this->weight . ", '" . $this->outcome . "', '" . $this->evidence . "' )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );
      }
    }

    public function delete()
    {
      $sql = "DELETE FROM assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME . " WHERE id = " . $this->id ;
      parent::delete( $sql, self::$DISPLAY_NAME );
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setType( $type ) {
      $this->type = $type;
    }

    public function getType() {
      return $this->type;
    }

    public function setCoreManagerialSkillId( $coreManagerialSkillId ) {
      $this->coreManagerialSkillId = $coreManagerialSkillId;
    }

    public function getCoreManagerialSkillId() {
      return $this->coreManagerialSkillId;
    }

    public function setUserId( $userId ) {
      $this->userId = $userId;
    }

    public function getUserId() {
      return $this->userId;
    }

    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }

    public function getEvalYearId() {
      return $this->evalYearId;
    }

    public function setCreatedById( $createdById ) {
      $this->createdById = $createdById;
    }

    public function getCreatedById() {
      return $this->createdById;
    }

    public function setCreatedOn( $createdOn ) {
      $this->createdOn = $createdOn;
    }

    public function getCreatedOn() {
      return $this->createdOn;
    }

    public function setSkillLevel( $skillLevel ) {
      $this->skillLevel = $skillLevel;
    }

    public function getSkillLevel() {
      return $this->skillLevel;
    }

    public function setMeasurement( $measurement ) {
      $this->measurement = $measurement;
    }

    public function getMeasurement() {
      return $this->measurement;
    }

    public function setComments( $comments ) {
      $this->comments = $comments;
    }

    public function getComments() {
      return $this->comments;
    }

    public function setWeight( $weight ) {
      $this->weight = $weight;
    }

    public function getWeight() {
      return $this->weight;
    }

    public function setOutcome( $outcome ) {
      $this->outcome = $outcome;
    }

    public function getOutcome() {
      return $this->outcome;
    }

    public function setEvidence( $evidence ) {
      $this->evidence = $evidence;
    }

    public function getEvidence() {
      return $this->evidence;
    }

    /* HELPER METHODS */
   public function getName()
   {
     return $this->getCoreManagerialSkill()->getSkill();
   }

    /* LAZY LOAD FUNCTIONS */
    public function getCreatedBy()
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = new User();
        $this->createdBy->loadFromId( $this->createdById );
      }

      return $this->createdBy;
    }

    public function getCoreManagerialSkill()
    {
      if( isset( $this->coreManagerialSkill ) == false )
      {
        $this->coreManagerialSkill = new CoreManagerialSkill();
        $this->coreManagerialSkill->loadFromId( $this->coreManagerialSkillId );
      }

      return $this->coreManagerialSkill;
    }

    /* UTILITY METHODS */
    public function getSkillLevelDisplay()
    {
      return self::$SKILL_LEVELS[ $this->skillLevel ];
    }

    /* STATIC UTILITY METHODS */
    public static function displaySkillLevel( $skillLevel )
    {
      return self::$SKILL_LEVELS[ $skillLevel ];
    }

    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }
  }
?>
