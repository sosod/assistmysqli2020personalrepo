<?php
  class CoreManagerialSkill extends Base
  {
    public static $TABLE_NAME = "core_managerial_skill";
    public static $DISPLAY_NAME = "Core Managerial Skill";

    private $id;
    private $skill;
    private $definition;
    private $active;
    private $definitionBasic;
    private $definitionCompetent;
    private $definitionAdvanced;
    private $definitionExpert;

    /* LOAD FUNCTIONS */
    public function loadFromId( $id )
    {
      $values = parent::load( self::$TABLE_NAME, $id );

      $this->loadFromRecord( $values );
    }

    public function loadFromRecord( $values )
    {
      $this->setId( $values["id"] );
      $this->setSkill( $values["skill"] );
      $this->setDefinition( $values["definition"] );
      $this->setActive( $values["active"] );
      $this->setDefinitionBasic( $values["definition_basic"] );
      $this->setDefinitionCompetent( $values["definition_competent"] );
      $this->setDefinitionAdvanced( $values["definition_advanced"] );
      $this->setDefinitionExpert( $values["definition_expert"] );
    }

		public function loadFromRequest( $values )
    {
      $this->setId( mysql_real_escape_string( $values["id"] ) );
      $this->setSkill( mysql_real_escape_string( $values["skill"] ) );
			$this->setDefinition( mysql_real_escape_string( $values["definition"] ) );
			$this->setActive( mysql_real_escape_string( $values["active"] ) );
			$this->setDefinitionBasic( mysql_real_escape_string($values["definitionBasic"] ) );
      $this->setDefinitionCompetent( mysql_real_escape_string($values["definitionCompetent"] ) );
      $this->setDefinitionAdvanced( mysql_real_escape_string($values["definitionAdvanced"] ) );
      $this->setDefinitionExpert( mysql_real_escape_string($values["definitionExpert"] ) );
    }

		/* DB FUNCTIONS */
    public function save()
    {
      if( $this->isTransient() == false )
      {
        $sql = "UPDATE assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME .
               " SET skill = '" . $this->skill . "', " .
							 "     definition = '" . $this->definition . "', " .
							 "     active = '" . $this->active . "', " .
               "     definition_basic = '" . $this->definitionBasic . "', " .
               "     definition_competent = '" . $this->definitionCompetent . "', " .
               "     definition_advanced = '" . $this->definitionAdvanced . "', " .
               "     definition_expert = '" . $this->definitionExpert . "' " .
               " WHERE id = " . $this->id;

        parent::save( parent::$UPDATE, $sql, self::$DISPLAY_NAME );
      }
      else
      {
        $sql = "INSERT INTO assist_" . getCompanyCode() . "_pmsps_" . self::$TABLE_NAME ." (skill,definition,active,definition_basic,definition_competent,definition_advanced,definition_expert) " .
               "VALUES ('" . $this->skill . "','" . $this->definition . "','" . $this->active . "','" . $this->definitionBasic . "','" . $this->definitionCompetent . "','" . $this->definitionAdvanced . "','" . $this->definitionExpert . "' )";

        parent::save( parent::$INSERT, $sql, self::$DISPLAY_NAME );
      }
    }

    /* GETTERS AND SETTERS */
    public function setId( $id ) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setSkill( $skill ) {
      $this->skill = $skill;
    }

    public function getSkill() {
      return $this->skill;
    }

    public function setDefinition( $definition ) {
      $this->definition = $definition;
    }

    public function getDefinition() {
      return $this->definition;
    }

    public function setEvalYearId( $evalYearId ) {
      $this->evalYearId = $evalYearId;
    }

    public function getActive() {
      return $this->active;
    }

    public function setActive( $active ) {
      $this->active = $active;
    }

    public function setDefinitionBasic( $definitionBasic ) {
      $this->definitionBasic = $definitionBasic;
    }

    public function getDefinitionBasic() {
      return $this->definitionBasic;
    }

    public function setDefinitionCompetent( $definitionCompetent ) {
      $this->definitionCompetent = $definitionCompetent;
    }

    public function getDefinitionCompetent() {
      return $this->definitionCompetent;
    }

    public function setDefinitionAdvanced( $definitionAdvanced ) {
      $this->definitionAdvanced = $definitionAdvanced;
    }

    public function getDefinitionAdvanced() {
      return $this->definitionAdvanced;
    }

    public function setDefinitionExpert( $definitionExpert ) {
      $this->definitionExpert = $definitionExpert;
    }

    public function getDefinitionExpert() {
      return $this->definitionExpert;
    }

    /* HELPER FUNCTIONS */
    public function isTransient()
    {
      return exists( $this->id ) == false;
    }

		public static function getList()
    {
      $records = getList( self::$TABLE_NAME );

      $results = array();

      foreach( $records as $record )
      {
        $obj = new CoreManagerialSkill();
        $obj->loadFromRecord( $record );

        $results[] = $obj;
      }

      return $results;
    }
  }
?>
