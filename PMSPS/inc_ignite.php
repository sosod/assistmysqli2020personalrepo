<?php
function __autoload($class_name) {
  require_once "model/" . $class_name . '.class.php';
}

require_once '../inc_session.php';

//include("../inc_ignite.php");
$logfile = "../logs/error_".$cmpcode.".txt";

include("inc_PMSPS_util.php");
include("inc_PMSPS_db.php");

/* Make a connection so that mysql_real_escape_string works */
getConnection();

/* LOAD THE LOGGED IN USER'S SETUP */
//$userSetup = $_SESSION["sess_user_setup_" .getLoggedInUserId()];

//if( isset( $userSetup ) == false || $userSetup->getUserId() != getLoggedInUserId() )
//{
  $userSetup = new UserSetup();
  $userSetup->loadFromUserId( getLoggedInUserId() );

//  $_SESSION["sess_user_setup_" .getLoggedInUserId()] = $userSetup;
//}

?>