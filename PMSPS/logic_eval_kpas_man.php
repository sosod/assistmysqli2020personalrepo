<?php
$manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() ); 
      
$manKPAsWithEval = array();

foreach( $manKPAs as $kpa )
{
  $kpaEvaluation = new KPAEvaluation();
  $kpaEvaluation->loadForMan( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
  
  if( $kpaEvaluation->isTransient() )
  {
    $allEvaluated = false;
  }
  
  if( $useWeights )
  {
    $totalPercentage += ( $kpaEvaluation->getRating() * $kpa->getWeight() / 5 );    
  }
  else
  {
    $totalMarks += $kpaEvaluation->getRating();  
  }
  
  $manKPAsWithEval[] = array( $kpa, $kpaEvaluation );
}  

$numOfKPAs = $numOfKPAs + count( $manKPAs );
?>
