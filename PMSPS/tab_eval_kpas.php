<div class="actions">
  <input type="button" value="Evaluate All" onclick="redirect('<?=$moduleType?>_evaluate_kpas.php');" <?if($disableBtn){?>disabled="disabled"<?}?>/>
</div>

<table class="list full" cellspacing="0">
  <tr>
    <? if($moduleType == "career"){?><th width="30%">Type</th><?}?>
    <th width="25%">KPI</th>
    <? if($moduleType == "perform" || $moduleType == "contract"){?><th width="30%">Measurement</th><?}?>
    <th width="8%">Rating</th>
		<th width="8%">Self Evaluation</th>
    <? if( $useWeights ){ ?>
    <th width="7%">Weight</th>
    <th width="7%">Percentage</th>
    <? }else{ ?>
    <th width="14%">Evaluated On</th>
    <? } ?>
    <th width="15%">Actions</th>
  </tr>
  <?
  foreach( $kpasWithEval as $kpaWithEval )
  {
    $kpa = $kpaWithEval[0];
    $kpaEvaluation = $kpaWithEval[1];
  ?>
  <tr>
    <? if($moduleType == "career"){?><td><?= $kpa->getTypeDisplay() ?></td><?}?>
    <td><?= $kpa->getName() ?></td>
    <? if($moduleType == "perform"){?><td><?= $kpa->getMeasurement() ?></td><?}?>
    <td><?= $kpaEvaluation->isTransient() || $kpaEvaluation->getRating() == null  ? "Not Set" : $kpaEvaluation->getRating() ?></td>
    <td><?= $kpaEvaluation->isTransient() || $kpaEvaluation->getSelfEvaluation() == null ? "Not Set" : $kpaEvaluation->getSelfEvaluation() ?></td>
    <? if( $useWeights ){ ?>
    <td><?= $kpa->getWeight() ?></td>
    <td><?= $kpaEvaluation->isTransient() ? "Not Set" : ($kpaEvaluation->getRating() * $kpa->getWeight() / 5) ?>%</td>
    <? }else{ ?>
    <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedOn() ?></td>
    <? } ?>
    <td>
      <input type="button" value="Evaluate" onclick="redirect('<?=$moduleType?>_evaluate_kpa.php?kpaId=<?= $kpa->getId() ?>');" <?if($disableBtn){?>disabled="disabled"<?}?>/>
      <?php if( $kpa->getKPAType() == "bonus" ){ ?>
      <input type="button" value="Ignore" class="" onclick="redirect('page_eval_ignore_bonus_kpa.php?moduleType=<?=$moduleType?>&kpaId=<?= $kpa->getId() ?>&evalYearId=<?= $selectedYear->getId() ?>&evalPeriod=<?= $selectedPeriod ?>');" <?if($disableBtn){?>disabled="disabled"<?}?>/>
      <?php } ?>
    </td>
  </tr>
<?
  }
?>
</table>