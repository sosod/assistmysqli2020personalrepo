<table class="info full" cellspacing="0">
  <tr>
    <th>Name:</th>
    <td><?= $selectedUser->getFullName() ?></td>
    <th>Manager:</th>
    <td><?= $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ?></td>
  </tr>
  <?php /*
  <tr>
    <th>Gender:</th>
    <td><?= $selectedUser->getGenderDisplay() ?></td>
    <th>Race:</th>
    <td><?= $selectedUser->getRace() ?></td>
  </tr>
  */?>
  <tr>
    <th>Department:</th>
    <td><?= $selectedUser->getDepartment() ?></td>
    <th>Section:</th>
    <td><?= $selectedUser->getSection() ?></td>
  </tr>
  <tr>
    <th>Job title:</th>
    <td><?= $selectedUser->getJobTitle() ?></td>
    <th>Job level:</th>
    <td><?= $selectedUser->getJobLevel() ?></td>
  </tr>
  <tr>
    <th>Job number</th>
    <td colspan="3"><?= $selectedUser->getJobNumber() ?></td>
  </tr>
</table>