<?php
include("report_inc.php");

$errors = array();

$departmentId = $_REQUEST["departmentId"];
$jobLevel = $_REQUEST["jobLevel"];

if( isset( $selectedYear ) && isset( $selectedPeriod ) )
{
  $departments = getDepartments( $departmentId );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Reporting :: Bell Curve Graphs</h1>

    <form action="<?=$moduleType?>_report_bellcurve.php" id="frmUserSelect" method="get" >
      <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
        <tr>
          <td valign="middle">
           <strong>Evaluation year:</strong> <?= select( "selectedYearId", getEvalYearSelectList(), ( isset( $selectedYear ) ? $selectedYear->getId() : "" ), true ) ?>
           <strong>Evaluation period:</strong> <?= select( "selectedPeriod", getTypePeriodSelectList( $moduleType ), $selectedPeriod, true ) ?>
           <strong>Department:</strong> <?= select( "departmentId", getDepartmentSelectList(), $departmentId, false, false, "All" ) ?>
           <strong>Job Level:</strong> <?= select( "jobLevel", getNumericSelectList(1,20), $jobLevel, false, false, "All" ) ?>
          </td>
          <td valign="middle"><input type="submit" value="Select" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="year_period_select" />
    </form>
    <hr/>

    <?
      if( isset( $selectedYear ) && isset( $selectedPeriod ) )
      {
    ?>

    <h5>Period <?= getPeriodStartDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?> to <?= getPeriodEndDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></h5>
    <br/>

    <script type="text/javascript" src="amline/swfobject.js"></script>

    <? foreach( $departments as $department ){ ?>
    <div id="department-graph">
      <h5><?= $department["value"] ?> department</h5>

      <div id="graph_<?=$department["id"]?>">
    		<strong>You need to upgrade your Flash Player</strong>
    	</div>

    	<script type="text/javascript">
    		// <![CDATA[
    		var so = new SWFObject("amline/amline.swf", "amline", "520", "400", "8", "#FFFFFF");
    		so.addVariable("path", "amline/");
    		so.addVariable("settings_file", encodeURIComponent("charts/bell_curve_settings.xml?<?php echo mktime();?>")); // you can set two or more different settings files here (separated by commas)
    		so.addVariable("data_file", encodeURIComponent("<?=$moduleType?>_report_bellcurve_data.php?selectedYearId=<?= $selectedYear->getId() ?>&selectedPeriod=<?= $selectedPeriod ?>&departmentId=<?=$department["id"]?>&jobLevel=<?=$jobLevel?>"));
        so.addVariable("loading_settings", "LOADING SETTINGS");
        so.addVariable("loading_data", "LOADING DATA");
        so.addVariable("preloader_color", "#666666");
    		so.write("graph_<?=$department["id"]?>");
    		// ]]>
    	</script>
    </div>
    <? } ?>

    <?
      }
      else
      {
    ?>
    <p class="instruction">Please select an evaluation year and period above to continue.</p>
    <?
      }
    ?>
  </body>
</html>