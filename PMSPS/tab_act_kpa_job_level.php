<? if( count( $jobLevelKPAs ) < $maxJobLevelKPAs && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New KPI linked to job level" onclick="redirect('<?=$moduleType?>_activate_kpa.php?kpaType=job_level')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="15%">Name</th>
    <th width="25%">Description</th>
    <th width="25%">Measurement</th>
    <th width="20%">Comments</th>
    <th width="15%">Actions</th>
  </tr>
  <?
    if( isset( $jobLevelKPAs ) && count( $jobLevelKPAs ) > 0 )
    {
      foreach( $jobLevelKPAs as $kpa ) {
  ?>
  <tr>
    <td><?= $kpa->getName() ?></td>
    <td><?= $kpa->getDescription() ?></td>
    <td><?= $kpa->getMeasurement() ?></td>
    <td><?= $kpa->getComments() ?></td>
    <td>
      <input type="button" value="View" onclick="redirect('<?=$moduleType?>_activate_kpa_info.php?id=<?= $kpa->getId() ?>');" />
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_kpa.php?id=<?= $kpa->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_kpa.php', '<?= $kpa->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="5">No KPIs linked to job level</td></tr>
  <?
    }
  ?>
</table>