<? if( count( $professionalBodies ) < $maxProfessionalBodies && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New Profession Body" onclick="redirect('<?=$moduleType?>_activate_professional_body.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="25%">Institution</th>
    <th width="12%">Member Since</th>
    <th width="12%">Membership Level</th>
    <th width="35%">Description</th>
    <th width="16%">Actions</th>
  </tr>
  <?
    if( isset( $professionalBodies ) && count( $professionalBodies ) > 0 )
    {
      foreach( $professionalBodies as $professionalBody ) {
  ?>
  <tr>
    <td><?= $professionalBody->getName() ?></td>
    <td><?= $professionalBody->getMemberSince() ?></td>
    <td><?= $professionalBody->getMemberLevel() ?></td>
    <td><?= $professionalBody->getDescription() ?></td>
    <td>
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_professional_body.php?id=<?= $professionalBody->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_professional_body.php', '<?= $professionalBody->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="4">No Professional Bodies</td></tr>
  <?
    }
  ?>
</table>