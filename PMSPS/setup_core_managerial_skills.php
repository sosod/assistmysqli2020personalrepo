<?php
include("inc_ignite.php");
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Core Managerial Skills</h1>
    
    <table class="list full" cellspacing="0">
      <tr>
        <th>Skill</th>
				<th>Definition</th>
				<th>Active</th>
        <th>Actions</th>        
      </tr>
  	  <?
      foreach( CoreManagerialSkill::getList() as $coreManagerialSkill )
      {
  	  ?>
      <tr>
        <td><?= $coreManagerialSkill->getSkill() ?></td>
				<td><?= $coreManagerialSkill->getDefinition() ?></td>
				<td><?= boolValue( $coreManagerialSkill->getActive() ) ?></td>
        <td>
          <input type="button" value="Edit" onclick="redirect('setup_core_managerial_skill.php?id=<?= $coreManagerialSkill->getId() ?>');">
        </td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');"/>
      <input type="button" value="Add" onclick="redirect('setup_core_managerial_skill.php');">
    </div>  
  </body>
</html>