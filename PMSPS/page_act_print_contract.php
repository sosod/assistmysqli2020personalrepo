<?php
include("activate_inc.php");

$action = $_REQUEST["action"];

if( isset( $selectedUser ) == false || isset( $selectedYear ) == false )
{
  redirect($moduleType."_activate.php");
}

require('fpdf/IgnitePDF.class.php');

if( $action == "agreement" )
{
  $manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

  $pdf=new IgnitePDF('P','pt','A4');
  $pdf->AddPage();

  $pdf->Ln(100);

  $pdf->H1("PERFORMANCE AGREEMENT","C","BU");
  $pdf->H1("MADE AND ENTERED INTO BY AND BETWEEN:","C");
  $pdf->H1( getCompanyName() ,"C","I");
  $pdf->H1("AS REPRESENTED BY ","C");
  $pdf->H1( $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ,"C","I");
  $pdf->H3("(herein and after referred as Employer)","C","");
  $pdf->H1("AND","C");
  $pdf->H1( $selectedUser->getTitle() . " " . $selectedUser->getDecodedFullName() ,"C","I");
  $pdf->H1( $selectedUser->getJobTitle(),"C","I");
  $pdf->H3("(herein and after referred as Employee)","C","");
  $pdf->H1("FOR THE FINANCIAL YEAR:","C");
  $pdf->H1( date( "d F Y ", strtotime($selectedYear->getStart()) ) . " - " . date( "d F Y ", strtotime($selectedYear->getEnd())), "C","I");

  $pdf->AddPage();

  $pdf->H3("1. INTRODUCTION");

  $textList = array( array("","1.1","The Employer has entered into a contract of employment with the Employee in terms of section 57(1)(a) of the Local Government: Municipal Systems Act 32 of 2000 (\"the Systems Act\").  The Employer and the Employee are hereinafter referred as \"the Parties\"."),
                     array("","1.2","Section 57(1)(b) of the Systems Act, read with the Contract of Employment concluded between the parties, requires the parties to conclude an annual performance Agreement."),
                     array("","1.3","The parties wish to ensure that they are clear about the goals to be achieved, and secure the commitment of the Employee to a set of outcomes that will secure local government policy goals."),
                     array("","1.4","The parties wish to ensure that there is compliance with Sections 57(4A), 57(4B) and 57(5) of the Systems Act.") );

  $pdf->textList( $textList );

  $pdf->H3("2. PURPOSE OF THIS AGREEMENT");
  $textList = array( array("","2.1","Comply with the provisions of Section 57(1)(b),(4A) and (5) of the Systems Act as well as the Contract of Employment entered into between the parties;"),
                     array("","2.2","Specify objectives and targets established for the Employee and to communicate to the Employee the Employer's expectations of the Employee's performance expectations and accountabilities;"),
                     array("","2.3","Specify accountabilities as set out in the Performance Plan (Annexure A);"),
                     array("","2.4","Monitor and measure performance against set targeted outputs;"),
                     array("","2.5","Use the Performance Agreement and Performance Plan as the basis for assessing the suitability of the Employee for permanent employment and/or to assess whether the Employee has met the performance expectations applicable to his/her job;"),
                     array("","2.6","Appropriately reward the Employee in accordance with the Employer's performance management policy in the event of outstanding performance; and"),
                     array("","2.7","Give effect to the Employer's commitment to a performance-orientated relationship with the Employee in attaining equitable and improved service delivery.") );

  $pdf->textList( $textList );

  $pdf->H3("3. DELIVERY");

  $textList = array( array("","3.1","This Agreement will commence on the " . date( "d F Y ", strtotime($selectedYear->getStart()) ) . " and will remain in force until " . date( "d F Y ", strtotime($selectedYear->getEnd()) ) . " where after a new Performance Agreement shall be concluded between the parties for the next financial year or any portion thereof."),
                     array("","3.2","The parties will review the provisions of this Agreement during June each year.  The parties will conclude a new Performance Agreement that replaces this Agreement at least once a year by not later than the beginning of each successive financial year."),
                     array("","3.3","This Agreement will terminate on the termination of the Employee's contract of employment for any reason."),
                     array("","3.4","The content of this Agreement may be revised at any time during the abovementioned period to determine the applicability of the matters agreed upon.") );

  $pdf->textList( $textList );

  $pdf->AddPage();

  $pdf->H3("4. PERFORMANCE OBJECTIVES");

  $textList = array( array("","4.1","The Performance Plan (Annexure A) sets out -"),
                     array("","","4.1.1","The performance objectives and targets that must be met by the Employee; and"),
                     array("","","4.1.2","The time frames within which those performance objectives and targets must be met."),
                     array("","4.2","The performance objectives and targets reflected in Annexure A are set by the Employer in consultation with the Employee and based on the Integrated Development Plan, Service Delivery and Budget Implementation Plan (SDBIP) and the Budget of the Employer, and shall include key objectives; key performance indicators; target dates and weightings."),
                     array("","4.3","The Employee's performance will, in addition, be measured in terms of contributions to the goals and strategies set out in the Employer's Integrated Development Plan.") );

  $pdf->textList( $textList );

  $pdf->H3("5. THE EMPLOYEE AGREES TO PARTICIPATE IN THE PERFORMANCE MANAGEMENT AND DEVELOPMENT SYSTEM.");

  $textList = array( array("","5.1","The Employee undertakes to actively focus towards the promotion and implementation of the Key Performance Areas KPI's (including special projects relevant to the employee's responsibilities) within the local government framework."),
                     array("","5.2","The criteria upon which the performance of the Employee shall be assessed shall consist of two components, both of which shall be contained in the Performance Agreement."),
                     array("","5.3","The Employee's assessment will be based on his/her performance in terms of the outputs/outcomes (performance indicators) identified as per attached Performance Plan, which are linked to the KPI's, and will constitute 80% of the overall assessment result as per the weightings agreed to between the Employer and Employee:") );

  $pdf->textList( $textList );

  $rows = array();

  $cnt = 1;
  foreach( ContractKPA::getList() as $kpa )
  {
    $rows[] = array( $cnt, $kpa->getName() );
    $cnt++;
  }

  $pdf->listTable( array( "Number", "Key Performance Area" ), $rows, array( 50, 350 ), "L", 100 );
  $pdf->SetX(100);
  $pdf->H3("Weight: " . $selectedUserStatus->getOrgKPAPercentage() ."%");

  $textList = array( array("","5.4","The CCR's will make up the other " . $selectedUserStatus->getManKPAPercentage() ."% of the Employee's assessment score.  CCR's that are deemed to be most critical for the Employee's specific job are reflected in the list below as agreed to between the Employer and Employee:") );
  $pdf->textList( $textList );

  $cnt = 1;
  $manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
  foreach( $manKPAs as $kpa )
  {
    $rows[] = array( $cnt, $kpa->getCoreManagerialSkill()->getSkill() );
    $cnt++;
  }

  $pdf->listTable( array( "Number", "Core Managerial Competency" ), $rows, array( 50, 350 ), "L", 100 );
  $pdf->SetX(100);
  $pdf->H3("Weight: " . $selectedUserStatus->getManKPAPercentage() ."%");

  $pdf->AddPage();

  $pdf->H3("6. PERFORMANCE MANAGEMENT SYSTEM");

  $textList = array( array("","6.1","The Performance Plan (Annexure A) to this Agreement sets out -"),
                     array("","","6.1.1","The standards and procedures for evaluating the Employee's performance; and"),
                     array("","","6.1.2","The intervals for the evaluation of the Employee's performance."),
                     array("","6.2","Despite the establishment of agreed intervals for evaluation, the Employer may in addition review the Employee's performance at any stage while the contract of employment remains in force."),
                     array("","6.3","Personal growth and development needs identified during any performance review discussion must be documented in a Personal Development Plan as well as the actions agreed to and implementation must take place within set time frames."),
                     array("","6.4","The Employee's performance will be measured in terms of contributions to the goals and strategies set out in the Employer's Integrated Development Plan (IDP)."),
                     array("","6.5","The assessment of the performance of the Employee will be based on the following rating scale for KPI's and CCRs:"));

  $pdf->textList( $textList );
  $rows = array();

  $rows[] = array("5","Outstanding performance","Performance far exceeds the standard expected of an employee at this level. The appraisal indicates that the Employee has achieved above fully effective results against all performance criteria and indicators as specified in the PA and Performance plan and maintained this in all areas of responsibility throughout the year.");
  $rows[] = array("4","Performance significantly above expectations","Performance is significantly higher than the standard expected in the job.  The appraisal indicates that the Employee has achieved above fully effective results against more than half of the performance criteria and indicators and fully achieved all others throughout the year.");
  $rows[] = array("3","Fully effective","Performance fully meets the standards expected in all areas of the job.  The appraisal indicates that the Employee has fully achieved effective results against all significant performance criteria and indicators as specified in the PA and Performance Plan.");
  $rows[] = array("2","Not fully effective","Performance is below the standard required for the job in key areas.  Performance meets some of the standards expected for the job.  The review/assessment indicates that the employee has achieved below fully effective results against more than half the key performance criteria and indicators as specified in the PA and Performance Plan.");
  $rows[] = array("1","Unacceptable performance","Performance does not meet the standard expected for the job.  The review/assessment indicates that they employee has achieved below fully effective results against almost all of the performance criteria and indicators as specified in the PA and Performance Plan.  The employee has failed to demonstrate the commitment or ability to bring performance up to the level expected in the job despite management efforts to encourage improvement.");

  $pdf->listTable( array( "Level", "Terminology", "Description" ), $rows, array( 50, 100, 300 ), "L", 50 );

  $textList = array();
  $textList[] = array("","6.6","For purposes of evaluating the performance of the Employee, an evaluation panel constituted of the following persons will be established -");

  $cnt = 1;
  $panelMembers = getPanelMembers( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

  foreach( $panelMembers as $panelMember )
  {
    $textList[] = array("","","6.6." . $cnt,$panelMember->getPanelTitle()->getName() );
    $cnt++;
  }

  $pdf->textList( $textList );

  $pdf->AddPage();

  $pdf->H3("7. SCHEDULE FOR PERFORMANCE REVIEWS");

  $numOfPeriods = 4;
  $evalPeriodType = getTypeCategory( $moduleType )->getEvalPeriodType();

  if( $evalPeriodType == "bi_annual" )
  {
    $numOfPeriods = 2;
  }

  $sentence = "The performance of each Employee in relation to his/her performance agreement shall be reviewed for the following periods within one month after the period ended";

  if( $numOfPeriods == 4 )
  {
    $sentence .= " with the understanding that the reviews for the first and third quarter may be verbal if performance is satisfactory";
  }

  $sentence .= ":";

  $textList = array( array("","7.1",$sentence) );
  $pdf->textList( $textList );

  for( $i = 1; $i <= $numOfPeriods; $i++ )
  {
    debug( "Period " . $i . " : " . getPeriodStartDate( $evalPeriodType, $selectedYear, $i ) . " - " . getPeriodEndDate( $evalPeriodType, $selectedYear, $i ) );
    $pdf->TextLine( "Period " . $i . " : " . getPeriodStartDate( $evalPeriodType, $selectedYear, $i ) . " - " . getPeriodEndDate( $evalPeriodType, $selectedYear, $i ), 70 );
  }

  $pdf->Ln();

  $textList = array( array("","7.2","The Employer shall keep a record of the mid-year review and annual assessment meetings."),
                     array("","7.3","Performance feedback shall be based on the Employer's assessment of the Employee's performance."),
                     array("","7.4","The Employer will be entitled to review and make reasonable changes to the provisions of Annexure A from time to time for operational reasons.  The Employee will be fully consulted before any such change is made."),
                     array("","7.5","The Employer may amend the provisions of Annexure A whenever the performance management system is adopted, implemented and/or amended as the case may be. In that case, the Employee will be fully consulted before any such change is made."));

  $pdf->textList( $textList );

  $pdf->H3("8. DEVELOPMENTAL REQUIREMENTS");
  $pdf->TextLine( "The Personal Development Plan (PDP) for addressing developmental gaps is attached as Annexure B. Such Plan may be implemented and/or amended as the case may be after the each assessment.  In that case, the Employee will be fully consulted before any such change or plan is made.", 50 );
  $pdf->Ln();

  $pdf->H3("9. OBLIGATIONS OF THE EMPLOYER");

  $textList = array( array("","9.1","The Employer shall-"),
                     array("","","9.1.1","Create an enabling environment (funding, resources, equipment) to facilitate effective performance by the employee;"),
                     array("","","9.1.2","Provide access to skills development and capacity building opportunities;"),
                     array("","","9.1.3","Work collaboratively with the Employee to solve problems and generate solutions to common problems that may impact on the performance of the Employee;"),
                     array("","","9.1.4","On the request of the Employee delegate such powers reasonably required by the Employee to enable him/her to meet the performance objectives and targets established in terms of this Agreement; and"),
                     array("","","9.1.5","Make available to the Employee such resources as the Employee may reasonably require from time to time assisting him/her to meet the performance objectives and targets established in terms of this Agreement.") );

  $pdf->textList( $textList );

  $pdf->H3("10. CONSULTATION");

  $textList = array( array("","10.1","The Employer agrees to consult the Employee timeously where the exercising of the powers will have amongst others-"),
                     array("","","10.1.1","A direct effect on the performance of any of the Employee's functions;"),
                     array("","","10.1.2","Commit the Employee to implement or to give effect to a decision made by the Employer; and"),
                     array("","","10.1.3","A substantial financial effect on the Employer."),
                     array("","10.2","The Employer agrees to inform the Employee of the outcome of any decisions taken pursuant to the exercise of powers contemplated in clause 12.1 as soon as is practicable to enable the Employee to take any necessary action with delay.") );

  $pdf->textList( $textList );

  $pdf->H3("11. REWARD");

  $textList = array( array("","11.1", "After the annual evaluation exercise has been completed, the employee will be eligible to receive a performance bonus based on the points calculated based on the weightings allocated to each performance objective in \"Annexure A\" of this agreement." ),
                     array("","11.2", "The performance bonus will be awarded based on the following scheme: \nPerformance Rating Bonus Amount:" ) );

  $pdf->textList( $textList );

  $bonusPolicies = getBonusPolicies( $selectedYear->getId() );

  foreach( $bonusPolicies as $bonusPolicy )
  {
    $pdf->TextLine( $bonusPolicy->getLowerLimit() . "% - " . $bonusPolicy->getUpperLimit() . "% - " . $bonusPolicy->getName() . " - " . $bonusPolicy->getPercentage() . "% of Total package", 80 );
  }

  $pdf->Ln();

  $pdf->H3("12. MANAGEMENT OF EVALUATION OUTCOMES");

  $textList = array( array("","12.1", "Where the employer is, at any time during the employees employment, not satisfied with the manager's performance with respect to any matter dealt with in this Agreement, the employer will give notice the employee to attend a meeting." ),
                     array("","12.2", "The employee will have the opportunity at the meeting to satisfy the employer of the measures being taken to ensure that his performance becomes satisfactory and any programme, including any dates, for implementing these measures." ),
                     array("","12.3", "Where there is a dispute or difference as to the performance of the employee under this Agreement, the parties will confer with a view to resolving the dispute or difference." ),
                     array("","12.4", "In the case of unacceptable performance, the employer shall -" ),
                     array("","","12.4.1", "Provide systematic remedial or developmental support to assist the Employee to improve his or her performance; and" ),
                     array("","","12.4.2", "After appropriate performance counselling and having provided the necessary guidance and/or support as well as reasonable time for improvement in performance, the Employer may consider steps to terminate the contract of employment of the Employee on grounds of unfitness or incapacity to carry out his or her duties." ) );

  $pdf->textList( $textList );

  $pdf->H3("13. DISPUTE RESOLUTION");

  $textList = array( array("","13.1", "In the event that the employee is dissatisfied with any decision or action of the Council in terms of this Agreement, or where a dispute or difference arises as to the extent to which the employee has achieved the performance objectives and targets established in terms of this Agreement, the employee may within 3 working days meet with the employer with a view to resolving the issue. The employer will record the outcome of the meeting in writing." ),
                     array("","13.2", "In the event that the employee remains dissatisfied with the outcome of that meeting, he/she may raise the issue in writing within 5 working days with the Mayor. The Mayor in consultation with the aggrieved employee will determine a process for resolving the issue, which will involve at least providing the manager with an opportunity to state his case orally or in writing within 5 working days.  The Mayor will record the decision on the issue in writing. The decision on the issue will be made within 10 days of the issue being raised." ),
                     array("","13.3", "The outcome of the meetings contemplated in 13.1 and 13.2 must form part of the annual review report to the Mayor. If the parties could not resolve the issues, an independent arbiter, acceptable to both parties, should be appointed to resolve the matter." ),
                     array("","","13.3.1", "In the instance where the matters referred to in 13.3 were not successful, should the matter be referred to the MEC for local government in the province within thirty (30) days of receipt of a formal dispute from the Employee; or" ),
                     array("","","13.3.2", "Any other person appointed by the MEC." ),
                     array("","13.4", "In the event that the mediation process contemplated above fails, the relevant clause of the Contract of Employment shall apply." ) );

  $pdf->textList( $textList );

  $pdf->H3("14. GENERAL");

  $textList = array( array("","14.1", "The contents of this agreement and the outcome of any review conducted in terms of Annexure A may be made available to the public by the Employer." ),
                     array("","14.2", "Nothing in this agreement diminishes the obligations, duties or accountabilities of the Employee in terms of his/her contract of employment, or the effects of existing or new regulations, circulars, policies, directives or other instruments." ) );

  $pdf->textList( $textList );

  $pdf->AddPage();

  $pdf->Ln(80);
  $pdf->SetFont('Arial','',9);
  $pdf->Cell(300,21,"Signed and accepted by the Employee",'T',1);
  $pdf->Ln(40);
  $pdf->Cell(300,21,"Date",'T',1);
  $pdf->Ln(80);
  $pdf->Cell(300,21,"Signed and accepted by the Manager",'T',1);
  $pdf->Ln(40);
  $pdf->Cell(300,21,"Date",'T',1);

  $pdf->Output( str_replace(" ", "_", strtolower($selectedUser->getDecodedFullName())) . '_agreement.pdf','I');
}
else if( $action == "annexure_a" )
{
  $orgKPAs = getOrganisationalKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
  $manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

  $pdf=new IgnitePDF('P','pt','A4');
  $pdf->AddPage();

  $pdf->H1( "ANNEXURE A","C" );
  $pdf->H1( "PERFORMANCE PLAN","C" );
  $pdf->H2( $selectedUser->getTitle() . " " . $selectedUser->getDecodedFullName() ,"C","I");
  $pdf->H2( $selectedUser->getJobTitle(),"C","I");

  $pdf->AddPage('L');

  $pdf->H3( "KEY PERFORMANCE INDICATORS" );
  $pdf->TextLine( "The following Key Performance Indicators (KPI's) provide the details of the evidence that must be provided to show that a key objective has been obtained. The weightings show the relative importance of the key objectives to each other and should add up to " . $selectedUserStatus->getOrgKPAPercentage() . "%.", 30 );
  $pdf->Ln();
  $headers = array( "National KPA", "Objective", "Key Performance Indicator", "Definition", "Baseline", "Target Unit", "Target Q1", "Target Q2", "Target Q3", "Target Q4", "Weight" );
  $widths = array( 90, 110, 110, 80, 70, 70, 50, 50, 50, 50, 50 );
  $rows = array();

  foreach( $orgKPAs as $kpa )
  {
    $rows[] = array( $kpa->getKPI(), $kpa->getObjective(), $kpa->getName(), $kpa->getMeasurement(), $kpa->getBaseline(), $kpa->getTargetUnit(), $kpa->getTarget1(), $kpa->getTarget2(), $kpa->getTarget3(), $kpa->getTarget4(), $kpa->getWeight() );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->AddPage('L');

  $pdf->H3( "CORE COMPETENCY FRAMEWORK" );

  $headers = array( "Core Managerial Skill", "Level", "Definition", "Weight", "Comments");
  $widths = array( 150, 50, 280, 50, 250 );
  $rows = array();

  foreach( $manKPAs as $kpa )
  {
    $rows[] = array( $kpa->getCoreManagerialSkill()->getSkill(), $kpa->getSkillLevel(), $kpa->getCoreManagerialSkill()->getDefinition(), $kpa->getWeight(), $kpa->getComments() );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->Output( str_replace(" ", "_", strtolower($selectedUser->getDecodedFullName())) . '_annexure_a.pdf','I');
}
else if( $action == "annexure_b" )
{
  $learningActivities = getLearningActivities( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

  $pdf=new IgnitePDF('L','pt','A4');
  $pdf->AddPage();

  $pdf->H1( "ANNEXURE B","C" );
  $pdf->H1( "PERSONAL DEVELOPMENT PLAN (PDP)", 'C' );
  $pdf->H2( $selectedUser->getTitle() . " " . $selectedUser->getDecodedFullName() ,"C","I");
  $pdf->H2( $selectedUser->getJobTitle(),"C","I");

  $headers = array( "Skills Performance Gap (in order of priority)",
                    "Outcomes Expected (measurable indicators: quantity, quality and time frames)",
                    "Suggested training and /or development activity",
                    "Suggested mode of delivery",
                    "Suggested Time Frames",
                    "Work opportunity created to practice skill/development area",
                    "Support Person" );

  $widths = array( 120, 110, 110, 110, 110, 110, 110 );
  $rows = array();

  foreach( $learningActivities as $learningActivity )
  {
    $rows[] = array( $learningActivity->getName(),
                     $learningActivity->getOutcome(),
                     $learningActivity->getTraining(),
                     $learningActivity->getDeliveryMode(),
                     $learningActivity->getTimeFrames(),
                     $learningActivity->getWorkOpportunity(),
                     $learningActivity->getSupportPerson() );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->AddPage();

  $pdf->Ln(80);
  $pdf->SetFont('Arial','',9);
  $pdf->Cell(300,21,"Signed and accepted by the Employee",'T',1);
  $pdf->Ln(40);
  $pdf->Cell(300,21,"Date",'T',1);
  $pdf->Ln(80);
  $pdf->Cell(300,21,"Signed and accepted by the Manager",'T',1);
  $pdf->Ln(40);
  $pdf->Cell(300,21,"Date",'T',1);

  $pdf->Output( str_replace(" ", "_", strtolower($selectedUser->getDecodedFullName())) . '_annexure_b.pdf','I');
}

exit;
?>