<? if( count( $careerGoals ) < $maxCareerGoals && ( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ) ){ ?>
<div class="actions">
  <input type="button" value="New career goal" onclick="redirect('<?=$moduleType?>_activate_career_goal.php')" />
</div>
<? } ?>

<table class="list full" cellspacing="0">
  <tr>
    <th width="30%">Name</th>
    <th width="50%">Description</th>
    <th width="20%">Actions</th>
  </tr>
  <?
    if( isset( $careerGoals ) && count( $careerGoals ) > 0 )
    {
      foreach( $careerGoals as $careerGoal ) {
  ?>
  <tr>
    <td><?= $careerGoal->getName() ?></td>
    <td><?= $careerGoal->getDescription() ?></td>
    <td>
      <? if( $selectedUserStatus->isActivationMutable() || $isManagedByLoggedInUser ){ ?>
      <input type="button" value="Edit" onclick="redirect('<?=$moduleType?>_activate_career_goal.php?id=<?= $careerGoal->getId() ?>');" />
      <input type="button" value="Delete" onclick="confirmDelete( '<?=$moduleType?>_activate_career_goal.php', '<?= $careerGoal->getId() ?>');" />
      <? } ?>
    </td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="4">No career goals</td></tr>
  <?
    }
  ?>
</table>