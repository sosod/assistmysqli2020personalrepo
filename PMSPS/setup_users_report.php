<?php
include("inc_ignite.php");

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - User Overview Report</h1>
		
    <table class="list full" cellspacing="0">
      <tr>
        <th>User</th>
        <th>Ignite Assist Login?</th>
        <th>Category</th>
        <th>Create</th>
				<? /*<th>Approve</th>*/ ?>
				<th>HR Manager</th>
				<th>Section</th>
				<th>Job Level</th>
				<th>Job Number</th>
        <th>Access</th>        
      </tr>
      <?
      $usersWithSetup = getUsersWithSetup();
      foreach( $usersWithSetup as $userWithSetup )
      {
        $user = $userWithSetup[0];
        $userSetup = $userWithSetup[1];
  	  ?>
      <tr>
        <td><?= $user->getFullName() ?></td>
				<td><?= $user->getStatus() == 1 ? "Yes" : "No" ?></td>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getCategory()->getName() ?></td>
				<td><?= $userSetup->isTransient() ? "Not set" : boolValue( $userSetup->getCreatePermission() ) ?></td>
				<? /*<td><?= $userSetup->isTransient() ? "Not set" : boolValue( $userSetup->getApprovePermission() ) ?></td> */?>
				<td><?= $userSetup->isTransient() ? "Not set" : boolValue( $userSetup->getHRManager() ) ?></td>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getSection() ?></td>
        <td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getJobLevel() ?></td>
				<td><?= $userSetup->isTransient() ? "Not set" : $userSetup->getJobNumber() ?></td>
        <td>
        	<?
					  $userAccess = new UserAccess();
            $userAccess->loadFromUserId( $user->getId() );
						
						if( count( $userAccess->getSections() ) > 0 )
            {
              foreach( $userAccess->getSections() as $section )
              { 
                echo UserAccess::displayAccessSection( $section ) . "<br/>";
              } 
            }
            else
            {
              echo "No Access"; 
            }
					?>
        </td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_users.php');" />
    </div>  
  </body>
</html>