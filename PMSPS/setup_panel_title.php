<?php
include("inc_ignite.php");


$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$panelTitle = new PanelTitle();

if( $action == "save" )
{
  $panelTitle->loadFromRequest( $_REQUEST );
  $panelTitle->save();
  
  redirect("setup_panel_titles.php" );
}

if( exists( $id ) )
{
  $panelTitle->loadFromId( $id );
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmEvalPanelTitle").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Panel Title</h1>
    
    <form id="frmEvalPanelTitle" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>Name</th>
          <td><input type="text" name="name" value="<?= $panelTitle->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Active</th>
          <td><?= select( "active", getYesNoSelectList(), $panelTitle->getActive(), true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $panelTitle->getId() ?>" />
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_panel_titles.php');" />
    </div>  
  </body>
</html>