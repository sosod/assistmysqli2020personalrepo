<?
include("evaluate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];
$skillType = $_REQUEST["skillType"];

$skill = new EvaluationSkill();

if( $action == "save" )
{
  $skill->loadFromRequest( $_REQUEST );
  $skill->save();
  
  redirect( $moduleType."_evaluate_period.php" );
}

if( exists( $id ) )
{
  $skill->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmSkill").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Evaluation :: <?= EvaluationSkill::displayType( $skillType ) ?></h1>
    <form id="frmSkill" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Message:</th>
          <td><textarea name="message" cols="50" rows="5" class="required"><?= $skill->getMessage() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?=$moduleType?>" />          
      <input type="hidden" name="id" value="<?= $skill->getId() ?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="evalPeriod" value="<?= $selectedPeriod ?>" />      
      <input type="hidden" name="skillType" value="<?= $skillType ?>" />      
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>  
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_evaluate_period.php');" />
    </div>  
  </body>
</html>