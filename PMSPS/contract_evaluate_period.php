<?php
include("inc_ignite.php");
include("contract_inc.php");
include("evaluate_inc.php");

$errors = array();

if( isset( $selectedUser ) == false || isset( $selectedYear ) == false || isset( $selectedPeriod ) == false  )
{
  redirect($moduleType . "_evaluate.php");
}

$action = $_REQUEST["action"];

include("logic_eval_status_change.php");

if( ( $selectedPeriodEvalStatus->getStatus() != "pending" && ( $userSetup->getHRManager() == false && getLoggedInUser()->isAssistSupportUser() == false ) ) ||
    $selectedPeriodEvalStatus->getLocked() )
{
  $disableBtn = true;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Evaluation</h1>

    <? include("frm_eval_user_year_select.php") ?>

    <? include("info_eval_summary.php") ?>

    <?
      $allEvaluated = true;
      $totalMarks = 0;
      $totalPercentage = 0;
      $numOfKPAs = 0;

      include("logic_eval_kpas_org.php");
      include("logic_eval_kpas_man.php");

      if( $useWeights )
      {
        $avgMark = $totalPercentage;
        $finalSymbol = KPAEvaluation::bellCurveSymbolFromPercentage( $totalPercentage );
      }
      else
      {
        $avgMark = ($allEvaluated && $numOfKPAs > 0) ? number_format(round( ($totalMarks / $numOfKPAs) ,1 ), 1, '.', '') : "?";
        $finalSymbol = ($allEvaluated && $numOfKPAs > 0) ? KPAEvaluation::bellCurveSymbol( $totalMarks / $numOfKPAs ) : "?";
      }

      if( $allEvaluated == false )
      {
        $errors[] = "Not all KPIs have been evaluated.";
      }
    ?>

    <? include("frm_eval_complete.php"); ?>

    <div id="tabs" class="tab-group" style="margin-top:15px;">
      <ul>
        <li class="ui-tabs-nav-item"><a href="#setup">Setup</a></li>
        <li class="ui-tabs-nav-item"><a href="#kpas">KPIs</a></li>
        <li class="ui-tabs-nav-item"><a href="#strong_areas">Strengths</a></li>
        <li class="ui-tabs-nav-item"><a href="#dev_areas">Development areas</a></li>
        <li class="ui-tabs-nav-item"><a href="#learning">Learning activities</a></li>
        <li class="ui-tabs-nav-item"><a href="#comments">Comments</a></li>
      </ul>
      <div id="setup">
        <? include("tab_eval_setup.php"); ?>
      </div><!-- end setup -->
      <div id="kpas">

        <h6>Organisational KPIs</h6>
        <? include("tab_eval_kpas_org.php"); ?>

        <h6>Managerial KPIs</h6>
        <? include("tab_eval_kpas_man.php"); ?>

        <p class="evaluation-mark">Final Evaluation Mark: <strong><?= $avgMark ?><?= $useWeights ? "%" : "" ?> - <?= $finalSymbol ?></strong></p>

      </div><!-- end kpas -->
      <div id="strong_areas">
        <? include("tab_eval_skills_strong.php"); ?>
      </div><!-- end strong areas -->
      <div id="dev_areas">
        <? include("tab_eval_skills_dev.php"); ?>
      </div><!-- end dev areas -->
      <div id="learning">
        <? include("tab_eval_learning_activities.php"); ?>
      </div><!-- end dev areas -->
      <div id="comments">
        <? include("tab_eval_comments.php"); ?>
      </div><!-- end comments -->
    </div><!-- end tabs -->
  </body>
</html>

