<?php
include("report_inc.php");

$errors = array();

if( isset( $selectedYear ) )
{
  $orderBy = $_REQUEST["orderBy"];
  if( exists( $orderBy ) == false )
  {
    $orderBy = "tkname";
  }
  $orderByDir = $_REQUEST["orderByDir"];
  if( exists( $orderByDir ) == false )
  {
    $orderByDir = "ASC";
  }

  $departmentId = $_REQUEST["departmentId"];
  $section = $_REQUEST["section"];
  $managerId = $_REQUEST["managerId"];
  $status = $_REQUEST["status"];

  /* BUILD UP THE SQL QUERY */
	$sql = "SELECT tk.tkid,
	               tk.tkname,
					       tk.tksurname,
					       (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_timekeep um WHERE um.tkid = tk.tkmanager ) AS manager,
					       dept.value AS department,
					       us.section
					FROM assist_".getCompanyCode()."_pmsps_user_setup us
					JOIN assist_".getCompanyCode()."_timekeep tk ON tk.tkid = us.user_id
					JOIN assist_".getCompanyCode()."_menu_modules_users m ON m.usrtkid = tk.tkid
					LEFT JOIN assist_".getCompanyCode()."_list_dept dept ON dept.id = tk.tkdept
					WHERE us.category_id = " . getTypeCategoryId( $moduleType ) . "
					AND tk.tkstatus in (1,2)
					AND m.usrmodref = 'PMSPS'";


  if( exists( $departmentId ) )
  {
    $sql .= " AND tk.tkdept = " . $departmentId;
  }

  if( exists( $section ) )
  {
    $sql .= " AND us.section = '" . $section . "' ";
  }

  if( exists( $managerId ) )
  {
    $sql .= " AND tk.tkmanager = '" . $managerId . "'";
  }

  $sql .= " ORDER BY " . $orderBy . " " . $orderByDir;

  debug( $sql );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Reporting :: Activation Status</h1>

    <form action="<?=$moduleType?>_report_status.php" id="frmUserSelect" method="get" >
      <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
        <tr>
          <td valign="middle">
           <strong>Evaluation year:</strong> <?= select( "selectedYearId", getEvalYearSelectList(), ( isset( $selectedYear ) ? $selectedYear->getId() : "" ), true ) ?>
           <strong>Evaluation period:</strong> <?= select( "selectedPeriod", getTypePeriodSelectList( $moduleType ), $selectedPeriod, true ) ?>
          </td>
          <td valign="middle"><input type="submit" value="Select" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="year_period_select" />
    </form>
    <hr/>

    <?
      if( isset( $selectedYear ) )
      {
    ?>

    <h5>Report Filter:</h5>

    <form action="<?=$moduleType?>_report_status.php" id="frmFilter" method="get" class="filter" >
      <div>
				<label>Manager</label>
        <?= select( "managerId", getManagerSelectList(), $managerId, false, false, "All" ) ?>
			</div>
			<div>
				<label>Department</label>
        <?= select( "departmentId", getDepartmentSelectList(), $departmentId, false, false, "All", "width:150px;" ) ?>
        <label>Section</label>
        <?= select( "section", getUserSectionSelectList(), $section, false, false, "All", "width:150px;" ) ?>
      </div>
      <div>
        <label>Order by</label>
				<?= select( "orderBy", getActivationStatusReportOrderBySelectList(), $orderBy, false, false, false  ) ?>
        <?= select( "orderByDir", getOrderByDirectionSelectList(), $orderByDir, false, false, false ) ?>
        <input type="submit" value="Select" />
      </div>
    </form>

    <table class="list full" cellspacing="0" style="margin-top:15px;">
      <tr>
        <th width="15%">User</th>
        <th width="13%">Department</th>
				<th width="15%">Manager</th>
        <th width="13%">Section</th>
        <th width="11%">Activation Status</th>
				<th width="11%">Activation Last Updated</th>
        <th width="11%">Evaluation Status</th>
        <th width="11%">Evaluation Last Updated</th>
      </tr>
      <?
        $data = getListRecords( $sql );

        if( count($data) > 0 )
        {
          foreach( $data as $record )
          {
          	$activationStatus = new ActivationStatus();
						$activationStatus->loadFor( $moduleType, $record["tkid"], $selectedYear->getId() );

						$evaluationStatus = new EvaluationStatus();
            $evaluationStatus->loadFor( $moduleType, $record["tkid"], $selectedYear->getId(), $selectedPeriod );

      ?>
      <tr>
        <td><?= $record["tkname"] ?> <?= $record["tksurname"] ?></td>
        <td><?= $record["department"] ?></td>
        <td><?= $record["manager"] ?></td>
				<td><?= $record["section"] ?></td>
				<td><?= $activationStatus->isTransient() ? "Not Done" : $activationStatus->getStatusDisplay() ?></td>
        <td><?= $activationStatus->isTransient() ? "Not Done" : $activationStatus->getActionOn() ?></td>
        <td><?= $evaluationStatus->isTransient() ? "Not Done" : $evaluationStatus->getStatusDisplay() ?></td>
        <td><?= $evaluationStatus->isTransient() ? "Not Done" : $evaluationStatus->getActionOn() ?></td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr><td colspan="10">No records match the selected filter options.</td></tr>
      <?
        }
      ?>
    </table>
    <?
      }
      else
      {
    ?>
    <p class="instruction">Please select an evaluation year above to continue.</p>
    <?
      }
    ?>
  </body>
</html>