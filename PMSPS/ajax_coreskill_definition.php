<?php
include("inc_ignite.php");

$id = $_REQUEST["coreManagerialSkillId"];
$skillLevel = $_REQUEST["skillLevel"];

$skill = new CoreManagerialSkill();
$skill->loadFromId( $id );

if( exists($skillLevel) )
{
  if( $skillLevel == "B" )
  {
    echo $skill->getDefinitionBasic();
  }
  else if( $skillLevel == "C" )
  {
    echo $skill->getDefinitionCompetent();
  }
  else if( $skillLevel == "A" )
  {
    echo $skill->getDefinitionAdvanced();
  }
  else if( $skillLevel == "E" )
  {
    echo $skill->getDefinitionExpert();
  }
}
else
{
  echo $skill->getDefinition();
}
?>