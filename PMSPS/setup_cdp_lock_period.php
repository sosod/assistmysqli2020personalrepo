<?php
include("inc_ignite.php");


$action = $_REQUEST["action"];
$selectedType = $_REQUEST["selectedType"];
$selectedYear = $_REQUEST["selectedYear"];
$selectedPeriod = $_REQUEST["selectedPeriod"];

if( $action == "lock" )
{
  $category = getTypeCategory( $selectedType );
  $categoryUsers = getCategoryUsers( $category->getId() );
  
  foreach( $categoryUsers as $user )
  {
    $selectedPeriodEvalStatus = new EvaluationStatus();
    $selectedPeriodEvalStatus->loadFor( $selectedType, $user->getId(), $selectedYear, $selectedPeriod ); 
    
    $selectedPeriodEvalStatus->setLocked(true);
    $selectedPeriodEvalStatus->setActionById( getLoggedInUserId() );
    
    if( $selectedPeriodEvalStatus->isTransient() )
    {
      $selectedPeriodEvalStatus->setStatus("pending");
      $selectedPeriodEvalStatus->setUserId( $user->getId() );
      $selectedPeriodEvalStatus->setEvalYearId( $selectedYear );
      $selectedPeriodEvalStatus->setEvalPeriod( $selectedPeriod );              
      $selectedPeriodEvalStatus->setEvalPeriodReviewType( $category->getEvalPeriodType() == "quart_oral" ? "oral" : "formal" );
    }
    
    $selectedPeriodEvalStatus->save(); 
  }  
  
  $msg = "Selected year and period has been locked. No more evaluations can be done in the period.";
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmLockPeriod").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup :: Maintenance :: Lock Period</h1>
    
    <p class="warning">Please note that this action cannot be undone. To unlock an evaluation period you will need to contact Ignite Assist.</p>
    
    <? if( exists($msg) ){ ?>
    <p class="message"><?= $msg ?></p>    
    <? } ?>
    
    <form id="frmLockPeriod" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>Evaluation Period</th>
          <td><?= select( "selectedType", getModuleTypeSelectList(), $selectedType, true ) ?></td>
        </tr> 
        <tr>
          <th>Evaluation year</th>
          <td><?= select( "selectedYear", getEvalYearSelectList(), $selectedYear, true ) ?></td>
        </tr>
        <tr>
          <th>Evaluation Period</th>
          <td><?= select( "selectedPeriod", getTypePeriodSelectList( $selectedType ), $selectedPeriod, true ) ?></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Lock" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="lock" />
      <input type="hidden" name="type" value="<?=$selectedType?>" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');"/>
    </div>  
  </body>
</html>