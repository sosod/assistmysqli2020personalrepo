<?php
include("inc_ignite.php");
?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup</h1>
    <h5>Module configuration</h5>
    <table cellspacing="0" class="info">
      <tr>
        <th width="120px">Evaluation Years</th>
        <td width="350px">Define the evaluation years</td>
        <td align="center"><input type="button" onclick="redirect('setup_eval_years.php');" value="Configure" /></td>
      </tr>
      <tr>
        <th>Categories</th>
        <td>Setup all category related parameters</td>
        <td align="center"><input type="button" onclick="redirect('setup_categories.php');" value="Configure" /></td>
      </tr>
      <tr>
        <th>Users</th>
        <td>Set the user's categories and other module parameters</td>
        <td align="center"><input type="button" onclick="redirect('setup_users.php');" value="Configure" /></td>
      </tr>
      <tr>
        <th>Evaluation panel</th>
        <td>Define the evaluation panel titles</td>
        <td align="center"><input type="button" onclick="redirect('setup_panel_titles.php');" value="Configure" /></td>
      </tr>
      <tr>
        <th>Performance contract KPIs</th>
        <td>Define the key performance indicators for the performance contract</td>
        <td align="center"><input type="button" onclick="redirect('setup_contract_kpas.php');" value="Configure" /></td>
      </tr>
			<tr>
        <th>Core Managerial Skills</th>
        <td>Define the core managerial skills used in the system</td>
        <td align="center"><input type="button" onclick="redirect('setup_core_managerial_skills.php');" value="Configure" /></td>
      </tr>
    </table>
    
    <h5>Maintenance</h5>
    <table cellspacing="0" class="info">
      <tr>
        <th width="120px">Lock evaluation period</th>
        <td width="250px">Lock an evaluation period so that no more evaluations can be done for that period.</td>
        <td align="center">
          <input type="button" onclick="redirect('setup_lock_period.php?selectedType=career');" value="Performance Development" />
          <input type="button" onclick="redirect('setup_lock_period.php?selectedType=perform');" value="Performance Agreement" />
          <input type="button" onclick="redirect('setup_lock_period.php?selectedType=contract');" value="S57 Appointees" />          
        </td>
      </tr>
    </table>
  </body>
</html>
