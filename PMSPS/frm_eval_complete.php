<?
  if( $selectedPeriodEvalStatus->getEvalPeriodReviewType() != "oral" )
  {
    printErrors( $errors ); 
  }
  
  if( count( $errors ) == 0 || $selectedPeriodEvalStatus->getEvalPeriodReviewType() == "oral" )
  {
    if( $selectedPeriodEvalStatus->getStatus() == "pending" ) 
    {
?>    
<form id="frmCompleteEvaluated" method="post">
  <input type="submit" value="Complete Evaluation" />
  <input type="hidden" name="action" value="complete_evaluation" />
  <input type="hidden" name="finalKPASymbol" value="<?= $finalSymbol ?>" />
</form>
<?
    }
    else
    {
?>
<h5>Evaluation completed<?= $selectedPeriodEvalStatus->getLocked() ? " (Locked)" : "" ?></h5>  
<?
    }
  }
?>