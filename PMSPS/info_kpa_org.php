<table class="info" cellspacing="0">
  <tr>
    <th>User:</th>
    <td><?= $selectedUser->getFullName() ?></td>
  </tr>
  <tr>
    <th>Evaluation year:</th>
    <td><?= $selectedYear->getName() ?></td>
  </tr>
  <tr>
    <th>Group Num:</th>
    <td><?= $kpa->getGroupNum() ?></td>
  </tr>
  <tr>
    <th>SDBIP KPI Num:</th>
    <td><?= $kpa->getSDBIPNum() ?></td>
  </tr>
  <tr>
    <th>Name:</th>
    <td><?= $kpa->getName() ?></td>
  </tr>
  <tr>
    <th>Objective:</th>
    <td><?= $kpa->getObjective() ?></td>
  </tr>
  <tr>
    <th>KPI:</th>
    <td><?= $kpa->getKPI() ?></td>
  </tr>
  <tr>
    <th>Unit of Measurement:</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <tr>
    <th>Baseline:</th>
    <td><?= $kpa->getBaseline() ?></td>
  </tr>
  <tr>
    <th>Target unit:</th>
    <td><?= $kpa->getTargetUnit() ?></td>
  </tr>
  <tr>
    <th>Target Q1:</th>
    <td><?= $kpa->getTarget1() ?></td>
  </tr>
  <tr>
    <th>Target Q2:</th>
    <td><?= $kpa->getTarget2() ?></td>
  </tr>
  <tr>
    <th>Target Q3:</th>
    <td><?= $kpa->getTarget3() ?></td>
  </tr>
  <tr>
    <th>Target Q4:</th>
    <td><?= $kpa->getTarget4() ?></td>
  </tr>
  <? if( $useWeights ){ ?>
  <tr>
    <th>Weight:</th>
    <td><?= $kpa->getWeight() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Outcome:</th>
    <td><?= $kpa->getOutcome() ?></td>
  </tr>
  <tr>
    <th>Porfolio of evidence:</th>
    <td><?= $kpa->getEvidence() ?></td>
  </tr>
  <tr>
    <th>Comments:</th>
    <td><?= $kpa->getComments() ?></td>
  </tr>
  <tr>
    <th>Created by</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on</th>
    <td><?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>