<?php
include("inc_ignite.php");
include("evaluate_inc.php");

$moduleType = $_REQUEST["moduleType"];
$kpaId = $_REQUEST["kpaId"];
$evalYearId = $_REQUEST["evalYearId"];
$evalPeriod = $_REQUEST["evalPeriod"];

$kpa = new KPA();
$kpa->loadFromId( $kpaId );

$kpaEvaluation = new KPAEvaluation();
$kpaEvaluation->loadFromRecord( KPAEvaluation::loadWhere( KPAEvaluation::$TABLE_NAME, "kpa_id = " . $kpaId . " and eval_year_id = " . $evalYearId . " and eval_period = " . $evalPeriod ) );

if( isset( $kpaEvaluation ) && $kpaEvaluation->isTransient() == false )
{
  $kpaEvaluation->setSelfEvaluation( 0 );
  $kpaEvaluation->setRating( 0 );
}
else
{
  $kpaEvaluation = new KPAEvaluation();
  $kpaEvaluation->setKPAId( $kpaId );
  $kpaEvaluation->setSelfEvaluation( 0 );
  $kpaEvaluation->setRating( 0 );
  $kpaEvaluation->setUserId( $kpa->getUserId() );
  $kpaEvaluation->setType( $moduleType );
  $kpaEvaluation->setEvalYearId( $evalYearId );
  $kpaEvaluation->setEvalPeriod( $evalPeriod );
}

$kpaEvaluation->setEvaluatedById( getLoggedInUserId() );
$kpaEvaluation->save();



redirect( $moduleType."_evaluate_period.php" );

?>