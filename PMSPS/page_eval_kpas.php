<?php
include("evaluate_inc.php");

$action = $_REQUEST["action"];
$kpas = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

if( $action == "save" )
{
  foreach( $kpas as $kpa )
  {
    $kpaEvaluation = new KPAEvaluation();
    $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
    $kpaEvaluation->loadFromRequest( $_REQUEST );
    $kpaEvaluation->setKPAId( $kpa->getId() );
    $kpaEvaluation->setRating( getSQLParam( $kpa->getId() . "_rating" ) );
    $kpaEvaluation->setSelfEvaluation( getSQLParam( $kpa->getId() . "_self_evaluation" ) );
		$kpaEvaluation->setComments( getSQLParam( $kpa->getId() . "_comments" ) );
    $kpaEvaluation->save();
  }

  redirect( $moduleType."_evaluate_period.php" );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmKPAs").validate();
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?=$moduleName?> :: Evaluation :: KPIs</h1>

    <h5>Selected user, evaluation year and evaluation period:</h5>
    <table class="info slim" cellspacing="0" style="margin-bottom:15px;">
      <tr>
        <th>User:</th>
        <td><?= $selectedUser->getFullName() ?></td>
        <th>Manager:</th>
        <td><?= $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ?></td>
      </tr>
      <tr>
        <th>Evaluation year:</th>
        <td><?= $selectedYear->getName() ?></td>
        <th>Evaluation Status:</th>
        <td><?= $selectedPeriodEvalStatus->getStatusDisplay() ?></td>
      </tr>
      <tr>
        <th>Period start:</th>
        <td><?= getPeriodStartDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></td>
        <th>Period end:</th>
        <td><?= getPeriodEndDate( getTypeCategory( $moduleType )->getEvalPeriodType(), $selectedYear, $selectedPeriod ) ?></td>
      </tr>
    </table>

    <form id="frmKPAs" method="post" >
      <table class="list autosize" cellspacing="0">
        <tr>
          <th width="3%">&nbsp;</th>
          <th width="15%">KPI:</th>
          <th width="22%">Measurement:</th>
          <?php if( $useWeights ){ ?>
          <th width="5%">Weight:</th>
          <?php } ?>
          <th width="5%">1 <img src="images/smiley_1_alt.png" alt="1" title="1"/></th>
          <th width="5%">2 <img src="images/smiley_2_alt.png" alt="2" title="2"/></th>
          <th width="5%">3 <img src="images/smiley_3_alt.png" alt="3" title="3"/></th>
          <th width="5%">4 <img src="images/smiley_4_alt.png" alt="4" title="4"/></th>
          <th width="5%">5 <img src="images/smiley_5_alt.png" alt="5" title="5"/></th>
          <th width="10%">Self Evaluation:</th>
					<th width="20%">Comments:</th>
        </tr>
        <?
          foreach( $kpas as $kpa )
          {
            $kpaEvaluation = new KPAEvaluation();
            $kpaEvaluation->loadForBasic( $moduleType, $kpa->getId(), $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod );
        ?>
        <tr>
          <td><a href="/PMSPS/ajax_kpa_info.php?id=<?= $kpa->getId() ?>&moduleType=<?= $moduleType ?>" rel="facebox"><img src="/PMSPS/images/magnifier.png" alt="View KPI details" title="View KPI details"/></a></td>
          <td><?= $kpa->getName() ?></td>
          <td><?= $kpa->getMeasurement() ?></td>
          <?php if( $useWeights ){ ?>
          <td><?= $kpa->getWeight() ?></td>
          <?php } ?>
          <td align="center"><input type="radio" name="<?= $kpa->getId() ?>_rating" value="1" <?= $kpaEvaluation->getRating() == "1" ? "checked=\"checked\"" : ""?> /></td>
          <td align="center"><input type="radio" name="<?= $kpa->getId() ?>_rating" value="2" <?= $kpaEvaluation->getRating() == "2" ? "checked=\"checked\"" : ""?> /></td>
          <td align="center"><input type="radio" name="<?= $kpa->getId() ?>_rating" value="3" <?= $kpaEvaluation->getRating() == "3" ? "checked=\"checked\"" : ""?> /></td>
          <td align="center"><input type="radio" name="<?= $kpa->getId() ?>_rating" value="4" <?= $kpaEvaluation->getRating() == "4" ? "checked=\"checked\"" : ""?> /></td>
          <td align="center"><input type="radio" name="<?= $kpa->getId() ?>_rating" value="5" <?= $kpaEvaluation->getRating() == "5" ? "checked=\"checked\"" : ""?> /></td>
					<td><?= select( $kpa->getId() . "_self_evaluation", getRatingSelectList(), $kpaEvaluation->getSelfEvaluation(), false ) ?></td>
          <td><input type="text" name="<?= $kpa->getId() ?>_comments" value="<?= $kpaEvaluation->getComments() ?>" style="width:95%;"/></td>
        </tr>
        <?
          }
        ?>
        <tr class="info">
          <td colspan="8"><input type="submit" value="Evaluate" /></td>
        </tr>
      </table>

      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="evalPeriod" value="<?= $selectedPeriod ?>" />
      <input type="hidden" name="evaluatedById" value="<?= getLoggedInUserId() ?>" />
    </form>

    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?=$moduleType?>_evaluate_period.php');" />
    </div>
  </body>
</html>