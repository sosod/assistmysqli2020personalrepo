<div class="actions">
  <input type="button" value="Evaluate All" onclick="redirect('<?=$moduleType?>_evaluate_kpas_org.php');" <?if($disableBtn){?>disabled="disabled"<?}?>/>
</div>

<table class="list full" cellspacing="0">
  <tr>
    <th width="5%">Group Num</th>
    <th width="5%">SDBIP KPI Num</th>
    <th width="22%">KPI</th>
    <th width="23%">Unit of Measurement</th>
    <th width="8%">Rating</th>
		<th width="8%">Self Evaluation</th>
    <? if( $useWeights ){ ?>
    <th width="6%">Weight</th>
    <th width="6%">Percentage</th>
    <? }else{ ?>
    <th width="12%">Evaluated On</th>
    <? } ?>
    <th width="15%">Actions</th>
  </tr>
  <?

  $lastGroupNum = -1;
  foreach( $orgKPAsWithEval as $kpaWithEval )
  {
    $kpa = $kpaWithEval[0];
    $kpaEvaluation = $kpaWithEval[1];
  ?>
  <tr>
    <td><?= $kpa->getGroupNum() ?></td>
    <td><?= $kpa->getSDBIPNum() ?></td>
    <td><?= $kpa->getName() ?></td>
    <td><?= $kpa->getMeasurement() ?></td>
    <td><?= $lastGroupNum == $kpa->getGroupNum() ? " " : ($kpaEvaluation->isTransient() || $kpaEvaluation->getRating() == null  ? "Not Set" : $kpaEvaluation->getRating()) ?></td>
    <td><?= $lastGroupNum == $kpa->getGroupNum() ? " " : ($kpaEvaluation->isTransient() || $kpaEvaluation->getSelfEvaluation() == null ? "Not Set" : $kpaEvaluation->getSelfEvaluation()) ?></td>
    <? if( $useWeights ){ ?>
    <td><?= $kpa->getWeight() ?></td>
    <td><?= $kpaEvaluation->isTransient() ? "Not Set" : ($kpaEvaluation->getRating() * $kpa->getWeight() / 5) ?>%</td>
    <? }else{ ?>
    <td><?= $kpaEvaluation->isTransient() ? "Not Set" : $kpaEvaluation->getEvaluatedOn() ?></td>
    <? } ?>
    <td><input type="button" value="Evaluate" onclick="redirect('<?=$moduleType?>_evaluate_kpa_org.php?kpaId=<?= $kpa->getId() ?>');" <?if($disableBtn){?>disabled="disabled"<?}?>/></td>
  </tr>
<?
    $lastGroupNum = $kpa->getGroupNum();
  }
?>
</table>