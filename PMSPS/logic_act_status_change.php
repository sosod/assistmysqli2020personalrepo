<?php
$oldStatus = null;
$newStatus = null;

if( $action == "request_approval" )
{
  $oldStatus = "new";
  $newStatus = "pending";
  $subject = $moduleName . " :: Manager approval required";
  $template = "template/tmpl_act_employer_approval_required.html";

  $selectedUserSetup = new UserSetup();
  $selectedUserSetup->loadFromUserId( $selectedUser->getId() );

  if( $selectedUser->getManagerId() == "S" )
	{
    $toEmail = $selectedUser->getEmail();
    $toName = $selectedUser->getFullName();
  }
  else
  {
    $toEmail = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();
  }
}
else if( $action == "employer_approval" )
{
  $oldStatus = "pending";
  $newStatus = "employer";
  $subject = $moduleName . " :: Employee approval required";
  $template = "template/tmpl_act_employee_approval_required.html";

  $selectedUserSetup = new UserSetup();
  $selectedUserSetup->loadFromUserId( $selectedUser->getId() );

  if( $selectedUserSetup->getCreatePermission() == false )
  {
    $toEmail = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();
  }
  else
  {
    $toEmail = $selectedUser->getEmail();
    $toName = $selectedUser->getFullName();
  }
}
else if( $action == "employee_approval" )
{
  $oldStatus = "employer";
  $newStatus = "employee";
  $subject = $moduleName . " :: Employee approved";
  $template = "template/tmpl_act_employee_approved.html";
  $toEmail = $selectedUser->getManagerEmail();
  $toName = $selectedUser->getManager();
}

//echo "oldStatus: " . $oldStatus . "<br/>";
//echo "newStatus: " . $newStatus . "<br/>";
//echo "toEmail: " . $toEmail . "<br/>";
//echo "toName: " . $toName . "<br/>";

if( exists( $oldStatus ) && exists( $newStatus ) )
{
  /* 1 - set status to pending approval */
  $selectedUserStatus->setStatus( $newStatus );
  $selectedUserStatus->setActionById( getLoggedInUserId() );
  $selectedUserStatus->save();

  //$_SESSION[$moduleType."_activate_sess_status_" .getLoggedInUserId()] = $selectedUserStatus;

  /* 2 - add status change entry */
  $statusChange = new ActivationStatusChange();
  $statusChange->setStatusId( $selectedUserStatus->getId() );
  $statusChange->setOldStatus( $oldStatus );
  $statusChange->setNewStatus( $newStatus );
  $statusChange->setChangedById( getLoggedInUserId() );
  $statusChange->save();

  /* 3 - send email to relevant person */
	if( exists( $toEmail ) )
	{
	  $body = getEmailTemplate( $template, array( array( "##employee##", $selectedUser->getFullName() ), array( "##evalYear##", $selectedYear->getName() ) ) );

	  //echo "body: " . $body . "<br/>";

	  $headers = 'From: no-reply@ignite4u.co.za' . "\r\n" .
	    'Reply-To: helpdesk@igniteconsult.co.za' . "\r\n";

	  //debug( "to address: " . $toEmail );
	  //debug( "to name: " . $toName );
	  //debug( "subject: " . $subject );
	  //debug( "body: " . $body );

	  $result = mail( $toEmail, $subject, $body, $headers );

	  //echo $result ? "Sent successful" : "Sent failed";
	}
}

if( $action == "reset_status" )
{
  $oldStatus = $selectedUserStatus->getStatus();
  $newStatus = "new";

  $selectedUserStatus->setStatus( $newStatus );
  $selectedUserStatus->setActionById( getLoggedInUserId() );
  $selectedUserStatus->save();

  $statusChange = new ActivationStatusChange();
  $statusChange->setStatusId( $selectedUserStatus->getId() );
  $statusChange->setOldStatus( $oldStatus );
  $statusChange->setNewStatus( $newStatus );
  $statusChange->setChangedById( getLoggedInUserId() );
  $statusChange->save();
}

?>