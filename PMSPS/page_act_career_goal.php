<?
include("activate_inc.php");

$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$careerGoal = new CareerGoal();

if( $action == "save" )
{
  $careerGoal->loadFromRequest( $_REQUEST );
  $careerGoal->save();
  
  redirect($moduleType."_activate.php" );
}

if( $action == "delete" )
{
  $careerGoal->setId( $id );
  $careerGoal->delete();
  
  redirect($moduleType."_activate.php" );
}

if( exists( $id ) )
{
  $careerGoal->loadFromId( $id );
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmCareerGoal").validate();        
      });
    </script>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1><?= $moduleName ?> :: Activation :: Career Goal</h1>

    <form id="frmCareerGoal" method="post" >
      <table class="form" cellspacing="0">
        <tr>
          <th>User:</th>
          <td><?= $selectedUser->getFullName() ?></td>
        </tr>
        <tr>
          <th>Evaluation year:</th>
          <td><?= $selectedYear->getName() ?></td>
        </tr>
        <tr>
          <th>Name:</th>
          <td><input type="text" name="name" value="<?= $careerGoal->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <th>Description:</th>
          <td><textarea name="description" cols="50" rows="5" class="required"><?= $careerGoal->getDescription() ?></textarea></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="action" value="save" />
      <input type="hidden" name="type" value="<?= $moduleType ?>" />            
      <input type="hidden" name="id" value="<?= $careerGoal->getId() ?>" />            
      <input type="hidden" name="userId" value="<?= $selectedUser->getId() ?>" />
      <input type="hidden" name="evalYearId" value="<?= $selectedYear->getId() ?>" />
      <input type="hidden" name="createdById" value="<?= getLoggedInUserId() ?>" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('<?= $moduleType ?>_activate.php');" />
    </div>  
  </body>
</html>