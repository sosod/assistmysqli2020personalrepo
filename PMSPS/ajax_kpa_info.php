<?php
include("inc_ignite.php");

$id = $_REQUEST["id"];
$moduleType = $_REQUEST["moduleType"];

$kpa = new KPA();
$kpa->loadFromId( $id );

$useWeights = getTypeCategory( $moduleType )->getUseWeights();

?>
<table class="info" cellspacing="0">
  <tr>
    <th>Type</th>
    <td><?= $kpa->getTypeDisplay() ?></td>
  </tr>
  <tr>
    <th>Name:</th>
    <td><?= $kpa->getName() ?></td>
  </tr>
  <tr>
    <th>Description:</th>
    <td><?= $kpa->getDescription() ?></td>
  </tr>
  <tr>
    <th>Measurement:</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <? if( $useWeights ){ ?>
  <tr>
    <th>Weight:</th>
    <td><?= $kpa->getWeight() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Comments:</th>
    <td><?= $kpa->getComments() ?></td>
  </tr>
  <tr>
    <th>Created by</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on</th>
    <td><?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>