<?php
include("inc_ignite.php");

$id = $_REQUEST["id"];
$moduleType = $_REQUEST["moduleType"];

$kpa = new KPAOrganisational();
$kpa->loadFromId( $id );

$useWeights = getTypeCategory( $moduleType )->getUseWeights();

?>
<table class="info" cellspacing="0">
  <tr>
    <th>Group Num:</th>
    <td><?= $kpa->getGroupNum() ?></td>
  </tr>
  <tr>
    <th>Name:</th>
    <td><?= $kpa->getName() ?></td>
  </tr>
  <tr>
    <th>Objective:</th>
    <td><?= $kpa->getObjective() ?></td>
  </tr>
  <tr>
    <th>KPI:</th>
    <td><?= $kpa->getKPI() ?></td>
  </tr>
  <tr>
    <th>Unit of Measurement:</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <tr>
    <th>Baseline:</th>
    <td><?= $kpa->getBaseline() ?></td>
  </tr>
  <tr>
    <th>Target unit:</th>
    <td><?= $kpa->getTargetUnit() ?></td>
  </tr>
  <tr>
    <th>Target Q1:</th>
    <td><?= $kpa->getTarget1() ?></td>
  </tr>
  <tr>
    <th>Target Q2:</th>
    <td><?= $kpa->getTarget2() ?></td>
  </tr>
  <tr>
    <th>Target Q3:</th>
    <td><?= $kpa->getTarget3() ?></td>
  </tr>
  <tr>
    <th>Target Q4:</th>
    <td><?= $kpa->getTarget4() ?></td>
  </tr>
  <? if( $useWeights ){ ?>
  <tr>
    <th>Weight:</th>
    <td><?= $kpa->getWeight() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Comments:</th>
    <td><?= $kpa->getComments() ?></td>
  </tr>
  <tr>
    <th>Created by</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on</th>
    <td><?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>