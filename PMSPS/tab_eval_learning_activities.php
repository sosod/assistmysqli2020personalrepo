<div class="actions">
  <input type="button" value="New learning activity" onclick="redirect('<?=$moduleType?>_evaluate_learning_activity.php')" <?if($disableBtn){?>disabled="disabled"<?}?>/>
</div>

<?
  $learningActivities = getLearningActivities( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
?>

<table class="list full" cellspacing="0">
  <tr>
    <th>Priority</th>
    <th>Name</th>
    <th>Training</th>
    <th>Mode of delivery</th>
    <th>Support Person</th>
  </tr>
  <?
    if( isset( $learningActivities ) && count( $learningActivities ) > 0 )
    { 
      foreach( $learningActivities as $learningActivity ) {
  ?>
  <tr>
    <td><?= $learningActivity->getPriority() ?></td>
    <td><?= $learningActivity->getName() ?></td>
    <td><?= $learningActivity->getTraining() ?></td>            
    <td><?= $learningActivity->getDeliveryMode() ?></td>
    <td><?= $learningActivity->getSupportPerson() ?></td>
  </tr>
  <?
      }
    }
    else 
    {
  ?>
  <tr><td colspan="5">No learning activities</td></tr> 
  <?
    }
  ?>
</table>