<?php
include("inc_ignite.php");


$id = $_REQUEST["id"];
$action = $_REQUEST["action"];

$contractKPA = new ContractKPA();

if( $action == "save" )
{
  $contractKPA->loadFromRequest( $_REQUEST );
  $contractKPA->save();
  
  redirect("setup_contract_kpas.php" );
}

if( $action == "delete" )
{
  $contractKPA->setId( $id );
  $contractKPA->delete();
  
  redirect("setup_contract_kpas.php" );
}

if( exists( $id ) )
{
  $contractKPA->loadFromId( $id );
}
?>
<html>
  <head>
    <? include("inc_head.php"); ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#frmContractKPA").validate();        
      });
    </script>        
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Contract KPI</h1>
    
    <form id="frmContractKPA" method="post" >  
      <table class="form" cellspacing="0">
        <tr>
          <th>Name</th>
          <td><input type="text" name="name" value="<?= $contractKPA->getName() ?>" class="required"/></td>
        </tr>
        <tr>
          <td></td><td><input type="submit" value="Save" /></td>
        </tr>
      </table>
      <input type="hidden" name="id" value="<?= $contractKPA->getId() ?>" />
      <input type="hidden" name="action" value="save" />
    </form>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup_contract_kpas.php');" />
    </div>  
  </body>
</html>