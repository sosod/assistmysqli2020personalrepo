<?php
include("activate_inc.php");

$action = $_REQUEST["action"];

if( isset( $selectedUser ) == false || isset( $selectedYear ) == false )
{
  redirect($moduleType."_activate.php");
}

$formalQualifications = getQualifications( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "formal" );
$informalQualifications = getQualifications( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "informal" );
$experiences = getExperiences( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
$professionalBodies = getProfessionalBodies( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
$jobFunctions = getJobFunctions( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
$careerGoals = getCareerGoals( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
$careerKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "career" );
$learningActivities = getLearningActivities( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
$comments = getActivationComments( $moduleType, $selectedUser->getId(), $selectedYear->getId() );

if( $moduleType == "career" )
{
  $jobLevelKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "job_level" );
  $uniqueKPAs = getKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId(), "unique" );
}

if( $moduleType == "perform" || $moduleType == "contract" )
{
  $orgKPAs = getOrganisationalKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
  $manKPAs = getManagerialKPAs( $moduleType, $selectedUser->getId(), $selectedYear->getId() );
}

if( $action == "pdf" )
{
  require('fpdf/IgnitePDF.class.php');

  $pdf=new IgnitePDF('L','pt','A4');
  $pdf->AddPage();
  $pdf->H1( $moduleName );


  /* PERSONAL INFORMATION */
  $pdf->H2('Personal Information');

  $widths = array( 200, 580 );
  $rows = array( array( "Name", $selectedUser->getDecodedFullName() ), /*array( "Gender", $selectedUser->getGenderDisplay() ),*/
                 /*array( "Race", $selectedUser->getRace() ),*/ array( "Department", $selectedUser->getDepartment() ), array( "Section", $selectedUser->getSection() ),
                 array( "Job title", $selectedUser->getJobTitle() ), array( "Job level", $selectedUser->getJobLevel() ), array( "Job number", $selectedUser->getJobNumber() ),
                 array( "Manager", $selectedUser->getManagerId() == "S" ? "Self" : $selectedUser->getManager() ),
                 array( "Performance year", $selectedYear->getName() ), array( "Date", date("Y-m-d H:i:s") ) );

  $pdf->infoTable( $rows, $widths );
  $pdf->AddPage();
  /* Qualifications */
  $pdf->H2('Formal Qualifications');
  $headers = array( "Name", "Accredited", "Comments" );
  $widths = array( 150, 80, 550 );
  $rows = array();

  foreach( $formalQualifications as $qualifications )
  {
    $rows[] = array( $qualifications->getName(), boolValue( $qualifications->getAccredited() ), $qualifications->getComments() );
  }

  for($i=1; $i <= $maxFormalQualifications - count($formalQualifications); $i++) {
    $rows[] = array( "", "", "" );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->H2('Informal Qualifications');
  $headers = array( "Type", "Name", "Accredited", "Comments" );
  $widths = array( 100, 150, 80, 450 );
  $rows = array();

  foreach( $informalQualifications as $qualifications )
  {
    $rows[] = array( $qualifications->getSecondaryTypeDisplay(), $qualifications->getName(), boolValue( $qualifications->getAccredited() ), $qualifications->getComments() );
  }

  for($i=1; $i <= $maxInformalQualifications - count($informalQualifications); $i++) {
    $rows[] = array( "", "", "", "" );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->AddPage();

  /* Experiences */
  $pdf->H2('Experience');
  $headers = array( "Experience type", "Obtained from", "Date Obtained", "Number of Years", "Description" );
  $widths = array( 150, 150, 80, 80, 320 );
  $rows = array();

  foreach( $experiences as $experience )
  {
    $rows[] = array( $experience->getName(), $experience->getObtainedFrom(), $experience->getObtainedOn(), $experience->getNumOfYears(), $experience->getDescription() );
  }

  for($i=1; $i <= $maxExperiences - count($experiences); $i++) {
    $rows[] = array( "", "", "", "", "" );
  }

  $pdf->listTable( $headers, $rows, $widths );

  /* Professional Bodies */
  $pdf->H2('Professional Bodies');
  $headers = array( "Institution", "Member Since", "Membership Level", "Description" );
  $widths = array( 150, 80, 150, 400 );
  $rows = array();

  foreach( $professionalBodies as $professionalBody )
  {
    $rows[] = array( $professionalBody->getName(), $professionalBody->getMemberSince(), $professionalBody->getMemberLevel(), $professionalBody->getDescription() );
  }

  for($i=1; $i <= $maxProfessionalBodies - count($professionalBodies); $i++) {
    $rows[] = array( "", "", "", "" );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->AddPage();

  if( $moduleType == "career" || $moduleType == "perform") {

    /* JOB FUNCTIONS */
    $pdf->H2('Job functions');
    $headers = array( "Name", "Description" );
    $widths = array( 200, 580 );
    $rows = array();

    foreach( $jobFunctions as $jobFunction )
    {
      $rows[] = array( $jobFunction->getName(), $jobFunction->getDescription() );
    }

    for($i=1; $i <= $maxJobFunctions - count($jobFunctions); $i++) {
      $rows[] = array( "", "" );
    }

    $pdf->listTable( $headers, $rows, $widths );

    /* CAREER GOALS */
    $pdf->H2('Career Goals');
    $headers = array( "Name", "Description" );
    $widths = array( 200, 580 );
    $rows = array();

    foreach( $careerGoals as $careerGoal )
    {
      $rows[] = array( $careerGoal->getName(), $careerGoal->getDescription() );
    }

    for($i=1; $i <= $maxCareerGoals - count($careerGoals); $i++) {
      $rows[] = array( "", "" );
    }

    $pdf->listTable( $headers, $rows, $widths );

    $pdf->AddPage();
  }

  /* KPAS */
  $pdf->H2('Key Performance Areas');

  if( $moduleType == "career")
  {
    $pdf->H3('Key Performance Areas linked to Job Level');

    $headers = array( "Name", "Description", "Measurement", "Outcome", "Portfolio of evidence" );
    $widths = array( 100, 170, 170, 170, 170 );

    if( $useWeights )
    {
      $headers = array( "Name", "Description", "Measurement", "Weight", "Outcome", "Portfolio of evidence" );
      $widths = array( 100, 160, 160, 40, 160, 160 );
    }

    $rows = array();

    foreach( $jobLevelKPAs as $kpa )
    {
      if( $useWeights )
      {
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpa->getWeight(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
    }

    for($i=1; $i <= $maxJobLevelKPAs - count($jobLevelKPAs); $i++) {
      if( $useWeights )
      {
        $rows[] = array( "", "", "", "", "", "" );
      }
      else
      {
        $rows[] = array( "", "", "", "", "" );
      }
    }

    $pdf->listTable( $headers, $rows, $widths );

    $pdf->H3('Key Performance Areas unique to this job');
    $headers = array( "Name", "Description", "Measurement", "Outcome", "Portfolio of evidence" );
    $widths = array( 100, 170, 170, 170, 170 );

    if( $useWeights )
    {
      $headers = array( "Name", "Description", "Measurement", "Weight", "Outcome", "Portfolio of evidence" );
      $widths = array( 100, 160, 160, 40, 160, 160 );
    }

    $rows = array();

    foreach( $uniqueKPAs as $kpa )
    {
      if( $useWeights )
      {
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpa->getWeight(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
    }

    for($i=1; $i <= $maxUniqueKPAs - count($uniqueKPAs); $i++) {
      if( $useWeights )
      {
        $rows[] = array( "", "", "", "", "", "" );
      }
      else
      {
        $rows[] = array( "", "", "", "", "" );
      }
    }

    $pdf->listTable( $headers, $rows, $widths );
  }

  if( $moduleType == "career" || $moduleType == "perform" )
  {

    $pdf->H3('Key Performance Areas linked to career goals');
    $headers = array( "Name", "Description", "Measurement", "Outcome", "Portfolio of evidence" );
    $widths = array( 100, 170, 170, 170, 170 );

    if( $useWeights )
    {
      $headers = array( "Name", "Description", "Measurement", "Weight", "Outcome", "Portfolio of evidence" );
      $widths = array( 100, 160, 160, 40, 160, 160 );
    }

    $rows = array();

    foreach( $careerKPAs as $kpa )
    {
      if( $useWeights )
      {
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpa->getWeight(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $rows[] = array( $kpa->getName(), $kpa->getDescription(), $kpa->getMeasurement(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
    }

    for($i=1; $i <= $maxCareerKPAs - count($careerKPAs); $i++) {
      if( $useWeights )
      {
        $rows[] = array( "", "", "", "", "", "" );
      }
      else
      {
        $rows[] = array( "", "", "", "", "" );
      }
    }

    $pdf->listTable( $headers, $rows, $widths );
  }

  if( $moduleType == "perform" || $moduleType == "contract" )
  {
    $pdf->H3('Organisational KPIs');
    $headers = array( "Group Num", "SDBIP KPI Num", "KPI", "Objective", "KPA", "Unit of Measurement", "Baseline", "Target Unit", "T1", "T2", "T3", "T4", "Outcome", "Portfolio of evidence" );
    $widths = array( 35, 35, 70, 75, 75, 75, 60, 65, 35, 35, 35, 35, 75, 75 );

    if( $useWeights )
    {
      $headers = array( "Group Num", "SDBIP KPI Num", "KPI", "Objective", "KPA", "Unit of Measurement", "Baseline", "Target Unit", "T1", "T2", "T3", "T4", "Weight", "Outcome", "Portfolio of evidence" );
      $widths = array( 30, 30, 70, 70, 70, 70, 55, 65, 40, 35, 35, 35, 35, 70, 70 );
    }

    $rows = array();

    foreach( $orgKPAs as $kpa )
    {
      if( $useWeights )
      {
        $rows[] = array( $kpa->getGroupNum(), $kpa->getSDBIPNum(), $kpa->getName(), $kpa->getObjective(), $kpa->getKPI(), $kpa->getMeasurement(),
                       $kpa->getBaseline(), $kpa->getTargetUnit(), $kpa->getTarget1(), $kpa->getTarget2(),
                       $kpa->getTarget3(), $kpa->getTarget4(), $kpa->getWeight(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $rows[] = array( $kpa->getGroupNum(), $kpa->getSDBIPNum(), $kpa->getName(), $kpa->getObjective(), $kpa->getKPI(), $kpa->getMeasurement(),
                       $kpa->getBaseline(), $kpa->getTargetUnit(), $kpa->getTarget1(), $kpa->getTarget2(),
                       $kpa->getTarget3(), $kpa->getTarget4(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
    }

    $pdf->listTable( $headers, $rows, $widths );

    $pdf->H3('Managerial KPIs');
    $headers = array( "Core Managerial Skill", "Skill Definition", "Skill Level", "Measurement", "Outcome", "Portfolio of evidence" );
    $widths = array( 110, 200, 80, 130, 130, 130 );

    if( $useWeights )
    {
      $headers = array( "Core Managerial Skill", "Skill Definition", "Skill Level", "Measurement", "Weight", "Outcome", "Portfolio of evidence" );
      $widths = array( 120, 300, 80, 180, 100 );
      $widths = array( 100, 190, 80, 120, 50, 120, 120 );
    }

    $rows = array();

    foreach( $manKPAs as $kpa )
    {
      if( $useWeights )
      {
        $rows[] = array( $kpa->getCoreManagerialSkill()->getSkill(), $kpa->getCoreManagerialSkill()->getDefinition(),
                       $kpa->getSkillLevel() . " - " . KPAManagerial::displaySkillLevel( $kpa->getSkillLevel() ),
                       $kpa->getMeasurement(), $kpa->getWeight(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
      else
      {
        $rows[] = array( $kpa->getCoreManagerialSkill()->getSkill(), $kpa->getCoreManagerialSkill()->getDefinition(),
                       $kpa->getSkillLevel() . " - " . KPAManagerial::displaySkillLevel( $kpa->getSkillLevel() ),
                       $kpa->getMeasurement(), $kpa->getOutcome(), $kpa->getEvidence() );
      }
    }

    $pdf->listTable( $headers, $rows, $widths );
  }

  $pdf->AddPage();

  /* LEARNING ACTIVITIES */
  $pdf->H2('Skills development plan');
  $headers = array( "Priority", "Name", "Outcome", "Training", "Delivery Mode", "Time frames", "Work opportunity", "Support person" );
  $widths = array( 39, 78, 117, 117, 117, 117, 117, 78 );

  $rows = array();

  foreach( $learningActivities as $learningActivity )
  {
    $rows[] = array( $learningActivity->getPriority(), $learningActivity->getName(), $learningActivity->getOutcome(),
                     $learningActivity->getTraining(), $learningActivity->getDeliveryMode(), $learningActivity->getTimeFrames(),
                     $learningActivity->getWorkOpportunity(), $learningActivity->getSupportPerson() );
  }

  for($i=1; $i <= $maxLearningActivities - count($learningActivities); $i++) {
    $rows[] = array( "", "", "", "", "", "", "", "" );
  }

  $pdf->listTable( $headers, $rows, $widths );

  /* COMMENTS */
  $pdf->H2('General Comments');
  $headers = array( "Created on", "Created by", "Message" );
  $widths = array( 156, 156, 468 );
  $rows = array();

  foreach( $comments as $comment )
  {
    $rows[] = array( $comment->getCreatedOn(), $comment->getCreatedBy()->getDecodedFullName(), $comment->getMessage() );
  }

  for($i=1; $i <= $maxComments - count($comments); $i++) {
    $rows[] = array( "", "", "" );
  }

  $pdf->listTable( $headers, $rows, $widths );

  $pdf->AddPage();

  $pdf->SetFont('Arial','',9);
  $pdf->Cell(300,21,"Signed and accepted by the Employee",'T',1);
  $pdf->Ln(20);
  $pdf->Cell(300,21,"Date",'T',1);
  $pdf->Ln(50);
  $pdf->Cell(300,21,"Signed and accepted by the Manager",'T',1);
  $pdf->Ln(20);
  $pdf->Cell(300,21,"Date",'T',1);

  $pdf->Output( $moduleType.'_' . str_replace(" ", "_", strtolower($selectedUser->getDecodedFullName())) . '.pdf','I');

  exit;
}
?>