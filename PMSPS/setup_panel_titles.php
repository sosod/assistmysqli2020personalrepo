<?php
include("inc_ignite.php");

?>
<html>
  <head>
    <? include("inc_head.php"); ?>
  </head>
  <body style="margin:0 5px 0 5px;">
    <h1>Setup - Evaluation Panel Titles</h1>
    
    <table class="list" cellspacing="0">
      <tr>
        <th>Name</th>
        <th>Active</th>
        <th>Actions</th>        
      </tr>
  	  <?
      $panelTitles = getPanelTitles();
      foreach( $panelTitles as $panelTitle )
      {
  	  ?>
      <tr>
        <td><?= $panelTitle->getName() ?></td>
        <td><?= boolValue( $panelTitle->getActive() ) ?></td>
        <td><input type="button" value="Edit" onclick="redirect('setup_panel_title.php?id=<?= $panelTitle->getId() ?>');"></td>		
      </tr>
      <?
      }
      ?>
    </table>
    
    <div class="actions">
      <input type="button" value="Back" onclick="redirect('setup.php');"/>
      <input type="button" value="Add" onclick="redirect('setup_panel_title.php');">
    </div>  
  </body>
</html>