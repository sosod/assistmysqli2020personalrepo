<?php
    include("inc_ignite.php");
    include("inc_admin.php");
    
$catetitle = $_POST["catetitle"];
$catematkid = $_POST["catematkid"];

if(strlen($catetitle) > 0)  //IF DATA HAS BEEN SUBMITTED VIA THE FORM
{
    $catetitle = str_replace("'","&#39",$catetitle);
    //CREATE NEW CATEGORY RECORD
    $sql = "INSERT INTO assist_".$moduledb."_categories SET ";
    $sql.= "catetitle = '".$catetitle."', ";
    $sql.= "catematkid = '".$catematkid."', ";
    $sql.= "cateyn = 'Y'";
    include("inc_db_con_assist.php");
        //LOG THE NEW CATEGORY
        $tsql = $sql;
        //$tref = "MN";
        $trans = "Added new category: ".$catetitle;
        include("inc_transaction_log.php");
    $sql = "SELECT * FROM assist_".$moduledb."_categories WHERE catetitle = '".$catetitle."' AND catematkid = '".$catematkid."' AND cateyn = 'Y'";
    include("inc_db_con_assist.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    $cateid = $row['cateid'];
    //CREATE VIEW ACCESS TO CATEGORY FOR ADMIN
    $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_list_users SET tkid = '".$catematkid."', cateid = ".$cateid.", yn = 'Y'";
    include("inc_db_con.php");
        //LOG THE VIEW ACCESS GRANTED TO THE NEW CATEGORY
        $tsql = $sql;
        //$tref = "MN";
        $trans = "Added view access for user ".$catematkid." to category ".$cateid;
        include("inc_transaction_log.php");
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delCate(c) {
    if(confirm("Are you sure you want to remove category "+c+"?\n\nNote that all documents associated with this category will be deleted.")==true)
    {
        //alert("delete");
        document.location.href = "setup_categories_delete.php?c="+c;
    }
    else
    {
        //alert("abort");
    }
}

function editCate(c) {
    if(confirm("Are you sure you want to edit category "+c+"?\n\nNote that all documents associated with this category will be affected.")==true)
    {
        //alert("edit");
        document.location.href = "setup_categories_edit.php?c="+c;
    }
    else
    {
        //alert("abort");
    }
}

function Validate(me) {

    var ct = me.catetitle.value;
    var ma = me.catematkid.value;

    if(ct.length == 0)
    {
        alert("Please enter a category title.");
    }
    else
    {
        if(ma == "X")
        {
            alert("Please select the administrator for this new category.");
        }
        else
        {
            return true;
        }
    }
    return false;
}
</script>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: 20px"><b><?php echo($moduletitle); ?>: Update</b></h1>
<h2 class=fc>Categories</h2>
<form name=addcate action=admin.php method=post onsubmit="return Validate(this);">
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader>Ref</td>
        <td class=tdheader>Category</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$moduledb."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
include("inc_db_con_assist.php");
$r = mysql_num_rows($rs);
if($r > 0)
{
    $ccount = 0;
    while($row = mysql_fetch_array($rs))
    {
        $caterow[$ccount] = $row;
        $ccount++;
    }
}
mysql_close();

$ccount = 0;
if($r > 0)
{
    foreach($caterow as $cate)
    {
?>
    <tr>
        <td class=tdgeneral valign=top align=center><?php echo($cate['cateid']); ?></td>
        <td class=tdgeneral><?php echo($cate['catetitle']); ?></td>
        <td class=tdgeneral><?php echo("<input type=button value=Edit onclick=\"editCate(".$cate['cateid'].")\"> <input type=button value=Del onclick=\"delCate(".$cate['cateid'].")\">");?></td>
    </tr>
<?php
    }
}
?>
    <tr>
        <td class=tdgeneral valign=top align=center>&nbsp;</td>
        <td class=tdgeneral><input type=text name=catetitle size=30></td>
        <td class=tdgeneral><input type=submit value=Add></td>
    </tr>
</table>
</form>
<h2 class=fc>Documents</h2>
<?php

$sql = "SELECT * FROM assist_".$moduledb."_categories c WHERE c.cateyn = 'Y' ORDER BY catetitle";
include("inc_db_con_assist.php");
$c = mysql_num_rows($rs);
if($c == 0)
{
    echo("<p>There are no categories to display.</p>");
}
else
{
    echo("<ul>");
    while($row = mysql_fetch_array($rs))
    {
        $url = "<a href=update.php?c=".$row['cateid'].">";
        echo("<li>".$url.$row['catetitle']."</a></li>");
    }
    echo("</ul>");
}
mysql_close();



?>





<?php
$helpfile = "../help/".$tref."_help_moduleadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
<?php
    include("inc_back.php");
?>
</body>

</html>
