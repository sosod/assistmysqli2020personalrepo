<div align=center>
<table border=1 cellpadding=10 cellspacing=0 width=500>
<tr><td class=tdgeneral>
Labour Relations has become an integral part of running any business.  Labour Relations Assist aims to provide you as the employer, with practical and easy-to-use assistance in managing general staff issues in terms of South African Labour Legislations.  All items included in Labour Relations Assist provide support to the business owner in dealing with the obligation placed on an employer in terms of either the Labour Relations Act 66 of 1995 as amended or the Basic Conditions of Employment Act 75 of 1997 as amended.
<br>&nbsp;<br>
It should be noted that Labour Relations Assists contains only background material relating to the matters identified and published.  The information is not intended to constitute advice on the topic covered as every situation depends on its own facts and circumstances for which professional advice should be sought.
</td></tr></table>
</div>
