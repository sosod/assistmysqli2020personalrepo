<?php
include("inc_ignite.php");
include("inc_errorlog.php");

$docid = $_GET['m'];

?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var minfile = me.minfile.value;
    var mday = me.mday.value;
    var mmon = me.mmon.value;
    var myear = me.myear.value;
    var mintoc = me.mintoc.value;
    var valid8 = "false";
    
    if(mday.length == 0 || mmon.length == 0 || myear.length == 0)
    {
        alert("Please indicate the date of the document.");
    }
    else
    {
        if(minfile.length == 0)
        {
            alert("Please select a file for upload.");
        }
        else
        {
//            var ftype = minfile.substr(minfile.lastIndexOf(".")+1,3);
//            if(ftype.toLowerCase() != "pdf")
//            {
//                alert("Invalid file.  Only PDF documents may be uploaded.");
//            }
//            else
//            {
                valid8 = "true";
//            }
        }
    }
    if(valid8 == "true")
    {
        if(mintoc.length == 0)
        {
            if(confirm("You have not entered any table of contents.\n\nAre you sure you wish to continue?") == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
    return false;
}

function showMin(mfn) {
    var url = "http://assist.ignite4u.co.za"+mfn;
    //alert(url);
    newwin = window.open(url,"","dependent=0 ,toolbar=1,location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=700,height=500")
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Update</h1>
<form name=update action="update_edit_process.php" method="post" enctype="multipart/form-data" lang=jscript onsubmit="return Validate(this);">
<?php
$sql = "SELECT * FROM assist_".$moduledb."_content m WHERE docid = ".$docid." ORDER BY docsort, docid DESC";
include("inc_db_con_assist.php");
$docrow = mysql_fetch_array($rs);
mysql_close();
?>
<table border=1 cellpadding=3 cellspacing=0 width=600>
    <tr valign=middle align=center>
        <td class=tdgeneral>&nbsp;</td>
        <td class=tdgeneral><b>Edited Info</b></td>
        <td class=tdgeneral><b>Original Info</b></td>
    </tr>
    <tr valign=top>
        <td class=tdgeneral><b>Category:</b>&nbsp;</td>
        <td class=tdgeneral><select name=mincateid>
<?php
$dcid = $docrow['doccateid'];
$sql = "SELECT * FROM assist_".$moduledb."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
include("inc_db_con_assist.php");
while($row = mysql_fetch_array($rs))
{
    echo("<option value=".$row['cateid']);
    if($row['cateid'] == $dcid)
    {
        echo(" selected");
        $catetitle = $row['catetitle'];
    }
    echo(">".$row['catetitle']."</option>");
}
mysql_close();
?>
        </select></td>
        <td class=tdgeneral><?php echo($catetitle); ?></td>
    </tr>
    <tr valign=top>
        <td class=tdgeneral><b>Date:</b></td>
        <td class=tdgeneral><input type=text name=mday size=3><select name=mmon><?php
            $month = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
            echo("<option selected value=1>".$month[1]."</option>");
            for($m=2;$m<13;$m++)
            {
                echo("<option value=".$m.">".$month[$m]."</option>");
            }
            ?></select><input type=text size=5 name=myear value=<?php echo(date("Y")); ?>></td>
        <td class=tdgeneral><?php echo(date("d F Y",$docrow['docdate'])); ?></td>
    </tr>
    <tr valign=top>
        <td class=tdgeneral valign=top><b>Contents:</b>&nbsp;</td>
        <td class=tdgeneral><textarea name=mintoc rows=3 cols=39></textarea></td>
        <td class=tdgeneral><?php echo($docrow['doctoc']); ?></td>
    </tr>
    <tr valign=top>
        <td class=tdgeneral valign=top><b>Display Order:</b>&nbsp;</td>
        <td class=tdgeneral><input type=text name=minsort></td>
        <td class=tdgeneral><?php echo($docrow['docsort']); ?></td>
    </tr>
    <tr valign=top>
        <td class=tdgeneral><b>File:</b></td>
        <td class=tdgeneral colspan=2 align=center><?php echo("<input type=button value=\"View document\" onclick=\"showMin('".$floc."')\">"); ?></td>
    </tr>
    <tr valign=top>
        <td class=tdgeneral colspan=3><input type=hidden name=docid value=<?php echo($docid); ?>><input type=submit value=Edit> <input type=reset></td>
    </tr>
</table>
</form>
</body>

</html>
