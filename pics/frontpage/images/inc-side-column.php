<div id="side-column">
  <div class="spacer">
  
    <form id="frmSearch" action="/search.php" method="get" class="round-wide">
	  <fieldset>
	    <h3>Search Our Site</h3>
	    <input class="inline title-tip" type="text" name="q" title="Enter your search phrase" />
	    <input class="button" type="image" name="sa" src="/images/btn-go.png" />
	    <input type="hidden" name="cx" value="009368657017812141101:fuzehfvrwe4" />
	    <input type="hidden" name="cof" value="FORID:9" />
	    <input type="hidden" name="ie" value="UTF-8" />
	  </fieldset>
	  <img src="/images/bg-hr-side-column.png" alt="Devider" class="divider" />
	</form>
  
    <div class="section">
      <img src="/images/decoration/side-irs.jpg" width="110" height="110" alt="Cash Management" class="float-left decoration" />
      <h3>Cash Management</h3>
      <p>Opportunity to earn interest at rates previously available to high net-worth investors.</p>
      <a href="/investiments/offering.php"><img src="/images/btn-more.png" alt="Read more" /></a>
	 <p><marquee scrollamount="3">Ignite Reporting Systems (Pty) Ltd earns interest at 5.00 % per annum | Current Repo Rate is 5.50 % per annum | Current Prime Rate is 9.00 % per annum | Cash Investment options: Call Deposit, Prime Saver Call Deposit, Term Deposit or Notice Deposit</marquee></p>
      <img src="/images/bg-hr-side-column.png" alt="Devider" class="divider" />
    </div>

    <div class="section">
      <img src="/images/decoration/side-assist.jpg" alt="Ignite Assist" class="float-left decoration" />
      <h3>Ignite Assist</h3>
      <p>Ignite Assist is an innovative online subscription offering where the user only pays for functionality used.</p>
      <a href="/igniteassist.php"><img src="/images/btn-more.png" alt="Read more" /></a>
      <img src="/images/bg-hr-side-column.png" alt="Devider" class="divider" />
    </div>

    <div class="section">
      <img src="/images/decoration/side-eignite.jpg" alt="e-Ignite" class="float-left decoration" />
      <h3>e-Ignite</h3>
      <p>Online store coming soon.  Watch this space!</p>
      <a href="/eignite.php"><img src="/images/btn-more.png" alt="Read more" /></a>
      <img src="/images/bg-hr-side-column.png" alt="Devider" class="divider" />
    </div>
    <!--
    <form id="frmNewsletterSide" action="/newsletter.php" method="get" class="round-wide">
      <fieldset>
        <h3>Newsletter Subscription</h3>
        <label>Email Address</label>
        <input class="inline title-tip" type="text" name="email" title="yourname@example.com" />
        <input class="button" type="image" src="/images/btn-go.png" />                          
      </fieldset>
      <img src="/images/bg-hr-side-column.png" alt="Devider" class="divider" />
      <input type="hidden" name="action" value="subscribe" />
    </form>
  -->
    <div class="section">
      <h3>Client Comment</h3>
      <p><?php include("inc-quote.php") ?></p>
    </div>
  </div>
</div>
<!--[if IE 6]><br class="clear"/><![endif]-->