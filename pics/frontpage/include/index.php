<?php
  $title = "Home";
  $keywords = "";
  $description = "";
  $section = "home";
  $breadcrumbs =array();// array( "Modules" => null );
?>
<script type="text/javascript" src="/include/js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="/include/css/jquery.lightbox-0.5" />
<script type="text/javascript">
$(function() {
	// This, or...
	$('#homeImage a').lightBox(); // Select all links in object with gallery ID
});
</script>
<?php include("header.php"); ?>

<div id="column-wrapper">
  <div id="content-column">
    <div class="spacer">
      <h1>Welcome to Ignite Reporting Systems</h1>
      <a href="/images/front_page.png" id="homeImage"><img class="decoration" alt="Ignite Assist" src="/images/frontpage.png"></a>
      <p>Ignite Reporting Systems (Pty) Ltd is a private company that focuses on the development and commercialisation web-based operational practice management offerings to the private and public sectors. Our offerings are scalable and can assist organisations of all sizes and disciplines.</p>      
	  <p>Our commitment to our clients is to identify, meet and exceed their expectations by providing an innovative, cost effective, efficient and personal service. We have adopted a philosophy of business partnerships.  We partner with our clients and build our long-term relationships based on trust and integrity.</p>
	  <p>Ignite Reporting Systems (Pty) Ltd has 5 shareholders. The current shareholding of Ignite Reporting Systems (Pty) Ltd is as follows:</p>
	 <p>
	 <table id="ownership">
		<thead>
		<tr>
		  <th style="text-align:center;">Black Male</th>
		  <th style="text-align:center;">Black Female</th>
		  <th style="text-align:center;">White Male</th>
		  <th style="text-align:center;">White Female</th>
		</tr>
		</thead>
		<tr>
		  <th style="text-align:center;">15%</th>
		  <th style="text-align:center;">0%</th>
		  <th style="text-align:center;">55%*</th>
		  <th style="text-align:center;">30%</th>
		</tr>	
		<tr>
		 <td valign="top" colspan="4"><b><small>*15 % of the shares are held by a Dutch National who permanently resides in The Netherlands.</small></b></td>
		</tr>
	 </table> 
	 </p>
	 <p>From a South African Broad Based Black Economic Empowerment perspective, Ignite Reporting Systems (Pty) Ltd is an exempted micro enterprise and has achieved a Level Four B-BBEE status. To view our BBBEE Verification Certificate, <a href="Abacus 2674 Ignite Reporting Systems - BEE Certificate.pdf" target="_blank">click here</a></p>
     <h2>Our offering includes:</h2> 
	 <p>
	 <table id="offering">
		<tr>
			<td valign="top" >Ignite Assist</td>
			<td valign="top" >Ignite Assist is an innovative online subscription offering which provides organisations with a secure encrypted web-based platform for the management of operational processes and data storage on the internet. The tool reduces the administration burden on stakeholders in the day to day operational running of the organisation and clearly assigns responsibility and accountability to all users.</td>
		</tr>
		<tr>
			<td valign="top" >Cash Management</td>
			<td valign="top" >Ignite Reporting Systems (Pty) Ltd has an established relationship with Investec Specialist Bank and we are able to offer a value added banking administration service to our clients and to the clients of our approved Business Partners who are operationally required to hold third party funds on behalf of their clients.</td>
		</tr>	
		<tr>
			<td valign="top" >e-Ignite</td>
			<td valign="top" >e-Ignite is the online document store of Ignite Reporting Systems (Pty) Ltd. This offering provides our registered users access to our on-line document store and provides a platform where the business owner can purchase and store business related documents that can assist with the operational running of their business activities.</td>
		</tr>			
	 </table>
	 </p>
	 <!-- <p>An independent database, branded with the organisations name and logo, is created for each individual Ignite Assist client. The registered users can use the various modules of Ignite Assist to manage operational processes which include:</p>
	  <ul class="default">
	    <li>allocating and tracking activities, tasks, actions, key performance indicators (KPI's) assigned to users;</li>
        <li>uploading documents for easy reference, back-up and / or document management purposes;</li>
        <li>accumulating data for dashboard and other forms of reporting; and</li>
        <li>generating different types of reports, graphs and / or dashboards.</li>
	  </ul>	 -->
	</div>
  </div>
  <?php include("inc-side-column.php"); ?>
</div>
<img id="column-divider-bottom" src="/images/bg-hr-column.png" alt="divider" />

<?php include("inc-section-calls.php"); ?>
<?php include("footer.php"); ?>