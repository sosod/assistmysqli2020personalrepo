$.fn.equalHeights = function(px) {
	$(this).each(function(){
		var currentTallest = 0;
		$(this).children().each(function(i){
			if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
		});
		// for ie6, set height since min-height isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({'height': currentTallest}); }
		$(this).children().css({'min-height': currentTallest}); 
	});
	return this;
};

function bookmark()
{
  var title = document.title; 
  var url = window.location.href;
  if( window.sidebar )
  {
	window.sidebar.addPanel(title, url,"");
  } 
  else if( window.external )
  { 
	window.external.AddFavorite( url, title); 
  }
  else if( window.opera && window.print ) 
  { 
    var elem = document.createElement('a');
    elem.setAttribute('href',url);
    elem.setAttribute('title',title);
    elem.setAttribute('rel','sidebar');
    elem.click();
  }
  
  return false;
}

$(document).ready(function(){
	
	$("input.title-tip").each(function(){
	  $this = $(this);
	  if( !$this.val() ) 
	  {
	    $this.val( $this.attr("title") );
	    $this.addClass("active")
	  }
	})
	.focus(function(){ 
	  $this = $(this); 
	  if( $this.val() == $this.attr("title") )
	  {
  	    $this.val("");
  	    $this.removeClass("active")
	  }
	})
	.blur(function(){ 
	  $this = $(this); 
	  if( !$this.val() )
	  {
	    $this.val( $this.attr("title") );
	    $this.addClass("active")
	  }
	});	
	
	$("label.required").each(function(){
	  $this = $(this);
	  $this.append("<span>*</span>")
	})
	
	$("form").submit(function(){
	  $("input.title-tip", this).each(function(){
	    $this = $(this);
	    if( $this.val() == $this.attr("title") )
	    {
  	      $this.val("");
	    }  
	  });  	
	});
	
	$("#action-print").click(function(){
	  window.print();
	  return false;
	});
	
	$("#size-normal").click(function(){
	  $("body").css( "font-size", "13px" ).css( "line-height","1.231" );
	  return false;
	});
	
	$("#size-large").click(function(){
	  $("body").css( "font-size", "14px" ).css( "line-height","1.431" );
	  return false;
	});
	
	$("#size-larger").click(function(){
	  $("body").css( "font-size", "15px" ).css( "line-height","1.631" );
	  return false;
	});
	
	$("#column-wrapper").equalHeights();
});



