<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Add New Directorate</b></h1>
<p>&nbsp;</p>
<?php
$dirtxt = htmlentities($_POST['dirtxt'],ENT_QUOTES,"ISO-8859-1");
$pos = $_POST['pos'];
$poslist = $_POST['poslist'];
$sub = $_POST['sub'];

function lastPos($cmpcode,$modref)
{
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                    $dirsort = $row['dirsort'] + 1;
                }
                else
                {
                    $dirsort = 1;
                }
            mysql_close();
            return $dirsort;
}

//SET POSITION
switch($pos)
{
    case "a":
        $dirsort = 0;
      //get position of dir to follow
        if(is_numeric($poslist))
        {
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' AND dirid = ".$poslist." ORDER BY dirsort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                }
                else
                {
                    $dirsort = lastPos($cmpcode,$modref);
                }
        }
        else
        {
            $dirsort = lastPos($cmpcode,$modref);
        }
        if($dirsort<1)
        {
          //set dirsort = apos + 1
            $dirsort = $row['dirsort']+1;
          //update sql set dirsort = dirsort + 1 where dirsort > apos
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_dir SET dirsort = dirsort + 1 WHERE diryn = 'Y' AND dirsort > ".$row['dirsort'];
            include("inc_db_con.php");
        }
        break;
    case "b":
        break;
    case "f":
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_dir SET dirsort = dirsort + 1 WHERE diryn = 'Y'";
        include("inc_db_con.php");
        $dirsort = 1;
        break;
    case "l":
        $dirsort = lastPos($cmpcode,$modref);
        break;
    default:
        $dirsort = lastPos($cmpcode,$modref);
        break;
}

//ADD record
$dirid = 0;
$sql = "INSERT INTO assist_".$cmpcode."_".$modref."_dir SET ";
$sql.= "  dirtxt = '".$dirtxt."'";
$sql.= ", dirsort = ".$dirsort;
$sql.= ", diryn = 'Y'";
include("inc_db_con.php");
//get dirid
$dirid = mysql_insert_id();
$tsql = $sql;
        $logref = $dirid;
        $logtype = "dir";
        $logaction = "Added directorate '".$dirtxt."'";
        $logdisplay = "Y";
        include("inc_log.php");
$told = "";
$trans = $logaction." with id ".$dirid;
include("inc_transaction_log.php");
if($dirid>0)
{
    //Create record in assist_cc_list_dept
    $sql = "INSERT INTO assist_".$cmpcode."_list_dept SET";
    $sql.= "  value = '".$dirtxt."'";
    $sql.= ", assist = 9";
    $sql.= ", yn = 'Y'";
//    include("inc_db_con.php");
//    $assid = mysql_insert_id();
    $sql = "INSERT INTO assist_".$cmpcode."_sdbp2_dir_dept SET ";
    $sql.= "  dirid = ".$dirid;
    $sql.= ", deptid = ".$assid;
    $sql.= ", ds = 'D'";
//    include("inc_db_con.php");
    //create HoD sub
    $subsort = 1;
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_dirsub SET ";
    $sql.= "  subtxt = 'Head of ".$dirtxt."'";
    $sql.= ", subyn = 'Y'";
    $sql.= ", subdirid = ".$dirid;
    $sql.= ", subsort = ".$subsort;
    $sql.= ", subhead = 'Y'";
    include("inc_db_con.php");
$tsql = $sql;
        $subid = mysql_insert_id();
        $logref = $subid;
        $logtype = "sub";
        $logaction = "Added sub-directorate 'Head of ".$dirtxt."'";
        $logdisplay = "Y";
        include("inc_log.php");
$told = "";
$trans = $logaction." with id ".$subid;
include("inc_transaction_log.php");
    //add subs
    foreach($sub as $subtxt)
    {
        if(strlen($subtxt)>0)
        {
            $subsort++;
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_dirsub SET ";
            $sql.= "  subtxt = '".htmlentities($subtxt,ENT_QUOTES,"ISO-8859-1")."'";
            $sql.= ", subyn = 'Y'";
            $sql.= ", subdirid = ".$dirid;
            $sql.= ", subsort = ".$subsort;
            $sql.= ", subhead = 'N'";
            include("inc_db_con.php");
                $subid = mysql_insert_id();
$tsql = $sql;
                $logref = $subid;
                $logtype = "sub";
                $logaction = "Added sub-directorate '".$subtxt."'";
                $logdisplay = "Y";
                include("inc_log.php");
$told = "";
$trans = $logaction." with id ".$subid;
include("inc_transaction_log.php");
        }
    }
    //display result
    ?>
    <script type=text/javascript>parent.header.location.href = '/title_login.php?c=SDBP2&cust=N';</script>
    <p>New Directorate '<?php echo($dirtxt); ?>' has been successfully added.</p>
    <p>&nbsp;</p>
<?php
}
else
{
    ?>
    <p>An error has occurred.  Please go back and try again.</p>
    <p>&nbsp;</p>
    <?php
}
?>
    <?php
    $urlback = "setup_dir.php";
    include("inc_goback.php");
    ?>
</body>
</html>
