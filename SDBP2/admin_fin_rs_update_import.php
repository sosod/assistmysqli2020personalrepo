<?php
    include("inc_ignite.php");
    
    $fileloc = $_POST['floc'];
    $timeid = $_POST['t'];
    
    if(strlen($timeid)>0 && is_numeric($timeid))
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$timeid;
        include("inc_db_con.php");
            $time = mysql_fetch_array($rs);
        mysql_close();
        $timerr = "N";
    }
    else
    {
        $timerr = "Y";
    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<h2 class=fc>Update Revenue By Source - Confirm import data<input type=hidden id=lab></h2><center>
<?php if($timerr == "Y") { ?>
<h3 class=fc>ERROR!</h3>
<p>An error has occurred.  Please go back and try again.</p>
                <?php
                    $urlback = "admin_fin.php";
                    include("inc_goback.php");
                ?>

<?php } else { //timerr ?>
<h3 class=fc><?php echo(date("F Y",$time['eval'])); ?></h3>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script>
<?php


$err = "N";
$p=0;
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}
else
{
}

if($err == "N")
{
            $file = fopen($fileloc,"r");
            $f = 1;
            $r=1;
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>5 && $r>1)
                {
                    $data[$f] = $tmpdata;
                    $f++;
                }
                $r++;
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
          
                ?>
                <p>Note: The import process will not be complete until you click the "Accept" button below the table.</p>
                <form id=accept method=post action=admin_fin_rs_update_import_accept.php><input type=hidden name=floc value="<?php echo($fileloc); ?>"><input type=hidden name=calc value="<?php echo($calc); ?>"><input type=hidden name=t value="<?php echo($timeid); ?>">
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader  colspan=2>Ignite<br>Ref</td>
                        <td class=tdheader >Line Item</td>
                        <td class=tdheader >Month</td>
                        <td class=tdheader >Billed</td>
                        <td class=tdheader >Received</td>
                    </tr>
                    <!-- <tr>
                    </tr> -->
                    <?php
                    $p2 = 100 / count($data);
                    foreach($data as $idata)
                    {
                        $p = $p + $p2;
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $lineid = $idata[0];
                        $month = date("M Y",$time['eval']);
                        if(strlen($lineid)>0 && is_numeric($lineid) && $lineid > 0)
                        {
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_revbysource ";
                            $sql.= " WHERE rsid = ".$lineid;
                            include("inc_db_con.php");
                                $mnr = mysql_num_rows($rs);
                                if($mnr > 0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $line = $row['rsline'];
                        ?>
                    <tr>
                        <td class=tdheader align=center><input type=checkbox name=import[] checked value=<?php echo($lineid); ?>></td>
                        <td class=tdheader><?php echo($lineid); ?></td>
                        <td class=tdgeneral><?php echo($line); ?></td>
                        <td class=tdgeneral style="border-right: 1px solid #555555"><?php echo($month); ?></td>
                    <?php
                            $val = 0;
                            $val = $idata[4];
                            if(!is_numeric($val))
                            {
                                $v = explode(",",$val);
                                $val = implode("",$v);
                            }
                            if(strlen($val)==0 || !is_numeric($val)) { $val = 0; }
                            echo("<td class=tdgeneral align=right>".number_format($val,2)."</td>");
                            $val = 0;
                            $val = $idata[5];
                            if(!is_numeric($val))
                            {
                                $v = explode(",",$val);
                                $val = implode("",$v);
                            }
                            if(strlen($val)==0 || !is_numeric($val)) { $val = 0; }
                            echo("<td class=tdgeneral align=right>".number_format($val,2)."</td>");
                    ?>
                    </tr>
                        <?php
                                }   //mnr > 0
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No line item found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                            mysql_close();
                        }   //valid cpid
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No line item found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                    }
                    ?>
                </table>
                <p><input type=button value=Accept id=ac onclick="document.forms['accept'].submit(); document.getElementById('ac').disabled=true;">  <input type=button value=Reject onclick="document.location.href = 'admin_fin_cf_update.php';"></p>
                </form>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "admin_fin_rs_update.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",500);
                </script>
<?php
}   //timeerr

?>
</body>
</html>
