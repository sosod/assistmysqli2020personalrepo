<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<script type=text/javascript>
function clearKPA() {
    if(confirm("MUNICIPAL KPAs\n\nDelete them all?")==true)
    {
        document.location.href = "import_clear.php?t=MUN";
    }
}

function clearCP() {
    if(confirm("CAPITAL PROJECTs\n\nDelete them all?")==true)
    {
        document.location.href = "import_clear.php?t=CAP";
    }
}

function clearKPI() {
    if(confirm("KPIs\n\nDelete them all?")==true)
    {
        document.location.href = "import_clear.php?t=KPI";
    }
}

function clearRevBS() {
    if(confirm("REVENUE BY SOURCE\n\nDelete them all?")==true)
    {
        document.location.href = "import_clear.php?t=RBS";
    }
}

function clearCashF() {
    if(confirm("MONTHLY CASHFLOW\n\nDelete them all?")==true)
    {
        document.location.href = "import_clear.php?t=CF";
    }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import</b></h1>

<table cellpadding=5 cellspacing=0>

<tr><td class=tdgeneral width=49% valign=top style="border-color: #fff;">
<table cellpadding=3 cellspacing=0 style="border: 0px solid #ffffff;" width=100%>


<tr>
    <td style="border: 0px solid #ffffff;" class=tdheaderred style="font-size: 12pt; line-height: 16pt; font-family: Arial">Municipal KPAs</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clearKPA();" style="background-color: #cc0001; color: #ffffff;">
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=munkpa action=import_munkpa.php method=post enctype="multipart/form-data">
            <b>Load:</b> <input type=file name=ifile> <input type=submit value=Import style="background-color: #cc0001; color: #ffffff;">
        </form>
    </td>
</tr>




<tr>
    <td style="border: 0px solid #ffffff;" class=tdheaderblue style="font-size: 12pt; line-height: 16pt; font-family: Arial">KPIs</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="-Clear" onclick="clearKPI();" style="background-color: #000099; color: #ffffff;">
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=kpi action=import_kpi.php method=post enctype="multipart/form-data">
            <b>Load:</b><br>
            &nbsp;&nbsp;&nbsp;<i>File: <input type=file name=ifile><br>
            &nbsp;&nbsp;&nbsp;Directorate:</i> <select name=dir><option selected value=X>--- SELECT ---</option>
            <?php
                $cdir = array();
                $sql = "select count(kpiid) as kc, dirid from assist_".$cmpcode."_".$modref."_kpi, assist_".$cmpcode."_".$modref."_dirsub, assist_".$cmpcode."_".$modref."_dir where dirid = subdirid and subid = kpisubid group by dirid";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $cdir[$row['dirid']] = $row['kc'];
                    }
                mysql_close();

                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $id = $row['dirid'];
                        $val = $row['dirtxt'];
                        echo("<option value=".$id.">".$id.": ".$val." (");
                        if(strlen($cdir[$id])==0) { echo("0"); } else { echo($cdir[$id]); }
                        echo(")</option>");
                    }
                mysql_close();
            ?>
            </select><br>
            <input type=submit value=Import style="background-color: #000099; color: #ffffff;"><br>
            <small>Ensure that Col A is blank where no KPI is to be loaded<br>else a Sub will be created.</small>
        </form>
    </td>
</tr>


</table>



</td>
<td class=tdgeneral  valign=top align=right width=2% style="border-color: #fff;">
&nbsp;
</td>
<td class=tdgeneral  valign=top align=right width=49% style="border-color: #fff;">


<table cellpadding=3 cellspacing=0 style="border: 0px solid #ffffff;" width=100%>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdheadergreen style="font-size: 12pt; line-height: 16pt; font-family: Arial">Capital Projects</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="-Clear" onclick="clearCP();" style="background-color: #009900; color: #ffffff;">
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=capital action=import_capital.php method=post enctype="multipart/form-data">
            <b>Load:</b> <input type=file name=ifile> <input type=submit value=Import style="background-color: #009900; color: #ffffff;">
        </form>
    </td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class=tdheaderorange style="font-size: 12pt; line-height: 16pt; font-family: Arial">Cashflow</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral><img src=/pics/blank.gif height=1></td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class=tdheaderorange>Revenue By Source</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clearRevBS();" style="background-color: #fe9900; color: #ffffff;">
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=revbysource action=import_revbysource.php method=post enctype="multipart/form-data">
            <b>Load:</b> <input type=file name=ifile> <input type=submit value=Import style="background-color: #fe9900; color: #ffffff;">
        </form>
    </td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class=tdheaderorange>Monthly Cashflow</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clearCashF();" style="background-color: #fe9900; color: #ffffff;">
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=cashflow action=import_cashflow.php method=post enctype="multipart/form-data">
            <b>Load data:</b> <input type=file name=ifile>&nbsp;<input type=submit value=Import style="background-color: #fe9900; color: #ffffff;">
        </form>
    </td>
</tr>


</table>

</td></tr></table>

</body>

</html>
