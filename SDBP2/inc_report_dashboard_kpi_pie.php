<style type=text/css>
.legend {
	font: Tahoma;
	font-size: 6.5pt;
	line-height: 7.5pt;
	border-color: #ffffff;
}
.charttitle {
	font-weight: bold;
	font-size: 13pt;
	font:Verdana; 
	text-decoration:underline;
	margin-top: 10pt;
}
</style>
<?php
//GET ACTIVEYN FOR TIME
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE sort = ".$wt;
include("inc_db_con.php");
	$row = mysql_fetch_assoc($rs);
mysql_close($con);
$active = strlen($row['active'])==1 ? $row['active'] : "N";

$layout = $_REQUEST['layout'];
if($layout != "P" && $layout != "L") { $layout = "ONS"; }
		$cwidth = 250;
		$cheight = 250;
		$cradius = 100;
switch($layout) {
	case "L":
		$tdl = 3;
		$cwidth = 250;
		$cheight = 200;
		$cradius = 75;
		break;
	case "P":
		$tdl = 2;
		break;
	default:
		$tdl = 1;
		$layout = "ONS";
		break;
}
if(strlen($referer)>0) { } else { $referer = "."; }
function drawLegend($kpi) {
global $dtype;
global $did;
global $wf;
global $wt;
global $layout;
$g = $kpi['g']>0 ? $kpi['g'] : 0;
$o = $kpi['o']>0 ? $kpi['o'] : 0;
$r = $kpi['r']>0 ? $kpi['r'] : 0;
$x = $kpi['x']>0 ? $kpi['x'] : 0;
$tot = $kpi['tot']>0 ? $kpi['tot'] : 0;
$url = "report_kpi_process.php?src=DASH&krsum=Y";
    switch($dtype)
    {
        case "d":
            $url.= "&dirfilter=".$did."&subfilter=ALL";
            break;
        case "s":
            $url.= "&dirfilter=ALL&subfilter=".$did;
            break;
        default:
			$url.= "&dirfilter=ALL&subfilter=ALL";
            break;
    }
$url.="&output=display&cf=".$wf."&ct=".$wt."&kpiresultfilter=";
echo("<table cellpadding=3 cellspacing=3 style=\"border: 1px solid #ababab;margin: 0 0 0 0;\">");
	echo("<tr>");
		echo("<td width=10 class=legend style=\"background-color: #006600;\">&nbsp;</td>");
		echo("<td class=legend><a href=".$url."3 style=\"color:#006600;\">KPIs Met</a> (".$g." of ".$tot.")</td>");
if($layout=="P") {
		echo("</tr><tr>");
} else {
		echo("<td width=20 class=legend>&nbsp;</td>");
}
		echo("<td width=10 class=legend style=\"background-color: #FE9900;\">&nbsp;</td>");
		echo("<td class=legend><a href=".$url."2 style=\"color:#FE9900;\">KPIs Almost Met</a> (".$o." of ".$tot.")</td>");
	echo("</tr>");
	echo("<tr>");
		echo("<td width=10 class=legend style=\"background-color: #CC0001;\">&nbsp;</td>");
		echo("<td class=legend><a href=".$url."1 style=\"color:#CC0001;\">KPIs Not Met</a> (".$r." of ".$tot.")</td>");
if($layout=="P") {
		echo("</tr><tr>");
} else {
		echo("<td width=20 class=legend>&nbsp;</td>");
}
		echo("<td width=10 class=legend style=\"background-color: #999;\">&nbsp;&nbsp;</td>");
		echo("<td class=legend><a href=".$url."0 style=\"color:#777;\">KPIs Not Measured</a> (".$x." of ".$tot.")</td>");
	echo("</tr>");
echo("</table>");
}

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE eval < ".$today." ORDER BY id DESC";
include("inc_db_con.php");
if(mysql_num_rows($rs)>0)
{
    $row = mysql_fetch_array($rs);
    $ytdid = $row['id'];
}
else
{
    $ytdid = 0;
}
mysql_close();


$kpi['g'] = 0;
$kpi['o'] = 0;
$kpi['r'] = 0;
$kpi['x'] = 0;
$kpi['err'] = 0;
$kpi['tot'] = 0;
$calctype = array();

//GENERATE SQL
$sql = "SELECT k.kpiid, k.kpicalctype, s.subid, d.dirid FROM";
$sql.= "  assist_".$cmpcode."_".$modref."_kpi k";
$sql.= ", assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s";
$sql.= " WHERE k.kpiyn = 'Y' AND k.kpisubid = s.subid AND s.subdirid = d.dirid AND s.subyn = 'Y' AND d.diryn = 'Y' ";
$sql.= " AND kpiid IN (SELECT DISTINCT krkpiid FROM assist_".$cmpcode."_".$modref."_kpi_result)";
    switch($dtype)
    {
        case "d":
            $sql.= " AND d.dirid = ".$did;
            break;
        case "s":
            $sql.= " AND k.kpisubid = ".$did;
            break;
        default:
            break;
    }
include("inc_db_con.php");
//echo("<P>".$sql);
    while($row = mysql_fetch_array($rs))
    {
        $kid = $row['kpiid'];
        $kct = strtoupper($row['kpicalctype']);
        if($who2=="Y")
        {
            if($dtype == "d")
            {
                $moreid = $row['subid'];
            }
            else
            {
                $moreid = $row['dirid'];
            }
        }
        else
        {
            $moreid = 0;
        }
        
        
                switch($kct)
                {
                    case "CO":
                        $act = "max(krtarget) as krtarget, max(kractual) as kractual";
                        break;
                    default:
                        $act = "sum(krtarget) as krtarget, sum(kractual) as kractual";
                        break;
                }
        $sql2 = "SELECT ".$act." FROM assist_".$cmpcode."_".$modref."_kpi_result r, assist_".$cmpcode."_".$modref."_list_time t ";
        $sql2.= " WHERE r.krkpiid = ".$kid." AND t.id = r.krtimeid AND t.sort <= ".$wt." AND t.yn = 'Y'";
        if($kct != "ACC" && $kct!="CO")    {    $sql2.= " AND t.sort >= ".$wf;    }
        if($referer!="report_dashboard") {        $sql2.= " AND (t.eval < ".$today." OR kractual > 0)";         }
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                            $mnr2 = mysql_num_rows($rs2);
                        mysql_close($con2);
//echo("<P>".$kid."-".$kct);
if($mnr2>0)
{
                $rt = $row2['krtarget'];
                $ra = $row2['kractual'];
//echo("-".$rt."-".$ra);

    $krr = "x";
    if($kct == "ZERO")
    {
        if($ra > $rt)
        {
            $krr = "r";
        }
        else
        {
            $krr = "g";
        }
    }
    else
    {
    if($referer!="report_dashboard" || ($referer=="report_dashboard" && ($ra>0 || $rt > 0)))// || ($when=="YTD" && $rt>0))))
    {
        if($ra >= $rt && $ra > 0)
        {
            $krr = "g";
        }
        else
        {
            if($rt > 0)
            {
                if($ra/$rt < 0.75)
                {
                    $krr = "r";
                }
                else
                {
                    $krr = "o";
                }
            }
        }
    }
    else
    {
        $krr = "x";
    }
    }
}
else
{
    $krr = "x";
}
//echo("-".$krr);

if($krr == "x" && $active == "N") { $krr = "r"; }
        $kpi[$krr]++;
        $kpi[$moreid][$krr]++;
    }
mysql_close;
$kpi['tot'] = $kpi['g'] + $kpi['o'] + $kpi['r'] + $kpi['x'];

?>
<div align=center style="margin: 0 0 0 0; padding: 0 0 0 0;">
<table cellpadding=2 cellspacing=0 style="border: 1px solid #FFF;">
    <tr>
        <td class=tdgeneral colspan=<?php echo $tdl; ?> align=center style="border: 1px solid #fff;" > 
<?php
if($kpi['tot'] > 0)
{
/*
 * Read README file first.
 *
 * This example shows how to create a simple pie chart with a few configuration
 * directives and a connected HTML table as legend.
 * The values are from http://www.globalissues.org/article/26/poverty-facts-and-stats
 */

// Require necessary files
require("lib/AmPieChart.php");

// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js and AmCharts.js (only needed for pie legend)
AmChart::$jsPath = "lib/AmCharts.js";
AmChart::$jQueryPath = "lib/jquery.js";
// Tell AmChart to load jQuery if you don't already use it on your site.
AmChart::$loadJQuery = true;

// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmPieChart("kpi_".$who);

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
$chart->setTitle("<p class=charttitle>".$titletxt."</p>");

// Add slices
$chart->addSlice("met", "KPI&#39;s met (".$kpi['g']." of ".$kpi['tot'].")", $kpi['g'], array("color" => "#009900"));
$chart->addSlice("almost", "KPI&#39;s almost met (".$kpi['o']." of ".$kpi['tot'].")", $kpi['o'], array("color" => "#fe9900"));
$chart->addSlice("notmet", "KPI&#39;s not met (".$kpi['r']." of ".$kpi['tot'].")", $kpi['r'], array("color" => "#cc0001"));
if($kpi['x']/$kpi['tot']<0.5 && $referer != "report_dashboard")
{
    $chart->addSlice("notmeasure", "KPI&#39;s not yet measured (".$kpi['x']." of ".$kpi['tot'].")", $kpi['x'], array("color" => "#999999", "pull_out" => "true"));
}
else
{
    $chart->addSlice("notmeasure", "KPI&#39;s not yet measured (".$kpi['x']." of ".$kpi['tot'].")", $kpi['x'], array("color" => "#999999"));//, "pull_out" => "true"));
}

//Settings
if($anima=="Y") {
    $chart->setConfigAll(array(
    "width" => 410,
    "height" => 350,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "40%",
    "pie.radius" => 100,
    "pie.height" => 5,
    "animation.start_time" => 2,
    "animation.start_effect" => "regular",
    "animation.pull_out_time" => 1,
    "animation.pull_out_effect" => "regular",
    "animation.pull_out_radius" => "10%",
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
//    "background.border_alpha" => 15,
    "legend.y" => "83%",
    "legend.x" => "2%",
    "legend.width" => "96%",
    "legend.border_alpha" => 20,
    "legend.text_size" => 9,
    "legend.spacing" => 4,
    "legend.margins" => 8,
    "legend.align" => "center",
	"legend.enabled" => "true",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));
}
else
{
	if($referer == "report_dashboard") { 
    $chart->setConfigAll(array(
    "width" => $cwidth,
    "height" =>$cheight,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "49%",
    "pie.radius" => $cradius,
    "pie.height" => 5,
    "background.border_alpha" => 0,
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
    "legend.enabled" => "false",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>" /*,
	"export_as_image.file" => "lib/amcharts/export.php",
	"export_as_image.target" => "_blank",
	"export_as_image.x" => 10,
	"export_as_image.y" => 10,
	"export_as_image.color" => '#000000'*/
    ));
	} else {
    $chart->setConfigAll(array(
    "width" => 400,
    "height" =>350,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "40%",
    "pie.radius" => 100,
    "pie.height" => 5,
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
    "legend.y" => "83%",
    "legend.x" => "2%",
    "legend.width" => "96%",
    "legend.border_alpha" => 20,
    "legend.text_size" => 9,
    "legend.spacing" => 4,
    "legend.margins" => 8,
    "legend.align" => "center",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));
	}
}
// Print the code
	echo html_entity_decode($chart->getCode());
	if($referer=="report_dashboard") {
		drawLegend($kpi);
	}
} else {
echo("<p><span style=\"font-weight: bold;align: center\">".$titletxt."</span></p>");
echo("<p>No KPIs to display.</p>");
}
?>
        </td>
    </tr>
    
<?php
if($who2=="Y")
{
$td2 = 1;
    $morerow = array();
    if($dtype=="d")
    {
        $sql = "SELECT subid id, subtxt txt FROM assist_".$cmpcode."_".$modref."_dirsub, assist_".$cmpcode."_".$modref."_kpi WHERE kpisubid = subid AND kpiyn = 'Y' AND subdirid = ".$did." AND subyn = 'Y' ORDER BY subsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $morerow[$row['id']] = $row;
            }
        mysql_close();
    }
    else
    {
        $sql = "SELECT dirid id, dirtxt txt FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $morerow[$row['id']] = $row;
            }
        mysql_close();
    }

    if(count($morerow)>1)
    {
        echo "<tr> ";
        $tdc = -1;
        foreach($morerow as $mr)
        {
            $tdc++;
$g = $kpi[$mr['id']]['g'];
$o = $kpi[$mr['id']]['o'];
$r = $kpi[$mr['id']]['r'];
$x = $kpi[$mr['id']]['x'];
if(strlen($g)==0) { $g=0; }
if(strlen($o)==0) { $o=0; }
if(strlen($r)==0) { $r=0; }
if(strlen($x)==0) { $x=0; }
$tot = $g+$o+$r+$x;
$kpi[$mr['id']]['tot'] = $tot;
if($tot > 0) {
$chart = new AmPieChart("kpi_".$mr['id']);

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
if($tdc==$tdl) {
$td2++;
if($td2 == 2) {
	echo("</tr><tr style=\"page-break-before: always;\"> ");
	$td2 = 0;
} else {
	echo("</tr><tr>");
}
echo("<td class=tdgeneral align=center style=\"border: 1px solid #FFF;\" valign=top width=410>");
$chart->setTitle("<p  class=charttitle>".$mr['txt']."</span></p>");
$tdc=0;
} else {
$chart->setTitle("<p class=charttitle>".$mr['txt']."</p>");
echo("<td class=tdgeneral align=center style=\"border: 1px solid #FFF;\" valign=top width=410>");
}



// Add slices
$chart->addSlice("met", "KPI&#39;s met (".$g." of ".$tot.")", $g, array("color" => "#009900"));
$chart->addSlice("almost", "KPI&#39;s almost met (".$o." of ".$tot.")", $o, array("color" => "#fe9900"));
$chart->addSlice("notmet", "KPI&#39;s not met (".$r." of ".$tot.")", $r, array("color" => "#cc0001"));
if($referer == "report_dashboard") {
	$chart->addSlice("notmeasure", "KPI&#39;s not yet measured (".$x." of ".$tot.")", $x, array("color" => "#999999"));
} else {
	$chart->addSlice("notmeasure", "KPI&#39;s not yet measured (".$x." of ".$tot.")", $x, array("color" => "#999999", "pull_out" => "true"));
}

	if($referer == "report_dashboard") { 
    $chart->setConfigAll(array(
    "width" => $cwidth,
    "height" =>$cheight,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "49%",
    "pie.radius" => $cradius,
    "pie.height" => 5,
    "background.border_alpha" => 0,
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
    "legend.enabled" => "false",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));
	} else {
    $chart->setConfigAll(array(
    "width" => 400,
    "height" =>350,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "40%",
    "pie.radius" => 100,
    "pie.height" => 5,
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
    "legend.y" => "83%",
    "legend.x" => "2%",
    "legend.width" => "96%",
    "legend.border_alpha" => 20,
    "legend.text_size" => 9,
    "legend.spacing" => 4,
    "legend.margins" => 8,
    "legend.align" => "center",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));
	}

// Print the code
echo html_entity_decode($chart->getCode());
	if($referer=="report_dashboard") {
		drawLegend($kpi[$mr['id']]);
	}
} else {
	echo("<p class=charttitle>".$mr['txt']."</p>");
	echo("<p>No KPIs to display.</p>");
}
            echo("</td>");

        }
        echo "</tr>";
    }

}
?>
</table>
</div>
<?php
//print_r($kpi);
?>

