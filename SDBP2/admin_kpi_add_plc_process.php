<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Add KPI</b></h1>
<p>&nbsp;</p>
<p>Processing...</p>
<?php
$err = "N";
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
$plcid = $_POST['plcid'];
//FORM DETAILS
$kpiid = $_POST['kpiid'];
$pname = $_POST['plcprojectname'];
$pdescrip = $_POST['plcprojectdescription'];
$pstartdate = $_POST['plcstartdate'];
$penddate = $_POST['plcenddate'];
$proccate = $_POST['proccate'];
//FORM ARRAYS
$phaseid = $_POST['phaseid'];
$phasetype = $_POST['phasetype'];
$appyn = $_POST['app'];
$cal = $_POST['cal'];
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$target = $_POST['target'];
$custom = $_POST['custom'];
//ARRAY COUNTERS
$pi = 0;    //phaseid
$pt = 0;    //phasetype
$cl = 0;    //cal
$sd = 0;    //startdate
$ed = 0;    //enddate
$tg = 0;    //target
$cu = 0;    //custom
$sort = 1;

/*echo("<P>pid-");
print_r($phaseid);
echo("<P>pt-");
print_r($phasetype);
echo("<P>app-");
print_r($appyn);
echo("<P>cal-");
print_r($cal);
echo("<P>sd-");
print_r($startdate);
echo("<P>ed-");
print_r($enddate);
echo("<P>tar-");
print_r($target);
echo("<P>cust-");
print_r($custom);
*/

//DELETE OLD PHASES IF PLCID EXISTS
if(strlen($plcid)>0 && is_numeric($plcid))
{
    $sql = "DELETE FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
    include("inc_db_con.php");
}

//VALIDATE KPI-PLC DETAILS
if(is_numeric($kpiid) && $kpiid > 0 && strlen($pstartdate) > 0 && strlen($penddate) > 0)
{
    //FORMAT KPI-PLC VARIABLES
    $pname = htmlentities($pname,ENT_QUOTES,"ISO-8859-1");
    $pdescrip = htmlentities($pdescrip,ENT_QUOTES,"ISO-8859-1");
    if(strlen($proccate)!=1 || !is_numeric($proccate) || $proccate < 1 || $proccate > 4)    { $proccate = 1; }
    $psd = explode("-",$pstartdate);    //STARTDATE
    $psd[0] = $psd[0] * 1;  //YEAR
    $psd[1] = $psd[1] * 1;  //MONTH
    $psd[2] = $psd[2] * 1;  //DAY
    $plcstart = mktime(12,0,0,$psd[1],$psd[2],$psd[0]);
    $ped = explode("-",$penddate);    //ENDDATE
    $ped[0] = $ped[0] * 1;  //YEAR
    $ped[1] = $ped[1] * 1;  //MONTH
    $ped[2] = $ped[2] * 1;  //DAY
    $plcend = mktime(12,0,0,$ped[1],$ped[2],$ped[0]);
    //CREATE KPI-PLC RECORD
    if(strlen($plcid)>0 && is_numeric($plcid))
    {
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc SET";
    }
    else
    {
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc SET";
    }
    $sql.= "  plcname = '".$pname."'";
    $sql.= ", plcdescription = '".$pdescrip."'";
    $sql.= ", plckpiid = ".$kpiid;
    $sql.= ", plcstartdate = ".$plcstart;
    $sql.= ", plcenddate = ".$plcend;
    $sql.= ", plcproccate = ".$proccate;
    $sql.= ", plcyn = 'P'";
    if(strlen($plcid)>0 && is_numeric($plcid))
    {
        $sql.= " WHERE plcid = ".$plcid;
    }
    include("inc_db_con.php");
    //GET KPI-PLC ID
    if(strlen($plcid)>0 && is_numeric($plcid))
    {
    }
    else
    {
        $plcid = mysql_insert_id($con);
    }
    //VALIDATE plcid
    if($plcid > 0 && is_numeric($plcid))
    {
        $a = 0;
        $pt=-1;
        //LOOP THROUGH APP ARRAY
        foreach($appyn as $app)
        {
//            echo("<P>".$a."-".$app."-i".$pi."-t".$pt."-c".$cl."-s".$sd."-e".$ed."-g".$tg);
            $a++;
            $pt++;
            $ptype = $phasetype[$pt];
            //IF APP = N
            if($app == "N")
            {
                //IF TYPE != C THEN INCREMENT PHASEID
                if($ptype != "C") { $pi++; /*echo("ME".$ptype.$pt."!");*/ }
                //INCREMENT PHASETYPE
//                $pt++;
            }
            else //IF APP = Y
            {
                $pid = $phaseid[$pi];
//                echo("<P>APP=Y:".$pic."
                if($pid == "X" && $ptype == "C")
                {
/*                    echo("<P>".$pi."-Blank Cust</p>");
                    echo("<P>pid:".$pid."-".$pi);
                    echo("<P>pt:".$ptype."-".$pt);
                    echo("<P>cd:".$cdays."-".$cl);
                    echo("<P>sd:".$sdate."-".$sd);
                    echo("<P>ed:".$edate."-".$ed);
                    echo("<P>tar:".$targ."-".$tg);
                    echo("<P>cust:".$cust."-".$cu);
*/                    $pi++;
//                    $pt++;
                }
                else
                {
                $cdays = $cal[$cl];
                $sdate = $startdate[$sd];
                $edate = $enddate[$ed];
                $targ = $target[$tg];
                if($ptype == "C")
                {
                    $cust = $custom[$cu];
                    $cu++;
                }
                //VALIDATE PHASE DETAILS
                if( ( ( $ptype == "C" && ( ($pid == "NEW" && strlen($cust) > 0) || (is_numeric($pid) && $pid > 0) ) ) || ( ( $ptype == "S" || $ptype == "P" || $ptype == "L" ) && is_numeric($pid) && $pid > 0 ) ) && strlen($sdate)==10 && strlen($edate)==10 )
                {
                    //IF NEW CUSTOM CREATE LIST-PLCPHASES-CUSTOM RECORD
                    if($ptype == "C" && $pid == "NEW" && strlen($cust) > 0)
                    {
                        if($dirid > 0 && strlen($dirid) > 0 && is_numeric($dirid))
                        {
                            $cust = htmlentities($cust,ENT_QUOTES,"ISO-8859-1");
                            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_plcphases_custom SET";
                            $sql.= "  value = '".$cust."'";
                            $sql.= ", yn = 'Y'";
                            $sql.= ", dirid = ".$dirid;
                            include("inc_db_con.php");
                            $pid = mysql_insert_id($con);
                        }
                    }
                    if($pid > 0 && is_numeric($pid) && strlen($pid) > 0)
                    {
                        //FORMAT VARIABLES
                        $ppsd = explode("-",$sdate);    //STARTDATE
                        $ppsd[0] = $ppsd[0] * 1;  //YEAR
                        $ppsd[1] = $ppsd[1] * 1;  //MONTH
                        $ppsd[2] = $ppsd[2] * 1;  //DAY
                        $ppstart = mktime(12,0,0,$ppsd[1],$ppsd[2],$ppsd[0]);
                        $pped = explode("-",$edate);    //ENDDATE
                        $pped[0] = $pped[0] * 1;  //YEAR
                        $pped[1] = $pped[1] * 1;  //MONTH
                        $pped[2] = $pped[2] * 1;  //DAY
                        $ppend = mktime(12,0,0,$pped[1],$pped[2],$pped[0]);
                        //IF VALID CREATE KPI-PLC-PHASES RECORD
                        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases SET";
                        $sql.= "  ppplcid = ".$plcid;
                        $sql.= ", pptype = '".$ptype."'";
                        $sql.= ", ppphaseid = ".$pid;
                        $sql.= ", ppsort = ".$sort;
                        $sql.= ", ppdays = ".$cdays;
                        $sql.= ", ppstartdate = ".$ppstart;
                        $sql.= ", ppenddate = ".$ppend;
                        $sql.= ", pptarget = ".$targ;
                        include("inc_db_con.php");
//                        echo("<P>".$pi."-".$sql);
                        //INCREMENT
                        $sort++;
                        $pi++;    //phaseid
//                        $pt++;    //phasetype
                        $cl++;    //cal
                        $sd++;    //startdate
                        $ed++;    //enddate
                        $tg++;    //target
                    }
                    else
                    {
                        echo("<P>ERROR - NO PHASE ID</p>");
                        $err = "Y";
                    }

                }
                else
                {
                    echo("<P>".$pi."-ERROR - INVALID PHASE DETAILS</p>");
/*                    echo("<P>pid:".$pid."-".$pi);
                    echo("<P>pt:".$ptype."-".$pt);
                    echo("<P>cd:".$cdays."-".$cl);
                    echo("<P>sd:".$sdate."-".$sd);
                    echo("<P>ed:".$edate."-".$ed);
                    echo("<P>tar:".$targ."-".$tg);
                    echo("<P>cust:".$cust."-".$cu);
*/                        $err = "Y";
                }
                }
            }
        }
                echo("<form id=step3 method=post action=\"admin_kpi_add_plc_confirm.php\">");
                echo("<input type=hidden name=kpiid value=\"".$kpiid."\">");
                echo("<input type=hidden name=plcid value=\"".$plcid."\">");
                echo("<input type=hidden name=dirid value=\"".$dirid."\">");
                echo("<input type=hidden name=subid value=\"".$subid."\">");
                echo("<input type=hidden name=typ value=\"".$typ."\">");
                echo("<input type=hidden name=src value=\"".$src."\">");
                echo("</form>");
                echo("<script type=text/javascript>");
                echo("document.forms['step3'].submit();");
                echo("</script>");

    }
    else
    {
        echo("<p>ERROR! - NO PLC ID</p>");
                        $err = "Y";
    }
}
else
{
    echo("<P>ERROR - NO PLC DETAILS");
                        $err = "Y";
}
if($err == "N")
{
    if($src == "s")
    {
        $urlback = "admin_sub.php?s=".$subid;
        include("inc_goback.php");
    }
    else
    {
        if($src=="d")
        {
            $urlback = "admin_dir.php?d=".$dirid;
            include("inc_goback.php");
        }
        else
        {
            if($dir == "ALL")
            {
                $urlback = "admin.php";
                include("inc_goback.php");
            }
            else
            {
                $urlback = "admin_dskpi.php?d=".$dir;
                include("inc_goback.php");
            }
        }
    }
}
else
{
    include("inc_goback_history.php");
}

?>
</body>

</html>
