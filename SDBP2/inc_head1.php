<link rel="stylesheet" href="lib/ui.core.css" type="text/css" />    
<link rel="stylesheet" href="lib/ui.theme.css" type="text/css" />    
<link rel="stylesheet" href="lib/ui.datepicker.css" type="text/css" />   
<link rel="stylesheet" href="lib/ui.custom.css" type="text/css" />    
<script type="text/javascript" src="lib/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="lib/jquery.validate.min.js"></script>  
<script type="text/javascript" src="lib/jquery.cookie.js"></script>
<script type="text/javascript" src="lib/ui.core.min.js"></script>
<script type="text/javascript" src="lib/ui.datepicker.min.js"></script>
