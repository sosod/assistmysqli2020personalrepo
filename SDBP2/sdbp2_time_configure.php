<?php
//    include("inc_ignite.php");
$gdate = getdate();
$today = $gdate[0];
$cmpcode = strtolower($_GET['cc']);
$sql = "SELECT * FROM assist_company WHERE cmpcode = '".strtoupper($cmpcode)."'";
include("inc_db_con_assist.php");
    $cmp = mysql_fetch_array($rs);
mysql_close($con);

    $modver = "SDBP2";
    $modtxt = "2009/2010";
    $modcurr = "Y";
    $modref = "sdp09";


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
body { font: 62.5%;}
</style>
<?php include("inc_head_msie.php"); ?>
		<script type="text/javascript">
			$(function(){

                //Closure
                $('#datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-m-yy',
                    changeMonth:true,
                    changeYear:true
                });

                //Emails
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-m-yy',
                    changeMonth:true,
                    changeYear:true
                });

			});

function closeTP(t,txt,cc) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        if(confirm("Are you sure you wish to close the time period: "+txt+"?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "sdbp2_time_configure_close.php?act=Y&cc="+cc+"&t="+t;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}

function openTP(t,txt,cc) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        if(confirm("Are you sure you wish to reopen the time period: "+txt+"?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "sdbp2_time_configure_open.php?act=Y&cc="+cc+"&t="+t;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}

		</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><?php echo($cmp['cmpname']); ?></h1>
<h2><b>SDBIP <?php echo($modtxt); ?>: Setup - Time Periods ~ Update</b></h2>
<?php
$tid = $_GET['t'];

    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$tid;
    include("inc_db_con.php");
        $trow = mysql_fetch_array($rs);
    mysql_close();
    $tval = date("d F Y", $trow['sval'])." - ".date("d F Y", $trow['eval']);
    
    //STATUS
		if($trow['active']=="N")
		{
            $status = "Closed";
		}
		else
		{
            if($trow['sval']<$today)
            {
                $status = "Open";
            }
            else
            {
                $status = "Available";
            }
		}
    //ACLOSE
    if($status == "Closed")
    {
        $aclose = "N/A";
    }
    else
    {
        $aclose = "<input type=text name=aclose id=\"datepicker\" readonly=\"readonly\" value=\"";
        if($trow['aclose']>0) { $aclose.= date("d-m-Y",$trow['aclose']); }
        $aclose.= "\">";
    }
    //AREM
    if($status == "Closed")
    {
        $arem = "N/A";
    }
    else
    {
        $arem = "<input type=text name=arem id=\"datepicker2\" readonly=\"readonly\" value=\"";
        if($trow['arem']>0) { $arem.= date("d-m-Y",$trow['arem']); }
        $arem.= "\">";
    }
    //BUTTON
    if($status != "Closed")
    {
        $abut = "<input type=submit value=\" Save Changes \">";
        if($trow['eval']<$today)
        {
            $bbut = "<input type=button value=\" Close \" onclick=\"closeTP(".$trow['id'].",'".$tval."','".$cmpcode."')\">";
        }
        else
        {
            $bbut = "";
        }
    }
    else
    {
        $abut = "";
        $bbut = "<input type=button value=\" Reopen \" onclick=\"openTP(".$trow['id'].",'".$tval."','".$cmpcode."')\">";
    }

?>
<h2><?php echo(date("d F Y",$trow['sval'])); ?> - <?php echo(date("d F Y",$trow['eval'])); ?></h2>
<form name=edit method=post action=sdbp2_time_configure_process.php>
<input type=hidden name=t value=<?php echo($tid); ?>>
<input type=hidden name=cc value=<?php echo($cmpcode); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=120>Status:</td>
		<td class=tdgeneral valign=top><?php echo($status); ?></td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>Auto Reminder:</td>
		<td class=tdgeneral valign=top><?php echo($arem); ?><input type=hidden id=actualDate2></td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>Auto Close:</td>
		<td class=tdgeneral valign=top><?php echo($aclose); ?></td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>&nbsp;</td>
		<td class=tdgeneral valign=top><?php echo($abut); ?> <?php echo($bbut); ?></td>
    </tr>
</table>
</form>
<p style="font-size: 7.5pt; margin-top: 5px;"><i> Note:<br>
<span style="font-size: 6.5pt;">+</span> To cancel an Auto Closure or Auto Reminder, set the date in the past.<br>
<span style="font-size: 6.5pt;">+</span> Time periods may only be closed once the end date has passed.<br>
<span style="font-size: 6.5pt;">+</span> All automatic closures and reminder emails occurr at 01:00am on the date selected.</i></p>
<?php
$urlback = "sdbp2_time_list.php?cc=".$cmpcode;
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
