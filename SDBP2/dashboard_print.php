<?php
// Require necessary files
require("lib/AmPieChart.php");

// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js and AmCharts.js (only needed for pie legend)
AmChart::$jsPath = "lib/AmCharts.js";
AmChart::$jQueryPath = "lib/jquery.js";
// Tell AmChart to load jQuery if you don't already use it on your site.
AmChart::$loadJQuery = true;

// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmPieChart("kpi_".$who);

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
$chart->setTitle("<span style=\"font-weight: bold;\">".$titletxt."</span>");

// Add slices
$chart->addSlice("met", "KPI&#39;s met (".$kpi['g']." of ".$kpi['tot'].")", $kpi['g'], array("color" => "#009900"));
$chart->addSlice("almost", "KPI&#39;s almost met (".$kpi['o']." of ".$kpi['tot'].")", $kpi['o'], array("color" => "#fe9900"));
$chart->addSlice("notmet", "KPI&#39;s not met (".$kpi['r']." of ".$kpi['tot'].")", $kpi['r'], array("color" => "#cc0001"));
$chart->addSlice("notmeasure", "KPI&#39;s not yet measured (".$kpi['x']." of ".$kpi['tot'].")", $kpi['x'], array("color" => "#999999", "pull_out" => "true"));

//Settings
if($anima=="Y") {
    $chart->setConfigAll(array(
    "width" => 410,
    "height" => 350,
    "pie.y" => "40%",
    "pie.radius" => 100,
    "pie.height" => 5,
    "animation.start_time" => 2,
    "animation.start_effect" => "regular",
    "animation.pull_out_time" => 1,
    "animation.pull_out_effect" => "regular",
    "animation.pull_out_radius" => "10%",
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
//    "background.border_alpha" => 15,
    "legend.y" => "83%",
    "legend.x" => "2%",
    "legend.width" => "96%",
    "legend.border_alpha" => 20,
    "legend.text_size" => 9,
    "legend.spacing" => 4,
    "legend.margins" => 8,
    "legend.align" => "center",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));
}
else
{
    $chart->setConfigAll(array(
    "width" => 410,
    "height" => 350,
    "pie.y" => "40%",
    "pie.radius" => 100,
    "pie.height" => 5,
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
//    "background.border_alpha" => 15,
    "legend.y" => "83%",
    "legend.x" => "2%",
    "legend.width" => "96%",
    "legend.border_alpha" => 20,
    "legend.text_size" => 9,
    "legend.spacing" => 4,
    "legend.margins" => 8,
    "legend.align" => "center",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));
}
// Print the code
//echo html_entity_decode($chart->getCode());











$txt = "<P>Hello World</p><p>".html_entity_decode($chart->getCode())."</p><p>Goodbye world</p>";







$tmpfile = tempnam("/tmp", "dompdf_");
file_put_contents($tmpfile, html_entity_decode($chart->getCode())); // Replace $smarty->fetch()
                                                // with your HTML string

$url = "SDBP2/lib/dompdf/dompdf.php?input_file=" . rawurlencode($tmpfile) .
       "&paper=letter&base_path=http://ignite-jeb/SDBP2/&output_file=" . rawurlencode("My Fancy PDF.pdf");

header("Location: http://" . $_SERVER["HTTP_HOST"] . "/".$url);
?>


