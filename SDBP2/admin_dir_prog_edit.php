<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var valid8 = "true";
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
        alert("Please complete the missing field as indicated.");
    }
    else
    {
        document.getElementById('val').className = 'reqmet';
    }
    if(valid8 == "false")
    {
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

function delVal(k) {
    if(!isNaN(parseInt(k)) && escape(k) == k)
    {
        if(confirm("Are you sure you wish to delete this objective?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
             document.location.href = "admin_dir_prog_del.php?p="+k;
        }
    }
    else
    {
        alert("An error has occurred.\nPlease reload the page and try again.");
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><?php
$progid = $_GET['p'];
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE id = ".$progid;
include("inc_db_con.php");
    $progrow = mysql_fetch_array($rs);
mysql_close();



$dirid = $progrow['dirid'];
$err = "Y";
$dir = array();
if(strlen($dirid)>0 && is_numeric($dirid))
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' AND dirid = ".$dirid;
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $dir = mysql_fetch_array($rs);
            $err = "N";
        }
    mysql_close();
}


if($err == "Y" || count($progrow)==0)
{
    ?>
    <h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin</b></h1>
    <p>&nbsp;</p>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
else
{


?>

<a name=top></a><h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin - <?php echo($dir['dirtxt']); ?> ~ Objective / Programme</b></h1>
<p>&nbsp;</p>
<?php
            $sql2 = "SELECT count(kpiid) AS kcount FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiprogid = ".$progid." AND kpiyn = 'Y'";
            include("inc_db_con2.php");
                $row2 = mysql_fetch_array($rs2);
            mysql_close($con2);
            $kcount = $row2['kcount'];


?>
<form name=edit method=post action=admin_dir_prog_edit_process.php onsubmit="return Validate(this);">
<input type=hidden name=p value=<?php echo($progid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=110 height=27>Ref:</td>
		<td class=tdgeneral valign=top><?php echo($progid); ?></td>
	</tr>
    <tr>
		<td class=tdheaderl width=110 height=27>Directorate:</td>
		<td class=tdgeneral valign=top><?php echo($dir['dirtxt']); ?></td>
	</tr>
    <tr>
		<td class=tdheaderl width=110>Objective:</td>
		<td class=tdgeneral valign=top><input type=text size=50 maxlength=100 name=val id=val value="<?php echo($progrow['value']); ?>"> <span style="font-size: 7.5pt;">(max 100 characters)</span></td>
	</tr>
    <tr>
		<td class=tdheaderl width=110 height=27>KPIs:*</td>
		<td class=tdgeneral valign=top><?php echo($kcount); ?>&nbsp;</td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral valign=top><input type=submit value=" Save Changes "> <?php if($kcount==0) { ?><input type=button value=" Delete " onclick="delVal(<?php echo($progid); ?>)"><?php } ?></td>
    </tr>
</table>
</form>
<p>* Only Objectives with no associated KPIs may be deleted.</p>
<?php

}   //err

include("inc_goback_history.php");
?>
<p>&nbsp;</p>
</body>
</html>
