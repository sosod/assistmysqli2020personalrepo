<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Delete Sub-Directorate</b></h1>
<p>&nbsp;</p>
<?php
$subid = $_GET['s'];
$dirid = $_GET['d'];
$src = $_GET['r'];

if(is_numeric($subid) && $subid > 0)
{
    $sub = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$subid;
    include("inc_db_con.php");
        $sub = mysql_fetch_array($rs);
    mysql_close();
    if(count($sub)>0)
    {
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_dirsub SET subyn = 'N' WHERE subid = ".$subid;
        include("inc_db_con.php");
        //display result
$tsql = $sql;
        $subtxt = $sub['subtxt'];
        $logref = $subid;
        $logtype = "sub";
        $logaction = "Deleted sub-directorate '".$subtxt."'";
        $logdisplay = "Y";
        include("inc_log.php");
$told = "";
$trans = $logaction." with id ".$subid;
include("inc_transaction_log.php");
        ?>
        <p>Sub-directorate '<?php echo($subtxt); ?>' has been successfully deleted.</p>
        <p>&nbsp;</p>
        <?php
    }
    else
    {
        //display result
        ?>
        <p>An error has occurred (Err: SDBIP11).  Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
    }
}
else
{
    //display result
    ?>
    <p>An error has occurred (Err: SDBIP12).  Please go back and try again.</p>
    <p>&nbsp;</p>
    <?php
}

if($dirid>0)
{
    switch($src) {
        case "de":
            $urlback = "setup_dir_edit.php?d=".$dirid;
            break;
        case "se":
            $urlback = "setup_dir_sub.php?d=".$dirid;
            break;
        default:
            $urlback = "setup_dir.php";
            break;
    }
}
else
{
    $urlback = "setup_dir.php";
}
include("inc_goback.php");
?>
</body>
</html>
