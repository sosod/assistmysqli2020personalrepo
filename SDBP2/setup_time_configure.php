<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
body { font: 62.5%;}
</style>
<?php include("inc_head_msie.php"); ?>
		<script type="text/javascript">
			$(function(){

                //Closure
                $('#datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-m-yy',
                    changeMonth:true,
                    changeYear:true
                });

                //Emails
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-m-yy',
                    changeMonth:true,
                    changeYear:true
                });

			});

function closeTP(t,txt) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        if(confirm("Are you sure you wish to close the time period: "+txt+"?\nTo reopen the time period, you will need to contact Ignite Advisory Services.\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "setup_time_configure_close.php?act=C&t="+t;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}

		</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Time Periods ~ Update</b></h1>
<?php
$tid = $_GET['t'];

    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$tid;
    include("inc_db_con.php");
        $trow = mysql_fetch_array($rs);
    mysql_close();
    $tval = date("d F Y", $trow['sval'])." - ".date("d F Y", $trow['eval']);

?>
<h2><?php echo(date("d F Y",$trow['sval'])); ?> - <?php echo(date("d F Y",$trow['eval'])); ?></h2>
<p>Please note: Auto reminder and closure functionality is no longer available for this version of SDBIP.</p>
<form name=edit method=post action=setup_time_configure_process.php>
<input type=hidden name=t value=<?php echo($tid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=120>Auto Reminder:</td>
		<td class=tdgeneral valign=top><!-- <input type=text name=arem id="datepicker2" readonly="readonly" value="<?php if($trow['arem']>0) { echo(date("d-m-Y",$trow['arem'])); } ?>"><input type=hidden id=actualDate2> --> N/A</td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>Auto Close:</td>
		<td class=tdgeneral valign=top><!-- <input type=text name=aclose id="datepicker" readonly="readonly" value="<?php if($trow['aclose']>0) { echo(date("d-m-Y",$trow['aclose'])); } ?>"> --> N/A</td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>&nbsp;</td>
		<td class=tdgeneral valign=top><input type=submit value=" Save Changes " disabled=disabled> <?php if($trow['eval']<$today) { ?><input type=button value=" Close " onclick="closeTP(<?php echo($trow['id']); ?>,'<?php echo($tval); ?>')"><?php } ?></td>
    </tr>
</table>
</form>
<p style="font-size: 7.5pt; margin-top: 5px;"><i> Note:<br>
<span style="font-size: 6.5pt;">+</span> To cancel an Auto Closure or Auto Reminder, set the date in the past.<br>
<span style="font-size: 6.5pt;">+</span> Time periods may only be closed once the end date has passed.<br>
<span style="font-size: 6.5pt;">+</span> Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.<br>
<span style="font-size: 6.5pt;">+</span> All automatic closures and reminder emails occurr at 01:00 on the date selected.</i></p>
<?php
$urlback = "setup_time_list.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
