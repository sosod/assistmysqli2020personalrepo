<?php
    include("inc_ignite.php");
$field = array();
$filter = array();
$filtertype = array();

$field['dir'] = $_POST['dir'];
$field['sub'] = $_POST['sub'];
$field['cashflow'] = $_POST['cashflow'];
$cf = $_POST['cf']; //budget from
$ct = $_POST['ct']; //budget to
$filter['dir'] = $_POST['dirfilter'];
$filter['sub'] = $_POST['subfilter'];
$sort = $_POST['sort'];
$output = $_POST['output'];

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CP' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $head[$row['headfield']] = $row;
        $field[$row['headfield']] = $_POST[$row['headfield']];
        $filter[$row['headfield']] = $_POST[$row['headfield'].'filter'];
        $filtertype[$row['headfield']] = $_POST[$row['headfield'].'filtertype'];
    }
mysql_close();

if(strlen($filter['cpstartdate'])>0)
{
    $cdate = explode("_",$filter['cpstartdate']);
    $filter['cpstartdate'] = mktime(12,0,0,$cdate[1],$cdate[0],$cdate[2]);
}
if(strlen($filter['cpenddate'])>0)
{
    $cdate = explode("_",$filter['cpenddate']);
    $filter['cpenddate'] = mktime(12,0,0,$cdate[1],$cdate[0],$cdate[2]);
}
/*
echo("<P>head-");
print_r($head);

echo("<P>field-");
print_r($field);

echo("<P>filter-");
print_r($filter);

echo("<P>filtertype-");
print_r($filtertype);

echo("<P>wards-");
print_r($field['wards']);
*/
$repsql = "SELECT p.cpid";
$repsql.= ", p.cpnum";
$repsql.= ", p.cpvotenum";
$repsql.= ", p.cpidpnum";
$repsql.= ", p.cpfundsource";
$repsql.= ", p.cpstartdate";
$repsql.= ", p.cpenddate";
$repsql.= ", p.cpproject";
$repsql.= ", d.dirtxt";
$repsql.= ", s.subtxt";
$repsql.= " FROM";
$repsql.= " assist_".$cmpcode."_".$modref."_capital p";
$repsql.= ", assist_".$cmpcode."_".$modref."_dirsub s";
$repsql.= ", assist_".$cmpcode."_".$modref."_dir d";
$repsql.= " WHERE";
$repsql.= " p.cpsubid = s.subid";
$repsql.= " AND s.subdirid = d.dirid";
$repsql.= " AND p.cpyn = 'Y'";
$repsql.= " AND d.diryn = 'Y'";
$repsql.= " AND s.subyn = 'Y'";

if($filter['dir'] != "ALL" && is_numeric($filter['dir']) && strlen($filter['dir'])>0)
{
    $repsql.= " AND d.dirid = ".$filter['dir'];
}
$subfilt = $filter['sub'];

if(count($subfilt)>1)
{
    $repsql.= " AND (";
    $s = 0;
    foreach($subfilt as $sf)
    {
        if($s > 0)
        {
            $repsql.= " OR ";
        }
        if($sf != "ALL" && is_numeric($sf) && strlen($sf)>0)
        {
            $repsql.= "s.subid = ".$sf;
            $s++;
        }
    }
    $repsql.= ") ";
}
else
{
    $sf = $subfilt[0];
        if($sf != "ALL" && is_numeric($sf) && strlen($sf)>0)
        {
            $repsql.= " AND s.subid = ".$sf;
        }
}

foreach($head as $hrow)
{
    $hf = $hrow['headfield'];
    $filt = $filter[$hf];
    $filttype = $filtertype[$hf];
    if(strlen($filt) > 0)
    {
        if($hf == "cpenddate" || $hf == "cpstartdate")
        {
            switch($filttype)
            {
                case "after":
                    $repsql.= " AND ".$hf." >= ".$filt;
                    break;
                case "before":
                    $repsql.= " AND ".$hf." <= ".$filt;
                    break;
                case "on":
                    $repsql.= " AND ".$hf." = ".$filt;
                    break;
            }
        }
        else
        {
            if($hf == "wards")
            {
                if(count($filt) > 1 || (count($filt)==1 && $filt[0]!="X"))
                {
                    $repsql.= " AND p.cpid IN (";
                    $repsql.= "SELECT DISTINCT cwcpid FROM assist_".$cmpcode."_".$modref."_capital_wards, assist_".$cmpcode."_".$modref."_list_wards WHERE cwwardsid  = id AND yn = 'Y' AND cwyn = 'Y' AND (";
                    $fc = 0;
                    foreach($filt as $f)
                    {
                        if($f != "X" && is_numeric($f) && strlen($f)>0)
                        {
                            if($fc>0)
                            {
                                if($filttype=="all")
                                {
                                    $repsql.=" AND ";
                                }
                                else
                                {
                                    $repsql.=" OR ";
                                }
                            }
                            $repsql.= " id = ".$f;
                            $fc++;
                        }
                    }
                    $repsql.= "))";
                }
            }
            else
            {
                if($hf != "progress")
                {
                    switch($filttype)
                    {
                        case "all":
                            $filarr = explode(" ",$filt);
                            foreach($filarr as $f)
                            {
                                $repsql.= " AND ".$hf." LIKE '%".$f."%'";
                            }
                            break;
                        case "any":
                            $filarr = explode(" ",$filt);
                            $repsql.= " AND (";
                            $fc = 0;
                            foreach($filarr as $f)
                            {
                                if($fc>0)
                                {
                                    $repsql.= " OR ";
                                }
                                $repsql.= $hf." LIKE '%".$f."%'";
                                $fc++;
                            }
                            $repsql.= ")";
                            break;
                        case "exact":
                            $repsql.= " AND ".$hf." LIKE '%".$filt."%'";
                            break;
                        default:
                            $repsql.= " AND ".$hf." LIKE '%".$filt."%'";
                            break;
                    }
                }
            }
        }
    }
}

switch($sort)
{
    case "dir":
        $repsql.= " ORDER BY d.dirsort, s.subsort, p.cpid";
        break;
    default:
        if(strlen($sort)>0)
        {
            $repsql.= " ORDER BY p.".$sort.", d.dirsort, s.subsort, p.cpid";
        }
        else
        {
            $repsql.= " ORDER BY d.dirsort, s.subsort, p.cpid";
        }
        break;
}


switch($output)
{
    case "csv":
        if($field['cashflow']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
            $fdata = "\"Ref\"";
            if($field['dir']=="Y") { $fdata.=",\"Directorate\""; }
            if($field['sub']=="Y") { $fdata.=",\"Sub-Directorate\""; }
            foreach($head as $hrow)
            {
                if($field[$hrow['headfield']] == "Y") { $fdata.=",\"".$hrow['headlist']."\""; }
                //echo("<td class=tdheader rowspan=".$rowspan.">".$hrow['headlist'].$field[$hrow['headfield']]."</td>");
            }
            if($field['cashflow'] == "Y")
            {
                $timearr = array();
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sort >= ".$cf." AND sort <= ".$ct." ORDER BY sort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $timearr[$row['sort']] = $row;
                        $fdata.=",\"".date("F-Y",$row['eval'])."\",\"\",\"\",\"\"";
                    }
                mysql_close();
                $fdata.= ",\"Total\",\"\"";
                $fdata.= "\r\n";
                $fdata.= "\"\"";
                if($field['dir']=="Y") { $fdata.=",\"\""; }
                if($field['sub']=="Y") { $fdata.=",\"\""; }
                foreach($head as $hrow)
                {
                    if($field[$hrow['headfield']] == "Y") { $fdata.=",\"\""; }
                }
                foreach($timearr as $tim)
                {
                    $fdata.=",\"Budget\",\"Actual\",\"Acc % - YTD\",\"Tot. R - YTD\"";
                }
            }
        $fdata .= "\r\n";
        $sql = $repsql;
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $results = array();
                $totb = 0;
                $tota = 0;
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_results WHERE crcpid = ".$row['cpid'];
                include("inc_db_con2.php");
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $results[$row2['crtimeid']] = $row2;
                    }
                mysql_close($rs2);
                $fdata.="\"".$row['cpid']."\"";
                if($field['dir']=="Y") { $fdata.=",\"".html_entity_decode($row['dirtxt'], ENT_QUOTES, "ISO-8859-1")."\""; }
                if($field['sub']=="Y") { $fdata.=",\"".html_entity_decode($row['subtxt'], ENT_QUOTES, "ISO-8859-1")."\""; }
                foreach($head as $hrow)
                {
                    if($field[$hrow['headfield']] == "Y")
                    {
                        switch($hrow['headfield'])
                        {
                            case "cpenddate":
                                $fdata.=",\"".date("d-M-Y",$row[$hrow['headfield']])."\"";
                                break;
                            case "cpstartdate":
                                $fdata.=",\"".date("d-M-Y",$row[$hrow['headfield']])."\"";
                                break;
                            case "wards":
                                $fdata.=",\"";
                                $sql2 = "SELECT w.* FROM assist_".$cmpcode."_".$modref."_capital_wards cw, assist_".$cmpcode."_".$modref."_list_wards w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = cw.cwwardsid AND cw.cwyn = 'Y' AND cw.cwcpid = ".$row['cpid'];
                                $sql2.= " ORDER BY w.numval, w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $fdata.=$row2['value']."; ";
                                    }
                                mysql_close($con2);
                                $fdata.="\"";
                                break;
                            case "progress":
                                $row2 = array();
                                $fdata.=",\"";
                                $sql2 = "SELECT krprogress FROM assist_".$cmpcode."_".$modref."_kpi, assist_".$cmpcode."_".$modref."_kpi_result";
                                $sql2.= " WHERE kpiyn = 'Y' AND kpiid = krkpiid AND krprogress <> '' AND kpicpid = ".$row['cpid'];
                                $sql2.= " ORDER BY krtimeid DESC";
                                include("inc_db_con2.php");
                                    $row2 = mysql_fetch_array($rs2);
                                    $fdata.= html_entity_decode($row2['krprogress'], ENT_QUOTES, "ISO-8859-1");
                                mysql_close($con2);
                                $fdata.="\"";
                                break;
                            default:
                                $fdata.=",\"".html_entity_decode($row[$hrow['headfield']], ENT_QUOTES, "ISO-8859-1")."\"";
                                break;
                        }
                    }
                }
                if($field['cashflow'] == "Y")
                {
                    $accytd = 0;
                    $totytd = 0;
                    foreach($timearr as $tim)
                    {
                        $totb = $totb + $results[$tim['id']]['crbudget'];
                        $tota = $tota + $results[$tim['id']]['cractual'];
                        if($results[$tim['id']]['craccytd']>0) { $accytd = $results[$tim['id']]['craccytd']; }
                        if($results[$tim['id']]['crtotytd']>0) { $totytd = $results[$tim['id']]['crtotytd']; }
                        $fdata.=",\"".number_format($results[$tim['id']]['crbudget'],2)."\"";
                        $fdata.=",\"".number_format($results[$tim['id']]['cractual'],2)."\"";
                        $fdata.=",\"".number_format($accytd,2)."%\"";
                        $fdata.=",\"".number_format($totytd,2)."\"";
                    }
                }
                $fdata.=",\"".number_format($totb,2)."\"";
                $fdata.=",\"".number_format($tota,2)."\"";
                $fdata.= "\r\n";
            }
        mysql_close();
        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/sdbip_cp_report_".date("Ymd_Hi",$today).".csv";
        $newfilename = "sdbip_cp_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$fdata."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);
        break;
    case "pdf":
        echo("<P>PDF");
        break;
    default:
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border: 1px solid #ababab;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}
$cspan=1;
?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - Capital Projects</b></h1>
<?php
        if($field['cashflow']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
        ?>
        <table cellpadding=3 cellspacing=0>
            <tr>
                <td class=tdheader rowspan=<?php echo($rowspan); ?>>Ref</td>
            <?php
            if($field['dir']=="Y") { echo("<td class=tdheader rowspan=".$rowspan.">Directorate</td>"); $cspan++; }
            if($field['sub']=="Y") { echo("<td class=tdheader rowspan=".$rowspan.">Sub-Directorate</td>"); $cspan++; }
            foreach($head as $hrow)
            {
                if($field[$hrow['headfield']] == "Y") { echo("<td class=tdheader rowspan=".$rowspan.">".$hrow['headlist']."</td>"); $cspan++; }
                //echo("<td class=tdheader rowspan=".$rowspan.">".$hrow['headlist'].$field[$hrow['headfield']]."</td>");
            }
            if($field['cashflow'] == "Y")
            {
                $timearr = array();
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sort >= ".$cf." AND sort <= ".$ct." ORDER BY sort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $timearr[$row['sort']] = $row;
                        echo("<td class=tdheader colspan=4>".date("F-Y",$row['eval'])."</td>");
                        $cspan = $cspan+4;
                    }
                mysql_close();
                echo("<td class=tdheader colspan=2>Total</td>");
                $cspan = $cspan+2;
            }

//                echo("<td class=tdheader rowspan=".$rowspan.">&nbsp;</td>");
            echo("</tr>");

            if($field['cashflow'] == "Y")
            {
                echo("<tr>");
                for($t=1;$t<=count($timearr);$t++)
                {
                    echo("<td class=tdheader>Budget</td><td class=tdheader>Actual</td><td class=tdheader>Acc % - YTD</td><td class=tdheader>Tot. R - YTD</td>");
                }
                echo("<td class=tdheader>Budget</td><td class=tdheader>Actual</td>");
                echo("</tr>");
            }

        $sql = $repsql;
        include("inc_db_con.php");
        if(mysql_num_rows($rs)==0)
        {
            echo("<tr><td class=tdgeneral colspan=".$cspan.">No Capital Projects meet your criteria.</td></tr>");
        }
        else
        {
            while($row = mysql_fetch_array($rs))
            {
                $results = array();
                $totb = 0;
                $tota = 0;
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_results WHERE crcpid = ".$row['cpid'];
                include("inc_db_con2.php");
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $results[$row2['crtimeid']] = $row2;
                    }
                mysql_close($rs2);
                echo("<tr>");
                echo("<td class=tdgeneral align=center>".$row['cpid']."</td>");
                if($field['dir']=="Y") { echo("<td class=tdgeneral>".$row['dirtxt']."</td>"); }
                if($field['sub']=="Y") { echo("<td class=tdgeneral>".$row['subtxt']."</td>"); }
                foreach($head as $hrow)
                {
                    if($field[$hrow['headfield']] == "Y")
                    {
                        switch($hrow['headfield'])
                        {
                            case "cpenddate":
                                echo("<td class=tdgeneral>".date("d M Y",$row[$hrow['headfield']])."</td>");
                                break;
                            case "cpstartdate":
                                echo("<td class=tdgeneral>".date("d M Y",$row[$hrow['headfield']])."</td>");
                                break;
                            case "wards":
                                echo("<td class=tdgeneral>");
                                $sql2 = "SELECT w.* FROM assist_".$cmpcode."_".$modref."_capital_wards cw, assist_".$cmpcode."_".$modref."_list_wards w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = cw.cwwardsid AND cw.cwyn = 'Y' AND cw.cwcpid = ".$row['cpid'];
                                $sql2.= " ORDER BY w.numval, w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        echo($row2['value']."; ");
                                    }
                                mysql_close($con2);
                                echo("&nbsp;</td>");
                                break;
                            case "progress":
                                $row2 = array();
                                echo("<td class=tdgeneral>");
                                $sql2 = "SELECT krprogress FROM assist_".$cmpcode."_".$modref."_kpi, assist_".$cmpcode."_".$modref."_kpi_result";
                                $sql2.= " WHERE kpiyn = 'Y' AND kpiid = krkpiid AND krprogress <> '' AND kpicpid = ".$row['cpid'];
                                $sql2.= " ORDER BY krtimeid DESC";
                                include("inc_db_con2.php");
                                    $row2 = mysql_fetch_array($rs2);
                                    echo($row2['krprogress']);
                                mysql_close($con2);
                                echo("&nbsp;</td>");
                                break;
                            default:
                                echo("<td class=tdgeneral>".$row[$hrow['headfield']]."</td>");
                                break;
                        }
                    }
                }
                if($field['cashflow'] == "Y")
                {
                    $accytd = 0;
                    $totytd = 0;
                    foreach($timearr as $tim)
                    {
                        $totb = $totb + $results[$tim['id']]['crbudget'];
                        $tota = $tota + $results[$tim['id']]['cractual'];
                        if($results[$tim['id']]['craccytd']>0) { $accytd = $results[$tim['id']]['craccytd']; }
                        if($results[$tim['id']]['crtotytd']>0) { $totytd = $results[$tim['id']]['crtotytd']; }
                        echo("<td class=tdgeneral align=right>".number_format($results[$tim['id']]['crbudget'],2)."</td>");
                        echo("<td class=tdgeneral align=right>".number_format($results[$tim['id']]['cractual'],2)."</td>");
                        echo("<td class=tdgeneral align=right>".number_format($accytd,2)."%</td>");
                        echo("<td class=tdgeneral align=right>".number_format($totytd,2)."</td>");
                    }
                }
                echo("<td class=tdgeneral align=right><b>".number_format($totb,2)."</b></td>");
                echo("<td class=tdgeneral align=right><b>".number_format($tota,2)."</b></td>");
//                echo("<td class=tdgeneral align=center><input type=button value=View></td>");
                echo("</tr>");
            }
        }
        mysql_close();
        ?>
        </table>
<p>&nbsp;</p>

</body>

</html>
        <?php
        break;
}
?>
