<?php
    include("inc_ignite.php");
    $r = 1;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css" />
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ababab;
    border-width: 1px;
    border-style: solid;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('#datepicker1').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

                //End
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

			});
		</script>
<script type=text/javascript>
function Validate(me) {
    var project = me.cpproject.value;
    var startdate = me.cpstartdate.value;
    var enddate = me.cpenddate.value;
    var subid = me.subid.value;
    var valid8 = "true";
    
    if(project.length == 0)
    {
        document.getElementById('cpproject').className = "reqtext";
        document.getElementById('cpproject').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('cpproject').className = "blank";
    }
    if(subid.length == 0 || subid == "X")
    {
        document.getElementById('subid').className = "reqtext";
        valid8 = "false";
    }
    else
    {
        document.getElementById('subid').className = "blank";
    }
    
    var dterr = "N";
    if(startdate.length == 0)
    {
        document.getElementById('datepicker1').className = "reqtext";
        valid8 = "false";
        dterr = "Y";
    }
    else
    {
        document.getElementById('datepicker1').className = "blank";
    }
    if(enddate.length == 0)
    {
        document.getElementById('datepicker2').className = "reqtext";
        valid8 = "false";
        dterr = "Y";
    }
    else
    {
        document.getElementById('datepicker2').className = "blank";
    }
    if(dterr == "N")
    {
        var sdatearr = startdate.split("_");
        var edatearr = enddate.split("_");
        var sdate = new Date();
        sdate.setFullYear(sdatearr[2],sdatearr[1],sdatearr[0]);
        var edate = new Date();
        edate.setFullYear(edatearr[2],edatearr[1],edatearr[0]);
        if(edate<sdate)
        {
            dterr = "D";
            document.getElementById('datepicker1').className = "reqtext";
            document.getElementById('datepicker2').className = "reqtext";
        }
    }

    var t = 1;
    var target;
    var berr = "N";
    for(t=1;t<13;t++)
    {
        target = document.getElementById('t'+t);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
        }
    }

    if(valid8 == "true")
    {
        return true;
    }
    else
    {
        var errtxt = "The following errors have occurred:\n - Please complete all required fields as indicated.";
        if(dterr == "D")
        {
            errtxt = errtxt + "\n - Your end date can not fall before your start date.";
        }
        if(berr == "Y")
        {
            errtxt = errtxt + "\n - Only numbers are allowed for Budget figures.";
        }
        alert(errtxt);
        return false;
    }
    
    return false;
}
function wardsAll(me) {
        var elements = document.getElementById('addcp');
        var f = parseInt(document.getElementById('f').value);
        var f2 = parseInt(document.getElementById('f2').value);
        var z = 0;
        f++;
    if(me.checked)
    {
        for(z=f;z<f2;z++)
        {
            elements[z].checked = false;
            elements[z].disabled = true;
        }
    }
    else
    {
        for(z=f;z<f2;z++)
        {
            elements[z].disabled = false;
        }
    }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin - Capital Projects ~ Add</b></h1>
<p>All fields marked with a * are required.</p>
<form id=addcp name=addcp method=POST action=admin_fin_cp_add_process.php onsubmit="return Validate(this);" language=jscript>
<h2 class=fc>Capital Project Details</h2>
<table cellpadding=3 cellspacing=0 width=570>
<tr>
    <td class=tdheaderl width=120>Sub-Directorate:*</td>
    <td class=tdgeneral><select name=subid id=subid>
        <option selected value=X>--- SELECT ---</option>
    		<?php
            $sql = "SELECT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND s.subdirid = d.dirid ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['subid'];
                    $val = $row['subtxt'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            mysql_close();
        ?>
    </select></td>
</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CP' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    $f = 1;
    while($row = mysql_fetch_array($rs))
    {
        $f++;
        switch($row['headfield'])
        {
            case "cpstartdate":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker1" readonly="readonly"> <input type=hidden size=10 name=cpstartdate id=startDate></td>
                </tr>
                <?php
                break;
            case "cpenddate":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker2" readonly="readonly"> <input type=hidden size=10 name=cpenddate id=endDate></td>
                </tr>
                <?php
                break;
            case "cpproject":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><?php echo("<input type=text size=50 maxlength=".$row['headmaxlen']." name=".$row['headfield']." id=".$row['headfield'].">  <span style=\"font-size: 7.5pt;\">(max ".$row['headmaxlen']." characters)</span>"); ?></td>
                </tr>
                <?php
                break;
            case "progress":
                break;
            case "wards":
                ?>
                <tr>
                    <td class=tdheaderl width=120 valign=top><?php echo($row['headdetail']); ?>:<input type=hidden id=f size=2 value=<?php echo($f); ?>></td>
                    <td class=tdgeneral>
                    <?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $c = 0;
                            $c2 = 0;
                                ?>
                                <table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
                                    <tr>
                                        <td class=tdgeneral style="border: 0px solid #ffffff;"><input type=checkbox name=wards[] value=1 onclick="wardsAll(this);"> All</td>
                                        <?php
                                            $c++;
                                            $c2++;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                echo("<td class=tdgeneral style=\"border: 0px solid #ffffff;\"><input type=checkbox name=wards[] value=".$row2['id']." >".$row2['value']."</td>");
                                                $c++;
                                                if($c==5)
                                                {
                                                    $c = 0;
                                                    echo("</tr><tr>");
                                                }
                                                $c2++;
                                            }
                                        ?>
                                    </tr>
                                </table>
                                <?php
                        mysql_close($con2);
                    ?>
                    </td>
                </tr>
                <?php
                $f2 = $f+$c2;
                break;
            default:
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
                    <td class=tdgeneral><input type=text size=<?php echo($row['headmaxlen']); ?> maxlength=<?php echo($row['headmaxlen']); ?> name=<?php echo($row['headfield']); ?>>  <span style="font-size: 7.5pt;">(max <?php echo($row['headmaxlen']); ?> characters)</span></td>
                </tr>
                <?php
                break;
        }
    }
mysql_close();
?>
</table>
<h2 class=fc>Capital Project Budget</h2>
<table cellpadding=3 cellspacing=0 width=570>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        while($row = mysql_fetch_array($rs))
        {
            $t++;
    ?>
    <tr>
        <td class=tdheaderl width=120><?php echo(date("M Y",$row['eval'])); ?>:</td>
        <td class=tdgeneral>&nbsp;R <input type=text value=0 style="text-align:right;" size=15 name=tid<?php echo($row['id']); ?> id=t<?php echo($t); ?>> <span style="font-size: 7.5pt;">(numbers only)</span></td>
    </tr>
    <?php
        }
    mysql_close();
    ?>
</table>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdgeneral align=center><input type=hidden id=f2 value=<?php echo($f2); ?>><input type=submit value=" Add "> <input type=reset ></td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_fin.php";
include("inc_goback.php");
?>
</body>
</html>
