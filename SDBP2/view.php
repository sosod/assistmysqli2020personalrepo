<?php
    include("inc_ignite.php");
    $r = 1;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdgeneral {
    border-left: 0px solid #ffffff;
//    padding-left: 10px;
}
.tdheaderl {
    border-right: 0px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    border-top: 0px solid #ffffff;
}
</style>
<base target="main">
<script type=text/javascript>
function chgWhat(me) {
    switch(me)
    {
        case "cf":
            document.getElementById('g').disabled = false;
            document.getElementById('gl').disabled = false;
            var dir = document.getElementById('dir');
            dir.disabled = false;
            dir.length = 0;
            var o = 0;
            var ds = true;
            var di = 0;
            var opt = new Array();
            for(d in dirarr)
            {
                dir.options[o] = new Option(dirarr[d][1],"d_"+dirarr[d][0],ds,ds);
                o++;
                ds = false;
                di = dirarr[d][0];
                opt = cf[di];
                for(k in opt)
                {
                    dir.options[o] = new Option("   - "+opt[k][1],"s_"+opt[k][0],ds,ds);
                    o++;
                }
            }
            dir.options[o] = new Option("Entire Municipality","ALL",false,false);
            break;
        case "cp":
            document.getElementById('g').disabled = true;
            document.getElementById('gl').disabled = true;
            var dir = document.getElementById('dir');
            dir.disabled = false;
            dir.length = 0;
            var o = 0;
            var di = 0;
            var ds = true;
            var opt = new Array();
            for(d in dirarr)
            {
                dir.options[o] = new Option(dirarr[d][1],"d_"+dirarr[d][0],ds,ds);
                o++;
                ds = false;
                di = dirarr[d][0];
                opt = cp[di];
                for(k in opt)
                {
                    dir.options[o] = new Option("   - "+opt[k][1],"s_"+opt[k][0],ds,ds);
                    o++;
                }
            }
            dir.options[o] = new Option("Entire Municipality","ALL",false,false);
            break;
        case "kpi":
            document.getElementById('g').disabled = false;
            document.getElementById('gl').disabled = false;
            var dir = document.getElementById('dir');
            dir.disabled = false;
            dir.length = 0;
            var o = 0;
            var di = 0;
            var ds = true;
            var opt = new Array();
            for(d in dirarr)
            {
                dir.options[o] = new Option(dirarr[d][1],"d_"+dirarr[d][0],ds,ds);
                o++;
                ds = false;
                di = dirarr[d][0];
                opt = kpi[di];
                for(k in opt)
                {
                    dir.options[o] = new Option("   - "+opt[k][1],"s_"+opt[k][0],ds,ds);
                    o++;
                }
            }
            break;
        case "rs":
            document.getElementById('g').disabled = true;
            document.getElementById('gl').disabled = true;
            document.getElementById('dir').disabled = true;
            break;
        default:
            break;
    }
}

</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: View</b></h1>
<form name=view action=view_process.php method=POST>
<table cellpadding=5 cellspacing=0 width=500>
    <tr id=what>
        <td class=tdheaderl valign=top width=120>What:</td>
        <td class=tdgeneral>
            <input type=radio checked name=w1 value=KPI id=kpi onclick="chgWhat('kpi');"> <label for=kpi>KPIs</label><br>
            <input type=radio name=w1 value=CP id=cp onclick="chgWhat('cp');"> <label for=cp>Capital Projects</label><br>
            <input type=radio name=w1 value=CF id=cf onclick="chgWhat('cf');"> <label for=cf>Monthly Cashflows</label><br>
            <input type=radio name=w1 value=RS id=rs onclick="chgWhat('rs');"> <label for=rs>Revenue By Source</label><br>
        </td>
    </tr>
        <?php
            $js = "var dirarr = new Array();".chr(10);
            $dird = array();
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $sort = $row['dirsort'];
                    $id = $row['dirid'];
                    $dird[] = $id;
                    $val = $row['dirtxt'];
                    $js.= "dirarr[".$sort."] = new Array(".$id.",\"".html_entity_decode($val, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
//                    $js.= "dirarr[".$sort."][1] = ;".chr(10);
                }
            mysql_close();
            $js.= "var kpi = new Array();".chr(10);
            foreach($dird as $d)
            {
                $js.= "kpi[".$d."] = new Array();".chr(10);
            }
                    $sql2 = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_kpi k";
                    $sql2.= " WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND d.dirid = s.subdirid AND k.kpiyn = 'Y' AND k.kpisubid = s.subid";
                    $sql2.= " ORDER BY s.subdirid, s.subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $sort2 = $row2['subsort'];
                            $did2 = $row2['subdirid'];
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            $js.= "kpi[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                        }
                    mysql_close($con2);
            $js.= "var cf = new Array();".chr(10);
            foreach($dird as $d)
            {
                $js.= "cf[".$d."] = new Array();".chr(10);
            }
                    $sql2 = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_finance_lineitems l, assist_".$cmpcode."_".$modref."_finance_cashflow c";
                    $sql2.= " WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND d.dirid = s.subdirid AND l.lineyn = 'Y' AND l.linesubid = s.subid AND l.lineid = c.cflineid AND c.cfyn = 'Y'";
                    $sql2.= " ORDER BY s.subdirid, s.subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $sort2 = $row2['subsort'];
                            $did2 = $row2['subdirid'];
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            $js.= "cf[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                        }
                    mysql_close($con2);
            $js.= "var cp = new Array();".chr(10);
            foreach($dird as $d)
            {
                $js.= "cp[".$d."] = new Array();".chr(10);
            }
                    $sql2 = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_capital c";
                    $sql2.= " WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND d.dirid = s.subdirid AND c.cpyn = 'Y' AND c.cpsubid = s.subid";
                    $sql2.= " ORDER BY s.subdirid, s.subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $sort2 = $row2['subsort'];
                            $did2 = $row2['subdirid'];
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            $js.= "cp[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                        }
                    mysql_close($con2);
        ?>
    <tr id=who>
        <td class=tdheaderl valign=top>Who:</td>
        <td class=tdgeneral><select name=dir id=dir>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['dirid'];
                    $val = $row['dirtxt'];
                    echo("<option ");
                    if($id==1) { echo("selected "); }
                    echo("value=d_".$id.">".$val."</option>");
//                    $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
                    $sql2 = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_kpi k";
                    $sql2.= " WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND d.dirid = s.subdirid AND k.kpiyn = 'Y' AND k.kpisubid = s.subid  AND d.dirid = ".$id;
                    $sql2.= " ORDER BY s.subdirid, s.subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            echo("<option value=s_".$id2.">&nbsp;&nbsp;&nbsp;- ".$val2."</option>");
                        }
                    mysql_close($con2);
                }
            mysql_close();
            $r = 1;
        ?>
        </select></td>
    </tr><?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        $tarr = array();
        while($row = mysql_fetch_array($rs))
        {
            $tarr[$t] = $row;
            $t++;
        }
    mysql_close();
    ?><tr id=when>
        <td class=tdheaderl valign=top width=120>When:</td>
        <td class=tdgeneral>
            <input type=radio checked name=w2 value=Q1 id=q1> <label for=q1>1st Quarter</label><br>
            <input type=radio name=w2 value=Q2 id=q2> <label for=q2>2nd Quarter</label><br>
            <input type=radio name=w2 value=Q3 id=q3> <label for=q3>3rd Quarter</label><br>
            <input type=radio name=w2 value=Q4 id=q4> <label for=q4>4th Quarter</label><br>
            <input type=radio name=w2 value=YTD id=ytd> <label for=ytd>Year-to-date</label><br>
            <input type=radio name=w2 value=ALL id=all> <label for=all>All months</label><br>
            <input type=radio name=w2 value=SEL id=sel> <label for=sel>From <select name=wf onclick="document.getElementById('sel').checked = true;" id=wf><?php
                $t1 = 0;
                foreach($tarr as $time)
                {
                    echo("<option ");
                    if(date("m Y",$today)==date("m Y",$time['sval']))
                    {
                        echo("selected ");
                        $t1++;
                    }
                    echo("value=".$time['sval'].">".date("M Y",$time['sval'])."</option>");
                }
            ?></select> to <select name=wt onclick="document.getElementById('sel').checked = true;" id=wt><?php
                $t2 = 0;
                foreach($tarr as $time)
                {
                    echo("<option ");
                    if(date("m Y",$today)==date("m Y",$time['eval']))
                    {
                        echo("selected ");
                        $t2++;
                    }
                    echo("value=".$time['eval'].">".date("M Y",$time['eval'])."</option>");
                }
            ?></select></label><br>
        </td>
    </tr>
    <?php
    if($t1==0)
    {
        ?>
        <script type=text/javascript>
        document.getElementById('wf').value = document.getElementById('wf').options[0].value;
        </script>
        <?php
    }
    if($t2==0)
    {
        ?>
        <script type=text/javascript>
        document.getElementById('wt').value = document.getElementById('wt').options[0].value;
        </script>
        <?php
    }
    ?>
    <tr id=graph>
        <td class=tdheaderl valign=top width=120>Graph:</td>
        <td class=tdgeneral>
            <input type=checkbox checked name=g value=Y id=g> <label for=g id=gl>Display graph</label><br>
        </td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=120>&nbsp;</td>
        <td class=tdgeneral><input type=submit value=View id=v></td>
    </tr>
</table>
</form>
<script type=text/javascript>
<?php echo($js); ?>
if(document.getElementById('dir').length == 0) { document.getElementById('v').disabled = true; }
</script>
<p>&nbsp;</p>
</body>
</html>
