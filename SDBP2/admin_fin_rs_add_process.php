<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>


<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Revenue By Source ~ Add Line Item</b></h1>
<p>&nbsp;</p>
<?php
$val = htmlentities($_POST['val'],ENT_QUOTES,"ISO-8859-1");
$budget = $_POST['budget'];

if(strlen($val)>0)
{
    $rssort = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_revbysource WHERE rsyn = 'Y' ORDER BY rssort DESC";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $row = mysql_fetch_array($rs);
            $rssort = $row['rssort']+1;
        }
        else
        {
            $rssort = 0;
        }
    mysql_close();
        
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_finance_revbysource SET rsline = '".$val."', rsyn = 'Y', rssort = ".$rssort."";
    for($b=0;$b<count($budget);$b++)
    {
        $m = $budget[$b];
        if(is_numeric($m))
        {
            $b2 = $b+1;
            $sql.= ", rs".$b2."budget = ".$m;
        }
    }
    include("inc_db_con.php");

$tsql = $sql;
$told = "";
$trans = "Added new RevBySource line";
include("inc_transaction_log.php");

    echo("<p>Line item '".$val."' successfully added.</p>");
    $urlback = "admin_fin_rs_setup.php";
    include("inc_goback.php");

}
else
{
    echo("<p>An error has occurred.  Please go back and try again.</p>");
    $urlback = "admin_fin_rs_add.php";
    include("inc_goback.php");
}



?>
<?php
?>
<p>&nbsp;</p>
</body>
</html>
