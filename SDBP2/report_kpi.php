<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 1px solid #ababab;
    }
	.b-w {
		border: 1px solid #ffffff;
	}
</style>
<script type=text/javascript>
function dirSub(me) {
        var dir = me.value;
        if(dir.length>0 && !isNaN(parseInt(dir)))
        {
            var sub = document.getElementById('sf');
            sub.length = 0;
            var sf = subs[dir];
            var o = 1;
            var ds = false;
            var opt = new Array();
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,true);
            for(s in sf)
            {
                sub.options[o] = new Option(sf[s][1],sf[s][0],ds,ds);
                o++;
            }
        }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - KPI</b></h1>
<form name=kpireport method=post action=report_kpi_process.php>
<h3 class=fc>1. Select the columns you want in your report:</h3>
<div style="margin-left: 17px">
<table cellpadding="3" cellspacing="0" width=700>
    <tr>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0  style="border-width: 0px">
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="dir" value="Y"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Directorate</td>
            	</tr>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="sub" value="Y"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Sub-Directorate</td>
            	</tr>
<?php
$head = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $m2 = $mnr / 2;
    $m = round($m2);
    if($m < $m2) { $m++; }
    $r=1;
    while($row = mysql_fetch_array($rs))
    {
        $head[$row['headfield']] = $row;
        $r++;
        if($r > $m)
        {
            $r=0;
            ?>
            </table>
        </td>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0 style="border: 0px">
            <?php
        }
        ?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="<?php echo($row['headfield']); ?>" value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle><?php echo($row['headdetail']); ?></td>
            	</tr>
        <?php
    }
mysql_close();

$timearr = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $timearr[$row['sort']]['sort'] = $row['sort'];
        $timearr[$row['sort']]['val'] = date("M-y",$row['eval']);
    }
mysql_close();
?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="results" value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>KPI Results<br>from <select name=cf id=cf>
                    <?php
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo("<option ");
                        if($t == 1) { echo(" selected "); }
                        echo("value=".$tim['sort'].">".$tim['val']."</option>");
                        $t++;
                    }
                    ?></select> to <select name=ct id=ct>
                    <?php
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo("<option ");
                        if($t == count($timearr)) { echo(" selected "); }
                        echo("value=".$tim['sort'].">".$tim['val']."</option>");
                        $t++;
                    }
                    ?></select></td>
            	</tr>
            </table>
        </td>
    </tr>
</table>
<p>Include Summary of Results? <input type=checkbox value=Y name=krsum checked></p>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class="tdgeneral" valign=top><b>Directorate:</b></td>
		<td class="tdgeneral" valign=top colspan=4><select name=dirfilter id=df onchange="dirSub(this);">
            <option selected value=ALL>All Directorates</option>
            <?php
            $js = "";
            $sql = "SELECT DISTINCT d.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
            $sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= ", assist_".$cmpcode."_".$modref."_kpi k ";
            $sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND k.kpiyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = k.kpisubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option value=".$row['dirid'].">".$row['dirtxt']."</option>");
                    $js.= "subs[".$row['dirid']."] = new Array();".chr(10);
                }
            mysql_close();
/*            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option value=".$row['dirid'].">".$row['dirtxt']."</option>");
                }
            mysql_close();
*/            ?>
        </select></td>
	</tr>
	<tr>
		<td class="tdgeneral" valign=top><b>Sub-Directorate:</b></td>
		<td class="tdgeneral" valign=top colspan=4><select name=subfilter[] id=sf size=6 multiple>
            <option selected value=ALL>All Sub-Directorates</option>
            <?php
            $sql = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
            $sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= ", assist_".$cmpcode."_".$modref."_kpi k ";
            $sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND k.kpiyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = k.kpisubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $did2 = $row['subdirid'];
                    $sort2 = $row['subsort'];
                    $id2 = $row['subid'];
                    $val2 = $row['subtxt'];
                    echo("<option value=".$row['subid'].">".$row['subtxt']."</option>");
                    $js.= "subs[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                }
            mysql_close();
            ?>
        </select><br><i><small>Use CTRL key to select multiple options</small></i></td>
	</tr>
	<?php
    foreach($head as $hrow)
    {
        $i=0;
        if($hrow['headfield']!="prog")
        {
        ?>
        <tr>
            <td class="tdgeneral" valign=top><b><?php echo($hrow['headdetail']); ?>:</b></td>
        <?php
        switch($hrow['headfield'])
        {
            case "ktype":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=5><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpitype WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "munkpa":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=5><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "natkpa":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=6><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_natkpa WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "kpicalctype":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=6><option selected value=all>All</option>");
                echo("<option value=ACC>Accumulative</option>");
                echo("<option value=CO>Carry-Over</option>");
                echo("<option value=STD>Stand-Alone</option>");
                echo("<option value=ZERO>Zero %</option>");
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "kpistratop":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=4><option selected value=all>All</option>");
                echo("<option value=S>Strategic</option>");
                echo("<option value=O>Operational</option>");
                echo("<option value=P>Project</option>");
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "wards":
                ?>
        		<td class="tdgeneral" valign=top><input type=checkbox name=wardsfilter[] value=X checked> Any&nbsp;<br>
                <input type=checkbox name=wardsfilter[] value=1> All&nbsp;<br>
        		<?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $wmnr = mysql_num_rows($rs2)+2;
                            $c2 = $wmnr / 4;
                            $c = round($c2);
                            if($c < $c2) { $c++; }
                            $w=2;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                $w++;
                                                if($w>$c)
                                                {
                                                    echo("</td><td class=tdgeneral valign=top>");
                                                    $w = 1;
                                                }
                                                echo("<input type=checkbox name=wardsfilter[] value=".$row2['id']." >".$row2['value']."<br>");
                                            }
                                        ?>
                                <?php
                        mysql_close($con2);
                break;
            default:
                ?>
                <td class="tdgeneral" valign=top colspan=4><input type=text name=<?php echo($hrow['headfield']."filter"); ?>>&nbsp;<select  name=<?php echo($hrow['headfield']."filtertype"); ?>><option selected value=all>Match all words</option><option value=any>Match any word</option><option value=exact>Match exact phrase</option></td>
                <?php
                break;
        }
        ?>
        </tr>
        <?php
        }
    }
    ?>
	<tr>
		<td class="tdgeneral" valign=top><b>Target Type:</b></td>
		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=targettypefilter size=4><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_targettype WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']." (".$row['code'].")</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
            </td>
    </tr>
	<tr>
		<td class="tdgeneral" valign=top><b>KPI Result:</b></td>
		<td class="tdgeneral" valign=top colspan=4>
                <select name=kpiresultfilter>
					<option selected value=ALL>All</option>
					<?php foreach($krsetup as $krs) {
						echo("<option value=".$krs['sort'].">".$krs['value']."</option>");
					} ?>
                </select> <img src=/pics/help.gif style="cursor:hand;" onclick="javascript:window.open('glossary.php#krr','Glossary','width=600,height=300');">
				<br><input type=checkbox name=krgloss value=Y> Include explanation of KPI Result classification in report?
            </td>
    </tr>
</table>
</div>
<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>
<div style="margin-left: 17px">
<table id="table3" cellspacing="0" cellpadding="3" width=700 class=b-w>
	<tr>
		<td class="tdgeneral b-w" style="border-bottom: 0px">Sort by:&nbsp;
		<select name=sort>
        <option selected value=dir>Directorate / Sub-Directorate</option>
		<?php foreach($head as $hrow) {
            if($hrow['headfield']!="wards")
            {
                echo(chr(10)."<option value=".$hrow['headfield'].">".$hrow['headdetail']."</option>");
            }
        }?>
        </select></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Select the report output method:</h3>
<div style="margin-left: 17px">
<table cellspacing="0" cellpadding="3" width=700 class=b-w>
	<tr>
		<td width=30 class="tdgeneral b-w" align=center><input type="radio" name="output" value="display" checked id=csvn></td>
		<td class="tdgeneral b-w"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class="tdgeneral b-w" align=center><input type="radio" name="output" value="csv" id=csvy></td>
		<td class="tdgeneral b-w"><label for=csvy>Microsoft Excel (Plain Text)</label></td>
	</tr>
	<tr>
		<td class="tdgeneral b-w" align=center><input type="radio" name="output" value="excel" id=excely></td>
		<td class="tdgeneral b-w"><label for=excely>Microsoft Excel (Formatted)*</label>
		</td>
	</tr>
</table>
</div>
<h3 class=fc>5. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel. 
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
<script type=text/javascript>
var subs = new Array();
<?php echo($js); ?>
</script>
</body>

</html>
