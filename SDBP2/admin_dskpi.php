<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function updateKPI(d) {
    var s = document.getElementById('ksub').value;
    if(s == "X" || s.length == 0)
    {
        s = 0;
    }
    document.location.href = "admin_kpi_edit_list.php?t=op&r=k&s="+s+"&d="+d;
}
function updateCP(d) {
    //var s = document.getElementById('csub').value;
    var s = "X";
    if(s == "X" || s.length == 0)
    {
        s = 0;
    }
    document.location.href = "admin_cp_edit_list.php?t=cp&r=k&s="+s+"&d="+d;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<?php
$referer = "-".$_SERVER['HTTP_REFERER']."-";
$referer2 = explode("/",$referer);
//if($referer=="--" || ($referer2[2]!="assist.ignite4u.co.za" && $referer2[2]!="ignite-jeb"))
//        die("<P>You are not authorised to view this page.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");


$dirid = $_GET['d'];
$err = "Y";
$dir = array();
if(strlen($dirid)>0 && is_numeric($dirid))
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' AND dirid = ".$dirid;
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $dir = mysql_fetch_array($rs);
            $err = "N";
        }
    mysql_close();
}


if($err == "Y")
{
    ?>
    <h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin</b></h1>
    <p>&nbsp;</p>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
else
{
?>

<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ <?php echo($dir['dirtxt']); ?></b></h1>
<p>&nbsp;</p>
<?php $width=170; ?>

<table cellpadding=3 cellspacing=0 width=570>
<?php // OPERATIONAL PERFORMANCE ?>
	<tr>
		<td style="border-bottom: 1px solid #ababab;" height=27 width=570 class=tdheaderl colspan=2>Operational Performance</td>
    </tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #dddddd; border-right: 0px solid #ffffff;" class=tdgeneral>Add new Operational Performance KPIs</td>
		<td style="border-bottom: 1px solid #dddddd; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value="  Add  " onclick="document.location.href = 'admin_kpi_add.php?t=op&r=k&s=0&d=<?php echo($dirid); ?>';"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; border-right: 0px solid #ffffff;" class=tdgeneral>Update Operational Performance KPI</td>
		<td style="border-bottom: 1px solid #ababab; border-left: 0px solid #ffffff;" class=tdgeneral align=right><select name=ksub id=ksub>
		<option value=X>Entire Directorate</option>
		<?php
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid." ORDER BY subsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                ?>
                <option <?php if($row['subhead']=="Y") { echo("selected"); } ?> value=<?php echo($row['subid']); ?>><?php echo($row['subtxt']); ?></option>
                <?php
            }
        mysql_close();
        ?>
        </select><input type=button value="Update" onclick="updateKPI(<?php echo($dirid); ?>);"></td>
	</tr>

<?php // CAPITAL PROJECTS ?>
	<tr>
		<td style="border-bottom: 1px solid #ababab;" width=570 class=tdheaderl colspan=2 height=27>Capital Projects</td>
    </tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #dddddd; border-right: 0px solid #ffffff;" class=tdgeneral>Add new Capital Project KPI</td>
		<td style="border-bottom: 1px solid #dddddd; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value="  Add  " onclick="document.location.href = 'admin_cp_list.php?t=cp&r=k&s=0&d=<?php echo($dirid); ?>';"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; border-right: 0px solid #ffffff;" class=tdgeneral>Update Capital Project KPI</td>
		<td style="border-bottom: 1px solid #ababab; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value="Update" id=cpup onclick="updateCP(<?php echo($dirid); ?>);"></td>
	</tr>


</table>

<?php

}   //IF ERR ON GET DIR DATA

$urlback = "admin.php";
include("inc_goback.php");
?>


</body>

</html>
