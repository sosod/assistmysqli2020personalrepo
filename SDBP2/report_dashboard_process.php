<?php
    include("inc_ignite.php");
    $r = 1;

$referer = "-".$_SERVER['HTTP_REFERER']."-";
$referer2 = explode("/",$referer);
$referer2[4] = explode(".",$referer2[4]);
$referer = $referer2[4][0];

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdgeneral {
    border-left: 0px solid #ffffff;
//    padding-left: 10px;
}
.tdheaderl {
    border-right: 0px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    border-top: 0px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: 0px;">SDBIP <?php echo($modtxt); ?>: Report - Dashboard
<?php
$what = $_POST['what'];
$graph = $_POST['graph'];
$who = $_POST['who'];
$who2 = $_POST['more'];
$when = $_POST['when'];

if($who2 != "Y")
{
    $who2 = "N";
}

if($when != "YTD" && $when != "SEL")
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        $tarr = array();
        while($row = mysql_fetch_array($rs))
        {
            $tarr[$t] = $row;
            $t++;
        }
    mysql_close();
}
$title = "";

switch($when)
{
    case "Q1":
        $title = "Year-to-date as at ".date("d M Y",$tarr[2]['eval']);
        $wf = 1;
        $wt = 3;
        break;
    case "Q2":
        $title = "Year-to-date as at ".date("d M Y",$tarr[5]['eval']);
        $wf = 1;
        $wt = 6;
        break;
    case "Q3":
        $title = "Year-to-date as at ".date("d M Y",$tarr[8]['eval']);
        $wf = 1;
        $wt = 9;
        break;
    case "Q4":
        $title = "Year-to-date as at ".date("d M Y",$tarr[11]['eval']);
        $wf = 1;
        $wt = 12;
        break;
    case "SEL":
        $wf = $_POST['wf'];
        $wt = $_POST['wt'];
        if(is_numeric($wf) && is_numeric($wt))
        {
            $title = "Results from ".date("M Y",$wf)." to ".date("M Y",$wt);
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sval = ".$wf." ORDER BY sort";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                $wf = $row['sort'];
            mysql_close();
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND eval = ".$wt." ORDER BY sort";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                $wt = $row['sort'];
            mysql_close();
        }
        else
        {
            $when = "YTD";
            $wf = 1;
            $title = "Year-to-date as at ".date("d M Y",$today);
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sval < ".$today." AND eval > ".$today." ORDER BY sort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                    $wt = $row['sort'];
                }
                else
                {
                    $wt=12;
                }
            mysql_close();
        }
        break;
    default:
        $title = "Year-to-date as at ".date("d M Y",$today);
        $wf = 1;
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sval < ".$today." AND eval > ".$today." ORDER BY sort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                    $wt = $row['sort'];
                }
                else
                {
                    $wt=12;
                }
            mysql_close();
        break;
}

if($who != "mun")
{
    $warr = explode("_",$who);
    $did = $warr[1];
    $dtype = $warr[0];
    $titletxt = $cmpname;
}
else
{
    $did = 0;
    $dtype = "m";
    $titletxt = $cmpname;
}

if(strlen($did)==0 || !is_numeric($did) || ($dtype != "s" && $dtype != "d") )
{
    $dtype = "m";
    $did = 0;
    $titletxt = $cmpname;
}
else
{
    if($dtype == "d")
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$did;
        include("inc_db_con.php");
            $dirrow = mysql_fetch_array($rs);
            $titletxt = $dirrow['dirtxt'];
        mysql_close();
    }
    else
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$did;
        include("inc_db_con.php");
            $subrow = mysql_fetch_array($rs);
            $titletxt = $subrow['subtxt'];
        mysql_close();
    }
}

?>
<span style="font-weight: normal; color: #000; text-decoration: none; font-size:8pt;"> (<?php echo($title); ?>)</h1>
<?php
if($what == "CF")
{
    if($graph == "M")
    {
        include("inc_report_dashboard_fin_line.php");
    }
    else
    {
        include("inc_report_dashboard_fin_bar.php");
    }
}
else
{
    if($graph == "M")
    {
        include("inc_report_dashboard_kpi_line.php");
    }
    else
    {
        include("inc_report_dashboard_kpi_pie.php");
    }
}
?>
</body>
</html>
