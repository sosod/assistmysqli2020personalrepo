<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}

			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<h2 class=fc>Update Capital Projects - Import update<input type=hidden id=lab></h2>
<center>
<span id=progbar><div id="progressbar" style="width: 500;"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script>
<p style="font-height: 1pt; line-height: 1pt; margin: 10 0 0 0">&nbsp;</p>
<?php
$fileloc = $_POST['floc'];
$calc = $_POST['calc'];
$impcheck = $_POST['import'];
//print_r($impcheck);
foreach($impcheck as $i)
{
    $import[$i] = "Y";
}
$p=0;
if(strlen($fileloc)>0 && file_exists($fileloc))
{
            $file = fopen($fileloc,"r");
            $f = 1;
            $r=1;
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>5 && $r>2)
                {
                    $data[$f] = $tmpdata;
                    $f++;
                }
                $r++;
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader rowspan=2>Ignite<br>Ref</td>
                        <td class=tdheader rowspan=2>Directorate</td>
                        <td class=tdheader rowspan=2>Sub-Directorate</td>
                        <td class=tdheader rowspan=2>Cap. Proj.<br>Number</td>
                        <td class=tdheader rowspan=2>Vote<br>Number</td>
                        <td class=tdheader rowspan=2>Project<br>Description</td>
                <?php
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sort >= 1 AND sort <= 12 ORDER BY sort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $timearr[$row['sort']] = $row;
                ?>
                        <td class=tdheader colspan=4><?php echo(date("F-Y",$row['eval'])); ?></td>
                    <?php
                    }
                mysql_close(); ?>
                    </tr>
                    <tr>
                    <?php foreach($timearr as $tim) { ?>
                        <td class=tdheader>Budget</td>
                        <td class=tdheader>Actual</td>
                        <td class=tdheader>Acc. % - YTD</td>
                        <td class=tdheader>R - YTD</td>
                    <?php } ?>
                    </tr>
                    <?php
                    $p2 = 100/count($data);
                    foreach($data as $idata)
                    {
set_time_limit(30);
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
//                        echo("<script type=text/javascript>incProg(".$p.");</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $cpid = $idata[0];
                    if(strlen($cpid)>0 && is_numeric($cpid) && $cpid > 0)
                    {
                        $impsql = array();
                        if($import[$cpid] == "Y")
                        {
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital c, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s ";
                            $sql.= " WHERE c.cpid = ".$cpid;
                            $sql.= " AND c.cpsubid = s.subid AND s.subdirid = d.dirid";
                            include("inc_db_con.php");
                                $mnr = mysql_num_rows($rs);
                                $row = mysql_fetch_array($rs);
                            mysql_close();
                                if($mnr > 0)
                                {
                                    $budget = array();
                                    $actual = array();
                                    $accytd = array();
                                    $totytd = array();
                                    $ctotytd = 0;
                                    $cbudytd = 0;
                                    $b = 0;
                                    $a = 0;
                                    $y = 0;
                                    $o = 0;
                                    $timeid = 0;
                                    $t=6;
                                    foreach($timearr as $tim)
                                    {
                                        $timeid = $tim['id'];
                                        $b = $idata[$t];
                                        if(!is_numeric($b))
                                        {
                                            $bud = explode(",",$b);
                                            $b = implode("",$bud);
                                        }
                                        if(strlen($b)==0 || !is_numeric($b)) { $b = 0; }
                                        $cbudytd = $cbudytd + $b;
                                        $budget[$timeid] = $b;
                                        $a = $idata[$t+1];
                                        if(!is_numeric($a))
                                        {
                                            $ac = explode(",",$a);
                                            $a = implode("",$ac);
                                        }
                                        if(strlen($a)==0 || !is_numeric($a)) { $a = 0; }
                                        $ctotytd = $ctotytd + $a;
                                        $actual[$timeid] = $a;
                                        if($calc == "Y")
                                        {
                                            $accytd[$timeid] = ($ctotytd / $cbudytd) * 100;
                                            $totytd[$timeid] = $ctotytd;
                                        }
                                        else
                                        {
                                            $y = $idata[$t+2];
                                            $accend = substr($y,-1,1);
                                            if($accend == "%") { $y = substr($y,0,-1); $y = $y * 1;}
                                            if(!is_numeric($y))
                                            {
                                                $ay = explode(",",$y);
                                                $y = implode("",$ay);
                                            }
                                            if(strlen($y)==0 || !is_numeric($y)) { $y = 0; }
                                            $accytd[$timeid] = $y;
                                            $o = $idata[$t+3];
                                            if(!is_numeric($o))
                                            {
                                                $tot = explode(",",$o);
                                                $o = implode("",$tot);
                                            }
                                            if(strlen($o)==0 || !is_numeric($o)) { $o = 0; }
                                            $totytd[$timeid] = $o;
                                        }
                                        $t = $t+4;
                                        $impsql[$timeid] = "UPDATE assist_".$cmpcode."_".$modref."_capital_results SET crbudget = ".number_format($budget[$timeid],2,".","").", cractual = ".number_format($actual[$timeid],2,".","").", craccytd = ".number_format($accytd[$timeid],2,".","").", crtotytd = ".number_format($totytd[$timeid],2,".","")." WHERE crcpid = ".$cpid." AND crtimeid = ".$timeid;
                                        $sql = $impsql[$timeid];
                                        include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Updated financials for CP ".$cpid." with timeid ".$timeid;
        include("inc_transaction_log.php");
                                        $class="tdheadergreen";
                                    }
                                    $dir = $row['dirtxt'];
                                    $sub = $row['subtxt'];
                                    $cpnum = $row['cpnum'];
                                    $vote = $row['cpvotenum'];
                                    $project = $row['cpproject'];
                        ?>
                    <tr>
                        <td class=<?php echo($class); ?>><?php echo($cpid); ?></td>
                        <td class=tdgeneral><?php echo($dir); ?></td>
                        <td class=tdgeneral><?php echo($sub); ?></td>
                        <td class=tdgeneral><?php echo($cpnum); ?></td>
                        <td class=tdgeneral><?php echo($vote); ?></td>
                        <td class=tdgeneral style="border-right: 1px solid #555555"><?php echo($project); ?></td>
                    <?php
                    foreach($timearr as $tim)
                    {
                        $tid = $tim['id'];
                        //echo("<P>".$cpid."-".$tid."-".$impsql[$tid]."</p>");
                                ?>
                        <td class=tdgeneral align=right style="border-left: 1px solid #555555"><?php echo(number_format($budget[$tid],2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($actual[$tid],2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($accytd[$tid],2)); ?>%</td>
                        <td class=tdgeneral align=right style="border-right: 1px solid #555555"><?php echo(number_format($totytd[$tid],2)); ?></td>
                    <?php
                    } ?>
                    </tr>
                        <?php
                                }   //mnr > 0
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($cpid); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No capital project found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                    }
                    else    //import check Y
                    {
                                    ?>
                    <tr>
                        <td class=tdheaderorange><?php echo($cpid); ?></td>
                        <td class=tdgeneral colspan=53>Line ignored per user request.</td>
                    </tr>
                                    <?php
                    }   //import check Y
                        }   //valid cpid
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($cpid); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No capital project found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                    }
                    ?>
                </table>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
else
{
            echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
}

?></center>
                <?php
                    $urlback = "admin_fin_cp_update.php";
                    include("inc_goback.php");
                    //print_r($impsql);
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
</body>
</html>
