<?php
    include("inc_ignite.php");
    $r = 1;

function calcKRR($kct, $rt, $ra)
{
    $krr = 0;
    if($kct == "ZERO")
    {
        if($ra > $rt)
        {
            $krr = 1;
        }
        else
        {
            $krr = 3;
        }
    }
    else
    {
        if($ra >= $rt && $ra > 0)
        {
            $krr = 3;
        }
        else
        {
            if($rt > 0)
            {
                if($ra/$rt < 0.75)
                {
                    $krr = 1;
                }
                else
                {
                    $krr = 2;
                }
            }
        }
    }

    return $krr;

}


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdgeneral {
    border-left: 0px solid #ffffff;
}
.tdheaderl {
    border-right: 0px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    border-top: 0px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: View KPI</b></h1>
<?php


$style[0] = "background-color: #555555;";
$style[1] = "background-color: #cc0001;";
$style[2] = "background-color: #fe9900;";
$style[3] = "background-color: #006600;";
$style[4] = "background-color: #000066;";

$style2[0] = "background-color: #555555; border-left: 1px solid #ffffff;";
$style2[1] = "background-color: #cc0001; border-left: 1px solid #ffffff;";
$style2[2] = "background-color: #fe9900; border-left: 1px solid #ffffff;";
$style2[3] = "background-color: #006600; border-left: 1px solid #ffffff;";
$style2[4] = "background-color: #000066; border-left: 1px solid #ffffff;";


$stylel[0] = "background-color: #dddddd;";
$stylel[1] = "background-color: #ffcccc;";
$stylel[2] = "background-color: #ffeaca;";
$stylel[3] = "background-color: #ceffde;";
$stylel[4] = "background-color: #ccccff;";


$kpiid = $_GET['k'];
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiid = ".$kpiid." AND kpiyn = 'Y'";
include("inc_db_con.php");
    $kpirow = mysql_fetch_array($rs);
    $kct = strtoupper($kpirow['kpicalctype']);
mysql_close();
$sql = "SELECT dirtxt, subtxt FROM assist_".$cmpcode."_".$modref."_dir, assist_".$cmpcode."_".$modref."_dirsub WHERE ";
$sql.= "subdirid = dirid";
$sql.= " AND subid = ".$kpirow['kpisubid'];
include("inc_db_con.php");
    $dirrow = mysql_fetch_array($rs);
mysql_close();
?>
<h2>KPI Details</h2>
<table cellpadding=5 cellspacing=0 width=500>
    <tr>
        <td class=tdheaderl valign=top width=120>Reference:</td>
        <td class=tdgeneral><?php echo($kpiid); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=120>Directorate:</td>
        <td class=tdgeneral><?php echo($dirrow['dirtxt']); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=120>Sub-Directorate:</td>
        <td class=tdgeneral><?php echo($dirrow['subtxt']); ?>&nbsp;</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headyn = 'Y' AND headdetailyn = 'Y' AND headtype = 'KPI' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $value  = "";
        switch($row['headfield'])
        {
            case "kpistratop":
                if($kpirow['kpistratop']=="S") { $value = "Strategic"; } else { $value = "Operational"; }
                break;
            case "ktype":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpitype WHERE id = ".$kpirow['kpitypeid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value'];
                break;
            case "munkpa":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE id = ".$kpirow['kpimunkpaid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value']." (".$row2['code'].")";
                break;
            case "natkpa":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_natkpa WHERE id = ".$kpirow['kpinatkpaid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value']." (".$row2['code'].")";
                break;
            case "prog":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE id = ".$kpirow['kpiprogid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value'];
                break;
            case "wards":
                        $sql2 = "SELECT w.value FROM assist_".$cmpcode."_".$modref."_kpi_wards k, assist_".$cmpcode."_".$modref."_list_wards w WHERE w.id = k.kwwardsid AND k.kwkpiid = ".$kpiid." AND k.kwyn = 'Y' AND w.yn = 'Y'";
                        include("inc_db_con2.php");
                            if(mysql_num_rows($rs2)>1)
                            {
                                    while($row2=mysql_fetch_array($rs2))
                                    {
                                        $value.= $row2['value']."; ";
                                    }
                            }
                            else
                            {
                                $row2 = mysql_fetch_array($rs2);
                                $value = $row2['value'];
                            }
                        mysql_close($con2);
                break;
            default:
                $value = $kpirow[$row['headfield']];
                break;
        }
        if($row['headfield']!="kpicalctype")
        {
?>
    <tr>
        <td class=tdheaderl valign=top width=120><?php echo($row['headdetail']); ?>:</td>
        <td class=tdgeneral><?php echo($value); ?>&nbsp;</td>
    </tr>
<?php
        }
    }
mysql_close();
?>
</table>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc WHERE plckpiid = ".$kpiid." AND plcyn = 'Y' ORDER BY plcid DESC";
include("inc_db_con.php");
    $plc = array();
    $mnr = mysql_num_rows($rs);
    $plc = mysql_fetch_array($rs);
mysql_close();

if(count($plc)>5)
{
?>
<form name=plc method=post action=view_kpi_detail_plc.php target="plcview" onsubmit="window.open('', 'plcview', 'width=700,height=500,status=1,toolbar=1,menubar=1,resizable=1,scrollbars=1')">
<input type=hidden name=plcid value="<?php echo($plc['plcid']); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<p style="margin: -10 0 0 0"><input type=submit value=" Project Life Cycle "></p>
</form>
<?php } ?>
<h2>KPI Results</h2>
<table cellpadding=5 cellspacing=0 width=500>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $krt0 = 0;
    $kra0 = 0;
    while($row = mysql_fetch_array($rs))
    {
        $thead = $row;
                switch($kct)
                {
                    case "CO":
                        $sql2 = "SELECT max(krtarget) as krtarget, max(kractual) as kractual FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid <= ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid = ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2x = mysql_fetch_array($rs2);
                            $row2['krprogress'] = $row2x['krprogress'];
                            $row2['krmanagement'] = $row2x['krmanagement'];
                            $row2['krtargettypeid'] = $row2x['krtargettypeid'];
                        mysql_close($con2);
                        break;
                    default:
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid = ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        break;
                }
                $krt = $row2['krtarget'];
                $kra = $row2['kractual'];

                if($kct == "CO")
                {
                    if($row2x['krtarget']==0 && $row['sval']>$today)
                    {
                        $krt0 = 0;
                        $kra0 = 0;
                    }
                    else
                    {
                        $krt0 = $krt;
                        $kra0 = $kra;
                    }
                }
                else
                {
                    $krt0 = $krt0+$krt;
                    $kra0 = $kra0+$kra;
                }

                $krr = calcKRR($kct,$krt,$kra);

                switch($row2['krtargettypeid'])
                {
                    case 1:
                        $krtarget = "R ".number_format($krt,0);
                        $kractual = "R ".number_format($kra,0);
                        break;
                    case 2:
                        $krtarget = number_format($krt,0)."%";
                        $kractual = number_format($kra,0)."%";
                        break;
                    default:
                        $krtarget = $krt;
                        $kractual = $kra;
                        break;
                }

        if($row['sval']<$today) { $rowspan=5; } else { $rowspan=1; }
        //$rowspan=5;
        ?>
    <tr>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style[$s]); ?>" rowspan=<?php echo($rowspan); ?>><?php echo(date("d M Y",$row['eval'])); ?>:</td>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style2[$s]); ?>">Target:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if(($krt0>0 || $kra0>0 || $kct == "ZERO")) { echo($krtarget); } ?>&nbsp;</td>
    </tr>
    <?php if($row['sval']<$today) { ?>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style2[$s]); ?>">Actual:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if(($krt0>0 || $kra0>0) || $kct == "ZERO") { echo($kractual); } ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style2[$s]); ?>">Result:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if($krr>0) { ?><table border=0 cellpadding=0 cellspacing=0 style="border: 0px;"><tr><td width=30 class=tdheaderl style="border: 1px solid #ffffff; <?php echo($style[$krr]); ?>">&nbsp;</td></tr></table><?php } else { echo("&nbsp;"); } ?></td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style2[$s]); ?>">Progress:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php echo($row2['krprogress']); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style2[$s]); ?>">Management:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php echo($row2['krmanagement']); ?>&nbsp;</td>
    </tr>
        <?php
        }   //if sval < today
        $s++;
        if($s>4) { $s=1; }
    }
mysql_close();
?>
</table>
<?php
if($kpirow['kpicpid']>0)
{
    $cprow = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital WHERE cpyn = 'Y' AND cpid = ".$kpirow['kpicpid'];
    include("inc_db_con.php");
        $cprow = mysql_fetch_array($rs);
    mysql_close();
    if(count($cprow)>0)
    {
?>
<h2>Capital Project Details</h2>
<table cellpadding=5 cellspacing=0 width=500>
    <tr>
        <td class=tdheaderl valign=top width=120>Reference:</td>
        <td class=tdgeneral><?php echo($kpirow['kpicpid']); ?>&nbsp;</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headyn = 'Y' AND headdetailyn = 'Y' AND headtype = 'CP' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $value = "";
        if($row['headfield']!="progress")
        {
            switch($row['headfield'])
            {
                case "cpenddate":
                    $value = date("d M Y",$cprow['cpenddate']);
                    break;
                case "cpstartdate":
                    $value = date("d M Y",$cprow['cpstartdate']);
                    break;
                case "wards":
                        $sql2 = "SELECT w.value FROM assist_".$cmpcode."_".$modref."_capital_wards k, assist_".$cmpcode."_".$modref."_list_wards w WHERE w.id = k.cwwardsid AND k.cwcpid = ".$kpirow['kpicpid']." AND k.cwyn = 'Y' AND w.yn = 'Y'";
                        include("inc_db_con2.php");
                            if(mysql_num_rows($rs2)>1)
                            {
                                    while($row2=mysql_fetch_array($rs2))
                                    {
                                        $value.= $row2['value']."; ";
                                    }
                            }
                            else
                            {
                                $row2 = mysql_fetch_array($rs2);
                                $value = $row2['value'];
                            }
                        mysql_close($con2);
                    break;
                default:
                    $value = $cprow[$row['headfield']];
                    break;
            }
    ?>
    <tr>
        <td class=tdheaderl valign=top width=120><?php echo($row['headdetail']); ?>:</td>
        <td class=tdgeneral><?php echo($value); ?>&nbsp;</td>
    </tr>
    <?php
        }   //if field != progress
    }   //while
mysql_close();
?>
</table>
<h2>Capital Project Progress</h2>
<table cellpadding=5 cellspacing=0 width=500>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_results WHERE crcpid = ".$kpirow['kpicpid']." ORDER BY crtimeid";
include("inc_db_con.php");
    $crrow = array();
    while($row = mysql_fetch_array($rs))
    {
        $crrow[$row['crtimeid']] = $row;
    }
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $accytd = 0;
    $totytd = 0;
    while($row = mysql_fetch_array($rs))
    {
        if($row['sval']<$today) { $rowspan = 4; } else { $rowspan=1; }
        //$rowspan=4;
        if($crrow[$row['id']]['craccytd']>0) { $accytd = $crrow[$row['id']]['craccytd']; }
        if($crrow[$row['id']]['crtotytd']>0) { $totytd = $crrow[$row['id']]['crtotytd']; }
?>
    <tr>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style[$s]); ?>" rowspan=<?php echo($rowspan); ?>><?php echo(date("d M Y",$row['eval'])); ?>:</td>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style2[$s]); ?>">Budget:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if($crrow[$row['id']]['crbudget']>0 || $crrow[$row['id']]['cractual']>0 ) { echo("R ".number_format($crrow[$row['id']]['crbudget'],0)); } ?>&nbsp;</td>
    </tr>
    <?php if($row['sval']<$today) { ?>
    <tr>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style2[$s]); ?>">Actual:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if($crrow[$row['id']]['crbudget']>0 || $crrow[$row['id']]['cractual']>0 ) { echo("R ".number_format($crrow[$row['id']]['cractual'],0)); } ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style2[$s]); ?>">% Spent - YTD:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php echo(number_format($accytd,2)."%"); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style2[$s]); ?>">R Spent - YTD:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php echo("R ".number_format($totytd,0)); ?>&nbsp;</td>
    </tr>
    <?php
        }   //if tp < today
        $s++;
        if($s>4) { $s=1; }
    }
mysql_close();
?>
</table>
<?php
    }  //if count(cprow) > 0
} // if kpicpprojid > 0
?>
<p style="margin-top: 30px"><a href=# onclick="history.back()"><img src=/pics/tri_left.gif align=absmiddle border=0><span style="text-decoration: none;"> </span>Go Back</a></p>
<p>&nbsp;</p>
</body>
</html>
