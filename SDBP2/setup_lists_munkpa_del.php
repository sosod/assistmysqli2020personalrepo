<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Municipal KPAs ~ Delete</b></h1>
<p>&nbsp;</p>
<?php
//get form data
$kpaid = $_GET['k'];

if(is_numeric($kpaid) && $kpaid > 0)
{
$row = array();
//validate in timekeep
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE id = ".$kpaid;
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
mysql_close();

if(count($row) > 0)
{
//if exists:
    //insert into list_admins
    $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_munkpa SET ";
    $sql.= " yn = 'N'";
    $sql.= " WHERE id = ".$kpaid;
    include("inc_db_con.php");
    //insert into sdbip log
$tsql = $sql;
        $logref = $kpaid;
        $logtype = "SLKPA";
        $logaction = "Deleted Municipal KPA '".$row['value']."'";
        $logdisplay = "Y";
        include("inc_log.php");
    //insert into transaction log
$told = "";
$trans = $logaction." with id ".$kpaid;
include("inc_transaction_log.php");
    //display result
    ?>
    <p>Municipal KPA '<?php echo($row['value']); ?>' has been successfully deleted.</p>
    <p>&nbsp;</p>
    <?php
    $urlback = "setup_lists_munkpa.php";
    include("inc_goback.php");
    ?>
<?php
}
else
{
//else
    //display error
        ?>
        <p>An error has occurred (Err: SDBIP32).  <br>
        &nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_lists_munkpa.php";
        include("inc_goback.php");
        ?>
        <?php
}
}
else    //is numeric kpaid
{
        ?>
        <p>An error has occurred (Err: SDBIP33).  <br>
        &nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_lists_munkpa.php";
        include("inc_goback.php");

}
?>

<p>&nbsp;</p>
</body>
</html>
