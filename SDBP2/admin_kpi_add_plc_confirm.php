<?php
    include("inc_ignite.php");
    $targets = array();
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<?php include("inc_head_msie.php"); ?>
<script type=text/javascript src="lib/inc_plc.js"></script>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<script type="text/javascript">
$(function(){
    $('.datepicker').datepicker({
        showOn: 'both',
        buttonImage: 'calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd',
        changeMonth:true,
        changeYear:true
    });
});
</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Add KPI - Confirm KPI Details</b></h1>
<?php
$kpiid = $_POST['kpiid'];
$plcid = $_POST['plcid'];
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];


$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiid = ".$kpiid;
include("inc_db_con.php");
    $kpirow = mysql_fetch_array($rs);
mysql_close();
$sql = "SELECT dirtxt, subtxt FROM assist_".$cmpcode."_".$modref."_dir, assist_".$cmpcode."_".$modref."_dirsub WHERE ";
$sql.= "subdirid = dirid";
$sql.= " AND subid = ".$kpirow['kpisubid'];
include("inc_db_con.php");
    $dirrow = mysql_fetch_array($rs);
mysql_close();
?>
<h2>KPI Details</h2>
<table cellpadding=5 cellspacing=0 width=500>
    <tr>
        <td class=tdheaderl valign=top width=120>Reference:</td>
        <td class=tdgeneral><?php echo($kpiid); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=120>Directorate:</td>
        <td class=tdgeneral><?php echo($dirrow['dirtxt']); ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=120>Sub-Directorate:</td>
        <td class=tdgeneral><?php echo($dirrow['subtxt']); ?>&nbsp;</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headyn = 'Y' AND headdetailyn = 'Y' AND headtype = 'KPI' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $value  = "";
        switch($row['headfield'])
        {
            case "kpistratop":
                if($kpirow['kpistratop']=="S") { $value = "Strategic"; } else { $value = "Operational"; }
                break;
            case "ktype":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpitype WHERE id = ".$kpirow['kpitypeid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value'];
                break;
            case "munkpa":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE id = ".$kpirow['kpimunkpaid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value']." (".$row2['code'].")";
                break;
            case "natkpa":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_natkpa WHERE id = ".$kpirow['kpinatkpaid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value']." (".$row2['code'].")";
                break;
            case "prog":
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE id = ".$kpirow['kpiprogid'];
                include("inc_db_con2.php");
                    $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $value = $row2['value'];
                break;
            case "wards":
                        $sql2 = "SELECT w.value FROM assist_".$cmpcode."_".$modref."_kpi_wards k, assist_".$cmpcode."_".$modref."_list_wards w WHERE w.id = k.kwwardsid AND k.kwkpiid = ".$kpiid." AND k.kwyn = 'Y' AND w.yn = 'Y'";
                        include("inc_db_con2.php");
                            if(mysql_num_rows($rs2)>1)
                            {
                                    while($row2=mysql_fetch_array($rs2))
                                    {
                                        $value.= $row2['value']."; ";
                                    }
                            }
                            else
                            {
                                $row2 = mysql_fetch_array($rs2);
                                $value = $row2['value'];
                            }
                        mysql_close($con2);
                break;
            default:
                $value = $kpirow[$row['headfield']];
                break;
        }
        if($row['headfield']!="kpicalctype")
        {
?>
    <tr>
        <td class=tdheaderl valign=top width=120><?php echo($row['headdetail']); ?>:</td>
        <td class=tdgeneral><?php echo($value); ?>&nbsp;</td>
    </tr>
<?php
        }
    }
mysql_close();
?>
</table>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc WHERE plcid = ".$plcid;
include("inc_db_con.php");
    $plcrow = mysql_fetch_array($rs);
mysql_close();
?>
<h2 class=fc>Project Life Cycle</h2>
<table border="0" cellspacing="0" cellpadding="3" style="border: 0px solid #ffffff;">
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Name:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($plcrow['plcname']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Description:&nbsp;</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($plcrow['plcdescription']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Start Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo(date("d M Y",$plcrow['plcstartdate'])); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>End Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo(date("d M Y",$plcrow['plcenddate'])); ?></td>
	</tr>
</table>
<p style="margin: 0 0 0 0;">&nbsp;</p>
<table border="1" cellspacing="0" cellpadding="3" width=100%>
	<tr>
		<td height=27 colspan="2" class="tdheader"><b>Project Phase</b></td>
		<td width=100 class="tdheader"><b>Calendar<br>Days</b></td>
		<td class="tdheader"><b>Start<br>Date</b></td>
		<td class="tdheader"><b>End<br>Date</b></td>
		<td class="tdheader"><b>% Target<br>of Project</b></td>
	</tr>
<?php
$tartot = 0;
$targets = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            switch($row['type'])
            {
        case "A":
//CUSTOM PHASES
        $sql2 = "SELECT p.*, c.value FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p, assist_".$cmpcode."_".$modref."_list_plcphases_custom c ";
        $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'C' AND p.ppphaseid = c.id ORDER BY p.ppsort";
        include("inc_db_con2.php");
            $mnr2 = mysql_num_rows($rs2);
            if($mnr2 > 0)
            {
            $a=0;
                while($row2 = mysql_fetch_array($rs2))
                {
                    $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = $targets[$Yr][$Mn] + $row2['pptarget'];
                    $a++;
?>
	<tr>
	<?php if($a==1) { ?>		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td><?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
	</tr>
<?php
                }
            }
        mysql_close($con2);
        break;
            case "P":
            //PROCUREMENT PHASES
                $sql2 = "SELECT p.*, c.value FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p, assist_".$cmpcode."_".$modref."_list_plcphases_proc c ";
                $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'P' AND p.ppphaseid = c.id ORDER BY p.ppsort";
                include("inc_db_con2.php");
                $mnr2 = mysql_num_rows($rs2);
                $r=0;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $r++;
                        $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = $targets[$Yr][$Mn] + $row2['pptarget'];
?>
	<tr>
        <?php if($r==1) { ?>
		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
        <?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
	</tr>
<?php
                    }
                mysql_close($con2);
            default:
            //STANDARD PHASES WHERE TYPE = S
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE pptype = 'S' AND ppphaseid = ".$row['id']." AND ppplcid = ".$plcid;
                include("inc_db_con2.php");
                    $mnr2 = mysql_num_rows($rs2);
                    if($mnr2 > 0)
                    {
                        $row2 = mysql_fetch_array($rs2);
                    }
                mysql_close($con2);
                if($mnr2>0)
                {
                    $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = $targets[$Yr][$Mn] + $row2['pptarget'];
?>
	<tr>
		<td colspan="2" class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
	</tr>
<?php
                }
                break;

            }
        }
    mysql_close();

?>
	<tr>
		<td height=27 colspan="2" class="tdgeneral"><b>Target % of Project Total:</b></td>
		<td class="tdgeneral" align="center" colspan=3></td>
		<td class="tdgeneral" align="center"><b><?php echo($tartot); ?> %</b></td>
	</tr>
</table>
<?php
//print_r($targets);
?>
<h2 class=fc>KPI Targets</h2>
<form name=plcconfirm method=post action=admin_kpi_add_plc_confirm_process.php>
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<table cellpadding=3 cellspacing=0 width=500>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $t=0;
    $targ2 = 0;
    while($row = mysql_fetch_array($rs))
    {
        //$targ = 0;
        $rowspan=2;
        $tar = $targets[date("Y",$row['eval'])][date("n",$row['eval'])];
        $targ = $targ + $tar;
        $targ2 = $targ2 + $tar;
        if($targ2==0 || strlen($targ2)==0)
        {
            $targ2 = 0;
        }
        if(strlen($targ)>0) { $targ = $targ." %"; }
        ?>
    <tr>
        <td class=tdheaderl width=180 style="<?php echo($style[$s]); ?>" ><?php echo(date("F Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if($tar>0) { echo($targ); } ?><input type=hidden name=krtarget[] value="<?php if($tar > 0) { echo($targ2); } else { echo("0"); }  ?>"></td>
    </tr>
        <?php
        $s++;
        if($s>4) { $s=1; }
        $t++;
    }
mysql_close();
?>
</table>
</form>

<p style="font-size: 1pt; line-height: 1pt;">&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="3" width=100%>
    <tr>
        <td class=tdgeneral align=center><input type=button value=" Confirm " onclick="document.forms['plcconfirm'].submit();"> <input type=button value=" Reject " onclick="document.forms['plcrej'].submit();"></td>
    </tr>
</table>
<form name=plcrej method=post action=admin_kpi_add_plc.php>
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
</form>
</form>
</body>

</html>
