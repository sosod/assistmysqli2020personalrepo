<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ababab; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    font-weight: normal;
}

button {
    font-size: 7pt;
    color: #cc0001;
}
</style>
<script type=text/javascript>
function adminKPI() {
    var d = document.getElementById('kpidirid').value;
    if(!isNaN(parseInt(d)))
    {
        document.location.href = "admin_dskpi.php?d="+d;
    }
    else
    {
        alert("Please select which Directorate you wish to administer.");
    }
}
</script>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin</b></h1>
<?php
//$referer = "-".$_SERVER['HTTP_REFERER']."-";
//$referer2 = explode("/",$referer);
//if($referer=="--" || ($referer2[2]!="assist.ignite4u.co.za" && $referer2[2]!="ignite-jeb"))
//        die("<P>You are not authorised to view this page.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");

?>
<p>&nbsp;</p>
<table cellpadding=5 cellspacing=0 width=570>
    <?php
    $width1 = 260;
    $width2 = 310;
//FINANCE ADMIN
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."' AND type = 'FIN'";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
    ?>
    <tr>
        <td width=570 colspan=2 class=tdheaderl height=30>Finance Administration</td>
    </tr>
	<?php include("inc_tr.php"); ?>
		<td height=25 style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral>Update financial information</td>
		<td height=25 style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> class=tdgeneral align=right><input type=button value=Configure onclick="document.location.href = 'admin_fin.php';" style="margin-right: 10px;"></td>
	</tr>
	<?php
        }
    mysql_close();

//DIR ADMIN
        $sql = "SELECT DISTINCT d.dirid, d.dirtxt FROM assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_list_admins a WHERE d.diryn = 'Y' AND a.yn = 'Y' AND a.tkid = '".$tkid."' AND a.type = 'DIR' AND d.dirid = a.ref ORDER BY d.dirsort";
        include("inc_db_con.php");
            if(mysql_num_rows($rs)>0)
            {
                ?>
    <tr>
        <td width=570 colspan=2 class=tdheaderl height=30>Directorate Administration</td>
    </tr>
                <?php
            }
            while($row = mysql_fetch_array($rs))
            {
    ?>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral><?php echo($row['dirtxt']); ?></b></td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> class=tdgeneral align=right><input type=button value=Configure onclick="document.location.href = 'admin_dir.php?d=<?php echo($row['dirid']); ?>';" style="margin-right: 10px;"></td>
	</tr>
	<?php
            }
        mysql_close();

//SUB ADMIN
        $sql = "SELECT DISTINCT s.subid, s.subtxt FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_list_admins a WHERE s.subyn = 'Y' AND a.yn = 'Y' AND a.tkid = '".$tkid."' AND a.type = 'SUB' AND s.subid = a.ref ORDER BY s.subsort";
        include("inc_db_con.php");
            if(mysql_num_rows($rs)>0)
            {
                ?>
    <tr>
        <td width=570 colspan=2 class=tdheaderl height=30>Sub-Directorate Administration</td>
    </tr>
                <?php
            }
            while($row = mysql_fetch_array($rs))
            {
    ?>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral><?php echo($row['subtxt']); ?></td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> class=tdgeneral align=right><input type=button value=Configure onclick="document.location.href = 'admin_sub.php?s=<?php echo($row['subid']); ?>';" style="margin-right: 10px;"></td>
	</tr>
	<?php
            }
        mysql_close();


//KPI ADMIN
    $kpiadmin = "N";
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."' AND type = 'KPI'";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $kpiadmin = "Y";
        }
    mysql_close();

    if($kpiadmin == "Y")
    {
    ?>
        <tr>
            <td width=570 colspan=2 class=tdheaderl height=30>KPI Administration</td>
        </tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral>Add KPI to all Directorates</td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> width=100 class=tdgeneral align=right><input type=button value="    Add    " onclick="document.location.href = 'admin_kpi_add.php?r=k&t=op&s=0&d=0';" style="margin-right: 10px;"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral>Update Directorate</td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> width=100 class=tdgeneral align=right><select id=kpidirid><option selected value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $dirt = html_entity_decode($row['dirtxt'], ENT_QUOTES, "ISO-8859-1");
                if(strlen($dirt)>30)
                {
                    $dirt = trim(substr($dirt,0,22))."...";
                }
                echo("<option value=".$row['dirid'].">".$dirt."</option>");
            }
        mysql_close();
        ?>
        </select> <input type=button value=" Update " id=kaup style="margin-right: 10px;" onclick="adminKPI();"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral>Project Life Cycle</td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> width=100 class=tdgeneral align=right><input type=button value=" Update " onclick="document.location.href = 'admin_kpi_plc.php?r=k&t=plc&s=0&d=0';" style="margin-right: 10px;"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral>PMS report</td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?> width=100 class=tdgeneral align=right><input type=button value="Generate" onclick="document.location.href = 'admin_kpi_pms.php?r=k&d=0';" style="margin-right: 10px;"></td>
	</tr>
    	<?php
    }
    ?>
</table>
<script type=text/javascript>
//document.getElementById('kaup').disabled = true;
</script>
<p>&nbsp;</p>
</body>

</html>
