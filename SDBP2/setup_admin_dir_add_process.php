<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Directorate Administrators ~ Add</b></h1>
<p>&nbsp;</p>
<?php
//get form data
$tki = $_POST['tkid'];
$dir = $_POST['dir'];
$dir = explode("_",$dir);

if(strlen($dir[0])>0 && strlen($dir[1])>0 && is_numeric($dir[1]))
{
    if($dir[0]=="D")
    {
        $sql = "SELECT dirid, dirtxt as txt FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dir[1];
    }
    else
    {
        $sql = "SELECT subdirid as dirid, subtxt as txt FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$dir[1];
    }
    include("inc_db_con.php");
        $dirsub = mysql_fetch_array($rs);
    mysql_close();

//validate in timekeep
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_menu_modules_users u, assist_".$cmpcode."_timekeep t WHERE u.usrmodref = '".$modver."' AND u.usrtkid = t.tkid AND t.tkstatus = 1 AND t.tkid = '".$tki."' ";
$sql.= " AND t.tkid NOT IN (SELECT a.tkid FROM assist_".$cmpcode."_".$modref."_list_admins a WHERE a.yn = 'Y' AND ref = ".$dir[1]." AND ";
if($dir[0]=="D") {
    $sql.= " a.type = 'DIR')";
} else {
    $sql.= " a.type = 'SUB')";
}
$sql.= " ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
    $row = mysql_fetch_array($rs);
mysql_close();
    $value = $row['tkname']." ".$row['tksurname'];

if($rnum == 1)
{
//if exists:
    //insert into list_admins
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_admins SET ";
    $sql.= "  tkid = '".$tki."'";
    $sql.= ", yn = 'Y'";
    if($dir[0]=="D") {
        $sql.= ", type = 'DIR'";
        $logtype = "SADIR";
    } else {
        $sql.= ", type = 'SUB'";
        $logtype = "SASUB";
    }
    $sql.= ", ref = ".$dir[1];
    include("inc_db_con.php");
        $aid = mysql_insert_id();
$tsql = $sql;
    //insert into sdbip log
        $logref = $aid;
        $logaction = "Added directorate administrator '".$value."' to ".$txt.".";
        $logdisplay = "Y";
        include("inc_log.php");
    //insert into transaction log
$told = "";
$trans = $logaction;
include("inc_transaction_log.php");
    //display result
    ?>
    <p>New Administrator '<?php echo($value); ?>' has been successfully added to <?php echo($dirsub['txt']); ?>.</p>
    <p>&nbsp;</p>
    <?php
    $urlback = "setup_admin_dir_config.php?d=".$dirsub['dirid'];
    include("inc_goback.php");
    ?>
<?php
}
else
{
//else
    //display error
        ?>
    <h3>ERROR!</h3>
        <p>An error has occurred (Err: SDBIP27).<Br>It is possible that the user you selected already has access to the (sub-)directorate in question.<br>&nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_admin_dir_add.php?d=".$dirsub['dirid'];
        include("inc_goback.php");
        ?>
        <?php
}


}
else    //error with dir
{
    ?>
    <h3>ERROR!</h3>
    <p>An error has occurred.  Please go back and try again.</p>
        <?php
        $urlback = "setup_admin_dir.php";
        include("inc_goback.php");
        ?>

    <?php
}
?>

<p>&nbsp;</p>
</body>
</html>
