<?php
    include("inc_ignite.php");
    $src = $_GET['r'];
    $dirid = $_GET['d'];
    $subid = $_GET['s'];
    switch($src)
    {
        case "d":
            if(strlen($dirid)>0 && is_numeric($dirid) && $dirid > 0)
            {
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dirid;
                include("inc_db_con.php");
                    if(mysql_num_rows($rs)==0)
                    {
                        die("ERROR! No Directorate Found.");
                    }
                    else
                    {
                        $dir = mysql_fetch_array($rs);
                    }
                mysql_close($con);
                $dirsub = array();
                $subids = array();
                $sql= "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid." ORDER BY subsort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $dirsub[] = $row;
                        $subids[] = $row['subid'];
                    }
                mysql_close($con);
            }
            else
            {
                die("ERROR! Invalid Directorate ID.");
            }
            break;
        case "k":
            $sql = "SELECT d.dirtxt, s.* FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= " WHERE d.diryn = 'Y' AND s.subyn = 'Y' AND s.subdirid = d.dirid ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $dirsub[] = $row;
                    $subids[] = $row['subid'];
                }
            mysql_close($con);
            break;
        case "s":
            if(strlen($subid)>0 && is_numeric($subid) && $subid > 0)
            {
                $dirsub = array();
                $subids = array();
                $sql= "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subid = ".$subid." ORDER BY subsort";
                include("inc_db_con.php");
                    if(mysql_num_rows($rs)==0)
                    {
                        die("ERROR!  No Sub-Directorate Found.");
                    }
                    else
                    {
                        $row = mysql_fetch_array($rs);
                        $dirsub[0] = $row;
                        $subids[0] = $row['subid'];
                    }
                mysql_close($con);
            }
            else
            {
                die("ERROR!");
            }
            break;
        default:
            die("ERROR");
            break;
        
    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 1px solid #ababab;
    }
</style>
<script type=text/javascript>
function filt(me) {
    if(me == "d")
    {
        document.getElementById('sf').disabled = true;
        document.getElementById('sfl').disabled = true;
        document.getElementById('df').disabled = false;
    }
    else
    {
        document.getElementById('df').disabled = true;
        document.getElementById('sf').disabled = false;
        document.getElementById('sfl').disabled = false;
    }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin - PMS Report</b></h1>
<form name=kpireport method=post action=admin_kpi_pms_process.php>
<input type=hidden name=r value=<?php echo($src); ?>>
<input type=hidden name=d value=<?php echo($dirid); ?>>
<input type=hidden name=s value=<?php echo($subid); ?>>
<h3 class=fc>1. Select the type of filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" width=500 style="border-width: 0px; border-color: #ffffff;">
	<tr>
		<td class="tdgeneral" style="border-width: 0px; border-color: #ffffff;" valign=top><input type=radio name=filter value=subdir id=s onclick="filt('s');"> <label for=s>Sub-Directorate</label></td>
	</tr>
        <tr>
            <td class="tdgeneral" style="border-width: 0px; border-color: #ffffff;" valign=top><input type=radio checked name=filter value=driver id=d onclick="filt('d');"> <label for=d>Program Driver</label></td>
        </tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" width=600>
	<tr>
		<td class="tdgeneral" valign=top height=25><b>Sub-Directorate:</b></td>
		<td class="tdgeneral" valign=top colspan=4>
		<?php if(count($dirsub)>1) {?>
        <select name=subdir id=sf>
            <option selected value=ALL>All Sub-Directorates</option>
            <?php
                foreach($dirsub as $sub)
                {
                    if($src == "k")
                    {
                        echo("<option value=".$sub['subid'].">".$sub['dirtxt']." - ".$sub['subtxt']."</option>");
                    }
                    else
                    {
                        echo("<option value=".$sub['subid'].">".$sub['subtxt']."</option>");
                    }
                }
            ?>
        </select><label for=sf id=sfl> </label>
        <?php } else { //count dirsub ?>
        <input type=hidden name=subdir id=sf value="<?php echo($dirsub[0]['subid']); ?>"> <label for=sf id=sfl><?php echo($dirsub[0]['subtxt']); ?></label>
        <?php } //count dirsub ?>
        </td>
	</tr>
        <tr>
            <td class="tdgeneral" valign=top><b>Program Driver:</b></td>
        <?php
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
        		$kpisub = implode(",",$subids);
                echo("<select name=driver id=df><option selected value=ALL>All</option>");
        		$sql = "SELECT kpidriver, count(kpiid) as ki FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiyn = 'Y' AND kpisubid IN (".$kpisub.") GROUP BY kpidriver ORDER BY kpidriver";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=\"".$row['kpidriver']."\">".$row['kpidriver']." (".$row['ki']." kpi");
                        if($row['ki']>1) { echo("s"); }
                        echo(")</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select>");
                ?></td>
        </tr>
</table>
</div>
<h3 class=fc>3. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<script type=text/javascript>
filt('d');
</script>
<p>&nbsp;</p>

</body>

</html>
