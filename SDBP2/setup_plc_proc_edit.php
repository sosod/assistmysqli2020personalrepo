<?php
    include("inc_ignite.php");
    
    include("inc_admin.php");
    
    $id = $_POST['id'];
    $act = $_POST['act'];
    $val = $_POST['val'];
    $req = $_POST['req'];
    $days1 = $_POST['days1'];
    $days2 = $_POST['days2'];
    $days3 = $_POST['days3'];
    $days4 = $_POST['days4'];

if(is_numeric($id) && strlen($id)>0 && $id > 0 && strlen($act)>0 && strlen($val)>0)
{
    
    if($act == "save")
    {
        $val = htmlentities($val,ENT_QUOTES,"ISO-8859-1");
        if($req != "R") { $req = "Y"; }
        if(strlen($days1)==0 || !is_numeric($days1)) { $days1 = 1; }
        if(strlen($days2)==0 || !is_numeric($days2)) { $days2 = 1; }
        if(strlen($days3)==0 || !is_numeric($days3)) { $days3 = 1; }
        if(strlen($days4)==0 || !is_numeric($days4)) { $days4 = 1; }
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_plcphases_proc SET value = '".$val."', yn = '".$req."', days1 = ".$days1.", days2 = ".$days2.", days3 = ".$days3.", days4 = ".$days4." WHERE id = ".$id;
        include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Updated PLC Procurement Phase ".$id;
        include("inc_transaction_log.php");
    }
    else
    {
        if($act == "del")
        {
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_plcphases_proc SET yn = 'N' WHERE id = ".$id;
            include("inc_db_con.php");
            $tsql = $sql;
            $told = "";
            $trans = "Deleted PLC Procurement Phase ".$id;
            include("inc_transaction_log.php");
        }
    }
    echo("<script type=text/javascript>document.location.href = 'setup_plc_proc.php';</script>");
}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function editPhase(act) {
    if(act.length==0) { act = "save"; }
    var err = "Y";
    
    if(act == "del")
    {
        if(confirm("Are you sure you wish to delete this phase?\nThis will impact all Project Life Cycles that use this phase.")==true)
        {
            err = "N";
        }
    }
    else
    {
        err = "N";
    }
    if(err == "N")
    {
        document.getElementById('act').value = act;
        document.forms['edit'].submit();
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdheaderlend { border-bottom: 1px solid #ababab; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Project Life Cycle</b></h1>
<h2 class=fc>Setup PRocurement Phases ~ Edit</h2>
<form name=edit method=post action=setup_plc_proc_edit.php>
<input type=hidden name=act id=act>
<?php

if(is_numeric($id) && strlen($id)>0 && $id > 0)
{
    $phase = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_proc WHERE id = ".$id;
    include("inc_db_con.php");
        $phase = mysql_fetch_array($rs);
    mysql_close();

    if(count($phase)>0)
    {
        ?>
        <table cellpadding=3 cellspacing=0 width=570>
            <tr>
                <td width=120 class=tdheaderl height=27>Ref:</td>
                <td class=tdgeneral><?php echo($phase['id']); ?><input type=hidden name=id value="<?php echo($id); ?>"></td>
            </tr>
            <tr>
                <td class=tdheaderl height=27>Original Phase:</td>
                <td class=tdgeneral><?php echo($phase['value']); ?></td>
            </tr>
            <tr>
                <td class=tdheaderl height=27>New Phase:</td>
                <td class=tdgeneral><input type=text name=val maxlength=150 size=50 value="<?php echo($phase['value']); ?>"></td>
            </tr>
            <tr>
                <td class=tdheaderl height=27>Required?:</td>
                <td class=tdgeneral><select name=req><option <?php if($phase['yn']=="R") { echo("selected"); } ?> value=R>Yes</option><option <?php if($phase['yn']!="R") { echo("selected"); } ?> value=Y>No</option></select></td>
            </tr>
            <?php if($plcsetup[4]=="Y") { ?>
            <tr>
                <td class=tdheaderl height=27>Category 1 Days:</td>
                <td class=tdgeneral><input type=text name=days1 size=5 value="<?php echo($phase['days1']); ?>"> <i>(Numbers only)</i></td>
            </tr>
            <tr>
                <td class=tdheaderl height=27>Category 2 Days:</td>
                <td class=tdgeneral><input type=text name=days2 size=5 value="<?php echo($phase['days2']); ?>"> <i>(Numbers only)</i></td>
            </tr>
            <tr>
                <td class=tdheaderl height=27>Category 3 Days:</td>
                <td class=tdgeneral><input type=text name=days3 size=5 value="<?php echo($phase['days3']); ?>"> <i>(Numbers only)</i></td>
            </tr>
            <tr>
                <td class=tdheaderl height=27 style="border-bottom-color: #ababab;">Category 4 Days:</td>
                <td class=tdgeneral><input type=text name=days4 size=5 value="<?php echo($phase['days4']); ?>"> <i>(Numbers only)</i></td>
            </tr>
            <?php } else { ?>
            <tr>
                <td class=tdheaderl height=27 style="border-bottom-color: #ababab;">Days:</td>
                <td class=tdgeneral><input type=text name=days1 size=5 value="<?php echo($phase['days1']); ?>"> <i>(Numbers only)</i></td>
            </tr>
            <?php } ?>
            <tr>
                <td class=tdgeneral>&nbsp;</td><td class=tdgeneral><input type=button value="Save changes" onclick="editPhase('save');"> <input type=button value="Delete" onclick="editPhase('del');"> <input type=button value="Cancel" onclick="document.location.href = 'setup_plc_proc.php';"></td>
            </tr>
        </table>
        <?php
    }
    else
    {
        echo("<p>An error has occurred.  Please go back and try again.</p>");
    }
}
else
{
    echo("<p>An error has occurred. Please go back and try again.</p>");
}
?>
<?php
$urlback = "setup_plc_proc.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
