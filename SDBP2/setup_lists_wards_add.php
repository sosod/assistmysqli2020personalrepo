<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var valid8 = "true";
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
        alert("Please complete the missing field as indicated.");
    }
    else
    {
        if(toUpperCase(val) == "ALL")
        {
            document.getElementById('val').className = 'reqtext';
            document.getElementById('val').focus();
            valid8 = "false";
            alert("Please note: 'ALL' is already on the system and can not be added again.");
        }
        else
        {
            document.getElementById('val').className = 'reqmet';
        }
    }
    if(valid8 == "false")
    {
        return false;
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Wards ~ Add</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=setup_lists_wards_add_process.php onsubmit="return Validate(this);">
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=110>Ward:</td>
		<td class=tdgeneral valign=top><input type=text size=30 maxlength=30 name=val id=val> <span style="font-size: 7.5pt;">(max 30 characters)</span></td>
		<td class=tdgeneral width=60>*required</td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral colspan=2 valign=top><input type=submit value=" Add "></td>
    </tr>
</table>
</form>
<?php
$urlback = "setup_lists_wards.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
