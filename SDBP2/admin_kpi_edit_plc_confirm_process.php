<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Edit KPI</b></h1>
<p>&nbsp;</p>
<?php
$err = "N";
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
//FORM DETAILS
$kpiid = $_POST['kpiid'];
$plcid = $_POST['plcid'];
$time = $_POST['time'];
$target = $_POST['target'];
$actual = $_POST['actual'];
$krtargettypeid = 2;
$tid = $_POST['tmpid'];
$tmpid = explode(",",$tid);
$appreq = $_POST['appreq'];
$tbl = $_POST['tbl'];

//echo("<P>".$plcid."-".$tbl);

//IF APPREQ == "N"  => Approval not required
if($appreq=="N")
{
//    echo("approval not required");
    if(strlen($plcid)>0 && is_numeric($plcid) && $plcid > 0)
    {
    //MOVE OLD DETAILS FROM PLC TO PLC_OLD
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_old (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,oldtkid,olddate) ";
        $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as oldtid, ".$today." as olddt FROM assist_".$cmpcode."_".$modref."_kpi_plc p2 WHERE plcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //MOVE NEW DETAILS FROM PLC_TEMP TO PLC
        $plcnew = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'P' ORDER BY tmpid DESC";
//echo("<P>".$sql);
        include("inc_db_con.php");
            $plcnew = mysql_fetch_array($rs);
        mysql_close();
        
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc SET";
        $sql.= "  plcname = '".$plcnew['plcname']."'";
        $sql.= ", plcdescription = '".$plcnew['plcdescription']."'";
        $sql.= ", plcstartdate = ".$plcnew['plcstartdate'];
        $sql.= ", plcenddate = ".$plcnew['plcenddate'];
        $sql.= ", plcproccate = ".$plcnew['plcproccate'];
        $sql.= ", plcyn = 'Y'";
        $sql.= " WHERE plcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_temp SET tmpyn = 'A' WHERE tmpid = ".$plcnew['tmpid'];
//echo("<P>".$sql);
        include("inc_db_con.php");
    //MOVE OLD PHASES TO PHASES_OLD
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases_old (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,oldtkid,olddate) ";
        $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //DELETE OLD PHASES
        $sql = "DELETE FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //MOVE NEW PHASES FROM PHASES_TEMP TO PHASES
        foreach($tmpid as $temp)
        {
            $phase = array();
            if(strlen($temp)>0 && is_numeric($temp) && $temp > 0)
            {
                //GET TEMP DETAILS
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp WHERE tmpid = ".$temp;
//echo("<P>".$sql);
                include("inc_db_con.php");
                    $phase = mysql_fetch_array($rs);
                mysql_close();
                //UPDATE PHASES
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases SET";
                $sql.= "  ppplcid = ".$plcid;
                $sql.= ", pptype = '".$phase['pptype']."'";
                $sql.= ", ppphaseid = ".$phase['ppphaseid'];
                $sql.= ", ppsort = ".$phase['ppsort'];
                $sql.= ", ppdays = ".$phase['ppdays'];
                $sql.= ", ppstartdate = ".$phase['ppstartdate'];
                $sql.= ", ppenddate = ".$phase['ppenddate'];
                $sql.= ", pptarget = ".$phase['pptarget'];
                $sql.= ", ppdone = '".$phase['ppdone']."'";
                $sql.= ", ppdonedate = ".$phase['ppdonedate'];
//echo("<P>".$sql);
                include("inc_db_con.php");
                //CHANGE STATUS
                $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp SET tmpyn = 'A' WHERE tmpid = ".$temp;
//echo("<P>".$sql);
                include("inc_db_con.php");
            }
        }
    //UPDATE KPI_RESULTS
/*        echo("<P>TIME-");
        print_r($time);
        echo("<P>TARGET-");
        print_r($target);
        echo("<P>ACTUAL-");
        print_r($actual);
*/        if(strlen($kpiid) > 0 && is_numeric($kpiid))
        {
            for($i=0;$i<count($time);$i++)
            {
                $t = $time[$i];
                $g = $target[$i];
                $a = $actual[$i];
                if(strlen($g)>0 && strlen($a)>0 && is_numeric($g) && is_numeric($a))
                {
                    $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_result SET ";
                    $sql.= "  krtarget = ".$g;
                    $sql.= ", kractual = ".$a;
                    $sql.= " WHERE krkpiid = ".$kpiid." AND krtimeid = ".$t;
//echo("<P>".$sql);
                    include("inc_db_con.php");
$tsql = $sql;
$told = "";
$trans = "Updated result of KPI ".$kpiid." for time ".$t." due to update to PLC ".$plcid;
include("inc_transaction_log.php");
                }
            }
        }
        else
        {
            echo("<p>Error on kpiid validation - can't update results");
            $err = "Y";
        }
    }
    else
    {
        echo("<P>Error on plcid validation");
        $err = "Y";
    }
    
    if($err == "N")
    {
        echo("<p>PLC successfullly updated.");
    }
}
else //ELSE IF APPREQ == "Y" => Approval is required
{
//    echo("approval required");
    if($tbl == "current")
    {
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_temp (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,tmptkid,tmpdate,tmpyn) ";
        $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as tmptid, ".$today." as tmpdt, 'P' as tyn FROM assist_".$cmpcode."_".$modref."_kpi_plc p2 WHERE plcid = ".$plcid;
        include("inc_db_con.php");
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,tmptkid,tmpdate,tmpyn) ";
        $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte, 'P' as tyn FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
        include("inc_db_con.php");
    }
    //CHANGE TEMP STATUS TO Y
        $plcnew = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'P' ORDER BY tmpid DESC";
//echo("<P>".$sql);
        include("inc_db_con.php");
            $plcnew = mysql_fetch_array($rs);
        mysql_close();
if(strlen($plcnew['tmpid'])>0 && is_numeric($plcnew['tmpid']))
{
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_temp SET tmpyn = 'Y' WHERE tmpid = ".$plcnew['tmpid'];
//echo("<P>".$sql);
        include("inc_db_con.php");
}
    //SEND AN EMAIL TO KPI ADMIN
    if($err == "N")
    {
        $tmpmnr = 0;
        $tmpplc = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_temp WHERE tmpyn = 'Y' AND tmpid = ".$plcnew['tmpid'];
        include("inc_db_con.php");
            $tmpmnr = mysql_num_rows($rs);
            $tmpplc =  mysql_fetch_array($rs);
        mysql_close($con);
        if($tmpmnr>0)
        {
            $ref = $tmpplc['plcid'];
            $plcn = html_entity_decode($tmpplc['plcname'], ENT_QUOTES, "ISO-8859-1");
            $plckpi = $tmpplc['plckpiid'];
            if(strlen($plckpi)>0 && is_numeric($plckpi))
            {
                $kpirow = array();
                $sql2 = "SELECT k.kpivalue, k.kpiid, s.subtxt, d.dirtxt, d.dirid, s.subid FROM assist_".$cmpcode."_".$modref."_kpi k, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s WHERE k.kpiid = ".$plckpi." AND k.kpisubid = s.subid AND s.subdirid = d.dirid AND d.diryn = 'Y' AND s.subyn = 'Y' AND k.kpiyn = 'Y'";
                include("inc_db_con2.php");
                    $kpirow = mysql_fetch_array($rs2);
                mysql_close($con2);

                $plcdir = html_entity_decode($kpirow['dirtxt'], ENT_QUOTES, "ISO-8859-1");
                $plcsub = html_entity_decode($kpirow['subtxt'], ENT_QUOTES, "ISO-8859-1");
                $plckpiv = html_entity_decode($kpirow['kpivalue'], ENT_QUOTES, "ISO-8859-1");

                $sql = "SELECT DISTINCT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid <> '0000' AND tkstatus = 1 AND tkid IN ";
                $sql.= "(SELECT tkid FROM assist_".$cmpcode."_sdp09_list_admins WHERE yn = 'Y' AND type= 'KPI');";
                include("inc_db_con.php");
                $to = "";
                while($row = mysql_fetch_array($rs))
                {
                    $tkem = $row['tkemail'];
                    if(strlen($to)>0 && strlen($tkem)>0) { $to.=","; }
                    $to.=$tkem;
                }
                mysql_close($con);
        $from = "no-reply@ignite4u.co.za";
        $subject = "Updated Project Life Cycle";
        $message = "A Project Life Cycle has been updated on SDBIP ".$modtxt." and requires approval.".chr(10);
        $message.= "Please log onto Ignite Assist to review the updated PLC.".chr(10).chr(10);
        $message.= "The PLC details are as follows:".chr(10);
        $message.= "Reference: ".$ref.chr(10);
        $message.= "Directorate: ".$plcdir.chr(10);
        $message.= "Sub-Directorate: ".$plcsub.chr(10);
        $message.= "PLC Name: ".$plcn.chr(10);
        $message.= "KPI: ".$plckpiv." (KPI Ref: ".$plckpi.")".chr(10);
        $message.= "Updated by: ".$tkname;
        
        $header = "From: ".$from."\r\nReply-to: ".$from;
        mail($to,$subject,$message,$header);
//echo("<P>".$to);
//echo("<P>".$message);
        echo("<p>KPI Administrators have been notified.");

            }
        }
    }
}
$tsql = "";
$told = "";
$trans = "Edited PLC ".$plcid." for KPI ".$kpiid;
include("inc_transaction_log.php");

if($err == "N")
{
    if($src == "s")
    {
        $urlback = "admin_sub.php?s=".$subid;
        include("inc_goback.php");
    }
    else
    {
        if($src=="d")
        {
            $urlback = "admin_dir.php?d=".$dirid;
            include("inc_goback.php");
        }
        else
        {
            if($dir == "ALL")
            {
                $urlback = "admin.php";
                include("inc_goback.php");
            }
            else
            {
                $urlback = "admin_dskpi.php?d=".$dir;
                include("inc_goback.php");
            }
        }
    }
}
else
{
    include("inc_goback_history.php");
}


?>
</body>

</html>
