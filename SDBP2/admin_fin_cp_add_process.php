<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>


<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin - Capital Projects ~ Add</b></h1>
<p>&nbsp;</p>
<?php
//GET DETAILS
$subid = $_POST['subid'];
$cpproject = htmlentities($_POST['cpproject'],ENT_QUOTES,"ISO-8859-1");
$cpnum = htmlentities($_POST['cpnum'],ENT_QUOTES,"ISO-8859-1");
$cpvotenum = htmlentities($_POST['cpvotenum'],ENT_QUOTES,"ISO-8859-1");
$cpidpnum = htmlentities($_POST['cpidpnum'],ENT_QUOTES,"ISO-8859-1");
$cpfundsource = htmlentities($_POST['cpfundsource'],ENT_QUOTES,"ISO-8859-1");
$wards = $_POST['wards'];
$sdate = $_POST['cpstartdate'];
$edate = $_POST['cpenddate'];

$sdate2 = explode("_",$sdate);
$edate2 = explode("_",$edate);

$cpstartdate = mktime(0,0,1,$sdate2[1],$sdate2[0],$sdate2[2]);
$cpenddate = mktime(0,0,1,$edate2[1],$edate2[0],$edate2[2]);

//CHECK SUB
if(is_numeric($subid) && $subid > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$subid." AND subyn = 'Y'";
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
    mysql_close();
}

    $time = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        while($row = mysql_fetch_array($rs))
        {
            $time[$t] = $row;
            $t++;
        }
    mysql_close();
    $budget = array();
    $b=0;
    foreach($time as $tarr)
    {
        $fld = "tid".$tarr['id'];
        $var = $_POST[$fld];
        $budget[$b]['budget'] = $var;
        $budget[$b]['id'] = $tarr['id'];
        $b++;
    }


//CHECK DETAILS
if(strlen($cpproject)>0 && $mnr>0 && $cpstartdate > 0 && $cpenddate > 0 && $cpenddate > $cpstartdate)
{
    //INSERT
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital SET";
    $sql.= "  cpnum = '".$cpnum."'";
    $sql.= ", cpsubid = ".$subid;
    $sql.= ", cpvotenum = '".$cpvotenum."'";
    $sql.= ", cpidpnum = '".$cpidpnum."'";
    $sql.= ", cpproject = '".$cpproject."'";
    $sql.= ", cpfundsource = '".$cpfundsource."'";
    $sql.= ", cpstartdate = ".$cpstartdate;
    $sql.= ", cpenddate = ".$cpenddate;
    $sql.= ", cpyn = 'Y'";
    include("inc_db_con.php");
    //GET ID
    $cpid = 0;
    $cpid = mysql_insert_id($con);
        $tsql = $sql;
        $told = "";
        $trans = "Inserted new CP ".$cpid;
        include("inc_transaction_log.php");
    //LOG
    if($cpid>0 && strlen($cpid)>0)
    {
    //FOREACH BUDGET
        foreach($budget as $res)
        {
        //INSERT
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_results SET";
            $sql.= "  crcpid = ".$cpid;
            $sql.= ", crbudget = ".$res['budget'];
            $sql.= ", cractual = 0, craccytd = 0, crtotytd = 0";
            $sql.= ", crtimeid = ".$res['id'];
            include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Inserted Budget for new CP ".$cpid." with timeid ".$res['id'];
        include("inc_transaction_log.php");
        //LOG
        }
    //FOREACH WARDS
        foreach($wards as $wrd)
        {
        //INSERT
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_wards SET";
            $sql.= "  cwcpid = ".$cpid;
            $sql.= ", cwwardsid = ".$wrd;
            $sql.= ", cwyn = 'Y'";
            include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Added ward to new CP ".$cpid;
        include("inc_transaction_log.php");
        //LOG
        }
    }

    echo("<p>Capital Project \"".$cpproject."\" successfully added with reference ".$cpid.".</p>");
    $urlback = "admin_fin.php";
    include("inc_goback.php");
}
else
{
    echo("<p>An error has occurred.  Please go back and try again.</p>");
    echo("<p style=\"margin-top: 30px\"><a href=# onclick=\"history.back();\"><img src=/pics/tri_left.gif align=absmiddle border=0><span style=\"text-decoration: none;\"> </span>Go Back</a></p>");
}

?>
<p>&nbsp;</p>
</body>
</html>
