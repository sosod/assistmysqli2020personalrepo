<?php

/*
 * Read README file first.
 *
 * This example shows how to create a simple bar chart with a few configuration
 * directives.
 * The values are from http://www.globalissues.org/article/26/poverty-facts-and-stats
 */

// Require necessary files
require("../SQL/lib/AmBarChart.php");

// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js and AmCharts.js (only needed for pie legend)
AmChart::$jsPath = "lib/AmCharts.js";
AmChart::$jQueryPath = "lib/jquery.js";
// Tell AmChart to load jQuery if you don't already use it on your site.
AmChart::$loadJQuery = true;

// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmBarChart("myBarChart");

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
$chart->setTitle("<b>".$cmpname."</b>");

// Add a label to describe the X values (inside the chart).
$chart->addLabel("% of total KPIs measured", 5, 15);







foreach($tarr as $tim)
{
    $s = $tim['sort'];
    //setup arrays
    $kpi[$s]['g'] = 0;
    $kpi[$s]['o'] = 0;
    $kpi[$s]['r'] = 0;
    $kpi[$s]['x'] = 0;
    $kpi[$s]['tot'] = 0;
    // Add all values for the X axis
    //if($wf <= $s && $wt >= $s) { $chart->addSerie($s, date("M Y",$tim['eval'])); }
}
$s = 0;
//GENERATE SQL
$sql = "SELECT k.kpiid, k.kpicalctype FROM";
$sql.= "  assist_".$cmpcode."_".$modref."_kpi k";
$sql.= " WHERE k.kpiyn = 'Y'";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $kid = $row['kpiid'];
        $kct = $row['kpicalctype'];
//        echo("<P>".$kid."-".$kct);
        $sql2 = "SELECT r.*, t.sort FROM assist_".$cmpcode."_".$modref."_kpi_result r, assist_".$cmpcode."_".$modref."_list_time t ";
        $sql2.= " WHERE r.krkpiid = ".$kid." AND t.sort >= ".$wf." AND t.sort <= ".$wt." AND t.yn = 'Y' AND r.krtimeid = t.id ORDER BY t.sort ASC";
        include("inc_db_con2.php");
        while($row2 = mysql_fetch_array($rs2))
        {
            $s = $row2['sort'];
//            echo("<br>".$s);
            switch($kct)
            {
                case "ACC":
                    break;
                case "CO":
                    break;
                case "STD":
                    $krt = $row2['krtarget'];
                    $kra = $row2['kractual'];
                    if($kra == 0 && $krt == 0)
                    {
                        $krr = "x";
                    }
                    else
                    {
                        if($kra >= $krt)
                        {
                            $krr = "g";
                        }
                        else
                        {
                            $calc = $kra/$krt;
                            if($calc < 0.75)
                            {
                                $krr = "r";
                            }
                            else
                            {
                                $krr = "o";
                            }
                        }
                    }
                    $kpi[$s][$krr]++;
                    break;
                case "ZERO":
                    $krt = $row2['krtarget'];
                    $kra = $row2['kractual'];
                        if($kra <= $krt)
                        {
                            $krr = "g";
                        }
                        else
                        {
                            $krr = "r";
                        }
                        $kpi[$s][$krr]++;
                    break;
                default:
                    break;
            }
//            echo($krr);
        }
        mysql_close($con2);
    }
mysql_close;
//CALCULATE RESULTS








?>
<table cellpadding=5 cellspacing=0 style="border: 0px solid #ffffff;" width=100%>
    <tr>
        <td class=tdgeneral align=center style="border: 0px solid #ffffff;">
<?php

$met = array();
$notmet = array();
$almost = array();

// Add all values for the X axis
for($ct=$wf;$ct<=$wt;$ct++)
{
    $chart->addSerie($ct, date("M-y",mktime(12,0,0,$ct+6,1,2009)));
    $kpitot[$ct] = $kpi[$ct]['g'] + $kpi[$ct]['o'] + $kpi[$ct]['r'];
    $met[$ct] = round(($kpi[$ct]['g'] / $kpitot[$ct]) * 100,0);
    $notmet[$ct] = round(($kpi[$ct]['r'] / $kpitot[$ct]) * 100,0);
    if($met[$ct] + $notmet[$ct] > 100) { $met[$ct] = 100 - $notmet[$ct]; }
    $almost[$ct] = 100 - $met[$ct] - $notmet[$ct];
}

/*
// Define graphs data
$met = array(
	"1" => ($kpi[1]['g'] / $kpitot[1]),
	"2" => ($kpi[2]['g'] / $kpitot[2]),
	"3" => ($kpi[3]['g'] / $kpitot[3]),
	"4" => ($kpi[4]['g'] / $kpitot[4]),
	"5" => ($kpi[5]['g'] / $kpitot[5]),
	"6" => ($kpi[6]['g'] / $kpitot[6]),
	"7" => ($kpi[7]['g'] / $kpitot[7]),
	"8" => ($kpi[8]['g'] / $kpitot[8]),
	"9" => ($kpi[9]['g'] / $kpitot[9]),
	"10" => ($kpi[10]['g'] / $kpitot[10]),
	"11" => ($kpi[11]['g'] / $kpitot[11]),
	"12" => ($kpi[12]['g'] / $kpitot[12])
);
$notmet = array(
	"1" => ($kpi[1]['r'] / $kpitot[1]),
	"2" => ($kpi[2]['r'] / $kpitot[2]),
	"3" => ($kpi[3]['r'] / $kpitot[3]),
	"4" => ($kpi[4]['r'] / $kpitot[4]),
	"5" => ($kpi[5]['r'] / $kpitot[5]),
	"6" => ($kpi[6]['r'] / $kpitot[6]),
	"7" => ($kpi[7]['r'] / $kpitot[7]),
	"8" => ($kpi[8]['r'] / $kpitot[8]),
	"9" => ($kpi[9]['r'] / $kpitot[9]),
	"10" => ($kpi[10]['r'] / $kpitot[10]),
	"11" => ($kpi[11]['r'] / $kpitot[11]),
	"12" => ($kpi[12]['r'] / $kpitot[12])
);
$almost = array(
	"1" => ($kpi[1]['o'] / $kpitot[1]),
	"2" => ($kpi[2]['o'] / $kpitot[2]),
	"3" => ($kpi[3]['o'] / $kpitot[3]),
	"4" => ($kpi[4]['o'] / $kpitot[4]),
	"5" => ($kpi[5]['o'] / $kpitot[5]),
	"6" => ($kpi[6]['o'] / $kpitot[6]),
	"7" => ($kpi[7]['o'] / $kpitot[7]),
	"8" => ($kpi[8]['o'] / $kpitot[8]),
	"9" => ($kpi[9]['o'] / $kpitot[9]),
	"10" => ($kpi[10]['o'] / $kpitot[10]),
	"11" => ($kpi[11]['o'] / $kpitot[11]),
	"12" => ($kpi[12]['o'] / $kpitot[12])
);*/
// Add graphs
$chart->addGraph("notmet", "KPIs Not Met", $notmet, array("color" => "#cc0001"));
$chart->addGraph("almost", "KPIs Almost Met", $almost, array("color" => "#fe9900"));
$chart->addGraph("met", "KPIs Met", $met, array("color" => "#009900"));

if(60 + (($wt-$wf+1)*45) < 350)
{
    $width = 350;
    $diff = round((350 - 60 - (($wt-$wf+1)*45)) / 2);
    $left = $diff + 13;
    $right = $diff - 12;
}
else
{
    $width = 60 + (($wt-$wf+1)*45);
    $left = 35;
    $right = 10;
}
if($left<35) {     $left = 35; }
if($right < 10) {    $right = 10; }

//SET CONFIG
$chart->setConfigAll(array(
"width" => $width,
"height" => 400,
//"depth" => 5,
//"angle" => 30,
"plot_area.margins.top" => 40,
"plot_area.margins.right" => $right,
"plot_area.margins.left" => $left,
"background.border_alpha" => 15,
"column.type" => "100% stacked",
"legend.y" => "90%",
"legend.x" => "2%",
"legend.width" => "96%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 8,
"legend.align" => "center",
"balloon.enabled" => "false",
//"balloon.alpha" => 80,
//"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125;%&#93;&#93;>",
"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>",
"column.data_labels_text_color" => "#ffffff",
"column.width" => 75//,
//"column.spacing" => 20
//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>",

));

// Print the code
echo html_entity_decode($chart->getCode());
?>
        </td>
    </tr>
</table>
<p>
<?php
//print_r($met);
?>
