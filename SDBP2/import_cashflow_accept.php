<?php
    include("inc_ignite.php");
//    include("../inc_codehelper.php");
    $fileloc = $_POST['floc'];
                        $mons = array();
                        for($m=1;$m<13;$m++)
                        {
                            $m2 = date("F",mktime(12,0,0,$m+6,1,2009));
                            $mons[$m2] = $m;
                        }
    $impcheck = $_POST['import'];
foreach($impcheck as $i)
{
    $import[$i] = "Y";
}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import ~ Monthly Cashflow<input type=hidden id=lab></b></h1>
<center>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script>
<?php
$err = "N";
$p=0;
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = "cashflow_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}
else
{
    //echo("<script type=text/javascript>document.getElementById('floc').value = '".$fileloc."';</script>");
}

if($err == "N")
{
            $file = fopen($fileloc,"r");
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <form id=accept method=post action=import_cashflow_accept.php><input type=hidden name=floc value="<?php echo($fileloc); ?>">
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader>Ref</td>
                        <td class=tdheader>Directorate</td>
                        <td class=tdheader>Sub-Directorate</td>
                        <td class=tdheader>Line Item</td>
                        <td class=tdheader>Vote Number</td>
                        <td class=tdheader>Month</td>
                        <td class=tdheader>Revenue</td>
                        <td class=tdheader>Opex</td>
                        <td class=tdheader>Capex</td>
                    </tr>
                    <?php
                    $p2 = 100 / count($data);
                    $id = 0;
                    foreach($data as $idata)
                    {
set_time_limit(30);
                        $id++;
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        if($import[$id]=="Y")
                        {
                        $dir = trim($idata[0]);
                        $dirid = 0;
                        $dirrow = array();
                        $direrr = "N";
                        $dir = htmlentities($dir,ENT_QUOTES,"ISO-8859-1");
                        $dir = substr($dir,0,100);
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirtxt = '".$dir."' AND diryn = 'Y'";
                        include("inc_db_con.php");
                            if(mysql_num_rows($rs)>0)
                            {
                                $dirrow = mysql_fetch_array($rs);
                                $dirid = $dirrow['dirid'];
                                if(strlen($dirid)>0 && is_numeric($dirid) && $dirid > 0)
                                {
                                    $direrr = "N";
                                }
                                else
                                {
                                    $direrr = "Y";
                                }
                            }
                            else
                            {
                                $direrr = "Y";
                            }
                        mysql_close();
                        $sub = trim($idata[1]);
                        $sid = 0;
                        $srow = array();
                        if($direrr == "N" && $dirid > 0)
                        {
                            $sub = htmlentities($sub,ENT_QUOTES,"ISO-8859-1");
                            $sub = substr($sub,0,100);
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subtxt = '".$sub."' AND subyn = 'Y' AND subdirid = ".$dirid;
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs)>0)
                                {
                                    $srow = mysql_fetch_array($rs);
                                    $sid = $srow['subid'];
                                }
                                else
                                {
                                    $sql2 = "INSERT INTO assist_".$cmpcode."_".$modref."_dirsub (subtxt,subyn,subdirid,subsort,subhead) ";
                                    $sql2.= "SELECT '".$sub."' as st, 'Y' as syn, ".$dirid." as sd, max(subsort)+1 as ss, 'N' as sh FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subyn = 'Y'";
                                    include("inc_db_con2.php");
                                    $sid = mysql_insert_id($con2);
                                }
                            mysql_close();
                        }
                        $line = trim($idata[2]);
                        if(strlen($line)==0) { $line = "Cashflow"; }
                        $vote = trim($idata[3]);
                        $lid = "NEW";
                        $lrow = array();
                        if($direrr == "N" && $dirid > 0 && $sid > 0)
                        {
                            //$line = htmlentities($line,ENT_QUOTES,"ISO-8859-1");
                            $line = code($line);
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_lineitems WHERE linetype = 'CF' AND lineyn = 'Y' AND linevalue = '".$line."' AND linesubid = ".$sid." AND linevote = '".$vote."'";
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs)>0)
                                {
                                    $lrow = mysql_fetch_array($rs);
                                    $lid = $lrow['lineid'];
                                }
                                else
                                {
                                    $ls = 0;
                                    $sql2 = "SELECT max(linesort)+1 as ls FROM assist_".$cmpcode."_".$modref."_finance_lineitems WHERE linesubid = ".$sid." AND lineyn = 'Y'";
                                    include("inc_db_con2.php");
                                            $lsrow = mysql_fetch_array($rs2);
                                            $ls = $lsrow['ls'];
                                            if(strlen($ls)==0) { $ls = 1; }
                                    mysql_close($con2);
                                    
                                    $sql2 = "INSERT INTO assist_".$cmpcode."_".$modref."_finance_lineitems (linevalue,lineyn,linesort,linetype,linesubid,linevote) ";
                                    $sql2.= "VALUES ('".$line."','Y',".$ls.",'CF',".$sid.",'".$vote."')";
                                    include("inc_db_con2.php");
                                    $lid = mysql_insert_id($con2);
                                }
                            mysql_close();
                        }
                        $month = trim($idata[4]);
                        $rev = $idata[5];
                        $opex = $idata[9];
                        $capex = $idata[13];
                        if(strlen($dirid)>0 && is_numeric($dirid) && $dirid > 0 && strlen($sid)>0 && is_numeric($sid) && $sid > 0 && strlen($lid)>0 && is_numeric($lid) && $lid > 0 && strlen($month)>0 && $direrr == "N")
                        {
                            if(strlen($rev)==0 || !is_numeric($rev)) { $rev = 0; }
                            if(strlen($opex)==0 || !is_numeric($opex)) { $opex = 0; }
                            if(strlen($capex)==0 || !is_numeric($capex)) { $capex = 0; }
                            $tid = 0;
                            $tid = $mons[$month];
                            if(strlen($tid)>0 && is_numeric($tid) && $tid > 0)
                            {
                                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_finance_cashflow (cflineid,cfyn,cftimeid,cfrev1,cfop1,cfcp1) ";
                                $sql.= "VALUES (".$lid.",'Y',".$tid.",".$rev.",".$opex.",".$capex.")";
                                include("inc_db_con.php");
                        ?>
                    <tr>
                        <td class=tdheader><?php echo($id); ?></td>
                        <td class=tdgeneral><?php echo($dir." (".$dirid.")"); ?></td>
                        <td class=tdgeneral><?php echo($sub." (".$sid.")"); ?></td>
                        <td class=tdgeneral><?php echo($line." (".$lid.")"); ?></td>
                        <td class=tdgeneral><?php echo($vote); ?></td>
                        <td class=tdgeneral><?php echo($tid); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($rev,2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($opex,2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($capex,2)); ?></td>
                    </tr>
                                    <?php
                                }
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($id); ?></td>
                        <td class=tdgeneral><?php echo($dir);?></td>
                        <td class=tdgeneral colspan=10>Invalid line (TIMEID).  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                                    }
                                    else
                                    {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($id); ?></td>
                        <td class=tdgeneral><?php echo($dir);?></td>
                        <td class=tdgeneral colspan=10>Invalid line (REQ FLDS).  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                            }
                            else
                            {
                                    ?>
                    <tr>
                        <td class=tdheaderorange><?php echo($id); ?></td>
                        <td class=tdgeneral colspan=10>Ignored as per checkbox.</td>
                    </tr>
                                    <?php
                            }
                    }
                    ?>
                </table>
                </form>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "import.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
</body>
</html>
