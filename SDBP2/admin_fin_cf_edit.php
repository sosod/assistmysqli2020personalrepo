<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var sub = me.subid.value;
    var t = me.t.value;
    t = parseInt(t);
    var valid8 = "true";
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
            document.getElementById('val').className = 'reqmet';
    }
    if(sub.length==0 || sub == "X" || sub == "dX")
    {
        document.getElementById('subid').className = 'reqtext';
        document.getElementById('subid').value = "X";
        if(valid8=="true")
        {
            document.getElementById('subid').focus();
        }
        valid8 = "false";
    }
    else
    {
            document.getElementById('subid').className = 'reqmet';
    }
    var target;
    var targ;
    var t2 = 0;
    var berr = "N";
    for(t2=0;t2<t;t2++)
    {
        targ = "r"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
        targ = "o"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
        targ = "c"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
    }
    if(valid8 == "false")
    {
        var errtxt = "Please complete all missing field(s) as indicated."
        if(berr == "Y")
        {
            errtxt = errtxt+"\nNote: Only numbers are allowed for budget figures.";
        }
        alert(errtxt);
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

function deleteLine(l) {
    if(!isNaN(parseInt(l)))
    {
        if(parseInt(l)>0)
        {
            if(confirm("Are you sure you wish to delete this line item?")==true)
            {
                document.location.href = "admin_fin_cf_delete.php?l="+l;
            }
        }
        else
        {
            alert("An error has occurred.  Please reload the page and try again.");
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Monthly Cashflows ~ Edit Line Item</b></h1>
<p>&nbsp;</p>
<?php
$lineid = $_GET['l'];
$line = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_lineitems WHERE lineid = ".$lineid." AND lineyn = 'Y' AND linetype = 'CF'";
include("inc_db_con.php");
    $line = mysql_fetch_array($rs);
mysql_close();
?>
<form name=edit method=post action=admin_fin_cf_edit_process.php onsubmit="return Validate(this);">
<input type=hidden name=lineid value=<?php echo($lineid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=120 height=27 >Ref:</td>
		<td class=tdgeneral width=450 colspan=2><?php echo($lineid); ?></td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>Sub-Directorate:</td>
		<td class=tdgeneral valign=top width=450 colspan=2><select name=subid id=subid>
            <option value=X>--- SELECT ---</option>
    		<?php
            $subid = $line['linesubid'];
            $sel = "N";
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['dirid'];
                    $val = $row['dirtxt'];
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            echo("<option ");
                            if($id2 == $subid) {
                                echo(" selected ");
                                $sel = "Y";
                            }
                            echo("value=".$id2.">".$val2."</option>");
                        }
                    mysql_close($con2);
                }
            mysql_close();
            if($sel != "Y")
            {
                ?><script type=text/javascript>
                    document.getElementById('subid').value = "X";
                </script><?php
            }
            ?>
        </select></td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>Line Item:</td>
		<td class=tdgeneral valign=top width=450 colspan=2><input type=text size=50 maxlength=100 name=val id=val value="<?php echo($line['linevalue']); ?>"> <span style="font-size: 7.5pt;">(max 100 characters)</span></td>
	</tr>
<?php
$style['th'][1] = "background-color: #cc0001; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][2] = "background-color: #fe9900; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][3] = "background-color: #006600; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][4] = "background-color: #000066; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";

$style['thl'][1]['R'] = "background-color: #ffabab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['R'] = "background-color: #ffddab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['R'] = "background-color: #a3ffc2; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['R'] = "background-color: #babaff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['O'] = "background-color: #ffcccc; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['O'] = "background-color: #ffe6bd; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['O'] = "background-color: #ceffde; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['O'] = "background-color: #ccccff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['C'] = "background-color: #ffdbdf; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['C'] = "background-color: #fff1de; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['C'] = "background-color: #deffea; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['C'] = "background-color: #ddddff; color: #000; border-bottom: 1px solid #fff;";

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
    ?>
    <tr>
		<td class=tdgeneral width=120 colspan=1 height=27 style="background-color: #888; color: #fff; font-weight: bold; border-bottom: 1px solid #fff; "><b>Original Budget</b></td>
		<td class=tdgeneral valign=top width=450 colspan=2 style="background-color: #888; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;">&nbsp;</td>
    </tr>
    <?php
        $s=1;
        $t=0;
        while($row = mysql_fetch_array($rs))
        {
            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_cashflow WHERE cfyn = 'Y' AND cflineid = ".$lineid." AND cftimeid = ".$row['id'];
            include("inc_db_con2.php");
                if(mysql_num_rows($rs2)>0)
                {
                    $row2 = mysql_fetch_array($rs2);
                }
                else
                {
                    $row2 = array();
                }
            mysql_close($con2);
            ?>
            <tr>
                <td class=tdgeneral width=120 rowspan=3 valign=top style="<?php echo($style['th'][$s]); ?> font-weight: bold;"><?php echo(date("M Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
                <td class=tdgeneral width=70 style="<?php echo($style['thl'][$s]['R']); ?> font-weight: bold; border-left: 1px solid #fff;">Revenue:</td>
                <td class=tdgeneral width=380 valign=top style="<?php echo($style['thl'][$s]['R']); ?>">R <input type=text size=10 name=revenue[] id=r<?php echo($t); ?> style="text-align: right" value=<?php if($row2['cfrev1']>0) { echo($row2['cfrev1']); } else { echo("0"); } ?>> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <tr>
                <td class=tdgeneral width=70 style="<?php echo($style['thl'][$s]['O']); ?> font-weight: bold; border-left: 1px solid #fff;">Op. Exp.:</td>
                <td class=tdgeneral width=380 valign=top style="<?php echo($style['thl'][$s]['O']); ?>">R <input type=text size=10 name=opex[] id=o<?php echo($t); ?> style="text-align: right" value=<?php if($row2['cfop1']>0) { echo($row2['cfop1']); } else { echo("0"); } ?>> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <tr>
                <td class=tdgeneral width=70 style="<?php echo($style['thl'][$s]['C']); ?> font-weight: bold; border-left: 1px solid #fff;">Cap. Exp.:</td>
                <td class=tdgeneral width=380 valign=top style="<?php echo($style['thl'][$s]['C']); ?>">R <input type=text size=10 name=capex[] id=c<?php echo($t); ?> style="text-align: right" value=<?php if($row2['cfcp1']>0) { echo($row2['cfcp1']); } else { echo("0"); } ?>> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <?php
            $t++;
            $s++;
            if($s>4) { $s=1; }
        }
    }
mysql_close();
?>
    <tr>
		<td class=tdheaderl width=120>&nbsp;<input type=hidden name=t value=<?php echo($t); ?> size=5></td>
		<td class=tdgeneral colspan=2 width=450 valign=top><input type=submit value=" Update "> <input type=button value="Delete" onclick="deleteLine(<?php echo($lineid); ?>);"> <input type=reset value="Reset"></td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_fin_cf_setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
