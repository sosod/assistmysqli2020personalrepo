<?php
    include("inc_ignite.php");
    
    $fileloc = $_POST['floc'];
    $impcheck = $_POST['import'];
//print_r($impcheck);
foreach($impcheck as $i)
{
    $import[$i] = "Y";
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import ~ Municipal KPAs<input type=hidden id=lab></b></h1>
<center>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script>
<?php
$err = "N";
$p=0;

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}
else
{
    echo("<script type=text/javascript>document.getElementById('floc').value = '".$fileloc."';</script>");
}

if($err == "N")
{
            $file = fopen($fileloc,"r");
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader>Ref</td>
                        <td class=tdheader>KPA</td>
                        <td class=tdheader>Code</td>
                    </tr>
                    <?php
                    $p2 = 100 / count($data);
                    foreach($data as $idata)
                    {
                        $id=0;
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $ref = $idata[0];
                        $kpa = trim($idata[1]);
                        $kpa = htmlentities($kpa,ENT_QUOTES,"ISO-8859-1");
                        $kpa = substr($kpa,0,200);

                        $code = trim($idata[2]);

                        if(strlen($kpa)>0 && strlen($code)>0 && strlen($code)<11 && $ref!="Ref")
                        {
                            if($import[$ref]=="Y")
                            {
                                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_munkpa (value,yn,code) VALUES ('".$kpa."','Y','".$code."')";
                                include("inc_db_con.php");
                                $id = mysql_insert_id($con);
                            
                        ?>
                    <tr>
                        <td class=tdheadergreen><?php echo($id); ?></td>
                        <td class=tdgeneral><?php echo($kpa); ?></td>
                        <td class=tdgeneral><?php echo($code); ?></td>
                    </tr>
                                    <?php
                            }
                            else        //user import validation
                            {
                                    ?>
                    <tr>
                        <td class=tdheaderorange><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=2>KPA ignored per user request.</td>
                    </tr>
                                    <?php
                            }
                                    }
                                    else    //kpa validation
                                    {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=2>Invalid KPA.  Either the KPA text is missing or the Short Code is too long.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                    }
                    ?>
                </table>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "import.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
</body>
</html>
