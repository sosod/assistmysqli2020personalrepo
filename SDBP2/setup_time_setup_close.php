<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function previewDates() {
    var c = document.getElementById('c').value;
    var r = document.getElementById('r').value;
    document.location.href = "setup_time_setup_close.php?c="+c+"&r="+r;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Time Periods ~ Setup</b></h1>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdheaderl height=27 width=120>Use Auto Close?:</td>
        <td class=tdgeneral><select><option>No</option></select></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>Close Time Periods: </td>
        <td class=tdgeneral><input type=text size=5 value=14 id=c> days after the last day of the month.</td>
    </tr>
    <tr>
        <td class=tdheaderl height=27 width=120>Use Auto Notice?:</td>
        <td class=tdgeneral><select><option>No</option></select></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>Email Notice: </td>
        <td class=tdgeneral><input type=text size=5 value=7 id=r> days after the last day of the month.</td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=2><input type=button value="Preview Dates" onclick="previewDates()"> <input type=button value="Update Time Periods"></td>
    </tr>
</table>
<p style="font-size: 7.5pt; margin-top: 5px;"><i>* Note: <br>All auto closure of Time Periods occur at 01:00 on the date given.<br>Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.</i></p>
<?php
$days = $_GET['d'];
if(strlen($days)>0 && is_numeric($days))
{
?>
<h2 class=fc>Preview of dates</h2>
<p style="font-size: 7.5pt; margin-top: 5px;"><i>Dates for individual Time Periods can be edited from the Time Period list page.</i></p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td class=tdheader width=20>&nbsp;</td>
		<td class=tdheader height=27>Time period</td>
		<td width=50 class=tdheader >Active</td>
		<td class=tdheader >Closure</td>
		<td class=tdheader >Notice</td>
	</tr>
	<?php
	$add = $days * 86400;
	$t = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sval, eval";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $t++;
            $tval = date("d F Y", $row['sval'])." - ".date("d F Y", $row['eval']);
    ?>
	<?php include("inc_tr.php"); ?>
		<td class=tdheader height=27 align=center><?php echo($t); ?></td>
		<td class=tdgeneral style="padding-left: 5px"><?php echo($tval); ?></td>
		<td class=tdgeneral align=center><?php
            if($row['active']=="Y") { echo("Yes"); } else { echo("No"); }
        ?></td>
		<td class=tdgeneral align=center><?php
            if($row['active']=="Y") { echo(date("d M Y",$row['aclose']+)); } else { echo("N/A"); }
        ?></td>
		<td class=tdgeneral align=center><?php
            if($row['active']=="Y") { echo(date("d M Y",$row['arem'])); } else { echo("N/A"); }
        ?></td>
	</tr>
	<?php
        }
    mysql_close();
    ?>
</table>
<?php } ?>
<?php
$urlback = "setup_time_setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
