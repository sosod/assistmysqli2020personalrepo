<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var cde = me.cde.value;
    var valid8 = "true";
    if(cde.length==0)
    {
        document.getElementById('cde').className = 'reqtext';
        document.getElementById('cde').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('cde').className = 'reqmet';
    }
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('val').className = 'reqmet';
    }
    if(document.getElementById('val').className == 'reqtext' || document.getElementById('cde').className == 'reqtext')
    {
        alert("Please complete the missing fields as indicated.");
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

function delVal(k) {
    if(!isNaN(parseInt(k)) && escape(k) == k)
    {
        if(confirm("Are you sure you wish to delete this municipal KPA?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
             document.location.href = "setup_lists_munkpa_del.php?k="+k;
        }
    }
    else
    {
        alert("An error has occurred.\nPlease reload the page and try again.");
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Municipal KPAs ~ Edit</b></h1>
<p>&nbsp;</p>
<?php
$kpaid = $_GET['k'];
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE id = ".$kpaid;
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
mysql_close();
?>
<form name=edit method=post action=setup_lists_munkpa_edit_process.php onsubmit="return Validate(this);">
<input type=hidden name=k value=<?php echo($kpaid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=110 height=27>Ref:</td>
		<td class=tdgeneral valign=top><?php echo($kpaid); ?></td>
		<td class=tdgeneral width=60>&nbsp;</td>
	</tr>
    <tr>
		<td class=tdheaderl width=110>Municipal KPA:</td>
		<td class=tdgeneral valign=top><input type=text size=30 maxlength=100 name=val id=val value="<?php echo($row['value']); ?>"> <span style="font-size: 7.5pt;">(max 100 characters)</span></td>
		<td class=tdgeneral width=60>*required</td>
	</tr>
    <tr>
		<td class=tdheaderl>Short Code**:</td>
		<td class=tdgeneral valign=top><input type=text size=10 maxlength=5 name=cde id=cde value="<?php echo($row['code']); ?>"> <span style="font-size: 7.5pt;">(max 5 characters)</span></td>
		<td class=tdgeneral>*required</td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral colspan=2 valign=top><input type=submit value=" Save Changes "> <input type=button value=" Delete " onclick="delVal(<?php echo($kpaid); ?>)"></td>
    </tr>
</table>
<p>** Used on the list view of KPIs.</p>
</form>
<?php
$urlback = "setup_lists_munkpa.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
