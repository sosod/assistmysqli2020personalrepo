<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Municipal KPAs ~ Edit</b></h1>
<p>&nbsp;</p>
<?php
//get form data
$value = htmlentities($_POST['val'],ENT_QUOTES,"ISO-8859-1");
$cde = $_POST['cde'];
$kpaid = $_POST['k'];

if(is_numeric($kpaid) && $kpaid > 0)
{
//validate in timekeep
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE yn = 'Y' AND value = '".$value."' AND id <> ".$kpaid;
include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE yn = 'Y' AND code = '".$cde."' AND id <> ".$kpaid;
include("inc_db_con.php");
    $rnum2 = mysql_num_rows($rs);
mysql_close();


if($rnum == 0 && $rnum2 == 0)
{
//if exists:
    //insert into list_admins
    $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_munkpa SET ";
    $sql.= "  value = '".$value."'";
    $sql.= ", yn = 'Y'";
    $sql.= ", code = '".$cde."'";
    $sql.= " WHERE id = ".$kpaid;
    include("inc_db_con.php");
$tsql = $sql;
    //insert into sdbip log
        $logref = $kpaid;
        $logtype = "SLKPA";
        $logaction = "Updated Municipal KPA '".$value."'";
        $logdisplay = "Y";
        include("inc_log.php");
    //insert into transaction log
$told = "";
$trans = $logaction." with id ".$kpaid;
include("inc_transaction_log.php");
    //display result
    ?>
    <p>Municipal KPA '<?php echo($value); ?>' has been successfully updated.</p>
    <p>&nbsp;</p>
    <?php
    $urlback = "setup_lists_munkpa.php";
    include("inc_goback.php");
    ?>
<?php
}
else
{
//else
    //display error
        ?>
        <p>An error has occurred (Err: SDBIP35).  <br>
        It is possible that the Municipal KPA you have tried to add already exists (either the text or the short code).<br>
        &nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_lists_munkpa.php";
        include("inc_goback.php");
        ?>
        <?php
}
}
else    //is numeric kpaid
{
        ?>
        <p>An error has occurred (Err: SDBIP34).  <br>
        &nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_lists_munkpa.php";
        include("inc_goback.php");

}
?>

<p>&nbsp;</p>
</body>
</html>
