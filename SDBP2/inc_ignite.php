<?php
require_once '../inc_session.php';

//CHECK YEAR COOKIE
if(strlen($modref)==0)
{
    $modver = "SDBP2";
    $modtxt = "2009/2010";
    $modcurr = "Y";
    $modref = "sdp09";
	$dbref = "assist_".$cmpcode."_".$modref;
}

$cmprow = getCMP($cmpcode);
$cmpname = $cmprow['cmpname'];

//$today = 1258290000;    //set date to 11 Nov 09 15h00 for dev purposes
$krsetup = Array ( 
	0 => Array ( 
		'id' => 4, 
		'value' => "KPI's Not Yet Measured",
		'mincalc' => '',
		'min' => 0, 
		'max' => 0, 
		'maxcalc' => '',
		'sort' => 0, 
		'yn' => 'Y', 
		'color' => '#AAAAAA',
		'code' => 'N/A'
	),
	1 => Array ( 
		'id' => 1, 
		'value' => "KPI's Not Met",
		'mincalc' => 'G',
		'min' => 0, 
		'max' => 75, 
		'maxcalc' => 'L',
		'sort' => 1, 
		'yn' => 'Y', 
		'color' => '#CC0001',
		'code' => 'R'
	),
	2 => Array ( 
		'id' => 2, 
		'value' => "KPI's Almost Met",
		'mincalc' => 'G-E',
		'min' => 75, 
		'max' => 100, 
		'maxcalc' => 'L',
		'sort' => 2, 
		'yn' => 'Y', 
		'color' => '#FE9900',
		'code' => 'O'
	),
	3 => Array ( 
		'id' => 3, 
		'value' => "KPI's Met",
		'mincalc' => 'G-E',
		'min' => 100, 
		'max' => 999999999999, 
		'maxcalc' => 'L',
		'sort' => 3, 
		'yn' => 'Y', 
		'color' => '#006600',
		'code' => 'G'
	)
);
foreach($krsetup as $krs) {
	$styler[$krs['sort']] = "background-color: ".$krs['color']."; color: #000; text-align: center;";
}



function calcKR($kct, $rt, $ra) {
    $krr = 0;
    if(strtoupper($kct) == "ZERO") {
        if($ra > $rt) {
            $krr = 1;
        } else {
            $krr = 3;
        }
    } else {
        if($ra >= $rt && $ra > 0) {
            $krr = 3;
        } else {
            if($rt > 0) {
                if($ra/$rt < 0.75) {
                    $krr = 1;
                } else {
                    $krr = 2;
                }
            }
        }
    }
    return $krr;
}

function KRGlossOp($op) {
                        switch($op)	{
                            case "G":
                                $echo = ">";
                                break;
                            case "G-E":
                                $echo=">=";
                                break;
                            case "E":
                                $echo="=";
                                break;
							case "L":
								$echo = "<";
								break;
							case "L-E":
								$echo = "<=";
								break;
                        }
	return $echo;
}
function displayKRGloss($src) {
	global $krsetup;
	if($src == "report") { 
		$echo = "<table cellpadding=0 cellspacing=0 style=\"float:right; border:0px;\"><tr><td class=noborder>";
		$echo.="<h2 name=krr>KPI Result Categories</h2>";
		$echo.="<table cellpadding=3 cellspacing=0 width=570>";
		$echo.= "<tr>        <th width=160>Category</th>        <th width=40>Color</th>        <th width=370>Explanation</th>    </tr>";
		foreach($krsetup as $row)
		{
			$echo.="<tr>";
			$echo.="<td>".$row['value']."</td>";
			$echo.="<td style=\"text-align:center\" ><table class=noborder><tr><td width=34 class=noborder style=\"background-color: ".$row['color'].";\">&nbsp;</td></tr></table></td>";
			$echo.="<td>";
            if($row['sort']==0) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" KPI Result = ".$row['max']);
                } else {
                    if($row['max']>1000) {
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']);
                    } else {
                        $echo.=($row['min']." ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']);
                    }
                }
            }
			$echo.="</td>";
			$echo.="</tr>";
		}
		$echo.="</table>";
		$echo.="</td></tr></table>"; 
	} elseif ($src == "excel") {
		$echo.="<tr></tr><tr><td nowrap class=title>KPI Result Categories</td></tr>";
		$echo.= "<tr>        <td class=head> </td>        <td class=head nowrap>Category</td>        <td class=head nowrap >Explanation</td>    </tr>";
		foreach($krsetup as $row)
		{
			$echo.="<tr>";
			$echo.="<td style=\"text-align:center;background-color: ".$row['color'].";\"> </td>";
			$echo.="<td nowrap>".$row['value']."</td>";
			$echo.="<td nowrap>";
            if($row['sort']==0) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" KPI Result = ".$row['max']);
                } else {
                    if($row['max']>1000) {
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']);
                    } else {
                        $echo.=($row['min']." ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']);
                    }
                }
            }
			$echo.="</td>";
			$echo.="</tr>";
		}
	} elseif ($src == "csv") {
		$echo.="\r\n\"KPI Result Categories\"\r\n";
		$echo.= "\" \",\"Category\",\"Explanation\"\r\n";
		foreach($krsetup as $row)
		{
			$echo.="\"".$row['code']."\"";
			$echo.=",\"".$row['value']."\"";
			$echo.=",\"";
            if($row['sort']==0) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" KPI Result = ".$row['max']);
                } else {
                    if($row['max']>1000) {
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']);
                    } else {
                        $echo.=($row['min']." ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']);
                    }
                }
            }
			$echo.="\"";
			$echo.="\r\n";
		}
	} else {	//glossary
		$echo.="<h2 name=krr>KPI Result Categories</h2>";
		$echo.="<table cellpadding=3 cellspacing=0 width=570>";
		$echo.= "<tr>        <th width=160>Category</th>        <th width=40>Color</th>        <th width=370>Explanation</th>    </tr>";
		foreach($krsetup as $row)
		{
			$echo.="<tr>";
			$echo.="<td>".$row['value']."</td>";
			$echo.="<td style=\"text-align:center\" ><table class=noborder><tr><td width=34 class=noborder style=\"background-color: ".$row['color'].";\">&nbsp;</td></tr></table></td>";
			$echo.="<td>";
            if($row['sort']==0) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" KPI Result = ".$row['max']);
                } else {
                    if($row['max']>1000) {
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']);
                    } else {
                        $echo.=($row['min']." ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" KPI Result ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']);
                    }
                }
            }
			$echo.="</td>";
			$echo.="</tr>";
		}
		$echo.="</table>";
	}
	return $echo;
}
?>