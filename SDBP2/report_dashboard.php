<?php
    include("inc_ignite.php");
    $r = 1;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdgeneral {
    border-left: 0px solid #ffffff;
//    padding-left: 10px;
}
.tdheaderl {
    border-right: 0px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    border-top: 0px solid #ffffff;
}
</style>
<base target="main">
<script type=text/javascript>
function changeWho(me) {
    var who = me.value;
    var fld = "more";
    var target = document.getElementById('more');
    var target2 = document.getElementById('moretxt');
    if(who == "mun")
    {
        target.disabled = false;
        target2.disabled = false;
        target2.innerText = "Include Directorates?";
    }
    else
    {
        var w2 = who.substring(0,1);
        if(w2 == "d")
        {
            target.disabled = false;
            target2.disabled = false;
            target2.innerText = "Include Sub-Directorates?";
        }
        else
        {
            target.disabled = true;
            target2.disabled = true;
        }
    }
}
function chgLayout(me) {
	if(me.checked == true) {
		document.getElementById('l2').disabled = false;
		document.getElementById('l3').disabled = false;
	} else {
		document.getElementById('l1').checked = true;
		document.getElementById('l2').disabled = true;
		document.getElementById('l3').disabled = true;
	}
}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - Dashboard</b></h1>
<form name=view action=report_dashboard_process.php method=POST>
<table cellpadding=5 cellspacing=0 width=550>
    <tr id=what>
        <td class=tdheaderl valign=top width=120>What:</td>
        <td class=tdgeneral>
            <select name=what><option value=KPI>KPIs</option><option value=CF>Monthly Cashflows</option></select><!-- &nbsp;-&nbsp;
            <select name=graph><option value=P>Position at the end of the period</option><option value=M>Movement over the period</option></select> -->
        </td>
    </tr>
    <tr id=who>
        <td class=tdheaderl valign=top>Who:</td>
        <td class=tdgeneral><select name=who id=dir onchange="changeWho(this);"><option selected value=mun>Entire Municipality</option>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['dirid'];
                    $val = $row['dirtxt'];
                    echo("<option ");
                    echo("value=d_".$id.">".$val."</option>");
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            echo("<option value=s_".$id2.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ".$val2."</option>");
                        }
                    mysql_close($con2);
                }
            mysql_close();
            $r = 1;
        ?>
        </select><br>
        <input type=checkbox name=more value=Y id=more onclick="chgLayout(this);"> <label for=more id=moretxt>Include Directorates?</label></td>
    </tr><?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND eval <= ".$today." ORDER BY sort";
//    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        $tarr = array();
        while($row = mysql_fetch_array($rs))
        {
            $tarr[$t] = $row;
            $t++;
        }
    mysql_close();
//    echo($tarr[2]['id']);
    ?><tr>
        <td class=tdheaderl valign=top width=120>When:</td>
        <td class=tdgeneral>
            <input type=radio name=when value=YTD id=ytd checked> <label for=ytd>Year-to-date</label><br>
            <?php //if(count($tarr) == 12) {
            if($tarr[2]['active'] == "N") { ?>
            <input type=radio name=when value=Q1 id=q1> <label for=q1>Year-to-date as at <?php if(count($tarr)==12) { echo(date("d M Y",$tarr[2]['eval'])); } else { echo("end of 1st Quarter"); } ?></label><br>
            <?php }
            if($tarr[5]['active'] == "N") { ?>
            <input type=radio name=when value=Q2 id=q2> <label for=q2>Year-to-date as at <?php if(count($tarr)==12) { echo(date("d M Y",$tarr[5]['eval'])); } else { echo("end of 2st Quarter"); } ?></label><br>
            <?php }
            if($tarr[9]['active'] == "N") { ?>
            <input type=radio name=when value=Q3 id=q3> <label for=q3>Year-to-date as at <?php if(count($tarr)==12) { echo(date("d M Y",$tarr[8]['eval'])); } else { echo("end of 3st Quarter"); } ?></label><br>
            <?php }
            if($tarr[11]['active'] == "N") { ?>
            <input type=radio name=when value=Q4 id=q4> <label for=q4>Year-to-date as at <?php if(count($tarr)==12) { echo(date("d M Y",$tarr[11]['eval'])); } else { echo("end of 4st Quarter"); } ?></label><br>
            <?php }
            //} ?>
            <input type=radio name=when value=SEL id=sel> <label for=sel>From <select name=wf onclick="document.getElementById('sel').checked = true;" id=wf><?php
                $t1 = 0;
                foreach($tarr as $time)
                {
                    echo("<option ");
                    if($t1 == 0)
                    {
                        echo("selected ");
                        $t1++;
                    }
                    echo("value=".$time['sval'].">".date("M Y",$time['sval']));
                    if($time['active']=="Y") { echo("*"); }
                    echo("</option>");
                }
            ?></select> to <select name=wt onclick="document.getElementById('sel').checked = true;" id=wt><?php
                $t2 = 0;
                foreach($tarr as $time)
                {
                    if($t2 == 0)
                    {
                        echo("<option ");
                        if(date("m Y",$today)==date("m Y",$time['eval']))
                        {
                            echo("selected ");
                            $t2++;
                        }
                        echo("value=".$time['eval'].">".date("M Y",$time['eval']));
                    if($time['active']=="Y") { echo("*"); }
                    echo("</option>");
                    }
                }
            ?></select></label><br><span style="font-size: 6.5pt;font-style: italic;">Note: Months marked with a * are still open for updating.</span>
        </td>
    </tr>
    <?php
    if($t1==0)
    {
        ?>
        <script type=text/javascript>
        document.getElementById('wf').value = document.getElementById('wf').options[0].value;
        </script>
        <?php
    }
    if($t2==0)
    {
        ?>
        <script type=text/javascript>
        document.getElementById('wt').value = document.getElementById('wt').options[0].value;
        </script>
        <?php
    }
    ?>
    <tr>
        <td class=tdheaderl valign=top width=120>Layout:</td>
        <td class=tdgeneral><input type=radio name=layout value=ons checked id=l1> Onscreen (1 graph per line)<br>
		<input type=radio name=layout value=P id=l2> Portrait (2 graphs per line)<br>
		<input type=radio name=layout value=L id=l3> Landscape (3 graphs per line)</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=120>&nbsp;</td>
        <td class=tdgeneral><input type=submit value=" View "></td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=2 style="font-size: 7pt;line-height: 9pt"><b>Note:</b> In order to print the Dashboard Report to PDF, you need to have a PDF printer installed on your computer. Please contact your IT department or the Ignite Helpdesk for more information.</i></td>
    </tr>
</table>
</form>
<?php
$urlback = "report.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
<script type=text/javascript>
	chgLayout(document.getElementById('more'));
</script>
</body>
</html>
