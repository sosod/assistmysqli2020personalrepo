<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 1px solid #ababab;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('#datepicker1').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

                //End
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

			});
function dirSub(me) {
        var dir = me.value;
        if(dir.length>0 && !isNaN(parseInt(dir)))
        {
            var sub = document.getElementById('sf');
            sub.length = 0;
            var sf = subs[dir];
            var o = 1;
            var ds = false;
            var opt = new Array();
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,true);
            for(s in sf)
            {
                sub.options[o] = new Option(sf[s][1],sf[s][0],ds,ds);
                o++;
            }
        }
}
</script>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - Capital Projects</b></h1>
<form name=cpreport method=post action=report_cp_process.php>
<h3 class=fc>1. Select the columns you want in your report:</h3>
<div style="margin-left: 17px">
<table cellpadding="3" cellspacing="0" width=700>
    <tr>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0  style="border-width: 0px">
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="dir" value="Y"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Directorate</td>
            	</tr>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="sub" value="Y"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Sub-Directorate</td>
            	</tr>
<?php
$head = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CP' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $m2 = ($mnr) / 2;
    $m = round($m2) + 1;
    if($m < $m2) { $m++; }
    $r=1;
    while($row = mysql_fetch_array($rs))
    {
        $head[$row['headfield']] = $row;
        $r++;
        if($r > $m)
        {
            $r=0;
            ?>
            </table>
        </td>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0 style="border: 0px">
            <?php
        }
        ?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="<?php echo($row['headfield']); ?>" value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle><?php echo($row['headdetail']); ?></td>
            	</tr>
        <?php
    }
mysql_close();

$timearr = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $timearr[$row['sort']]['sort'] = $row['sort'];
        $timearr[$row['sort']]['val'] = date("M-y",$row['eval']);
    }
mysql_close();
?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="cashflow" value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Monthly Cashflow Figures<br>from <select name=cf id=cf>
                    <?php
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo("<option ");
                        if($t == 1) { echo(" selected "); }
                        echo("value=".$tim['sort'].">".$tim['val']."</option>");
                        $t++;
                    }
                    ?></select> to <select name=ct id=ct>
                    <?php
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo("<option ");
                        if($t == count($timearr)) { echo(" selected "); }
                        echo("value=".$tim['sort'].">".$tim['val']."</option>");
                        $t++;
                    }
                    ?></select></td>
            	</tr>
            </table>
        </td>
    </tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class="tdgeneral" valign=top><b>Directorate:</b></td>
		<td class="tdgeneral" valign=top colspan=4><select name=dirfilter id=df onchange="dirSub(this);">
            <option selected value=ALL>All Directorates</option>
            <?php
            $js = "";
            $sql = "SELECT DISTINCT d.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
            $sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= ", assist_".$cmpcode."_".$modref."_capital c ";
            $sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND c.cpyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = c.cpsubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option value=".$row['dirid'].">".$row['dirtxt']."</option>");
                    $js.= "subs[".$row['dirid']."] = new Array();".chr(10);
                }
            mysql_close();
/*            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option value=".$row['dirid'].">".$row['dirtxt']."</option>");
                }
            mysql_close();
*/            ?>
        </select></td>
	</tr>
	<tr>
		<td class="tdgeneral" valign=top><b>Sub-Directorate:</b></td>
		<td class="tdgeneral" valign=top colspan=4><select name=subfilter[] id=sf size=6 multiple>
            <option selected value=ALL>All Sub-Directorates</option>
            <?php
            $sql = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
            $sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= ", assist_".$cmpcode."_".$modref."_capital c ";
            $sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND c.cpyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = c.cpsubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $did2 = $row['subdirid'];
                    $sort2 = $row['subsort'];
                    $id2 = $row['subid'];
                    $val2 = $row['subtxt'];
                    echo("<option value=".$row['subid'].">".$row['subtxt']."</option>");
                    $js.= "subs[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                }
            mysql_close();
            ?>
        </select><br><i><small>Use CTRL key to select multiple options</small></i></td>
	</tr>
	<?php
    foreach($head as $hrow)
    {
        $i=0;
        if($hrow['headfield']!="progress")
        {
        ?>
        <tr>
            <td class="tdgeneral" valign=top><b><?php echo($hrow['headdetail']); ?>:</b><?php
            if($hrow['headfield']=="wards")
            {
                echo("<br><select name=wardsfiltertype><option selected value=\"any\">Match any selected ward</option><option value=\"all\">Match all selected wards</option></select>");
            }
            ?></td>
        <?php
        switch($hrow['headfield'])
        {
            case "cpenddate":
                ?>
        		<td class="tdgeneral" valign=top colspan=4><input type=text size=15 id="datepicker2" readonly="readonly"> <input type=hidden size=10  name=<?php echo($hrow['headfield']."filter"); ?> id=endDate> <select name=<?php echo($hrow['headfield']."filtertype"); ?>><option selected value=on>On this date</option><option value=before>On or before this date</option><option value=after>On or after this date</option></select></td>
                <?php
                break;
            case "cpstartdate":
                ?>
        		<td class="tdgeneral" valign=top colspan=4><input type=text size=15 id="datepicker1" readonly="readonly"> <input type=hidden size=10  name=<?php echo($hrow['headfield']."filter"); ?> id=startDate> <select name=<?php echo($hrow['headfield']."filtertype"); ?>><option selected value=on>On this date</option><option value=before>On or before this date</option><option value=after>On or after this date</option></select></td>
                <?php
                break;
            case "wards":
                ?>
        		<td class="tdgeneral" valign=top><input type=checkbox name=wardsfilter[] value=X checked> Any&nbsp;<br>
                <input type=checkbox name=wardsfilter[] value=1> All&nbsp;<br>
        		<?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $wmnr = mysql_num_rows($rs2)+2;
                            $c2 = $wmnr / 4;
                            $c = round($c2);
                            if($c < $c2) { $c++; }
                            $w=2;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                $w++;
                                                if($w>$c)
                                                {
                                                    echo("</td><td class=tdgeneral valign=top>");
                                                    $w = 1;
                                                }
                                                echo("<input type=checkbox name=wardsfilter[] value=".$row2['id']." >".$row2['value']."<br>");
                                            }
                                        ?>
                                <?php
                        mysql_close($con2);
                break;
            default:
                ?>
                <td class="tdgeneral" valign=top colspan=4><input type=text name=<?php echo($hrow['headfield']."filter"); ?>>&nbsp;<select  name=<?php echo($hrow['headfield']."filtertype"); ?>><option selected value=all>Match all words</option><option value=any>Match any word</option><option value=exact>Match exact phrase</option></td>
                <?php
                break;
        }
        ?>
        </tr>
        <?php
        }
    }
    ?>
</table>
</div>
<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class=tdgeneral align=center style="border-bottom: 0px">Sort by:</td>
		<td class="tdgeneral" style="border-bottom: 0px"><select name=sort><option selected value=dir>Directorate / Sub-Directorate</option>
		<?php foreach($head as $hrow) {
    		if($hrow['headfield']!="wards" && $hrow['headfield']!="progress")
            {
                echo(chr(10)."<option value=".$hrow['headfield'].">".$hrow['headdetail']."</option>");
            }
        }?>
        </select></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td width=30 class=tdgeneral align=center><input type="radio" name="output" value="display" checked id=csvn></td>
		<td class="tdgeneral"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="output" value="csv" id=csvy></td>
		<td class=tdgeneral><label for=csvy>Microsoft Excel file</label></td>
	</tr>
<!--	<tr>
		<td class=tdgeneral align=center style="border-bottom: 0px"><input type="radio" name="output" value="pdf" id=pdfy></td>
		<td class=tdgeneral style="border-bottom: 0px"><label for=pdfy>PDF File</label></td>
	</tr> -->
</table>
</div>
<h3 class=fc>5. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>
<script type=text/javascript>
var subs = new Array();
<?php echo($js); ?>
</script>

</body>

</html>
