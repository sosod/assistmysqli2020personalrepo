<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Add KPI</b></h1>
<p>&nbsp;</p>
<?php
//GET HEADING FIELDS
$head = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headdetailyn = 'Y' ORDER BY headdetailsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $hf = $row['headfield'];
            $head[$hf][0] = $_POST[$hf];
            $head[$hf][1] = $hf;
        }
    mysql_close();
//GET OTHER FIELDS
$dir = $_POST['dirid'];
$sub = $_POST['subid'];
$src = $_POST['r'];
$typ = $_POST['typ'];
$cpid = $_POST['cpid'];
$progval = $_POST['progval'];
$krtargettypeid = $_POST['krtargettypeid'];
$target = $_POST['krtarget'];
$time = $_POST['time'];
$plc = $_POST['plc'];

//GET DIR DETAILS IF ALL ELSE SET ARRAY TO DIR ID
if($dir == "ALL")
{
        $d = 0;
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $dirs[$d] = $row['dirid'];
                $d++;
            }
        mysql_close();
}
else
{
    $dirs[0] = $dir;
}

$err = "N";
//echo("<P>dirs:");
//print_r($dirs);
foreach($dirs as $dirid)
{
//VALIDATE DIRID
if(is_numeric($dirid) && $dirid > 0 && $err == "N")
{
    //GET SUB ID INTO ARRAY
    if($sub == "ALL")
    {
        $s = 0;
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid." ORDER BY subsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $subs[$s][0] = $row['subid'];
                $subs[$s][1] = $row['subtxt'];
                $s++;
            }
        mysql_close();
    }
    else
    {
        if($sub == "HEAD")
        {
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid." AND subhead = 'Y'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                $subs[0][0] = $row['subid'];
                $subs[0][1] = $row['subtxt'];
            mysql_close();
        }
        else
        {
            $subs[0][0] = $sub;
        }
    }
//    echo("<P>subs:");
//print_r($subs);
    //CREATE PROG RECORD IF NEW PROG
    $progid = $head['prog'][0];
    $prow = array();
    $prow2 = array();
    $pid = 0;
    if($src == "k")
    {
        if($head['prog'][0] == "NEW")
        {
            if(strlen($progval) > 0)
            {
                $progval = htmlentities($progval,ENT_QUOTES,"ISO-8859-1");
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_prog SET value = '".$progval."', yn = 'Y', dirid = ".$dirid;
                include("inc_db_con.php");
                    $head['prog'][0] = mysql_insert_id();
            }
        }
        else
        {
            $pid = $head['prog'][0];
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE id = ".$pid." AND yn = 'Y'";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $prow = mysql_fetch_array($rs);
                }
                else
                {
                    $err = "Y";
                    ?>
                    <p>An error has occurred.  Please go back and try again.</p>
                    <?php
                    include("inc_goback_history.php");
                }
            mysql_close();
            if($err == "N" && count($prow)>0)
            {
                if($prow['dirid'] == $dirid)
                {
                    $head['prog'][0] = $prow['id'];
                }
                else
                {
                    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE dirid = ".$dirid." AND yn = 'Y' AND value = '".$prow['value']."'";
                    include("inc_db_con.php");
                        if(mysql_num_rows($rs)>0)
                        {
                            $prow2 = mysql_fetch_array($rs);
                            $head['prog'][0] = $prow2['id'];
                        }
                        else
                        {
                            $sql2 = "INSERT INTO assist_".$cmpcode."_".$modref."_list_prog SET value = '".$prow['value']."', yn = 'Y', dirid = ".$dirid;
                            include("inc_db_con2.php");
                                $head['prog'][0] = mysql_insert_id($con2);
                        }
                    mysql_close();
                }
            }
            else
            {
                $err = "Y";
                ?>
                <p>An error has occurred.  Please go back and try again.</p>
                <?php
                include("inc_goback_history.php");
            }
        }
    }
    else    //obj/prog & src=k
    {
        if($head['prog'][0] == "NEW" && strlen($progval) > 0)
        {
            $progval = htmlentities($progval,ENT_QUOTES,"ISO-8859-1");
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_prog SET value = '".$progval."', yn = 'Y', dirid = ".$dirid;
            include("inc_db_con.php");
                $head['prog'][0] = mysql_insert_id();
        }
    }   //obj/prog & src=k
    
//echo("<P>prog:".$head['prog'][0]);

    //VALIDATE KPI VALUES
    if(strlen($head['kpivalue'][0]) > 0 && is_numeric($head['natkpa'][0]) && $head['natkpa'][0] > 0 && is_numeric($head['munkpa'][0]) && $head['munkpa'][0] > 0 && is_numeric($head['prog'][0]) && $head['prog'][0] > 0 && strlen($head['ktype'][0]) > 0 && strlen($head['kpistratop'][0]) > 0 && strlen($head['kpicalctype'][0]) > 0 && count($head['wards'][0]) > 0 && count($time) == count($target) && is_numeric($krtargettypeid) && $krtargettypeid > 0 && $err == "N")
    {

        //FOREACH SUB DIR
        foreach($subs as $sub2)
        {
            $subid = $sub2[0];
            //CREATE KPI
            $kpiidpnum = htmlentities($head['kpiidpnum'][0],ENT_QUOTES,"ISO-8859-1");
            $kpisubid = $subid;
            $kpiprogid = $head['prog'][0];
            $kpinatkpaid = $head['natkpa'][0];
            $kpimunkpaid = $head['munkpa'][0];
            $kpivalue = htmlentities($head['kpivalue'][0],ENT_QUOTES,"ISO-8859-1");
            $kpitypeid = $head['ktype'][0];
            $kpistratop = $head['kpistratop'][0];
            $kpidefinition = htmlentities($head['kpidefinition'][0],ENT_QUOTES,"ISO-8859-1");
            $kpidriver = htmlentities($head['kpidriver'][0],ENT_QUOTES,"ISO-8859-1");
            $kpibaseline = htmlentities($head['kpibaseline'][0],ENT_QUOTES,"ISO-8859-1");
            $kpitargetunit = htmlentities($head['kpitargetunit'][0],ENT_QUOTES,"ISO-8859-1");
            $kpicalctype = $head['kpicalctype'][0];
            $kpicpid = $cpid;
            if($plc == "N" || strlen($plc) == 0 || $plc == "X")
            {
                $kpiyn = 'Y';
            }
            else
            {
                $kpiyn = 'P';
            }
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi SET";
            $sql.= "  kpiidpnum = '".$kpiidpnum."'";
            $sql.= ", kpisubid = ".$kpisubid;
            $sql.= ", kpiprogid = ".$kpiprogid;
            $sql.= ", kpinatkpaid = ".$kpinatkpaid;
            $sql.= ", kpimunkpaid = ".$kpimunkpaid;
            $sql.= ", kpivalue = '".$kpivalue."'";
            $sql.= ", kpitypeid = ".$kpitypeid;
            $sql.= ", kpistratop = '".$kpistratop."'";
            $sql.= ", kpidefinition = '".$kpidefinition."'";
            $sql.= ", kpidriver = '".$kpidriver."'";
            $sql.= ", kpibaseline = '".$kpibaseline."'";
            $sql.= ", kpitargetunit = '".$kpitargetunit."'";
            $sql.= ", kpicalctype = '".$kpicalctype."'";
            $sql.= ", kpicpid = ".$kpicpid;
            $sql.= ", kpiyn = '".$kpiyn."'";
//    echo("<P>".$sql);
            include("inc_db_con.php");
            //GET KPIID
                $kpiid = 0;
                $kpiid = mysql_insert_id();
$tsql = $sql;
$told = "";
$trans = "Added new KPI ".$kpiid;
include("inc_transaction_log.php");
            //VALIDATE KPIID
            if(is_numeric($kpiid) && $kpiid > 0)
            {
                //FOREACH WARD
                $wards = $head['wards'][0];
                foreach($wards as $ward)
                {
                    //INSERT KPI_WARDS
                    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_wards SET";
                    $sql.= " kwkpiid = ".$kpiid;
                    $sql.= ", kwwardsid = ".$ward;
                    $sql.= ", kwyn = 'Y'";
                    include("inc_db_con.php");
//    echo("<P>".$sql);
$tsql = $sql;
$told = "";
$trans = "Added Ward to KPI ".$kpiid;
include("inc_transaction_log.php");
                }
            //IF PLC WAS NOT CHOSEN
            if($plc == "N" || strlen($plc) == 0 || $plc == "X")
            {
                //foreach timeid
                for($t=0;$t<count($time);$t++)
                {
                    $krtimeid = $time[$t];
                    $krtarget = $target[$t];
                    if(strlen($krtarget)==0 || !is_numeric($krtarget)) { $krtarget = 0; }
                    //insert results
                    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_result SET";
                    $sql.= "  krkpiid = ".$kpiid;
                    $sql.= ", krtarget = ".$krtarget;
                    $sql.= ", kractual = 0";
                    $sql.= ", krtargettypeid = ".$krtargettypeid;
                    $sql.= ", krprogress = ''";
                    $sql.= ", krmanagement = ''";
                    $sql.= ", krtimeid = ".$krtimeid;
                    include("inc_db_con.php");
//    echo("<P>".$sql);
$tsql = $sql;
$told = "";
$trans = "Added KPI result to KPI ".$kpiid." for time ".$krtimeid;
include("inc_transaction_log.php");
                }

                ?>
                <p>KPI '<?php echo($kpivalue); ?>' has been successfully added<?php if(strlen($sub2[1])>0) { echo(" to Sub-Directorate '".$sub2[1]."'"); } ?>.</p>
                <?php
            }   //if plc = n or n/a
            else
            {
                echo("<form id=step2 method=post action=\"admin_kpi_add_plc.php\">");
                echo("<input type=hidden name=kpiid value=\"".$kpiid."\">");
                echo("<input type=hidden name=dirid value=\"".$dirid."\">");
                echo("<input type=hidden name=subid value=\"".$subid."\">");
                echo("<input type=hidden name=typ value=\"".$typ."\">");
                echo("<input type=hidden name=src value=\"".$src."\">");
                echo("</form>");
                echo("<script type=text/javascript>");
                echo("document.forms['step2'].submit();");
                echo("</script>");
            }
            }
            else    //kpiid validation
            {
                $err = "Y";
                ?>
                <p>An error has occurred.  Please go back and try again.</p>
                <?php
                include("inc_goback_history.php");
            }
        } //foreach subs
    }
    else    //value validation
    {
        $err = "Y";
        ?>
        <p>An error has occurred.  Please go back and try again.</p>
        <?php
        include("inc_goback_history.php");
    }
}
else    //validate dirid
{
    if($err == "N")
    {
        ?>
        <p>An error has occurred.  Please go back and try again.</p>
        <?php
        include("inc_goback_history.php");
    }
    $err = "Y";
}
    $head['prog'][0] = $progid;
}   //foreach dirs

if($err == "N")
{
    if($src == "s")
    {
        $urlback = "admin_sub.php?s=".$subid;
        include("inc_goback.php");
    }
    else
    {
        if($src=="d")
        {
            $urlback = "admin_dir.php?d=".$dirid;
            include("inc_goback.php");
        }
        else
        {
            if($dir == "ALL")
            {
                $urlback = "admin.php";
                include("inc_goback.php");
            }
            else
            {
                $urlback = "admin_dskpi.php?d=".$dir;
                include("inc_goback.php");
            }
        }
    }
}
?>
</body>

</html>
