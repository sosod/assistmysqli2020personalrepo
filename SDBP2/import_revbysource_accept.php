<?php
    include("inc_ignite.php");
    
    $fileloc = $_POST['floc'];
    $impcheck = $_POST['import'];
//print_r($impcheck);
foreach($impcheck as $i)
{
    $import[$i] = "Y";
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import ~ Revenue By Source<input type=hidden id=lab></b></h1>
<center>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script>
<?php
$err = "N";
$p=0;

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}

if($err == "N")
{
            $file = fopen($fileloc,"r");
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader>ID</td>
                        <td class=tdheader>Line Item</td>
                        <td class=tdheader>Vote Number</td>
                        <?php
                        $time = array();
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time ORDER BY sort";
                        include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs))
                            {
                                $time[] = $row;
                                echo("<td class=tdheader>".date("M-Y",$row['eval'])."</td>");
                            }
                        mysql_close();
                        ?>
                    </tr>
                    <?php
                    $sql = "SELECT max(rsid) as id FROM assist_".$cmpcode."_".$modref."_finance_revbysource";
                    include("inc_db_con.php");
                        $row1 = mysql_fetch_array($rs);
                    mysql_close();
                    $p2 = 100 / count($data);
                    $id = $row1['id'];
                    if(strlen($id)==0) { $id = 0; }
                    foreach($data as $idata)
                    {
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $kpa = trim($idata[0]);
                        $kpa = htmlentities($kpa,ENT_QUOTES,"ISO-8859-1");
                        $kpa = substr($kpa,0,200);
                        $code = trim($idata[1]);
                        $t=2;
                        $nums = array();
                        $isnum = "Y";
                        foreach($time as $tim)
                        {
                            $n = $idata[$t];
                            if(strlen($n)>0)
                            {
                                if(is_numeric($n))
                                {
                                    $nums[$t-1] = $n;
                                }
                                else
                                {
                                    //$nums[$t-1] = $n;
                                    $isnum = "N";
                                }
                            }
                            else
                            {
                                $nums[$t-1] = 0;
                            }
                            $t++;
                        }

                        if(strlen($kpa)>0 && $isnum == "Y")
                        {
                        $id++;
                            if($import[$id]=="Y")
                            {
                                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_finance_revbysource (rsline,rsyn,rssort";
                                for($s=1;$s<13;$s++)
                                {
                                    $sql.= ",rs".$s."budget";
                                }
                                $sql.= ",rsvote) VALUES ('".$kpa."','Y',".$id;
                                for($s=1;$s<13;$s++)
                                {
                                    $n = 0;
                                    $n = $nums[$s];
                                    if(strlen($n)==0) { $n = 0; }
                                    if(!is_numeric($n)) { $n = 0; }
                                    $sql.=",".$n;
                                }
                                $sql.= ",'".$code."')";
                                include("inc_db_con.php");
                                $ref = mysql_insert_id($con);
                            
                        ?>
                    <tr>
                        <td class=tdheadergreen><?php echo($ref); ?></td>
                        <td class=tdgeneral><?php echo($kpa); ?></td>
                        <td class=tdgeneral><?php echo($code); ?></td>
                        <?php
                        foreach($nums as $num)
                        {
                            echo("<td class=tdgeneral align=right>".number_format($num,2)."</td>");
                        }
                        ?>
                    </tr>
                                    <?php
                            }
                            else        //user import validation
                            {
                                    ?>
                    <tr>
                        <td class=tdheaderorange>&nbsp;</td>
                        <td class=tdgeneral colspan=14>KPA ignored per user request.</td>
                    </tr>
                                    <?php
                            }
                                    }
                                    else    //kpa validation
                                    {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdgeneral colspan=14>Invalid line item.</td>
                    </tr>
                                    <?php
                                }
                    }
                    ?>
                </table>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "import.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
</body>
</html>
