<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Update KPI</b></h1>
<p>&nbsp;</p>
<?php
$head = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headdetailyn = 'Y' ORDER BY headdetailsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $hf = $row['headfield'];
            $head[$hf][0] = $_POST[$hf];
            $head[$hf][1] = $hf;
        }
    mysql_close();
$plcact = $_POST['plcact'];
$plc = $_POST['plcid'];
$dir = $_POST['dirid'];
$origsub = $_POST['origsub'];
$sub2[0] = $_POST['subid'];
$subid0 = $sub2[0];
$src = $_POST['r'];
$typ = $_POST['typ'];
$cpid = $_POST['cpid'];
$progval = $_POST['progval'];
$krtargettypeid = $_POST['krtargettypeid'];
$target = $_POST['krtarget'];
$time = $_POST['time'];
$kpiid = $_POST['kpiid'];
$actual = $_POST['kractual'];
$progress = $_POST['krprogress'];
$management = $_POST['krmanagement'];
$refres = $_POST['refres'];

//echo($krtargettypeid);
    $dirid = $dir;

$err = "N";
/*echo("<P>time:");
print_r($time);
echo("<P>tar:");
print_r($target);
echo("<P>act:");
print_r($actual);
echo("<P>progress:");
print_r($progress);
echo("<P>mngmnt:");
print_r($management);
*/

//validate dirid
if(is_numeric($dirid) && $dirid > 0 && $err == "N")
{
            $subs[0][0] = $sub;

    //insert progval if new prog
    $progid = $head['prog'][0];
    $prow = array();
    $prow2 = array();
    $pid = 0;
        if($head['prog'][0] == "NEW" && strlen($progval) > 0)
        {
            $progval = htmlentities($progval,ENT_QUOTES,"ISO-8859-1");
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_prog SET value = '".$progval."', yn = 'Y', dirid = ".$dirid;
            include("inc_db_con.php");
                $head['prog'][0] = mysql_insert_id();
        $tsql = $sql;
        $told = "";
        $trans = "Added programme ".$head['prog'][0];
        include("inc_transaction_log.php");
        }
    
//echo("<P>prog:".$head['prog'][0]);

    //validate values
    if(strlen($head['kpivalue'][0]) > 0 && is_numeric($head['natkpa'][0]) && $head['natkpa'][0] > 0 && is_numeric($head['munkpa'][0]) && $head['munkpa'][0] > 0 && is_numeric($head['prog'][0]) && $head['prog'][0] > 0 && strlen($head['ktype'][0]) > 0 && strlen($head['kpistratop'][0]) > 0 && strlen($head['kpicalctype'][0]) > 0 && count($head['wards'][0]) > 0 && $err == "N" && is_numeric($kpiid) && $kpiid > 0)
    {

            $subid = $sub2[0];
            //insert kpi
            $kpiidpnum = htmlentities($head['kpiidpnum'][0],ENT_QUOTES,"ISO-8859-1");
            $kpisubid = $subid;
            $kpiprogid = $head['prog'][0];
            $kpinatkpaid = $head['natkpa'][0];
            $kpimunkpaid = $head['munkpa'][0];
            $kpivalue = htmlentities($head['kpivalue'][0],ENT_QUOTES,"ISO-8859-1");
            $kpitypeid = $head['ktype'][0];
            $kpistratop = $head['kpistratop'][0];
            $kpidefinition = htmlentities($head['kpidefinition'][0],ENT_QUOTES,"ISO-8859-1");
            $kpidriver = htmlentities($head['kpidriver'][0],ENT_QUOTES,"ISO-8859-1");
            $kpibaseline = htmlentities($head['kpibaseline'][0],ENT_QUOTES,"ISO-8859-1");
            $kpitargetunit = htmlentities($head['kpitargetunit'][0],ENT_QUOTES,"ISO-8859-1");
            $kpicalctype = $head['kpicalctype'][0];
            $kpicpid = $cpid;
            $kpiyn = 'Y';
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi SET";
            $sql.= "  kpiidpnum = '".$kpiidpnum."'";
            $sql.= ", kpisubid = ".$kpisubid;
            $sql.= ", kpiprogid = ".$kpiprogid;
            $sql.= ", kpinatkpaid = ".$kpinatkpaid;
            $sql.= ", kpimunkpaid = ".$kpimunkpaid;
            $sql.= ", kpivalue = '".$kpivalue."'";
            $sql.= ", kpitypeid = ".$kpitypeid;
            $sql.= ", kpistratop = '".$kpistratop."'";
            $sql.= ", kpidefinition = '".$kpidefinition."'";
            $sql.= ", kpidriver = '".$kpidriver."'";
            $sql.= ", kpibaseline = '".$kpibaseline."'";
            $sql.= ", kpitargetunit = '".$kpitargetunit."'";
            $sql.= ", kpicalctype = '".$kpicalctype."'";
            $sql.= ", kpicpid = ".$kpicpid;
            $sql.= ", kpiyn = '".$kpiyn."'";
            $sql.= " WHERE kpiid = ".$kpiid."";
//    echo("<P>".$sql);
            include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Updated KPI ".$kpiid;
        include("inc_transaction_log.php");

                //foreach wards
                $wards = $head['wards'][0];
                if(count($wards)>0)
                {
                    $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_wards SET";
                    $sql.= " kwyn = 'N'";
                    $sql.= " WHERE kwkpiid = ".$kpiid;
                    include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Deleted KPI Wards for KPI ".$kpiid;
        include("inc_transaction_log.php");

                    foreach($wards as $ward)
                    {
                        //insert kpi_wards
                        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_wards SET";
                        $sql.= " kwkpiid = ".$kpiid;
                        $sql.= ", kwwardsid = ".$ward;
                        $sql.= ", kwyn = 'Y'";
                        include("inc_db_con.php");
//    echo("<P>".$sql);
        $tsql = $sql;
        $told = "";
        $trans = "Added KPI Ward for KPI ".$kpiid;
        include("inc_transaction_log.php");
                    }
                }
if($src=="k")
{
                    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." LIMIT 1";
                    include("inc_db_con.php");
                        $krtt = mysql_fetch_array($rs);
                    mysql_close($con);
                    if($krtt['krtargettypeid']!=$krtargetypeid)
                    {
                        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_result SET krtargettypeid = ".$krtargettypeid." WHERE krkpiid = ".$kpiid." LIMIT 12";
                        include("inc_db_con.php");
        $tsql = $sql;
        $told = $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_result SET krtargettypeid = ".$krtt['krtargettypeid']." WHERE krkpiid = ".$kpiid." LIMIT 12";;
        $trans = "Updated KPI Target Type for KPI ".$kpiid." results ";
        include("inc_transaction_log.php");
                    }
}
                //foreach timeid
                for($t=0;$t<count($time);$t++)
                {
                    $krtimeid = $time[$t];
                    $krtarget = $target[$t];
                    if(strlen($krtarget)==0 || !is_numeric($krtarget)) { $krtarget = 0; }
                    $kractual = $actual[$t];
                    if(strlen($kractual)==0 || !is_numeric($kractual)) { $kractual = 0; }
                    $krprogress = htmlentities($progress[$t],ENT_QUOTES,"ISO-8859-1");
                    $krmanagement = htmlentities($management[$t],ENT_QUOTES,"ISO-8859-1");
                    //insert results
                    $kr = array();
                    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid = ".$krtimeid;
                    include("inc_db_con.php");
                        $kr = mysql_fetch_array($rs);
                    mysql_close();
                    $sql = "";
                    if(count($kr)>0)
                    {
                        if( $kr['kractual']!=$kractual  ||  $kr['krprogress']!=$krprogress  ||  $kr['krmanagement']!=$krmanagement  ||  ($kr['krtarget']!=$krtarget && $src=="k") )
                        {
                            $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_result SET";
                            if($src == "k") {
                                $sql.= " krtarget = ".$krtarget.",";
                            }
                            $sql.= " kractual = ".$kractual;
                            $sql.= ", krprogress = '".$krprogress."'";
                            if($src!="k") {
                                $sql.= ", krmanagement = '".$krmanagement."'";
                            } else {
                                $sql.= ", krmanagement = '".$kr['krmanagement']."'";
                            }
                            $sql.= " WHERE krid = ".$kr['krid'];
                            include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Updated KPI result for KPI ".$kpiid." result ".$kr['krid'];
        include("inc_transaction_log.php");
                        }
                    }
//    echo("<P>".$krtimeid."-".$sql);
                }

                ?>
                <p>KPI '<?php echo($kpivalue); ?>' has been successfully updated.</p>
                <?php
                
                
        if($typ == "cp")
        {
            //echo($cpid);

$cpproject = htmlentities($_POST['cpproject'],ENT_QUOTES,"ISO-8859-1");
$cpnum = htmlentities($_POST['cpnum'],ENT_QUOTES,"ISO-8859-1");
$cpvotenum = htmlentities($_POST['cpvotenum'],ENT_QUOTES,"ISO-8859-1");
$cpidpnum = htmlentities($_POST['cpidpnum'],ENT_QUOTES,"ISO-8859-1");
$cpfundsource = htmlentities($_POST['cpfundsource'],ENT_QUOTES,"ISO-8859-1");


$sdate = $_POST['cpstartdate'];
$edate = $_POST['cpenddate'];

$sdate2 = explode("_",$sdate);
$edate2 = explode("_",$edate);

$cpstartdate = mktime(0,0,1,$sdate2[1],$sdate2[0],$sdate2[2]);
$cpenddate = mktime(0,0,1,$edate2[1],$edate2[0],$edate2[2]);


if(strlen($cpproject)>0 && $cpstartdate > 0 && $cpenddate > 0 && $cpenddate >= $cpstartdate && $cpid>0 && strlen($cpid)>0)
{
    //INSERT
    $sql = "UPDATE assist_".$cmpcode."_".$modref."_capital SET";
    $sql.= "  cpnum = '".$cpnum."'";
    $sql.= ", cpsubid = ".$subid;
    $sql.= ", cpvotenum = '".$cpvotenum."'";
    $sql.= ", cpidpnum = '".$cpidpnum."'";
    $sql.= ", cpproject = '".$cpproject."'";
    $sql.= ", cpfundsource = '".$cpfundsource."'";
    $sql.= ", cpstartdate = ".$cpstartdate;
    $sql.= ", cpenddate = ".$cpenddate;
    $sql.= " WHERE cpid = ".$cpid;
    include("inc_db_con.php");
    //LOG
        $tsql = $sql;
        $told = "";
        $trans = "Updated CP ".$cpid;
        include("inc_transaction_log.php");

    $sql = "UPDATE assist_".$cmpcode."_".$modref."_capital_wards SET cwyn = 'N' WHERE cwcpid = ".$cpid;
    include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Deleted Ward for CP ".$cpid;
        include("inc_transaction_log.php");

    //FOREACH WARDS
        foreach($wards as $wrd)
        {
        //INSERT
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_wards SET";
            $sql.= "  cwcpid = ".$cpid;
            $sql.= ", cwwardsid = ".$wrd;
            $sql.= ", cwyn = 'Y'";
            include("inc_db_con.php");
        //LOG
        $tsql = $sql;
        $told = "";
        $trans = "Added ward to CP ".$cpid;
        include("inc_transaction_log.php");
        }

    echo("<p>Capital Project '".$cpproject."' successfully updated.</p>");

}//cp validation
    else    //value validation
    {
        $err = "Y";
        ?>
        <p>An error has occurred when updating the Capital Project.  Please go back and try again.</p>
        <?php
        include("inc_goback_history.php");
    }

    }//typ=cp

                        
    }
    else    //value validation
    {
        $err = "Y";
        ?>
        <p>An error has occurred.  Please go back and try again.</p>
        <?php
        include("inc_goback_history.php");
    }
}
else    //validate dirid
{
    if($err == "N")
    {
        ?>
        <p>An error has occurred.  Please go back and try again.</p>
        <?php
        include("inc_goback_history.php");
    }
    $err = "Y";
}
    $head['prog'][0] = $progid;
//}   //foreach dirs

if($err == "N")
{
    if($plcact == "Y")
    {
        ?>
<form name=plcformedit id=plcformedit method=post action=admin_kpi_edit_plc.php>
<input type=hidden name=plcid value="<?php echo($plc); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=typ id=typ size=2 value=<?php echo($typ); ?>>
<input type=hidden name=src id=r size=2 value=<?php echo($src); ?>>
<input type=hidden name=cpid id=cpid size=2 value=<?php echo($cpid); ?>>
<input type=hidden name=dirid value=<?php echo($dir); ?>>
<input type=hidden name=subid value=<?php echo($subid0); ?>>
</form>
<script type=text/javascript>
document.forms['plcformedit'].submit();
</script>
        <?php
    }
    else
    {
        if($src == "d" || $src == "k")
        {
            $s=$origsub;
        }
        else
        {
            $s=$sub2[0];
        }
        if($typ == "op")
        {
            if($refres=="Y")
            {
                echo("<script type=text/javascript>");
                echo("document.location.href = 'admin_kpi_edit_list.php?d=".$dirid."&s=".$s."&t=".$typ."&r=".$src."&k=".$kpiid."';");
                echo("</script>");
                $urlback = "admin_kpi_edit_list.php?d=".$dirid."&s=".$s."&t=".$typ."&r=".$src."&k=".$kpiid;
            }
        }
        else
        {
            if($refres == "Y")
            {
                echo("<script type=text/javascript>");
                echo("document.location.href = 'admin_cp_edit_list.php?d=".$dirid."&s=".$s."&t=".$typ."&r=".$src."&k=".$cpid."';");
                echo("</script>");
                $urlback = "admin_cp_edit_list.php?d=".$dirid."&s=".$s."&t=".$typ."&r=".$src."&k=".$cpid;
            }
        }
        //echo($urlback);
        if($refres=="Y") { include("inc_goback.php"); }
    }
}

?>
</body>

</html>
