<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheader { border: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Directorate Administrators</b></h1>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td class=tdheader width=30>Ref</td>
		<td class=tdheader width=170>Directorate</td>
		<td class=tdheader width=250>Sub-Directorates</td>
		<td class=tdheader width=120>&nbsp;</td>
	</tr>
	<?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
    $dirnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=4>No Directorates available.  Please use the Setup - Directorates page to add a Directorate.</td>
	</tr>
    <?php
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
            include("inc_tr.php");
    ?>

		<td class=tdheader valign=top><?php echo($id); ?></td>
		<td class=tdgeneral valign=top><b><?php echo($val); ?></b></td>
		<td class=tdgeneral valign=top><ul style="margin: 2 0 0 40;">
		  <?php
            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $id2 = $row2['subid'];
                    $val2 = $row2['subtxt'];
            ?>
                    <li><?php echo($val2); ?></li>
            <?php
                }
            mysql_close($con2);
            ?></ul></td>
		<td class=tdgeneral align=center valign=top>
		<input type=button value="Configure" onclick="document.location.href = 'setup_admin_dir_config.php?d=<?php echo($id); ?>';"></td>
	</tr>
    <?php
        }
    }
    mysql_close();
    ?>
</table>
<?php
$urlback = "setup_admin.php";
include("inc_goback.php");
?>

<p>&nbsp;</p>
</body>
</html>
