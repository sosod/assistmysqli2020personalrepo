<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>


<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Monthly Cashflows ~ Add Line Item</b></h1>
<p>&nbsp;</p>
<?php
$val = htmlentities($_POST['val'],ENT_QUOTES,"ISO-8859-1");
$subid = $_POST['subid'];
$revenue = $_POST['revenue'];
$opex = $_POST['opex'];
$capex = $_POST['capex'];
$time = $_POST['time'];

if(is_numeric($subid) && $subid > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$subid." AND subyn = 'Y'";
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
    mysql_close();
}

if(strlen($val)>0 && $mnr>0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_lineitems WHERE linesubid = ".$subid." AND lineyn = 'Y' ORDER BY linesort DESC";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $row = mysql_fetch_array($rs);
            $linesort = $row['linesort']+1;
        }
        else
        {
            $linesort = 0;
        }
    mysql_close();
    $lineid = 0;
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_finance_lineitems SET linevalue = '".$val."', lineyn = 'Y', linesort = ".$linesort.", linesubid = ".$subid.", linetype = 'CF'";
    include("inc_db_con.php");
//GET ID
    $lineid = mysql_insert_id($con);
    if(is_numeric($lineid) && $lineid > 0 )
    {
        $rc = count($revenue);
        $cc = count($capex);
        $oc = count($opex);
        $tc = count($time);
        if($tc == $oc && $tc == $rc && $tc == $cc)
        {
            $c = 0;
            for($c=0;$c<$tc;$c++)
            {
                $rev = $revenue[$c];
                $op = $opex[$c];
                $cap = $capex[$c];
                $tim = $time[$c];
                //VALIDATE BUDGET
                if(is_numeric($rev) && is_numeric($tim) && $tim > 0 && is_numeric($op) && is_numeric($cap))
                {
                    //ADD FINANCE_CASHFLOW LINE
                    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_finance_cashflow SET ";
                    $sql.= "  cflineid = ".$lineid;
                    $sql.= ", cfyn = 'Y'";
                    $sql.= ", cftimeid = ".$tim;
                    $sql.= ", cfrev1 = ".$rev;
                    $sql.= ", cfop1 = ".$op;
                    $sql.= ", cfcp1 = ".$cap;
                    include("inc_db_con.php");
                    $lineid = mysql_insert_id($con);
        $tsql = $sql;
        $told = "";
        $trans = "Added line item ".$lineid;
        include("inc_transaction_log.php");
                }
            }
        }
    }
    echo("<p>Line item '".$val."' successfully added.</p>");
    $urlback = "admin_fin_cf_setup.php";
    include("inc_goback.php");

}
else
{
    echo("<p>An error has occurred.  Please go back and try again.</p>");
    $urlback = "admin_fin_cf_add.php";
    include("inc_goback.php");
}



?>
<?php
?>
<p>&nbsp;</p>
</body>
</html>
