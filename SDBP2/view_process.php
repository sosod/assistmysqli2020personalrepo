<?php
    include("inc_ignite.php");
    $r = 1;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>

<style type=text/css>
.tdgeneral {
    border-left: 0px solid #ffffff;
//    padding-left: 10px;
}
.tdheaderl {
    border-right: 0px solid #ffffff;
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<script type=text/javascript>
function changeDisplay() {
    var disp = document.getElementById('dis').value;
    document.getElementById('disp').value = disp;
    document.forms['dd'].submit();
}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
$what = $_POST['w1'];
$who = $_POST['dir'];
$when = $_POST['w2'];
$graph = $_POST['g'];
$err = "N";
    $display = $_POST['disp'];
    if(strlen($display)==0) { $display = $_GET['disp']; }
    if(strlen($display)==0) { $display = "sum"; }

?>
<form name=dd id=dd action=view_process.php method=post>
<input type=hidden name=w1 value="<?php echo($what); ?>">
<input type=hidden name=w2 value="<?php echo($when); ?>">
<input type=hidden name=dir value="<?php echo($who); ?>">
<input type=hidden name=g value="<?php echo($graph); ?>">
<?php

if($when=="SEL")
{
    $whenfrom = $_POST['wf'];
    $whento = $_POST['wt'];
    if(strlen($whenfrom)==0 || strlen($whento)==0)
    {
        $err = "Y";
    }
}
?>
<input type=hidden name=wf value="<?php echo($whenfrom); ?>">
<input type=hidden name=wt value="<?php echo($whento); ?>">
<input type=hidden name=disp id=disp value="<?php echo($display); ?>">
</form>
<?php

if($when=="SEL")
{
    $whenfrom = $_POST['wf'];
    $whento = $_POST['wt'];
    if(strlen($whenfrom)==0 || strlen($whento)==0)
    {
        $err = "Y";
    }
}

if(strlen($what)>0 && $err=="N")
{
    switch($what)
    {
        case "CF":
            if(strlen($who)==0 && strlen($when)==0 ) {
                $err = "Y";
            } else {
                $url = "view_fin_cf.php";
            }
            break;
        case "CP":
            if(strlen($who)==0 && strlen($when)==0 ) {
                $err = "Y";
            } else {
                $url = "view_cp.php";
            }
            break;
        case "KPI":
            if(strlen($who)==0 && strlen($when)==0 ) {
                $err = "Y";
            } else {
                $url = "view_kpi.php";
            }
            break;
        case "RS":
            $url = "view_fin_rs.php";
            break;
        default:
            if(strlen($who)==0 && strlen($when)==0 ) {
                $err = "Y";
            } else {
                $url = "view_kpi.php";
            }
            break;
    }
}
else
{
    $err = "Y";
}

if($err=="Y")
{
    ?>
    <h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: View</b></h1>
    <h2>Error!</h2>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
    $urlback = "view.php";
    include("inc_goback.php");
}
else
{
    if(strlen($graph)==0) { $graph = "N"; }
    include($url);
    ?>
    <form id=frm action=<?php echo($url); ?> method=POST>
        <input type=hidden name=who value=<?php echo($who); ?>>
        <input type=hidden name=when value=<?php echo($when); ?>>
        <input type=hidden name=wto value=<?php echo($whento); ?>>
        <input type=hidden name=wfrom value=<?php echo($whenfrom); ?>>
        <input type=hidden name=graph value=<?php echo($graph); ?>>
    </form>
    <?php
}
?>
</body>
</html>
