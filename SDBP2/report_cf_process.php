<?php
    include("inc_ignite.php");

/* START OF FUNCTIONS */

//function decode($str) {
//    return html_entity_decode($str, ENT_QUOTES, "ISO-8859-1");
//}

function displayDirTot($dir,$c,$tot,$dir0) {
    global $field1;
    global $output;
    $display = "";
    if(count($tot)>0)
    {
        if($output=="csv")
        {
            $display.= "\"Total for $dir0:\",\"\",\"\"";
            $end = "\r\n";
        }
        else
        {
            $display.= "<tr><td colspan=2 class=\"tdgeneral dirtotal\" style=\"border-right: 2px solid #cc0001;\"> Total for $dir0: </td>";
            $end = "</tr>";
        }
        $fcount = count($field1);
        $fct = 0;
        if($field1['revenue']=="Y")
        {
            $fct++;
            $display.=displaySec("cfrev",$tot,$fct,$fcount,"dirtotal");
        }
        if($field1['opex']=="Y")
        {
            $fct++;
            $display.=displaySec("cfop",$tot,$fct,$fcount,"dirtotal");
        }
        if($field1['capex']=="Y")
        {
            $fct++;
            $display.=displaySec("cfcp",$tot,$fct,$fcount,"dirtotal");
        }
        $display.=$end;
    }
    return $display;
}


function displayDir($dir,$c,$tot,$dir0) {
    global $field1;
    global $output;
    $display = "";
    $c++; $c++;
    if($dir0!="")
    {
        if($output == "csv")
        {
            $display.= "\"\"\r\n";
        }
        else
        {
            $display.= "<tr><td colspan=$c height=5><img src=\"../pics/blank.gif\" height=5></td></tr>";
        }
    }
    if($output == "csv")
    {
        $display.= "\"$dir\"\r\n";
    }
    else
    {
        $display.= "<tr><td colspan=$c class=\"tdgeneral dir\"> $dir </td></tr>";
    }
    return $display;
}

function displaySubTot($dir,$c,$tot,$dir0) {
    global $field1;
    global $output;
    $display = "";
    if(count($tot)>0)
    {
        if($output == "csv")
        {
            $display.= "\"\",\"Total for $dir0:\",\"\"";
            $end = "\r\n";
        }
        else
        {
            $display.= "<tr><td colspan=2 class=\"tdgeneral subtotal\" style=\"border-right: 2px solid #cc0001;\"> Total for $dir0: </td>";
            $end = "</tr>";
        }
        $fcount = count($field1);
        $fct = 0;
        if($field1['revenue']=="Y")
        {
            $fct++;
            $display.=displaySec("cfrev",$tot,$fct,$fcount,"subtotal");
        }
        if($field1['opex']=="Y")
        {
            $fct++;
            $display.=displaySec("cfop",$tot,$fct,$fcount,"subtotal");
        }
        if($field1['capex']=="Y")
        {
            $fct++;
            $display.=displaySec("cfcp",$tot,$fct,$fcount,"subtotal");
        }

        $display.=$end;
    }
    return $display;
}


function displaySub($dir,$c,$tot,$dir0) {
    global $field1;
    global $output;
    $display = "";
    $c++; $c++;
    if($output == "csv")
    {
        $display.= "\"$dir\"\r\n";
    }
    else
    {
        $display.= "<tr><td colspan=$c class=\"tdgeneral sub\"> $dir </td></tr>";
    }
    return $display;
}

function displaySec($col,$row,$fct,$fc,$class) {
    global $head;
    global $field;
    global $fields;
    global $output;
    $colcount = count($field);
    if($fct<$fc)
    {
        $style0 = "style=\"border-right: 2px solid #cc0001;\"";
    }
    else
    {
        $style0 = "";
    }
            $colc = 0;
            $display = "";
            if($output == "csv")
            {
                $begin = ",\"";
                $end = "\"";
            }
            else
            {
                $begin = "<td class=\"tdgeneral $class\" align=right $style >";
                $end = "</td>";
            }
            foreach($head as $h)
            {
                $hf = $h['headfield'];
                if($field[$hf]=="Y")
                {
                    $fld = $col.$fields[$hf];
                    $colc++;
                    if($colc==$colcount)
                    {
                        $style=$style0;
                    }
                    else
                    {
                        $style="";
                    }
                    if($hf!="accytd")
                    {
                        $display.=$begin.number_format($row[$fld],2).$end;
                    }
                    else
                    {
                        if($class!="dirtotal" && $class!="subtotal")
                        {
                            $display.=$begin.number_format($row[$fld],2)."%".$end;
                        }
                        else
                        {
                            $display.=$begin." ".$end;
                        }
                    }
                }
            }
        return $display;
}

function displayTable($sql,$title) {
    global $cmpcode;
    global $field1;
    global $head;
    global $field;
    global $fields;
    global $output;
    include("inc_db_con.php");
    ?>
<?php echo($title); ?>
<table cellpadding=3 cellspacing=0 >
    <tr>
        <th class=tdheader rowspan=2>Line Item</th>
        <th class=tdheader rowspan=2 style="border-right: 2px solid #ffffff;">Vote</th>
        <?php
        if($field1['revenue']=="Y") { echo("<th height=30 class=tdheader colspan=".count($field)." style=\"border-right: 2px solid #ffffff;\">Revenue</th>"); }
        if($field1['opex']=="Y") { echo("<th height=30 class=tdheader colspan=".count($field)." style=\"border-right: 2px solid #ffffff;\">Operational Expenditure</th>"); }
        if($field1['capex']=="Y") { echo("<th height=30 class=tdheader colspan=".count($field).">Capital Expenditure</th>"); }
        ?>
    </tr>
    <tr>
        <?php
        $colcount = count($field);
        foreach($field1 as $f1)
        {
            $colc = 0;
            foreach($head as $h)
            {
                if($field[$h['headfield']]=="Y")
                {
                    $colc++;
                    if($colc==$colcount)
                    {
                        $style="style=\"border-right: 2px solid #ffffff;\"";
                    }
                    else
                    {
                        $style = "";
                    }
                    echo("<td class=tdheader $style >".$h['headlist']."</td>");
                }
            }
        }
        ?>
    </tr>
    <?php
    $display = "";
    $dirtxt0 = "";
    $dtot = array();
    $subtxt0 = "";
    $stot = array();
    while($row = mysql_fetch_array($rs))
    {
        $dtxt = $row['dirtxt'];
        $stxt = $row['subtxt'];
        if($dtxt != $dirtxt0)
        {
            $dc = $colcount*count($field1);
            $display.=displaySubTot($stxt,$dc,$stot,$subtxt0);
            $stot = array();
            $display.=displayDirTot($dtxt,$dc,$dtot,$dirtxt0);
            $dtot = array();
            $display.=displayDir($dtxt,$dc,$dtot,$dirtxt0);
            $dirtxt0 = $dtxt;
        }
        if($stxt != $subtxt0)
        {
            $sc = $colcount*count($field1);
            $display.=displaySubTot($stxt,$sc,$stot,$subtxt0);
            $stot = array();
            $display.=displaySub($stxt,$sc,$stot,$subtxt0);
            $subtxt0 = $stxt;
        }
        $display.= chr(10)."<tr>";
        $display.= chr(10)."<td class=tdgeneral>".str_replace(" ","&nbsp;",$row['linevalue'])."</td>";
        $display.= chr(10)."<td class=tdgeneral align=center style=\"border-right: 2px solid #cc0001;\">".$row['linevote']."</td>";
        $fcount = count($field1);
        $fct = 0;
        if($field1['revenue']=="Y")
        {
            $fct++;
            for($r=1;$r<9;$r++)
            {
                $cr = "cfrev".$r;
                $dtot[$cr] = $dtot[$cr]+$row[$cr];
                $stot[$cr] = $stot[$cr]+$row[$cr];
            }
            $display.=displaySec("cfrev",$row,$fct,$fcount,"");
        }
        if($field1['opex']=="Y")
        {
            $fct++;
            for($r=1;$r<9;$r++)
            {
                $cr = "cfop".$r;
                $dtot[$cr] = $dtot[$cr]+$row[$cr];
                $stot[$cr] = $stot[$cr]+$row[$cr];
            }
            $display.=displaySec("cfop",$row,$fct,$fcount,"");
        }
        if($field1['capex']=="Y")
        {
            $fct++;
            for($r=1;$r<9;$r++)
            {
                $cr = "cfcp".$r;
                $dtot[$cr] = $dtot[$cr]+$row[$cr];
                $stot[$cr] = $stot[$cr]+$row[$cr];
            }
            $display.=displaySec("cfcp",$row,$fct,$fcount,"");
        }
        $display.= chr(10)."</tr>";
    }
    mysql_close($con);
            $dc = $colcount*count($field1);
            $display.=displaySubTot($stxt,$dc,$stot,$subtxt0);
            $display.=displayDirTot($dtxt,$dc,$dtot,$dirtxt0);

    echo($display);
    ?>
    </table>
    <?php
}

function csvTable($sql,$title) {
    global $cmpcode;
    global $field1;
    global $head;
    global $field;
    global $fields;
    global $output;
    include("inc_db_con.php");
$data = "";
if($title!="Summary")
{
    $data.= "\"\"\r\n\"\"\r\n";
}
$data.= "\" ".$title." \"\r\n";
$data.= "\"\",\"Line Item\",\"Vote\"";

        if($field1['revenue']=="Y")
        {
            $data.=",\"Revenue\"";
            for($c=2;$c<=count($field);$c++)
            {
                $data.=",\"\"";
            }
        }
        if($field1['opex']=="Y")
        {
            $data.=",\"Operational Expenditure\"";
            for($c=2;$c<=count($field);$c++)
            {
                $data.=",\"\"";
            }
        }
        if($field1['capex']=="Y")
        {
            $data.=",\"Capital Expenditure\"";
            for($c=2;$c<=count($field);$c++)
            {
                $data.=",\"\"";
            }
        }
        $data.="\r\n";
        $data.="\"\",\"\",\"\"";
        $colcount = count($field);
        foreach($field1 as $f1)
        {
            $colc = 0;
            foreach($head as $h)
            {
                if($field[$h['headfield']]=="Y")
                {
                    $colc++;
                    $data.= ",\"".$h['headlist']."\"";
                }
            }
        }
    $data.="\r\n";
    $display = "";
    $dirtxt0 = "";
    $dtot = array();
    $subtxt0 = "";
    $stot = array();
    while($row = mysql_fetch_array($rs))
    {
        $dtxt = $row['dirtxt'];
        $stxt = $row['subtxt'];
        if($dtxt != $dirtxt0)
        {
            $dc = $colcount*count($field1);
            $display.=displaySubTot($stxt,$dc,$stot,$subtxt0);
            $stot = array();
            $display.=displayDirTot($dtxt,$dc,$dtot,$dirtxt0);
            $dtot = array();
            $display.=displayDir($dtxt,$dc,$dtot,$dirtxt0);
            $dirtxt0 = $dtxt;
        }
        if($stxt != $subtxt0)
        {
            $sc = $colcount*count($field1);
            $display.=displaySubTot($stxt,$sc,$stot,$subtxt0);
            $stot = array();
            $display.=displaySub($stxt,$sc,$stot,$subtxt0);
            $subtxt0 = $stxt;
        }
        $display.= "\"\",\"".decode($row['linevalue'])."\"";
        $display.= ",\"".decode($row['linevote'])."\"";
        $fcount = count($field1);
        $fct = 0;
        if($field1['revenue']=="Y")
        {
            $fct++;
            for($r=1;$r<9;$r++)
            {
                $cr = "cfrev".$r;
                $dtot[$cr] = $dtot[$cr]+$row[$cr];
                $stot[$cr] = $stot[$cr]+$row[$cr];
            }
            $display.=displaySec("cfrev",$row,$fct,$fcount,"");
        }
        if($field1['opex']=="Y")
        {
            $fct++;
            for($r=1;$r<9;$r++)
            {
                $cr = "cfop".$r;
                $dtot[$cr] = $dtot[$cr]+$row[$cr];
                $stot[$cr] = $stot[$cr]+$row[$cr];
            }
            $display.=displaySec("cfop",$row,$fct,$fcount,"");
        }
        if($field1['capex']=="Y")
        {
            $fct++;
            for($r=1;$r<9;$r++)
            {
                $cr = "cfcp".$r;
                $dtot[$cr] = $dtot[$cr]+$row[$cr];
                $stot[$cr] = $stot[$cr]+$row[$cr];
            }
            $display.=displaySec("cfcp",$row,$fct,$fcount,"");
        }
        $display.= "\r\n";
    }
    mysql_close($con);
            $dc = $colcount*count($field1);
            $display.=displaySubTot($stxt,$dc,$stot,$subtxt0);
            $display.=displayDirTot($dtxt,$dc,$dtot,$dirtxt0);

    $data.=$display;
    
    return $data;

}


/*   END FUNCTIONS   */

$fields['budget'] = 1;
$fields['actual'] = 2;
$fields['accytd'] = 3;
$fields['totytd'] = 4;
$fields['var'] = 5;
$fields['vire'] = 6;
$fields['adjest'] = 7;
$fields['adjbudget'] = 8;

$field = array();
$filter = array();

$filter['dir'] = $_POST['dirfilter'];
$filter['sub'] = $_POST['subfilter'];
$filter['time'] = $_POST['time'];

$field1['revenue'] = $_POST['revenue'];
$field1['opex'] = $_POST['opex'];
$field1['capex'] = $_POST['capex'];

if($field1['revenue'] != "Y") { unset($field1['revenue']); }
if($field1['opex'] != "Y") { unset($field1['opex']); }
if($field1['capex'] != "Y") { unset($field1['capex']); }


$output = $_POST['output'];

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CF' AND headyn = 'Y' AND headdetailyn = 'Y' AND headdetailsort>0 ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $head[$row['headfield']] = $row;
        $val = $_POST[$row['headfield']];
        if($val=="Y")
        {
            $field[$row['headfield']] = "Y";
        }
    }
mysql_close();
//echo("<P>");
//print_r($field);
$sql0 = " FROM assist_".$cmpcode."_".$modref."_finance_cashflow c";
$sql0.= ", assist_".$cmpcode."_".$modref."_finance_lineitems l";
$sql0.= ", assist_".$cmpcode."_".$modref."_dirsub s";
$sql0.= ", assist_".$cmpcode."_".$modref."_dir d";
$sql0.= " WHERE c.cflineid = l.lineid";
$sql0.= " AND l.linesubid = s.subid";
$sql0.= " AND s.subdirid = d.dirid";
$sql0.= " AND d.diryn = 'Y'";
$sql0.= " AND s.subyn = 'Y'";
$sql0.= " AND l.lineyn = 'Y'";
$sql0.= " AND c.cfyn = 'Y'";

$ff = $filter['dir'];
if($ff!="ALL" && is_numeric($ff) && strlen($ff)>0)
{
    $sql0.= " AND d.dirid = ".$ff;
}
$ff = $filter['sub'];
if($ff!="ALL" && is_numeric($ff) && strlen($ff)>0)
{
    $sql0.= " AND s.subid = ".$ff;
}
/*$ff = $filter['time'];
if(count($ff)>0)
{
    $sql.= " AND (c.cftimeid = ".implode(" OR c.cftimeid = ",$ff).")";
}
*/
$sql2.= " ORDER BY d.dirsort, s.subsort, l.linesort";
//include("inc_db_con.php");

if($output == "csv")
{


$ff = $filter['time'];
if(count($ff)>0)
{
    $data = "";
    //TOTAL
    $title = "Summary";
    $sqlt = "SELECT d.dirtxt, s.subtxt, l.linevalue, l.linevote";
    $sqlt.= ", sum(c.cfrev1) as cfrev1, sum(c.cfrev2) as cfrev2, sum(c.cfrev3) as cfrev3, sum(c.cfrev4) as cfrev4, sum(c.cfrev5) as cfrev5, sum(c.cfrev6) as cfrev6, sum(c.cfrev7) as cfrev7, sum(c.cfrev8) as cfrev8";
    $sqlt.= ", sum(c.cfop1) as cfop1, sum(c.cfop2) as cfop2, sum(c.cfop3) as cfop3, sum(c.cfop4) as cfop4, sum(c.cfop5) as cfop5, sum(c.cfop6) as cfop6, sum(c.cfop7) as cfop7, sum(c.cfop8) as cfop8";
    $sqlt.= ", sum(c.cfcp1) as cfcp1, sum(c.cfcp2) as cfcp2, sum(c.cfcp3) as cfcp3, sum(c.cfcp4) as cfcp4, sum(c.cfcp5) as cfcp5, sum(c.cfcp6) as cfcp6, sum(c.cfcp7) as cfcp7, sum(c.cfcp8) as cfcp8";
    $sqlt.= $sql0." AND (c.cftimeid = ".implode(" OR c.cftimeid = ",$ff).") GROUP BY d.dirtxt, s.subtxt, l.linevalue, l.linevote ".$sql2;
    $data.=csvTable($sqlt,$title);
    //MONTHS
    foreach($ff as $ft)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$ft;
        include("inc_db_con.php");
            $time = mysql_fetch_array($rs);
        mysql_close();
        $title = date("F Y",$time['eval']);
        $sqlt = "SELECT d.dirtxt, s.subtxt, l.linevalue, l.linevote, c.* ".$sql0." AND c.cftimeid = ".$ft." ".$sql2;
        $data.=csvTable($sqlt,$title);
//        echo("<P>".$sqlt);
    }
//    echo($data);
        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/sdbip_cf_report_".date("Ymd_His",$today).".csv";
        $newfilename = "sdbip_cashflow_report_".date("Ymd_His",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$data."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);
}
else
{
    echo("<script type=text/javascript>alert('No data matches the criteria you selected.<br>Please go back and try again.'); document.location.href = 'report_cf.php'; </p>");
}


}
else
{
//print_r($field);
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    .tdheader {
        border-width: 1px;
        border-style: solid;
        border-color: #ffffff;
    }
    .dir {
        background-color: #333333;
        color: #ffffff;
        font-weight: bold;
    }
    .dirtotal {
        background-color: #a9a9a9;
        color: #000000;
        font-weight: bold;
        text-align: right;
    }
    .sub {
        background-color: #666666;
        color: #ffffff;
        font-weight: bold;
    }
    .subtotal {
        background-color: #cccccc;
        color: #000000;
        font-weight: bold;
        text-align: right;
    }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - Monthly Cashflow</b></h1>
<?php
$ff = $filter['time'];
if(count($ff)>0)
{
    //TOTAL
    $title = "<h2 class=fc>Summary</h2>";
    $sqlt = "SELECT d.dirtxt, s.subtxt, l.linevalue, l.linevote";
    $sqlt.= ", sum(c.cfrev1) as cfrev1, sum(c.cfrev2) as cfrev2, sum(c.cfrev3) as cfrev3, sum(c.cfrev4) as cfrev4, sum(c.cfrev5) as cfrev5, sum(c.cfrev6) as cfrev6, sum(c.cfrev7) as cfrev7, sum(c.cfrev8) as cfrev8";
    $sqlt.= ", sum(c.cfop1) as cfop1, sum(c.cfop2) as cfop2, sum(c.cfop3) as cfop3, sum(c.cfop4) as cfop4, sum(c.cfop5) as cfop5, sum(c.cfop6) as cfop6, sum(c.cfop7) as cfop7, sum(c.cfop8) as cfop8";
    $sqlt.= ", sum(c.cfcp1) as cfcp1, sum(c.cfcp2) as cfcp2, sum(c.cfcp3) as cfcp3, sum(c.cfcp4) as cfcp4, sum(c.cfcp5) as cfcp5, sum(c.cfcp6) as cfcp6, sum(c.cfcp7) as cfcp7, sum(c.cfcp8) as cfcp8";
    $sqlt.= $sql0." AND (c.cftimeid = ".implode(" OR c.cftimeid = ",$ff).") GROUP BY d.dirtxt, s.subtxt, l.linevalue, l.linevote ".$sql2;
    displayTable($sqlt,$title);
    //MONTHS
    foreach($ff as $ft)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$ft;
        include("inc_db_con.php");
            $time = mysql_fetch_array($rs);
        mysql_close();
        $title = "<h2 class style=\"page-break-before: always;\">".date("F Y",$time['eval'])."</h2>";
        $sqlt = "SELECT d.dirtxt, s.subtxt, l.linevalue, l.linevote, c.* ".$sql0." AND c.cftimeid = ".$ft." ".$sql2;
        displayTable($sqlt,$title);
//        echo("<P>".$sqlt);
    }
}
else
{
    echo("<P>No data matches the criteria you selected.<br>Please go back and try again.</p>");
}
?>
<p>&nbsp;</p></body></html>
<?php
}

?>
