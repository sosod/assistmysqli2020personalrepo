<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>

<base target="main">
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var pos = me.pos.value;
    var valid8 = "true";
    
    if(val.length==0)
    {
        document.getElementById('val').className = "reqtext";
        valid8 = "false";
    }
    else
    {
        document.getElementById('val').className = "blank";
    }
    if(pos.length==0 || (pos != "f" && pos != "l" && pos != "b" && pos != "a"))
    {
        me.pos.value = "l";
    }
    else
    {
        if(pos == "a" || pos == "b")
        {
            var poslist = me.poslist.value;
            if(poslist.length == 0 || poslist == "X" || isNaN(parseInt(poslist)))
            {
                document.getElementById('poslist').className = "reqtext";
                valid8 = "false";
            }
            else
            {
                document.getElementById('poslist').className = "blank";
            }
        }
    }

    if(valid8=="true")
    {
        return true;
    }
    else
    {
        alert("Please complete all missing field(s) as indicated.");
        return false;
    }
    return false;
}
    function chgPos(me) {
        if(me.value == "f" || me.value == "l")
        {
            document.getElementById('poslist').disabled = true;
        }
        else
        {
            document.getElementById('poslist').disabled = false;
        }
    }
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Add Sub-Directorate</b></h1>
<p>&nbsp;</p>
<?php
    $dirid = $_GET['d'];
    $err = "Y";
    
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $dir = mysql_fetch_array($rs);
            $err = "N";
        }
    mysql_close();

    if($err == "N")
    {
?>
<form name=add method=post action=setup_dir_sub_add_process.php onsubmit="return Validate(this);" language=jscript>
<table cellpadding=3 cellspacing=0 width=570 id="table1">
	<tr>
		<td class=tdheaderl width=130>Directorate:</td>
		<td class=TDgeneral><?php echo(html_entity_decode($dir['dirtxt'],ENT_QUOTES,"ISO-8859-1")); ?><input type=hidden name=dirid value=<?php echo($dirid); ?>>&nbsp;</td>
		<td class=TDgeneral width=110><img src="/pics/blank.gif" width=1 height=19>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdheaderl width=130>Sub-Directorate:</td>
		<td class=TDgeneral><input type=text size="50" value="" name=val id=val>&nbsp;</td>
		<td class=TDgeneral width=110><small>Max 50 characters</small></td>
	</tr>
	<?php
	$pos = "l";
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subyn = 'Y' AND subhead = 'N' ORDER BY subsort DESC";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)==0) {
            $pos = "f";
        }
    mysql_close();
    ?>
	<tr>
		<td class=tdheaderl valign=top height=27>Display position:</td>
		<td class=tdgeneral valign=top colspan="2"><select id=pos onchange="chgPos(this)" name=pos>
			<option <?php if($pos=="f") { echo("selected"); } ?> value=f>First</option>
			<option value=b>Before</option>
			<option <?php if($pos=="a") { echo("selected"); } ?> value=a>After</option>
			<option <?php if($pos=="l") { echo("selected"); } ?> value=l>Last</option>
		</select><select id=poslist name=poslist><option selected value=X>--- SELECT ---</option><?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid." AND subhead <> 'Y' ORDER BY subsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['subid'];
            $val = $row['subtxt'];
            echo("<option ");
            echo("value=".$id.">".$val."</option>");
        }
    mysql_close();
    $r = 1;
?></select>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdheaderl valign=top>&nbsp;</td>
		<td class=tdgeneral valign=top colspan="2"><input type=submit value=" Add "></td>
	</tr>
</table>
<?php
    }    else    {    //IF NO ERROR
        echo("<p>An error has occurred.  Please go back and try again.</p>");
    }
?>
</form>
<script type=text/javascript>
var pos = document.getElementById('pos').value;
if(pos == "f" || pos == "l")
{
    document.getElementById('poslist').disabled = true;
}
</script>
<?php
$urlback = "setup_dir_sub.php?d=".$dirid;
include("inc_goback.php");
?>
</body>
</html>
