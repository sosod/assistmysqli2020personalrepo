<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function updateCP() {
    var d = document.getElementById('dir').value;
    if(d!="X")
    {
        if(!isNaN(parseInt(d)))
        {
            if(parseInt(d)>0)
            {
                document.location.href = "admin_fin_cp_update.php?d="+d;
            }
            else
            {
                alert("Please select a Directorate from the drop down list.");
            }
        }
        else
        {
            alert("Please select a Directorate from the drop down list.");
        }
    }
    else
    {
        alert("Please select a Directorate from the drop down list.");
    }
}
function updateCF() {
    var t = document.getElementById('t').value;
    if(t!="X")
    {
        if(!isNaN(parseInt(t)))
        {
            if(parseInt(t)>0)
            {
                document.location.href = "admin_fin_cf_update.php?t="+t;
            }
            else
            {
                alert("Please select a time period from the drop down list.");
            }
        }
        else
        {
            alert("Please select a time period from the drop down list.");
        }
    }
    else
    {
        alert("Please select a time period from the drop down list.");
    }
}

function updateRS() {
    var t = document.getElementById('t2').value;
    if(t!="X")
    {
        if(!isNaN(parseInt(t)))
        {
            if(parseInt(t)>0)
            {
                document.location.href = "admin_fin_rs_update.php?t="+t;
            }
            else
            {
                alert("Please select a time period from the drop down list.");
            }
        }
        else
        {
            alert("Please select a time period from the drop down list.");
        }
    }
    else
    {
        alert("Please select a time period from the drop down list.");
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<?php
$referer = "-".$_SERVER['HTTP_REFERER']."-";
$referer2 = explode("/",$referer);
//if($referer=="--" || $referer2[2]!="ignite-jeb")
//if($referer=="--" || $referer2[2]!="assist.ignite4u.co.za")
//        die("<P>You are not authorised to view this page.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");

?>
<p>&nbsp;</p>
<?php $width=170; ?>
<table cellpadding=5 cellspacing=0 width=570>
	<tr>
		<td style="border-bottom: 1px solid #ffffff;" width=80 class=tdheaderl rowspan=2 style="text-align: center;">Capital<br>Projects</td>
		<td style="border-bottom: 1px solid #dddddd; border-right: 0px solid #ffffff;" class=tdgeneral width=<?php echo($width); ?>>Update financial information</td>
		<td style="border-bottom: 1px solid #dddddd; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value=Update onclick="document.location.href = 'admin_fin_cp_update.php';" id=1></td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #ababab; border-right: 0px solid #ffffff;" class=tdgeneral width=<?php echo($width); ?>>Add new capital project</td>
		<td style="border-bottom: 1px solid #ababab; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value=" Add " onclick="document.location.href = 'admin_fin_cp_add.php';"></td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #ffffff;" width=80 class=tdheaderl rowspan=2 style="text-align: center;">Monthly<Br>Cashflows</td>
		<td style="border-bottom: 1px solid #dddddd; border-right: 0px solid #ffffff;" class=tdgeneral width=<?php echo($width); ?>>Update Monthly Cashflows</td>
		<td style="border-bottom: 1px solid #dddddd; border-left: 0px solid #ffffff;" class=tdgeneral align=right><select name=t id=t>
		<option selected value=X>--- SELECT ---</option>
		<?php
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND active = 'Y' ORDER BY sort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                ?>
                <option value=<?php echo($row['id']); ?>><?php echo(date("F Y",$row['eval'])); ?></option>
                <?php
            }
        mysql_close();
        ?>
        </select><input type=button value=Update onclick="updateCF();" id=2></td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #ababab; border-right: 0px solid #ffffff;" class=tdgeneral width=<?php echo($width); ?>>Setup Line Items</td>
		<td style="border-bottom: 1px solid #ababab; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value=Setup onclick="document.location.href = 'admin_fin_cf_setup.php';"></td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #ffffff;" width=80 class=tdheaderl rowspan=2 style="text-align: center;">Revenue By<Br>Source</td>
		<td style="border-bottom: 1px solid #dddddd; border-right: 0px solid #ffffff;" class=tdgeneral width=<?php echo($width); ?>>Update Revenue By Source</td>
		<td style="border-bottom: 1px solid #dddddd; border-left: 0px solid #ffffff;" class=tdgeneral align=right><select name=t id=t2>
		<option selected value=X>--- SELECT ---</option>
		<?php
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND active = 'Y' ORDER BY sort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                ?>
                <option value=<?php echo($row['id']); ?>><?php echo(date("F Y",$row['eval'])); ?></option>
                <?php
            }
        mysql_close();
        ?>
        </select><input type=button value=Update onclick="updateRS();" id=3></td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #ababab; border-right: 0px solid #ffffff;" class=tdgeneral width=<?php echo($width); ?>>Setup Line Items</td>
		<td style="border-bottom: 1px solid #ababab; border-left: 0px solid #ffffff;" class=tdgeneral align=right><input type=button value=Setup onclick="document.location.href = 'admin_fin_rs_setup.php';"></td>
	</tr>
</table>
<script type=text/javascript>
//document.getElementById('1').disabled = true;
//document.getElementById('2').disabled = true;
//document.getElementById('3').disabled = true;
</script>
<?php
$urlback = "admin.php";
include("inc_goback.php");
?>
</body>

</html>
