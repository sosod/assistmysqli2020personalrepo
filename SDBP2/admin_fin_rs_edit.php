<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var sub = me.subid.value;
    var valid8 = "true";
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
            document.getElementById('val').className = 'reqmet';
    }
    if(sub.length==0 || sub == "X" || sub == "dX")
    {
        document.getElementById('subid').className = 'reqtext';
        document.getElementById('subid').value = "X";
        if(valid8=="true")
        {
            document.getElementById('subid').focus();
        }
        valid8 = "false";
    }
    else
    {
            document.getElementById('subid').className = 'reqmet';
    }
    if(valid8 == "false")
    {
        alert("Please complete all missing field(s) as indicated.");
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

function deleteLine(l) {
    if(!isNaN(parseInt(l)))
    {
        if(parseInt(l)>0)
        {
            if(confirm("Are you sure you wish to delete this line item?")==true)
            {
                document.location.href = "admin_fin_rs_delete.php?r="+l;
            }
        }
        else
        {
            alert("An error has occurred.  Please reload the page and try again.");
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Revenue By Source ~ Edit Line Item</b></h1>
<p>&nbsp;</p>
<?php
$lineid = $_GET['r'];
$line = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_revbysource WHERE rsid = ".$lineid." AND rsyn = 'Y'";
include("inc_db_con.php");
    $line = mysql_fetch_array($rs);
mysql_close();
?>
<form name=edit method=post action=admin_fin_rs_edit_process.php onsubmit="return Validate(this);">
<input type=hidden name=lineid value=<?php echo($lineid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=110 height=27 >Ref:</td>
		<td class=tdgeneral><?php echo($lineid); ?></td>
	</tr>
    <tr>
		<td class=tdheaderl width=110>Line Item:</td>
		<td class=tdgeneral valign=top><input type=text size=50 maxlength=100 name=val id=val value="<?php echo($line['rsline']); ?>"> <span style="font-size: 7.5pt;">(max 100 characters)</span></td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral colspan=1 valign=top><input type=submit value=" Update "> <input type=button value="Delete" onclick="deleteLine(<?php echo($lineid); ?>);"> <input type=reset value="Reset"></td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_fin_rs_setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
