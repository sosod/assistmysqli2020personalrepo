<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>

<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup</b></h1>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td style="border-bottom: 1px solid #ffffff;" width=120 class=tdheaderl>Directorates</td>
		<td style="border-bottom: 1px solid #ababab;" class=tdgeneral>Configure Directorate and their Sub-Directorates</td>
		<td style="border-bottom: 1px solid #ababab;" width=100 class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_dir.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>Administrators</td>
		<td class=tdgeneral>Configure the users who can update the SDBIP</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_admin.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>Time Periods</td>
		<td class=tdgeneral>Setup and maintain the time periods</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_time.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>Lists</td>
		<td class=tdgeneral>Define drop down list items</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_lists.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>Project Life Cycle</td>
		<td class=tdgeneral>Define phases for Project Life Cycle</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_plc.php';"></td>
	</tr>
</table>

<p>&nbsp;</p>
</body>

</html>
