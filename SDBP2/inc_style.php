<?php
include("../styles/SDBP2.php");
?>
<script language=JavaScript>
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
            var pbar = 0;
            var pb2 = 0;
			function incProg(pbar) {
                pbar = parseInt(pbar);
                lbl2.innerText=pbar+"%";
                pb2 = pbar * 2;
                document.getElementById('tbl3').width = pb2;
                document.getElementById('lbl2').innerText = pbar+'%';
			}

			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
</script>
<style type="text/css">
.blank { background-color: #ffffff; }
.tdhover { background-color: #e1e1e1; }
.reqtext {
    background-color: #eb5050;
    color: #ffffff;
}
.reqmet {
    background-color: #ffffff;
    color: #000000;
}
table {
    border-color: #ababab;
    border-collapse: collapse;
    border-style: solid;
    border-width: 1px;
}
table td {
    border-color: #ababab;
    border-width: 1px;
    border-style: solid;
}
.tdlog {
	color: #555555;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7.5pt;
	line-height: 10pt;
    border-left: 1px solid #cecece;
    border-right: 1px solid #cecece;
    border-bottom: 1px solid #ababab;
}

.tdloghead {
	color: #ffffff;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7.5pt;
	line-height: 10pt;
    border-left: 1px solid #cecece;
    border-right: 1px solid #cecece;
    border-bottom: 1px solid #ababab;
    background-color: #909090;
    font-weight: bold;
    text-align: center;
}
.lab1
{
    font-weight: bold;
    color: 006600;
}
.lab2
{
    background-color: #cc0001;
    color: #cc0001;
}
.lab3
{
    background-color: #006600;
    color: #006600;
}
</style>

