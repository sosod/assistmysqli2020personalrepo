<?php
    include("inc_ignite.php");
    
    $fileloc = $_POST['floc'];
    $dir = $_POST['dir'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import ~ Municipal KPAs<input type=hidden id=lab></b></h1>
<?php
if(strlen($dir)>0 && is_numeric($dir))
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dir;
    include("inc_db_con.php");
        $dirrow = mysql_fetch_array($rs);
    mysql_close();
?>
<center>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script><p>Note: The import process will not be complete until you click the "Accept" button below the table.</p>
<?php
$err = "N";

$p=0;
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = "kpi_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}
else
{
//    echo("<script type=text/javascript>document.getElementById('floc').value = '".$fileloc."';</script>");
}

if($err == "N")
{
    $r=0;
            $file = fopen($fileloc,"r");
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $r++;
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>1 && $r>4)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <form id=accept method=post action=import_kpi_accept.php><input type=hidden name=floc value="<?php echo($fileloc); ?>"><input type=hidden name=dir value="<?php echo($dir); ?>">
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader colspan=2>&nbsp;</td>
                        <td class=tdheader>Directorate</td>
                        <td class=tdheader>Sub-Directorate</td>
                        <td class=tdheader>Municipal KPA</td>
                        <td class=tdheader>National KPA</td>
                        <td class=tdheader>Objective</td>
                        <td class=tdheader>KPI Name</td>
                        <td class=tdheader>CPID</td>
                        <td class=tdheader>KPI Type</td>
                        <td class=tdheader>S/O/P</td>
                        <td class=tdheader>KPI Definition</td>
                        <td class=tdheader>Wards</td>
                        <td class=tdheader>Area</td>
                        <td class=tdheader>Driver</td>
                        <td class=tdheader>Baseline</td>
                        <td class=tdheader>Target Unit</td>
                        <td class=tdheader>Calculation Type</td>
                        <td class=tdheader>Target Type</td>
                        <?php
                        $time = array();
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
                        include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs))
                            {
                                echo("<td class=tdheader>".date("m-Y",$row['eval'])."</td>");
                                $time[] = $row;
                            }
                        mysql_close();
                        ?>
                    </tr>
                    <?php
                    $sql = "SELECT max(kpiid) as id FROM assist_".$cmpcode."_".$modref."_kpi";
                    include("inc_db_con.php");
                        $row1 = mysql_fetch_array($rs);
                    mysql_close();
                    $p2 = 100 / count($data);
                    $id = $row1['id'];
                    foreach($data as $idata)
                    {
                        $err = "N";
                        $id++;
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $sub = trim($idata[0]);
                        if(strlen($sub)>0)
                        {
                            $sub = htmlentities($sub,ENT_QUOTES,"ISO-8859-1");
                            $sub = substr($sub,0,100);
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subtxt = '".$sub."' AND subyn = 'Y' AND subdirid = ".$dir;
//                            echo("<script type=text/javascript>alert(\"".$sql."\");</script>");
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs)>0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $sub.= " (".$row['subid'].")";
                                }
                                else
                                {
                                    $sub.= " (NEW)";
                                }
                            mysql_close();
                        }

                        $idp = trim($idata[1]);

                        $munkpa = trim($idata[2]);
                        if(strlen($munkpa)>0)
                        {
                            $munkpa = htmlentities($munkpa,ENT_QUOTES,"ISO-8859-1");
                            $munkpa = substr($munkpa,0,200);
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE code = '".$munkpa."' AND yn = 'Y'";
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs)>0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $munid = $row['id'];
                                }
                                else
                                {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE value = '".$munkpa."' AND yn = 'Y'";
                                    include("inc_db_con2.php");
                                    if(mysql_num_rows($rs2)>0)
                                    {
                                        $row = mysql_fetch_array($rs2);
                                        $munid = $row['id'];
                                    }
                                    else
                                    {
                                        $munid = 0;
                                        $err = "R";
                                    }
                                    mysql_close($con2);
                                }
                            mysql_close();
                            $munkpa.= " (".$munid.")";
                        }
                        else
                        {
                            $munid = 0;
                            $err = "R";
                        }

                        $natkpa = trim($idata[3]);
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_natkpa WHERE yn = 'Y' AND code = '".$natkpa."'";
                        include("inc_db_con.php");
                            if(mysql_num_rows($rs)>0)
                            {
                                $row = mysql_fetch_array($rs);
                                $natid = $row['id'];
                            }
                            else
                            {
                                $natid = 0;
                                $err = "R";
                            }
                        mysql_close();

                        $obj = trim($idata[4]);
                        $name = trim($idata[5]);
                        $cpid = trim($idata[6]);
                        $kpitype = trim($idata[7]);
                        if(strlen($kpitype)>0 && is_numeric($kpitype))
                        {
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpitype WHERE id = ".$kpitype." AND yn = 'Y'";
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs)>0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $kpitype = $row['value']." (".$row['id'].")";
                                }
                                else
                                {
                                    $kpitype = "N/A";
                                }
                            mysql_close();
                        }
                        else
                        {
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpitype WHERE value = '".$kpitype."' AND yn = 'Y'";
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs)>0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $kpitype = $row['value']." (".$row['id'].")";
                                }
                                else
                                {
                                    $kpitype = "<font color=red>N/A</font>";
                                }
                            mysql_close();

//                            $kpitype = "N/A";
                        }
                        $sop = trim($idata[8]);
                        if(strlen($sop)>1)
                        {
                            $sop2 = substr($sop,0,1);
                            if($sop2 != "O" && $sop2 != "P" && $sop2 != "S") { $sop = "<font color=red><b>O</b></font>"; } else { $sop = $sop2; }
                        }
                        else
                        {
                            if($sop != "O" && $sop != "P" && $sop != "S") { $sop = "<font color=red><b>O</b></font>"; }
                        }
                        $definition = trim($idata[9]);
                        $wards = trim($idata[10]);
                        if(strlen($wards)==0) { $wards = "N/A"; }
                        $area = trim($idata[11]);
                        $driver = trim($idata[12]);
                        $baseline = trim($idata[13]);
                        $target = trim($idata[14]);
                        $calc = trim($idata[15]);
                        if(strtoupper($calc) != "CO" && strtoupper($calc) != "ACC" && strtoupper($calc) != "STD" && strtoupper($calc) != "STAND" && strtoupper($calc) != "ZERO") { $calc = "CO"; }
                        $ttype = trim($idata[16]);
                        if($ttype != "%" && $ttype != "#" && $ttype != "R") { $ttype = "%"; }

                        if(count($idata)>5 && strlen($sub)>0 && strlen($munid)>0 && is_numeric($munid) && $munid > 0 && $natid > 0 && $err == "N" && strlen($natkpa)>0 && strlen($name)>0)
                        {
                        ?>
                    <tr>
                        <td class=tdheader align=center><input type=checkbox name=import[] checked value=<?php echo($id); ?>></td>
                        <td class=tdheader><?php echo($id); ?></td>
                        <td class=tdgeneral><?php echo($dirrow['dirtxt']); ?></td>
                        <td class=tdgeneral><?php echo($sub); ?></td>
                        <td class=tdgeneral><?php echo($munkpa); ?></td>
                        <td class=tdgeneral><?php echo($natkpa); ?></td>
                        <td class=tdgeneral><?php echo($obj); ?></td>
                        <td class=tdgeneral><?php echo($name); ?></td>
                        <td class=tdgeneral><?php echo($cpid); ?></td>
                        <td class=tdgeneral><?php echo($kpitype); ?></td>
                        <td class=tdgeneral><?php echo($sop); ?></td>
                        <td class=tdgeneral><?php echo($definition); ?></td>
                        <td class=tdgeneral><?php echo($wards); ?></td>
                        <td class=tdgeneral><?php echo($area); ?></td>
                        <td class=tdgeneral><?php echo($driver); ?></td>
                        <td class=tdgeneral><?php echo($baseline); ?></td>
                        <td class=tdgeneral><?php echo($target); ?></td>
                        <td class=tdgeneral><?php echo($calc); ?></td>
                        <td class=tdgeneral><?php echo($ttype); ?></td>
                        <?php
                        $d = 16;
                        for($t=0;$t<count($time);$t++)
                        {
                            $krt = 0;
                            $d++;
                            $krt = $idata[$d];
                            if(strlen($krt)==0) { $krt = 0; }
                            ?>
                        <td class=tdgeneral><?php echo($krt); ?></td>
                        <?php
                        }
                        ?>
                    </tr>
                                    <?php
                                    }
                                    else
                                    {
                                    $id--;
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=20>Invalid KPI. This line will be ignored. <?php // echo(count($idata)." ".$munid." ".$natid); ?></td>
                    </tr>
                                    <?php
                                }
                    }
                    ?>
                </table>
                <p><input type=submit value=Accept>  <input type=button value=Reject onclick="document.location.href = 'import.php';"></p>
                </form>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "import.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
<?php
}
else
{
    echo("<P>Please select the directorate.</p>");
    include("inc_goback_history.php");
}
?>
</body>
</html>
