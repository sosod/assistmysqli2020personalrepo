<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<script type=text/javascript>
var fld = "";
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Directorates</b></h1>
<h2 style="margin-top: -5px; margin-bottom: 20px;">Directorate Report as at <?php echo(date("d-M-Y H:i",$today)); ?></h2>

<table cellpadding=3 cellspacing=0>
	<tr>
		<td class=tdheader width=30 height=30>Ref</td>
		<td class=tdheader width=200>(Sub-)Directorate</td>
		<td class=tdheader>KPIs</td>
		<td class=tdheader>Capital<br>Projects</td>
		<td class=tdheader>Total Revenue<Br>Budget</td>
		<td class=tdheader>Total Opex<Br>Budget</td>
		<td class=tdheader>Total Capex<br>Budget</td>
	</tr>
	<?php
    $dir = array();
    $dirnum = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
    $dirnum = mysql_num_rows($rs);
        while($row = mysql_fetch_array($rs))
        {
            $dir[] = $row;
        }
    mysql_close();
    if($dirnum==0 || strlen($dirnum)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=7>No Directorates available.  Please go back and click the "Add New" button to add Directorates.</td>
	</tr>
    <?php
    }
    else
    {
        foreach($dir as $row)
        {
                    $dkc = 0;
                    $dcc = 0;
                    $drb = 0;
                    $dob = 0;
                    $dcb = 0;

            $id = $row['dirid'];
            $val = $row['dirtxt'];
            include("inc_tr.php");
    ?>
	
		<td class=tdheader valign=top style="background-color: #333333;"><?php echo($id); ?></td>
		<td class=tdheader width=200 valign=top style="background-color: #333333; text-align: left"><b><?php echo($val); ?><input type=hidden name=l></b></td>
		<td class=tdheader style="background-color: #333333;"><label for=l id=kc<?php echo($id); ?>>-</label></td>
		<td class=tdheader style="background-color: #333333;"><label for=l id=cc<?php echo($id); ?>>-</label></td>
		<td class=tdheader style="background-color: #333333;text-align: right"><label for=l id=rb<?php echo($id); ?>>-</label></td>
		<td class=tdheader style="background-color: #333333;text-align: right"><label for=l id=ob<?php echo($id); ?>>-</label></td>
		<td class=tdheader style="background-color: #333333;text-align: right"><label for=l id=cb<?php echo($id); ?>>-</label></td>
        </tr>
		  <?php
            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    include("inc_tr");
                    $kc = 0;
                    $cc = 0;
                    $rb = 0;
                    $ob = 0;
                    $cb = 0;
                    $id2 = $row2['subid'];
                    $val2 = $row2['subtxt'];
                    if($row2['subhead']=="Y") { $val2 = $val2."*"; }
                    $sql = "SELECT count(kpiid) as kc FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpisubid = ".$id2." AND kpiyn = 'Y'";
                    include("inc_db_con.php");
                        $cr = mysql_fetch_array($rs);
                        $kc = $cr['kc'];
                    mysql_close($con);
                    if(strlen($kc)==0) { $kc = 0; }
                    $sql = "SELECT count(cpid) as cc FROM assist_".$cmpcode."_".$modref."_capital WHERE cpsubid = ".$id2." AND cpyn = 'Y'";
                    include("inc_db_con.php");
                        $cr = mysql_fetch_array($rs);
                        $cc = $cr['cc'];
                    mysql_close($con);
                    if(strlen($cc)==0) { $cc = 0; }
                    $sql = "SELECT sum(cfrev1) as rb, sum(cfop1) as ob, sum(cfcp1) as cb FROM assist_".$cmpcode."_".$modref."_finance_lineitems, assist_".$cmpcode."_".$modref."_finance_cashflow WHERE linesubid = ".$id2." AND lineyn = 'Y' AND lineid = cflineid AND cfyn = 'Y'";
                    include("inc_db_con.php");
                        $cr = mysql_fetch_array($rs);
                        $rb = $cr['rb'];
                        $ob = $cr['ob'];
                        $cb = $cr['cb'];
                    mysql_close($con);
                    if(strlen($rb)==0) { $rb = 0; }
                    if(strlen($ob)==0) { $ob = 0; }
                    if(strlen($cb)==0) { $cb = 0; }
                    $dkc = $dkc + $kc;
                    $dcc = $dcc + $cc;
                    $drb = $drb + $rb;
                    $dob = $dob + $ob;
                    $dcb = $dcb + $cb;
            ?>
                    <td class=tdheader valign=top><?php echo($id2); ?></td>
                    <td class=tdgeneral width=200 valign=top><?php echo($val2); ?></td>
              		<td class=tdgeneral align=center valign=top><?php if($kc==0) { echo("-"); } else { echo($kc); } ?></td>
            		<td class=tdgeneral align=center valign=top><?php if($cc==0) { echo("-"); } else { echo($cc); } ?></td>
            		<td class=tdgeneral align=right valign=top><?php if($rb==0) { echo("-"); } else { echo(number_format($rb,0)); } ?></td>
            		<td class=tdgeneral align=right valign=top><?php if($ob==0) { echo("-"); } else { echo(number_format($ob,0)); } ?></td>
            		<td class=tdgeneral align=right valign=top><?php if($cb==0) { echo("-"); } else { echo(number_format($cb,0)); } ?></td>
                </tr>
            <?php
                }
            mysql_close($con2);
            ?>
            <script type=text/javascript>
                fld = "kc<?php echo($id); ?>";
                document.getElementById(fld).innerText = <?php echo($dkc); ?>;
                fld = "cc<?php echo($id); ?>";
                document.getElementById(fld).innerText = <?php echo($dcc); ?>;
                fld = "rb<?php echo($id); ?>";
                document.getElementById(fld).innerText = "<?php if($drb==0) { echo("-"); } else { echo(number_format($drb,0)); } ?>";
                fld = "ob<?php echo($id); ?>";
                document.getElementById(fld).innerText = "<?php if($dob==0) { echo("-"); } else { echo(number_format($dob,0)); } ?>";
                fld = "cb<?php echo($id); ?>";
                document.getElementById(fld).innerText = "<?php if($dcb==0) { echo("-"); } else { echo(number_format($dcb,0)); } ?>";
            </script>
    <?php
        }
    }
    ?>
</table>
<?php
$urlback = "setup_dir.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
