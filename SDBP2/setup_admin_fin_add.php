<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var tkid = me.tkid.value;
    if(tkid == "X")
    {
        alert("Please select the user you wish to add as a finance administrator.");
        return false;
    }
    else
    {
        return true;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Finance Administrators ~ Add</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=setup_admin_fin_add_process.php onsubmit="return Validate(this);">
<table cellpadding=3 cellspacing=0 width=570>
	<?php
    $sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_menu_modules_users u, assist_".$cmpcode."_timekeep t WHERE u.usrmodref = '".$modver."' AND u.usrtkid = t.tkid AND t.tkstatus = 1";
    $sql.= " AND t.tkid NOT IN (SELECT a.tkid FROM assist_".$cmpcode."_".$modref."_list_admins a WHERE a.yn = 'Y' AND a.type = 'FIN')";
    $sql.= " ORDER BY t.tkname, t.tksurname";
    include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=2>No Users available.</td>
	</tr>
    <?php
    }
    else
    {
    ?>
    <tr>
		<td class=tdheaderl width=170>User:</td>
		<td class=tdgeneral valign=top><select name=tkid>
            <option value=X selected>--- SELECT ---</option>
        <?php
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            echo("<option value=\"".$id."\">".$val."</option>");
        }
        ?>
        </select></td>
	</tr>
    <tr>
		<td class=tdheaderl width=170>&nbsp;</td>
		<td class=tdgeneral valign=top><input type=submit value=" Add "></td>
    </tr>
    <?php
    }
    mysql_close();
    ?>
</table>
</form>
<?php
$urlback = "setup_admin_fin.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
