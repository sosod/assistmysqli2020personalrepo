<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Wards ~ Edit</b></h1>
<p>&nbsp;</p>
<?php
//get form data
$value = htmlentities($_POST['val'],ENT_QUOTES,"ISO-8859-1");
$kpaid = $_POST['k'];

if(is_numeric($kpaid) && $kpaid > 0)
{
//validate in timekeep
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value = '".$value."' AND id <> ".$kpaid;
include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
mysql_close();


if($rnum == 0)
{
    if(is_numeric($value)) { $numval = $value * 1; } else { $numval = 0; }
//if exists:
    //insert into list_admins
    $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_wards SET ";
    $sql.= "  value = '".$value."'";
    $sql.= ", yn = 'Y'";
    $sql.= ", numval = ".$numval;
    $sql.= " WHERE id = ".$kpaid;
    include("inc_db_con.php");
$tsql = $sql;
    //insert into sdbip log
        $logref = $kpaid;
        $logtype = "SLWRD";
        $logaction = "Updated Ward '".$value."'";
        $logdisplay = "Y";
        include("inc_log.php");
    //insert into transaction log
$told = "";
$trans = $logaction." with id ".$kpaid;
include("inc_transaction_log.php");
    //display result
    ?>
    <p>Ward '<?php echo($value); ?>' has been successfully updated.</p>
    <p>&nbsp;</p>
    <?php
    $urlback = "setup_lists_wards.php";
    include("inc_goback.php");
    ?>
<?php
}
else
{
//else
    //display error
        ?>
        <p>An error has occurred (Err: SDBIP30).  <br>
        It is possible that the Ward you have tried to add already exists.<br>
        &nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_lists_wards.php";
        include("inc_goback.php");
        ?>
        <?php
}
}
else    //is numeric kpaid
{
        ?>
        <p>An error has occurred (Err: SDBIP39).  <br>
        &nbsp;<br>Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_lists_wards.php";
        include("inc_goback.php");

}
?>

<p>&nbsp;</p>
</body>
</html>
