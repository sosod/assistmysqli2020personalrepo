<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Add KPI</b></h1>
<p>&nbsp;</p>
<?php
$err = "N";
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
//FORM DETAILS
$kpiid = $_POST['kpiid'];
$plcid = $_POST['plcid'];
$time = $_POST['time'];
$target = $_POST['krtarget'];
$krtargettypeid = 2;

if(strlen($kpiid)>0 && is_numeric($kpiid) && strlen($plcid)>0 && is_numeric($plcid))
{
                for($t=0;$t<count($time);$t++)
                {
                    $krtimeid = $time[$t];
                    $krtarget = $target[$t];
                    if(strlen($krtarget)==0 || !is_numeric($krtarget)) { $krtarget == 0; }
                    //insert results
                    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_result SET";
                    $sql.= "  krkpiid = ".$kpiid;
                    $sql.= ", krtarget = ".$krtarget;
                    $sql.= ", kractual = 0";
                    $sql.= ", krtargettypeid = ".$krtargettypeid;
                    $sql.= ", krprogress = ''";
                    $sql.= ", krmanagement = ''";
                    $sql.= ", krtimeid = ".$krtimeid;
                    include("inc_db_con.php");
                }

    $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi SET kpiyn = 'Y' WHERE kpiid = ".$kpiid;
    include("inc_db_con.php");
    $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc SET plcyn = 'Y' WHERE plcid = ".$plcid;
    include("inc_db_con.php");
    echo("<p>KPI with Project Life Cycle successfully added.</p>");

$tsql = $sql;
$told = "";
$trans = "Added PLC ".$plcid." to KPI ".$kpiid;
include("inc_transaction_log.php");

}
else
{
    echo("<p>An error has occurred.  The KPI could not be added.</p>");
    $err = "Y";
}

if($err == "N")
{
    if($src == "s")
    {
        $urlback = "admin_sub.php?s=".$subid;
        include("inc_goback.php");
    }
    else
    {
        if($src=="d")
        {
            $urlback = "admin_dir.php?d=".$dirid;
            include("inc_goback.php");
        }
        else
        {
            if($dir == "ALL")
            {
                $urlback = "admin.php";
                include("inc_goback.php");
            }
            else
            {
                $urlback = "admin_dskpi.php?d=".$dir;
                include("inc_goback.php");
            }
        }
    }
}
else
{
    include("inc_goback_history.php");
}

?>
</body>

</html>
