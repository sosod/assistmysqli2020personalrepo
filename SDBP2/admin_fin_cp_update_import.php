<?php
    include("inc_ignite.php");
    
    $fileloc = $_POST['floc'];
    $calc = $_POST['calc'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<h2 class=fc>Update Capital Projects - Confirm import data<input type=hidden id=lab></h2><center>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script><p>Note: The import process will not be complete until you click the "Accept" button below the table.</p>
<span id=dispcalc><form name=calc method=post action=admin_fin_cp_update_import.php>
<input type=hidden name=floc value="<?php echo($fileloc); ?>" id=floc>
<?php if(strlen($calc)==0 || $calc == "N") { ?>
<input type=hidden name=calc value=Y>
<p>Automatically calculate YTD figures? <input type=submit value=Calculate></p>
<?php } else { ?>
<input type=hidden name=calc value=N>
<p>Use YTD figures from import file? <input type=submit value=Import></p>
<?php
}   //if calc
?>
</form></span>
<script type=text/javascript>document.getElementById('dispcalc').style.display = "none";</script>
<?php
$err = "N";
$p=0;
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}
else
{
    echo("<script type=text/javascript>document.getElementById('floc').value = '".$fileloc."'; document.getElementById('dispcalc').style.display = \"inline\";</script>");
}

if($err == "N")
{
            $file = fopen($fileloc,"r");
            $f = 1;
            $r=1;
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>5 && $r>2)
                {
                    $data[$f] = $tmpdata;
                    $f++;
                }
                $r++;
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <form id=accept method=post action=admin_fin_cp_update_import_accept.php><input type=hidden name=floc value="<?php echo($fileloc); ?>"><input type=hidden name=calc value="<?php echo($calc); ?>">
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader rowspan=2 colspan=2>Ignite<br>Ref</td>
                        <td class=tdheader rowspan=2>Directorate</td>
                        <td class=tdheader rowspan=2>Sub-Directorate</td>
                        <td class=tdheader rowspan=2>Cap. Proj.<br>Number</td>
                        <td class=tdheader rowspan=2>Vote<br>Number</td>
                        <td class=tdheader rowspan=2>Project<br>Description</td>
                <?php
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sort >= 1 AND sort <= 12 ORDER BY sort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $timearr[$row['sort']] = $row;
                ?>
                        <td class=tdheader colspan=4><?php echo(date("F-Y",$row['eval'])); ?></td>
                    <?php
                    }
                mysql_close(); ?>
                    </tr>
                    <tr>
                    <?php foreach($timearr as $tim) { ?>
                        <td class=tdheader>Budget</td>
                        <td class=tdheader>Actual</td>
                        <td class=tdheader>Acc. % - YTD</td>
                        <td class=tdheader>R - YTD</td>
                    <?php } ?>
                    </tr>
                    <?php
                    $p2 = 100 / count($data);
                    foreach($data as $idata)
                    {
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $cpid = $idata[0];
                        if(strlen($cpid)>0 && is_numeric($cpid) && $cpid > 0)
                        {
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital c, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s ";
                            $sql.= " WHERE c.cpid = ".$cpid;
                            $sql.= " AND c.cpsubid = s.subid AND s.subdirid = d.dirid";
                            include("inc_db_con.php");
                                $mnr = mysql_num_rows($rs);
                                if($mnr > 0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $dir = $row['dirtxt'];
                                    $sub = $row['subtxt'];
                                    $cpnum = $row['cpnum'];
                                    $vote = $row['cpvotenum'];
                                    $project = $row['cpproject'];
                        ?>
                    <tr>
                        <td class=tdheader align=center><input type=checkbox name=import[] checked value=<?php echo($cpid); ?>></td>
                        <td class=tdheader><?php echo($cpid); ?></td>
                        <td class=tdgeneral><?php echo($dir); ?></td>
                        <td class=tdgeneral><?php echo($sub); ?></td>
                        <td class=tdgeneral><?php echo($cpnum); ?></td>
                        <td class=tdgeneral><?php echo($vote); ?></td>
                        <td class=tdgeneral style="border-right: 1px solid #555555"><?php echo($project); ?></td>
                    <?php
                    $t = 6;
                    $ctotytd = 0;
                    $cbudytd = 0;
                    foreach($timearr as $tim)
                            {
                                $budget = $idata[$t];
                                if(!is_numeric($budget))
                                {
                                    $bud = explode(",",$budget);
                                    $budget = implode("",$bud);
                                }
                                if(strlen($budget)==0 || !is_numeric($budget)) { $budget = 0; }
                                $cbudytd = $cbudytd + $budget;

                                $actual = $idata[$t+1];
                                if(!is_numeric($actual))
                                {
                                    $ac = explode(",",$actual);
                                    $actual = implode("",$ac);
                                }
                                if(strlen($actual)==0 || !is_numeric($actual)) { $actual = 0; }
                                $ctotytd = $ctotytd + $actual;

                                $accytd = $idata[$t+2];
                                $accend = substr($accytd,-1,1);
                                if($accend == "%") { $accytd = substr($accytd,0,-1); $accytd = $accytd * 1;}
                                if(!is_numeric($accytd))
                                {
                                    $ay = explode(",",$accytd);
                                    $accytd = implode("",$ay);
                                }
                                if(strlen($accytd)==0 || !is_numeric($accytd)) { $accytd = 0; }

                                $totytd = $idata[$t+3];
                                if(!is_numeric($totytd))
                                {
                                    $tot = explode(",",$totytd);
                                    $totytd = implode("",$tot);
                                }
                                if(strlen($totytd)==0 || !is_numeric($totytd)) { $totytd = 0; }

                                $caccytd = ($ctotytd / $cbudytd) * 100;
                                if($calc == "Y")
                                {
                                    $accytd = $caccytd;
                                    $totytd = $ctotytd;
                                }
                                ?>
                        <td class=tdgeneral align=right style="border-left: 1px solid #555555"><?php echo(number_format($budget,2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($actual,2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($accytd,2)); ?>%</td>
                        <td class=tdgeneral align=right style="border-right: 1px solid #555555"><?php echo(number_format($totytd,2)); ?></td>
                    <?php
                    $t = $t+4;
                    } ?>
                    </tr>
                        <?php
                                }   //mnr > 0
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No capital project found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                            mysql_close();
                        }   //valid cpid
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No capital project found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                    }
                    ?>
                </table>
                <p><input type=submit value=Accept>  <input type=button value=Reject onclick="document.location.href = 'admin_fin_cp_update.php';"></p>
                </form>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "admin_fin_cp_update.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
</body>
</html>
