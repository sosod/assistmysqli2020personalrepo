<?php
include("inc_ignite.php");
//SET ARRAYS;
$krarray = array();
foreach($krsetup as $krs) 
{
	$krarray[$krs['sort']] = array(
		'sort' => $krs['sort'],
		'filter' => 'N',
	);
}

$field = array();
$filter = array();
$filtertype = array();
$kpis = array();
$results = array();
$time = array();
$src = $_REQUEST['src'];
if($src=="DASH") {
	$field['dir'] = "Y";
	$field['sub'] = "Y";
	$field['results'] = "Y";
} else {
	$field['dir'] = $_REQUEST['dir'];
	$field['sub'] = $_REQUEST['sub'];
	$field['results'] = $_REQUEST['results'];
}
$cf = $_REQUEST['cf']; //budget from
$ct = $_REQUEST['ct']; //budget to
$kpir = $_REQUEST['kpir'];
$filter['targettype'] = $_REQUEST['targettypefilter'];
$filter['dir'] = $_REQUEST['dirfilter'];
$filter['sub'] = $_REQUEST['subfilter'];
$sort = $_REQUEST['sort'];
$output = $_REQUEST['output'];
//if($output!="csv") { echo "Processing..."; }
$krsum = $_REQUEST['krsum'];
$echocount = 0;
$kpiresultfilter = $_REQUEST['kpiresultfilter'];
if($kpiresultfilter=="ALL") {
	foreach($krarray as $krar)
	{
		$krarray[$krar['sort']]['filter'] = 'Y';
	}
} else {
	$krarray[$kpiresultfilter]['filter'] = 'Y';
}
$krgloss = $_REQUEST['krgloss'];
//GET FIELD HEADINGS
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $head[$row['headfield']] = $row;
		if($src=="DASH") {
			$field[$row['headfield']] = "Y";
		} else {
			$field[$row['headfield']] = $_REQUEST[$row['headfield']];
		}
        $filter[$row['headfield']] = $_REQUEST[$row['headfield'].'filter'];
        $filtertype[$row['headfield']] = $_REQUEST[$row['headfield'].'filtertype'];
    }
mysql_close($con);
//GET TIME PERIODS
$timesql = " FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sort >= ".$cf." AND sort <= ".$ct." ORDER BY sort";
$sql = "SELECT * ".$timesql;
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$time[$row['sort']] = $row;
		$totals[$row['id']] = array(0=>0,1=>0,2=>0,3=>0);
		$active = $row['active'];
	}
mysql_close($con);
$totals[0] = array(0=>0,1=>0,2=>0,3=>0);
$active = strlen($active)==1 ? $active : "N";

//SET REPORT SQL
$repsql[0] = "SELECT k.kpiid";
$repsql[1] = ", k.kpivalue";
$repsql[1].= ", k.kpiidpnum";
$repsql[1].= ", k.kpistratop";
$repsql[1].= ", k.kpidefinition";
$repsql[1].= ", k.kpidriver";
$repsql[1].= ", k.kpibaseline";
$repsql[1].= ", k.kpitargetunit";
$repsql[1].= ", k.kpicalctype";
$repsql[1].= ", d.dirtxt";
$repsql[1].= ", s.subtxt";
$repsql[1].= ", n.value natkpa";
$repsql[1].= ", m.value munkpa";
$repsql[1].= ", t.value ktype";
$repsql[1].= ", p.value prog";
$repsql[2] = " FROM";
$repsql[2].= " assist_".$cmpcode."_".$modref."_kpi k";
$repsql[2].= ", assist_".$cmpcode."_".$modref."_dirsub s";
$repsql[2].= ", assist_".$cmpcode."_".$modref."_dir d";
$repsql[2].= ", assist_".$cmpcode."_".$modref."_list_natkpa n";
$repsql[2].= ", assist_".$cmpcode."_".$modref."_list_munkpa m";
$repsql[2].= ", assist_".$cmpcode."_".$modref."_list_kpitype t";
$repsql[2].= ", assist_".$cmpcode."_".$modref."_list_prog p";
$repsql[2].= " WHERE";
$repsql[2].= " k.kpisubid = s.subid";
$repsql[2].= " AND s.subdirid = d.dirid";
$repsql[2].= " AND n.id = k.kpinatkpaid";
$repsql[2].= " AND m.id = k.kpimunkpaid";
$repsql[2].= " AND t.id = k.kpitypeid";
$repsql[2].= " AND p.id = k.kpiprogid";
$repsql[2].= " AND k.kpiyn = 'Y'";
$repsql[2].= " AND d.diryn = 'Y'";
$repsql[2].= " AND s.subyn = 'Y'";
$repsql[2].= " AND k.kpiid IN (SELECT DISTINCT krkpiid FROM assist_".$cmpcode."_".$modref."_kpi_result) ";
	//SQL FILTERS
if($filter['dir'] != "ALL" && is_numeric($filter['dir']) && strlen($filter['dir'])>0) {
    $repsql[2].= " AND d.dirid = ".$filter['dir'];
}
$subfilt = $filter['sub'];
if($src=="DASH") {
	if($filter['sub']!= "ALL" && checkIntRef($filter['sub'])) {
		$repsql[2].= " AND s.subid = ".$filter['sub'];
	}
} else {
	if(count($subfilt)>1)
	{
		$repsql[2].= " AND (";
		$s = 0;
		foreach($subfilt as $sf)
		{
			if($s > 0) {
				$repsql[2].= " OR ";
			}
			if($sf != "ALL" && is_numeric($sf) && strlen($sf)>0) {
				$repsql[2].= "s.subid = ".$sf;
				$s++;
			}
		}
		$repsql[2].= ") ";
	} else {
		$sf = $subfilt[0];
			if($sf != "ALL" && is_numeric($sf) && strlen($sf)>0) {
				$repsql[2].= " AND s.subid = ".$sf;
			}
	}
}

$headlists = array("HEADINGLISTS","natkpa","munkpa","ktype","kpistratop","kpicalctype","targettype");
foreach($head as $hrow)
{
    $hf = $hrow['headfield'];
    $filt = $filter[$hf];
    $filttype = $filtertype[$hf];
    if(strlen($filt) > 0) {
            if($hf == "wards") {
                if(count($filt) > 1 || (count($filt)==1 && $filt[0]!="X")) {
                    $repsql[2].= " AND k.kpiid IN (";
                    $repsql[2].= "SELECT DISTINCT kwkpiid FROM assist_".$cmpcode."_".$modref."_kpi_wards, assist_".$cmpcode."_".$modref."_list_wards WHERE kwwardsid  = id AND yn = 'Y' AND kwyn = 'Y' AND (";
                    $fc = 0;
                    foreach($filt as $f)
                    {
                        if($f != "X" && is_numeric($f) && strlen($f)>0) {
                            if($fc>0) {
                                if($filttype=="all") { $repsql[2].=" AND "; } else { $repsql[2].=" OR "; }
                            }
                            $repsql[2].= " id = ".$f;
                            $fc++;
                        }
                    }
                    $repsql[2].= "))";
                }
            } else {
                if(array_search($hf,$headlists,true)>0) {
                    if(count($filt)>1 || $filt[0] != "all") {
                        if($hf == "natkpa" || $hf == "munkpa" || $hf == "ktype") {
                            if($hf == "natkpa") { $ref = "n"; } else { if($hf == "munkpa") { $ref = "m"; } else { $ref = "t"; } }
                            $repsql[2].= " AND (";
                            $ft = 0;
                            foreach($filt as $f)
                            {
                                if($f != "all") {
                                    if($ft > 0) { $repsql.=" OR "; }
                                    $repsql[2].= " ".$ref.".id = ".$f;
                                    $ft++;
                                }
                            }
                            $repsql[2].= ")";
                        } else {
                            $ref = "k";
                            $repsql[2].= " AND (";
                            $ft = 0;
                            foreach($filt as $f)
                            {
                                if($f != "all") {
                                    if($ft > 0) { $repsql.=" OR "; }
                                    $repsql[2].= " ".$ref.".".$hf." = '".$f."'";
                                    $ft++;
                                }
                            }
                            $repsql[2].= ")";
                        }
                    }
                } else {
                    switch($filttype)
                    {
                        case "all":
                            $filarr = explode(" ",$filt);
                            foreach($filarr as $f) { $repsql[2].= " AND ".$hf." LIKE '%".$f."%'"; }
                            break;
                        case "any":
                            $filarr = explode(" ",$filt);
                            $repsql[2].= " AND (";
                            $fc = 0;
                            foreach($filarr as $f)
                            {
                                if($fc>0) { $repsql[2].= " OR "; }
                                $repsql[2].= $hf." LIKE '%".$f."%'";
                                $fc++;
                            }
                            $repsql[2].= ")";
                            break;
                        case "exact":
                            $repsql[2].= " AND ".$hf." LIKE '%".$filt."%'";
                            break;
                        default:
                            $repsql[2].= " AND ".$hf." LIKE '%".$filt."%'";
                            break;
                    }
                }
            }
    }
}
	//SQL ORDER
switch($sort)
{
    case "dir":
        $repsql[3] = " ORDER BY d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        break;
    case "natkpa":
        $repsql[3] = " ORDER BY n.value, d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        break;
    case "munkpa":
        $repsql[3] = " ORDER BY m.value, d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        break;
    case "prog":
        $repsql[3] = " ORDER BY p.value, d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        break;
    case "ktype":
        $repsql[3] = " ORDER BY t.value, d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        break;
    default:
        if(strlen($sort)>0) {
            $repsql[3] = " ORDER BY k.".$sort.", d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        } else {
            $repsql[3] = " ORDER BY d.dirsort ASC, s.subsort ASC, k.kpicpid DESC, k.kpiid DESC";
        }
        break;
}

//GET KPIs
$sql = implode(" ",$repsql);
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$kpis[$row['kpiid']] = $row;
	}
mysql_close($con);

//GET RESULTS
if($field['results'] == "Y") {
	$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid IN (";
	$sql.= $repsql[0].$repsql[2].")";
	$sql.= " AND krtimeid IN (SELECT id ".$timesql.")";
	$sql.= " ORDER BY krkpiid, krtimeid";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs))
		{
			$results[$row['krkpiid']][$row['krtimeid']] = $row;
			$results[$row['krkpiid']][$row['krtimeid']]['krr'] = 0;
		}
	mysql_close($con);

	foreach($results as $row) 
	{
					$kpiresult = $row;
					$kc = 0;
					$ytdt = 0;
					$ytda = 0;
					$krt0 = 0;
					$kra0 = 0;
					$krr1 = 0;
					$krc = 0;
                    foreach($time as $tim)
                    {
						$timeid = $tim['id'];
						$kpiid = $kpiresult[$timeid]['krkpiid'];
						$kct = strtoupper($kpis[$kpiid]['kpicalctype']);
                        switch($kct)
                        {
                            case "CO":
                                $sql2 = "SELECT max(krtarget) as krtarget, max(kractual) as kractual FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid <= ".$timeid;
                                include("inc_db_con2.php");
                                    $row2 = mysql_fetch_array($rs2);
                                mysql_close($con2);
                                $row2['krprogress'] = $kpiresult[$timeid]['krprogress'];
                                $row2['krtargettypeid'] = $kpiresult[$timeid]['krtargettypeid'];
                                break;
                            default:
								$row2 = $kpiresult[$timeid];
								break;
                        }
                        $krt = $row2['krtarget'];
                        $kra = $row2['kractual'];
						if(checkIntRef($krt)) {
							if($kct == "CO") {
								$krt0 = $krt;
							} else {
								$krt0 += $krt;
								if($krt>0) { $kc++; }
							}
						}
						if(checkIntRef($kra)) {
							if($kct == "CO") {
								$kra0 = $kra;
							} else {
								$kra0 += $kra;
							}
						}
                        if($krt > 0 || $kra > 0 || $kct == "ZERO") {
                            switch($row2['krtargettypeid'])
                            {
                                case 1:
                                    $krtarget = "R ".number_format($krt,0);
                                    $kractual = "R ".number_format($kra,0);
                                    break;
                                case 2:
                                    $krtarget = number_format($krt,0)."%";
                                    $kractual = number_format($kra,0)."%";
                                    break;
                                default:
									if(is_numeric($krt)) {
										$krtarget = number_format($krt,0);
									} else {
										$krtarget = $krt;
									}
									if(is_numeric($kra)) {
										$kractual = number_format($kra,0);
									} else {
										$kractual = $kra;
									}
                                    break;
                            }
                        } else {
                            $krtarget = "";
                            $kractual = "";
						}
						if($tim['eval']<$today || $kra > 0 || $kct == "ZERO" || $krc > 0) {
							$krr = calcKR($kct,$krt,$kra);
						} else {
							$krr = 0;
                        }
						$krr1 += $krr;
						if($krr>0) { $krc++; }
						$results[$kpiid][$timeid]['krr'] = $krr;
						$results[$kpiid][$timeid]['krt'] = 0+$krt;
						$results[$kpiid][$timeid]['kra'] = $kra;
						$results[$kpiid][$timeid]['krtdisp'] = $krtarget." ";
						$results[$kpiid][$timeid]['kradisp'] = $kractual;
						$totals[$timeid][$krr] += 1;
                    }
					if($kc==0 && ($kra0 > 0 || $krt0 > 0)) { $kc = 1; }
                
					if($krr1>0 || $krc > 0) {
						if($kct == "STD") { $krt0 = $krt0 / $kc; $kra0 = $kra0 / $kc; }
						$krr0 = calcKR(strtoupper($kct),$krt0,$kra0);
					} else {
						$krr0 = 0;
					}
					if($krr0==0 && $active == "N") { $krr0 = 1; }
					$krtarget0 = "";
					$kractual0 = "";
                        if($krt0 > 0 || $kra0 > 0 || $kct == "ZERO") {
                            switch($row2['krtargettypeid'])
                            {
                                case 1:
                                    $krtarget0 = ($krt0!=round($krt0,0) || $kra0!=round($kra0,0)) ? "R ".number_format($krt0,2) : "R ".number_format($krt0,0); 
                                    $kractual0 = ($krt0!=round($krt0,0) || $kra0!=round($kra0,0)) ? "R ".number_format($kra0,2) : "R ".number_format($kra0,0); 
                                    break;
                                case 2:
                                    $krtarget0 = ($krt0!=round($krt0,0) || $kra0!=round($kra0,0)) ? number_format($krt0,2)."%" : number_format($krt0,0)."%"; 
                                    $kractual0 = ($krt0!=round($krt0,0) || $kra0!=round($kra0,0)) ? number_format($kra0,2)."%" : number_format($kra0,0)."%";
                                    break;
                                default:
									if(is_numeric($krt0)) {
										$krtarget0 = ($krt0!=round($krt0,0) || $kra0!=round($kra0,0)) ? number_format($krt0,2) : number_format($krt0,0); 
									} else {
										$krtarget0 = $krt0;
									}
									if(is_numeric($kra0)) {
										$kractual0 = $kra0!=round($kra0,0) ? number_format($kra0,2) : number_format($kra0,0); 
									} else {
										$kractual0 = $kra0;
									}
                                    break;
                            }
                        } else {
                            $krtarget0 = "";
                            $kractual0 = "";
                        }
						if($krarray[$krr0]['filter']=="Y") {
							$results[$kpiid]['krr0'] = $krr0;
							$results[$kpiid]['krt0'] = $krt0;
							$results[$kpiid]['kra0'] = $kra0;
							$results[$kpiid]['krtdisp0'] = $krtarget0;
							$results[$kpiid]['kradisp0'] = $kractual0;
							$totals[0][$krr0] += 1;
						} else {
							$totals[0][$krr0] += 1;
							unset($results[$kpiid]);
							unset($kpis[$kpiid]);
						}
	}
}
/*//DISPLAY RESULTS - development
foreach($results as $res)
{
	foreach($time as $t)
	{
		$r = $res[$t['id']];
		echo("<P>".$r['krkpiid']." - ".$t['id']." : ");
		print_r($r);
	}
}*/

//OUTPUT
$echo = "";
switch($output) 
{
case "csv":
	$cspan = 1 + count($head);
	if($field['dir']=="Y") { $cspan++; }
	if($field['sub']=="Y") { $cspan++; }
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22][0] = "\""; //result cell
	$cella[22][1] = "\""; //result cell
	$cella[22][2] = "\""; //result cell
	$cella[22][3] = "\""; //result cell
	$rowa = "";
	$rowz = "\r\n";
	$pagea = "\"SDBIP ".$modtxt.": Report - KPIs\"\r\n";
	$pagez = "";
	break;
case "excel":
	if($field['results']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<td class=head>"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[10] = "<td class=head colspan=5>"; //time heading cell
	$cellz[10] = "</td>"; //time heading cell
	$cella[11] = "<td class=head colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</td>"; //period-to-date heading cell
	$cella[12] = "<td class=head rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</td>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22][0] = "<td style=\"".$styler[0]."\">"; //result cell
	$cella[22][1] = "<td style=\"".$styler[1]."\">"; //result cell
	$cella[22][2] = "<td style=\"".$styler[2]."\">"; //result cell
	$cella[22][3] = "<td style=\"".$styler[3]."\">"; //result cell
	$rowa = chr(10)."<tr>";
	$rowb = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>KPI Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>".chr(10)." td { font:Calibri; font-size:11pt; } .head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font:Calibri;} ".chr(10)." .title { font-size:14pt; font:Calibri; color:#CC0001; font-weight:bold; text-decoration:underline; } </style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap width=50>SDBIP 2009/2010: KPI Report</td></tr><tr><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr><tr></tr>";
	$pagez = "</table></body></html>";
	break;
default:
	if($field['results']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[10] = "<th colspan=5>"; //time heading cell
	$cellz[10] = "</th>"; //time heading cell
	$cella[11] = "<th colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<th rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</th>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22][0] = "<td style=\"".$styler[0]."\">"; //result cell
	$cella[22][1] = "<td style=\"".$styler[1]."\">"; //result cell
	$cella[22][2] = "<td style=\"".$styler[2]."\">"; //result cell
	$cella[22][3] = "<td style=\"".$styler[3]."\">"; //result cell
	$rowa = "<tr>";
	$rowb = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/default.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"/styles/style_red.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"lib/SDBP2.css\" type=\"text/css\">";
	$pagea.= "<style type=text/css>table { border-width: 1px; border-style: solid; border-color: #ababab; }    table td { border: 1px solid #ababab; } .b-w { border: 1px solid #fff; }</style>";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>SDBIP ".$modtxt.": Report - KPIs</b></h1>";
	$pagea.= "<table cellpadding=3 cellspacing=0>";
	$pagez = "</table></body></html>";
	break;
}

//START PAGE
$echo = $pagea;

//TABLE HEADINGS
	$echo.= $rowa;
	$echo.= $cella[12]."Ref".$cellz[12];
	if($field['dir']=="Y") { $echo.= $cella[12]."Directorate".$cellz[12]; }
	if($field['sub']=="Y") { $echo.= $cella[12]."Sub-Directorate".$cellz[12]; }
	foreach($head as $hrow)
	{
		if($field[$hrow['headfield']] == "Y") { $echo.= $cella[12].$hrow['headlist'].$cellz[12]; }
	}
	if($field['results'] == "Y")
	{
		foreach($time as $tim)
		{
			$echo.= $cella[10].date("F-Y",$tim['eval']).$cellz[10];
			if($output == "csv") {
				for($c=1;$c<=4;$c++)
				{
					$echo.=$cella[1]." ".$cellz[1];
				}
			}
		}
		$echo.= $cella[11]."Period-To-Date".$cellz[11];
	}
	$echo.=$rowz;
	if($field['results'] == "Y")
	{
		$echo.=$rowa;
		if($output == "csv") {
			for($c=1;$c<=$cspan;$c++)
			{
				$echo.=$cella[1]." ".$cellz[1];
			}
		}
		for($t=1;$t<=count($time);$t++)
		{
			$echo.=$cella[1]."Target".$cellz[1];
			$echo.=$cella[1]."Actual".$cellz[1];
			$echo.=$cella[1]."R".$cellz[1];
			$echo.=$cella[1]."Progress Comment".$cellz[1];
			$echo.=$cella[1]."Management Comment".$cellz[1];
		}
		$echo.=$cella[1]."Target".$cellz[1];
		$echo.=$cella[1]."Actual".$cellz[1];
		$echo.=$cella[1]."R".$cellz[1];
		$echo.=$rowz;
	}

//TABLE ROWS
	foreach($kpis as $kpi)
	{
		$kpiid = $kpi['kpiid'];
		$echo.= $rowa;
		$echo.= $cella[2].$kpiid.$cellz[2];
		if($field['dir']=="Y") { $echo.= $cella[2].decode($kpi['dirtxt']).$cellz[2]; }
		if($field['sub']=="Y") { $echo.= $cella[2].decode($kpi['subtxt']).$cellz[2]; }
		foreach($head as $hrow)
		{
			if($field[$hrow['headfield']] == "Y") { 
                        switch($hrow['headfield'])
                        {
                            case "kpicalctype":
                                $echo.= $cella[2];
                                    switch(strtoupper($kpi[$hrow['headfield']]))
                                    {
                                        case "ACC":
                                            $echo.= ("Accumulative");
                                            break;
                                        case "CO":
                                            $echo.= ("Carry-Over");
                                            break;
                                        case "STD":
                                            $echo.= ("Stand-Alone");
                                            break;
                                        case "ZERO":
                                            $echo.= ("Zero %");
                                            break;
                                    }
                                $echo.= (" ".$cellz[2]);
                                break;
                            case "kpistratop":
                                $echo.= $cella[2];
                                    switch($kpi[$hrow['headfield']])
                                    {
                                        case "O":
                                            $echo.= ("Operational");
                                            break;
                                        case "P":
                                            $echo.= ("Project");
                                            break;
                                        case "S":
                                            $echo.= ("Strategic");
                                            break;
                                    }
                                $echo.= (" ".$cellz[2]);
                                break;
                            case "wards":
                                $echo.= $cella[2];
                                $sql2 = "SELECT w.* FROM assist_".$cmpcode."_".$modref."_kpi_wards cw, assist_".$cmpcode."_".$modref."_list_wards w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = cw.kwwardsid AND cw.kwyn = 'Y' AND cw.kwkpiid = ".$kpiid;
                                $sql2.= " ORDER BY w.numval, w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $echo.= ($row2['value']."; ");
                                    }
                                mysql_close($con2);
                                $echo.= (" ".$cellz[2]);
                                break;
                            default:
                                $echo.= $cella[2].decode($kpi[$hrow['headfield']])." ".$cellz[2];
                                break;
                        }
			}
		}
		if($field['results'] == "Y")
		{
			$res = $results[$kpiid];
			foreach($time as $tim)
			{
				$tid = $tim['id'];
				$krr = $res[$tid]['krr'];
				if(!checkIntRef($krr)) { $krr = 0; }
				$echo.=$cella[2].$res[$tid]['krtdisp'].$cellz[2];
				$echo.=$cella[2].$res[$tid]['kradisp'].$cellz[2];
				$echo.=$cella[22][$krr].$krsetup[$krr]['code'].$cellz[2];
				$echo.=$cella[2].$res[$tid]['krprogress'].$cellz[2];
				$echo.=$cella[2].$res[$tid]['krmanagement'].$cellz[2];
			}
			$echo.=$cella[21].$res['krtdisp0'].$cellz[21];
			$echo.=$cella[21].$res['kradisp0'].$cellz[21];
			$echo.=$cella[22][$res['krr0']].$krsetup[$res['krr0']]['code'].$cellz[21];
			$echo.=$rowz;
		}
	}

if($krgloss == "Y" && $output == "display") { $echo.= displayKRGloss('report'); }

//SUMMARY OF RESULTS
function drawKRSum($what,$txt,$val,$i) {
	global $output;
	global $styler;
	switch($what) {
		case "title":
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"Summary of Results\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td nowrap class=title>Summary of Results</td>";
			} else {
				$echo ="</table><h2>Summary of Results</h2><table cellpadding=3 cellspacing=5 width=330>";
			}
			break;
		case "sum":
			if($output=="csv") {
				$echo ="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo = "<tr><td></td><td nowrap colspan=2 style=\"font-weight: bold;\">".$txt."</td><td nowrap style=\"text-align: right;font-weight: bold;\">".$val."</td></tr>";
			} else {
				$echo = "<tr><td class=b-w>&nbsp;&nbsp;</td><td class=b-w style=\"font-weight: bold;\">".$txt."</td><td class=b-w style=\"text-align: right;font-weight: bold;\">&nbsp;".$val."</td></tr>";
				$echo.= "</table>";
			}
			break;
		default:
			if($output=="csv") {
				$echo.="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo.= "<tr><td style=\"".$styler[$i]."\"> </td><td nowrap colspan=2>".$txt."</td><td nowrap style=\"text-align: right;\">".$val."</td></tr>";
			} else {
				$echo.= "<tr><td class=b-w style=\"".$styler[$i]."\">&nbsp;</td><td class=b-w>".$txt."</td><td class=b-w style=\"text-align: right;\">&nbsp;".$val."</td></tr>";
			}
			break;
	}
	return $echo;
}
if($krsum == "Y") {
	$echo.= drawKRSum('title','',0,0);
			if($krarray[3]['filter']=="Y") { 
				$echo.= drawKRSum('row','KPIs Met (100% <= G)',$totals[0][3],3);
			} if($krarray[2]['filter']=="Y") { 
				$echo.= drawKRSum('row','KPIs Almost Met (75% <= O < 100%)',$totals[0][2],2);
			} if($krarray[1]['filter']=="Y") { 
				$echo.= drawKRSum('row','KPIs Not Met (R < 75%)',$totals[0][1],1);
			} if($krarray[0]['filter']=="Y") { 
				$echo.= drawKRSum('row','KPIs Not Measured (N/A)',$totals[0][0],0);
			}
	$echo.= drawKRSum('sum','Total KPIs',array_sum($totals[0]),0);
}

if($krgloss == "Y" && $output != "display") { $echo.= displayKRGloss($output); }
	
//END PAGE
$echo.= $pagez;

//DISPLAY OUTPUT
switch($output)
{
	case "csv":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "../files/".$cmpcode."/reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = $chkloc."/".$modref."_kpi_".date("Ymd_Hi",$today).".csv";
        $newfilename = "kpi_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'text/plain';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("download.php");
		break;
	case "excel":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "../files/".$cmpcode."/reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = $chkloc."/".$modref."_kpi_".date("Ymd_Hi",$today).".xml";
        $newfilename = "kpi_report_".date("Ymd_Hi",$today).".xls";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'application/ms-excel';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("download.php");
		break;
	default:
		echo $echo;
		break;
}
?>