<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var cde = me.cde.value;
    var valid8 = "true";
    if(cde.length==0)
    {
        document.getElementById('cde').className = 'reqtext';
        document.getElementById('cde').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('cde').className = 'reqmet';
    }
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('val').className = 'reqmet';
    }
    if(document.getElementById('val').className == 'reqtext' || document.getElementById('cde').className == 'reqtext')
    {
        alert("Please complete the missing fields as indicated.");
        return false;
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Municipal KPAs ~ Add</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=setup_lists_munkpa_add_process.php onsubmit="return Validate(this);">
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=110>Municipal KPA:</td>
		<td class=tdgeneral valign=top><input type=text size=30 maxlength=50 name=val id=val> <span style="font-size: 7.5pt;">(max 50 characters)</span></td>
		<td class=tdgeneral width=60>*required</td>
	</tr>
    <tr>
		<td class=tdheaderl>Short Code**:</td>
		<td class=tdgeneral valign=top><input type=text size=10 maxlength=5 name=cde id=cde> <span style="font-size: 7.5pt;">(max 5 characters)</span></td>
		<td class=tdgeneral>*required</td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral colspan=2 valign=top><input type=submit value=" Add "></td>
    </tr>
</table>
<p>** Used on the list view of KPIs.</p>
</form>
<?php
$urlback = "setup_lists_munkpa.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
