<?php
    include("inc_ignite.php");
    
    $fileloc = $_POST['floc'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #555555;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){
				// Progressbar
				$("#progressbar").progressbar({
					value: 0
				});
			});

            var i = 0;
			function incProg(i) {
                $("#progressbar").progressbar( 'value' , i );
			}
			
			function hide() {
    			document.getElementById('progbar').style.display = "none";
			}
		</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import ~ Capital Projects<input type=hidden id=lab></b></h1>
<center>
<span id=progbar><div id="progressbar" style="width:500"></div><label id=lbl for=lab>Processing...</label><label for=lab id=lbl2>0%</label></span>
<script type=text/javascript>
$("#progressbar").progressbar({
    value: 0
});
</script><p>Note: The import process will not be complete until you click the "Accept" button below the table.</p>
<?php
$err = "N";
$p=0;
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = "capital_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}

if($err == "N")
{
            $file = fopen($fileloc,"r");
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <form id=accept method=post action=import_capital_accept.php><input type=hidden name=floc value="<?php echo($fileloc); ?>">
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader colspan=3>Ref</td>
                        <td class=tdheader>Directorate</td>
                        <td class=tdheader>Sub-Directorate</td>
                        <td class=tdheader>C.P. Num</td>
                        <td class=tdheader>IDP</td>
                        <td class=tdheader>Vote</td>
                        <td class=tdheader>Project</td>
                        <td class=tdheader>Wards</td>
                        <td class=tdheader>Area</td>
                        <td class=tdheader>Funding Source</td>
                        <td class=tdheader>Start date</td>
                        <td class=tdheader>End date</td>
                        <?php
                        $time = array();
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time ORDER BY sort";
                        include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs))
                            {
                                $time[] = $row;
                                echo("<td class=tdheader>".date("M-Y",$row['eval'])."</td>");
                            }
                        mysql_close();
                        ?>
                        <td class=tdheader>&nbsp;</td>
                        <td class=tdheader>Total - File</td>
                        <td class=tdheader>Total - Sys</td>
                        <td class=tdheader>Difference</td>
                    </tr>
                    <?php
                    $sql = "SELECT max(cpid) as id FROM assist_".$cmpcode."_".$modref."_capital";
                    include("inc_db_con.php");
                        $row1 = mysql_fetch_array($rs);
                    mysql_close();
                    $p2 = 100 / count($data);
                    $id = $row1['id'];
                    if(strlen($id)==0) { $id = 0; }
                    foreach($data as $idata)
                    {
set_time_limit(1800);
                        $p = $p + $p2;
//                        echo("<script type=text/javascript>alert(".$p."); $(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
                        echo("<script type=text/javascript>incProg(".$p."); document.getElementById('lbl2').innerText = '".number_format($p,2)."%'</script>");
                        $ref = $idata[0];
                        $dir = trim($idata[1]);
                        $dir = htmlentities($dir,ENT_QUOTES,"ISO-8859-1");
                        $dir = substr($dir,0,100);
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirtxt = '".$dir."' AND diryn = 'Y'";
                        include("inc_db_con.php");
                            if(mysql_num_rows($rs)==0)
                            {
                                $direrr = "Y";
                            }
                            else
                            {
                                $dirrow = mysql_fetch_array($rs);
                                $dirid = $dirrow['dirid'];
                                if(strlen($dirid)>0 && is_numeric($dirid) && $dirid > 0)
                                {
                                    $direrr = "N";
                                }
                                else
                                {
                                    $direrr = "Y";
                                }
                            }
                        mysql_close();
                        $sub = trim($idata[2]);
                        if($direrr == "N")
                        {
                                $sub = htmlentities($sub,ENT_QUOTES,"ISO-8859-1");
                                $sub = substr($sub,0,100);
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subtxt = '".$sub."'";
                            include("inc_db_con.php");
                                if(mysql_num_rows($rs) > 0 )
                                {
                                    $subrow = mysql_fetch_array($rs);
                                    $subid = $subrow['subid'];
                                    if(strlen($subid)==0 || !is_numeric($subid)) { $direrr = "Y"; }
                                }
                                else
                                {
                                    $subid = "NEW";
                                }
                            mysql_close();
                        }

                        
                        $cpnum = trim($idata[3]);
                        $idp = trim($idata[4]);
                        $vote = trim($idata[5]);
                        $project = trim($idata[6]);
                        $wards = trim($idata[7]);
                        $area = trim($idata[8]);
                        $fund = trim($idata[9]);
                        $start = trim($idata[10]);
                        if(strlen($start) > 0)
                        {
                            $start = $start * 86400;
                            $start = $start - 2209165200;
                        }
                        else
                        {
                            $start = "1 July 2009";
                        }
                        $end = trim($idata[11]);
                        if(strlen($end) > 0)
                        {
                            $end = $end * 86400;
                            $end = $end - 2209165200;
                        }
                        else
                        {
                            $end = "30 June 2010";
                        }
                        if(strlen($ref)>0 && $ref != "Ref" && strlen($dir)>0 && $direrr == "N" && strlen($sub)>0 && strlen($project)>0)
                        {
                            $id++;
                        ?>
                    <tr>
                        <td class=tdheader align=center><input type=checkbox name=import[] checked value=<?php echo($ref); ?>></td>
                        <td class=tdheader><?php echo($ref); ?></td>
                        <td class=tdheader><?php echo($id); ?></td>
                        <td class=tdgeneral><?php echo($dir." (".$dirid.")"); ?></td>
                        <td class=tdgeneral><?php echo($sub." (".$subid.")"); ?></td>
                        <td class=tdgeneral><?php echo($cpnum); ?></td>
                        <td class=tdgeneral><?php echo($idp); ?></td>
                        <td class=tdgeneral><?php echo($vote); ?></td>
                        <td class=tdgeneral><?php echo($project); ?></td>
                        <td class=tdgeneral><?php echo($wards); ?></td>
                        <td class=tdgeneral><?php echo($area); ?></td>
                        <td class=tdgeneral><?php echo($fund); ?></td>
                        <td class=tdgeneral align=center><?php echo(date("d M Y",$start)); ?></td>
                        <td class=tdgeneral align=center><?php echo(date("d M Y",$end)); ?></td>
                        <?php
                        $d=12;
                        $numtot = 0;
                        foreach($time as $tim)
                        {
                            $num = 0;
                            $num = trim($idata[$d]);
                            if(strlen($num)==0) { $num = 0; }
                            if(!is_numeric($num))
                            {
                                $num2 = explode(" ",$num);
                                $num = impolde("",$num2);
                                $num2 = explode(",",$num);
                                $num = implode("",$num2);
                                if(!is_numeric($num)) { $num = $num * 1; }
                                if(!is_numeric($num)) { $num = 0; }
                            }
                            $numtot = $numtot + $num;
                            echo("<td class=tdgeneral align=right>".number_format($num,2)."</td>");
                            $d++;
                        }
                        $diff = 0;
                        $diff = $idata[23] - $numtot;
                        $diffclass = "tdgeneral";
                        if($diff != 0) { $diffclass = "tdheader"; }
                        ?>
                        <td class=tdheader><b>&nbsp;</b></td>
                        <td class=tdgeneral align=right><b><?php echo(number_format($idata[23],2)); ?></b></td>
                        <td class=tdgeneral align=right><b><?php echo(number_format($numtot,2)); ?></b></td>
                        
                        <td class=<?php echo($diffclass); ?> align=right><?php if($diff != 0) { echo(number_format($diff,2)); } else { echo("-"); } ?></td>
                    </tr>
                                    <?php
                                }
                                else
                                {
                                    if($direrr == "Y" && $ref != "Ref" && strlen($ref)>0)
                                    {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdgeneral><?php echo($dir); ?></td>
                        <td class=tdgeneral colspan=26>Invalid directorate.  The directorate could not be found.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                    <tr>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdheaderblue><?php echo($idata[0]); ?></td>
                        <td class=tdheaderblue>&nbsp;</td>
                        <td class=tdgeneral colspan=26>Invalid line.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                    }
                                }
                    }
                    ?>
                </table>
                <p><input type=submit value=Accept>  <input type=button value=Reject onclick="document.location.href = 'import.php';"></p>
                </form>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: 99	});</script>");
?></center>
                <?php
                    $urlback = "import.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>
                <script type=text/javascript>
                incProg(100);
                var t = setTimeout("return hide();",3000);
                </script>
</body>
</html>
