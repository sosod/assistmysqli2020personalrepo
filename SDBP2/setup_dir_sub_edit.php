<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>

<base target="main">
<script type=text/javascript>
    function delSub(s,d) {
        if(!isNaN(parseInt(s)) && !isNaN(parseInt(d)))
        {
            if(confirm("Are you sure you wish to delete this sub-directorate?")==true)
            {
                document.location.href = "setup_dir_sub_del.php?r=se&s="+s+"&d="+d;
            }
        }
        else
        {
            alert("An error has occurred.  Please reload the page and try again.");
        }
    }
    
function Validate(me) {
    var val = me.val.value;
    var valid8 = "true";

    if(val.length==0)
    {
        document.getElementById('val').className = "reqtext";
        valid8 = "false";
    }
    else
    {
        document.getElementById('val').className = "blank";
    }

    if(valid8=="true")
    {
        return true;
    }
    else
    {
        alert("Please complete all missing field(s) as indicated.");
        return false;
    }
    return false;
}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Edit Sub-Directorate</b></h1>
<p>&nbsp;</p>
<?php
    $subid = $_GET['s'];
    $dirid = $_GET['d'];
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$subid." AND subyn = 'Y' ORDER BY subsort";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $sub = mysql_fetch_array($rs);
            $err = "N";
        }
        else
        {
            $dir = array();
            $err = "Y";
        }
    mysql_close();

    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        $dir = mysql_fetch_array($rs);
    mysql_close();

    if($err == "N")
    {
?>
<form name=subedit method=POST action=setup_dir_sub_edit_process.php language=jscript onsubmit="return Validate(this);">
<input type=hidden name=subid value=<?php echo($subid); ?>>
<input type=hidden name=dirid value=<?php echo($dirid); ?>>
<table cellpadding=3 cellspacing=0 width=570 id="table1">
	<tr>
		<td class=tdheaderl width=130>Directorate:</td>
		<td class=tdgeneral><?php echo(html_entity_decode($dir['dirtxt'],ENT_QUOTES,"ISO-8859-1")); ?>&nbsp;</td>
		<td class=tdgeneral width=110><img src="/pics/blank.gif" width=1 height=19>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdheaderl width=130>Sub-Directorate:</td>
		<td class=tdgeneral><input type=text size="50" name=val id=val value="<?php echo(html_entity_decode($sub['subtxt'],ENT_QUOTES,"ISO-8859-1")); ?>">&nbsp;</td>
		<td class=tdgeneral width=110><small>Max 50 characters</small></td>
	</tr>
	<?php
	$pos = "l";
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subsort < ".$sub['subsort']." AND subyn = 'Y' AND subhead = 'N' ORDER BY subsort DESC";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)==0) {
            $pos = "f";
        } else {
            $pos = "a";
            $posrow = mysql_fetch_array($rs);
        }
    mysql_close();
    if($pos != "f")
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subsort > ".$sub['subsort']." AND subyn = 'Y' ORDER BY subsort";
        include("inc_db_con.php");
            if(mysql_num_rows($rs)==0) {    $pos = "l";    }
        mysql_close();
    }
    ?>
	<tr>
		<td class=tdheaderl valign=top>&nbsp;</td>
		<td class=tdgeneral valign=top colspan="2"><input type=submit value="Save Changes"> <input type=button value=Delete onclick="delSub(<?php echo($subid); ?>,<?php echo($dirid); ?>);"></td>
	</tr>
</table>
<?php
    }    else    {    //IF NO ERROR
        echo("<p>An error has occurred.  Please go back and try again.</p>");
    }
?>
</form>
<h2>SDBIP Details</h2>
<table cellpadding=3 cellspacing=0 style="border-collapse: collapse; border: 1px solid #ababab;" width=570>
    <tr>
        <td class=tdheaderl width=130>KPIs:</td>
        <td class=tdgeneral><?php
            $sql = "SELECT count(kpiid) as kc FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiyn = 'Y' AND kpisubid = ".$subid;
            include("inc_db_con.php");
                $detail = mysql_fetch_array($rs);
            mysql_close();
            echo($detail['kc']);
            $detail = array();
        ?></td>
    </tr>
    <tr>
        <td class=tdheaderl width=130>Capital Projects:</td>
        <td class=tdgeneral><?php
            $sql = "SELECT count(cpid) as kc FROM assist_".$cmpcode."_".$modref."_capital WHERE cpyn = 'Y' AND cpsubid = ".$subid;
            include("inc_db_con.php");
                $detail = mysql_fetch_array($rs);
            mysql_close();
            echo($detail['kc']);
            $detail = array();
        ?></td>
    </tr><?php
            $sql = "SELECT sum(cfrev1) as rb, sum(cfop1) as ob, sum(cfcp1) as cb FROM assist_".$cmpcode."_".$modref."_finance_cashflow WHERE cfyn = 'Y' AND cflineid IN (SELECT lineid FROM assist_".$cmpcode."_".$modref."_dirsub, assist_".$cmpcode."_".$modref."_finance_lineitems WHERE lineyn = 'Y' AND linesubid = ".$subid.")";
            include("inc_db_con.php");
                $detail = mysql_fetch_array($rs);
            mysql_close();
    ?><tr>
        <td class=tdheaderl width=130>Original Revenue Budget:</td>
        <td class=tdgeneral>R <?php echo(number_format($detail['rb'],2)); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl width=130>Original Opex Budget:</td>
        <td class=tdgeneral>R <?php echo(number_format($detail['ob'],2)); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl width=130>Original Capex Budget:</td>
        <td class=tdgeneral>R <?php echo(number_format($detail['cb'],2)); ?></td>
    </tr>
</table>

<script type=text/javascript>
var pos = document.getElementById('pos').value;
if(pos == "f" || pos == "l")
{
    document.getElementById('poslist').disabled = true;
}
</script>
<?php
$urlback = "setup_dir_sub.php?d=".$dirid;
include("inc_goback.php");
?>
</body>
</html>
