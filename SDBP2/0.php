<html>
<head>
</head>
<body>
<h1>GRAPH TEST</h1>
<h2>Start</h2>
<?php
require("lib/AmPieChart.php");
AmChart::$swfObjectPath = "lib/swfobject.js";
AmChart::$libraryPath = "lib/amcharts";
AmChart::$jsPath = "lib/AmCharts.js";
AmChart::$jQueryPath = "lib/jquery-1.3.2.min.js";
AmChart::$loadJQuery = true;

$krsetup = Array ( 
	0 => Array ( 	'id' => 4, 		'value' => "Not Measured",		'mincalc' => '',		'min' => 0, 		'max' => 0, 		'maxcalc' => '',		'sort' => 0, 		'yn' => 'Y', 		'color' => '#777777',		'code' => 'N/A'	),
	1 => Array ( 		'id' => 1, 		'value' => "Not Met",		'mincalc' => 'G',		'min' => 0, 		'max' => 75, 		'maxcalc' => 'L',		'sort' => 1, 		'yn' => 'Y', 		'color' => '#CC0001',		'code' => 'R'	),
	2 => Array ( 		'id' => 2, 		'value' => "Almost Met",		'mincalc' => 'G-E',		'min' => 75, 		'max' => 100, 		'maxcalc' => 'L',		'sort' => 2, 		'yn' => 'Y', 		'color' => '#FE9900',		'code' => 'O'	),
	3 => Array ( 		'id' => 3, 		'value' => "Met",		'mincalc' => 'G-E',		'min' => 100, 		'max' => 999999999999, 		'maxcalc' => 'L',		'sort' => 3, 		'yn' => 'Y', 		'color' => '#009900',		'code' => 'G'	)
);
foreach($krsetup as $krs) {	$kpi[$krs['sort']] = 10;}
$kpi['err'] = 0;
$kpi['tot'] = 0;
$calctype = array();
$kpitot = 0 + array_sum($kpi) - $kpi['err'];
$kpi['tot'] = $kpitot;

$chart = new AmPieChart("kpi_".$who);
$chart->setTitle("<p>".$titletxt."</p>");
foreach($krsetup as $krs) {
	$chart->addSlice($krs['sort'],$krs['value']." (".$kpi[$krs['sort']]." of ".$kpi['tot'].")", $kpi[$krs['sort']], array("color" => $krs['color']));
}
    $chart->setConfigAll(array(
    "width" => 500,
    "height" =>500,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "35%",
    "pie.radius" => 150,
    "pie.height" => 5,
    "background.border_alpha" => 100,
    "data_labels.radius" => "-20%",
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
    ));
	echo html_entity_decode($chart->getCode());
?>
<h2>END</h2>
</body>
</html>
