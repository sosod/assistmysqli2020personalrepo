<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function timeSel(me) {
    if(me.value.indexOf("_")>0)
    {
//        alert(me.value.indexOf("_"));
        document.getElementById('a4').disabled = true;
        document.getElementById('a3').checked = true;
    }
    else
    {
        document.getElementById('a4').disabled = false;
        document.getElementById('a4').checked = true;
    }
}

function Validate(me) {
/*    if(me.dirfilter.value == "ALL")
    {
        if(confirm("Are you sure you wish to draw the report for ALL Directorates?\nDue to the amount of information the report can possibly fail to generate fully.\n\nAre you sure you wish to continue?\nIf yes, click OK else click Cancel to go back.")==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
*/        return true;
//    }
//    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 1px solid #ababab;
    }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - KPI PDF Report</b></h1>
<form name=kpireport method=post action=report_kpi_process.php onsubmit="return Validate(this);" language=jscript>
<h3 class=fc>1. Select the columns you want in your report:</h3>
<div style="margin-left: 17px">
<table cellpadding="3" cellspacing="0" width=700>
    <tr>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0  style="border-width: 0px">
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="hidden" name="dir" value="Y"><img src="../pics/tick_check.gif"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Directorate</td>
            	</tr>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="hidden" name="sub" value="Y"><img src="../pics/tick_check.gif"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Sub-Directorate</td>
            	</tr>
<?php
$pdfhead = array(
'natkpa'=>'Y',
'prog'=>'Y',
'kpivalue'=>'Y',
'kpidefinition'=>'Y',
'wards'=>'Y',
'kpidriver'=>'Y',
'kpibaseline'=>'Y',
'kpitargetunit'=>'Y'
);
$head = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    $mnr = 10;
    $m2 = $mnr / 2;
    $m = round($m2);
    if($m < $m2) { $m++; }
    $r=1;
    while($row = mysql_fetch_array($rs))
    {
        if($pdfhead[$row['headfield']]=="Y")
        {
        $head[$row['headfield']] = $row;
        $r++;
        if($r > $m)
        {
            $r=0;
            ?>
            </table>
        </td>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0 style="border: 0px">
            <?php
        }
        ?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="hidden" checked name="<?php echo($row['headfield']); ?>" value=Y><img src="../pics/tick_check.gif"></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle><?php echo($row['headdetail']); ?></td>
            	</tr>
        <?php
        }
    }
mysql_close();

$timearr = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $timearr[$row['sort']]['sort'] = $row['sort'];
        $timearr[$row['sort']]['val'] = date("M-y",$row['eval']);
        $timearr[$row['sort']]['sval'] = $row['sval'];
        $timearr[$row['sort']]['eval'] = $row['eval'];
    }
mysql_close();
?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="results" value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>KPI Results <select name=kpir id=cf onchange="timeSel(this);">
                    <?php
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo("<option ");
                        if($t == 1) { echo(" selected "); }
                        echo("value=".$tim['sort'].">".$tim['val']."</option>");
                        $t++;
                        if($tim['sort']/3==round($tim['sort']/3,0))
                        {
                            $q = $tim['sort']/3;
                            $v = ($tim['sort']-2)."_".$tim['sort'];
                            echo("<option ");
                            echo("value=".$v.">Quarter ".$q."</option>");
                        }
                    }
                    ?></select></td>
            	</tr>
            </table>
        </td>
    </tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class="tdgeneral" valign=top><b>Directorate:</b></td>
		<td class="tdgeneral" valign=top colspan=4><select name=dirfilter>
<!--            <option selected value=ALL>All Directorates</option> -->
            <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
            $d=1;
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option ");
                    if($d==1) { echo(" selected "); }
                    echo("value=".$row['dirid'].">".$row['dirtxt']."</option>");
                    $d++;
                }
            mysql_close();
            ?>
        </select></td>
	</tr>
	<?php
    foreach($head as $hrow)
    {
        $i=0;
        if($hrow['headfield']!="prog")
        {
        ?>
        <tr>
            <td class="tdgeneral" valign=top><b><?php echo($hrow['headdetail']); ?>:</b></td>
        <?php
        switch($hrow['headfield'])
        {
            case "ktype":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=5><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpitype WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "munkpa":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=5><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_munkpa WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "natkpa":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=6><option selected value=all>All</option>");
        		$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_natkpa WHERE yn = 'Y'";
        		include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                        $i++;
                    }
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "kpicalctype":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=6><option selected value=all>All</option>");
                echo("<option value=ACC>Accumulative</option>");
                echo("<option value=CO>Carry-Over</option>");
                echo("<option value=STD>Stand-Alone</option>");
                echo("<option value=ZERO>Zero %</option>");
                mysql_close();
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "kpistratop":
                ?>
        		<td class="tdgeneral" valign=top colspan=4>
        		<?php
                echo("<select multiple name=".$hrow['headfield']."filter[] size=4><option selected value=all>All</option>");
                echo("<option value=S>Strategic</option>");
                echo("<option value=O>Operational</option>");
                echo("<option value=P>Project</option>");
                echo("</select><input type=hidden name=".$hrow['headfield']."filtertype value=any>");
                ?><br><i><small>Use CTRL key to select multiple options</small></i>
                </td>
                <?php
                break;
            case "wards":
                ?>
        		<td class="tdgeneral" valign=top><input type=checkbox name=wardsfilter[] value=X checked> Any&nbsp;<br>
                <input type=checkbox name=wardsfilter[] value=1> All&nbsp;<br>
        		<?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $wmnr = mysql_num_rows($rs2)+2;
                            $c2 = $wmnr / 4;
                            $c = round($c2);
                            if($c < $c2) { $c++; }
                            $w=2;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                $w++;
                                                if($w>$c)
                                                {
                                                    echo("</td><td class=tdgeneral valign=top>");
                                                    $w = 1;
                                                }
                                                echo("<input type=checkbox name=wardsfilter[] value=".$row2['id']." >".$row2['value']."<br>");
                                            }
                                        ?>
                                <?php
                        mysql_close($con2);
                break;
            default:
                ?>
                <td class="tdgeneral" valign=top colspan=4><input type=text name=<?php echo($hrow['headfield']."filter"); ?>>&nbsp;<select  name=<?php echo($hrow['headfield']."filtertype"); ?>><option selected value=all>Match all words</option><option value=any>Match any word</option><option value=exact>Match exact phrase</option></td>
                <?php
                break;
        }
        ?>
        </tr>
        <?php
        }
    }
    ?>
</table>
</div>
<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class=tdgeneral align=center style="border-bottom: 0px">Sort by:</td>
		<td class="tdgeneral" style="border-bottom: 0px"><select name=sort>
        <option selected value=dir>Directorate / Sub-Directorate</option>
		<?php foreach($head as $hrow) {
            if($hrow['headfield']!="wards")
            {
                echo(chr(10)."<option value=".$hrow['headfield'].">".$hrow['headdetail']."</option>");
            }
        }?>
        </select></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Select the paper size of your report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class=tdgeneral align=center style="border-bottom: 0px" width=40><input type=radio name=paper value=a4 checked id=a4></td>
		<td class=tdgeneral style="border-bottom: 0px"><label for=a4>A4 (1 month per page)</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center style="border-bottom: 0px" width=40><input type=radio name=paper value=a3 id=a3></td>
		<td class=tdgeneral style="border-bottom: 0px"><label for=a3>A3 (3 months per page)</label></td>
	</tr>
</table>
</div>
<h3 class=fc>5. Click the button:</h3>
<p style="margin-left: 17px"><input type=hidden name=output value=pdf><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
<p style="font-size: 7pt;">Note: The report can take some time to generate.  Please be patient.</p>
</form>
<p>&nbsp;</p>

</body>

</html>
