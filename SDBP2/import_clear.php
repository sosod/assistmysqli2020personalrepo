<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import - Clear Old Data</b></h1>

<?php
$what = $_GET['t'];

switch($what)
{
    case "CAP":
        echo("<h2>Capital Projects</h2>");
        //capital
        //capital_wards
        //capital_results
        break;
    case "CF":
        echo("<h2>Monthly Cashflows</h2>");
        //finance_lineitems
        $sql = "DROP TABLE IF EXISTS assist_".$cmpcode."_sdp09_finance_lineitems";
        include("inc_db_con.php");
        echo("<P>1. Finance_lineitems DROPPED.</p>");
        $sql = "CREATE TABLE `assist_".$cmpcode."_sdp09_finance_lineitems` (";
        $sql.= "  `lineid` int(10) unsigned NOT NULL auto_increment,";
        $sql.= "  `linevalue` varchar(255) NOT NULL default '',";
        $sql.= "  `lineyn` varchar(1) NOT NULL default 'Y',";
        $sql.= "  `linesort` int(10) unsigned NOT NULL default '0',";
        $sql.= "  `linetype` varchar(2) NOT NULL default 'CF' COMMENT 'CF OR RS',";
        $sql.= "  `linesubid` int(10) unsigned NOT NULL default '0',";
        $sql.= "  `linevote` varchar(50) NOT NULL default '',";
        $sql.= "  PRIMARY KEY  (`lineid`)";
        $sql.= ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
        include("inc_db_con.php");
        echo("<P>2. Finance_lineitems CREATED.</p>");

        //finance_cashflow
        $sql = "DROP TABLE IF EXISTS `assist_".$cmpcode."_sdp09_finance_cashflow`";
        include("inc_db_con.php");
        echo("<P>3. Finance_cashflow DROPPED.</p>");
        $sql = "CREATE TABLE `assist_".$cmpcode."_sdp09_finance_cashflow` ( ";
        $sql.= "  `cfid` int(10) unsigned NOT NULL auto_increment, ";
        $sql.= "  `cflineid` int(10) unsigned NOT NULL default '0', ";
        $sql.= "  `cfyn` varchar(1) NOT NULL default 'Y', ";
        $sql.= "  `cftimeid` int(10) unsigned NOT NULL default '0', ";
        $sql.= "  `cfrev1` double NOT NULL default '0' COMMENT 'obudget', ";
        $sql.= "  `cfrev2` double NOT NULL default '0' COMMENT 'actual', ";
        $sql.= "  `cfrev3` double NOT NULL default '0' COMMENT 'accytd', ";
        $sql.= "  `cfrev4` double NOT NULL default '0' COMMENT 'totytd', ";
        $sql.= "  `cfrev5` double NOT NULL default '0' COMMENT 'var', ";
        $sql.= "  `cfrev6` double NOT NULL default '0' COMMENT 'vire', ";
        $sql.= "  `cfrev7` double NOT NULL default '0' COMMENT 'adjest', ";
        $sql.= "  `cfrev8` double NOT NULL default '0' COMMENT 'adjbudget', ";
        $sql.= "  `cfop1` double NOT NULL default '0' COMMENT 'obudget', ";
        $sql.= "  `cfop2` double NOT NULL default '0' COMMENT 'actual', ";
        $sql.= "  `cfop3` double NOT NULL default '0' COMMENT 'accytd', ";
        $sql.= "  `cfop4` double NOT NULL default '0' COMMENT 'totytd', ";
        $sql.= "  `cfop5` double NOT NULL default '0' COMMENT 'var', ";
        $sql.= "  `cfop6` double NOT NULL default '0' COMMENT 'vire', ";
        $sql.= "  `cfop7` double NOT NULL default '0' COMMENT 'adjest', ";
        $sql.= "  `cfop8` double NOT NULL default '0' COMMENT 'adjbudget', ";
        $sql.= "  `cfcp1` double NOT NULL default '0' COMMENT 'obudget', ";
        $sql.= "  `cfcp2` double NOT NULL default '0' COMMENT 'actual', ";
        $sql.= "  `cfcp3` double NOT NULL default '0' COMMENT 'accytd', ";
        $sql.= "  `cfcp4` double NOT NULL default '0' COMMENT 'totytd', ";
        $sql.= "  `cfcp5` double NOT NULL default '0' COMMENT 'var', ";
        $sql.= "  `cfcp6` double NOT NULL default '0' COMMENT 'vire', ";
        $sql.= "  `cfcp7` double NOT NULL default '0' COMMENT 'adjest', ";
        $sql.= "  `cfcp8` double NOT NULL default '0' COMMENT 'adjbudget', ";
        $sql.= "  PRIMARY KEY  (`cfid`) ";
        $sql.= ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
        include("inc_db_con.php");
        echo("<P>4. Finance_cashflow CREATED.</p>");
        break;
    case "KPI":
        echo("<h2>KPIs</h2>");
        //kpi
        //kpi_wards
        //kpi_results
        break;
    case "MUN":
        echo("<h2>Municipal KPAs</h2>");
        //list_munkpa
        $sql = "DROP TABLE IF EXISTS `assist_".$cmpcode."_sdp09_list_munkpa` ";
        include("inc_db_con.php");
        echo("<P>1. list_munkpa DROPPED.</p>");
        $sql = "CREATE TABLE `assist_".$cmpcode."_sdp09_list_munkpa` ( ";
        $sql.= "  `id` int(10) unsigned NOT NULL auto_increment, ";
        $sql.= "  `value` varchar(200) NOT NULL default '', ";
        $sql.= "  `yn` varchar(1) NOT NULL default 'Y', ";
        $sql.= "  `code` varchar(10) NOT NULL, ";
        $sql.= "  PRIMARY KEY  (`id`) ";
        $sql.= ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6  ";
        include("inc_db_con.php");
        echo("<P>2. list_munkpa CREATED.</p>");
        break;
    case "RBS":
        echo("<h2>Revenue By Source</h2>");
        //finance_revbysource
        $sql = "DROP TABLE IF EXISTS `assist_".$cmpcode."_sdp09_finance_revbysource` ";
        include("inc_db_con.php");
        echo("<P>1. finance_revbysource DROPPED.</p>");
        $sql = "CREATE TABLE `assist_".$cmpcode."_sdp09_finance_revbysource` ( ";
        $sql.= "  `rsid` int(10) unsigned NOT NULL auto_increment, ";
        $sql.= "  `rsline` varchar(200) NOT NULL default '0', ";
        $sql.= "  `rsyn` varchar(1) NOT NULL default 'Y', ";
        $sql.= "  `rssort` int(10) unsigned NOT NULL, ";
        $sql.= "  `rs1budget` double NOT NULL default '0', ";
        $sql.= "  `rs1actual` double NOT NULL default '0', ";
        $sql.= "  `rs2budget` double NOT NULL default '0', ";
        $sql.= "  `rs2actual` double NOT NULL default '0', ";
        $sql.= "  `rs3budget` double NOT NULL default '0', ";
        $sql.= "  `rs3actual` double NOT NULL default '0', ";
        $sql.= "  `rs4budget` double NOT NULL default '0', ";
        $sql.= "  `rs4actual` double NOT NULL default '0', ";
        $sql.= "  `rs5budget` double NOT NULL default '0', ";
        $sql.= "  `rs5actual` double NOT NULL default '0', ";
        $sql.= "  `rs6budget` double NOT NULL default '0', ";
        $sql.= "  `rs6actual` double NOT NULL default '0', ";
        $sql.= "  `rs7budget` double NOT NULL default '0', ";
        $sql.= "  `rs7actual` double NOT NULL default '0', ";
        $sql.= "  `rs8budget` double NOT NULL default '0', ";
        $sql.= "  `rs8actual` double NOT NULL default '0', ";
        $sql.= "  `rs9budget` double NOT NULL default '0', ";
        $sql.= "  `rs9actual` double NOT NULL default '0', ";
        $sql.= "  `rs10budget` double NOT NULL default '0', ";
        $sql.= "  `rs10actual` double NOT NULL default '0', ";
        $sql.= "  `rs11budget` double NOT NULL default '0', ";
        $sql.= "  `rs11actual` double NOT NULL default '0', ";
        $sql.= "  `rs12budget` double NOT NULL default '0', ";
        $sql.= "  `rs12actual` double NOT NULL default '0', ";
        $sql.= "  `rsvote` varchar(45) NOT NULL default '', ";
        $sql.= "  PRIMARY KEY  (`rsid`) ";
        $sql.= ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
        include("inc_db_con.php");
        echo("<P>2. finance_revbysource CREATED.</p>");
        break;
    default:
        echo("<h2>ERROR</h2>");
        //do nothing
        break;
}

$urlback = "import.php";
include("inc_goback.php");

?>

</body>

</html>
