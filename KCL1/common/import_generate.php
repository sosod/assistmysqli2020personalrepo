<?php
require_once("../../module/autoloader.php");

$section = $_REQUEST['section'];
$name_divider = ":";
$headingObject = new KCL1_HEADINGS();
$headings = $headingObject->getMainObjectHeadings($section,"DETAILS","MANAGE");


$original_headings = $headings;
$headings = array('rows'=>array());

switch($section) {
	case "SUB":
		$headings['rows']['sub_function_id'] = array(
			'id'=>0,
			'field'=>"sub_function_id",
			'name'=>"Parent |function|",
			'section'=>"SUB",
			'type'=>"OBJECT",
			'list_table'=>"FUNCTION",
			'max'=>0,
			'parent_id'=>0,
			'parent_link'=>"",
			'default_value'=>"",
			'required'=>1,
			'help'=>"",
		);
		break;
	case "ACTIVITY":
		$headings['rows']['activity_sub_id'] = array(
			'id'=>0,
			'field'=>"activity_sub_id",
			'name'=>"Parent |function| $name_divider |sub|",
			'section'=>"SUB",
			'type'=>"OBJECT",
			'list_table'=>"SUB",
			'max'=>0,
			'parent_id'=>0,
			'parent_link'=>"",
			'default_value'=>"",
			'required'=>1,
			'help'=>"",
		);
		break;
}

foreach($original_headings['rows'] as $fld => $head) {
	if($head['type']=="REF" || $head['type']=="COMPETENCY") {
//		unset($headings['rows'][$fld]);
	} elseif($head['type']=="TARGET") {
		$listObject = new KCL1_LIST("month");
		$months = $listObject->getActiveListItemsFormattedForSelect();
		foreach($months as $pi => $pp) {
			$headings['rows']['at_'.$pi] = array(
				'id'=>0,
				'field'=>"at_".$pi,
				'name'=>$pp." (".$head['name'].")",
				'section'=>"TARGET",
				'type'=>"TARGET",
				'list_table'=>$pi,
				'max'=>0,
				'parent_id'=>0,
				'parent_link'=>"",
				'default_value'=>"",
				'required'=>0,
				'help'=>"",
			);
		}
	} else {
		$headings['rows'][$fld] = $head;
	}
}

$data = array(
	0=>array(),
	1=>array(),
);

foreach($headings['rows'] as $fld => $head) {
	$name = $headingObject->replaceAllNames($head['name']);
	if($head['type']!="REF" && $head['type']!="COMPETENCY") {
		$data[0][] = str_replace('"',"'",ASSIST_HELPER::decode($name)).($head['required']==1?"*":"");
		switch($head['type']) {
			case "MEDVC":
			case "LRGVC":
				$data[1][] = "Max characters: ".$head['max'];
				break;
			case "BOOL":
				$data[1][] = "Yes/No";
				break;
			case "NUM":
			case "TARGET":
				$data[1][] = "Numbers only";
				break;
			case "TEXT":
			case "LIST":
			default:
				$data[1][] = "";
				break;
		}
	}
}

$fdata = "\"".implode("\",\"",$data[0])."\"\r\n\"".implode("\",\"",$data[1])."\"";

$cmpcode = $headingObject->getCmpCode();
$modref = $headingObject->getModRef();
$today = time();

//WRITE DATA TO FILE
$filename = "../../files/".$cmpcode."/".$modref."_template_".date("Ymd_Hi",$today).".csv";
$newfilename = "template.csv";
$file = fopen($filename,"w");
fwrite($file,$fdata."\n");
fclose($file);
//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$newfilename.'"');
readfile($filename);


?>