<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class KCL1_ACTIVITY extends KCL1 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_subfunction_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "JA";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "ACTIVITY"; 
    const OBJECT_NAME = "activity"; 
	const PARENT_OBJECT_TYPE = "SUB";
     
    const TABLE = "activity";
    const TABLE_FLD = "activity";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0,$modref="") {
        parent::__construct($modref);
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
		unset($var['object_id']); //remove incorrect field value from add form
		if(isset($var['at'])) {
			$activity_target = $var['at'];
			unset($var['at']);
		} else {
			$activity_target = array();
			for($i=1;$i<=12;$i++) {
				$activity_target[$i] = 0;
			}
		}
		if(isset($var['ac'])) {
			$activity_competency = $var['ac'];
			unset($var['ac']);
			$activity_competency_proficiency = $var['ac_prof'];
			unset($var['ac_prof']);
		} else {
			$activity_competency= array();
			$activity_competency_proficiency = array();
		}

		
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_status'] = KCL1::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			
			if(count($activity_competency)>0){
				$acompObject = new KCL1_ACTIVITY_COMPETENCY();
				foreach($activity_competency as $ci => $ac) {
					if($ac*1==1) {
						$acompObject->addObject(array(),$id,$ci,$activity_competency_proficiency[$ci]);
					}
				} 
			}
			$atObject = new KCL1_ACTIVITY_TARGET();
			foreach($activity_target as $ti => $t) {
				if(strlen($t)==0) { $t = 0; }
				$atObject->addObject(array(),$id,$ti,$t);
			}
			
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> KCL1_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}




	public function editObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		
		$activity_target = $var['at'];
		unset($var['at']);
		$activity_competency = $var['ac'];
		unset($var['ac']);
		$activity_competency_proficiency = $var['ac_prof'];
		unset($var['ac_prof']);
		
		$extra_logs = array();
		
		$atObject = new KCL1_ACTIVITY_TARGET();
		$at_old = $atObject->getRawObjectGroupedByParent($object_id);
		$at_old = $at_old[$object_id];
		foreach($activity_target as $ti => $at) {
			$old = $at_old[$ti];
			if($at!=$old) {
				$edit_var = array(
					'parent'=>$object_id,
					'secondary'=> $ti,
					'value'=>$at,
				);
				$atObject->editObject($edit_var);
				$extra_logs['at_id'][$ti] = array('to'=>$at,'from'=>$old);
			}
		}
		
		$acObject = new KCL1_ACTIVITY_COMPETENCY();
		$ac_old = $acObject->getRawObjectGroupedByParent($object_id);
		$ac_old = $ac_old[$object_id];
		foreach($activity_competency as $ci => $ac) {
			$ci = intval($ci); 
			$ac = intval($ac);
			$new = $activity_competency_proficiency[$ci];
			if(isset($ac_old[$ci])) {
				//check for change in proficiency
				$old = $ac_old[$ci];
				if($new!=$old) {
					$edit_var = array(
						'parent'=>$object_id,
						'secondary'=> $ci,
						'value'=>$new,
					);
					$acObject->editObject($edit_var);
					$extra_logs['activity_competency'][$ci] = array('to'=>$new,'from'=>$old);
				}
			} else {
				//Add object
				$acObject->addObject(array(),$object_id,$ci,$new);
				$extra_logs['activity_competency'][$ci] = "|competency| ".$ci." added to |activity| ".$object_id." with |proficiency| ".$new;
			}
		}
		foreach($ac_old as $oi => $old) {
			if(!isset($activity_competency[$oi])) {
				//delete object
				$edit_var = array(
					'parent'=>$object_id,
					'secondary'=> $oi,
				);
					//'value'=>$activity_competency_proficiency[$oi],
				$acObject->removeObject($edit_var);
				$extra_logs['activity_competency'][$oi] = "|competency| ".$oi." removed from |activity| ".$object_id;
			}
		}
		
		
		$result = $this->editMyObject($var,$extra_logs);
		
		
		
		
		
		
		return $result;
	}


	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = ".self::DELETED." 
				WHERE ".$this->getIDFieldName()." = ".$var['object_id'];
		$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $var['object_id'],
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> KCL1_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
		return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$var['object_id']." deleted successfully.");
	}

    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id,$get_parent=false) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		//$data['activity_annual_target'] = intval($data['activity_annual_target'])==($data['activity_annual_target']) ? number_format($data['activity_annual_target'] : "XXX"; //intval($data['activity_annual_target'])!==$data['activity_annual_target']*1 ? number_format($data['activity_annual_target'],2) : intval($data['activity_annual_target']);

		$acObject = new KCL1_ACTIVITY_COMPETENCY();
		$ac_objects = $acObject->getRawObjectGroupedByParent($obj_id);
		$data['activity_competency'] = isset($ac_objects[$obj_id]) ? $ac_objects[$obj_id] : array();
		
		$atObject = new KCL1_ACTIVITY_TARGET();
		$at_objects = $atObject->getRawObjectGroupedByParent($obj_id);
		$data['activity_target'] = isset($at_objects[$obj_id]) ? $at_objects[$obj_id] : array();

		if($get_parent==true) {
			$parentObject = new KCL1_SUBFUNCTION();
			$parent_id = $data[$this->getParentFieldName()];
			$data['parent'] = $parentObject->getFullName($parent_id);
		}

		return $data;
	}
	
	
	
	/**
	 * Get list of active objects limited to specific FUNCTION ready to populate a SELECT element 
	 */
	public function getLimitedActiveObjectsFormattedForSelect($options=array()) { //$this->arrPrint($options);
		$rows = $this->getOrderedObjects($options['FUNCTION']);
		//$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($rows);
		$items = array();
		foreach($rows as $i => $r) {
			foreach($r as $i2 => $r2) {
				$items[$r2['id']] = $r2['name'];
			}
		}
		asort($items);
		return $items;
		//return $items;
	}	
	
//parent_parent = FUNCTION; parent = SUBFUNCTION
	public function getOrderedObjects($parent_parent_id=0, $parent_id=0){
		$sql_status = $this->getActiveStatusSQL("A");
		$err = false;
		//If PARENT_ID = immediate sent then find all records with that ID using $this->parent field
		if(is_array($parent_id)) {
			$parent_id = ASSIST_HELPER::removeBlanksFromArray($parent_id);
			if(count($parent_id)>0) {
				$sql_from = "WHERE ".$this->getParentFieldName()." IN (".implode(",",$parent_id).") AND ";
			} else {
				$err = true;
			}
		} elseif($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			//Else join up with immediate parent
			$parentObject = new KCL1_SUBFUNCTION();
			$sql_status.=" AND ".$parentObject->getActiveStatusSQL("B");
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B 
						  ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." ";
			if(is_array($parent_parent_id)) {
				$parent_parent_id = ASSIST_HELPER::removeBlanksFromArray($parent_parent_id);
				if(count($parent_parent_id)>0) {
					$sql_from.=" AND B.".$parentObject->getParentFieldName()." IN (".implode(",",$parent_parent_id).") WHERE ";
				} else {
					$err = true;
				}
			} elseif($this->checkIntRef($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
			} else {
				$err = true;
			}
		}
		if(!$err) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					, ".$this->getParentFieldName()." as parent_id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$sql_status;
			$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		} else {
			$res2 = array();
		}
		//echo $sql;
		return $res2;
	}

	
	

//parent_parent = FUNCTION; parent = SUBFUNCTION
	public function getRawOrderedObjects($parent_parent_id=0, $parent_id=0){
		$err = false;
		//If PARENT_ID = immediate sent then find all records with that ID using $this->parent field
		if(is_array($parent_id)) {
			$parent_id = ASSIST_HELPER::removeBlanksFromArray($parent_id);
			if(count($parent_id)>0) {
				$sql_from = "WHERE ".$this->getParentFieldName()." IN (".implode(",",$parent_id).") AND ";
			} else {
				$err = true;
			}
		} elseif($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			//Else join up with immediate parent
			$parentObject = new KCL1_SUBFUNCTION();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B 
						  ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." ";
			if(is_array($parent_parent_id)) {
				$parent_parent_id = ASSIST_HELPER::removeBlanksFromArray($parent_parent_id);
				if(count($parent_parent_id)) {
					$sql_from.=" AND B.".$parentObject->getParentFieldName()." IN (".implode(",",$parent_parent_id).") WHERE ";
				} else {
					$err = true;
				}
			} elseif($this->checkIntRef($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
			} else {
				$err = true;
			}
		}
		if($err) {
			$res2 = array();
		} else {
			$sql = "SELECT *
					, ".$this->getIDFieldName()." as id
					, ".$this->getParentFieldName()." as parent_id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$this->getActiveStatusSQL("A");
			$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
			
			/*
			COMPETENCY CODE NEEDED HERE
			
			$sql = "SELECT ".$this->getIDFieldName()." as id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$this->getActiveStatusSQL("A");
			$ids = $this->mysql_fetch_all_by_value($sql,"id");
			
			if(count($ids)>0) {
				$sql = "SELECT kp_kpi_id as kpi_id, kp_proj_id as proj_id FROM ".$this->getTableName()."_project WHERE kp_kpi_id IN (".implode(",",$ids).") AND kp_status = ".self::ACTIVE;
				$all_ids = $this->mysql_fetch_array_by_id($sql, "kpi_id","proj_id",false);
				$proj_ids = array();
				foreach($all_ids as $kpi_id => $p_ids) {
					foreach($p_ids as $i => $p) {
						if(!in_array($p,$proj_ids)) {
							$proj_ids[] = $p;
						}
					}
				}
				
				$projObject = new KCL1_PROJECT();
				$proj = $projObject->getObjectsForKPIs($proj_ids);
				
				foreach($res2 as $parent_id => $kpis_array) {
					foreach($kpis_array as $kpi_id => $row) {
						$res2[$parent_id][$kpi_id]['kp_proj_id'] = array();
						$kp_proj_ids = isset($all_ids[$kpi_id]) ? $all_ids[$kpi_id] : array();
						foreach($kp_proj_ids as $p) {
							$res2[$parent_id][$kpi_id]['kp_proj_id'][$p] = $proj[$p]['name']." [".$proj[$p]['reftag']."]";
						}
					}
				}
			}
		*/
		}
		
		
		return $res2;
	}
	


/*

From original IDP1 module

	public function getIDPidFromParentID($obj_id) {
			
		
		$parentObject = new KCL1_PMPROG();
		$grandParentObject = new KCL1_PMKPA();
		$sql_from = "INNER JOIN ".$grandParentObject->getTableName()." B 
					  ON A.".$parentObject->getParentFieldName()." = B.".$grandParentObject->getIDFieldName()."
					  WHERE ".$parentObject->getIDFieldName()." = ".$obj_id." AND ";
		$sql_status = $grandParentObject->getActiveStatusSQL("B")." AND ".$parentObject->getActiveStatusSQL("A");
		
		$sql = "SELECT ".$grandParentObject->getParentFieldName()." as parent_id
				FROM ".$parentObject->getTableName()." A
				".$sql_from."  ".$sql_status;
		$res2 = $this->mysql_fetch_one($sql);
		return $res2['parent_id'];
		
	}

*/

	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>