<?php
/**
 * To manage any MASTER FILE classes
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class KCL1_MASTER extends KCL1 {

	private $master_list = "";
	private $masterObject;
	private $local_modref;

	public function __construct($ml = "") {
		parent::__construct();
		$this->local_modref = $this->getModRef();
		$this->master_list = $ml;
		if(strlen($ml)>0) {
			$this->setList($ml);
		}
	}

	private function setList($list) {
		switch(strtoupper($list)) {
			case "BUSPROC":
				$this->masterObject = new S_BUSPROC();
				break;
			case "FINYEAR":
				$this->masterObject = new S_FINANCIALYEARS();
				break;
		}
	}

	public function getActiveItemsFormattedForSelect() {
		$rows = $this->masterObject->getActiveItemsFormattedForSelectFromExternalModule();
		$this->setDBRef($this->local_modref);
		return $rows;
	}





}


?>