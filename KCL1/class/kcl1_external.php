<?php


class KCL1_EXTERNAL extends ASSIST_MODULE_HELPER {

	private $local_modref;
	private $external_modloc;
	private $external_modref;
	private $external_class;
	private $variables;
	private $externalObject;


	public function __construct($external_class,$external_modref="",$var="") {
		parent::__construct();
		$this->local_modref = $this->getDBRef();
		$this->external_class = $external_class;
		$this->external_modref = $external_modref;

		//use external class to calculate modloc
		$x = explode("_",$external_class);
		$this->external_modloc = $x[0];

		//EMP1 / Employee Assist needs to be handled slightly differently due to modref complications
		if($this->external_modloc=="EMP1") {
			$this->externalObject = new KCL1_EMPLOYEE($this->external_class);
		} else {
			//Create object in other module
			$class_name = strtoupper($this->external_modloc."_INTERNAL");
			$this->externalObject = new $class_name($this->external_modref);
		}
		//Send any additional info e.g. list table for _LIST class
		$this->variables = $var;
		$this->externalObject->setExtraData($this->variables);

	}

	public function getActiveListItemsFormattedForSelect() {
		//set external object modref
		if(strlen($this->external_modref)>0) {
			$this->externalObject->setDBRef($this->external_modref);
		}
		$rows = $this->externalObject->getActiveListItemsFormattedForSelect();

		$this->setDBRef($this->local_modref);
		return $rows;
	}


}