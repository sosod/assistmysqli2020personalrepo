<?php
/**
 * To manage the link between EMPLOYEE Assist and PM6
 * Glossary explanations added on 1 April 2021 #AA-238 JC
 *
 * Created on: 1 January 2016
 * Authors: Janet Currie
 *
 */

class KCL1_EMPLOYEE extends ASSIST_MODULE_HELPER {

	private $employee_modref = "EMP";
	private $employee_modref_confirmed = false;
	private $external_module_class = "";
	private $extra_data = "";
	private $internalObject;


	public function __construct($emp1_module_class="") {
		parent::__construct();

		$this->external_module_class = $emp1_module_class;

		//Set the Employee Assist modref - to catch clients with EMP1 instead of EMP as the modref #AA-644 JC 9 July 2021
		$is_there_a_session_variable_about_employee_modref = isset($_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']);

		if($is_there_a_session_variable_about_employee_modref) {
			$this->employee_modref_confirmed = $_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref_confirmed'];
			$this->employee_modref = $_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref'];
		} else {
			//Look in the menu table for EMP & EMP1 and get the oldest record
			$sql = "SELECT * FROM assist_menu_modules WHERE modref IN ('EMP','EMP1') AND modyn = 'Y' ORDER BY modid DESC";
			$row = $this->mysql_fetch_one($sql);
			if(isset($row['modref'])) {
				$this->employee_modref = $row['modref'];
				$this->employee_modref_confirmed = true;
				$_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref'] = $this->employee_modref;
				$_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref_confirmed'] = $this->employee_modref_confirmed;
			}
		}

	}

	public function setExtraData($var) {
		$this->extra_data = $var;
	}


	public function getActiveListItemsFormattedForSelect() {
		$class_name = $this->external_module_class;
		//if class is LIST then set list table first & create object
		if(strpos($class_name,"_LIST")!==false) {
			if(is_array($this->extra_data)) {
				if(isset($this->extra_data['list_table']) && strlen($this->extra_data['list_table'])>0) {
					$list_table = $this->extra_data['list_table'];
				} else {
					$list_table = false;
				}
			} elseif(strlen($this->extra_data)>0) {
				$list_table = $this->extra_data;
			} else {
				$list_table = false;
			}
			$this->internalObject = new $class_name($list_table,$this->employee_modref);
		} else {
			$this->internalObject = new $class_name($this->employee_modref);
		}
		return $this->internalObject->getActiveListItemsFormattedForSelect();
	}

	public function __destruct() {
		parent::__destruct();
	}

}


?>