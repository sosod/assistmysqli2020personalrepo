<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class KCL1_COMPETENCY_PROFICIENCY extends KCL1 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_comp_id";
	protected $secondary_parent_field = "_prof_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "CP";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "COMPETENCY_PROFICIENCY"; 
    const OBJECT_NAME = "competency_proficiency"; 
	const PARENT_OBJECT_TYPE = "COMPETENCY";
	const SECONDARY_PARENT_OBJECT_TYPE = "PROFICIENCY";
     
    const TABLE = "competency_proficiency";
    const TABLE_FLD = "cp";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->has_secondary_parent = true;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($insert_data=array(),$comp_id=0,$prof_id=0,$name="") {
		
		if(count($insert_data)>0) {
			$var = array();
			foreach($insert_data as $key => $val) {
				$var[$this->getTableField()."_".$key] = $val;
			}
		} else {
			$var = array(
				$this->getParentFieldName() => $comp_id,
				$this->getSecondaryParentFieldName() => $prof_id,
				$this->getNameFieldName() => $name,
			);
		}
		
		$var[$this->getTableField().'_status'] = KCL1::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		/* if($id>0) {
			
			
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> KCL1_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}*/
		//return array("error","Testing: ".$sql);
	}




	public function editObject($var,$attach=array()) {
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getNameFieldName()." = '".$var['value']."' 
				WHERE ".$this->getParentFieldName()." = ".$var['parent']." 
				  AND ".$this->getSecondaryParentFieldName()." = ".$var['secondary'];
		$this->db_update($sql);
		return true;
	}



    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }
  /*  
  Function from IDP1 - removed for KCL1
  public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}*/
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	
	public function getOrderedObjects($object_id=0) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, ".$this->getNameFieldName()." as name
					, ".$this->getTableField()."_ref as ref
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					FROM ".$this->getTableName()." A
					WHERE  ".($this->checkIntRef($object_id) ? $this->getIDFieldName()." = ".$object_id." AND " : "")." ".$this->getActiveStatusSQL("A");
				$res2 = $this->mysql_fetch_all_by_id($sql, "id");
		return $res2;
	}

/**
 * Function to get details for viewing / confirmation
 */
	public function getOrderedObjectsWithChildren($object_id=0){
		$res = $this->getOrderedObjects($object_id);
		$data = array(
			$this->getMyObjectType()=>$res,
		);
		
		$goalObject = new KCL1_SUBFUNCTION();
		$goal_objects = $goalObject->getOrderedObjects($object_id);
		$data[$goalObject->getMyObjectType()] = $goal_objects;
		
		$resultsObject = new KCL1_ACTIVITY();
		$results_objects = $resultsObject->getRawOrderedObjects($object_id);
		$data[$resultsObject->getMyObjectType()] = $results_objects;
		
		return $data;
	}


	
	public function getSimpleDetails($id=0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'id'=>$id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"])>0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}
	

	
	
	
	
	public function getRecordsForListDisplay($comp_id=array(),$format_for_list=true) {
		if(!is_array($comp_id)) {
			$comp_id = array($comp_id);
		}
		$comp_id = ASSIST_HELPER::removeBlanksFromArray($comp_id);
			
		$rows = array();
		if(count($comp_id)>0) {
			
			$listObject = new KCL1_LIST("proficiency");
			$prof_objects = $listObject->getActiveListItemsFormattedForSelect(); //ASSIST_HELPER::arrPrint($prof_objects);
			if(count($prof_objects)>0) {
				$sql = "SELECT CP.".$this->getIDFieldName()." as id
							, CP.".$this->getNameFieldName()." as name 
							, CP.".$this->getParentFieldName()." as comp 
							, CP.".$this->getSecondaryParentFieldName()." as prof 
						FROM ".$this->getTableName()." CP 
						WHERE (".$this->getActiveStatusSQL("CP").") 
						AND ".$this->getParentFieldName()." IN (".implode(",",$comp_id).")
						AND ".$this->getSecondaryParentFieldName()." IN (".implode(",",array_keys($prof_objects)).")";
				$res = $this->mysql_fetch_all_by_id($sql, "id");
				$res2 = array(); //ASSIST_HELPER::arrPrint($res);
				foreach($res as $key => $r) { //ASSIST_HELPER::arrPrint($r); echo "<P>".$r['prof']."</p>";
					if(isset($prof_objects[$r['prof']])) {
						if($format_for_list) {
							$n = "<span class=b>".$prof_objects[$r['prof']].":</span> ".$r['name'];
						} else {
							$n = $r['name'];
						}
						$res2[$r['comp']][$r['prof']] = array(
							'id'=>$key,
							'name'=>$n
						);
					}
				} //ASSIST_HELPER::arrPrint($res2);
				if($format_for_list) {
					foreach($comp_id as $ci) {
						if(isset($res2[$ci])) {
							$ci_rows = $res2[$ci];
							foreach($prof_objects as $pi => $p) {
								if(isset($ci_rows[$pi])) {
									$r = $ci_rows[$pi];
									$rows[$ci][$r['id']] = $r['name'];
								}
							}
						}
					}
				} else {
					$rows = $res2;
				}
			}
		} //ASSIST_HELPER::arrPrint($rows);
		return $rows;
		
	}
	
	
	public function getRawObjectGroupedByParent($parent_id) {
		$sql = "SELECT ".$this->getIDFieldName()." as id
		 			, ".$this->getParentFieldName()." as parent
		 			, ".$this->getSecondaryParentFieldName()." as secondary
		 			, ".$this->getNameFieldName()." as name
		 FROM ".$this->getTableName()." AC WHERE AC.".$this->getParentFieldName()." = ".$parent_id." AND ".$this->getActiveStatusSQL("AC");
		$rows = $this->mysql_fetch_all_by_id($sql,"id");
		$data = array();
		
		foreach($rows as $i => $r) {
			$data[$r['parent']][$r['secondary']] = $r['name'];
		}
		
		return $data;
	}		
	
	
	
	
	
	
	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>