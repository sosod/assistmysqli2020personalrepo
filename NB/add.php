<?php
include("inc_header.php");
$nbtype = isset($_GET['t']) ? $_GET['t'] :"";
if(strlen($nbtype)==0)
{
    $nbtype = "a";
    $nbid = 0;
    $nbrow = "";
}
else
{
    $nbid = $_GET['i'];
    $sql = "SELECT t.tkname, t.tksurname, n.* FROM assist_".$cmpcode."_nb_notices n, assist_".$cmpcode."_timekeep t WHERE t.tkid = n.nbtkid AND nbid = ".$nbid;
    //include("inc_db_con.php");
        $nbrow = $helper->mysql_fetch_one($sql);
    //mysql_close();
}
?>
<?php
switch($nbtype){
    case "a":
        echo("<h1>Add a Notice</h1>");
        $nbsubject = "<span class=nbsubject>Subject:</span> <input type=text name=nbsubject value=\"\" maxlength=45 size=40>";
        $nbreturn = "<a href=main.php>Return to Notice Board</a>";
        break;
    case "r":
        echo("<h1>Reply to a Notice</h1>");
        $nbsubject = "<span class=nbsubject>Re: ".$nbrow['nbsubject']."</span><input type=hidden name=nbsubject value=\"Re: ".$nbrow['nbsubject']."\" maxlength=50>";
        $nbreturn = "<a href=# onclick=\"history.back();\">Return to previous notice</a>";
        break;
}
?>
<form name=nbnotice action=add_process.php method=post enctype="multipart/form-data" onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=nbreplyid value=<?php echo($nbid); ?>>
<table border="1" cellspacing="0" cellpadding="5">
	<tr class=tdnblight>
		<td><?php echo($nbsubject); ?><br>
            <font size="1">by <?php echo($tkname); ?> on <?php echo(date("d F Y H:i")); ?></font>
            <hr size=1>
            <?php
            if($nbtype == "r")
            {
                echo("<input type=hidden name=nbcateid value=".$nbrow['nbcateid'].">");
            }
            else
            {
                ?>
                <b>Category:</b> <select name=nbcateid><option selected value=X>--- SELECT ---</option><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_nb_categories WHERE cateyn = 'Y' ORDER BY catesort";
                    //include("inc_db_con.php");
                $rows = $helper->mysql_fetch_all($sql);
                        //while($row = mysql_fetch_array($rs)) {
                foreach ($rows as $row){
                            echo("<option value=".$row['cateid'].">".$row['category']."</option>");
                        }
                    //mysql_close();
                ?></select><br>&nbsp;<br>
                <?php
            }
            ?>
            <b>Message:</b><br><textarea name=nbmessage rows=10 cols=50 style="margin-right: 30; margin-left: 10"></textarea>
            <hr size=1>
    		<p class=nbattach>Attachment(s)</p>
            <table style="margin-left: 35; margin-right: 0; margin-top: 5; margin-bottom: 10" border="0" cellpadding="2" cellspacing="0">
                <tr class=tdgeneral>
                    <td valign="center"><img src="/pics/bullet.gif" width="10" height="10" hspace="16" alt="bullet" style="margin-right: -10"></td>
				    <td valign="top"><input type=file name=at1></td>
    			</tr>
                <tr class=tdgeneral>
                    <td valign="center"><img src="/pics/bullet.gif" width="10" height="10" hspace="16" alt="bullet" style="margin-right: -10"></td>
				    <td valign="top"><input type=file name=at2></td>
    			</tr>
                <tr class=tdgeneral>
                    <td valign="center"><img src="/pics/bullet.gif" width="10" height="10" hspace="16" alt="bullet" style="margin-right: -10"></td>
				    <td valign="top"><input type=file name=at3></td>
    			</tr>
            </table>
            <p><input type=submit value=Submit class=isubmit>
        </td>
	</tr>
<?php
if($nbtype == "r")
{
?>
	<tr class=tdnbdark>
		<td><b>The message you are replying to is:</b><br>&nbsp;<br>
		  <span class=nbsubject><?php echo($nbrow['nbsubject']); ?></span><br>
            <font size="1">by <?php echo($nbrow['tkname']." ".$nbrow['tksurname']); ?> on <?php echo(date("d F Y H:i",$nbrow['nbdate'])); ?></font>
            <hr size=1>
            <?php echo(str_replace(chr(10),"<br>",$nbrow['nbmessage'])); ?>
            <hr size=1>
        </td>
	</tr>
<?php
}
?>
</table>
</form>
<?php echo $helper->displayGoBack("",""); ?>
<script language=JavaScript>
	function Validate(me) {
		nbsubject = me.nbsubject.value;
		nbcateid = me.nbcateid.value;
		nbmessage = me.nbmessage.value;
		if(nbsubject.length == 0)
		{
			alert("Please enter a subject for your notice.");
		}
		else
		{
			if(nbcateid.length == 0 || nbcateid == "X")
			{
				alert("Please select a category for your notice.");
			}
			else
			{
				if(nbmessage.length == 0)
				{
					alert("Please enter a message.");
				}
				else
				{
					return true;
				}
			}
		}
		return false;
	}
</script>
</body>

</html>
