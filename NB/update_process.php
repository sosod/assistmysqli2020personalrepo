<?php
include("inc_header.php");

$nbid = isset($_GET['n']) ? $_GET['n'] : 0;

//SET NBYN = N
$sql = "UPDATE assist_".$cmpcode."_nb_notices SET nbyn = 'N' WHERE nbid = ".$nbid;
//include("inc_db_con.php");
$helper->db_update($sql);
    //ADD TRANSACTION LOG
    $tsql = $sql;
    $tref = "NB";
    $trans = "Deleted notice ".$nbid;
    //include("inc_transaction_log.php");
//SET ATTACHYN = N
$sql = "UPDATE assist_".$cmpcode."_nb_attach SET atyn = 'N' WHERE atnbid = ".$nbid;
//include("inc_db_con.php");
$helper->db_update($sql);
    //ADD TRANSACTION LOG
    $tsql = $sql;
    $tref = "NB";
    $trans = "Deleted attachment records for notice ".$nbid;
    //include("inc_transaction_log.php");
//RENAME FILES
$sql = "SELECT * FROM assist_".$cmpcode."_nb_attach WHERE atnbid = ".$nbid;
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){
        $atfile = $row['atdoc'];
        $fileloc = "../files/".$cmpcode."/NB/";
        $atfilenew = "deleted_".date("dmy")."_".$atfile;
        rename($atfile,$atfilenew);
    }
//mysql_close();
//SET NBYN = N FOR REPLIES
$sql = "UPDATE assist_".$cmpcode."_nb_notices SET nbyn = 'N' WHERE nbreplyid = ".$nbid;
//include("inc_db_con.php");
$helper->db_update($sql);
    //ADD TRANSACTION LOG
    $tsql = $sql;
    $tref = "NB";
    $trans = "Deleted replies to notice ".$nbid;
    //include("inc_transaction_log.php");
//SET ATTACHYN = N
$sql = "SELECT nbid FROM assist_".$cmpcode."_nb_notices WHERE nbreplyid = ".$nbid;
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
    $nb = 0;
    //while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){
        $nbrow[$nb] = $row['nbid'];
        $nb++;
    }
//mysql_close();
$at = 0;
foreach($nbrow as $nbrep)
{
    $sql = "UPDATE assist_".$cmpcode."_nb_attach SET atyn = 'N' WHERE atnbid = ".$nbrep;
    //include("inc_db_con.php");
	$helper->db_update($sql);
        //ADD TRANSACTION LOG
        $tsql = $sql;
        $tref = "NB";
        $trans = "Deleted attachment records for notice ".$nbrep." due to deletion of notice ".$nbid;
        //include("inc_transaction_log.php");
    //RENAME FILES
    $sql = "SELECT * FROM assist_".$cmpcode."_nb_attach WHERE atnbid = ".$nbrep;
    //include("inc_db_con.php");
    $rows = $helper->mysql_fetch_all($sql);
    foreach ($rows as $row){
        //while($row = mysql_fetch_array($rs)) {
            $atfile = $row['atdoc'];
            $fileloc = "../files/".$cmpcode."/NB/";
            $atfilenew = "deleted_".date("dmy")."_".$atfile;
            rename($atfile,$atfilenew);
        }
    //mysql_close();
}

echo("<script language=JavaScript>document.location.href='update.php';</script>");
?>
</body>

</html>
