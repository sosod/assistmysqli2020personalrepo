<?php
include("inc_header.php");

$a = isset($_GET['a']) ? $_GET['a'] : "";
$i = isset($_GET['i']) ? $_GET['i'] : 0;

if($a == "add")
{
    $sql = "SELECT * FROM assist_".$cmpcode."_nb_list_users WHERE tkid = '".$i."' AND yn = 'Y'";
    //include("inc_db_con.php");
        if($helper->db_get_num_rows($sql)>0)
        {
            $err = "Y";
        }
        else
        {
            $err = "N";
        }
    //mysql_close();
    if($err == "N")
    {
        $sql = "INSERT INTO assist_".$cmpcode."_nb_list_users SET tkid = '".$i."', yn = 'Y'";
        //include("inc_db_con.php");
	    $helper->db_insert($sql);
            $tsql = $sql;
            $tref = "NB";
            $trans = "Added MA access to NB for user ".$i;
            //include("inc_transaction_log.php");
    }
}
else
{
    if($a == "del")
    {
        $sql = "UPDATE assist_".$cmpcode."_nb_list_users SET yn = 'N' WHERE tkid = '".$i."'";
        //include("inc_db_con.php");
	    $helper->db_update($sql);
            $tsql = $sql;
            $tref = "NB";
            $trans = "Removed MA access to NB for user ".$i;
            //include("inc_transaction_log.php");
    }
}
?>

<h1 class=fc>Notice Board: Setup - Module Administrators</h1>
<form name=adduser>
<table width=350>
    <tr>
        <td class=tdheader>User</td>
        <td class=tdheader>Action</td>
    </tr>
<?php
$sadmin = "";
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_setup s WHERE t.tkid <> '0000' AND s.ref = 'NB' AND s.refid = 0 AND s.value = t.tkid";
//include("inc_db_con.php");
if($helper->db_get_num_rows($sql) > 0)
{
    $row = $helper->mysql_fetch_one($sql);
    $sadmin = $row['tkid'];
    $tki = $row['tkid'];
    $tkn = $row['tkname']." ".$row['tksurname'];
    echo("<tr>");
        echo("<td class=tdgeneral colspan=2>".$tkn." (Setup Administrator)</td>");
    echo("</tr>");
}
//mysql_close();

if(strlen($sadmin)==0)
{
    $sadmin = "0000";
}
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_nb_list_users u WHERE u.tkid = t.tkid AND t.tkstatus = 1 AND t.tkid <> '0000' AND t.tkid <> '".$sadmin."' AND u.yn = 'Y'";
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
    //while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){
    $tki = $row['tkid'];
    $tkn = $row['tkname']." ".$row['tksurname'];
    echo("<tr>");
        echo("<td class=tdgeneral>".$tkn."</td>");
        $tkn2 = str_replace(" ","_",$tkn);
        echo("<td class=tdgeneral><input type=button value=Delete onclick=\"delUser('".$tki."','".$tkn2."')\"></td>");
    echo("</tr>");
}
//mysql_close();
?>
<?php
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m WHERE m.usrtkid = t.tkid AND m.usrmodref = 'NB' AND t.tkstatus = 1 AND t.tkid <> '0000' AND t.tkid NOT IN (SELECT tkid FROM assist_".$cmpcode."_nb_list_users WHERE yn = 'Y')";
//include("inc_db_con.php");
if($helper->db_get_num_rows($sql) > 0)
{
?>
    <tr>
        <td class=tdgeneral><select name=nbuser><option selected value=X>--- SELECT ---</option>
        <?php
        $rows = $helper->mysql_fetch_all($sql);
        //while($row = mysql_fetch_array($rs)) {
        foreach ($rows as $row){
            $tki = $row['tkid'];
            $tkn = $row['tkname']." ".$row['tksurname'];
            echo("<option value=\"".$tki."\">".$tkn."</option>");
        }
        ?>
        </select></td>
        <td class=tdgeneral><input type=button value=Add onclick="addUser()"></td>
    </tr>
<?php
}
//mysql_close();
?>
    <script type="text/javascript">
		function addUser() {
			var i = document.adduser.nbuser.value;
			if(i != "X" && i.length > 0)
			{
				document.location.href = "setup_users.php?a=add&i="+i;
			}
		}
		function delUser(i,n) {
			while(n.indexOf("_")>0)
			{
				n = n.replace("_"," ");
			}
			if(confirm("Are you sure you wish to remove Module Administrative access from "+n+"?")==true)
			{
				document.location.href = "setup_users.php?a=del&i="+i;
			}
		}
    </script>
</table>
</form>
</body></html>
