<?php
include("inc_header.php");

?>
<h1>Update your notices</h1>
<table border="1" cellspacing="0" cellpadding="5">
	<tr >
		<th>Ref</th>
		<th>Category</th>
		<th>Post</th>
		<th>Replies</th>
		<th>Action</th>
	</tr>
<?php
$sql = "SELECT n.*, c.*, t.tkname, t.tksurname FROM assist_".$cmpcode."_nb_notices n, assist_".$cmpcode."_nb_categories c, assist_".$cmpcode."_timekeep t WHERE n.nbcateid = c.cateid AND n.nbtkid = '".$tkid."' AND n.nbyn = 'Y' AND n.nbtkid = t.tkid ORDER BY n.nbsort DESC";
//include("inc_db_con.php");
    $nb = $helper->db_get_num_rows($sql);
    $rows = $helper->mysql_fetch_all($sql);
    $n = 0;
    //while($row = mysql_fetch_array($rs)) {
    foreach ($rows as $row){
        $nbrow[$n] = $row;
        $n++;
    }
//mysql_close();
if($nb > 0)
{
    $nbcol = 0;
    foreach($nbrow as $nrow)
    {
        switch($nbcol)
        {
            case 0:
                echo("<tr class=tdnblight>");
                $nbcol=1;
                break;
            case 1:
                echo("<tr class=tdnbdark>");
                $nbcol=0;
                break;
        }
        //CHECK FOR ATTACHMENTS
        $sql = "SELECT * FROM assist_".$cmpcode."_nb_attach WHERE atnbid = ".$nrow['nbid'];
        //include("inc_db_con.php");
            $at = $helper->db_get_num_rows($sql);
        //mysql_close();
        //CHECK FOR REPLIES
        $sql = "SELECT * FROM assist_".$cmpcode."_nb_notices WHERE nbreplyid = ".$nrow['nbid']." AND nbyn = 'Y' ORDER BY nbsort DESC";
        //include("inc_db_con.php");
            $nbr = $helper->db_get_num_rows($sql);
        //mysql_close();
?>
		<td class="middle center"><?php echo($nrow['nbid']); ?></td>
		<td class=middle><?php echo($nrow['category']); ?></td>
		<td class=middle><?php echo("<a href=view_post.php?s=update&i=".$nrow['nbid'].">".$nrow['nbsubject']."</a>");?><?php if($at>0){?> <img border="0" src="attach.gif" width="7" height="11"><br><?php } else { echo("<br>"); }?>
        <small>by <?php echo($nrow['tkname']." ".$nrow['tksurname']); ?> on <?php echo(date("d F Y H:i",$nrow['nbdate'])); ?></small></td>
		<td class="middle center"><?php echo($nbr); ?></td>
		<td class=middle><input type=button value=Delete onclick="delNotice(<?php echo($nrow['nbid']); ?>)"></td>
	</tr>
<?php
    }
}
else
{
    ?>
	<tr class=tdnblight>
		<td colspan=4>No notices to display.</td>
	</tr>
    <?php
}
//mysql_close();
?>
</table>
<div align="left">
	<table border="0" id="table2" cellspacing="0" cellpadding="0" style="margin-top: 20" class=noborder>
		<tr class=tdgeneral>
			<td class=noborder><img border="0" src="../pics/tri_left.gif" width="20" height="25"></td>
			<td class=noborder>&nbsp;&nbsp;<a href=main.php>Return to Notice Board</a></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	function delNotice(n) {
		if(confirm("Are you sure you want to delete notice "+n+"?\n\nNote: Any replies to your notice will be deleted as well.")==true)
		{
			document.location.href = "update_process.php?n="+n;
		}
	}
</script>
</body>

</html>
