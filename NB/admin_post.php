<?php
include("inc_header.php");
$nbid = isset($_GET['i']) ? $_GET['i'] : 0;

//GET NUMBER OF REPLY POSTS
$sql = "SELECT * FROM assist_".$cmpcode."_nb_notices WHERE nbreplyid = ".$nbid." AND nbyn = 'Y'";
//include("inc_db_con.php");
$nbr = $helper->db_get_num_rows($sql);
//mysql_close();
//INCREASE POST COUNT FOR ORIGINAL NOTICE
$nbr++;
//GET NOTICE DETAILS WITH TIMEKEEP AND CATEGORY
$sql = "SELECT t.tkname, t.tksurname, c.category, n.* ";
$sql.= "FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_nb_categories c, assist_".$cmpcode."_nb_notices n ";
$sql.= "WHERE n.nbid = ".$nbid." ";
$sql.= "AND n.nbtkid = t.tkid ";
$sql.= "AND n.nbcateid = c.cateid";
//include("inc_db_con.php");
    $nbrow = $helper->mysql_fetch_one($sql);
//mysql_close();
?>
<h1><?php echo($nbrow['category']); ?></h1>
<table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" class=noborder>
	<tr class="tdgeneral noborder">
		<td class=noborder><font size="1"><input type=button value=Reply style="font-family: Arial; font-size: 10pt; margin-top: 5; margin-bottom: 5" onclick=noticeReply(<?php echo($nbid)?>)></font></td>
		<td class=noborder valign=bottom align=right><font size="2"><?php echo($nbr);?> post<?php if($nbr>1){echo("s");}?>&nbsp;</font></td>
	</tr>
	<tr>
		<td colspan=2 class=noborder>
<table border="1" width="100%" cellspacing="0" cellpadding="5">
<?php
//ORIGINAL NOTICE
?>
	<tr class=tdnblight>
		<td><span class=nbsubject><?php echo($nbrow['nbsubject']);?></span><br>
        <font size="1">by <?php echo($nbrow['tkname']." ".$nbrow['tksurname']); ?> on <?php echo(date("d F Y H:i",$nbrow['nbdate'])); ?></font>
        <hr size=1>
<?php echo(str_replace(chr(10),"<br>",$nbrow['nbmessage'])); ?>
        <hr size=1>
<?php
//IF ATTACHMENTS
$sql = "SELECT * FROM assist_".$cmpcode."_nb_attach WHERE atnbid = ".$nbid." AND atyn = 'Y' ORDER BY atdisplay DESC, atid ASC";
//include("inc_db_con.php");
    if($helper->db_get_num_rows($sql) > 0)
    {
?>
		<p class=nbattach>Attachment(s)</p>
        <table style="margin-left: 35; margin-right: 0; margin-top: 5; margin-bottom: 10" border="0" cellpadding="2" cellspacing="0" width="100%" class=noborder>
        <?php
        $at = 1;
        $rows = $helper->mysql_fetch_all($sql);
        //while($atrow = mysql_fetch_array($rs)) {
        foreach ($rows as $atrow){
            $atdoc = $atrow['atdoc'];
            $atdisp = $atrow['atdisplay'];
            $atfile = "../files/".$cmpcode."/NB/".$atdoc;
            if(file_exists($atfile))
            {
                switch($atdisp) {
                    case "link":
                        $attach = "<a href=/files/".$cmpcode."/NB/".$atdoc." target=_blank>Attachment ".$at."</a>";
                        break;
                    case "img":
                        $attach = "<a href=/files/".$cmpcode."/NB/".$atdoc." target=_blank><img src=/files/".$cmpcode."/NB/".$atdoc." border=0></a>";
                        break;
                }
                ?>
                <tr class=tdgeneral>
                    <td class=noborder valign="top" width="42"><img src="/pics/bullet.gif" width="10" height="10" hspace="16" alt="bullet"></td>
				    <td class=noborder valign="top" width="100%"><?php echo($attach); ?></td>
    			</tr>
                <?php
            }
            $at++;
        }
        ?>
        </table>
<?php
    }   //END IF ATTACHMENTS
//mysql_close();
?>
        </td>
	</tr>
<?php
//FOREACH REPLY
if($nbr>1)
{
    $sql = "SELECT t.tkname, t.tksurname, n.* ";
    $sql.= "FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_nb_notices n ";
    $sql.= "WHERE n.nbreplyid = ".$nbid." ";
    $sql.= "AND n.nbtkid = t.tkid ";
    $sql.= "AND nbyn = 'Y' ORDER BY nbsort ASC";
    //include("inc_db_con.php");
        $nbrep = 0;
//        $nbrcount = $nbr-1;
//        while($nbrep < 10)
	$rows = $helper->mysql_fetch_all($sql);
	//while($nbreprow = mysql_fetch_array($rs)) {
	foreach ($rows as $nbreprow){
//            $nbreprow = mysql_fetch_array($rs);
            $nbrrow[$nbrep] = $nbreprow;
            $nbrep++;
        }
    //mysql_close();
    $nbcol = 1;
    foreach($nbrrow as $nbreprow)
    {
        switch($nbcol)
        {
            case 0:
                $tr = "<tr class=tdnblight>";
                $nbcol=1;
                break;
            case 1:
                $tr = "<tr class=tdnbdark>";
                $nbcol=0;
                break;
        }
        echo($tr);
?>
		<td><span class=nbsubject><?php echo($nbreprow['nbsubject']);?></span><br>
        <font size="1">by <?php echo($nbreprow['tkname']." ".$nbreprow['tksurname']); ?> on <?php echo(date("d F Y H:i",$nbreprow['nbdate'])); ?></font>
        <hr size=1>
<?php echo(str_replace(chr(10),"<br>",$nbreprow['nbmessage'])); ?>
        <hr size=1>
<?php
//IF ATTACHMENTS
$sql = "SELECT * FROM assist_".$cmpcode."_nb_attach WHERE atnbid = ".$nbreprow['nbid']." AND atyn = 'Y' ORDER BY atdisplay DESC, atid ASC";
//include("inc_db_con.php");
    if($helper->db_get_num_rows($sql) > 0)
    {
?>
		<p class=nbattach>Attachment(s)</p>
        <table style="margin-left: 35; margin-right: 0; margin-top: 5; margin-bottom: 10" border="0" cellpadding="2" cellspacing="0" width="100%" class=noborder>
        <?php
        $at = 1;
        $rows = $helper->mysql_fetch_all($sql);
        //while($atrow = mysql_fetch_array($rs)) {
        foreach ($rows as $atrow){
            $atdoc = $atrow['atdoc'];
            $atdisp = $atrow['atdisplay'];
            $atfile = "../files/".$cmpcode."/NB/".$atdoc;
            if(file_exists($atfile))
            {
                switch($atdisp) {
                    case "link":
                        $attach = "<a href=/files/".$cmpcode."/NB/".$atdoc." target=_blank>Attachment ".$at."</a>";
                        break;
                    case "img":
                        $attach = "<a href=/files/".$cmpcode."/NB/".$atdoc." target=_blank><img src=/files/".$cmpcode."/NB/".$atdoc." border=0></a>";
                        break;
                }
                echo($tr);
                ?>
                    <td class=noborder valign="top" width="42"><img src="/pics/bullet.gif" width="10" height="10" hspace="16" alt="bullet"></td>
				    <td class=noborder valign="top" width="100%"><?php echo($attach); ?></td>
    			</tr>
                <?php
            }
            $at++;
        }
        ?>
        </table>
<?php
    }   //END IF ATTACHMENTS
//mysql_close();
?>
        </td>
	</tr>
<?php
$tr = "";
//END FOREACH REPLY
    }
}
?>
</table>
</td>
</tr>
	<tr class=tdgeneral>
		<td class=noborder><font size="1"><input type=button value=Reply style="font-family: Arial; font-size: 10pt; margin-top: 5; margin-bottom: 5" onclick=noticeReply(<?php echo($nbid)?>)></font></td>
		<td class=noborder valign=top align=right><font size="2"><?php echo($nbr);?> post<?php if($nbr>1){echo("s");}?>&nbsp;</font></td>
	</tr>
</table>
<div align="left">
	<table border="0" id="table2" cellspacing="0" cellpadding="0" style="margin-top: 20" class=noborder>
		<tr class=tdgeneral>
			<td class=noborder><img border="0" src="../pics/tri_left.gif" width="20" height="25"></td>
			<td class=noborder>&nbsp;&nbsp;<?php echo("<a href=admin_list.php?c=".$nbrow['nbcateid'].">Return to ".$nbrow['category']."</a>"); ?></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	function noticeReply(n) {
		document.location.href = "add.php?t=r&i="+n;
	}
</script>
</body>

</html>
