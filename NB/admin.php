<?php
include("inc_header.php");
$req_id = isset($_REQUEST['i']) ? $_REQUEST['i'] : 0;
if(isset($_REQUEST['act'])) {
switch($_REQUEST['act']) {
case "NEW":
if(isset($_REQUEST['new_cate']) && strlen($_REQUEST['new_cate'])>0) {
	$sql = "INSERT INTO assist_".$cmpcode."_nb_categories (cateid, category, cateyn, catesort) VALUES (null,'".$helper->code($_REQUEST['new_cate'])."','Y',0)";
	//include("inc_db_con.php");
	$id = $helper->db_insert($sql);//mysql_insert_id();
	if($helper->checkIntRef($id)) {
		$sql = "UPDATE assist_".$cmpcode."_nb_categories SET catesort = $id WHERE cateid = $id";
		//include("inc_db_con.php");
		$helper->db_update($sql);
		$result = array("ok","Category '".$_REQUEST['new_cate']."' has been added successfully.");
	} else {
		$result = array("error","An error occurred.  Please try again.");
	}
} else {
			$result = array("error","An error occurred.  Please try again.");
}
break;
case "DEL":
	if(isset($_REQUEST['i']) && checkIntRef($_REQUEST['i'])) {
		$sql = "UPDATE assist_".$cmpcode."_nb_categories SET cateyn = 'N' WHERE cateid = ".$req_id;
		//include("inc_db_con.php");
		$mar = $helper->db_update($sql);//mysql_affected_rows();
		if($helper->checkIntRef($mar)) {
			$result = array("ok","Category ".$req_id." has been deleted.");
		} else {
			$result = array("info","No category with reference ".$req_id." could be found to be deleted.");
		}
	} else {
			$result = array("error","An error occurred.  Please try again.");
	}
break;
case "EDIT":
	if(isset($_REQUEST['i']) && $helper->checkIntRef($req_id) && isset($_REQUEST['new_cate']) && strlen($_REQUEST['new_cate'])>0) {
		$sql = "UPDATE assist_".$cmpcode."_nb_categories SET category = '".$helper->code($_REQUEST['new_cate'])."' WHERE cateid = ".$req_id;
		//include("inc_db_con.php");
		$mar = $helper->db_update($sql);//mysql_affected_rows();
		if($mar>0) {
			$result = array("ok","Category ".$req_id." has been updated.");
		} else {
			$result = array("info","No change was found to be made.");
		}
	} else {
			$result = array("error","An error occurred.  Please try again.");
	}
break;
}
}


$sql = "SELECT * FROM assist_".$cmpcode."_nb_categories WHERE cateyn = 'Y' ORDER BY catesort";
//include("inc_db_con.php");
$rows = $helper->mysql_fetch_all($sql);
$cate = array();
//while($row = mysql_fetch_assoc($rs)) {
foreach ($rows as $row){
    $cate[$row['cateid']] = $row; }
//mysql_close($con);
?>
<h1><?php
echo($cmpname."'");
if(substr($cmpname,strlen($cmpname)-1,1)!="s")
{
    echo("s");
}
 ?> Noticeboard - Admin</h1>
<?php
$result = isset($result) ? $result : array();
$helper->displayResult($result); ?>
<form method=post action=admin.php><input type=hidden name=act value=NEW /><p><b>New Category:</b> <input type=text size=20 name=new_cate maxlength=75 /> <input type=submit value=Add /></p></form>
<table width=700>
	<tr>
		<th>TOPIC</th>
		<th width=50>POSTS</th>
		<th>LAST POST</th>
		<th width=50>&nbsp;</th>
	</tr>
	<?php
    $sql = "SELECT count(nbid) AS nbcount, nbcateid FROM assist_".$cmpcode."_nb_notices WHERE nbyn = 'Y' GROUP BY nbcateid";
    //include("inc_db_con.php");
    $rows = $helper->mysql_fetch_all($sql);
        //while($row = mysql_fetch_array($rs)) {
    $nbcount = array();
        foreach ($rows as $row){
            $nbcount[$row['nbcateid']] = $row['nbcount'];
        }
    //mysql_close();
    for($c=1;$c<4;$c++)
    {
	    $nbcount_c = array_key_exists($c,$nbcount) ? $nbcount[$c] : 0;
	    if($nbcount_c>0)
        {
            $sql = "SELECT tkname, tksurname, nbdate FROM assist_".$cmpcode."_nb_notices n, assist_".$cmpcode."_timekeep t WHERE t.tkid = n.nbtkid AND n.nbcateid = ".$c." AND nbyn = 'Y' ORDER BY nbdate ASC";
            //include("inc_db_con.php");
                $row = $helper->mysql_fetch_one($sql);
                $nbuser[$c] = $row['tkname']." ".$row['tksurname'];
                $nbtime[$c] = $row['nbdate'];
            //mysql_close();
        }
        else
        {
            $nbcount[$c] = 0;
            $nbuser[$c] = "";
            $nbtime[$c] = "";
        }
    }
	$id = isset($id) ? $id : 0;
	$nbcount_id = array_key_exists($id,$nbcount) ? $nbcount[$id] : 0;
	foreach($cate as $id => $c) {
    ?>
	<tr height=40>
		<?php echo "<td class=middle><label id=c-".$id."><a href=\"admin_list.php?c=".$id."\" >".$helper->decode($c['category'])."</a></label></td>"; ?>
		<td class="middle center"><?php echo isset($nbcount_id) ? $nbcount_id : 0; ?></td>
		<td class="middle"><?php if($nbcount_id>0) {?>by <?php echo($nbcount_id); ?><br><small>on <?php echo(date("d F Y H:i",$nbtime[$id])."</small>"); } else { echo("&nbsp;"); } ?></td>
		<td class="middle center"><input type=button value=Edit id=<?php echo $id; ?> categ='<?php echo $helper->decode($c['category']) ?>' class=edit /></td>
	</tr>
	<?php
	}
	?>
</table>
<div id=dialog_edit title="Edit Category">
<form method=post action=admin.php><input type=hidden name=act value=EDIT />
<table width=100%>
	<tr>
		<th class=left>Reference:</th>
		<td class=middle><span id=ref></span><input type=hidden name=i id=i value="" /></td>
	</tr>
	<tr>
		<th class=left>Old Category:</th>
		<td class=middle><span id=val></span></td>
	</tr>
	<tr>
		<th class=left>New Category:</th>
		<td class=middle><input type=text value="" size=30 id=new name=new_cate maxlength=75 /></td>
	</tr>
	<tr>
		<th class=left>&nbsp;</th>
		<td class=middle><input type=submit value=Save class=isubmit /><span style="float: right; margin-top: -23px"><input type=button value=Delete class=idelete /></span></td>
	</tr>
</table>
<p><input type=button value=Cancel id=cancel /></p>
</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dialog_edit").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$(".idelete").click(function() {
		var i = $("#i").attr("value");
		if(confirm("Are you sure you wish to delete this category?")) {
			document.location.href = 'admin.php?act=DEL&i='+i;
		}
	});
	$(".edit").click(function() {
		var i = $(this).attr("id");
		var c = $(this).attr("categ");
		$("#ref").html(i);
		$("#i").attr("value",i);
		$("#val").html(c);
		$("#new").attr("value",c);
		$("#dialog_edit").dialog("open");
	});
	$("#cancel").click(function() {
		$("#dialog_edit").dialog("close");
	});
});
</script>


</body></html>
