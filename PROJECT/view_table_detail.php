<?php
$width = $_SESSION['screen']['width'];
if($width*.75<700) {
	$width = 700;
} else {
	$width*=0.75;
}

$my_head = $mheadings[$section];
$result_head = $mheadings[$r_section];
if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if(isset($my_head[$table_fld.'repkpi'])) {
	unset($my_head[$table_fld.'repkpi']);
}
if($section=="KPI" || $section == "TOP") {
	$obj_tt = $object['tt'];
	$obj_ct = $object['ct'];
}
//arrPrint($my_head);
//arrPrint($object);
//arrPrint($extras);
?>
<h2>Details</h2>
<table width=<?php echo $width; ?>>
	<tr>
		<th width=25% class=left>Reference</th>
		<td><?php echo (isset($id_label)?$id_label:"").$obj_id; ?></td>
	</tr>
<?php 
foreach($my_head as $fld => $h) {
	$v = (!isset($object[$fld]) || ($h['h_type']=="LIST" && strlen($object[$fld])==0)) ? $unspecified : $object[$fld];
	echo chr(10)."<tr>";
		echo "<th class=left>".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;</th>";
	switch($h['h_type']) {
/*		case "CAP":
			echo "<td>".$fld;
			echo "</td>";
			break;*/
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			$values = $extras[$fld];
			$val = array();
			foreach($values as $v) {
				$val[] = ((strlen($v['code'])>0 && strtolower($v['value'])!="all") ? decode($v['code']).": " : "").decode($v['value']);
			}
			echo "<td>";
				echo implode("<br />",$val);
			echo "</td>";
			break;
		case "DATE":
			echo "<td>".($v>0 ? date("d M Y",strtotime($v)) : "")."</td>";
			break;
		case "TEXTLIST":
			echo "<td>";//.$v;
				$a = array();
				if(strlen($v)>0) {
					$a = explode(";",$v);
					if(in_array("0",$a)) {
						$a = array("0");
					}
				} else {
					$a[] = "0";
				} 
				//arrPrint($a);
				$v = array();
				foreach($a as $k){
					//echo "<P>".$k." ::> ".$fld.(isset($extras[$fld][$k]))."</p>";
					if(strlen($k)>0) {
						if($k=="0" || !isset($extras[$fld][$k])) {
							$v[] = $unspecified;
							break;
						} else {
							$v[] = $extras[$fld][$k]['value'];
						}
					}
				}
				echo count($v)>0 ? implode("; ",$v) : $unspecified;
			echo "</td>";
			break;
		default:
			switch($fld) {
				case "kpi_annual":
				case "kpi_revised":
				case "top_annual":
				case "top_revised":
					echo "<td>".KPIresultDisplay($v,$obj_tt)."</td>";
					break;
				default:
					echo "<td>".$v."</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
?>
</table>
<h2>Results</h2>
<?php //echo "ABC"; arrPrint($lists);
switch($section) {
	case "KPI":
	case "TOP":
		if($section=="TOP") {
			$mytime = $timeObj->getTopTime();
		} else {
			$mytime = $timeObj->getAllTime();
		}
		$kr = $myObj->getUpdateFormDetails($obj_id); //arrPrint($kr);
		$drawObj->drawKPIResults($myObj,$result_head,$mytime,$kr);

		if($section=="TOP") {
			echo "<h2>Forecast Annual Target</h2>
					<table id=tblfore>";
				foreach($lists['years'] as $i => $l) {
					if($i>1) {
						echo "<tr>
								<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
								<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['tf_value']) ? $forecast[$i]['tf_value'] : 0),$obj_tt)."&nbsp;</td>
							</tr>";
					}
				}			
			echo "</table>";
		}
		break;
	case "CAP":
		$drawObj->drawCapitalResults($myObj,$mheadings,$timeObj->getAllTime(),$myObj->getResults($obj_id));
	

		echo "<h2>Forecast Annual Budget</h2>
				<table id=tblfore>
					<tr>
						<td></td>
						<th>CRR</th>
						<th>Other</th>
					</tr>";
			foreach($lists['years'] as $i => $l) {
					echo "<tr>
							<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['cf_crr']) ? $forecast[$i]['cf_crr'] : 0),1)."&nbsp;</td>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['cf_other']) ? $forecast[$i]['cf_other'] : 0),1)."&nbsp;</td>
						</tr>";
			}			
		echo "</table>";
		break;
}
displayGoBack("","");


//if TOP LAYER then display associated KPI details
if($section=="TOP" || $section=="CAP") {
	$kpiObj = new DEPT();
	//$kpis = $section_object->getAssociatedKPIs($obj_id);
	//arrPrint($kpis);
	$kpis = $kpiObj->getAssociatesKPIs($obj_id,$kpiObj->getAssocID($section)); //arrPrint($kpis);
	if(count($kpis)>0) {
		echo "<h2>Associated Departmental KPI".(count($kpis)>1 ? "s" : "")."</h2>";
		$kpi_head = $headObj->getHeadings($kpiObj->getHeadingCodes()); //arrPrint($kpi_head);
		$head = array_merge(array('dir'=>$kpi_head['dir']),$kpi_head[$kpiObj->getHeadCode()]);
		$rhead = $kpi_head[$kpiObj->getRHeadCode()];
		$kpi_time = $timeObj->getAllTime();
		foreach($kpis as $k) {	
			$i = DEPT::REFTAG.$k;
			$kr = $kpiObj->getUpdateFormDetails($k);
			$kr['display_id'] = $i;
			//arrPrint($kr);
			$drawObj->drawLiteKPI($kpiObj,$head,$rhead,$kpi_time,$kr);
		}
	}
} elseif($section=="KPI") {
	if(strlen($object['kpi_topid'])>0 && $object['kpi_topid']!="N/A") {
		echo "<h2>Associated Top Layer KPI</h2>";
		$k = $object['okpi_topid'];
		$kpiObj = new TOP();
		$kpi_head = $headObj->getHeadings($kpiObj->getHeadingCodes()); //arrPrint($kpi_head);
		$head = $kpi_head[$kpiObj->getHeadCode()];
		$rhead = $kpi_head[$kpiObj->getRHeadCode()];
		$kpi_time = $timeObj->getTopTime();
		$i = TOP::REFTAG.$k;
		$kr = $kpiObj->getUpdateFormDetails($k);
		$kr['display_id'] = $i;
		$drawObj->drawLiteKPI($kpiObj,$head,$rhead,$kpi_time,$kr);
	}

	if(strlen($object['kpi_capitalid'])>0 && $object['kpi_capitalid']!="N/A") {
		echo "<h2>Associated Capital Project</h2>";
		$cap_id = $object['okpi_capitalid'];
		$capObj = new CAPITAL();
		$cap_head = $headObj->getHeadings($capObj->getHeadingCodes());
		$cap_time = $timeObj->getAllTime();
		$drawObj->drawLiteCapital($capObj,$cap_head,$cap_time,$object['okpi_capitalid']);
	}



}



?>