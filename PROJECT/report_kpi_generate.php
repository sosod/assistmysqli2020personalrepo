<?php 
/* REPORT GENERATOR */
displayResult($result);
?>
<form id=gen method=post action=<?php echo $self; ?>><input type=hidden name=act value=GENERATE /><input type=hidden name=page_id value=<?php echo $page_id; ?> />
<?php


//arrPrint($lists['subdir']);



/*********** 1. Fields *********/
$total_head = count($head)+2+count($mheadings[$r_section])-2+4;
if(isset($head[$table_fld."targettype"])) { $total_head-=1; }
if(isset($head[$table_fld."repkpi"])) { $total_head-=1; }
$third = ceil($total_head/3);
echo "<h2>1. Select the information to be displayed in the report:</h2>";
echo "<div style=\"margin-left: 17px\" id=fields>";
echo "<table>";
echo "	<tr><td style=\"padding: 5px 20px 5px 5px;\"><table>";
$c = 0;
	foreach($head as $fld => $h)
    {	
		if($fld!=$table_fld."targettype" && $fld!=$table_fld."repkpi") {
			$c++;
			echo "<tr>";
				echo "<td><input type=checkbox checked name=\"fields[]\" value=\"$fld\"></td>";
				echo "<td>".$h['h_client']."</td>";
			echo "</tr>";
			if($c==$third) {
				echo "</table></td><td style=\"padding: 5px 20px 5px 20px;\"><table>";
				$c=0;
			}
		}
    } 
			echo "<tr>";
				echo "<td colspan=2>Results from <select name=r_from>";
					foreach($time as $i => $t) {
						echo "<option value=$i ".($i==1 ? "selected" : "").">".$t['display_short']."</option>";
					}
				echo "</select> to <select name=r_to>";
					foreach($time as $i => $t) {
						echo "<option value=$i ".($i==$current_time_id ? "selected" : "").">".$t['display_short']."</option>";
					}
				echo "</select></td>";
			echo "</tr>";
			echo "<tr>
					<td class=top><input type=checkbox checked name=\"fields[]\" value=\"results\" /></td>
					<td>".$mheadings[$r_section][$r_table_fld."target"]['h_client'].",
					".$mheadings[$r_section][$r_table_fld."actual"]['h_client']." & Result
					<div style=\"margin-top: 5px; margin-left: 10px;font-style: italic\">For each time period display:<br />
						&nbsp;&nbsp;<input type=radio name=\"r_calc\" value=captured id=vcap checked /><label for=vcap> Values as captured</label><br />
						&nbsp;&nbsp;<input type=radio name=\"r_calc\" value=ytd id=vytd /><label for=vytd> Year-To-Date Values</label><br />
						&nbsp;&nbsp;<input type=radio name=\"r_calc\" value=ptd id=vptd /><label for=vptd> Period-To-Date Values</label></div>
					</td>
				</tr>";
			foreach($mheadings[$r_section] as $fld => $h) {
				if($fld != $r_table_fld."actual" && $fld != $r_table_fld."target") {
					echo "<tr>
							<td><input type=checkbox checked name=\"fields[]\" value=$fld /></td>
							<td>".$h['h_client']."</td>
						</tr>";
				}
			}
echo "	</table></td></tr>";
echo "</table>";
	//check/uncheck options
	echo "<p style=\"margin-top: 3px; font-size: 6.5pt; line-height: 7pt;\">";
		echo "<span id=checkall style=\"text-decoration: underline; cursor: pointer;\" class=color>Check All</span>";
		echo " | ";
		echo "<span id=uncheckall style=\"text-decoration: underline; cursor: pointer;\" class=color>Uncheck All</span>";
		echo " | ";
		echo "<span id=invert style=\"text-decoration: underline; cursor: pointer;\" class=color>Invert</span>";
	echo "</p>";
echo "<p>Include Summary of Results? <input type=checkbox value=Y id=sum name=summary checked /></p>";
echo "</div>";












/********** 2. FILTERS *********/
echo "<h2>2. Select the filter you wish to apply:</h2>";
echo "<div style=\"margin-left: 17px\" id=filters>";
echo "<table>";
    foreach($head as $fld => $h)
    {
		echo "<tr>
				<th>".$h['h_client'].":&nbsp;</th>";
			echo "<td>";
				switch($h['h_type']) {
					case "WARDS":
					case "AREA":
					case "TEXTLIST":
					case "LIST":
							$size = ($h['h_type']=="WARDS" || $h['h_type']=="AREA") ? 10 : 10;
							$plussize = in_array($fld,$absolute_required_fields) ? 1 : 2;
							if($h['h_table']=="subdir") {
								$list = $lists[$h['h_table']]['dir'];
							} else {
								$list = $lists[$h['h_table']];
							}
							if(count($list)>1) {
								echo "<select multiple name=filter[$fld][] size=".(count($list)>$size ? $size : count($list)+$plussize)." id=".$h['h_table'].">";
									echo "<option value=ANY selected>--- ANY LIST ITEM ---</option>";
									if(!in_array($fld,$absolute_required_fields)) { echo "<option value=0>".$unspecified."</option>"; }
									if($h['h_table']=="subdir") {
										foreach($lists['dir'] as $did => $d) {
											foreach($list[$did] as $i => $s) {
												$lv = $d['value']." - ".$s['value'];
												$val = strlen(decode($lv))>100 ? substr(decode($lv),0,97)."..." : decode($lv);
												echo "<option value=".$i.">".$val."</option>";								
											}
										}
									} else {
											foreach($list as $i => $l) {
												$val = strlen(decode($l['value']))>100 ? substr(decode($l['value']),0,97)."..." : decode($l['value']);
												if($h['h_type']=="WARDS") {
													echo "<option value=".$i.">".(is_numeric($l['code']) ? $l['code'].": " : "").$val."</option>";
												} else {
													echo "<option value=".$i.">".$val.( (isset($l['code']) && strlen($l['code'])>0 && !($h['h_table']=="riskrating" && $l['code']=="X") && !($h['h_table']=="calctype" || $h['h_table']=="targettype") ) ? " [".$l['code']."]" : "")."</option>";
												}
											}
									}
								echo "</select><br /><span class=\"ctrlclick iinform\">CTRL + Click to select multiple options</span> ";
							} else {
								foreach($list as $i => $l) { break; }
								echo "<input type=hidden name=filter[$fld][] value=\"$i\" />".$l['value'];
							}
						break;
					case "TOP":
						echo "<input type=radio name=filter[$fld][] value=ALL checked id=topa> <label for=topa>All KPIs</label><br />";
						echo "<input type=radio name=filter[$fld][] value=0 id=top0> <label for=top0>Not linked to Top Layer KPI</label><br />";
						echo "<input type=radio name=filter[$fld][] value=1 id=top1> <label for=top1>Linked to Top Layer KPI</label>";
						break;
					case "CAP":
						echo "<input type=radio name=filter[$fld][] value=ALL checked id=cpa> <label for=cpa>All KPIs</label><br />";
						echo "<input type=radio name=filter[$fld][] value=0 id=cp0> <label for=cp0>Operational KPIs only</label><br />";
						echo "<input type=radio name=filter[$fld][] value=1 id=cp1> <label for=cp1>KPIs associated with Capital Projects only</label>";
						break;
					case "BOOL":
						echo "<input type=radio name=filter[$fld] value=ALL checked id=repa> <label for=repa>All KPIs</label><br />";
						echo "<input type=radio name=filter[$fld] value=0 id=rep0> <label for=rep0>Reporting KPIs only</label><br />";
						echo "<input type=radio name=filter[$fld] value=1 id=rep1> <label for=rep1>Non-Reporting KPIs only</label>";
						break;
					case "VC":
					case "TEXT":
					default:
						echo "<input type=text size=30 name=filter[$fld][0] /> <select name=filter[$fld][1]><option value=ANY selected>Match any words</option><option value=ALL>Match all words</option><option value=EXACT>Match exact phrase</option></select>";
						break;
				}
			echo "</td>";
		echo "</tr>";
    }
	echo "<tr><th>KPI Result:</th>";
		echo "<td>";
                echo "<select name=filter[result]>";
					echo "<option selected value=ALL>All</option>";
					foreach($result_settings as $i => $l) {
						echo "<option value=$i>".$l['value']."</option>";
					}
                echo "</select>";
		echo "</td>";
	echo "</tr>";
if(isset($page_id) && in_array($page_id,array("deptchange","topchange","change"))) {
	echo "<tr><th>KPI Status:</th>
			<td>
				<select name=kpi_status>
					<option  value=DEL>Deleted</option>
					<option  value=ADD>Added (and not deleted)</option>
					<option  value=EDIT>Edited (and not deleted)</option>
					<option  value=ANY>All KPIs</option>
				</select>
			</td>
		</tr><tr>
			<th>Logs to Display:</th>
			<td>
				<select name=log_type>
					<option  value=ALL>All logs</option>
					<option  value=EDIT>Edits only</option>
					<option  value=UPDATE>Updates only</option>
				</select>
			</td>
		</tr>";
}
echo "</table>
</div>";









/*************** SORT BY *******************/
echo "<h2>3. Choose your group and sort options:</h2>";
echo "<div style=\"margin-left: 17px\" id=mysort>";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<th>Group by:&nbsp;</th>";
			echo "<td><select name=groupby><option select value=X>No grouping</option>";
			foreach($head as $fld => $h) {
				if((!in_array($fld,$nogroup) && !in_array($fld,$nosort)) || $fld=="dir" || $fld==$table_fld."subid") {
					echo "<option value=\"$fld\">".$h['h_client']."</option>";
				}
			}
			echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<th>Sort by:&nbsp;</th>";
			echo "<td>"; 
			echo "<div><ul id=sortable>";
			echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$table_fld."id\">KPI Ref</li>";
			if($section=="KPI") { 
				$fld = "dir"; $h['h_client'] = $head['dir']['h_client']." / ".$head[$table_fld."subid"]['h_client'];
					echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$fld."\">".$h['h_client']."</li>";
			}
			foreach($head as $fld => $h) {
				if(!in_array($fld,$nosort)) {
					echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$fld."\">".$h['h_client']."</li>";
				}
			}
			echo "</ul></div></td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";



/********** OUTPUT ***************/
if(!isset($output) || strlen($output)==0) { $output = "display"; }
echo "<h2>4. Choose the document format of your report:</h2>";
echo "<div style=\"margin-left: 17px\" id=output>";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<td><input type=radio name=\"output\" value=\"display\" ".($output=="display" ? "checked" : "")." id=display></td>";
			echo "<td id=displaytd class=clickme>Onscreen display</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td><input type=radio name=\"output\" value=\"csv\" "; if($output=="csv") { echo "checked"; } echo " id=csv></td>";
			echo "<td id=csvtd class=clickme>Microsoft Excel (Plain Text)</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td><input type=radio name=\"output\" value=\"excel\" "; if($output=="excel") { echo "checked"; } echo " id=excel></td>";
			echo "<td id=exceltd class=clickme>Microsoft Excel (Formatted)*</td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";



/************** BUTTONS ***************/
echo "<h2>5. Generate the report:</h2>";
	echo "<p style=\"margin-left: 17px\" id=generate>";
	echo "<b>Report Title:</b> <input type=text name=rhead value=\"\" maxlength=100 size=70> <i><small>(Displays at the top of the report.)</small></i><br />";
	echo "<input type=hidden name=act id=act value=GENERATE>";
	echo "<input type=submit value=\"Generate Report\">&nbsp;<input type=reset value=Reset>";
/*	echo "</p><p style=\"margin-left: 17px\">";
	echo "<b>Report Name:</b> <input type=text name=rname value=\"\" maxlength=50 size=30> <i><small>(To identify the report in the quick report list.)</small></i><br />";
if(isset($report['id']) && checkIntRef($report['id'])) {
		echo "<input type=button value=\"Save Changes\" id=update>&nbsp;";
		echo "<input type=button value=\"Save As New Report\" id=create>&nbsp;";
		echo "<input type=button value=\"Delete Report\" id=delete>&nbsp;";
} else {
	echo "<input type=button value=\"Save Report\" id=create>&nbsp;";
}*/
	echo "</p>";
?>
</form>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel. 
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
<script type=text/javascript>
$(document).ready(function() {
/* * FIELDS * */
	$("#fields tr").unbind('mouseenter mouseleave');
	$("#fields table, #fields td").css("border-color","#FFFFFF");
	$("#fields #checkall").click(function() {
		$("#fields input:checkbox").each(function() {
			if($(this).attr("id")!="sum") {
				$(this).prop("checked",true);
			}
		});
	});
	$("#fields #uncheckall").click(function() {
		$("#fields input:checkbox").each(function() {
			if($(this).attr("id")!="sum") {
				$(this).prop("checked",false);
			}
		});
	});
	$("#fields #invert").click(function() {
		$("#fields input:checkbox").each(function() {
			if($(this).attr("id")!="sum") {
				$(this).prop("checked",!$(this).prop("checked"));
			}
		});
	});
/* * FILTERS * */
	$("#filters th").css({"text-align":"left","vertical-align":"top"});
	$("#filters #dir").change(function() {
		
	});
/* * SORT * */
	$("#mysort th").css({"text-align":"left","vertical-align":"top"});
	$("#mysort tr").unbind('mouseenter mouseleave');
	$("#sortable").sortable({ placeholder: "ui-state-info" });
	$("#sortable").disableSelection();
/* * OUTPUT * */
	$("#output table, #output td").css("border-color","#FFFFFF");
	$("#output tr").unbind('mouseenter mouseleave');
	$("#output #displaytd").click(function() {	$("#output #display").attr("checked",true);	});
	$("#output #csvtd").click(function() {		$("#output #csv").attr("checked",true); 	});
	$("#output #exceltd").click(function() {	$("#output #excel").attr("checked",true);	});
	$("#output .clickme").css("cursor","hand");
/* * GENERATE * */
	$("#generate #create").click(function() {
		$("#generate #act").val("CREATE");
		$("#gen").submit();
	});
	$("#generate #delete").click(function() {
		$("#generate #act").val("DELETE");
		$("#gen").submit();
	});
	$("#generate #update").click(function() {
		$("#generate #act").val("UPDATE");
		$("#gen").submit();
	});
});
</script>