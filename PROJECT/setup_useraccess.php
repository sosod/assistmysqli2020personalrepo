<?php 
$log_section = "USER";
include("inc/header.php"); 
//phpinfo();

$user_sql = "SELECT * FROM ".$dbref."_user_access WHERE active = true";
$users = mysql_fetch_alls($user_sql,"tkid");


if(isset($_REQUEST['action'])) {
	$count = 0;
	$u_tkid = $_REQUEST['tkid'];
	$u_values = array();
	foreach($user_access_fields as $fld => $h) {
		$u_values[$fld] = $_REQUEST[$fld];
	}
if(count($u_tkid)>0) {
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as tkn FROM assist_".$cmpcode."_timekeep WHERE tkid IN (".implode(",",$u_tkid).")";
	$tknames = mysql_fetch_alls($sql,"tkid");
	switch($_REQUEST['action']) {
		case "SAVE":
			foreach($u_tkid as $u => $tk) {
				if(!isset($users[$tk])) {
					$sql = "INSERT INTO ".$dbref."_user_access (tkid,module,kpi,finance,toplayer,assurance,second,setup,active)
					VALUES ('$tk',".$u_values['module'][$u].",".$u_values['kpi'][$u].",".$u_values['finance'][$u].",0,0,".$u_values['second'][$u].",".$u_values['setup'][$u].",true)";
					$id=db_insert($sql);
					if(checkIntRef($id)) {
						$count++;
							$change = array();
							if($u_values['module'][$u]=="true") { $change[] = " - Module Admin = Yes"; } else { $change[] = " - Module Admin = No"; }
							if($u_values['kpi'][$u]=="true") { $change[] = " - KPI Admin = Yes"; } else { $change[] = " - KPI Admin = No"; }
							if($u_values['finance'][$u]=="true") { $change[] = " - Finance Admin = Yes"; } else { $change[] = " - Finance Admin = No"; }
							//if($u_values['toplayer'][$u]=="true") { $change[] = " - Top Layer Admin = Yes"; } else { $change[] = " - Top Layer Admin = No"; }
							//if($u_values['assurance'][$u]=="true") { $change[] = " - Assurance Provider = Yes"; } else { $change[] = " - Assurance Provider = No"; }
							if($u_values['second'][$u]=="true") { $change[] = " - Secondary Time Period = Yes"; } else { $change[] = " - Secondary Time Period = No"; }
							if($u_values['setup'][$u]=="true") { $change[] = " - Setup = Yes"; } else { $change[] = " - Setup = No"; }
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$tk,
											'field'=>"value",
											'text'=>"Added user access for <i>".code($tknames[$tk]['tkn'])."</i>:<br />".implode("<br />",$change),
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					}
					$users[$tk] = array('id'=>$id,'tkid'=>$tk,'active'=>true);
					foreach($user_access_fields as $fld => $h) { $users[$tk][$fld] = ($u_values[$fld][$u]=="true") ? 1 : 0; }
				}
			}
			if($count >0) {
				displayResult(array("ok","User Access saved"));
			}
			break;
			
		case "EDIT":
			foreach($u_tkid as $u => $tk) {
				$update = array();
				foreach($user_access_fields as $fld => $h) {
					$update[] = $fld." = ".$u_values[$fld][$u];
				}
				$sql = "UPDATE ".$dbref."_user_access SET ".implode(", ",$update)." WHERE active = true AND tkid = '$tk'";
				$count+=db_update($sql);
				$c = array();
				foreach($user_access_fields as $fld => $h) { 
					if( ($users[$tk][$fld] && $u_values[$fld][$u]!="true") || (!$users[$tk][$fld] && $u_values[$fld][$u]!="false") ) { $c[] = $fld; }
					$users[$tk][$fld] = ($u_values[$fld][$u]=="true") ? 1 : 0; 
				}
			}
			if($count >0) {
						if(in_array("module",$c)) {	if($u_values['module'][$u]=="true") { $change[] = " - Module Admin = Yes"; } else { $change[] = " - Module Admin = No"; } }
						if(in_array("kpi",$c)) {	if($u_values['kpi'][$u]=="true") { $change[] = " - KPI Admin = Yes"; } else { $change[] = " - KPI Admin = No"; }	}
						if(in_array("finance",$c)) {	if($u_values['finance'][$u]=="true") { $change[] = " - Finance Admin = Yes"; } else { $change[] = " - Finance Admin = No"; }	}
						//if(in_array("toplayer",$c)) {	if($u_values['toplayer'][$u]=="true") { $change[] = " - Top Layer Admin = Yes"; } else { $change[] = " - Top Layer Admin = No"; }	}
						//if(in_array("assurance",$c)) {	if($u_values['assurance'][$u]=="true") { $change[] = " - Assurance Provider = Yes"; } else { $change[] = " - Assurance Provider = No"; }	}
						if(in_array("second",$c)) {	if($u_values['second'][$u]=="true") { $change[] = " - Secondary Time Period = Yes"; } else { $change[] = " - Secondary Time Period = No"; }	}
						if(in_array("setup",$c)) {	if($u_values['setup'][$u]=="true") { $change[] = " - Setup = Yes"; } else { $change[] = " - Setup = No"; }	}
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$tk,
											'field'=>"value",
											'text'=>"Edited user access for <i>".code($tknames[$tk]['tkn'])."</i>:<br />".((count($change)>0) ? implode("<br />",$change) : " - No change made."),
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
				displayResult(array("ok","User Access updated"));
			}
			break;
	}
}//count u_tkid
}

//arrPrint($users);

echo "<form method=post action=$self><input type=hidden name=action value=SAVE />";

echo "<table class=noborder><tr><td class=noborder style=\"background-color: #FFFFFF;\">";
//drawPaging($self,'',0,15,1,1,"User",$self,'100%')
?>
<table id=user_list>
	<tr>
		<th>ID</th>
		<th>User</th>
		<?php
		foreach($user_access_fields as $fld => $h) {
			echo "<th>$h</th>";
		}
		?>
		<th>&nbsp;</th>
	</tr>
<?php 
$sql = "SELECT DISTINCT tkid, CONCAT_WS(' ',tkname, tksurname) as name FROM assist_".$cmpcode."_timekeep INNER JOIN assist_".$cmpcode."_menu_modules_users ON usrtkid = tkid AND usrmodref = '".strtoupper($_SESSION['modref'])."' WHERE tkstatus = 1 AND tkid <> '0000' ORDER BY tkname, tksurname";
$rs = getRS($sql);
$width=90;
while($row = mysql_fetch_assoc($rs)) {
	echo "<tr>
		<th>".$row['tkid']."</th>
		<td>".$row['name']."<input type=hidden name=x[] id=tkn_".$row['tkid']." value=\"".decode($row['name'])."\" /></td>";
		foreach($user_access_fields as $fld => $h) {
			echo "<td class=centre width=$width >";
			if(isset($users[$row['tkid']][$fld])) {
				echo "<input type=hidden value=\"".(($users[$row['tkid']][$fld]) ? "true" : "false")."\" name=x[] id=".$fld."_".$row['tkid']." />".((($users[$row['tkid']][$fld]) || $users[$row['tkid']][$fld]==1) ? $me->drawStatus(true) : $me->drawStatus(false));
				//echo $users[$row['tkid']][$fld];
			} else {
				echo "<select name=".$fld."[] id=".$fld."_".$row['tkid']." class=$fld><option selected value=false>No</option><option value=true>Yes</option></select>";
			}
			echo "</td>";
		}
		echo "<td class=centre>";
			if(isset($users[$row['tkid']])) 
				echo "<input type=button value=Edit id='".$row['tkid']."' class=edit />";
			else
				echo "<input type=hidden name=tkid[] value='".$row['tkid']."' /><input type=button value=Save class=save1 id='".$row['tkid']."' />";
		echo "</td>";
	echo "</tr>";
}
?>
	<tr id=save_row>
		<th>&nbsp;</th>
		<td colspan=9 class=blank>
			<table class="noborder blank" width=100%><tr><td class="left noborder blank">
				<input type=submit class=isubmit value="Save All" id=save_all />
			</td><td class="right noborder blank">
				<input type=reset  />
			</td></tr></table>
		</td>
	</tr>
</table>

</td></tr></table>
</form>

<form id=save_one method=post action=<?php echo $self; ?> >
<input type=hidden name=action value=SAVE />
<input type=hidden name=tkid[] id=tkid value="" />
<?php
foreach($user_access_fields as $fld => $h) {
	echo "<input type=hidden value=\"\" name=".$fld."[] id=".$fld." />";
}
?>
</form>
<script type=text/javascript>
$(function() {

if($("table #user_list select").length==0) {
	$("table tr #save_row").hide();
}

	$(".save1").click(function() {
		var tkid = $(this).prop("id");
		$("#tkid").val(tkid);
		var t = "";
		var v = "";
		<?php
		foreach($user_access_fields as $fld => $h) {
			echo chr(10)."t = \"#".$fld."_\"+tkid;";
			echo chr(10)."v = $(t).val();";
			echo chr(10)."$(\"#".$fld."\").val(v);";
		}
		echo chr(10);
		?>
		$("#save_one").submit();
	});
	$(".module").change(function() {
		var i = $(this).prop("id");
		var u = i.indexOf("_");
		var s = i.substring(u+1,i.length);
		var t = "";
		if($(this).val()=="true") {
		<?php
		foreach($user_access_fields as $fld => $h) {
			if($fld !="module") {
				echo chr(10)."t = \"#".$fld."_\"+s;";
				echo chr(10)."$(t).val(\"true\");";
			}
		}
		?>
		}
	});
});
</script>


<?php /***** EDIT DIALOG *****/ ?>
<div id=dialog_edit title="Edit User Access">
	<form id=edit action=<?php echo $self; ?> method=post><input type=hidden name=action value=EDIT />
		<table width=450>
			<tr>
				<th class=left width=150>ID:</th>
				<td width=300><input type=hidden id=tkid_edit name=tkid[] /><label id=tkid_e for=tkid>0000</label></td>
			</tr>
			<tr>
				<th class=left>User:</th>
				<td><label id=tkn_e for=tkid>abc</label></td>
			</tr>
		<?php foreach($user_access_fields as $fld => $h) { ?>
			<tr>
				<th class=left><?php echo str_replace("<br />"," ",$h); ?>:</th>
				<td><?php echo "<select name=".$fld."[] id=".$fld."_edit>"; ?><option selected value=false>No</option><option value=true>Yes</option></select></td>
			</tr>
		<?php } ?>
			<tr>
				<td colspan=2><input type=button value="Save Changes" id=save_edit name=but class=isubmit /></td>
			</tr>
		</table>
		<p><input type=button value=Cancel id=cancel /></p>
	</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dialog_edit").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$("#cancel").click(function() { $("#dialog_edit").dialog("close"); });
	$("#delete").click(function() {
		if(confirm("Are you sure you wish to deactivate this list item?")==true) {
			var id = $("#i").val();
		}
	});
	$(".edit:button").click(function() {
		var tkid = $(this).prop("id");
//		$("#tkid").attr("value",tkid);
		$("#tkid_edit").val(tkid);
		$("#tkid_e").html(tkid);
		var t = "";
		t = "#tkn_"+tkid;
		var v = "";
		v = $(t).val();
		$("#tkn_e").html(v);
		<?php
		foreach($user_access_fields as $fld => $h) {
			echo chr(10)."t = \"#".$fld."_\"+tkid;";
			echo chr(10)."v = $(t).val();";
			echo chr(10)."$(\"#".$fld."_edit\").val(v);";
		}
		echo chr(10);
		?>
		$("#dialog_edit").dialog("open");
	});
	$("#module_edit").change(function() {
		var v = $(this).val();
		if(v=="true") {
			var t = "";
			<?php
			foreach($user_access_fields as $fld => $h) {
				echo chr(10)."t = \"#".$fld."_edit\";";
				echo chr(10)."$(t).val(\"true\");";
			}
			?>
		}
	});
	$("#save_edit").click(function() {
		$("#edit").submit();
	});
});
</script>
<?php
	$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
	displayLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));

?>
</body>
</html>