<?php 
$section = "KPI";
$get_lists = true;
$get_active_lists = false;
$locked_column = true;
$get_open_time = true;
include("inc/header.php"); 

if($my_access['access']['second'] || ($my_access['access']['toplayer'] && $section=="TOP")) { $active_fld = "secondary"; } else { $active_fld = "primary"; }
$time = getOpenTime($time,$active_fld);
$time2 = getTime($get_time,false);
//arrPrint($mheadings);
//arrPrint($lists['owner']);

if(count($time)==0 && $page_id !="update") { $time = getTime(array(12),false); }

if(!$import_status[$section] && $page_id != "create") {
	die("<p>Departmental SDBIP has not yet been created.  Please try again later.</p>");
} elseif($page_id=="pms") {
	include("manage_pms.php");
} elseif($page_id=="create") {
	include("manage_dept_create.php");
} else {
//arrPrint($time);
//$_SESSION[$self."_".$page_id] = array();
//Filter settings

	$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
				INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT k.kpi_subid FROM ".$dbref."_kpi k WHERE kpi_active = true ) 
				WHERE d.active = true
				AND (
					d.id IN (SELECT ref FROM ".$dbref."_user_admins WHERE type = 'DIR' AND active = true AND tkid = '$tkid' AND act_".$page_id." = true) 
				OR 
					s.id IN (SELECT ref FROM ".$dbref."_user_admins WHERE type = 'SUB' AND active = true AND tkid = '$tkid' AND act_".$page_id." = true)
				)";
				//echo $dir_sql;
	$valid_dir_sub = mysql_fetch_alls2($dir_sql,"dir","subdir");
	//arrPrint($valid_dir_sub);
//arrPrint($my_access['manage']);


if(count($time)>0 && (count($valid_dir_sub)>0 || (isset($my_access['manage']['OWN']) && count($my_access['manage']['OWN'])>0 && $page_id == "update"))) {
//echo "filter";


//if(!isset($_SESSION[$self."_".$page_id] = array();
$filter = isset($_SESSION[$self."_".$page_id]) ? $_SESSION[$self."_".$page_id] : array();
	//WHEN
	//echo "when";
	if(isset($_REQUEST['filter_when'])) {
		$filter_when = $_REQUEST['filter_when'];
		$filter['when'] = $filter_when;
		//echo "request";
	} elseif(!isset($filter['when'])) {
		//foreach($time as $ti => $t) {
			//$filter_when = $ti; 
			//if($page_id=="update" || $page_id=="deptupdate") { break; }
		//}
		$filter_when = $current_time_id;
		//echo "filterwhen";
		$filter['when'] = $filter_when;
	} //arrPrint($filter);
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
		$filter['who'] = $filter_who;
	} elseif(!isset($filter['who'])) {
		$filter_who = array("","All");
		$filter['who'] = $filter_who;
	}
	//WHAT
	if(isset($_REQUEST['filter_what'])) {
		$filter['what'] = $_REQUEST['filter_what'];
	} elseif(!isset($filter['what'])) {
		$filter['what'] = "All";
	}
	//DISPLAY
	if(isset($_REQUEST['filter_display'])) {
		$filter['display'] = $_REQUEST['filter_display'];
	} elseif(!isset($filter['display'])) {
		$filter['display'] = "LIMIT";
	}

	



	/*$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
		INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT k.kpi_subid FROM ".$dbref."_kpi k
		INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE kpi_active = true ) WHERE d.active = true";
	$valid_dir_sub = mysql_fetch_alls2($dir_sql,"dir","subdir");*/

	
	
	//drawViewFilter($who,$what,$when,$sel,$filter)
	$head = $time[$filter['when']]['display_full'];
	$filter['who'] = drawManageFilter(true,true,true,true,$filter,$head);

//arrPrint($filter);


	//SQL

	$object_sql1 = " ".$dbref."_kpi k
		INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true
		".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
		INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
		WHERE kpi_active = true ".($filter['who'][0]=="S" && checkIntRef($filter['who'][1]) ? " AND kpi_subid = ".$filter['who'][1] : "")
		.($filter['who'][0]=="O" && checkIntRef($filter['who'][1]) ? " AND kpi_ownerid = ".$filter['who'][1] : "");
		if(isset($filter['what']) && strtolower($filter['what'])!="all") {
			switch($filter['what']) {
				case "op":	$object_sql1.= " AND kpi_capitalid = 0 AND kpi_topid = 0"; break;
				case "cap":	$object_sql1.= " AND kpi_capitalid > 0"; break;
				case "top":	$object_sql1.= " AND kpi_topid > 0"; break;
			}
		}

function getResults() {	
	global $dbref;
	global $filter;
	global $object_sql1; 
	if(count($filter['when'])>0) {
		$results_sql = "SELECT r.* FROM ".$dbref."_kpi_results r
		INNER JOIN ".$dbref."_kpi k ON r.kr_kpiid = k.kpi_id AND k.kpi_active = true
		WHERE kr_timeid IN (".($filter['when']-1).",".$filter['when'].",".($filter['when']+1).") AND kr_kpiid IN (SELECT k.kpi_id FROM ".$object_sql1.")";
	
		return mysql_fetch_alls2($results_sql,"kr_kpiid","kr_timeid");
	} else {
		return array();
	}
}
	$results = getResults();
	$fld_target = "kr_target";
	$fld_actual = "kr_actual";
	
	if(isset($mheadings['KPI']['kpi_topid'])) {
		$top_sql = "SELECT t.top_id, t.top_value FROM ".$dbref."_top t
			WHERE t.top_id IN (SELECT k.kpi_topid FROM ".$object_sql1.") AND t.top_active = true";
		$tops = mysql_fetch_alls($top_sql,"top_id");
	}
	if(isset($mheadings['KPI']['kpi_capitalid'])) {
		$cap_sql = "SELECT c.cap_id, c.cap_name FROM ".$dbref."_capital c
			WHERE c.cap_id IN (SELECT k.kpi_capitalid FROM ".$object_sql1.") AND c.cap_active = true";
		$caps = mysql_fetch_alls($cap_sql,"cap_id");
	}
	if(isset($mheadings['KPI']['kpi_wards'])) {
		$wards_sql = "SELECT kw_kpiid, id, value, code FROM ".$dbref."_kpi_wards INNER JOIN ".$dbref."_list_wards ON kw_listid = id 
			WHERE kw_active = true AND kw_kpiid IN (SELECT k.kpi_id FROM ".$object_sql1.")";
		$wa['kpi_wards'] = mysql_fetch_alls2($wards_sql,"kw_kpiid","id");
	}
	if(isset($mheadings['KPI']['kpi_area'])) {
		$area_sql = "SELECT ka_kpiid, id, value, code FROM ".$dbref."_kpi_area INNER JOIN ".$dbref."_list_area ON ka_listid = id 
			WHERE ka_active = true AND ka_kpiid IN (SELECT k.kpi_id FROM ".$object_sql1.")";
		$wa['kpi_area'] = mysql_fetch_alls2($area_sql,"ka_kpiid","id");
	}

	$object_sql = "SELECT k.* FROM ".$object_sql1;
	
	/*if(isset($_REQUEST['r'])) {
		$result = $_REQUEST['r'];
		displayResult($result);
	}*/
	$_SESSION[$self."_".$page_id] = $filter;
	switch($page_id) {
		case "update":	include("manage_table_update.php");	break;
		case "edit":	include("manage_table_edit.php");	break;
	}
		
} elseif(!(count($valid_dir_sub)>0 || (isset($my_access['manage']['OWN']) && count($my_access['manage']['OWN'])>0 && $page_id == "update"))) {
	echo "<P>You do not have access to ".$page_id." any Departmental KPIs</p>";
} else {
	echo "<P>There are no open time periods to update</p>";
}
	
	
}	//if import_status['import_Section'] == false else	

//phpinfo();
//arrPrint($_SESSION);
?>
</body>
</html>