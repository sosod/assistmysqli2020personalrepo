<?php

if(!isset($time2)) { $time2 = getTime($get_time,false); }

$my_head = $mheadings[$object_head];
$result_head = $mheadings[$r_section];
$update_head = $result_head;
if(isset($update_head['kr_target'])) { unset($update_head['kr_target']); }
if(isset($update_head['tr_target'])) { unset($update_head['tr_target']); }
if(isset($update_head['tr_dept'])) { 
	unset($result_head['tr_dept']); 
	unset($update_head['tr_dept']); 
}
if(isset($update_head['tr_dept_correct'])) { 
	unset($result_head['tr_dept_correct']); 
	unset($update_head['tr_dept_correct']); 
}
if(isset($update_head['tr_dept_attachment'])) { 
	unset($result_head['tr_dept_attachment']); 
	unset($update_head['tr_dept_attachment']); 
}

$performance_fields = array("tr_perf","kr_perf");

$wide_keys = array_flip($wide_headings);

//arrPrint($wide_keys);

unset($wide_headings[$wide_keys['kpi_topid']]); unset($wide_headings[$wide_keys['kpi_capitalid']]);

if(!isset($mheadings['dir']) && isset($mheadings[$section][$table_fld."dirid"])) {
	$mheadings['dir'][0] = $mheadings[$section][$table_fld."dirid"];
}
//arrPrint($mheadings);

/*************************
****** SAVING UPDATE *****
*************************/
//arrPrint($_REQUEST);



$flds = array();
echo "<form id=save_object method=post action=$self ><input type=hidden name=obj_id[] id=obj_id value=\"\" />";
	drawManageFormFilter(true,true,true,true,$filter,$page_id);
	foreach($update_head as $fld => $h) { $flds[] = $fld;  echo "<input type=hidden name=".$fld."[] id=$fld value=\"\" />"; }
echo "</form>";

if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if($section=="KPI" || $section == "TOP") {
	$colspan = count($result_head)+($section=="TOP" ? 1 : 0);
} elseif($section=="CF" || $section=="CAP") {
	if($filter['display']!="LIMIT") {
		$colspan = count($mheadings[$rheadings]);
	} else {
		$colspan = 0;
		foreach($mheadings[$rheadings] as $h) {
			if($h['c_list']) 
				$colspan++;
		}
	}
} else {
	$colspan = 2;
}

echo "<form method=post action=$self id=save_all >";
drawManageFormFilter(true,true,true,true,$filter,$page_id);
?>


<table id=tbl_update>
<thead>
<?php
$total_columns = 0;
	$total = array();
	$total['all']['target'] = 0;
	$total['all']['actual'] = 0;
	echo "<tr>
		<th rowspan=2>Ref</th>";
		foreach($mheadings['FIXED'] as $key => $row) {
			$total_columns++;
			echo "<th rowspan=2 class=fixed>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
		}
		foreach($my_head as $key => $row) {
			if(($filter['display']!= "LIMIT" || $row['c_list']) && !$row['fixed']) {
				$total_columns++;
				echo "<th rowspan=2>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
			}
		}
		$tc = 0;
		$t_css = 2;
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $ti => $row) {
				$tc++;
				echo "<th colspan=$colspan class=\"time".$t_css."\">".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
			}
		} else {
			$ti = $filter['when'];
			$t = $time[$ti];
					$tc++;
			$ti_past = $ti - $time_increment;
			$ti_next = $ti + $time_increment;
			if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
					$col_past = 0;
					if($setup_defaults[$section."_past_r"]=="Y") { $col_past+= 3; }
					if($setup_defaults[$section."_past_c"]=="Y") { $col_past+= $section=="TOP" ? 4 : 2; }
					echo "<th colspan=".$col_past." class=\" time".($t_css+1)."\">".$time2[$ti_past]['display_full']."</th>";
			}
					echo "<th colspan=".($colspan)." class=\" time".$t_css."\">".$t['display_full']."</th>";
					$total[$ti]['target'] = 0;
					$total[$ti]['actual'] = 0;
			if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
					$col_next = 0;
					if($setup_defaults[$section."_next_r"]=="Y") { $col_next+= 1; }
					echo "<th colspan=".$col_next." class=\" time".($t_css-1)."\">".$time2[$ti_next]['display_full']."</th>";
			}
					$t_css = ($t_css>=4) ? 1 : $t_css+1;
		}
	echo "	</tr>";
	echo "	<tr>";
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $h => $cf_h) {
				foreach($mheadings['CF_R'] as $r => $cf_r) {
					if($filter['display']!="LIMIT" || $cf_r['c_list']) {
						echo "<th class=\"time".$t_css."\">".(strlen($cf_r['h_client'])>0 ? $cf_r['h_client'] : $cf_r['h_ignite'])."</th>";
						$total[$h][$r] = 0;
					}
				}
				$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			$t_css = 2;
				switch($section) {
					case "TOP":
					case "KPI":
						if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
								foreach($result_head as $fld => $h) {
									if( ($setup_defaults[$section."_past_r"]=="Y" && ($fld==$fld_target || $fld==$fld_actual)) || ($setup_defaults[$section."_past_c"]=="Y" && $fld!=$fld_target && $fld!=$fld_actual ) ) {
										echo "<th class=\"time".($t_css+1)."\" >".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'])."</th>";
										if($setup_defaults[$section."_past_r"]=="Y" && $fld==$fld_actual) {
											echo "<th class=\"time".($t_css+1)."\" >R</th>";
										}
									}
								}
						}
						foreach($result_head as $fld => $h) {
							/*if($fld==$fld_target) {
								echo "<th class=\"time".$t_css."\" >&nbsp;</th>";
							}*/
							echo "<th class=\"time".$t_css."\" >".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).((in_array($fld,$performance_fields) && $setup_defaults['perfcomm']=="Y") ? "<br /><span class=i style=\"font-size:6.5pt;\">(Required)</span>" : "")."</th>";
						}
						if($section=="TOP") {
							echo "<th class=\"time".$t_css."\" >Auto Update</th>";
						}
						if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
								echo "<th class=\" time".($t_css-1)."\">".$result_head[$fld_target]['h_client']."</th>";
						}
						break;
					/*case "CAP":
						foreach($mheadings[$rheadings] as $r => $cr) {
							if($filter['display']!="LIMIT" || $cr['c_list']) {
								echo "<th class=\"time".$t_css."\">".(strlen($cr['h_client'])>0 ? $cr['h_client'] : $cr['h_ignite'])."</th>";
							}
						}
						break;
					default:
						echo "<th  class=\"time".$t_css."\" >Budget</th>
							<th  class=\"time".$t_css."\" >Actual</th>";*/
				}
		}
	echo "	</tr>";
?>
</thead>


<tbody>

<?php
$rc = 0;
	$object_rs = getRS($object_sql);
	while($object = mysql_fetch_assoc($object_rs)) { //arrPrint($object);
		$obj_id = $object[$table_fld.'id'];
		echo "<tr title='Click to update this KPI' id=".$obj_id." update=1>
			<th id=ref_".$obj_id.">".(isset($id_label)?$id_label:"").$obj_id."</th>";
				foreach($mheadings['FIXED'] as $key => $h) {
					echo "<td class=fixed id=".$key."_".$obj_id.">";
					if($h['h_type']=="LIST") {
						if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
							echo "<div class=centre>".$lists[$h['h_table']][$object[$key]]['code']."</div>";
						} else {
							echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
						}
					} else {
						echo displayValue($object[$key]);
					}
					echo "</td>";
				}
				foreach($my_head as $key => $h) {
					if(($filter['display']!="LIMIT" || $h['c_list']) && !$h['fixed']) {
						echo "<td>";
						if($h['h_type']=="LIST") {
							if(in_array($h['h_table'],$code_lists) && isset($lists[$h['h_table']][$object[$key]]) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
								echo "<div class=centre title=\"".$lists[$h['h_table']][$object[$key]]['value']."\" style=\"text-decoration: underline\">".$lists[$h['h_table']][$object[$key]]['code']."</div>";
							} elseif(isset($lists[$h['h_table']][$object[$key]])) {
								echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
							} else {
								echo $unspecified;
							}
							if(in_array($key,array("kpi_calctype","top_calctype"))) {
								echo "<input type=hidden value=".$object[$key]." name=abc[] id=ct_".$obj_id." />";
							}
						} elseif($h['h_type']=="TEXTLIST") {
							$x = $object[$key];
							$a = array();
							if(strlen($x)>0 && $x != "0") {
								$y = explode(";",$x);
								foreach($y as $z) {
									if(isset($lists[$h['h_table']][$z]['value'])) {
										$a[] = $lists[$h['h_table']][$z]['value'];
									}
								}
							}
							if(count($a)>0) {
								$v = implode("; ",$a);
							} else {
								$v = $unspecified;
							}
							echo displayValue($v);
						} else {
							switch($key) {
								case $table_fld."area":
								case $table_fld."wards":
								case "capital_area":
								case "capital_wards":
								case "capital_fundsource":
									if(isset($wa) && isset($wa[$key]) && isset($wa[$key][$obj_id])) {
										//echo $key." :: "; arrPrint($wa[$key][$obj_id]);
										foreach($wa[$key][$obj_id] as $v) {
											if(isset($v['code']) && strlen($v['code'])) {
												echo $v['code'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											} else {
												echo $v['value'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											}
										}
									}
									break;
								case $table_fld."topid":
									if(isset($tops) && isset($tops[$object[$key]])) {
										//echo displayValue($tops[$object[$key]]['top_value']." [".$id_labels_all['TOP'].$object[$key]."]");
										echo $id_labels_all['TOP'].$object[$key];
									}
									break;
								case $table_fld."capitalid":
									if(isset($caps) && isset($caps[$object[$key]])) {
										//echo displayValue($caps[$object[$key]]['cap_name']." [".$id_labels_all['CAP'].$object[$key]."]");
										echo $id_labels_all['CAP'].$object[$key];
									}
									break;
								case "top_annual":
								case "top_revised":
									echo "<div class=right>".KPIresultDisplay($object[$key],$object[$table_fld.'targettype'])."</div>";
									break;
								case "cap_planstart":
								case "cap_planend":
								case "cap_actualstart":
								case "cap_actualend":
									if(strlen($object[$key])>1) {
										echo date("d M Y",$object[$key]);
									}
									break;
								default:
									echo displayValue($object[$key]);
									break;
							}
						}
						echo "</td>";
					}
				}
				$result_object = $results[$obj_id];
				$t_css = 2;
				switch($section) {
					case "KPI":
					case "TOP":
						$obj_tt = $object[$table_fld.'targettype'];
						$obj_ct = $object[$table_fld.'calctype'];
						$values = array();
						//DISPLAY PAST MONTH
						if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
							$values_past = $result_object[$ti_past];
							$v_past = array(
								'target'=>array($ti_past=>$values_past[$fld_target]),
								'actual'=>array($ti_past=>$values_past[$fld_actual]),
							);
							if($setup_defaults[$section."_past_r"]=="Y") {
								$r_past = KPIcalcResult($v_past,$obj_ct,array(),$ti_past);
							}
								foreach($result_head as $fld => $h) {
									if( ($setup_defaults[$section."_past_r"]=="Y" && ($fld==$fld_target || $fld==$fld_actual)) || ($setup_defaults[$section."_past_c"]=="Y" && $fld!=$fld_target && $fld!=$fld_actual ) ) {
										echo "<td class=\"right time".($t_css+1)."\" >";
										if($fld=="tr_dept" || $fld == "tr_dept_correct") {
											echo "<div class=left>".displayTRDept($values_past[$fld],"HTML")."</div>";
										} else {
											switch($h['h_type']) {
												case "TEXT": 
													echo "<div class=left>".decode($values_past[$fld])."</div>"; break;
												case "NUM":
												default:	echo KPIresultDisplay($values_past[$fld],$obj_tt); break;
											}
										}
										if($setup_defaults[$section."_past_r"]=="Y" && $fld == $fld_actual) {
											echo "</td><td class=\"".$r_past['style']."\" id=".$obj_id."_r>".$r_past['text'];
										}
										echo "</td>";
									}
								}
						}
						//DISPLAY CURRENT MONTH
						$update_class = $obj_id."updateclass";
						//echo "<td class=\"center time".$t_css." ".$update_class."\">".$result_object[$ti]['kr_update'].ASSIST_HELPER::drawStatus($result_object[$ti]['kr_update'])."</td>";
						foreach($result_head as $fld => $h) {
							$values[$fld] = $result_object[$ti][$fld];
							echo "
							<td class=\"right time".$t_css." ".$update_class."\" id=".$obj_id."_".$fld.">";
							if($fld=="kr_target" || $fld == "tr_target") {
								echo KPIresultDisplay($values[$fld],$obj_tt);//."<input type=hidden value=\"".$values[$fld]."\" name=abc[] id=tar_".$obj_id." />";
							} elseif($fld=="tr_dept" || $fld=="tr_dept_correct") {
								echo "<div class=left>".displayTRDept($values[$fld],"HTML")."</div>";
							} else {
								switch($h['h_type']) {
									case "TEXT": 
										if(in_array($fld,array('tr_perf','kr_perf'))) { 
											$class = "valid8me perf"; 
										} elseif(in_array($fld,array('kr_correct'))) { 
											$class = "valid8me correct"; 
										} else { 
											$class = ""; 
										}
										if(in_array($fld,array('tr_attachment','kr_attachment'))) {
											$x = unserialize($values[$fld]);
											$poe = array();
											if(isset($x['poe']) && strlen($x['poe'])>0) { $poe[] = $x['poe']; }
											$a = isset($x['attach']) ? $x['attach'] : array();
											if(count($a)>0) { 
												$attach = array();
												foreach($a as $b) {
													$attach[] = $b['original_filename'];
												}
												$poe[] = "".implode("<br />",$attach); 
											}
											$v = implode("<br />",$poe);
										} else {
											$v = $values[$fld];
										}
										echo decode($v); 
										break;
									case "NUM":
										echo KPIresultDisplay($values[$fld],$obj_tt); break;
									default:	break;//echo ($obj_tt==1?"R":"")."".$values[$fld]."".($obj_tt==2?"%":"")."<input type=hidden size=5 value=\"".$values[$fld]."\" name=old_".$fld."[] class=\"\" id=old_".$fld."_".$obj_id." />"; break;
								}
							}
							if($fld==$fld_actual) {
								$ds = "N";
								if($result_object[$ti][$r_table_fld.'update']) { 
									$ds = "Y";
								} elseif($result_object[$ti][$r_table_fld.'target']==0) {
									$ds = "info";
								}
								echo ASSIST_HELPER::drawStatus($ds);
							}
							echo "</td>";
						}
						if($section=="TOP") {
							echo "<td class=\"center time".$t_css."\">";
							if($object['assoc']>0) {
								$assoc_ct = explode("|",$object['assoc_calctype']);
								$assoc_tt = explode("|",$object['assoc_targettype']);
								if(count($assoc_ct)>1 || !in_array($obj_ct,$assoc_ct) || count($assoc_tt)>1 || !in_array($obj_tt,$assoc_tt)) {
									echo $me->drawStatus("warn");
								} else {
									switch($result_object[$ti]['tr_auto']) {
										case 1:
											echo $me->drawStatus(true);
											break;
										case 0:
											echo $me->drawStatus("lock");
											break;
									}
								}
							} else {
								echo $me->drawStatus(false);
							}
							echo "</td>";
						}
						//DISPLAY NEXT MONTH
						if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
							$values_next = $result_object[$ti_next][$fld_target];
								echo "<td class=\"right time".($t_css-1)."\">".KPIresultDisplay($values_next,$obj_tt)."</td>";
						}
						break;
					case "CF":
						$result_object = $results[$obj_id][$filter['when'][0]];
						$t_css = 3;
						foreach($mheadings['CF_H'] as $rs => $cf_h) {
							$cfh_values = array();
							$budget = 0;
							foreach($mheadings['CF_R'] as $res => $cf_r) {
								$fld = "cr_".$cf_h['field'].$cf_r['field'];
								$cfh_values[$fld] = $result_object[$fld];
								switch($res) {
									case "_1": case "_2": case "_3":	$budget+=$cfh_values[$fld];				break;	//o. budget, adj. est., vire
									case "_4":							$cfh_values[$fld] = $budget;			break;	//adj. budget
									case "_5":							$actual = $cfh_values[$fld];			break;	//actual
									case "_6":							$cfh_values[$fld] = $actual - $budget;	break;	//variance
								}
								//if($filter['display']!="LIMIT" || $cf_r['c_list']) {
									$val = $cfh_values[$fld];
									$total[$rs][$res]+= $val;
									echo "<td class=\"right time".$t_css."\">";
										switch($cf_r['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
									echo "</td>";
								//}
							}
							$t_css = ($t_css<=1) ? 3 : $t_css-1;
						}
						break;
					case "CAP":
						$values = array();
						/*foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {*/
								foreach($mheadings[$rheadings] as $r => $cr) {
									if(!isset($total[$ti][$r])) { $total[$ti][$r] = 0; }
									if(!isset($values[$r])) { $values[$r] = 0; }
									$val = $result_object[$ti][$r];
									$total[$ti][$r]+=$val;
									$values[$r]+=$val;
									//if($filter['display']!="LIMIT" || $cr['c_list']) {
										echo "<td class=\"right time".$t_css."\">";
										switch($cr['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
										echo "</th>";
									//}
								}
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
						break;
					default:
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								$total[$ti]['target']+=$values['target'][$ti];
								$total[$ti]['actual']+=$values['actual'][$ti];
								echo "<td class=\"right time".$t_css."\">";
									echo "<input type=text value=\"".$values['target'][$ti]."\" />";
								echo "</td><td class=\"right time".$t_css."\">";
									echo number_format($values['actual'][$ti],2);
								echo "</td>";
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
						break;
				}
			/*echo "<td class=centre><input type=hidden name=obj_id[] value=".$obj_id." class=objid />
						<input type=button value=Save id=".$obj_id." class=act1 />";
			if($section=="TOP" && !$result_object[$ti]['tr_auto']) {
				echo " <input type=button value=Unlock id=".$obj_id." class=act1 />";
			} elseif($section=="KPI" && $obj_tt==2 && $obj_ct=="CO" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") {
				echo " <input type=button value=PLC id=".$obj_id." class=plc />";
			}
			echo "</td>";*/
		echo "</tr>";
	}
	?>
	</tbody>
</table>

</form>
<div id=div_update title="">
	<div id=div_auto_update>
		<?php $me->displayResult(array("error","Warning!  Updating this KPI will prevent any future automatic updates from the Departmental SDBIP for this Time Period.")); ?>
	</div>
	<form name=frm_update method=post action="controller/manage_table_update.php" language=jscript enctype="multipart/form-data">
		<input type=hidden name=section value='<?php echo $section; ?>' id=section />
		<input type=hidden name=action value='SAVE_UPD' id=action />
		<input type=hidden name=after_action value='' id=after_action />
		<input type=hidden name=obj_id value='' id=obj_id />
		<input type=hidden name=time_id value='<?php echo $ti; ?>' id=time_id />
		<input type=hidden name=old_actual value='0' id=old_actual />
		<input type=hidden name=old_target value='0' id=old_target />
		<input type=hidden name=calctype value='STD' id=calctype />
	<div id=div_update_form style='background-color: #ffffff'>
		<h2>Update <?php echo $head; ?></h2>
		<table class=form width=100%>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."target"]['h_client']; ?>:</th>
				<td id=target>0</td>
			</tr><tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?>:</th>
				<td id=td_actual><input type=text size=10 id=actual name="<?php echo $r_table_fld."actual"; ?>" /></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required)</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=perfcomm name="<?php echo $r_table_fld."perf"; ?>"></textarea></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required if ".$mheadings[$r_section][$r_table_fld."actual"]['h_client']." does not meet ".$mheadings[$r_section][$r_table_fld."target"]['h_client'].")</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=correct name="<?php echo $r_table_fld."correct"; ?>"></textarea></td>
			</tr><?php
			if($setup_defaults['POE']=="T" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>: <span class=float>
				<?php 
				if(isset($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) && strlen($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) >0) { 
					$me->displayIcon("info","id=poe_glossary");
				} ?></span>
				</th>
				<td ><textarea cols=50 rows=7 id=proof name="poe"></textarea></td>
			</tr>
			<?php
			}	//if setup defaults allows for textfield
			if($setup_defaults['POE']=="A" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th>Attachment:</th>
				<td >
					<div id=attach_form><?php $me->displayAttachmentForm(); ?></div>
					<div id=div_update_attach_display></div>
				</td>
			</tr>
			<?php
			}	//if setup defaults allows for attachments
			?>
			<tr>
				<th></th>
				<td><input type=button value="Save Update" class=isubmit /> <?php if($setup_defaults['PLC']=="Y" && isset($can_i_plc)) { echo "<input type=button value=\"Project Life Cycle\" class=plc />"; } ?><span class=float><input type=button value=Cancel class=cancel /></span></td>
			</tr>
		</table>
	</div>
	<div id=div_detail_display style='background-color: #ffffff;'>
	</div>
	<p class=float style='padding-bottom: 20px;'><input type=button value=Cancel class=cancel /></p>
	<div id=div_poe_glossary title="<?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>">
		<p><?php echo str_replace(chr(10),"<br />",decode($mheadings[$r_section][$r_table_fld."attachment"]['glossary'])); ?></p>
	</div>
	<div id=dlg_result>
	</div>
</div>
<div style='position:fixed;right:10px;top:10px;width:300px;'>
	<?php $me->displayResult(array("info","To update a KPI, click anywhere in the row.")); ?>
</div>
<div id=dlg_msg title=Processing>
</div>
<div id=dlg_response title=Success>
<?php echo str_replace(chr(10),"",$me->getDisplayResult(array("ok","Update processed successfully."))); ?>
</div>
<script type="text/javascript"> 

//	setTimeout(function () { $("#dispRes").css('display','none') }, 2500);
		var windowSize = getWindowSize();
		var dialogWidth = windowSize['width']<850 ? windowSize['width']-50 : 800;
		var dialogHeight = windowSize['height']<850 ? windowSize['height']-50 : 800;
		var section = "<?php echo $section; ?>";
		var use_plc = "<?php echo ($me->usePLC() ? "Y" : "N"); ?>";
		var require_comment = <?php echo $me->requireUpdateComments()==true ? "true" : "false"; ?>;
		var comment_length = <?php echo $me->requiredCommentLength(); ?>;
		var okIcon = "<?php echo ASSIST_HELPER::drawStatus(true); ?>";
		var timeInc = <?php echo $time_increment; ?>;
		var month1 = <?php echo $first_month; ?>;
		var head = "<?php echo $head; ?>";
		var processing_html = "<p class=center>Please wait while your form is processed...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p><p class=center>This may take some time depending on the size of the attachments added.</p>";
		var loading_html = "<p class='b center'>Loading...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p>";

	$(function() {
		$("tr.no-highlight").unbind("mouseenter mouseleave");
		
		$("form[name=frm_update] input").keypress(function(e) {
			if(e.keyCode==13) {
				e.preventDefault();
			}
		});
		
		
		var attach_form = $("#attach_form").html();

		var loadingSize = {width:250,height:300};
		var dlgLoadingOpts = ({
			modal: true,
			closeOnEscape: false,
			autoOpen: false,
			width: loadingSize['width'],
			height: loadingSize['height'],
			position: { my: "center", at: "center", of: $("#div_update") }
		});

		var dlgResultsOpts = ({
			closeOnEscape: true,
			modal: true,
			autoOpen: false,
			position: { my: "center", at: "center", of: $("#div_update") },
			buttons: [{ text: "Ok", click: function() { 
					$("#dlg_response").dialog("close"); 
				} 
			}],
			close: function() { 
				$("#div_update").dialog("close");
			}

		});
		var dlgErrorOpts = ({
			modal: true,
			autoOpen: false,
			width: 300,
			height: 320,
			buttons:[{
				text: "Ok",
				click: function() { $(this).dialog("close"); }
			}]
		});
		$("#dlg_msg").dialog({modal: true,autoOpen: false}).html(processing_html);
		$("#dlg_msg").dialog("option",dlgLoadingOpts);
		$("#dlg_response").dialog(dlgResultsOpts);
		$(".ui-dialog-titlebar").hide();
		//createResultsDialog();
		//$("#div_auto_update").hide();
		$("#div_poe_glossary").dialog({
			autoOpen: false,
			modal: true
		});
		$("#div_update #poe_glossary").css("cursor","pointer").click(function() {
			$("#div_poe_glossary").dialog("open").dialog("option","position",{ my: "center", at: "center", of: $("#div_update") });
		});
		$("#div_update").dialog({
			autoOpen: false,
			modal: true,
			width: dialogWidth,
			height: dialogHeight,
			close: function() { 
				$("textarea, input:text").removeClass("required");
				$("input:file").val("");
				enable_scroll();
				//$("body").css("overflow","scroll"); 
			}
		});
		$("#div_update input:button.cancel").click(function() {
			$("#div_update").dialog("close");
		});
		$("#tbl_update tbody tr").click(function() {
			//$("#dlg_msg").html(loading_html).dialog('option',dlgLoadingOpts).dialog("open");
			$("#attach_form").html(attach_form);
			var i = $(this).prop("id");
			var ti = $("#div_update #time_id").val();
			var dta = "action=GET_UPDATE_DETAILS&id="+i+"&time_id="+ti+"&section="+section;
			//alert(dta);
			var d = doAjax("controller/manage_table_update.php",dta);
			//alertArray(d,0);
			//alert(d[1]);
			if(d[0]==1) {
				$("#div_detail_display").html("<h2>Details</h2>"+d['detail_display']);
				$("#div_update_form #td_actual span").remove();
				$("#div_update #obj_id").val(i);
				$("#div_update_form #target").html(d['data']['results']['target'][ti]);
				$("#div_update #old_target").val(d['data']['results']['r'][ti]['target']);
				$("#div_update #calctype").val(d['data']['calctype']);
				$("#div_update_form #actual").val(d['data']['results']['r'][ti]['actual']);
				$("#div_update_form #perfcomm").val(AssistString.decode(d['data']['results']['perfcomm'][ti]));
				$("#div_update_form #correct").val(AssistString.decode(d['data']['results']['correct'][ti]));
				$("#div_update_form #proof").val(AssistString.decode(d['data']['results']['poe'][ti]));
				if(d['data']['targettype']==2) {
					$('<span>%</span>').insertAfter($("#div_update_form #actual"));
				} else if(d['data']['targettype']==2) {
					$('<span>R </span>').insertBefore($("#div_update_form #actual"));
				}
				//alert(d['attach_display']);
				$("#div_update_form #div_update_attach_display").html(d['attach_display']); 
				if(use_plc=="Y" && d['data']['targettype']==2 && d['data']['calctype']=="CO") {
					$("#div_update input:button.plc").show();
				} else {
					$("#div_update input:button.plc").hide();
				}
				if(section=="TOP" && d['data']['results']['auto_update'][ti]==1 && d['data']['assoc']>0) {
					$("#div_auto_update").show();
				} else {
					$("#div_auto_update").hide();
				}
				//alert("Don't forget to: \n- Validate PLC Button");
				disable_scroll();
				//$("body").css("overflow","hidden");
				$("#div_update").dialog("option","title","Update KPI "+d['ref']+" for "+head).dialog("open").dialog("option","position",{ my: "center", at: "top", of: document });
				$("li.hover").hover(
					function(){ $(this).addClass("trhover-dark"); },
					function(){ $(this).removeClass("trhover-dark"); }
				);
			}
			$("#dlg_msg").dialog("close");
		});
		$("#div_update input:button.isubmit").click(function() {
			$("#dlg_msg").html(processing_html).dialog('option','buttons',[]).dialog('option','height',loadingSize['height']).dialog("open");
			var err = "";
			var valid = true;
			var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9",".");
			//validate actual
			$("#div_update #actual").removeClass("required");
			var actual = $("#div_update #actual").val();
			if(actual.length > 0) {
				var a = actual.split("");
				for(x in a) { 
					//alert(a[x]+" : "+($.inArray(a[x],validNumbers)<0));
					if($.inArray(a[x],validNumbers)<0) {
						valid = false;
						break;
					}
				}
			} else {
				valid = false;
			}
			if(!valid) {
				$("#div_update #actual").addClass("required");
				err+="<li><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?> (Only numbers are allowed)</li>";
			}
			//validate comments
			//if(valid) {
				var t = $("#div_update #old_target").val();
				var ct = $("#div_update #calctype").val();
				var fld = $("#div_update_form #perfcomm").prop("name");
				
				//alert(fld+" :: "+require_comment+" :: "+comment_length+" :: "+$("#div_update #perfcomm").val());
				valid_perf = validateMe(fld,require_comment,comment_length,$("#div_update #perfcomm").val(),t,actual,ct);
				fld = $("#div_update_form #correct").prop("name");
				valid_correct = validateMe(fld,require_comment,comment_length,$("#div_update #correct").val(),t,actual,ct);
				if(!valid_perf && require_comment) {
					$("#div_update #perfcomm").addClass("required");
					err+="<li>A <?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?> is required (must exceed "+comment_length+" characters in length.)</li>";
				}
				if(!valid_correct && require_comment) {
					$("#div_update #correct").addClass("required");
					err+="<li>A <?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?> is required where the target has not been met for the period (must exceed "+comment_length+" characters in length.)</li>";
				}
			//}
			//if valid
			if(valid && valid_perf && valid_correct) {
				//save update
				//var f = 0;
				//$("#div_update input:file").each(function() {
				//	f+=$(this).val().length;
				//});
				$form = $("form[name=frm_update]");
				$form.prop("target","file_upload_target");
				//var d = doAjax("controller/manage_table_update.php",AssistForm.serialize($form));
				//alert(d[1]);
				//upload attachments
					//$("#result").val(d[0]);
					//$("#response").val(d[1]);
					//if(f>0) {
						var post_action = "var okIcon = '"+okIcon.replace(/'/g,'"')+"'; 	";
						post_action+= "window.parent.$('#dlg_msg').dialog('close');	";
						post_action+= "window.parent.$('td."+$("#div_update #obj_id").val()+"updateclass').html(okIcon);	";
						post_action+= "window.parent.$('#dlg_response #spn_displayResultText').text('UPDATERESPONSEMSG');	";
						post_action+= "window.parent.$('#dlg_response').dialog('open'); ";
						//	$("form[name=frm_update] #action").val("UPDATE_ATTACH");
						$("form[name=frm_update] #after_action").val(post_action);//what must be done after the form has submitted
						$form.submit();
					/*} else {
						$('#dlg_msg').dialog('close');
						$('td.'+$("#div_update #obj_id").val()+'updateclass').html(okIcon);
						$('#dlg_response #spn_displayResultText').text('Update processed successfully.');
						$('#dlg_response').dialog('open');
					}*/
			//else not valid then display error message
			} else {
				//alert("DEV NOTE: DISPLAY ERROR MESSAGE HERE: "+err);
				$("#dlg_msg").html("<h1 class=red>Error</h1><p>Please complete the following required fields:</p><ul id=ul_req>"+err+"</ul>").dialog("option",dlgErrorOpts);
			}
		});
	});	
	function showResponse(txt) {
		$(function() {
			//alert(txt);
			$("#dlg_response #spn_displayResultText").text(txt);
			$("#dlg_response").dialog("open");
		});
	}
	function showDialog(r,response) {
		$(function() {
				$('<div />',{id:'response_msg', html:response}).dialog({
					autoOpen	: true,
					modal 		: true,
					title 		: r,
					position	: { my: 'center', at: 'center', of: $('#div_update') },
					width 		: 'auto',
					height    	: 'auto',
					buttons		: [{ text: 'Ok', click: function() { 
										$('#response_msg').dialog('destroy'); 
										$('#response_msg').remove(); 
									} 
								}],
					});
		});
	}

</script>