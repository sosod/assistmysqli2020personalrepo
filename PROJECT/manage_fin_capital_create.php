<?php 
$get_open_time = false;
$time = getTime($get_time,$get_open_time);

if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
//arrPrint($_REQUEST);


	
	
	
	
	JSdisplayResultPrep("Processing...");
	//echo "<table><tr><th>Fld</th><th>Type</th><th>New</th><th>Old</th><th>Action</th></tr>";
	$updates = array();
	$trans = array();
	//$changes = array();
	$changes[] = $table_fld."active = true";
	$sql = array();
	$extras_table_fld = array("WARDS"=>"cw","AREA"=>"ca","FUNDSRC"=>"cs");
	foreach($mheadings[$section] as $fld => $h) {
		$log_trans = "";
		switch($h['h_type']) {
			case "WARDS":
			case "FUNDSRC":
			case "AREA":
				$e_table_fld = $extras_table_fld[$h['h_type']];
				$s = array();
				$deleted = array();
				$added = array();
				$log_trans = "";
				$n = array();
				foreach($_REQUEST[$fld] as $i) {
					$n[] = $lists[$h['h_table']][$i]['value']." => ".(!isset($extras[$fld][$i]) ? "NEW" : "");
					if(!isset($extras[$fld][$i])) {
						$log_trans.="<br />- Added \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$lists[$h['h_table']][$i]['value']."\'";
						$added[] = $i;
					}
				}
				$new = implode("<br />",$n);
				if(strlen($log_trans)>0) {
					$log_trans = "Added ".$h['h_client'].":".$log_trans;
					$extras[$fld] = array('new'=>$added,'lt'=>$log_trans);
				}
				/*if(strlen($log_trans)>0) {
					$log_trans = "Added ".$h['h_client'].":".$log_trans;
					if(count($added)>0) {
						$sa = "INSERT INTO ".$dbref."_".$fld." (".$e_table_fld."_".$table_id."id, ".$e_table_fld."_listid, ".$e_table_fld."_active) VALUES (".$obj_id.",".implode(",true),(".$obj_id.",",$added).",true)";
						db_insert($sa);
						$s[] = $sa;
					}
					$v = array(
						'fld'=>$fld,
						'timeid'=>0,
						'text'=>$log_trans,
						'old'=>'',
						'new'=>implode(",",$added),
						'act'=>"C",
						'YN'=>"N"
					);
					logChanges($section,$obj_id,$v,code(implode(chr(10),$s)));
				}*/
				break;
			case "DATE":
				$new = isset($_REQUEST[$fld]) ? strtotime($_REQUEST[$fld]) : "";
				if(checkIntRef($new)) {
					if(strpos($fld,"end")>0) { $new+=86400-1; }
					if($new!=$old) {
						$log_trans = addslashes("Edited ".$h['h_client']." to '".date("d M Y",$new).((checkIntRef($old)) ? "' from '".date("d M Y",$old) : "")."'");
						$trans[$fld]['lt'] = $log_trans;
						$trans[$fld]['n'] = $new;
						$trans[$fld]['o'] = $old;
						$changes[] = $fld." = '".$new."'";
					}
				} else {$changes[] = $fld." = '0'";}
				break;
			case "LIST":
				$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
					$log_trans = addslashes("Set ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."' from '".$object[$fld]."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				break;
			default:
				$new = isset($_REQUEST[$fld]) ? code($_REQUEST[$fld]) : "";
					$log_trans = addslashes("Set ".$h['h_client']." to '".$new."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				break;
		}
		/*echo "<tr>
		<th>".$fld."</th>
		<td>".$h['h_type']."</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	if(count($changes)>0) {
		$sb = "INSERT INTO ".$dbref."_".$table_tbl." SET ".implode(", ",$changes); //." WHERE ".$table_fld."id = ".$obj_id;
		$obj_id = db_insert($sb);
		if(checkIntRef($obj_id)) {
			foreach($trans as $fld => $lt) {
				$v = array(
					'fld'=>$fld,
					'timeid'=>0,
					'text'=>$lt['lt'],
					'old'=>$lt['o'],
					'new'=>$lt['n'],
					'act'=>"C",
					'YN'=>"N"
				);
				logChanges($section,$obj_id,$v,code($sb));
			}
							$v = array(
								'fld'=>"cap_id",
								'timeid'=>0,
								'text'=>"Created Capital Project ".$id_labels_all[$section].$obj_id.".",
								'old'=>0,
								'new'=>0,
								'act'=>"C",
								'YN'=>"Y"
							);
							logChanges($section,$obj_id,$v,code($sb));
			foreach($extras as $fld => $e) {
				$added = $e['new'];
				$log_trans = $e['lt'];
				$h = $mheadings[$section][$fld];
				if(count($added)>0) {
								$sa = "INSERT INTO ".$dbref."_".$fld." (".$wa_tbl_fld[$fld]."_".$table_id."id, ".$wa_tbl_fld[$fld]."_listid, ".$wa_tbl_fld[$fld]."_active) VALUES (".$obj_id.",".implode(",true),(".$obj_id.",",$added).",true)";
								db_insert($sa);
								$s[] = $sa;
							}
							$v = array(
								'fld'=>$fld,
								'timeid'=>0,
								'text'=>$log_trans,
								'old'=>"",
								'new'=>implode(",",$added),
								'act'=>"C",
								'YN'=>"N"
							);
							logChanges($section,$obj_id,$v,code(implode(chr(10),$s)));
			}
			$total = 0;
			$results = array();
			foreach($_REQUEST['ti'] as $ti) {
				$target = (isset($_REQUEST[$fld_target."_".$ti]) && checkIntRef($_REQUEST[$fld_target."_".$ti])) ? $_REQUEST[$fld_target."_".$ti] : 0;
				$total += $target;
				$results[$ti] = array(
					'target'	=> $target,
					'ytd'		=> $total
				);
			}
			$r_sql = array();
			foreach($results as $ti => $r) {
				$r_sql[] = "(".$obj_id.",$ti,".$r['target'].",0,0,0,".$r['ytd'].",0,0,0,$total,0,0,0)";
			}
			$sd = "INSERT INTO ".$dbref."_".$table_tbl."_results (cr_capid,cr_timeid,cr_target,cr_actual,cr_var,cr_perc,cr_ytd_target,cr_ytd_actual,cr_ytd_var,cr_ytd_perc,cr_tot_target,cr_tot_actual,cr_tot_var,cr_tot_perc) VALUES ".implode(",",$r_sql);
			db_insert($sd);
							$v = array(
								'fld'=>$fld_target,
								'timeid'=>0,
								'text'=>"Set budget.",
								'old'=>0,
								'new'=>0,
								'act'=>"C",
								'YN'=>"N"
							);
							logChanges($section,$obj_id,$v,code($sd));
			$results = array();
			$y_sql = array();
			foreach($_REQUEST['yi'] as $yi) {
				$crr = (isset($_REQUEST['years']['crr'][$yi]) && checkIntRef($_REQUEST['years']['crr'][$yi])) ? $_REQUEST['years']['crr'][$yi] : 0;
				$other = (isset($_REQUEST['years']['other'][$yi]) && checkIntRef($_REQUEST['years']['other'][$yi])) ? $_REQUEST['years']['other'][$yi] : 0;
				$y_sql[] = "(".$obj_id.",$yi, 1, $crr, $other)";
			}
			$sy = "INSERT INTO ".$dbref."_".$table_tbl."_forecast (cf_capid, cf_listid, cf_active, cf_crr, cf_other) VALUES ".implode(",",$y_sql);
			db_insert($sy);
							$v = array(
								'fld'=>"forecast",
								'timeid'=>0,
								'text'=>"Set Forecast budget.",
								'old'=>0,
								'new'=>0,
								'act'=>"C",
								'YN'=>"N"
							);
							logChanges($section,$obj_id,$v,code($sy));
			//echo "</table>";
			echo "<script>JSdisplayResult('ok','ok','Capital Project ".$id_labels_all[$section].$obj_id." successfully created.');</script>";
		} else {	//checkintref obj_id
			echo "<script>JSdisplayResult('error','error','An error occurred while trying to create the new Capital Project.  Please try again.');</script>";
		}
	} else {	//count changes > 0
		echo "<script>JSdisplayResult('error','error','An error occurred while trying to create the new Capital Project.  Please try again.');</script>";
		//$result = array("error","An error occurred while trying to create the new Capital Project.  Please try again.");
	}
}

//if(isset($result)) { displayResult($result); }

$obj_id = "";
include("manage_fin_table_detail.php");

?>