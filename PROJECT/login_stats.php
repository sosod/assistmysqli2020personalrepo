<?php
$now = strtotime(date("d F Y",($today))); //echo date("d F Y H:i:s",$now);
$future = strtotime(date("d F Y",($today+( ($next_due)*24*3600)))); //echo date("d F Y H:i:s",$future);


require_once("class/sdbp5_helper.php");
require_once("class/sdbp5_headings.php");
require_once("class/sdbp5_user.php");
require_once("class/sdbp5_useraccess.php");
require_once("class/dirsub.php");
require_once("class/stime.php");
require_once("class/lists.php");
require_once("class/kpi.php");
require_once("class/dept.php");
require_once("class/top.php");

/*****
count kpis where user has departmental['update'] access
and time periods are open
$count['past'] is always = 0 as KPIs cannot be updated after closure date.
*****/

//Read user access to determine time period activity - primary (default), secondary or tertiary
$userObj = new SDBP5_USER(true);
$active_time = $userObj->getActiveTimeField();
$importStatus = $userObj->getImportStatus(); //arrPrint($importStatus);
if($importStatus['KPI']==1) {
	$canUpdate = $userObj->canUpdateDept(); 
} elseif($importStatus['TOP']==1) {
	$canUpdate = $userObj->canUpdateTopLayer();
} else {
	$canUpdate = false;
} //echo ":".$canUpdate.":";
if($canUpdate===true && $importStatus['LIST']==1 && ($importStatus['KPI']==1 || $importStatus['TOP']==1)) { //echo "canUpdate";
	//Find open time periods WHERE closure date falls on or before $future 
	$timeObj = new STIME();
	$open_time = $timeObj->getOpenTime($active_time);//,"close_".$active_time." <= ".$future." AND close_".$active_time." > 0");
//arrPrint($open_time);
	if(count($open_time)>0) {
		//If admin access exists - count KPIs where kpi_active = true, kpi_subid in admin list, kr_timeid in time list, kr_update = false, kr_target>0||kpi_calctype!="ZERO" GROUP BY kr_timeid
		$admins = $userObj->getAdmins();
		if($importStatus['KPI']==1) {
			$subs = array();
			foreach($admins['SUB'] as $s => $v) {
				if($userObj->canUpdateSub($s)) {
					$subs[] = $s;
				}
			}
			$own = array();
			foreach($admins['OWN'] as $s => $v) {
				if($userObj->canUpdateOwn($s)) {
					$own[] = $s;
				}
			}
			
			if(count($subs)>0 || count($own)>0) {
				$time_ids = array_keys($open_time);
				$sql = "SELECT kr_timeid as id, count(kpi_id) as c
						FROM ".$sdb->getDBRef()."_kpi
						INNER JOIN ".$sdb->getDBRef()."_kpi_results 
						ON kr_kpiid = kpi_id AND kr_timeid IN (".implode(",",$time_ids).")  AND kr_update = 0
						WHERE kpi_active = 1 AND (
							".(count($subs)>0 ? "kpi_subid IN (".implode(",",$subs).")" : "")."
							".(count($subs)>0 && count($own)>0 ? "	OR	" : "")."
							".(count($own)>0 ? "kpi_ownerid IN (".implode(",",$own).")" : "")."
						) AND (kpi_calctype = 'ZERO' OR kr_target>0)
						GROUP BY kr_timeid"; //echo $sql;
				$rows = $sdb->mysql_fetch_all_by_id($sql,"id");
				foreach($rows as $t=>$r) {
					$tc = strtotime(date("d F Y",$open_time[$t]["close_".$active_time]));
					if($tc==$now) {
						$count['present']+=$r['c'];
					} elseif($tc<$now) {
						$count['past']+=$r['c'];
					} else {
						$count['future']+=$r['c'];
					}
				}
			}
		} elseif($importStatus['TOP']==1) {
			$subs = array();
			foreach($admins['TOP'] as $s => $v) {
				if($userObj->canUpdateTop($s)) {
					$subs[] = $s;
				}
			}
			$own = array();
			foreach($admins['T_OWN'] as $s => $v) {
				if($userObj->canUpdateTopOwn($s)) {
					$own[] = $s;
				}
			}
			
			if(count($subs)>0 || count($own)>0) {
				$time_ids = array_keys($open_time);
				$sql = "SELECT tr_timeid as id, count(top_id) as c
						FROM ".$sdb->getDBRef()."_top
						INNER JOIN ".$sdb->getDBRef()."_top_results 
						ON tr_topid = top_id AND tr_timeid IN (".implode(",",$time_ids).")  AND tr_update = 0
						WHERE top_active = 1 AND (
							".(count($subs)>0 ? "top_dirid IN (".implode(",",$subs).")" : "")."
							".(count($subs)>0 && count($own)>0 ? "	OR	" : "")."
							".(count($own)>0 ? "top_ownerid IN (".implode(",",$own).")" : "")."
						) AND (top_calctype = 'ZERO' OR tr_target>0)
						GROUP BY tr_timeid"; //echo $sql;
				$rows = $sdb->mysql_fetch_all_by_id($sql,"id");
				foreach($rows as $t=>$r) {
					$tc = strtotime(date("d F Y",$open_time[$t]["close_".$active_time]));
					if($tc==$now) {
						$count['present']+=$r['c'];
					} elseif($tc<=$future) {
						$count['future']+=$r['c'];
					}
				}
			}
		}
	}
}
//To perform count: 
//Check Time period closure date
//If closure date = $now then $count['present']++
//else $count['future']++

?>