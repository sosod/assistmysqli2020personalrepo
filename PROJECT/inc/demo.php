<?php



function drawListTable() {
	global $mheadings;
	$my_head = $mheadings;
	unset($my_head['FIXED']);

	echo "<table width=100%>
		<tr>
			<th>Ref</th>";
	foreach($mheadings['FIXED'] as $key => $row) {
			if($row['c_list'])
				echo "<th>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
	}
	foreach($my_head as $h => $sec) {
		foreach($sec as $key => $row) {
			if($row['c_list'] && !$row['fixed'])
				echo "<th>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
		}
	}
	echo "	</tr>";
	for($i=1;$i<=15;$i++) {
		echo "<tr>
			<th> $i </th>";
				foreach($mheadings['FIXED'] as $key => $row) {
						if($row['c_list'])
							echo "<td>".$key."</td>";
				}
				foreach($my_head as $h => $sec) {
					foreach($sec as $key => $row) {
						if($row['c_list'] && !$row['fixed'])
							echo "<td>".$key."</td>";
					}
				}
		echo "</tr>";
	}
	echo "</table>";
}
?>