<!-- <link rel="stylesheet" href="lib/locked-column.css" type="text/css"> -->
<?php
$my_head = $mheadings[$section];
if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
//[JC] temporary fix R393
if($section=="TOP") {
	unset($mheadings[$r_section]['tr_dept']);
    unset($mheadings[$r_section]['tr_dept_correct']);
    unset($mheadings[$r_section]['tr_dept_attachment']);
}
if($section=="KPI" || $section == "TOP") {
	if($filter['display']!="LIMIT") {
		$colspan = count($mheadings[$r_section])+1;
	} else {
		$colspan = 3;
	}
} elseif($section=="CF" || $section=="CAP") {
	if($filter['display']!="LIMIT") {
		$colspan = count($mheadings[$rheadings]);
	} else {
		$colspan = 0;
		foreach($mheadings[$rheadings] as $h) {
			if($h['c_list']) 
				$colspan++;
		}
	}
} else {
	$colspan = 2;
}
?>
<div id=tbl-container>
<table id=tbl>



<thead>
<?php
$total_columns = 0;
	$total = array();
	$total['all']['target'] = 0;
	$total['all']['actual'] = 0;
	echo "<tr>
		<th rowspan=2>Ref</th>";
		foreach($mheadings['FIXED'] as $key => $row) {
			$total_columns++;
			echo "<th rowspan=2 >".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
		}
		foreach($my_head as $key => $row) {
			if(($filter['display']!= "LIMIT" || $row['c_list']) && !$row['fixed']) {
				$total_columns++;
				echo "<th rowspan=2>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
			}
		}
		$tc = 0;
		$t_css = 1;
		if($section == "CF" || $section == "CAP") {
			$t_css = 3;
			foreach($mheadings[$section.'_H'] as $ti => $row) {
				$tc++;
				echo "<th colspan=$colspan class=\"time".$t_css."\">".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
				$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			foreach($time as $ti => $t) {
				if($ti > $filter['when'][1]) {
					break;
				} elseif($ti>=$filter['when'][0]) {
					$tc++;
					echo "<th colspan=$colspan class=\" time".$t_css."\">".$t['display_full']."</th>";
					$total[$ti]['target'] = 0;
					$total[$ti]['actual'] = 0;
					$t_css = ($t_css>=4) ? 1 : $t_css+1;
				}
			}
		}
		if(($section!="CF" && $section!="CAP") && $filter['when'][0]!=$filter['when'][1]) {
			switch($section) {
				case "TOP":
				case "KPI":
					echo "<th class=\" total\" colspan=$colspan>Overall Performance</th>";
					break;
				default:
					echo "<th class=\"total\" colspan=$colspan>Total</th>";
			}
		}
	echo "	</tr>";
	echo "	<tr>";
		if($section == "CF" || $section == "CAP") {
			$t_css = 3;
			foreach($mheadings[$section.'_H'] as $h => $cf_h) {
				foreach($mheadings[$section.'_R'] as $r => $cf_r) {
					if($filter['display']!="LIMIT" || $cf_r['c_list']) {
						echo "<th class=\"time".$t_css."\">".(strlen($cf_r['h_client'])>0 ? $cf_r['h_client'] : $cf_r['h_ignite'])."</th>";
						$total[$h][$r] = 0;
					}
				}
				$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			$t_css = 1;
			for($i=1;$i<=$tc;$i++) {
				switch($section) {
					case "TOP":
					case "KPI":
						if($filter['display']!="LIMIT") {
							foreach($mheadings[$r_section] as $fld => $h) {
								echo "<th class=\"time".$t_css."\">".$h['h_client']."</th>";
								if($fld==$fld_actual) {
									echo "<th class=\"time".$t_css."\">R<img src=\"/pics/blank.gif\" height=1 width=15></th>";
								}
							}
						} else {
							echo "<th class=\"time".$t_css."\" >Target</th>
								<th  class=\"time".$t_css."\" >Actual</th>
								<th class=\"time".$t_css."\"  >R<img src=\"/pics/blank.gif\" height=1 width=15></th>";
						}
						break;
					default:
						echo "<th  class=\"time".$t_css."\" >Budget</th>
							<th  class=\"time".$t_css."\" >Actual</th>";
				}
				$t_css = ($t_css>=4) ? 1 : $t_css+1;
			}
		}
		if($section!="CF" && $section !="CAP" && $filter['when'][0]!=$filter['when'][1]) {
			switch($section) {
				case "TOP":
				case "KPI":
					echo "<th class=\" total\" >Target</th>
						<th class=\" total\" >Actual</th>
						<th class=\" total\" >R<img src=\"/pics/blank.gif\" height=1 width=15></th>";
					break;
				default:
					echo "<th class=\" total\" >Budget</th>
						<th class=\" total\" >Actual</th>";
			}
		}
	echo "	</tr>";

?>
</thead>






<tbody>
<?php

$rc = 0;
	$object_rs = getRS($object_sql);
	while($object = mysql_fetch_assoc($object_rs)) {
		$obj_id = $object[$table_fld.'id'];
		echo "<tr>
			<td class=thtd>".(isset($id_label) ? $id_label : "").$obj_id."</td>";
				foreach($mheadings['FIXED'] as $key => $h) {
					echo "<td>";
					if($h['h_type']=="LIST") {
						if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
							echo "<div class=centre>".$lists[$h['h_table']][$object[$key]]['code']."</div>";
						} else {
							if(!isset($lists[$h['h_table']][$object[$key]]['value'])) {
								$v = $unspecified;
							} else {
								$v = $lists[$h['h_table']][$object[$key]]['value'];
							}
							echo displayValue($v);
						}
					} else {
						if($key==$detail_link && ($section=="KPI" || $section == "TOP" || $section == "CAP")) {
							echo "<a href=".$base[0]."_".$base[1]."_detail.php?id=".$obj_id.">";
						}
						echo displayValue($object[$key])."</a>";
					}
					echo "</td>";
				}
				foreach($my_head as $key => $h) {
					if(($filter['display']!="LIMIT" || $h['c_list']) && !$h['fixed']) {
						echo "<td>";
						if($h['h_type']=="LIST") {
							if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
								echo "<div class='centre hand' title=\"".$lists[$h['h_table']][$object[$key]]['value']."\" style=\"text-decoration: underline\">".$lists[$h['h_table']][$object[$key]]['code']."</div>";
							} else {
								if(!isset($lists[$h['h_table']][$object[$key]]['value'])) {
									$v = $unspecified;
								} else {
									$v = $lists[$h['h_table']][$object[$key]]['value'];
								}
								echo displayValue($v);
							}
						} elseif($h['h_type']=="TEXTLIST") {
							$x = $object[$key];
							$a = array();
							if(strlen($x)>0 && $x != "0") {
								$y = explode(";",$x);
								foreach($y as $z) {
									if(isset($lists[$h['h_table']][$z]['value'])) {
										$a[] = $lists[$h['h_table']][$z]['value'];
									}
								}
							}
							if(count($a)>0) {
								$v = implode("; ",$a);
							} else {
								$v = $unspecified;
							}
							echo displayValue($v);
						} else {
							switch($key) {
								case $table_fld."area":
								case $table_fld."wards":
								case "capital_area":
								case "capital_wards":
								case "capital_fundsource":
									if(isset($wa) && isset($wa[$key]) && isset($wa[$key][$obj_id])) {
										//echo $key." :: "; arrPrint($wa[$key][$obj_id]);
										foreach($wa[$key][$obj_id] as $v) {
											if(isset($v['code']) && strlen($v['code'])) {
												echo $v['code'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											} else {
												echo $v['value'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											}
										}
									}
									break;
								case $table_fld."topid":
									if(isset($tops) && isset($tops[$object[$key]])) {
										echo displayValue($tops[$object[$key]]['top_value']." [".$id_labels_all['TOP'].$object[$key]."]");
									}
									break;
								case $table_fld."capitalid":
									if(isset($caps) && isset($caps[$object[$key]])) {
										echo displayValue($caps[$object[$key]]['cap_name']." [".$id_labels_all['CAP'].$object[$key]."]");
									}
									break;
								case "top_annual":
								case "top_revised":
									echo "<div class=right>".KPIresultDisplay($object[$key],$object[$table_fld.'targettype'])."</div>";
									break;
								case "cap_planstart":
								case "cap_planend":
								case "cap_actualstart":
								case "cap_actualend":
									if(strlen($object[$key])>1) {
										echo date("d M Y",$object[$key]);
									}
									break;
								default:
									echo displayValue($object[$key]);
									break;
							}
						}
						echo "</td>";
					}
				}
				
				
				
				
				
				
				
				
				$result_object = $results[$obj_id];
				$t_css = 1;
				switch($section) {
					case "KPI":
					case "TOP":
						$obj_tt = $object[$table_fld.'targettype'];
						$obj_ct = $object[$table_fld.'calctype'];
						$values = array('target'=>array(),'actual'=>array());
						foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								$r = KPIcalcResult($values,$obj_ct,$filter['when'],$ti);
								if($filter['display']!="LIMIT") {
									foreach($mheadings[$r_section] as $fld => $h) {
										if($fld==$fld_target) {
											echo "<td class=\"right time".$t_css."\">";
												echo KPIresultDisplay($r['target'],$obj_tt);
										} elseif($fld==$fld_actual) {
											echo "<td class=\"right time".$t_css."\">";
												echo KPIresultDisplay($r['actual'],$obj_tt);
											echo "</td><td class=\"".$r['style']."\">".$r['text'];
										} else {
											echo "<td class=\"left time".$t_css."\">";
											if($fld=="tr_dept" || $fld=="tr_dept_correct") {
												echo displayValue(displayTRdept($result_object[$ti][$fld],"HTML"));
											} else {
												if(in_array($fld,array('tr_attachment','kr_attachment'))) {
													$x = unserialize($result_object[$ti][$fld]);
													$poe = array();
													if(isset($x['poe'])) { $poe[] = $x['poe']; }
													$a = isset($x['attach']) ? $x['attach'] : array();
													if(count($a)>0) { 
														$attach = array();
														foreach($a as $b) {
															$attach[] = $b['original_filename'];
														}
														$poe[] = "- ".implode("<br />- ",$attach); 
													}
													$v = implode("<br />",$poe);
												} else {
													$v = $result_object[$ti][$fld];
												}
												//$echo.=decode(str_replace(chr(10),$newline,$v));
											
												echo displayValue($v);
											}
										}
										echo "</td>";
									}
								} else {
									echo "<td class=\"right time".$t_css."\">";
										echo KPIresultDisplay($r['target'],$obj_tt);
									echo "</td><td class=\"right time".$t_css."\">";
										echo KPIresultDisplay($r['actual'],$obj_tt);
									echo "</td>";
									echo "<td class=\"".$r['style']."\">".$r['text']."</td>";
								}
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
							}
						}
						if($filter['when'][0]!=$filter['when'][1]) {
							$r = KPIcalcResult($values,$obj_ct,$filter['when'],"ALL");
							echo "<td class=\"right total\">";
								echo KPIresultDisplay($r['target'],$obj_tt);
							echo "</td><td class=\"right total\">";
								echo KPIresultDisplay($r['actual'],$obj_tt);
							echo "</td>";
							echo "<td class=\"".$r['style']."\">".$r['text']."</td>";
						}
						break;
					case "CF":
						$result_object = $results[$obj_id][$filter['when'][0]];
						$t_css = 3;
						foreach($mheadings['CF_H'] as $rs => $cf_h) {
							$cfh_values = array();
							$budget = 0;
							foreach($mheadings['CF_R'] as $res => $cf_r) {
								$fld = "cr_".$cf_h['field'].$cf_r['field'];
								$cfh_values[$fld] = $result_object[$fld];
								switch($res) {
									case "_1": case "_2": case "_3":	$budget+=$cfh_values[$fld];				break;	//o. budget, adj. est., vire
									case "_4":							$cfh_values[$fld] = $budget;			break;	//adj. budget
									case "_5":							$actual = $cfh_values[$fld];			break;	//actual
									case "_6":							$cfh_values[$fld] = $actual - $budget;	break;	//variance
								}
								if($filter['display']!="LIMIT" || $cf_r['c_list']) {
									$val = $cfh_values[$fld];
									$total[$rs][$res]+= $val;
									echo "<td class=\"right time".$t_css."\">";
										switch($cf_r['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
									echo "</td>";
								}
							}
							$t_css = ($t_css<=1) ? 3 : $t_css-1;
						}
						break;
					case "CAP":
						$t_css = 3;
						$values = array();
						//foreach($time as $ti => $t) {
						//	if($ti > $filter['when'][1]) {
						//		break;
						//	} elseif($ti>=$filter['when'][0]) {
							$t = $time[$filter['when'][0]];
							$ti = $t['id'];
							foreach($mheadings['CAP_H'] as $fh => $f) {
								foreach($mheadings[$rheadings] as $rh => $cr) {
									$r = $fh.$rh;
									if(!isset($total[$ti][$r])) { $total[$ti][$r] = 0; }
									if(!isset($values[$r])) { $values[$r] = 0; }
									$val = $result_object[$ti][$r];
									$total[$ti][$r]+=$val;
									$values[$r]+=$val;
									if($filter['display']!="LIMIT" || $cr['c_list']) {
										echo "<td class=\"right time".$t_css."\">";
										switch($cr['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
										echo "</th>";
									}
								}
								$t_css = ($t_css<=1) ? 3 : $t_css-1;
							}
						//}
						/*if($filter['when'][0]!=$filter['when'][1]) {
							echo "<td class=\"right total\">";
								echo number_format($values['cr_target'],2);
								$total['all']['target']+=$values['cr_target'];
							echo "</td><td class=\"right total\">";
								echo number_format($values['cr_actual'],2);
								$total['all']['actual']+=$values['cr_actual'];
							echo "</td>";
						}*/
						break;
					default:
						foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								$total[$ti]['target']+=$values['target'][$ti];
								$total[$ti]['actual']+=$values['actual'][$ti];
								echo "<td class=\"right time".$t_css."\">";
									echo number_format($values['target'][$ti],2);
								echo "</td><td class=\"right time".$t_css."\">";
									echo number_format($values['actual'][$ti],2);
								echo "</td>";
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
							}
						}
						if($filter['when'][0]!=$filter['when'][1]) {
							echo "<td class=\"right total\">";
								echo number_format(array_sum($values['target']),2);
							echo "</td><td class=\"right total\">";
								echo number_format(array_sum($values['actual']),2);
							echo "</td>";
						}
						break;
				}
		echo "</tr>";
	}
	?>
	</tbody>
	
	
	
	
	
	
	
	<?php
	if($section=="RS" || $section == "CAP" || $section == "CF") {
		$t_css = 3;
		echo "<tfoot>";
		echo "<tr>";
			echo "<td>&nbsp;</td>";
			echo "<td class=totaltext colspan=".$total_columns." style=\"font-weight: bold; text-align: right;\">Total:</td>";
			switch($section) {
			case "CF":
						//$t_css = 3;
						foreach($mheadings['CF_H'] as $rs => $cf_h) {
							foreach($mheadings['CF_R'] as $res => $cf_r) {
								if($filter['display']!="LIMIT" || $cf_r['c_list']) {
									$val = $total[$rs][$res];
									echo "<td class=\"right time".$t_css."\">";
										switch($cf_r['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo "&nbsp"; break;
											default: echo number_format($val,2); break;
										}
									echo "</td>";
								}
							}
							$t_css = ($t_css<=1) ? 3 : $t_css-1;
						}
				break;
			case "CAP":
				/*foreach($time as $ti => $t) {
					if($ti > $filter['when'][1]) {
						break;
					} elseif($ti>=$filter['when'][0]) { */
					$ti = $filter['when'][0];
					foreach($mheadings['CAP_H'] as $fh => $h) {
						foreach($mheadings[$rheadings] as $rh => $cr) {
							$r = $fh.$rh;
							if($filter['display']!="LIMIT" || $cr['c_list']) {
								echo "<td class=\"right time".$t_css."\">";
									if($cr['h_type']!="PERC") {
										echo number_format($total[$ti][$r],2);
									}
								echo "</th>";
							}
						}
						$t_css = ($t_css<=1) ? 3 : $t_css-1;
					}
				//}
				/*if($filter['when'][0]!=$filter['when'][1]) {
					echo "<td class=total style=\"text-align: right;\">";
						echo number_format($total['all']['target'],2);
					echo "</td>";
					echo "<td class=total style=\"text-align: right;\">";
						echo number_format($total['all']['actual'],2);
					echo "</td>";
				}*/
				break;
			default:
				$t_css = 1;
				foreach($time as $ti => $t) {
					if($ti > $filter['when'][1]) {
						break;
					} elseif($ti>=$filter['when'][0]) {
						$total['all']['target']+= $total[$ti]['target'];
						$total['all']['actual']+= $total[$ti]['actual'];
						echo "<td class=\"time".$t_css."\" style=\"text-align: right;\">".number_format($total[$ti]['target'],2)."</td>";
						echo "<td class=\"time".$t_css."\" style=\"text-align: right;\">".number_format($total[$ti]['actual'],2)."</td>";
						$t_css = ($t_css>=4) ? 1 : $t_css+1;
					}
				}
				if($filter['when'][0]!=$filter['when'][1]) {
					echo "<td class=total style=\"text-align: right;\">".number_format($total['all']['target'],2)."</td>";
					echo "<td class=total style=\"text-align: right;\">".number_format($total['all']['actual'],2)."</td>";
				}
			}
		echo "</tr>";
		echo "</tfoot>";
	}
	
?>
</table>
</div>
<div id=dlg_popup>

</div>
<script type="text/javascript"> 
$(function() {
	$(".hand").click(function() {
		var t = $(this).prop("title");
		if(t.length>0) { alert(t); }
	});
	$("tr.no-highlight").unbind("mouseenter mouseleave");
});
/*	var table = document.getElementById('tbl');
	var cTR = table.getElementsByTagName('TR');  //collection of rows
 
		for (i = 0; i < cTR.length; i++)
		{
			if(i!=1) {
				var tr = cTR.item(i);
				tr.cells[0].className = 'locked';
			}
		}*/
		
</script>