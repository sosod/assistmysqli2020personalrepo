<?php

class CAPITAL extends SDBP5_HELPER {

	private $section = "CAP";
	private $table_name;
	private $results_table_name;
	
	const ACTIVE = 1;
	
	const HEAD = "CAP";
	const HHEAD = "CAP_H";
	const RHEAD = "CAP_R";
	
	const REFTAG = "CP";

	public function __construct() {
		parent::__construct();
		$this->table_name = $this->getDBRef()."_capital";
		$this->results_table_name = $this->getDBRef()."_capital_results";
	}

	
	/********************************
			GET FUNCTIONS
	********************************/
	public function getHeadingCodes() { return array(self::HEAD,self::HHEAD,self::RHEAD); }
	
	public function getResults($i) {
		$result_sql = "SELECT * FROM ".$this->results_table_name." WHERE cr_capid = ".$i." ORDER BY cr_timeid";
		$results = $this->mysql_fetch_all_fld($result_sql,"cr_timeid");
		
		return $results;
	}
	
	
	public function getLiteDetails($i) {
		$sql = "SELECT c.cap_name, c.cap_descrip, c.cap_vote, cap_subid as si
				FROM ".$this->table_name." c
				WHERE c.cap_id = ".$i;
		$data = $this->mysql_fetch_one($sql);
		if(isset($data['si'])) {
			$ds = new DIRSUB();
			$data['dir'] = $ds->getDirSubName($data['si']);
		}
		return $data;
	}
	
	
	
	
}


?>