<?php

class DISPLAY extends SDBP5_HELPER {

	public function __construct() {
		parent::__construct();
	}

	
	
	
	
	
	
	public function drawLiteKPI($kpiObj,$head,$rhead,$time,$kr,$update_form=false) {
		echo $this->getLiteKPI($kpiObj,$head,$rhead,$time,$kr,$update_form);
	}
	
	
	
	public function getLiteKPI($kpiObj,$head,$rhead,$time,$kr,$update_form=false) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$i = $kr['display_id'];
		if(!isset($head['dir']) && isset($head['top_dirid'])) {
			$head['dir'] = $head['top_dirid'];
		}
		$echo = "";
		if(!$update_form) {
			$echo .= "<h4>".$kr['value']." (".$i.")</h4>";
		}
		$echo.= "
		<table class=form width=".$width.">
			<tr>
				<th>Ref:</th>
				<td id=ref>".$i."</td>
			</tr>
			<tr>
				<th width=25%>".$head['dir']['h_client'].":</th>
				<td id=dir>".$kr['dir']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getValueField()]['h_client'].":</th>
				<td id=value>".$kr['value']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getUnitField()]['h_client'].":</th>
				<td id=unit>".$kr['unit']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getPOEField()]['h_client'].":</th>
				<td id=poe>".$kr['poe']."</td>
			</tr>
			<tr>
				<th>".$head[$kpiObj->getCTField()]['h_client'].":</th>
				<td id=ct>".$kr['ct']."</td>
			</tr>
		</table>";
		if($update_form) {
			$echo.= "<h2>Current Results</h2>";
		}
		$echo.=$this->getKPIResults($kpiObj,$rhead,$time,$kr);
		return $echo;
	}
	
	
	
	public function drawKPIResults($kpiObj,$rhead,$time,$kr,$edit=false,$obj_id=0) {
		echo $this->getKPIResults($kpiObj,$rhead,$time,$kr,$edit,$obj_id);
	}
	
	public function getKPIResults($kpiObj,$rhead,$time,$kr,$edit=false,$obj_id=0) {
		//$kpiObj->arrPrint($kr);
		$echo = "";
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$width+=($edit ? 100 : 0);
		if(isset($GLOBALS['userObj'])) { $userObj = $GLOBALS['userObj']; } else { $userObj = new SDBP5_USER(); }
		$active_time_field = $userObj->getActiveTimeField();
		$canEdit = $edit;
		$isManage = $kpiObj->isManage();
		$isAdmin = $kpiObj->isAdmin();
		$target_type = $kr['targettype'];
		$echo.= "
		<table width=".$width." style='margin-top: 10px'>
			<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>	
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>
				<th rowspan=2>".$rhead[$kpiObj->getRPerfField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRCorrectField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRAttachField()]['h_client']."</th>
				".($edit ? "<th rowspan=2 width=100>Update Status</th>" : "")."
				".($edit && $kpiObj->isTop() ? "<th rowspan=2 width=100>Auto Update</th>" : "")."
			</tr><tr>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
			</tr>";
			foreach($time as $ti => $t) {
				$poe = "";
				$attach = "";
				$i = $t['id']; 
				$canEdit = ($edit && $t['active_'.$active_time_field]);
				if($t['start']<$this->today) {
					//if(strlen($kr['results']['poe'][$i])>0) { $poe = $this->decode($kr['results']['poe'][$i]); }
					if(strlen($kr['results']['poe'][$i])>0) { $poe = ($kr['results']['poe'][$i]); }
					$a = $kr['results']['attach'][$i];
					//arrPrint($a);
					if(count($a)>0) {
						$attachments = array();
						foreach($a as $b=>$c) {
							$attachments[$b] = $c;
						}
						//arrPrint($attachments);
						$attach = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder());//,$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
					}
					//$poe = implode("<br />",$z);
					/*echo "
					<tr id=tr_".$i.">
						<th class=left>".$t['display']."</th>
						<td class=right>".( ($isAdmin && $canEdit) ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()) : $kr['results']['target'][$i])."</td>
						<td class=right>".( ($canEdit) ? $this->KPIresultEdit($kr['results']['r'][$i]['actual'],$target_type,$kpiObj->getRActualField()) : $kr['results']['actual'][$i])."</td>
						<td class='".$kr['results']['r'][$i]['style']."'>".$kr['results']['r'][$i]['text']."</td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right>".$kr['results']['ytd_actual'][$i]."</td>
						<td class='".$kr['results']['ytd_r'][$i]['style']."'>".$kr['results']['ytd_r'][$i]['text']."</td>
						<td>".($canEdit ? $this->KPIcommentEdit($kr['results']['perfcomm'][$i],$kpiObj->getRPerfField(),$i) : $kr['results']['perfcomm'][$i])."</td>
						<td>".($canEdit ? $this->KPIcommentEdit($kr['results']['correct'][$i],$kpiObj->getRCorrectField(),$i) : $kr['results']['correct'][$i])."</td>
						<td>".($canEdit ? $this->KPIcommentEdit($poe,$kpiObj->getRAttachField(),$i) : $poe).$attach."</td>
						<td class=center>".$this->drawStatus($kr['results']['update'][$i]==true)."</td>
					</tr>";*/
					if(isset($kr['results']['target'][$ti]) && ($kr['results']['target'][$i]>0 || $kr['calctype']=="ZERO" || $kr['results']['update'][$i]==true)) {	
						if($kr['results']['update'][$i]==true) { 
							$us = "Y";
						} else {
							$us = "N";
						}
					} else {
						$us = "info";
					}
					$unlock = false;
					if($edit) {
						$update_status = $this->drawStatus($us);
						if($kr['results']['update'][$i]==true) {
							$update_status.="<p style='margin-top: 1px; margin-bottom: 0px;'>By ".$kr['results']['updateuser'][$i]." on ".date("d-M-Y",strtotime($kr['results']['updatedate'][$i]));
						}
						if($kpiObj->isTop()) {
							$auto_update = $kpiObj->drawStatus($kr['results']['auto'][$i]);
							switch($kr['results']['auto'][$i]) {
								case "Y": 		$auto_update.=""; break;
								case "warn": 	$auto_update.="Conflict with associated Departmental KPIs"; break;
								case "lock": 	$auto_update="<div style='cursor:pointer;' title='Click to Unlock' class=unlock_auto time_id=".$i." time_name='".$t['display']."' obj_id=".$obj_id.">".$auto_update."Locked for Manual Update</div>"; $unlock = true; break;
								case "N": 		$auto_update.="No associated Departmental KPIs"; break;
							}
						}
					}
					$echo.= "
					<tr id=tr_".$i.">
						<th class=left>".$t['display'].($canEdit ? "<input type=hidden name=ti[] value=$i />" : "")."</th>
						<td class=right>".( ($isAdmin && $canEdit) ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()."[$i]",true) : $kr['results']['target'][$i])."</td>
						<td class=right>".($kr['results']['actual'][$i])."</td>
						<td class='".$kr['results']['r'][$i]['style']."'>".$kr['results']['r'][$i]['text']."</td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right>".$kr['results']['ytd_actual'][$i]."</td>
						<td class='".$kr['results']['ytd_r'][$i]['style']."'>".$kr['results']['ytd_r'][$i]['text']."</td>
						<td>".($kr['results']['perfcomm'][$i])."</td>
						<td>".($kr['results']['correct'][$i])."</td>
						<td>".$poe.$attach."</td>
						".($edit ? "<td class=center>".$update_status."</td>" : "")."
						".($edit && $kpiObj->isTop() ? "<td class=center id=auto_".$i.">".$auto_update."</td>" : "")."
					</tr>";
			} else {
					if($edit) {
						if($kpiObj->isTop()) {
							$auto_update = $kpiObj->drawStatus($kr['results']['auto'][$i]);
							switch($kr['results']['auto'][$i]) {
								case "Y": 		$auto_update.=""; break;
								case "warn": 	$auto_update.="Conflict with associated Departmental KPIs"; break;
								case "lock": 	$auto_update="<div style='cursor:pointer;' title='Click to Unlock' class=unlock_auto time_id=".$i." time_name='".$t['display']."' obj_id=>".$auto_update."Locked for Manual Update</div>"; $unlock = true; break;
								case "N": 		$auto_update.="No assoc. Departmental KPIs"; break;
							}
						}
					}
					$echo.= "
					<tr id=tr_".$i.">
						<th class=left>".$t['display'].($canEdit ? "<input type=hidden name=ti[] value=$i />" : "")."</th>
						<td class=right>".($isAdmin && $canEdit ? $this->KPIresultEdit($kr['results']['r'][$i]['target'],$target_type,$kpiObj->getRTargetField()."[$i]",true) : $kr['results']['target'][$i])."</td>
						<td class=right></td>
						<td class=''></td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td></td>
						<td></td>
						<td></td>
						".($edit ? "<td></td>" : "" )."
						".($edit && $kpiObj->isTop() ? "<td class=center id=auto_".$i.">".$auto_update."</td>" : "" )."
					</tr>";
			}
		}
		$echo .="
		</table>";
		if($edit && $unlock) {
			$echo.="
			<script type=text/javascript>
			var winSize = getWindowSize();
			var dlgWidth = 400;
			var cenVert = winSize['height']/2-100;
			var cenHor = winSize['width']/2-dlgWidth/2;
			$(function() {
				$('div.unlock_auto').click(function() {
					if(confirm('Are you sure you wish to unlock this KPI for '+$(this).attr('time_name')+'?')) {
						var ti = $(this).attr('time_id');
						var dta = 'section=TOP&action=UNLOCK_AUTO&id=".$obj_id."&time_id='+ti;
						var r = doAjax('controller/edit.php',dta);
						//change display object in cell to green unlocked icon with no text ->drawStatus('unlock')
						$('td #auto_'+ti).html(r['icon']);
						//Display confirmation result div
						//alert(r[1]);
						var response = r['msg'];
						$('<div />',{id:'response_msg', html:response}).dialog({
							autoOpen	: true,
							modal 		: true,
							title 		: r['title'],
							position	: [cenHor, cenVert],
							width 		: dlgWidth,
							height    	: 'auto',
							buttons		: [{ text: 'Ok', click: function() { 
												$('#response_msg').dialog('destroy'); 
												$('#response_msg').remove(); 
											} 
										}],
							});
					}
				});
			});
			</script>";
		}
		return $echo;
	}
	
	
	function KPIresultEdit($val,$tt,$fld,$annual=false,$hidden=false) {
		if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
		switch($tt) {
			case 1:	return "R <input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\"  />"; break;
			case 2:	return "<input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\"  /> %"; break;
			case 3: return "<input type=text name=$fld size=7 value=\"".$val."\" class=\"valid8me right ".($annual ? "annual_revised" : "")."\" />"; break;
			default: return $val; break;
		}
	}
	

	function KPIresultDisplay($val,$tt,$annual=false) {
		$ret = "";
		if(ceil($val)==$val) { $decimal = 0; } else { $decimal = 2; }
		switch($tt) {
			case 1:	$ret = "R ".number_format($val,$decimal); break;
			case 2:	$ret = number_format($val,$decimal)."%"; break;
			case 3: $ret = number_format($val,$decimal); break;
			default: $ret = $val; break;
		}
		if($annual) {
			$ret.= "<input type=hidden name=hidden value=\"".$val."\" class=\"".($annual ? "annual_revised" : "")."\" />";
		}
		return $ret;
	}
	
	function KPIcommentEdit($val,$fld,$ti) {
		return "<textarea name=\"".$fld."_".$ti."\" rows=3 cols=40>".$val."</textarea>";
	}
	
	
	
	//CAPITAL PROJECT
	
	public function drawLiteCapital($capObj,$head,$time,$cap_id) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$h = $head[CAPITAL::HEAD];
		$cap = $capObj->getLiteDetails($cap_id);
		echo "<h4>".$cap['cap_name']." (".CAPITAL::REFTAG.$cap_id.")</h4>
		<table class=form width=$width>
			<tr>
				<th width=25%>".$head['dir']['h_client'].":</th>
				<td>".$cap['dir']."</td>
			</tr>
			<tr>
				<th>".$h['cap_vote']['h_client'].":</th>
				<td>".$cap['cap_vote']."</td>
			</tr>
			<tr>
				<th>".$h['cap_descrip']['h_client'].":</th>
				<td>".$cap['cap_descrip']."</td>
			</tr>
		</table>
		";
		
		
		$this->drawCapitalResults($capObj,$head,$time,$capObj->getResults($cap_id));
	
	}
	
	
	public function drawCapitalResults($capObj,$head,$time,$cr) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		$head1 = $head[CAPITAL::HHEAD];
		$head2 = $head[CAPITAL::RHEAD];
		echo "<table width=".$width." style='margin-top: 10px' >
			<tr>
				<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
			foreach($head1 as $fldh => $f) {
				echo "<th colspan=".count($head2).">".$f['h_client']."</th>";
			}
			echo "
			</tr>
			<tr>";
			foreach($head1 as $fldh => $f) {
				foreach($head2 as $fld => $h) {
					echo "<th>".$h['h_client']."</th>";
				}
			}
			echo "</tr>";
		foreach($time as $ti => $t) {
			$i = $t['id'];
			$c = $cr[$i];
			echo "
			<tr>
				<th class=left>".$t['display']."</th>";
				foreach($head1 as $fldh => $f) {
					foreach($head2 as $fldr => $h) {
						$fld = $fldh.$fldr;
						echo "<td class=right>".$capObj->formatResult($c[$fld],$fldr=="perc" ? "perc" : "number")."</td>";
					}
				}
			echo "
			</tr>";
		}

		echo "</table>";	
	
	}
	
	
	
	
}








/*	public function drawKPIResults($kpiObj,$rhead,$time,$kr,$edit=false) {
		if(isset($GLOBALS['width'])) { $width = $GLOBALS['width']; } else { $width = 700; }
		echo "
		<table width=".$width." style='margin-top: 10px'>
			<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>	
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>
				<th rowspan=2>".$rhead[$kpiObj->getRPerfField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRCorrectField()]['h_client']."</th>
				<th rowspan=2>".$rhead[$kpiObj->getRAttachField()]['h_client']."</th>
			</tr><tr>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
				<th>".$rhead[$kpiObj->getRTargetField()]['h_client']."</th><th>".$rhead[$kpiObj->getRActualField()]['h_client']."</th><th>R</th>
			</tr>";
			foreach($time as $ti => $t) {
				$i = $t['id']; 
				if($t['start']<$this->today) {
					$z = array();
					if(strlen($kr['results']['poe'][$i])>0) { $z[] = str_replace(chr(10),"<br />",$kr['results']['poe'][$i]); }
					$a = $kr['results']['attach'][$i];
					//arrPrint($a);
					if(count($a)>0) {
						$attachments = array();
						foreach($a as $b=>$c) {
							$attachments[$b] = $c;
						}
						//arrPrint($attachments);
						$z[] = $kpiObj->getObjectAttachmentDisplay(false,$attachments,$kpiObj->getAttachFolder(),$kpiObj->getAttachmentDownloadLink(),$kpiObj->getAttachmentDownloadOptions());
					}
					$poe = implode("<br />",$z);
					echo "
					<tr id=tr_".$i.">
						<th class=left>".$t['display']."</th>
						<td class=right>".$kr['results']['target'][$i]."</td>
						<td class=right>".$kr['results']['actual'][$i]."</td>
						<td class='".$kr['results']['r'][$i]['style']."'>".$kr['results']['r'][$i]['text']."</td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right>".$kr['results']['ytd_actual'][$i]."</td>
						<td class='".$kr['results']['ytd_r'][$i]['style']."'>".$kr['results']['ytd_r'][$i]['text']."</td>
						<td>".$kr['results']['perfcomm'][$i]."</td>
						<td>".$kr['results']['correct'][$i]."</td>
						<td>".$poe."</td>
					</tr>";
			} else {
					echo "
					<tr id=tr_".$i.">
						<th class=left>".$t['display']."</th>
						<td class=right>".$kr['results']['target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td class=right>".$kr['results']['ytd_target'][$i]."</td>
						<td class=right></td>
						<td class=''></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>";
			}
		}
		echo "
		</table>";
	}
*/




?>