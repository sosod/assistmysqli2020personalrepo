<?php

class KPI extends SDBP5_HELPER {

	protected $sdbp5;
	protected $section_details;
	
	protected $table_name;
	protected $results_table_name;
	protected $results_id_name;

	const DELETED = 0;
	const ACTIVE = 1;
	
	private $head;
	private $rhead;
	
	private $page_section;
	
	public function __construct($h,$rh) {
		parent::__construct();
		$this->sdbp5 = new SDBP5();
		$this->section_details = $this->sdbp5->getSection($this->section);
		$this->setAttachmentDownloadOptions("section=".$this->section);
		$this->setAttachmentDeleteOptions("section=".$this->section."&results_fld=".$this->getResultsFieldPrefix());
		$this->setHelperAttachmentFolder($this->getAttachFolder());
		$this->table_name = $this->getDBRef()."_".$this->table;
		$this->results_table_name = $this->getDBRef()."_".$this->table."_results";
		$this->results_id_name = $this->results_table_field.$this->id_field."id";
		$this->head = $h;
		$this->rhead = $rh;
	}

	/*******************************
	       GET FUNCTIONS 
	*******************************/
	public function getAttachFolder() {				return $this->getModRef()."/".$this->section;	}
	public function getHeadingCodes() {				return array($this->head,$this->rhead);	}
	public function getRHeadCode() { 				return $this->rhead; }
	public function getHeadCode() { 				return $this->head; }
	public function getSection() {					return $this->section;	}
	public function getSectionHead() {				return $this->section_head;	}
	public function getTimeIDs() {					return $this->time_ids;	}
	public function getFirstMonth() {				return $this->first_month;	}
	public function getTimeIncrement() {			return $this->time_increment;	}
	public function getAbsoluteRequiredFields() {	return $this->absolute_required_fields; }
	//field names
	public function getTableName() { 				return $this->table_name; }
	public function getTable() {	 				return $this->table; }
	public function getValueField() { 	 			return $this->table_field."value"; }
	public function getUnitField() { 	 			return $this->table_field."unit"; }
	public function getPOEField() { 	 			return $this->table_field."poe"; }
	public function getCTField() { 		 			return $this->table_field."calctype"; }
	public function getRPerfField() { 	 			return $this->results_table_field."perf"; }
	public function getRCorrectField() { 			return $this->results_table_field."correct"; }
	public function getRAttachField() {  			return $this->results_table_field."attachment"; }
	public function getRTargetField() {  			return $this->results_table_field."target"; }
	public function getRActualField() {  			return $this->results_table_field."actual"; }
	public function getFieldPrefix() {		 		return $this->table_field; }
	public function getIDFieldName() {				return $this->id_field; }
	public function getResultsFieldPrefix() {		return $this->results_table_field; }
	public function getWardsAreaTableDetails() { 	return $this->wa_table_field; }
	public function isManage() {					return ($this->page_section=="MANAGE"); }
	public function isAdmin() {						return ($this->page_section=="ADMIN"); }
	public function isTop() {						return ($this->section=="TOP"); }
	
	//kpi
	public function getObject($i) {	
		$sql = "SELECT * FROM ".$this->getDBRef()."_".$this->table." WHERE ".$this->table_field."id = $i";
		return $this->mysql_fetch_one($sql);
	}
	
	//attachment	
	function getAttachmentDetails($kpi_id,$time_id,$attach_id) {
		$record = $this->getUpdateRecord($kpi_id,$time_id);
		$attachments = $record['attachment']['attach'];
		if(isset($attachments[$attach_id])) {
			$a = $attachments[$attach_id];
			$a[0] = true;
			$a[2] = $record;
			return $a;
		} else {
			return array(0=>false,1=>$attachments,2=>$record);
		}
	}
	
	
	
	
	public function getUpdateRecord($i,$t) {
		$data = array();
		$sql = "SELECT * FROM ".$this->results_table_name." WHERE ".$this->results_id_name." = $i AND ".$this->results_table_field."timeid = $t ";
		$data = $this->mysql_fetch_one($sql);
		$p = $data[$this->results_table_field.'attachment'];
		if(strlen($p)>0) {
			$p = unserialize($p);
		} else {
			$p = array('poe'=>"",'attach'=>array());
		}
		$data['attachment'] = $p;
		return $data;
	}
	
	
	
	public function getAssociatesKPIs($i,$field) {
		$sql = "SELECT ".$this->table_field."id as id FROM ".$this->table_name." WHERE ".$this->table_field.$field." = ".$i." AND ".$this->table_field."active & ".self::ACTIVE." = ".self::ACTIVE;
		$keys = $this->mysql_fetch_all_by_value($sql,"id");
		return $keys;
	}
	
	
	
	
	
	
	
	
	/*****************************
	SET FUNCTIONS 
	*****************************/
	public function setAsManage() {
		$this->page_section = "MANAGE";
	}
	public function setAsAdmin() {
		$this->page_section = "ADMIN";
	}
	public function setPageSection($m) {
		$this->page_section = strtoupper($m);
	}
	
	
	
	
	
	
	
}


?>