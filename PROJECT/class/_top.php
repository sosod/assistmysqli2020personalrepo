<?php

class TOP extends KPI {

	protected $section = "KPI";
	
	protected $section_head = "Departmental SDBIP";

	protected $object_name = "KPI";
	protected $object_title = "Departmental KPI";
	
	protected $table_field = "kpi_";
	protected $table = "kpi";
	protected $id_field = "kpi";
	protected $detail_link = "kpi_value";
	protected $results_table_field = "kr_";
	protected $target_field = "kr_target";
	protected $actual_field = "kr_actual";
	protected $wa_table_field = array('wards'=>"kw",'area'=>"ka","kpi_wards"=>"kw","kpi_area"=>"ka");
	protected $time_ids = array(1,2,3,4,5,6,7,8,9,10,11,12);
	protected $first_month = 1;
	protected $time_increment = 1;
	
	
	const REFTAG = "D";
	
	const HEAD = "KPI";
	const RHEAD = "KPI_R";
	
	
	public function __construct() {
		parent::__construct(self::HEAD,self::RHEAD);
	}

	
	
	
	
	
	
	
	/****************
	SAVE FUNCTIONS
	*****************/
	
	public function saveUpdate($var) {

		$obj_id = $var['obj_id'];
		$time_id = $var['time_id'];
		$section = $var['section'];
		$actual = $var['kr_actual'];
		$perf = $this->code($var['kr_perf']);
		$correct = $this->code($var['kr_correct']);
		$poe = isset($var['poe']) ? $this->code($var['poe']) : "";
		$attachment = array('poe'=>$poe,'attach'=>array());

		$headings = $this->getModuleHeadings(array("KPI_R"));
		$object = $this->getObject($obj_id);
		$time = $this->getTime(array($time_id));
		$result = array(0=>"ok",1=>"Update saved for ".$this->section_details['code'].$obj_id);
		$old = $this->getUpdateRecord($obj_id,$time_id);
		
		//validate change
		if($old['kr_actual']!=$actual
			|| $old['kr_perf']!=$perf
			|| $old['kr_correct']!=$correct
			|| $old['attachment']['poe']!=$attachment['poe']
		) {
			//update result
			$attachment['attach'] = isset($old['attachment']['attach']) ? $old['attachment']['attach'] : array();
			$attach = serialize($attachment);
			$lsql = "UPDATE ".$this->getDBRef()."_kpi_results SET
				kr_actual = ".$actual.",
				kr_perf = '".$perf."',
				kr_correct = '".$correct."',
				kr_attachment = '".$attach."',
				kr_update = 1,
				kr_updateuser = '".$this->getUserID()."',
				kr_updatedate = now()
				WHERE kr_kpiid = ".$obj_id." AND kr_timeid = ".$time_id;
			$mar = $this->db_update($lsql);
			//log update
			$log = array();
			if($old['kr_actual']!=$actual) {
				$fld = "kr_actual";
				$new = $actual;
				$n = $this->KPIresultDisplay($actual,$object['kpi_targettype']);
				$o = $this->KPIresultDisplay($old[$fld],$object['kpi_targettype']);
				$h = $headings['KPI_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['kr_perf']!=$perf) {
				$fld = "kr_perf";
				$new = $perf;
				$n = $this->decode($perf);
				$o = $this->decode($old[$fld]);
				$h = $headings['KPI_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['kr_correct']!=$correct) {
				$fld = "kr_correct";
				$new = $correct;
				$n = $this->decode($correct);
				$o = $this->decode($old[$fld]);
				$h = $headings['KPI_R'][$fld];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old[$fld],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			if($old['attachment']['poe']!=$attachment['poe']) {
				$fld = "kr_attachment_poe";
				$new = $poe;
				$n = $this->decode($poe);
				$o = $this->decode($old['attachment']['poe']);
				$h = $headings['KPI_R']['kr_attachment'];
				$log[$fld] = array(
					'fld'=>$fld,
					'new'=>$new,
					'old'=>$old['attachment']['poe'],
					'act'=>"U",
					'YN'=>"Y",
					'timeid'=>$time_id,
					'text'=>$this->code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$n."' from '".$o."' for time period ".$time[$time_id]['display_short'])
				);
			}
			foreach($log as $v) {
				$this->logChanges($section,$obj_id,$v,$this->code($lsql));
			}
		} else {
			$result[0] = "info";
			$result[1] = "No change found to be saved.";
		}
		//return result*/
		return $result;
	}
	
	public function saveUpdateAttachments($i,$t,$new,$old) {
		$headings = $this->getModuleHeadings(array("KPI_R"));
		$time = $this->getTime(array($t));
		$section = $this->section;
		
		$attach = array();
		$old_files = array();
		foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
		$new_files = array();
		foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }
		
		$update = array('poe'=>$old['poe'],'attach'=>$attach);
		$sql = "UPDATE ".$this->getDBRef()."_kpi_results SET kr_attachment = '".serialize($update)."' WHERE kr_kpiid = ".$i." AND kr_timeid = ".$t;
		$this->db_update($sql);
		
		$fld = "kr_attachment_files";
		$n = implode(", ",$new_files);
		$o = implode(", ",$old_files);
		$h = $headings['KPI_R']['kr_attachment'];
		$log = array(
			'fld'=>$fld,
			'new'=>serialize($new),
			'old'=>serialize($old['attach']),
			'act'=>"U",
			'YN'=>"Y",
			'timeid'=>$t,
			'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$t]['display_short'])
		);
		$this->logChanges($section,$i,$log,$this->code($sql));
	}
	
	
	public function deleteAttachment($kpi_id,$time_id,$attach_id) {
		$section = $this->section;
		$attach = $this->getAttachmentDetails($kpi_id,$time_id,$attach_id);
		//$this->arrPrint($attach);
		if($attach[0]===true) {
			$this->checkFolder($this->getAttachmentDeleteFolder());
			$old_path = "../../files/".$this->getCmpCode()."/".$this->getAttachFolder()."/".$attach['system_filename'];
			$new_path = "../../files/".$this->getCmpCode()."/".$this->getAttachmentDeleteFolder()."/".$section."_".date("YmdHis")."_".$attach['system_filename'];
			if(copy($old_path,$new_path)) {
				unlink($old_path);
			}
			$record = $attach[2]; //$this->getUpdateRecord($kpi_id,$time_id);
			$a = $record['attachment'];
			unset($a['attach'][$attach_id]);
			$n = serialize($a);
			$sql = "UPDATE ".$this->getDBRef()."_kpi_results SET kr_attachment = '$n' WHERE kr_kpiid = ".$kpi_id." AND kr_timeid = ".$time_id." ";
			$this->db_update($sql);
			
			$headings = $this->getModuleHeadings(array("KPI_R"));
			$time = $this->getTime(array($time_id));
			
			$old = $record;
			$new = $a;
			
			$attach = array();
			$old_files = array();
			foreach($old['attach'] as $a => $x) { $attach[$a] = $x; $old_files[] = $x['original_filename']; }
			$new_files = array();
			foreach($new as $a => $x) { $attach[$a] = $x; $new_files[] = $x['original_filename']; }
			
			$fld = "kr_attachment_files";
			$n = implode(", ",$new_files);
			$o = implode(", ",$old_files);
			$h = $headings['KPI_R']['kr_attachment'];
			$log = array(
				'fld'=>$fld,
				'new'=>serialize($new),
				'old'=>serialize($old['attach']),
				'act'=>"U",
				'YN'=>"Y",
				'timeid'=>$t,
				'text'=>$this->code("Added new ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." Attachment(s) '$n' to time period ".$time[$time_id]['display_short'])
			);
			$this->logChanges($section,$i,$log,$this->code($sql));
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**********************************
		GET FUNCTIONS
	***********************************/

	
	public function getUpdateFormDetails($id) {
		$data = $this->getUpdateDetailsArray();
		$sql = "SELECT kpi_value, kpi_unit, kpi_poe, kpi_targettype, kpi_calctype, CONCAT(d.value,' - ',s.value) as dir, ct.value as ct 
				FROM ".$this->getDBRef()."_kpi k
				INNER JOIN ".$this->getDBRef()."_subdir s
				  ON k.kpi_subid = s.id
				INNER JOIN ".$this->getDBRef()."_dir d
				  ON s.dirid = d.id
				INNER JOIN ".$this->getDBRef()."_list_calctype ct
				  ON ct.code = k.kpi_calctype
				WHERE kpi_id = ".$id;
		$row = $this->mysql_fetch_one($sql);
		$data['dir'] = $this->decode($row['dir']);
		$data['value'] = $this->decode($row['kpi_value']);
		$data['unit'] = $this->decode($row['kpi_unit']);
		$data['poe'] = $this->decode($row['kpi_poe']);
		$data['calctype'] = $this->decode($row['kpi_calctype']);
		$data['ct'] = $this->decode($row['ct'])." (".$data['calctype'].")";
		$data['targettype'] = $this->decode($row['kpi_targettype']);
		$sql = "SELECT * FROM ".$this->getDBRef()."_kpi_results WHERE kr_kpiid = ".$id." ORDER BY kr_timeid";
		$results = $this->mysql_fetch_all_by_id($sql,"kr_timeid");
		$targets = array();
		$actuals = array();
		for($ti=1;$ti<=12;$ti++) {
			if(isset($results[$ti])) {
				$r = $results[$ti];
				$r['kr_attachment'] = unserialize($r['kr_attachment']);
				$data['results']['target'][$ti] = $r['kr_target'];
				$data['results']['actual'][$ti] = $r['kr_actual'];
				$data['results']['perfcomm'][$ti] = $this->decode($r['kr_perf']);
				$data['results']['correct'][$ti] = $this->decode($r['kr_correct']);
				$data['results']['poe'][$ti] = $this->decode($r['kr_attachment']['poe']);
				$data['results']['attach'][$ti] = array();
				if(is_array($r['kr_attachment']['attach'])) {
					foreach($r['kr_attachment']['attach'] as $key=>$a) {
						$data['results']['attach'][$ti][$id."_".$ti."_".$key] = array(
							'original_filename'=>$a['original_filename'],
							'system_filename'=>$a['system_filename'],
						);
					}
				}
			}
			$data['results']['r'][$ti] = $this->KPIcalcResult(array('target'=>$data['results']['target'],'actual'=>$data['results']['actual']),$data['calctype'],array(1,$ti),$ti);
			$data['results']['ytd_r'][$ti] = $this->KPIcalcResult(array('target'=>$data['results']['target'],'actual'=>$data['results']['actual']),$data['calctype'],array(1,$ti),"ALL");
			$data['results']['target'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['target']*1,$data['targettype']);
			$data['results']['actual'][$ti] = $this->KPIresultDisplay($data['results']['r'][$ti]['actual']*1,$data['targettype']);
			$data['results']['ytd_target'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['target']*1,$data['targettype']);
			$data['results']['ytd_actual'][$ti] = $this->KPIresultDisplay($data['results']['ytd_r'][$ti]['actual']*1,$data['targettype']);
		}
				
		return $data;
	}
		


	
	public function getAssocID($s) {
		switch($s) {
			case "TOP": return "topid"; break;
			case "CAP": return "capitalid"; break;
		}
		return false;
	}
	

}



?>