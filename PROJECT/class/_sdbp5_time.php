<?php

class SDBP5_TIME extends SDBP5_HELPER {

	const ACTIVE = 1;
	
	private $time_settings;

	public function __construct() {
		parent::__construct();
		$this->time_settings = array(
			'TOP'=>array(3,6,9,12),
			'ALL'=>array(1,2,3,4,5,6,7,8,9,10,11,12),
		);
	}

	
	public function getAllTime() {
		$time = $this->getMyTime("ALL");
		return $time;
	}
	
	public function getMyTime($section) {
		$time_ids = $this->getTimeIDs($section);
		if(count($time_ids)>0) {
			$sql = "SELECT * FROM ".$this->getDBRef()."_list_time WHERE id IN (".implode(",",$time_ids).") AND active & ".SDBP5_TIME::ACTIVE." = ".SDBP5_TIME::ACTIVE." ORDER BY start_date";
			$t = $this->mysql_fetch_all($sql);
			$time = $this->processTime($t);
		} else {
			$time = array();
		}
		return $time;
	}
	
	
	
	
	
	private function getTimeIDs($s) {
		if(isset($this->time_settings[$s])) {
			return $this->time_settings[$s];
		} else {
			return $this->time_settings['ALL'];
		}
	}
	private function processTime($t) {
		$time = array();
		foreach($t as $x) {
			$end = strtotime($x['end_date']);
			$start = strtotime($x['start_date']);
			$display = date("F Y",$end);
			$time[$end] = array(
				'id'		=> $x['id'],
				'start'		=> $start,
				'end'		=> $end,
				'display'	=> $display,
			);
		}
		return $time;
	}
	
}


?>