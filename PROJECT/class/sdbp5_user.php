<?php

class SDBP5_USER extends SDBP5_HELPER {

	private $access;
	private $admin;
	private $strict;
	
	public function __construct($strict=false) {
		$this->strict = $strict;
		//echo "sdbp5_user";
		parent::__construct();
		//echo "... trying useraccess";
		$userObj = new SDBP5_UserAccess();
		//$userObj->testUserAccess();
		$this->access = $userObj->getUserAccess();
		$this->admin = $userObj->getAdminAccess("",$strict);
		
		//$this->arrPrint($this->admin);
		
	}

	public function canSetup() {	return ($this->access[0]===true && $this->access['setup']==true);	}
	public function canAdminDept() {	return ($this->access[0]===true && $this->access['kpi']==true);	}
	public function canAdminTop() {	return ($this->access[0]===true && $this->access['toplayer']==true);	}
	public function canManageFinancials() {	return ($this->access[0]===true && $this->access['finance']==true);	}
	public function canManageAssurance() {	return ($this->access[0]===true && $this->access['assurance']==true);	}
	
	public function canManageTop() {	return ($this->admin[0]===true && count($this->admin['TOP'])>0);	}
	public function canManageDept() {	return ($this->admin[0]===true && ( count($this->admin['DIR'])>0 || count($this->admin['SUB'])>0 || count($this->admin['OWN'])>0) );	}

	public function canUpdateDir($d) {	return ($this->admin[0]===true && isset($this->admin['DIR'][$d]) && $this->admin['DIR'][$d]['act_update']==true);	}
	public function canEditDir($d) {	return ($this->admin[0]===true && isset($this->admin['DIR'][$d]) && $this->admin['DIR'][$d]['act_edit']==true);	}
	public function canCreateDir($d) {	return ($this->admin[0]===true && isset($this->admin['DIR'][$d]) && $this->admin['DIR'][$d]['act_create']==true);	}
	
	public function canUpdateSub($d) {	return ($this->admin[0]===true && isset($this->admin['SUB'][$d]) && $this->admin['SUB'][$d]['act_update']==true);	}
	public function canEditSub($d) {	return ($this->admin[0]===true && isset($this->admin['SUB'][$d]) && $this->admin['SUB'][$d]['act_edit']==true);	}
	public function canCreateSub($d) {	return ($this->admin[0]===true && isset($this->admin['SUB'][$d]) && $this->admin['SUB'][$d]['act_create']==true);	}

	public function canUpdateOwn($d) {	return ($this->admin[0]===true && isset($this->admin['OWN'][$d]) && $this->admin['OWN'][$d]['act_update']==true);	}
	public function canUpdateTopOwn($d) {	return ($this->admin[0]===true && isset($this->admin['T_OWN'][$d]) && $this->admin['T_OWN'][$d]['act_update']==true);	}
	
	public function canUpdateTop($d) {	return ($this->admin[0]===true && isset($this->admin['TOP'][$d]) && $this->admin['TOP'][$d]['act_update']==true);	}
	public function canEditTop($d) {	return ($this->admin[0]===true && isset($this->admin['TOP'][$d]) && $this->admin['TOP'][$d]['act_edit']==true);	}
	public function canCreateTop($d) {	return ($this->admin[0]===true && isset($this->admin['TOP'][$d]) && $this->admin['TOP'][$d]['act_create']==true);	}
	
	public function canUpdateDept($level=array()) {
		$u = false;
		if($this->admin[0]===true) {
			if(isset($this->admin['SUB']) && (count($level)==0 || in_array("SUB",$level))) {
				foreach($this->admin['SUB'] as $key => $s) {
					if($this->canUpdateSub($key)===true) {
						$u = true;
						break;
					}
				}
			}
			if(isset($this->admin['OWN']) && !$u && (count($level)==0 || in_array("OWN",$level))) {
				foreach($this->admin['OWN'] as $key => $s) {
					if($this->canUpdateOwn($key)===true) {
						$u = true;
						break;
					}
				}
			}
			if(isset($this->admin['DIR']) && !$u && (count($level)==0 || in_array("DIR",$level))) {
				foreach($this->admin['DIR'] as $key => $s) { //echo $key;
					if($this->canUpdateDir($key)===true) {
						$u = true;
						break;
					}
				}
			}
		} //echo ":".$u.":";
		return $u;
	}

	public function canUpdateTopLayer($level=array()) {
		$u = false;
		if($this->admin[0]===true) {
			if(isset($this->admin['TOP']) && (count($level)==0 || in_array("TOP",$level))) {
				foreach($this->admin['TOP'] as $key => $s) {
					if($this->canUpdateTop($key)===true) {
						$u = true;
						break;
					}
				}
			}
			if(isset($this->admin['T_OWN']) && !$u && (count($level)==0 || in_array("T_OWN",$level))) {
				foreach($this->admin['T_OWN'] as $key => $s) {
					if($this->canUpdateTopOwn($key)===true) {
						$u = true;
						break;
					}
				}
			}
		}
		return $u;
	}

	
	public function getActiveTimeField() { 
		if($this->access[0]!==true) {
			return "primary";
		} else {
			if(isset($this->access['tertiary']) && $this->access['tertiary'] ==true) {
				return "tertiary";
			} elseif($this->access['second']==true) {
				return "secondary";
			} else {
				return "primary";
			}
		}
	}
	
	public function getAdmins() {
		return $this->admin;
	}
	public function getAccess() {
		return $this->access;
	}
	
}


?>