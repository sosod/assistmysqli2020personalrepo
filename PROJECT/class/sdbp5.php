<?php

class SDBP5 extends SDBP5_HELPER {

	protected $sections = array(
		'KPI'	=> array(
			'code'	=>"D",
			'name'	=>"Departmental SDBIP",
			'status'=>1,
			'page'	=>"dept",
			'field'	=>"kpi_",
		),
		'TOP'	=> array(
			'code'	=>"TL",
			'name'	=>"Top Layer SDBIP",
			'status'=>1,
			'page'	=>"top",
			'field'	=>"top_",
		),
		'CAPITAL'	=> array(
			'code'	=>"CAP",
			'name'	=>"Capital Projects",
			'status'=>1,
			'page'	=>"capital",
		),
		'CASHFLOW'	=> array(
			'code'	=>"CF",
			'name'	=>"Monthly Cashflows",
			'status'=>1,
			'page'	=>"cashflow",
		),
		'REVBYSRC'	=> array(
			'code'	=>"RS",
			'name'	=>"Revenue By Source",
			'status'=>1,
			'page'	=>"revbysrc",
		),
	);
	private $module_defaults = array();
	

	public function __construct() {
		parent::__construct();
		$this->module_defaults = $this->getModuleDefaults();
		if(!isset($this->module_defaults['PLC'])) { $this->module_defaults['PLC'] = "N"; }
	}

	
	public function getSections() {
		return $this->sections;
	}
	
	public function getSection($s) {
		return $this->sections[$s];
	}
	
	/**** shortcuts to access module defaults ****/
	public function requireUpdateComments() {
		return ($this->module_defaults['perfcomm']=="Y" ? true : false);
	}
	public function requiredCommentLength() {
		return $this->module_defaults['commlen'];
	}
	public function usePLC() {
		return ($this->module_defaults['PLC']=="Y" ? true : false);
	}
	public function usePOE() {
		return $this->module_defaults['POE'];
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*******************************
	  USER MANUALS
	*******************************/
	
	public function getUserManuals($modref="",$user_id="") {
		$manuals = array();
		$mdb = new ASSIST_DB("MASTER");
		
		$userObj = new SDBP5_USER();
		if(strlen($modref)>0) {
			$this->setDBRef($modref);
			$userObj->setDBRef($modref);
		}
		if(strlen($user_id)==0) { $user_id = $this->getUserID(); }
		
		$sql = "SELECT * FROM assist_user_manuals WHERE modlocation = 'SDBP5' AND status & 1 = 1 ORDER BY sort";
		$rows = $mdb->mysql_fetch_all($sql);
		//$this->arrPrint($rows);
		foreach($rows as $r) {
			$active = false;
			$name = $r['link'];
			$e = explode(".",$name);
			$ext = $e[1];
			switch($r['usage']) {
				case "view":
				case "report":
				case "glossary":
					$active = !($this->isAdminUser());
					break;
				case "manage_dept_action":
					$active = $userObj->canUpdateDept(array("SUB","OWN"));
					break;
				case "manage_dept_update":
					$active = $userObj->canUpdateDept(array("DIR","SUB","OWN"));
					break;
				case "manage_dept":
					$active = $userObj->canManageDept();
					//$name = "Manage Departmental SDBIP ".$r['date'].".".$ext;
					break;
				case "manage_top":
					$active = $userObj->canManageTop();
					break;
				case "manage_fin":
					$active = $userObj->canManageFinancials();
					break;
				case "manage_assurance":
					$active = $userObj->canManageAssurance();
					break;
				case "admin_dept":
					$active = $userObj->canAdminDept();
					break;
				case "admin_top":
					$active = $userObj->canAdminTop();
					break;
				case "setup":
					$active = ($userObj->canSetup() || $this->isAdminUser());
					//echo ":".$userObj->canSetup().":";
					//$name = "Setup ".$r['date'].".".$ext;
					break;
				case "support":
					$active = $this->isSupportUser();
					break;
			}
			if($active) {
				$manuals[] = array(
					'text'=>$r['text'],
					'name'=>$name,
					'date'=>strtotime($r['date']),
					'link'=>$r['link']
				);
			}
		}
		//$this->arrPrint($manuals);
		return $manuals;
	}
	
	
	
	
	
	
}


?>