<?php 
$section = "TOP";
			$get_lists = true;
			$get_active_lists = false;
			$locked_column = true;

include("inc/header.php"); 
unset($mheadings['TOP']['top_repkpi']);
//arrPrint($_REQUEST);
//Filter settings
$filter = array();
	//WHEN
	if(isset($_REQUEST['filter_when'])) {
		$f_when = $_REQUEST['filter_when'];
		$filter_when = array(
			0=>($f_when[0]<=$f_when[1] ? $f_when[0] : $f_when[1]),
			1=>($f_when[0]<=$f_when[1] ? $f_when[1] : $f_when[0])
		);
	} else {
		$filter_to = $current_time_id;
		$filter_when = array(1,$filter_to);
	}
	$filter_when[0] = ceil($filter_when[0]/3)*3;
	$filter_when[1] = ceil($filter_when[1]/3)*3;
	$filter['when'] = $filter_when;
	//WHO
	if(isset($_REQUEST['filter_who'])) {
		$f_who = $_REQUEST['filter_who'];
		if(strtolower($f_who)=="all") {
			$filter_who = array("","All");
		} else {
			$filter_who = explode("_",$f_who);
		}
	} else {
		$filter_who = array("","All");
	}
	$filter['who'] = $filter_who;
	//WHAT
	$filter['what'] = isset($_REQUEST['filter_what']) ? $_REQUEST['filter_what'] : "All";
	//DISPLAY
	$filter['display'] = isset($_REQUEST['filter_display']) ? $_REQUEST['filter_display'] : "LIMIT";


//arrPrint($filter);


if(!$import_status[$section]) {
	die("<p>Top Layer SDBIP has not yet been created.  Please try again later.</p>");
} else {

	$dir_sql = "SELECT d.id as dir FROM ".$dbref."_dir d
		WHERE d.id IN (SELECT DISTINCT t.top_dirid FROM ".$dbref."_top t
		WHERE top_active = true ) AND d.active = true";
	$valid_dir_sub = mysql_fetch_alls($dir_sql,"dir");


	//drawViewFilter($who,$what,$when,$sel,$filter)
	$head = "<h2 style=\"margin-top: 0px;\">Quarter ending ";
	if($filter['when'][0]==$filter['when'][1]) {
		$head.= $time[$filter['when'][0]]['display_full'];
	} else {
		$head.= $time[$filter['when'][0]]['display_full']." - ".$time[$filter['when'][1]]['display_full'];
	}
	$head.="</h2>";
	$filter['who'] = drawViewFilter(true,false,true,2,true,$filter,$head);
	//drawPaging($url,$search,$start,$limit,$page,$pages,$search_label,$form)
	//drawPaging_noSearch($self,'',0,15,1,10,'Reference',$self);


	//SQL
	$object_sql = "SELECT t.* FROM ".$dbref."_top t
	INNER JOIN ".$dbref."_dir dir ON t.top_dirid = dir.id AND dir.active = true
	WHERE top_active = true ".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND top_dirid = ".$filter['who'][1] : "");
	
	$results_sql = "SELECT r.* FROM ".$dbref."_top_results r
	INNER JOIN ".$dbref."_top t ON r.tr_topid = t.top_id AND t.top_active = true
	WHERE tr_timeid <= ".$filter['when'][1]." AND tr_topid IN (SELECT t.top_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
	
	$results = mysql_fetch_alls2($results_sql,"tr_topid","tr_timeid");
	$fld_target = "tr_target";
	$fld_actual = "tr_actual";
	
	if(isset($mheadings[$section][$table_fld.'wards'])) {
		$wards_sql = "SELECT tw_topid, id, value, code FROM ".$dbref."_top_wards INNER JOIN ".$dbref."_list_wards ON tw_listid = id 
		WHERE tw_active = true AND tw_topid IN (SELECT t.top_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
		$wa[$table_fld.'wards'] = mysql_fetch_alls2($wards_sql,"tw_topid","id");
	}
	if(isset($mheadings[$section][$table_fld.'area'])) {
		$area_sql = "SELECT ta_topid, id, value, code FROM ".$dbref."_top_area INNER JOIN ".$dbref."_list_area ON ta_listid = id 
		WHERE ta_active = true AND ta_topid IN (SELECT t.top_id FROM ".substr($object_sql,16,strlen($object_sql)).")";
		$wa[$table_fld.'area'] = mysql_fetch_alls2($area_sql,"ta_topid","id");
	}

	
	//$object_sql.= " LIMIT 50";
	
	include("view_table.php");

	
	
}	//if import_status['import_Section'] == false else	
?>
</body>
</html>