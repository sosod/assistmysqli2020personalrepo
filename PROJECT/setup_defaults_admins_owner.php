<?php
//$type = "OWN";
$log_section.= "_".$type;
$heading = strlen($mheadings['KPI']['kpi_ownerid']['h_client'])>0 ? $mheadings['KPI']['kpi_ownerid']['h_client'] : $mheadings['KPI']['kpi_ownerid']['h_ignite'];
$sql = "SELECT * FROM ".$dbref."_list_owner WHERE active = true";
$owners = mysql_fetch_alls($sql,"id");
$sql = "SELECT tkid as id, CONCAT_WS(' ',tkname, tksurname) as value FROM assist_".$cmpcode."_timekeep
		INNER JOIN assist_".$cmpcode."_menu_modules_users ON usrtkid = tkid AND usrmodref = '$modref' 
		WHERE tkstatus = 1 
		ORDER BY tkname, tksurname";
$users = mysql_fetch_alls($sql,"id");
$sql = "SELECT a.*, CONCAT_WS(' ',t.tkname,t.tksurname) as value FROM ".$dbref."_user_admins a
		INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = a.tkid AND t.tkstatus = 1
		WHERE type = '$type'
		ORDER BY a.ref, t.tkname, t.tksurname";
$admins = mysql_fetch_alls2($sql,"ref","tkid");


if(isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case "ADD":
			$u = $_REQUEST['user'];
			$i = $_REQUEST['i'];
			if(!checkIntRef($i) || strlen($u)==0) {
				$result = array("error","An error occurred while trying to add the administrator.  Please try again.");
			} else {
				if(isset($admins[$i][$u])) {
					$result = array("info","Administrator '".$admins[$i][$u]['value']."' already exists for ".$heading." '".$owners[$i]['value']."'.");
				} else {
					$sql = "INSERT INTO ".$dbref."_user_admins (id, tkid, active, ref, type, act_update, act_edit, act_create, act_approve) VALUES (null,'$u',true,$i,'$type',true,false,false,false)";
					$id = db_insert($sql);
					if(checkIntRef($id)) {
						$result = array("ok","New administrator '".$users[$u]['value']."' has been added to $heading '".$owners[$i]['value']."'.");
						$admins[$i][$u] = array('id'=>$id,'tkid'=>$u,'active'=>true,'ref'=>$id,'type'=>$type,'value'=>$users[$u]['value']);
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$id,
											'field'=>"",
											'text'=>"Added new ".$log_title." <i>".$users[$u]['value']."</i> to $heading <i>".$owners[$i]['value']."</i>.",
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					} else {
						$result = array("error","An error occurred while trying to add the administrator.  Please try again.");
					}
				}
			}
			break;
		case "DEACTIVATE":
			$i = $_REQUEST['i'];
			if(!checkIntRef($i)) {
				$result = array("error","An error occurred while trying to deactivate the administrator.  Please try again.");
			} else {
				$sql = "SELECT a.*, CONCAT_WS(' ',t.tkname,t.tksurname) as value FROM ".$dbref."_user_admins a
						INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = a.tkid AND t.tkstatus = 1
						WHERE active = true AND type = '$type' AND a.id = $i
						ORDER BY a.ref, t.tkname, t.tksurname";
				$adm = mysql_fetch_all($sql);
				if(count($adm)==0) {
					$result = array("info","Administrator could not be found.");
				} else {
					$sql = "UPDATE ".$dbref."_user_admins SET active = false WHERE id = ".$i;
					$mar = db_update($sql);
					if($mar > 0) {
						$u = $adm[0]['tkid'];
						$r = $adm[0]['ref'];
						$result = array("ok","Administrator '".$adm[0]['value']."' has been deactivated from $heading '".$owners[$r]['value']."'.");
						$admins[$r][$u]['active'] = false;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$r,
											'field'=>"active",
											'text'=>"Deactivated ".$log_title." <i>".$users[$u]['value']."</i> from $heading <i>".$owners[$r]['value']."</i>.",
											'old'=>"true",
											'new'=>"false",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					} else {
						$result = array("error","An error occurred while trying to deactivate the administrator.  Please try again.");
					}
				}
			}
			break;
		case "RESTORE":
			$i = $_REQUEST['i'];
			if(!checkIntRef($i)) {
				$result = array("error","An error occurred while trying to restore the administrator.  Please try again.");
			} else {
				$sql = "SELECT a.*, CONCAT_WS(' ',t.tkname,t.tksurname) as value FROM ".$dbref."_user_admins a
						INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = a.tkid AND t.tkstatus = 1
						WHERE active = false AND type = '$type' AND a.id = $i
						ORDER BY a.ref, t.tkname, t.tksurname";
				$adm = mysql_fetch_all($sql);
				if(count($adm)==0) {
					$result = array("info","Administrator could not be found.");
				} else {
					$sql = "UPDATE ".$dbref."_user_admins SET active = true WHERE id = ".$i;
					$mar = db_update($sql);
					if($mar > 0) {
						$u = $adm[0]['tkid'];
						$r = $adm[0]['ref'];
						$result = array("ok","Administrator '".$adm[0]['value']."' has been restored to $heading '".$owners[$r]['value']."'.");
						$admins[$r][$u]['active'] = true;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$r,
											'field'=>"active",
											'text'=>"Restored ".$log_title." <i>".$users[$u]['value']."</i> to $heading <i>".$owners[$r]['value']."</i>.",
											'old'=>"false",
											'new'=>"true",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					} else {
						$result = array("error","An error occurred while trying to restore the administrator.  Please try again.");
					}
				}
			}
			break;
	}
}


displayResult($result);
if(count($owners)>0) {
?>
<p>These administrators receive Update only access to the KPIs associated with each <?php echo $heading; ?>.  Edit/Create/Approve access can only be granted per Directorate/Sub-Directorate.</p>
<table>
	<tr>
		<th>Ref</th><th><?php echo $heading; ?></th><th>Administrators</th>
	</tr>
	<?php 
	foreach($owners as $d) {
		echo "<tr>";
			echo "<th>".$d['id']."</th>";
			echo "<td valign=top><label id=own-".$d['id']."-value>".decode($d['value'])."</label></td>";
			echo "<td style=\"padding: 0 0 0 0;\"><table width=100% class=noborder style=\"background-color: #FFFFFF;\">";
			if(isset($admins[$d['id']])) {
				foreach($admins[$d['id']] as $s) {
					echo "<tr id=hover2>";
					echo "<th class=\"th2 noborder\" ".(!$s['active'] ? "style=\"background-color: #777777;\"" : "")." width=30>".$s['id']."</th>";
					echo "<td class=\"noborder ".(!$s['active'] ? "inact" : "")."\"><label id=admin-".$s['id']."-value>".decode($s['value'])."</label></td>";
					echo "<td class=\"right noborder ".(!$s['active'] ? "inact" : "")."\">&nbsp;&nbsp;&nbsp;";
					if($s['active'] && $d['active']) {
						echo "<input type=button value=Deactivate class=deact id=".$s['id']." name=".$d['id']." />";
					} elseif(!$s['active']) {
						echo "<input type=button value=Restore class=restore id=".$s['id']." name=".$d['id']." />";
					}
				}
			}
			echo "<tr id=hover2><td class=\"noborder\" width=30></td><td colspan=2 class=\"noborder right\"><input type=button value=Add class=add id=".$d['id']." /></td></tr></table></td>";
		echo "</tr>";
	}
	?>
</table>
<div id=add title="Add">
	<form id=frm_add method=post action=<?php $self; ?>>
	<input type=hidden name=action value=ADD /><input type=hidden name=page_id value=<?php echo $page_id; ?> />
	<table width=100%>
		<tr>
			<th class=left><?php echo $heading; ?>:</th>
			<td><input type=hidden name=i value="" id=id /><label id=val></label></td>
		</tr>
		<tr>
			<th class=left>Administrator:</th>
			<td><select name=user id=u><option selected value=X>--- SELECT ---</option>
			<?php 
			foreach($users as $u) {
				echo "<option value=".$u['id'].">".$u['value']."</option>";
			}
			?>
			</select></td>
		</tr>
		<tr>
			<th class=left>&nbsp;</th>
			<td><input type=button value="Save Changes" class=isubmit id=add-submit /></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=add-cancel /></p>
	</form>
</div>
<script type=text/javascript>
$(function() {
	$("#add").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true,
		close: function() {
			$("#id").val("");
			$("#val").html("");
			$("#u").val("X");
		}
	});
	$(".add").click(function() {
		var id = $(this).prop("id");
		var fld = "#own-"+id+"-value";
		$("#id").val(id);
		$("#val").html($(fld).html());
		$("#add").dialog("open").dialog("option","position", {my:'center',at:'top',of:document});
	});
	$("#add-submit").click(function() {
		if($("#u").val().length==0 || $("#u").val()=="X") {
			alert("Please select an administrator from the drop down list\n or else click the \"Cancel\" button to close.");
		} else {
			$("#frm_add").submit();
		}
	});
	$("#add-cancel").click(function() {
		$("#add").dialog("close");
	});
	$(".deact").click(function() {
		var id = $(this).prop("id");
		var val = html_entity_decode($("#admin-"+id+"-value").html(),"ENT_QUOTES");
		var o = $(this).prop("name");
		var own = html_entity_decode($("#own-"+o+"-value").html(),"ENT_QUOTES");
		if(confirm("Are you sure that you wish to DEACTIVATE administrator '"+val+"' from <?php echo $heading; ?> '"+own+"'?")) {
			document.location.href = '<?php echo $self; ?>?t=<?php echo $page_type; ?>&page_id=owner&action=DEACTIVATE&i='+id;
		}
	});
	$(".restore").click(function() {
		var id = $(this).prop("id");
		var val = html_entity_decode($("#admin-"+id+"-value").html(),"ENT_QUOTES");
		var o = $(this).prop("name");
		var own = html_entity_decode($("#own-"+o+"-value").html(),"ENT_QUOTES");
		if(confirm("Are you sure that you wish to RESTORE administrator '"+val+"' to <?php echo $heading; ?> '"+own+"'?")) {
			document.location.href = '<?php echo $self; ?>?t=<?php echo $page_type; ?>&page_id=owner&action=RESTORE&i='+id;
		}
	});
});
</script>
<?php


} else {
	echo "<p>There are no active ".$heading." to display.</p>";
}


goBack("setup.php","Back to Setup");
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayAuditLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));

?>