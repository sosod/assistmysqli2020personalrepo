<?php 
$get_lists = false;
$section = "TOP";
$page_title = "Detail";
$obj_id = $_REQUEST['id'];
include("inc/header.php"); 
$lists['years'] = getList("years","id","true");
//arrPrint($mheadings[$section]);
//arrPrint($_REQUEST);

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
		case "TEXTLIST":
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			break;
		default: $fields[] = "t.".$fld;
	}
}

$object_sql = "SELECT targettype.id as tt, calctype.code as ct, ".implode(", ",$fields)."
FROM ".$dbref."_top t
INNER JOIN ".$dbref."_dir as dir ON t.top_dirid = dir.id";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="dir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE t.top_id = ".$obj_id;
//echo "<P>".$object_sql;
$object = mysql_fetch_one($object_sql);
//$object = $object[0];


$extras = array();
//WARDS
$fld = "top_wards";
$tbl = "wards";
$a = "w";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.t".$a."_listid AND b.t".$a."_active = true AND b.t".$a."_topid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_all($a_sql);
//AREAS
$fld = "top_area";
$tbl = "area";
$a = "a";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.t".$a."_listid AND b.t".$a."_active = true AND b.t".$a."_topid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_all($a_sql);
//REPCATE
$fld = "top_repcate";
$tbl = "repcate";
/*******************???????????????????????????????*********************/
$extras[$fld] = $me_lists->getAllListItems($tbl);

//FORECAST
$f_sql = "SELECT * FROM ".$dbref."_top_forecast WHERE tf_topid = ".$obj_id." AND tf_active = true";
$forecast = mysql_fetch_alls($f_sql,"tf_listid");



$result_sql = "SELECT * FROM ".$dbref."_top_results WHERE tr_topid = ".$obj_id." ORDER BY tr_timeid";
$results = mysql_fetch_alls($result_sql,"tr_timeid");


include("view_table_detail.php");

$log_sql = "SELECT tlog_date, CONCAT_WS(' ',tkname,tksurname) as tlog_tkname, tlog_transaction FROM ".$dbref."_top_log 
			INNER JOIN assist_".$cmpcode."_timekeep ON tlog_tkid = tkid 
			WHERE tlog_topid = '".$obj_id."' AND tlog_yn = 'Y' ORDER BY tlog_id DESC";
			//echo $log_sql;
displayAuditLog($log_sql,array('date'=>"tlog_date",'user'=>"tlog_tkname",'action'=>"tlog_transaction"));
//displayAuditLog($log_sql,array('date'=>"klog_date",'user'=>"klog_tkname",'action'=>"klog_transaction"));

?>