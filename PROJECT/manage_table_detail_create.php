<?php
function maxlenOption($v) {
	global $select_option_maxlen;
	if(strlen($v)>$select_option_maxlen) {
		$v = substr($v,0,($select_option_maxlen-3))."...";
	}
	return $v;
}
$my_head = $mheadings[$section];
$result_head = $mheadings[$r_section];

$time = getTime($get_time,false);

//arrPrint($_REQUEST);

/** ACTION **/
if(isset($_REQUEST['act']) && $_REQUEST['act']=="CREATE") {
	echo "<p id=p>Processing new KPI...</p>";
	//echo "<table><tr><th>Fld</th><th>Type</th><th>New</th><th>Action</th></tr>";
	$extras = array();
	$updates = array();
	$trans = array();
	$changes = array();
	$sql = array();
	$changes[] = $table_fld."active = true";
	//echo "<ol>";
	foreach($mheadings[$section] as $fld => $h) {
		$log_trans = "";
		switch($h['h_type']) {
			case "TOP":
				$new = "Do nothing";
				$old = "Do nothing";
				$log_trans = "";
				$changes[] = "$fld = 0";
				break;
			case "TEXTLIST":
				if(isset($_REQUEST[$fld]) && is_array($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
					$new = ";".implode(";",$_REQUEST[$fld]).";"; 
					$n = $_REQUEST[$fld];
					$nw = array();
					foreach($n as $i) {
						//echo "==".$i;
						$nw[] = $lists[$h['h_table']][$i]['value'];
					}
					if(count($nw)>0) {
						$new_text = implode("; ",$nw);
					} else {
						$new = ";0;";
						$new_text = $unspecified;
					}
				} else {
					$new = ";0;";
					$new_text = $unspecified;
				}
	//			$old = isset($object['o'.$fld]) ? $object['o'.$fld] : ";0;";
				if(isset($object['o'.$fld]) && strlen($object['o'.$fld])>0 && $object['o'.$fld]!=";0;") { //&& is_array($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
					$old = $object['o'.$fld];
					$n = explode(";",$old);
					$nw = array();
					if(!in_array("0",$n)) {
						foreach($n as $i) {
							if(strlen($i)>0) {
								$nw[] = $lists[$h['h_table']][$i]['value'];
							}
						}
					}
					if(count($nw)>0) {
						$old_text = implode("; ",$nw);
					} else {
						$old = ";0;";
						$old_text = $unspecified;
					}
				} else {
					$old = ";0;";
					$old_text = $unspecified;
				}
				if($new!=$old) {
					$log_trans = addslashes("Updated ".$h['h_client']." to '".$old_text."' from '".$new_text."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
			case "WARDS":
			case "AREA":
				$s = array();
				$added = array();
				$log_trans = "";
				$n = array();
				foreach($_REQUEST[$fld] as $i) {
					$n[] = $lists[$h['h_table']][$i]['value']." => ".(!isset($extras[$fld][$i]) ? "NEW" : "");
					if(!isset($extras[$fld][$i])) {
						$log_trans.="<br />- \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$lists[$h['h_table']][$i]['value']."\'";
						$added[] = $i;
					}
				}
				$new = implode("<br />",$n);
				if(strlen($log_trans)>0) {
					$log_trans = "Added ".$h['h_client'].":".$log_trans;
					$extras[$fld] = array('new'=>$added,'lt'=>$log_trans);
				}
				break;
			case "CAP":
			case "LIST":
				$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
					if(in_array($h['h_type'],array("CAP"))) {
						$log_trans = addslashes("Added link to ".$h['h_client']." '".$id_labels_all[$h['h_type']].$new."'");
					} elseif(isset($lists[$h['h_table']][$new])) {
						$log_trans = addslashes("Set ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."'");
					} else {
						$log_trans = $unspecified;
					}
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = 0;
					$changes[] = $fld." = '".$new."'";
				break;
			default:
				$new = isset($_REQUEST[$fld]) ? code($_REQUEST[$fld]) : "";
					$log_trans = addslashes("Set ".$h['h_client']." to '".$new."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = "";
					$changes[] = $fld." = '".$new."'";
				break;
		}
		/*echo "<tr>
		<th>".$fld."</th>
		<td>".$h['h_type']."</td>
		<td>".$new."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	//create KPI
	$sb = "INSERT INTO ".$dbref."_".$table_tbl." SET ".implode(", ",$changes)."";
	$obj_id = db_insert($sb);
	foreach($trans as $fld => $lt) {
		$v = array(
			'fld'=>$fld,
			'timeid'=>0,
			'text'=>$lt['lt'],
			'old'=>$lt['o'],
			'new'=>$lt['n'],
			'act'=>"C",
			'YN'=>"N"
		);
		logChanges($section,$obj_id,$v,code($sb));
	}
	foreach($extras as $fld => $e) {
		$added = $e['new'];
		$log_trans = $e['lt'];
		$h = $mheadings[$section][$fld];
		if(count($added)>0) {
						$sa = "INSERT INTO ".$dbref."_".$fld." (".$wa_tbl_fld[$fld]."_".$table_id."id, ".$wa_tbl_fld[$fld]."_listid, ".$wa_tbl_fld[$fld]."_active) VALUES (".$obj_id.",".implode(",true),(".$obj_id.",",$added).",true)";
						db_insert($sa);
						$s[] = $sa;
					}
					$v = array(
						'fld'=>$fld,
						'timeid'=>0,
						'text'=>$log_trans,
						'old'=>"",
						'new'=>implode(",",$added),
						'act'=>"C",
						'YN'=>"N"
					);
					logChanges($section,$obj_id,$v,code(implode(chr(10),$s)));
	}
	$obj_tt = $_REQUEST[$table_fld.'targettype'];
	$rheadings = $mheadings[$r_section];
	if(isset($rheadings['tr_dept_attachment'])) { unset($rheadings['tr_dept_attachment']); }
	if(isset($rheadings['tr_dept_correct'])) { unset($rheadings['tr_dept_correct']); }
	if(isset($rheadings['tr_dept'])) { unset($rheadings['tr_dept']); }
	foreach($_REQUEST['ti'] as $ti) {
		$trans = array();
		$result_changes = array();
		if($section=="TOP") {
			$result_changes[] = "tr_auto = 1";
		}
		$log_trans = "";
			$fld = $fld_target;
			$new = $_REQUEST[$fld.'_'.$ti];
			$old = 0;
				$log_trans = "Created Target for ".$time[$ti]['display_full']." to \'".KPIresultDisplay($new,$obj_tt)."\'";
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$result_changes[] = "$fld = $new";
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Target</td>
			<td>".$new."</td>
			<th>".$log_trans."</th>
			</tr>";*/
		$log_trans = "";
		foreach($rheadings as $fld => $h) {
			if($fld!=$fld_target) {
				switch($h['h_type']) {
				case "NUM": $result_changes[] = "$fld = 0"; break;
				case "TEXT":
				default:
					$result_changes[] = "$fld = ''";
					break;
				}
			}
		}
			$sc = "INSERT INTO ".$dbref."_".$table_tbl."_results SET ".implode(", ",$result_changes).", ".$r_table_fld.$table_id."id = ".$obj_id.", ".$r_table_fld."timeid = ".$ti;
			db_insert($sc);
			foreach($trans as $fld => $lt) {
				$v = array(
					'fld'=>$fld_target,
					'timeid'=>$ti,
					'text'=>$lt['lt'],
					'old'=>$lt['o'],
					'new'=>$lt['n'],
					'act'=>"C",
					'YN'=>"N"
				);
				logChanges($section,$obj_id,$v,code($sc));
			}
	}
	if($section=="TOP") {
		$log_trans = "";
		$added = array();
		$s = array();
		foreach($_REQUEST['yi'] as $i) {
			$new = $_REQUEST['years_'.$i];
			$new = checkIntRef($new) ? $new : 0;
			$added[$i] = array("new"=>$new,"id"=>$i,"sql"=>"(null,".$obj_id.",$i,1,$new)");
			$log_trans.= "<br />Set ".$new." as the target for ".$lists['years'][$i]['value'];
			$s[] = $added[$i]['sql'];
		}
		$sd = "INSERT INTO ".$dbref."_top_forecast (tf_id,tf_topid,tf_listid,tf_active,tf_value) VALUES ".implode(",",$s);
		db_insert($sd);
				$v = array(
					'fld'=>"forecast",
					'timeid'=>0,
					'text'=>"Added Forecast Annual Targets:".$log_trans,
					'old'=>'',
					'new'=>'',
					'act'=>"C",
					'YN'=>"N"
				);
				logChanges($section,$obj_id,$v,code($sd));
	}
	//echo "</table>";
				$v = array(
					'fld'=>'',
					'timeid'=>0,
					'text'=>"KPI created.",
					'old'=>'',
					'new'=>'',
					'act'=>"C",
					'YN'=>"Y"
				);
				logChanges($section,$obj_id,$v,code($sb));
	if(!$import_status[$section]) {
		db_update("UPDATE ".$dbref."_import_status SET status = 1 WHERE value = '$section'");
		$import_status[$section] = 1;
	}
	displayResult(array("ok","KPI ".$id_labels_all[$section].$obj_id." has been successfully created."));
}

if(isset($_REQUEST['goto_plc']) && $_REQUEST['goto_plc']=="Y") {
	echo "	<script type=text/javascript>
				document.location.href = '".$base[0]."_".$base[1]."_plc.php?page_id=create&id=".$obj_id."&r[]=ok&r[]=KPI+".$id_labels_all[$section].$obj_id."+has+been+successfully+created.';
			</script>";
} else {
	echo "<script type=text/javascript>
			$(\"#p\").hide();
			</script>";
}

//echo "<div style='width:200px; float: right;'>"; 
ASSIST_HELPER::displayResult(array("info","** indicates required fields.")); 
//echo "</div>";

?>
<form id=frm_edit method=post action=<?php echo $self; ?> style="margin: 0 0 0 0;">
<input type=hidden name=page_id value="<?php echo $page_id; ?>" />
<input type=hidden name=act value="CREATE" />
<table class=noborder>
	<tr class=no-highlight>
		<td class=noborder>
<h2 style="margin-top: -0;">Details</h2>
<table id=tbl>
	<tr>
		<th class=left>Reference:</th>
		<td>TBD</td>
	</tr>
<?php 
foreach($my_head as $fld => $h) {
	echo chr(10)."<tr>";
		echo "<th class=\"left top\">".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;".($h['i_required']+$h['c_required']>0 ? "**<input type=hidden id=".$fld."-r value=Y />" : "<input type=hidden id=".$fld."-r value=N />")."</th>";
	switch($h['h_type']) {
		case "CAP":
			echo "<td>";
				if(isset($object[$fld]) && $object[$fld]>0) {
					echo "<label for=".$fld." id=".$fld."_disp>".$object[$fld.'_disp']."</label><input type=hidden name=".$fld." id=".$fld." value=".$object[$fld]." /><span class=float><input type=button value=\"Remove Link\" class=idelete id=del_cap />";
				} else {
					echo "<select name=$fld id=$fld>";
						echo "<option ".(!isset($object[$fld]) || $object[$fld]==0 ? "selected" : "")." value=0>N/A</option>";
						$other_sql = "SELECT cap_id as id, cap_name as value FROM ".$dbref."_capital WHERE cap_active = true AND cap_id NOT IN (SELECT kpi_capitalid FROM ".$dbref."_kpi WHERE kpi_capitalid > 0 AND kpi_active = true)";
						$others = mysql_fetch_alls($other_sql,"id");
						foreach($others as $i => $o) {
							$v = maxlenOption(decode($o['value']));
							echo "<option ".($object[$fld]==$i ? "selected" : "")." value=".$i.">[".$id_labels_all[$h['h_type']].$i."] ".$v."</option>";
						}
					echo "</select>";
				}
			echo "</td>";
			break;
		case "TOP":
			echo "<td>N/A</td>";
			break;
		case "WARDS":
		case "AREA":
		case "TEXTLIST":
		case "FUNDSRC":
			if($h['h_type']=="TEXTLIST") {
				$values = array();
			} else {
				$values = $extras[$fld];
			}
			//foreach($values as $v) {
				//$val[] = ((strlen($v['code'])>0 && strtolower($v['value'])!="all") ? decode($v['code']).": " : "").maxlenOption(decode($v['value']));
			//}
			echo "<td>";
			//arrPrint($values);
			//arrPrint($lists[$h['h_table']]);
				//echo implode("<br />",$val);
			if(count($lists[$h['h_table']])>1) {
				echo "<select name=".$fld."[] id=$fld multiple ".(count($lists[$h['h_table']])<=10 ? "size=".count($lists[$h['h_table']]) : "size=10").">";
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<option ".(isset($values[$i]) ? "selected " : "")." value=$i full=\"".addslashes((isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").(decode($l['value'])))."\">".(isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").maxlenOption(decode($l['value']))."</option>";
					}
				echo "</select>";
				echo "<br /><span class=\"ctrlclick iinform\">CTRL + Click to select multiple options</span>
				<div id=div_".$fld." class='display_me ui-state-ok' style='width: 500px;margin: 5px;'></div>";
			} else {
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<input type=hidden name=".$fld."[] id=$fld value=$i>".(isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").(decode($l['value']))."";
					}
			}
			echo "</td>";
			break;
		case "DATE":
			//echo "<td>".($object[$fld]>0 ? date("d M Y",$object[$fld]) : "")."</td>";
			echo "<td><input type=text class=datepr value=\"".($object[$fld]>0 ? date("Y-m-d",$object[$fld]) : "")."\" name=".$fld." id=".$fld." /> ".(strpos($fld,"_plan")>0 ? "" : "<input type=button value=Clear class=cleardate id=c-".$fld." />")."</td>";
			break;
		case "LIST":
			echo "<td>";
			echo "<input type=hidden id=lbl-".$fld."-v value=\"".$object['o'.$fld]."\" />
					<label for=".$fld." id=lbl-".$fld."-text>".$object[$fld]."</label>
					<span id=lbl-".$fld."-lb></span>";
				echo "<select id=".$fld." name=".$fld."><option value=0>--- SELECT ---</option>";
				if($h['h_table']=="subdir") {
						foreach($lists['dir'] as $di => $dl) {
							$d = $dl['value'];
							foreach($lists[$h['h_table']]['dir'][$di] as $i => $l) {
								if( (($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") || (isset($my_access['manage']['DIR'][$di]) && $my_access['manage']['DIR'][$di]['act_create']==true) || (isset($my_access['manage']['SUB'][$i]) && $my_access['manage']['SUB'][$i]['act_create']==true)) {
									echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i.">".maxlenOption(decode($d." - ".$l['value']))."</option>";
								}
							}
						}
				} elseif($h['h_table']=="dir" && $base[1]=="top") {
					foreach($lists['dir'] as $i => $l) {
						if(isset($my_access['manage']['TOP'][$i]) && $my_access['manage']['TOP'][$i]['act_create']==true) {
							echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i." full=\"".addslashes((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."\">".maxlenOption((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."</option>";
						}
					}
				} else {
						foreach($lists[$h['h_table']] as $i => $l) {
							echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i." full=\"".addslashes((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."\">".maxlenOption((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."</option>";
						}
				}
				echo "</select>
				<div id=div_".$fld." class='display_me ui-state-ok' style='width: 500px;margin: 5px;'></div>";
			echo "</td>";
			break;
		case "BOOL":
			echo "<td>";
				echo "<input type=hidden id=lbl-".$fld."-v value=\"".$object[$fld]."\" /><label for=".$fld." id=lbl-".$fld."-text>".$object[$fld]."</label><span id=lbl-".$fld."-lb></span>";
				echo "<select id=".$fld." name=".$fld."><option ".($object[$fld]!=0 ? "selected" : "")." value=1>Yes</option><option ".(!$object[$fld] ? "selected" : "")." value=0>No</option>";
				echo "</select>";
			echo "</td>";
			break;
		default:
			switch($fld) {
				case "top_annual":
				case "kpi_annual":
					echo "<td>".KPIresultEdit($object[$fld],3,$fld,true)."</td>";
					break;
				case "top_revised":
				case "kpi_revised":
					echo "<td class=revised><input type=hidden name=".$fld." class=revised value='' /></td>";
					break;
				default:
					echo "<td>";
					if($h['c_maxlen']<=0 || $h['c_maxlen']>90) {
						echo "<textarea rows=4 cols=60 name=$fld id=$fld>".decode($object[$fld])."</textarea>";
						if($h['c_maxlen']>0) {
							echo "<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
						}
					} else {
						echo "<input type=text id=".$fld." name=".$fld." value=\"".$object[$fld]."\" size=".($h['c_maxlen']>90 ? "90" : $h['c_maxlen']+5)." maxlength=".$h['c_maxlen']." /> 
							<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
					}
					echo "</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
 ?>
</table>
<?php displayGoBack("",""); ?>
<?php 
switch($section) {
	case "KPI":
		echo "<h2>Results</h2>
				<table id=tblres>";
			echo "<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>
				<th>Period Performance</th>";
			echo "</tr>";
			echo "<tr>";
				echo "<th width=110>".$result_head[$fld_target]['h_client']."</th>";
			echo "</tr>";
		foreach($time as $ti => $t) {
			echo "<tr>";
				echo "<th class=left width=150>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
				echo "<td class=\"right\">".KPIresultEdit($r[$i]['target'],3,$fld_target."_".$ti)."&nbsp;</td>";
			echo "</tr>";
		}
		echo "</table>";
		break;
	case "TOP": 
		unset($time[1]); unset($time[4]); unset($time[7]); unset($time[10]);
		echo "<table class=noborder><tr><td class=noborder style=\"padding-right: 20px\">
				<h2>Results</h2>
				<table id=tblres>";
			echo "<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>
				<th>Period Performance</th>";
			echo "</tr>";
			echo "<tr>";
				echo "<th width=110>".$result_head[$fld_target]['h_client']."</th>";
			echo "</tr>";
		foreach($time as $ti => $t) {
			echo "<tr>";
				echo "<th class=left width=150>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
				echo "<td class=\"right\">".KPIresultEdit(0,3,$fld_target."_".$ti)."&nbsp;</td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "</td><td class=noborder style=\"padding-left: 20px\">";
		echo "<h2>Forecast Annual Target</h2>
				<table id=tblfore>";
			foreach($lists['years'] as $i => $l) {
				if($i>1) {
					echo "<tr>
							<th>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
							<td>".KPIresultEdit(0,3,"years_".$i)."&nbsp;</td>
						</tr>";
				}
			}			
		echo "</table>";
		echo "</td></tr></table>";
		break;
}
?>
<?php displayGoBack("",""); ?>
	<table width=100% id=ftbl><tr class=no-highlight><td class=center>
		<input type=hidden value=N id=goto_plc name=goto_plc />
		<input type=submit value="Save Changes" class=isubmit /> 
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?><span id=plc><input type=button value="Save Changes & Go To Project Life Cycle" id=trigger_plc class=isubmit /></span><?php } ?> <input type=reset /> 
		<?php if( isset($my_action_is_not_create) && ($my_access['access']['module'] || $my_access['access']['kpi']) && $base[1]=="admin") { ?><span class=float><input type=button value=Delete class=idelete id=delete_kpi /><?php } ?></span>
	</td></tr></table>
</td></tr></table>
</form>
<p>&nbsp;</p>
<script type=text/javascript>
	$(document).ready(function() {
		$("#tblres input, #tblres textarea").hover(
			function() { $(this).css("background-color","#DDFFDD"); },
			function() { $(this).css("background-color",""); }
		);
		$("#tblres input, #tblres textarea").focus(
			function() { $(this).css("background-color","#DDFFDD"); }
		);
		$("input:text.annual_revised").blur(function() {
			$("td.revised").html($(this).val());
			$("input:hidden.revised").val($(this).val());
		});
		$("#tblres input, #tblres textarea").blur(
			function() { $(this).css("background-color",""); }
		);
		$("tr").unbind('mouseenter mouseleave');
		$(".datepr").datepicker({
			onClose: function() {
				i = $(this).attr("id");
				v = $(this).val();
				req = $("#"+i+"-r").val();
				if(req=="Y") {
					if(v.length==0) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				}
			},
			showOn: 'both',
			buttonImage: '../library/jquery/css/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd',
			changeMonth:true,
			changeYear:true		
		});
		$(".cleardate").click(function() {
			i = $(this).attr("id");
			i = i.substring(2,i.length);
			$("#"+i).val("");
		});
	});
	$(function () {
		var head = [];
		<?php foreach($my_head as $fld => $h) { echo chr(10)." head['".$fld."'] = '".addslashes(decode($h['h_client']))."';";	} ?>
		var mytime = [];
		<?php foreach($time as $ti => $t) { echo chr(10)." mytime['".$ti."'] = '".addslashes(decode($t['display_full']))."';";	} ?>
		
	<?php if($section=="TOP") { ?>
		var myyears = [];
		<?php foreach($lists['years'] as $i => $l) { echo chr(10)." myyears['".$i."'] = '".addslashes(decode($l['value']))."';";	} ?>
	<?php } ?>
		
		
		/* * ON DOCUMENT LOAD * */
		$("div.display_me").hide();
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?> 
				var ct = $("#tbl #kpi_calctype").val();
				var tt = $("#tbl #kpi_targettype").val();
				if(ct=="CO" && tt=="2") {
					$("#ftbl #plc").show();
				} else {
					$("#ftbl #plc").hide();
				}
		<?php } ?>
		$("#tbl select").each(function() {
			$(this).trigger("change");
		});
		$("#tbl textarea, #tbl input:text").each(function() {
			i = $(this).prop("id");
			if(i.indexOf("-")<0) {
				$(this).trigger("keyup");
			}
		});
		
		
		/* * ON FORM EDIT * */
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?> 
		$("#tbl #kpi_calctype, #tbl #kpi_targettype").change(function() {
			var ct = $("#tbl #kpi_calctype").val();
			var tt = $("#tbl #kpi_targettype").val();
			if(ct=="CO" && tt=="2") {
				$("#ftbl #plc").show();
			} else {
				$("#ftbl #plc").hide();
			}
		});
		<?php } ?>
		$("#tbl textarea, #tbl input").keyup(function() {
			//get details
			v = $(this).val();
			i = $(this).prop("id");
			//check maxlength limit
			var mfld = "#"+i+"-max";
			if($(mfld).length>0) {
			//if(i!="kpi_value" && i!="top_value") {
				m = $(mfld).val();
				rem = m - v.length;
				if(rem>0) {
					$("#"+i+"-c").text(rem);
					//$("#"+i+"-c").css({"color":"#000000","font-weight":"normal","background-color":"#FFFFFF"});
					if(rem>=10) {
						$("#"+i+"-c").css({"color":"#000000","font-weight":"normal","background-color":"#FFFFFF","padding-left":"0px"});
					} else {
						$("#"+i+"-c").css({"color":"#000000","font-weight":"bold","background-color":"#FE9900","padding-left":"6px"});
					}
				} else {
					$("#"+i+"-c").text("0");
						if($(this).get(0).tagName=="textarea")
							$(this).text(v.substring(0,m));
						else
							$(this).val(v.substring(0,m));
					//$("#"+i+"-c").css({"color":"#FFFFFF","font-weight":"bold","background-color":"#CC0001"});
					$("#"+i+"-c").css({"color":"#FFFFFF","font-weight":"bold","background-color":"#CC0001","padding-left":"6px"});
				}
			}
			//check required
			req = $("#"+i+"-r").val();
			if(req=="Y") {
				if(v.length==0) {
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			}
		});
		$("#tbl select").change(function() {
			i = $(this).prop("id");
			if(!$(this).val() || ($(this).val()=="0" && i!="kpi_topid" && i!="kpi_capitalid" && i!="top_repkpi")) {
				$(this).addClass("required");
				$("#div_"+i).text('').hide();
			} else {
				$(this).removeClass("required");
				if(!$(this).prop("multiple")) {
					var alt = $(this).find(":selected").attr("full");
					alt = stripslashes(alt);
					if(alt!="undefined" && alt!=$(this).find(":selected").text()) {
						$("#div_"+i).show().html('<p>'+alt+'</p>');
					} else {
						$("#div_"+i).text('').hide();
					}
				} else {
					var $opt = $(this).find(":selected");
					var txt = "";
					var alt = "";
					$opt.each(function() {
						alt = stripslashes($(this).attr("full"));
						if(alt!="undefined") {
							if(alt.length>0 && txt.length>0) { 
								txt+="<br />"; 
							}
							txt+="+ "+alt;
						}
					});
					$("#div_"+i).show().html('<p>'+txt+'</p>');
				}
			}
		});

		$("#tblres input").blur(function() {
			v = $(this).val();
			if(v.length>0) {
				valid8b = validateActual(v);
				if(!valid8b) {
				//if(isNaN(parseFloat(v)) || !isFinite(v)) {
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			} else {
				$(this).val("0");
			}
		});
		<?php if($section=="TOP") { ?>
			$("#tblfore input").blur(function() {
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
		<?php } ?>
		
		
		/* * SUBMIT FORM * */
		$("#trigger_plc").click(function() {
			if(confirm("Moving to the Project Life Cycle page will save the changes you have made to this KPI.\n\nDo you wish to continue?")==true) {
				$("#goto_plc").val("Y");
				$("form").trigger("submit");
			}
		});
		$("form").submit(function() {
			var err = "";
			$("#tbl input, #tbl select, #tbl textarea").each(function() {
				i = $(this).prop("id");
				if(i=="obj_id") {
				} else if(i.indexOf("-")<0) {
					req = $("#"+i+"-r").val();
					if(req=="Y") {
						ok = true;
						if(i!="kpi_topid" && i!="kpi_capitalid" && i!="top_repkpi") {
							if(!($(this).val())) 																				ok = false;
							else if((i=="kpi_calctype" || i=="top_calctype") && (parseInt($(this).val())==0)) 					ok = false;
							else if( $(this).get(0).tagName=="SELECT" && i!="kpi_calctype" && i!="top_calctype" && i!="top_repkpi" && !(parseInt($(this).val())>0) )	ok = false;
							if(!ok) err+="\n - "+head[i];
						}
					}
				}
			});
			err_res = "";
			//$(".valid8me").each(function() {
			$("#tblres input").each(function() {
				i = $(this).prop("name");
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
					//if(isNaN(parseFloat(v)) || !isFinite(v)) {
						if(i.substring(3,9)=="target") { n = "Target"; } else { n = "Actual"; }
						ti = i.substring(10,12);
						n+=" for "+mytime[ti];
						err_res+="\n - "+n;
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
			err_fore = "";
		<?php if($section=="TOP") { ?>
			$("#tblfore input").each(function() {
				i = $(this).prop("name");
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
					//if(isNaN(parseFloat(v)) || !isFinite(v)) {
						n = "Forecast Target";
						yi = i.substring(6,8);
						n+=" for "+myyears[yi];
						err_fore+="\n - "+n;
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
		<?php } ?>
			if(err.length>0 || err_res.length>0 || err_fore.length>0) {
				display = "";
				if(err.length>0) display = "The following required fields have not been completed:"+err;
				if(err.length>0 && err_res.length>0) display+="\n\n";
				if(err_res.length>0) display+="The following time periods have invalid values:"+err_res;
				if(err.length>0 && err_fore.length>0) display+="\n\n";
				if(err_fore.length>0) display+="The following forecast years have invalid values:"+err_fore;
				alert(display);
				$("#goto_plc").val("N");
				return false;
			} else {
				return true;
			}
		});
		$("#del_cap").click(function() {
			if(confirm("Are you sure you wish to remove the link between this KPI and the related Capital Project?")==true) {
				$("#tbl #kpi_capitalid").val(0);
				$("#tbl #kpi_capitalid_disp").css("color","#999999");
				$("#tbl #del_cap").prop("disabled",true);
				alert("Please click the 'Save Changes' button at the bottom of the page to save this change.");
			}
		});
		
	});
</script>