<?php 
function drawStatus($s,$i) {
	return "&nbsp;<div style='width: 16px; float: right' id=d_".$i." class='ui-widget ui-state-".($s===true ? "ok" : "fail")."'><span  id=s_".$i." class='ui-icon ui-icon-".($s===true ? "check" : "closethick")."'>&nbsp;&nbsp;</span></div>";
}

$page_title = "Users";
$prev_module = array('modref'=>"SDP12",'modtext'=>"SDBIP 2012/2013");
if(!isset($_REQUEST['act'])) { $act = "PREVIEW"; } else { $act = $_REQUEST['act']; }
include("inc/header.php"); 

$output = "<style type=text/css>
	.YES { ".($act=="SAVE" ? "color: #009900; font-weight: bold;" : "background-color: #009900; color: #ffffff;")." }
	.NO { background-color: #ffffff; color: #cc0001; }
</style>";

$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > Users", $self);

	$tk_done = array();

$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$modref."' AND modyn = 'Y'";
$menu = mysql_fetch_one($sql);

$sql = "SELECT modid FROM assist_".$cmpcode."_menu_modules WHERE modmenuid = '".$menu['modid']."'";
$m = mysql_fetch_one($sql);
$menu['mm'] = $m['modid'];

$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$prev_module['modref']."' AND modyn = 'Y'";
$prev_module['menu'] = mysql_fetch_one($sql);










if(count($prev_module['menu'])>1) {
	$sql = "SELECT * FROM assist_".$cmpcode."_".strtolower($prev_module['modref'])."_user_access WHERE active = 1";
	$old_user_access = mysql_fetch_all_fld($sql,"tkid");

	$sql = "SELECT mmu.*, CONCAT_WS(' ',t.tkname,t.tksurname) as tkn
			FROM assist_".$cmpcode."_menu_modules_users mmu
			INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = mmu.usrtkid AND t.tkstatus = 1
			WHERE mmu.usrmodref = '".$prev_module['modref']."'
			ORDER BY t.tkname, t.tksurname
			";
	$old_menu_access = array();
	$old_menu_access = mysql_fetch_all_fld($sql,"usrtkid");
	$mnr = count($old_menu_access);
	
	/*$sql = "SELECT tkid, count(distinct CONCAT(type,ref)) as c FROM `assist_".$cmpcode."_".strtolower($prev_module['modref'])."_user_admins` WHERE active GROUP BY tkid";
	$old_admin_access = mysql_fetch_all_fld($sql,"tkid");*/
} else {
	$mnr = 0;
}

$access_headings = array(
	'module'	=> "Module Admin",
	'kpi'		=> "Dept KPI Admin",
	'finance'	=> "Finance Admin",
	'toplayer'	=> "Top Layer Admin",
	'second'	=> "Secondary Time",
	'setup'		=> "Setup",
);

function echoHead($extra = false) {
	global $access_headings;
	$echo = "<tr>
		<th>User</th>
		<th>Menu<br />Access</th>";
	foreach($access_headings as $fld => $h) {
		$echo.= "<th>".str_replace(' ','<br />',$h)."</th>";
	}
	if($extra) { $echo.="<th></th>"; }
	$echo.= "</tr>";
	return $echo;
}

if($act=="SAVE") {
	//arrPrint($_REQUEST);
	$t_sql1 = strtolower("INSERT INTO assist_".$cmpcode."_".$modref."_user_access (tkid, module, kpi, finance, toplayer, assurance, second, setup, active) VALUES ");
	$t_sql = array();
	$m_sql1 = strtolower("INSERT INTO assist_".$cmpcode."_menu_modules_users (usrmodid, usrtkid, usrmodref) VALUES ");
	$m_sql = array();
	$s_sql = array();
/*	foreach($_REQUEST['tkid'] as $t) {
		if(isset($_REQUEST['access'][$t]) && is_array($_REQUEST['access'][$t])) {
			$a = $_REQUEST['access'][$t];
			$t_sql[] = "('$t',".(isset($a['module']) ? $a['module'] : 0).", ".(isset($a['kpi']) ? $a['kpi'] : 0).", ".(isset($a['finance']) ? $a['finance'] : 0).", ".(isset($a['toplayer']) ? $a['toplayer'] : 0).", 0, ".(isset($a['second']) ? $a['second'] : 0).", ".(isset($a['setup']) ? $a['setup'] : 0).", 1)";
		}
		if(count($t_sql)>20) {
			echo $sql.implode(",",$t_sql);
			$t_sql = array();
		}
	}
	if(count($t_sql)>0) {
		echo $sql.implode(",",$t_sql);
		$t_sql = array();
	}*/
	$output.= "<p>Starting user access import from ".$prev_module['modtext']."</p><table>";
	echo $output;
	$output.= echoHead(true);
	$c = 0;
	
	foreach($old_menu_access as $i => $u) {
		$c++;
		if($c > 15) { $output.= echoHead(true); $c = 0; }
		if(in_array($i,$_REQUEST['tkid']) && isset($_REQUEST['access'][$i]) && is_array($_REQUEST['access'][$i])) {
			$a = $_REQUEST['access'][$i];
			if($a['module']==1) { foreach($access_headings as $fld => $h) { $a[$fld] = 1; } }
			$m_text = "<span class=YES>Yes</span>";
			$m = true;
			$m_sql[] = "(".$menu['mm'].",'$i','".strtoupper($modref)."')";
			$t_sql[] = "('$i',".(isset($a['module']) ? $a['module'] : 0).", ".(isset($a['kpi']) ? $a['kpi'] : 0).", ".(isset($a['finance']) ? $a['finance'] : 0).", ".(isset($a['toplayer']) ? $a['toplayer'] : 0).", 0, ".(isset($a['second']) ? $a['second'] : 0).", ".(isset($a['setup']) ? $a['setup'] : 0).", 1)";
		} else {
			$m_text = "<span class=NO>No</span>";
			$m = false;
			$a = array();
		}
		$output.= "<tr>
				<td>".$u['tkn']."</td>
				<td class=center> $m_text </td>";
		foreach($access_headings as $fld => $h) {
			$yn = isset($a[$fld]) && $a[$fld] == 1 ? 1 : 0;
			if(!$m) { $yn = "-"; }
			switch(strval($yn)) { case "1": $ynt = "Yes"; break; case "0": $ynt = "No"; break; default: $ynt = $yn; }
			$output.= "<td class=center>".($yn==1 ? "<span class=YES>$ynt</span>" : $ynt)."</td>";
		}
		$output.= "<td>".($m ? $m_sql[count($m_sql)-1]."<br />".$t_sql[count($t_sql)-1] : "-")."</td></tr>";
		if(count($t_sql)>20) {
			$s = $t_sql1.implode(",",$t_sql);
			db_insert($s);
			$s_sql[] = $s;
			//$output.="<tr><td colspan=8>".$s."</td></tr>";
			$s = $m_sql1.implode(",",$m_sql);
			db_insert($s);
			$s_sql[] = $s;
			//$output.="<tr><td colspan=8>".$s."</td></tr>";
			
			$t_sql = array();
			$m_sql = array();
			
		}

	}
		if(count($t_sql)>0) {
			$s = strtolower($t_sql1.implode(",",$t_sql));
			db_insert($s);
			$s_sql[] = $s;
			//$output.="<tr><td colspan=8>".$s."</td></tr>";
			$s = strtolower($m_sql1.implode(",",$m_sql));
			db_insert($s);
			$s_sql[] = $s;
			//$output.="<tr><td colspan=8>".$s."</td></tr>";
			
			$t_sql = array();
			$m_sql = array();
			
		}

	$output.= "</table>
	<p>User Access import completed.</p>";
	$sql = "UPDATE assist_".$cmpcode."_menu_modules SET moduserallowed = '-1', modusercount = '0' WHERE modid = ".$menu['mm'];
	db_update($sql);
	$output.= "<p>Module has been opened to allow for new users to be added.</p>";
	//echo $output;
	$output.= "<h3>Statements Run</h3><p>".implode("</p><p>",$s_sql)."</p>";
} else {
	$output.= "<form method=post action=support_import_users_SDP12.php><input type=hidden name=act value=SAVE />
	<table>";
	$output.= echoHead();
	$c = 0;
	foreach($old_menu_access as $i => $u) {
	$c++;
	if($c > 15) { $output.= echoHead(); $c = 0; }
		$output.= "<tr>
				<td>".$u['tkn']."</td>
				<td class=center><input type=checkbox value='$i' name=tkid[] id=$i checked /></td>";
		foreach($access_headings as $fld => $h) {
			$yn = isset($old_user_access[$i][$fld]) && $old_user_access[$i][$fld] == 1 ? 1 : 0;
			$output.= "<td class=center><select name=access[$i][$fld] class='$i $fld ".($yn==1 ? "YES" : "NO")."' alt='".$i."_".$fld."'><option value=1 ".($yn==1 ? "selected" : "")." class=YES>Yes</option><option value=0 ".($yn!=1 ? "selected" : "")." class=NO>No</option></select> </td>";//.drawStatus(($yn==1 ? true: false),"".$i."_".$fld."")."</td>";
		}
		$output.= "</tr>";
	}
	$output.= "</table>
	<p><input type=submit value=Save class=isubmit /></p></form>";
	echo $output;
}
	$save_echo_filename = "USERS_".date("YmdHis")."_".$act.".html";
	saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$output."</form></body></html>");
	logImport($import_log_file, "User Access > $act > ".$save_echo_filename, $self);
	if($act=="SAVE") { 
		mail("janet@igniteassist.co.za","SDBIP opened",$modref." has been opened for ".$cmpcode);
		echo "<script>document.location.href = 'support_import.php?r[]=ok&r[]=User+Access+Updated.';</script>"; 
	}

	
fclose($import_log_file);

?>
<script type=text/javascript>
$(function() {
	$("input:checkbox").click(function() {
		var i = $(this).prop("id");
		$("select."+i).prop("disabled",!$(this).prop("checked"));
		if($("select."+i+".module").val()==1) {
			var d = true;
			$("select."+i).each(function() {
				if(!$(this).hasClass("module")) {
					$(this).prop("disabled",d);
				}
			});
		}
	});
	function moduleSelect($me) {
		var i = $me.prop("class").substr(0,4);
		var v = $me.val();
		//alert(v);
		if(v==1) { d = "disabled"; } else { d = ""; }
		$("select."+i).each(function() {
			if(!$(this).hasClass("module")) {
				//alert("not module");
				$(this).prop("disabled",d);
			} else {
				//alert("mode");
			}
		});
	}
	$("select.module").each(function() {
		moduleSelect($(this));
	});
	$("select").change(function() {
		$me = $(this);
		var a = $me.attr("alt");
		//alert(a+" :: "+$me.val());
		if($me.val()==1) {
			$me.addClass("YES").removeClass("NO");
			//$("#d_"+a).removeClass("ui-state-fail").addClass("ui-state-ok");
			//$("#s_"+a).removeClass("ui-icon-closethick").addClass("ui-icon-check");
		} else {
			$me.addClass("NO").removeClass("YES");
			//$("#s_"+a).removeClass("ui-icon-check").addClass("ui-icon-closethick");
			//$("#d_"+a).removeClass("ui-state-ok").addClass("ui-state-fail");
		}
		if($me.hasClass("module")) {
			moduleSelect($(this));
		}
	});
});
</script>
</body>
</html>