<?php 
$section = $_REQUEST['kpi'];

$src = "home";

include("inc/header.php");

//$myObj->arrPrint($_REQUEST);

$obj_id = $_REQUEST['id'];
$time_id = $_REQUEST['time_id'];

$time = $timeObj->getTimeById($time_id);


$update_details = array();
			$update_details['data'] = $myObj->getUpdateFormDetails($obj_id); //$object->getSectionHead();//
			$a = $update_details['data']['results']['attach'][$time_id];
			$update_details['attach_display'] = $myObj->getObjectAttachmentDisplay(true,$a);
/*
$myObj->arrPrint($update_details);*/
			
 ?>
 
<h1>Update <?php echo $section=="KPI" ? "Departmental" : "Top Layer"; ?> KPI</h1>
<div id=div_update title="" >
	<div id=div_update_form style='background-color: #ffffff; width: 49%' class=float>
		<form name=frm_update method=post action="controller/manage_table_update.php" language=jscript enctype="multipart/form-data">
		<input type=hidden name=section value='<?php echo $section; ?>' id=section />
		<input type=hidden name=action value='SAVE_UPD' id=action />
		<input type=hidden name=after_action value='' id=after_action />
		<input type=hidden name=obj_id value='' id=obj_id />
		<input type=hidden name=time_id value='<?php echo $time_id; ?>' id=time_id />
		<input type=hidden name=old_actual value='0' id=old_actual />
		<input type=hidden name=old_target value='0' id=old_target />
		<input type=hidden name=calctype value='STD' id=calctype />
		<h2>Update <?php echo $time['display']; ?></h2>
		<table class=form width=100%>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."target"]['h_client']; ?>:</th>
				<td id=target>0</td>
			</tr><tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?>:</th>
				<td id=td_actual><input type=text size=10 id=actual name="<?php echo $r_table_fld."actual"; ?>" /></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required)</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=perfcomm name="<?php echo $r_table_fld."perf"; ?>"></textarea></td>
			</tr>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?>:<?php if($me->requireUpdateComments()) { echo "<br /><span class='i' style='font-size: 6.5pt'>(Required if ".$mheadings[$r_section][$r_table_fld."actual"]['h_client']." does not meet ".$mheadings[$r_section][$r_table_fld."target"]['h_client'].")</span>"; } ?></th>
				<td ><textarea cols=50 rows=7 id=correct name="<?php echo $r_table_fld."correct"; ?>"></textarea></td>
			</tr><?php
			if($setup_defaults['POE']=="T" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th><?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>: <span class=float>
				<?php 
				if(isset($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) && strlen($mheadings[$r_section][$r_table_fld.'attachment']['glossary']) >0) { 
					$me->displayIcon("info","id=poe_glossary");
				} ?></span>
				</th>
				<td ><textarea cols=50 rows=7 id=proof name="poe"></textarea></td>
			</tr>
			<?php
			}	//if setup defaults allows for textfield
			if($setup_defaults['POE']=="A" || $setup_defaults['POE']=="B") {
			?>
			<tr>
				<th>Attachment:</th>
				<td >
					<div id=attach_form><?php $me->displayAttachmentForm(); ?></div>
					<div id=div_update_attach_display></div>
				</td>
			</tr>
			<?php
			}	//if setup defaults allows for attachments
			?>
			<tr>
				<th></th>
				<td><input type=button value="Save Update" class=isubmit /> <input type=reset /></td>
			</tr>
		</table>
		</form>
	</div>
<div id=div_details style='background-color: #ffffff; width: 49%' >
	<div id=div_update_contents style='background-color: #ffffff;'>
		<h2>Details</h2>
		<?php

			$kpi_head = $headObj->getHeadings($myObj->getHeadingCodes()); //arrPrint($kpi_head);
			$head = array_merge(array('dir'=>$kpi_head['dir']),$kpi_head[$myObj->getHeadCode()]);
			$rhead = $kpi_head[$myObj->getRHeadCode()];
			$kpi_time = $timeObj->getAllTime();

			$i = ($section=="KPI" ? DEPT::REFTAG : TOP::REFTAG).$obj_id;
			$kr = $update_details['data']; //$kpiObj->getUpdateFormDetails($k);
			$kr['display_id'] = $i;
			//arrPrint($kr);
			$drawObj->drawLiteKPI($myObj,$head,$rhead,$kpi_time,$kr,true);
		?>
	</div>
</div>
	<div id=div_poe_glossary title="<?php echo $mheadings[$r_section][$r_table_fld."attachment"]['h_client']; ?>">
		<p><?php echo str_replace(chr(10),"<br />",decode($mheadings[$r_section][$r_table_fld."attachment"]['glossary'])); ?></p>
	</div>
	<div id=dlg_result>
	</div>

</div>
<div id=dlg_msg title=Processing>
</div>
<div id=dlg_response title=Success>
<?php echo str_replace(chr(10),"",$me->getDisplayResult(array("ok","Update processed successfully."))); ?>
<p>Click "OK" to be redirected back to the Action Dashboard.</p>
</div>
<script type="text/javascript"> 

//	setTimeout(function () { $("#dispRes").css('display','none') }, 2500);
		var windowSize = getWindowSize();
		var dialogWidth = (windowSize['width']/2)-25;
		var centerVertical = windowSize['height']/2;
		var centerHorizontal = windowSize['width']/2;
		//var dialogWidth = windowSize['width']<850 ? windowSize['width']-50 : 800;
		//var dialogHeight = windowSize['height']<850 ? windowSize['height']-50 : 800;
		var section = "<?php echo $section; ?>";
		var use_plc = "<?php echo ($me->usePLC() ? "Y" : "N"); ?>";
		var require_comment = <?php echo $me->requireUpdateComments()==true ? "true" : "false"; ?>;
		var comment_length = <?php echo $me->requiredCommentLength(); ?>;
		var okIcon = "<?php echo ASSIST_HELPER::drawStatus(true); ?>";
		var timeInc = <?php echo $time_increment; ?>;
		var month1 = <?php echo $first_month; ?>;
		var processing_html = "<p class=center>Please wait while your form is processed...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p><p class=center>This may take some time depending on the size of the attachments added.</p>";
		var loading_html = "<p class=center>Please wait while your form loads...</p><p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p>";
		var redirecting_html = "<p class=center>Redirecting...</p>";
		
	$(function() {
		$("tr.no-highlight").unbind("mouseenter mouseleave");
		
		$("form[name=frm_update] input").keypress(function(e) {
			if(e.keyCode==13) {
				e.preventDefault();
			}
		});
		
		var attach_form = $("#attach_form").html();

		
		var loadingSize = {width:250,height:300};
		var dlgLoadingOpts = ({
			modal: true,
			closeOnEscape: false,
			autoOpen: false,
			width: loadingSize['width'],
			height: loadingSize['height'],
			position:  [(centerHorizontal-(loadingSize['width']/2)), (centerVertical-(loadingSize['height']/2))] 
		});

		var resultSize = {width:300,height:200};
		var dlgResultsOpts = ({
			closeOnEscape: true,
			modal: true,
			autoOpen: false,
			width: resultSize['width'],
			height: resultSize['height'],
			position:  [(centerHorizontal-(resultSize['width']/2)), (centerVertical-(resultSize['height']/2))] ,
			buttons: [{ text: "Ok", click: function() { 
					$("#dlg_response").dialog("close"); 
				} 
			}],
			close: function() { 
				$("#dlg_msg").html("<p>Redirecting...</p>").dialog('option','height',100).dialog("open");
				$('#backHome', window.parent.header.document).click();
			}

		});
		var errorSize = {width:300,height:320};
		var dlgErrorOpts = ({
			modal: true,
			autoOpen: false,
			width: errorSize['width'],
			height: errorSize['height'],
			buttons:[{
				text: "Ok",
				click: function() { $(this).dialog("close"); }
			}]
		});
		$("#dlg_msg").dialog({modal: true,autoOpen: false}).html(loading_html);
		$("#dlg_msg").dialog("option",dlgLoadingOpts);
		//$("#dlg_msg").dialog('option',dlgLoadingOpts).dialog("open");
		$("#dlg_response").dialog(dlgResultsOpts);
		$(".ui-dialog-titlebar").hide();
		//createResultsDialog();
		//$("#div_auto_update").hide();
		$("#div_poe_glossary").dialog({
			autoOpen: false,
			modal: true
		});
		$("#div_update #poe_glossary").css("cursor","pointer").click(function() {
			$("#div_poe_glossary").dialog("open").dialog("option","position",{ my: "center", at: "center", of: $("#div_update") });
		});
		/*$("#div_update").dialog({
			autoOpen: false,
			modal: true,
			width: dialogWidth,
			height: dialogHeight,
			close: function() { 
				$("textarea, input:text").removeClass("required");
				$("input:file").val("");
				enable_scroll();
				//$("body").css("overflow","scroll"); 
			}
		});*/
		//$("#div_update_form, #div_details").css("width",dialogWidth);
		$("#div_details table").css("width","100%");
		$("#div_update input:button.cancel").click(function() {
			$("#div_update").dialog("close");
		});
		//$("#tbl_update tbody tr").click(function() {
			
			$("#attach_form").html(attach_form);
			var i = <?php echo $obj_id; ?>; //$(this).prop("id");
			var ti = <?php echo $time_id; ?>;//$("#div_update #time_id").val();
			var dta = "action=GET_UPDATE_DETAILS&id="+i+"&time_id="+ti+"&section="+section;
			//alert(dta);
			var d = doAjax("controller/manage_table_update.php",dta);
			//alertArray(d,0);
			//alert(d[1]);
			if(d[0]==1) {
				$("#div_update_form #td_actual span").remove();
				//$("#div_update_form h2").html(d['ref']+": "+d['data']['value']);
				$("#div_update #obj_id").val(i);
				$("#div_update_form #target").html(d['data']['results']['target'][ti]);
				$("#div_update #old_target").val(d['data']['results']['r'][ti]['target']);
				$("#div_update #calctype").val(AssistString.decode(d['data']['calctype']));
				$("#div_update_form #actual").val(d['data']['results']['r'][ti]['actual']);
				$("#div_update_form #perfcomm").val(AssistString.decode(d['data']['results']['perfcomm'][ti]));
				$("#div_update_form #correct").val(AssistString.decode(d['data']['results']['correct'][ti]));
				$("#div_update_form #proof").val(AssistString.decode(d['data']['results']['poe'][ti]));
				if(d['data']['targettype']==2) {
					$('<span>%</span>').insertAfter($("#div_update_form #actual"));
				} else if(d['data']['targettype']==2) {
					$('<span>R </span>').insertBefore($("#div_update_form #actual"));
				}
				//alert(d['attach_display']);
				$("#div_update_form #div_update_attach_display").html(d['attach_display']); //$("#div_update_form #proof").val(d['attach_display']);
				$("li.hover").hover(
					function(){ $(this).addClass("trhover-dark"); },
					function(){ $(this).removeClass("trhover-dark"); }
				);
			}
		//});
		$("#div_update input:button.isubmit").click(function() {
			$("#dlg_msg").html(processing_html).dialog('option',dlgLoadingOpts).dialog('option','buttons',[]).dialog("open");
			var err = "";
			var valid = true;
			var validNumbers = new Array("0","1","2","3","4","5","6","7","8","9",".");
			//validate actual
			$("#div_update #actual").removeClass("required");
			var actual = $("#div_update #actual").val();
			if(actual.length > 0) {
				var a = actual.split("");
				for(x in a) { 
					//alert(a[x]+" : "+($.inArray(a[x],validNumbers)<0));
					if($.inArray(a[x],validNumbers)<0) {
						valid = false;
						break;
					}
				}
			} else {
				valid = false;
			}
			if(!valid) {
				$("#div_update #actual").addClass("required");
				err+="<li><?php echo $mheadings[$r_section][$r_table_fld."actual"]['h_client']; ?> (Only numbers are allowed)</li>";
			}
			//validate comments
			//if(valid) {
				var t = $("#div_update #old_target").val();
				var ct = $("#div_update #calctype").val();
				var fld = $("#div_update_form #perfcomm").prop("name");
				
				//alert(fld+" :: "+require_comment+" :: "+comment_length+" :: "+$("#div_update #perfcomm").val());
				valid_perf = validateMe(fld,require_comment,comment_length,$("#div_update #perfcomm").val(),t,actual,ct);
				fld = $("#div_update_form #correct").prop("name");
				valid_correct = validateMe(fld,require_comment,comment_length,$("#div_update #correct").val(),t,actual,ct);
				if(!valid_perf && require_comment) {
					$("#div_update #perfcomm").addClass("required");
					err+="<li>A <?php echo $mheadings[$r_section][$r_table_fld."perf"]['h_client']; ?> is required (must exceed "+comment_length+" characters in length.)</li>";
				}
				if(!valid_correct && require_comment) {
					$("#div_update #correct").addClass("required");
					err+="<li>A <?php echo $mheadings[$r_section][$r_table_fld."correct"]['h_client']; ?> is required where the target has not been met for the period (must exceed "+comment_length+" characters in length.)</li>";
				}
			//}
			//if valid
			if(valid && valid_perf && valid_correct) {
				//save update
				var f = 0;
				$("#div_update input:file").each(function() {
					f+=$(this).val().length;
				});
				$form = $("form[name=frm_update]");
				$form.prop("target","file_upload_target");
				//var d = doAjax("controller/manage_table_update.php",$form.serialize());
				//upload attachments
					//$("#result").val(d[0]);
					//$("#response").val(d[1]);
					//if(f>0) {
						var post_action = "var okIcon = '"+okIcon.replace(/'/g,'"')+"'; 	";
						post_action+= "window.parent.$('#dlg_msg').dialog('close');	";
						//post_action+= "window.parent.$('td."+$("#div_update #obj_id").val()+"updateclass').html(okIcon);	";
						post_action+= "window.parent.$('#dlg_response #spn_displayResultText').text('UPDATERESPONSEMSG');	";
						post_action+= "window.parent.$('#dlg_response').dialog('open'); ";
						//$("form[name=frm_update] #action").val("UPDATE_ATTACH");
						$("form[name=frm_update] #after_action").val(post_action);
						$form.submit();
					/*} else {
						//alert("DEV NOTE: Update background page and Display completed message!");
						$("#dlg_msg").dialog("close");
						//$("td."+$("#div_update #obj_id").val()+"updateclass").html(okIcon);
						$("#dlg_response #spn_displayResultText").text("Update processed successfully.");
						$("#dlg_response").dialog("open");
					}*/
			//else not valid then display error message
			} else {
				//alert("DEV NOTE: DISPLAY ERROR MESSAGE HERE: "+err);
				$("#dlg_msg").html("<h1 class=red>Error</h1><p>Please complete the following required fields:</p><ul id=ul_req>"+err+"</ul>").dialog("option",dlgErrorOpts);
			}
		});

	});	
	function showResponse(txt) {
		$(function() {
			$("#dlg_response #spn_displayResultText").text("Attachment deleted successfully.");
			$("#dlg_response").dialog("open");
		});
	}
	function showDialog(r,response) {
		$(function() {
				$('<div />',{id:'response_msg', html:response}).dialog({
					autoOpen	: true,
					modal 		: true,
					title 		: r,
					width 		: 430,
					height    	: 'auto',
					position	: [(centerHorizontal-(430/2)), (centerVertical-50)],
					buttons		: [{ text: 'Ok', click: function() { 
										$('#response_msg').dialog('destroy'); 
										$('#response_msg').remove(); 
									} 
								}],
					});
		});
	}

</script>