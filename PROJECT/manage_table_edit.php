<!-- <link rel="stylesheet" href="lib/locked-column.css" type="text/css"> -->
<?php
$my_head = $mheadings[$section];
if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if($section=="KPI" || $section == "TOP") {
	$colspan = 3;
} elseif($section=="CF" || $section=="CAP") {
	if($filter['display']!="LIMIT") {
		$colspan = count($mheadings[$rheadings]);
	} else {
		$colspan = 0;
		foreach($mheadings[$rheadings] as $h) {
			if($h['c_list']) 
				$colspan++;
		}
	}
} else {
	$colspan = 2;
}
/*echo "<h2>";
if($section=="TOP") { echo "Quarter ending "; }
if($section=="CF" || $filter['when'][0]==$filter['when'][1]) {
	echo $time[$filter['when'][0]]['display_full'];
} else {
	echo $time[$filter['when'][0]]['display_full']." - ".$time[$filter['when'][1]]['display_full'];
}
echo "</h2>";*/
	echo "<div id=dispRes>";
	if(isset($_REQUEST['r'])) {
		displayResult($_REQUEST['r']);
	}
	echo "</div>";
?>
<div id=tbl-container>
<table id=tbl>



<thead>
<?php
$total_columns = 0;
	$total = array();
	$total['all']['target'] = 0;
	$total['all']['actual'] = 0;
	echo "<tr>
		<th rowspan=2>Ref</th>";
		foreach($mheadings['FIXED'] as $key => $row) {
			$total_columns++;
			echo "<th rowspan=2 >".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
		}
		if($section=="TOP") {
			echo "<th rowspan=2>Assoc. Dept KPIs</th>";
		}
		foreach($my_head as $key => $row) {
			if(($filter['display']!= "LIMIT" || $row['c_list']) && !$row['fixed']) {
				$total_columns++;
				echo "<th rowspan=2>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
			}
		}
		$tc = 0;
		$t_css = 2;
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $ti => $row) {
				$tc++;
				echo "<th colspan=$colspan class=\"time".$t_css."\">".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
				//$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			$ti = $filter['when'];
			$t = $time[$ti];
			/*foreach($time as $ti => $t) {
				if($ti > $filter['when'][1]) {
					break;
				} elseif($ti>=$filter['when'][0]) {*/
					$tc++;
					echo "<th colspan=$colspan class=\" time".$t_css."\">".$t['display_full']."</th>";
					$total[$ti]['target'] = 0;
					$total[$ti]['actual'] = 0;
					$t_css = ($t_css>=4) ? 1 : $t_css+1;
				/*}
			}*/
		}
		/*if($section!="CF" && $filter['when'][0]!=$filter['when'][1]) {
			switch($section) {
				case "TOP":
				case "KPI":
					echo "<th class=\" total\" colspan=$colspan>Overall Performance</th>";
					break;
				default:
					echo "<th class=\"total\" colspan=$colspan>Total</th>";
			}
		}*/
	echo "	</tr>";
	echo "	<tr>";
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $h => $cf_h) {
				foreach($mheadings['CF_R'] as $r => $cf_r) {
					if($filter['display']!="LIMIT" || $cf_r['c_list']) {
						echo "<th class=\"time".$t_css."\">".(strlen($cf_r['h_client'])>0 ? $cf_r['h_client'] : $cf_r['h_ignite'])."</th>";
						$total[$h][$r] = 0;
					}
				}
				$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			$t_css = 2;
			//for($i=1;$i<=$tc;$i++) {
				switch($section) {
					case "TOP":
					case "KPI":
						echo "<th class=\"time".$t_css."\" >Target</th>
							<th  class=\"time".$t_css."\" >Actual</th>
							<th class=\"time".$t_css."\"  >R<img src=\"/pics/blank.gif\" height=1 width=15></th>";
						break;
					case "CAP":
						foreach($mheadings[$rheadings] as $r => $cr) {
							if($filter['display']!="LIMIT" || $cr['c_list']) {
								echo "<th class=\"time".$t_css."\">".(strlen($cr['h_client'])>0 ? $cr['h_client'] : $cr['h_ignite'])."</th>";
							}
						}
						break;
					default:
						echo "<th  class=\"time".$t_css."\" >Budget</th>
							<th  class=\"time".$t_css."\" >Actual</th>";
				}
				//$t_css = ($t_css>=4) ? 1 : $t_css+1;
			//}
		}
		/*if($section!="CF" && $filter['when'][0]!=$filter['when'][1]) {
			switch($section) {
				case "TOP":
				case "KPI":
					echo "<th class=\" total\" >Target</th>
						<th class=\" total\" >Actual</th>
						<th class=\" total\" >R<img src=\"/pics/blank.gif\" height=1 width=15></th>";
					break;
				default:
					echo "<th class=\" total\" >Budget</th>
						<th class=\" total\" >Actual</th>";
			}
		}*/
	echo "	</tr>";

?>
</thead>






<tbody>
<?php

$rc = 0;
	$object_rs = getRS($object_sql);
	while($object = mysql_fetch_assoc($object_rs)) {
		$obj_id = $object[$table_fld.'id'];
		echo "<tr>
			<td class=thtd>".(isset($id_label)?$id_label:"").$obj_id."</td>";
				foreach($mheadings['FIXED'] as $key => $h) {
					echo "<td>";
					if($h['h_type']=="LIST") {
						if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
							echo "<div class=centre>".$lists[$h['h_table']][$object[$key]]['code']."</div>";
						} else {
							//echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
							if(!isset($lists[$h['h_table']][$object[$key]]['value'])) {
								$v = $unspecified;
							} else {
								$v = $lists[$h['h_table']][$object[$key]]['value'];
							}
							echo displayValue($v);
						}
						} elseif($h['h_type']=="TEXTLIST") {
							$x = $object[$key];
							$a = array();
							if(strlen($x)>0 && $x != "0") {
								$y = explode(";",$x);
								foreach($y as $z) {
									if(isset($lists[$h['h_table']][$z]['value'])) {
										$a[] = $lists[$h['h_table']][$z]['value'];
									}
								}
							}
							if(count($a)>0) {
								$v = implode("; ",$a);
							} else {
								$v = $unspecified;
							}
							echo displayValue($v);
					} else {
						if($key==$detail_link && ($section=="KPI" || $section == "TOP" || $section == "CAP")) {
							echo "<a href=".$base[0]."_".$base[1]."_detail.php?page_id=".$page_id."&id=".$obj_id.">";
						}
						echo displayValue($object[$key])."</a>";
					}
					echo "</td>";
				}
				if($section=="TOP") {
					echo "<td class=center>".(isset($dept_kpis[$obj_id]) ? $dept_kpis[$obj_id]['kc'] : "0")."<br /><input type=button value=Edit class=assockpi id=".$obj_id." /></td>";
				}
				foreach($my_head as $key => $h) {
					if(($filter['display']!="LIMIT" || $h['c_list']) && !$h['fixed']) {
						echo "<td>";
						if($h['h_type']=="LIST") {
							if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
								echo "<div class=centre title=\"".$lists[$h['h_table']][$object[$key]]['value']."\" style=\"text-decoration: underline\">".$lists[$h['h_table']][$object[$key]]['code']."</div>";
							} else {
//								echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
								if(!isset($lists[$h['h_table']][$object[$key]]['value'])) {
									$v = $unspecified;
								} else {
									$v = $lists[$h['h_table']][$object[$key]]['value'];
								}
								echo displayValue($v);
							}
						} elseif($h['h_type']=="TEXTLIST") {
							$x = $object[$key];
							$a = array();
							if(strlen($x)>0 && $x != "0") {
								$y = explode(";",$x);
								foreach($y as $z) {
									if(isset($lists[$h['h_table']][$z]['value'])) {
										$a[] = $lists[$h['h_table']][$z]['value'];
									}
								}
							}
							if(count($a)>0) {
								$v = implode("; ",$a);
							} else {
								$v = $unspecified;
							}
							echo displayValue($v);
						} else {
							switch($key) {
								case $table_fld."area":
								case $table_fld."wards":
								case "capital_area":
								case "capital_wards":
								case "capital_fundsource":
									if(isset($wa) && isset($wa[$key]) && isset($wa[$key][$obj_id])) {
										//echo $key." :: "; arrPrint($wa[$key][$obj_id]);
										foreach($wa[$key][$obj_id] as $v) {
											if(isset($v['code']) && strlen($v['code'])) {
												echo $v['code'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											} else {
												echo $v['value'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											}
										}
									}
									break;
								case $table_fld."topid":
									if(isset($tops) && isset($tops[$object[$key]])) {
										echo displayValue($tops[$object[$key]]['top_value']." [".$id_labels_all['TOP'].$object[$key]."]");
									}
									break;
								case $table_fld."capitalid":
									if(isset($caps) && isset($caps[$object[$key]])) {
										echo displayValue($caps[$object[$key]]['cap_name']." [".$id_labels_all['CAP'].$object[$key]."]");
									}
									break;
								case "top_annual":
								case "top_revised":
									echo "<div class=right>".KPIresultDisplay($object[$key],$object[$table_fld.'targettype'])."</div>";
									break;
								case "cap_planstart":
								case "cap_planend":
								case "cap_actualstart":
								case "cap_actualend":
									if(strlen($object[$key])>1) {
										echo date("d M Y",$object[$key]);
									}
									break;
								default:
									echo displayValue($object[$key]);
									break;
							}
						}
						echo "</td>";
					}
				}
				$result_object = $results[$obj_id];
				$t_css = 2;
				switch($section) {
					case "KPI":
					case "TOP":
						$obj_tt = $object[$table_fld.'targettype'];
						$obj_ct = $object[$table_fld.'calctype'];
						$values = array('target'=>array(),'actual'=>array());
						/*foreach($time as $ti => $t) {
						//echo "<P>".$ti;
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {*/
							//echo " :: ".$ti;
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								//function KPIcalcResult(array of all targets/actuals, calctype, time period range, current time id)
								//$r = array('style'=>"result0",'text'=>"ERR",'target'=>0,'actual'=>0);
								$r = KPIcalcResult($values,$obj_ct,$filter['when'],$ti);
								echo "<td class=\"right time".$t_css."\">";
									echo KPIresultDisplay($r['target'],$obj_tt);
								echo "</td><td class=\"right time".$t_css."\">";
									echo KPIresultDisplay($r['actual'],$obj_tt);
								echo "</td>";
								echo "<td class=\"".$r['style']."\">".$r['text']."</td>";
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
							/*}
						}*/
						/*if($filter['when'][0]!=$filter['when'][1]) {
							//function KPIcalcResult(array of all targets/actuals, calctype, time period range, current time id)
							$r = KPIcalcResult($values,$obj_ct,$filter['when'],"ALL");
							echo "<td class=\"right total\">";
								echo KPIresultDisplay($r['target'],$obj_tt);
							echo "</td><td class=\"right total\">";
								echo KPIresultDisplay($r['actual'],$obj_tt);
							echo "</td>";
							echo "<td class=\"".$r['style']."\">".$r['text']."</td>";
						}*/
						break;
					case "CF":
						$result_object = $results[$obj_id][$filter['when'][0]];
						$t_css = 3;
						foreach($mheadings['CF_H'] as $rs => $cf_h) {
							$cfh_values = array();
							$budget = 0;
							foreach($mheadings['CF_R'] as $res => $cf_r) {
								$fld = "cr_".$cf_h['field'].$cf_r['field'];
								$cfh_values[$fld] = $result_object[$fld];
								switch($res) {
									case "_1": case "_2": case "_3":	$budget+=$cfh_values[$fld];				break;	//o. budget, adj. est., vire
									case "_4":							$cfh_values[$fld] = $budget;			break;	//adj. budget
									case "_5":							$actual = $cfh_values[$fld];			break;	//actual
									case "_6":							$cfh_values[$fld] = $actual - $budget;	break;	//variance
								}
								//if($filter['display']!="LIMIT" || $cf_r['c_list']) {
									$val = $cfh_values[$fld];
									$total[$rs][$res]+= $val;
									echo "<td class=\"right time".$t_css."\">";
										switch($cf_r['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
									echo "</td>";
								//}
							}
							$t_css = ($t_css<=1) ? 3 : $t_css-1;
						}
						break;
					case "CAP":
						$values = array();
						/*foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {*/
								foreach($mheadings[$rheadings] as $r => $cr) {
									if(!isset($total[$ti][$r])) { $total[$ti][$r] = 0; }
									if(!isset($values[$r])) { $values[$r] = 0; }
									$val = $result_object[$ti][$r];
									$total[$ti][$r]+=$val;
									$values[$r]+=$val;
									//if($filter['display']!="LIMIT" || $cr['c_list']) {
										echo "<td class=\"right time".$t_css."\">";
										switch($cr['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
										echo "</th>";
									//}
								}
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
							/*}
						}
						if($filter['when'][0]!=$filter['when'][1]) {
							echo "<td class=\"right total\">";
								echo number_format($values['cr_target'],2);
								$total['all']['target']+=$values['cr_target'];
							echo "</td><td class=\"right total\">";
								echo number_format($values['cr_actual'],2);
								$total['all']['actual']+=$values['cr_actual'];
							echo "</td>";
						}*/
						break;
					default:
						/*foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {*/
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								$total[$ti]['target']+=$values['target'][$ti];
								$total[$ti]['actual']+=$values['actual'][$ti];
								echo "<td class=\"right time".$t_css."\">";
									echo number_format($values['target'][$ti],2);
								echo "</td><td class=\"right time".$t_css."\">";
									echo number_format($values['actual'][$ti],2);
								echo "</td>";
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
							/*}
						}
						if($filter['when'][0]!=$filter['when'][1]) {
							echo "<td class=\"right total\">";
								echo number_format(array_sum($values['target']),2);
							echo "</td><td class=\"right total\">";
								echo number_format(array_sum($values['actual']),2);
							echo "</td>";
						}*/
						break;
				}
		echo "</tr>";
	}
	?>
	</tbody>
</table>
</div>
<script type="text/javascript"> 
/*	var table = document.getElementById('tbl');
	var cTR = table.getElementsByTagName('TR');  //collection of rows
 
		for (i = 0; i < cTR.length; i++)
		{
			if(i!=1) {
				var tr = cTR.item(i);
				tr.cells[0].className = 'locked';
			}
		}*/
	$(function() {
		setTimeout(function () { $("#dispRes").css('display','none') }, 2500);
		$(".assockpi").click(function() {
			i = $(this).attr("id");
			document.location.href = '<?php echo $base[0]."_".$base[1]."_assoc.php?page_id=".$page_id; ?>&id='+i;
		});
	});
</script>