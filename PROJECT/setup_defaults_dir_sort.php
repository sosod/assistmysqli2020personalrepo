<?php 
$log_section = "DIR";
$page_title = "<a href=setup_defaults_dir.php class=breadcrumb>Directorates</a> >> Sort";
include("inc/header.php"); 
/*$sql = "SELECT * FROM ".$dbref."_setup_headings WHERE h_table IN ('dir','subdir')";
$headings = mysql_fetch_all($sql,"h_table");
$head_dir = (strlen($headings['dir']['h_client'])>0) ? $headings['dir']['h_client'] : $headings['dir']['h_ignite'];
$head_sub = (strlen($headings['sub']['h_client'])>0) ? $headings['sub']['h_client'] : $headings['sub']['h_ignite'];*/

if(isset($_REQUEST['action']) && $_REQUEST['action']=="SAVE") {
	if(!isset($_REQUEST['sort']) || !isset($_REQUEST['type'])) { die("<P>An error has occurred.  Please go back and try again.</p>"); }
	$sort = $_REQUEST['sort'];
	$type = $_REQUEST['type'];
	if($type=="S" && (!isset($_REQUEST['i']) || !checkIntRef($_REQUEST['i']))) { die("<P>An error has occurred. Please go back and try again.</p>"); }
	//$id = $_REQUEST['i'];
	switch($type) {
		case "D":
			$tbl = "dir";
			$text = "Changed the display order of the ".$head_dir."s.";
			foreach($sort as $s => $i) {
				$sql = "UPDATE ".$dbref."_".$tbl." SET sort = $s WHERE id = $i";
				db_update($sql);
			}
			break;
		case "S":
			$tbl = "subdir";
			$text = "Changed the display order of the ".$head_sub."s for ".$head_dir." ".$_REQUEST['i'].".";
				$sql = "UPDATE ".$dbref."_".$tbl." SET sort = 1 WHERE head = true";
				db_update($sql);
			foreach($sort as $s => $i) {
				$sql = "UPDATE ".$dbref."_".$tbl." SET sort = ".($s+2)." WHERE id = $i";
				db_update($sql);
			}
			break;
		default: die("<P>An error has occurred.  Please go back and try again.");
	}
											$v_log = array(
											'section'=>$log_section,
											'ref'=>0,
											'field'=>$tbl,
											'text'=>$text,
											'old'=>"",
											'new'=>"",
											'YN'=>"Y"
											);
										logChanges('SETUP',0,$v_log,code($sql));
	echo "<script type=text/javascript>";
		echo "document.location.href = 'setup_defaults_dir.php?r[]=ok&r[]=Display+order+has+been+updated.';";
	echo "</script>";
	
	
	
	
	
}

if(!isset($_REQUEST['type'])) {
	die("<p>An error has occurred.  Please go back and try again.</p>");
} else {
	$type = $_REQUEST['type'];
	switch($type) {
		case "D":
			$id = 0;
			$sql = "SELECT * FROM ".$dbref."_dir WHERE active = true ORDER BY sort";
			break;
		case "S":
			if(!isset($_REQUEST['i']) || !checkIntRef($_REQUEST['i'])) {
				die("<P>An error has occurred.  Please go back and try again.</p>");
			} else {
				$id = $_REQUEST['i'];
				$sql = "SELECT * FROM ".$dbref."_subdir WHERE active = true AND dirid = $id AND head = false ORDER BY sort";
			}
			break;
		default:
			die("<P>An error has occurred.  Please go back and try again.</p>");
	}
	$items = mysql_fetch_all($sql);
	?>
	<table width=350 class=noborder><tr><td class="blank noborder">
		<form action=<?php echo $self; ?> method=post>
			<input type=hidden name=action value=SAVE />
			<input type=hidden name=type value=<?php echo $type; ?> />
			<input type=hidden name=i value=<?php echo $id; ?> />
			<?php drawSortUl($items,350); ?>
			<p><input type=submit class=isubmit value="Save Changes" /><span class=float><input type=button  value="Reset" /></span></p>
		</form>
	</td></tr></table>
	<?php
}

goBack("setup_defaults_dir.php","");
?>
<script type=text/javascript>
	$(function() {
		$(".ireset.").click(function() {
			document.location.href = '<?php echo $self."?type=".$type."&i=".(isset($_REQUEST['i']) ? $_REQUEST['i'] : 0); ?>';
		});
	});
</script>
</body>
</html>