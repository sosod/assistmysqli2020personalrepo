<?php 
$page_title = "Headings";
include("inc/header.php");
//arrPrint($_REQUEST);
if($page_id!="term") {
	if(!isset($_REQUEST['view']) || $_REQUEST['view']!="SORT") {
		echo "<p>To make the field required on the New/Edit page, tick the \"Required\" checkbox.  Fields required by the system are ticked by default and cannot be changed.
		<br />To make the field display on the List page automatically (Limited view setting), tick the \"List View\" checkbox.
		<br />Use the slider to change the number of characters allowed in text fields.  The maximum is limited to what the system will allow.  If the SDBIP has been loaded, the minimum will be limited to the longest existing text for that field.</p>
		<p><input type=button value='Headings Display Order' onclick=\"document.location.href = 'setup_defaults_head.php?page_id=".$page_id."&view=SORT';\" /></p>";
		$goto = "setup_defaults_head_fld.php"; 
	} else {
		echo "<p>To change the display order of a column/field, click on the Heading and drag the field to the required position.</p>
				<p><input type=button value='Headings Settings' onclick=\"document.location.href = 'setup_defaults_head.php?page_id=".$page_id."&view=SET';\" /></p>";
		$goto = "setup_defaults_head_sort.php"; 
	}
	switch($page_id) {
		case "dept":	
			$section = "KPI";
			$fld_name = "kpi_";
			$fixed = "kpi_subid";
			$unset = array("kpi_calctype","kpi_targettype");
			$result = array("ok","Departmental SDBIP Headings updated.");
			$section_name = "Departmental SDBIP";
			$maxlen_sql = "SELECT 
				MAX(LENGTH(kpi_idpref)) as kpi_idpref,
				MAX(LENGTH(kpi_mtas)) as kpi_mtas,
				MAX(LENGTH(kpi_value)) as kpi_value,
				MAX(LENGTH(kpi_unit)) as kpi_unit,
				MAX(LENGTH(kpi_riskref)) as kpi_riskref,
				MAX(LENGTH(kpi_risk)) as kpi_risk, 
				MAX(LENGTH(kpi_baseline)) as kpi_baseline,
				MAX(LENGTH(kpi_pyp)) as kpi_pyp,
				MAX(LENGTH(kpi_perfstd)) as kpi_perfstd,
				MAX(LENGTH(kpi_poe)) as kpi_poe
				FROM ".$dbref."_kpi";
			break;
		case "top":		
			$section = "TOP";
			$fld_name = "top_";
			$fixed = "top_dirid";
			$unset = array("top_calctype","top_targettype");
			$result = array("ok","Top Layer SDBIP Headings updated.");
			$section_name = "Top Layer SDBIP";
			$maxlen_sql = "SELECT 
				MAX(LENGTH(top_pmsref)) as top_pmsref
				, MAX(LENGTH(top_mtas)) as top_mtas
				, MAX(LENGTH(top_value)) as top_value
				, MAX(LENGTH(top_unit)) as top_unit
				, MAX(LENGTH(top_risk)) as top_risk
				, MAX(LENGTH(top_baseline)) as top_baseline
				, MAX(LENGTH(top_poe)) as top_poe
				, MAX(LENGTH(top_pyp)) as top_pyp
				FROM ".$dbref."_top";
			break;
		case "cap":		
			$section = "CAP";
			$fld_name = "cap_";
			$fixed = "cap_subid";
			$unset = array();
			$result = array("ok","Capital Projects Headings updated.");
			$section_name = "Capital Projects";
			$maxlen_sql = "SELECT 
				MAX(LENGTH(cap_cpref)) as cap_cpref
				, MAX(LENGTH(cap_idpref)) as cap_idpref
				, MAX(LENGTH(cap_vote)) as cap_vote
				, MAX(LENGTH(cap_name)) as cap_name
				, MAX(LENGTH(cap_descrip)) as cap_descrip
				FROM ".$dbref."_capital";
			break;
		case "cf":		
			$section = "CF";
			$fld_name = "cf_";
			$fixed = "cf_subid";
			$unset = array();
			$result = array("ok","Monthly Cashflow Headings updated.");
			$section_name = "Monthly Cashflow";
			$maxlen_sql = "SELECT 
				MAX(LENGTH(cf_value)) as cf_value,
				MAX(LENGTH(cf_vote)) as cf_vote
				FROM ".$dbref."_cashflow";
			break;
		case "rs":		
			$section = "RS";
			$fld_name = "rs_";
			$fixed = "rs_";
			$unset = array();
			$result = array("ok","Revenue By Source Headings updated.");
			$section_name = "Revenue By Source";
			$maxlen_sql = "SELECT 
				MAX(LENGTH(rs_value)) as rs_value,
				MAX(LENGTH(rs_vote)) as rs_vote
				FROM ".$dbref."_revbysrc";
			break;
	}
} else {
	$goto = "setup_defaults_head_term.php"; 
}

if(isset($goto)) {
	include($goto);
}

?>

</body>
</html>