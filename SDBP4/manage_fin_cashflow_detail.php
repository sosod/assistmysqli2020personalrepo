<?php 
$section = "CF";
$tab = "edit";
$page_title = "Edit";
$get_lists = true;
$get_active_lists = true;
$jquery_date = true;
include("inc/header.php");

$obj_id = (isset($_REQUEST['id']) && checkIntRef($_REQUEST['id'])) ? $_REQUEST['id'] : die("An error occurred.  Please go back and try again.");

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			$fields[] = "o.".$fld." as o".$fld;
			break;
		default: $fields[] = "o.".$fld;
	}
}

$object_sql = "SELECT dir.value, ".implode(", ",$fields)."
FROM ".$dbref."_".$table_tbl." o
INNER JOIN ".$dbref."_subdir as subdir ON o.".$table_fld."subid = subdir.id
INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE o.".$table_fld."id = ".$obj_id;
$object = mysql_fetch_all($object_sql);
$object = $object[0];

$result_sql = "SELECT * FROM ".$dbref."_".$table_tbl."_results WHERE ".$r_table_fld.$table_id."id = ".$obj_id." ORDER BY ".$r_table_fld."timeid";
$results = mysql_fetch_alls($result_sql,$r_table_fld."timeid");






if(isset($_REQUEST['act'])) {
if($_REQUEST['act']=="DELETE") {
	echo "<p>Processing delete request...</p>";
	$sql = "UPDATE ".$dbref."_".$table_tbl." SET cf_active = false WHERE cf_id = ".$obj_id;
	db_update($sql);
		$v = array(
			'fld'=>"cf_active",
			'timeid'=>0,
			'text'=>"Deleted Monthly Cashflow Line Item ".$id_labels_all[$section].$obj_id.".",
			'old'=>0,
			'new'=>0,
			'act'=>"D",
			'YN'=>"Y"
		);
		logChanges($section,$obj_id,$v,code($sql));
		echo "	<script type=text/javascript>
					document.location.href = '".$base[0]."_".$base[1].".php?page_id=cashflow&tab=edit&r[]=ok&r[]=Monthly+Cashflow+Line+Item+".$id_labels_all[$section].$obj_id."+has+been+successfully+deleted.';
				</script>";
	
} else if($_REQUEST['act']=="SAVE") {
//	echo "SAVE ME";
	//arrPrint($_REQUEST);
	
	
	
	
	
	
	echo "<p>Processing edit...</p>";
	//echo "<table><tr><th>Fld</th><th>Type</th><th>New</th><th>Old</th><th>Action</th></tr>";
	$updates = array();
	$trans = array();
	$changes = array();
	$sql = array();
	foreach($mheadings[$section] as $fld => $h) {
		$log_trans = "";
		switch($h['h_type']) {
			case "DATE":
				$new = isset($_REQUEST[$fld]) ? strtotime($_REQUEST[$fld]) : "";
				$old = isset($object[$fld]) ? $object[$fld] : "";
				if(checkIntRef($new) || checkIntRef($old)) {
					if(strpos($fld,"end")>0) { $new+=86400-1; }
					if($new!=$old) {
						$log_trans = addslashes("Edited ".$h['h_client']." to '".date("d M Y",$new).((checkIntRef($old)) ? "' from '".date("d M Y",$old) : "")."'");
						$trans[$fld]['lt'] = $log_trans;
						$trans[$fld]['n'] = $new;
						$trans[$fld]['o'] = $old;
						$changes[] = $fld." = '".$new."'";
					}
				}
				break;
			case "LIST":
				$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
				if($h['h_type']=="LIST") {
					$old = isset($object['o'.$fld]) ? $object['o'.$fld] : "0";
				} else {
					$old = isset($object[$fld]) ? $object[$fld] : "0";
				}
				if($new!=$old) {
						$log_trans = addslashes("Edited ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."' from '".$object[$fld]."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
			default:
				$new = isset($_REQUEST[$fld]) ? code($_REQUEST[$fld]) : "";
				$old = isset($object[$fld]) ? $object[$fld] : "";
				if($new!=$old) {
					$log_trans = addslashes("Edited ".$h['h_client']." to '".$new."' from '".$old."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
		}
		/*echo "<tr>
		<th>".$fld."</th>
		<td>".$h['h_type']."</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	if(count($changes)>0) {
		$sb = "UPDATE ".$dbref."_".$table_tbl." SET ".implode(", ",$changes)." WHERE ".$table_fld."id = ".$obj_id;
		db_update($sb);
		foreach($trans as $fld => $lt) {
			$v = array(
				'fld'=>$fld,
				'timeid'=>0,
				'text'=>$lt['lt'],
				'old'=>$lt['o'],
				'new'=>$lt['n'],
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sb));
		}
	}
	/* TREAT EACH _R SECTION SEPARATELY */
	$f = array(
		'original'	=> "_1",
		'adjest'	=> "_2",
		'vire'		=> "_3",
		'monbudg'	=> "_4",
		'monact'	=> "_5",
		'monvar'	=> "_6",
		'monperc'	=> "_7",
		'ytdbudg'	=> "_8",
		'ytdact'	=> "_9",
		'ytdvar'	=> "_10",
		'ytdperc'	=> "_11",
		'totbudg'	=> "_12",
		'totact'	=> "_13",
		'totvar'	=> "_14",
		'totperc'	=> "_15",
	);
	foreach($mheadings['CF_H'] as $fldh => $hf) {
		$request = $_REQUEST['cr_'.$fldh.'_1'];
		//echo "<h3>".$fldh."</h3>";
		//arrPrint($request);
		//echo "<P>".array_sum($request);
		$res = array();
		$totals = array('b'=>0,'m'=>0);
		foreach($time as $ti => $t) {
			$r = array();
			foreach($f as $me => $i) {
				$v = 0;
				switch($me) {
				case "original": $v = ( (in_array($ti,$_REQUEST['ti']) && isset($request[$ti]) ) ? $request[$ti] : $results[$ti]['cr_'.$fldh.'_1']); $totals['m']=$v; break;
				case "totact":
				case "ytdact":
				case "monact":	
				case "adjest":
				case "vire":	$v = $results[$ti]['cr_'.$fldh.$i]; $totals['m']+=$v; break;
				case "monbudg":	$v = $totals['m']; $totals['b']+=$v; break;
				case "monvar":	$v = $r['monbudg'] - $r['monact']; break;
				case "monperc":	$v = $r['monact']/$r['monbudg']; break;
				case "ytdbudg":	$v = $totals['b']; break;
				case "ytdvar":	$v = $totals['b'] - $r['ytdact']; break;
				case "ytdperc":	$v = $r['ytdact']/$totals['b']; break;
				case "totbudg":	$v = 0; break;
				case "totvar":	
				case "totperc": $v = 0; break;
				}
				$r[$me] = $v;
			}
			$res[$ti] = $r;
		}
		foreach($time as $ti => $t) {
			$res[$ti]['totbudg'] = $totals['b'];
			$res[$ti]['totvar'] = $totals['b'] - $res[$ti]['totact'];
			$res[$ti]['totperc'] = $res[$ti]['totact']/$totals['b'];
			$r_sql = array();
			$changes = array();
			foreach($f as $me => $i) {
				$n = ( isset($res[$ti][$me]) && strlen($res[$ti][$me])>0 ) ? $res[$ti][$me] : 0;
				$o = $results[$ti]['cr_'.$fldh.$i];
				if($n!=$o) {
					$r_sql[] = $r_table_fld.$fldh.$i." = ".$n;
					$changes[] = "- Changed ".$mheadings['CF_R'][$i]['h_client']." to '".number_format($n,2)."' from '".number_format($o,2)."'";
				}
			}
			$sql = "UPDATE ".$dbref."_".$table_tbl."_results SET ".implode(", ",$r_sql)." WHERE cr_cfid = ".$obj_id." AND cr_timeid = ".$ti;
			//echo "<P>".$sql;
			db_update($sql);
			$trans = "Updated ".$hf['h_client']." financials for ".$t['display_full']." due to change in Original Budget:<br />".implode("<br />",$changes);
			//echo "<P>".$trans;
					$v = array(
						'fld'=>$fldh,
						'timeid'=>$ti,
						'text'=>addslashes($trans),
						'old'=>'',
						'new'=>'',
						'act'=>"E",
						'YN'=>"Y"
					);
					logChanges($section,$obj_id,$v,code($sql));

			
		}
	}
	//echo "</table>";
		echo "	<script type=text/javascript>
					document.location.href = '".$base[0]."_".$base[1].".php?page_id=cashflow&tab=edit&r[]=ok&r[]=Monthly+Cashflow+Line+Item+".$id_labels_all[$section].$obj_id."+has+been+successfully+updated.';
				</script>";



	
	
	
}	
	
	
} else {
//VIEW EDIT FORM
	include("manage_fin_table_detail.php");
	$log_sql = "SELECT flog_date, CONCAT_WS(' ',tkname,tksurname) as flog_tkname, flog_transaction FROM ".$dbref."_cashflow_log INNER JOIN assist_".$cmpcode."_timekeep ON flog_tkid = tkid WHERE flog_kpiid = '".$obj_id."' AND flog_yn = 'Y' ORDER BY flog_date DESC";
	$log_flds = array('date'=>"flog_date",'user'=>"flog_tkname",'action'=>"flog_transaction");
displayAuditLog($log_sql,$log_flds);
}

?>
</body>
</html>