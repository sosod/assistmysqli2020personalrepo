<?php 
include("inc/header.php"); 

$sql = "SELECT COUNT(DISTINCT usrtkid) as c FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '".$modref."'";
$r = mysql_fetch_one($sql);
$c = $r['c'];

if($c<=1) {
	$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$modref."'";
	$menu = mysql_fetch_one($sql);

	$sql = "SELECT * FROM assist_".$cmpcode."_menu_modules WHERE modmenuid = ".$menu['modid'];
	$mod = mysql_fetch_one($sql);

	if($mod['moduserallowed']>0) {
		$import_status['USERS'] = false;
	} else {
		$import_status['USERS'] = true;
	}
	
	if($modref=="SDP12") {
		$prev_module = array(
			'modref'=>"SDP11",
			'modtext'=>"SDBIP 2011/2012",
			'exists'=>false
		);
	} else {
		$prev_module = array(
			'modref'=>"SDP10",
			'modtext'=>"SDBIP 2010/2011",
			'exists'=>false
		);
	}
	$sql = "SELECT * FROM assist_menu_modules WHERE modref = '".$prev_module['modref']."'";
	$rs = getRS($sql);
	if(mysql_num_rows($rs)>0) {
		$prev_module['exists']=true;
	}
	
	
} else {
	$import_status['USERS'] = true;
}

$import_log_file = openLogImport();
//fdata = "date("Y-m-d H:i:s")","$_SERVER['REMOTE_ADDR']","action","$self","implode(",",$_REQUEST)"
logImport($import_log_file, "Open Support > Import", $self);
?>
<script>
$(function() {
	$(":button").click(function() {
		var id = $(this).attr("id");
		if(id=="list") {
			var t = $("#tbl").attr("value");
			var url = "support_import_list.php?t="+t;
		} else {
			var url = "support_import_"+id+".php";
		}
		document.location.href = url;
	});
});
</script>
<?php
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
displayResult($result);
?>
<p>Please note that the various sections of the SDBIP must be loaded in the order in which they appear:
	<ul>
		<li>All lists must be loaded before Top Layer, Capital Projects, Departmental SDBIP & Monthly Cashflows.</li>
		<li>Top Layer & Capital Projects must be loaded before the Departmental SDBIP if there are any links in the Municipal Scorecard or Capital Project columns as the system validates those links before it will create the Departmental SDBIP KPIs.</li>
		<li>Revenue By Source can be loaded at any stage as there are no links to the other sections.</li>
	</ul>
</p>
<table>
<!--	<tr>
		<th class=left>Microsoft Excel Template:&nbsp;</th>
		<td>Template for preparing the SDBIP for import.&nbsp;&nbsp;<span class=float><input type=hidden id=tbl value=all /><input type=button value="  Go  " id=template /></span></td>
	</tr> -->
	<tr>
		<th class=left>Default Lists:&nbsp;</th>
		<td>Preloaded lists.&nbsp;&nbsp;<span class=float><input type=hidden id=tbl value=all /><input type=button value="  Go  " id=def /></span></td>
	</tr>
	<tr>
		<th class=left>Lists:&nbsp;</th>
		<?php
			if($import_status['LIST']) {
				echo "<td>Loaded.";
//				$sql = "SELECT * FROM ".$dbref."_setup_headings WHERE h_type = 'LIST' AND h_table NOT IN ('calctype','targettype','kpiconcept','kpitype','riskrating') ORDER BY h_section, h_client";
//				$i_lists = mysql_fetch_all($sql);
				echo "&nbsp;<span class=float><input type=hidden id=tbl value=all /><input type=button value=\"  Go  \" id=list /></span>";
			} else {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
				echo "&nbsp;&nbsp;<span class=float><input type=hidden id=tbl value=all /><input type=button value=\"  Go  \" id=list /></span>";
			}
		?></td>
	</tr>
	<tr>
		<th class=left>Top Layer:&nbsp;</th>
		<?php
			if(!$import_status['LIST']) {
				echo "<td class=\"ui-state-error\">Please load the Lists first.";
			} elseif(!$import_status['TOP']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
		if($import_status['LIST']) { ?>&nbsp;&nbsp;<span class=float><input type=button value="  Go  " id=top /><?php } ?></td>
	</tr>
	<tr>
		<th class=left>Capital Projects:&nbsp;</th>
		<?php
			if(!$import_status['LIST']) {
				echo "<td class=\"ui-state-error\">Please load the Lists first.";
			} elseif(!$import_status['CAP']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
		if($import_status['LIST']) { ?>&nbsp;&nbsp;<span class=float><input type=button value="  Go  " id=cap /><?php } ?></td>
	</tr>
	<tr>
		<th class=left>Departmental SDBIP:&nbsp;</th>
		<?php
			if(!$import_status['LIST']) {
				echo "<td class=\"ui-state-error\">Please load the Lists first.";
			} elseif(!$import_status['KPI']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
		if($import_status['LIST']) { ?>&nbsp;&nbsp;<span class=float><input type=button value="  Go  " id=kpi /><?php } ?></td>
	</tr>
	<tr>
		<th class=left>Monthly Cashflow:&nbsp;</th>
		<?php
			if(!$import_status['LIST']) {
				echo "<td class=\"ui-state-error\">Please load the Lists first.";
			} elseif(!$import_status['CF']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
		if($import_status['LIST']) { ?>&nbsp;&nbsp;<span class=float><input type=button value="  Go  " id=cf /><?php } ?></td>
	</tr>
	<tr>
		<th class=left>Revenue By Source:&nbsp;</th>
		<?php
			if(!$import_status['RS']) {
				echo "<td class=\"ui-state-ok\">This section has not yet been loaded.";
			} else {
				echo "<td>Loaded.";
			}
			?>&nbsp;&nbsp;<span class=float><input type=button value="  Go  " id=rs /></span>
		</td>
	</tr>
	<tr>
		<th class=left><?php echo $prev_module['modtext']; ?> Users:&nbsp;</th>
		<?php
			if(!$import_status['LIST']) {
				echo "<td class=\"ui-state-error\">Please load the Lists first.";
			} elseif(!$import_status['USERS']) {
				if($prev_module['exists']) {
					echo "<td class=\"ui-state-ok\">Users have not yet been given menu access.&nbsp;&nbsp;<span class=float><input type=button value=\"  Go  \" id=users_".strtoupper($prev_module['modref'])." /></span>";
				} else {
					echo "<td class=\"ui-state-info\">No previous SDBIP module exists from which to import users.&nbsp;<span class=float><input type=button value=\"Open Menu Access\" id=users_menu /></span>";
				}
			} else {
				echo "<td>Done.";
			}
		 ?>
		</td>
	</tr>
	<tr>
		<th class=left>Import Log:&nbsp;</th>
		<td>&nbsp;&nbsp;<span class=float><input type=button value="  View  " id=log /></span>
		</td>
	</tr>
</table>
<?php
fclose($import_log_file);

?>
</body>
</html>