<?php 
include("inc/header.php"); 

$default_lists = array(
	'calctype'		=> array('name'=>"KPI Calculation Type"),
	'kpiconcept'	=> array('name'=>"KPI Concept"),
	'kpitype'		=> array('name'=>"KPI Type"),
	'riskrating'	=> array('name'=>"Risk Rating"),
	'targettype'	=> array('name'=>"Target Type")
);

foreach($default_lists as $tbl => $list) {
	echo "<h3>".$list['name']."</h3>";
	?>
	<table>
		<tr>
			<th>Ref</th>
			<th>List Item</th>
			<th>Code</th>
		</tr>
		<?php 
		$rs = getRS("SELECT * FROM ".$dbref."_list_".$tbl." WHERE active = true ORDER BY sort, value");
		while($row = mysql_fetch_assoc($rs)) {
		?>
			<tr>
				<th><?php echo $row['id']; ?></th>
				<td><?php echo $row['value']; ?>&nbsp;&nbsp;</td>
				<td class=centre><?php echo $row['code']; ?></td>
			</tr>
		<?php
		}
		?>
	</table>
	<?php
}



goBack("support_import.php","");
?>

</body>
</html>