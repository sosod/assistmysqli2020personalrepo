<?php
$my_head = $mheadings[$section];
$result_head = $mheadings[$r_section];
if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if(isset($my_head[$table_fld.'repkpi'])) {
	unset($my_head[$table_fld.'repkpi']);
}
if($section=="KPI" || $section == "TOP") {
	$obj_tt = $object['tt'];
	$obj_ct = $object['ct'];
}
//arrPrint($my_head);
//arrPrint($object);
?>
<h2>Details</h2>
<table width=700>
	<tr>
		<th class=left>Reference</th>
		<td><?php echo (isset($id_label)?$id_label:"").$obj_id; ?></td>
	</tr>
<?php 
foreach($my_head as $fld => $h) {
	echo chr(10)."<tr>";
		echo "<th class=left>".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;</th>";
	switch($h['h_type']) {
/*		case "CAP":
			echo "<td>".$fld;
			echo "</td>";
			break;*/
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			$values = $extras[$fld];
			$val = array();
			foreach($values as $v) {
				$val[] = ((strlen($v['code'])>0 && strtolower($v['value'])!="all") ? decode($v['code']).": " : "").decode($v['value']);
			}
			echo "<td>";
				echo implode("<br />",$val);
			echo "</td>";
			break;
		case "DATE":
			echo "<td>".($object[$fld]>0 ? date("d M Y",$object[$fld]) : "")."</td>";
			break;
		default:
			switch($fld) {
				case "top_annual":
				case "top_revised":
					echo "<td>".KPIresultDisplay($object[$fld],$obj_tt)."</td>";
					break;
				default:
					echo "<td>".$object[$fld]."</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
?>
</table>
<h2>Results</h2>
<?php //echo "ABC"; arrPrint($lists);
switch($section) {
	case "KPI":
		$width=700;
		$comment_fields = array('kr_perf','kr_correct');
		echo "<table width=$width>";
			echo "<tr>
				<td rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>
				<th colspan=3>Period Performance</th>
				<th colspan=3>Overall Performance</th>";
			foreach($comment_fields as $fld) {
				echo "<th rowspan=2>".$result_head[$fld]['h_client']."</th>";
			}
			echo "</tr>";
			echo "<tr>";
			for($i=1;$i<=2;$i++) {
				foreach($result_head as $fld => $h) {
					if(!in_array($fld,$comment_fields)) {
						echo "<th>".$h['h_client']."</th>";
					}
					if($fld=="kr_actual") {
						echo "<th>R</th>";
					}
				}
			}
			echo "</tr>";
		$values = array();
		foreach($time as $ti => $t) {
			$res_obj = $results[$ti];
			$values['target'][$ti] = $res_obj['kr_target'];
			$values['actual'][$ti] = $res_obj['kr_actual'];
			$r[1] = KPIcalcResult($values,$obj_ct,array(1,$ti),$ti);
			$r[2] = KPIcalcResult($values,$obj_ct,array(1,$ti),"ALL");
			echo "<tr>";
				echo "<th class=left>".$t['display_full']."</th>";
				for($i=1;$i<=2;$i++) {
					echo "<td class=\"right\">".KPIresultDisplay($r[$i]['target'],$obj_tt)."&nbsp;</td>";
					if($today < $t['start_stamp']) {
						echo "<td class=\"right\">&nbsp;</td>";
						echo "<td></td>";
					} else {
						echo "<td class=\"right\">".KPIresultDisplay($r[$i]['actual'],$obj_tt)."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					}
				}
				$flds = $comment_fields;
				foreach($flds as $fld) {
					echo "<td>".str_replace(chr(10),"<br />",decode($res_obj[$fld]))."</td>";
				}
			echo "</tr>";
		}
		echo "</table>";
		break;
	case "TOP":
		$width=900;
		echo "<table width=$width>";
			echo "<tr>
				<td  rowspan=2 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>
				<th  colspan=3>Period Performance</th>
				<th  colspan=3>Overall Performance</th>
				<th  rowspan=2>".$result_head['tr_perf']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				<th  rowspan=2>".$result_head['tr_correct']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				<th  rowspan=2>".$result_head['tr_dept']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				<th  rowspan=2>".$result_head['tr_dept_correct']['h_client']."<br /><img src=\"/pics/blank.gif\" height=1 width=150></th>
				</tr>";
			echo "<tr>";
			for($i=1;$i<=2;$i++) {
				foreach($result_head as $fld => $h) {
					if($fld=="tr_target" || $fld=="tr_actual") {
						echo "<th>".$h['h_client']."</th>";
					}
					if($fld=="tr_actual") {
						echo "<th>R</th>";
					}
				}
			}
			echo "</tr>";
		$values = array();
		$start = strtotime("2011-07-01 00:00:00");
		foreach($time as $ti => $t) {
			$res_obj = $results[$ti];
			$values['target'][$ti] = $res_obj['tr_target'];
			$values['actual'][$ti] = $res_obj['tr_actual'];
			$r[1] = KPIcalcResult($values,$obj_ct,array(1,$ti),$ti);
			$r[2] = KPIcalcResult($values,$obj_ct,array(1,$ti),"ALL");
			echo "<tr>";
				echo "<th class=left>Quarter ending<br />".$t['display_full']."</th>";
				for($i=1;$i<=2;$i++) {
					echo "<td class=\"right\">".KPIresultDisplay($r[$i]['target'],$obj_tt)."&nbsp;</td>";
					if($today < $start) {
						echo "<td class=\"right\">&nbsp;</td>";
						echo "<td></td>";
					} else {
						echo "<td class=\"right\">".KPIresultDisplay($r[$i]['actual'],$obj_tt)."&nbsp;</td>";
						echo "<td class=".$r[$i]['style'].">".$r[$i]['text']."</td>";
					}
				}
				$start = $t['end_stamp']+1;
				$flds = array("tr_perf","tr_correct","tr_dept","tr_dept_correct");
				foreach($flds as $fld) {
					echo "<td>".displayTRDept(str_replace(chr(10),"<br />",decode($res_obj[$fld])),"HTML")."</td>";
				}
			echo "</tr>";
		}
		echo "</table>";
		echo "<h2>Forecast Annual Target</h2>
				<table id=tblfore>";
			foreach($lists['years'] as $i => $l) {
				if($i>1) {
					echo "<tr>
							<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['tf_value']) ? $forecast[$i]['tf_value'] : 0),$obj_tt)."&nbsp;</td>
						</tr>";
				}
			}			
		echo "</table>";
		break;
	case "CAP":
		$first_head = $mheadings['CAP_H'];
		echo "<table>";
			echo "<tr>
				<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
			foreach($first_head as $fldh => $f) {
				echo "<th colspan=".count($result_head).">".$f['h_client']."</th>";
			}
			echo "</tr>";
			echo "<tr>";
			foreach($first_head as $fldh => $f) {
				foreach($result_head as $fld => $h) {
					echo "<th>".$h['h_client']."</th>";
				}
			}
			echo "</tr>";
		$values = array();
		$start = strtotime("2011-07-01 00:00:00");
		foreach($time as $ti => $t) {
			$res_obj = $results[$ti];
			echo "<tr>";
				echo "<th class=left>".$t['display_full']."</th>";
				foreach($first_head as $fldh => $f) {
					foreach($result_head as $fld => $h) {
						$r_fld = $fldh.$fld;
						echo "<td class=right>";
						if($today >= $start || $fld=="target") {
							switch($h['h_type']) {
								case "PERC":
									echo number_format($res_obj[$r_fld],2)."%";
									break;
								case "NUM":
								default:
									echo number_format($res_obj[$r_fld],2);
									break;
							}
						}
						echo "</td>";
					}
				}
				$start = $t['end_stamp']+1;
			echo "</tr>";
		}
		echo "</table>";
		echo "<h2>Forecast Annual Budget</h2>
				<table id=tblfore>
					<tr>
						<td></td>
						<th>CRR</th>
						<th>Other</th>
					</tr>";
			foreach($lists['years'] as $i => $l) {
					echo "<tr>
							<th class=left width=100>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['cf_crr']) ? $forecast[$i]['cf_crr'] : 0),1)."&nbsp;</td>
							<td class=right width=100>".KPIresultDisplay((isset($forecast[$i]['cf_other']) ? $forecast[$i]['cf_other'] : 0),1)."&nbsp;</td>
						</tr>";
			}			
		echo "</table>";
		break;
}
displayGoBack("","");
?>
