<?php 
$page_title = "Monthly Cashflow";
$import_section = "CF";
$head_section = "CF";
$get_headings = array("CF","CF_R","CF_H");
include("support_import_headings.php");
include("inc/header.php"); 

set_time_limit(1800);

$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > ".$page_title, $self);


//GET ALL LIST VALUES
$listoflists = array(
	'dir'=>				array('name'=>"Directorate",'data'=>array()),
	'subdir'=>			array('name'=>"Sub-Directorate",'data'=>array()),
	'list_gfs'=>		array('name'=>"GFS Classification",'data'=>array())
);
foreach($listoflists as $tbl => $l) {
	$rs = getRS("SELECT * FROM ".$dbref."_".$tbl." WHERE active = true");
	while($row = mysql_fetch_assoc($rs)) {
		if($tbl!="list_calctype") {
			$listoflists[$tbl]['data'][$row['id']] = $row;
		} else {
			$listoflists[$tbl]['data'][$row['code']] = $row;
		}
	}
}
//arrPrint($listoflists['list_area']);
//arrPrint($wa['area']);
//arrPrint($listoflists['list_wards']);
//arrPrint($wa['wards']);
function valError($v,$e) {
	return $v."<br /><b>-> ".$e."</b>";
}
function valList($v,$e) {
	return $v."<br />=> ".$e;
}

$file_error = array("error","An error occurred while trying to import the file.  Please go back and try again.");
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VIEW";

switch($act) {
	case "RESET":
		$r_sql = array();
		$r_sql[] = "TRUNCATE ".$dbref."_cashflow";
		$r_sql[] = "TRUNCATE ".$dbref."_cashflow_results";
		foreach($r_sql as $sql) {	
			//$mnr = db_update($sql);
			$rs = getRS($sql);
		}
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = '".$import_section."'");
		$import_status[$import_section] = false;
		$result = array("ok","All $page_title tables successfully reset.");
		logImport($import_log_file, $page_title." > Reset tables", $self);
		break;
}

?>		<style type=text/css>
		table th {
			vertical-align: top;
		}
		.ireset {
			background-color: #CC0001;
			color: #ffffff;
			line-height: 1.5em;
		}
		.dark-info {
			background: #FE9900 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		.dark-error {
			background: #CC0001 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		th.ignite { background-color: #444444; }
		th.doc { background-color: #777777; }
		th.cf { background-color: #aaaaaa; }
		</style>
<?php
logImport($import_log_file, $page_title." > ".$act, $self);
$save_echo_filename = $import_section."_".date("YmdHis")."_".$act.".html";
$onscreen = "";
switch($act) {
	case "VIEW": case "RESET": case "GENERATE":
		?>
		<script>
			$(function() {
				$(":button").click(function() {
					var id = $(this).attr("id");
					switch(id) {
						case "generate": document.location.href = 'support_import_generate.php?i=<?php echo $import_section; ?>'; break;
						case "reset": if(confirm("This will delete any existing data!\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=RESET'; } break;
					}
				});
				$("#isubmit:button").click(function() {
					var ifl = $("#ifl").attr("value");
					if(ifl.length > 0) 
						$("#list").submit();
					else
						alert("Please select a file for import.");
				});
			});
		</script>
		<?php displayResult($result); ?>
		<p>In order to generate the CSV files necessary for the import, simply go to each worksheet in the SDBIP document and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'.
		<br /><b>PLEASE ENSURE THAT THE COLUMNS MATCH THOSE AS GIVEN ON THE IMPORT PAGE FOR EACH SECTION!!!</b></p>
		<?php
$i = 1;
			?>
			<form id=list action="<?php echo $self; ?>" method=post enctype="multipart/form-data">
				<input type=hidden name=act value=PREVIEW />
				<table>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Generate the CSV file:
							<br />+ <input type=button value=Generate id=generate /> the CSV template here and copy the data into the sheet from the SDBIP template  OR
							<br />+ Open the SDBIP template, go to the Lists sheet, click File > Save As & change the File Type option to Comma-delimited (*.csv).
							<br />&nbsp;<br />Please ensure that the columns match up to those given below.&nbsp;
						</td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reset the tables to delete previously loaded information.&nbsp;<span class=float><input type=button value="Reset Tables" id=reset class=idelete /></span></td>
					</tr>
					<?php if(!$import_status[$import_section]) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Select the CSV file and click the Import button. &nbsp;&nbsp;<input type=file name=ifile id=ifl />&nbsp;<span class=float><input type=button id=isubmit value=Import class=isubmit /></span></td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Review the information.  If the information is correct, click the Accept button.
						<br /><b>NO DATA WILL BE IMPORTED UNTIL THE FINAL ACCEPT BUTTON HAS BEEN CLICKED!!</b></td>
					</tr>
					<?php } ?>
				</table>
			</form>
			<?php displayGoBack("support_import.php"); 
			
			drawCSVColumnsGuidelines("cf_id",$target_text);
			
		break;	//switch act case view/reset/generate
	case "PREVIEW":
		$onscreen.= "<h1 class=centre>$page_title Preview</h1><p class=centre>Please click the \"Accept\" button at the bottom of the page<br />in order to finalise the import of the list items.</p>";
		$onscreen.= "<p class=centre>Any errors highlighted in <span class=dark-error>red</span> are errors that prevent the import (fatal errors).<br />The \"Accept\" button will not be available until all of these errors have been corrected.</p>";
		$onscreen.= "<p class=centre>Any cells highlighted in <span class=dark-info>orange</span> are where the value associated with the given list reference does not match the value given in the next cell.<br />These errors do not prevent the import (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Please remember, if you get any \"Too long\" errors, that any non-alphanumeric characters in text fields are converted a special code to allow them to be uploaded to the database.
		<br />This code is generally 6 characters long and counts towards the overall length of the text field.</p>";
	case "ACCEPT":
		if($act!="PREVIEW") {
			$accept_text = "<h1 class=centre>Processing...</h1><p class=centre>The import is being processed.<br />Please be patient.  You will be redirected back to the Support >> Import page when the processing is complete.</p>";
			$onscreen.= $accept_text;
			echo $accept_text;
		}
		$onscreen.= "<form id=save method=post action=\"$self\">";
		$onscreen.= "<input type=hidden name=act value=ACCEPT />";
		$err = false;
		$file_location = $modref."/import/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$modref."/import/";
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				logImport($import_log_file, $page_title." > ERROR > $file doesn't exist", $self);
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [Too big]", $self);
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [No file found]", $self);
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"], $self);
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					logImport($import_log_file, $page_title." > ERROR > Invalid file extension - ".$ext, $self);
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
					logImport($import_log_file, $page_title." > $file uploaded", $self);
				}
			}
		}
		//READ FILE
		if(!$err) {
			$onscreen.= "<input type=hidden name=f value=".(isset($filen) ? $filen : $_REQUEST['f'])." />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);

			if(count($data)>2) {
				$doc_headings = $data[0];
				$doc_headings2 = $data[1];
				unset($data[0]); unset($data[1]);
//		$fdata = "\"Sub-Directorate [R]\",\" \",\" \",\"Line Item [R]\",\"GFS Classification [R]\",\" \",\"Vote Number\",\" \",\"July 2011\",\" \",\" \",\"August 2011\",\" \",\" \",\"September 2011\",\" \",\" \",\"October 2011\",\" \",\" \",\"November 2011\",\" \",\" \",\"December 2011\",\" \",\" \",\"January 2012\",\" \",\" \",\"February 2012\",\" \",\" \",\"March 2012\",\" \",\" \",\"April 2012\",\" \",\" \",\"May 2012\",\" \",\" \",\"June 2012\",\" \",\" \"\n";
//		$fdata.= "\"Ignite\",\"Directorate\",\"List\",\"200 characters\",\"Ignite\",\"List\",\"100 characters\",\" \",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\"";
		$columns[0] = array("Sub-Directorate [R]","","","Line Item [R]","GFS Classification [R]","","Vote Number"," ","July 2011","","","August 2011","","","September 2011","","","October 2011","","","November 2011","","","December 2011","","","January 2012","","","February 2012","","","March 2012","","","April 2012","","","May 2012","","","June 2012","","");
		$columns[1] = array("Ignite","Directorate","List","200 characters","Ignite","List","100 characters"," ","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.","Revenue","Op.&nbsp;Exp.","Cap.&nbsp;Exp.");
				$colspanned[2] = array(4);
				$colspanned[3] = array(0);
				/*$cells = array(
					0=>'cf_subid',
					3=>'cf_value',
					4=>'cf_gfsid',
					6=>'cf_vote',
					8=>'cf_rev_1_1',
					9=>'cf_opex_1_1',
					10=>'cf_capex_1_1',
					11=>'cf_rev_1_2',
					12=>'cf_opex_1_2',
					13=>'cf_capex_1_2',
					14=>'cf_rev_1_3',
					15=>'cf_opex_1_3',
					16=>'cf_capex_1_3',
					17=>'cf_rev_1_4',
					18=>'cf_opex_1_4',
					19=>'cf_capex_1_4',
					20=>'cf_rev_1_5',
					21=>'cf_opex_1_5',
					22=>'cf_capex_1_5',
					23=>'cf_rev_1_6',
					24=>'cf_opex_1_6',
					25=>'cf_capex_1_6',
					26=>'cf_rev_1_7',
					27=>'cf_opex_1_7',
					28=>'cf_capex_1_7',
					29=>'cf_rev_1_8',
					30=>'cf_opex_1_8',
					31=>'cf_capex_1_8',
					32=>'cf_rev_1_9',
					33=>'cf_opex_1_9',
					34=>'cf_capex_1_9',
					35=>'cf_rev_1_10',
					36=>'cf_opex_1_10',
					37=>'cf_capex_1_10',
					38=>'cf_rev_1_11',
					39=>'cf_opex_1_11',
					40=>'cf_capex_1_11',
					41=>'cf_rev_1_12',
					42=>'cf_opex_1_12',
					43=>'cf_capex_1_12'
				);*/
				$listinfo = array(
					'cf_subid' 		=> array('tbl' => "subdir", 'other' => 2, 'dir'=>1),
					'cf_gfsid' 		=> array('tbl' => "list_gfs", 'other' => 5)
				);
				
				$onscreen.="<table>".getImportHeadings();
					/*$onscreen.="<tr><th>?</td>";
						foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 3 : 1;
							$colspan = ($cell==4) ? 2 : $colspan;
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan >".$c."</th>";
							}
						}
					$onscreen.="</tr>";
					$onscreen.="<tr><th>?</td>";
						foreach($columns[1] as $cell => $c) {
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan >".$c."</th>";
							}
						}*/
					/*$onscreen.= "<tr><th rowspan=3 style='vertical-align: middle;'> ? </td>";
						foreach($columns[0] as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 3 : 1;
							$colspan = ($cell==4) ? 2 : $colspan;
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan class=ignite >".$c."</th>";
							}
						}
					$onscreen.= "</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							$colspan = in_array($cell,$colspanned) ? 3 : 1;
							$colspan = ($cell==4) ? 2 : $colspan;
							if( $cell<44 && (strlen($c)>0 || $cell == 7)) {
								$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
							}
						}
					$onscreen.= "</tr><tr>";
						foreach($columns[1] as $cell => $c) {
							if($cell < 7) { $c = " "; }
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan class=cf >".$c."</th>";
							}
						}
					$onscreen.="</tr>";*/
					
					$t_sql = array();
					$r_sql = array();
					$f_sql = array();
					$refs = array();
					$values = array();
					$all_valid = true;
					$error_count = 0;
					$info_count = 0;
					foreach($data as $i => $d) {
						$targettype = 0;
						$valid = true;
						$row = array();
						$display = array();
						$val = array();
						foreach($cells as $c => $fld) {
							$v = $d[$c];
							$val[$c][0] = 1;
							//DATA VALIDATION HERE
							switch($fld) {
								case "cf_subid":		//DIRECTORATE
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];
									$dir = $listinfo[$fld]['dir'];

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
										$val[$dir] = array(1,$d[$dir]);
									}
									break;
								case "cf_gfsid":
									$tbl = $listinfo[$fld]['tbl'];
									$other = $listinfo[$fld]['other'];

									if(strlen(trim($v))==0) {			//required
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(!isset($listoflists[$tbl]['data'][$v])) {	//valid directorate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"No such ".$listoflists[$tbl]['name']." exists");
									} else {
										$v2 = strtolower(trim(code($d[$other])));
										$me = strtolower($listoflists[$tbl]['data'][$v]['value']);
										if($v2!=$me) {	
											$val[$c][0] = 2;
											$info_count++;
										}
										$val[$c][1] = valList($v,$listoflists[$tbl]['data'][$v]['value']);
										$row[$fld] = $v;
										$val[$other] = array(1,$d[$other]);
									}
									break;
								case "cf_value":		//PMS REF
								case "cf_vote":
									$len = $mheadings[$head_section][$fld]['c_maxlen'];
									$type = $mheadings[$head_section][$fld]['h_type'];
									if(($mheadings[$head_section][$fld]['i_required']+$mheadings[$head_section][$fld]['c_required'])>0 && strlen(trim(code($v))) == 0) {			//required field
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(strlen(trim(code($v)))> $len && $type!="TEXT") {			//too long
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Too long: ".strlen(trim(code($v)))." chars (max ".$len." chars)".$fld);
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = trim(code($v));
									}
									break;
								case "cf_rev_1_1":
								case "cf_opex_1_1":
								case "cf_capex_1_1":
								case "cf_rev_1_2":
								case "cf_opex_1_2":
								case "cf_capex_1_2":
								case "cf_rev_1_3":
								case "cf_opex_1_3":
								case "cf_capex_1_3":
								case "cf_rev_1_4":
								case "cf_opex_1_4":
								case "cf_capex_1_4":
								case "cf_rev_1_5":
								case "cf_opex_1_5":
								case "cf_capex_1_5":
								case "cf_rev_1_6":
								case "cf_opex_1_6":
								case "cf_capex_1_6":
								case "cf_rev_1_7":
								case "cf_opex_1_7":
								case "cf_capex_1_7":
								case "cf_rev_1_8":
								case "cf_opex_1_8":
								case "cf_capex_1_8":
								case "cf_rev_1_9":
								case "cf_opex_1_9":
								case "cf_capex_1_9":
								case "cf_rev_1_10":
								case "cf_opex_1_10":
								case "cf_capex_1_10":
								case "cf_rev_1_11":
								case "cf_opex_1_11":
								case "cf_capex_1_11":
								case "cf_rev_1_12":
								case "cf_opex_1_12":
								case "cf_capex_1_12":
									$v = trim($v);
									if(!is_numeric($v) && strlen($v)>0) {	//invlid number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data: number only");
									} else {
										if(strlen($v)==0) {
											$v = 0;
										}
										$row[$fld] = $v;
										$val[$c][1] = $v;
										if($targettype==2 && $v <=1 && $v>0) {
											$val[$c][0]=2; $info_count++;
										}
									}
									break;
								default:
									//$val[$c][1] = $v;
									//$row[$fld] = $v;
							}
						}
						$val['a'] = $valid;
						$onscreen.= "<tr>";
							switch($val['a']) {
								case true:
									$onscreen.= "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									break;
								case false:
								default:
									$onscreen.= "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
							}
							foreach($columns[1] as $cell => $c) {
								if(isset($val[$cell])) {
									$v = $val[$cell];
									switch($v[0]) {
										case 0:
											$onscreen.= "<td class=\"dark-error\">";
											break;
										case 2:
											$onscreen.= "<td class=\"dark-info\">";
											break;
										case 1:
										default:
											$onscreen.= "<td ".($cell>7 ? "class=right" : "").">";
									}
									$onscreen.= $cell>7 ? number_format($val[$cell][1],2,".",",") : $val[$cell][1];
									$onscreen.= "</td>";
								} else {
									$onscreen.= "<td></td>";
								}
							}
						$onscreen.= "</tr>";
						if($valid) {
							$values[$i] = $val;
							$row['cf_id'] = $i + 1;
							//CASHFLOW
							$t_sql[] = "(".$row['cf_id'].", ".$row['cf_subid'].", '".$row['cf_value']."', ".$row['cf_gfsid'].", '".$row['cf_vote']."',$i,true)";
							//RESULTS
							$tot_targ = array('rev'=>0,'opex'=>0,'capex'=>0);
							//$ytd_targ = array();
							$secfld = array("rev","opex","capex");
							$res_sql = "";
							for($z=1;$z<=12;$z++) {
								$res_sql.="(null,".$row['cf_id'].",".$z;
								//$ytd_targ[$z] = array('rev'=>0,'opex'=>0,'capex'=>0);
								foreach($secfld as $sf) {
									$tot_targ[$sf]+=$row['cf_'.$sf.'_1_'.$z];
									//$ytd_targ[$z][$sf]+=$tot_targ[$sf];
									$res_sql.=",".$row['cf_'.$sf.'_1_'.$z].",".$row['cf_'.$sf.'_1_'.$z].",".$tot_targ[$sf];
								}
								$res_sql.=",".$tot_targ['rev'].",".$tot_targ['opex'].",".$tot_targ['capex'].")".($z<12 ? "," : "");
							}
							/*	foreach($secfld as $sf) {
									$res_sql.=",".$row['cf_'.$sf.'_1_'.$z].",".$row['cf_'.$sf.'_1_'.$z].",".$ytd_targ[$z][$sf].",".$tot_targ[$sf];
								}
								$res_sql.=")";*/
								//if($z<12) { $res_sql.=","; }
							$r_sql[] = "INSERT INTO ".$dbref."_cashflow_results (cr_id, cr_cfid, cr_timeid, cr_rev_1, cr_rev_4, cr_rev_8, cr_opex_1, cr_opex_4, cr_opex_8, cr_capex_1, cr_capex_4, cr_capex_8, cr_rev_12, cr_opex_12, cr_capex_12) VALUES ".$res_sql;
							/*(null,".$row['cf_id'].",1,".$row['cf_rev_1_1'].",0,0,0,0,0,0,0,".$row['cf_capex_1_1'].",0,0,0,0,0,0,0,".$row['cf_capex_1_1'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",2,".$row['cf_rev_1_2'].",0,0,0,0,0,0,0,".$row['cf_capex_1_2'].",0,0,0,0,0,0,0,".$row['cf_capex_1_2'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",3,".$row['cf_rev_1_3'].",0,0,0,0,0,0,0,".$row['cf_capex_1_3'].",0,0,0,0,0,0,0,".$row['cf_capex_1_3'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",4,".$row['cf_rev_1_4'].",0,0,0,0,0,0,0,".$row['cf_capex_1_4'].",0,0,0,0,0,0,0,".$row['cf_capex_1_4'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",5,".$row['cf_rev_1_5'].",0,0,0,0,0,0,0,".$row['cf_capex_1_5'].",0,0,0,0,0,0,0,".$row['cf_capex_1_5'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",6,".$row['cf_rev_1_6'].",0,0,0,0,0,0,0,".$row['cf_capex_1_6'].",0,0,0,0,0,0,0,".$row['cf_capex_1_6'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",7,".$row['cf_rev_1_7'].",0,0,0,0,0,0,0,".$row['cf_capex_1_7'].",0,0,0,0,0,0,0,".$row['cf_capex_1_7'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",8,".$row['cf_rev_1_8'].",0,0,0,0,0,0,0,".$row['cf_capex_1_8'].",0,0,0,0,0,0,0,".$row['cf_capex_1_8'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",9,".$row['cf_rev_1_9'].",0,0,0,0,0,0,0,".$row['cf_capex_1_9'].",0,0,0,0,0,0,0,".$row['cf_capex_1_9'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",10,".$row['cf_rev_1_10'].",0,0,0,0,0,0,0,".$row['cf_capex_1_10'].",0,0,0,0,0,0,0,".$row['cf_capex_1_10'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",11,".$row['cf_rev_1_11'].",0,0,0,0,0,0,0,".$row['cf_capex_1_11'].",0,0,0,0,0,0,0,".$row['cf_capex_1_11'].",0,0,0,0,0,0,0), 
							(null,".$row['cf_id'].",12,".$row['cf_rev_1_12'].",0,0,0,0,0,0,0,".$row['cf_capex_1_12'].",0,0,0,0,0,0,0,".$row['cf_capex_1_12'].",0,0,0,0,0,0,0)";*/
						} else {
							$all_valid = false;
						}
						//arrPrint($row);
					}
					
					
				$onscreen.="</table>";
				
				if($act!="ACCEPT") {
					$onscreen.= "<p class=centre>There are $error_count fatal error(s).<br />There are $info_count non-fatal error(s).</p>";
					$onscreen.= "<p class=centre><input type=submit value=\"Accept\" class=isubmit id=submit_me /> <input type=button id=reset_me class=idelete value=Reject /><p>";
					if(!$all_valid || $error_count > 0) {
						$onscreen.= "<script>$(function() {
							$(\"#submit_me\").attr(\"disabled\",true);
							$(\"#submit_me\").removeClass(\"isubmit\");
							$(\"#reset_me\").attr(\"disabled\",true);
							$(\"#reset_me\").removeClass(\"ireset\");
						});</script>";
					}
					//echo "<p class=centre>Warning: When you click the Accept button, only those list items with a <span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> will be imported.</p>";
					echo $onscreen;
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
				} else {
					$z_sql = array();
					$z=0;
					$z_sql[$z] = "INSERT INTO ".$dbref."_cashflow (cf_id, cf_subid, cf_value, cf_gfsid, cf_vote, cf_sort, cf_active) VALUES ";
					$a = 0;
					foreach($t_sql as $t) {
						if($a>=50) {
							$z++;
							$z_sql[$z] = "INSERT INTO ".$dbref."_cashflow (cf_id, cf_subid, cf_value, cf_gfsid, cf_vote, cf_sort, cf_active) VALUES ";
							$a=0;
						} elseif($a>0) {
							$z_sql[$z].=",";
						}
						$z_sql[$z].=$t;
						$a++;
					}
					foreach($z_sql as $key => $sql) {
						$id = db_insert($sql);
						//echo "<P>".$id;
						$onscreen.="<P>".$sql;
					}
					foreach($r_sql as $key => $sql) {
						$id = db_insert($sql);
						//echo "<P>".$id;
						$onscreen.="<P>".$sql;
					}
					db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = '$import_section'");
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
					echo "<script>document.location.href = 'support_import.php?r[]=ok&r[]=Monthly+Cashflow+imported.';</script>";
				}
				echo "</form>";
			} else {	//data count
				logImport($import_log_file, $page_title." > ERROR > No info to upload [".$file."]", $self);
				die(displayResult(array("error","Couldn't find any information to import.")));
			}
			
		} else {	//if !err
			die(displayResult($file_error));
		}
		break;	//switch act case accept
}	//switch act
?>

</body>
</html>