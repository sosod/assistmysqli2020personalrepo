<?php 
$get_lists = false;
$section = "KPI";
$page_title = "Detail";
$obj_id = $_REQUEST['id'];

include("inc/header.php"); 

//arrPrint($mheadings[$section]);

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			break;
		default: $fields[] = "k.".$fld;
	}
}

$object_sql = "SELECT dir.value, targettype.id as tt, calctype.code as ct, ".implode(", ",$fields)."
FROM ".$dbref."_kpi k
INNER JOIN ".$dbref."_subdir as subdir ON k.kpi_subid = subdir.id
INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE k.kpi_id = ".$obj_id;
//echo "<P>".$object_sql;
$object = mysql_fetch_all($object_sql);
$object = $object[0];

$extras = array();
//WARDS
$fld = "kpi_wards";
$tbl = "wards";
$a = "w";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.k".$a."_listid AND b.k".$a."_active = true AND b.k".$a."_kpiid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_all($a_sql);
//AREAS
$fld = "kpi_area";
$tbl = "area";
$a = "a";
$a_sql = "SELECT a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.k".$a."_listid AND b.k".$a."_active = true AND b.k".$a."_kpiid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_all($a_sql);



if($object['kpi_topid']>0) {
	$top_sql = "SELECT top_value FROM ".$dbref."_top WHERE top_id = ".$object['kpi_topid']." AND top_active = true";
	$o = array();
	$o = mysql_fetch_all($top_sql);
	if(isset($o[0])) {
		$object['kpi_topid'] = $o[0]['top_value']." [".$id_labels_all['TOP'].$object['kpi_topid']."]";
	} else {
		$object['kpi_topid'] = "N/A";
	}
} else {
	$object['kpi_topid'] = "N/A";
}
if($object['kpi_capitalid']>0) {
	$top_sql = "SELECT cap_name FROM ".$dbref."_capital WHERE cap_id = ".$object['kpi_capitalid']." AND cap_active = true";
	$o = array();
	$o = mysql_fetch_all($top_sql);
	if(isset($o[0])) {
		$object['kpi_capitalid'] = $o[0]['cap_name']." [".$id_labels_all['CAP'].$object['kpi_capitalid']."]";
	} else {
		$object['kpi_capitalid'] = "N/A";
	}
} else {
	$object['kpi_capitalid'] = "N/A";
}

$result_sql = "SELECT * FROM ".$dbref."_kpi_results WHERE kr_kpiid = ".$obj_id." ORDER BY kr_timeid";
$results = mysql_fetch_alls($result_sql,"kr_timeid");


include("view_table_detail.php");


$plc = getValidPLC($obj_id);
if($plc[0]) {
	$plcrow = $plc[1];
	echo drawPLC($plcrow);
	displayGoBack("","");
}


$log_sql = "SELECT klog_date, CONCAT_WS(' ',tkname,tksurname) as klog_tkname, klog_transaction FROM ".$dbref."_kpi_log INNER JOIN assist_".$cmpcode."_timekeep ON klog_tkid = tkid WHERE klog_kpiid = '".$obj_id."' AND klog_yn = 'Y' ORDER BY klog_id DESC";
displayAuditLog($log_sql,array('date'=>"klog_date",'user'=>"klog_tkname",'action'=>"klog_transaction"));

?>