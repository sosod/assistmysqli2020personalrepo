<?php 
include("inc/header.php"); 

$sections = array(
	'KPI'=>"Departmental SDBIP",
	'TOP'=>"Top Layer SDBIP",
	'CAP'=>"Capital Projects",
	'CF'=>"Monthly Cashflow",
	'RS'=>"Revenue By Source",
	'LIST'=>"Lists"
);

foreach($sections as $key => $sec) {
	$toc[] = "<a href=#".$key.">".$sec."</a>";
}

echo "<p style=\"margin-top: -10px; font-size: 7.5pt\"><a name=top></a>".implode(" | ",$toc)."</p>";


foreach($sections as $key => $sec) {
	echo "<h2><a name=".$key."></a>".$sec."</h2>";
	if($key!="LIST") {
		$gloss = 0;
		echo "<table width=600px>";
			foreach($mheadings[$key] as $fld => $h) {
				if(strlen($h['glossary'])>0) {
					$gloss++;
					echo "<tr>";
						echo "<th style=\"vertical-align: top;\" class=left>".$h['h_client'].":&nbsp;</td>";
						echo "<td>".$h['glossary']."</td>";
					echo "</tr>";
				}
			}
		echo "</table>";
		if(!$gloss) {
			echo "<p>No definitions available.</p>";
		}
		echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
	} else {
		$sql = "SELECT * FROM ".$dbref."_setup_headings WHERE h_type = 'LIST' AND h_table NOT IN ('dir','subdir','owner','targettype')";
		$rs = getRS($sql);
		while($h = mysql_fetch_assoc($rs)) {
			echo "<h3>".$h['h_client']."</h3>";
			?>
			<table>
				<tr>
					<th>Ref</th>
					<th>List Item</th>
					<?php if(in_array($h['h_table'],$code_lists)) { echo "<th>Code</th>"; } ?>
					<?php if($h['h_table']=="calctype") { echo "<th>Description</th>"; } ?>
				</tr>
				<?php 
				$rs2 = getRS("SELECT * FROM ".$dbref."_list_".$h['h_table']." WHERE active = true ORDER BY sort, value");
				while($row = mysql_fetch_assoc($rs2)) {
				?>
					<tr>
						<th><?php echo $row['id']; ?></th>
						<td><?php echo $row['value']; ?>&nbsp;&nbsp;</td>
						<?php if(in_array($h['h_table'],$code_lists)) { echo "<td class=centre>".$row['code']."</td>"; } ?>
					<?php if($h['h_table']=="calctype") { echo "<td>".$row['descrip']."</td>"; } ?>
					</tr>
				<?php
				}
				?>
			</table>
			<?php
			echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		}
	}
	echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
}

?>
</body>
</html>