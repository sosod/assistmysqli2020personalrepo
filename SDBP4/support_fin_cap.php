<?php 
$page_title = "Capital Projects";
$section = "CAP";
$log_fld = "clog_";
include("inc/header.php"); 
$ref_lbl = "LOG_".$id_labels_all[$section];

$log_sql = "SELECT l.*, CONCAT_WS(' ',tkname,tksurname) as clog_tkname FROM ".$dbref."_capital_log l
			INNER JOIN assist_".$cmpcode."_timekeep ON clog_tkid = tkid 
			WHERE clog_capid = '0' AND clog_yn = 'Y' ORDER BY clog_date DESC";

$rs = getRS($log_sql);
$logs = array();
$log_files = array();
while($row = mysql_fetch_assoc($rs)) {
	$logs[$row['clog_id']] = $row;
	$log_files[] = $row['clog_new'];
}
mysql_close();
?>
<table>
	<tr>
		<th>Log ID</th>
		<th>Date</th>
		<th>User</th>
		<th>Activity</th>
		<th>File</th>
	</tr>
<?php
foreach($logs as $i => $l) {
	echo "<tr>
			<td>".$ref_lbl.$i."</td>
			<td>".$l[$log_fld.'date']."</td>
			<td>".$l[$log_fld.'tkname']."</td>
			<td>".$l[$log_fld.'transaction']."</td>
			<td>".$l[$log_fld.'new']."</td>
		</tr>";
}
?>
</table>
</body>
</html>