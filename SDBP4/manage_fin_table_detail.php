<?php 
function maxlenOption($v) {
	global $select_option_maxlen;
	if(strlen($v)>$select_option_maxlen) {
		$v = substr($v,0,($select_option_maxlen-3))."...";
	}
	return $v;
}
$select_option_maxlen = 115;
$my_head = $mheadings[$section];
$result_head = $mheadings[$r_section];
if($section=="KPI" || $section == "TOP") {
	$obj_tt = $object['tt'];
	$obj_ct = $object['ct'];
}

?>
<form id=edit method=post action=<?php echo $self; ?> style="margin-top: -5px;">
<input type=hidden name=page_id id=p_id value="<?php echo $page_id; ?>" />
<input type=hidden name=tab value="<?php echo $tab; ?>" />
<input type=hidden name=act value="SAVE" />
<table class=noborder>
	<tr class=no-highlight>
		<td class=noborder>
<h2>Details</h2>
<?php
echo "<table id=tbl>
	<tr>
		<th class=left>Reference</th>
		<td>".(isset($id_label)?$id_label:"").$obj_id."<input type=hidden name=id id=obj_id value=".$obj_id." /></td>
	</tr>";

foreach($my_head as $fld => $h) {
	echo chr(10)."<tr>";
		echo "<th class=\"left top\">".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).":&nbsp;".($h['i_required']+$h['c_required']>0 ? "**<input type=hidden id=".$fld."-r value=Y />" : "<input type=hidden id=".$fld."-r value=N />")."</th>";
	switch($h['h_type']) {
		case "CAP":
			echo "<td>";
				if(isset($object[$fld]) && $object[$fld]>0) {
					echo "<label for=".$fld." id=".$fld."_disp>".$object[$fld.'_disp']."</label><input type=hidden name=".$fld." id=".$fld." value=".$object[$fld]." /><span class=float><input type=button value=\"Remove Link\" class=idelete id=del_cap />";
				} else {
					echo "<select name=$fld id=$fld>";
						echo "<option ".(!isset($object[$fld]) || $object[$fld]==0 ? "selected" : "")." value=0>N/A</option>";
						$other_sql = "SELECT cap_id as id, cap_name as value FROM ".$dbref."_capital WHERE cap_active = true AND cap_id NOT IN (SELECT kpi_capitalid FROM ".$dbref."_kpi WHERE kpi_capitalid > 0 AND kpi_active = true)";
						$others = mysql_fetch_alls($other_sql,"id");
						foreach($others as $i => $o) {
							$v = maxlenOption(decode($o['value']));
							echo "<option ".($object[$fld]==$i ? "selected" : "")." value=".$i.">[".$id_labels_all[$h['h_type']].$i."] ".$v."</option>";
						}
					echo "</select>";
				}
			echo "</td>";
			break;
		case "TOP":
			echo "<td>";
				echo $object[$fld];
			echo "</td>";
			break;
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			$values = $extras[$fld];
			$val = array();
			foreach($values as $v) {
				$val[] = ((strlen($v['code'])>0 && strtolower($v['value'])!="all") ? decode($v['code']).": " : "").maxlenOption(decode($v['value']));
			}
			echo "<td>";
			//arrPrint($values);
			//arrPrint($lists[$h['h_table']]);
				//echo implode("<br />",$val);
			if(count($lists[$h['h_table']])>1) {
				echo "<select name=".$fld."[] id=$fld multiple ".(count($lists[$h['h_table']])<=10 ? "size=".count($lists[$h['h_table']]) : "size=10").">";
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<option ".(isset($values[$i]) ? "selected " : "")."value=$i>".(isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").maxlenOption(decode($l['value']))."</option>";
					}
				echo "</select>";
				echo "<br /><span class=\"ctrlclick iinform\">CTRL + Click to select multiple options</span>";
			} else {
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<input type=hidden name=".$fld."[] id=$fld value=$i>".(isset($l['code']) && strlen($l['code'])>0 && strtoupper($l['value'])!="ALL" ? $l['code'].": " : "").maxlenOption(decode($l['value']))."";
					}
			}
			echo "</td>";
			break;
		case "DATE":
			echo "<td><input type=text class=datepr value=\"".($object[$fld]>0 ? date("Y-m-d",$object[$fld]) : "")."\" name=".$fld." id=".$fld." /> ".(strpos($fld,"_plan")>0 ? "" : "<input type=button value=Clear class=cleardate id=c-".$fld." />")."</td>";
			break;
		case "LIST":
			echo "<td>";
			if($tab!="create") { echo "<input type=hidden id=lbl-".$fld."-v value=\"".$object['o'.$fld]."\" /><label for=".$fld." id=lbl-".$fld."-text>".$object[$fld]."</label><span id=lbl-".$fld."-lb></span>"; }
			echo "<select id=".$fld." name=".$fld."><option value=0>--- SELECT ---</option>";
			if($h['h_table']=="subdir") {
					foreach($lists['dir'] as $di => $dl) {
						$d = $dl['value'];
						foreach($lists[$h['h_table']]['dir'][$di] as $i => $l) {
							echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i.">".maxlenOption(decode($d." - ".$l['value']))."</option>";
						}
					}
			} else {
					foreach($lists[$h['h_table']] as $i => $l) {
						echo "<option ".($i==$object['o'.$fld] ? "selected" : "")." value=".$i.">".maxlenOption((isset($l['code']) && strlen($l['code'])>0 && strlen(trim($l['value']))>0 ? $l['code'].": " : "").decode($l['value']))."</option>";
					}
			}
			echo "</select>";
			echo "</td>";
			break;
		case "BOOL":
			echo "<td>";
				echo "<input type=hidden id=lbl-".$fld."-v value=\"".$object[$fld]."\" /><label for=".$fld." id=lbl-".$fld."-text>".$object[$fld]."</label><span id=lbl-".$fld."-lb></span>";
				echo "<select id=".$fld." name=".$fld."><option ".($object[$fld]!=0 ? "selected" : "")." value=1>Yes</option><option ".(!$object[$fld] ? "selected" : "")." value=0>No</option>";
				echo "</select>";
			echo "</td>";
			break;
		default:
			switch($fld) {
				case "top_annual":
				case "top_revised":
					echo "<td>".KPIresultEdit($object[$fld],$obj_tt,$fld)."</td>";
					break;
				default:
					echo "<td>";
					if($h['h_type']=="TEXT" || $h['c_maxlen']>99) {
						echo "<textarea rows=4 cols=60 name=$fld id=$fld>".decode($object[$fld])."</textarea>";
						if($h['h_type']!="TEXT") {
							echo "<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
						}
					} else {
						echo "<input type=text id=".$fld." name=".$fld." value=\"".$object[$fld]."\" size=".($h['c_maxlen']>90 ? "90" : $h['c_maxlen']+5)." maxlength=".$h['c_maxlen']." /> 
							<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
					}
					echo "</td>";
					break;
			}
			break;
	}
	echo "</tr>";
}
 ?>
</table>
<?php displayGoBack("",""); ?>
<?php 
if($tab=="create" || $section=="CF") {



	//arrPrint($mheadings['CAP_H']);
	switch($section) {
		case "CAP":
		//container table
		echo "<table class=noborder><tr><td class=noborder style=\"padding-right: 20px\">
		<h2>Results</h2>";
			$first_head = $mheadings['CAP_H'];
			echo "<table id=tbl_res>";
				echo "<tr>
					<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
				foreach($first_head as $fldh => $f) {
					echo "<th colspan=1>".$f['h_client']."</th>";
					break;
				}
				echo "</tr>";
				echo "<tr>";
				foreach($first_head as $fldh => $f) {
					foreach($result_head as $fld => $h) {
						echo "<th>".$h['h_client']."</th>";
						break;
					}
					break;
				}
				echo "</tr>";
			$values = array();
			$start = strtotime("2011-07-01 00:00:00");
			foreach($time as $ti => $t) {
				$res_obj = $results[$ti];
				echo "<tr>";
					echo "<th class=left>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
					foreach($first_head as $fldh => $f) {
						foreach($result_head as $fld => $h) {
							$r_fld = $fldh.$fld;
							echo "<td class=right>";
							if($fldh!="cr_" || $fld!="target") {
								switch($h['h_type']) {
									case "PERC":
										echo FINresultDisplay($res_obj[$r_fld],2);
										break;
									case "NUM":
									default:
										echo FINresultDisplay($res_obj[$r_fld],1);
										break;
								}
							} else {
								echo KPIresultEdit(0,1,"cr_target_".$ti);
							}
							echo "</td>";
							break;
						}
						break;
					}
					$start = $t['end_stamp']+1;
				echo "</tr>";
			}
			echo "</table>";
		echo "</td><td class=noborder style=\"padding-left: 20px\">";
			echo "<h2>Forecast Annual Budget</h2>
					<table id=tblfore><tr><td></td><th>CRR</th><th>Other</th></tr>";
					foreach($lists['years'] as $i => $l) {
						echo "<tr>
								<th>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
								<td>".KPIresultEdit(0,1,"years[crr][$i]")."&nbsp;</td>
								<td>".KPIresultEdit(0,1,"years[other][$i]")."&nbsp;</td>
							</tr>";
					}		
			echo "</table>";
		echo "</td></tr></table>";
			break;
	case "CF":
		echo "<h2>Results</h2>";
		echo "<h3>Budget Only</h3>";
			$first_head = $mheadings['CF_H'];
			echo "<table id=tblres>";
				echo "<tr>
					<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
				foreach($first_head as $fldh => $f) {
					echo "<th colspan=1>".$f['h_client']."</th>";
				}
				echo "</tr>";
				echo "<tr>";
				foreach($first_head as $fldh => $f) {
					foreach($result_head as $fld => $h) {
						echo "<th>".$h['h_client']."</th>";
						break;
					}
				}
				echo "</tr>";
			$values = array();
			$start = strtotime("2011-07-01 00:00:00");
			foreach($time as $ti => $t) {
				$res_obj = isset($results[$ti]) ? $results[$ti] : array();
				echo "<tr>";
					echo "<th class=left>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
					foreach($first_head as $fldh => $f) {
						foreach($result_head as $fld => $h) {
							$r_fld = $r_table_fld.$fldh.$fld;
							$val = (isset($res_obj[$r_fld]) ? $res_obj[$r_fld] : 0);
							echo "<td class=right>";
							if($fld!="_1") {
								switch($h['h_type']) {
									case "PERC":
										echo FINresultDisplay($val,2);
										break;
									case "NUM":
									default:
										echo FINresultDisplay($val,1);
										break;
								}
							} else {
								echo ($tab=="create" || $t['active_finance']) ? KPIresultEdit($val,1,$r_fld."[".$ti."]") : FINresultDisplay($val,1)."<input type=hidden name=".$r_fld."[".$ti."]"." value=".$val." />";
							}
							echo "</td>";
							break;
						}
					}
					$start = $t['end_stamp']+1;
				echo "</tr>";
			}
			echo "</table>";		
			break;	}

	
	

} else { //else if tab


	//arrPrint($mheadings['CAP_H']);
	switch($section) {
		case "CAP":
		echo "<h2>Results</h2>";
			$first_head = $mheadings['CAP_H'];
			echo "<table id=tblres>";
				echo "<tr>
					<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
				foreach($first_head as $fldh => $f) {
					echo "<th colspan=".count($result_head).">".$f['h_client']."</th>";
				}
				echo "</tr>";
				echo "<tr>";
				foreach($first_head as $fldh => $f) {
					foreach($result_head as $fld => $h) {
						echo "<th>".$h['h_client']."</th>";
					}
				}
				echo "</tr>";
			$values = array();
			$start = strtotime("2011-07-01 00:00:00");
			foreach($time as $ti => $t) {
				$res_obj = $results[$ti];
				echo "<tr>";
					echo "<th class=left>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
					foreach($first_head as $fldh => $f) {
						foreach($result_head as $fld => $h) {
							$r_fld = $fldh.$fld;
							echo "<td class=right>";
							if($fldh!="cr_" || $fld!="target") {
								switch($h['h_type']) {
									case "PERC":
										echo FINresultDisplay($res_obj[$r_fld],2);
										break;
									case "NUM":
									default:
										echo FINresultDisplay($res_obj[$r_fld],1);
										break;
								}
							} else {
								echo ($t['active_finance']) ? KPIresultEdit($res_obj[$r_fld],1,"cr_target_".$ti) : KPIresultDisplay($res_obj[$r_fld],1,"cr_target_".$ti)."<input type=hidden name=cr_target_".$ti." value=".$res_obj[$r_fld]." />";
							}
							echo "</td>";
						}
					}
					$start = $t['end_stamp']+1;
				echo "</tr>";
			}
			echo "</table>";
			echo "<h2>Forecast Annual Budget</h2>
					<table id=tblfore><tr><td></td><th>CRR</th><th>Other</th></tr>";
					foreach($lists['years'] as $i => $l) {
						echo "<tr>
								<th>".$l['value'].":&nbsp;&nbsp;<input type=hidden name=yi[] value=".$i." /></th>
								<td>".KPIresultEdit((isset($forecast[$i]['cf_crr']) ? $forecast[$i]['cf_crr'] : 0),1,"years[crr][$i]")."&nbsp;</td>
								<td>".KPIresultEdit((isset($forecast[$i]['cf_other']) ? $forecast[$i]['cf_other'] : 0),1,"years[other][$i]")."&nbsp;</td>
							</tr>";
					}		
			echo "</table>";
			break;

	}

}	//end if tab
?>
<?php displayGoBack("",""); ?>
	<table width=100% id=ftbl><tr class=no-highlight><td class=center>
		<input type=submit value="Save Changes" class=isubmit /> 
		<?php if( $tab!="create") { 
			if($section!="CAP") {
				$err = false;
			} else {
				$sql = "SELECT count(kpi_id) as kc FROM ".$dbref."_kpi WHERE kpi_capitalid = ".$obj_id." AND kpi_active = true";
				$kc = mysql_fetch_one($sql);
				if($kc['kc']>0) {
					$err = true;
				} else {
					$err = false;
				}
			}
			if(!$err) {
				?><span class=float><input type=button value=Delete class=idelete id=delete_kpi /></span><?php 
			} else {
				?><span class="float iinform"><input type=button value=Delete  disabled />*</span><?php 
			}
		} ?>
	</td></tr></table>
<?php
if($section=="CAP" && isset($kc) && $kc['kc']>0) {
	echo "<P><span class=iinform>*Please note:</span> A Capital Project may only be deleted once all links to the Departmental KPIs have been removed.</p>
		<p>The following KPIs are associated with this Capital Project:<ul>";
	$sql = "SELECT kpi_id, kpi_value FROM ".$dbref."_kpi WHERE kpi_capitalid = ".$obj_id." AND kpi_active = true";
	$k = mysql_fetch_all($sql);
	foreach($k as $a) {
		echo "<li>".$a['kpi_value']." [".$id_labels_all['KPI'].$a['kpi_id']."]</li>";
	}
	echo "</ul></p>";
}

?>
</td></tr></table>
</form>
<?php 
if($section=="CF" && $tab=="edit" && isset($results)) {
			echo "<h3>All Results</h3>";
			$first_head = $mheadings['CF_H'];
			echo "<table>";
				echo "<tr>
					<td rowspan=2 width=130 style=\"border-color: #FFFFFF; background-color: #FFFFFF;\">&nbsp;</td>";
				foreach($first_head as $fldh => $f) {
					echo "<th colspan=".count($result_head).">".$f['h_client']."</th>";
				}
				echo "</tr>";
				echo "<tr>";
				foreach($first_head as $fldh => $f) {
					foreach($result_head as $fld => $h) {
						echo "<th>".$h['h_client']."</th>";
					}
				}
				echo "</tr>";
			$values = array();
			$start = strtotime("2011-07-01 00:00:00");
			foreach($time as $ti => $t) {
				$res_obj = $results[$ti];
				echo "<tr>";
					echo "<th class=left>".$t['display_full']."<input type=hidden name=ti[] value=$ti /></th>";
					foreach($first_head as $fldh => $f) {
						foreach($result_head as $fld => $h) {
							$r_fld = $r_table_fld.$fldh.$fld;
							echo "<td class=right>";
							if($fldh!="cr_" || $fld!="_1") {
								switch($h['h_type']) {
									case "PERC":
										echo FINresultDisplay($res_obj[$r_fld],2);
										break;
									case "NUM":
									default:
										echo FINresultDisplay($res_obj[$r_fld],1);
										break;
								}
							} else {
								echo FINresultDisplay($res_obj[$r_fld],1,"");
							}
							echo "</td>";
						}
					}
					$start = $t['end_stamp']+1;
				echo "</tr>";
			}
			echo "</table>";


}
?>
<p>&nbsp;</p>
<script type=text/javascript>
	$(document).ready(function() {
		$("#tblres input, #tblres textarea, #tblfore input").hover(
			function() { $(this).css("background-color","#DDFFDD"); },
			function() { $(this).css("background-color",""); }
		);
		$("#tblres input, #tblres textarea, #tblfore input").focus(
			function() { $(this).css("background-color","#DDFFDD"); }
		);
		$("#tblres input, #tblres textarea, #tblfore input").blur(
			function() { $(this).css("background-color",""); }
		);
		$("tr").unbind('mouseenter mouseleave');
	});
	$(function () {
		var head = [];
		<?php foreach($my_head as $fld => $h) { echo chr(10)." head['".$fld."'] = '".addslashes(decode($h['h_client']))."';";	} ?>
		var mytime = [];
		<?php foreach($time as $ti => $t) { echo chr(10)." mytime['".$ti."'] = '".addslashes(decode($t['display_full']))."';";	} ?>
	<?php if($section=="CAP") { ?>
		var myyears = [];
		<?php foreach($lists['years'] as $i => $l) { echo chr(10)." myyears[".$i."] = '".addslashes(decode($l['value']))."';";	} ?>
	<?php } ?>
		/* * ON DOCUMENT LOAD * */
		if($("#p_id").val()=="cashflow") { $("#tblres input").attr("size",20); }
		$("#tbl select").each(function() {
			//alert($(this).attr("id")+" => "+$(this).val());
			id = $(this).attr("id");
			//alert(id+" :: "+$(this).attr("multiple"));
			if(id!="kpi_topid" && id!="kpi_capitalid") {
				if(!$(this).attr("multiple")) {
					oldv = $("#lbl-"+id+"-v").attr("value");
					$("#lbl-"+id+"-lb").attr("innerText","\n");
					if(oldv!=$(this).val()) {
						$(this).addClass("required");
						$(this).val("0");
					} else {
						$("#lbl-"+id+"-lb").hide();
						$("#lbl-"+id+"-text").hide();
					}
				} else {
					if(!$(this).val() && id!="kpi_topid" && id!="kpi_capitalid") { $(this).addClass("required"); }
				}
			}
		});
		$("#tbl textarea, #tbl input").each(function() {
			i = $(this).attr("id");
			if(i.indexOf("-")<0) {
				//get details
				if($(this).get(0).tagName=="textarea") {
					v = $(this).text();
				} else {
					v = $(this).val();
				}
				//check maxlength limit
				m = $("#"+i+"-max").val();
				rem = m - v.length;
				if(rem>0) {
					$("#"+i+"-c").text(rem);
					$("#"+i+"-c").css({"color":"#000000","font-weight":"normal","background-color":"#FFFFFF"});
				} else {
					$("#"+i+"-c").text("0");
					if($(this).get(0).tagName=="textarea")
						$(this).text(v.substring(0,m));
					else
						$(this).val(v.substring(0,m));
					$("#"+i+"-c").css({"color":"#FFFFFF","font-weight":"bold","background-color":"#CC0001"});
				}
				//check required
				req = $("#"+i+"-r").val();
				//alert(i+" => "+v+" ::> "+req);
				if(req=="Y") {
					if(v.length==0) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				}
			}
		});
		
		
		/* * ON FORM EDIT * */
		<?php if($section=="KPI" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") { ?> 
		$("#tbl #kpi_calctype, #tbl #kpi_targettype").change(function() {
			var ct = $("#tbl #kpi_calctype").val();
			var tt = $("#tbl #kpi_targettype").val();
			if(ct=="CO" && tt=="2") {
				$("#ftbl #plc").show();
			} else {
				$("#ftbl #plc").hide();
			}
		});
		<?php } ?>
		$("#tbl textarea, #tbl input").keyup(function() {
			//get details
			if($(this).get(0).tagName=="textarea") {
				v = $(this).text();
			} else {
				v = $(this).val();
			}
			i = $(this).attr("id");
			//check maxlength limit
			if(i!="kpi_value" && i!="top_value") {
				m = $("#"+i+"-max").val();
				rem = m - v.length;
				if(rem>0) {
					$("#"+i+"-c").text(rem);
					$("#"+i+"-c").css({"color":"#000000","font-weight":"normal","background-color":"#FFFFFF"});
				} else {
					$("#"+i+"-c").text("0");
						if($(this).get(0).tagName=="textarea")
							$(this).text(v.substring(0,m));
						else
							$(this).val(v.substring(0,m));
					$("#"+i+"-c").css({"color":"#FFFFFF","font-weight":"bold","background-color":"#CC0001"});
				}
			}
			//check required
			req = $("#"+i+"-r").val();
			if(req=="Y") {
				if(v.length==0) {
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			}
		});
		$(".datepr").datepicker({
			onClose: function() {
				i = $(this).attr("id");
				v = $(this).val();
				req = $("#"+i+"-r").val();
				if(req=="Y") {
					if(v.length==0) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				}
			},
			showOn: 'both',
			buttonImage: '../library/jquery/css/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd',
			changeMonth:true,
			changeYear:true		
		});
		$("#tbl select").change(function() {
			i = $(this).attr("id");
			if(!$(this).val() || ($(this).val()=="0" && i!="kpi_topid" && i!="kpi_capitalid")) {
				$(this).addClass("required");
			} else {
				$(this).removeClass("required");
			}
		});
		$("#tblres input").keyup(function() {
			v = $(this).val();
			if(isNaN(parseFloat(v)) || !isFinite(v)) {
				$(this).addClass("required");
			} else {
				$(this).removeClass("required");
			}
			
		});
			$("#tblfore input").blur(function() {
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
		
		
		
		/* * SUBMIT FORM * */
		$("#trigger_plc").click(function() {
			if(confirm("Moving to the Project Life Cycle page will save the changes you have made to this KPI.\n\nDo you wish to continue?")==true) {
				$("#goto_plc").val("Y");
				$("form").trigger("submit");
			}
		});
		$("form").submit(function() {
			var err = "";
			$("#tbl input, #tbl select, #tbl textarea").each(function() {
				i = $(this).attr("id");
				if(i=="obj_id") {
				} else if(i.indexOf("-")<0) {
					req = $("#"+i+"-r").val();
					if(req=="Y") {
						ok = true;
						if(i!="kpi_topid" && i!="kpi_capitalid") {
							if(!($(this).val())) 																				ok = false;
							else if((i=="kpi_calctype" || i=="top_calctype") && (parseInt($(this).val())==0)) 					ok = false;
							else if( $(this).get(0).tagName=="SELECT" && i!="kpi_calctype" && i!="top_calctype" && i!="top_repkpi" && !(parseInt($(this).val())>0) )	ok = false;
							if(!ok) err+="\n - "+head[i];
						}
					}
				}
			});
			err_res = "";
			$("#tblres input").each(function() {
				i = $(this).attr("name");
				v = $(this).val();
				if(isNaN(parseFloat(v)) || !isFinite(v)) {
					if(i.substring(3,9)=="target") { n = "Target"; } else { n = "Actual"; }
					ti = i.substring(10,12);
					n+=" for "+mytime[ti];
					err_res+="\n - "+n;
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			});
			err_fore = "";
			$("#tblfore input").each(function() {
				i = $(this).attr("name");
				v = $(this).val();
				if(v.length>0) {
					valid8b = validateActual(v);
					if(!valid8b) {
						n = "Forecast Budget";
						yi = i.substring(i.length-2,i.length-1);
						n+=" for "+myyears[yi];
						err_fore+="\n - "+n;
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				} else {
					$(this).val("0");
				}
			});
			if(err.length>0 || err_res.length>0) {
				display = "";
				if(err.length>0) display = "The following required fields have not been completed:"+err;
				if(err.length>0 && err_res.length>0) display+="\n\n";
				if(err_res.length>0) display+="The following time periods have invalid values:"+err_res;
				if(err.length>0 && err_fore.length>0) display+="\n\n";
				if(err_fore.length>0) display+="The following forecast years have invalid values:"+err_fore;
				alert(display);
				$("#goto_plc").val("N");
				return false;
			} else {
				return true;
			}
		});
		$("#del_cap").click(function() {
			if(confirm("Are you sure you wish to remove the link between this KPI and the related Capital Project?")==true) {
				$("#tbl #kpi_capitalid").val(0);
				$("#tbl #kpi_capitalid_disp").css("color","#999999");
				$("#tbl #del_cap").attr("disabled",true);
				alert("Please click the 'Save Changes' button at the bottom of the page to save this change.");
			}
		});
		$(".cleardate").click(function() {
			i = $(this).attr("id");
			i = i.substring(2,i.length);
			$("#"+i).val("");
		});
	
	$("#delete_kpi").click(function() {
		if(confirm("Are you sure you wish to delete <?php echo $id_labels_all[$section].$obj_id; ?>?")==true) {
			document.location.href = '<?php echo $self; ?>?act=DELETE&tab=edit&page_id=<?php echo $page_id; ?>&id=<?php echo $obj_id; ?>';
		}
	});
});
</script>