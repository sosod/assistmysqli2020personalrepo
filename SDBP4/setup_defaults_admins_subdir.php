<?php
//$type = "SUB";
$log_section.= "_".$type;
$heading = strlen($mheadings['KPI']['kpi_subid']['h_client'])>0 ? $mheadings['KPI']['kpi_subid']['h_client'] : $mheadings['KPI']['kpi_subid']['h_ignite'];
//$heading = "Dir";
//arrPrint($mheadings);

$sql = "SELECT id, value FROM ".$dbref."_dir WHERE active = true ORDER BY sort";
$dirs = mysql_fetch_alls($sql,"id");
$sql = "SELECT s.* FROM ".$dbref."_subdir s 
		INNER JOIN ".$dbref."_dir d ON d.id = s.dirid AND d.active = true 
		WHERE s.active = true 
		ORDER BY d.sort, s.sort";
$objects = mysql_fetch_alls($sql,"id");
$sql = "SELECT tkid as id, CONCAT_WS(' ',tkname, tksurname) as value FROM assist_".$cmpcode."_timekeep
		INNER JOIN assist_".$cmpcode."_menu_modules_users ON usrtkid = tkid AND usrmodref = '$modref' 
		WHERE tkstatus = 1 
		ORDER BY tkname, tksurname";
$users = mysql_fetch_alls($sql,"id");
$sql = "SELECT a.*, CONCAT_WS(' ',t.tkname,t.tksurname) as value FROM ".$dbref."_user_admins a
		INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = a.tkid AND t.tkstatus = 1
		WHERE type = '$type'
		ORDER BY a.ref, t.tkname, t.tksurname";
$admins = mysql_fetch_alls2($sql,"ref","tkid");


if(isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case "ADD":
			$u = $_REQUEST['user'];
			$i = $_REQUEST['i'];
			if(!checkIntRef($i) || strlen($u)==0) {
				$result = array("error","An error occurred while trying to add the administrator.  Please try again.");
			} else {
				if(isset($admins[$i][$u])) {
					$result = array("info","Administrator '".$admins[$i][$u]['value']."' already exists for ".$heading." '".$objects[$i]['value']."'.");
				} else {
					$a = "update"; 		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "true";
					$a = "edit"; 		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "false";
					$a = "create"; 		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "false";
					$a = "approve";		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "false";
					$access = array(); foreach($act as $a => $c) { if($c=="true") { $access[] = ucfirst($a); } }
					if(count($access)==0) {
						$result = array("error","Administrator could not be edited -> At least one administrator function must be set to 'Yes'.");
					} else {
						$sql = "INSERT INTO ".$dbref."_user_admins (id, tkid, active, ref, type, act_update, act_edit, act_create, act_approve) VALUES 
								(null,'$u',true,$i,'$type',".$act['update'].",".$act['edit'].",".$act['create'].",".$act['approve'].")";
						$id = db_insert($sql);
						if(checkIntRef($id)) {
							$result = array("ok","New administrator '".$users[$u]['value']."' has been added to $heading '".$objects[$i]['value']."'.");
							$admins[$i][$u] = array('id'=>$id,'tkid'=>$u,'active'=>true,'ref'=>$id,'type'=>$type,'value'=>$users[$u]['value']);
							$a = "update"; 		$admins[$i][$u]['act_'.$a] = ($act[$a]=="true" ? true : false);
							$a = "edit"; 		$admins[$i][$u]['act_'.$a] = ($act[$a]=="true" ? true : false);
							$a = "create"; 		$admins[$i][$u]['act_'.$a] = ($act[$a]=="true" ? true : false);
							$a = "approve";		$admins[$i][$u]['act_'.$a] = ($act[$a]=="true" ? true : false);
											$v_log = array(
												'section'=>$log_section,
												'ref'=>$id,
												'field'=>"",
												'text'=>"Added new administrator \'".$users[$u]['value']."\' to $heading \'".$objects[$i]['value']."\' with access to ".implode(", ",$access).".",
												'old'=>"",
												'new'=>"",
												'YN'=>"Y"
											);
											logChanges('SETUP',0,$v_log,code($sql));
						} else {
							$result = array("error","An error occurred while trying to add the administrator.  Please try again.");
						}
					}
				}
			}
			break;
		case "DEACTIVATE":
			$i = $_REQUEST['i'];
			if(!checkIntRef($i)) {
				$result = array("error","An error occurred while trying to deactivate the administrator.  Please try again.");
			} else {
				$sql = "SELECT a.*, CONCAT_WS(' ',t.tkname,t.tksurname) as value FROM ".$dbref."_user_admins a
						INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = a.tkid AND t.tkstatus = 1
						WHERE active = true AND type = '$type' AND a.id = $i
						ORDER BY a.ref, t.tkname, t.tksurname";
				$adm = mysql_fetch_all($sql);
				if(count($adm)==0) {
					$result = array("info","Administrator could not be found.");
				} else {
					$sql = "UPDATE ".$dbref."_user_admins SET active = false WHERE id = ".$i;
					$mar = db_update($sql);
					if($mar > 0) {
						$u = $adm[0]['tkid'];
						$r = $adm[0]['ref'];
						$result = array("ok","Administrator '".$adm[0]['value']."' has been deactivated from $heading '".$objects[$r]['value']."'.");
						$admins[$r][$u]['active'] = false;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$r,
											'field'=>"active",
											'text'=>"Deactivated administrator \'".$users[$u]['value']."\' from $heading \'".$objects[$r]['value']."\'.",
											'old'=>"true",
											'new'=>"false",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					} else {
						$result = array("error","An error occurred while trying to deactivate the administrator.  Please try again.");
					}
				}
			}
			break;
		case "RESTORE":
			$i = $_REQUEST['i'];
			if(!checkIntRef($i)) {
				$result = array("error","An error occurred while trying to restore the administrator.  Please try again.");
			} else {
				$sql = "SELECT a.*, CONCAT_WS(' ',t.tkname,t.tksurname) as value FROM ".$dbref."_user_admins a
						INNER JOIN assist_".$cmpcode."_timekeep t ON t.tkid = a.tkid AND t.tkstatus = 1
						WHERE active = false AND type = '$type' AND a.id = $i
						ORDER BY a.ref, t.tkname, t.tksurname";
				$adm = mysql_fetch_all($sql);
				if(count($adm)==0) {
					$result = array("info","Administrator could not be found.");
				} else {
					$sql = "UPDATE ".$dbref."_user_admins SET active = true WHERE id = ".$i;
					$mar = db_update($sql);
					if($mar > 0) {
						$u = $adm[0]['tkid'];
						$r = $adm[0]['ref'];
						$result = array("ok","Administrator '".$adm[0]['value']."' has been restored to $heading '".$objects[$r]['value']."'.");
						$admins[$r][$u]['active'] = true;
										$v_log = array(
											'section'=>$log_section,
											'ref'=>$r,
											'field'=>"active",
											'text'=>"Restored administrator \'".$users[$u]['value']."\' to $heading \'".$objects[$r]['value']."\'.",
											'old'=>"false",
											'new'=>"true",
											'YN'=>"Y"
										);
										logChanges('SETUP',0,$v_log,code($sql));
					} else {
						$result = array("error","An error occurred while trying to restore the administrator.  Please try again.");
					}
				}
			}
			break;
		case "EDIT":
			$u = $_REQUEST['adminid'];
			$ui = $_REQUEST['user'];
			$i = $_REQUEST['i'];
			if(!checkIntRef($i) || !checkIntRef($u)) {
				$result = array("error","An error occurred while trying to save the changes.  Please try again.");
			} else {
				if(!isset($admins[$i][$ui])) {
					$result = array("error","The Administrator could not be found.  Please try again.");
				} else {
					$a = "update"; 		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "true";
					$a = "edit"; 		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "false";
					$a = "create"; 		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "false";
					$a = "approve";		$act[$a] = isset($_REQUEST['act_'.$a]) ? $_REQUEST['act_'.$a] : "false";
					$access = array(); foreach($act as $a => $c) { if($c=="true") { $access[] = ucfirst($a); } }
					if(count($access)==0) {
						$result = array("error","Administrator could not be edited -> At least one administrator function must be set to 'Yes'.");
					} else {
						$sql = "UPDATE ".$dbref."_user_admins SET act_update = ".$act['update'].", act_edit = ".$act['edit'].", act_create = ".$act['create'].", act_approve = ".$act['approve']." WHERE id = ".$u;
						$mar = db_update($sql);
						if($mar>0) {
							$result = array("ok","Changes for Administrator '".$users[$ui]['value']."' for $heading '".$objects[$i]['value']."' have been saved.");
							//$admins[$i][$u] = array('id'=>$id,'tkid'=>$u,'active'=>true,'ref'=>$id,'type'=>$type,'value'=>$users[$u]['value']);
							$a = "update"; 		$admins[$i][$ui]['act_'.$a] = ($act[$a]=="true" ? true : false);
							$a = "edit"; 		$admins[$i][$ui]['act_'.$a] = ($act[$a]=="true" ? true : false);
							$a = "create"; 		$admins[$i][$ui]['act_'.$a] = ($act[$a]=="true" ? true : false);
							$a = "approve";		$admins[$i][$ui]['act_'.$a] = ($act[$a]=="true" ? true : false);
											$v_log = array(
												'section'=>$log_section,
												'ref'=>$id,
												'field'=>"",
												'text'=>"Edited administrator \'".$users[$ui]['value']."\' for $heading \'".$objects[$i]['value']."\' -> changed access to ".implode(", ",$access).".",
												'old'=>"",
												'new'=>"",
												'YN'=>"Y"
											);
											logChanges('SETUP',0,$v_log,code($sql));
						} else {
							$result = array("info","No changes could be found.  Please try again.");
						}
					}
				}
			}
			break;
	}
}


displayResult($result);
if(count($objects)>0) {
?>
<table >
	<tr>
		<th>Ref</th><th><?php echo $heading; ?></th><th>Administrators</th>
	</tr>
	<?php 
	foreach($objects as $d) {
		echo "<tr>";
			echo "<th>".$d['id']."</th>";
			echo "<td valign=top><label id=own-".$d['id']."-value>".decode($d['value'])."</label><br />
					<i>&nbsp;&nbsp;(".$dirs[$d['dirid']]['value'].")</i></td>";
			echo "<td style=\"padding: 0 0 0 0;vertical-align: top\"><table width=100% class=noborder style=\"background-color: #FFFFFF;\">";
			if(isset($admins[$d['id']])) {
				echo "<tr id=hover2>
						<th class=th2>Ref</th>
						<th class=th2>Admin</th>
						<th class=th2>Update</th>
						<th class=th2>Edit</th>
						<th class=th2>Create</th>
						<th class=th2>Approve</th>
						<th class=th2>&nbsp;</th>
					</tr>";
				foreach($admins[$d['id']] as $s) {
					echo "<tr id=hover2>";
					/*echo "<th class=\"th2 noborder\" ".(!$s['active'] ? "style=\"background-color: #777777;\"" : "")." width=30>".$s['id']."</th>";
					echo "<td class=\"noborder ".(!$s['active'] ? "inact" : "")."\"><label id=admin-".$s['id']."-value>".decode($s['value'])."</label></td>";
					echo "<td class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_update'] ? "Yes" : "No")."</td>";
					echo "<td class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_edit'] ? "Yes" : "No")."</td>";
					echo "<td class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_create'] ? "Yes" : "No")."</td>";
					echo "<td class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_approve'] ? "Yes" : "No")."</td>";
					echo "<td class=\"right noborder ".(!$s['active'] ? "inact" : "")."\">";
					if($s['active'] && $d['active']) {
						echo "<input type=button value=Deactivate class=deact id=".$s['id']." name=".$d['id']." />";*/
					echo "<th class=\"th2 noborder\" ".(!$s['active'] ? "style=\"background-color: #777777;\"" : "")." width=30>".$s['id']."<input type=hidden id=admin-".$s['id']."-tkid value=\"".$s['tkid']."\" /></th>";
					echo "<td class=\"noborder ".(!$s['active'] ? "inact" : "")."\"><label id=admin-".$s['id']."-value>".decode($s['value'])."</label></td>";
					echo "<td id=\"td-".$s['id']."-up\" class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_update'] ? "Yes" : "No")."</td>";
					echo "<td id=\"td-".$s['id']."-ed\" class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_edit'] ? "Yes" : "No")."</td>";
					echo "<td id=\"td-".$s['id']."-cr\" class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_create'] ? "Yes" : "No")."</td>";
					echo "<td id=\"td-".$s['id']."-ap\" class=\"noborder centre ".(!$s['active'] ? "inact" : "")."\">".($s['act_approve'] ? "Yes" : "No")."</td>";
					echo "<td class=\"right noborder ".(!$s['active'] ? "inact" : "")."\">";
					if($s['active'] && $d['active']) {
						echo "<input type=button value=Edit class=edit id=".$s['id']." name=".$d['id']." />";
					} elseif(!$s['active']) {
						echo "<input type=button value=Restore class=restore id=".$s['id']." name=".$d['id']." />";
					}
				}
			}
			echo "<tr id=hover2><td class=\"noborder\" width=30></td><td colspan=6 class=\"noborder right\"><input type=button value=Add class=add id=".$d['id']." /></td></tr></table></td>";
		echo "</tr>";
	}
	?>
</table>
<div id=add title="Add">
	<form id=frm_add method=post action=<?php $self; ?>>
	<input type=hidden name=action value=ADD /><input type=hidden name=page_id value=<?php echo $page_id; ?> />
	<table width=100%>
		<tr>
			<th class=left><?php echo $heading; ?>:</th>
			<td><input type=hidden name=i value="" id=id /><label id=val></label></td>
		</tr>
		<tr>
			<th class=left>Administrator:</th>
			<td><select name=user id=u><option selected value=X>--- SELECT ---</option>
			<?php 
			foreach($users as $u) {
				echo "<option value=".$u['id'].">".$u['value']."</option>";
			}
			?>
			</select></td>
		</tr>
		<tr>
			<th class=left>Update:</th>
			<td><select name=act_update id=up><option selected value=true>Yes</option><option value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>Edit:</th>
			<td><select name=act_edit id=ed><option value=true>Yes</option><option selected value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>Create:</th>
			<td><select name=act_create id=cr><option value=true>Yes</option><option selected value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>Approve:</th>
			<td><select name=act_approve id=ap><option value=true>Yes</option><option selected value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>&nbsp;</th>
			<td><input type=button value="Save Changes" class=isubmit id=add-submit /></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=add-cancel /></p>
	</form>
</div>
<div id=edit title="Edit">
	<form id=frm_edit method=post action=<?php $self; ?>>
	<input type=hidden name=action value=EDIT /><input type=hidden name=page_id value=<?php echo $page_id; ?> />
	<table width=100%>
		<tr>
			<th class=left><?php echo $heading; ?>:</th>
			<td><input type=hidden name=i value="" id=id /><label id=val></label></td>
		</tr>
		<tr>
			<th class=left>Administrator:</th>
			<td><input type=hidden name=user id=ui /><input type=hidden name=adminid id=u /><label for=u id=ut></label></td>
		</tr>
		<tr>
			<th class=left>Update:</th>
			<td><select name=act_update id=up><option value=true>Yes</option><option value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>Edit:</th>
			<td><select name=act_edit id=ed><option value=true>Yes</option><option selected value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>Create:</th>
			<td><select name=act_create id=cr><option value=true>Yes</option><option selected value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>Approve:</th>
			<td><select name=act_approve id=ap><option value=true>Yes</option><option selected value=false>No</option></select></td>
		</tr>
		<tr>
			<th class=left>&nbsp;</th>
			<td><input type=button value="Save Changes" class=isubmit id=edit-submit /><span class=float><input type=button value="Deactivate" id=deact name=0 class=idelete /></td>
		</tr>
	</table>
	<p><input type=button value=Cancel id=edit-cancel /></p>
	</form>
</div><script type=text/javascript>
$(function() {
	$("#add").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$(".add").click(function() {
		var id = $(this).attr("id");
		var fld = "#own-"+id+"-value";
		$("#frm_add #id").attr("value",id);
		$("#frm_add #val").attr("innerText",$(fld).attr("innerText"));
		$("#add").dialog("open");
	});
	$("#frm_add #add-submit").click(function() {
		if($("#frm_add #u").attr("value").length==0 || $("#frm_add #u").attr("value")=="X") {
			alert("Please select an administrator from the drop down list\n or else click the \"Cancel\" button to close.");
		} else {
			$("#frm_add").submit();
		}
	});
	$("#frm_add #add-cancel").click(function() {
		$("#frm_add #id").attr("value","");
		$("#frm_add #val").attr("innerText","");
		$("#frm_add #u").attr("value","X");
		$("#add").dialog("close");
	});
/* * EDIT * */
	$("#edit").dialog({
		autoOpen: false, 
		width: 500, 
		height: "auto",
		modal: true
	});
	$(".edit").click(function() {
		var id = $(this).attr("id");
		$("#frm_edit #deact").attr("name",id);
		var o = $(this).attr("name");
		var fld = "#own-"+o+"-value";
		var sel = "up";
		if($("#td-"+id+"-"+sel).attr("innerText")=="Yes") { $("#frm_edit #"+sel).val("true"); } else { $("#frm_edit #"+sel).val("false"); }
		var sel = "ed";
		if($("#td-"+id+"-"+sel).attr("innerText")=="Yes") { $("#frm_edit #"+sel).val("true"); } else { $("#frm_edit #"+sel).val("false"); }
		var sel = "cr";
		if($("#td-"+id+"-"+sel).attr("innerText")=="Yes") { $("#frm_edit #"+sel).val("true"); } else { $("#frm_edit #"+sel).val("false"); }
		var sel = "ap";
		if($("#td-"+id+"-"+sel).attr("innerText")=="Yes") { $("#frm_edit #"+sel).val("true"); } else { $("#frm_edit #"+sel).val("false"); }
		$("#frm_edit #id").attr("value",o);
		$("#frm_edit #val").attr("innerText",$(fld).attr("innerText"));
		$("#frm_edit #ut").attr("innerText",$("#admin-"+id+"-value").attr("innerText"));
		$("#frm_edit #u").val(id);
		$("#frm_edit #ui").val($("#admin-"+id+"-tkid").val());
		$("#edit").dialog("open");
	});
	$("#frm_edit #edit-submit").click(function() {
		$("#frm_edit").submit();
	});
	$("#frm_edit #edit-cancel").click(function() {
		$("#frm_edit #id").attr("value","");
		$("#frm_edit #val").attr("innerText","");
		$("#frm_edit #u").attr("value","");
		$("#frm_edit #ut").attr("innerText","");
		$("#edit").dialog("close");
	});
	$("#frm_edit #deact").click(function() {
		var id = $(this).attr("name");
		var val = $("#frm_edit #ut").attr("innerText");
		var own = $("#frm_edit #val").attr("innerText");
		if(confirm("Are you sure that you wish to DEACTIVATE administrator '"+val+"' from <?php echo $heading; ?> '"+own+"'?")) {
			document.location.href = '<?php echo $self; ?>?page_id=<?php echo $page_id; ?>&action=DEACTIVATE&i='+id;
		}
	});
	$(".restore").click(function() {
		var id = $(this).attr("id");
		var val = $("#admin-"+id+"-value").attr("innerText");
		var o = $(this).attr("name");
		var own = $("#own-"+o+"-value").attr("innerText");
		if(confirm("Are you sure that you wish to RESTORE administrator '"+val+"' to <?php echo $heading; ?> '"+own+"'?")) {
			document.location.href = '<?php echo $self; ?>?page_id=<?php echo $page_id; ?>&action=RESTORE&i='+id;
		}
	});
});
</script>
<?php


} else {
	echo "<p>There are no active ".$heading." to display.</p>";
}


goBack("setup.php","Back to Setup");
$log_sql = "SELECT slog_date, slog_tkname, slog_transaction FROM ".$dbref."_setup_log WHERE slog_section = '".$log_section."' AND slog_yn = 'Y' ORDER BY slog_id DESC";
displayAuditLog($log_sql,array('date'=>"slog_date",'user'=>"slog_tkname",'action'=>"slog_transaction"));

?>