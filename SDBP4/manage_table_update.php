<?php

//arrPrint($setup_defaults);

$my_head = $mheadings[$section];
$result_head = $mheadings[$section."_R"];
$update_head = $result_head;
if(isset($update_head['kr_target'])) { unset($update_head['kr_target']); }
if(isset($update_head['tr_target'])) { unset($update_head['tr_target']); }
if(isset($update_head['tr_dept'])) { unset($update_head['tr_dept']); }
if(isset($update_head['tr_dept_correct'])) { unset($update_head['tr_dept_correct']); }

$performance_fields = array("tr_perf","kr_perf");

$wide_keys = array_flip($wide_headings);

//arrPrint($wide_keys);

unset($wide_headings[$wide_keys['kpi_topid']]); unset($wide_headings[$wide_keys['kpi_capitalid']]);

//arrPrint($wide_headings);

/*************************
****** SAVING UPDATE *****
*************************/
//arrPrint($_REQUEST);
if(isset($_REQUEST['action']) && ($_REQUEST['action']=="SAVE" || ($section=="TOP" && $_REQUEST['action']=="UNLOCK"))) {
	$sql_timeid = $filter['when'];
	$result = array("ok","Update saved");
	$save_id = $_REQUEST['obj_id'];
	$save_values = array();
	$sql = array();
	foreach($update_head as $fld => $h) {
		$save_values[$fld] = $_REQUEST[$fld];
	}
	//arrPrint($save_id);
	switch($section) {
		case "KPI":
			foreach($save_id as $s => $i) {
				if(checkIntRef($i)) {
					$sql[$s] = array(
						'sql'=>"",
						'id'=>$i,
						'values'=>array()
					);
					$q = "UPDATE ".$dbref."_kpi_results SET ";
					$q_values = array();
					foreach($update_head as $fld => $h) {
						if($h['h_type']=="NUM" && strlen($save_values[$fld][$s])==0) { $save_values[$fld][$s] = 0; }
						$q_values[$fld] = $fld." = ".($h['h_type']!="NUM" ? "'" : "").($h['h_type']!="NUM" ? code($save_values[$fld][$s]) : $save_values[$fld][$s]).($h['h_type']!="NUM" ? "'" : "");
						if(isset($save_values[$fld][$s]) && ($results[$i][$sql_timeid][$fld])!= code($save_values[$fld][$s])) {
							$sql[$s]['values'][$fld] = array(
								'fld'=>$fld,
								'new'=>($h['h_type']!="NUM" ? code($save_values[$fld][$s]) : $save_values[$fld][$s]),
								'old'=>($h['h_type']!="NUM" ? ($results[$i][$sql_timeid][$fld]) : $results[$i][$sql_timeid][$fld]),
								'act'=>"U",
								'YN'=>"Y",
								'timeid'=>$sql_timeid,
								'text'=>code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." to '".$save_values[$fld][$s]."'".(strlen($results[$i][$sql_timeid][$fld])>0 ? " from '".$results[$i][$sql_timeid][$fld]."'" : "")." for time period ".$time[$sql_timeid]['display_short'])
							);
						}
					}
					//arrPrint($sql_values);
					$q.= implode(", ",$q_values)." WHERE kr_kpiid = $i AND kr_timeid = ".$sql_timeid;
					$sql[$s]['sql'] = $q;
				}
			}
			//arrPrint($sql);
			foreach($sql as $q) {
				$mar = db_update($q['sql']);
				if($mar>0) {
					foreach($q['values'] as $v) {
						if($v['new']!=$v['old']) {
							logChanges($section,$q['id'],$v,code($q['sql']));
							$results[$q['id']][$sql_timeid][$v['fld']] = $v['new'];
						}
					}
				}
			}
			//$results = getResults();
			break;
		case "TOP":
			if($_REQUEST['action']=="SAVE") {
				foreach($save_id as $s => $i) {
					if(checkIntRef($i)) {
						$sql[$s] = array(
							'sql'=>"",
							'id'=>$i,
							'values'=>array()
						);
						$q = "UPDATE ".$dbref."_top_results SET ";
						$q_values = array();
						foreach($update_head as $fld => $h) {
							if($h['h_type']=="NUM" && strlen($save_values[$fld][$s])==0) { $save_values[$fld][$s] = 0; }
							$q_values[$fld] = $fld." = ".($h['h_type']!="NUM" ? "'" : "").($h['h_type']!="NUM" ? code($save_values[$fld][$s]) : $save_values[$fld][$s]).($h['h_type']!="NUM" ? "'" : "");
							if(strpos($fld,"actual")>0 && ($save_values[$fld][$s]!=$results[$i][$sql_timeid][$fld])) {
								$q_values[$fld].= ", tr_auto = 0 ";
								$results[$i][$sql_timeid]['tr_auto'] = 0;
							}
							$sql[$s]['values'][$fld] = array(
								'fld'=>$fld,
								'new'=>($h['h_type']!="NUM" ? code($save_values[$fld][$s]) : $save_values[$fld][$s]),
								'old'=>($h['h_type']!="NUM" ? code($results[$i][$sql_timeid][$fld]) : $results[$i][$sql_timeid][$fld]),
								'act'=>"U",
								'YN'=>"Y",
								'timeid'=>$sql_timeid,
								'text'=>code("Updated ".(strlen($h['h_client'])>0?$h['h_client']:$h['h_ignite'])." for ".$time[$sql_timeid]['display_full']." to '".$save_values[$fld][$s]."'".(strlen($results[$i][$sql_timeid][$fld])>0 ? " from '".$results[$i][$sql_timeid][$fld]."'" : "").".")
							);
							if(strpos($fld,"actual")>0 && ($save_values[$fld][$s]!=$results[$i][$sql_timeid][$fld])) {
								$sql[$s]['values'][$fld]['text'].= " Locked time period for manual updating only.";
							}
						}
						//arrPrint($sql_values);
						$q.= implode(", ",$q_values)." WHERE tr_topid = $i AND tr_timeid = ".$sql_timeid;
						$sql[$s]['sql'] = $q;
					}
				}
			} else {
				foreach($save_id as $s => $i) {
					if(checkIntRef($i)) {
						$sql[$s] = array(
							'sql'=>"",
							'id'=>$i,
							'values'=>array()
						);
						$q = "UPDATE ".$dbref."_top_results SET tr_auto = 1";
						$results[$i][$sql_timeid]['tr_auto'] = 0;
						$fld = "tr_auto";
							$sql[$s]['values'][$fld] = array(
								'fld'=>$fld,
								'new'=>1,
								'old'=>0,
								'act'=>"U",
								'YN'=>"Y",
								'timeid'=>$sql_timeid,
								'text'=>code("Unlocked ".$time[$sql_timeid]['display_full']." for automatic updating.")
							);
						//arrPrint($sql_values);
						$q.= " WHERE tr_topid = $i AND tr_timeid = ".$sql_timeid;
						$sql[$s]['sql'] = $q;
					}
				}
			}
			foreach($sql as $q) {
				if(db_update($q['sql'])) {
					foreach($q['values'] as $v) {
						if($v['new']!=$v['old']) {
							logChanges($section,$q['id'],$v,code($q['sql']));
							$results[$q['id']][$sql_timeid][$v['fld']] = $v['new'];
						}
					}
				}
			}
			break;
	}
	echo "<div id=dispRes>";
		displayResult($result);
	echo "</div>";
}


$flds = array();
echo "<form id=save_object method=post action=$self style=\"margin-bottom: -15px;\"><input type=hidden name=obj_id[] id=obj_id value=\"\" />";
	drawManageFormFilter(true,true,true,true,$filter,$page_id);
	foreach($update_head as $fld => $h) { $flds[] = $fld;  echo "<input type=hidden name=".$fld."[] id=$fld value=\"\" />"; }
echo "</form>";
?>
<link rel="stylesheet" href="lib/locked-column-form.css" type="text/css">
<?php
if(isset($my_head[$table_fld.'targettype'])) {
	unset($my_head[$table_fld.'targettype']);
}
if($section=="KPI" || $section == "TOP") {
	$colspan = count($result_head)+($section=="TOP" ? 1 : 0);
} elseif($section=="CF" || $section=="CAP") {
	if($filter['display']!="LIMIT") {
		$colspan = count($mheadings[$rheadings]);
	} else {
		$colspan = 0;
		foreach($mheadings[$rheadings] as $h) {
			if($h['c_list']) 
				$colspan++;
		}
	}
} else {
	$colspan = 2;
}

echo "<form method=post action=$self style=\"margin-top: 0px\" id=save_all class=tbl_contain>";
echo "<div id=tbl-container>";
drawManageFormFilter(true,true,true,true,$filter,$page_id);
?>


<table id=tbl>



<thead>
<?php
$total_columns = 0;
	$total = array();
	$total['all']['target'] = 0;
	$total['all']['actual'] = 0;
	echo "<tr>
		<th rowspan=2 class=thtd>Ref</th>";
		foreach($mheadings['FIXED'] as $key => $row) {
			$total_columns++;
			echo "<th rowspan=2 class=fixed>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
		}
		foreach($my_head as $key => $row) {
			if(($filter['display']!= "LIMIT" || $row['c_list']) && !$row['fixed']) {
				$total_columns++;
				echo "<th rowspan=2>".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite']).(in_array($row['field'],$wide_headings) ? "<br /><img src=\"/pics/blank.gif\" height=1 width=200>" : "")."</th>";
			}
		}
		$tc = 0;
		$t_css = 2;
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $ti => $row) {
				$tc++;
				echo "<th colspan=$colspan class=\"time".$t_css."\">".(strlen($row['h_client'])>0 ? $row['h_client'] : $row['h_ignite'])."</th>";
			}
		} else {
			$ti = $filter['when'];
			$t = $time[$ti];
					$tc++;
			$ti_past = $ti - ($section=="TOP" ? 3 : 1);
			$ti_next = $ti + ($section=="TOP" ? 3 : 1);
			if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
					$col_past = 0;
					if($setup_defaults[$section."_past_r"]=="Y") { $col_past+= 3; }
					if($setup_defaults[$section."_past_c"]=="Y") { $col_past+= $section=="TOP" ? 4 : 2; }
					echo "<th colspan=".$col_past." class=\" time".($t_css+1)."\">".$time2[$ti_past]['display_full']."</th>";
			}
					echo "<th colspan=$colspan class=\" time".$t_css."\">".$t['display_full']."</th>";
					$total[$ti]['target'] = 0;
					$total[$ti]['actual'] = 0;
			if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
					$col_next = 0;
					if($setup_defaults[$section."_next_r"]=="Y") { $col_next+= 1; }
					echo "<th colspan=".$col_next." class=\" time".($t_css-1)."\">".$time2[$ti_next]['display_full']."</th>";
			}
					$t_css = ($t_css>=4) ? 1 : $t_css+1;
		}
		echo "<th rowspan=2>Action</th>";
	echo "	</tr>";
	echo "	<tr>";
		if($section == "CF") {
			$t_css = 3;
			foreach($mheadings['CF_H'] as $h => $cf_h) {
				foreach($mheadings['CF_R'] as $r => $cf_r) {
					if($filter['display']!="LIMIT" || $cf_r['c_list']) {
						echo "<th class=\"time".$t_css."\">".(strlen($cf_r['h_client'])>0 ? $cf_r['h_client'] : $cf_r['h_ignite'])."</th>";
						$total[$h][$r] = 0;
					}
				}
				$t_css = ($t_css<=1) ? 3 : $t_css-1;
			}
		} else {
			$t_css = 2;
				switch($section) {
					case "TOP":
					case "KPI":
						if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
								foreach($result_head as $fld => $h) {
									if( ($setup_defaults[$section."_past_r"]=="Y" && ($fld==$fld_target || $fld==$fld_actual)) || ($setup_defaults[$section."_past_c"]=="Y" && $fld!=$fld_target && $fld!=$fld_actual ) ) {
										echo "<th class=\"time".($t_css+1)."\" >".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite'])."</th>";
										if($setup_defaults[$section."_past_r"]=="Y" && $fld==$fld_actual) {
											echo "<th class=\"time".($t_css+1)."\" >R</th>";
										}
									}
								}
						}
						foreach($result_head as $fld => $h) {
							echo "<th class=\"time".$t_css."\" >".(strlen($h['h_client'])>0 ? $h['h_client'] : $h['h_ignite']).((in_array($fld,$performance_fields) && $setup_defaults['perfcomm']=="Y") ? "<br /><span class=i style=\"font-size:6.5pt;\">(Required)</span>" : "")."</th>";
						}
						if($section=="TOP") {
							echo "<th class=\"time".$t_css."\" >Locked</th>";
						}
						if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
								echo "<th class=\" time".($t_css-1)."\">".$result_head[$fld_target]['h_client']."</th>";
						}
						break;
					case "CAP":
						foreach($mheadings[$rheadings] as $r => $cr) {
							if($filter['display']!="LIMIT" || $cr['c_list']) {
								echo "<th class=\"time".$t_css."\">".(strlen($cr['h_client'])>0 ? $cr['h_client'] : $cr['h_ignite'])."</th>";
							}
						}
						break;
					default:
						echo "<th  class=\"time".$t_css."\" >Budget</th>
							<th  class=\"time".$t_css."\" >Actual</th>";
				}
		}
	echo "	</tr>";

?>
</thead>






<tbody>
<?php

$rc = 0;
	$object_rs = getRS($object_sql);
	while($object = mysql_fetch_assoc($object_rs)) {
		$obj_id = $object[$table_fld.'id'];
		echo "<tr>
			<td class=thtd>".(isset($id_label)?$id_label:"").$obj_id."</td>";
				foreach($mheadings['FIXED'] as $key => $h) {
					echo "<td class=fixed>";
					if($h['h_type']=="LIST") {
						if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
							echo "<div class=centre>".$lists[$h['h_table']][$object[$key]]['code']."</div>";
						} else {
							echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
						}
					} else {
						echo displayValue($object[$key]);
					}
					echo "</td>";
				}
				foreach($my_head as $key => $h) {
					if(($filter['display']!="LIMIT" || $h['c_list']) && !$h['fixed']) {
						echo "<td>";
						if($h['h_type']=="LIST") {
							if(in_array($h['h_table'],$code_lists) && strlen($lists[$h['h_table']][$object[$key]]['code'])) {
								echo "<div class=centre title=\"".$lists[$h['h_table']][$object[$key]]['value']."\" style=\"text-decoration: underline\">".$lists[$h['h_table']][$object[$key]]['code']."</div>";
							} else {
								echo displayValue($lists[$h['h_table']][$object[$key]]['value']);
							}
							if(in_array($key,array("kpi_calctype","top_calctype"))) {
								echo "<input type=hidden value=".$object[$key]." name=abc[] id=ct_".$obj_id." />";
							}
						} else {
							switch($key) {
								case $table_fld."area":
								case $table_fld."wards":
								case "capital_area":
								case "capital_wards":
								case "capital_fundsource":
									if(isset($wa) && isset($wa[$key]) && isset($wa[$key][$obj_id])) {
										//echo $key." :: "; arrPrint($wa[$key][$obj_id]);
										foreach($wa[$key][$obj_id] as $v) {
											if(isset($v['code']) && strlen($v['code'])) {
												echo $v['code'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											} else {
												echo $v['value'].(count($wa[$key][$obj_id])>1 ? "; " : "");
											}
										}
									}
									break;
								case $table_fld."topid":
									if(isset($tops) && isset($tops[$object[$key]])) {
										//echo displayValue($tops[$object[$key]]['top_value']." [".$id_labels_all['TOP'].$object[$key]."]");
										echo $id_labels_all['TOP'].$object[$key];
									}
									break;
								case $table_fld."capitalid":
									if(isset($caps) && isset($caps[$object[$key]])) {
										//echo displayValue($caps[$object[$key]]['cap_name']." [".$id_labels_all['CAP'].$object[$key]."]");
										echo $id_labels_all['CAP'].$object[$key];
									}
									break;
								case "top_annual":
								case "top_revised":
									echo "<div class=right>".KPIresultDisplay($object[$key],$object[$table_fld.'targettype'])."</div>";
									break;
								case "cap_planstart":
								case "cap_planend":
								case "cap_actualstart":
								case "cap_actualend":
									if(strlen($object[$key])>1) {
										echo date("d M Y",$object[$key]);
									}
									break;
								default:
									echo displayValue($object[$key]);
									break;
							}
						}
						echo "</td>";
					}
				}
				$result_object = $results[$obj_id];
				$t_css = 2;
				switch($section) {
					case "KPI":
					case "TOP":
						$obj_tt = $object[$table_fld.'targettype'];
						$obj_ct = $object[$table_fld.'calctype'];
						$values = array();
						if(isset($time2[$ti_past]) && ($setup_defaults[$section."_past_r"]=="Y" || $setup_defaults[$section."_past_c"]=="Y")) {
							$values_past = $result_object[$ti_past];
							$v_past = array(
								'target'=>array($ti_past=>$values_past[$fld_target]),
								'actual'=>array($ti_past=>$values_past[$fld_actual]),
							);
							if($setup_defaults[$section."_past_r"]=="Y") {
								$r_past = KPIcalcResult($v_past,$obj_ct,array(),$ti_past);
							}
								foreach($result_head as $fld => $h) {
									if( ($setup_defaults[$section."_past_r"]=="Y" && ($fld==$fld_target || $fld==$fld_actual)) || ($setup_defaults[$section."_past_c"]=="Y" && $fld!=$fld_target && $fld!=$fld_actual ) ) {
										echo "<td class=\"right time".($t_css+1)."\" >";
										if($fld=="tr_dept" || $fld == "tr_dept_correct") {
											echo "<div class=left>".displayTRDept($values_past[$fld],"HTML")."</div>";
										} else {
											switch($h['h_type']) {
												case "TEXT": 
													echo "<div class=left>".decode($values_past[$fld])."</div>"; break;
												case "NUM":
												default:	echo KPIresultDisplay($values_past[$fld],$obj_tt); break;
											}
										}
										if($setup_defaults[$section."_past_r"]=="Y" && $fld == $fld_actual) {
											echo "</td><td class=\"".$r_past['style']."\">".$r_past['text'];
										}
										echo "</td>";
									}
								}
						}
						foreach($result_head as $fld => $h) {
							$values[$fld] = $result_object[$ti][$fld];
							echo "<td class=\"right time".$t_css."\">";
							if($fld=="kr_target" || $fld == "tr_target") {
								echo KPIresultDisplay($values[$fld],$obj_tt)."<input type=hidden value=\"".$values[$fld]."\" name=abc[] id=tar_".$obj_id." />";
							} elseif($fld=="tr_dept" || $fld=="tr_dept_correct") {
								echo "<div class=left>".displayTRDept($values[$fld],"HTML")."</div>";
							} else {
								switch($h['h_type']) {
									case "TEXT": 
										if(in_array($fld,array('tr_perf','kr_perf'))) { 
											$class = "valid8me perf"; 
										} elseif(in_array($fld,array('kr_correct'))) { 
											$class = "valid8me correct"; 
										} else { 
											$class = ""; 
										}
										echo "<textarea rows=5 cols=25 name=".$fld."[] id=".$fld."_".$obj_id." class=\"$class\">".decode($values[$fld])."</textarea>"; break;
									case "NUM":
									default:	echo ($obj_tt==1?"R":"")."<input type=text size=5 value=\"".$values[$fld]."\" name=".$fld."[] class=\"valid8me right actual\" id=".$fld."_".$obj_id." />".($obj_tt==2?"%":"")."<input type=hidden size=5 value=\"".$values[$fld]."\" name=old_".$fld."[] class=\"\" id=old_".$fld."_".$obj_id." />"; break;
								}
							}
							echo "</td>";
						}
						if($section=="TOP") {
							echo "<td class=\"center time".$t_css."\">";
							switch($result_object[$ti]['tr_auto']) {
								case 1:
									echo "No";
									break;
								case 0:
									echo "Yes";
									break;
							}
							echo "</td>";
						}
						if(isset($time2[$ti_next]) && $setup_defaults[$section."_next_r"]=="Y") {
							$values_next = $result_object[$ti_next][$fld_target];
								echo "<td class=\"right time".($t_css-1)."\">".KPIresultDisplay($values_next,$obj_tt)."</td>";
						}
						break;
					case "CF":
						$result_object = $results[$obj_id][$filter['when'][0]];
						$t_css = 3;
						foreach($mheadings['CF_H'] as $rs => $cf_h) {
							$cfh_values = array();
							$budget = 0;
							foreach($mheadings['CF_R'] as $res => $cf_r) {
								$fld = "cr_".$cf_h['field'].$cf_r['field'];
								$cfh_values[$fld] = $result_object[$fld];
								switch($res) {
									case "_1": case "_2": case "_3":	$budget+=$cfh_values[$fld];				break;	//o. budget, adj. est., vire
									case "_4":							$cfh_values[$fld] = $budget;			break;	//adj. budget
									case "_5":							$actual = $cfh_values[$fld];			break;	//actual
									case "_6":							$cfh_values[$fld] = $actual - $budget;	break;	//variance
								}
								//if($filter['display']!="LIMIT" || $cf_r['c_list']) {
									$val = $cfh_values[$fld];
									$total[$rs][$res]+= $val;
									echo "<td class=\"right time".$t_css."\">";
										switch($cf_r['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
									echo "</td>";
								//}
							}
							$t_css = ($t_css<=1) ? 3 : $t_css-1;
						}
						break;
					case "CAP":
						$values = array();
						/*foreach($time as $ti => $t) {
							if($ti > $filter['when'][1]) {
								break;
							} elseif($ti>=$filter['when'][0]) {*/
								foreach($mheadings[$rheadings] as $r => $cr) {
									if(!isset($total[$ti][$r])) { $total[$ti][$r] = 0; }
									if(!isset($values[$r])) { $values[$r] = 0; }
									$val = $result_object[$ti][$r];
									$total[$ti][$r]+=$val;
									$values[$r]+=$val;
									//if($filter['display']!="LIMIT" || $cr['c_list']) {
										echo "<td class=\"right time".$t_css."\">";
										switch($cr['h_type']) {
											case "NUM": echo number_format($val,2); break;
											case "PERC": echo number_format($val,2)."%"; break;
											default: echo number_format($val,2); break;
										}
										echo "</th>";
									//}
								}
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
						break;
					default:
								$values['target'][$ti] = $result_object[$ti][$fld_target];
								$values['actual'][$ti] = $result_object[$ti][$fld_actual];
								$total[$ti]['target']+=$values['target'][$ti];
								$total[$ti]['actual']+=$values['actual'][$ti];
								echo "<td class=\"right time".$t_css."\">";
									echo "<input type=text value=\"".$values['target'][$ti]."\" />";
								echo "</td><td class=\"right time".$t_css."\">";
									echo number_format($values['actual'][$ti],2);
								echo "</td>";
								$t_css = ($t_css>=4) ? 1 : $t_css+1;
						break;
				}
			echo "<td class=centre><input type=hidden name=obj_id[] value=".$obj_id." class=objid />
						<input type=button value=Save id=".$obj_id." class=act1 />";
			if($section=="TOP" && !$result_object[$ti]['tr_auto']) {
				echo " <input type=button value=Unlock id=".$obj_id." class=act1 />";
			} elseif($section=="KPI" && $obj_tt==2 && $obj_ct=="CO" && isset($setup_defaults['PLC']) && $setup_defaults['PLC']=="Y") {
				echo " <input type=button value=PLC id=".$obj_id." class=plc />";
			}
			echo "</td>";
		echo "</tr>";
	}
	?>
	</tbody>
	<tfoot>
	<?php
	echo "<tr>";
		echo "<td>&nbsp;</td>";
		echo "<td colspan=".($total_columns+$col_past).">&nbsp;</td>";
		echo "<td colspan=$colspan class=\"time".$t_css." centre\">";
			echo "<input type=submit value=\"Save All\" class=isubmit />&nbsp;&nbsp;&nbsp;&nbsp;<input type=reset value=\" Reset \" class=idelete />";
		echo "</td>";
		echo "<td>&nbsp;</td>";
	echo "</tr>";
	?>
	</tfoot>
</table>

</form>
</div>
<script type="text/javascript"> 
$(function() {
cTR = $( "#tbl").find("tr");
$(cTR).each(function() {
	tr = $(this).find("th");
	$(tr).each(function() {
		if($(this).hasClass("thtd")) {
			$(this).addClass("locked");
		} else {
			/*if($(this).hasClass("fixed")) {
				$(this).addClass("isubmit");
			}*/
		}
	});
	tr = $(this).find("td");
	$(tr).each(function() {
		if($(this).hasClass("thtd")) {
			$(this).addClass("locked");
		} else {
			/*if($(this).hasClass("fixed")) {
				$(this).addClass("locked");
				var cssObj = { 
					'background-color' : '#ffffff', 
					'color' : '#000099', 
					'text-align' : 'left' 
				} 
				$(this).css(cssObj);
			}*/
		}
	});
});
});		
	setTimeout(function () { $("#dispRes").css('display','none') }, 2500);
		
	$(function() {
		$(".plc").click(function() {
			var id = $(this).attr("id");
			if(confirm("Moving to the Project Life Cycle for KPI D"+id+" will\nDISCARD ALL updates made on this page\nwhich have not yet been saved.\n\nAre you sure you wish to continue?")) {
				document.location.href = 'SDBP3/admin_kpi_edit_plc.php?page_id=edit&kpiid='+id;
			}
		});
		$(".act1").click(function() {
			var v = $(this).val();
			if(v=="Unlock") { var act = "UNLOCK"; } else { var act = "SAVE"; }
			$("#action").val(act);
			var flds = new Array('<?php echo implode("','",$flds); ?>');
			var id = $(this).attr("id");
			$("#obj_id").attr("value",id);
			var a;
			var b;
			var c;
				actual1 = 0;
				actual0 = 0;
				actual_valid = true;
				t = $("#tar_"+id).val();
				ct = $("#ct_"+id).val();
				valid8 = true;
			for(f in flds) { //alert(flds[f]+"start of for "+valid8);
				a = "#"+flds[f]+"_"+id;
				o = "#old_"+flds[f]+"_"+id;
				c = "#"+flds[f];
				b = $(a).attr("value");
				//alert(flds[f]+" :: b="+b+" :: "+actual1+" :: "+actual0);
				if(b.length==0 && $(a).hasClass("actual")) { b = "0"; $(a).val("0"); }
				if($(a).hasClass("valid8me")) {
					if($(a).hasClass("perf")) {
						if(actual_valid && actual1 != actual0) {
							new_valid8 = validateMe(flds[f],<?php echo $setup_defaults['perfcomm']=="Y" ? "true" : "false"; ?>,<?php echo checkIntRef($setup_defaults['commlen']) ? $setup_defaults['commlen'] : 0; ?>,b,t,actual1,ct);
							//alert("actual "+valid8);
						} else {
							new_valid8 = true;
							//alert("no change :: 1="+actual1+" :: 0="+actual0);
						}
					} else if ($(a).hasClass("correct")) {
						if(actual_valid && actual1 != actual0) {
							new_valid8 = validateMe(flds[f],<?php echo $setup_defaults['perfcomm']=="Y" ? "true" : "false"; ?>,<?php echo checkIntRef($setup_defaults['commlen']) ? $setup_defaults['commlen'] : 0; ?>,b,t,actual1,ct);
							//alert("validating correct "+new_valid8);
						} else {
							new_valid8 = true;
						}
						//alert(flds[f]+" :: "+v+" :: "+t+" :: "+actual1+" :: "+ct+" :: nv8-"+new_valid8+" :: v8-"+valid8);
					} else {
						new_valid8 = validateMe(flds[f],<?php echo $setup_defaults['perfcomm']=="Y" ? "true" : "false"; ?>,<?php echo checkIntRef($setup_defaults['commlen']) ? $setup_defaults['commlen'] : 0; ?>,b,t,actual1,ct);
					}
					//alert(flds[f]+" after vMe "+new_valid8);
					if(!new_valid8) {
						$(a).addClass("required");
					} else {
						$(a).removeClass("required");
					}
					//alert(flds[f]+" is red "+$(a).hasClass("required"));
					if($(a).hasClass("actual")) {
						actual_valid = new_valid8;
						if(actual_valid) {
							actual1 = b;
							actual0 = $(o).val();
						}
					}
					//alert(flds[f]+" :: "+actual1+" :: "+actual0+" :: "+new_valid8+" :: "+valid8);
					if(!new_valid8) { valid8 = false; } //alert("updated "+valid8);
				}
				$(c).attr("value",b);
			}
			//alert("final "+valid8);
			if(valid8) {
						$("#save_object").submit();
						//alert("final validation accepted");
			} else {
				//alert("Invalid actual captured.  Only numbers are allowed e.g. '40' or '45.67'.\n\nPlease review the actuals highlighted in red.");
				alert("Please review the missing/invalid information highlighted in red.\n\nPlease note the following:\n- Only numbers are allowed as Actuals e.g. '40' or '45.67'.<?php echo $setup_defaults['perfcomm']=="Y" ? "\\n- Performance Comment is required." : ""; ?>\n- Corrective Measures is required where the target has not been met/exceeded.");
			}
		});
		
		$("#save_all").submit(function() {
			var valid8 = true;
			var a;
			var b;
			var c;
			$(".objid").each(function() {
				var flds = new Array('<?php echo implode("','",$flds); ?>');
				id = $(this).val();
				actual1 = 0;
				actual0 = 0;
				actual_valid = true;
				t = $("#tar_"+id).val();
				ct = $("#ct_"+id).val();
				for(f in flds) {
					a = "#"+flds[f]+"_"+id;
					o = "#old_"+flds[f]+"_"+id;
					b = $(a).attr("value");
					if(b.length==0 && $(a).hasClass("actual")) { b = "0"; $(a).val("0"); }
					if($(a).hasClass("valid8me")) {
						if($(a).hasClass("perf")) {
							if(actual_valid && actual1 != actual0) {
								new_valid8 = validateMe(flds[f],<?php echo $setup_defaults['perfcomm']=="Y" ? "true" : "false"; ?>,<?php echo checkIntRef($setup_defaults['commlen']) ? $setup_defaults['commlen'] : 0; ?>,b,t,actual1,ct);
							} else {
								new_valid8 = true;
							}
						} else if ($(a).hasClass("correct")) {
							if(actual_valid && actual1 != actual0) {
								new_valid8 = validateMe(flds[f],<?php echo $setup_defaults['perfcomm']=="Y" ? "true" : "false"; ?>,<?php echo checkIntRef($setup_defaults['commlen']) ? $setup_defaults['commlen'] : 0; ?>,b,t,actual1,ct);
							} else {
								new_valid8 = true;
							}
						} else {
							new_valid8 = validateMe(flds[f],<?php echo $setup_defaults['perfcomm']=="Y" ? "true" : "false"; ?>,<?php echo checkIntRef($setup_defaults['commlen']) ? $setup_defaults['commlen'] : 0; ?>,b,t,actual1,ct);
						}
						if(!new_valid8) {
							$(a).addClass("required");
						} else {
							$(a).removeClass("required");
						}
						if($(a).hasClass("actual")) {
							actual_valid = new_valid8;
							if(actual_valid) {
								actual1 = b;
								actual0 = $(o).val();
							}
						}
					}
					if(!new_valid8) { valid8 = false; }
				}
			});
			if(!valid8) {
				//alert("Invalid actual captured.  Only numbers are allowed e.g. '40' or '45.67'.\n\nPlease review the actuals highlighted in red.");
				//alert("Please review the missing/invalid information highlighted in red.\n(Only numbers are allowed as Actuals e.g. '40' or '45.67'.)");
				alert("Please review the missing/invalid information highlighted in red.\n\nPlease note the following:\n- Only numbers are allowed as Actuals e.g. '40' or '45.67'.<?php echo $setup_defaults['perfcomm']=="Y" ? "\\n- Performance Comment is required." : ""; ?>\n- Corrective Measures is required where the target has not been met/exceeded.");
			}
			return valid8;
			//alert(valid8);
			//return false;
		});	
		
	});	
</script>