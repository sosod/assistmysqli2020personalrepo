<?php
	$target_start = 100;	//all
	$forecast_start = 100;	//top
	$forecast2_start = 100;	//cap
	$month_head = array();
	for($i=1;$i<13;$i++) {
		$month_head[$i] = date("F",mktime(12,0,0,$i+6,1,2012));
	}
switch($import_section) {
case "LIST":
	$import_columns = array(
		0=>array(
			'fld'=>"",
			'txt'=>"",
		),
		1=>array(
			'fld'=>"dir",
			'txt'=>"Ref",
		),
		2=>array(
			'fld'=>"dir",
			'txt'=>"Name",
		),
		3=>array(
			'fld'=>"",
			'txt'=>"",
		),
		4=>array(
			'fld'=>"",
			'txt'=>"",
		),
		5=>array(
			'fld'=>"subdir",
			'txt'=>"Parent ".$listoflists['dir']['h_client']." Ref",
		),
		6=>array(
			'fld'=>"subdir",
			'txt'=>"Parent ".$listoflists['dir']['h_client']." Ref",
		),
		7=>array(
			'fld'=>"subdir",
			'txt'=>"Name",
		),
		8=>array(
			'fld'=>"subdir",
			'txt'=>"Primary Y/N",
		),
		9=>array(
			'fld'=>"subdir",
			'txt'=>"Ref",
		),
		10=>array(
			'fld'=>"",
			'txt'=>"",
		),
		11=>array(
			'fld'=>"",
			'txt'=>"",
		),
		12=>array(
			'fld'=>"list_gfs",
			'txt'=>"Name",
		),
		13=>array(
			'fld'=>"list_gfs",
			'txt'=>"Ref",
		),
		14=>array(
			'fld'=>"",
			'txt'=>"",
		),
		15=>array(
			'fld'=>"list_munkpa",
			'txt'=>"Name",
		),
		16=>array(
			'fld'=>"list_munkpa",
			'txt'=>"Short Code",
		),
		17=>array(
			'fld'=>"list_munkpa",
			'txt'=>"Ref",
		),
		18=>array(
			'fld'=>"",
			'txt'=>"",
		),
		19=>array(
			'fld'=>"list_natkpa",
			'txt'=>"Name",
		),
		20=>array(
			'fld'=>"list_natkpa",
			'txt'=>"Short Code",
		),
		21=>array(
			'fld'=>"list_natkpa",
			'txt'=>"Ref",
		),
		22=>array(
			'fld'=>"",
			'txt'=>"",
		),
		23=>array(
			'fld'=>"list_natoutcome",
			'txt'=>"Name",
		),
		24=>array(
			'fld'=>"list_natoutcome",
			'txt'=>"Ref",
		),
		25=>array(
			'fld'=>"",
			'txt'=>"",
		),
		26=>array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Name",
		),
		27=>array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Associated ".$listoflists['list_munkpa']['h_client']." Name",
		),
		28=>array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Associated ".$listoflists['list_natkpa']['h_client']." Name",
		),
		29=>array(
			'fld'=>"list_idp_kpi",
			'txt'=>"Ref",
		),
		30=>array(
			'fld'=>"",
			'txt'=>"",
		),
		31=>array(
			'fld'=>"list_idp_top",
			'txt'=>"Name",
		),
		32=>array(
			'fld'=>"list_idp_top",
			'txt'=>"Ref",
		),
		33=>array(
			'fld'=>"",
			'txt'=>"",
		),
		34=>array(
			'fld'=>"list_kpiconcept",
			'txt'=>"Name",
		),
		35=>array(
			'fld'=>"list_kpiconcept",
			'txt'=>"Short Code",
		),
		36=>array(
			'fld'=>"list_kpiconcept",
			'txt'=>"Ref",
		),
		37=>array(
			'fld'=>"",
			'txt'=>"",
		),
		38=>array(
			'fld'=>"list_kpitype",
			'txt'=>"Name",
		),
		39=>array(
			'fld'=>"list_kpitype",
			'txt'=>"Short Code",
		),
		40=>array(
			'fld'=>"list_kpitype",
			'txt'=>"Ref",
		),
		41=>array(
			'fld'=>"",
			'txt'=>"",
		),
		42=>array(
			'fld'=>"list_riskrating",
			'txt'=>"Name",
		),
		43=>array(
			'fld'=>"list_riskrating",
			'txt'=>"Short Code",
		),
		44=>array(
			'fld'=>"list_riskrating",
			'txt'=>"Ref",
		),
		45=>array(
			'fld'=>"",
			'txt'=>"",
		),
		46=>array(
			'fld'=>"list_wards",
			'txt'=>"Ref",
		),
		47=>array(
			'fld'=>"list_wards",
			'txt'=>"Name",
		),
		48=>array(
			'fld'=>"list_wards",
			'txt'=>"Municipal Ref",
		),
		49=>array(
			'fld'=>"",
			'txt'=>"",
		),
		50=>array(
			'fld'=>"list_area",
			'txt'=>"Ref",
		),
		51=>array(
			'fld'=>"list_area",
			'txt'=>"Name",
		),
		52=>array(
			'fld'=>"",
			'txt'=>"",
		),
		53=>array(
			'fld'=>"list_owner",
			'txt'=>"Name",
		),
		54=>array(
			'fld'=>"",
			'txt'=>"",
		),
		55=>array(
			'fld'=>"",
			'txt'=>"",
		),
		56=>array(
			'fld'=>"",
			'txt'=>"",
		),
		57=>array(
			'fld'=>"",
			'txt'=>"",
		),
		58=>array(
			'fld'=>"",
			'txt'=>"",
		),
		59=>array(
			'fld'=>"",
			'txt'=>"",
		),
		60=>array(
			'fld'=>"",
			'txt'=>"",
		),
		61=>array(
			'fld'=>"",
			'txt'=>"",
		),
		62=>array(
			'fld'=>"",
			'txt'=>"",
		),
		63=>array(
			'fld'=>"list_fundsource",
			'txt'=>"Name",
		),
		64=>array(
			'fld'=>"list_fundsource",
			'txt'=>"Ref",
		),
	);
	break;
case "KPI":
	$target_start = 44;
	$target_text = "Target";
				$import_columns = array(
					0=>array(
						'fld'=>"kpi_id",
						'txt'=>"KPI Ref"),
					1=>array(
						'fld'=>"kpi_subid",
						'txt'=>"Ref"),
					2=>array(
						'fld'=>"kpi_subid",
						'txt'=>"Directorate"),
					3=>array(
						'fld'=>"kpi_subid",
						'txt'=>"Value"),
					4=>array(
						'fld'=>"kpi_topid",
						'txt'=>"Ref"),
					5=>array(
						'fld'=>"kpi_topid",
						'txt'=>"Directorate"),
					6=>array(
						'fld'=>"kpi_topid",
						'txt'=>"KPI Name"),
					7=>array(
						'fld'=>"kpi_gfsid",
						'txt'=>"Ref"),
					8=>array(
						'fld'=>"kpi_gfsid",
						'txt'=>"Value"),
					9=>array(
						'fld'=>'kpi_idpref',
						'txt'=>""),
					10=>array(
						'fld'=>'kpi_natoutcomeid',
						'txt'=>"Ref"),
					11=>array(
						'fld'=>'kpi_natoutcomeid',
						'txt'=>"Value"),
					12=>array(
						'fld'=>'kpi_idpid',
						'txt'=>"Ref"),
					13=>array(
						'fld'=>'kpi_idpid',
						'txt'=>"Value"),
					14=>array(
						'fld'=>'kpi_natkpaid',
						'txt'=>"Ref"),
					15=>array(
						'fld'=>'kpi_natkpaid',
						'txt'=>"Value"),
					16=>array(
						'fld'=>'kpi_munkpaid',
						'txt'=>"Ref"),
					17=>array(
						'fld'=>'kpi_munkpaid',
						'txt'=>"Value"),
					18=>array(
						'fld'=>'kpi_mtas',
						'txt'=>""),
					19=>array(
						'fld'=>'kpi_capitalid',
						'txt'=>"Ref"),
					20=>array(
						'fld'=>'kpi_capitalid',
						'txt'=>"Directorate"),
					21=>array(
						'fld'=>'kpi_capitalid',
						'txt'=>"Project Name"),
					22=>array(
						'fld'=>'kpi_value',
						'txt'=>""),
					23=>array(
						'fld'=>'kpi_unit',
						'txt'=>""),
					24=>array(
						'fld'=>'kpi_conceptid',
						'txt'=>"Ref"),
					25=>array(
						'fld'=>'kpi_conceptid',
						'txt'=>"Value"),
					26=>array(
						'fld'=>'kpi_typeid',
						'txt'=>"Ref"),
					27=>array(
						'fld'=>'kpi_typeid',
						'txt'=>"Value"),
					28=>array(
						'fld'=>'kpi_riskref',
						'txt'=>""),
					29=>array(
						'fld'=>'kpi_risk',
						'txt'=>""),
					30=>array(
						'fld'=>'kpi_riskratingid',
						'txt'=>"Ref"),
					31=>array(
						'fld'=>'kpi_riskratingid',
						'txt'=>"Value"),
					32=>array(
						'fld'=>'kpi_wards',
						'txt'=>""),
					33=>array(
						'fld'=>'kpi_area',
						'txt'=>""),
					34=>array(
						'fld'=>'kpi_ownerid',
						'txt'=>""),
					35=>array(
						'fld'=>'kpi_baseline',
						'txt'=>""),
					36=>array(
						'fld'=>'kpi_pyp',
						'txt'=>""),
					37=>array(
						'fld'=>'kpi_perfstd',
						'txt'=>""),
					38=>array(
						'fld'=>'kpi_poe',
						'txt'=>""),
					39=>array(
						'fld'=>'kpi_calctype',
						'txt'=>"Ref"),
					40=>array(
						'fld'=>'kpi_calctype',
						'txt'=>"Value"),
					41=>array(
						'fld'=>'kpi_targettype',
						'txt'=>"Ref"),
					42=>array(
						'fld'=>'kpi_targettype',
						'txt'=>"Value"),
					43=>array(
						'fld'=>"",
						'txt'=>""),
					44=>array(
						'fld'=>'target_1',
						'txt'=>$month_head[1]),
					45=>array(
						'fld'=>'target_2',
						'txt'=>$month_head[2]),
					46=>array(
						'fld'=>'target_3',
						'txt'=>$month_head[3]),
					47=>array(
						'fld'=>'target_4',
						'txt'=>$month_head[4]),
					48=>array(
						'fld'=>'target_5',
						'txt'=>$month_head[5]),
					49=>array(
						'fld'=>'target_6',
						'txt'=>$month_head[6]),
					50=>array(
						'fld'=>'target_7',
						'txt'=>$month_head[7]),
					51=>array(
						'fld'=>'target_8',
						'txt'=>$month_head[8]),
					52=>array(
						'fld'=>'target_9',
						'txt'=>$month_head[9]),
					53=>array(
						'fld'=>'target_10',
						'txt'=>$month_head[10]),
					54=>array(
						'fld'=>'target_11',
						'txt'=>$month_head[11]),
					55=>array(
						'fld'=>'target_12',
						'txt'=>$month_head[12]),
				);
	break;
	case "TOP":
		$target_start = 32;
		$forecast_start = 37;
		$target_text = "Target";
				$import_columns = array(
					0=>array(
						'fld'=>"top_id",
						'txt'=>"KPI Ref"),
					1=>array(
						'fld'=>"top_dirid",
						'txt'=>"Ref"),
					2=>array(
						'fld'=>"top_dirid",
						'txt'=>"Value"),
					3=>array(
						'fld'=>"top_repkpi",
						'txt'=>""),
					4=>array(
						'fld'=>"top_pmsref",
						'txt'=>""),
					5=>array(
						'fld'=>"top_gfsid",
						'txt'=>"Ref"),
					6=>array(
						'fld'=>"top_gfsid",
						'txt'=>"Value"),
					7=>array(
						'fld'=>"top_natoutcomeid",
						'txt'=>"Ref"),
					8=>array(
						'fld'=>"top_natoutcomeid",
						'txt'=>"Value"),
					9=>array(
						'fld'=>"top_natkpaid",
						'txt'=>"Ref"),
					10=>array(
						'fld'=>"top_natkpaid",
						'txt'=>"Value"),
					11=>array(
						'fld'=>"top_mtas",
						'txt'=>""),
					12=>array(
						'fld'=>"top_idp",
						'txt'=>"Ref"),
					13=>array(
						'fld'=>"top_idp",
						'txt'=>"Value"),
					14=>array(
						'fld'=>"top_munkpaid",
						'txt'=>"Ref"),
					15=>array(
						'fld'=>"top_munkpaid",
						'txt'=>"Value"),
					16=>array(
						'fld'=>"top_value",
						'txt'=>""),
					17=>array(
						'fld'=>"top_unit",
						'txt'=>""),
					18=>array(
						'fld'=>"top_risk",
						'txt'=>""),
					19=>array(
						'fld'=>"top_wards",
						'txt'=>""),
					20=>array(
						'fld'=>"top_area",
						'txt'=>""),
					21=>array(
						'fld'=>"top_ownerid",
						'txt'=>""),
					22=>array(
						'fld'=>"top_baseline",
						'txt'=>""),
					23=>array(
						'fld'=>"top_poe",
						'txt'=>""),
					24=>array(
						'fld'=>"top_pyp",
						'txt'=>""),
					25=>array(
						'fld'=>"top_calctype",
						'txt'=>"Ref"),
					26=>array(
						'fld'=>"top_calctype",
						'txt'=>"Value"),
					27=>array(
						'fld'=>"top_targettype",
						'txt'=>"Ref"),
					28=>array(
						'fld'=>"top_targettype",
						'txt'=>"Value"),
					29=>array(
						'fld'=>"top_annual",
						'txt'=>""),
					30=>array(
						'fld'=>"top_revised",
						'txt'=>""),
					31=>array(
						'fld'=>"",
						'txt'=>""),
					32=>array(
						'fld'=>"target_1",
						'txt'=>$month_head[3]),
					33=>array(
						'fld'=>"target_2",
						'txt'=>$month_head[6]),
					34=>array(
						'fld'=>"target_3",
						'txt'=>$month_head[9]),
					35=>array(
						'fld'=>"target_4",
						'txt'=>$month_head[12]),
					36=>array(
						'fld'=>"",
						'txt'=>""),
					37=>array(
						'fld'=>"forecast_1",
						'txt'=>"2013/2014"),
					38=>array(
						'fld'=>"forecast_2",
						'txt'=>"2014/2015"),
					39=>array(
						'fld'=>"forecast_3",
						'txt'=>"2015/2016"),
					40=>array(
						'fld'=>"forecast_4",
						'txt'=>"2016/2017"),
				);
	break;
case "RS":
		$target_start = 3;
		$target_text = "Budget";
				$import_columns = array(
					0=>array(
						'fld'=>"rs_id",
						'txt'=>"Line Ref"),
					1=>array(
						'fld'=>"rs_value",
						'txt'=>""),
					2=>array(
						'fld'=>"rs_vote",
						'txt'=>""),
					3=>array(
						'fld'=>"budget_1",
						'txt'=>$month_head[1]),
					4=>array(
						'fld'=>"budget_2",
						'txt'=>$month_head[2]),
					5=>array(
						'fld'=>"budget_3",
						'txt'=>$month_head[3]),
					6=>array(
						'fld'=>"budget_4",
						'txt'=>$month_head[4]),
					7=>array(
						'fld'=>"budget_5",
						'txt'=>$month_head[5]),
					8=>array(
						'fld'=>"budget_6",
						'txt'=>$month_head[6]),
					9=>array(
						'fld'=>"budget_7",
						'txt'=>$month_head[7]),
					10=>array(
						'fld'=>"budget_8",
						'txt'=>$month_head[8]),
					11=>array(
						'fld'=>"budget_9",
						'txt'=>$month_head[9]),
					12=>array(
						'fld'=>"budget_10",
						'txt'=>$month_head[10]),
					13=>array(
						'fld'=>"budget_11",
						'txt'=>$month_head[11]),
					14=>array(
						'fld'=>"budget_12",
						'txt'=>$month_head[12]),
				);
	break;
	case "CAP":
		$forecast2_start = 31;
		$target_start = 18;
		$target_text = "Budget";
				$import_columns = array(
					0=>array(
						'fld'=>"cap_id",
						'txt'=>"Project Ref"),
					1=>array(
						'fld'=>"cap_subid",
						'txt'=>"Ref"),
					2=>array(
						'fld'=>"cap_subid",
						'txt'=>"Directorate"),
					3=>array(
						'fld'=>"cap_subid",
						'txt'=>"Value"),
					4=>array(
						'fld'=>"cap_gfsid",
						'txt'=>"Ref"),
					5=>array(
						'fld'=>"cap_gfsid",
						'txt'=>"Value"),
					6=>array(
						'fld'=>"cap_cpref",
						'txt'=>""),
					7=>array(
						'fld'=>"cap_idpref",
						'txt'=>""),
					8=>array(
						'fld'=>"cap_vote",
						'txt'=>""),
					9=>array(
						'fld'=>"cap_name",
						'txt'=>""),
					10=>array(
						'fld'=>"cap_descrip",
						'txt'=>""),
					11=>array(
						'fld'=>"capital_fundsource",
						'txt'=>""),
					12=>array(
						'fld'=>"cap_planstart",
						'txt'=>""),
					13=>array(
						'fld'=>"cap_planend",
						'txt'=>""),
					14=>array(
						'fld'=>"cap_actualstart",
						'txt'=>""),
					15=>array(
						'fld'=>"capital_wards",
						'txt'=>""),
					16=>array(
						'fld'=>"capital_area",
						'txt'=>""),
					17=>array(
						'fld'=>"",
						'txt'=>""),
					18=>array(
						'fld'=>"target_1",
						'txt'=>$month_head[1]),
					19=>array(
						'fld'=>"target_2",
						'txt'=>$month_head[2]),
					20=>array(
						'fld'=>"target_3",
						'txt'=>$month_head[3]),
					21=>array(
						'fld'=>"target_4",
						'txt'=>$month_head[4]),
					22=>array(
						'fld'=>"target_5",
						'txt'=>$month_head[5]),
					23=>array(
						'fld'=>"target_6",
						'txt'=>$month_head[6]),
					24=>array(
						'fld'=>"target_7",
						'txt'=>$month_head[7]),
					25=>array(
						'fld'=>"target_8",
						'txt'=>$month_head[8]),
					26=>array(
						'fld'=>"target_9",
						'txt'=>$month_head[9]),
					27=>array(
						'fld'=>"target_10",
						'txt'=>$month_head[10]),
					28=>array(
						'fld'=>"target_11",
						'txt'=>$month_head[11]),
					29=>array(
						'fld'=>"target_12",
						'txt'=>$month_head[12]),
					30=>array(
						'fld'=>"",
						'txt'=>""),
					31=>array(
						'fld'=>"forecast_1_1",
						'txt'=>"2012/2013"),
					32=>array(
						'fld'=>"forecast_1_2",
						'txt'=>"2012/2013"),
					33=>array(
						'fld'=>"forecast_3_1",
						'txt'=>"2013/2014"),
					34=>array(
						'fld'=>"forecast_3_2",
						'txt'=>"2013/2014"),
					35=>array(
						'fld'=>"forecast_4_1",
						'txt'=>"2014/2015"),
					36=>array(
						'fld'=>"forecast_4_2",
						'txt'=>"2014/2015"),
					37=>array(
						'fld'=>"forecast_5_1",
						'txt'=>"2015/2016"),
					38=>array(
						'fld'=>"forecast_5_2",
						'txt'=>"2015/2016"),
					39=>array(
						'fld'=>"forecast_6_1",
						'txt'=>"2016/2017"),
					40=>array(
						'fld'=>"forecast_6_2",
						'txt'=>"2016/2017"),
				);
	break;
case "CF":
	$target_start = 8;
	$target_text = "Budget";
				$import_columns = array(
					0=>array(
						'fld'=>"cf_subid",
						'txt'=>"Ref"),
					1=>array(
						'fld'=>"cf_subid",
						'txt'=>"Directorate"),
					2=>array(
						'fld'=>"cf_subid",
						'txt'=>"Value"),
					3=>array(
						'fld'=>"cf_value",
						'txt'=>""),
					4=>array(
						'fld'=>"cf_gfsid",
						'txt'=>"Ref"),
					5=>array(
						'fld'=>"cf_gfsid",
						'txt'=>"Value"),
					6=>array(
						'fld'=>"cf_vote",
						'txt'=>""),
					7=>array(
						'fld'=>"",
						'txt'=>""),
					8=>array(
						'fld'=>"cf_rev_1_1",
						'txt'=>""),
					9=>array(
						'fld'=>"cf_opex_1_1",
						'txt'=>""),
					10=>array(
						'fld'=>"cf_capex_1_1",
						'txt'=>""),
					11=>array(
						'fld'=>"cf_rev_1_2",
						'txt'=>""),
					12=>array(
						'fld'=>"cf_opex_1_2",
						'txt'=>""),
					13=>array(
						'fld'=>"cf_capex_1_2",
						'txt'=>""),
					14=>array(
						'fld'=>"cf_rev_1_3",
						'txt'=>""),
					15=>array(
						'fld'=>"cf_opex_1_3",
						'txt'=>""),
					16=>array(
						'fld'=>"cf_capex_1_3",
						'txt'=>""),
					17=>array(
						'fld'=>"cf_rev_1_4",
						'txt'=>""),
					18=>array(
						'fld'=>"cf_opex_1_4",
						'txt'=>""),
					19=>array(
						'fld'=>"cf_capex_1_4",
						'txt'=>""),
					20=>array(
						'fld'=>"cf_rev_1_5",
						'txt'=>""),
					21=>array(
						'fld'=>"cf_opex_1_5",
						'txt'=>""),
					22=>array(
						'fld'=>"cf_capex_1_5",
						'txt'=>""),
					23=>array(
						'fld'=>"cf_rev_1_6",
						'txt'=>""),
					24=>array(
						'fld'=>"cf_opex_1_6",
						'txt'=>""),
					25=>array(
						'fld'=>"cf_capex_1_6",
						'txt'=>""),
					26=>array(
						'fld'=>"cf_rev_1_7",
						'txt'=>""),
					27=>array(
						'fld'=>"cf_opex_1_7",
						'txt'=>""),
					28=>array(
						'fld'=>"cf_capex_1_7",
						'txt'=>""),
					29=>array(
						'fld'=>"cf_rev_1_8",
						'txt'=>""),
					30=>array(
						'fld'=>"cf_opex_1_8",
						'txt'=>""),
					31=>array(
						'fld'=>"cf_capex_1_8",
						'txt'=>""),
					32=>array(
						'fld'=>"cf_rev_1_9",
						'txt'=>""),
					33=>array(
						'fld'=>"cf_opex_1_9",
						'txt'=>""),
					34=>array(
						'fld'=>"cf_capex_1_9",
						'txt'=>""),
					35=>array(
						'fld'=>"cf_rev_1_10",
						'txt'=>""),
					36=>array(
						'fld'=>"cf_opex_1_10",
						'txt'=>""),
					37=>array(
						'fld'=>"cf_capex_1_10",
						'txt'=>""),
					38=>array(
						'fld'=>"cf_rev_1_11",
						'txt'=>""),
					39=>array(
						'fld'=>"cf_opex_1_11",
						'txt'=>""),
					40=>array(
						'fld'=>"cf_capex_1_11",
						'txt'=>""),
					41=>array(
						'fld'=>"cf_rev_1_12",
						'txt'=>""),
					42=>array(
						'fld'=>"cf_opex_1_12",
						'txt'=>""),
					43=>array(
						'fld'=>"cf_capex_1_12",
						'txt'=>""),
				);

	break;
}
$cells = array();
foreach($import_columns as $c => $cl) {
	if(strlen($cl['fld'])==0 || !in_array($cl['fld'],$cells)) {
		$cells[$c] = $cl['fld'];
	}
}







/** FUNCTIONS **/
function drawCSVColumnsGuidelines($id_fld,$target_text="Target") {
	global $mheadings, $import_columns;
	global $target_start, $forecast_start, $forecast2_start;
	global $import_section;
	global $month_head;
	echo "
			<h3>CSV Columns</h3>
			<table>
				<tr>
					<th>Column</th>
					<th>Heading</th>
				</tr>";
for($i=1;$i<=count($import_columns);$i++) {
	echo "<tr><td class='center b'>";
	if($i<=26) {
		echo chr(64+$i);
	} else {
		echo chr(64+floor(($i-1)/26)).chr(64+$i-(floor(($i-1)/26)*26));
	}
	$col = $import_columns[$i-1];
	$fld = isset($col['fld']) ? $col['fld'] : "";
	if($fld==$id_fld) { 
		$txt = $col['txt']; 
	} else {
		if(strlen($fld)==0) { 
			$txt = "";
		} elseif($i>$forecast2_start) { 
			$txt = $col['txt']." Forecast ".$target_text.": ".($i % 2 == 1 ? "Other" : "CRR");
		} elseif($i>$forecast_start) { 
			$txt = "Forecast ".$target_text;
		} elseif($i>$target_start) { 
			if($import_section=="CF") { 
				$calc = ($i-$target_start)%3;
				switch($calc) {
				case 1:	$txt = "Revenue"; break;
				case 2: $txt = "Op. Exp."; break;
				case 0: $txt = "Cap. Exp."; break;
				}
				$txt.= " ".$target_text;
				$mon = explode("_",$fld);
				$txt = $month_head[$mon[3]]." ".$txt;
			} else {
				$txt = $target_text;
			}
		} else {
			$txt = isset($mheadings[$import_section][$fld]['h_client']) ? $mheadings[$import_section][$fld]['h_client'] : "";
		}
		$txt.= isset($col['txt']) && $i<=$forecast2_start && strlen($col['txt'])>0 ? ": ".$col['txt'] : "";
	}
	echo "</td><td>";
		echo $txt;
	echo "</td></tr>";
}
	echo "
	</table>";
	displayGoBack("support_import.php");
}

function getImportHeadings() {
	global $mheadings, $import_columns, $cells, $colspanned;
	global $doc_headings, $doc_headings2;
	global $target_start, $forecast_start, $forecast2_start;
	global $import_section;
	global $month_head;
$onscreen = "<tr><th rowspan=2 style='vertical-align: middle;'> ? </th>";
	$cdata = array();
		for($i=0;$i<count($import_columns);$i++) {
			if(isset($cells[$i])) {
				$col = $import_columns[$i];
				$fld = isset($col['fld']) ? $col['fld'] : "";
				$colspan = 1;
					if(isset($colspanned[2]) && is_array($colspanned[2]) && in_array($i,$colspanned[2])) {
						$colspan = 2;
					} elseif(isset($colspanned[3]) && is_array($colspanned[3]) && in_array($i,$colspanned[3])) {
						$colspan = 3;
					}
				if(strlen($fld)==0) {
					$txt = "";
				} elseif($fld==strtolower($import_section)."_id") { 
					$txt = $col['txt']; 
				} elseif($i>=$forecast2_start) { 
					$txt = ($i%2==0 ? $col['txt'].": Other" : $col['txt'].": CRR");
				} elseif($i>=$forecast_start) { 
					$txt = $col['txt']." Forecast";
				} elseif($i>=$target_start) { 
					if($import_section=="CF") { 
						$f = explode("_",$fld);
						$txt = $month_head[$f[3]];
						switch($f[1]) {
							case "rev": $txt.= ": Revenue"; break;
							case "opex": $txt.= ": Op.&nbsp;Exp."; break;
							case "capex": $txt.= ": Cap.&nbsp;Exp."; break;
						}
					} else {
						$txt = isset($col['txt']) && strlen($col['txt'])>0 ? $col['txt'] : "";
					}
				} else {
					$txt = isset($mheadings[$import_section][$fld]['h_client']) ? $mheadings[$import_section][$fld]['h_client'] : "";
				}
				$cdata[]="<th class=ignite colspan=".$colspan.">".$txt."</th>";
				//$cdata[] = "<th>".$txt.":".$i."</th>";
			} else {
				$txt = "";
			}
		}
	$onscreen.= implode("",$cdata);
	
					$onscreen.= "</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							if($cell < count($import_columns)) {
								$colspan = in_array($cell,$colspanned[2]) ? 2 : 1;
								$colspan = in_array($cell,$colspanned[3]) ? 3 : $colspan;
								if($import_section=="CAP" && $cell>=$forecast2_start) {
									$c = (strlen($c)>0 ? $c : $doc_headings[$cell-1]).": ".$doc_headings2[$cell];
								} elseif($import_section=="CF" && $cell>=$target_start) {
									if(strlen($c)==0) {
										$c = $doc_headings[$cell-1];
									}
									if(strlen($c)==0) {
										$c = $doc_headings[$cell-2];
									}
									$c.=": ".$doc_headings2[$cell];
								}
								if(strlen($c)>0 || $cell>=($target_start-1)) {
									$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
								}
							}
						}
					$onscreen.= "</tr>";

	return $onscreen;
}
?>