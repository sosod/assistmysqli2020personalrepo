<?php 
$get_lists = true;
$get_active_lists = true;
$section = "KPI";
$page_title = "Projet Life Cycle";
$obj_id = $_REQUEST['id'];
$jquery_date = true;
include("inc/header.php"); 

if(isset($_REQUEST['id']) && checkIntRef($_REQUEST['id'])) {
	$obj_id = $_REQUEST['id'];
	$o_sql = "SELECT * FROM ".$dbref."_kpi WHERE kpi_id = ".$obj_id." AND kpi_active = true";
	$object = mysql_fetch_all($o_sql);
	$object = $object[0];
} else {
	die(displayResult(array("error","An error occurred while trying to open the PLC.  Please go back and try again.")));
}
?>
<form id=plc_form action=manage_dept_plc_review.php method=post><input type=hidden name=act value=PREVIEW />
<table class="noborder"><tr class="no-highlight"><td class="noborder no-highlight">
<h2>PLC Details</h2>
<table id=details>
	<tr>
		<th>KPI #:</th>
		<td><?php echo $id_labels_all[$section].$obj_id; ?><input type=hidden name=plc_kpiid value=<?php echo $obj_id; ?> /></td>
	</tr>
	<tr>
		<th>Project Name:</th>
		<td><?php
			$fld = "kpi_value"; 
			$h = $mheadings[$section][$fld];
			echo "<textarea rows=4 cols=60 name=plc_name id=$fld>".decode($object[$fld])."</textarea>"; 
		?></td>
	</tr>
	<tr>
		<th>Project Description:</th>
		<td><?php 
			$fld = "kpi_unit"; 
			$h = $mheadings[$section][$fld];
			echo "<textarea rows=4 cols=60 name=plc_description id=$fld>".decode($object[$fld])."</textarea>"; 
			echo "<br /><label id=".$fld."-c>".($h['c_maxlen'] - strlen(decode($object[$fld])))."</label> characters remaining<input type=hidden id=".$fld."-max value=\"".$h['c_maxlen']."\" />";
		?></td>
	</tr>
	<tr>
		<th>Start Date:</th>
		<td><input type=text class=datepicker id=firststart name=plc_startdate value=<?php echo date("Y-m-d"); ?> /></td>
	</tr>
	<tr>
		<th>End Date:</th>
		<td><label id=final_enddate /></label><input type=hidden name=plc_enddate id=txt_enddate value="" /></td>
	</tr>
	<tr>
		<th>Use PLC to Update<br />KPI Results?:</th>
		<td><select name=plc_updatekpi id=plc_up>
			<option selected value=ACTUAL>Actuals only</option>
			<option value=ALL>Targets & Actuals*</option>
			<option value=N>No</option>
		</select><br /><span style="font-size: 6.5pt; font-style: italic">*Updating KPI Targets from the PLC requires authorisation from the KPI Administrator.</span></td>
	</tr>
</table>
<?php 

/*** GET PHASES ***/
$plc = array('std'=>array(),'proc'=>array(),'add'=>array());
$sql = "SELECT * FROM ".$dbref."_list_plc_std WHERE yn <> 'N' ORDER BY sort";
$p = mysql_fetch_alls($sql,"id");
$plc['std'] = $p;
$sql = "SELECT * FROM ".$dbref."_list_plc_proc WHERE yn <> 'N' ORDER BY sort";
$p = mysql_fetch_alls($sql,"id");
$plc['proc'] = $p;
$sql = "SELECT * FROM ".$dbref."_list_plc_custom WHERE yn <> 'N' ORDER BY value";
$p = mysql_fetch_alls($sql,"id");
$plc['add'] = $p;

?>
<h2>PLC Phases</h2>
<table id=phases>
<thead>
	<tr>
		<th colspan=2>Phases</th>
		<th>     Use?    </th>
		<th> Calendar Days </th>
		<th>  Start Date  </th>
		<th>   End Date   </th>
		<th> % of Project </th>
		<th>  Completed?  </th>
		<th>Completion Date</th>
	</tr>
</thead>
<tbody>
<?php 
$pid_array = array();
foreach($plc['std'] as $i => $p) { 
	if($p['type']=="P") {
		$days = array(1=>array(),2=>array(),3=>array(),4=>array());
		$proc_id = array();
		echo "<tr>
			<td class=phase rowspan=".count($plc['proc'])."><input type=hidden value=\"S_".$i."\" class=pid />".$p['value'].":
			<br /><span style=\"font-style: italic;\">Project Category:</span> <select id=proc_cate name=plc_proccate>
																					<option selected value=1>1</option>
																					<option value=2>2</option>
																					<option value=3>3</option>
																					<option value=4>4</option>
																				</select></td>";
		$pc = 0;
		foreach($plc['proc'] as $pi => $pp) {
			$days[1]['P_'.$pi] = $pp['days1'];	$days[2]['P_'.$pi] = $pp['days2'];	$days[3]['P_'.$pi] = $pp['days3'];	$days[4]['P_'.$pi] = $pp['days4'];
			$proc_id[] = "P_".$pi;
			$pid_array[] = "P_".$pi;
			if($pc>0) { echo "<tr>"; }	$pc++;
			echo "<td class=\"phase proc\"><input type=hidden value=\"P_".$pi."\" class=pid name=pp_type_id[] />".$pp['value'].":</td>";
			echo "<td>".($p['yn']=="R" ? "<span class=isubmit style=\"font-weight: normal\">Required</span><input type=text id=use-P_".$pi." value=R  name=pp[P_".$pi."][use] />" : "<select id=use-P_".$pi." class=use name=pp[P_".$pi."][use]><option selected class=isubmit value=Y>Yes</option><option class=idelete value=N>No</option></select>")."</td>
				<td><input type=text size=3 id=days-P_".$pi." class=days value=".$pp['days1']." name=pp[P_".$pi."][days] /></td>
				<td><input type=text size=10 class=datepicker id=start-P_".$pi." name=pp[P_".$pi."][start] /></td>
				<td>&nbsp;<label id=lblend-P_".$pi."></label><input type=hidden size=10 id=end-P_".$pi." name=pp[P_".$pi."][end] />&nbsp;</td>
				<td><span id=lblperc-P_".$pi."><input type=text size=3 id=perc-P_".$pi." value=0 class=perc name=pp[P_".$pi."][perc] />%</span></td>
				<td><input type=checkbox id=fini-P_".$pi." value=Y  name=pp[P_".$pi."][done] /></td>
				<td><input type=text size=10 class=datepicker id=finidate-P_".$pi." name=pp[P_".$pi."][donedate] /></td>
			</tr>";
		}
	} elseif($p['type']=="A") {
		echo "<tr>
			<td class=phase rowspan=10><input type=hidden value=\"S_".$i."\" class=pid  />".$p['value'].":</td>";
		for($pc=0;$pc<10;$pc++) {
			$pid_array[] = "A_".$pc;
			if($pc>0) { echo "<tr id=\"tr-A_".$pc."\">"; }
			echo "<td class=\"phase custom\" id=\"A_".$pc."\"><input type=hidden value=\"A_".$pc."\" class=pid name=pp_type_id[] />
					<select id=cust-A_".$pc." class=custom_phase name=custom_phase[A_".$pc."][pp] >
						<option selected value=X>--- SELECT ---</option>
						<option value=add>Add new</option>
					</select>
					<span id=span-add-A_".$pc."><br /><input class=new_custom type=text size=30 maxlength=50 id=add-A_".$pc." name=custom_phase[A_".$pc."][new] /></span>
					</td>";
			echo "<td><select id=use-A_".$pc." class=use name=pp[A_".$pc."][use]><option value=Y class=isubmit>Yes</option><option selected value=N class=idelete>No</option></select></td>
				<td><input type=text size=3 id=days-A_".$pc." class=days value=1 name=pp[A_".$pc."][days] /></td>
				<td><input type=text size=10 class=datepicker id=start-A_".$pc." name=pp[A_".$pc."][start] /></td>
				<td>&nbsp;<label id=lblend-A_".$pc."></label><input type=hidden size=10 id=end-A_".$pc." name=pp[A_".$pc."][end] />&nbsp;</td>
				<td><span id=lblperc-A_".$pc."><input type=text size=3 id=perc-A_".$pc." value=0 class=perc name=pp[A_".$pc."][perc] />%</span></td>
				<td><input type=checkbox id=fini-A_".$pc." value=Y  name=pp[A_".$pc."][done] /></td>
				<td><input type=text size=10 class=datepicker id=finidate-A_".$pc." name=pp[A_".$pc."][donedate] /></td>
			</tr>";
		}
	} else {
		$pid_array[] = "S_".$i;
		echo "<tr>
			<td class=phase colspan=2><input type=hidden value=\"S_".$i."\" class=pid name=pp_type_id[] />".$p['value'].":</td>
			<td>".($p['yn']=="R" ? "<span class=isubmit style=\"font-weight: normal\">Required</span><input type=hidden name=pp[S_".$i."][use] id=use-S_".$i." value=R />" : "<select name=pp[S_".$i."][use] id=use-S_".$i." class=use><option selected class=isubmit value=Y>Yes</option><option class=idelete value=N>No</option></select>")."</td>
			<td><input type=text size=3 id=days-S_".$i." class=days value=1 name=pp[S_".$i."][days] /></td>
			<td><input type=text size=10 class=datepicker id=start-S_".$i." name=pp[S_".$i."][start] /></td>
			<td>&nbsp;<label id=lblend-S_".$i."></label><input type=hidden size=10 id=end-S_".$i." name=pp[S_".$i."][end] />&nbsp;</td>
			<td><span id=lblperc-S_".$i."><input type=text size=3 id=perc-S_".$i." value=0 class=perc name=pp[S_".$i."][perc] />%</span></td>
			<td><input type=checkbox id=fini-S_".$i." value=Y  name=pp[S_".$i."][done] /></td>
			<td><input type=text size=10 class=datepicker id=finidate-S_".$i." name=pp[S_".$i."][donedate] /></td>
		</tr>";
	}
} ?>
</tbody>
<tfoot>
	<tr>
		<td colspan=2></th>
		<td></th>
		<td id=total-days></td>
		<td id=first-start></td>
		<td id=final-end></td>
		<td><label id=total-perc>0</label>%</td>
		<td></td>
		<td></td>
	</tr>
</tfoot>
</table>
<p>&nbsp;</p>
<table width=100%><tr><td class=center><input type=submit value=Submit class=isubmit /> <input type=reset /> <span class=float><input type=button value=Cancel class=idelete /></span></td></tr></table>
</td></tr></table>
</form>
<script type=text/javascript>
$(function() {
	var phase_fields = new Array("use","days","start","end","perc","fini","finidate");
	var pid_array = new Array(<?php echo "'".implode("','",$pid_array)."'"; ?>);
	var proc_array = new Array(<?php echo "'".implode("','",$proc_id)."'"; ?>);
	var days1 = new Array(<?php echo "'".implode("','",$days[1])."'"; ?>);
	var days2 = new Array(<?php echo "'".implode("','",$days[2])."'"; ?>);
	var days3 = new Array(<?php echo "'".implode("','",$days[3])."'"; ?>);
	var days4 = new Array(<?php echo "'".implode("','",$days[4])."'"; ?>);
	var field_to_start_date = "end";
	/* ON LOAD */
	/*$(".datepicker").each(function() {
		i = $(this).attr("id");
		if(i.substr(0,1)=="f") {
			$(this).datepicker("option","maxDate",'0d');
		}
	});*/
	$("#details th").addClass("top left");
	$("#phases input:text").addClass("right");
	$("#phases .new_custom").removeClass("right");
	$("#phases td").addClass("center");
	$("#phases td.phase").removeClass("center");
	$("#details textarea").trigger("keyup");
	$("#phases .proc").css("font-style","italic");
	$("#phases tfoot tr").css("font-weight","bold");
	/* ON DETAILS EDIT */
		$("#details textarea").keyup(function() {
			i = $(this).attr("id");
			if(i.indexOf("-")<0) {
				//get details
				v = $(this).text();
				//check maxlength limit
				m = $("#"+i+"-max").val();
				rem = m - v.length;
				if(rem>0) {
					$("#"+i+"-c").text(rem);
					$("#"+i+"-c").css({"color":"#000000","font-weight":"normal","background-color":"#FFFFFF"});
				} else {
					$("#"+i+"-c").text("0");
					$(this).text(v.substring(0,m));
					$("#"+i+"-c").css({"color":"#FFFFFF","font-weight":"bold","background-color":"#CC0001"});
				}
				//check required
				req = $("#"+i+"-r").val();
				if(req=="Y") {
					if(v.length==0) {
						$(this).addClass("required");
					} else {
						$(this).removeClass("required");
					}
				}
			}
		});
		$("#phases select.use").change(function() {
			total_days = 0;
			ds = $("#details #firststart").val();
			my_date = createDate(ds);
			started = false;
			//last_date = first_date;
			found = false;
			original_i = $(this).attr("id");
			for(p in pid_array) {
				i = pid_array[p];
				use = $("#phases #use-"+i).val();
					if(use=="Y" || use=="R") {
						days = parseInt($("#phases #days-"+i).val());
						total_days+=days;
					}
				if($(this).attr("id")=="use-"+i) { 
					found = true; 
					if(p>0) {
						if(field_to_start_date == "end") {
							ds = $("#phases #end-"+pid_array[p-1]).val();
							started = true;
						} else {
							ds = $("#phases #start-"+i).val();
						}
						my_date = createDate(ds);
					}
				}
				if(found) {
					if(use=="Y" || use=="R") {
						useHideShow(i,"show");
						if(!started) {
							$("#phases #start-"+i).val(displayDate(my_date));
							started = true;
						} else {
							my_date.setDate(my_date.getDate()+1);
							$("#phases #start-"+i).val(displayDate(my_date));
						}
						my_date.setDate(my_date.getDate()+days);
						$("#phases #end-"+i).val(displayDate(my_date));
						$("#phases #lblend-"+i).attr("innerText",displayDate(my_date));
					} else {
						useHideShow(i,"hide");
					}
				}
			}
			field_to_start_date = "end";
			$("#details #final_enddate").attr("innerText",displayDate(my_date)); 
			$("#phases #total-days").attr("innerText",total_days+' days');
			$("#phases #final-end").attr("innerText",displayDate(my_date));
			$("#details #txt_enddate").val(displayDate(my_date));
		});
		$("#phases .days").blur(function() {
			if(isNaN(parseInt($(this).val()))) {
				alert("Invalid number");
				$(this).val(1);
			} else {
				$(this).val(parseInt($(this).val()));
				i = $(this).attr("id");
				i = i.substring(5,i.length)
				$("#phases #use-"+i).trigger("change");
			}
		});
		$(".datepicker").change(function() { 
			i = $(this).attr("id");
			if(i.substr(0,1)=="s") {
				fi = $("#phases .datepicker:first").attr("id");
				if(i==fi) { $("#details #firststart").val($(this).val()); } 
				i = i.substring(6,i.length);
				field_to_start_date = "start";
				$("#phases #use-"+i).trigger("change"); 
			}
		});
		$("#phases input:checkbox").click(function() {
			i = $(this).attr("id");
			checkFini(i,$(this));
			//alert($(this).attr("checked"));
		});
		$("#phases select.custom_phase").change(function() {
			v = $(this).val();
			vi = $(this).attr("id");
			i = vi.substring(5,vi.length);
			if(v=="X") {
				$("#phases #use-"+i).attr("disabled","false");
				customKillMe(i);
			} else {
				if(v=="add") {
					x = "#phases #span-add-"+i;
					$(x).show();
					$("#phases #use-"+i).show();
					$("#phases #use-"+i).attr("disabled","true");
				} else {
					$("#phases #use-"+i).show();
					$("#phases #use-"+i).attr("disabled","false");
					$("#phases #use-"+i).val("Y");
				}
				x = "";
				switch(i) {
					case "A_1": x = "#phases #cust-A_2"; break;
					case "A_2": x = "#phases #cust-A_3"; break;
					case "A_3": x = "#phases #cust-A_4"; break;
					case "A_4": x = "#phases #cust-A_5"; break;
					case "A_5": x = "#phases #cust-A_6"; break;
					case "A_6": x = "#phases #cust-A_7"; break;
					case "A_7": x = "#phases #cust-A_8"; break;
					case "A_8": x = "#phases #cust-A_9"; break;
					case "A_9": x = ""; break;
					case "A_0": x = "#phases #cust-A_1"; break;
				}
				if(x.length>0) { $(x).attr("disabled",false); }
			}
		});
		$("#phases input:text.new_custom").keyup(function() {
			v = $(this).val();
			vi = $(this).attr("id");
			i = vi.substring(4,vi.length);
			x = "#phases #use-"+i;
			if(v.length>0) {
				$(x).attr("disabled",false);
			} else {
				$(x).attr("disabled",true);
			}
		});
		$("#phases .perc").blur(function() {
			total = 0;
			$("#phases .perc").each(function() {
				perc = $(this).val();
				if(perc.length>0) {
					if(isNaN(parseInt(perc))) {
						$(this).val("0");
						alert("Invalid number");
						perc = 0;
					} else {
						perc = perc*1;
					}
					total = total + perc;
				} else {
					$(this).val("0");
				}
			});
			if(total>100) {
				$(this).val("0");
				alert("Invliad percentage.\n\nTotal percentage cannot exceed 100%.");
			} else {
				$("#total-perc").attr("innerText",total);
			}
		});
		$("#phases #proc_cate").change(function() {
			d = $(this).val();
			switch(d) {
				case "2": days = days2; break;
				case "3": days = days3; break;
				case "4": days = days4; break;
				case "1": default: days = days1; break;
			}
			for(x in proc_array) {
				px = proc_array[x];
				dx = days[x];
				$("#phases #days-"+px).val(dx);
			}
			$("#phases #use-P_1").trigger("change");
		});
		$(".idelete").click(function() { document.location.href = '<?php echo $base[0]."_".$base[1].".php"; ?>'; });
	/* SETUP FORM */
	$("#details #firststart").change(function() { $("#phases #use-S_1").trigger("change"); });
	$("#phases .custom").each(function() {
		i = $(this).attr("id");
		v = $("#phases #cust-"+i).val();
		if(v=="X") {	
			customKillMe(i);
		}
	});
	$("#phases #use-S_1").trigger("change");
	
	function displayDate(my_date) {
		my_str = ""+my_date.getFullYear()+"-";
		m = my_date.getMonth();
		m = parseInt(m)+1;
		if(m<10) 
			my_str=my_str+"0";
		my_str=my_str+""+m+"-";
		if(my_date.getDate()<10) 
			my_str=my_str+"0";
		my_str=my_str+""+my_date.getDate();
		return my_str;
	}
	function createDate(str_date) {
		nDate = new Date();
		nDate.setFullYear(ds.substring(0,4));
		nDate.setMonth(ds.substr(5,2)-1);
		nDate.setDate(ds.substr(8,2));
		return nDate;
	}
	function customKillMe(i) {
			x = "#phases #span-add-"+i;
			$(x).hide();
			for(pf in phase_fields) {
				if(phase_fields[pf]=="perc") {
					x = "#phases #lbl"+phase_fields[pf]+"-"+i;
				} else {
					x = "#phases #"+phase_fields[pf]+"-"+i;
				}
				if($(x).hasClass("datepicker")) {
					$(x).datepicker("destroy");
				} 
				$(x).hide();
			}
			x = "#phases #use-"+i;
			$(x).val("N");
			if(i!="A_0") {
				x = "#phases #cust-"+i;
				$(x).attr("disabled",true);
			}
	}
	function useHideShow(i,act) {
		for(pf in phase_fields) {
			if(phase_fields[pf]!="use") {
				if(phase_fields[pf]=="perc") {
					x = "#phases #lbl"+phase_fields[pf]+"-"+i;
				} else {
					x = "#phases #"+phase_fields[pf]+"-"+i;
				}
				if(act=="hide") {
					if($(x).hasClass("datepicker")) {
						$(x).datepicker("destroy");
					} 
					$(x).hide();
					if(phase_fields[pf]=="end") {
						x = "#phases #lbl"+phase_fields[pf]+"-"+i;
						$(x).hide();
					}
				} else {
					$(x).show();
					if($(x).hasClass("datepicker")) {
						$(x).trigger("datepickercreate");
						if(phase_fields[pf]=="finidate") {
							$(x).datepicker("option","maxDate",'0d');
						}
					}
					if(phase_fields[pf]=="end") {
						x = "#phases #lbl"+phase_fields[pf]+"-"+i;
						$(x).show();
					}
				}
			}
		}
		x = "fini-"+i;
		checkFini(x,$(x));
	}
	function checkFini(i,me) {
			i = i.substring(5,i.length)
			x = "#phases #finidate-"+i;
			if(me.attr("checked")==true) {
				$(x).show();
				$(x).trigger("datepickercreate");
				$(x).datepicker("option","maxDate",'0d');
			} else {
				$(x).hide();
				$(x).datepicker("destroy");
			}
	}
	
	
	
	$("#plc_form").submit(function() {
		p = 0;
		err = 0;
		perc = 0;
		for(pi in pid_array) {
			i = pid_array[pi]
			u = "#phases #use-"+i;
			if($(u).val()=="Y" || $(u).val() == "R") {
				p++;
				f = "#phases #fini-"+i;
				fd = "#phases #finidate-"+i;
				if($(f).attr("checked")==true) {
					fdv = $(fd).val();
					if(fdv.length==0) {
						$(fd).addClass("required");
						err++;
					}
				}
				pc = "#phases #perc-"+i;
				perc+=$(pc).val();
			}
		}
		if(p==0) {
			alert("Please select at least one phase to use.");
		} else if(err>0) {
			alert("Please fix the highlighted errors.\n\nNote that if you mark a phases as completed you must enter a completion date.");
		} else if(perc==0 && $("#details #plc_up").val()!="N") {
			alert("You have selected to update the KPI results from the PLC however you have not captured any percentages for the project.\n\nThe PLC cannot be submitted unless you change the Update option or add 1 or more percentages.");
		} else {
			return true;
		}
		return false;
	});
});
</script>
<p>&nbsp;</p><p>&nbsp;</p>
</body>
</html>