function goNext(url,s,f) {
    f = escape(f);
    document.location.href = url+"&s="+s+"&f="+f;
}

//Function to change the background of a table row on hover
//EFFECTS ALL TABLES
/*$(document).ready(function(){
	$("tr").hover(
		function(){ $(this).css("background-color","#EFEFEF"); },
		function(){ $(this).css("background-color","#FFFFFF"); }
	);
});*/
$(document).ready(function(){
	$("tr").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$("tr#hover2").hover(
		function(){ $(this).addClass("trhover2"); },
		function(){ $(this).removeClass("trhover2"); }
	);
	$(".no-highlight").unbind('mouseenter mouseleave');
	$(".datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '../library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'yy-mm-dd',
                    changeMonth:true,
                    changeYear:true		
                });
	$(".datepicker").bind("datepickercreate",function() {
		$(this).datepicker({
                    showOn: 'both',
                    buttonImage: '../library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'yy-mm-dd',
                    changeMonth:true,
                    changeYear:true		
                });
	});
});

function validateActual(v) {
	valid = true;
	decimal = 0;
	if(v.length>0) {
		if(!isNaN(parseFloat(v))) {
			for(var i=0; i<v.length; i++) {
				if(isNaN(v.substring(i,i+1))) {
					if(v.substring(i,i+1)==".") {
						if(decimal>0) {
							valid = false;
							break;
						} else {
							decimal++;
						}
					} else {
						valid = false;
						break;
					}
				}
			}
		} else {
			valid = false;
		}
	} else {
		valid = false;
	}
	return valid;
}

function validatePerf(required,minsize,v) {
	vali8 = false;
	if(!required) {
		vali8 = true;
	} else {
		vali8 = validateMinSize(minsize,v);
	}
	return vali8;
}

function validateMinSize(minsize,v) {
	val8 = false;
			var len = v.length;
			if(len>=minsize) {
				val8 = true;
			}
			//alert("vMS "+valid8+" len: "+len);
	return val8;
}

function validateCorrect(required,minsize,v,t,a,ct) {
	va8 = true;
	//alert(required);
	if(!required) {
		va8 = true;
	} else {
			a = parseFloat(a);
			t = parseFloat(t);
		if(ct=="ZERO" || ct=="REV") {
			if(a>t) {
				va8 = validateMinSize(minsize,v);
			}
		} else {
			if(a<t) {
				va8 = validateMinSize(minsize,v);
			}
		}
	}
	//alert("vC "+va8+" ms: "+minsize+" v: '"+v+"' ct:"+ct+" t: "+t+" a: "+a+" a<t: "+(a<t));
	return va8;
}

function validateMe(fld,required,minsize,v,t,a,ct) {
	var v8 = true;
	var valid_act = true;
	//alert(fld+" :: "+v+" :: "+a);
	switch(fld) {
	case "kr_perf": case "kr_perf[]":
	case "tr_perf": case "tr_perf[]":
		if(valid_act) {
			v8 = validatePerf(required,minsize,v);
		}
		break;
	case "kr_correct": case "kr_correct[]":
	case "tr_correct": case "tr_correct[]":
		//alert(required);
		if(valid_act) {
			v8 = validateCorrect(required,minsize,v,t,a,ct);
		}
		//alert("vMe "+fld+" "+v8);
		break;
	case "kr_actual": case "kr_actual[]":
	case "tr_actual": case "tr_actual[]":
		v8 = validateActual(v);
		valid_act = v8;
		break;
	}
	//alert("vMe "+fld+" "+valid8);
	return v8;
}

function stripslashes (str) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +      fixed by: Mick@el
    // +   improved by: marrtins    // +   bugfixed by: Onno Marsman
    // +   improved by: rezna
    // +   input by: Rick Waldron
    // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +   input by: Brant Messenger (http://www.brantmessenger.com/)    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: stripslashes('Kevin\'s code');
    // *     returns 1: "Kevin's code"
    // *     example 2: stripslashes('Kevin\\\'s code');
    // *     returns 2: "Kevin\'s code"    
	return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
        case '\\':
            return '\\';
        case '0':            return '\u0000';
        case '':
            return '';
        default:
            return n1;        
		}
    });
}