<?php 

if(isset($_SESSION['ia_cmp_reseller']) && $_SESSION['ia_cmp_reseller']=="IGN0001") { $has_pmsps = true; } else { $has_pmsps = false; }

$select_option_maxlen = 90;
$id_labels_all = array(
	'KPI'=>"D",
	'TOP'=>"TL",
	'CAP'=>"CP",
	'CF'=>"CF",
	'RS'=>"RS"
);
if(isset($section)) {
	$id_label = $id_labels_all[$section];
	switch($section) {
		case "KPI":
			$table_fld = "kpi_";
			$table_tbl = "kpi";
			$table_id = "kpi";
			$get_headings = isset($get_headings) ? $get_headings : array("KPI","KPI_R");
			$detail_link = "kpi_value";
			$get_time = isset($get_time) ? $get_time : array(1,2,3,4,5,6,7,8,9,10,11,12);
			$r_section = "KPI_R";
			$r_table_fld = "kr_";
			$section_head = "Departmental SDBIP";
			$fld_target = "kr_target";
			$fld_actual = "kr_actual";
			$wa_tbl_fld = array('wards'=>"kw",'area'=>"ka","kpi_wards"=>"kw","kpi_area"=>"ka");
			$first_month = 1;
			$time_period_size = 1;
			break;
		case "TOP":
			$table_fld = "top_";
			$table_tbl = "top";
			$table_id = "top";
			$get_headings = array("TOP","TOP_R");
			$get_time = !isset($get_time) ? array(3,6,9,12) : $get_time;
			$detail_link = "top_value";
			$r_section = "TOP_R";
			$r_table_fld = "tr_";
			$section_head = "Top Layer";
			$fld_target = "tr_target";
			$fld_actual = "tr_actual";
			$wa_tbl_fld = array('wards'=>"tw",'area'=>"ta",'top_wards'=>"tw",'top_area'=>"ta");
			$first_month = 3;
			$time_period_size = 3;
			$dept_flds = array("tr_dept","tr_dept_correct");
			break;
		case "CAP":
			$table_fld = "cap_";
			$table_tbl = "capital";
			$table_id = "cap";
			$get_headings = array("CAP","CAP_H","CAP_R");
			$detail_link = "cap_name";
			$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
			$r_section = "CAP_R";
			$h_section = "CAP_H";
			$r_table_fld = "cr_";
			$section_head = "Capital Projects";
			$fld_target = "cr_target";
			$fld_actual = "cr_actual";
			$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs",'capital_wards'=>"cw",'capital_area'=>"ca",'capital_fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			break;
		case "CF":
			$table_fld = "cf_";
			$table_tbl = "cashflow";
			$table_id = "cf";
			$get_headings = array("CF","CF_H","CF_R");
			$detail_link = "cf_value";
			$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
			$r_section = "CF_R";
			$h_section = "CF_H";
			$r_table_fld = "cr_";
			$section_head = "Monthly Cashflow";
			$fld_target = "4";
			$fld_actual = "5";
			//$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			break;
		case "RS":
			$table_fld = "rs_";
			$table_tbl = "revbysrc";
			$table_id = "rs";
			$get_headings = array("RS","RS_R");
			$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
			$detail_link = "rs_value";
			$r_section = "RS_R";
			$r_table_fld = "rr_";
			$section_head = "Revenue By Source";
			$fld_target = "rr_budget";
			$fld_actual = "rr_actual";
			//$wa_tbl_fld = array('wards'=>"cw",'area'=>"ca",'fundsource'=>"cs");
			$first_month = 1;
			$time_period_size = 1;
			break;
	}
}

$user_access_fields = array(
	'module'=>"Module<br />Admin",
	'kpi'=>"KPI<br />Admin",
	'finance'=>"Finance<br />Admin",
	'toplayer'=>"Top Layer<br />Admin",
	'assurance'=>"Assurance<br />Provider",
	'second'=>"Secondary<br />Time Period",
	'setup'=>"Setup",
);



$page_src = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['PHP_SELF'];
$self = $_SERVER['PHP_SELF'];
$page = explode("/",$self);
$self = $page[count($page)-1];
$page = substr($self,0,-4);
$base = explode("_",$page);
if(!isset($src)) {
	$src = isset($base[1]) ? $base[1] : "";
}
$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "";



require_once("inc_ignite.php"); 
require_once("inc.php");
require_once("inc_draw.php");

//GET USER DETAILS
logUser();
$my_access = getUserAccess($tkid);

$all_time = getTime(array(1,12),false);
$today = $_SERVER['REQUEST_TIME'];
if($today>=strtotime($all_time[12]['end_date'])) { $current_time_id = 12; } else { $current_time_id = (date("m")>6 ? date("m")-6 : date("m")+6 ); }
$get_open_time = !isset($get_open_time) ? false : $get_open_time;
$get_lists = !isset($get_lists) ? false : $get_lists;
$result = array();

//include("demo.php");

//arrPrint($mheadings);
function breadCrumb($bc) {
	global $self;
	if($self!=$bc)
		return "<a href=".$bc." class=breadcrumb>";
	else
		return false;
}


require_once("inc_module.php");

//arrPrint($my_access);
//IF THE PAGE IS NOT A REPORT THEN DISPLAY THE BODY PORTION
if(!isset($onscreen_header) && !(($base[0]=="report" || $page_id=="change") && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE")) {

if(!isset($locked_column) || !$locked_column) {
	?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
	<?php 
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>

	<?php if(isset($amcharts) && $amcharts=="java") { ?>
		<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
		<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
	<?php } //else { ?>
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery.min.js"></script>
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui.min.js"></script>
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui-timepicker-addon.js"></script>
		<?php if(isset($jquery_date) && $jquery_date==true && $abc == "DEF") { ?>
			<link href="/library/jquery/css/jquery-ui-date.css" rel="stylesheet" type="text/css"/>
		<?php } else { ?>
			<link href="/library/jquery-ui-1.8.24/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<?php } ?>
	<?php //} ?>
	<link rel="stylesheet" href="/assist.css" type="text/css">
	<link rel="stylesheet" href="inc/main.css" type="text/css">
	<script type ="text/javascript" src="inc/main.js"></script>
</head>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
//$result = $_REQUEST['r'];
//displayResult($result);

/** GENERATE NAV BUTTONS & PAGE TITLE **/

/* 2ND TIER MENU / 1ST TIER NAV BUTTONS */
$menu = array(
	'base_pages' => array(
						'main' 	=> $_SESSION['modtext'],
						'setup' => "Setup"
	),
	'setup' => array(
						'defaults' 	 => "Defaults",
						'useraccess' => "User Access"
	),
	'manage' => array(
						'dept' 	 	=> "Departmental SDBIP",
						'top'		=> "Top Layer SDBIP",
						'fin'		=> "Financials",
						'admin'		=> "KPI Admin",
						'topadmin'	=> "Top Layer Admin",
						//'profile'	=> "My Profile",
	//					'assurance'	=> "--Assurance"
	),
	'view' => array(
						'dept' 	 	=> "Departmental SDBIP",
						'top'		=> "Top Layer SDBIP",
						'capital'	=> "Capital Projects",
						'cashflow'	=> "Monthly Cashflow",
						'revbysrc'	=> "Revenue By Source",
						'dash'		=> "YTD Dashboard",
	),
	'report' => array(
						'dept' 	 	=> "Departmental SDBIP",
						'top'		=> "Top Layer SDBIP",
						'capital'	=> "Capital Projects",
						'cashflow'	=> "Monthly Cashflow",
						'revbysrc'	=> "Revenue By Source",
						'graph'		=> "Graphs",
	),
	'support'	=>	array(
						'fin'		=>	"Financials",
						'dkpi'		=>	"Deleted Departmental KPIs",
	),
);
$redirect = array(
	'setup' 		=> "setup_defaults.php",
	'manage' 		=> "manage_dept.php",
	'view' 		=> "view_dept.php",
	'report'		=> "report_dept.php",
	'support_fin'		=> "support.php",
	'main'			=> "view_dept.php",
);


if(isset($redirect[$page])) {
	echo "<script type=text/javascript>document.location.href = '".$redirect[$page]."';</script>";
}

//arrPrint($my_access);

if(!isset($_REQUEST['nav']) || $_REQUEST['nav']) {

if(isset($menu[$base[0]]) && $base[0]!="support") {
	$submenu = $menu[$base[0]];
	$nav = array();
	foreach($submenu as $id => $display) {
		$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$id.".php",'active'=>(($src==$id) ? "Y" : "N"),'display'=>$display);
	}
	echoNavigation(1,$nav);
}

/* 3RD TIER MENU / 2ND TIER NAV BUTTONS */
switch($base[0]) {
	case "manage":
		$m = array("profile","admin","assurance","fin","top");
		switch($base[1]) {
		case "topadmin":
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "edit";
			$menu2 = array(
				'edit' => "Edit",
				'create' => "Create",
				'auto' => "Auto Update",
				'change' => "Changelog Report",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			if(!isset($page_title)) {
				$page_title = $menu2[$page_id];
			}
			break;
		case "top":
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "update";
			$menu2 = array(
				'update' => "Update",
				'edit' => "Edit",
				//'approve' => "--Approve",
				'create' => "Create",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			if(!isset($page_title)) {
				$page_title = $menu2[$page_id];
			}
			echo chr(10)."<script>$(function() { ";
				if($my_access['act']['TOP']['create']==0) {
					echo chr(10)."	$(\"#create\").button(\"option\",\"disabled\",true); ";
				}
				if($my_access['act']['TOP']['edit']==0) {
					echo chr(10)."	$(\"#edit\").button(\"option\",\"disabled\",true); ";
				}
			echo chr(10)."});</script>";
			break;
		case "admin":
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "deptadmin";
if($has_pmsps===true) {
			$menu2 = array(
				'deptadmin' 	 	=> "Edit",
				'deptcreate'		=> "Create",
				'pms'				=> "PMS Report",
				'plc'				=> "Project Life Cycle",
				'change' 			=> "Changelog Report",
			);
} else {
			$menu2 = array(
				'deptadmin' 	 	=> "Edit",
				'deptcreate'		=> "Create",
				'plc'				=> "Project Life Cycle",
				'change' 			=> "Changelog Report",
			);
}
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
			break;
		case "fin":
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "capital";
			$menu2 = array(
				'capital' 	 	=> "Capital Projects",
				'cashflow'		=> "Monthly Cashflows",
				'revbysrc'	 	=> "Revenue By Source",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
			break;
		default:
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "update";
if($has_pmsps===true) {
			$menu2 = array(
				'update' => "Update",
				'edit' => "Edit",
				//'approve' => "--Approve",
				'create' => "Create",
				'pms' => "PMS Report",
			);
} else {
			$menu2 = array(
				'update' => "Update",
				'edit' => "Edit",
				//'approve' => "--Approve",
				'create' => "Create",
			);
}
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			if(!isset($page_title)) {
				$page_title = $menu2[$page_id];
			}
			echo chr(10)."<script>$(function() { ";
				if($my_access['act']['KPI']['create']==0) {
					echo chr(10)."	$(\"#create\").button(\"option\",\"disabled\",true); ";
				}
				if($my_access['act']['KPI']['edit']==0) {
					echo chr(10)."	$(\"#edit\").button(\"option\",\"disabled\",true); ";
				}
			echo chr(10)."});</script>";
			break;
		}
		/*if(!in_array($base[1],$m)) {
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "update";
			$menu2 = array(
				'update' => "Update",
				'edit' => "Edit",
				//'approve' => "--Approve",
				'create' => "Create",
				'pms' => "PMS Report",
			);
			if($base[1] == "top") { 
				unset($menu2['pms']); 
				//$menu2['audit'] = "Audit Log"; 
				$menu2['auto'] = "Auto Update";
				$menu2['change'] = "Changelog Report";
			}
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			if(!isset($page_title)) {
				$page_title = $menu2[$page_id];
			}
			echo chr(10)."<script>$(function() { ";
			//echo chr(10)."	$(\"#approve\").button(\"option\",\"disabled\",true); ";
			if($base[1]=="dept") {
				if($my_access['act']['create']==0) {
					echo chr(10)."	$(\"#create\").button(\"option\",\"disabled\",true); ";
				}
				if($my_access['act']['edit']==0) {
					echo chr(10)."	$(\"#edit\").button(\"option\",\"disabled\",true); ";
				}
			}
			echo chr(10)."});</script>";
		} elseif($base[1]=="admin") {
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "deptadmin";
			$menu2 = array(
				'deptadmin' 	 	=> "Edit",
				'deptcreate'		=> "Create",
				'pms'				=> "PMS Report",
				'plc'				=> "Project Life Cycle",
				'change' 			=> "Changelog Report",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
			echo chr(10)."<script>$(function() { ";
			//echo chr(10)."	$(\"#other\").button(\"option\",\"disabled\",true); ";
			echo chr(10)."});</script>";
		} elseif($base[1]=="fin") {
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "capital";
			$menu2 = array(
				'capital' 	 	=> "Capital Projects",
				'cashflow'		=> "Monthly Cashflows",
				'revbysrc'	 	=> "Revenue By Source",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
			echo chr(10)."<script>$(function() { ";
			//echo chr(10)."	$(\"#other\").button(\"option\",\"disabled\",true); ";
			echo chr(10)."});</script>";
		}*/
		echo chr(10)."<script>$(function() { ";
		if(!(count($my_access['manage']['TOP'])>0 || count($my_access['manage']['T_OWN'])>0)) {
			echo chr(10)."	$(\"#top\").button(\"option\",\"disabled\",true); ";
		}
		if(!$my_access['access']['toplayer']) {
			echo chr(10)."	$(\"#topadmin\").button(\"option\",\"disabled\",true); ";
		}
		if(!$my_access['access']['kpi']) {
			echo "	$(\"#admin\").button(\"option\",\"disabled\",true); ";
		}
		//if(!$my_access['access']['assurance']) {
			//echo chr(10)."	$(\"#assurance\").button(\"option\",\"disabled\",true); ";
		//}
		if(!$my_access['access']['finance']) {
			echo chr(10)."	$(\"#fin\").button(\"option\",\"disabled\",true); ";
		}
		//echo "	$(\"#profile\").button(\"option\",\"disabled\",true); ";
		echo chr(10)."});</script>";
		break;
	case "view":
/*		if(isset($base[1]) && $base[1]=="dash") {
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "deptytd";
			$menu2 = array(
						'deptytd' 	 	=> "Departmental SDBIP",
						'topytd'		=> "Top Layer SDBIP",
						'capitalytd'	=> "Capital Projects",
						'cashflowytd'	=> "Monthly Cashflow",
						'revbysrcytd'	=> "Revenue By Source",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$self."?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
			/*echo "<script>$(function() { 
				$(\"#capitalytd\").button(\"option\",\"disabled\",true); 
				$(\"#revbysrcytd\").button(\"option\",\"disabled\",true); 
			});</script>";*/
	//	}
		echo "<script>$(function() { 
			//$(\"#dash\").button(\"option\",\"disabled\",true); 
		});</script>";
		break;
	case "report":
		if($base[1]!="graph") {
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "generate";
			$menu2 = array(
				'generate' => "Generator",
				'quick' => "Quick Reports",
//				'fixed' => "Fixed Reports",
			);
		} else {
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "graph_dept";
			$menu2 = array(
				'graph_dept' => "Departmental SDBIP",
				'graph_top' => "Top Layer SDBIP",
				'graph_cashflow' => "Monthly Cashflow",
			);
		}
		$nav = array();
		foreach($menu2 as $id => $display) {
			$nav[$id] = array('id'=>$id,'url'=>$self."?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
		}
		echoNavigation(2,$nav);
		$page_title = $menu2[$page_id];
		echo "<script>$(function() { 
			$(\"#quick\").button(\"option\",\"disabled\",true); ";
//			$(\"#graph\").button(\"option\",\"disabled\",true); 
		echo "});</script>";
		
		break;
	case "setup":
		if($base[1]=="defaults") {
			if(isset($base[2]) && $base[2] == "head") {
				$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "term";
				$menu2 = array(
							'term'	=> "Terminology",
							'dept' 	=> "Departmental SDBIP",
							'top'	=> "Top Layer SDBIP",
							'cap'	=> "Capital Projects",
							'cf'	=> "Monthly Cashflow",
							'rs'	=> "Revenue By Source",
				);
				$nav = array();
				foreach($menu2 as $id => $display) {
					$nav[$id] = array('id'=>$id,'url'=>$self."?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
				}
				echoNavigation(2,$nav);
				$page_title = (isset($page_title) ? $page_title." >> " : "").$menu2[$page_id];
			} elseif(isset($base[2]) && $base[2] == "admins") {
				$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "dir";
				$page_type = isset($page_type) ? $page_type : "DEPT";
				if($page_type=="TOP") {
					$menu2 = array(
								'dir'		=> $head_dir,
								'owner'		=> strlen($mheadings['TOP']['top_ownerid']['h_client'])>0 ? $mheadings['TOP']['top_ownerid']['h_client'] : $mheadings['TOP']['top_ownerid']['h_ignite'],
								'report'	=> "Report",
					);
				} else {
					$menu2 = array(
								'dir'		=> $head_dir,
								'subdir' 	=> $head_sub,
								'owner'		=> strlen($mheadings['KPI']['kpi_ownerid']['h_client'])>0 ? $mheadings['KPI']['kpi_ownerid']['h_client'] : $mheadings['KPI']['kpi_ownerid']['h_ignite'],
								'report'	=> "Report",
					);
				}
				$nav = array();
				foreach($menu2 as $id => $display) {
					$nav[$id] = array('id'=>$id,'url'=>$self."?t=".$page_type."&page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
				}
				echoNavigation(2,$nav);
				$page_title = (isset($page_title) ? $page_title." >> " : "").$menu2[$page_id];
			} elseif(isset($base[2]) && $base[2] == "dir") {
				$page_title = $head_dir;
			}
		}
		break;
	case "support":	
		break;
}
}	//request_nav = true
/* PAGE TITLE */
echo "<h1>".breadCrumb($base[0].".php").(isset($menu['base_pages'][$base[0]]) ? $menu['base_pages'][$base[0]] : ucfirst($base[0]));
if(isset($base[1])) {
	$breadcrumb = $base[0]."_".$base[1].".php";
	echo "</a> >> ".breadCrumb($breadcrumb).(isset($menu[$base[0]][$base[1]]) ? $menu[$base[0]][$base[1]] : ucfirst($base[1]));
}
if(isset($page_title)) {
	echo "</a> >> ".$page_title;
}
echo "</a></h1>";

//arrPrint($my_access);
}
if(isset($section)) {
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",
		"dir",$table_fld."subid",
		$table_fld."annual",
		$table_fld."revised",
		$table_tbl."_wards",
		$table_tbl."_area",
		$table_tbl."_fundsource",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
		$table_fld."pmsref",
		"cap_cpref", "cap_name","cap_descrip","cap_planstart","cap_planend","cap_actualstart","cap_actualend",
		$table_fld."vote",
	);
}



?>