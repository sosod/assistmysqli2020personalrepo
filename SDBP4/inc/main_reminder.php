<?php
//error_reporting(-1);
$modref = "sdp11";
$dbref = "assist_".$cmpcode."_".$modref;
//include("SDBP3/lib/inc_SDBP3.php");
//$module_exists = getModuleRefExistance($modref,'list_time');
$module_exists = false;
$checksql = "SELECT * FROM assist_menu_modules WHERE modref = '".strtoupper($modref)."'";
$rs = getRS($checksql);
if(mysql_num_rows($rs)>0) { $module_exists = true; }
//echo $dbref;

if($module_exists) {
	include("SDBP4/inc/inc.php");
//	include("SDBP4/inc/inc_cron.php");
	$user_access = getUserAccess($tkid);
	//arrPrint($user_access);
	
	$manage = 0;
	if(isset($user_access['manage']['DIR'])) { $manage+=count($user_access['manage']['DIR']); }
	if(isset($user_access['manage']['SUB'])) { $manage+=count($user_access['manage']['SUB']); }
	if(isset($user_access['manage']['OWN'])) { $manage+=count($user_access['manage']['OWN']); }
	
	if($manage>0 || $user_access['access']['toplayer'] || $user_access['access']['finance']) {
		$sql = "SELECT * FROM ".$dbref."_list_time 
				WHERE active = true
				AND start_date < '".date("Y-m-d H:i:s")."'
				AND (active_primary = true OR active_secondary = true OR active_finance = true)
				AND (close_primary > 0 OR close_secondary > 0 OR close_finance > 0)";
		$time = mysql_fetch_alls($sql,"id");
		$week = $today + (86400 * 7);
		//arrPrint($time);
		$reminders = array('primary'=>false,'secondary'=>false,'toplayer'=>false,'finance'=>false);
		if($manage>0 && !$user_access['access']['second']) {
			$reminders['primary'] = true;
			$reminders['secondary'] = false;
		} elseif($manage>0 && $user_access['access']['second']) {
			$reminders['primary'] = false;
			$reminders['secondary'] = true;
		}
		if($user_access['access']['toplayer']) {
			$reminders['toplayer'] = true;
		}
		if($user_access['access']['finance']) {
			$reminders['finance'] = true;
		}
		$closures = array();
		foreach($time as $ti => $t) {
			$fld = "primary"; $section = "Primary";
			if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week) {
				$closures[]= "Departmental SDBIP - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld]).".";
			}
			$fld = "secondary"; $section = "Secondary";
			if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week) {
				$closures[]= "Departmental SDBIP - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld])."."
							.($t['close_primary']>0 ? "<br /><i><small>&nbsp;&nbsp;&nbsp;(Following primary closure on ".date("d M Y",$t['close_primary']).".)</small></i>" : "");
			}
if(ceil($ti/3)==$ti/3) {
			$fld = "secondary"; $section = "Top Layer";
			if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week) {
				$closures[]= "Top Layer SDBIP - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld]).".";
			}
}
			$fld = "finance"; $section = "Finance";
			if($reminders[$fld] && $t['active_'.$fld] && $t['close_'.$fld]>($today-86400) && $t['close_'.$fld]<=$week) {
				$closures[]= "Financials - ".date("F Y",strtotime($t['end_date']))." will close on ".date("d M Y H:i",$t['close_'.$fld]).". ";
			}
		}

		if(count($closures)>0) {
			?>
			<div align=center><table style="border: 2px solid #cc0001;"><tr><td style="padding: 20 20 20 20;">
			<h2 style="margin-top: 0px; color: #cc0001;">SDBIP 2011/2012 Closure Reminder</h2>
			<p>Please note the following forthcoming closure dates:<ul><li>
			<?php
				echo implode("</li><li>",$closures);
			?>
			</li></ul>
			<p style="font-style: italic">Please ensure that you have completed all necessary updates.</p>
			</td></tr></table>
			</div>
			<?php
		}
	}
	
}
?>