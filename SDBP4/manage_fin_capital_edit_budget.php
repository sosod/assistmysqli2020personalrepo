<?php 
$time_all = getTime($get_time,false);

	$total_changes = 0;
	$log_headings = array();
	//GET REQUEST DATA
	if(!isset($_REQUEST['t'])) { die(displayResult(array("error","An error occurred while trying to determine the time period."))); }
	$time_id = $_REQUEST['t'];
	if($_REQUEST['act']=="REVIEW") {
		echo JSdisplayResultPrep("Processing...");
		echo "<script type=text/javascript>
			JSdisplayResult('info','info','Processing...');
		</script>";
		/*echo "<p>Please note: 
			<br />1. This function only updates the ACTUALS and does not affect the budget figures.
			<br />2. Any changes will be highlighted in green.  Any errors will be highlighted in red.</p>";*/
	} else {
		echo JSdisplayResultPrep("Processing...");
		echo "<script type=text/javascript>
			JSdisplayResult('info','info','Processing...');
		</script>";
	}
	echo "<form id=up method=post action=".$self.">
			<input type=hidden name=t value=".$time_id." />
			<input type=hidden name=tab value=".$tab." />
			<input type=hidden name=act value=SAVE />";
	//CHECK FOR IMPORT FILE
		$err = false;
		$file_location = $modref."/import/".$section."/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$file_location;
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = date("Ymd_Hi",$today)."_".substr($_FILES['ifile']['name'],0,-4).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
				}
			}
		}
		//READ FILE
		if(!$err) {
			echo "<input type=hidden name=f value=\"".(isset($filen) ? $filen : $_REQUEST['f'])."\" />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);
		//GET DATA
			if(count($data)>2) {
			//arrPrint($data);
				unset($data[0]); unset($data[1]);
				$new = array();
				$object_sql[0] = "SELECT subdir.value as list_cap_subid, dir.value as list_0, gfs.value as list_cap_gfsid, c.* ";
				$object_sql[1] = "FROM ".$dbref."_capital c
				INNER JOIN ".$dbref."_subdir subdir ON cap_subid = subdir.id AND subdir.active = true
				INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
				INNER JOIN ".$dbref."_list_gfs gfs ON cap_gfsid = gfs.id 
				WHERE cap_active = true ";
				$objects = mysql_fetch_alls(implode($object_sql),"cap_id");

				$results_sql = "SELECT r.* FROM ".$dbref."_capital_results r
				INNER JOIN ".$dbref."_capital c ON r.cr_capid = c.cap_id AND c.cap_active = true";
				
				$results = mysql_fetch_alls2($results_sql,"cr_capid","cr_timeid");
				
				echo "<table class=noborder><tr class=no-highlight><td class=noborder><table>
						<thead><tr>
							<th rowspan=2>Ref</th>
							<th rowspan=2>".$mheadings['dir'][0]['h_client']."</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'subid']['h_client']."</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'vote']['h_client']."</th>
							<th rowspan=2>".$mheadings[$section][$table_fld.'name']['h_client']."</th>";
						echo "<th style=\"border-left: 2px solid #ffffff;\" colspan=".count($mheadings[$r_section]).">Original ".$mheadings[$h_section]['cr_']['h_client']."</th>";
					foreach($mheadings[$h_section] as $hfld => $h2) {
						echo "<th style=\"border-left: 2px solid #ffffff;\" colspan=".count($mheadings[$r_section]).">".$h2['h_client']."</th>";
					}
					echo "<th rowspan=2></th>";
					echo "</tr><tr>";
						foreach($mheadings[$r_section] as $fld => $h) {
							if($fld == "target") { $style = "border-left: 2px solid #ffffff;"; } else { $style = ""; }
							echo "<th style=\"$style\">".$h['h_client']."</th>";
						}
					foreach($mheadings[$h_section] as $hfld => $h2) {
						foreach($mheadings[$r_section] as $fld => $h) {
							if($fld == "target") { $style = "border-left: 2px solid #ffffff;"; } else { $style = ""; }
							echo "<th style=\"$style\">".$h['h_client']."</th>";
							if(!isset($log_headings[$hfld.$fld])) { $log_headings[$hfld.$fld] = substr($h2['h_client'],0,strpos($h2['h_client']," "))." ".$h['h_client']; }
						}
					}
					echo "</tr></thead>
						<tbody>";
				foreach($data as $d => $di) { set_time_limit(1800);
					$err = false;
					if(strlen($di[0])>0) {
						$obj_id = substr($di[0],strlen($id_labels_all[$section]),strlen($di[0]));
						if(!isset($objects[$obj_id])) { $err = true; } else {	$obj = $objects[$obj_id];  $res = $results[$obj_id]; $new[$obj_id] = array(); }
						echo "<tr>
							<th>".$id_labels_all[$section].$obj_id."</th>";
							if(!$err) {
								echo "<td>".$obj['list_0']."</td>";
								echo "<td>".$obj['list_'.$table_fld.'subid']."</td>";
								echo "<td>".$obj[$table_fld.'vote']."</td>";
								echo "<td>".$obj[$table_fld.'name']."</td>";
								foreach($mheadings[$r_section] as $fld => $h) {
									if($fld == "target") { $style = "border-left: 2px solid #000099;"; } else { $style = ""; }
									echo "<td class=right style=\"font-style: italic; background-color: #eeeeee; $style \">".number_format($res[$time_id]['cr_'.$fld],2)."</td>";
								}
								foreach($mheadings[$h_section] as $hfld => $h2) {
								$values = array();
								if($hfld=="cr_") { $change = false; }
								$new_actual = 0;
								foreach($mheadings[$r_section] as $fld => $h) {
									if($fld == "target") { $style = "border-left: 2px solid #000099;"; } else { $style = ""; }
									switch($hfld) {
									case "cr_":
											$val = 0; $class = ""; 
											switch($fld) {
											case "target":
												$val = $di[10];
												if(!is_numeric($val)) {
													$change = false;
													$err = true;
													$class = "result1";
												} else {
													$new_actual = $val;
													if($val!=$res[$time_id][$hfld.$fld]) { 
														$class = "result3"; $change = true; 
													}
													$new[$obj_id][$hfld.$fld] = $val;
													$values[$fld] = $val;
												}
												/*$val = $di[17];
												if($val!=$res[$time_id][$hfld.$fld]) { 
													$class = "result3"; $change = true; 
												}
												$new[$obj_id][$hfld.$fld] = $val;*//*
												$val = $res[$time_id][$hfld.$fld];
												$values[$fld] = $val;
												*/break;
											case "actual":
												$val = $res[$time_id][$hfld.$fld];
												$values[$fld] = $val;
/*												$val = $di[11];
												if(!is_numeric($val)) {
													$change = false;
													$err = true;
													$class = "result1";
												} else {
													$new_actual = $val;
													if($val!=$res[$time_id][$hfld.$fld]) { 
														$class = "result3"; $change = true; 
													}
													$new[$obj_id][$hfld.$fld] = $val;
													$values[$fld] = $val;
												}*/
												break;
											case "perc":
												if($change) {
													$val = calcChange($fld,$values['target'],$values['actual']);
													$class = "time3";
													$new[$obj_id][$hfld.$fld] = $val;
												} else {
													$val = $res[$time_id][$hfld.$fld];
												}
												break;
											case "var":		
												if($change) {
													$val = calcChange($fld,$values['target'],$values['actual']);
													$class = "time3";
													$new[$obj_id][$hfld.$fld] = $val;
												} else {
													$val = $res[$time_id][$hfld.$fld];
												}
												break;
											}
											$new[$obj_id]['change'] = $change;
										//}
										break;
									case "cr_ytd_":
										//foreach($mheadings[$r_section] as $fld => $h) {
											if($change) {
												$val = 0; $class = "";
												switch($fld) {
												case "target":
													/*for($i=1;$i<$time_id;$i++) {
														$val+=$res[$i][$fld_target];
													}
													$val+=$new[$obj_id][$fld_target];*/
													for($i=1;$i<$time_id;$i++) {
														$val+=$res[$i][$fld_target];
													}
													$val+=$new[$obj_id][$fld_target];
													$values[$fld] = $val;
													break;
												case "actual":
													$val = $res[$time_id][$hfld.$fld];
													$values[$fld] = $val;
													break;
												case "perc":
												case "var":
													$val = calcChange($fld,$values['target'],$values['actual']);
													break;
												}
												if($val!=$res[$time_id][$hfld.$fld]) { $class = "time3"; }
												$new[$obj_id][$hfld.$fld] = $val;
											} else {
												$val = $res[$time_id][$hfld.$fld];
											}
											//echo "<td class=\"$class right\">".number_format($val,2)."</td>";
										//}
										break;
									case "cr_tot_":
										//foreach($mheadings[$r_section] as $fld => $h) {
											if($change) {
												$val = 0; $class = "";
												switch($fld) {
												case "target":
													for($i=1;$i<$time_id;$i++) {
														$val+=$res[$i][$fld_target];
													}
													$val+=$new[$obj_id][$fld_target];
													$values[$fld] = $val;
													break;
												case "actual":
													$val = $res[$time_id][$hfld.$fld];
													$values[$fld] = $val;
													break;
												case "perc":
												case "var":
													$val = calcChange($fld,$values['target'],$values['actual']);
													break;
												}
												if($val!=$res[$time_id][$hfld.$fld]) { $class = "time3"; }
												$new[$obj_id][$hfld.$fld] = $val;
											} else {
												$val = $res[$time_id][$hfld.$fld];
											}
											//echo "<td class=\"$class right\">".number_format($val,2)."</td>";
										//}
										break;
									default:
										//foreach($mheadings[$r_section] as $fld => $h) {
											//echo "<td class=right>".$res[$time_id][$hfld.$fld]."</td>";
										//}
									}
									if($class=="result1") {
										echo "<td class=\"$class right\" style=\"$style\">Invalid number</td>";
									} else {
										echo "<td class=\"$class\" style=\"text-align: right; $style\">".number_format($val,2).($fld=="perc" ? "%" : "")."</td>";
									}
								}
								}
								$sql = "";
								if($new[$obj_id]['change']) {
									if($_REQUEST['act']=="SAVE") {
										$sql_fld = array();
										$log_changes = array();
										foreach($new[$obj_id] as $fld=>$v) {
											if($fld!="change") {
												$sql_fld[] = $fld." = ".number_format($v,4,".","");
												if($v!=$res[$time_id][$fld]) {
													$oldv=$res[$time_id][$fld];
													$log_changes[] = $log_headings[$fld]." = ".number_format($v,2).(strpos($fld,"perc")>0?"%":"")." (was ".number_format($oldv,2).(strpos($fld,"perc")>0?"%":"").")";
												}
											}
										}
										$sql = "UPDATE ".$dbref."_capital_results SET ".implode(", ",$sql_fld)." WHERE cr_capid = ".$obj_id." AND cr_timeid = ".$time_id;
										$mar = db_update($sql);
										$total_changes+=$mar;
										//echo "<td>$sql <br /> $mar </td>";
										if($mar>0) {
											//LOG UPDATE
											$v = array(
												'fld' => "",
												'timeid' => $time_id,
												'text' => addslashes("Updated financials for ".$time[$time_id]['display_full']." as follows:<br /> - ".implode("<br /> - ",$log_changes)),
												'old' => "",
												'new' => addslashes($file),
												'act' => "E",
												'YN' => "Y"
											);
											logChanges($section,$obj_id,$v,addslashes($sql));
										//}//dev
											//UPDATE YTD AND TOTAL CHANGES
												$future = array();
												$val_act = 0;
												$val_budg = 0; 
												$val_tot_budg = 0;
												for($i=1;$i<=12;$i++) { $val_tot_budg+= ( ($i==$time_id) ? $new[$obj_id][$fld_target] : $res[$i][$fld_target] ); }
												for($i=1;$i<=12;$i++) {
													$val_budg+= ( ($i==$time_id) ? $new[$obj_id][$fld_target] : $res[$i][$fld_target] );
													$val_act = $res[$i]['cr_ytd_actual'];
													if($i>$time_id) {
														$future[$i]['actual'] = $val_act;
														$future[$i]['ytd_target'] = $val_budg;
														$future[$i]['ytd_var'] = calcChange("var",$val_budg,$val_act);
														$future[$i]['ytd_perc'] = calcChange("perc",$val_budg,$val_act);
														$future[$i]['tot_target'] = $val_tot_budg;
														$future[$i]['tot_var'] = calcChange("var",$val_tot_budg,$val_act);
														$future[$i]['tot_perc'] = calcChange("perc",$val_tot_budg,$val_act);
													}
												}
												foreach($future as $i => $f) {
													$sql2 = "UPDATE ".$dbref."_capital_results 
															SET cr_ytd_target = ".number_format($f['ytd_target'],4,".","").", 
															cr_ytd_var = ".number_format($f['ytd_var'],4,".","").", 
															cr_ytd_perc = ".number_format($f['ytd_perc'],4,".","").", 
															cr_tot_target = ".number_format($f['tot_target'],4,".","").", 
															cr_tot_var = ".number_format($f['tot_var'],4,".","").", 
															cr_tot_perc = ".number_format($f['tot_perc'],4,".","")."
															WHERE cr_capid = ".$obj_id." AND cr_timeid = ".$i;
													$mar2 = db_update($sql2);
													if($mar2>0) {
														$v = array(
															'fld' => "",
															'timeid' => $i,
															'text' => addslashes("Updated YTD and Total financials for ".$time_all[$i]['display_full']." due to change in ".$time[$time_id]['display_full']."."),//:<br /> - ".implode("<br /> - ",$log_changes)),
															'old' => serialize($res[$i]),
															'new' => serialize($f),
															'act' => "E",
															'YN' => "Y"
														);
														logChanges($section,$obj_id,$v,addslashes($sql2));
													}
												}
											echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> Success</td>";
										} else {
											echo "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span> Error on update</td>";
										}
									} else {
										echo "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									}
								} elseif($err) {
									echo "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
								} else {
									echo "<td></td>";
								}
									if($_REQUEST['act']=="SAVE") {
									}
								//echo "<td>$sql</td>";
							} else {
								echo "<td colspan=21>";
									displayResult(array("error","Reference not found"));
								echo "</td>";
							}
						echo "</tr>";
					}	//strlen ref col
				}	//foreach data
				echo "</tbody>
				</table>";
				echo "<script type=text/javascript>
				$(function() { $('th').addClass('center'); });
				</script>";
				if($_REQUEST['act']=="REVIEW") {
					echo "<p>&nbsp;</p>";
					echo "<table width=100%><tr><td class=center><input type=submit value=\"Accept changes\" class=isubmit /></td></tr></table></td></tr></table>";
					echo "<script type=text/javascript>
					$(function() {
						$('#display_result').removeClass();
						$('#display_result').html('<p><span class=iinform>Please note:</span><br />1. This function only updates the BUDGET and does not affect any other fields.<br />2. Any changes will be highlighted in green.  Any errors will be highlighted in red.</p>');
					});
					</script>";
				} else {
					if($total_changes>0) {
						//LOG SINGLE CHANGE
						$v = array(
							'fld' => "",
							'timeid' => $time_id,
							'text' => addslashes("Updated financials for ".$time[$time_id]['display_full']),
							'old' => "",
							'new' => addslashes($file),
							'act' => "E",
							'YN' => "Y"
						);
						logChanges($section,0,$v,"");
						?><script type=text/javascript> 
							$(document).ready(function() {
								//JSdisplayResult(result,icon,text)	result=ok||error||info  icon=ok||error||info||...
								JSdisplayResult("ok","ok","Update complete.");
							});
						</script><?php
					} else {
						?><script type=text/javascript> 
							$(document).ready(function() {
								//JSdisplayResult(result,icon,text)	result=ok||error||info  icon=ok||error||info||...
								JSdisplayResult("info","info","Update complete.  No changes were found.");
							});
						</script><?php
					}
				}
				displayGoBack($self."?page_id=".$page_id."&tab=".$tab,"Go Back");
			} else {
				die(displayResult(array("error","No data found to import.")));
			}
		} else {
			die(displayResult(array("error","An error occurred while trying to read the file.")));
		}
?>