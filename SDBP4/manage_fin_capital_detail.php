<?php 
$section = "CAP";
$page_title = "Edit";
$get_lists = true;
$get_active_lists = true;
$jquery_date = true;
include("inc/header.php");


$obj_id = (isset($_REQUEST['id']) && checkIntRef($_REQUEST['id'])) ? $_REQUEST['id'] : die("An error occurred.  Please go back and try again.");

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
		case "FUNDSRC":
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			$fields[] = "o.".$fld." as o".$fld;
			break;
		default: $fields[] = "o.".$fld;
	}
}

$object_sql = "SELECT dir.value, ".implode(", ",$fields)."
FROM ".$dbref."_".$table_tbl." o
INNER JOIN ".$dbref."_subdir as subdir ON o.".$table_fld."subid = subdir.id
INNER JOIN ".$dbref."_dir as dir ON subdir.dirid = dir.id";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="subdir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE o.".$table_fld."id = ".$obj_id;
$object = mysql_fetch_all($object_sql);
$object = $object[0];

$result_sql = "SELECT * FROM ".$dbref."_".$table_tbl."_results WHERE ".$r_table_fld.$table_id."id = ".$obj_id." ORDER BY ".$r_table_fld."timeid";
$results = mysql_fetch_alls($result_sql,$r_table_fld."timeid");

//WARDS
$fld = $table_tbl."_wards";
$tbl = "wards";
$a = "cw";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.".$a."_listid AND b.".$a."_active = true AND b.".$a."_".$table_id."id = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql,"id");
//AREAS
$fld = $table_tbl."_area";
$tbl = "area";
$a = "ca";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.".$a."_listid AND b.".$a."_active = true AND b.".$a."_".$table_id."id = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql, "id");
//FORECAST AREAS
$fld = $table_tbl."_fundsource";
$tbl = "fundsource";
$a = "cs";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.".$a."_listid AND b.".$a."_active = true AND b.".$a."_".$table_id."id = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql, "id");



//FORECAST
$f_sql = "SELECT * FROM ".$dbref."_capital_forecast WHERE cf_capid = ".$obj_id." AND cf_active = true";
$forecast = mysql_fetch_alls($f_sql,"cf_listid");





if(isset($_REQUEST['act'])) {
if($_REQUEST['act']=="DELETE") {
	echo "<p>Processing delete request...</p>";
	$k = array();
	$sql = "SELECT kpi_id FROM ".$dbref."_kpi WHERE kpi_capitalid = ".$obj_id." AND kpi_active = true";
	$k = mysql_fetch_all($sql);
	if(count($k)>0) {
		foreach($k as $ka) {
			$ki = $ka['kpi_id'];
			$sql = "UPDATE ".$dbref."_kpi SET kpi_capitalid = 0 WHERE kpi_id = $ki";
			db_update($sql);
			$v = array(
				'fld'=>"kpi_capitalid",
				'timeid'=>0,
				'text'=>"Removed link to Capital Project ".$id_labels_all[$section].$obj_id." due to deletion of Capital Project.",
				'old'=>$obj_id,
				'new'=>0,
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges("KPI",$ki,$v,code($sql));
			$v = array(
				'fld'=>"kpi_capitalid",
				'timeid'=>$ki,
				'text'=>"Removed link to Departmental KPI ".$id_labels_all['KPI'].$ki." due to deletion of Capital Project.",
				'old'=>$obj_id,
				'new'=>0,
				'act'=>"D",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sql));
		}
	}
	$sql = "UPDATE ".$dbref."_".$table_tbl." SET cap_active = false WHERE cap_id = ".$obj_id;
	db_update($sql);
		$v = array(
			'fld'=>"cap_active",
			'timeid'=>0,
			'text'=>"Deleted Capital Project ".$id_labels_all[$section].$obj_id.".",
			'old'=>0,
			'new'=>0,
			'act'=>"D",
			'YN'=>"Y"
		);
		logChanges($section,$obj_id,$v,code($sql));
		echo "	<script type=text/javascript>
					document.location.href = '".$base[0]."_".$base[1].".php?page_id=capital&tab=edit&r[]=ok&r[]=Capital Project+".$id_labels_all[$section].$obj_id."+has+been+successfully+deleted.';
				</script>";
	
} else if($_REQUEST['act']=="SAVE") {
//	echo "SAVE ME";
//	arrPrint($_REQUEST);
	
	
	
	
	
	
	echo "<p>Processing edit...</p>";
	//echo "<table><tr><th>Fld</th><th>Type</th><th>New</th><th>Old</th><th>Action</th></tr>";
	$updates = array();
	$trans = array();
	$changes = array();
	$sql = array();
	$extras_table_fld = array("WARDS"=>"cw","AREA"=>"ca","FUNDSRC"=>"cs");
	foreach($mheadings[$section] as $fld => $h) {
		$log_trans = "";
		switch($h['h_type']) {
			case "WARDS":
			case "FUNDSRC":
			case "AREA":
				$e_table_fld = $extras_table_fld[$h['h_type']];
				$s = array();
				$deleted = array();
				$added = array();
				$log_trans = "";
				$n = array();
				foreach($_REQUEST[$fld] as $i) {
					$n[] = $lists[$h['h_table']][$i]['value']." => ".(!isset($extras[$fld][$i]) ? "NEW" : "");
					if(!isset($extras[$fld][$i])) {
						$log_trans.="<br />- Added \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$lists[$h['h_table']][$i]['value']."\'";
						$added[] = $i;
					}
				}
				$new = implode("<br />",$n);
				$o = array();
				foreach($extras[$fld] as $i => $l) {
					$o[] = $l['value'];
					if(!in_array($i,$_REQUEST[$fld])) {
						$log_trans.="<br />- Deleted \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$l['value']."\'";
						$deleted[] = $i;
					}
				}
				$old = implode("<br />",$o);
				if(strlen($log_trans)>0) {
					$log_trans = "Updated ".$h['h_client'].":".$log_trans;
					if(count($deleted)>0) {
						$sa = "UPDATE ".$dbref."_".$fld." SET ".$e_table_fld."_active = false WHERE ".$e_table_fld."_".$table_id."id = ".$obj_id." AND ".$e_table_fld."_listid IN (".implode(",",$deleted).")";
						db_update($sa);
						$s[] = $sa;
					}
					if(count($added)>0) {
						$sa = "INSERT INTO ".$dbref."_".$fld." (".$e_table_fld."_".$table_id."id, ".$e_table_fld."_listid, ".$e_table_fld."_active) VALUES (".$obj_id.",".implode(",true),(".$obj_id.",",$added).",true)";
						db_insert($sa);
						$s[] = $sa;
					}
					$v = array(
						'fld'=>$fld,
						'timeid'=>0,
						'text'=>$log_trans,
						'old'=>implode(",",$deleted),
						'new'=>implode(",",$added),
						'act'=>"E",
						'YN'=>"Y"
					);
					logChanges($section,$obj_id,$v,code(implode(chr(10),$s)));
				}
				break;
			case "DATE":
				$new = isset($_REQUEST[$fld]) ? strtotime($_REQUEST[$fld]) : "";
				$old = isset($object[$fld]) ? $object[$fld] : "";
				if(checkIntRef($new) || checkIntRef($old)) {
					if(strpos($fld,"end")>0) { $new+=86400-1; }
					if($new!=$old) {
						$log_trans = addslashes("Edited ".$h['h_client']." to '".date("d M Y",$new).((checkIntRef($old)) ? "' from '".date("d M Y",$old) : "")."'");
						$trans[$fld]['lt'] = $log_trans;
						$trans[$fld]['n'] = $new;
						$trans[$fld]['o'] = $old;
						$changes[] = $fld." = '".$new."'";
					}
				}
				break;
			case "LIST":
				$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
				if($h['h_type']=="LIST") {
					$old = isset($object['o'.$fld]) ? $object['o'.$fld] : "0";
				} else {
					$old = isset($object[$fld]) ? $object[$fld] : "0";
				}
				if($new!=$old) {
					if(in_array($h['h_type'],array("CAP","TOP"))) {
						if($old==0) {
							$log_trans = addslashes("Added link to ".$h['h_client']." '".$id_labels_all[$h['h_type']].$new."'");
						} elseif($new==0) {
							$log_trans = addslashes("Deleted link to ".$h['h_client']." '".$id_labels_all[$h['h_type']].$old."'");
						} else {
							$log_trans = addslashes("Changed link to ".$h['h_client']." to '".$id_labels_all[$h['h_type']].$new."' from '".$id_labels_all[$h['h_type']].$old."'");
						}
					} else {
						$log_trans = addslashes("Edited ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."' from '".$object[$fld]."'");
					}
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
			default:
				$new = isset($_REQUEST[$fld]) ? code($_REQUEST[$fld]) : "";
				$old = isset($object[$fld]) ? $object[$fld] : "";
				if($new!=$old) {
					$log_trans = addslashes("Edited ".$h['h_client']." to '".$new."' from '".$old."'");
					$trans[$fld]['lt'] = $log_trans;
					$trans[$fld]['n'] = $new;
					$trans[$fld]['o'] = $old;
					$changes[] = $fld." = '".$new."'";
				}
				break;
		}
		/*echo "<tr>
		<th>".$fld."</th>
		<td>".$h['h_type']."</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	if(count($changes)>0) {
		$sb = "UPDATE ".$dbref."_".$table_tbl." SET ".implode(", ",$changes)." WHERE ".$table_fld."id = ".$obj_id;
		db_update($sb);
		foreach($trans as $fld => $lt) {
			$v = array(
				'fld'=>$fld,
				'timeid'=>0,
				'text'=>$lt['lt'],
				'old'=>$lt['o'],
				'new'=>$lt['n'],
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sb));
		}
	}
	$result_changes = array();
	$trans = array();
	$values = array();
	foreach($_REQUEST['ti'] as $ti) {
		$res = $results[$ti];
		$fld = $r_table_fld."target";
		$values['actual'][$ti] = $res[$r_table_fld.'actual'];
		$log_trans = "";
		if(isset($_REQUEST[$r_table_fld.'target_'.$ti]) && ($my_access['access']['module'] || $my_access['access']['finance']) && $base[1]=="fin") {
			$new = $_REQUEST[$r_table_fld.'target_'.$ti];
			$old = $res[$r_table_fld.'target'];
			if($new!=$old) {
				$values['target'][$ti] = $new;
				$log_trans = "Edited Budget for ".$time[$ti]['display_full']." to \'".KPIresultDisplay($new,1)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,1)."\'" : "");
				$trans[$ti][$fld]['lt'] = $log_trans;
				$trans[$ti][$fld]['n'] = $new;
				$trans[$ti][$fld]['o'] = $old;
				$result_changes[$ti][] = $r_table_fld."target = $new";
			} else {
				$values['target'][$ti] = $old;
			}
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Budget</td>
			<td>".$new."</td>
			<td>".$old."</td>
			<th>".$log_trans."</th>
			</tr>";*/
		}
	}
		if(count($result_changes)>0) {
			$total_budget = array_sum($values['target']);
			$ytd_budget = 0;
			$actual = 0;
			for($i=1;$i<=12;$i++) {
				$actual = $results[$i]['cr_ytd_actual'];
				//TOTAL BUDGET
				if($total_budget!=$results[$i]['cr_tot_target']) {
					//target
					$fld = "cr_tot_target";
					$new = $total_budget;
					$old = $results[$i][$fld];
					$result_changes[$i][] = "$fld = $new";
					$log_trans = "Edited Total Budget for ".$time[$i]['display_full']." to \'".KPIresultDisplay($new,1)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,1)."\'" : "");
					$trans2[$i][$fld]['lt'] = $log_trans;
					$trans2[$i][$fld]['n'] = $new;
					$trans2[$i][$fld]['o'] = $old;
					//var
					$fld = "cr_tot_var";
					$new = calcChange('var',$total_budget,$actual);
					$old = $results[$i][$fld];
					$result_changes[$i][] = "$fld = $new";
					$log_trans = "Edited Total Variance for ".$time[$i]['display_full']." to \'".KPIresultDisplay($new,1)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,1)."\'" : "");
					$trans2[$i][$fld]['lt'] = $log_trans;
					$trans2[$i][$fld]['n'] = $new;
					$trans2[$i][$fld]['o'] = $old;
					//perc
					$fld = "cr_tot_perc";
					$new = calcChange('perc',$total_budget,$actual);
					$old = $results[$i][$fld];
					$result_changes[$i][] = "$fld = $new";
					$log_trans = "Edited Total Percentage Spent for ".$time[$i]['display_full']." to \'".KPIresultDisplay($new,2)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,2)."\'" : "");
					$trans2[$i][$fld]['lt'] = $log_trans;
					$trans2[$i][$fld]['n'] = $new;
					$trans2[$i][$fld]['o'] = $old;
				}
				$ytd_budget+=$values['target'][$i];
				if($ytd_budget!=$results[$i]['cr_ytd_target']) {
					//target
					$fld = "cr_ytd_target";
					$new = $ytd_budget;
					$old = $results[$i][$fld];
					$result_changes[$i][] = "$fld = $new";
					$log_trans = "Edited YTD Budget for ".$time[$i]['display_full']." to \'".KPIresultDisplay($new,1)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,1)."\'" : "");
					$trans2[$i][$fld]['lt'] = $log_trans;
					$trans2[$i][$fld]['n'] = $new;
					$trans2[$i][$fld]['o'] = $old;
					//var
					$fld = "cr_ytd_var";
					$new = calcChange('var',$ytd_budget,$actual);
					$old = $results[$i][$fld];
					$result_changes[$i][] = "$fld = $new";
					$log_trans = "Edited YTD Variance for ".$time[$i]['display_full']." to \'".KPIresultDisplay($new,1)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,1)."\'" : "");
					$trans2[$i][$fld]['lt'] = $log_trans;
					$trans2[$i][$fld]['n'] = $new;
					$trans2[$i][$fld]['o'] = $old;
					//perc
					$fld = "cr_ytd_perc";
					$new = calcChange('perc',$ytd_budget,$actual);
					$old = $results[$i][$fld];
					$result_changes[$i][] = "$fld = $new";
					$log_trans = "Edited YTD Percentage Spent for ".$time[$i]['display_full']." to \'".KPIresultDisplay($new,2)."\'".(checkIntRef($old) ? " from \'".KPIresultDisplay($old,2)."\'" : "");
					$trans2[$i][$fld]['lt'] = $log_trans;
					$trans2[$i][$fld]['n'] = $new;
					$trans2[$i][$fld]['o'] = $old;
				}
			}
			foreach($result_changes as $ti => $r_c) {
				$sc = "UPDATE ".$dbref."_".$table_tbl."_results SET ".implode(", ",$r_c)." WHERE ".$r_table_fld.$table_id."id = ".$obj_id." AND ".$r_table_fld."timeid = ".$ti;
				db_update($sc);
				foreach($trans[$ti] as $fld => $lt) {
					$v = array(
						'fld'=>$fld,
						'timeid'=>$ti,
						'text'=>$lt['lt'],
						'old'=>$lt['o'],
						'new'=>$lt['n'],
						'act'=>"E",
						'YN'=>"Y"
					);
					logChanges($section,$obj_id,$v,code($sc));
					$results[$ti][$fld] = $lt['n'];
				}
				foreach($trans2[$ti] as $fld => $lt) {
					$v = array(
						'fld'=>$fld,
						'timeid'=>$ti,
						'text'=>$lt['lt'],
						'old'=>$lt['o'],
						'new'=>$lt['n'],
						'act'=>"E",
						'YN'=>"N"
					);
					logChanges($section,$obj_id,$v,code($sc));
					$results[$ti][$fld] = $lt['n'];
				}
			}
		}
		foreach($_REQUEST['yi'] as $yi) {
			$new_crr = (isset($_REQUEST['years']['crr'][$yi]) && checkIntRef($_REQUEST['years']['crr'][$yi])) ? $_REQUEST['years']['crr'][$yi] : 0;
			$old_crr = isset($forecast[$yi]['cf_crr']) ? $forecast[$yi]['cf_crr'] : 0;
			$new_other = (isset($_REQUEST['years']['other'][$yi]) && checkIntRef($_REQUEST['years']['other'][$yi])) ? $_REQUEST['years']['other'][$yi] : 0;
			$old_other = isset($forecast[$yi]['cf_other']) ? $forecast[$yi]['cf_other'] : 0;
			$y_changes = array();
			$y_trans = array();
			if($new_crr!=$old_crr) {
				$y_changes[] = "cf_crr = ".$new_crr;
				$y_trans[] = "- Changed CRR to '".number_format($new_crr,2)."' from '".number_format($old_crr,2)."'";
			}
			if($new_other!=$old_other) {
				$y_changes[] = "cf_other = ".$new_other;
				$y_trans[] = "- Changed Other to '".number_format($new_other,2)."' from '".number_format($old_other,2)."'";
			}
			if(count($y_changes)>0) {
				$sy = "UPDATE ".$dbref."_".$table_tbl."_forecast SET ".implode(",",$y_changes)." WHERE cf_capid = ".$obj_id." AND cf_listid = $yi";
				$mar = db_update($sy);
				if(checkIntRef($mar)) {
					$v = array(
						'fld'=>"",
						'timeid'=>$yi,
						'text'=>addslashes("Updated Forecast Annual Budget for ".$lists['years'][$yi]['value']." as follows: <br />".implode("<br />",$y_trans)),
						'old'=>$old_crr."_".$old_other,
						'new'=>$new_crr."_".$new_other,
						'act'=>"E",
						'YN'=>"Y"
					);
					logChanges($section,$obj_id,$v,code($sy));
				}
			}
		}
	//echo "</table>";
		echo "	<script type=text/javascript>
					document.location.href = '".$base[0]."_".$base[1].".php?page_id=capital&tab=edit&r[]=ok&r[]=Capital Project+".$id_labels_all[$section].$obj_id."+has+been+successfully+updated.';
				</script>";



	
	
	
}	
	
	
} else {
//VIEW EDIT FORM
	include("manage_fin_table_detail.php");
	$log_sql = "SELECT clog_date, CONCAT_WS(' ',tkname,tksurname) as clog_tkname, clog_transaction FROM ".$dbref."_capital_log INNER JOIN assist_".$cmpcode."_timekeep ON clog_tkid = tkid WHERE clog_capid = '".$obj_id."' AND clog_yn = 'Y' ORDER BY clog_id DESC";
	displayAuditLog($log_sql,array('date'=>"clog_date",'user'=>"clog_tkname",'action'=>"clog_transaction"));

}

?>
</body>
</html>