<?php 
$page_title = "Revenue By Source";
$import_section = "RS";
$head_section = "RS";
$get_headings = array("RS","RS_R");
include("support_import_headings.php");
include("inc/header.php"); 

$import_log_file = openLogImport();
logImport($import_log_file, "Open Support > Import > ".$page_title, $self);



function valError($v,$e) {
	return $v."<br /><b>-> ".$e."</b>";
}
function valList($v,$e) {
	return $v."<br />=> ".$e;
}

$file_error = array("error","An error occurred while trying to import the file.  Please go back and try again.");
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VIEW";

switch($act) {
	case "RESET":
		$r_sql = array();
		$r_sql[] = "TRUNCATE ".$dbref."_revbysrc";
		$r_sql[] = "TRUNCATE ".$dbref."_revbysrc_results";
		foreach($r_sql as $sql) {	
			$mnr = db_update($sql);
		}
		db_update("UPDATE ".$dbref."_import_status SET status = false WHERE value = '".$import_section."'");
		$import_status[$import_section] = false;
		$result = array("ok","All $page_title tables successfully reset.");
		logImport($import_log_file, $page_title." > Reset tables", $self);
		break;
}

?>		<style type=text/css>
		table th {
			vertical-align: top;
		}
		.ireset {
			background-color: #CC0001;
			color: #ffffff;
			line-height: 1.5em;
		}
		.dark-info {
			background: #FE9900 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		.dark-error {
			background: #CC0001 url();
			color: #ffffff;
			border: 1px solid #ffffff;
		}
		th.ignite { background-color: #555555; }
		th.doc { background-color: #999999; }
		</style>
<?php
logImport($import_log_file, $page_title." > ".$act, $self);
$save_echo_filename = $import_section."_".date("YmdHis")."_".$act.".html";
$onscreen = "";
switch($act) {
	case "VIEW": case "RESET": case "GENERATE":
		?>
		<script>
			$(function() {
				$(":button").click(function() {
					var id = $(this).attr("id");
					switch(id) {
						case "generate": document.location.href = 'support_import_generate.php?i=<?php echo $import_section; ?>'; break;
						case "reset": if(confirm("This will delete any existing data!\n\nAre you sure you wish to continue?")) { document.location.href = '<?php echo $self; ?>?act=RESET'; } break;
					}
				});
				$("#isubmit:button").click(function() {
					var ifl = $("#ifl").attr("value");
					if(ifl.length > 0) 
						$("#list").submit();
					else
						alert("Please select a file for import.");
				});
			});
		</script>
		<?php displayResult($result); ?>
		<p>In order to generate the CSV files necessary for the import, simply go to each worksheet in the SDBIP document and select File > Save As and change the File Type option to 'Comma-delimited (*.csv)'.
		<br /><b>PLEASE ENSURE THAT THE COLUMNS MATCH THOSE AS GIVEN ON THE IMPORT PAGE FOR EACH SECTION!!!</b></p>
		<?php
$i = 1;
			?>
			<form id=list action="<?php echo $self; ?>" method=post enctype="multipart/form-data">
				<input type=hidden name=act value=PREVIEW />
				<table>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Generate the CSV file:
							<br />+ <input type=button value=Generate id=generate /> the CSV template here and copy the data into the sheet from the SDBIP template  OR
							<br />+ Open the SDBIP template, go to the Lists sheet, click File > Save As & change the File Type option to Comma-delimited (*.csv).
							<br />&nbsp;<br />Please ensure that the columns match up to those given below.&nbsp;
						</td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Reset the tables to delete previously loaded information.&nbsp;<span class=float><input type=button value="Reset Tables" id=reset class=idelete /></span></td>
					</tr>
					<?php if(!$import_status[$import_section]) { ?>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Select the CSV file and click the Import button. &nbsp;&nbsp;<input type=file name=ifile id=ifl />&nbsp;<span class=float><input type=button id=isubmit value=Import class=isubmit /></span></td>
					</tr>
					<tr>
						<th>Step <?php echo $i; $i++; ?>:</th>
						<td>Review the information.  If the information is correct, click the Accept button.
						<br /><b>NO DATA WILL BE IMPORTED UNTIL THE FINAL ACCEPT BUTTON HAS BEEN CLICKED!!</b></td>
					</tr>
					<?php } ?>
				</table>
			</form>
			<?php displayGoBack("support_import.php"); 
			
			drawCSVColumnsGuidelines("rs_id",$target_text);
			
		break;	//switch act case view/reset/generate
	case "PREVIEW":
		$onscreen.= "<h1 class=centre>$page_title Preview</h1><p class=centre>Please click the \"Accept\" button at the bottom of the page<br />in order to finalise the import of the list items.</p>";
		$onscreen.= "<p class=centre>Any errors highlighted in <span class=dark-error>red</span> are errors that prevent the import (fatal errors).<br />The \"Accept\" button will not be available until all of these errors have been corrected.</p>";
		$onscreen.= "<p class=centre>Any cells highlighted in <span class=dark-info>orange</span> are where the value associated with the given list reference does not match the value given in the next cell.<br />These errors do not prevent the import (non-fatal errors) but should be checked to ensure that the correct data is being imported.</p>";
		$onscreen.= "<p class=centre>Please remember, if you get any \"Too long\" errors, that any non-alphanumeric characters in text fields are converted a special code to allow them to be uploaded to the database.
		<br />This code is generally 6 characters long and counts towards the overall length of the text field.</p>
		<p class=center><span class=iinform>Please note:</span> The top heading row displays the headings from the module as per Setup > Defaults > Headings.
		<br />The second heading row displays the headings from the import file.
		<br />Please ensure that they match up to ensure that the correct data is being imported into the correct field.</p>";
	case "ACCEPT":
		if($act!="PREVIEW") {
			$accept_text = "<h1 class=centre>Processing...</h1><p class=centre>The import is being processed.<br />Please be patient.  You will be redirected back to the Support >> Import page when the processing is complete.</p>";
			$onscreen.= $accept_text;
			echo $accept_text;
		}
		$onscreen.= "<form id=save method=post action=\"$self\">";
		$onscreen.= "<input type=hidden name=act value=ACCEPT />";
		$err = false;
		$file_location = $modref."/import/";
		checkFolder($file_location);
		$file_location = "../files/".$cmpcode."/".$modref."/import/";
		//GET FILE
		if(isset($_REQUEST['f'])) {
			$file = $file_location.$_REQUEST['f'];
			if(!file_exists($file)) {
				logImport($import_log_file, $page_title." > ERROR > $file doesn't exist", $self);
				die(displayResult(array("error","An error occurred while trying to retrieve the file.  Please go back and import the file again.")));
			}
		} else {
			if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
				switch($_FILES["ifile"]["error"]) {
					case 2: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [Too big]", $self);
						die(displayResult(array("error","The file you are trying to import exceeds the maximum allowed file size of 5MB."))); 
						break;
					case 4:	
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"]." [No file found]", $self);
						die(displayResult(array("error","No file found.  Please select a file to import."))); 
						break;
					default: 
						logImport($import_log_file, $page_title." > ERROR > ".$_FILES["ifile"]["name"]." > ".$_FILES["ifile"]["error"], $self);
						die(displayResult(array("error","File error: ".$_FILES["ifile"]["error"])));	
						break;
				}
				$err = true;
			} else {    //IF ERROR WITH UPLOAD FILE
				$ext = substr($_FILES['ifile']['name'],-3,3);
				if(strtolower($ext)!="csv") {
					logImport($import_log_file, $page_title." > ERROR > Invalid file extension - ".$ext, $self);
					die(displayResult(array("error","Invalid file type.  Only CSV files may be imported.")));
					$err = true;
				} else {
					$filen = substr($_FILES['ifile']['name'],0,-4)."_".date("Ymd_Hi",$today).".csv";
					$file = $file_location.$filen;
					//UPLOAD UPLOADED FILE
					copy($_FILES["ifile"]["tmp_name"], $file);
					logImport($import_log_file, $page_title." > $file uploaded", $self);
				}
			}
		}
		//READ FILE
		if(!$err) {
			$onscreen.= "<input type=hidden name=f value='".(isset($filen) ? $filen : $_REQUEST['f'])."' />";
		    $import = fopen($file,"r");
            $data = array();
            while(!feof($import)) {
                $tmpdata = fgetcsv($import);
                if(count($tmpdata)>1) {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($import);

			if(count($data)>2) {
				$doc_headings = $data[0];
				unset($data[0]); unset($data[1]);
//		$fdata = "\"Sub-Directorate [R]\",\" \",\" \",\"Line Item [R]\",\"GFS Classification [R]\",\" \",\"Vote Number\",\" \",\"July 2011\",\" \",\" \",\"August 2011\",\" \",\" \",\"September 2011\",\" \",\" \",\"October 2011\",\" \",\" \",\"November 2011\",\" \",\" \",\"December 2011\",\" \",\" \",\"January 2012\",\" \",\" \",\"February 2012\",\" \",\" \",\"March 2012\",\" \",\" \",\"April 2012\",\" \",\" \",\"May 2012\",\" \",\" \",\"June 2012\",\" \",\" \"\n";
//		$fdata.= "\"Ignite\",\"Directorate\",\"List\",\"200 characters\",\"Ignite\",\"List\",\"100 characters\",\" \",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\"";
		//$columns[0] = array("Sub-Directorate [R]","","","Line Item [R]","GFS Classification [R]","","Vote Number"," ","July 2011","","","August 2011","","","September 2011","","","October 2011","","","November 2011","","","December 2011","","","January 2012","","","February 2012","","","March 2012","","","April 2012","","","May 2012","","","June 2012","","");
		//$columns[1] = array("Ignite","Directorate","List","200 characters","Ignite","List","100 characters"," ","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.","Revenue","Op. Exp.","Cap. Exp.");
		$columns[0] = array("Ignite","Line Item","Vote Number","July","August","September","October","November","December","January","February","March","April","May","June");
		$columns[1] = array("Ref","200 characters","100 characters","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number");
				$colspanned = array();
				/*$cells = array(
					0=>'rs_id',
					1=>'rs_value',
					2=>'rs_vote',
					3=>'budget_1',
					4=>'budget_2',
					5=>'budget_3',
					6=>'budget_4',
					7=>'budget_5',
					8=>'budget_6',
					9=>'budget_7',
					10=>'budget_8',
					11=>'budget_9',
					12=>'budget_10',
					13=>'budget_11',
					14=>'budget_12'
				);*/
				$listinfo = array(
					'cf_subid' 		=> array('tbl' => "subdir", 'other' => 2, 'dir'=>1),
					'cf_gfsid' 		=> array('tbl' => "list_gfs", 'other' => 5)
				);
				
				$onscreen.="<table>".getImportHeadings();
					/*$onscreen.="<tr><th rowspan=2 style='vertical-align: middle;'>?</td>";
						foreach($columns[0] as $cell => $c) {
							$colspan = 1;
							if(strlen($c)>0) {
								$onscreen.= "<th colspan=$colspan class=ignite>".$c."</th>";
							}
						}
					$onscreen.="</tr><tr>";
						foreach($doc_headings as $cell => $c) {
							if(strlen($c)>0 && $cell < 15) {
								$onscreen.= "<th colspan=$colspan class=doc >".$c."</th>";
							}
						}
					$onscreen.="</tr>";*/
					
					$t_sql = array();
					$r_sql = array();
					$f_sql = array();
					$refs = array();
					$values = array();
					$all_valid = true;
					$error_count = 0;
					$info_count = 0;
					foreach($data as $i => $d) {
						$targettype = 0;
						$valid = true;
						$row = array();
						$display = array();
						$val = array();
						foreach($cells as $c => $fld) {
							$v = $d[$c];
							$val[$c][0] = 1;
							//DATA VALIDATION HERE
							switch($fld) {
								case "rs_id":		//IGNITE REF
									$v = $v*1;
									if(!checkIntRef($v)) {			//not a number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid number");
									} elseif(in_array($v,$refs)) {	//duplicate
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Duplicate reference");
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = $v;
									}
									break;
								case "rs_value":		//PMS REF
								case "rs_vote":
									$len = $mheadings[$head_section][$fld]['c_maxlen'];
									$type = $mheadings[$head_section][$fld]['h_type'];
									if(($mheadings[$head_section][$fld]['i_required']+$mheadings[$head_section][$fld]['c_required'])>0 && strlen(trim(code($v))) == 0) {			//required field
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Required field");
									} elseif(strlen(trim(code($v)))> $len && $type!="TEXT") {			//too long
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Too long: ".strlen(trim(code($v)))." chars (max ".$len." chars)".$fld);
									} else {						//valid
										$val[$c][1] = $v;
										$row[$fld] = trim(code($v));
									}
									break;
								case "budget_1":
								case "budget_2":
								case "budget_3":
								case "budget_4":
								case "budget_5":
								case "budget_6":
								case "budget_7":
								case "budget_8":
								case "budget_9":
								case "budget_10":
								case "budget_11":
								case "budget_12":
									$v = trim($v);
									if(!is_numeric($v) && strlen($v)>0) {	//invlid number
										$valid = 0; $error_count++;
										$val[$c][0] = 0;
										$val[$c][1] = valError($v,"Invalid data: number only");
									} else {
										if(strlen($v)==0) {
											$v = 0;
										}
										$row[$fld] = $v;
										$val[$c][1] = $v;
										if($targettype==2 && $v <=1 && $v>0) {
											$val[$c][0]=2; $info_count++;
										}
									}
									break;
								default:
									//$val[$c][1] = $v;
									//$row[$fld] = $v;
							}
						}
						$val['a'] = $valid;
						$onscreen.= "<tr>";
							switch($val['a']) {
								case true:
									$onscreen.= "<td class=\"ui-state-ok\"><span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span></td>";
									break;
								case false:
								default:
									$onscreen.= "<td class=\"ui-state-error\"><span class=\"ui-icon ui-icon-closethick\" style=\"margin-right: .3em;\"></span></td>";
							}
							foreach($columns[1] as $cell => $c) {
								if(isset($val[$cell])) {
									$v = $val[$cell];
									switch($v[0]) {
										case 0:
											$onscreen.= "<td class=\"dark-error\">";
											break;
										case 2:
											$onscreen.= "<td class=\"dark-info\">";
											break;
										case 1:
										default:
											$onscreen.= "<td ".($cell>2 ? "class=right" : "").">";
									}
									$onscreen.= $cell > 2 ? number_format($val[$cell][1],2,".",",") : $val[$cell][1];
									$onscreen.= "</td>";
								} else {
									$onscreen.= "<td></td>";
								}
							}
						$onscreen.= "</tr>";
						if($valid) {
							$values[$i] = $val;
							//CASHFLOW
							$t_sql[] = "(".$row['rs_id'].", '".$row['rs_value']."', '".$row['rs_vote']."',".$row['rs_id'].",true)";
							//RESULTS
							$r_sql[] = "INSERT INTO ".$dbref."_revbysrc_results (rr_id, rr_rsid, rr_timeid, rr_budget, rr_actual) VALUES (null,".$row['rs_id'].",1,".$row['budget_1'].",0), (null,".$row['rs_id'].",2,".$row['budget_2'].",0), (null,".$row['rs_id'].",3,".$row['budget_3'].",0), (null,".$row['rs_id'].",4,".$row['budget_4'].",0), (null,".$row['rs_id'].",5,".$row['budget_5'].",0), (null,".$row['rs_id'].",6,".$row['budget_6'].",0), (null,".$row['rs_id'].",7,".$row['budget_7'].",0), (null,".$row['rs_id'].",8,".$row['budget_8'].",0), (null,".$row['rs_id'].",9,".$row['budget_9'].",0), (null,".$row['rs_id'].",10,".$row['budget_10'].",0), (null,".$row['rs_id'].",11,".$row['budget_11'].",0), (null,".$row['rs_id'].",12,".$row['budget_12'].",0)";
						} else {
							$all_valid = false;
						}
						//arrPrint($row);
					}
					
				
				$onscreen.="</table>";
				
				if($act!="ACCEPT") {
					$onscreen.= "<p class=centre>There are $error_count fatal error(s).<br />There are $info_count non-fatal error(s).</p>";
					$onscreen.= "<p class=centre><input type=submit value=\"Accept\" class=isubmit id=submit_me /> <input type=button id=reset_me class=idelete value=Reject /><p>";
					if(!$all_valid || $error_count > 0) {
						$onscreen.= "<script>$(function() {
							$(\"#submit_me\").attr(\"disabled\",true);
							$(\"#submit_me\").removeClass(\"isubmit\");
							$(\"#reset_me\").attr(\"disabled\",true);
							$(\"#reset_me\").removeClass(\"ireset\");
						});</script>";
					}
					//echo "<p class=centre>Warning: When you click the Accept button, only those list items with a <span class=\"ui-icon ui-icon-check\" style=\"margin-right: .3em;\"></span> will be imported.</p>";
					echo $onscreen;
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
				} else {
					$z_sql = array();
					$z=0;
					$z_sql[$z] = "INSERT INTO ".$dbref."_revbysrc (rs_id, rs_value, rs_vote, rs_sort, rs_active) VALUES ";
					$a = 0;
					foreach($t_sql as $t) {
						if($a>=50) {
							$z++;
							$z_sql[$z] = "INSERT INTO ".$dbref."_revbysrc (rs_id, rs_value, rs_vote, rs_sort, rs_active) VALUES ";
							$a=0;
						} elseif($a>0) {
							$z_sql[$z].=",";
						}
						$z_sql[$z].=$t;
						$a++;
					}
					foreach($z_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.="<P>".$sql;
						//echo "<P>".$id;
					}
					foreach($r_sql as $key => $sql) {
						$id = db_insert($sql);
						$onscreen.="<P>".$sql;
						//echo "<P>".$id;
					}
					db_update("UPDATE ".$dbref."_import_status SET status = true WHERE value = '$import_section'");
					saveEcho($modref."/import", $save_echo_filename, htmlPageHeader().$onscreen."</form></body></html>");
					logImport($import_log_file, $page_title." > $act > ".$save_echo_filename, $self);
					echo "<script>document.location.href = 'support_import.php?r[]=ok&r[]=Revenue+By+Source+imported.';</script>";
				}
				echo "</form>";
			} else {	//data count
				logImport($import_log_file, $page_title." > ERROR > No info to upload [".$file."]", $self);
				die(displayResult(array("error","Couldn't find any information to import.")));
			}
			
		} else {	//if !err
			die(displayResult($file_error));
		}
		break;	//switch act case accept
}	//switch act
?>

</body>
</html>