<?php 
$get_lists = true;
$get_active_lists = true;
$section = "TOP";
$page_title = "Detail";
$obj_id = $_REQUEST['id'];
include("inc/header.php"); 

/*** GET OBJECT DETAILS ***/

$fields = array();
foreach($mheadings[$section] as $fld => $h) {
	switch($h['h_type']) {
		case "WARDS":
		case "AREA":
			break;
		case "LIST":
			$fields[] = $h['h_table'].".value as ".$fld;
			$fields[] = "t.".$fld." as o".$fld;
			break;
		default: $fields[] = "t.".$fld;
	}
}

$object_sql = "SELECT targettype.id as tt, calctype.code as ct, ".implode(", ",$fields)."
FROM ".$dbref."_top t
INNER JOIN ".$dbref."_dir as dir ON dir.id = t.top_dirid";
foreach($mheadings[$section] as $fld => $h) {
	if($h['h_type']=="LIST" && $h['h_table']!="dir") {
		$a = $h['h_table']=="calctype" ? "code" : "id";
		$object_sql.= " INNER JOIN ".$dbref."_list_".$h['h_table']." as ".$h['h_table']." ON ".$h['h_table'].".".$a." = ".$fld;
	}
}
$object_sql.= " WHERE t.top_id = ".$obj_id;
//echo "<P>".$object_sql;
$object = mysql_fetch_all($object_sql);
$object = $object[0];

$extras = array();
//WARDS
$fld = "top_wards";
$tbl = "wards";
$a = "tw";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.".$a."_listid AND b.".$a."_active = true AND b.".$a."_topid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql,"id");
//AREAS
$fld = "top_area";
$tbl = "area";
$a = "ta";
$a_sql = "SELECT a.id, a.value, a.code FROM ".$dbref."_list_".$tbl." a 
INNER JOIN ".$dbref."_".$fld." b ON a.id = b.".$a."_listid AND b.".$a."_active = true AND b.".$a."_topid = ".$obj_id."
WHERE a.active = true ORDER BY sort, code, value";
$extras[$fld] = mysql_fetch_alls($a_sql, "id");



//FORECAST
$f_sql = "SELECT * FROM ".$dbref."_top_forecast WHERE tf_topid = ".$obj_id." AND tf_active = true";
$forecast = mysql_fetch_alls($f_sql,"tf_listid");



$result_sql = "SELECT * FROM ".$dbref."_top_results WHERE tr_topid = ".$obj_id." ORDER BY tr_timeid";
$results = mysql_fetch_alls($result_sql,"tr_timeid");




/** ACTION **/
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
echo "<p>Processing edit...</p>";
//echo "<table><tr><th>Fld</th><th>Type</th><th>New</th><th>Old</th><th>Action</th></tr>";
$updates = array();
$trans = array();
$changes = array();
$sql = array();
foreach($mheadings[$section] as $fld => $h) {
	$log_trans = "";
	switch($h['h_type']) {
		case "TOP":
		case "CAP":
			$new = "Do nothing";
			$old = "Do nothing";
			$log_trans = "";
			break;
		case "WARDS":
		case "AREA":
			$s = array();
			$deleted = array();
			$added = array();
			$log_trans = "";
			$n = array();
			foreach($_REQUEST[$fld] as $i) {
				$n[] = $lists[$h['h_table']][$i]['value']." => ".(!isset($extras[$fld][$i]) ? "NEW" : "");
				if(!isset($extras[$fld][$i])) {
					$log_trans.="<br />- Added \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$lists[$h['h_table']][$i]['value']."\'";
					$added[] = $i;
				}
			}
			$new = implode("<br />",$n);
			$o = array();
			foreach($extras[$fld] as $i => $l) {
				$o[] = $l['value'];
				if(!in_array($i,$_REQUEST[$fld])) {
					$log_trans.="<br />- Deleted \'".(isset($l['code']) && strlen($l['code'])>0 ? $l['code'].": " : "").$l['value']."\'";
					$deleted[] = $i;
				}
			}
			$old = implode("<br />",$o);
			if(strlen($log_trans)>0) {
				$log_trans = "Updated ".$h['h_client'].":".$log_trans;
				if(count($deleted)>0) {
					$sa = "UPDATE ".$dbref."_".$fld." SET t".(substr($h['h_table'],0,1))."_active = false WHERE t".(substr($h['h_table'],0,1))."_topid = ".$obj_id." AND t".(substr($h['h_table'],0,1))."_listid IN (".implode(",",$deleted).")";
					db_update($sa);
					$s[] = $sa;
				}
				if(count($added)>0) {
					$sa = "INSERT INTO ".$dbref."_".$fld." (t".(substr($h['h_table'],0,1))."_topid, t".(substr($h['h_table'],0,1))."_listid, t".(substr($h['h_table'],0,1))."_active) VALUES (".$obj_id.",".implode(",true),(".$obj_id.",",$added).",true)";
					db_insert($sa);
					$s[] = $sa;
				}
				$v = array(
					'fld'=>$fld,
					'timeid'=>0,
					'text'=>$log_trans,
					'old'=>implode(",",$deleted),
					'new'=>implode(",",$added),
					'act'=>"E",
					'YN'=>"Y"
				);
				logChanges($section,$obj_id,$v,code(implode(chr(10),$s)));
			}
			break;
		case "LIST":
			$new = isset($_REQUEST[$fld]) ? $_REQUEST[$fld] : "0";
			$old = isset($object['o'.$fld]) ? $object['o'.$fld] : "0";
			if($new!=$old && $new!="0") {
				$log_trans = addslashes("Updated ".$h['h_client']." to '".$lists[$h['h_table']][$new]['value']."' from '".$object[$fld]."'");
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$changes[] = $fld." = '".$new."'";
			}
			break;
		default:
			$new = isset($_REQUEST[$fld]) ? code($_REQUEST[$fld]) : "";
			$old = isset($object[$fld]) ? $object[$fld] : "";
			if($new!=$old) {
				$log_trans = addslashes("Updated ".$h['h_client']." to '".$new."' from '".$old."'");
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$changes[] = $fld." = '".$new."'";
			}
			break;
	}
	/*echo "<tr>
	<th>".$fld."</th>
	<td>".$h['h_type']."</td>
	<td>".$new."</td>
	<td>".$old."</td>
	<th>".$log_trans."</th>
	</tr>";*/
}
if(count($changes)>0) {
	$sb = "UPDATE ".$dbref."_top SET ".implode(", ",$changes)." WHERE top_id = ".$obj_id;
	db_update($sb);
	foreach($trans as $fld => $lt) {
		$v = array(
			'fld'=>$fld,
			'timeid'=>0,
			'text'=>$lt['lt'],
			'old'=>$lt['o'],
			'new'=>$lt['n'],
			'act'=>"E",
			'YN'=>"Y"
		);
		logChanges($section,$obj_id,$v,code($sb));
	}
}
foreach($_REQUEST['ti'] as $ti) {
	$trans = array();
	$result_changes = array();
	$log_trans = "";
	if(isset($_REQUEST[$r_table_fld.'target_'.$ti]) && ($my_access['access']['module'] || $my_access['access']['toplayer'])) {
		$fld = $r_table_fld."target";
		$new = $_REQUEST[$r_table_fld.'target_'.$ti];
		$old = $results[$ti][$r_table_fld.'target'];
		if($new!=$old) {
			$log_trans = "Updated Target for ".$time[$ti]['display_full']." to \'$new\' from \'$old\'";
			$trans[$fld]['lt'] = $log_trans;
			$trans[$fld]['n'] = $new;
			$trans[$fld]['o'] = $old;
			$result_changes[] = $r_table_fld."target = $new";
		}
		/*echo "<tr>
		<th>".$ti."</th>
		<td>Target</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
		
	}
	$log_trans = "";
	if(isset($_REQUEST[$r_table_fld.'actual_'.$ti])) {
		$fld = $r_table_fld."actual";
		$new = $_REQUEST[$r_table_fld.'actual_'.$ti];
		$old = $results[$ti][$r_table_fld.'actual'];
		if($new!=$old) {
			$log_trans = "Updated Actual for ".$time[$ti]['display_full']." to \'$new\' from \'$old\'";
			$trans[$fld]['lt'] = $log_trans;
			$trans[$fld]['n'] = $new;
			$trans[$fld]['o'] = $old;
			$result_changes[] = $r_table_fld."actual = $new";
		}
		/*echo "<tr>
		<th>".$ti."</th>
		<td>Actual</td>
		<td>".$new."</td>
		<td>".$old."</td>
		<th>".$log_trans."</th>
		</tr>";*/
	}
	$comment_flds = array("tr_perf","tr_correct");
	foreach($comment_flds as $fld) {
		if(isset($_REQUEST[$fld.'_'.$ti])) {
			$new = code($_REQUEST[$fld.'_'.$ti]);
			$old = $results[$ti][$fld];
			if($new!=$old) {
				$log_trans = "Updated ".$mheadings[$r_section][$fld]['h_client']." for ".$time[$ti]['display_full']." to \'$new\'".(strlen($old)>0 ? " from \'$old\'" : "");
				$trans[$fld]['lt'] = $log_trans;
				$trans[$fld]['n'] = $new;
				$trans[$fld]['o'] = $old;
				$result_changes[] = "$fld = '$new'";
			}
			/*echo "<tr>
			<th>".$ti."</th>
			<td>Actual</td>
			<td>".$new."</td>
			<td>".$old."</td>
			<th>".$log_trans."</th>
			</tr>";*/
		}
	}
	if(count($result_changes)>0) {
		$sc = "UPDATE ".$dbref."_".$table_tbl."_results SET ".implode(", ",$result_changes)." WHERE ".$r_table_fld."topid = ".$obj_id." AND ".$r_table_fld."timeid = ".$ti;
		db_update($sc);
		foreach($trans as $fld => $lt) {
			$v = array(
				'fld'=>$fld,
				'timeid'=>$ti,
				'text'=>$lt['lt'],
				'old'=>$lt['o'],
				'new'=>$lt['n'],
				'act'=>"E",
				'YN'=>"Y"
			);
			logChanges($section,$obj_id,$v,code($sc));
		}
			/*echo "<tr>
			<th>".$ti."</th>
			<td colspan=4>$sc</td>
			</tr>";*/
	}
}
		foreach($_REQUEST['yi'] as $i) {
			$log_trans = "";
			$new = $_REQUEST['years_'.$i];
			$new = checkIntRef($new) ? $new : 0;
			if(isset($forecast[$i]['tf_value'])) {
				$old = $forecast[$i]['tf_value'];
				if($new!=$old) {
					$log_trans.= "Updated Forecast Annual Target for ".$lists['years'][$i]['value']." to ".$new." from ".$old.".";
					$sd = "UPDATE ".$dbref."_top_forecast SET tf_value = ".$new." WHERE tf_topid = ".$obj_id." AND tf_listid = ".$i;
					db_update($sd);
						$v = array(
							'fld'=>"forecast",
							'timeid'=>$i,
							'text'=>$log_trans,
							'old'=>$old,
							'new'=>$new,
							'act'=>"E",
							'YN'=>"Y"
						);
						logChanges($section,$obj_id,$v,code($sd));
				}
			} else {
				if($new!=0) {
					$log_trans.= "Added Forecast Annual Target for ".$lists['years'][$i]['value']." as ".$new.".";
					$sd = "INSERT INTO ".$dbref."_top_forecast SET tf_value = ".$new.", tf_topid = ".$obj_id.", tf_listid = ".$i.", tf_active = true";
					db_insert($sd);
						$v = array(
							'fld'=>"forecast",
							'timeid'=>$i,
							'text'=>$log_trans,
							'old'=>$old,
							'new'=>$new,
							'act'=>"E",
							'YN'=>"Y"
						);
						logChanges($section,$obj_id,$v,code($sd));
				}
			}
		}
//echo "</table>";


echo "	<script type=text/javascript>
			document.location.href = '".$base[0]."_".$base[1].".php?page_id=edit&r[]=ok&r[]=KPI+".$id_labels_all[$section].$obj_id."+has+been+successfully+updated.';
		</script>";


} elseif(isset($_REQUEST['act']) && $_REQUEST['act']=="DELETE") {

	$obj_sql = "UPDATE ".$dbref."_top SET top_active = false WHERE top_id = ".$obj_id;
	$mar = db_update($obj_sql);
				$v = array(
					'fld'=>"top_active",
					'timeid'=>0,
					'text'=>"Deleted Top Layer KPI ".$id_labels_all[$section].$obj_id,
					'old'=>1,
					'new'=>0,
					'act'=>"D",
					'YN'=>"Y"
				);
				logChanges($section,$obj_id,$v,code($obj_sql));
	echo "	<script type=text/javascript>
				document.location.href = '".$base[0]."_".$base[1].".php?page_id=".$_REQUEST['page_id']."&r[]=ok&r[]=Top+Layer+KPI+".$id_labels_all[$section].$obj_id."+has+been+successfully+deleted.';
			</script>";


} else {





	include("manage_table_detail.php");


	$log_sql = "SELECT tlog_date, CONCAT_WS(' ',tkname,tksurname) as tlog_tkname, tlog_transaction, tlog_tkid FROM ".$dbref."_top_log 
				LEFT OUTER JOIN assist_".$cmpcode."_timekeep ON tlog_tkid = tkid WHERE tlog_topid = '".$obj_id."' AND tlog_yn = 'Y' ORDER BY tlog_id DESC";
	displayAuditLog($log_sql,array('date'=>"tlog_date",'user'=>"tlog_tkname",'action'=>"tlog_transaction",'tkid'=>"tlog_tkid"));

//echo $log_sql;

}

?>