<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php //include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1><?php echo $_SESSION['modtext']; ?> >> Manage >> Project Life Cycle</h1>
<p>Processing...</p>
<?php
$err = "N";
$dirid = $_REQUEST['dirid'];
$subid = $_REQUEST['subid'];
$typ = $_REQUEST['typ'];
$src = $_REQUEST['src'];
//FORM DETAILS
$kpiid = $_REQUEST['kpiid'];
$plcid = $_REQUEST['plcid'];
$tme = $_REQUEST['time'];
$target = $_REQUEST['target'];
$actual = $_REQUEST['actual'];
$krtargettypeid = 2;
$tid = $_REQUEST['tmpid'];
$tmpid = explode(",",$tid);
$appreq = $_REQUEST['appreq'];
$tbl = $_REQUEST['tbl'];


//IF APPREQ == "N"  => Approval not required
if($appreq=="N") {
	if(checkIntRef($plcid)) {
    //MOVE OLD DETAILS FROM PLC TO PLC_OLD
        $sql = "INSERT INTO ".$dbref."_kpi_plc_old (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,oldtkid,olddate) ";
        $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as oldtid, ".$today." as olddt FROM ".$dbref."_kpi_plc p2 WHERE plcid = ".$plcid;
		db_insert($sql);
        include("inc_db_con.php");
    //MOVE NEW DETAILS FROM PLC_TEMP TO PLC
        $plcnew = array();
        $sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'P' ORDER BY tmpid DESC";
            $plcnew = mysql_fetch_one($sql);
        
        $sql = "UPDATE ".$dbref."_kpi_plc SET";
        $sql.= "  plcname = '".$plcnew['plcname']."'";
        $sql.= ", plcdescription = '".$plcnew['plcdescription']."'";
        $sql.= ", plcstartdate = ".$plcnew['plcstartdate'];
        $sql.= ", plcenddate = ".$plcnew['plcenddate'];
        $sql.= ", plcproccate = ".$plcnew['plcproccate'];
        $sql.= ", plcyn = 'Y'";
        $sql.= " WHERE plcid = ".$plcid;
		db_update($sql);
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE ".$dbref."_kpi_plc_temp SET tmpyn = 'A' WHERE tmpid = ".$plcnew['tmpid'];
		db_update($sql);
    //MOVE OLD PHASES TO PHASES_OLD
        $sql = "INSERT INTO ".$dbref."_kpi_plc_phases_old (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,oldtkid,olddate) ";
        $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte FROM ".$dbref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
		db_insert($sql);
    //DELETE OLD PHASES
        $sql = "DELETE FROM ".$dbref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
		db_update($sql);
    //MOVE NEW PHASES FROM PHASES_TEMP TO PHASES
        foreach($tmpid as $temp)
        {
            $phase = array();
            if(checkIntRef($temp))
            {
                //GET TEMP DETAILS
                $sql = "SELECT * FROM ".$dbref."_kpi_plc_phases_temp WHERE tmpid = ".$temp;
                    $phase = mysql_fetch_one($sql);
                //UPDATE PHASES
                $sql = "INSERT INTO ".$dbref."_kpi_plc_phases SET";
                $sql.= "  ppplcid = ".$plcid;
                $sql.= ", pptype = '".$phase['pptype']."'";
                $sql.= ", ppphaseid = ".$phase['ppphaseid'];
                $sql.= ", ppsort = ".$phase['ppsort'];
                $sql.= ", ppdays = ".$phase['ppdays'];
                $sql.= ", ppstartdate = ".$phase['ppstartdate'];
                $sql.= ", ppenddate = ".$phase['ppenddate'];
                $sql.= ", pptarget = ".$phase['pptarget'];
                $sql.= ", ppdone = '".$phase['ppdone']."'";
                $sql.= ", ppdonedate = ".$phase['ppdonedate'];
				db_insert($sql);
                //CHANGE STATUS
                $sql = "UPDATE ".$dbref."_kpi_plc_phases_temp SET tmpyn = 'A' WHERE tmpid = ".$temp;
				db_update($sql);
            }
        }
		//LOG UPDATE TO PLC
		$filename = logPLC($plcid,"current",$src);
		/*if($src=="new") {
			$log_trans = "Created";
		} else {
			$log_trans = "Updated";
		}
		$log_trans.= " Project Life Cycle PLC".$plcid." [<a target=_blank href=".$filename.">View here</a>].";*/
		$log_trans = plcLogText(($src=="new" ? "Created" : "Updated"),$plcid,$filename,"");
				$v = array(
					'fld'=>"PLC",
					'timeid'=>0,
					'text'=>$log_trans,
					'old'=>'',
					'new'=>'',
					'act'=>"U",
					'YN'=>"Y"
				);
		logChanges($section,$kpiid,$v,$filename);
		
		//UPDATE KPI_RESULTS
		if(checkIntRef($kpiid)) {
			$sqla = "SELECT kr_actual, kr_timeid FROM ".$dbref."_kpi_results WHERE kr_kpiid = $kpiid";
			$old_results = mysql_fetch_alls($sqla,"kr_timeid");
			foreach($tme as $ti) {
                $g = $target[$ti];
                $a = $actual[$ti];
				$old = isset($old_results[$ti]['kr_actual']) ? $old_results[$ti]['kr_actual'] : 0;
                if(strlen($a)>0 && is_numeric($a)) {
                    $sql = "UPDATE ".$dbref."_kpi_results 
							SET kr_actual = ".$a." 
							WHERE kr_kpiid = ".$kpiid." AND kr_timeid = ".$ti;
					$mar = db_update($sql);
					if(checkIntRef($mar)) {
					//IF CHANGES THEN LOG CHANGE TO KPI RESULT
							$v = array(
								'fld'=>"kr_actual",
								'timeid'=>$ti,
								'text'=>code("Updated Actual to '".$a."' from '".$old."' for time period ".$time[$ti]['display_short']." due to update in Project Life Cycle PLC".$plcid),
								'new'=>$a,
								'old'=>$old,
								'act'=>"U",
								'YN'=>"Y",
							);
						logChanges($section,$kpiid,$v,code($sql));
					}
                }
            }
        } else { //else invalid kpiid
            echo("<p>Error on kpiid validation - can't update results");
            $err = "Y";
        }
    } else {	//invalid plcid
        echo("<P>Error on plcid validation");
        $err = "Y";
    }
    
    if($err == "N") {
        echo("<p>PLC successfully updated.");
    }
}
else //ELSE IF APPREQ == "Y" => Approval is required
{
//    echo("approval required");
    if($tbl == "current")
    {
        $sql = "INSERT INTO ".$dbref."_kpi_plc_temp (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,tmptkid,tmpdate,tmpyn) ";
        $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as tmptid, ".$today." as tmpdt, 'P' as tyn FROM ".$dbref."_kpi_plc p2 WHERE plcid = ".$plcid;
        include("inc_db_con.php");
        $sql = "INSERT INTO ".$dbref."_kpi_plc_phases_temp (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,tmptkid,tmpdate,tmpyn) ";
        $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte, 'P' as tyn FROM ".$dbref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
        include("inc_db_con.php");
    }
    //CHANGE TEMP STATUS TO Y
        $plcnew = array();
        $sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'P' ORDER BY tmpid DESC";
//echo("<P>".$sql);
        //include("inc_db_con.php");
            $plcnew = mysql_fetch_one($sql);
        //mysql_close();
if(strlen($plcnew['tmpid'])>0 && is_numeric($plcnew['tmpid']))
{
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE ".$dbref."_kpi_plc_temp SET tmpyn = 'Y' WHERE tmpid = ".$plcnew['tmpid'];
//echo("<P>".$sql);
		db_update($sql);
//        include("inc_db_con.php");
}

//CREATE LOG PAGE
$filename = logPLC($plcid,"temp",$src);
		/*if($src=="new") {
			$log_trans = "Created";
		} else {
			$log_trans = "Updated";
		}
		$log_trans.= " Project Life Cycle PLC".$plcid." [<a target=_blank href=".$filename.">View here</a>].  This PLC has been submitted to KPI Admin for review.";*/
		$log_trans = plcLogText(($src=="new" ? "Created" : "Updated"),$plcid,$filename,"This PLC has been submitted to KPI Admin for review.");
				$v = array(
					'fld'=>"PLC",
					'timeid'=>0,
					'text'=>$log_trans,
					'old'=>'',
					'new'=>'',
					'act'=>"U",
					'YN'=>"Y"
				);
		logChanges($section,$kpiid,$v,$filename);

    //SEND AN EMAIL TO KPI ADMIN
    if($err == "N")
    {
        $tmpmnr = 0;
        $tmpplc = array();
        $sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE tmpyn = 'Y' AND tmpid = ".$plcnew['tmpid'];
        include("inc_db_con.php");
            $tmpmnr = mysql_num_rows($rs);
            $tmpplc =  mysql_fetch_array($rs);
        //mysql_close($con);
        if($tmpmnr>0)
        {
            $ref = $tmpplc['plcid'];
            $plcn = html_entity_decode($tmpplc['plcname'], ENT_QUOTES, "ISO-8859-1");
            $plckpi = $tmpplc['plckpiid'];
            if(strlen($plckpi)>0 && is_numeric($plckpi))
            {
                $kpirow = array();
				$sql2 = "SELECT kpi_value as kpivalue, kpi_id as kpiid, s.value as subtxt, d.value as dirtxt, d.id as dirid, s.id as subid
						FROM ".$dbref."_kpi
						INNER JOIN ".$dbref."_subdir s ON kpi_subid = s.id AND s.active = true
						INNER JOIN ".$dbref."_dir d ON d.id = s.dirid AND d.active = true
						WHERE kpi_id = $plckpi AND kpi_active = true";
                /*$sql2 = "SELECT k.kpivalue, k.kpiid, s.subtxt, d.dirtxt, d.dirid, s.subid 
						FROM ".$dbref."_kpi k
						, ".$dbref."_dir d
						, ".$dbref."_dirsub s 
						WHERE k.kpiid = ".$plckpi." 
						AND k.kpisubid = s.subid 
						AND s.subdirid = d.dirid 
						AND d.diryn = 'Y' 
						AND s.subyn = 'Y' 
						AND k.kpiyn = 'Y'";*/
                //include("inc_db_con2.php");
                    $kpirow = mysql_fetch_one($sql2);
                //mysql_close($con2);

                $plcdir = html_entity_decode($kpirow['dirtxt'], ENT_QUOTES, "ISO-8859-1");
                $plcsub = html_entity_decode($kpirow['subtxt'], ENT_QUOTES, "ISO-8859-1");
                $plckpiv = html_entity_decode($kpirow['kpivalue'], ENT_QUOTES, "ISO-8859-1");

                $sql = "SELECT DISTINCT tkemail FROM assist_".$cmpcode."_timekeep WHERE tkid <> '0000' AND tkstatus = 1 AND tkid IN ";
                $sql.= "(SELECT tkid FROM ".$dbref."_user_access WHERE active = true and kpi = true);";
                include("inc_db_con.php");
                //$to = "";
				$tkem = array();
                while($row = mysql_fetch_array($rs))
                {
                    $tkem[] = $row['tkemail'];
                    //if(strlen($to)>0 && strlen($tkem)>0) { $to.=","; }
                    //$to.=$tkem;
                }
				$to = implode(", ",$tkem);
                //mysql_close($con);
        $from = "no-reply@ignite4u.co.za";
        $subject = "Updated Project Life Cycle [PLC".$plcid."]";
        $message = "A Project Life Cycle [Ref: PLC".$plcid."] has been updated on SDBIP ".$modtxt." and requires approval.".chr(10);
        $message.= "Please log onto Ignite Assist to review the updated PLC.".chr(10).chr(10);
        $message.= "The PLC details are as follows:".chr(10);
        $message.= "Reference: ".$ref.chr(10);
        $message.= "Directorate: ".$plcdir.chr(10);
        $message.= "Sub-Directorate: ".$plcsub.chr(10);
        $message.= "PLC Name: ".$plcn.chr(10);
        $message.= "KPI: ".$plckpiv." (KPI Ref: ".$id_labels_all[$section].$plckpi.")".chr(10);
        $message.= "Updated by: ".$tkname;
        
        $header = "From: ".$from."\r\nReply-to: ".$from;
        mail($to,$subject,$message,$header);
//echo("<P>".$to);
//echo("<P>".$message);
        echo("<p>KPI Administrators have been notified.");

            }
        }
    }
}
$tsql = "";
$told = "";
//$trans = "Updated PLC ".$plcid." for KPI ".$id_labels_all[$section].$kpiid;
//include("inc_transaction_log.php");



?>
</body>

</html>
