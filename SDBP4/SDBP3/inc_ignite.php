<?php
require_once '../../inc_session.php';
require_once '../inc/inc.php';
require_once '../inc/inc_draw.php';
require_once 'lib/inc_SDBP3.php';
//require_once 'lib/inc2.php';

//CHECK YEAR COOKIE
$modref = strtoupper($_SESSION['ref']);
$modtxt = "2011/2012";
$cmpcode = $_SESSION['cc'];
$dbref = "assist_".strtolower($_SESSION['cc'])."_".strtolower($modref);
$id_labels_all = array(
	'KPI'=>"D",
	'TOP'=>"TL",
	'CAP'=>"CP",
	'CF'=>"CF",
	'RS'=>"RS"
);

$sql = "SELECT * FROM ".$dbref."_kpi_plc_setup";
$plcsetup = mysql_fetch_alls($sql,"id");
$setup_defaults = getModuleDefaults();
$plcsetup[1] = array('id'=>1,'value'=>$setup_defaults['PLC'],'active'=>1,'descrip'=>"Use Project Lifecycle?");

function logSDBP4User() {
	global $tkid;
	global $tkname;
	global $dbref;
	$req = $_REQUEST;
	$page = substr($_SERVER['PHP_SELF'],7,strlen($_SERVER['PHP_SELF']));
	$tkname = strFn("substr",code($tkname),0,99);
	$val = "";
	if(count($req)>0) {
		$vals = array();
		$keys = array_keys($req);
		foreach($keys as $k) {
			if($k!="PHPSESSID") {
				if(is_array($req[$k])) {
					$v = "array(";
					$v2 = array();
						foreach($req[$k] as $a => $b) {
							$v2[] = $a."=".$b;
						}
					$v.=implode(", ",$v2).")";
				} else {
					$v = $req[$k];
				}
				$vals[] = $k."=".$v;
			}
		}
		$val = code(implode(", ",$vals));
	}
	$sql = "INSERT INTO ".$dbref."_user_log (id,tkid,tkname,dttoday,page,request) VALUES (null,'$tkid','$tkname',now(),'$page','$val')";
	db_insert($sql);
}

logSDBP4User();
$result_settings = array(
	0 => array('id'=>0,'r'=>0,'value'=>"KPI Not Yet Measured"	,'text'=>"N/A"	,'style'=>"result0",'color'=>"#999999", 'glossary'=> "KPIs with no targets or actuals in the selected period."),
	1 => array('id'=>1,'r'=>1,'value'=>"KPI Not Met"			,'text'=>"R"	,'style'=>"result1",'color'=>"#CC0001", 'glossary'=> "0% >= Actual/Target < 75%"),
	2 => array('id'=>2,'r'=>2,'value'=>"KPI Almost Met"			,'text'=>"O"	,'style'=>"result2",'color'=>"#FE9900", 'glossary'=> "75% >= Actual/Target < 100%"),
	3 => array('id'=>3,'r'=>3,'value'=>"KPI Met"				,'text'=>"G"	,'style'=>"result3",'color'=>"#009900", 'glossary'=> "Actual/Target = 100%"),
	4 => array('id'=>4,'r'=>4,'value'=>"KPI Well Met"			,'text'=>"G2"	,'style'=>"result4",'color'=>"#005500", 'glossary'=> "100% > Actual/Target < 150%"),
	5 => array('id'=>5,'r'=>5,'value'=>"KPI Extremely Well Met"	,'text'=>"B"	,'style'=>"result5",'color'=>"#000077", 'glossary'=> "Actual/Target >= 150%"),
);
$base = array("manage","dept","edit");
$page_id = "edit";
$section = "KPI";
$get_time = array(1,2,3,4,5,6,7,8,9,10,11,12);
$get_open_time = false;
$time = getTime($get_time,$get_open_time);

function plcLogText($start,$plcid,$filename,$end) {
	return $start." Project Life Cycle PLC".$plcid." [<a id=".$filename.">View here</a>].".$end;
}
//$today = 1289833200;    //set date to 11 Nov 2010 15h00 for dev purposes
/*

    $munlists = array(
        'munkpa'=>array('name'=>"Municipal KPAs",'id'=>"munkpa",'code'=>"Y",'len'=>150,'fld'=>"code"),
        'wards'=>array('name'=>"Wards",'id'=>"wards",'code'=>"Y",'len'=>50,'fld'=>"numval"),
        'area'=>array('name'=>"Areas",'id'=>"area",'code'=>"N",'len'=>50,'fld'=>"code"),
        'prog'=>array('name'=>"Objective&#047;Programme",'id'=>"prog",'code'=>"N",'len'=>150,'fld'=>"dirid"),
        'fundsource'=>array('name'=>"Funding Source",'id'=>"fundsource",'code'=>"N",'len'=>50,'fld'=>"code"),
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"N",'len'=>150,'fld'=>"code")
    );
    
/*    $fixedlists = array(
        'natkpa'=>array('name'=>"National KPAs",'id'=>"natkpa",'code'=>"Y"),
        'tas'=>array('name'=>"TAS Key Focus Area",'id'=>"tas",'code'=>"Y"),
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"Y"),
        'fundsource'=>array('name'=>"Funding Source",'id'=>"fundsource",'code'=>"N")
    );*//*

    $ignitelists = array(
        'kpitargettype'=>array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y"),
        'kpicalctype'=>array('name'=>"Calculation Type",'id'=>"kpicalctype",'code'=>"Y"),
        'ktype'=>array('name'=>"KPI Type",'id'=>"kpitype",'code'=>"N")
    );
    
    $dlist = array(
        'munkpa'=>array('name'=>"Municipal KPAs",'id'=>"munkpa",'code'=>"Y",'len'=>150,'fld'=>"code",'yn'=>"Y",'headfield'=>"munkpa",'kpi'=>"kpimunkpaid"),
        'prog'=>array('name'=>"Objective&#047;Programme",'id'=>"prog",'code'=>"N",'len'=>150,'yn'=>"Y",'headfield'=>"prog",'kpi'=>"kpiprogid"),
        'natkpa'=>array('name'=>"National KPAs",'id'=>"natkpa",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"natkpa",'kpi'=>"kpinatkpaid"),
        'tas'=>array('name'=>"TAS Key Focus Area",'id'=>"tas",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"tas",'kpi'=>"kpitasid"),
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"N",'yn'=>"Y",'headfield'=>"gfs",'kpi'=>"kpigfsid"),
        //'kpicalctype'=>array('name'=>"KPI Calculation Type",'id'=>"kpicalctype",'code'=>"Y",'yn'=>"Y",'headfield'=>"kpicalctype",'kpi'=>"kpicalctypeid",'fld'=>"code"),
        'kpitargettype'=>array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpitargettype",'kpi'=>"kpitargettypeid"),
        'kpistratop'=>array('name'=>"S/O/P",'id'=>"stratop",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpistratop",'kpi'=>"kpistratop"),
        'kpiriskrate'=>array('name'=>"Risk Rating",'id'=>"riskrate",'code'=>"N",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpiriskrate",'kpi'=>"kpiriskrate"),
        'ktype'=>array('name'=>"KPI Type",'id'=>"kpitype",'code'=>"N",'yn'=>"Y",'headfield'=>"ktype",'kpi'=>"kpitypeid")
    );

*/
?>