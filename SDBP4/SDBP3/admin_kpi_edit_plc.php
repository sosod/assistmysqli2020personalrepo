<?php
    include("inc_ignite.php");

$reqd = array();

function appYN($c,$yn) {
    global $reqd;
    if($yn == "R")
    {
        echo("Required"."<input type=hidden name=app[] id=app".$c." value=Y>");
        $reqd[$c] = $c;
    }
    else
    {
        echo("<select size=1 name=app[] id=app".$c." onchange=\"usePhase(this,".$c."); targetChange(".$c.");\"><option ");
        if($yn != "N") {    echo("selected");    }
        echo(" value=\"Y\">Yes</option><option ");
        if($yn == "N") {    echo("selected");    }
        echo(" value=\"N\">No</option></select>");
    }
}
$procstart = 0;
$procend = 0;
$add1 = 0;
$add2 = 0;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php include("inc_head_msie.php"); ?>
<script type=text/javascript src="lib/inc_plc.js"></script>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1><?php echo $_SESSION['modtext']; ?> >> Manage >> Project Life Cycle</h1>
<!-- CONTAINER -->
<table class=noborder><tr><td class=noborder>

<?php
$kpiid = $_REQUEST['kpiid'];
$dirid = 0;
$src = "";
$typ = "";
$subid = 0;
$plcid = isset($_REQUEST['plcid']) ? $_REQUEST['plcid'] : 0;
$tbl = isset($_REQUEST['tbl']) ? $_REQUEST['tbl'] : "current";   //table source for phases
switch($tbl) {
    case "temp":
        $tble = "phases_temp";
        break;
    case "current":
    default:
        $tble = "phases";
        break;
}
if(strlen($tble)==0) { $tble = "phases"; }
if(checkIntRef($kpiid)) {
    $sql = "SELECT * FROM ".$dbref."_kpi WHERE kpi_id = ".$kpiid;
	$kpirow = mysql_fetch_one($sql);
} else {
	die("<h2 class=red>Error</h2><p>An error has occurred.  Please go back and try again.</p>");
}
$plcrow = array();
$phase = array();
if(checkIntRef($plcid)) {
    $sql = "SELECT * FROM ".$dbref."_kpi_plc WHERE plcid = ".$plcid;
		$plcrow = mysql_fetch_one($sql);
    $sql = "SELECT * FROM ".$dbref."_kpi_plc_".$tble." WHERE ppplcid = ".$plcid;
		$phase = mysql_fetch_alls2($sql,"pptype","ppphaseid");
} else {
    $sql = "SELECT * FROM ".$dbref."_kpi_plc WHERE plckpiid = ".$kpiid." ORDER BY plcid DESC";
		$plcrow = mysql_fetch_one($sql);
	$plcid = isset($plcrow['plcid']) ? $plcrow['plcid'] : 0;
    if(checkIntRef($plcid)) {
        $sql = "SELECT * FROM ".$dbref."_kpi_plc_".$tble." WHERE ppplcid = ".$plcid;
			$phase = mysql_fetch_alls2($sql,"pptype","ppphaseid");
	}
}

if(checkIntRef($plcid)) {
    $plcstartdate = date("Y-m-d",$plcrow['plcstartdate']);
    $plcenddate = date("Y-m-d",$plcrow['plcenddate']);
    $plcname = $plcrow['plcname'];
    $plcdescrip = $plcrow['plcdescription'];
    $proccate = $plcrow['plcproccate'];
    if(strlen($proccate)!=1) { $proccate = 1; }
    $plctype = "edit";
	$src = "existing";
} else {
    $plcstartdate = date("Y-m-d",$today);
    $plcenddate = "";
    $plcname = $kpirow['kpi_value'];
    $plcdescrip = $kpirow['kpi_unit'];
    $proccate = 1;
    $plctype = "new";
	$src = "new";
}
?>
<p><i>Note: Any changes that affect KPI targets must be approved by a KPI administrator before they can be accepted.</i></p>
<h2>Details</h2>
<form name=plc id=editp method=post action=admin_kpi_edit_plc_process.php onsubmit="return Validate();">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=tbl value="<?php echo($tbl); ?>">
<table>
	<tr>
		<th class=left>KPI:</th>
		<td><?php echo $kpirow['kpi_value']." [".$id_labels_all[$section].$kpiid."]"; ?></td>
	</tr>
	<tr>
		<th class=left>PLC Ref:</th>
		<td><?php echo checkIntRef($plcid) ? "PLC".$plcid : "New"; ?></td>
	</tr>
	<tr>
		<th class=left>Project Name:</b></th>
		<td><input type="text" name="plcprojectname" size="50" value="<?php echo($plcname); ?>" maxlength=100 /></td>
	</tr>
	<tr>
		<th class=left>Project Description:&nbsp;</b></th>
		<td><input type="text" name="plcprojectdescription" size="75" value="<?php echo($plcdescrip); ?>" maxlength=150 /></td>
	</tr>
	<tr>
		<th class=left>Start Date:</b></th>
		<td><input type="text" name="plcstartdate" size="20" value="<?php echo($plcstartdate); ?>" readonly="readonly" id=plcstartdate class=datepicker onchange="startDate(0);"></td>
	</tr>
	<tr>
		<th class=left>End Date:</b></th>
		<td><input type="text" name="plcenddate" size="20" value="<?php echo($plcenddate); ?>" readonly="readonly" id=plcenddate></td>
	</tr>
	<tr>
		<th class=left>Update KPI Results?:</b></th>
		<td><select name=upkpi id=upk><option selected value=Y>Yes</option><option value=N>No</option></select></td>
	</tr>
</table>
<p style="margin: 0 0 0 0;">&nbsp;</p>
<h2>Phases</h2>
<table>
	<tr>
		<th height=27 colspan="2" ><b>Project Phase</b></th>
		<th width=100  class=center><b>Use Phase?</b></th>
		<th width=100 ><b>Calendar<br>Days</b></th>
		<th ><b>Start<br>Date</b></th>
		<th ><b>End<br>Date</b></th>
		<th ><b>% Target<br>of Project</b></th>
		<th ><b>Completed?</b></th>
		<th ><b>Date of<br>Completion</b></th>
	</tr>
<?php
    $c=1;
    $sql = "SELECT * FROM ".$dbref."_list_plc_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
	$std_phases = mysql_fetch_all($sql);
    //include("inc_db_con.php");
	//$rs = getRS($sql);
        //while($row = mysql_fetch_array($rs))
		foreach($std_phases as $row)
        {
            $prow = array();
            switch($row['type'])
            {
    case "A":
    $no1 = $c + 1;
//CUSTOM PHASES
    $c2 = $c;
    $add1 = $c;
    $fc=0;
    $phaseC = isset($phase['C']) ? $phase['C'] : array();
	$sql2 = "SELECT * FROM ".$dbref."_list_plc_custom WHERE yn = 'Y' ORDER BY value";
	$cust_phases = mysql_fetch_all($sql2);
    foreach($phaseC as $pC)
    {
        if($fc==9)
        {
            $s = 28;
            $w = 25;
        }
        else
        {
            $s = 30;
            $w = 18;
        }
?>
	<tr>
        <?php if($c2==$c) { ?>
		<td valign=top rowspan=10 ><b><?php echo($row['value']); ?>:</b></td>
		<?php } ?>
		<td ><i><?php echo($fc+1); ?>.</i> <input type=hidden name=phasetype[] value=C id=ptype<?php echo($c); ?>>
            <select name=phaseid[] onchange="changePhase(this,<?php echo($c); ?>);" id=phase<?php echo($c); ?>>
                <option selected value=X>-- SELECT --</option>
                <option value=NEW>-- Add new --</option>
                <?php
					foreach($cust_phases as $row2)
                    {
                        echo("<option ");
                        if($pC['ppphaseid']==$row2['id']) { echo(" selected "); }
                        echo(" value=".$row2['id'].">".$row2['value']."</option>");
                    }
                $ppdone = "";
                $ppdonedate = "";
                if($pC['ppdone']=="Y")
                {
                    $ppdone = " checked ";
                    if($pC['ppdonedate']>0)
                    {
                        $ppdonedate = date("Y-m-d",$pC['ppdonedate']);
                    } else {
                        $ppdonedate = "";
                    }
                } else {
                    $ppdone = "";
                    $ppdonedate = "";
                }
                ?>
            </select><?php echo("<span id=new".$c."><br><input type=text name=custom[] onchange=\"newPhase(this,".$c.")\" id=cust".$c." size=".$s." style=\"margin-left: ".$w."\" maxlength=100></span>"); ?>
        </td>
		<td  class=center><?php appYN($c,"Y"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=\"".$pC['ppdays']."\" onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"".date("Y-m-d",$pC['ppstartdate'])."\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"".date("Y-m-d",$pC['ppenddate'])."\" readonly=\"readonly\" id=end".$c." class=datepicker>"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"".$pC['pptarget']."\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
		<td  class=center><?php echo("<input type=\"checkbox\" ".$ppdone." name=\"donechk[]\" size=\"5\" value=\"Y\" id=done".$c." onclick=\"doneChk(".$c.")\"><input type=hidden name=\"done[]\" value=\"".(isset($pC['ppdone']) ? $pC['ppdone'] : "")."\" id=doneb".$c.">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"donedate[]\" readonly=readonly class=datepicker size=\"10\" value=\"".$ppdonedate."\" id=donedate".$c." onchange=\"doneDateChange(".$c.")\">"); ?></td>
	</tr>
<?php
        $c++;
        $fc++;
    }

    for($f=$fc;$f<10;$f++)
    {
        if($f==9)
        {
            $s = 28;
            $w = 25;
        }
        else
        {
            $s = 30;
            $w = 18;
        }
?>
	<tr>
        <?php if($c2==$c) { ?>
		<td valign=top rowspan=10 ><b><?php echo($row['value']); ?>:</b></td>
		<?php } ?>
		<td ><i><?php echo($f+1); ?>.</i> <input type=hidden name=phasetype[] value=C id=ptype<?php echo($c); ?>>
            <select name=phaseid[] onchange="changePhase(this,<?php echo($c); ?>);" id=phase<?php echo($c); ?>>
                <option selected value=X>-- SELECT --</option>
                <option value=NEW>-- Add new --</option>
                <?php
					foreach($cust_phases as $row2)
                    {
                        echo("<option value=".$row2['id'].">".$row2['value']."</option>");
                    }
                $ppdone = "";
                $ppdonedate = "";
                ?>
            </select><?php echo("<span id=new".$c."><br><input type=text name=custom[] onchange=\"newPhase(this,".$c.")\" id=cust".$c." size=".$s." style=\"margin-left: ".$w."\" maxlength=100></span>"); ?>
        </td>
		<td  class=center><?php if($f==0) { appYN($c,"Y"); } else {  appYN($c,"N");  } ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=1 onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"\" readonly=\"readonly\" id=end".$c." class=datepicker>"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"0\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
		<td  class=center><?php echo("<input type=\"checkbox\" ".$ppdone." name=\"donechk[]\" size=\"5\" value=\"Y\" id=done".$c." onclick=\"doneChk(".$c.")\"><input type=hidden name=\"done[]\" value=\"".(isset($pC['ppdone']) ? $pC['ppdone'] : "")."\" id=doneb".$c.">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"donedate[]\" readonly=readonly class=datepicker size=\"10\" value=\"".$ppdonedate."\" id=donedate".$c." onchange=\"doneDateChange(".$c.")\">"); ?></td>
	</tr>
<?php
        $c++;
    }
    $add2 = $c;
    $no2 = $c;
    break;
            case "P":
            //PROCUREMENT PHASES
                $c2 = $c;
                $days = array();
                $sql2 = "SELECT * FROM ".$dbref."_list_plc_proc WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
                //include("inc_db_con2.php");
                //$mnr2 = mysql_num_rows($rs2);
				$proc_phases = mysql_fetch_all($sql2);
				$mnr2 = count($proc_phases);
                $r=0;
                $procstart = $c;
                    //while($row2 = mysql_fetch_array($rs2))
					foreach($proc_phases as $row2)
                    {
						if(isset($phase[$row['type']][$row2['id']])) {
							$prow = $phase[$row['type']][$row2['id']];
                            $pcal = $prow['ppdays'];
                            $pstart = date("Y-m-d",$prow['ppstartdate']);
                            $pend = date("Y-m-d",$prow['ppenddate']);
                            $ptarg = $prow['pptarget'];
                            $pyn = "Y";
							if($prow['ppdone']=="Y")
							{
								$ppdone = " checked ";
								if($prow['ppdonedate']>0) {
									$ppdonedate = date("Y-m-d",$prow['ppdonedate']);
								} else {
									$ppdonedate = "";
								}
							} else {
								$ppdone = "";
								$ppdonedate = "";
							}
                        }
                        else
                        {
                            $pcal = 1;
                            $pstart = "";
                            $pend = "";
                            $ptarg = 0;
                            if(count($plcrow)>1) { $pyn = "N"; } else { $pyn = $row['yn']; }
							$ppdone = "";
							$ppdonedate = "";
                        }
                        $r++;
                        $days[1][$c] = $row2['days1'];
                        $days[2][$c] = $row2['days2'];
                        $days[3][$c] = $row2['days3'];
                        $days[4][$c] = $row2['days4'];
                        if(strlen($pstart)==0)
                        {
                            $pcal = $row2['days'.$proccate];
                        }
						if($row2['yn'] == "R") { $pyn = "R"; }
?>
	<tr>
        <?php if($c2==$c) { ?>
		<td valign=top rowspan=<?php echo($mnr2); ?> ><b><?php echo($row['value']); ?>:
<?php if($plcsetup[4]['value']!="N") { ?>
        <br>
        <i>Project Category: <select name=proccate onchange="procCate(this)">
        <?php
        for($f=1;$f<5;$f++) {
            echo("<option ");
            if($proccate == $f) { echo("selected"); }
            echo(" value=".$f.">".$f."</option>");
         } ?>
        </select></i><?php } else { ?><input type=hidden name=proccate value=1><?php } ?></b></td>
        <?php } ?>
		<td ><i><?php echo($row2['value']); ?>:</i></td>
		<td  class=center><?php appYN($c,$pyn); ?><input type=hidden name=phaseid[] value="<?php echo($row2['id']); ?>"><input type=hidden name=phasetype[] value="<?php echo($row['type']); ?>" id="ptype<?php echo($c); ?>"></td>
		<?php if($plcsetup[5]['value']!="N") { ?>
		<td  class=center><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=\"".$pcal."\" onchange=\"startDate(".$c.")\">"); ?></td>
		<?php } else { ?>
		<td  class=center><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=\"".$pcal."\" onchange=\"startDate(".$c.")\" readonly=readonly style=\"background-color: #dddddd;\">"); ?></td>
		<?php } ?>
		<td  class=center><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"".$pstart."\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"".$pend."\" readonly=\"readonly\" id=end".$c." class=datepicker>"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"".$ptarg."\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
		<td  class=center><?php echo("<input type=\"checkbox\" ".$ppdone." name=\"donechk[]\" size=\"5\" value=\"Y\" id=done".$c." onclick=\"doneChk(".$c.")\"><input type=hidden name=\"done[]\" value=\"".(isset($prow['ppdone']) ? $prow['ppdone'] : "")."\" id=doneb".$c.">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"donedate[]\" readonly=readonly class=datepicker size=\"10\" value=\"".$ppdonedate."\" id=donedate".$c." onchange=\"doneDateChange(".$c.")\">"); ?></td>
	</tr>
<?php
                        if($r<$mnr2) { $c++; }
                    }
                //mysql_close();
                $procend = $c;
                break;
                default:
                //STANDARD PHASES WHERE TYPE = S
                $prow = isset($phase[$row['type']][$row['id']]) ? $phase[$row['type']][$row['id']] : array();
                if(count($prow)>1)
                {
                    $pcal = $prow['ppdays'];
                    $pstart = date("Y-m-d",$prow['ppstartdate']);
                    $pend = date("Y-m-d",$prow['ppenddate']);
                    $ptarg = $prow['pptarget'];
                    $pyn = "Y";
					if($prow['ppdone']=="Y")
					{
						$ppdone = " checked ";
						if($prow['ppdonedate']>0)
						{
							$ppdonedate = date("Y-m-d",$prow['ppdonedate']);
						} else {
							$ppdonedate = "";
						}
					} else {
						$ppdone = "";
						$ppdonedate = "";
					}
                }
                else
                {
                    $pcal = 1;
                    $pstart = "";
                    $pend = "";
                    $ptarg = 0;
                    if(count($plcrow)>1) { $pyn = "N"; } else { $pyn = $row['yn']; }
                    $ppdone = "";
                    $ppdonedate = "";
                }
                if($row['yn'] == "R") { $pyn = "R"; }
?>
	<tr>
		<td colspan="2" ><b><?php echo($row['value']); ?>:</b><input type=hidden name=phaseid[] value="<?php echo($row['id']); ?>"><input type=hidden name=phasetype[] value="<?php echo($row['type']); ?>" id="ptype<?php echo($c); ?>"></td>
		<td  class=center><?php appYN($c,$pyn); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=".$pcal." onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"".$pstart."\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"".$pend."\" readonly=\"readonly\" id=end".$c." class=datepicker onchange=\"endDate(".$c.")\">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"".$ptarg."\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
		<td  class=center><?php echo("<input type=\"checkbox\" ".$ppdone." name=\"donechk[]\" size=\"5\" value=\"Y\" id=done".$c." onclick=\"doneChk(".$c.")\"><input type=hidden name=\"done[]\" value=\"".(isset($prow['ppdone']) ? $prow['ppdone'] : "")."\" id=doneb".$c.">"); ?></td>
		<td  class=center><?php echo("<input type=\"text\" name=\"donedate[]\" readonly=readonly class=datepicker size=\"10\" value=\"".$ppdonedate."\" id=donedate".$c." onchange=\"doneDateChange(".$c.")\">"); ?></td>
	</tr>
<?php
                break;
            }
            if($row['type']!="A") {            $c++;    }
        }
    //mysql_close();
?>
	<tr>
		<td height=27 colspan="6" class=right ><b>Total Target % of Project:</b></td>
		<td  class=center><label id=tartot style="font-weight: bold;">0</label> %</td>
		<td colspan=2>&nbsp;</td>
	</tr>
</table>
<input type=hidden name=c id=c value=<?php echo($c); ?>>
<script type=text/javascript>
<?php echo("var c = ".$c.";"); ?>
<?php echo("var cust1 = ".$add1.";"); ?>
<?php echo("var cust11 = ".$add2.";"); ?>
document.getElementById('plcenddate').style.color = "777777";
var apyn;
var doctype = "edit";
<?php
echo(chr(10)."var procstart = ".$procstart.";");
$procend++;
echo(chr(10)."var procend = ".$procend.";");
echo(chr(10)."var days = new Array();");
$dayskeys = array_keys($days);
$d1 = 1;
foreach($days as $d)
{
    echo(chr(10)."days[".$d1."] = new Array(".implode($d,",").");");
    $d1++;
}
echo(chr(10)."var req = new Array(".implode($reqd,",").");");
for($e=1;$e<$c;$e++)
{
	
    echo(chr(10)."var apptarg = document.getElementById('app".$e."');");
    echo(chr(10)."usePhase(apptarg,".$e.");");
}
echo(chr(10));
?>
</script>
<p style="font-size: 1pt; line-height: 1pt;">&nbsp;</p>
<table width=100%>
    <tr>
        <td class=center><input type=submit value="Submit PLC" id=subbut class=isubmit> <input type=reset></td>
    </tr>
</table>
</form>
<script type="text/javascript">
$(function(){
    $('.datepicker').datepicker({
        showOn: 'both',
        buttonImage: '/library/jquery/css/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd',
        changeMonth:true,
        changeYear:true
    });
	$(".datepicker").each(function() {
		var i = $(this).attr("id");
		i = i.substr(0,4);
		if(i=="done") {
			$(this).datepicker("option","maxDate",'0d');
		}
	});
});
</script>
<script type=text/javascript>
var targ = document.getElementById('app1');
targetChange(1);
startDate(0);
doneChange(-1);
</script>
<!-- END CONTAINER -->
</td></tr></table>
</body>

</html>
