<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1><?php echo $_SESSION['modtext']; ?> >> Manage >> Project Life Cycle</h1>
<p>&nbsp;</p>
<p>Processing...</p>
<?php
//arrPrint($_REQUEST);

$err = "N";
$dirid = $_REQUEST['dirid'];
$subid = $_REQUEST['subid'];
$typ = $_REQUEST['typ'];
$src = $_REQUEST['src'];
$plcid = $_REQUEST['plcid'];
$upkpi = $_REQUEST['upkpi'];
$tbl = $_REQUEST['tbl'];
if(strlen($tbl)==0) { $tbl = "current"; }
//FORM DETAILS
$kpiid = $_REQUEST['kpiid'];
$pname = $_REQUEST['plcprojectname'];
$pdescrip = $_REQUEST['plcprojectdescription'];
$pstartdate = $_REQUEST['plcstartdate'];
$penddate = $_REQUEST['plcenddate'];
$proccate = $_REQUEST['proccate'];
//FORM ARRAYS
$phaseid = $_REQUEST['phaseid'];
$phasetype = $_REQUEST['phasetype'];
$appyn = $_REQUEST['app'];
$cal = $_REQUEST['cal'];
$startdate = $_REQUEST['startdate'];
$enddate = $_REQUEST['enddate'];
$target = $_REQUEST['target'];
$custom = $_REQUEST['custom'];
$done = $_REQUEST['done'];
$donedate = $_REQUEST['donedate'];
//ARRAY COUNTERS
$pi = 0;    //phaseid
$pt = 0;    //phasetype
$cl = 0;    //cal
$sd = 0;    //startdate
$ed = 0;    //enddate
$tg = 0;    //target
$cu = 0;    //custom
$sort = 1;

$err = "N";
$err2 = "N";

if($tbl == "temp")
{
    $tabl = "phases_temp";
}
else
{
    $tabl = "phases";
}
/*
echo("<P>plcid-".$plcid);
echo("<P>kpiid-".$kpiid);
echo("<P>tbl-".$tbl);
echo("<P>upkpi-".$upkpi);


echo("<P>pid-");
print_r($phaseid);
echo("<P>pt-");
print_r($phasetype);
echo("<P>app-");
print_r($appyn);
echo("<P>cal-");
print_r($cal);
echo("<P>sd-");
print_r($startdate);
echo("<P>ed-");
print_r($enddate);
echo("<P>tar-");
print_r($target);
echo("<P>cust-");
print_r($custom);
echo("<P>done-");
print_r($done);
echo("<P>donedate-");
print_r($donedate);
*/
//VALIDATE KPIID
if(checkIntRef($kpiid))
{
//PROCESS
    //IF UPKPI == N -> Don't update KPI results
    if($upkpi == "N")
    {
        //UPDATE PLC TABLE
        //Backup old data
        if(checkIntRef($plcid))
        {
            $sql = "INSERT INTO ".$dbref."_kpi_plc_old (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,oldtkid,olddate) ";
            $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as oldtid, ".$today." as olddt FROM ".$dbref."_kpi_plc p2 WHERE plcid = ".$plcid;
            include("inc_db_con.php");
        }
        //Update with new plc data
        $pname = htmlentities($pname,ENT_QUOTES,"ISO-8859-1");
        $pdescrip = htmlentities($pdescrip,ENT_QUOTES,"ISO-8859-1");
        if(strlen($proccate)!=1 || !is_numeric($proccate) || $proccate < 1 || $proccate > 4)    { $proccate = 1; }
        $psd = explode("-",$pstartdate);    //STARTDATE
        $psd[0] = $psd[0] * 1;  //YEAR
        $psd[1] = $psd[1] * 1;  //MONTH
        $psd[2] = $psd[2] * 1;  //DAY
        $plcstart = mktime(12,0,0,$psd[1],$psd[2],$psd[0]);
        $ped = explode("-",$penddate);    //ENDDATE
        $ped[0] = $ped[0] * 1;  //YEAR
        $ped[1] = $ped[1] * 1;  //MONTH
        $ped[2] = $ped[2] * 1;  //DAY
        $plcend = mktime(12,0,0,$ped[1],$ped[2],$ped[0]);
        //CREATE KPI-PLC RECORD
        if(checkIntRef($plcid))
        {
            $sql = "UPDATE ".$dbref."_kpi_plc SET";
			$src = "old";
        }
        else
        {
			$src = "new";
            $sql = "INSERT INTO ".$dbref."_kpi_plc SET";
        }
        $sql.= "  plcname = '".$pname."'";
        $sql.= ", plcdescription = '".$pdescrip."'";
        $sql.= ", plckpiid = ".$kpiid;
        $sql.= ", plcstartdate = ".$plcstart;
        $sql.= ", plcenddate = ".$plcend;
        $sql.= ", plcproccate = ".$proccate;
        $sql.= ", plcyn = 'Y'";
        if(checkIntRef($plcid))
        {
            $sql.= " WHERE plcid = ".$plcid;
        }
        include("inc_db_con.php");
//        echo("<P>plc-sql:".$sql);
        //GET KPI-PLC ID
        if(!checkIntRef($plcid)) {
            $plcid = mysql_insert_id();
        }

        if(checkIntRef($plcid)) {
            //UPDATE PHASES
            //Copy all current phases to_phases_old
            $sql = "INSERT INTO ".$dbref."_kpi_plc_phases_old (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,oldtkid,olddate) ";
            $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte FROM ".$dbref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
            include("inc_db_con.php");
//            echo("<P>COPY-".$sql);
            //Delete all current phases
            $sql = "DELETE FROM ".$dbref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
            include("inc_db_con.php");
//            echo("<P>DEL-".$sql);
            //Upload new phases (same process as kpi_add_plc_process.php)
            include("inc_plc_process.php");
            if($err != "Y") {
                echo("<P>Project Life Cycle update complete.</p>");
            }
        }
        else    //validate plcid
        {
            //ERROR
            echo("<P>Error on plcid validation");
            $err2 = "Y";
        }
    }
    //ELSE (UPKPI == N) -> Update KPI results
    else
    {
        //if TBL == current -> First time through the edit page & not a reject of previous edit
            //insert into phases_temp
        //else
            //update phases_temp
        //PLC
        if(checkIntRef($kpiid) && strlen($pstartdate) > 0 && strlen($penddate) > 0)
        {
            //FORMAT KPI-PLC VARIABLES
            $pname = htmlentities($pname,ENT_QUOTES,"ISO-8859-1");
            $pdescrip = htmlentities($pdescrip,ENT_QUOTES,"ISO-8859-1");
            if(strlen($proccate)!=1 || !is_numeric($proccate) || $proccate < 1 || $proccate > 4)    { $proccate = 1; }
            $psd = explode("-",$pstartdate);    //STARTDATE
            $psd[0] = $psd[0] * 1;  //YEAR
            $psd[1] = $psd[1] * 1;  //MONTH
            $psd[2] = $psd[2] * 1;  //DAY
            $plcstart = mktime(12,0,0,$psd[1],$psd[2],$psd[0]);
            $ped = explode("-",$penddate);    //ENDDATE
            $ped[0] = $ped[0] * 1;  //YEAR
            $ped[1] = $ped[1] * 1;  //MONTH
            $ped[2] = $ped[2] * 1;  //DAY
            $plcend = mktime(12,0,0,$ped[1],$ped[2],$ped[0]);
            //CREATE KPI-PLC RECORD
            if(checkIntRef($plcid))
            {
                $sql = "INSERT INTO ".$dbref."_kpi_plc_temp SET";
                $sql.= "  plcid = ".$plcid.", ";
                $plctype = "old";
            }
            else
            {
                $sql = "INSERT INTO ".$dbref."_kpi_plc SET";
                $plctype = "new";
            }
            $sql.= " plcname = '".$pname."'";
            $sql.= ", plcdescription = '".$pdescrip."'";
            $sql.= ", plckpiid = ".$kpiid;
            $sql.= ", plcstartdate = ".$plcstart;
            $sql.= ", plcenddate = ".$plcend;
            $sql.= ", plcproccate = ".$proccate;
            $sql.= ", plcyn = 'P'";
            if(checkIntRef($plcid))
            {
                $sql.= ", tmptkid = '".$tkid."'";
                $sql.= ", tmpdate = ".$today;
                $sql.= ", tmpyn = 'P'";
            }
            //echo("<P>".$sql);
            include("inc_db_con.php");
            if($plctype == "new") {
				$plcid = mysql_insert_id();
                //$plcid = db_insert($sql);
                $tabl = "phases";
            } else {
				//db_update($sql);
                $tabl = "phases_temp";
                $sql = "DELETE FROM ".$dbref."_kpi_plc_phases_temp WHERE ppplcid = ".$plcid;
                include("inc_db_con.php");
            }
        }
        //PHASES
        include("inc_plc_process.php");
        //change tbl to "temp"
        if($plctype == "new")
        {
            $tbl = "current";
        }
        else
        {
            $tbl = "temp";
        }
        //transfer to plc_confirm.php
                echo("<form id=step3 method=post action=\"admin_kpi_edit_plc_confirm.php\">");
                echo("<input type=hidden name=kpiid value=\"".$kpiid."\">");
                echo("<input type=hidden name=plcid value=\"".$plcid."\">");
                echo("<input type=hidden name=dirid value=\"".$dirid."\">");
                echo("<input type=hidden name=subid value=\"".$subid."\">");
                echo("<input type=hidden name=typ value=\"".$typ."\">");
                echo("<input type=hidden name=src value=\"".$src."\">");
                echo("<input type=hidden name=tbl value=\"".$tbl."\">");
                echo("</form>");
                echo("<script type=text/javascript>");
                echo("document.forms['step3'].submit();");
                echo("</script>");
    }
}
else    //validate kpiid
{
    //ERROR
    echo("<P>Error on kpiid validation");
    $err2 = "Y";
}
       /* $tsql = "";
        $told = "";
        $trans = "Updated KPI Plc ".$plcid." for KPI ".$kpiid;
        include("inc_transaction_log.php");*/
?>
<p>&nbsp;</p>
</body>

</html>
