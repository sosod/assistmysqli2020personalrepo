<?php
    include("inc_ignite.php");
    $targets = array();
?>	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<link rel="stylesheet" href="../inc/main.css" type="text/css">
<?php include("inc_head_msie.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1><?php echo $_SESSION['modtext']; ?> >> Manage >> Project Life Cycle</h1>

<!-- CONTAINER TABLE -->
<table class=noborder><tr><td class=noborder>


<?php
$kpiid = $_REQUEST['kpiid'];
$plcid = $_REQUEST['plcid'];
$dirid = $_REQUEST['dirid'];
$subid = $_REQUEST['subid'];
$typ = $_REQUEST['typ'];
$src = $_REQUEST['src'];
$tbl = $_REQUEST['tbl'];
if($tbl == "temp")
{
    $plctabl = "plc_temp";
    $tabl = "phases_temp";
}
else
{
    $plctabl = "plc";
    $tabl = "phases";
}

//echo($plctabl."-".$tabl);

$sql = "SELECT * FROM ".$dbref."_kpi WHERE kpi_id = ".$kpiid;
//include("inc_db_con.php");
    $kpirow = mysql_fetch_one($sql);
//mysql_close();
$sql = "SELECT d.value as dirtxt, s.value as subtxt FROM ".$dbref."_dir d INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.id = ".$kpirow['kpi_subid'];
//include("inc_db_con.php");
    $dirrow = mysql_fetch_one($sql);
//mysql_close();

$sql = "SELECT * FROM ".$dbref."_kpi_".$plctabl." WHERE plcid = ".$plcid;
if($plctabl=="plc_temp") { $sql.= " ORDER BY tmpid DESC"; }
    $plcrow = mysql_fetch_one($sql);
$tmpid = array();
?> 
<h2>Details</h2>
<table>
	<tr>
		<th class=left>Project Ref:</th>
		<td><?php echo("PLC".$plcrow['plcid']); ?></td>
	</tr>
	<tr>
		<th class=left>Departmental KPI:</th>
		<td><?php echo($kpirow['kpi_value']." (".$id_labels_all[$section].$kpiid.")"); ?>&nbsp;&nbsp;&nbsp;<span class=float><input type=button value="View Details" id=view /></span></td>
	</tr>
	<tr>
		<th class=left>Project Name:</th>
		<td><?php echo($plcrow['plcname']); ?></td>
	</tr>
	<tr>
		<th class=left>Project Description:&nbsp;</th>
		<td><?php echo($plcrow['plcdescription']); ?></td>
	</tr>
	<tr>
		<th class=left>Start Date:</th>
		<td><?php echo(date("d M Y",$plcrow['plcstartdate'])); ?></td>
	</tr>
	<tr>
		<th class=left>End Date:</th>
		<td><?php echo(date("d M Y",$plcrow['plcenddate'])); ?></td>
	</tr>
</table>
<p style="margin: 0 0 0 0;">&nbsp;</p>
<h2>Phases</h2>
<table>
	<tr>
		<th height=27 colspan="2" >Project Phase</th>
		<th width=100>Calendar<br>Days</th>
		<th>Start<br>Date</th>
		<th>End<br>Date</th>
		<th>% Target<br>of Project</th>
		<th>Completed?</th>
		<th>Date of<br>Completion</th>
	</tr>
<?php
$tartot = 0;
$targets = array();
$actuals = array();
    $sql = "SELECT * FROM ".$dbref."_list_plc_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            switch($row['type'])
            {
        case "A":
//CUSTOM PHASES
        $sql2 = "SELECT p.*, c.value FROM ".$dbref."_kpi_plc_".$tabl." p, ".$dbref."_list_plc_custom c ";
        $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'C' AND p.ppphaseid = c.id AND p.tmpyn = 'P' ORDER BY p.ppsort";
        include("inc_db_con2.php");
            $mnr2 = mysql_num_rows($rs2);
            if($mnr2 > 0)
            {
            $a=0;
                while($row2 = mysql_fetch_array($rs2))
                {
                    //targets
                    $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = (isset($targets[$Yr][$Mn]) ? $targets[$Yr][$Mn] : 0) + $row2['pptarget'];
                    $a++;
                    $tmpid[] = $row2['tmpid'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                            $aYr = date("Y",$row2['ppdonedate']);
                            $aMn = date("n",$row2['ppdonedate']);
                            $actuals[$aYr][$aMn] = (isset($actuals[$aYr][$aMn]) ? $actuals[$aYr][$aMn] : 0) + $row2['pptarget'];
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
	<?php if($a==1) { ?>		<td class=top rowspan=<?php echo($mnr2); ?>><b><?php echo($row['value']); ?>:</b></td><?php } ?>
		<td class=i><i><?php echo($row2['value']); ?>:</i></td>
		<td class="center"><?php echo($row2['ppdays']); ?> day(s)</td>
		<td class="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="center"><?php echo($ppdone); ?></td>
		<td class="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                }
            }
        break;
            case "P":
            //PROCUREMENT PHASES
                $sql2 = "SELECT p.*, c.value FROM ".$dbref."_kpi_plc_".$tabl." p, ".$dbref."_list_plc_proc c ";
                $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'P' AND p.ppphaseid = c.id AND p.tmpyn = 'P'  ORDER BY p.ppsort";
                include("inc_db_con2.php");
                $mnr2 = mysql_num_rows($rs2);
                $r=0;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $r++;
                        $tmpid[] = $row2['tmpid'];
                        $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = (isset($targets[$Yr][$Mn]) ? $targets[$Yr][$Mn] : 0) + $row2['pptarget'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                            $aYr = date("Y",$row2['ppdonedate']);
                            $aMn = date("n",$row2['ppdonedate']);
                            $actuals[$aYr][$aMn] = (isset($actuals[$aYr][$aMn]) ? $actuals[$aYr][$aMn] : 0) + $row2['pptarget'];
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
        <?php if($r==1) { ?>
		<td class=top rowspan=<?php echo($mnr2); ?> ><b><?php echo($row['value']); ?>:</b></td>
        <?php } ?>
		<td ><i><?php echo($row2['value']); ?>:</i></td>
		<td class="center"><?php echo($row2['ppdays']); ?> day(s)</td>
		<td class="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="center"><?php echo($ppdone); ?></td>
		<td class="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                    }
                //mysql_close($con2);
				break;
            default:
            //STANDARD PHASES WHERE TYPE = S
                $sql2 = "SELECT * FROM ".$dbref."_kpi_plc_".$tabl." WHERE pptype = 'S' AND ppphaseid = ".$row['id']." AND tmpyn = 'P'  AND ppplcid = ".$plcid;
                include("inc_db_con2.php");
                    $mnr2 = mysql_num_rows($rs2);
                    if($mnr2 > 0)
                    {
                        $row2 = mysql_fetch_array($rs2);
                    }
                //mysql_close($con2);
                if($mnr2>0)
                {
                    $tmpid[] = $row2['tmpid'];
                    $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = (isset($targets[$Yr][$Mn]) ? $targets[$Yr][$Mn] : 0) + $row2['pptarget'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                            $aYr = date("Y",$row2['ppdonedate']);
                            $aMn = date("n",$row2['ppdonedate']);
                            $actuals[$aYr][$aMn] = (isset($actuals[$aYr][$aMn]) ? $actuals[$aYr][$aMn] : 0) + $row2['pptarget'];
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
		<td colspan="2" ><b><?php echo($row['value']); ?>:</b></td>
		<td class="center"><?php echo($row2['ppdays']); ?> day(s)</td>
		<td class="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="center"><?php echo($ppdone); ?></td>
		<td class="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                }
                break;

            }
        }

?>
	<tr>
		<td  colspan="5" class=right><b>Target % of Project Total:</b></td>
		<td class="center"><b><?php echo($tartot); ?> %</b></td>
		<td class="center" colspan=2></td>
	</tr>
</table>
<?php
//print_r($targets);
?>
<!-- KPI RESULTS -->
<?php
//get original results
$sql = "SELECT * FROM ".$dbref."_kpi_results WHERE kr_kpiid = ".$kpiid;
$original_results = mysql_fetch_alls($sql,"kr_timeid");
?>
<h2 >KPI Results</h2>
<form name=plcconfirm method=post action=admin_kpi_plc_view_process.php>
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=tbl value="<?php echo($tbl); ?>">
<input type=hidden name=act value="A">
<input type=hidden name=tmpid value="<?php echo(implode(",",$tmpid)); ?>">
<table>
    <tr>
        <td colspan=1 rowspan=3>&nbsp;</td>
        <th colspan=6>Original Performance</td>
        <th colspan=6>Updated Performance</td>
		<th rowspan=3>Changes to Period Performance</td>
    </tr>
    <tr>
        <th colspan=3>Period Performance</td>
        <th colspan=3>Overall Performance</td>
        <th colspan=3>Period Performance</td>
        <th colspan=3>Overall Performance</td>
    </tr>
	<tr>
		<th>Target</th>
		<th>Actual</th>
		<th>R</th>
		<th>Target</th>
		<th>Actual</th>
		<th>R</th>
		<th>Target</th>
		<th>Actual</th>
		<th>R</th>
		<th>Target</th>
		<th>Actual</th>
		<th>R</th>
	</tr>
<?php
    $s=1;
    $krt0 = 0;
    $kra0 = 0;
    $targ = 0;
    $actl = 0;
	$otargets = array();
    $years = array();
    $no_approval = true;
    //while($row = mysql_fetch_array($rs))
	$values = array('target'=>array(),'actual'=>array());
	$valuesN = array('target'=>array(),'actual'=>array(),'db'=>array('target'=>array(),'actual'=>array()));
    foreach($time as $ti => $row) {
        $thead = $row;
        //NEW RESULTS
        $tYr = date("Y",$row['end_stamp']);
        $years[] = $tYr;
        $tMn = date("n",$row['end_stamp']);
        $tart = isset($targets[$tYr][$tMn]) ? $targets[$tYr][$tMn] : 0;
        $actu = isset($actuals[$tYr][$tMn]) ? $actuals[$tYr][$tMn] : 0;
        $targ += $tart;
        $actl += $actu;
		$valuesN['target'][$ti] = (checkIntRef($tart) ? $targ : 0);
		$valuesN['actual'][$ti] = (checkIntRef($actu) ? $actl : 0);
		$valuesN['db']['target'][$ti] = (checkIntRef($tart) ? $targ : 0);
		$valuesN['db']['actual'][$ti] = (checkIntRef($actu) ? $actl : 0);
		$rNp = KPIcalcResult($valuesN,"CO",array(),$ti);
		$rNo = KPIcalcResult($valuesN,"CO",array(),"ALL");

        //OLD RESULTS
		$values['target'][$ti] = $original_results[$ti]['kr_target'];
		$values['actual'][$ti] = $original_results[$ti]['kr_actual'];
		$ro = KPIcalcResult($values,"CO",array(),"ALL");
		$rp = KPIcalcResult($values,"CO",array(),$ti);

		$affect = array();
		if($original_results[$ti]['kr_target']!=$valuesN['db']['target'][$ti]) {
			$affect[] = "Monthly Target changed to '".KPIresultDisplay($valuesN['db']['target'][$ti],2)."' from '".KPIresultDisplay($original_results[$ti]['kr_target'],2)."'";
			$no_approval = false;
		}
		if($original_results[$ti]['kr_actual']!=$valuesN['db']['actual'][$ti]) {
			$affect[] = "Monthly Actual updated to '".KPIresultDisplay($valuesN['db']['actual'][$ti],2)."' from '".KPIresultDisplay($original_results[$ti]['kr_actual'],2)."'";
		}
		
	echo "	<tr>
			<th class=left>".$row['display_full'].":<input type=hidden name=time[] value=$ti /></th>
			<!-- Original Values -->
			<td class=right>".KPIresultDisplay($original_results[$ti]['kr_target'],2)."&nbsp;</td>
			<td class=right>".KPIresultDisplay($original_results[$ti]['kr_actual'],2)."&nbsp;</td>
			<td class=\"".$rp['style']."\">".$rp['text']."</td>
			<td class=right style=\"background-color: #eeeeee;\">".KPIresultDisplay($ro['target'],2)."&nbsp;</td>
			<td class=right style=\"background-color: #eeeeee;\">".KPIresultDisplay($ro['actual'],2)."&nbsp;</td>
			<td class=\"".$ro['style']."\">".$ro['text']."</td>
			<!-- New Values -->
			<td class=right>".KPIresultDisplay($valuesN['db']['target'][$ti],2)."&nbsp;
				<input type=hidden name=target[$ti] value=\"".$valuesN['db']['target'][$ti]."\">
				<input type=hidden name=actual[$ti] value=\"".$valuesN['db']['actual'][$ti]."\">
			</td>
			<td class=right>".KPIresultDisplay($valuesN['db']['actual'][$ti],2)."&nbsp;</td>
			<td class=\"".$rNp['style']."\">".$rNp['text']."</td>
			<td class=right style=\"background-color: #eeeeee;\">".KPIresultDisplay($rNo['target'],2)."&nbsp;</td>
			<td class=right style=\"background-color: #eeeeee;\">".KPIresultDisplay($rNo['actual'],2)."&nbsp;</td>
			<td class=\"".$rNo['style']."\">".$rNo['text']."</td>
			<td>".implode("<br />",$affect)."</td>
		</tr>";
    }
?>
</table>


<p style="font-size: 1pt; line-height: 1pt;">&nbsp;</p>
<table width=100%>
    <tr>
        <td class=center>
			<input type=button id=actbut value=" Approve " class=isubmit onclick="document.forms['plcconfirm'].submit();"> 
			<input type=button class=idelete value=" Reject " id=rejbut1 onclick="document.getElementById('rejectnotice').style.display = 'inline'; document.getElementById('rejbut1').disabled = true;">
<span id=rejectnotice><br>Please give a reason for the rejection of this update:<br><textarea cols=50 rows=5 id=rejnotice></textarea><br><input type=button value="Send Rejection" id=rejbut2 onclick="document.getElementById('rejnote').value = document.getElementById('rejnotice').value; submitRej();"></span>
        </td>
    </tr>
</table>
</form>
<script type=text/javascript>
document.getElementById('rejectnotice').style.display = "none";
</script>
<form name=plcrej method=post action=admin_kpi_plc_view_process.php>
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=tbl value="<?php echo($tbl); ?>">
<input type=hidden name=act value="R">
<input type=hidden name=tmpid value="<?php echo(implode(",",$tmpid)); ?>">
<input type=hidden name=rejnote id=rejnote value="">
</form>
<p>&nbsp;</p>
<script type=text/javascript>
function submitRej() {
    document.getElementById('actbut').disabled = true;
    document.getElementById('rejbut2').disabled = true;
    t = setTimeout("document.forms['plcrej'].submit()",500);
}
$(function() {
	$("#view").click(function() {
		url = "../view_dept_detail.php?nav=0&id=<?php echo $kpiid; ?>";
		window.open(url, '_blank',"status=0,toolbar=0,location=0,scrollbars=1");
	});
});
</script>
<p>&nbsp;</p>
<!-- END CONTAINER -->
</td></tr></table>
</body>

</html>
