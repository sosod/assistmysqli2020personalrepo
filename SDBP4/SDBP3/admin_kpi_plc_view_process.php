<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1><?php echo $_SESSION['modtext']; ?> >> Manage >> Project Life Cycle</h1>
<p>Processing...</p>
<?php
$err = "N";
$dirid = $_REQUEST['dirid'];
$subid = $_REQUEST['subid'];
$typ = $_REQUEST['typ'];
$src = $_REQUEST['src'];
//FORM DETAILS
$kpiid = $_REQUEST['kpiid'];
$plcid = $_REQUEST['plcid'];
$tme = $_REQUEST['time'];
$target = $_REQUEST['target'];
$actual = $_REQUEST['actual'];
$krtargettypeid = 2;
$tid = $_REQUEST['tmpid'];
$tmpid = explode(",",$tid);
$act = $_REQUEST['act'];
$rejnote = isset($_REQUEST['rejnote']) ? $_REQUEST['rejnote'].chr(10)."[Written by ".$_SESSION['tkn']." on ".date("d M Y H:i:s")."]" : "";

/*echo("<p>".$act);
echo("<p>".$rejnote);
echo("<p>".$kpiid);
echo("<p>".$plcid);
*/




//IF act = "A" => approved
if($act=="A")
{
    if(checkIntRef($plcid))
    {
    //MOVE OLD DETAILS FROM PLC TO PLC_OLD
        $sql = "INSERT INTO ".$dbref."_kpi_plc_old (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,oldtkid,olddate) ";
        $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as oldtid, ".$today." as olddt FROM ".$dbref."_kpi_plc p2 WHERE plcid = ".$plcid;
        db_insert($sql);
    //MOVE NEW DETAILS FROM PLC_TEMP TO PLC
        $plcnew = array();
        $sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'Y' ORDER BY tmpid DESC";
        //$sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE plcid = ".$plcid." ORDER BY tmpid DESC"; //for dev testing
            $plcnew = mysql_fetch_one($sql);
        
        $sql = "UPDATE ".$dbref."_kpi_plc SET";
        $sql.= "  plcname = '".$plcnew['plcname']."'";
        $sql.= ", plcdescription = '".$plcnew['plcdescription']."'";
        $sql.= ", plcstartdate = ".$plcnew['plcstartdate'];
        $sql.= ", plcenddate = ".$plcnew['plcenddate'];
        $sql.= ", plcproccate = ".$plcnew['plcproccate'];
        $sql.= ", plcyn = 'Y'";
        $sql.= " WHERE plcid = ".$plcid;
        db_update($sql);
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE ".$dbref."_kpi_plc_temp SET tmpyn = 'A' WHERE tmpid = ".$plcnew['tmpid'];
        db_update($sql);
    //MOVE OLD PHASES TO PHASES_OLD
        $sql = "INSERT INTO ".$dbref."_kpi_plc_phases_old (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,oldtkid,olddate) ";
        $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte FROM ".$dbref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
        db_insert($sql);
    //DELETE OLD PHASES
        $sql = "DELETE FROM ".$dbref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
        db_update($sql);
    //MOVE NEW PHASES FROM PHASES_TEMP TO PHASES
        foreach($tmpid as $temp)
        {
            $phase = array();
            if(checkIntRef($temp))
            {
                //GET TEMP DETAILS
                $sql = "SELECT * FROM ".$dbref."_kpi_plc_phases_temp WHERE tmpid = ".$temp;
                    $phase = mysql_fetch_one($sql);
                //UPDATE PHASES
                $sql = "INSERT INTO ".$dbref."_kpi_plc_phases SET";
                $sql.= "  ppplcid = ".$plcid;
                $sql.= ", pptype = '".$phase['pptype']."'";
                $sql.= ", ppphaseid = ".$phase['ppphaseid'];
                $sql.= ", ppsort = ".$phase['ppsort'];
                $sql.= ", ppdays = ".$phase['ppdays'];
                $sql.= ", ppstartdate = ".$phase['ppstartdate'];
                $sql.= ", ppenddate = ".$phase['ppenddate'];
                $sql.= ", pptarget = ".$phase['pptarget'];
                $sql.= ", ppdone = '".$phase['ppdone']."'";
                $sql.= ", ppdonedate = ".$phase['ppdonedate'];
				db_insert($sql);
                //CHANGE STATUS
                $sql = "UPDATE ".$dbref."_kpi_plc_phases_temp SET tmpyn = 'A' WHERE tmpid = ".$temp;
				db_update($sql);
            }
        }
		//LOG UPDATE TO PLC
		$filename = logPLC($plcid,"current",$src);
		//plcLogText($start,$plcid,$filename,$end) => "$start Project Life Cycle PLC$plcid [<a id=$filename>View here</a>]. $end";
		$log_trans = plcLogText("Approved",$plcid,$filename,"");
		//$log_trans = "Approved Project Life Cycle PLC".$plcid." [<a target=_blank href=".$filename.">View here</a>].";
				$v = array(
					'fld'=>"PLC",
					'timeid'=>0,
					'text'=>$log_trans,
					'old'=>'',
					'new'=>'',
					'act'=>"U",
					'YN'=>"Y"
				);
		logChanges($section,$kpiid,$v,$filename);
		
    //UPDATE KPI_RESULTS
        if(checkIntRef($kpiid)) {
			$sqla = "SELECT kr_actual, kr_timeid FROM ".$dbref."_kpi_results WHERE kr_kpiid = $kpiid";
			$old_results = mysql_fetch_alls($sqla,"kr_timeid");
			foreach($tme as $ti) {
                $g = isset($target[$ti]) ? $target[$ti] : 0;
                $a = isset($actual[$ti]) ? $actual[$ti] : 0;
                if(is_numeric($a) && is_numeric($g)) {
                    $sql = "UPDATE ".$dbref."_kpi_results 
							SET kr_target = ".$g."
							, kr_actual = ".$a."
							WHERE kr_kpiid = ".$kpiid." AND kr_timeid = ".$ti;
					$mar = db_update($sql);
					if(checkIntRef($mar)) {
					//IF CHANGES THEN LOG CHANGE TO KPI RESULT
						$old = isset($old_results[$ti]['kr_target']) ? $old_results[$ti]['kr_target'] : 0;
						if($g!=$old) {
								$v = array(
									'fld'=>"kr_target",
									'timeid'=>$ti,
									'text'=>code("Updated Target to '".$g."' from '".$old."' for time period ".$time[$ti]['display_short']." due to update in Project Life Cycle PLC".$plcid),
									'new'=>$g,
									'old'=>$old,
									'act'=>"U",
									'YN'=>"Y",
								);
							logChanges($section,$kpiid,$v,code($sql));
						}
						$old = isset($old_results[$ti]['kr_actual']) ? $old_results[$ti]['kr_actual'] : 0;
						if($a!=$old) {
								$v = array(
									'fld'=>"kr_actual",
									'timeid'=>$ti,
									'text'=>code("Updated Actual to '".$a."' from '".$old."' for time period ".$time[$ti]['display_short']." due to update in Project Life Cycle PLC".$plcid),
									'new'=>$a,
									'old'=>$old,
									'act'=>"U",
									'YN'=>"Y",
								);
							logChanges($section,$kpiid,$v,code($sql));
						}
					}
                }
            }
        }
        else
        {
            echo("<p>Error on kpiid validation - can't update results");
            $err = "Y";
        }
    }
    else
    {
        echo("<P>Error on plcid validation");
        $err = "Y";
    }
    
    if($err == "N")
    {
        echo("<p>PLC successfully updated.");
    }
} else { //else if act = rejected
    //CHANGE TEMP STATUS TO Y
        $plcnew = array();
        $sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'Y' ORDER BY tmpid DESC";
		//$sql = "SELECT * FROM ".$dbref."_kpi_plc_temp WHERE plcid = ".$plcid." ORDER BY tmpid DESC";	//for repeated testing
            $plcnew = mysql_fetch_one($sql);
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE ".$dbref."_kpi_plc_temp SET tmpyn = 'R', tmpreason = '".code($rejnote)."' WHERE tmpid = ".$plcnew['tmpid'];
		db_update($sql);

    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE ".$dbref."_kpi_plc_phases_temp SET tmpyn = 'R' WHERE ppplcid = ".$plcid;
		db_update($sql);


		//LOG UPDATE TO PLC
		$filename = logPLC($plcid,"temp",$src);
		//$log_trans = "Rejected Project Life Cycle PLC".$plcid." [<a target=_blank href=".$filename.">View here</a>].  Reason given: ".code($rejnote);
		$log_trans = plcLogText("Rejected",$plcid,$filename,((isset($_REQUEST['rejnote']) && strlen($_REQUEST['rejnote'])>0) ? "Reason given: ".code($_REQUEST['rejnote']) : ""));
				$v = array(
					'fld'=>"PLC",
					'timeid'=>0,
					'text'=>$log_trans,
					'old'=>'',
					'new'=>'',
					'act'=>"U",
					'YN'=>"Y"
				);
		logChanges($section,$kpiid,$v,$filename);

		
    //SEND AN EMAIL TO USER
	$sql = "SELECT tkemail, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$plcnew['tmptkid']."' AND tkstatus = 1";
	$tk = mysql_fetch_one($sql);
	if(isset($tk['tkemail']) && strlen($tk['tkemail'])>0) {
		$to = $tk['tkemail'];
        $from = "no-reply@ignite4u.co.za";
        $subject = "Rejected Project Life Cycle [PLC".$plcid."]";
        $message = "Project Life Cycle [PLC".$plcid."] which you updated on SDBIP ".$modtxt." has been rejected.".chr(10).chr(10);
        $message.= "The reason given was:".chr(10).$rejnote;
        
        $header = "From: ".$from."\r\nReply-to: ".$from;
        mail($to,$subject,$message,$header);
		
	}
	
	
	
	
    echo("<p>Update complete.</p>");

}

?>
</body>

</html>
