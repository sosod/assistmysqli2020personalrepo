<?php
function getListKPI($dl) {
    global $cmpcode;
    global $modref;
    $list = array();
    
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_".$dl['id']." WHERE yn = 'Y' ";
	if($dl['headfield']=="kpiriskrate" || $dl['headfield']=="ktype") {
	} elseif($dl['code']=="N") { 
		$sql.= " ORDER BY value"; 
	} else { 
		$sql.= " ORDER BY ".$dl['fld'].", value"; 
	}
//    echo("<P>".$sql);
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
			if($dl['headfield']=="kpicalctype" || $dl['headfield']=="kpistratop" || $dl['headfield']=="topcalctype") {
				$id = $row['code'];
			} else {
				$id = $row['id'];
			}
            $val = $row['value'];
            $code = $row[$dl['fld']];
            $list[$id]['id'] = $id;
            $list[$id]['value'] = $val;
            $list[$id]['code'] = $code;
        }
    mysql_close($con);
    
    return $list;
}

function getListValue($dl,$i) {
    global $cmpcode;
    global $modref;
    $list = array();
if($dl['id'] =="stratop" || $dl['id']=="riskrate") {
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_".$dl['id']." WHERE code = '$i' ";
} else {
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_".$dl['id']." WHERE id = $i ";
}
//    echo("<P>".$sql);
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['id'];
            $val = $row['value'];
            $code = $row[$dl['fld']];
            $list['id'] = $id;
            $list['value'] = $val;
            $list['code'] = $code;
        }
    mysql_close($con);

    return $list;
}

function getKRSetup() {
    global $cmpcode;
    global $modref;
    $kr = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpiresult WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
			$k = $row['sort'];
            $kr[$k] = $row;
            $k++;
        }
    mysql_close($con);
    
    return $kr;
}

function calcKR($kct, $rt, $ra) {
	global $krsetup;
	/* KRR REFS
	1 = NOT MEASURED (GREY)	
	2 = NOT MET (RED)
	3 = ALMOST MET (ORANGE)
	4 = MET (GREEN)
	5 = WELL MET (DARK GREEN)
	6 = EXTREMELY WELL MET (DARK BLUE) */
    $krr = 1;
    switch($kct)
    {
        case "NA":
            $krr = 1;	//NA = not measured	
            break;
        case "ZERO":
		case "REV":
			if($ra > $rt) {
				$res = 0;	//if actual>target then not met
			} elseif($ra == $rt) {
				$res = 1;	//if target=actual then met
			} else {
				$res = 1.1;	//if target>actual then well met
			}
			$krr = checkResult($res);
            break;
        default:
			if($ra==0 && $rt == 0) {
				$krr = 1;	//IF no target & actual then "not measured";
			} else {
				if($ra>0 && $rt == 0) {
					$res = 1.51;	//if no target but actual > 0 then default to 151% (level 6)
				} else {
					$res = $ra/$rt;
				}
				$krr = checkResult($res);
			}
            break;
    }
//	echo($res);
    return $krr;
}

function checkResult($res) {
	global $krsetup;
	$krr = 1;
	$yn = 'N';
	for($k=2;$k<6;$k++) {
		$max = $krsetup[$k]['max'] / 100;
		$acalc = $krsetup[$k]['maxcalc'];
		switch($acalc)
		{
			case "E":
				if($res == $max) { $yn = 'Y'; }
				break;
			case "L":
				if($res < $max) { $yn = 'Y'; }
				break;
			case "L-E":
				if($res <= $max) { $yn = 'Y'; }
				break;
		}
		if($yn == 'Y') {
			$krr = $k;
			$k = 10;
		}
	}
	if($krr == 1 && $yn == 'N') {
		$krr = 1;
		$min = $krsetup[6]['min'] / 100;
		$icalc = $krsetup[6]['mincalc'];
		switch($icalc)
		{
			case "E":
				if($res == $min) { $yn = 'Y'; }
				break;
			case "G":
				if($res > $min) { $yn = 'Y'; }
				break;
			case "G-E":
				if($res >= $min) { $yn = 'Y'; }
				break;
		}
		if($yn == 'Y') {
			$krr = 6;
		}
	}
	return $krr;
}

function formatRes($tt,$val) {
	$echo = "";
	if(is_numeric($tt)) {
		switch($tt) {
			case 1: $tt = "R"; break;
			case 2: $tt = "%"; break;
			case 3: default: $tt = "#"; break;
		}
	}
	switch($tt) {
		case "R":
			$echo = "R ".number_format($val,2);
			break;
		case "%":
			if($val == ceil($val)) 
				$echo = $val.$tt;
			else
				$echo = number_format($val,2).$tt;
			break;
		case "#":
		default:
			if($val==ceil($val)) {
				$echo = $val;
			} else {
				$echo = number_format($val,2);
			}
			break;
	}
	return $echo;
}

function KRGlossOp($op) {
                        switch($op)	{
                            case "G":
                                $echo = ">";
                                break;
                            case "G-E":
                                $echo=">=";
                                break;
                            case "E":
                                $echo="=";
                                break;
							case "L":
								$echo = "<";
								break;
							case "L-E":
								$echo = "<=";
								break;
                        }
	return $echo;
}
function displayKRGloss($src) {
	global $krsetup;
	if($src == "report") { 
		$echo = "<table cellpadding=0 cellspacing=0 style=\"float:right; border:0px;\"><tr><td class=noborder>";
		$echo.="<h2 name=krr>KPI Result Categories</h2>";
		$echo.="<table cellpadding=3 cellspacing=0 width=570>";
		$echo.= "<tr>        <th width=160>Category</th>        <th width=40>Color</th>        <th width=370>Explanation</th>    </tr>";
		foreach($krsetup as $row)
		{
			$echo.="<tr>";
			$echo.="<td>".$row['value']."</td>";
			$echo.="<td style=\"text-align:center\" ><table class=noborder><tr><td width=34 class=noborder style=\"background-color: ".$row['color'].";\">&nbsp;</td></tr></table></td>";
			$echo.="<td>";
            if($row['sort']==1) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" Actual/Target = ".$row['max']."%");
                } else {
                    if($row['max']>1000) {
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']."%");
                    } else {
                        $echo.=($row['min']."% ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']."%");
                    }
                }
            }
			$echo.="</td>";
			$echo.="</tr>";
		}
		$echo.="</table>";
		$echo.="</td></tr></table>"; 
	} elseif ($src == "excel") {
		$echo.="<tr></tr><tr><td nowrap class=title2 style=\"text-align: left;\">KPI Result Categories</td></tr>";
		$echo.= "<tr>        <td class=head style=\"border: none;\"> </td>        <td class=head style=\"border: none;\" nowrap>Category</td>        <td class=head nowrap style=\"border: none;\">Explanation</td>    </tr>";
		foreach($krsetup as $row)
		{
			$echo.="<tr>";
			$echo.="<td style=\"text-align:center;background-color: ".$row['color'].";\"> </td>";
			$echo.="<td nowrap>".$row['value']."</td>";
			$echo.="<td nowrap>";
            if($row['sort']==1) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" Actual/Target = ".$row['max']."%");
                } else {
                    if($row['max']>1000) {
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']."%");
                    } else {
                        $echo.=($row['min']."% ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']."%");
                    }
                }
            }
			$echo.="</td>";
			$echo.="</tr>";
		}
	} elseif ($src == "csv") {
		$echo.="\r\n\"KPI Result Categories\"\r\n";
		$echo.= "\" \",\"Category\",\"Explanation\"\r\n";
		foreach($krsetup as $row)
		{
			$echo.="\"".$row['code']."\"";
			$echo.=",\"".$row['value']."\"";
			$echo.=",\"";
            if($row['sort']==1) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" Actual/Target = ".$row['max']."%");
                } else {
                    if($row['max']>1000) {
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']."%");
                    } else {
                        $echo.=($row['min']."% ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']."%");
                    }
                }
            }
			$echo.="\"";
			$echo.="\r\n";
		}
	} else {	//glossary
		$echo.="<h2><a name=krr></a>KPI Result Categories</h2>";
		$echo.="<table cellpadding=3 cellspacing=0 width=570>";
		$echo.= "<tr>        <th width=160>Category</th>        <th width=40>Color</th>        <th width=370>Explanation</th>    </tr>";
		foreach($krsetup as $row)
		{
			$echo.="<tr>";
			$echo.="<td>".$row['value']."</td>";
			$echo.="<td style=\"text-align:center\" ><table class=noborder><tr><td width=34 class=noborder style=\"background-color: ".$row['color'].";\">&nbsp;</td></tr></table></td>";
			$echo.="<td>";
            if($row['sort']==1) {
                $echo.=("KPIs with no targets or actuals in the selected period.");
            } else {
                if($row['mincalc']=="E") {
                        $echo.=(" Actual/Target = ".$row['max']."%");
                } else {
                    if($row['max']>1000) {
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" ".$row['min']."%");
                    } else {
                        $echo.=($row['min']."% ");
                        $echo.=(KRGlossOp($row['mincalc']));
                        $echo.=(" Actual/Target ");
                        $echo.=(KRGlossOp($row['maxcalc']));
                        $echo.=(" ".$row['max']."%");
                    }
                }
            }
			$echo.="</td>";
			$echo.="</tr>";
		}
		$echo.="</table>";
	}
	return $echo;
}

function getHeadings($where) {
	global $cmpcode;
	global $dbref;
	$head = array();
	$sql = "SELECT * FROM ".$dbref."_headings WHERE $where";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs))
		{
			$head[$row['headfield']] = $row;
		}
	mysql_close($con);
	return $head;
}


function OdisplayResult($result) {
    if(count($result)>0) {
        echo("<div class=\"ui-widget\">");
        if($result[0]=="check")
        {
            echo("<div class=\"ui-state-highlight ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">");
            echo("<p><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span>");
        }
        else
        {
            echo("<div class=\"ui-state-error ui-corner-all\" style=\"margin: 5px 0px 10px 0px; padding: 0 .3em;\">");
            echo("<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>");
        }
        echo($result[1]."</p></div></div>");
    }
}

function logChange($ref,$type,$action,$disp,$lsql) {
	global $dbref;
	global $cmpcode;
	$t = getdate();
	$today = $t[0];
	global $tkid;
	
	if(!checkIntRef($ref)) { $ref = 0; }
	if(strlen($disp)!=1) { $disp = 'N'; }
	$lsql = code($lsql);
	if(strlen($type)==0) { $type = code($_SERVER['PHP_SELF']); }
	
	$sql = "INSERT INTO ".$dbref."_log (id, tkid, date, ref, type, action, disply, lsql) ";
	$sql.= " VALUES (null,'$tkid',$today,$ref,'$type','$action','$disp','$lsql')";
	include("inc_db_con.php");
	
}


function getForecastYears() {
	global $dbref;
	global $cmpcode;
	$fore = array();
	
	$sql = "SELECT * FROM ".$dbref."_list_forecastyears ";
	$sql.= " ORDER BY sort";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs))
			$fore[$row['id']] = $row;
	mysql_close($con);
	
	return $fore;
}

function logTL($topid,$field,$old,$new,$type,$docid,$lsql,$timeid,$action) {
	global $tkid;
	global $today;
	global $dbref;
	global $cmpcode;
	
	$sql = "INSERT INTO ".$dbref."_toplevel_log (logtopid,logtkid,logdate,logfield,logold,lognew,logtype,logdocid,logyn) ";
	$sql.= "VALUES ($topid,'$tkid',$today,'$field','$old','$new','$type',0,'Y')";
	include("inc_db_con.php");	
	logChange('TL','',$action,'N',$lsql);
}


function logTLEdit($topid,$field,$old,$new,$type,$docid,$lsql,$timeid,$action,$yn) {
	global $tkid;
	global $today;
	global $dbref;
	global $cmpcode;
	$sql = "INSERT INTO ".$dbref."_toplevel_log (logtopid,logtkid,logdate,logfield,logold,lognew,logtype,logdocid,logyn) ";
	$sql.= "VALUES ($topid,'$tkid',$today,'$field','$old','$new','$type',$docid,'$yn')";
	include("inc_db_con.php");	
	logChange('TL','',$action,'N',$lsql);
}



function logKPI($kpiid,$field,$old,$new,$lsql,$timeid,$type,$action) {
	global $tkid;
	global $today;
	global $dbref;
	
	$sql = "INSERT INTO ".$dbref."_kpi_log (logkpiid,logtkid,logdate,logfield,logold,lognew,logtype,logyn) ";
if($type == "KU") {
	$sql.= "                          VALUES ($kpiid,'$tkid',$today,'".$field."_".$timeid."','$old','$new','$type','Y')";
} else {
	$sql.= "                          VALUES ($kpiid,'$tkid',$today,'$field','$old','$new','$type','Y')";
}
//	$sql.= "                          VALUES ($kpiid,'$tkid',$today,'$field','$old','$new','$type','Y')";
	include("inc_db_con.php");
	/*CREATE TABLE IF NOT EXISTS `assist_twk0001_sdp10_kpi_log` (
  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logkpiid` int(10) unsigned NOT NULL,
  `logtkid` varchar(10) NOT NULL,
  `logdate` int(10) unsigned NOT NULL,
  `logfield` varchar(45) NOT NULL,
  `logold` text NOT NULL,
  `lognew` text NOT NULL,
  `logtype` varchar(2) NOT NULL,
  `logyn` char(1) NOT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;*/
}

function getUserTime() {
	global $tkid;
	global $cmpcode;
	global $dbref; 
	$ut = "active";
	$sql = "SELECT * FROM ".$dbref."_list_admins WHERE yn = 'Y' AND type = 'TPS' AND tkid = '$tkid'";
	include("inc_db_con.php");
		if(mysql_num_rows($rs)>0) { 
			$ut = "activekpi";
		}
	mysql_close($con);
	return $ut;
}


function formatValue($tt,$val,$nf) {	//Targettype, value, decimal points
	$echo = "";
	switch($tt) {
		case "R":
		case "1":
			$echo = "R ".number_format($val,$nf);
			break;
		case "%":
		case "2":
			$echo = number_format($val,$nf)."%";
			break;
		case "#":
		case "3":
		default:
			$echo = number_format($val,$nf);
			break;
	}
	return $echo;
}


function drawPie($data_type,$results,$kpi,$chart_title,$title_class,$layout,$legend_layout,$required,$link,$from,$to,$mds,$graph_ref,$groupby) {
	global $krsetup;
	/*** REQUIRED ***/
	if($required == "Y") {
		require("lib/amcharts2-php/AmPieChart.php");
		AmChart::$swfObjectPath = "swfobject.js";
		AmChart::$libraryPath = "lib/amcharts2/ampie/";
		AmChart::$jsPath = "lib/amcharts2-php/AmCharts.js";
		AmChart::$jQueryPath = "lib/amcharts2/ampie/jquery.js";
		AmChart::$loadJQuery = true;
	}
	/*** LAYOUT ***/
	$dlabel = "-25%";
	switch($layout) {
		case "3":
			$tdl = 1;
			$cradius = 90;
			$cwidth = 10;
			break;
		case "2":
			$tdl = 3;
			$cradius = 100;
			$cwidth = 30;
			break;
		default:
			$cradius = 120;
			$dlabel = "-30%";
			$tdl = 3;
			$layout = "ONS";
			$cwidth = 30;
			break;
	}
	$cwidth+= $cradius * 2;
	$cheight = $cwidth;
	/*** CHART ***/
	$chart = new AmPieChart("pie_".$graph_ref);
	if(strlen($chart_title)>0) { 
		if(strlen($chart_title)>100) { $chart_title = "<small>".$chart_title."</small>"; }
		$chart_title = "<p class=$title_class>".$chart_title."</p>";
		$chart->setTitle($chart_title); 
	}
	foreach($krsetup as $krs) {
		if(in_array($krs['sort'],$results)) {
			$chart->addSlice($krs['sort'], code($krs['value'])." (".$kpi[$krs['sort']]." of ".$kpi['tot'].")", $kpi[$krs['sort']], array("color" => $krs['color']));
		}
	}
	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" =>$cheight,
		"font" => "Tahoma",
		"decimals_separator" => ".",
		"pie.y" => "49%",
		"pie.radius" => $cradius,
		"pie.height" => 5,
		"pie.inner_radius" => 0,
		"animation.pull_out_on_click" => false,
//		"pie.brightness_step" =>10,
//		"pie.inner_radius"=>30,
//		"pie.gradient"=>'radial',
//		"pie.gradient_ratio"=>'0,0,-50,0,0,0,0,-50', 
		"background.border_alpha" => 0,
		"data_labels.radius" => $dlabel,
		"data_labels.text_color" => "0xFFFFFF",
		"data_labels.show" => "<![CDATA[{percents}%]]>",
/*		"export_as_image.file" => 'lib/amcharts2/ampie/export.php',
		"export_as_image.color" => '0xCC0001',
		"export_as_image.alpha" => '50',*/
		"legend.enabled" => "false",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
	));
	echo html_entity_decode($chart->getCode());

	/*** LEGEND ***/
	if($data_type=="TOP") {
		$url = "report_tl_process.php?src=DASH&krsum=Y";
		$url.= "&dirfilter=".$mds['dir']."&subfilter=".$mds['sub'];
		if($groupby == "dir") {
			$url.="&groupby=sub";
		} elseif($groupby!="X") {
			$url.="&groupby=".$groupby;
			$url.="&".$groupby."filter[]=".$graph_ref;
		}
	} else {
		$url = "report_kpi_process.php?src=DASH&krsum=Y";
		$url.= "&dirfilter=".$mds['dir']."&subfilter[]=".$mds['sub'];
		if($groupby == "dir") {
			if($mds['type']=="M") {
				$url.="&groupby=dir";
			} else {
				$url.="&groupby=sub";
			}
		} elseif($groupby!="X") {
			$url.="&groupby=".$groupby;
			$url.="&".$groupby."filter[]=".$graph_ref;
		}
	}	
	$url.="&output=display&cf=".$from."&ct=".$to;
	$url.="&kpiresultfilter=";
	$krsetup2 = array_reverse($krsetup,true);
	if(count($results)>=4 && $legend_layout == "W") {
		$kc = ceil(count($results)/2);
	} else {
		$kc = count($results);
	}
	echo "<div align=center><table cellpadding=0 cellspacing=2 style=\"border: 1px solid #ababab;margin: 0 0 0 0;\" >";
		echo "<tr><td class=legend><table cellspacing=$tdl style=\"border-width: 0px; margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				if(in_array($krs['sort'],$results)) {
					$k++;
					if($k<=$kc) {
						echo "<tr>";
						echo "<td class=legend width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
						echo "<td class=legend>";
						if($link == "Y") {
							echo "<a href=".$url.$krs['sort']." style=\"color:".$krs['color'].";\">";
						}
						echo $krs['value']."</a> (".$kpi[$krs['sort']]." of ".$kpi['tot'].")</td>";
						echo "</tr>";
					}
				}
			}
		echo "</table></td>";
		echo "<td class=legend><table cellspacing=$tdl style=\"border-width: 0px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				if(in_array($krs['sort'],$results)) {
					$k++;
					if($k>$kc) {
						echo "<tr>";
						echo "<td class=legend width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
						echo "<td class=legend>";
						if($link == "Y") {
							echo "<a href=".$url.$krs['sort']." style=\"color:".$krs['color'].";\">";
						}
						echo $krs['value']."</a> (".$kpi[$krs['sort']]." of ".$kpi['tot'].")</td>";
						echo "</tr>";
					}
				}
			}
		echo "</table></td></tr>";
	echo "</table></div>";
}

//function drawPie($data_type,$results,$kpi,$chart_title,$title_class,$layout,$legend_layout,$required,$link,$from,$to,$mds,$graph_ref,$groupby) {
function drawFinBar($data_type,$sum,$chart_title,$title_class,$layout,$required,$graph_ref) {

	if($required=="Y") {
		require("lib/amcharts2-php/AmBarChart.php");
		AmChart::$swfObjectPath = "swfobject.js";
		AmChart::$libraryPath = "lib/amcharts2/amcolumn/";
		AmChart::$jsPath = "lib/amcharts2-php/AmCharts.js";
		AmChart::$jQueryPath = "lib/amcharts2/amcolumn/jquery.js";
		AmChart::$loadJQuery = true;
	}
	
	if($layout==2) {
		$cwidth = 400;
		$cheight = 200;
		$cdepth = 5;
		$cangle = 25;
		$ccolwidth = 60;
		$style = "font-size: 7pt; line-height: 9pt; padding-right: 10px;";
		$cmargin_top = 20;
		$cmargin_bottom = 40;		
	} else {
		$cwidth = 550;
		$cheight = 370;
		$cdepth = 10;
		$cangle = 45;
		$ccolwidth = 70;
		$font_size = 9;
		$cmargin_top = 20;
		$cmargin_bottom = 30;
		$style="font-size: 8pt; line-height: 10pt; padding-right: 10px;";
	}
	$lwidth = ($cwidth-60)/2;
	
	
	$chart = new AmBarChart("fin_".$graph_ref);
	$chart->setTitle("<p class=".$title_class." style=\"margin: 10 0 0 0;\">".$chart_title."</p>");
	
	$chart->addSerie("r", "Revenue");
	$chart->addSerie("o", "Operational Expenditure");
	$chart->addSerie("c", "Capital Expenditure");

	$budget = array("r" => $sum['rb'], "o" => $sum['ob'], "c" => $sum['cb']);
	$actual = array("r" => $sum['ra'], "o" => $sum['oa'], "c" => $sum['ca']);

	$chart->addGraph("budget", "Adjusted Budget", $budget, array("color" => "#FF9900"));
	$chart->addGraph("actual", "Actuals", $actual, array("color" => "#009900"));

	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" => $cheight,
		"depth" => $cdepth,
		"angle" => $cangle,
		"font" => "Tahoma",
		"decimals_separator" => ".",
		"thousands_separator" => ",",
		"plot_area.margins.top" => $cmargin_top,
		"plot_area.margins.right" => 20,
		"plot_area.margins.left" => 100,
		"plot_area.margins.bottom" => $cmargin_bottom,
		"background.border_alpha" => 0,
		"legend.enabled" => "false",
		/*"legend.y" => "90%",
		"legend.x" => "25%",
		"legend.width" => "50%",
		"legend.border_alpha" => 20,
		"legend.text_size" => 9,
		"legend.spacing" => 4,
		"legend.margins" => 8,
		"legend.align" => "center",*/
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125;&#93;&#93;>",
		"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
		"column.data_labels_text_color" => "#ffffff",
		"column.width" => $ccolwidth
		//"column.spacing" => 20,
		//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"
	));

	echo html_entity_decode($chart->getCode());

	echo "<table cellpadding=2 cellspacing=0 width=$cwidth style=\"border: 1px solid #FFFFFF;\">";
		echo "<tr>";
			echo "<td width=60 style=\"border: 1px solid #FFFFFF;\">&nbsp;</td>";
			echo "<td width=$lwidth style=\"border: 1px solid #FFFFFF; background-color: #FF9900; font-weight: bold; text-align: center; color: #ffffff; ".$style."\">Adjusted Budget</td>";
			echo "<td width=$lwidth style=\"border: 1px solid #FFFFFF; background-color: #009900; font-weight: bold; text-align: center; color: #ffffff; ".$style."\">Actuals</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td  style=\"border: 1px solid #FFFFFF;text-align: right; ".$style."\"><b>Revenue:</b></td>";
			echo "<td  style=\"border: 1px solid #FFFFFF;text-align: right; background-color: #FED97F; ".$style."\">&nbsp;R ".number_format($sum['rb'],2)."</td>";
			echo "<td  style=\"border: 1px solid #FFFFFF;text-align: right; background-color: #BCFFB1; ".$style."\">&nbsp;R ".number_format($sum['ra'],2)."</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td style=\"border: 1px solid #FFFFFF;text-align: right; ".$style."\"><b>Opex:</b></td>";
			echo "<td style=\"border: 1px solid #FFFFFF;text-align: right; background-color: #FED97F; ".$style."\">&nbsp;R ".number_format($sum['ob'],2)."</td>";
			echo "<td style=\"border: 1px solid #FFFFFF;text-align: right; background-color: #BCFFB1; ".$style."\">&nbsp;R ".number_format($sum['oa'],2)."</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td style=\"border: 1px solid #FFFFFF;text-align: right; ".$style."\"><b>Capex:</b></td>";
			echo "<td style=\"border: 1px solid #FFFFFF;text-align: right; background-color: #FED97F; ".$style."\">&nbsp;R ".number_format($sum['cb'],2)."</td>";
			echo "<td style=\"border: 1px solid #FFFFFF;text-align: right; background-color: #BCFFB1; ".$style."\">&nbsp;R ".number_format($sum['ca'],2)."</td>";
		echo "</tr>";
	echo "</table>";

}

//function drawPie($data_type,$results,$kpi,$chart_title,$title_class,$layout,$legend_layout,$required,$link,$from,$to,$mds,$graph_ref,$groupby) {
//function drawFinBar($data_type,$sum,$chart_title,$title_class,$layout,$required,$graph_ref) {
function drawStackedBar($data_type,$results,$kpi,$chart_title,$title_class,$layout,$legend_layout,$required,$link,$from,$to,$mds,$graph_ref,$group,$values_maxlen) {
	global $krsetup;
	//global $values_maxlen;
	if($required=="Y") {
		require("lib/amcharts2-php/AmBarChart.php");
		AmChart::$swfObjectPath = "swfobject.js";
		AmChart::$libraryPath = "lib/amcharts2/amcolumn/";
		AmChart::$jsPath = "lib/amcharts2-php/AmCharts.js";
		AmChart::$jQueryPath = "lib/amcharts2/amcolumn/jquery.js";
		AmChart::$loadJQuery = true;
	}
	
	if($layout==2) {
		$cwidth = 400;
		$cheight = 200;
		$cdepth = 5;
		$cangle = 25;
		$ccolwidth = 60;
		$style = "font-size: 7pt; line-height: 9pt; padding-right: 10px;";
		$cmargin_top = 20;
		$cmargin_bottom = 40;		
	} else {
		$cmargin_right = 130;
		$cmargin_left = 10;
		$cwidth = $cmargin_right + $cmargin_left*2 + count($group)*80; 
		if($cwidth>650) { 
			$cwidth = 650;
			$legend_x = 530;
		} else {
			$legend_x = count($group)*80+$cmargin_left+20;
		}
		//$cwidth = 600;
		//$legend_x = 470;
		$cmargin_bottom = 100;
		$cheight = 420;
		if(isset($values_maxlen) && $values_maxlen>0) {
			$values_rows = ceil($values_maxlen/10);
			//if($values_rows < 2) { $values_rows = 2; }
			if($values_rows < 10) { 
				$cheight = $cheight - $cmargin_bottom;
				$cmargin_bottom = 20 + 10 * $values_rows;
				$cheight+= $cmargin_bottom;
			}
		}
		//echo ": ".$values_rows." :: ".$cheight." :: ".$cmargin_bottom." :";
		$cdepth = 10;
		$cangle = 30;
		$ccolwidth = 80;
		$font_size = 9;
		$cmargin_top = 20;
		$style="font-size: 8pt; line-height: 10pt; padding-right: 10px;";
	}
	//$lwidth = ($cwidth-60)/2;
	$label_x = $cwidth/2 - (strlen($chart_title)*14)/2;
	
	//echo $cwidth;
	
	$chart = new AmBarChart("kpi_".$graph_ref);
	//$chart->setTitle("<p class=".$title_class." style=\"margin: 10 0 0 0;\">".$chart_title."</p>");
		
	foreach($group as $g) {
		$chart->addSerie($g['id'], chartEncode(decode($g['value'])));
	}

	foreach($results as $r) {
		$kpires = array();
		foreach($group as $g) { if(isset($kpi[$g['id']][$r]) && $kpi[$g['id']][$r]>0) { $kpires[$g['id']] = $kpi[$g['id']][$r]; } }
		$chart->addGraph($r, $krsetup[$r]['value'], $kpires, array("color" => $krsetup[$r]['color']));
	}

	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" => $cheight,
		"depth" => $cdepth,
		"angle" => $cangle,
		"font" => "Tahoma",
		"text_size" => 9,
		"column.type" => "100% stacked",
//		"column.alpha" => 70,
		"column.width" => $ccolwidth,
		"column.data_labels" => "<!".chartEncode("[CDATA[{percents}%]]").">",
		"column.data_labels_text_color" => "#ffffff",
		"decimals_separator" => ".",
//		"thousands_separator" => ",",
		"plot_area.margins.top" => $cmargin_top,
		"plot_area.margins.right" => $cmargin_right,
		"plot_area.margins.left" => $cmargin_left,
		"plot_area.margins.bottom" => $cmargin_bottom-2,
		"background.border_alpha" => 20,
		"grid.category.alpha" => 0,
		"grid.value.alpha" => 0,
		"axes.category.alpha" => 0,
		"axes.value.alpha" => 0,
		"values.value.enabled" => "false",
//		"values.category.rotate" => 90,
		"legend.enabled" => "true",
		"legend.y" => 70,
		"legend.x" => $legend_x,
		"legend.width" => 120,
		"legend.border_alpha" => 0,
		"legend.text_size" => 11,
//		"legend.text_size" => 8,
//		"legend.spacing" => 1,
//		"legend.margins" => 3,
//		"legend.align" => "center",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!".chartEncode("[CDATA[{title}: {value} of {total} ]]").">"
/*		"labels.label.x" => $label_x,
		"labels.label.y" => 15,
		"labels.label.text_size" => 14,
		"labels.label.text" => "<!".chartEncode("[CDATA[ <b>".$chart_title."</b> ]]").">",*/
		//"export_as_image.file" => "lib/amcharts2/amcolumn/export.php",
		//"export_as_image.target" => "_blank",
		//"export_as_image.color" => "0x232323",
		//"export_as_image.alpha" => 40
		//"column.spacing" => 20,
		//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"
	));

	echo html_entity_decode($chart->getCode());

}

function chartEncode($str) {

	//$str = code($str);

	$str = str_replace("&","&#38;",$str);
	$str = str_replace("\"","&#34;",$str);
	
	$str = str_replace("[","&#91;",$str);
	$str = str_replace("]","&#93;",$str);

	$str = str_replace("{","&#123;",$str);
	$str = str_replace("}","&#125;",$str);

	return $str;
}




?>