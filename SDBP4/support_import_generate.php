<?php
$import_section = isset($_REQUEST['i']) ? $_REQUEST['i'] : die("<p>An error occurred while trying to generate the template.  Please go back and try again.</p>");
include("inc/inc_ignite.php"); 
include("inc/inc.php"); 
if($import_section=="LIST") {
	$listoflists = array(
		'all'=>				array('ignite'=>"All Lists",'doc'=>0),
		'dir'=>				array('ignite'=>"Directorates",'doc'=>0),
		'subdir'=>			array('ignite'=>"Sub-Directorates",'doc'=>4),
		'list_gfs'=>		array('ignite'=>"GFS Classification",'doc'=>12),
		'list_munkpa'=>		array('ignite'=>"Municipal KPA",'doc'=>15),
		'list_natkpa'=>		array('ignite'=>"National KPA",'doc'=>19),
		'list_natoutcome'=>	array('ignite'=>"National Outcome",'doc'=>23),
		'list_idp_kpi'=>	array('ignite'=>"Departmental SDBIP: IDP Objective",'doc'=>26),
		'list_idp_top'=>	array('ignite'=>"Top Layer: IDP Objective",'doc'=>31),
		'list_area'=>		array('ignite'=>"Area",'doc'=>50),
		'list_wards'=>		array('ignite'=>"Wards",'doc'=>46),
		'list_owner'=>		array('ignite'=>"KPI Owner / Driver",'doc'=>53),
		'list_fundsource'=>	array('ignite'=>"Funding Source",'doc'=>63),
		'list_kpiconcept'=>	array('ignite'=>"KPI Concept",'doc'=>34),
		'list_kpitype'=>	array('ignite'=>"KPI Type",'doc'=>38),
		'list_riskrating'=>	array('ignite'=>"Risk Rating",'doc'=>42),
	);
	$sql = "SELECT h_client, h_table FROM ".$dbref."_setup_headings WHERE h_type IN ('LIST','WARDS','AREA','FUNDSRC')";
	$client_headings = mysql_fetch_all($sql);
	foreach($client_headings as $chead) {
		$htbl = $chead['h_table'];
		$chv = $chead['h_client'];
		if(isset($listoflists[$htbl])) {
			$listoflists[$htbl]['client'] = $chv;
			$listoflists[$htbl]['h_client'] = $chv;
		} elseif(isset($listoflists['list_'.$htbl])) {
			$listoflists['list_'.$htbl]['client'] = $chv;
			$listoflists['list_'.$htbl]['h_client'] = $chv;
		}
	}
	$mheadings['LIST'] = $listoflists;
} else {
	$mheadings = getModuleHeadings(array($import_section));
}
include("support_import_headings.php");
$import_log_file = openLogImport();


switch($import_section) {
	case "LIST":
/*		$t = isset($_REQUEST['t']) ? $_REQUEST['t'] : "all";
		$fdata = "\"DIRECTORATES\",\"\",\"\",\"\",\"SUB-DIRECTORATES\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"GFS CLASSIFICATION\",\"\",\"\",\"MUNICIPAL KPA\",\"\",\"\",\"\",\"NATIONAL KPA\",\"\",\"\",\"\",\"NATIONAL OUTCOME\",\"\",\"\",\"DEPARTMENTAL: IDP OBJECTIVE\",\"\",\"\",\"\",\"\",\"TOP LAYER: IDP OBJECTIVE\",\"\",\"\",\"KPI CONCEPT\",\"\",\"\",\"\",\"KPI TYPE\",\"\",\"\",\"\",\"RISK RATING\",\"\",\"\",\"\",\"WARDS\",\"\",\"\",\"\",\"AREA\",\"\",\"\",\"DRIVER\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"FUNDING SOURCE\",\"\"\n";
		$fdata.= "\"\",\"Dir Ref\",\"Directorates\",\"\",\"\",\"Directorate\",\"DirRef\",\"Sub-Directorate\",\"Primary\",\"Sub Ref\",\"\",\"\",\"GFS\",\"GFS Ref\",\"\",\"Municipal KPA\",\"Code\",\"MK Ref\",\"\",\"National KPA\",\"Code\",\"NK Ref\",\"\",\"National Outcome\",\"Nout Ref\",\"\",\"IDP Objective\",\"Municipal KPA\",\"National KPA\",\"Obj Ref\",\"\",\"IDP Objective\",\"IDP Ref\",\"\",\"Name\",\"Code\",\"Ref\",\"\",\"Name\",\"Code\",\"Ref\",\"\",\"Name\",\"Code\",\"Ref\",\"\",\"Ignite Ref\",\"Ward Name\",\"Mun. Ref\",\"\",\"Ignite Ref\",\"Area Name\",\"\",\"Driver\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Funding Source\",\"FS Ref\"";
//		$fdata = "\"DIRECTORATES\",\"\",\"\",\"\",\"SUB-DIRECTORATES\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"GFS CLASSIFICATION\",\"\",\"\",\"MUNICIPAL KPA\",\"\",\"\",\"\",\"NATIONAL KPA\",\"\",\"\",\"\",\"NATIONAL OUTCOME\",\"\",\"\",\"DEPARTMENTAL: IDP OBJECTIVE\",\"\",\"\",\"\",\"\",\"TOP LAYER: IDP OBJECTIVE\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"WARDS\",\"\",\"\",\"\",\"AREA\",\"\",\"\",\"DRIVER\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"FUNDING SOURCE\",\"\"\n";
//		$fdata.= "\"\",\"Dir Ref\",\"Directorates\",\"\",\"\",\"Directorate\",\"DirRef\",\"Sub-Directorate\",\"Primary\",\"Sub Ref\",\"\",\"\",\"GFS\",\"GFS Ref\",\"\",\"Municipal KPA\",\"Code\",\"MK Ref\",\"\",\"National KPA\",\"Code\",\"NK Ref\",\"\",\"National Outcome\",\"Nout Ref\",\"\",\"IDP Objective\",\"Municipal KPA\",\"National KPA\",\"Obj Ref\",\"\",\"IDP Objective\",\"IDP Ref\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Ignite Ref\",\"Ward Name\",\"Mun. Ref\",\"\",\"Ignite Ref\",\"Area Name\",\"\",\"Driver\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Funding Source\",\"FS Ref\"";
		break;
		/*$fdata = "\"Ignite\",\"Directorate [R]\",\"\",\"PMS Ref\",\"Reporting Requirement\",\"GFS Classification [R]\",\"\",\"National Outcome [R]\",\"\",\"National KPA [R]\",\"\",\"MTAS Indicator\",\"IDP Objective [R]\",\"\",\"Municipal KPA [R]\",\"\",\"KPI [R]\",\"Unit of Measurement\",\"Risk\",\"Wards [R]\",\"Area [R]\",\"Program Driver [R]\",\"Baseline\",\"POE\",\"Past Year Performance\",\"KPI Calculation Type [R]\",\"\",\"KPI Target Type [R]\",\"\",\"Annual Target\",\"Revised Target\",\"\",\"Q1\",\"Q2\",\"Q3\",\"Q4\",\"\",\"2012/2013\",\"2013/2014\",\"2014/2015\",\"2015/2016\"\n";
		$fdata.= "\"Ref\",\"Dir Ref\",\"Dir Name\",\"40 characters\",\"Y / N\",\"GFS Ref\",\"GFS Value\",\"Ref\",\"Value\",\"Ref\",\"Value\",\"200 characters\",\"Ref\",\"Value\",\"Ref\",\"Value\",\"200 characters\",\"200 characters\",\"200 characters\",\"Mun Ref ;\",\"Ignite Ref ;\",\"Value\",\"200 characters\",\"200 characters\",\"100 characters\",\"Code\",\"Value\",\"Ref\",\"Value\",\"Number\",\"Number\",\"\",\"Number\",\"Number\",\"Number\",\"Number\",\"\",\"Number\",\"Number\",\"Number\",\"Number\"";
		break;*/
	case "CAP":
		//$fdata = "\"Ignite\",\"Sub-Directorate [R]\",\"\",\"\",\"GFS Classification [R]\",\"\",\"Mun CP Ref\",\"IDP Number\",\"Vote Number\",\"Project name [R]\",\"Project Description\",\"Funding source [R]\",\"Planned Start Date [R]\",\"Planned Completion Date [R]\",\"Actual Start Date\",\"Ward [R]\",\"Area [R]\",\"\",\"July 2011\",\"August 2011\",\"September 2011\",\"October 2011\",\"November 2011\",\"December 2011\",\"January 2012\",\"February 2012\",\"March 2012\",\"April 2012\",\"May 2012\",\"June 2012\",\"Total\",\"2011/2012\",\"\",\"2012/2013\",\"\",\"2013/2014\",\"\",\"2014/2015\",\"\",\"2015/2016\",\"\"\n";
		//$fdata.= "\"Ref\",\"Ignite\",\"Directorate\",\"List\",\"Ignite\",\"List\",\"40 characters\",\"40 characters\",\"40 characters\",\"200 characters\",\"65000 characters\",\"Ignite ref ;\",\"YYYY/MM/DD\",\"YYYY/MM/DD\",\"YYYY/MM/DD\",\"Mun. Ref separated ;\",\"Ignite ref separated by ;\",\"\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"\",\"CRR\",\"Other\",\"CRR\",\"Other\",\"CRR\",\"Other\",\"CRR\",\"Other\",\"CRR\",\"Other\"";
		//break;
	case "TOP":
	case "KPI":
	case "RS":
	case "CF":
		if($import_section=="KPI") { $fdata = "\" \",\" \"\n"; }  else { $fdata = ""; }
		$cdata = array();
		for($i=1;$i<=count($import_columns);$i++) {
			if(isset($cells[$i-1])) {
				$col = $import_columns[$i-1];
				$fld = isset($col['fld']) ? $col['fld'] : "";
				if(strlen($fld)==0) {
					$txt = "";
				} elseif($fld==strtolower($import_section)."_id") { 
					$txt = $col['txt']; 
				} elseif($i>$forecast2_start) { 
					$txt = ($i%2==1 ? "" : $col['txt']);
				} elseif($i>$target_start) {
					if($import_section=="CF") {
						$f = explode("_",$fld);
						if($f[1]=="rev") {
							$txt = $month_head[$f[3]];
						} else {
							$txt = "";
						}
					} else {
						$txt = isset($col['txt']) && strlen($col['txt'])>0 ? $col['txt'] : "";
					}
				} else {
					$txt = isset($mheadings[$import_section][$fld]['h_client']) ? $mheadings[$import_section][$fld]['h_client'] : "";
				}
			} else {
				$txt = "";
			}
			$cdata[]="\"".str_replace('"',"'",decode($txt))."\"";
		}
		$fdata.=implode(",",$cdata)."\n";
		$cdata = array();
		//sub headings
		for($i=1;$i<=count($import_columns);$i++) {
				$col = $import_columns[$i-1];
				$fld = isset($col['fld']) ? $col['fld'] : "";
				if($fld==strtolower($import_section)."_id") {  
					$txt = ""; 
				} else {
					if(strlen($fld)==0) { 
						$txt = "";
					} elseif($i>$forecast2_start) { 
						$txt = ($i%2==1 ? "Other" : "CRR");
					} elseif($i>$forecast_start) { 
						$txt = "Forecast ".$target_text;
					} elseif($i>$target_start) { 
						if($import_section=="CF") { 
							$calc = ($i-1-$target_start)%3;
							switch($calc) {
							case 0:	$txt = "Revenue"; break;
							case 1: $txt = "Op. Exp."; break;
							case 2: $txt = "Cap. Exp."; break;
							}
						} else {
							$txt = $target_text;
						}
					} else {
						if(in_array($mheadings[$import_section][$fld]['h_type'],array("TEXT","VC"))) {
							if($mheadings[$import_section][$fld]['c_maxlen']>0) {
								$txt = $mheadings[$import_section][$fld]['c_maxlen']." characters";
							} else {
								$txt = "";
							}
						} else {
							$txt = isset($col['txt']) && strlen($col['txt'])>0 ? $col['txt'] : "";
						}
					}
				}
			$cdata[]="\"".str_replace('"',"'",decode($txt))."\"";
		}
		$fdata.=implode(",",$cdata)."\n";
		break;
	/*case "CF":
		$fdata = "\"Sub-Directorate [R]\",\" \",\" \",\"Line Item [R]\",\"GFS Classification [R]\",\" \",\"Vote Number\",\" \",\"July 2011\",\" \",\" \",\"August 2011\",\" \",\" \",\"September 2011\",\" \",\" \",\"October 2011\",\" \",\" \",\"November 2011\",\" \",\" \",\"December 2011\",\" \",\" \",\"January 2012\",\" \",\" \",\"February 2012\",\" \",\" \",\"March 2012\",\" \",\" \",\"April 2012\",\" \",\" \",\"May 2012\",\" \",\" \",\"June 2012\",\" \",\" \"\n";
		$fdata.= "\"Ignite\",\"Directorate\",\"List\",\"200 characters\",\"Ignite\",\"List\",\"100 characters\",\" \",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\",\"Revenue\",\"Op. Exp.\",\"Cap. Exp.\"";
		break;
	/*case "RS":
		$fdata = "\"Ignite\",\"Line Item [R]\",\"Vote Number\",\"July\",\"August\",\"September\",\"October\",\"November\",\"December\",\"January\",\"February\",\"March\",\"April\",\"May\",\"June\"\n";
		$fdata.= "\"Ref\",\"200 characters\",\"100 characters\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\",\"Number\"";
		break;*/
}
		
		//SAVE FILES
        $folder = $modref."/import";
		$filename = strtolower($import_section)."_template_".date("YmdHis").".csv";
		saveEcho($folder, $filename, $fdata);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $newfilename = strtolower($cmpcode)."_".strtolower($import_section).".csv";
        $content = 'text/plain';
		downloadFile2($folder,$filename,$newfilename,$content);
		
		logImport($import_log_file, "Generate Template for ".$import_section." [".$filename."]", $_SERVER['PHP_SELF']);


fclose($import_log_file);
?>