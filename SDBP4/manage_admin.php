<?php 
$section = "KPI";
$get_lists = true;
if(!isset($_REQUEST['page_id']) || $_REQUEST['page_id']!="change") {
	$get_active_lists = false;
} else {
	if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") { $get_active_lists = false; } else { $get_active_lists = true; }
}
$locked_column = (isset($_REQUEST['page_id']) && $_REQUEST['page_id']=="deptcreate") ? false : true;
$get_open_time = true;
include("inc/header.php"); 

if(!isset($_REQUEST['page_id']) || $_REQUEST['page_id']!="change") {
	$active_fld = "secondary";
	$time = getOpenTime($time,$active_fld);
} else {
	$time = getTime($get_time,false);
}
if(count($time)==0)  { $time = getTime($get_time,false); }

switch($page_id) {
case "pms":
	include("manage_pms.php");
	break;
case "plc":
	include("SDBP3/admin_kpi_plc.php");
	break;
case "change":
	/* SETUP VARIABLES */
	$nosort = array(
		$table_fld."calctype", $table_fld."targettype",
		"kpi_topid","kpi_capitalid",
		$table_fld."wards",$table_fld."area",
		"dir",$table_fld."subid",
	);
	$nogroup = array(
		$table_fld."mtas",	$table_fld."value",	$table_fld."unit",
		$table_fld."riskref",	$table_fld."risk",
		$table_fld."baseline",	$table_fld."pyp",	$table_fld."perfstd", $table_fld."poe",
		$table_fld."idpref",
	);
	$head0['dir'] = $mheadings['dir'][0];
	$head = array_merge($head0,$mheadings[$section]);

	$src = isset($_REQUEST['src']) ? $_REQUEST['src'] : "GENERATOR";
	if($src=="GRAPH" && isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
		$_REQUEST['fields'] = array(
				0 => "dir",
				1 => "kpi_subid",
				2 => "kpi_topid",
				3 => "kpi_capitalid",
				4 => "kpi_gfsid",
				5 => "kpi_idpref",
				6 => "kpi_natoutcomeid",
				7 => "kpi_idpid",
				8 => "kpi_natkpaid",
				9 => "kpi_munkpaid",
				10 => "kpi_mtas",
				11 => "kpi_value",
				12 => "kpi_unit",
				13 => "kpi_conceptid",
				14 => "kpi_typeid",
				15 => "kpi_riskref",
				16 => "kpi_risk",
				17 => "kpi_riskratingid",
				18 => "kpi_wards",
				19 => "kpi_area",
				20 => "kpi_ownerid",
				21 => "kpi_baseline",
				22 => "kpi_pyp",
				23 => "kpi_perfstd",
				24 => "kpi_poe",
				25 => "kpi_calctype",
				26 => "results",
				27 => "kr_perf",
				28 => "kr_correct",
			);
		//$_REQUEST['r_from'] => from graph report
		//$_REQUEST['r_to'] => from graph report
		$_REQUEST['r_calc'] = "ytd";
		$_REQUEST['summary'] = "Y";
		//$_REQUEST['filter']['group_by_filter'] = array([0]=>#) => from graph where applicable
		//$_REQUEST['filter']['result'] = # => from graph
		//$_REQUEST['groupby'] => from graph
		$_REQUEST['filter']['kpi_wards'] = array("ANY");
		$_REQUEST['filter']['kpi_area'] = array(1);
		$_REQUEST['sort'] = array(
				0 => "dir",
				1 => "kpi_gfsid",
				2 => "kpi_idpref",
				3 => "kpi_natoutcomeid",
				4 => "kpi_idpid",
				5 => "kpi_natkpaid",
				6 => "kpi_munkpaid",
				7 => "kpi_mtas",
				8 => "kpi_value",
				9 => "kpi_unit",
				10 => "kpi_conceptid",
				11 => "kpi_typeid",
				12 => "kpi_riskref",
				13 => "kpi_risk",
				14 => "kpi_riskratingid",
				15 => "kpi_ownerid",
				16 => "kpi_baseline",
				17 => "kpi_pyp",
				18 => "kpi_perfstd",
				19 => "kpi_poe",
			);	
		$_REQUEST['output'] = "display";
		$_REQUEST['rhead'] = "";
	}
//arrPrint($_REQUEST);
		if(isset($_REQUEST['act']) && $_REQUEST['act']=="GENERATE") {
			include("report_kpi_process.php");
		} else {
			include("report_kpi_generate.php");
		}
	break;
case "deptadmin":
	if(!$import_status[$section]) {
		die("<p>Departmental SDBIP has not yet been created.  Please try again later.</p>");
	} else {
		/*** Filter settings ***/
		$filter = isset($_SESSION[$self."_".$page_id]) ? $_SESSION[$self."_".$page_id] : array();
		//WHEN
		if(isset($_REQUEST['filter_when'])) {
			$filter_when = $_REQUEST['filter_when'];
			$filter['when'] = $filter_when;
		} elseif(!isset($filter['when'])) {
			foreach($time as $ti => $t) {
				$filter_when = $ti;
			}
			$filter['when'] = $filter_when;
		}
		if(!isset($filter['when']) || strlen($filter['when'])==0) { $filter['when'] = 12; }
		//WHO
		if(isset($_REQUEST['filter_who'])) {
			$f_who = $_REQUEST['filter_who'];
			if(strtolower($f_who)=="all") {
				$filter_who = array("","All");
			} else {
				$filter_who = explode("_",$f_who);
			}
			$filter['who'] = $filter_who;
		} elseif(!isset($filter['who'])) {
			$filter_who = array("","All");
			$filter['who'] = $filter_who;
		}
		//WHAT
		if(isset($_REQUEST['filter_what'])) {
			$filter['what'] = $_REQUEST['filter_what'];
		} elseif(!isset($filter['what'])) {
			$filter['what'] = "All";
		}
		//DISPLAY
		if(isset($_REQUEST['filter_display'])) {
			$filter['display'] = $_REQUEST['filter_display'];
		} elseif(!isset($filter['display'])) {
			$filter['display'] = "LIMIT";
		}

		/*** Departmental Details ***/
		$dir_sql = "SELECT d.id as dir, s.id as subdir FROM ".$dbref."_dir d
			INNER JOIN ".$dbref."_subdir s ON s.dirid = d.id AND s.active = true AND s.id IN (SELECT DISTINCT k.kpi_subid FROM ".$dbref."_kpi k
			INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
			WHERE kpi_active = true ) WHERE d.active = true";
		$valid_dir_sub = mysql_fetch_alls2($dir_sql,"dir","subdir");
		
		//drawViewFilter($who,$what,$when,$sel,$filter)
		$head = $time[$filter['when']]['display_full'];
		$filter['who'] = drawManageFilter(true,true,true,true,$filter,$head);

		//SQL

		$object_sql1 = " ".$dbref."_kpi k
			INNER JOIN ".$dbref."_subdir subdir ON kpi_subid = subdir.id AND subdir.active = true
			".($filter['who'][0]=="D" && checkIntRef($filter['who'][1]) ? " AND subdir.dirid = ".$filter['who'][1] : "")."
			INNER JOIN ".$dbref."_dir dir ON subdir.dirid = dir.id AND dir.active = true
			WHERE kpi_active = true ".($filter['who'][0]=="S" && checkIntRef($filter['who'][1]) ? " AND kpi_subid = ".$filter['who'][1] : "");
			if(isset($filter['what']) && strtolower($filter['what'])!="all") {
				switch($filter['what']) {
					case "op":	$object_sql1.= " AND kpi_capitalid = 0 AND kpi_topid = 0"; break;
					case "cap":	$object_sql1.= " AND kpi_capitalid > 0"; break;
					case "top":	$object_sql1.= " AND kpi_topid > 0"; break;
				}
			}

	function getResults() {	
		global $dbref;
		global $filter;
		global $object_sql1;
		$results_sql = "SELECT r.* FROM ".$dbref."_kpi_results r
			INNER JOIN ".$dbref."_kpi k ON r.kr_kpiid = k.kpi_id AND k.kpi_active = true
			WHERE kr_timeid = ".$filter['when']." AND kr_kpiid IN (SELECT k.kpi_id FROM ".$object_sql1.")";
		
		return mysql_fetch_alls2($results_sql,"kr_kpiid","kr_timeid");
	}
		$results = getResults();
		$fld_target = "kr_target";
		$fld_actual = "kr_actual";
		
		if(isset($mheadings['KPI']['kpi_topid'])) {
			$top_sql = "SELECT t.top_id, t.top_value FROM ".$dbref."_top t
				WHERE t.top_id IN (SELECT k.kpi_topid FROM ".$object_sql1.") AND t.top_active = true";
			$tops = mysql_fetch_alls($top_sql,"top_id");
		}
		if(isset($mheadings['KPI']['kpi_capitalid'])) {
			$cap_sql = "SELECT c.cap_id, c.cap_name FROM ".$dbref."_capital c
				WHERE c.cap_id IN (SELECT k.kpi_capitalid FROM ".$object_sql1.") AND c.cap_active = true";
			$caps = mysql_fetch_alls($cap_sql,"cap_id");
		}
		if(isset($mheadings['KPI']['kpi_wards'])) {
			$wards_sql = "SELECT kw_kpiid, id, value, code FROM ".$dbref."_kpi_wards INNER JOIN ".$dbref."_list_wards ON kw_listid = id 
				WHERE kw_active = true AND kw_kpiid IN (SELECT k.kpi_id FROM ".$object_sql1.")";
			$wa['kpi_wards'] = mysql_fetch_alls2($wards_sql,"kw_kpiid","id");
		}
		if(isset($mheadings['KPI']['kpi_area'])) {
			$area_sql = "SELECT ka_kpiid, id, value, code FROM ".$dbref."_kpi_area INNER JOIN ".$dbref."_list_area ON ka_listid = id 
				WHERE ka_active = true AND ka_kpiid IN (SELECT k.kpi_id FROM ".$object_sql1.")";
			$wa['kpi_area'] = mysql_fetch_alls2($area_sql,"ka_kpiid","id");
		}

		$object_sql = "SELECT k.* FROM ".$object_sql1;
		
		$_SESSION[$self."_".$page_id] = $filter;


		include("manage_table_edit.php");

	
//	arrPrint($filter);
//echo $object_sql;
	}	//if import_status['import_Section'] == false else	
	break;	//case: deptadmin
case "deptcreate":
	include("manage_table_detail_create.php");
	break;
}
if($page_id !="change" || (isset($_REQUEST['act']) && $_REQUEST['act']!="GENERATE")) {
	echo "</body>
	</html>";
}
?>