<?php
function calculateVar($arr) {
	return ($arr['budget']-$arr['actual']);
}
	//GET DATA
		//$time_id = date("m") + ( (date("m")<6) ? 6 : -6);
		//$filter_to = $current_time_id;
		$time_id = $filter_to;
		
		$test_sql = "SELECT cr_timeid, sum( cr_opex_5 ) AS actual FROM ".$dbref."_".$table_tbl."_results 
					WHERE cr_timeid <= ".$time_id." GROUP BY cr_timeid ORDER BY cr_timeid DESC";
		$rs = getRS($test_sql);
		while($row = mysql_fetch_assoc($rs)) {
			if($row['actual']>0) { $filter_to = $row['cr_timeid']; break; }
		}
		mysql_close();
//echo "<p style=\"font-style: italic;\">Year to date (".$time[1]['display_full']." - ".$time[$filter_to]['display_full'].") drawn on ".date("d F Y")." at ".date("H:i:s").".</p>";
$cf_sections = array('rev'=>"Revenue",'opex'=>"Operational Expenditure",'capex'=>"Capital Expenditure");
		$dir = array();
		$results_sql = "SELECT cf_subid as sub, sum(cr_opex_4) as opex_budget, sum(cr_opex_5) as opex_actual";
		foreach($cf_sections as $cs_id => $cs) {
			$dir[$cs_id]=array('budget'=>0,'actual'=>0);
			$results_sql.= ", sum(cr_".$cs_id."_4) as ".$cs_id."_budget, sum(cr_".$cs_id."_5) as ".$cs_id."_actual";
		}
		$results_sql.= " FROM ".$dbref."_".$table_tbl."_results
						INNER JOIN ".$dbref."_".$table_tbl." ON cf_id = cr_cfid AND cf_active = true
						INNER JOIN ".$dbref."_subdir ON id = cf_subid AND active = true AND dirid = ".$graph_id."
						WHERE cr_timeid <= ".$filter_to." 
						GROUP BY cf_subid";
		$results = mysql_fetch_alls($results_sql,"sub");
		$sub_chartData = array();
		$dir_Data = array();
		foreach($cf_sections as $cs_id => $cs) {
			foreach($results as $i => $r) {
				$dir[$cs_id]['actual']+= $r[$cs_id.'_actual'];
				$dir[$cs_id]['budget']+= $r[$cs_id.'_budget'];
					$sval = explode(" ",decode(decode($lists['subdir'][$i]['value'])));
					$s_name = "";
					$s_n = "";
					foreach($sval as $sv) {
						if(strlen($s_n.$sv)>20) {
							$s_name.=$s_n."\\n";
							$s_n = "";
						}
						$s_n.=" ".$sv;
					}
					$s_name.=" ".$s_n;

				$sub_chartData[$cs_id][] = "{text:\"".$s_name."\",budget:".number_format($r[$cs_id.'_budget'],2,".","").",actual:".number_format($r[$cs_id.'_actual'],2,".","")."}";
			}
			$dir_Data[] = "{text:\"$cs\",budget:".number_format($dir[$cs_id]['budget'],0,".","").",actual:".number_format($dir[$cs_id]['actual'],0,".","")."}";
		}
	//if(array_sum($dir)>0) {
		$graph_width = count($sub_chartData['opex'])*120+50;
		$total['dir'] = array('budget'=>0,'actual'=>0);
		?>
<div align=center>
	<table width=<?php echo $graph_width+20; ?> class=whiteborder>
		<tr>
			<th><?php echo $lists['dir'][$graph_id]['value']; ?></th>
		</tr>
		<tr class=no-highlight>
			<td class=whiteborder style="padding-bottom: 15px;"><div align=center>
				<div id="dir" style="width:550px; height:250px; background-color:#ffffff;"></div>
				<table class=whiteborder>
					<thead><tr class=no-highlight><td class=whiteborder></td><th class=time2>Budget</th><th class=time3>Actual</th><th class=total>Variance</th></tr></thead>
					<tbody>
						<?php foreach($cf_sections as $cs_id => $cs) { ?>
							<tr>
								<td class="whiteborder right" style="font-weight: bold; line-height: 14pt;"><?php echo $cs; ?></td>
								<td class="time2 right whiteborder"><?php echo number_format($dir[$cs_id]['budget'],2); $total['dir']['budget']+= $dir[$cs_id]['budget']; ?></td>
								<td class="time3 right whiteborder"><?php echo number_format($dir[$cs_id]['actual'],2);  $total['dir']['actual']+= $dir[$cs_id]['actual'];?></td>
								<td class="total right whiteborder"><?php echo number_format(calculateVar($dir[$cs_id]),2); ?></td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
							<tr>
								<td class="whiteborder right totaltext" style="line-height: 14pt;">Total:</td>
								<td class="time2 right whiteborder"><?php echo number_format($total['dir']['budget'],2); ?></td>
								<td class="time3 right whiteborder"><?php echo number_format($total['dir']['actual'],2); ?></td>
								<td class="total right whiteborder"><?php echo number_format(calculateVar($total['dir']),2); ?></td>
							</tr>
					</tfoot>
				</table>
			</div></td>
		</tr>
		<tr>
			<th><?php echo $head_sub; ?>s</th>
		</tr>
	<?php foreach($cf_sections as $cs_id => $cs) { $total['sub'] = array('budget'=>0,'actual'=>0); ?>	
		<tr>
			<td class=whiteborder style="padding-bottom: 15px;"><div align=center>
				<span style="font-weight: bold; font-size: 10pt; line-height: 14pt;"><?php echo $cs ?></span>
				<div id="<?php echo $cs_id; ?>" style="height:250px; background-color:#ffffff;"></div>
				<table class=whiteborder>
					<thead><tr class=no-highlight><td class=whiteborder></td><th class=time2>Budget</th><th class=time3>Actual</th><th class=total>Variance</th></tr></thead>
					<tbody>
						<?php foreach($lists['subdir']['dir'][$graph_id] as $s_id => $s) { $sub_total = array('budget'=>0,'actual'=>0);  ?>
							<tr>
								<td class="whiteborder right" style="font-weight: bold; font-size: 7pt;"><?php echo $s['value']; ?></td>
								<td class="time2 right whiteborder"><?php 
									if(isset($results[$s_id][$cs_id.'_budget']) && $results[$s_id][$cs_id.'_budget']>0) {
										echo number_format($results[$s_id][$cs_id.'_budget'],2); 
										$sub_total['budget']+=$results[$s_id][$cs_id.'_budget'];
									} else {
										echo "-";
									} ?></td>
								<td class="time3 right whiteborder"><?php 
									if(isset($results[$s_id][$cs_id.'_actual']) && $results[$s_id][$cs_id.'_actual']>0) {
										echo number_format($results[$s_id][$cs_id.'_actual'],2);
										$sub_total['actual']+=$results[$s_id][$cs_id.'_actual'];
									} else {
										echo "-";
									} ?></td>
								<td class="total right whiteborder"><?php echo calculateVar($sub_total)!=0 ? number_format(calculateVar($sub_total),2) : "-"; ?></td>
							</tr>
						<?php $total['sub']['budget']+=$sub_total['budget']; $total['sub']['actual']+=$sub_total['actual']; } ?>
					</tbody>
					<tfoot>
						<tr>
							<td class="whiteborder right totaltext">Total:</td>
							<td class="time2 right whiteborder"><?php echo number_format($total['sub']['budget'],2); ?></td>
							<td class="time3 right whiteborder"><?php echo number_format($total['sub']['actual'],2); ?></td>
							<td class="total right whiteborder"><?php echo number_format(calculateVar($total['sub']),2); ?></td>
						</tr>
					</tfoot>
				</table>
			</div></td>
		</tr>
	<?php } ?>
	</table>
</div>
        <script type="text/javascript">
        var chart;
		var dir_chartData = [<?php echo implode(",",$dir_Data); ?>];
	<?php 
	foreach($cf_sections as $cs_id => $cs) {
		echo chr(10)."var sub_".$cs_id."_chartData = [".implode(",",$sub_chartData[$cs_id])."];";
	}
	?>
			$(document).ready(function() {
				$("tr").unbind("mouseenter mouseleave");
				var valAxis = new AmCharts.ValueAxis();
					valAxis.gridAlpha = 0.1;
					valAxis.axisAlpha = 0;
					valAxis.minimum = 0;
					valAxis.fontSize=8;
				dir_chart = new AmCharts.AmSerialChart();
					dir_chart.dataProvider = dir_chartData;
					dir_chart.categoryField = "text";
					dir_chart.plotAreaBorderAlpha = 0.2;
					dir_chart.angle = 45;
					dir_chart.depth3D = 15;
					dir_chart.columnSpacing = 1;
					dir_chart.columnWidth = 0.9;
				var graph = new AmCharts.AmGraph();
					graph.title = "Budget";
					graph.valueField = "budget";
					graph.labelText = "[[value]]";
					graph.fontSize = 8;
					graph.type = "column";
					graph.lineAlpha = 0;
					graph.fillAlphas = 1;
					graph.lineColor = "#FE9900";
					dir_chart.addGraph(graph);
				var graph = new AmCharts.AmGraph();
					graph.title = "Actual";
					graph.valueField = "actual";
					graph.labelText = "[[value]]";
					graph.fontSize = 8;
					graph.type = "column";
					graph.lineAlpha = 0;
					graph.fillAlphas = 1;
					graph.lineColor = "#009900";
					dir_chart.addGraph(graph);
					dir_chart.addValueAxis(valAxis);

				dir_chart.categoryAxis.gridAlpha = 0.1;
				dir_chart.categoryAxis.axisAlpha = 0;
				dir_chart.categoryAxis.gridPosition = "start";
				
				dir_chart.write("dir");
				<?php 
				foreach($cf_sections as $cs_id => $cs) {
					echo chr(10)."	".$cs_id."_chart = new AmCharts.AmSerialChart();
										".$cs_id."_chart.dataProvider = sub_".$cs_id."_chartData;
										".$cs_id."_chart.categoryField = \"text\";
										".$cs_id."_chart.plotAreaBorderAlpha = 0.2;
										".$cs_id."_chart.angle = 45;
										".$cs_id."_chart.depth3D = 15;
										".$cs_id."_chart.fontSize = 7;
									var ".$cs_id."graph = new AmCharts.AmGraph();
										".$cs_id."graph.title = \"Budget\";
										".$cs_id."graph.valueField = \"budget\";
										".$cs_id."graph.type = \"column\";
										".$cs_id."graph.lineAlpha = 0;
										".$cs_id."graph.fillAlphas = 1;
										".$cs_id."graph.lineColor = \"#FE9900\";
										".$cs_id."_chart.addGraph(".$cs_id."graph);
									var ".$cs_id."graph = new AmCharts.AmGraph();
										".$cs_id."graph.title = \"Actual\";
										".$cs_id."graph.valueField = \"actual\";
										".$cs_id."graph.type = \"column\";
										".$cs_id."graph.lineAlpha = 0;
										".$cs_id."graph.fillAlphas = 1;
										".$cs_id."graph.lineColor = \"#009900\";
										".$cs_id."_chart.addGraph(".$cs_id."graph);
									
									".$cs_id."_chart.addValueAxis(valAxis);
									".$cs_id."_chart.categoryAxis.gridAlpha = 0.5;
									".$cs_id."_chart.categoryAxis.axisAlpha = 0;
									".$cs_id."_chart.categoryAxis.gridPosition = \"start\";
									
									".$cs_id."_chart.write(\"".$cs_id."\");";
				} 
				?>
			});
	    </script>
<?php 
	/*} else {
		echo "<p>No data to display for this ".$head_dir."</p>";
	}*/
displayGoBack("view_dash.php","Go Back"); ?>