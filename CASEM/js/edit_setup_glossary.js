// JavaScript Document
$(function(){
	
	$("#edit_glossary").click(function(){	
		var message  	= $("#glossary_messsage");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			message.html("Please select category")
			return false;
		} else if ( terminology == "" ) {
			message.html("Please enter the termilogy")
			return false;
		} else if ( explanation == "" ){
			message.html("Please enter the explanation")
			return false;
		} else  {
			
			$.post("controller.php?action=updateGlossary",
			  {
				id			: $("#glossary_id").val(),
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData == 1 ) {
					message.html("Glossary term saved");
				} else {
					message.html("Error saving the glossary term ");
				}
			});
		 }
				return false;
	});		   
})