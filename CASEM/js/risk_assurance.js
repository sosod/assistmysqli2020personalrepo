// JavaScript Document
$(function(){
		$(".datepicker").datepicker({	
			showOn			: "button",
			buttonImage 	: "/library/jquery/css/calendar.gif",
			buttonImageOnly	: true,
			changeMonth		:true,
			changeYear		:true,
			dateFormat 		: "dd-M-yy"
		});		

	$.get( "controller.php?action=getRiskAssurance", { id:$("#risk_id").val() }, function( riskData ){
		$.each( riskData, function( index, query){
			$("<tr />")
			.append($("<th />",{html:index+":"}).css({"text-align":"left"}))
			.append($("<td />",{html:query}))
			.insertAfter($("#risk_assurance_table"))	
		});
	
		$("#view_actions").live("click", function(){
			document.location.href = "../actions/view.php?id="+$("#risk_id").val();
			return false;
		});
	},"json");

	
	$("#cancel_newRisk_assurance").live("click", function(){
		$("#new_risk_assurance").fadeOut();
		return false;
	});
	
})