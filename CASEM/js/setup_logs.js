/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/21/11
 * Time: 2:17 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

   $(".logs").click(function(){
       var id = $(this).attr("id");
       var table = id.replace("view_", "");
       $.post("controller.php?action=view_log", { table_name : table}, function( data ){
            $("#log_div").html("");
            if( $.isEmptyObject(data)) {
                $("<div />",{id:"logs",html:"There are no audit logs "}).insertAfter("#log_div")
            } else {

               $("#log_div").append($("<div />",{id:"logs"}).append($("<table />",{id:"log_table"})))

                $("#log_table").append($("<tr />")
                    .append($("<th />",{html:"Date"}).addClass("log"))
                    .append($("<th />",{html:"Ref"}).addClass("log"))
                    .append($("<th />",{html:"Audit log"}).addClass("log"))
                )
                $.each( data, function( index, log){
                    $("#log_table").append($("<tr />")
                        .append($("<td />",{html:log.date}))
                        .append($("<td />",{html:log.ref}))
                        .append($("<td />",{html:log.changes}))
                    )
                });
            }

       },"json");

   });


})