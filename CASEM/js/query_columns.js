$(function(){
	
	
	$("#querycolumns tbody").sortable({
		items	: "tr:not(.disabled)"		
	})
	
	QueryColumns.get();
	
});


var QueryColumns 		= {
		
		get			: function()
		{
			$.getJSON("controller.php?action=getQueryColumns", function( data ){
				$.each( data, function( index, column){
					$("#querycolumns").append($("<tr />")
					  .append($("<td />",{html:column.client_terminology}))		
					  .append($("<td />")
						 .append($("<input />",{type:"checkbox", name:"manage", id:"manage_"+column.id, value:column.id }))	  
					  )
					  .append($("<td />")
						.append($("<input />",{type:"checkbox", name:"new", id:"new_"+column.id, value:column.id }))
						.append($("<input />",{type:"hidden", name:"position", id:"position", value:column.id }))
					  )
					)	
					if((column.active & 4) == 4)
					{
	   				   $("#manage_"+column.id).attr("checked", "checked");
					} 
					if((column.active & 8) ==8)
					{    
					   $("#new_"+column.id).attr("checked", "checked");
					}    
										
					//$("#new_"+column.id).attr("checked", ((column.active & 8) == 8 ? "checked" : ""));
					
				});
				
				$("#querycolumns").append($("<tr />").addClass("disabled")
				  .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
					.append($("<input />",{type:"button", name:"update", id:"update", value:"Update Changes"}))	  
				  )		
				)				
			});

			
			$("#update").live("click", function(){
				jsDisplayResult("info", "info", "Updating columns . . . <img src='../images/loaderA32.gif' >");
				$.post("controller.php?action=updateColumns",{
					data 	: $("#querycolumnsform").serializeArray()					
				}, function( response ) {
					if( response.error ) {
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text);
					}
				},"json")
				return false;
			});
			
		} , 
		
		update		: function()
		{
			
			
			
		}  
	
};
