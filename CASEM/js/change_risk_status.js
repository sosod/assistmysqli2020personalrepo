// JavaScript Document
$(function(){
	
	$("#save_status").click(function(){
		var message = $("#risk_status_message");
			$.post("controller.php?action=changeRiskStatus",
				   { 
				   	id		: $("#risk_status_id").val(),
					status 	: $("#status_status :selected").val()
					}, function( deacData ) {
				if( deacData == 1){
					message.html("Status has been changed").animate({opacity:"0.0"},4000);
				} else if( deacData == 0){
					message.html("No change was made to the status ").animate({opacity:"0.0"},4000);
				} else {
					message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
				}		   
			});	  
			return false;								 
	});

	$("#cancel").click(function(){
		history.back();
		return false;
	});
});