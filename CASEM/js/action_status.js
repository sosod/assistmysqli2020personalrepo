$(function(){
	
	$(".textcounter").textcounter({maxLength:100});
	
	if( $("#action_status_id").val() == undefined)
	{
		ActionStatus.get();		
	}
	
	$("#add_actionstatus").click( function() {
		ActionStatus.save();
		return false;
	})
	
	$("#edit_actionstatus").click( function() {
		ActionStatus.edit();
		return false;
	});
	
	$("#changes_actionstatus").click(function(){
		ActionStatus.change();
		return false;
	})
	
	$("#cancel_actionstatus").click(function(){
		history.back();
		return false;								   
	})	
	
});


var ActionStatus	= {
		
	get				: function()
	{
		$.getJSON( "controller.php?action=getActionStatus" , function( data ) {
			$.each( data, function( index, val){
				ActionStatus.display( val )				
			})
		})
	} , 
	
	save			: function()
	{
		var name 		= $("#actionstatus").val();
		var client_term = $("#action_client_term").val();
		var color 		= $("#action_color").val();		
		if ( name == "" ) {
			jsDisplayResult("error", "error", "Please enter the action status name");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving ...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=newActionStatus", {
				name		: name,
				client_term	: client_term,
				color		: color
			}, function( response ) {
				if( response.error ) {
					jsDisplayResult("error", "error", response.text );
				} else {
					$("#actionstatus").val("");
					$("#action_client_term").val("")
					$("#action_color").val("");
					jsDisplayResult("ok", "ok",  response.text );
					var data = { id : response.id, name : name, client_terminology:client_term, color : color, status : 1 };
					ActionStatus.display( data );
				}
			},"json");	
		}	
	} , 
	
	edit			: function()
	{
		var name 		= $("#actionstatus").val();
		var client_term = $("#action_client_term").val();
		var color 		= $("#action_color").val();		
		if ( name == "" ) {
			jsDisplayResult("error", "error", "Please enter the action status name");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating ...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=updateActionStatus", 
			   {
				 id				: $("#action_status_id").val(),
				 name			: name,
				 client_term	: client_term,
				 client_terminology : client_term,
				 color			: color
				}, function( retData ) {
					if( retData == 1 ) {
						jsDisplayResult("ok", "ok", "Action status successfully updated");						
					} else if( retData == 0 ){
						jsDisplayResult("info", "info", "Action status not changed");
					} else {
						jsDisplayResult("error", "error", "Error updating the action staus");
					}
			},"json");	
		}
		
	} ,
	
	change			: function()
	{
		jsDisplayResult("info", "info", "Updating ...  <img src='../images/loaderA32.gif' />");
		$.post( "controller.php?action=changeActionstatus",
			   {
				   id		: $("#action_status_id").val(), 
				   status 	: $("#action_status_status :selected").val()
				   }, function( deactData ){
			if( deactData == 1 ) {
				jsDisplayResult("ok", "ok", "Status updated successfully")
			} else if( deactData == 0 ) {
				jsDisplayResult("ok", "ok", "No changes were made")
			} else {
				jsDisplayResult("error", "error", "Error updating , please try again");
			}														
		});
	} , 
	
	display			: function( val )
	{
		$("#action_status_table")
		.append($("<tr />",{id:"tr_"+val.id})
			.append($("<td />",{html:val.id}))		  
			.append($("<td />",{html:val.name}))		  
			.append($("<td />",{html:val.client_terminology}))		  
			.append($("<td />")
			  .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))		
			)		  
			.append($("<td />")
			 .append($("<input />",{type:"button",name:"edit_"+val.id, value:"Edit", id:"edit_"+val.id }))
			 .append($("<input />",{type:"button",name:"del_"+val.id, value:"Del", id:((val.status & 4) == 4 ? "del__"+val.id : "del_"+val.id) }))
			)		  
			.append($("<td />",{html:((val.status & 1) == 1 ? "<b>Active</b>"  : "<b>InActive</b>" )}))	
			.append($("<td />")
			  .append($("<input />",{type:"button", name:"change_"+val.id, id:((val.status & 4) == 4 ? "change__"+val.id: "change_"+val.id), value:"Change Status"}))
			)
		)
		
		if((val.status & 4) == 4)
		{
		   $("#del__"+val.id).attr("disabled", "disabled");
		   $("#change__"+val.id).attr("disabled", "disabled");
		}
				
		$("#change_"+val.id).live("click", function(){
			document.location.href = "change_action_status.php?id="+val.id;		
			return false
		});		
	
		$("#edit_"+val.id).live( "click", function(){
			//showEditForm( val.id );
			document.location.href = "edit_action_status.php?id="+val.id;
			return false;						  
		});
		
		$("#del_"+val.id).live( "click", function(){
			if( confirm(" Are you sure you want to delete this action status ")) {
				jsDisplayResult("info", "info", "Deleting  ...  <img src='../images/loaderA32.gif' />");					
				$.post( "controller.php?action=changeActionstatus", { id:val.id, status:2 }, function( delData  ) {
					if( delData == 1 ) {
						$("#tr_"+val.id).fadeOut();
						jsDisplayResult("ok", "ok", "Action Status deleted");	
					} else if( delData == 0 ) {
						jsDisplayResult("info", "info", "No changes were made to the status");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again");
					}						
				});  			
			} 
			return false;									  
		});

	}
		
};
