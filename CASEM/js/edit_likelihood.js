// JavaScript Document
$(function(){
		   
		$("#edit_likelihood").click(function(){  
	
		var message 	= $("#likelihood_message")						
		var rating_from = $("#lkrating_from").val();
		var rating_to 	= $("#lkrating_to").val();
		var assessment	= $("#lkassessment").val();
		var definition  = $("#lkdefinition").val();
		var probability = $("#lkprobability").val();		
		var color 		= $("#llkcolor").val();
	
		if ( rating_from == "") {
			message.html("Please enter the rating feom for this likelihood")
			return false;
		} else if( rating_to == "" ) {
			message.html("Please enter the rating to for this likelihood");
			return false;
		} else if ( assessment == "" ) {
			message.html("Please enter the asssessment for this likelihood");
			return false;
		} else if( definition == "" ) {
			message.html("Please enter the definition for this likelihood");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=updateLikelihood",
				   {
					   id		: $("#likelihood_id").val(),
					   from		: rating_from,
					   to		: rating_to,
					   assmnt	: assessment,
					   dfn		: definition,
					   prob		: probability,
					   clr		: color
					}
				   , function( retData ) {
						 if( retData == 1 ) {
							message.html("Likelihood successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Likelihood not changed");
						} else {
							message.html("Error updating the likelihood");
						}
			},"json")	
		}
		return false;								  
	});
	
	$("#change_likelihood").click(function(){
		var message 	= $("#likelihood_message")											  
		$.post( "controller.php?action=changeLikestat", 
			   { id		: $("#likelihood_id").val(),
			   	 status : $("#like_status :selected").val()
			   }, function( retData ) {
			if( retData  == 1){
				message.html("Likelihood status changed").animate({opacity:"0.0"},4000);
			} else if( retData  == 0){
				message.html("No change was made to the likelihood status ").animate({opacity:"0.0"},4000);
			} else {
				message.html("Error updating status  , please try again ").animate({opacity:"0.0"},4000);
			}										 
		})									
		return false;										  								   
	});	
		
		
	$("#cancel_edit_likelihood").click(function(){
		history.back();
		return false;								   
	});
	
	   
})
