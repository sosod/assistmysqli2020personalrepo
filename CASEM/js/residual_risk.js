// JavaScript Document
$(function() {
		
	/**
		get all the residual risks
	**/
	$("#residual_risk_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getResidualRisk", function( data ) {
		$("#residual_risk_message").html("")
		var message = $("#residual_risk_message")
		$.each( data , function( index, val) {
			$("#residual_risk_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.magnitude}))
			  .append($("<td />",{html:val.response}))
			  .append($("<td />",{css:{"background-color":"#"+val.color},html:val.color }))
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"residualedit_"+val.id, name:"residualedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"residualdel_"+val.id, name:"residualdel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "InActive")+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"residualact_"+val.id, name:"residualact_"+val.id}))	
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ),value:"Inactivate", id:"residualdeact_"+val.id, name:"residualdeact_"+val.id}))							
			   )
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		
			   )
			)
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_residual_status.php?id="+val.id;
				return false;										  
			});			
			$("#residualedit_"+val.id).live( "click", function() {
				document.location.href = "edit_residual.php?id="+val.id;
				return false;										  
			});
			$("#residualdel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this residual risk ")) {														  					$.post( "controller.php?action=changeResidualStatus", 
																																									   						{ id	: val.id , status : 0}, function( delData ) {
						if( delData == 1){
							message.html("Residual risk deactivated").animate({opacity:"0.0"},4000);
						} else if( delData == 0){
							message.html("No change was made to the residual risk").animate({opacity:"0.0"},4000);
						} else {
							message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
						}										 
					});
				 }
				return false;										  
			});
			$("#residualact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=residualactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						message.html("Residual risk activated").animate({opacity:"0.0"},4000);
					} else if( actData == 0){
						message.html("No change was made to the residual risk").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}										 
				})															 
				return false;										  
			});
		});
	}, "json")
	
	/**
		Adding a new residual risk
	**/
	$("#add_residual").click( function() {
		var message 		= $("#residual_risk_message")						
		var residual_from 	= $("#residual_risk_from").val();
		var residual_to 	= $("#residual_risk_to").val();
		var residual		= $("#residual_magnitude").val();
		var response  		= $("#residual_response").val();
		var color 			= $("#residual_color").val();
	
		if ( residual_from == "") {
			message.html("Please enter the rating from for this residual risk")
			return false;
		} else if( residual_to == "" ) {
			message.html("Please enter the rating to for this residual risk");
			return false;
		} else if ( residual == "" ) {
			message.html("Please enter the magnitude for this residual risk");
			return false;
		} else if( response == "" ) {
			message.html("Please enter the response for this residual risk");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=newResidualRisk", 
				   {
					   from		: residual_from,
					   to		: residual_to,
					   magn		: residual,
					   resp		: response,
					   clr		: color
					}
				   , function( retData ) {
				 if( retData > 0 ) {
					 $("#residual_risk_from").val("")
					 $("#residual_risk_to").val("")
					 $("#residual_magnitude").val("")
					 $("#residual_response").val("")
					 message.html("Residual risk saved successifully ....");
					$("#residual_risk_table")
					.append($("<tr />")
					  .append($("<td />",{html:retData}))
					  .append($("<td />",{html:residual_from+" to "+residual_to}))
					  .append($("<td />",{html:residual}))
					  .append($("<td />",{html:response}))
					  .append($("<td />",{css:{"background-color":"#"+color}, html:color}))
					  .append($("<td />")
						.append($("<input />",{type:"submit", value:"Edit", id:"residualedit_"+retData, name:"residualedit_"+retData}))	
						.append($("<input />",{type:"submit", value:"Del", id:"residualdel_"+retData, name:"residualdel_"+retData}))					
					   )
					  .append($("<td />",{html:"<b>Active</b>"})
						//.append($("<input />",{type:"submit", value:"Activate", id:"residualact", name:"residualact"}))	
						//.append($("<input />",{types:"submit",value:"Inactivate", id:"residualdeact", name:"residualdeact"}))							
					   )
					  .append($("<td />")
						.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))		
					   )
					)
					$("#change_"+retData).live( "click", function() {
						document.location.href = "change_residual_status.php?id="+retData;
						return false;										  
					});			
					$("#residualedit_"+retData).live( "click", function() {
						document.location.href = "edit_residual.php?id="+retData;
						return false;										  
					});					
				 } else {
					message.html("There was an error saving , please try again")	 
				 }
			},"json")	
		}
		return false;							 
	});	   
})