$(function(){
	$("#report-header-form").find("input[type='checkbox']").attr("checked","checked");

	$("#sortable").sortable({placeholder:"ui-state-highlight"});
	$("#sortable").disableSelection();	
	
	$(".datepicker").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		:true,
		changeYear		:true,
		dateFormat 		: "dd-M-yy"
	});	

	$.getJSON("controller.php?action=getSavedReport",{ id : $("#resportId").val() }, function( responseData ){
		//check those that are saved 
		$(".queryf").each(function( index ){
			var id = $(this).attr("id");
			
			if( responseData.header[id] == 1){
			   $(this).attr("checked", "checked");
			} else if(responseData.header[id] == undefined){		
			   $(this).attr("checked", "");
			}
		});
		
			
		$.each( responseData.match, function( index , key ){
			$("#match_"+index+" option[value='"+key+"']").attr("selected", "selected")
		});
			
		$.each( responseData.values, function( index , key ){
			
			$("#_"+index).val( key );
			
			$("#_"+index+" option[value='"+key+"']").attr("selected", "selected")
		});

		$("#group_by option[value='"+responseData.group_by+"']").attr("selected", "selected")
		
		
		var sortNames = {
						 __id						: "Query Item",
						 __type						: "Query Type",
						 __category					: "Query Category",
						 __description				: "Query Description",
						 __background 				: "Query Background",
						 __financial_exposure		: "Financial Exposure",
						 __monetary_implication 	: "Monetary Implication",
						 __risk_level				: "Risk Level",
						 __risk_type				: "Risk Type",
						 __risk_detail				: "Risk Detail",
						 __finding					: "Finding",
						 __recommendation			: "Recommendation",
						 __client_response			: "Client Response",
						 __auditor_conclusion		: "Auditor Conclusion",
						 __internal_control_deficiency			: "Internal Control Deficiency"
						}
		$.each( responseData.sort, function( index , val ){

			$("#sortable").append($("<li />").addClass("ui-state-default")
				.append($("<span />",{
										id 		: val  
									}).addClass("ui-icon").addClass("ui-icon-arrowthick-2-n-s").addClass("sort")	
				)
			   .append($("<input />",{
				   					type	: "hidden",
				   					name	: "sort[]",
				   					value	: val
			   }))
			   .append( sortNames[val] )
			)
		});		
		
		$("#report_description").val( responseData.report_description )
		$("#report_title").val( responseData.report_title )
		$("#report_name").val( responseData.report_name )
	
	});
	
    $(".quick_report").click(function(){
        document.location.href = "process_report.php?generate_quick_report=1&id="+$(this).attr("id");
        return false;
    });

	$("#r_checkAll").click(function(){ 
		$("#report-header-form").find("input[type='checkbox']").not(".inaction").attr("checked","checked");
        $(".queryf").attr("checked", "checked");
		return false;
	});
	
	$("#r_uncheckAll").click(function(){ 
		$("#report-header-form").find("input[type='checkbox']").not(".inaction").attr("checked","");
        $(".queryf").attr("checked", "");
		return false;
	});

    $("#r_invert").click(function(){
       $(".queryf").each(function(){
            if( $(this).is(":checked")){
	            $(this).attr("checked","");
            } else {
                $(this).attr("checked","checked");
            }
        });
		return false;
	});

	$("#a_checkAll").click(function(){ 
		$(".actionf").attr("checked","checked");
		return false;
	});
	
	$("#a_uncheckAll").click(function(){ 
		$(".actionf").attr("checked","");
		return false;
	});

	$("#a_invert").click(function(){
        $(".actionf").each(function(){

            if( $(this).is(":checked")){
	            $(this).attr("checked","");
            } else {
                $(this).attr("checked","checked");
            }

        });
		return false;
	});
	//==============================================================================================================
		/**
		get all the risk types
	**/
	$.get( "controller.php?action=getQueryStatuses", function( data ) {
		$.each( data ,function( index, val ) {
			//$("#_status")
			//.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");
	//==============================================================================================================
		/**
		get all the risk types
	**/
	$.get( "controller.php?action=getActionStatuses", function( data ) {
		$.each( data ,function( index, val ) {
			//$("#actionstatus")
			//.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/									
		$.get( "controller.php?action=getRiskLevel", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				//$("#_risk_level")
				//.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
	//================================================================================================================
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			//$("#_financial_exposure")
			//.append($("<option />",{text:val.name, value:val.id,  selected:((val.status & 4) == 4 ?  "selected" : "")}));
		})			
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getQueryType", function( data ) {
		$.each( data ,function( index, val ) {
			//$("#_type")
			//.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			//$("#_risk_type")
			//.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#_type").live("change", function() {
		$("#_category").html("");									
		$.post("controller.php?action=getCategory", {id:$(this).val()}, function( data ) {
			$.each( data ,function( index, val ) {
				$("#_category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});
	
	$.get( "controller.php?action=getCategories", function( data ) {
		$.each( data ,function( index, val ) {
			//$("#_category")
			//.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//================================================================================================================	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_query_user_responsible")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
			
			$("#_action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));

			$("#aaction_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})											 
	},"json");	
	//================================================================================================================	
	$.get("controller.php?action=getDirectorate", function( direData ){
	$.each( direData ,function( index, directorate ) {
			//$("#_risk_owner")
			//.append($("<option />",{text:directorate.dirtxt	, value:directorate.subid}));
		})
	}, "json");
	
	
	
	
});
