// JavaScript Document
$(function(){
$.get("controller.php?action=getNotifications", function( notiData ){
	$.each( notiData, function( index, notification ) {
		what_to_recive = getWhat( notification.recieve_what );
		var day 	   = getRecDay( notification.recieve_day );
		$("#profile")
		.append($("<tr />",{ id:"tr_"+notification.id})
			.append($("<td />",{html:notification.id}))
			.append($("<td />",{html:what_to_recive}))
			.append($("<td />",{html:(day == "" ? notification.recieve_when : "("+notification.recieve_when+")"+" "+day) }))
			.append($("<td />",{html:notification.inserdate}))
			.append($("<td />")
			  .append($("<input />",{type:"button", id:"edit_"+notification.id, name:"edit_"+notification.id, value:"Edit"}))
			  .append($("<input />",{type:"button", id:"del_"+notification.id, name:"del_"+notification.id, value:"Delete"}))				
			)				
		)
		
		$("#edit_"+notification.id).live("click", function(){
			document.location.href = "edit_profile.php?id="+notification.id;
			return false;
		});
		
		$("#del_"+notification.id).live("click", function(){
			if(confirm("Are you sure you want to delete this notification")){				
				$.post("controller.php?action=deleteNot", { id : notification.id }, function( delData ){
					$("#tr_"+notification.id).fadeOut();									   
				})
			}
			return false;
		});			
	});		
	
	$("#profile")
		.append($("<tr />")
			.append($("<td />",{colspan:"6", align:"left"})
				.append($("<input />",{type:"submit", name:"new_notification", id:"new_notification" , value:"New"}))
				.append($("<input />",{type:"submit", name:"cancel_notification", id:"cancel_notification" , value:"Cancel"}))					
			)		  
		)
},"json");
	
	$.get("controller.php?action=getRisk", function( riskData ){							
		$.each( riskData , function( index, risk){
			$("#recieve_risk")
				.append($("<option />",{text:risk.risk_descr, value:risk.id}))
		});
	},"json");
	
	$("#recieve_risk").live( "change", function(){						
		var risk = $("#recieve_risk :selected").val();
		if( risk !== ""){
			$.post( "../actions/controller.php?action=getActions",{ id:risk }, function( actionData ){
				if( $.isEmptyObject(actionData) ) {
					$("#recieve_risk_action").fadeOut();
				} else {
					$.each( actionData , function( index, action){
						$("#recieve_risk_action")
						.append($("<option />",{text:action.action, value:action.id}))
						.fadeIn()
					});
				}
			},"json")	
		}
	});
	
	$("#new_notification").live("click",function(){
		$("#notification").fadeIn();
		return false;								  
	});
	
	$("#cancel_notification").live("click",function(){
		$("#notification").fadeOut();
		return false;								  
	});
	
	$("#recieve_when").live( "change", function(){
		if( $("#recieve_when :selected").val() == "daily" ) {
			$("#recieve_day").fadeIn();	
		} else {
			$("#recieve_day").fadeOut();	
		}
		return false;						  
	});

	$("#update").click( function(){
		var message 	 = $("#profile_message")
		var recieve 	 = $("#recieve :selected").val();
		var recieve_when = $("#recieve_when :selected").val();
		var recieve_day  = $("#recieve_day :selected").val();
		var recieve_what = $("#recieve_what :selected").val();
		
		if( recieve == "" ){
			message.html("You have not selected to recieve email notificatons")	;
			return false;		
		} else {	
			if( recieve_when == "" ) {
				message.html(" Please select when to recieve the email notification ");
				return false;
			} else {
				// if daily
				if( recieve_when == "daily" ) {
					if( recieve_day == "" ) {
						message.html("Please select the day to recieve the email notification ");
						return false;			
					} else {
						if( recieve_what == "") {
							message.html("Please select notification to recieve ");
							return false;										
						} else {
							saveNotification( recieve, recieve_when, recieve_day, recieve_what);
						}	
					}
				} else {
						//weekly
							saveNotification( recieve, recieve_when, recieve_day, recieve_what);
				}
			}
		}
		
		return false;
	});
	
	var getWhat = function( recieveWhat ){
		switch( recieveWhat ){
			case "due_this_week":
				return "Actions due this week";
				break;
			case "due_on_or_before_week":
				return "Actions due on or before this week";
				break;
			case "due_today":
				return "Actions due today";
				break;
			case "due_on_or_before_today":
				return "Actions due on or before today";
				break;
			case "all_incomplete_actions":
				return "All Incomplete actions";
				break;			
			default:
				return "";
				break;
		}	
	}
	var getRecDay = function( day ){
		switch( day ){
			case "sun":
				return "Sunday";
				break;
			case "mon":
				return "Monday";
				break;
			case "tues":
				return "Tuesday";
				break;
			case "wed":
				return "Wednesday";
				break;
			case "thurs":
				return "Thursday";
				break;
			case "fri":
				return "Friday";
				break;
			case "sat":
				return "Saturday";
				break;						
			default:
				return "";
				break;
		}	
	}	
});

function saveNotification( recieve, recieve_when, recieve_day, recieve_what){
	var message = $("#profile_message") 
	$.post("controller.php?action=saveNotification",
		{ 
			risk_id		 : $("#recieve_risk :selected").val(),
			action_id	 : $("#recieve_risk_action :selected").val(),
			notification : "",
			user_id		 : "1",
			recieve 	 : recieve ,
			recieve_when : recieve_when,
			recieve_day	 : recieve_day,
			recieve_what : recieve_what
 		},
		function( retData ) 
		{
			if( retData > 0 ) {
				message.html( "Notification saved successifully" )
			} else {
				message.html( "Error saving notification" )	
			}
		}, 
	"json")
	}
