// JavaScript Document
$(function(){
	$("#update").click(function(){
		var message 	 = $("#profile_message")
		var recieve 	 = $("#recieve :selected").val();
		var recieve_when = $("#recieve_when :selected").val();
		var recieve_day  = $("#recieve_day :selected").val();
		var recieve_what = $("#recieve_what :selected").val();
		
		$.post("controller.php?action=updateNotification",
			{ 
				risk_id		 : $("#recieve_risk :selected").val(),
				action_id	 : $("#recieve_risk_action :selected").val(),
				notification : "",
				user_id		 : "1",
				recieve 	 : recieve ,
				recieve_when : recieve_when,
				recieve_day	 : recieve_day,
				recieve_what : recieve_what,
				id			 : $("#notid").val()
			},
			function( retData ) 
			{
				if( retData > 0 ) {
					message.html( "Notification saved successifully" )
				} else {
					message.html( "Error saving notification" )	
				}
			}, 
		"json")		
		
		return false;
	});		   
		
	$("#recieve_when").live( "change", function(){
		if( $("#recieve_when :selected").val() == "daily" ) {
			$("#recieve_day").fadeIn();	
		} else {
			$("#recieve_day").fadeOut();	
		}
		return false;						  
	});
	
});