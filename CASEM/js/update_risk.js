// JavaScript Document
//View Risk
$(function(){
	$("em").css({"color":"#FF0000", "font-size":"0.8em"})	
	
	var total = 0
	var current = 1
	var start 	= 0;
	var limit  	= 10;
	var displaying = "";
	// initially display risk that your are the risk owner
	getUpdateRiskData( current, start, limit, "viewRiskActions" );
	$('body').data("displaying", "viewRiskActions");
	// display all the risks and sets the display to diaplay all the risks 
	$("#btn_displayAll").click(function(){
		$(this).parent().hide();
		$("#displayOwn").show();
		getUpdateRiskData( current, start, limit, "viewUpdateRisk" );								
		$('body').data("displaying", "viewUpdateRisk");		
		return false;
	});
	// diaplay risks for the user logged in , and set to show for the user logged in
	$("#btn_displayOwn").click(function(){
		$(this).parent().hide();
		$("#displayAll").show();							
		getUpdateRiskData( current, start, limit, "viewRiskActions");
		$('body').data("displaying", "viewRiskActions");	
		return false;
	});	
	
	$("#b1").live("click", function(){
		current = 1;
		start = 0;
		limit = 10;
		displaying = $('body').data("displaying");
		getUpdateRiskData( current, start, limit, displaying);
		return false;
	});
	
	$("#b2").live("click", function(){
		limit = 10;
		total = parseFloat( $('body').data('total') );
		var pages = Math.ceil( total/10 );
		current = parseFloat( current ) - 1;

		start 	= (current-1)*limit;
		displaying = $('body').data("displaying");
		getUpdateRiskData( current, start, limit, displaying);
		return false;
	});
	
	$("#b3").live("click", function(){
		limit = 10;
		current = current+1;
		start = parseFloat( current ) * parseFloat( limit );
		displaying = $('body').data("displaying");
		getUpdateRiskData( current, start, limit, displaying);
		return false;
	});

	$("#b4").live("click", function(){
		current =  Math.floor( parseFloat( $('body').data('total') )/ parseFloat( limit ));
		start = parseFloat(current-1) * parseFloat(limit);
		displaying = $('body').data("displaying");
		getUpdateRiskData( current, start, limit, displaying);	 
		return false;
	});
	
});

function getUpdateRiskData( current, start, limit, updatePage ) {
	$("#risk_table").html("Loading risks  .... <img src='../images/loaderA32.gif' />");
	$.get("controller.php?action=getRisk",
	  { 
	  	page		: updatePage,
	  	start		: start,
		limit 		: limit
	  } ,
	  function(  data ) {
		  if( $.isEmptyObject( data ) ) {
			message.html("No Risks were found");			  
		  } else {
			$("#risk_table").html("");
			createPager( data.total , current);
			populateHeaders( data.headers );
			populateData( data.riskData, data.useraccess , data.actionOnly );			  
		  }
	}, "json");
}

function populateHeaders( headerData ) 
{
	var th = $("<tr />");
	$.each( headerData , function( index, headerObject){
		th.append($("<th />",{html:headerObject.client_terminology}));							   
	});	
	$(th).append($("<th />",{html:''}))
	$("#risk_table").append( th );
}

function populateData( riskData, userAccess , actionOnly)
{
	$.each( riskData, function( index , risk){
		var showUpdateRisk = true;
		if( ! $.isEmptyObject( actionOnly ) )  {
			if( $.inArray(index, actionOnly) >= 0 ){
				showUpdateRisk = false;
			}
		}
		var tr = $("<tr />")
		$.each( risk, function( i, rsk){
		 	tr.append($("<td />",{html:rsk}))						  		   			   
		});
		$(tr)
		.append($("<td />")
		.append($("<input />",{
		  type	: (((userAccess.module_admin == 0 && userAccess.create_risks == 0 ) || showUpdateRisk == false ) ? "hidden" : "submit"),
		  name	: "update_"+index,
		  value	: "Update Risk",
		  id	: "update_"+index
		  }))		
		.append($("<br />"))
		.append($("<input />",{type:"submit", name:"action_update_"+index, value:"Update Action", id:"action_update_"+index}))		  			
		)		
		$("#risk_table").append(tr);

		$("#update_"+index).live( "click", function(){
			document.location.href = "updaterisk.php?id="+index;								  
			return false;
		});
		
		$("#action_update_"+index).live( "click", function(){
			document.location.href = "../actions/action_update.php?id="+index;								  
			return false;
		});		
	});
}

function namingHeader( idHeader, defaultName) 
{	
	if( idHeader == "" || idHeader == null  ) 
	{
		return defaultName;	
	} else {
		return idHeader;	
	}
}

function createPager( total , current ) 
{
	var pager =  Math.floor( parseFloat( total )/ parseFloat( 10 )	 );
	$("#risk_table").html("");	
	$("#risk_table")
	.append($("<tr />")
	  .append($("<td />",{colspan:"20"})
		.append($("<table />",{id:"paging_risk", cellspacing:"5", width:"100%"}))		  
	   )		  
	)
	$("#paging_risk").html("");
	$("#paging_risk")
	.append($("<tr />")
		.append($("<td />")
		.append($("<input />",{type:"submit", name:"b1", value:"|<", id:"b1", disabled:(current < 2 ? 'disabled' : '' )}))
		.append($("<input />",{type:"submit", name:"b2", value:"<", id:"b2", disabled:(current < 2 ? 'disabled' : '' )}))
	  	.append($("<span />",{html:" Page "+current+"/"+( isNaN(pager) ?  0 : (pager == 0 ? "1" : pager) )+" "}))
		.append($("<input />",{type:"submit", name:"b3", value:">", id:"b3", disabled:((current==pager || pager == 0) ? 'disabled' : '' )}))
		.append($("<input />",{type:"submit", name:"b4", value:">|", id:"b4", disabled:((current==pager || pager == 0) ? 'disabled' : '' )}))					
		)
		.append($("<td />",{width:"50%"})
					  
		)
	)		
}

