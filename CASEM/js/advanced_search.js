// JavaScript Document
$(function(){  
	//set the table th text to align to the left
	$("table#risk_new").find("th").css({"text-align":"left"})

	$(".datepicker").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy"
	});		
   //=======================================================================================================================

        $.get( "controller.php/?action=getRiskLevel", { id : $(this).val() }, function( data ) {
            $.each( data ,function( index, val ) {
                $("#risk_level")
                .append($("<option />",{text:val.name, value:val.id, selected:((val.status & 4) == 4 ?  "selected" : "")}));
            })
        },"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#type").live("change", function() {
		$("#category").html("");
		$("#category").append($("<option />",{value:"", text:"--category--"}))
		$.get( "controller.php/?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});
	//================================================================================================================
	/**	get all the risk impacts
	**/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			$("#financial_exposure")
			.append($("<option />",{text:val.name, value:val.id,  selected:((val.status & 4) == 4 ?  "selected" : "")}));
		})			
	},"json");
  //======================================================================================================================
	/**
		get all the query types
	**/
	$.get( "controller.php/?action=getQueryType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#type")
			.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");

	//================================================================================================================
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#query_user_responsible")
			.append($("<option />",{text:val.tkname, value:val.tkid}));

			$("#action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})
	},"json");	

	//================================================================================================================	
	$.get("controller.php?action=getDirectorate", function( direData ){
	$.each( direData ,function( index, directorate ) {
			$("#sub_id")
			.append($("<option />",{text:directorate.dirtxt	, value:directorate.subid}));
		})
	}, "json");


	$("#search").click(function(){
	if( $("#resultsBox").length > 0){
		$("#resultsBox").show();
	} 
	$("#searchBox").fadeOut();
	$("#span_again").fadeIn();	
	jsDisplayResult("info", "info", "Searching  ...  <img src='../images/loaderA32.gif' />");
	$.getJSON("controller.php?action=advancedSearch", {
			formdata : $("#advanced-search-form").serializeArray()  
		  } , function( searchResults ){
			  if( $.isEmptyObject( searchResults.riskData ) ){
				  $("#searchresults").html("");				  
				  jsDisplayResult("info", "info", "No Query data was found");
			  } else {
				 jsDisplayResult("ok", "ok", searchResults.total+" results found");
				$("#searchresults")
					.css({"clear":"both", "margin-left":"0px"})
					.html("")
					.append($("<table />", {id:"risk_table", align:"left"}))
				populateHeaders( searchResults.headers );
				populateData( searchResults.riskData );			  
			  }	
	});
	return false;
	});
	
	$("#search_again").click(function(){
		if( $("#searchBox").length > 0){
			$("#searchBox").show();
		}
		$("#resultsBox").hide();
		//$("#span_again").fadeOut();			
		//$("#searchresults").html("");
		//$("#display_objresult").hide();
		$("#heading").html("Advanced Search")		
		return false;					  
	});
	
});


function populateHeaders( headerData ) 
{		
		var th = $("<tr />");
		$.each( headerData , function( index, headerObject){
			th.append($("<th />",{html:headerObject}));							   
		});	
		$("#risk_table").append( th );
}


function populateData( riskData )
{
	$.each( riskData, function( index , risk){
		var tr = $("<tr />")
		$.each( risk, function( i, rsk){

				tr.append($("<td />",{html:rsk}))					  		   			   
		});
		$("#risk_table").append(tr);					   
	});
}