// JavaScript Document
$(function(){
	$(".datepicker").live( "focus",function(){
		$(this).datepicker({ changeMonth:true, changeYear:true, dateFormat:"yy-m-d" })								 
	});
	//

	
	$("#show_action_table").html("");
	$.post("controller.php?action=getAction", { id:$("#action_id").val() }, function( actionData ){	
		//$("#show_action_table")	
		
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Action #:"}).css({"text-align":"left"}))
			.append($("<td />",{html:actionData.id}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Action:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:actionData.action}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Deliverable:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:actionData.deliverable}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Action Owner:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:actionData.tkname}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Time Scale:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:actionData.timescale}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Deadline:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:actionData.deadline}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Status:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:((actionData.status == "" || actionData.status == null ) ? "New" : actionData.status) }))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Progress:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:(actionData.progress == "" || actionData.progress == null ? "0" : actionData.progress )+"%"}))		  
		 )
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Remind On:"}).css({"text-align":"left"}))							  
			.append($("<td />",{html:actionData.remindon}))		  
		 )

		$("body").append($("<input />",{type:"hidden", value:actionData.id, name:"action_id", id:"action_id"}))		  

	},"json");		   
	
	$("table#show_action_table").find("th").css({"text-align":"left"})
	
		$("#save_action_update").click(function() {
		var message 	= $("#update_message").slideDown("fast");
		var description = $("#description").val();
		var status		= $("#status :selected").val();
		var progress	= ((status =='3' || $("#status :selected").text()== "Addressed") ? "100" : $("#progress").val());
		var remindon	= $("#remindon").val();
		var attachment	= $("#update_attachment").val();
		var approve 	= ($("#approval").is(":checked") ? "on" : "" );

		if( description == "") {
			jsDisplayResult("error", "error", "Please enter the description for the update");
			return false;
		} else if( status == "" ) {
			jsDisplayResult("error", "error", "Please select the status for the update");
			return false;		
		} else if( progress == "" ) {
			jsDisplayResult("error", "error", "Please enter the progress for the update");
			return false;
		} else if( isNaN( progress ) ) {
			jsDisplayResult("error", "error", "Progress must be a number!!!");	
			return false;							
		} else if(progress == "100" && status !== "3" ){
			jsDisplayResult("error", "error", "Progress cannot be 100% , if the status in not addressed");
			return false;	
		} else {
			if( $("#approval").is(":checked") ) {
				var message = $(".message").slideDown();
				jsDisplayResult("info", "info", "Sending request approval email ...  <img src='../images/loaderA32.gif' />");
				$.post("controller.php?action=sendRequestApprovalEmail", { id:$("#action_id").val()}, function( response ){
					if( response.error ) {
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text );					
					}
				},"json");				
			}			
			jsDisplayResult("info", "info", "Saving action update ...  <img src='../images/loaderA32.gif' />");			
			$.post( "controller.php?action=newActionUpdate", 
				   {
				   	id 			: $("#action_id").val(),
				   	description	: description ,
					status		: status ,
					progress	: progress ,
					remindon 	: remindon,
					attachment	: attachment,
					approval	: approve
				  }, function( response ){
					if( response.error ) {
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text );					
					}					  
				}, "json")
		}	
		
		return false;
	});
	/**
		Add another browse button to attach files
	**/
	$("#attach_another").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	
	$("#cancel_action_update").click(function(){
		history.back();
		return false;
	})
	
	$("#view_action_update_log").click( function() {								
		var container = $("#view_action_updates");
		container.fadeIn("slow").html("Loading updates .... <img src='../images/loaderA32.gif'>")									 
		$.post("controller.php?action=getActionUpdates", { id : $("#action_id").val() }, function( actionUpdates ) {
			if($.isEmptyObject( actionUpdates ) ) {
				container.html("No updates have been logged");
			} else {
				container.html("")
				container.append($("<table />",{id:"updates", width:"67%"}))
				$("#updates")
				 .append($("<tr />")
					.append($("<th />",{html:"Date"}))
					.append($("<th />",{html:"Audit Log"}))
					.append($("<th />",{html:"Status"}))
				 )
				$.each( actionUpdates, function( index, update){
					$("#updates")
					 .append($("<tr />")
						.append($("<td />",{html:update.date}))
						.append($("<td />",{html:update.changeMessage}))
						.append($("<td />",{html:update.status}))					
					 )
				})					
			}																					  									  																				
		},"json");																					
		return false;
	});
})
