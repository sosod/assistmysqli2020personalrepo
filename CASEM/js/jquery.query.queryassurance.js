$.widget("ui.queryassurance",{

    options     : {
		tableId		: "queryassurance_"+(Math.floor(Math.random(78)*89)),
		risk_id	    : 0,
		start		: 0,
		limit		: 10,
		current		: 1,
		total		: 1,
    } ,
    
    _init       : function()
    {
       this._getQueryAssurances();    
    },
    
    _create     : function()
    {
        var self = this;
        var html = [];
        html.push("<table width='100%'>");
          html.push("<tr>");
            html.push("<td><h4>Query Assurances</h4></td>");
          html.push("</tr>");
          html.push("<tr>");
            html.push("<td class='noborder'>");
            html.push("<table class='noborder' id='"+self.options.tableId+"' width='100%'></table>");
            html.push("</td>");
          html.push("</tr>");
        html.push("</table>");
        
        $(self.element).append(html.join(' '));
    } ,
    
    _getQueryAssurances  : function()
    {
		var self = this;
	     $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );			
		$.getJSON("controller.php?action=getQueryAssurances",{
		      query_id  : self.options.risk_id,
		      start     : self.options.start,
		      limit     : self.options.limit				  
		  }, function(queryAssurances){
		    $("#actionLoadingDiv").remove();
		    $("#"+self.options.tableId).html("");
		    self._displayPager(queryAssurances.total);
		    self._headers();
		    if(!$.isEmptyObject(queryAssurances))
		    {
			    //self.options.total = queryAssurances.total;
			    self._display(queryAssurances.assurances);														 
		    } 	
		    var html = [];
		    html.push("<tr>");
		      html.push("<td colspan='8'><input type='button' name='add' id='add' value='Add New' /></td>");
		    html.push("</tr>");
		    $("#"+self.options.tableId).append(html.join(' '));
		    $("#add").on("click", function(e){
		        self._addAssurance();
		        e.preventDefault();		    
		    });												
		});
    }, 
    
    _display            : function(queryAssurances)
    {
        var self = this;
        var html = []; 
        $.each(queryAssurances, function(index, assurance){
            html.push("<tr id='assurance_"+assurance.id+"'>");
              html.push("<td>"+assurance.id+"</td>");
              html.push("<td>"+assurance.datetime+"</td>");
              html.push("<td>"+assurance.date_tested+"</td>");
              html.push("<td>"+assurance.response+"</td>");
              html.push("<td>"+assurance.actionstatus+"</td>");
              html.push("<td>"+assurance.user+"</td>");
              html.push("<td>"+assurance.attachment+"</td>");
              html.push("<td>");
                html.push("<input type='button' name='edit_"+assurance.id+"' id='edit_"+assurance.id+"' value='Edit' />");
                html.push("<input type='button' name='del_"+assurance.id+"' id='del_"+assurance.id+"' value='Delete' />")
              html.push("</td>");
            html.push("</tr>");
            $("#edit_"+assurance.id).live("click", function(e){
                self._editAssurance(assurance);
               e.preventDefault();
            });
            
            $("#del_"+assurance.id).live("click", function(e){
                if(confirm("Are you sure you want to delete this query assurance"))
                {
                   self._deleteAssurance(assurance);
                }
               e.preventDefault();
            });            
        });
        $("#"+self.options.tableId).append(html.join(' '));
    } ,
    
    _headers            : function()
    {
        var self = this;
        var html = [];
        html.push("<tr>");
          html.push("<th>Ref</th>");
          html.push("<th>System Date and Time</th>");
          html.push("<th>Date Tested</th>");
          html.push("<th>Response</th>");
          html.push("<th>Status</th>");
          html.push("<th>Assurance Provider</th>");
          html.push("<th>Attachment</th>");
          html.push("<th></th>");
        html.push("<tr>");
        $("#"+self.options.tableId).append(html.join(' '));
    } ,
    
    _displayPager       : function()
    {
    
    } ,
    
    _addAssurance      : function()
    {
        var self = this;
        var html = [];
        html.push("<table width='100%'>");
          html.push("<tr>");
           html.push("<th>Response:</th>");
           html.push("<td>");
             html.push("<textarea cols='50' rows='7' name='response' id='response'></textarea>");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Action On:</th>");
           html.push("<td>");
             html.push("<input type='text' name='date_tested' id='date_tested' value='' readonly='readonly' class='datepicker' />");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Status:</th>");
           html.push("<td>");
             html.push("<select name='signoff' id='signoff'>");
             html.push("</select>");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Attachment:</th>");
           html.push("<td>");
             html.push("<input type='file' name='qassurance_attachment_"+self.options.risk_id+"' id='qassurance_attachment_"+self.options.risk_id+"' class='action_upload' />");
             html.push("<p class='ui-state ui-state-info'><small>You can attach more than 1 file.</small></p>");
             html.push("<p id='file_upload'></span>");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<td colspan='2'><p style='padding:5px' id='msg'></p></td>");
          html.push("</tr>");
        html.push("</table>");
        if($("#queryassurance_dialog").length > 0)
        {
           $("#queryassurance_dialog").remove();
        }
        
        $("<div />",{id:"queryassurance_dialog"}).append(html.join(' '))
        .dialog({
                    autoOpen    : true,
                    modal       : true,
                    position    : "top",
                    title       : "Add Query Assuarance",
                    width       : 'auto',
                    buttons     : {
                                    "Save"  : function()
                                    {
                                        $("#msg").addClass("ui-state-info").html("saving ...");
                                        if($("#response").val() == "")
                                        {
                                           $("#msg").addClass("ui-state-error").html("Please enter the response");
                                        } else if($("#signoff").val() == "") {
                                            $("#msg").addClass("ui-state-error").html("Please select the status");
                                        } else {
                                            $.post("controller.php?action=saveQueryAssurance",{
                                                response    : $("#response").val(),
                                                date_tested : $("#date_tested").val(),
                                                signoff     : $("#signoff :selected").val(),
                                                query_id    : self.options.risk_id
                                            },function(response) {
                                               if(response.error)
                                               {
							 					  jsDisplayResult("error", "error", response.text);		
                                               } else {
                                                  jsDisplayResult("ok", "ok", response.text);
                                                  self._getQueryAssurances();
                                               }
                                               $("#queryassurance_dialog").dialog("destroy").remove();
                                            },"json");
                                        }
                                    } ,
                                    
                                    "Cancel"    : function()
                                    {
                                       $("#queryassurance_dialog").dialog("destroy").remove();
                                    }
                    } ,
                    open        : function(event, ui)
                    {
					    $(".datepicker").datepicker({
				            showOn: 'both',
				            buttonImage: '/library/jquery/css/calendar.gif',
				            buttonImageOnly: true,
				            dateFormat: 'dd-M-yy',
				            changeMonth:true,
				            changeYear:true		
				        });
                    } ,
                    close       : function(event, ui)
                    {
                        $("#queryassurance_dialog").dialog("destroy").remove();
                    }
          });
        self._loadActionAssuranceStatus();
        $(".action_upload").live("change", function(){
           if($(this).val() !== "")
           {
                self._uploadAttachment(this.id);
           }
        });        
    }, 
    
    _editAssurance      : function(assurance)
    {
        var self = this;
        var html = [];
        html.push("<table width='100%'>");
          html.push("<tr>");
           html.push("<th>Response:</th>");
           html.push("<td>");
             html.push("<textarea cols='50' rows='7' name='response' id='response'>"+assurance.response+"</textarea>");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Action On:</th>");
           html.push("<td>");
             html.push("<input type='text' name='date_tested' id='date_tested' value='"+assurance.date_tested+"' class='datepicker' />");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Status:</th>");
           html.push("<td>");
             html.push("<select name='signoff' id='signoff'>");
             html.push("</select>");
           html.push("</td>");
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Attachment:</th>");
           html.push("<td>");
             html.push("<input type='file' name='qassurance_attachment_"+assurance.id+"' id='qassurance_attachment_"+assurance.id+"' class='action_upload' />");
             html.push("<p class='ui-state ui-state-info'><small>You can attach more than 1 file.</small></p>");
             html.push("<p>"+assurance._attachment+"</p>");
           html.push("</td>");
          html.push("</tr>");
        html.push("</table>");
        if($("#editaqueryassurance_dialog").length > 0)
        {
           $("#editaqueryassurance_dialog").remove();
        }
        
        $("<div />",{id:"editaqueryassurance_dialog"}).append(html.join(' '))
          .dialog({
                    autoOpen    : true,
                    modal       : true,
                    position    : "top",
                    title       : "Edit Query Assuarance",
                    width       : 'auto',
                    buttons     : {
                                    "Save Changes"  : function()
                                    {
                                        $("#msg").addClass("ui-state-info").html("updating...");
                                        if($("#response").val() == "")
                                        {
                                           $("#msg").addClass("ui-state-error").html("Please enter the response");
                                        } else {
                                            $.post("controller.php?action=updateQueryAssurance",{
                                                response    : $("#response").val(),
                                                date_tested : $("#date_tested").val(),
                                                signoff     : $("#signoff :selected").val(),
                                                id          : assurance.id,
                                                risk_id     : self.options.risk_id 
                                            },function(response) {
                                               if(response.error)
                                               {
							 					  jsDisplayResult("error", "error", response.text);		
                                               } else {
                                                  if(response.updated)
                                                  {
                                                    jsDisplayResult("ok", "ok", response.text);
                                                    self._getQueryAssurances();
                                                  } else {
                                                     jsDisplayResult("info", "info", response.text);
                                                  }
                                               }
                                               $("#editaqueryassurance_dialog").dialog("destroy").remove();
                                            },"json");
                                        }
                                    } ,
                                    
                                    "Cancel"    : function()
                                    {
                                        $("#editaqueryassurance_dialog").dialog("destroy").remove();
                                    }
                    } ,
                    open        : function(event, ui)
                    {
					    $(".datepicker").datepicker({
				            showOn: 'both',
				            buttonImage: '/library/jquery/css/calendar.gif',
				            buttonImageOnly: true,
				            dateFormat: 'dd-M-yy',
				            changeMonth:true,
				            changeYear:true		
				        });
                    } ,
                    close       : function(event, ui)
                    {
                        $("#editaqueryassurance_dialog").dialog("destroy").remove();
                    }
          });
          self._loadActionAssuranceStatus(assurance.signoff);
          $(".action_upload").live("change", function(){
            if($(this).val() !== "")
            {
                self._uploadAttachment(this.id);
            }
          });
          
        $(".remove_attach").live("click", function(e){
              var id       = this.id
              var ext      = $(this).attr('title');
              var type     = $(this).attr('ref');
              var filename = $(this).attr("file");
              $.post('controller.php?action=deleteAttachment', 
              { 
                  attachment : id,
                  ext        : ext,
                  type       : type,
                  filename   : filename,
                  query_id   : self.options.risk_id,
                  id         : assurance.id
               }, function(response){     
                   if(response.error)
                   {
                      $("#result_message").css({padding:"5px", clear:"both"}).html(response.text);
                   } else {
                       $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text);
                       $("#li_"+ext).fadeOut();
                       $("#_li_"+ext).fadeOut();
                   }
              },'json');                   
              e.preventDefault();
        });            
          
    } ,
    
    _loadActionAssuranceStatus      : function(id)
    {
        var self = this;
        $.getJSON("controller.php?action=getQueryAssuranceStatus", function(actionStatuses){
           var html = [];
           $("#signoff").html('');
           html.push("<option value=''>--please select--</option>");     
           $.each(actionStatuses, function(index, status){
              if(id == status.id)
              {
                html.push("<option value='"+status.id+"' selected='selected'>"+status.client_terminology+"</option>");
              } else {
                html.push("<option value='"+status.id+"'>"+status.client_terminology+"</option>");
              }
           });
           $("#signoff").append(html.join(' '));
        });
    } ,
    
    _deleteAssurance        : function(assurance)
    {
        $.post("controller.php?action=updateQueryAssurance",
        {
            status   : 2,
            id       : assurance.id
        },function(response) {
           if(response.error)
           {
			  jsDisplayResult("error", "error", response.text);		
           } else {
              if(response.updated)
              {
                jsDisplayResult("ok", "ok", response.text);
                $("#assurance_"+assurance.id).fadeOut();
                self._getQueryAssurances();
              } else {
                 jsDisplayResult("info", "info", response.text);
              }
           }
        },"json"); 
    },
    
    _uploadAttachment       : function(element_id)
    {
        var self = this; 
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
	    $.ajaxFileUpload
	    (
		    {
			    url			  : 'controller.php?action=uploadAttachment&element='+element_id,
			    secureuri	  : false,
			    fileElementId : element_id,
			    dataType	  : 'json',
			    success       : function (response, status)
			    {
                    $("#file_upload").html("");
                    if(response.error )
                    {
                        $("#file_upload").html(response.text )
                    } else {
                        $("#file_upload").html("");
                        if(response.error)
                        {
                            $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                        } else {
                            if($("#result_message"))
                            {
                               $("#result_message").remove();
                            }
                            $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                            ).append($("<div />",{id:"files_uploaded"}))
                            if(!$.isEmptyObject(response.files))
                            {     
                                var list = [];  
                                list.push("<span>Files uploaded ..</span><br />");          
                                $.each(response.files, function(ref, file){
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                list.push("</p>");
                                //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                                });
                                $("#files_uploaded").html(list.join(' '));

                                $(".delete").click(function(e){
                                    var id = this.id
                                    var ext = $(this).attr('title');
                                    $.post('controller.php?action=removeAttachment', 
                                    {
                                         attachment : id,
                                         ext        : ext,
                                         element    : element_id
                                    }, function(response){     
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                             $("#result_message").addClass('ui-state-ok').html(response.text)
                                             $("#li_"+id).fadeOut();
                                        }
                                    },'json');                   
                                    e.preventDefault();
                                 });
                               $("#"+element_id).val("");
                            } 
                        }  				



                        if( $("#delmessage").length != 0)
                        {
                            $("#delmessage").remove()
                        } 
                        $("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
                        .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
                        .append($("<span />",{html:data.file+" "+data.text}))		
                        )						
                        $("#fileloading").append($("<ul />",{id:"attachment_list"}))
                        if(!$.isEmptyObject(data.files))
                        {     
                            var list = [];            
                            $.each(response.files, function(ref, file){
                                list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                                list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                list.push("</p>");
                                //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                            });
                            $("#attachment_list").html(list.join(' '));

                            $(".delete").click(function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('controller.php?action=removeattachment', 
                                {
                                    attachment : id,
                                    ext        : ext,
                                    action_id  : action_id
                                }, function(response){     
                                    if(response.error)
                                    {
                                       $("#result_message").html(response.text)
                                    } else {
                                       $("#result_message").addClass('ui-state-ok').html(response.text)
                                       $("#li_"+id).fadeOut();
                                    }
                                },'json');                   
                                e.preventDefault();
                            });
                            $("#"+element_id).val("");
                        } 
                    }
			    },
			    error: function (data, status, e)
			    {
				    $("#file_upload").html( "Ajax error -- "+e+", please try uploading again"); 
			    }
		    }
	    )
    }
});
