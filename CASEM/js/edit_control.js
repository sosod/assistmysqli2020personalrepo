// JavaScript Document
$(function(){
	
	$("#edit_control").click(function(){
		var message 		= $("#control_message")						
		var ctrl_shortcode	= $("#control_shortcode").val();		
		var control_effect	= $("#control_effect").val();
		var quali_critera 	= $("#quali_critera").val();
		var control_rating	= $("#control_rating").val();
		var color 			= $("#control_color").val();

		if ( ctrl_shortcode == "") {
			message.html("Please enter the short code for this control")
			return false;
		} else if( control_effect == "" ) {
			message.html("Please enter the control effectiveness ");
			return false;
		} else if ( quali_critera == "" ) {
			message.html("Please enter the qualification criteria for this control");
			return false;
		} else if( control_rating == "" ) {
			message.html("Please enter the qualifaction criteria for this control");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=updateControl",
				   {
					  id			: $("#control_id").val(),
					  ctrl			: control_effect,
					  qual			: quali_critera,
					  rating		: control_rating,
					  clr			: color,
					  ctrl_shortcode: ctrl_shortcode
					}
				   , function( retData ) {
						if( retData == 1 ) {
							message.html("Control Effectiveness successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Control Effectiveness not changed");
						} else {
							message.html("Error updating the control effectiveness");
						}					    
			},"json")	
		}
		return false;								  
	});
	
	$("#change_control").click(function() {
		var message = $("#control_message")											
		$.post( "controller.php?action=changeControl", 
			   { 
			   		id		: $("#control_id").val(),
					status  : $("#control_status :selected").val()
			   }, function( deactData ) {
			if( deactData == 1){
				message.html("Control effectveness status changed").animate({opacity:"0.0"},4000);
			} else if( deactData == 0){
				message.html("No change was made to the control effectveness ").animate({opacity:"0.0"},4000);
			} else {
				message.html("Error updating status, please try again ").animate({opacity:"0.0"},4000);
			}										 
		})									
		return false;										  						
	});
	
	$("#cancel_control").click(function(){
		history.back();
		return false;										
	});
	
});