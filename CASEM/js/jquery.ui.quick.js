$.widget("ui.quick", {
	
	options	: {
		start	: 0,
		limit	: 10,
		current	: 1,
		total  	: 0,
		tableId : "quick_"+(Math.floor(Math.random(78) * 56)),
		userId	: 0	
	} , 
	
	_init		: function()
	{
		this._getQuick();	
	} , 
	
	_create		: function()
	{
		$(this.element).append($("<table />",{id:this.options.tableId}))
		
	} , 
	
	_getQuick		: function()
	{
		var self = this;
		$.getJSON("controller.php?action=getQuick",{
				start		: self.options.start,
				limit		: self.options.limit
			},	function( response ){
			$("#"+self.options.tableId).html("");
			self.options.total = response.total;
			self._displayPaging();
			self._displayHeaders();
			if( $.isEmptyObject( response.quick ) ){
				$("#"+self.options.tableId).append($("<tr />")
				   .append($("<td />",{colspan:"7", html:"No quick reports saved"}))		
				)
			} else {
				self.display( response.quick );
			}
			
		});		
		
	} , 
	
	display			: function( quicks  )
	{
		var self  = this;
		$.each(quicks, function( index, quick ){
			$("#"+self.options.tableId).append($("<tr />",{id:"tr_"+quick.id})
				.append($("<td />",{html:quick.id}))
				.append($("<td />",{html:quick.name}))
				.append($("<td />",{html:quick.description}))
				.append($("<td />",{html:"On Screen"}))
				.append($("<td />")
				   .append($("<input />",{
	   					type 	: "button",
	   					name	: "generate_"+quick.id,
	   					id		: "generate_"+quick.id,
	   					value	: "Generate"
				   }))	
				)
				.append($("<td />")
					.append($("<input />",{
						type	  : "button",
						name	  : "edit_"+quick.id,
						id	  : (self.options.userId == quick.insertuser ? "edit_"+quick.id : "edit__"+quick.id),
						value  : "Edit"
					}))						
				)
				.append($("<td />")
					.append($("<input />",{
						type	: "button",
						name	: "del_"+quick.id,
						id		: (self.options.userId == quick.insertuser ? "del_"+quick.id : "del__"+quick.id),
						value	: "Del"
					}))							
				)
			)		
			if(self.options.userId == quick.insertuser)
			{
			    $("#del__"+quick.id).attr('disabled', 'disabled');
			    $("#edit__"+quick.id).attr('disabled', 'disabled');
			}
			
		    $("#generate_"+quick.id).click(function(){
		        document.location.href = "process_report.php?action=generate&id="+quick.id;
		        return false;
		    });
			
		    $("#edit_"+quick.id).click(function(){
		        document.location.href = "edit_quick_report.php?action=edit_quick&id="+quick.id;
		        return false;
		    });
				
		    $("#del_"+quick.id).click(function(){
		    	if(confirm("Are you sure you want to delete this report")){
		    		jsDisplayResult("info", "info", "Updating query...  <img src='../images/loaderA32.gif' />");
		    		$.post("controller.php?action=deleteQuickReport", { id : quick.id } ,function( response ){
						if( response.error )
						{
							jsDisplayResult("error", "error", response.text );
						} else {
							jsDisplayResult("ok", "ok", response.text );
							$("#tr_"+quick.id).fadeOut();
						}
		    		},"json") 		
		    		
		    	}  	
		        return false;
		    });
							
			
		});	
		
	} , 
	
	_displayHeaders		: function()
	{
		var self = this;
		$("#"+self.options.tableId).append($("<tr />")
		   .append($("<th />",{html:"Ref"}))
		   .append($("<th />",{html:"Report Name"}))
		   .append($("<th />",{html:"Report Description"}))
		   .append($("<th />",{html:"Display"}))
		   .append($("<th />",{html:"Action"}))
		   .append($("<th />",{html:""}))
		   .append($("<th />",{html:""}))
		)
		
	} ,
	
	_displayPaging		: function()
	{
		var self		= this;
		if( self.options.total%self.options.limit > 0){
			pages   = Math.ceil(self.options.total/self.options.limit); 				
		} else {
			pages   = Math.floor(self.options.total/self.options.limit); 				
		}

		$("#"+self.options.tableId)
		.append($("<tr/>")
			.append($("<td />",{colspan:"7"})
			  .append($("<input />",{type:"button", value:" |< ", id:"first", name:"first"}))
			  .append("&nbsp;")
			  .append($("<input />",{type:"button", value:" < ", id:"previous", name:"previous"}))
			  .append("&nbsp;&nbsp;&nbsp;")
			  .append($("<span />", {colspan:"2",html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages), align:"center"}))	
			  .append("&nbsp;&nbsp;&nbsp;")	
			  .append($("<input />",{type:"button", value:" > ", id:"next", name:"next"}))			  
			  .append("&nbsp;")
			  .append($("<input />",{type:"button", value:" >| ", id:"last", name:"last"}))				  
			 )													   
		)
	       if(self.options.current < 2)
	       {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
	       }
	       if((self.options.current == pages || pages == 0))
	       {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
	       }	
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
	} ,
	
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getQuick();
	},	
	_getLast  			: function( rk ) {
		//var rk 			   = this;
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getQuick();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getQuick();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getQuick();				
	}	
	
	
		
	
});
