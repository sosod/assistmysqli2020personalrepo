// JavaScript Document
$(function(){
	/**
	Get all the likelihoods
	**/
	$("#likelihood_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getLikelihood", function( data ) {
		$("#likelihood_message").html("")
		var message = $("#likelihood_message")
		$.each( data, function( index, val) {
			$("#likelihood_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.assessment}))
			  .append($("<td />",{html:val.definition}))
			  .append($("<td />",{html:val.probability}))			  
			  .append($("<td />",{css:{"background-color":"#"+val.color},html:val.color }))
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"lkactedit_"+val.id, name:"lkactedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"lkactdel_"+val.id, name:"lkactdel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"lkactact_"+val.id, name:"lkactact_"+val.id}))	
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ),value:"Inactivate", id:"lkactdeact_"+val.id, name:"lkactdeact_"+val.id}))							
			   )		
			  .append($("<td />")
				.append($("<input />",{type:"submit", id:"change_"+val.id, name:"change_"+val.id, value:"Change Status"}))		
			  )
			)
			
			$("#change_"+val.id).live("click",function(){
				document.location.href = "change_like_status.php?id="+val.id
				return false;								   
			});
			
			$("#lkactedit_"+val.id).live( "click", function() {
				document.location.href = "edit_likelihood.php?id="+val.id;
				return false;										  
			});
			$("#lkactdel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this likelihood ")) {														   
					$.post( "controller.php?action=changeLikestat", 
						   {id		: val.id,
						   	status  : 0
						   }, function( delData ) {
						if( delData == 1){
							message.html("Likelihood deleted").animate({opacity:"0.0"},4000);
							$("#tr_"+val.id).fadeOut();
						} else if( delData == 0){
							message.html("No change was made to the likelihood").animate({opacity:"0.0"},4000);
						} else {
							message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
						}										 
					});
				}
				return false;										  
			});
			$("#lkactact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=likeactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						message.html("Likelihood deactivated").animate({opacity:"0.0"},4000);
					} else if( actData == 0){
						message.html("No change was made to the likelihood ").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}					 
				})															 
				return false;										  
			});

		});						   
	} ,"json");
	
	/**
		Adding a new impact
	**/
	$("#add_likelihood").click( function() {
		var message 	= $("#likelihood_message")						
		var rating_from = $("#lkrating_from").val();
		var rating_to 	= $("#lkrating_to").val();
		var assessment	= $("#lkassessment").val();
		var definition  = $("#lkdefinition").val();
		var probability = $("#lkprobability").val();		
		var color 		= $("#llkcolor").val();
	
		if ( rating_from == "") {
			message.html("Please enter the rating feom for this likelihood")
			return false;
		} else if( rating_to == "" ) {
			message.html("Please enter the rating to for this likelihood");
			return false;
		} else if ( assessment == "" ) {
			message.html("Please enter the asssessment for this likelihood");
			return false;
		} else if( definition == "" ) {
			message.html("Please enter the definition for this likelihood");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=newLikelihood",
				   {
					   from		: rating_from,
					   to		: rating_to,
					   assmnt	: assessment,
					   dfn		: definition,
					   prob		: probability,
					   clr		: color
					}
				   , function( retData ) {
				if( retData > 0 ) {
					$("#lkrating_from").val("");
					$("#lkrating_to").val("")
					$("#lkassessment").val("")
					$("#lkdefinition").val("")
					$("#lkprobability").val("")
					message.html("Likelihood saved successifully ...")
					$("#likelihood_table")
					.append($("<tr />")
					  .append($("<td />",{html:retData}))
					  .append($("<td />",{html:rating_from+" to "+rating_to}))
					  .append($("<td />",{html:assessment}))
					  .append($("<td />",{html:definition}))
					  .append($("<td />",{html:probability}))				  
					  .append($("<td />",{css:{"background-color":"#"+color}, html:color }))
					  .append($("<td />")
						.append($("<input />",{type:"submit", value:"Edit", id:"lkactedit", name:"lkactedit"}))	
						.append($("<input />",{type:"submit", value:"Del", id:"lkactdel", name:"lkactde"}))					
					   )
					  .append($("<td />",{html:"<b>Active</b>"})
						//.append($("<input />",{type:"submit", value:"Activate", id:"lkactact", name:"lkactact"}))	
						//.append($("<input />",{type:"submit",value:"Inactivate", id:"lkactdeact", name:"lkactdeact"}))							
					   )
					  .append($("<td />")
						.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))
						)
					)
					$("#change_"+retData).live("click",function(){
						document.location.href = "change_like_status.php?id="+retData
						return false;								   
					});
				} else {
					message.html("Error saving likelihood");	
				}
			},"json")	
		}
		return false;							 
	});		
	
})