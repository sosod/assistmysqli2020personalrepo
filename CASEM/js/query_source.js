//contracttype
$(function(){

     QuerySource.get();
     $("#add").click(function(e){
       QuerySource.addQuerySource();
       e.preventDefault();  
     });
});

var QuerySource    = {

     get            : function()
     {
        var self = this;
        $(".querysource").remove();
        $.getJSON("controller.php?action=getQuerySources", function(querysources){
           if($.isEmptyObject(querysources))
           {
               $("#querysource_table").append($("<tr />").addClass("querysource")
                 .append($("<td />",{colspan:5, html:"There are no query sources"}))
               )
           } else {
               $.each(querysources, function(index, querysource){
                    self._display(querysource)
               });
           }        
        });
     
     } ,
     
     _display       : function(querysource)
     {
       var self = this;
       $("#querysource_table").append($("<tr />").addClass("querysource")
          .append($("<td />",{html:querysource.id}))
          .append($("<td />",{html:querysource.name}))
          .append($("<td />",{html:querysource.description}))
          .append($("<td />",{html:((querysource.status & 1) ? "<b>Active</b>" : "<b>In-Active</b>" )}))
          .append($("<td />")
            .append($("<input />",{type:"button", name:"edit_"+querysource.id, id:"edit_"+querysource.id, value:"Edit"}))
          )
       )
       
       $("#edit_"+querysource.id).live("click", function(e){
          self._updateQuerySource(querysource);
          e.preventDefault();
       });     
       
     } ,
     
     _updateQuerySource         : function(querysource)
     {
         var self = this;
         if($("#editquerysourcedialog").length > 0)
         {
            $("#editquerysourcedialog").remove();
         }
        
        $("<div />",{id:"editquerysourcedialog"}).append($("<table />",{width:"100%"})
           .append($("<tr />")
             .append($("<th />",{html:"Query Source Name :"}))
             .append($("<td />")
               .append($("<input />",{type:"text", name:"name", id:"name", value:querysource.name}))
             )
           )
           .append($("<tr />")
             .append($("<th />",{html:"Query Source Description :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:querysource.description}))
             )
           )
           .append($("<tr />")
             .append($("<td />",{colspan:"2"})
               .append($("<span />",{id:"message"}).css({padding:"5px"}))
             )
           )                                         
        ).dialog({
               autoOpen       : true,
               modal          : true,
               position       : "top",
               title          : "Query Source",
               width          : 500,
               buttons        : {
                                  "Save Changes"       : function()
                                  {
                                    if($("#name").val() == ""){
                                      $("#message").addClass("ui-state-error").html("please enter the query source name");
                                      return false;
                                    } else {
                                       self.update(querysource.id);
                                    }
                                  } ,
                                  
                                  "Cancel"             : function()
                                  {
                                     $("#editquerysourcedialog").dialog("destroy").remove();                                   
                                  } ,
                                  
                                  "Activate"           : function()
                                  {
                                     self._updateStatus(querysource.id, 1);
                                  } ,
                                  
                                  "De-Activate"        : function()
                                  {
                                    self._updateStatus(querysource.id, 0);
                                  } ,
                                  
                                  "Delete"             : function()
                                  {
                                    self._updateStatus(querysource.id, 2);
                                  }
               } ,
               close          : function(event, ui)
               {
                 $("#editquerysourcedialog").dialog("destroy").remove();
               } ,
               open           : function(event, ui)
               {
                var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                var saveBtn       = btns[0];
                var cancelBtn     = btns[1];
                var activateBtn   = btns[2];
                var deactivateBtn = btns[3];
                var deleteBtn     = btns[4];
                    	                
                if((querysource.status & 1) == 0)
                {
                  $(deactivateBtn).css({"display":"none"});
                } else {
                  $(activateBtn).css({"display":"none"});
                }
                
                $(saveBtn).css({"color":"#090"});
                $(deactivateBtn).css({"color":"red"});
                $(activateBtn).css({"color":"#090"});
                $(deleteBtn).css({"color":"red"});
               }         
        })  
        
     
     } ,
     
     addQuerySource          : function()
     {
         var self = this;
         if($("#addquerysourcedialog").length > 0)
         {
            $("#addquerysourcedialog").remove();
         }
        
        $("<div />",{id:"addquerysourcedialog"}).append($("<table />",{width:"100%"})
           .append($("<tr />")
             .append($("<th />",{html:"Query Source Name :"}))
             .append($("<td />")
               .append($("<input />",{type:"text", name:"name", id:"name", value:""}))
             )
           )
           .append($("<tr />")
             .append($("<th />",{html:"Query Source Description :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
             )
           )
           .append($("<tr />")
             .append($("<td />",{colspan:"2"})
               .append($("<span />",{id:"message"}).css({padding:"5px"}))
             )
           )                                         
        ).dialog({
               autoOpen       : true,
               modal          : true,
               position       : "top",
               title          : "Query Source",
               width          : 500,
               buttons        : {
                                  "Save"       : function()
                                  {
                                    if($("#name").val() == ""){
                                      $("#message").addClass("ui-state-error").html("please enter the query source name");
                                      return false;
                                    } else {
                                       self._save();
                                    }
                                  } ,
                                  
                                  "Cancel"             : function()
                                  {
                                     $("#addquerysourcedialog").dialog("destroy").remove();                                   
                                  } 
               } ,
               close          : function(event, ui)
               {
                 $("#addquerysourcedialog").dialog("destroy").remove();
               } ,
               open           : function(event, ui)
               {
                var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                var saveBtn       = btns[0];
                var cancelBtn     = btns[1];

                $(saveBtn).css({"color":"#090"});
               }         
        })  
        
     } ,
     
     _save           : function()
     {    
         var self = this;
         $.post("controller.php?action=saveQuerySource",{
            name         : $("#name").val(),
            description  : $("#description").val()
         }, function(response){
             if(response.error)
             {
               $("#message").addClass("ui-state-error").html( response.text ) 
             } else {
               jsDisplayResult("ok", "ok", response.text );
               $("#addquerysourcedialog").dialog("destroy").remove();      
               self.get();
             }        
         },"json");
     },
     
     _updateStatus        : function(id, status)
     {
         var self = this;
         $.post("controller.php?action=updateQuerySource",{
            id      : id,
            status  : status
         }, function(response){
             if(response.error)
             {
               $("#message").addClass("ui-state-error").html( response.text ) 
             } else {
               if(response.updated)
               {
                 jsDisplayResult("ok", "ok", response.text );     
                 self.get();                         
               } else {     
                 jsDisplayResult("info", "info", response.text );                                                  
               }                  
               $("#editquerysourcedialog").dialog("destroy").remove();                
             }        
         },"json");
     } ,
     
     update              : function(id)
     {
         var self = this;
         $.post("controller.php?action=updateQuerySource",{
            id           : id,
            name         : $("#name").val(),
            description  : $("#description").val()
         }, function(response){
             if(response.error)
             {
               $("#message").addClass("ui-state-error").html( response.text ) 
             } else {
               if(response.updated)
               {
                 jsDisplayResult("ok", "ok", response.text );     
                 self.get();                         
               } else {     
                 jsDisplayResult("info", "info", response.text );                                                  
               }                              
                 $("#editquerysourcedialog").dialog("destroy").remove();                
             }        
         },"json");
              
     }
     

};
