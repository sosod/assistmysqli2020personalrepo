// JavaScript Document
$(function(){
  
	$("#edit_actionstatus").click( function() {
											
		var message 	= $("#editactionstatus_message");
		var name 		= $("#actionstatus").val();
		var client_term = $("#action_client_term").val();
		var color 		= $("#action_color").val();		
		if ( name == "" ) {
			message.html("Please enter the action status name");
			return false;
		} else {
			$.post( "controller.php?action=updateActionStatus", 
			   {
				 id				: $("#action_status_id").val(),
				 name			: name,
				 client_term	: client_term,
				 color			: color
				}, function( retData ) {
					if( retData == 1 ) {
						message.html("Action status successifully updated .. ");						
					} else if( retData == 0 ){
						message.html("Action status not changed");
					} else {
						message.html("Error updating the action staus");
					}
			},"json");	
		}
		return false;								  
	});
	
	$("#cancel_actionstatus").click(function(){
		history.back();
		return false;								   
	})
	
});