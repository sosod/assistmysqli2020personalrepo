var Status = {
  
  addStatus     : function(statusContext)
  {
         
        if($("#"+statusContext+"_status").length > 0)
        {
          $("#"+statusContext+"_status").remove();
        }
        var self = this;
        $("<div />",{id:statusContext+"_status"}).append($("<table />", {width:"100%"})
          .append($("<tr />")
            .append($("<th />",{html:"Status Name :"}))
            .append($("<td />")
              .append($("<input />",{id:"name", name:"name", value:""}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Client Terminology :"}))
            .append($("<td />")
              .append($("<input />",{id:"client_terminology", name:"client_terminology", value:""}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Status Color :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", name:"color", id:"color", readonly:"readonly"}))
            )
          )    
          .append($("<tr />")
            .append($("<td />",{colspan:2})
              .append($("<span />",{id:"message"}).css({padding:"5px;"}))
            )
          )                                      
        ).dialog({
            autoOpen          : true,
            position          : "top",
            modal             : true,
            width             : 500,
            title             : "Add New Status",
            buttons           : {
                                   "Save"         : function()
                                   {
                                      $("#message").addClass("ui-state-info").html("saving . . .<img src='../../images/loaderA32.gif' />" );
                                      if($("#name").val() == "")
                                      {
                                        $("#message").addClass("ui-state-error").html("Status Name is required ");
                                      } else if($("#client_terminology").val() == ""){
                                        $("#message").addClass("ui-state-error").html("Client terminology is required ");
                                      } else {
                                        self._save(statusContext);
                                      }
                                   } ,
                                   
                                   "Cancel"       : function()
                                   {
                                      $("#"+statusContext+"_status").dialog("destroy");
                                      $("#"+statusContext+"_status").remove();
                                   }
            } ,
            close             : function(event, ui)
            {
              $("#"+statusContext+"_status").dialog("destroy");
              $("#"+statusContext+"_status").remove();            
            } ,
            open              : function(event, ui)
            {
			 var myPicker = new jscolor.color(document.getElementById('color'), {})
			 myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable   
            }
        });     
     
  } , 
  
  _save              : function(statusContext)
  {
     var self = this;
     $.post("controller.php?action=save"+statusContext, {
          name                : $("#name").val(),
          client_terminology  : $("#client_terminology").val(),
          color               : $("#color").val()
     },function(response) {
        if(response.error)
        {
          $("#message").addClass("ui-state-error").html( response.text ) 
        } else {
          jsDisplayResult("ok", "ok", response.text );
          $("#"+statusContext+"_status").dialog("destroy");
          $("#"+statusContext+"_status").remove();          
          self.get(statusContext);
        }
     }, "json");
  
  } ,
  
  get            : function(statusContext)
  {
       var self = this;
       $(".status").remove();
       $.getJSON("controller.php?action=getAll"+statusContext, function(statuses) {
          if($.isEmptyObject(statuses))
          {
               $("#"+statusContext+"_table").append($("<tr />").addClass("status")
                .append($("<td />",{colspan:"7", html:"There are no statuses configured"}))
              )
          } else {
             $.each(statuses,function(index, status) {
               self._display(status, self, statusContext);
             });
          }       
       });
     } ,
     
     _display        : function(status, self, statusContext)
     {
          $("#"+statusContext+"_table").append($("<tr />").addClass("status")
           .append($("<td />",{html:status.id}))
           .append($("<td />",{html:status.name}))
           .append($("<td />",{html:status.client_terminology}))
           .append($("<td />")
             .append($("<span />",{html:"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}).css({"background-color":status.color, padding:"1px", width:"100px"}))
           )
          .append($("<td />",{html:((status.status & 1) ? "<b>Active</b>" : "<b>Inactive</b>")}))           
           .append($("<td />")
              .append($("<input />",{type:"button", name:"edit_"+status.id, id:"edit_"+status.id, value:"Edit"}))
           )
          )
          
          $("#edit_"+status.id).live("click", function(e){
            self._editStatus(status, statusContext);
            e.preventDefault();
          });
     } ,  
     
     _editStatus      : function(status, statusContext)
     {
        if($("#"+statusContext+"_"+status.id+"_status").length > 0)
        {
          $("#"+statusContext+"_"+status.id+"_status").remove();
        }
        var self = this;
        $("<div />",{id:statusContext+"_"+status.id+"_status"}).append($("<table />", {width:"100%"})
          .append($("<tr />")
            .append($("<th />",{html:"Status Name :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", id:"name", value:status.name, readonly:(status.defaults ? "readonly" : "")}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Client Terminology :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", id:"client_terminology", value:status.client_terminology}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Status Color :"}))
            .append($("<td />")
              .append($("<input />",{type:"text", name:"color", id:"color", readonly:"readonly"}))
            )
          )    
          .append($("<tr />")
            .append($("<td />",{colspan:2})
              .append($("<span />",{id:"message"}).css({padding:"5px;"}))
            )
          )                                      
        ).dialog({
            autoOpen          : true,
            position          : "top",
            modal             : true,
            width             : 500,
            title             : "Edit "+statusContext+" Status",
            buttons           : {
                                   "Save Changes"         : function()
                                   {
                                      $("#message").addClass("ui-state-info").html("updating . . .<img src='../../images/loaderA32.gif' />" );
                                      if($("#name").val() == "")
                                      {
                                        $("#message").addClass("ui-state-error").html("Status Name is required ");
                                      } else if($("#client_terminology").val() == ""){
                                        $("#message").addClass("ui-state-error").html("Client terminology is required ");
                                      } else {
                                        self._update(status.id, statusContext);
                                      }
                                   } ,
                                   
                                   "Cancel"       : function()
                                   {
                                      $("#"+statusContext+"_"+status.id+"_status").dialog("destroy");
                                      $("#"+statusContext+"_"+status.id+"_status").remove();
                                   } , 
                                   
                                   "Activate"          : function()
                                   {
                                       self.updateStatus(status.id, statusContext, 1);                                   
                                   } , 
                                   
                                   "De-Activate"       : function()
                                   {
                                      self.updateStatus(status.id, statusContext, 0);
                                   } ,
                                   
                                   "Delete"            : function()
                                   {
                                      self.updateStatus(status.id, statusContext, 2);
                                   }
            } ,
            close             : function(event, ui)
            {
              $("#"+statusContext+"_"+status.id+"_status").dialog("destroy");
              $("#"+statusContext+"_"+status.id+"_status").remove();            
            } ,
            open              : function(event, ui)
            {
                var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                var saveBtn       = btns[0];
                var cancelBtn     = btns[1];
                var activateBtn = btns[2];
                var deactivateBtn   = btns[3];
                var deleteBtn     = btns[4];
                    	                
                if((status.status & 1) == 0)
                {
                  $(deactivateBtn).css({"display":"none"});
                } else {
                  $(activateBtn).css({"display":"none"});
                }
                
                if((status.status & 4) == 4)
                {
                  $(deactivateBtn).css({"display":"none"});
                  $(deleteBtn).css({"display":"none"});
                  $(activateBtn).css({"display":"none"});
                }               
                
                $(saveBtn).css({"color":"#090"});
                $(deactivateBtn).css({"color":"red"});
                $(activateBtn).css({"color":"#090"});
                $(deleteBtn).css({"color":"red"});

			 var myPicker = new jscolor.color(document.getElementById('color'), {})
			 myPicker.fromString(status.color)  // now you can access API via 'myPicker' variable   
            }
        });     
     } ,
     
     _update              : function(id, statusContext)
     {
          var self = this;
          $.post("controller.php?action=update"+statusContext, {
               id                  : id,          
               name                : $("#name").val(),
               client_terminology  : $("#client_terminology").val(),
               color               : $("#color").val()
          },function(response) {
             if(response.error)
             {
               $("#message").removeClass("ui-state-info").addClass("ui-state-error").html( response.text ) 
             } else {
               if(!response.updated)
               {
                   jsDisplayResult("info", "info", response.text );
               } else {
                  jsDisplayResult("ok", "ok", response.text );  
                  self.get(statusContext);                    
               }
               $("#"+statusContext+"_"+id+"_status").dialog("destroy");
               $("#"+statusContext+"_"+id+"_status").remove();                       
             }
          }, "json");
     } ,
     
     updateStatus        : function(id, statusContext, status)
     {
        var self = this;
         $.post("controller.php?action=update"+statusContext, {
               id          : id,
               status      : status
          },function(response) {
             if(response.error)
             {
               $("#message").addClass("ui-state-error").html( response.text ) 
             } else {
               if(!response.updated)
               {
                   jsDisplayResult("info", "info", response.text);
               } else {
                  jsDisplayResult("ok", "ok", response.text );         
                  self.get(statusContext);
               }               
               $("#"+statusContext+"_"+id+"_status").dialog("destroy");
               $("#"+statusContext+"_"+id+"_status").remove(); 
             }
          }, "json");
     }
     
     

}
