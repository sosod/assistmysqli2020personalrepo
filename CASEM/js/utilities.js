// JavaScript Document
$(function() {	
	var maxLength = $(".maxlimit").attr("title");
	var id 		  = $(".maxlimit").attr("id");
	$(".count_"+id).html("Max : "+maxLength)

	$('.maxlimit').keydown(function() {
		var remaining = maxLength - $(this).val().length;

		if($(this).val().length > (maxLength == "" ? 255 : maxLength)) {
			return false;
		}
		$(".count_"+id).html(remaining+" characters remaining") 
	});
});

function fileUploader( uploader )
{	
	var elementId = $(uploader).attr("id")
	var actionMethod = $("#actionname").val();
	
	$.ajaxFileUpload({
		fileElementId : elementId,
		url		  	  : "controller.php?action="+actionMethod,
		secureuri  	  : false, 
		dataType  	  : "json",
		success	  	  : function( response, status )
		{
			$("#loading").html( "<br />"+response.text+"<br />" ).css({"color":"green"});
			if( response.total > 1 ){
				$.each( response.other, function( index, value ){
					$("#loading").append($("<span />",{html:value}).css({"color":"orange"}));
					$("#loading").append($("<br />"));
				});
			} else {
				$("#loading").append($("<span />",{html:response.file}).css({"color":"orange"}));
			}
		} , 
		error	  : function( data, status, e )
		{
			$("#loading").html(" <br />Error uploading  "+data.responseText);			
		}
	});
}