$(function(){
	
	ActionColumns.get();
	
	$("#actioncolumns tbody").sortable({
		items	: "tr:not(.disabled)"		
	})
	
});

ActionColumns		= {
	
		get			: function()
		{
			$.getJSON("controller.php?action=getActionColumns", function( actionColumns ){
				
				$.each( actionColumns, function( index , column){
					
					$("#actioncolumns").append($("<tr />")
						.append($("<td />",{html:column.client_terminology}))
						.append($("<td />")
						  .append($("<input />",{type:"hidden", value:column.id, name:"position"}))
						  .append($("<input />",{type:"checkbox", name:"manage", id:"manage_"+column.id, value:column.id}))		
						)
						.append($("<td />")
						  .append($("<input />",{type:"checkbox", name:"new", id:"new_"+column.id, value:column.id}))		
						)
					)
					if((column.active & 4) == 4)
					{
	   				   $("#manage_"+column.id).attr("checked", "checked");
					} 
					if((column.active & 8) ==8)
					{    
					   $("#new_"+column.id).attr("checked", "checked");
					}    					
					
				});
				
				$("#actioncolumns").append($("<tr />")
					.append($("<td />",{colspan:"3"}).css({"text-align":"right"})
					  .append($("<input />",{type:"button", name:"save_changes", id:"save_changes", value:"Update Columns"}))	   
					)		
				)
				
				$("#save_changes").live("click", function(){
					
					jsDisplayResult("info", "info", "Updating columns . . .  <img src='../images/loaderA32.gif' />");
					$.post("controller.php?action=updateColumns", {
						data	: $("#actioncolumnsform").serializeArray()						
					}, function( response ){
						if( response.error ) {
							jsDisplayResult("error", "error", response.text );	
						} else {
							jsDisplayResult("ok", "ok", response.text );
						} 						
					},"json")	
					return false;
				});
			});
		}
	
};
