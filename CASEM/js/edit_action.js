// JavaScript Document
$(function(){
		   
	$("table#edit_action_table").find("th").css({"text-algn":"left"})	   
	
	$(".datepicker_").live( "focus",function(){
		$(this).datepicker({ changeMonth:true, changeYear:true, dateFormat:"d-MM-yy" })
	});
	
	
	$(".datepicker").live( "focus",function(){
		$(this).datepicker({ changeMonth:true, changeYear:true })								 
	});
	
	$("#edit_action").click(function(){
		var message 		= $("#edit_action_message").slideDown();
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val();		
		var rs_deliverable  = $("#deliverable").val();
		var rs_timescale    = $("#timescale").val();		
		var rs_deadline     = $("#deadline").val();
		var rs_progress     = $("#progress").val();
		var rs_status       = $("#action_status :selected").val();
		var rs_remindon     = $("#remindon").val();		
		
		if( rs_action == "" ) {
			jsDisplayResult("error", "error", "Please enter the action");				
			return false;
		} else if( rs_action_owner == "" ) {	
			jsDisplayResult("error", "error", "Please enter the action owner for this action");
			return false;				
		} else if( rs_deliverable == "" ) {		
			jsDisplayResult("error", "error", "Please enter the deliverable for this action");	
			return false;				
		} else if( rs_timescale == "" ) {
			jsDisplayResult("error", "error", "Please enter the time scale for this action");	
			return false;					
		} else if( rs_deadline == "" ) {
			jsDisplayResult("error", "error", "Please enter the deadline for this action");	
			return false;					
		} else {
			
			jsDisplayResult("info", "info", "Updating action...  <img src='../images/loaderA32.gif' />" );
			$.post( "controller.php?action=updateRiskAction" , 
				   {
					   	id				: $("#actionid").val() ,
						r_action 		: rs_action ,
						action_owner    : rs_action_owner ,
						deliverable     : rs_deliverable ,
						timescale       : rs_timescale ,
						deadline     	: rs_deadline ,
						remindon     	: rs_remindon ,
						progress     	: rs_progress ,	
						status			: rs_status
				   } ,
				   function( response ) {
					if( !response.error){ 
						jsDisplayResult("ok", "ok", response.text);	
					} else {
						jsDisplayResult("error", "error", response.text);					
					}
			} ,"json");
		}
		return false;
	});
	
		$("#delete_action").live( "click", function(){					
		if(confirm("Are you sure you want to delete this action ")){	
			jsDisplayResult("info", "info", "Deleting . . .  <img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=deleteAction", { id : $("#actionid").val() }, function( ret ){
				if( ret == 1 ) {
					jsDisplayResult("error", "error", "Query action deleted");	
				} else if( ret == 0 ) {
					jsDisplayResult("info", "info", "No change was made");
				} else {
					jsDisplayResult("error", "error", "Error deleting the query action");	
				}													  
				});
			}
			return false;
		});

	$("#cancel_action").click(function(){
		history.back();
		return false;
	})	
	
	$("#viewactionlog").click(function(){
		$("#actioneditlog").html("<img src='../images/loaderA32.gif' />");
		$.getJSON("controller.php?action=getEditLog", { id:$("#actionid").val() }, function( editLogs ){												
			$("#actioneditlog").html("");		
			
			$("#actioneditlog").append($("<table />",{id:"action_edit_table",width:"67%"}))																				
				$("#action_edit_table").append($("<tr />")
				  .append($("<th />",{html:"Date"}))	
				  .append($("<th />",{html:"Audit Log"}))	
				  .append($("<th />",{html:"Status"}))	
				)						   			
			$.each( editLogs, function( index , action){
				$("#action_edit_table").append($("<tr />")	
				  .append($("<td />",{html:action.date}))	
				  .append($("<td />",{html:action.changeMessage}))	
				  .append($("<td />",{html:((action.status == null || action.status == "") ? "New" : action.status)}))					  
				)						   
			})										   
		});
		return false;								   
	})
	
});



//JavaScript Document
$(function(){
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "dd-M-yy"
		});						   
	});		
	/**
		Get the risk info to display when adding a new action, to show user which risk is biend refered to 
	**/
	$.getJSON("controller.php?action=getARisk", { id : $("#risk_id").val() }, function( riskData ){
		//$("#riskInfo")
		$("#goback").before($("<tr />")
			.append($("<th />",{html:"Query Ref:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.id}))
		  )
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:"Query Reference:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.query_reference}))
		  )		 
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:"Type:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.type}))
		  )
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:"Category:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.cat_descr}))
		  )
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:"Description:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.description}))
		  )	 
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:"Background:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.background}))
		  )	 		 
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:"Query Owner:"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.risk_owner}))
		  )	 
		 $("#goback").before($("<tr />",{rowspan:"6"}))
		 		 
	});
	//========================================================================================================
	$.get("controller.php?action=getUsers", function( data ){
			$.each( data , function( index, val){
				$("#action_owner")
				.append($("<option />",{value:val.tkid, text:val.tkname+" "+val.tksurname}))								
			})
		},"json")
		
		$.get("controller.php?action=getStatus", function( statuses ){
			$.each( statuses , function( index, val){
			$("#action_status")
			.append($("<option />",{value:val.id, text:val.name}))								
			})
		},"json")
	//========================================================================================================
	/**	
		get all the actions associalted with this risk
	
	$.get("controller.php?action=getActions", {id : $("#risk_id").val() }, function( data ){
		var message = $("#addaction_message");
		if( $.isEmptyObject( data ) ) {
			$("#actiondetails")
			.append($("<tr />",{id:"tr_nothing_yet"})
			 .append($("<td />",{colspan:"5",html:"No actions  have been logged yet "}))		  
			)
		} else {
		  $.each( data , function( index ,val ) {
			$("#actiondetails")
			.append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.action}))
				.append($("<td />",{html:(val.status == "" ? "New" : val.status)}))				
				.append($("<td />",{html:(val.progress == "" ? "0" : val.progress)+"%"}))
				)
			});				
		}
	}, "json");**/
	//=============================================================================================================
	/**
		Add another browse button to attach files
	**/
	$("#another_attach").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
  //==============================================================================================================	
	$("#save_action").live( "click", function() {
		//var message 		= $("#addaction_message").slideDown("fast");
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val()		
		var rs_deliverable  = $("#deliverable").val()
		var rs_timescale    = $("#timescale").val()		
		var rs_deadline     = $("#deadline").val()
		var rs_progress     = $("#progress").val()
		var rs_status       = $("#action_status :selected").val()
		var rs_remindon     = $("#remindon").val()		
		
		var rs_attachements  	= [];
		$(".attachents").each( function( index, val){
			rs_attachements.push($(this).val());
		});

		if( rs_action == "" ) {
			jsDisplayResult("error", "error", "Please enter the action");				
			return false;
		} else if( rs_action_owner == "" ) {	
			jsDisplayResult("error", "error", "Please enter the action owner for this action");
			return false;				
		} else if( rs_deliverable == "" ) {		
			jsDisplayResult("error", "error", "Please enter the deliverable for this action");	
			return false;				
		} else if( rs_timescale == "" ) {
			jsDisplayResult("error", "error", "Please enter the time scale for this action");	
			return false;					
		} else if( rs_deadline == "" ) {
			jsDisplayResult("error", "error", "Please enter the deadline for this action");	
			return false;					
		} else {
			jsDisplayResult("info", "info", "Saving action...  <img src='../images/loaderA32.gif' />" );
			$.post( "controller.php?action=newRiskAction" , 
			   {
					id				: $("#risk_id").val() ,
					r_action 		: rs_action ,
					action_owner    : rs_action_owner ,
					deliverable     : rs_deliverable ,
					timescale       : rs_timescale ,
					deadline     	: rs_deadline ,
					remindon     	: rs_remindon ,
					progress     	: "0" ,	
					status			: "1",
					attachment		: rs_attachements					
			   } ,
			   function( response ) {
					 if(response.saved ) {
						 $("#tr_nothing_yet").remove();
						 
						 $("#table_actions")
 						  .append($("<tr />")
							.append($("<td />",{html:response.id}))
							.append($("<td />",{html:rs_action}))
							.append($("<td />",{html:rs_deliverable}))							
							.append($("<td />",{html:"New"}))
							.append($("<td />",{html:"0%"}))	
							.append($("<td />",{html:$("#action_owner :selected").text()}))								
						   ).before($("#extras"))
						  
						 
						$("#action").val("");
						$("#deliverable").val("")
						$("#timescale").val("")
						$("#deadline").val("")
						//$("#progress").val("")
						$("#remindon").val("")	
						if( response.error )
						{
							jsDisplayResult("error", "error", response.text );
						} else {
							jsDisplayResult("ok", "ok", response.text );							
						}
					} else {
						jsDisplayResult("error", "error", response.text );
					}
			   } ,
			   "json");
		}
		return false;
	});
	//=================================================================================================================
	$("#cancel_action").live( "click", function(){
		history.back();	
		return false;
	});
		
});