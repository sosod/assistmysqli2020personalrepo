// JavaScript Document
$(function(){
	Defaults.get();
});

var Defaults = {
		
		defaultsElements  :  [	
			{description:"Are actions going to be applied to risks", defvalue:"yes",id:"actions_aplied_to_risks"},
			{description:"Risk Responsibility is allocated to Directorates. The directorate defines who is the person responsible for the directorates", defvalue:"yes",id:"risk_is_to_directorate"},
			{description:"Risk Responsibility is allocated to Sub-Directorate. The  Sub Directorate defines who is the person responsible for the Sub Directorate", defvalue:"no",id:"risk_is_to_subdirectorate"},
			{description:"Are completed Actions to be signed off", defvalue:"yes",id:"actions_signed_off"},
			{description:"If yes, does the person assigning a action to the risk signs it off.If No, it action defaults to the Risk Manager", defvalue:"yes",id:"person_sign_of"},
			{description:"Will the assuarance provider audit the risks", defvalue:"no",id:"assurance_provider"},
			{description:"Will the assuarance Provider Audit the actions assigned to the risks", defvalue:"no",id:"audit_actions"},
			{description:"Can the Module Administrator make changes to  data in earlier periods", defvalue:"no",id:"module_changes"}							
			] , 
		
	get 	: function()
	{
		$.getJSON("controller.php?action=getDefaults", function( data ){
			Defaults.display( data );														
		});
	} , 
	
	update	: function()
	{
		$("#update_action_risk").click(function(){
			var action_risk = $("input[name='action_risk']").attr("checked","checked");
			
			return false;
		});
	} , 
	
	display 	: function ( defaultsElements )
	{
		$.each( defaultsElements, function( index, val ) {
			$("#defaults_table")								   
			.append($("<tr />")
				.append($("<td />",{html:val.id}))
				.append($("<th />",{align:"left", html:val.description}).css({"text-align":"left"}))
				.append($("<td />")
				  .append($("<select />",{id:"default_settings_"+val.id, name:"default_settings_"+val.id})
					.append($("<option />",{value:"yes",text:"Yes"}))
					.append($("<option />",{value:"no",text:"No"}))
				  )
				)	
				.append($("<td />")
					.append($("<input />",{type:"submit", name:"update_"+val.id, id:"update_"+val.id, value:"Update"}))		  
				)
			)
			if(val.value == 0)
			{
			   $("#default_settings_"+val.id).attr('selected', 'selected');
			}
			if(val.value == 1)
			{
			   $("#default_settings_"+val.id).attr('selected', 'selected');
			}			
			
			$("#update_"+val.id).live("click", function(){
				var message = $("#defaults_message")
				var selected = $("#default_settings_"+val.id+" :selected").val();
				jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
				$.post("controller.php?action=updateDefaults", 
					   {
							name  : val.id,
							value : $("#default_settings_"+val.id+" :selected").val()
						},
					   function( retData ){
						   
						if( retData > 0 ) {
							jsDisplayResult("ok", "ok", "Changes to the default settings saved");
						} else if( retData == 0){
							jsDisplayResult("info", "info", "No changes we made to the default setting");	 
						} else {
							jsDisplayResult("error", "error", "There was an error saving changes, try again");	 		
						}
					   }, "json")									   
				return false;
			});
		});
	}
	
	
}
