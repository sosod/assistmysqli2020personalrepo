$.widget("ui.riskassurance",{
	
	options 	: {
			risk_id			: "",
			start			: 0,
			limit			: 10,
			current			: 1,
			total			: 1,
			assuranceObj	: {},
			tableId 		: "assurance_"+(Math.floor(Math.random(45)*98)),
			month			: {}, 
			toDay 			: "",
			company			: "",
			modref			: ""
	} ,
	
	_init		: function()
	{
		this._getRiskAssurances();		
	} ,
	
	_create		: function()
	{
		//this.element($)
		$(this.element).append($("<tr />")
		  .append($("<td />",{colspan:"7", html:"<h4>Query Assurance</h4>"}))
		)
		
		this.options.month = { 1  : "Jan", 2  : "Feb", 3  : "Mar", 4  : "Apr", 5  : "May",
							   6  : "Jun", 7  : "Jul", 8  : "Aug", 9  : "Sep", 10 : "Oct",
							   11 : "Nov", 12 : "Dec"
							};	
		var today = new Date();
		var min   = today.getMinutes();
		if( min < 10){
			min = "0"+min;				
		} else{
			min = min;
		}
		this.options.toDay	  = today.getDate()+"-"+this.options.month[today.getMonth()+1]+"-"+today.getFullYear()+" "+today.getHours()+":"+min; 
	} ,	
	
	_getRiskAssurances	: function()
	{
		var  self = this;
		$.getJSON("controller.php?action=getRiskAssurances", { 
				  id 	: self.options.risk_id,
				  start : self.options.start,
				  limit : self.options.limit				  
				  }, function( riskAssurances ){
			$(self.element).html("");
			self.options.total = riskAssurances.total;
			self.options.company	   = riskAssurances.company;
			self.options.modref		   = riskAssurances.mod;
			self._displayPaging( riskAssurances.total );														  																
			self._display( riskAssurances.data );														  														
		});
	} ,
	
	_display		: function( assurances )
	{
		var self = this;
		
		$(self.element).append($("<tr />")
			.append($("<th />",{html:"Ref"}))
			.append($("<th />",{html:"System Date and Time "}))
			.append($("<th />",{html:"Date Tested"}))
			.append($("<th />",{html:"Response"}))
			.append($("<th />",{html:"Sign Off"}))
			.append($("<th />",{html:"Assurance Provider"}))
			.append($("<th />",{html:"Attachment"}))
			.append($("<th />"))
		)
	 	
		$.each( assurances , function( index, assurance){
				self._displayAssurance( assurance )			
		});
		
		$(self.element).append($("<tr />",{id:"extra"})
				.append($("<td />",{colspan:"8"})
				  .append($("<input />",{
					  					type	: "button",
					  					name	: "add_assurance",
					  					id		: "add_assurance",
					  					value	: "Add New"					  
				  }))		
				)
		)

		$("#add_assurance").live("click", function(){
			
			if( $("#add_dialog").length > 0){
				$("#add_dialog").remove();
			}
			
			$("<div />",{id:"add_dialog"})
			 .append($("<table />",{width:"100%"})
			   .append($("<tr />")
				 .append($("<th />",{html:"Response:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"response", name:"response", cols:"30", rows:"7"}))		 
				 )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Sign Off"}))
				  .append($("<td />")
					  .append($("<select />",{id:"signoff", name:"signoff"})
						.append($("<option />",{id:"0", text:"No"}))
						.append($("<option />",{id:"1", text:"Yes"}))
					   )						  
				  )
			   )
			  .append($("<tr />")
				 .append($("<th />",{html:"Date Tested"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly"}).addClass("datepicker"))		 
				 )
			  )
			  .append($("<tr />")
				 .append($("<th />",{html:"Attachment:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"file", name:"attachment", id:"attachment"}))		 
				 )
			  )
			  .append($("<tr />")
				 .append($("<th />"))
				 .append($("<td />")
				  .append($("<div />",{id:"loader"}) )		 
				 )
			  )
			 ) 
			.dialog({autoOpen:true, modal:true,width:500, height:"auto", title:"Query Assurance", 
					buttons		: {
									"Save"		: function()
									{
											jsDisplayResult("info", "info", "Saving query assurance . . .  <img src='../images/loaderA32.gif' />");					
											$.post("controller.php?action=newRiskAssurance",{
											   id 			:  self.options.risk_id,
											   date_tested	:  $("#date_tested").val(),
											   response		:  $("#response").val(), 
											   signoff		:  $("#signoff :selected").val()
											},function( response ){
												if( response.error ){
													jsDisplayResult("error", "error", response.text );
												} else {
													$("#extra").before($("<tr />")
														.append($("<td />",{html:response.data.id}))
														.append($("<td />",{html:self.options.toDay}))
														.append($("<td />",{html:$("#date_tested").val()}))
														.append($("<td />",{html:$("#response").val()}))
														.append($("<td />",{html:$("#signoff :selected").text()}))
														.append($("<td />",{html:response.data.assurance_provider+"<em>"+response.data.department+"</em>"}))
														.append($("<td />")
														  .append($("<input />",{type:"button", name:"edit_"+response.data.id, id:"edit_"+response.data.id, value:"Edit"}))
														  .append($("<input />",{type:"button", name:"del_"+response.data.id, id:"del_"+response.data.id, value:"Del"}))
														)
													)
													
													$("#del_"+response.data.id).live("click", function(){
														self._deleteAssurance( response.data );
														return false;
													})
													
													
													$("#edit_"+response.data.id).live("click", function(){
														 self._editAssurance( response.data ) ;
														return false;
													});
													
													if( response.total > 10 ){
														self._getRiskAssurances();
													}
													$("#add_dialog").dialog().dialog("destroy");
													jsDisplayResult("ok", "ok", response.text );
												}
											},"json");
				
									} , 
									
									"Cancel"	: function()
									{
										$("#add_dialog").dialog().dialog("destroy");										
									}
							
				
						}				
			});
					
			$("#attachment").live("change", function(){
				$("#loader")
				.ajaxStart(function(){
					//$(this).html("Uploading . . . <img src='../images/ajax-loader.gif' />").show();
				})
				.ajaxComplete(function(){
					//$(this).hide();
				});

				$.ajaxFileUpload
				(
					{
						url			  : 'controller.php?action=addAssuranceAttachment',
						secureuri	  : false,
						fileElementId : 'attachment',
						dataType	  : 'json',
						success       : function (data, status)
						{
							$("#loader").show().html("");
							if( data.error )
							{
								$("#loader").prepend($("<p />",{html:data.text}))
							} else {
								$("#loader").append($("<p />",{id:"message"}).css({padding:"0.7em", marginLeft:"4px"}).addClass("ui-widget").addClass("ui-state-ok").addClass("ui-corner-all")
								   .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"margin-left":"0.3em", "float":"left"}))
								   .append($("<span />",{html:data.text}))
								)
								if(!$.isEmptyObject(data.other))
								{
									$("#loader").append($("<ul />",{id:"attachment_list"}))
									$.each( data.other, function( index, file) {
										var dotPos = index.indexOf(".");
										var id = index.substr(0, dotPos);
										$("#attachment_list").append($("<li />",{id:"li_"+id})
										  .append($("<span />",{html:file}).css({"float":"left", "padding-right":"0.7em"}))
										  .append($("<span />")
											.append($("<a />",{href:"#", html:"Remove", id:"remove_"+id}).addClass("remove").css({"padding":"0.7em"}))	  
										  )
										)	
										
										$("#remove_"+id).live("click", function(){
											$("#message").html("Removing file . . . <img src='../images/ajax-loader.gif' />").show();
											$.post("controller.php?action=removeAssuranceFile",{
												 file  : file , 
												 index : index 
											} ,function( response ){			
												if( response.error )
												{
													$("#message").html( response.text )
												} else {
													$("#message").html( response.text )
													$("#li_"+id).fadeOut();
												}
											}, "json")										
											return false;
										});										
										
									});	
								}
							}							
							
							
						},
						error: function (data, status, e)
						{
							$("#loader").html("Ajax error => "+e);
						}
					}
				)
				return false;
			});
			
			$(".datepicker").datepicker({
				showOn			: "both",
				buttonImage 	: "/library/jquery/css/calendar.gif",
				buttonImageOnly	: true,
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "dd-M-yy",
				altField		: "#startDate",
				altFormat		: 'd_m_yy'
			});			

			return false;
		});
			
	} , 
	
	_displayAssurance	: function( assurance ){
		var self = this;	
		var attach = "<ul style='list-style:none;'>";
		if( !$.isEmptyObject(assurance.attachment))
		{
			$.each( assurance.attachment, function( index , val){
				var dotPos  = val.indexOf("."); 
				var content = val.substr(dotPos+1);
				var file = index+"."+content;
				attach += "<li id="+index+"><span style='padding-right:0.7em; float:left;'><a href='download.php?company="+self.options.company+"&mod="+self.options.modref+"&folder=assuranceattachment&content="+content+"&file="+file+"'>"+val+"</a></span></li>";
			});
		}
		attach += "</ul>";
		$(self.element).append($("<tr />",{id:"tr_"+assurance.id})
			.append($("<td />",{html:assurance.id}))
			.append($("<td />",{html:self.options.toDay}))
			.append($("<td />",{html:assurance.date_tested}))
			.append($("<td />",{html:assurance.response}))
			.append($("<td />",{html:(assurance.signoff == 0 ?  "No" : "Yes") }))
			.append($("<td />",{html:assurance.assurance_provider+"   (<em>"+assurance.department+"</em>)"}))
			.append($("<td />",{html:attach}))
			.append($("<td />")
			  .append($("<input />",{
				  					type	: "button" ,
				  					name	: "edit_"+assurance.id,
				  					id		: "edit_"+assurance.id,
				  					value	: "Edit"
				    }))
		     .append($("<input />",{
		    	 					type	: "button",
		    	 					name	: "del_"+assurance.id,
		    	 					id		: "del_"+assurance.id,
		    	 					value	: "Del"
		    	 
		     }))
			)
		)			
		
		
		$("#del_"+assurance.id).live("click", function(){
			self._deleteAssurance(assurance);
		});		
		
		
		 $("#edit_"+assurance.id).live("click", function(){
			 self._editAssurance( assurance ) ;
			 return false;
		 })

	},
	
	
	_editAssurance 		: function( assurance )
	{
		var self   = this; 
		var attach = "<ul id='attachment_list'>";
		attach += "<span id='message' />";
		if( !$.isEmptyObject(assurance.attachment))
		{
			$.each( assurance.attachment, function( index , val) {
				var dotPos  = val.indexOf("."); 
				var content = val.substr(dotPos+1);
				var file = index+"."+content;
				//attach += "<li id="+index+"><a href='download.php?company="+self.options.company+"&mod="+self.options.modref+"&new="+val+"&folder=assuranceattachment&content="+content+"&file="+file+">"+val+"</a></li>";				
			    attach += "<li id='li_"+index+"'><span style='padding-right:0.7em; float:left;'><a href='download.php?company="+self.options.company+"&mod="+self.options.modref+"&folder=assuranceattachment&content="+content+"&file="+file+"'>"+val+"</a></span><span><a href='#' id="+index+" class='remove' rel="+val+">Remove</a></span></li>";
			});
		}
		attach	+= "</ul>"; 
		var self = this;
				if( $("#edit_dialog").length > 0){
					$("#edit_dialog").remove();
				}
				
				$("<div />",{id:"edit_dialog"})
				 .append($("<table />",{width:"100%"})
				   .append($("<tr />")
					 .append($("<th />",{html:"Response:"}))
					 .append($("<td />")
					   .append($("<textarea />",{id:"response", name:"response", cols:"30", rows:"7", text:assurance.response}))		 
					 )
				   )
				   .append($("<tr />")
					  .append($("<th />",{html:"Sign Off:"}))
					  .append($("<td />")
						  .append($("<select />",{id:"signoff", name:"signoff"})
							.append($("<option />",{value:"0", text:"No", selected:(assurance.signoff == 0 ? "selected" : "")}))
							.append($("<option />",{value:"1", text:"Yes", selected:(assurance.signoff == 1 ? "selected" : "")}))
						   )						  
					  )
				   )
				  .append($("<tr />")
					 .append($("<th />",{html:"Date Tested:"}))
					 .append($("<td />")
					   .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly", value:assurance.date_tested}).addClass("datepicker"))		 
					 )
				  )
				  .append($("<tr />")
					 .append($("<th />",{html:"Attachment:"}))
					 .append($("<td />")
					   .append($("<div />",{id:"attachments"}))
					   .append($("<div />",{html:attach}))
					   .append($("<input />",{type:"file", name:"attachment", id:"attachment"}))		 
					 )
				  )
				 ) 
				.dialog({autoOpen:true, modal:true,width:500, height:"auto", title:"Query Assurance",
						buttons		: {
										"Update"		: function()
										{
												jsDisplayResult("info", "info", "Updating query assurance . . .  <img src='../images/loaderA32.gif' />");											
												$.post("controller.php?action=updateRiskAssurance",{
												   risk_id 		:  self.options.risk_id,
												   id			:  assurance.id	,
												   date_tested	:  $("#date_tested").val(),
												   response		:  $("#response").val(), 
												   signoff		:  $("#signoff :selected").val()
												},function( response ){
													
													if( response == 1){
														self._getRiskAssurances();
														$("#edit_dialog").dialog().dialog("destroy");
														jsDisplayResult("ok", "ok", "Query assurance updated successfully . . ." );
													} else if( response == 0) {
														jsDisplayResult("info", "info", "No change was made to the assurance" );
													} else {
														jsDisplayResult("error", "error", "Error updating query assurance . . ." );
													}				
												},"json");
					
										} , 
										
										"Cancel"	: function()
										{
											$("#edit_dialog").dialog().dialog("destroy");										
										}
								
					
							}				
				});
					
				$(".remove").live("click", function(){			
					var id   = this.id
					var file = $(this).attr("rel"); 
					$.post("controller.php?action=deleteAssuranceFile",{
						index 	: id,
						file 	: file
					}, function( response ){
						if( $("#messagedeleted").length == 0)
						{
							$("#attachments").prepend($("<p />").css({padding:"0.7em", marginLeft:"4px"}).addClass("ui-widget").addClass("ui-state-ok").addClass("ui-corner-all")
							  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"margin-left":"0.3em", "float":"left"}))
							  .append($("<span />",{id:"messagedeleted"}))
							)							
						}
						if( response.error )
						{
							$("#messagedeleted").html( response.text )
						} else {
							$("#messagedeleted").html( response.text )
							$("#li_"+id).fadeOut();
							$("#"+id).fadeOut();
						}
					},"json")
					return false;
				})
				
				$("#attachment").live("change", function(){

					$.ajaxFileUpload
					(
						{
							url			  : 'controller.php?action=editAssuranceAttachment',
							secureuri	  : false,
							fileElementId : 'attachment',
							dataType	  : 'json',
							success       : function (data, status)
							{
								if( $("#attachmentlist").length > 0 )
								{
									$("#attachmentlist").remove();								
								}
							
								$("#attachments").html("");
								if( data.error )
								{
									$("#attachments").prepend($("<p />",{html:data.text}))
								} else {
									$("#attachments").append($("<p />",{id:"message"}).css({padding:"0.7em", marginLeft:"4px"}).addClass("ui-widget").addClass("ui-state-ok").addClass("ui-corner-all")
									   .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"margin-left":"0.3em", "float":"left"}))
									   .append($("<span />",{html:data.text}))
									)
									if(!$.isEmptyObject(data.other))
									{	
										$("#attachments").append($("<ul />",{id:"attachmentlist"}))
										$.each( data.other, function( index, file) {
											var dotPos = index.indexOf(".");
											var id = index.substr(0, dotPos);
											$("#attachmentlist").append($("<li />",{id:"li_"+id})
											  .append($("<span />",{html:file}).css({"float":"left", "padding-right":"0.7em"}))
											  .append($("<span />")
												.append($("<a />",{href:"#", html:"Remove", id:"remove_"+id}).addClass("remove").css({"padding":"0.7em"}))	  
											  )
											)	
											
											$("#remove_"+id).live("click", function(){
												$("#message").html("Removing file . . . <img src='../images/ajax-loader.gif' />").show();
												$.post("controller.php?action=removeAssuranceFile",{
													 file  : file , 
													 index : index 
												} ,function( response ){			
													if( response.error )
													{
														$("#message").html( response.text )
													} else {
														$("#message").html( response.text )
														$("#li_"+id).fadeOut();
													}
												}, "json")										
												return false;
											});										
											
										});	
									}
								}							
							},
							error: function (data, status, e)
							{
								$("#loader").html("Ajax error => "+e);
							}
						}
					)
					return false;
				});
				
				
				$(".datepicker").datepicker({
					showOn			: "both",
					buttonImage 	: "/library/jquery/css/calendar.gif",
					buttonImageOnly	: true,
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "dd-M-yy",
					altField		: "#startDate",
					altFormat		: 'd_m_yy'
				});			

	
		
	} ,
	
	_deleteAssurance		: function( assurance ){
		
		if( confirm("Are you sure you want to delete this query assurance")){
			jsDisplayResult("info", "info", "Deleting query assurance . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=deleteRiskAssurance", { id:assurance.id}, function( response ){
				if( response.error ){																	  
					jsDisplayResult("error", "error", response.text );							
				} else {
					$("#tr_"+assurance.id).fadeOut();																	  
					jsDisplayResult("ok", "ok", response.text );							
				}																  
			},"json")
		}									 					
		
	} ,
	
	_displayPaging		: function( total )
	{
		var $this 	= this;	
		var pages;
		if( total%$this.options.limit > 0){
			pages = Math.ceil( total/$this.options.limit )
		} else {
			pages = Math.floor( total/$this.options.limit )
		}
	
		$("#riskassurance")
		 .append($("<tr />")
			.append($("<td />",{colspan:8})
			   .append($("<input />",{type:"button", id:"first", name:"first", value:" |< "}))		  
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"previous", name:"previous", value:" < "}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{align:"center", html:"Page "+$this.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))	   			
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", id:"next", name:"next", value:" > "}))		  
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"last", name:"last", value:" >| "}))		 			   
			 )	   
			//.append($("<td />",{colspan:($this.options.columDisplay == "none" ? "3" : "4")}))	   			
		  )
	       if($this.options.current < 2)
	       {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
	       }
	       if(($this.options.current == pages || pages == 0))
	       {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
	       }	
			$("#next").bind("click", function(){
				$this._getNext( $this );
			});
			$("#last").bind("click",  function(){
				$this._getLast( $this );
			});
			$("#previous").bind("click",  function(){
				$this._getPrevious( $this );
			});
			$("#first").bind("click",  function(){
				$this._getFirst( $this );
			});			 
		} , 
		
		_getNext  			: function( $this ) {
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getRiskAssurances();
		},	
		
		_getLast  			: function( $this ) {
			$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
			$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );			
			this._getRiskAssurances();
		},
		
		_getPrevious    	: function( $this ) {
			$this.options.current   = parseFloat( $this.options.current ) - 1;
			$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
			this._getRiskAssurances();			
		},
		
		_getFirst  			: function( $this ) {
			$this.options.current   = 1;
			$this.options.start 	= 0;
			this._getRiskAssurances();				
		}
	
	
	
	
});
