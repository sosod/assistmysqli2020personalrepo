<?php
$scripts 	= array( 'add_action.js','menu.js',  'ajaxfileupload.js'  );
$styles 	= array( 'colorpicker.css',   );
$page_title = "Action Edit";
require_once("../inc/header.php");
$stat 		= new ActionStatus("", "", "");
$statuses 	=  $stat -> getOrderedStatuses();

$actionObj 	= new RiskAction("", "", "", "", "", "", "", "", "");
$action     = $actionObj -> getActionDetail($_GET['id']);

$colObj   = new ActionColumns();
$columns  = $colObj -> getHeaderList();

$action_on = date('d-M-Y');
if(strtotime($action['action_on']) > 0)
{
  $action_on = date("d-M-Y", strtotime($action['action_on']));
}
?>
<script>
$(function(){
	$("table#update").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="update-action-form" name="update-action-form">
	<table align="left" border="1" id="update" width="100%" class="noborder">
    	<tr>
        	<td valign="top" align="left" width="50%" class="noborder">
            	<table width="100%">
                   	<tr>
                   	    <td>Action Details</td>
                        <td align="right">
                          <input type="button" name="show_query" id="show_query" value="<?php echo Risk::OBJECT_NAME; ?>" />
                        &nbsp;&nbsp;&nbsp;
                        </td>
                   	</tr>  
            		<tr>
            			<th><?php echo (isset($columns['action_ref']) ? $columns['action_ref'] : "Ref"); ?>:</th>
            			<td><?php echo $_GET['id']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['query_action']; ?>:</th>
            			<td><?php echo $action['action']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deliverable']; ?>:</th>
            			<td><?php echo $action['deliverable']; ?></td>
            		</tr> 
            		<tr>
            			<th><?php echo $columns['action_owner']; ?>:</th>
            			<td><?php echo $action['action_owner']; ?></td>
            		</tr>             		           		
            		<tr>
            			<th><?php echo $columns['action_status']; ?>:</th>
            			<td><?php echo $action['actionstatus']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['progress']; ?>:</th>
            			<td><?php echo $action['progress']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deadline']; ?>:</th>
            			<td><?php echo $action['deadline']; ?></td>
            		</tr>            		          
            		<tr>
            			<th><?php echo $columns['timescale']; ?>:</th>
            			<td><?php echo $action['timescale']; ?></td>
            		</tr>           		  		
            		<tr id="goback_">
				       	<td align="left" class="noborder">
				        	<?php displayGoBack("",""); ?>
				        </td>
				        <td class="noborder"></td>
            		</tr>
                </table>
            </td>
            <td valign="top" align="right" width="70%" class="noborder">
            	<table id="new_update" width="100%">
                	<tr>
                    	<td colspan="2"><h4>New Update</h4></td>
                    </tr>
                    <tr>
                    	<th>Description:</th>
                        <td><textarea id="description" name="description" cols="35" rows="7"></textarea></td>
                    </tr>
                    <tr>
                    	<th>Status:</th>
                        <td>
                        <select id="status" name="status">
                        	<option disabled=disabled >--status--</option>
                            <?php
                            foreach($statuses as $status ) 
                            {
                            	if($action['status'] == "" || $action['status'] == "0")
                            	{
                            		$action['statusid'] = 1;
                            	}
							?>
                            	<option value="<?php echo $status['id']; ?>"
                                 <?php if($status['name']=="New" || $status['id'] == 1 ) { ?>
                                 	disabled="disabled" 
                               <?php } if($status['id'] == $action['status'] ) { ?>
                                	selected="selected" 
								<?php } ?>>
								<?php echo $status['name']; ?>
                               </option>  
                             <?php
							}
                            ?>
                        </select>
			              <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>Completed actions will be sent to the relevant person(s) for approval and cannot be updated further.</small>
			              </p>                   
			              <input type="hidden" name="_status" id="_status" value="<?php echo $action['status']; ?>"/>
                        </td>
                    </tr>
                    <tr>
                       <th>Action On:</th>
                       <td>
                          <input type="text" name="action_on" id="action_on" class="historydate" readonly="readonly" value="<?php echo $action_on; ?>" />
                       </td>
                    </tr>    
                    <tr>
                    	<th>Progress:</th>
                        <td>
                        	<input type="text" name="progress" id="progress" value="<?php echo $action['progress']; ?>" />
                            <em style="color:#FF0000">%</em>
                            <input type="hidden" name="_progress" id="_progress" value="<?php echo $action['progress']; ?>" />
                        </td>
                    </tr>
                    <tr>
                    	<th>Remind On:</th>
                        <td><input type="text" name="remindon" id="remindon" value="<?php echo $action['remindon']; ?>"  class="datepicker" readonly="readonly" /></td>
                    </tr>
                    <tr>
                    	<th>Attachment:</th>
                        <td>
                            <input id="action_attachment_<?php echo $action['id']; ?>" name="action_attachment_<?php echo $action['id']; ?>" type="file" class="upload_action" />                            
                            <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                              <small>You can attach more than 1 file.</small>
                            </p>
                            <?php
                             Attachment::displayAttachmentList($action['attachement']);
                            ?>
                        </td>
                    </tr>
				    <tr>
					    <th>Send Approval Notification:</th>
					    <td>
						    <input type="checkbox" id="approval" name="approval"
							    <?php if($action['progress'] !== "100" || $action['status'] != 3) { ?> disabled="disabled" <?php } ?> value="1"  />
							    
			              <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>This will send an email notification to the relevant person(s) responsible for approving this Action.</small>
			              </p>
					    </td>
				    </tr> 
                    <tr>
                    	<th></th>
                        <td>
                        	<input type="submit" name="save_action_update" id="save_action_update" value="Save" class="isubmit"  />
                            <input type="submit" name="cancel_action_update" id="cancel_action_update" value="Cancel" class="idelete"  />
                            <span style="float:right;">
                            <?php displayAuditLogLink("actions_update" , false); ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
	<input type="hidden" class="logid" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
</form>
</div>
<div id="view_action_updates" style="clear:both;"></div>
