<?php
$scripts = array( 'menu.js', "jquery.ui.approve.js");
$styles = array();
$page_title = "Approve Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#test").approve({loggedUserId:$("#userID").val()});
	})
</script>
<?php JSdisplayResultObj(""); ?>
<div id="test"></div>
<div id="approve_message" class="message"></div>	
<div id="displayactions">
	<input type="hidden" id="userID" value="<?php echo $_SESSION['tid']; ?>" name="userID"  />
</div>
<div id="approve_dialog" style="display:none; z-index:999px; background-color:#999999; color:#FFFFFF; border:1px solid #000000; width:250px; position:absolute;">
	<table border="2" align="center">
    	<tr>
        	<td>Response : </td>
            <td><textarea id="approval_response" name="approval_response"></textarea></td>
        </tr>
       <tr>
       	<td>Sign Off</td>
        <td>
        	<select name="approval_signoff" id="approval_signoff">
            	<option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
        </td>
       </tr> 
        <tr>
        	<td colspan="2">
             <input type="submit" name="approve_action" id="approve_action" value="Approve" />
             <input type="submit" name="decline_action" id="decline_action" value="Decline" /> 
             <input type="hidden"  name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>" />
            </td>
        </tr>       
        <tr>
        	<td align="right" colspan="2"><a href="" id="closedialog">Close</a></td>
        </tr>
    </table>
</div>
</body>

