<?php
$scripts = array( 'jquery.ui.action.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Action update";
require_once("../inc/header.php");
$risk 	  = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$query 	  = $risk -> getQueryDetail($_GET['id']);

//echo (17665 & 1024)."<br /><br />";
$query_logs = $risk -> getQueryActivityLog($_GET['id']);

$colObj   = new QueryColumns();
$rowNames = $colObj -> getHeaderList();

?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder" valign="top" width="50%">
			<table align="left"border="1" id="edit_action_table" width="100%"> 
    			<tr>
    				<td colspan="2"><h4><?php echo Risk::OBJECT_NAME; ?> Details</h4></td>
    			</tr>
       		    <tr>
                    <th><?php echo $rowNames['query_item']; ?>:</th>
                    <td>#<?php echo $query['query_item']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_reference']; ?>:</th>
                    <td><?php echo $query['query_reference']; ?></td>
                  </tr>
                 <tr>
                    <th><?php echo $rowNames['query_source']; ?>:</th>
                    <td><?php echo $query['query_source']; ?></td>
                  </tr>           
                  <tr>
                    <th><?php echo $rowNames['query_description']; ?>:</th>
                    <td><?php echo $query['description']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_background']; ?>:</th>
                    <td><?php echo $query['background']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_type']; ?>:</th>
                    <td> <?php echo $query['query_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_category']; ?>:</th>
                    <td><?php echo $query['query_category']; ?></td>
                  </tr>
	              <tr>
                    <th><?php echo $rowNames['financial_exposure']; ?>:</th>
                    <td><?php echo $query['financial_exposure']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['monetary_implication']; ?>:</th>
                    <td><?php echo $query['monetary_implication']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_level']; ?>:</th>
                    <td><?php echo $query['risk_level']; ?></td>
                  </tr>      
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_type']; ?>:</th>
                    <td><?php echo $query['risk_type']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_detail']; ?>:</th>
                    <td><?php echo $query['risk_detail']; ?></td>
                  </tr>      
                  <tr class="more_details">
                    <th><?php echo $rowNames['finding']; ?>:</th>
                    <td><?php echo $query['finding']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
                    <td><?php echo $query['internal_control_deficiency']; ?></td>
                  </tr>        
                  <tr class="more_details">
                    <th><?php echo $rowNames['recommendation']; ?>:</th>
                    <td><?php echo $query['recommendation']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['client_response']; ?>:</th>
                    <td><?php echo $query['client_response']; ?></td>
                  </tr>    
                  <tr class="more_details">
                    <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
                    <td><?php echo $query['auditor_conclusion']; ?></td>
                  </tr> 
                 <tr>
                   <th><?php echo $rowNames['financial_year']; ?>:</th>
                   <td><?php echo $query['financial_year']; ?></td>
                 </tr>
                 <tr>
                    <th><?php echo $rowNames['query_owner']; ?>:</th>
                    <td><?php echo $query['query_owner']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_date']; ?>:</th>
                    <td><?php echo $query['query_date']; ?></td>
                  </tr>      
                  <tr>
                  	<th><?php echo $rowNames['query_deadline_date']; ?>:</th>
                  	<td><?php echo $query['query_deadline_date']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['attachements']; ?>:</th>
                    <td><?php Attachment::displayAttachmentOnly($query['attachment'], 'query');  ?></td>
                  </tr> 
				  <tr id="go_back">
					<td class="noborder" align="left" width="150">
						<?php displayGoBack("",""); ?>
					</td>
					<td class="noborder">

					</td>
				  </tr>		             
			</table>
    </td>
    <td class="noborder" valign="top" width="50%">
	    <table width="100%">
	      <tr>
	        <th class="th2" colspan='4'>Activity Log</th>
	      </tr>
	      <tr>
	        <th>Date Logged</th>
	        <th>User</th>
	        <th>Change Log</th>
	        <th>Status</th>
	      </tr>
	      <?php
	        if(!empty($query_logs))
	        {
	           foreach($query_logs as $q_index => $log)
	           {
            ?>	            
	          <tr>
	            <td align="center"><?php echo $log['date_loged']; ?></td>
	            <td><?php echo $log['user']; ?></td>
	            <td><?php echo $log['changeMessage']; ?></td>
	            <td><?php echo $log['status']; ?></td>
	          </tr>	        
	        <?php   
	           }
	        } else {
	        ?>
	          <tr>
	            <td align="center" colspan="4">There are no logs yet</td>
	          </tr>	        	        
	        <?php
	        }
	      ?>
	    </table>        
    </td>
  </tr>
</table>
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
