<?php
$scripts    = array('jquery.query.notification.js','menu.js');
$styles     = array( 'colorpicker.css' );
$page_title = "My profile";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#notifications").notification();
	});
</script>
<?php JSdisplayResultObj(""); ?>
<div id="notifications"></div>
<?php displayAuditLogLink("user_notifications_logs", true) ?>

