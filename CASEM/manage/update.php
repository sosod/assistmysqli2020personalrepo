<?php
$scripts    = array( 'jquery.ui.query.js', 'menu.js',  );
$styles     = array( 'colorpicker.css' );
$page_title = "Edit Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#risk").query({updateQuery:true, updateActions:true, view:"viewMyn", page:"update", section:"manage"});	
	});
</script>
<div id="risk"></div>
