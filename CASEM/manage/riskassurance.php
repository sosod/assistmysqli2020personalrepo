<?php
$scripts = array('jquery.query.queryassurance.js', 'ajaxfileupload.js' );
$styles = array( );
$page_title = "Assurance of risk";
require_once("../inc/header.php");
$risk 	  = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$query 	  = $risk -> getQueryDetail($_GET['id']);
$colObj   = new QueryColumns();
$rowNames = $colObj -> getHeaderList();
?>
<script>
$(function(){
	$("table#risk_assurance_table").find("th").css({"text-align":"left"})
	$("#riskassurance").queryassurance({risk_id:$("#risk_id").val()});
});
</script>
<?php JSdisplayResultObj(""); ?>
<form method="post" name="assurance-form" id="assurance-form">
<table align="left" border="1" id="assurance" class="noborder" width="100%">
	<tr>
    	<td valign="top" width="50%" class="noborder">
        	<table width="100%">
            	<tr>
                	<td colspan="2"><h4>Query Details</h4></td>                  
                </tr>
       		    <tr>
                    <th><?php echo $rowNames['query_item']; ?>:</th>
                    <td>#<?php echo $query['query_item']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_reference']; ?>:</th>
                    <td><?php echo $query['query_reference']; ?></td>
                  </tr>      
                  <tr>
                    <th><?php echo $rowNames['query_description']; ?>:</th>
                    <td><?php echo $query['description']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_background']; ?>:</th>
                    <td><?php echo $query['background']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_type']; ?>:</th>
                    <td> <?php echo $query['query_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_category']; ?>:</th>
                    <td><?php echo $query['query_category']; ?></td>
                  </tr>
	              <tr>
                    <th><?php echo $rowNames['financial_exposure']; ?>:</th>
                    <td><?php echo $query['financial_exposure']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['monetary_implication']; ?>:</th>
                    <td><?php echo $query['monetary_implication']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_level']; ?>:</th>
                    <td><?php echo $query['risk_level']; ?></td>
                  </tr>      
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_type']; ?>:</th>
                    <td><?php echo $query['risk_type']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_detail']; ?>:</th>
                    <td><?php echo $query['risk_detail']; ?></td>
                  </tr>      
                  <tr class="more_details">
                    <th><?php echo $rowNames['finding']; ?>:</th>
                    <td><?php echo $query['finding']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
                    <td><?php echo $query['internal_control_deficiency']; ?></td>
                  </tr>        
                  <tr class="more_details">
                    <th><?php echo $rowNames['recommendation']; ?>:</th>
                    <td><?php echo $query['recommendation']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['client_response']; ?>:</th>
                    <td><?php echo $query['client_response']; ?></td>
                  </tr>    
                  <tr class="more_details">
                    <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
                    <td><?php echo $query['auditor_conclusion']; ?></td>
                  </tr> 
                 <tr>
                   <th><?php echo $rowNames['financial_year']; ?>:</th>
                   <td><?php echo $query['financial_year']; ?></td>
                 </tr>
                 <tr>
                    <th><?php echo $rowNames['query_owner']; ?>:</th>
                    <td><?php echo $query['query_owner']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_date']; ?>:</th>
                    <td><?php echo $query['query_date']; ?></td>
                  </tr>      
                  <tr>
                  	<th><?php echo $rowNames['query_deadline_date']; ?>:</th>
                  	<td><?php echo $query['query_deadline_date']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['attachements']; ?>:</th>
                    <td><?php Attachment::displayAttachmentOnly($query['attachment'], 'query');  ?></td>
                  </tr>                
                <tr>
	                <td align="left" class="noborder">
	        			<?php displayGoBack("",""); ?>
	        		</td>
	        		<td class="noborder">
	        			<?php displayAuditLogLink( "assurance_logs" , false); ?>
	        		</td>
        		</tr>    
        		<tr>
        			<td class="noborder"></td>
        			<td class="noborder"><input type="submit" value="View Actions Associated to Query", id="view_actions" /></td>
        		</tr> 
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
        	<table  class="noborder" width="100%" id="riskassurance">
        		<tr>
        			<td id="add_riskassurance" style="display:none;" class="noborder">
        				<span id="newassurance_message"></span>
        			</td>
        		</tr>
        	</table>
        </td>
    </tr>
</table>
<input type="hidden" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" id="risk_id"  /> 
</form>
