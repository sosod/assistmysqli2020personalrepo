<?php
$scripts = array(  'jquery.ui.query.js', 'menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#risk").query({editQuery:true, editActions:true, view:"viewAll"});	
	});
</script>
<div id="risk"></div>
