<?php
	$scripts = array( 'fixedreports.js' );
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../inc/header.php");
?>
<table width="">
	<tr>
    <td valign="top">
        <h1 align="center"><?php echo ucwords($_SESSION['cc']); ?></h1>
        <?php
            $rep  = new Report();
            $rep -> drawBarGraph();
        ?>
    </td>
  	  <td valign="top">
		<table id="risk_table" width="100%"></table>
   	  </td>
    </tr>
</table>
<p>
	<?php echo "<em>Report genereated on ".date("d")." ".date("F")." ".date("Y")."</em> at ".date("H:m"); ?>
</p>