<?php
	$scripts = array('menu.js',  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Generate Report";
	require_once("../inc/header.php");


/**************************************	
		START JANET'S CODE
***************************************/
$row_count = 0;
/** GET HEADINGS **/
include("generate_headings.php");

/** START OF CODE COPED FROM COMPLAINTS REPORT.php **/
/***** FUNCTION TO RESET FORM *****/
function resetFields(&$fieldP,&$filterP,&$groupP,&$sortP,&$reportP) {
	global $head;
	global $nosort;
	foreach($head as $h) {
		$hf = $h['field'];
		$fieldP[$hf] = array($hf,"Y");
		$ht = $h['type'];
		if($ht == "Y" || $ht == "L") {
			$filterP[$hf] = array($hf,array("X"),"");
		} elseif($ht=="D") {
			$filterP[$hf] = array($hf,array("",""),"any");
		} else {
			$filterP[$hf] = array($hf,"","any");
		}
		$groupP = "X";
		if(!in_array($hf,$nosort)) { 
			$sortP[] = $hf;
		}
	}
	$reportP = array('id'=>"",'rhead'=>"",'rname'=>"");
}
/***** FILTER DISPLAY FUNCTIONS *****/
function filterText($hf,$hfilter) {
				echo "<input type=text name=".$hf."filter value=".$hfilter[1].">";
				echo " <select name=".$hf."filtertype>";
					echo "<option "; if($hfilter[2]=="any" || !isset($hfilter[1])) { echo "selected"; } echo " value=any>Match any word</option>";
					echo "<option "; if($hfilter[2]=="all") { echo "selected"; } echo " value=all>Match all words</option>";
					echo "<option "; if($hfilter[2]=="exact") { echo "selected"; } echo " value=exact>Match exact phrase</option>";
				echo "</select>";
}
function filterDate($hf,$hfilter) {
	echo "From <input type=text class=datepicker size=10 name=".$hf."filter[] value=\"".$hfilter[1][0]."\" id=".$hf."filterfrom>";
	echo " to <input type=text class=datepicker size=10 name=".$hf."filter[] value=\"".$hfilter[1][1]."\" id=".$hf."filterto>";
	echo " <input type=button value=Clear onclick=\"resetDate('".$hf."filter');\">";
}
function filterSelect($hf,$hfilter,$list) {
				$echo = "";
				/*if(count($list)<=5) {	
					foreach($list as $l) {
						if(strlen($echo)>0) { $echo.="<br />"; }
						$echo.= "<input type=checkbox name=".$hf."filter[] ";
							if(in_array($l['id'],$hfilter[1]) || !isset($hfilter[1]) || in_array("X",$hfilter[1])) { $echo.= "checked"; }
						$echo.= " value=".$l['id']."> ".$l['value'];
					}
				} else {*/
					$size = (count($list)>5) ? 6 : count($list) + 1;
					$echo= "<select name=".$hf."filter[] multiple size=$size >";
						$echo.= "<option "; if(in_array("X",$hfilter[1]) || !isset($hfilter[1])) { $echo.= "selected"; } $echo.= " value=X>ALL</option>";
						foreach($list as $l) {
							$echo.= "<option value=".$l['id'];
								if(in_array($l['id'],$hfilter[1])) { $echo.=" selected"; }
							$echo.= ">".$l['value']."</option>";
						}
					$echo.= "</select>";
					$echo.= "<br /><i><small>Use CTRL key to select multiple options</small></i>";
				//}
				echo $echo;
}

/***** VARIABLES *****/
$field = array();
$filter = array();			//filter[headfield] = array(headfield,filter,filtertype);
$sort = array();
$group = "";
//$nogroup => inc_ignite.php
//$nosort => inc_ignite.php
//$nofilter => inc_ignite.php
$output = "display";
$report = array();
$var = $_REQUEST;

/***** SAVE QUICK REPORT *****/
if(isset($var['act'])) {
	switch($var['act']) {
		case "DEL":
			if(checkIntRef($var['i'])) {
				$sql = "UPDATE ".$dbref."_reports SET yn = 'N', modified_on = $today WHERE id = ".$var['i'];
				include("generate_db.php");
				$result[0] = "check";
				$result[1] = "The report has been deleted.";
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred.  The requested report could not be found.  Please try again.";
				resetFields($field,$filter,$group,$sort,$report);
			}
			break;
		case "EDIT":
		case "SAVE":
			//get variables as used by generate_process.php
			$field = array();
			$field['id'] = "Y";
			$filter = array();
			$filtertype = array();
			$records = array();
			$sortarr = $var['sort'];
			if(count($sortarr)==0) { $sort = array(); }
			//GET FIELD HEADINGS
				foreach($head as $row)
				{
					$hf = $row['field'];
					if(isset($_REQUEST[$hf]) && $_REQUEST[$hf]=="Y") {
						$field[$hf] = array($hf,"Y");
					} else {
						$field[$hf] = array($hf,"N");
					}
					if(!in_array($hf,$nofilter)) {
						$filter[$hf] = array($hf,$_REQUEST[$hf.'filter']);
						if($row['type']=="T" || $row['type']=="M") {
							$filter[$hf][2] = $_REQUEST[$hf.'filtertype'];
						} else {
							$filter[$hf][2] = "";
						}
						if(count($sortarr)==0 && !in_array($hf,$nosort)) {
							$sort[] = $hf;
						}
					}
				}
			if(count($sortarr)==0) { $sortarr = $sort; $sort = null; }
			$sort = $sortarr;
			//convert to db format    field: headfield|=|value1|_|     filter: headfield|=|filter|=|filtertype|_|
			$field2 = array();
			foreach($field as $f) { $field2[] = implode("|=|",$f); }
			$db_field = implode("|_|",$field2);
			$filter2 = array();
			foreach($filter as $f) {
				$hf = $f[0];
				if($head[$hf]['type']!="T" && $head[$hf]['type']!="M") {
					$f1 = implode("|",$f[1]);
					$f[1] = $f1;
				}
				$filter2[] = strFn("implode",$f,"|=|","");
			}
			$db_filter = strFn("implode",$filter2,"|_|","");
			$db_sortby = strFn("implode",$sort,"|_|","");
			$db_output = $var['output'];
			$output = $db_output;
			$db_groupby = $var['groupby'];
			$group = $db_groupby;
			$db_rname = code($var['rname']);
			$db_rhead = code($var['rhead']);
			if($var['act']=="EDIT" && checkIntRef($var['i'])) {
				$sql = "UPDATE ".$dbref."_reports SET rname = '$db_rname', rhead = '$db_rhead', fields = '$db_field', filters = '$db_filter', sort_by = '$db_sortby', group_by = '$db_groupby', export_to = '$db_output', modified_on = $today WHERE id = ".$var['i'];
				include("generate_db.php");
				$report['id'] = mysql_insert_id($con);
				$report['rname'] = $db_rname;
				$report['rhead'] = $db_rhead;
				$var['a'] = "E";
				$result[0] = "check";
				$result[1] = "Changes have been saved to report '$db_rname'. <input type=submit value=\"Generate Report\" onclick=\"document.report.submit();\">";
			} else {
				$sql = "INSERT INTO ".$dbref."_reports (tkid,rtype,rname,rhead,fields,filters,sort_by,group_by,export_to,yn,added_on,modified_on) ";
				$sql.= "VALUES ('$tkid','','$db_rname','$db_rhead','$db_field','$db_filter','$db_sortby','$db_groupby','$db_output','Y',$today,0)";
				//echo "<P>".$sql;
				include("generate_db.php");
				$report['id'] = mysql_insert_id($con);
				$report['rname'] = $db_rname;
				$report['rhead'] = $db_rhead;
				$var['a'] = "E";
				$result[0] = "check";
				$result[1] = "Report '$db_rname' has been saved. <input type=submit value=\"Generate Report\" onclick=\"document.report.submit();\">";
			}
			break;
		case "VIEW":
			if(checkIntRef($var['i'])) {
				$report['id'] = $var['i'];
				$sql = "SELECT * FROM ".$dbref."_reports WHERE id = ".$report['id'];
				include("generate_db.php");
					$report = mysql_fetch_array($rs);
				mysql_close($con);
				$db_filter = $report['filters'];
				$group = $report['group_by'];
				$output = $report['export_to'];
				$sort = explode("|_|",$report['sort_by']);
				$fields2 = strFn("explode",$report['fields'],"|_|","");
				foreach($fields2 as $f) {
					$f2 = strFn("explode",$f,"|=|","");
					$field[$f2[0]] = array($f2[0],$f2[1]);
				}
				$filter2 = strFn("explode",$report['filters'],"|_|","");
				foreach($filter2 as $f) {
					$f2 = strFn("explode",$f,"|=|","");
					$hf = $f2[0];
					if($head[$hf]['type']!="T" && $head[$hf]['type']!="M") {
						$f1 = explode("|",$f2[1]);
					} else {
						$f1 = $f2[1];
					}
					$filter[$hf] = array($hf,$f1,$f2[2]);
				}
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred.  The requested report could not be found.  Please try again.";
				resetFields($field,$filter,$group,$sort,$report);
			}
			break;
		default:
			resetFields($field,$filter,$group,$sort,$report);
			break;
	}
} else {	//No save/edit/delete detected therefore create blank form
	resetFields($field,$filter,$group,$sort,$report);
}

$s = 1;	//variable used to number sections
$tr = 0; //variable for colour change effect in filters

if(isset($result[0])) {
	displayResColor($result);
}


?>
<script type="text/javascript">
			$(function(){
                $('.datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: '/lib/jquery/css/blue/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
					changeMonth: true,
					changeYear: true
                });
				$( "#sortable" ).sortable({
					placeholder: "ui-state-highlightsort"
				});
				$( "#sortable" ).disableSelection();
			});
function resetDate(hf) {
	document.getElementById(hf+'from').value = '';
	document.getElementById(hf+'to').value = '';
}
function Validate(me) {
    return true;
}
function dateFiler(me) {
}
function checks(c,e) {
	var act;
	if(c=="U") { act = false; } else { act = true; }
	for(i=0;i<e;i++) {	
		if(c=="I") {
			if(document.forms[0].elements[i].checked==true) 
				document.forms[0].elements[i].checked = false;
			else
				document.forms[0].elements[i].checked = true;
		} else {
			document.forms[0].elements[i].checked = act;
		}
	}
}
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
function saveReport(act) {
	switch(act) {
		case "D":
			document.report.act.value = "DEL";
			break;
		case "E":
			document.report.act.value = "EDIT";
			break;
		case "N":	//NEW REPORT
		default:
			document.report.act.value = "SAVE";
	}
	document.report.action = "generate.php";
	document.report.submit();
}
function viewReport() {
	var i = document.getElementById('i').value;
	if(isNaN(parseInt(i))) 
		document.location.href = "generate.php";
	else
		document.location.href = "generate.php?act=VIEW&i="+i;
}
</script>
<style type=text/css>
    table {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 0px solid #ababab;
    }
	.b-w {
		border: 0px solid #ffffff;
	}
	.filter {
		vertical-align: top;
		padding: 10 10 10 3;
	}
	.head {
		font-weight: bold;
	}
	.blank { background-color: #ffffff; }
	.tdhover { background-color: #e1e1e1; }	
	h3 { margin-bottom: 0px; margin-top: 20px; }
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.0em; height: 18px; width: 200px;}
	#sortable li span { position: absolute; margin-left: -1.3em; }
	.ui-state-highlightsort { height: 1.5em; line-height: 1.2em; }
	.shortcut { text-decoration: underline; cursor: hand; }
	.ui-state-highlight-green, .ui-widget-content .ui-state-highlight-green, .ui-widget-header .ui-state-highlight-green  {border: 1px solid #009900; background: #f7fff7 ; color: #333333; }
	.ui-state-highlight-green a, .ui-widget-content .ui-state-highlight-green a,.ui-widget-header .ui-state-highlight-green a { color: #333333; }
	.ui-state-error-red, .ui-widget-content .ui-state-error-red, .ui-widget-header .ui-state-error-red {border: 1px solid #cc0001; background: #fef1ec url(images/ui-bg_inset-soft_95_fef1ec_1x100.png) 50% bottom repeat-x; color: #cc0001; }
	.ui-state-error-red a, .ui-widget-content .ui-state-error-red a, .ui-widget-header .ui-state-error-red a { color: #cc0001; }
	.ui-state-highlight-green .ui-icon {background-image: url(/lib/jquery/css/blue_orange/images/ui-icons_009900_256x240.png); }
	.ui-state-error-red .ui-icon, .ui-state-error-text .ui-icon {background-image: url(/lib/jquery/css/blue_orange/images/ui-icons_cc0001_256x240.png); }
</style>
<?php
$sql = "SELECT * FROM ".$dbref."_reports WHERE tkid = '$tkid' AND yn = 'Y' ORDER BY rname";
include("generate_db.php");
if(mysql_num_rows($rs)>0) {
	echo "<P><B>Quick Report:</b> <select id=i>";
		echo "<option "; if(!checkIntRef($report['id'])) { echo " selected "; } echo "value=X>Blank report generator</option>";
		while($row = mysql_fetch_array($rs)) {
			echo "<option "; if($report['id']==$row['id']) { echo " selected "; } echo "value=".$row['id'].">".$row['rname']." [";
			if($row['modified_on']>0) {
				echo "Edited: ".date("d M Y",$row['modified_on']);
			} else {
				echo "Added: ".date("d M Y",$row['added_on']);
			}
			echo "]"."</option>";
		}
	echo "</select> <input type=button value=Go onclick=\"viewReport()\"/></p>";
}
mysql_close($con);
?>
<form name=report method=post action=generate_process.php  onsubmit="return Validate(this);">
<?php
/***** FIELDS *****/
echo "<h3>".$s.". Select the information to be displayed in the report:</h3>"; $s++;
echo "<div style=\"margin-left: 4px\">";
echo "<table cellpadding=13 cellspacing=0 style=\"margin-top: -13px; margin-bottom: -10px;\">";
    echo "<tr>";
        echo "<td valign=top  style=\"\">";
            echo "<table cellpadding=3 cellspacing=0  style=\"border-width: 0px\">";
	//Calculate column breaks
	$mnr = count($head)-1;
    $m2 = $mnr / 3;
    $m = round($m2);
    if($m < $m2) { $m++; }	//rows per column
    $r=0;	//rows
	foreach($head as $row)
    { if($row['field']!="id") {
        $r++;
		//if row exceeds row per column, apply column break
        if($r > $m) {
            $r=1;
			//CLOSE PREVIOUS COLUMN
            echo "</table>";
			echo "</td>";
			//OPEN NEW COLUMN
			echo "<td valign=top  style=\"border-bottom: 0px;\">";
            echo "<table cellpadding=3 cellspacing=0 style=\"border: 0px\">";
        }
		//DISPLAY HEADROW
		echo "<tr>";
			echo "<td style=\"border-bottom: 0px\" align=center><input type=checkbox "; if($field[$row['field']][1]=="Y") { echo "checked"; } echo " name=\"".$row['field']."\" value=Y></td>";
			echo "<td style=\"border-bottom: 0px; padding-left: 10px;\">".$row['display']."</td>";
		echo "</tr>";
	} }
            echo "</table>";
        echo "</td>";
    echo "</tr>";
echo "</table>";
$hend = count($head)-1;
	//check/uncheck options
	echo "<p style=\"margin-top: 3px; font-size: 6.5pt; line-height: 7pt;\">";
		echo "<span onclick=\"checks('C',$hend)\" class=\"fc shortcut\">Check All</span>";
		echo " | ";
		echo "<span onclick=\"checks('U',$hend)\" class=\"fc shortcut\">Uncheck All</span>";
		echo " | ";
		echo "<span onclick=\"checks('I',$hend)\" class=\"fc shortcut\">Invert</span>";
	echo "</p>";
echo "</div>";


/***** FILTERS *****/
echo "<h3>".$s.". Select the filter you wish to apply:</h3>"; $s++;
echo "<div style=\"margin-left: 17px\">";
echo "<table cellspacing=0 cellpadding=3>";
/*** CALL ***/
foreach($riskheadings as $h) {
	$hf = $h['field'];
	//include("inc_tr.php");	//change background-colour on hover
	echo("<tr id=tr".$row_count." onmouseover=\"hovCSS('tr".$row_count."');\" onmouseout=\"hovCSS2('tr".$row_count."');\">"); $row_count++;
	if(!in_array($hf,$nofilter)) {
		$ht = $h['type'];
		echo "<td class=\"filter head\">".$h['display'].":</td>";
		echo "<td class=filter>";
		switch($ht) {
			case "Y": case "L":
				$table_field = $h['table_field'];
				//get list items
				$list = array();
					if($hf=="risk_owner") {
						$loggedby = array();
						$sql = "SELECT DISTINCT risk_owner FROM ".$dbref."_risk_register";
						include("generate_db.php");
							while($row = mysql_fetch_assoc($rs)) {
								if(strlen($row['risk_owner'])>0) {
									$loggedby[] = $row['risk_owner'];
								}
							}
						mysql_close($con);
						$sql = "SELECT tkid id, CONCAT_WS(' ',tkname,tksurname) as value FROM assist_".$cmpcode."_timekeep WHERE tkid IN ('".strFn("implode",$loggedby,"','","")."') ORDER BY tkname, tksurname";
					} elseif($hf=="category") {
//						$sql = "SELECT * FROM ".$dbref."_".$h['table']." WHERE active = true ORDER BY ".$table_field;
						$sql = "SELECT c.id, CONCAT_WS(' - ',t.name,c.name) as name FROM ".$dbref."_categories c INNER JOIN ".$dbref."_types t ON c.type_id = t.id AND t.active = true WHERE c.active = true ORDER BY t.name, c.name";
					} else {
						$sql = "SELECT * FROM ".$dbref."_".$h['table']." WHERE 1 ORDER BY ".$table_field;
					}
					include("generate_db.php");
						while($row = mysql_fetch_assoc($rs)) {
								$l = array('id'=>$row['id'],'value'=>$row[$table_field]);
								$list[] = $l;
						}
					mysql_close($con);
				//Display choices
				filterSelect($hf,$filter[$hf],$list);
				break;
			case "D":
				filterDate($hf,$filter[$hf]);
				break;
			default:
				filterText($hf,$filter[$hf]);
		}
		echo "</td>";
	}
	echo "</tr>";
}
echo "</table>";
echo "</div>";


/***** GROUP & SORT *****/
echo "<h3>".$s.". Choose your group and sort options:</h3>"; $s++;
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<td style=\"font-weight: bold;\">Group by:&nbsp;</td>";
			echo "<td><select name=groupby><option "; if($group=="X" || strlen($group)==0) { echo "selected"; } echo " value=\"X\">No grouping</option>";
			foreach($head as $h) {
				if(!in_array($h['field'],$nogroup) && !in_array($h['field'],$nosort)) {
					echo "<option "; if($group==$h['field']) { echo "selected"; } echo " value=\"".$h['field']."\">".$h['display']."</option>";
				}
			}
			echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td style=\"font-weight: bold; vertical-align: top\">Sort by:&nbsp;</td>";
			echo "<td>"; echo "<div class=demo><ul id=sortable>";
			foreach($sort as $hf) {
				echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$hf."\">".$head[$hf]['display']."</li>";
			}
			echo "</ul></div></td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";



/***** OUTPUT FORMAT *****/
echo "<h3>".$s.". Choose the document format of your report:</h3>"; $s++;
if(strlen($output)==0) { $output = "display"; }
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<td width=30 class=\"b-w\" align=center><input type=radio name=\"output\" value=\"display\" "; if($output=="display") { echo "checked"; } echo " id=csvn></td>";
			echo "<td class=\"b-w\"><label for=csvn>Onscreen display</label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class=\"b-w\" align=center><input type=radio name=\"output\" value=\"csv\" "; if($output=="csv") { echo "checked"; } echo " id=csvy></td>";
			echo "<td class=\"b-w\"><label for=csvy>Microsoft Excel (Plain Text)</label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class=\"tdgeneral b-w\" align=center><input type=radio name=\"output\" value=\"excel\" "; if($output=="excel") { echo "checked"; } echo " id=excely></td>";
			echo "<td class=\"tdgeneral b-w\"><label for=excely>Microsoft Excel (Formatted)*</label>";
			echo "</td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";




/***** BUTTONS *****/
echo "<h3>".$s.". Generate the report:</h3>"; $s++;
	echo "<p style=\"margin-left: 17px\">";
	echo "<b>Report Title:</b> <input type=text name=rhead value=\"".decode($report['rhead'])."\" maxlength=100 size=70> <i><small>(Displays at the top of the report.)</small></i><br />";
	echo "<input type=hidden name=act value=VIEW>";
	echo "<input type=hidden name=src value=generator>";
	echo "<input type=hidden name=i value=\"".$report['id']."\">";
	echo "<input type=submit value=\"Generate Report\" name=\"B1\">&nbsp;<input type=reset value=Reset>";
	echo "</p><p style=\"margin-left: 17px\">";
	echo "<b>Report Name:</b> <input type=text name=rname value=\"".decode($report['rname'])."\" maxlength=50 size=30> <i><small>(To identify the report in the quick report list.)</small></i><br />";
if(checkIntRef($report['id'])) {
		echo "<input type=button value=\"Save Changes\" onclick=\"saveReport('E')\">&nbsp;";
		echo "<input type=button value=\"Save As New Report\" onclick=\"saveReport('N')\">&nbsp;";
		echo "<input type=button value=\"Delete Report\" onclick=\"saveReport('D')\">&nbsp;";
} else {
	echo "<input type=button value=\"Save Report\" onclick=\"saveReport('A')\">&nbsp;";
}
	echo "</p>";
	
	
	
	
?>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel. 
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
</form>
</html></body>