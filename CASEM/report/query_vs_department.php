<?php
$scripts = array(  'menu.js' );
$styles = array();
$page_title = "Report";
require_once("../inc/header.php");
?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<script>
	$(function(){
		$.post("controller.php?action=queryVSdepartment", function( response ){
			var chart;
			
			chart = new AmCharts.AmSerialChart();		
			chart.dataProvider = response;
			chart.categoryField = "department";
			chart.color = "#000000";
			chart.fontSize = 9;
			chart.marginTop = 35;
			chart.marginBottom = 40;
			chart.marginRight = 80;
			chart.angle = 45;
			chart.depth3D = 15;
			chart.plotAreaFillAlphas= 0.9;
			chart.plotAreaBorderAlpha = 0.2;
			
			var graph1 = new AmCharts.AmGraph();
			graph1.title = "Department";
			graph1.valueField = "queries";
			graph1.type = "column";
			graph1.lineAlpha = 0;
			graph1.lineColor = "#FF6600";
			graph1.fillAlphas = 1;
			graph1.balloonText="Query : [[value]]";		
			chart.addGraph(graph1);

			var valAxis = new AmCharts.ValueAxis();
			valAxis.gridAlpha = 0.1;
			valAxis.axisAlpha = 0;
			valAxis.minimum = 0;
			valAxis.fontSize=8;
			chart.addValueAxis(valAxis);
			
			var catAxis = chart.categoryAxis;
			 chart.categoryAxis.gridAlpha = 0.1;
			 chart.categoryAxis.axisAlpha = 0;
			 chart.categoryAxis.gridPosition = "start";			
			 chart.write("chartdiv");
			
		},"json");	
	});
</script>
<table width="100%" class="noborder">
  <tr>
   <td align="center" class="noborder">
   		<center>
   		<h1>
		  	<?php 
		  		echo $_SESSION['cn'];
		  	?>
  		</h1>
  		</center>
  	</td>
  </tr>
  <tr>
    <td colspan="2" class="noborder" align="center">
    	<center><div id="chartdiv" style="width: 70%; height: 300px;"></div></center>
    </td>
  </tr>
  <tr>
  	<td colspan="2" class="noborder">
  		<center>
 			<?php echo "<font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>"; ?>
 		</center>
  	</td>
  </tr>
  <tr>
    <td class="noborder"><?php displayGoBack("",""); ?></td>
    <td class="noborder"></td>
  </tr>
</table>

