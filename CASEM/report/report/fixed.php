<?php
	$scripts = array( 'quick.js','menu.js',  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Quick Report";
	require_once("../inc/header.php");
?>
<div>
	<div id="quick_report_message"></div>
    <table border="1" id="hideOnshow">
    	<tr>
        	<th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Report Format</th>
            <th>Action</th>
        </tr>
        <tr>
        	<td>1</td>
            <td>Query Register</td>
			<td>The Query Register details the current query as defined by the Query Owner(s)</td>
            <td>On Screen</td>            
            <td><input type="button" id="show_riskregister" value=" Generate "  /></td>
        </tr>
       
    </table>
</div>
<div id="register" style="display:none;">
<h2 style="color:#990000;">Risk Register report for <?php echo ucwords($_SESSION['cc'])." on ".date('d')." ".date('F')." ".date('Y'); ?> </h2> 
<table align="left" id="risk_table" border="1" >
<tr>
    <td  id="paging" colspan="15" align="left">
        <table id="paging_risk" border="2" width="100%" cellpadding="5" cellspacing="0">
        </table>              
    </td>    
</tr>        
</table>
</div>