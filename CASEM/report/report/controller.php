<?php
@session_start();
include_once("../inc/init.php");


switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {
	case "getRisk":
		$type = new Risk( "", "", "", "" ,"", "", "", "", "", "", "", "", "", "");
		echo json_encode( $type-> getAllRisk( (isset($_REQUEST['start']) ?  $_REQUEST['start'] : ""), (isset($_REQUEST['limit']) ? $_REQUEST['limit'] : "" ) ) );	
		break;					
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;		
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;	
		
	case "generateReport":
		$rep 		= new Report();
		$dbNames 	= $rep  -> getHeaders();
		$headers 	= array(); 
		$where 		= "";
		$fields		= "";
		$from 		= "";
		$groupby 	= "";
		$riskAction = "";
		if( is_array( $_REQUEST['headers'] ) ){
			foreach( $_REQUEST['headers'] as $key => $headerValue ) {
				//get the heading names to display in the report	
			/*	echo "How about this ".$dbNames[$headerValue['name']]."\r\n\n";*/
	
				foreach( $dbNames as $ref => $value ) {
					//echo $headerValue['name']."\r\n\n";
					if( $headerValue['name'] == $ref ) {
						$headers[] = $value;
					}		
				} 
				//echo $headerValue['name']."\r\n";
				//$headers[] = $rep -> getHeaders( $headerValue['name'] );
				
				$fields .= $rep -> getField( $headerValue['name'] );
				$from 	.= $rep -> getTableFrom( $headerValue['name'] );
			}
		} 
		
		
		$where  = $rep -> getWhere( $_REQUEST['values'] );
				
		//create the group by if it has been chosen
		if( isset( $_REQUEST['grouping']) ) {
			$groupby = $rep -> getGrouping( rtrim( $_REQUEST['grouping'][0]['value'],"_") );
		}
		if($where !== 1 && !empty($_REQUEST['includeaction']) && empty($_REQUEST['noAction']) ) {
			$riskAction = "LEFT JOIN ".$_SESSION['dbref']."_actions RA ON RA.risk_id = RR.id";
		}
		$from 		  = $_SESSION['dbref']."_risk_register RR \r\n".$riskAction." ".$from;

		$response = $rep -> generateReport( rtrim(trim($fields),",") ,$where , $from ,$groupby );
		echo json_encode( array("headers" => $headers, "data" => $response) );
		break;
	case "_generateReport":
		$mainArray = array();
		$headers   = array();
		foreach( $_REQUEST['headers'] as $key => $nameValue ) {
		 foreach($_REQUEST['values'] as $val => $valuesArray ) {	 
		 	// if its a clicked check box ,, the get its value entered	
			if( ltrim($valuesArray['name'],"_") ==  $nameValue['name'] ) {
				
				$mainArray[$nameValue['name']] = array(	
															"value" 		=> $valuesArray['value'],
															"grouping"		=> "",
															"sortable"		=> "",
															"tablePrefix"	=> ""
													  );
				// if there is grouping , add the grouping key to the main array
				foreach( $_REQUEST['grouping'] as $key => $groupValue ) {	
					if( rtrim($groupValue['value'],"_") == $nameValue['name'] )	{
						$mainArray[$nameValue['name']]["grouping"] =  "Yes";
					}
					if( ltrim( $groupValue['value'],"__") == $nameValue['name'] ) {
						$mainArray[$nameValue['name']]["sortable"] =  "Yes";
					}
			   }
				// add table and the table prefix	
				if(array_key_exists( $nameValue['name'], $fieldTable ) && isset( $fieldTable[$nameValue['name']]['table'] ) ) {
					$headers[] = $nameValue['name'] ;
					$mainArray[$nameValue['name']]["searchArray"] =  (isset($fieldTable[$nameValue['name']]['searchField']) ? $fieldTable[$nameValue['name']]['searchField']  : "");
					
					if( isset($fieldTable[$nameValue['name']]['as']) && !empty($fieldTable[$nameValue['name']]['as']) ) {
					 $mainArray[$nameValue['name']]["AS"] =   $fieldTable[$nameValue['name']]['as'];	
					}
					$mainArray[$nameValue['name']]["returnField"] = $fieldTable[$nameValue['name']]['name'];
					$mainArray[$nameValue['name']]["table"]		  =  $fieldTable[$nameValue['name']]['table'];
					$mainArray[$nameValue['name']]["tablePrefix"] =  $fieldTable[$nameValue['name']]['prefix'];	
					$mainArray[$nameValue['name']]["join"] 		  =  $fieldTable[$nameValue['name']]['joinField'];								
				}						
				}

		  }
	    }
		$rep 		= new Report();
		$response 	= $rep -> generateReport( $mainArray );
		echo json_encode( $response );
		break;
		case "residualVsInherent":
		//$rep 		= new Report();
		//echo json_encode( $rep -> residualVsInherent() );
		break;	
		}
?>
