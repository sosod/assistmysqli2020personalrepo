<?php
$scripts 	= array( 'add_action.js','menu.js',  );
$styles 	= array( 'colorpicker.css',   );
$page_title = "Action Edit";
require_once("../inc/header.php");
$stat 		= new ActionStatus("", "", "");
$statuses 	=  $stat -> getStatuses();
$update 	= new RiskAction("", "", "", "", "", "", "", "", "");
$action 	= $update ->getRawAction($_REQUEST['id']);
$latestUp 	= $update -> getLatestActionUpdates( $_REQUEST['id'] );
?>
<script>
$(function(){
	$("table#update").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="update-action-form" name="update-action-form">
	<table align="left" border="1" id="update" width="90%" class="noborder">
    	<tr>
        	<td valign="top" align="left" width="50%" class="noborder">
            	<table width="70%">
            		<tr>
            			<td colspan="2"><h4>Action Details</h4></td>
            		</tr>
            		<tr id="goback">
				       	<td align="left" class="noborder">
				        	<?php displayGoBack("",""); ?>
				        </td>
				        <td class="noborder">
				        	<?php displayAuditLogLink( "actions_update" , false); ?>
				        </td>
            		</tr>
                </table>
            </td>
            <td valign="top" align="right" width="70%" class="noborder">
            	<table id="new_update" width="100%">
                	<tr>
                    	<td colspan="2"><h4>New Update</h4></td>
                    </tr>
                    <tr>
                    	<th>Description:</th>
                        <td><textarea id="update_description" name="update_description" cols="35" rows="7"><?php //echo $latestUp['description']; ?></textarea></td>
                    </tr>
                    <tr>
                    	<th>Status:</th>
                        <td>
                        <select id="update_status" name="update_status">
                        	<option>--status--</option>
                            <?php
                            foreach( $statuses as $status ) {
							?>
                            	<option value="<?php echo $status['id']; ?>"
                                 <?php if($status['name']=="New" || $status['id'] == 1) { ?>
                                 	disabled="disabled" 
                               <?php } if(($status['id'] == $latestUp['status']) || ($status['name'] == "New") ) { ?>
                                selected="selected" 
								<?php } ?>>
								<?php echo $status['name']; ?>
                               </option>  
                             <?php
							}
                            ?>
                        </select>
                        </td>
                    </tr>
                    <tr>
                    	<th>Progress:</th>
                        <td>
                        	<input type="text" name="update_progress" id="update_progress" value="<?php echo $latestUp['progress']; ?>" />
                            <em style="color:#FF0000">%</em>
                        </td>
                    </tr>
                    <tr>
                    	<th>Remind On:</th>
                        <td><input type="text" name="update_remindon" id="update_remindon" value="<?php echo ($latestUp['remindon'] == "0000-00-00" ? "" : $latestUp['remindon']); ?>"  class="datepicker" /></td>
                    </tr>
                    <tr>
                    	<th>Attachment:</th>
                        <td>
                        <input type="file" name="update_attachment" id="update_attachment"  />
                       <!-- <input type="submit" name="attach_another" id="attach_another" value="Add Another" />-->
                        </td>
                    </tr>
                    <tr>
                    	<th></th>
                        <td>
                        	<input type="checkbox" name="update_approval" id="update_approval" 
							<?php if($latestUp['progress'] !== "100") { ?> disabled="disabled" <?php } ?>  />
                            Request approval that the risk has been addressed
                        </td>
                    </tr>
                    <tr>
                    	<th></th>
                        <td>
                        	<input type="submit" name="save_action_update" id="save_action_update" value="Save" class="isubmit"  />
                            <input type="submit" name="cancel_action_update" id="cancel_action_update" value="Cancel" class="idelete"  />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
</form>
</div>
<div id="view_action_updates" style="clear:both;"></div>