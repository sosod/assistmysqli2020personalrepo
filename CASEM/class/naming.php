<?php
/**
	* Naming conversion for the module 
	* @author 	: admire<azinamo@gmail.com>
	* copyright	: 2011 Ignite Assist , risk module

**/
class Naming extends DBConnect
{
	/**
		@varchar 
	**/
	protected $item;
	/**
		@varchar 
	**/
	protected $type;
	/**
		@varchar 
	**/
	protected $category;
	/**
		@varchar 
	**/	
	protected $description;
	/**
		@varchar 
	**/	
	protected $background;
	/**
		@varchar 
	**/	
	protected $impact;
	/**
		@varchar 
	**/	
	protected $impact_rating;
	/**
		@varchar 
	**/	
	protected $likelihood;
	/**
		@varchar 
	**/	
	protected $likelihood_rating;
	/**
		@varchar 
	**/	
	protected $inherent_risk_exposure;
	/**
		@varchar 
	**/	
	protected $inherent_risk_rating;
	/**
		@varchar 
	**/	
	protected $current_controls;
	/**
		@varchar 
	**/	
	protected $percieved_control_effectiveness;
	/**
		@varchar 
	**/	
	protected $control_effectiveness_rating;
	/**
		@varchar 
	**/	
	protected $residual_risk;
	/**
		@varchar 
	**/	
	protected $residual_risk_exposure;
	/**
		@varchar 
	**/	
	protected $risk_owner;
	/**
		@varchar 
	**/	
	protected $actions_to_improve;
	/**
		@varchar 
	**/	
	protected $action_owner;
	/**
		@varchar 
	**/	
	protected $time_scale;
	/**
		@text 
	**/	
	protected $status;
	/**
		@text 
	**/	
	protected $udf;
	/**
		@varchar 
	**/	
	protected $company_id;
	/**
		@varchar 
	**/	
	protected $optionSql;
	

	function __construct($section = "")
	{
	    if($section == "new")
	    {
		  $this -> optionSql  = " AND active & 8 = 8 ";
	    } else if($section == "manage") {
		   $this -> optionSql = " AND active & 4 = 4 ";
	    }	    
		//$db 	= new DBConnect();
		//echo $db -> databaseRef;
		parent::__construct(); 
	}
	/**
		@boolean
	**/
	function _checkCompany( $id  )
	{
		$response = $this -> getWhere( "naming_conversion", "company_id=$id");
		if( empty($response) ) {
			return false;
		} else {
			return true;
		}
	}
	
	function getHeaderList($options = "")
	{
	   $namingColumns  = $this -> getNaming($options);
	   $cols 		   = array();	
	   foreach($namingColumns as $index => $column)
	   {
		 $cols[$column['name']] = $column['client_terminology'];			
	   }
	  return $cols;
	}
	
	function saveNaming( $field, $value ,$company_id = NULL )
	{
		$insertdata = array(
						$field 		 => $value,
						'insertuser' => $_SESSION['tid'],
		);
		$response = $this -> update( 'naming_conversion', $insertdata ,"id=1");	

		echo $response; 
	}
	
	function updateHeaderNames( $field, $value  )
	{
		$insertdata = array("client_terminology" => $value, "insertuser" => $_SESSION['tid']);
        $data     =  $this -> getAHeader($field);
        $response = 0;
        if($data['client_terminology'] != $value)
        {
            $logsObj = new Logs();
            $_POST['id'] = $data['id'];
            $_POST['client_terminology'] = $value;
            unset($_POST['value']);
            $logsObj -> setParameters($_POST, $data, "header_names");
		    $response = $this -> update('header_names', $insertdata ,"name='".$field."'");	
        }
		return $response; 
	}
	
	function getNaming()
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_header_names " );
		return $response;
	}

	function getHeaderNames()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type = 'query' " );
		return $response;
	}

    function getActionColums()
    {
        $results = $this->get("SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type <> 'query'");
        $rows    = array();
        foreach( $results  as $key => $value)
        {
            $rows[$value['name']] = ($value['client_terminology'] == "" ? $value['ignite_terminology'] : $value['client_terminology']);
        }
        return $rows;
    }


	function getNamingConversion()
	{
		$response = $this -> getNameColumn();
		$headRes = array();
		foreach($response as $key => $headerArray){
		  $headRes[$headerArray['name']] = ($headerArray['client_terminology'] == "" ?  $headerArray['ignite_terminology'] : $headerArray['client_terminology'] );
		}
		return  $headRes ;
	}
	
	function nameLabel( $fieldId, $defaultName)
	{
		$namingConversion =  $this-> getNamingConversion();
		if( isset($namingConversion[$fieldId]) && !(empty($namingConversion[$fieldId])) ) {
			return $namingConversion[$fieldId];
		} else {
			return $defaultName;
		}
	}
    
	function setHeader( $key )
	{
		$headerName = "";
		foreach($this->getHeaderNames() as $index => $value){
			if( $key == $value['name'] ){
				$headerName = (isset($value['client_terminology']) ? $value['ignite_terminology'] : "");
			} else {
				$headerName = ucwords( str_replace("_", " ", $key));
			}
		}
		return $headerName;
	}	
	function rowLabel()
	{
		$namingConversion =  $this-> getNameColumn();
		$keyLabel		  = array(); 
		foreach( $namingConversion  as $key => $valArr){
			$keyLabel[$valArr['name']] = ($valArr['client_terminology'] == "" ? $valArr['ignite_terminology'] : $valArr['client_terminology'] ); 
		}
		return $keyLabel;
	}
	
	function getNameColumn()
	{
		$response = $this -> get("SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE active <> 0 AND type = 'query' ORDER BY ordernumber");
		return $response;
	}

	function getAHeader( $id )
	{
		$response = $this -> getRow("SELECT id, name AS fieldname, client_terminology FROM ".$_SESSION['dbref']."_header_names WHERE name = '".$id."'");

		return $response;
	}
	
	function sortedHeaders()
	{
		$response = $this->get("SELECT * FROM ".$_SESSION['dbref']."_header_names");
		foreach( $response  as $key => $valArr)
		{
			$keyLabel[$valArr['name']] = ($valArr['client_terminology'] == "" ? $valArr['ignite_terminology'] : $valArr['client_terminology'] ); 
		}
		return $keyLabel;		
	}
	
	function getReportingNaming($type="")
	{
		if($type == "action") {
			$type = "type <> 'query'";
		} else {
			$type = "type = '".$type."'";
		}
		$response = $this -> get("SELECT name AS id, if(client_terminology<>'',client_terminology,ignite_terminology) as name FROM ".$_SESSION['dbref']."_header_names WHERE $type ORDER BY ordernumber");
		$headRes = array();
		foreach($response as $key => $headerArray){
		  $headRes[$headerArray['id']] = $headerArray['name'];
		}
		return  $headRes ;
	}	
}
?>