<?php
/**
	* Risk category
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/	
class RiskCategory extends DBConnect
{
	
	protected $category;
	
	protected $shortcode;
	
	protected $description;
	
	protected $type;
	
	function __construct( $category, $shortcode, $description, $type)
	{
		$this -> category 	 = trim($category);
		$this -> shortcode 	 = trim($shortcode);
		$this -> description = trim($description);
		$this -> type 		 = trim($type);		
		parent::__construct();
	}

	function getCategory()
	{
		$response = $this -> get( "SELECT RC.id,RC.shortcode, RC.active,  RC.name, RC.description as descr, RT.name as risktype
								   FROM ".$_SESSION['dbref']."_categories RC 
								   LEFT JOIN ".$_SESSION['dbref']."_types RT 
								   ON RT.id = RC.type_id WHERE RC.active & 2 <> 2
								 " );
		return $response;
	}
	
	function getTypeCategory( $id )
	{
		$response = $this -> get( "SELECT RC.id,RC.shortcode, RC.active,  RC.name, RC.description as descr, RT.name as risktype
								   FROM ".$_SESSION['dbref']."_categories RC 
								   LEFT JOIN ".$_SESSION['dbref']."_types RT 
								   ON RT.id = RC.type_id WHERE RC.type_id = '".$id."' 
								 " );
		return $response;
	}	
	
	function getACategory( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_categories RC WHERE RC.id = $id " );
		return $response;
	}

	function saveCategory()
	{
		$insert_data = array( 
					"name" 		  => $this -> category,
					"shortcode"   => $this -> shortcode,
					"description" => $this -> description,
					"type_id" 	  => $this -> type ,
					"insertuser"  => $_SESSION['tid'],									
					);
		$response = $this -> insert( "categories" , $insert_data );
		echo $this -> insertedId() ;		
	}
	
	function updateCategory( $id )
	{
		$update_data = array( 
					"name" 		  =>  $this -> category ,
					"shortcode"   =>  $this -> shortcode ,
					"description" => $this -> description,
					"type_id" 	  =>  $this -> type ,
					"insertuser"  => $_SESSION['tid'],										
					);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $logsObj -> setParameters( $_POST, $data, "categories");
		$response = $this -> update( "categories" , $update_data, "id=$id"  );
		return $response;		
	}
	
	
	function deactivateCategory( $id, $status )
	{
		$updatedata = array(
					'active' 	  => $status,
					"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $logsObj -> setParameters( $_POST, $data, "categories");
		$response = $this -> update( 'categories', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateCategory( $id )
	{
		$update_data = array(
					'active' 	  => "1",
					"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $logsObj -> setParameters( $_POST, $data, "categories");
		$response = $this -> update( 'categories', $update_data, "id=$id" );
		echo $response;
	}
	
	function getReportList()
	{
		$response = $this -> get( "SELECT RC.id, RC.name
								   FROM ".$_SESSION['dbref']."_categories RC 
								   LEFT JOIN ".$_SESSION['dbref']."_query_register Q
								   ON Q.category = RC.id WHERE RC.active & 2 <> 2
								 " );
		return $response;
	}		
}
?>