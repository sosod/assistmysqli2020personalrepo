<?php
/**
	* Risk types for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class QueryType extends DBConnect
{
	
	protected $shortcode;
	
	protected $type;
	
	protected $description;
	
	protected $name;
	
	
	function __construct( $shortcode, $name, $description, $type)
	{
		$this -> shortcode 	 = trim($shortcode);
		$this -> name 		 = trim($name);
		$this -> description = trim($description);
		$this -> type        = trim($type);		
		parent::__construct();
	}
	
	function saveType()
	{
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"insertuser"  => $_SESSION['tid'],
						);
		$response = $this -> insert( "types" , $insert_data );
		echo $this -> insertedId();		
	}
	
	function getType()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE active & 2 <> 2" );
		return $response ;		
	}	
	
	function getARiskType( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE id = '".$id."'" );
		return $response ;		
	}	
	/**
		Get used risk types , so they cannot be deleted
	**/
	function getUsedTypes()
	{	
		$response = $this -> get("SELECT type FROM ".$_SESSION['dbref']."_risk_register GROUP BY type");
		return $response;
	}
	/**
		Fetches all active risk types
	**/
	function getActiveRiskType()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE active = 1" );
		return $response ;		
	}		
			
	function updateType( $id )
	{
        $logsObj = new Logs();
        $data = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "types");

		$insertdata = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> update( "types", $insertdata , "id=$id");
		return $response;
	}
	
	function changeTypeStatus( $id, $status )
	{
        $logsObj = new Logs();	
        $data = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "types");
		$updatedata = array(
						'active' 	  => $status,
						"insertuser"  => $_SESSION['tid'],						
		);
		$response = $this -> update( 'types', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateType( $id )
	{
        $logsObj = new Logs();
        $data = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "types");
		$update_data = array(
						'active' 	  => "1",
						"insertuser"  => $_SESSION['tid'],						
		);
		$response = $this -> update( 'types', $update_data, "id=$id" );
		echo $response;
	}

	function getReportList() {
		$response = $this -> get( "SELECT t.id, t.name FROM ".$_SESSION['dbref']."_types t INNER JOIN ".$_SESSION['dbref']."_query_register q ON q.type = t.id WHERE t.active & 2 <> 2" );
		return $response ;		
	}
}
?>