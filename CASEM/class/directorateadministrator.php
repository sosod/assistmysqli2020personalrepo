<?php
//require_once "../../library/class/assist_dbconn.php";
//require_once "../../library/class/assist_db.php";
//require_once "../../library/class/assist_helper.php";
if(is_dir("../../library/class")) {
	require_once "../../library/class/assist_dbconn.php";
	require_once "../../library/class/assist_db.php";
	require_once "../../library/class/assist_helper.php";
} elseif(is_dir("../library/class")) {
	require_once "../library/class/assist_dbconn.php";
	require_once "../library/class/assist_db.php";
	require_once "../library/class/assist_helper.php";
}
class DirectorateAdministrator {

	private $user_id;
	private $status;
	private $dir_ref;
	private $dir_type;
	private $access;

	//administrator is inactive
	const INACTIVE = 2;
	//administrator is active and can act on department
	const ACTIVE = 4;
	//administrator can update query in department
	const UPDATE = 8;
	//administrator can edit query in department
	const EDIT = 16;
	//administrator can approve actions assigned to query in department
	const APPROVE = 32;
	//administrator can delete actions/query in department
	const DELETE = 64;
	//administrator can add actions to any object in department
	const ADD_ACTION = 128;
	//administrator can add objects to department
	const ADD_OBJECT = 256;
	
	
	public function __construct($a=array(),$u="",$s="",$r=0,$t="") {
		$this->user_id = isset($a['user_id']) ? $a['user_id'] : $u;
		$this->status = isset($a['active']) ? $a['active'] : $s;
		$this->dir_ref = isset($a['ref']) ? $a['ref'] : $r;
		$this->dir_type = isset($a['type']) ? $a['type'] : $t;
		$this->status *=1;
		if(strlen($this->status)>0) {
			$this->access = $this->getAccess();
		}
	}
	
	static function get_class_constants() {
		$reflect = new ReflectionClass(get_class());
		return $reflect->getConstants();
    }
	
	public function getAccess($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$access = array(
			'update' => $this->canUpdate($status),
			'edit'=>$this->canEdit($status),
			'approve'=>$this->canApprove($status),
			'delete'=>$this->canDelete($status),
			'addaction'=>$this->canAddAction($status),
			'addobject'=>$this->canAddObject($status),
		);
		return $access;
	}
	
	public function canUpdate($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$evaluate = ($status & self::UPDATE);
		if($evaluate == self::UPDATE) {
			return true;
		}
		return false;
	}
	public function canEdit($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$evaluate = ($status & self::EDIT);
		if($evaluate == self::EDIT) {
			return true;
		}
		return false;
	}
	public function canDelete($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$evaluate = ($status & self::DELETE);
		if($evaluate == self::DELETE) {
			return true;
		}
		return false;
	}
	public function canApprove($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$evaluate = ($status & self::APPROVE);
		if($evaluate == self::APPROVE) {
			return true;
		}
		return false;
	}
	public function canAddAction($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$evaluate = ($status & self::ADD_ACTION);
		if($evaluate == self::ADD_ACTION) {
			return true;
		}
		return false;
	}
	public function canAddObject($status="") {
		if(strlen($status)==0) { $status = $this->status; }
		$evaluate = ($status & self::ADD_OBJECT);
		if($evaluate == self::ADD_OBJECT) {
			return true;
		}
		return false;
	}
	
	
}

?>