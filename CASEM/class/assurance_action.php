<?php 
$scripts = array( 'assurance_action.js', 'jquery.ui.actionassurance.js', 'menu.js',  );
$styles = array( );
$page_title = "Action Assurance";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#actionassurance").actionassurance({action_id:$("#action_id").val()});
	});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="assurance-action-form" name="assurance-action-form" enctype="multipart/form-data">
	<table id="assurance_action_table" class="noborder">
    	<tr>
        	<td valign="top" class="noborder" width="40%">
            	<table align="left" width="100%">
                    <tr>
                        <td colspan="2" class="noborder"><h4>Action Information</h4></td>
                    </tr>  
					<tr id="goback">
						<td class="noborder"><?php displayGoBack("", ""); ?></td>
						<td class="noborder"></td>
					</tr>                        
                </table>
            </td>
            <td valign="top" class="noborder">
            <h4>Action Assurances</h4>
            <div id="actionassurance"></div>
                <table style="display:none;">
                	<tr>
                		<td colspan="2"><h4>Action Assurance</h4></td>
                	</tr>
                    <tr>
                        <th>Response</th>
                        <td><textarea name="assurance_response" id="assurance_response" ></textarea></td>            
                    </tr>
                    <tr>
                        <th>Date Tested:</th>
                        <td><input type="text" class="datepicker" name="date_tested" id="date_tested" ></td>            
                    </tr>        
                    <tr>
                        <th>Sign-Off</th>
                        <td>
                            <select name="assurance_signoff" id="assurance_signoff">
                                <option value="1">Yes</option>
                                <option value="0">No</option>                    
                            </select>
                        </td>            
                    </tr>           
                    <tr>
                        <th>Attachment</th>
                        <td>
                        <input type="file" name="assurance_attachment" id="assurance_attachment" />
                        <input type="submit" name="attach_another" id="attach_another" value="Attach Another"  />
                         </td>            
                    </tr>   
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="add_newAction_assurance" id="add_newAction_assurance" value="Save" class="isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="cancel_newAction_assurance" id="cancel_newAction_assurance" value="Cancel" class="idelete" />
                        </td>            
                    </tr>                                       	                
                </table>                
            </td>
        </tr>
    </table>
    <input type="hidden" id="action_id" name="action_id" value="<?php echo $_REQUEST['id']; ?>" />
</form>
</div>


