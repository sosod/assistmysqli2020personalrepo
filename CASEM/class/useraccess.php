<?php
/**
	* @package 	: User Access
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class UserAccess extends DBConnect
{
	
	protected $user;
	
	protected $module_admin;
	
	protected $create_risks;
	
	protected $create_actions;
	
	protected $view_all;
	
	protected $edit_all;
	
	protected $reports;
	
	protected $assurance;
	
	protected $setup;
	
	function __construct( $user, $module_admin, $create_risks, $create_actions,  $view_all, $edit_all, $reports, $assurance, $setup )
	{
	
		$this -> user			= trim( $user );
		$this -> module_admin   = trim( $module_admin );
		$this -> create_risks   = trim( $create_risks );
		$this -> create_actions = trim( $create_actions );
		$this -> view_all 	    = trim( $view_all );
		$this -> edit_all       = trim( $edit_all );
		$this -> reports        = trim( $reports );		
		$this -> assurance 		= trim( $assurance );
		$this -> setup 		    = trim( $setup );								
		parent::__construct();
	}
	
	function saveUserAccess()
	{

		$insert_data = array( 
						"user" 			=> $this -> user,
						"module_admin"  => $this -> module_admin,
						"create_risks"  => $this -> create_risks,
						"create_actions"=> $this -> create_actions,
						"view_all"      => $this -> view_all,
						"edit_all" 		=> $this -> edit_all,
						"report"   		=> $this -> reports,
						"assurance"  	=> $this -> assurance,
						"setup"  		=> $this -> setup,
						"insertuser"    => $_SESSION['tid'],											
						);
		$response = $this -> insert( "useraccess" , $insert_data );
		echo $this -> insertedId();		
	}
	
	function updateUserAccess()
	{
		$updatedata = array( 
						"module_admin"  => $this -> module_admin,
						"create_risks"  => $this -> create_risks,
						"create_actions"=> $this -> create_actions,
						"view_all"      => $this -> view_all,
						"edit_all" 		=> $this -> edit_all,
						"report"   		=> $this -> reports,
						"assurance"  	=> $this -> assurance,
						"setup"  		=> $this -> setup,
						"insertuser"    => $_SESSION['tid'],											
						);
		$response = $this -> update( "useraccess" , $updatedata, "id=".$this -> user );
		return $response;	
	}

	function getUser($id)
	{
		$response = $this -> getRow( "
									SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.user = $id
								  ");
		return $response;
	}	
	
	function getUserAccessInfo( $id )
	{	
		$response = $this -> getRow( "
									SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.id = $id
								  ");
		return $response;
	}
	
	function getUserAccess()
	{	
		$response = $this -> get( "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
								   " );
		return $response;
	}
	
	
	function getAUser()
	{
		$response = $this -> get( "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
								   " );
		return $response;
	}
		
	function getUsers()
	{							  							    
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail,
		                           CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
								   ORDER BY TK.tkname, TK.tksurname" );
		return $response;
	}
	
	function getUsersList()
	{
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail,
		                           CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
								   ORDER BY TK.tkname, TK.tksurname" );
		$results   = array();	
		foreach($response as $index => $res)
		{
		   $results[$res['tkid']] = $res;
		}
	  return $results;	    
	}
	
	function getRiskManager()
	{							  							    
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   INNER JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user = TK.tkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."' AND UA.risk_manager = 1" );
		return $response;
	}

	function getARiskManager( $id )
	{
		$response  = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_useraccess WHERE user = '".$id."'" );
		return $response; 
	}

	function getRiskManagerEmails()
	{							  						    
		$response = $this -> get( "SELECT  tkemail
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   INNER JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user = TK.tkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."' AND UA.risk_manager = 1" );
		$emailTo   = "";
		if(!empty($response)){
			foreach($response as $userEmail ) {
				$emailTo = $userEmail['tkemail'].",";
			}
		}
		$emailTo = rtrim( $emailTo, ",");
		return $emailTo;
	}
	/**
		Fetched the email of the user who has been asssigned and action or risk
	**/
	function getUserResponsiblityEmail( $id ) 
	{
		$result  = $this -> getRow("SELECT tkemail, tkname, tksurname FROM assist_".$_SESSION['cc']."_timekeep 
		                             WHERE tkid = '".$id."'
		                           ");
		return $result;
	}
	
	function getUserEmailUnderDirectorate( $ref )
	{
		$response = $this->get("SELECT TK.tkemail 
								FROM assist_".$_SESSION['cc']."_timekeep TK 
								INNER JOIN  ".$_SESSION['dbref']."_dir_admins DA ON DA.tkid = TK.tkid 
								WHERE DA.ref = '".$ref."'
								"
							  );
							  
		return $response;
	}
	
	
	function setRiskManager( $id, $status )
	{
		$updatedata = array(
						'risk_manager'  => $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'useraccess', $updatedata, "user=$id" );
		echo $response;
	}
	
	function isRiskManager()
	{
		$response = $this -> getRow("SELECT risk_manager 
									 FROM ".$_SESSION['dbref']."_useraccess 
									 WHERE user = '".$_SESSION['tid']."' "
									);
		if( $response['risk_manager'] == 1 ) 
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getUserDirectorates()
	{
		$results = $this -> get("SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
								 WHERE tkid = '".$_SESSION['tid']."' AND type = 'DIR' AND active = 1 
							    ");
		$riskRefs = array();
		foreach($results as $r_index => $userRef)
		{
		    $riskRefs[] = $userRef['ref']; 
		}
	   return $riskRefs;	
	}
	
	function getUserRefDirectorate()
	{
		$response = $this -> get("SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
								   WHERE tkid = '".$_SESSION['tid']."' AND type = 'DIR' AND active = 1 
								 ");
		$riskRefs = "";
		foreach($response as $row)
		{
			$riskRefs .= "'".$row['ref']."',";
		}
		return rtrim( ltrim( rtrim($riskRefs, ","), "'") , "'");
	}
	
	function getManagerUsers()
	{
		$response = $this -> get( "SELECT TK.tkname, TK.tksurname, UA.user
								   FROM  ".$_SESSION['dbref']."_useraccess UA 
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
								   WHERE UA.insertuser = '".$_SESSION['tid']."'
								   " 
								);
		return $response;
	}
	
}
?>
