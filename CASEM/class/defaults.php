<?php
/**
	* @package 	  : Defaults
	* @author	  : admire<azinamo@gmail.com>
	* @copyright  : 2011 Ignite Assist
**/
class Defaults extends DBConnect{

	function __construct()
	{
		parent::__construct();
	}
	
	function updateDefaults( $id , $value ) 
	{
		$insert_data = array( "value" => $value );
		$response = "";
        $logsObj = new Logs();
        $data    = $this->getADefault( $id );
        $_POST['status'] = ($_POST['value'] == "no" ? "0" : "1");
        $data['status'] = ($data['value'] == "no" ? "0" : "1");
        unset($_POST['value']);
        unset($_POST['name']);
        unset($data['value']);
        unset($data['name']);
        $logsObj -> setParameters( $_POST, $data, "defaults");
		$response = $this -> update( 'defaults' , $insert_data, "id=$id" );
		echo $response;
	}

    function getADefault( $id ){
		$response = $this -> getRow("SELECT * FROM  ".$_SESSION['dbref']."_defaults WHERE id = $id");
		return $response;
    }

	function getDefaults()
	{
		$response = $this -> get("SELECT * FROM  ".$_SESSION['dbref']."_defaults");
		return $response;
	}
}
?>
