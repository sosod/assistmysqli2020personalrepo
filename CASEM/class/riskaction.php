<?php
/**
	* @package 	: Risk Action
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 ignite Assist
**/
class RiskAction extends DBConnect
{
	/**
		@int
	**/
	protected $id;
	/**
		@int
	**/
	protected $risk_id;
	/**
		@text
	**/
	protected $action;
	/**
		@int
	**/
	protected $action_owner;
	/**
		@text
	**/
	protected $deliverable;
	/**
		@text
	**/
	protected $timescale;
	/**
		@var
	**/
	protected $deadline;
	/**
		@var
	**/
	protected $progress;
	/**
		@var
	**/
	protected $attachement;
	/**
		@var
	**/
	protected $status;		
	/**
		@int
	**/
	protected $remindon;
	
	protected $_errors = array();
	
	const REQUEST_APPROVAL	= 1; 
	
	const DECLINE_APPROVAL	= 2;
	
	const ACTION_APPROVED	= 4;
	
	const AWAITING_APPROVAL = 8;
	
	const UNLOCKED			= 16;
    
    const REFTAG            = "CA";

	function __construct($risk_id, $action, $action_owner, $deliverable, $timescale, $deadline, $remindon, $progress, $status)
	{
		
		$this -> risk_id 		= $risk_id;
		$this -> action  		= $action;
		$this -> action_owner 	= $action_owner;
		$this -> deliverable 	= $deliverable;
		$this -> timescale 		= $timescale;
		$this -> deadline 		= $deadline;
		$this -> remindon 		= $remindon;
		$this -> progress 		= $progress;
		$this -> status			= $status;
		parent::__construct();
	}

    function getActionDetail($action_id)
    {
        $result = $this -> getRow("SELECT A.id, A.action, A.deliverable, A.timescale, A.deadline, A.remindon,A.action_on,
                                   A.progress, A.action_owner AS owner, A.status, RAS.name as actionstatus, A.action_status,
                                   CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner, TK.tkemail, A.attachement, A.risk_id
                                   FROM #_actions A
                                   LEFT JOIN #_action_status RAS ON RAS.id = A.status
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.action_owner
                                   WHERE A.id = '".$action_id."'
                                  ");
        return $result;
    }
    
	/**
		Edit risk action
	**/
    function editRiskAction($action_id, $update_data, $updates)
    {
       $res  = 0;
       if(!empty($update_data))
       {
          $res  = $this -> update('actions', $update_data, "id='".$action_id."'");
       }
       if(!empty($updates))
       {
          $this -> insert('action_edit', $updates);
          $res  +=  $this -> insertedId();        
       }
	   return $res;
    }
    
    /*
    Update risk action
    */
	function updateAction($action_id, $update_data, $updates)
	{
       $res  = 0;
       if(!empty($update_data))
       {
          $res  = $this -> update('actions', $update_data, "id='".$action_id."'");
       }
       if(!empty($updates))
       {
          $this -> insert('actions_update', $updates);
          $res  +=  $this -> insertedId();        
       }	   
	   return $res;
	}      

	function saveRiskAction($data = array())
	{
		$riskObj = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$risk    = $riskObj -> getARisk( $this -> risk_id );
		if($this-> _checkValidity( $risk['query_deadline_date'], $this -> deadline )){
	        if( $this-> _checkValidity($this->deadline, $this -> remindon))
	        {
	            $insert_data = array(
	                            'risk_id' 		=> $this -> risk_id,
	                            'action'  		=> $this -> action,
	                            'action_owner' 	=> $this -> action_owner,
	                            'deliverable' 	=> $this -> deliverable,
	                            'timescale' 	=> $this -> timescale,
	                            'deadline' 		=> $this -> deadline,
	                            'remindon' 		=> $this -> remindon,
	                            'progress' 		=> $this -> progress,
	                            'status' 		=> $this -> status,
	            				"attachement"	=> (isset($data['attachments']) ? $data['attachments'] : ""),
	                            'insertuser'	=> $_SESSION['tid'],
	                    );
	                $response = $this -> insert( 'actions', $insert_data );
	                return $this -> insertedId();
	        } else {
	          return  "The remind date cannot be after the deadline";
	        }
		} else {
			return  "The deadline date cannot be after the ".Risk::OBJECT_NAME." deadline date (".$risk['query_deadline_date'].")";		
		}
	}
	/**
		Creates a new risk update
	**/

    function _checkValidity( $deadline, $remindon)
    {
        if( strtotime($remindon) > strtotime($deadline))
        {
            return FALSE;
        } else {
            return TRUE;

        }
    }
    /*
        Fetch risk actions
    */
    function fetchAll($option_sql = "")
    {
 		$response = $this -> get("SELECT DISTINCT(A.id), A.action AS query_action, A.deliverable, A.timescale, A.deadline,
								  A.progress, A.remindon, A.action_status, RAS.name AS statusname,
								  A.action_owner AS owner,	CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner,
								  DP.value AS department, A.risk_id, QR.sub_id, A.id as action_ref
								  FROM #_actions A
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid = A.action_owner
								  LEFT JOIN #_action_status RAS ON A.status = RAS.id 
								  LEFT JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								  INNER JOIN #_query_register QR ON QR.id = A.risk_id
								  WHERE 1 AND A.active = 1
								  $option_sql
						         ");					
		return $response;     
    }    
    
    function getTotalActions($option_sql = "")
    {
 		$response = $this -> getRow("SELECT COUNT(DISTINCT(A.id)) AS total
								     FROM #_actions A
								     INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid = A.action_owner
								     LEFT JOIN #_action_status RAS ON A.status = RAS.id 
								     LEFT JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								     INNER JOIN #_query_register QR ON QR.id = A.risk_id
								     WHERE 1 AND A.active = 1
								     $option_sql
						         ");					
		return $response['total'];    
    }
    
	/**
		Sets the view options for each used based on the user access settings
		@param , userAccess array , pageRequest page requesting the view
		@return string, sql string
	**/
	function _setViewAccess($useraccess, $options = array()) 
	{
	    $viewType        = (isset($options['view']) ? $options['view'] : "viewMyn");
		$view_access_sql = "";
	    //$view_access_sql = " AND (DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB' ";
	    if(!empty($useraccess))
	    {
	       $sub_sql = "";
	       $page    = (isset($options['page']) ? $options['page'] : "");
           if($page == "edit_actions")
           {
             $page = "edit";
           } elseif($page == "update_actions"){
             $page = "update";
           }
	       if(isset($useraccess[$page]))
	       {
	           foreach($useraccess[$page] as $index => $val)
	           {
                  $sub_sql .= " QR.sub_id = ".$val." OR";
	           }
	           if($page == "edit")
	           {
	             $view_access_sql .= " AND (".rtrim($sub_sql, "OR").") ";
	           } else {
	               if(!empty($sub_sql))
	               {
	                  $view_access_sql .= " OR (".rtrim($sub_sql, "OR").") ";
	               }		
	           }       
	       } 
	    }

		return $view_access_sql;
	}	    
    
    function getQueryActions($options = array())
    {
        $optionSql = "";
        if(isset($options['id']) && !empty($options['id']))
        {
           $optionSql = " AND A.risk_id = '".$options['id']."' ";
        }
        $diradminObj  = new Administrator($_SESSION['tid']);
        $useraccess   = $diradminObj -> getAdminAccess();
        $user_sql     = "";
        if(isset($options['page']))
        {
            if($options['section'] != "admin")
            {
               if($options['page'] == "edit_actions")
               { 
                 $user_sql = $this -> _setViewAccess($useraccess, $options);
                 if(!empty($user_sql))
                 {
                   $optionSql .= $user_sql;
                 } else {
                   /*
                    get nothing since the edit under manage is done to actions 
                    where you are the responsible owner, that is sub_id is in useraccess[edit] array
                   */
                   $optionSql .= " AND A.id = 0 ";
                  }
               }
               if($options['page'] == "update_actions")
               {
                 //$user_sql   = $this -> _setViewAccess($useraccess, $options);
                 $optionSql .= " AND A.action_status & ".RiskAction::AWAITING_APPROVAL." <> ".RiskAction::AWAITING_APPROVAL." ";
                 $optionSql .= " AND A.action_status & ".RiskAction::ACTION_APPROVED." <> ".RiskAction::ACTION_APPROVED." ";
                 $optionSql .= " AND (A.action_owner = '".$_SESSION['tid']."' ".$user_sql.")";           
               }
            }
            /*
            if($options['page'] == "edit_actions" || $options['page'] == "update_actions")
            {
               if($options['section'] !== "admin")
               {
                  $optionSql .= " AND A.action_owner = '".$_SESSION['tid']."' ";
               }
            }
            */
        }

        if(isset($options['page']) && $options['page'] !== "view")
        {
           //$optionSql .= " AND A.action_status & ".RiskAction::AWAITING_APPROVAL." <> ".RiskAction::AWAITING_APPROVAL." ";
           //$optionSql .= " AND A.action_status & ".RiskAction::ACTION_APPROVED." <> ".RiskAction::ACTION_APPROVED." ";
        }
        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
          $optionSql .= " AND QR.financial_year = '".$options['financial_year']."' ";
        }
        if(isset($options['action_owner']) && !empty($options['action_owner']))
        {
          $optionSql .= " AND A.action_owner = '".$options['action_owner']."' ";
        }
        if(isset($options['action_id']) && !empty($options['action_id']))
        {
          $optionSql .= " AND A.id = '".$options['action_id']."' ";
        }        
        
        
        $total   = $this -> getTotalActions($optionSql);
        //echo $optionSql;
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $optionSql .= " LIMIT ".(int)$options['start'].",".$options['limit'];
        }
        $actions = $this -> fetchAll($optionSql);
	    $data    = $this -> sortActions($actions, $total, $options);  
	    return $data;
    }
    
    function getActionActivityLog($action_id)
    {
        $columnObj   = new ActionColumns();
        $columns     = $columnObj -> getHeaderList();

	    $action_edits = $this -> get("SELECT id, action_id, changes, insertdate FROM #_action_edit WHERE action_id = '".$action_id."' ");
	    $actionedits  = Risk::logs($action_edits, $columns);

	    $action_updates = $this -> get("SELECT id, action_id, changes, insertdate FROM #_actions_update WHERE action_id = '".$action_id."' ");
	    $actionupdates  = Risk::logs($action_updates, $columns);

	    $logs = array_merge($actionupdates, $actionedits);
	    uasort($logs, function($a, $b){
	        return ($a['key'] > $b['key'] ? 1 : -1);
	    });    
        return $logs;
    }

    function getActionUpdateLog($action_id)
    {
        $columnObj      = new ActionColumns();
        $columns        = $columnObj -> getHeaderList();
	    $action_updates = $this -> get("SELECT id, action_id, changes, insertdate FROM #_actions_update 
	                                    WHERE action_id = '".$action_id."' ");
	    $action_logs    = Risk::logs($action_updates, $columns);
	    uasort($action_logs, function($a, $b) {
	        return ($a['key'] > $b['key'] ? 1 : -1);
	    });    
	     
	    $table = "<table width='100%'>";
	     $table .= "<tr><th class='th2' colspan='4'>Activity Log</th></tr>";
	      $table .= "<tr>";
	        $table .= "<th>Date Logged</th>";
	        $table .= "<th>User</th>";
	        $table .= "<th>Change Log</th>";
	        $table .= "<th>Status</th>";
	      $table .= "</tr>";
	        if(!empty($action_logs))
	        {
	           foreach($action_logs as $q_index => $log)
	           {
	              $table .= "<tr>";
	                $table .= "<td align='center'>".$log['date_loged']."</td>";
	                $table .= "<td>".$log['user']."</td>";
	                $table .= "<td>".$log['changeMessage']."</td>";
	                $table .= "<td>".$log['status']."</td>";
	              $table .= "</tr>";
	           }
	        } else {
	          $table .= "<tr>";
	            $table .= "<td align='center' colspan='4'>There are no logs yet</td>";
	          $table .= "</tr>";	        	        
	        }
	    $table .= "</table>"; 
        return $table;
    }

	function getActionToApprove($options = array()) 
	{
	    $option_sql  = " AND A.action_status & ".RiskAction::AWAITING_APPROVAL." = ".RiskAction::AWAITING_APPROVAL." ";
	    $option_sql .= " AND A.progress  = 100 AND A.status = 3  ";
		$diradminObj = new Administrator($_SESSION['tid']);
		$useraccess  = $diradminObj -> getAdminAccess();
        if(!empty($useraccess))
        {
            if(isset($useraccess['approve']))
            {
                $sub_sql = "";
                foreach($useraccess['approve'] as $index => $val)
                {
                   $sub_sql .= " QR.sub_id = ".$val." OR";
                }
                if(!empty($sub_sql))
                {
                   $option_sql .= " AND (".rtrim($sub_sql, "OR").") ";
                }		        
            }        
        } else {
          $option_sql .= " AND QR.id = 0";
        }

	    $results     = $this -> fetchAll($option_sql);
	    $data        = $this -> sortActions($results, $total = 0, $options);  
	    return $data;
	}

	/**
		Get all the approved actions 
	**/
	function getApprovedActions($options = array()) 
	{
	    $option_sql  = " AND A.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED." ";
	    $option_sql .= " AND A.progress  = 100 AND A.status = 3  ";
		$diradminObj = new Administrator($_SESSION['tid']);
		$useraccess  = $diradminObj -> getAdminAccess();
		if(!empty($useraccess))
		{
            if(isset($useraccess['approve']))
            {
                $sub_sql = "";
                foreach($useraccess['approve'] as $index => $val)
                {
                   $sub_sql .= " QR.sub_id = ".$val." OR";
                }
                if(!empty($sub_sql))
                {
                   $option_sql .= " AND (".rtrim($sub_sql, "OR").") ";
                }		        
            }		
		} else {
		    $option_sql .= " AND QR.id = 0";
		}

	    $results     = $this -> fetchAll($option_sql);
	    $data        = $this -> sortActions($results, 0, $options);  
        return $data;
	}
	
	function sortActions($actions, $total = 0, $options = array())
	{
        $columnObj           = new ActionColumns();
        $columns             = $columnObj -> getHeaderList();
        $data['total']       = $total;
        $data['headers']     = array();
        $data['actions']     = array();
        $data['avgProgress'] = 0;
        $data['progress']    = 0;
        $data['isOwner']     = array();
        $data['actionStatus']= array();
        $diradminObj  = new Administrator($_SESSION['tid']);
        $useraccess   = $diradminObj -> getAdminAccess();

		foreach($actions as $index => $action)
		{
		   $data['progress']                   += $action['progress']; 
		   $data['actionStatus'][$action['id']] = $action['action_status'];
		   if($action['owner'] == $_SESSION['tid'])
		   {
              $data['isOwner'][$action['id']]  = TRUE;
		   } else {
		      if($options['page'] == "edit" || $options['page'] == "edit_actions")
		      {
		        if(isset($useraccess['edit'][$action['sub_id']]))
		        {	        
		          $data['isOwner'][$action['id']]  = TRUE;  
		        } else {
		          $data['isOwner'][$action['id']]  = FALSE;
		        }
		      } elseif($options['page'] == "update" || $options['page'] == "update_actions") {
		        if(isset($useraccess['update'][$action['sub_id']]))
		        {
		          $data['isOwner'][$action['id']]  = TRUE;  
		        } else {
		          $data['isOwner'][$action['id']]  = FALSE;
		        }
		      } else {
		        $data['isOwner'][$action['id']]  = FALSE;
		      }
		      
		   }
	       foreach($columns as $field => $val)
		   {
			   if(array_key_exists($field, $action))
			   {
			      if($field == "action_status")
			      {
			         $data['actions'][$action['id']][$field] = $action['statusname'];
			         $data['headers'][$field]                = $val;
			      } elseif($field == "progress"){
			         $data['actions'][$action['id']][$field] = ASSIST_HELPER::format_percent($action['progress']);
			         $data['headers'][$field]                = $val;
			      } elseif($field == "action_ref"){
			         $data['actions'][$action['id']][$field] = RiskAction::REFTAG."".$action[$field];
			         $data['headers'][$field]                = $val;			      
			      } else {
                    if(Util::isTextTooLong($action[$field]))
                    {
                       $text = Util::cutString($action[$field], $field."_".$action['id']);
			           $data['actions'][$action['id']][$field] = $text;
			           $data['headers'][$field]                = $val;
                    } else {
			           $data['actions'][$action['id']][$field] = $action[$field];
			           $data['headers'][$field]                = $val;
                    } 
			      }
			   }	
		   }			
		}        
		if($total != 0)
		{
		   $data['avgProgress']  = ASSIST_HELPER::format_percent(($data['progress'] / $total));
		}
		
		if(empty($data['headers']))
		{
		  $data['headers'] = $columns;
		}

		if(empty($data['actions']))
		{
		    $msg = "";
		    if(isset($options['action_id']) && !empty($options['action_id']))
		    {
		        $msg = "There are no Actions that matched the entered criteria";
		    } else if((isset($options['financial_year']) && !empty($options['financial_year'])) &&  
		                    ($options['page'] == "update_actions" || $options['page'] == "action_update")) 
		    {
		        $msg = "You do not have any outstanding Actions to update for the selected criteria";
		    } else if( ((isset($options['financial_year']) && !empty($options['financial_year'])) ||
		               (isset($options['id']) && !empty($options['id'])) ||
		               (isset($options['action_owner']) && !empty($options['action_owner']))) &&  
		               ($options['page'] == "edit_actions" || $options['page'] == "action_edit")) {
		        $msg = "There are no Actions to edit for the selected criteria";
		    } else {
		        if(isset($options['page']))
		        {
		            if($options['page'] == "edit_actions" || $options['page'] == "action_edit")
		            {
		               $msg = "You do not have access to edit any Actions";
		            } elseif($options['page'] == "update_actions" || $options['page'] == "action_update") {
		              $msg = "You do not have any outstanding Actions to update";
		            }
		        } else {
                    $msg = "There are no Actions";
		        }		        
		    }
		    $data['action_message'] = $msg;
		}
		$data['columns'] = count($data['headers']);	          
		$data['total']       = $total;
		return $data;	
	}

	function getRiskActions($start, $limit , $option = "") 
	{		
		$response = $this->get("SELECT 
								RA.id,
								RA.action AS query_action,
								RA.deliverable, 
								RA.timescale,
								RA.deadline,
								RA.progress,
								RA.remindon,
								RA.action_status AS actionstatus,
								RAS.name AS action_status,
								RA.action_owner AS owner,
								CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner,
								DP.value AS department
								FROM #_actions RA
								INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid  = RA.action_owner
								LEFT JOIN #_action_status RAS ON RA.status = RAS.id 
								LEFT JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								WHERE RA.risk_id = '".$this -> risk_id."' 
								AND RA.active = 1
								$option
								LIMIT $start,$limit
						");					
		return $response; 
	}
	
	
	function _getRiskActions()
	{
		$response = $this->getRow("SELECT AVG(progress) AS progress, id, risk_id  
		                           FROM #_actions WHERE risk_id = '".$this -> risk_id."' \
		                           AND active = 1
		                          ");
		return $response;		
	}
	

	
	/**
		Get the total actions to be approved
	**/
	function _totalToBeApproved( $user )
	{
		$userQuery = "";
		if( $user == ""){
			$userQuery = " 1 ";
		} else {
			$userQuery = "RA.action_owner = '".$user."'";	
		}

		$response = $this -> get( " SELECT 
									COUNT(*) AS total 
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
									WHERE $userQuery 
									AND RA.action_status & ".RiskAction::AWAITING_APPROVAL." = ".RiskAction::AWAITING_APPROVAL."
		 						");
		 $total = 0;
		 foreach($response as $key => $value){
		 	$total = $value;
		 }
		 return $total; 		
	}
	
	function unlockAction( $id , $status){
		$up = $this->update( "actions" , array("action_status" => $status + RiskAction::UNLOCKED), "id=$id" );
		return $up;
	}

		
	function getAAction( $id ) {	

		$reponse = $this -> getRow( " SELECT 
									  RA.id ,
									  RA.action,
									  RA.deliverable,
									  RA.timescale,
									  RA.deadline,
									  AU.progress AS updatedprogres,
									  RA.progress,
									  RA.remindon,
									  RA.action_status,
									  RAS.name as status,
									  RAST.name as updatedstatus,
									  RAS.color as statuscolor,
									  UA.tkname ,
									  UA.tksurname, 
									  RA.action_owner,
									  RA.risk_id,
									  RR.description ,
									  AC.tkemail AS action_creator ,
									  UA.tkemail AS action_owner_email							  
									  FROM  ".$_SESSION['dbref']."_actions RA 
									  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id								
									  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid	
									  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep AC ON RA.insertuser = AC.tkid
									  LEFT JOIN  ".$_SESSION['dbref']."_actions_update AU ON RA.id = AU.action_id
									  LEFT JOIN ".$_SESSION['dbref']."_action_status RAST ON AU.status = RAST.id
									  INNER JOIN  ".$_SESSION['dbref']."_query_register RR ON RR.id = RA.risk_id
									  WHERE RA.id = '".$id."' ORDER BY AU.insertdate DESC
		 ");
		return $reponse;
	}
		
	/**
		Get all actions for this logged in user	
	**/	
	function getRiskAction( $id ) {

		$response = $this -> getRow( " 
									SELECT 
									RA.id ,
									RA.action,
									RA.deliverable,
									RA.timescale,
									RA.deadline,
									RA.progress,
									RA.remindon,
									RAS.name as status,
									RAS.color as statuscolor,
									UA.tkname ,
									RA.action_owner,
									RA.action_status,
									attachement
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid
									WHERE RA.id = '".$id."'
		 ");
		 $latestUpdate = $this -> getLatestActionUpdates( $id );
	 
		 if( isset( $latestUpdate ) && !empty( $latestUpdate ) ) {
				$response['progress'] 	= $latestUpdate['progress'];
				$response['status'] 	= $latestUpdate['name'];
				//$response['remindon'] 	= $latestUpdate['remindon'];				
		 }
		return $response;
	}
	
	function getRiskAssurance( $id ) {
								   	
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_assurance AA
								   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.insertuser
								   LEFT JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   WHERE action_id = '".$id."' ");
		return $response;
	}
	
	function saveActionAssurance($action_id, $date_tested, $response, $signoff, $assurance, $attachment, $active)
	{
		$insert_data = array(
						'action_id' 		=> $action_id,
						'response'  		=> $response,
						'signoff' 			=> $signoff,
						'attachment' 		=> $attachment,
						'insertuser'		=> $_SESSION['tid'],						
		);
		$response = $this -> insert( 'action_assurance', $insert_data );
		echo $this -> insertedId();
	}
	
	function getApprovalRequestStatus( $id )
	{
		$response = $this -> getRow("SELECT action_status FROM ".$_SESSION['dbref']."_actions  WHERE id = $id");
		return $response;
	}
	
	function declineAction( $id, $state)
	{
		$statusObj 			  = new ActionStatus("", "", "");
		$status 			  = $statusObj -> getAStatus(2);
		$res   				  = $this -> update( "actions" , array("status" => 2, "action_status" => 0 ), "id=$id");
		$changes			  = array("user" => $_SESSION['tkn'] );
		$changes['declined']  = "Action #".$id." has been declined";
		$changes['response'] = $_POST['response'];
		$changes['currentstatus'] = $status['name'];  
		$insertdata 		= array("changes" => base64_encode( serialize($changes) ), "action_id" => $id );
		
		$saveUpdate			 =  $this->insert("actions_update", $insertdata);
		return $res;	
	}
	
	function setActionStatus( $id, $state )
	{
		$res   = $this -> update( "actions" , array( "action_status" => $state ), "id=$id");
		return $res;
	}
	
	function approveAction( $id, $response, $approval, $notification, $attachment, $status)
	{
		$insertdata  = array(  "action_id" 	    => $id,
							   "description"    => $response,
							   "approval" 		=> $approval,
							   "attachment" 	=> $attachment,
							   "notification" 	=> $notification,						   
							   "insertuser"		=> $_SESSION['tid'],
			);
		$statusObj 			  = new ActionStatus("", "", "");
		$status 			  = $statusObj -> getAStatus(3);
		$changes			  = array("user" => $_SESSION['tkn'] );
		$changes['approved']  = "Action #".$id." has been approved";
		$changes['response'] 	  = $response;
		$changes['currentstatus'] = $status['name'];  
		$updatedata 		= array("changes" => base64_encode( serialize($changes) ), "action_id" => $id );		
		$saveUpdate			 =  $this->insert("actions_update", $updatedata);				
			
		$this->update("actions", array("action_status" => RiskAction::ACTION_APPROVED ), "id=$id");
		$this -> insert( "action_approved", $insertdata );
		return $this -> insertedId();
	}
	
	function updateRiskAction( $id , $changes, $attachment)
	{
		$actionInfo = $this->getAAction( $id );
		$riskObj = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$risk    = $riskObj -> getARisk( $actionInfo['risk_id'] );
		
		if($this-> _checkValidity( $risk['query_deadline_date'], $this -> deadline )){		
			if( $this-> _checkValidity($this->deadline, $this -> remindon))
	        {

				$insert_data = array(
								'action'  		=> $this -> action,
								'action_owner' 	=> $this -> action_owner,
								'deliverable' 	=> $this -> deliverable,
								'timescale' 	=> $this -> timescale,
								'deadline' 		=> $this -> deadline ,
								'remindon' 		=> $this -> remindon ,
								'insertuser'    => $_SESSION['tid'],										
				);
				$res = $this -> insert( 'action_edit', array_merge($insert_data ,array("action_id" => $id, "changes" => $changes )));
				$insert_data['attachement'] = $attachment; 
				$response = $this -> update( 'actions', $insert_data , "id = $id" );
				return $res;
			} else {
				return  "The remind on date cannot be after the deadline";
			}
		} else{
			return  "The deadline date cannot be after the ".Risk::OBJECT_NAME." deadline date (".$risk['query_deadline_date'].")";				
		}
				
	}	
	
	function getActionEditLog( $id )
	{
					  							
		$latest = $this->getLatestActionUpdates( $id );
		$join   = ""; 
		if( isset($latest) && !empty($latest)){
			$join = "LEFT JOIN ".$_SESSION['dbref']."_actions_update A ON A.action_id = AU.action_id ";			
		} else{
			$join = "INNER JOIN ".$_SESSION['dbref']."_actions A ON A.id = AU.action_id ";			
		}
		$response = $this -> get("SELECT 
								  AU.id,
								  AU.insertdate,
								  AU.changes , 
								  AST.name AS status,
								  AST.color,
								  CONCAT(TK.tkname, ' ', TK.tksurname) AS updateby
								  FROM ".$_SESSION['dbref']."_action_edit AU
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AU.insertuser
								  ".$join."
								  LEFT JOIN ".$_SESSION['dbref']."_action_status AST ON A.status = AST.id
								  WHERE AU.action_id = '".$id."'
								  ORDER BY AU.insertdate ASC, AU.id ASC
								  ");
		return $response;		
	}
	
	function getActionUpdates( $id )
	{
		
		
		$response = $this -> get("SELECT 
								  AU.id,
								  AU.changes,
								  AU.insertdate,
								  AST.name AS status
								  FROM ".$_SESSION['dbref']."_actions_update AU
								  INNER JOIN ".$_SESSION['dbref']."_action_status AST ON AST.id = AU.status 
								  WHERE AU.action_id = '".$id."'
							      ORDER BY AU.insertdate DESC, AU.id ASC								  
								  "
								  );
		return $response;
	}

	function getLatestActionUpdates( $id )
	{

		$response = $this -> getRow("SELECT
		                             AU.progress,
		                             AU.description,
		                             AU.insertdate,
		                             ASt.name,
		                             AU.remindon,
		                             AU.status
								     FROM ".$_SESSION['dbref']."_actions_update AU
								     INNER JOIN ".$_SESSION['dbref']."_action_status ASt ON ASt.id = AU.status
								     WHERE AU.action_id = '".$id."' ORDER BY insertdate desc");
		return $response;
	}
	
	function deleteRiskAction( $id )
	{
		$updatedata = array( 
							"active" 		=> 0,
							'insertuser'	=> $_SESSION['tid'],
							);
		$response  = $this -> update( 'actions' , $updatedata , "id = $id");
		return $response;
	}
	/**
		Activate an actions
		@paran int , action id
		@return int
	**/
	function activateRiskAction( $id )
	{
		$updatedata = array( 
								"active" 		=> 1,
								'insertuser'	=> $_SESSION['tid'],
								);
		$response  = $this -> update( 'actions' , $updatedata , "id = $id");
		echo $response;
	}
	/**
	 Fetched the actions associated with the user for a particular risks
	 @param int riskid , int userid- risk owner
	 @return arrray, of risk ids that the user has actions to it
	**/
	function userAssignedAction( $user ) {
		$response = $this -> get("
								  SELECT id, risk_id, action_owner, action 
								  FROM ".$_SESSION['dbref']."_actions 
								  WHERE action_owner = '".$user."'
								  GROUP BY risk_id"
								  );
		return $response;
	}
	
	function searchActionToApprove( $text )
	{
		$query = "SELECT RA.id ,RA.action, RA.deliverable, RA.timescale, RA.deadline, RA.progress, RA.remindon,
				  RAS.name as status, RAS.color as statuscolor, UA.user  
				  FROM  ".$_SESSION['dbref']."_actions RA 
				  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
				  LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
				  WHERE RA.action LIKE '%".$text."%' OR RAS.name LIKE '%".$text."%'
				  OR RA.progress LIKE '%".$text."%' OR RA.deliverable LIKE '%".$text."%'
		 ";
		 $response = $this -> get( $query );
		 return $response;
	}
	
	function getActionNotification( $where )
	{
		$reponse = $this -> getRow( " SELECT 
									  RA.id ,
									  RA.action,
									  RA.deliverable,
									  RA.timescale,
									  RA.deadline,
									  AU.progress AS updatedprogres,
									  RA.progress,
									  RA.remindon,
									  RA.action_status,
									  RAS.name as status,
									  RAST.name as updatedstatus,
									  RAS.color as statuscolor,
									  UA.tkname ,
									  UA.tksurname, 
									  RA.action_owner,
									  RA.risk_id,
									  RR.description ,
									  AC.tkemail AS action_creator ,
									  UA.tkemail AS action_owner_email							  
									  FROM  ".$_SESSION['dbref']."_actions RA 
									  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id								
									  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid	
									  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep AC ON RA.insertuser = AC.tkid
									  LEFT JOIN  ".$_SESSION['dbref']."_actions_update AU ON RA.id = AU.action_id
									  LEFT JOIN ".$_SESSION['dbref']."_action_status RAST ON AU.status = RAST.id
									  INNER JOIN  ".$_SESSION['dbref']."_query_register RR ON RR.id = RA.risk_id
									  WHERE $where ORDER BY AU.insertdate DESC
		 ");
		return $reponse;	
	}
	
	function getRawAction( $id )
	{
				$response = $this -> getRow( " 
									SELECT 
									RA.id ,
									RA.action,
									RA.deliverable,
									RA.timescale,
									RA.deadline,
									RA.progress,
									RA.remindon,
									RA.status AS statusid,
									RAS.name as status,
									RAS.color as statuscolor,
									CONCAT(UA.tkname ,' ',UA.tksurname) AS owner ,
									RA.action_owner,
									RA.action_status,
									attachement
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid
									WHERE RA.id = '".$id."'");
			return $response;
	}
	
     function filterActions($fields, $whereStr = "", $groupBy = "", $sortBy = "", $joins = "")
     {
	     $orderBy = (!empty($sortBy) ? "ORDER BY $sortBy " : "");
	     $groupBy = (!empty($groupBy) ? "GROUP BY $groupBy " : "");
		 $results = $this -> get("SELECT A.id, A.action_status, A.status AS actionstatus, 
                                  A.progress AS action_progress, A.action_on AS datecompleted, 
 								  A.deadline AS action_deadline, ".$fields." 
				                  FROM #_actions A
				                  INNER JOIN #_query_register RR ON RR.id = A.risk_id 
				                  $joins
				                  WHERE 1
				                  $whereStr
				                  $groupBy
				                  $orderBy
					            ");
		return $results;		
     }	
     
	 static function getStatusSQLForWhere($a) 
     {
        $sql = "(
                        ".$a.".active = 1
                        
        )";
        return $sql;
     }     
     	
}
?>