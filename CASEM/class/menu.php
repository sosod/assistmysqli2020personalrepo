<?php
/**
	* Database connection 
	* @author 	 : admire<azinamo@gmail.com>
	* @copyright : 2011 Ignite Assit risk module
	* @filename  : menu.php
**/
class Menu extends DBConnect{
	
	function __construct(){
		parent::__construct();
	}
	
	function getMenu(){
		$query = mysql_query( "SELECT id , parent_id , name , client_name , folder FROM  ".$_SESSION['dbref']."_menu " );
		$menudata = array();
		 while( $row = mysql_fetch_assoc( $query ) ) {
		 	$menudata[$row['id']] = array( $row['id'] , $row['parent_id'] ,($row['client_name'] == "" ? $row['name'] : $row['client_name'] ) ,$row['folder'] ); 
		 }
		 return $menudata;
	}
	
	function getAllMenu()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_menu " );
		return $response;
	}

    function getAMenu( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_menu WHERE folder = '".$id."'" );
		return $response;
	}
	
	function updateMenuName( $old_ref , $client_value ){
		$insert_data = array(
			'client_name' => $client_value,
			'insertuser'  => $_SESSION['tid'],
		);
		$where    = "folder = '".$old_ref."'";
        $data     = $this->getAMenu( $old_ref );
        $response = 0;
        if($data['client_name'] != $client_value)
        {
            $logsObj              = new Logs();
            $_POST['id']          = $data['id'];
            $_POST['client_name'] = $client_value;
            unset($_POST['value']);
            unset($_POST['name']);
            $logsObj -> setParameters( $_POST, $data, "menu");
		    $response =  $this -> update('menu', $insert_data, $where );            
        }
		echo  $response;
	}
	
	function getNaming(){
		
	}
	
	function getSubMenu( $basename ) {
				   
		$query 	 = "SELECT id, name, client_name, folder FROM ".$_SESSION['dbref']."_menu 
				   WHERE parent_id = (SELECT id FROM ".$_SESSION['dbref']."_menu WHERE folder = '".$basename."' )";
		$results =  $this -> get( $query );
		return $results;
	}
	
}
?>