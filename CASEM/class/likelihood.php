<?php
/**
	* Likelihood 
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ingite Assit
**/
class Likelihood extends DBConnect
{
	
	protected $rating_from;
	protected $rating_to;
	protected $assessment;
	protected $definition;
	protected $probability;
	protected $color;

	function __construct( $rating_from, $rating_to, $assessment, $definition, $probability,  $color )
	{
		$this -> rating_from = trim($rating_from);
		$this -> rating_to 	 = trim($rating_to);
		$this -> assessment  = trim($assessment);
		$this -> definition  = trim($definition);
		$this -> probability = trim($probability);		
		$this -> color 		 = trim($color);						
		parent::__construct();
	}

	function saveLikelihood()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"probability" => $this -> probability,						
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "likelihood" , $insert_data );
		echo $this -> insertedId();		
	}	
	
	function updateLikelihood( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"probability" => $this -> probability,						
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> update( "likelihood" , $insert_data , "id = $id");
		return $response;		
	}		
		
	function getLikelihood()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_likelihood WHERE  1" );
		return $response;
	}
	
	function getALikelihood( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_likelihood WHERE id = $id" );
		return $response;
	}
	
	function getLikelihoodRating( $id )
	{
		$response = $this -> get( "SELECT rating_from, rating_to FROM ".$_SESSION['dbref']."_likelihood WHERE id = '".$id."' " );
		$diff = $to = $from = 0;
		foreach($response as $row ) {
			$diff 	= $row['rating_to'] - $row['rating_from'];
			$to 	= $row['rating_to'];
			$from 	= $row['rating_from'];
		} 
		
		echo json_encode( array( "from" => $from, "to" => $to , "diff" => $diff) );
	}	

	function updateLikelihoodStatus( $id , $status)
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'likelihood', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateLikelihood( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'likelihood', $update_data, "id=$id" );
		echo $response;
	}
}
?>