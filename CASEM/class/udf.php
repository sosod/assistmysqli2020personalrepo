<?php
/**
	@package 	: User defined fields 
	@author		: admire<azinamo@gmail.com>
**/
class UDF extends DBConnect
{

	protected $udfvalue;
	
	protected $udfilist;
	
	protected $udfiref;
	
	protected $udficustom;
	
	protected $udfisort;
	
	protected $udfiyn;
	
	protected $udfirequired;
	
	protected $udfidefault;
	

	function __construct()
	{
		parent::__construct();
		// how about create a class that initalizes post variables , just call it here 
		$this -> udfvalue 		= (isset($_POST['udfvalue']) ? $_POST['udfvalue'] : "");		
		$this -> udfilist 		= (isset($_POST['udfilist']) ? $_POST['udfilist'] : "");		
		$this -> udfiref		= (isset($_POST['udfiref']) ? $_POST['udfiref'] : "");
		$this -> udficustom		= (isset($_POST['udficustom']) ? $_POST['udficustom'] : "");
		$this -> udfisort		= (isset($_POST['udfisort']) ? $_POST['udfisort'] : "");		
		$this -> udfiyn 		= (isset($_POST['udfiyn']) ? $_POST['udfiyn'] : "");
		$this -> udfirequired	= (isset($_POST['udfirequired']) ? $_POST['udfirequired'] : "");
		$this -> udfidefault 	= (isset($_POST['udfidefault']) ? $_POST['udfidefault'] : "");
	}

	function saveUDF()
	{
		$insertdata = array( 
							"udfivalue" 	=> $this -> udfvalue,
							"udfvalue"  	=> $this -> udfvalue,
							"udfilist"		=> $this -> udfilist,
							"udfiref"		=> $this -> udfiref,
							"udficustom"	=> $this -> udficustom,	
							"udfisort"		=> $this -> udfisort,
							"udfiyn"		=> $this -> udfiyn, 	
							"udfirequired"	=> $this -> udfirequired,
							"udfidefault" 	=> $this -> udfidefault,
						 );
		$this -> insertForCompany( "udfindex", $insertdata );
		echo $this -> insertedId();
	}

	function updateUDF( $id )
	{
		$insertdata = array( 
							"udfivalue" 	=> $this -> udfvalue,
							"udfvalue"  	=> $this -> udfvalue,
							"udfilist"		=> $this -> udfilist,
							"udfiref"		=> $this -> udfiref,
							"udficustom"	=> $this -> udficustom,	
							"udfisort"		=> $this -> udfisort,
							"udfiyn"		=> $this -> udfiyn, 	
							"udfirequired"	=> $this -> udfirequired,
							"udfidefault" 	=> $this -> udfidefault,
						 );
		$this -> updateForCompany( "udfindex", $updatedata, "udfid=$id");
		echo $this -> insertedId();
	}

}
?>