<?php
/**
	* Finicial Year class
	* @author 	: <azinamo@gmail.com>
	* copyright	: 2011 Ignite Assist
	* 
**/
class FinancialYear extends DBConnect
{
	protected $last_day;
	
	protected $start_date;
	
	protected $end_date;
	
	protected $_months = array(
						"01" => "January",
						"02" => "Febrauary",
						"03" => "March",
						"04" => "April",
						"05" => "May",
						"06" => "June",
						"07" => "July",
						"08" => "August",
						"09" => "September",
						"10" => "October",
						"11" => "November",
						"11" => "December",
						);
	
	protected $_errors = array();
	
	function __construct($last_day, $start_date, $end_date ) {
		$this -> last_day 	= $last_day;//$this -> _formatDate( $last_day ) ;
		$this -> start_date = $start_date;//$this -> _formatDate( $start_date ) ;
		$this -> end_date 	= $end_date; //$this -> _formatDate( $end_date );
		//$conn = new DB_Connect("root" ,"","ignite_i".$_SESSION['cc']);
		parent::__construct();
	}
	
	function _formatDate( $date )
	{
		$dateArray 	  = explode("/",$date);
		$formatedDate = $dateArray[1]."-".$this -> _months[$dateArray[0]]."-".$dateArray[2];	
		return $formatedDate;
	}
	/**
		Validate the date posted format
	**/
	function _validateDateFormat( $postedFormat ){
	
		if( strpos( $postedFormat ,"/" ) > 1 ) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	function _validateYearStartEnd()
	{	
	   $starttimestamp = strtotime($this -> start_date);
	   $endtimestamp   = strtotime($this -> end_date);
	   if($endtimestamp > $starttimestamp) 
	   {
		  return TRUE;
	   } else {
		  return FALSE;
	   }
	}
	
	function getFinYear()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_financial_years WHERE active = 1" );
		return $response;
	}
	
	function getAFinYear( $id ){
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_financial_years WHERE id = $id" );
		return $response;
	}
	
	/**
		Save the financial year
	**/
	function saveFinYear()
	{		
	  $response =  array();
	  if($this -> isValidEndDate($this -> last_day, $this -> end_date))
	  {
	     if($this -> isValidEndDate($this -> end_date, $this -> start_date))
	     {
	        $insertdata = array(
					        "start_date" 	=> $this -> start_date,
					        "end_date"		=> $this -> end_date,
					        "last_day"		=> $this -> last_day,
					        "insertuser"	=> $_SESSION['tid']
	        );
	        $insertid = $this -> insert("financial_years", $insertdata );
	        if($this -> insertedId() > 0)
	        {
	           $response = array('error' => false, 'text' => 'Financial year successfully saved');
	        } else {
	           $response = array('error' => true, 'text' => 'Error saving financial year');
	        }
	     } else {
	       $response = array('error' => true, 'text' => 'Financial year start date cannot be after the end date');      
	     }
	  } else {
	    $response = array('error' => true, 'text' => 'Financial year end date cannot be after the last day'); 
	  }	  
	  return $response;
	}
	/**
	 Update the financial year
	**/
	function updateFinacialYear( $id )
	{	  
	  $response =  array();
	  if($this -> isValidEndDate($this -> last_day, $this -> end_date))
	  {
	     if($this -> isValidEndDate($this -> end_date, $this -> start_date))
	     {
		    $updatedata = array(
						    "start_date" => $this -> start_date,
						    "end_date"	 => $this -> end_date,
						    "last_day"	 => $this -> last_day,
						    "insertuser"	=> $_SESSION['tid']
		    );
		    $data    = $this -> getAFinYear($id);
		    $logsObj = new Logs();
		    $logsObj -> setParameters($_POST, $data, "financial_year");
		    $updateid = $this -> update("financial_years", $updatedata , "id=$id");

		    if($updateid > 0)
		    {
		      $response = array('error' => false, 'text' => 'Financial year updated successfully', 'updated' => true);       
		    } elseif($updateid == 0){
		      $response = array('error' => false, 'text' => 'No change made to the financial year', 'updated' => false);
		    } else {
		      $response = array('error' => true, 'text' => 'Error updating financial year', 'updated' => false);  
		    }
	     } else {
	       $response = array('error' => true, 'text' => 'Financial year start date cannot be after the end date');      
	     }
	  } else {
	    $response = array('error' => true, 'text' => 'Financial year end date cannot be after the last day'); 
	  }
	  return $response;
	}
	
	function isValidEndDate($enddate, $startdate)
	{
	   $enddate   = strtotime($enddate);
	   $startdate = strtotime($startdate);
	   if($enddate >= $startdate)
	   {
	     return TRUE;
	   } 
	  return FALSE;
	}
	
	function deleteFinacialYear( $id )
	{
		$response = array();
		$res      = $this -> update( "financial_years" , array( "active" => 0 ), "id=$id");
	    if($res > 0)
	    {
	      $response = array('error' => false, 'text' => 'Financial year deleted successfully', 'updated' => true);       
	    } elseif($updateid == 0){
	      $response = array('error' => false, 'text' => 'No change made to the financial year', 'updated' => false);
	    } else {
	      $response = array('error' => true, 'text' => 'Error deleted financial year', 'updated' => false);  
	    }
		return $response;
	}
	/**
		Convert fro user readable format , to a date picker format , for editing 
		@return string;
	**/
	function setToDatePickerFormat( $date )
	{
		$datePieces = explode("-", $date);
		$day 		= $datePieces['0'];
		$_month 		= $datePieces['1'];
		$year 		= $datePieces['2'];
		
		foreach( $this -> _months as $key => $month ) {
			if( $month == $_month ) {
				$mon = $key;
			}
		} 
		return $mon."/".$day."/".$year;
	}
	
	function getReportList()
	{
		$data = array();
		$response = $this -> getFinYear();
		foreach($response as $d) {
			$data[$d['id']] = array(
				'id'=>$d['id'],
				'name'=>$d['start_date']." - ".$d['end_date'],
			);
		}
		return $data;
	}	
	
}
?>