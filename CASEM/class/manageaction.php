<?php
class ManageAction extends ActionManager
{
	
	function __construct()
	{
		parent::__construct();		
	}

	function getOptions($options = array())
	{
	    $option_sql = "";
	    if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
	    {
	       $option_sql .= " AND A.deliverable_id = '".$options['deliverable_id']."' ";
	    }
        if(isset($options['page']))
        {
            if($options['page'] === "update")
            {
               $option_sql .= " AND A.owner = '".$_SESSION['tid']."' ";
            }
            if($options['page'] === "edit")
            {
               //allow deliverable accountable persons to edit actions
               $accountableObj = new AccountablePersonMatch(); 
               $user_sql       = " AND APU.user_id = '".$_SESSION['tid']."' ";
               $accountables   = $accountableObj -> getUserAccountableDeliverable($user_sql);
               if(!empty($accountables))
               {
                  foreach($accountables as $index => $val)
                  {
                      $acc_sql .= " OR D.id = '".$val['deliverable_id']."' ";
                  }   
               }                  
               $option_sql  .= " AND (D.responsibility_owner = '".$_SESSION['tid']."' ".$acc_sql.")";
            }
                  
        }
        if(isset($options['view']) && $options['view'] == "all")
        {
        
        } else {
	     $option_sql .= " AND (A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
	    		           OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
	    		           OR A.actionstatus & ".ACTION::MANAGE_CREATED." = ".ACTION::MANAGE_CREATED."
	    		        )";	   
	    }
	   if(isset($options['financial_year']) && !empty($options['financial_year']))
	   {
	     $option_sql .= " AND L.financial_year = '".$options['financial_year']."' ";
	     //$optionSql .= " AND L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." ";
         $option_sql .= " AND (L.legislation_status & ".Legislation::ACTIVE." = ".Legislation::ACTIVE." 
                              OR L.legislation_status & ".Legislation::CREATED." = ".Legislation::CREATED.") ";		     
	   }
	   
	   if(isset($options['legislation_id']) && !empty($options['legislation_id']))
	   {
	     $option_sql .= " AND L.id = '".$options['legislation_id']."' ";
	   }	 
   	   $option_sql .= " AND D.status & ".Deliverable::EVENT_ACTIVATED." <> ".Deliverable::EVENT_ACTIVATED." ";
	   if(isset($options['limit']) && !empty($options['limit']))
	   {
	     $option_sql .= " LIMIT ".$options['start']." , ".$options['limit']."";
	   } 
	   return $option_sql;
	}

     /*
	function getActions($start, $limit, $options = array() )
	{
		$optionSql = "";

		if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
		{
			$this->deliverableId = $options['deliverable_id'];
			$optionSql = " AND A.deliverable_id = '".$options['deliverable_id']."'";		
		}	
		if($options['page'] == "update" || $options['page'] == "edit")
		{
		  $optionSql .= " AND A.owner = '".$_SESSION['tid']."' ";
		}
		$optionSql .= " AND (A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
		                     OR A.actionstatus & ".ACTION::CONFIRMED." = ".ACTION::CONFIRMED." 
		    		           OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
		    		           OR A.actionstatus & ".ACTION::MANAGE_CREATED." = ".ACTION::MANAGE_CREATED."
		    		        )";	 		        	
		$actions  = $this->actionObj->fetchAll($start,$limit, $optionSql);
		$actionsInfor = $this->actionObj->getActionProgressStatitics($optionSql);
		$response = $this->sortActions($actions, new ActionNaming("manage"));
		$response['total'] = $actionsInfor['totalActions'];
		$response['totalActions'] = $actionsInfor['totalActions'];
		$response['average'] = round($actionsInfor['averageProgress'], 2);
		return $response;
	}
	
	function updateAction( $data )
	{
		$actionObj 	= new Action(); 
		$id 		= $data['id'];
		$action     = $actionObj -> fetch($id);
		unset($data['id']);
		$updatedata = array();
		$updatedata['progress'] = $data['progress'];
		$updatedata['status']   = $data['status'];
		$updatedata['reminder'] = $data['reminder'];
		if(($action['actionstatus'] & Action::AWAITING_APPROVAL) != Action::AWAITING_APPROVAL)
		{
		  if($data['progress'] == "100" || $data['status'] == "3")
		  {
			$updatedata['actionstatus'] = $action['actionstatus'] + Action::AWAITING_APPROVAL;
		  }		     
		}
			
		$actionObj->setTablename("action");
		$res 	  = $actionObj->update($id, $updatedata, new ActionAuditLog(), $actionObj, new ActionNaming());
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Action updated", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the action", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the action", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function getApprovalActions( $start, $limit, $id)
	{	
		$actionObj = new Action();
		//get the actions awaiting to be approved
		$this->deliverableId  = $id;
		$optionSql = "";
		if(!empty($this->deliverableId))
		{
		   $optionSql = " AND deliverable_id = '".$this->deliverableId."' ";
		}
          $optionSql .=  "AND ( A.actionstatus & ".Action::MANAGE_CREATED." = ".Action::MANAGE_CREATED."
                               OR A.actionstatus & ".ACTION::ACTIVATED." = ".ACTION::ACTIVATED."
                               OR A.actionstatus & ".ACTION::CREATEDBY_SUBEVENT." = ".ACTION::CREATEDBY_SUBEVENT."
                         )";
		$awaitingOptions = " AND A.actionstatus & ".Action::AWAITING_APPROVAL." = ".Action::AWAITING_APPROVAL."  $optionSql";
		$awaitingActions = $actionObj->fetchAll($start, $limit, $awaitingOptions);  
		$awaiting 	  = $this->sortActions( $awaitingActions, new ActionNaming("manage")); 
		
		$approvedOptions = " AND A.actionstatus & ".Action::APPROVED." = ".Action::APPROVED." $optionSql";
		$approvedActions = $actionObj -> fetchAll($start, $limit, $approvedOptions);
		$approved		 = $this->sortActions( $approvedActions, new ActionNaming("manage"));
		$actions = array("awaiting" => $awaiting, "approved" => $approved);
		return $actions;
	}
	
	function approveAction( $data )
	{
		$actionObj = new Action();		
		$id = $data['id'];
		$action     = $actionObj -> fetch($id);		
		$updatedata = array();
		//unsetting or removing the awaiting approval status
		$updatedata['actionstatus'] = ($action['actionstatus'] - Action::AWAITING_APPROVAL) + Action::APPROVED;
			
		$actionObj->setTablename("action");

		$res 	  = $actionObj->update($id, $updatedata, new ActionAuditLog(), $actionObj, new ActionNaming());
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Action approved", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the action", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the action", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function declineAction( $data )
	{
		$actionObj = new Action();		
		$id = $data['id'];
		$action     = $actionObj -> fetch($id);		
		$updatedata = array();
		//unsetting or removing the awaiting approval status
		$updatedata['actionstatus'] = ($action['actionstatus'] - Action::AWAITING_APPROVAL);
		$updatedata['status'] 	    = 2;
		//adding the approved status
			
		$actionObj->setTablename("action");

		$res 	  = $actionObj->update($id, $updatedata, new ActionAuditLog(), $actionObj, new ActionNaming());
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Action declined ", "error" => false, "updated" => true );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the action", "error" => false, "updated" => true);
		} else {
			$response = array("text" => "Error saving the action", "error" => true, "updated" => false);
		}
		return $response;
	}
	
	function getUpdatedActions()
	{
		$actionObj = new Action();
		$updatedActions = array();
		$actionVersions = $actionObj -> getCurrentVersions();

		$actions  = array();
		foreach( $actionVersions as $index => $action) 
		{
			$latestVersion  = $actionObj -> getLatestVersion( $action['id'] );

			if(!empty($latestVersion))
			{
				if($latestVersion['id'] != $action['version_in_use'])
				{
					$actions[] = "'".$action['id']."'";
				}
			}		
		}
	
		if(!empty($actions))
		{
			$options = " AND A.id IN(".implode(",", $actions).")";
			$updatedActions =  $actionObj->fetchAll( 0, 10, "", $options );
		}
		$actionReferences = array();
		foreach( $updatedActions as $index => $action)
		{
			$actionReferences[$action['ref']]= $action['action_reference'];
 		}
		$response  = array("actions" => $this->sortHeaders($updatedActions, new ActionNaming()) , "actionsRef" => $actionReferences);
		return $response;
	}
	
	function allowUpdate( $data )
	{
		list($refId , $idRef ) = explode("_", $data['ref'] );
		$ref 	 	  = substr($refId, 6);
		$id 		  = substr($idRef, 2 );
		$actionObj 	  = new Action(); 
		$latestaction = $actionObj -> importAction( $ref );
		$version  	  = $actionObj -> getLatestVersion( $id );
		$res		  =  ""; 
		$response  	  = array();
		if(!empty($latestaction))
		{
			$actionObj -> setTablename("action");
			$res = $actionObj -> update($id, $latestaction, new ActionAuditLog());
			if( $res > 0){
				$actionObj -> setTablename("action_usage");
				$usage = array("current_version" => $version['id'], "version_in_use" => $version['id'] );

				$actionObj -> update($id, $usage, new ActionAuditLog());
			}
		}
		if( $res > 0)
		{
			$response = array("text" => "Action updated . . ", "error" => false);
		} else {
			$response = array("text" => "Error updating action", "error" => true);
		}
		return $response;
	}
	*/


}
?>
