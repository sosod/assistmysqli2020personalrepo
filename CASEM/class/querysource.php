<?php
/**
	* Risk types for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class QuerySource extends DBConnect
{
	
	var $type;
	
	var $description;
	
	var $name;
	
	
	function __construct()
	{
		parent::__construct();
	}
	
	function saveQuerySource($insert_data)
	{
        $insert_data['insertuser'] = $_SESSION['tid'];
		$response                  = $this -> insert( "query_source" , $insert_data );
		return $this -> insertedId();		
	}
	
	function getQuerySource($id)
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_query_source WHERE id = '".$id."' " );
		return $response ;
	}
	
	function getQuerySources($option_sql = "")
	{
		$response = $this -> get("SELECT * FROM ".$_SESSION['dbref']."_query_source WHERE status & 2 <> 2 $option_sql");
		return $response ;		
	}	
	/**
		Get used risk types , so they cannot be deleted
	**/
	function getUsedSources()
	{	
		$response = $this -> get("SELECT query_source FROM ".$_SESSION['dbref']."_risk_register GROUP BY query_source");
		return $response;
	}
	
			
	function updateQuerySource($id, $update_data, $updates)
	{
	    $res = 0;
	    if(!empty($update_data))
	    {
	        $res += $this -> update('query_source', $update_data, "id='".$id."' ");
	    }
	    if(!empty($updates))
	    {
	      $this -> insert('query_source_logs', $updates);
	      $res += $this -> insertedId();
	    }
		return $res;
	}

	function getReportList() 
	{
		$response = $this -> get( "SELECT t.id, t.name FROM ".$_SESSION['dbref']."_query_source t INNER JOIN ".$_SESSION['dbref']."_query_register q ON q.type = t.id WHERE t.status & 2 <> 2 AND q.active & 2 <> 2" );
		return $response ;		
	}
}
?>