<?php
/**
	* Risk level for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class RiskLevel extends DBConnect
{
	
	protected $shortcode;
	
	protected $status;
	
	protected $description;
	
	protected $name;
	
	function __construct( $shortcode, $name, $description, $status)
	{
		$this -> shortcode 	 = trim($shortcode);
		$this -> name 		 = trim($name);
		$this -> description = trim($description);
		$this -> status      = trim($status);		
		parent::__construct();
	}
	
		
	function saveLevel()
	{
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"status" 	 => $this -> status ,
						"insertuser"  => $_SESSION['tid'],
						);
			
		$response = $this -> insert( "level" , $insert_data );
		echo $this -> insertedId();		
	}
	
	function getLevel()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE status <> 2" );
		return $response ;		
	}	
	
	function getARiskLevel( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE id = '".$id."'" );
		return $response ;		
	}	
	/**
		Get used risk types , so they cannot be deleted
	**/
	function getUsedLevel()
	{	
		$response = $this -> get("SELECT level FROM ".$_SESSION['dbref']."_risk_register GROUP BY level");
		return $response;
	}
	/**
		Fetches all active risk types
	**/
	function getActiveRiskLevel()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE active = 1" );
		return $response ;		
	}		
			
	function updateLevel( $id )
	{
		$insertdata = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"status"	  => $this -> status, 
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $logsObj -> setParameters( $_POST, $data, "level");						
		$response = $this -> update( "level", $insertdata , "id=$id");
		return $response;
	}
	
	function changeLevelStatus( $id, $status )
	{
		$updatedata = array(
						'status' 	  => $status,
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $logsObj -> setParameters( $_POST, $data, "level");		
		$response = $this -> update( 'level', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateLevel( $id )
	{
		$update_data = array(
						'status' 	  => "1",
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $logsObj -> setParameters( $_POST, $data, "level");		
		$response = $this -> update( 'level', $update_data, "id=$id" );
		echo $response;
	}	
	
	function getReportList()
	{
		$response = $this -> get( "SELECT L.id, L.name
			   FROM ".$_SESSION['dbref']."_level L 
			   LEFT JOIN ".$_SESSION['dbref']."_query_register Q
			   ON Q.risk_level = L.id WHERE L.status & 2 <> 2
		 " );
		return $response;
	}
}
?>