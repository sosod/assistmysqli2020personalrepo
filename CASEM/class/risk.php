<?php
/**
	* @package 		: Risk 
	* @author 		: admire<azinamo@gmail.com>
	* @copyright	: 2011 Ignite Assist
**/
class Risk extends DBConnect
{
	/**
		@ int	 	 	 	 	 	 	
	**/
	protected $id;
	/**
		@int
	**/
	protected $item;
		/**
		@int
	**/
	protected $type;
	/**
		@int
	**/
	protected $category;
	/**
		@char
	**/
	protected $description;
	/**
		@char
	**/
	protected $background;
	/**
		@int
	**/
	protected $query_reference;
	/**
		@int
	**/
	protected $financial_exposure;
	/**
		@int
	**/
	protected $monetary_implication;
	/**
		@int
	**/
	protected $risk_detail;
	/**
		@text
	**/
	protected $finding;
	/**
		@text
	**/
	protected $client_response;
	/**
		@char
	**/
	protected $recommendation;
	/**
		@int
	**/
	protected $risk_level;
	/**
		@int
	**/
	protected $query_date;
	/**
		@char
	**/
	//protected $risk_level;
	/**
		@char
	**/
	protected $residual_risk_exposure;
	/**
		@char
	**/
	protected $risk_owner;
	/**
		@char
	**/
	protected $actions_to_improve;
	/**
		@char
	**/
	protected $action_owner;
	/**
		@char
	**/
	protected $time_scale;
	/**
		@char
	**/
	protected $status;
	/**
		@char
	**/
	protected $udf;

    protected $risk_type;
	/**
		@char
	**/
	protected $attachement;
	/**
		@char
	**/
	protected $auditor_conclusion;
	/**
		@char
	**/	
	protected $internal_control_deficiency;
	/**
		@date
	**/	
	protected $insertdate;
	/**
		@int
	**/	
	protected $start;
	
	protected $limit;
	
	protected $query_deadline_date;
	
	const DELETED   = 2;
	
	const ACTIVATED = 4;
	
	const REFTAG    = "C";
	const OBJECT_NAME = "Case";
	const PLURAL_OBJECT_NAME = "Cases";
	const PLURAL_OBJECT_POSTFIX = "s";
 	
	/**
	 Action ids which are associated to a user
	 @array
	**/
	protected $actionIds = array();
	
	function __construct( $type, $category,  $description, $background, $financial_exposure, $monetary_implication, $risk_detail, 
						  $finding, $recommendation, $query_reference, $client_response, $risk_owner , $attachement
		  )
	{
		//$this -> item 								= $item;
		$this -> type   							= $type;
		$this -> category 							= $category;
		$this -> background 						= $background;		
		$this -> description 						= $description;
		$this -> query_reference 					= $query_reference;
		$this -> monetary_implication 				= $monetary_implication;
		$this -> risk_detail 						= $risk_detail; 
		$this -> finding 							= $finding;
		$this -> recommendation 					= $recommendation;
		$this -> client_response 					= $client_response;
		$this -> risk_level 						= (isset($_POST['risk_level']) ? $_POST['risk_level'] : ""); 
		$this -> query_date 						= (isset($_POST['query_date']) ? $_POST['query_date'] : "");
        $this -> risk_type 						    = (isset($_POST['risk_type']) ? $_POST['risk_type'] : "");
        $this -> query_deadline_date				= (isset($_POST['query_deadline_date']) ? $_POST['query_deadline_date'] : "");        
		$this -> financial_exposure 				= $financial_exposure;
		$this -> internal_control_deficiency		= (isset($_REQUEST['internal_control_deficiency']) ? $_REQUEST['internal_control_deficiency'] : "");
		$this -> auditor_conclusion 				= (isset($_REQUEST['auditor_conclusion']) ? $_REQUEST['auditor_conclusion'] : "");
		//$this -> residual_risk_exposure 			= $residual_risk_exposure;
		$this -> risk_owner 						= $risk_owner;	
		//$this -> actions_to_improve 				= $actions_to_improve;
		//$this -> action_owner 						= $action_owner;
		//$this -> time_scale 						= $time_scale;
		//$this -> status 							= $status;
 		$this -> attachement						= $attachement;	

		parent::__construct();
	}
	
	function  saveQuery($data = array())
	{
		$insertdata = array(
			'type'   							=> $this -> type,
			'category' 							=> $this -> category,
			'description' 						=> $this -> description,
			'background'						=> $this -> background ,
			'query_reference' 					=> $this -> query_reference,
			'financial_exposure'				=> $this -> financial_exposure,
			'monetary_implication'				=> $this -> monetary_implication,
			'risk_detail'						=> $this -> risk_detail,
			'finding'							=> $this -> finding,						
			'recommendation' 					=> $this -> recommendation,
			'client_response' 					=> $this -> client_response,
			'risk_level' 						=> $this -> risk_level,
			'query_date' 						=> $this -> query_date,
			'risk_type' 	                    => $this -> risk_type,
			'query_deadline_date' 				=> $this -> query_deadline_date,
			'internal_control_deficiency'		=> $this -> internal_control_deficiency,
			'auditor_conclusion'				=> $this -> auditor_conclusion,
			'attachment' 						=> $this -> attachement	,		
			'sub_id' 							=> $this -> risk_owner,
			'query_source' 					    => (isset($_POST['query_source']) ? $_POST['query_source'] : 0),
			'insertuser'						=> $_SESSION['tid'],		
			'financial_year'                    => (isset($data['financial_year']) ? $data['financial_year'] : "")
		);
		$response  = $this -> insert('query_register', $insertdata);
		return $this -> insertedId();
	}
	
	
	function getQueryDetail($id)
	{
        $results = $this -> getRow("SELECT RR.id AS query_item, RR.description, RR.background, RR.monetary_implication,
                                    RR.query_deadline_date, RR.risk_detail, RR.finding, RR.recommendation, RR.client_response, 
                                    RR.query_reference, RR.internal_control_deficiency, RR.auditor_conclusion, RR.query_date, 
                                    RR.attachment, RC.name AS query_category, RT.name AS query_type, RRT.name AS risk_type,
                                    RS.name as query_status, RL.name AS risk_level, FE.name AS financial_exposure,
                                    D.dirtxt  AS query_owner, CONCAT(FC.start_date, ' - ', FC.end_date) AS financial_year,
							        RR.type, RR.category, RR.risk_type AS risktype, RR.sub_id AS riskowner, 
							        RR.sub_id AS user_responsible, RR.active,
							        RR.risk_level AS risklevel, RR.status, RR.financial_exposure AS financialexposure,
							        QS.name AS query_source, RR.query_source AS querysource, RR.insertuser, RR.remind_on
                                    FROM ".$_SESSION['dbref']."_query_register RR
                                    LEFT JOIN ".$_SESSION['dbref']."_financial_years FC ON FC.id = RR.financial_year
                                    LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
                                    LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
                                    LEFT JOIN ".$_SESSION['dbref']."_query_source QS ON QS.id = RR.query_source
                                    LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							        LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							        LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = RR.risk_level
							        LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
							        LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = RR.sub_id
							        LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							        LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                    WHERE RR.active <> 0 AND  RR.id = '".$id."'
                                  ");  
        return $results;
	}
	/*
	    fetches the queris
	*/
	function fetchAll($option_sql = "" )
	{
	    $results = $this -> get("SELECT DISTINCT(RR.id) AS query_item, RR.id, RR.description, RR.background AS query_background,
							      RR.monetary_implication, RR.query_deadline_date, RR.risk_detail, RRT.name AS risk_type,
							      RR.finding, RR.recommendation, RR.client_response, RR.query_reference, RR.client_response,
							      RR.internal_control_deficiency, RR.auditor_conclusion, RR.query_date,
							      FE.name AS financial_exposure, RL.name AS risk_level, RC.name as query_category, 
							      RT.name as query_type, RT.shortcode as type_code, 
							      CONCAT(D.dirtxt, ' - ', DS.subtxt) AS query_owner, RR.sub_id, RS.name AS query_status,
							      RS.color AS query_status_color, CONCAT(FC.start_date, ' - ', FC.end_date) AS financial_year, 
							      RR.attachment, QS.name AS query_source
							      FROM ".$_SESSION['dbref']."_query_register RR 
							      LEFT JOIN ".$_SESSION['dbref']."_financial_years FC ON FC.id = RR.financial_year
							      LEFT JOIN ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							      LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							      LEFT JOIN ".$_SESSION['dbref']."_query_source QS ON QS.id = RR.query_source
							      LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							      LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							      LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = RR.risk_level	
							      LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure	
							      LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = RR.sub_id
							      LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							      LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							      WHERE RR.active <> 0 $option_sql 
							    ");
	   return $results;
	}
	/*
	    get the total queris
	*/
	function getTotalQueries($option_sql = "" )
	{
	   $result = $this -> getRow("SELECT COUNT(DISTINCT(RR.id)) AS total
							       FROM ".$_SESSION['dbref']."_query_register RR 
							       LEFT JOIN ".$_SESSION['dbref']."_financial_years FC ON FC.id = RR.financial_year
							       LEFT JOIN ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							       LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							       LEFT JOIN ".$_SESSION['dbref']."_query_source QS ON QS.id = RR.query_source
							       LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							       LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							       LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = RR.risk_level	
							       LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure	
							       LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = RR.sub_id
							       LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							       LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							       WHERE RR.active <> 0 $option_sql 
							    ");
	   return $result['total'];
	}
	
	/**
		Sets the view options for each used based on the user access settings
		@param , userAccess array , pageRequest page requesting the view
		@return string, sql string
	**/
	function _setViewAccess($useraccess, $options = array(), $userRiskRef = array()) 
	{
	    $viewType        = (isset($options['view']) ? $options['view'] : "viewMyn");
		$view_access_sql = "";
        if($viewType == "viewMyn") 
        {
		    //$view_access_sql = " AND (DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB' ";
		    if(!empty($useraccess))
		    {
		       $sub_sql = "";
		       $page    = (isset($options['page']) ? $options['page'] : "");	       
		       if(isset($useraccess[$page]))
		       {
		           foreach($useraccess[$page] as $index => $val)
		           {
                      $sub_sql .= " RR.sub_id = ".$val." OR";
		           }
		           if(!empty($sub_sql))
		           {
		              $view_access_sql .= " AND (".rtrim($sub_sql, "OR").") ";
		           }		       
		       } else {
		         $view_access_sql .= " AND RR.id = 0";
		       }
		    } else {
		      $view_access_sql .= " AND RR.id = 0";
		    }
		    //$view_access_sql .= " OR RR.insertuser = '".$_SESSION['tid']."' )";              
		}
		if(isset($options['financial_year']) && !empty($options['financial_year']))
		{
		    $view_access_sql .= " AND RR.financial_year = '".$options['financial_year']."' ";
		}
		if(isset($options['query_owner']) && !empty($options['query_owner']))
		{
		    $view_access_sql .= " AND RR.sub_id = '".$options['query_owner']."' ";
		}		
		if(isset($options['query_id']) && !empty($options['query_id']))
		{
		    $view_access_sql .= " AND RR.id = '".$options['query_id']."' ";
		}
		
		if(isset($options['section']))
		{
           if($options['section'] == "new")
           {
             $view_access_sql = " AND RR.active & ".Risk::ACTIVATED." <> ".Risk::ACTIVATED." ";
           } else { 
		     $view_access_sql .= " AND RR.active & ".Risk::ACTIVATED." = ".Risk::ACTIVATED." ";
		   }
		}
		return $view_access_sql;
	}	
	
	/**
		Fetches all the risk in detail 
		@return array;
	**/
	function getQueries($options = array())
	{
	    $columnObj   = new QueryColumns($options['section']);
	    $columns     = $columnObj -> getHeaderList();
	    $actionObj   = new RiskAction("", "", "", "", "", "", "", "", "");
        $userObj 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess  = $userObj -> getUser($_SESSION['tid']);
		$diradminObj = new Administrator($_SESSION['tid']);
		$useraccess  = $diradminObj -> getAdminAccess();
		$option_sql  = $this -> _setViewAccess($useraccess , $options);

		$total       = $this -> getTotalQueries($option_sql);

		
		if(isset($options['limit']) && !empty($options['limit']))
		{
		  $option_sql .= " LIMIT ".$options['start'].",".$options['limit']." "; 
		}
		$queries      = $this -> fetchAll($option_sql);

		$queryActions = $actionObj -> fetchAll();
		$default  	  = new Defaults("","");
		$defaults 	  = $default -> getDefaults();
		$res 		  = new ResidualRisk( "", "", "", "", "");
		$ihr 		  = new InherentRisk("", "", "", "", "");
		$colored  	  = array("query_status_color" => "query_status",
		                      "impact_rating"      => "IMrating",
		                      "likelihood_rating"  => "LKrating");
        $additionals = array("finding", "recommendation", "client_response", "auditor_conclusion", "internal_control_deficiency");

		$data =  $this -> _sortQuery($queries, $total, $options);
	    		
        return $data;
	}
	
	function getActionQueries($options = array())
	{
	    $columnObj   = new QueryColumns($options['section']);
	    $columns     = $columnObj -> getHeaderList();
	    $actionObj   = new RiskAction("", "", "", "", "", "", "", "", "");
        $userObj 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess  = $userObj -> getUser($_SESSION['tid']);
		$diradminObj = new Administrator($_SESSION['tid']);
		$useraccess  = $diradminObj -> getAdminAccess();
		if($options['page'] == "edit_actions")
		{
		    $options['page'] = "edit";
		} elseif($options['page'] == "update_actions"){
		    $options['page'] = "update";
		}
		
		
		$option_sql  = $this -> _setViewAccess($useraccess , $options);
		$results     = $this -> fetchAll($option_sql);
		$queries     = array();
        foreach($results as $index => $query)
        {
           $shortDescription = "";
           if(Util::isTextTooLong($query['description']))
           {    
             $shortDescription = Util::cutText($query['description'], $query['id']."_description");
           } else {
             $shortDescription = $query['description'];
           }
           $queries[$query['id']]                      = $query;
           $queries[$query['id']]['query_description'] = "[".Risk::REFTAG."".$query['id']."]     ".$shortDescription;
        }
       return $queries;
	}
	
	function getActivationQueries($options = array())
	{ 
	    if(isset($options['section']))
	    {
	       $options['section'] = "";
	    }          
        $awaiting_sql = " AND RR.active & ".Risk::ACTIVATED." <> ".Risk::ACTIVATED." ";
		$awaiting     = $this -> _sortQuery($this -> fetchAll($awaiting_sql), 0, $options);
				
	    $activated_sql = " AND RR.active & ".Risk::ACTIVATED." = ".Risk::ACTIVATED." ";
	    $activated     = $this -> _sortQuery($this -> fetchAll($activated_sql), 0, $options); 
        return array('awaiting' => $awaiting, 'activated' => $activated);
	}	
	
	function activateQuery($options = array())
	{   
	   $res      = 0; 
	   if(isset($options['query_id']) && !empty($options['query_id']))
	   {
	      $query       = $this -> getQueryDetail($options['query_id']);
	      $update_data = array();
	      if(($query['active'] & Risk::ACTIVATED) != Risk::ACTIVATED)
	      {
	        $update_data              = array('active' => $query['active'] + Risk::ACTIVATED);
	        if(isset($options['response']) && !empty($options['response']))
	        {
               $changes['response']       = $options['response'];	            
	        }
	        $changes['message']       = Risk::OBJECT_NAME." ".Risk::REFTAG.$options['query_id']." activated";
	        $changes['user']          = $_SESSION['tkn'];
	        $changes['currentstatus'] = "New";
	        $updates['changes']       = base64_encode(serialize($changes));
	        $updates['risk_id']       = $options['query_id'];
	        $updates['insertuser']    = $_SESSION['tid'];
	        $res                      = $this -> updateQuery($options['query_id'], $update_data, $updates);
	      }
	   }
	   return $res;
	}

	function editQuery($query_id, $update_data, $insert_data)
	{
	    $res = 0;
	    if(!empty($update_data))
	    {
	       $res += $this -> update('query_register', $update_data, "id='".$query_id."'");
	    }
	    if(!empty($insert_data))
	    {
	       $res += $this -> insert("risk_edits", $insert_data);
	    }
		return $res;
	}
	
	function updateQuery($query_id, $update_data, $insert_data)
	{
	    $res = 0;
	    if(!empty($update_data))
	    {
	       $res += $this -> update('query_register', $update_data, "id='".$query_id."'");
	    }
	    if(!empty($insert_data))
	    {
	       $res += $this -> insert("risk_update", $insert_data);
	    }
		return $res;
	}
		

	function _sortQuery($queries, $total = 0, $options)
	{
	    $columnObj    = new QueryColumns($options['section']);
	    $columns      = $columnObj -> getHeaderList();
	    $actionObj    = new RiskAction("", "", "", "", "", "", "", "", "");
        $userObj 	  = new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess   = $userObj -> getUser($_SESSION['tid']); 
		$diradminObj  = new Administrator($_SESSION['tid']);
		$useraccess   = $diradminObj -> getAdminAccess();
		$queryActions = $actionObj -> fetchAll();
		$default  	  = new Defaults("","");
		$defaults 	  = $default -> getDefaults();
		$res 		  = new ResidualRisk( "", "", "", "", "");
		$ihr 		  = new InherentRisk("", "", "", "", "");
		$colored  	  = array("query_status_color" => "query_status",
		                      "impact_rating"      => "IMrating",
		                      "likelihood_rating"  => "LKrating");
        $additionals = array("finding", "recommendation", "client_response", "auditor_conclusion", "internal_control_deficiency");
        $data['headers']      = array();
		$data['queries']      = array();
		$data['total']        = $total;
		$data['actions']      = array();
		$data['canEdit']      = array();
		$data['canUpdate']    = array();
		$data['canAddAction'] = array();
		foreach($queries as $index => $query)
		{
		    $totalProgress  = 0;
		    $totalActions   = 0;
		    $queryProgress  = 0;
 		    if(!empty($queryActions))
		    {
		       $actionsProgress = 0; 
		       foreach($queryActions as $action_id => $action)
		       {
		          if($action['risk_id'] == $query['id'])
		          {
		              $data['actions'][$query['id']][$action_id] = $action; 
		              $totalActions                             += 1;
		              $actionsProgress                          += $action['progress'];		            
		          }
		       }
		       if($totalActions != 0)
		       {
		          $queryProgress = ($actionsProgress / $totalActions);
		       }
		       $queryProgress  = ASSIST_HELPER::format_percent($queryProgress);
		    }
		    if(isset($useraccess['update']))
		    {
		       if(isset($useraccess['update'][$query['sub_id']]))
		       {
		         $data['canUpdate'][$query['id']] = TRUE; 
		       } else {
		         if(isset($options['section']) == "admin")
		         {
		           $data['canUpdate'][$query['id']] = TRUE; 
		         } else {
		            $data['canUpdate'][$query['id']] = FALSE;  
		         }		         
		       }
		    }
		    if(isset($useraccess['edit']))
		    {
		       if(isset($useraccess['edit'][$query['sub_id']]))
		       {
		         $data['canEdit'][$query['id']] = TRUE; 
		       } else {
		         if(isset($options['section']) == "admin" && $userAccess['edit_all'] == 1)
		         {
		            $data['canEdit'][$query['id']] = TRUE; 
		         } else {
		            $data['canEdit'][$query['id']] = FALSE; 
		         }
		       }
		    }
		    if(isset($useraccess['addaction']))
		    {
		       if(isset($useraccess['addaction'][$query['sub_id']]))
		       {
		         $data['canAddAction'][$query['id']] = TRUE; 
		       } else {
		         if(isset($options['section']) == "admin" && $userAccess['edit_all'] == 1)
		         {
		            $data['canAddAction'][$query['id']] = TRUE; 
		         } else {
		            $data['canAddAction'][$query['id']] = FALSE; 
		         }
		       }
		    }
		    
		   	foreach($columns as $field => $value) 
			{ 
		        if($field == "query_progress")
		        {
		           $data['queries'][$query['id']][$field] = $queryProgress;
		           $data['headers'][$field]               = $value;
		        } elseif($field == "attachements") {
				  $attachment                            = "";
				  $attachment                            = Attachment::getAtachmentsList($query['attachment'], 'query', FALSE);
				  $data['queries'][$query['id']][$field] = $attachment;
				  $data['headers'][$field]               = $value;
		        } elseif($field == "query_item") {
				  $data['queries'][$query['id']][$field] = Risk::REFTAG."".$query[$field];
				  $data['headers'][$field]               = $value;
		        } elseif($field == "query_description"){
                    if(Util::isTextTooLong($query['description']))
                    {
                        $text = Util::cutString($query['description'], $field."_".$query['id']);
		                $data['queries'][$query['id']][$field] = $text;
		                $data['headers'][$field]               = $value;	
                    } else {
		               $data['queries'][$query['id']][$field] = $query['description'];
		               $data['headers'][$field]               = $value;	                        
                    }
	        
		        } elseif(isset($query[$field])) {
                    if(Util::isTextTooLong($query[$field]))
                    {
                        $text = Util::cutString($query[$field], $field."_".$query['id']);
 		                $data['queries'][$query['id']][$field] = $text;
		                $data['headers'][$field]               = $value;
                    } else {
		               $data['queries'][$query['id']][$field] = $query[$field];
		               $data['headers'][$field]               = $value;                         
                    }  
		        }
		    }
		}
		if(empty($data['headers']))
		{
		  $data['headers'] = $columns;
		}
		if(empty($data['queries']))
		{
		    $msg = "";
		    if(isset($options['query_id']) && !empty($options['query_id']))
		    {
		       $msg = "There are no ".Risk::PLURAL_OBJECT_NAME." that matched the entered criteria";
		    } else {
		        if(isset($options['page']))
		        {
		            if($options['page'] == "edit")
		            {
		               $msg = "You do not have access to edit any ".Risk::PLURAL_OBJECT_NAME."";
		            } elseif($options['page'] == "update") {
		              $msg = "You do not have any outstanding ".Risk::PLURAL_OBJECT_NAME." to update";
		            } else {
		               $msg = "There are no ".Risk::PLURAL_OBJECT_NAME." that matched the selected criteria";
		            }
		        } else {
                    $msg = "There are no ".Risk::PLURAL_OBJECT_NAME."";
		        }		        
		    }
		    $data['query_message'] = $msg;
		}
		$data['columns'] = count($data['headers']);
        return $data;
	}
	
	function getQueryActivityLog($query_id)
	{
        $columnObj   = new QueryColumns();
        $columns     = $columnObj -> getHeaderList();
        
	    $query_edits = $this -> get("SELECT id, risk_id, changes, insertdate FROM #_risk_edits WHERE risk_id = '".$query_id."' ");
	    $queryedits  = self::logs($query_edits, $columns);
	    
	    $query_updates = $this -> get("SELECT id, risk_id, changes, insertdate 
	                                   FROM #_risk_update WHERE risk_id = '".$query_id."' 
	                                  ");
	    $queryupdates  = self::logs($query_updates, $columns);
        
	    $logs = array_merge($queryupdates, $queryedits);
	    uasort($logs, function($a, $b){
	        return ($a['key'] > $b['key'] ? 1 : -1);
	    });
        return $logs;
	}
	
	public static function logs($logs, $columns)
	{
        $changes    = array();
	    if(!empty($logs))
	    {
	       foreach($logs as $l_index => $log)
	       {
	          $key                         = strtotime($log['insertdate']);
	          $changes[$key]['key']        = $key;
	          $changes[$key]['date_loged'] = date("d-M-Y H:i:s", $key);
	          $changesArr                  = @unserialize(base64_decode($log['changes']));
	          if(!empty($changesArr))
	          {
		          if(isset($changesArr['user']))
		          {
		              $changes[$key]['user'] = $changesArr['user'];
		              unset($changesArr['user']);
		          }
		          if(isset($changesArr['currentstatus']))
		          {
		             $changes[$key]['status'] = $changesArr['currentstatus'];
		             unset($changesArr['currentstatus']);
		          }
		          
		          //extract other changes
		          if( isset($changesArr) && !empty($changesArr))
		          {
		              $message = self:: _getChanges($changesArr, $columns);
		              if(!empty($message))
		              {
		                $changes[$key]['changeMessage']  = $message;
		              } else {
		                unset($changes[$key]);
		              }
		          } else {
		              $changes[$key]['changeMessage'] = "";
		          }		          
	          }
	       }	    
	    }
	    return $changes;
	}
	
    public static function _getChanges($changesArr, $columns)
    {
    	$changeMessage = "";
	    foreach($changesArr as $index => $valArr)
	    {
		    if( $index == "user")
		    {
		 	   continue;
		    } else if($index == "attachment") {
		        if(is_array($valArr))
		        {
			       /*
			       foreach($valArr as $aKey => $aVal)
			       {
			          if(!is_array($aVal))
			          {
                         
			             $changeMessage .= $aVal."<br />";
			          }
			       }*/
			       $changeMessage .= Attachment::displayAttachments($valArr);
			    } else {
			        $changeMessage .= $valArr."<br />";
			    }
	        } else if($index == "response") {
	            if( is_array($valArr)) 
	            {	
	                if( isset($valArr['to']) && isset($valArr['from']))
	                {
		               $key_str = "";
		               if(isset($columns[$index]))
		               {
		                    $key_str = $columns[$index];
		               } else {
		                    $key_str = ucwords(str_replace("_", " ", $index));
		               }	                    
		               $changeMessage .= $key_str." was changed to <span class='change'><i>".$valArr['to']."</i></span> from <span><i>".$valArr['from']."</i></span><br />";
	                } else {
		               $changeMessage .= "<span class='change'><i>".$valArr."</i></span><br />";
	                }
	            } else {
	               $changeMessage .= "Added response : <span class='change'><i>".$valArr."</i></span><br />";
	            }
	        } else {			
		        if(is_array($valArr))
		        {
		           if(array_key_exists("to", $valArr) && array_key_exists("from", $valArr))
		           {
		             $key_str = "";
		             if(isset($columns[$index]))
		             {
		                $key_str = $columns[$index];
		             } else {
		                $key_str = ucwords(str_replace("_", " ", $index));
		             }
			         $changeMessage .= $key_str." was changed <b>to</b> <span class='change'><i>".$valArr['to']."</i></span> <b>from</b> <span class='change'><i>".$valArr['from']."</i></span><br />";
		          } else {
			         $changeMessage .= self::_getChanges($valArr , $columns);		
		          } 
		        } else {
			        $changeMessage .= "<span class='change'><i>".$valArr."</span></i><br />";
		        }
	        }		
	    }	
	  return $changeMessage;
    }	
	
	function getRiskChange($postData, $queryData)
	{
       $colObj       = new QueryColumns();
       $rowNames     = $colObj -> getHeaderList();
       $qTypeObj     = new QueryType( "", "", "", "" );
       $types	 	 = $qTypeObj -> getActiveRiskType();	
       $risktypeObj  = new RiskType( "", "", "", "" );
       $risktypes    = $risktypeObj -> getType();
       $finObj 		 = new FinancialExposure( "", "", "", "", "");
       $finExposure  = $finObj -> getFinancialExposure();
       $levelObj     = new RiskLevel( "", "", "", "", "","");
       $level 	     = $levelObj -> getLevel();
       $statusObj 	 = new RiskStatus( "", "", "" );
       $statuses 	 = $statusObj -> getStatus();
       $riskcat 	 = new RiskCategory( "", "", "", "" );
       $categories   = $riskcat  -> getCategory();
       $sourceObj    = new QuerySource();       
       $querySources = $sourceObj -> getQuerySources();             
	   $data['changes']        = array();
	   $data['change_message'] = "";
	   $data['update_data']    = array();
	   $msg                    = "";

       foreach($queryData as $field => $val)
       {
           if(isset($postData[$field]))
           {
               if($field == "status")
               {
                    $from = (isset($statuses[$queryData['status']]) ? $statuses[$queryData['status']]['name'] : " - ");
                    $to   = (isset($statuses[$postData['status']]) ? $statuses[$postData['status']]['name'] : " - ");             
                    if($from != $to)
                    { 
                      $data['changes'][$field]     = array("from" => $from, "to" => $to);
                      $msg                         .= ucwords('query_status')." changed to ".$to." from ".$from."\r\n\n";
                      $data['update_data'][$field] = $postData['status']; 
                      $data['changes']['currentstatus'] = $to;
                   } 
               } elseif($field == "risktype") {    
	              $from  = (!empty($queryData['risk_type']) ? $queryData['risk_type'] : " - ");
	              $to    = (isset($risktypes[$postData[$field]]) ? $risktypes[$postData[$field]]['name'] : " - ");
	              if($from !== $to)
	              { 
                     $data['changes']['risk_type']     = array("from" => $from, "to" => $to);
                     $msg                             .= ucwords('risk_type')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['risk_type'] = $postData[$field]; 
	              }
	           } elseif($field == "financialexposure") {
	              $from = (!empty($queryData['financial_exposure']) ? $queryData['financial_exposure'] : " - ");
	              $to   = (isset($finExposure[$postData[$field]]) ? $finExposure[$postData[$field]]['name'] : " - ");
	              if($from !== $to)
	              { 
                     $data['changes']['financial_exposure'] = array("from" => $from, "to" => $to);
                     $msg                                  .= ucwords('financial_exposure')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['financial_exposure']= $postData[$field]; 
	              }
	           } elseif($field == "risklevel") {
	              $from = (!empty($queryData['risk_level']) ? $queryData['risk_level'] : " - ");
	              $to   = (isset($level[$postData[$field]]) ? $level[$postData[$field]]['name'] : " - ");
	              if($from !== $to)
	              { 
                     $data['changes']['risk_level']     = array("from" => $from, "to" => $to);
                     $msg                              .= ucwords('risk_level')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['risk_level'] = $postData[$field]; 
	              }
	           } elseif($field == "type") {
	              $from = (!empty($queryData['query_type']) ? $queryData['query_type'] : " - ");
	              $to   = (isset($types[$postData[$field]]) ? $types[$postData[$field]]['name'] : " - ");
	              if($from != $to)
	              { 
                     $data['changes']['query_type']  = array("from" => $from, "to" => $to);
                     $msg                            = ucwords('query_type')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['type']    = $postData[$field]; 
	              }
	           } elseif($field == "category") {
	              $from = (!empty($queryData['query_category']) ? $queryData['query_category'] : " - ");
	              $to   = (isset($categories[$postData[$field]]) ? $categories[$postData[$field]]['name'] : " - ");
	              if($from !== $to)
	              { 
                     $data['changes']['query_category'] = array("from" => $from, "to" => $to);
                     $msg                              .= ucwords('query_category')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['category']   = $postData[$field]; 
	              }
	           } elseif($field == "querysource") {
	              $from = (!empty($queryData['query_source']) ? $queryData['query_source'] : " - ");
	              $to   = (isset($querySources[$postData[$field]]) ? $querySources[$postData[$field]]['name'] : " - ");
	              if($from !== $to)
	              { 
                     $data['changes']['query_source'] = array("from" => $from, "to" => $to);
                     $msg                                  .= ucwords('query_source')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['query_source']= $postData[$field]; 
	              }
	           } elseif($field == "remind_on"){
                   $from   = (strtotime($queryData['remind_on']) == 0 ? "-" : date("d-M-Y", strtotime($queryData['remind_on'])) );
                   $to     = (strtotime($postData['remind_on']) == 0 ? "-" : date("d-M-Y", strtotime($postData['remind_on'])) );
                   if(strtotime($from) !== strtotime($to))
                   {
                     $data['changes']['remind_on']     = array('from' => $from, 'to' => $to);
                     $msg                             .= "Remind On changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['remind_on'] = date('Y-m-d', strtotime($_POST['remind_on']));
                   }
	            } elseif($field == "user_responsible"){
	              $from = (!empty($queryData['query_owner']) ? $queryData['query_owner'] : " - ");
	              $to   = $postData['user_responsible_text'];
	              if($from !== $to)
	              { 
                     $data['changes']['query_owner'] = array("from" => $from, "to" => $to);
                     $msg                                  .= ucwords('query_owner')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data']['sub_id']= $postData[$field]; 
	              }
	            } elseif($postData[$field] != $val) {
                    $from                        = $val;
                    $to                          = $postData[$field];
                    $data['changes'][$field]     = array("from" => $from, "to" => $to);
                    $msg                        .= ucwords($field)." changed to ".$to." from ".$from."\r\n\n";
                    $data['update_data'][$field] = $postData[$field]; 
               }
           }
       }
       /*
	   foreach($queryData as $field => $val)
	   {
           $from = $to = "";
	       if($field == "risktype")
	       {    
	          $from  = (!empty($queryData['risk_type']) ? $queryData['risk_type'] : " - ");
	          $to    = (isset($risktypes[$postData['risk_type']]) ? $risktypes[$postData['risk_type']]['name'] : " - ");
	          if($from !== $to)
	          { 
                 $data['changes']['risk_type']     = array("from" => $from, "to" => $to);
                 $msg                             .= ucwords('risk_type')." changed to ".$to." from ".$from."\r\n\n";
                 $data['update_data']['risk_type'] = $postData['risk_type']; 
	          }
	       } elseif($field == "risklevel") {
	          $from = (!empty($queryData['risk_level']) ? $queryData['risk_level'] : " - ");
	          $to   = (isset($level[$postData['risk_level']]) ? $level[$postData['risk_level']]['name'] : " - ");
	          if($from !== $to)
	          { 
                 $data['changes']['risk_level']     = array("from" => $from, "to" => $to);
                 $msg                              .= ucwords('risk_level')." changed to ".$to." from ".$from."\r\n\n";
                 $data['update_data']['risk_level'] = $postData['risk_level']; 
	          }
	       }  elseif(isset($postData[$field])){
	          if($field == "status")
	          {
	              $from = (isset($statuses[$queryData['status']]) ? $statuses[$queryData['status']]['name'] : " - ");
	              $to   = (isset($statuses[$postData['status']]) ? $statuses[$postData['status']]['name'] : " - ");             
	              if($from != $to)
	              { 
                     $data['changes'][$field]     = array("from" => $from, "to" => $to);
                     $msg                         .= ucwords('query_status')." changed to ".$to." from ".$from."\r\n\n";
                     $data['update_data'][$field] = $postData['status']; 
                     $data['changes']['currentstatus'] = $to;
	              } 
	          } elseif($postData[$field] != $val) {
	             $from                        = $val;
	             $to                          = $postData[$field];
                 $data['changes'][$field]     = array("from" => $from, "to" => $to);
                 $msg                        .= ucwords($field)." changed to ".$to." from ".$from."\r\n\n";
                 $data['update_data'][$field] = $postData[$field]; 
	          }
	       } 
	   }
	   */
       $att_change_str = Attachment::processAttachmentChange($queryData['attachment'], "query_".$queryData['query_item'], 'query');

	   if(!empty($_SESSION['uploads']['attachments']))
	   {
	      $attachments = $_SESSION['uploads']['attachments'];		
	   }
       if(isset($_SESSION['uploads']))
       {
           if(isset($_SESSION['uploads']['actionchanges']))
           {
             $data['changes']['attachment'] = $_SESSION['uploads']['actionchanges'];
             $msg                          .= $att_change_str;
           }
           if(isset($_SESSION['uploads']['attachments']))
           {
              if($queryData['attachment'] != $_SESSION['uploads']['attachments'])
              {
                 $data['update_data']['attachment'] = $_SESSION['uploads']['attachments'];
              }
           }
       }
	   if(isset($postData['response']) && !empty($postData['response']))
	   {
	      $data['changes']['response'] = $postData['response'];
	      $msg                        .= "Added response ".$postData['response'];
	   }
  
	   if(!empty($data['changes']))
	   {
	      $data['changes']['currentstatus'] = (isset($statuses[$queryData['status']]) ? $statuses[$queryData['status']]['name'] : " - ");
	      $data['changes']['user']          = $_SESSION['tkn'];
	      $data['change_message']           = $msg;
	   } 
       Attachment::clear("qryadded", 'query_'.$queryData['query_item']);
       Attachment::clear("qrydeleted", 'query_'.$queryData['query_item']);
	   return $data;
	}
	
    function getRiskTable($action_id)
    {
       $actionObj = new RiskAction("", "", "","" , "", "", "", "", "");
       $action    = $actionObj -> getActionDetail($action_id);
       $query     = $this -> getQueryDetail($action['risk_id']);
       
       $namingObj = new QueryColumns();
       $columns   = $namingObj -> getHeaderList();
       
       $table     = "<table width='100%'>";
         foreach($columns as $field => $header)
         {
            if(isset($query[$field]))
            {
                $table .= "<tr>";
                  $table .= "<th class=left>".$header.":</th>";
                  $table .= "<td>".($field=="query_item" ? Risk::REFTAG : "").$query[$field]."</td>";
                $table .= "</tr>";
            }
         }
       $table    .= "</table>";
       return $table;
    }
	
	function  saveRiskUpdate( $id , $status, $response, $changes)
	{
		$insertdata = array(
						'financial_exposure' 				=> $this -> financial_exposure,
						'monetary_implication' 				=> $this -> monetary_implication,
						'risk_detail'						=> $this -> risk_detail,
						'risk_type'							=> $this -> risk_type,
						'finding'							=> $this -> finding,						
						'recommendation' 					=> $this -> recommendation,
						'client_response' 					=> $this -> client_response,
						'internal_control_deficiency'		=> $this -> internal_control_deficiency,
						'auditor_conclusion'				=> $this -> auditor_conclusion,		
						'risk_level' 						=> $this -> risk_level,
						'attachment' 						=> $this -> attachement	,	
						'status'							=> $status					
		);
		$responseUpdate  = $this -> update( 'query_register' , $insertdata, "id = $id");
		$response  		 = $this -> insert( 'risk_update' , array(
															"risk_id" 			=> $id,
															"response"			=> $response,
															"changes"			=> $changes,
                                                            'status'			=> $status
														) 
											);
		return $this -> insertedId();
	}
	
	function getRiskUpdate( $id )
	{  								  
		$response = $this -> getRow( " SELECT status, name, color
									   FROM ".$_SESSION['dbref']."_query_register RU
									   INNER JOIN ".$_SESSION['dbref']."_status S ON RU.status = S.id
									   WHERE RU.id = '".$id."' 
									  " );
		return $response;
	}
	
	function getAllRisk( $start , $limit)
	{
		$sqlLimit = "";
		if( $start !== "" && $limit !== "") {
			$sqlLimit = "LIMIT $start , $limit";
		} else {
			$sqlLimit = "";
		}
		$uaccess 		= new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess     = $uaccess -> getUser( $_SESSION['tid'] );
		$userRiskRef     = $uaccess -> getUserRefDirectorate();
		$viewAccess 	= $this -> _setViewAccess( $userAccess , (isset($_GET['page']) ? $_GET['page'] : "") , $userRiskRef );
		$response = $this -> get( "
							SELECT DISTINCT(RR.id) AS query_item,
							RR.description AS query_description,
							RR.background AS query_background,
							FE.name AS financial_exposure,
							RR.monetary_implication, 
							RR.risk_detail, 
							RR.finding,
							RR.query_date,
							RR.query_deadline_date,
							RL.name AS risk_level,
							RRT.name AS risk_type,
							RR.recommendation , 
							RR.client_response,
							RR.internal_control_deficiency,
							RR.auditor_conclusion,		
							RC.name as query_category, 
							RC.description as cat_descr, 
							D.dirtxt  AS query_owner,
							RT.name AS query_type,
							RS.color AS risk_status
							FROM ".$_SESSION['dbref']."_query_register RR 
							LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status							
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = RR.risk_level	
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure			
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE RR.active = 1 $viewAccess ORDER BY RR.id asc $sqlLimit
							");
		$totalrisk = $this -> totalRisk( $viewAccess  );
		$nm 			= new Naming();
		$default  		= new Defaults("","");
		$defaults 		= $default -> getDefaults();
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");		
		$data = array();
		$riskArray = array();
		foreach( $response as $key => $risk )
		{
			$updateArray 	= $this -> getRiskUpdate( $risk['query_item'] );		
			$riskArray[$risk['query_item']] = array(
							"description"		=> $risk['query_description'],
							"background"		=> $risk['query_background'],
							"financial_exposure"=> $risk['financial_exposure'],
							"monetary_implication"	=> $risk['monetary_implication'],
							"risk_detail"		=> $risk['risk_detail'],
							"finding"			=> $risk['finding'],
							"recommendation"	=> $risk['recommendation'],
							"client_response"	=> $risk['client_response'],
							"internal_control_deficiency" => $risk['internal_control_deficiency'],
							"auditor_conclusion"=> $risk['auditor_conclusion'],	
							"category"			=> $risk['query_category'],
							"catDescr"			=> $risk['cat_descr'],
							"type"				=> $risk['query_type'],
                            "risk_type"			=> $risk['risk_type'],
							"tkname"			=> $risk['query_owner'],
							"status"			=> (empty($updateArray) ? "New" : $updateArray['name']),
							"rscolor"			=> (empty($updateArray) ? "" : $updateArray['color']),																																										
						);

			}
		$cols = $nm -> getNameColumn();

		$data = array( "total" => $totalrisk, "riskData" => $riskArray, "headers" => $nm -> getNameColumn() );
		return $data;
	}
	
	function getRiskEdits( $id )
	{
	
		$response = $this -> get(  "
							SELECT							
							RR.id AS query_item,
							RR.changes,
							S.name AS query_status,
							RR.insertdate						
							FROM ".$_SESSION['dbref']."_risk_edits RR
							INNER JOIN ".$_SESSION['dbref']."_query_register QR ON QR.id = RR.risk_id
                            INNER JOIN ".$_SESSION['dbref']."_status S ON S.id = QR.status
							WHERE RR.risk_id = '".$id."' ORDER BY RR.insertdate DESC 
							");
		return $response;
	}
	/**
		Fetches a specified risk id 
		@param risk id
		@return boolelan of ids
	**/
	function _isRiskOwner( $id , $riskid)
	{
		$response = $this -> getRow("SELECT id 
								  FROM ".$_SESSION['dbref']."_query_register 
								  WHERE risk_owner = '".$id."' AND id = '".$riskid."' AND active = 1 "
								  ); 
		if( isset($response['id']) && !empty($response['id'])) {
			//echo "This risk id ".$riskid." the person is the owner \r\n\n\n";
			return TRUE;
		} else {
			//echo "This risk id ".$riskid." the person is the action owner only \r\n\n\n";		
			return FALSE;
		}						  
	}
	/**
	 Creates a string of comma separated risk ids the users has actions assoociated to it
	 @param array  , that has an index of risk id
	 @return string
	**/
	function _setRiskIds( $risIdkArray )
	{
		$riskids = "";
		if( is_array( $risIdkArray )) 
			foreach( $risIdkArray as $key => $valueArray ) 
			{
				$riskids .= "'".$valueArray['risk_id']."',";
			}
		$riskids = rtrim( ltrim( rtrim($riskids, ","), "'") , "'");
		return $riskids;
	}


	/**
		Fetches the total risks available 
		@return int
	**/
	function totalRisk( $viewAccess)
	{

		$response = $this -> getRow( "
							SELECT COUNT( DISTINCT(RR.id) ) as total
							FROM ".$_SESSION['dbref']."_query_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							INNER JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid							
							WHERE RR.active = 1 $viewAccess  
							" );
		return $response['total'];
	}
	/**
		Fetches detailed information about a single risk
		@return array
	**/
	function getRisk($id)
	{	
		$where = "";
		if($id == "") 
		{
			$where = "WHERE 1";	
		} else {
			$where = "WHERE RR.id = $id"; 
		}			
		
		$response = $this -> getRow("SELECT RR.*, RC.name as category, RC.id as categoryid,	RC.description as cat_descr,
							         RT.name as type, RT.id as typeid, RT.shortcode as type_code, D.dirtxt  AS risk_owner,
							         RS.name AS status, RS.id as statusId, RS.color AS rscolor
							         FROM ".$_SESSION['dbref']."_query_register RR 
							         LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							         LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							         LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = RR.risk_type
							         LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							         LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = RR.risk_level	
							         LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure	
							         LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							         LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							         LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							         $where
							        " );
		return $response ;
	}
	/**
		Fetches limited infomation about the risk of specified id
		@return array
	**/
	function getARisk( $id )
	{
		$response = $this -> getRow ( "SELECT * FROM #_query_register WHERE id = '".$id."'" );
		return $response;
	}
	
	function updateRisk( $id, $changes )
	{
		$insertdata = array(
			'type'   							=> $this -> type,
            'risk_type'							=> $this -> risk_type,
			'category' 							=> $this -> category,
			'description' 						=> $this -> description,
			'background'						=> $this -> background ,
			'query_reference' 					=> $this -> query_reference,
			'financial_exposure'				=> $this -> financial_exposure,
			'monetary_implication'				=> $this -> monetary_implication,
			'query_date' 						=> $this -> query_date,
			'query_deadline_date' 				=> $this -> query_deadline_date,
			'finding'							=> $this -> finding,			
			'risk_detail'						=> $this -> risk_detail,					
			'internal_control_deficiency'		=> $this -> internal_control_deficiency,
			'auditor_conclusion'				=> $this -> auditor_conclusion,				
			'recommendation' 					=> $this -> recommendation,
			'client_response' 					=> $this -> client_response,
			'risk_level' 						=> $this -> risk_level,						
			'attachment' 						=> $this -> attachement,
			'sub_id' 							=> $this -> risk_owner,
		);
		$response  	  = $this -> update( 'query_register' , $insertdata , "id = $id");
		$responseEdit = $this -> insert( 'risk_edits' ,array("risk_id" => $id,'changes'=> $changes) );		
		return $response;
	}
	
	function deleteRisk( $id )
	{
		// send email notifying of the delete
		$updatedata = array( 
							'active' 		=> 0,
							'insertuser'	=> $_SESSION['tid']
							);
		$response  = $this -> update( 'query_register' , $updatedata , "id = $id");
		return $response;
	}
	
	function getRiskUpdates( $id )
	{	
	
	 $response = $this -> get( "SELECT 
	 							RU.id AS query_item,
								RU.changes,
								S.name AS query_status,
	 							RU.response,
								RU.insertdate,
								RU.insertuser,
								TK.tkname,
								TK.tksurname
	 							FROM ".$_SESSION['dbref']."_risk_update RU
	 							INNER JOIN ".$_SESSION['dbref']."_query_register QR ON QR.id = RU.risk_id
	 							INNER JOIN ".$_SESSION['dbref']."_status S ON S.id = RU.status
								LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = RU.insertuser
								WHERE RU.risk_id = '".$id."' ORDER BY RU.insertdate DESC ,RU.id DESC						
	  " );
	  return $response; 
	}
	
	 static function getStatusSQLForWhere($a) 
     {
        $sql = "(
                        ".$a.".active & ".Risk::ACTIVATED. " = ".Risk::ACTIVATED. "
                        
        )";
        return $sql;
     }
}
?>