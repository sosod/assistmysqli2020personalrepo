<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} elseif( file_exists( "../../".strtolower($classname).".php" ) ) {
		require_once( "../../".strtolower($classname).".php" );		
	} elseif(file_exists("../../library/dbconnect/".strtolower($classname).".php")) {
		require_once( "../../library/dbconnect/".strtolower($classname).".php" );		
	} elseif("../../library/class/assist_helper.php") {
       require_once( "../../library/class/assist_helper.php" );		
	}  else {
		require_once( "../../library/".strtolower($classname).".php" );		
	}
}
?>
