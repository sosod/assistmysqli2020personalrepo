<?php 
$scripts = array( 'action_approval.js','jquery.uploadify.v2.1.4.min.js', 'swfobject.js','menu.js',  );
$styles = array( 'colorpicker.css', 'uploadify.css' );
$page_title = "Action Approval";
require_once("../inc/header.php");
?>
<script>
$(function(){
	$("table#new_action_approval").find("th").css({"text-align":"left"})
});
</script>
<div id="newapproval_message" class="message"></div>
<form id="approval-action-form" name="approval-action-form" enctype="multipart/form-data">
	<table align="left" border="1" id="approval_action_table" width="70%">
    	<tr>
       	<td valign="top">
        	<table id="action_information_table" width="100%" border="1">
            	<tr>
                	<th colspan="2">Action Information</th>
                </tr>
            </table>	
        </td>
       	<td valign="top">
        <table align="left" id="new_action_approval" border="1" width="100%">
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <th colspan="2">Approval</th>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td><textarea name="approval_response" id="approval_response" ></textarea></td>            
                </tr>
                <tr>
                    <th>Approval:</th>
                    <td>
                        <select name="approval_signoff" id="approval_signoff">
                            <option value="1">Yes</option>
                            <option value="0">No</option>                    
                        </select>
                    </td>            
                </tr>           
                <tr>
                    <th>Attachment:</th>
                    <td>
                    <input type="file" name="approval_attachment" id="approval_attachment" />
                    <input type="submit" name="attach_another" id="attach_another" value="Attach Another" />
                    </td>            
                </tr> 
                <tr>
                    <th></th>
                    <td><input type="checkbox" name="approval_notification" id="approval_notification" />
                    	Send notification that approved or declined
                    </td>            
                </tr>                   
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="add_newAction_approval" id="add_newAction_approval" value="Save" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="cancel_newAction_approval" id="cancel_newAction_approval" value="Cancel" />
                    </td>            
                </tr>                       
            </table>
        </td>         
        </tr>
        <tr>
        	<td colspan="2" align="left">
            	<?php displayGoBack("",""); ?>
            </td>
        </tr>
    </table>
	<input type="hidden" name="action_id" value="<?php echo $_REQUEST['id']?>" id="action_id"  />
</form>
