<?php
if( ! ini_get('date.timezone') )
{
   date_default_timezone_set('GMT');
} 
@session_start();
include_once("../inc/init.php");
switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) )
{
	
case "getActions":
	$risk = new RiskAction( "" , "", "", "", "", "", "", "", "", "", "", "","");
	echo json_encode($risk -> getQueryActions($_GET));
	break;
case "getActionsUnderAssurance":
	$naming        = new ActionColumns();
	$options       = " AND active & 4 = 4";
	$headers       = $naming -> getHeaderList( $options );
	$act           = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", ""); 
	$actions       = $act -> getRiskActions($_GET['start'], $_GET['limit'], '');
	$total 	       = $act -> _getTotalAction();
	//array( "data" => $response, "total" => $this->_getTotalAction() );
	$actionsArr    = array();
	$headerArr     = array();
	$isOwners      = array();
	$actionStatus  = array();
	$totalProgress = 0;
	foreach($actions as $key => $action)
	{
		$actionsArr[$action['id']]['reference'] = $action['id'];
		$headerArr['reference'] = "Reference #";		
		foreach($headers as $index => $head)
		{
		   if(array_key_exists($index, $action))
		   {
			  if($index == "progress")
			  {
				$actionsArr[$action['id']][$index] = ($action[$index] == "" ? 0 : $action[$index])." % ";
				$headerArr[$index]		           = str_replace(" ", "<br />", $head);					
			  } else if($index == "action_owner"){
				$actionsArr[$action['id']][$index] = $action[$index]."<em>(".$action['department'].")</em>";
				$headerArr[$index]		           = str_replace(" ", "<br />", $head);;					
			  } else {
				$actionsArr[$action['id']][$index] = $action[$index];
				$headerArr[$index]		           = str_replace(" ", "<br />", $head);;
			  }			
		    }		
		}
		$actionStatus[$action['id']] = $action['actionstatus'];
		$totalProgress              += $action['progress'];		
		if($action['owner'] == $_SESSION['tid'])
		{
		   $isOwners[] =  $action['id'];
		}
	}	
	if($total == 0)
	{
	   $avrProgess  = "0 %";
	} else {
	   $avrProgess  = round( ($totalProgress/$total),2)." %";		
	}
	if(empty($headerArr))
	{
	   $headerArr = $headers;
	} 
	echo json_encode( array("headers" => $headerArr, "cols" => count($headerArr), "aveProgress" => $avrProgess, "actions" => $actionsArr, "total" => $total, "isOwner" => $isOwners, "status" => $actionStatus ) );
	break;
	
case "getActionsToApprove":
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	echo json_encode( $act -> getActionToApprove() );
	break;
case "editAttachment":
    $action_id = $_REQUEST['action_id'];
    $attObj    = new Attachment('action_'.$action_id, 'edit_action_attachment_'.$action_id); 
    $results   = $attObj -> upload('action_'.$action_id);
    echo json_encode($results);
    exit();	
break;
case "_uploadAttachment":
    $key       = str_replace("_attachment", "", $_REQUEST['element']);  
    $attObj    = new Attachment($key, $_REQUEST['element']); 
    $results   = $attObj -> upload($key);
    echo json_encode($results);
    exit();
	break;
case "_removeAttachment":
    $key      = str_replace("_attachment", "", $_REQUEST['element']); 
    $file     = $_POST['attachment'].".".$_POST['ext'];
    $attObj   = new Attachment($key, $_REQUEST['element']);
    $response = $attObj -> removeFile($file, $key);
    echo json_encode($response);
    exit();	    
    break;
case "_deleteAttachment":
   $file      = $_POST['attachment'];
   $attObj    = new Attachment($_POST['type']."_".$_POST['id'], $_POST['type']."_".$_POST['id']);
   $response  = $attObj -> deleteFile($file, $_POST['filename']);
   echo json_encode($response);
   exit();
break;
case "updateAttachment":
    $action_id = $_REQUEST['action_id'];
    $attObj    = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id); 
    $results   = $attObj -> upload('action_'.$action_id);
    echo json_encode($results);
    exit();
    /*
	$attObj = new Attachment($_FILES, "action");
	$attObj -> upload("updatedactionupload");		
	*/
break;
case "deleteAttachment":
   $file      = $_POST['attachment'];
   $action_id = $_POST['action_id'];
   $attObj    = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id);
   $response  = $attObj -> deleteFile($file, 'action_'.$action_id);
   echo json_encode($response);
   exit();
   break;
case "removeAttachment":
    $action_id= $_POST['action_id'];
    $file     = $_POST['attachment'].".".$_POST['ext'];
    $attObj   = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id);
    $response = $attObj -> removeFile($file, 'action_'.$action_id);
    echo json_encode($response);
    exit();
    /*
	$filename = $_POST['id'];
	$file	  =  $_POST['file'];
	$att      = new Attachment(array(), "action");
	$att    -> deletedFile( $filename, $file ); 		
	*/
	break;
case "RemoveActionFile":
	$filename = $_POST['id'];
	$file	  =  $_POST['file'];
	$att = new Attachments(array(), "action");
	$att->deletedFile( $filename, $file ); 	
	break;	
case "saveActionAttachment":
    $key       = str_replace("_attachment", "", $_REQUEST['element']);  
    $attObj    = new Attachment($key, $_REQUEST['element']); 
    $results   = $attObj -> upload($key);
    echo json_encode($results);
    exit();
    break;
case "removeActionFile":
    $key      = str_replace("_attachment", "", $_REQUEST['element']); 
    $file     = $_POST['attachment'].".".$_POST['ext'];
    $attObj   = new Attachment($key, $_REQUEST['element']);
    $response = $attObj -> removeFile($file, $key);
    echo json_encode($response);
    exit();	    
    break;
case "deleteActionAttachment":
   $file      = $_POST['attachment'];
   $attObj    = new Attachment($_POST['type']."_".$_POST['id'], $_POST['type']."_".$_POST['id']);
   $response  = $attObj -> deleteFile($file, $_POST['filename']);
   echo json_encode($response);
   exit();
break;	
case "getRisk":
	$act = new Risk("", "", "", "", "", "", "","","", "", "", "", "" );
	echo json_encode( $act -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getARisk":
	$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
	echo json_encode( $risk -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getRiskAction( $_REQUEST['id'] ) );
	break;
case "getAAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getAAction( $_REQUEST['id'] ) );
	break;	
case "getUserAccess":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "");
	echo json_encode( $uaccess -> getUserAccess() );
	break;	
case "getUsers":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
	echo json_encode( $uaccess -> getUsers() );
	break;
case "getStatus":
	$stat = new RiskStatus("", "", "");
	echo json_encode( $stat -> getStatus() );
	break;
case "newRiskAction":
	$act = new RiskAction( $_REQUEST['id'],
						   $_REQUEST["r_action"],
						   $_REQUEST["action_owner"],
						   $_REQUEST["deliverable"],
						   $_REQUEST["timescale"],
						   $_REQUEST["deadline"],
						   $_REQUEST["remindon"],
						   $_REQUEST['progress'],
						   $_REQUEST['status']
						  );
		$res 	   = $act -> saveRiskAction();
		$response  = array();
        if(is_numeric($res))
        {
                if( $res > 0 ) {
                    $response["id"] 			  = $res;
                    $response['success']["saved"] = "Action #".$res." has been successfully saved";
                    $user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
                    $users 			     		  = $user -> getRiskManagerEmails();
                    $responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
                    $risk 						  = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
                    $riskInfo 					  = $risk -> getRisk($_REQUEST['id']);
                    $email 			     		  = new Email();
                    $email -> userFrom   		  = $_SESSION['tkn'];
                    $email -> subject    		  = "New Action";
                    //Object is something like Risk or Action etc.  Object_id is the reference of that object
                    $message  = "";
                    $message .= $email -> userFrom." created an Action relating to query #".$_REQUEST['id'];
                    $message .= " : ".$riskInfo['description']." identified in the Query Register. The details of the action are as follows :";
                    $message .= " \r\n\n";	//blank row above Please log on line
                    $message .= "Action Number : ".$res."\r\n";
                    $message .= "Action : ".$_REQUEST['r_action']."\r\n";
                    $message .= "Deliverable : ".$_REQUEST['deliverable']."\r\n";
                    $message .= "Action Owner : ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
                    $message .= "Time Scale : ".$_REQUEST['timescale']."\r\n";
                    $message .= "Deadline : ".$_REQUEST['deadline']."\r\n";
                    $message .= " \r\n\n";	//blank row above Please log on line
                    $message .= "<i>Please log onto Ignite Assist in order to View or Update the Action.</i> \n";
                    $message  = nl2br($message);
                    $email -> body     = $message;
                    $emailTo 		   = "";
                    foreach($users as $userEmail ) {
                        $emailTo = $userEmail['tkemail'].",";
                    }
                    $email -> to 	   = $emailTo.";".$responsibleUserEmail['tkemail'];
                    $email -> body     = $message;
                    $emailTo 		   = "";
                    if(!empty($user)){
                        foreach($users as $userEmail ) {
                            $emailTo = $userEmail['tkemail'].",";
                        }
                    }
                    $email -> to 	   = $responsibleUserEmail['tkemail'];
                    $email -> from     = "info@ingite4u.com";
                    if($email -> sendEmail()) 
                    {
                        $response['success']["emailsent"] = "and a notification has been sent";
                    } else {
                        $response['error']["emailsent"] = "There was an error sending the email";
                    }
                } else {
                    $response['error']["saved"] = "Error saving the action";
                }
        } else {
            $response = array("error" => true, "text" => $res );
        }
		if(isset($_SESSION['uploads']['action'])){
			unset($_SESSION['uploads']['action']);
		}	        
		echo json_encode( $response );
	break;
case "saveActionAttachement":
		$att = new Attachments($_FILES, "action");		
		$att -> upload();	
	break;
case "deleteAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	$res = $act -> deleteRiskAction( $_REQUEST['id'] );
	$response = array();
	if( $res == 1){
		$response = array("text" => "Action deleted successfully", "error" => false);
	} else if( $res == 0){
		$response = array("text" => "Action deleted successfully", "error" => false);
	} else {
		$response = array("text" => "Error deleting the action", "error" => false);
	}
	echo json_encode( $response );
	break;

case "getEditLog":
	$act 	 		= new RiskAction("", "", "", "", "", "", "", "", "" );
	$editLog 		= $act -> getActionEditLog( $_REQUEST['id'] );
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $editLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been edited by ".$changes['user']."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else{
				   $changeMessage .= ucwords($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	  usort($changeLog, "compare");
	}
	echo json_encode( $changeLog );
	break;
case "getActionUpdates":
	$act 			= new RiskAction("", "", "","" , "", "", "", "", "");
	$updatesLog     = $act -> getActionUpdates( $_REQUEST['id'] ); 	
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $updatesLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been updated by ".(isset($changes['user']) ? $changes['user'] : "")."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else if($key == "description"){
					$changeMessage 	.= $change."\r\n<br />";
				}else{
				   $changeMessage .= "\r\n".ucwords($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	  usort($changeLog, "compare");
	}
	echo json_encode( $changeLog );
	break;
case "updateRiskAction":
    $action_id      = $_POST['id'];
	$actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	$action	        = $actObj -> getActionDetail($action_id) ;
	$statusObj 		= new ActionStatus("","", "");
	$statuses 		= $statusObj -> getStatuses();
    $uaccess 	    = new UserAccess( "", "", "", "", "", "", "", "", "");
    $users 	        = $uaccess -> getUsersList();
	$attachment     = array();
	$change_arr 	= array();
	$change_msg	    = "";	
	$update_data    = array();
	$attStr 		= "";	

    $att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
	if($_POST['deadline'] !== $action['deadline'])
	{
       $from                  = (strtotime($action['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($action['deadline'])) );
       $to                    = (strtotime($_POST['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['deadline'])) );
       if(strtotime($from) !== strtotime($to))
       {
         $change_arr['deadline']  = array('from' => $from, 'to' => $to);
         $change_msg              .= "Action Deadline changed from ".$action['deadline']." to ".$_POST['deadline']."\r\n\n";
         $update_data['deadline'] = $_POST['deadline'];
       }
	}
	if($_POST['timescale'] !== $action['timescale'])
	{
	   $from                        = $action['timescale'];
	   $to                          = $_POST['timescale'];
	   $update_data['timescale']    = $_POST['timescale'];
	   $change_msg                  = "Action Timescale changed to ".$to." from ".$from."\r\n";
	   $change_arr['timescale']     = array('from' => $from, 'to' => $to);
	}
	if($_POST['r_action'] !== $action['action'])
	{
	   $from                        = $action['action'];
	   $to                          = $_POST['r_action'];
       if(trim($frfom) != trim($to))
       {
           $update_data['action']       = $_POST['r_action'];
           $change_msg                  = "Action Name changed to ".$to." from ".$from."\r\n";
           $change_arr['action_name']   = array('from' => $from, 'to' => $to);	        
       }	   
	   
	}		
	if($_POST['action_owner'] != $action['owner'])
	{
	   $from                          = $users[$action['owner']]['user'];
	   $to                            = $users[$_POST['action_owner']]['user'];
	   $update_data['action_owner']   = $_POST['action_owner'];
	   $change_msg                    = "Action Owner changed to ".$to."% from ".$from."%\r\n";
	   $change_arr['action_owner']    = array('from' => $from, 'to' => $to);
	}
	if($_POST['remindon'] != $action['remindon'])
	{
       $from                  = (strtotime($action['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($action['remindon'])) );
       $to                    = (strtotime($_POST['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['remindon'])) );
       if(strtotime($from) !== strtotime($to))
       {
         $change_arr['action_remind_on']  = array('from' => $from, 'to' => $to);
         $change_msg                     .= "Remind On changed to ".$to." from ".$from."\r\n\n";
         $update_data['remindon']         = $_POST['remindon'];
       }
	}
	if($_POST['deliverable'] != $action['deliverable'])
	{
       $from                             = $action['deliverable'];
       $to                               = $_POST['deliverable'];
       $change_arr['action_deliverable'] = array('from' => $from, 'to' => $to);
       $change_msg                      .= "Action Deliverable changed to ".$to." from ".$from."\r\n\n";
       $update_data['deliverable']       = $_POST['deliverable'];
	}	
	
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['actionchanges']))
       {
          $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
          $change_msg              .= $att_change_str;
       }
       if(isset($_SESSION['uploads']['attachments']))
       {
          if($action['attachement'] != $_SESSION['uploads']['attachments'])
          {
             $update_data['attachement'] = $_SESSION['uploads']['attachments'];
          }
       }
    }
    $updates = array();
    if(!empty($change_arr))
    {
        if(!isset($change_arr['currentstatus']))
        {
           $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
        }
        $change_arr['user']   = $_SESSION['tkn'];
        $updates['changes']   = base64_encode(serialize($change_arr));
        $updates['action_id'] = $action_id;
    }
	$res                      = $actObj -> editRiskAction($action_id, $update_data, $updates);
	$response  = array(); 
	if($res > 0)
	{
		$response["id"] 	 = $res;
		$error 				 = false;
		$text 				 = " Action #".$action_id." has been successfully saved <br />";
		$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		$emailTo 		     = $user -> getRiskManagerEmails();		
		$email 			     = new Email();
		
		$email -> userFrom  = $_SESSION['tkn'];
		$email -> subject   = " Edit Action ";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message            = ""; 
		//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		$message           .= " \n";	//blank row above Please log on line
		$message           .= $_SESSION['tkn']." edited action #".$action_id." related to query #".$action['risk_id'];
		if(!empty($change_msg))
		{
			$message .= $change_msg;
		}
		if(!empty($att_change_str))
		{
		    $message .= $att_change_str;
		}			
		$message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		$message           = nl2br($message);			
		$email -> body     = $message;
		$email_to          = "";
		if(!empty($mailTo))
		{
		   $email_to = $emailTo.",";
		}
		$email_to          = $action['tkemail'];
		$email -> to 	  .= $email_to; 
		$email -> from     = "no-reply@ingite4u.com";
		if($email -> sendEmail()) 
		{
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		} else {
		   $error = true;
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		}
		$response = array("text" => $text, "error" => $error );
	} elseif($res == 0){
	  $response = array("text" => "No change made to the action", "error" => false, "updated" => false);
	} else {
	  $response = array("text" => "Error editing the action", "error" => true, "updated" => false);
	}
    Attachment::clear("qryadded", 'action_'.$_REQUEST['id']);
    Attachment::clear("qrydeleted", 'action_'.$_REQUEST['id']);
	echo json_encode( $response );
	break;	
case "newActionUpdate":
    $action_id      = $_POST['id'];
	$actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	$action	        = $actObj -> getActionDetail($action_id) ;
	$statusObj 		= new ActionStatus("","", "");
	$statuses 		= $statusObj -> getStatuses();
	$naming 	    = new Naming();
	$attachment     = array();
	$change_arr 	= array();
	$change_msg	    = "";	
	$update_data    = array();
	$attStr 		= "";	
    $att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
	if($_POST['status'] !== $action['status'])
	{
	   $from                        = $statuses[$action['status']]['name'];
	   $to                          = $statuses[$_POST['status']]['name'];
	   $update_data['status']       = $_POST['status'];
	   $change_msg                  = "Action Status changed to ".$to." from ".$from."\r\n";
	   $change_arr['action_status'] = array('from' => $from, 'to' => $to);
	   $change_arr['currentstatus'] = $to;
	}
	
	if($_POST['progress'] != $action['progress'])
	{
	   $from                          = $action['progress'];
	   $to                            = $_POST['progress'];
	   $update_data['progress']       = $_POST['progress'];
	   $change_msg                    = "Action Progress changed to ".$to."% from ".$from."%\r\n";
	   $change_arr['action_progress'] = array('from' => $from."%", 'to' => $to."%");
	}
	
	if($_POST['remindon'] != $action['remindon'])
	{
	   $from                           = $action['remindon'];
	   $to                             = $_POST['remindon'];
	   $update_data['remindon']        = $_POST['remindon'];
	   $change_msg                     = "Action Remind On changed to ".$to." from ".$from."\r\n";
	   $change_arr['action_remind_on'] = array('from' => $from, 'to' => $to);  
	}
	if(isset($_POST['action']))
	{
	    if($_POST['action_on'] != $action['action_on'])
	    {
           $from                  = (strtotime($action['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($action['action_on'])) );
           $to                    = (strtotime($_POST['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['action_on'])) );
           if(strtotime($from) !== strtotime($to))
           {
             $change_arr['action_on']  = array('from' => $from, 'to' => $to);
             $change_msg              .= "Action on changed from ".$action['action_on']." to ".$_POST['action_on']."\r\n\n";
             $update_data['action_on'] = date("Y-m-d", strtotime($_POST['action_on']));
           }
	    }		    
	}
	
	if(isset($_POST['description']) && !empty($_POST['description']))
	{
	  $change_arr['response'] = $_POST['description'];
	  $change_msg             = " Added response ".$_POST['description'];
	}
	
    if(isset($_POST['approval']) && $_POST['approval'] == 'on')
    {
       $change_arr['approval'] = "Requested sending of approval email.";  
       $change_msg            .= "Requested sending of approval email.\r\n\n";
    }	
    
    if($_POST['progress'] == 100 && $_POST['status'] == 3)
    {
        $change_arr['approval_status']  = "Awaiting approval status added.";  
        $change_msg                    .= "Awaiting approval status added.\r\n\n";
        if(($action['action_status'] & RiskAction::AWAITING_APPROVAL) != RiskAction::AWAITING_APPROVAL)
        {
           $update_data['action_status']   = $action['action_status'] + RiskAction::AWAITING_APPROVAL; 
        }
    }    
    
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['actionchanges']))
       {
          $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
          $change_msg              .= $att_change_str;
       }
       if(isset($_SESSION['uploads']['attachments']))
       {
          if($action['attachement'] != $_SESSION['uploads']['attachments'])
          {
             $update_data['attachement'] = $_SESSION['uploads']['attachments'];
          }
       }
    }	

    $updates = array();
    if(!empty($change_arr))
    {
        if(!isset($change_arr['currentstatus']))
        {
           $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
        }
        $change_arr['user']   = $_SESSION['tkn'];
        $updates['changes']   = base64_encode(serialize($change_arr));
        $updates['action_id'] = $action_id;
    }
	$id                      = $actObj -> updateAction($action_id, $update_data, $updates);
	$response  = array(); 
	if($id > 0)
	{
		$response["id"] 	 = $id;
		$error 				 = false;
		$text 				 = " New action update for action #".$action_id." has been successfully saved <br />";
		$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		$emailTo 		     = $user -> getRiskManagerEmails();		
		$email 			     = new Email();
		
		$email -> userFrom  = $_SESSION['tkn'];
		$email -> subject   = " Update Action ";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message            = ""; 
		//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		$message           .= " \n";	//blank row above Please log on line
		$message           .= $_SESSION['tkn']." Updated action #".$action_id." related to query #".$action['risk_id'];
		if(!empty($change_msg))
		{
			$message .= $change_msg;
		}
		if(!empty($att_change_str))
		{
		    $message .= $att_change_str;
		}			
		
		$message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		$message           = nl2br($message);			
		$email -> body     = $message;
		$email_to          = "";
		if(!empty($mailTo))
		{
		   $email_to = $emailTo.",";
		}
		$email_to          = $action['tkemail'];
		$email -> to 	  .= $email_to; 
		$email -> from     = "no-reply@ingite4u.com";
		if($email -> sendEmail()) 
		{
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		} else {
		   $error = true;
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		}
		$response = array("text" => $text, "error" => $error );
	} else {
		$response = array("text" => "Error saving the action update", "error" => true, "updated" => false);
	}
	Attachment::clear('qryadded', 'action_'.$action['id']);
	echo json_encode($response);
	break;	
case "getActionAssurances":
	$action = new ActionAssurance($_GET['action_id'], "", "", "", "", "", "");
	echo json_encode( $action -> getActionAssurances($_GET) );
	break;
case "getActionAssurance":
	$action = new RiskAction("", "", "", "", "", "", "","", "");
	$action -> saveActionAssurance($_REQUEST['id'], "", $_REQUEST['response'], $_REQUEST['signoff'], "", "", "" ) ;
	break;
case "saveActionAssurance":
    $att_change_str = Attachment::processAttachmentChange("", "actionassurance_".$_POST['action_id'], 'actionassurance');
    $attachments    = "";
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['attachments']))
       {
          if(!empty($_SESSION['uploads']['attachments']))
          {
            //$update_data['attachment'] = $_SESSION['uploads']['attachments'];
             $attachments               = $_SESSION['uploads']['attachments'];
          }
       }
    }
	$newAssurance = new ActionAssurance($_POST['action_id'], $_POST['date_tested'], $_POST['response'], $_POST['signoff'], "", $attachments, "");
	echo json_encode( $newAssurance -> saveActionAssurance() );
    Attachment::clear('qryadded');
    Attachment::clear('qrydeleted');	
	break;
case "updateActionAssurance":
	$assuranceObj = new ActionAssurance("", "","", "", "", "", "");
	$assurance    = $assuranceObj -> getAssurance($_POST['id']);
    $statusObj    = new ActionAssuranceStatus();
    $statuses     = $statusObj -> getAll();
    $currentstatus= "";
	$_changes     = array();
	$update_data  = array();
	foreach($assurance as $field => $value)
	{
	   if(isset($_POST[$field]))
	   {
	      if($_POST[$field] != $value)
	      {
             if($field == "status")
             {  
               $from = $to = "";
               if($_POST['status'] == 2)
               {
                  $_changes['status_'] = "Action Assurance #".$_POST['id']." has beed deleted"; 
               } elseif($_POST['status'] == 0) {
                  $_changes['status_'] = "Action Assurance #".$_POST['id']." has beed deactivated"; 
               }
               $update_data[$field] = $_POST[$field];
             } elseif($field == "signoff"){
                $from = $statuses[$value]['client_terminology'];
                $to   = $statuses[$_POST[$field]]['client_terminology'];
                $_changes[$field]     = array('from' => $from, 'to' => $to);
                $update_data[$field] = $_POST[$field];      
                $currentstatus       = $to;       
             } else {
                $from = $to = "";
                $_changes[$field]     = array('from' => $value, 'to' => $_POST[$field]);
                $update_data[$field] = $_POST[$field];
             }
	      }
	   }
	}
    $change_str = Attachment::processAttachmentChange($assurance['attachment'], "actionassurance_".$_POST['id'], 'actionassurance');
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['actionchanges']))
       {
          $_changes['attachment'] = $_SESSION['uploads']['actionchanges'];
          //$change_msg               .= $att_change_str;
       }
       if(isset($_SESSION['uploads']['attachments']))
       {
          if($assurance['attachment'] != $_SESSION['uploads']['attachments'])
          {
             $update_data['attachment'] = $_SESSION['uploads']['attachments'];
             $attachments               = $_SESSION['uploads']['attachments'];
          }
       }
    }
	$updates  = array();
	if(!empty($_changes))
	{
	   $changes['user']          = $_SESSION['tkn'];
	   $changes['ref_']          = "Action Assurance Ref #".$_POST['id'];
	   $changes                  = array_merge($changes, $_changes);
	   if(!empty($currentstatus))
	   {
	      $changes['currentstatus'] = $currentstatus;
	   } else {
	      if(isset($statuses[$assurance['signoff']]))
	      {
	        $changes['currentstatus']  = $statuses[$assurance['signoff']]['client_terminology'];
	      } else {
	        $changes['currentstatus']  = " - ";
	      }
	   }
	   $updates['changes']       = base64_encode(serialize($changes));
	   $updates['insertuser']    = $_SESSION['tid'];
	}
	$res  = $assuranceObj -> updateAssurance($_POST['id'], $update_data, $updates);
	
	$response     = array();
	if($res > 0)
	{
	   $response  = array('text' => 'Action assurance updated successfully', 'error' => false, 'updated' => true);
	} else if($res == 0){
	   $response  = array('text' => 'No changes to action assurance', 'error' => false, 'updated' => false);
	} else {
	   $response  = array('text' => 'Error updating action assurance', 'error' => true);
	} 
	echo json_encode($response);
    Attachment::clear('qryadded');
    Attachment::clear('qrydeleted');	
	break;
case "deleteActionAssurance":
	$newAssurance = new ActionAssurance( "", "", "", "", "", "", "" );
	$res = $newAssurance -> deleteActionAssurance( $_POST['id'] );
	$response = array();	
	if( $res == 1){
		$response =array("text" => "Action assurance deleted . . . ", "error" => false);
	} else {
		$response = array("text" => "Error deleting the action assurance", "error" => true);
	}
	echo json_encode( $response );
	break;	
case "sendRequestApprovalEmail":
			$response = array(); 
			$emailTo  = "";
			$act 	  = new RiskAction("", "", "", "", "", "", "","", "");
			$status   = $act -> getApprovalRequestStatus( $_REQUEST['id'] );
			if( ($status['action_status'] & RiskAction::REQUEST_APPROVAL) == RiskAction::REQUEST_APPROVAL  )
			{
				$response = array("error" => true, "text" => "A request for approval has already been sent");
			} else {
				$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
				$users 			     		  = $user -> getRiskManagerEmails();
				$action						  = $act -> getAAction( $_REQUEST['id'] );
				$email 			     		  = new Email();
				$email -> userFrom   		  = $_SESSION['tkn'];
				$email -> subject    		  = "Action Approval Request";
				//Object is something like Risk or Action etc.  Object_id is the reference of that object
				$message   = ""; 
				$message .= $email->userFrom." has submitted a request for approval of an action #".$_REQUEST['id'];
				$message  .= " related to Query ".$action['risk_id']." : ".$action['description'];
				$message .= " \r\n\n";
					//blank row above Please log on line
				$message .= "Please log onto Ignite Assist to approve the action.\r\n\n";
				$message  = nl2br($message);		
				$email -> body     = $message;
				$emailTo 		   = "";
				if(!empty($users)){
					foreach($users as $userEmail ) {
						$emailTo .= $userEmail['tkemail'].",";
					}
				}
				$email -> to 	   = ($emailTo == "" ? "" : $emailTo)."".$action['action_creator'];
				$email -> from     = "info@ignite4u.com";
				if( $email -> sendEmail() ) {
					$id	= $act->setActionStatus( $_REQUEST['id'], $status['action_status'] + RiskAction::REQUEST_APPROVAL);
					$response = array("text" => "Email send successfully", "error" => false);					
				} else {
					$response = array( "error" => true, "text" => "There was an error sending the request approval email" );
				}
			} 
		echo json_encode( $response );			
		break;	
case "searchActionToApprove":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> searchActionToApprove( $_REQUEST['searchtext'] ) );
	break;
case "addAssuranceAttachment":
	$attObj = new Attachments($_FILES, "action");
	$attObj -> upload();
break;
case "removeAssuranceFile":
	$attObj = new Attachments("", "action");
	$attObj -> deletedFile($_POST['index'], $_POST['file'] );
break;
case "editAssuranceAttachment":
	$attObj = new Attachments($_FILES, "action");
	$attObj -> upload("editactionassurance");
break;
case "deleteAssuranceFile":
	$attObj = new Attachments("", "action");
	$attObj -> remove( $_POST['index'], $_POST['file'], "deleteactionassurance" );	
break;
case "getActionQuery":
	$riskObj = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
	$query   = $riskObj -> getRiskTable($_GET['id'] );
	echo $query;
break;
case "getActionAssuranceStatus":
  $actionAssuranceObj = new ActionAssuranceStatus();
  $assurances         = $actionAssuranceObj -> getAll();
  echo json_encode($assurances);
break;
}

	function compare($a ,$b)
	{
		return ( $a['date'] > $b['date'] ? -1 : 1 );
	}

?>
