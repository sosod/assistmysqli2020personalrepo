<?php
$scripts = array( 'risk_level.js','menu.js','jpickermin.js','ignite.textcounter.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Edit Risk Level";
require_once("../inc/header.php");
$level 	   = new RiskLevel( "", "", "", "" );
$risklevel  = $level-> getARiskLevel( $_REQUEST['id'] ) ;	 
?>
<script>
$(function(){
		$("table#editarisk_level_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<form id="edit-form-risk-level" name="edit-form-risk-level">
<table border="1" id="editarisk_level_table" cellpadding="5" cellspacing="0">
  <tr>
	    <th>Ref #:</th>
	    <td><?php echo $risklevel['id']; ?></td>
   </tr>
   <tr>
    <th>Short Code:</th>
    <td><input type="text" name="risklevel_shortcode" id="risklevel_shortcode" value="<?php echo $risklevel['shortcode']; ?>" /></td>
   </tr>
  <tr>
   <th>Query Level:</th>
   <td>
   	  <textarea name="risklevel" id="risklevel" class="textcounter"><?php echo $risklevel['name']; ?></textarea>
   	</td>
  </tr>
  <tr> 
    <th>Description:</th>
    <td>
    	<textarea id="risklevel_description" name="risklevel_description" class="mainbox"><?php echo $risklevel['description']; ?></textarea>
    </td>
   </tr>
  <tr>  
    <th></th>
    <td>
    	<input type="hidden" name="risklevel_id" id="risklevel_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="hidden" name="risklevel_status" id="risklevel_status" value="<?php echo $risklevel['status'] ?>" />        
    	<input type="submit" name="edit_risklevel" id="edit_risklevel" value="Edit" />
        <input type="submit" name="cancel_risklevel" id="cancel_risklevel" value="Cancel" />
    </td>
  </tr>
  <tr>
    <td class="noborder" align="left"><?php displayGoBack("risk_level.php", ""); ?></td>
  	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>

