<?php
$scripts = array( 'control.js','menu.js', 'jscolor.js');
$styles = array();
$page_title = "Set Up Risk Control Effectiveness";
require_once("../inc/header.php");
?>
<div>
<form id="form-control-effectiveness" name="form-control-effectiveness">
<div id="control_message"></div>
<br /><br />
<table border="1" id="control_table">
  <tr>
    <th>ID</th>
    <th>Short Code</th>
    <th>Control Effectiveness</th>
    <th>Qualification Criteria</th>    
    <th>Rating</th>
    <th>Colour assigned to</th>
    <th></th>    
    <th>Status</th>        
    <th></th>        
  </tr>
  <tr>
    <td>#</td>
    <td><input type="text" name="control_shortcode" id="control_shortcode" size="10" /></td>
    <td><input type="text" name="control_effect" id="control_effect" /></td>
    <td><input type="text" name="quali_critera" id="quali_critera" /></td>
    <td><input type="text" name="control_rating" id="control_rating" /></td>
    <td><input type="text" name="control_color" id="control_color" class="color" value="abf207" /></td>
    <td><input type="submit" name="add_control" id="add_control" value="Add" /></td>
    <td></td>    
    <td></td>        
  </tr>
</table>
</form>
</div>

