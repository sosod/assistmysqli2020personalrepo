<?php
$scripts = array( 'risk_manager.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
$user = $uaccess -> getARiskManager( $_REQUEST['id']);
?>
<?php JSdisplayResultObj(""); ?>
<table width="40%">
	<tr>
    	<td colspan="2"><h4>Update the query manager</h4></td>        
    </tr>
	<tr>
    	<th>Set as query manager</th>
    	<td>
        	<select id="query_manager" name="query_manager">
            	<option value="1" <?php if($user['risk_manager'] == 1) { ?> selected <?php } ?>>Yes</option>
            	<option value="0" <?php if($user['risk_manager'] == 0) { ?> selected <?php } ?>>No</option>                
            </select>
        </td>        
    </tr>    
	<tr>
		<th></th>
    	<td>
            <input type="hidden" name="userid" value="<?php echo $_REQUEST['id']; ?>" id="userid" />        
        	<input type="submit" name="update_risk_manager" value="Save Changes" id="update_risk_manager" />
            <input type="submit" name="cancel_changes" value="Cancel" id="cancel_changes" />
        </td>        
    </tr>   
    <tr>
    	<td class="noborder"><?php displayGoBack("",""); ?></td>
    	<td class="noborder"></td>
    </tr> 
</table>