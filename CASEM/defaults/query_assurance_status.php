<?php
$scripts    = array('jscolor.js', 'status.js', 'query_assurance_status.js');
$styles     = array();
$page_title = "Query Assurance Statuses";
require_once("../inc/header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
 <tr>
   <td colspan="2" class="noborder">
   <table width="100%" id="queryassurancestatus_table"">
      <tr>
        <td colspan="6">
          <input type="button" name="add" value="Add New" id="add" />
        </td>
      </tr>
      <tr>
        <th>Ref</th>
        <th>Query Status</th>
        <th>Client Terminology</th>
        <th>Color</th>
        <th>Status</th>
        <th></th>
      </tr>      
   </table>
   </td>
 </tr>
 <tr>
   <td class="noborder"><?php displayGoBack("", ""); ?></td>
   <td class="noborder"><?php displayAuditLogLink("query_assurance_status_logs", true) ?></td>
 </tr>
</table>
