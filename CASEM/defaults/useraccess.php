<?php
$scripts = array( 'useraccess.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "User Access";
require_once("../inc/header.php");
?>
<div>
<form method="post" id="useraccess-form" name="useraccess-form">
<div id="useraccess_message" class="message"></div>
<table border="1" id="useraccess_table">
  <tr>
    <th>Ref</th>
    <th>User</th>
    <th>Module Admin</th>
    <th>Create Queries</th>
    <!-- <th>Create Actions</th> -->
    <th>Update All</th>
    <th>Edit All</th>
    <th>Reports</th>
    <th>Assurance</th>
    <th>Setup</th>
    <th>Action</th>
  </tr>
  <tr>
    <td>#</td>
    <td>
		<select name="userselect" id="userselect">
        	<option value="">--select user--</option>
        </select>
    </td>
    <td>
    	<select id="module_admin" name="module_admin">
        	<option>--module admin--</option>
            <option value="1">yes</option>
            <option value="0" selected="selected">no</option>            
        </select>
    </td>
    <td>
    	 <select id="create_risks">
        	<option>--create_risks--</option>
            <option value="1">yes</option>
            <option value="0" selected="selected">no</option>            
        </select>
    </td>
    <!-- <td>
    	 <select id="create_actions" name="create_actions">
        	<option>--create actions--</option>
            <option value="1" selected="selected">yes</option>
            <option value="0">no</option>            
        </select>
    </td> -->    
    <td>
    	 <select id="view_all" name="view_all">
        	<option>--view all--</option>
            <option value="1" selected="selected">yes</option>
            <option value="0">no</option>            
        </select>
    </td>
    <td>
    	<select id="edit_all" name="edit_all">
        	<option>--edit_all--</option>
            <option value="1">yes</option>
            <option value="0" selected="selected">no</option>            
        </select>
    </td>
    <td>
    	 <select id="reports" name="reports">
        	<option>--reports--</option>
            <option value="1" selected="selected">yes</option>
            <option value="0">no</option>            
        </select>
    </td>
    <td>
    	 <select id="assurance" name="assurance">
        	<option>--assurance--</option>
            <option value="1">yes</option>
            <option value="0" selected="selected">no</option>            
        </select>
    </td>    
    <td>
    	 <select id="usersetup" name="usersetup">
        	<option>--setup--</option>
            <option value="1">yes</option>
            <option value="0" selected="selected">no</option>            
        </select>
    </td>
    <td><input type="submit" value="Add" id="setup_access" name="setup_access" /></td>
  </tr>
</table>
<div><?php displayGoBack("",""); ?></div>
</form>
</div>
