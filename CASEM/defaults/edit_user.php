<?php
$scripts = array( 'edit_useraccess.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "User Access";
require_once("../inc/header.php");
$us 	= new UserAccess(  "", "", "", "", "", "", "", "", "" );
$user   = $us -> getUserAccessInfo($_REQUEST['id']); 
?>
<div>
<script>
$(function(){
		$("table#edit_user_table").find("th").css({"text-align":"left"})
});
</script>
<div class="message" id="useraccess_message"></div>
<form id="edit-user-form" name="edit-user-form">
	<table id="edit_user_table" width="40%">
    	<tr>
        	<th>User</th>
            <td><?php echo $user['tkname']." ".$user['tksurname']; ?></td>
        </tr>
    	<tr>
        	<th>Module Admin:</th>
            <td>
            	<select id="module_admin">
                	<option value="1" <?php if($user['module_admin']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['module_admin']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>        
    	<tr>
        	<th>Create Query:</th>
            <td>
            	<select id="create_risk">
                	<option value="1" <?php if($user['create_risks']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['create_risks']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>                
<!--
    	<tr>
        	<th>Create Actions:</th>
            <td>
            	<select id="create_actions">
                	<option value="1" <?php if($user['create_actions']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['create_actions']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>  
-->                     
    	<tr>
        	<th>Update All:</th>
            <td>
            	<select id="view_all">
                	<option value="1" <?php if($user['view_all']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['view_all']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>              
    	<tr>
        	<th>Edit All:</th>
            <td>
            	<select id="edit_all">
                	<option value="1" <?php if($user['edit_all']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['edit_all']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>  
    	<tr>
        	<th>Reports:</th>
            <td>
            	<select id="reports">
                	<option value="1" <?php if($user['report']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['report']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>                      
    	<tr>
        	<th>Assurance:</th>
            <td>
            	<select id="assurance">
                	<option value="1" <?php if($user['assurance']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['assurance']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>                       
    	<tr>
        	<th>Setup:</th>
            <td>
            	<select id="setup" name="setup">
                	<option value="1" <?php if($user['setup']==1) {?> selected="selected" <?php } ?>>Yes</option>
                	<option value="0" <?php if($user['setup']==0) {?> selected="selected" <?php } ?>>No</option>                    
                </select>
			</td>
        </tr>                        
        <tr>
        	<th></th>
        	<td align="right">
            	<input type="hidden" name="user_id" value="<?php echo $_REQUEST['id']; ?>" id="user_id" />
                <input type="button" name="save_user" id="save_user" value="Save Chages" />
                <input type="button" name="cancel_changes" id="cancel_changes" value="Cancel" />
            </td>
        </tr>
        <tr>
        	<td class="noborder" align="left">
            	<?php displayGoBack("",""); ?>
            </td>
            <td class="noborder"></td>
        </tr>
    </table>
</form>
</div>
