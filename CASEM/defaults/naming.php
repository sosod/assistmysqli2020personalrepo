<?php
$scripts = array( 'naming.js','menu.js', 'setup_logs.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<p>Headings Naming Conversion ( Client can define own naming conversion ) </p>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<div id="container" style="clear: both;">
			    <div id="naming_message" class="message" style="clear: both;"></div>
			</div>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("",""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("header_names_logs", true)?></td>
	</tr>
</table>


