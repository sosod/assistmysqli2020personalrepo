<?php
$scripts = array( 'setup_glossary.js', 'setup_logs.js');
$styles = array( 'colorpicker.css' );
$page_title = "Setup Glossary";
require_once("../inc/header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form method="post">	
			<table align="left" id="glossary_table">
				<tr>
			    	<th>Category</th>
			    	<th>Terminology</th>
			    	<th>Explanation</th>   
			    	<th></th>                        
			    </tr>
			    <tr>
			        <td>
			        	<textarea name="category" id="category"></textarea>
			        </td>
			        <td>
			        	<textarea name="terminolgy" id="terminolgy"></textarea>
			        </td>    
			        <td>
						<textarea name="explanation" id="explanation"></textarea>
			        </td> 
			    	<td align="right" colspan="2">
			       	 	<input type="submit" name="save_glossary" id="save_glossary" value="Save" />
			        </td>                   
			    </tr>  
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("",""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("glossary_logs", true)?></td>
	</tr>
</table>
