<?php
$scripts = array('query_source.js','menu.js', 'setup_logs.js');
$styles = array();
$page_title = "Set Up Query Source";
require_once("../inc/header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
 <tr>
   <td colspan="2" class="noborder">
   <table width="100%" id="querysource_table"">
      <tr>
        <td colspan="6">
          <input type="button" name="add" value="Add New" id="add" />
        </td>
      </tr>
      <tr>
        <th>Ref</th>
        <th>Query Source Name</th>
        <th>Query Source Description</th>
        <th>Status</th>
        <th></th>
      </tr>      
   </table>
   </td>
 </tr>
 <tr>
   <td class="noborder"><?php displayGoBack("", ""); ?></td>
   <td class="noborder"><?php displayAuditLogLink("query_source_logs", true) ?></td>
 </tr>
</table>
