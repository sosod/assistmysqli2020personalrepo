<?php
$scripts = array( 'edit_inherentrisk.js','menu.js' , 'jscolor.js');
$styles = array();
$page_title = "Edit Inherent Risk";
require_once("../inc/header.php");

$ihr 	= new InherentRisk( "", "", "", "", "");
$inherentrisk =  $ihr -> getAInherentRisk( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#inherentrisk_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<form id="edit-inherentrisk-form" name="edit-inherentrisk-form">
<div id="inherentrisk_message"></div>
<table border="1" id="inherentrisk_table" cellpadding="5" cellspacing="0">
  <tr>
  	
    <th>Ref:</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Rating:</th>
    <td>
   		<input type="text" name="inherent_rating_from" id="inherent_rating_from" size="2" value="<?php echo $inherentrisk['rating_from']; ?>" /> 
   	to
   		 <input type="text" name="inherent_rating_to" id="inherent_rating_to" size="2" value="<?php echo $inherentrisk['rating_to']; ?>" />
    </td>
    </tr>
    <tr>
    <th>Inherent risk magnitude:</th>
    <td><input type="text" name="inherent_risk_magnitude" id="inherent_risk_magnitude" value="<?php echo $inherentrisk['magnitude']; ?>" /></td>
    </tr>
    <tr>
    <th>Response:</th>
    <td><textarea id="inherent_response" name="inherent_response"><?php echo $inherentrisk['response']; ?></textarea></td>
    </tr>
    <tr>
    <th>Colour:</th>
    <td><input type="text" name="inherent_color" id="inherent_color" class="color" value="<?php echo ($inherentrisk['color'] == "" ? "e2ddcf" :$inherentrisk['color'] ) ?>"/></td>
    </tr>
  <tr>
      <td>&nbsp;</td>
    <td>
    <input type="hidden" name="inherentrisk_id" id="inherentrisk_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="edit_inherentrisk" id="edit_inherentrisk" value="Edit" />
    <input type="submit" name="cancel_inherentrisk" id="cancel_inherentrisk" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td colspan="2"><?php displayGoBack("",""); ?></td>
  </tr>
</table>
</form>
</div>

