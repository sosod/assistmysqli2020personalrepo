<?php
$scripts    = array('jscolor.js', 'status.js', 'action_assurance_status.js');
$styles     = array();
$page_title = "Set Up Risk Statuses";
require_once("../inc/header.php");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
 <tr>
   <td colspan="2" class="noborder">
   <table width="100%" id="actionassurancestatus_table"">
      <tr>
        <td colspan="6">
          <input type="button" name="add" value="Add New" id="add" />
        </td>
      </tr>
      <tr>
        <th>Ref</th>
        <th>Action Assurnace Status</th>
        <th>Client Terminology</th>
        <th>Color</th>
        <th>Status</th>
        <th></th>
      </tr>      
   </table>
   </td>
 </tr>
 <tr>
   <td class="noborder"><?php displayGoBack("", ""); ?></td>
   <td class="noborder"><?php displayAuditLogLink("action_assurance_status_logs", true) ?></td>
 </tr>
</table>
