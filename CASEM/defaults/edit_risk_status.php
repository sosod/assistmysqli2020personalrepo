<?php
$scripts = array( 'risk_status.js','menu.js', 'jscolor.js', 'ignite.textcounter.js');
$styles = array();
$page_title = "Edit Risk Status";
require_once("../inc/header.php");
$getstat = new RiskStatus( "", "", "" );
$status =  $getstat -> getAStatus( $_REQUEST['id']) ;	
?>
<script>
$(function(){
		$("table#edit_risk_status_table").find("th").css({"text-align":"left"})
});
</script>
<?php JSdisplayResultObj(""); ?>
    <div>
    <form id="edit-risk-status-form" name="edit-risk-status-form" method="post">
    <table id="edit_risk_status_table" cellpadding="5" cellspacing="0">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Query Status:</th>
        <td>
        	<textarea name="status" id="status" class="textcounter" <?php if($status['defaults'] == 1) { ?> disabled="disabled"<?php } ?>><?php echo $status['name']; ?></textarea>
        </td>
      </tr>
      <tr>
        <th>&lt;Client&gt; Terminology:</th>
        <td>
        	<textarea name="client_term" id="client_term" class="textcounter"><?php echo $status['client_terminology']; ?></textarea>
        </td>
      </tr>
      <tr>
      	<th>Color:</th>
        <td>
         	<input name="risktype_color" id="risktype_color" class="color" value="<?php echo ($status['color']=="" ? "66ff00" : $status['color']); ?>">
        </td>
      </tr>
      <tr>
      	<th></th>
        <td>
        <input type="hidden" name="risk_status_id" id="risk_status_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_status" id="edit_status" value="Edit" />
        <input type="submit" name="cancel_status" id="cancel_status" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left"><?php displayGoBack("/QUERY/defaults/risk_status.php",""); ?></td>
      	<td class="noborder"></td>
      </tr>
    </table>
    </form>
    </div>

