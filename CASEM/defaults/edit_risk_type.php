<?php
$scripts = array( 'risk_types.js','menu.js', 'ignite.textcounter.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Edit Risk Types";
require_once("../inc/header.php");
$type 	   = new RiskType( "", "", "", "" );
$risktype  = $type-> getARiskType( $_REQUEST['id'] ) ;	 
?>
<script>
$(function(){
		$("table#editarisk_type_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<form id="edit-form-risk-type" name="edit-form-risk-type">
<table border="1" id="editarisk_type_table" cellpadding="5" cellspacing="0">
  <tr>
	    <th>Ref #:</th>
	    <td><?php echo $risktype['id']; ?></td>
   </tr>    
   <tr>
    <th>Short Code:</th>
    <td>
    	<input type="text" name="risktype_shortcode" id="risktype_shortcode" value="<?php echo $risktype['shortcode']; ?>" />
    </td>
   </tr>
  <tr>
   <th>Risk Type:</th>
   <td>
   		<textarea name="risktype" id="risktype" class="textcounter"><?php echo $risktype['name']; ?></textarea>
   	</td>
  </tr>
  <tr> 
    <th>Description:</th>
    <td>
    	<textarea class="mainbox" id="risktype_descri" name="risktype_descri"><?php echo $risktype['description']; ?></textarea></td>
   </tr>
  <tr>  
    <th></th>
    <td>
    	<input type="hidden" name="risktype_id" id="risktype_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="edit_risktype" id="edit_risktype" value="Edit" />
        <input type="submit" name="cancel_risktype" id="cancel_risktype" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left"><?php displayGoBack("/QUERY/defaults/risk_type.php",""); ?></td>
    <td class="noborder"></td>
  </tr>
</table>
</form>
</div>

