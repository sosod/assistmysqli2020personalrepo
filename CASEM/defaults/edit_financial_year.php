<?php
$scripts = array( 'financial_year.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit Finicial Year";
require_once("../inc/header.php");
$fnyr  = new FinancialYear( "", "", "" );
$year  = $fnyr -> getAFinYear( $_REQUEST['id']);
//$start = $fnyr -> setToDatePickerFormat( $year['start_date'] );
//$end   = $fnyr -> setToDatePickerFormat( $year['end_date'] );
?>
<script>
$(function(){
	$("table#edit_fin_year").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<table id="edit_fin_year">  
 <tr>
   <th>Last day of Financial year:</th>
	<td>
	 <input type="text" name="edit_last_year" id="edit_last_year" class="datepicker" value="<?php echo $year['last_day']; ?>" readonly="readonly" />
	</td>	
 </tr>
 <tr>
   <th>Start date:</th>
   <td><input type="text" name="edit_start_date" id="edit_start_date" class="datepicker" value="<?php echo $year['start_date']; ?>" readonly="readonly" /></td>
 </tr>
 <tr>
   <th>End date:</th>
   <td><input type="text" name="edit_end_date" id="edit_end_date" class="datepicker" value="<?php echo $year['end_date']; ?>" readonly="readonly" /></td>
 </tr>
 <tr>
   <th>
   <input type="hidden" value="<?php echo $_REQUEST['id']; ?>" name="fini_year_id" id="fini_year_id" />
  </th>
   <td> 
   	<input type="submit" value="Save Changes" id="save_changes_fin_year" />
       	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	<input type="submit" value="Cancel" id="cancel_changes_fin_year" />
   </td>        
 </tr>
 <tr>
 	<td align="left" class="noborder">
  		<?php displayGoBack("financial_year.php",""); ?>
  </td>
  <td class="noborder"></td>
 </tr>
</table>
</div>	

