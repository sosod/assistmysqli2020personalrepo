<?php
$scripts = array( 'action_status.js','menu.js', 'jscolor.js', 'ignite.textcounter.js');
$styles = array();
$page_title = "Change Action Statuses";
require_once("../inc/header.php");
$actionstatus 	= new ActionStatus( "", "", "" );
$action_status	= $actionstatus  -> getAStatus( $_REQUEST['id'] );	
?>
<div>
<?php JSdisplayResultObj(""); ?>
<form id="edit-form-action-status" name="edit--action-status">
<table border="1" id="editaction_status_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $action_status['id']; ?></td>
    </tr>
   <tr>
    <th>Action Status:</th>
    <td>
    	<select id="action_status_status" name="action_status_status">
        	<option value="1" <?php if( ($action_status['status'] & 1) == 1 ) {?> selected <?php } ?> >Active</option>
        	<option value="0" <?php if( ($action_status['status'] & 1) == 0 ) {?> selected <?php } ?>>Inactive</option>
        </select>
     </td>
   </tr>
   <tr>
    <th></th>
    <td>
    	<input type="hidden" name="action_status_id" id="action_status_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="changes_actionstatus" id="changes_actionstatus" value="Save Changes" />
        <input type="submit" name="cancel_actionstatus" id="cancel_actionstatus" value="Cancel" />
    </td>
  </tr>
 <tr>
   <td class="noborder" align="left"><?php displayGoBack("/QUERY/defaults/action_status.php",""); ?></td>
   <td class="noborder"></td> 
 </tr>
</table>
</form>
</div>

