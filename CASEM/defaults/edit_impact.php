<?php
$scripts = array( 'edit_impact.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit Impact";
require_once("../inc/header.php");

$imp 	= new Impact( "", "", "", "", "");
$impact =  $imp -> getAImpact( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#impact_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<form id="edit-impact-form" name="edit-impact-form">
<div id="impact_message"></div>
<table border="1" id="impact_table">
  <tr>
  	
    <th>Ref:</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Rating:</th>
    <td>
   		<input type="text" name="imprating_from" id="imprating_from" size="2" value="<?php echo $impact['rating_from']; ?>" /> 
   	to
   		 <input type="text" name="imprating_to" id="imprating_to" size="2" value="<?php echo $impact['rating_to']; ?>" />
    </td>
    </tr>
    <tr>
    <th>Assessment:</th>
    <td><input type="text" name="impact_assessment" id="impact_assessment" value="<?php echo $impact['assessment']; ?>" /></td>
    </tr>
    <tr>
    <th>Definition:</th>
    <td><textarea id="impact_definition" name="impact_definition"><?php echo $impact['definition']; ?></textarea></td>
    </tr>
    <tr>
    <th>Colour:</th>
    <td><input type="text" name="impact_color" id="impact_color" class="color" value="<?php echo ($impact['color'] == "" ? "e2ddcf" :$impact['color'] ) ?>"/></td>
    </tr>
  <tr>
      <th>&nbsp;</th>
    <td>
    <input type="hidden" name="impact_id" id="impact_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="edit_impact" id="edit_impact" value="Edit" />
    <input type="submit" name="cancel_impact" id="cancel_impact" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td colspan="2" align="left"><?php displayGoBack("",""); ?></td>
  </tr>
</table>
</form>
</div>

