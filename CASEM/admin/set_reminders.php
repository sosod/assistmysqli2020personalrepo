<?php
$scripts 	= array('reminder.js');
$styles 	= array('colorpicker.css',   );
$page_title = "Reminder";
require_once("../inc/header.php");
$reminderObj = new Reminder();
$reminder    = $reminderObj -> getAReminder();

?>
<?php JSdisplayResultObj(""); ?>
<form id="notification_form" name="notification_form">
<table id="admintable">
	<tr>
		<th>No</th>
		<th>Reminder e-mail</th>
		<th>Reminder Type</th>
		<th>Days before deadline</th>
	</tr>
	<tr>
		<td>1</td>
		<td>Automatic Action Reminder</td>
		<td>All Users</td>
		<td>
		  <select id="action_days" name="query_days">
		    <?php 
		      for($i = 1; $i <= 31; $i++)
		      {
		    ?>
    		    <option value="<?php echo $i; ?>" 
    		    <?php 
    		      if(!empty($reminder))
    		      {
    		        if(isset($reminder['action_days']))
    		        {
    		           echo ($reminder['action_days'] == $i ? "selected='selected'" : "");
    		        }
    		      }
    		    ?>>
    		      <?php echo $i; ?>
    		    </option>
		    <?php
		      }
		    ?>
		  </select>
		</td>
	</tr>
	<tr>
		<td>2</td>
		<td>Automatic Query Reminder</td>
		<td>All Users</td>
		<td>
		  <select id="query_days" name="query_days">
		    <?php 
		      for($i = 1; $i <= 31; $i++)
		      {
		    ?>
    		    <option value="<?php echo $i; ?>" 
    		    <?php 
    		      if(!empty($reminder))
    		      {
    		        if(isset($reminder['query_days']))
    		        {
    		           echo ($reminder['query_days'] == $i ? "selected='selected'" : "");
    		        }
    		      }
    		    ?>>
    		      <?php echo $i; ?>
    		    </option>
		    <?php
		      }
		    ?>
		  </select>
		</td>
	</tr>	
		
	<tr>
		<td colspan="4" style="text-align:right;">
  		  <?php
  		   if(empty($reminder))
  		   {
  		  ?>
  		    <input type="submit" name="save_reminders" id="save_reminders" value="Save" /> 
  		  <?php  		   
  		   } else {
  		  ?>
  		    <input type="submit" name="update_reminders" id="update_reminders" value="Save Changes" /> 
  		    <input type="hidden" name="id" id="id" value="<?php echo $reminder['id']; ?>" /> 
  		  <?php
  		   } 
  		  ?>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="text-align:left;"><?php displayGoBack("",""); ?></td>
		<td colspan="2" style="text-align:right;"><?php displayAuditLogLink("reminders_logs" , true); ?></td>
	</tr>																														
</table>
</form>

