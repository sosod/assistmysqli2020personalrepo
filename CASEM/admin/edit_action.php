<?php
$scripts = array( 'add_action.js','menu.js', 'ajaxfileupload.js' );
$styles = array( 'colorpicker.css' );
$page_title = "Edit Action";
require_once("../inc/header.php");

$actionObj 	= new RiskAction("", "", "", "", "", "", "", "", "");
$action     = $actionObj -> getActionDetail($_GET['id']);

$colObj    = new ActionColumns();
$columns   = $colObj -> getHeaderList();

$uaccess 	= new UserAccess( "", "", "", "", "", "", "", "", "");
$users 	    = $uaccess -> getUsers();
?>
<script>
$(function(){
	$("table#edit_action_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php JSdisplayResultObj(""); ?>
<form method="post" name="edit-action-table" id="edit-action-table">
   <table align="left" id="edit_action_table" border="1">
   	<tr>
   	    <td>Action Details</td>
        <td align="right">
          <input type="button" name="show_query" id="show_query" value="Query" />
        &nbsp;&nbsp;&nbsp;
        </td>
   	</tr>   	
   	<tr>
    	<th><?php echo $columns['action_ref']; ?>#:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>   	<tr>
    	<th><?php echo $columns['query_action']; ?>:</th>
        <td><textarea name="action" id="action" cols="80" rows="10"><?php echo $action['action']; ?></textarea></td>
    </tr>
   	<tr>
    	<th><?php echo $columns['action_owner']; ?>:</th>
        <td>
        	<select name="action_owner" id="action_owner">
            	<option>--action owner--</option>
                <?php
                foreach($users as $user)
                {
				?>
                   	<option value="<?php echo $user['tkid']; ?>" 
				    <?php  
				      if($user['tkid']==$action['owner'])
				      { 
				    ?>
                      selected="selected" 
                    <?php } ?>>
			        <?php echo $user['tkname']." ".$user['tksurname']; ?>
                    </option>
                <?php
				}
				?>
            </select>
        </td>
    </tr>
    <tr>
    	<th><?php echo $columns['deliverable']; ?>:</th>
        <td><textarea id="deliverable" name="deliverable" cols="80" rows="10"><?php echo $action['deliverable']; ?></textarea></td>
    </tr>
   	<tr>
    	<th><?php echo $columns['timescale']; ?>:</th>
        <td><input type="text" id="timescale" name="timescale" value="<?php echo $action['timescale']; ?>" /></td>
    </tr>
   	<tr>
    	<th><?php echo $columns['deadline']; ?>:</th>
        <td><input type="text" id="deadline" name="deadline" value="<?php echo $action['deadline']; ?>" class="datepicker" readonly="readonly" /></td>
    </tr>
    <tr>
    	<th>Remind On:</th>
        <td><input type="text" id="remindon" name="remindon" value="<?php echo $action['remindon']; ?>" class="datepicker" readonly="readonly" /></td>
    </tr>
    <tr>
    	<th>Attachment:</th>
        <td>
            <input id="action_attachment_<?php echo $_GET['id']; ?>" name="action_attachment_<?php echo $_GET['id']; ?>" type="file" class="upload_action" />                            
            <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
              <small>You can attach more than 1 file.</small>
            </p>
            <?php
             Attachment::displayAttachmentList($action['attachement'], "action");
            ?>
        </td>
    </tr>    
    <tr>
		<th></th>
    	<td>
        <input type="submit" name="edit_action" id="edit_action" value="Edit Action" class="isubmit"  />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" name="cancel_action" id="cancel_action" value="Cancel" class="idelete"  />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        
        <input type="submit" name="delete_action" id="delete_action" value="Delete" class="idelete"  />        
        <input type="hidden" name="actionid" id="actionid" value="<?php echo $_GET['id'] ?>"  />
        <input type="hidden" name="action_id" id="action_id" value="<?php echo $_GET['id'] ?>" class="logid" />
        </td>
    </tr>
   <tr>
   	<td  align="left" class="noborder">
    	<?php displayGoBack("",""); ?>
    </td>
   	<td align="right" class="noborder">
   		<?php displayAuditLogLink( "action_edit" , false); ?>
    </td>    
   </tr>
   </table>
</form>
<div id="actioneditlog" style="clear:both;"></div>
</div>
