<?php
$scripts = array('search.js','menu.js');
$styles = array( 'colorpicker.css' );
$page_title = "Search";
require_once("../inc/header.php");
?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="search-form" name="search-form">
	<table align="left" class="noborder">
    	<tr>
        	<td class="noborder">Search By </td>
            <td class="noborder">
            	<input type="text" name="search_text" id="search_text" size="50"  /> 
            </td>
            <td class="noborder">
            	<input type="submit" name="go" id="go" value=" Go " />
            </td>
        </tr>
       	<tr>
        	<td class="noborder" align="right" colspan="5">
            	<a href="advanced_search.php" id="advanced_search">Advanced Search</a>
            </td>
        </tr>
    </table>
    <div id="searchresults" align="left" style="clear:both; margin-left:250px; padding-top:20px;">
    	<table id="risk_table"></table>
    </div>
</form>