<?php
include("inc_ignite.php");

$cdate = $_GET['d'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function pageRep(d) {
    document.location.href = "admin_report_deposit_list.php?d="+d;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Report: Deposits by Date Deposited</b></h1>
<?php
    echo("<p><input type=button value='List report' onclick=pageRep(".$cdate.")></p>");
?>

<?php
$c = 1;
$sql = "SELECT * FROM assist_".$cmpcode."_ud_deposit d, assist_".$cmpcode."_timekeep u, assist_".$cmpcode."_ud_claim c, assist_".$cmpcode."_ud_list_bank b, assist_".$cmpcode."_ud_list_type t WHERE u.tkid = c.claimtkid AND t.typeid = c.claimtypeid AND b.bankid = d.udbankid AND d.udid = c.claimudid AND d.uddepdate = '".$cdate."'";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    if($c > 1)
    {
        echo("<div style='page-break-before: always'>");
    }
    $c = $c+1;

?>
<table border="0" id="table2" cellspacing="0" cellpadding="15" bordercolor=006600>
	<tr>
		<td class=tdgeneral>

		<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td colspan="2" valign=top class=tdheader>Claim Details</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Claimed by:</b></td>
		<td colspan="1" class=tdgeneral><?php echo($row['tkname']." ".$row['tksurname']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Client num:</b></td>
		<td class=tdgeneral><?php echo($row['claimclient']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Matter num:</b></td>
		<td class=tdgeneral><?php echo($row['claimmatter']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Invoice num:</b></td>
		<td class=tdgeneral><?php echo($row['claiminvoice']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Type:</b></td>
		<td colspan="1" class=tdgeneral><?php echo($row['type']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral valign=top><b>Comment:</b></td>
		<td colspan="1" class=tdgeneral><?php echo(str_replace(chr(10),"<br>",$row['claimcomment'])); ?>&nbsp;</td>
	</tr>
</table>


		</td>
		<td valign=top>
		<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td colspan="2" class=tdheader>Deposit Details</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Date:</b></td>
		<td class=tdgeneral><?php echo(date("d F Y",$row['uddepdate'])); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Bank:</b></td>
		<td class=tdgeneral><?php echo($row['banktext']); ?> - <?php echo($row['banktype']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Receipt:</b></td>
		<td class=tdgeneral><?php echo($row['udreceipt']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Bank statement:</b></td>
		<td class=tdgeneral><?php echo($row['udbankstat']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Narration:</b></td>
		<td class=tdgeneral><?php echo($row['udnarration']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Amount:</b></td>
		<td class=tdgeneral>R <?php echo(number_format($row['udamount'],2)); ?></td>
	</tr>
	</table>

		</td>
	</tr>
</table>
<hr>
<?php
}
mysql_close();
?>
</body>

</html>
