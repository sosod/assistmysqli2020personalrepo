<?php
include("inc_ignite.php");

$cdate = $_GET['d'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function pageRep(d) {
    document.location.href = "admin_report_deposit_page.php?d="+d;
}
</script>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Report: Deposits by Date Deposited</b></h1>
<?php
    echo("<p><input type=button value='Page report' onclick=pageRep(".$cdate.")></p>");
?>
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader colspan=7>Deposit Details</td>
        <td class=tdheader colspan=5>Claim Details</td>
    </tr>
    <tr>
        <td class=tdheader>Ref</td>
        <td class=tdheader>Date<br>Deposited</td>
        <td class=tdheader>Bank</td>
        <td class=tdheader>Receipt</td>
        <td class=tdheader>Bank<br>Statement</td>
        <td class=tdheader>Narration</td>
        <td class=tdheader>Amount</td>
        <td class=tdheader>Date<br>Claimed</td>
        <td class=tdheader>Claimed By</td>
        <td class=tdheader>Details</td>
        <td class=tdheader>Type</td>
        <td class=tdheader>Comment</td>
    </tr>
<?php

$sql = "SELECT * FROM assist_".$cmpcode."_ud_deposit d, assist_".$cmpcode."_timekeep u, assist_".$cmpcode."_ud_claim c, assist_".$cmpcode."_ud_list_bank b, assist_".$cmpcode."_ud_list_type t WHERE u.tkid = c.claimtkid AND t.typeid = c.claimtypeid AND b.bankid = d.udbankid AND d.udid = c.claimudid AND d.uddepdate = '".$cdate."'";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
?>
    <tr>
        <td valign=top class=tdgeneral><?php echo($row['udid']); ?></td>
        <td valign=top class=tdgeneral><?php echo(date("d F Y",$row['uddepdate'])); ?></td>
        <td valign=top class=tdgeneral><?php echo($row['banktext']." - ".$row['banktype']); ?></td>
        <td valign=top class=tdgeneral><?php echo($row['udreceipt']); ?></td>
        <td valign=top class=tdgeneral><?php echo($row['udbankstat']); ?></td>
        <td valign=top class=tdgeneral><?php echo($row['udnarration']); ?></td>
        <td valign=top class=tdgeneral align=right><?php echo("R ".number_format($row['udamount'],2)); ?></td>
        <td valign=top class=tdgeneral><?php echo(date("d F Y",$row['claimdate'])); ?></td>
        <td valign=top class=tdgeneral><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
        <td valign=top class=tdgeneral><ul><li><?php
$detail = array();
$d = 0;
if(strlen($row['claimclient'])>0)
{
    $detail[$d] = $row['claimclient'];
    $d++;
}
if(strlen($row['claimclientname'])>0)
{
    $detail[$d] = $row['claimclientname'];
    $d++;
}
if(strlen($row['claimmatter'])>0)
{
    $detail[$d] = $row['claimmatter'];
    $d++;
}
if(strlen($row['claimmattername'])>0)
{
    $detail[$d] = $row['claimmattername'];
    $d++;
}
$det = implode("</li><li>",$detail);
echo($det);
 ?></li></ul></td>
        <td valign=top class=tdgeneral><?php echo($row['type']); ?></td>
        <td valign=top class=tdgeneral><?php echo(str_replace(chr(10),"<br>",$row['claimcomment'])); ?>&nbsp;</td>
    </tr>
<?php
}
mysql_close();
?>
</table>
</body>

</html>
