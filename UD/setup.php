<?php
    include("inc_ignite.php");

$result = "";
$udadm = $_POST['udadm'];
$udem = $_POST['udemail'];
$udval = $_POST['udval'];
$a = $_POST['a'];
if(strlen($udadm) > 0 && $a == "admin")
{
    $sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'UD' AND refid = 0";
    include("inc_db_con.php");
    $u = mysql_num_rows($rs);
    if($u == 0)
    {
        $sql2 = "INSERT INTO assist_".$cmpcode."_setup SET ref = 'UD', refid = 0, value = '".$udadm."', comment = 'Setup administrator', field = ''";
    }
    else
    {
        $sql2 = "UPDATE assist_".$cmpcode."_setup SET value = '".$udadm."' WHERE ref = 'UD' AND refid = 0";
    }
    mysql_close();
    $sql = $sql2;
    include("inc_db_con.php");
    $sql = "";
    $sql = "SELECT * FROM assist_".$cmpcode."_ud_list_users WHERE tkid = '".$udadm."' AND yn = 'Y'";
    include("inc_db_con.php");
    $u = mysql_fetch_array($rs);
    if($u == 0)
    {
        $err = "Y";
        $sql = "INSERT INTO assist_".$cmpcode."_ud_list_users SET tkid = '".$udadm."', yn = 'Y'";
    }
    else
    {
        $err = "N";
        $sql = "";
    }
    mysql_close();
    if($err == "Y")
    {
        include("inc_db_con.php");
    }
    $result = "<p><i>Unclaimed Deposits Setup Administrator update complete.</i></p>";
    
}
else
{
    if($a == "email")
    {
        if($udem == "Y")
        {
            $udvalue = $udval;
        }
        else
        {
            $udvalue = $udem;
        }
        if(strlen($udvalue) == 0)
        {
            $result = "<p><b>ERROR:</b> If you say 'Yes' to receiving emails then please enter a valid email address in the box provided.<br>Update not done.</p>";
        }
        else
        {
            $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$udvalue."' WHERE ref = 'UD' AND refid = 1";
            include("inc_db_con.php");
            $result = "<p><i>Email receiving update complete.</i></p>";
        }
    }
}

    include("inc_udadmin.php");
    
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function udEmail() {
    var udemail = document.email.udemail.value;
    var target = document.getElementById('toggle');
    if(udemail == "Y")
    {
        target.style.display = "inline";
        document.email.udval.value = "";
    }
    else
    {
        target.style.display = "none";
    }
}
</script>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Setup</b></h1>
<?php echo($result); ?>
<ul>
    <form name=admin method=post action=setup.php>
        <li>Unclaimed Deposits Administrator: <select name=udadm>
            <?php
                if($udadmin == "0000")
                {
                    echo("<option selected value=0000>Ignite Assist Administrator</option>");
                }
                else
                {
                    echo("<option value=0000>Ignite Assist Administrator</option>");
                }
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000' ORDER BY tkname, tksurname";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $tk = $row['tkname']." ".$row['tksurname'];
                    if($id == $udadmin)
                    {
                        echo("<option selected value=".$id.">".$tk."</option>");
                    }
                    else
                    {
                        echo("<option value=".$id.">".$tk."</option>");
                    }
                }
                mysql_close();
            ?>
        </select>&nbsp;<input type=hidden name=a value=admin><input type=submit value=Update></li>
    </form>
    <form name=email method=post action=setup.php>
        <li>Receive an email when deposits are claimed?:<br><select name=udemail onchange=udEmail()>
            <?php
                if($udemail == "N")
                {
                    echo("<option selected value=N>No</option>");
                }
                else
                {
                    echo("<option value=N>No</option>");
                }
                if($udemail == "A")
                {
                    echo("<option selected value=A>Yes - Admins</option>");
                }
                else
                {
                    echo("<option value=A>Yes - Admins</option>");
                }
                if($udemail != "N" && $udemail != "A")
                {
                    echo("<option selected value=Y>Yes</option>");
                }
                else
                {
                    echo("<option value=Y>Yes</option>");
                }
            ?>
        </select><span id=toggle>&nbsp;Email: <input type=text name=udval value=<?php echo($udemail); ?>></span><input type=hidden name=a value=email>&nbsp;<input type=submit value=Update></li>
    </form>
    <li><a href=setup_banks.php>Banks</a></li>
    <li><a href=setup_users.php>Module Administrators</a></li>
</ul>
<?php
$helpfile = "../help/UD_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
<script language=JavaScript>
var udemail = document.email.udemail.value;
var target = document.getElementById('toggle');
if(udemail == "Y")
{
    target.style.display = "inline";
}
else
{
    target.style.display = "none";
}
</script>
</body>

</html>
