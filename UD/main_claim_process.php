<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    alert("validate");
    var client = me.claimclient.value;
    var matter = me.claimmatter.value;
    var invoice = me.claiminvoice.value;
    if(client.length == 0 && matter.length == 0 && invoice.length == 0)
    {
        alert("Please fill in all the required fields.");
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Claim a deposit</b></h1>
<?php
$udid = $_POST['udid'];
$claimtkid = $_POST['claimtkid'];
$claimclient = $_POST['claimclient'];
$claimmatter = $_POST['claimmatter'];
$claiminvoice = $_POST['claiminvoice'];
$claimcomment = $_POST['claimcomment'];
$claimtypeid = $_POST['claimtypeid'];

$day = date("d",$today);
$mon = date("n",$today);
$year = date("Y",$today);
$claimlistdate = mktime(0,0,0,$mon,$day,$year);

$claimclient = str_replace("'","&#39",$claimclient);
$claimmatter = str_replace("'","&#39",$claimmatter);
$claiminvoice = str_replace("'","&#39",$claiminvoice);
$claimcomment = str_replace("'","&#39",$claimcomment);

$sql = "INSERT INTO assist_".$cmpcode."_ud_claim SET ";
$sql .= "claimudid = ".$udid.", ";
$sql .= "claimtkid = '".$claimtkid."', ";
$sql .= "claimclient = '".$claimclient."', ";
$sql .= "claimmatter = '".$claimmatter."', ";
$sql .= "claiminvoice = '".$claiminvoice."', ";
$sql .= "claimcomment = '".$claimcomment."', ";
$sql .= "claimtypeid = ".$claimtypeid.", ";
$sql .= "claimdate = '".$today."', ";
$sql .= "claimuserid = '".$tkid."', ";
$sql .= "claimlistdate = '".$claimlistdate."'";
include("inc_db_con.php");

$sql2 = str_replace("'","|",$sql);

$sql = "UPDATE assist_".$cmpcode."_ud_deposit SET udclaimyn = 'Y' WHERE udid = ".$udid;
include("inc_db_con.php");

$logtrans = "Deposit ".$udid." claimed: ".$sql2;
$ref = "UD";
include("inc_db_log.php");

$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'UD' AND refid = 1";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r == 0)
{
    $udemail = "N";
}
else
{
    $row = mysql_fetch_array($rs);
    $udemail = $row['value'];
}

if($udemail != "N")
{
    if($udemail == "A")
    {
        $sql = "SELECT t.tkemail FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ud_list_users u WHERE t.tkid = u.tkid AND u.yn = 'Y'";
        include("inc_db_con.php");
        $r = mysql_num_rows($rs);
        if($r > 1)
        {
            $row = mysql_fetch_array($rs);
            $to = $row['tkemail'];
            while($row = mysql_fetch_array($rs))
            {
                $to .= ", ".$row['tkemail'];
            }
        }
        else
        {
            $row = mysql_fetch_array($rs);
            $to = $row['tkemail'];
        }
        mysql_close();
    }
    else
    {
        $to = $udemail;
    }
    
    $sql = "SELECT * FROM assist_".$cmpcode."_ud_deposit, assist_".$cmpcode."_ud_list_bank WHERE udbankid = bankid AND udid = ".$udid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $uddepdate = date("d F Y",$row['uddepdate']);
        $udbank = $row['banktext']." - ".$row['banktype'];
        $udreceipt = $row['udreceipt'];
        $udbankstat = $row['udbankstat'];
        $udnarration = $row['udnarration'];
        $udamount = $row['udamount'];
    mysql_close();
    
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$claimtkid."'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $tkname = $row['tkname']." ".$row['tksurname'];
    mysql_close();
    
    $sql = "SELECT * FROM assist_".$cmpcode."_ud_list_type WHERE typeid = ".$claimtypeid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $claimtype = $row['type'];
    mysql_close();
    
    $comment = str_replace(chr(10),"<br>",$claimcomment);
    
    $subject = "Deposit claimed";
    $message = "<font face=arial>";
    $message = $message."<p>An unclaimed deposit has been claimed.</p>";
    $message = $message."<p><b>The deposit details are:</b><br><small>";
    $message = $message."<b>Date Deposited:</b> ".$uddepdate."<br>";
    $message = $message."<b>Bank:</b> ".$udbank."<br>";
    $message = $message."<b>Receipt:</b> ".$udreceipt."<br>";
    $message = $message."<b>Bank statement:</b> ".$udbankstat."<br>";
    $message = $message."<b>Narration:</b> ".$udnarration."<br>";
    $message = $message."<b>Amount:</b> R ".number_format($udamount,2)."</p>";
    $message = $message."</small><p><b>The claim details are:</b><br><small>";
    $message = $message."<b>Claimed by:</b> ".$tkname."<br>";
    $message = $message."<b>Client number:</b> ".$claimclient."<br>";
    $message = $message."<b>Matter number:</b> ".$claimmatter."<br>";
    $message = $message."<b>Invoice number:</b> ".$claiminvoice."<br>";
    $message = $message."<b>Type:</b> ".$claimtype."<br>";
    $message = $message."<b>Comment:</b> ".$comment."</p>";
    $message = $message."<p>To view a report of all deposits claimed today please log on to Ignite Assist at <b><a href=http://assist.ignite4u.co.za style='color: #cc0001'>http://assist.ignite4u.co.za</a></b>.</p>";
    $message = $message."</small></font>";

    include("../assist_from.php");
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";

    mail($to,$subject,$message,$header);

    $logtrans = "Deposit ".$udid." claim email sent to: ".$to;
    $ref = "UD";
    include("inc_db_log.php");

}
?>
<p>&nbsp;</p>
<p>Claim successful.</p>
<p>&nbsp;</p>
<p><img src=/pics/tri_left.gif align=absmiddle > <a href=main.php class=grey>Go back</a></p>
</body>

</html>
