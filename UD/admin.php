<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Admin</b></h1>
<ul>
    <li><a href=admin_add.php#top>Add unclaimed deposits</a></li>
    <li><a href=admin_add.php#del>Delete unclaimed deposits</a></li>
    <li>View report: List deposits by...</li>
    <ul class=ul2>
        <li><a href=admin_report_claim.php>Date claimed</a></li>
        <li><a href=admin_report_deposit.php>Date deposited</a></li>
    </ul>
</ul>
<?php
$helpfile = "../help/UD_help_moduleadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</body>

</html>
