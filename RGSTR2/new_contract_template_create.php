<?php
$page_redirect_path = "new_contract_template.php?";
//$display_navigation_buttons = false;
include("inc_header.php");


$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "TEMPLATE_CONTRACT";
$temp_id = $_REQUEST['object_id'];

$tempObj = new RGSTR2_TEMPLATE();
$object_id = $tempObj->getConIDFromTemp($temp_id);
//echo ":".$object_id.":";
$myObject = new RGSTR2_CONTRACT(0, "TEMPLATE");
$childObject = new RGSTR2_RISK(0, "TEMPLATE");
//$child_objects = $childObject->getObject(array('type'=>"ALL",'section'=>"NEW",'contract_id'=>$object_id,'limit'=>false,'page'=>"CONFIRM")); 
$child_objects = $childObject->getOrderedObjects($object_id);
ASSIST_HELPER::arrPrint($object_id);
$child_redirect = "new_confirm_details.php?object_type=DELIVERABLE&object_id=";
$child_name = $helper->getDeliverableObjectName(true);


?>
<table class=tbl-container width=100%>
	<tr>
		<td><?php $js.= $displayObject->drawDetailedView("TEMPLATE", $temp_id); ?></td>
	</tr>
	<tr>
		<td><?php $js.= $displayObject->drawDetailedView($object_type, $object_id); ?></td>
	</tr>
	<?php if(isset($childObject)) { ?>
	<tr>
		<td colspan=1>
			<hr />
			<h2><?php echo $child_name; ?></h2>
			<hr />
			<?php 	$gchildObject = new RGSTR2_ACTION(0,"TEMPLATE");
					$gchild_redirect = "new_confirm_details.php?object_type=ACTION&object_id=";
					$gchild_name = $helper->getActionObjectName(true);

			foreach($child_objects[0] as $key=>$item){
				//1 detail display per deliverable
				$js.=$displayObject->drawDetailedView("TEMPLATE_DELIVERABLE", $key, false);
				if($item['del_type'] !== "MAIN"){
					echo "<h3 style='margin-bottom: 5px;'>".$gchildObject->getObjectName("ACTION",true)."</h3>";
					$gchild_objects = $gchildObject->getObject(array('type'=>"ALL",'section'=>"NEW",'deliverable_id'=>$key,'limit'=>false,'page'=>"CONFIRM")); 
					$js.=$displayObject->drawListTable($gchild_objects,false);
				}else{
					if(isset($child_objects[$key])){
						foreach($child_objects[$key] as $key2=>$item){
							$js.=$displayObject->drawDetailedView("TEMPLATE_DELIVERABLE", $key2, false, true);
							echo "<h3 class='sub_head'>".$gchildObject->getObjectName("ACTION",true)."</h3>";
							$gchild_objects = $gchildObject->getObject(array('type'=>"ALL",'section'=>"NEW",'deliverable_id'=>$key2,'limit'=>false,'page'=>"CONFIRM")); 
							$js.=$displayObject->drawListTable($gchild_objects,false, "", array(), true);
						}
					}
				}
			}	
					//All actions of said deliverable
/*
					foreach($gchild_objects['rows'] as $index=>$item){
						//$js.=$displayObject2->drawDetailedView("ACTION", $index, false);
					}
	
 * 
 */				
			?>
		</td>
	</tr>
	<?php }  ?>
</table>

<div id="confirm_div" style="display:none">
	<h3>Customise this contract</h3>
	<p>Capture the following information:</p><br>
		<table>
			<tr>
				<th>Contract name</th>				
		 		<td>
					<form id="con_details" >
			 			<input req="1" name="contract_name" id="con_name" type="text" />
					</form>
		 		</td>
			</tr>
		</table>
</div>
<script type="text/javascript">
var url = "<?php echo $child_redirect; ?>";
$(function() {
	<?php echo $js; ?>
	$('#make_con').button().click(function(){
			$('#confirm_div').show().dialog({
			modal:true,
			buttons:[{
					text:"Okay",
					click:function(){
						AssistHelper.processing();
					 	$form = $("#con_details");
			//??		 	var serialized = CntrctHelper.processObjectForm($form, "Template.Add&id=<?php //echo $object_id; ?>", "admin_template_new.php?");
					 	var serialize = AssistForm.serialize($form);
						var tempdata = AssistHelper.doAjax("inc_controller.php?action=Template.Get&id="+<?php echo $object_id; ?>,serialize);
					//1	var result = AssistHelper.doAjax("inc_controller.php?action=Contract.Add&", tempdata[0]);
					//	var result = AssistHelper.doAjax("inc_controller.php?action=Deliverable.Add&", tempdata);
					//3	var result = AssistHelper.doAjax("inc_controller.php?action=Contract.Add&", tempdata);
						$(this).dialog("close");
						//AssistHelper.finishedProcessing(result[0],result[1]);
					 	console.log(tempdata);
						setTimeout(function(){
						//	document.location.href = "new_contract_edit.php?object_id="+result[2];
						},2000);
				 	}
				},{
					text:"Cancel",
					click:function(){
						$(this).dialog("close");
					}
			}]
		});
		});
	$("h2").css({"margin-top":"10px","margin-bottom":"5px"});
	$("#tbl_summ th").addClass("th2");
	$("#tbl_summ tr:first th:first").css("background-color","#ffffff");
	$("#tbl_summ tr:gt(0) th").css("text-align","left");
	$("#tbl_summ tr:gt(0)").each(function() { $(this).find("td:eq(0)").css("text-align","center"); });
	$("input:button.btn_view").click(function() {
		var i = $(this).attr("ref");
		url = url+i;
		if($(this).hasClass("action")){
			url = url + "&object_type=ACTION";
		}else if($(this).hasClass("deliverable")){
			url = url + "&object_type=DELIVERABLE";
		}else{
			url = url + "&object_type=CONTRACT";
		}
		document.location.href = url;
	});
});
</script>