<?php
include("inc_header.php");

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

$page_action = "UPDATE";

switch($object_type) {
	case "CONTRACT":
	case "REGISTER":
		$myObject = new RGSTR2_REGISTER();
		$page_redirect = "manage_update_contract.php?";
		break;
	case "DELIVERABLE":
	case "RISK":
		$myObject = new RGSTR2_RISK();
		$page_redirect = "manage_update_deliverable.php?";
		break;
	case "ACTION":
		$myObject = new RGSTR2_ACTION();
		$page_redirect = "manage_update_action.php?";
		break;
}

$object = $myObject->getObjectForUpdate($object_id);

?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Update</h2><?php 
			include("common/generic_object_update_form.php");
			$js.= $helper->drawPageFooter("",strtolower($object_type),"object_id=".$object_id."&log_type=".RGSTR2_LOG::UPDATE);
		?></td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td colspan=1>
			<?php  ?>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>
});
</script>