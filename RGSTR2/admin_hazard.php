<?php

require_once("inc_header.php");

$dialog_url = "admin_hazard_edit_dialog.php";

$_REQUEST['object_type'] = "HAZARD";
$page_action = "EDIT";
$page_section = "NEW";
$child_object_type = "HAZARD";
$button_label = "|edit|";
$button_icon = "pencil";



$add_button = true;
$add_button_label = "|add| |hazard|";
$add_button_function = "showAddDialog();";

//$page_direct = "admin_hazard.php";
$page_direct = "admin_hazard_view.php";


//$alternative_button_click_function = "editObject";
include("common/generic_comp_list_page.php");

?>

<div id=dlg_add_hazard title='<?php echo $helper->replaceAllNames("|add| |competency|"); ?>'>
    <?php

    $section = "NEW";
    $page_redirect_path = "admin_hazard.php?";
    $page_action = "Add";

    $uaObject = new RGSTR2_USERACCESS();

    $child_object_type = "HAZARD";
    $child_object_id = 0;
    $childObject = new RGSTR2_HAZARD();

    $data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
    echo $data['display'];

    ?>
</div>

<div id=dlg_child class=dlg-child title="">
    <iframe id=ifr_form_display style="border:0px solid #000000;" src="">

    </iframe>
</div>



<script type="text/javascript" >
    $(function() {
        $("#dlg_add_hazard").dialog({
            autoOpen: false,
            modal: true
        });
        <?php echo $data['js']; ?>



    });

    function showAddDialog() {
        $(function() {
            $("#dlg_add_hazard").dialog("open");
            var my_window = AssistHelper.getWindowSize();
            if(my_window['width']>800) {
                $("#dlg_add_hazard form[name=frm_object] table.form:first").css("width","800px");
                $("#dlg_add_hazard").dialog("option","width",850);
            } else {
                $("#dlg_add_hazard form[name=frm_object] table.form:first").css("width",(my_window['width']-100)+"px");
                $("#dlg_add_hazard").dialog("option","width",(my_window['width']-50));
            }
            $("#dlg_add_hazard").dialog("option","height",my_window['height']-50);
        });
    }
    <?php

    $t1_object_name = $childObject->getMyObjectName();
    $t1_object_type = $childObject->getMyObjectType();
    echo "
	var object_names = new Array();
	object_names['tier1'] = '".$helper->getObjectName($t1_object_name)."';
	object_names['".$t1_object_type."'] = '".$helper->getObjectName($t1_object_name)."';
	";
    ?>
    var my_window = AssistHelper.getWindowSize();
    //alert(my_window['height']);
    if(my_window['width']>800) {
        var my_width = 850;
    } else {
        var my_width = 800;
    }
    var my_height = my_window['height']-50;

    $("div.dlg-child").dialog({
        autoOpen: false,
        modal: true,
        width: my_width,
        height: my_height,
        beforeClose: function() {
            AssistHelper.closeProcessing();
        },
        open: function() {
            $(this).dialog('option','width',my_width);
            $(this).dialog('option','height',my_height);
        }
    });
    function editObject($me) {
        //alert("edit :"+$me.attr('ref')+":");
        $(function() {
            AssistHelper.processing();
            var i = $me.attr('ref');
            var t = "HAZARD";
            $dlg = $("div#dlg_child");
            act = "edit";
            var obj = t.toLowerCase();
            var heading = AssistString.ucwords(act)+" "+object_names[t];
            var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&object_type="+t+"&object_id="+i;

            $dlg.dialog("option","title",heading);
            //$dlg.find("iframe").prop("src",url);
            $dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
            //FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
            $dlg.dialog("open");
        });
    }

    function dialogFinished(icon,result) {
        if(icon=="ok") {
            document.location.href = '<?php echo $page_redirect_path; ?>r[]=ok&r[]='+result;
        } else {
            AssistHelper.processing();
            AssistHelper.finishedProcessing(icon,result);
        }
    }
</script>
