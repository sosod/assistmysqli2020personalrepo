<?php
require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);


$head_name = $_REQUEST['head_name'];
$sub_name = $_REQUEST['head_name'];

$helper = new RGSTR2();

$deptObject = new RGSTR2_CONTRACT_OWNER();
$displayObject = new RGSTR2_DISPLAY();
$admins_list = $deptObject->getAllActiveAdmins();

$dept_id = $_REQUEST['dept_id'];
$record_id = $_REQUEST['record_id'];

$user_record = $admins_list[$dept_id][$record_id];

$user_status = $user_record['status'];

//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($admins_list[$dept_id][$record_id]);
//echo '</pre>';

$js = "";
?>

<h2>Edit <?php echo $head_name; ?></h2>
<form name=frm_edit>
    <input type="hidden" name="dept_id" id="dept_id" value="<?php echo $dept_id; ?>" />
    <input type="hidden" name="record_id" id="record_id" value="<?php echo $record_id; ?>" />

    <table class=form style='margin-left: 5px; margin-right: 25px !important; width: 98%'>
        <tr>
            <th width=150px>Section:</th>
            <td id=td_add_dept><?php echo $_REQUEST['dept_name']; ?></td>
        </tr>
        <tr>
            <th><?php echo $sub_name; ?>:</th>
            <td><?php echo $user_record['name'];?></td>
        </tr>
<!--        <tr>-->
<!--            <th>Create Deliverable Access (Manage):</th>-->
<!--            <td>-->
<!--                --><?php
//                $name_and_id = 'create';
//                $bool_button_value = (($user_record['status'] & RGSTR2_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE)==RGSTR2_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE ? true : false);
//                $js .= $displayObject->drawFormField('BOOL_BUTTON', array('id' => $name_and_id, 'name' => $name_and_id, 'required' => false), $bool_button_value);
//                ?>
<!--            </td>-->
<!--        </tr>-->
        <tr>
            <th style="white-space:nowrap;">Update <?php echo $helper->replaceAllNames("|risks|"); ?> Access (Manage):</th>
            <td>
                <?php
                $name_and_id = 'update';
                $bool_button_value = (($user_record['status'] & RGSTR2_CONTRACT_OWNER::CAN_UPDATE)==RGSTR2_CONTRACT_OWNER::CAN_UPDATE ? true : false);
                $js .= $displayObject->drawFormField('BOOL_BUTTON', array('id' => $name_and_id, 'name' => $name_and_id, 'required' => false, 'small_size'=>true), $bool_button_value);
                ?>
            </td>
        </tr>
        <tr>
            <th style="white-space:nowrap;">Edit <?php echo $helper->replaceAllNames("|actions|"); ?> Access (Manage):</th>
            <td>
                <?php
                $name_and_id = 'edit';
                $bool_button_value = (($user_record['status'] & RGSTR2_CONTRACT_OWNER::CAN_EDIT)==RGSTR2_CONTRACT_OWNER::CAN_EDIT ? true : false);
                $js .= $displayObject->drawFormField('BOOL_BUTTON', array('id' => $name_and_id, 'name' => $name_and_id, 'required' => false, 'small_size'=>true), $bool_button_value);
                ?>
            </td>
        </tr>
        <tr>
            <th style="white-space:nowrap;">Approve Completed <?php echo $helper->replaceAllNames("|actions|"); ?> Access (Manage):</th>
            <td>
                <?php
                $name_and_id = 'approve';
                $bool_button_value = (($user_record['status'] & RGSTR2_CONTRACT_OWNER::CAN_APPROVE)==RGSTR2_CONTRACT_OWNER::CAN_APPROVE ? true : false);
                $js .= $displayObject->drawFormField('BOOL_BUTTON', array('id' => $name_and_id, 'name' => $name_and_id, 'required' => false, 'small_size'=>true), $bool_button_value);
                ?>
            </td>
        </tr>
    </table>
</form>



<script type="text/javascript">
    $(function() {
        <?php echo $js; ?>

        //Tell parent window to open the dialog once the page has finished loading
        window.parent.openDialog();
//        window.parent.changeDialogButton();
    });
</script>