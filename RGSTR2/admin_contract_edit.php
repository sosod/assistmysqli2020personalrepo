<?php
$object_type = "CONTRACT";
$object_id = $_REQUEST['object_id'];

$section = "ADMIN";
$page_redirect_path = "admin_contract.php?";
$page_action = "Edit";

require_once("inc_header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$child_object_type = "REGISTER";
$child_object_id = $object_id;
$childObject = new RGSTR2_REGISTER($object_id);
?>
<table class=tbl-container>
	<tr>
		<td width=60%><h2><?php echo $helper->getObjectName($object_type); ?> Details</h2><?php 
$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
echo $data['display'];
$js.=$data['js'];
			//include("common/generic_object_form.php");
			$js.= $helper->drawPageFooter($helper->getGoBack('admin_contract.php'),strtolower($object_type),"object_id=".$object_id."&log_type=".RGSTR2_LOG::EDIT);
		?></td>
		<td>&nbsp;</td>
	</tr>
</table>
<?php
//Get the register id for this particular risk
$reg_obj = $childObject;
////Auto-populate Acceptable Risk Exposure
$reg_sql = 'SELECT * FROM ' . $reg_obj->getTableName() . ' ';
$reg_sql .= 'WHERE register_id = ' . $object_id . ' ';
$reg_details = $helper->mysql_fetch_one($reg_sql);
$register_max_acceptable_exposure = $reg_details['register_max_acceptable_exposure'];
?>
<script type=text/javascript>
$(function() {
	<?php 	echo $js;	?>
});
</script>

<script>
	$(function() {
		$("#register_max_acceptable_exposure").val(<?php  echo $register_max_acceptable_exposure; ?>);
	});
</script>