<?php
include("inc_header.php");

$display_type = !isset($display_type) ? "default" : $display_type; 

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

$page_action = (isset($page_section) && strlen($page_section)>0 ? $page_section."." : "")."Edit";


switch($object_type) {
	case "CONTRACT":
	case "REGISTER":
		//Object type converstions == Start
		$object_type = 'REGISTER';
		//Object type converstions == End

		$myObject = new RGSTR2_REGISTER($object_id);
		
		break;
	case "DELIVERABLE":
	case "RISK":
		//Object type converstions == Start
		$object_type = 'RISK';
		//Object type converstions == End


		$childObject = new RGSTR2_RISK($object_id);
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new RGSTR2_REGISTER($parent_id);
		$parent_object_type = "REGISTER";
		break;
	case "ACTION":
		$childObject = new RGSTR2_ACTION($object_id);
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new RGSTR2_RISK($parent_id);
		$parent_object_type = "RISK";
		break;
}
?>
<table class=tbl-container>
	<tr>
	<?php if($display_type!= "dialog" && $object_type!="CONTRACT" && $object_type!="REGISTER") { ?>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($parent_object_type, $parent_id, true); ?></td>
		<td width=5%>&nbsp;</td>
	<?php } ?>
		<td width=48%>
		<?php if($display_type!="dialog") { ?>
			<h2><?php echo $helper->getObjectName($object_type); ?> Details</h2><?php
		} 
			//include("common/generic_object_form.php");
			$data = $displayObject->getObjectForm($object_type, $childObject, $object_id, $parent_object_type, $myObject, $parent_id, $page_action, $page_redirect_path);
echo $data['display'];
$js.=$data['js'];
if(!isset($page_section) || $page_section!="NEW") {
			$js.= $displayObject->drawPageFooter($helper->getGoBack($page_redirect_path),strtolower($object_type),"object_id=".$object_id."&log_type=".RGSTR2_LOG::EDIT);
} elseif($page_redirect_path!="dialog") {
			$js.= $displayObject->drawPageFooter($helper->getGoBack($page_redirect_path));
}
		?></td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	if($object_type=="DELIVERABLE") {
		echo "$(\"#del_legislation_section_id\").val(del_legislation_section_value).trigger('change');";
	}
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($object_type,"dlg_child",$_REQUEST);
	}
	?>
});
</script>