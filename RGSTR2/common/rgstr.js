function boolButtonClickExtra($btn) {
	var i = $btn.prop("id");
	var us = i.lastIndexOf("_");
	var tr = "tr_"+i.substr(0,us);
	var act = i.substr(us+1,3);
	if(act=="yes") {
		$("tr."+tr).show();
	} else {
		$("tr."+tr).hide();
	}
}

function processCurrency($inpt) {
	var h = $inpt.parents("tr").children("th").html();
	h = h.substr(0,h.lastIndexOf(":"));
	alert("Only numbers (0-9) and a period (.) are permitted in the "+h+" field.");		
}

$(function() {

	$("#del_type").change(function() {
		if($(this).val()=="SUB") {
			$(".tr_del_type").show();
		} else {
			$(".tr_del_type").hide();
		}
	});
	
	$("form select").each(function() {
		if($(this).children("option").length==2) {
			if($(this).children("option:first").val()=="X") {
				$(this).children("option:last").prop("selected",true);
			}
		}
	});
	
	
	$("input:visible, select, textarea").blur(function() {
		$(this).removeClass("required");
});

});