<?php
include("inc_header.php");

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

$page_action = "UPDATE";

switch($object_type) {
	case "CONTRACT":
	case "REGISTER":
		$myObject = new RGSTR2_REGISTER();
		$page_redirect = $page_section."_update_contract.php?";
		$parent_object_type = null;
		break;
	case "DELIVERABLE":
	case "RISK":
		$myObject = new RGSTR2_RISK();
		$page_redirect = $page_section."_update_deliverable.php?";
		$parent_object_type = "REGISTER";
		break;
	case "ACTION":
		$myObject = new RGSTR2_ACTION();
		$parent_object_type = "RISK";
		if(isset($_REQUEST['page_redirect'])) {
			$page_redirect = $_REQUEST['page_redirect'];
		} else {
			$page_redirect = $page_section."_update_action.php?";
		}
		break;
}

$object = $myObject->getObjectForUpdate($object_id);
//ASSIST_HELPER::arrPrint($myObject->getRecipients($object_id));
?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Update</h2><?php 
			//include("common/generic_object_update_form.php");
			$data = $displayObject->getObjectForm($object_type, $myObject, $object_id, $parent_object_type, null, 0, $page_action, $page_redirect);
echo $data['display'];
$js.=$data['js'];
			$js.= $displayObject->drawPageFooter("",strtolower($object_type),"object_id=".$object_id."&log_type=".RGSTR2_LOG::UPDATE);
		?></td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td colspan=1>
			<?php  ?>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>

});
</script>

<!--<div id=dlg_parent_details title="Select lines">-->
<!--    <iframe id=ifr_lines></iframe>-->
<!--</div>-->
<script>
//    $(function() {
//
//        //Determine the necessary width and height settings for the dlg_parent_details & ifr_lines elements
//        var scr = AssistHelper.getWindowSize();
//        var dlg_width = scr['width']*0.5;
//        var dlg_height = scr['height']*0.95;
//        var ifr_width = dlg_width-20;
//        var ifr_height = dlg_height-100;
//
//        /**
//         * "Go" button - process the selected Source and Filter and display the available lines in the dlg_parent_details dialog pop-up
//         */
//        $('input:button.btn_parent').button()
//            .click(function(e) {
//                e.preventDefault();
//                AssistHelper.processing();
//                $("#dlg_parent_details").dialog("option","title","Details");
//
//                var child_id = '<?php //echo $object_id; ?>//';
//                var child_type = '<?php //echo $object_type; ?>//';
//                var parent_type = $(this).attr('id');
//
//                var url = "manage_update_dlg_parent_details.php?child_id="+ child_id +"&child_type="+ child_type + "&parent_type=" + parent_type;
//
//
//                //call iframe in dialog
//                $("#ifr_lines").prop("src",url);
//                //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
//                $("#dlg_parent_details").dialog("open");
//            });
//
//        /**
//         * Dialog to display available lines after clicking the "Go" button
//         */
//        $("#dlg_parent_details").dialog({
//            autoOpen: false,
//            modal: true,
//            width: dlg_width,
//            height: dlg_height,
//            buttons: [{
//                text:"Close",
//                icons: {primary: "ui-icon-close"},
//                click:function(e) {
//                    e.preventDefault();
//                    //Close the dialog
//                    $("#dlg_parent_details").dialog("close");
//                    AssistHelper.closeProcessing();
//                }
//            }]
//        });
//        AssistHelper.formatDialogButtonsByClass($("#dlg_parent_details"),0,"ui-state-green");
//        /**
//         * Iframe within the dlg_parent_details div
//         */
//        $("#ifr_lines").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");
//
//
//        $('#dlg_parent_details').on('dialogclose', function(event) {
//            AssistHelper.closeProcessing();
//        });
//    });
</script>