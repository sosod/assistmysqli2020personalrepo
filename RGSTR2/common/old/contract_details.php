<?php

/**
 * REQUIRES preset variable: $contract_id to contain the id of the contract being displayed
 */
if(!isset($contract_id)) {
	$contract_id = $_REQUEST['contract_id'];
}

$contractHeadings = $headingObject->getMainObjectHeadings("CONTRACT");

$contractObject = new RGSTR2_CONTRACT($contract_id);
$contract = $contractObject->getObject(array('type'=>"DETAILS"));
//ASSIST_HELPER::arrPrint($contract);
?>
<table class=form><?php
	foreach($contractHeadings['rows'] as $fld => $head) {
		if($head['parent_id']==0) {
			$h_type = $head['type'];
			if($h_type!="HEADING" && $h_type!="CONTRACT_SUPPLIER") {
				$val = $contract[$fld];
				if($h_type=="LIST" || $h_type == "OWNER" || $h_type == "MASTER") {
					$val = $contract[$head['list_table']];
					if($val=="0") { $val = ""; }
				} elseif($h_type=="DATE") {
					if(strtotime($val)>0) {
						$val = date("d-M-Y",strtotime($val));
					} else {
						$val = "N/A";
					}
				} elseif($h_type=="CURRENCY") {
					$val = ASSIST_HELPER::format_currency($val);
				} elseif($h_type=="BOOL"){
					$val = $val==1 ? $helper->getDisplayIconAsDiv("ok")." Yes" : $helper->getDisplayIconAsDiv("0")."No";
				} elseif($h_type=="REF") {
					$val = $contractObject->getRefTag().$contract_id;
				} elseif($h_type=="TEMPLATE") {
					$val = $val==0?"":$val;
				}
			} elseif($fld=="contract_supplier") {
				$sub_head = $contractHeadings['sub'][$head['id']];
				$val = "";
				if(count($contract[$fld])>0) {
					$cs = array();
					foreach($contract[$fld] as $supp) {
						foreach($sub_head as $fld=>$shead) {
							$v = $supp[$shead['field']];
							switch($shead['type']) {
								case "LIST": $v = $supp[$shead['list_table']]; break;
								case "CURRENCY": $v = ASSIST_HELPER::format_currency($v); break;
							}
							$cs[] = "<tr><th class=th2 width=40%>".$shead['name'].":</th><td>".$v."</td></tr>";
						}
					}
					$val = "<table width=100% class=sub_form>".implode("",$cs)."</table>";
				}
			} elseif($fld=="contract_assess") {
				$sub_display = true;
				$sub_head = $contractHeadings['sub'][$head['id']];
				$val = "";
					$cs = array();
					foreach($sub_head as $fld=>$shead) {
						$v = $shead['field'];
						switch($v) {
							case "contract_assess_other_name":
								if($contractObject->mustIAssessOther()) { 
									$v = $contract[$v];
								} else {
									$sub_display = false;
								} 
								break;
							case "contract_assess_qual": 	$cas = !isset($cas) ? $contractObject->mustIAssessQuality() : $cas;
							case "contract_assess_quan": 	$cas = !isset($cas) ? $contractObject->mustIAssessQuantity() : $cas;
							case "contract_assess_other":	$cas = !isset($cas) ? $contractObject->mustIAssessOther() : $cas; 
								//$v = $contract['contract_assess_type'];
								//if(($v & $cas) == $cas) {
								if($cas) {
									$v = ASSIST_HELPER::getDisplayIconAsDiv("ok")." Yes";
								} else {
									$v = ASSIST_HELPER::getDisplayIconAsDiv("0")." No";
								}
								break;
						}
						if($sub_display) {
							$cs[] = "<tr><th class=th2 width=40%>".$shead['name'].":</th><td>".$v."</td></tr>";
						}
						unset($cas);
					}
					$val = "<table width=100% class=sub_form>".implode("",$cs)."</table>";
				
			} elseif($fld=="contract_assessment_statuses") {
				$val = $contract['contract_assess_status'];
					$listObject = new RGSTR2_LIST("deliverable_status");
					$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
					$last_deliverable_status = 0;
					$sub_head = array();
					$sh_type = "BOOL";
					foreach($list_items as $key => $status) {
						$sub_head[$key] = array(
							'field'=>$key,
							'type'=>$sh_type,
							'name'=>$status,
							'required'=>1,
							'parent_link'=>"",
						);
						$last_deliverable_status = $key;
					}
				$td = "<table class=sub_form width=100%>";
					foreach($sub_head as $key => $shead) {
						$sh_type = $shead['type'];
						$sfld = $shead['field'];
						if($sh_type=="BOOL"){
							$test = ($val & pow(2,$key)) == pow(2,$key);
							$sval = $test!=false ? $helper->getDisplayIconAsDiv("ok")." Yes" : $helper->getDisplayIconAsDiv("0")." No";
						}
						$td.="
						<tr >
							<th class=th2 width=40%>".$shead['name'].":</th>
							<td>".$sval."</td>
						</tr>";
					}
				$td.= "
					</table></span>
				</div>";
				$val = $td;
			} else {
				$val = $fld;
			}
			echo "
			<tr >
				<th width=40%>".$head['name'].":</th>
				<td>".$val."
				</td>
			</tr>";
		}
	}
		?>
</table>