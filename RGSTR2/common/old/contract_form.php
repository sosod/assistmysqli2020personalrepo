<?php
if(isset($contract_id)) {
	$contractObject = new RGSTR2_REGISTER();
	$contract = $contractObject->getRawObject($contract_id);
} else {
	$contract = array();
	$contract_id = 0;
}


$headings = $headingObject->getMainObjectHeadings("CONTRACT");
//ASSIST_HELPER::arrPrint($headings);



?>
<div id=div_error class=div_frm_error>
	
</div>
<form name=frm_contract>
	<table class=form><?php
	$form_valid8 = true;
	$form_error = array();
	foreach($headings['rows'] as $fld => $head) {
		if($head['parent_id']==0) {
			$val = "";
			$h_type = $head['type'];
			if($h_type!="HEADING") {
				$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
				if($head['required']==1) { $options['title'] = "This is a required field."; }
				if($h_type=="LIST") {
					$list_items = array();
					$listObject = new RGSTR2_LIST($head['list_table']);
					$list_items = $listObject->getActiveListItemsFormattedForSelect();
					$options['options'] = $list_items;
					if(count($list_items)==0 && $head['required']==1) {
						$form_valid8 = false;
						$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
						$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
					}
					if($page_action=="Edit") {
						$val = isset($contract[$fld]) && strlen($contract[$fld])>0 && $contract[$fld]>0 ? $contract[$fld] : "X";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
					}
				} elseif(in_array($h_type,array("MASTER","USER","OWNER","TEMPLATE"))) {
					$list_items = array();
					switch($h_type) {
						case "USER":
							$userObject = new RGSTR2_USERACCESS();
							$list_items = $userObject->getActiveUsersFormattedForSelect();
							break; 
						case "TEMPLATE":
							$templateObject = new RGSTR2_TEMPLATE();
							$list_items = $templateObject->getActiveTemplatesFormattedForSelect();
							break;
						case "OWNER":
							$ownerObject = new RGSTR2_CONTRACT_OWNER();
							$list_items = $ownerObject->getActiveOwnersFormattedForSelect();
							break;
						case "MASTER":
							$masterObject = new RGSTR2_MASTER($head['list_table']);
							$list_items = $masterObject->getActiveItemsFormattedForSelect();
							break; 
						default:
							echo $h_type; 
							break;
					}
					$options['options'] = $list_items;
					$h_type = "LIST";
					if(count($list_items)==0 && $head['required']==1) {
						$form_valid8 = false;
						$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
						$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
					}
					if($page_action=="Edit") {
						$val = isset($contract[$fld]) && strlen($contract[$fld])>0 && $contract[$fld]>0 ? $contract[$fld] : "X";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
					}
				} elseif($h_type=="DATE") {
					$options['options'] = array();
					$options['class'] = "";
					if($page_action=="Edit") {
						$val = isset($contract[$fld]) && strlen($contract[$fld])>0 && $contract[$fld]>0 && strtotime($contract[$fld])>0 ? date("d-M-Y",strtotime($contract[$fld])) : "";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
					}
				} elseif($h_type=="CURRENCY") {
					$options['extra'] = "processCurrency";
					if($page_action=="Edit") {
						$val = isset($contract[$fld]) && strlen($contract[$fld])>0 && $contract[$fld]>0 ? $contract[$fld] : "";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
					}
				} elseif($h_type=="BOOL"){
					$h_type = "BOOL_BUTTON";
					$options['yes'] = 1;
					$options['no'] = 0;
					$options['extra'] = "boolButtonClickExtra";
					if($page_action=="Edit") {
						$val = isset($contract[$fld]) && strlen($contract[$fld])>0 && $contract[$fld]>0 ? $contract[$fld] : "0";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
					}
				} elseif($h_type=="REF") {
					if($page_action=="Add") {
						$val = "System Generated";
					} else {
						$val = RGSTR2_CONTRACT::REFTAG.$contract_id;
					}
				} else {
					if($page_action=="Edit") {
						$val = isset($contract[$fld]) ? $contract[$fld] : "";
					} else {
						$val = isset($head['default_value']) ? $head['default_value'] : "";
					}
				}
				$display = $displayObject->createFormField($h_type,$options,$val);
				$js.=$display['js'];
			} else {//if($fld=="contract_supplier") {
				if($fld=="contract_assessment_statuses") {
					$listObject = new RGSTR2_LIST("deliverable_status");
					$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
					$last_deliverable_status = 0;
					$sub_head = array();
					$sh_type = "BOOL";
					foreach($list_items as $key => $status) {
						$sub_head[$fld."-".$key] = array(
							'field'=>$fld."-".$key,
							'type'=>$sh_type,
							'name'=>$status,
							'required'=>1,
							'parent_link'=>"",
						);
						$last_deliverable_status = $key;
					}
				} else {
					$sub_head = $headings['sub'][$head['id']];
				}
				$td = "
				<div class=".$fld."><span class=spn_".$fld.">
					<table class=sub_form width=100%>";
					$add_another[$fld] = false;
					foreach($sub_head as $shead) {
						$sh_type = $shead['type'];
						$sfld = $shead['field'];
						if($fld=="contract_supplier") {
							$options = array('name'=>$sfld."[]",'req'=>$head['required']);
						} else {
							$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
						}
						$val = "";
						if($sh_type=="LIST") {
							$list_items = array();
							$listObject = new RGSTR2_LIST($shead['list_table']);
							$list_items = $listObject->getActiveListItemsFormattedForSelect();
							$options['options'] = $list_items;
							if(count($list_items)==0 && $shead['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The".$shead['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if(count($list_items)>1) {
								$add_another[$fld] = true;
							}
						} elseif($sh_type=="BOOL"){
							$sh_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							$val = 1;
						} elseif($sh_type=="CURRENCY") {
							$options['extra'] = "processCurrency";
							$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
						}
						$sdisplay = $displayObject->createFormField($sh_type,$options,$val);
						$js.= $sdisplay['js'];
						$td.="
						<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
							<th class=th2>".$shead['name'].":</th>
							<td>".$sdisplay['display']."</td>
						</tr>";
					}
				$td.= "
					</table></span>
					".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
				</div>";
				$display = array('display'=>$td);
			} 
			echo "
			<tr ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
				<th>".$head['name'].":".($head['required']==1?"*":"")."</th>
				<td>".$display['display']."
				</td>
			</tr>";
		}
	}
	if(!$form_valid8) {
		$js.="
		$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
		AssistHelper.hideDialogTitlebar('id','div_error');
		AssistHelper.hideDialogCSS('class','dlg_frm_error');
		$('#div_error #btn_error').button().click(function() {
			$('#div_error').dialog(\"close\");
		}).blur().parent('p').addClass('float');
		$('html, body').animate({scrollTop:0});
		";
	}
		?><tr>
			<th></th>
			<td>
				<input type=button value="Save new <?php echo $contract_object_name; ?>" class=isubmit id=btn_add />
				<input type=hidden name=last_deliverable_status value="<?php echo $last_deliverable_status; ?>" />
				<input type=hidden name=contract_id value="<?php echo $contract_id; ?>" />
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$(function() {
		var page_action = "<?php echo $page_action; ?>";
		var error_html = "<p>The following errors need to be attended to before any <?php echo $contract_object_name."s"; ?> can be added:</p> <ul><li><?php echo implode('</li><li>',$form_error); ?></li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>";
		<?php echo $js; ?>
		$("form[name=frm_add] select").each(function() {
			if($(this).children("option").length<=1 && !$(this).hasClass("required")) {
//				$(this).prop("disabled",true);
			}
		});
		$("#btn_add").click(function() {
			$form = $("form[name=frm_contract]");
			var dta = AssistForm.serialize($form); //$("<div />",{html:dta}).dialog("width:500px");
			var result = AssistHelper.doAjax("inc_controller.php?action=Contract."+page_action,dta);
			if(result[0]=="ok") {
				document.location.href = '<?php echo $page_redirect_path; ?>r[]='+result[0]+'&r[]='+result[1];
			} else {
				alert(result[1]);
			}
		});
		
		function boolButtonClickExtra($btn) {
			var i = $btn.prop("id");
			var us = i.lastIndexOf("_");
			var tr = "tr_"+i.substr(0,us);
			var act = i.substr(us+1,3);
			if(act=="yes") {
				$("tr."+tr).show();
			} else {
				$("tr."+tr).hide();
			}
		}
		
		function processCurrency($inpt) {
			var h = $inpt.parents("tr").children("th").html();
			h = h.substr(0,h.lastIndexOf(":"));
			alert("Only numbers (0-9) and a period (.) are permitted in the "+h+" field.");		
		}
		
		<?php 
		if(isset($add_another) && count($add_another)>0) {
			foreach($add_another as $key => $aa) {
				if($aa==true) {
					echo "
					var ".$key." = $('div.".$key." span:first-child').html();
					$('#btn_".$key."').click(function() {
						$('div.".$key." span').children('table:last').after(".$key.");
					});
					";
				}
			}
		}
		?>
		
	});
</script>