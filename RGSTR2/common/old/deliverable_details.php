<?php

/**
 * REQUIRES preset variable: $contract_id to contain the id of the contract being displayed
 */
if(!isset($object_id)) {
	$object_id = $_REQUEST['deliverable_id'];
}

$myObject = new RGSTR2_RISK($object_id);
$object = $myObject->getObject(array('type'=>"DETAILS",'id'=>$object_id));
//ASSIST_HELPER::arrPrint($object);
?>
<input type=button value='Contract Details' class=float />
<h2>Details</h2>
<table class=form>
	<?php
	foreach($object['head'] as $fld => $head) {
		echo "
		<tr>
			<th width=40%>".$head['name'].":</th>
			<td>".$object['rows'][$fld]."</td>
		</tr>";
	}
	?>
</table>
<script type="text/javascript">
	$(function() {
		$("input:button.float").button();
	});
</script>