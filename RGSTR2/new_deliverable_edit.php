<?php
//$parent_object_type = $_REQUEST['object_type'];
//$parent_object_id = $_REQUEST['object_id'];

require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

//$page_redirect_path = "new_deliverable_add.php?object_type=".$parent_object_type."&object_id=".$parent_object_id."&";
$page_action = "Edit";
$page_section = "NEW";

$_REQUEST['object_type'] = "RISK";

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];

$myObject = new RGSTR2_RISK($object_id);
$parent_object_id = $myObject->getParentID($object_id);
$parent_object_type = "REGISTER";

$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "default";
if($display_type=="dialog") {
	$no_page_heading = true;
	$page_redirect_path = "dialog";
} else {
	$page_redirect_path = "new_deliverable_edit_object.php?object_id=".$object_id."&";
}


include("common/edit_object.php");


?>
<?php
//Auto-populate Acceptable Risk Exposure
$object_details = $childObject->getObjectDetails();
$rsk_acceptable_risk_exposure = $object_details['rsk_acceptable_risk_exposure'];
?>
<script>
    $(function() {
        $("#rsk_acceptable_risk_exposure").val(<?php  echo $rsk_acceptable_risk_exposure; ?>);
    });
</script>
