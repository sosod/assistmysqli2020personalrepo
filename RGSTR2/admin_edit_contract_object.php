<?php
$object_type = "REGISTER";
$object_id = $_REQUEST['object_id'];

$section = "ADMIN";
$page_redirect_path = "admin_edit_contract.php?";
$page_action = "Edit";

$add_button = true;
$add_button_label = "|add| |risk|";
require_once("inc_header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$child_object_type = "REGISTER";
$child_object_id = $object_id;
$childObject = new RGSTR2_REGISTER($object_id);
?>
<table class=tbl-container>
	<tr>
		<td width=60%><h2><?php echo $helper->getObjectName($object_type); ?> Details</h2><?php 
$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
echo $data['display'];
$js.=$data['js'];
			//include("common/generic_object_form.php");
			$js.= $displayObject->drawPageFooter($helper->getGoBack('admin_contract.php'),strtolower($object_type),"object_id=".$object_id."&log_type=".RGSTR2_LOG::EDIT);
		?></td>
		<td>&nbsp;</td>
	</tr>
</table>
<?php

switch($object_type) {
    case "REGISTER":
        $child_type = "RISK";
        $childObject_risk = new RGSTR2_RISK();
        $child_options = array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$object_id);
        $child_objects = $childObject_risk->getObject($child_options);
        $child_redirect = "manage_view_object.php?object_type=RISK&object_id=";
        $child_name = $helper->getDeliverableObjectName(true);
        break;

}

if($add_button) { $add_button_label = $helper->replaceAllNames($add_button_label); }
$button_label = $helper->getActivityName("open");
$button_icon = 'newwin';
$button = array('value'=>$helper->replaceAllNames($button_label),'class'=>"btn_view",'type'=>"button");
if(isset($button_icon)) {
    $button['icon'] = $button_icon;
}

$js.="
		$(\"#paging_obj_list_view .btn_view\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";
?>
<div id=dlg_child class=dlg-child title="">
    <iframe id=ifr_form_display style="border:0px solid #000000;" src="">

    </iframe>
</div>
<table class=tbl-container id="risks-list">

    <?php if(isset($childObject_risk)) {?>
        <tr>
            <td colspan=3>
                <h2><?php echo $child_name; ?></h2>
                <?php $js.=$displayObject->drawListTable($child_objects,$button,$child_type,$child_options,false,array( (isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : ""))); ?>
            </td>
        </tr>
    <?php } ?>
</table>


<?php
//Get the register id for this particular risk
$reg_obj = $childObject;
////Auto-populate Acceptable Risk Exposure
$reg_sql = 'SELECT * FROM ' . $reg_obj->getTableName() . ' ';
$reg_sql .= 'WHERE register_id = ' . $object_id . ' ';
$reg_details = $helper->mysql_fetch_one($reg_sql);
$register_max_acceptable_exposure = $reg_details['register_max_acceptable_exposure'];
?>
<script type=text/javascript>
	var url = "<?php echo $child_redirect; ?>";
$(function() {
	<?php 	echo $js;	?>
	var object_names = [];
	object_names['contract'] = "<?php echo $helper->getObjectName("register"); ?>";
	object_names['deliverable'] = "<?php echo $helper->getObjectName("risk"); ?>";
	object_names['action'] = "<?php echo $helper->getObjectName("action"); ?>";

	var my_window = AssistHelper.getWindowSize();
	//alert(my_window['height']);
	if(my_window['width']>800) {
		var my_width = 850;
	} else {
		var my_width = 800;
	}
	var my_height = my_window['height']-50;

    <?php
    /**
     * AA-613 RGSTR2 - Allow super user to add objects after activation
     * edited by : Sondelani Dumalisile
     */
    ?>
	$("#risks-list #btn_paging_add").click(function() {
		AssistHelper.processing();
		var i = 0;//$(this).parent("td").attr("object_id");
		var t = "deliverable";//$(this).parent("td").attr("object_type");
		var act = "add";
		var dta = "object_type="+t+"&object_id="+i;
//		AssistHelper.hideProcessing();
		$dlg = $("#dlg_child");
		var obj = "deliverable";
		if(t=="contract" || t=="action" ) { obj = t; }
		//var obj = (t=="action" ? "action" : "deliverable");
		var heading = AssistString.ucwords(act)+" "+object_names[obj];
		var parent_id = "<?php echo $object_id; ?>";//$(this).attr("parent_id");
		var url = "new_"+obj+"_"+act+"_object.php?display_type=dialog&object_id="+parent_id;
		$dlg.dialog("option","title",heading);
		//$dlg.find("iframe").prop("src",url);
		$dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
		$dlg.dialog("open");
		AssistHelper.hideProcessing();
	});
	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		position: ['center',20],
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			$(this).dialog('option','width',my_width);
			$(this).dialog('option','height',my_height);
		}
	});
});
	function tableButtonClick($me) {
		var i = $me.attr("ref");
		url = url+i;
		document.location.href = url;
	}
	function dialogFinished(icon,result) {
		if(icon=="ok") {
			document.location.href = '<?php echo $page_redirect_path; ?>r[]=ok&r[]='+result;
		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon,result);
		}
	}
</script>

<script>
	$(function() {
		$("#register_max_acceptable_exposure").val(<?php  echo $register_max_acceptable_exposure; ?>);
	});
</script>