<?php
$_REQUEST['object_type'] = "CONTRACT";
$object_type="CONTRACT";
$page_action = "ACTIVATE_WAITING";
$page_section = "NEW";
$child_object_type = "CONTRACT";
$page_direct = "new_activate_waiting.php";

$button_label = "|activate|";

$alternative_button_click_function = "activateButtonClick";

include("common/generic_list_page.php");







$page_redirect_path = "new_activate_waiting.php?";

//require_once("inc_header.php");

//ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

//$contractObject = new RGSTR2_REGISTER();
//$objects = $contractObject->getObject(array('type'=>"LIST",'section'=>"NEW",'page'=>"ACTIVATE_WAITING"));

//$js.=$displayObject->drawListTable($objects,array('value'=>$helper->getActivityName("activate"),'class'=>"btn_activate"));

//$js.=" 
//var reftag = '".$contractObject->getRefTag()."' 
//var page_redirect_path = '".$page_redirect_path."' ";

	?>
<div style="display:none" id="confirm_sms">
	<h3>Activate <?php echo $helper->getContractObjectName(); ?></h3>
<!--	<p>How do you want to notify people that this --><?php //echo $helper->getContractObjectName(); ?><!-- has been Activated?</p>-->
<!--	<div id="confirm_div">-->
<!--		<div id="sms_frm">-->
<!--			<input type='radio' checked='checked' name='radio' id='mail_btn'><label for='mail_btn'>Email</label>-->
<!--			<input type='radio' name='radio' id='both_btn'><label for='both_btn'>Email and SMS</label>-->
<!--		</div>-->
<!--		<div id='notification_recipients'>-->
<!--			<p>Recipients:</p>-->
<!--		</div>-->
<!--	</div>-->
	<p>Are you sure you want to activate this <?php echo $helper->getContractObjectName(); ?>? (It will become active in the rest of the module).</p>
</div>


<script type="text/javascript">
var reftag = '<?php echo $myObject->getRefTag(); ?>';
var page_redirect_path = '<?php echo $page_redirect_path; ?>';
window.useSMS = 0;

function getRecipes(r){
	recipes = AssistHelper.doAjax('inc_controller.php?action=<?php echo $object_type; ?>.Recipients&id='+r+'&activity=activate');
	console.log(recipes);
	$('#notification_btns').buttonset('refresh');
}
function drawRecipes(recipes){
	$('#notification_recipients').html('');
	$('#notification_btns').buttonset('refresh');
		for(x in recipes){
			if(recipes[x] instanceof Array){
				$('#notification_recipients').append('<p>'+x+'s:');
				for(var i=0; i< recipes[x].length; i++){
					$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;'+recipes[x][i].name+' - '+recipes[x][i].email+'<br>');
				}
				$('#notification_recipients').append('</p>');
			}else{
				$('#notification_recipients').append('<p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+'<br>');
			}
		}
	}


function drawBoth(recipes){
	$('#notification_recipients').html('');
	$('#notification_btns').buttonset('refresh');
	for(x in recipes){
		if(recipes[x] instanceof Array){
			$('#notification_recipients').append('<p>'+x+'s:');
			for(var i=0; i< recipes[x].length; i++){
				$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;<input hidden id=\"recipient_mobile_'+recipes[x][i].tkid+'\" name=\"recipient_mobile['+recipes[x][i].tkid+']\" value=\"'+recipes[x][i].mobile+'\" />'+recipes[x][i].name+' - '+recipes[x][i].email+' - '+recipes[x][i].mobile+'<br>');
			}
			$('#notification_recipients').append('</p>');
		}else{
			$('#notification_recipients').append('<input hidden id=\"recipient_mobile_'+recipes[x].tkid+'\" name=\"recipient_mobile['+recipes[x].tkid+']\" value=\"'+recipes[x].mobile+'\" /><p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+' - '+recipes[x].mobile+'<br>');
		}
	}
}
var useSMS = AssistHelper.doAjax('inc_controller.php?action=SetupNotifications.Approve&activity=confirm&object=<?php echo $object_type; ?>');
if(useSMS[0] == false){
	$("#confirm_div").hide();
}
function activateButtonClick($me) {
		var r = $me.attr("ref");
		getRecipes(r);
		var i = reftag+r;
		drawRecipes(recipes);
		$('#sms_frm').buttonset();
			$('#mail_btn').button({
				icons:{
					primary: 'ui-icon-mail-closed'
				}
			}).click(function(){
				window.useSMS = 0;
				drawRecipes(recipes);
			});
			$('#both_btn').button({
				icons:{
					primary: 'ui-icon-mail-closed',
					secondary: 'ui-icon-signal'
				}				
			}).click(function(){
				window.useSMS = 1;
				drawBoth(recipes);
			});
		$("#confirm_sms").dialog({
			modal:true,
			width:500,
			buttons:{
				Activate:function(){
					AssistHelper.processing();
					var mobile = [];
					$("#notification_recipients input").each(function(){
						mobile.push($(this).val());
					});
					var dta = "contract_id="+r+"&sms="+window.useSMS+"&recipient_mobile[]="+mobile;
					var result = AssistHelper.doAjax("inc_controller.php?action=Contract.activateObject",dta);
					if(result[0]=="ok") {
						document.location.href = page_redirect_path+"r[]="+result[0]+"&r[]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
				},
				Cancel:function(){
					$(this).dialog("close");
				}
			}
		});
		AssistHelper.hideDialogTitlebar("id","confirm_sms");
		AssistHelper.formatDialogButtons($("#confirm_sms"),0,AssistHelper.getGreenCSS());
	}

</script>
