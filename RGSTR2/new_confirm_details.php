<?php
$page_redirect_path = "new_confirm.php?";

include("inc_header.php");


$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "REGISTER";
$object_id = $_REQUEST['object_id'];
			$options = array(
				'type'=>"LIST",
				'section'=>"NEW",
				'page'=>"CONFIRM",
			);
			
switch($object_type) {
	case "CONTRACT":
	case "REGISTER":
		$child_type = "RISK";
		$myObject = new RGSTR2_REGISTER();
		$childObject = new RGSTR2_RISK();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$object_id);
		$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'contract_id'=>$object_id,'page'=>"CONFIRM"));// ASSIST_HELPER::arrPrint($child_objects);
		$child_redirect = "new_confirm_details.php?object_type=RISK&object_id=";
		$child_name = $helper->getDeliverableObjectName(true);
		$child_object_type = "RISK";
		break;
	case "DELIVERABLE":
	case "RISK":
		$child_type = "ACTION";
		$myObject = new RGSTR2_RISK();
		$childObject = new RGSTR2_ACTION();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id);
		$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'deliverable_id'=>$object_id,'page'=>"CONFIRM")); 
		$child_redirect = "new_confirm_details.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName(true);
		$child_object_type = "ACTION";
		break;
	case "ACTION":
		$myObject = new RGSTR2_ACTION();
		$child_redirect = "";
		$child_object_type="";
		break;
}

$button_label = $helper->getActivityName("open");
$button_icon = 'newwin';
$button = array('value'=>$helper->replaceAllNames($button_label),'class'=>"btn_view",'type'=>"button");
if(isset($button_icon)) {
	$button['icon'] = $button_icon;
}

$js.="
		$(\"button.btn_view\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";
?>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,($object_type=="CONTRACT")); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<?php if($object_type == "CONTRACT" || $object_type == "REGISTER") { ?>
			<h2>Summary of <?php echo $helper->getContractObjectName()." ".$myObject->getRefTag().$object_id; 
				$summ = $myObject->getSummary($object_id); ?></h2>
			<table class='form th2' width=50%>
				<tr>
					<th width=150px>Risks:</th>
					<td><?php echo $summ['DEL']['deliverable']; ?></td>
				</tr><tr>
					<th>Risk Actions:</th>
					<td><?php echo $summ['DEL']['action']; ?></td>
				</tr><tr>
					<th>Parent Risks:</th>
					<td><?php echo $summ['MAIN']['deliverable']; ?></td>
				</tr><tr>
					<th>Sub Risks:</th>
					<td><?php echo $summ['SUB']['deliverable']; ?></td>
				</tr><tr>
					<th>Sub Risk Actions:</th>
					<td><?php echo $summ['SUB']['action']; ?></td>
				</tr>
			</table>
			<?php } ?>
		</td>
	</tr>
	<?php if(isset($childObject)) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php

//$helper->arrPrint($options);
			$js.=$displayObject->drawListTable($child_objects,$button,$child_type,$child_options,false,array( (isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : "")));
			?>
		</td>
	</tr>
	<?php } 
	if($object_type=="CONTRACT" || $object_type=="REGISTER") {
	?>
	<tr>
		<td colspan=3 class=center><?php
		if($myObject->canIConfirm($object_id)) {
			echo "<input type=hidden name=contract_id value=".$object_id." id=contract_id /><input type=button value=Confirm class=isubmit id=btn_confirm />";
		} else {
			ASSIST_HELPER::displayResult(array("error","The ".$helper->getContractObjectName()." cannot be confirmed without at least 1 ".$helper->getDeliverableObjectName()." and ".$helper->getActionObjectName()));
		}
		?></td>
	</tr>
	<?php } ?>
</table>
<div style="display:none" id="confirm_sms">
	<h3>Confirm <?php echo $helper->getContractObjectName(); ?></h3>
<!--	<div id="confirm_div">-->
<!--		<p>How do you want to notify the --><?php //echo $helper->getContractObjectName(); ?><!-- Authoriser that this --><?php //echo $helper->getContractObjectName(); ?><!-- is ready for Activation?</p>-->
<!--		<div id="sms_frm">-->
<!--			<input type='radio' checked='checked' name='radio' id='mail_btn'><label for='mail_btn'>Email</label>-->
<!--			<input type='radio' name='radio' id='both_btn'><label for='both_btn'>Email and SMS</label>-->
<!--		</div>-->
<!--		<div id='notification_recipients'>-->
<!--			<p>Recipients:</p>-->
<!--		</div>-->
<!--	</div>-->
	<p>Are you sure you want to confirm this <?php echo $helper->getContractObjectName(); ?>?</p>
</div>
<iframe id=ifrm_all style='width: 0px;height:0px'></iframe>
<script type="text/javascript">
var url = "<?php echo $child_redirect; ?>";
window.useSMS = 0;
function getRecipes(){
	recipes = AssistHelper.doAjax('inc_controller.php?action=<?php echo $object_type; ?>.Recipients&id=<?php echo $object_id; ?>&activity=confirm');
	//console.log('inc_controller.php?action=<?php echo $object_type; ?>.Recipients&id=<?php echo $object_id; ?>&activity=confirm');
	//console.log(recipes);
	$('#notification_btns').buttonset('refresh');
	if(window.useSMS === 0){
		drawRecipes(recipes);
	}else{
		drawBoth(recipes);
	}
}
function drawRecipes(recipes){
	$('#notification_recipients').html('');
	$('#notification_btns').buttonset('refresh');
		for(x in recipes){
			if(recipes[x] instanceof Array){
				$('#notification_recipients').append('<p>'+x+'s:');
				for(var i=0; i< recipes[x].length; i++){
					$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;'+recipes[x][i].name+' - '+recipes[x][i].email+'<br>');
				}
				$('#notification_recipients').append('</p>');
			}else{
				$('#notification_recipients').append('<p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+'<br>');
			}
		}
	}


function drawBoth(recipes){
	$('#notification_recipients').html('');
	$('#notification_btns').buttonset('refresh');
	for(x in recipes){
		if(recipes[x] instanceof Array){
			$('#notification_recipients').append('<p>'+x+'s:');
			for(var i=0; i< recipes[x].length; i++){
				$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;<input hidden id=\"recipient_mobile_'+recipes[x][i].tkid+'\" name=\"recipient_mobile['+recipes[x][i].tkid+']\" value=\"'+recipes[x][i].mobile+'\" />'+recipes[x][i].name+' - '+recipes[x][i].email+' - '+recipes[x][i].mobile+'<br>');
			}
			$('#notification_recipients').append('</p>');
		}else{
			$('#notification_recipients').append('<input hidden id=\"recipient_mobile_'+recipes[x].tkid+'\" name=\"recipient_mobile['+recipes[x].tkid+']\" value=\"'+recipes[x].mobile+'\" /><p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+' - '+recipes[x].mobile+'<br>');
		}
	}
}


$(function() {
	<?php echo $js; ?>
	var useSMS = AssistHelper.doAjax('inc_controller.php?action=SetupNotifications.Approve&activity=confirm&object=<?php echo $object_type; ?>');
	if(useSMS[0] == false){
		$("#confirm_div").hide();
	}else{
		getRecipes();
	}
    var page_redirect_path = "<?php echo isset($page_redirect_path) ? $page_redirect_path : ""; ?>";
		$("#btn_confirm").click(function() {
			$('#sms_frm').buttonset();
			$('#mail_btn').button({
				icons:{
					primary: 'ui-icon-mail-closed'
				}
			}).click(function(){
				window.useSMS = 0;
				drawRecipes(recipes);
			});
			$('#both_btn').button({
				icons:{
					primary: 'ui-icon-mail-closed',
					secondary: 'ui-icon-signal'
				}				
			}).click(function(){
				window.useSMS = 1;
				drawBoth(recipes);
			});
			
		$("#confirm_sms").dialog({
			modal:true,
			buttons:{
				Confirm:function(){
					AssistHelper.processing();
					var mobile = [];
					$("#notification_recipients input").each(function(){
						mobile.push($(this).val());
					});
					var dta = "contract_id="+$("#contract_id").val()+"&sms="+window.useSMS+"&recipient_mobile[]="+mobile;
					var result = AssistHelper.doAjax("inc_controller.php?action=Contract.confirmObject",dta);
					//console.log(result)
					if(result[0]=="ok") {
						document.location.href = page_redirect_path+"r[]="+result[0]+"&r[]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
				},
				Cancel:function(){
					$(this).dialog("close");
				}
			}
		});
		AssistHelper.hideDialogTitlebar("id","confirm_sms");
		AssistHelper.formatDialogButtons($("#confirm_sms"),0,AssistHelper.getGreenCSS());
	});
	$("input:button.btn_view").click(function() {
		tableButtonClick($(this));
	});
	$("#view_all").click(function() {
		var page_html = document.getElementById('ifrm_all').contentWindow.document.body.innerHTML;
		if(page_html.length==0){
			var url = "http<?php echo ($_SERVER["SERVER_PORT"]=="443" ? "s" : ""); ?>://<?php echo $_SERVER["HTTP_HOST"]."/".$myObject->getModLocation(); ?>/new_confirm_details_all.php?object_id=<?php echo $object_id; ?>";
			if(confirm("Please be patient while the page loads.  It can take some time depending on the amount of information.")==true) {
				AssistHelper.processing();
				$("#ifrm_all").prop("src",url);
			}
		} else {
			AssistHelper.processing();
			nextSteps();
		}
		AssistHelper.closeProcessing();
		document.location.href = url;
	});
});
function tableButtonClick($me) {
		var i = $me.attr("ref");
		document.location.href = url+i;
}
function nextSteps() {
	$(function() {
		AssistHelper.closeProcessing();
		var winsize = AssistHelper.getWindowSize();
		var h = winsize.height*0.9;
		var page_html = document.getElementById('ifrm_all').contentWindow.document.body.innerHTML;
		var head_html = document.getElementById('ifrm_all').contentWindow.document.head.innerHTML;
		$("<div />",{id:"dlg_view_all",html:page_html}).dialog({
			modal:true,
			width:"90%",
			height:h,
			buttons: [{
				text:"Print",
				click:function(){
					var viewAll = window.open("","ViewAll");
					viewAll.document.body.innerHTML = page_html;
					viewAll.document.head.innerHTML = head_html;
					viewAll.print();
				}
			},{
				text:"Close",
				click:function(){
					$(this).dialog("destroy");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id","dlg_view_all");
	});
}
</script>