<?php

require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);





/**
 * Other module code here e.g. get PARENT OBJECT details etc.
 */
$active_record = 2;	//replace with something like MODLOC_OBJECT::ACTIVE

$hazard_value = $_REQUEST['hazard_value'];

if($hazard_value == 'X'){
    $object_id = $_REQUEST['object_id'];//rsk_id
    $object_type = $_REQUEST['object_type'];//rsk


    /**
     * Actual code starts here
     */

    $source_module = $_REQUEST['src'];
    $filter = $_REQUEST['filter'];


    $linkObject = new ASSIST_INTERNAL();
    $linkObject->setSource($source_module);
    $data = $linkObject->getLinesByFilter($filter);
    $headings= $data['head'];
    $lines = $data['rows'];

//GET EXISTING LINE INFORMATION
//Could be a call to a function in MODREF_OBJECT class
    $sql = "SELECT lne_id as id, lne_srcid as line_id ";
    $sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
    $sql .= "WHERE lne_srcmodref = '$source_module' ";
    $sql .= "AND lne_objectid = $object_id ";
    $sql .= "AND lne_object_type = '$object_type' ";
    $sql .= "AND (lne_status & ".$active_record.") = ".$active_record;
    $existing_lines = $linkObject->mysql_fetch_value_by_id($sql,"id","line_id");

//    echo '<pre style="font-size: 18px">';
//    echo '<p>DATA LOOKS LIKE SO</p>';
//    print_r($existing_lines);
//    echo '</pre>';
//
//    echo '<pre style="font-size: 18px">';
//    echo '<p>LINES</p>';
//    print_r($lines);
//    echo '</pre>';
}else{
    $object_id = $_REQUEST['hazard_value'];//haz_id
    $object_type = $_REQUEST['object_type'];//hzd

    $linkObject = new ASSIST_INTERNAL();
    $linkObject->setSourceType("library");
    $sources = $linkObject->getSources();

    $hazard_id = $object_id;
    $src = $_REQUEST['src'];
    $filter = $_REQUEST['filter'];

//    echo '<pre style="font-size: 18px">';
//    echo '<p>FILTER</p>';
//    print_r($filter);
//    echo '</pre>';

    $sql = "SELECT lne_id as id, lne_srcmodref as modref, lne_srcid as line_id ";
    $sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
    $sql .= "WHERE lne_objectid = $hazard_id ";
    $sql .= "AND lne_object_type = '$object_type' ";
    $sql .= "AND lne_srcmodref = '$src'";
    $sql .= "AND (lne_status & ". 2 .") = " . 2;

    $existing_lines = $linkObject->mysql_fetch_value_by_id2($sql, "modref","id","line_id");

    $legacy_data_structure = $linkObject->getLegacyDataStructure($src);

    $filters_data_structure = array();
    if(isset($existing_lines) && is_array($existing_lines) && count($existing_lines) > 0){
        foreach($sources as $modref => $src) {
            if(array_key_exists($modref, $existing_lines)){
                foreach($existing_lines[$modref] as $index => $value_thereof){
                    //Search Algorithm for finding the line items connected to the hazard by going through the legacy data structure
                    //Start with the Parent
                    foreach($legacy_data_structure as $parent_id  => $parent_data){
                        //Then go through the kids
                        foreach($parent_data['children_objects'] as $child_id => $child_data){
                            //Check if the hazard connected line item_id is in this child item
                            if(array_key_exists($value_thereof, $child_data['child_activities'])){
                                //Set up the filter data structure, that we'll use to generate the option box
                                if(!array_key_exists($child_id, $filters_data_structure)){
                                    $filters_data_structure[$child_id]['filter_parent'] = $parent_data['parent_object'];
                                    $filters_data_structure[$child_id]['filter_object'] = $child_data['child_object'];
                                }
                                $filters_data_structure[$child_id]['filter_activities'][$value_thereof] = $child_data['child_activities'][$value_thereof];
                            }
                        }
                    }
                }
            }
        }
    }

//    echo '<pre style="font-size: 18px">';
//    echo '<p>FILTER DATA STRUCTURE</p>';
//    print_r($filters_data_structure);
//    echo '</pre>';

    //==================================== NEW STUFF

    $data = $linkObject->getLinesByFilter($filter);
    $headings= $data['head'];
    $lines = $data['rows'];

//    echo '<pre style="font-size: 18px">';
//    echo '<p>ALL LINES</p>';
//    print_r($lines);
//    echo '</pre>';

    $hazard_linked_lines = array();

    $filters_data_structure_lines_array = $filters_data_structure[$filter]['filter_activities'];

//    echo '<pre style="font-size: 18px">';
//    echo '<p>FILTER DS LINES ARRAY</p>';
//    print_r($filters_data_structure_lines_array);
//    echo '</pre>';

    foreach($filters_data_structure_lines_array as $key => $val ){
        $hazard_linked_lines[$key] = $lines[$key];
    }

//    echo '<pre style="font-size: 18px">';
//    echo '<p>HAZARD LINKED LINES NEW LINES</p>';
//    print_r($hazard_linked_lines);
//    echo '</pre>';

    unset($lines);
    $lines = $hazard_linked_lines;


    //================================= GET RISK HAZARD CONNECTED LINES AS EXISTING LINES


}
?>
<h1>Select Lines</h1>
<table>
	<tr>
		<th><?php 
		if(count($lines)>0) {
			echo "<input type=checkbox value=ALL name=chk_all id=chk_all />";
		} ?></th>
	<?php
	foreach($headings as $fld => $head) {
		echo "<th>$head</th>";
	}
	?>
	</tr>
	<?php
	if(count($lines)>0) {
			
		foreach($lines as $line_id => $line) {
			if(in_array($line_id,$existing_lines)) {
				$checkbox = $linkObject->getDisplayIcon("ok");
			} else {
				$checkbox = "<input type=checkbox class=chk_line line=".$line_id." value=Y name=chk_use[$line_id] />";
			}
			echo "
			<tr>
				<td class=center>".$checkbox."</td>";
			foreach($headings as $fld => $head) {
				echo "
				<td>".$line[$fld]."</td>
				";
			}
			echo "
			</tr>";
		}
	} else {
		echo "
		<tr>
			<td colspan=".(count($headings)+1).">No lines available.</td>
		</tr>";
	}
	?>
</table>

<script type="text/javascript">
$(function() {
	//Check if there are any lines to select
	if($("input:checkbox").length==1) {
		//hide select all checkbox
		$("input:checkbox").hide();
		//change button from Save to Close
		window.parent.changeDialogButtons();
	}
	
	
	//Tell parent window to open the dialog once the page has finished loading
	window.parent.openDialog();

	//"Select All" checkbox
	$("#chk_all").click(function() {
		if($(this).is(":checked")) {
			$("input:checkbox").prop("checked",true);
		} else {
			$("input:checkbox").prop("checked",false);
		}
	}).focus();
	
});
</script>