<?php
require_once("../module/autoloader.php");
$result = array("info","Sorry, I couldn't figure out what you wanted me to do.");

/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */
$my_action = explode(".",$_REQUEST['action']);
$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);
$page_direct = (isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : '' );
unset($_REQUEST['page_direct']);



switch($class) {
	case "DISPLAY":
		$object = new RGSTR2_DISPLAY();
		break;
	/**
	 * Log object
	 */
	case "LOG":
		$log_table = $_REQUEST['log_table'];
		unset($_REQUEST['log_table']);
		$object = new RGSTR2_LOG($log_table);
		break;
	/**
	 * Setup objects
	 */
	case "CONTRACTOWNER":
		$object = new RGSTR2_CONTRACT_OWNER();
		break;
	case "USERACCESS":
		$object = new RGSTR2_USERACCESS();
		break;
	case "LISTS":
		$list_id = $_REQUEST['list_id'];
		unset($_REQUEST['list_id']);
		$object = new RGSTR2_LIST($list_id);
		break;
	case "MENU":
		$object = new RGSTR2_MENU();
		break;
	case "NAMES":
		$object = new RGSTR2_NAMES();
		break;
	case "HEADINGS":
		$object = new RGSTR2_HEADINGS();
		break;
	case "PROFILE":
		$object = new RGSTR2_PROFILE();
		break;
	/**
	 * Module objects
	 */
	case "CONTRACT":
	case "REGISTER":
		$object = new RGSTR2_REGISTER();
		break;
	case "DELIVERABLE":
	case "RISK":
		$object = new RGSTR2_RISK();
		break;
	case "ACTION":
		$object = new RGSTR2_ACTION();
		break;
	case "FINANCE":
		$object = new RGSTR2_FINANCE();
		break;
	case "TEMPLATE":
		$object = new RGSTR2_TEMPLATE();
		break;
}

$result[1].=":".$my_action[1].":";
if(isset($object)) {
	switch($activity) {
		case "GET_ATTACH":
			$object_id = $_REQUEST['object_id'];
			$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			$attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
			$folder = $object->getParentFolderPath();
            $old = $folder."/".$attach['location']."/".$attach['system_filename'];
            $new = $attach['original_filename'];

            $old;

//            if(file_exists($old)){
//                echo '<pre style="font-size: 18px">';
//                echo '<p>PRINTHERE</p>';
//                print_r('YATAAA!!');
//                echo '</pre>';
//            }

//            echo '<pre style="font-size: 18px">';
//            echo '<p>ATTACH</p>';
//            print_r($attach);
//            echo '</pre>';
			$object->downloadFile3($old,$new);
			break;
	}
}
?>