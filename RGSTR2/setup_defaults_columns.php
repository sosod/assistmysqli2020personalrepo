<?php
require_once("inc_header.php");

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

ASSIST_HELPER::displayResult(array("info","Click and drag each column into the desired table position and then click the 'Save All Changes' button at the bottom of the page.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The New/Manage/Admin section will indicate on which list page the column will display."));


$head_section = array(
	"REGISTER",
	"RISK",
	"ACTION",
);

?>
<table class='tbl-container not-max'><tr><td>
	<form name=frm_list>
<?php
foreach($head_section as $h_section) {
$headings = $headingObject->getListColumnHeadings($h_section);   //ASSIST_HELPER::arrPrint($headings);
$o = explode("_",$h_section);
$object_type = $o[0];

/*****
 * section to hide the first column if it is the ref field.
 */
foreach($headings as $field=>$h) {
	if($h['type']=="REF") {
		unset($headings[$field]);
	}
	break;
}
/********************/
?>
<input type=hidden name=section[] value=<?php echo $h_section.($h_section=="ACTION"?"_OBJECT":""); ?> />
<h2><?php echo $headingObject->getObjectName($object_type).(isset($o[1]) && $o[1]=="UPDATE" ? " ".$menuObject->getAMenuNameByField("manage_update"):""); ?> Headings</h2>
<div class='horscroll'>
<table class='tbl-container not-max'>
	<tr><td>
		<?php 
		echo "<ul id=sortable_".$h_section." class=sortable >";
		foreach($headings as $field=>$h) {
			echo "
			<li >
				<table width=100%>
					<tr>
						<th>".$h['client']."<input type=hidden name=".strtolower(($h_section!="ACTION")?$h_section:$h_section."_object")."[] value=".$h['id']." /></td>
					</tr>
					<tr>
						<td>
			<table class=sub-table width=95%>
				<tr>
					<td>New:</td>
					<td>".$helper->getDisplayIcon($h['list_new']==1?"ok":"error")."</td>
				</tr>
				<tr>
					<td>Manage:</td>
					<td>".$helper->getDisplayIcon($h['list_manage']==1?"ok":"error")."</td>
				</tr>
				<tr>
					<td>Admin:</td>
					<td>".$helper->getDisplayIcon($h['list_admin']==1?"ok":"error")."</td>
				</tr>
			</table>
						</td>
					</tr>

				</table>
			</li>";
		}
		echo "</ul>"; 
		?>
	</td></tr>
</table>
</div>
<?php
}
?>
</form>
<p style='margin-top: 20px'><input type=button class=isubmit value='Save All Changes' /></p>
</td></tr>
</table>
<table class='tbl-container not-max' width=50%>
<tr>
	<td>&nbsp;</td>
</tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"COL")); ?></td>
	</tr>
</table>
<style type="text/css" >
	.horscroll { 
	        overflow-x: auto;
	        overflow-y: hidden;
	}
	.sortable { 
		list-style-type: none;
		list-style-image: url();
		margin: 0;
		padding: 0;
	}
	.sortable li {
		margin: 3px;
		padding: 3px;
		float: left;
		width: 130px;
		text-align: center;
		vertical-align: middle;
		background-color: #000099;
		line-height: 0pt;
	}
	.sortable table, .sortable table td, .sortable table th {
		border: 0px solid #FFFFFF;
	}
	.sortable table.sub-table {
		margin: 0 auto;
		background-color: #FFFFFF;
	}
		/*width: 100%;*/
	.sortable .sub-table td {
		padding: 1px 3px 1px 3px;
		font-size: 75%;
		font-weight: bold;
	}
		/*text-align: center;*/
</style>
<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	
	//Autoscrolling tables if they exceed screen width
	$(window).resize(function() { 
		var w= AssistHelper.getWindowSize();
		var wi = w["width"];
		var wid = wi-50;
		var widt = ""+wid+"px";
		$(".horscroll").css("width", widt);
	});
	$(window).trigger("resize");
	
	$(".sortable").sortable({
		placeholder: "ui-state-highlight"
	});
	$(".sortable").disableSelection();
	
	var maxHeight = -1;
	var thHeight = -1;
	$(".sortable").each(function() {
		var $li = $(this).find("li");
		var $th = $(this).find("th");
		var c = $li.length;
		$(this).css("width",(c*(130+15))+"px");
		//$p = $(this).parent().parent().parent().parent(); //td tr table div
		//$(li).each(function() {
			//maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
		//});
		$th.each(function() {
			thHeight = thHeight > $(this).height() ? thHeight : $(this).height();
		});
	});
	$(".sortable li table tr th").each(function() {
		$(this).height(thHeight);
	});
	$(".sortable").each(function() {
		var $li = $(this).find("li");
		//var th = $(this).find("th");
		//var c = li.length;
		//$(this).css("width",(c*(130+15))+"px");
		//$p = $(this).parent().parent().parent().parent(); //td tr table div
		$li.each(function() {
			maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
		});
		//$(th).each(function() {
		//	thHeight = thHeight > $(this).height() ? thHeight : $(this).height();
		//});
	});
	$(".sortable li").each(function() {
		$(this).height(maxHeight);
		$(this).css("cursor","pointer");
	});
	$(".sortable").on("sortstart", function( event, ui ) {
		 ui.placeholder.height(ui.item.height());
	});
	//alert($(".horscroll:first").height());
	$(".horscroll").height(maxHeight+40);
	
	/* Save changes */
	$(".isubmit").click(function() {
		AssistHelper.processing();
		$form = $("form[name=frm_list]");
		var dta = AssistForm.serialize($form);
		var url = "inc_controller.php?action=Headings.saveListColumns";
		var result = AssistHelper.doAjax(url,dta);
		AssistHelper.finishedProcessing(result[0],result[1]);
		//alert(url);
		//alert(dta);
	});
});
</script>