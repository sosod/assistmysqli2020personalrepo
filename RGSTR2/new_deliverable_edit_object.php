<?php
//$parent_object_type = $_REQUEST['object_type'];
//$parent_object_id = $_REQUEST['object_id'];

if(isset($parent_object_id)){
    $parent_object_id = $parent_object_id;
}else{
    $parent_object_id = $_REQUEST['object_id'];
    $object_id = $_REQUEST['object_id'];
    $parent_id = $parent_object_id;
}

//$page_redirect_path = "new_deliverable_add.php?object_type=".$parent_object_type."&object_id=".$parent_object_id."&";
$page_action = "Edit";
$page_section = "NEW";

$_REQUEST['object_type'] = "RISK";

$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "default";
if($display_type=="dialog") {
	$no_page_heading = true;
	$page_redirect_path = "dialog";
} else {
	$page_redirect_path = "new_deliverable_edit_object.php?object_id=".$parent_object_id."&";
}


include("common/edit_object.php");


?>
<?php
//Auto-populate Acceptable Risk Exposure
$object_details = $childObject->getObjectDetails();
$rsk_acceptable_risk_exposure = $object_details['rsk_acceptable_risk_exposure'];
?>
<script>
    $(function() {
        $("#rsk_acceptable_risk_exposure").val(<?php  echo $rsk_acceptable_risk_exposure; ?>);
    });
</script>