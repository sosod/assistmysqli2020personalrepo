<?php
require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

$displayObject = new RGSTR2_DISPLAY();
$child_type = $_REQUEST['child_type'];
$child_id = $_REQUEST['child_id'];
$object_type = $_REQUEST['parent_type'];

if($child_type == 'ACTION'){
    $myObject = new RGSTR2_ACTION();
    $object_id = $myObject->getParentID($child_id);
    if($object_type == 'REGISTER'){
        $myObject = new RGSTR2_RISK();
        $object_id = $myObject->getParentID($object_id);
    }
}else{
    $myObject = new RGSTR2_RISK();
    $object_id = $myObject->getParentID($child_id);
}

$displayObject->drawDetailedView($object_type, $object_id);
