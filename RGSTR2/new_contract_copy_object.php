<?php
$page_redirect_path = "new_contract_copy.php?";

include("inc_header.php");


$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
$object_id = $_REQUEST['object_id'];
			$options = array(
				'type'=>"LIST",
				'section'=>"NEW",
				'page'=>"COPY",
			);
			
switch($object_type) {
	case "CONTRACT":
		$myObject = new RGSTR2_REGISTER();
		break;
}


?>
<form name=frm_copy method=post action=new_contract_copy_details.php>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,false); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<?php if($object_type == "CONTRACT") { ?>
			<h2>Summary of <?php echo $helper->getContractObjectName()." ".$myObject->getRefTag().$object_id; 
				$summ = $myObject->getSummary($object_id); ?></h2>
			<table class='form th2' width=50%>
				<tr>
					<th width=150px>Deliverables:</th>
					<td><?php echo $summ['DEL']['deliverable']; ?></td>
				</tr><tr>
					<th>Deliverable Actions:</th>
					<td><?php echo $summ['DEL']['action']; ?></td>
				</tr><tr>
					<th>Parent Deliverables:</th>
					<td><?php echo $summ['MAIN']['deliverable']; ?></td>
				</tr><tr>
					<th>Sub Deliverables:</th>
					<td><?php echo $summ['SUB']['deliverable']; ?></td>
				</tr><tr>
					<th>Sub Deliverable Actions:</th>
					<td><?php echo $summ['SUB']['action']; ?></td>
				</tr>
			</table>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td colspan=3> <div class=center >
			<input type=button value="View Complete <?php echo $helper->getObjectName("contract"); ?>" id=btn_view_all />
			<?php
			echo "<input type=hidden name=contract_id value=".$object_id." id=contract_id /><input type=button value='".$helper->getActivityName("copy")."*'  id=btn_copy />";
			?>
			</div>
			<div style='width: 400px; margin: 0 auto; '><?php
			ASSIST_HELPER::displayResult(array("info","* This will display the complete ".$helper->getContractObjectName()." (excluding any associated attachments) so that you can amend any details as necessary.<br />You must click the '".$helper->getActivityName("save")."' button at the end of the page in order to finalise the ".$helper->getActivityName("copy")."."));
			?>
			</div>
		</td>
	</tr>
</table>
</form>
<iframe id=ifrm_all style='width: 0px;height:0px; border:0px solid #ffffff;'></iframe>
<script type="text/javascript" >
$(function(){
	<?php echo $js; ?>
	$("#btn_copy").button().addClass("ui-state-ok").click(function() {
		$("form[name=frm_copy]").submit();
	});
	$("#btn_view_all").button().click(function() {
		var page_html = document.getElementById('ifrm_all').contentWindow.document.body.innerHTML;
		if(page_html.length==0){
			var url = "http<?php echo ($_SERVER["SERVER_PORT"]=="443" ? "s" : ""); ?>://<?php echo $_SERVER["HTTP_HOST"]."/".$myObject->getModLocation(); ?>/new_contract_copy_details_all.php?object_id=<?php echo $object_id; ?>";
			if(confirm("Please be patient while the page loads.  It can take some time depending on the amount of information.")==true) {
				AssistHelper.processing();
				$("#ifrm_all").prop("src",url);
			}
		} else {
			AssistHelper.processing();
			nextSteps();
		}
		//AssistHelper.closeProcessing();
		//window.open(url,"view_all");
	});
	
});
function nextSteps() {
	$(function() {
		AssistHelper.closeProcessing();
		var winsize = AssistHelper.getWindowSize();
		var h = winsize.height*0.9;
		var page_html = document.getElementById('ifrm_all').contentWindow.document.body.innerHTML;
		var head_html = document.getElementById('ifrm_all').contentWindow.document.head.innerHTML;
		$("<div />",{id:"dlg_view_all",html:page_html}).dialog({
			modal:true,
			width:"90%",
			height:h,
			buttons: [{
				text:"Print",
				click:function(){
					var viewAll = window.open("","ViewAll");
					viewAll.document.body.innerHTML = page_html;
					viewAll.document.head.innerHTML = head_html;
					viewAll.print();
				}
			},{
				text:"Close",
				click:function(){
					$(this).dialog("destroy");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id","dlg_view_all");
	});
}
</script>