<?php

$page_redirect_path = "admin_template_manage.php";
$noparams = explode("&",$_SERVER['REQUEST_URI']);
$pageonly = explode("/",$noparams[0]);
$thispage = $pageonly[2];
include("inc_header.php");

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
$object_id = $_REQUEST['object_id'];

$tempObject = new RGSTR2_TEMPLATE();

$myObject = new RGSTR2_REGISTER();
$childObject = new RGSTR2_RISK();
$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"ADMIN",'contract_id'=>$object_id)); 
$child_redirect = "new_confirm_details.php?object_type=DELIVERABLE&object_id=";
$child_name = $helper->getDeliverableObjectName(true);

$gchildObject = new RGSTR2_ACTION();
$gchild_objects = $gchildObject->getObject(array('type'=>"LIST",'section'=>"ADMIN",'C.contract_id'=>$object_id)); 
$gchild_redirect = "new_confirm_details.php?object_type=ACTION&object_id=";
$gchild_name = $helper->getActionObjectName(true);

//ASSIST_HELPER::arrPrint($child_objects);
?>
<div id="div_error">
	
</div>
<h2>Summary of <?php echo $helper->getTemplateObjectName()." ".$tempObject->getTemplateName($object_id); ?></h2>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%>
		<?php 
			//print_r($myObject->getDetailedObject("CONTRACT", 1));
			//print_r($tempObject->getListObject(1, "CON"));
			//$js.=$displayObject->drawDetailedView("TEMPCON", $object_id,false);
			echo "<input type=hidden name=contract_id value=".$object_id." id=contract_id /><input type=button value=Delete class=idelete id=btn_con_del />";
		?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<h2><?php echo $child_name; ?></h2>
				<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>"Delete",'class'=>"btn_del_del")); ?><br><br>
			<h2><?php echo $gchild_name; ?></h2>
				<?php $js.=$displayObject->drawListTable($gchild_objects,array('value'=>"Delete",'class'=>"btn_act_del", 'pager'=>"second")); ?>
		</td>
	</tr>
	<tr>
		<td colspan=3>
		</td>
	</tr>
	<?php 
	if($object_type=="CONTRACT") {
	?>
	<tr>
		<td colspan=3 class=center><?php
		if($myObject->canIConfirm($object_id)) {
		} else {
			ASSIST_HELPER::displayResult(array("error","The ".$helper->getContractObjectName()." cannot be confirmed without at least 1 ".$helper->getDeliverableObjectName()." and ".$helper->getActionObjectName()));
		}
		?></td>
	</tr>
	<?php } ?>
</table>
<div id="con_del_div" style="display:none">
	<h3>Delete <?php echo $helper->getContractObjectName(); ?></h3>
	<p>Are you sure you wish to remove this <?php echo $helper->getContractObjectName(); ?> from the template?</p><br><br>		
</div>
<div id="del_del_div" style="display:none">
	<h3>Delete <?php echo $helper->getDeliverableObjectName(); ?></h3>
	<p>Are you sure you wish to remove this <?php echo $helper->getDeliverableObjectName(); ?> from the template?</p><br><br>		
</div>
<div id="act_del_div" style="display:none">
	<h3>Delete <?php echo $helper->getActionObjectName(); ?></h3>
	<p>Are you sure you wish to remove this <?php echo $helper->getActionObjectName(); ?> from the template?</p><br><br>	
</div>
<div id="deactivate_div" style="display:none">
	<h3>Deactivate <?php echo $helper->getTemplateObjectName(); ?></h3>
	<p>Are you sure you wish to deactivate this <?php echo $helper->getTemplateObjectName(); ?> ?</p>
	<p>It will no longer be useable, but it can be reactivated later.</p><br><br>	
</div>
<div id="temp_summ">
	<br><input class='btn_deact' type='button' id='btn_tmp_deact' value='Deactivate' />
</div>
<script type="text/javascript">
	<?php echo $js; ?>
	$('.btn_del_del').addClass('idelete');
	$('.btn_act_del').addClass('idelete');
	$('#btn_tmp_deact').addClass('idelete');
	
	$('#btn_tmp_deact').click(function(){
		var id = "<?php echo $object_id; ?>";
		$("#deactivate_div").show().dialog({
			modal:true,
			buttons:[{
				text:"Deactivate",
				click:function(){
					AssistHelper.processing();
					var result = AssistHelper.doAjax("inc_controller.php?action=Template.deactivate", "id="+id);
					console.log(result);
					$(this).dialog("close");
					AssistHelper.finishedProcessing(result[0],result[1]);
					setTimeout(function(){
						//document.location.href = "<?php //echo $thispage; ?>";
					},2000);
				}
			},{
				text:"Cancel",
				click:function(){
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "deactivate_div");
	});
	
	$('#btn_con_del').click(function(){
		$('#con_del_div').show().dialog({
			modal:true,
			buttons:[{
					text:"Delete",
					click:function(){
						AssistHelper.processing();
					 	var result = AssistHelper.doAjax("inc_controller.php?action=Template.delete&obj=CON&id="+"<?php echo $object_id; ?>");
						$(this).dialog("close");
						AssistHelper.finishedProcessing(result[0], result[1]);
						console.log(result);
				 	}
				},{
					text:"Cancel",
					click:function(){
						$(this).dialog("close");
					}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "con_del_div");
	});
	
	$('.btn_del_del').click(function(){
		var del_id = $(this).attr("ref");
		$('#del_del_div').show().dialog({
			modal:true,
			buttons:[{
					text:"Delete",
					click:function(){
						AssistHelper.processing();
					 	var result = AssistHelper.doAjax("inc_controller.php?action=Template.delete&obj=DEL&id="+del_id);
						$(this).dialog("close");
						AssistHelper.finishedProcessing(result[0], result[1]);
						console.log(result);
				 	}
				},{
					text:"Cancel",
					click:function(){
						$(this).dialog("close");
					}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "del_del_div");
	});
	
	$('.btn_act_del').click(function(){
		var act_id = $(this).attr("ref");
		$('#act_del_div').show().dialog({
			modal:true,
			buttons:[{
					text:"Delete",
					click:function(){
						AssistHelper.processing();
					 	var result = AssistHelper.doAjax("inc_controller.php?action=Template.delete&obj=ACT&id="+act_id);
						$(this).dialog("close");
						AssistHelper.finishedProcessing(result[0], result[1]);
						console.log(result);
				 	}
				},{
					text:"Cancel",
					click:function(){
						$(this).dialog("close");
					}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "act_del_div");
	});

/*
 	$('#btn_process').click(function(){
		AssistHelper.processing();
		AssistHelper.finishedProcessing(result[0], result[1]);
	});	
*/

</script>




