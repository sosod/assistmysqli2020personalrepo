<?php

$_REQUEST['object_type'] = "CONTRACT";
$page_action = "ACTIVATE_WAITING";
$page_section = "NEW";
$object_type="CONTRACT";
$child_object_type = "CONTRACT";
$page_direct = "new_confirmed.php";

$button_label = "|unconfirm|";

$alternative_button_click_function = "reverseButtonClick";

include("common/generic_list_page.php");


$page_redirect_path = "new_activate_done.php?";
	?>
<div style="display:none" id="confirm_sms">
	<h3>Undo Confirmation of <?php echo $helper->getContractObjectName(); ?></h3>
	<p>Are you sure you want to undo the confirmation of this <?php echo $helper->getContractObjectName(); ?>?</p>
</div>
	
	
<script type="text/javascript">
var reftag = '<?php echo $myObject->getRefTag(); ?>';
var page_redirect_path = '<?php echo $page_redirect_path; ?>';
window.useSMS = 0;

function reverseButtonClick($me) {
		var r = $me.attr("ref"); //alert(r);
		var i = reftag+r;
			$("#confirm_sms").dialog({
			modal:true,
			width:500,
			buttons:{
				Undo:function(){
					AssistHelper.processing();
					var dta = "contract_id="+r;
					//console.log(dta);
					var result = AssistHelper.doAjax("inc_controller.php?action=Contract.reverseObjectConfirmation",dta);
					if(result[0]=="ok") {
						document.location.href = page_redirect_path+"r[]="+result[0]+"&r[]="+result[1];
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
				},
				Cancel:function(){
					$(this).dialog("close");
				}
			}
		});
		AssistHelper.hideDialogTitlebar("id","confirm_sms");
		AssistHelper.formatDialogButtons($("#confirm_sms"),0,AssistHelper.getRedCSS());
}
</script>
