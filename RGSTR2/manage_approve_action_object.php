<?php
include("inc_header.php");

//$helper->arrPrint($_REQUEST);

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "ACTION";
$object_id = $_REQUEST['object_id'];

$page_action = $_REQUEST['page_action'];
//ASSIST_HELPER::arrPrint($page_action);
$real_action = strtoupper($_REQUEST['page_action'])=="APPROVED"?"unlock":$_REQUEST['page_action'];

$page_redirect = "manage_approve_".strtolower($object_type).".php?page_action=".$page_action;

switch($object_type) {
	case "REGISTER":
	case "CONTRACT":
		$child_type = "RISK";
		$childObject = new RGSTR2_RISK();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getDeliverableObjectName(true);
		break;
	case "RISK":
	case "DELIVERABLE":
		$child_type = "ACTION";
		$childObject = new RGSTR2_ACTION();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName(true);
		break;
	case "ACTION":
		$child_redirect = "";
		break;
}
$logObject = new RGSTR2_LOG(strtolower($object_type));
$audit_log = $logObject->getObject(array('object_id'=>$object_id));
$audit_log = $audit_log[0];

$approve = $helper->getActivityName("approve");
$decline = $helper->getActivityName("decline");
switch($page_action) {
	case "APPROVED":
		$form_heading = $decline;
		$edit = $helper->getActivityName("edit");
		$restore = $helper->getActivityName("restore");
		$update = $helper->getActivityName("update");
		$form_message = $helper->getDisplayResult(array("warn","This will reverse the previous approval of the ".$helper->getActionObjectName()." and $restore it for $edit and $update."));
		$form_buttons = "<input type=button value='".$decline."' class='isubmit unlock' />";
		break;
	case "APPROVE":
	default:
		$form_heading = $approve." / ".$decline;
		$form_message = "";
		$form_buttons = "<input type=button value='".$approve."' class='isubmit approve' /> <span class=float><input type=button value='".$decline."' class='idelete decline'></span>";
		break;
}



?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php 
			$js.= $displayObject->drawDetailedView($object_type, $object_id, !($object_type=="CONTRACT"),false,($object_type=="CONTRACT")); 
			echo "<h2>Activity Log</h2>".$audit_log;
		?></td>
		<td width=5%>&nbsp;</td>
		<td ><h2><?php echo $form_heading; ?></h2><table class='tbl-container not-max'><tr><td>
			<form name=frm_approve>
				<input type=hidden name=object_id value=<?php echo $object_id; ?> />
				<input type=hidden name=page_action value=<?php echo $page_action; ?> />
			<?php echo $form_message; ?>
			<table class=form>
				<tr>
					<th>Response:</th>
					<td><?php $js.=$displayObject->drawFormField("TEXT",array('id'=>"approve_response",'name'=>"approve_response",'rows'=>10,'cols'=>75)); ?></td>
				</tr>
				<tr>
					<th></th>
					<td>
						<?php echo $form_buttons; ?>
					</td>
				</tr>
			</table>
			<?php ASSIST_HELPER::displayResult(array("info","A response is required when if you choose to ".$helper->getActivityName("decline")." the ".$helper->getObjectName($object_type).".")); ?>
		</form></td></tr></table>
		</td>
	</tr>
	<?php if(isset($childObject)) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>"View",'class'=>"btn_view"),$child_type,$child_options); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
var url = "<?php echo $child_redirect; ?>";
var page_redirect = "<?php echo $page_redirect; ?>";
var ot = "<?php echo strtoupper($object_type); ?>";
$(function() {
	<?php
		echo $js;
	?>
	
	$("input:button.isubmit, input:button.idelete").click(function() {
		AssistHelper.processing();
		var valid8 = true;
		if($(this).hasClass("decline") || $(this).hasClass("unlock")) {
			if($("#approve_response").val().length==0) {
				valid8 = false;
				AssistHelper.finishedProcessing("error","Please enter a response.");
			} else {
				var action = ot+"."+($(this).hasClass("unlock") ? "UNLOCK_APPROVE" : "DECLINE");
			}
		} else {
			var action = ot+".APPROVE";
		}
		if(valid8==true) {
			var dta = AssistForm.serialize($("form[name=frm_approve]"));
			//console.log(dta);
			var result = AssistHelper.doAjax("inc_controller.php?action="+action,dta);
			if(result[0]=="ok") {
				document.location.href = page_redirect+"&r[]="+result[0]+"&r[]="+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	});
});
</script>
<?php
//markTime("end");
?>