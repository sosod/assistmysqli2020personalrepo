<?php
require_once("inc_header.php");

$head_name = $headingObject->getAHeadingNameByField("rsk_owner_id");
$sub_name = $helper->getTerm('SUB');

$deptObject = new RGSTR2_CONTRACT_OWNER();
$dept_list = $deptObject->getActiveMasterDepartmentsList();
$admins_list = $deptObject->getAllActiveAdmins();
$displayObject = new RGSTR2_DISPLAY();
//ASSIST_HELPER::arrPrint($dept_list);

/**********************************
 * format data for the add/edit dialog forms
 */
$list_for_select = array(
	'dept'=>array(),
	'admin'=>array(),
);
foreach($dept_list as $key=>$row){
	$list_for_select['dept'][$key] = $row['name'];
}
$userObject = new RGSTR2_USERACCESS();
$a = $userObject->getActiveUsers();
foreach($a as $key=>$row){
	$list_for_select['admin'][$key] = $row['name'];
}
/**
 * 
 ********************************************/

echo ASSIST_HELPER::getFloatingDisplay(array("info","To add a new ".$sub_name." please contact your Assist Administrator."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

$parent_name = "";

?>
<table class=tbl-container>
    <tr>
        <td>
            <table>
                <tr>
                    <th><?php echo $sub_name; ?></th>
                    <th><?php echo $head_name; ?></th>
                </tr>
                <?php foreach($dept_list as $key => $dept) { ?>
                    <?php if($dept['parent']!=$parent_name) { ?>
                        <?php $parent_name = $dept['parent']; ?>
                        <tr>
                            <th class="thsub left" colspan=3><?php echo $parent_name ?></th>
                        </tr>
                    <?php } ?>

                    <tr>
                        <td class=b><?php echo $dept['child']; ?></td>
                        <td style='padding: 0px;'>
                            <table width=100% class=sub_table style='margin: 0px;'>
                                <tr>
                                    <th class=th2>Administrator</th>
<!--                                    <th class=th2 width=50px>Create</th>-->
                                    <th class=th2 width=50px>Update <?php echo $helper->replaceAllNames("|risks|"); ?></th>
                                    <th class=th2 width=50px>Edit <?php echo $helper->replaceAllNames("|actions|"); ?></th>
                                    <th class=th2 width=50px>Approve Completed <?php echo $helper->replaceAllNames("|actions|"); ?></th>
                                    <th class=th2></th>
                                </tr>
                                <?php //ASSIST_HELPER::arrPrint($admins_list); ?>
                                <?php if(isset($admins_list[$key])) { ?>
                                    <?php foreach($admins_list[$key] as $tkey => $admin) {?>
                                        <?php ?>
                                        <tr sect="<?php echo $key ?>" usr="<?php echo $admin['tkid'] ?>">
                                            <td><?php echo $admin['name'] ?></td>
<!--                                            <td class="center">-->
<!--                                                --><?php //ASSIST_HELPER::displayIconAsDiv(($admin['status'] & RGSTR2_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE)==RGSTR2_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE); ?>
<!--                                            </td>-->
                                            <td class="center">
                                                <?php ASSIST_HELPER::displayIconAsDiv(($admin['status'] & RGSTR2_CONTRACT_OWNER::CAN_UPDATE)==RGSTR2_CONTRACT_OWNER::CAN_UPDATE); ?>
                                            </td>
                                            <td class="center">
                                                <?php ASSIST_HELPER::displayIconAsDiv(($admin['status'] & RGSTR2_CONTRACT_OWNER::CAN_EDIT)==RGSTR2_CONTRACT_OWNER::CAN_EDIT); ?>
                                            </td>
                                            <td class="center">
                                                <?php ASSIST_HELPER::displayIconAsDiv(($admin['status'] & RGSTR2_CONTRACT_OWNER::CAN_APPROVE)==RGSTR2_CONTRACT_OWNER::CAN_APPROVE); ?>
                                            </td>
                                            <td>
                                                <input type="button" class="btn_edit" value="Edit" data-dept_id="<?php echo $key ?>" data-record_id="<?php echo $admin['id'] ?>" data-dept_name="<?php echo $dept['name'] ?>" data-user_tkid="<?php echo $admin['tkid'] ?>"/>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php }?>
                                <tr>
                                    <td colspan=4 class=right></td>
                                    <td><input type=button value=Add class=btn_add dept_id='<?php echo $key; ?>' dept='<?php echo $dept['name']; ?>'  /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td>
	        <?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"OWNER")); ?>
        </td>
    </tr>
</table>




<div id=dlg_add title="Add">
	<h2>Add New <?php echo $head_name; ?></h2>
	<form name=frm_add>
		<input type=hidden name=owner_subid id=dept_id value=0 />
		<table id=tbl_add class=form style='margin-left: 5px; margin-right: 20px'>
			<tr>
				<th width=150px><?php echo $sub_name; ?>:</th>
				<td id=td_add_dept></td>
			</tr>
			<tr>
				<th><?php echo $head_name; ?>:</th>
				<td><?php $js.= $displayObject->drawFormField("LIST",array('id'=>"add_tkid",'name'=>"owner_tkid",'options'=>$list_for_select['admin']),"X");?></td>
			</tr>
<!--			<tr>-->
<!--				<th>Create Deliverable Access (Manage):</th>-->
<!--				<td>--><?php //$js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_create",'name'=>"create",'yes'=>RGSTR2_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE,'no'=>0),RGSTR2_CONTRACT_OWNER::CAN_CREATE_DELIVERABLE); ?><!--</td>-->
<!--			</tr>-->
			<tr>
				<th>Update <?php echo $helper->replaceAllNames("|risks|"); ?> Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_update",'name'=>"update",'yes'=>RGSTR2_CONTRACT_OWNER::CAN_UPDATE,'no'=>0),RGSTR2_CONTRACT_OWNER::CAN_UPDATE); ?></td>
			</tr>
			<tr>
				<th>Edit <?php echo $helper->replaceAllNames("|actions|"); ?> Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_edit",'name'=>"edit",'yes'=>RGSTR2_CONTRACT_OWNER::CAN_EDIT,'no'=>0),RGSTR2_CONTRACT_OWNER::CAN_EDIT); ?></td>
			</tr>
			<tr>
				<th>Approve Completed <?php echo $helper->replaceAllNames("|actions|"); ?> Access (Manage):</th>
				<td><?php $js.= $displayObject->drawFormField("BOOL_BUTTON",array('id'=>"add_approve",'name'=>"approve",'yes'=>RGSTR2_CONTRACT_OWNER::CAN_APPROVE,'no'=>0),RGSTR2_CONTRACT_OWNER::CAN_APPROVE); ?></td>
			</tr>
		</table>
	</form>
</div>

<div id=dlg_edit title="Edit">
	<iframe id=ifr_edit></iframe>
</div>


<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	$("#dlg_add").dialog({
		autoOpen: false,
		modal: true,
		width: "auto",
		buttons: [{
			text:"Save New <?php echo $head_name; ?>",
			click:function() {
				AssistHelper.processing();
				if($("#add_tkid").val()=="X") {
					AssistHelper.finishedProcessing("error","Please select a user.");
				} else {
					var dta = AssistForm.serialize($("form[name=frm_add]"));
					//alert(dta);
					var result = AssistHelper.doAjax("inc_controller.php?action=ContractOwner.Add",dta);
					//console.log(result);
					if(result[0]=="kk") {
		//AssistHelper.finishedProcessing(result[0],result[1]);
						document.location.href = "setup_defaults_admins.php?r[]=ok&r[]="+result[1];
					} else {
						//AssistHelper.closeProcessing();
						AssistHelper.finishedProcessing(result[0],result[1]);
						//alert(result[1]);
					}
				}
			}
		},{
			text:"Cancel",
			click: function() {
				$(this).dialog("close");
			}
		}]
	});
	AssistHelper.hideDialogTitlebar("id","dlg_add");
    AssistHelper.formatDialogButtons($("#dlg_add"),0,AssistHelper.getDialogSaveCSS());
    AssistHelper.formatDialogButtons($("#dlg_add"),1,AssistHelper.getCloseCSS());

	$(".btn_add").click(function() {
		$("#dlg_add #add_tkid option").each(function(){
			$(this).prop("hidden",false);
		});
		var d = $(this).attr("dept_id");
		var n = $(this).attr("dept");
		$("#dlg_add #dept_id").val(d);
		$("#dlg_add #td_add_dept").html(n);
		$("#dlg_add #add_tkid").val("X");
		//Check if user is already an admin - exclude them from the select
		$usrs = $("#dlg_add #add_tkid");
		var existing = [];
		
		$("tr [sect='"+d+"']").each(function(){
			existing.push($(this).attr("usr"));
		});
		//console.log(existing);
		$("#dlg_add #add_tkid option").each(function(){
			var val = $(this).val();
			if(existing.indexOf(val) !== -1){
				$(this).prop("hidden",true);
			}
		});
		$("#dlg_add #add_update_yes").trigger("click");
		$("#dlg_add #add_edit_yes").trigger("click");
		$("#dlg_add #add_approve_yes").trigger("click");
		$("#dlg_add").dialog("open");
	});

	//Determine the necessary width and height settings for the dlg_lines & ifr_lines elements
	var scr = AssistHelper.getWindowSize();
	var dlg_width = 550;
	var dlg_height = 225;
	var ifr_width = 550;
	var ifr_height = 225;


	//Determine the necessary width and height settings for the dlg_edit element
	var tbl_edit = parseInt(AssistString.substr($("#dlg_edit").find("table:first").css("width"),0,-2));
	var edit_width = (tbl_edit+50)+"px";

	/**
	 * Edit dialog - to edit the main object
	 */
	$("#dlg_edit").dialog({
		autoOpen: false,
		modal: true,
        width: "auto",
		buttons: [{
			text:"Save",
			icons: {primary: "ui-icon-disk"},
			click:function(e) {
				e.preventDefault();
				AssistHelper.processing();
				/**
				 * PROCESS EDIT FORM HERE (check for required fields etc)
				 */

                var dept_id = $("#ifr_edit").contents().find("#dept_id").val();
                var record_id = $("#ifr_edit").contents().find("#record_id").val();
                var update = $("#ifr_edit").contents().find("#update").val();
                var edit = $("#ifr_edit").contents().find("#edit").val();
                var approve = $("#ifr_edit").contents().find("#approve").val();

                var dta = "dept_id="+dept_id+"&record_id="+record_id+"&update="+update+"&edit="+edit+"&approve="+approve;

                console.log('DATA');
                console.log(dta);

				var result = AssistHelper.doAjax("inc_controller.php?action=ContractOwner.Edit",dta);	//live result process
//				var result = ["ok","display the result of the form processing here and then reload the page to reflect the changes"]; //result message for demo purposes
				//If the process was finished with an "ok" result, display and then reload the page to reflect the changes
				if(result[0]=="ok") {
					var url = document.location.href;
                    document.location.href = document.location.href +"?"+"r[]="+result[0]+"&r[]="+result[1];
				} else {
					//if result was not "ok" then display the message - don't close the dialog or reload the page so that the user can decide what to do after seeing the message
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		},{
			text: "Delete",
            icons: {primary: "ui-icon-trash"},
			click:function(e){
                e.preventDefault();
                AssistHelper.processing();
                /**
                 * PROCESS EDIT FORM HERE (check for required fields etc)
                 */

                var dept_id = $("#ifr_edit").contents().find("#dept_id").val();
                var record_id = $("#ifr_edit").contents().find("#record_id").val();
                var update = $("#ifr_edit").contents().find("#update").val();
                var edit = $("#ifr_edit").contents().find("#edit").val();
                var approve = $("#ifr_edit").contents().find("#approve").val();

                var dta = "dept_id="+dept_id+"&record_id="+record_id+"&update="+update+"&edit="+edit+"&approve="+approve;

                console.log('DATA');
                console.log(dta);

                var result = AssistHelper.doAjax("inc_controller.php?action=ContractOwner.DELETE",dta);	//live result process
//				var result = ["ok","display the result of the form processing here and then reload the page to reflect the changes"]; //result message for demo purposes
                //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                if(result[0]=="ok") {
                    var url = document.location.href;
                    document.location.href = document.location.href +"?"+"r[]="+result[0]+"&r[]="+result[1];
                } else {
                    //if result was not "ok" then display the message - don't close the dialog or reload the page so that the user can decide what to do after seeing the message
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }
			}
		},{
            text: "Cancel",
            click:function(e){
                e.preventDefault();
                //Close the dialog
                $("#dlg_edit").dialog("close");
            }
        }]
	});
    AssistHelper.hideDialogTitlebar("id","dlg_edit");
	AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),0,"ui-state-green");
	AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),1,"ui-state-error");
	AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),2,"ui-state-error");
	/**
	 * "Edit" button - display the dlg_edit dialog pop-up
	 */
	$(".btn_edit")
		/*.button({icons:{primary:"ui-icon-pencil"}})*/
		.click(function(e) {
			e.preventDefault();
			$("#dlg_edit").dialog("open");

            var dept_id = $(this).data("dept_id");
            var record_id = $(this).data("record_id");
            var dept_name = $(this).data("dept_name");
            var user_tkid = $(this).data("user_tkid");

            console.log('DEPIT, DEPTNAME, & USER TKID');
            console.log(dept_id);
            console.log(record_id);
            console.log(user_tkid);

            var url = "setup_defaults_admins_edit_dlg.php?dept_id="+dept_id+"&record_id="+record_id+"&dept_name="+dept_name+"&user_tkid="+user_tkid+"&head_name=<?php echo $head_name; ?>&sub_name=<?php echo $sub_name; ?>";
			//call iframe in dialog
			$("#ifr_edit").prop("src",url).prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid white");
		})/*.removeClass("ui-state-default")/*.addClass("ui-state-black")*/;



});

/**
 * Function called by the ifr_lines Iframe to open the dlg_lines dialog once the iframe source page has loaded and is ready to be displayed to the user
 */
function openDialog() {
    $(function() {
        $("#dlg_edit").dialog("open");
        AssistHelper.closeProcessing();
    });
}

/**
 * Function called by the ifr_lines Iframe to remove the save button and replace with a close button if all lines are already selected
 */
function changeDialogButton() {
    $(function() {
        var butt = [{text:"Close",click:function(e) {e.preventDefault(); $("#dlg_edit").dialog("close");}}]
        $("#dlg_edit").dialog('option','buttons',butt);
    });
}
</script>