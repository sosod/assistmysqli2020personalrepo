<?php
$future = strtotime(date("d F Y",($today+($next_due*24*3600))));

$myObject = new RGSTR2_ACTION();
$my_field_name = $myObject->getTableField();
$data = $myObject->getObject(array('type'=>"FRONTPAGE_TABLE",'deadline'=>$future,'limit'=>(isset($action_profile['field2']) ? $action_profile['field2'] : 20)));
$headings2 = $data['head'];
$rows = $data['rows'];
$headObject = new RGSTR2_HEADINGS();
$headings = $headObject->getMainObjectHeadings("ACTION","LIST","DASHBOARD");
$head = array();
foreach($headings2 as $fld => $h) {
	if(isset($headings[$fld]) || substr($fld,0,strlen($my_field_name))!=$my_field_name) {
		$head[$fld] = array(
			'text'=>$h['name'],
			'long'=>($h['type']=="TEXT"),
			'deadline'=>($fld==$myObject->getDeadlineFieldName()),
		);
	}
}
$head['link'] = array('text'=>"",'long'=>false,'deadline'=>false);
$actions = array();
foreach($rows as $id => $r) { //arrPrint($r);
	$actions[$id] = array();
	foreach($head as $fld=> $h){
		if($fld!="link"){
			if(is_array($r[$fld])) {
				$actions[$id][$fld] = $r[$fld]['display']; //(isset($r[$fld]['display']) ? $r[$fld]['display'] : $r[$fld]);
			} else {
				$actions[$id][$fld] = $r[$fld]; //(isset($r[$fld]['display']) ? $r[$fld]['display'] : $r[$fld]);
			}
		}
	}
	$actions[$id]['link'] = "manage_update_action_object.php?page_redirect=home|object_id=".$id;
	
}