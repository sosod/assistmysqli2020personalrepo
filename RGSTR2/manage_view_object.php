<?php
include("inc_header.php");

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "REGISTER";
$object_id = $_REQUEST['object_id'];

//echo '<pre style="font-size: 18px">';
//echo '<p>REQUEST</p>';
//print_r($_REQUEST);
//echo '</pre>';

switch($object_type) {
	case "REGISTER":
		$child_type = "RISK";
		$childObject = new RGSTR2_RISK();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=RISK&object_id=";
		$child_name = $helper->getDeliverableObjectName(true);
		break;
	case "DELIVERABLE":
	case "RISK":
		$child_type = "ACTION";
		$childObject = new RGSTR2_ACTION();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName(true);
		break;
	case "ACTION":
		$child_redirect = "";
		break;
}

$logObject = new RGSTR2_LOG(strtolower($object_type));
$audit_log = $logObject->getObject(array('object_id'=>$object_id));
$audit_log = $audit_log[0];


/*
 * ********************************************************************
 * 					TSHEGO  - Get rid this when done
 * ********************************************************************
 */

//echo '<pre style="font-size: 18px">';
//echo '<p>OBJECT TYPE</p>';
//print_r($object_type);
//echo '</pre>';

//echo '<pre style="font-size: 18px">';
//echo '<p>CHILD OBJECTS</p>';
//print_r($child_objects);
//echo '</pre>';


$button_label = $helper->getActivityName("open");
$button_icon = 'newwin';
$button = array('value'=>$helper->replaceAllNames($button_label),'class'=>"btn_view",'type'=>"button");
if(isset($button_icon)) {
	$button['icon'] = $button_icon;
}

$js.="
		$(\"button.btn_view\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";



?>
<!--<button id=btn_add_comment>Add Comment</button>-->
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, !($object_type=="REGISTER"),false,($object_type=="REGISTER")); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Activity Log</h2><?php 
			echo $audit_log;
		?></td>
	</tr>
	<?php if(isset($childObject)) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,$button,$child_type,$child_options,false,array( (isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : ""))); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
var url = "<?php echo $child_redirect; ?>";
$(function() {
	<?php 
	echo $js; 
	?>
	$("#view_all").val("View Complete "+window.contract_object_name);
	$("input:button.btn_view").click(function() {
		tableButtonClick($(this));
	});
	$("#view_all").click(function(){
		AssistHelper.processing();
		if(confirm("Please be patient while the page loads.  It can take some time depending on the amount of information.")==true) {
			document.location.href = "manage_view_contract_edit_object.php?object_id=<?php echo $object_id; ?>";
		} else {
			AssistHelper.closeProcessing();
		}
	});
	
	$("#btn_add_comment").button({
		icons: {primary: "ui-icon-comment"},
	}).css({"position":"absolute","top":"10px","right":"10px"}).removeClass("ui-state-default").addClass("ui-button-state-ok")
	.click(function(e) {
		e.preventDefault();
		$("<div />",{id:"dlg_comment"}).dialog({modal: true});
	});
	
});
function tableButtonClick($me) {
	var i = $me.attr("ref");
	url = url+i;
	document.location.href = url;
}
</script>
<?php
//markTime("end");
?>