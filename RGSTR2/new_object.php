<?php
include("inc_header.php");

$display_type = !isset($display_type) ? "default" : $display_type;

//$helper->arrPrint($_REQUEST);

//echo '<pre style="font-size: 18px">';
//echo '<p>PARENT TYPE</p>';
//print_r($parent_object_type);
//echo '</pre>';

$parent_object_type = ($parent_object_type == 'CONTRACT' ? 'REGISTER' : $parent_object_type);
$parent_object_type = ($parent_object_type == 'DELIVERABLE' ? 'RISK' : $parent_object_type);

switch($parent_object_type) {
	case "CONTRACT":
	case "REGISTER":
		$childObject = new RGSTR2_RISK();

		if($display_type=="default") {
			$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'contract_id'=>$parent_object_id));
		}

		$child_redirect = "new_deliverable_edit.php?object_type=RISK&object_id=";
		$child_name = $helper->getDeliverableObjectName();

		$parentObject = new RGSTR2_REGISTER($parent_object_id);
		$child_object_type = "RISK";
		$child_object_id = 0;

		$parent_id_name = $childObject->getParentFieldName();
		break;
	case "RISK":
		$childObject = new RGSTR2_ACTION();
		if($display_type=="default") {
			$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'deliverable_id'=>$parent_object_id));
		} 
		$child_redirect = "new_action_edit.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName();
		$parentObject = new RGSTR2_RISK($parent_object_id);
		$child_object_type = "ACTION";
		$child_object_id = 0;		
		$parent_id_name = $childObject->getParentFieldName();
		//$parent_id = $object_id;

		break;
	case "ACTION":
		$child_redirect = "";
		$parent_id = $object_id;
		$child_objects = array();
		break;
}


$options = array(
	'type'=>"LIST",
	'section'=>"NEW",
	'page'=>$page_action,
);

$button_label = 'edit';
$button_icon = 'pencil';
$button = array('value'=>$helper->replaceAllNames($button_label),'class'=>"btn_list",'type'=>"button");
if(isset($button_icon)) {
	$button['icon'] = 'pencil';
}

$js.="
		$(\"button.btn_list\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class='tbl-container not-max'>
	<tr>
	<?php if($display_type=="default") { ?>
        <?php $td2_width = "48%"; ?>
		<td width=47%>
			<?php 
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="REGISTER"));
			$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : ""; 
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=<?php echo $td2_width; ?>>
            <h2>New <?php echo $child_name; ?></h2>
	<?php } else {  ?>
		<td>
    <?php } ?>

        <?php
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?>
        </td>
	</tr>
	<?php if(isset($child_objects) && count($child_objects)>0 && $display_type=="default") { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,$button,$child_object_type,$options,false,array( (isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : ""))); ?>
			<?php //$js.=$displayObject->drawListTable($child_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_edit")); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
$(function() {
	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
	}
	?>
});
</script>

<script type=text/javascript>
	var url = "<?php echo $child_redirect; ?>";
	var section = "<?php echo $child_object_type; ?>";
	$(function() {
		<?php echo $js; ?>
	});
	function tableButtonClick($me) {
		console.log($me);
		<?php
		if(isset($alternative_button_click_function) && $alternative_button_click_function!==false && strlen($alternative_button_click_function)>0) {
			echo $alternative_button_click_function."($"."me);";
		} else {
			echo "
			AssistHelper.processing();
				var r = $"."me.attr(\"ref\");
				
				document.location.href = url+r;
				";
		}
		?>

	}

	function formatTableButton($me) {
		$me.button({
			icons: {primary: "ui-icon-<?php echo isset($button['icon']) ? $button['icon'] : "newwin"; ?>"},
		}).children(".ui-button-text").css({"font-size":"80%"}).click(function(e) {
			e.preventDefault();
			tableButtonClick($(this));
			console.log('BLue Duval');
		});
	}
</script>
