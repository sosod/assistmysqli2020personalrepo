<?php

require_once("../module/autoloader.php");
$scripts = array();
ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);





/**
 * Other module code here e.g. get PARENT OBJECT details etc.
 */
//$objectObject = new MODLOC_OBJECT();
$object_name = "OBJECT";	//replace with something like $modlocObject->getObjectName();
$active_record = 2;	//replace with something like MODLOC_OBJECT::ACTIVE

$object_id = $_REQUEST['object_id'];
$object_type = $_REQUEST['object_type'];


/**
 * Actual code starts here
*/

$source_module = $_REQUEST['src'];
$filter = $_REQUEST['filter'];





$linkObject = new ASSIST_INTERNAL();
$linkObject->setSource($source_module);
$data = $linkObject->getLinesByFilter($filter);
$headings= $data['head'];
$lines = $data['rows'];

//GET EXISTING LINE INFORMATION
//Could be a call to a function in MODREF_OBJECT class
$sql = "SELECT lne_id as id, lne_srcid as line_id ";
$sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
$sql .= "WHERE lne_srcmodref = '$source_module' ";
$sql .= "AND lne_objectid = $object_id ";
$sql .= "AND lne_object_type = '$object_type' ";
$sql .= "AND (lne_status & ".$active_record.") = ".$active_record;
$existing_lines = $linkObject->mysql_fetch_value_by_id($sql,"id","line_id");
?>
<h1>Select Lines</h1>
<table>
	<tr>
		<th><?php 
		if(count($lines)>0) {
			echo "<input type=checkbox value=ALL name=chk_all id=chk_all />";
		} ?></th>
	<?php
	foreach($headings as $fld => $head) {
		echo "<th>$head</th>";
	}
	?>
	</tr>
	<?php
	if(count($lines)>0) {
			
		foreach($lines as $line_id => $line) {
			if(in_array($line_id,$existing_lines)) {
				$checkbox = $linkObject->getDisplayIcon("ok");
			} else {
				$checkbox = "<input type=checkbox class=chk_line line=".$line_id." value=Y name=chk_use[$line_id] />";
			}
			echo "
			<tr>
				<td class=center>".$checkbox."</td>";
			foreach($headings as $fld => $head) {
				echo "
				<td>".$line[$fld]."</td>
				";
			}
			echo "
			</tr>";
		}
	} else {
		echo "
		<tr>
			<td colspan=".(count($headings)+1).">No lines available.</td>
		</tr>";
	}
	?>
</table>

<script type="text/javascript">
$(function() {
	//Check if there are any lines to select
	if($("input:checkbox").length==1) {
		//hide select all checkbox
		$("input:checkbox").hide();
		//change button from Save to Close
		window.parent.changeDialogButtons();
	}
	
	
	//Tell parent window to open the dialog once the page has finished loading
	window.parent.openDialog();

	//"Select All" checkbox
	$("#chk_all").click(function() {
		if($(this).is(":checked")) {
			$("input:checkbox").prop("checked",true);
		} else {
			$("input:checkbox").prop("checked",false);
		}
	}).focus();
	
});
</script>