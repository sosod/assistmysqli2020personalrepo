<?php
$_REQUEST['object_type'] = "CONTRACT";
$page_action = "RESTORE";
$page_section = "ADMIN";
$child_object_type = "CONTRACT";

$page_direct = "admin_restore_contract.php";

$button_label = "|restore|";

$alternative_button_click_function = "restoreButtonClick";

include "common/generic_list_page.php";

$page_redirect_path = "admin_restore_contract.php?";
?>
<div style="display:none" id="confirm_sms">
    <h3>Restore <?php echo $helper->getContractObjectName(); ?></h3>
    <p>Are you sure you want to restore this <?php echo $helper->getContractObjectName(); ?>?</p>
</div>


<script type="text/javascript">
    var reftag = '<?php echo $myObject->getRefTag(); ?>';
    var page_redirect_path = '<?php echo $page_redirect_path; ?>';
    window.useSMS = 0;

    function restoreButtonClick($me) {
        var r = $me.attr("ref"); //alert(r);
        var i = reftag+r;
        $("#confirm_sms").dialog({
            modal:true,
            width:500,
            buttons:{
                Restore:function(){
                    AssistHelper.processing();
                    var dta = "contract_id="+r;
                    //console.log(dta);
                    var result = AssistHelper.doAjax("inc_controller.php?action=Contract.restoreObject",dta);
                    if(result[0]=="ok") {
                        document.location.href = page_redirect_path+"r[]="+result[0]+"&r[]="+result[1];
                    } else {
                        AssistHelper.finishedProcessing(result[0],result[1]);
                    }
                },
                Cancel:function(){
                    $(this).dialog("close");
                }
            }
        });
        AssistHelper.hideDialogTitlebar("id","confirm_sms");
        AssistHelper.formatDialogButtons($("#confirm_sms"),0,AssistHelper.getRedCSS());
    }
</script>