<?php

$_REQUEST['object_type'] = "CONTRACT";
$page_action = "EDIT";
$page_section = "NEW";
$child_object_type = "CONTRACT";
$button_label = "|view|";

$add_button = true;
$add_button_label = "|add| |register|";
$add_button_function = "showAddDialog();";


include("common/generic_list_page.php");


?>

<div id=dlg_add_contract title="<?php echo $helper->replaceAllNames("|add| |register|"); ?>">
	<?php
	
	$section = "NEW";
	$page_redirect_path = "new_contract_edit.php?";
	$page_action = "Add";
	
	$uaObject = new RGSTR2_USERACCESS();

	$child_object_type = "REGISTER";
	$child_object_id = 0;
	$childObject = new RGSTR2_REGISTER();
	
	$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
	echo $data['display'];

?>
</div>
<script type="text/javascript">
$(function () {
	$("#dlg_add_contract").dialog({
		autoOpen: false,
		modal: true
	});
	<?php echo $data['js']; ?>
	
});
function showAddDialog() {
	$(function() {
		$("#dlg_add_contract").dialog("open");
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>800) {
			$("#dlg_add_contract form[name=frm_object] table.form").css("width","800px");
			$("#dlg_add_contract").dialog("option","width",850);
		} else {
			$("#dlg_add_contract form[name=frm_object] table.form").css("width",(my_window['width']-100)+"px");
			$("#dlg_add_contract").dialog("option","width",(my_window['width']-50));
		}
		$("#dlg_add_contract").dialog("option","height",my_window['height']-50);
	});
}
</script>
