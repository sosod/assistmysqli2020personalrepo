<?php

$parent_object_type = "RISK";
$parent_object_id = $_REQUEST['object_id'];

$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "default";
if($display_type=="dialog") {
    $no_page_heading = true;
    $page_redirect_path = "dialog";
} else {
    $page_redirect_path = "new_action_add_object.php?object_id=".$parent_object_id."&";
}

$go_back = "new_libraries.php";
$page_action = "Add";

//===========================================

include("inc_header.php");

$display_type = !isset($display_type) ? "default" : $display_type;

switch($parent_object_type) {
    case "RISK":
        $childObject = new RGSTR2_ACTION();
        if($display_type=="default") {
            $child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'deliverable_id'=>$parent_object_id));
        }
        $child_redirect = "new_action_edit.php?object_type=ACTION&object_id=";
        $child_name = $helper->getActionObjectName();
        $parentObject = new RGSTR2_RISK($parent_object_id);
        $child_object_type = "ACTION";
        $child_object_id = 0;
        $parent_id_name = $childObject->getParentFieldName();

        break;
}

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class="tbl-container not-max" "width=47%">
    <tr>

        <td>
            <?php
            $js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="REGISTER"));
            $js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : "";
            ?>
        </td>
    </tr>
</table>
<script type=text/javascript>
    $(function() {
        <?php echo $js; ?>
        <?php
        if($display_type=="dialog") {
            echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
        }
        ?>
    });
</script>

<?php /*============================================================*/ ?>
<?php /*=============== LINE ITEM SECTION CODE - START =============*/ ?>
<?php /*============================================================*/ ?>

<?php
//===================
$object_id = $parent_object_id;

?>
<h2>Select Line Items</h2>
<table>
    <tr>
        <td>Link Library Items By Hazard:</td>
        <td>
            <input type="radio" name="context" value="hazard"/>
        </td>
    </tr>
    <tr>
        <td>Link Library Items Directly:</td>
        <td>
            <input type="radio" name="context" value="library"/>
        </td>
    </tr>
</table>

<br/>
<br/>

<Table>
    <tr id="hazard_table_row">
        <td class=b>Hazard:</td>
        <td>
            <select id="sel_hazard">
                <option value=X>--- SELECT ---</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class=b>Source:</td>
        <td><select id=sel_source>
                <option value=X >--- SELECT ---</option>
            </select></td>
    </tr>
    <tr>
        <td class=b>Filter:</td>
        <td><select id=sel_filter><option value=X>--- SELECT ---</option>
            </select></td>
    </tr>
    <tr>
        <td class=b></td>
        <td class=right><button id="btn_filter" data-request_scheme="<?php echo $_SERVER['REQUEST_SCHEME']; ?>" data-server_name="<?php echo $_SERVER['SERVER_NAME']; ?>" data-mod_location="<?php echo $_SESSION['modlocation']; ?>">Go</button></td>
    </tr>
</Table>

<?php
/**
 * DIV & IFRAME elements to handle dialog needed to display available lines
 */
?>
<div id=dlg_lines title="Select lines">
    <iframe id=ifr_lines_1></iframe>
</div>
<?php
/**
 * Existing LINES ITEMS already linked to the PARENT OBJECT
 */
?>


<script>
    $(function() {

        //Determine the necessary width and height settings for the dlg_lines & ifr_lines_1 elements
        var scr = AssistHelper.getWindowSize();
        var dlg_width = scr['width']*0.95;
        var dlg_height = scr['height']*0.95;
        var ifr_width = dlg_width-20;
        var ifr_height = dlg_height-100;

        $('input:radio[name=context]').on('change', function(e) {
            //console.log('KING PRO');
            //console.log($('input:radio[name=context]:checked').val());

            var context = $('input:radio[name=context]:checked').val();

            if(context == 'hazard'){
                //get hazard specific stuff
                $("#hazard_table_row").show();
                $("#sel_hazard option").remove();

                //Get all available hazards and put them in a box
                var dta = '';
                var html = AssistHelper.doAjax("inc_controller.php?action=HAZARD.GETHAZARDSFORSELECT",dta);
                $("#sel_hazard").append(html);

                //Make the Source Box Inform the user that they must select a hazard first
                $("#sel_source").empty();
                var html = '<option value="X">--- Select a Hazard First ---</option>';
                $("#sel_source").append(html);

                //Make the Filter Box Inform the user that they must select a source first
                $("#sel_filter").empty();
                var html = '<option value="X">--- Select a Source First ---</option>';
                $("#sel_filter").append(html);

            }else{
                //we're working with direct links
                $("#sel_hazard").empty();
                var html = '<option value="X">--- No Hazard To SELECT ---</option>';
                $("#sel_hazard").append(html);
                $("#hazard_table_row").hide();

                //populate source box with all the available libraries
                $("#sel_source").empty();
                var dta = '';
                var html = AssistHelper.doAjax("inc_controller.php?action=LINES.GETSOURCESELECT",dta);
                $("#sel_source").append(html);

                //Make the Filter Box Inform the user that they must select a source first
                $("#sel_filter").empty();
                var html = '<option value="X">--- Select a Source First ---</option>';
                $("#sel_filter").append(html);
            }
        });


        $('#sel_hazard').on('change', function(e) {
            console.log('Hazard Change');
            console.log($(this).val());

            var value = $(this).val();

            if(value != 'X'){
                $("#sel_source").empty();
                //populate source box with all the available libraries linked to this hazard
                var dta = "hazard_id=" + value;
                var html = AssistHelper.doAjax("inc_controller.php?action=LINES.GETLINKEDSOURCESELECT",dta);
                $("#sel_source").append(html);

            }else{
                //Make the Source Box Inform the user that they must select a hazard first
                $("#sel_source").empty();
                var html = '<option value="X">--- Select a Hazard First ---</option>';
                $("#sel_source").append(html);
            }

            //Make the Filter Box Inform the user that they must select a source first
            $("#sel_filter").empty();
            var html = '<option value="X">--- Select a Source First ---</option>';
            $("#sel_filter").append(html);

        });

        $('#sel_source').on('change', function(e) {
            console.log('Source Change');
            console.log($(this).val());

            var value = $(this).val();

            if(value != 'X'){
                $("#sel_filter").empty();
                //populate source box with all the available libraries linked to this hazard
                var dta = "src=" + value;
                var html = '';
                var hazard_value = $('#sel_hazard').val();

                console.log(hazard_value);

                if(hazard_value == 'X'){
                    var filters = AssistHelper.doAjax("inc_controller.php?action=INTERNAL.getFilters",dta);
                    if(Object.keys(filters).length>0) {
                        $("#sel_filter").append($("<option />",{value:"X",text:"--- SELECT ---"}));
                        //populate with remaining filters
                        Object.keys(filters).forEach(function(key) {
                            console.log(key + ': ' + filters[key]);
                            $("#sel_filter").append($("<option />",{value:key,text:filters[key]}));
                        });
                    } else {
                        $("#sel_filter").append($("<option />",{value:"X",text:"--- No options available. Please select another source. ---"}));
                    }
                }else{
                    var dta = "src=" + value + "&" + "hazard_id=" + hazard_value;

                    html = AssistHelper.doAjax("inc_controller.php?action=LINES.GETLINKEDFILTERS",dta);
                    $("#sel_filter").append(html);
                }

            }else{
                //Make the Filter Box Inform the user that they must select a source first
                $("#sel_filter").empty();
                var html = '<option value="X">--- Please select a source above ---</option>';
                $("#sel_filter").append(html);
            }

        });

        /**
         * "Go" button - process the selected Source and Filter and display the available lines in the dlg_lines dialog pop-up
         */
        $("#btn_filter").button()
            .click(function(e) {
                e.preventDefault();
                AssistHelper.processing();
                if($("#sel_source").val()=="X") {
                    AssistHelper.finishedProcessing("error","Please select a Source.");
                } else if($("#sel_filter").val()=="X") {
                    AssistHelper.finishedProcessing("error","Please select a Filter.");
                } else {
                    var src = $("#sel_source option:selected").text();
                    var filt = $("#sel_filter option:selected").text();
                    $("#dlg_lines").dialog("option","title","Select Lines from "+src+": "+filt);


                    var filter_id = $("#sel_filter").val();
                    var src = $("#sel_source").val();
                    var object_id = <?php echo $object_id; ?>;//rsk_id
                    var object_type;

                    //Now to connect an actual connection
                    var hazard_value = $('#sel_hazard').val();
                    if(hazard_value == 'X'){//This option connects the library tems to the risk directly
                        object_type = 'rsk';//
                    }else{
                        object_type = 'hzd';//
                    }

                    var modlocation = $(this).data('mod_location');
                    var request_scheme = $(this).data('request_scheme');
                    var server_name = $(this).data('server_name');

                    var url = request_scheme + "://" + server_name + "/" + modlocation + "/new_libraries_dialog_lines.php?src="+src+"&filter="+filter_id+"&object_id="+object_id+"&object_type="+object_type+"&hazard_value="+hazard_value;


                    console.log('===== DIALOGUE URL - LINES =====');
                    console.log(url);

                    //call iframe in dialog
                    $("#ifr_lines_1").prop("src",url);
//                    $('#ifr_lines_1').attr('src', url);

                    console.log('===== IFRAME SRC =====');
                    console.log($("#ifr_lines_1").attr('src'))
                    //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
                    //$("#dlg_lines").dialog("open");
                }
            }).removeClass("ui-state-default").addClass("ui-state-black");

        /**
         * Dialog to display available lines after clicking the "Go" button
         */
        $("#dlg_lines").dialog({
            autoOpen: false,
            modal: true,
            width: dlg_width,
            height: dlg_height,
            buttons: [{
                text:"Save",
                icons: {primary: "ui-icon-disk"},
                click:function(e) {
                    e.preventDefault();
                    AssistHelper.processing();
                    var object_type = 'rsk';//
                    var hazard_value = $('#sel_hazard').val();
                    var dta = "object_id=<?php echo $object_id; ?>&srcmodref="+$("#sel_source").val()+"&object_type="+object_type+"&hazard_value="+hazard_value;
                    var line_count = 0;
                    $("#ifr_lines_1").contents().find("input:checkbox.chk_line").each(function() {
                        if($(this).attr("line") && $(this).is(":checked")) {
                            dta+="&chk_use["+$(this).attr("line")+"]="+$(this).val();
                            line_count++;
                        }
                    });
                    if(line_count>0) {
                        //live process
                        console.log('THESE ARE THE LINES');
                        console.log(dta);

                        var result;

                        if(hazard_value == 'X'){//This option connects the library tems to the risk directly
                            result = AssistHelper.doAjax("inc_controller.php?action=LINES.ADDLINKS",dta);
                        }else{
                            result = AssistHelper.doAjax("inc_controller.php?action=LINES.ADDHAZARDLINELINKSTORISK",dta);
                        }


                        console.log('THESE ARE THE RESULTS === FORMATTED FOR DEV RIGHT NOW');
                        console.log(result);
                    } else {
                        var result = ["error","You did not select any lines to save."];
                    }
                    //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                    if(result[0]=="ok") {
                        var url = document.location.href;
                        //AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                        //failSafe Hack
                    document.location.href = 'new_libraries_view.php?' + 'r[]=' + result[0] + '&r[]=' + result[1] + '&object_id=' + <?php echo $object_id; ?>;
                    } else {
                        //if result was not "ok" then display the message
                        AssistHelper.finishedProcessing(result[0],result[1]);
                    }
                }
            },{
                text: "Cancel",
                click:function(e){
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_lines").dialog("close");
                }
            }]
        });
        AssistHelper.formatDialogButtonsByClass($("#dlg_lines"),0,"ui-state-green");
        /**
         * Iframe within the dlg_lines div
         */
        $("#ifr_lines_1").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");


        /******************
         * EXISTING LINES section
         */



        /**
         * "Delete" button - remove link between module and src line
         */
        $(".btn_delete")
            .button({
                icons:{primary:"ui-icon-trash"},
                text: false
            }).click(function(e) {
            e.preventDefault();
            var ref = $(this).attr("ref");

            if(confirm("Are you sure you wish to unlink line "+ref+"? This will not delete the line, only remove the link to this risk.")==true) {
                var record_id = $(this).attr("record");
                var result;

                var link_type = $(this).data('link_type');

                if(link_type == 'direct'){
                    result = AssistHelper.doAjax("inc_controller.php?action=LINES.DELETELINK","line_id="+record_id)
                }else{
                    result = AssistHelper.doAjax("inc_controller.php?action=LINES.DELETEHAZARDLINELINKSFROMRISK","line_id="+record_id)
                }

                console.log('THESE ARE THE RESULTS === FORMATTED FOR DEV RIGHT NOW');
                console.log(result);

                //The delete function should: UPDATE dbref_lines SET lne_status = 0 WHERE lne_id = ".$var[line_id]
                if(result[0]=="ok") {
                    var url = document.location.href;
                    AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);

                    //failSafe Hack
                    document.location.href = 'new_libraries_view.php?' + 'r[]=' + result[0] + '&r[]=' + result[1] + '&object_id=' + $(this).data('object_id');
                } else {
                    //if result was not "ok" then display the message
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }
            }
        }).removeClass("ui-state-default").addClass("ui-state-error");

    });


    /*****************************
     * ADDITIONAL FUNCTIONS CALLED BY IFRAME
     */


    /**
     * Function called by the ifr_lines_1 Iframe to open the dlg_lines dialog once the iframe source page has loaded and is ready to be displayed to the user
     */
    function openDialog() {
        $(function() {
            $("#dlg_lines").dialog("open");
            AssistHelper.closeProcessing();
        });
    }

    /**
     * Function called by the ifr_lines_1 Iframe to remove the save button and replace with a close button if all lines are already selected
     */
    function changeDialogButton() {
        $(function() {
            var butt = [{text:"Close",click:function(e) {e.preventDefault(); $("#dlg_lines").dialog("close");}}]
            $("#dlg_lines").dialog('option','buttons',butt);
        });
    }
</script>


<?php /*============================================================================*/ ?>
<?php /*=============== DIRECTLY LINKED LINE ITEM SECTION CODE - START =============*/ ?>
<?php /*============================================================================*/ ?>
<?php
$active_record = 2;	//replace with something like MODLOC_OBJECT::ACTIVE
$object_type = 'rsk';
/**
 * Actual code starts here
 *
 * 1. Create ASSIST_INTERNAL object
 * 2. Tell the ASSIST_INTERNAL object that you are working with LIBRARY modules
 * 3. Get ASSIST_INTERNAL to find all LIBRARY modules available on the client database - used to populate the SOURCE select.
 *
 */
$linkObject = new ASSIST_INTERNAL();
$linkObject->setSourceType("library");
$sources = $linkObject->getSources();
$existing_lines = array();
$existing_lines_details = array();

$sources_session_array = $_SESSION[$_SESSION['modref']]['sources'];

//GET EXISTING LINE INFORMATION
//Could be a call to a function in MODREF_OBJECT class
$sql = "SELECT lne_id as id, lne_srcmodref as modref, lne_srcid as line_id ";
$sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
$sql .= "WHERE lne_objectid = $object_id ";
$sql .= "AND lne_object_type = '$object_type' ";
$sql .= "AND (lne_status & ".$active_record.") = ".$active_record;
$existing_lines = $linkObject->mysql_fetch_value_by_id2($sql, "modref","id","line_id");

foreach($sources as $modref => $src) {
    if(isset($existing_lines[$modref])) {
        $existing_lines_details[$modref] = $linkObject->getLinesByID($modref,$existing_lines[$modref]);
    } else {
        $existing_lines_details[$modref] = array();
    }
}
?>
<h2>Directly Linked Line Items</h2>
<h3>Line Items</h3>
<?php foreach($sources as $modref => $src) { ?>
    <?php if(isset($existing_lines[$modref]) && count($existing_lines[$modref])>0 && isset($existing_lines_details[$modref]['rows']) && count($existing_lines_details[$modref]['rows'])>0){ ?>
        <h3><?php echo $src; ?></h3>
        <table width=100%>
            <tr>
                <th>Ref</th>
                <?php foreach($existing_lines_details[$modref]['head'] as $fld => $head) { ?>
                    <th><?php echo $head; ?></th>
                <?php } ?>
                <th width=50px></th>
            </tr>

            <?php foreach($existing_lines[$modref] as $record_id => $src_line_id) { ?>
                <?php
                $row = $existing_lines_details[$modref]['rows'][$src_line_id];
                $ref = $modref."/".$src_line_id;

                //Preparation for the pop up dialogue
                $mod_location = $sources_session_array[$modref]['modlocation'];
                $lib_object_type = 'ACTIVITY';
                $lib_object_id = $src_line_id;
                ?>
                <tr>
                    <td><?php echo $ref; ?></td>
                    <?php foreach($existing_lines_details[$modref]['head'] as $fld => $head) { ?>
                        <td><?php echo $row[$fld]; ?> <?php echo ($fld == 'activity_name' ? '<a href="#" class="action_dlg" data-mod_location="' . $mod_location . '" data-mod_ref="' . $modref . '" data-lib_object_id="' . $lib_object_id . '" data-lib_object_type="' . $lib_object_type . '" data-request_scheme="' . $_SERVER['REQUEST_SCHEME'] . '" data-server_name="' . $_SERVER['SERVER_NAME'] . '">View</a>' : ''); ?></td>
                    <?php } ?>
                    <td class=center><button class=btn_delete record="<?php echo $record_id; ?>" ref="<?php echo $ref; ?>" data-object_id="<?php echo $object_id; ?>" data-link_type="direct">Delete</button></td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
<?php } ?>

<?php /*============================================================================*/ ?>
<?php /*=============== DIRECTLY LINKED LINE ITEM SECTION CODE - END ===============*/ ?>
<?php /*============================================================================*/ ?>



<?php /*============================================================================*/ ?>
<?php /*=============== HAZARD LINKED LINE ITEM SECTION CODE - START =============*/ ?>
<?php /*============================================================================*/ ?>
<?php
$active_record = 2;
$object_type = 'hzd';

//GET EXISTING LINE INFORMATION
//Could be a call to a function in MODREF_OBJECT class
$sql = "SELECT * ";
$sql .= "FROM ".$_SESSION['dbref']."_risk_hazard_library_link_lines ";
$sql .= "INNER JOIN ".$_SESSION['dbref']."_hazard_library_link_lines ";
$sql .= "ON rhl_lne_id = lne_id ";
$sql .= "WHERE rhl_risk_id = $object_id ";
$sql .= "AND lne_object_type = '$object_type' ";
$sql .= "AND (lne_status & ".$active_record.") = ".$active_record;
$sql .= " ";
$sql .= "AND (rhl_status & ".$active_record.") = ".$active_record;
$risk_hazard_existing_lines = $helper->mysql_fetch_all($sql);

$hazard_object = new RGSTR2_HAZARD();
$hazard_list = $hazard_object->getList('manage', array());

$processed_line_items = array();
foreach($risk_hazard_existing_lines as $key => $val){
    $hazard_name = $hazard_list['rows'][$val['lne_objectid']]['haz_name']['display'];
    $src = $val['lne_srcmodref'];
    $line_id = $val['lne_id'];
    $src_id = $val['lne_srcid'];

    $processed_line_items[$hazard_name][$src][$line_id] = $src_id;
}

$existing_lines_details = array();
foreach($sources as $modref => $src) {
    foreach($processed_line_items as $hazard_name => $existing_lines) {
        if(isset($existing_lines[$modref])) {
            $existing_lines_details[$hazard_name][$modref] = $linkObject->getLinesByID($modref,$existing_lines[$modref]);
        } else {
            $existing_lines_details[$hazard_name][$modref] = array();
        }
    }
}

?>

<?php foreach($processed_line_items as $hazard_name => $existing_lines) { ?>
    <h2>Line Items LINKED TO <?php echo $hazard_name; ?></h2>
    <h3>Line Items</h3>
    <?php foreach($sources as $modref => $src) { ?>
        <?php if(isset($existing_lines[$modref]) && count($existing_lines[$modref])>0 && isset($existing_lines_details[$hazard_name][$modref]['rows']) && count($existing_lines_details[$hazard_name][$modref]['rows'])>0){ ?>
            <h3><?php echo $src; ?></h3>
            <table width=100%>
                <tr>
                    <th>Ref</th>
                    <?php foreach($existing_lines_details[$hazard_name][$modref]['head'] as $fld => $head) { ?>
                        <th><?php echo $head; ?></th>
                    <?php } ?>
                    <th width=50px></th>
                </tr>

                <?php foreach($existing_lines[$modref] as $record_id => $src_line_id) { ?>
                    <?php
                    $row = $existing_lines_details[$hazard_name][$modref]['rows'][$src_line_id];
                    $ref = $modref."/".$src_line_id;

                    //Preparation for the pop up dialogue
                    $mod_location = $sources_session_array[$modref]['modlocation'];
                    $lib_object_type = 'ACTIVITY';
                    $lib_object_id = $src_line_id;
                    ?>
                    <tr>
                        <td><?php echo $ref; ?></td>
                        <?php foreach($existing_lines_details[$hazard_name][$modref]['head'] as $fld => $head) { ?>
                            <td><?php echo $row[$fld]; ?> <?php echo ($fld == 'activity_name' ? '<a href="#" class="action_dlg" data-mod_location="' . $mod_location . '" data-mod_ref="' . $modref . '" data-lib_object_id="' . $lib_object_id . '" data-lib_object_type="' . $lib_object_type . '" data-request_scheme="' . $_SERVER['REQUEST_SCHEME'] . '" data-server_name="' . $_SERVER['SERVER_NAME'] . '">View</a>' : ''); ?></td>
                        <?php } ?>
                        <td class=center><button class=btn_delete record="<?php echo $record_id; ?>" ref="<?php echo $ref; ?>" data-object_id="<?php echo $object_id; ?>" data-link_type="hazard_linked">Delete</button></td>
                    </tr>
                <?php } ?>
            </table>
        <?php } ?>
    <?php } ?>
<?php } ?>
<?php /*============================================================================*/ ?>
<?php /*=============== HAZARD LINKED LINE ITEM SECTION CODE - END ===============*/ ?>
<?php /*============================================================================*/ ?>

<div id=dlg_action_details title="Select lines">
    <iframe id=ifr_action_details></iframe>
</div>
<script>
    $(function() {

        //Determine the necessary width and height settings for the dlg_action_details & ifr_lines elements
        var scr = AssistHelper.getWindowSize();
        var dlg_width = scr['width']*0.5;
        var dlg_height = scr['height']*0.95;
        var ifr_width = dlg_width-20;
        var ifr_height = dlg_height-100;

        /**
         * "Go" button - process the selected Source and Filter and display the available lines in the dlg_action_details dialog pop-up
         */
        $('a.action_dlg').button({
            icons:{primary:"ui-icon-arrow-4-diag"},
            text: false
        }).click(function(e) {
                e.preventDefault();
                AssistHelper.processing();
                $("#dlg_action_details").dialog("option","title","Action Details");

                var modlocation = $(this).data('mod_location');
                var modref = $(this).data('mod_ref');
                var object_type = $(this).data('lib_object_type');
                var object_id = $(this).data('lib_object_id');

                var request_scheme = $(this).data('request_scheme');
                var server_name = $(this).data('server_name');

                var url = request_scheme + "://" + server_name + "/" + modlocation + "/manage_view_dialog.php?display_type=external_dialog&display_parents=true&modref="+ modref.toLowerCase() +"&object_type="+ object_type + "&object_id=" + object_id;

                console.log('===== DIALOGUE URL =====');
                console.log(url);

                //call iframe in dialog
                $("#ifr_action_details").prop("src",url);
                //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
                $("#dlg_action_details").dialog("open");
            }).removeClass("ui-state-default").addClass("ui-state-black");;

        /**
         * Dialog to display available lines after clicking the "Go" button
         */
        $("#dlg_action_details").dialog({
            autoOpen: false,
            modal: true,
            width: dlg_width,
            height: dlg_height,
            buttons: [{
                text:"Close",
                icons: {primary: "ui-icon-close"},
                click:function(e) {
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_action_details").dialog("close");
                    AssistHelper.closeProcessing();
                }
            }]
        });
        AssistHelper.formatDialogButtonsByClass($("#dlg_action_details"),0,"ui-state-green");
        /**
         * Iframe within the dlg_action_details div
         */
        $("#ifr_action_details").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");


        $('#dlg_action_details').on('dialogclose', function(event) {
            AssistHelper.closeProcessing();
        });

        /******************
         * EXISTING LINES section
         */

    });
</script>
