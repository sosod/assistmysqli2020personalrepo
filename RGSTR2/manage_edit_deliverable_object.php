<?php
$_REQUEST['object_type'] = "DELIVERABLE";

$page_redirect_path = "manage_edit_deliverable.php?";
$page_action = "Edit";

include("common/edit_object.php");

?>

<?php
//Get the register id for this particular risk
$parent_object_id = $parent_id;

//Auto-populate Acceptable Risk Exposure
$object_details = $childObject->getObjectDetails();
$rsk_acceptable_risk_exposure = $object_details['rsk_acceptable_risk_exposure'];
?>

<script>
    $(function() {
        $("#rsk_type").change(function() {
            console.log($(this).val());

            var value = $(this).val();

            if(value == "SUB"){
                //Show the risks that can have sub risks

                var register_id = <?php echo $parent_object_id; ?>

                var dta = "register_id=" + register_id;

                var main_risk_table_row = AssistHelper.doAjax("inc_controller.php?action=RISK.GETMAINRISKTABLEROW",dta);

                $("#tr_rsk_type").after(main_risk_table_row);
            }else{
                //Remove the main risk table row if it exists
                $("#tr_rsk_parent_id").remove();
            }

        });

        $("#rsk_type").trigger('change');

        $("#rsk_acceptable_risk_exposure").val(<?php  echo $rsk_acceptable_risk_exposure; ?>);



    });
</script>

<?php if((int)$object_details['rsk_status_id'] != 1){ ?>
    <script>
        $(function() {
            $("#rsk_current_controls").prop('disabled', true);
        });
    </script>
<?php } ?>