<?php
$parent_object_type = "REGISTER";
$parent_object_id = $_REQUEST['object_id'];

$display_type = isset($_REQUEST['display_type']) ? $_REQUEST['display_type'] : "default";
if($display_type=="dialog") {
	$no_page_heading = true;
	$page_redirect_path = "dialog";
} else {
	$page_redirect_path = "new_deliverable_add_object.php?object_id=".$parent_object_id."&";
}


$page_action = "Add";

include("new_object.php");

 
?>

<script>
	$(function() {
	        $("#rsk_type").change(function() {
				console.log($(this).val());

				var value = $(this).val();

	            if(value == "SUB"){
					//Show the risks that can have sub risks

					var register_id = <?php echo $parent_object_id; ?>

					var dta = "register_id=" + register_id;

	                var main_risk_table_row = AssistHelper.doAjax("inc_controller.php?action=RISK.GETMAINRISKTABLEROW",dta);

	                $("#tr_rsk_type").after(main_risk_table_row);
	            }else{
					//Remove the main risk table row if it exists
					$("#tr_rsk_parent_id").remove();
				}

	        });

	    });
</script>

<!--<tr id="tr_rsk_parent_id" class="tr_rsk_type">-->
<!--	<th class="" width="40%">Main Risk:</th>-->
<!--	<td class="">-->
<!--		<select id="rsk_parent_id" name="rsk_parent_id" req="0">-->
<!--			<option value="0">[Unspecified]</option>-->
<!--			<option value="132" full="poiuytrew" list_num="0" >poiuytrew</option>-->
<!--		</select>-->
<!--	</td>-->
<!--</tr>-->

<script>
//    var currentScript = $('script').last();
//
//    $(function() {
//        $("#tr_rsk_use_king_iv .btn_yes").on( "click", function() {
//            if(!($('#principles').length > 0)){
//                //Get a table with the King IV principles
//                var dta = "";
//
//                var king_iv_table = AssistHelper.doAjax("inc_controller.php?action=KING_IV.GETKINGIVTABLE",dta);
//
//                var html = king_iv_table['html'];
//                var javascript = king_iv_table['javascript'];
//
//                $("#tr_rsk_use_king_iv").after(html);
//                currentScript.after(javascript);
//            }
//
//        });
//
//        $("#tr_rsk_use_king_iv .btn_no").on( "click", function() {
//            if($('#principles').length > 0){
//                //Remove the table row
//                $("#principles").remove();
//                $("#king_iv_script").remove();
//            }
//        });
//    });
</script>


<?php
//$kingiv_obj = new RGSTR2_KING_IV();
//$kingiv_obj->buildKingIVCheckboxedTableForRiskLink();
//
//
//$sql = 'SELECT * FROM ' .$_SESSION['dbref'].'_risk_king_iv_link ';
//$sql .= 'WHERE rkl_rsk_id = 141';
//$king_iv_principles_links = $helper->mysql_fetch_all_by_id($sql, 'rkl_kiv_id');
//
////echo '<pre style="font-size: 18px">';
////echo '<p>PRINTHERE</p>';
////print_r($king_iv_principles_links);
////echo '</pre>';
//
//
//$left_joins = array();
//$headObject = new RGSTR2_HEADINGS();
//$final_data = array('head'=>array(),'rows'=>array());
//
////set up variables
//$idp_headings = $headObject->getMainObjectHeadings("KING_IV","FULL","MANAGE");
//
//$idpObject = new RGSTR2_KING_IV();
//
//$idp_tblref = $idpObject->getMyObjectType();
//$idp_table = $idpObject->getTableName();
//$idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();
//
//$where = $idpObject->getActiveStatusSQL($idp_tblref);
//
//$id_fld = $idpObject->getIDFieldName();
//$sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
//$from = " $idp_table $idp_tblref
//					";
//$final_data['head'] = $idp_headings;
//$ref_tag = $idpObject->getRefTag();
//$group_by = "";
//
//$all_headings = array_merge($idp_headings);
//
//$listObject = new RGSTR2_LIST();
//
//foreach($all_headings as $fld => $head) {
//    $lj_tblref = $head['section'];
//    if($head['type']=="LIST") {
//        $listObject->changeListType($head['list_table']);
//        $sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
//        $left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])."
//										AS ".$head['list_table']."
//										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld."
//										AND (".$head['list_table'].".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
//    } elseif($head['type']=="MASTER") {
//        $tbl = $head['list_table'];
//        $masterObject = new RGSTR2_MASTER($fld);
//        $fy = $masterObject->getFields();
//        $sql.=", ".$fld.".".$fy['name']." as ".$tbl;
//        $left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
//    } elseif($head['type']=="USER") {
//        $sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
//        $left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
//    }
//}
//$sql.= " FROM ".$from.implode(" ",$left_joins);
//$sql.= " WHERE ".$where;
//
//$sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";
//$sql.=" ORDER BY " . $id_fld;
//$rows = $idpObject->mysql_fetch_all_by_id($sql,$id_fld);
//
//$final_data = $idpObject->formatKingIVRowDisplay($rows, $final_data,$id_fld,$headObject,$ref_tag,false);
//
//
//$final_data['head'] = $idpObject->replaceObjectNames($final_data['head']);
//
////echo '<pre style="font-size: 18px">';
////echo '<p>FINAL DATA</p>';
////print_r($final_data['head']);
////echo '</pre>';

?>

<!--<h2>King IV Principles Linked to RR141</h2>-->
<!--<table>-->
<!--    <tr>-->
<!--        <th>--><?php //echo $final_data['head']['kiv_id']['name']; ?><!--</th>-->
<!--        <th>--><?php //echo $final_data['head']['kiv_name']['name']; ?><!--</th>-->
<!--        <th>--><?php //echo $final_data['head']['kiv_description']['name']; ?><!--</th>-->
<!--        <th>Applied</th>-->
<!--        <th>Justification</th>-->
<!--    </tr>-->
<!--    --><?php //foreach($king_iv_principles_links as $key => $val){ ?>
<!--    <tr>-->
<!--        <td>--><?php //echo $final_data['rows'][$key]['kiv_id']['display']; ?><!--</td>-->
<!--        <td>--><?php //echo $final_data['rows'][$key]['kiv_name']['display']; ?><!--</td>-->
<!--        <td>--><?php //echo $final_data['rows'][$key]['kiv_description']['display']; ?><!--</td>-->
<!--        <td>--><?php //echo ($val['rkl_apply_kiv'] == '1' ? 'Yes' : 'No'); ?><!--</td>-->
<!--        <td>--><?php //echo $val['rkl_application_description']; ?><!--</td>-->
<!--    </tr>-->
<!--    --><?php //} ?>
<!--</table>-->
