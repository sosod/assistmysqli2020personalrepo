<?php
/**
 * To manage the ASSESSMENT function within the module
 * 
 * Created on: September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * SERIOUSLY UNDEVELOPED - in need of work
 * 		don't forget to update deliverable, scores, lists, settings in RGSTR2_LISTS class
 * 
 */

 
class RGSTR2_ASSESSMENT extends RGSTR2 {
	
	protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $progress_status_field = "_status_id";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $name_field = "_name";
	protected $deadline_field = "_deadline";
	
	protected $object_location = "OBJECT";
	
	 /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "DELIVERABLE";
    const TABLE = "assessment_results";
    const TABLE_FLD = "ar";
    const REFTAG = "CD";
	const LOG_TABLE = "deliverable";
	
	/********************************
	 * CONSTRUCTOR
	 */
	public function __construct($ol="OBJECT") {
		parent::__construct();
		$this->object_location=$ol;
		$this->status_field = $this->getTableField().$this->status_field;
		$this->progress_status_field = $this->getTableField().$this->progress_status_field;
		$this->id_field = $this->getTableField().$this->id_field;
		$this->parent_field = $this->getTableField().$this->parent_field;
		$this->name_field = $this->getTableField().$this->name_field;
		$this->deadline_field = $this->getTableField().$this->deadline_field;
	}
	
	
	
	/*********************************
	 * CONTROLLER functions
	 */
	
	
	/********************************
	 * GET functions
	 */
	 
	public function getNameField(){
    	return $this->getTableFieldName()."_name";
    }
    public function getTableFieldName(){
    	return $this->getTableField();
    }
	public function getTableField() {
		return self::TABLE_FLD; 
	}
	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE; 
	}
	/*******************************
	 * SET / UPDATE functions
	 */
	
	
	
	/********************************
	 * PROTECTED functions
	 */
	
	
	
	
	
	
	/********************************
	 * PRIVATE functions
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	/******************************
	 * DESTRUCT
	 */
	public function __destruct() {
		$this->db_close();
		parent::__desctruct();
	}
	
}
?>
