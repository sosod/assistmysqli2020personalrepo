<?php
/**
 * To manage the user access of the RGSTR2 module
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * 
 * 26 Feb 2015 [JC]: Create Deliverables option removed due to introduction of tree hierarchy in New.  Contract Manager will now be responsible for adding deliverables and actions.
 * 
 * 
 */

class RGSTR2_USERACCESS extends RGSTR2 {
	
	private $table_name = "_useraccess";
	
		//'ua_create_deliverable'	=> "Create |risk|",
	private $field_names = array(
		'ua_module'				=> "Module Admin",
		'ua_create_contract'	=> "Create |register|",
		'ua_update_all'			=> "Update All",
		'ua_edit_all'			=> "Edit All",
		'ua_report'				=> "Reports",
		'ua_limited'			=> "Limited View",
		'ua_assurance'			=> "Assurance Provider",
		'ua_contract'			=> "|register| Admin",
		'ua_hazard'			    => "Hazard Admin",
		'ua_setup'				=> "Setup",
	);
	

	private $field_defaults = array(
		'ua_module'				=> 0,
		'ua_create_contract'	=> 0,
		'ua_update_all'			=> 0,
		'ua_edit_all'			=> 0,
		'ua_report'				=> 1,
		'ua_limited'			=> 0,
		'ua_assurance'			=> 0,
		'ua_contract'			=> 0,
		'ua_hazard'			    => 0,
		'ua_setup'				=> 0,
	);

	public function __construct() {
		parent::__construct();
		$this->field_names = $this->replaceObjectNames($this->field_names);
		$this->table_name = $this->getDBRef().$this->table_name;
		//Module admin is depreciated
		unset($this->field_names['ua_module']);
		unset($this->field_defaults['ua_module']);
	}	
	
	/**
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['ua_status'])) { $var['ua_status'] = self::ACTIVE; }
		if(!isset($var['ua_insertuser'])) { $var['ua_insertuser'] = $this->getUserID(); }
		if(!isset($var['ua_insertdate'])) { $var['ua_insertdate'] = date("Y-m-d H:i:s"); }
		return $this->addUser($var);
		//return array("info","No changes were found to be saved.");
	}
	public function editObject($var){
		$id = $var['ua_id'];
		unset($var['ua_id']);
		return $this->editUser($id,$var);
	}
	
	
	
	/**
	 * GET functions
	 */
	public function getUserAccessFields() { return $this->field_names; }
	public function getUserAccessDefaults()	{ return $this->field_defaults; }
	public function getTableName() { return $this->table_name; }
	/**
	 * Get the list of users who have not yet had their user access defined.
	 */
	public function getUsersWithNoAccess() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				LEFT JOIN ".$this->getTableName()."
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND ua_status IS NULL 
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
				
	}
	/**
	 * Get the list of users with access
	 */
	public function getActiveUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, UA.*
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;	
	}
	public function getActiveUsersFormattedForSelect($ids="") {
		$rows = $this->getActiveUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	/**
	 * Get the current user's access
	 */
	public function getMyUserAccess($ui="") {
		$ui = strlen($ui)==0 ? $this->getUserID() : $ui;
		$sql = "SELECT UA.* FROM ".$this->getTableName()." UA WHERE ua_tkid = '".$ui."'";
		$row = $this->mysql_fetch_one($sql);
		if(!isset($row['ua_status'])) {
			$row = $this->field_defaults;
			$row['ua_status'] = 0;
			$row['ua_tkis'] = $ui;
			$row['ua_new'] = 0;
			$row['ua_admin'] = 0;
		} else {
			$row['ua_new'] = 0;
			$row['ua_admin'] = 0;
			if($row['ua_update_all']==1
				|| $row['ua_edit_all']==1
				|| $row['ua_contract']==1
				|| $row['ua_hazard']==1
			) {
				$row['ua_admin']=1;	
			}
			/*check user access to new*/
			//ua_create_contract = 1 OR
			if($row['ua_create_contract']==1) {
				$row['ua_new']=1;
			//ua_create_deliverable = 1 && count(admins::create_deliverable)>0 OR
			} else {
				if($row['ua_create_deliverable']==1) {
					$deptObject = new RGSTR2_CONTRACT_OWNER();
					if($deptObject->checkIfCanCreate()) {
						$row['ua_new'] = 1;
					}
				}
				//count(contract[manager])>0 OR count(contract[authoriser]>0)
				if($row['ua_new']!=1) {
					$contractObject = new RGSTR2_REGISTER();
					if($contractObject->checkIfIsAContractAuthoriser() || $contractObject->checkIfIsAContractManager()){
						$row['ua_new'] = 1;
					}
				}
			}
		}
		$user_access = array();
		foreach($row as $key => $val){
			$user_access[substr($key,3)] = $val;
		}
		return $user_access;
	}
	
	
	
	/**
	 * SET / UPDATE functions
	 */
	/**
	 * Adds a new user
	 */
	private function addUser($var){
		$result = array("info","No change was found to be saved.");
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0) {
			$result = array("ok","User added successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'	=> "Added user access for user: ".$this->getAUserName($var['ua_tkid']),
			);
			$log_var = array(
				'section'	=> "USER",
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);
		} else {
		/*
		 * */
			$result = array("error","Sorry, something went wrong while trying to add the user.  Please try again.");
		}
		return $result;
	}
	
	
	private function editUser($id,$var) {
		if(ASSIST_HELPER::checkIntRef($id)) {
			$old = $this->mysql_fetch_one("SELECT * FROM ".$this->getTableName()." WHERE ua_id = ".$id);
			//$edits = array();
			//foreach($var as $fld=>$v){
			//	$edits[substr($fld,5)] = $v;
			//}
			$edits = $var;
			$update_data = $this->convertArrayToSQL($edits);
			$sql = "UPDATE ".$this->getTableName()." SET ".$update_data." WHERE ua_id = ".$id;
			$mar = $this->db_update($sql);
			if($mar>0){
				$un = $this->getAUserName($old['ua_tkid']);
				//$user_access_fields = $this->getUserAccessFields();
				$changes = array(
					'user'=>$this->getUserName(),
					'response'	=> "Edited user access for user: ".$un,
				);
				foreach($old as $key=>$value){
					if(isset($edits[$key]) && $value != $edits[$key]){
						//$changes[$user_access_fields[$key]] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
						$changes[$key] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
					}
				}
							
				$log_var = array(
					'section'	=> "USER",
					'object_id'	=> $id,
					'changes'	=> $changes,
					'log_type'	=> RGSTR2_LOG::EDIT,
				);
				$this->addActivityLog("setup", $log_var);
				return array("ok","Changes to ".$un." have been saved successfully.");
			} else {
				return array("info","No change was found to be saved.");
			}
		}
		return array("error","An error occurred while trying to save the changes. Please try again.");
	}
	
	
	
	
	
	
	
	public function canICreateDeliverables($ui="") {
		$ua = $this->getMyUserAccess($ui);
		return ($ua['create_deliverable']==1);
	}
	
	
	
	
}



?>