<?php
/**
 * To manage the ASSURANCE function
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_ASSURANCE_DELIVERABLE extends RGSTR2_ASSURANCE {
    
    /*************
     * CONSTANTS
     */
    //const OBJECT_TYPE = "ASSURANCE";
    const TABLE = "deliverable_assurance";
    const TABLE_FLD = "da";
    const REFTAG = "CDAS";
	protected $object_id = 0;
	protected $object_details = array();
    
    //protected $progress_status_field = "_status_id";
	//protected $status_field = "_status";
	protected $id_field = "da_id";
	protected $parent_field = "da_deliverable_id";
	//protected $name_field = "_name";
	//protected $deadline_field = "_planned_date_completed";
	//protected $owner_field = "_owner_id";
	//protected $manager_field = "_manager";
	//protected $authoriser_field = "_authoriser";
	protected $attachment_field = "da_attachment";
	//protected $progress_field = "_progress";

	//const LOG_TABLE = "assurance";
	/**
	 * STATUS CONSTANTS
	 */
	//const CONFIRMED = 32;
	//const ACTIVATED = 64;
	/**
	 * ASSESSMENT TYPE CONSTANTS
	 */
	//const QUALITATIVE = 1024;
	//const QUANTITATIVE = 2048;
	//const OTHER = 4096;
    
    public function __construct($object_id=0) {
        parent::__construct();
		if($object_id>0) {
	    	$this->object_id = $object_id;
			$this->object_details = $this->getRawObject($object_id);  //$this->arrPrint($this->object_details);
		}
    }
    
	public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
    //public function getMyObjectType() { return self::OBJECT_TYPE; }
    //public function getMyLogTable() { return self::LOG_TABLE; }
	
	/*******************************
	 * CONTROLLER functions
	 */
	//public function addObject($var) { - parent class
	public function addObject($var,$fld="") {
		unset($var['attachments']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}else if($key == "_contract_id"){
				$newkey = $fld.$key;
				$var[$newkey]=$v;
				unset($var[$key]);
			}
		}
		unset($var['object_id']);
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_status'] = RGSTR2::ACTIVE;
		
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|assurance| added.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::CREATE,
				//'progress'	=> 0,
				//'status_id'	=> 1,		
			);
			/* moved to controller for attachment functionality */
			//$this->addActivityLog("contract", $log_var);
			$result = array(
				0=>"ok",
				1=>"New ".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully added.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}


	//public function editObject($var,$attach=array()) { - parent class
	

	
    /*************************************
     * GET functions
     * */
     public function getObject($id){
     	$id = $id['id'];
     	$sql = "SELECT * FROM ".$this->getTableName()." WHERE da_id = $id";
		return $this->mysql_fetch_one($sql);
     }
     
	 public function getAObject($id=0,$options=array()){
	 	//return $this->getMyList("ASSURANCE","MANAGE");
	 	$head = new RGSTR2_HEADINGS();
		$headarr = $head->getMainObjectHeadings("ASSURANCE","LIST");
		foreach($headarr as $key=>$val){
			foreach($val as $index=>$item){
				$data['head'][$index]=$item;
			}
		}		
		$rows = $this->getRawObject($id);
		//Sanitize rows data for display
		foreach($rows as $key=>$val){
			foreach($val as $index=>$item){
				if($index == "da_deliverable_id" ||$index == "da_status" ||$index == "da_insertuser" ||$index == "da_insertdate"){
					unset($rows[$key][$index]);
				}else if($index == "da_attachment" && strlen($item) > 0){
					$attach = unserialize($item);
					if($attach[0]['status'] == RGSTR2_LOG::ACTIVE){
						$attach = $attach[0];
						$link="<a href='../files/".$this->getCmpCode()."/CONTRACT/assurance/".$attach['system_filename']."'>".$attach['original_filename']."</a>";
						$rows[$key][$index]=$link;
					}
				}
			}
		}
		foreach($rows as $key=>$val){
			$rows[$val['da_id']]=$val;
			unset($rows[$key]);
		}
		$data['rows']=$rows;
		return $data;
	 	return $this->getDetailedObject("ASSURANCE", $id,$options);
	 }
	 
	 	public function getRawUpdateObject($id){
		return $this->getRawObject($id);
	}
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id="") {
		$tmp = is_numeric($obj_id)?" AND da_deliverable_id = ".$obj_id:"";
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE (da_status & ".RGSTR2_LOG::ACTIVE.")=".RGSTR2_LOG::ACTIVE.$tmp;
		$data = $this->mysql_fetch_all($sql);
		
		return $data;
	}
	
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}


?>