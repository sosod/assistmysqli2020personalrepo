<?php
/**
 * To manage the various list items
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_LIST extends RGSTR2 {
    
    private $my_list = "";
    private $list_table = "";
    private $fields = array();
    private $field_names = array();
    private $field_types = array();
    private $associatedObject;
    private $object_table = "";
    private $object_field = "";
	private $parent_field = "";
	private $sort_by = "sort, name";
	private $sort_by_array = array("sort","name");
	private $my_name = "name";
	private $sql_name = "|X|name";
    
    private $lists = array();   
    private $default_field_names = array(
        'id'=>"Ref",
        'name'=>"Name",
        'default_name'=>"Default Name",
        'client_name'=>"Your Name",
        'shortcode'=>"Short Code",
        'description'=>"Description",
        'status'=>"Status",
        'colour'=>"Colour",
        'rating'=>"Score",
        'rating_range'=>"Rating Range",
        'probability'=>"Probability",
        'list_num'=>"Risk Type",
    );
    private $default_field_types = array(
        'id'=>"REF",
        'default_name'=>"LRGVC",
        'client_name'=>"LRGVC",
        'name'=>"LRGVC",
        'shortcode'=>"SMLVC",
        'description'=>"TEXT",
        'status'=>"STATUS",
        'colour'=>"COLOUR",
        'rating'=>"RATING",
        'rating_range'=>"RATINGRANGE",
        'probability'=>"NUM",
        'list_num'=>"LIST",
    );
    private $required_fields = array(
        'id'=>false,
        'name'=>true,
        'default_name'=>false,
        'client_name'=>true,
        'shortcode'=>true,
        'description'=>false,
        'status'=>false,
        'colour'=>true,
        'rating'=>true,
        'rating_range'=>true,
        'probability'=>true,
        'list_num'=>true,
    );
    
    
/**    
 * @param {String} list     The list identifier
 */
    public function __construct($list="",$list_is_field_name=false){
        parent::__construct();
        if(strlen($list)>0) {
            if($list_is_field_name) {
                $headObject = new RGSTR2_HEADINGS();
                $list = $headObject->getAListTableFromFieldName($list);
            }
            $this->my_list = $list;
            $this->setListDetails();
        }
    }

	public function changeListType($list="") {
		if(strlen($list)>0) {
        	$this->my_list = $list;
        	$this->setListDetails();
		}
	}
        
	/**
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['status'])) { $var['status'] = self::ACTIVE; }
		return $this->addListItem($var);
	}
	public function editObject($var){
		$id = $var['id'];
		return $this->editListItem($id,$var);
	}
	public function deleteObject($var){
		$id = $var['id'];
		return $this->deleteListItem($id);
	}
	public function deactivateObject($var){
		$id = $var['id'];
		return $this->deactivateListItem($id);
	}
	public function restoreObject($var){
		$id = $var['id'];
		return $this->restoreListItem($id);
	}
	
	
	
    /***************************
     * GET functions
     **************************/
	public function getMyName(){
		return $this->my_name;
	}
	public function getSQLName($t="") {
		return str_ireplace("|X|", (strlen($t)>0?$t.".":""), $this->sql_name);
	}
	public function getListTable(){
		return $this->list_table;
	}
	public function getSortBy($as_array=true) {
		if($as_array) {
			return $this->sort_by_array;
		} else {
			return $this->sort_by;
		}
	}
	public function getAllListTables($lists=array()) {
		$data = array();
		foreach($lists as $l) {
        	$this->my_list = $l;
        	$this->setListDetails();
			$data[$l] = $this->getListTable();			
		}
		return $data;
	}
	/**
	 * Returns the field names for the given list
	 */
	public function getFieldNames() {
		return $this->field_names;
	}

    public function getDefaultFieldNames() {
        return $this->default_field_names;
    }
	/**
	 * Returns the required fields for the given list
	 */
	public function getRequredFields() {
		return $this->required_fields;
	}
	/**
	 * Returns the field names for the given list
	 */
	public function getFieldTypes() {
		return $this->field_types;
	}
    /**
     * Returns list items in an array with the id as key depending on the input options
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */ 
    public function getListItems($options="") {
        $sql = "SELECT * ";
		if(in_array("client_name",$this->fields)) {
			$sql.=", IF(LENGTH(client_name)>0,client_name,default_name) as name ";
		}

        if(in_array("rating_range",$this->fields)) {
            $sql.=", CONCAT(rating_from,' to ',rating_to) as rating_range ";
        }
        
        $sql.= " FROM ".$this->getListTable()." WHERE (status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED.(strlen($options)>0 ? " AND ".$options : "")." ORDER BY ".$this->sort_by;
        //echo $sql;
        return $this->mysql_fetch_all_by_id($sql, "id");
    }   
    /**
     * Returns all list items which have been used formatted for reporting purposes
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getItemsForReport($options="") {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")."(status & ".RGSTR2::DELETED." <> ".RGSTR2::DELETED." ) AND (id IN (SELECT DISTINCT ".$this->object_field." FROM ".$this->object_table."))";
        $items = $this->getListItems($options);
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i['name'];
		}
		return $data;
    } 

    public function getItemsInUse($parent_id=0) {
        $options="(status & ".RGSTR2::DELETED." <> ".RGSTR2::DELETED." ) 
        AND (id IN (SELECT DISTINCT ".$this->object_field." FROM ".$this->object_table.($parent_id > 0 ? " WHERE ".$this->parent_field." = ".$parent_id : "")."))";
		//echo $options;
        $items = $this->getListItems($options);
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i['name'];
		}
		return $data;
    } 
    /**
     * Returns all active list items in an array with the id as key
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItems($options="") {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")."(status & ".RGSTR2::ACTIVE." = ".RGSTR2::ACTIVE." )";
        return $this->getListItems($options);
    } 
    /**
     * Returns all active list items formatted for display in SELECT => array with the id as key and value as element
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItemsFormattedForSelect($options="") {
        $rows = $this->getActiveListItems($options);
		$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num",$this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
    } 
	
	
	public function getActiveListItemsNotInUseFormattedForSelect($parent_id,$is_edit_page=false,$edit_id="") {
		$items_in_use = $this->getItemsInUse($parent_id);  //echo $parent_id; $this->arrPrint($items_in_use);
		$active_items = $this->getActiveListItemsFormattedForSelect(); //$this->arrPrint($active_items);
		$data = $active_items['options'];  //$this->arrPrint($data);
		//array_diff($data, $items_in_use);
		foreach($items_in_use as $i => $v) {
			if(isset($data[$i]) && (!$is_edit_page || $i!=$edit_id)) {
				unset($data[$i]);
			}
		} 
		if($is_edit_page && strlen($edit_id)>0 && !isset($data[$edit_id])) {
			$data[$edit_id] = $items_in_use[$edit_id];
		}
		//$this->arrPrint($data);
		return $data;
		//return $active_items;
		//return $items_in_use;
	}
	
	
    /**
     * Returns all list items not deleted
     * @return (Array of Records)
     */
    public function getAllListItems() {
        return $this->getListItems();
    }
    /**
     * Returns a specific list item determined by the input id
     * @param (INT) id = The ID of the list item 
     * @return (One Record)
	 * OR
	 * @param (Array) id = array of IDs to be found
	 * @return (Array of Arrays) records found
     */    
    public function getAListItem($id) {
    	if(is_array($id)) {
        	$data = $this->getListItems("id IN (".implode(",",$id).")");
			return $data;
    	} else {
        	$data = $this->getListItems("id = ".$id);
	        return $data[$id];
        }
    }    
	/**
	 * returns the names of specific list items determined by the input id(s)
	 * @param (int) id = id of the list item to be found
	 * @return (String) name of list item
	 * OR
	 * @param (array) id = array of ids to be found
	 * @return (Array of Strings) names of the list items with the ID as index
	 */
	public function getAListItemName($id) {
    	$data = $this->getAListItem($id);
    	if(is_array($id)) {
    		$items = array();
			foreach($data as $i=>$d) {
				$items[$i] = $d['name'];
			}
			return $items;
    	} else {
	        return $data['name'];
        }
	}
	
    /**
     * Returns a list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
     * @param (INT) id = The ID of the list item
     * @return (One record)
     */
    public function getAListItemForSetup($id) {
        $data = $this->getAListItem($id);
        /*****
         * TO BE PROGRAMMED: 
         *     $sql = SELECT count(O.$this->object_field) as in_use 
         *      FROM $this->object_table O
         *      ON L.id = O.$this->object_field
         *      WHERE O.$this->object_field = $id
         *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
         * $row = $this->mysql_fetch_one_value($sql,"in_use")
         * parse results: if in_use > 0 then $data['can_delete'] = false else = true 
         * 
         */
         return $data;
    }

    /**
     * Returns  list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
     * @return (records)
     */
	public function getListItemsForSetup() {
		$data = $this->getListItems();
		/*****
		 * TO BE PROGRAMMED: 
		 *     $sql = SELECT count(O.$this->object_field) as in_use 
		 *      FROM $this->object_table O
		 *      ON L.id = O.$this->object_field
		 *      WHERE O.$this->object_field = $id
		 *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
		 * $row = $this->mysql_fetch_one_value($sql,"in_use")
		 * parse results: if in_use > 0 then $data['can_delete'] = false else = true 
		 * 
		 */
		if(count($data)>0) {
			$keys = array_keys($data);
			if(is_array($this->object_field)) {
				$u = array();
				$used = array();
				foreach($this->object_field as $k => $of) {
					$ot = $this->object_table[$k];
					$sql = "SELECT ".$of." as fld, count(".$of.") as in_use FROM ".$ot." WHERE ".$of." IN (".implode(",",$keys).") GROUP BY ".$of;
					$u[] = $this->mysql_fetch_all_by_id($sql, "fld");
					foreach($u as $f => $s) {
						$s['in_use'] = isset($s['in_use']) ? $s['in_use'] : 0;
						if(isset($used[$f])) {
							$used[$f]['in_use']+= $s['in_use'];
						} else {
							$used[$f] = $s;
						}
					}
				}
			} else {
				$sql = "SELECT ".$this->object_field." as fld, count(".$this->object_field.") as in_use FROM ".$this->object_table." WHERE ".$this->object_field." IN (".implode(",",$keys).") GROUP BY ".$this->object_field;
				$used = $this->mysql_fetch_all_by_id($sql, "fld");
			}
			foreach($data as $key=>$item) {
				if(!isset($used[$key]) || $used[$key]['in_use']==0) {
					$data[$key]['can_delete'] = true;
				} else {
					$data[$key]['can_delete'] = false;
				}
			}
		}
		return $data;
	}




    /****************************
     * SET/UPDATE functions
     *****************************/
    /**
     * Create a new list item
     * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
     * @return TBD (INT) id of new value? or true/false to indicate success or failure
     */
    public function addListItem($var) {
        $insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getListTable()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0){
			$result = array("ok","List item added successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Added a new item " . $id . ": " . $var[$this->getMyName()],
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::CREATE,
			);
			$this->addActivityLog("setup", $log_var);
		} else {
			$result = array("error","Sorry, something went wrong while trying to add the list item. Please try again.");
		}
		return $result;
    }
    /**
     * Edit a list item details
     * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
     * @return TBD (INT) id of new value? or true/false to indicate success or failure
     */
    public function editListItem($id,$var) {
        $old_sql = "SELECT * FROM " . $this->getListTable() . " WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);
        $insert_data = $this->convertArrayToSQL($var);
		$sql = "UPDATE ".$this->getListTable()." SET ".$insert_data . " WHERE id = $id";
		$mar = $this->db_update($sql);
		if($mar>0){
			$result = array("ok","List item modified successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " modified.",
			);
			foreach($old_result as $key=>$value){
				if(isset($var[$key]) && $value != $var[$key]){
					if($key=="list_num") {
						$listObject2 = new RGSTR2_LIST("risk_types");
						$item_names = $listObject2->getAListItemName(array($var[$key],$value));
						$item_names[0] = $this->getUnspecified();
						$changes[$key] =array('to'=>$item_names[$var[$key]], 'from'=>$item_names[$value]);
					} else {
						$changes[$key] =array('to'=>$var[$key], 'from'=>$value);
					}
				}
			}
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to change the list item. Please try again.");
		}
		return $result;
    }
    /**
     * Delete a list item by removing the ACTIVE status and adding a DELETED status - the item should no longer show anywhere within the module
     * @param (INT) id = id of list item to be deleted
     * @return (BOOL) status of success as true/false
     */
    public function deleteListItem($id) {
        /****
         * TO BE PROGRAMMED: UPDATE ... SET status = (status - ACTIVE + DELETED) WHERE id = $id
         */
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".RGSTR2::ACTIVE." + ".RGSTR2::DELETED.") WHERE id = $id";
		$mar = $this->db_update($sql);
		
		if($mar>0){
			$result = array("ok","List item removed successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " deleted.",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::DELETE,
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to remove the list item. Please try again.");
		}
		
		return $result;
    }
    
    /**
     * Deactivate a list item by removing the ACTIVE status and adding INACTIVE status - the item should no longer be availavble in new or edit
     * @param (INT) id = id of list item to be deactivated
     * @return (BOOL) status of success as true/false
     */
    public function deactivateListItem($id) {
        /****
         * TO BE PROGRAMMED: UPDATE ... SET status = (status - ACTIVE + INACTIVE) WHERE id = $id
         */
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".RGSTR2::ACTIVE." + ".RGSTR2::INACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);
		
		if($mar>0){
			$result = array("ok","List item deactivated successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " deactivated.",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::DEACTIVATE,
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to deactivate the list item. Please try again.");
		}
		
		return $result;
    }
    /**
     * Restore a deactivated list item by removing the INACTIVE status and adding ACTIVE - the list item should be available for use throughout the module
     * @param (INT) id = id of list item to be restored
     * @return (BOOL) status of success as true/false
     */
    public function restoreListItem($id) {
		
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".RGSTR2::INACTIVE." + ".RGSTR2::ACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);
		
		if($mar>0){
			$result = array("ok","List item restored successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " restored.",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::RESTORE,
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to restore the list item. Please try again.");
		}
		
		return $result;
		 
    }
        
        
        
        
        
        
        
    /********************************
     * Protected functions: functions available for use in class heirarchy
     ********************************/
     
     
     
     
     
     
     
         
    /****************************
     * Private functions: functions only for use within the class
     *********************/
     
    /**
     * Sets the various variables needed by the class depending on the list name provided
     */
    private function setListDetails() {
		$this->sort_by = "sort, name";
		$this->sort_by_array = array("sort","name");
		$this->my_name = "name";
		$this->sql_name = "|X|name";
        switch($this->my_list) {
            case "contract_type": 
                $this->list_table = $this->getDBRef()."_list_contract_type"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_REGISTER();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_type_id"; 
                break; 
                
            case "contract_category": 
                $this->list_table = $this->getDBRef()."_list_contract_category"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_REGISTER();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_category_id"; 
                break; 
        
            case "contract_status": 
                $this->list_table = $this->getDBRef()."_list_contract_status"; 
				$this->sort_by = "sort, if(length(client_name) > 0, client_name, default_name)";
				$this->sort_by_array = array("sort","if(length(|X|.client_name) > 0, |X|.client_name, |X|.default_name)");
				$this->my_name = "client_name";
                $this->fields = array(
                    "id",
                    "default_name",
                    "client_name",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_REGISTER();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_status_id"; 
                break; 
        
            case "contract_supplier": 
                $this->list_table = $this->getDBRef()."_list_contract_supplier"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "list_num",
                    "status",
                );
                $this->associatedObject = new RGSTR2_FINANCE_BUDGET();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_supplier_id";
				$this->parent_field = $this->associatedObject->getParentFieldName(); 
                break; 
        
            case "contract_supplier_category": 
                $this->list_table = $this->getDBRef()."_list_contract_supplier_category"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
				$this->object_table = $this->getDBRef()."_list_contract_supplier";
				$this->object_field = "list_num";
                break; 
        
            case "finance_payment_term": 
                $this->list_table = $this->getDBRef()."_list_finance_payment_term"; 
                $this->fields = array(
                    "id",
                    "name",
                    "status",
                );
                $this->associatedObject = new RGSTR2_FINANCE_BUDGET();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_payment_term_id"; 
                break; 
        
            case "finance_payment_frequency": 
                $this->list_table = $this->getDBRef()."_list_finance_payment_frequency"; 
                $this->fields = array(
                    "id",
                    "name",
                    "status",
                );
                $this->associatedObject = new RGSTR2_FINANCE_BUDGET();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_payment_frequency_id"; 
                break; 
        
            case "deliverable_category": 
                $this->list_table = $this->getDBRef()."_list_deliverable_category"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_category_id"; 
                break; 
        
            case "deliverable_quantity_weight": 
            case "deliverable_quality_weight": 
            case "deliverable_other_weight": 
                $this->list_table = "assist_".$this->getCmpCode()."_master_weights"; 
				$this->sort_by = "rating, name";
				$this->sort_by_array = array("rating","name");
                $this->fields = array(
                    "id",
                    "rating",
                    "name",
                    "colour",
                    "status",
                );
                $f = explode("_",$this->my_list);
				$f = $f[1];
				switch($f) {
					case "quantity": $f = "quant"; break;
					case "quality": $f = "qual"; break;
				}
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_".$f."_id"; 
                break;
            case "deliverable_quantity_score": 
                $this->list_table = $this->getDBRef()."_list_deliverable_quantity_score"; 
				$this->sort_by="sort, rating, name";
				$this->sort_by_array=array("sort", "rating", "name");
                $this->fields = array(
                    "id",
                    "rating",
                    "name",
                    "description",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_ASSESSMENT();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_quant_score"; 
                break; 
        
            case "deliverable_quality_score": 
                $this->list_table = $this->getDBRef()."_list_deliverable_quality_score"; 
				$this->sort_by="sort, rating, name";
				$this->sort_by_array=array("sort", "rating", "name");
                $this->fields = array(
                    "id",
                    "rating",
                    "name",
                    "description",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_ASSESSMENT();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_qual_score"; 
                break; 
        
            case "deliverable_other_score": 
                $this->list_table = $this->getDBRef()."_list_deliverable_other_score"; 
				$this->sort_by="sort, rating, name";
				$this->sort_by_array=array("sort", "rating", "name");
                $this->fields = array(
                    "id",
                    "rating",
                    "name",
                    "description",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_ASSESSMENT();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_other_score"; 
                break;
            case "action_status": 
                $this->list_table = $this->getDBRef()."_list_action_status"; 
				$this->sort_by = "sort, if(length(client_name) > 0, client_name, default_name)";
				$this->sort_by_array = array("sort","if(length(|X|.client_name) > 0, |X|.client_name, |X|.default_name)");
				$this->my_name = "client_name";
                $this->fields = array(
                    "id",
                    "default_name",
                    "client_name",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_ACTION();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_status_id"; 
                break; 
			case "del_status":
            case "deliverable_status": 
                $this->list_table = $this->getDBRef()."_list_deliverable_status"; 
				$this->sort_by = "sort, if(length(client_name) > 0, client_name, default_name)";
				$this->sort_by_array = array("sort","if(length(|X|.client_name) > 0, |X|.client_name, |X|.default_name)");
				$this->my_name = "client_name";
                $this->fields = array(
                    "id",
                    "default_name",
                    "client_name",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_status_id"; 
                break; 
			case "del_legislation":
            case "deliverable_legislation": 
                $this->list_table = $this->getDBRef()."_list_deliverable_legislation"; 
				$this->sort_by = "sort, name, shortcode";
				$this->sort_by_array = array("sort", "name", "shortcode");
				$this->my_name = "name";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_legislation_id"; 
                break; 
            case "del_legislation_section": 
            case "deliverable_legislation_section": 
                $this->list_table = $this->getDBRef()."_list_deliverable_legislation_section"; 
				$this->sort_by = "sort, shortcode, name";
				$this->sort_by_array = array("sort", "shortcode", "name");
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "list_num",
                    "status",
                );
                $this->default_field_names['list_num'] = "Compliance Legislation";
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_legislation_section_id"; 
                break; 
            case "finance_funding_type": 
            case "funding_type": 
                $this->list_table = $this->getDBRef()."_list_finance_funding_type"; 
                $this->fields = array(
                    "id",
                    "name",
                    "status",
                );
                $this->associatedObject = array();
                $this->object_table = array(); 
                $this->object_field = array(); 
                $aO = new RGSTR2_FINANCE_INCOME();
                $this->associatedObject[] = $aO;
                $this->object_table[] = $aO->getTableName(); 
                $this->object_field[] = $aO->getTableField()."_funding_type_id";
                unset($aO); 
                $aO = new RGSTR2_FINANCE_EXPENSE();
                $this->associatedObject[] = $aO;
                $this->object_table[] = $aO->getTableName(); 
                $this->object_field[] = $aO->getTableField()."_funding_type_id"; 
                break; 
            case "finance_funding_source": 
            case "funding_source": 
                $this->list_table = $this->getDBRef()."_list_finance_funding_source"; 
                $this->fields = array(
                    "id",
                    "name",
                    "status",
                );
                $this->associatedObject = array();
                $this->object_table = array(); 
                $this->object_field = array(); 
                $aO = new RGSTR2_FINANCE_INCOME();
                $this->associatedObject[] = $aO;
                $this->object_table[] = $aO->getTableName(); 
                $this->object_field[] = $aO->getTableField()."_funding_src_id";
                unset($aO); 
                $aO = new RGSTR2_FINANCE_EXPENSE();
                $this->associatedObject[] = $aO;
                $this->object_table[] = $aO->getTableName(); 
                $this->object_field[] = $aO->getTableField()."_funding_src_id"; 
                break;
            /*
             * *************************************************************************
             *                      TSHEGO ADDED THE FOLLOWING
             * **************************************************************************/
            case "risk_categories":
                $this->list_table = $this->getDBRef()."_list_risk_categories";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "list_num",
                    "status",
                );
                $this->default_field_names['list_num'] = "Risk Type";
//                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = /*$this->associatedObject->getTableName();*/ $this->getDBRef()."_list_risk_categories";;
                $this->object_field = /*$this->associatedObject->getTableField()."id"*/ 'id';
                break;
            case "risk_level":
                $this->list_table = $this->getDBRef()."_list_risk_level";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_types":
                $this->list_table = $this->getDBRef()."_list_risk_types";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_financial_exposure":
                $this->list_table = $this->getDBRef()."_list_risk_financial_exposure";
                $this->fields = array(
                    "id",
                    "rating_range",
                    "name",
                    "description",
                    "colour",
                    "status",
                );

                $this->default_field_names['name'] = "Magnitude";
                $this->default_field_names['description'] = "Response";

                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_impact":
                $this->sort_by = "rating_from, rating_to, name";
                $this->list_table = $this->getDBRef()."_list_risk_impact";
                $this->fields = array(
                    "id",
                    "rating_range",
                    "name",
                    "description",
                    "colour",
                    "status",
                );

                $this->default_field_names['name'] = "Assessment";
                $this->default_field_names['description'] = "Definition";

                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_likelihood":
                $this->sort_by = "rating_from, rating_to, name";
                $this->list_table = $this->getDBRef()."_list_risk_likelihood";
                $this->fields = array(
                    "id",
                    "rating_range",
                    "name",
                    "description",
                    "probability",
                    "colour",
                    "status",
                );

                $this->default_field_names['name'] = "Assessment";
                $this->default_field_names['description'] = "Definition";

                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_inherent_exposure":
                $this->sort_by = "rating_from, rating_to, name";
                $this->list_table = $this->getDBRef()."_list_risk_inherent_exposure";
                $this->fields = array(
                    "id",
                    "rating_range",
                    "name",
                    "description",
                    "colour",
                    "status",
                );

                $this->default_field_names['name'] = "Magnitude";
                $this->default_field_names['description'] = "Response";

                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_control_effectiveness":
                $this->sort_by = "rating, name";
                $this->list_table = $this->getDBRef()."_list_risk_control_effectiveness";
                $this->fields = array(
                    "id",
                    "name",
                    "description",
                    "rating",
                    "colour",
                    "status",
                );

                $this->default_field_names['name'] = "Effectiveness";
                $this->default_field_names['description'] = "Qualification Criteria";

                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->associatedObject->getTableName();
                $this->object_field = $this->associatedObject->getTableField()."_category_id";
                break;
            case "risk_residual_exposure":
                $this->sort_by = "rating_from, rating_to, name";
                $this->list_table = $this->getDBRef()."_list_risk_residual_exposure";
                $this->fields = array(
                    "id",
                    "rating_range",
                    "name",
                    "description",
                    "colour",
                    "status",
                );

                $this->default_field_names['name'] = "Magnitude";
                $this->default_field_names['description'] = "Response";

                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_status":
                $this->list_table = $this->getDBRef()."_list_risk_status";
                $this->fields = array(
                    "id",
                    "name",
                    "description",
                    "colour",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_kpa":
                $this->list_table = $this->getDBRef()."_list_risk_kpa";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_business_activities":
                $this->list_table = $this->getDBRef()."_list_risk_business_activities";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_hazards":
                $this->list_table = $this->getDBRef()."_list_risk_hazards";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_legislation":
                $this->list_table = $this->getDBRef()."_list_risk_legislation";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_legislation_section":
                //============================
                $this->list_table = $this->getDBRef()."_list_risk_legislation_section";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "list_num",
                    "status",
                );
                $this->default_field_names['list_num'] = "Compliance Legislation";
//                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->getDBRef()."_list_risk_legislation_section";;
                $this->object_field = 'id';
                break;
            case "proficiency":
                $this->list_table = $this->getDBRef()."_list_proficiency";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_RISK();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "hazard_type":
                $this->list_table = $this->getDBRef()."_list_hazard_type";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_HAZARD();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "risk_operational_condition":
                $this->list_table = $this->getDBRef()."_list_risk_operational_condition";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_HAZARD();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
            case "hazard_category":
                $this->list_table = $this->getDBRef()."_list_hazard_category";
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                $this->associatedObject = new RGSTR2_HAZARD();
                $this->object_table = $this->list_table;
                $this->object_field = 'id';
                break;
        }
		if($this->my_name!="name") { $this->sql_name = " if(length(|X|client_name) > 0, |X|client_name, |X|default_name) "; } else {}
		foreach($this->fields as $key){
			if(!isset($this->field_names[$key])) {
				$this->field_names[$key] = $this->default_field_names[$key];
			}
			if(!isset($this->field_types[$key])) {
				$this->field_types[$key] = $this->default_field_types[$key];
			}
		}
    }
    
} 

?>