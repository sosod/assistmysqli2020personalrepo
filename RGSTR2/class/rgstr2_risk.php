<?php
/**
 * To manage the DELIVERABLE object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_RISK extends RGSTR2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $progress_status_field = "_status_id";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $name_field = "_name";
	protected $deadline_field = "_deadline";
	protected $owner_field = "_owner";
	protected $progress_field = "_progress";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	
	protected $summary_heading_fields = array(
		"rsk_id",
		"rsk_name",
		"rsk_owner",
	);

	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "RISK";
    const TABLE = "risk";
    const TABLE_FLD = "rsk";
    const REFTAG = "RR";
	const LOG_TABLE = "risk";
    
    public function __construct($del_id = 0) {
        parent::__construct();
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		
		$this->object_form_extra_js = "
	
	var " .  $this->getTableField()  . "_legislation_section_value = $('#" .  $this->getTableField()  . "_legislation_section_id').val(); 
	//console.log('" .  $this->getTableField()  . "_legislation_section_value = '+" .  $this->getTableField()  . "_legislation_section_value);
	var " .  $this->getTableField()  . "_legislation_section_options = [];
	$('#" .  $this->getTableField()  . "_legislation_section_id option:gt(0)').each(function() {
		" .  $this->getTableField()  . "_legislation_section_options.push({value:$(this).val(), text:$(this).text(), list_num: $(this).attr('list_num'), full: $(this).attr('full')});
	});
	$('#" .  $this->getTableField()  . "_legislation_id').change(function() {
		if($(this).val()=='X' || $(this).val()=='0') {
			$('#" .  $this->getTableField()  . "_legislation_section_id option:gt(0)').remove();
			$('#" .  $this->getTableField()  . "_legislation_section_id').val($('#" .  $this->getTableField()  . "_legislation_section_id option:first').val());
		} else {
			var v = $(this).val();
			$('#" .  $this->getTableField()  . "_legislation_section_id option:gt(0)').remove();
			$.each(" .  $this->getTableField()  . "_legislation_section_options, function(i){
				var opt = " .  $this->getTableField()  . "_legislation_section_options[i];
				if(v==opt.list_num) {
					$('#" .  $this->getTableField()  . "_legislation_section_id').append(
						$('<option />',{val:opt.value, text:opt.text, full:opt.full})
					);
				}
			});
			$('#" .  $this->getTableField()  . "_legislation_section_id').val($('#" .  $this->getTableField()  . "_legislation_section_id option:first').val());
		}
		$('#" .  $this->getTableField()  . "_legislation_section_id').trigger('change');
	});
	//$('#" .  $this->getTableField()  . "_legislation_id').trigger('change');
	//console.log('after change '+$('#" .  $this->getTableField()  . "_legislation_section_id').val());
	//$('#" .  $this->getTableField()  . "_legislation_section_id').val(" .  $this->getTableField()  . "_legislation_section_value);
	//console.log('after reset '+$('#" .  $this->getTableField()  . "_legislation_section_id').val());
	//$('#" .  $this->getTableField()  . "_legislation_section_id').val(" .  $this->getTableField()  . "_legislation_section_value);
	
//variable to remember the previous progress value when defaulting to 100
var old_progress = $('#".$this->getProgressFieldName()."').val();
 
//on change of the status drop down
$('#".$this->getProgressStatusFieldName()."').change(function() {
                //get the selected option value
                var v = ($(this).val())*1;
                //if it is set to \"Completed\" (always id=3 except deliverable which is 5)
                if(v==5) {
                                //if the progress field is not already set to 100, then remember the current value
                                if(($('#".$this->getProgressFieldName()."').val())*1!=100) {
                                                old_progress = $('#".$this->getProgressFieldName()."').val();
                                }
                                //update progress field to 100 and disable
                                $('#".$this->getProgressFieldName()."').val('100').prop('disabled',true);
                //else if the status field is not Completed, check if the progress field was previously disabled
                } else if($('#".$this->getProgressFieldName()."').prop('disabled')==true) {
                                //cancel the disable and reset the value back to the last remembered value
                                $('#".$this->getProgressFieldName()."').val(old_progress).prop('disabled',false);
                }
});
	//console.log('after status function '+$('#" .  $this->getTableField()  . "_legislation_section_id').val());
	";
		
		
		if($del_id>0) {
			$this->object_id = $del_id;
			$this->object_details = $this->getRawObject($del_id);
		}
    }
    
    
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['deliverable_id']); //remove incorrect deliverable_id value from add form
		unset($var['last_deliverable_status']); //remove incorrect deliverable_id value from add form
		unset($var['object_id']);
		$contract_id = $var['rsk_contract_id'];
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_progress'] = 0;
		$var[$this->getTableField().'_status_id'] = 1;
		$var[$this->getTableField().'_status'] = RGSTR2::ACTIVE;

        //DO ANY ADDITIONAL PROCESSING HERE!!!!

        //just make it work - Remove when done
        $var['rsk_attachment'] = '';
        $var['rsk_update_attachment'] = '';

        // All the following requires that I do some kind of math to input into the table
        $var['rsk_category_id'] = 1*$var['rsk_category_id'];

        $impact_rating = 1*$var['rsk_impact_rating'];
        $likelihood_rating = 1*$var['rsk_likelihood_rating'];
        $control_effectiveness_rating = 1*$var['rsk_control_effectiveness_rating'];

        /*
         * ****************************************************************************************
         * **************** INHERENT RISK EXPOSURE CALCULATION AND CONNECTION - START *************
         * ****************************************************************************************
         * */
        $var['rsk_inherent_exposure'] = 5;//This requires dynamic functionality

        //Let's start by calculating the actual values we captured in the form
        $inherent_exposure_rating = $impact_rating * $likelihood_rating;
        $var['rsk_inherent_exposure_rating'] = $inherent_exposure_rating;

        //And then get on with connecting those values to the respective exposure list information

        $listObject = new RGSTR2_LIST('risk_inherent_exposure');
        //Get all the inherent risk exposure list items
        $inherent_exposure_items = $listObject->getActiveListItems();

        $inherent_exposure_id = 0;

        //Foreach through them...
        foreach($inherent_exposure_items as $key => $val){
            //... get the rating_from and rating_to values for this record...
            $from = 1*$val['rating_from'];
            $to = 1*$val['rating_to'];
            //... Create an array with all the numbers in between these values...
            $range_value_array = range($from, $to);

            //... Check if the rating that we captured is in that range...
            if(in_array($inherent_exposure_rating, $range_value_array)){
                //... If it is, then assign the id of this record to the $inherent_exposure_id, which will be used as the FK in the Risk table...
                $inherent_exposure_id = 1*$val['id'];
                //... Break out of this loop, because we found what we're looking for...
                break;
            }
        }
        //... Then make the FK assignment
        $var['rsk_inherent_exposure'] = $inherent_exposure_id;
        //... AND LIFE IS A JOY!!!
        /*
         * ****************************************************************************************
         * **************** INHERENT RISK EXPOSURE CALCULATION AND CONNECTION - END *************
         * ****************************************************************************************
         * */



        /*
         * ****************************************************************************************
         * **************** RESIDUAL RISK EXPOSURE CALCULATION AND CONNECTION - START *************
         * ****************************************************************************************
         * */
        //Let's start by calculating the actual values we captured in the form

        $residual_exposure_rating = $inherent_exposure_rating * $control_effectiveness_rating;
        //Get a round figure, then assign it to the field in the risk table
        $residual_exposure_rating = 1*$residual_exposure_rating;
        $var['rsk_residual_exposure_rating'] = $residual_exposure_rating;

        //And then get on with connecting those values to the respective exposure list information

        //Get all the inherent risk exposure list items
        $listObject->changeListType('risk_residual_exposure');
        $residual_exposure_items = $listObject->getActiveListItems();

        $residual_exposure_id = 0;

        //Foreach through them...
        foreach($residual_exposure_items as $key => $val){
            //... get the rating_from and rating_to values for this record...
            $from = 1*$val['rating_from'];
            $to = 1*$val['rating_to'];
            //... Create an array with all the numbers in between these values...
            $range_value_array = range($from, $to);

            //... Check if the rating that we captured is in that range...
            if($residual_exposure_rating >= $from && $residual_exposure_rating <= $to){
                //... If it is, then assign the id of this record to the $residual_exposure_id, which will be used as the FK in the Risk table...
                $residual_exposure_id = 1*$val['id'];
                //... Break out of this loop, because we found what we're looking for...
                break;
            }
        }
        //... Then make the FK assignment
        $var['rsk_residual_exposure'] = $residual_exposure_id;
        //... AND LIFE IS A JOY!!!
        /*
         * ****************************************************************************************
         * **************** RESIDUAL RISK EXPOSURE CALCULATION AND CONNECTION - END *************
         * ****************************************************************************************
         * */

        //Acceptable risk check
        $var['rsk_acceptable_risk_exposure'] = 1*$var['rsk_acceptable_risk_exposure'];

        $var['rsk_is_risk_acceptable'] = 0;
        if($var['rsk_acceptable_risk_exposure'] >= $residual_exposure_rating){
            $var['rsk_is_risk_acceptable'] = 1;
        }

        //====== Dummy Code for the King IV Stuff - START

        if($var['rsk_use_king_iv'] == "1"){
            //Put all the King IV Values into an array
            $king_iv_principles_use_array = array();
            foreach($var as $key => $val){
                $key_string_array = explode('_', $key);
                if($key_string_array[0] == 'use'){
                    $king_iv_principles_use_array[$key_string_array[3]]['use'] = $val;
                    unset($var[$key]);
                }

                if($key_string_array[0] == 'king'){
                    $king_iv_principles_use_array[$key_string_array[3]]['justification'] = $val;

                    unset($var[$key]);
                }
            }
        }

//        $result = array(
//            0=>"error",
//            1=>"This is only a test",
//            'Passed in'=>$var,
//            'Processed King IV'=>$king_iv_principles_use_array,
//        );
//        return $result;
        //====== Dummy Code for the King IV Stuff - END

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {

		    //if there are king iv fields to be linked, process the linking here
            if(isset($king_iv_principles_use_array) && is_array($king_iv_principles_use_array) && count($king_iv_principles_use_array) > 0){
                $rsk_id = $id;
                $iteration = 1;

                $sql = "INSERT INTO ".$_SESSION['dbref']."_risk_king_iv_link";
                $sql .= "(rkl_rsk_id";
                $sql .= ", rkl_kiv_id";
                $sql .= ", rkl_apply_kiv";
                $sql .= ", rkl_application_description)";
                $sql .= " VALUES ";
                foreach($king_iv_principles_use_array as $principle_id => $application_fields){
                    $sql .= "(" . $rsk_id;
                    $sql .= ", " . $principle_id;
                    $sql .= ", " . $application_fields['use'];
                    $sql .= ", '" . $application_fields['justification'] . "')";
                    $sql .= ($iteration < count($king_iv_principles_use_array) ? ", " : "; " );
                    $iteration++;
                }

                $affected_rows = $this->db_query($sql);

            }

			$changes = array(
				'response'=>"|risk| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::CREATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$registerObj = new RGSTR2_REGISTER(); //AA-719 RGSR 2 Automated Emails Cancelation to Assignees before Register Activation [SD] 03/10/2021
			$sql = "SELECT ".$registerObj->getStatusFieldName()." as status FROM ".$registerObj->getTableName()." WHERE ".$registerObj->getIDFieldName()." = ".$contract_id;
			$old = $this->mysql_fetch_one($sql);
			$old_status = $old['status'];
			if($old_status == (RGSTR2_REGISTER::ACTIVE + RGSTR2_REGISTER::CONFIRMED + RGSTR2_REGISTER::ACTIVATED)) {
				$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			}

			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}

	public function updateObject($var, $attach = array()) {
		$object_id = $var['object_id'];
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$status_id = 1*$var[$si_field];
        $c = array();

		$old_data = $this->getRawObject((string)$object_id);
		$contract_id = $old_data['rsk_contract_id'];

		$update_data = array(
			$si_field=>$status_id,
		);
		//if completed then send for approval
		if($status_id==5) {
			$new_status = $old_data[$s_field] + RGSTR2::AWAITING_APPROVAL;
			$update_data[$s_field] = $new_status;
		} else {
			$new_status = $old_data[$s_field];
		}


        /*
         * *************************************************************************************************************
         * **************** NEW RISK UPDATE CODE WITH ALL THE NEW UPDATE-ABLE FIELDS FROM THE EDIT - START *************
         * *************************************************************************************************************
         * */
        $impact_field = 'rsk_impact';
        $impact_rating_field = 'rsk_impact_rating';

        $likelihood_field = 'rsk_likelihood';
        $likelihood_rating_field = 'rsk_likelihood_rating';

        $control_effectiveness_field = 'rsk_control_effectiveness';
        $control_effectiveness_rating_field = 'rsk_control_effectiveness_rating';

        $impact = $var[$impact_field];
        $likelihood = $var[$likelihood_field];
        $control_effectiveness = $var[$control_effectiveness_field];

        $impact_rating = 1*$var[$impact_rating_field];
        $likelihood_rating = 1*$var[$likelihood_rating_field];
        $control_effectiveness_rating = 1*$var[$control_effectiveness_rating_field];

        if($old_data[$impact_field] != $impact) {
            $update_data[$impact_field] = $impact;
            $c[$impact_field] = array('to'=>$impact,'from'=>$old_data[$impact_field]);
        }

        if($old_data[$likelihood_field] != $likelihood) {
            $update_data[$likelihood_field] = $likelihood;
            $c[$likelihood_field] = array('to'=>$likelihood,'from'=>$old_data[$likelihood_field]);
        }

        if($old_data[$control_effectiveness_field] != $control_effectiveness) {
            $update_data[$control_effectiveness_field] = $control_effectiveness;
            $c[$control_effectiveness_field] = array('to'=>$control_effectiveness,'from'=>$old_data[$control_effectiveness_field]);
        }

        if($old_data[$impact_rating_field] != $impact_rating) {
            $update_data[$impact_rating_field] = $impact_rating;
            $c[$impact_rating_field] = array('to'=>$impact_rating,'from'=>$old_data[$impact_rating_field]);
        }

        if($old_data[$likelihood_rating_field] != $likelihood_rating) {
            $update_data[$likelihood_rating_field] = $likelihood_rating;
            $c[$likelihood_rating_field] = array('to'=>$likelihood_rating,'from'=>$old_data[$likelihood_rating_field]);
        }

        if($old_data[$control_effectiveness_rating_field] != $control_effectiveness_rating) {
            $update_data[$control_effectiveness_rating_field] = $control_effectiveness_rating;
            $c[$control_effectiveness_rating_field] = array('to'=>$control_effectiveness_rating,'from'=>$old_data[$control_effectiveness_rating_field]);
        }
        /**************************************************************************************************************/

        /*
         * ****************************************************************************************
         * **************** INHERENT RISK EXPOSURE CALCULATION AND CONNECTION - START *************
         * ****************************************************************************************
         * */

        //Let's start by calculating the actual values we captured in the form
        $inherent_exposure_rating = $impact_rating * $likelihood_rating;
        $var['rsk_inherent_exposure_rating'] = $inherent_exposure_rating;

        //And then get on with connecting those values to the respective exposure list information

        $listObject = new RGSTR2_LIST('risk_inherent_exposure');
        //Get all the inherent risk exposure list items
        $inherent_exposure_items = $listObject->getActiveListItems();

        $inherent_exposure_id = 0;

        //Foreach through them...
        foreach($inherent_exposure_items as $key => $val){
            //... get the rating_from and rating_to values for this record...
            $from = 1*$val['rating_from'];
            $to = 1*$val['rating_to'];
            //... Create an array with all the numbers in between these values...
            $range_value_array = range($from, $to);

            //... Check if the rating that we captured is in that range...
            if(in_array($inherent_exposure_rating, $range_value_array)){
                //... If it is, then assign the id of this record to the $inherent_exposure_id, which will be used as the FK in the Risk table...
                $inherent_exposure_id = 1*$val['id'];
                //... Break out of this loop, because we found what we're looking for...
                break;
            }
        }
        //... Then make the FK assignment
        $var['rsk_inherent_exposure'] = $inherent_exposure_id;
        //... AND LIFE IS A JOY!!!


        $inherent_exposure_field = 'rsk_inherent_exposure';
        $inherent_exposure_rating_field = 'rsk_inherent_exposure_rating';

        $inherent_exposure = $var[$inherent_exposure_field];

        $inherent_exposure_rating = 1*$var[$inherent_exposure_rating_field];


        if($old_data[$inherent_exposure_field] != $inherent_exposure) {
            $update_data[$inherent_exposure_field] = $inherent_exposure;
            $c[$inherent_exposure_field] = array('to'=>$inherent_exposure,'from'=>$old_data[$inherent_exposure_field]);
        }

        if($old_data[$inherent_exposure_rating_field] != $inherent_exposure_rating) {
            $update_data[$inherent_exposure_rating_field] = $inherent_exposure_rating;
            $c[$inherent_exposure_rating_field] = array('to'=>$inherent_exposure_rating,'from'=>$old_data[$inherent_exposure_rating_field]);
        }

        /*
         * ****************************************************************************************
         * **************** INHERENT RISK EXPOSURE CALCULATION AND CONNECTION - END *************
         * ****************************************************************************************
         * */



        /*
         * ****************************************************************************************
         * **************** RESIDUAL RISK EXPOSURE CALCULATION AND CONNECTION - START *************
         * ****************************************************************************************
         * */
        //Let's start by calculating the actual values we captured in the form

        $residual_exposure_rating = $inherent_exposure_rating * $control_effectiveness_rating;
        //Get a round figure, then assign it to the field in the risk table
        $residual_exposure_rating = 1*$residual_exposure_rating;
        $var['rsk_residual_exposure_rating'] = $residual_exposure_rating;

        //And then get on with connecting those values to the respective exposure list information

        //Get all the inherent risk exposure list items
        $listObject->changeListType('risk_residual_exposure');
        $residual_exposure_items = $listObject->getActiveListItems();

        $residual_exposure_id = 0;

        //Foreach through them...
        foreach($residual_exposure_items as $key => $val){
            //... get the rating_from and rating_to values for this record...
            $from = 1*$val['rating_from'];
            $to = 1*$val['rating_to'];
            //... Create an array with all the numbers in between these values...
            $range_value_array = range($from, $to);

            //... Check if the rating that we captured is in that range...
            if($residual_exposure_rating >= $from && $residual_exposure_rating <= $to){
                //... If it is, then assign the id of this record to the $residual_exposure_id, which will be used as the FK in the Risk table...
                $residual_exposure_id = 1*$val['id'];
                //... Break out of this loop, because we found what we're looking for...
                break;
            }
        }
        //... Then make the FK assignment
        $var['rsk_residual_exposure'] = $residual_exposure_id;
        //... AND LIFE IS A JOY!!!

		//Acceptable risk check
		//AA-686 Risk Assist Acceptable Risk Exposure status not Functioning [SD] 26-08-2021
		$rsk_acceptable_risk_exposure = "rsk_acceptable_risk_exposure";
		if(isset($var[$rsk_acceptable_risk_exposure])){
			$acceptable_risk_exposure = $var[$rsk_acceptable_risk_exposure];
			$var[$rsk_acceptable_risk_exposure] = 1*$var[$rsk_acceptable_risk_exposure];
		}else{
			$acceptable_risk_exposure = $old_data[$rsk_acceptable_risk_exposure];
			$var[$rsk_acceptable_risk_exposure] = 1*$old_data[$rsk_acceptable_risk_exposure];
		}

		$is_risk_acceptable = 0;
		if($acceptable_risk_exposure >= $residual_exposure_rating){
			$is_risk_acceptable = 1;
		}
		if($old_data['rsk_is_risk_acceptable'] != $is_risk_acceptable) {
			$update_data['rsk_is_risk_acceptable'] = $is_risk_acceptable;
			$c['rsk_is_risk_acceptable'] = array('to'=>$is_risk_acceptable,'from'=>$old_data['rsk_is_risk_acceptable']);
		}



        $residual_exposure_field = 'rsk_residual_exposure';
        $residual_exposure_rating_field = 'rsk_residual_exposure_rating';

        $residual_exposure = $var[$residual_exposure_field];

        $residual_exposure_rating = 1*$var[$residual_exposure_rating_field];


        if($old_data[$residual_exposure_field] != $residual_exposure) {
            $update_data[$residual_exposure_field] = $residual_exposure;
            $c[$residual_exposure_field] = array('to'=>$residual_exposure,'from'=>$old_data[$residual_exposure_field]);
        }

        if($old_data[$residual_exposure_rating_field] != $residual_exposure_rating) {
            $update_data[$residual_exposure_rating_field] = $residual_exposure_rating;
            $c[$residual_exposure_rating_field] = array('to'=>$residual_exposure_rating,'from'=>$old_data[$residual_exposure_rating_field]);
        }
        /*
         * ****************************************************************************************
         * **************** RESIDUAL RISK EXPOSURE CALCULATION AND CONNECTION - END *************
         * ****************************************************************************************
         * */

        if($old_data['rsk_current_controls'] != $var['rsk_current_controls']) {
            $update_data['rsk_current_controls'] = $var['rsk_current_controls'];
            $c['rsk_current_controls'] = array('to'=>$var['rsk_current_controls'],'from'=>$old_data['rsk_current_controls']);
        }


        /**************************************************************************************************************/

        /*
         * *************************************************************************************************************
         * **************** NEW RISK UPDATE CODE WITH ALL THE NEW UPDATE-ABLE FIELDS FROM THE EDIT - START *************
         * *************************************************************************************************************
         * */



		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

        $mar = 1*$mar;
		if($mar>0 || strlen($var['response'])>0) {
			$changes = array(
				'response'=>$var['response'],
				'user'=>$this->getUserName(),
				'action_on'=>$var['action_on'],
			);

			if($old_data[$si_field] != $status_id) {
				$listObject = new RGSTR2_LIST('risk_status');
				$items = $listObject->getAListItemName(array($status_id,$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$status_id],'from'=>$items[$old_data[$si_field]]);
			}

            if(isset($c) && is_array($c) && count($c) > 0){
                foreach($c as $key => $val){
                    $changes[$key] = $val;
                }
            }

			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getUpdateAttachmentFieldName()] = $x;
			if( ($new_status & RGSTR2::AWAITING_APPROVAL) == RGSTR2::AWAITING_APPROVAL) {
				$changes['status'] = "|risk| is now awaiting approval.";
			}
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::UPDATE,
				'status_id'	=> $status_id,		
				'attachment'	=> '',
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);

			$registerObj = new RGSTR2_REGISTER(); //AA-719 RGSR 2 Automated Emails Cancelation to Assignees before Register Activation [SD] 03/10/2021
			$sql = "SELECT ".$registerObj->getStatusFieldName()." as status FROM ".$registerObj->getTableName()." WHERE ".$registerObj->getIDFieldName()." = ".$contract_id;
			$old = $this->mysql_fetch_one($sql);
			$old_status = $old['status'];
			if($old_status == (RGSTR2_REGISTER::ACTIVE + RGSTR2_REGISTER::CONFIRMED + RGSTR2_REGISTER::ACTIVATED)) {
				$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			}

			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." has been updated successfully.");
	//		ASSIST_HELPER::arrPrint($note['progress']);
	//		return array("info", $note);
		}
		
		return array("error","An error occurred.  Please try again.".$sql);
		
		
	}

	public function editObject($var,$attach=array()) {
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
//		$noter = strtoupper($var['notification_chooser']);
		$noter = 'Non existant again';
//		$recipients = $var['recipient_mobile'];
		$recipients = 'Non Existant';
//		$include = $var['include_rec'];
		$include = '';
		unset($var['include_rec']);
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */


        $object_id = $var['object_id'];
        $c = array();

        $old_data = $this->getRawObject((string)$object_id);
		$contract_id = $old_data['rsk_contract_id'];

        $update_data = array( );


        /*
         * *************************************************************************************************************
         * **************** NEW RISK UPDATE CODE WITH ALL THE NEW UPDATE-ABLE FIELDS FROM THE EDIT - START *************
         * *************************************************************************************************************
         * */
        $impact_field = 'rsk_impact';
        $impact_rating_field = 'rsk_impact_rating';

        $likelihood_field = 'rsk_likelihood';
        $likelihood_rating_field = 'rsk_likelihood_rating';

        $control_effectiveness_field = 'rsk_control_effectiveness';
        $control_effectiveness_rating_field = 'rsk_control_effectiveness_rating';

        if(isset($var[$impact_field]) || isset($var[$likelihood_field]) || isset($var[$control_effectiveness_field])){
            $impact = $var[$impact_field];
            $likelihood = $var[$likelihood_field];
            $control_effectiveness = $var[$control_effectiveness_field];

            $impact_rating = 1*$var[$impact_rating_field];
            $likelihood_rating = 1*$var[$likelihood_rating_field];
            $control_effectiveness_rating = 1*$var[$control_effectiveness_rating_field];

            if($old_data[$impact_field] != $impact) {
                $update_data[$impact_field] = $impact;
                $c[$impact_field] = array('to'=>$impact,'from'=>$old_data[$impact_field]);
            }

            if($old_data[$likelihood_field] != $likelihood) {
                $update_data[$likelihood_field] = $likelihood;
                $c[$likelihood_field] = array('to'=>$likelihood,'from'=>$old_data[$likelihood_field]);
            }

            if($old_data[$control_effectiveness_field] != $control_effectiveness) {
                $update_data[$control_effectiveness_field] = $control_effectiveness;
                $c[$control_effectiveness_field] = array('to'=>$control_effectiveness,'from'=>$old_data[$control_effectiveness_field]);
            }

            if($old_data[$impact_rating_field] != $impact_rating) {
                $update_data[$impact_rating_field] = $impact_rating;
                $c[$impact_rating_field] = array('to'=>$impact_rating,'from'=>$old_data[$impact_rating_field]);
            }

            if($old_data[$likelihood_rating_field] != $likelihood_rating) {
                $update_data[$likelihood_rating_field] = $likelihood_rating;
                $c[$likelihood_rating_field] = array('to'=>$likelihood_rating,'from'=>$old_data[$likelihood_rating_field]);
            }

            if($old_data[$control_effectiveness_rating_field] != $control_effectiveness_rating) {
                $update_data[$control_effectiveness_rating_field] = $control_effectiveness_rating;
                $c[$control_effectiveness_rating_field] = array('to'=>$control_effectiveness_rating,'from'=>$old_data[$control_effectiveness_rating_field]);
            }
            /**************************************************************************************************************/

            /*
             * ****************************************************************************************
             * **************** INHERENT RISK EXPOSURE CALCULATION AND CONNECTION - START *************
             * ****************************************************************************************
             * */

            //Let's start by calculating the actual values we captured in the form
            $inherent_exposure_rating = $impact_rating * $likelihood_rating;
            $var['rsk_inherent_exposure_rating'] = $inherent_exposure_rating;

            //And then get on with connecting those values to the respective exposure list information

            $listObject = new RGSTR2_LIST('risk_inherent_exposure');
            //Get all the inherent risk exposure list items
            $inherent_exposure_items = $listObject->getActiveListItems();

            $inherent_exposure_id = 0;

            //Foreach through them...
            foreach($inherent_exposure_items as $key => $val){
                //... get the rating_from and rating_to values for this record...
                $from = 1*$val['rating_from'];
                $to = 1*$val['rating_to'];
                //... Create an array with all the numbers in between these values...
                $range_value_array = range($from, $to);

                //... Check if the rating that we captured is in that range...
                if(in_array($inherent_exposure_rating, $range_value_array)){
                    //... If it is, then assign the id of this record to the $inherent_exposure_id, which will be used as the FK in the Risk table...
                    $inherent_exposure_id = 1*$val['id'];
                    //... Break out of this loop, because we found what we're looking for...
                    break;
                }
            }
            //... Then make the FK assignment
            $var['rsk_inherent_exposure'] = $inherent_exposure_id;
            //... AND LIFE IS A JOY!!!


            $inherent_exposure_field = 'rsk_inherent_exposure';
            $inherent_exposure_rating_field = 'rsk_inherent_exposure_rating';

            $inherent_exposure = $var[$inherent_exposure_field];

            $inherent_exposure_rating = 1*$var[$inherent_exposure_rating_field];


            if($old_data[$inherent_exposure_field] != $inherent_exposure) {
                $update_data[$inherent_exposure_field] = $inherent_exposure;
                $c[$inherent_exposure_field] = array('to'=>$inherent_exposure,'from'=>$old_data[$inherent_exposure_field]);
            }

            if($old_data[$inherent_exposure_rating_field] != $inherent_exposure_rating) {
                $update_data[$inherent_exposure_rating_field] = $inherent_exposure_rating;
                $c[$inherent_exposure_rating_field] = array('to'=>$inherent_exposure_rating,'from'=>$old_data[$inherent_exposure_rating_field]);
            }

            /*
             * ****************************************************************************************
             * **************** INHERENT RISK EXPOSURE CALCULATION AND CONNECTION - END *************
             * ****************************************************************************************
             * */



            /*
             * ****************************************************************************************
             * **************** RESIDUAL RISK EXPOSURE CALCULATION AND CONNECTION - START *************
             * ****************************************************************************************
             * */
            //Let's start by calculating the actual values we captured in the form

            $residual_exposure_rating = $inherent_exposure_rating * $control_effectiveness_rating;
            //Get a round figure, then assign it to the field in the risk table
            $residual_exposure_rating = 1*$residual_exposure_rating;
            $var['rsk_residual_exposure_rating'] = $residual_exposure_rating;

            //And then get on with connecting those values to the respective exposure list information

            //Get all the inherent risk exposure list items
            $listObject->changeListType('risk_residual_exposure');
            $residual_exposure_items = $listObject->getActiveListItems();

            $residual_exposure_id = 0;

            //Foreach through them...
            foreach($residual_exposure_items as $key => $val){
                //... get the rating_from and rating_to values for this record...
                $from = 1*$val['rating_from'];
                $to = 1*$val['rating_to'];
                //... Create an array with all the numbers in between these values...
                $range_value_array = range($from, $to);

                //... Check if the rating that we captured is in that range...
                if($residual_exposure_rating >= $from && $residual_exposure_rating <= $to){
                    //... If it is, then assign the id of this record to the $residual_exposure_id, which will be used as the FK in the Risk table...
                    $residual_exposure_id = 1*$val['id'];
                    //... Break out of this loop, because we found what we're looking for...
                    break;
                }
            }
            //... Then make the FK assignment
            $var['rsk_residual_exposure'] = $residual_exposure_id;
            //... AND LIFE IS A JOY!!!

            $residual_exposure_field = 'rsk_residual_exposure';
            $residual_exposure_rating_field = 'rsk_residual_exposure_rating';

            $residual_exposure = $var[$residual_exposure_field];

            $residual_exposure_rating = 1*$var[$residual_exposure_rating_field];


            if($old_data[$residual_exposure_field] != $residual_exposure) {
                $update_data[$residual_exposure_field] = $residual_exposure;
                $c[$residual_exposure_field] = array('to'=>$residual_exposure,'from'=>$old_data[$residual_exposure_field]);
            }

            if($old_data[$residual_exposure_rating_field] != $residual_exposure_rating) {
                $update_data[$residual_exposure_rating_field] = $residual_exposure_rating;
                $c[$residual_exposure_rating_field] = array('to'=>$residual_exposure_rating,'from'=>$old_data[$residual_exposure_rating_field]);
            }
            /*
             * ****************************************************************************************
             * **************** RESIDUAL RISK EXPOSURE CALCULATION AND CONNECTION - END *************
             * ****************************************************************************************
             * */

            if($old_data['rsk_current_controls'] != $var['rsk_current_controls']) {
                $update_data['rsk_current_controls'] = $var['rsk_current_controls'];
                $c['rsk_current_controls'] = array('to'=>$var['rsk_current_controls'],'from'=>$old_data['rsk_current_controls']);
            }
        }
		//Acceptable risk check
		//AA-686 Risk Assist Acceptable Risk Exposure status not Functioning [SD] 26-08-2021
		$rsk_acceptable_risk_exposure = "rsk_acceptable_risk_exposure";
		if(isset($var[$rsk_acceptable_risk_exposure])){
			$acceptable_risk_exposure = $var[$rsk_acceptable_risk_exposure];
			$var[$rsk_acceptable_risk_exposure] = 1*$var[$rsk_acceptable_risk_exposure];
		}else{
			$acceptable_risk_exposure = $old_data[$rsk_acceptable_risk_exposure];
			$var[$rsk_acceptable_risk_exposure] = 1*$old_data[$rsk_acceptable_risk_exposure];
		}

		$residual_exposure_rating = ($old_data['rsk_inherent_exposure_rating'] * $old_data['rsk_control_effectiveness_rating']) * 1;
		$is_risk_acceptable = 0;
		if($acceptable_risk_exposure >= $residual_exposure_rating){
			$is_risk_acceptable = 1;
		}
		if($old_data['rsk_is_risk_acceptable'] != $is_risk_acceptable) {
			$update_data['rsk_is_risk_acceptable'] = $is_risk_acceptable;
			$c['rsk_is_risk_acceptable'] = array('to'=>$is_risk_acceptable,'from'=>$old_data['rsk_is_risk_acceptable']);
		}
        /**************************************************************************************************************/

        /*
         * *************************************************************************************************************
         * **************** NEW RISK UPDATE CODE WITH ALL THE NEW UPDATE-ABLE FIELDS FROM THE EDIT - START *************
         * *************************************************************************************************************
         * */


        /*
         * TAKEN FROM RGSTR2 - START*/
        $headObject = new RGSTR2_HEADINGS();
        $headings = $headObject->getMainObjectHeadings($this->getMyObjectType(),"FORM");
        foreach($var as $fld=>$v) {
            if(isset($headings['rows'][$fld])) {
                if($this->isDateField($fld) || $headings['rows'][$fld]['type']=="DATE") {
                    if(strlen($v)>0){
                        $update_data[$fld] = date("Y-m-d",strtotime($v));
                    }
                } elseif($fld=="rsk_type") {
                    if($v=="DEL") {
                        $var['rsk_parent_id'] = 0;
                        $update_data['rsk_parent_id'] = 0;
                    }
                    $update_data[$fld] = $v;
                } else {
                    $update_data[$fld] = $v;
                }
            }
        }
        /*
         * TAKEN FROM RGSTR2 - END*/



        $sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
        $mar = $this->db_update($sql);

        $mar = 1*$mar;
        if($mar>0) {
            $changes = array(
                'user'=>$this->getUserName(),
            );

            foreach($update_data as $fld => $v) {
                $h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type'=>"TEXT");
                if($old_data[$fld] != $v || $h['type']=="HEADING") {
                    if(in_array($h['type'],array("LIST","MASTER","USER","OWNER","DEL_TYPE","DELIVERABLE"))) {
                        $list_items = array();
                        $ids = array($v,$old_data[$fld]);
                        switch($h['type']) {
                            case "LIST":
                                $listObject = new RGSTR2_LIST($h['list_table']);
                                $list_items = $listObject->getAListItemName($ids);
                                break;
                            case "USER":
                                $userObject = new RGSTR2_USERACCESS();
                                $list_items = $userObject->getActiveUsersFormattedForSelect($ids);
                                break;
                            case "OWNER":
                                $ownerObject = new RGSTR2_CONTRACT_OWNER();
                                $list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
                                break;
                            case "DELIVERABLE":
                                $delObject = new RGSTR2_RISK();
                                $list_items = $delObject->getDeliverableNamesForSubs($ids);
                                break;
                            case "DEL_TYPE":
                                $delObject = new RGSTR2_RISK();
                                $list_items = $delObject->getDeliverableTypes($ids);
                                break;
                        }

                        unset($listObject);
                        $changes[$fld] = array('to'=>$list_items[$v],'from'=>isset($list_items[$old_data[$fld]]) ? $list_items[$old_data[$fld]] : $this->getUnspecified(), 'raw'=>array('to'=>$v,'from'=>$old_data[$fld]));
                    } else {
                        $changes[$fld] = array('to'=>$v,'from'=>$old_data[$fld]);
                    }
                }
            }

            if(isset($c) && is_array($c) && count($c) > 0){
                foreach($c as $key => $val){
                    $changes[$key] = $val;
                }
            }

            $x = array();
            foreach($attach as $a) {
                $x[] = "|attachment| ".$a['original_filename']." has been added.";
            }
            $changes[$this->getUpdateAttachmentFieldName()] = $x;

            $log_var = array(
                'object_id'	=> $object_id,
                'changes'	=> $changes,
                'log_type'	=> RGSTR2_LOG::EDIT,
                'status_id'	=> $old_data[$this->getTableField()."_status"],
                'attachment'	=> '',
            );
            $this->addActivityLog(self::LOG_TABLE, $log_var);

			$registerObj = new RGSTR2_REGISTER(); //AA-719 RGSR 2 Automated Emails Cancelation to Assignees before Register Activation [SD] 03/10/2021
			$sql = "SELECT ".$registerObj->getStatusFieldName()." as status FROM ".$registerObj->getTableName()." WHERE ".$registerObj->getIDFieldName()." = ".$contract_id;
			$old = $this->mysql_fetch_one($sql);
			$old_status = $old['status'];
			if($old_status == (RGSTR2_REGISTER::ACTIVE + RGSTR2_REGISTER::CONFIRMED + RGSTR2_REGISTER::ACTIVATED)) {
				$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			}
            return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." has been updated successfully.");

        }

        return array("error","An error occurred.  Please try again.");
	}


    public function deleteObject($var) {
        unset($var['attachments']);

        $object_id = $var['object_id'];

        $sql = "SELECT ".$this->getStatusFieldName()." as status ,rsk_contract_id as contract_id FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
        $old = $this->mysql_fetch_one($sql);

		$contract_id = $old['contract_id'];
        $old_status = $old['status'];
        $new_status = 1*$old_status - self::ACTIVE + self::DELETED;

        $sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
        $mar = $this->db_update($sql);
        if($mar>0) {
            //LOG!!!
            $changes = array(
                'response'=>"|risk| deletion.",
                'user'=>$this->getUserName(),
            );

            $sql = "SELECT * FROM ".$this->getDBRef()."_risk_log ORDER BY dl_id DESC ";
            $last_log = $this->mysql_fetch_one($sql);

            $status_id = $last_log['dl_status_id'];

            $log_var = array(
                'object_id'	=> $object_id,
                'changes'	=> $changes,
                'log_type'	=> RGSTR2_LOG::DELETE,
                'status_id'	=> $status_id,
                'attachment'	=> "",
            );

            $this->addActivityLog("risk", $log_var);

			$registerObj = new RGSTR2_REGISTER(); //AA-719 RGSR 2 Automated Emails Cancelation to Assignees before Register Activation [SD] 03/10/2021
			$sql = "SELECT ".$registerObj->getStatusFieldName()." as status FROM ".$registerObj->getTableName()." WHERE ".$registerObj->getIDFieldName()." = ".$contract_id;
			$old = $this->mysql_fetch_one($sql);
			$old_status = $old['status'];
			if($old_status == (RGSTR2_REGISTER::ACTIVE + RGSTR2_REGISTER::CONFIRMED + RGSTR2_REGISTER::ACTIVATED)) {
				$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			}

            return array(0 => "ok", 1 => $this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully deleted.");
        } else {
            return array("error","An error occurred while trying to delete ".$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id.". Please reload the page and try again.");
        }
    }

    public function restoreObject($var) {
        unset($var['attachments']);

        $object_id = $var['action_id'];

        $sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$object_id;
        $old = $this->mysql_fetch_one($sql);

        $old_status = $old['status'];
        $new_status = 1*$old_status - self::DELETED + self::ACTIVE;

        $sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$object_id;
        $mar = $this->db_update($sql);
        if($mar>0) {
            //LOG!!!
            $changes = array(
                'response'=>"|risk| restored.",
                'user'=>$this->getUserName(),
            );

            $sql = "SELECT * FROM ".$this->getDBRef()."_risk_log ORDER BY dl_id DESC ";
            $last_log = $this->mysql_fetch_one($sql);

            $status_id = $last_log['dl_status_id'];

            $log_var = array(
                'object_id'	=> $object_id,
                'changes'	=> $changes,
                'log_type'	=> RGSTR2_LOG::RESTORE,
                'status_id'	=> $status_id,
                'attachment'	=> "",
            );

            $this->addActivityLog("risk", $log_var);

            $note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
            return array(0 => "ok", 1 => $this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." successfully restored.");
        } else {
            return array("error","An error occurred while trying to restore ".$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id.". Please reload the page and try again.");
        }
    }



	public function approveObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
//		$noter = strtoupper($var['notification_chooser']);
//		$recipients = $var['recipient_mobile'];
//		unset($var['recipient_mobile']);
//		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - RGSTR2::AWAITING_APPROVAL + RGSTR2::APPROVED;
		
		$update_data = array(
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			$changes['status'] = "|risk| has been approved.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::APPROVE,
				'progress'	=> $old_data[$p_field],
				'status_id'	=> $old_data[$si_field],		
				'attachment'	=> '',
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);


            $note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			/** NOTIFY DELIVERABLE OWNER THAT DELIVERABLE HAS BEEN APPROVED **/
//			$log_var['recipients']=$recipients;
//			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			return array("ok",$this->getDeliverableObjectName()." ".$this->getRefTag().$object_id." has been successfully approved.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}

	public function declineObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - RGSTR2::AWAITING_APPROVAL;
		
		$update_data = array(
			$p_field=>"99",
			$si_field=>"2",
			$dc_field=>"0000-00-00",
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			if($old_data[$p_field] != $update_data[$p_field]) {
				$changes[$p_field] = array('to'=>$update_data[$p_field],'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $update_data[$si_field]) {
				$listObject = new RGSTR2_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($update_data[$si_field],$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$update_data[$si_field]],'from'=>$items[$old_data[$si_field]]);
			}
			if($old_data[$dc_field] != $update_data[$dc_field]) {
				$changes[$dc_field] = array('to'=>$update_data[$dc_field],'from'=>$old_data[$dc_field]);
			}
			$changes['status'] = "|risk| has been declined.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::DECLINE,
				'progress'	=> $update_data[$p_field],
				'status_id'	=> $update_data[$si_field],		
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);


            $note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			/** NOTIFY DELIVERABLE OWNER THAT DELIVERABLE HAS BEEN DECLINED **/
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			return array("ok",$this->getDeliverableObjectName()." ".$this->getRefTag().$object_id." has been successfully declined.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}



    public function unlockApprovedObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - RGSTR2::APPROVED;
		
		$update_data = array(
			$p_field=>"99",
			$si_field=>"2",
			$dc_field=>"0000-00-00",
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			if($old_data[$p_field] != $update_data[$p_field]) {
				$changes[$p_field] = array('to'=>$update_data[$p_field],'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $update_data[$si_field]) {
				$listObject = new RGSTR2_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($update_data[$si_field],$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$update_data[$si_field]],'from'=>$items[$old_data[$si_field]]);
			}
			if($old_data[$dc_field] != $update_data[$dc_field]) {
				$changes[$dc_field] = array('to'=>$update_data[$dc_field],'from'=>$old_data[$dc_field]);
			}
			$changes['status'] = "Approval of |risk| has been reversed.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::UNLOCK,
				'progress'	=> $update_data[$p_field],
				'status_id'	=> $update_data[$si_field],		
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			/** NOTIFY DELIVERABLE OWNER THAT APPROVAL OF DELIVERABLE HAS BEEN REVERSED **/
			$log_var['recipients']=$recipients;
            $note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			
			return array("ok",$this->getDeliverableObjectName()." ".$this->getRefTag().$object_id." has been successfully unlocked.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}



	
    /************************************
     * GET functions
     * */
    public function getSummaryFields() { return $this->summary_heading_fields; }
	public function getTableField() { return self::TABLE_FLD; }
	public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
	public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
	public function getParentID($i) {
		$sql = "SELECT " .  $this->getTableField()  . "_contract_id FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_id = ".$i;
		return $this->mysql_fetch_one_value($sql, "" .  $this->getTableField()  . "_contract_id");
	}
	public function getDeliverablesForParentSelect($contract_id) {
		$sql = "SELECT D." .  $this->getTableField()  . "_id as id, D." .  $this->getTableField()  . "_name as name FROM ".$this->getTableName()." D WHERE D." .  $this->getTableField()  . "_contract_id = ".$contract_id." AND D." .  $this->getTableField()  . "_type = 'MAIN' AND ".$this->getActiveStatusSQL("D");
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
	}
	public function getDeliverablesWithDeadlineForParentSelect($contract_id) {
		$sql = "SELECT D." .  $this->getTableField()  . "_id as id, D." .  $this->getTableField()  . "_name as name FROM ".$this->getTableName()." D WHERE D." .  $this->getTableField()  . "_contract_id = ".$contract_id." AND D." .  $this->getTableField()  . "_type = 'MAIN' AND ".$this->getActiveStatusSQL("D");
		return $this->mysql_fetch_all_by_id($sql, "id");
	}

	public function getList($section,$options=array()) {
		return $this->getMyList("RISK", $section,$options);
	}
	
	public function getAObject($id,$options=array()) {
		return $this->getDetailedObject("RISK", $id,$options);
	}     

	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id="",$parent_id="") {
		if(is_array($obj_id) && count($obj_id)>0) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_id IN (".implode(",",$obj_id).")";
			$data = $this->mysql_fetch_all($sql);
		} elseif(!is_array($obj_id) && strlen($obj_id)>0 && is_numeric($obj_id)) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_id = $obj_id ";
			$data = $this->mysql_fetch_one($sql);
		} elseif(is_array($parent_id) && count($parent_id)>0) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_contract_id IN (".implode(",",$parent_id).")";
			$data = $this->mysql_fetch_all($sql);
		} elseif(!is_array($parent_id) && strlen($parent_id)>0 && is_numeric($parent_id)) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_id = $parent_id ";
			$data = $this->mysql_fetch_one($sql);
		} else {
			$data = array();
		}
		return $data;
	}

    public function getObjectDetails(){
        return $this->object_details;
    }
	
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		$data = array(
			$this->getUpdateAttachmentFieldName()=>$raw[$this->getUpdateAttachmentFieldName()],
			$this->getProgressFieldName()=>$raw[$this->getProgressFieldName()],
			$this->getProgressStatusFieldName()=>$raw[$this->getProgressStatusFieldName()],
		);
		return $data;
	}
		
	public function getOrderedObjects($contract_id){
		$actObject = new RGSTR2_ACTION();
		$act_tbl = $actObject->getTableName();
		$act_progress = $actObject->getProgressFieldName();
		$act_id = $actObject->getIDFieldName();
		$act_parent = $actObject->getParentFieldName();
		$sql = "SELECT " .  $this->getTableField()  . "_id as id
						, " .  $this->getTableField()  . "_type
						, " .  $this->getTableField()  . "_parent_id as parent_id
						, " .  $this->getTableField()  . "_name as name
						, CONCAT('".$this->getRefTag()."'," .  $this->getTableField()  . "_id) as reftag
						, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
						, AVG(A.".$act_progress.") as action_progress
						, COUNT(A.".$act_id.") as action_count
						FROM ".$this->getTableName()." D
						LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = D.".$this->getOwnerFieldName()." 
						LEFT OUTER JOIN ".$act_tbl." A ON A.".$act_parent." = D.".$this->getIDFieldName()." 
						WHERE D." .  $this->getTableField()  . "_contract_id = $contract_id AND ".$this->getActiveStatusSQL("D")."
						GROUP BY D." .  $this->getTableField()  . "_id
						ORDER BY " .  $this->getTableField()  . "_id ASC";
		//$res = $this->mysql_fetch_all_by_id($sql, "" .  $this->getTableField()  . "_id");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		foreach($res2 as $p => $g) {
			foreach($g as $i => $d){
				if(is_null($d['action_progress']) || $d['action_count']==0) {
					if($d['" .  $this->getTableField()  . "_type']=="MAIN") {
						$res2[$p][$i]['action_progress'] = "-";
					} else {
						$res2[$p][$i]['action_progress'] = "N/A";
					}
				}
			}
		}
		return $res2;
	}
	public function getCategoryGroupedOrderedObjects($contract_id, $group_field = ''){
		$actObject = new RGSTR2_ACTION();
		$act_tbl = $actObject->getTableName();
		$act_progress = $actObject->getProgressFieldName();
		$act_id = $actObject->getIDFieldName();
		$act_parent = $actObject->getParentFieldName();

        if(strlen($group_field) == 0){
            $setupObject = new RGSTR2_SETUP_PREFERENCES();
            $group_field = $setupObject->getAnswerToQuestion(13);
        }
		
		$res2 = array();
		$sql = "SELECT " .  $this->getTableField()  . "_id as id
						, " .  $group_field  . " as group_field
						, " .  $this->getTableField()  . "_type
						, " .  $this->getTableField()  . "_parent_id as parent_id
						, " .  $this->getTableField()  . "_name as name
						, CONCAT('".$this->getRefTag()."'," .  $this->getTableField()  . "_id) as reftag 
						, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
						, AVG(A.".$act_progress.") as action_progress
						FROM ".$this->getTableName()." D
						LEFT OUTER JOIN ".$act_tbl." A ON A.".$act_parent." = D.".$this->getIDFieldName()." 
						LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = D.".$this->getOwnerFieldName()." 
						WHERE " .  $this->getTableField()  . "_contract_id = $contract_id AND " .  $this->getTableField()  . "_parent_id = 0 AND ".$this->getActiveStatusSQL("D")."
						GROUP BY D." .  $this->getTableField()  . "_id
						ORDER BY " .  $this->getTableField()  . "_id ASC";
		//$res = $this->mysql_fetch_all_by_id($sql, "" .  $this->getTableField()  . "_id");
		$res2['main'] = $this->mysql_fetch_all_by_id2($sql,"group_field", "id");
		$sql = "SELECT " .  $this->getTableField()  . "_id as id
						, " .  $group_field  . " as group_field
						, " .  $this->getTableField()  . "_type
						, " .  $this->getTableField()  . "_parent_id as parent_id
						, " .  $this->getTableField()  . "_name as name
						, CONCAT('".$this->getRefTag()."'," .  $this->getTableField()  . "_id) as reftag
						, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
						, AVG(A.".$act_progress.") as action_progress
						FROM ".$this->getTableName()." D
						LEFT OUTER JOIN ".$act_tbl." A ON A.".$act_parent." = D.".$this->getIDFieldName()." 
						LEFT JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = D.".$this->getOwnerFieldName()." 
						WHERE " .  $this->getTableField()  . "_contract_id = $contract_id AND " .  $this->getTableField()  . "_parent_id > 0 AND ".$this->getActiveStatusSQL("D")."
						GROUP BY D." .  $this->getTableField()  . "_id
						ORDER BY " .  $this->getTableField()  . "_id ASC";
		//$res = $this->mysql_fetch_all_by_id($sql, "" .  $this->getTableField()  . "_id");
		$res2['subs'] = $this->mysql_fetch_all_by_id2($sql,"parent_id", "id");
		return $res2;
	}
	/***
	 * Returns an array of the IDs of deliverables associated with a contract
	 */
	public function getDelIds($obj_id, $fields=array()) {
		if(is_array($obj_id)) {
			$sql = "SELECT " .  $this->getTableField()  . "_id FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_contract_id IN (".implode(",",$obj_id).")";
			$data = $this->mysql_fetch_all($sql);				
		} else {
			$sql = "SELECT " .  $this->getTableField()  . "_id ".(count($fields)>0?", ".implode(",",$fields):"")." FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_contract_id = $obj_id ";
			$data = $this->mysql_fetch_all($sql);
		}
		return $data;
	}
	/***
	 * returns the display name of a specific deliverable
	 */
	public function getDeliverableNamesForSubs($ids) {
		$sql = "SELECT D." .  $this->getTableField()  . "_parent_id as id, CONCAT(P." .  $this->getTableField()  . "_name,' [".$this->getRefTag()."',D." .  $this->getTableField()  . "_parent_id,']') as name
				FROM  ".$this->getTableName()." D
				INNER JOIN ".$this->getTableName()." P ON D." .  $this->getTableField()  . "_parent_id = P." .  $this->getTableField()  . "_id
				WHERE ".(is_array($ids) ? "D." .  $this->getTableField()  . "_id IN ( ".implode(",",$ids)." ) " : "D." .  $this->getTableField()  . "_id = $ids ");
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");  //$this->arrPrint($data);
		return $data;
	}
     
	/**
	 * Returns status check for Deliverables which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
     
	 
	public function getDeadlineDate() {
//		return $this->object_details[ $this->getTableField() . '_deadline'];
		return false;
	}
	public function hasDeadline() { return true; }
	public function getDeadlineField() { return $this->deadline_field; }
	
	/**
	 * Function to fetch the recipients of the notifications which are about to go out
	 */
	public function getObjectRecipients($data){
	$id = $data['id'];
	$action = $data['activity'];
	$extra = strlen($data['extra'])>0 ? $data['extra'] : "";
	$recipients = $this->getRecipients($id, $action,true);
	$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
	if(strlen($extra)>0 && $extra !== $recipients[$owner] && $extra !== $this->getUserID()){
		$recipients["Old ".$owner]=$recipients[$owner];
		$recipients["New ".$owner]=$extra;
		unset($recipients[$owner]);
	}
	$mailObj = new ASSIST_MODULE_EMAIL("Deliverable","CD");
	$smsObj = new ASSIST_SMS();
	foreach($recipients as $key=>$val){
		if(is_array($val)){
			foreach($val as $index=>$thing){
				if($thing != $this->getUserID()){
					$adds = $mailObj->getEmailAddresses(array($thing), array());
					$phone = $smsObj->getMobilePhones(array($thing));
					$adds['to']['mobile'] = $phone[$thing]['num'];
					$adds['to']['tkid'] = $thing;
					foreach($adds['to'][0] as $a=>$b){
						$adds['to'][$a] = $b;
					}
					unset($adds['to'][0]);
					$recipes[$key][] = $adds['to'];
				}
			}
		}else{
			if($val != $this->getUserID()){
				$adds = $mailObj->getEmailAddresses(array($val), array());
				$phone = $smsObj->getMobilePhones(array($val));
				$adds['to']['mobile'] = $phone[$val]['num'];
				$adds['to']['tkid'] = $val;
				foreach($adds['to'][0] as $a=>$b){
					$adds['to'][$a] = $b;
				}
				unset($adds['to'][0]);
				$recipes[$key] = $adds['to'];
			}
		}
	}
	return $recipes;
	}

	public function getRecipients($id,$action,$mode=false){
		$headObj = new RGSTR2_HEADINGS();
		switch (strtoupper($action)) {
			case 'UNLOCK':
			case 'APPROVE':
			case 'DECLINE':
			case 'UPDATE':
			case 'EDIT':
			case 'NEW':
				$sql = "SELECT " .  $this->getTableField()  . "_owner FROM ".$this->getTableName()." WHERE " .  $this->getTableField()  . "_id = $id ";
				//$tkid[] = $this->mysql_fetch_one_value($sql, "" .  $this->getTableField()  . "_owner");
				if($mode){
					//$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
					$owner = $headObj->getAHeadingNameByField("" .  $this->getTableField()  . "_owner");
					$tkid[$owner] = $this->mysql_fetch_one_value($sql, "" .  $this->getTableField()  . "_owner");
				}else{
					$tkid[] = $this->mysql_fetch_one_value($sql, "" .  $this->getTableField()  . "_owner");
				}
				$parent_id = $this->getParentID($id);
				$conObject = new RGSTR2_REGISTER();
				$rawCon = $conObject->getRawObject($parent_id);
				$manager_fld = $conObject->getManagerFieldName();
				$manager = $rawCon[$manager_fld];
				$ownerObj = new RGSTR2_CONTRACT_OWNER();
				$owner_fld = $conObject->getOwnerFieldName();
				$owner_id = $rawCon[$owner_fld];
				$owners = $ownerObj->getActiveAdminsForOwner($owner_id);
				if($mode){
					$title3 = ucwords($this->getObjectName("register"))." Owner";
					//$title3 = $headObj->getAHeadingNameByField("con_owner");
					foreach($owners as $key=>$val){
						$tkid[$title3][]=$key;
					}
				}else{
					foreach($owners as $key=>$val){
						$tkid[]=$key;
					}
				}
				$c_auth = $rawCon['register_authoriser'];
				if($mode){
					$title = ucwords($this->getObjectName("register"))." Manager";
					$title2 = ucwords($this->getObjectName("register"))." Authoriser";
					$tkid[$title] = $manager;
					$tkid[$title2] = $c_auth;
				}else{
					$tkid[]=$manager;
					$tkid[]=$c_auth;
				}
				break;
		}	
		return $tkid;
	}


	//Get main risk
    public function getMainRiskTableRow($register_id){
        $parent_object_id = $register_id;
        $list_items = $this->getDeliverablesWithDeadlineForParentSelect($parent_object_id);

        $select = '<select id="rsk_parent_id" name="rsk_parent_id" req="0">';
        foreach($list_items as $key => $val){
            $select .= '<option value="' . $key . '" full="' . $val['name'] . '" list_num="0" >' . $val['name'] . '</option>';
        }
        $select .= '</select>';

        $table_row_open = '<tr id="tr_rsk_parent_id" class="tr_rsk_type">';
        $table_head = '<th class="" width="40%">Main Risk:</th>';
        $table_data_open = '<td class="">';
        $table_data_close = '</td>';
        $table_row_close = '</tr>';

        $html = '';
        $html .= $table_row_open;
        $html .= $table_head;
        $html .= $table_data_open;
        $html .= $select;
        $html .= $table_data_close;
        $html .= $table_row_close;

        return $html;
    }
	 
	 
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>