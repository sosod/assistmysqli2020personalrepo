<?php
	class RGSTR2_STANDALONE{
		private $db;
		public function __construct($db){
			$this->db = $db;
		} 
		
		protected function getFormattedNames($type) {
			$names = array(); 
			$sql = "SELECT name_section as section, IF(LENGTH(name_client)>0,name_client,name_default) as name 
					FROM ".$this->db->getDBRef()."_setup_names
					WHERE (name_status & 32 = 32)";
			$rows = $this->db->mysql_fetch_all($sql);
			foreach($rows as $r){
				$s = explode("_",$r['section']);
				$names[end($s)] = $r['name'];
			}
			return $names;
		}
		
		public function getProfile($usr){
			$sql = "SELECT yr_strt, financial_year, table_rows FROM ".$this->db->getDBRef()."_userprofile WHERE up_tkid = $usr";
			$res = $this->db->mysql_fetch_all($sql);
			return $res;
		}
		
		public function performSearch($section,$user,$filter=""){
			switch ($section) {
				case '0':
				//Actions -> kk
					switch ($filter) {
						case '2':
							//case 1 = incomplete actions due this week
							$days_to_saturday = 6 - date("w");
							$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
							$days_to_sunday = date("w"); 
							$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
							$date = date('Y-m-d',$sat_cutoff);
							$date2 = date('Y-m-d',$sun_cutoff);
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_owner = $user AND action_status_id <> 3 AND action_progress < 100 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline <= '$date' AND action_deadline >= '$date2'";
							//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '3':
							//case 2 = incomplete actions due up to this week
							$days_to_saturday = 6 - date("w");
							$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
							$date = date('Y-m-d',$sat_cutoff);
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_owner = $user AND action_status_id <> 3 AND action_progress < 100 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline <= '$date' ";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '4':
							//case 3 = incomplete actions due today
							$date = date('Y-m-d');
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_owner = $user AND action_status_id <> 3 AND action_progress < 100 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline = '$date' ";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '5':
							//case 4 = overdue and incomplete actions due up to today
							$date = date('Y-m-d');
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_owner = $user AND action_status_id <> 3 AND action_progress < 100 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline <= '$date' ";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						
						default:
							//case 0 = all incomplete actions
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_owner = $user AND action_status_id <> 3 AND action_progress < 100 ";
							$res = $this->db->mysql_fetch_all($sql);
							break;
					}
					break;
				case '1':
				//Deliverables
					switch ($filter) {
						case '2':
						//k
							//case 1 = incomplete deliverables due this week
							$days_to_saturday = 6 - date("w");
							$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
							$days_to_sunday = date("w"); 
							$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
							$date = date('Y-m-d',$sat_cutoff);
							$date2 = date('Y-m-d',$sun_cutoff);
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND del_deadline <= '$date' AND del_deadline >= '$date2'";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '3':
						//k
							//case 2 = overdue and incomplete deliverables due this week
							$days_to_saturday = 6 - date("w");
							$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
							$date = date('Y-m-d',$sat_cutoff);
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND del_deadline <= '$date' ";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '4':
						//k
							//case 3 = incomplete deliverables due today
							$date = date('Y-m-d');
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND del_deadline = '$date' ";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '5':
						//k
							//case 4 = overdue and incomplete deliverables due up to today
							$date = date('Y-m-d');
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND del_deadline <= '$date' ";
						//return $sql;
							$res = $this->db->mysql_fetch_all($sql);
							break;
						case '6':
						//k
							//case 5 = incomplete actions under your deliverable
							$sql = "SELECT del_id FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE;
							$del_ids = $this->db->mysql_fetch_all_by_value($sql,"del_id");
							$sql2 = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_deliverable_id IN (".implode(",",$del_ids).") AND action_status_id <> 3 AND action_progress < 100 ";
						//return $sql2;
							$res = $this->db->mysql_fetch_all($sql2);
							break;
						case '7':
						//k
							//case 6 = actions under your deliverable due this week
							$days_to_saturday = 6 - date("w");
							$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
							$days_to_sunday = date("w"); 
							$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
							$date = date('Y-m-d',$sat_cutoff);
							$date2 = date('Y-m-d',$sun_cutoff);
							$sql = "SELECT del_id FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE;
							$del_ids = $this->db->mysql_fetch_all_by_value($sql,"del_id");
							$sql2 = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_deliverable_id IN (".implode(",",$del_ids).") AND action_status_id <> 3 AND action_deadline <= '$date' AND action_deadline >= '$date2'";
						//return $sql2;
							$res = $this->db->mysql_fetch_all($sql2);
							break;
						case '8':
						//k
							//case 7 = incomplete actions under your deliverable due this week
							$days_to_saturday = 6 - date("w");
							$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
							$days_to_sunday = date("w"); 
							$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
							$date = date('Y-m-d',$sat_cutoff);
							$date2 = date('Y-m-d',$sun_cutoff);
							$sql = "SELECT del_id FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE;
							$del_ids = $this->db->mysql_fetch_all_by_value($sql,"del_id");
							$sql2 = "SELECT * FROM ".$this->db->getDBRef()."_action WHERE action_deliverable_id IN (".implode(",",$del_ids).") AND action_status_id <> 3 AND action_progress < 100 AND action_deadline <= '$date' AND action_deadline >= '$date2'";
						//return $sql2;
							$res = $this->db->mysql_fetch_all($sql2);
							break;
						
						default:
						//k
							//case 0 = all incomplete deliverables
							$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_owner = $user AND del_status_id <> 3 AND del_progress < 100 ";
							$res = $this->db->mysql_fetch_all($sql);
						//return $sql;	
							break;
					}		
					break;
				case '2':
				switch($filter){
					case '2':
					//k
						//case 1 = incomplete deliverables under your contracts due this week
						$days_to_saturday = 6 - date("w");
						$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
						$days_to_sunday = date("w"); 
						$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
						$date = date('Y-m-d',$sat_cutoff);
						$date2 = date('Y-m-d',$sun_cutoff);
						$sql_man = "SELECT contract_id FROM ".$this->db->getDBRef()."_contract WHERE contract_manager = $user AND contract_status_id <> 3 AND contract_progress < 100 AND (contract_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE;
						$man_cons = $this->db->mysql_fetch_all_by_value($sql_man,"contract_id");
						$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_contract_id IN(".implode(",",$man_cons).") AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND del_deadline <= '$date' AND del_deadline >= '$date2'";
						$res = $this->db->mysql_fetch_all($sql);
					//return $sql;
						break;
					default:
					//k
						//case 0 = all incomplete deliverables
						$sql_man = "SELECT contract_id FROM ".$this->db->getDBRef()."_contract WHERE contract_manager = $user AND contract_status_id <> 3 AND contract_progress < 100 AND (contract_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE;
						$man_cons = $this->db->mysql_fetch_all_by_value($sql_man,"contract_id");
						$sql = "SELECT * FROM ".$this->db->getDBRef()."_deliverable WHERE del_contract_id IN(".implode(",",$man_cons).") AND del_status_id <> 3 AND del_progress < 100 AND (del_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE;
					//return $sql;
						$res = $this->db->mysql_fetch_all($sql);
						break;
				}
				break;
			}
			return $res;
			
		/*
			
			
		switch (strtoupper($filter)) {
			case 'ALL':
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." ORDER BY action_deadline ASC";
				break;
			case 'THISWEEK':
				$days_to_saturday = 6 - date("w");
				$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
				$days_to_sunday = date("w"); 
				$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
				$date = date('Y-m-d',$sat_cutoff);
				$date2 = date('Y-m-d',$sun_cutoff);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline <= '$date' AND action_deadline >= '$date2' ORDER BY action_deadline ASC";
				//return array($date,$date2);
				break;
			case 'OVERTHISWEEK':
				$days_to_saturday = 6 - date("w");
				$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
				$date = date('Y-m-d',$sat_cutoff);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline <= '$date' ORDER BY action_deadline ASC";
				break;
			case 'TODAY':
				$now = strtotime('now');
				$date = date('Y-m-d', $now);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline = '$date' ORDER BY action_id DESC";
				break;
			case 'OVERTODAY':
				$now = strtotime('now');
				$date = date('Y-m-d', $now);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." AND action_deadline <= '$date' ORDER BY action_id DESC";
				break;
			default:
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".RGSTR2::ACTIVE.") = ".RGSTR2::ACTIVE." ORDER BY action_deadline ASC";
				break;
		}
			
		$res = $this->mysql_fetch_all($sql);
		return $res;
		*/
		
		}
		
		
		
	}
?>