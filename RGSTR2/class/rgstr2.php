<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */

class RGSTR2 extends RGSTR2_HELPER {
    /***
     * Module Wide variables
     */	
	protected $object_form_extra_js = "";
	
	protected $deliverable_types = array(
		'DEL'=>"|risk| With |actions|",
		'MAIN'=>"|risk| With Sub-|risks|",
		'SUB'=>"Sub-|risk| With |actions|",
	);
	
	protected $copy_function_protected_fields = array("contract_have_subdeliverables","rsk_type","rsk_parent_id");
	protected $copy_function_protected_heading_types = array("ATTACH","DEL_TYPE");
	
	protected $has_target = false;
	
    /**
     * Module Wide Constants
     * */
    const SYSTEM_DEFAULT = 1;
    const ACTIVE = 2;
    const INACTIVE = 4;
    const DELETED = 8;
	
	const AWAITING_APPROVAL = 16;
	const APPROVED = 128;
	
	
	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct() {
		parent::__construct();
		$this->deliverable_types = $this->replaceObjectNames($this->deliverable_types);
	}






	


/*********************
 * MODULE OBJECT functions
 * for CONTRACT, DELIVERABLE AND ACTION
 */
	public function getObject($var) { //$this->arrPrint($var);
//        echo '<pre style="font-size: 18px">';
//        echo '<p>GET_OBJECT_VAR</p>';
//        print_r($var);
//        echo '</pre>';

		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				$data = $this->getList($var['section'],$options);//section = MANAGE... $options = $var['type'] = LIST
				if($var['type']=="LIST" || $var['type']=="ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data,$this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details)>0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'],$var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'],$var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}
	public function getStatusFieldName() {
		return $this->status_field;
	}
	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}
	public function getProgressFieldName() {
		return $this->progress_field;
	}
	public function getIDFieldName() {
		return $this->id_field;
	}
	public function getParentFieldName() {
		return $this->parent_field;
	}
	public function getNameFieldName() {
		return $this->name_field;
	}
	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}
	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}
	public function getOwnerFieldName() {
		return $this->owner_field;
	}
	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}
	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}
	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}
	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}
	
	public function getPageLimit() {
		$profileObject = new RGSTR2_PROFILE();
		return $profileObject->getProfileTableLength();
	}
	
	public function getDateOptions($fld) {
		if(stripos($fld,"action_on")!==FALSE) { $fld = "action_on"; }
		switch($fld) {
			case "action_on":
				return array('maxDate'=>"'+0D'");
				break;
			default:
				return array();
		}
	}
	
	public function getExtraObjectFormJS() { return $this->object_form_extra_js; }

	public function getAllDeliverableTypes() { return $this->deliverable_types; }
	
	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key=>$t){
			$arr[]=$t;
		}
		return $arr;
	}
	
	public function getDeliverableTypes($contract_id) {
		$contractObject = new RGSTR2_REGISTER($contract_id);
		if($contractObject->doIHaveSubDeliverables()==FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types; 
	}
	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt,$ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE",$options);
		//$this->arrPrint($data);
		$result = array(
			'past'=>0,
			'present'=>0,
			'future'=>0,
		);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z<$now) {
				$result['past']++; //echo "past";
			} elseif($z>$now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE",$options);
		return $data;
	}

	public function getMyList($obj_type,$section,$options=array()) {
		//echo "<P>section: ".$section;
		//echo "<P>options: "; $this->arrPrint($options);
		if(isset($options['page'])) {
			$page = strtoupper($options['page']);
			unset($options['page']);
		} else {
			$page = "DEFAULT";
		}

		$trigger_rows = true; 
		if(isset($options['trigger'])) {
			$trigger_rows = $options['trigger'];
			unset($options['trigger']);
		}
		//echo $page;
		if($page=="LOGIN_STATS" || $page=="LOGIN_TABLE") {
			$future_deadline = $options['deadline'];
			unset($options['deadline']);
			if(!isset($options['limit'])) { $options['limit'] = false; }
		}
		$compact_details = ($page == "CONFIRM" || $page == "ALL");
		if(isset($options['limit']) && ($options['limit'] === false || $options['limit']=="ALL")){
			$pagelimit = false;
			$start = 0;
			$limit = false;
			unset($options['start']);
			unset($options['limit']);
		}else{
			$pagelimit = $this->getPageLimit();
			$start = isset($options['start']) ? $options['start'] : 0;  unset($options['start']);
			$limit = isset($options['limit']) ? $options['limit'] : $pagelimit;	unset($options['limit']);
			if($start<0) {
				$start = 0;
				$limit = false;
			}
		}	
		$order_by = ""; //still to be processed
		$left_joins = array();
		
		$headObject = new RGSTR2_HEADINGS();
		$final_data = array('head'=>array(),'rows'=>array());
		
		//set up contract variables
		$register_headings = $headObject->getMainObjectHeadings("REGISTER",($obj_type=="REGISTER"?"LIST":"FULL"),$section);

		$register_Object = new RGSTR2_REGISTER();
		$register_tblref_alias = "REGISTER";
		$register_table = $register_Object->getTableName();
		$register_id = $register_tblref_alias.".".$register_Object->getIDFieldName();
		$register_status = $register_tblref_alias.".".$register_Object->getStatusFieldName();
		$register_status_fld = $register_tblref_alias.".".$register_Object->getProgressStatusFieldName();
		$register_name = $register_Object->getNameFieldName();
		$register_deadline = $register_Object->getDeadlineFieldName();
		$register_field = $register_tblref_alias.".".$register_Object->getTableField();
		
		$where = (!($page == 'RESTORE_CONTRACT') ? $register_Object->getActiveStatusSQL($register_tblref_alias) : $register_Object->getStatusSQL("DELETED",$register_tblref_alias,true) ); // (( C.contract_status & 8) <> 8 AND ( C.contract_status & 2) = 2)

        $risk_tblref_alias = 'RISK';
		$risk_Object = new RGSTR2_RISK();
		$risk_table = $risk_Object->getTableName(); //Dev - 01 August 2017 - Currently: "assist_testcon_rgstr2_risk"
		$risk_id = $risk_tblref_alias.".".$risk_Object->getIDFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_id"
		$risk_status = $risk_tblref_alias.".".$risk_Object->getStatusFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_status"
		$register_id_as_risk_parent = $risk_tblref_alias.".".$risk_Object->getParentFieldName(); // 31 July 2017 - Currently still connected to contract, but will connect to the register table once  have all the risk functionality down

		$action_tblref_alias = "ACTION";
		$action_Object = new RGSTR2_ACTION();
		$action_table = $action_Object->getTableName(); //Dev - 01 August 2017 - Currently: assist_testcon_rgstr2_action
		$action_id = $action_tblref_alias.".".$action_Object->getIDFieldName(); //Dev - 01 August 2017 - Currently: ACTION.action_id
		$action_status = $action_tblref_alias.".".$action_Object->getStatusFieldName(); //Dev - 01 August 2017 - Currently: ACTION.action_status
		$risk_id_as_action_parent = $action_tblref_alias.".".$action_Object->getParentFieldName(); // 01 August 2017 - Still called "action_deliverable_id" , to be changed to action_risk_id later
		$action_progress = $action_tblref_alias.".".$action_Object->getProgressFieldName(); //Dev - 01 August 2017 - Currently: "ACTION.action_progress"
		$action_progress2 = $action_Object->getProgressFieldName(); //Dev - 01 August 2017 - Currently: action_progress


		//if type is something other than contract
		if($obj_type!="CONTRACT" && $obj_type!="REGISTER") {
			//setup risk variables
//			$risk_headings = $headObject->getMainObjectHeadings("DELIVERABLE",($obj_type=="DELIVERABLE" ? "LIST" : "FULL"),$section);
			$risk_headings = $headObject->getMainObjectHeadings("RISK",($obj_type=="RISK" ? "LIST" : "FULL"),$section);

            $all_risk_headings = $headObject->getMainObjectHeadings("RISK","FULL",$section);

            //RSK OWNER STUFF
            $rsk_owner_id_field = 'rsk_owner_id';
            $risk_headings[$rsk_owner_id_field] = $all_risk_headings[$rsk_owner_id_field];


			$action_status_fld = $risk_tblref_alias . '.' . $risk_Object->getProgressStatusFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_status_id"
			$risk_name = $risk_Object->getNameFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_name"
			$risk_deadline = $risk_Object->getDeadlineFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_deadline", this is going to die soon
			$risk_field = $risk_tblref_alias . '.' . $risk_Object->getTableField(); //Dev - 01 August 2017 - Currently: "RISK.rsk"


			$where .= ' AND ' . (!($page == 'RESTORE_RISK') ? $risk_Object->getActiveStatusSQL($risk_tblref_alias) : $risk_Object->getStatusSQL("DELETED",$risk_tblref_alias,true) );
            // I'll get back to what this is later

			//setup action variables
			if($obj_type=="ACTION") {
//			    echo '<pre style="font-size: 18px">';
//                echo '<p>HERE WE ARE</p>';
//                print_r('ONE');
//                echo '</pre>';

				$action_status_fld = $action_tblref_alias . '.' . $action_Object->getProgressStatusFieldName(); //Dev - 01 August 2017 - Currently: ACTION.action_status_id
				$action_name = $action_Object->getNameFieldName(); //Dev - 01 August 2017 - Currently: "action_name"
				$action_deadline = $action_Object->getDeadlineFieldName(); //Dev - 01 August 2017 - Currently: "action_deadline"
				$action_field = $action_tblref_alias . '.' . $action_Object->getTableField(); //Dev - 01 August 2017 - Currently: "ACTION.action"
				$action_headings = $headObject->getMainObjectHeadings("ACTION","LIST",$section);
				$id_field_name = $action_Object->getIDFieldName(); //Dev - 01 August 2017 - Currently: "action_id"

                //I'll get to the rest of this stuff when i start working on actions

				if(!$compact_details){
					$sql ="SELECT DISTINCT $action_status as my_status, ".$action_tblref_alias.".*
					, IF(".$action_tblref_alias.".action_target>0,CONCAT(".$action_tblref_alias.".action_target,' ',".$action_tblref_alias.".action_target_unit),'N/A') as action_target
					, IF(".$action_tblref_alias.".action_actual>0,CONCAT(".$action_tblref_alias.".action_actual,' ',".$action_tblref_alias.".action_target_unit),IF(".$action_tblref_alias.".action_target>0,CONCAT('0 ',".$action_tblref_alias.".action_target_unit),'N/A')) as action_actual
					, CONCAT(".$risk_tblref_alias.".".$risk_name.",' [".$risk_Object->getRefTag()."',".$risk_id.",']') as ".$risk_name." 
					, CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name;
				}else{
					$sql ="SELECT DISTINCT $action_status as my_status, ".$action_tblref_alias.".* ";
				}
				$from = " $action_table $action_tblref_alias INNER JOIN $risk_table $risk_tblref_alias ON $risk_id = $risk_id_as_action_parent INNER JOIN $register_table $register_tblref_alias ON $register_id = $register_id_as_risk_parent ";

                $where .= ' AND ' . (!($page == 'RESTORE_ACTION') ? $action_Object->getActiveStatusSQL($action_tblref_alias) : $action_Object->getStatusSQL("DELETED",$action_tblref_alias,true) );

				$final_data['head'][$id_field_name] = $action_headings[$id_field_name];
				if(!$compact_details){
					$final_data['head'][$register_name] = $register_headings[$register_name];
					$final_data['head'][$risk_name] = $risk_headings[$risk_name];
				}
				foreach($action_headings as $fld=>$head){
					if($fld!=$id_field_name) {
						$final_data['head'][$fld] = $head;
					}
				}
				$where_tblref = $action_tblref_alias;
				$where_deadline = $action_deadline;
				$ref_tag = $action_Object->getRefTag();
				if($page=="LOGIN_TABLE") {
					$order_by = $action_tblref_alias.".".$action_deadline;
				}
				$status_id_fld = $action_Object->getProgressStatusFieldName();
				$where_status_id_fld = $action_tblref_alias.".".$action_Object->getProgressStatusFieldName();
				$where_status_fld = $action_tblref_alias.".".$action_Object->getStatusFieldName();
			} else {
//                echo '<pre style="font-size: 18px">';
//                echo '<p>HERE WE ARE</p>';
//                print_r('TWO');
//                echo '</pre>';

			    //Delete this one and use the one I've commented out once I get it to work
				$sql ="SELECT DISTINCT $risk_status as my_status
						, ".$risk_tblref_alias.".* 
						, AVG(".$action_progress.") as ".$action_progress2."
						, CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name;
                /*
                 * Translation:
                 * SELECT DISTINCT RISK.rsk_status as my_status
                 * , RISK.*
                 * , AVG(ACTION.action_progress) as action_progress
                 * , CONCAT(C.contract_name,' [C',C.contract_id,']') as contract_name
                 */

//                $sql = 'SELECT DISTINCT ' . $risk_status . ' as my_status, ';
//                $sql .= $risk_tblref_alias . '.*, ';
//                $sql .= 'AVG(' . $action_progress . ') as ' . $action_progress2 . ' ';
//                $sql .= 'CONCAT(' . $register_tblref_alias . '.' . $register_name . ',\' [' . $register_Object->getRefTag() . '\',' . $register_id . ',\']\') as ' . $register_name . ' ' ;
//
//                /*
//                 * Translation:
//                 * SELECT DISTINCT RISK.rsk_status as my_status,
//                 * RISK.*,
//                 * AVG(ACTION.action_progress) as action_progress,
//                 * CONCAT(C.contract_name,' [C',C.contract_id,']') as contract_name,
//                 */

				$action_headings = array();

                //Delete this one and use the one I've commented out once I get it to work
				$from = " $risk_table $risk_tblref_alias 
						INNER JOIN $register_table $register_tblref_alias ON $register_id = $register_id_as_risk_parent 
						LEFT OUTER JOIN $action_table $action_tblref_alias
						  ON $risk_id_as_action_parent = $risk_id AND ( $action_status & ".RGSTR2::ACTIVE." ) = ".RGSTR2::ACTIVE."
				";
                /*
                 * Translation:
                 * assist_testcon_rgstr2_risk RISK
                 * INNER JOIN assist_testcon_rgstr2_contract C ON C.contract_id = RISK.rsk_contract_id
                 * LEFT OUTER JOIN assist_testcon_rgstr2_action ACTION
                 *  ON ACTION.action_deliverable_id = RISK.rsk_id AND ( ACTION.action_status & 2 ) = 2
                 */

//                $from = $risk_table . $risk_tblref_alias . ' ';
//                $from .= 'INNER JOIN ' . $register_table . $register_tblref_alias . ' ON ' . $register_id . ' = ' . $register_id_as_risk_parent . ' ';
//                $from .= 'LEFT OUTER JOIN ' . $action_table . $action_tblref_alias . ' ';
//                $from .= 'ON ' . $risk_id_as_action_parent . ' = ' . $risk_id . ' ';
//                $from .= 'AND ( ' . $action_status . ' & ' . RGSTR2::ACTIVE . ' ) = ' . RGSTR2::ACTIVE . ' ';
//
//                /*
//                 * Translation:
//                 * assist_testcon_rgstr2_risk RISK
//                 * INNER JOIN assist_testcon_rgstr2_contract C ON C.contract_id = RISK.rsk_contract_id
//                 * LEFT OUTER JOIN assist_testcon_rgstr2_action ACTION
//                 * ON ACTION.action_deliverable_id = RISK.rsk_id
//                 * AND ( ACTION.action_status & 2 ) = 2
//                 */


				$id_field_name = $risk_Object->getIDFieldName(); //Dev - 01 August 2017 - Currently: "rsk_id"

				$final_data['head'][$id_field_name] = $risk_headings[$id_field_name];
				$final_data['head'][$register_name] = $register_headings[$register_name];

				foreach($risk_headings as $fld => $head){
					if($fld != $id_field_name) {
						$final_data['head'][$fld] = $head;
					}
				}

				$where_tblref = $risk_tblref_alias; //Dev - 01 August 2017 - Currently: "RISK"
				$where_deadline = 'rsk_id'; //Dev - 01 August 2017: because risks don't have deadlines
				$ref_tag = $risk_Object->getRefTag(); //Dev - 01 August 2017 - Currently: "RR" for Register Risk

				if($page=="LOGIN_TABLE") {
					$order_by = $risk_tblref_alias.".".$risk_deadline;
				}

				$where_status_id_fld = $risk_tblref_alias.".".$risk_Object->getProgressStatusFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_deadline"
				$status_id_fld = $risk_Object->getProgressStatusFieldName(); //Dev - 01 August 2017 - Currently: "rsk_status_id"
				$where_status_fld = $risk_tblref_alias.".".$risk_Object->getStatusFieldName(); //Dev - 01 August 2017 - Currently: "RISK.rsk_status_id"
				$group_by = "GROUP BY ".$risk_id;
			}
		} else {

//            echo '<pre style="font-size: 18px">';
//            echo '<p>HERE WE ARE</p>';
//            print_r('THREE');
//            echo '</pre>';

			$id_field_name = $register_Object->getIDFieldName();

			$sql = "SELECT DISTINCT $register_status as my_status,";
            $sql .=" ".$register_tblref_alias.".*, AVG(".$action_progress.") as ".$action_progress2;

			$risk_headings = array();
			$action_headings = array();
			$from = " $register_table $register_tblref_alias 
			LEFT OUTER JOIN $risk_table $risk_tblref_alias
			  ON $register_id_as_risk_parent = $register_id AND ( $risk_status & ".RGSTR2::ACTIVE." ) = ".RGSTR2::ACTIVE."
			LEFT OUTER JOIN $action_table $action_tblref_alias
			  ON $risk_id_as_action_parent = $risk_id AND ( $action_status & ".RGSTR2::ACTIVE." ) = ".RGSTR2::ACTIVE."
			";
			$final_data['head'] = $register_headings;
			$where_tblref = $register_tblref_alias;
			$where_deadline = 'register_id';//Because the deadline is dead now
			$ref_tag = $register_Object->getRefTag();
			if($page=="LOGIN_TABLE") {
				$order_by = $register_tblref_alias.".".$register_deadline;
			}
			$where_status_id_fld = $register_tblref_alias.".".$register_Object->getProgressStatusFieldName();
			$status_id_fld = $register_Object->getProgressStatusFieldName();
			$where_status_fld = $register_tblref_alias.".".$register_Object->getStatusFieldName();
			$group_by = "GROUP BY ".$register_id;
		}

		if(count($options)>0) {
			foreach($options as $key => $filter) {
				if(substr($key,0,3)!=substr($this->getTableField(),0,3) && strrpos($key,".")===FALSE) {
					$key = $this->getTableField()."_".$key;
				}
				$where.= " AND ".(strrpos($key,".")===FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
			}
		}


			$all_headings = array_merge($register_headings,$risk_headings,$action_headings);

			$listObject = new RGSTR2_LIST();


			foreach($all_headings as $fld => $head) {
				$left_join_tblref_alias = substr($head['section'],0,1);

                //Register/Contract hack
                $left_join_tblref_alias = ($left_join_tblref_alias == 'C' ? 'REGISTER' : $left_join_tblref_alias); //Tshego Hack
                $left_join_tblref_alias = ($left_join_tblref_alias == 'R' ? 'RISK' : $left_join_tblref_alias); //Tshego Hack

                //Risk/Deliverable hack
                $left_join_tblref_alias = ($left_join_tblref_alias == 'D' ? 'RISK' : $left_join_tblref_alias); //Tshego Hack
                $left_join_tblref_alias = ($left_join_tblref_alias == 'R' ? 'RISK' : $left_join_tblref_alias); //Tshego Hack

                //Action Hack
                $left_join_tblref_alias = ($left_join_tblref_alias == 'A' ? 'ACTION' : $left_join_tblref_alias); //Tshego Hack

                //One more Hack
                if(substr($head['section'],0,1) == 'R'){
                    if(substr($head['section'],0,3) == 'REG'){
                        $left_join_tblref_alias = 'REGISTER';
                    }else{
                        $left_join_tblref_alias = 'RISK';
                    }
                }


                if($head['type']=="LIST") {
                    /*
                     * Alright, so we're going to use the Risk Level Setup Heading
                     * record as the example that's going to help us understand what this section of code does
                     * and how it all ties in to the overall sql statement that results to give us the data that we're looking for
                     */

					$listObject->changeListType($head['list_table']);

					$sql .= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
                    /*
                     * Translation Example:
                     * , risk_level.name as risk_level
                     */

                    //This is for the colour functionality
                    if($head['field'] == 'rsk_impact' || $head['field'] == 'rsk_likelihood' || $head['field'] == 'rsk_inherent_exposure' || $head['field'] == 'rsk_control_effectiveness' || $head['field'] == 'rsk_residual_exposure' || $head['field'] == 'rsk_financial_exposure'){
                        $sql .= ', ' . $head['list_table'] . '.colour as ' . $head['list_table'] . '_colour';
                    }

                    $left_join_sql = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$left_join_tblref_alias.".".$fld." 
										AND (".$head['list_table'].".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
                    /*
                     * Translation Example:
                     * LEFT OUTER JOIN assist_testcon_rgstr2_list_risk_level
                     * AS risk_level
                     * ON risk_level.id = RISK.rsk_level
                     * AND (risk_level.status & 8) <> 8
                     */

                    $left_joins[] = $left_join_sql;

				} elseif($head['type']=="MASTER") {

					$tbl = $head['list_table'];
					$masterObject = new RGSTR2_MASTER($fld);
					$fy = $masterObject->getFields();
					$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$left_join_tblref_alias.".".$fld." = ".$fld.".".$fy['id'];

				} elseif($head['type']=="USER") {

					$sql .= ", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
                    /*
                     * Translation Example:
                     * , CONCAT(contract_manager.tkname,' ',contract_manager.tksurname) as contract_manager_tkid
                     */

                    $left_join_sql = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." 
                                        ON ".$fld.".tkid = ".$left_join_tblref_alias.".".$fld." 
                                        AND ".$fld.".tkstatus = 1";
                    /*
                     * Translation Example:
                     * INNER JOIN assist_testcon_timekeep rsk_owner
                     * ON rsk_owner.tkid = RISK.rsk_owner
                     * AND rsk_owner.tkstatus = 1
                     */

                    $left_joins[] = $left_join_sql;

				} elseif($head['type']=="OWNER") {

				    $ownerObject = new RGSTR2_CONTRACT_OWNER();
					$tbl = $head['list_table'];
					$dir_tbl = $fld."_parent";
					$sub_tbl = $fld."_child";
					$own_tbl = $fld;

					$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
                    /*
                     * Translation Example:
                     * , CONCAT(contract_owner_id_parent.dirtxt, ' - ',contract_owner_id_child.subtxt) as contract_owner
                     */

					$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." 
					                ON ".$left_join_tblref_alias.".".$fld." = ".$sub_tbl.".subid";
                    /*
                     * Translation Example:
                     * LEFT OUTER JOIN assist_testcon_list_dirsub AS contract_owner_id_child
                     * ON C.contract_owner_id = contract_owner_id_child.subid
                     */
					$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." 
					                ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
                    /*
                     * Translation Example:
                     * INNER JOIN assist_testcon_list_dir AS contract_owner_id_parent
                     * ON contract_owner_id_child.subdirid = contract_owner_id_parent.dirid
                     */
					$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." AS ".$own_tbl." 
					                ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
                    /*
                     * Translation Example:
                     * LEFT OUTER JOIN assist_testcon_rgstr2_list_contract_owner AS contract_owner_id
                     * ON contract_owner_id_child.subid = contract_owner_id.owner_subid
                     */

				}
			}

			$sql .= " FROM " . $from . implode(" ",$left_joins);
			$sql .= " WHERE " . $where;


//        $ownerObject = new RGSTR2_CONTRACT_OWNER();
//        $owner_items = $ownerObject->getActiveOwners();
//
//        $active_admins = $ownerObject->getAllActiveAdmins();
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>OWNERITEMS</p>';
//        print_r($owner_items);
//        echo '</pre>';
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>ACTIVE ADMINS</p>';
//        print_r($active_admins);
//        echo '</pre>';

			switch($section) {
				case "ACTIVE":
					$sql.=" AND ".$register_Object->getActiveStatusSQL("REGISTER");
					if($obj_type=="ACTION" || $obj_type=="RISK") {
						$sql.=" AND ".$risk_Object->getActiveStatusSQL($risk_tblref_alias);
					}
					if($obj_type=="ACTION") {
						$sql.=" AND ".$action_Object->getActiveStatusSQL($action_tblref_alias);
					}
					break;
				case "NEW":
					if($page=="ACTIVATE_WAITING") {
						$sql.=" AND ".$register_Object->getActivationStatusSQL("REGISTER");
					} elseif($page=="ACTIVATE_DONE") {
						$sql.=" AND ".$register_Object->getReportingStatusSQL("REGISTER");
					} elseif($page=="COPY_CONTRACT") {
						$sql.=" AND ".$register_Object->getActiveStatusSQL("REGISTER");
					} else {
						$sql.=" AND ".$register_Object->getNewStatusSQL("REGISTER");
						if($obj_type=="ACTION" || $obj_type=="RISK") {
							$sql.=" AND ".$risk_Object->getActiveStatusSQL($risk_tblref_alias);
						}
						if($obj_type=="ACTION") {
							$sql.=" AND ".$action_Object->getActiveStatusSQL($action_tblref_alias);
						}
					}
					switch($page) {
						case "COPY_CONTRACT":
							break;
						case "EDIT_CONTRACT":
						case "EDIT":
						case "CONFIRM":
                        case "ADD_DELIVERABLE":
                        case "ADD_ACTION":
							$sql.= " AND ".$register_tblref_alias.".register_manager = '".$this->getUserID()."'";
							break;
						case "ACTIVATE_WAITING":
						case "ACTIVATE_DONE":
							$sql.= " AND ".$register_tblref_alias.".register_authoriser = '".$this->getUserID()."'";
							break;
						case "DEFAULT":
						default:
							break;
					}
					break;
				case "MANAGE":
					$sql.=($obj_type=="DELIVERABLE" ? "AND ".$risk_tblref_alias.".rsk_type <> 'MAIN'" : "")." AND ".$register_Object->getReportingStatusSQL($register_tblref_alias);
					if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
						$sql.=" AND ".$risk_Object->getReportingStatusSQL($risk_tblref_alias);
					}
					if($obj_type=="ACTION") {
						$sql.=" AND ".$action_Object->getReportingStatusSQL($action_tblref_alias);
					}
					switch($page) {
						case "UPDATE_CONTRACT":		$owner_status = RGSTR2_CONTRACT_OWNER::CAN_UPDATE;
						case "UPDATE_REGISTER":		$owner_status = RGSTR2_CONTRACT_OWNER::CAN_UPDATE;
						case "EDIT_DELIVERABLE":
						case "EDIT_RISK":
						case "APPROVE_DELIVERABLE":	$owner_status = !isset($owner_status) ? RGSTR2_CONTRACT_OWNER::CAN_APPROVE : $owner_status;
						case "APPROVED_DELIVERABLE":	$owner_status = !isset($owner_status) ? RGSTR2_CONTRACT_OWNER::CAN_APPROVE : $owner_status;


                            $sql.= " AND (
										".$register_tblref_alias.".register_manager = '".$this->getUserID()."'
										OR ".$register_tblref_alias.".register_authoriser = '".$this->getUserID()."'";

                            if(isset($owner_status) && isset($own_tbl)){
                                $sql.= "OR (
                                            ".$own_tbl.".owner_tkid = '".$this->getUserID()."'
                                            AND ( (".$own_tbl.".owner_status & ".$owner_status.") = ".$owner_status." )
                                        )";
                            }

                            $sql.= ")";

							if($page=="APPROVE_DELIVERABLE") {
								$sql.=" AND ( (".$where_status_fld." & ".RGSTR2::AWAITING_APPROVAL.") = ".RGSTR2::AWAITING_APPROVAL." ) ";
							} elseif($page=="APPROVED_DELIVERABLE") {
								$sql.=" AND ( (".$where_status_fld." & ".RGSTR2::APPROVED.") = ".RGSTR2::APPROVED." ) ";
							} else {
								$sql.=" AND ( (".$where_status_fld." & ".RGSTR2::APPROVED.") <> ".RGSTR2::APPROVED." ) ";
							}
							break;
						case "ASSESSMENT":
						case "FINANCE":
							$sql.= " AND ".$register_tblref_alias.".register_manager = '".$this->getUserID()."'";
							break;
						case "UPDATE_DELIVERABLE":
						case "UPDATE_RISK":
                            $owner_status = RGSTR2_CONTRACT_OWNER::CAN_UPDATE;

                            $sql.= " AND (
										".$risk_tblref_alias.".rsk_owner = '".$this->getUserID()."'
										OR (
											".$own_tbl.".owner_tkid = '".$this->getUserID()."'
											AND ( (".$own_tbl.".owner_status & ".$owner_status.") = ".$owner_status." )
										)
									 )";

                            break;
						case "APPROVE_ACTION": $owner_status = RGSTR2_CONTRACT_OWNER::CAN_APPROVE;
						case "APPROVED_ACTION": $owner_status = RGSTR2_CONTRACT_OWNER::CAN_APPROVE;
						case "EDIT_ACTION": $owner_status = RGSTR2_CONTRACT_OWNER::CAN_EDIT;

							$sql.= " AND ".$risk_tblref_alias.".rsk_owner = '".$this->getUserID()."'";

                        $sql.= " AND ( 
										".$register_tblref_alias.".register_manager = '".$this->getUserID()."' 
										OR ".$risk_tblref_alias.".rsk_owner = '".$this->getUserID()."'";

                        if(isset($owner_status)){
                            $sql.= "OR (
                                            ".$own_tbl.".owner_tkid = '".$this->getUserID()."'
                                            AND ( (".$own_tbl.".owner_status & ".$owner_status.") = ".$owner_status." )
                                        )";
                        }

                        $sql.= ")";


							if($page=="APPROVE_ACTION") {
								$sql.=" AND ( (".$where_status_fld." & ".RGSTR2::AWAITING_APPROVAL.") = ".RGSTR2::AWAITING_APPROVAL." ) ";
							} elseif($page=="APPROVED_ACTION") {
                                $sql.=" AND ( (".$where_status_fld." & ".RGSTR2::APPROVED.") = ".RGSTR2::APPROVED." ) ";
							} else {
								$sql.=" AND ( (".$where_status_fld." & ".RGSTR2::APPROVED.") <> ".RGSTR2::APPROVED." ) 
										AND ( (".$where_status_fld." & ".RGSTR2::AWAITING_APPROVAL.") <> ".RGSTR2::AWAITING_APPROVAL.") ";
							}
							break;
						case "UPDATE_ACTION":
							$sql.= " AND ".$action_tblref_alias.".action_owner = '".$this->getUserID()."'
									AND ( (".$action_status." & ".RGSTR2::AWAITING_APPROVAL.") <> ".RGSTR2::AWAITING_APPROVAL." ) 
									AND ( (".$action_status." & ".RGSTR2::APPROVED.") <> ".RGSTR2::APPROVED." ) ";
							break;
						case "LOGIN_TABLE":
						case "LOGIN_STATS":
							//all objects not completed where you are the owner for updating
							switch($obj_type) {
								case "ACTION":
									$where_deadline = $action_deadline;
									$where_status = $action_status_fld;
									$sql.= " AND ".$action_tblref_alias.".action_owner = '".$this->getUserID()."'";
									break;
								case "RISK":
									$where_deadline = $risk_deadline;
									$sql.= " AND ".$risk_tblref_alias.".rsk_owner = '".$this->getUserID()."'";
									$where_status = $risk_status_fld;
									break;
								case "REGISTER":
									$where_deadline = $register_deadline;
									$sql.= " AND ( 
										".$register_tblref_alias.".register_manager = '".$this->getUserID()."' 
										OR ".$register_tblref_alias.".register_authoriser = '".$this->getUserID()."' 
										OR (
												".$own_tbl.".owner_tkid = '".$this->getUserID()."' 
												AND ( (".$own_tbl.".owner_status & ".RGSTR2_CONTRACT_OWNER::CAN_UPDATE.") = ".RGSTR2_CONTRACT_OWNER::CAN_UPDATE.")  
											)
										)";
									$where_status = $register_status_fld;
									break;
							}
							$sql.= " AND ".$where_status." <> 3 AND ".$where_tblref.".".$where_deadline." < '".date("Y-m-d",$future_deadline)."' ";
							break;
						case "VIEW":
						default:
							break;
					}
					break;
				case "ADMIN":
                    if(!(substr($page,0,7) == 'RESTORE')){
                        $sql.=" AND ".$register_Object->getReportingStatusSQL($register_tblref_alias);
                        if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
                            $sql.=" AND ".$risk_Object->getReportingStatusSQL($risk_tblref_alias);
                        }
                        if($obj_type=="ACTION") {
                            $sql.=" AND ".$action_Object->getReportingStatusSQL($action_tblref_alias);
                        }
                    }
					switch($page) {
						case "UPDATE_ACTION":
						case "UPDATE_DELIVERABLE":
							$sql.= " AND ( (".$where_status_fld." & ".RGSTR2::APPROVED.") <> ".RGSTR2::APPROVED." ) ";

							break;
						case "EDIT_ACTION":
						case "EDIT_DELIVERABLE":
							/** approval status has no effect on edit function - the objects won't be available for edit but must be available to be unlocked **/
							break;
					}
					break;
				case "REPORT":
				case "SEARCH":
					$sql.=" AND ".$register_Object->getReportingStatusSQL($register_tblref_alias);
					if($obj_type=="ACTION" || $obj_type=="DELIVERABLE") {
						$sql.=" AND ".$risk_Object->getReportingStatusSQL($risk_tblref_alias);
					}
					if($obj_type=="ACTION") {
						$sql.=" AND ".$action_Object->getReportingStatusSQL($action_tblref_alias);
					}
					break;
			}
			$sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";

//            echo '<pre style="font-size: 18px">';
//            echo '<p>SQL</p>';
//            print_r($sql);
//            echo '</pre>';

			$mnr = $this->db_get_num_rows($sql);
		if($trigger_rows==true || $page=="APPROVED_ACTION") {
			$start = ($start!=false && is_numeric($start) ? $start : "0");
			$sql.= (strlen($order_by)>0 ? " ORDER BY ".$order_by : " ORDER BY ".$where_tblref.".".$where_deadline) . ($limit !== false?" LIMIT ".$start." , $limit ":"");
			//echo $sql;
			//return array($sql);
			$rows = $this->mysql_fetch_all_by_id($sql,$id_field_name);

			$final_data = $this->formatRowDisplay($mnr,$rows, $final_data,$id_field_name,$headObject,$ref_tag,$status_id_fld,array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
		} else {
			$final_data['rows'] = array();
		}
		$final_data['head'] = $this->replaceObjectNames($final_data['head']);

        $final_data['the_sql_that_generated_this_data'] = $sql;

//        echo '<pre style="font-size: 18px">';
//        echo '<p>PRINT THE RECORDS ON THE ORIGINAL MANAGE PAGE</p>';
//        print_r($final_data);
//        echo '</pre>';

		return $final_data;
	}
	function formatRowDisplay($mnr,$rows,$final_data,$id_field_name,$headObject,$ref_tag,$status_id_fld,$paging) {
		$limit = $paging['limit'];
		$pagelimit = $paging['pagelimit'];
		$start = $paging['start'];
		$risk_Object = new RGSTR2_RISK();
			//ASSIST_HELPER::arrPrint($rows);
			$keys = array_keys($rows);
			$displayObject = new RGSTR2_DISPLAY();
			foreach($rows as $r) {
				$row = array();
				$i = $r[$id_field_name];
				foreach($final_data['head'] as $fld=> $head) {
					if($head['parent_id']==0){
						if($headObject->isListField($head['type']) && $head['type']!="DELIVERABLE" && $head['type']!="RISK" && $head['type']!="DEL_TYPE") {
						    //Testing out where I'm going to put the color functionality - START
//                            echo '<pre style="font-size: 18px">';
//                            echo '<p>ROW</p>';
//                            print_r($r);
//                            echo '</pre>';
                            if($head['field'] == 'rsk_impact' || $head['field'] == 'rsk_likelihood' || $head['field'] == 'rsk_inherent_exposure' || $head['field'] == 'rsk_control_effectiveness' || $head['field'] == 'rsk_residual_exposure' || $head['field'] == 'rsk_financial_exposure'){
                                $decription = $r[$head['list_table']];
                                $background_colour = $r[$head['list_table'] . '_colour'];
                                $row[$fld] = '<span style="display: inline-block; background-color: ' . $background_colour . '">' . $decription . '</span>';
                            }else{
                                $row[$fld] = $r[$head['list_table']];
                            }
                            //Testing out where I'm going to put the color functionality - END

//							$row[$fld] = $r[$head['list_table']];
							if($fld==$status_id_fld) {
								if(($r['my_status'] & RGSTR2::AWAITING_APPROVAL) == RGSTR2::AWAITING_APPROVAL) {
									$row[$fld].=" (Awaiting approval)";
								} elseif(($r['my_status'] & RGSTR2::APPROVED) == RGSTR2::APPROVED) {
									$row[$fld].= " (Approved)";
								}
							}
						} elseif($this->isDateField($fld)) {
							$row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
						} elseif($head['type']=="RISK") {
							if($r["rsk_type"]=='SUB') {
								if($r[$fld]==0) {
									$row[$fld] = $this->getUnspecified();
								} else {
									if(!isset($deliverable_names_for_subs)) {
										$deliverable_names_for_subs = $risk_Object->getDeliverableNamesForSubs($keys);
									}
									$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
								}
							} else {
								$row[$fld] = "N/A";
							}
						} elseif($head['type']=="DEL_TYPE") {
							if(isset($this->deliverable_types[$r[$fld]])) {
								$row[$fld] = $this->deliverable_types[$r[$fld]];
							} else {
								$row[$fld] = $this->getUnspecified().$r[$fld];
							}
						} else {
                            $row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));

						}
					}
				}
				$final_data['rows'][$i] = $row;
				//$mnr++;
			}

			if($mnr==0 || $limit === false) {
				$totalpages = 1;
				$currentpage = 1;
			} else {
				$totalpages = round(($mnr/$pagelimit),0);
				$totalpages += (($totalpages*$pagelimit)>=$mnr ? 0 : 1);
				if($start==0) {
					$currentpage = 1;
				} else {
					$currentpage = round($start/$pagelimit,0);
					$currentpage += (($currentpage*$pagelimit)>$start ? 0 : 1);
				}
			}
			$final_data['paging'] = array(
				'totalrows'=>$mnr,
				'totalpages'=>$totalpages,
				'currentpage'=>$currentpage,
				'pagelimit'=>$pagelimit,
				'first'=>($start==0 ? false : 0),
				'prev'=>($start==0 ? false : ($start-$pagelimit)),
				'next'=>($totalpages==$currentpage ? false : ($start+$pagelimit)),
				'last'=>($currentpage==$totalpages ? false : ($totalpages-1)*$pagelimit),
			);
		return $final_data;
	}


	public function getDetailedObject($obj_type,$id,$options=array()) { //ASSIST_HELPER::arrPrint($options);
		$left_joins = array();

		if(isset($options['compact_view'])) {
			$compact_view = $options['compact_view'];
		} else {
			$compact_view = false;
		}

		if(isset($options['type'])) {
			if($options['type']=="EMAIL"){
				$format_for_emails = true;
			}
			unset($options['type']);
		} else {
			$format_for_emails = false;
		}
		if(isset($options['attachment_buttons'])) {
			$attachment_buttons = $options['attachment_buttons'];
			unset($options['attachment_buttons']);
		} else {
			$attachment_buttons = true;
		}

		$headObject = new RGSTR2_HEADINGS();
		$final_data = array('head'=>array(),'rows'=>array());

		//set up contract variables
		$register_headings = $headObject->getMainObjectHeadings("REGISTER",($compact_view==true ? "COMPACT" : "DETAILS"));
		$register_Object = new RGSTR2_REGISTER();
		$register_tblref_alias = "REGISTER";
		$register_table = $register_Object->getTableName();
		$register_id = $register_tblref_alias.".".$register_Object->getIDFieldName();
		$register_id_fld = $register_Object->getIDFieldName();
		$register_status = $register_tblref_alias.".".$register_Object->getStatusFieldName();
		$register_name = $register_Object->getNameFieldName();
		$c_owner = $register_Object->getOwnerFieldName();
		$c_manager = $register_Object->getManagerFieldName();
		$register_deadline = $register_Object->getDeadlineFieldName();
		$register_field = $register_tblref_alias.".".$register_Object->getTableField();

		$where = $register_Object->getActiveStatusSQL($register_tblref_alias);

        $risk_Object = new RGSTR2_RISK();
        $risk_tblref_alias = "RISK";
        $risk_table = $risk_Object->getTableName();
        $risk_id_fld = $risk_Object->getIDFieldName();
        $register_id_as_risk_parent = $risk_tblref_alias.".".$risk_Object->getParentFieldName();

        $action_Object = new RGSTR2_ACTION();
        $action_tblref_alias = "ACTION";
        $action_table = $action_Object->getTableName();
        $action_id_fld = $action_Object->getIDFieldName();
        $risk_id_as_action_parent = $action_tblref_alias.".".$action_Object->getParentFieldName();

//        echo '<pre style="font-size: 18px">';
//        echo '<p>DETAILED OBJECT TYPE</p>';
//        print_r($obj_type);
//        echo '</pre>';

		//if type is something other than contract
		if($obj_type != "CONTRACT" && $obj_type != "REGISTER") {
			//setup deliverable variables
			$risk_status = $risk_tblref_alias.".".$risk_Object->getStatusFieldName();
			$risk_id = $risk_tblref_alias.".".$risk_Object->getIDFieldName();
			$risk_headings = $headObject->getMainObjectHeadings("RISK",($compact_view==true ? "COMPACT" : "DETAILS"));
			$risk_name = $risk_Object->getNameFieldName();
			$d_owner = $risk_Object->getOwnerFieldName();
			$risk_deadline = $risk_Object->getDeadlineFieldName();
			$risk_field = $risk_tblref_alias.".".$risk_Object->getTableField();
			$where.=" AND ".$risk_Object->getActiveStatusSQL($risk_tblref_alias);
			//setup action variables
			if($obj_type=="ACTION") {
				$action_status = $action_tblref_alias.".".$action_Object->getStatusFieldName();
				$action_id = $action_tblref_alias.".".$action_Object->getIDFieldName();
				$action_name = $action_Object->getNameFieldName();
				$action_deadline = $action_Object->getDeadlineFieldName();
				$a_owner = $action_Object->getOwnerFieldName();
				$action_field = $action_tblref_alias.".".$action_Object->getTableField();
				$action_headings = $headObject->getMainObjectHeadings("ACTION",($compact_view==true ? "COMPACT" : "DETAILS"));
				$id_field_name = $action_Object->getIDFieldName();
				if($format_for_emails) {
					$final_data['head'][$id_field_name] = $action_headings['rows'][$id_field_name];
					$final_data['head'][$action_name] = $action_headings['rows'][$action_name];
					$final_data['head'][$a_owner] = $action_headings['rows'][$a_owner];
					$final_data['head'][$action_deadline] = $action_headings['rows'][$action_deadline];
					$final_data['head'][$register_name] = $register_headings['rows'][$register_name];
					$final_data['head'][$register_id_fld] = $register_headings['rows'][$register_id_fld];
					$final_data['head'][$c_manager] = $register_headings['rows'][$c_manager];
					$final_data['head'][$risk_name] = $risk_headings['rows'][$risk_name];
					$final_data['head'][$risk_id_fld] = $risk_headings['rows'][$risk_id_fld];
					$final_data['head'][$d_owner] = $risk_headings['rows'][$d_owner];
					$sql ="SELECT ".$action_tblref_alias.".".$id_field_name." as id
							, CONCAT('".$action_Object->getRefTag()."',".$action_id.") as ".$action_id_fld." 
							, CONCAT(".$action_tblref_alias.".".$action_name.",' [".$action_Object->getRefTag()."',".$action_id.",']') as ".$action_name." 
							, ".$action_tblref_alias.".".$action_name."
							, ".$action_tblref_alias.".".$a_owner."
							, ".$action_tblref_alias.".".$action_deadline."
							, CONCAT('".$risk_Object->getRefTag()."',".$risk_id.") as ".$risk_id_fld." 
							, CONCAT(".$risk_tblref_alias.".".$risk_name.",' [".$risk_Object->getRefTag()."',".$risk_id.",']') as ".$risk_name." 
							, CONCAT('".$register_Object->getRefTag()."',".$register_id.") as ".$register_id_fld."
							, CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name."
							, ".$register_tblref_alias.".".$c_owner."
							, ".$register_tblref_alias.".".$c_manager."
							, ".$risk_tblref_alias.".".$d_owner."
							";
				} else {
					$sql ="SELECT ".$action_tblref_alias.".*, CONCAT(".$risk_tblref_alias.".".$risk_name.",' [".$risk_Object->getRefTag()."',".$risk_id.",']') as ".$risk_name." , CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name;
					$sql .=",$risk_tblref_alias.rsk_description,$risk_tblref_alias.rsk_current_controls,$risk_tblref_alias.rsk_impact_rating,$risk_tblref_alias.rsk_likelihood_rating,$risk_tblref_alias.rsk_control_effectiveness_rating";
                    $final_data['head'][$id_field_name] = $action_headings['rows'][$id_field_name];
                    $final_data['head'][$risk_name] = $risk_headings['rows'][$risk_name];

                    $final_data['head']['rsk_description'] = $risk_headings['rows']['rsk_description'];
                    $final_data['head']['rsk_current_controls'] = $risk_headings['rows']['rsk_current_controls'];
                    $final_data['head']['rsk_impact'] = $risk_headings['rows']['rsk_impact'];
                    $final_data['head']['rsk_impact_rating'] = $risk_headings['rows']['rsk_impact_rating'];
                    $final_data['head']['rsk_likelihood'] = $risk_headings['rows']['rsk_likelihood'];
                    $final_data['head']['rsk_likelihood_rating'] = $risk_headings['rows']['rsk_likelihood_rating'];
                    $final_data['head']['rsk_control_effectiveness'] = $risk_headings['rows']['rsk_control_effectiveness'];
                    $final_data['head']['rsk_control_effectiveness_rating'] = $risk_headings['rows']['rsk_control_effectiveness_rating'];

                    foreach($action_headings['rows'] as $fld=>$head){
						//if($fld!=$id_field_name) {
							$final_data['head'][$fld] = $head;
						//}
					}
				}
				$from = " $action_table $action_tblref_alias INNER JOIN $risk_table $risk_tblref_alias ON $risk_id = $risk_id_as_action_parent INNER JOIN $register_table $register_tblref_alias ON $register_id = $register_id_as_risk_parent ";
				$where.= " AND ".$action_Object->getActiveStatusSQL($action_tblref_alias);
				$where_tblref = $action_tblref_alias;
				$ref_tag = $action_Object->getRefTag();
			} else {
				$id_field_name = $risk_Object->getIDFieldName();
				$action_headings = array('rows'=>array());
				$from = " $risk_table $risk_tblref_alias 
						INNER JOIN $register_table $register_tblref_alias ON $register_id = $register_id_as_risk_parent 
						LEFT OUTER JOIN $action_table $action_tblref_alias
						  ON $risk_id_as_action_parent = $risk_tblref_alias.$risk_id_fld ";
				if($format_for_emails) {
					$sql ="SELECT ".$risk_tblref_alias.".".$id_field_name." as id 
							, CONCAT('".$risk_Object->getRefTag()."',".$risk_id.") as ".$risk_id_fld." 
							, CONCAT(".$risk_tblref_alias.".".$risk_name.",' [".$risk_Object->getRefTag()."',".$risk_id.",']') as ".$risk_name." 
							, CONCAT('".$register_Object->getRefTag()."',".$register_id.") as ".$register_id_fld."
							, CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name."
							, ".$register_tblref_alias.".".$c_owner."
							, ".$register_tblref_alias.".".$c_manager."
							, ".$risk_tblref_alias.".".$d_owner."
							";
					$final_data['head'][$id_field_name] = $risk_headings['rows'][$id_field_name];
					$final_data['head'][$register_name] = $register_headings['rows'][$register_name];
					$final_data['head'][$register_id_fld] = $register_headings['rows'][$register_id_fld];
					$final_data['head'][$c_manager] = $register_headings['rows'][$c_manager];
					$final_data['head'][$risk_name] = $risk_headings['rows'][$risk_name];
					$final_data['head'][$risk_id_fld] = $risk_headings['rows'][$risk_id_fld];
					$final_data['head'][$d_owner] = $risk_headings['rows'][$d_owner];
				} else {
					$sql ="SELECT ".$risk_tblref_alias.".* , AVG(ACTION.action_progress) as action_progress, CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name."";
					$final_data['head'][$id_field_name] = $risk_headings['rows'][$id_field_name];
					$final_data['head'][$register_name] = $register_headings['rows'][$register_name];
					$final_data['head'][$risk_name] = $risk_headings['rows'][$risk_name];
					foreach($risk_headings['rows'] as $fld=>$head){
						//if($fld!=$id_field_name) {
							$final_data['head'][$fld] = $head;
						//}
					}
					$group_by = " GROUP BY $risk_id";
				}
				$where_tblref = $risk_tblref_alias;
				$ref_tag = $risk_Object->getRefTag();
			}
		} else {
			$id_field_name = $register_Object->getIDFieldName();
			$action_headings = array('rows'=>array());
			$risk_headings = array('rows'=>array());
			if($format_for_emails) {
//				$final_data['head'][$id_field_name] = $risk_headings['rows'][$id_field_name];
				$final_data['head'][$register_name] = $register_headings['rows'][$register_name];
				$final_data['head'][$register_id_fld] = $register_headings['rows'][$register_id_fld];
				$final_data['head'][$c_manager] = $register_headings['rows'][$c_manager];
				$sql ="SELECT ".$register_tblref_alias.".".$id_field_name." as id 
						, CONCAT('".$register_Object->getRefTag()."',".$register_id.") as ".$register_id_fld."
						, CONCAT(".$register_tblref_alias.".".$register_name.",' [".$register_Object->getRefTag()."',".$register_id.",']') as ".$register_name."
						, ".$register_tblref_alias.".".$c_manager."";
			} else {
				$sql = "SELECT ".$register_tblref_alias.".*, AVG(ACTION.action_progress) as action_progress";
				$risk_headings = array('rows'=>array());
				$action_headings = array('rows'=>array());
				$final_data['head'] = $register_headings['rows'];
				unset($final_data['head']['cs_supplier_id']);
				unset($final_data['head']['cs_project_value']);
				unset($final_data['head']['contract_template_id']);
				$group_by = " GROUP BY ".$register_tblref_alias.".".$id_field_name;
			}
			$from = " $register_table $register_tblref_alias 
					LEFT OUTER JOIN $risk_table $risk_tblref_alias
					  ON $register_tblref_alias.$id_field_name = $register_id_as_risk_parent
					LEFT OUTER JOIN $action_table $action_tblref_alias
					  ON $risk_id_as_action_parent = $risk_tblref_alias.$risk_id_fld ";
			$where_tblref = $register_tblref_alias;
			$ref_tag = $register_Object->getRefTag();
		}

		$where.= " AND ".$where_tblref.".".$id_field_name." = ".$id;

		$all_headings = array_merge(
			$register_headings['rows'],
			$risk_headings['rows'],
			$action_headings['rows']
		);
		//return $risk_headings;
		$listObject = new RGSTR2_LIST();

		//$list_headings = $headObject->getAllListHeadings();
		//$list_tables = $listObject->getAllListTables(array_keys($list_headings));

		foreach($all_headings as $fld => $head) {
			$left_join_tblref_alias = substr($head['section'],0,1);

            //Copied ths hack from one of the functions above
            //Register/Contract hack
            $left_join_tblref_alias = ($left_join_tblref_alias == 'C' ? 'REGISTER' : $left_join_tblref_alias); //Tshego Hack
            $left_join_tblref_alias = ($left_join_tblref_alias == 'R' ? 'RISK' : $left_join_tblref_alias); //Tshego Hack

            //Risk/Deliverable hack
            $left_join_tblref_alias = ($left_join_tblref_alias == 'D' ? 'RISK' : $left_join_tblref_alias); //Tshego Hack
            $left_join_tblref_alias = ($left_join_tblref_alias == 'R' ? 'RISK' : $left_join_tblref_alias); //Tshego Hack

            //Action Hack
            $left_join_tblref_alias = ($left_join_tblref_alias == 'A' ? 'ACTION' : $left_join_tblref_alias); //Tshego Hack

            //One more Hack
            if(substr($head['section'],0,1) == 'R'){
                if(substr($head['section'],0,3) == 'REG'){
                    $left_join_tblref_alias = 'REGISTER';
                }else{
                    $left_join_tblref_alias = 'RISK';
                }
            }

			if($head['type']=="LIST" && $head['parent_id']==0) {
				$listObject->changeListType($head['list_table']);
				$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
									AS ".$head['list_table']." 
									ON ".$head['list_table'].".id = ".$left_join_tblref_alias.".".$fld." 
									AND (".$head['list_table'].".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
			} elseif($head['type']=="MASTER") {
				$tbl = $head['list_table'];
				$masterObject = new RGSTR2_MASTER($fld);
				$fy = $masterObject->getFields();
				$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
				$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$left_join_tblref_alias.".".$fld." = ".$fld.".".$fy['id'];
			} elseif($head['type']=="USER") {
				$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
				$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$left_join_tblref_alias.".".$fld." AND ".$fld.".tkstatus = 1";
			} elseif($head['type']=="OWNER") {
				$ownerObject = new RGSTR2_CONTRACT_OWNER();
				$tbl = $head['list_table'];
				$dir_tbl = $fld."_parent";
				$sub_tbl = $fld."_child";
				$own_tbl = $fld;
				$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
				$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." ON ".$left_join_tblref_alias.".".$fld." = ".$sub_tbl.".subid";
				$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
				$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." AS ".$own_tbl." ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
			} elseif($head['type']=="CONTRACT_SUPPLIER") {

			}
		}
		$sql.= " FROM ".$from.implode(" ",$left_joins);
		$sql.= " WHERE ".$where.(isset($group_by) ? $group_by : "");
		//return $sql;
		$r = $this->mysql_fetch_one($sql);

		$row = array();
		$displayObject = new RGSTR2_DISPLAY();
//		foreach($rows as $r) {
	//$this->arrPrint($r);
			$i = $r[$id_field_name];
			foreach($final_data['head'] as $fld=> $head) {
				$val = isset($r[$fld]) ? $r[$fld] : $fld;
				//format by type
				if($headObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
					$row[$fld] = ( (!isset($r[$head['list_table']]) || is_null($r[$head['list_table']])) ? $this->getUnspecified() : $r[$head['list_table']]);
				//} elseif($head['type']=="BOOL") {
					//$row[$fld] = $displayObject->getDataField("BOOL", $val);
				} elseif($head['type']=="DEL_TYPE"){
					$row[$fld] = $this->deliverable_types[$val];
				} elseif($head['type']=="DELIVERABLE") {
					if($r["rsk_type"]=='SUB') {
						if($r[$fld]==0) {
							$row[$fld] = $this->getUnspecified();
						} else {
							if(!isset($deliverable_names_for_subs)) {
								$deliverable_names_for_subs = $risk_Object->getDeliverableNamesForSubs($i);
								//$this->arrPrint($deliverable_names_for_subs);
							}
							$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
						}
					} else {
						unset($final_data['head'][$fld]);
					}
				} elseif($head['section']=="DELIVERABLE_ASSESS") {
					$assess_status = $r['contract_assess_type'];
					$display_me = true;
						if($register_Object->mustIDoAssessment($r['contract_do_assessment'])===FALSE) {
							$display_me = false;
						} else {
							//validate the assessment status field here
							$lt = explode("_",$head['list_table']);
							switch($lt[1]) {
								case "quality": $display_me = $register_Object->mustIAssessQuality($assess_status); break;
								case "quantity": $display_me = $register_Object->mustIAssessQuantity($assess_status); break;
								case "other": $display_me = $register_Object->mustIAssessOther($assess_status); break;
							}
						}
					if($display_me) {
						$row[$fld] = is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']];
						if($lt[1]=="other" && strlen($r['contract_assess_other_name'])>0) {
							$final_data['head'][$fld]['name'] = str_ireplace("Other", $r['contract_assess_other_name'], $final_data['head'][$fld]['name']);
						}
					} else {
						unset($final_data['head'][$fld]);
						//$row[$fld] = $assess_status;
					}
				} elseif($this->isDateField($fld) || $head['type']=="DATE") {
					$row[$fld] = $displayObject->getDataField("DATE", $val,array('include_time'=>false));
				//format by fld
				} elseif($fld==$id_field_name){//} || ($obj_type=="ACTION" && $fld==$action_id_fld) || ($obj_type=="DELIVERABLE" && $fld==$risk_id_fld) || ($obj_type=="CONTRACT" && $fld==$register_id_fld)) {
					if($fld==$id_field_name && !$format_for_emails) {
						$row[$fld] = $ref_tag.$val;
					} else {
						$row[$fld] = $val;
					}

				} elseif($fld=="contract_assess_status") {
					$val = $r['contract_assess_status'];
					$listObject = new RGSTR2_LIST("deliverable_status");
					$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
					$last_deliverable_status = 0;
					$sub_head = array();
					$sh_type = "BOOL";
					foreach($list_items as $key => $status) {
						$sub_head[$key] = array(
							'field'=>$key,
							'type'=>$sh_type,
							'name'=>$status,
							'required'=>1,
							'parent_link'=>"",
						);
						$last_deliverable_status = $key;
					}
					$td = array();
					foreach($sub_head as $key => $shead) {
						$sh_type = $shead['type'];
						$sfld = $shead['field'];
						if($sh_type=="BOOL"){
							$pow = pow(2,$key);
							$test = ( (($val & $pow) == $pow) ? "1" : "0");
							$sval = $displayObject->getDataField("BOOL", $test);
						}
						$td[]=array($shead['name']=>$sval['display']);
					}
					$row[$fld] = $td;
				} elseif($fld=="contract_assess_type") {
					$sub_head = $register_headings['sub'][$head['id']];
					$val = $r['contract_assess_type'];
					$sub_display = true;
					$td = array();
					foreach($sub_head as $shead) {
						$v = $shead['field'];
						switch($v) {
							case "contract_assess_other_name":
								if($register_Object->mustIAssessOther($val)) { 
									$v = $r[$v];
								} else {
									$sub_display = false;
								} 
								break;
							case "contract_assess_qual": 	$cas = !isset($cas) ? $register_Object->mustIAssessQuality($val) : $cas;
							case "contract_assess_quan": 	$cas = !isset($cas) ? $register_Object->mustIAssessQuantity($val) : $cas;
							case "contract_assess_other":	$cas = !isset($cas) ? $register_Object->mustIAssessOther($val) : $cas; 
								if($cas) {
									$v = $displayObject->getDataField("BOOL", "1");
								} else {
									$v = $displayObject->getDataField("BOOL", "0");
								}
								$v = $v['display'];
								break;
						}
						if($sub_display) {
							$td[] = array($shead['name']=>$v);
						}
						unset($cas);
					}
					$row[$fld] = $td;
				} elseif($head['type']=="REF") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
				} elseif($head['type']=="ATTACH") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
					$attach_display_options = array(
						'can_edit'=>false,
						'object_type'=>$obj_type,
						'object_id'=>$id,
						'buttons'=>$attachment_buttons,
					);
					$row[$fld] = $displayObject->getDataField($head['type'], $val,$attach_display_options);
				} else {
					//$row[$fld] = $val;
					$row[$fld] = $displayObject->getDataField($head['type'], $val);
				}
			}
			if($format_for_emails) {
				switch($obj_type) {
					case "ACTION":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$action_name] = $row[$action_name]; 
						$sorted_row['child'][$a_owner] = $row[$a_owner]; 
						$sorted_row['child'][$action_deadline] = $row[$action_deadline]; 
						$sorted_row['parent'][$risk_name] = $row[$risk_name]; 
						$sorted_row['parent'][$d_owner] = $row[$d_owner];
						$sorted_row['grandparent'][$register_name] = $row[$register_name];
						$sorted_row['grandparent'][$c_manager] = $row[$c_manager];
						$field_names['child']['id'] = $row[$action_id_fld];
						$field_names['child']['name'] = $action_name; 
						$field_names['child']['owner'] = $a_owner; 
						$field_names['child']['deadline'] = $action_deadline; 
						$field_names['parent']['id'] = $row[$risk_id_fld]; 
						$field_names['parent']['name'] = $risk_name; 
						$field_names['parent']['owner'] = $d_owner;
						$field_names['grandparent']['id'] = $row[$register_id_fld]; 
						$field_names['grandparent']['name'] = $register_name;
						$field_names['grandparent']['manager'] = $c_manager;
						break;
					case "DELIVERABLE":
					case "RISK":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$risk_name] = $row[$risk_name]; 
						$sorted_row['child'][$d_owner] = $row[$d_owner];
						$sorted_row['parent'][$register_name] = $row[$register_name];
						$sorted_row['parent'][$c_manager] = $row[$c_manager];
						$field_names['child']['id'] = $row[$risk_id_fld]; 
						$field_names['child']['name'] = $risk_name; 
						$field_names['child']['owner'] = $d_owner;
						$field_names['parent']['id'] = $row[$register_id_fld]; 
						$field_names['parent']['name'] = $register_name;
						$field_names['parent']['manager'] = $c_manager;
						break;
					case "CONTRACT":
					case "REGISTER":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$register_name] = $row[$register_name];
						$sorted_row['child'][$c_manager] = $row[$c_manager];
						$field_names['child']['id'] = $row[$register_id_fld];
						$field_names['child']['name'] = $register_name;
						$field_names['child']['manager'] = $c_manager;
						break;
				}				
				$final_data['rows'] = $sorted_row;
				$final_data['email_fields'] = $field_names;
				//$final_data['email_full'] = $r;
			} else {
				$final_data['rows'] = $row;
			}
		//}

		return $final_data;
	}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}
	
	public function getMyHazardList($obj_type,$section,$options=array()){
        //echo "<P>object: ".$obj_type;
        //echo "<P>section: ".$section;
        //echo "<P>options: "; $this->arrPrint($options);
        if(isset($options['page'])) {
            $page = strtoupper($options['page']);
            unset($options['page']);
        } else {
            $page = "DEFAULT";
        }
        $trigger_rows = true;
        if(isset($options['trigger'])) {
            $trigger_rows = $options['trigger'];
            unset($options['trigger']);
        }
        //echo $page;
        if($page=="LOGIN_STATS" || $page=="LOGIN_TABLE") {
            $future_deadline = $options['deadline'];
            unset($options['deadline']);
            if(!isset($options['limit'])) { $options['limit'] = false; }
        }
        $compact_details = ($page == "CONFIRM" || $page == "ALL");
        if(isset($options['limit']) && ($options['limit'] === false || $options['limit']=="ALL")){
            $pagelimit = false;
            $start = 0;
            $limit = false;
            unset($options['start']);
            unset($options['limit']);
        }else{
            $pagelimit = $this->getPageLimit();
            $start = isset($options['start']) ? $options['start'] : 0;  unset($options['start']);
            $limit = isset($options['limit']) ? $options['limit'] : $pagelimit;	unset($options['limit']);
            if($start<0) {
                $start = 0;
                $limit = false;
            }
        }
        $order_by = ""; //still to be processed
        $left_joins = array();

        $headObject = new RGSTR2_HEADINGS();
        $final_data = array('head'=>array(),'rows'=>array());

        //set up variables
        $idp_headings = $headObject->getMainObjectHeadings($obj_type,($obj_type=="IDP"?"LIST":"FULL"),$section);
        switch($obj_type) {

            case "FUNCTION":
                $idpObject = new RGSTR2_FUNCTION();
                break;
            case "COMPETENCY":
                $idpObject = new RGSTR2_COMPETENCY();
                break;
            case "HAZARD":
                $idpObject = new RGSTR2_HAZARD();
                break;
            case "KING_IV":
                $idpObject = new RGSTR2_KING_IV();
                break;
        }
        $idp_tblref = $idpObject->getMyObjectType();
        $idp_table = $idpObject->getTableName();
        $idp_id = $idp_tblref.".".$idpObject->getIDFieldName();
        $idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();
        $idp_status_fld = false;//$idp_tblref.".".$idpObject->getProgressStatusFieldName();
        $idp_name = $idp_tblref.".".$idpObject->getNameFieldName();
        $idp_deadline = false;//$idpObject->getDeadlineFieldName();
        $idp_field = $idp_tblref.".".$idpObject->getTableField();

        $where = $idpObject->getActiveStatusSQL($idp_tblref);
        //if type is something other than contract 
        if($obj_type!="IDP") {
            switch($obj_type) {

                case "FUNCTION":
                case "COMPETENCY":
                case "HAZARD":
                case "KING_IV":
                    $id_fld = $idpObject->getIDFieldName();
                    $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
                    $from = " $idp_table $idp_tblref 
					";
                    $final_data['head'] = $idp_headings;
                    $where_tblref = $idp_tblref;
                    $where_deadline = $idp_deadline;
                    $where_order = $idp_name;
                    $ref_tag = $idpObject->getRefTag();
                    if($page=="LOGIN_TABLE") {
                        $order_by = $idp_name;
                    }
                    $where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
                    $group_by = "";
                    break;

            }
        } else {
            $id_fld = $idpObject->getIDFieldName();
            $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
            $from = " $idp_table $idp_tblref 
			";
            $final_data['head'] = $idp_headings;
            $where_tblref = $idp_tblref;
            $where_deadline = $idp_deadline;
            $where_order = $idp_name;
            $ref_tag = $idpObject->getRefTag();
            if($page=="LOGIN_TABLE") {
                $order_by = $idp_name;
            }
            $where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
            $group_by = "";
        }
//ASSIST_HELPER::arrPrint($final_data);
        if(count($options)>0) {
            foreach($options as $key => $filter) {
                if(substr($key,0,3)!=substr($this->getTableField(),0,3) && strrpos($key,".")===FALSE) {
                    $key = $this->getTableField()."_".$key;
                }
                $where.= " AND ".(strrpos($key,".")===FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
            }
        }


        $all_headings = array_merge($idp_headings);

        $listObject = new RGSTR2_LIST();

        foreach($all_headings as $fld => $head) {
            $lj_tblref = $head['section'];
            if($head['type']=="LIST") {
                $listObject->changeListType($head['list_table']);
                $sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
                $left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
										AND (".$head['list_table'].".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
            } elseif($head['type']=="MASTER") {
                $tbl = $head['list_table'];
                $masterObject = new RGSTR2_MASTER($fld);
                $fy = $masterObject->getFields();
                $sql.=", ".$fld.".".$fy['name']." as ".$tbl;
                $left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
            } elseif($head['type']=="USER") {
                $sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
                $left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
            }
        }
        $sql.= " FROM ".$from.implode(" ",$left_joins);
        $sql.= " WHERE ".$where;
        switch($section) {
            case "ACTIVE":
                $sql.=" AND ".$idpObject->getActiveStatusSQL($idp_tblref);
                break;
            case "NEW":
//                if($page=="ACTIVATE_WAITING") {
//                    $sql.=" AND ".$idpObject->getActivationStatusSQL($idp_tblref);
//                } elseif($page=="ACTIVATE_DONE") {
//                    $sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
//                } else {
//                    $sql.=" AND ".$idpObject->getNewStatusSQL($idp_tblref);
//                }
                break;
            case "MANAGE":
                $sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
                break;
            case "REPORT":
            case "SEARCH":
                $sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
                break;
        }
        //$sql.= isset($parent_id) ? " AND ".$this->getParentFieldName()." = ".$parent_id : "";
        $sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";

//        echo '<pre style="font-size: 18px">';
//        echo '<p>PRINTHERE</p>';
//        print_r($sql);
//        echo '</pre>';


        $mnr = $this->db_get_num_rows($sql);
        if($trigger_rows==true) {
            $start = ($start!=false && is_numeric($start) ? $start : "0");
            $sql.=" ORDER BY " . $id_fld;
            $sql.=($limit !== false?" LIMIT ".$start." , $limit ":"");
            //echo $sql;
            //return array($sql);
            $rows = $this->mysql_fetch_all_by_id($sql,$id_fld);
            $all_my_lists = array();
            foreach($all_headings as $fld => $head) {
                switch($head['type']) {
                    case "MULTILIST":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $listObject = new RGSTR2_LIST($head['list_table']);
                            $all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
                            unset($listObject);
                        }
                        //check each row and replace as needed
                        foreach($rows as $key => $r) {
                            $val = explode(";",$r[$fld]);
                            $rows[$key][$fld] = array();
                            foreach($val as $x) {
                                if(isset($all_my_lists[$fld][$x])) {
                                    $rows[$key][$fld][] = $all_my_lists[$fld][$x];
                                }
                            }
                            if(count($rows[$key][$fld])>0) {
                                $rows[$key][$fld] = implode("; ",$rows[$key][$fld]);
                            } else {
                                $rows[$key][$fld] = $this->getUnspecified();
                            }
                        }
                        break;
                    case "OBJECT":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $list_object_name = $head['list_table'];
                            $listObject = new $list_object_name();
                            $all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect();
                            unset($listObject);
                        }
                        //check each row and replace as needed
                        foreach($rows as $key => $r) {
                            $x = $r[$fld];
                            if(isset($all_my_lists[$fld][$x])) {
                                $rows[$key][$fld] = $all_my_lists[$fld][$x];
                            } else {
                                $rows[$key][$fld] = $this->getUnspecified();
                            }
                        }
                        break;
                    case "COMPETENCY_PROFICIENCY":
                        $cpObject = new RGSTR2_COMPETENCY_PROFICIENCY();
                        $cp_rows = $cpObject->getRecordsForListDisplay(array_keys($rows));
                        foreach($rows as $key => $r) {
                            $v = "<ul><li>".implode("</li><li>",$cp_rows[$key])."</li></ul>";
                            $rows[$key][$fld] = $v;
                        }
                        break;
                }
            }

            $final_data = $this->formatHazRowDisplay($mnr,$rows, $final_data,$id_fld,$headObject,$ref_tag,false,array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
        } else {
            $final_data['rows'] = array();
        }
        $final_data['head'] = $this->replaceObjectNames($final_data['head']);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>FINAL DATA</p>';
//        print_r($final_data);
//        echo '</pre>';

        return $final_data;
    }


    public function getHazardDetails($obj_type,$section,$options=array()){
        //echo "<P>object: ".$obj_type;
        //echo "<P>section: ".$section;
        //echo "<P>options: "; $this->arrPrint($options);
        if(isset($options['page'])) {
            $page = strtoupper($options['page']);
            unset($options['page']);
        } else {
            $page = "DEFAULT";
        }
        $trigger_rows = true;
        if(isset($options['trigger'])) {
            $trigger_rows = $options['trigger'];
            unset($options['trigger']);
        }
        //echo $page;
        if($page=="LOGIN_STATS" || $page=="LOGIN_TABLE") {
            $future_deadline = $options['deadline'];
            unset($options['deadline']);
            if(!isset($options['limit'])) { $options['limit'] = false; }
        }
        $compact_details = ($page == "CONFIRM" || $page == "ALL");
        if(isset($options['limit']) && ($options['limit'] === false || $options['limit']=="ALL")){
            $pagelimit = false;
            $start = 0;
            $limit = false;
            unset($options['start']);
            unset($options['limit']);
        }else{
            $pagelimit = $this->getPageLimit();
            $start = isset($options['start']) ? $options['start'] : 0;  unset($options['start']);
            $limit = isset($options['limit']) ? $options['limit'] : $pagelimit;	unset($options['limit']);
            if($start<0) {
                $start = 0;
                $limit = false;
            }
        }
        $order_by = ""; //still to be processed
        $left_joins = array();

        $headObject = new RGSTR2_HEADINGS();
        $final_data = array('head'=>array(),'rows'=>array());

        //set up variables
        $idp_headings = $headObject->getMainObjectHeadings($obj_type,($obj_type=="IDP"?"LIST":"FULL"),$section);
        switch($obj_type) {

            case "FUNCTION":
                $idpObject = new RGSTR2_FUNCTION();
                break;
            case "COMPETENCY":
                $idpObject = new RGSTR2_COMPETENCY();
                break;
            case "HAZARD":
                $idpObject = new RGSTR2_HAZARD();
                break;
            case "KING_IV":
                $idpObject = new RGSTR2_KING_IV();
                break;
        }
        $idp_tblref = $idpObject->getMyObjectType();
        $idp_table = $idpObject->getTableName();
        $idp_id = $idp_tblref.".".$idpObject->getIDFieldName();
        $idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();
        $idp_status_fld = false;//$idp_tblref.".".$idpObject->getProgressStatusFieldName();
        $idp_name = $idp_tblref.".".$idpObject->getNameFieldName();
        $idp_deadline = false;//$idpObject->getDeadlineFieldName();
        $idp_field = $idp_tblref.".".$idpObject->getTableField();

        $where = $idpObject->getActiveStatusSQL($idp_tblref);
        //if type is something other than contract
        if($obj_type!="IDP") {
            switch($obj_type) {

                case "FUNCTION":
                case "COMPETENCY":
                case "HAZARD":
                    $id_fld = $idpObject->getIDFieldName();
                    $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
                    $from = " $idp_table $idp_tblref 
					";
                    $final_data['head'] = $idp_headings;
                    $where_tblref = $idp_tblref;
                    $where_deadline = $idp_deadline;
                    $where_order = $idp_name;
                    $ref_tag = $idpObject->getRefTag();
                    if($page=="LOGIN_TABLE") {
                        $order_by = $idp_name;
                    }
                    $where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
                    $group_by = "";
                    break;

            }
        } else {
            $id_fld = $idpObject->getIDFieldName();
            $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
            $from = " $idp_table $idp_tblref 
			";
            $final_data['head'] = $idp_headings;
            $where_tblref = $idp_tblref;
            $where_deadline = $idp_deadline;
            $where_order = $idp_name;
            $ref_tag = $idpObject->getRefTag();
            if($page=="LOGIN_TABLE") {
                $order_by = $idp_name;
            }
            $where_status_fld = $idp_tblref.".".$idpObject->getStatusFieldName();
            $group_by = "";
        }
//ASSIST_HELPER::arrPrint($final_data);
        if(count($options)>0) {
            foreach($options as $key => $filter) {
                if(substr($key,0,3)!=substr($this->getTableField(),0,3) && strrpos($key,".")===FALSE) {
                    $key = $this->getTableField()."_".$key;
                }
                $where.= " AND ".(strrpos($key,".")===FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
            }
        }


        $all_headings = array_merge($idp_headings);

        $listObject = new RGSTR2_LIST();

        foreach($all_headings as $fld => $head) {
            $lj_tblref = $head['section'];
            if($head['type']=="LIST") {
                $listObject->changeListType($head['list_table']);
                $sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
                $left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
										AND (".$head['list_table'].".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
            } elseif($head['type']=="MASTER") {
                $tbl = $head['list_table'];
                $masterObject = new RGSTR2_MASTER($fld);
                $fy = $masterObject->getFields();
                $sql.=", ".$fld.".".$fy['name']." as ".$tbl;
                $left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
            } elseif($head['type']=="USER") {
                $sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
                $left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
            }
        }
        $sql.= " FROM ".$from.implode(" ",$left_joins);
        $sql.= " WHERE ".$where;
        switch($section) {
            case "ACTIVE":
                $sql.=" AND ".$idpObject->getActiveStatusSQL($idp_tblref);
                break;
            case "NEW":
//                if($page=="ACTIVATE_WAITING") {
//                    $sql.=" AND ".$idpObject->getActivationStatusSQL($idp_tblref);
//                } elseif($page=="ACTIVATE_DONE") {
//                    $sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
//                } else {
//                    $sql.=" AND ".$idpObject->getNewStatusSQL($idp_tblref);
//                }
                break;
            case "MANAGE":
                $sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
                break;
            case "REPORT":
            case "SEARCH":
                $sql.=" AND ".$idpObject->getReportingStatusSQL($idp_tblref);
                break;
        }
        //$sql.= isset($parent_id) ? " AND ".$this->getParentFieldName()." = ".$parent_id : "";
        $sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";

//        echo '<pre style="font-size: 18px">';
//        echo '<p>PRINTHERE</p>';
//        print_r($sql);
//        echo '</pre>';


        $mnr = $this->db_get_num_rows($sql);
        if($trigger_rows==true) {
            $start = ($start!=false && is_numeric($start) ? $start : "0");
            $sql.=" ORDER BY " . $id_fld;
            $sql.=($limit !== false?" LIMIT ".$start." , $limit ":"");
            //echo $sql;
            //return array($sql);
            $rows = $this->mysql_fetch_all_by_id($sql,$id_fld);
            $all_my_lists = array();
            foreach($all_headings as $fld => $head) {
                switch($head['type']) {
                    case "MULTILIST":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $listObject = new RGSTR2_LIST($head['list_table']);
                            $all_my_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
                            unset($listObject);
                        }
                        //check each row and replace as needed
                        foreach($rows as $key => $r) {
                            $val = explode(";",$r[$fld]);
                            $rows[$key][$fld] = array();
                            foreach($val as $x) {
                                if(isset($all_my_lists[$fld][$x])) {
                                    $rows[$key][$fld][] = $all_my_lists[$fld][$x];
                                }
                            }
                            if(count($rows[$key][$fld])>0) {
                                $rows[$key][$fld] = implode("; ",$rows[$key][$fld]);
                            } else {
                                $rows[$key][$fld] = $this->getUnspecified();
                            }
                        }
                        break;
                    case "OBJECT":
                        //get list items if not already available
                        if(!isset($all_my_lists[$fld])) {
                            $list_object_name = $head['list_table'];
                            $listObject = new $list_object_name();
                            $all_my_lists[$fld] = $listObject->getActiveObjectsFormattedForSelect();
                            unset($listObject);
                        }
                        //check each row and replace as needed
                        foreach($rows as $key => $r) {
                            $x = $r[$fld];
                            if(isset($all_my_lists[$fld][$x])) {
                                $rows[$key][$fld] = $all_my_lists[$fld][$x];
                            } else {
                                $rows[$key][$fld] = $this->getUnspecified();
                            }
                        }
                        break;
                    case "COMPETENCY_PROFICIENCY":
                        $cpObject = new RGSTR2_COMPETENCY_PROFICIENCY();
                        $cp_rows = $cpObject->getRecordsForListDisplay(array_keys($rows));
                        foreach($rows as $key => $r) {
                            $v = "<ul><li>".implode("</li><li>",$cp_rows[$key])."</li></ul>";
                            $rows[$key][$fld] = $v;
                        }
                        break;
                }
            }

            $final_data = $this->formatHazRowDisplay($mnr,$rows, $final_data,$id_fld,$headObject,$ref_tag,false,array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
        } else {
            $final_data['rows'] = array();
        }
        $final_data['head'] = $this->replaceObjectNames($final_data['head']);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>FINAL DATA</p>';
//        print_r($final_data);
//        echo '</pre>';

        return $final_data;
    }


    public function formatHazRowDisplay($mnr,$rows,$final_data,$id_fld,$headObject,$ref_tag,$status_id_fld,$paging){
        $limit = $paging['limit'];
        $pagelimit = $paging['pagelimit'];
        $start = $paging['start'];
        //ASSIST_HELPER::arrPrint($rows);
        $keys = array_keys($rows);
        $displayObject = new RGSTR2_DISPLAY();
        foreach($rows as $r) {
            $row = array();
            $i = $r[$id_fld];
            foreach($final_data['head'] as $fld=> $head) {
                if($head['parent_id']==0){
                    if($headObject->isListField($head['type']) && $head['type']!="DELIVERABLE") {
                        $row[$fld] = strlen($r[$head['list_table']])>0 ? $r[$head['list_table']] : $this->getUnspecified();
                        if($status_id_fld!==false && $fld==$status_id_fld) {
                            if(($r['my_status'] & RGSTR2::AWAITING_APPROVAL) == RGSTR2::AWAITING_APPROVAL) {
                                $row[$fld].=" (Awaiting approval)";
                            } elseif(($r['my_status'] & RGSTR2::APPROVED) == RGSTR2::APPROVED) {
                                $row[$fld].= " (Approved)";
                            }
                        }
                    } elseif($this->isDateField($fld)) {
                        $row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
                    } else {
                        //$row[$fld] = $r[$fld];
                        $row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
                    }
                }
            }
            $final_data['rows'][$i] = $row;
            //$mnr++;
        }

        if($mnr==0 || $limit === false) {
            $totalpages = 1;
            $currentpage = 1;
        } else {
            $totalpages = round(($mnr/$pagelimit),0);
            $totalpages += (($totalpages*$pagelimit)>=$mnr ? 0 : 1);
            if($start==0) {
                $currentpage = 1;
            } else {
                $currentpage = round($start/$pagelimit,0);
                $currentpage += (($currentpage*$pagelimit)>$start ? 0 : 1);
            }
        }
        $final_data['paging'] = array(
            'totalrows'=>$mnr,
            'totalpages'=>$totalpages,
            'currentpage'=>$currentpage,
            'pagelimit'=>$pagelimit,
            'first'=>($start==0 ? false : 0),
            'prev'=>($start==0 ? false : ($start-$pagelimit)),
            'next'=>($totalpages==$currentpage ? false : ($start+$pagelimit)),
            'last'=>($currentpage==$totalpages ? false : ($totalpages-1)*$pagelimit),
        );
        return $final_data;
    }
	
    
    /***********************************
     * GET functions
     */
    
    /**
	 * (ECHO) Function to display the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
echo "WRONG PAGE FOOTER";
return "";
    }
    /**
	 * Function to create the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="") {
/*    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=div_audit_log></div>";
		if(strlen($log_table)>0){
			$js = "
			$(\"#disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#div_audit_log\").html(\"\");
					$(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					$(\"#div_audit_log\").html(result[0]);
					$(\"#log_txt\").html('Hide Activity Log');
				}
				
			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
 **/
		return array('display'=>"WRONG PAGE FOOTER",'js'=>"");
    }
 
	public function drawActivityLog() {
		
	}
 
 
 
   
    /*********************************************
     * SET/UPDATE functions
     */
	//public function addActivityLog($log_table,$var) {
	//	$logObject = new RGSTR2_LOG($log_table);
	//	$logObject->addObject($var);
	//}
	
    
    public function notify($data,$type,$object){
    	$noteObj = new RGSTR2_NOTIFICATION($object);
		$result = $noteObj->prepareNote($data, $type, $object);
		return $result;
    }
   
   
   

	public function editMyObject($var,$attach=array(),$recipients=array(),$noter="",$include=array()) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$headObject = new RGSTR2_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(),"FORM");
		$mar=0;

		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld=>$v) {
			if(isset($headings['rows'][$fld])/* || in_array($fld,array("contract_assess_status","contract_assess_type","contract_assess_other_name"))*/) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type']=="DATE") {
					if(strlen($v)>0){
						$insert_data[$fld] = date("Y-m-d",strtotime($v));
					}
				} elseif($fld=="rsk_type") {
					if($v=="DEL") {
						$var['rsk_parent_id'] = 0;
						$insert_data['rsk_parent_id'] = 0;
					}
					$insert_data[$fld] = $v;
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}
		
		
		$old = $this->getRawObject($object_id);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

        $mar = (int)$mar;
		
		if($mar > 0 || count($attach)>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			foreach($insert_data as $fld => $v) {
                $h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type'=>"TEXT");
				if($old[$fld]!=$v || $h['type']=="HEADING") {
					if(in_array($h['type'],array("LIST","MASTER","USER","OWNER","DEL_TYPE","DELIVERABLE"))) {
						$list_items = array();
						$ids = array($v,$old[$fld]);
						switch($h['type']) {
							case "LIST":
								$listObject = new RGSTR2_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								break;
							case "USER":
								$userObject = new RGSTR2_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "OWNER":
								$ownerObject = new RGSTR2_CONTRACT_OWNER();
								$list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new RGSTR2_MASTER($head['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break;
							case "DELIVERABLE":
								$delObject = new RGSTR2_RISK();
								$list_items = $delObject->getDeliverableNamesForSubs($ids);
								break;
							case "DEL_TYPE":
								$delObject = new RGSTR2_RISK();
								$list_items = $delObject->getDeliverableTypes($ids);
								break;
						}

						unset($listObject);
						$changes[$fld] = array('to'=>$list_items[$v],'from'=>$list_items[$old[$fld]], 'raw'=>array('to'=>$v,'from'=>$old[$fld]));
					} elseif($h['type']=="HEADING") {
						if($fld=="contract_assess_type") {
							$shead = $headings['sub'][$h['id']];
							foreach($shead as $sh) {
								$sfld = $sh['field'];
								switch($sh['type']) {
									case "BOOL":
										$bitwise_value = 0;
										$b = explode("_",$sfld);
										$f = end($b);
										switch($f) {
											case "other":	$bitwise_value = RGSTR2_CONTRACT::OTHER;	break;
											case "qual":	$bitwise_value = RGSTR2_CONTRACT::QUALITATIVE;	break;
											case "quan":	$bitwise_value = RGSTR2_CONTRACT::QUANTITATIVE;	break;
										}
										$new_val = 0;
										if(($v & $bitwise_value) == $bitwise_value ) {
											$new_val = 1;
										}
										$old_val = 0;
										if(($old[$fld] & $bitwise_value) == $bitwise_value) {
											$old_val = 1;
										}
										if($old_val != $new_val) {
											$changes[$sfld] = array('to'=>$new_val,'from'=>$old_val);
										}
										break;
									default:
										if($old[$sfld]!=$insert_data[$sfld]) {
											$changes[$sfld] = array('to'=>$insert_data[$sfld],'from'=>$old[$sfld]);
										}
										break;
								}
							}
						} elseif($fld=="contract_assess_status") {
							$listObject = new RGSTR2_LIST("deliverable_status");
							$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
							unset($listObject);
							foreach($list_items as $key => $status) {
								$bitwise_value = pow(2,$key);
								$new_val = 0;
								if(($v & $bitwise_value) == $bitwise_value ) {
									$new_val = 1;
								}
								$old_val = 0;
								if(($old[$fld] & $bitwise_value) == $bitwise_value) {
									$old_val = 1;
								}
								if($old_val != $new_val) {
									$changes[$fld][$key] = array('to'=>$new_val,'from'=>$old_val);
								}
							}
						}
					} else {
						$changes[$fld] = array('to'=>$v,'from'=>$old[$fld]);
					}
				}
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getAttachmentFieldName()] = $x;
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::EDIT,
				'attachment'	=> '',
			);

            if(!($this->getMyObjectType() == 'REGISTER' || $this->getMyObjectType() == 'RISK' || $this->getMyObjectType() == 'ACTION')){
                $log_var['log_object_type'] = $this->getMyObjectType();
            }

            if($this->getTableField() == 'action'){
                $log_var['progress'] = $old[$this->getTableField()."_progress"];
            }

            if($this->getTableField() == 'action' || $this->getTableField() == 'rsk'){
                $log_var['status_id'] = $old[$this->getTableField()."_status"];
            }

            $note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());

			$this->addActivityLog($this->getMyLogTable(), $log_var);

			return array("ok",$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been updated successfully.",array(/*$log_var*/array('what it do'),$this->getMyObjectType()));
		}
		return array("info","No changes were found to be saved.  Please try again.");
	}

   
   
   
   
   
   
   

    
    /********************************************
     * PROTECTED functions
     */


	 
	 
	 
	 
	 
	 
	 
	/*******************************************
     * PRIVATE functions
     */
     
     
     
     
     
     
   
   
   
   
   
   
   
   

     
     public function __destruct() {
     	parent::__destruct();
     }
     
     
     
	
}






?>