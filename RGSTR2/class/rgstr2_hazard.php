<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_HAZARD extends RGSTR2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "HAZ";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "HAZARD";
    const OBJECT_NAME = "hazard";
	const PARENT_OBJECT_TYPE = null;
     
    const TABLE = "hazard";
    const TABLE_FLD = "haz";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		//$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
		unset($var['object_id']); //remove incorrect field value from add form
		
//		$competency_proficiency = $var['cp'];
//		unset($var['cp']);

        //Minor hack
        unset($var['value']);

		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_status'] = RGSTR2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

        //Set my own insert vars
        $insert_vars = array();

        $insert_vars[$this->getTableField().'_ref'] = $var[$this->getTableField().'_ref'];
        $insert_vars[$this->getTableField().'_name'] = $var[$this->getTableField().'_name'];
        $insert_vars[$this->getTableField().'_description'] = $var[$this->getTableField().'_description'];
        $insert_vars[$this->getTableField().'_type'] = $var[$this->getTableField().'_type'];
        $insert_vars[$this->getTableField().'_category'] = $var[$this->getTableField().'_category'];


        $insert_vars[$this->getTableField().'_status'] = $var[$this->getTableField().'_status'];
        $insert_vars[$this->getTableField().'_insertdate'] = $var[$this->getTableField().'_insertdate'];
        $insert_vars[$this->getTableField().'_insertuser'] = $var[$this->getTableField().'_insertuser'];

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_vars);
		$id = $this->db_insert($sql);
		if($id>0) {
//			$cpObject = new RGSTR2_COMPETENCY_PROFICIENCY();
//			foreach($competency_proficiency as $prof_id => $name) {
//				$cpObject->addObject(array(),$id,$prof_id,$name);
//			}
			
			$changes = array(
                'response'=>"|hazard| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> RGSTR2_LOG::CREATE,
                'attachment'	=> '',
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}




	public function editObject($var) {
		$extra_logs = array();
		$result = $this->editMyObject($var,$extra_logs);
		return $result;
	}

	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = ".self::DELETED." 
				WHERE ".$this->getIDFieldName()." = ".$var['object_id'];
		$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $var['object_id'],
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> RGSTR2_LOG::DELETE,
                'attachment'	=> '',
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
		return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$var['object_id']." deleted successfully.");
	}


    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }

	public function getList($section,$options) {
		return $this->getMyHazardList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getHazardDetails($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	
	public function getOrderedObjects($object_id=0) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, ".$this->getNameFieldName()." as name
					, ".$this->getTableField()."_description as description
					, ".$this->getTableField()."_ref as ref
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					FROM ".$this->getTableName()." A
					WHERE  ".($this->checkIntRef($object_id) ? $this->getIDFieldName()." = ".$object_id." AND " : "")." ".$this->getActiveStatusSQL("A"); //echo $sql;
				$res2 = $this->mysql_fetch_all_by_id($sql, "id");
		return $res2;
	}

/**
 * Function to get details for viewing / confirmation
 */
	public function getOrderedObjectsWithChildren($object_id=0){
		$res = $this->getOrderedObjects($object_id);
		$data = array(
			$this->getMyObjectType()=>$res,
		);
		
		$goalObject = new RGSTR2_SUBFUNCTION();
		$goal_objects = $goalObject->getOrderedObjects($object_id);
		$data[$goalObject->getMyObjectType()] = $goal_objects;
		
		$resultsObject = new RGSTR2_ACTIVITY();
		$results_objects = $resultsObject->getRawOrderedObjects($object_id);
		$data[$resultsObject->getMyObjectType()] = $results_objects;
		
		return $data;
	}


	
	public function getSimpleDetails($id=0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'id'=>$id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"])>0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}
	

	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }

	public function getHazardsForSelect(){
        $html = '';
        $hazards_list = $this->getList('new', array());
        $hazards = $hazards_list['rows'];

        $sorted_haz_array = array();
        foreach($hazards as $key => $val){
            $sorted_haz_array[$val['haz_name']['display']] = $key;
        }

        ksort($sorted_haz_array);

        $html .= '<option value="X">--- SELECT ---</option>';
        foreach($sorted_haz_array as $key => $val){
            $html .= '<option value="' . $val . '">' . $key . '</option>';
        }
	    return $html;
    }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>