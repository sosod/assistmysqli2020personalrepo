<?php
/**
 * To manage the contract admins
 * 
 * Created on: September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * 
 */

 
class RGSTR2_CONTRACT_OWNER extends RGSTR2 {
	
	private $parent_table;
	private $child_table;
	private $owner_table;
	
	
	
	const CAN_CREATE_DELIVERABLE = 128;
	const CAN_UPDATE = 256;
	const CAN_EDIT = 512;
	const CAN_APPROVE = 1024;
	
	
	public function __construct() {
		parent::__construct();
		$this->parent_table = "assist_".$this->getCmpCode()."_list_dir";
		$this->child_table = "assist_".$this->getCmpCode()."_list_dirsub";
		$this->owner_table = $this->getDBRef()."_list_contract_owner";
	}	
	
	
	/************************************
	 * CONTROLLER functions
	 */
	public function addObject($var){
        $var['create'] = 0;//Manage Create Hack, turn it into zero, return it to it natural state when the time comes to do it
		$status = self::ACTIVE + $var['create'] + $var['update'] + $var['edit'] + $var['approve'];
		if($status==self::ACTIVE) {
			return array("error","User must have at least 1 Manage Access granted to them");
		} else {
			$var['owner_status'] = $status;
			unset($var['update']);
			unset($var['edit']);
			unset($var['approve']);
			unset($var['create']);
			$mid = $this->addOwner($var);
			//If successfull then the mysql_insert_id returned will be > 0
			if($mid>0){
				//Add log record
				$dept_name = $this->getAMasterDepartmentName($var['owner_subid']);
				$user_name = $this->getAUserName($var['owner_tkid']);
				$changes = array(
					'user'=>$this->getUserName(),
					'response'	=> "Added ".$user_name." as an administrator to ".$dept_name,
				);
				$log_var = array(
					'section'	=> "OWNER",
					'object_id'	=> $mid,
					'changes'	=> $changes,
					'log_type'	=> RGSTR2_LOG::CREATE,
				);
				$this->addActivityLog("setup", $log_var);
									
				return array("kk",$user_name." was added successfully to ".$dept_name.".");
			} else{
				return array("error","Something went wrong.  Please try again");
			}
		}
		return array('info',"Don't know what you want to add");
	}


    public function editObject($var){
        $admins_list = $this->getAllActiveAdmins();

        $dept_id = $var['dept_id'];
        $record_id = $var['record_id'];

        $user_record = $admins_list[$dept_id][$record_id];

        $current_user_status = $user_record['status'] * 1;

        $current_update_boolean = ( ($current_user_status & RGSTR2_CONTRACT_OWNER::CAN_UPDATE)==RGSTR2_CONTRACT_OWNER::CAN_UPDATE ? 1 : 0 );
        $current_edit_boolean = ( ($current_user_status & RGSTR2_CONTRACT_OWNER::CAN_EDIT)==RGSTR2_CONTRACT_OWNER::CAN_EDIT ? 1 : 0 );
        $current_approve_boolean = ( ($current_user_status & RGSTR2_CONTRACT_OWNER::CAN_APPROVE)==RGSTR2_CONTRACT_OWNER::CAN_APPROVE ? 1 : 0 );

        $var['create'] = 0;//Manage Create Hack, turn it into zero, return it to it natural state when the time comes to do it
        $update_status = $current_user_status;


        $form_update_boolean = $var['update'] * 1;
        $form_edit_boolean = $var['edit'] * 1;
        $form_approve_boolean = $var['approve'] * 1;

        if($form_update_boolean != $current_update_boolean){
            if($form_update_boolean == 1){
                $update_status += RGSTR2_CONTRACT_OWNER::CAN_UPDATE;
            }else{
                $update_status -= RGSTR2_CONTRACT_OWNER::CAN_UPDATE;
            }
        }

        if($form_edit_boolean != $current_edit_boolean){
            if($form_edit_boolean == 1){
                $update_status += RGSTR2_CONTRACT_OWNER::CAN_EDIT;
            }else{
                $update_status -= RGSTR2_CONTRACT_OWNER::CAN_EDIT;
            }
        }

        if($form_approve_boolean != $current_approve_boolean){
            if($form_approve_boolean == 1){
                $update_status += RGSTR2_CONTRACT_OWNER::CAN_APPROVE;
            }else{
                $update_status -= RGSTR2_CONTRACT_OWNER::CAN_APPROVE;
            }
        }

        if($update_status==self::ACTIVE) {
            return array("error","User must have at least 1 Manage Access granted to them");
        } else {

            $update_var['owner_status'] = $update_status;

//            return array("ok", "data" => $var);

            unset($var['update']);
            unset($var['edit']);
            unset($var['approve']);
            unset($var['create']);

            $sql = "UPDATE ".$this->getOwnerTable()." SET ".$this->convertArrayToSQL($update_var)." WHERE owner_id = ".$record_id;
            $mid = $this->db_update($sql);

            //If successfull then the mysql_insert_id returned will be > 0
            if($mid>0){
                //Add log record
                $dept_name = $this->getAMasterDepartmentName($user_record['dept_id']);
                $user_name = $this->getAUserName($user_record['tkid']);
                $changes = array(
                    'user'=>$this->getUserName(),
                    'response'	=> "Administrator to ".$dept_name . ", " . $user_name . " has been edited.",
                );
                $log_var = array(
                    'section'	=> "OWNER",
                    'object_id'	=> $mid,
                    'changes'	=> $changes,
                    'log_type'	=> RGSTR2_LOG::EDIT,
                );
                $this->addActivityLog("setup", $log_var);

                return array("ok","Administrator to ".$dept_name . ", " . $user_name . " has been edited.");
            } else{
                return array("error","Something went wrong.  Please try again");
            }
        }
        return array('info',"Don't know what you want to add");
    }


    public function deleteObject($var){
	    $admins_list = $this->getAllActiveAdmins();

        $dept_id = $var['dept_id'];
        $record_id = $var['record_id'];

        $user_record = $admins_list[$dept_id][$record_id];

        $update_status = 0;
        $update_var['owner_status'] = $update_status;

        $sql = "UPDATE ".$this->getOwnerTable()." SET ".$this->convertArrayToSQL($update_var)." WHERE owner_id = ".$record_id;
        $mid = $this->db_update($sql);

        if($mid>0){
            //Add log record
            $dept_name = $this->getAMasterDepartmentName($user_record['dept_id']);
            $user_name = $this->getAUserName($user_record['tkid']);
            $changes = array(
                'user'=>$this->getUserName(),
                'response'	=> "Administrator to ".$dept_name . ", " . $user_name . " has been deleted.",
            );
            $log_var = array(
                'section'	=> "OWNER",
                'object_id'	=> $mid,
                'changes'	=> $changes,
                'log_type'	=> RGSTR2_LOG::DELETE,
            );
            $this->addActivityLog("setup", $log_var);

            return array("ok","Administrator to ".$dept_name . ", " . $user_name . " has been successfully deleted.");
        } else{
            return array("error","Something went wrong.  Please try again");
        }
    }
	
	
	
	
	/***********************************
	 * GET functions
	 */
	/** 
	 * Get list of departments with count of admins
	 */
	public function getActiveOwners($ids="") {
		$sql = "SELECT subid as id, CONCAT(dirtxt,' - ',subtxt) as name , dirtxt as parent, subtxt as child
				FROM ".$this->getChildTable()."
				INNER JOIN ".$this->getParentTable()."
				  ON subdirid = dirid AND diryn = 'Y' 
				WHERE subyn = 'Y' ";
		if(is_array($ids)) {
			$sql.=" AND subid IN (".implode(",",$ids).")";
		} elseif(strlen($ids)>0) {
			$sql.= " AND subid = $ids ";
		}
		$sql.= "
				ORDER BY dirsort,subsort";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}
	public function getActiveOwnersFormattedForSelect($ids="") {
		return $this->formatRowsForSelect($this->getActiveOwners($ids));
	}
	public function getItemsForReport() {
		$contractObject = new RGSTR2_REGISTER();
		$data = array();
		$sql = "SELECT subid as id, CONCAT(dirtxt,' - ',subtxt) as name , dirtxt as parent, subtxt as child
				FROM ".$this->getChildTable()."
				INNER JOIN ".$this->getParentTable()."
				  ON subdirid = dirid
				WHERE subid IN (SELECT DISTINCT ".$contractObject->getOwnerFieldName()." FROM ".$contractObject->getTableName().")
				ORDER BY dirsort,subsort";
		$rows = $this->mysql_fetch_all_by_id($sql, "id"); 
		foreach($rows as $id => $r) {
			$data[$id] = $r['name'];
		}
		return $data;
	}
	/*************
	 * MASTER DEPARTMENT FUNCTIONS
	 */
	public function getParentTable() { return $this->parent_table; }
	public function getChildTable() { return $this->child_table; }
	public function getActiveMasterDepartmentsList() {
		$sql = "SELECT subid as id, CONCAT(dirtxt,' - ',subtxt) as name , dirtxt as parent, subtxt as child
				FROM ".$this->getChildTable()."
				INNER JOIN ".$this->getParentTable()."
				  ON subdirid = dirid AND diryn = 'Y' 
				WHERE subyn = 'Y' 
				ORDER BY dirsort,subsort";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}
	public function getAMasterDepartment($id) {
		$sql = "SELECT subid as id, CONCAT(dirtxt,' - ',subtxt) as name , dirtxt as parent, subtxt as child
				FROM ".$this->getChildTable()."
				INNER JOIN ".$this->getParentTable()."
				  ON subdirid = dirid AND diryn = 'Y' 
				WHERE subyn = 'Y' AND subid = $id
				ORDER BY dirsort,subsort";
		return $this->mysql_fetch_one($sql);
	}
	public function getAMasterDepartmentName($id) {
		$d = $this->getAMasterDepartment($id);
		return $d['name'];
	}
	/*************
	 * ADMINS FUNCTIONS
	 */
	public function getOwnerTable() { return $this->owner_table; }
	public function getAllActiveAdmins() {
		$sql = "SELECT owner_id as id, owner_subid as dept_id, CONCAT(tkname, ' ', tksurname) as name , tkid, owner_status as status
				FROM ".$this->getOwnerTable()."
				INNER JOIN assist_".$this->getCmpCode()."_timekeep 
				  ON tkid = owner_tkid AND tkstatus = 1 
				WHERE owner_status & ".RGSTR2::ACTIVE." = ".RGSTR2::ACTIVE." 
				ORDER BY tkname, tksurname";
		$admins = $this->mysql_fetch_all_by_id2($sql, "dept_id", "id");
		return $admins;
	}
	public function addOwner($var){
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getOwnerTable()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		return $id;
	}
	/**
	 * Function to get the users responsible for a specific department (Contract Owner) as well as their access settings to that department
	 * @param (INT) id of department/section (contract_owner_id field) to be checked
	 * 
	 * @return (ARRAY) array of users in following format:
	 * 		a[tkid] = array(
	 * 			'id'=>user's tkid (same as key) e.g. 0002
	 * 			'name'=> user's name e.g. Mike Billing,
	 * 			'status'=> unprocessed bitwise status e.g. 1536 (edit+approve)
	 * 			'can_create_deliverables'=>true/false (bitwise status processed against CAN_CREATE_DELIERABLES const)
	 * 			'can_update'=>true/false (bitwise status processed against CAN_UPDATE const)
	 * 			'can_edit'=>true/false (bitwise status processed against CAN_EDIT const)
	 * 			'can_approve'=>true/false (bitwise status processed against CAN_APPROVE const)
	 * 		);
	 */
	public function getActiveAdminsForOwner($dept_id) {
		if($this->checkIntRef($dept_id)) {
			$sql = "SELECT owner_tkid as id
						, CONCAT(tkname, ' ', tksurname) as name 
						, owner_status as status
						, IF((owner_status & ".self::CAN_UPDATE.") = ".self::CAN_UPDATE." , 1, 0) as can_update
						, IF((owner_status & ".self::CAN_EDIT.") = ".self::CAN_EDIT." , 1, 0) as can_edit
						, IF((owner_status & ".self::CAN_APPROVE.") = ".self::CAN_APPROVE." , 1, 0) as can_approve
					FROM ".$this->getOwnerTable()."
					INNER JOIN assist_".$this->getCmpCode()."_timekeep 
					  ON tkid = owner_tkid AND tkstatus = 1 
					WHERE owner_status & ".RGSTR2::ACTIVE." = ".RGSTR2::ACTIVE."
					AND owner_subid = $dept_id 
					ORDER BY tkname, tksurname";
			$admins = $this->mysql_fetch_all_by_id($sql, "id");
			foreach($admins as $key => $a) {
				$admins[$key]['can_create_deliverables'] = ( ($a['status'] & self::CAN_CREATE_DELIVERABLE) == self::CAN_CREATE_DELIVERABLE);
				$admins[$key]['can_update'] = ( ($a['status'] & self::CAN_UPDATE) == self::CAN_UPDATE);
				$admins[$key]['can_edit'] = ( ($a['status'] & self::CAN_EDIT) == self::CAN_EDIT);
				$admins[$key]['can_approve'] = ( ($a['status'] & self::CAN_APPROVE) == self::CAN_APPROVE);
			}
		} else {
			$admins = array();
		}
		return $admins;
	}
	
	/**
	 * Check ability to perform activities on depts
	 */
	public function checkIfCanCreate() {
		$status = self::CAN_CREATE_DELIVERABLE;
		$c = $this->runAdminCheck($status);
		return ($c>0);
	}
	public function checkIfCanUpdate() {
		$status = self::CAN_UPDATE;
		$c = $this->runAdminCheck($status);
		return ($c>0);
	}
	public function checkIfCanEdit() {
		$status = self::CAN_EDIT;
		$c = $this->runAdminCheck($status);
		return ($c>0);
	}
	public function checkIfCanApprove() {
		$status = self::CAN_APPROVE;
		$c = $this->runAdminCheck($status);
		return ($c>0);
	}
	public function runAdminCheck($status, $count_only=true) {
		$sql = "SELECT ".($count_only==true ? "count(owner_id) as count" : "*")." FROM ".$this->getOwnerTable()." 
				WHERE (owner_tkid = '".$this->getUserID()."' AND (owner_status & ".self::ACTIVE.") = ".self::ACTIVE.") 
				AND ( (owner_status & ".$status.") = ".$status.") ";
		if($count_only==true) {
			$row = $this->mysql_fetch_one($sql);
			$data = $row['count'];
		} else {
			$data = $this->mysql_fetch_all($sql);
		}
		return $data;
	}
	
	
	
	
	
	/***********************************
	 * SET / UPDATE functions
	 */
	
	
	
	
	
	
	
	
	
	
	/***********************************
	 * PROTECTED functions
	 */
	
	
	
	
	
	
	
	
	
	
	
	/***********************************
	 * PRIVATE functions
	 */


}
?>