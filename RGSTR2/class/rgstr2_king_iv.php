<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_KING_IV extends RGSTR2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "KIV";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "KING_IV";
    const OBJECT_NAME = "king_iv";
	const PARENT_OBJECT_TYPE = null;
     
    const TABLE = "king_iv";
    const TABLE_FLD = "kiv";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		//$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
		unset($var['object_id']); //remove incorrect field value from add form
		
//		$competency_proficiency = $var['cp'];
//		unset($var['cp']);

        //Minor hack
        unset($var['value']);

		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_status'] = RGSTR2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

        //Set my own insert vars
        $insert_vars = array();

        $insert_vars[$this->getTableField().'_ref'] = $var[$this->getTableField().'_ref'];
        $insert_vars[$this->getTableField().'_name'] = $var[$this->getTableField().'_name'];
        $insert_vars[$this->getTableField().'_description'] = $var[$this->getTableField().'_description'];

        $insert_vars[$this->getTableField().'_status'] = $var[$this->getTableField().'_status'];
        $insert_vars[$this->getTableField().'_insertdate'] = $var[$this->getTableField().'_insertdate'];
        $insert_vars[$this->getTableField().'_insertuser'] = $var[$this->getTableField().'_insertuser'];

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_vars);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
                'response'=>"|hazard| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> RGSTR2_LOG::CREATE,
                'attachment'	=> '',
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}




	public function editObject($var) {
		$extra_logs = array();
		$result = $this->editMyObject($var,$extra_logs);
		return $result;
	}

	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = ".self::DELETED." 
				WHERE ".$this->getIDFieldName()." = ".$var['object_id'];
		$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $var['object_id'],
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> RGSTR2_LOG::DELETE,
                'attachment'	=> '',
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
		return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$var['object_id']." deleted successfully.");
	}


    public function buildKingIVCheckboxedTableForRiskLink() {
        $left_joins = array();

        $headObject = new RGSTR2_HEADINGS();
        $final_data = array('head'=>array(),'rows'=>array());

        //set up variables
        $idp_headings = $headObject->getMainObjectHeadings("KING_IV","FULL","MANAGE");

        $idpObject = $this;

        $idp_tblref = $idpObject->getMyObjectType();
        $idp_table = $idpObject->getTableName();
        $idp_status = $idp_tblref.".".$idpObject->getStatusFieldName();

        $where = $idpObject->getActiveStatusSQL($idp_tblref);

        $id_fld = $idpObject->getIDFieldName();
        $sql = "SELECT DISTINCT $idp_status as my_status, ".$idp_tblref.".*";
        $from = " $idp_table $idp_tblref 
					";
        $final_data['head'] = $idp_headings;
        $ref_tag = $idpObject->getRefTag();
        $group_by = "";

        $all_headings = array_merge($idp_headings);

        $listObject = new RGSTR2_LIST();

        foreach($all_headings as $fld => $head) {
            $lj_tblref = $head['section'];
            if($head['type']=="LIST") {
                $listObject->changeListType($head['list_table']);
                $sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
                $left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
										AND (".$head['list_table'].".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
            } elseif($head['type']=="MASTER") {
                $tbl = $head['list_table'];
                $masterObject = new RGSTR2_MASTER($fld);
                $fy = $masterObject->getFields();
                $sql.=", ".$fld.".".$fy['name']." as ".$tbl;
                $left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
            } elseif($head['type']=="USER") {
                $sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
                $left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
            }
        }
        $sql.= " FROM ".$from.implode(" ",$left_joins);
        $sql.= " WHERE ".$where;

        $sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";
        $sql.=" ORDER BY " . $id_fld;
        $rows = $this->mysql_fetch_all_by_id($sql,$id_fld);

        $final_data = $this->formatKingIVRowDisplay($rows, $final_data,$id_fld,$headObject,$ref_tag,false);


        $final_data['head'] = $this->replaceObjectNames($final_data['head']);

        //Now build the table
        $display_object = new RGSTR2_DISPLAY();
        //Boolean Field
        $boolean_field_type = "BOOL_BUTTON";
        $use_options = array();
        $use_options['req'] = "1";
        $use_options['yes'] = 1;
        $use_options['no'] = 0;
        $use_options['extra'] = "boolButtonClickExtra";

        $val = "0";

        //Text Field Stuff
        $text_field_type = "TEXT";
        $justification_options = array();
        $justification_options['req'] = "1";

        $form_javascript = '';

        $king_iv_table = '<table>';

        $king_iv_table .= '<tr>';
        foreach($final_data['head'] as $table_field_name => $heading_record){
            if($table_field_name != 'kiv_description'){
                $king_iv_table .= '<th>' . $heading_record['name'] . '</th>';
            }
        }
        $king_iv_table .= '<th>Use</th>';
        $king_iv_table .= '<th>Justification</th>';
        $king_iv_table .= '</tr>';

        foreach($final_data['rows'] as $principle_id => $record_values){
            $king_iv_table .= '<tr>';
            foreach($final_data['head'] as $table_field_name => $heading_record){
                if($table_field_name != 'kiv_description'){
                    $king_iv_table .= '<td>' . $record_values[$table_field_name]['display'] . '</td>';
                }
            }
            //Boolean Field Stuff
            $input_id_and_name = 'use_king_iv_' . $principle_id;
            $use_options['id'] = $input_id_and_name;
            $use_options['name'] = $input_id_and_name;
            $boolean_form_field = $display_object->createFormField($boolean_field_type,$use_options,$val);
            $form_javascript .= $boolean_form_field['js'];
            $king_iv_table .= '<td>' . $boolean_form_field['display'] . '</td>';

            //Text Field Stuff
            $input_id_and_name = 'king_iv_justification_' . $principle_id;
            $justification_options['id'] = $input_id_and_name;
            $justification_options['name'] = $input_id_and_name;
            $text_form_field = $display_object->createFormField($text_field_type,$justification_options,'');
            $form_javascript .= $text_form_field['js'];
            $king_iv_table .= '<td>' . $text_form_field['display'] . '</td>';

            $king_iv_table .= '</tr>';
        }

        $king_iv_table .= '</table>';



        $html = '<tr id="principles">';
        $html .= '<th class="" width="40%">King IV Principles:</th>';
        $html .= '<td>';
        $html .= $king_iv_table;
        $html .= '</td>';
        $html .= '</tr>';

        $opening_script_tag = '<script id="king_iv_script">';
        $closing_script_tag = '</script>';

        $javascript_element = $opening_script_tag;
        $javascript_element .= $form_javascript;
        $javascript_element .= $closing_script_tag;

        $king_iv_array = array(
                            'html' => $html,
                            'javascript' => $javascript_element,
        );

        return $king_iv_array;
    }

    public function formatKingIVRowDisplay($rows,$final_data,$id_fld,$headObject,$ref_tag,$status_id_fld){
        $displayObject = new RGSTR2_DISPLAY();
        foreach($rows as $r) {
            $row = array();
            $i = $r[$id_fld];
            foreach($final_data['head'] as $fld=> $head) {
                if($head['parent_id']==0){
                    if($headObject->isListField($head['type']) && $head['type']!="DELIVERABLE") {
                        $row[$fld] = strlen($r[$head['list_table']])>0 ? $r[$head['list_table']] : $this->getUnspecified();
                        if($status_id_fld!==false && $fld==$status_id_fld) {
                            if(($r['my_status'] & RGSTR2::AWAITING_APPROVAL) == RGSTR2::AWAITING_APPROVAL) {
                                $row[$fld].=" (Awaiting approval)";
                            } elseif(($r['my_status'] & RGSTR2::APPROVED) == RGSTR2::APPROVED) {
                                $row[$fld].= " (Approved)";
                            }
                        }
                    } elseif($this->isDateField($fld)) {
                        $row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
                    } else {
                        //$row[$fld] = $r[$fld];
                        $row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
                    }
                }
            }
            $final_data['rows'][$i] = $row;
        }
        return $final_data;
    }


    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }

	public function getList($section,$options) {
		return $this->getMyHazardList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getHazardDetails($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	
	public function getOrderedObjects($object_id=0) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, ".$this->getNameFieldName()." as name
					, ".$this->getTableField()."_description as description
					, ".$this->getTableField()."_ref as ref
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					FROM ".$this->getTableName()." A
					WHERE  ".($this->checkIntRef($object_id) ? $this->getIDFieldName()." = ".$object_id." AND " : "")." ".$this->getActiveStatusSQL("A"); //echo $sql;
				$res2 = $this->mysql_fetch_all_by_id($sql, "id");
		return $res2;
	}

/**
 * Function to get details for viewing / confirmation
 */
	public function getOrderedObjectsWithChildren($object_id=0){
		$res = $this->getOrderedObjects($object_id);
		$data = array(
			$this->getMyObjectType()=>$res,
		);
		
		$goalObject = new RGSTR2_SUBFUNCTION();
		$goal_objects = $goalObject->getOrderedObjects($object_id);
		$data[$goalObject->getMyObjectType()] = $goal_objects;
		
		$resultsObject = new RGSTR2_ACTIVITY();
		$results_objects = $resultsObject->getRawOrderedObjects($object_id);
		$data[$resultsObject->getMyObjectType()] = $results_objects;
		
		return $data;
	}


	
	public function getSimpleDetails($id=0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'id'=>$id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"])>0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}
	

	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }

	public function getHazardsForSelect(){
        $html = '';
        $hazards_list = $this->getList('new', array());
        $hazards = $hazards_list['rows'];

        $html .= '<option value="X">--- SELECT ---</option>';
        foreach($hazards as $key => $val){
            $html .= '<option value="' . $key . '">' . $val['haz_name']['display'] . '</option>';
        }
	    return $html;
    }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>