<?php
/**
 * To manage the DELIVERABLE object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_RISK extends RGSTR2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $progress_status_field = "_status_id";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $name_field = "_name";
	protected $deadline_field = "_deadline";
	protected $owner_field = "_owner";
	protected $progress_field = "_progress";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	
	protected $summary_heading_fields = array(
		"del_id",
		"del_name",
		"del_owner",
		"del_deadline",
		"del_category_id",
	);

	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "DELIVERABLE";
    const TABLE = "deliverable";
    const TABLE_FLD = "del";
    const REFTAG = "CD";
	const LOG_TABLE = "deliverable";
    
    public function __construct($del_id=0) {
        parent::__construct();
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		
		$this->object_form_extra_js = "
	
	var del_legislation_section_value = $('#del_legislation_section_id').val(); 
	//console.log('del_legislation_section_value = '+del_legislation_section_value);
	var del_legislation_section_options = [];
	$('#del_legislation_section_id option:gt(0)').each(function() {
		del_legislation_section_options.push({value:$(this).val(), text:$(this).text(), list_num: $(this).attr('list_num'), full: $(this).attr('full')});
	});
	$('#del_legislation_id').change(function() {
		if($(this).val()=='X' || $(this).val()=='0') {
			$('#del_legislation_section_id option:gt(0)').remove();
			$('#del_legislation_section_id').val($('#del_legislation_section_id option:first').val());
		} else {
			var v = $(this).val();
			$('#del_legislation_section_id option:gt(0)').remove();
			$.each(del_legislation_section_options, function(i){
				var opt = del_legislation_section_options[i];
				if(v==opt.list_num) {
					$('#del_legislation_section_id').append(
						$('<option />',{val:opt.value, text:opt.text, full:opt.full})
					);
				}
			});
			$('#del_legislation_section_id').val($('#del_legislation_section_id option:first').val());
		}
		$('#del_legislation_section_id').trigger('change');
	});
	//$('#del_legislation_id').trigger('change');
	//console.log('after change '+$('#del_legislation_section_id').val());
	//$('#del_legislation_section_id').val(del_legislation_section_value);
	//console.log('after reset '+$('#del_legislation_section_id').val());
	//$('#del_legislation_section_id').val(del_legislation_section_value);
	
//variable to remember the previous progress value when defaulting to 100
var old_progress = $('#".$this->getProgressFieldName()."').val();
 
//on change of the status drop down
$('#".$this->getProgressStatusFieldName()."').change(function() {
                //get the selected option value
                var v = ($(this).val())*1;
                //if it is set to \"Completed\" (always id=3 except deliverable which is 5)
                if(v==5) {
                                //if the progress field is not already set to 100, then remember the current value
                                if(($('#".$this->getProgressFieldName()."').val())*1!=100) {
                                                old_progress = $('#".$this->getProgressFieldName()."').val();
                                }
                                //update progress field to 100 and disable
                                $('#".$this->getProgressFieldName()."').val('100').prop('disabled',true);
                //else if the status field is not Completed, check if the progress field was previously disabled
                } else if($('#".$this->getProgressFieldName()."').prop('disabled')==true) {
                                //cancel the disable and reset the value back to the last remembered value
                                $('#".$this->getProgressFieldName()."').val(old_progress).prop('disabled',false);
                }
});
	//console.log('after status function '+$('#del_legislation_section_id').val());
	";
		
		
		if($del_id>0) {
			$this->object_id = $del_id;
			$this->object_details = $this->getRawObject($del_id);
		}
    }
    
    
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['deliverable_id']); //remove incorrect deliverable_id value from add form
		unset($var['last_deliverable_status']); //remove incorrect deliverable_id value from add form
		unset($var['object_id']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_progress'] = 0;
		$var[$this->getTableField().'_status_id'] = 1;
		$var[$this->getTableField().'_status'] = RGSTR2::ACTIVE;

//DO ANY ADDITIONAL PROCESSING HERE!!!!

		
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|risk| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::CREATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
	//		$this->addActivityLog("deliverable", $log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
				
	//		$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
	//		return array("info", $note);
	//		return array("ok","New ".$this->getDeliverableObjectName()." ".$this->getRefTag().$id." has been successfully created.");
		}
		return array("error","Testing: ".$sql);
	}

	public function updateObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$progress = $var[$p_field];
		$status_id = $var[$si_field];
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$update_data = array(
			$p_field=>$progress,
			$si_field=>$status_id,
		);
		//if completed then send for approval
		if($progress==100 && $status_id==5) {
			$new_status = $old_data[$s_field] + RGSTR2::AWAITING_APPROVAL;
			$update_data[$s_field] = $new_status;
			if((strtotime($var['action_on']) != strtotime($old_data[$dc_field]))) {
				$update_data[$dc_field] = date("Y-m-d",strtotime($var['action_on']));
				$c[$dc_field] = array('to'=>date("Y-m-d",strtotime($var['action_on'])),'from'=>$old_data[$dc_field]);
			}
		} else {
			$new_status = $old_data[$s_field];
		}
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		
		if($mar>0) {
			$changes = array(
				'response'=>$var['response'],
				'user'=>$this->getUserName(),
				'action_on'=>$var['action_on'],
			);
			if($old_data[$p_field] != $progress) {
				$changes[$p_field] = array('to'=>$progress,'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $status_id) {
				$listObject = new RGSTR2_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($status_id,$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$status_id],'from'=>$items[$old_data[$si_field]]);
			}
			if(isset($c[$dc_field])) {
				$changes[$dc_field] = $c[$dc_field];
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getUpdateAttachmentFieldName()] = $x;
			if( ($new_status & RGSTR2::AWAITING_APPROVAL) == RGSTR2::AWAITING_APPROVAL) {
				$changes['status'] = "|risk| is now awaiting approval.";
			}
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::UPDATE,
				'progress'	=> $progress,
				'status_id'	=> $status_id,		
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." has been updated successfully.");
	//		ASSIST_HELPER::arrPrint($note['progress']);
	//		return array("info", $note);
		}
		
		return array("error","An error occurred.  Please try again.".$sql);
		
		
	}

	public function editObject($var,$attach=array()) {
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		$include = $var['include_rec'];
		unset($var['include_rec']);
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$result = $this->editMyObject($var,$attach,$recipients,$noter,$include);
	/*
		$result = $this->editMyObject($var,$attach);
		$for_note = $result[2];
		$for_note[0]['recipients']=$recipients;
		$for_note[0]['include']=$include;
		$note = $this->notify($for_note[0], $noter,$for_note[1]);
		unset($result[2]);
	*/
//		ASSIST_HELPER::arrPrint($note);	
//		return $note;
//	return array("info",json_encode($var));
		return $result;
	}




	public function approveObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - RGSTR2::AWAITING_APPROVAL + RGSTR2::APPROVED;
		
		$update_data = array(
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			$changes['status'] = "|risk| has been approved.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::APPROVE,
				'progress'	=> $old_data[$p_field],
				'status_id'	=> $old_data[$si_field],		
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			
			/** NOTIFY DELIVERABLE OWNER THAT DELIVERABLE HAS BEEN APPROVED **/
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			return array("ok",$this->getDeliverableObjectName()." ".$this->getRefTag().$object_id." has been successfully approved.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}

	public function declineObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - RGSTR2::AWAITING_APPROVAL;
		
		$update_data = array(
			$p_field=>"99",
			$si_field=>"2",
			$dc_field=>"0000-00-00",
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			if($old_data[$p_field] != $update_data[$p_field]) {
				$changes[$p_field] = array('to'=>$update_data[$p_field],'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $update_data[$si_field]) {
				$listObject = new RGSTR2_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($update_data[$si_field],$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$update_data[$si_field]],'from'=>$items[$old_data[$si_field]]);
			}
			if($old_data[$dc_field] != $update_data[$dc_field]) {
				$changes[$dc_field] = array('to'=>$update_data[$dc_field],'from'=>$old_data[$dc_field]);
			}
			$changes['status'] = "|risk| has been declined.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::DECLINE,
				'progress'	=> $update_data[$p_field],
				'status_id'	=> $update_data[$si_field],		
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			
			/** NOTIFY DELIVERABLE OWNER THAT DELIVERABLE HAS BEEN DECLINED **/
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			return array("ok",$this->getDeliverableObjectName()." ".$this->getRefTag().$object_id." has been successfully declined.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}



    public function unlockApprovedObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - RGSTR2::APPROVED;
		
		$update_data = array(
			$p_field=>"99",
			$si_field=>"2",
			$dc_field=>"0000-00-00",
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			if($old_data[$p_field] != $update_data[$p_field]) {
				$changes[$p_field] = array('to'=>$update_data[$p_field],'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $update_data[$si_field]) {
				$listObject = new RGSTR2_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($update_data[$si_field],$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$update_data[$si_field]],'from'=>$items[$old_data[$si_field]]);
			}
			if($old_data[$dc_field] != $update_data[$dc_field]) {
				$changes[$dc_field] = array('to'=>$update_data[$dc_field],'from'=>$old_data[$dc_field]);
			}
			$changes['status'] = "Approval of |risk| has been reversed.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> RGSTR2_LOG::UNLOCK,
				'progress'	=> $update_data[$p_field],
				'status_id'	=> $update_data[$si_field],		
			);
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			/** NOTIFY DELIVERABLE OWNER THAT APPROVAL OF DELIVERABLE HAS BEEN REVERSED **/
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			
			return array("ok",$this->getDeliverableObjectName()." ".$this->getRefTag().$object_id." has been successfully unlocked.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}



	
    /************************************
     * GET functions
     * */
    public function getSummaryFields() { return $this->summary_heading_fields; }
	public function getTableField() { return self::TABLE_FLD; }
	public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
	public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
	public function getParentID($i) {
		$sql = "SELECT del_contract_id FROM ".$this->getTableName()." WHERE del_id = ".$i;
		return $this->mysql_fetch_one_value($sql, "del_contract_id");
	}
	public function getDeliverablesForParentSelect($contract_id) {
		$sql = "SELECT D.del_id as id, D.del_name as name FROM ".$this->getTableName()." D WHERE D.del_contract_id = ".$contract_id." AND D.del_type = 'MAIN' AND ".$this->getActiveStatusSQL("D");
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
	}
	public function getDeliverablesWithDeadlineForParentSelect($contract_id) {
		$sql = "SELECT D.del_id as id, D.del_name as name, D.del_deadline as deadline FROM ".$this->getTableName()." D WHERE D.del_contract_id = ".$contract_id." AND D.del_type = 'MAIN' AND ".$this->getActiveStatusSQL("D");
		return $this->mysql_fetch_all_by_id($sql, "id");
	}

	public function getList($section,$options=array()) {
		return $this->getMyList("DELIVERABLE", $section,$options); 
	}
	
	public function getAObject($id,$options=array()) {
		return $this->getDetailedObject("DELIVERABLE", $id,$options);
/*		$tblref = "D";
		$headObject = new RGSTR2_HEADINGS();
		$headings = $headObject->getMainObjectHeadings("DELIVERABLE");
		$left_joins = array();
		$sql = "SELECT ".$tblref.".* ";
			foreach($headings['rows'] as $fld => $head) {
				if($head['type']=="LIST" && $head['parent_id']==0){
					$tbl = $head['list_table'];
					$listObject = new RGSTR2_LIST($tbl);
					$sql.= ", ".$listObject->getSQLName($tbl)." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." ".$tbl." 
										ON ".$tbl.".id = ".$tblref.".".$fld." 
										AND (".$tbl.".status & ".RGSTR2::DELETED.") <> ".RGSTR2::DELETED;
					unset($listObject); 
				} elseif($head['type']=="USER") {
					$sql.=", ".$tblref.".".$fld." as ".$fld."_tkid, CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$fld;
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$tblref.".".$fld." AND ".$fld.".tkstatus = 1";
				}
			}
		$sql.= " FROM ".$this->getTableName()." ".$tblref." ".implode(" ",$left_joins);
		$sql.= " WHERE ".$this->getTableField()."_id = ".$id;
		$object = $this->mysql_fetch_one($sql);
		return $object; */
	}     

	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id="",$parent_id="") {
		if(is_array($obj_id) && count($obj_id)>0) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE del_id IN (".implode(",",$obj_id).")";
			$data = $this->mysql_fetch_all($sql);
		} elseif(!is_array($obj_id) && strlen($obj_id)>0 && is_numeric($obj_id)) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE del_id = $obj_id ";
			$data = $this->mysql_fetch_one($sql);
		} elseif(is_array($parent_id) && count($parent_id)>0) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE del_contract_id IN (".implode(",",$parent_id).")";
			$data = $this->mysql_fetch_all($sql);
		} elseif(!is_array($parent_id) && strlen($parent_id)>0 && is_numeric($parent_id)) {
			$sql = "SELECT * FROM ".$this->getTableName()." WHERE del_id = $parent_id ";
			$data = $this->mysql_fetch_one($sql);
		} else {
			$data = array();
		}
		return $data;
	}
	
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		$data = array(
			$this->getUpdateAttachmentFieldName()=>$raw[$this->getUpdateAttachmentFieldName()],
			$this->getProgressFieldName()=>$raw[$this->getProgressFieldName()],
			$this->getProgressStatusFieldName()=>$raw[$this->getProgressStatusFieldName()],
		);
		return $data;
	}
		
	public function getOrderedObjects($contract_id){
		$actObject = new RGSTR2_ACTION();
		$act_tbl = $actObject->getTableName();
		$act_progress = $actObject->getProgressFieldName();
		$act_id = $actObject->getIDFieldName();
		$act_parent = $actObject->getParentFieldName();
		$sql = "SELECT del_id as id
						, del_type
						, del_parent_id as parent_id
						, del_name as name
						, CONCAT('".$this->getRefTag()."',del_id) as reftag
						, del_deadline as deadline 
						, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
						, AVG(A.".$act_progress.") as action_progress
						, COUNT(A.".$act_id.") as action_count
						FROM ".$this->getTableName()." D
						LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = D.".$this->getOwnerFieldName()." 
						LEFT OUTER JOIN ".$act_tbl." A ON A.".$act_parent." = D.".$this->getIDFieldName()." 
						WHERE D.del_contract_id = $contract_id AND ".$this->getActiveStatusSQL("D")."
						GROUP BY D.del_id
						ORDER BY del_deadline ASC, del_id ASC";
		//$res = $this->mysql_fetch_all_by_id($sql, "del_id");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		foreach($res2 as $p => $g) {
			foreach($g as $i => $d){
				if(is_null($d['action_progress']) || $d['action_count']==0) {
					if($d['del_type']=="MAIN") {
						$res2[$p][$i]['action_progress'] = "-";
					} else {
						$res2[$p][$i]['action_progress'] = "N/A";
					}
				}
			}
		}
		return $res2;
	}
	public function getCategoryGroupedOrderedObjects($contract_id){
		$actObject = new RGSTR2_ACTION();
		$act_tbl = $actObject->getTableName();
		$act_progress = $actObject->getProgressFieldName();
		$act_id = $actObject->getIDFieldName();
		$act_parent = $actObject->getParentFieldName();
		
		$res2 = array();
		$sql = "SELECT del_id as id
						, del_category_id as cate_id
						, del_type
						, del_parent_id as parent_id
						, del_name as name
						, CONCAT('".$this->getRefTag()."',del_id) as reftag
						, del_deadline as deadline 
						, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
						, AVG(A.".$act_progress.") as action_progress
						FROM ".$this->getTableName()." D
						LEFT OUTER JOIN ".$act_tbl." A ON A.".$act_parent." = D.".$this->getIDFieldName()." 
						LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = D.".$this->getOwnerFieldName()." 
						WHERE del_contract_id = $contract_id AND del_parent_id = 0 AND ".$this->getActiveStatusSQL("D")."
						GROUP BY D.del_id
						ORDER BY del_deadline ASC, del_id ASC";
		//$res = $this->mysql_fetch_all_by_id($sql, "del_id");
		$res2['main'] = $this->mysql_fetch_all_by_id2($sql,"cate_id", "id");
		$sql = "SELECT del_id as id
						, del_category_id as cate_id
						, del_type
						, del_parent_id as parent_id
						, del_name as name
						, CONCAT('".$this->getRefTag()."',del_id) as reftag
						, del_deadline as deadline 
						, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
						, AVG(A.".$act_progress.") as action_progress
						FROM ".$this->getTableName()." D
						LEFT OUTER JOIN ".$act_tbl." A ON A.".$act_parent." = D.".$this->getIDFieldName()." 
						LEFT JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = D.".$this->getOwnerFieldName()." 
						WHERE del_contract_id = $contract_id AND del_parent_id > 0 AND ".$this->getActiveStatusSQL("D")."
						GROUP BY D.del_id
						ORDER BY del_deadline ASC, del_id ASC";
		//$res = $this->mysql_fetch_all_by_id($sql, "del_id");
		$res2['subs'] = $this->mysql_fetch_all_by_id2($sql,"parent_id", "id");
		return $res2;
	}
	/***
	 * Returns an array of the IDs of deliverables associated with a contract
	 */
	public function getDelIds($obj_id, $fields=array()) {
		if(is_array($obj_id)) {
			$sql = "SELECT del_id FROM ".$this->getTableName()." WHERE del_contract_id IN (".implode(",",$obj_id).")";
			$data = $this->mysql_fetch_all($sql);				
		} else {
			$sql = "SELECT del_id ".(count($fields)>0?", ".implode(",",$fields):"")." FROM ".$this->getTableName()." WHERE del_contract_id = $obj_id ";
			$data = $this->mysql_fetch_all($sql);
		}
		return $data;
	}
	/***
	 * returns the display name of a specific deliverable
	 */
	public function getDeliverableNamesForSubs($ids) {
		$sql = "SELECT D.del_parent_id as id, CONCAT(P.del_name,' [".$this->getRefTag()."',D.del_parent_id,']') as name
				FROM  ".$this->getTableName()." D
				INNER JOIN ".$this->getTableName()." P ON D.del_parent_id = P.del_id
				WHERE ".(is_array($ids) ? "D.del_id IN ( ".implode(",",$ids)." ) " : "D.del_id = $ids ");
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");  //$this->arrPrint($data);
		return $data;
	}
     
	/**
	 * Returns status check for Deliverables which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
     
	 
	public function getDeadlineDate() {
		return $this->object_details['del_deadline'];
	}
	public function hasDeadline() { return true; }
	public function getDeadlineField() { return $this->deadline_field; }
	
	/**
	 * Function to fetch the recipients of the notifications which are about to go out
	 */
	public function getObjectRecipients($data){
	$id = $data['id'];
	$action = $data['activity'];
	$extra = strlen($data['extra'])>0 ? $data['extra'] : "";
	$recipients = $this->getRecipients($id, $action,true);
	$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
	if(strlen($extra)>0 && $extra !== $recipients[$owner] && $extra !== $this->getUserID()){
		$recipients["Old ".$owner]=$recipients[$owner];
		$recipients["New ".$owner]=$extra;
		unset($recipients[$owner]);
	}
	$mailObj = new ASSIST_MODULE_EMAIL("Deliverable","CD");
	$smsObj = new ASSIST_SMS();
	foreach($recipients as $key=>$val){
		if(is_array($val)){
			foreach($val as $index=>$thing){
				if($thing != $this->getUserID()){
					$adds = $mailObj->getEmailAddresses(array($thing), array());
					$phone = $smsObj->getMobilePhones(array($thing));
					$adds['to']['mobile'] = $phone[$thing]['num'];
					$adds['to']['tkid'] = $thing;
					foreach($adds['to'][0] as $a=>$b){
						$adds['to'][$a] = $b;
					}
					unset($adds['to'][0]);
					$recipes[$key][] = $adds['to'];
				}
			}
		}else{
			if($val != $this->getUserID()){
				$adds = $mailObj->getEmailAddresses(array($val), array());
				$phone = $smsObj->getMobilePhones(array($val));
				$adds['to']['mobile'] = $phone[$val]['num'];
				$adds['to']['tkid'] = $val;
				foreach($adds['to'][0] as $a=>$b){
					$adds['to'][$a] = $b;
				}
				unset($adds['to'][0]);
				$recipes[$key] = $adds['to'];
			}
		}
	}
	return $recipes;
	}

	public function getRecipients($id,$action,$mode=false){
		$headObj = new RGSTR2_HEADINGS();
		switch (strtoupper($action)) {
			case 'UNLOCK':
			case 'APPROVE':
			case 'DECLINE':
			case 'UPDATE':
			case 'EDIT':
			case 'NEW':
				$sql = "SELECT del_owner FROM ".$this->getTableName()." WHERE del_id = $id ";
				//$tkid[] = $this->mysql_fetch_one_value($sql, "del_owner");
				if($mode){
					//$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
					$owner = $headObj->getAHeadingNameByField("del_owner");
					$tkid[$owner] = $this->mysql_fetch_one_value($sql, "del_owner");
				}else{
					$tkid[] = $this->mysql_fetch_one_value($sql, "del_owner");
				}
				$parent_id = $this->getParentID($id);
				$conObject = new RGSTR2_REGISTER();
				$rawCon = $conObject->getRawObject($parent_id);
				$manager_fld = $conObject->getManagerFieldName();
				$manager = $rawCon[$manager_fld];
				$ownerObj = new RGSTR2_CONTRACT_OWNER();
				$owner_fld = $conObject->getOwnerFieldName();
				$owner_id = $rawCon[$owner_fld];
				$owners = $ownerObj->getActiveAdminsForOwner($owner_id);
				if($mode){
					$title3 = ucwords($this->getObjectName("register"))." Owner";
					//$title3 = $headObj->getAHeadingNameByField("con_owner");
					foreach($owners as $key=>$val){
						$tkid[$title3][]=$key;
					}
				}else{
					foreach($owners as $key=>$val){
						$tkid[]=$key;
					}
				}
				$c_auth = $rawCon['contract_authoriser'];
				if($mode){
					$title = ucwords($this->getObjectName("register"))." Manager";
					$title2 = ucwords($this->getObjectName("register"))." Authoriser";
					$tkid[$title] = $manager;
					$tkid[$title2] = $c_auth;
				}else{
					$tkid[]=$manager;
					$tkid[]=$c_auth;
				}
				break;
		}	
		return $tkid;
	}
	 
	 
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>