<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class RGSTR2_LINES extends RGSTR2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "LIN";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "LINE";
    const OBJECT_NAME = "line";
	const PARENT_OBJECT_TYPE = null;
     
    const TABLE = "hazard_library_link_lines";
    const TABLE_FLD = "lne";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		//$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addLinkObjects($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
//		unset($var['object_id']); //remove incorrect field value from add form

		$var[$this->getTableField().'_status'] = RGSTR2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

        //Set my own insert vars
        $insert_vars = array();
        $insert_vars[$this->getTableField().'_objectid'] = $var['object_id'];//Hazard ID
        $insert_vars[$this->getTableField().'_object_type'] = $var['object_type'];//hzd || rsk
        $insert_vars[$this->getTableField().'_srcmodref'] = $var['srcmodref'];

        $line_item_array = $var['chk_use'];

        $line_item_ids = array();

        foreach($line_item_array as $key => $val){
            if($val == 'Y'){
                $line_item_ids[$key] = $key;
            }
        }


        $insert_vars[$this->getTableField().'_status'] = $var[$this->getTableField().'_status'];
        $insert_vars[$this->getTableField().'_insertdate'] = $var[$this->getTableField().'_insertdate'];
        $insert_vars[$this->getTableField().'_insertuser'] = $var[$this->getTableField().'_insertuser'];

        $sql = 'INSERT INTO ' . $this->getTableName() . ' ';
        $sql .= '(' . $this->getTableField() . '_objectid';
        $sql .= ', ' . $this->getTableField() . '_object_type';
        $sql .= ', ' . $this->getTableField() . '_srcmodref';
        $sql .= ', ' . $this->getTableField() . '_srcid';
        $sql .= ', ' . $this->getTableField() . '_status';
        $sql .= ', ' . $this->getTableField() . '_insertuser';
        $sql .= ', ' . $this->getTableField() . '_insertdate) ';

        $sql .= 'VALUES';

        $iteration_count = 1;
        foreach($line_item_ids as $key => $val){
            $sql .= '(' . $insert_vars[$this->getTableField().'_objectid'];
            $sql .= ', \'' . $insert_vars[$this->getTableField().'_object_type'] . '\'';
            $sql .= ', \'' . $insert_vars[$this->getTableField().'_srcmodref'] . '\'';
            $sql .= ', ' . $val;
            $sql .= ', ' . $insert_vars[$this->getTableField().'_status'];
            $sql .= ', \'' . $insert_vars[$this->getTableField().'_insertuser'] . '\'';
            $sql .= ', \'' . $insert_vars[$this->getTableField().'_insertdate'] . '\')';

            $sql .= ($iteration_count == count($line_item_ids) ? ';' : ',' );

            $iteration_count++;
        }

		$query_executed = $this->db_query($sql);

        $var['the_sql'] = $sql;
        $var['query_executed'] = $query_executed;

		if($query_executed) {
			
			$changes = array(
                'response'=>"Line Item" . (count($line_item_ids) > 1 ? 's' : '' ) . " created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $insert_vars[$this->getTableField().'_objectid'],
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> RGSTR2_LOG::CREATE,
                'attachment'	=> '',
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);

            $dialog_message = 'Selected line item' . (count($line_item_ids) > 1 ? 's ' : ' ' );
            $dialog_message .= 'from ' . $insert_vars[$this->getTableField().'_srcmodref'] . ' library successfully linked to '. ($var['object_type'] == 'hzd' ? 'HAZ' : 'RR' ) . $insert_vars[$this->getTableField().'_objectid'];

			$result = array(
				0=>"ok",
				1=>$dialog_message,
				'object_id'=>$insert_vars[$this->getTableField().'_objectid'],
				'log_var'=>$log_var,

				'the_sql'=>$sql,
				'query_executed'=>$query_executed,

			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}

    public function addLinkObjectstoRisk($var) {

        $rhl_prefix = 'rhl';
        $rhl_table_ref = $_SESSION['dbref']."_risk_hazard_library_link_lines ";

        $var[$rhl_prefix.'_status'] = RGSTR2::ACTIVE;
        $var[$rhl_prefix.'_insertdate'] = date("Y-m-d H:i:s");
        $var[$rhl_prefix.'_insertuser'] = $this->getUserID();

        //Set my own insert vars
        $insert_vars = array();
        $insert_vars[$rhl_prefix.'_risk_id'] = $var['object_id'];//
        $insert_vars[$rhl_prefix.'_lne_id'] = '';//Foreach through this

        $line_item_array = $var['chk_use'];

        $line_item_ids = array();
        foreach($line_item_array as $key => $val){
            if($val == 'Y'){
                $line_item_ids[$key] = $key;
            }
        }

        $linkObject = new ASSIST_INTERNAL();
        $linkObject->setSourceType("library");
        $sources = $linkObject->getSources();

        $hazard_id = $var['hazard_value'];
        $hazard_type = 'hzd';
        $src = $var['srcmodref'];

        $sql = "SELECT lne_id as id, lne_srcmodref as modref, lne_srcid as line_id ";
        $sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
        $sql .= "WHERE lne_objectid = $hazard_id ";
        $sql .= "AND lne_object_type = '$hazard_type' ";
        $sql .= "AND lne_srcmodref = '$src' ";
        $sql .= "AND lne_srcid IN (" . implode(',', $line_item_ids) . ") ";
        $sql .= "AND (lne_status & ". 2 .") = " . 2;

        $hazard_linked_lines = $linkObject->mysql_fetch_value_by_id($sql,"id","line_id");
        $debug_return_val = array(
            0=>"ok",
            1=>"DEBUG VALUES",
            'object_id'=>$hazard_id,
            'log_var'=>array('plus', 'stuff'),
            'the_sql'=>$sql,
            'sql_result'=>$hazard_linked_lines,
        );

//        return $debug_return_val;


        $insert_vars[$rhl_prefix.'_status'] = $var[$rhl_prefix.'_status'];
        $insert_vars[$rhl_prefix.'_insertdate'] = $var[$rhl_prefix.'_insertdate'];
        $insert_vars[$rhl_prefix.'_insertuser'] = $var[$rhl_prefix.'_insertuser'];

        $sql = 'INSERT INTO ' . $rhl_table_ref . ' ';
        $sql .= '(' . $rhl_prefix . '_risk_id';
        $sql .= ', ' . $rhl_prefix . '_lne_id';
        $sql .= ', ' . $rhl_prefix . '_status';
        $sql .= ', ' . $rhl_prefix . '_insertuser';
        $sql .= ', ' . $rhl_prefix . '_insertdate) ';

        $sql .= 'VALUES';

        $iteration_count = 1;
        foreach($hazard_linked_lines as $key => $val){
            $sql .= '(' . $insert_vars[$rhl_prefix.'_risk_id'];
            $sql .= ', ' . $key;
            $sql .= ', ' . $insert_vars[$rhl_prefix.'_status'];
            $sql .= ', \'' . $insert_vars[$rhl_prefix.'_insertuser'] . '\'';
            $sql .= ', \'' . $insert_vars[$rhl_prefix.'_insertdate'] . '\')';

            $sql .= ($iteration_count == count($hazard_linked_lines) ? ';' : ',' );

            $iteration_count++;
        }

        $query_executed = $this->db_query($sql);

        $var['the_sql'] = $sql;
        $var['query_executed'] = $query_executed;

        if($query_executed) {

            $changes = array(
                'response'=>"Line Item" . (count($line_item_ids) > 1 ? 's' : '' ) . " created.",
                'user'=>$this->getUserName(),
            );
            $log_var = array(
                'object_id'		=> $insert_vars[$rhl_prefix.'_risk_id'],
                'object_type'	=> $this->getMyObjectType(),
                'changes'		=> $changes,
                'log_type'		=> RGSTR2_LOG::CREATE,
                'attachment'	=> '',
            );
            $this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);

            $dialog_message = 'Selected line item' . (count($line_item_ids) > 1 ? 's ' : ' ' );
            $dialog_message .= 'from ' . $src . ' library successfully linked to '. ($var['object_type'] == 'hzd' ? 'HAZ' : 'RR' ) . $insert_vars[$rhl_prefix.'_risk_id'];

            $result = array(
                0=>"ok",
                1=>$dialog_message,
                'object_id'=>$insert_vars[$rhl_prefix.'_risk_id'],
                'log_var'=>$log_var,

                'the_sql'=>$sql,
                'query_executed'=>$query_executed,

            );
            return $result;
        }
        return array("error","Testing: ".$sql);
    }


	public function deleteLinkObject($var) {

		$id = $var['line_id'];

		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = ".self::DELETED." 
				WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> RGSTR2_LOG::DELETE,
                'attachment'	=> '',
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
		return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." deleted successfully.");
	}

    public function deleteLinkObjectFromRisk($var) {

        $id = $var['line_id'];

        $rhl_prefix = 'rhl';
        $rhl_table_ref = $_SESSION['dbref']."_risk_hazard_library_link_lines ";

        $sql = "UPDATE ".$rhl_table_ref." 
				SET ".$rhl_prefix."_status = ".self::DELETED." 
				WHERE ".$rhl_prefix."_lne_id = ".$id;
        $this->db_update($sql);
        $changes = array(
            'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deleted.",
            'user'=>$this->getUserName(),
        );
        $log_var = array(
            'object_id'		=> $id,
            'object_type'	=> $this->getMyObjectType(),
            'changes'		=> $changes,
            'log_type'		=> RGSTR2_LOG::DELETE,
            'attachment'	=> '',
        );
        $this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
        return array("ok",$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." deleted successfully.");
    }

    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }

	public function getList($section,$options) {
		return $this->getMyHazardList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}

    public function getSourceSelectItems() {
        $linkObject = new ASSIST_INTERNAL();
        $linkObject->setSourceType("library");
        $sources = $linkObject->getSources();

        $html = '';

        $html .= '<option value="X">--- SELECT ---</option>';
        foreach($sources as $modref => $src) {
            $html .= '<option value=' . $modref . '>'.$src.'</option>';
        }
        return $html;
    }

    public function getLinkedSourceSelectItems($var) {
        $hazard_id = $var['hazard_id'];

        $object_type = 'hzd';

        $linkObject = new ASSIST_INTERNAL();
        $linkObject->setSourceType("library");
        $sources = $linkObject->getSources();

        $sql = "SELECT lne_id as id, lne_srcmodref as modref, lne_srcid as line_id ";
        $sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
        $sql .= "WHERE lne_objectid = $hazard_id ";
        $sql .= "AND lne_object_type = '$object_type' ";
        $sql .="AND (lne_status & ". 2 .") = " . 2;

        $existing_lines = $linkObject->mysql_fetch_value_by_id2($sql, "modref","id","line_id");

        $html = '';
        if(isset($existing_lines) && is_array($existing_lines) && count($existing_lines) > 0){
            $html .= '<option value="X">--- SELECT ---</option>';
            foreach($sources as $modref => $src) {
                if(array_key_exists($modref, $existing_lines)){
                    $html .= '<option value=' . $modref . '>'.$src.'</option>';
                }

            }
        }else{
            $html .= '<option value="X">--- No Line Items Linked to this Hazard ---</option>';
        }

        return $html;
    }

    public function getLinkedFilterSelectItems($var) {
        $hazard_id = $var['hazard_id'];
        $src = $var['src'];

        $object_type = 'hzd';

        $linkObject = new ASSIST_INTERNAL();
        $linkObject->setSourceType("library");
        $sources = $linkObject->getSources();

        $sql = "SELECT lne_id as id, lne_srcmodref as modref, lne_srcid as line_id ";
        $sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
        $sql .= "WHERE lne_objectid = $hazard_id ";
        $sql .= "AND lne_object_type = '$object_type' ";
        $sql .= "AND lne_srcmodref = '$src'";
        $sql .= "AND (lne_status & ". 2 .") = " . 2;

        $existing_lines = $linkObject->mysql_fetch_value_by_id2($sql, "modref","id","line_id");

        $legacy_data_structure = $linkObject->getLegacyDataStructure($src);

        $filters_data_structure = array();
        if(isset($existing_lines) && is_array($existing_lines) && count($existing_lines) > 0){
            foreach($sources as $modref => $src) {
                if(array_key_exists($modref, $existing_lines)){
                    foreach($existing_lines[$modref] as $index => $value_thereof){
                        //Search Algorithm for finding the line items connected to the hazard by going through the legacy data structure
                        //Start with the Parent
                        foreach($legacy_data_structure as $parent_id  => $parent_data){
                            //Then go through the kids
                            foreach($parent_data['children_objects'] as $child_id => $child_data){
                                //Check if the hazard connected line item_id is in this child item
                                if(array_key_exists($value_thereof, $child_data['child_activities'])){
                                    //Set up the filter data structure, that we'll use to generate the option box
                                    if(!array_key_exists($child_id, $filters_data_structure)){
                                        $filters_data_structure[$child_id]['filter_parent'] = $parent_data['parent_object'];
                                        $filters_data_structure[$child_id]['filter_object'] = $child_data['child_object'];
                                    }
                                    $filters_data_structure[$child_id]['filter_activities'][$value_thereof] = $child_data['child_activities'][$value_thereof];
                                }
                            }
                        }
                    }
                }
            }
        }

        $filters = array();

        if(isset($filters_data_structure) && is_array($filters_data_structure) && count($filters_data_structure) > 0){
            foreach($filters_data_structure as $filter_id => $filter_data){
                $filter_string = $filter_data['filter_parent']['name'] . ': ';
                $filter_string .= $filter_data['filter_object']['name'] . ' ';
                $activity_count = count($filter_data['filter_activities']);
                $filter_string .= '(' . $activity_count . ' record' . ($activity_count > 1 ? 's' : '' ) . ')';

                $filters[$filter_id] = $filter_string;
            }
        }

        $html = '';
        if(isset($filters) && is_array($filters) && count($filters) > 0){
            $html .= '<option value="X">--- SELECT ---</option>';
            foreach($filters as $filter_id => $filter_string) {
                $html .= '<option value=' . $filter_id . '>'.$filter_string.'</option>';

            }
        }else{
            $html .= '<option value="X">--- No Line Items Linked to this Hazard ---</option>';
        }

        return $html;
    }
}