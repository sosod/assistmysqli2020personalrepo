<?php
require_once("inc_header.php");
$page_redirect_path = "admin_template_manage.php?";
$noparams = explode("&",$_SERVER['REQUEST_URI']);
$pageonly = explode("/",$noparams[0]);
$thispage = $pageonly[2];

$templateObject = new RGSTR2_TEMPLATE();
$objects = $templateObject->getActiveTemplates();
$inactive_objects = $templateObject->getInActiveTemplates();
foreach($objects as $ind=>$thing){

	foreach($thing as $key=>$val){

	}	

}

//$js.=$displayObject->drawListTable($objects,array('value'=>"View",'class'=>"btn_view"));
ASSIST_HELPER::arrPrint($objects);
?>
<p>List all active TEMPLATES</p>
<p>allow option to edit/delete template (contract), deliverable (+add) & action (+add)</p>
<div class="div_error">
	
</div>
<table id="template_manage" style="width:800px;">
	<tr>
		<td colspan=42>Active Templates</td>
	</tr>
	<tr>
		<th>Template ID</th>
		<th>Template Name</th>
		<th>Source Contract</th>
		<th>Status</th>
		<th>Creator</th>
		<th>Creation date</th>
		<th></th>
	</tr>
		<?php
			foreach($objects as $ind=>$thing){	
				echo "<tr>";
				foreach($thing as $key=>$val){
					echo"<td id='".$key."'>";	
					if($key == "temp_status"){
						if($val == 2){
							echo "Active";
							echo"</td>";
						}
					}else if($key == "temp_insertuser"){
						echo $templateObject->getAUserName($val);
						echo"</td>";
					}else if($key == "temp_insertdate"){
						echo $val;
						echo"</td>";
						echo "<td><input class='btn_view' type='button' id='".$thing['temp_id']."' value='Edit' /></td>";
					}else if($key == "temp_source_contract_id"){
						echo"<a href='manage_view_object.php?object_type=CONTRACT&object_id=".$val."'>".$val."</a>";
						echo"</td>";
					}else{
						echo $val;
						echo"</td>";
					}
				}	
				echo "</tr>";
			}		
		?>
	</tr>
	</table>
<br>
	<table style="width:800px;">
	<tr>
		<td colspan=42>Inactive Templates</td>
	</tr>
	<tr>
		<th>Template ID</th>
		<th>Template Name</th>
		<th>Source Contract</th>
		<th>Status</th>
		<th>Creator</th>
		<th>Creation date</th>
		<th></th>
	</tr>
	<?php
			foreach($inactive_objects as $ind=>$thing){	
				echo "<tr>";
				foreach($thing as $key=>$val){
					echo"<td id='".$key."'>";	
					if($key == "temp_status"){
						if($val == 2){
							echo "Active";
							echo"</td>";
						}
					}else if($key == "temp_insertuser"){
						echo $templateObject->getAUserName($val);
						echo"</td>";
					}else if($key == "temp_insertdate"){
						echo $val;
						echo"</td>";
						echo "<td><input class='btn_act' type='button' id='D".$thing['temp_id']."' value='Restore' /></td>";
					}else{
						echo $val;
						echo"</td>";
					}
				}	
				echo "</tr>";
			}		
		?>
</table>
<div id="deactivate_div" style="display:none">
	<h3>Deactivate <span id="span_name">name</span></h3>
	<p>Are you sure you want to deactivate Template <span id="span_id">X</span>?</p>
</div>
<div id="activate_div" style="display:none">
	<h3>Restore <span id="span_name2">name</span></h3>
	<p>Are you sure you want to restore Template <span id="span_id2">X</span>?</p>
</div>

<script>
	<?php echo $js; ?>
	$(".btn_view").click(function(){
		var id = $(this).prop("id");
		document.location.href = "admin_template_edit.php?object_type=CONTRACT&object_id="+id;
	});
	
	$(".btn_act").click(function(){
		var id = $(this).parents("tr").children("#temp_id").prop("textContent");
		var name = $(this).parents("tr").children("#temp_name").prop("textContent");
		$("#span_name2").html(name);
		$("#span_id2").html(id);
		$("#activate_div").show();
		$("#activate_div").dialog({
			modal:true,
			buttons:[{
				text:"Restore",
				click:function(){
					AssistHelper.processing();
					var result = AssistHelper.doAjax("inc_controller.php?action=Template.restore", "id="+id);
					console.log(result);
					$(this).dialog("close");
					AssistHelper.finishedProcessing(result[0],result[1]);
					setTimeout(function(){
						document.location.href = "<?php echo $thispage; ?>";
					},2000);
				}
			},{
				text:"Cancel",
				click:function(){
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "activate_div");
	});
</script>





















