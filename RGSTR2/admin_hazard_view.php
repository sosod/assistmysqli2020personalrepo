<?php require_once("inc_header.php"); ?>
<?php require_once("../module/autoloader.php"); ?>

<?php /*============================================================*/ ?>
<?php ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); ?>
<?php
$dialog_url = "admin_hazard_edit_dialog.php";

$_REQUEST['object_type'] = "HAZARD";
$page_action = "EDIT";
$page_section = "NEW";
$child_object_type = "HAZARD";
$button_label = "|open|";
$button_icon = "pencil";



$add_button = true;
$add_button_label = "|add| |hazard|";
$add_button_function = "showAddDialog();";

$page_direct = "admin_hazard.php";
$alternative_button_click_function = "editObject";

$object_type = $_REQUEST['object_type'];

?>

<?php
$object_type = 'HAZARD';
$object_id = $_GET['object_id'];

$myObject = new RGSTR2_HAZARD();
$get_current_hazard_record = $myObject->getAObject($object_id);

//echo '<pre style="font-size: 18px">';
//echo '<p>HAZARD</p>';
//print_r($get_current_hazard_record);
//echo '</pre>';
?>

<!-- Details Table  - START -->
<h2><?php echo $helper->getObjectName($object_type); ?> Details</h2>
<?php $js .= $displayObject->drawDetailedHazardView($object_id); ?>
<!-- Details Table  - END -->


<!-- Details Table Logic - START -->
<?php $page_direct = 'admin_hazard_view.php?object_id='; ?>
<?php $js .= " var page_direct = '".$page_direct."';"; ?>
<?php

    $js .= "
		$('button.dummyLink').button({icons:{primary:'ui-icon-pencil'}}).click(function() {
		tableButtonClick($(this));
		});
		";

?>
<script type=text/javascript>
    var url = "<?php echo $page_direct; ?>";
    var section = "<?php echo $object_type; ?>";
    $(function() {
        <?php echo $js; ?>
    });
    function tableButtonClick($me) {
        <?php
        if(isset($alternative_button_click_function) && $alternative_button_click_function!==false && strlen($alternative_button_click_function)>0) {
            echo $alternative_button_click_function."($"."me);";
        } else {
            echo "
			AssistHelper.processing();
				var r = $"."me.attr(\"ref\");
				document.location.href = url+r;
				";
        }
        ?>

    }
</script>
<!-- Details Table Logic - END -->

<?php /*============================================================*/ ?>
<?php /*=============== IFRAME DIALOG BOX STUFF - START ============*/ ?>
<?php /*============================================================*/ ?>


<div id=dlg_add_hazard title='<?php echo $helper->replaceAllNames("|add| |competency|"); ?>'>
    <?php

    $section = "NEW";
    $page_redirect_path = "admin_hazard.php?";
    $page_action = "Add";

    $uaObject = new RGSTR2_USERACCESS();

    $child_object_type = "HAZARD";
    $child_object_id = 0;
    $childObject = new RGSTR2_HAZARD();

    $data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
    echo $data['display'];

    ?>
</div>

<div id=dlg_child class=dlg-child title="">
    <iframe id=ifr_form_display style="border:0px solid #000000;" src="">

    </iframe>
</div>



<script type="text/javascript" >
    $(function() {
        $("#dlg_add_hazard").dialog({
            autoOpen: false,
            modal: true
        });
        <?php echo $data['js']; ?>



    });

    function showAddDialog() {
        $(function() {
            $("#dlg_add_hazard").dialog("open");
            var my_window = AssistHelper.getWindowSize();
            if(my_window['width']>800) {
                $("#dlg_add_hazard form[name=frm_object] table.form:first").css("width","800px");
                $("#dlg_add_hazard").dialog("option","width",850);
            } else {
                $("#dlg_add_hazard form[name=frm_object] table.form:first").css("width",(my_window['width']-100)+"px");
                $("#dlg_add_hazard").dialog("option","width",(my_window['width']-50));
            }
            $("#dlg_add_hazard").dialog("option","height",my_window['height']-50);
        });
    }
    <?php

    $t1_object_name = $childObject->getMyObjectName();
    $t1_object_type = $childObject->getMyObjectType();
    echo "
	var object_names = new Array();
	object_names['tier1'] = '".$helper->getObjectName($t1_object_name)."';
	object_names['".$t1_object_type."'] = '".$helper->getObjectName($t1_object_name)."';
	";
    ?>
    var my_window = AssistHelper.getWindowSize();
    //alert(my_window['height']);
    if(my_window['width']>800) {
        var my_width = 850;
    } else {
        var my_width = 800;
    }
    var my_height = my_window['height']-50;

    $("div.dlg-child").dialog({
        autoOpen: false,
        modal: true,
        width: my_width,
        height: my_height,
        beforeClose: function() {
            AssistHelper.closeProcessing();
        },
        open: function() {
            $(this).dialog('option','width',my_width);
            $(this).dialog('option','height',my_height);
        }
    });
    function editObject($me) {
        //alert("edit :"+$me.attr('ref')+":");
        $(function() {
            AssistHelper.processing();
            var i = $me.attr('ref');
            var t = "HAZARD";
            $dlg = $("div#dlg_child");
            act = "edit";
            var obj = t.toLowerCase();
            var heading = AssistString.ucwords(act)+" "+object_names[t];
            var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&object_type="+t+"&object_id="+i;

            $dlg.dialog("option","title",heading);
            //$dlg.find("iframe").prop("src",url);
            $dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
            //FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
            $dlg.dialog("open");
        });
    }

    function dialogFinished(icon,result) {
        if(icon=="ok") {
            document.location.href = '<?php echo $page_redirect_path; ?>r[]=ok&r[]='+result;
        } else {
            AssistHelper.processing();
            AssistHelper.finishedProcessing(icon,result);
        }
    }
</script>

<?php /*============================================================*/ ?>
<?php /*=============== IFRAME DIALOG BOX STUFF - END ==============*/ ?>
<?php /*============================================================*/ ?>


<?php /*============================================================*/ ?>
<?php /*=============== LINE ITEM SECTION CODE - START =============*/ ?>
<?php /*============================================================*/ ?>


<?php
$active_record = 2;	//replace with something like MODLOC_OBJECT::ACTIVE
$object_name = "OBJECT";

$object_type = 'hzd';

/**
 * Actual code starts here
 *
 * 1. Create ASSIST_INTERNAL object
 * 2. Tell the ASSIST_INTERNAL object that you are working with LIBRARY modules
 * 3. Get ASSIST_INTERNAL to find all LIBRARY modules available on the client database - used to populate the SOURCE select.
 *
 */
$linkObject = new ASSIST_INTERNAL();
$linkObject->setSourceType("library");
$sources = $linkObject->getSources();
$existing_lines = array();
$existing_lines_details = array();

//echo '<pre style="font-size: 18px">';
//echo '<p>SOURCES</p>';
//print_r($sources);
//echo '</pre>';



//GET EXISTING LINE INFORMATION
//Could be a call to a function in MODREF_OBJECT class
$sql = "SELECT lne_id as id, lne_srcmodref as modref, lne_srcid as line_id ";
$sql .= "FROM ".$_SESSION['dbref']."_hazard_library_link_lines ";
$sql .= "WHERE lne_objectid = $object_id ";
$sql .= "AND lne_object_type = '$object_type' ";
$sql .= "AND (lne_status & ".$active_record.") = ".$active_record;
$existing_lines = $linkObject->mysql_fetch_value_by_id2($sql, "modref","id","line_id");

//echo '<pre style="font-size: 18px">';
//echo '<p>PRINTHERE</p>';
//print_r($existing_lines);
//echo '</pre>';

$sources_session_array = $_SESSION[$_SESSION['modref']]['sources'];
?>




<?php
/**
 * Source & Filter select elements - needed to populate the dlg_lines & ifr_lines elements so that the user can chose which lines from the SOURCE module must be linked to the parent OBJECT
 */
?>
<h2>Select Line Items</h2>
<Table>
    <tr>
        <td class=b>Source:</td>
        <td><select id=sel_source><option value=X >--- SELECT ---</option>
                <?php
                foreach($sources as $modref => $src) {
                    echo "<option value=$modref >".$src."</option>";
                    if(isset($existing_lines[$modref])) {
                        $existing_lines_details[$modref] = $linkObject->getLinesByID($modref,$existing_lines[$modref]);
                    } else {
                        $existing_lines_details[$modref] = array();
                    }
                }
                ?>
            </select></td>
    </tr>
    <tr>
        <td class=b>Filter:</td>
        <td><select id=sel_filter><option value=X>--- SELECT ---</option>
            </select></td>
    </tr>
    <tr>
        <td class=b></td>
        <td class=right><button id=btn_filter>Go</button></td>
    </tr>
</Table>
<?php
/**
 * DIV & IFRAME elements to handle dialog needed to display available lines
 */
?>
<div id=dlg_lines title="Select lines">
    <iframe id=ifr_lines></iframe>
</div>
<?php
/**
 * Existing LINES ITEMS already linked to the PARENT OBJECT
 */
?>
<h2>Line Items</h2>
<?php foreach($sources as $modref => $src) { ?>
    <?php if(isset($existing_lines[$modref]) && count($existing_lines[$modref])>0 && isset($existing_lines_details[$modref]['rows']) && count($existing_lines_details[$modref]['rows'])>0){ ?>
        <h3><?php echo $src; ?></h3>
        <table width=100%>
            <tr>
                <th>Ref</th>
                <?php foreach($existing_lines_details[$modref]['head'] as $fld => $head) { ?>
                    <th><?php echo $head; ?></th>
                <?php } ?>
                <th width=50px></th>
            </tr>

            <?php foreach($existing_lines[$modref] as $record_id => $src_line_id) { ?>
                <?php
                $row = $existing_lines_details[$modref]['rows'][$src_line_id];
                $ref = $modref."/".$src_line_id;

                //Preparation for the pop up dialogue
                $mod_location = $sources_session_array[$modref]['modlocation'];
                $lib_object_type = 'ACTIVITY';
                $lib_object_id = $src_line_id;
                ?>
                <tr>
                    <td><?php echo $ref; ?></td>
                    <?php foreach($existing_lines_details[$modref]['head'] as $fld => $head) { ?>
                        <td><?php echo $row[$fld]; ?> <?php echo ($fld == 'activity_name' ? '<a href="#" class="action_dlg" data-mod_location="' . $mod_location . '" data-mod_ref="' . $modref . '" data-lib_object_id="' . $lib_object_id . '" data-lib_object_type="' . $lib_object_type . '" data-request_scheme="' . $_SERVER['REQUEST_SCHEME'] . '" data-server_name="' . $_SERVER['SERVER_NAME'] . '">View</a>' : ''); ?></td>
                    <?php } ?>
                    <td class=center><button class=btn_delete record="<?php echo $record_id; ?>" ref="<?php echo $ref; ?>" data-object_id="<?php echo $object_id; ?>">Delete</button></td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
<?php } ?>

<script type="text/javascript">
    $(function() {
        //Determine the necessary width and height settings for the dlg_lines & ifr_lines elements
        var scr = AssistHelper.getWindowSize();
        var dlg_width = scr['width']*0.95;
        var dlg_height = scr['height']*0.95;
        var ifr_width = dlg_width-20;
        var ifr_height = dlg_height-100;


        //Determine the necessary width and height settings for the dlg_edit element
        var tbl_edit = parseInt(AssistString.substr($("#dlg_edit").find("table:first").css("width"),0,-2));
        var edit_width = (tbl_edit+50)+"px";


        /**************************
         * MAIN OBJECT SECTION
         */


        /**
         * Edit dialog - to edit the main object
         */
        $("#dlg_edit").dialog({
            autoOpen: false,
            modal: true,
            width: edit_width,
            buttons: [{
                text:"Save",
                icons: {primary: "ui-icon-disk"},
                click:function(e) {
                    e.preventDefault();
                    AssistHelper.processing();
                    /**
                     * PROCESS EDIT FORM HERE (check for required fields etc)
                     */
                    var dta = AssistForm.serialize($("form[name=frm_edit]"));
                    //var result = AssistHelper.doAjax("inc_controller.php?action=OBJECT.EDIT",dta);	//live result process
                    var result = ["ok","display the result of the form processing here and then reload the page to reflect the changes"]; //result message for demo purposes
                    //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                    if(result[0]=="ok") {
                        var url = document.location.href;
                        AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                    } else {
                        //if result was not "ok" then display the message - don't close the dialog or reload the page so that the user can decide what to do after seeing the message
                        AssistHelper.finishedProcessing(result[0],result[1]);
                    }
                }
            },{
                text: "Cancel",
                click:function(e){
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_edit").dialog("close");
                }
            },{
                text: "Delete",
                icons: {primary:"ui-icon-trash"},
                click:function(e){
                    e.preventDefault();
                    if(confirm("Are you sure you wish to delete this <?php echo $object_name; ?>?")==true) {
                        AssistHelper.processing();
                        var object_id = <?php echo $object_id; ?>
                        //var result = AssistHelper.doAjax("inc_controller.php?action=OBJECT.DELETE","object_id="+object_id);	//live result process
                        //The delete function should: UPDATE object_table SET object_status = 0 WHERE object_id = $obj
                        var result = ["ok","display the result here from deleting the object and then redirect back to the list page"];	//result message for demo purposes
                        //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                        if(result[0]=="ok") {
                            //live process for ok result
                            //var url = "go back to list page";
                            //AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                            //process for demo purposes
                            var url = document.location.href;
                            AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                        } else {
                            //if result was not "ok" then display the message
                            AssistHelper.finishedProcessing(result[0],result[1]);
                        }
                    } else {
                        //do nothing
                    }
                }
            }]
        });
        AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),0,"ui-state-green");
        AssistHelper.formatDialogButtonsByClass($("#dlg_edit"),2,"ui-state-error");
        /**
         * "Edit" button - display the dlg_edit dialog pop-up
         */
        $("#btn_edit")
            .button({icons:{primary:"ui-icon-pencil"}})
            .click(function(e) {
                e.preventDefault();
                $("#dlg_edit").dialog("open");
            }).removeClass("ui-state-default").addClass("ui-state-black");





        /*****************************
         * SELECT LINES section
         */

        /**
         * Source select element - on change, get and display the associated filters
         */
        $("#sel_source").change(function() {
            $("#sel_filter option").remove();
            if($(this).val()=="X") {
                $("#sel_filter").append($("<option />",{value:"X",text:"--- Please select a source above ---"}));
            } else {
                //get filters from assist_internal
                var dta = "src="+$(this).val();
                console.log('THIS IS THE DATA');
                console.log(dta);
                var filters = AssistHelper.doAjax("inc_controller.php?action=INTERNAL.getFilters",dta);
                if(Object.keys(filters).length>0) {
                    $("#sel_filter").append($("<option />",{value:"X",text:"--- SELECT ---"}));
                    //populate with remaining filters
                    Object.keys(filters).forEach(function(key) {
                        console.log(key + ': ' + filters[key]);
                        $("#sel_filter").append($("<option />",{value:key,text:filters[key]}));
                    });
                } else {
                    $("#sel_filter").append($("<option />",{value:"X",text:"--- No options available. Please select another source. ---"}));
                }
            }
        }).trigger("change");	//trigger change to set the filter select element to blank

        /**
         * "Go" button - process the selected Source and Filter and display the available lines in the dlg_lines dialog pop-up
         */
        $("#btn_filter").button()
            .click(function(e) {
                e.preventDefault();
                AssistHelper.processing();
                if($("#sel_source").val()=="X") {
                    AssistHelper.finishedProcessing("error","Please select a Source.");
                } else if($("#sel_filter").val()=="X") {
                    AssistHelper.finishedProcessing("error","Please select a Filter.");
                } else {
                    var src = $("#sel_source option:selected").text();
                    var filt = $("#sel_filter option:selected").text();
                    $("#dlg_lines").dialog("option","title","Select Lines from "+src+": "+filt);
                    var filter_id = $("#sel_filter").val();
                    var src = $("#sel_source").val();
                    var object_id = <?php echo $object_id; ?>;//haz_id
                    var object_type = 'hzd';//
                    var url = "admin_hazard_dialog_lines.php?src="+src+"&filter="+filter_id+"&object_id="+object_id+"&object_type="+object_type
                    //call iframe in dialog
                    $("#ifr_lines").prop("src",url);
                    //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
                    //$("#dlg_lines").dialog("open");
                }
            }).removeClass("ui-state-default").addClass("ui-state-black");

        /**
         * Dialog to display available lines after clicking the "Go" button
         */
        $("#dlg_lines").dialog({
            autoOpen: false,
            modal: true,
            width: dlg_width,
            height: dlg_height,
            buttons: [{
                text:"Save",
                icons: {primary: "ui-icon-disk"},
                click:function(e) {
                    e.preventDefault();
                    AssistHelper.processing();
                    var object_type = 'hzd';//
                    var dta = "object_id=<?php echo $object_id; ?>&srcmodref="+$("#sel_source").val()+"&object_type="+object_type;
                    var line_count = 0;
                    $("#ifr_lines").contents().find("input:checkbox.chk_line").each(function() {
                        if($(this).attr("line") && $(this).is(":checked")) {
                            dta+="&chk_use["+$(this).attr("line")+"]="+$(this).val();
                            line_count++;
                        }
                    });
                    if(line_count>0) {
                        //live process
                        console.log('THESE ARE THE LINES');
                        console.log(dta);

                        var result = AssistHelper.doAjax("inc_controller.php?action=LINES.ADDLINKS",dta);


                        console.log('THESE ARE THE RESULTS === FORMATTED FOR DEV RIGHT NOW');
                        console.log(result);
                    } else {
                        var result = ["error","You did not select any lines to save."];
                    }
                    //If the process was finished with an "ok" result, display and then reload the page to reflect the changes
                    if(result[0]=="ok") {
                        var url = document.location.href;
                        AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
                    } else {
                        //if result was not "ok" then display the message
                        AssistHelper.finishedProcessing(result[0],result[1]);
                    }
                }
            },{
                text: "Cancel",
                click:function(e){
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_lines").dialog("close");
                }
            }]
        });
        AssistHelper.formatDialogButtonsByClass($("#dlg_lines"),0,"ui-state-green");
        /**
         * Iframe within the dlg_lines div
         */
        $("#ifr_lines").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");




        /******************
         * EXISTING LINES section
         */



        /**
         * "Delete" button - remove link between module and src line
         */
        $(".btn_delete")
            .button({
                icons:{primary:"ui-icon-trash"},
                text: false
            }).click(function(e) {
            e.preventDefault();
            var ref = $(this).attr("ref");
            if(confirm("Are you sure you wish to unlink line "+ref+"? This will not delete the line, only remove the link to this <?php echo $object_name; ?>.")==true) {
                var record_id = $(this).attr("record");
                var result = AssistHelper.doAjax("inc_controller.php?action=LINES.DELETELINK","line_id="+record_id)

                console.log('THESE ARE THE RESULTS === FORMATTED FOR DEV RIGHT NOW');
                console.log(result);

                //The delete function should: UPDATE dbref_lines SET lne_status = 0 WHERE lne_id = ".$var[line_id]
                if(result[0]=="ok") {
                    var url = document.location.href;
                    AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);

                    //failSafe Hack
                    document.location.href = 'admin_hazard_view.php?' + 'r[]=' + result[0] + '&r[]=' + result[1] + '&object_id=' + $(this).data('object_id');
                } else {
                    //if result was not "ok" then display the message
                    AssistHelper.finishedProcessing(result[0],result[1]);
                }
            }
        }).removeClass("ui-state-default").addClass("ui-state-error");

    });


    /*****************************
     * ADDITIONAL FUNCTIONS CALLED BY IFRAME
     */


    /**
     * Function called by the ifr_lines Iframe to open the dlg_lines dialog once the iframe source page has loaded and is ready to be displayed to the user
     */
    function openDialog() {
        $(function() {
            $("#dlg_lines").dialog("open");
            AssistHelper.closeProcessing();
        });
    }

    /**
     * Function called by the ifr_lines Iframe to remove the save button and replace with a close button if all lines are already selected
     */
    function changeDialogButton() {
        $(function() {
            var butt = [{text:"Close",click:function(e) {e.preventDefault(); $("#dlg_lines").dialog("close");}}]
            $("#dlg_lines").dialog('option','buttons',butt);
        });
    }
</script>


<?php /*============================================================*/ ?>
<?php /*=============== LINE ITEM SECTION CODE - END ===============*/ ?>
<?php /*============================================================*/ ?>

<div id=dlg_action_details title="Select lines">
    <iframe id=ifr_action_details></iframe>
</div>
<script>
    $(function() {

        //Determine the necessary width and height settings for the dlg_action_details & ifr_lines elements
        var scr = AssistHelper.getWindowSize();
        var dlg_width = scr['width']*0.5;
        var dlg_height = scr['height']*0.95;
        var ifr_width = dlg_width-20;
        var ifr_height = dlg_height-100;

        /**
         * "Go" button - process the selected Source and Filter and display the available lines in the dlg_action_details dialog pop-up
         */
        $('a.action_dlg').button({
            icons:{primary:"ui-icon-arrow-4-diag"},
            text: false
        }).click(function(e) {
            e.preventDefault();
            AssistHelper.processing();
            $("#dlg_action_details").dialog("option","title","Action Details");

            var modlocation = $(this).data('mod_location');
            var modref = $(this).data('mod_ref');
            var object_type = $(this).data('lib_object_type');
            var object_id = $(this).data('lib_object_id');

            var request_scheme = $(this).data('request_scheme');
            var server_name = $(this).data('server_name');

            var url = request_scheme + "://" + server_name + "/" + modlocation + "/manage_view_dialog.php?display_type=external_dialog&display_parents=true&modref="+ modref.toLowerCase() +"&object_type="+ object_type + "&object_id=" + object_id;

            console.log('===== DIALOGUE URL =====');
            console.log(url);

            //call iframe in dialog
            $("#ifr_action_details").prop("src",url);
            //for test purposes - open the dialog to see any errors that prevent the iframe from calling the opendialog function
            $("#dlg_action_details").dialog("open");
        }).removeClass("ui-state-default").addClass("ui-state-black");;

        /**
         * Dialog to display available lines after clicking the "Go" button
         */
        $("#dlg_action_details").dialog({
            autoOpen: false,
            modal: true,
            width: dlg_width,
            height: dlg_height,
            buttons: [{
                text:"Close",
                icons: {primary: "ui-icon-close"},
                click:function(e) {
                    e.preventDefault();
                    //Close the dialog
                    $("#dlg_action_details").dialog("close");
                    AssistHelper.closeProcessing();
                }
            }]
        });
        AssistHelper.formatDialogButtonsByClass($("#dlg_action_details"),0,"ui-state-green");
        /**
         * Iframe within the dlg_action_details div
         */
        $("#ifr_action_details").prop("width",ifr_width+"px").prop("height",ifr_height+"px").css("border","1px solid #000000");


        $('#dlg_action_details').on('dialogclose', function(event) {
            AssistHelper.closeProcessing();
        });

        /******************
         * EXISTING LINES section
         */

    });
</script>

