<?php
$_REQUEST['object_type'] = "DELIVERABLE";

$page_redirect_path = "admin_edit_deliverable.php?";
$page_action = "EDIT";
$object_id = isset($_REQUEST['object_id']) ? $_REQUEST['object_id'] : 0;

$add_button = true;
$add_button_label = "|add| |action|";
//$add_button_function = "showAddDialog();";

include("common/edit_object.php");

switch($_REQUEST['object_type']) {
    case "DELIVERABLE":
    case "RISK":
        $child_type = "ACTION";
        $childObject_action = new RGSTR2_ACTION();
        $child_options = array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id);
        $child_objects = $childObject_action->getObject($child_options);
        $child_redirect = "manage_view_object.php?object_type=ACTION&object_id=";
        $child_name = $helper->getActionObjectName(true);
        break;
    case "ACTION":
        $child_redirect = "";
        break;
}
if($add_button) { $add_button_label = $helper->replaceAllNames($add_button_label); }
$button_label = $helper->getActivityName("open");
$button_icon = 'newwin';
$button = array('value'=>$helper->replaceAllNames($button_label),'class'=>"btn_view",'type'=>"button");
if(isset($button_icon)) {
    $button['icon'] = $button_icon;
}

$js.="
		$(\"#paging_obj_list_view .btn_view\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";
?>
<div id=dlg_child class=dlg-child title="">
    <iframe id=ifr_form_display style="border:0px solid #000000;" src="">

    </iframe>
</div>
<table class=tbl-container id="actions-list">

    <?php if(isset($childObject_action)) {?>
        <tr>
            <td colspan=3>
                <h2><?php echo $child_name; ?></h2>
                <?php $js.=$displayObject->drawListTable($child_objects,$button,$child_type,$child_options,false,array( (isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : ""))); ?>
            </td>
        </tr>
    <?php } ?>
</table>
<?php
//Get the register id for this particular risk
$parent_object_id = $parent_id;

//Auto-populate Acceptable Risk Exposure
$object_details = $childObject->getObjectDetails();
$rsk_acceptable_risk_exposure = $object_details['rsk_acceptable_risk_exposure'];
?>

<script>
	var url = "<?php echo $child_redirect; ?>";
    $(function() {
        <?php
        echo $js;
        ?>
	    var object_names = [];
	    object_names['contract'] = "<?php echo $helper->getObjectName("register"); ?>";
	    object_names['deliverable'] = "<?php echo $helper->getObjectName("risk"); ?>";
	    object_names['action'] = "<?php echo $helper->getObjectName("action"); ?>";

	    var my_window = AssistHelper.getWindowSize();
	    //alert(my_window['height']);
	    if(my_window['width']>800) {
		    var my_width = 850;
	    } else {
		    var my_width = 800;
	    }
	    var my_height = my_window['height']-50;

        $("#rsk_type").change(function() {
            console.log($(this).val());

            var value = $(this).val();

            if(value == "SUB"){
                //Show the risks that can have sub risks

                var register_id = <?php echo $parent_object_id; ?>

                var dta = "register_id=" + register_id;

                var main_risk_table_row = AssistHelper.doAjax("inc_controller.php?action=RISK.GETMAINRISKTABLEROW",dta);

                $("#tr_rsk_type").after(main_risk_table_row);
            }else{
                //Remove the main risk table row if it exists
                $("#tr_rsk_parent_id").remove();
            }

        });

        $("#rsk_type").trigger('change');

        $("#rsk_acceptable_risk_exposure").val(<?php  echo $rsk_acceptable_risk_exposure; ?>);
<?php
        /**
         * AA-613 RGSTR2 - Allow super user to add objects after activation
         * edited by : Sondelani Dumalisile
         * Comment  : Add action button repositioned from  manage > view to admin>edit>risk
         */
        ?>
	    $("#actions-list #btn_paging_add").click(function() {
		    AssistHelper.processing();
		    var i = 0;//$(this).parent("td").attr("object_id");
		    var t = "action";//$(this).parent("td").attr("object_type");
		    act = "add";
		    var dta = "object_type="+t+"&object_id="+i;
//		AssistHelper.hideProcessing();
			    $dlg = $("#dlg_child");
			    var obj = "deliverable";
			    if(t=="contract" || t=="action") { obj = t; }
			    //var obj = (t=="action" ? "action" : "deliverable");
			    var heading = AssistString.ucwords(act)+" "+object_names[obj];
			    var parent_id = "<?php echo $object_id; ?>";//$(this).attr("parent_id");
			    var url = "new_"+obj+"_"+act+"_object.php?display_type=dialog&object_id="+parent_id;
			    $dlg.dialog("option","title",heading);
			    //$dlg.find("iframe").prop("src",url);
			    $dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
			    //$dlg.dialog("open");
		    AssistHelper.hideProcessing();
	    });
	    $("div.dlg-child").dialog({
		    autoOpen: false,
		    modal: true,
		    width: my_width,
		    height: my_height,
		    position: ['center',20],
		    beforeClose: function() {
			    AssistHelper.closeProcessing();
		    },
		    open: function() {
			    $(this).dialog('option','width',my_width);
			    $(this).dialog('option','height',my_height);
		    }
	    });

    });
    function tableButtonClick($me) {
	    var i = $me.attr("ref");
	    url = url+i;
	    document.location.href = url;
    }
	function dialogFinished(icon,result) {
		if(icon=="ok") {
			document.location.href = '<?php echo $page_redirect_path; ?>r[]=ok&r[]='+result;
		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon,result);
		}
	}
</script>

<?php if((int)$object_details['rsk_status_id'] != 1){ ?>
    <script>
        $(function() {
            $("#rsk_current_controls").prop('disabled', true);
        });
    </script>
<?php } ?>
