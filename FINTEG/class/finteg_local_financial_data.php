<?php
class FINTEG_LOCAL_FINANCIAL_DATA extends FINTEG
{
    private $db_connection;

    public function __construct($company_code) {

        $this->db_connection = new ASSIST_MODULE_HELPER('client', $company_code);
    }

    public function getLatestIncomeFinancialData($financial_year_id){
        $financial_year_id = $financial_year_id;
        $finteg_transaction_type = 1;
        return $this->getAmountsForSDBIPReport($financial_year_id, $finteg_transaction_type);
    }

    public function getLatestExpenseFinancialData($financial_year_id){
        $financial_year_id = $financial_year_id;
        $finteg_transaction_type = 2;
        return $this->getAmountsForSDBIPReport($financial_year_id, $finteg_transaction_type);
    }

    public function getAmountsForSDBIPReport($financial_year_id, $finteg_transaction_type){
        $result_ex = array();
        $result_ex['#project_guid#'] = array();

        $result_ex['#project_guid#']['#incremental_id#'] = array();

        $result_ex['#project_guid#']['#incremental_id#']['function'] = '#mscoa_function_id_at_lowest_level#';
        $result_ex['#project_guid#']['#incremental_id#']['item'] = '#mscoa_item_id_at_item_level_given#';
        $result_ex['#project_guid#']['#incremental_id#']['region'] = '#mscoa_region_id_at_lowest_level#';
        $result_ex['#project_guid#']['#incremental_id#']['msc'] = '#mscoa_msc_id_at_lowest_level#';

        $result_ex['#project_guid#']['#incremental_id#']['results'] = array();
        $result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#'] = array();
        $result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#']['budget'] = '#sum_of_budgets#';
        $result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#']['adjustments'] = '#sum_of_adjustments#';
        $result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#']['actual'] = '#sum_of_actuals#';

        $sql = "SELECT * FROM assist_finteg_line_item ";
        $sql .= "WHERE assist_finteg_line_item.fli_fin_year_id = " . $financial_year_id . " ";
        $sql .= "AND assist_finteg_line_item.fli_ftt_id = " . $finteg_transaction_type . " ";
        $line_items = $this->db_connection->mysql_fetch_all($sql);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>LINE ITEMS</p>';
//        print_r($line_items);
//        echo '</pre>';

        $sql = "SELECT assist_finteg_line_item_amount.*, assist_finteg_record_type.* FROM assist_finteg_line_item_amount ";
        $sql .= "INNER JOIN  assist_finteg_line_item ";
        $sql .= "ON assist_finteg_line_item_amount.flia_fli_id = assist_finteg_line_item.fli_id ";
        $sql .= "INNER JOIN  assist_finteg_record_type ";
        $sql .= "ON assist_finteg_record_type.frt_id = assist_finteg_line_item_amount.flia_frt_id ";
        $sql .= "WHERE assist_finteg_line_item.fli_fin_year_id = " . $financial_year_id . " ";
        $sql .= "AND assist_finteg_line_item.fli_ftt_id = " . $finteg_transaction_type . " ";
        $line_item_amounts = $this->db_connection->mysql_fetch_all($sql);

//        echo '<pre style="font-size: 18px">';
//        echo '<p>LINE ITEM AMOUNTS</p>';
//        print_r($line_item_amounts);
//        echo '</pre>';

        $line_item_data_structure = array();
        $count = 0;
        foreach($line_items as $key => $val){
            $line_item_id = $val['fli_id'];
            $line_item_data_structure[$line_item_id]['line_item'] = $val;

            foreach($line_item_amounts as $key2 => $val2){
                if($val2['flia_fli_id'] == $line_item_id){
                    $line_item_data_structure[$line_item_id]['line_item_amounts'][$val2['flia_id']] = $val2;
                    unset($line_item_amounts[$key2]);
                }
            }
            unset($line_items[$key]);
            $count++;
        }

//        echo '<pre style="font-size: 18px">';
//        echo '<p>LINE ITEM DATA STRUCTURE</p>';
//        print_r($line_item_data_structure);
//        echo '</pre>';

        $result = array();
        $count = 0;
        foreach($line_item_data_structure as $key => $val){
            $line_item = $val['line_item'];
            $project_guid = $line_item['fli_project'];
            $result[$project_guid][$count]['fin_year_id'] = $financial_year_id;
            $result[$project_guid][$count]['function'] = $line_item['fli_function_id'];
            $result[$project_guid][$count]['funding'] = $line_item['fli_funding_id'];
            $result[$project_guid][$count]['region'] = $line_item['fli_region_id'];
            $result[$project_guid][$count]['msc'] = $line_item['fli_msc_id'];
            $result[$project_guid][$count]['item'] = $line_item['fli_item_id'];

            foreach($val['line_item_amounts'] as $key2 => $val2){
                $frt_index = $val2['frt_name'];
                $result[$project_guid][$count]['results'][$val2['flia_month_id']][$frt_index] = $val2['flia_value'];
            }

            $count++;
        }

        unset($line_item_data_structure[$key]);

        return $result;
    }
}