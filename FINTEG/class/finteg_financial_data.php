<?php

class FINTEG_FINANCIAL_DATA extends FINTEG {
	private $db_connection;
	const LINE_LIMIT = 100;

	public function __construct($company_code, $server = 'TEST') {
		//$company_code = 'ipmsdev';//Get rid of this when I copy the
		$this->db_connection = new ASSIST_DB_REMOTE('client', $company_code, $server);

	}

	public function getLatestIncomeFinancialData($financial_year_id, $start = 0, $limit = self::LINE_LIMIT) {
		$financial_year_id = $financial_year_id;
		$finteg_transaction_type = 1;
		return $this->getAmountsForSDBIPReport($financial_year_id, $finteg_transaction_type, $start, $limit);
	}

	public function getLatestExpenseFinancialData($financial_year_id, $start = 0, $limit = self::LINE_LIMIT) {
		$financial_year_id = $financial_year_id;
		$finteg_transaction_type = 2;
		return $this->getAmountsForSDBIPReport($financial_year_id, $finteg_transaction_type, $start, $limit);
	}

	public function getAmountsForSDBIPReport($financial_year_id, $finteg_transaction_type, $start = 0, $limit = self::LINE_LIMIT) {
		echo "<h4>Start @ _".$start."_ for _".$limit."_ rows</h4>"; //dev purposes

//		$result_ex = array();
//		$result_ex['#project_guid#'] = array();
//
//		$result_ex['#project_guid#']['#incremental_id#'] = array();
//
//		$result_ex['#project_guid#']['#incremental_id#']['function'] = '#mscoa_function_id_at_lowest_level#';
//		$result_ex['#project_guid#']['#incremental_id#']['item'] = '#mscoa_item_id_at_item_level_given#';
//		$result_ex['#project_guid#']['#incremental_id#']['region'] = '#mscoa_region_id_at_lowest_level#';
//		$result_ex['#project_guid#']['#incremental_id#']['msc'] = '#mscoa_msc_id_at_lowest_level#';
//
//		$result_ex['#project_guid#']['#incremental_id#']['results'] = array();
//		$result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#'] = array();
//		$result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#']['budget'] = '#sum_of_budgets#';
//		$result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#']['adjustments'] = '#sum_of_adjustments#';
//		$result_ex['#project_guid#']['#incremental_id#']['results'][' #time_id#']['actual'] = '#sum_of_actuals#';

//		$sql = "SELECT * FROM assist_finteg_line_item ";
//		$sql .= "WHERE assist_finteg_line_item.fli_fin_year_id = ".$financial_year_id." ";
//		$sql .= "AND assist_finteg_line_item.fli_ftt_id = ".$finteg_transaction_type." ";
//		$sql .= "LIMIT ".$start." , ".$limit." ";
//		$line_items = $this->db_connection->mysql_fetch_all_by_id($sql, "fli_id");


		//if(count($line_items) > 0) {

//
//echo "<h2>".$sql."</h2>";
//        echo '<pre style="font-size: 18px">';
//        echo '<p>LINE ITEMS</p>';
//        print_r($line_items);
//        echo '</pre>';

//			$sql = "SELECT assist_finteg_line_item_amount.*, assist_finteg_record_type.* FROM assist_finteg_line_item_amount ";
//			$sql .= "INNER JOIN  assist_finteg_line_item ";
//			$sql .= "ON assist_finteg_line_item_amount.flia_fli_id = assist_finteg_line_item.fli_id ";
//			$sql .= "INNER JOIN  assist_finteg_record_type ";
//			$sql .= "ON assist_finteg_record_type.frt_id = assist_finteg_line_item_amount.flia_frt_id ";
//			$sql .= "WHERE assist_finteg_line_item.fli_fin_year_id = ".$financial_year_id." ";
//			$sql .= "AND assist_finteg_line_item.fli_ftt_id = ".$finteg_transaction_type." ";
//			$sql .= "AND assist_finteg_line_item_amount.flia_fli_id IN (".implode(",",array_keys($line_items)).") ";

//temporarily only get project data
						//,  L.fli_function_id
						//,  L.fli_funding_id
						//,  L.fli_item_id
						//,  L.fli_region_id
						//,  L.fli_costing_id
						//,  L.fli_msc_id

		$sql = "SELECT L.fli_project
						, 0 as fli_function_id
						, 0 as fli_funding_id
						, 0 as fli_item_id
						, 0 as fli_region_id
						, 0 as fli_costing_id
						, 0 as fli_msc_id
						, T.frt_name
						, A.flia_month_id
						, SUM(flia_value) as flia_value
						FROM assist_finteg_line_item_amount A
						INNER JOIN  assist_finteg_line_item L
						ON A.flia_fli_id = L.fli_id 
						INNER JOIN  assist_finteg_record_type T
						ON T.frt_id = A.flia_frt_id 
						WHERE L.fli_fin_year_id = ".$financial_year_id."
						AND L.fli_ftt_id = ".$finteg_transaction_type."
						GROUP BY
						L.fli_project
						
						, T.frt_name
						, A.flia_month_id
						LIMIT ".$start." , ".$limit."
				";
				//, L.fli_function_id
				//		, L.fli_funding_id
				//		, L.fli_item_id
				//		, L.fli_region_id
				//		, L.fli_costing_id
				//		, L.fli_msc_id
			$line_item_amounts = $this->db_connection->mysql_fetch_all($sql);

//
//        echo "<h2>".$sql."</h2>";
//
//        echo '<pre style="font-size: 18px">';
//        echo '<p>LINE ITEM AMOUNTS</p>';
//        print_r($line_item_amounts);
//        echo '</pre>';

		if(count($line_item_amounts) > 0) {
			$line_item_data_structure = array();
			$count = 0;




			/*foreach($line_items as $key => $val) {
				$line_item_id = $val['fli_id'];
				$line_item_data_structure[$line_item_id]['line_item'] = $val;

				foreach($line_item_amounts as $key2 => $val2) {
					if($val2['flia_fli_id'] == $line_item_id) {
						$line_item_data_structure[$line_item_id]['line_item_amounts'][$val2['flia_id']] = $val2;
						unset($line_item_amounts[$key2]);
					}
				}
				unset($line_items[$key]);
				$count++;
			}*/

//        echo '<pre style="font-size: 18px">';
//        echo '<p>LINE ITEM DATA STRUCTURE</p>';
//        print_r($line_item_data_structure);
//        echo '</pre>';

			$result = array();
			$count = 0;
			foreach($line_item_amounts as $key => $line_item) {
				//$line_item = $val['line_item'];
				//create unique key for each possible combination of segments/projects/line_items coming from the database
				$array_key = $financial_year_id."|".$line_item['fli_project']."|".$line_item['fli_function_id']."|".$line_item['fli_funding_id']."|".$line_item['fli_region_id']."|".$line_item['fli_msc_id']."|".$line_item['fli_item_id'];
				$project_guid = $line_item['fli_project'];
				//if key isn't in array then create it & populate it
				if(!isset($result[$project_guid][$array_key])) {
					$result[$project_guid][$array_key]['fin_year_id'] = $financial_year_id;
					$result[$project_guid][$array_key]['function'] = $line_item['fli_function_id'];
					$result[$project_guid][$array_key]['funding'] = $line_item['fli_funding_id'];
					$result[$project_guid][$array_key]['region'] = $line_item['fli_region_id'];
					$result[$project_guid][$array_key]['msc'] = $line_item['fli_msc_id'];
					$result[$project_guid][$array_key]['item'] = $line_item['fli_item_id'];
					$result[$project_guid][$array_key]['results'] = array();
				}
				//add financials
				$month_id = $line_item['flia_month_id'];
				$frt_index = $line_item['frt_name'];
				if(!isset($result[$project_guid][$array_key]['results'][$month_id][$frt_index])) {
					$result[$project_guid][$array_key]['results'][$month_id][$frt_index] = $line_item['flia_value'];
				} else {
					$result[$project_guid][$array_key]['results'][$month_id][$frt_index]+=$line_item['flia_value'];
				}
//				foreach($val['line_item_amounts'] as $key2 => $val2) {
//					$frt_index = $val2['frt_name'];
//					$result[$project_guid][$count]['results'][$val2['flia_month_id']][$frt_index] = $val2['flia_value'];
//				}

				$count++;
			}

			unset($line_item_data_structure[$key]);

			return array('result'=>$result,'start'=>$start+$limit);
		} else {
			return array('result'=>array(),'start'=>-1);
		}
	}
}