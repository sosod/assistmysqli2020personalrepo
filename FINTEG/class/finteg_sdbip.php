<?php
class FINTEG_SDBIP extends FINTEG
{
    public function getAllTimePeriodsForAGivenClient($company_code) {

        $db = new ASSIST_DB("client",$company_code); //To be changed to a remote database call once ready to work across servers

        //which SDBIP module versions must I look in? - will probably be relocated to /library/class/assist.php at some point to centralise with other module lists
        $valid_sdbip_module_versions = array("SDBP6");

        //get list of active SDBIPs within the client database
        $sql = "SELECT modref FROM assist_menu_modules WHERE modlocation IN ('".implode("','",$valid_sdbip_module_versions)."') AND modyn = 'Y'";

        $modules = $db->mysql_fetch_all_by_value($sql,"modref");

        $data = array();

        if(count($modules)>0) {
            //check each active SDBIP for financial years and time periods
            foreach($modules as $i => $modref) {
                //set the db variables
                $db->setDBRef($modref, $company_code);

                $dbref = $db->getDBRef();

                //get the time periods details plus the financial year id from the main SDBIP table
                $sql = "SELECT T.id as id, T.code as code, DATE_FORMAT(T.end_date, '%M %Y') as name, S.sdbip_fin_year_id as fin_year_id
                        FROM ".$dbref."_setup_time T
                        INNER JOIN ".$dbref."_sdbip S
                        ON T.sdbip_id = S.sdbip_id AND (S.sdbip_status & 2) = 2
                        WHERE (T.status & 2) = 2";

                $rows = $db->mysql_fetch_all($sql);

                //merge the new rows into the existing data (resulting array will have time periods from multiple financial years)
                $data = array_merge($data,$rows);
            }
        }

        //return data
        return $data;
    }

    public function getFinancialYears($company_code) {

        if($_SERVER['SERVER_NAME'] == 'action4u.finteg'){
            $db = new ASSIST_DB("client",$company_code);
        }else{
            //$db = new ASSIST_DB_REMOTE('client', $company_code, 'TEST');//This used to get the financial years from assist
            $db = new ASSIST_DB("client",$company_code);
        }
        $sql = "SELECT id, fin_ref as code, value as name FROM assist_" . strtolower($company_code) . "_master_financialyears WHERE status = 1 ORDER BY value";
        $rows = $db->mysql_fetch_all_by_id($sql, 'code');
        return $rows;
    }
}