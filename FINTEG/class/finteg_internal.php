<?php


class FINTEG_INTERNAL extends FINTEG {


	public function __construct($modref="") {
		parent::__construct($modref);
	}



	public function getLatestFinances($cmpcode,$server,$type="INCOME",$financial_year_id=0,$start=0,$limit=FINTEG_FINANCIAL_DATA::LINE_LIMIT) {
		$data = array();
		$finteg_fin_data_obj = new FINTEG_FINANCIAL_DATA($cmpcode, $server);
            if($type == 'INCOME'){
                $data = $finteg_fin_data_obj->getLatestIncomeFinancialData($financial_year_id,$start,$limit);
            }else{
                $data = $finteg_fin_data_obj->getLatestExpenseFinancialData($financial_year_id,$start,$limit);
            }
		return $data;
	}

    public function getLatestLocallyUploadedFinances($cmpcode,$type="INCOME",$financial_year_id=0) {
        $data = array();
        $finteg_fin_data_obj = new FINTEG_LOCAL_FINANCIAL_DATA($cmpcode);
        if($type == 'INCOME'){
            $data = $finteg_fin_data_obj->getLatestIncomeFinancialData($financial_year_id);
        }else{
            $data = $finteg_fin_data_obj->getLatestExpenseFinancialData($financial_year_id);
        }
        return $data;
    }

    public function uploadFilesToCompanyPendingFolder($company_code, $files,$is_manual=false,$type=""){
        $file_handler_obj = new FINTEG_FILE_HANDLER();
        $file_handler_obj->processCompanyIntegrationDirectories($company_code);
        if($is_manual) {
        	$result = $file_handler_obj->uploadManualFileToCompanyPendingFolder($files,$type);
		} else {
        	$result = $file_handler_obj->uploadFilesToCompanyPendingFolder($files);
		}
        return $result;
    }

    public function processUploadedFileDataIntoTemporaryStorage($company_code,$file_name=false,$var = array()){
        $file_handler_obj = new FINTEG_FILE_HANDLER();
        $result = $file_handler_obj->processUploadedFileData($company_code,$file_name,true,$var);
        return $result;
    }



}

?>