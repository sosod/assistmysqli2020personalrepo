<?php

class FINTEG_MSCOA_SEGMENTS extends FINTEG
{
    public $projectcate_items;
    public $function_items;
    public $item_items;
    public $region_items;
    public $costing_items;
    public $msc_items;
    public $funding_items;

    public $projectcate_account_prefixes;
    public $function_account_prefixes;
    public $item_account_prefixes;
    public $region_account_prefixes;
    public $costing_account_prefixes;
    public $msc_account_prefixes;
    public $funding_account_prefixes;

    public $item_nicknames;

    private $projectcate_table_name = 'projectcate';
    private $function_table_name = 'function';
    private $item_table_name = 'item';
    private $region_table_name = 'region';
    private $costing_table_name = 'costing';
    private $msc_table_name = 'msc';
    private $funding_table_name = 'fund';

    private $company_code;
    private $modref;
    private $company_db_object;


    public function __construct($company_code){
        parent::__construct();
        $this->company_code = strtolower($company_code);
        $this->company_db_object = new ASSIST_MODULE_HELPER('client', $company_code, true, "");
        $this->prepareAllMscoaSegments();
        $this->prepareAllMscoaSegmentAccountPrefixes();
    }

    private function prepareAllMscoaSegments(){
        $this->projectcate_items = $this->getProcessedScoaSegmentItems($this->projectcate_table_name);
        $this->function_items = $this->getProcessedScoaSegmentItems($this->function_table_name);
        $this->item_items = $this->getProcessedScoaSegmentItems($this->item_table_name);
        $this->region_items = $this->getProcessedScoaSegmentItems($this->region_table_name);
        $this->costing_items = $this->getProcessedScoaSegmentItems($this->costing_table_name);
        $this->msc_items = $this->getProcessedScoaSegmentItems($this->msc_table_name);
        $this->funding_items = $this->getProcessedScoaSegmentItems($this->funding_table_name);
    }

    private function getProcessedScoaSegmentItems($table_name){
        $scoa_segment = $this->getScoaSegment($table_name);
        $scoa_segment_items = $this->processScoaSegmentItems($scoa_segment);
        return $scoa_segment_items;
    }

    private function getScoaSegment($table_name){
        $segment_table_name = 'assist_' . $this->company_code . '_mscoa_segment_' . $table_name;
        $sql = "SELECT id as id
                , ref
                , name as name
                , description as description
                , parent_id as parent
                , has_child as has_children
                , can_post as can_assign
                , IF(status = 2,1,0) as active
                FROM $segment_table_name S
                WHERE  (( S.status & 8) <> 8 OR ( S.status & 2) = 2 OR ( S.status & 4) = 4) 
              
                ORDER BY parent_id, name";
        $scoa_segment = $this->company_db_object->mysql_fetch_all_by_id($sql, 'id');

        return $scoa_segment;
    }

    private function processScoaSegmentItems($scoa_segment){
        $scoa_segment_items = array();
        foreach($scoa_segment as $key => $val){
            $scoa_segment_items[$key] = $val['ref'];
        }

        return $scoa_segment_items;
    }

    private function prepareAllMscoaSegmentAccountPrefixes(){
        $this->projectcate_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->projectcate_table_name);
        $this->function_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->function_table_name);
        $this->item_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->item_table_name);
        $this->region_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->region_table_name);
        $this->costing_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->costing_table_name);
        $this->msc_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->msc_table_name);
        $this->funding_account_prefixes = $this->getProcessedScoaSegmentAccountPrefix($this->funding_table_name);
    }

    private function getProcessedScoaSegmentAccountPrefix($table_name){
        $segment_table_name = 'assist_' . $this->company_code . '_mscoa_segment_' . $table_name;
        $sql = "SELECT id as id
                , ref
                , nickname as nickname
                FROM $segment_table_name S
                WHERE (( S.status & 8) <> 8 OR ( S.status & 2) = 2 OR ( S.status & 4) = 4) 
              
                ORDER BY parent_id, name";
        $item_nicknames = $this->company_db_object->mysql_fetch_value_by_id($sql, 'id', 'nickname');

        return $item_nicknames;
    }

    public function getItemsNicknamedIAA(){
        $segment_table_name = 'assist_' . $this->company_code . '_mscoa_segment_' . $this->item_table_name;
        $sql = "SELECT id as id
                , ref
                , nickname as nickname
                FROM $segment_table_name S
                WHERE nickname = 'IAA' 
                AND (( S.status & 8) <> 8 OR ( S.status & 2) = 2 OR ( S.status & 4) = 4) 
              
                ORDER BY parent_id, name";
        $item_nicknames = $this->company_db_object->mysql_fetch_value_by_id($sql, 'id', 'ref');

        return $item_nicknames;
    }

}