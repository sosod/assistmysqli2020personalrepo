<?php
//require_once $_SERVER['DOCUMENT_ROOT'] . '/library/composer/vendor/autoload.php';
//use Box\Spout\Reader\ReaderFactory;
//use Box\Spout\Common\Type;

class FINTEG_FILE_HANDLER extends FINTEG {

	private $finteg_files_dir;
	private $pending_validation_dir;
	private $successfully_validated_dir;
	private $unsuccessful_validation_dir;

	private $remote_lines_table = "finteg_all_lines";
	private $remote_fin_table = "finteg_all_financials";
	private $local_lines_table = "_financials_lines";
	private $local_fin_table = "_financials_lines_results";
	private $local_temp_table = "_financials_lines_import";


    public function __construct(){
        parent::__construct();
        $this->finteg_files_dir = $_SERVER['DOCUMENT_ROOT'] . "/FINTEG_FILES/";
        $this->pending_validation_dir = $this->finteg_files_dir . 'pending_validation/';
        $this->successfully_validated_dir = $this->finteg_files_dir . 'successfully_validated/';
        $this->unsuccessful_validation_dir = $this->finteg_files_dir . 'unsuccessful_validation/';
    }

    public function processCompanyIntegrationDirectories($company_code){
        if(!$this->companyIntegrationDirectoriesExist($company_code)){
            $this->createCompanyIntegrationDirectories($company_code);
            $this->processCompanyIntegrationDirectories($company_code);
        }else{
            return true;
        }

    }

    private function companyIntegrationDirectoriesExist($company_code){
        $companyIntegrationDirectoriesExist = false;
        if(is_dir($this->companyPendingDirectory($company_code))
            && is_dir($this->companySuccessfullyValidatedDirectory($company_code))
            && is_dir($this->companyUnsuccessfulValidationDirectory($company_code))){
            $companyIntegrationDirectoriesExist = true;
        }
        return $companyIntegrationDirectoriesExist;

    }

    private function createCompanyIntegrationDirectories($company_code){
        mkdir($this->companyPendingDirectory($company_code), 0777, true);
        mkdir($this->companySuccessfullyValidatedDirectory($company_code), 0777, true);
        mkdir($this->companyUnsuccessfulValidationDirectory($company_code), 0777, true);
    }

    private function companyPendingDirectory($company_code){
        return $this->pending_validation_dir.$company_code . $this->addDirectorySlash();
    }

    private function companySuccessfullyValidatedDirectory($company_code){
        return $this->successfully_validated_dir.$company_code . $this->addDirectorySlash();
    }

    private function companyUnsuccessfulValidationDirectory($company_code){
        return $this->unsuccessful_validation_dir.$company_code . $this->addDirectorySlash();
    }

    private function addDirectorySlash(){
        return '/';
    }

    public function processAllIntegrationData(){
        $company_object = new FINTEG_COMPANY();
        $companies_with_active_integration = $company_object->getAllCompaniesWithActiveFinancialIntegration();

        foreach($companies_with_active_integration as $comp_id => $company_record){
            $company_code = strtolower($company_record['ftc_cc']);
            $scoa_segments_object = new FINTEG_MSCOA_SEGMENTS($company_code);

            if(/*$this->retrievalMethodIsinternal($company_record)*/false){
                $directory_content = scandir($this->companyPendingDirectory($company_code), 1);

                $files_to_process = $this->getFilesToProcess($directory_content, $company_code);
                if(isset($files_to_process) && is_array($files_to_process) && count($files_to_process) > 0){
                    foreach($files_to_process as $key => $val){
                        $inputFileName = $val;

                        if($company_code == 'ort0001'){
                            $record_classification = array();
                            $record_classification['Budget'] = 1;
                            $record_classification['Actual'] = 2;
                            $record_classification['Adjustment'] = 3;

                            $months = array();
                            $months[1] = 'July';
                            $months[2] = 'August';
                            $months[3] = 'September';
                            $months[4] = 'October';
                            $months[5] = 'November';
                            $months[6] = 'December';
                            $months[7] = 'January';
                            $months[8] = 'February';
                            $months[9] = 'March';
                            $months[10] = 'April';
                            $months[11] = 'May';
                            $months[12] = 'June';

                            $fin_year_index = 1;

                            $project_index = 64;

                            $scoa_segment_indexes = array();
                            $scoa_segment_indexes['projectcate'] = 64;
                            $scoa_segment_indexes['function'] = 66;
                            $scoa_segment_indexes['item'] = 68;
                            $scoa_segment_indexes['region'] = 71;
                            $scoa_segment_indexes['costing'] = 70;
                            $scoa_segment_indexes['msc'] = 36;

                            $scoa_segment_indexes['funding'] = 69;


                            $starting_index_for_expense_budget = 7;
                            $starting_index_for_expense_actual = 19;
                            $starting_index_for_expense_adjustments = 19;

                            $starting_index_for_revenue_budget = 7;
                            $starting_index_for_revenue_actual = 19;
                            $starting_index_for_revenue_adjustments = 19;

                        }elseif($company_code == 'hes0001'){
                            $record_classification = array();
                            $record_classification['Budget'] = 1;
                            $record_classification['Actual'] = 2;
                            $record_classification['Adjustment'] = 3;

                            $months = array();
                            $months[1] = 'July';
                            $months[2] = 'August';
                            $months[3] = 'September';
                            $months[4] = 'October';
                            $months[5] = 'November';
                            $months[6] = 'December';
                            $months[7] = 'January';
                            $months[8] = 'February';
                            $months[9] = 'March';
                            $months[10] = 'April';
                            $months[11] = 'May';
                            $months[12] = 'June';

                            $fin_year_index = 14;

                            $project_index = 74;

                            $scoa_segment_indexes = array();
                            $scoa_segment_indexes['projectcate'] = 74;
                            $scoa_segment_indexes['function'] = 76;
                            $scoa_segment_indexes['item'] = 78;
                            $scoa_segment_indexes['region'] = 81;
                            $scoa_segment_indexes['costing'] = 80;
                            $scoa_segment_indexes['msc'] = 46;

                            $scoa_segment_indexes['funding'] = 79;


                            $starting_index_for_expense_budget = 17;
                            $starting_index_for_expense_actual = 29;
                            $starting_index_for_expense_adjustments = 29;

                            $starting_index_for_revenue_budget = 17;
                            $starting_index_for_revenue_actual = 29;
                            $starting_index_for_revenue_adjustments = 29;

                        }elseif($company_code == 'ipmsdev'){
                            $record_classification = array();
                            $record_classification['Budget'] = 1;
                            $record_classification['Actual'] = 2;
                            $record_classification['Adjustment'] = 3;

                            $months = array();
                            $months[1] = 'July';
                            $months[2] = 'August';
                            $months[3] = 'September';
                            $months[4] = 'October';
                            $months[5] = 'November';
                            $months[6] = 'December';
                            $months[7] = 'January';
                            $months[8] = 'February';
                            $months[9] = 'March';
                            $months[10] = 'April';
                            $months[11] = 'May';
                            $months[12] = 'June';

                            $fin_year_index = 1;

                            $project_index = 17;

                            $scoa_segment_indexes = array();
                            $scoa_segment_indexes['projectcate'] = 17;
                            $scoa_segment_indexes['function'] = 9;
                            $scoa_segment_indexes['item'] = 11;
                            $scoa_segment_indexes['region'] = 21;
                            $scoa_segment_indexes['costing'] = 25;
                            $scoa_segment_indexes['msc'] = 17;

                            $scoa_segment_indexes['funding'] = 0;

                            $starting_index_for_expense_budget = 45;
                            $starting_index_for_expense_actual = 32;
                            $starting_index_for_expense_adjustments = 32;

                            $starting_index_for_revenue_budget = 19;
                            $starting_index_for_revenue_actual = 4;
                            $starting_index_for_revenue_adjustments = 4;
                        }else{
                            $xlsx = new XLSXReader($val);
                            $sheets = $xlsx->getSheetNames();

                            if(is_array($sheets) && count($sheets) > 0){
                                foreach($sheets as $keyx => $valx){
                                    $file_lines = $xlsx->getSheetData($valx);
                                }

                                $record_classification = array();
                                $months = array();
                                if(is_array($file_lines) && count($file_lines) > 0){
                                    foreach($file_lines as $key2 => $val2){
                                        if(is_array($val2) && count($val2) > 1){//Count should be equal to the amount of elements we expect
                                            $months[$val2[2]] = 4;
                                            $record_classification['Budget'] = 1;
                                        }
                                    }
                                }

                                $project_index = 3;

                                $scoa_segment_indexes = array();
                                $scoa_segment_indexes['projectcate'] = 3;
                                $scoa_segment_indexes['function'] = 4;
                                $scoa_segment_indexes['item'] = 5;
                                $scoa_segment_indexes['region'] = 6;
                                $scoa_segment_indexes['costing'] = 7;
                                $scoa_segment_indexes['msc'] = 8;

                                $scoa_segment_indexes['funding'] = 5;

                                $starting_index_for_expense_budget = 9;
                                $starting_index_for_expense_actual = 9;
                                $starting_index_for_expense_adjustments = 9;

                                $starting_index_for_revenue_budget = 9;
                                $starting_index_for_revenue_actual = 9;
                                $starting_index_for_revenue_adjustments = 9;
                            }
                        }

                        //
                        $expenseLoopDataStructure = array();
                        $revenueLoopDataStructure = array();
                        foreach($record_classification as $rc_key => $rc_val){
                            $expenseLoopDataStructure[$rc_key]['id'] = $rc_val;
                            foreach($months as $m_key => $m_val){
                                $expenseLoopDataStructure[$rc_key]['months'][$m_key]['month_name'] = $m_val;

                                if($rc_val == 1){
                                    $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = $starting_index_for_expense_budget;
                                    $starting_index_for_expense_budget++;
                                    $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = $starting_index_for_revenue_budget;
                                    $starting_index_for_revenue_budget++;
                                }elseif($rc_val == 2){
                                    $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = $starting_index_for_expense_actual;
                                    $starting_index_for_expense_actual++;
                                    $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = $starting_index_for_revenue_actual;
                                    $starting_index_for_revenue_actual++;
                                }else{
                                    $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = $starting_index_for_expense_adjustments;
                                    $starting_index_for_expense_adjustments++;
                                    $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = $starting_index_for_revenue_adjustments;
                                    $starting_index_for_revenue_adjustments++;
                                }
                            }
                        }

                        //
                        //
                        $file_name_head = $this->companyPendingDirectory($company_code) . 'finteg_' . $company_code . '_';
                        $file_name_tail = $this->dataLoadFilenameTail();

                        $line_item_csv_file_name = $file_name_head . 'line_item' .  $file_name_tail;
                        $line_item_amount_csv_file_name = $file_name_head . 'line_item_amount' .  $file_name_tail;

                        $sdbip_obj = new FINTEG_SDBIP();
                        $fin_year = $sdbip_obj->getFinancialYears($company_code);

                        $fin_years_in_the_file = array();

                        $company_db_object = new ASSIST_MODULE_HELPER('client', /*$company_code*/'ipmsdev');

                        $original_line_item_id = $this->getAutoIncrementValue($company_db_object, 'assist_finteg_line_item');
                        $line_item_amount_id = $this->getAutoIncrementValue($company_db_object, 'assist_finteg_line_item_amount');

                        $line_item_csv_file = fopen($line_item_csv_file_name,"w");
                        $line_item_amount_csv_file = fopen($line_item_amount_csv_file_name,"w");

                        /**   NEW SPOUT LIBRARY CODE - START  */
                        if(mime_content_type($val) == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
                            $readerType = Type::XLSX;
                        }else{
                            $readerType = Type::CSV;
                        }
                        $reader = ReaderFactory::create($readerType);
                        $reader->open($inputFileName);

                        $row_number = 0;
                        $items_processed = 0;

                        foreach ($reader->getSheetIterator() as $sheet) {
                            if($sheet->getName() == 'gs360'){
                                $line_item_id = $original_line_item_id;

                                foreach ($sheet->getRowIterator() as $row) {
                                    if(is_array($row) && count($row) > 1 && $row_number > 0 &&
                                        (
                                            ($company_code == 'ipmsdev' && (count($row) == 58+1 || count($row) == 33)) ||
                                            ($company_code == 'ort0001' && ($row[48] == 'INCOME' || $row[48] == 'EXPENDITURE')) ||
                                            ($company_code == 'hes0001' && ($row[58] == 'INCOME' || $row[58] == 'EXPENDITURE')) ||
                                            ($company_code != 'ipmsdev' && $company_code != 'ort0001')
                                        ) //&& false
                                    ){//Count should be equal to the amount of elements we expect

                                        $items_processed = ($items_processed == 0 ? 1 : $items_processed );

                                        $financial_year = (isset($fin_year[$row[$fin_year_index]]) ? $fin_year[$row[$fin_year_index]]['id'] : 0 );

                                        if(!in_array($financial_year, $fin_years_in_the_file)){
                                            $fin_years_in_the_file[] = $financial_year;
                                        }

                                        $project = $this->cleanUpCellContent($row[$project_index]);

                                        $function = $this->cleanUpCellContent($row[$scoa_segment_indexes['function']]);
                                        $funding = $this->cleanUpCellContent($row[$scoa_segment_indexes['funding']]);
                                        $item = $this->cleanUpCellContent($row[$scoa_segment_indexes['item']]);
                                        $region = $this->cleanUpCellContent($row[$scoa_segment_indexes['region']]);
                                        $costing = $this->cleanUpCellContent($row[$scoa_segment_indexes['costing']]);
                                        $msc = $this->cleanUpCellContent($row[$scoa_segment_indexes['msc']]);

                                        $line_item_insert_values = array();
                                        $line_item_insert_values['fli_id'] = $line_item_id;
                                        $line_item_insert_values['fli_fin_year_id'] = $financial_year;
                                        $line_item_insert_values['fli_project'] = (strlen($project) > 0 ? $project : ' ' );
                                        $line_item_insert_values['fli_function_id'] = (in_array($function, $scoa_segments_object->function_items) ? array_search($function, $scoa_segments_object->function_items) : 0 );
                                        $line_item_insert_values['fli_funding_id'] = (in_array($funding, $scoa_segments_object->funding_items) ? array_search($funding, $scoa_segments_object->funding_items) : 0 );
                                        $line_item_insert_values['fli_item_id'] = (in_array($item, $scoa_segments_object->item_items) ? array_search($item, $scoa_segments_object->item_items) : 0 );
                                        $line_item_insert_values['fli_region_id'] = (in_array($region, $scoa_segments_object->region_items) ? array_search($region, $scoa_segments_object->region_items) : 0 );
                                        $line_item_insert_values['fli_costing_id'] = (in_array($costing, $scoa_segments_object->costing_items) ? array_search($costing, $scoa_segments_object->costing_items) : 0 );
                                        $line_item_insert_values['fli_msc_id'] = (in_array($msc, $scoa_segments_object->msc_items) ? array_search($msc, $scoa_segments_object->msc_items) : 0 );

                                        if(($company_code == 'ipmsdev' && count($row) == 33) || ($company_code == 'ort0001' && $row[48] == 'INCOME') || ($company_code == 'hes0001' && $row[58] == 'INCOME')){
                                            $line_item_insert_values['fli_ftt_id'] = 1;//Income
                                        }else{
                                            $line_item_insert_values['fli_ftt_id'] = 2;//Expense
                                        }

                                        $line_item_csv_insert = $line_item_insert_values['fli_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_fin_year_id'] . ',';
                                        $line_item_csv_insert .= trim($line_item_insert_values['fli_project']) . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_function_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_funding_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_item_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_region_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_costing_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_msc_id'] . ',';
                                        $line_item_csv_insert .= $line_item_insert_values['fli_ftt_id'] . ',';

                                        fputcsv($line_item_csv_file,explode(',',$line_item_csv_insert));

                                        //Loop through the Record classifications
                                        foreach($record_classification as $rc_key => $rc_val){
                                            //Loop each month for each record classification
                                            foreach($months as $m_key => $m_val){
                                                //Then insert the values into the db
                                                $line_item_amount_insert_values = array();
                                                $line_item_amount_insert_values['flia_id'] = $line_item_amount_id;
                                                $line_item_amount_insert_values['flia_fli_id'] = $line_item_id;
                                                $line_item_amount_insert_values['flia_month_id'] = $m_key;
                                                $line_item_amount_insert_values['flia_amount'] = (float)$row[(($company_code == 'ipmsdev' && count($row) == 33) || ($company_code == 'ort0001' && $row[48] == 'INCOME') ? $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] : $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'])];
                                                $line_item_amount_insert_values['flia_frt_id'] = $rc_val;


                                                $line_item_amount_csv_insert = $line_item_amount_insert_values['flia_id'] . ',';
                                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_fli_id'] . ',';
                                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_month_id'] . ',';
                                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_amount'] . ',';
                                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_frt_id'];

                                                fputcsv($line_item_amount_csv_file,explode(',',$line_item_amount_csv_insert));

                                                $line_item_amount_id++;

                                                $items_processed++;
                                            }
                                        }
                                        $line_item_id++;
                                    }
                                    $row_number++;
                                }
                            }

                        }

                        $reader->close();
                        /**   NEW SPOUT LIBRARY CODE - END  */


                        fclose($line_item_csv_file);
                        fclose($line_item_amount_csv_file);

                        $successfully_processed = false;
                        if(strlen(file_get_contents($line_item_csv_file_name)) > 0 && strlen(file_get_contents($line_item_amount_csv_file_name))){
                            $this->deleteRecordsThatMatchTheProcessedData($company_db_object, $fin_years_in_the_file, $months);

                            $id = $this->processCsvIntoTheDB($company_db_object, $line_item_csv_file_name, 'assist_finteg_line_item');
                            $id = $this->processCsvIntoTheDB($company_db_object, $line_item_amount_csv_file_name, 'assist_finteg_line_item_amount');

                            $successfully_processed = true;
                        }

                        unlink($line_item_csv_file_name);
                        unlink($line_item_amount_csv_file_name);

                        $this->moveInputFileToAppropriateDirectory($inputFileName, $successfully_processed, $company_code);
                    }
                }
            }else{/* The Retrival Method is External*/
                //get the oci db details
                $oracle_conn = oci_connect('aassist_user', 'ksjdfvuajsg', '10.56.0.1/MUNPRO3i');

                if (!$oracle_conn) {
                    $e = oci_error();
                    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                }

                //GET THE FIELDS
                $record_classification = array();
                $record_classification['Budget'] = 1;
                $record_classification['Actual'] = 2;
                $record_classification['Adjustment'] = 3;

                $months = array();
                $months[1] = 'July';
                $months[2] = 'Aug';
                $months[3] = 'Sep';
                $months[4] = 'Oct';
                $months[5] = 'Nov';
                $months[6] = 'Dec';
                $months[7] = 'Jan';
                $months[8] = 'Feb';
                $months[9] = 'March';
                $months[10] = 'April';
                $months[11] = 'May';
                $months[12] = 'June';

                $fin_year_index = 'FIN_YEAR';

                $project_index = 'PROJECT';

                $scoa_segment_indexes = array();
                $scoa_segment_indexes['projectcate'] = 'PROJECT';
                $scoa_segment_indexes['function'] = 'FUNCTION';
                $scoa_segment_indexes['item'] = 'ITEM';
                $scoa_segment_indexes['region'] = 'REGION';
                $scoa_segment_indexes['costing'] = 'COSTING';
                $scoa_segment_indexes['msc'] = 'MUN_FUNCTION';

                $scoa_segment_indexes['funding'] = 'FUND';


                $starting_index_for_expense_budget = 'JULY_BUDGET';
                $starting_index_for_expense_actual = 'JULY_ACTUAL';
                $starting_index_for_expense_adjustments = 'JULY_ACTUAL';

                $starting_index_for_revenue_budget = 'JULY_BUDGET';
                $starting_index_for_revenue_actual = 'JULY_ACTUAL';
                $starting_index_for_revenue_adjustments = 'JULY_ACTUAL';



                /*********************/////////////////////////////////////////
                $expenseLoopDataStructure = array();
                $revenueLoopDataStructure = array();
                foreach($record_classification as $rc_key => $rc_val){
                    $expenseLoopDataStructure[$rc_key]['id'] = $rc_val;
                    foreach($months as $m_key => $m_val){
                        $expenseLoopDataStructure[$rc_key]['months'][$m_key]['month_name'] = $m_val;

                        if($rc_val == 1){
                            $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = strtoupper($m_val) . '_BUDGET';
                            $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = strtoupper($m_val) . '_BUDGET';
                        }elseif($rc_val == 2){
                            $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = strtoupper($m_val) . '_ACTUAL';
                            $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = strtoupper($m_val) . '_ACTUAL';
                        }else{
                            $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = strtoupper($m_val) . '_ACTUAL';
                            $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] = strtoupper($m_val) . '_ACTUAL';
                        }
                    }
                }

                //
                //
                $file_name_head = $this->companyPendingDirectory($company_code) . 'finteg_' . $company_code . '_';
                $file_name_tail = $this->dataLoadFilenameTail();

                $line_item_csv_file_name = $file_name_head . 'line_item' .  $file_name_tail;
                $line_item_amount_csv_file_name = $file_name_head . 'line_item_amount' .  $file_name_tail;

                $sdbip_obj = new FINTEG_SDBIP();
                $fin_year = $sdbip_obj->getFinancialYears($company_code);

                $fin_years_in_the_file = array();

                $company_db_object = new ASSIST_MODULE_HELPER('client', /*$company_code*/'ipmsdev');

                $original_line_item_id = $this->getAutoIncrementValue($company_db_object, 'assist_finteg_line_item');
                $line_item_amount_id = $this->getAutoIncrementValue($company_db_object, 'assist_finteg_line_item_amount');

                $line_item_csv_file = fopen($line_item_csv_file_name,"w");
                $line_item_amount_csv_file = fopen($line_item_amount_csv_file_name,"w");

                /*READ BEFORE LOOP*/
                //$sql = 'SELECT * FROM MUNPRO.AA_ACTUALS_BUDGET FETCH FIRST 5 ROWS ONLY';
                $sql = 'SELECT * FROM MUNPRO.AA_ACTUALS_BUDGET';
                $sql_execution = oci_parse($oracle_conn, $sql);
                oci_execute($sql_execution);

                $rows = array();
                while ($row = oci_fetch_array($sql_execution, OCI_ASSOC+OCI_RETURN_NULLS)) {
                    $rows[] = $row;
                }

                $row_number = 0;
                $items_processed = 0;

                $line_item_id = $original_line_item_id;
                foreach($rows as $key => $row) {
                    if(is_array($row) && count($row) > 1 && $row_number > 0 &&
                        (
                            ($company_code == 'ipmsdev' && (count($row) == 58+1 || count($row) == 33)) ||
                            ($company_code == 'ort0001' && isset($row[48]) && ($row[48] == 'INCOME' || $row[48] == 'EXPENDITURE')) ||
                            ($company_code == 'hes0001' && isset($row[58]) && ($row[58] == 'INCOME' || $row[58] == 'EXPENDITURE')) ||
                            ($company_code != 'ipmsdev' && $company_code != 'ort0001')
                        ) //&& false
                    ){//Count should be equal to the amount of elements we expect

                        $items_processed = ($items_processed == 0 ? 1 : $items_processed );

                        $financial_year = (isset($fin_year[$row[$fin_year_index]]) ? $fin_year[$row[$fin_year_index]]['id'] : 0 );

                        if(!in_array($financial_year, $fin_years_in_the_file)){
                            $fin_years_in_the_file[] = $financial_year;
                        }

                        $project = $this->cleanUpCellContent($row[$project_index]);

                        $function = $this->cleanUpCellContent($row[$scoa_segment_indexes['function']]);
                        $funding = $this->cleanUpCellContent($row[$scoa_segment_indexes['funding']]);
                        $item = $this->cleanUpCellContent($row[$scoa_segment_indexes['item']]);
                        $region = $this->cleanUpCellContent($row[$scoa_segment_indexes['region']]);
                        $costing = $this->cleanUpCellContent($row[$scoa_segment_indexes['costing']]);
                        $msc = $this->cleanUpCellContent($row[$scoa_segment_indexes['msc']]);

                        $line_item_insert_values = array();
                        $line_item_insert_values['fli_id'] = $line_item_id;
                        $line_item_insert_values['fli_fin_year_id'] = $financial_year;
                        $line_item_insert_values['fli_project'] = (strlen($project) > 0 ? $project : ' ' );
                        $line_item_insert_values['fli_function_id'] = (in_array($function, $scoa_segments_object->function_items) ? array_search($function, $scoa_segments_object->function_items) : 0 );
                        $line_item_insert_values['fli_funding_id'] = (in_array($funding, $scoa_segments_object->funding_items) ? array_search($funding, $scoa_segments_object->funding_items) : 0 );
                        $line_item_insert_values['fli_item_id'] = (in_array($item, $scoa_segments_object->item_items) ? array_search($item, $scoa_segments_object->item_items) : 0 );
                        $line_item_insert_values['fli_region_id'] = (in_array($region, $scoa_segments_object->region_items) ? array_search($region, $scoa_segments_object->region_items) : 0 );
                        $line_item_insert_values['fli_costing_id'] = (in_array($costing, $scoa_segments_object->costing_items) ? array_search($costing, $scoa_segments_object->costing_items) : 0 );
                        $line_item_insert_values['fli_msc_id'] = (in_array($msc, $scoa_segments_object->msc_items) ? array_search($msc, $scoa_segments_object->msc_items) : 0 );

                        if(($company_code == 'ipmsdev' && count($row) == 33) || ($company_code == 'ort0001' && isset($row[48]) && $row[48] == 'INCOME') || ($company_code == 'hes0001' && isset($row[58]) && $row[58] == 'INCOME')){
                            $line_item_insert_values['fli_ftt_id'] = 1;//Income
                        }else{
                            $line_item_insert_values['fli_ftt_id'] = 2;//Expense
                        }

                        $line_item_csv_insert = $line_item_insert_values['fli_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_fin_year_id'] . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fli_project']) . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_function_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_funding_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_item_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_region_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_costing_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_msc_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['fli_ftt_id'] . ',';

                        fputcsv($line_item_csv_file,explode(',',$line_item_csv_insert));

                        //Loop through the Record classifications
                        foreach($record_classification as $rc_key => $rc_val){
                            //Loop each month for each record classification
                            foreach($months as $m_key => $m_val){
                                //Then insert the values into the db
                                $line_item_amount_insert_values = array();
                                $line_item_amount_insert_values['flia_id'] = $line_item_amount_id;
                                $line_item_amount_insert_values['flia_fli_id'] = $line_item_id;
                                $line_item_amount_insert_values['flia_month_id'] = $m_key;
                                $line_item_amount_insert_values['flia_amount'] = (float)$row[(($company_code == 'ipmsdev' && count($row) == 33) || ($company_code == 'ort0001' && isset($row[48]) && $row[48] == 'INCOME') ? $revenueLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'] : $expenseLoopDataStructure[$rc_key]['months'][$m_key]['amount_index'])];
                                $line_item_amount_insert_values['flia_frt_id'] = $rc_val;


                                $line_item_amount_csv_insert = $line_item_amount_insert_values['flia_id'] . ',';
                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_fli_id'] . ',';
                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_month_id'] . ',';
                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_amount'] . ',';
                                $line_item_amount_csv_insert .= $line_item_amount_insert_values['flia_frt_id'];

                                fputcsv($line_item_amount_csv_file,explode(',',$line_item_amount_csv_insert));

                                $line_item_amount_id++;

                                $items_processed++;
                            }
                        }
                        $line_item_id++;
                    }
                    $row_number++;

                }

                fclose($line_item_csv_file);
                fclose($line_item_amount_csv_file);

                if(strlen(file_get_contents($line_item_csv_file_name)) > 0 && strlen(file_get_contents($line_item_amount_csv_file_name))){
                    $this->deleteRecordsThatMatchTheProcessedData($company_db_object, $fin_years_in_the_file, $months);

                    $id = $this->processCsvIntoTheDB($company_db_object, $line_item_csv_file_name, 'assist_finteg_line_item');
                    $id = $this->processCsvIntoTheDB($company_db_object, $line_item_amount_csv_file_name, 'assist_finteg_line_item_amount');
                }

                unlink($line_item_csv_file_name);
                unlink($line_item_amount_csv_file_name);

            }



        }
    }

    public function processUploadedFileData($company_code,$file_name=false,$temporary_storage = false, $var=array()){
    	$data_type = $var['fin_type'];
    	$time_id = $var['time_id'];
    	$import_id = $var['import_id'];
        $directory_content = scandir($this->companyPendingDirectory($company_code), 1);
        //check for manual file name passed in from finteg_internal - if it is set then only process that file and ignore all others - just in case of a break in process that has left errant files behind - AA-147 [JC] 3 Feb 2020
		if($file_name !== false) {
			foreach($directory_content as $key => $dc) {
				if($dc != $file_name) {
					unset($directory_content[$key]);
				}
			}
		}
        $files_to_process = $this->getFilesToProcess($directory_content, $company_code);
        $scoa_segments_object = new FINTEG_MSCOA_SEGMENTS('mscoa01');
        if(isset($files_to_process) && is_array($files_to_process) && count($files_to_process) > 0){
            foreach($files_to_process as $key => $val){
                $inputFileName = $val;

                if (mime_content_type($inputFileName) == "text/plain") {

                    $file_lines = array();
                    if ($fh = fopen($inputFileName, 'r')) {
                        while (!feof($fh)) {
                            $file_lines[] = explode('|', fgets($fh));
                        }
                        fclose($fh);
                    }

                    $record_classification = array();
                    $months = array();
                    if(is_array($file_lines) && count($file_lines) > 0){
                        foreach($file_lines as $key2 => $val2){
                            if(is_array($val2) && count($val2) > 1){//Count should be equal to the amount of elements we expect
                                $months[$time_id] = $val2[2];
                                $record_classification['Budget'] = 1;
                            }
                        }
                    }

                    $fin_year_index = 1;

                    $project_index = 3;

                    $scoa_segment_indexes = array();
                    $scoa_segment_indexes['projectcate'] = 3;//JC confirmed on MOS0001 import 24 August 2021
                    $scoa_segment_indexes['function'] = 4;//JC confirmed on MOS0001 import 24 August 2021
                    $scoa_segment_indexes['item'] = 5;//JC confirmed on MOS0001 import 24 August 2021
                    $scoa_segment_indexes['region'] = 8;//6;//JC confirmed on MOS0001 import 24 August 2021
                    $scoa_segment_indexes['costing'] = 7;//JC confirmed on MOS0001 import 24 August 2021
                    $scoa_segment_indexes['msc'] = 10;//8;//JC confirmed on MOS0001 import 24 August 2021

                    $scoa_segment_indexes['funding'] = 6;//5;//JC confirmed on MOS0001 import 24 August 2021

                    $starting_index_for_expense_budget = 9;
                    $starting_index_for_expense_actual = 9;
                    $starting_index_for_expense_adjustments = 9;

                    $starting_index_for_revenue_budget = 9;
                    $starting_index_for_revenue_actual = 9;
                    $starting_index_for_revenue_adjustments = 9;
                } else {
                	return array("error","Invalid file type : $inputFileName checked as '".mime_content_type($inputFileName)."' but only 'text/plain' is accepted.");
				}
/*
                $LoopDataStructure = array();
                foreach($months as $m_key => $m_val){
                    $LoopDataStructure['months'][$m_key]['month_name'] = $m_val;
                    $LoopDataStructure['months'][$m_key]['budget_amount_index'] = $starting_index_for_expense_budget;
                    $LoopDataStructure['months'][$m_key]['actual_amount_index'] = $starting_index_for_expense_actual;
                    $starting_index_for_expense_budget++;
                    $starting_index_for_expense_actual++;
                }*/

                //
                //
                $file_name_head = $this->companyPendingDirectory($company_code) . 'finteg_' . $company_code . '_';
                $file_name_tail = $this->dataLoadFilenameTail();

                $line_item_csv_file_name = $file_name_head . 'line_item' .  $file_name_tail;
                $line_item_amount_csv_file_name = $file_name_head . 'line_item_amount' .  $file_name_tail;

                /*$sdbip_obj = new FINTEG_SDBIP();
                $fin_year = $sdbip_obj->getFinancialYears($company_code);

                $fin_years_in_the_file = array();*/

                $company_db_object = new ASSIST_MODULE_HELPER('client', $company_code, true);
                $company_db_line_table = $company_db_object->getDBRef().$this->local_lines_table;
                $company_db_fin_table = $company_db_object->getDBRef().$this->local_fin_table;
                $company_db_temp_table = $company_db_object->getDBRef().$this->local_temp_table;

                //$finteg_db_object = new ASSIST_DB("finteg");
//Removing unused CSV stuff [JC] 2021-08-25
//                $original_line_item_id = $this->getAutoIncrementValue($company_db_object, $company_db_line_table);
//                $line_item_amount_id = $this->getAutoIncrementValue($company_db_object, $company_db_fin_table);

//                $line_item_csv_file = fopen($line_item_csv_file_name,"w");
//                $line_item_amount_csv_file = fopen($line_item_amount_csv_file_name,"w");

//                $row_number = 0;
//                $items_processed = 0;

//                $line_item_id = $original_line_item_id;
                foreach($file_lines as $row_key => $row) {
					if(is_array($row) && count($row) > 1 && count($row) == 10) {
						/*$financial_year = (isset($fin_year[$row[$fin_year_index]]) ? $fin_year[$row[$fin_year_index]]['id'] : 0);
						if($financial_year == 0) {
							return array("error",
										 "Unknown financial year \"".$row[$fin_year_index]."\".  Please ensure that the financial year given in the attached document matches the Financial Year Code indicated above.");
						}*/
						/*if(!in_array($financial_year, $fin_years_in_the_file)) {
							$fin_years_in_the_file[] = $financial_year;
						}*/

						$project = $this->cleanUpCellContent($row[$project_index]);

						$function = $this->cleanUpCellContent($row[$scoa_segment_indexes['function']]);
						$funding = $this->cleanUpCellContent($row[$scoa_segment_indexes['funding']]);
						$item = $this->cleanUpCellContent($row[$scoa_segment_indexes['item']]);
						$region = $this->cleanUpCellContent($row[$scoa_segment_indexes['region']]);
						$costing = $this->cleanUpCellContent($row[$scoa_segment_indexes['costing']]);
						$msc = $this->cleanUpCellContent(isset($row[$scoa_segment_indexes['msc']])?$row[$scoa_segment_indexes['msc']]:"");

						$line_item_insert_values = array();
//                        $line_item_insert_values['line_id'] = $line_item_id;
						$line_item_insert_values['import_id'] = $import_id;

						$project_nt_guid = substr($project, 0, 36);
						$pc_nt_guid = $project_nt_guid;
						$pc_valid = '0';
						$pc_prefix = 'NA';
						if(in_array($project_nt_guid, $scoa_segments_object->projectcate_items)) {
							$pc_valid = '1';
							$pc_prefix = $scoa_segments_object->projectcate_account_prefixes[array_search($project_nt_guid, $scoa_segments_object->projectcate_items)];
						}
						$line_item_insert_values['pc_nt_guid'] = $pc_nt_guid;
						$line_item_insert_values['pc_client_guid'] = $project;
						$line_item_insert_values['pc_valid'] = $pc_valid;
						$line_item_insert_values['pc_prefix'] = $pc_prefix;

						$item_nt_guid = substr($item, 0, 36);
						$itm_nt_guid = $item_nt_guid;
						$itm_valid = '0';
						$itm_prefix = 'NA';
						if(in_array($item_nt_guid, $scoa_segments_object->item_items)) {
							$itm_valid = '1';
							$itm_prefix = $scoa_segments_object->item_account_prefixes[array_search($item_nt_guid, $scoa_segments_object->item_items)];
						}
						$line_item_insert_values['item_nt_guid'] = $itm_nt_guid;
						$line_item_insert_values['item_client_guid'] = $item;
						$line_item_insert_values['item_valid'] = $itm_valid;
						$line_item_insert_values['item_prefix'] = $itm_prefix;

						$funding_nt_guid = substr($funding, 0, 36);
						$fund_nt_guid = $funding_nt_guid;
						$fund_valid = '0';
						$fund_prefix = 'NA';
						if(in_array($funding_nt_guid, $scoa_segments_object->funding_items)) {
							$fund_valid = '1';
							$fund_prefix = $scoa_segments_object->funding_account_prefixes[array_search($funding_nt_guid, $scoa_segments_object->funding_items)];
						}
						$line_item_insert_values['fund_nt_guid'] = $fund_nt_guid;
						$line_item_insert_values['fund_client_guid'] = $funding;
						$line_item_insert_values['fund_valid'] = $fund_valid;
						$line_item_insert_values['fund_prefix'] = $fund_prefix;

						$function_nt_guid = substr($function, 0, 36);
						$fx_nt_guid = $function_nt_guid;
						$fx_valid = '0';
						$fx_prefix = 'NA';
						if(in_array($function_nt_guid, $scoa_segments_object->function_items)) {
							$fx_valid = '1';
							$fx_prefix = $scoa_segments_object->function_account_prefixes[array_search($function_nt_guid, $scoa_segments_object->function_items)];
						}
						$line_item_insert_values['fx_nt_guid'] = $fx_nt_guid;
						$line_item_insert_values['fx_client_guid'] = $function;
						$line_item_insert_values['fx_valid'] = $fx_valid;
						$line_item_insert_values['fx_prefix'] = $fx_prefix;

						$costing_nt_guid = substr($costing, 0, 36);
						$cost_nt_guid = $costing_nt_guid;
						$cost_valid = '0';
						//Fix prefix to CO - only prefix in NT document [JC] 2021-08-25
						$cost_prefix = 'CO';
						if(in_array($costing_nt_guid, $scoa_segments_object->costing_items)) {
							$cost_valid = '1';
							$cost_prefix = 'CO';//$scoa_segments_object->costing_account_prefixes[array_search($costing_nt_guid, $scoa_segments_object->costing_items)];
						}
						$line_item_insert_values['cost_nt_guid'] = $cost_nt_guid;
						$line_item_insert_values['cost_client_guid'] = $costing;
						$line_item_insert_values['cost_valid'] = $cost_valid;
						$line_item_insert_values['cost_prefix'] = $cost_prefix;

						$region_nt_guid = substr($region, 0, 36);
						$reg_nt_guid = $region_nt_guid;
						$reg_valid = '0';
						$reg_prefix = 'NA';
						if(in_array($region_nt_guid, $scoa_segments_object->region_items)) {
							$reg_valid = '1';
							$reg_prefix = $scoa_segments_object->region_account_prefixes[array_search($region_nt_guid, $scoa_segments_object->region_items)];
						}
						$line_item_insert_values['reg_nt_guid'] = $reg_nt_guid;
						$line_item_insert_values['reg_client_guid'] = $region;
						$line_item_insert_values['reg_valid'] = $reg_valid;
						$line_item_insert_values['reg_prefix'] = $reg_prefix;

						$line_item_insert_values['msc_name'] = '';
						$line_item_insert_values['msc_guid'] = $msc;
						$line_item_insert_values['value'] = (float)$row[$starting_index_for_expense_budget];
						$line_item_insert_values['line_status'] = 2;

						//insert db record here to temp table & change IAA update below to temp table
						$sql = "INSERT INTO ".$company_db_temp_table." SET ".$this->convertArrayToSQLForSave($line_item_insert_values);
						$company_db_object->db_insert($sql);

					}
				}

/*
                        $line_item_csv_insert = $line_item_insert_values['line_id'] . ',';
                        $line_item_csv_insert .= $line_item_insert_values['line_fin_year'] . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['pc_nt_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['pc_client_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['pc_valid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['pc_prefix']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['item_nt_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['item_client_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['item_valid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['item_prefix']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fund_nt_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fund_client_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fund_valid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fund_prefix']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fx_nt_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fx_client_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fx_valid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['fx_prefix']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['reg_nt_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['reg_client_guid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['reg_valid']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['reg_prefix']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['msc_name']) . ',';
                        $line_item_csv_insert .= trim($line_item_insert_values['msc_guid']) . ',';
                        $line_item_csv_insert .= $line_item_insert_values['line_status'] . ',';

                        fputcsv($line_item_csv_file,explode(',',$line_item_csv_insert));

                        //Loop each month
                        foreach($LoopDataStructure['months'] as $m_key => $m_val){
                            //Then insert the values into the db
                            $line_item_amount_insert_values = array();
                            $line_item_amount_insert_values['fin_id'] = $line_item_amount_id;
                            $line_item_amount_insert_values['fin_line_id'] = $line_item_id;
                            $line_item_amount_insert_values['fin_time_id'] = $m_key;
                            $line_item_amount_insert_values['fin_budget'] = (float)$row[$m_val['budget_amount_index']];
                            $line_item_amount_insert_values['fin_actual'] = (float)$row[$m_val['actual_amount_index']];


                            $line_item_amount_csv_insert = $line_item_amount_insert_values['fin_id'] . ',';
                            $line_item_amount_csv_insert .= $line_item_amount_insert_values['fin_line_id'] . ',';
                            $line_item_amount_csv_insert .= $line_item_amount_insert_values['fin_time_id'] . ',';
                            $line_item_amount_csv_insert .= $line_item_amount_insert_values['fin_budget'] . ',';
                            $line_item_amount_csv_insert .= $line_item_amount_insert_values['fin_actual'];

                            fputcsv($line_item_amount_csv_file,explode(',',$line_item_amount_csv_insert));

                            $line_item_amount_id++;

                            $items_processed++;
                        }
                        $line_item_id++;
                    }
                }

                fclose($line_item_csv_file);
                fclose($line_item_amount_csv_file);

                $action_log = 'No Data to be Processed';
                $successfully_processed = false;

                if(strlen(file_get_contents($line_item_csv_file_name)) > 0 && strlen(file_get_contents($line_item_amount_csv_file_name))){
                    $this->deleteRecordsThatMatchTheProcessedData($company_db_object, $fin_years_in_the_file, $months,$company_db_line_table,$company_db_fin_table);

                    $id = $this->processCsvIntoTheDB($company_db_object, $line_item_csv_file_name, $company_db_line_table);
                    $id = $this->processCsvIntoTheDB($company_db_object, $line_item_amount_csv_file_name, $company_db_fin_table);

                    $action_log = 'Data Processed Successfully';
                    $successfully_processed = true;
                }

                //$results_for_html_logs[$company_id]['action_log'][] = $action_log;

                unlink($line_item_csv_file_name);
                unlink($line_item_amount_csv_file_name);

                $this->moveInputFileToAppropriateDirectory($inputFileName, $successfully_processed, $company_code);
*/





                ////********************************************************************/////////**********/////////*
                //if($successfully_processed === true) {

					$item_nicknamed_IAA = $scoa_segments_object->getItemsNicknamedIAA();

					$sql = 'UPDATE '.$company_db_temp_table.' ';
					$sql .= 'SET item_prefix = \'IAA\' ';
					$sql .= 'WHERE item_nt_guid IN (\''.implode("','", $item_nicknamed_IAA).'\') ';
					$id = $company_db_object->db_update($sql);
				//}

            }
        }
        return array('ok', 'We got through it');
    }

    private function retrievalMethodIsinternal($company_record){
        $retrievalMethodIsInternal = false;
        if($company_record['ftrm_name'] == 'Internal'){
            $retrievalMethodIsInternal = true;
        }
        return $retrievalMethodIsInternal;
    }

    private function getFilesToProcess($directory_content, $company_code){

        $files_to_process = array();
        foreach($directory_content as $key => $val){
            $file_directory = $this->companyPendingDirectory($company_code) . $val;
            if(is_file($file_directory)){
                $files_to_process[] = $file_directory;
            }
        }

        return $files_to_process;
    }

    private function dataLoadFilenameTail(){
        date_default_timezone_set('Africa/Johannesburg');
        $date_in_ddmmyyy_format = date("dmY");
        $time_in_24hmm_format = date("His");
        $csv_extension = '.csv';
        $file_name_tail = '_' . $date_in_ddmmyyy_format . '_' . $time_in_24hmm_format . $csv_extension;
        return $file_name_tail;
    }

    private function getAutoIncrementValue($company_db_object, $table_name){
        $sql = "SHOW TABLE STATUS WHERE `Name` = '$table_name'";
        $data = $company_db_object->mysql_fetch_one($sql);
        //remove mysqli specific db calls [JC] 25 August 2021
        //$result = mysqli_query($company_db_object->getConn(), $sql);
        //$data = mysqli_fetch_assoc($result);
        $auto_inc = (int)$data['Auto_increment'];
        return $auto_inc;
    }

    private function cleanUpCellContent($cell_content){
        $cleaned_up_cell_content = strtolower($cell_content);
        $cleaned_up_cell_content = str_replace ('"', '', $cleaned_up_cell_content);
        $cleaned_up_cell_content = str_replace (' ', '', $cleaned_up_cell_content);
        $cleaned_up_cell_content = trim($cleaned_up_cell_content);
        return $cleaned_up_cell_content;
    }

    private function deleteRecordsThatMatchTheProcessedData($company_db_object, $fin_years_in_the_file, $months,$lines_table="finteg_all_lines",$fin_table="finteg_all_financials"){
        $delete_matching_data_sql = "DELETE $lines_table , $fin_table ";
        $delete_matching_data_sql .= "FROM $lines_table ";
        $delete_matching_data_sql .= "INNER JOIN $fin_table ";
        $delete_matching_data_sql .= "ON ".$lines_table.".line_id = ".$fin_table.".fin_line_id ";
        $delete_matching_data_sql .= "WHERE ".$lines_table.".line_fin_year IN (" . implode(",", $fin_years_in_the_file) . ") ";
        $delete_matching_data_sql .= "AND ".$fin_table.".fin_time_id IN (" . implode(",", array_flip($months)) . ") ";
        $company_db_object->db_query($delete_matching_data_sql);
    }

    private function processCsvIntoTheDB($company_db_object, $file_name, $table_name){
        $id = false;
        if(strlen(file_get_contents($file_name)) > 0){
            //$company_db_object->db_query($this->truncateTableSQL($table_name));
            $id = $company_db_object->db_query($this->loadDataInFileSQL($file_name, $table_name));
        }
        return $id;
    }

    private function loadDataInFileSQL($file_name, $table_name){
        $sql = "LOAD DATA LOCAL INFILE '$file_name' ";
        $sql .= "INTO TABLE $table_name ";
        $sql .= "FIELDS TERMINATED BY ','";
        return $sql;
    }

    private function truncateTableSQL($table_name){
        $sql = "TRUNCATE TABLE $table_name";
        return $sql;
    }

    private function moveInputFileToAppropriateDirectory($inputFileName, $successfully_processed, $company_code){
        $oldFilePath = $inputFileName;
        $inputFileBaseName = basename($inputFileName);
        if($successfully_processed === true){
            $newFilePath = $this->companySuccessfullyValidatedDirectory($company_code) . $inputFileBaseName;
        }else{
            $newFilePath = $this->companyUnsuccessfulValidationDirectory($company_code) . $inputFileBaseName;
        }
        rename($oldFilePath, $newFilePath);
    }

    public function uploadFilesToCompanyPendingFolder($files){
        foreach($files['attachments']['tmp_name'] as $key => $tmp_name) {
            if($files['attachments']['error'][$key] == 0) {
                move_uploaded_file($files['attachments']['tmp_name'][$key], $this->companyPendingDirectory($_SESSION['cc']) . $files['attachments']['name'][$key]);
            }
        }

        return array('info', $files['attachments']['name'][0]);
    }
    public function uploadManualFileToCompanyPendingFolder($files,$type=""){
    	$new_file_name = false;
    	$original_file_name = false;
        foreach($files['attachments']['tmp_name'] as $key => $tmp_name) {
            if($files['attachments']['error'][$key] == 0) {
            	$original_file_name = $files['attachments']['name'][$key];
            	$f = explode(".",$original_file_name);
            	$new_file_name = $_SESSION['cc']."_manual_finance_".(strlen($type)>0?$type."_":"").date("YmdHis").".".$f[1];
                move_uploaded_file($files['attachments']['tmp_name'][$key], $this->companyPendingDirectory($_SESSION['cc']) . $new_file_name);
                break;	//only process 1 file so break loop immediately
            }
        }

        return array('info', $new_file_name,$original_file_name);
    }

}