<?php

	function arrayToString($a,$i) {
		$s = array();
		$i++;
		foreach($a as $k=>$v) {
			if(is_array($v)) {
				$v = arrayToString($v, $i);
			}
			$s[] = $k."=".$v."";
		}
		return "<ul><li>{".implode("; ",$s)."}</li></ul>";
	}
if(is_array($_REQUEST) && count($_REQUEST)>0) {
//$d = serialize($_REQUEST);
		$sqlerror = "<html><body>
		<h1>---ERROR DETAILS BEGIN---</h1>";
		$sqlerror.="<h2>--- REQUEST DETAILS ---</h2><ul>";
		foreach($_REQUEST as $r => $v) {
			if(is_array($v)) {
				$sqlerror.="<li>".$r." => ".arrayToString($v, 0)."</li>";
			} else {
				$sqlerror.="<li>".$r." => ".$v."</li>";					
			}
		}
		$sqlerror.="</ul><p>".serialize($_REQUEST);
		$sqlerror.="<hr /><h2>--- SESSION DETAILS ---</h2><ul>";
		foreach($_SESSION as $s => $v) {
			if(is_array($v)) {
				$sqlerror.="<li>".arrayToString($v, 0)."</li>";
			} else {
				$sqlerror.="<li>".$v."</li>";					
			}
		}
		$sqlerror.="</ul><p>".serialize($_SESSION);
		$sqlerror.="<hr /><h2>--- OTHER DETAILS ---</h2><ul>";
			$sqlerror.="<p>HTTP USER AGENT :: ".$_SERVER['HTTP_USER_AGENT'];
		foreach($_SERVER as $r => $v) {
			if(is_array($v)) {
				$sqlerror.="<li>".arrayToString($v, 0)."</li>";
			} else {
				$sqlerror.="<li>".$v."</li>";					
			}
		}
		$sqlerror.="</ul><p>".serialize($_SERVER);
		$sqlerror.="<hr /><h1>--- END ---</h1></body></html>";
		$src_path = explode("/",$_SERVER["REQUEST_URI"]);
		$path = ""; 
		if(count($src_path)<=2) { 
		} else { 
			$c = count($src_path)-2;
			for($i=1;$i<=$c;$i++) { $path.="../"; }
		}
		$now = date("Ymd_His");
		$error_code = base64_encode($now."_ajax");
		$path .= "logs/".$now."_ajax_error_log_".$error_code.".html";
	    $file = fopen($path,"w");
       	fwrite($file,$sqlerror."\n");
	    fclose($file);
		
		
		echo json_encode(array("error","Unfortunately something went wrong and an error has occurred.  To follow up, please contact the Helpdesk or your Business Partner with the following error code: ".$error_code));
} else {
		echo json_encode(array("error","Your session has expired.  Please login again."));
}
?>