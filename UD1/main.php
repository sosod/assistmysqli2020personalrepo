<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function claimDep(id) {
    document.location.href = "main_claim.php?i="+id;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Claim a deposit</b></h1>
<p>&nbsp;</p>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_deposit, assist_".$cmpcode."_".$modref."_list_bank WHERE udbankid = bankid AND udclaimyn = 'N' ORDER BY udsort ASC";
    include("inc_db_con.php");
    $r = mysql_num_rows($rs);
    if($r == 0)
    {
        echo("<p>There are no unclaimed deposits.&nbsp;</p>");
    }
    else
    {
?>
<table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Date<br>Deposited</td>
        <td class=tdheader>Bank</td>
        <td class=tdheader>Receipt</td>
        <td class=tdheader>Bank<br>Statement</td>
        <td class=tdheader>Narration</td>
        <td class=tdheader>Amount</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
    <?php
    while($row = mysql_fetch_array($rs))
    {
        ?>
        <tr>
            <td class=tdgeneral><?php echo(date("d M Y",$row['uddepdate'])); ?>&nbsp;</td>
            <td class=tdgeneral><?php echo($row['banktext']." - ".$row['banktype']); ?>&nbsp;&nbsp;</td>
            <td class=tdgeneral align=center><?php echo($row['udreceipt']); ?></td>
            <td class=tdgeneral align=center><?php echo($row['udbankstat']); ?></td>
            <td class=tdgeneral><?php echo($row['udnarration']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral align=right>&nbsp;&nbsp;R <?php echo(number_format($row['udamount'],2)); ?>&nbsp;</td>
            <td class=tdheader>&nbsp;<input type=button value="Claim " onclick="claimDep(<?php echo($row['udid']); ?>)">&nbsp;</td>
        </tr>
        <?php
    }
    ?>
</table>
<?php
}
mysql_close();
?>

</body>

</html>
