<?php
    include("inc_ignite.php");

$cateid = $_GET['c'];
$catetitle = $_GET["catetitle"];
$catematkid = $_GET["catematkid"];

if(strlen($catetitle) > 0 && strlen($catematkid) > 0)  //IF DATA HAS BEEN SUBMITTED VIA THE FORM
{
    $catetitle = htmlentities($catetitle,ENT_QUOTES,"ISO-8859-1");
    //CREATE NEW CATEGORY RECORD
    $sql = "UPDATE assist_".$cmpcode."_".$moduledb."_categories SET ";
    $sql.= "catetitle = '".$catetitle."', ";
    $sql.= "catematkid = '".$catematkid."', ";
    $sql.= "cateyn = 'Y' ";
    $sql.= "WHERE cateid = ".$cateid;
    include("inc_db_con.php");
        //LOG THE NEW CATEGORY
        $tsql = $sql;
        //$tref = "MN";
        $trans = "Updated category: ".$catetitle;
        include("inc_transaction_log.php");
    $sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_list_users WHERE cateid = ".$cateid." AND tkid = '".$catematkid."' AND yn = 'Y'";
    include("inc_db_con.php");
        $u = mysql_num_rows($rs);
    mysql_close();
    if($u == 0)
    {
        $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_list_users SET tkid = '".$tkid."', cateid = ".$cateid.", yn = 'Y'";
        include("inc_db_con.php");
            //LOG THE NEW CATEGORY
            $tsql = $sql;
            //$tref = "MN";
            $trans = "Added user view access to updated category ".$cateid." due to change in administrator.";
            include("inc_transaction_log.php");
    }
    $editresult = "Y";
}
else
{
    $editresult = "N";
}

    include("inc_admin.php");
    
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {

    var ct = me.catetitle.value;
    var ma = me.catematkid.value;
    
    if(ct.length == 0)
    {
        alert("Please enter a category title.");
    }
    else
    {
        if(ma == "X")
        {
            alert("Please select the administrator for this category.");
        }
        else
        {
            return true;
        }
    }
    return false;
}
</script>

<link rel="stylesheet" href="/assist.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Setup - Categories</b></h1>
<form name=editcate action=setup_categories_edit.php method=get onsubmit="return Validate(this);">
<input type=hidden name=c value=<?php echo($cateid); ?>>
<input type=hidden name=r value=<?php echo($editresult); ?>>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cateid." ORDER BY catetitle";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r > 0)
{
    $ccount = 0;
    while($row = mysql_fetch_array($rs))
    {
        $caterow[$ccount] = $row;
        $ccount++;
    }
}
mysql_close();

$ccount = 0;
if($r > 0)
{
    foreach($caterow as $cate)
    {
?>
<table border=1 cellpadding=3 cellspacing=0 width=450>
    <tr>
        <td class=tdheaderl width=155 height=27>Reference:</td>
        <td class=tdgeneral><?php echo($cate['cateid']); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>Original Category:</td>
        <td class=tdgeneral><?php echo($cate['catetitle']); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>New Category:</td>
        <td class=tdgeneral><input type=text name=catetitle size=40 maxlength=40 value="<?php echo($cate['catetitle']); ?>"></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>Original Administrator:</td>
        <td class=tdgeneral><?php
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t WHERE t.tkid = '".$cate['catematkid']."' ORDER BY t.tkname, t.tksurname";
                include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    $tk = $row['tkname']." ".$row['tksurname'];
                    echo($tk);
                mysql_close();

          ?></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27>New Administrator:</td>
        <td class=tdgeneral><select name=catematkid><?php
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u WHERE t.tkstatus = 1 AND t.tkid <> '0000' AND u.usrtkid = t.tkid AND u.usrmodref = '".$tref."' ORDER BY t.tkname, t.tksurname";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $tk = $row['tkname']." ".$row['tksurname'];
                    if($id == $cate['catematkid'])
                    {
                        echo("<option selected value=".$id.">".$tk."</option>");
                    }
                    else
                    {
                        echo("<option value=".$id.">".$tk."</option>");
                    }
                }
                mysql_close();

          ?></select></td>
    </tr>
    <tr>
        <td class=tdgeneral height=27 colspan=2 align=center><input type=submit value=Edit style="padding: 0 6 0 6;"> <input type=reset style="padding: 0 2 0 2;"></td>
    </tr>
</table>
<?php
    }
}
?>
</form>

<?php
$helpfile = "../help/".$tref."_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
<script language=JavaScript>
    var dr = document.editcate.r.value;
    if(dr == "Y")
    {
        document.location.href = "setup_categories.php";
    }
</script>
</body>

</html>
