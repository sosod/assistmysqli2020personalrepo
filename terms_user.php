<?php 
require 'inc_session.php'; 

$site_name = ((isset($_SESSION['DISPLAY_INFO']['ignite_name']) && strlen($_SESSION['DISPLAY_INFO']['ignite_name'])>0) ? ($_SESSION['DISPLAY_INFO']['ignite_name']) : "Assist");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate() {
    if(document.terms.terms.checked)
    {
        return true;
    }
    else
    {
        alert("You must agree to our terms and conditions before you can proceed.");
        return false;
    }
}
</script>
<link rel="stylesheet" href="assist.css" type="text/css">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1><?php echo $site_name; ?> Terms and Conditions</h1>
<p>Before proceeding further, please read our terms and conditions.</p>
<h2>1. Definitions</h2>
<table border="1" width="100%" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class=tdgeneral><b>Company:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means Action iT (Pty) Ltd and includes the subscription
offering on the Website;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Customer:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means you the subscriber and includes the organisation that the
subscriber represents;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Customer Database:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>database created for the Customer on which Customer data is
stored;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Administrator:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the person that the Customer appoints for taking on the
responsibility of managing the Customer User data and the selection of products
subscribed for on the Website. The Administrator is also an User and needs to
comply with the obligations for a User;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Agreement:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the agreement set out in this document together with any
Appendices to this document and referred to in the <?php echo $site_name; ?> Debit Order
Agreement;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Initial subscription:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the initial subscription of the products selected on
the Website;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Subsequent subscription:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the subsequent change in subscription of the
products on the Website;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Monthly Fee:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the monthly charge including VAT (Value Added Tax);</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Product:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the subscription products on the Website �assist.ignite4u.co.za�;
		</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>User:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the person whom has logged onto the Website and is using the
subscription service;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Username:</b></td>
		<td class=tdgeneral>means the username assigned to user of the Website;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Password:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the password that is assigned to the user to gain access to the Website;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Working hours:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means the hours of 08h30 to 16h30 on a Monday to Friday, in South
Africa, excluding official Public Holidays;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Effective date:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>the date of acceptance of this agreement by the Company;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Website:&nbsp;&nbsp; </b> </td>
		<td class=tdgeneral>means <?php echo $site_name; ?> available at http://assist.ignite4u.co.za</td>
	</tr>
</table>
<p>When the Customer subscribes to the Website, the Company will issue the Users
with a username, a customer code and a password. By using the username and
password to access the subscription service offered by the Company, the Customer
will be signifying their acceptance of these terms and conditions of use, which
is a binding agreement between the Company and the Customer. This agreement
constitutes the whole agreement between the parties. No variation, addition or
variation of this agreement or any waiver of any rights shall be of any force
unless reduced to writing and signed by both the parties.</p>

<h2>2. Intellectual Property Rights </h2>
<p>All rights not expressly granted are reserved to the Company. To obtain
permissions for the commercial use of any content on the Website, contact info@actionassist.co.za. All the content, trademarks and data on
this web site, including but not limited to, software, databases, text,
graphics, icons, hyperlinks, private information, designs and agreements, are
the property of or licensed to the Company and as such are protected from
infringement by local and international legislation and treaties. The Company
retains the right, title and interest in ownership of the copyright and all
other intellectual property rights in the product and the documentation. </p>

<h2>3. The Customer Database</h2>
<p>The Administrator is responsible for maintaining the confidentiality of the
Customers company data. The Administrator is also a User in his/her own right
and agrees to accept the responsibility for all activities that occur under his
/ her Username and Password. The User is responsible for maintaining the
confidentiality of his/her Username, Company code and Password and for
restricting access to their computer, and the User agrees to accept
responsibility for all activities that occur under their Username and Password.</p>

<h2>4. Company Obligations</h2>
<p>The Company shall use reasonable endeavours to support the product during the
term of this agreement by providing online, telephonic and e-mail support during
working hours. The Company shall use reasonable means to ensure the security of
data contained in the Customer Database. The Company, at its sole discretion,
reserves the right to update the versions of the products on the Website. The
Company will provide access to the number of users as identified on the signed
Debit Order Agreement form. 
</p>

<h2>5. Customer Obligations </h2>
<p>The Customer will assign an Administrator to deal with all correspondence,
communication, account queries, product selection, user activation and
maintenance on the Customer Database created for the Customer. The Customer
agrees not to give any unauthorised third parties access to the products on the
website who do not comply with the subscription requirements. 
</p>

<h2>
6. Administrator Obligations </h2><p>
The Administrator is responsible for the set-up of the Users on the Subscription
Service, the completion of Debit Orders when the number of subscription users
change, notification to the Company of any changes to the bank account details
in the Debit Order Details contained in this agreement. The Customer agrees to
pay to the Company an administration fee of R350 for each and every instance
that a debit order payment is rejected by the Customers bankers. The
Administrator will ensure that the product is suitable for the intended purpose.
The Company's preferred medium of correspondence and communication with the
Customer will be by means of e-mail. The Administrator is responsible for
supplying the Company with a valid e-mail address and notifying the Company of
any changes in this e-mail address from time to time. </p>

<h2>7. User Obligations </h2>
<p>All Users will not disclose their Username and Password to any other person for
any reason whatsoever and will maintain the confidentiality thereof. </p>

<h2>8. Invoicing and Payment</h2>
<p>The Customer shall pay to the Company all amounts due in terms of this agreement
on a monthly basis. The monthly fee payable by the Customer or any monthly
renewal will be paid by means of a debit order. The Customer, by virtue of their
signature to this agreement, agrees to payment by debit order, and agrees not to
cancel the debit order for the duration of the agreement. The Customer agrees to
comply with the Monthly Fees as identified on the Debit Order
Agreement. </p>
<p>The fees listed in this agreement exclude any fees payable in respect of
consulting services, non-online support and any other customisation. These will
be invoiced to the Customer based on time spent by the Company at the then
current rates of service and traveling on a time and material basis. </p>
<p>The Company shall be entitled to increase the monthly subscription fee and other
fees from time to time. Details of such increases will be published on the
Company's Website and the Customer will be notified in advance. </p>
<p>Failure by the Customer to pay any amounts due in terms of the Agreement on the
due date, shall entitle the Company, without prejudice to any other remedies, to
suspend the Customer�s access to the Website. The Customer agrees to receive tax
invoices and other documents in electronic form. </p>

<h2>9. Termination </h2>
<p>The Customer may terminate the agreement on a notice period of 30 days.</p>
<p>The Company may terminate this agreement summarily if (a) the customer breaches
any terms of this agreement and fails to remedy same within 15 (fifteen) days of
written notification; (b) the Customer fails to pay any amount due in terms of
this agreement on the due date; and /or (c) the Customer commits an act of
insolvency as defined in the Insolvency Act. In the event that this agreement is
terminated and the Customer wishes to enter into a new agreement, the Customer
will be charged such amounts as would be applicable to a new agreement. </p>

<h2>10. Privacy</h2>
<p>The Company may electronically collect, store and use Customer information and
agrees not to disclose any personal information with third parties.</p>

<h2>11. Comments, Communications and Other Content </h2>
<p>All comments, communications, ideas, and other content disclosed, submitted or
offered to the Company on or by this Website or otherwise disclosed, submitted
or offered in connection with the Users� use of this Website remain the property
of the Company. Such disclosure, submission or offer of any information shall
constitute an assignment to the Company of all worldwide rights, titles and
interests in all copyrights and other intellectual properties in the information
submitted. The Company will own exclusively all such rights, titles and
interests and shall not be limited in any way in its use, commercial or
otherwise of any information submitted. The Company is and shall be under no
obligation (a) to maintain any information submitted in confidence; (b) to pay
to the User any compensation for any information submitted; or (c) to respond to
any User comments. The User agrees that no comments submitted to the Website
will violate any right of any third party, including copyright, trademark,
privacy or other personal or proprietary right(s). </p>

<h2>12. Links to Other Websites</h2>
<p>The Website may provide, or third parties may provide, links to other Internet
sites or resources. Because the Company has no control over such sites and
resources, the User acknowledges and agrees that the Company is not responsible
for the availability of such external sites or resources, and does not endorse
and is not responsible or liable for any content, advertising, products, or
other materials on or available from such sites or resources. The Company will
not be responsible or liable, directly or indirectly, for any damage or loss
caused or alleged to be caused by or in connection with use of or reliance on
any such content, goods or services available on or through any such site or
resource. This Website may be linked to other websites that are not under the
control of and are not maintained by the Company. The Company provides these
links to the user only as a convenience. </p>

<h2>13. Disclaimer </h2>
<p>The User uses this Website at their own risk. This Website is provided by the
Company on an &quot;as is&quot; and &quot;as available&quot; basis. Unless otherwise explicitly
stated, the content on this website is provided &quot;as is&quot;, &quot;with all faults&quot;, and
is for commercial use only. The Company gives no guarantee that (a) the Website
will be uninterrupted, timely, secure, or error- free; (b) the results that may
be obtained from the use of the Website will be accurate or reliable; (c) the
products will meet your expectations; or (d) any errors in the Website
programming will be corrected. No advice or information, whether oral or
written, obtained by the User from the Company or from the Website will create
any warranty or condition not expressly stated in the terms. The Company�s
employees are not authorised to vary these terms. </p>

<h2>14. Jurisdiction</h2>
<p>This agreement shall be construed and interpreted in accordance with the laws of
South Africa and the phrases, words and clauses defined in the definitions shall
apply in the entire agreement. In the event of the Company instituting legal
proceedings against the Customer to recover amounts due to the Company or taking
any other legal steps arising out of this agreement, the Customer shall be
liable for legal costs on the scale as between attorney and own client and for
any costs incurred in the collection of the outstanding amount. </p>
<hr>
<form name=terms action=terms_user_process.php method=POST onsubmit="return Validate()" language=jscript>
<p><input type=checkbox value=Y name=terms> I've read and agree to the terms and conditions.</p>
<p><input type=submit value=Go class=isubmit></p>
</form>
<p>&nbsp;</p>

</body>

</html>
