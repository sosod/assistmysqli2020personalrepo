<?php
//error_reporting(-1);
include("module/autoloader.php");

$me = new ASSIST();
$me->displayPageHeader();

$new_manuals = array("SDBP5","COMPL");
$new_path = "manuals/";

$modules = $me->getMenuModules();

$oldmanuals = $me->getOldUserManuals();
$mrefs = $oldmanuals['ref'];
$mlocs = $oldmanuals['location'];
$docs = $oldmanuals['docs'];

$e = 0;	//count of user manuals displayed
?>
<h1>User Manuals</h1>
<div style='width: 50%'><ul>
<li>While every effort has been made to maintain the accuracy and scope of these manuals, some methods may differ slightly from those applied on your Assist system.</li>
<li>Screenshots in these manuals may differ slightly from those found in your web browser. Lists and data have been captured from a demonstration database and will differ from those in your organisation. Certain headings and text fields may differ or be absent from those in the manuals, according to the Setup steps taken by your Assist Administrator.</li>
<li>For technical assistance, please contact your system administrator or click the Business Partner link on the Assist login page.</li>
</ul></div>
<table width=500 class=noborder style='margin-bottom: 20px;'><tr><td class=noborder>
<?php
foreach($modules as $mod) {
	$echo = "";
	if(in_array($mod['modlocation'],$new_manuals)) {
		$me->setSessionDetails($mod['modref'],$mod);
		$modloc = $mod['modlocation'];
		
		$manObj = new $modloc();
		$my_manuals = $manObj->getUserManuals($mod['modref']);
		foreach($my_manuals as $m) {
			//echo "<P>".$new_path.$m['link'];
			if(file_exists($new_path.$m['link'])) {
				$fs = filesize($new_path.$m['link']);
				$echo.= "
				<tr>
					<td><span class=b>".$m['text']."</span> <span class=size>(".$me->formatBytes($fs,1).")</span></td>
					<td class=center>".date("d F Y",$m['date'])."</td>
					<td class=center><input type=button value=Open link='".$new_path.$m['link']."' doc='".$m['name']."' /></td>
				</tr>";
			}
		}
	} else {
		$dl = array();
		$mr = $mod['modref'];
		$ml = $mod['modlocation'];
		$mra = isset($mrefs[$mr]) ? $mrefs[$mr] : array();
		$mla = isset($mlocs[$ml]) ? $mlocs[$ml] : array();
		$mi = array_unique(array_merge($mra,$mla));
		if(count($mi)>0) {
			foreach($mi as $i) {
				$d = $docs[$i];
				if(file_exists($d['doclink']) && !in_array($d['doclink'],$dl)) {
					$dl[] = $d['doclink'];
					$fs = filesize($d['doclink']);
					$t = explode("<br />",$d['doctitle']);
					$dt = implode("</span><br />",$t);
					$dn = explode("/",$d['doclink']);
					$name = $dn[count($dn)-1];
						$echo.= "
						<tr>
							<td><span class=b>".$dt."</span> <span class=size>(".$me->formatBytes($fs,1).")</span></td>
							<td class=center>".date("d F Y",$d['docdate'])."</td>
							<td class=center><input type=button value=Open link='".$d['doclink']."' doc='$name' /></td>
						</tr>";
				}
			}
		}
	}
	if(strlen($echo)>0) {
		$e++;
		echo "<h2>".$mod['modtext']."</h2>";
		echo "
		<table width=100%>
			<tr>
				<th>Title</th>
				<th width=120>Published</th>
				<th width=60></th>
			</tr>".$echo."</table>";
	}
}
if($e==0) {
	echo "<p>No User Manuals are currently available.</p>";
}
?>
</td></tr></table>
<?php
if($me->isAdminUser() ) {
	echo "
	<hr width=75% color=#000099 />
	<h1>Administrator Manuals</h1>
	<table width=500 class=noborder style='margin-bottom: 20px;'><tr><td class=noborder>
	";
	$menu = $me->getAdminSystemMenu();
	$modules = array();
	foreach($menu as $i=>$m) {
		$modules[$i] = array(
			'modref'=>$i,
			'modlocation'=>$i,
			'modtext'=>$m
		);
	}


	foreach($modules as $mod) {
		$echo = "";
		if(in_array($mod['modlocation'],$new_manuals)) {
			/*$me->setSessionDetails($mod['modref'],$mod);
			$modloc = $mod['modlocation'];
			
			$manObj = new $modloc();
			$my_manuals = $manObj->getUserManuals($mod['modref']);
			
			foreach($my_manuals as $m) {
				if(file_exists($new_path.$m['link'])) {
					$fs = filesize($new_path.$m['link']);
					$echo.= "
					<tr>
						<td><span class=b>".$m['text']."</span> <span class=size>(".$me->formatBytes($fs,1).")</span></td>
						<td class=center>".date("d F Y",$m['date'])."</td>
						<td class=center><input type=button value=Open link='".$new_path.$m['link']."' doc='".$m['name']."' /></td>
					</tr>";
				}
			}*/
		} else {
			$dl = array();
			$mr = $mod['modref'];
			$ml = $mod['modlocation'];
			$mra = isset($mrefs[$mr]) ? $mrefs[$mr] : array();
			$mla = isset($mlocs[$ml]) ? $mlocs[$ml] : array();
			$mi = array_unique(array_merge($mra,$mla));
			if(count($mi)>0) {
				foreach($mi as $i) {
					$d = $docs[$i];
					if(file_exists($d['doclink']) && !in_array($d['doclink'],$dl)) {
						$dl[] = $d['doclink'];
						$fs = filesize($d['doclink']);
						$t = explode("<br />",$d['doctitle']);
						$dt = implode("</span><br />",$t);
						$dn = explode("/",$d['doclink']);
						$name = $dn[count($dn)-1];
							$echo.= "
							<tr>
								<td><span class=b>".$dt."</span> <span class=size>(".$me->formatBytes($fs,1).")</span></td>
								<td class=center>".date("d F Y",$d['docdate'])."</td>
								<td class=center><input type=button value=Open link='".$d['doclink']."' doc='$name' /></td>
							</tr>";
					}
				}
			}
		}
		if(strlen($echo)>0) {
			$e++;
			echo "<h2>".$mod['modtext']."</h2>";
			echo "
			<table width=100%>
				<tr>
					<th>Title</th>
					<th width=120>Published</th>
					<th width=60></th>
				</tr>".$echo."</table>";
		}
	}
	if($e==0) {
		echo "<p>No User Manuals are currently available.</p>";
	}	
	echo "</td></tr></table>";
}
?>
<style type=text/css>
//.noborder { border: 1px dashed #009900; }
.size { font-style: italic; font-size: 6.5pt; }
</style>
<script type=text/javascript>
$(function() {
	$("input:button").click(function() {
		var url = $(this).attr("link");
		var doc = $(this).attr("doc");
		document.location.href = "library/controller/download.php?p="+url+"&f="+doc;
	});
});
</script>