<?php
    include("inc_ignite.php");

if($setuptopleveltype=="TBL") {
	$sql = "SHOW TABLES LIKE '".$dbref."_toplevel'";
	include("inc_db_con.php");
		$tlmnr = mysql_num_rows($rs);
	mysql_close($con);
	if($tlmnr>0) {
		$subarr = array();
		$sql = "SELECT DISTINCT dirid, dirtxt FROM ".$dbref."_toplevel, ".$dbref."_dir WHERE topyn != 'C' AND diryn = 'Y' AND dirid = topsubid ORDER BY dirsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				$sval = decode($row['dirtxt']);
				if(strlen($sval)>30) { 
					$sval = substr($sval,0,29)."..."; 
					$row['dirtxt'] = code($sval);
				}
				$subarr[] = $row;
			}
		mysql_close($con);

		$sql = "SELECT topid FROM ".$dbref."_toplevel LIMIT 1";
		include("inc_db_con.php");
			$tl = mysql_fetch_array($rs);
		mysql_close($con);
		$pyp = "N";
		if(checkIntRef($tl['topid'])) {
			$sql = "DESCRIBE ".$dbref."_toplevel";
			include("inc_db_con.php");
				while($row = mysql_fetch_array($rs)) {
					if($row['Field']=="toppastyear") { $pyp = "Y"; }
				}
			mysql_close($con);
			if($pyp == "Y") {
				$sql = "SELECT count(toppastyear) as pyp FROM ".$dbref."_toplevel WHERE toppastyear <> ''";
				include("inc_db_con.php");
					$row = mysql_fetch_array($rs);
					if($row['pyp'] > 0) { $pyp = "Y"; } else { $pyp = "N"; }
				mysql_close($con);
			}
		}
	}
}
	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ababab;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ababab; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    font-weight: normal;
}

.line1 { border-bottom: 1px solid #ababab; padding-left: 10px; }
.line2 { border-bottom: 1px solid #ababab; font-style: italic; padding-left: 30px; }
<?php
if(strFn("strpos",$_SERVER['HTTP_USER_AGENT'],"Firefox","")) {
	echo ".float { float: right; margin-right: 10px; }";
} else {
	echo ".float { float: right; margin-top: -17px; margin-right: 10px; }";
} ?>
</style>
<script type=text/javascript>
function autoTL() {
	if(confirm("This will update all Top Level KPIs from the Departmental SDBIP.\n\nAre you sure you wish to continue?\n\nNote: This update will happen automatically at midnight."))
		document.location.href = 'admin_tl_auto.php?act=preview';
}
function adminKPI() {
    var d = document.getElementById('kpidirid').value;
    if(!isNaN(parseInt(d)))
    {
        document.location.href = "admin_dskpi.php?d="+d;
    }
    else
    {
        alert("Please select which Directorate you wish to administer.");
    }
}
function updateTop() {
    var d = document.getElementById('tsid').value;
    if(!isNaN(parseInt(d)))
    {
        document.location.href = "admin_tl_update_list.php?s="+d;
    }
    else
    {
        alert("Please select which Sub-Directorate you wish to update.");
    }
}
function editTop(type) {
	if(type=="link") {
		var d = document.getElementById('etsid').value;
		var url = "admin_tl_edit_list.php?s="+d;
	} else {
		var d = document.getElementById('tl_edit2').value;
		var url = "admin_tl_edit2_list.php?s="+d;
	}
	if(!isNaN(parseInt(d)))
		document.location.href = url;
	else
		alert("Please select which Sub-Directorate you wish to update.");
}
</script>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Support</h1>
<?php
if($tkuser!="support" || $tkname != "Ignite Support")
	die("<P>You are not authorised to view this page.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");

?>
<p>&nbsp;</p>
<table cellpadding=5 cellspacing=0 width=600>
        <tr>
            <td class=tdheaderl height=30>Top Level</td>
        </tr>

	<?php 
	$is = 0;
	if(!checkIntRef($tl['topid'])) { 
		$is++;
?>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Import Top Level
			<input type=button value="   Go   " onclick="document.location.href = 'admin_tl_import.php';" id=t6 class=float>
		</td>
	</tr>
    	<?php
	}	//checkintref topid
	elseif($pyp == "N") 
	{
		$is++;
?>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Amend Top Level: New Columns
			<input type=button value="   Go   " onclick="document.location.href = 'admin_tl_amend.php';" id=t8 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Add Top Level [Import]
			<input type=button value="   Go   " onclick="document.location.href = 'admin_tl_add_import.php';" id=t9 class=float>
		</td>
	</tr>
	<?php } ?>
        <tr>
            <td class=tdheaderl height=30>Financials</td>
        </tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Replace Monthly Cashflow
			<input type=button value="   Go   " onclick="document.location.href = 'admin_cf_import.php';" id=t10 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Replace Capital Projects
			<input type=button value="   Go   " onclick="document.location.href = 'admin_cp_import.php';" id=t11 class=float>
		</td>
	</tr>
        <tr>
            <td class=tdheaderl height=30>Support</td>
        </tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Time Periods
			<input type=button value="   Go   " onclick="document.location.href = 'support_time_list.php';" id=t12 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Calculation Type Review
			<input type=button value="   Go   " onclick="document.location.href = 'support_calctype.php';" id=t12 class=float>
		</td>
	</tr>
	<!--
	<?php include("inc_tr.php"); ?>
		<td class=line1>Undo Delete
			<input type=button value="   Go   " onclick="document.location.href = 'admin_cf_import.php';" id=t10 class=float>
		</td>
	</tr> -->
        <tr>
            <td class=tdheaderl height=30>Other Reports</td>
        </tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Departmental SDBIP
			<input type=button value="   Go   " onclick="document.location.href = 'support_report_kpi.php';" id=t12 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Top Level SDBIP
			<input type=button value="   Go   " onclick="document.location.href = 'support_report_top.php';" id=t12 class=float>
		</td>
	</tr>
</table>
<p>&nbsp;</p>
</body>


</html>
