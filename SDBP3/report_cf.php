<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 1px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 1px solid #ababab;
    }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>
		<script type="text/javascript">
function allTime() {
    var fld;
    var targ;
    var act = document.getElementById('ta').value;
    //alert(act);
    for(t=1;t<13;t++)
    {
        fld = "t"+t;
        targ = document.getElementById(fld);
        if(act=="Deselect All")
        {
            targ.checked = false;
        }
        else
        {
            targ.checked = true;
        }
    }
    if(act=="Deselect All")
    {
        document.getElementById('ta').value = "Select All";
    }
    else
    {
        document.getElementById('ta').value = "Deselect All";
    }
}
function dirSub(me) {
        var dir = me.value;
        //alert(dir);
        if(dir.length>0 && !isNaN(parseInt(dir)))
        {
            var sub = document.getElementById('sf');
            sub.length = 0;
            var sf = subs[dir];
            var o = 1;
            var ds = false;
            var opt = new Array();
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,true);
            for(s in sf)
            {
                sub.options[o] = new Option(sf[s][1],sf[s][0],ds,ds);
                o++;
            }
        }
        else
        {
            var sub = document.getElementById('sf');
            sub.length = 0;
            var sf;
            var o = 1;
            var ds = false;
            var opt= new Array();
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,true);
            for(d in subs)
            {
                sf = subs[d];
                for(s in sf)
                {
                    sub.options[o] = new Option(sf[s][1],sf[s][0],ds,ds);
                    o++;
                }
            }
        }
}
</script>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - Monthly Cashflow</b></h1>
<form name=cpreport method=post action=report_cf_process.php>
<h3 class=fc>1. Select the columns you want in your report:</h3>
<div style="margin-left: 17px">
<table cellpadding="3" cellspacing="0" width=700>
    <tr>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0  style="border-width: 0px">
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name=revenue value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Revenue</td>
            	</tr>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name=opex value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Operational Expenditure</td>
            	</tr>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name=capex value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle>Capital Expenditure</td>
            	</tr>
            </table>
        </td>
        <td valign=top width=50% style="border-bottom: 0px">
            <table cellpadding=3 cellspacing=0  style="border-width: 0px">
<?php
$head = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CF' AND headyn = 'Y' AND headdetailyn = 'Y' AND headdetailsort>0 ORDER BY headdetailsort";
include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $m2 = ($mnr) / 2;
    $m = round($m2) + 1;
    if($m < $m2) { $m++; }
    $r=1;
    while($row = mysql_fetch_array($rs))
    {
        $head[$row['headfield']] = $row;
        ?>
            	<tr>
            		<td style="border-bottom: 0px" class="tdgeneral" align=center><input type="checkbox" checked name="<?php echo($row['headfield']); ?>" value=Y></td>
            		<td style="border-bottom: 0px" class="tdgeneral"><img src="/pics/blank.gif" width=1 height=25 align=absmiddle><?php echo($row['headdetail']); ?></td>
            	</tr>
        <?php
    }
mysql_close();

$timearr = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $tactive = 0;
    while($row = mysql_fetch_array($rs))
    {
        $timearr[$row['sort']]['id'] = $row['id'];
        $timearr[$row['sort']]['val'] = date("M-y",$row['eval']);
        $timearr[$row['sort']]['active'] = $row['active'];
        if($row['active']=="N") { $tactive++; }
    }
mysql_close();
?>
            </table>
        </td>
    </tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td class="tdgeneral" valign=top width=150><b>Directorate:</b></td>
		<td class="tdgeneral" valign=top width=550><select name=dirfilter id=df  onchange="dirSub(this);">
            <option selected value=ALL>All Directorates</option>
            <?php
            $js = "";
            $sql = "SELECT DISTINCT d.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
            $sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= ", assist_".$cmpcode."_".$modref."_finance_lineitems l ";
            $sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND l.lineyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = l.linesubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option value=".$row['dirid'].">".$row['dirtxt']."</option>");
                    $js.= "subs[".$row['dirid']."] = new Array();".chr(10);
                }
            mysql_close();
            ?>
        </select></td>
	</tr>
	<tr>
		<td class="tdgeneral" valign=top><b>Sub-Directorate:</b></td>
		<td class="tdgeneral" valign=top><select name=subfilter[] id=sf size=6 multiple>
            <option selected value=ALL>All Sub-Directorates</option>
            <?php
            $sql = "SELECT DISTINCT s.*, d.dirtxt FROM assist_".$cmpcode."_".$modref."_dirsub s";
            $sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
            $sql.= ", assist_".$cmpcode."_".$modref."_finance_lineitems l ";
            $sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND l.lineyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = l.linesubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $did2 = $row['subdirid'];
                    $sort2 = $row['subsort'];
                    $id2 = $row['subid'];
                    $val2 = $row['subtxt'];
                    echo("<option value=".$row['subid'].">".$row['subtxt']." (".$row['dirtxt'].")</option>");
                    $js.= "subs[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                }
            mysql_close();
            ?>
        </select><br><i><small>Use CTRL key to select multiple options</small></i></td>
	</tr>
	<?php
    if(count($timearr)==12)
    {
    ?>
	<tr>
		<td class="tdgeneral" valign=top><b>Time Period:</b><br><input type=button value=" Select All " onclick="allTime();" id=ta></td>
		<td class="tdgeneral" valign=top><table cellpadding=3 cellspacing=0 style="border: 0px solid #ffffff;">
            <?php
            for($t=1;$t<=12;$t+=2)
            {
                $t1 = array();
                $t1 = $timearr[$t];
                $t2 = array();
                $t2 = $timearr[$t+1];
                $ti = $t+6;
            ?>
            <tr>
		<td class=tdgeneral width=50% style="border: 0px solid #ffffff;">
			<input type=checkbox name=time[] value=<?php echo($t1['id']); ?> <?php if($t1['active']=="N" || ($t==1 && $tactive==0)) { echo("checked"); }  echo(" id=t$t "); ?>><?php echo($t1['val']); ?>
		</td>
		<td class=tdgeneral width=50% style="border: 0px solid #ffffff;">
			<input type=checkbox name=time[] value=<?php echo($t2['id']); ?> <?php if($t2['active']=="N") { echo("checked"); }  echo(" id=t$ti "); ?>><?php echo($t2['val']); ?>
		</td>
            </tr>
            <?php
            }
            ?>
        </table></td>
	</tr>
	<?php
    }
    ?>
</table>
</div>

<h3 class=fc>3. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" width=700>
	<tr>
		<td width=30 class=tdgeneral align=center><input type="radio" name="output" value="display" checked id=csvn></td>
		<td class="tdgeneral"><label for=csvn>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="output" value="csv" id=csvy></td>
		<td class=tdgeneral><label for=csvy>Microsoft Excel file</label></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Click the button:</h3>
<!-- <h3 class=fc>3. Click the button:</h3> -->
<!-- <input type=hidden name=output value=display> -->
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>
<script type=text/javascript>
var subs = new Array();
<?php echo($js); ?>
</script>

</body>

</html>
