<?php
    include("inc_ignite.php");
    
    include("inc_admin.php");
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function editPhase(i) {
    document.getElementById('ref').value = i;
    document.forms['edit'].submit();
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Project Life Cycle</b></h1>
<h2 class=fc>Setup Standard Phases</h2>
<p><input type=button value=" Add New " onclick="document.location.href = 'setup_plc_std_add.php';"> <input type=button value="Display Order" onclick="document.location.href = 'setup_plc_std_order.php';"></p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td width=30 class=tdheader height=27>Ref</td>
		<td class=tdheader>Phase</td>
		<td width=60 class=tdheader>Required?</td>
		<td width=50 class=tdheader>&nbsp;</td>
	</tr>
	<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_std WHERE (yn = 'Y' OR yn = 'R') ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($row['type']!="P" && $row['type']!="A") { $id = $row['id']; } else { $id = $row['type']; }
    ?>
	<?php include("inc_tr.php");?>
		<td class=tdgeneral align=center height=27><b><?php echo($id); ?></b></td>
		<td class=tdgeneral><?php echo($row['value']); ?></td>
		<td class=tdgeneral align=center><?php if($row['yn']=="R") { echo("Yes"); } else { echo("No"); } ?></td>
		<td class=tdgeneral align=center><?php if(is_numeric($id)) { ?><input type=button value=Edit onclick="editPhase(<?php echo($id); ?>)"><?php } else { echo("&nbsp;"); } ?></td>
	</tr>
    <?php
    }
mysql_close();
?>
</table>
<p><input type=button value=" Add New " onclick="document.location.href = 'setup_plc_std_add.php';"> <input type=button value="Display Order" onclick="document.location.href = 'setup_plc_std_order.php';"></p>
<form name=edit method=post action=setup_plc_std_edit.php>
<input type=hidden name=id id=ref>
</form>
<?php
$urlback = "setup_plc.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
