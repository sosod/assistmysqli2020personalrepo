<?php
    include("inc_ignite.php");
    
    $sadmin = $_POST['sadmin'];
    if(strlen($sadmin)>0)
    {
        $tk = 0;
        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$sadmin."' AND tkstatus = 1";
        include("inc_db_con.php");
            $tk = mysql_num_rows($rs);
        mysql_close();
        if($tk>0)
        {
            ?>
            <script type=text/javascript>parent.header.location.href = '/title_login.php?m=SDBP3';</script>
            <?php
            $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$sadmin."' WHERE ref = '".strtoupper($modref)."' AND refid = 0";
            include("inc_db_con.php");
        }
    }
    
    
    
    include("inc_admin.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function updateSDBIP() {
    var sa = document.getElementById('sa').value;
    document.getElementById('sadmin').value = sa;
    document.getElementById('sdbipupdate').submit();
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Administrators</b></h1>
<form name=sdbip method=post action=setup_admin.php id=sdbipupdate>
<input type=hidden value="" id=sadmin name=sadmin>
</form>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td width=120 class=tdheaderl>SDBIP</td>
		<td class=tdgeneral height=38><select name=sadmin id=sa>
            <option <?php if($setupadmin == "0000") { echo("selected"); } ?> value='0000'>Ignite Assist Administrator</option>
            <?php
            $sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_menu_modules_users u, assist_".$cmpcode."_timekeep t WHERE u.usrmodref = '".$modref."' AND u.usrtkid = t.tkid AND t.tkstatus = 1";
            $sql.= " ORDER BY t.tkname, t.tksurname";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    echo("<option ");
                    if($setupadmin == $row['tkid']) { echo("selected"); }
                    echo(" value='".$row['tkid']."'>".$row['tkname']." ".$row['tksurname']."</option>");
                }
            mysql_close();
        ?>
        </select>&nbsp;</td>
		<td width=100 class=tdgeneral align=center><input type=button value="  Update  " onclick="updateSDBIP();"></td>
	</tr>
	<tr>
		<td width=120 class=tdheaderl>Finance</td>
		<td class=tdgeneral>These are the users who can update financial information and add Capital Projects.&nbsp;</td>
		<td width=100 class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_admin_fin.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>(Sub-)Directorate</td>
		<td class=tdgeneral>These are users that can update KPIs and Capital Projects within a specified directorate or sub-directorate.&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_admin_dir.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>KPI</td>
		<td class=tdgeneral>These are users that can update KPIs and Capital Projects within any directorate or sub-directorate.&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_admin_kpi.php';"></td>
	</tr>
<!--	<tr>
		<td class=tdheaderl>Assurance Provider</td>
		<td class=tdgeneral>These are users that can audit the KPIs.&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_admin_ap.php';"></td>
	</tr> -->
	<tr>
		<td class=tdheaderl height=38 >Top Level</td>
		<td class=tdgeneral>These are users that can manage the Top Level KPIs.&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_admin_tl.php';"></td>
	</tr>
	<tr>
		<td class=tdheaderl>Report</td>
		<td class=tdgeneral height=38>Generate a report on Administrative access per user.&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value="    View    " onclick="document.location.href = 'setup_admin_report_user.php';"></td>
	</tr>
</table>


<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
