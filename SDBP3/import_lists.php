<?php
    include("inc_ignite.php");
	
$lists = array(
	'L_DIR'=>array('id'=>"L_DIR",'value'=>"Directorates"),
	'L_DIRSUB'=>array('id'=>"L_DIRSUB",'value'=>"Sub-Directorates"),
	'L_AREA'=>array('id'=>"L_AREA",'value'=>"Area"),
	'L_FSRC'=>array('id'=>"L_FSRC",'value'=>"Funding Source"),
	'L_GFS'=>array('id'=>"L_GFS",'value'=>"GFS Classification"),
	'L_KPA'=>array('id'=>"L_KPA",'value'=>"Municipal KPAs"),
	'L_PROG'=>array('id'=>"PROG",'value'=>"Program"),
	'L_WARDS'=>array('id'=>"L_WARDS",'value'=>"Wards")
); 

$t = $_REQUEST['t'];
$what = $lists[$t];
$act = $_REQUEST['act'];
$fileloc = $_REQUEST['fl'];

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import - Lists</b></h1>
<?php
if(strlen($what['id'])==0) {
	die("<P>An error has occurred - ".$_REQUEST['t'].".<p>"); print_r($what);
}

echo "<h2>".$t.": ".$what['value']."</h2>";

if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                die("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                die("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                die("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            die("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    die("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}

if($err != "Y")
{
            $file = fopen($fileloc,"r");
            $data = array();
	set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
//				echo "<P>".count($tmpdata)." - "; print_r($tmpdata);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
	set_time_limit(30);
} else {
	die("<P>Erryn = 'Y'"); 
}

if(count($data)>0)
{
	//print_r($data);
	echo "<form action=import_lists.php method=post><input type=hidden name=t value=".$_REQUEST['t']."><input type=hidden name=fl value=".$fileloc."><input type=hidden name=act value=sbmt>";
	echo "<table cellpadding=3>";
		$dt = $data[0];
		echo "<tr><td width=30>&nbsp;</td>";
		foreach($dt as $d) {
			echo "<th>".$d."</th>";
		}
		echo "</tr>";
		for($a=1;$a<count($data);$a++) {
			$dt = $data[$a];
			if($act=="sbmt") {
				switch($t) {
					case "L_DIR":
						$sql = "INSERT INTO ".$dbref."_dir (dirid, dirtxt, dirsort, diryn) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[1])."',".$dt[0].",'Y')";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
					case "L_DIRSUB":
						$sql = "INSERT INTO ".$dbref."_dirsub (subid, subtxt, subdirid, subhead, subsort, subyn) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[1])."',".$dt[2].",'".$dt[3]."',".$dt[0].",'Y')";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
					case "L_AREA":
						$sql = "INSERT INTO ".$dbref."_list_area (id, value, yn, code) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[1])."','Y','')";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
					case "L_FSRC":
						$sql = "INSERT INTO ".$dbref."_list_fundsource (id, value, yn, code) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[1])."','Y','')";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
					case "L_GFS":
						$sql = "INSERT INTO ".$dbref."_list_gfs (id, value, yn, code) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[1])."','Y','')";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
					case "L_KPA":
						$sql = "INSERT INTO ".$dbref."_list_munkpa (id, value, yn, code) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[1])."','Y','".$dt[2]."')";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
					case "L_WARDS":
						$sql = "INSERT INTO ".$dbref."_list_wards (id, value, yn, numval) VALUES ";
						$sql.= "(".$dt[0].",'".code($dt[2])."','Y',".$dt[1].")";
						include("inc_db_con.php");
							$i = mysql_insert_id($con);
						if($i == $dt[0]) { $class = "tdheadergreen"; } else { $class = "tdheaderred"; }
						break;
				}
			} elseif(!checkIntRef($dt[0])) {
				$class = "tdheaderred"; 
			} else {
				$class = "tdheaderblue"; 
			}
			echo "<tr>";
			echo "<td class=$class >&nbsp;</td>";
			foreach($dt as $d) {
				echo "<td>".$d."</td>";
			}
			echo "</tr>";
		}
	echo "</table>";
	if($act != "sbmt") {
		echo "<p><input type=submit></p>";
	}
	echo "</form>";
} else {
	echo "<P>NO DATA";
}

$urlback = "import.php";
include("inc_goback.php");

?>

</body>

</html>
