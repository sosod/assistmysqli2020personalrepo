<?php
include("inc_ignite.php");

	$administ = array();
	$topidarr = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."' AND type = 'TL'";
include("inc_db_con.php");
		$tladmin = mysql_num_rows($rs);
mysql_close($con);
if($tladmin ==0)
	die("<P>You are not authorised to view this page.</p>");

$act = $_REQUEST['act'];
$csql = array();
if($act=="SAVE") {
	//ALTER _toplevel => toppastyear
	$yn = 'N';
	$sql = "DESCRIBE ".$dbref."_toplevel";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs)) {
			if($row['Field']=="toppastyear") { $yn = 'Y'; }
		}
	mysql_close($con);
	if($yn !='Y') {
		$csql[] = "ALTER TABLE ".$dbref."_toplevel ADD COLUMN `toppastyear` VARCHAR(255) NOT NULL AFTER `topdriver`";
	}
	//ALTER _toplevel_result => trcorrective
	$yn = 'N';
	$sql = "DESCRIBE ".$dbref."_toplevel_result";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs)) {
			if($row['Field']=="trcorrective") { $yn = 'Y'; }
		}
	mysql_close($con);
	if($yn !='Y') {
		$csql[] = "ALTER TABLE ".$dbref."_toplevel_result ADD COLUMN `trcorrective` TEXT NOT NULL AFTER `trsdbipcomment`";
	}
	//UPDATE _headings => increase sort
	//INSERT INTO _headings
	$sql = "SELECT headid FROM ".$dbref."_headings WHERE headfield IN ('toppastyear','trcorrective') AND headyn = 'Y'";
	include("inc_db_con.php");
		$hmnr = mysql_num_rows($rs);
	mysql_close($con);
	if($hmnr==0) {
		$csql[] = "UPDATE ".$dbref."_headings SET headlistsort = headlistsort+1, headdetailsort = headdetailsort+1 WHERE headdetailsort > 12 AND headtype = 'TL'";
		$csql[] = "INSERT INTO ".$dbref."_headings VALUES (null,'TL','Y','Previous Year Actual Performance','Previous Year Actual Performance',13,'Y','Previous Year Actual','Previous Year Actual',13,'Y','N','toppastyear',150,'N',''), (null,'TL','Y','Corrective Measures','Corrective Measures',99,'Y','Corrective Measures','Corrective Measures',99,'Y','N','trcorrective',0,'N','')";
	}
	if(count($csql)>0) {
		foreach($csql as $sql) {
			include("inc_db_con.php");
		}
	}
}
//echo "<pre>"; print_r($csql); echo "</pre>";
?>
<html>
<head>
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Ignite4u.co.za</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdhead2 { background-color: #ccc; text-align:center;}
.done { background-color: #009900; }
.ok { background-color: #000099; }
.err { background-color: #CC0001; color: #FFFFFF;}
</style>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc>SDBIP <?php echo($modtxt); ?>: Admin ~ Top Level: Amend</h1>
<?php 
if($act == "SAVE")
	echo "<h2>Finalise Top Level Amendments</h2>";
else
	echo "<h2>Validate Top Level Amendments</h2>";

//UPLOAD FILE
if($act!="SAVE") {
	if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
		switch($_FILES["ifile"]["error"])
		{
			case 2:
				echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
				break;
			case 4:
				echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
				break;
			default:
				echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
				break;
		}
		die();
	} else { //IF ERROR WITH UPLOAD FILE
		$ext = substr($_FILES['ifile']['name'],-3,3);
		if(strtolower($ext)!="csv") {
			echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
			die();
		} else {
			$filename = "TL-IMPORT_".substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
			$fileloc = "../files/".$cmpcode."/".$filename;
			//UPLOAD UPLOADED FILE
			set_time_limit(180);
			copy($_FILES["ifile"]["tmp_name"], $fileloc);
		}
	}
} else {	//act = save
	$fileloc = $_REQUEST['fileloc'];
}	//act=save

if(!file_exists($fileloc)) {
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    die();
}

//SETUP VALIDATE VARIABLES
$top = array();
$sql = "SELECT topid, dirtxt, topvalue, topdriver FROM ".$dbref."_toplevel INNER JOIN ".$dbref."_dir ON dirid = topsubid WHERE topyn = 'Y' AND diryn = 'Y' ORDER BY dirsort";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	$top[$row['topid']] = $row;
}
mysql_close($con);


//GET DATA FROM FILE
set_time_limit(1800);
$file = fopen($fileloc,"r");
$data = array();
while(!feof($file)) {
	$tmpdata = fgetcsv($file);
	if(count($tmpdata)>1) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($file);
unset($data[0]);
unset($data[1]);
$head = array();
	$head[0] = array("Ignite ref","Directorate","KPI Name","Program Driver","","Past Year Performance","Jul-Sep Corrective Measures","Oct-Dec Corrective Measures","Jan-Mar Corrective Measures","Apr-Jun Corrective Measures");
	$head[1] = array("","","","","","","Text (150 characters)","Text","Text","Text","Text");
$cols = count($head[0]);
//CREATE FORM
?>
<form name=import id=imp action=admin_tl_amend_process.php method=post>
<input type=hidden name=act value=SAVE><input type=hidden name=fileloc value="<?php echo $fileloc; ?>">
<table cellpadding=3 cellspacing=0>
<?php
$r = 0;
	foreach($head as $d) {
		echo "<tr><td>&nbsp;</td>";
		switch($r) {
			case 0: 
				$class="tdheader"; 
				break;
			case 1: 
				$class="tdhead2"; 
				break;
			default:
				$class="";
				break;
		}
		for($c=0;$c<$cols;$c++) {
			$t = $d[$c];
			if($c != 4) { echo "<td class=$class nowrap>".$t."</td>"; } else { echo "<td class=tdhead2>&nbsp;</td>"; }
		}
		echo "</tr>";
		$r++;
	}
	$e = 0;
	$idarr = array();
	$idarr[] = 0;
	foreach($data as $d) {
		set_time_limit(1800);
		$class = ""; $class1 = "";
		$csql = array();
		$pyp = "";
		$correct = array();
		$err = "N";
		$c = 0;
		//VALIDATION
		$topid = $d[0];
		for($c=0;$c<$cols;$c++) {
			$t = trim($d[$c]);
			if($c==1 && $err == "Y") { break; }
			switch($c) {
				case 0:	//topid
					if(!checkIntRef($t) || !isset($top[$t])) {
						$err = "Y";
						$d[$c].= " -> Invalid Ref";
					}
					break;
				case 1:	//dir
					$d[$c].=" :: ".$top[$topid]['dirtxt'];
					if($err!="Y") {
						if(decode($top[$topid]['dirtxt']) != $t) {
							$err = "Y";
							$d[$c].= " -> No match";
						}
					}
					break;
				case 2:	//value
					$d[$c].=" :: ".$top[$topid]['topvalue'];
					if($err!="Y") {
						if(decode($top[$topid]['topvalue']) != $t) {
							$err = "Y";
							$d[$c].= " -> No match";
						}
					}
					break;
				case 3: //driver
					$d[$c].=" :: ".$top[$topid]['topdriver'];
					if($err!="Y") {
						if(decode($top[$topid]['topdriver']) != $t) {
							$err = "Y";
							$d[$c].= " -> No match";
						}
					}
					break;
				case 5: //pyp
					if(strlen(code($t))>200) {	
						$err = "Y";
						$d[$c].= " -> Too long";
					} else {
						$pyp = code($t);
					}
					break;
				case 6:
				case 7:
				case 8:
				case 9:
					$a = ($c-5)*3;
					$correct[$a] = code($t);
					break;
				default:
					break;
			}
		}	//for
		if($err == "Y") {
			$e++;
			$class = "err";
			$class1 = $class;
		} else {
			//CREATE CSQL
			$csql[] = "UPDATE ".$dbref."_toplevel SET toppastyear = '$pyp' WHERE topid = $topid";
			$logold = $csql[count($csql)-1];
			$csql[] = "INSERT INTO assist_".$cmpcode."_sdp10_toplevel_log (logtopid,logtkid,logdate,logfield,logold,lognew,logtype,logdocid,logyn) VALUES ($topid,'$tkid',$today,'toppastyear','".str_replace("'","\'",$logold)."','Amended Top Level KPI $topid','I',0,'N')";
			for($r=3;$r<=12;$r+=3) {
				$csql[] = "UPDATE assist_".$cmpcode."_sdp10_toplevel_result SET trcorrective = '".$correct[$r]."' WHERE trtopid = $topid AND trtimeid = $r";
				$logold = $csql[count($csql)-1];
				$csql[] = "INSERT INTO assist_".$cmpcode."_sdp10_toplevel_log (logtopid,logtkid,logdate,logfield,logold,lognew,logtype,logdocid,logyn) VALUES ($topid,'$tkid',$today,'trcorrective','".str_replace("'","\'",$logold)."','Amended Top Level KPI $topid','I',0,'N')";
			}
			if($act == "SAVE") {
				//RUN CSQL
				foreach($csql as $sql) {
					include("inc_db_con.php");
				}
				$class = "done";
				$class1 = "";
			} else {
				$class = "ok";
				$class1 = "";
			}
		}
		echo "<tr><td class=\"$class\">&nbsp;</td>";
		$c = 0;
		for($c=0;$c<$cols;$c++)
		{
			if($c != 4) { echo "<td class=\"$class1\">".$d[$c]."</td>"; } else { echo "<td class=tdhead2>&nbsp;</td>"; }
		}
		echo "</tr>";
		//ECHO SQL
		/*if($act != "SAVE") {
			echo "<tr><td>&nbsp;</td>";
			echo "<td colspan=$cols nowrap>";
			if($err == "N")
				arrPrint($csql);
			else
				echo "ERROR encountered.";
			echo "</td>";
			echo "</tr>";
		}*/
	}	//foreach $data
?>
</table>
<?php
if($act != "SAVE") {
	if($e>0) {
		echo "<P style=\"text-align:center;\">$e errors found.<br>Top Level KPIs cannot be amended.</p>";
	} else {
		echo "<p style=\"text-align:center;\"><input type=submit value=\"  Accept  \" style=\"color: #FFFFFF; background-color: #009900; padding: 10 10 10 10;\"></p>";
	}
}
?>
<p>&nbsp;</p>
</form>
</body></html>