<?php
    include("inc_ignite.php");
	$head = getHeadings("headtype = 'CP' AND headyn = 'Y' ORDER BY headdetailsort");
	$headr = getHeadings("headtype = 'CPR' AND headyn = 'Y' ORDER BY headdetailsort");
    $r = 1;

    $sql = "SELECT dirid id, dirtxt txt, dirsort sort, dirid sdid FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y'";;
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $dirs[$row['id']] = $row;
            $sql2 = "SELECT subid id, subtxt txt, subsort sort, subdirid sdid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$row['id'];
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $subs[$row['id']][$row2['id']] = $row2;
                }
            mysql_close($con2);
        }
    mysql_close($con);
	
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
	.tddir {
		background-color: #555555;
		color: #FFFFFF;
		padding: 3px 3px 3px 10px;
		font-weight: bold;
		height: 27px;
		border: 1px solid #FFFFFF;
	}
	.tdsub {
		background-color: #999999;
		color: #FFFFFF;
		padding: 3px 3px 3px 20px;
		font-weight: bold;
		height: 27px;
		border: 1px solid #FFFFFF;
	}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
?>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Capital Projects ~ Setup</b></h1>
<p><input type=button value="Add New" onclick="document.location.href = 'admin_fin_cp.php';"></p>
<table cellpadding=3 cellspacing=0 width=600>
        <tr>
            <th width=30 height=27>Ref</th>
            <th><?php echo $head['cpname']['headlist']; ?></th>
            <th><?php echo $head['cpproject']['headlist']; ?></th>
            <th><?php echo $headr['crbudget']['headlist']; ?></th>
            <th width=50>&nbsp;</th>
        </tr>
	<?php foreach($dirs as $d) { 
		echo "<tr><td colspan=5 class=tddir>".$d['txt']."</td></tr>";
		$sub = $subs[$d['id']];
		foreach($sub as $s) {
			echo "<tr><td colspan=5 class=tdsub>".$s['txt']."</td></tr>";
			$sql = "SELECT cpid, cpname, cpproject, SUM(crbudget) as budget FROM ".$dbref."_capital, ".$dbref."_capital_results WHERE cpyn = 'Y' AND cpsubid = ".$s['id']." AND cpid = crcpid GROUP BY cpid, cpname, cpproject";
			include("inc_db_con.php");
			if(mysql_num_rows($rs)>0) {
				$c = 0;
				while($row = mysql_fetch_array($rs))
				{
					echo "<tr>";
						echo "<th ";
						if($c == 0) { echo " style=\"border-top: 1px solid #ababab;\""; }
						echo ">".$row['cpid']."</th>";
						echo "<td>".$row['cpname']."</td>";
						echo "<td>".$row['cpproject']."</td>";
						echo "<td style=\"text-align:right;\">".number_format($row['budget'],2)."</td>";
						echo "<td style=\"text-align:center;\"><input type=button value=Edit onclick=\"document.location.href = 'admin_fin_cp.php?action=edit&c=".$row['cpid']."';\"></td>";
					echo "</tr>";
					$c++;
				}
			} else {
					echo "<tr>";
						echo "<td style=\"border-right-color: #FFFFFF;\">&nbsp;</td>";
						echo "<td colspan=3 style=\"font-style: italic;border-left-color: #ffffff;border-right-color: #ffffff;\">No Capital Projects found.</td>";
						echo "<td style=\"text-align:center;border-left-color: #FFFFFF\"><input type=button value=Add onclick=\"document.location.href='admin_fin_cp.php?subid=".$s['id']."';\"></td>";
					echo "</tr>";
			}
			mysql_close($con);
		} //foreach sub
	 } //foreach $dirs?>
</table>
<p><input type=button value="Add New" onclick="document.location.href = 'admin_fin_cp.php?action=add';"></p>
<?php
$urlback = "admin_fin.php";
include("inc_goback.php");
?>
</body>
</html>
