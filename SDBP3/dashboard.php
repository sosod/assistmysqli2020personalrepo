<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table td {
        border-width: 0px;
        border-bottom: 1px solid #bbbbbb;
    }
</style>
<script type=text/javascript>
function viewKPI(i) {
    if(i.length > 0)
    {
        document.getElementById('i').value = i;
        document.getElementById('t').value = "KPI";
        document.forms['dash'].submit();
    }
}
function viewFin(i) {
    if(i.length > 0)
    {
        document.getElementById('i').value = i;
        document.getElementById('t').value = "Fin";
        document.forms['dash'].submit();
    }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Dashboard</b></h1>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=500>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral><b>Entire Municipality</b></td>
        <td class=tdgeneral width=70><input type=button value="    KPI    " onclick="viewKPI('mun');"></td>
        <td class=tdgeneral width=70><input type=button value="Financial" onclick="viewFin('mun');"></td>
    </tr>
<?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
?>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral><?php echo($val); ?></td>
        <td class=tdgeneral width=70><input type=button value="    KPI    " onclick="viewKPI('<?php echo($id); ?>');"></td>
        <td class=tdgeneral width=70><input type=button value="Financial" onclick="viewFin('<?php echo($id); ?>');"></td>
    </tr>
<?php
        }
    mysql_close();
?>
</table>
<p style="font-size: 7pt;line-height: 9pt"><b>Note:</b> In order to print the Dashboard to PDF, you need to have a PDF printer installed on your computer.<br>Please contact your IT department or the Ignite Helpdesk for more information.</i></td>
<form name=dash id=dash method=post action=dashboard_view.php>
<input type=hidden name=id value="" id=i>
<input type=hidden name=t value="" id=t>
</form>

</body>

</html>
