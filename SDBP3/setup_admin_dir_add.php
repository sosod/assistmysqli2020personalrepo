<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var tkid = me.tkid.value;
    var dir = me.dir.value;
    if(tkid == "X")
    {
        alert("Please select the user you wish to add as a directorate administrator.");
        return false;
    }
    else
    {
        if(dir == "D")
        {
            dirid = me.dirid.value;
            me.dir.value = "D_"+dirid;
            return true;
        }
        else
        {
            if(dir=="S")
            {
                alert("Please select which Sub-Directorate you wish to give the user administrative access to.");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Directorate Administrators ~ Add</b></h1>
<p>&nbsp;</p>
<?php
$dirid = $_GET['d'];
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        $dir = mysql_fetch_array($rs);
    mysql_close();
?>
<form name=add method=post action=setup_admin_dir_add_process.php onsubmit="return Validate(this);">
<input type=hidden name=dirid value=<?php echo($dirid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=170>(Sub-)Directorate:</td>
		<td class=tdgeneral valign=top><select name=dir>
            <option value=D>--- DIRECTORATE ---</option>
            <option value=D_<?php echo($dirid); ?> selected><?php echo($dir['dirtxt']); ?></option>
            <option value=S>--- SUB-DIRECTORATE ---</option>
		  <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con.php");
                while($sub = mysql_fetch_array($rs))
                {
                    echo("<option value=S_".$sub['subid'].">".$sub['subtxt']."</option>");
                }
            mysql_close();
?>
        </select></td>
    </tr>
	<?php
    $sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_menu_modules_users u, assist_".$cmpcode."_timekeep t WHERE u.usrmodref = '".strtoupper($modref)."' AND u.usrtkid = t.tkid AND t.tkstatus = 1";
    $sql.= " ORDER BY t.tkname, t.tksurname";
    include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=2>No Users available.</td>
	</tr>
    <?php
    }
    else
    {
    ?>
    <tr>
		<td class=tdheaderl>User:</td>
		<td class=tdgeneral valign=top><select name=tkid>
            <option value=X selected>--- SELECT ---</option>
        <?php
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            echo("<option value=\"".$id."\">".$val."</option>");
        }
        ?>
        </select></td>
	</tr>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral valign=top><input type=submit value=" Add "></td>
    </tr>
    <?php
    }
    mysql_close();
    ?>
</table>
</form>
<?php
$urlback = "setup_admin_dir_config.php?d=".$dirid;
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
