<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>


<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Revenue By Source ~ Edit Line Item</b></h1>
<p>&nbsp;</p>
<?php
$lineid = $_POST['lineid'];
$val = htmlentities($_POST['val'],ENT_QUOTES,"ISO-8859-1");
$linemnr = 0;
if(is_numeric($lineid) && $lineid > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_revbysource WHERE rsid = ".$lineid." AND rsyn = 'Y'";
    include("inc_db_con.php");
        $linemnr = mysql_num_rows($rs);
    mysql_close();

}

if($linemnr > 0)
{
    if(strlen($val)>0)
    {
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_finance_revbysource SET rsline = '".$val."' WHERE rsid = ".$lineid;
        include("inc_db_con.php");
        echo("<p>Line item '".$val."' successfully edited.</p>");
        $urlback = "admin_fin_rs_setup.php";
        include("inc_goback.php");
$tsql = $sql;
$told = "";
$trans = "Updated RevBySource line ".$lineid;
include("inc_transaction_log.php");
    }
    else
    {
        echo("<p>An error has occurred.  Please go back and try again.</p>");
        $urlback = "admin_fin_rs_setup.php";
        include("inc_goback.php");
    }
}
else
{
    echo("<p>An error has occurred.  Please go back and try again.</p>");
    $urlback = "admin_fin_rs_setup.php";
    include("inc_goback.php");
}

?>
<p>&nbsp;</p>
</body>
</html>
