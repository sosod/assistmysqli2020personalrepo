<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function delAdmin(d,dtxt) {
    var err = "N";
    if(!isNaN(parseInt(d)) && escape(d) == d)
    {
        d = parseInt(d);
        if(d>0)
        {
            if(confirm("Are you sure you wish to delete KPI administrator '"+dtxt+"'?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
            {
                document.location.href = "setup_admin_kpi_del.php?a="+d;
            }
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }

    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheader { border: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - KPI Administrators</b></h1>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td class=tdheader width=30>Ref</td>
		<td class=tdheader>User</td>
		<td class=tdheader width=80>&nbsp;</td>
	</tr>
	<?php
    $sql = "SELECT a.id, t.tkname, t.tksurname FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_timekeep t WHERE a.yn = 'Y' AND a.type = 'KPI' AND a.tkid = t.tkid AND t.tkstatus = 1";
    include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=4>No Administrators available.  Please click "Add New" to add a KPI Administrator.</td>
	</tr>
    <?php
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['id'];
            $val = $row['tkname']." ".$row['tksurname'];
            $val2 = str_replace("'","\'",$val);
            include("inc_tr.php");
    ?>

		<td class=tdheader valign=top><?php echo($id); ?></td>
		<td class=tdgeneral valign=top style="padding-left: 10px"><?php echo($val); ?></td>
		<td class=tdgeneral align=center valign=top>
		<input type=button value=" Delete " onclick="delAdmin(<?php echo($id); ?>,'<?php echo($val2); ?>')"></td>
	</tr>
    <?php
        }
    }
    mysql_close();
    ?>
</table>
<p><input type=button value="Add New" onclick="document.location.href = 'setup_admin_kpi_add.php';"></p>
<?php
$urlback = "setup_admin.php";
include("inc_goback.php");
?>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_log WHERE type = 'SAKPI' ORDER BY id DESC";
    include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
    ?>
<h2 style="color: #909090">Change log</h2>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdloghead>Date</td>
        <td class=tdloghead>User</td>
        <td class=tdloghead>Action</td>
    </tr>
    <?php
        while($row = mysql_fetch_array($rs))
        {
            ?>
    <tr>
        <td class=tdlog><?php echo(date("d M Y H:i",$row['date'])); ?></td>
        <td class=tdlog><?php
        $sql2 = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['tkid']."'";
        include("inc_db_con2.php");
            $row2 = mysql_fetch_array($rs2);
        mysql_close();
        echo($row2['tkname']." ".$row2['tksurname']);
         ?></td>
        <td class=tdlog><?php echo($row['action']); ?></td>
    </tr>
            <?php
        }
        ?>
</table>
<?php
$urlback = "setup_admin.php";
include("inc_goback.php");
?>
        <?php
    }
    mysql_close();
    ?>
<p>&nbsp;</p>
</body>
</html>
