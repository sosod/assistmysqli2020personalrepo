<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
    table td {
        border-collapse: collapse;
        border: 1px solid #dddddd;
    }
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<h2 class=fc>Update Capital Projects - Import update</h2>

<?php
$fileloc = $_POST['floc'];
$calc = $_POST['calc'];
$impcheck = $_POST['import'];

foreach($impcheck as $i)
{
    $import[$i] = "Y";
}

if(strlen($fileloc)>0 && file_exists($fileloc))
{
            $file = fopen($fileloc,"r");
            $f = 1;
            $r=1;
            $data = array();
set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
                if(count($tmpdata)>5 && $r>2)
                {
                    $data[$f] = $tmpdata;
                    $f++;
                }
                $r++;
                $tmpdata = array();
            }
            fclose($file);
set_time_limit(30);
            if(count($data)>0)
            {
                ?>
                <table cellpadding=3 cellspacing=0>
                    <tr>
                        <td class=tdheader rowspan=2>Ignite<br>Ref</td>
                        <td class=tdheader rowspan=2>Directorate</td>
                        <td class=tdheader rowspan=2>Sub-Directorate</td>
                        <td class=tdheader rowspan=2>Cap. Proj.<br>Number</td>
                        <td class=tdheader rowspan=2>Vote<br>Number</td>
                        <td class=tdheader rowspan=2>Project<br>Description</td>
                <?php
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sort >= 1 AND sort <= 12 ORDER BY sort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $timearr[$row['sort']] = $row;
                ?>
                        <td class=tdheader colspan=4><?php echo(date("F-Y",$row['eval'])); ?></td>
                    <?php
                    }
                mysql_close(); ?>
                    </tr>
                    <tr>
                    <?php foreach($timearr as $tim) { ?>
                        <td class=tdheader>Budget</td>
                        <td class=tdheader>Actual</td>
                        <td class=tdheader>Acc. % - YTD</td>
                        <td class=tdheader>R - YTD</td>
                    <?php } ?>
                    </tr>
                    <?php
                    foreach($data as $idata)
                    {
                        $cpid = $idata[0];
                    if($import[$cpid] == "Y")
                    {
                        if(strlen($cpid)>0 && is_numeric($cpid) && $cpid > 0)
                        {
                            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital c, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s ";
                            $sql.= " WHERE c.cpid = ".$cpid;
                            $sql.= " AND c.cpsubid = s.subid AND s.subdirid = d.dirid";
                            include("inc_db_con.php");
                                $mnr = mysql_num_rows($rs);
                                if($mnr > 0)
                                {
                                    $row = mysql_fetch_array($rs);
                                    $dir = $row['dirtxt'];
                                    $sub = $row['subtxt'];
                                    $cpnum = $row['cpnum'];
                                    $vote = $row['cpvotenum'];
                                    $project = $row['cpproject'];
                        ?>
                    <tr>
                        <td class=tdheader><?php echo($cpid); ?></td>
                        <td class=tdgeneral><?php echo($dir); ?></td>
                        <td class=tdgeneral><?php echo($sub); ?></td>
                        <td class=tdgeneral><?php echo($cpnum); ?></td>
                        <td class=tdgeneral><?php echo($vote); ?></td>
                        <td class=tdgeneral style="border-right: 1px solid #555555"><?php echo($project); ?></td>
                    <?php
                    $t = 6;
                    $ctotytd = 0;
                    $cbudytd = 0;
                    foreach($timearr as $tim)
                            {
                                $budget = $idata[$t];
                                if(!is_numeric($budget))
                                {
                                    $bud = explode(",",$budget);
                                    $budget = implode("",$bud);
                                }
                                if(strlen($budget)==0 || !is_numeric($budget)) { $budget = 0; }
                                $cbudytd = $cbudytd + $budget;

                                $actual = $idata[$t+1];
                                if(!is_numeric($actual))
                                {
                                    $ac = explode(",",$actual);
                                    $actual = implode("",$ac);
                                }
                                if(strlen($actual)==0 || !is_numeric($actual)) { $actual = 0; }
                                $ctotytd = $ctotytd + $actual;

                                $accytd = $idata[$t+2];
                                $accend = substr($accytd,-1,1);
                                if($accend == "%") { $accytd = substr($accytd,0,-1); $accytd = $accytd * 1;}
                                if(!is_numeric($accytd))
                                {
                                    $ay = explode(",",$accytd);
                                    $accytd = implode("",$ay);
                                }
                                if(strlen($accytd)==0 || !is_numeric($accytd)) { $accytd = 0; }

                                $totytd = $idata[$t+3];
                                if(!is_numeric($totytd))
                                {
                                    $tot = explode(",",$totytd);
                                    $totytd = implode("",$tot);
                                }
                                if(strlen($totytd)==0 || !is_numeric($totytd)) { $totytd = 0; }

                                $caccytd = ($ctotytd / $cbudytd) * 100;
                                if($calc == "Y")
                                {
                                    $accytd = $caccytd;
                                    $totytd = $ctotytd;
                                }
                                ?>
                        <td class=tdgeneral align=right style="border-left: 1px solid #555555"><?php echo(number_format($budget,2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($actual,2)); ?></td>
                        <td class=tdgeneral align=right><?php echo(number_format($accytd,2)); ?>%</td>
                        <td class=tdgeneral align=right style="border-right: 1px solid #555555"><?php echo(number_format($totytd,2)); ?></td>
                    <?php
                    $t = $t+4;
                    } ?>
                    </tr>
                        <?php
                                }   //mnr > 0
                                else
                                {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($cpid); ?></td>
                        <td class=tdgeneral colspan=53>Invalid reference number.  No capital project found with that reference.  This line will be ignored.</td>
                    </tr>
                                    <?php
                                }
                            mysql_close();
                        }   //valid cpid
                    }
                    else    //import check Y
                    {
                                    ?>
                    <tr>
                        <td class=tdheaderblue><?php echo($cpid); ?></td>
                        <td class=tdgeneral colspan=53>Line ignored per user request.</td>
                    </tr>
                                    <?php
                    }   //import check Y
                    }
                    ?>
                </table>
                <?php
            }
            else
            {
                echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
            }
}
else
{
            echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
}

?>
                <?php
                    $urlback = "admin_fin.php";
                    include("inc_goback.php");
                ?>
                <p>&nbsp;</p>

</body>
</html>
