<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Time Periods</b></h1>

<p>Please note: Auto reminders and closures are no longer available for this version of SDBIP.</p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td width=120 class=tdheaderl>Update</td>
		<td class=tdgeneral height=40>Update a time period.*</td>
		<td width=80 class=tdgeneral align=center><input type=button value=" Update " onclick="document.location.href = 'setup_time_list.php';"></td>
	</tr>
<!--	<tr>
		<td width=120 height=40 class=tdheaderl>Setup</td>
		<td class=tdgeneral>Setup automatic reminder emails and closure* of time periods.</td>
		<td width=80 class=tdgeneral align=center><input type=button id=setbut value="  Setup  " onclick="document.location.href = 'setup_time_setup.php';"></td>
	</tr> -->
	<tr>
		<td width=120 height=40 class=tdheaderl>Secondary Users</td>
		<td class=tdgeneral>Setup users for whom the secondary closure date is applicable.</td>
		<td width=80 class=tdgeneral align=center><input type=button id=setbut value="  Setup  " onclick="document.location.href = 'setup_time_users.php';"></td>
	</tr>
</table>
<p style="font-size: 7.5pt; margin-top: 5px;"><i>* Note: Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.</i></p>
<script type=text/javascript>
//document.getElementById('setbut').disabled = true;
</script>

<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
