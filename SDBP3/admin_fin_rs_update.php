<?php
    include("inc_ignite.php");
    $timeid = $_GET['t'];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<script type=text/javascript>
function imPort(mn) {
    if(document.getElementById('ifl').value.length>0)
    {
        if(confirm("You are about to update the financial cashflows for the month of "+mn+".\n\nDo you wish to continue?")==true)
        {
            document.forms['impt'].submit();
        }
    }
    else
    {
        alert("ERROR\nPlease select the file you wish to import.");
    }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<?php
$terr = "N";
if(is_numeric($timeid) && strlen($timeid)>0)
{
    $time = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$timeid." AND yn = 'Y'";
    include("inc_db_con.php");
        $time = mysql_fetch_array($rs);
    mysql_close();
    if(count($time)>4)
    {
    }
    else
    {
        $terr = "Y";
    }
}
else
{
    $terr = "Y";
}


if($terr == "Y")
{
    echo("<h2>Error</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
?>
<h2 class=fc>Update Revenue By Source - <u><?php echo(date("M Y",$time['eval'])); ?></u></h2>
<form id=impt method=post action=admin_fin_rs_update_import.php enctype="multipart/form-data" style="margin-top: 0"><input type=hidden name=t value="<?php echo($timeid); ?>">
<h3 class=fc>Update multiple line items:</h3>
<ol>
    <li>Export project details: <input type=button value=Export onclick="document.location.href = 'admin_fin_rs_update_export.php?t=<?php echo($timeid); ?>';"></li>
    <li>Update the financial details in the CSV document saved in step 1.</li>
    <li>Import updated information: <input type=file name=ifile id=ifl> (<span style="text-decoration: underline; color: #cc0001;"><?php echo(date("M Y",$time['eval'])); ?></span>) <input type=button onclick="imPort('<?php echo(date("F Y",$time['eval'])); ?>');" value=Import id=imp>
    <Br>Note: Once you have clicked the "Import" button, you will be given an opportunity to review the Import data before finalising the import.
    <br>Until you click the "Accept" button, on the next page, no information will be updated.</li>
</ol>
<p style="margin-bottom: 0;"><b>Import file information:</b>
<ol type=i>
<li>The first two lines will be treated as header rows and will not be imported.</li>
<li>The data needs to be in CSV format, seperated by commas, in the same layout as the export file generated in step 1.</li>
<li>The system will update the financials for the line items identified by the Reference in the first column.  Any lines that do not contain a valid Reference number will be ignored during the upload.  If you wish to add a line item please use the "<a href=admin_fin_cf_add.php>Add Cashflow Line Item</a>" form.</li>
<li>Please note that, by using this function, all columns for specified time period will be updated with the information in the import file.  You can choose whether to update the Adjusted Budget, Variance, Accumulated % Spent - YTD and Accumulated R Spent - YTD from the import file, or to allow the system to automatically calculate it for you.</li>
</ol>
</p>
</form>
<?php
}

$urlback = "admin_fin.php";
include("inc_goback.php");
?>
<script type=text/javascript>
//document.getElementById('imp').disabled = true;
</script>
</body>

</html>
