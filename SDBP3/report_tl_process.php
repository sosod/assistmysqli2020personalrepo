<?php
ini_set('max_execution_time',1800); 
include("inc_ignite.php");
//error_reporting(-1);
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
$nosort = array("wards","area","topbaseline","topcalctype","toptargettype","toptargetannual","toptargetrevised");
//SUMMARY OF RESULTS
function drawKRSum($what,$txt,$val,$i) {
	global $output;
	global $styler;
	$txt = decode($txt);
	if(!checkIntRef($val)) { $val = 0; }
	switch($what) {
		case "title":
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"Summary of Results\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td nowrap class=title2 style=\"text-align: left;\">Summary of Results</td>";
			} else {
				$echo ="</table><h2>Summary of Results</h2><table cellpadding=3 cellspacing=5 width=330>";
			}
			break;
		case "title0":
			$t = "Summary of Results: ".$txt;
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"".$t."\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td nowrap class=title4 style=\"text-align: left;\">".$t."</td>";
			} else {
				$echo ="</table><h4 style=\"margin-bottom: 5px; margin-top: 10px;\">".$t."</h4><table cellpadding=3 cellspacing=5 width=330 style=\"margin-bottom: 20px;\">";
			}
			break;
		case "sum":
			if($output=="csv") {
				$echo ="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo = "<tr><td></td><td nowrap style=\"font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$txt."</td><td nowrap style=\"text-align: left; font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$val."</td></tr>";
			} else {
				$echo = "<tr><td class=b-w>&nbsp;&nbsp;</td><td class=b-w style=\"font-weight: bold;\">".$txt."</td><td class=b-w style=\"text-align: right;font-weight: bold;\">&nbsp;".$val."</td></tr>";
				$echo.= "</table>";
			}
			break;
		default:
			if($output=="csv") {
				$echo.="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo.= "<tr><td style=\"".$styler[$i]."\"> </td><td nowrap>".$txt."</td><td nowrap style=\"text-align: left;\">".$val."</td></tr>";
			} else {
				$echo.= "<tr><td class=b-w style=\"".$styler[$i]."\">&nbsp;</td><td class=b-w>".$txt."</td><td class=b-w style=\"text-align: right;\">&nbsp;".$val."</td></tr>";
			}
			break;
	}
	return $echo;
}


//LISTS
	unset($dlist['prog']);
	unset($dlist['kpistratop']);
	unset($dlist['kpiriskrate']);
	unset($dlist['ktype']);
	unset($dlist['kpitargettype']);
$dlist['topcalctype']=array('name'=>"KPI Calculation Type",'id'=>"kpicalctype",'code'=>"Y",'yn'=>"Y",'headfield'=>"topcalctype",'kpi'=>"topcalctype",'fld'=>"code");
$dlist['toptargettype']=array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"toptargettype",'kpi'=>"toptargettypeid");
$dlist['idp']=array('name'=>"IDP Goal",'id'=>"idpgoal",'code'=>"N",'fld'=>"code",'yn'=>"Y",'headfield'=>"idp",'kpi'=>"topidpid");
$dlist['natkpa']['kpi'] = "topnatkpaid";
$dlist['munkpa']['kpi'] = "topmunkpaid";
$dlist['gfs']['kpi'] = "topgfsid";
$dlist['tas']['kpi'] = "toptasid";

foreach($dlist as $dl) {
		$dld = getListKPI($dl);
		$dlist[$dl['headfield']]['data'] = $dld;
}
//print_r($dlist['topcalctype']['data']);
  //WARDS
$dlist['wards'] = array('id'=>"wards",'headfield'=>"wards",'data'=>array());
$sql = "SELECT * FROM ".$dbref."_list_wards WHERE yn = 'Y' ORDER BY numval";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) { $dlist['wards']['data'][$row['id']] = $row; }
mysql_close($con);
  //AREA
$dlist['area'] = array('id'=>"area",'headfield'=>"area",'data'=>array());
$sql = "SELECT * FROM ".$dbref."_list_area WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) { $dlist['area']['data'][$row['id']] = $row; }
mysql_close($con);


//SET ARRAYS
$krarray = array();
foreach($krsetup as $krs) 
{
	$krarray[$krs['sort']] = array(
		'sort' => $krs['sort'],
		'filter' => 'N',
	);
}


/********* VARIABLES *************/
$field = array();
$filter = array();
$filtertype = array();
$kpis = array();
$results = array();
$time = array();
$src = $_REQUEST['src'];
if($src=="DASH") {
	$field['sub'] = "Y";
	$field['results'] = "Y";
	$field['krresult'] = "Y";
	$field['krcomment'] = "N";
	$field['krsdbipcomment'] = "N";
} else {
	$field['sub'] = $_REQUEST['sub'];
	$field['results'] = $_REQUEST['results'];
	$field['krresult'] = $_REQUEST['krresult'];
	$field['krcomment'] = $_REQUEST['krcomment'];
	$field['krsdbipcomment'] = $_REQUEST['krsdbipcomment'];
}
$cf = $_REQUEST['cf']; //budget from
$ct = $_REQUEST['ct']; //budget to
$kpir = $_REQUEST['topr'];
$filter['sub'] = $_REQUEST['subfilter'];
$sortarr = $_REQUEST['sort'];
$group = $_REQUEST['groupby'];
$output = $_REQUEST['output'];
$krsum = $_REQUEST['krsum'];
$rhead = $_REQUEST['rhead'];
$echocount = 0;
$kpiresultfilter = $_REQUEST['kpiresultfilter'];
if($kpiresultfilter=="ALL") {
	foreach($krarray as $krar)
	{
		$krarray[$krar['sort']]['filter'] = 'Y';
	}
} else {
	$krarray[$kpiresultfilter]['filter'] = 'Y';
}
$krgloss = $_REQUEST['krgloss'];
if(count($sortarr)==0) { $sort = array(); }
//HEADINGS
$sql = "SELECT * FROM ".$dbref."_headings WHERE headtype = 'TL' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
		if($src=="DASH" || $_REQUEST[$row['headfield']]=="Y") {
			$field[$row['headfield']] = "Y";
		}
		$head[$row['headfield']] = $row;
		$filter[$row['headfield']] = $_REQUEST[$row['headfield'].'filter'];
		$filtertype[$row['headfield']] = $_REQUEST[$row['headfield'].'filtertype'];
		if(count($sortarr)==0 && !in_array($row['headfield'],$nosort)) {
			$sort[] = $row['headfield'];
		}
    }
mysql_close($con);
if(count($sortarr)==0) { $sortarr = $sort; $sort = null; }
$head['toptargetannual'] = array('headdetail'=>"Annual Target",'headlist'=>"Annual Target",'headfield'=>"toptargetannual");
	$row['headfield'] = "toptargetannual";
		if($src=="DASH" || $_REQUEST[$row['headfield']]=="Y") {
			$field[$row['headfield']] = "Y";
		}
		$filter[$row['headfield']] = $_REQUEST[$row['headfield'].'filter'];
		$filtertype[$row['headfield']] = $_REQUEST[$row['headfield'].'filtertype'];
$head['toptargetrevised'] = array('headdetail'=>"Revised Target",'headlist'=>"Revised Target",'headfield'=>"toptargetrevised");
	$row['headfield'] = "toptargetrevised";
		if($src=="DASH" || $_REQUEST[$row['headfield']]=="Y") {
			$field[$row['headfield']] = "Y";
		}
		$filter[$row['headfield']] = $_REQUEST[$row['headfield'].'filter'];
		$filtertype[$row['headfield']] = $_REQUEST[$row['headfield'].'filtertype'];
//GET TIME PERIODS
$timesql = " FROM ".$dbref."_list_time WHERE yn = 'Y' AND sort >= ".$cf." AND sort <= ".$ct." AND id IN (3,6,9,12) ORDER BY sort";
$sql = "SELECT * ".$timesql;
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$time[$row['sort']] = $row;
		foreach($krarray as $krs) {
			$totals[$row['id']][$krs['sort']] = 0;
		}
	}
mysql_close($con);
		foreach($krarray as $krs) {
			$totals[0][$krs['sort']] = 0;
		}
//WARDS FILTER
$hf = "wards";
$kpiwa[$hf] = array();
if(count($filter[$hf]) != count($dlist[$hf]['data']) && count($filter[$hf])>0) {
	if($filtertype[$hf] == "any") {
		$sql = "SELECT DISTINCT twtopid as kpiid FROM ".$dbref."_toplevel_wards WHERE twwardsid IN (".implode(", ",$filter[$hf]).") AND twyn = 'Y' ORDER BY twtopid";
	} elseif($filtertype[$hf]=="all") {
		$wwhere = array();
		$sql = "SELECT t.topid as kpiid FROM ".$dbref."_toplevel k";
		foreach($filter[$hf] as $w) {
			$n = "w".$w;
			$sql.= " INNER JOIN ".$dbref."_toplevel_wards $n ON ".$n.".twtopid = k.topid";
			$wwhere[] = $n.".twwardsid = $w AND ".$n.".twyn = 'Y'";
		}
		$sql.= " WHERE ".implode(" AND ",$wwhere);
		$sql.= " AND k.topyn = 'Y' ORDER BY k.topid";
	} else {
		die("<h2>Error</h2><p>An error has occurred.  The Report cannot be generated.</p>");
	}
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) { $kpiwa[$hf][] = $row['kpiid']; }
	mysql_close($con);
} else {
	$filter[$hf] = array();
}

//AREA FILTER
$hf = "area";
$kpiwa[$hf] = array();
if(count($filter[$hf]) != count($dlist[$hf]['data']) && count($filter[$hf])>0) {
	if($filtertype[$hf] == "any") {
		$sql = "SELECT DISTINCT tatopid as kpiid FROM ".$dbref."_toplevel_area WHERE taareaid IN (".implode(", ",$filter[$hf]).") AND tayn = 'Y' ORDER BY tatopid";
	} elseif($filtertype[$hf]=="all") {
		$wwhere = array();
		$sql = "SELECT k.topid FROM ".$dbref."_toplevel k";
		foreach($filter[$hf] as $w) {
			$n = "a".$w;
			$sql.= " INNER JOIN ".$dbref."_toplevel_area $n ON ".$n.".tatopid = k.topid";
			$wwhere[] = $n.".taareaid = $w AND ".$n.".tayn = 'Y'";
		}
		$sql.= " WHERE ".implode(" AND ",$wwhere);
		$sql.= " AND k.topyn = 'Y' ORDER BY k.topid";
	} else {
		die("<h2>Error</h2><p>An error has occurred.  The Report cannot be generated.</p>");
	}
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) { $kpiwa[$hf][] = $row['kpiid']; }
	mysql_close($con);
} else {
	$filter[$hf] = array();
}

//INTERSECTING AREA/WARDS
if(count($filter['wards'])>0 && count($filter['area'])>0) {
	$kpiwardsarea = array_intersect($kpiwa['wards'],$kpiwa['area']);
} elseif(count($filter['wards'])>0 && !(count($filter['area'])>0)) {
	$kpiwardsarea = $kpiwa['wards'];
} elseif(!(count($filter['wards'])>0) && count($filter['area'])>0) {
	$kpiwardsarea = $kpiwa['area'];
} else {
	$kpiwardsarea = array();
}


//GROUP BY
$glist = array();
if(strlen($group)==0 || $group == "X") {
	$group = "X";
	$groupid = 0;
	$glist[] = array('id'=>0,'value'=>"");
} elseif(isset($dlist[$group])) {
	$groupid = $dlist[$group]['kpi'];
	$sql = "SELECT * FROM ".$dbref."_list_".$dlist[$group]['id']." WHERE yn = 'Y' ORDER BY value";
	include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$glist[] = $row;
	}
	mysql_close($con);
} elseif($group=="sub") {
	$groupid = "topsubid";
	$sql = "SELECT d.dirid as id, d.dirtxt as subtxt FROM ".$dbref."_dir d WHERE diryn = 'Y' ORDER BY dirsort";
	include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$glist[] = array('id'=>$row['id'],'value'=>$row['subtxt']);
	}
	mysql_close($con);
} else {
	$groupid = $group;
	$sql = "SELECT DISTINCT $groupid FROM ".$dbref."_toplevel WHERE topyn = 'Y'";
	include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$gval = $row[$groupid];
		$gid = strFn("str_replace",strtolower(decode($gval))," ","");
		$glist[] = array('id'=>$gid,'value'=>$gval);
	}
	mysql_close($con);
}		

		
		//SET REPORT SQL
$repsql[0] = "SELECT k.topid";
$repsql[1] = ", k.*";
$repsql[1].= ", d.dirtxt";
$repsql[2] = " FROM";
$repsql[2].= " ".$dbref."_toplevel k";
$repsql[2].= " INNER JOIN ".$dbref."_dir d ON d.dirid = k.topsubid ";
foreach($dlist as $d) {
	$hf = $d['headfield'];
	if($hf!="wards" && $hf!="area") {
		$repsql[2].= " INNER JOIN ".$dbref."_list_".$d['id']." ".$hf." ON ";
		switch($hf) {
			case "topcalctype":
				$repsql[2].= $hf.".code = k.".$d['kpi'];
				break;
			default:
				$repsql[2].= $hf.".id = k.".$d['kpi'];
		}
	}
}

$repsql[2].= " WHERE";
/*foreach($sortarr as $s) {
	if(isset($dlist[$s])) {
		$repsql[2].= " k.".$dlist[$s]['kpi']." = ".$s.".id AND ".$s.".yn = 'Y' AND";
	}
}*/
//$repsql[2].= " k.topsubid = d.dirid AND ";
$repsql[2].= " k.topyn = 'Y'";
$repsql[2].= " AND d.diryn = 'Y'";
$repsql[2].= " AND k.topid IN (SELECT DISTINCT trtopid FROM ".$dbref."_toplevel_result) ";
	//SQL FILTERS
if($filter['sub'] != "ALL" && is_numeric($filter['sub']) && strlen($filter['sub'])>0) {
    $repsql[2].= " AND d.dirid = ".$filter['sub'];
}
$headlists = array("HEADINGLISTS");
foreach($dlist as $dl) {
	$headlists[] = $dl['headfield'];
}
$headlists[] = "wards";
$headlists[] = "area";
$wardsareadone = "N";
foreach($head as $h) {
	$where = "";
	$hf = $h['headfield'];
	if (($hf == "wards" || $hf == "area")) {				//WARDS/AREA
		if(count($kpiwardsarea)>0 && $wardsareadone == "N") {
			$where = " k.topid IN (".implode(", ",$kpiwardsarea).")";
			$wardsareadone = "Y";
		}
	} elseif(array_key_exists($hf,$dlist)) {					//LISTS
		switch($hf) {
			case "topcalctype":
				if(count($filter[$hf])!=count($dlist[$hf]['data'])) {
					if(count($filter[$hf])>1 && strtolower($filter[$hf][0])!="all") {
//					if(count($filter[$hf])!=count($dlist[$hf]['data'])) {
						$where = "(";
						$where.= $hf.".code = '".implode("' OR ".$hf.".code = '",$filter[$hf])."'";
						$where.= ")";
//					}
					} elseif(strtolower($filter[$hf][0])!="all" && strlen($filter[$hf][0])>0) {
						$where = $hf.".code = '".$filter[$hf][0]."'";
					}
				}
				break;
			default:
				if(count($filter[$hf])>1 && strtolower($filter[$hf][0])!="all") {
					if(count($filter[$hf])!=count($dlist[$hf]['data'])) {
						$where = "(";
						$where.= $hf.".id = ".implode(" OR ".$hf.".id = ",$filter[$hf]);
						$where.= ")";
					}
				} elseif(strtolower($filter[$hf][0])!="all" && strlen($filter[$hf][0])>0) {
					$where = $hf.".id = ".$filter[$hf][0];
				}
				break;
		}
	} elseif(strlen($filter[$hf])>0) {							//TEXT FIELDS
			$filter[$hf] = trim($filter[$hf]);
                    switch($filtertype[$hf])
                    {
						case "X":
							break;
                        case "all":
                            $ft = array_unique(explode(" ",code($filter[$hf])));
							$where = "(".$hf." LIKE '%".strFn("implode",$ft,"%' AND $hf LIKE '%","")."%')";
							$where = str_replace("AND $hf LIKE '%%'","",$where);
                            break;
                        case "any":
                            $ft = array_unique(explode(" ",code($filter[$hf])));
							$where = "(".$hf." LIKE '%".strFn("implode",$ft,"%' OR $hf LIKE '%","")."%')";
							$where = str_replace("OR $hf LIKE '%%'","",$where);
                            break;
                        case "exact":
                            $where = $hf." LIKE '%".code($filter[$hf])."%'";
                            break;
                        default:
                            $where = $hf." LIKE '%".code($filter[$hf])."%'";
                            break;
                    }
	}
	if(strlen($where)>0) {
		$repsql[2].=" AND ".$where;
	}
}
	//SQL ORDER
	$repsql[3] = " ORDER BY ";
	$r=0;
foreach($sortarr as $s) {
	$r++;
	if($r>1) { $repsql[3].=", "; }
	if(isset($dlist[$s])) {
		$repsql[3].= " ".$s.".value ASC";
	} elseif($s=="sub") {
		$repsql[3].= " d.dirsort ASC";
	} else {
		$repsql[3].= " k.".$s." ASC";
	}
}

//GET KPIs
$sql = implode(" ",$repsql);
//echo $sql;
//echo "<pre>";
//print_r($filter);
//echo "</pre>";
include("inc_db_con.php");
$gkpis = array();
	while($row = mysql_fetch_assoc($rs))
	{
		if(is_numeric($groupid)) { 
			$kpis[0][$row['topid']] = $row;
			$gkpis[$row['topid']] = 0;
		} elseif(isset($dlist[$group])) {
			$kpis[$row[$groupid]][$row['topid']] = $row;
			$gkpis[$row['topid']] = $row[$groupid];
		} else {
			$gval = strFn("str_replace",strtolower(decode($row[$groupid]))," ","");
			$kpis[$gval][$row['topid']] = $row;
			$gkpis[$row['topid']] = $gval;
		}
	}
mysql_close($con);
//GET RESULTS
if($field['results'] == "Y") {
	$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (";
	$sql.= $repsql[0].$repsql[2].")";
	$sql.= " AND trtimeid IN (SELECT id ".$timesql.")";
	$sql.= " ORDER BY trtopid, trtimeid";
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs))
		{
			$results[$row['trtopid']][$row['trtimeid']] = $row;
			$results[$row['trtopid']][$row['trtimeid']]['krr'] = 0;
		}
	mysql_close($con);
	foreach($results as $row) 
	{
					$kpiresult = $row;
					$kc = 0;
					$ytdt = 0;
					$ytda = 0;
					$krt0 = 0;
					$kra0 = 0;
					$krr1 = 0;
					$krc = 0;
                    foreach($time as $tim)
                    {
						$timeid = $tim['id'];
						$kpiid = $kpiresult[$timeid]['trtopid'];
						$gval = $gkpis[$kpiid];
						$kct = $kpis[$gval][$kpiid]['topcalctype'];
						$ktt = $kpis[$gval][$kpiid]['toptargettypeid'];
                        switch($kct)
                        {
                            case "CO":
                                $sql2 = "SELECT max(trtarget) as trtarget, max(tractual) as tractual FROM ".$dbref."_toplevel_result WHERE trtopid = ".$kpiid." AND trtimeid <= ".$timeid;
                                include("inc_db_con2.php");
                                    $row2 = mysql_fetch_array($rs2);
                                mysql_close($con2);
                                $row2['trprogress'] = $kpiresult[$timeid]['trprogress'];
								$row2['trsdbipcomment'] = $kpiresult[$timeid]['trsdbipcomment'];
                                $row2['trtargettypeid'] = $kpiresult[$timeid]['trtargettypeid'];
                                break;
                            default:
								$row2 = $kpiresult[$timeid];
								break;
                        }
                        $krt = $row2['trtarget'];
                        $kra = $row2['tractual'];
						if(checkIntRef($krt)) {
							if($kct == "CO") {
								$krt0 = $krt;
							} else {
								$krt0 += $krt;
								if($krt>0) { $kc++; }
							}
						} else { $krt = 0; }
						if(checkIntRef($kra)) {
							if($kct == "CO") {
								$kra0 = $kra;
							} else {
								$kra0 += $kra;
							}
						} else { $kra = 0; }
                        if($krt > 0 || $kra > 0 || $kct == "ZERO" || $kct == "REV") {
							$nf=0;
							if(round($krt)!=$krt || round($kra)!=$kra) { $nf = 2; }
							$krtarget = formatValue($ktt,$krt,$nf);
							$kractual = formatValue($ktt,$kra,$nf);
                        } else {
                            $krtarget = "";
                            $kractual = "";
						}
						if($tim['eval']<$today || $kra > 0 || (($kct == "ZERO" || $kct == "REV") && $tim['sval']>$today) || $krc > 0) {
							$krr = calcKR($kct,$krt,$kra);
						} else {
							$krr = 0;
                        }
						$krr1 += $krr;
						if($krr>1) { $krc++; }
						$results[$kpiid][$timeid]['krr'] = $krr;
						$results[$kpiid][$timeid]['krt'] = 0+$krt;
						$results[$kpiid][$timeid]['kra'] = $kra;
						$results[$kpiid][$timeid]['krtdisp'] = $krtarget." ";
						$results[$kpiid][$timeid]['kradisp'] = $kractual;
						$totals[$timeid][$krr] += 1;
                    }
					if($kc==0 && ($kra0 > 0 || $krt0 > 0)) { $kc = 1; }
                
					if($krr1>0 || $krc > 0) {
						if(($kct == "STD" || $kct == "REV") && $kc >0) { $krt0 /= $kc; $kra0 /= $kc; }
						$krr0 = calcKR($kct,$krt0,$kra0);
					} else {
						$krr0 = 1;
					}
					$krtarget0 = "";
					$kractual0 = "";
                        if($krt0 > 0 || $kra0 > 0 || $kct == "ZERO" || $kct == "REV") {
							$nf=0;
							if(round($krt0)!=$krt0 || round($kra0)!=$kra0) { $nf = 2; }
							$krtarget0 = formatValue($ktt,$krt0,$nf);
							$kractual0 = formatValue($ktt,$kra0,$nf);
                        } else {
                            $krtarget0 = "";
                            $kractual0 = "";
                        }
						if($krarray[$krr0]['filter']=="Y") {
							$results[$kpiid]['krr0'] = $krr0;
							$results[$kpiid]['krt0'] = $krt0;
							$results[$kpiid]['kra0'] = $kra0;
							$results[$kpiid]['krtdisp0'] = $krtarget0;
							$results[$kpiid]['kradisp0'] = $kractual0;
							$totals[0][$krr0] += 1;
						} else {
							$totals[0][$krr0] += 1;
							unset($results[$kpiid]);
							unset($kpis[$gval][$kpiid]);
						}
	}
}

//OUTPUT
				$c2 = 0;
				if($field['krresult']=="Y") $c2+=3;
				if($field['krcomment']=="Y") $c2+=1;
				if($field['krsdbipcomment']=="Y") $c2+=1;

$echo = "";
$cspan = 1 + count($field) - 4;
$tspan = (count($time) * $c2) + 3;
$cspan += $tspan;
switch($output) 
{
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22][0] = "\""; //result cell
	$cella[22][1] = "\""; //result cell
	$cella[22][2] = "\""; //result cell
	$cella[22][3] = "\""; //result cell
	$cella[22][4] = "\""; //result cell
	$cella[22][5] = "\""; //result cell
	$cella[22][6] = "\""; //result cell
	$rowa = "";
	$rowz = "\r\n";
	if(strlen($rhead)>0) {
		$pagea = "\"".$rhead."\"\r\n";
	} else {
		$pagea = "\"SDBIP ".$modtxt.": Top Level SDBIP Report\"\r\n";
	}
	$table[1] = "";
	$table[9] = "";
	$pagez = "\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	break;
case "excel":
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	if($field['results']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[10] = "<td class=\"head\" colspan=$c2>"; //time heading cell
	$cellz[10] = "</td>"; //time heading cell
	$cella[11] = "<td class=\"head\" colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</td>"; //period-to-date heading cell
	$cella[12] = "<td class=\"head\" rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</td>"; //rowspan heading cell
	$cella[2] = "<td class=kpi>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=kpi style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22][0] = "<td class=kpi>"; //result cell
	$cella[22][1] = "<td class=kpi style=\"".$styler[1]."\">"; //result cell
	$cella[22][2] = "<td class=kpi style=\"".$styler[2]."\">"; //result cell
	$cella[22][3] = "<td class=kpi style=\"".$styler[3]."\">"; //result cell
	$cella[22][4] = "<td class=kpi style=\"".$styler[4]."\">"; //result cell
	$cella[22][5] = "<td class=kpi style=\"".$styler[5]."\">"; //result cell
	$cella[22][6] = "<td class=kpi style=\"".$styler[6]."\">"; //result cell
	$rowa = chr(10)."<tr>";
	$rowb = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>KPI Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>".chr(10)." td { font-style: Calibri; font-size:11pt; } .kpi { border-width: thin; border-color: #000000; border-style: solid; } .head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } ".chr(10)." .title { font-size:20pt; font-style: Calibri; color:#CC0001; font-weight:bold; text-decoration:underline; text-align: center; } ".chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#CC0001; font-weight:bold; text-decoration:underline; text-align: center;} .title3 { font-size:14pt; font-style: Calibri; color:#CC0001; font-weight:bold;} .title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$cspan>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$cspan>";
	if(strlen($rhead)>0) {
		$pagea.= $rhead;
	} else {
		$pagea.= "SDBIP ".$modtxt.": Top Level SDBIP Report";
	}
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = "<br />";
	break;
default:
	$gcell[1] = "<h3 class=fc>";	//start of group
	$gcell[9] = "</h3>";	//end of group
	if($field['results']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[10] = "<th colspan=$c2>"; //time heading cell
	$cellz[10] = "</th>"; //time heading cell
	$cella[11] = "<th colspan=3>"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<th rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</th>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22][0] = "<td>"; //result cell
	$cella[22][1] = "<td style=\"".$styler[1]."\">"; //result cell
	$cella[22][2] = "<td style=\"".$styler[2]."\">"; //result cell
	$cella[22][3] = "<td style=\"".$styler[3]."\">"; //result cell
	$cella[22][4] = "<td style=\"".$styler[4]."\">"; //result cell
	$cella[22][5] = "<td style=\"".$styler[5]."\">"; //result cell
	$cella[22][6] = "<td style=\"".$styler[6]."\">"; //result cell
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/default.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"/styles/style_red.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"lib/SDBP2.css\" type=\"text/css\">";
	$pagea.= "<style type=text/css>table { border-width: 1px; border-style: solid; border-color: #ababab; }    table td { border: 1px solid #ababab; } .b-w { border: 1px solid #fff; }</style>";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc style=\"text-align: center;\">".$cmpname."</h1><h2 class=fc style=\"text-align: center;\">";
	if(strlen($rhead)>0) {
		$pagea.=$rhead;
	} else {
		$pagea.= "SDBIP ".$modtxt.": Top Level SDBIP Report";
	}
	$pagea.= "</b></h2>";
	$table[1] = "<table cellpadding=3 cellspacing=0 width=100%>";
	$table[9] = "</table>";
	$pagez = "<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	break;
}

//START PAGE
$echo = $pagea;
//GROUP LOOP
foreach($glist as $g) {
	$groupresults = array();	//reset summary/group array
	$kpi2 = $kpis[$g['id']];
if(count($kpi2)>0) {
	if($g['id']!=0) {
		$echo.= $table[9].$gcell[1].$g['value'].$gcell[9].$table[1];
	} else {
		$echo.= $table[1];
	}
//TABLE HEADINGS
	$echo.= $rowa;
	$echo.= $cella[12]."Ref".$cellz[12];
	if($field['sub']=="Y") { $echo.= $cella[12]."Directorate".$cellz[12]; }
	foreach($head as $hrow)
	{
		if($field[$hrow['headfield']] == "Y" && $hrow['headfield']!="trcorrective") { $echo.= $cella[12].decode($hrow['headdetail']).$cellz[12]; }
	}
	if($field['results'] == "Y")
	{
		if($c2>0) {
			foreach($time as $tim) {
				$echo.= $cella[10]."Quarter ending ".date("d F Y",$tim['eval']).$cellz[10];
				if($output == "csv") {
					for($c=1;$c<$c2;$c++) {
						$echo.=$cella[1].$cellz[1];
					}
				}
			}
		}
		if(count($time)==4) {
			$echo.= $cella[11]."Overall Performance for the Year".$cellz[11];
		} elseif(count($time)>1) {
			$echo.= $cella[11]."Overall Performance for Quarters Ending ".date("d M Y",$time[$cf]['eval'])." - ".date("d M Y",$time[$ct]['eval']).$cellz[11];
		} else {
			$echo.= $cella[11]."Overall Performance for Quarter Ending ".date("d M Y",$time[$cf]['eval']).$cellz[11];
		}
	}
	if($field['trcorrective']=="Y") { 
		if($field['results']=="Y" && $output=="csv") { for($i=1;$i<3;$i++) { $echo.=$cella[1].$cellz[1]; } }
		$echo.= $cella[12].decode($head['trcorrective']['headdetail']).$cellz[12]; 
	}
	$echo.=$rowz;
	if($field['results'] == "Y")
	{
		$echo.=$rowa;
		if($output == "csv") {
			for($c=1;$c<(count($field)-3);$c++)
			{
				$echo.=$cella[1].$cellz[1];
			}
		}
		for($t=1;$t<=count($time);$t++)
		{
			if($field['krresult']=="Y") {
				$echo.=$cella[1]."Target".$cellz[1];
				$echo.=$cella[1]."Actual".$cellz[1];
				$echo.=$cella[1]."R".$cellz[1];
			}
			if($field['krsdbipcomment']=="Y") {
				$echo.=$cella[1]."Dept. SDBIP Comment".$cellz[1];
			}
			if($field['krcomment']=="Y") {
				$echo.=$cella[1]."Performance Comment".$cellz[1];
			}
		}
		$echo.=$cella[1]."Target".$cellz[1];
		$echo.=$cella[1]."Actual".$cellz[1];
		$echo.=$cella[1]."R".$cellz[1];
		$echo.=$rowz;
	}

//TABLE ROWS
	foreach($kpi2 as $kpi)
	{
		$kpiid = $kpi['topid'];
		if(isset($results[$kpiid])) {
		$echo.= $rowa;
		$echo.= $cella[2].$kpiid.$cellz[2];
		if($field['sub']=="Y") { $echo.= $cella[2].decode($kpi['dirtxt']).$cellz[2]; }
		foreach($head as $hrow)
		{
			$hfield = $hrow['headfield'];
			if($field[$hfield] == "Y" && $hfield!="trcorrective") { 
				$echo.= $cella[2];
				if($dlist[$hfield]['yn']=="Y") {
					$k = $dlist[$hfield]['kpi'];
					$dl = $dlist[$hfield]['data'];
					$echo.= decode($dl[$kpi[$k]]['value']);
					if($dlist[$hfield]['code']=="Y" && $hfield != "topcalctype") { $echo.= " (".$dl[$kpi[$k]]['code'].")"; }
				} else {
                        switch($hrow['headfield'])
                        {
                            case "area":
                                $sql2 = "SELECT a.* FROM ".$dbref."_toplevel_area ta, ".$dbref."_list_area a";
                                $sql2.= " WHERE a.yn = 'Y' AND a.id = ta.taareaid AND ta.tayn = 'Y' AND ta.tatopid = ".$kpiid;
                                $sql2.= " ORDER BY a.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $echo.= ($row2['value']."; ");
                                    }
                                mysql_close($con2);
                                $echo.= (" ");
                                break;
                            case "wards":
								$wecho = "";
                                $sql2 = "SELECT w.* FROM ".$dbref."_toplevel_wards tw, ".$dbref."_list_wards w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = tw.twwardsid AND tw.twyn = 'Y' AND tw.twtopid = ".$kpiid;
                                $sql2.= " ORDER BY w.numval, w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
										if($row2['numval']>0) {
											$wecho .= $row2['numval']."; ";
										} else {
											$wecho.= ($row2['value']."; ");
										}
                                    }
                                mysql_close($con2);
                                $echo.= $wecho;
                                break;
							case "toptargetannual":
							case "toptargetrevised":
								$echo.= formatValue($kpi['toptargettypeid'],$kpi[$hrow['headfield']],0)." ";
								break;
                            default:
                                $echo.= decode($kpi[$hrow['headfield']])." ";
                                break;
                        }
				}
				$echo.= $cellz[2];
			}
		}
		$corrective = array();
		if($field['results'] == "Y" || $field['trcorrective']=="Y")
		{
			$res = $results[$kpiid];
			foreach($time as $tim)
			{
				$tid = $tim['id'];
				$krr = $res[$tid]['krr'];
				if(!checkIntRef($krr)) { $krr = 0; }
				if($field['krresult']=="Y") {
					$echo.=$cella[2].$res[$tid]['krtdisp'].$cellz[2];
					$echo.=$cella[2].$res[$tid]['kradisp'].$cellz[2];
					$echo.=$cella[22][$krr].$krsetup[$krr]['code'].$cellz[2];
				}
				if($field['krsdbipcomment']=="Y") {
					$echo.=$cella[2].$res[$tid]['trsdbipcomment'].$cellz[2];
				}
				if($field['krcomment']=="Y") {
					$echo.=$cella[2].$res[$tid]['trcomment'].$cellz[2];
				}
				$corr = "";
				$corr = decode($res[$tid]['trcorrective']);
				if($output!="csv") { 
					$corr.= " <small><i>[".date("d M Y",$tim['eval'])."]</i></small>";
				} else {
					$corr.= " [".date("d M Y",$tim['eval'])."]";
				}
				$corrective[] = $corr;
			}
			if($field['results']=="Y") {
				$echo.=$cella[21].$res['krtdisp0'].$cellz[21];
				$echo.=$cella[21].$res['kradisp0'].$cellz[21];
				$echo.=$cella[22][$res['krr0']].$krsetup[$res['krr0']]['code'].$cellz[21];
			}
			$groupresults[$res['krr0']] += 1;
			if($field['trcorrective']=="Y") {
				$echo.= $cella[2];
				$echo.= implode($newline,$corrective);
				$echo.= $cellz[2];
			}
			$echo.=$rowz;
		}
		}//results is set
	}
	if($krsum == "Y" && $field['results'] == "Y" && $g['id']!=0) {
		$echo.= drawKRSum('title0',$g['value'],0,0);
		foreach($krarray as $kra) {
			if($kra['filter']=="Y") { 
				$echo.= drawKRSum('row',$krsetup[$kra['sort']]['value'],$groupresults[$kra['sort']],$kra['sort']);
			}
		}
		$echo.= drawKRSum('sum','Total KPIs',array_sum($groupresults),0);
	}
} //end if group(kpis)>0
} //end foreach group

if($krgloss == "Y" && $output == "display" && $field['results'] == "Y") { $echo.= displayKRGloss('report'); }

if($krsum == "Y" && $field['results'] == "Y") {
	$echo.= drawKRSum('title','',0,0);
	foreach($krarray as $kra) {
			if($kra['filter']=="Y") { 
				$echo.= drawKRSum('row',$krsetup[$kra['sort']]['value'],$totals[0][$kra['sort']],$kra['sort']);
			}
	}
	$echo.= drawKRSum('sum','Total KPIs',array_sum($totals[0]),0);
}

if($krgloss == "Y" && $output != "display" && $field['results'] == "Y") { $echo.= displayKRGloss($output); }
	
//END PAGE
$echo.= $table[9].$pagez;

//DISPLAY OUTPUT
switch($output)
{
	case "csv":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/".$_ta_."_kpi_".date("Ymd_Hi",$today).".csv";
        $newfilename = "kpi_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'text/plain';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("download.php");
		break;
	case "excel":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/".$_ta_."_kpi_".date("Ymd_Hi",$today).".xml";
        $newfilename = "kpi_report_".date("Ymd_Hi",$today).".xls";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'application/ms-excel';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("download.php");
		break;
	default:
		echo $echo;
		break;
}

?>