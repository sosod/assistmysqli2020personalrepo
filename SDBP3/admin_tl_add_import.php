<?php
    include("inc_ignite.php");

	$administ = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."' AND type = 'TL'";
    include("inc_db_con.php");
		$tladmin = mysql_num_rows($rs);
    mysql_close($con);
	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<script type=text/javascript>
function Validate(me) {
	if(me.ifile.value.length > 0) {
		if(confirm("You are about to import Top Level KPIs for <?php echo strtoupper($cmpcode); ?>.\n\nAre you sure you wish to continue?")==true)
			return true;
	} else {
		alert("Please select the import file by clicking on the \"Browse\" button.");
	}
	return false;
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Top Level: Import</b></h1>
<?php
if($tladmin ==0)
	die("<P>You are not authorised to view this page.</p>");
?>
<form name=import id=imp action=admin_tl_add_import_process.php method=post onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">
<input type=hidden name=act value=CHECK>
<ol>
	<li>Ensure that all of the <a href=setup_lists.php>lists</a> include those list items in the Top Level:
		<ul>
			<li><a href=setup_dir.php>Directorates</a></li>
			<li><a href="setup_lists_config.php?l=munkpa">Municipal KPA</a></li>
			<li><a href="setup_lists_config.php?l=idpgoal">IDP Goal</a></li>
			<li><a href="setup_lists_config.php?l=gfs">GFS Classification</a></li>
			<li><a href="setup_lists_config.php?l=wards">Wards</a></li>
			<li><a href="setup_lists_config.php?l=area">Area</a></li>
		</ul>
	</li>
	<li>Generate a Top Level Import Template <input type=button value="  Go  " onclick="document.location.href = 'admin_tl_add_import_export.php';"></li>
	<li>Copy the Top Level data into the template.  Please note the following:
		<ul>
			<li>Keep the template in CSV format.</li>
			<li>Top Level KPIs must start from Row 3.  The first 2 rows will be ignored.</li>
			<li>Leave the column A blank.  The system will automatically generate the Ignite Reference and store it in this column.</li>
			<li>Required fields are marked with a *.</li>
			<li>Note the formatting guidelines given in Row 2.</li>
		</ul>
	</li>
	<li>Import the updated template: <input type=file name=ifile id=ife> <input type=submit value=Import></li>
	<li>Review the data as imported by the system and click "Accept" to finalise the import.<br />Any KPIs with import errors will be highlighted in <span style="color: #CC0001">RED</span>.  Until all errors are attended to the "Accept" button will not be available.<br />Until the "Accept" button is clicked, the Top Level will not be made available on the system.</li>
</ol>
</form>
<p>&nbsp;</p>
</body>


</html>
