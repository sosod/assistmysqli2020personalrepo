<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>

<base target="main">
<script type="text/javascript">
function Validate(me) {
    var dirtxt = me.dirtxt.value;
    var pos = me.pos.value;
    var poslist = me.poslist.value;
    var valid8 = "false";
    if(dirtxt.length == 0)
    {
        alert("Please enter the name of the Directorate.");
    }
    else
    {
        if(pos == "a" || pos == "b")
        {
            if(poslist == "X")
            {
                alert("Please complete the Display Position.");
            }
            else
            {
                valid8 = "true";
            }
        }
        else
        {
            valid8 = "true";
        }
    }
    if(valid8=="true")
        return true;
    else
        return false;
}

function chgPos(me) {
    var pos = document.getElementById('pos').value;
    if(pos == "f" || pos == "l")
    {
        document.getElementById('poslist').disabled = true;
    }
    else
    {
        document.getElementById('poslist').disabled = false;
    }
}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Add New Directorate</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=setup_dir_add_process.php onsubmit="return Validate(this);">
<table cellpadding=3 cellspacing=0 width=570 id="table1">
	<tr>
		<td class=tdheaderl width=130>Name:</td>
		<td class=TDgeneral><input type=text size="50" name=dirtxt maxlength=50>&nbsp;</td>
		<td class=TDgeneral width=110><small>Max 50 characters</small></td>
	</tr>
	<tr>
		<td class=tdheaderl valign=top>Display position:</td>
		<td class=tdgeneral valign=top colspan="2"><select name=pos id=pos onchange="chgPos(this)">
			<option value=f>First</option>
			<option value=b>Before</option>
			<option value=a>After</option>
			<option selected value=l>Last</option>
		</select><select name=poslist id=poslist><option selected value=X>--- SELECT ---</option><?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
            echo("<option value=".$id.">".$val."</option>");
        }
    mysql_close();
    $r = 1;
?></select>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdheaderl valign=top >Sub-Directorates:</td>
		<td class=tdgeneral valign=top>
            <input type=text size="50" name=sub[] maxlength=50><br>
            <input type=text size="50" name=sub[] maxlength=50><br>
            <input type=text size="50" name=sub[] maxlength=50><br>
            <input type=text size="50" name=sub[] maxlength=50><br>
            <input type=text size="50" name=sub[] maxlength=50>&nbsp;
        </td>
		<td class=tdgeneral valign=top><small>Max 50 characters</small></td>
	</tr>
	<tr>
		<td class=tdheaderl valign=top>&nbsp;</td>
		<td class=tdgeneral valign=top colspan="2"><input type=submit value=" Add "> <input type=reset value="Reset"></td>
	</tr>
</table>
<script type=text/javascript>
var pos = document.getElementById('pos').value;
if(pos == "f" || pos == "l")
{
    document.getElementById('poslist').disabled = true;
}
</script>
<?php
$urlback = "setup_dir.php";
include("inc_goback.php");
?>
</body>
</html>
