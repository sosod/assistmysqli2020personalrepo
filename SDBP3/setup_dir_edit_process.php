<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Edit Directorate</b></h1>
<p>&nbsp;</p>
<?php
$dirid = $_POST['dirid'];
$dirtxt = htmlentities($_POST['dirtxt'],ENT_QUOTES,"ISO-8859-1");
$pos = $_POST['pos'];
$poslist = $_POST['poslist'];

function lastPos($cmpcode,$modref)
{
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                    $dirsort = $row['dirsort'] + 1;
                }
                else
                {
                    $dirsort = 1;
                }
            mysql_close();
            return $dirsort;
}

//SET POSITION
switch($pos)
{
    case "a":
        $dirsort = 0;
      //get position of dir to follow
        if(is_numeric($poslist))
        {
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' AND dirid = ".$poslist." ORDER BY dirsort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                }
                else
                {
                    $dirsort = lastPos($cmpcode,$modref);
                }
        }
        else
        {
            $dirsort = lastPos($cmpcode,$modref);
        }
        if($dirsort<1)
        {
          //set dirsort = apos + 1
            $dirsort = $row['dirsort']+1;
          //update sql set dirsort = dirsort + 1 where dirsort > apos
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_dir SET dirsort = dirsort + 1 WHERE diryn = 'Y' AND dirsort > ".$row['dirsort'];
            include("inc_db_con.php");
        }
        break;
    case "b":
        $dirsort = 0;
      //get position of dir to follow
        if(is_numeric($poslist))
        {
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' AND dirid = ".$poslist." ORDER BY dirsort DESC";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $row = mysql_fetch_array($rs);
                }
                else
                {
                    $dirsort = lastPos($cmpcode,$modref);
                }
        }
        else
        {
            $dirsort = lastPos($cmpcode,$modref);
        }
        if($dirsort<1)
        {
          //set dirsort = apos + 1
            $dirsort = $row['dirsort']-1;
          //update sql set dirsort = dirsort + 1 where dirsort > apos
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_dir SET dirsort = dirsort + 1 WHERE diryn = 'Y' AND dirsort >= ".$row['dirsort'];
            include("inc_db_con.php");
        }
        break;
    case "f":
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_dir SET dirsort = dirsort + 1 WHERE diryn = 'Y'";
        include("inc_db_con.php");
        $dirsort = 1;
        break;
    case "l":
        $dirsort = lastPos($cmpcode,$modref);
        break;
    default:
        $dirsort = lastPos($cmpcode,$modref);
        break;
}

if($dirsort<0 || strlen($dirsort)==0 || !is_numeric($dirsort)) { $dirsort = lastPos($cmpcode,$modref); }

//edit record
$sql = "UPDATE assist_".$cmpcode."_".$modref."_dir SET ";
$sql.= "  dirtxt = '".$dirtxt."'";
$sql.= ", dirsort = ".$dirsort;
$sql.= ", diryn = 'Y'";
$sql.= " WHERE dirid = ".$dirid;
include("inc_db_con.php");
$tsql = $sql;

$told = "";
$trans = $logaction;
include("inc_transaction_log.php");
    //update record in assist_cc_list_dept
    $sql = "SELECT * FROM assist_".$cmpcode."_sdbp2_dir_dept WHERE dirid = ".$dirid;
//    include("inc_db_con.php");
        $ddrow = mysql_fetch_array($rs);
    mysql_close();
    $sql = "UPDATE assist_".$cmpcode."_list_dept SET";
    $sql.= "  value = '".$dirtxt."'";
    $sql.= ", yn = 'Y'";
    $sql.= " WHERE id = ".$ddrow['deptid'];
//    include("inc_db_con.php");
    //update HoD sub
    $sql = "UPDATE assist_".$cmpcode."_".$modref."_dirsub SET ";
    $sql.= "  subtxt = 'Head of ".$dirtxt."'";
    $sql.= ", subyn = 'Y'";
    $sql.= "WHERE subdirid = ".$dirid;
    $sql.= " AND subhead = 'Y' AND subsort = 1";
    include("inc_db_con.php");
    //display result
    ?>
    <p>Directorate '<?php echo($dirtxt); ?>' has been successfully edited.</p>
    <p>&nbsp;</p>
    <?php
    $urlback = "setup_dir.php";
    include("inc_goback.php");
    ?>
    <script type=text/javascript>
    document.location.href = 'setup_dir.php';
    </script>
</body>
</html>
