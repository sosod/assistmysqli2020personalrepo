<?php
/************** FUNCTION *********************/
function formatResult($tt,$val) {
	$echo = "";
	switch($tt) {
		case "R":
			$echo = "R ".number_format($val,2);
			break;
		case "%":
			$echo = number_format($val,2).$tt;
			break;
		case "#":
		default:
			$echo = number_format($val,2);
			break;
	}
	return $echo;
}
/************** GET LISTS *********************/
$sql = "SELECT headlist, headfield FROM ".$dbref."_headings WHERE headtype = 'TL' AND headlistyn = 'Y' AND headfield <> 'topcalctype' AND headfield <> 'topvalue' ORDER BY headlistsort";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$head[$row['headfield']] = $row;
	}
mysql_close($con);
$head['toptargetannual'] = array('headlist'=>"Annual Target",'headfield'=>"toptargetannual");
$head['toptargetrevised'] = array('headlist'=>"Revised Target",'headfield'=>"toptargetrevised");
/*$sql = "SELECT id, sval, eval FROM ".$dbref."_list_time WHERE id IN (3,6,9,12)";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$time[$row['id']] = $row;
	}
mysql_close($con);*/
$sql = "SELECT id, sval, eval, active FROM ".$dbref."_list_time WHERE id IN (1,3,4,6,7,9,10,12)";
include("inc_db_con.php");
$sval = 0;
	while($row = mysql_fetch_assoc($rs)) {
		$id = $row['id'];
		switch($id) {
			case 1: case 4: case 7: case 10:
				$sval = $row['sval'];
				break;
			case 3: case 6: case 9: case 12:
				$time[$id] = $row;
				$time[$id]['sval'] = $sval;
				break;
		}
	}
mysql_close($con);
$lists = array();
$sql = "SELECT id, value FROM ".$dbref."_list_gfs WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['gfs'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_idpgoal WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['idp'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_area WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['area'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value, numval FROM ".$dbref."_list_wards WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['wards'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_kpicalctype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$calctype[$row['code']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_munkpa WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['munkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_natkpa WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['natkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_targettype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$targettype[$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_tas WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['tas'][$row['id']] = $row;
	}
mysql_close($con);
?>
<style type=text/css>
.3 { background-color: #CC0001; }
.6 { background-color: #FE9900; }
.9 { background-color: #009900; }
.12 { background-color: #000099; }
.0 { background-color: #999999; }
.3b { background-color: #ffcccc; }
.6b { background-color: #ffeaca; }
.9b { background-color: #ceffde; }
.12b { background-color: #ccccff; }
.0b { background-color: #EEEEEE; }
</style>
<h1>SDBIP <?php echo($modtxt); ?>: View Top Level KPIs</h1>
<?php //print_r($lists); ?>
<?php include("inc_progressbar.php"); ?>
				<?php /********** LOAD GRAPH HERE ***************/ ?>
<script type=text/javascript>
	incProg(10);	//POST GRAPH
</script>
				<?php /*********** STYLES ****************/ ?>
<style type=text/css>
	.subtxt {
		color: #000000;
		background-color: #dddddd;
		font-weight: bold;
		font-size: 10pt;
		line-height: 16pt;
		padding: 5 5 5 10;
	}
</style>
				<?php /*********** GO TO AND DISPLAY OPTIONS ****************/ ?>
				<?php /*********** CREATE FREEZEFRAMES CONTAINER ****************/ ?>
<div>
<table cellpadding=3 cellspacing=0>
				<?php /*********** HEADINGS ****************/ ?>
<?php
$c = 1;
echo "<tr>";
	echo "<th rowspan=2>KPI ID</th>"; $c++;
	echo "<th rowspan=2 style=\"padding: 0 70 0 70\">KPI Name</th>";
	foreach($head as $h) {
		if($h['headfield']!="trcorrective") {
			echo "<th rowspan=2";
			switch($h['headfield']) {
				case "topunit":
				case "toprisk":
				case "idp":
					echo " style=\"padding: 0 70 0 70\""; 
					break;
				default:
					echo " style=\"padding: 0 10 0 10\""; 
					break;
			} 
			echo ">".$h['headlist']."</th>"; $c++;
		}
	}
	foreach($time as $t) {
		if($t['sval']<$today) { $tspan = 5; } else { $tspan = 1; }
		echo "<th colspan=$tspan class=".$t['id'].">Quarter ending <u>".date("d M Y",$t['eval'])."</u></th>"; $c+=$tspan;
	}
	echo "<th colspan=3 class=0>Year-To-Date<br />(".date("d M Y").")</th>"; $c+=3;
	if(isset($head['trcorrective'])) { echo "<th rowspan=2 nowrap style=\"padding: 0 70 0 70\">Corrective Measures</th>"; $c++; }
echo "</tr>";
echo "<tr>";
	foreach($time as $t) {
		echo "<th class=".$t['id'].">Target</th>"; 
		if($t['sval']<$today) {
			echo "<th class=".$t['id'].">Actual</th>"; 
			echo "<th class=".$t['id'].">R</th>"; 
			echo "<th class=".$t['id'].">SDBIP Comment</th>"; 
			echo "<th class=".$t['id'].">Top Level Comment</th>"; 
		}
	}
	echo "<th class=0>Target</th>"; 
	echo "<th class=0>Actual</th>"; 
	echo "<th class=0>R</th>"; 
echo "</tr>";
?>
				<?php /*********** LIST KPIS ****************/ ?>
<?php
$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (SELECT topid FROM ".$dbref."_toplevel t, ".$dbref."_dir d WHERE t.topsubid = d.dirid AND t.topyn = 'Y' AND d.diryn = 'Y')";
include("inc_db_con.php");
$result = array();
	while($row = mysql_fetch_assoc($rs)) {
		$result[$row['trtopid']][$row['trtimeid']] = $row;
	}
mysql_close($con);
$tprog = 15;
	echo "<script type=text/javascript>incProg($tprog);</script>";
$subid = 0;
$sql = "SELECT t.*, d.dirid as subid, d.dirtxt as subtxt FROM ".$dbref."_toplevel t, ".$dbref."_dir d WHERE t.topsubid = d.dirid AND t.topyn = 'Y' AND d.diryn = 'Y' ORDER BY dirsort, topid";
include("inc_db_con.php");
$tmnr = mysql_num_rows($rs);
$tinc = 85/$tmnr;
while($top = mysql_fetch_assoc($rs))
{
	$tprog+=$tinc;
	echo "<script type=text/javascript>incProg($tprog);</script>";
	if($subid != $top['topsubid']) {
		echo "<tr>";
			echo "<td colspan=".$c." class=subtxt>".$top['subtxt']."</td>";
		echo "</tr>";
		$subid = $top['topsubid'];
	}
	$tt = $targettype[$top['toptargettypeid']]['code'];
	$topid = $top['topid'];
	echo "<tr>";
		echo "<td>".$top['topid']."</td>";
		echo "<td>".$top['topvalue']."</td>";
	foreach($head as $h) {
		$hf = $h['headfield'];
		if($hf != "trcorrective") {
			switch($hf) {
				case "munkpa":
				case "tas":
				case "natkpa":
					echo "<td style=\"text-align: center;\">";
					echo "<span style=\"text-decoration: underline;\" title='".$lists[$hf][$top['top'.$hf.'id']]['value']."'>".$lists[$hf][$top['top'.$hf.'id']]['code']."</span>";
					break;
				case "gfs":
					echo "<td>";
					echo $lists[$hf][$top['top'.$hf.'id']];
					break;
				case "idp":
					echo "<td>";
					$value = $lists[$hf][$top['top'.$hf.'id']];
					echo $value;
					break;
				case "toptargetannual":
				case "toptargetrevised":
					echo "<td style=\"text-align:right;\">";
					if($hf!="toptargetrevised" || $top[$hf]>0) {
						echo formatResult($tt,$top[$hf]);
					}
					break;
				case "wards":
					echo "<td>";
					$sql2 = "SELECT * FROM ".$dbref."_list_wards WHERE id IN (SELECT twwardsid FROM ".$dbref."_toplevel_wards WHERE twtopid = $topid AND twyn = 'Y') AND yn = 'Y' ORDER BY numval";
					include("inc_db_con2.php");
						while($w = mysql_fetch_assoc($rs2)) {
							echo $w['value'].";";
						}
					mysql_close($con2);
					break;
				case "area":
					echo "<td>";
					$sql2 = "SELECT * FROM ".$dbref."_list_area WHERE id IN (SELECT taareaid FROM ".$dbref."_toplevel_area WHERE tatopid = $topid AND tayn = 'Y') AND yn = 'Y' ORDER BY value";
					include("inc_db_con2.php");
						while($w = mysql_fetch_assoc($rs2)) {
							echo $w['value'].";";
						}
					mysql_close($con2);
					break;
				default:
					echo "<td>";
					echo $top[$hf];
					break;
			}
			echo "</td>";
		}
	}
	$ytdtar = 0;
	$ytdact = 0;
	$krtcount = 0;
	$kct = $top['topcalctype'];
	$corrective = array();
	foreach($time as $t) {
		$res = $result[$topid][$t['id']];
		$tar = $res['trtarget'];
		$act = $res['tractual'];
		// TARGET
		echo "<td style=\"text-align: right;\" class=".$t['id']."b>";
			echo formatResult($tt,$tar);
		echo "</td>";
		if($t['sval']<$today) {
			if($tar > 0) { $krtcount++; }
if($kct=="CO") {
	$ytdtar = ($tar>$ytdtar) ? $tar : $ytdtar;
	$ytdact = ($act>$ytdact) ? $act : $ytdact;
} else {
			$ytdtar += $tar;
			$ytdact += $act;
}
			// ACTUAL
			echo "<td style=\"text-align: right;\" class=".$t['id']."b>";
				echo formatResult($tt,$act);
			echo "</td>";
			// RESULT
			if($tar>0 || $act>0 || $kct == "ZERO" || $t['eval']<$today) {
			$krr = calcKR($kct,$tar,$act);
				echo "<td style=\"text-align: center;".$styler[$krr]."\">";
					echo $krr;
			} else {
				echo "<td>&nbsp;";
			}
			echo "</td>";
			// COMMENT
			echo "<td class=".$t['id']."b>";
				echo $res['trsdbipcomment'];
			echo "</td>";
			echo "<td class=".$t['id']."b>";
				echo $res['trcomment'];
			echo "</td>";
			if(isset($res['trcorrective']) && strlen($res['trcorrective'])>0) {
				$corrective[] = $res['trcorrective']." <small><i>[".date("d M Y",$t['eval'])."]</small></i>";
			}
		}
	}
				if(($kct == "STD" || $kct=="REV") && $krtcount > 0) {
					$ytdtar/=$krtcount;
					$ytdact/=$krtcount;
				}

	//YEAR TO DATE
		echo "<td style=\"text-align: right;\" class=0b>";
			echo formatResult($tt,$ytdtar);
		echo "</td>";
		echo "<td style=\"text-align: right;\" class=0b>";
			echo formatResult($tt,$ytdact);
		echo "</td>";
		$krr = calcKR($kct,$ytdtar,$ytdact);
		echo "<td style=\"text-align: center;".$styler[$krr]."\">";
			echo $krr;
		echo "</td>";
	//TOP COMMENT
	$pstyle = " style=\"margin: 0px 0px 0px 0px; padding: 0px 0px 5px 0px\"";
		echo "<td style=\"vertical-align: top\"><p $pstyle >".implode("</p><p $pstyle >",$corrective)."</p></td>";
	echo "</tr>";
}
mysql_close($con);
?>
				<?php /*********** END ****************/ ?>
</table>
</div>
<script type="text/javascript"> 
	//lockCol('tbl');
	incProg(100);
	hide();
</script>

