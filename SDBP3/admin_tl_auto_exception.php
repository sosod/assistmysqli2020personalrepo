<?php 
	include("inc_ignite.php"); 

/*****************************************************************/
/*** COMPANY SPECIFIC TOP LEVEL UPDATE FUNCTION ******************/
/*****************************************************************/

$cmpcode3 = $cmpcode;
$updatecount = 0;

	/*********** GET DISPLAY INFO *******************/
		$dir = array();
		$sql = "SELECT dirid, dirtxt FROM ".$dbref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				$dir[] = $row;
			}
		mysql_close($con);

/************** GET LISTS *********************/
/*$sql = "SELECT headlist, headfield FROM ".$dbref."_headings WHERE headtype = 'TL' AND headlistyn = 'Y' AND headfield <> 'topcalctype' AND headfield <> 'topvalue' ORDER BY headlistsort";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$head[$row['headfield']] = $row;
	}
mysql_close($con);*/
$head['assockpi'] = array('headlist'=>"Assoc. SDBIP KPIs",'headfield'=>"assockpi");
$head['topcalctype'] = array('headlist'=>"Calculation Type",'headfield'=>"topcalctype");
$head['topcalcaction'] = array('headlist'=>"Calculation Action",'headfield'=>"topcalcaction");
$head['toptargettypeid'] = array('headlist'=>"Target Type",'headfield'=>"toptargettypeid");

$sql = "SELECT id, sval, eval, activeKPI as active FROM ".$dbref."_list_time WHERE id IN (1,3,4,6,7,9,10,12)";
include("inc_db_con.php");
$sval = 0;
	while($row = mysql_fetch_assoc($rs)) {
		$id = $row['id'];
		switch($id) {
			case 1: case 4: case 7: case 10:
				$sval = $row['sval'];
				break;
			case 3: case 6: case 9: case 12:
				$time[$id] = $row;
				$time[$id]['sval'] = $sval;
				$time[$id]['active'] = "";
				break;
		}
	}
mysql_close($con);
$sql = "SELECT id, activeKPI as active FROM ".$dbref."_list_time";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$id = $row['id'];
		$i = ceil($id/3);
		$i*=3;
		$time[$i]['active'].=strtoupper($row['active']);
	}
mysql_close($con);

$lists = array();
$sql = "SELECT id, value FROM ".$dbref."_list_gfs WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['gfs'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_idpgoal WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['idp'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_area WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['area'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value, numval FROM ".$dbref."_list_wards WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['wards'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_kpicalctype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$calctype[$row['code']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_munkpa WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['munkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_natkpa WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['natkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_targettype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$targettype[$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_tas WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['tas'][$row['id']] = $row;
	}
mysql_close($con);

$kpimsr = array();
$sql = "SELECT kpiid, kpimsr, kpicalctype, kpitargettypeid FROM ".$dbref."_kpi WHERE kpiyn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$kpimsr[$row['kpimsr']][$row['kpiid']] = $row;
	}
mysql_close($con);

$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Ignite4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"/default.css\" type=\"text/css\">";
$echo.= "<style type=text/css>";
$echo.= "input { text-align: right; }";
$echo.= "table th { background-color: #CC0001; }";
$echo.= ".kpi { font-weight: normal; font-style: italic; text-align: right;}";
$echo.= ".kpitxt { color: #777777; }";
$echo.= "</style>";
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";

$echo.= "<h1>SDBIP $modtxt: Admin ~ Top Level: Automatic Update Exception Report</h1>";
$c = 0;

$echo.= "<table cellpadding=3 cellspacing=0>";
/*********** HEADINGS ****************/ 
//$c = 1;
$echo.= chr(10)."<tr>";
	$echo.= "<th>TL ID</th>"; $c++;
	$echo.= "<th style=\"padding: 0 70 0 70\">KPI Name</th>"; $c++;
	foreach($head as $h) {
		$echo.= "<th";
		switch($h['headfield']) {
			case "topunit":
			case "toprisk":
			case "idp":
			case "gfs":
				$echo.= " style=\"padding: 0 70 0 70\""; 
				break;
			default:
				$echo.= " style=\"padding: 0 10 0 10\""; 
				break;
		} 
		$echo.= ">".$h['headlist']."</th>"; $c++;
	}
	$echo.= "<th class=".$t['id'].">Result</th>"; $c++;
$echo.= "</tr>";
				/*********** LIST KPIS ****************/ 
$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (SELECT topid FROM ".$dbref."_toplevel WHERE topyn = 'Y')";
include("inc_db_con.php");
$result = array();
	while($row = mysql_fetch_assoc($rs)) {
		$result[$row['trtopid']][$row['trtimeid']] = $row;
	}
mysql_close($con);
$cspan = $c;
foreach($dir as $sub) {
	$subid = $sub['dirid'];
	$echo.= chr(10)."<tr><td colspan=$cspan style=\"color: #000000; font-weight: bold; background-color: #DEDEDE; text-align: left; height: 30;\">".$sub['dirtxt']."</td></tr>";

$sql = "SELECT t.* FROM ".$dbref."_toplevel t WHERE t.topsubid = $subid AND t.topyn = 'Y' ORDER BY topid";
include("inc_db_con.php");
$tmnr = mysql_num_rows($rs);
while($top = mysql_fetch_assoc($rs))
{
	$tt = $targettype[$top['toptargettypeid']]['code'];
	$topid = $top['topid'];
	$kct = $top['topcalctype'];
	//$rowcount = checkIntRef($kpimsr[$topid]) ? $kpimsr[$topid] : 1;
	$echo.= chr(10)."<tr>";
		$echo.= "<td>".$top['topid']."</td>";
		$echo.= "<td>".$top['topvalue']."</td>";
	foreach($head as $h) {
		$hf = $h['headfield'];
			switch($hf) {
				case "topcalctype":
					$echo.= "<td style=\"text-align: center;\">";
					$echo.=$calctype[$top[$hf]]['value'];
					break;
				case "topcalcaction":
					$echo.= "<td style=\"text-align: center;\">";
					switch($top[$hf]) {
						case "AVE":
							$echo.= "Average";
							break;
						default:
							$echo.= "Sum";
							break;
					}
					break;
				case "toptargettypeid":
					$echo.= "<td style=\"text-align: center;\">";
					$echo.=$targettype[$top[$hf]]['code'];
					break;
				case "assockpi":
					$echo.= "<td style=\"text-align: center;\">";
					$r = $kpimsr[$topid];
					$kassoc = count($r);
					$echo.=$kassoc;
					break;
				case "munkpa":
				case "tas":
				case "natkpa":
					$echo.= "<td style=\"text-align: center;\">";
					$echo.= "<span style=\"text-decoration: underline;\" title='".$lists[$hf][$top['top'.$hf.'id']]['value']."'>".$lists[$hf][$top['top'.$hf.'id']]['code']."</span>";
					break;
				case "gfs":
					$echo.= "<td>";
					$echo.= $lists[$hf][$top['top'.$hf.'id']];
					break;
				case "idp":
					$echo.= "<td>";
					$value = $lists[$hf][$top['top'.$hf.'id']];
					$echo.= $value;
					break;
				case "wards":
					$echo.= "<td>";
					$sql2 = "SELECT * FROM ".$dbref."_list_wards WHERE id IN (SELECT twwardsid FROM ".$dbref."_toplevel_wards WHERE twtopid = $topid AND twyn = 'Y') AND yn = 'Y' ORDER BY numval";
					include("inc_db_con2.php");
						while($w = mysql_fetch_assoc($rs2)) {
							$echo.= $w['value'].";";
						}
					mysql_close($con2);
					break;
				case "area":
					$echo.= "<td>";
					$sql2 = "SELECT * FROM ".$dbref."_list_area WHERE id IN (SELECT taareaid FROM ".$dbref."_toplevel_area WHERE tatopid = $topid AND tayn = 'Y') AND yn = 'Y' ORDER BY value";
					include("inc_db_con2.php");
						while($w = mysql_fetch_assoc($rs2)) {
							$echo.= $w['value'].";";
						}
					mysql_close($con2);
					break;
				default:
					$echo.= "<td>";
					$echo.= $top[$hf];
					break;
			}
		$echo.= "</td>";
	}
	$ytdtar = 0;
	$ytdact = 0;
	$toperr = "N";
	$error = array();
	if($kassoc==0) { $error[] = "No associated SDBIP KPIs."; }
	foreach($time as $t) {
		$res = $result[$topid][$t['id']];
		$auto = $res['trauto'];
		if($auto == "N") { $error[]= "Quarter ending ".date("d M Y",$t['eval'])." locked for manual update."; }
	}
	$kpi = $kpimsr[$topid];
	foreach($kpi as $k) {		//FOREACH KPI
		$krkct = $k['kpicalctype'];
		$krtt = $targettype[$k['kpitargettypeid']]['code'];
		if($krkct != $kct) {
			$error[] = "Type Mismatch: Calculation Type for KPI ".$k['kpiid'];
		}
		if($krtt != $tt) {
			$error[] = "Type Mismatch: Target Type for KPI ".$k['kpiid'];
		}
	}
	if(count($error)>0) {
		$echo.="<td style=\"color: #CC0001; font-weight: bold;\">".strFn("implode",$error,"<br />","")."</td>";
	} else {
		$echo.="<td style=\"color: #009900; font-weight: normal;\">No errors found - will update automatically.</td>";
	}
	$echo.= "</tr>";
	$kpitop = $kpimsr[$topid];
	foreach($kpitop as $kt2) {
		$kt = $kt2['kpiid'];
		$echo.= "<tr>";
			$echo.= "<td colspan=".(count($head)-2).">&nbsp;</td>";
			$echo.= "<td class=\"kpi kpitxt\" style=\"font-weight: bold;\">KPI ".$kt."</td>";
			$echo.= "<td class=\"kpi kpitxt\" style=\"text-align: center;\">".$calctype[$kt2['kpicalctype']]['value']."</td>";
			$echo.= "<td class=kpi>&nbsp;</td>";
			$echo.= "<td class=\"kpi kpitxt\" style=\"text-align: center;\">".$targettype[$kt2['kpitargettypeid']]['code']."</td>";
			$echo.= "<td>&nbsp;</td>";
		}
		$echo.=	"</tr>";
	
}
mysql_close($con);
}	//END FOREACH DIR AS SUB

				 /*********** END ****************/ 
$echo .= "</table>";
$echo .= "</body></html>";

//SAVE UPDATE RECORD
echo $echo;
/********************************/
/*** END TL UPDATE FUNCTION *****/