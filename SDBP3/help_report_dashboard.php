<?php
    include("inc_ignite.php");
	error_reporting(-1);

	$data_type = "KPI";
	$title_class = "charttitle";
	$mds = array('type'=>"M",'dir'=>"ALL",'sub'=>"ALL");
	$from = 1; $to = 1;
	$link = "N";
	$required = "Y";
	$layout_page = "ONS";
	$legend_layout = "W";
	$kpi = array(); 	$kpi_combined = array(); 	$kpi_na = array();
	$tot = 0; 	$tot_combined = 0; 	$tot_na = 0;
	$results = array(); 	$results_combined = array(); 	$results_na = array();
	foreach($krsetup as $krs) {
		$kpi[$krs['sort']] = 10;
		$tot+=10;
		$results[] = $krs['sort'];
		if($krs['sort']!=1) {
			$tot_na+=10;
			$results_na[] = $krs['sort'];
			$kpi_na[$krs['sort']] = 10;
		}
		if($krs['sort']>4) {
			if(!isset($kpi_combined[4])) { $kpi_combined[4] = 0; }
			$tot_combined+=10;
			$kpi_combined[4] +=10;
		} else {
			$tot_combined+=10;
			$kpi_combined[$krs['sort']] =10;
			$results_combined[] = $krs['sort'];
		}
	}
	$kpi['tot'] = $tot;
	$kpi_combined['tot'] = $tot_combined;
	$kpi_na['tot'] = $tot_na;

	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
a { text-decoration: none; }
.return { border: 0px; cursor: hand; vertical-align: middle; }
.u { text-decoration: underline; }
</style>
<base target="_self">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<a name=top></a><h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Help - Dashboard Report</b></h1>
<p><b>KPI Filters:</b> <a href=#kpina class=u>KPIs Not Measured Filter</a> | <a href=#kpimet class=u>KPIs Met</a> &nbsp; <b>Layout:</b> <a href=#layout_page_kpi class=u>Page Layout (KPIs)</a> | <a href=#layout_page_cf class=u>Page Layout (Cashflow)</a> | <a href=#layout_overall class=u>Primary Graph Layout</a></p>
<hr>
<a name=kpina></a><h2>KPI Filters - KPIs Not Measured Filter</h2>
<p>This filter allows you to choose whether to include/exclude KPIs Not Measured from the graph.<br />A KPI is determined to be "Not Measured" if it has no target or actual in the time period selected or the KPI Calculation Type is set to "N/A".</p>
<p><b>Example:</b> Assume that all reports are drawn in <u>August</u> and that the KPI is a Carry-Over KPI.</p>
<table cellpadding=3>
	<tr>
		<th colspan=3>July</th>
		<th colspan=3>August</th>
		<th colspan=3>September</th>
		<th colspan=3>Performance for the Period</th>
		<th rowspan=2>Graph Result</th>
	</tr>
	<tr>
		<?php for($i=1;$i<=4;$i++) {
			echo "<th>Target</th>";
			echo "<th>Actual</th>";
			echo "<th>R</th>";
			} ?>
	</tr>
	<tr>
		<td colspan=13><b>Example 1: Report drawn for July - August.</b></td>
	</tr>
	<tr>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td style="text-align: center">&nbsp;</td>
		<td style="text-align: center">&nbsp;</td>
		<td style="text-align: center">&nbsp;</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td>Not Measured</td>
	</tr>
	<tr>
		<td colspan=13><b>Example 2: Report drawn for July - September.</b></td>
	</tr>
	<tr>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[2]; ?>">R</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[2]; ?>">R</td>
		<td>Not Met</td>
	</tr>
	<tr>
		<td colspan=13><b>Example 3: Report drawn for July - August with an actual in August.</b></td>
	</tr>
	<tr>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center; <?php echo $styler[6]; ?>">B</td>
		<td style="text-align: center">&nbsp;</td>
		<td style="text-align: center">&nbsp;</td>
		<td style="text-align: center">&nbsp;</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center; <?php echo $styler[6]; ?>">B</td>
		<td>Extremely Well Met</td>
	</tr>
	<tr>
		<td colspan=13><b>Example 4: Report drawn for July - September with an actual in August.</b></td>
	</tr>
	<tr>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center; <?php echo $styler[1]; ?>">NA</td>
		<td style="text-align: center">0%</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center; <?php echo $styler[6]; ?>">B</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center; <?php echo $styler[4]; ?>">G</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center">10%</td>
		<td style="text-align: center; <?php echo $styler[4]; ?>">G</td>
		<td>Met</td>
	</tr>
</table>
<div align=center>
<table class=graph>
	<tr>
		<td class=graph style="vertical-align: top";><?php
			drawPie($data_type,$results,$kpi,"Include",$title_class,$layout_page,$legend_layout,$required,$link,$from,$to,$mds,"inc","X"); $required = "N";
		?></td>
		<td class=graph style="margin-left: 20px; margin-right: 20px; font-weight: bold; vertical-align: middle;"><img src=../pics/tri_right.gif style="vertical-align: middle"></td>
		<td class=graph><?php
			drawPie($data_type,$results_na,$kpi_na,"Exclude",$title_class,$layout_page,"W",$required,$link,$from,$to,$mds,"ex","X"); $required = "N";
		?></td>
	</tr>
</table>
</div>		
<p><a href=#top><img src=../pics/tri_up.gif class=return> Return to Top</a></p>
<hr>
<a name=kpimet></a><h2>Filters - KPIs Met</h2>
<p>This filter allows you to choose how to display KPIs where the result is greater than or equal to 100%.</p>
<div align=center>
<table class=graph>
	<tr>
		<td class=graph><?php
			drawPie($data_type,$results,$kpi,"Separate",$title_class,$layout_page,$legend_layout,$required,$link,$from,$to,$mds,"sep","X"); $required = "N";
		?></td>
		<td class=graph style="margin-left: 20px; margin-right: 20px; font-weight: bold; vertical-align: middle;"><img src=../pics/tri_right.gif style="vertical-align: middle"></td>
		<td class=graph><?php
			drawPie($data_type,$results_combined,$kpi_combined,"Combined",$title_class,$layout_page,$legend_layout,$required,$link,$from,$to,$mds,"com","X"); $required = "N";
		?></td>
	</tr>
</table>
</div>		
<p><a href=#top><img src=../pics/tri_up.gif class=return> Return to Top</a></p>
<hr>
<a name=layout_page_kpi></a><h2>Layout - Page Layout (KPIs)</h2>
<p>The Page Layout option allows you to choose how the graph should display depending on how you want to print the graphs.</p>
<p>Please note that the Page Layout option is only available if you select a "Group By" option.</p>
<p>To see the page layout options for Monthly Cashflows, please <a href=#layout_page_cf>click here</a>.</p>
<div align=center> 
<table>
	<tr>
		<td style="text-align: center; padding: 0 50 0 50;">
			<p class="charttitle fc">1 Graph Per Line</p>
			<table class=graph>
				<tr>
					<td class=graph>
						<p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_sml.jpg></p>
					</td>
				</tr>
				<tr>
					<td class=graph>
						<p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_sml.jpg></p>
					</td>
				</tr>
				<tr>
					<td class=graph>
						<p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_sml.jpg></p>
					</td>
				</tr>
			</table>
		</td>
		<td style="text-align: center; vertical-align: top; padding: 0 50 0 50;">
			<p class="charttitle fc">2 Graphs Per Line [Portrait]</p>
			<table class=graph>
				<tr>
					<td colspan=2 class=graph><p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 3<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 4<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=2 style="text-align: center; padding: 0 50 0 50;">
			<p class="charttitle fc">3 Graphs Per Line [Landscape]</p>
			<table class=graph>
				<tr>
					<td colspan=3 class=graph><p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 3<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 4<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 5<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 6<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
			</table>
		</td>
	</tr>
</table></div>
<p><a href=#top><img src=../pics/tri_up.gif class=return> Return to Top</a></p>
<hr>
<a name=layout_page_cf></a><h2>Layout - Page Layout (Cashflow)</h2>
<p>The Page Layout option allows you to choose how the graph should display depending on how you want to print the graphs.</p>
<p>Please note that the Page Layout option is only available if you select a "Group By" option.</p>
<p>To see the page layout options for Departmental and Top Level KPIs, please <a href=#layout_page_kpi>click here</a>.</p>
<div align=center> 
<table>
	<tr>
		<td style="text-align: center; padding: 0 50 0 50;">
			<p class="charttitle fc">1 Graph Per Line [Portrait]</p>
			<table class=graph>
				<tr>
					<td class=graph>
						<p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_cf_sml.jpg></p>
					</td>
				</tr>
				<tr>
					<td class=graph>
						<p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_cf_sml.jpg></p>
					</td>
				</tr>
				<tr>
					<td class=graph>
						<p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_cf_sml.jpg></p>
					</td>
				</tr>
			</table>
		</td>
		<td style="text-align: center; vertical-align: top; padding: 0 50 0 50;">
			<p class="charttitle fc">2 Graphs Per Line [Landscape]</p>
			<table class=graph>
				<tr>
					<td colspan=2 class=graph><p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_cf_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_cf_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_cf_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 3<br /><img src=../pics/help_chart_cf_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 4<br /><img src=../pics/help_chart_cf_sml.jpg></p></td>
				</tr>
			</table>
		</td>
	</tr>
</table></div>
<p><a href=#top><img src=../pics/tri_up.gif class=return> Return to Top</a></p>
<hr>
<a name=layout_overall></a><h2>Layout - Primary Graph Layout</h2>
<p>This option allows you to choose where to position the primary graph, again depending on how you want to print the graphs and how many secondary graphs there are.<br />
In certain circumstances choosing "Inline" rather than "Above" can help you save paper when printing the graphs.</p>
<div align=center>
<table>
	<tr>
		<td style="text-align: center; vertical-align: top; padding: 0 50 0 50;">
			<p class="charttitle fc">2 Graphs Per Line [Portrait] - Above</p>
			<table class=graph>
				<tr>
					<td colspan=2 class=graph><p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 3<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph >&nbsp;</td>
				</tr>
			</table>
		</td>
		<td style="text-align: center; vertical-align: top; padding: 0 50 0 50;">
			<p class="charttitle fc">2 Graphs Per Line [Portrait] - Inline</p>
			<table class=graph>
				<tr>
					<td class=graph><p class=charttitle>Primary Graph<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph><p class=charttitle2>Secondary Graph 1<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
				<tr>
					<td class=graph ><p class=charttitle2>Secondary Graph 2<br /><img src=../pics/help_chart_sml.jpg></p></td>
					<td class=graph ><p class=charttitle2>Secondary Graph 3<br /><img src=../pics/help_chart_sml.jpg></p></td>
				</tr>
			</table>
		</td>
	</tr>
</table></div>
<p><a href=#top><img src=../pics/tri_up.gif class=return> Return to Top</a></p>
<hr>
<p>&nbsp;</p>
</body>
</html>