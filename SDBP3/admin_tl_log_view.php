<?php 
	include("inc_ignite.php"); 

	$subid = $_REQUEST['subid'];
	$topid = $_REQUEST['topid'];
	
	$subarr = array();
	$sql = "SELECT DISTINCT dirid, dirtxt FROM ".$dbref."_toplevel, ".$dbref."_dir WHERE topyn != 'C' AND diryn = 'Y' AND dirid = topsubid ORDER BY dirsort";
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) {
			$subarr[] = $row;
		}
	mysql_close($con);
	
	if(!checkIntRef($subid)) { $subid = $subarr[0]['dirid']; }

	
/**************** GET LISTS ********************/
$sql = "SELECT id, sval, eval, activeKPI as active FROM ".$dbref."_list_time WHERE id IN (1,3,4,6,7,9,10,12)";
include("inc_db_con.php");
$sval = 0;
	while($row = mysql_fetch_assoc($rs)) {
		$id = $row['id'];
		switch($id) {
			case 1: case 4: case 7: case 10:
				$sval = $row['sval'];
				break;
			case 3: case 6: case 9: case 12:
				$time[$id] = $row;
				$time[$id]['sval'] = $sval;
				break;
		}
	}
mysql_close($con);
$sql = "SELECT headlist, headfield FROM ".$dbref."_headings WHERE headtype LIKE 'TL%' ORDER BY headlistsort";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$head[$row['headfield']] = $row;
	}
mysql_close($con);
if(!isset($head['trsdbipcomment'])) {
	$head['trsdbipcomment'] = array('headlist'=>"SDBIP Comment",'headfield'=>"trsdbipcomment");
}
$head['toptargetannual'] = isset($head['toptargetannual']) ? $head['toptargetannual'] : array('headlist'=>"Annual Target",'headfield'=>"toptargetannual");
$head['toptargetrevised'] = isset($head['toptargetrevised']) ? $head['toptargetrevised'] : array('headlist'=>"Revised Target",'headfield'=>"toptargetrevised");

$head['topcalcaction'] = isset($head['topcalcaction']) ? $head['topcalcaction'] : array('headlist'=>"Calculation Action",'headfield'=>"topcalcaction");
$head['toptargettypeid'] = isset($head['toptargettypeid']) ? $head['toptargettypeid'] : array('headlist'=>"Target type",'headfield'=>"toptargettypeid");
$head['assockpi'] = isset($head['assockpi']) ? $head['assockpi'] : array('headlist'=>"Assoc. SDBIP KPIs",'headfield'=>"assockpi");
$head['topsubid'] = isset($head['topsubid']) ? $head['topsubid'] : array('headlist'=>"Directorate",'headfield'=>"topsubid");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.logth { }
.logtd {  }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Admin ~ Top Level: Audit Log</h1>
<?php /************* SELECT KPI TO DISPLAY **************/ ?>
<form id=what action=admin_tl_log_view.php method=post>
<table cellpadding=10 cellspacing=0 style="margin-left: 10;">	<tr>
		<td>
			<select name=subid id=sid onchange="document.getElementById('what').submit();" style="margin-bottom: 2px;"><?php
				foreach($subarr as $s) {
					$sid = $s['dirid'];
					echo "<option ";
						echo $sid == $subid ? "selected " : "";
					echo "value=".$sid.">".$s['dirtxt']."</option>";
				}
			?></select>&nbsp;<img src="/pics/tri_right.gif" style="margin: 0 10 -2 10;" height=20>&nbsp;<select name=topid id=tid onchange="document.getElementById('what').submit();" style="margin-bottom: 2px;"><?php
$sql = "SELECT topid, topvalue FROM ".$dbref."_toplevel WHERE topsubid = $subid AND topyn != 'C' ORDER BY topid";
include("inc_db_con.php");
$t=0;
$err = "Y";
	while($top = mysql_fetch_assoc($rs)) {
		$tid = $top['topid'];
		if($t == 0) { $top0 = $tid; }
		$tval = substr(decode($top['topvalue']),0,60);
		$tval.= $tval != decode($top['topvalue']) ? "..." : "";
		echo "<option ";
			if((!checkIntRef($topid) && $t == 0) || $topid == $tid) { $topid = $tid; $topvalue = $tval; echo " selected "; $err = "N"; }
		echo "value=".$tid.">[".$tid."] ".$tval."</option>";
		$t++;
	}
mysql_close($con);
if($err == "Y") { $topid = $top0; }
			?></select>
		</td>
</tr>	</table>
</form>
<?php 
/*********** GET KPI DETAILS **************/
$sql = "SELECT * FROM ".$dbref."_toplevel WHERE topid = $topid";
include("inc_db_con.php");
	$top = mysql_fetch_assoc($rs);
mysql_close($con);
echo "<h2><span style=\"text-decoration: underline;\">KPI $topid: $topvalue</span></h2>";

/**************** DISPLAY LOG ****************/
include("inc_tl_log.php");

?>
</body>
</html>