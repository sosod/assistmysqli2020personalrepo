<?php
    include("inc_ignite.php");

	$administ = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."' AND type = 'FIN'";
    include("inc_db_con.php");
		$tladmin = mysql_num_rows($rs);
    mysql_close($con);
	
	$sql = "SHOW TABLE STATUS LIKE '".$dbref."_capital'";
	include("inc_db_con.php");
	$row = mysql_fetch_assoc($rs);
	mysql_close($con);
	$next_cpid = $row['Auto_increment'];
	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php 
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

include("inc_style.php"); ?>
<script type=text/javascript>
function Validate(me) {
	if(me.ifile.value.length > 0) {
		if(confirm("You are about to import Capital Projects for <?php echo strtoupper($cmpcode); ?>.\n\nAre you sure you wish to continue?")==true)
			return true;
	} else {
		alert("Please select the import file by clicking on the \"Browse\" button.");
	}
	return false;
}
function clearFin() {
	if(confirm("Please confirm - DELETE ALL EXISTING CAPITAL PROJECTS?")==true) {
		document.location.href = 'admin_cp_import_delete.php?type=fin';
	}
}
function clearLinks() {
	if(confirm("Please confirm - DELETE ALL LINKS BETWEEN CAPITAL PROJECTS AND KPIs?")==true) {
		document.location.href = 'admin_cp_import_delete.php?type=link';
	}
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Amend Capital Projects</b></h1>
<?php
displayResult($_REQUEST['r']);
if($tladmin ==0)
	die("<P>You are not authorised to view this page.</p>");
?>

<form name=import id=imp action=admin_cp_import_process.php method=post onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">
<input type=hidden name=act value=CHECK>
<ol>
	<li>DELETE ALL existing capital projects? <input type=button value=Delete onclick="clearFin()" /> <i>(not required)<br />
	This will not delete the links between capital projects and capital KPIs.</i></li>
	<li>DELETE ALL links between capital projects and capital KPIs? <input type=button value=Delete onclick="clearLinks()" /> <i>(not required)<br />
	This will move all Capital KPIs to Operational KPIs until you re-link the KPIs to a capital project.<br />
	To link an existing KPI to a Capital Project use the "Existing KPI" drop down available on the Add Capital Project KPI page.</i></li>
	<li>Ensure that all of the <a href=setup_lists.php>lists</a> include those list items in the Monthly Cashflow:
		<ul>
			<li><a href=setup_dir.php>Sub-Directorates</a></li>
			<li><a href="setup_lists_config.php?l=gfs">GFS Classification</a></li>
			<li><a href="setup_lists_config.php?l=fundsource">Funding Source</a></li>
			<li><a href="setup_lists_config.php?l=wards">Ward</a></li>
			<li><a href="setup_lists_config.php?l=area">Area</a></li>
		</ul>
	</li>
	<li>Generate an Import Template <input type=button value="  Go  " onclick="document.location.href = 'admin_cp_import_export.php';"></li>
	<li>Copy the Capital Projects data into the template.  Please note the following:
		<ul>
			<li>Keep the template in CSV format.</li>
			<li>Projects must start from Row 4.  The first 3 rows will be ignored.</li>
			<li>Either leave column A blank OR enter the Ignite Ref (the next assignable reference is <u><font color=red><?php echo $next_cpid; ?></u></font>) for all capital projects in the document.
			<br />If it is left blank, the system will automatically generate the Ignite Reference and store it in this column.
			<br />If you enter the Ignite Ref in column A the system will assign that number to the capital project as long as it is a number and is not duplicated.
			<br /><u>Use the drop down list below to indicate whether the system must auto generate the reference or use the reference in the import file.</u>			
			</li>
			<li>Required fields are marked with a *.  These fields are: Sub ID, GFS Classification, Project Name, Funding source, Planned Start, Planned End, Ward, Area.</li>
			<li>Note the formatting guidelines given in Row 3.<br />
			Reminder: Area & Funding Source are the IGNITE reference separated by a semi-colon (;) while the Wards is the MUNICIPAL reference separated by a semi-colon (;)<br />No other fields may contain more than 1 piece of information.</li>
		</ul>
	</li>
	<li>Import the updated template: <input type=file name=ifile id=ife> <select name=whatid><option selected value=auto>Auto assign references</option><option value=cola>Use references in column A</option></select> <input type=submit value=Import></li>
	<li>Review the data as imported by the system and click "Accept" to finalise the import.
		<br />Any errors will be highlighted in <span style="color: #CC0001">RED</span>.
		<br />Until all errors are attended to the "Accept" button will not be available.
		<br />Until the "Accept" button is clicked, the Capital Projects will not be made available on the system.
	</li>
</ol>
</form>
<p>&nbsp;</p>
</body>


</html>
