<?php
include("inc_ignite.php");
$modref = strtoupper($modref);

	$idarr = array();
	$idarr[] = 0;
	
$act = $_REQUEST['act'];
$whatid = isset($_REQUEST['whatid']) && strlen($_REQUEST['whatid'])>0 ? $_REQUEST['whatid'] : "auto";

$csql = array();
?>
<html>
<head>
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Ignite4u.co.za</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdhead2 { background-color: #efefef; text-align:center; }
.done { background-color: #009900; }
.ok { background-color: #000099; }
.err { background-color: #CC0001; color: #FFFFFF; }
.th1 { background-color: #000099; color: #FFFFFF; font-weight: bold; text-align: center; }
.th2 { background-color: #009900; color: #FFFFFF; font-weight: bold; text-align: center; }
.th3 { background-color: #FE9900; color: #FFFFFF; font-weight: bold; text-align: center; }
.th0 { background-color: #CC0000; color: #FFFFFF; font-weight: bold; text-align: center; }
.th1b { background-color: #aaaaff; font-weight: bold; text-align: center; }
.th2b { background-color: #aaffaa; font-weight: bold; text-align: center; }
.th3b { background-color: #ffc771; font-weight: bold; text-align: center; }
.th0b { background-color: #ffaaaa; font-weight: bold; text-align: center; }
.td1 { background-color: #ccccff; text-align: right; }
.td2 { background-color: #ccffcc; text-align: right; }
.td3 { background-color: #ffe5bb; text-align: right; }
.td0 { background-color: #ffdddd; text-align: right; }
table { border-color: #555555; }
table td { border-color: #555555; }
</style>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc>SDBIP <?php echo($modtxt); ?>: Admin ~ Amend Capital Projects [Import]</h1>
<?php 
if($act == "SAVE")
	echo "<h2>Finalise Import</h2>";
else
	echo "<h2>Validate Import</h2>";

//UPLOAD FILE
if($act!="SAVE") {
	if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
		switch($_FILES["ifile"]["error"])
		{
			case 2:
				echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
				break;
			case 4:
				echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
				break;
			default:
				echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
				break;
		}
		die();
	} else { //IF ERROR WITH UPLOAD FILE
		$ext = substr($_FILES['ifile']['name'],-3,3);
		if(strtolower($ext)!="csv") {
			echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
			die();
		} else {
			$filename = "CF-IMPORT_".substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
			$fileloc = "../files/".$cmpcode."/".$modref."/import/".$filename;
			//UPLOAD UPLOADED FILE
			set_time_limit(180);
			copy($_FILES["ifile"]["tmp_name"], $fileloc);
		}
	}
} else {	//act = save
	$fileloc = $_REQUEST['fileloc'];
}	//act=save

if(!file_exists($fileloc)) {
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    die();
}
	
//SETUP VARIABLES
if($whatid=="auto") {
	$sql = "SHOW TABLE STATUS LIKE '".$dbref."_capital'";
	include("inc_db_con.php");
		$row = mysql_fetch_array($rs);
		$new_topid = $row['Auto_increment'];
		//$new_topid+= $row['lineid'];
	mysql_close($con);
} else {
	$sql = "SELECT cpid FROM ".$dbref."_capital";
	include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$idarr[] = $row['cpid'];
	}
	mysql_close($con);
}
$sql = "SELECT subid, subtxt FROM ".$dbref."_dirsub WHERE subyn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$sub[$row['subid']] = $row['subtxt'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_gfs WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$gfs[$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_fundsource WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$fundsource[$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value, numval as code FROM ".$dbref."_list_wards WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		if(checkIntRef($row['code'])) {
			$wards[$row['code']] = $row;
		} else {
			$wards[strtolower($row['value'])] = $row;
		}
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_area WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$area[$row['id']] = $row['value'];
	}
mysql_close($con);

set_time_limit(180);
$file = fopen($fileloc,"r");
$data = array();
while(!feof($file)) {
	$tmpdata = fgetcsv($file);
	if(count($tmpdata)>1) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($file);
unset($data[0]);
unset($data[1]);
unset($data[2]);
$head = array();
//	$head[0] = array("","PMS Ref (IDP)","*DIR ID","Directorate","*GFS Vote","*National KPA","*TAS Key Focus Area","*IDP Goal","*Municipal Key Performance Area","*Key Performance Indicator","Unit of measurement","Risk","*Ward","*Area","Program Driver","Baseline (31/12/09)","*Calculation Type","*Calculation Action","*Target Type","*Annual Target","Revised Target","Qtr ending 30 Sep","Qtr ending 31 Dec","Qtr ending 31 March","Qtr ending 30 June","2011/12","2012/13","2013/14","2014/15","Comments");
//	$head[1] = array("","Text (50 characters)","ID as per CONSTANTS","Name as per CONSTANTS","ID as per LISTS","CODE as per LISTS","CODE as per LISTS","Text (200 characters)","CODE as per LISTS","Text (200 characters)","Text (200 characters)","Text (200 characters)","Mun. Ref as per CONSTANTS","Ignite Ref as per CONSTANTS","Text (150 characters)","Text (150 characters)","CODE as per LISTS","SUM or AVE","CODE as per LISTS","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","Text");
//$head[0] = array("","*SUB ID","Sub-Directorate","*Line Item","*GFS Classification","Vote Number","*Month/Time Period ID","Revenue Original Budget","Opex Original Budget","Capex Original Budget");
//$head[1] = array("","Ignite Ref","","200 characters","Ignite Ref","20 characters","Month name in full OR Time Period ID e.g. July = 1, August = 2 etc.","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY");
/*$head[0] = array("Line ID","*SUB ID","Sub-Directorate","*Line Item","*GFS Classification","Vote Number");
for($i=1;$i<=12;$i++) {
	$head[0][]= date("F",mktime(12,0,0,$i+6,1,2010));
	$head[0][] = "";
	$head[0][] = "";
}
$head[1] = array("","Ignite Ref","","200 characters","Ignite Ref","20 characters");
for($i=1;$i<=12;$i++) {
	$head[1][] = "Revenue Original Budget";
	$head[1][] = "Opex Original Budget";
	$head[1][] = "Capex Original Budget";
}*/
$head[0] = array("Ignite Ref","SUB ID*","Sub-Directorate","GFS Classification*","Municipal Capital Project Reference","IDP Number","Vote Number","Project name*","Project Description","Funding source*","Planned Start Date*","Planned Completion Date*","Actual Start Date","Actual Completion Date","Ward*","Area*","July 2010","August 2010","September 2010","October 2010","November 2010","December 2010","January 2011","February 2011","March 2011","April 2011","May 2011","June 2011","Total","2010/2011","","2011/2012","","2012/2013","","2013/2014","","2014/2015","");
//$head[1] = array("","","","","","","","","","","","","","","",""
$head[1] = array("Number","Ignite Ref","","Ignite Ref","40 characters","40 characters","40 characters","200 characters","200 characters","Ignite ref separated by ;","YYYY/MM/DD","YYYY/MM/DD","YYYY/MM/DD","YYYY/MM/DD","Mun. Ref separated by ;","Ignite ref separated by ;","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","Original Budget","","CRR","Other","CRR","Other","CRR","Other","CRR","Other","CRR","Other");

$cols = count($head[1]);
//CREATE FORM
?>
<form name=import id=imp action=admin_cp_import_process.php method=post>
<input type=hidden name=act value=SAVE><input type=hidden name=fileloc value="<?php echo $fileloc; ?>">
<input type=hidden name=whatid value="<?php echo $whatid; ?>">
<table cellpadding=3 cellspacing=0>
<?php
$r = 0;
	foreach($head as $d) {
		echo "<tr><td>&nbsp;</td>";
		switch($r) {
			case 0: 
				$class="tdheader"; 
				break;
			case 1:
				$class="tdhead2"; 
				break;
			default:
				$class="";
				break;
		}
		for($c=0;$c<$cols;$c++) {
			$t = $d[$c];
			$colspan=1;
			if($c>=16 && $r < 2) {
				$cl = "class=th";
				if($c >= 29) { 
					if((($c-15)%4)%2==1) { $cl.= (($c-15)%4-1); } else { $cl.= ($c-15)%4; }  
				} else { 
					$cl.=($c-15)%4; 
				}
				if($r==1) { $cl.="b"; } elseif($c>=29 && $r ==0) { $colspan=2; }
			} else {
				$cl = "class=$class";
			}
			echo "<td $cl nowrap colspan=$colspan >".$t."</td>";
			$c+=$colspan-1;
		}
		echo "</tr>";
		$r++;
	}
	$e = 0;
	foreach($data as $d) {
		set_time_limit(1800);
		$class = ""; $class1 = "";
		$csql = array(); $target = array(); $forecast = array(); $results = array(); $wardsid = array(); $areaid = array(); $fundsourceid = array(); $dates = array();
		$err = "N";
		$c = 0;
		for($c=0;$c<$cols;$c++) {
			$etxt = "";
			$t = trim($d[$c]);
			switch($c) {
				case 0:	// 0 = *Ignite ref
					if($whatid=="auto") {
						$t = $new_topid++;
					}
					if(!checkIntRef($t)) {
						$err = "Y";
						$etxt = "ERROR - Not a valid number";
					} elseif(is_numeric(array_search($t,$idarr))) {
						$err = "Y";
						$etxt = "ERROR - Duplicate";
					} else {
						$id = $t;
						$idarr[] = $t;
					}
					break;
				case 1:	// SUB ID
					if(!checkIntRef($t)) {
						$err = "Y";
						$etxt = "ERROR - Invalid ID";
					} elseif(!array_key_exists($t,$sub)) {
						$etxt = "ERROR - Invalid ID";
						$err = "Y";
					} else {
						$csub = $sub[$t];
						$etxt = "=> ".$csub;
						$subid = $t;
					}
					break;
				case 2:	// 2 = Sub-Directorate
					break;
				case 3:	// 3 = *GFS Vote
					if(!checkIntRef($t)) {
						$err = "Y";
						$etxt = "ERROR - Invalid ID";
					} elseif(!array_key_exists($t,$gfs)) {
						$etxt = "ERROR - Invalid ID";
						$err = "Y";
					} else {
						$csub = $gfs[$t];
						$etxt = "=> ".$csub;
						$gfsid = $t;
					}
					break;
				case 4:	// 4 = mun cp ref
				case 5: // 5 = idp
				case 6: // 6 = vote
					if(strlen($t)>20 || strlen(code($t)) > 100) {
						$err = "Y";
						$etxt = "ERROR - too long";
					} else {
						switch($c) {
							case 4: $cpnum = code($t); break;
							case 5: $cpidpnum = code($t); break;
							case 6: $cpvotenum = code($t); break;
						}
					}
					break;
				case 7:	// 7 = project name
				case 8: //8 = project description
					if(strlen($t)>200 || strlen(code($t)) > 250) {
						$err = "Y";
						$etxt = "ERROR - too long";
					} else {
						if(strlen($t)==0 && $c == 7) {
							$err = "Y";
							$etxt = "ERROR - Required field";
						} else {
							if($c==7) {
								$cpname = code($t);
							} else {
								$cpproject = code($t);
							}
						}
					}
					break;
				case 9: //funding source
					if(strlen($t)==0) {
						$err = "Y";
						$etxt = "ERROR - Required field";
					} else {
						$src = explode(";",$t);
						foreach($src as $t) {
							if(!checkIntRef($t)) {
								$err = "Y";
								$etxt = "ERROR - Invalid ID";
							} elseif(!array_key_exists($t,$fundsource)) {
								$etxt = "ERROR - Invalid ID";
								$err = "Y";
							} else {
								$csub = $fundsource[$t];
								$etxt.= "=> ".$csub;
								$fundsourceid[] = $t;
							}
							if($err == "Y") { break; }
						}
					}
					break;
				case 10: //planstart
				case 11: //plan end
				case 12: //actual start
				case 13: //actual end
					if($c<12 || strlen($t) > 0) {
						$dt = explode("/",$t);
						if(count($dt)!=3 || !checkIntRef($dt[0]) || !checkIntRef($dt[1]) || !checkIntRef($dt[2]) || $dt[1] > 12 || $dt[2] > 31) {
							$err = "Y";
							$etxt = "ERROR - Not a valid date";
						} else {
							$mk = mktime(12,0,0,$dt[1],$dt[2],$dt[0]);
							if($t!=date("Y/m/d",$mk)) {
								$err = "Y";
								$etxt = "ERROR - Invalid date";
							} else {
								$etxt = "=> ".date("d F Y",$mk);
								$dates[$c] = $mk;
							}
						}
					} else {
						$dates[$c] = 0;
					}
					break;
				case 14: //wards
					if(strlen($t)==0) {
						$err = "Y";
						$etxt = "ERROR - Required field";
					} else {
						$etxt = "";
						if(strpos("--".strtolower($t),"all")>0) {
							$etxt.= "=> All";
							$wardsid[] = $wards['all']['id'];
						} else {
							$src = explode(";",$t);
							foreach($src as $t) {
								if(strlen($t)>0) {
									if(!checkIntRef($t)) {
										$err = "Y";
										$etxt.= "ERROR - Invalid ID";
									} elseif(!array_key_exists($t,$wards)) {
										$etxt.= "ERROR - Invalid ID";
										$err = "Y";
									} else {
										$csub = $wards[$t]['value'];
										$etxt.= "=> ".$csub;
										$wardsid[] = $t;
									}
								}
								if($err == "Y") { break; }
							}
						}
					}
					break;
				case 15: //area
					if(strlen($t)==0) {
						$err = "Y";
						$etxt = "ERROR - Required field";
					} else {
						$src = explode(";",$t);
						foreach($src as $t) {
							if(!checkIntRef($t)) {
								$err = "Y";
								$etxt = "ERROR - Invalid ID";
							} elseif(!array_key_exists($t,$area)) {
								$etxt = "ERROR - Invalid ID";
								$err = "Y";
							} else {
								$csub = $area[$t];
								$etxt.= "=> ".$csub;
								$areaid[] = $t;
							}
							if($err == "Y") { break; }
						}
					}
					break;
				//monthly budgets
				case 16: case 17: case 18: case 19: case 20: case 21:
				case 22: case 23: case 24: case 25: case 26: case 27:
					if(strlen($t)>0 && !is_numeric($t)) {
						$err = "Y";
						$etxt = "ERROR - Not a valid number";
					} else {
						if(strlen($t)==0) {
							$t = 0;
						}
						$timeid = $c-15;
						$results[$timeid] = $t*1;
					}
					break;
				//total
				case 28:
					$t = array_sum($results);
					break;
				//forecast
				case 29: case 30: case 31: case 32: case 33:
				case 34: case 35: case 36: case 37: case 38:
					if(strlen($t)>0 && !is_numeric($t)) {
						$err = "Y";
						$etxt = "ERROR - Not a valid number";
					} else {
						if(strlen($t)==0) {
							$t = 0;
						}
						switch($c) {
							case 29: case 30: $foreid = 1; break;
							case 31: case 32: $foreid = 2; break;
							case 33: case 34: $foreid = 3; break;
							case 35: case 36: $foreid = 4; break;
							case 37: case 38: $foreid = 5; break;
						}
						if($c%2==0) {
							$fld = "cfother";
						} else {
							$fld = "cfcrr";
						}
						$forecast[$foreid][$fld] = $t*1;
					}
					break;
			}	//end switch
			//$d[$c].=strlen($etxt);
			if(strlen($etxt)>0) {
				$d[$c].= " ".$etxt;
			} elseif($c==0) {
				$d[$c] = $t." ".$etxt;
			} elseif($d[$c]!=$t || $t==0) {
				$d[$c] = $t;
			}
		}	//for
		if($err == "Y") {
			$e++;
			$class = "err";
			$class1 = $class;
			$results = array();
			$forecast = array();
			$dates = array();
			$csql = array();
		} else {
			//CREATE CSQL
			//project
			$csql[0] = "INSERT INTO ".$dbref."_capital (cpid,cpsubid,cpgfsid,cpnum,cpidpnum,cpvotenum,cpname,cpproject,cpstartdate,cpenddate,cpstartactual,cpendactual,cpyn,cpfundsourceid) VALUES ($id,$subid,$gfsid,'$cpnum','$cpidpnum','$cpvotenum','$cpname','$cpproject',".$dates[10].",".$dates[11].",".$dates[12].",".$dates[13].",'Y',0)";
			$logold = $csql[0];
			//fundsource
			$carr = array();
			foreach($fundsourceid as $s) {
				$carr[] = "(null,$id,$s,'Y')";
			}
			$csql[] = "INSERT INTO ".$dbref."_capital_source (csid,cscpid,cssrcid,csyn) VALUES ".implode(",",$carr);
			//wards
			$carr = array();
			foreach($wardsid as $s) {
				$carr[] = "(null,$id,$s,'Y')";
			}
			$csql[] = "INSERT INTO ".$dbref."_capital_wards (cwid,cwcpid,cwwardsid,cwyn) VALUES ".implode(",",$carr);
			//area
			$carr = array();
			foreach($areaid as $s) {
				$carr[] = "(null,$id,$s,'Y')";
			}
			$csql[] = "INSERT INTO ".$dbref."_capital_area (caid,cacpid,caareaid,cayn) VALUES ".implode(",",$carr);
			//results
			$carr = array();
			for($r=1;$r<=12;$r++) {
				$carr[] = "($id,".$results[$r].",$r)";
			}
			$csql[] = "INSERT INTO ".$dbref."_capital_results (crcpid,crbudget,crtimeid) VALUES ".implode(",",$carr);
			//forecast
			$carr = array();
			for($r=1;$r<=5;$r++) {
				$carr[] = "($id,".$forecast[$r]['cfcrr'].",".$forecast[$r]['cfother'].",$r)";
			}
			$csql[] = "INSERT INTO ".$dbref."_capital_forecast (cfcpid,cfcrr,cfother,cfyearid) VALUES ".implode(",",$carr);
			if($act == "SAVE") {
				//RUN CSQL
				foreach($csql as $sql) {
					//echo "<P>".$sql;
					include("inc_db_con.php");
				}
				$class = "done";
				$class1 = "";
			} else {
				$class = "ok";
				$class1 = "";
			}
		}
		echo "<tr><td class=\"$class\">&nbsp;</td>";
		$c = 0;
		for($c=0;$c<$cols;$c++)
		{
			if($c>=16 && is_numeric($d[$c])) {
				if($c>=29) {
					$class1 = "td".((($c-15)%4)-((($c-15)%4)%2));
				} else {
					$class1 = "td".(($c-15)%4);
				}
				$v = number_format($d[$c],2);
			} else {
				$v = $d[$c];
			}
			echo "<td class=\"$class1\" nowrap>".$v."</td>";
		}
		echo "</tr>";
		//ECHO SQL
		/*if($act != "SAVE") {
			echo "<tr><td>&nbsp;</td>";
			echo "<td colspan=$cols nowrap>";
			if($err == "N")
				arrPrint($csql);
			else
				echo "ERROR encountered.";
			echo "</td>";
			echo "</tr>";
		}*/
	}	//foreach $data
?>
</table>
<?php
	$sql = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,$today,'$tkid','$modref','Reimported Monthly Cashflow - $fileloc','','')";
if($act != "SAVE") {
	if($e>0) {
		echo "<P style=\"text-align:center;\">$e errors found.<br>Capital Projects cannot be imported.</p>";
	} else {
		echo "<p style=\"text-align:center;\"><input type=submit value=\"  Accept  \" style=\"color: #FFFFFF; background-color: #009900; padding: 10 10 10 10;\"></p>";
	}
} else {
	//include("inc_db_con.php");
	echo "<P style=\"text-align:center;font-weight: bold; color: #009900;\">Capital Projects imported.</p>";
}	
	
?>
</body></html>	