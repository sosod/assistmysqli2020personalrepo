<?php
    include("inc_ignite.php");
/* --- FUNCTION TO DRAW THE GRAPH LEGEND WITH LINKS TO KPI REPORT IF NEEDED --- */
function drawLegend($kpi) {
	global $dtype;
	global $did;
	global $wf;
	global $wt;
	global $layout;
	global $krsetup;
	global $referer;
	$krsetup2 = array_reverse($krsetup,true);
	$kc = ceil(count($krsetup2)/2);
	echo "<div align=center>";
	echo("<table cellpadding=2 cellspacing=0 style=\"border: 1px solid #ababab; margin: 0 0 0 0;\">");
		echo "<tr><td class=legend><table cellpadding=3 cellspacing=3 style=\"border-width: 0px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				$k++;
//				if($k<=$kc) {
					echo "<tr>";
					echo "<td class=legend width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
					echo "<td class=legend>".$krs['value']." (".$kpi[$krs['sort']]." of ".$kpi['tot'].")</td>";
					echo "</tr>";
//				}
			}
/*		echo "</table></td>";
		echo "<td class=legend><table cellpadding=3 cellspacing=3 style=\"border-width: 0px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				$k++;
				if($k>$kc) {
					echo "<tr>";
					echo "<td class=legend width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
					echo "<td class=legend>".$krs['value']." (".$kpi[$krs['sort']]." of ".$kpi['tot'].")</td>";
					echo "</tr>";
				}
			}*/
		echo "</table></td></tr>";
	echo("</table></div>");
}
/* --- END DRAWLEGEND FUNCTION --- */

	require("lib/AmPieChart.php");
	require("lib/AmBarChart.php");
	AmChart::$swfObjectPath = "lib/swfobject.js";
	AmChart::$libraryPath = "lib/amcharts";
	AmChart::$jsPath = "lib/AmCharts.js";
	AmChart::$jQueryPath = "lib/jquery.js";
	AmChart::$loadJQuery = true;

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table { border: 1px solid #FFFFFF; }
table td { border: 1px solid #FFFFFF; text-align: center; vertical-align: top; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
echo "<h1>SDBIP $modtxt</h1>";
/****** GET PERIOD DETAILS **********/
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sval < ".$today." ORDER BY sort DESC LIMIT 1";
include("inc_db_con.php");
	$period = mysql_fetch_array($rs);
mysql_close($con);
/****** GET SNAPSHOT DETAILS ********/
$sql = "SELECT * FROM ".$dbref."_snapshot WHERE type = 'K' AND period = 0 ORDER BY snap DESC LIMIT 1";
include("inc_db_con.php");
	$snap[0] = mysql_fetch_assoc($rs);
mysql_close($con);
/******* SET GRAPH DETAILS *************/
foreach($krsetup as $krs) {
	$s = $krs['sort'];
	$k = $snap[0]['kpi'.$s];
	$kpi0[$s] = !checkIntRef($k) ? 0 : $k * 1;
}
$budg0['r'] = (strlen($snap[0]['fin1'])>0 && is_numeric($snap[0]['fin1'])) ? $snap[0]['fin1'] * 1 : 0;
$budg0['o'] = (strlen($snap[0]['fin3'])>0 && is_numeric($snap[0]['fin3'])) ? $snap[0]['fin3'] * 1 : 0;
$budg0['c'] = (strlen($snap[0]['fin5'])>0 && is_numeric($snap[0]['fin5'])) ? $snap[0]['fin5'] * 1 : 0;
$actul0['r'] = (strlen($snap[0]['fin2'])>0 && is_numeric($snap[0]['fin2'])) ? $snap[0]['fin2'] * 1 : 0;
$actul0['o'] = (strlen($snap[0]['fin4'])>0 && is_numeric($snap[0]['fin4'])) ? $snap[0]['fin4'] * 1 : 0;
$actul0['c'] = (strlen($snap[0]['fin6'])>0 && is_numeric($snap[0]['fin6'])) ? $snap[0]['fin6'] * 1 : 0;

/********** DISPLAY GRAPHS *********/
echo "<table cellpadding=2 cellspacing=0 width=100% style=\"margin-top: -10px;\">";
	echo "<tr>";
		echo "<td colspan=2><h2 style=\"margin-top: -5px;\">Performance For The Year 2010/2011</h2>";
		echo "<p class=fc style=\"font-style: italic; font-size: 8pt; line-height: 9pt; margin-top: -9px; margin-bottom: 0px;\">as at <u>".date("d F Y H:i",$snap[0]['snap'])."</u></p>";
		echo "</td></tr><tr><td width=50%>";
			$kpi = $kpi0;
			$who = 0;
			include("inc_report_dashboard_kpi_snap.php");
			echo "</td><td width=50%>";
			$budg = $budg0;
			$actul = $actul0;
			include("inc_report_dashboard_fin_snap.php");
		echo "</td></tr>";
echo "</table>";
?>
</body>
</html>