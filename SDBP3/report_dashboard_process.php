<?php
ini_set('max_execution_time',1800); 
	include("inc_ignite.php");
//	error_reporting(-1);

function calcGrouping($group,&$group_max,&$values_maxlen) {
	global $group_graph;
		$g = 0;
		$h = 0;
		$group_limit = 7;
		$values_maxlen[0] = 0;
		$first_group = $group_limit;
		if(((count($group) % $group_limit) == floor(count($group) / $group_limit)) && (count($group)%$group_limit)>0) { 
			$group_limit++;
			$first_group++;
		} elseif(count($group)%$group_limit == 1 ) {
			$first_group++;
		}
		if(count($group)>$group_limit && $group_graph=="stack") {
			foreach($group as $grp) {
				$h++;
				if(($h > $group_limit && $g >0) || ($h > $first_group && $g==0)) { $g++; $h=1; $values_maxlen[$g] = 0; }
				$group_max[$g][] = $grp;
				if(strlen($grp['value'])>$values_maxlen[$g]) { $values_maxlen[$g] = strlen($grp['value']); }
			}
		} else {
			$group_max[$g] = $group;
			foreach($group as $grp) {
				if(strlen($grp['value'])>$values_maxlen[$g]) { $values_maxlen[$g] = strlen($grp['value']); }
			}
		}
}



	$var = $_REQUEST;
	$data_type = $var['data_type'];
	if(strlen($var['rhead'])>0) {
		$rhead = $var['rhead'];
	} else {
		switch($data_type) {
			case "TOP":
				$rhead = "SDBIP $modtxt: Top Level Dashboard Report";
				break;
			case "CF":
				$rhead = "SDBIP $modtxt: Monthly Cashflow Dashboard Report";
				break;
			case "KPI":
			default:
				$rhead = "SDBIP $modtxt: Departmental KPI Dashboard Report";
				break;
		}
	}
	$groupby = $var['groupby'];
	if(isset($var['layout_overall'])) { 
		$layout_overall = $var['layout_overall'];
	} else {
		$layout_overall = "above";
	}
	if(isset($var['layout_page'])) {
		$layout_page = $var['layout_page'];
	} else {
		$layout_page = 1;
	}
	$legend_layout = "T";
	if($layout_page==3) { $legend_layout = "W"; }
	$to = $var['to'];
	$from = $var['from'];
	$dirsub = $var['dirsub'];
	if($dirsub == "ALL") {										//municipality
		$mds['type'] = "M";
		$mds['dir'] = "ALL";
		$mds['sub'] = "ALL";
	} elseif (checkIntRef($dirsub)) {	//sub-dir
		$mds['type'] = "S";
		$mds['dir'] = "ALL";
		$mds['sub'] = $dirsub;
	} else {													//dir
		if($data_type=="TOP") { $id = "sub"; $id2 = "dir"; } else { $id = "dir"; $id2 = "sub"; }
		$mds['type'] = "D";
		$mds[$id] = substr($dirsub,2,strlen($dirsub));
		$mds[$id2] = "ALL";
	}
	$graph = $var['graph'];
	if($data_type == "CF") {
	} else {
		$kpimet = $var['kpimet'];
		$kpifilter = $var['kpifilter'];
		$results = array();
		if($kpifilter!="exclude") {
			$results[] = 1;
		}
		if($kpimet == "combine") { $r = 4; } else { $r = 6; }
		for($k=2;$k<=$r;$k++) { $results[] = $k; }
		$group_graph = isset($var['group_graph']) ? $var['group_graph'] : "PIE";
		if($data_type == "KPI") {
			$cpfilter = isset($var['cpfilter']) ? $var['cpfilter'] : "ALL";
		}
	}

$time = array();
$sql = "SELECT id, eval FROM ".$dbref."_list_time WHERE id IN ($from,$to)";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$time[$row['id']] = $row;
	}
mysql_close($con);
$blurb = "";
if($from==$to) {
	$blurb = "<br />for the month of ".date("F Y",$time[$from]['eval']);
} else {
	$blurb = "<br />for the months of ".date("F Y",$time[$from]['eval'])." to ".date("F Y",$time[$to]['eval']);
}
	
	
	
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
h1 { text-align: center; margin: 5 0 5 0; }
h2 { text-align: center; margin: 5 0 5 0; }
</style>
<base target="_self">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
echo "<h1>$cmpname</h1>";
echo "<h2>$rhead</h2>";
//echo "<pre>"; print_r($var); echo "</pre>";
/***** GET INFORMATION *****/
switch($data_type) {
	case "TOP":
		include("report_dashboard_top.php");
		break;
	case "CF":
		include("report_dashboard_cashflow.php");
		break;
	case "KPI":
	default:
		include("report_dashboard_kpi.php");
		break;
}

/***** DISPLAY GRAPH *****/
switch($graph) {
	case "CF_BAR":
		$required = "Y";
		echo "<p style=\"text-align: center; font-style: italic;margin-top: 0px;\">Report drawn on ".date("d F Y")." at ".date("H:i").$blurb.".</p>";
		echo "<div align=center><table class=graph >";
		echo "<tr style=\"page-break-after: always;\">";
		$colspan = 1;
		if($layout_overall=="above") { $colspan = $layout_page; }
		echo "<td class=graph colspan=$colspan>";
			$c = $colspan;
			$r = 0;
			drawFinBar($data_type,$sum[0],$overall_title,"charttitle",$layout_page,$required,"ALL"); $required = "N";
		echo "</td>";
		if($groupby != "X") {
			foreach($group as $g) {
				if(isset($sum[$g['id']])) {
					if($c == $layout_page) {
						$r++;
						echo "</tr><tr";
						if($r==2) {
							echo " style=\"page-break-after: always;\"";
							$r = 0;
						}
						echo ">";
						$c = 0;
					}
					echo "<td class=graph >";
						drawFinBar($data_type,$sum[$g['id']],$g['value'],"charttitle2",$layout_page,$required,$g['id']); $required = "N";
					echo "</td>";
					$c++;
				}
			}
		}
		echo "</tr></table></div>";
		break;
	case "PIE":
	default:
		$required = "Y";
		$link = "Y";
		if($kpimet=="combine") { $link = "N"; }
		//drawPie($results,$kpi,$chart_title,$layout,$legend_layout,$required,$link,$from,$to,$dir,$sub,$graph_ref,$groupby);
		echo "<p style=\"text-align: center; font-style: italic; margin-top: 0px; font-size: 6.5pt; line-height: 8pt;\">Report drawn on ".date("d F Y")." at ".date("H:i").$blurb.".</p>";
		echo "<div align=center><table class=graph>";
		echo "<tr>";
		$colspan = 1;
		$colspan2 = 1;
		if($layout_overall=="above" && $group_graph != "stack") { $colspan = $layout_page; }
		if($layout_overall!="above" && $group_graph == "stack") { $colspan2 = 2; }
		echo "<td class=graph colspan=$colspan>"; //print_r($group_max);
			$c = $colspan;
			$r=1;
			drawPie($data_type,$results,$kpi[0],$overall_title,"charttitle",$layout_page,$legend_layout,$required,$link,$from,$to,$mds,'ALL',$groupby);
			$required = "N";
		echo "</td>";//print_r($values_maxlen);
		if($groupby != "X") {
			if($group_graph == "stack") {
				$required = "Y";
				for($i=0;$i<count($group_max);$i++) {
					if($layout_overall == "above" || $i > 0) {
						$r++;
						echo "</tr><tr";
						if($r == 2 || $layout_overall!="above") {
							echo " style=\"page-break-after: always;\"";
							$r = 0;
						}
						echo ">";
					}
					echo "<td class=graph ";
						if($i>0) {
							echo "colspan=$colspan2 ";
						}
					echo "style=\"text-align: center;\">";
						drawStackedBar($data_type,$results,$kpi,$group_title,"charttitle2",$layout_page,$legend_layout,$required,$link,$from,$to,$mds,$groupby."_".$i,$group_max[$i],$values_maxlen[$i]);
						$required = "N";
					echo "</td>";
				}
			} else {
				$group = $group_max[0];
				foreach($group as $g) {
					if(isset($kpi[$g['id']])) {
						if($c == $layout_page) {
							$r++;
							echo "</tr><tr";
							if($r==2) {
								echo " style=\"page-break-after: always;\"";
								$r = 0;
							}
							echo ">";
							$c = 0;
						}
						echo "<td class=graph>";
							drawPie($data_type,$results,$kpi[$g['id']],$g['value'],"charttitle2",$layout_page,$legend_layout,$required,$link,$from,$to,$g['mds'],$g['id'],$groupby);
						echo "</td>";
						$c++;
					}
				}
			}
		}
		echo "</tr></table></div>";
		break;
}


?>
</body>
</html>