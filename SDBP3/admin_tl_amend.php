<?php
    include("inc_ignite.php");

	$administ = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."' AND type = 'TL'";
    include("inc_db_con.php");
		$tladmin = mysql_num_rows($rs);
    mysql_close($con);
	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<script type=text/javascript>
function Validate(me) {
//return false;
	if(me.ifile.value.length > 0) {
		if(confirm("You are about to amend Top Level KPIs for <?php echo strtoupper($cmpcode); ?>.\n\nAre you sure you wish to continue?")==true)
			return true;
	} else {
		alert("Please select the import file by clicking on the \"Browse\" button.");
	}
	return false;
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Top Level: Amend</b></h1>
<?php
if($tladmin ==0)
	die("<P>You are not authorised to view this page.</p>");
?>
<form name=import id=imp action=admin_tl_amend_process.php method=post onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">
<input type=hidden name=act value=CHECK>
<ol>
	<li>Generate a Top Level Template <input type=button value="  Go  " onclick="document.location.href = 'admin_tl_amend_export.php';"></li>
	<li>Copy the Top Level data into the template.  Please note the following:
		<ul>
			<li>Keep the template in CSV format.</li>
			<li>Top Level KPIs must start from Row 3.  The first 2 rows will be ignored.</li>
			<li>Note the formatting guidelines given in Row 2.</li>
			<li>DO NOT make any changes to the information in columns A to D - this information will be used to validate the import.</li>
		</ul>
	</li>
	<li>Import the updated template: <input type=file name=ifile id=ife> <input type=submit value=Import id=impbut></li>
	<li>Review the data as imported by the system and click "Accept" to finalise the import.<br />Any KPIs with import errors will be highlighted in <span style="color: #CC0001">RED</span>.  Until all errors are attended to the "Accept" button will not be available.<br />Until the "Accept" button is clicked, the changes will not be made available on the system.</li>
</ol>
</form>
<script type=text/javascript>
//document.getElementById('impbut').disabled = true;
</script>
<p>&nbsp;</p>
</body>


</html>
