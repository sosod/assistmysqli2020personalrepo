<style type=text/css>
.legend {
	font: Tahoma;
	font-size: 6.5pt;
	line-height: 7.5pt;
	border-color: #ffffff;
	text-align: left;
}
.charttitle {
	font-weight: bold;
	font-size: 13pt;
	font:Verdana; 
	text-decoration:underline;
	margin-top: 10pt;
}
</style>
<?php

$layout = $_REQUEST['layout'];
if($layout != "P" && $layout != "L") { $layout = "ONS"; }
		$cwidth = 300;
		$cheight = 300;
		$cradius = 100;
		$dlabel = "-20%";
switch($layout) {
	case "L":
		$tdl = 3;
		$cwidth = 250;
		$cheight = 200;
		$cradius = 75;
		break;
	case "P":
		$tdl = 2;
		break;
	default:
		$cwdith = 400;
		$cradius = 120;
		$dlabel = "-20%";
		$tdl = 1;
		$layout = "ONS";
		break;
}
//echo $_SERVER['PHP_SELF'];
//echo strpos($_SERVER['PHP_SELF'],"report_dashboard");
if(strlen($referer)>0) { } else { $referer = "."; }


/* --- CALCULATE THE NUMBER OF RESULTS PER CATEGORY --- */
$kpi['tot'] = 0;
$kpitot = 0 + array_sum($kpi);
$kpi['tot'] = $kpitot;

/* --- START THE GRAPH GENERATION --- */
//MAIN GRAPH
if($kpi['tot'] > 0)
{
	$chart = new AmPieChart("kpi_".$who);
	foreach($krsetup as $krs) {
		$chart->addSlice($krs['sort'], code($krs['value'])." (".$kpi[$krs['sort']]." of ".$kpi['tot'].")", $kpi[$krs['sort']], array("color" => $krs['color']));
	}
	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" =>$cheight,
		"font" => "Tahoma",
		"decimals_separator" => ".",
		"pie.y" => "49%",
		"pie.radius" => $cradius,
		"pie.height" => 5,
		"background.border_alpha" => 0,
		"data_labels.radius" => $dlabel,
		"data_labels.text_color" => "0xFFFFFF",
		"data_labels.show" => "<![CDATA[{percents}%]]>",
		"legend.enabled" => "false",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
	));
	echo html_entity_decode($chart->getCode());
	drawLegend($kpi);
} else {
	echo("<p class=chartitle>".$titletxt."</p>");
	echo("<p>No KPIs to display.</p>");
}

?>