<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>


<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin - Capital Projects ~ Add</b></h1>
<p>&nbsp;</p>
<?php
//GET DETAILS
$vars = $_REQUEST;
$gfsid = $vars['gfs'];
$fsid = $vars['fundsource'];
$sactual = $vars['startactual'];
$eactual = $vars['endactual'];

$subid = $_POST['subid'];
$cpproject = htmlentities($_POST['cpproject'],ENT_QUOTES,"ISO-8859-1");
$cpname = htmlentities($_POST['cpname'],ENT_QUOTES,"ISO-8859-1");
$cpnum = htmlentities($_POST['cpnum'],ENT_QUOTES,"ISO-8859-1");
$cpvotenum = htmlentities($_POST['cpvotenum'],ENT_QUOTES,"ISO-8859-1");
$cpidpnum = htmlentities($_POST['cpidpnum'],ENT_QUOTES,"ISO-8859-1");
$wards = $_POST['wards'];
$area = $_POST['area'];
$sdate = $_POST['cpstartdate'];
$edate = $_POST['cpenddate'];

$sdate2 = explode("_",$sdate);
$edate2 = explode("_",$edate);
$cpstartdate = mktime(0,0,1,$sdate2[1],$sdate2[0],$sdate2[2]);
$cpenddate = mktime(0,0,1,$edate2[1],$edate2[0],$edate2[2]);

if(strlen($sactual)>0) {
    $sactual2 = explode("_",$sactual);
    $cpstartactual = mktime(0,0,1,$sactual2[1],$sactual2[0],$sactual2[2]);
} else {
    $cpstartactual = 0;
}
if(strlen($eactual)>0) {
    $eactual2 = explode("_",$eactual);
    $cpendactual = mktime(0,0,1,$eactual2[1],$eactual2[0],$eactual2[2]);
} else {
    $cpendactual = 0;
}

//CHECK SUB
if(is_numeric($subid) && $subid > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$subid." AND subyn = 'Y'";
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
    mysql_close();
}

    $time = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        while($row = mysql_fetch_array($rs))
        {
            $time[$t] = $row;
            $t++;
        }
    mysql_close();
    $budget = array();
    $b=0;
    foreach($time as $tarr)
    {
        $fld = "tid".$tarr['id'];
        $var = $_POST[$fld];
        $budget[$b]['budget'] = $var;
        $budget[$b]['id'] = $tarr['id'];
        $b++;
    }
    $forecast = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_forecastyears WHERE yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
        $t = 0;
        while($row = mysql_fetch_array($rs))
        {
            $forecast[$t] = $row;
            $t++;
        }
    mysql_close();
    $fbudget = array();
    $b=0;
    foreach($forecast as $farr)
    {
        $fld = "fidcrr".$farr['id'];
        $var = $_POST[$fld];
        $fbudget[$b]['crr'] = $var;
        $fbudget[$b]['id'] = $farr['id'];
        $fld = "fidother".$farr['id'];
        $var = $_POST[$fld];
        $fbudget[$b]['other'] = $var;
        $b++;
    }


//CHECK DETAILS
if(strlen($cpproject)>0 && $mnr>0 && $cpstartdate > 0 && $cpenddate > 0 && $cpenddate > $cpstartdate && checkIntRef($gfsid) && checkIntRef($fsid))
{
    //INSERT
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital SET";
    $sql.= "  cpnum = '".$cpnum."'";
    $sql.= ", cpsubid = ".$subid;
    $sql.= ", cpgfsid = ".$gfsid;
    $sql.= ", cpvotenum = '".$cpvotenum."'";
    $sql.= ", cpidpnum = '".$cpidpnum."'";
    $sql.= ", cpname = '".$cpname."'";
    $sql.= ", cpproject = '".$cpproject."'";
    $sql.= ", cpfundsourceid = '".$fsid."'";
    $sql.= ", cpstartdate = ".$cpstartdate;
    $sql.= ", cpenddate = ".$cpenddate;
    $sql.= ", cpstartactual = ".$cpstartactual;
    $sql.= ", cpendactual = ".$cpendactual;
    $sql.= ", cpyn = 'Y'";
    include("inc_db_con.php");
    //GET ID
    $cpid = 0;
    $cpid = mysql_insert_id($con);
        $tsql = $sql;
        $told = "";
        $trans = "Inserted new CP ".$cpid;
        include("inc_transaction_log.php");
    //LOG
    if(checkIntRef($cpid))
    {
    //FOREACH BUDGET
        foreach($budget as $res)
        {
        //INSERT
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_results SET";
            $sql.= "  crcpid = ".$cpid;
            $sql.= ", crbudget = ".$res['budget'];
            $sql.= ", cractual = 0, craccpercytd = 0, crtotrandytd = 0";
            $sql.= ", crtimeid = ".$res['id'];
            include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Inserted Budget for new CP ".$cpid." with timeid ".$res['id'];
        include("inc_transaction_log.php");
        //LOG
        }
    //FOREACH FORCAST
        foreach($fbudget as $fres)
        {
            if(!checkIntRef($fres['crr'])) { $fres['crr'] = 0; }
            if(!checkIntRef($fres['other'])) { $fres['other'] = 0; }
        //INSERT
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_forecast SET";
            $sql.= "  cfcpid = ".$cpid;
            $sql.= ", cfcrr = ".$fres['crr'];
            $sql.= ", cfother = ".$fres['other'];
            $sql.= ", cfyearid = ".$fres['id'];
            include("inc_db_con.php");
        $tsql = $sql;
        $told = "";
        $trans = "Inserted Forecast Budget for new CP ".$cpid." with yearid ".$fres['id'];
        include("inc_transaction_log.php");
        //LOG
        }
        //WARDS
        if($wards[0]==1 || !checkIntRef($wards[0])) {
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_wards SET";
                $sql.= "  cwcpid = ".$cpid;
                $sql.= ", cwwardsid = 1";
                $sql.= ", cwyn = 'Y'";
                include("inc_db_con.php");
            $tsql = $sql;
            $told = "";
            $trans = "Added ward 1 to new CP ".$cpid;
            include("inc_transaction_log.php");
        } else {
            //FOREACH WARDS
            foreach($wards as $wrd)
            {
            //INSERT
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_wards SET";
                $sql.= "  cwcpid = ".$cpid;
                $sql.= ", cwwardsid = ".$wrd;
                $sql.= ", cwyn = 'Y'";
                include("inc_db_con.php");
            $tsql = $sql;
            $told = "";
            $trans = "Added ward ".$wrd." to new CP ".$cpid;
            include("inc_transaction_log.php");
            //LOG
            }
        }
        //AREA
        if($area[0]==1 || !checkIntRef($area[0])) {
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_area SET";
                $sql.= "  cacpid = ".$cpid;
                $sql.= ", caareaid = 1";
                $sql.= ", cayn = 'Y'";
                include("inc_db_con.php");
            $tsql = $sql;
            $told = "";
            $trans = "Added area 1 to new CP ".$cpid;
            include("inc_transaction_log.php");
        } else {
            //FOREACH WARDS
            foreach($area as $ara)
            {
            //INSERT
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_capital_area SET";
                $sql.= "  cacpid = ".$cpid;
                $sql.= ", caareaid = ".$ara;
                $sql.= ", cayn = 'Y'";
                include("inc_db_con.php");
            $tsql = $sql;
            $told = "";
            $trans = "Added area ".$wara." to new CP ".$cpid;
            include("inc_transaction_log.php");
            //LOG
            }
        }
    }

    echo("<p>Capital Project \"".$cpproject."\" successfully added with reference ".$cpid.".</p>");
    $urlback = "admin_fin.php";
    include("inc_goback.php");
}
else
{
    echo("<p>An error has occurred.  Please go back and try again.</p>");
    echo("<p style=\"margin-top: 30px\"><a href=# onclick=\"history.back();\"><img src=/pics/tri_left.gif align=absmiddle border=0><span style=\"text-decoration: none;\"> </span>Go Back</a></p>");
}

?>
<p>&nbsp;</p>
</body>
</html>
