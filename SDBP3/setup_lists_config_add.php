<?php
    include("inc_ignite.php");
	$munlists['idpgoal']= array('name'=>"IDP Objective",'id'=>"idpgoal",'code'=>"N",'len'=>150,'fld'=>"code");

    $listid = $_GET['l'];
    if(strlen($listid)==0) { killMe(""); }
    $list = $munlists[$listid];
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var cde = me.cde.value;
    var valid8 = "true";
    if(cde.length==0)
    {
        document.getElementById('cde').className = 'reqtext';
        document.getElementById('cde').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('cde').className = 'reqmet';
    }
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('val').className = 'reqmet';
    }
    if(document.getElementById('val').className == 'reqtext' || document.getElementById('cde').className == 'reqtext')
    {
        alert("Please complete the missing fields as indicated.");
        return false;
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - <?php echo($list['name']); ?> ~ Add</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=setup_lists_config_add_process.php onsubmit="return Validate(this);"><input type=hidden name=l value="<?php echo($listid); ?>">
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=110>Value:</td>
		<td class=tdgeneral valign=top><input type=text size=30 maxlength=<?php echo($list['len']); ?> name=val id=val> <span style="font-size: 7.5pt;">(max <?php echo($list['len']); ?> characters)</span></td>
		<td class=tdgeneral width=60>*required</td>
	</tr>
<?php if($list['code']=="Y") { ?>
    <tr>
		<td class=tdheaderl>Short Code**:</td>
		<td class=tdgeneral valign=top><input type=text size=10 maxlength=5 name=cde id=cde> <span style="font-size: 7.5pt;">(max 5 characters)</span></td>
		<td class=tdgeneral>*required</td>
	</tr>
<?php } ?>
    <tr>
		<td class=tdheaderl>&nbsp;</td>
		<td class=tdgeneral colspan=2 valign=top><input type=submit value=" Add "></td>
    </tr>
</table><?php if($list['code']=="Y") { ?>
<p>** Used on the list view of KPIs.</p>
<?php } else { echo("<input type=hidden value='1' name=cde id=cde"); } ?></form>
<?php
$urlback = "setup_lists_config.php?l=".$listid;
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
