<?php
/********* SET VARIABLES *********/
$kpi = array();
foreach($results as $krs) {
	$kpi[0][$krs] = 0;
}
$kpi[0]['tot'] = 0;
unset($dlist['prog']);
unset($dlist['kpistratop']);
unset($dlist['kpiriskrate']);
unset($dlist['ktype']);
unset($dlist['kpitargettype']);
$dlist['topcalctype']=array('name'=>"KPI Calculation Type",'id'=>"kpicalctype",'code'=>"Y",'yn'=>"Y",'headfield'=>"topcalctype",'kpi'=>"topcalctype",'fld'=>"code");
$dlist['toptargettype']=array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"toptargettype",'kpi'=>"toptargettypeid");
$dlist['idp']=array('name'=>"IDP Goal",'id'=>"idpgoal",'code'=>"N",'fld'=>"code",'yn'=>"Y",'headfield'=>"idp",'kpi'=>"topidpid");
$dlist['natkpa']['kpi'] = "topnatkpaid";
$dlist['munkpa']['kpi'] = "topmunkpaid";
$dlist['gfs']['kpi'] = "topgfsid";
$dlist['tas']['kpi'] = "toptasid";

//if($groupby!= "X" && $groupby != "dir") { $groupby = $dlist[$groupby]['kpi']; } 


/********* GET INFORMATION **************/
	/*** RESULTS INTO ARRAY ***/
	$result = array();
$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (SELECT topid FROM ".$dbref."_toplevel t, ".$dbref."_dir d WHERE t.topsubid = d.dirid AND t.topyn = 'Y' AND d.diryn = 'Y')";
include("inc_db_con.php");
$result = array();
	while($row = mysql_fetch_assoc($rs)) {
		$result[$row['trtopid']][$row['trtimeid']] = $row;
	}
mysql_close($con);
	/*** GET KPIS ***/
	$toprow = array();
//$sql = "SELECT t.topid, t.topsubid, t.topcalctype FROM ".$dbref."_toplevel t, ".$dbref."_dir d WHERE t.topsubid = d.dirid AND t.topyn = 'Y' AND d.diryn = 'Y' ORDER BY dirsort, topid";
$sql = "SELECT k.topid, k.topsubid dir, k.topcalctype ";
	if($groupby != "X" && $groupby!="dir") { $sql.= ", k.".$dlist[$groupby]['kpi']; }
	$sql.= " FROM ".$dbref."_toplevel k";
	$sql.= " INNER JOIN ".$dbref."_dir d ON d.dirid = k.topsubid ";
	foreach($dlist as $d) {
		$hf = $d['headfield'];
		if($hf!="wards" && $hf!="area") {
			$sql.= " INNER JOIN ".$dbref."_list_".$d['id']." ".$hf." ON ";
			switch($hf) {
				case "topcalctype":
					$sql.= $hf.".code = k.".$d['kpi'];
					break;
				default:
					$sql.= $hf.".id = k.".$d['kpi'];
			}
		}
	}
	$sql.= " WHERE k.topyn = 'Y' AND d.diryn = 'Y' AND k.topid IN (SELECT DISTINCT trtopid FROM ".$dbref."_toplevel_result) ";
	if($dirsub!="ALL") {
//		$dirsub2 = substr($dirsub,2,strlen($dirsub));
//		if(checkIntRef($dirsub2)) {
			$sql.= " AND d.dirid = ".$mds['sub'];
//		}
//		$sub = $dirsub2;
//	} else {
//		$sub = "ALL";
	}
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$toprow[$row['topid']] = $row;
	}
mysql_close($con);

/********* CALCULATE RESULTS ************/
foreach($toprow as $top) {
	$topid = $top['topid'];
	if($groupby=="X") {
		$moreid = "X";
	} elseif($groupby == "dir") {
		$moreid = $top['dir'];
	} else {
		$moreid = $top[$dlist[$groupby]['kpi']];
	}
	$kct = $top['topcalctype'];
	$ytdtar = 0;
	$ytdact = 0;
	$krtcount = 0;
	$tar = 0;
	$act = 0;
	if($kct=="ACC" || $kct == "CO") {
		$tfrom = 1;
	} else {
		$tfrom = $from;
	}
	for($t=$tfrom;$t<=$to;$t++) {
		if(isset($result[$topid][$t])) {
			$res = $result[$topid][$t];
			$tar = $res['trtarget'];
			$act = $res['tractual'];
			if($tar > 0) { $krtcount++; }
			if($kct!="CO") {
				$ytdtar += $tar;
				$ytdact += $act;
			} else {
				if($tar > $ytdtar) { $ytdtar = $tar; }
				if($act > $ytdact) { $ytdact = $act; }
			}
		}
		$tar = 0;
		$act = 0;
	}
	if(($kct == "STD" || $kct == "REV") && $krtcount > 0) {
		$ytdtar/=$krtcount;
		$ytdact/=$krtcount;
	}

	$krr = calcKR($kct,$ytdtar,$ytdact);
	if($kpimet == "combine" && $krr > 4) { $krr = 4; }
	if(in_array($krr,$results)) {
		$kpi[0][$krr]++;
		$kpi[0]['tot']++;
		if($groupby!="X") {
			if(!isset($kpi[$moreid])) {
				$kpi[$moreid]['tot'] = 0;
				foreach($results as $krs) {
					$kpi[$moreid][$krs] = 0;
				}
			}
			$kpi[$moreid][$krr]++;
			$kpi[$moreid]['tot']++;
		}
	}
}	

//echo "<p>".$sql;
//echo "<pre>"; print_r($kpi); echo "</pre>";
/********** GET GROUP INFORMATION **********/
$group = array();
$max_len = 110;
switch($groupby) {
	case "X":
		break;
	case "dir":
			$sql = "SELECT d.dirid as id, d.dirtxt as value FROM ".$dbref."_dir d WHERE d.dirid IN (SELECT topsubid FROM ".$dbref."_toplevel WHERE topyn = 'Y') AND d.diryn = 'Y' ORDER BY d.dirsort";
			$mds2 = array('type'=>"M",'sub'=>$row['id'],'dir'=>"ALL");
		break;
	default:
		$sql = "SELECT id, value FROM ".$dbref."_list_".$dlist[$groupby]['id']." WHERE id IN (SELECT ".$dlist[$groupby]['kpi']." FROM ".$dbref."_toplevel WHERE topyn = 'Y') AND yn = 'Y' ORDER BY value";
		$mds2 = $mds;
		break;
}
if(isset($sql) && $groupby != "X") {
		include("inc_db_con.php");
			while($row = mysql_fetch_array($rs)) {
				$group[$row['id']] = $row;
				$group[$row['id']]['mds'] = $mds2;
				$group[$row['id']]['value'] = decode($row['value']);
				if(strlen($group[$row['id']]['value'])>$max_len) { $group[$row['id']]['value'] = substr($group[$row['id']]['value'],0,($max_len-5))."..."; }
			}
		mysql_close($con);
}

		$group_max = array();
		$values_maxlen = array();
		calcGrouping($group,$group_max,$values_maxlen);

$overall_title = "";

if($mds['type']=="M") {
	if($groupby!="X" && $groupby != "dir") {
		$overall_title = $dlist[$groupby]['name'];
	} else {
		$overall_title = $cmpname;
	}
} else {
	$sql = "SELECT dirtxt as txt FROM ".$dbref."_dir WHERE dirid = ".$mds['sub'];
	include("inc_db_con.php");
		$dsrow = mysql_fetch_array($rs);
	mysql_close($con);
	if($groupby!="X" && $groupby != "dir") {
		$overall_title = $dlist[$groupby]['name'];
		if(isset($dsrow['txt'])) { $overall_title.= " for ".$dsrow['txt']; }
	} else {
		$overall_title = $dsrow['txt'];
	}
}

/*if($groupby!="X" && $groupby != "dir") {
	$overall_title = $dlist[$groupby]['name'];
} elseif($mds['type']=="S") {
	$sql = "SELECT dirtxt FROM ".$dbref."_dir WHERE dirid = ".$dirsub2;
	include("inc_db_con.php");
		$drow = mysql_fetch_array($rs);
	mysql_close($con);
	if(isset($drow['dirtxt'])) { 
		$overall_title = $drow['dirtxt'];
	}
} elseif($dirsub=="ALL" && $groupby == "dir") {
	$overall_title = $cmpname;
}*/
$group_title = "";
if($group_graph == "stack" && $groupby !="X") {
	if($groupby != "dir") {
		$group_title = $dlist[$groupby]['name'];
	} else {
		$group_title = "Directorates";
	}
}
?>