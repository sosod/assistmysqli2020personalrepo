<?php 
	include("inc_ignite.php"); 
	
	/*********** FUNCTIONS ****************/
	function getFileDate($fn) {
		$fdate = strFn("substr",$fn,0,8);
		$d = strFn("substr",$fdate,6,2);
		$m = strFn("substr",$fdate,4,2);
		$y = strFn("substr",$fdate,0,4);
		$ftime = strFn("substr",$fn,9,6);
		$H = strFn("substr",$ftime,0,2);
		$M = strFn("substr",$ftime,2,2);
		$s = strFn("substr",$ftime,4,2);
		return mktime($H,$M,$s,$m,$d,$y);
	}
	
	/*********** END FUNCTIONS ***************/
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.logth { }
.logtd {  }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Admin ~ Top Level: Automatic Update History</h1>
<?php
$path = strtoupper($modref)."/TOP";
checkFolder($path);
$path = "../files/$cmpcode/".$path."/";
$files = glob($path."*");
$files = array_reverse($files);
if(count($files)>0) {
	foreach($files as $f) {
		$fn = strFn("explode",$f,"/","");
		$file = $fn[count($fn)-1];
		$startdate = getFileDate($file);
		echo "<P><a href=".$path.$file.">".date("d M Y H:i:s",$startdate)."</a>";
	}
} else {
	echo "<P>No history available to view.</p>";
}

?>


</body>
</html>