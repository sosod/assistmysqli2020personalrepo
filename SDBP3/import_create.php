<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import - CREATE TABLES</b></h1>
<?php


$sql = "DROP TABLE IF EXISTS `".$dbref."_capital`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital` (";
$sql.= "  `cpid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cpsubid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpgfsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpnum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `cpidpnum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `cpvotenum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `cpname` varchar(250) NOT NULL DEFAULT '',";
$sql.= "  `cpproject` text NOT NULL,";
$sql.= "  `cpstartdate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpenddate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpstartactual` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpendactual` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `cpfundsourceid` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`cpid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_capital_area`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_area`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_area` (";
$sql.= "  `caid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cacpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `caareaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cayn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`caid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_capital_forecast`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_forecast`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_forecast` (";
$sql.= "  `cfid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cfcpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cfcrr` double NOT NULL DEFAULT '0',";
$sql.= "  `cfother` double NOT NULL DEFAULT '0',";
$sql.= "  `cfyearid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`cfid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_capital_results`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_results`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_results` (";
$sql.= "  `crid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `crcpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `crbudget` double NOT NULL DEFAULT '0',";
$sql.= "  `cractual` double NOT NULL DEFAULT '0',";
$sql.= "  `craccpercytd` double NOT NULL DEFAULT '0',";
$sql.= "  `crtotrandytd` double NOT NULL DEFAULT '0',";
$sql.= "  `crtimeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `crvire` double NOT NULL DEFAULT '0',";
$sql.= "  `cradjest` double NOT NULL DEFAULT '0',";
$sql.= "  `cradjbudget` double NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`crid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_capital_source`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_source`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_source` (";
$sql.= "  `csid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cscpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cssrcid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `csyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`csid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_capital_wards`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_wards`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_wards` (";
$sql.= "  `cwid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cwcpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cwwardsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cwyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`cwid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_dir`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_dir`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_dir` (";
$sql.= "  `dirid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `dirtxt` varchar(100) NOT NULL,";
$sql.= "  `dirsort` int(10) unsigned NOT NULL,";
$sql.= "  `diryn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`dirid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_dirsub`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_dirsub`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_dirsub` (";
$sql.= "  `subid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `subtxt` varchar(100) NOT NULL,";
$sql.= "  `subyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `subdirid` int(10) unsigned NOT NULL,";
$sql.= "  `subsort` int(10) unsigned NOT NULL,";
$sql.= "  `subhead` varchar(1) NOT NULL DEFAULT 'N',";
$sql.= "  PRIMARY KEY (`subid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_finance_cashflow`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_finance_cashflow`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_finance_cashflow` (";
$sql.= "  `cfid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cflineid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cfyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `cftimeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cfrev1` double NOT NULL DEFAULT '0' COMMENT 'obudget',";
$sql.= "  `cfrev2` double NOT NULL DEFAULT '0' COMMENT 'actual',";
$sql.= "  `cfrev3` double NOT NULL DEFAULT '0' COMMENT 'accytd',";
$sql.= "  `cfrev4` double NOT NULL DEFAULT '0' COMMENT 'totytd',";
$sql.= "  `cfrev5` double NOT NULL DEFAULT '0' COMMENT 'var',";
$sql.= "  `cfrev6` double NOT NULL DEFAULT '0' COMMENT 'vire',";
$sql.= "  `cfrev7` double NOT NULL DEFAULT '0' COMMENT 'adjest',";
$sql.= "  `cfrev8` double NOT NULL DEFAULT '0' COMMENT 'adjbudget',";
$sql.= "  `cfop1` double NOT NULL DEFAULT '0' COMMENT 'obudget',";
$sql.= "  `cfop2` double NOT NULL DEFAULT '0' COMMENT 'actual',";
$sql.= "  `cfop3` double NOT NULL DEFAULT '0' COMMENT 'accytd',";
$sql.= "  `cfop4` double NOT NULL DEFAULT '0' COMMENT 'totytd',";
$sql.= "  `cfop5` double NOT NULL DEFAULT '0' COMMENT 'var',";
$sql.= "  `cfop6` double NOT NULL DEFAULT '0' COMMENT 'vire',";
$sql.= "  `cfop7` double NOT NULL DEFAULT '0' COMMENT 'adjest',";
$sql.= "  `cfop8` double NOT NULL DEFAULT '0' COMMENT 'adjbudget',";
$sql.= "  `cfcp1` double NOT NULL DEFAULT '0' COMMENT 'obudget',";
$sql.= "  `cfcp2` double NOT NULL DEFAULT '0' COMMENT 'actual',";
$sql.= "  `cfcp3` double NOT NULL DEFAULT '0' COMMENT 'accytd',";
$sql.= "  `cfcp4` double NOT NULL DEFAULT '0' COMMENT 'totytd',";
$sql.= "  `cfcp5` double NOT NULL DEFAULT '0' COMMENT 'var',";
$sql.= "  `cfcp6` double NOT NULL DEFAULT '0' COMMENT 'vire',";
$sql.= "  `cfcp7` double NOT NULL DEFAULT '0' COMMENT 'adjest',";
$sql.= "  `cfcp8` double NOT NULL DEFAULT '0' COMMENT 'adjbudget',";
$sql.= "  PRIMARY KEY (`cfid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_finance_lineitems`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_finance_lineitems`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_finance_lineitems` (";
$sql.= "  `lineid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `linevalue` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `lineyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `linesort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `linetype` varchar(2) NOT NULL DEFAULT 'CF' COMMENT 'CF OR RS',";
$sql.= "  `linesubid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `linegfsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `linevote` varchar(45) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`lineid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_finance_revbysource`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_finance_revbysource`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_finance_revbysource` (";
$sql.= "  `rsid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `rsline` varchar(200) NOT NULL DEFAULT '0',";
$sql.= "  `rsyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `rssort` int(10) unsigned NOT NULL,";
$sql.= "  `rs1budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs1actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs2budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs2actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs3budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs3actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs4budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs4actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs5budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs5actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs6budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs6actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs7budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs7actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs8budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs8actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs9budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs9actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs10budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs10actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs11budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs11actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs12budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs12actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rsvote` varchar(45) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`rsid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_headings`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_headings`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_headings` (";
$sql.= "  `headid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `headtype` varchar(5) NOT NULL COMMENT 'KPI, CP, CF',";
$sql.= "  `headyn` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '''YN''',";
$sql.= "  `headdetail` varchar(100) NOT NULL,";
$sql.= "  `headdetailstd` varchar(100) NOT NULL,";
$sql.= "  `headdetailsort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `headdetailyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `headlist` varchar(100) NOT NULL,";
$sql.= "  `headliststd` varchar(100) NOT NULL,";
$sql.= "  `headlistsort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `headlistyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `headfixed` varchar(1) NOT NULL COMMENT '''YN - cmp change head?''',";
$sql.= "  `headfield` varchar(15) NOT NULL COMMENT '''S-Q-L field''',";
$sql.= "  `headmaxlen` int(11) NOT NULL,";
$sql.= "  `headrequired` varchar(1) NOT NULL COMMENT '''YN - cmp can-not remove''',";
$sql.= "  `headdescrip` text NOT NULL,";
$sql.= "  PRIMARY KEY (`headid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi` (";
$sql.= "  `kpiid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `kpimsr` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `kpisubid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpigfsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpiidpnum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `kpicpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpimunkpaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpinatkpaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpitasid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpiprogid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpivalue` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpicpmun` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `kpitypeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpistratop` varchar(1) NOT NULL DEFAULT 'O',";
$sql.= "  `kpidefinition` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpioutcome` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpichallenges` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpiriskdef` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpiriskrate` varchar(1) NOT NULL DEFAULT '',";
$sql.= "  `kpidriver` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpibaseline` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpitargetunit` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpipoe` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpifield1` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpifield2` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpifield3` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpicalctype` varchar(5) NOT NULL DEFAULT 'INC' COMMENT 'INC, STD, ZERO',";
$sql.= "  `kpitargettypeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpiyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`kpiid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_area`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_area`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_area` (";
$sql.= "  `kaid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `kakpiid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kaareaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kayn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`kaid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_plc`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_plc`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_plc` (";
$sql.= "  `plcid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `plcname` varchar(255) NOT NULL,";
$sql.= "  `plcdescription` varchar(255) NOT NULL,";
$sql.= "  `plckpiid` int(10) unsigned NOT NULL,";
$sql.= "  `plcstartdate` int(10) unsigned NOT NULL,";
$sql.= "  `plcenddate` int(10) unsigned NOT NULL,";
$sql.= "  `plcproccate` int(10) unsigned NOT NULL,";
$sql.= "  `plcyn` varchar(1) NOT NULL,";
$sql.= "  PRIMARY KEY (`plcid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_plc_old`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_plc_old`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_plc_old` (";
$sql.= "  `oldid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `plcid` int(10) unsigned NOT NULL,";
$sql.= "  `plcname` varchar(255) NOT NULL,";
$sql.= "  `plcdescription` varchar(255) NOT NULL,";
$sql.= "  `plckpiid` int(10) unsigned NOT NULL,";
$sql.= "  `plcstartdate` int(10) unsigned NOT NULL,";
$sql.= "  `plcenddate` int(10) unsigned NOT NULL,";
$sql.= "  `plcproccate` int(10) unsigned NOT NULL,";
$sql.= "  `plcyn` varchar(1) NOT NULL,";
$sql.= "  `oldtkid` varchar(10) NOT NULL,";
$sql.= "  `olddate` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`oldid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_plc_phases`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_plc_phases`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_plc_phases` (";
$sql.= "  `ppid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `ppplcid` int(10) unsigned NOT NULL,";
$sql.= "  `ppphaseid` int(10) unsigned NOT NULL,";
$sql.= "  `ppsort` int(10) unsigned NOT NULL,";
$sql.= "  `ppdays` int(10) unsigned NOT NULL,";
$sql.= "  `ppstartdate` int(10) unsigned NOT NULL,";
$sql.= "  `ppenddate` int(10) unsigned NOT NULL,";
$sql.= "  `pptarget` double NOT NULL,";
$sql.= "  `pptype` varchar(1) NOT NULL COMMENT 'S = Std, P = Proc, C = Cus, L = Last',";
$sql.= "  `ppdone` varchar(1) NOT NULL DEFAULT 'N',";
$sql.= "  `ppdonedate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`ppid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_plc_phases_old`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_plc_phases_old`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_plc_phases_old` (";
$sql.= "  `oldid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `ppid` int(10) unsigned NOT NULL,";
$sql.= "  `ppplcid` int(10) unsigned NOT NULL,";
$sql.= "  `ppphaseid` int(10) unsigned NOT NULL,";
$sql.= "  `ppsort` int(10) unsigned NOT NULL,";
$sql.= "  `ppdays` int(10) unsigned NOT NULL,";
$sql.= "  `ppstartdate` int(10) unsigned NOT NULL,";
$sql.= "  `ppenddate` int(10) unsigned NOT NULL,";
$sql.= "  `pptarget` double NOT NULL,";
$sql.= "  `pptype` varchar(1) NOT NULL COMMENT 'S = Std, P = Proc, C = Cus, L = Last',";
$sql.= "  `ppdone` varchar(1) NOT NULL DEFAULT 'N',";
$sql.= "  `ppdonedate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `oldtkid` varchar(10) NOT NULL,";
$sql.= "  `olddate` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`oldid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_plc_phases_temp`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_plc_phases_temp`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_plc_phases_temp` (";
$sql.= "  `tmpid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `ppid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `ppplcid` int(10) unsigned NOT NULL,";
$sql.= "  `ppphaseid` int(10) unsigned NOT NULL,";
$sql.= "  `ppsort` int(10) unsigned NOT NULL,";
$sql.= "  `ppdays` int(10) unsigned NOT NULL,";
$sql.= "  `ppstartdate` int(10) unsigned NOT NULL,";
$sql.= "  `ppenddate` int(10) unsigned NOT NULL,";
$sql.= "  `pptarget` double NOT NULL,";
$sql.= "  `pptype` varchar(1) NOT NULL COMMENT 'S = Std, P = Proc, C = Cus, L = Last',";
$sql.= "  `ppdone` varchar(1) NOT NULL DEFAULT 'N',";
$sql.= "  `ppdonedate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `tmptkid` varchar(10) NOT NULL,";
$sql.= "  `tmpdate` int(10) unsigned NOT NULL,";
$sql.= "  `tmpyn` varchar(1) NOT NULL,";
$sql.= "  PRIMARY KEY (`tmpid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_plc_temp`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_plc_temp`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_plc_temp` (";
$sql.= "  `tmpid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `plcid` int(10) unsigned NOT NULL,";
$sql.= "  `plcname` varchar(255) NOT NULL,";
$sql.= "  `plcdescription` varchar(255) NOT NULL,";
$sql.= "  `plckpiid` int(10) unsigned NOT NULL,";
$sql.= "  `plcstartdate` int(10) unsigned NOT NULL,";
$sql.= "  `plcenddate` int(10) unsigned NOT NULL,";
$sql.= "  `plcproccate` int(10) unsigned NOT NULL,";
$sql.= "  `plcyn` varchar(1) NOT NULL,";
$sql.= "  `tmptkid` varchar(10) NOT NULL,";
$sql.= "  `tmpdate` int(10) unsigned NOT NULL,";
$sql.= "  `tmpyn` varchar(1) NOT NULL DEFAULT 'P' COMMENT 'P=pending confirm, Y = awaiting approval, A = approved, R = rejected',";
$sql.= "  PRIMARY KEY (`tmpid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_result`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_result`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_result` (";
$sql.= "  `krid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `krkpiid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `krtarget` double NOT NULL DEFAULT '0',";
$sql.= "  `kractual` double NOT NULL DEFAULT '0',";
$sql.= "  `krprogress` text NOT NULL,";
$sql.= "  `krmanagement` text NOT NULL,";
$sql.= "  `krtimeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`krid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_kpi_wards`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_wards`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_wards` (";
$sql.= "  `kwid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `kwkpiid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kwwardsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kwyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`kwid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_admins`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_admins`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_admins` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `tkid` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `type` varchar(5) NOT NULL DEFAULT 'DIR',";
$sql.= "  `ref` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_area`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_area`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_area` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_forecastyears`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_forecastyears`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_forecastyears` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(45) NOT NULL,";
$sql.= "  `sort` int(10) unsigned NOT NULL,";
$sql.= "  `yn` varchar(1) NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_fundsource`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_fundsource`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_fundsource` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_gfs`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_gfs`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_gfs` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_kpicalctype`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_kpicalctype`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_kpicalctype` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  `descrip` varchar(100) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_kpiresult`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_kpiresult`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_kpiresult` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(50) NOT NULL,";
$sql.= "  `stdvalue` varchar(50) NOT NULL,";
$sql.= "  `mincalc` varchar(5) NOT NULL,";
$sql.= "  `min` int(10) unsigned NOT NULL,";
$sql.= "  `max` int(10) unsigned NOT NULL,";
$sql.= "  `maxcalc` varchar(5) NOT NULL,";
$sql.= "  `stdmincalc` varchar(5) NOT NULL,";
$sql.= "  `stdmin` int(10) unsigned NOT NULL,";
$sql.= "  `stdmax` int(10) unsigned NOT NULL,";
$sql.= "  `stdmaxcalc` varchar(5) NOT NULL,";
$sql.= "  `sort` int(10) unsigned NOT NULL,";
$sql.= "  `yn` varchar(1) NOT NULL,";
$sql.= "  `color` varchar(10) NOT NULL,";
$sql.= "  `code` varchar(5) NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_kpitype`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_kpitype`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_kpitype` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(30) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_munkpa`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_munkpa`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_munkpa` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// 
// Table structure for table `".$dbref."_list_natkpa`
// 

include("inc_db_con.php");    echo "<p>".$sql;
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_natkpa`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_natkpa` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(5) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_plcphases_custom`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_plcphases_custom` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL,";
$sql.= "  `yn` varchar(1) NOT NULL,";
$sql.= "  `dirid` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_plcphases_proc`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_plcphases_proc` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL,";
$sql.= "  `yn` varchar(1) NOT NULL,";
$sql.= "  `sort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `days1` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `days2` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `days3` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `days4` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_plcphases_std`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_plcphases_std` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL,";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y' COMMENT 'Yes/No/Req',";
$sql.= "  `sort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `type` varchar(1) NOT NULL COMMENT 'S = Std, P = Proc, L = Last',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_prog`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_prog` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `dirid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_riskrate`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_riskrate` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_stratop`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_stratop` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_targettype`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_targettype` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(30) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_tas`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_tas` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_time`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_time` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `sval` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `eval` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `sort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `active` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `activefin` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `activedir` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `activesub` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `activekpi` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `aclose` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `arem` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_list_wards`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_wards` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(100) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `numval` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "DROP TABLE IF EXISTS `".$dbref."_log`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_log` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `tkid` varchar(10) NOT NULL,";
$sql.= "  `date` int(10) unsigned NOT NULL,";
$sql.= "  `ref` int(10) unsigned NOT NULL,";
$sql.= "  `type` varchar(255) NOT NULL,";
$sql.= "  `action` text NOT NULL,";
$sql.= "  `disply` varchar(1) NOT NULL,";
$sql.= "  `lsql` text NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_forecastyears` (`id`, `value`, `sort`, `yn`) VALUES ";
$sql.= "(1, '2010/2011', 1, 'Y'),";
$sql.= "(2, '2011/2012', 2, 'Y'),";
$sql.= "(3, '2012/2013', 3, 'Y'),";
$sql.= "(4, '2013/2014', 4, 'Y'),";
$sql.= "(5, '2014/2015', 5, 'Y')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_kpicalctype` (`id`, `value`, `yn`, `code`, `descrip`) VALUES ";
$sql.= "(1, 'Carry Over', 'Y', 'CO', 'Targets &amp; Actuals carry over from one period to the next (% of project complete).'),";
$sql.= "(2, 'Accumulative', 'Y', 'ACC', 'The system sums the Targets &amp; Actuals  (progressive).'),";
$sql.= "(3, 'Stand-alone', 'Y', 'STD', 'Targets &amp; Actuals don&#039;t carry over from one time period to the next.'),";
$sql.= "(4, 'Zero %', 'Y', 'ZERO', 'Targets &amp; Actuals are 0.'),";
$sql.= "(5, 'NA', 'Y', 'NA', 'Not applicable.')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_kpiresult` (`id`, `value`, `stdvalue`, `mincalc`, `min`, `max`, `maxcalc`, `stdmincalc`, `stdmin`, `stdmax`, `stdmaxcalc`, `sort`, `yn`, `color`, `code`) VALUES ";
$sql.= "(1, 'KPI&#039;s Not Met', 'KPI&#039;s Not Met', 'G-E', 0, 75, 'L', 'G-E', 0, 75, 'L', 2, 'Y', '#CC0001', 'R'),";
$sql.= "(2, 'KPI&#039;s Almost Met', 'KPI&#039;s Almost Met', 'G-E', 75, 100, 'L', 'G-E', 75, 100, 'L', 3, 'Y', '#FE9900', 'O'),";
$sql.= "(3, 'KPI&#039;s Met', 'KPI&#039;s Met', 'E', 100, 100, 'E', 'E', 100, 100, 'E', 4, 'Y', '#009900', 'G'),";
$sql.= "(4, 'KPI&#039;s Well Met', 'KPI&#039;s Well Met', 'G', 100, 150, 'L', 'G', 100, 150, 'L', 5, 'Y', '#004400', 'G2'),";
$sql.= "(5, 'KPI&#039;s Not Yet Measured', 'KPI&#039;s Not Yet Measured', '', 0, 0, '', '', 0, 0, '', 1, 'Y', '#AAAAAA', 'N/A'),";
$sql.= "(6, 'KPI&#039;s Extremely Well Met', 'KPI&#039;s Extremely Well Met', 'G-E', 150, 99999999, 'L', 'G-E', 150, 99999999, 'L', 6, 'Y', '#000044', 'B')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_kpitype` (`id`, `value`, `yn`) VALUES ";
$sql.= "(1, 'N/A', 'Y'),";
$sql.= "(2, 'Input', 'Y'),";
$sql.= "(3, 'Process', 'Y'),";
$sql.= "(4, 'Output', 'Y'),";
$sql.= "(5, 'Outcome', 'Y'),";
$sql.= "(6, 'Legal Requirement', 'Y')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_natkpa` (`id`, `value`, `yn`, `code`) VALUES ";
$sql.= "(1, 'Municipal Transformation and Institutional Development', 'Y', 'MTID'),";
$sql.= "(2, 'Basic Service Delivery', 'Y', 'BSD'),";
$sql.= "(3, 'Local Economic Development', 'Y', 'LED'),";
$sql.= "(4, 'Municipal Financial Viability and Management', 'Y', 'MFVM'),";
$sql.= "(5, 'Good Governance and Public Participation', 'Y', 'GGPP')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_riskrate` (`id`, `value`, `yn`, `code`) VALUES ";
$sql.= "(1, 'High', 'Y', 'H'),";
$sql.= "(2, 'Medium', 'Y', 'M'),";
$sql.= "(3, 'Low', 'Y', 'L'),";
$sql.= "(4, 'N/A', 'Y', 'N')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_stratop` (`id`, `value`, `yn`, `code`) VALUES ";
$sql.= "(1, 'Strategic', 'Y', 'S'),";
$sql.= "(2, 'Operational', 'Y', 'O'),";
$sql.= "(3, 'Project', 'Y', 'P')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_targettype` (`id`, `value`, `yn`, `code`) VALUES ";
$sql.= "(1, 'Rands', 'Y', 'R'),";
$sql.= "(2, 'Percentage', 'Y', '%'),";
$sql.= "(3, 'Number', 'Y', '#')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_tas` (`id`, `value`, `yn`, `code`) VALUES ";
$sql.= "(1, 'Service delivery', 'Y', 'SD'),";
$sql.= "(2, 'Spatial conditions', 'Y', 'SC'),";
$sql.= "(3, 'Governance', 'Y', 'G'),";
$sql.= "(4, 'Financial Management', 'Y', 'FM'),";
$sql.= "(5, 'Local Economic Development', 'Y', 'LED'),";
$sql.= "(6, 'Labour Relations', 'Y', 'LR')";
include("inc_db_con.php");    echo "<p>".$sql;

$sql = "INSERT INTO `".$dbref."_list_time` (`id`, `sval`, `eval`, `sort`, `active`, `activefin`, `activedir`, `activesub`, `activekpi`, `yn`, `aclose`, `arem`) VALUES ";
$sql.= "(1, 1277935201, 1280613599, 1, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1281740400, 1281135600),";
$sql.= "(2, 1280613601, 1283291999, 2, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1284418800, 1283814000),";
$sql.= "(3, 1283292001, 1285883999, 3, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1287010800, 1286406000),";
$sql.= "(4, 1285884001, 1288562399, 4, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1289689200, 1289084400),";
$sql.= "(5, 1288562401, 1291154399, 5, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1292367600, 1292194800),";
$sql.= "(6, 1291154401, 1293832799, 6, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1294959600, 1294354800),";
$sql.= "(7, 1293832801, 1296511199, 7, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1297638000, 1297033200),";
$sql.= "(8, 1296511201, 1298930399, 8, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1300057200, 1299452400),";
$sql.= "(9, 1298930401, 1301608799, 9, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1302735600, 1302130800),";
$sql.= "(10, 1301608801, 1304200799, 10, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1305327600, 1304722800),";
$sql.= "(11, 1304200801, 1306879199, 11, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1308006000, 1307401200),";
$sql.= "(12, 1306879201, 1309471199, 12, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 1310598000, 1309993200)";
include("inc_db_con.php");    echo "<p>".$sql;



$sql = "INSERT INTO `".$dbref."_headings` (`headid`, `headtype`, `headyn`, `headdetail`, `headdetailstd`, `headdetailsort`, `headdetailyn`, `headlist`, `headliststd`, `headlistsort`, `headlistyn`, `headfixed`, `headfield`, `headmaxlen`, `headrequired`, `headdescrip`) VALUES ";
$sql.= "(1, 'KPI', 'Y', 'Municipal Scorecard Ref', 'Municipal Scorecard Ref', 1, 'Y', 'Mun. Scorecard', 'Mun. Scorecard', 1, 'N', 'Y', 'kpimsr', 20, 'N', 'The reference number of the KPI in the municipal scorecard that this KPI is linked to in order to calculate the actual performance of the municipal scorecard. '),";
$sql.= "(2, 'KPI', 'Y', 'GFS Classiciation', 'GFS Classiciation', 2, 'Y', 'GFS', 'GFS', 2, 'N', 'Y', 'gfs', 0, 'Y', 'Financial classification reference as per budget guidelines'),";
$sql.= "(3, 'KPI', 'Y', 'IDP Number', 'IDP Number', 3, 'Y', 'IDP', 'IDP', 3, 'N', 'N', 'kpiidpnum', 20, 'N', 'Reference to Intergrated development plan (IDP), where applicable'),";
$sql.= "(4, 'KPI', 'Y', 'Municipal KPA', 'Municipal KPA', 4, 'Y', 'Mun. KPA', 'Mun. KPA', 4, 'N', 'Y', 'munkpa', 0, 'Y', 'Key performance area (KPA) - focal performance areas for the complete municipality as  identified in the IDP'),";
$sql.= "(5, 'KPI', 'Y', 'National KPA', 'National KPA', 5, 'Y', 'Nat. KPA', 'Nat. KPA', 5, 'N', 'Y', 'natkpa', 0, 'Y', 'National Key performance indicators for municipalities as identified in the Local Government Strategic Agenda'),";
$sql.= "(6, 'KPI', 'Y', 'TAS Key Focus Area', 'TAS Key Focus Area', 6, 'Y', 'TAS', 'TAS', 6, 'N', 'Y', 'tas', 0, 'Y', 'Key focus areas applicable to municipalities as identified in the local government turnaround strategy document of December 2009'),";
$sql.= "(7, 'KPI', 'Y', 'Objective / Programme', 'Objective / Programme', 7, 'Y', 'Org / Prog', 'Org / Prog', 7, 'N', 'N', 'prog', 0, 'Y', 'Strategic objectives and/or programmes as identified by the municipality (also refer to IDP)'),";
$sql.= "(8, 'KPI', 'Y', 'KPI Name', 'KPI Name', 8, 'Y', 'KPI Name', 'KPI Name', 8, 'Y', 'N', 'kpivalue', 100, 'Y', 'The name of the Key Performance Indicator (KPI) - the action of what needs to be done'),";
$sql.= "(9, 'KPI', 'Y', 'Municipal Capital Project Ref', 'Municipal Capital Project Ref', 9, 'Y', 'CP Ref', 'CP Ref', 9, 'N', 'Y', 'kpicpmun', 30, 'N', 'Municipal reference number of capital project (only applicable to capital projects) '),";
$sql.= "(10, 'KPI', 'Y', 'KPI Type', 'KPI Type', 10, 'Y', 'Type', 'Type', 10, 'N', 'N', 'ktype', 0, 'Y', 'The type of KPI (is it a process, does it indicate an outcome / output or is it input)'),";
$sql.= "(11, 'KPI', 'Y', 'Strategic / Operational', 'Strategic / Operational', 11, 'Y', 'Strat. / Op.', 'Strat. / Op.', 11, 'N', 'Y', 'kpistratop', 0, 'Y', 'Does the KPI describe a strategic or operational action'),";
$sql.= "(12, 'KPI', 'Y', 'KPI Definition', 'KPI Definition', 12, 'Y', 'Def&#039;', 'Def&#039;n', 12, 'Y', 'N', 'kpidefinition', 200, 'N', 'More detail regarding the KPI to ensure that it complies with the SMART principle'),";
$sql.= "(13, 'KPI', 'Y', 'Outcome', 'Outcome', 13, 'Y', 'Outcome', 'Outcome', 13, 'N', 'Y', 'kpioutcome', 200, 'N', 'What will you achieve by implementing the KPI, e.g. Improved skills development'),";
$sql.= "(14, 'KPI', 'Y', 'Challenges', 'Challenges', 14, 'Y', 'Challenges', 'Challenges', 14, 'N', 'Y', 'kpichallenges', 200, 'N', 'What could possibly hamper you in achieving the KPI for eg. Basic computer skills of 90% of the staff are lacking'),";
$sql.= "(15, 'KPI', 'Y', 'Risk Definition', 'Risk Definition', 15, 'Y', 'Risk Def&#039;n', 'Risk Def&#039;n', 15, 'N', 'Y', 'kpiriskdef', 100, 'N', 'Reference to the risk identified in the risk assessment completed by Internal Audit - this is to ensure that we address our risks'),";
$sql.= "(16, 'KPI', 'Y', 'Risk Rating', 'Risk Rating', 16, 'Y', 'Risk Rating', 'Risk Rating', 16, 'Y', 'Y', 'kpiriskrate', 0, 'Y', 'The rating of the risk as rated by the Internal Auditors'),";
$sql.= "(17, 'KPI', 'Y', 'Wards', 'Wards', 17, 'Y', 'Wards', 'Wards', 17, 'N', 'Y', 'wards', 0, 'Y', 'Indication of wards that can be linked to the KPI, this is normally applicable to capital or ward projects'),";
$sql.= "(18, 'KPI', 'Y', 'Area', 'Area', 18, 'Y', 'Area', 'Area', 18, 'N', 'Y', 'area', 0, 'Y', 'Reference to the area / town that will benefit from the specific project identified in the KPI'),";
$sql.= "(19, 'KPI', 'Y', 'KPI Owner', 'KPI Owner', 19, 'Y', 'Owner', 'Owner', 19, 'Y', 'N', 'kpidriver', 100, 'N', 'Position of the person that will be responsible for executing the KPI, e.g. Head: Information Systems'),";
$sql.= "(20, 'KPI', 'Y', 'Baseline', 'Baseline', 20, 'Y', 'Baseline', 'Baseline', 20, 'Y', 'N', 'kpibaseline', 200, 'N', 'Reference to performance history calculated by asking what the performance during the previous financial year was. '),";
$sql.= "(21, 'KPI', 'Y', 'Target Unit', 'Target Unit', 21, 'Y', 'Target Unit', 'Target Unit', 21, 'Y', 'N', 'kpitargetunit', 100, 'N', 'How will the result be calculated which can include a % of something or a number of actions or rand value of something'),";
$sql.= "(22, 'KPI', 'Y', 'POE', 'POE', 22, 'Y', 'POE', 'POE', 22, 'N', 'Y', 'kpipoe', 200, 'N', 'What will you use in your Portfolio of Evidence (POE) as evidence that you have done what was expected from you for eg. Minutes of the monthly meetings'),";
$sql.= "(23, 'KPI', 'N', 'Blank Field 1', 'Blank Field 1', 23, 'Y', 'Blank 1', 'Blank 1', 23, 'N', 'Y', 'kpifield1', 200, 'N', ''),";
$sql.= "(24, 'KPI', 'N', 'Blank Field 2', 'Blank Field 2', 24, 'Y', 'Blank 2', 'Blank 2', 24, 'N', 'Y', 'kpifield2', 200, 'N', ''),";
$sql.= "(25, 'KPI', 'N', 'Blank Field 3', 'Blank Field 3', 25, 'Y', 'Blank 3', 'Blank 3', 25, 'N', 'Y', 'kpifield3', 200, 'N', ''),";
$sql.= "(26, 'KPI', 'Y', 'KPI Calculation Type', 'KPI Calculation Type', 26, 'Y', 'KPI Calc', 'KPI Calc', 26, 'N', 'Y', 'kpicalctype', 0, 'Y', ''),";
$sql.= "(27, 'KPIR', 'Y', 'Target', 'Target', 100, 'Y', 'Target', 'Target', 100, 'Y', 'Y', 'krtarget', 0, 'Y', ''),";
$sql.= "(28, 'KPIR', 'Y', 'Actual', 'Actual', 101, 'Y', 'Actual', 'Actual', 101, 'Y', 'Y', 'kractual', 0, 'Y', ''),";
$sql.= "(29, 'KPIR', 'Y', 'Progress Comment', 'Progress Comment', 102, 'Y', 'Progress', 'Progress', 102, 'Y', 'N', 'krprogress', 200, 'N', ''),";
$sql.= "(30, 'KPIR', 'Y', 'Management Comment', 'Management Comment', 103, 'Y', 'Management', 'Management', 103, 'Y', 'N', 'krmanagement', 200, 'N', '')";
include("inc_db_con.php");    echo "<p>".$sql;



$sql = "INSERT INTO `".$dbref."_headings` (`headid`, `headtype`, `headyn`, `headdetail`, `headdetailstd`, `headdetailsort`, `headdetailyn`, `headlist`, `headliststd`, `headlistsort`, `headlistyn`, `headfixed`, `headfield`, `headmaxlen`, `headrequired`, `headdescrip`) VALUES ";
$sql.= "(31, 'CP', 'Y', 'GFS Classification', 'GFS Classification', 1, 'Y', 'GFS', 'GFS', 1, 'N', 'N', 'gfs', 0, 'Y', ''),";
$sql.= "(32, 'CP', 'Y', 'Capital Project Number', 'Capital Project Number', 2, 'Y', 'CP. Num.', 'CP. Num.', 2, 'Y', 'N', 'cpnum', 10, 'N', ''),";
$sql.= "(33, 'CP', 'Y', 'IDP Number', 'IDP Number', 3, 'Y', 'IDP Num.', 'IDP Num.', 3, 'N', 'N', 'cpidpnum', 20, 'N', ''),";
$sql.= "(34, 'CP', 'Y', 'Vote Number', 'Vote Number', 4, 'Y', 'Vote Num.', 'Vote Num.', 4, 'N', 'N', 'cpvotenum', 20, 'N', ''),";
$sql.= "(35, 'CP', 'Y', 'Project Name', 'Project Name', 5, 'Y', 'Name', 'Name', 5, 'Y', 'Y', 'cpname', 100, 'Y', ''),";
$sql.= "(36, 'CP', 'Y', 'Project Description', 'Project Description', 6, 'Y', 'Description', 'Description', 6, 'Y', 'N', 'cpproject', 200, 'Y', ''),";
$sql.= "(37, 'CP', 'Y', 'Funding Source', 'Funding Source', 7, 'Y', 'Fund. Source', 'Fund. Source', 7, 'N', 'N', 'fundsource', 0, 'Y', ''),";
$sql.= "(38, 'CP', 'Y', 'Planned Start Date', 'Planned Start Date', 8, 'Y', 'Planned Start', 'Planned Start', 8, 'Y', 'N', 'cpstartdate', -1, 'Y', ''),";
$sql.= "(39, 'CP', 'Y', 'Planned Completion Date', 'Planned Completion Date', 9, 'Y', 'Planned End', 'Planned End', 9, 'Y', 'N', 'cpenddate', -1, 'Y', ''),";
$sql.= "(40, 'CP', 'Y', 'Actual Start Date', 'Actual Start Date', 10, 'Y', 'Actual Start', 'Actual Start', 10, 'N', 'Y', 'cpstartactual', -1, 'N', ''),";
$sql.= "(41, 'CP', 'Y', 'Actual End Date', 'Actual End Date', 11, 'Y', 'Actual End', 'Actual End', 11, 'N', 'Y', 'cpendactual', -1, 'N', ''),";
$sql.= "(42, 'CP', 'Y', 'Wards', 'Wards', 12, 'Y', 'Wards', 'Wards', 12, 'N', 'Y', 'wards', 0, 'Y', ''),";
$sql.= "(43, 'CP', 'Y', 'Area', 'Area', 13, 'Y', 'Area', 'Area', 13, 'N', 'Y', 'area', 0, 'Y', ''),";
$sql.= "(44, 'CP', 'Y', 'Progress', 'Progress', 14, 'Y', 'Progress', 'Progress', 14, 'Y', 'Y', 'progress', 0, 'Y', ''),";
$sql.= "(45, 'CPR', 'Y', 'Original Budget', 'Original Budget', 100, 'Y', 'Org. Budget', 'Org. Budget', 100, 'Y', 'Y', 'crbudget', 0, 'Y', ''),";
$sql.= "(46, 'CPR', 'Y', 'Actual', 'Actual', 104, 'Y', 'Actual', 'Actual', 104, 'Y', 'Y', 'cractual', 0, 'Y', ''),";
$sql.= "(47, 'CPR', 'Y', 'Accumulated % Spent - YTD', 'Accumulated % Spent - YTD', 105, 'Y', 'Acc % - YTD', 'Acc % - YTD', 105, 'Y', 'N', 'craccytd', 0, 'Y', ''),";
$sql.= "(48, 'CPR', 'Y', 'Total Rand Spent YTD', 'Total Rand Spent YTD', 106, 'Y', 'Tot. R - YTD', 'Tot. R - YTD', 106, 'Y', 'Y', 'crtotytd', 0, 'Y', ''),";
$sql.= "(49, 'CPF', 'Y', 'C.R.R.', 'C.R.R.', 200, 'Y', 'C.R.R.', 'C.R.R.', 200, 'Y', 'Y', 'cfcrr', 0, 'Y', ''),";
$sql.= "(50, 'CPF', 'Y', 'Other', 'Other', 201, 'Y', 'Other', 'Other', 201, 'Y', 'Y', 'cfother', 0, 'Y', '')";
include("inc_db_con.php");    echo "<p>".$sql;



$sql = "INSERT INTO `".$dbref."_headings` (`headid`, `headtype`, `headyn`, `headdetail`, `headdetailstd`, `headdetailsort`, `headdetailyn`, `headlist`, `headliststd`, `headlistsort`, `headlistyn`, `headfixed`, `headfield`, `headmaxlen`, `headrequired`, `headdescrip`) VALUES ";
$sql.= "(51, 'CF', 'Y', 'Line Item', 'Line Item', 0, 'Y', 'Line Item', 'Line Item', 0, 'Y', 'N', 'line', 200, 'Y', ''),";
$sql.= "(52, 'CF', 'Y', 'GFS Classification', 'GFS Classification', 0, 'Y', 'GFS', 'GFS', 0, 'Y', 'Y', 'gfs', 0, 'Y', ''),";
$sql.= "(53, 'CF', 'Y', 'Vote Number', 'Vote Number', 0, 'Y', 'Vote', 'Vote', 0, 'Y', 'Y', 'vote', 20, 'Y', ''),";
$sql.= "(54, 'CF', 'Y', 'Original Budget', 'Original Budget', 1, 'Y', 'Budget', 'Budget', 1, 'N', 'Y', 'budget', 0, 'Y', ''),";
$sql.= "(55, 'CF', 'Y', 'Virements', 'Virements', 2, 'Y', 'Virements', 'Virements', 2, 'N', 'Y', 'vire', 0, 'Y', ''),";
$sql.= "(56, 'CF', 'Y', 'Adjustments Estimate', 'Adjustments Estimate', 3, 'Y', 'Adj. Est.', 'Adj. Est.', 3, 'N', 'Y', 'adjest', 0, 'Y', ''),";
$sql.= "(57, 'CF', 'Y', 'Adjusted Budget', 'Adjusted Budget', 4, 'Y', 'Adj. Budget', 'Adj. Budget', 4, 'Y', 'Y', 'adjbudget', 0, 'Y', ''),";
$sql.= "(58, 'CF', 'Y', 'Actual', 'Actual', 5, 'Y', 'Actual', 'Actual', 5, 'Y', 'Y', 'actual', 0, 'Y', ''),";
$sql.= "(59, 'CF', 'Y', 'Variance', 'Variance', 6, 'Y', 'Variance', 'Variance', 6, 'Y', 'Y', 'var', 0, 'Y', ''),";
$sql.= "(60, 'CF', 'Y', 'Accumulated % Spent - YTD', 'Accumulated % Spent - YTD', 7, 'Y', 'Acc % - YTD', 'Acc % - YTD', 7, 'Y', 'Y', 'accytd', 0, 'Y', ''),";
$sql.= "(61, 'CF', 'Y', 'Total Rand Spent - YTD', 'Total Rand Spent - YTD', 8, 'Y', 'Tot. R - YTD', 'Tot. R - YTD', 8, 'Y', 'Y', 'totytd', 0, 'Y', ''),";
$sql.= "(62, 'CPR', 'Y', 'Virements', 'Virements', 101, 'Y', 'Virements', 'Virements', 101, 'Y', 'Y', 'crvire', 0, 'Y', ''),";
$sql.= "(63, 'CPR', 'Y', 'Adjustments Estimate', 'Adjustments Estimate', 102, 'Y', 'Adj. Est.', 'Adj. Est.', 102, 'Y', 'Y', 'cradjest', 0, 'Y', ''),";
$sql.= "(64, 'CPR', 'Y', 'Adjusted Budget', 'Adjusted Budget', 103, 'Y', 'Adj. Budget', 'Adj. Budget', 103, 'Y', 'Y', 'cradjbudget', 0, 'Y', '')";
include("inc_db_con.php");    echo "<p>".$sql;






$urlback = "import.php";
include("inc_goback.php");

?>

</body>

</html>
