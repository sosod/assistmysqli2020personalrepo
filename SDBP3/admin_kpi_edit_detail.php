<?php
    include("inc_ignite.php");
	$mytime = getUserTime();
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>

		<script type="text/javascript">
			$(function(){

                //Start
                $('#datepicker1').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
					changeMonth: true,
					changeYear: true
                });

                //End
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
					changeMonth: true,
					changeYear: true
                });

                //Start
                $('#datepicker3').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startActual',
                    altFormat: 'd_m_yy',
					changeMonth: true,
					changeYear: true
                });

                //End
                $('#datepicker4').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endActual',
                    altFormat: 'd_m_yy',
					changeMonth: true,
					changeYear: true
                });

			});
		</script>

<script type=text/javascript>
function kpiReset() {
document.location.href = document.location.href;
}
function changeDir(me) {
    var newdir = me.value;
    if(newdir.length>0 && !isNaN(parseInt(newdir)))
    {
        var target = document.getElementById('sid');
            target.length = 0;
            var o = 0;
            var ds = true;
            var opt = new Array();
                opt = sub[newdir];
                for(k in opt)
                {
                    if(o==0)
                        ds = true;
                    else
                        ds = false;
                    target.options[o] = new Option(opt[k][1],opt[k][0],ds,ds);
                    o++;
                }
/*        var target2 = document.getElementById('prog');
            target2.length = 2;
            o = 2;
            var opt2 = new Array();
                opt2 = prog[newdir];
                for(k in opt2)
                {
                    if(o==2)
                        ds = true;
                    else
                        ds = false;
                    target2.options[o] = new Option(opt2[k][1],opt2[k][0],ds,ds);
                    o++;
                }*/
    }
}
function delKPI(t,i,d) {
    if(!isNaN(parseInt(i)) && !isNaN(parseInt(d)))
    {
        var confirmtxt = "";
        if(t=="cp")
        {
            confirmtxt = "Are you sure you wish to delete this KPI and Capital Project?";
        }
        else
        {
            confirmtxt = "Are you sure you wish to delete this KPI?";
        }
        if(confirm(confirmtxt)==true)
        {
            document.location.href = "admin_kpi_delete.php?i="+i+"&d="+d;
        }
    }
    else
    {
        alert("An error occurred.\nPlease reload the page and try again.");
    }
}
function calcType(me) {
    if(me.value == "CO")
    {
        document.getElementById('plcedit').style.display = "inline";
    }
    else
    {
        document.getElementById('plcedit').style.display = "none";
    }
}
function chgProg(me) {
    if(me.value == "NEW")
    {
        document.getElementById('progval').style.display = "inline";
    }
    else
    {
        document.getElementById('progval').style.display = "none";
    }
}

function Validate(me) {
//    alert("validate");
    var flds = me.fields.value+"|pval";
    var fields = flds.split("|");
    var valid8 = "true";
    var target;
    var dirid = me.dirid.value;
    var subid = me.subid.value;
    var prog;
    var fd2 = fields.length;
//alert("fields");
//fields
    for(fd=0;fd<fd2;fd++)
    {
        if(fields[fd]!="wards")
        {
            target = document.getElementById(fields[fd]);
            targ = target.value;
//            alert(fields[fd]+"-"+targ);
            if(fields[fd]=="prog")
            {
                prog = targ;
            }
            if(fields[fd]=="pval")
            {
//                alert("check pval");
                if((targ.length==0 || targ == "X") && prog == "NEW")
                {
                    target.className = "reqtext";
                    valid8 = "false";
                }
                else
                {
                    target.className = "blank";
                }
//                alert("end pval");
            }
            else
            {
                if(targ.length==0 || targ == "X")
                {
                    target.className = "reqtext";
                    valid8 = "false";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
//            alert("wards");
            var werr = 0;
            var elements = document.getElementById('editkpi');
            var f = parseInt(document.getElementById('f').value);
            var f2 = parseInt(document.getElementById('f2').value);
            var z = 0;
            for(z=f;z<f2;z++)
            {
                if(elements[z].checked)
                {
                    werr++;
                }
            }
            if(werr==0)
            {
                elements[f].checked = true;
                f++;
                for(z=f;z<f2;z++)
                {
                    elements[z].checked = false;
                    elements[z].disabled = true;
                }
            }
        }
    }
//alert("end fields");

    var t = me.t.value;
    var t2 = 0;
    var berr = "N";
//target
//alert("targets");
if(document.getElementById('r').value == "k")
{
//    alert("checking targets");
    for(t2=0;t2<t;t2++)
    {
        target = document.getElementById('t'+t2);
        targ = target.value;
        if(targ.length==0)
        {
            target.value = 0;
        }
        else
        {
            if(isNaN(parseFloat(targ)))
            {
                berr = "Y";
                valid8 = "false";
            }
            else
            {
                if(parseFloat(targ)!=escape(targ))
                {
                    berr = "Y";
                    valid8 = "false";
                }
            }
            if(berr == "Y")
            {
                target.className = "reqtext";
            }
            else
            {
                target.className = "blank";
            }
        }
    }
}

//actuals
    var a = me.a.value;
    var a2 = 0;
//alert("checking actuals");
    for(a2=0;a2<a;a2++)
    {
        target = document.getElementById('a'+a2);
        targ = target.value;
        if(targ.length==0)
        {
            target.value = 0;
        }
        else
        {
            if(isNaN(parseFloat(targ)))
            {
                berr = "Y";
                valid8 = "false";
            }
            else
            {
                if(parseFloat(targ)!=escape(targ))
                {
                    berr = "Y";
                    valid8 = "false";
                }
            }
            if(berr == "Y")
            {
                target.className = "reqtext";
            }
            else
            {
                target.className = "blank";
            }
        }
    }

//alert("return validate");
    if(valid8=="true")
    {
        if(dirid == "ALL" || subid == "ALL" || subid == "HEAD")
        {
            if(confirm("Please note that adding KPIs to muliple Sub-Directorates can take some time.\nPlease do not click the 'Add' button on the form again.\n\nClick 'OK' to continue adding the KPIs or 'Cancel' to stop adding the KPIs.")==true)
            {
                document.getElementById('ad').disabled = true;
                document.getElementById('rst').disabled = true;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        var errtxt = "Please complete all missing fields as indicated.";
        if(berr == "Y")
        {
            errtxt = errtxt+"\nNote: Only numbers are allowed for ";
            if(document.getElementById('r').value=="k")
            {
                errtxt = errtxt+"Targets & ";
            }
            errtxt = errtxt+"Actuals.";
        }
        alert(errtxt);
        return false;
    }
    return false;
}

function wardsAll(me) {
        var elements = document.getElementById('editkpi');
        var f = parseInt(document.getElementById('f').value);
        var f2 = parseInt(document.getElementById('f2').value);
        var z = 0;
        f++;
    if(me.checked)
    {
        for(z=f;z<f2;z++)
        {
            elements[z].checked = false;
            elements[z].disabled = true;
        }
    }
    else
    {
        for(z=f;z<f2;z++)
        {
            elements[z].disabled = false;
        }
    }
}
function areaAll(me) {
        var elements = document.getElementById('editkpi');
        var f = parseInt(document.getElementById('f').value);
        var f2 = parseInt(document.getElementById('f2').value);
        var f3 = parseInt(document.getElementById('f3').value);
//        alert(f+"-"+f2+"-"+f3);
//        f3 = f3 + f3 - f;
        var z = 0;
        f = f2+2;
        //f++;
        f3++;
    if(me.checked)
    {
        for(z=f;z<f3;z++)
        {
            elements[z].checked = false;
            elements[z].disabled = true;
        }
    }
    else
    {
        for(z=f;z<f3;z++)
        {
            elements[z].disabled = false;
        }
    }
}
function plcEdit() {
//    document.forms['plcformedit'].submit();
    document.getElementById('plcact').value = "Y";
    if(confirm("Moving to the Project Life Cycle page will save the changes you have made to this KPI.\n\nDo you wish to continue?")==true)
    {
        document.forms['editkpi'].submit();
    }
    else
    {
        document.getElementById('plcact').value = "N";
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Update KPI</b></h1>
<?php
$referer = "-".$_SERVER['HTTP_REFERER']."-";
$referer2 = explode("/",$referer);
$referer2[4] = explode(".",$referer2[4]);
$refres = "Y";
//if($referer=="--" || ($referer2[2]!="assist.ignite4u.co.za" && $referer2[2]!="ignite-jeb") || ($referer2[4][0]!="admin_kpi_edit_list" && $referer2[4][0]!="admin_cp_edit_list"))
//{
//    $refres = "N";
//}
//print_r($referer2);
$kpiid = $_GET['k'];
$typ = $_GET['t'];  //op = Operational, cp = Capital
$src = $_GET['r'];  //d = Dir, s = Sub, k = KPI
$dirid = $_GET['d'];
$subid = $_GET['s'];
$origsub = $subid;
$err = "Y";
$wards = array();

$kpi = array();
if(is_numeric($kpiid) && $kpiid > 0)
{
    $mnr = 0;
    //CHECK FOR SUB ADMIN
    $sql = "SELECT a.id FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_".$modref."_kpi k ";
    $sql.= "WHERE k.kpisubid = a.ref AND a.type = 'SUB' AND a.yn = 'Y' AND k.kpiid = ".$kpiid." AND a.tkid = '".$tkid."'";
    include("inc_db_con.php");
    $mnr = $mnr + mysql_num_rows($rs);
    mysql_close($con);
    //CHECK FOR DIR ADMIN
    $sql = "SELECT a.id FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_".$modref."_kpi k, assist_".$cmpcode."_".$modref."_dirsub s ";
    $sql.= "WHERE k.kpisubid = s.subid AND s.subdirid = a.ref AND a.type = 'DIR' AND a.yn = 'Y' AND k.kpiid = ".$kpiid." AND a.tkid = '".$tkid."'";
    include("inc_db_con.php");
    $mnr = $mnr + mysql_num_rows($rs);
    mysql_close($con);
    //CHECK FOR KPI ADMIN
    $sql = "SELECT a.id FROM assist_".$cmpcode."_".$modref."_list_admins a ";
    $sql.= "WHERE a.type = 'KPI' AND a.yn = 'Y' AND a.tkid = '".$tkid."'";
    include("inc_db_con.php");
    $mnr = $mnr + mysql_num_rows($rs);
    mysql_close($con);

    if($mnr==0)
        die("<P>You are not authorised to edit this KPI.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");

    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiid = ".$kpiid;
    include("inc_db_con.php");
        $kpi = mysql_fetch_array($rs);
	$kct = $kpi['kpicalctype'];
    mysql_close();

} else {
        die("<P>You are not authorised to edit this KPI.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");
}

if(count($kpi)>0)
{
    $err = "N";
}


if($typ == "cp")
{
    $cpid = $_GET['i'];
}
else
{
    $cpid = 0;
}

if($err == "Y")
{
    ?>
    <p>&nbsp;</p>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
else
{
$err = "Y";

$sub = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subid = ".$kpi['kpisubid'];
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
        $sub = mysql_fetch_array($rs);
        $dirid = $sub['subdirid'];
        $err = "N";
    }
mysql_close();


if($err == "Y")
{
    ?>
    <p>&nbsp;</p>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
else
{
$err = "Y";

$dir = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' AND dirid = ".$sub['subdirid'];
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
        $dir = mysql_fetch_array($rs);
        $err = "N";
    }
mysql_close();


if($err == "Y")
{
    ?>
    <p>&nbsp;</p>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
else
{

$style[0] = "background-color: #555555; border-right: 1px solid #ffffff;";
$style[1] = "background-color: #cc0001;";
$style[2] = "background-color: #fe9900;";
$style[3] = "background-color: #006600;";
$style[4] = "background-color: #000066;";

$style2[0] = "background-color: #555555; border-left: 1px solid #ffffff;";
$style2[1] = "background-color: #cc0001; border-left: 1px solid #ffffff;";
$style2[2] = "background-color: #fe9900; border-left: 1px solid #ffffff;";
$style2[3] = "background-color: #006600; border-left: 1px solid #ffffff;";
$style2[4] = "background-color: #000066; border-left: 1px solid #ffffff;";


$stylel[0] = "background-color: #dddddd;";
$stylel[1] = "background-color: #ffcccc;";
$stylel[2] = "background-color: #ffeaca;";
$stylel[3] = "background-color: #ceffde;";
$stylel[4] = "background-color: #ccccff;";

    $widthth = 180;
    $widthtg = 420;
?>
<p style="margin-top: -5px;"><i>Note: All fields marked * are required.</i></p>
<form name=editkpi id=editkpi method=POST action=admin_kpi_edit_process.php onsubmit="return Validate(this);" language=jscript>
<h2>KPI Details</h2>
<table cellpadding=3 cellspacing=0 width=600>
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Reference:</td>
        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><?php echo($kpiid); ?><input type=hidden name=kpiid value=<?php echo($kpiid); ?>></td>
    </tr>
    <?php if($src != "k") { ?>
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Directorate*:</td>
        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><?php echo($dir['dirtxt']); ?><input type=hidden name=dirid value=<?php echo($sub['subdirid']); ?>></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Sub-Directorate*:</td>
        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><?php if(($src == "d" || $src == "k") && !checkIntRef($kpi['kpicpid'])) { ?><select name=subid><?php
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$sub['subdirid']." ORDER BY subsort";
                        include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs))
                            {
                                ?>
                                <option <?php if($row['subid'] == $kpi['kpisubid']) { echo("selected"); } ?> value=<?php echo($row['subid']); ?>><?php echo($row['subtxt']); ?></option>
                                <?php
                            }
                        mysql_close();
        ?></select><?php } else { ?><?php echo($sub['subtxt']); ?><input type=hidden name=subid value=<?php echo($kpi['kpisubid']); ?>><?php } ?></td>
    </tr>
    <?php } else { //src=k ?>
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Directorate*:</td>
        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><select name=dirid onchange="changeDir(this);"><?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
        while($row = mysql_fetch_array($rs))
        {
            echo("<option ");
            if($row['dirid'] == $dir['dirid']) { echo(" selected "); }
            echo("value=".$row['dirid'].">".$row['dirtxt']."</option>");
        }
    }
mysql_close();
        ?></select></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Sub-Directorate*:</td>
        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><select name=subid id=sid><?php
        $js = "var sub = new Array();".chr(10);
        $js.= "sub[".$sub['subdirid']."] = new Array();".chr(10);
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$sub['subdirid']." ORDER BY subsort";
                        include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs))
                            {
                                $sort2 = $row['subid'];
                                $did2 = $row['subdirid'];
                                $id2 = $row['subid'];
                                $val2 = $row['subtxt'];
                                $js.= "sub[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                                ?>
                                <option <?php if($row['subid'] == $kpi['kpisubid']) { echo("selected"); } ?> value=<?php echo($row['subid']); ?>><?php echo($row['subtxt']); ?></option>
                                <?php
                            }
                        mysql_close();
        ?></select><input type=hidden name=oldsubid id=osid value=<?php echo($kpi['kpisubid']); ?>></td>
    </tr>
<?php
                        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid <> ".$sub['subdirid']." ORDER BY subdirid, subsort";
                        include("inc_db_con.php");
                        $d1 = 0;
                            while($row = mysql_fetch_array($rs))
                            {
                                $sort2 = $row['subid'];
                                $did2 = $row['subdirid'];
                                if($did2 != $d1)
                                {
                                    $js.= "sub[".$did2."] = new Array();".chr(10);
                                    $d1 = $did2;
                                }
                                $id2 = $row['subid'];
                                $val2 = $row['subtxt'];
                                $js.= "sub[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                            }
                        mysql_close();

?>
    <?php } //src ?>
    <?php
    $f=3;
    $fields = "dirid|subid";
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headdetailyn = 'Y' AND headyn = 'Y' ORDER BY headdetailsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
			$hf = $row['headfield'];
            $f++;
            if($row['headrequired']=="Y")
            {
                $fields.="|".$row['headfield'];
            }
            ?>
                    <tr>
                        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>><?php echo($row['headdetail']); ?>:<?php if($row['headrequired']=="Y") { echo("*"); } ?></td>
            <?php
        if($dlist[$hf]['yn']=="Y" && $hf!="prog") {
            $dl = $dlist[$hf];
                        echo "<td height=27 width=$widthtg>";
                            echo "<select name=\"$hf\" id=\"$hf\">";
                                echo "<option selected value=X>--- SELECT ---</option>";
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_".$dl['id']." WHERE yn = 'Y' ";
								if($hf!="kpiriskrate") { $sql2.= "ORDER BY value"; }
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
										if($hf!="kpiriskrate" && $hf!="kpistratop") {
											echo("<option value=".$row2['id']);
											if($row2['id']==$kpi[$dl['kpi']]) { echo(" selected"); }
											echo ">".$row2['value'];
										} else {
											$kvalue = $kpi[$dl['kpi']];
											if($kvalue == "X" && $hf == "kpiriskrate") { 
												$kvalue = "N"; 
//											} elseif($kvalue == "X" && $hf == "kpistratop") { 
//												$kvalue = "O";
											}
											if(checkIntRef($kvalue)) {
												echo("<option value=".$row2['code']);
													if($row2['id']==$kvalue) { echo(" selected"); }
												echo ">".$row2['value'];
											} elseif ($kvalue == "X") {
												echo "<option value=".$row2['code'].">".$row2['value'];
											} else {
												echo("<option value=".$row2['code']);
													if($row2[$dl['fld']]==$kvalue) { echo(" selected"); }
												echo ">".$row2['value'];
											}
										}
										if($dl['code']=="Y") { echo " (".$row2[$dl['fld']].")"; } 
										echo "</option>";
                                    }
                                mysql_close($con2);
                            echo "</select>";
                        echo "</td>";
        } else {
            switch($row['headfield'])
            {
                case "area":
                    if($src == "k") { $f++; }
                    ?>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><input type=hidden id=f size=2 value=<?php echo($f); ?>>
                    <?php
                        $wards = array();
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_area WHERE kayn = 'Y' AND kakpiid = ".$kpiid."";
                        include("inc_db_con2.php");
                            while($row2 = mysql_fetch_array($rs2))
                            {
                                $wards[$row2['kaareaid']] = "Y";
                            }
                        mysql_close($con2);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_area WHERE yn = 'Y' AND value <> 'All' ORDER BY value";
                        include("inc_db_con2.php");
                            $c = 0;
                            $c2 = 0;
                                ?>
                                <table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
                                    <tr>
                                        <td class=tdgeneral style="border: 0px solid #ffffff;"><input type=checkbox name=area[] <?php if($wards[1]=="Y") { echo("checked"); } ?> value=1 onclick="areaAll(this);" id=wall> All</td>
                                        <?php
                                            $c++;
                                            $c2++;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                echo("<td class=tdgeneral style=\"border: 0px solid #ffffff;\"><input type=checkbox ");
                                                if($wards[$row2['id']]=="Y") { echo(" checked "); }
                                                echo("name=area[] value=".$row2['id']." >".$row2['value']."</td>");
                                                $c++;
                                                if($c==5)
                                                {
                                                    $c = 0;
                                                    echo("</tr><tr>");
                                                }
                                                $c2++;
                                            }
                                        ?>
                                    </tr>
                                </table>
                                <?php
                        mysql_close($con2);
                    ?>
                        </td>
                    <?php
                    $f3 = $f2+$c2;
                    break;
				case "kpimsr":
					echo "<td height=27 width=$widthtg>";
						echo $kpi[$row['headfield']];
						if($setuptopleveltype=="TBL") {	
							$sql2 = "SHOW TABLES LIKE '".$dbref."_toplevel'";
							include("inc_db_con2.php");
								$tmnr = mysql_num_rows($rs2);
							mysql_close($con2);
							if($tmnr>0 && checkIntRef($kpi['kpimsr'])) {
								$sql2 = "SELECT topvalue, topunit FROM ".$dbref."_toplevel WHERE topid = ".$kpi['kpimsr']." LIMIT 1";
								include("inc_db_con2.php");
									if(mysql_num_rows($rs2)>0) {
										$top = mysql_fetch_assoc($rs2);
										echo ": ".$top['topvalue']." [".$top['topunit']."]";
									}
								mysql_close($con2);
							}
						}
					echo "<input type=hidden name=kpimsr value=\"".$kpi['kpimsr']."\"></td>";
					break;
				case "kpicalctype":
                    ?>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>>
                            <select name=<?php echo($row['headfield']); ?> id=<?php echo($row['headfield']); ?>>
                                <option selected value=X>--- SELECT ---</option>
                                <?php
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_kpicalctype WHERE yn = 'Y' ";
								$sql2.= "ORDER BY value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        echo("<option value=".$row2['code']);
                                        if($row2['code']==$kpi['kpicalctype']) { echo(" selected"); }
                                        echo(">".$row2['value']."</option>");
                                    }
                                mysql_close($con2);
                                ?>
                            </select>
                        </td>
                    <?php
					break;
                case "prog":
                    $f++;
                    ?>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>>
                                <?php
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE yn = 'Y' ORDER BY value";
//                                echo("<P>".$sql2);
                                include("inc_db_con2.php");
                                ?><select name=<?php echo($row['headfield']); ?> id=<?php echo($row['headfield']); ?> onchange="chgProg(this)"><?php
                                    $pmnr = mysql_num_rows($rs2);
                                    if($pmnr > 0)
                                    {
                                        ?>
                                        <option <?php if($kpi[$row['headfield']]=="X") { echo("selected"); } ?> value=X>--- SELECT ---</option>
                                        <option value=NEW>Add new</option>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option <?php if($kpi[$row['headfield']]=="NEW") { echo("selected"); } ?> value=NEW>Add new</option>
                                        <?php
                                    }
                                    $v2 = "";
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                            ?><option <?php if($kpi['kpiprogid']==$row2['id']) { echo("selected"); } ?> value=<?php echo($row2['id']); ?>><?php echo($row2['value']); ?></option><?php
                                    }
                                mysql_close();
                                ?>
                            </select><span id=progval><br><input type=text maxlength=100 name=progval id=pval size=40></span>
                        </td>
                    <?php
/*                    if($src=="k")
                    {
                        $p=0;
                        $js.= "var prog = new Array();".chr(10);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_prog WHERE yn = 'Y' ORDER BY dirid, value";
                        include("inc_db_con2.php");
                        $d1=0;
                            while($row2 = mysql_fetch_array($rs2))
                            {
                                $p++;
                                $did2 = $row2['dirid'];
                                if($did2!=$d1)
                                {
                                    $js.= "prog[".$did2."] = new Array();".chr(10);
                                    $d1 = $did2;
                                }
                                $id2 = $row2['id'];
                                $val2 = $row2['value'];
                                $js.= "prog[".$did2."][".$p."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                            }
                        mysql_close($con2);
                    }*/
                    break;
                case "wards":
                    if($src == "k") { $f++; }
                    ?>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><input type=hidden id=f size=2 value=<?php echo($f); ?>>
                    <?php
                        $wards = array();
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_wards WHERE kwyn = 'Y' AND kwkpiid = ".$kpiid."";
                        include("inc_db_con2.php");
                            while($row2 = mysql_fetch_array($rs2))
                            {
                                $wards[$row2['kwwardsid']] = "Y";
                            }
                        mysql_close($con2);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $c = 0;
                            $c2 = 0;
                                ?>
                                <table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
                                    <tr>
                                        <td class=tdgeneral style="border: 0px solid #ffffff;"><input type=checkbox name=wards[] <?php if($wards[1]=="Y") { echo("checked"); } ?> value=1 onclick="wardsAll(this);" id=wall> All</td>
                                        <?php
                                            $c++;
                                            $c2++;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                echo("<td class=tdgeneral style=\"border: 0px solid #ffffff;\"><input type=checkbox ");
                                                if($wards[$row2['id']]=="Y") { echo(" checked "); }
                                                echo("name=wards[] value=".$row2['id']." >".$row2['value']."</td>");
                                                $c++;
                                                if($c==5)
                                                {
                                                    $c = 0;
                                                    echo("</tr><tr>");
                                                }
                                                $c2++;
                                            }
                                        ?>
                                    </tr>
                                </table>
                                <?php
                        mysql_close($con2);
                    ?>
                        </td>
                    <?php
                    $f2 = $f+$c2;
                    break;
                default:
                    $val = "";
                    $val = $kpi[$row['headfield']];
                    $size = $row['headmaxlen'];
                    if($size>40)
                    {
                        $size = 40;
                    }
                    $maxlen = $row['headmaxlen'];
                    ?>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>>
                            <input type=text size=<?php echo($size); ?> maxlength=<?php echo($maxlen); ?> name=<?php echo($row['headfield']); ?> value="<?php echo($val); ?>">  <span style="font-size: 7.5pt;">(max <?php echo($row['headmaxlen']); ?> characters)</span>
                        </td>
                    <?php
                    break;
            }
            }
            ?></tr>
            <?php
        }
    mysql_close();
    ?>
</table>
<?php
if($refres=="Y")
{
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc WHERE plckpiid = ".$kpiid." AND plcyn = 'Y' ORDER BY plcid DESC";
include("inc_db_con.php");
    $plc = array();
//    $mnr = mysql_num_rows($rs);
    $plc = mysql_fetch_array($rs);
//    while($row = mysql_fetch_array($rs))
//    {
//        $plc = $row;
//    }
mysql_close();
if(strlen($plc['plcid'])>0) { $plcbut = " Update Project Life Cycle"; } else { $plcbut = " Add project Life Cycle "; }
?><span id=plcedit><p style="margin-left: -5;"><input type=button value="<?php echo($plcbut); ?>" onclick="plcEdit();"></p></span>
<?php
} else {
echo("<span id=plcedit><P>To update/add a Project Life Cycle, please log onto <a href=http://assist.ignite4u.co.za>Ignite Assist</a>.</p></span>");
}
?>
<?php if($kpi['kpicalctype'] != "CO") { ?>
<script type=text/javascript> document.getElementById('plcedit').style.display = "none"; </script>
<?php } ?>
<h2>KPI Results</h2>
<?php
$sql = "SELECT tt.id, tt.value FROM assist_".$cmpcode."_".$modref."_list_targettype tt WHERE tt.id  = ".$kpi['kpitargettypeid'];
include("inc_db_con.php");
    $ttrow = mysql_fetch_array($rs);
mysql_close();
?>
<table cellpadding=5 cellspacing=0 width=600 style="margin-bottom: 10px;">
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Target Type:*</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?>><?php if($src!="k") { ?><?php echo($ttrow['value']); ?><input type=hidden name=krtargettypeid value="<?php echo $ttrow['id']; ?>"><?php } else { ?><select name=krtargettypeid id=krtargettypeid>
            <?php
            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_targettype WHERE yn = 'Y' ORDER BY value";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $id2 = $row2['id'];
                    $val2 = $row2['value']." (".$row2['code'].")";
                    echo("<option ");
                    if($id2==$ttrow['id']) { echo("selected "); }
                    echo("value=".$id2.">".$val2."</option>");
                }
            mysql_close();
            ?>
        </select><?php } //src=k?></td>
    </tr>
</table>
<?php

?>
<table cellpadding=3 cellspacing=0 width=600>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $t=0;
    $a=0;
    while($row = mysql_fetch_array($rs))
    {
        $rowspan=2;
        $kr = array();
        $sql2 = "SELECT kr.* FROM assist_".$cmpcode."_".$modref."_kpi_result kr WHERE kr.krkpiid = ".$kpiid." AND kr.krtimeid = ".$row['id'];
        include("inc_db_con2.php");
            $kr = mysql_fetch_array($rs2);
        mysql_close($con2);
        $kr['code'] = $ttrow['code'];
                switch($kct)
                {
                    case "CO":
                        $sql2 = "SELECT max(krtarget) as krtarget, max(kractual) as kractual FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid <= ".$row['id'];
                        include("inc_db_con2.php");
                            $kr2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid = ".$row['id'];
                        include("inc_db_con2.php");
                            $kr2x = mysql_fetch_array($rs2);
                            $kr2['krprogress'] = $row2x['krprogress'];
                            $kr2['krmanagement'] = $row2x['krmanagement'];
                            $kr2['krtargettypeid'] = $row2x['krtargettypeid'];
                        mysql_close($con2);
                        break;
                    default:
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid = ".$row['id'];
                        include("inc_db_con2.php");
                            $kr2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        break;
                }
        $krtarget = $kr2['krtarget'];
        $kractual = $kr2['kractual'];

        $krr = calcKR($kct,$krtarget,$kractual);

        if($kr['code']=="%")
        {
            $krt = number_format($krtarget,2)." %";
            $leng = 10;
        }
        else
        {
            if($kr['code']=="R")
            {
                $krt = "R ".number_format($krtarget,2);
                $leng = 20;
            }
            else
            {
                $krt = number_format($krtarget,0);
                $leng = 10;
            }
        }
        $widthth2 = $widthth-40;
        $rowspan=1;
        if($row['sval']<$today) { if($src!= "k") { $rowspan = 4; } else { $rowspan=3; } }
        $active = $row[$mytime];
        
        if($active == "Y")
        {
        ?>
    <tr>
        <td class=tdheaderl valign=top rowspan=<?php echo($rowspan); ?> width=<?php echo($widthth); ?> style="<?php echo($style[$s]); ?> border-right: 1px solid #fff;" ><?php echo(date("F Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
        <td class=tdheaderl valign=top height=27 width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Target:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php if($src=="k" && $active=="Y") { ?><?php if($kr['code']=="R") { echo("R "); } ?><input type=text size=<?php echo($leng); ?> name=krtarget[] id=<?php echo("t".$t); ?> style="text-align:right;" value="<?php echo($krtarget); ?>"><?php if($kr['code']=="%") { echo(" %"); } ?>&nbsp;<span style="font-size: 7.5pt;">(Numbers only)<?php } else { ?><?php echo($krt); ?><?php } ?></td>
    </tr>
                <?php if($row['sval']<$today) { ?>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Actual:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php if($kr['code']=="R") { echo("R "); } if($active=="Y") { ?><input type=text size=<?php echo($leng); ?> name=kractual[] id=<?php echo("a".$a); ?> style="text-align:right;" value="<?php echo($kractual); ?>"><?php } else { echo($kractual); } if($kr['code']=="%") { echo(" %"); } ?>&nbsp;<span style="font-size: 7.5pt;">(Numbers only)</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Comment:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><textarea cols=50 rows=5 name=krprogress[] id=<?php echo("p".$t); ?>><?php echo($kr['krprogress']); ?></textarea></td>
    </tr>
    <?php if($src != "k") { ?>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Management:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><textarea cols=50 rows=5 name=krmanagement[] id=<?php echo("m".$t); ?> ><?php echo($kr['krmanagement']); ?></textarea></td>
    </tr>
    <?php } //if src != k
            } //sval < today
        } else { //active == y else
        $rowspan=5;
    ?>
    <tr>
        <td class=tdheaderl valign=top rowspan=<?php echo($rowspan); ?> width=<?php echo($widthth); ?> style="<?php echo($style[$s]); ?> border-right: 1px solid #fff;" ><?php echo(date("F Y",$row['eval'])); ?>:</td>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Target:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php if($kr['code']=="R") { echo("R "); } ?><?php echo($krtarget); ?><?php if($kr['code']=="%") { echo(" %"); } ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Actual:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php if($kr['code']=="R") { echo("R "); } ?><?php echo($kractual); ?><?php if($kr['code']=="%") { echo(" %"); } ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Result:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php if($krr>0) { ?><table border=0 cellpadding=0 cellspacing=0 style="border: 0px;"><tr><td width=30 class=tdheaderl style="border: 1px solid #ffffff; <?php echo($styler[$krr]); ?>">&nbsp;</td></tr></table><?php } else { echo("&nbsp;"); } ?></td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Comment:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php echo($kr['krprogress']); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top width=<?php echo($widthth2); ?> style="<?php echo($style[$s]); ?>" >Management:</td>
        <td class=tdgeneral width=<?php echo($widthtg); ?> style="<?php echo($stylel[$s]); ?>"><?php echo($kr['krmanagement']); ?></td>
    </tr>
    <?php
    } //active == y endif
        $s++;
        if($s>4) { $s=1; }
        $t++;
        if($row['id']<6) { $a++; }
    }   //while
mysql_close();
?>
</table><input type=hidden name=t value=<?php echo($t); ?>><input type=hidden name=a value=<?php echo($a); ?>>


<?php if($typ=="cp") {
$cpid = $kpi['kpicpid'];
$cprow = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital WHERE cpid = ".$cpid;
include("inc_db_con.php");
    $cprow = mysql_fetch_array($rs);
mysql_close();

?>
<h2>Capital Project Details</h2>
<table cellpadding=3 cellspacing=0 width=600>
    <tr>
        <td class=tdheaderl height=27 width=<?php echo($widthth); ?>>Reference:</td>
        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>><?php echo($cpid); ?></td>
    </tr>
<?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CP' AND headdetailyn = 'Y' ORDER BY headdetailsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            if($row['headfield'] != "progress" && $row['headfield'] != "wards" && $row['headfield'] != "area")
            {
            if($row['headrequired']=="Y")
            {
                $fields.="|".$row['headfield'];
            }
        if($dlist[$row['headfield']]['yn']=="Y" && $row['headfield']!="prog") {
            $dl = $dlist[$row['headfield']];
                    ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>>
                            <select name=<?php echo($row['headfield']); ?> id=<?php echo($row['headfield']); ?>>
                                <option selected value=X>--- SELECT ---</option>
                                <?php
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_".$dl['id']." WHERE yn = 'Y' ORDER BY value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $fld = "cp".$row['headfield']."id";
                                        if($row2['id']==$cprow[$fld]) {
                                            $seld = " selected ";
                                        } else {
                                            $seld = "";
                                        }
                                        ?><option <?php echo($seld); ?> value=<?php echo($row2['id']); ?>><?php echo($row2['value']); ?><?php if($dl['code']=="Y") { ?> (<?php echo($row2[$dl['fld']]); ?>)<?php } ?></option><?php
                                    }
                                mysql_close();
                                ?>
                            </select>
                        </td>
                    <?php
        } else {
        switch($row['headfield'])
        {
            case "cpstartdate":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker1" readonly="readonly" value="<?php echo(date("d M Y",$cprow['cpstartdate'])); ?>"> <input type=hidden size=10 name=cpstartdate id=startDate value="<?php echo(date("d_n_Y",$cprow['cpstartdate'])); ?>"></td>
                </tr>
                <?php
                break;
            case "cpenddate":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker2" readonly="readonly" value="<?php echo(date("d M Y",$cprow['cpenddate'])); ?>"> <input type=hidden size=10 name=cpenddate value="<?php echo(date("d_n_Y",$cprow['cpenddate'])); ?>" id=endDate></td>
                </tr>
                <?php
                break;
            case "cpstartactual":
                if(checkIntRef($cprow['cpstartactual'])) {
                    $value = date("d M Y",$cprow['cpstartactual']);
                } else {
                    $value = "";
                }
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker3" readonly="readonly" value="<?php echo($value); ?>"> <input type=hidden size=10 name=cpendactual value="<?php echo(date("d_n_Y",$cprow['cpendactual'])); ?>" id=endActual></td>
                </tr>
                <?php
                break;
            case "cpendactual":
                if(checkIntRef($cprow['cpendactual'])) {
                    $value = date("d M Y",$cprow['cpendactual']);
                } else {
                    $value = "";
                }
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker4" readonly="readonly" value="<?php echo($value); ?>"> <input type=hidden size=10 name=cpendactual value="<?php echo(date("d_n_Y",$cprow['cpendactual'])); ?>" id=endActual></td>
                </tr>
                <?php
                break;
            case "cpproject":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><?php echo("<textarea rows=5 cols=30 name=".$row['headfield']." id=".$row['headfield'].">".$cprow['cpproject']."</textarea>  <span style=\"font-size: 7.5pt;\">(max ".$row['headmaxlen']." characters)</span>"); ?></td>
                </tr>
                <?php
                break;
					case "fundsource":
						echo "<tr>";
						echo "<td class=tdheaderl width=120>".$row['headdetail'].":</td>";
                        echo "<td id=".$row['cpid']."-".$r." style=\"".$style['kpi'][3][$st]."\">";
                        $sql2 = "SELECT DISTINCT w.id, w.value FROM assist_".$cmpcode."_".$modref."_capital_source c, assist_".$cmpcode."_".$modref."_list_fundsource w WHERE w.id = c.cssrcid AND c.cscpid = ".$kpi['kpicpid']." AND csyn = 'Y' AND w.yn = 'Y'";
                        include("inc_db_con2.php");
							while($row2 = mysql_fetch_array($rs2)) {
                                echo($row2['value']."; "); 
							}
                        mysql_close($con2);
						echo "</td></tr>";
						break;
            case "progress":
                break;
            case "wards":
                ?>
                <tr>
                    <td class=tdheaderl width=120 valign=top><?php echo($row['headdetail']); ?>:<input type=hidden id=f size=2 value=<?php echo($f); ?>></td>
                    <td class=tdgeneral>
                    <?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $c = 0;
                            $c2 = 0;
                                ?>
                                <table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
                                    <tr>
                                        <td class=tdgeneral style="border: 0px solid #ffffff;"><input type=checkbox <?php if($cpwards[1]== "Y") { echo(" checked "); } ?> name=wards[] value=1 onclick="wardsAll(this);"> All</td>
                                        <?php
                                            $c++;
                                            $c2++;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                echo("<td class=tdgeneral style=\"border: 0px solid #ffffff;\"><input type=checkbox ");
                                                if($cpwards[$row2['id']] == "Y") { echo(" checked "); }
                                                echo("name=wards[] value=".$row2['id']." >".$row2['value']."</td>");
                                                $c++;
                                                if($c==5)
                                                {
                                                    $c = 0;
                                                    echo("</tr><tr>");
                                                }
                                                $c2++;
                                            }
                                        ?>
                                    </tr>
                                </table>
                                <?php
                        mysql_close($con2);
                    ?>
                <?php
                $f2 = $f+$c2;
                break;
            default:
                $leng = $row['headmaxlen'];
                if($leng > 30 || !checkIntRef($leng)) { $leng = 30; }
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
					<?php if($row['headfield']!= "cpvotenum") { ?>
                    <td class=tdgeneral><input type=text size=<?php echo($leng); ?> maxlength=<?php echo($row['headmaxlen']); ?> name=<?php echo($row['headfield']); ?> value="<?php echo($cprow[$row['headfield']]); ?>">  <span style="font-size: 7.5pt;">(max <?php echo($row['headmaxlen']); ?> characters)</span></td>
					<?php } else { ?>
                    <td class=tdgeneral><?php echo($cprow[$row['headfield']]); ?> <input type=hidden size=<?php echo($leng); ?> maxlength=<?php echo($row['headmaxlen']); ?> name=<?php echo($row['headfield']); ?> value="<?php echo($cprow[$row['headfield']]); ?>"></td>
					<?php } ?>
                </tr>
                <?php
                break;
        }
        }
            }//if not progress
            ?></tr>
            <?php
        }
    mysql_close();
    ?>
</table>
<h2 style="margin-top: 20px;" class=fc>Capital Project Progress</h2>
<table cellpadding=5 cellspacing=0 width=570>
<tr>
    <td class=tdgeneral width=130>&nbsp;</td>
    <td class=tdheader width=110 >Budget</td>
    <td class=tdheader width=110 >Actual</td>
    <td class=tdheader width=110 >% Spent - YTD</td>
    <td class=tdheader width=110 >R Spent - YTD</td>
</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_results WHERE crcpid = ".$cpid." ORDER BY crtimeid";
include("inc_db_con.php");
    $crrow = array();
    while($row = mysql_fetch_array($rs))
    {
        $crrow[$row['crtimeid']] = $row;
    }
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $totytd = 0;
    while($row = mysql_fetch_array($rs))
    {
    $accytd = 0;
        $rowspan=1;
        if($crrow[$row['id']]['craccpercytd']>0) { $accytd = $crrow[$row['id']]['craccpercytd']; }
        if($crrow[$row['id']]['crtotrandytd']>0) { $totytd = $crrow[$row['id']]['crtotrandytd']; }
?>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style[$s]); ?>" rowspan=<?php echo($rowspan); ?>><?php echo(date("d M Y",$row['eval'])); ?>:</td>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php if($crrow[$row['id']]['crbudget']>0 || $crrow[$row['id']]['cractual']>0 ) { if($crrow[$row['id']]['crbudget']==0) { echo("R -"); } else { echo("R ".number_format($crrow[$row['id']]['crbudget'],0)); } } ?>&nbsp;</td>
    <?php if($row['sval']<$today) { //if($row['sval']>$today) {  ?>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php if($crrow[$row['id']]['crbudget']>0 || $crrow[$row['id']]['cractual']>0 ) { echo("R ".number_format($crrow[$row['id']]['cractual'],0)); } ?>&nbsp;</td>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php echo(number_format($accytd,2)." %"); ?>&nbsp;</td>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php echo("R ".number_format($totytd,0)); ?>&nbsp;</td>
    <?php
        } else {   //if tp < today ?>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
    <?php
        }   //if tp < today
        $s++;
        if($s>4) { $s=1; }
    }
mysql_close();
?>
</table>
<?php } ?>


<table cellpadding=5 cellspacing=0 width=600 style="margin-top: 20px;">
    <tr>
        <td class=tdgeneral align=center>
            <input type=hidden id=plcact name=plcact size=2 value=N>
            <input type=hidden id=f2 size=2 value=<?php echo($f2); ?>>
            <input type=hidden id=f3 size=2 value=<?php echo($f3); ?>>
            <input type=hidden name=typ id=typ size=2 value=<?php echo($typ); ?>>
            <input type=hidden name=r id=r size=2 value=<?php echo($src); ?>>
            <input type=hidden name=origsub id=origsub size=2 value=<?php echo($origsub); ?>>
            <input type=hidden name=cpid id=cpid size=2 value=<?php echo($cpid); ?>>
            <input type=hidden name=kpiid id=kpiid size=2 value=<?php echo($kpiid); ?>>
            <input type=hidden name=fields value=<?php echo($fields); ?>>
            <input type=hidden name=plcid value="<?php echo($plc['plcid']); ?>">
            <input type=hidden name=refres value="<?php echo($refres); ?>">
            <input type=submit value="Save Changes" id=ad>&nbsp;&nbsp;&nbsp;<?php if($src != "k") { ?><input type=reset id=rst><?php } else { ?><input type=button id=rst onclick="kpiReset();" value=Reset>&nbsp;&nbsp;&nbsp;<input type=button value=Delete onclick="delKPI(<?php echo("'".$typ."',".$kpiid); ?>,<?php echo($dirid); ?>)"><?php } ?>
        </td>
    </tr>
</table>
<script type=text/javascript>
var p = document.getElementById('prog').value;
//alert(p);
if(p != "NEW")
{
    document.getElementById('progval').style.display = "none";
}


    var wall = document.getElementById('wall');
        var elements = document.getElementById('editkpi');
        var f = parseInt(document.getElementById('f').value);
        var f2 = parseInt(document.getElementById('f2').value);
        var z = 0;
        f++;
    if(wall.checked)
    {
        for(z=f;z<f2;z++)
        {
            elements[z].checked = false;
            elements[z].disabled = true;
        }
    }
    else
    {
        for(z=f;z<f2;z++)
        {
            elements[z].disabled = false;
        }
    }


</script>
<?php
}   //IF ERR ON DIR CHECK
}   //IF ERR ON SUB CHECK
}   //IF ERR ON KPI CHECK

if($refres=="Y")
{
include("inc_goback_history.php");
?>
</form>
<form name=plcformedit id=plcformedit method=post action=admin_kpi_edit_plc.php>
<input type=hidden name=plcid value="<?php echo($plc['plcid']); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=typ id=typ size=2 value=<?php echo($typ); ?>>
<input type=hidden name=src id=r size=2 value=<?php echo($src); ?>>
<input type=hidden name=cpid id=cpid size=2 value=<?php echo($cpid); ?>>
<input type=hidden name=dirid value=<?php echo($sub['subdirid']); ?>>
<input type=hidden name=subid value=<?php echo($kpi['kpisubid']); ?>>
</form>
<?php
}
?>
<script type=text/javascript>
<?php echo($js); ?>
</script>
</body>

</html>
