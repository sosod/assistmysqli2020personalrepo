<?php
//CALCULATE RESULTS
$budg['r'] = 0;
$budg['o'] = 0;
$budg['c'] = 0;
$actul['r'] = 0;
$actul['o'] = 0;
$actul['c'] = 0;


$sql = "SELECT c.*, d.dirid, s.subid FROM assist_".$cmpcode."_".$modref."_finance_cashflow c ";
$sql.= ", assist_".$cmpcode."_".$modref."_finance_lineitems l";
$sql.= ", assist_".$cmpcode."_".$modref."_list_time t ";
$sql.= ", assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s";
$sql.= " WHERE c.cfyn = 'Y'";
$sql.= " AND l.lineyn = 'Y'";
$sql.= " AND d.dirid = s.subdirid AND l.linesubid = s.subid ";
$sql.= " AND l.lineid = c.cflineid";
$sql.= " AND t.id = c.cftimeid";
$sql.= " AND t.sort <= ".$wt;
$sql.= " AND t.sort >= ".$wf;
    switch($dtype)
    {
        case "d":
            $sql.= " AND d.dirid = ".$did;
            break;
        case "s":
            $sql.= " AND l.linesubid = ".$did;
            break;
        default:
            break;
    }
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        if($who2=="Y")
        {
            if($dtype == "d")
            {
                $moreid = $row['subid'];
            }
            else
            {
                $moreid = $row['dirid'];
            }
        }
        else
        {
            $moreid = 0;
        }
        $budg['r'] = $budg['r'] + $row['cfrev8'];
        $budg['o'] = $budg['o'] + $row['cfop8'];
        $budg['c'] = $budg['c'] + $row['cfcp8'];
        $actul['r'] = $actul['r'] + $row['cfrev2'];
        $actul['o'] = $actul['o'] + $row['cfop2'];
        $actul['c'] = $actul['c'] + $row['cfcp2'];
        $budg[$moreid]['r'] = $budg[$moreid]['r'] + $row['cfrev8'];
        $budg[$moreid]['o'] = $budg[$moreid]['o'] + $row['cfop8'];
        $budg[$moreid]['c'] = $budg[$moreid]['c'] + $row['cfcp8'];
        $actul[$moreid]['r'] = $actul[$moreid]['r'] + $row['cfrev2'];
        $actul[$moreid]['o'] = $actul[$moreid]['o'] + $row['cfop2'];
        $actul[$moreid]['c'] = $actul[$moreid]['c'] + $row['cfcp2'];
    }
mysql_close();

if( (array_sum($budg) + array_sum($actul) ) > 0 ) {

/*echo("<P>");
print_r($budg);
echo("<P>&nbsp;</p><p>");
print_r($actul);
*/




//GENERATE GRAPH
/*
 * Read README file first.
 *
 * This example shows how to create a simple bar chart with a few configuration
 * directives.
 * The values are from http://www.globalissues.org/article/26/poverty-facts-and-stats
 */

// Require necessary files
if($doc == "main")
{
    require("lib/AmBarChart.php");
}
else
{
    require("../lib/AmBarChart.php");
// Alls paths are relative to your base path (normally your php file)
// Path to swfobject.js
AmChart::$swfObjectPath = "lib/swfobject.js";
// Path to AmCharts files (SWF files)
AmChart::$libraryPath = "lib/amcharts";
// Path to jquery.js
AmChart::$jQueryPath = "lib/jquery.js";
}


// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmBarChart("fin_".$who);

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
$chart->setTitle("<p class=charttitle>".$titletxt."</p>");

// Add a label to describe the X values (inside the chart).
//$chart->addLabel("The values on the X axis describe the Purchasing Power in USD Dollars a day.", 0, 20);

// Add all values for the X axis
$chart->addSerie("r", "Revenue");
$chart->addSerie("o", "Operational Expenditure");
$chart->addSerie("c", "Capital Expenditure");

// Define graphs data
$budget = array("r" => $budg['r'], "o" => $budg['o'], "c" => $budg['c']);
$actual = array("r" => $actul['r'], "o" => $actul['o'], "c" => $actul['c']);
//$cap = array("b" => 40686051, "a" => 31473248);

// Add graphs
$chart->addGraph("budget", "Budget", $budget, array("color" => "#FF9900"));
$chart->addGraph("actual", "Actuals", $actual, array("color" => "#009900"));
//$chart->addGraph("cap", "Capex", $cap);//, array("color" => "#DD0000"));
/*$chart->addGraph("oa", "Opex (Actual)", $oa, array("color" => "#AA0000"));
$chart->addGraph("cb", "Capex (Budget)", $cb, array("color" => "#FF9900"));
$chart->addGraph("ca", "Capex (Actual)", $ca, array("color" => "#CD7B00"));
*/
//SET CONFIG
$chart->setConfigAll(array(
"width" => 600,
"height" => 400,
"depth" => 10,
"angle" => 45,
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 20,
"plot_area.margins.left" => 100,
"plot_area.margins.bottom" => 40,
"background.border_alpha" => 0,
"legend.enabled" => "false",
/*"legend.y" => "90%",
"legend.x" => "25%",
"legend.width" => "50%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 8,
"legend.align" => "center",*/
"balloon.enabled" => "true",
"balloon.alpha" => 80,
"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125;&#93;&#93;>",
"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
"column.data_labels_text_color" => "#ffffff",
"column.width" => 75
//"column.spacing" => 20,
//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"

));



?>
<style type=text/css>
table {
    border: 1px solid #ffffff;
}
table td {
    border: 1px solid #ffffff;
}
</style>
<div align=center>
<!-- <table cellpadding=10 cellspacing=0 style="border: 0px solid #ddd;">
    <tr>
        <td class=tdgeneral align=center style="border: 0px solid #ddd;"> -->
<?php
echo html_entity_decode($chart->getCode());

?>
<!--        </td> -->
<!--        <td class=tdgeneral align=center style="border: 1px solid #dddddd;"> -->
            <table cellpadding=3 cellspacing=0 width=400>
                <tr>
                    <td class=tdgeneral width=60>&nbsp;</td>
                    <td class=tdgeneral width=170 align=center style="background-color: #FF9900; font-weight: bold;">Adjusted Budget</td>
                    <td class=tdgeneral width=170 align=center style="background-color: #009900; font-weight: bold;">Actuals</td>
                </tr>
                <tr>
                    <td class=tdgeneral align=right><b>Revenue:</b></td>
                    <td class=tdgeneral align=right style="background-color: #FED97F;">&nbsp;R <?php echo(number_format($budg['r'],2)); ?></td>
                    <td class=tdgeneral align=right style="background-color: #BCFFB1">&nbsp;R <?php echo(number_format($actul['r'],2)); ?></td>
                </tr>
                <tr>
                    <td class=tdgeneral align=right><b>Opex:</b></td>
                    <td class=tdgeneral align=right style="background-color: #FED97F;">&nbsp;R <?php echo(number_format($budg['o'],2)); ?></td>
                    <td class=tdgeneral align=right style="background-color: #BCFFB1">&nbsp;R <?php echo(number_format($actul['o'],2)); ?></td>
                </tr>
                <tr>
                    <td class=tdgeneral align=right><b>Capex:</b></td>
                    <td class=tdgeneral align=right style="background-color: #FED97F;">&nbsp;R <?php echo(number_format($budg['c'],2)); ?></td>
                    <td class=tdgeneral align=right style="background-color: #BCFFB1">&nbsp;R <?php echo(number_format($actul['c'],2)); ?></td>
                </tr>
            </table>
<?php
}
else
{
echo("<div style=\"text-align: center\"><p><span style=\"font-weight: bold;\">".$titletxt."</span></p>");
echo("<p>No Financials to display.</p></div>");
}


if($who2 == "Y") { ?>            <hr style="color: #555555" size=1> <?php } ?>
<!--        </td>
    </tr> -->
<?php
if($who2 == "Y")
{
    $morerow = array();
    if($dtype=="d")
    {
        $sql = "SELECT subid id, subtxt txt FROM assist_".$cmpcode."_".$modref."_dirsub, assist_".$cmpcode."_".$modref."_finance_lineitems WHERE lineyn = 'Y' AND linesubid = subid AND subdirid = ".$did." AND subyn = 'Y' ORDER BY subsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $morerow[$row['id']] = $row;
            }
        mysql_close();
    }
    else
    {
        $sql = "SELECT dirid id, dirtxt txt FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $morerow[$row['id']] = $row;
            }
        mysql_close();
    }
//echo("<P>&nbsp;</p><p>");
//print_r($morerow);
    if(count($morerow)>0)
    {
        $tdc = 1;
        foreach($morerow as $mr)
        {
            $mid = $mr['id'];
            $tdc++;
//            echo "<tr>";
            ?>
<!--                <td class=tdgeneral align=center style="border: 0px solid #dddddd; <?php //if($tdc==2) { echo("page-break-before: always"); $tdc=0; } ?>" valign=top> -->
                <?php
              //echo($mid);

$bud = $budg[$mid];
$actl = $actul[$mid];

/*echo("<P>".$mid."-".(count($bud)>0)."</p><p>");
print_r($bud);
echo("<P>".count($actl)."</p><p>");
print_r($actl);
*/
$rb = 0;
$ob = 0;
$cb = 0;
$ra = 0;
$oa = 0;
$ca = 0;

$rb = $bud['r'];
$ob = $bud['o'];
$cb = $bud['c'];
$ra = $actl['r'];
$oa = $actl['o'];
$ca = $actl['c'];

if(strlen($rb)==0) { $rb = 0; }
if(strlen($ob)==0) { $ob = 0; }
if(strlen($cb)==0) { $cb = 0; }
if(strlen($ra)==0) { $ra = 0; }
if(strlen($oa)==0) { $oa = 0; }
if(strlen($ca)==0) { $ca = 0; }

$tot = $rb + $ra + $ob + $oa + $cb + $ca;

if(count($bud)>0 || count($actl)>0)
{


$chart = new AmBarChart("fin_".$mid);

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
//$chart->setTitle("<span style=\"font-weight: bold;\">".$mr['txt']."</span>");
//if($tdc==2) {
$chart->setTitle("<p class=charttitle style=\"page-break-before: always;\">".$mr['txt']."</p>");
//$tdc=0;
//} else {
//$chart->setTitle("<p style=\"margin: 5 5 5 5; padding: 0 0 0 0;\"><span style=\"font-weight: bold;\">".$mr['txt']."</span></p>");
//}

// Add a label to describe the X values (inside the chart).
//$chart->addLabel("The values on the X axis describe the Purchasing Power in USD Dollars a day.", 0, 20);

// Add all values for the X axis
$chart->addSerie("r", "Revenue");
$chart->addSerie("o", "Operational Expenditure");
$chart->addSerie("c", "Capital Expenditure");

// Define graphs data
$budget = array("r" => $rb, "o" => $ob, "c" => $cb);
$actual = array("r" => $ra, "o" => $oa, "c" => $ca);
//$cap = array("b" => 40686051, "a" => 31473248);

// Add graphs
$chart->addGraph("budget", "Budget", $budget, array("color" => "#FF9900"));
$chart->addGraph("actual", "Actuals", $actual, array("color" => "#009900"));
//$chart->addGraph("cap", "Capex", $cap);//, array("color" => "#DD0000"));
/*$chart->addGraph("oa", "Opex (Actual)", $oa, array("color" => "#AA0000"));
$chart->addGraph("cb", "Capex (Budget)", $cb, array("color" => "#FF9900"));
$chart->addGraph("ca", "Capex (Actual)", $ca, array("color" => "#CD7B00"));
*/
//SET CONFIG
$chart->setConfigAll(array(
"width" => 600,
"height" => 400,
"depth" => 10,
"angle" => 45,
"font" => "Tahoma",
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 20,
"plot_area.margins.left" => 100,
"plot_area.margins.bottom" => 40,
"background.border_alpha" => 0,
"legend.enabled" => "false",
/*"legend.y" => "90%",
"legend.x" => "25%",
"legend.width" => "50%",
"legend.border_alpha" => 20,
"legend.text_size" => 9,
"legend.spacing" => 4,
"legend.margins" => 8,
"legend.align" => "center",*/
"balloon.enabled" => "true",
"balloon.alpha" => 80,
"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125;&#93;&#93;>",
"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
"column.data_labels_text_color" => "#ffffff",
"column.width" => 75
//"column.spacing" => 20,
//"column.data_labels" => "<!&#91;CDATA&#91;&#123;value&#125;%&#93;&#93;>"

));

echo html_entity_decode($chart->getCode());
?>
            <table cellpadding=3 cellspacing=0 width=400>
                <tr>
                    <td class=tdgeneral width=60>&nbsp;</td>
                    <td class=tdgeneral width=170 align=center style="background-color: #FF9900; font-weight: bold;">Adjusted Budget</td>
                    <td class=tdgeneral width=170 align=center style="background-color: #009900; font-weight: bold;">Actuals</td>
                </tr>
                <tr>
                    <td class=tdgeneral align=right><b>Revenue:</b></td>
                    <td class=tdgeneral align=right style="background-color: #FED97F;">&nbsp;R <?php echo(number_format($rb,2)); ?></td>
                    <td class=tdgeneral align=right style="background-color: #BCFFB1">&nbsp;R <?php echo(number_format($ra,2)); ?></td>
                </tr>
                <tr>
                    <td class=tdgeneral align=right><b>Opex:</b></td>
                    <td class=tdgeneral align=right style="background-color: #FED97F;">&nbsp;R <?php echo(number_format($ob,2)); ?></td>
                    <td class=tdgeneral align=right style="background-color: #BCFFB1">&nbsp;R <?php echo(number_format($oa,2)); ?></td>
                </tr>
                <tr>
                    <td class=tdgeneral align=right><b>Capex:</b></td>
                    <td class=tdgeneral align=right style="background-color: #FED97F;">&nbsp;R <?php echo(number_format($cb,2)); ?></td>
                    <td class=tdgeneral align=right style="background-color: #BCFFB1">&nbsp;R <?php echo(number_format($ca,2)); ?></td>
                </tr>
            </table>
            <hr style="color: #555555" size=1>
<!--            <hr style="page-break-after:always; color: #555555" size=1> -->

<?php
}
else
{
    echo("<b>".$mr['txt']."</b><br>&nbsp;<br>");
    echo("No financial results to display.");
    echo("            <hr style=\"color: #555555\" size=1>");
//    echo("            <hr style=\"page-break-after:always; color: #555555\" size=1>");
}

                
                ?>
<!--                </td>  -->
            <?php
//            echo "</tr>";
        }
    }
}
?>

<!--</table>-->
</div>

<?php
//print_r($budg);
?>
