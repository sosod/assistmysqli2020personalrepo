/* ON CHANGE OF USE PHASE YES/NO DROP DOWN */
function usePhase(me,c) {
    var fld;
    var target;
    fld = "ptype"+c;
    target = document.getElementById(fld);
    if(me.value == "Y")     //IF USEPHASE = Y > ENABLE
    {
        if(target.value == "C")     //IF PHASE TYPE = CUSTOM
        {
            var c2 = c - 1;
            //ENABLE PHASE DROP DOWN
            fld = "phase"+c;            target = document.getElementById(fld);            target.disabled = false;
            if(target.value != "X")     //IF PHASE DROP DOWN IS NOT --SELECT--
            {
                if(target.value == "NEW")   //IF PHASE DROP DOWN IS SET TO ADD NEW
                {
                    fld = "cust"+c;                    target = document.getElementById(fld);
                    target.style.display = "inline";    //DISPLAY ADD NEW CUSTOM FIELD
                    if(target.value.length > 0)   //IF ADD NEW CUSTOM FIELD IS NOT BLANK
                    {
                        //ENABLE ALL FIELDS
                        fld = "cal"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';                        fld = "start"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';                        fld = "end"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';                        fld = "tar"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';
                        if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = false; }
                    }
                    else
                    {
                        fld = "cust"+c;                    target = document.getElementById(fld);
                        target.style.display = "none";    //DISPLAY ADD NEW CUSTOM FIELD
                        //DISABLE ALL FIELDS
                        fld = "cal"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "start"+c;        target = document.getElementById(fld);       target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "end"+c;        target = document.getElementById(fld);        target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "tar"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';
                        if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = true; }
                    }
                }
                else        //IF PHASE DROP DOWN IS NOT X AND NOT NEW
                {
                        fld = "cust"+c;                    target = document.getElementById(fld);
                        target.style.display = "none";    //DISPLAY ADD NEW CUSTOM FIELD
                        //ENABLE ALL FIELDS
                        fld = "phase"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        fld = "cal"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';                        fld = "start"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';                        fld = "end"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';                        fld = "tar"+c;                        target = document.getElementById(fld);                        target.disabled = false;                        target.style.backgroundColor = 'ffffff';
                        if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = false; }
                }
            }
            else
            {
                fld = "cust"+c;                    target = document.getElementById(fld);
                target.style.display = "none";    //DISPLAY ADD NEW CUSTOM FIELD
                //DISABLE ALL FIELDS
                fld = "cal"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "start"+c;        target = document.getElementById(fld);       target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "end"+c;        target = document.getElementById(fld);        target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "tar"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';
                if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = true; }
            }
        }
        else        //IF PHASE TYPE IS NOT CUSTOM
        {
                    //ENABLE ALL FIELDS
            fld = "cal"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "start"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "end"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "tar"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';
            if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = false; }
        }
    }
    else        //IF USEPHASE = N > DISABLE
    {
        if(target.value == "C")     //IF PHASE TYPE = CUSTOM
        {
            var c2 = c - 1;
            fld = "app"+c2;            target = document.getElementById(fld);
            //alert(target.value);
            if(target.disabled == true || target.value == "N")
            {
                me.disabled = true;
            }
            else
            {
                fld = "phase"+c2;            target = document.getElementById(fld);
                if(target.value == "X" || target.value.length == 0 || target.disabled == true)
                {
                    me.disabled = true;
                }
                else
                {
                    if(target.value == "NEW")
                    {
                        fld = "cust"+c2;            target = document.getElementById(fld);
                        if(target.value.length == 0)
                        {
                            me.disabled = true;
                        }
                    }
                }
            }
            fld = "phase"+c;            target = document.getElementById(fld);
            target.disabled = true;     //DISABLE PHASE DROP DOWN
            fld = "cust"+c;            target = document.getElementById(fld);
            target.style.display = "none";  //HIDE ADD NEW CUSTOM FIELD
        }
                    //DISABLE ALL FIELDS
        fld = "cal"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "start"+c;        target = document.getElementById(fld);       target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "end"+c;        target = document.getElementById(fld);        target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "tar"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';
        if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = true; }
    }
    fld = "plcstartdate";
    target = document.getElementById(fld);
    if(target.value.length > 0)
    {
        c = c-1;
        startDate(c);
    }
    
}




/* ON CHANGE OF ADDITIONAL PHASE DROP DOWN */
function changePhase(me,c) {
    var fld;
    var target;

    fld = "cust"+c;
    target = document.getElementById(fld);
    if(me.value=="NEW")
    {
        target.style.display = "inline";
        if(target.value.length == 0)
        {
            //DISABLE ALL FIELDS
            fld = "cal"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "start"+c;        target = document.getElementById(fld);       target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "end"+c;        target = document.getElementById(fld);        target.value = "";        target.disabled = true;        target.style.backgroundColor = 'dddddd';        fld = "tar"+c;        target = document.getElementById(fld);        target.disabled = true;        target.style.backgroundColor = 'dddddd';
            if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = true; }
            if(c < (cust11-1))
            {
                var c2 = c + 1;
                fld = "app"+c2;            target = document.getElementById(fld);            target.disabled = true;
            }
        }
        else
        {
            if(c < (cust11-1))
            {
                var c2 = c + 1;
                fld = "app"+c2;            target = document.getElementById(fld);            target.disabled = false;
            }
        }
    }
    else
    {
        target.style.display = "none";
        if(me.value != "X")
        {
            //ENABLE ALL FIELDS
            fld = "cal"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "start"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "end"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "tar"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';
            if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = false; }
            if(c < (cust11-1))
            {
                var c2 = c + 1;
                fld = "app"+c2;            target = document.getElementById(fld);            target.disabled = false;
            }
        }
        else
        {
            if(c < (cust11-1))
            {
                var c2 = c + 1;
                fld = "app"+c2;            target = document.getElementById(fld);            target.disabled = true;
            }
        }
    }
    fld = "plcstartdate";
    target = document.getElementById(fld);
    if(target.value.length > 0)
    {
        var chk = "N";
        while(chk=="N")
        {
            c = c-1;
            if(c>0)
            {
                fld = "start"+c;                target = document.getElementById(fld);
                if(target.disabled == false)
                {
                    chk = "Y";
                }
            }
            else
            {
                chk = "E";
            }
        }
        startDate(c);
    }
}




/* ON CHANGE OF ADDITIONAL PHASE NEW PHASE TEXT BOX */
function newPhase(me,c) {
    var txt = me.value;
    var fld;
    var target;
    if(txt.length > 0)      //IF NEW PHASE FIELD IS NOT BLANK
    {
        //ENABLE ALL FIELDS
            fld = "cal"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "start"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "end"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';            fld = "tar"+c;            target = document.getElementById(fld);            target.disabled = false;            target.style.backgroundColor = 'ffffff';
        if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = false; }
            if(c < (cust11-1))
            {
                var c2 = c + 1;
                fld = "app"+c2;            target = document.getElementById(fld);            target.disabled = false;
            }
    }
    else
    {
        //DISABLE ALL FIELDS
            fld = "cal"+c;            target = document.getElementById(fld);            target.disabled = true;            target.style.backgroundColor = 'dddddd';            fld = "start"+c;            target = document.getElementById(fld);            target.disabled = true;            target.style.backgroundColor = 'dddddd';            fld = "end"+c;            target = document.getElementById(fld);            target.disabled = true;            target.style.backgroundColor = 'dddddd';            fld = "tar"+c;            target = document.getElementById(fld);            target.disabled = true;            target.style.backgroundColor = 'dddddd';
        if(doctype == "edit") { fld = "done"+c;            target = document.getElementById(fld);            target.disabled = true; }
            if(c < (cust11-1))
            {
                var c2 = c + 1;
                fld = "app"+c2;            target = document.getElementById(fld);            target.disabled = false;
            }
    }
    fld = "plcstartdate";
    target = document.getElementById(fld);
    if(target.value.length > 0)
    {
        var chk = "N";
        while(chk=="N")
        {
            c = c-1;
            if(c>0)
            {
                fld = "start"+c;                target = document.getElementById(fld);
                if(target.disabled == false)
                {
                    chk = "Y";
                }
            }
            else
            {
                chk = "E";
            }
        }
        startDate(c);
    }
}







/* ON CHANGE OF PLCSTARTDATE */
/* ON CHANGE OF PHASE START DATE */
/* ON CHANGE OF CALENDAR DAYS */
/* PARSEINT CAUSED ERROR ON AUG ETC. SO USED MULTIPLICATION TO CONVERT TO NUMERIC */
function startDate(d) {
    var d2;         //COUNT TO START RECORDING DATE
    var fld;        //ELEMENT ID NAME
    var target;     //ELEMENT OBJECT
    var sd;         //START DATE ARRAY
    var da = new Date();         //DATE OBJECT
    var date1;      //NEW DATE ARRAY
    var date2;      //NEW DATE TEXT
    var days        //CALENDAR DAYS
    var days2       //GETDATE + CALENDAR DAYS
    
    //IF POSITION COUNT > 0 i.e. DATE IS NOT PLCSTARTDATE
    if(d > 0)
    {
        fld = "start"+d;
        d2 = d;
    }
    else
    {
        fld = "plcstartdate";
        d2 = 1;
    }
    target = document.getElementById(fld);  //GET DATE ELEMENT
if(target.value.length > 0)
{
    sd = target.value.split("-");           //CREATE DATE ARRAY
    sd[1] = sd[1] * 1;                      //REDUCE MONTH FOR JAVASCRIPT
    sd[1]--;
    da.setYear(sd[0]);                      //SET YEAR FROM DATE ARRAY
    da.setMonth(sd[1],sd[2]);               //SET MONTH AND DAY FROM DATE ARRAY
    for(f=d2;f<c;f++)                       //FOREACH DOCUMENT ELEMENT DATE OBJECT
    {
        fld = "app"+f;
        target = document.getElementById(fld);
        //IF USE PHASE ELEMENT IS YES
        if(target.value == "Y")
        {
            //IF CALENDAR DAYS IS NOT DISABLED
            fld = "cal"+f;
            target = document.getElementById(fld);
            if(!target.disabled)
            {
                if(target.value.length == 0)
                {
                    target.value = 1;
                }
                //GET CALENDAR DAYS
                days = target.value;
                //VALIDATE DAYS
                if(days == parseFloat(days) && days.length > 0)
                {
                    //IF DAYS IS NOT A WHOLE NUMBER
                    if(days != parseInt(days))
                    {
                        target.value = parseInt(days);
                    }
                    days = parseInt(days);
                    //IF DAYS IS LESS THAN 1 i.e. 0
                    if(days < 1)
                    {
                        target.value = 1;
                        days = 1;
                    }
                    //REDUCE DAYS BY 1
                    days = days - 1;
                    //GENERATE START DATE FOR DISPLAY
                    date1 = new Array(da.getYear(),parseInt(da.getMonth())+1,parseInt(da.getDate()));
                    if(date1[1]<10) { date1[1] = "0" + date1[1]; }
                    if(date1[2]<10) { date1[2] = "0" + date1[2]; }
                    date2 = date1.join("-");
                    //LOAD START DATE
                    fld = "start"+f;
                    document.getElementById(fld).value = date2;
                    //RESET DATE ARRAY
                    sd = "";                                //RESET sd VARIABLE
                    sd = new Array();
                    sd = date2.split("-");                  //CREATE DATE ARRAY
                    sd[1] = sd[1] * 1;                      //REDUCE MONTH FOR JAVASCRIPT
                    sd[1]--;
                    da = new Date();                        //RESET da VARIABLE
                    da.setYear(sd[0]);                      //SET YEAR FROM DATE ARRAY
                    da.setMonth(sd[1],sd[2]);               //SET MONTH AND DAY FROM DATE ARRAY
                    //CALCULATE END DATE
                    days2 = da.getDate();
                    days2 = parseInt(days2);
                    days2 = days2 + days;
                    da.setDate(days2);
                    //GENERATE END DATE FOR DISPLAY
                    date1 = new Array(da.getYear(),da.getMonth(),da.getDate());
                    date1[1] = date1[1] + 1;
                    if(date1[1]<10) { date1[1] = "0" + date1[1]; }
                    if(date1[2]<10) { date1[2] = "0" + date1[2]; }
                    date2 = date1.join("-");
                    //LOAD END DATE
                    fld = "end"+f;
                    document.getElementById(fld).value = date2;
                    //CALCULATE NEW START DATE
                    days2 = da.getDate();
                    days2 = days2 + 1;
                    da.setDate(days2);
                }
            }
        }
    }
    //CALCULATE FINAL END DATE
    days2 = da.getDate();
    days2 = days2 - 1;
    da.setDate(days2);
    //LOAD FINAL END DATE
    fld = "plcenddate";
    document.getElementById(fld).value = date2;
}//if date length > 0
}

/* ON CHANGE OF PHASE END DATE */
/* THEN UPDATE CALENDAR DAYS AND NEXT STARTDATE */
function endDate(d) {
    var d2;         //COUNT TO START RECORDING DATE
    var fld;        //ELEMENT ID NAME
    var target;     //ELEMENT OBJECT
    var sd;         //START DATE ARRAY
    var da = new Date();         //DATE OBJECT
    var da2 = new Date();
    var date1;      //NEW DATE ARRAY
    var date2;      //NEW DATE TEXT
    var days        //CALENDAR DAYS
    var days2       //GETDATE + CALENDAR DAYS

    //IF POSITION COUNT > 0 i.e. DATE IS NOT PLCSTARTDATE
        fld = "end"+d;
        d2 = d+1;
    target = document.getElementById(fld);  //GET DATE ELEMENT
    ed = target.value.split("-");           //CREATE DATE ARRAY
    ed[1] = ed[1] * 1;                      //REDUCE MONTH FOR JAVASCRIPT
    ed[1]--;
    ed[2]++;
    da.setYear(ed[0]);                      //SET YEAR FROM DATE ARRAY
    da.setMonth(ed[1],ed[2]);               //SET MONTH AND DAY FROM DATE ARRAY
    //STARTDATE
        fld = "start"+d;
    target = document.getElementById(fld);  //GET DATE ELEMENT
    sd = target.value.split("-");           //CREATE DATE ARRAY
    sd[1] = sd[1] * 1;                      //REDUCE MONTH FOR JAVASCRIPT
    sd[1]--;
    sd[2]++;
    da2.setYear(sd[0]);                      //SET YEAR FROM DATE ARRAY
    da2.setMonth(sd[1],sd[2]);               //SET MONTH AND DAY FROM DATE ARRAY
    var newcal = ((da - da2) / 86400000) + 1;
    fld = "cal"+d;
    target = document.getElementById(fld);
    target.value = newcal;
    //GENERATE START DATE FOR DISPLAY
    date1 = new Array(da.getYear(),parseInt(da.getMonth())+1,parseInt(da.getDate()));
    if(date1[1]<10) { date1[1] = "0" + date1[1]; }
    if(date1[2]<10) { date1[2] = "0" + date1[2]; }
    date2 = date1.join("-");
    //LOAD START DATE
    fld = "start"+d2;
    document.getElementById(fld).value = date2;
    for(f=d2;f<c;f++)                       //FOREACH DOCUMENT ELEMENT DATE OBJECT
    {
        fld = "app"+f;
        target = document.getElementById(fld);
        //IF USE PHASE ELEMENT IS YES
        if(target.value == "Y")
        {
            //IF CALENDAR DAYS IS NOT DISABLED
            fld = "cal"+f;
            target = document.getElementById(fld);
            if(!target.disabled)
            {
                if(target.value.length == 0)
                {
                    target.value = 1;
                }
                //GET CALENDAR DAYS
                days = target.value;
                //VALIDATE DAYS
                if(days == parseFloat(days) && days.length > 0)
                {
                    //IF DAYS IS NOT A WHOLE NUMBER
                    if(days != parseInt(days))
                    {
                        target.value = parseInt(days);
                    }
                    days = parseInt(days);
                    //IF DAYS IS LESS THAN 1 i.e. 0
                    if(days < 1)
                    {
                        target.value = 1;
                        days = 1;
                    }
                    //REDUCE DAYS BY 1
                    days = days - 1;
                    //GENERATE START DATE FOR DISPLAY
                    date1 = new Array(da.getYear(),parseInt(da.getMonth())+1,parseInt(da.getDate()));
                    if(date1[1]<10) { date1[1] = "0" + date1[1]; }
                    if(date1[2]<10) { date1[2] = "0" + date1[2]; }
                    date2 = date1.join("-");
                    //LOAD START DATE
                    fld = "start"+f;
                    document.getElementById(fld).value = date2;
                    //RESET DATE ARRAY
                    sd = "";                                //RESET sd VARIABLE
                    sd = new Array();
                    sd = date2.split("-");                  //CREATE DATE ARRAY
                    sd[1] = sd[1] * 1;                      //REDUCE MONTH FOR JAVASCRIPT
                    sd[1]--;
                    da = new Date();                        //RESET da VARIABLE
                    da.setYear(sd[0]);                      //SET YEAR FROM DATE ARRAY
                    da.setMonth(sd[1],sd[2]);               //SET MONTH AND DAY FROM DATE ARRAY
                    //CALCULATE END DATE
                    days2 = da.getDate();
                    days2 = parseInt(days2);
                    days2 = days2 + days;
                    da.setDate(days2);
                    //GENERATE END DATE FOR DISPLAY
                    date1 = new Array(da.getYear(),da.getMonth(),da.getDate());
                    date1[1] = date1[1] + 1;
                    if(date1[1]<10) { date1[1] = "0" + date1[1]; }
                    if(date1[2]<10) { date1[2] = "0" + date1[2]; }
                    date2 = date1.join("-");
                    //LOAD END DATE
                    fld = "end"+f;
                    document.getElementById(fld).value = date2;
                    //CALCULATE NEW START DATE
                    days2 = da.getDate();
                    days2 = days2 + 1;
                    da.setDate(days2);
                }
            }
        }
    }
    //CALCULATE FINAL END DATE
    days2 = da.getDate();
    days2 = days2 - 1;
    da.setDate(days2);
    //LOAD FINAL END DATE
    fld = "plcenddate";
    document.getElementById(fld).value = date2;
}








/* ON CHANGE OF TARGET */
function targetChange(t) {
    var fld;
    var target;
    var tartot = 0;
    fld = "tar"+t;
    target = document.getElementById(fld);
    if(target.disabled == false)
    {
        target.style.backgroundColor = "ffffff";
        target.style.color = "000000";
    }
    document.getElementById('subbut').disabled = false;

    if(target.value.length == 0)
    {
        target.value = 0;
    }
    if(target.value > 100)
    {
        target.style.backgroundColor = "cc0001";
        target.style.color = "ffffff";
        alert("Error: You cannot enter a target greater than 100%.");
        document.getElementById('subbut').disabled = true;
    }
    else
    {
        fld = "";
        target = "";
        erryn = "N";
        for(z=1;z<c;z++)
        {
            fld = "tar"+z;
            target = document.getElementById(fld);
            if(target.disabled == false)
            {
                if(target.value == parseFloat(target.value))
                {
                    tartot = tartot + parseFloat(target.value);
                }
                else
                {
                    erryn = "Y";
                    target.style.backgroundColor = "cc0001";
                    target.style.color = "ffffff";
                }
            }
        }
        fld = "tartot";
        target = document.getElementById(fld);
        target.innerText = tartot;
        if(tartot > 100)
        {
            target.style.backgroundColor = "cc0001";
            target.style.color = "ffffff";
            alert("Error: Your project cannot have a total target greater than 100%.");
            document.getElementById('subbut').disabled = true;
        }
        else
        {
            if(erryn == "Y")
            {
                alert("Error: There is an invalid target.");
                document.getElementById('subbut').disabled = true;
            }
            else
            {
                target.style.backgroundColor = "ffffff";
                target.style.color = "000000";
                document.getElementById('subbut').disabled = false;
            }
        }
    }
}





/* ONCHANGE OF PROCUREMENT PROJECT CATEGORY */
function procCate(me) {
    var p;
    var i = 0;
    for(p=procstart;p<procend;p++)
    {
        document.getElementById('cal'+p).value = days[me.value][i];
        i++;
    }
    startDate(procstart);
}


function ValidateTrue() {
    var abc = true;
}



/* VALIDATE FORM */
function Validate() {
    //kpi name
    var projectname = parent.main.plc.plcprojectname.value;
    //plcstartdate
    var startdate = document.plc.plcstartdate.value;
    //plcenddate
    var enddate = document.plc.plcenddate.value;
    //total target
    var tartot = document.getElementById('tartot').innerText;
    //required fields
    var reqyn;


    if(projectname.length > 0 && startdate.length > 0 && enddate.length > 0 && (tartot < 100 || tartot == 100))
    {
        reqyn = validRequired();
        if(reqyn == "Y")
        {
            for(e=1;e<c;e++)
            {
                document.getElementById('app'+e).disabled = false;
            }
//            alert("true");
            setTimeout("ValidateTrue()",1000);
            return true;
        }
        else
        {
            alert("Please complete the required fields as highlighted.");
            return false;
        }
    }
    else
    {
        if(projectname.length == 0) { document.plc.plcprojectname.style.backgroundColor = "cc0001"; } else { document.plc.plcprojectname.style.backgroundColor = "ffffff"; }
        if(startdate.length == 0) { document.plc.plcstartdate.style.backgroundColor = "cc0001"; } else { document.plc.plcstartdate.style.backgroundColor = "ffffff"; }
        if(enddate.length == 0) { document.plc.plcenddate.style.backgroundColor = "cc0001"; } else { document.plc.plcenddate.style.backgroundColor = "ffffff"; }
        reqyn = validRequired();
        alert("Please complete the required fields as highlighted.");
        return false;
    }
    return false;
}

function validRequired()
{
    var reqdays;
    var reqstart;
    var reqend;
    var x;
    var reqyn = "Y";
        for(x in req)
        {
                reqdays = document.getElementById('cal'+req[x]).value;
                reqstart = document.getElementById('start'+req[x]).value;
                reqend = document.getElementById('end'+req[x]).value;
                if(reqdays.length == 0 || reqstart.length == 0 || reqend.length == 0)
                {
                    reqyn = "N";
                }
                if(reqdays.length == 0) { styleReq('cal'+req[x]); } else { styleGot('cal'+req[x]); }
                if(reqstart.length == 0) { styleReq('start'+req[x]); } else { styleGot('start'+req[x]); }
                if(reqend.length == 0) { styleReq('end'+req[x]); } else { styleGot('end'+req[x]); }
        }
    return reqyn;
}

function styleReq(fld) {
    document.getElementById(fld).style.backgroundColor = "cc0001";
    document.getElementById(fld).style.color = "ffffff";
}

function styleGot(fld) {
    document.getElementById(fld).style.backgroundColor = "ffffff";
    document.getElementById(fld).style.color = "000000";
}








function doneChange() {
//    alert("DC");
    var target;
    var fld;
    var target2;
    var target3;
        for(dc=1;dc<c;dc++)
        {
            fld = "done"+dc;
            target = document.getElementById(fld);
            fld = "donedate"+dc;
            target2 = document.getElementById(fld);
            fld = "app"+dc;
            target3 = document.getElementById(fld);
            if(target3.value == "N")
            {
                target.disabled = true;
                target2.style.backgroundColor = "dddddd";
                target2.disabled = true;
            }
        }
}




function doneDateChange(d) {
    var fld = "donedate"+d;
    var target = document.getElementById(fld);
    if(target.value.length>0)
    {
        fld = "done"+d;
        document.getElementById(fld).checked = true;
        fld = "done2"+d;
        document.getElementById(fld).value = "Y";
    }
}

function doneChk(d) {
    var fld = "done"+d;
    var target = document.getElementById(fld);
    if(target.checked == true)
    {
        fld = "done2"+d;
        document.getElementById(fld).value = "Y";
    }
    else
    {
        fld = "done2"+d;
        document.getElementById(fld).value = "N";
    }
}

