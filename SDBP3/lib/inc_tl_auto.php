<?php 
	//include("inc_ignite.php"); 
/************** GENERIC FUNCTIONS *********************/
function average($a){
  return array_sum($a)/count($a) ;
}

function formatResultI($tt,$val,$name,$t) {
	$echo = "";
	switch($tt) {
		case "R":
			$echo = "R <input type=text value=\"".$val."\" name=\"".$name."[]\" size=3 id=$name$t>";
			break;
		case "%":
			$echo = "<input type=text value=\"".$val."\" name=\"".$name."[]\" size=3 id=$name$t>".$tt;
			break;
		case "#":
		default:
			$echo = "<input type=text value=\"".$val."\" name=\"".$name."[]\" size=3 id=$name$t>";
			break;
	}
	return $echo;
}
function formatResult($tt,$val) {
	$echo = "";
	switch($tt) {
		case "R":
			$echo = "R ".$val;
			break;
		case "%":
			$echo = $val.$tt;
			break;
		case "#":
		default:
			$echo = $val;
			break;
	}
	return $echo;
}

/*** END GENERIC FUNCTIONS ***/	



/*****************************************************************/
/*** COMPANY SPECIFIC TOP LEVEL UPDATE FUNCTION ******************/
/*****************************************************************/

function updateTopLevel($cmpcode,$dbref) {
$cmpcode3 = $cmpcode;
global $modref;
$updatecount = 0;
$gdate = getdate();
$today = $gdate[0];
global $krsetup;
//echo "<p>"; print_r($krsetup);

	/*********** GET DISPLAY INFO *******************/
		$dir = array();
		$sql = "SELECT dirid, dirtxt FROM ".$dbref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				$dir[] = $row;
			}
		mysql_close($con);

/************** GET LISTS *********************/
$sql = "SELECT headlist, headfield FROM ".$dbref."_headings WHERE headtype = 'TL' AND headlistyn = 'Y' AND headfield <> 'topcalctype' AND headfield <> 'topvalue' ORDER BY headlistsort";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$head[$row['headfield']] = $row;
	}
mysql_close($con);
$head['toptargetannual'] = array('headlist'=>"Annual Target",'headfield'=>"toptargetannual");
$head['toptargetrevised'] = array('headlist'=>"Revised Target",'headfield'=>"toptargetrevised");
$head['assockpi'] = array('headlist'=>"Assoc. SDBIP KPIs",'headfield'=>"assockpi");
$head['topcalctype'] = array('headlist'=>"Calculation Type",'headfield'=>"topcalctype");
$head['topcalcaction'] = array('headlist'=>"Calculation Action",'headfield'=>"topcalcaction");
$head['toptargettypeid'] = array('headlist'=>"Target Type",'headfield'=>"toptargettypeid");

$sql = "SELECT id, sval, eval, activeKPI as active FROM ".$dbref."_list_time WHERE id IN (1,3,4,6,7,9,10,12)";
include("inc_db_con.php");
$sval = 0;
	while($row = mysql_fetch_assoc($rs)) {
		$id = $row['id'];
		switch($id) {
			case 1: case 4: case 7: case 10:
				$sval = $row['sval'];
				break;
			case 3: case 6: case 9: case 12:
				$time[$id] = $row;
				$time[$id]['sval'] = $sval;
				$time[$id]['active'] = "";
				break;
		}
	}
mysql_close($con);
$sql = "SELECT id, activeKPI as active FROM ".$dbref."_list_time";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$id = $row['id'];
		$i = ceil($id/3);
		$i*=3;
		$time[$i]['active'].=strtoupper($row['active']);
	}
mysql_close($con);

$lists = array();
$sql = "SELECT id, value FROM ".$dbref."_list_gfs WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['gfs'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_idpgoal WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['idp'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_area WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['area'][$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value, numval FROM ".$dbref."_list_wards WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['wards'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_kpicalctype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$calctype[$row['code']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_munkpa WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['munkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_natkpa WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['natkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_targettype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$targettype[$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_tas WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['tas'][$row['id']] = $row;
	}
mysql_close($con);

$kpimsr = array();
$sql = "SELECT kpiid, kpimsr, kpicalctype, kpitargettypeid FROM ".$dbref."_kpi WHERE kpiyn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$kpimsr[$row['kpimsr']][$row['kpiid']] = $row;
	}
mysql_close($con);

$echo = "";
$echo.= "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><title>www.Ignite4u.co.za</title>";
$echo.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /></head>";
$echo.= "<link rel=\"stylesheet\" href=\"https://assist.ignite4u.co.za/default.css\" type=\"text/css\">";
$echo.= chr(10);
	foreach($krsetup as $krs) {
		$styler[$krs['sort']] = "background-color: ".$krs['color']."; color: ".$krs['color']."; text-align: center;";
		$stylecss.=".krr".$krs['sort']." { ".$styler[$krs['sort']]." }";
	}
$echo.= "<style type=text/css>";
$echo.= $stylecss;
$echo.= "input { text-align: right; }";
$echo.= "table th { background-color: #CC0001; }";
$echo.= ".3 { background-color: #CC0001; }";
$echo.= ".6 { background-color: #FE9900; }";
$echo.= ".9 { background-color: #009900; }";
$echo.= ".12 { background-color: #000099; }";
$echo.= ".0 { background-color: #999999; }";
$echo.= ".3b { background-color: #ffcccc; }";
$echo.= ".6b { background-color: #ffeaca; }";
$echo.= ".9b { background-color: #ceffde; }";
$echo.= ".12b { background-color: #ccccff; }";
$echo.= ".0b { background-color: #EEEEEE; }";
$echo.= ".3n { background-color: #ffa1a1; font-weight: bold; }";
$echo.= ".6n { background-color: #ffd38c; font-weight: bold; }";
$echo.= ".9n { background-color: #75FFA3; font-weight: bold; }";
$echo.= ".12n { background-color: #a1a1ff; font-weight: bold; }";
$echo.= ".comm { font-weight: normal; font-size: 7pt; line-height: 8pt; }";
$echo.= ".kpi { font-weight: normal; font-style: italic; text-align: right;}";
$echo.= ".kpitxt { color: #777777; }";
$echo.= "</style>";
$echo.= chr(10)."<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>";

$echo.= "<h1>SDBIP $modtxt: Admin ~ Top Level: Update</h1>";
/*if(strlen($rest)>0 && count($res)>0 && $act == "SAVE") {
	$result[0] = $rest;
	$result[1] = implode("&nbsp;&nbsp;",$res);
	displayResult($result);
} else {
	if($act != "SAVE") {
		$result[0] = "error";
		$result[1] = "";
	}
}*/
$c = 0;

$echo.= "<table cellpadding=3 cellspacing=0>";
/*********** HEADINGS ****************/ 
//$c = 1;
$echo.= chr(10)."<tr>";
	$echo.= "<th rowspan=2>KPI ID</th>"; $c++;
	$echo.= "<th rowspan=2 style=\"padding: 0 70 0 70\">KPI Name</th>"; $c++;
	foreach($head as $h) {
		$echo.= "<th rowspan=2";
		switch($h['headfield']) {
			case "topunit":
			case "toprisk":
			case "idp":
			case "gfs":
				$echo.= " style=\"padding: 0 70 0 70\""; 
				break;
			default:
				$echo.= " style=\"padding: 0 10 0 10\""; 
				break;
		} 
		$echo.= ">".$h['headlist']."</th>"; $c++;
	}
	
	foreach($time as $t) {
		if($t['sval']<$today) { 
			if($t['active']=="NNN") {
				$tspan = 5;
			} else {
				$tspan = 8+4; 
			}
		} else { $tspan = 2; }
		$echo.= "<th colspan=$tspan class=".$t['id'].">Quarter ending <u>".date("d M Y",$t['eval'])."</u>"; $c+=$tspan;
	}
$echo.= "</tr>";
$echo.= "<tr>";
	foreach($time as $t) {
		$echo.= "<th class=".$t['id'].">Target</th>"; 
		if($t['sval']<$today) {
			switch($t['active']) {
				case "NNN":
					$echo.= "<th class=".$t['id'].">Actual</th>"; 
					$echo.= "<th class=".$t['id'].">R</th>"; 
					$echo.= "<th class=".$t['id'].">Dept. Comment</th>"; 
					break;
				default:
					$echo.= "<th class=".$t['id'].">Original Actual";
					$echo.= "<input type=hidden value=".$t['id']." name=timeid[]>";
					$echo.= "</th>"; 
					$echo.= "<th class=".$t['id'].">R</th>"; 
					$echo.= "<th class=".$t['id'].">Original Dept. Comment</th>"; 
					$echo.= "<th class=".$t['id'].">".($t['id']-2)."</th>"; 
					$echo.= "<th class=".$t['id'].">".($t['id']-1)."</th>"; 
					$echo.= "<th class=".$t['id'].">".$t['id']."</th>"; 
					$echo.= "<th class=".$t['id'].">Q</th>"; 
					$echo.= "<th class=".$t['id'].">New Actual</th>"; 
					$echo.= "<th class=".$t['id'].">R</th>"; 
					$echo.= "<th class=".$t['id'].">New Dept. Comment</th>"; 
					break;
			}
		}
		$echo.= "<th class=".$t['id'].">Action Taken</th>"; 
	}
$echo.= "</tr>";
				/*********** LIST KPIS ****************/ 
$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (SELECT topid FROM ".$dbref."_toplevel WHERE topyn = 'Y')";
include("inc_db_con.php");
$result = array();
	while($row = mysql_fetch_assoc($rs)) {
		$result[$row['trtopid']][$row['trtimeid']] = $row;
	}
mysql_close($con);
$cspan = $c;
foreach($dir as $sub) {
	$subid = $sub['dirid'];
	$echo.= chr(10)."<tr><td colspan=$cspan style=\"color: #000000; font-weight: bold; background-color: #DEDEDE; text-align: left; height: 30;\">".$sub['dirtxt']."</td></tr>";

$sql = "SELECT t.* FROM ".$dbref."_toplevel t WHERE t.topsubid = $subid AND t.topyn = 'Y' ORDER BY topid";
include("inc_db_con.php");
$tmnr = mysql_num_rows($rs);
while($top = mysql_fetch_assoc($rs))
{
	$tt = $targettype[$top['toptargettypeid']]['code'];
	$topid = $top['topid'];
	$kct = $top['topcalctype'];
	//$rowcount = checkIntRef($kpimsr[$topid]) ? $kpimsr[$topid] : 1;
	$echo.= chr(10)."<tr>";
		$echo.= "<td>".$top['topid']."</td>";
		$echo.= "<td>".$top['topvalue']."</td>";
	foreach($head as $h) {
		$hf = $h['headfield'];
			switch($hf) {
				case "topcalctype":
					$echo.= "<td style=\"text-align: center;\">";
					$echo.=$calctype[$top[$hf]]['value'];
					break;
				case "topcalcaction":
					$echo.= "<td style=\"text-align: center;\">";
					switch($top[$hf]) {
						case "AVE":
							$echo.= "Average";
							break;
						default:
							$echo.= "Sum";
							break;
					}
					break;
				case "toptargettypeid":
					$echo.= "<td style=\"text-align: center;\">";
					$echo.=$targettype[$top[$hf]]['code'];
					break;
				case "assockpi":
					$echo.= "<td style=\"text-align: center;\">";
					//$sql2 = "SELECT count(kpiid) as kc FROM ".$dbref."_kpi WHERE kpiyn = 'Y' AND kpimsr = $topid";
					//include("inc_db_con2.php");
					//	$r = mysql_fetch_assoc($rs2);
					//	$kassoc = $r['kc'];
					//	$echo.=$kassoc;
					//mysql_close($con2);
					$r = isset($kpimsr[$topid]) ? $kpimsr[$topid] : array();
					$kassoc = count($r);
					$echo.=$kassoc;
					break;
				case "munkpa":
				case "tas":
				case "natkpa":
					$echo.= "<td style=\"text-align: center;\">";
					$echo.= "<span style=\"text-decoration: underline;\" title='".$lists[$hf][$top['top'.$hf.'id']]['value']."'>".$lists[$hf][$top['top'.$hf.'id']]['code']."</span>";
					break;
				case "gfs":
					$echo.= "<td>";
					$echo.= $lists[$hf][$top['top'.$hf.'id']];
					break;
				case "idp":
					$echo.= "<td>";
					$value = $lists[$hf][$top['top'.$hf.'id']];
					$echo.= $value;
					break;
				case "toptargetannual":
					$echo.= "<td style=\"text-align:right;\">";
						$echo.= formatResult($tt,$top[$hf]);
					break;
				case "toptargetrevised":
					$echo.= "<td style=\"text-align:right;\">";
					$tactive = "N";
					for($t=3;$t<13;$t+=3) { if($time[$t]['active']=="Y") { $tactive = "Y"; break; } }
					if($top[$hf]>0 || $tactive == "Y") {
						if($tactive !="Y") {
							$echo.= formatResult($tt,$top[$hf]);
						} else {
							$echo.= formatResult($tt,$top[$hf]);
						}
					}
					break;
				case "wards":
					$echo.= "<td>";
					$sql2 = "SELECT * FROM ".$dbref."_list_wards WHERE id IN (SELECT twwardsid FROM ".$dbref."_toplevel_wards WHERE twtopid = $topid AND twyn = 'Y') AND yn = 'Y' ORDER BY numval";
					include("inc_db_con2.php");
						while($w = mysql_fetch_assoc($rs2)) {
							$echo.= $w['value'].";";
						}
					mysql_close($con2);
					break;
				case "area":
					$echo.= "<td>";
					$sql2 = "SELECT * FROM ".$dbref."_list_area WHERE id IN (SELECT taareaid FROM ".$dbref."_toplevel_area WHERE tatopid = $topid AND tayn = 'Y') AND yn = 'Y' ORDER BY value";
					include("inc_db_con2.php");
						while($w = mysql_fetch_assoc($rs2)) {
							$echo.= $w['value'].";";
						}
					mysql_close($con2);
					break;
				default:
					$echo.= "<td>";
					$echo.= $top[$hf];
					break;
			}
		$echo.= "</td>";
	}
	$ytdtar = 0;
	$ytdact = 0;
	$kpiresult = array();
	$toperr = "N";
	foreach($time as $t) {
		$res = $result[$topid][$t['id']];
		$timeid = $t['id'];
		$auto = $res['trauto'];
		$targ = $res['trtarget'];
		$actl = $res['tractual']; $act = $actl;
		// TARGET
		$echo.= "<td style=\"text-align: center;\" class=".$t['id']."b>";
			$echo.= formatResult($tt,$targ);
		$echo.= "</td>";

		if($t['sval']<$today) {
/***** ORIGINAL VALUES *****/
			$kct = $top['topcalctype'];
			$kca = $top['topcalcaction'];
			$ytdtar += $targ;
			$ytdact += $actl;
			// ACTUAL
			$echo.= "<td style=\"text-align: center;\" class=".$t['id']."b>";
					$echo.= formatResult($tt,$actl);
			$echo.= "</td>";
			// RESULT
			if($targ>0 || $actl>0 || $kct == "ZERO" || $kct == "REV" || $t['eval']<$today) {
				$krr = calcKR($kct,$targ,$actl);
				$echo.= "<td class=\"krr".$krr."\" id=krr$topid>";
					$echo.= $krr;
			} else {
				$echo.= "<td id=krr$topid>&nbsp;";
			}
			$echo.= "</td>";
			// COMMENT
			$comm = $res['trsdbipcomment'];
			$echo.= "<td class=\"".$t['id']."b comm\">".$comm."</td>";
/**** NEW VALUES ****/
			if($auto=="N" || $kassoc==0) {
				$echo.= "<td style=\"text-align: center;\" class=".$t['id']."n colspan=7>";
				if($auto == "N") { $echo.= "Locked for manual updating.<br />No change will be made."; } else { $echo.= "No associated SDBIP KPIs."; }
				$echo.= "</td>";
			} elseif($t['active']!="NNN") {
			$echo.= "<td class=".$t['id']."n colspan=4>&nbsp;</td>";
			$act2 = 0;
			$comm2 = "";
			$krr2 = 0;
			$kpi = array();
			$sql2 = "SELECT kr.*, s.subtxt, t.eval FROM ".$dbref."_kpi_result kr, ".$dbref."_kpi, ".$dbref."_dirsub s, ".$dbref."_list_time t ";
			$sql2.= "WHERE ";
			if($kct<> "CO") {
				$sql2.= "krtimeid >= ".($t['id']-2)." AND ";
			}
			$sql2.= "krtimeid <= ".$t['id']." ";
			$sql2.= "AND krkpiid = kpiid AND kpisubid = subid AND krtimeid = t.id ";
			$sql2.= "AND krkpiid IN (SELECT kpiid FROM ".$dbref."_kpi WHERE kpimsr = $topid AND kpiyn = 'Y')";
			include("inc_db_con2.php");
			while($row2 = mysql_fetch_array($rs2)) {
				$kpi[$row2['krkpiid']][$row2['krtimeid']] = $row2;
			}
			mysql_close($con2);
			$actl = array();
			$c = array();
			foreach($kpi as $k) {		//FOREACH KPI
				$a = 0;
				$krtar = 0;
				foreach($k as $r) {		//FOREACH TIME PERIOD IN QUARTER
					$ki = $r['krkpiid'];
				$krkct = $kpimsr[$topid][$ki]['kpicalctype'];
				$krtt = $targettype[$kpimsr[$topid][$ki]['kpitargettypeid']]['code'];
					$kt = $r['krtimeid'];
					$kpiresult[$ki][$kt] = array('a'=>0,'t'=>0);
					$ra = $r['kractual'];
					$rt = $r['krtarget'];
					$rc = $r['krprogress'];
					$kpiresult[$ki][$kt]['t'] = $rt; 
					if(strlen($rc)>2 && ($kt>=($t['id']-2))) { $c[] = "<b>".$r['subtxt'].":</b> ".$rc." <i>[".date("d M Y",$r['eval'])."]</i>"; }
					switch($kct) {
						case "CO":	//MAX ACTUAL
							if(is_numeric($ra)) { 
								if($ra>0) {
									$kpiresult[$ki][$kt]['a'] = $ra;
								} else {
									$kpiresult[$ki][$kt]['a'] = $a;
								}
								if($ra>$a) {
									$a = $ra;  
								}
							}
							break;
						case "STD":	//AVERAGE ACTUALS OVER # OF TARGETS
						case "REV":
							if(is_numeric($ra)) { $a+=$ra; $kpiresult[$ki][$kt]['a'] = $ra;}
							if(is_numeric($r['krtarget']) && $r['krtarget']>0) { $krtar++; }
							break;
						default:	//ZERO || ACC	= 	SUM ACTUALS
							if(is_numeric($ra)) { $a+=$ra; $kpiresult[$ki][$kt]['a'] = $ra; }
							break;
					}
					//$echo.= "<td class=".$t['id']."n>&nbsp;</td>";
				}
				//$echo.= "<td class=".$t['id']."n>&nbsp;</td>";
				if(($kct=="STD" || $kct=="REV") && $krtar>0) {
					$a2 = $a/$krtar;
				} else {
					$a2 = $a;
				}
				$actl[$ki] = $a2;
				$kpiresult[$ki]['Q'.$kt] = $a2;
			}	//foreach kpi
			if($krkct != $kct || $krtt != $tt) {
				$toperr = "Y";
			}
			$act2 = 0;
				switch($kca) {
					case "AVE":
						if(array_sum($actl)>0 && count($actl)>0) {
							$act2 = average($actl);
						}
						break;
					case "SUM":
					default:
						$act2 = array_sum($actl);
						break;
				}
				//NEW ACTUAL
				if($toperr != "Y") {
					$echo.= "<td style=\"text-align: center;\" class=".$t['id']."n>";
						$echo.= formatResult($tt,$act2);
					$echo.= "</td>";
					//NEW RESULT
					if($targ>0 || $act2>0 || $kct == "ZERO" || $t['eval']<$today) {
						$krr2 = calcKR($kct,$targ,$act2);
						$echo.= "<td class=\"krr".$krr2."\">";
							$echo.= $krr2;
					} else {
						$echo.= "<td>&nbsp;";
					}
					$echo.= "</td>";
					//NEW COMMENT
					$comm2 = strFn("implode",$c,"<br />","");
					$echo.= "<td class=\"".$t['id']."n comm\" style=\"font-weight: normal;\">";
						$echo.= $comm2;
					$echo.= "</td>";
				} else {
					$echo.= "<td class=".$t['id']."b colspan=4 style=\"color: #CC0001; font-weight: bold;\">Calc Type or Targ Type mismatch. No update was done.</td>";
				}
			}
		}
		//ACTION COMMENT
		if($auto!= "N" && $toperr != "Y" && $kassoc>0) {
			$echo.= "<td class=".$t['id']."b>";
			if($t['sval']<$today) {
				if(strlen($act)==0) { $act = 0; }
				if($t['active']=="NNN") {
					$echo.= "Quarter closed.";
				} elseif($act2 <> $act || $comm <> $comm2) {
					$tm = $t['id'];
					$sql3 = "UPDATE ".$dbref."_toplevel_result SET tractual = $act2, trsdbipcomment = '$comm2' WHERE trtopid = $topid AND trtimeid = $timeid";
					//include("inc_db_con3.php");
					$mar3 = Odb_update($sql3,$cmpcode);
					if($mar3>0) {
//					if(mysql_affected_rows($con3)>0) {
						$updatecount++;
						$echo.= "Result updated.";
						if($act2 <> $act) {
							logTL($topid,'tractual_'.$tm,$act,$act2,'A',0,$sql,$tm,'Actual updated for TL KPI $topid for Quarter ending $tm.');
						}
						if($comm <> $comm2) {
							logTL($topid,'trsdbipcomment_'.$tm,$comm,$comm2,'A',0,$sql,$tm,'SDBIP Comment updated for TL KPI $topid for Quarter ending $tm.');
						}
					} else {
						$echo.= "No change";
					}
				} else {
					$echo.= "-";
				}
			} else {
				$echo.= "N/A";
			}
//			$echo.= strFn("implode",$actl,"<br />","");
			$echo.= "</td>";
		} elseif ($toperr == "Y" && $t['sval']>$today) {
			$echo.= "<td class=".$t['id']."b>N/A</td>";
		}
	}
	$echo.= "</tr>";
	$kpitop = isset($kpimsr[$topid]) ? $kpimsr[$topid] : array();
	foreach($kpitop as $kt2) {
		$kt = $kt2['kpiid'];
		$echo.= "<tr>";
			$echo.= "<td colspan=".(count($head)-2).">&nbsp;</td>";
			$echo.= "<td class=\"kpi kpitxt\" style=\"font-weight: bold;\">KPI ".$kt."</td>";
			$echo.= "<td class=\"kpi kpitxt\" style=\"text-align: center;\">".$calctype[$kt2['kpicalctype']]['value']."</td>";
			$echo.= "<td class=kpi>&nbsp;</td>";
			$echo.= "<td class=\"kpi kpitxt\" style=\"text-align: center;\">".$targettype[$kt2['kpitargettypeid']]['code']."</td>";
			$kr = $kpiresult[$kt];
		foreach($time as $t) {
			$echo.= "<td>&nbsp;</th>"; 
			$auto = $result[$topid][$t['id']]['trauto'];
			if($t['sval']<$today) {
				switch($t['active']) {
					case "NNN":
						$echo.= "<td>&nbsp;</td>"; 
						$echo.= "<td>&nbsp;</td>"; 
						$echo.= "<td>&nbsp;</td>"; 
						break;
					default:
						$echo.= "<td>&nbsp;</td>";
						$echo.= "<td>&nbsp;</td>";
						$echo.= "<td>&nbsp;</td>";
						if($auto!="N") {
							$echo.= "<td class=\"".$t['id']."b kpi\">".formatResult($tt,$kr[$t['id']-2]['a']);
							$echo.= "<br />(".formatResult($tt,$kr[$t['id']-2]['t']).")";
							$echo.= "</td>"; 
							$echo.= "<td class=\"".$t['id']."b kpi\">".formatResult($tt,$kr[$t['id']-1]['a']);
							$echo.= "<br />(".formatResult($tt,$kr[$t['id']-1]['t']).")";
							$echo.= "</td>"; 
							$echo.= "<td class=\"".$t['id']."b kpi\">".formatResult($tt,$kr[$t['id']-0]['a']);
							$echo.= "<br />(".formatResult($tt,$kr[$t['id']-0]['t']).")";
							$echo.= "</td>"; 
							$echo.= "<td class=\"".$t['id']."n kpi\" style=\"font-weight: bold;\">".formatResult($tt,$kr['Q'.$t['id']])."</td>"; 
						} else {
							$echo.= "<td>&nbsp;</td>";
							$echo.= "<td>&nbsp;</td>";
							$echo.= "<td>&nbsp;</td>";
							$echo.= "<td>&nbsp;</td>";
						}
						$echo.= "<td>&nbsp;</td>";
						$echo.= "<td>&nbsp;</td>";
						$echo.= "<td>&nbsp;</td>";
						break;
				}
			}
			$echo.= "<td>&nbsp;</td>";
		}
		$echo.=	"</tr>";
	}
}
mysql_close($con);
}	//END FOREACH DIR AS SUB

				 /*********** END ****************/ 
$echo .= "</table>";
$echo .= "</body></html>";

//SAVE UPDATE RECORD
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "SDP10/TOP";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/".date("Ymd_His").".html";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);

$res = $updatecount;
	return $res;
}	// END FUNCTION

/********************************/
/*** END TL UPDATE FUNCTION *****/