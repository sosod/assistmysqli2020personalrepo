<?php
    include("inc_ignite.php");
    $targets = array();
function calcKRR($kct, $rt, $ra)
{
    $krr = 0;
    if($kct == "ZERO")
    {
        if($ra > $rt)
        {
            $krr = 1;
        }
        else
        {
            $krr = 3;
        }
    }
    else
    {
        if($ra >= $rt && $ra > 0)
        {
            $krr = 3;
        }
        else
        {
            if($rt > 0)
            {
                if($ra/$rt < 0.75)
                {
                    $krr = 1;
                }
                else
                {
                    $krr = 2;
                }
            }
        }
    }

    return $krr;

}



$style[0] = "background-color: #555555;";
$style[1] = "background-color: #cc0001;";
$style[2] = "background-color: #fe9900;";
$style[3] = "background-color: #006600;";
$style[4] = "background-color: #000066;";

$style2[0] = "background-color: #555555; border-left: 1px solid #ffffff;";
$style2[1] = "background-color: #cc0001; border-left: 1px solid #ffffff;";
$style2[2] = "background-color: #fe9900; border-left: 1px solid #ffffff;";
$style2[3] = "background-color: #006600; border-left: 1px solid #ffffff;";
$style2[4] = "background-color: #000066; border-left: 1px solid #ffffff;";


$stylel[0] = "background-color: #dddddd;";
$stylel[1] = "background-color: #ffcccc;";
$stylel[2] = "background-color: #ffeaca;";
$stylel[3] = "background-color: #ceffde;";
$stylel[4] = "background-color: #ccccff;";


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>

<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Edit KPI - Approve Update</b></h1>
<?php
$kpiid = $_POST['kpiid'];
$plcid = $_POST['plcid'];
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
$tbl = $_POST['tbl'];
if($tbl == "temp")
{
    $plctabl = "plc_temp";
    $tabl = "phases_temp";
}
else
{
    $plctabl = "plc";
    $tabl = "phases";
}

//echo($plctabl."-".$tabl);

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiid = ".$kpiid;
include("inc_db_con.php");
    $kpirow = mysql_fetch_array($rs);
mysql_close();
$sql = "SELECT dirtxt, subtxt FROM assist_".$cmpcode."_".$modref."_dir, assist_".$cmpcode."_".$modref."_dirsub WHERE ";
$sql.= "subdirid = dirid";
$sql.= " AND subid = ".$kpirow['kpisubid'];
include("inc_db_con.php");
    $dirrow = mysql_fetch_array($rs);
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_".$plctabl." WHERE plcid = ".$plcid;
if($plctabl=="plc_temp") { $sql.= " ORDER BY tmpid DESC"; }
include("inc_db_con.php");
    $plcrow = mysql_fetch_array($rs);
mysql_close();
$tmpid = array();
?>
<h2 class=fc>Project Life Cycle</h2>
<table border="0" cellspacing="0" cellpadding="3" style="border: 0px solid #ffffff;">
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Name:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($plcrow['plcname']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Description:&nbsp;</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($plcrow['plcdescription']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Start Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo(date("d M Y",$plcrow['plcstartdate'])); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>End Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo(date("d M Y",$plcrow['plcenddate'])); ?></td>
	</tr>
</table>
<p style="margin: 0 0 0 0;">&nbsp;</p>
<table border="1" cellspacing="0" cellpadding="3" width=100%>
	<tr>
		<td height=27 colspan="2" class="tdheader"><b>Project Phase</b></td>
		<td width=100 class="tdheader"><b>Calendar<br>Days</b></td>
		<td class="tdheader"><b>Start<br>Date</b></td>
		<td class="tdheader"><b>End<br>Date</b></td>
		<td class="tdheader"><b>% Target<br>of Project</b></td>
		<td class="tdheader"><b>Completed?</b></td>
		<td class="tdheader"><b>Date of<br>Completion</b></td>
	</tr>
<?php
$tartot = 0;
$targets = array();
$actualss = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            switch($row['type'])
            {
        case "A":
//CUSTOM PHASES
        $sql2 = "SELECT p.*, c.value FROM assist_".$cmpcode."_".$modref."_kpi_plc_".$tabl." p, assist_".$cmpcode."_".$modref."_list_plcphases_custom c ";
        $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'C' AND p.ppphaseid = c.id ORDER BY p.ppsort";
        include("inc_db_con2.php");
            $mnr2 = mysql_num_rows($rs2);
            if($mnr2 > 0)
            {
            $a=0;
                while($row2 = mysql_fetch_array($rs2))
                {
                    //targets
                    $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = $targets[$Yr][$Mn] + $row2['pptarget'];
                    $a++;
                    $tmpid[] = $row2['tmpid'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                            $aYr = date("Y",$row2['ppdonedate']);
                            $aMn = date("n",$row2['ppdonedate']);
                            $actuals[$aYr][$aMn] = $actuals[$aYr][$aMn] + $row2['pptarget'];
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
	<?php if($a==1) { ?>		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td><?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdone); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                }
            }
        mysql_close($con2);
        break;
            case "P":
            //PROCUREMENT PHASES
                $sql2 = "SELECT p.*, c.value FROM assist_".$cmpcode."_".$modref."_kpi_plc_".$tabl." p, assist_".$cmpcode."_".$modref."_list_plcphases_proc c ";
                $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'P' AND p.ppphaseid = c.id ORDER BY p.ppsort";
                include("inc_db_con2.php");
                $mnr2 = mysql_num_rows($rs2);
                $r=0;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $r++;
                        $tmpid[] = $row2['tmpid'];
                        $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = $targets[$Yr][$Mn] + $row2['pptarget'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                            $aYr = date("Y",$row2['ppdonedate']);
                            $aMn = date("n",$row2['ppdonedate']);
                            $actuals[$aYr][$aMn] = $actuals[$aYr][$aMn] + $row2['pptarget'];
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
        <?php if($r==1) { ?>
		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
        <?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdone); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                    }
                mysql_close($con2);
            default:
            //STANDARD PHASES WHERE TYPE = S
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_".$tabl." WHERE pptype = 'S' AND ppphaseid = ".$row['id']." AND ppplcid = ".$plcid;
                include("inc_db_con2.php");
                    $mnr2 = mysql_num_rows($rs2);
                    if($mnr2 > 0)
                    {
                        $row2 = mysql_fetch_array($rs2);
                    }
                mysql_close($con2);
                if($mnr2>0)
                {
                    $tmpid[] = $row2['tmpid'];
                    $tartot = $tartot+$row2['pptarget'];
                    $Yr = date("Y",$row2['ppenddate']);
                    $Mn = date("n",$row2['ppenddate']);
                    $targets[$Yr][$Mn] = $targets[$Yr][$Mn] + $row2['pptarget'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                            $aYr = date("Y",$row2['ppdonedate']);
                            $aMn = date("n",$row2['ppdonedate']);
                            $actuals[$aYr][$aMn] = $actuals[$aYr][$aMn] + $row2['pptarget'];
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
		<td colspan="2" class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdone); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                }
                break;

            }
        }
    mysql_close();

?>
	<tr>
		<td height=27 colspan="2" class="tdgeneral"><b>Target % of Project Total:</b></td>
		<td class="tdgeneral" align="center" colspan=3></td>
		<td class="tdgeneral" align="center"><b><?php echo($tartot); ?> %</b></td>
	</tr>
</table>
<?php
//print_r($targets);
?>
<h2 class=fc>KPI Results</h2>
<form name=plcconfirm method=post action=admin_kpi_edit_plc_confirm_process.php>
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=tbl value="<?php echo($tbl); ?>">
<input type=hidden name=tmpid value="<?php echo(implode(",",$tmpid)); ?>">
<table cellpadding=5 cellspacing=0 width=500>
    <tr>
        <td class=tdgeneral colspan=2>&nbsp;</td>
        <td class=tdheader style="background-color: #555555;">Old Results</td>
        <td class=tdheader style="background-color: #555555;">New Results</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $krt0 = 0;
    $kra0 = 0;
    $targ = 0;
    $actl = 0;
    $years = array();
    $res = "true";
    while($row = mysql_fetch_array($rs))
    {
        $thead = $row;
        //NEW RESULTS
        $tYr = date("Y",$row['eval']);
        $years[] = $tYr;
        $tMn = date("n",$row['eval']);
        $tart = $targets[$tYr][$tMn];
        $actu = $actuals[$tYr][$tMn];
        $targ = $targ + $targets[$tYr][$tMn];
        $actl = $actl + $actuals[$tYr][$tMn];
        $asum = 0;
        foreach($actuals as $acts)
        {
            $asum = $asum + array_sum($acts);
        }
//        if($tart>0 || $actu > 0) {
        if($asum != $actl || $actu > 0 || $row['sval']<$today) {
            $krrN = calcKRR($kct,$targ,$actl);
        } else {
            $krrN = 0;
        }
        $krtargetN = number_format($targ,0)."%";
        $kractualN = number_format($actl,0)."%";
        if($tart>0) {  $ntargets[$tYr][$tMn] = $targ; } else { $ntargets[$tYr][$tMn] = 0; }

        //OLD RESULTS
                        $sql2 = "SELECT max(krtarget) as krtarget, max(kractual) as kractual FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid <= ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$kpiid." AND krtimeid = ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2x = mysql_fetch_array($rs2);
                            $row2['krtargettypeid'] = $row2x['krtargettypeid'];
                        mysql_close($con2);

                $oYr = date("Y",$row['eval']);
                $years[] = $oYr;
                $oMn = date("n",$row['eval']);

                $otargets[$oYr][$oMn] = $otargets[$oYr][$oMn] + $row2x['krtarget'];

                if($row2x['krtarget']==0 && $row2x['kractual']==0)
                {
                    $krt = 0;
                    $krt2 = "";
                    $kra = 0;
                    $krr = 0;
                }
                else
                {
                    $krt = $row2['krtarget'];
                    $krt2 = $krt;
                    if($row['sval']<$today)
                    {
                        $kra = $row2['kractual'];
                        $krr = calcKRR($kct,$krt,$kra);
                    }
                    else
                    {
                        $kra = 0;
                        $krr = 0;
                    }
                }
                $decpt = 0;
                if(round($krt)!=$krt || round($kra)!=$kra) { $decpt = 2; }
                if($krt > 0 || $kra > 0) { $krtarget = number_format($krt,$decpt)."%"; } else { $krtarget = ""; }
                if($kra > 0 || $row['sval']<$today) { $kractual = number_format($kra,$decpt)."%"; } else { $kractual = ""; }

if($row['sval']<$today) { $rowspan=3; } else { $rowspan=1; }
if(($targ!=$krt2 && $tart>0) || ($tart==0 && $krt!=$tart)) { $res = "false"; }
        ?>
    <tr>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style[$s]); ?>" rowspan=<?php echo($rowspan); ?>><?php echo(date("d M Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
        <td class=tdheaderl valign=top width=100 style="<?php echo($style2[$s]); ?>">Target:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if(($krt>0 || $kra>0)) { echo($krtarget); } ?>&nbsp;<?php // echo(":".$krt2.":");?></td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if((($tart > 0 || $actl > 0) && $row['sval']<$today) || $tart>0) { echo($krtargetN); } ?>&nbsp;<input type=hidden name=target[] value="<?php if($tart>0) { echo($targ); } else { echo(0); }?>"><input type=hidden name=actual[] value="<?php if($actu>0) { echo($actl); } else { echo(0); }?>"></td>
    </tr>
    <?php if($row['sval']<$today) { ?>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style2[$s]); ?>">Actual:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if(($krt>0 || $kra>0) ) { echo($kractual); } ?>&nbsp;</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if(($targ>0 && ($actu > 0 || $asum != $actl || $row['sval']<$today)) || $actl>0) { echo($kractualN); } ?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style2[$s]); ?>">Result:</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if($krr>0) { ?><table border=0 cellpadding=0 cellspacing=0 style="border: 0px;"><tr><td width=30 class=tdheaderl style="border: 1px solid #ffffff; <?php echo($style[$krr]); ?>">&nbsp;</td></tr></table><?php } else { echo("&nbsp;"); } ?></td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>"><?php if($krrN>0) { ?><table border=0 cellpadding=0 cellspacing=0 style="border: 0px;"><tr><td width=30 class=tdheaderl style="border: 1px solid #ffffff; <?php echo($style[$krrN]); ?>">&nbsp;</td></tr></table><?php } else { echo("&nbsp;"); } ?></td>
    </tr>
        <?php
        } //if sval < today
        $s++;
        if($s>4) { $s=1; }
    }
mysql_close();
?>
</table>

<?php
/*echo("<P>NEW-TARGETS-");
print_r($ntargets);
echo("<P>OLD-TARGETS-");
print_r($otargets);
echo("<p>");
//print_r(array_diff($targets,$otargets));
print_r($years);
$years = array_unique($years);
$years = array_values($years);
echo("<p>");
print_r($years);
//$res = "true";
foreach($years as $yr)
{
//    if($ntargets[$yr]!=$otargets[$yr]) { $res = "false"; }
}
*/
//echo($res);
?>
<p style="font-size: 1pt; line-height: 1pt;">&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="3" width=100%>
    <tr>
        <td class=tdgeneral align=center><?php
if($res == "true")
{
    ?>There are no changes to the KPI targets and therefore <u style="color: #006600"><b>no approval is required</b></u> to finalise this update.<input type=hidden name=appreq value=N><?php
}
else
{
    ?>There are changes to the KPI targets and therefore <u style="color: #cc0001;"><b>approval is required</b></u> to finalise this update.<input type=hidden name=appreq value=Y><?php
}
        ?></td>
    </tr>
</table>
<p style="font-size: 1pt; line-height: 1pt;">&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="3" width=100%>
    <tr>
        <td class=tdgeneral align=center><input type=button id=actbut value=" Confirm " onclick="document.forms['plcconfirm'].submit();"> <input type=button value=" Reject " onclick="document.forms['plcrej'].submit();"></td>
    </tr>
</table>
</form>
<form name=plcrej method=post action=admin_kpi_edit_plc.php>
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=tbl value="<?php echo($tbl); ?>">
</form>
<script type=text/javascript>
<?php
if($res == "true")
{
    echo("document.getElementById('actbut').value = 'Accept Update';");
}
else
{
    echo("document.getElementById('actbut').value = 'Accept Update & Request Approval';");
}
?>
</script>
</body>

</html>
