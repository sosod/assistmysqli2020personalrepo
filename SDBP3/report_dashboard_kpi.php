<?php
/********* SET VARIABLES *********/
$kpi = array();
foreach($results as $krs) {
	$kpi[0][$krs] = 0;
}
$kpi[0]['tot'] = 0;

/********* GET INFORMATION **************/
	/*** RESULTS INTO ARRAY ***/
	$result = array();
$sql = "SELECT * FROM ".$dbref."_kpi_result WHERE krkpiid IN (";
	$sql.= "SELECT kpiid FROM ".$dbref."_kpi k, ".$dbref."_dir d, ".$dbref."_dirsub s WHERE k.kpisubid = s.subid AND s.subdirid = d.dirid AND k.kpiyn = 'Y' AND d.diryn = 'Y' AND s.subyn = 'Y'";
	if(isset($cpfilter) && strtoupper($cpfilter) != "ALL") {
		if(strtoupper($cpfilter) == "CP") {
			$sql.= " AND k.kpicpid > 0";
		} else {
			$sql.= " AND k.kpicpid = 0";
		}
	}
$sql.= ")";
include("inc_db_con.php");
$result = array();
	while($row = mysql_fetch_assoc($rs)) {
		$result[$row['krkpiid']][$row['krtimeid']] = $row;
	}
mysql_close($con);
	/*** GET KPIS ***/
	$kpirow = array();
//$sql = "SELECT t.topid, t.topsubid, t.topcalctype FROM ".$dbref."_toplevel t, ".$dbref."_dir d WHERE t.topsubid = d.dirid AND t.topyn = 'Y' AND d.diryn = 'Y' ORDER BY dirsort, topid";
$sql = "SELECT k.kpiid, k.kpisubid, d.dirid, k.kpicalctype ";
	if($groupby != "X" && $groupby!="dir") { $sql.= ", k.".$dlist[$groupby]['kpi']; }
	$sql.= " FROM ".$dbref."_kpi k";
	$sql.= " INNER JOIN ".$dbref."_dirsub s ON s.subid = k.kpisubid ";
	$sql.= " INNER JOIN ".$dbref."_dir d ON d.dirid = s.subdirid ";
	foreach($dlist as $d) {
		$hf = $d['headfield'];
		if($hf!="wards" && $hf!="area") {
			$sql.= " INNER JOIN ".$dbref."_list_".$d['id']." ".$hf." ON ";
			switch($hf) {
				case "kpistratop":
				case "kpiriskrate":
				case "kpicalctype":
					$sql.= $hf.".code = k.".$d['kpi'];
					break;
				default:
					$sql.= $hf.".id = k.".$d['kpi'];
			}
		}
	}
	$sql.= " WHERE k.kpiyn = 'Y' AND d.diryn = 'Y' AND s.subyn = 'Y' AND k.kpiid IN (SELECT DISTINCT krkpiid FROM ".$dbref."_kpi_result) ";
	if(checkIntRef($dirsub)) {
		$sql.= " AND s.subid = ".$dirsub;
		$sub = $dirsub;
		$dir = "ALL";
	} elseif($dirsub!="ALL") {
		$dirsub2 = substr($dirsub,2,strlen($dirsub));
		if(checkIntRef($dirsub2)) {
			$sql.= " AND d.dirid = ".$dirsub2;
		}
		$dir = $dirsub2;
		$sub = "ALL";
	} else {
		$sub = "ALL";
		$dir = "ALL";
	}
	if(isset($cpfilter) && strtoupper($cpfilter) != "ALL") {
		if(strtoupper($cpfilter) == "CP") {
			$sql.= " AND k.kpicpid > 0";
		} else {
			$sql.= " AND k.kpicpid = 0";
		}
	}
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$kpirow[$row['kpiid']] = $row;
	}
mysql_close($con);
/********* CALCULATE RESULTS ************/
foreach($kpirow as $kpir) {
	$kpiid = $kpir['kpiid'];	//echo "<P>".$kpiid;
	if($groupby=="X" || checkIntRef($dirsub)) {
		$moreid = "X";
		//echo "X";
	} elseif($groupby == "dir" && $dirsub == "ALL") {
		$moreid = $kpir['dirid'];
		//echo "dirid";
	} elseif($groupby == "dir" && subStr($dirsub,0,1)=="D") {
		$moreid = $kpir['kpisubid'];
		//echo "subid";
	} else {
		$moreid = $kpir[$dlist[$groupby]['kpi']];
		//echo $groupby;
	}
	$kct = $kpir['kpicalctype'];
	$ytdtar = 0;
	$ytdact = 0;
	$krtcount = 0;
	$tar = 0;
	$act = 0;
	if($kct=="ACC" || $kct == "CO") {
		$tfrom = 1;
	} else {
		$tfrom = $from;
	}
	for($t=$tfrom;$t<=$to;$t++) {
		if(isset($result[$kpiid][$t])) {
			$res = $result[$kpiid][$t];
			$tar = $res['krtarget'];
			$act = $res['kractual'];
			if($tar > 0) { $krtcount++; }
			if($kct!="CO") {
				$ytdtar += $tar;
				$ytdact += $act;
			} else {
				if($tar > $ytdtar) { $ytdtar = $tar; }
				if($act > $ytdact) { $ytdact = $act; }
			}
		}
		$tar = 0;
		$act = 0;
	}
	if(($kct == "STD" || $kct == "REV") && $krtcount > 0) {
		$ytdtar/=$krtcount;
		$ytdact/=$krtcount;
	}

	$krr = calcKR($kct,$ytdtar,$ytdact);
	if($kpimet == "combine" && $krr > 4) { $krr = 4; }
	if(in_array($krr,$results)) {
		$kpi[0][$krr]++;
		$kpi[0]['tot']++;
		if($groupby!="X") {
			if(!isset($kpi[$moreid])) {
				$kpi[$moreid]['tot'] = 0;
				foreach($results as $krs) {
					$kpi[$moreid][$krs] = 0;
				}
			}
			$kpi[$moreid][$krr]++;
			$kpi[$moreid]['tot']++;
		}
	}
}	

//echo "<p>".$sql;
//echo "<pre>"; print_r($kpi); echo "</pre>";
/********** GET GROUP INFORMATION **********/
$group = array();
$mds2id = null;
/*switch($groupby) {
	case "X":
		break;
	case "dir":*/
	if($groupby=="dir") {
		$mds2 = array();
		if($mds['type'] == "M") {
			$sql = "SELECT d.dirid as id, d.dirtxt as value FROM ".$dbref."_dir d WHERE d.diryn = 'Y' ORDER BY d.dirsort";
			$mds2['type'] = "M";
			$mds2['sub'] = "ALL";
			$mds2id = "dir";
		} else {
			$sql = "SELECT s.subid as id, s.subtxt as value FROM ".$dbref."_dirsub s INNER JOIN ".$dbref."_dir d ON d.dirid = s.subdirid ";
			$sql.= " WHERE d.diryn = 'Y' AND s.subyn = 'Y'";
			$mds2['type'] = "D";
			$mds2['dir'] = "ALL";
			$mds2id = "sub";
		}
	} elseif($groupby!="X") {
		$sql = "SELECT id, value FROM ".$dbref."_list_".$dlist[$groupby]['id']." WHERE yn = 'Y' ORDER BY value";
		$mds2 = $mds;
	}
	if($groupby!="X") {
		include("inc_db_con.php");
			$group = array();
			$max_len = 110;
			while($row = mysql_fetch_array($rs)) {
				if(isset($kpi[$row['id']])) {
					$group[$row['id']] = $row;
					$group[$row['id']]['mds'] = $mds2;
					if(isset($mds2id)) {
						$group[$row['id']]['mds'][$mds2id] = $row['id'];
					}
					$group[$row['id']]['value'] = decode($row['value']);
					if(strlen($group[$row['id']]['value'])>$max_len) { $group[$row['id']]['value'] = substr($group[$row['id']]['value'],0,($max_len-5))."..."; }
				}
			}
		mysql_close($con);
	}
		$group_max = array();
		$values_maxlen = array();
		calcGrouping($group,$group_max,$values_maxlen);
		
/*		break;
	default:
		$sql = "SELECT id, value FROM ".$dbref."_list_".$dlist[$groupby]['id']." WHERE yn = 'Y' ORDER BY value";
		include("inc_db_con.php");
			while($row = mysql_fetch_array($rs)) {
				$group[$row['id']] = $row;
				$group[$row['id']]['mds'] = $mds;
					//$group[$row['id']]['value'] = strlen($group[$row['id']]['value'])." ".$group[$row['id']]['value'];
					$group[$row['id']]['value'] = decode($group[$row['id']]['value']);
					if(strlen($group[$row['id']]['value'])>110) { $group[$row['id']]['value'] = substr($group[$row['id']]['value'],0,105)."..."; }
					if(strlen($group[$row['id']]['value'])>$values_maxlen) { $values_maxlen = strlen($group[$row['id']]['value']); }
			}
		mysql_close($con);
		break;
}*/
$overall_title = "";

if($mds['type']=="M") {
	if($groupby!="X" && $groupby != "dir") {
		$overall_title = $dlist[$groupby]['name'];
	} else {
		$overall_title = $cmpname;
	}
} else {
	if($mds['type'] == "D") { 
		$sql = "SELECT dirtxt as txt FROM ".$dbref."_dir WHERE dirid = ".$mds['dir'];
	} else {
		$sql = "SELECT subtxt as txt FROM ".$dbref."_dirsub WHERE subid = ".$mds['sub'];
	}
	include("inc_db_con.php");
		$dsrow = mysql_fetch_array($rs);
	mysql_close($con);
	if($groupby!="X" && $groupby != "dir") {
		$overall_title = $dlist[$groupby]['name'];
		if(isset($dsrow['txt'])) { $overall_title.= " for ".$dsrow['txt']; }
	} else {
		$overall_title = $dsrow['txt'];
	}
}
$group_title = "";
if($group_graph == "stack" && $groupby !="X") {
	if($groupby != "dir") {
		$group_title = $dlist[$groupby]['name'];
	} elseif($mds['type']=="M") {
		$group_title = "Directorates";
	} else {
		$group_title = "Sub-Directorates";
	}
}
//echo "<pre>"; print_r($group_max); echo "</pre>";
?>