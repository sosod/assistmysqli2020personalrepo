<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>

<base target="main">
<script type=text/javascript>
function editDir(id) {
    var err = "N";
    if(!isNaN(parseInt(id)) && escape(id) == id)
    {
        id = parseInt(id);
        if(id>0)
        {
            document.location.href = "setup_dir_edit.php?d="+id;
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }
    
    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
function editSub(id) {
    var err = "N";
    if(!isNaN(parseInt(id)) && escape(id) == id)
    {
        id = parseInt(id);
        if(id>0)
        {
            document.location.href = "setup_dir_sub.php?d="+id;
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }

    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Directorates</b></h1>
<p>&nbsp;</p>

<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td class=tdheader width=30>Ref</td>
		<td class=tdheader width=170>Name</td>
		<td class=tdheader width=250>Sub-Directorates</td>
		<td class=tdheader width=120>&nbsp;</td>
	</tr>
	<?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
    $dirnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=4>No Directorates available.  Please click "Add New" to add Directorates.</td>
	</tr>
    <?php
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
            include("inc_tr.php");
    ?>
	
		<td class=tdheader valign=top><?php echo($id); ?></td>
		<td class=tdgeneral valign=top><b><?php echo($val); ?></b></td>
		<td class=tdgeneral valign=top><ul style="margin: 2 0 0 40;">
		  <?php
            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $id2 = $row2['subid'];
                    $val2 = $row2['subtxt'];
                    if($row2['subhead']=="Y") { $val2.="*"; }
            ?>
                    <li><?php echo($val2); ?></li>
            <?php
                }
            mysql_close($con2);
            ?></ul></td>
		<td class=tdgeneral align=center valign=top>
		<input type=button value="Edit Directorate" onclick="editDir(<?php echo($id); ?>)"><br><input type=button value="Sub-Directorates" onclick="editSub(<?php echo($id); ?>)"></td>
	</tr>
    <?php
        }
    }
    mysql_close();
    ?>
</table>
<p><input type=button value="Add New" onclick="document.location.href = 'setup_dir_add.php';"> <?php if($dirnum>0) { ?><input type=button value="Display Order" onclick="document.location.href = 'setup_dir_order.php';"> <input type=button value="Report" onclick="document.location.href = 'setup_dir_report.php';"><?php } ?></p>
<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<h2 style="color: #909090">Change log</h2>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdloghead>Date</td>
        <td class=tdloghead>User</td>
        <td class=tdloghead>Action</td>
    </tr>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_log WHERE type = 'DIR' ORDER BY id DESC";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            ?>
    <tr>
        <td class=tdlog><?php echo(date("d M Y H:i",$row['date'])); ?></td>
        <td class=tdlog><?php
        $sql2 = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['tkid']."'";
        include("inc_db_con2.php");
            $row2 = mysql_fetch_array($rs2);
        mysql_close();
        echo($row2['tkname']." ".$row2['tksurname']);
         ?></td>
        <td class=tdlog><?php echo($row['action']); ?></td>
    </tr>
            <?php
        }
    mysql_close();
    ?>
</table>
<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
