<?php
    include("inc_ignite.php");

/*** FUNCTION ***/
function displayReportOptions($rep,$t) {
	global $type;
			if(count($rep)>0) {
				if($t=="S") {
					echo "<option value=X>--- ".strtoupper($type[$t][1])." ---</option>";
				} else {
					echo "<option value=X>--- MY REPORTS ---</option>";
				}
				foreach($rep as $r) {
					echo "<option value=\"".$r['id']."\">".$r['rname']." (";
					if($r['modified_on']>0) {
						echo "Edited: ".date("d M Y",$r['modified_on']);
					} else {
						echo "Added: ".date("d M Y",$r['added_on']);
					}
					echo ")</option>";
				}
			}

}
	
	$sql = "SELECT * FROM ".$dbref."_reports WHERE (tkid = '$tkid' OR rtype LIKE 'S%') AND yn = 'Y' ORDER BY rname";
	include("inc_db_con.php");
		$reports = array();
		while($row = mysql_fetch_array($rs)) {
			$reports[$row['rtype']][] = $row;
		}
	mysql_close($con);
	$type = array(
		'KPI'=>array("KPI","Departmental SDBIP"),
		'TL'=>array("TL","Top Level SDBIP"),
		'CP'=>array("CP","Capital Projects"),
		'CF'=>array("CF","Monthly Cashflow"),
		'DASH'=>array("DASH","Dashboard"),
		'S'=>array("S","Standard reports")
	);

	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
a { color: #cc0001; }
a:visited { color: #cc0001; }
table th { text-align: right; }
table td { text-align: right; padding: 3 3 3 3; }
</style>
<script type=text/javascript>
function quickReport(loc, act) {
	var id = document.getElementById(loc).value;
	var url;
	if(loc=="DASH") { // _KPI" || loc == "DASH_TOP" || loc == "DASH_CF") {
		url = "report_dashboard.php?type="+id;
	} else {
		url = "report_"+loc.toLowerCase()+".php";
		switch(id) {
			case "X":
				alert("Please select a report.");
				die();
				break;
			case loc:
				url = url+"?i=0&a=E";
				break;
			default:
				url = url+"?i="+id+"&a="+act;
				break;
		}
	}
	document.location.href = url;
}
function reportList(loc) {
if(loc != "DASH") {
	var rlist = document.getElementById(loc);
	var ebut = document.getElementById(loc+'_E');
	var vbut = document.getElementById(loc+'_V');
	ebut.disabled = false;
	vbut.disabled = false;
	switch(rlist.value) {
		case "X":
			ebut.disabled = true;
			vbut.disabled = true;
			break;
		case loc:
			ebut.value = '  Go  ';
			vbut.disabled = true;
			break;
		default:
			ebut.value = ' Edit ';
			vbut.value = 'Generate';
			break;
	}
}
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report</b></h1>
<p>&nbsp;</p>
<table cellpadding=0 cellspacing=0>
<tr>
	<th>Departmental SDBIP&nbsp;</th>
	<td><?php
	echo "<select id=KPI onchange=\"reportList('KPI');\">";
		echo "<option selected value=KPI>Report generator</option>";
		if(count($reports['KPI'])+count($reports['S_KPI'])>0) {
			displayReportOptions($reports['S_KPI'],'S');
			displayReportOptions($reports['KPI'],'KPI');
		}
	echo "</select>";
	echo "&nbsp;<input type=button value=\" Go \" onclick=\"quickReport('KPI','E');\" id=KPI_E> <input type=button value=\"Generate\" onclick=\"quickReport('KPI','V');\" id=KPI_V>";
	?></td>
</tr>
<tr>
	<th>Top Level SDBIP</th>
	<td><?php $rtype = 'TL';
	echo "<select id=$rtype onchange=\"reportList('$rtype');\">";
		echo "<option selected value=$rtype>Report generator</option>";
		if(count($reports[$rtype])+count($reports['S_'.$rtype])>0) {
			displayReportOptions($reports['S_'.$rtype],'S');
			displayReportOptions($reports[$rtype],$rtype);
		}
	echo "</select>";
	echo "&nbsp;<input type=button value=\" Go \" onclick=\"quickReport('$rtype','E');\" id=".$rtype."_E> <input type=button value=\"Generate\" onclick=\"quickReport('$rtype','V');\" id=".$rtype."_V>";
	?></td>
</tr>
<tr>
	<th>Capital Projects</th>
	<td><?php $rtype = 'CP'; 
	echo "<select id=$rtype onchange=\"reportList('$rtype');\">";
		echo "<option selected value=$rtype>Report generator</option>";
		if(count($reports[$rtype])+count($reports['S_'.$rtype])>0) {
			displayReportOptions($reports['S_'.$rtype],'S');
			displayReportOptions($reports[$rtype],$rtype);
		}
	echo "</select>";
	echo "&nbsp;<input type=button value=\" Go \" onclick=\"quickReport('$rtype','E');\" id=".$rtype."_E> <input type=button value=\"Generate\" onclick=\"quickReport('$rtype','V');\" id=".$rtype."_V>";
	?></td>
</tr>
<tr>
	<th>Monthly Cashflow</th>
	<td><?php $rtype = 'CF'; /*
	echo "<select id=$rtype onchange=\"reportList('$rtype');\">";
		echo "<option selected value=$rtype>Report generator</option>";
		if(count($reports[$rtype])+count($reports['S_'.$rtype])>0) {
			displayReportOptions($reports['S_'.$rtype],'S');
			displayReportOptions($reports[$rtype],$rtype);
		}
	echo "</select>";
	echo "&nbsp;<input type=button value=\" Go \" onclick=\"quickReport('$rtype','E');\" id=".$rtype."_E> <input type=button value=\"Generate\" onclick=\"quickReport('$rtype','V');\" id=".$rtype."_V>";*/
	echo "&nbsp;<input type=button value=\"Report Generator\" onclick=\"quickReport('$rtype','V');\" id=".$rtype."_V><input type=hidden id=$rtype value=$rtype>";
	?></td>
</tr>
<tr>
	<th>Dashboard</th>
	<td><?php $rtype = 'DASH'; 
	echo "<select id=$rtype onchange=\"reportList('$rtype');\">";
		echo "<option value=\"".$rtype."_KPI\">Departmental KPIs</option>";
		echo "<option value=\"".$rtype."_TOP\">Top Level KPIs</option>";
		echo "<option value=\"".$rtype."_CF\">Monthly Cashflows</option>";
		if(count($reports[$rtype])+count($reports['S_'.$rtype])>0) {
			displayReportOptions($reports['S_'.$rtype],'S');
			displayReportOptions($reports[$rtype],$rtype);
		}
	echo "</select>";
	echo "&nbsp;<input type=button value=\" Go \" onclick=\"quickReport('$rtype','E');\" id=".$rtype."_E>";// <input type=button value=\"Generate\" onclick=\"quickReport('$rtype','V');\" id=".$rtype."_V>";
	//echo "&nbsp;<input type=button value=\"Generator\" onclick=\"quickReport('$rtype','V');\" id=".$rtype."_V>"; //<input type=hidden id=$rtype value=$rtype>";
	?></td>
</tr>
</table>
<script type=text/javascript>
	reportList('KPI');
	reportList('TL');
	//reportList('CP');
	//reportList('CF');
	//reportList('DASH');
</script>
</body>

</html>
