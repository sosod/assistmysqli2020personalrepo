<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function exportCSV() {
    if(confirm("Please remember that you cannot add new Capital Projects to this document.\nAll new Capital Projects must be added using the Add Capital Projects form.\n\nAre you sure you wish to continue with the export?  If yes, then click OK.")==true)
    {
        document.location.href = 'admin_fin_cp_update_export.php';
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Finance</b></h1>
<h2 class=fc>Update Capital Projects</h2>
<!--<p>&nbsp;</p>-->
<!-- <h3 class=fc>Manually update individual project:</h3>
<p><select><option>--- SELECT ---</option></select> <input type=button value=Go></p>
<p>&nbsp;</p>-->
<form method=post action=admin_fin_cp_update_import.php enctype="multipart/form-data" style="margin-top: 0">
<h3 class=fc>Update multiple projects:</h3>
<ol>
    <li>Export project details: <input type=button value=Export onclick="exportCSV();"><br>
    <i>Please ensure that all new Capital Projects have been added using the "<a href=admin_fin_cp_add.php>Add Capital Project</a>" form before clicking the "Export" button.</i></li>
    <li>Update the financial details in the CSV document saved in step 1.</li>
    <li>Import updated information: <input type=file name=ifile> <input type=submit value=Import><Br>Note: Once you have clicked the "Import" button, you will be given an opportunity to review the Import data before finalising the import.<br>Until you click the "Accept" button, on the next page, no information will be updated.</li>
</ol>
<p style="margin-bottom: 0;"><b>Import file information:</b>
<ol type=i>
<li>The first two lines will be treated as header rows and will not be imported.</li>
<li>The data needs to be in CSV format, seperated by commas, in the same layout as the export file generated in step 1.</li>
<li>The system will upload the financials to the capital project identified by the Reference in the first column.  Any lines that do not contain a valid Reference number will be ignored during the upload.  If you wish to add a capital project please use the "<a href=admin_fin_cp_add.php>Add Capital Project</a>" form.</li>
<li>Please note that, by using this function, Budgets and Actuals for all time periods will be updated with the information in the import file.  You can choose whether to update the Accumulated % Spent - YTD and Accumulated R Spent - YTD from the import file, or to allow the system to automatically calculate it for you.</li>
</ol>
</p>
</form>
<?php
$urlback = "admin_fin.php";
include("inc_goback.php");
?>
</body>

</html>
