<?php
    include("inc_ignite.php");
//	error_reporting(-1);

	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "ZeroX";
	$kpitype = isset($_REQUEST['kpitype']) ? $_REQUEST['kpitype'] : "DEPT";
	
	$sql = "SELECT * FROM ".$dbref."_list_time ORDER BY id";
	include("inc_db_con.php");
	$time = array();
	while($row = mysql_fetch_assoc($rs)) {
		$time[$row['id']] = $row;
	}
	mysql_close($con);
	
	function displayTT($val,$tt,$nf) {
		switch($tt) {
			case 1: return "R ".number_format($val,$nf); break;
			case 2: return number_format($val,$nf)."%"; break;
			case 3: return number_format($val,$nf); break;
		}
	}
	
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.time1 { text-align: right; background-color: #FFE8C4; }
.time2 { text-align: right; background-color: #ddffdd; }
.time3 { text-align: right; background-color: #ffdddd; }
.time0 { text-align: right; background-color: #ddddff; }
.final { text-align: center; background-color: #eeeeee; }
.nochange { background-color: #ffffff; }
.changed { background-color: #00ff00; }
.ref { width: 50px; text-align: center; }
</style>
<script type=text/javascript>
function changeCT(k) {
	var oldC = "old_"+k;
	var newC = "new_"+k;
	if(document.getElementById(oldC).value != document.getElementById(newC).value) {
		document.getElementById(k).className = "changed";
	} else {
		document.getElementById(k).className = "nochange";
	}
}
</script>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Support Admin ~ Calculation Type</h1>
<?php
if($tkuser!="support" || $tkname != "Ignite Support")
	die("<P>You are not authorised to view this page.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");
?>
<form name=calctype method=post action=support_calctype.php>
<p><b>Calculation Type:</b> <select name=id><option selected value=ZeroX>Invalid Zero%</option><?php
$sql = "SELECT * FROM ".$dbref."_list_kpicalctype";
include("inc_db_con.php");
$calctype = array();
while($row = mysql_fetch_assoc($rs)) {
	echo "<option ".($row['code']==$id ? " selected " : "")." value=".$row['code'].">".$row['value']."</option>";
	$calctype[$row['code']] = $row;
}
mysql_close($con);
?></select> <select name=kpitype><option <?php if($kpitype != "TL") { echo selected; } ?> value=DEPT>Departmental KPIs</option><option <?php if($kpitype == "TL") { echo "selected"; } ?> value=TL>Top Level</option></select><input type=submit value=Go /></p>
</form>
<form name=newcalc method=post action=support_calctype_process.php>
<?php
echo "<input type=hidden name=kpitype value=$kpitype />";
echo "<input type=hidden name=id value=$id />";
switch($kpitype) {


case "TL":
	echo "<h2>Top Level KPIs >> ".(isset($calctype[$id]) ? $calctype[$id]['value'] : "Invalid Zero%")."</h2>";
	
	
	
	$display_kpi = array();
	switch($id) {
	case "ZeroX":
		$kpisql[0] = "SELECT DISTINCT k.topid, d.dirtxt as topsub, k.topvalue, k.topdriver, k.topunit, k.topbaseline, k.topcalctype, k.toptargettypeid ";
		$kpisql[1] = "FROM ".$dbref."_toplevel k 
			INNER JOIN ".$dbref."_dir d ON d.dirid = k.topsubid AND d.diryn = 'Y'
			WHERE k.topcalctype = 'ZERO' AND topyn = 'Y'";
		$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (SELECT DISTINCT k.topid ".$kpisql[1].")";
		include("inc_db_con.php");
		$results = array();
		$totals = array();
		while($row = mysql_fetch_assoc($rs)) {
			$results[$row['trtopid']][$row['trtimeid']] = $row;
			if(!isset($totals[$row['trtopid']])) { $totals[$row['trtopid']] = 0; }
			$totals[$row['trtopid']]+=$row['trtarget'];
		}
		mysql_close($con);
		foreach($totals as $key => $t) {
			if($t > 0) { $display_kpi[] = $key; }
		}
		$sql = implode(" ",$kpisql);
		break;
	default:
		$kpisql[0] = "SELECT DISTINCT k.topid, d.dirtxt as topsub, k.topvalue, k.topdriver, k.topunit, k.topbaseline, k.topcalctype, k.toptargettypeid ";
		//$kpisql[1] = "FROM ".$dbref."_toplevel k WHERE k.topcalctype = '$id'";
		$kpisql[1] = "FROM ".$dbref."_toplevel k 
			INNER JOIN ".$dbref."_dir d ON d.dirid = k.topsubid AND d.diryn = 'Y'
			WHERE k.topcalctype = '$id' AND topyn = 'Y'";
		$sql = "SELECT * FROM ".$dbref."_toplevel_result WHERE trtopid IN (SELECT DISTINCT k.topid ".$kpisql[1].")";
		include("inc_db_con.php");
		$results = array();
		$totals = array();
		while($row = mysql_fetch_assoc($rs)) {
			$results[$row['trtopid']][$row['trtimeid']] = $row;
			$display_kpi[] = $row['trtopid'];
		}
		mysql_close($con);
		$sql = implode(" ",$kpisql);
		break;
	}
	//echo $sql;
	echo chr(10)."<table cellpadding=3 cellspacing=0>";
		echo "<tr>";
			//echo "<th rowspan=2>&nbsp;</th>";
			echo "<th rowspan=2 class=ref>TL Ref</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;Directorate&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;KPI&nbsp;Name&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;&nbsp;Definition&nbsp;&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;Driver&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;Baseline&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >Calculation Type</th>";
			echo "<th colspan=3>Overall Result</th>";
			echo "<td rowspan=2 class=noborder>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		foreach($time as $t) {
			if($t['id']%3==0) {
				echo "<th colspan=3>".date("F Y",$t['eval'])."</th>";
			}
		}
		echo "</tr>";
		echo "<tr>";
			echo "<th>Target</th><th>Actual</th><th>R</th>";
		foreach($time as $t) {
			if($t['id']%3==0) {
				echo "<th>Target</th><th>Actual</th><th>R</th>";
			}
		}
		echo "</tr>";
	include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$kpiid = $row['topid'];
		if(in_array($kpiid,$display_kpi)) {
			echo "<tr>";
			//echo "<td><input type=checkbox value=".$kpiid."></td>";
			echo "<td class=ref>".$kpiid."<input type=hidden value=$kpiid name=kpiid[] /></td>";
			echo "<td>".$row['topsub']."</td>";
			echo "<td>".$row['topvalue']."</td>";
			echo "<td width=200><small><small>".$row['topunit']."</small></small></td>";
			echo "<td>".$row['topdriver']."</td>";
			echo "<td>".$row['topbaseline']."</td>";
			echo "<td id=$kpiid class=nochange>".$calctype[$row['topcalctype']]['value']."<input type=hidden name=old_".$kpiid." value=\"".$row['topcalctype']."\" /><br />";
				echo "<select name=new_".$kpiid." onchange=\"changeCT($kpiid);\">";
					foreach($calctype as $c) {
						echo "<option ".($c['code']==$row['topcalctype'] ? " selected " : "")." value=".$c['code'].">".$c['value']."</option>";
					}
				echo "</select>";
			echo "</span></td>";
			$echo = "";
			$ytdtar = 0; $ytdact = 0; $kcount = 0;
			foreach($time as $t) {
				if($t['id']%3==0) {
					$tid = $t['id'];
					$r = $results[$kpiid][$tid];
					$echo.= "<td class=time".($tid%4).">".displayTT($r['trtarget'],$row['toptargettypeid'],0)."</td>";
					$echo.= "<td class=time".($tid%4).">".displayTT($r['tractual'],$row['toptargettypeid'],0)."</td>";
					$k = calcKR($row['topcalctype'],$r['trtarget'],$r['tractual']);
					$echo.= "<td style=\"background-color: ".$krsetup[$k]['color']."\">".$krsetup[$k]['code']."</td>";
					switch($row['topcalctype']) {
						case "STD": case "REV":
							$ytdtar += $r['trtarget'];
							$ytdact += $r['tractual'];
							if($r['trtarget']>0) { $kcount++; }
							break;
						case "ZERO":
							$ytdtar += $r['trtarget'];
							$ytdact += $r['tractual'];
							break;
						case "ACC":
							$ytdtar += $r['trtarget'];
							$ytdact += $r['tractual'];
							break;
						case "CO":
							if($r['trtarget']>$ytdtar) { $ytdtar = $r['trtarget']; }
							if($r['tractual']>$ytdact) { $ytdact = $r['tractual']; }
							break;
					}
				}
			}
			if($kcount>0 && ($row['topcalctype']=="STD" || $row['topcalctype'] == "REV")) { $ytdtar /= $kcount; $ytdact /= $kcount; }
			$krr = calcKR($row['topcalctype'],$ytdtar,$ytdact);
			echo "<td class=final>".displayTT($ytdtar,$row['toptargettypeid'],2)."</td>";
			echo "<td class=final>".displayTT($ytdact,$row['toptargettypeid'],2)."</td>";
			echo "<td class=final style=\"background-color: ".$krsetup[$krr]['color']."\">".$krsetup[$krr]['code']."</td>";
			echo "<td >&nbsp;</td>";
			echo $echo;
			echo "</tr>";
		}
	}
	mysql_close($con);
	echo "</table>";



break;




case "DEPT":
default:

	echo "<h2>Departmental KPIs >> ".(isset($calctype[$id]) ? $calctype[$id]['value'] : "Invalid Zero%")."</h2>";

	$display_kpi = array();
	switch($id) {
	case "ZeroX":
		$kpisql[0] = "SELECT DISTINCT k.kpiid, CONCAT_WS(' - ',d.dirtxt, s.subtxt) as kpisub, k.kpimsr, k.kpivalue, k.kpidriver, k.kpidefinition, k.kpibaseline, k.kpitargetunit, k.kpicalctype, k.kpitargettypeid ";
		$kpisql[1] = "FROM ".$dbref."_kpi k 
			INNER JOIN ".$dbref."_dirsub s ON s.subid = k.kpisubid AND s.subyn = 'Y'
			INNER JOIN ".$dbref."_dir d ON d.dirid = s.subdirid AND d.diryn = 'Y'
			WHERE k.kpicalctype = 'ZERO' AND k.kpiyn = 'Y'";
		$sql = "SELECT * FROM ".$dbref."_kpi_result WHERE krkpiid IN (SELECT DISTINCT k.kpiid ".$kpisql[1].")";
		include("inc_db_con.php");
		$results = array();
		$totals = array();
		while($row = mysql_fetch_assoc($rs)) {
			$results[$row['krkpiid']][$row['krtimeid']] = $row;
			if(!isset($totals[$row['krkpiid']])) { $totals[$row['krkpiid']] = 0; }
			$totals[$row['krkpiid']]+=$row['krtarget'];
		}
		mysql_close($con);
		foreach($totals as $key => $t) {
			if($t > 0) { $display_kpi[] = $key; }
		}
		$sql = implode(" ",$kpisql);
		break;
	default:
		$kpisql[0] = "SELECT DISTINCT k.kpiid, CONCAT_WS(' - ',d.dirtxt, s.subtxt) as kpisub, k.kpimsr, k.kpivalue, k.kpidriver, k.kpidefinition, k.kpibaseline, k.kpitargetunit, k.kpicalctype, k.kpitargettypeid ";
		$kpisql[1] = "FROM ".$dbref."_kpi k 
			INNER JOIN ".$dbref."_dirsub s ON s.subid = k.kpisubid AND s.subyn = 'Y'
			INNER JOIN ".$dbref."_dir d ON d.dirid = s.subdirid AND d.diryn = 'Y'
			WHERE k.kpicalctype = '$id' AND k.kpiyn = 'Y'";
		$sql = "SELECT * FROM ".$dbref."_kpi_result WHERE krkpiid IN (SELECT DISTINCT k.kpiid ".$kpisql[1].")";
		include("inc_db_con.php");
		$results = array();
		while($row = mysql_fetch_assoc($rs)) {
			$results[$row['krkpiid']][$row['krtimeid']] = $row;
			$display_kpi[] = $row['krkpiid'];
		}
		mysql_close($con);
		$sql = implode(" ",$kpisql);
		break;
	}
	
	echo chr(10)."<table cellpadding=3 cellspacing=0>";
		echo "<tr>";
			//echo "<th rowspan=2>&nbsp;</th>";
			echo "<th rowspan=2 class=ref>KPI Ref</th>";
			echo "<th rowspan=2 >Directorate - Sub-Directorate</th>";
			echo "<th rowspan=2 >Assoc. TL&nbsp;KPI</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;KPI&nbsp;Name&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;&nbsp;Definition&nbsp;&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;Driver&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;Baseline&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >&nbsp;&nbsp;Target Unit&nbsp;&nbsp;</th>";
			echo "<th rowspan=2 >Calculation Type</th>";
			echo "<th colspan=3>Overall Result</th>";
			echo "<td rowspan=2 class=noborder>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		foreach($time as $t) {
			echo "<th colspan=3>".date("F Y",$t['eval'])."</th>";
		}
		echo "</tr>";
		echo "<tr>";
			echo "<th>Target</th><th>Actual</th><th>R</th>";
		foreach($time as $t) {
			echo "<th>Target</th><th>Actual</th><th>R</th>";
		}
		echo "</tr>";
	include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$kpiid = $row['kpiid'];
		if(in_array($kpiid,$display_kpi)) {
			echo "<tr>";
			//echo "<td><input type=checkbox value=".$kpiid."></td>";
			echo "<td class=ref>".$kpiid."<input type=hidden value=$kpiid name=kpiid[] /></td>";
			echo "<td>".$row['kpisub']."</td>";
			echo "<td>".$row['kpimsr']."</td>";
			echo "<td>".$row['kpivalue']."</td>";
			echo "<td width=200><small><small>".$row['kpidefinition']."</small></small></td>";
			echo "<td>".$row['kpidriver']."</td>";
			echo "<td>".$row['kpibaseline']."</td>";
			echo "<td>".$row['kpitargetunit']."</td>";
			echo "<td id=$kpiid class=nochange>".$calctype[$row['kpicalctype']]['value']."<input type=hidden name=old_".$kpiid." value=\"".$row['kpicalctype']."\" /><br />";
				echo "<select name=new_".$kpiid." onchange=\"changeCT($kpiid);\">";
					foreach($calctype as $c) {
						echo "<option ".($c['code']==$row['kpicalctype'] ? " selected " : "")." value=".$c['code'].">".$c['value']."</option>";
					}
				echo "</select>";
			echo "</span></td>";
			$echo = "";
			$ytdtar = 0; $ytdact = 0; $kcount = 0;
			foreach($time as $t) {
				$tid = $t['id'];
				$r = $results[$kpiid][$tid];
				$echo.= "<td class=time".($tid%4).">".displayTT($r['krtarget'],$row['kpitargettypeid'],0)."</td>";
				$echo.= "<td class=time".($tid%4).">".displayTT($r['kractual'],$row['kpitargettypeid'],0)."</td>";
				$k = calcKR($row['kpicalctype'],$r['krtarget'],$r['kractual']);
				$echo.= "<td style=\"background-color: ".$krsetup[$k]['color']."\">".$krsetup[$k]['code']."</td>";
				switch($row['kpicalctype']) {
					case "STD": case "REV":
						$ytdtar += $r['krtarget'];
						$ytdact += $r['kractual'];
						if($r['krtarget']>0) { $kcount++; }
						break;
					case "ZERO":
						$ytdtar += $r['krtarget'];
						$ytdact += $r['kractual'];
						break;
					case "ACC":
						$ytdtar += $r['krtarget'];
						$ytdact += $r['kractual'];
						break;
					case "CO":
						if($r['krtarget']>$ytdtar) { $ytdtar = $r['krtarget']; }
						if($r['kractual']>$ytdact) { $ytdact = $r['kractual']; }
						break;
				}
			}
			if($kcount>0 && ($row['kpicalctype']=="STD" || $row['kpicalctype'] == "REV")) { $ytdtar /= $kcount; $ytdact /= $kcount; }
			$krr = calcKR($row['kpicalctype'],$ytdtar,$ytdact);
			echo "<td class=final>".displayTT($ytdtar,$row['kpitargettypeid'],2)."</td>";
			echo "<td class=final>".displayTT($ytdact,$row['kpitargettypeid'],2)."</td>";
			echo "<td class=final style=\"background-color: ".$krsetup[$krr]['color']."\">".$krsetup[$krr]['code']."</td>";
			echo "<td >&nbsp;</td>";
			echo $echo;
			echo "</tr>";
		}
	}
	mysql_close($con);
	echo "</table>";


break;

}
?>
<p style="text-align:center"><input type=submit value="Save Changes" /> <input type=reset /></p>
</form>
<p>&nbsp;</p>
</body>
</html>