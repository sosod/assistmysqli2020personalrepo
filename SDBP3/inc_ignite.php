<?php
require_once '../inc_session.php';
require_once 'lib/inc_SDBP3.php';

//CHECK YEAR COOKIE
if(strlen($modref)==0)
{
    $modver = "SDBP3";
    $modtxt = "2010/2011";
    $modcurr = "Y";
    $modref = "sdp10";
}
$dbref = "assist_".$cmpcode."_".$modref;

//$today = 1289833200;    //set date to 11 Nov 2010 15h00 for dev purposes

$sql = "SHOW TABLES FROM ignittps_i".$cmpcode." LIKE '%".$modref."%'";
include("inc_db_con.php");
	$modmnr = mysql_num_rows($rs);
mysql_close($con);
if($modmnr>0) {
	$stylecss = "<style type=\"text/css\">";
	$krsetup = getKRSetup();
	foreach($krsetup as $krs) {
		$styler[$krs['sort']] = "background-color: ".$krs['color']."; color: ".$krs['color']."; text-align: center;";
		$stylecss.=".krr".$krs['sort']." { ".$styler[$krs['sort']]." }";
	}
	$stylecss.= "</style>";
}

//SETUP TOP LEVEL TYPE
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = '".strtoupper($modref)."' AND refid = 6";
include("inc_db_con.php");
	$r = mysql_num_rows($rs);
	if($r > 0) {
		$row = mysql_fetch_array($rs);
		$setuptopleveltype = $row['value'];
		$err = "N";
	} else {
		$err = "Y";
	}
mysql_close($con);
if($err == "Y") {
	$sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = '".strtoupper($modref)."', refid = 6, value = 'DOC', comment = 'Top Level Type - DOC = Document TBL = database', field = ''";
	include("inc_db_con.php");
	$setuptopleveltype = "DOC";
}
//PRIMARY VERSUS SECONDARY AUTO CLOSURE
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = '".strtoupper($modref)."' AND refid = 6";
include("inc_db_con.php");
	$r = mysql_num_rows($rs);
	if($r > 0) {
		$row = mysql_fetch_array($rs);
		if(is_numeric($row['value'])) {
			$setupsecondtime = $row['value'];
			$err = "N";
		} else {
			$err = "I";
		}
	} else {
		$err = "Y";
	}
mysql_close($con);
if($err == "Y") {
	$sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = '".strtoupper($modref)."', refid = 7, value = '3', comment = 'Days between primary and secondary auto closure', field = ''";
	include("inc_db_con.php");
	$setupsecondtime = 3;
} elseif($err == "I") {
	$sql = "UPDATE assist_".$cmpcode."_setup SET value = '3' WHERE ref = '".strtoupper($modref)."' AND refid = 7";
	include("inc_db_con.php");
	$setupsecondtime = 3;
}


    $munlists = array(
        'munkpa'=>array('name'=>"Municipal KPAs",'id'=>"munkpa",'code'=>"Y",'len'=>150,'fld'=>"code"),
        'wards'=>array('name'=>"Wards",'id'=>"wards",'code'=>"Y",'len'=>50,'fld'=>"numval"),
        'area'=>array('name'=>"Areas",'id'=>"area",'code'=>"N",'len'=>50,'fld'=>"code"),
        'prog'=>array('name'=>"Objective&#047;Programme",'id'=>"prog",'code'=>"N",'len'=>150,'fld'=>"dirid"),
        'fundsource'=>array('name'=>"Funding Source",'id'=>"fundsource",'code'=>"N",'len'=>50,'fld'=>"code"),
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"N",'len'=>150,'fld'=>"code")
    );
    
/*    $fixedlists = array(
        'natkpa'=>array('name'=>"National KPAs",'id'=>"natkpa",'code'=>"Y"),
        'tas'=>array('name'=>"TAS Key Focus Area",'id'=>"tas",'code'=>"Y"),
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"Y"),
        'fundsource'=>array('name'=>"Funding Source",'id'=>"fundsource",'code'=>"N")
    );*/

    $ignitelists = array(
        'kpitargettype'=>array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y"),
        'kpicalctype'=>array('name'=>"Calculation Type",'id'=>"kpicalctype",'code'=>"Y"),
        'ktype'=>array('name'=>"KPI Type",'id'=>"kpitype",'code'=>"N")
    );
    
    $dlist = array(
        'munkpa'=>array('name'=>"Municipal KPAs",'id'=>"munkpa",'code'=>"Y",'len'=>150,'fld'=>"code",'yn'=>"Y",'headfield'=>"munkpa",'kpi'=>"kpimunkpaid"),
        'prog'=>array('name'=>"Objective&#047;Programme",'id'=>"prog",'code'=>"N",'len'=>150,'yn'=>"Y",'headfield'=>"prog",'kpi'=>"kpiprogid"),
        'natkpa'=>array('name'=>"National KPAs",'id'=>"natkpa",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"natkpa",'kpi'=>"kpinatkpaid"),
        'tas'=>array('name'=>"TAS Key Focus Area",'id'=>"tas",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"tas",'kpi'=>"kpitasid"),
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"N",'yn'=>"Y",'headfield'=>"gfs",'kpi'=>"kpigfsid"),
        //'kpicalctype'=>array('name'=>"KPI Calculation Type",'id'=>"kpicalctype",'code'=>"Y",'yn'=>"Y",'headfield'=>"kpicalctype",'kpi'=>"kpicalctypeid",'fld'=>"code"),
        'kpitargettype'=>array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpitargettype",'kpi'=>"kpitargettypeid"),
        'kpistratop'=>array('name'=>"S/O/P",'id'=>"stratop",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpistratop",'kpi'=>"kpistratop"),
        'kpiriskrate'=>array('name'=>"Risk Rating",'id'=>"riskrate",'code'=>"N",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpiriskrate",'kpi'=>"kpiriskrate"),
        'ktype'=>array('name'=>"KPI Type",'id'=>"kpitype",'code'=>"N",'yn'=>"Y",'headfield'=>"ktype",'kpi'=>"kpitypeid")
    );

if($tkuser=="support" && $tkname == "Ignite Support" && $modmnr > 0) {
	$asql = array();
	//AUTO CREATE ADMIN LINKS
	$asql[] = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) SELECT tkid,'Y','AP',0 FROM assist_".$cmpcode."_timekeep WHERE tkid = '$tkid' AND tkid NOT IN (SELECT tkid FROM ".$dbref."_list_admins WHERE yn = 'Y' AND type = 'AP' AND ref = 0 AND tkid = '$tkid')";
	$asql[] = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) SELECT tkid,'Y','TL',0 FROM assist_".$cmpcode."_timekeep WHERE tkid = '$tkid' AND tkid NOT IN (SELECT tkid FROM ".$dbref."_list_admins WHERE yn = 'Y' AND type = 'TL' AND ref = 0 AND tkid = '$tkid')";
	$asql[] = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) SELECT tkid,'Y','KPI',0 FROM assist_".$cmpcode."_timekeep WHERE tkid = '$tkid' AND tkid NOT IN (SELECT tkid FROM ".$dbref."_list_admins WHERE yn = 'Y' AND type = 'KPI' AND ref = 0 AND tkid = '$tkid')";
	$asql[] = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) SELECT tkid,'Y','FIN',0 FROM assist_".$cmpcode."_timekeep WHERE tkid = '$tkid' AND tkid NOT IN (SELECT tkid FROM ".$dbref."_list_admins WHERE yn = 'Y' AND type = 'FIN' AND ref = 0 AND tkid = '$tkid')";
	$asql[] = "INSERT INTO ".$dbref."_list_admins (tkid, yn, type, ref) SELECT '$tkid','Y','DIR',dirid FROM ".$dbref."_dir WHERE diryn = 'Y' AND dirid NOT IN (SELECT ref FROM ".$dbref."_list_admins WHERE yn = 'Y' AND type = 'DIR' AND tkid = '$tkid')";
	foreach($asql as $sql) {
		include("inc_db_con.php");
	}
	$asql = array();
	
}
	logUser($_REQUEST);
?>