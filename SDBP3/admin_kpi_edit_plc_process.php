<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Edit KPI</b></h1>
<p>&nbsp;</p>
<p>Processing...</p>
<?php
$err = "N";
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
$plcid = $_POST['plcid'];
$upkpi = $_POST['upkpi'];
$tbl = $_POST['tbl'];
if(strlen($tbl)==0) { $tbl = "current"; }
//FORM DETAILS
$kpiid = $_POST['kpiid'];
$pname = $_POST['plcprojectname'];
$pdescrip = $_POST['plcprojectdescription'];
$pstartdate = $_POST['plcstartdate'];
$penddate = $_POST['plcenddate'];
$proccate = $_POST['proccate'];
//FORM ARRAYS
$phaseid = $_POST['phaseid'];
$phasetype = $_POST['phasetype'];
$appyn = $_POST['app'];
$cal = $_POST['cal'];
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$target = $_POST['target'];
$custom = $_POST['custom'];
$done = $_POST['done'];
$donedate = $_POST['donedate'];
//ARRAY COUNTERS
$pi = 0;    //phaseid
$pt = 0;    //phasetype
$cl = 0;    //cal
$sd = 0;    //startdate
$ed = 0;    //enddate
$tg = 0;    //target
$cu = 0;    //custom
$sort = 1;

$err = "N";
$err2 = "N";

if($tbl == "temp")
{
    $tabl = "phases_temp";
}
else
{
    $tabl = "phases";
}
/*
echo("<P>plcid-".$plcid);
echo("<P>kpiid-".$kpiid);
echo("<P>tbl-".$tbl);
echo("<P>upkpi-".$upkpi);


echo("<P>pid-");
print_r($phaseid);
echo("<P>pt-");
print_r($phasetype);
echo("<P>app-");
print_r($appyn);
echo("<P>cal-");
print_r($cal);
echo("<P>sd-");
print_r($startdate);
echo("<P>ed-");
print_r($enddate);
echo("<P>tar-");
print_r($target);
echo("<P>cust-");
print_r($custom);
echo("<P>done-");
print_r($done);
echo("<P>donedate-");
print_r($donedate);
*/
//VALIDATE KPIID
if(strlen($kpiid)>0 && is_numeric($kpiid))
{
//PROCESS
    //IF UPKPI == N -> Don't update KPI results
    if($upkpi == "N")
    {
        //UPDATE PLC TABLE
        //Backup old data
        if(strlen($plcid)>0 && is_numeric($plcid))
        {
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_old (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,oldtkid,olddate) ";
            $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as oldtid, ".$today." as olddt FROM assist_".$cmpcode."_".$modref."_kpi_plc p2 WHERE plcid = ".$plcid;
            include("inc_db_con.php");
        }
        //Update with new plc data
        $pname = htmlentities($pname,ENT_QUOTES,"ISO-8859-1");
        $pdescrip = htmlentities($pdescrip,ENT_QUOTES,"ISO-8859-1");
        if(strlen($proccate)!=1 || !is_numeric($proccate) || $proccate < 1 || $proccate > 4)    { $proccate = 1; }
        $psd = explode("-",$pstartdate);    //STARTDATE
        $psd[0] = $psd[0] * 1;  //YEAR
        $psd[1] = $psd[1] * 1;  //MONTH
        $psd[2] = $psd[2] * 1;  //DAY
        $plcstart = mktime(12,0,0,$psd[1],$psd[2],$psd[0]);
        $ped = explode("-",$penddate);    //ENDDATE
        $ped[0] = $ped[0] * 1;  //YEAR
        $ped[1] = $ped[1] * 1;  //MONTH
        $ped[2] = $ped[2] * 1;  //DAY
        $plcend = mktime(12,0,0,$ped[1],$ped[2],$ped[0]);
        //CREATE KPI-PLC RECORD
        if(strlen($plcid)>0 && is_numeric($plcid))
        {
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc SET";
        }
        else
        {
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc SET";
        }
        $sql.= "  plcname = '".$pname."'";
        $sql.= ", plcdescription = '".$pdescrip."'";
        $sql.= ", plckpiid = ".$kpiid;
        $sql.= ", plcstartdate = ".$plcstart;
        $sql.= ", plcenddate = ".$plcend;
        $sql.= ", plcproccate = ".$proccate;
        $sql.= ", plcyn = 'Y'";
        if(strlen($plcid)>0 && is_numeric($plcid))
        {
            $sql.= " WHERE plcid = ".$plcid;
        }
        include("inc_db_con.php");
//        echo("<P>plc-sql:".$sql);
        //GET KPI-PLC ID
        if(strlen($plcid)>0 && is_numeric($plcid))
        {
        }
        else
        {
            $plcid = mysql_insert_id($con);
        }

        if(strlen($plcid)>0 && is_numeric($plcid))
        {
            //UPDATE PHASES
            //Copy all current phases to_phases_old
            $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases_old (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,oldtkid,olddate) ";
            $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
            include("inc_db_con.php");
//            echo("<P>COPY-".$sql);
            //Delete all current phases
            $sql = "DELETE FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
            include("inc_db_con.php");
//            echo("<P>DEL-".$sql);
            //Upload new phases (same process as kpi_add_plc_process.php)
            include("inc_plc_process.php");
            if($err != "Y")
            {
                echo("<P>Project Life Cycle update complete.</p>");
                if($src == "d" || $src == "k")
                {
                    $s=0;
                }
                else
                {
                    $s=$subid;
                }
                if($typ == "op")
                {
                    $urlback = "admin_kpi_edit_list.php?d=".$dirid."&s=".$s."&t=".$typ."&r=".$src."&k=".$kpiid;
                }
                else
                {
                    $urlback = "admin_cp_edit_list.php?d=".$dirid."&s=".$s."&t=".$typ."&r=".$src."&k=".$kpiid;
                }
                include("inc_goback.php");
            }
        }
        else    //validate plcid
        {
            //ERROR
            echo("<P>Error on plcid validation");
            $err2 = "Y";
        }
    }
    //ELSE (UPKPI == N) -> Update KPI results
    else
    {
        //if TBL == current -> First time through the edit page & not a reject of previous edit
            //insert into phases_temp
        //else
            //update phases_temp
        //PLC
        if(is_numeric($kpiid) && $kpiid > 0 && strlen($pstartdate) > 0 && strlen($penddate) > 0)
        {
            //FORMAT KPI-PLC VARIABLES
            $pname = htmlentities($pname,ENT_QUOTES,"ISO-8859-1");
            $pdescrip = htmlentities($pdescrip,ENT_QUOTES,"ISO-8859-1");
            if(strlen($proccate)!=1 || !is_numeric($proccate) || $proccate < 1 || $proccate > 4)    { $proccate = 1; }
            $psd = explode("-",$pstartdate);    //STARTDATE
            $psd[0] = $psd[0] * 1;  //YEAR
            $psd[1] = $psd[1] * 1;  //MONTH
            $psd[2] = $psd[2] * 1;  //DAY
            $plcstart = mktime(12,0,0,$psd[1],$psd[2],$psd[0]);
            $ped = explode("-",$penddate);    //ENDDATE
            $ped[0] = $ped[0] * 1;  //YEAR
            $ped[1] = $ped[1] * 1;  //MONTH
            $ped[2] = $ped[2] * 1;  //DAY
            $plcend = mktime(12,0,0,$ped[1],$ped[2],$ped[0]);
            //CREATE KPI-PLC RECORD
            if(strlen($plcid)>0 && is_numeric($plcid))
            {
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_temp SET";
                $sql.= "  plcid = ".$plcid.", ";
                $plctype = "old";
            }
            else
            {
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc SET";
                $plctype = "new";
            }
            $sql.= " plcname = '".$pname."'";
            $sql.= ", plcdescription = '".$pdescrip."'";
            $sql.= ", plckpiid = ".$kpiid;
            $sql.= ", plcstartdate = ".$plcstart;
            $sql.= ", plcenddate = ".$plcend;
            $sql.= ", plcproccate = ".$proccate;
            $sql.= ", plcyn = 'P'";
            if(strlen($plcid)>0 && is_numeric($plcid))
            {
                $sql.= ", tmptkid = '".$tkid."'";
                $sql.= ", tmpdate = ".$today;
                $sql.= ", tmpyn = 'P'";
            }
            //echo("<P>".$sql);
            include("inc_db_con.php");
            if($plctype == "new")
            {
                $plcid = mysql_insert_id($con);
                $tabl = "phases";
            }
            else
            {
                $tabl = "phases_temp";
                $sql = "DELETE FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp WHERE ppplcid = ".$plcid;
                include("inc_db_con.php");
            }
        }
        //PHASES
        include("inc_plc_process.php");
        //change tbl to "temp"
        if($plctype == "new")
        {
            $tbl = "current";
        }
        else
        {
            $tbl = "temp";
        }
        //transfer to plc_confirm.php
                echo("<form id=step3 method=post action=\"admin_kpi_edit_plc_confirm.php\">");
                echo("<input type=hidden name=kpiid value=\"".$kpiid."\">");
                echo("<input type=hidden name=plcid value=\"".$plcid."\">");
                echo("<input type=hidden name=dirid value=\"".$dirid."\">");
                echo("<input type=hidden name=subid value=\"".$subid."\">");
                echo("<input type=hidden name=typ value=\"".$typ."\">");
                echo("<input type=hidden name=src value=\"".$src."\">");
                echo("<input type=hidden name=tbl value=\"".$tbl."\">");
                echo("</form>");
                echo("<script type=text/javascript>");
                echo("document.forms['step3'].submit();");
                echo("</script>");
    }
}
else    //validate kpiid
{
    //ERROR
    echo("<P>Error on kpiid validation");
    $err2 = "Y";
}
        $tsql = "";
        $told = "";
        $trans = "Updated KPI Plc ".$plcid." for KPI ".$kpiid;
        include("inc_transaction_log.php");

if($err2 == "Y")
{
}
else
{

}
?>
<p>&nbsp;</p>
</body>

</html>
