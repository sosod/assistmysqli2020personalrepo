<?php
    include("inc_ignite.php");
	

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.head1 {
	font-size: 12pt;
	line-height: 16pt;
	text-decoration:underline;
	border: 3px solid #ffffff;
	font-family: Arial;
}
.head2 {
	border: 1px solid #ffffff;
}
</style>
<script type=text/javascript>
function clFn(t) {
//	alert(t);
	if(confirm(t+"\n\nDelete them all?")==true)
	{
        document.location.href = "import_clear.php?t="+t;
	}
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import</b></h1>
<p><input type=button value=" CREATE SDP10 TABLES " onclick="document.location.href='import_create.php';"></p>
<?php if($modmnr>0) { ?>
<table cellpadding=5 cellspacing=0>

<tr><td class=tdgeneral width=49% valign=top style="border-color: #fff;">
<table cellpadding=3 cellspacing=0 style="border: 0px solid #ffffff;" width=100%>


<tr>
    <td class="tdheaderorange head1">Lists</td>
</tr>
<?php 
$lists = array(
	array('id'=>"L_DIR",'value'=>"Directorates"),
	array('id'=>"L_DIRSUB",'value'=>"Sub-Directorates"),
	array('id'=>"L_AREA",'value'=>"Area"),
	array('id'=>"L_FSRC",'value'=>"Funding Source"),
	array('id'=>"L_GFS",'value'=>"GFS Classification"),
	array('id'=>"L_KPA",'value'=>"Municipal KPAs"),
	array('id'=>"PROG",'value'=>"Program"),
	array('id'=>"L_WARDS",'value'=>"Wards")
); 
foreach($lists as $l) {
?>
<tr>
    <td style="" class="tdheaderorange head2"><?php echo $l['value']; ?></td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clFn('<?php echo $l['id']; ?>');" class=orange >
    </td>
</tr>
<?php if($l['id']!="PROG") { ?>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=<?php echo "list_".$l['id']; ?> action=import_lists.php method=post enctype="multipart/form-data">
		<input type=hidden name=t value=<?php echo $l['id']; ?>>
            <b>Load:</b> <input type=file name=ifile> <input type=submit value=Import class=orange >
        </form>
    </td>
</tr>
<?php } } ?>





</table>



</td>
<td class=tdgeneral  valign=top align=right width=2% style="border-color: #fff;">
&nbsp;
</td>
<td class=tdgeneral  valign=top align=right width=49% style="border-color: #fff;">


<table cellpadding=3 cellspacing=0 style="border: 0px solid #ffffff;" width=100%>
<tr>
    <td style="border: 0px solid #ffffff;" class="tdheaderred head1">KPIs</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clFn('KPI');" class=red>
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=kpi action=import_kpi.php method=post enctype="multipart/form-data">
            <b>Load:</b><br>
            &nbsp;&nbsp;&nbsp;<i>File: <input type=file name=ifile><br>
            &nbsp;&nbsp;&nbsp;Directorate:</i> <select name=dirid><option selected value=X>--- SELECT ---</option>
            <?php
                $cdir = array();
                $sql = "select count(kpiid) as kc, dirid from assist_".$cmpcode."_".$modref."_kpi, assist_".$cmpcode."_".$modref."_dirsub, assist_".$cmpcode."_".$modref."_dir where dirid = subdirid and subid = kpisubid group by dirid";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $cdir[$row['dirid']] = $row['kc'];
                    }
                mysql_close();

                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        $id = $row['dirid'];
                        $val = $row['dirtxt'];
                        echo("<option value=".$id.">".$id.": ".$val." (");
                        if(strlen($cdir[$id])==0) { echo("0"); } else { echo($cdir[$id]); }
                        echo(")</option>");
                    }
                mysql_close();
            ?>
            </select><br>
            <input type=submit value=Import class=red><br>
            <small>Ensure that Col A is blank where no KPI is to be loaded<br>else a Sub will be created.</small>
        </form>
    </td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class="tdheaderblue head1">Capital Projects</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clFn('CP');" class=blue>
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=capital action=import_capital.php method=post enctype="multipart/form-data">
			<b>Date:</b> <input type=text name=dtformat><br />
            <b>Load:</b> <input type=file name=ifile> <input type=submit value=Import class=blue>
        </form>
    </td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class="tdheadergreen head1">Cashflow</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral><img src=/pics/blank.gif height=1></td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class="tdheadergreen head2">Revenue By Source</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clFn('RBS');" class=green>
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=revbysource action=import_fin_revbysrc.php method=post enctype="multipart/form-data">
            <b>Load:</b> <input type=file name=ifile> <input type=submit value=Import class=green>
        </form>
    </td>
</tr>

<tr>
    <td style="border: 0px solid #ffffff;" class="tdheadergreen head2">Monthly Cashflow</td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <b>Clear old data:</b> <input type=button value="Clear" onclick="clear('CF');" class=green>
    </td>
</tr>
<tr>
    <td style="border: 0px solid #ffffff;" class=tdgeneral valign=top>
        <form name=cashflow action=import_fin_cashflow.php method=post enctype="multipart/form-data">
            <b>Load data:</b> <input type=file name=ifile>&nbsp;<input type=submit value=Import class=green>
        </form>
    </td>
</tr>


</table>

</td></tr></table>
<?php } ?>
</body>

</html>
