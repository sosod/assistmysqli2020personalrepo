<?php
//error_reporting(-1);
ini_set('max_execution_time',1800);
	require("lib/AmPieChart.php");
	AmChart::$swfObjectPath = "lib/swfobject.js";
	AmChart::$libraryPath = "lib/amcharts";
	AmChart::$jsPath = "lib/AmCharts.js";
	AmChart::$jQueryPath = "lib/jquery.js";
	AmChart::$loadJQuery = true;

if($layout != "P" && $layout != "L") { $layout = "ONS"; }
		$cwidth = 300;
		$cheight = 300;
		$cradius = 100;
		$dlabel = "-20%";
switch($layout) {
	case "L":
		$tdl = 3;
		$cwidth = 250;
		$cheight = 200;
		$cradius = 75;
		break;
	case "P":
		$tdl = 2;
		break;
	default:
		$cwdith = 400;
		$cradius = 120;
		$dlabel = "-30%";
		$tdl = 1;
		$layout = "ONS";
		break;
}
//echo $_SERVER['PHP_SELF'];
//echo strpos($_SERVER['PHP_SELF'],"report_dashboard");
if(strlen($referer)>0) { } else { $referer = "."; }
/* --- FUNCTION TO DRAW THE GRAPH LEGEND WITH LINKS TO KPI REPORT IF NEEDED --- */
function drawLegend($kpi,$dtype,$did) {
//	global $dtype;
//	global $did;
	global $wf;
	global $wt;
	global $layout;
	global $krsetup;
	global $referer;
	$url = "report_kpi_process.php?src=DASH&krsum=Y";
    switch($dtype)
    {
        case "d":
            $url.= "&dirfilter=".$did."&subfilter=ALL";
            break;
        case "s":
            $url.= "&dirfilter=ALL&subfilter=".$did;
            break;
        default:
			$url.= "&dirfilter=ALL&subfilter=ALL";
            break;
    }
	$url.="&output=display&cf=".$wf."&ct=".$wt."&kpiresultfilter=";
	$krsetup2 = array_reverse($krsetup,true);
	$kc = ceil(count($krsetup2)/2);
	echo("<table cellpadding=3 cellspacing=3 style=\"border: 1px solid #ababab;margin: 0 0 0 0;\">");
		echo "<tr><td class=legend><table cellpadding=3 cellspacing=3 style=\"border-width: 0px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				$k++;
				if($k<=$kc) {
					echo "<tr>";
					echo "<td class=legend width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
					echo "<td class=legend>";
					if($referer=="report_dashboard" || strpos($_SERVER['PHP_SELF'],"report_dashboard") > 0) {
						echo "<a href=".$url.$krs['sort']." style=\"color:".$krs['color'].";\">";
					}
					echo $krs['value']."</a> (".$kpi[$krs['sort']]." of ".$kpi['tot'].")</td>";
					echo "</tr>";
				}
			}
		echo "</table></td>";
		echo "<td class=legend><table cellpadding=3 cellspacing=3 style=\"border-width: 0px;margin: 0 0 0 0;\">";
			$k = 0;
			foreach($krsetup2 as $krs) {
				$k++;
				if($k>$kc) {
					echo "<tr>";
					echo "<td class=legend width=10 style=\"background-color: ".$krs['color']."\">&nbsp;</td>";
					echo "<td class=legend>";
					if($referer=="report_dashboard" || strpos($_SERVER['PHP_SELF'],"report_dashboard") > 0) {
						echo "<a href=".$url.$krs['sort']." style=\"color:".$krs['color'].";\">";
					}
					echo $krs['value']."</a> (".$kpi[$krs['sort']]." of ".$kpi['tot'].")</td>";
					echo "</tr>";
				}
			}
		echo "</table></td></tr>";
	echo("</table>");
}
/* --- END DRAWLEGEND FUNCTION --- */


/* --- CALCULATE THE NUMBER OF RESULTS PER CATEGORY --- */
foreach($krsetup as $krs) {
	$kpi[$krs['sort']] = 0;
}
$kpi['err'] = 0;
$kpi['tot'] = 0;
$calctype = array();

$kpiid = array();
$kpicalctype = array();
$kpimoreid = array();
$results = array();

//GET KPIS
$sql = "SELECT k.kpiid, k.kpicalctype, s.subid, d.dirid FROM";
//$sql.= "  ".$dbref."_kpi k";
//$sql.= ", ".$dbref."_dir d, ".$dbref."_dirsub s";
//$sql.= " WHERE k.kpiyn = 'Y' AND k.kpisubid = s.subid AND s.subdirid = d.dirid AND s.subyn = 'Y' AND d.diryn = 'Y' ";
$sql.= " ".$dbref."_kpi k INNER JOIN ".$dbref."_dirsub s ON k.kpisubid = s.subid INNER JOIN ".$dbref."_dir d ON s.subdirid = d.dirid INNER JOIN ".$dbref."_list_munkpa munkpa ON munkpa.id = k.kpimunkpaid INNER JOIN ".$dbref."_list_prog prog ON prog.id = k.kpiprogid INNER JOIN ".$dbref."_list_natkpa natkpa ON natkpa.id = k.kpinatkpaid INNER JOIN ".$dbref."_list_tas tas ON tas.id = k.kpitasid INNER JOIN ".$dbref."_list_gfs gfs ON gfs.id = k.kpigfsid INNER JOIN ".$dbref."_list_targettype kpitargettype ON kpitargettype.id = k.kpitargettypeid INNER JOIN ".$dbref."_list_stratop kpistratop ON kpistratop.code = k.kpistratop INNER JOIN ".$dbref."_list_riskrate kpiriskrate ON kpiriskrate.code = k.kpiriskrate INNER JOIN ".$dbref."_list_kpitype ktype ON ktype.id = k.kpitypeid INNER JOIN ".$dbref."_list_kpicalctype kpicalctype ON kpicalctype.code = k.kpicalctype WHERE k.kpiyn = 'Y' AND d.diryn = 'Y' AND s.subyn = 'Y'";
$sql.= " AND kpiid IN (SELECT DISTINCT krkpiid FROM ".$dbref."_kpi_result)";
    switch($dtype)
    {
        case "d":
            $sql.= " AND d.dirid = ".$did;
            break;
        case "s":
            $sql.= " AND k.kpisubid = ".$did;
            break;
        default:
            break;
    }
include("inc_db_con.php"); //echo $sql;
    while($row = mysql_fetch_array($rs))
    {
		$kid = $row['kpiid'];
		$kpiid[] = $kid;
		$kpicalctype[$kid] = $row['kpicalctype'];
		if($dtype == "d") {
			$kpimoreid[$kid] = $row['subid'];
		} else {
			$kpimoreid[$kid] = $row['dirid'];
		}
    }
mysql_close($con);
//echo "<pre>"; print_r($kpiid); echo "</pre>";
//echo "<P>KPIMOREID "; print_r($kpimoreid);

//GET TARGETS & ACTUALS
$sql = "SELECT sum(krtarget) as krtarget, sum(kractual) as kractual, krkpiid ";
$sql.= " FROM ".$dbref."_kpi_result r ";
$sql.= " WHERE krkpiid IN (".strFn("implode",$kpiid,",","").") ";
$sql.= " AND krtimeid <= $wt AND krtimeid >= $wf ";
$sql.= " GROUP BY krkpiid";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$t = $row['krtarget'];
		$a = $row['kractual'];
		$k = $row['krkpiid'];
		$results[$k]['t'] = $t;
		$results[$k]['a'] = $a;
	}
mysql_close($con);
//echo "<table>";
//CALCULATE RESULT
foreach($kpiid as $k) {
	$ct = $kpicalctype[$k];
	$m = $kpimoreid[$k];
	
	if($ct=="CO") {
		$sql = "SELECT max(krtarget) as krtarget, max(kractual) as kractual, krkpiid ";
		$sql.= " FROM ".$dbref."_kpi_result r ";
		$sql.= " WHERE krkpiid = $k ";
		$sql.= " AND krtimeid <= $wt ";
		include("inc_db_con.php");
			$row = mysql_fetch_array($rs);
		mysql_close($con);
			$t = $row['krtarget'];
			$a = $row['kractual'];
	/*} elseif($ct=="ACC" && $wf > 1) {
		$sql = "SELECT sum(krtarget) as krtarget, sum(kractual) as kractual, krkpiid ";
		$sql.= " FROM ".$dbref."_kpi_result r ";
		$sql.= " WHERE krkpiid = $k ";
		$sql.= " AND krtimeid <= $wt ";
		include("inc_db_con.php");
			$row = mysql_fetch_array($rs);
		mysql_close($con);
			$t = $row['krtarget'];
			$a = $row['kractual'];*/
	} elseif($ct=="STD" || $ct == "REV") {
		$sql = "SELECT count(krtarget) as kc ";
		$sql.= " FROM ".$dbref."_kpi_result ";
		$sql.= " WHERE krkpiid = $k AND krtarget > 0";
		$sql.= " AND krtimeid <= $wt AND krtimeid >= $wf ";
		include("inc_db_con.php");
			$row = mysql_fetch_array($rs);
			$kc = $row['kc']>0 ? $row['kc'] : 1;
		mysql_close($con);
			$t = $results[$k]['t'];
			$a = $results[$k]['a'];
			$t/=$kc;
			$a/=$kc;
	} else {
		$t = $results[$k]['t'];
		$a = $results[$k]['a'];
	}
	if(strlen($t)==0 || !is_numeric($t)) { $t = 0; }
	if(strlen($a)==0 || !is_numeric($a)) { $a = 0; }
	$results[$k]['t'] = $t;
	$results[$k]['a'] = $a;
	
	$krr = calcKR($ct,$t,$a);
	//echo "<tr><td>$k</td><td>$krr</td>";
	$kpi[$krr]++;
	$kpim[$m][$krr]++;
}
$kpitot = 0 + array_sum($kpi) - $kpi['err'];
$kpi['tot'] = $kpitot;

echo "<P style=\"margin: -10 0 0 0; padding: 0 0 0 0;\">"; 
/* --- START THE GRAPH GENERATION --- */
echo "<div align=center style=\"margin: 0 0 0 0; padding: 0 0 0 0;\">";
echo "<table cellpadding=2 cellspacing=0 style=\"border: 0px solid #FFF;\">";
    echo "<tr>";
        echo "<td colspan=".$tdl." style=\"text-align:center;border: 0px solid #FFF;\">";
//MAIN GRAPH
if($kpi['tot'] > 0)
{
	$chart = new AmPieChart("kpi_".$who);
	$chart->setTitle("<p class=charttitle>".$titletxt."</p>");
	foreach($krsetup as $krs) {
		$chart->addSlice($krs['sort'], code($krs['value'])." (".$kpi[$krs['sort']]." of ".$kpi['tot'].")", $kpi[$krs['sort']], array("color" => $krs['color']));
	}
	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" =>$cheight,
		"font" => "Tahoma",
		"decimals_separator" => ".",
		"pie.y" => "49%",
		"pie.radius" => $cradius,
		"pie.height" => 5,
		"background.border_alpha" => 0,
		"data_labels.radius" => $dlabel,
		"data_labels.text_color" => "0xFFFFFF",
		"data_labels.show" => "<![CDATA[{percents}%]]>",
		"legend.enabled" => "false",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
	));
	echo html_entity_decode($chart->getCode());
	drawLegend($kpi,$dtype,$did);
} else {
	echo("<p class=chartitle>".$titletxt."</p>");
	echo("<p>No KPIs to display.</p>");
}
        echo "</td>";
    echo "</tr>";

//SUB GRPAHS
if($who2=="Y")
{
$td2 = 1;
    $morerow = array();
    if($dtype=="d")
    {
	$dt = "s";
        $sql = "SELECT subid id, subtxt txt FROM ".$dbref."_dirsub, ".$dbref."_kpi WHERE kpisubid = subid AND kpiyn = 'Y' AND subdirid = ".$did." AND subyn = 'Y' ORDER BY subsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $morerow[$row['id']] = $row;
            }
        mysql_close();
    }
    else
    {
	$dt = "d";
        $sql = "SELECT dirid id, dirtxt txt FROM ".$dbref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $morerow[$row['id']] = $row;
            }
        mysql_close();
    }
//	echo "<P>MORE: "; print_r($morerow);
//	foreach($morerow as $mr) { echo "<P>".$mr['id']; print_r($kpim[$mr['id']]); }
        echo "<tr> ";
        $tdc = -1;
        foreach($morerow as $mr)
        {
            $tdc++;
			foreach($krsetup as $krs) {
				if(!checkIntRef($kpim[$mr['id']][$krs['sort']])) { $kpim[$mr['id']][$krs['sort']]= 0; }
			}
$tot = array_sum($kpim[$mr['id']]) - $kpim[$mr['id']]['err'];
$kpim[$mr['id']]['tot'] = $tot;
if($tot > 0) {
$chart = new AmPieChart("kpi_".$mr['id']);

if($tdc==$tdl) {
	$td2++;
	if($td2 == 2) {
		echo("</tr><tr style=\"page-break-before: always;\"> ");
		$td2 = 0;
	} else {
		echo("</tr><tr>");
	}
	echo("<td class=tdgeneral align=center style=\"border: 0px solid #FFF;\" valign=top>");
	$chart->setTitle("<p  class=charttitle>".$mr['txt']."</span></p>");
	$tdc=0;
} else {
	$chart->setTitle("<p class=charttitle>".$mr['txt']."</p>");
	echo("<td class=tdgeneral align=center style=\"border: 0px solid #FFF;\" valign=top>");	
}

foreach($krsetup as $krs) {
	$chart->addSlice($krs['sort'], code($krs['value'])." (".$kpim[$mr['id']][$krs['sort']]." of ".$kpim[$mr['id']]['tot'].")", $kpim[$mr['id']][$krs['sort']], array("color" => $krs['color']));
}

    $chart->setConfigAll(array(
    "width" => $cwidth,
    "height" =>$cheight,
	"font" => "Tahoma",
	"decimals_separator" => ".",
    "pie.y" => "50%",
    "pie.radius" => $cradius,
    "pie.height" => 5,
    "background.border_alpha" => 0,
    "data_labels.radius" => $dlabel,
    "data_labels.text_color" => "0xFFFFFF",
    "data_labels.show" => "<![CDATA[{percents}%]]>",
    "legend.enabled" => "false",
    "balloon.enabled" => "true",
    "balloon.alpha" => 80,
    "balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;percents&#125;%&#93;&#93;>"
    ));

	echo html_entity_decode($chart->getCode());
	drawLegend($kpim[$mr['id']],$dt,$mr['id']);
} else {
	if($tdc==$tdl) {
		$td2++;
		if($td2 == 2) {
			echo("</tr><tr style=\"page-break-before: always;\"> ");
			$td2 = 0;
		} else {
			echo("</tr><tr>");
		}
		echo("<td class=tdgeneral align=center style=\"border: 0px solid #FFF;\" valign=top>");
		$tdc=0;
	} else {
		echo("<td class=tdgeneral align=center style=\"border: 0px solid #FFF;\" valign=top>");	
	}
	echo("<p class=charttitle>".$mr['txt']."</p>");
	echo("<p>No KPIs to display.</p>");
}
            echo("</td>");
        }
        echo "</tr>";
    }
echo "</table>";
echo "</div>";

//echo "<P>KPIM "; print_r($kpim);
?>