<?php
    include("inc_ignite.php");
	
	$tid = $_REQUEST['t'];
	$act = $_REQUEST['act'];
	$result = array();

	if(!checkIntRef($tid))
		die("An error has occurred.  Please go back and try again.");
	
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$tid;
    include("inc_db_con.php");
        $trow = mysql_fetch_array($rs);
    mysql_close($con);
	if($act == "SAVE") {
		$err = "";
		$flds = array('arem'=>"arem",'aclose'=>"aclose",'aclosekpi'=>"aclosekpi");
		$vars = $_REQUEST;
		foreach($flds as $f) {
			$val = $vars[$f];
			if(strlen($val)>0) {
				$v = strFn("explode",$val," ","");
				$d = mktime(23,59,0,getMonth($v[1]),$v[0],$v[2]);
				if($d<$today) { $d = 0; }
			} else {
				$d = 0;
			}
			$flds[$f] = $d;
		}
		/************
		TP VALIDATION
		1. Auto Rem < Primary
		2. Primary < Secondary
		************/
		if($flds['aclose']==0 && $flds['arem']>0) {
			$result[0] = "error";
			$result[1] = "An Auto Reminder date can only be set if a Primary Closure date is selected.";
		}
		if($flds['aclose']<$flds['arem']) {
			$result[0] = "error";
			$result[1] = "The Auto Reminder date must occur before the Primary Closure date.";
		}
		if($flds['aclosekpi']<$flds['aclose'] && $trow['active']!="N" && $flds['aclosekpi']>0) {
			$result[0] = "error";
			$result[1] = "The Secondary Closure must occur after the Primary Closure date.";
		}

		if(count($result)==0) {
			$sql = "UPDATE ".$dbref."_list_time SET arem = ".$flds['arem'].", aclose = ".$flds['aclose'].", aclosekpi = ".$flds['aclosekpi']." WHERE id = $tid";
			include("inc_db_con.php");
			$trow['arem'] = $flds['arem'];
			$trow['aclose'] = $flds['aclose'];
			$trow['aclosekpi'] = $flds['aclosekpi'];
			$result[0] = "check";
			$result[1] = "Changes Saved.";
		}
	}

	
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
body { font: 62.5%; }
.left { text-align: left; }
table td {
	padding-left: 10px;
}
</style>
<?php include("inc_head_msie.php"); ?>
		<script type="text/javascript">
			$(function(){

                //Closure
                $('#datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    changeMonth:true,
                    changeYear:true
                });

                //Emails
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    changeMonth:true,
                    changeYear:true
                });

                //2nd
                $('#datepicker3').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    changeMonth:true,
                    changeYear:true
                });
			});

function closeTP(t,txt) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        if(confirm("Are you sure you wish to close the time period "+txt+" for all primary users?\nTo reopen the time period, you will need to contact Ignite Advisory Services.\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "setup_time_configure_close.php?act=CP&t="+t;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}
function closeTP2(t,txt) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        if(confirm("Are you sure you wish to close the time period "+txt+" for all secondary users and Top Level?\nTo reopen the time period, you will need to contact Ignite Advisory Services.\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "setup_time_configure_close.php?act=CS&t="+t;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}

		</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
echo "<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Time Periods ~ Update</b></h1>";
if(count($result)==2) {
	displayResult($result);
}

    $tval = date("d F Y", $trow['sval'])." - ".date("d F Y", $trow['eval']);

	$width=150;

echo "<h2>$tval</h2>";
echo "<form name=edit method=post action=setup_time_configure.php>";
echo "<input type=hidden name=t value=\"$tid\"><input type=hidden name=act value=SAVE>";
echo "<table cellpadding=5 cellspacing=0 width=650>";
    echo "<tr>";
		echo "<th class=left width=$width>Auto Reminder:</th>";
		echo "<td valign=top>";
		if($trow['active']=="Y") {
			//echo "<input type=text name=arem id=\"datepicker2\" readonly=\"readonly\" value=\"";
			//if($trow['arem']>0) { echo date("d M Y",$trow['arem']); }
			//echo "\">";
			echo "N/A";
		} else {
			echo "N/A<input type=hidden name=arem>";
		}
		echo "</td>";
	echo "</tr>";
    echo "<tr>";
		echo "<th class=left width=$width>Primary Closure:</th>";
		echo "<td valign=top>";
		if($trow['active']=="Y") {
//			echo "<input type=text name=aclose id=\"datepicker\" readonly=\"readonly\" value=\"";
//			if($trow['aclose']>0) { echo date("d M Y",$trow['aclose']); }
//			echo "\">";
//			if($trow['eval']<$today) { echo " <input type=button value=\" Close \" onclick=\"closeTP(".$trow['id'].",'$tval')\">"; }
			echo "N/A";
		} else {
			echo "Closed<input type=hidden name=aclose>";
		}
		echo "</td>";
	echo "</tr>";
    echo "<tr>";
		echo "<th class=left width=$width>Secondary Closure:</th>";
		echo "<td valign=top>";//<input type=text name=aclosekpi id=\"datepicker3\" readonly=\"readonly\" value=\"";
		if($trow['activekpi']=="Y") {
//			if($trow['aclosekpi']>0) { echo date("d M Y",$trow['aclosekpi']); }
//			echo "\">";
			echo "N/A";
			if($trow['eval']<$today && $trow['active']!="Y") { echo " <input type=button value=\" Close \" onclick=\"closeTP2(".$trow['id'].",'$tval')\">"; }
		} else {
			echo "Closed<input type=hidden name=aclosekpi>";
		}
		echo "</td>";
	echo "</tr>";
    echo "<tr>";
		echo "<th class=left width=$width>&nbsp;</th>";
		echo "<td valign=top><input type=submit value=\" Save Changes \" disabled=disabled>";
			
		echo "</td>";
	echo "</tr>";
echo "</table>";
echo "</form>";
?>
<p style="font-size: 7.5pt; margin-top: 5px;"><i> Note:<br>
<span style="font-size: 6.5pt;">+</span> To cancel an Auto Closure or Auto Reminder, set the date in the past.<br>
<span style="font-size: 6.5pt;">+</span> Time periods may only be closed once the end date has passed.<br>
<span style="font-size: 6.5pt;">+</span> Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.<br>
<span style="font-size: 6.5pt;">+</span> Auto Reminders are only applicable to the Primary Closure dates.  Reminders cannot be sent for Secondary Closure dates.<br>
<span style="font-size: 6.5pt;">+</span> The time period can only be closed for secondary users and Top Level after the time period has been closed for primary users.<br>
<span style="font-size: 6.5pt;">+</span> All automatic closures and reminder emails occurr at 23:59 on the date selected.</i></p>
<?php
$urlback = "setup_time_list.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
