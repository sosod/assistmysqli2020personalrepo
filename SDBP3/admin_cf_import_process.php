<?php
include("inc_ignite.php");
$modref = strtoupper($modref);

	$topidarr = array();
if($tkuser != "support" || $tkname != "Ignite Support")
	die("<P>You are not authorised to view this page.</p>");

$act = $_REQUEST['act'];
$csql = array();
?>
<html>
<head>
	<meta http-equiv="Content-Language" content="en-za">
	<title>www.Ignite4u.co.za</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdhead2 { background-color: #efefef; text-align:center;}
.done { background-color: #009900; }
.ok { background-color: #000099; }
.err { background-color: #CC0001; color: #FFFFFF; }
.th1 { background-color: #000099; color: #FFFFFF; font-weight: bold; text-align: center; }
.th2 { background-color: #009900; color: #FFFFFF; font-weight: bold; text-align: center; }
.th3 { background-color: #FE9900; color: #FFFFFF; font-weight: bold; text-align: center; }
.th0 { background-color: #CC0000; color: #FFFFFF; font-weight: bold; text-align: center; }
.th4 { background-color: #555555; color: #FFFFFF; font-weight: bold; text-align: center; }
.th1b { background-color: #aaaaff; font-weight: bold; }
.th2b { background-color: #aaffaa; font-weight: bold; }
.th3b { background-color: #ffc771; font-weight: bold; }
.th0b { background-color: #ffaaaa; font-weight: bold; }
.th4b { background-color: #aaaaaa; font-weight: bold; }
.td1 { background-color: #ccccff; text-align: right; }
.td2 { background-color: #ccffcc; text-align: right; }
.td3 { background-color: #ffe5bb; text-align: right; }
.td0 { background-color: #ffdddd; text-align: right; }
.td4 { background-color: #e9e9e9; text-align: right; }
table { border-color: #555555; }
table td { border-color: #555555; }
</style>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc>SDBIP <?php echo($modtxt); ?>: Admin ~ Amend Monthly Cashflow [Import]</h1>
<?php 
if($act == "SAVE")
	echo "<h2>Finalise Import</h2>";
else
	echo "<h2>Validate Import</h2>";

//UPLOAD FILE
if($act!="SAVE") {
	if($_FILES["ifile"]["error"] > 0) { //IF ERROR WITH UPLOAD FILE
		switch($_FILES["ifile"]["error"])
		{
			case 2:
				echo("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
				break;
			case 4:
				echo("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
				break;
			default:
				echo("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
				break;
		}
		die();
	} else { //IF ERROR WITH UPLOAD FILE
		$ext = substr($_FILES['ifile']['name'],-3,3);
		if(strtolower($ext)!="csv") {
			echo("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
			die();
		} else {
			$filename = "CF-IMPORT_".substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
			$fileloc = "../files/".$cmpcode."/".$modref."/import/".$filename;
			//UPLOAD UPLOADED FILE
			set_time_limit(180);
			copy($_FILES["ifile"]["tmp_name"], $fileloc);
		}
	}
} else {	//act = save
	$fileloc = $_REQUEST['fileloc'];
}	//act=save

if(!file_exists($fileloc)) {
    echo("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    die();
}
	
//SETUP VARIABLES
$sql = "SELECT max(lineid) as lineid FROM ".$dbref."_finance_lineitems";
include("inc_db_con.php");
	$row = mysql_fetch_array($rs);
	$new_topid = 1;
	$new_topid+= $row['lineid'];
mysql_close($con);
$sql = "SELECT subid, subtxt FROM ".$dbref."_dirsub WHERE subyn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$sub[$row['subid']] = $row['subtxt'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_gfs WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$gfs[$row['id']] = $row['value'];
	}
mysql_close($con);

set_time_limit(180);
$file = fopen($fileloc,"r");
$data = array();
while(!feof($file)) {
	$tmpdata = fgetcsv($file);
	if(count($tmpdata)>1) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($file);
unset($data[0]);
unset($data[1]);
unset($data[2]);
$head = array();
//	$head[0] = array("","PMS Ref (IDP)","*DIR ID","Directorate","*GFS Vote","*National KPA","*TAS Key Focus Area","*IDP Goal","*Municipal Key Performance Area","*Key Performance Indicator","Unit of measurement","Risk","*Ward","*Area","Program Driver","Baseline (31/12/09)","*Calculation Type","*Calculation Action","*Target Type","*Annual Target","Revised Target","Qtr ending 30 Sep","Qtr ending 31 Dec","Qtr ending 31 March","Qtr ending 30 June","2011/12","2012/13","2013/14","2014/15","Comments");
//	$head[1] = array("","Text (50 characters)","ID as per CONSTANTS","Name as per CONSTANTS","ID as per LISTS","CODE as per LISTS","CODE as per LISTS","Text (200 characters)","CODE as per LISTS","Text (200 characters)","Text (200 characters)","Text (200 characters)","Mun. Ref as per CONSTANTS","Ignite Ref as per CONSTANTS","Text (150 characters)","Text (150 characters)","CODE as per LISTS","SUM or AVE","CODE as per LISTS","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY","Text");
//$head[0] = array("","*SUB ID","Sub-Directorate","*Line Item","*GFS Classification","Vote Number","*Month/Time Period ID","Revenue Original Budget","Opex Original Budget","Capex Original Budget");
//$head[1] = array("","Ignite Ref","","200 characters","Ignite Ref","20 characters","Month name in full OR Time Period ID e.g. July = 1, August = 2 etc.","NUMBER ONLY","NUMBER ONLY","NUMBER ONLY");
$head[0] = array("Line ID","*SUB ID","Sub-Directorate","*Line Item","*GFS Classification","Vote Number");
for($i=1;$i<=12;$i++) {
	$head[0][]= date("F",mktime(12,0,0,$i+6,1,2010));
	$head[0][] = "";
	$head[0][] = "";
}
$head[0][] = "Total for the year";
$head[0][] = "";
$head[0][] = "";
$head[1] = array("","Ignite Ref","","200 characters","Ignite Ref","20 characters");
for($i=1;$i<=13;$i++) {
	$head[1][] = "Revenue Original Budget";
	$head[1][] = "Opex Original Budget";
	$head[1][] = "Capex Original Budget";
}
$cols = count($head[0]);
//CREATE FORM
?>
<form name=import id=imp action=admin_cf_import_process.php method=post>
<input type=hidden name=act value=SAVE><input type=hidden name=fileloc value="<?php echo $fileloc; ?>">
<table cellpadding=3 cellspacing=0>
<?php
$r = 0;
	foreach($head as $d) {
		echo "<tr><td>&nbsp;</td>";
		switch($r) {
			case 0: 
				$class="tdheader"; 
				break;
			case 1: 
				$class="tdhead2"; 
				break;
			default:
				$class="";
				break;
		}
		for($c=0;$c<$cols;$c++) {
			$t = $d[$c];
			$colspan=1;
			if($c>=6) {
				if($c<42) {
					$cl = "class=th".((floor($c/3)-1)%4);
				} else {
					$cl = "class=th4";
				}
				if($r==1) { $cl.="b"; } else { $colspan=3; }
			} else {
				$cl = "class=$class";
			}
			echo "<td $cl nowrap colspan=$colspan >".$t."</td>";
			$c+=$colspan-1;
		}
		echo "</tr>";
		$r++;
	}
	$e = 0;
	$idarr = array();
	$idarr[] = 0;
	foreach($data as $d) {
		set_time_limit(1800);
		$class = ""; $class1 = "";
		$csql = array(); $budget = array(); $total = array();
		$err = "N";
		$c = 0;
		for($c=0;$c<$cols;$c++) {
			$etxt = "";
			$t = trim($d[$c]);
			switch($c) {
				case 0:	// 0 = *Ignite ref
					$t = $new_topid++;
					if(!checkIntRef($t)) {
						$err = "Y";
						$etxt = "ERROR - Not a valid number";
					} elseif(is_numeric(array_search($t,$idarr))) {
						$err = "Y";
						$etxt = "ERROR - Duplicate";
					} else {
						$id = $t;
						$idarr[] = $t;
					}
					break;
				case 1:	// SUB ID
					if(!checkIntRef($t)) {
						$err = "Y";
						$etxt = "ERROR - Invalid ID";
					} elseif(!array_key_exists($t,$sub)) {
						$etxt = "ERROR - Invalid ID";
						$err = "Y";
					} else {
						$csub = $sub[$t];
						$etxt = "=> ".$csub;
						$subid = $t;
					}
					break;
				case 2:	// 2 = Sub-Directorate
					break;
				case 3:	// Line Item
					if(strlen($t)>200 || strlen(code($t)) > 250) {
						$err = "Y";
						$etxt = "ERROR - too long";
					} else {
						if(strlen($t)==0) {
							$err = "Y";
							$etxt = "ERROR - Required field";
						} else {
							$linevalue = code($t);
						}
					}
					break;
				case 4:	// 4 = *GFS Vote
					if(!checkIntRef($t)) {
						$err = "Y";
						$etxt = "ERROR - Invalid ID";
					} elseif(!array_key_exists($t,$gfs)) {
						$etxt = "ERROR - Invalid ID";
						$err = "Y";
					} else {
						$csub = $gfs[$t];
						$etxt = "=> ".$csub;
						$gfsid = $t;
					}
					break;
				case 5:	// 6 = Vote number
					if(strlen($t)>20 || strlen(code($t)) > 100) {
						$err = "Y";
						$etxt = "ERROR - too long";
					} else {
						$linevote = code($t);
					}
					break;
				//Values
				case 6:		case 7:		case 8:		//july
				case 9:		case 10:	case 11:	//august
				case 12:	case 13:	case 14:	//september
				case 15:	case 16:	case 17:	//october
				case 18:	case 19:	case 20:	//november
				case 21:	case 22:	case 23:	//december
				case 24:	case 25:	case 26:	//january
				case 27:	case 28:	case 29:	//february
				case 30:	case 31:	case 32:	//march
				case 33:	case 34:	case 35:	//april
				case 36:	case 37:	case 38:	//may
				case 39:	case 40:	case 41:	//june
					if(strlen($t)>0 && !is_numeric($t)) {
						$err = "Y";
						$etxt = "ERROR - Not a valid number";
					} else {
						if(strlen($t)==0) {
							$t = 0;
						}
						$timeid = floor($c/3)-1;
						if(floor($c/3)==($c/3)) {
							$fld = "cfrev1";
						} elseif($c%3==1) {
							$fld = "cfop1";
						} else {
							$fld = "cfcp1";
						}
						$budget[$timeid][$fld] = $t*1;
						$total[$fld]+=$t*1;
					}
					break;
				//total for the year
				case 42:
					$etxt = $total['cfrev1'];
					break;
				case 43: 
					$etxt = $total['cfop1'];
					break;
				case 44:
					$etxt = $total['cfcp1'];
					break;
			}	//end switch
			if(strlen($etxt)>0) {
				$d[$c].= " ".$etxt;
			} elseif($c==0) {
				$d[$c] = $t." ".$etxt;
			} elseif($d[$c]!=$t || $t==0) {
				$d[$c] = $t;
			}
		}	//for
		if($err == "Y") {
			$e++;
			$class = "err";
			$class1 = $class;
			$budget = array();
			$csql = array();
		} else {
			//CREATE CSQL
			$csql[] = "INSERT INTO ".$dbref."_finance_lineitems (lineid, linevalue, lineyn, linesort, linetype, linesubid, linegfsid, linevote) VALUES ($id,'$linevalue','Y',$id,'CF',$subid,$gfsid,'$linevote')";
			$logold = $csql[count($csql)-1];
			for($r=1;$r<=12;$r++) {
				$csql[] = "INSERT INTO ".$dbref."_finance_cashflow (cflineid,cfyn,cftimeid,cfrev1,cfop1,cfcp1,cfrev8,cfop8,cfcp8) VALUES ($id,'Y',$r,".$budget[$r]['cfrev1'].",".$budget[$r]['cfop1'].",".$budget[$r]['cfcp1'].",".$budget[$r]['cfrev1'].",".$budget[$r]['cfop1'].",".$budget[$r]['cfcp1'].")";
			}
			if($act == "SAVE") {
				//RUN CSQL
				foreach($csql as $sql) {
					//echo "<P>".$sql;
					include("inc_db_con.php");
				}
				$class = "done";
				$class1 = "";
			} else {
				$class = "ok";
				$class1 = "";
			}
		}
		echo "<tr><td class=\"$class\">&nbsp;</td>";
		$c = 0;
		for($c=0;$c<$cols;$c++)
		{
			if($c>=6) {
				if($c<42) {
					$class1 = "td".((floor($c/3)-1)%4);
				} else {
					$class1 = "td4";
				}
				$v = number_format($d[$c],2);
			} else {
				$v = $d[$c];
			}
			echo "<td class=\"$class1\" nowrap>".$v."</td>";
		}
		echo "</tr>";
		//ECHO SQL
		/*if($act != "SAVE") {
			echo "<tr><td>&nbsp;</td>";
			echo "<td colspan=$cols nowrap>";
			if($err == "N")
				arrPrint($csql);
			else
				echo "ERROR encountered.";
			echo "</td>";
			echo "</tr>";
		}*/
	}	//foreach $data
?>
</table>
<?php
	$sql = "INSERT INTO assist_".$cmpcode."_log (id,date,tkid,ref,transaction,tsql,told) VALUES (null,$today,'$tkid','$modref','Reimported Monthly Cashflow - $fileloc','','')";
if($act != "SAVE") {
	if($e>0) {
		echo "<P style=\"text-align:center;\">$e errors found.<br>Monthly Cashflow cannot be imported.</p>";
	} else {
		echo "<p style=\"text-align:center;\"><input type=submit value=\"  Accept  \" style=\"color: #FFFFFF; background-color: #009900; padding: 10 10 10 10;\"></p>";
	}
} else {
	include("inc_db_con.php");
	echo "<P style=\"text-align:center;font-weight: bold; color: #009900;\">Monthly Cashflow imported.</p>";
}	
	
?>
</body></html>	