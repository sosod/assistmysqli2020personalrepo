<?php
if( (array_sum($budg) + array_sum($actul) ) > 0 ) {

// Initialize the chart (the parameter is just a unique id used to handle multiple
// charts on one page.)
$chart = new AmBarChart("fin_".$who);

// The title we set will be shown above the chart, not in the flash object.
// So you can format it using CSS.
$chart->setTitle("<span style=\"font-weight: bold;\">".$titletxt."</span>");

// Add all values for the X axis
$chart->addSerie("r", "Revenue");
$chart->addSerie("o", "Operational Expenditure");
$chart->addSerie("c", "Capital Expenditure");

// Define graphs data
$budget = array("r" => $budg['r'], "o" => $budg['o'], "c" => $budg['c']);
$actual = array("r" => $actul['r'], "o" => $actul['o'], "c" => $actul['c']);

// Add graphs
$chart->addGraph("budget", "Budget", $budget, array("color" => "#FF9900"));
$chart->addGraph("actual", "Actuals", $actual, array("color" => "#009900"));

//SET CONFIG
$chart->setConfigAll(array(
"width" => 450,
"height" => 350,
"depth" => 10,
"angle" => 45,
"font" => "Tahoma",
"text_size" => 10,
"decimals_separator" => ".",
"thousands_separator" => ",",
"plot_area.margins.top" => 40,
"plot_area.margins.right" => 20,
"plot_area.margins.left" => 100,
"plot_area.margins.bottom" => 40,
"background.border_alpha" => 0,
"legend.enabled" => "false",
"balloon.enabled" => "true",
"balloon.alpha" => 80,
"balloon.show" => "<!&#91;CDATA&#91;&#123;title&#125;: &#123;value&#125;&#93;&#93;>",
"column.data_labels" => "<!&#91;CDATA&#91;&#93;&#93;>",
"column.data_labels_text_color" => "#ffffff",
"column.width" => 75

));



?>
<style type=text/css>
table {
    border: 1px solid #ffffff;
}
table td {
    border: 1px solid #ffffff;
}
.fl {
	font-size: 6.5pt;
	line-height: 7.5pt;
}
.fl-line {
	text-align: right;
	font-weight: bold;
}
.fl-th-b {
	background-color: #FF9900; 
	font-weight: bold; 
	vertical-align: middle;
}
.fl-th-a {
	background-color: #009900; 
	font-weight: bold;
	vertical-align: middle;
}
.fl-td-b {
	text-align: right; 
	background-color: #FED97F;
}
.fl-td-a {
	text-align: right; 
	background-color: #BCFFB1
}
</style>
<div align=center>
<?php
echo html_entity_decode($chart->getCode());
?>
            <table cellpadding=3 cellspacing=0 width=400>
                <tr>
                    <td width=60>&nbsp;</td>
                    <td width=170 class="fl fl-th-b">Adjusted Budget</td>
                    <td width=170 class="fl fl-th-a">Actuals</td>
                </tr>
                <tr>
                    <td class="fl fl-line">Revenue:</td>
                    <td class="fl fl-td-b">&nbsp;R <?php echo(number_format($budg['r'],2)); ?></td>
                    <td class="fl fl-td-a">&nbsp;R <?php echo(number_format($actul['r'],2)); ?></td>
                </tr>
                <tr>
                    <td class="fl fl-line">Opex:</td>
                    <td class="fl fl-td-b">&nbsp;R <?php echo(number_format($budg['o'],2)); ?></td>
                    <td class="fl fl-td-a">&nbsp;R <?php echo(number_format($actul['o'],2)); ?></td>
                </tr>
                <tr>
                    <td class="fl fl-line">Capex:</td>
                    <td class="fl fl-td-b">&nbsp;R <?php echo(number_format($budg['c'],2)); ?></td>
                    <td class="fl fl-td-a">&nbsp;R <?php echo(number_format($actul['c'],2)); ?></td>
                </tr>
            </table>
<?php
}
else
{
echo("<div style=\"text-align: center\"><p><span style=\"font-weight: bold;\">".$titletxt."</span></p>");
echo("<p>No Financials to display.</p></div>");
}
?>