<?php
	include("inc_ignite.php");
	$head = getHeadings("headtype = 'CP' AND headyn = 'Y' ORDER BY headdetailsort");
//	$headr = getHeadings("headtype = 'CPR' AND headyn = 'Y' ORDER BY headdetailsort");
	$headf = getHeadings("headtype = 'CPF' AND headyn = 'Y' ORDER BY headdetailsort");
	$gfs = getListKPI($dlist['gfs']);
	$fundsrc = getListKPI($munlists['fundsource']);
	$time = getTime('all');
	$foreyears = getForecastYears();
	$wards = getListKPI($munlists['wards']);
	$area = getListKPI($munlists['area']);
	$width=130;
	
	$result = array();
	$v = $_REQUEST;
	$act = $v['action'];
//	print_r($v);
	switch($act)
	{
		case "add":
			$title = "Add";
			$nextact = "addsave";
			break;
		case "addsave":
			$title = "Add";
			$nextact = "addsave";
			//GET DATA
			$subid = $v['subid'];
			$gfsid = $v['gfs'];
			$fsr = $v['fundsource'];
			$fundsource = array();
			foreach($fsr as $f) { $fundsource[$f] = array('id'=>$f,'yn'=>"Y"); }
			$ws = $v['wards'];
			$wds = array();
			foreach($ws as $w) { $wds[$w] = array('id'=>$w,'yn'=>"Y"); }
			if($wds[1]['yn']=="Y") { $wds = array(); $wds[1] = array('id'=>1,'yn'=>"Y"); } //IF ALL[1] WARD SELECTED REMOVE ALL OTHER SELECTED
			$aa = $v['area'];
			$ara = array();
			foreach($aa as $a) { $ara[$a] = array('id'=>$a,'yn'=>"Y"); }
			if($ara[1]['yn']=="Y") { $ara = array(); $ara[1] = array('id'=>1,'yn'=>"Y"); } //IF ALL[1] AREA SELECTED REMOVE ALL OTHER SELECTED
			//VALIDATE REQUIRED FIELDS
			$err = "N"; $errtxt = "Please complete all required fields:";
			if(!checkIntRef($subid)) { $err = "Y"; $errtxt.="<br />Sub-Directorate"; }
			foreach($head as $hd) {
				if($hd['headmaxlen']==0) {
					switch($hd['headfield'])
					{
						case "area":
							if(count($ara)==0)  { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; }
							break;
						case "fundsource":
							if(count($fundsource)==0)  { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; }
							break;
						case "gfs":
							if(!checkIntRef($gfsid)) { $err = "Y"; $errtxt.="<br />GFS Classification"; }
							break;
						case "wards":
							if(count($wds)==0)  { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; }
							break;
					}
				} else {
					if(strlen($v[$hd['headfield']])==0 && $hd['headrequired']=="Y") { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; } 
				}
			}
			if($err == "N") {
				$start1 = $v['cpstartdate'];
				$start2 = $v['cpstartactual'];
				$end1 = $v['cpenddate'];
				$end2 = $v['cpendactual'];
				if(strlen($start1)==0) { 
					$cpstartdate = 0; 
				} else {
					$st = strFn("explode",$start1,"-","");
					$cpstartdate = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				if(strlen($start2)==0) { 
					$cpstartactual = 0; 
				} else {
					$st = strFn("explode",$start2,"-","");
					$cpstartactual = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				if(strlen($end1)==0) { 
					$cpenddate = 0; 
				} else {
					$st = strFn("explode",$end1,"-","");
					$cpenddate = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				if(strlen($end2)==0) { 
					$cpendactual = 0; 
				} else {
					$st = strFn("explode",$end2,"-","");
					$cpendactual = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				//CREATE CAPITAL PROJECT IN DBREF_CAPITAL
				$sql = "INSERT INTO ".$dbref."_capital (cpid, cpsubid, cpgfsid, cpnum, cpidpnum, cpvotenum, cpname, cpproject, cpstartdate, cpenddate, cpstartactual, cpendactual, cpyn, cpfundsourceid) VALUES ";
				$sql.= "(null, $subid, $gfsid, '".code($v['cpnum'])."','".code($v['cpidpnum'])."','".code($v['cpvotenum'])."','".code($v['cpname'])."','".code($v['cpproject'])."',$cpstartdate,$cpenddate,$cpstartactual,$cpendactual,'Y',0)";
				include("inc_db_con.php");
					$cpid = mysql_insert_id($con);
					if(!checkIntRef($cpid)) {
						$result[0] = "error";
						$result[1] = "An error occurred and the Capital Project could not be created.  Please try again.";
					} else {
						logChange($cpid,'','Created Capital Project '.$cpid,'N',$sql);
						//CREATE FUND SOURCE RECORDS
						$lsql = array();
						foreach($fundsource as $fs) {
							$sql = "INSERT INTO ".$dbref."_capital_source (csid, cscpid, cssrcid, csyn) VALUES (null,$cpid,".$fs['id'].",'Y')";
							include("inc_db_con.php");
							$lsql[] = $sql;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Fund Source records for new Capital Project '.$cpid,'N',$lsql);
						//CREATE WARDS RECORDS
						$lsql = array();
						foreach($wds as $fs) {
							$sql = "INSERT INTO ".$dbref."_capital_wards (cwid, cwcpid, cwwardsid, cwyn) VALUES (null,$cpid,".$fs['id'].",'Y')";
							include("inc_db_con.php");
							$lsql[] = $sql;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Ward records for new Capital Project '.$cpid,'N',$lsql);
						//CREATE AREA RECORDS
						$lsql = array();
						foreach($ara as $fs) {
							$sql = "INSERT INTO ".$dbref."_capital_area (caid, cacpid, caareaid, cayn) VALUES (null,$cpid,".$fs['id'].",'Y')";
							include("inc_db_con.php");
							$lsql[] = $sql;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Area records for new Capital Project '.$cpid,'N',$lsql);
						//CREATE BUDGET RECORDS
						$lsql = array();
						$budget = $v['budget'];
						$t = 1;
						foreach($budget as $fs) {
							if(is_numeric($fs) && strlen($fs)>0) { $val = $fs; } else { $val = 0; }
							$sql = "INSERT INTO ".$dbref."_capital_results (crid, crcpid, crbudget, crtimeid) VALUES (null,$cpid,$val,$t)";
							include("inc_db_con.php");
							$lsql[] = $sql;
							$t++;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Results records for new Capital Project '.$cpid,'N',$lsql);
						//CREATE FORECAST RECORDS
						$lsql = array();
						$crr = $v['fore1'];
						$other = $v['fore2'];
						$t = 0;
						foreach($foreyears as $fs) {
							$cfcrr = $crr[$t];
							if(is_numeric($cfcrr) && strlen($cfcrr)>0) { $val1 = $cfcrr; } else { $val1 = 0; }
							$cfother = $other[$t];
							if(is_numeric($cfother) && strlen($cfother)>0) { $val2 = $cfother; } else { $val2 = 0; }
							$sql = "INSERT INTO ".$dbref."_capital_forecast (cfid, cfcpid, cfcrr, cfother, cfyearid) VALUES (null,$cpid,$val1,$val2,".$fs['id'].")";
							include("inc_db_con.php");
							$lsql[] = $sql;
							$t++;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Forecast records for new Capital Project '.$cpid,'N',$lsql);
						
						$result[0] = "check";
						$result[1] = "Capital Project created successfully.";
					}
			} else {
				$result[0] = "error";
				$result[1] = $errtxt;
			}
			break;
		case "edit":
			$title = "Edit";
			$nextact = "editsave";
			$cpid = $v['c'];
			if(!checkIntRef($cpid)) {
				$result[0] = "error";
				$result[1] = "Invalid Capital Project reference.  Please go back and try again.";
			} else {
				$sql = "SELECT * FROM ".$dbref."_capital WHERE cpid = $cpid";
				include("inc_db_con.php");
					$cp = mysql_fetch_array($rs);
				mysql_close($con);
				$v['subid'] = $cp['cpsubid'];
				$v['gfs'] = $cp['cpgfsid'];
				$v['cpname'] = decode($cp['cpname']);
				$v['cpproject'] = decode($cp['cpproject']);
				$v['cpnum'] = decode($cp['cpnum']);
				$v['cpidpnum'] = decode($cp['cpidpnum']);
				$v['cpvotenum'] = decode($cp['cpvotenum']);
				if($cp['cpstartdate']>0) { $v['cpstartdate']=date("d-m-Y",$cp['cpstartdate']); }
				if($cp['cpenddate']>0) { $v['cpenddate']=date("d-m-Y",$cp['cpenddate']); }
				if($cp['cpstartactual']>0) { $v['cpstartactual']=date("d-m-Y",$cp['cpstartactual']); }
				if($cp['cpendactual']>0) { $v['cpendactual']=date("d-m-Y",$cp['cpendactual']); }
				$sql = "SELECT DISTINCT cssrcid FROM ".$dbref."_capital_source WHERE csyn = 'Y' and cscpid = $cpid ";
				include("inc_db_con.php");
					while($row = mysql_fetch_array($rs))
					{
						$fundsource[$row['cssrcid']]['yn'] = "Y";
					}
				mysql_close($con);
				$sql = "SELECT DISTINCT cwwardsid FROM ".$dbref."_capital_wards WHERE cwyn = 'Y' and cwcpid = $cpid ";
				include("inc_db_con.php");
					while($row = mysql_fetch_array($rs))
					{
						$wds[$row['cwwardsid']]['yn'] = "Y";
					}
				mysql_close($con);
				$sql = "SELECT DISTINCT caareaid FROM ".$dbref."_capital_area WHERE cayn = 'Y' and cacpid = $cpid ";
				include("inc_db_con.php");
					while($row = mysql_fetch_array($rs))
					{
						$ara[$row['caareaid']]['yn'] = "Y";
					}
				mysql_close($con);
			}
			break;
		case "editsave":
			$title = "Edit";
			$nextact = "editsave";
			$cpid = $v['cpid'];
			if(!checkIntRef($cpid)) {
				$result[0] = "error";
				$result[1] = "Invalid Capital Project reference.  Please <a href=admin_fin_cp_setup.php>go back</a> and try again.";
			} else {
			//GET DATA
			$subid = $v['subid'];
			$gfsid = $v['gfs'];
			$fsr = $v['fundsource'];
			$fundsource = array();
			foreach($fsr as $f) { $fundsource[$f] = array('id'=>$f,'yn'=>"Y"); }
			$ws = $v['wards'];
			$wds = array();
			foreach($ws as $w) { $wds[$w] = array('id'=>$w,'yn'=>"Y"); }
			if($wds[1]['yn']=="Y") { $wds = array(); $wds[1] = array('id'=>1,'yn'=>"Y"); } //IF ALL[1] WARD SELECTED REMOVE ALL OTHER SELECTED
			$aa = $v['area'];
			$ara = array();
			foreach($aa as $a) { $ara[$a] = array('id'=>$a,'yn'=>"Y"); }
			if($ara[1]['yn']=="Y") { $ara = array(); $ara[1] = array('id'=>1,'yn'=>"Y"); } //IF ALL[1] AREA SELECTED REMOVE ALL OTHER SELECTED
			//VALIDATE REQUIRED FIELDS
			$err = "N"; $errtxt = "Please complete all required fields:";
			if(!checkIntRef($subid)) { $err = "Y"; $errtxt.="<br />Sub-Directorate"; }
			foreach($head as $hd) {
				if($hd['headmaxlen']==0) {
					switch($hd['headfield'])
					{
						case "area":
							if(count($ara)==0)  { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; }
							break;
						case "fundsource":
							if(count($fundsource)==0)  { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; }
							break;
						case "gfs":
							if(!checkIntRef($gfsid)) { $err = "Y"; $errtxt.="<br />GFS Classification"; }
							break;
						case "wards":
							if(count($wds)==0)  { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; }
							break;
					}
				} else {
					if(strlen($v[$hd['headfield']])==0 && $hd['headrequired']=="Y") { $err = "Y"; $errtxt.="<br />".$hd['headdetail']; } 
				}
			}
			if($err == "N") {
				$start1 = $v['cpstartdate'];
				$start2 = $v['cpstartactual'];
				$end1 = $v['cpenddate'];
				$end2 = $v['cpendactual'];
				if(strlen($start1)==0) { 
					$cpstartdate = 0; 
				} else {
					$st = strFn("explode",$start1,"-","");
					$cpstartdate = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				if(strlen($start2)==0) { 
					$cpstartactual = 0; 
				} else {
					$st = strFn("explode",$start2,"-","");
					$cpstartactual = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				if(strlen($end1)==0) { 
					$cpenddate = 0; 
				} else {
					$st = strFn("explode",$end1,"-","");
					$cpenddate = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				if(strlen($end2)==0) { 
					$cpendactual = 0; 
				} else {
					$st = strFn("explode",$end2,"-","");
					$cpendactual = mktime(12,0,0,$st[1],$st[0],$st[2]);
				}
				//CREATE CAPITAL PROJECT IN DBREF_CAPITAL
				$sql = "UPDATE ".$dbref."_capital SET ";
				$sql.= "cpsubid = $subid";
				$sql.= ", cpgfsid = $gfsid";
				$sql.= ", cpnum = '".code($v['cpnum'])."'";
				$sql.= ", cpidpnum = '".code($v['cpidpnum'])."'";
				$sql.= ", cpvotenum = '".code($v['cpvotenum'])."'";
				$sql.= ", cpname = '".code($v['cpname'])."'";
				$sql.= ", cpproject = '".code($v['cpproject'])."'";
				$sql.= ", cpstartdate = $cpstartdate";
				$sql.= ", cpenddate = $cpenddate";
				$sql.= ", cpstartactual = $cpstartactual";
				$sql.= ", cpendactual = $cpendactual";
				$sql.= " WHERE cpid = $cpid ";
				include("inc_db_con.php");
					$mar = mysql_affected_rows($con);
					if($mar > 1) {
						$result[0] = "error";
						$result[1] = "An unexpected error occurred. $mar Capital Project were updated.";
					} else {
						logChange($cpid,'','Updated Capital Project '.$cpid,'N',$sql);
						//CREATE FUND SOURCE RECORDS
						$lsql = array();
						$sql = "UPDATE ".$dbref."_capital_source SET csyn = 'N' WHERE cscpid = $cpid";
						include("inc_db_con.php");
							logChange($cpid,'','Removed old Fund Source records for updated CP $cpid','N',$sql);
						foreach($fundsource as $fs) {
							$sql = "INSERT INTO ".$dbref."_capital_source (csid, cscpid, cssrcid, csyn) VALUES (null,$cpid,".$fs['id'].",'Y')";
							include("inc_db_con.php");
							$lsql[] = $sql;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created new Fund Source records for updated Capital Project '.$cpid,'N',$lsql);
						//CREATE WARDS RECORDS
						$sql = "UPDATE ".$dbref."_capital_wards SET cwyn = 'N' WHERE cwcpid = $cpid";
						include("inc_db_con.php");
							logChange($cpid,'','Removed old Wards records for updated CP $cpid','N',$sql);
						$lsql = array();
						foreach($wds as $fs) {
							$sql = "INSERT INTO ".$dbref."_capital_wards (cwid, cwcpid, cwwardsid, cwyn) VALUES (null,$cpid,".$fs['id'].",'Y')";
							include("inc_db_con.php");
							$lsql[] = $sql;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Ward records for new Capital Project '.$cpid,'N',$lsql);
						//CREATE AREA RECORDS
						$sql = "UPDATE ".$dbref."_capital_area SET cayn = 'N' WHERE cacpid = $cpid";
						include("inc_db_con.php");
							logChange($cpid,'','Removed old Area records for updated CP $cpid','N',$sql);
						$lsql = array();
						foreach($ara as $fs) {
							$sql = "INSERT INTO ".$dbref."_capital_area (caid, cacpid, caareaid, cayn) VALUES (null,$cpid,".$fs['id'].",'Y')";
							include("inc_db_con.php");
							$lsql[] = $sql;
						}
						$lsql = strFn("implode",$lsql," | ","");
						logChange($cpid,'','Created Area records for new Capital Project '.$cpid,'N',$lsql);
						$result[0] = "check";
						$result[1] = "Capital Project updated successfully.";
					}
			} else {
				$result[0] = "error";
				$result[1] = $errtxt;
			}
			}
			break;
		default:
			$title = "Add";
			$nextact = "addsave";
			$act = "add";
			break;
	}
?>
<html>
	<head>
		<meta http-equiv="Content-Language" content="en-za">
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<title>www.Ignite4u.co.za</title>
	</head>
	<link rel="stylesheet" href="/default.css" type="text/css">
	<?php include("inc_style.php"); ?>
	<style type=text/css>
		table th {	
			text-align: left;
		}
		table td {
			border: 1px solid #ababab;
		}
		.comm {
			font-style: italic;
			font-size: 6.5pt;
			color: #777777;
		}
	</style>
	<link type="text/css" href="lib/jquery-ui-1.7.1.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="lib/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="lib/jquery-ui-1.7.1.custom.min.js"></script>
	<script type=text/javascript>
		$(document).ready(function(){
	            $(".date-input").datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                });
		})
	</script>
	<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
echo "<h1>SDBIP $modtxt: Admin - Capital Projects ~ $title</h1>";
displayResult($result); 
if($result[0]=="check") {
	if($act == "addsave" || $nextact == "addsave") {
		echo "<p style=\"margin-top:20px\"><input type=button value=\"Add Another Capital Project\" onclick=\"document.location.href='admin_fin_cp.php?act=add';\"></p>";
	}
	$urlback = "admin_fin_cp_setup.php";
	include("inc_goback.php");
} else {
?>	
<p>All fields marked with a * are required.</p>
<form id=addcp name=addcp method=POST action=admin_fin_cp.php language=jscript>
<?php echo "<input type=hidden name=action value=$nextact><input type=hidden name=cpid value=$cpid>"; ?>
<h2>Capital Project Details</h2>
<table cellpadding=3 cellspacing=0 width=600>
<?php
if($nextact == "editsave") {
	echo "<tr>";
		echo "<th>Reference</th>";
		echo "<td style=\"color:#cc0001;font-weight:bold;\">$cpid </td>";
	echo "</tr>";
}
?>
	<tr>
		<th>Sub-Directorate:*</th>
		<td><select name=subid><?php
			echo "<option ";
			if(!checkIntRef($v['subid'])) { echo " selected"; }
			echo " value=X>--- SELECT ---</option>";
            $sql = "SELECT s.*, d.dirtxt FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_dir d WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND s.subdirid = d.dirid ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['subid'];
                    $val = $row['dirtxt']." - ".$row['subtxt'];
                    echo"<option "; if($v['subid'] == $id) { echo " selected "; } echo "value=".$id.">".$val."</option>";
                }
            mysql_close($con);
        ?></select></td>
	</tr>
<?php
	foreach($head as $hd) {
		$hfield = $hd['headfield'];
		if($hfield!="progress") {
			echo "<tr>";
				echo "<th width=$width >".$hd['headdetail'].":";
				if($hd['headrequired']=="Y") { echo "*"; }
				echo "</th>";
				echo "<td>";
				switch($hfield)
				{
					case "gfs":
						echo "<select name=$hfield ><option value=X "; if(!checkIntRef($v['gfs'])) { echo " selected "; } echo ">--- SELECT ---</option>";
						foreach($gfs as $g) {
							echo "<option value=".$g['id'];	if($v['gfs']==$g['id']) { echo " selected "; }	echo ">".$g['value']."</option>";
						}
						echo "</select>";
						break;
					case "fundsource":
						foreach($fundsrc as $f) {
							echo "<input type=checkbox ";
							if($fundsource[$f['id']]['yn'] == "Y") { echo "checked "; }
							echo "name=fundsource[] value=".$f['id']."> ".$f['value']."<br />";
						}
						break;
					case "wards":
						foreach($wards as $w) {
							echo "<input type=checkbox name=wards[] value=".$w['id'];
							if(($w['value']=="All" && count($wds)==0) || $wds[$w['id']]['yn']=="Y") { echo " checked "; }
							echo "> ";
							if($w['value']!="All" && $w['value']!="N/A") { echo $w['code'].": "; }
							echo $w['value']."<br />";
						}
						break;
					case "area":
						foreach($area as $w) {
							echo "<input type=checkbox name=area[] value=".$w['id'];
							if(($w['value']=="All" && count($ara)==0) || $ara[$w['id']]['yn']=="Y") { echo " checked "; }
							echo "> ";
							echo $w['value']."<br />";
						}
						break;
					default:
						$maxlen = $hd['headmaxlen'];
						if($maxlen > 0) {
							if($maxlen > 50) { $size=50; } else { $size=20; }
							echo "<input type=text name=$hfield size=$size maxlength=$maxlen value=\"".$v[$hd['headfield']]."\"> <span class=comm>(Maximum ".$hd['headmaxlen']." characters)</span>";
						} else {
							echo "<input type=datepicker readonly=readonly name=$hfield class=date-input size=15 value=\"".$v[$hd['headfield']]."\"> <span class=comm>(dd-mm-yyyy)</span>";
						}
						break;
				}
				echo "</td>";
			echo "</tr>";
		}
	}
?>
</table>
<?php if($act == "add" || $act == "addsave" || $nextact == "addsave" ) { ?>
<h2>Capital Project Budget for <?php echo $modtxt; ?></h2>
<table cellpadding=3 cellspacing=0 width=600>
	<?php
	$budget = $v['budget'];
	$t = 0;
	foreach($time as $te) {
		if(is_numeric($budget[$t]) && strlen($budget[$t])>0) { $val = $budget[$t]; } else { $val = 0; }
		echo "<tr>";
			echo "<th width=$width >".date("M Y",$te['eval']).":</th>";
			echo "<td> R <input type=text value=\"$val\" style=\"text-align:right;\" name=budget[] > <span class=comm>(numbers only)</span></td>";
		echo "</tr>";
		$t++;
	}
	?>
</table>
<h2>Forecast Budget</h2>
<table cellpadding=3 cellspacing=0 width=600>
	<?php
	$budget1 = $v['fore1'];
	$budget2 = $v['fore2'];
	$t = 0;
	foreach($foreyears as $fy) {
		if(is_numeric($budget1[$t]) && strlen($budget1[$t])>0) { $val1 = $budget1[$t]; } else { $val1 = 0; }
		if(is_numeric($budget2[$t]) && strlen($budget2[$t])>0) { $val2 = $budget2[$t]; } else { $val2 = 0; }
		echo "<tr>";
			echo "<th rowspan=2 width=$width >".$fy['value'].": </th>";
			echo "<th width=$width >".$headf['cfcrr']['headdetail'].": </th>";
			echo "<td> R <input type=text value=\"$val1\" style=\"text-align:right;\" name=fore1[]> <span class=comm>(numbers only)</span></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<th>".$headf['cfother']['headdetail'].": </th>";
			echo "<td> R <input type=text value=\"$val2\" style=\"text-align:right;\" name=fore2[]> <span class=comm>(numbers only)</span></td>";
		echo "</tr>";
		$t++;
	}
	?>
</table>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=600>
    <tr>
        <td style="text-align:center;">
            <input type=submit value=" Add "> <input type=reset ></td>
    </tr>
</table>
<?php } else { ?>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=600>
    <tr>
        <td style="text-align:center;">
            <input type=submit value="Save Changes"> <input type=reset ></td>
    </tr>
</table>
<?php } ?>
</form>
<?php
}	//if result <> check
?>	
</body>
</html>