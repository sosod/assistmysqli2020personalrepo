<?php
    include("inc_ignite.php");

    $plc4 = $_POST['plc4'];
    $plc5 = $_POST['plc5'];
    if(strlen($plc4)==1 && strlen($plc5)==1)
    {
        if($plc4 != "N") { $plc4 = "Y"; }
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$plc4."' WHERE ref = '".strtoupper($modref)."' AND refid = 4";
        include("inc_db_con.php");
        if($plc5 != "N") { $plc5 = "Y"; }
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$plc5."' WHERE ref = '".strtoupper($modref)."' AND refid = 5";
        include("inc_db_con.php");
    }


    
    include("inc_admin.php");
    $rowspan=1;
    if($plcsetup[4]=="Y") { $rowspan=2; }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function editPhase(i) {
    document.getElementById('ref').value = i;
    document.forms['edit'].submit();
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Project Life Cycle</b></h1>
<h2 class=fc>Setup Procurement Phases</h2>
<form name=setup method=post action=setup_plc_proc.php>
<table cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdgeneral width=300>&nbsp;Use multiple project categories for procurement?</td>
        <td class=tdgeneral><select name=plc4><option <?php if($plcsetup[4]!="N") { echo("selected"); } ?> value=Y>Yes</option><option <?php if($plcsetup[4]=="N") { echo("selected"); } ?> value=N>No</option></select>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral>&nbsp;Allow users to override procurement days?</td>
        <td class=tdgeneral><select name=plc5><option <?php if($plcsetup[5]!="N") { echo("selected"); } ?> value=Y>Yes</option><option <?php if($plcsetup[5]=="N") { echo("selected"); } ?> value=N>No</option></select>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=2>&nbsp;<input type=submit value="Update"> <input type=reset value="Reset"></td>
    </tr>
</table>
</form>
<p><input type=button value=" Add New " onclick="document.location.href = 'setup_plc_proc_add.php';"> <input type=button value="Display Order" onclick="document.location.href = 'setup_plc_proc_order.php';"></p>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td width=30 rowspan=<?php echo($rowspan); ?> class=tdheader height=27>Ref</td>
		<td rowspan=<?php echo($rowspan); ?> class=tdheader>Phase</td>
		<td width=60 rowspan=<?php echo($rowspan); ?> class=tdheader>Required?</td>
		<?php if($plcsetup[4]=="Y") {?>
		<td colspan=4 class=tdheader>Project Categories (Calendar Days)</td>
		<?php } else { ?>
		<td width=25 class=tdheader>Days</td>
		<?php } ?>
		<td width=50 rowspan=<?php echo($rowspan); ?> class=tdheader>&nbsp;</td>
	</tr>
<?php if($plcsetup[4]=="Y") {?>
    <tr>
		<td class=tdheader>1</td>
		<td class=tdheader>2</td>
		<td class=tdheader>3</td>
		<td class=tdheader>4</td>
    </tr>
<?php } ?>
	<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_proc WHERE (yn = 'Y' OR yn = 'R') ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $id = $row['id'];
    ?>
	<?php include("inc_tr.php");?>
		<td class=tdgeneral align=center height=27><b><?php echo($id); ?></b></td>
		<td class=tdgeneral><?php echo($row['value']); ?></td>
		<td class=tdgeneral align=center><?php if($row['yn']=="R") { echo("Yes"); } else { echo("No"); } ?></td>
<?php if($plcsetup[4]=="Y") { ?>
		<td class=tdgeneral align=center><?php echo($row['days1']); ?></td>
		<td class=tdgeneral align=center><?php echo($row['days2']); ?></td>
		<td class=tdgeneral align=center><?php echo($row['days3']); ?></td>
		<td class=tdgeneral align=center><?php echo($row['days4']); ?></td>
<?php } else { ?>
		<td class=tdgeneral align=center><?php echo($row['days1']); ?></td>
<?php } ?>
		<td class=tdgeneral align=center><?php if(is_numeric($id)) { ?><input type=button value=Edit onclick="editPhase(<?php echo($id); ?>)"><?php } else { echo("&nbsp;"); } ?></td>
	</tr>
    <?php
    }
mysql_close();
?>
</table>
<p><input type=button value=" Add New " onclick="document.location.href = 'setup_plc_proc_add.php';"> <input type=button value="Display Order" onclick="document.location.href = 'setup_plc_proc_order.php';"></p>
<form name=edit method=post action=setup_plc_proc_edit.php>
<input type=hidden name=id id=ref>
</form>
<?php
$urlback = "setup_plc.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
