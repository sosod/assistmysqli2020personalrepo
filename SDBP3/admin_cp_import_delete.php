<?php
    include("inc_ignite.php");
$modref = strtoupper($modref);
	
	$action = $_REQUEST['type'];
	
	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Amend Capital Projects</b></h1>
<?php
switch($action) {
	case "fin":
		?>
		<p>Deleting monthly cashflow...</p>
		<?php
		$tables = array("capital","capital_area","capital_forecast","capital_results","capital_source","capital_wards");
		foreach($tables as $tbl) {
			//BACKUP
			$data = "";
			$adata = array();
			//get lineitems field headings
			$sql = "DESCRIBE ".$dbref."_".$tbl;
			include("inc_db_con.php");
			$rdata = array();
			while($row = mysql_fetch_array($rs)) {
				$rdata[] = $row['Field'];
			}
			mysql_close($con);
			$adata[0] = $rdata;
			//get lineitems values
			$sql = "SELECT * FROM ".$dbref."_".$tbl;
			include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				$adata[] = $row;
			}
			mysql_close($con);
			//write lineitems to csv
			$bdata = array();
			foreach($adata as $ad) {
				$bdata[] = "\"".implode("\",\"",$ad)."\"";
			}
			$data = implode("\n",$bdata);
				//CHECK EXISTANCE OF STORAGE LOCATION
				$chkloc = $modref."/import";
				checkFolder($chkloc);
				//WRITE DATA TO FILE
				$filename = "../files/".$cmpcode."/".$chkloc."/CP_".date("YmdHis")."_".$tbl."_BACKUP.csv";
				$file = fopen($filename,"w");
				fwrite($file,$data."\n");
				fclose($file);
			//delete
			$sql = "DELETE FROM ".$dbref."_".$tbl;
			include("inc_db_con.php");
		}
		//log
		$sql = "INSERT INTO assist_".$cmpcode."_log (date, tkid, ref, transaction) VALUES ($today,'$tkid','$modref','Deleted all capital projects via admin_cp_import.php')";
		include("inc_db_con.php");
		$res = "Capital projects successfully deleted";
		break;
	case "link":
		$data = "";
		$data = "kpiid,kpicpid";
		$sql = "SELECT kpiid, kpicpid FROM ".$dbref."_kpi WHERE kpicpid > 0";
		include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				$data.= "\n".$row['kpiid'].",".$row['kpicpid'];
			}
		mysql_close($con);
			//CHECK EXISTANCE OF STORAGE LOCATION
			$chkloc = $modref."/import";
			checkFolder($chkloc);
			//WRITE DATA TO FILE
			$filename = "../files/".$cmpcode."/".$chkloc."/CP_".date("YmdHis")."_link_BACKUP.csv";
			$file = fopen($filename,"w");
			fwrite($file,$data."\n");
			fclose($file);
		$sql = "UPDATE ".$dbref."_kpi SET kpicpid = 0";
		include("inc_db_con.php");
		$sql = "INSERT INTO assist_".$cmpcode."_log (date, tkid, ref, transaction) VALUES ($today,'$tkid','$modref','Deleted all capital projects and capital KPI links via admin_cp_import.php')";
		include("inc_db_con.php");
		$res = "Capital project links to KPIs successfully deleted";
		break;
	default:
		break;
}



?>
<script type=text/javascript>
	document.location.href = 'admin_cp_import.php?r[]=check&r[]=<?php echo str_replace(' ','+',$res); ?>';
</script>
<p>&nbsp;</p>
</body>


</html>
