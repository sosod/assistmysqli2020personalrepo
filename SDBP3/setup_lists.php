<?php
    include("inc_ignite.php");
    
	$munlists['idpgoal']= array('name'=>"IDP Objective",'id'=>"idpgoal",'code'=>"N",'len'=>150,'fld'=>"code");


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Lists</b></h1>

<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
    <?php foreach($munlists as $l) { 
	if($l['id']=="prog") { $comm = "Departmental KPIs"; } elseif($l['id']=="idpgoal") { $comm = "Top Level KPIs"; } else { $comm = ""; }
	?>
	<tr>
		<td width=120 class=tdheaderl><?php echo($l['name']); ?></td>
		<td class=tdgeneral><?php echo $comm; ?></td>
		<td width=100 class=tdgeneral align=center><input type=button value=Configure onclick="document.location.href = 'setup_lists_config.php?l=<?php echo($l['id']); ?>';"></td>
	</tr>
	<?php
    }
    ?>
</table>



<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
