<?php
    include("inc_ignite.php");
    $targets = array();
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<?php include("inc_head_msie.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: View KPI</b></h1>
<?php
$kpiid = $_POST['kpiid'];
$plcid = $_POST['plcid'];
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];


$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiid = ".$kpiid;
include("inc_db_con.php");
    $kpirow = mysql_fetch_array($rs);
mysql_close();
$sql = "SELECT dirtxt, subtxt FROM assist_".$cmpcode."_".$modref."_dir, assist_".$cmpcode."_".$modref."_dirsub WHERE ";
$sql.= "subdirid = dirid";
$sql.= " AND subid = ".$kpirow['kpisubid'];
include("inc_db_con.php");
    $dirrow = mysql_fetch_array($rs);
mysql_close();
?>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc WHERE plcid = ".$plcid;
include("inc_db_con.php");
    $plcrow = mysql_fetch_array($rs);
mysql_close();
?>
<h2 class=fc>Project Life Cycle</h2>
<table border="0" cellspacing="0" cellpadding="3" style="border: 0px solid #ffffff;">
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Name:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($plcrow['plcname']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Description:&nbsp;</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($plcrow['plcdescription']); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Start Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo(date("d M Y",$plcrow['plcstartdate'])); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>End Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo(date("d M Y",$plcrow['plcenddate'])); ?></td>
	</tr>
</table>
<p style="margin: 0 0 0 0;">&nbsp;</p>
<table border="1" cellspacing="0" cellpadding="3" width=100%>
	<tr>
		<td height=27 colspan="2" class="tdheader"><b>Project Phase</b></td>
		<td width=100 class="tdheader"><b>Calendar<br>Days</b></td>
		<td class="tdheader"><b>Start<br>Date</b></td>
		<td class="tdheader"><b>End<br>Date</b></td>
		<td class="tdheader"><b>% Target<br>of Project</b></td>
		<td class="tdheader"><b>Completed?</b></td>
		<td class="tdheader"><b>Date of<br>Completion</b></td>
	</tr>
<?php
$tartot = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            switch($row['type'])
            {
        case "A":
//CUSTOM PHASES
        $sql2 = "SELECT p.*, c.value FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p, assist_".$cmpcode."_".$modref."_list_plcphases_custom c ";
        $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'C' AND p.ppphaseid = c.id ORDER BY p.ppsort";
        include("inc_db_con2.php");
            $mnr2 = mysql_num_rows($rs2);
            if($mnr2 > 0)
            {
            $a=0;
                while($row2 = mysql_fetch_array($rs2))
                {
                    $tartot = $tartot+$row2['pptarget'];
                    $a++;
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
	<?php if($a==1) { ?>		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td><?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdone); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                }
            }
        mysql_close($con2);
        break;
            case "P":
            //PROCUREMENT PHASES
                $sql2 = "SELECT p.*, c.value FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p, assist_".$cmpcode."_".$modref."_list_plcphases_proc c ";
                $sql2.= "WHERE p.ppplcid = ".$plcid." AND p.pptype = 'P' AND p.ppphaseid = c.id ORDER BY p.ppsort";
                include("inc_db_con2.php");
                $mnr2 = mysql_num_rows($rs2);
                $r=0;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $r++;
                        $tartot = $tartot+$row2['pptarget'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
        <?php if($r==1) { ?>
		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
        <?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdone); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                    }
                mysql_close($con2);
            default:
            //STANDARD PHASES WHERE TYPE = S
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE pptype = 'S' AND ppphaseid = ".$row['id']." AND ppplcid = ".$plcid;
                include("inc_db_con2.php");
                    $mnr2 = mysql_num_rows($rs2);
                    if($mnr2 > 0)
                    {
                        $row2 = mysql_fetch_array($rs2);
                    }
                mysql_close($con2);
                if($mnr2>0)
                {
                    $tartot = $tartot+$row2['pptarget'];
                    if($row2['ppdone']=="Y")
                    {
                        $ppdone = "<img src=/pics/tick_sml.gif>";
                        if($row2['ppdonedate']>0)
                        {
                            $ppdonedate = date("d M Y",$row2['ppdonedate']);
                        } else {
                            $ppdonedate = "&nbsp;";
                        }
                    } else {
                        $ppdone = "&nbsp;";
                        $ppdonedate = "&nbsp;";
                    }
?>
	<tr>
		<td colspan="2" class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
		<td class="tdgeneral" align="center"><?php echo($row2['ppdays']); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppstartdate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo(date("d M Y",$row2['ppenddate'])); ?></td>
		<td class="tdgeneral" align="center"><?php echo($row2['pptarget']." %"); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdone); ?></td>
		<td class="tdgeneral" align="center"><?php echo($ppdonedate); ?></td>
	</tr>
<?php
                }
                break;

            }
        }
    mysql_close();

?>
	<tr>
		<td height=27 colspan="2" class="tdgeneral"><b>Target % of Project Total:</b></td>
		<td class="tdgeneral" align="center" colspan=3></td>
		<td class="tdgeneral" align="center"><b><?php echo($tartot); ?> %</b></td>
	</tr>
</table>
<?php include("inc_goback_history.php"); ?>
</body>

</html>
