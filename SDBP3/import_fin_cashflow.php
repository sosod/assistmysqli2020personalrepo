<?php
    include("inc_ignite.php");
	
$act = $_REQUEST['act'];
$fileloc = $_REQUEST['fl'];

$time = array(
	'July'=>1,
	'August'=>2,
	'September'=>3,
	'October'=>4,
	'November'=>5,
	'December'=>6,
	'January'=>7,
	'February'=>8,
	'March'=>9,
	'April'=>10,
	'May'=>11,
	'June'=>12
);

$sql = "SELECT * FROM ".$dbref."_dir";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$dir[code($row['dirtxt'])] = $row;
	}
mysql_close($con);
$sql = "SELECT * FROM ".$dbref."_dirsub";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$sub[code($row['subtxt'])] = $row;
	}
mysql_close($con);
//echo "<P>"; print_r($dir);
//echo "<P>"; print_r($sub);

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import - Finance ~ Cashflow</b></h1>
<?php
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                die("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                die("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                die("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            die("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    die("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}

if($err != "Y")
{
            $file = fopen($fileloc,"r");
            $data = array();
	set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
//				echo "<P>".count($tmpdata)." - "; print_r($tmpdata);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
	set_time_limit(30);
} else {
	die("<P>Erryn = 'Y'"); 
}

if(count($data)>0)
{
	//print_r($data);
	echo "<form action=import_fin_cashflow.php method=post><input type=hidden name=fl value=".$fileloc."><input type=hidden name=act value=sbmt>";
	echo "<table cellpadding=3>";
		$lc = 0;
		for($a=0;$a<count($data);$a++) {
			$dt = $data[$a];
			if($dt[0]=="Directorate" || $dt[0] == "Dirid") {
				echo "<tr><td width=30>&nbsp;</td>";
				foreach($dt as $d) {
					echo "<th>".$d."</th>";
				}
				echo "</tr>";
			} else {
				$month = $time[$dt[5]];
				if($act=="sbmt" && checkIntRef($month)) {
					$sub = $dt[1];
					$line = code($dt[2]);
					$sql = "SELECT lineid FROM ".$dbref."_finance_lineitems WHERE linesubid = $sub AND linevalue = '$line'";
					include("inc_db_con.php");
						$lmnr = mysql_num_rows($rs);
						if($lmnr>0) { $lrow = mysql_fetch_array($rs); }
					mysql_close($con);
					if($lmnr>0) {
						$lineid = $lrow['lineid'];
					} else {
						$lc++;
						$sql = "INSERT INTO ".$dbref."_finance_lineitems (linevalue,lineyn,linesort,linetype,linesubid,linegfsid,linevote) VALUES ";
						$sql.= "('$line','Y',$lc,'CF',$sub,".$dt[3].",'".code($dt[4])."')";
						include("inc_db_con.php");
							$lineid = mysql_insert_id($con);
					}
					$rev = $dt[6]; if(strlen($rev)==0 || !is_numeric($rev)) { $rev = 0; }
					$op = $dt[7]; if(strlen($op)==0 || !is_numeric($op)) { $op = 0; }
					$cp = $dt[8]; if(strlen($cp)==0 || !is_numeric($cp)) { $cp = 0; }
					$sql = "INSERT INTO ".$dbref."_finance_cashflow (cflineid,cfyn,cftimeid,cfrev1,cfop1,cfcp1) VALUES ";
					$sql.= "($lineid,'Y',$month,$rev,$op,$cp)";
					include("inc_db_con.php");
						$class = "tdheadergreen";
				} else {
					if(!checkIntRef($month)) {
						$class = "tdheaderred"; 
					} else {
						$dt[5] = $month;
						$class = "tdheaderblue"; 
					}
				}
				echo "<tr>";
				echo "<td class=$class >&nbsp;</td>";
				foreach($dt as $d) {
					echo "<td>".$d."</td>";
				}
				echo "</tr>";
			}
		}
	echo "</table>";
	if($act != "sbmt") {
		echo "<p><input type=submit></p>";
	}
	echo "</form>";
} else {
	echo "<P>NO DATA";
}

$urlback = "import.php";
include("inc_goback.php");

?>

</body>

</html>
