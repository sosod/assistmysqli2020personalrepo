<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>

<base target="main">
<script type=text/javascript>
function Validate(me) {
    var dirtxt = me.dirtxt.value;
    var pos = me.pos.value;
    var poslist = me.poslist.value;
    var valid8 = "false";
    if(dirtxt.length == 0)
    {
        alert("Please enter the name of the Directorate.");
    }
    else
    {
        if(pos == "a" || pos == "b")
        {
            if(poslist == "X")
            {
                alert("Please complete the Display Position.");
            }
            else
            {
                valid8 = "true";
            }
        }
        else
        {
            valid8 = "true";
        }
    }
    if(valid8=="true")
        return true;
    else
        return false;
}

function chgPos(me) {
        if(me.value == "f" || me.value == "l")
        {
            document.getElementById('poslist').disabled = true;
        }
        else
        {
            document.getElementById('poslist').disabled = false;
        }
}
    
function delSub(s,stxt,d,src) {
    var err = "N";
    if(!isNaN(parseInt(s)) && escape(s) == s)
    {
        s = parseInt(s);
        if(s>0)
        {
            if(confirm("Are you sure you wish to delete sub-directorate '"+stxt+"'?\nAny KPIs and Capital Projects associated with this sub-directorate will also be removed.\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
            {
                document.location.href = "setup_dir_sub_del.php?s="+s+"&d="+d+"&r="+src;
            }
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }

    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}

function delDir(dtxt,d) {
    var err = "N";
    if(!isNaN(parseInt(d)) && escape(d) == d)
    {
        d = parseInt(d);
        if(d>0)
        {
            if(confirm("Are you sure you wish to delete directorate '"+dtxt+"'?\nAll Sub-Directorates, KPIs and Capital Projects associated with this directorate will also be removed.\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
            {
                document.location.href = "setup_dir_del.php?d="+d;
            }
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }

    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}

</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Edit Directorate</b></h1>
<p>&nbsp;</p>
<form name=edit method=post action=setup_dir_edit_process.php onsubmit="return Validate(this);">
<?php
    $dirid = $_GET['d'];
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dirid." AND diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)>0)
        {
            $dir = mysql_fetch_array($rs);
            $err = "N";
        }
        else
        {
            $dir = array();
            $err = "Y";
        }
    mysql_close();

    if($err == "N")
    {
?>
<input type=hidden name=dirid value=<?php echo($dirid); ?>>
<table cellpadding=3 cellspacing=0 width=570 id="table1">
	<tr>
		<td class=tdheaderl width=130>Name:</td>
		<td class=TDgeneral><input type=text name=dirtxt size="50" value="<?php echo(html_entity_decode($dir['dirtxt'],ENT_QUOTES,"ISO-8859-1")); ?>">&nbsp;</td>
		<td class=TDgeneral width=110><small>Max 50 characters</small></td>
	</tr>
	<?php
	$pos = "l";
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirsort < ".$dir['dirsort']." AND diryn = 'Y' ORDER BY dirsort DESC";
    include("inc_db_con.php");
        if(mysql_num_rows($rs)==0) {
            $pos = "f";
        } else {
            $pos = "a";
            $posrow = mysql_fetch_array($rs);
        }
    mysql_close();
    if($pos != "f")
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirsort > ".$dir['dirsort']." AND diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            if(mysql_num_rows($rs)==0) {    $pos = "l";    }
        mysql_close();
    }
    ?>
	<tr>
		<td class=tdheaderl valign=top>Display position:</td>
		<td class=tdgeneral valign=top colspan="2"><select id=pos name=pos onchange="chgPos(this)">
			<option <?php if($pos=="f") { echo("selected"); } ?> value=f>First</option>
			<option value=b>Before</option>
			<option <?php if($pos=="a") { echo("selected"); } ?> value=a>After</option>
			<option <?php if($pos=="l") { echo("selected"); } ?> value=l>Last</option>
		</select><select id=poslist name=poslist><option selected>--- SELECT ---</option><?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
            echo("<option ");
            if($pos=="a" && $id == $posrow['dirid']) { echo(" selected "); }
            echo("value=".$id.">".$val."</option>");
        }
    mysql_close();
    $r = 1;
?></select>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdheaderl valign=top >Sub-Directorates:</td>
		<td class=tdgeneral valign=top>
            <table width=100% cellpadding=3 cellspacing=0 style="border: 0px solid #ffffff;">
                <?php
            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$dirid." AND subyn = 'Y' ORDER BY subsort";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2))
                {
                    $id2 = $row2['subid'];
                    $val2 = $row2['subtxt'];
                    include("inc_tr.php");
                    $stxt = html_entity_decode($val2,ENT_QUOTES,"ISO-8859-1");
                    $stxt = str_replace("'","\'",$stxt);
                    $stxt = str_replace("\"","\'\'",$stxt);
                ?>
                    <td class=tdgeneral style="border-bottom: 0px solid #ffffff;"><?php echo($val2); ?><?php if($row2['subhead']=="Y") { ?>*<?php } ?></td>
                    <td class=tdgeneral style="border-bottom: 0px solid #ffffff;" width=30><?php if($row2['subhead']=="N") { ?><input type=button value=" Del " onclick="delSub(<?php echo($id2); ?>,'<?php echo($stxt); ?>',<?php echo($dirid); ?>,'de')"><?php } ?></td>
                </tr>
                <?php
                }
            mysql_close($con2);
                ?>
            </table>
        </td>
		<td class=tdgeneral valign=top><small>&nbsp;</small></td>
	</tr>
	<tr>
		<td class=tdheaderl valign=top>&nbsp;</td>
        <?php
                    $dtxt = html_entity_decode($dir['dirtxt'],ENT_QUOTES,"ISO-8859-1");
                    $dtxt = str_replace("'","\'",$dtxt);
                    $dtxt = str_replace("\"","\'\'",$dtxt);

        ?>
		<td class=tdgeneral valign=top colspan="2"><input type=submit value="Save Changes"> <input type=button value=Delete onclick="delDir('<?php echo($dtxt); ?>',<?php echo($dirid); ?>)"></td>
	</tr>
</table>
<?php
    }    else    {    //IF NO ERROR
        echo("<p>An error has occurred.  Please go back and try again.</p>");
    }
?>
</form>
<script type=text/javascript>
var pos = document.getElementById('pos').value;
if(pos == "f" || pos == "l")
{
    document.getElementById('poslist').disabled = true;
}
</script>
<p style="font-size: 7.5pt; margin-top: -15px;"><i>* This is an automatically generated Sub-Directorate that is required by the system and cannot be edited.</i></p>
<h2>SDBIP Details</h2>
<table cellpadding=3 cellspacing=0 style="border-collapse: collapse; border: 1px solid #ababab;" width=570>
    <tr>
        <td class=tdheaderl width=130>KPIs:</td>
        <td class=tdgeneral><?php
            $sql = "SELECT count(kpiid) as kc FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiyn = 'Y' AND kpisubid IN (SELECT subid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid.")";
            include("inc_db_con.php");
                $detail = mysql_fetch_array($rs);
            mysql_close();
            echo($detail['kc']);
            $detail = array();
        ?></td>
    </tr>
    <tr>
        <td class=tdheaderl width=130>Capital Projects:</td>
        <td class=tdgeneral><?php
            $sql = "SELECT count(cpid) as kc FROM assist_".$cmpcode."_".$modref."_capital WHERE cpyn = 'Y' AND cpsubid IN (SELECT subid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dirid.")";
            include("inc_db_con.php");
                $detail = mysql_fetch_array($rs);
            mysql_close();
            echo($detail['kc']);
            $detail = array();
        ?></td>
    </tr><?php
            $sql = "SELECT sum(cfrev1) as rb, sum(cfop1) as ob, sum(cfcp1) as cb FROM assist_".$cmpcode."_".$modref."_finance_cashflow WHERE cfyn = 'Y' AND cflineid IN (SELECT lineid FROM assist_".$cmpcode."_".$modref."_dirsub, assist_".$cmpcode."_".$modref."_finance_lineitems WHERE lineyn = 'Y' AND linesubid = subid AND subyn = 'Y' AND subdirid = ".$dirid.")";
            include("inc_db_con.php");
                $detail = mysql_fetch_array($rs);
            mysql_close();
    ?><tr>
        <td class=tdheaderl width=130>Original Revenue Budget:</td>
        <td class=tdgeneral>R <?php echo(number_format($detail['rb'],2)); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl width=130>Original Opex Budget:</td>
        <td class=tdgeneral>R <?php echo(number_format($detail['ob'],2)); ?></td>
    </tr>
    <tr>
        <td class=tdheaderl width=130>Original Capex Budget:</td>
        <td class=tdgeneral>R <?php echo(number_format($detail['cb'],2)); ?></td>
    </tr>
</table>
<?php
$urlback = "setup_dir.php";
include("inc_goback.php");
?>
</body>
</html>
