<?php
    include("inc_ignite.php");
	$gfs = getListKPI($dlist['gfs']);
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var sub = me.subid.value;
    var t = me.t.value;
    t = parseInt(t);
    var valid8 = "true";
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
            document.getElementById('val').className = 'reqmet';
    }
    if(sub.length==0 || sub == "X" || sub == "dX")
    {
        document.getElementById('subid').className = 'reqtext';
        document.getElementById('subid').value = "X";
        if(valid8=="true")
        {
            document.getElementById('subid').focus();
        }
        valid8 = "false";
    }
    else
    {
            document.getElementById('subid').className = 'reqmet';
    }
    var target;
    var targ;
    var t2 = 0;
    var berr = "N";
    for(t2=0;t2<t;t2++)
    {
        targ = "r"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
        targ = "o"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
        targ = "c"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
    }
    if(valid8 == "false")
    {
        var errtxt = "Please complete all missing field(s) as indicated."
        if(berr == "Y")
        {
            errtxt = errtxt+"\nNote: Only numbers are allowed for budget figures.";
        }
        alert(errtxt);
        return false;
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
	vertical-align: top;
}
table th {
	text-align: left;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Monthly Cashflows ~ Add Line Item</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=admin_fin_cf_add_process.php onsubmit="return Validate(this);">
<table cellpadding=3 cellspacing=0 width=575>
    <tr>
		<th style="text-align: left;" width=125>Sub-Directorate:</th>
		<td colspan=2 width=450><select name=linesubid id=subid>
            <option value=X>--- SELECT ---</option>
    		<?php
            $subid = $_GET['s'];
            $sel = "N";
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['dirid'];
                    $val = $row['dirtxt'];
//                    echo("<option value=dX>".$val."</option>");
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$id." AND subyn = 'Y' ORDER BY subsort";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            $id2 = $row2['subid'];
                            $val2 = $row2['subtxt'];
                            echo("<option ");
                            if($id2 == $subid) {
                                echo(" selected ");
                                $sel = "Y";
                            }
                            echo("value=".$id2.">".$val2."</option>");
                        }
                    mysql_close($con2);
                }
            mysql_close();
            if($sel != "Y")
            {
                ?><script type=text/javascript>
                    document.getElementById('subid').value = "X";
                </script><?php
            }
            ?>
        </select></td>
	</tr>
    <tr>
		<th width=125>Line Item:</th>
		<td colspan=2 width=450 style="border-bottom: 1px solid #fff;"><input type=text size=50 maxlength=100 name=linevalue id=val> <span style="font-size: 7.5pt;">(max 100 characters)</span></td>
	</tr>
    <tr>
		<th width=125>Vote Number:</th>
		<td colspan=2 width=450 style="border-bottom: 1px solid #fff;"><input type=text size=30 maxlength=40 name=linevote id=val> <span style="font-size: 7.5pt;">(max 40 characters)</span></td>
	</tr>
    <tr>
		<th width=125>GFS Classification:</th>
		<td colspan=2 width=450 style="border-bottom: 1px solid #fff;"><select name=linegfsid id=gfs>
			<option selected value=X>--- SELECT ---</option>
			<?php
				foreach($gfs as $g) 
				{
					echo("<option value=".$g['id'].">".$g['value']."</option>");
				}
			?>
		</select></td>
	</tr>
<?php
$style['th'][1] = "background-color: #cc0001; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][2] = "background-color: #fe9900; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][3] = "background-color: #006600; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][4] = "background-color: #000066; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";

$style['thl'][1]['R'] = "background-color: #ffabab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['R'] = "background-color: #ffddab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['R'] = "background-color: #a3ffc2; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['R'] = "background-color: #babaff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['O'] = "background-color: #ffcccc; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['O'] = "background-color: #ffe6bd; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['O'] = "background-color: #ceffde; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['O'] = "background-color: #ccccff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['C'] = "background-color: #ffdbdf; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['C'] = "background-color: #fff1de; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['C'] = "background-color: #deffea; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['C'] = "background-color: #ddddff; color: #000; border-bottom: 1px solid #fff;";

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
    ?>
    <tr>
		<td class=tdgeneral width=120 colspan=1 height=27 style="background-color: #888; color: #fff; font-weight: bold; border-bottom: 1px solid #fff; "><b>Original Budget</b></td>
		<td class=tdgeneral valign=top width=450 colspan=2 style="background-color: #888; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;">&nbsp;</td>
    </tr>
    <?php
        $s=1;
        $t=0;
        while($row = mysql_fetch_array($rs))
        {
            ?>
            <tr>
                <td class=tdgeneral width=120 rowspan=3 valign=top style="<?php echo($style['th'][$s]); ?> font-weight: bold;"><?php echo(date("M Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
                <td class=tdgeneral width=70 style="<?php echo($style['thl'][$s]['R']); ?> font-weight: bold; border-left: 1px solid #fff;">Revenue:</td>
                <td class=tdgeneral width=380 valign=top style="<?php echo($style['thl'][$s]['R']); ?>">R <input type=text size=10 name=revenue[] id=r<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <tr>
                <td class=tdgeneral width=70 style="<?php echo($style['thl'][$s]['O']); ?> font-weight: bold; border-left: 1px solid #fff;">Op. Exp.:</td>
                <td class=tdgeneral width=380 valign=top style="<?php echo($style['thl'][$s]['O']); ?>">R <input type=text size=10 name=opex[] id=o<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <tr>
                <td class=tdgeneral width=70 style="<?php echo($style['thl'][$s]['C']); ?> font-weight: bold; border-left: 1px solid #fff;">Cap. Exp.:</td>
                <td class=tdgeneral width=380 valign=top style="<?php echo($style['thl'][$s]['C']); ?>">R <input type=text size=10 name=capex[] id=c<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <?php
            $t++;
            $s++;
            if($s>4) { $s=1; }
        }
    }
mysql_close();
?>
    <tr>
		<td class=tdheaderl width=120>&nbsp;<input type=hidden name=t value=<?php echo($t); ?> size=5></td>
		<td class=tdgeneral colspan=2 valign=top width=450><input type=submit value=" Add "></td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_fin_cf_setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
