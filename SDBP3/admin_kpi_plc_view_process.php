<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Edit KPI</b></h1>
<p>&nbsp;</p>
<?php
$err = "N";
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
//FORM DETAILS
$kpiid = $_POST['kpiid'];
$plcid = $_POST['plcid'];
$time = $_POST['time'];
$target = $_POST['target'];
$actual = $_POST['actual'];
$krtargettypeid = 2;
$tid = $_POST['tmpid'];
$tmpid = explode(",",$tid);
$act = $_POST['act'];
$rejnote = $_POST['rejnote'];

/*echo("<p>".$act);
echo("<p>".$rejnote);
echo("<p>".$kpiid);
echo("<p>".$plcid);
*/




//IF APPREQ == "N"  => Approval not required
if($act=="A")
{
//    echo("approval not required");
    if(strlen($plcid)>0 && is_numeric($plcid) && $plcid > 0)
    {
    //MOVE OLD DETAILS FROM PLC TO PLC_OLD
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_old (plcid,plcname,plcdescription,plckpiid,plcstartdate,plcenddate,plcproccate,plcyn,oldtkid,olddate) ";
        $sql.= " SELECT p2.plcid,p2.plcname,p2.plcdescription,p2.plckpiid,p2.plcstartdate,p2.plcenddate,p2.plcproccate,p2.plcyn,'".$tkid."' as oldtid, ".$today." as olddt FROM assist_".$cmpcode."_".$modref."_kpi_plc p2 WHERE plcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //MOVE NEW DETAILS FROM PLC_TEMP TO PLC
        $plcnew = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'Y' ORDER BY tmpid DESC";
//echo("<P>".$sql);
        include("inc_db_con.php");
            $plcnew = mysql_fetch_array($rs);
        mysql_close();
        
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc SET";
        $sql.= "  plcname = '".$plcnew['plcname']."'";
        $sql.= ", plcdescription = '".$plcnew['plcdescription']."'";
        $sql.= ", plcstartdate = ".$plcnew['plcstartdate'];
        $sql.= ", plcenddate = ".$plcnew['plcenddate'];
        $sql.= ", plcproccate = ".$plcnew['plcproccate'];
        $sql.= ", plcyn = 'Y'";
        $sql.= " WHERE plcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_temp SET tmpyn = 'A' WHERE tmpid = ".$plcnew['tmpid'];
//echo("<P>".$sql);
        include("inc_db_con.php");
    //MOVE OLD PHASES TO PHASES_OLD
        $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases_old (ppid,ppplcid,pptype,ppphaseid,ppsort,ppdays,ppstartdate,ppenddate,pptarget,ppdone,ppdonedate,oldtkid,olddate) ";
        $sql.= " SELECT p2.ppid,p2.ppplcid,p2.pptype,p2.ppphaseid,p2.ppsort,p2.ppdays,p2.ppstartdate,p2.ppenddate,p2.pptarget,p2.ppdone,p2.ppdonedate, '".$tkid."' as oldtid, ".$today." as olddte FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases p2 WHERE ppplcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //DELETE OLD PHASES
        $sql = "DELETE FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");
    //MOVE NEW PHASES FROM PHASES_TEMP TO PHASES
        foreach($tmpid as $temp)
        {
            $phase = array();
            if(strlen($temp)>0 && is_numeric($temp) && $temp > 0)
            {
                //GET TEMP DETAILS
                $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp WHERE tmpid = ".$temp;
//echo("<P>".$sql);
                include("inc_db_con.php");
                    $phase = mysql_fetch_array($rs);
                mysql_close();
                //UPDATE PHASES
                $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_kpi_plc_phases SET";
                $sql.= "  ppplcid = ".$plcid;
                $sql.= ", pptype = '".$phase['pptype']."'";
                $sql.= ", ppphaseid = ".$phase['ppphaseid'];
                $sql.= ", ppsort = ".$phase['ppsort'];
                $sql.= ", ppdays = ".$phase['ppdays'];
                $sql.= ", ppstartdate = ".$phase['ppstartdate'];
                $sql.= ", ppenddate = ".$phase['ppenddate'];
                $sql.= ", pptarget = ".$phase['pptarget'];
                $sql.= ", ppdone = '".$phase['ppdone']."'";
                $sql.= ", ppdonedate = ".$phase['ppdonedate'];
//echo("<P>".$sql);
                include("inc_db_con.php");
                //CHANGE STATUS
                $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp SET tmpyn = 'A' WHERE tmpid = ".$temp;
//echo("<P>".$sql);
                include("inc_db_con.php");
            }
        }
    //UPDATE KPI_RESULTS
/*        echo("<P>TIME-");
        print_r($time);
        echo("<P>TARGET-");
        print_r($target);
        echo("<P>ACTUAL-");
        print_r($actual);
*/         if(strlen($kpiid) > 0 && is_numeric($kpiid))
        {
            for($i=0;$i<count($time);$i++)
            {
                $t = $time[$i];
                $g = $target[$i];
                $a = $actual[$i];
                if(strlen($g)>0 && strlen($a)>0 && is_numeric($g) && is_numeric($a))
                {
                    $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_result SET ";
                    $sql.= "  krtarget = ".$g;
                    $sql.= ", kractual = ".$a;
                    $sql.= " WHERE krkpiid = ".$kpiid." AND krtimeid = ".$t;
//echo("<P>".$sql);
                    include("inc_db_con.php");
                }
            }
        }
        else
        {
            echo("<p>Error on kpiid validation - can't update results");
            $err = "Y";
        }
    }
    else
    {
        echo("<P>Error on plcid validation");
        $err = "Y";
    }
    
    if($err == "N")
    {
        echo("<p>PLC successfullly updated.");
    }
}
else //ELSE IF APPREQ == "Y" => Approval is required
{
//    echo("approval required");
    //CHANGE TEMP STATUS TO Y
        $plcnew = array();
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_temp WHERE plcid = ".$plcid." AND tmpyn = 'Y' ORDER BY tmpid DESC";
//echo("<P>".$sql);
        include("inc_db_con.php");
            $plcnew = mysql_fetch_array($rs);
        mysql_close();

    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_temp SET tmpyn = 'R' WHERE tmpid = ".$plcnew['tmpid'];
//echo("<P>".$sql);
        include("inc_db_con.php");

    //CHANGE PLC TEMP STATUS
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_kpi_plc_phases_temp SET tmpyn = 'R' WHERE ppplcid = ".$plcid;
//echo("<P>".$sql);
        include("inc_db_con.php");


    //SEND AN EMAIL TO KPI ADMIN
    if($err == "N")
    {
        echo("<p>Update complete.</p>");
    }

}

        $tsql = "";
        $told = "";
        $trans = "Updated PLC ".$plcid;
        include("inc_transaction_log.php");


if($err == "N")
{
                $urlback = "admin.php";
                include("inc_goback.php");
}
else
{
    include("inc_goback_history.php");
}


?>
</body>

</html>
