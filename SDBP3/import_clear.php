<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import - Clear Old Data</b></h1>
<?php
$what = $_GET['t'];

switch($what)
{
    case "CP":
        echo("<h2>Capital Projects</h2>");
        //capital
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital` (";
$sql.= "  `cpid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cpsubid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpgfsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpnum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `cpidpnum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `cpvotenum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `cpname` varchar(250) NOT NULL DEFAULT '',";
$sql.= "  `cpproject` text NOT NULL,";
$sql.= "  `cpstartdate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpenddate` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpstartactual` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpendactual` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cpyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `cpfundsourceid` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`cpid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
        //capital_results
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_results`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_results` (";
$sql.= "  `crid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `crcpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `crbudget` double NOT NULL DEFAULT '0',";
$sql.= "  `cractual` double NOT NULL DEFAULT '0',";
$sql.= "  `craccpercytd` double NOT NULL DEFAULT '0',";
$sql.= "  `crtotrandytd` double NOT NULL DEFAULT '0',";
$sql.= "  `crtimeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `crvire` double NOT NULL DEFAULT '0',";
$sql.= "  `cradjest` double NOT NULL DEFAULT '0',";
$sql.= "  `cradjbudget` double NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`crid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//capital_area
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_area`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_area` (";
$sql.= "  `caid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cacpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `caareaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cayn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`caid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//capital_forecast
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_forecast`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_forecast` (";
$sql.= "  `cfid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cfcpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cfcrr` double NOT NULL DEFAULT '0',";
$sql.= "  `cfother` double NOT NULL DEFAULT '0',";
$sql.= "  `cfyearid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`cfid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//capital_source
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_source`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_source` (";
$sql.= "  `csid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cscpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cssrcid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `csyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`csid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//capital_wards
$sql = "DROP TABLE IF EXISTS `".$dbref."_capital_wards`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_capital_wards` (";
$sql.= "  `cwid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cwcpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cwwardsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cwyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`cwid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
        break;
    case "CF":
        echo("<h2>Monthly Cashflows</h2>");
        //finance_lineitems
$sql = "DROP TABLE IF EXISTS `".$dbref."_finance_lineitems`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_finance_lineitems` (";
$sql.= "  `lineid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `linevalue` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `lineyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `linesort` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `linetype` varchar(2) NOT NULL DEFAULT 'CF' COMMENT 'CF OR RS',";
$sql.= "  `linesubid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `linegfsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `linevote` varchar(45) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`lineid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ";
		include("inc_db_con.php");    echo "<p>".$sql;
        //finance_cashflow
$sql = "DROP TABLE IF EXISTS `".$dbref."_finance_cashflow`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_finance_cashflow` (";
$sql.= "  `cfid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `cflineid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cfyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `cftimeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `cfrev1` double NOT NULL DEFAULT '0' COMMENT 'obudget',";
$sql.= "  `cfrev2` double NOT NULL DEFAULT '0' COMMENT 'actual',";
$sql.= "  `cfrev3` double NOT NULL DEFAULT '0' COMMENT 'accytd',";
$sql.= "  `cfrev4` double NOT NULL DEFAULT '0' COMMENT 'totytd',";
$sql.= "  `cfrev5` double NOT NULL DEFAULT '0' COMMENT 'var',";
$sql.= "  `cfrev6` double NOT NULL DEFAULT '0' COMMENT 'vire',";
$sql.= "  `cfrev7` double NOT NULL DEFAULT '0' COMMENT 'adjest',";
$sql.= "  `cfrev8` double NOT NULL DEFAULT '0' COMMENT 'adjbudget',";
$sql.= "  `cfop1` double NOT NULL DEFAULT '0' COMMENT 'obudget',";
$sql.= "  `cfop2` double NOT NULL DEFAULT '0' COMMENT 'actual',";
$sql.= "  `cfop3` double NOT NULL DEFAULT '0' COMMENT 'accytd',";
$sql.= "  `cfop4` double NOT NULL DEFAULT '0' COMMENT 'totytd',";
$sql.= "  `cfop5` double NOT NULL DEFAULT '0' COMMENT 'var',";
$sql.= "  `cfop6` double NOT NULL DEFAULT '0' COMMENT 'vire',";
$sql.= "  `cfop7` double NOT NULL DEFAULT '0' COMMENT 'adjest',";
$sql.= "  `cfop8` double NOT NULL DEFAULT '0' COMMENT 'adjbudget',";
$sql.= "  `cfcp1` double NOT NULL DEFAULT '0' COMMENT 'obudget',";
$sql.= "  `cfcp2` double NOT NULL DEFAULT '0' COMMENT 'actual',";
$sql.= "  `cfcp3` double NOT NULL DEFAULT '0' COMMENT 'accytd',";
$sql.= "  `cfcp4` double NOT NULL DEFAULT '0' COMMENT 'totytd',";
$sql.= "  `cfcp5` double NOT NULL DEFAULT '0' COMMENT 'var',";
$sql.= "  `cfcp6` double NOT NULL DEFAULT '0' COMMENT 'vire',";
$sql.= "  `cfcp7` double NOT NULL DEFAULT '0' COMMENT 'adjest',";
$sql.= "  `cfcp8` double NOT NULL DEFAULT '0' COMMENT 'adjbudget',";
$sql.= "  PRIMARY KEY (`cfid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
        break;
    case "KPI":
        echo("<h2>KPIs</h2>");
		//kpi
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi` (";
$sql.= "  `kpiid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `kpimsr` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `kpisubid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpigfsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpiidpnum` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `kpicpid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpimunkpaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpinatkpaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpitasid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpiprogid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpivalue` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpicpmun` varchar(50) NOT NULL DEFAULT '',";
$sql.= "  `kpitypeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpistratop` varchar(1) NOT NULL DEFAULT 'O',";
$sql.= "  `kpidefinition` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpioutcome` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpichallenges` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpiriskdef` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpiriskrate` varchar(1) NOT NULL DEFAULT '',";
$sql.= "  `kpidriver` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpibaseline` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpitargetunit` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpipoe` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpifield1` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpifield2` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpifield3` varchar(255) NOT NULL DEFAULT '',";
$sql.= "  `kpicalctype` varchar(5) NOT NULL DEFAULT 'CO' COMMENT 'CO, ACC, STD, ZERO',";
$sql.= "  `kpitargettypeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kpiyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`kpiid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//kpiarea
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_area`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_area` (";
$sql.= "  `kaid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `kakpiid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kaareaid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kayn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`kaid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//result
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_result`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_result` (";
$sql.= "  `krid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `krkpiid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `krtarget` double NOT NULL DEFAULT '0',";
$sql.= "  `kractual` double NOT NULL DEFAULT '0',";
$sql.= "  `krprogress` text NOT NULL,";
$sql.= "  `krmanagement` text NOT NULL,";
$sql.= "  `krtimeid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  PRIMARY KEY (`krid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
		//wards
$sql = "DROP TABLE IF EXISTS `".$dbref."_kpi_wards`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_kpi_wards` (";
$sql.= "  `kwid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `kwkpiid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kwwardsid` int(10) unsigned NOT NULL DEFAULT '0',";
$sql.= "  `kwyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`kwid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
        break;
    case "RBS":
        echo("<h2>Revenue By Source</h2>");
$sql = "DROP TABLE IF EXISTS `".$dbref."_finance_revbysource`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_finance_revbysource` (";
$sql.= "  `rsid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `rsline` varchar(200) NOT NULL DEFAULT '0',";
$sql.= "  `rsyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `rssort` int(10) unsigned NOT NULL,";
$sql.= "  `rs1budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs1actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs2budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs2actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs3budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs3actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs4budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs4actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs5budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs5actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs6budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs6actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs7budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs7actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs8budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs8actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs9budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs9actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs10budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs10actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs11budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs11actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rs12budget` double NOT NULL DEFAULT '0',";
$sql.= "  `rs12actual` double NOT NULL DEFAULT '0',";
$sql.= "  `rsvote` varchar(100) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`rsid`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
        break;
    default:
		$w = strFn("explode",$what,"_","");
		if(count($w)!=2) {
			die("<h2>ERROR!!!!</h2>");
		} else {
			echo("<h2>Lists</h2>");
			switch($w[1])
			{
				case "AREA": //areas
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_area`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_area` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
					break;
				case "DIR":
$sql = "DROP TABLE IF EXISTS `".$dbref."_dir`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_dir` (";
$sql.= "  `dirid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `dirtxt` varchar(100) NOT NULL,";
$sql.= "  `dirsort` int(10) unsigned NOT NULL,";
$sql.= "  `diryn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  PRIMARY KEY (`dirid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
					break;
				case "DIRSUB":
$sql = "DROP TABLE IF EXISTS `".$dbref."_dirsub`";
include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_dirsub` (";
$sql.= "  `subid` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `subtxt` varchar(100) NOT NULL,";
$sql.= "  `subyn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `subdirid` int(10) unsigned NOT NULL,";
$sql.= "  `subsort` int(10) unsigned NOT NULL,";
$sql.= "  `subhead` varchar(1) NOT NULL DEFAULT 'N',";
$sql.= "  PRIMARY KEY (`subid`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
include("inc_db_con.php");    echo "<p>".$sql;
					break;
				case "FSRC": //funding source
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_fundsource`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_fundsource` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
					break;
				case "GFS": //gfs classification
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_gfs`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_gfs` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL DEFAULT '',";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
					break;
				case "KPA": //municipal kpas
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_munkpa`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_munkpa` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(200) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `code` varchar(10) NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
					break;
				case "WARDS": //wards
$sql = "DROP TABLE IF EXISTS `".$dbref."_list_wards`";
		include("inc_db_con.php");    echo "<p>".$sql;
$sql = "CREATE TABLE IF NOT EXISTS `".$dbref."_list_wards` (";
$sql.= "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
$sql.= "  `value` varchar(100) NOT NULL DEFAULT '',";
$sql.= "  `yn` varchar(1) NOT NULL DEFAULT 'Y',";
$sql.= "  `numval` int(10) unsigned NOT NULL,";
$sql.= "  PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
		include("inc_db_con.php");    echo "<p>".$sql;
					break;
			}
		}
	break;
}

$urlback = "import.php";
include("inc_goback.php");

?>

</body>

</html>
