<?php
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
ini_set('max_execution_time',1800); 
include("inc_ignite.php");
//error_reporting(-1);
$nosort = array("wards","area","fundsource","progress");
$nogroup = array("cpproject","dir");
$nofilter = array("progress");
    $dlist = array(
        'gfs'=>array('name'=>"GFS Classification",'id'=>"gfs",'code'=>"N",'yn'=>"Y",'headfield'=>"gfs",'kpi'=>"cpgfsid")
	);
$object_id = "cpid";
$object_table = "capital";
$page_title = "Capital Projects";
//SUMMARY OF RESULTS
/*function drawSum($what,$txt,$val,$i) {
	global $output;
	global $styler;
	global $time;
	//if(!checkIntRef($val)) { $val = 0; }
	switch($what) {
		case "title":
			if($output=="csv") {
				$echo ="\"\",\"\"\r\n\"Summary of Results\"\r\n";
			} elseif ($output == "excel") {
				$echo ="<tr></tr><tr><td nowrap class=title2 style=\"text-align: left;\">Summary of Results</td>";
			} else {
				$echo ="</table><h2>Summary of Results</h2><table cellpadding=3 cellspacing=0>";
				$echo.= "<tr><th rowspan=2>&nbsp;</th>";
				foreach($time as $tim) {
					$echo.= "<th colspan=4>".date("F Y",$tim['eval'])."</th>";
				}
				$echo.="<th colspan=3>Total</th></tr><tr>";
				foreach($time as $tim) {
					$echo.="<th>Budget</th><th>Actual</th><th>Acc. % Spent YTD</th><th>Total R Spent YTD</th>";
				}
				$echo.="<th>Budget</th><th>Actual</th><th>Acc. % Spent YTD</th></tr>";
			}
			break;
		case "sum":
			if($output=="csv") {
				$echo ="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo = "<tr><td></td><td nowrap style=\"font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$txt."</td><td nowrap style=\"text-align: left; font-weight: bold; border-top: thin solid #000000; border-bottom: thin solid #000000;\">".$val."</td></tr>";
			} else {
				$echo.= "<tr><td style=\"font-weight: bold;\">".$txt."</td>";
				$b = 0;
				$a = 0;
				foreach($time as $tim) {
					$b+=$val[$tim['id']]['b'];
					$a+=$val[$tim['id']]['a'];
					$echo.= "<td style=\"text-align: right;\">".number_format($val[$tim['id']]['b'],2)."</td>";	//budget
					$echo.= "<td style=\"text-align: right;\">".number_format($val[$tim['id']]['a'],2)."</td>";	//actual
					$p=0;
					if($b==0) {
						$p = $a==0 ? 0 : 100;
					} else {
						$p = ($a/$b)*100;
					}
					$echo.= "<td style=\"text-align: right;\">".number_format($p,2)."%</td>";	//acc%
					$echo.= "<td style=\"text-align: right;\">".number_format($a,2)."</td>";	//totR
				}
				$p=0;
				if($b==0) {
					$p = ($a==0) ? 0 : 100;
				} else {
					$p = ($a/$b)*100;
				}
				$echo.= "<td style=\"text-align: right; background-color: #eeeeee;\">".number_format($b,2)."</td>";	//budget
				$echo.= "<td style=\"text-align: right; background-color: #eeeeee;\">".number_format($a,2)."</td>";	//actual
				$echo.= "<td style=\"text-align: right; background-color: #eeeeee;\">".number_format($p,2)."%</td>";	//acc%
				$echp.="</tr>";
				$echo.= "</table>";
			}
			break;
		default:
			if($output=="csv") {
				$echo.="\"".$txt."\",\"\",\"".$val."\"\r\n";
			} elseif ($output == "excel") {
				$echo.= "<tr><td style=\"".$styler[$i]."\"> </td><td nowrap>".$txt."</td><td nowrap style=\"text-align: left;\">".$val."</td></tr>";
			} else {
				$echo.= "<tr><td style=\"font-weight: bold;\">".$txt."</td>";
				$b = 0;
				$a = 0;
				foreach($time as $tim) {
					$b+=$val[$tim['id']]['b'];
					$a+=$val[$tim['id']]['a'];
					$echo.= "<td style=\"text-align: right;\">".number_format($val[$tim['id']]['b'],2)."</td>";	//budget
					$echo.= "<td style=\"text-align: right;\">".number_format($val[$tim['id']]['a'],2)."</td>";	//actual
					$p=0;
					if($b==0) {
						$p = $a==0 ? 0 : 100;
					} else {
						$p = ($a/$b)*100;
					}
					$echo.= "<td style=\"text-align: right;\">".number_format($p,2)."%</td>";	//acc%
					$echo.= "<td style=\"text-align: right;\">".number_format($a,2)."</td>";	//totR
				}
				$p=0;
				if($b==0) {
					$p = ($a==0) ? 0 : 100;
				} else {
					$p = ($a/$b)*100;
				}
				$echo.= "<td style=\"text-align: right; background-color: #eeeeee;\">".number_format($b,2)."</td>";	//budget
				$echo.= "<td style=\"text-align: right; background-color: #eeeeee;\">".number_format($a,2)."</td>";	//actual
				$echo.= "<td style=\"text-align: right; background-color: #eeeeee;\">".number_format($p,2)."%</td>";	//acc%
				$echo.="</tr>";
			}
			break;
	}
	return $echo;
}*/

//GET LISTS
foreach($dlist as $dl) {
		$dld = getListKPI($dl);
		$dlist[$dl['headfield']]['data'] = $dld;
}
  //WARDS
$dlist['wards'] = array('id'=>"wards",'headfield'=>"wards",'data'=>array());
$sql = "SELECT * FROM ".$dbref."_list_wards WHERE yn = 'Y' ORDER BY numval";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) { $dlist['wards']['data'][$row['id']] = $row; }
mysql_close($con);
  //AREA
$dlist['area'] = array('id'=>"area",'headfield'=>"area",'data'=>array());
$sql = "SELECT * FROM ".$dbref."_list_area WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) { $dlist['area']['data'][$row['id']] = $row; }
mysql_close($con);

//echo "<pre>";  print_r($_REQUEST); echo "</pre>";

//GET VARIABLES
$field = array();
$filter = array();
$filtertype = array();
$objects = array();
$results = array();
$time = array();
$group = $_REQUEST['groupby'];
$src = $_REQUEST['src'];
if($src=="DASH") {
	$field['dir'] = "Y";
	$field['sub'] = "Y";
	$field['results'] = "Y";
	$field['crresult'] = "Y";
	$field['crytd'] = "N";
	if($group=="dir") { $group = "sub"; }
} else {
	$field['dir'] = $_REQUEST['dir'];
	$field['sub'] = $_REQUEST['sub'];
	$field['results'] = $_REQUEST['results'];
	$field['crresult'] = $_REQUEST['crresult'];
	$field['crytd'] = $_REQUEST['crytd'];
}
$cf = $_REQUEST['cf']; //budget from
$ct = $_REQUEST['ct']; //budget to
$filter['dir'] = $_REQUEST['dirfilter'];
$filter['sub'] = $_REQUEST['subfilter'];
$sortarr = $_REQUEST['sort'];
$output = $_REQUEST['output'];
switch($output) {
	case "csv": $thou_sep = ""; break;
	case "excel": $thou_sep = " "; break;
	default: $thou_sep = ",";
}
$sum = $_REQUEST['sum'];
$kpicpid = $_REQUEST['kpicpid'];
$rhead = $_REQUEST['rhead'];
$echocount = 0;
if(count($sortarr)==0) { $sort = array(); }
//GET FIELD HEADINGS
$sql = "SELECT * FROM ".$dbref."_headings WHERE headtype = 'CP' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
		if($src=="DASH" || $_REQUEST[$row['headfield']]=="Y") {
			$field[$row['headfield']] = "Y";
		}
		$head[$row['headfield']] = $row;
		$filter[$row['headfield']] = $_REQUEST[$row['headfield'].'filter'];
		$filtertype[$row['headfield']] = $_REQUEST[$row['headfield'].'filtertype'];
		if(count($sortarr)==0 && !in_array($row['headfield'],$nosort)) {
			$sort[] = $row['headfield'];
		}
    }
mysql_close($con);

if(count($sortarr)==0) { $sortarr = $sort; $sort = null; }
//GET TIME PERIODS
$timesql = " FROM ".$dbref."_list_time WHERE yn = 'Y' AND sort >= ".$cf." AND sort <= ".$ct." ORDER BY sort";
$sql = "SELECT * ".$timesql;
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		$time[$row['sort']] = $row;
	}
mysql_close($con);
//echo "<pre>"; print_r($time); echo "</pre>";
//WARDS FILTER
$hf = "wards";
$objectwa[$hf] = array();
if(count($filter[$hf]) != count($dlist[$hf]['data']) && count($filter[$hf])>0) {
	if($filtertype[$hf] == "any") {
		$sql = "SELECT DISTINCT cw".$object_id." as ".$object_id." FROM ".$dbref."_capital_wards WHERE cwwardsid IN (".implode(", ",$filter[$hf]).") AND cwyn = 'Y' ORDER BY cwpid";
	} elseif($filtertype[$hf]=="all") {
		$wwhere = array();
		$sql = "SELECT c.".$object_id." FROM ".$dbref."_capital c";
		foreach($filter[$hf] as $w) {
			$n = "w".$w;
			$sql.= " INNER JOIN ".$dbref."_capital_wards $n ON ".$n.".cw".$object_id." = c.".$object_id."";
			$wwhere[] = $n.".cwwardsid = $w AND ".$n.".cwyn = 'Y'";
		}
		$sql.= " WHERE ".implode(" AND ",$wwhere);
		$sql.= " AND c.cpyn = 'Y' ORDER BY c.".$object_id."";
	} else {
		die("<h2>Error</h2><p>An error has occurred.  The Report cannot be generated.</p>");
	}
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) { $objectwa[$hf][] = $row['".$object_id."']; }
	mysql_close($con);
} else {
	$filter[$hf] = array();
}

//AREA FILTER
$hf = "area";
$objectwa[$hf] = array();
if(count($filter[$hf]) != count($dlist[$hf]['data']) && count($filter[$hf])>0) {
	if($filtertype[$hf] == "any") {
		$sql = "SELECT DISTINCT ca".$object_id." as ".$object_id." FROM ".$dbref."_capital_area WHERE caareaid IN (".implode(", ",$filter[$hf]).") AND cayn = 'Y' ORDER BY ca".$object_id."";
	} elseif($filtertype[$hf]=="all") {
		$wwhere = array();
		$sql = "SELECT c.".$object_id." FROM ".$dbref."_capital c";
		foreach($filter[$hf] as $w) {
			$n = "a".$w;
			$sql.= " INNER JOIN ".$dbref."_capital_area $n ON ".$n.".ca".$object_id." = c.".$object_id."";
			$wwhere[] = $n.".caareaid = $w AND ".$n.".cayn = 'Y'";
		}
		$sql.= " WHERE ".implode(" AND ",$wwhere);
		$sql.= " AND c.cpyn = 'Y' ORDER BY c.".$object_id."";
	} else {
		die("<h2>Error</h2><p>An error has occurred.  The Report cannot be generated.</p>");
	}
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) { $objectwa[$hf][] = $row['".$object_id."']; }
	mysql_close($con);
} else {
	$filter[$hf] = array();
}

//INTERSECTING AREA/WARDS
if(count($filter['wards'])>0 && count($filter['area'])>0) {
	$objwardsarea = array_intersect($objectwa['wards'],$objectwa['area']);
} elseif(count($filter['wards'])>0 && !(count($filter['area'])>0)) {
	$objwardsarea = $objectwa['wards'];
} elseif(!(count($filter['wards'])>0) && count($filter['area'])>0) {
	$objwardsarea = $objectwa['area'];
} else {
	$objwardsarea = array();
}


//GROUP BY
$glist = array();
if(strlen($group)==0 || $group == "X") {
	$group = "X";
	$groupid = 0;
	$glist[] = array('id'=>0,'value'=>"");
} elseif(isset($dlist[$group])) {
	$groupid = $dlist[$group]['kpi'];
	$sql = "SELECT * FROM ".$dbref."_list_".$dlist[$group]['id']." WHERE yn = 'Y' ORDER BY value";
	include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$glist[$row['id']] = $row;
	}
	mysql_close($con);
} elseif($group=="sub") {
	$groupid = "cpsubid";
	$sql = "SELECT s.subid id, s.subtxt, d.dirtxt FROM ".$dbref."_dirsub s, ".$dbref."_dir d WHERE subyn = 'Y' AND diryn = 'Y' AND subdirid = dirid ORDER BY dirsort, subsort";
	include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$glist[$row['id']] = array('id'=>$row['id'],'value'=>$row['dirtxt']." - ".$row['subtxt']);
	}
	mysql_close($con);
} else {
	$groupid = $group;
	$sql = "SELECT DISTINCT $groupid FROM ".$dbref."_capital WHERE cpyn = 'Y'";
	include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$gval = $row[$groupid];
		$gid = strFn("str_replace",strtolower(decode($gval))," ","");
		$glist[$gval] = array('id'=>$gid,'value'=>$gval);
	}
	mysql_close($con);
}		

		
/**************** SET KPI SQL ************/
//SELECT
$repsql[0] = "SELECT c.*";
$repsql[1].= ", d.dirtxt";
$repsql[1].= ", s.subtxt";
//FROM
$repsql[2] = " FROM ".$dbref."_capital c";
$repsql[2].= " INNER JOIN ".$dbref."_dirsub s ON c.cpsubid = s.subid";
$repsql[2].= " INNER JOIN ".$dbref."_dir d ON s.subdirid = d.dirid";
foreach($dlist as $d) {
	$hf = $d['headfield'];
	if($hf!="wards" && $hf!="area") {
		$repsql[2].= " INNER JOIN ".$dbref."_list_".$d['id']." ".$hf." ON ";
		$repsql[2].= $hf.".id = c.".$d['kpi'];
	}
}
//WHERE
$repsql[2].= " WHERE c.cpyn = 'Y' AND d.diryn = 'Y' AND s.subyn = 'Y'";
if($filter['dir']!="ALL" && checkIntRef($filter['dir'])) {
	$repsql[2].= " AND d.dirid = ".$filter['dir'];
}
if($filter['sub'][0]!="ALL" && checkIntRef($filter['sub'][0])) {
	$repsql[2].= " AND (s.subid = ".strFn("implode",$filter['sub']," OR s.subid = ","").") ";
}
$wardsareadone = "N";
foreach($head as $h) {
	$where = "";
	$hf = $h['headfield'];
	if (($hf == "wards" || $hf == "area" || $hf == "fundsource")) {				//WARDS/AREA
		if(count($objectwardsarea)>0 && $wardsareadone == "N") {
			$where = " c.".$object_id." IN (".implode(", ",$objectwardsarea).")";
			$wardsareadone = "Y";
		}
	} elseif(array_key_exists($hf,$dlist)) {					//LISTS
		if(count($filter[$hf])>1 && strtolower($filter[$hf][0])!="all") {
			if(count($filter[$hf])!=count($dlist[$hf]['data'])) {
				$where = "(";
				$where.= $hf.".id = ".implode(" OR ".$hf.".id = ",$filter[$hf]);
				$where.= ")";
			}
		} elseif(strtolower($filter[$hf][0])!="all" && checkIntRef($filter[$hf][0])) {
			$where = $hf.".id = ".$filter[$hf][0];
		}
	} elseif($hf=="cpstartdate" || $hf == "cpenddate" || $hf == "cpstartactual" || $hf == "cpendactual") {
			$from = $filter[$hf][0];
			$to = $filter[$hf][1];
			if(strlen($from)>0 && strlen($to)>0) {
				$from = strFn("explode",$from," ","");
				$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
				$to = strFn("explode",$to," ","");
				$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
			} elseif(strlen($from)>0) {
				$from = strFn("explode",$from," ","");
				$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
				$to = mktime(23,59,59,getMonth($from[1]),$from[0],$from[2]);
			} elseif(strlen($to)>0) {
				$to = strFn("explode",$to," ","");
				$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
				$from = mktime(0,0,0,getMonth($to[1]),$to[0],$to[2]);
			}
			if(is_numeric($from) && is_numeric($to)) {
				$where = "$hf >= $from AND $hf <= $to";
			}
	} elseif(strlen($filter[$hf])>0) {							//TEXT FIELDS
			$filter[$hf] = trim($filter[$hf]);
                    switch($filtertype[$hf])
                    {
						case "X":
							break;
                        case "all":
                            $ft = array_unique(explode(" ",code($filter[$hf])));
							$where = "(".$hf." LIKE '%".strFn("implode",$ft,"%' AND $hf LIKE '%","")."%')";
							$where = str_replace("AND $hf LIKE '%%'","",$where);
                            break;
                        case "any":
                            $ft = array_unique(explode(" ",code($filter[$hf])));
							$where = "(".$hf." LIKE '%".strFn("implode",$ft,"%' OR $hf LIKE '%","")."%')";
							$where = str_replace("OR $hf LIKE '%%'","",$where);
                            break;
                        case "exact":
                            $where = $hf." LIKE '%".code($filter[$hf])."%'";
                            break;
                        default:
                            $where = $hf." LIKE '%".code($filter[$hf])."%'";
                            break;
                    }
	}
	if(strlen($where)>0) {
		$repsql[2].=" AND ".$where;
	}
}

if(is_numeric($kpicpid) && $kpicpid==0) {
	$repsql[2].= " AND $object_id NOT IN (SELECT kpicpid FROM ".$dbref."_kpi WHERE kpiyn = 'Y' AND kpicpid > 0)";
} elseif(is_numeric($kpicpid) && $kpicpid==1) {
	$repsql[2].= " AND $object_id IN (SELECT kpicpid FROM ".$dbref."_kpi WHERE kpiyn = 'Y' AND kpicpid > 0)";
}
//SQL ORDER
$repsql[3] = " ORDER BY ";
$r=0;
foreach($sortarr as $s) {
	$r++;
	if($r>1) { $repsql[3].=", "; }
	if(isset($dlist[$s])) {
		$repsql[3].= " ".$s.".value ASC";
	} elseif($s=="dir") {
		$repsql[3].= " d.dirsort ASC";
	} elseif($s=="sub") {
		$repsql[3].= " s.subsort ASC";
	} else {
		$repsql[3].= " c.".$s." ASC";
	}
}
/************* END KPI SQL ***************/

//GET KPIs
$gkpi = array();
$sql = implode(" ",$repsql);
//echo "<p>".$sql;
//arrPrint($filter);
include("inc_db_con.php");
$gkpis = array();
	while($row = mysql_fetch_array($rs))
	{
		if(is_numeric($groupid)) { 
			$objects[0][$row[$object_id]] = $row;
			$gobjectss[$row[$object_id]] = 0;
		} elseif(isset($dlist[$group])) {
			$objects[$row[$groupid]][$row[$object_id]] = $row;
			$gobjects[$row[$object_id]] = $row[$groupid];
		} else {
			$gval = strFn("str_replace",strtolower(decode($row[$groupid]))," ","");
			$objects[$gval][$row[$object_id]] = $row;
			$gobjects[$row[$object_id]] = $gval;
		}
	}
mysql_close($con);

//GET RESULTS
if($field['results'] == "Y") {
	$sql = "SELECT * FROM ".$dbref."_".$object_table."_results WHERE cr".$object_id." IN (";
	$sql.= "SELECT c.".$object_id." ".$repsql[2].")";
	$sql.= " AND crtimeid IN (SELECT id ".$timesql.")";
	$sql.= " ORDER BY cr".$object_id.", crtimeid";
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs))
		{
			$results[$row['cr'.$object_id]][$row['crtimeid']] = $row;
		}
	mysql_close($con);

	foreach($results as $objid => $row) {
		foreach($time as $tim) {
			$timeid = $tim['id'];
			$budget = $row[$timeid]['crbudget'];
			$actual = $row[$timeid]['cractual'];
			$results[$objid][$timeid]['b'] = number_format($budget,2,".",$thou_sep);
			$results[$objid][$timeid]['a'] = number_format($actual,2,".",$thou_sep);
			$results[$objid]['budget']+=$budget;
			$results[$objid]['actual']+=$actual;
			$results[$objid]['b'] = number_format($results[$objid]['budget'],2,".",$thou_sep);
			$results[$objid]['a'] = number_format($results[$objid]['actual'],2,".",$thou_sep);
			if($results[$objid]['actual']>0 || $results[$objid]['budget'] >0) {
				$results[$objid][$timeid]['perc'] = ($results[$objid]['budget']>0) ? ($results[$objid]['actual'] / $results[$objid]['budget'])*100 : 100;
			} else {
				$results[$objid][$timeid]['perc'] = 0;
			}
			$results[$objid][$timeid]['p'] = number_format($results[$objid][$timeid]['perc'],2,".",$thou_sep)."%";
			$results[$objid][$timeid]['rand'] = $results[$objid]['actual'];
			$results[$objid][$timeid]['r'] = number_format($results[$objid]['actual'],2,".",$thou_sep);
			$gid = $gobjects[$objid];
			$glist[$gid]['res'][$timeid]['b']+=$budget;
			$glist[$gid]['res'][$timeid]['a']+=$actual;
		}
	}
	
}

//OUTPUT
				$c2 = 0;
				if($field['crresult']=="Y") $c2+=2;
				if($field['crytd']=="Y") $c2+=2;
				//if($field['krmanage']=="Y") $c2+=1;

$echo = "";
$cspan = 1 + count($field) - 3;
$gspan = $cspan;
$tspan = (count($time) * $c2) + 3;
$cspan += $tspan;
switch($output) 
{
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[10] = "\""; //heading cell
	$cellz[10] = "\","; //heading cell
	$cella[11] = "\""; //heading cell
	$cellz[11] = "\","; //heading cell
	$cella[12] = "\""; //heading cell
	$cellz[12] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$cella[22][0] = "\""; //result cell
	$cella[22][1] = "\""; //result cell
	$cella[22][2] = "\""; //result cell
	$cella[22][3] = "\""; //result cell
	$cella[22][4] = "\""; //result cell
	$cella[22][5] = "\""; //result cell
	$cella[22][6] = "\""; //result cell
	$cella[22][9] = "\""; //result cell
	$cella[22][90] = "\""; //result cell
	$rowa = "";
	$rowz = "\r\n";
	$pagea = "\"".decode($cmpname)."\"\r\n";
	if(strlen($rhead)>0) {
		$pagea.= "\"".$rhead."\"\r\n";
	} else {
		$pagea.= "\"SDBIP ".$modtxt.": $page_title Report\"\r\n";
	}
	$table[1] = "";
	$table[9] = "";
	$pagez = "\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	break;
case "excel":
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	if($field['results']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[10] = "<td class=\"head\" colspan=$c2>"; //time heading cell
	$cellz[10] = "</td>"; //time heading cell
	$cella[11] = "<td class=\"head\" colspan=2>"; //period-to-date heading cell
	$cellz[11] = "</td>"; //period-to-date heading cell
	$cella[12] = "<td class=\"head\" rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</td>"; //rowspan heading cell
	$cella[2] = "<td class=kpi>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=kpi style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=kpi style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22][0] = "<td class=kpi>"; //result cell
	$cella[22][1] = "<td class=kpi>"; //result cell
	$cella[22][2] = "<td class=kpi>"; //result cell
	$cella[22][3] = "<td class=kpi>"; //result cell
	$cella[22][4] = "<td class=kpi>"; //result cell
	$cella[22][5] = "<td class=kpi>"; //result cell
	$cella[22][6] = "<td class=kpi>"; //result cell
	$cella[22][9] = "<td class=kpitotalc>"; //result cell
	$cella[22][90] = "<td class=kpitotalr>"; //result cell
	$rowa = chr(10)."<tr>";
	$rowb = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>KPI Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>".chr(10)." td { font-style: Calibri; font-size:11pt; } .kpi { border-width: thin; border-color: #000000; border-style: solid; } .kpitotalc { background-color: #dedede; font-weight: bold; border-width: thin; border-color: #000000; border-style: solid; } .kpitotalr { font-weight: bold; background-color: #cccccc; font-weight: bold; border-width: thin; border-color: #000000; border-style: solid; } .head { font-weight: bold; text-align: center; background-color: #cccccc; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } ".chr(10)." .title { font-size:20pt; font-style: Calibri; color:#CC0001; font-weight:bold; text-decoration:underline; text-align: center; } ".chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#CC0001; font-weight:bold; text-decoration:underline; text-align: center;} .title3 { font-size:14pt; font-style: Calibri; color:#CC0001; font-weight:bold;} .title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$cspan>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$cspan>";
	if(strlen($rhead)>0) {
		$pagea.= $rhead;
	} else {
		$pagea.= "SDBIP ".$modtxt.": $page_title Report";
	}
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = chr(10);
	break;
default:
	$gcell[1] = "<h3 class=fc>";	//start of group
	$gcell[9] = "</h3>";	//end of group
	if($field['results']=="Y") { $rowspan = 2; } else { $rowspan = 1; }
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[10] = "<th colspan=$c2>"; //time heading cell
	$cellz[10] = "</th>"; //time heading cell
	$cella[11] = "<th colspan=2>"; //period-to-date heading cell
	$cellz[11] = "</th>"; //period-to-date heading cell
	$cella[12] = "<th rowspan=$rowspan>"; //rowspan heading cell
	$cellz[12] = "</th>"; //rowspan heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td style=\"text-align: right;background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$cella[22][0] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][1] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][2] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][3] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][4] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][5] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][6] = "<td style=\"text-align: right;\">"; //result cell
	$cella[22][9] = "<td style=\"text-align: right;background-color: #dedede; font-weight: bold;\">"; //total result cell
	$cella[22][90] = "<td style=\"text-align: right;background-color: #cccccc; font-weight: bold;\">"; //total result cell
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/default.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"/styles/style_red.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"lib/SDBP2.css\" type=\"text/css\">";
	$pagea.= "<style type=text/css>table { border-width: 1px; border-style: solid; border-color: #ababab; }    table td { border: 1px solid #ababab; } .b-w { border: 1px solid #fff; }</style>";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc style=\"text-align: center;\">".$cmpname."</h1><h2 class=fc style=\"text-align: center;\">";
	if(strlen($rhead)>0) {
		$pagea.=$rhead;
	} else {
		$pagea.= "SDBIP ".$modtxt.": $page_title Report";
	}
	$pagea.= "</b></h2>";
	$table[1] = "<table cellpadding=3 cellspacing=0 width=100%>";
	$table[9] = "</table>";
	$pagez = "<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	break;
}
//arrPrint($field);
//START PAGE
$echo = $pagea;
//GROUP LOOP
$gtotal = array();
foreach($glist as $g) {

	$groupresults = array();	//reset summary/group array
	$obj2 = $objects[$g['id']];	//get kpis for group
		if(count($gtotal)>0 && $field['results'] == "Y") {
			$group_total = "Y";
			$echo.= $rowa;
			for($c=1;$c<$gspan;$c++) {
				$echo.=$cella[22][9]."".$cellz[2];
			}
			$echo.= $cella[22][9]."Total:".$cellz[2];
			$gbudget = 0;
			$gactual = 0;
			foreach($time as $tim) {
				$timeid = $tim['id'];
				if($field['crresult']=="Y") {
					$echo.= $cella[22][9].number_format($gtotal[$timeid]['b'],2,".",$thou_sep).$cellz[2];
					$echo.= $cella[22][9].number_format($gtotal[$timeid]['a'],2,".",$thou_sep).$cellz[2];
				}
				if($field['crytd']=="Y") {
					$echo.= $cella[22][9]."".$cellz[2];
					$echo.= $cella[22][9]."".$cellz[2];
				}
				$gbudget+=$gtotal[$timeid]['b'];
				$gactual+=$gtotal[$timeid]['a'];
			}
			$echo.= $cella[22][90].number_format($gbudget,2,".",$thou_sep).$cellz[2];
			$echo.= $cella[22][90].number_format($gactual,2,".",$thou_sep).$cellz[2];
			$echo.= $rowz;
		}
if(count($obj2)>0) {	//if there are kpis for the group
	if($g['id']!=0) {	//display the group title
		$echo.= $table[9].$gcell[1].decode($g['value']).$gcell[9].$table[1];
	} else {
		$echo.= $table[1];
	}
//TABLE HEADINGS
	$echo.= $rowa;
	$echo.= $cella[12]."Ref".$cellz[12];
	if($field['dir']=="Y") { $echo.= $cella[12]."Directorate".$cellz[12]; }
	if($field['sub']=="Y") { $echo.= $cella[12]."Sub-Directorate".$cellz[12]; }
	foreach($head as $hrow)
	{
		if($field[$hrow['headfield']] == "Y") { $echo.= $cella[12].decode($hrow['headlist']).$cellz[12]; }
	}
	if($field['results'] == "Y")
	{
		if($c2 > 0) {
			foreach($time as $tim) {
				$echo.= $cella[10].date("F-Y",$tim['eval']).$cellz[10];
				if($output == "csv") {
					for($c=1;$c<$c2;$c++) {
						$echo.=$cella[1]." ".$cellz[1];
					}
				}
			}
		}
		$echo.= $cella[11]."Total Spend for ".date("M Y",$time[$cf]['sval']);
		if($cf!=$ct) { $echo.=" - ".date("M Y",$time[$ct]['eval']); }
		$echo.=$cellz[11];
	}
	$echo.=$rowz;
	if($field['results'] == "Y")
	{
		$echo.=$rowa;
		if($output == "csv") {
			for($c=1;$c<=$gspan;$c++) {
				$echo.=$cella[1]." ".$cellz[1];
			}
		}
		for($t=1;$t<=count($time);$t++)
		{
			if($field['crresult']=="Y") {
				$echo.=$cella[1]."Budget".$cellz[1];
				$echo.=$cella[1]."Actual".$cellz[1];
			}
			if($field['crytd']=="Y") {
				$echo.=$cella[1]."Total % Spent YTD".$cellz[1];
				$echo.=$cella[1]."Total R Spent YTD".$cellz[1];
			}
			/*if($field['krmanage']=="Y") {
				$echo.=$cella[1]."Management Comment".$cellz[1];
			}*/
		}
		$echo.=$cella[1]."Budget".$cellz[1];
		$echo.=$cella[1]."Actual".$cellz[1];
		$echo.=$rowz;
	}

//TABLE ROWS
	foreach($obj2 as $obj)
	{
		$objid = $obj[$object_id];
		if(isset($results[$objid])) {
		$echo.= $rowa;
		$echo.= $cella[2].$objid.$cellz[2];
		if($field['dir']=="Y") { $echo.= $cella[2].decode($obj['dirtxt']).$cellz[2]; }
		if($field['sub']=="Y") { $echo.= $cella[2].decode($obj['subtxt']).$cellz[2]; }
		foreach($head as $hrow)
		{
			$hfield = $hrow['headfield'];
			if($field[$hfield] == "Y") { 
				$echo.= $cella[2];
				if($dlist[$hfield]['yn']=="Y") {
					$k = $dlist[$hfield]['kpi']; //$echo.=$k;
					$dl = $dlist[$hfield]['data'];
					$echo.= decode($dl[$obj[$k]]['value']);
				} else {
                        switch($hrow['headfield'])
                        {
                            case "area":
                                $sql2 = "SELECT w.* FROM ".$dbref."_".$object_table."_area cw, ".$dbref."_list_area w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = cw.caareaid AND cw.cayn = 'Y' AND cw.cacpid = ".$objid;
                                $sql2.= " ORDER BY w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $echo.= decode($row2['value']."; ");
                                    }
                                mysql_close($con2);
                                $echo.= (" ");
                                break;
                            case "fundsource":
                                $sql2 = "SELECT w.* FROM ".$dbref."_".$object_table."_source cw, ".$dbref."_list_fundsource w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = cw.cssrcid AND cw.csyn = 'Y' AND cw.cscpid = ".$objid;
                                $sql2.= " ORDER BY w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $echo.= decode($row2['value']."; ");
                                    }
                                mysql_close($con2);
                                $echo.= (" ");
                                break;
                            case "wards":
								$wecho = "";
                                $sql2 = "SELECT w.* FROM ".$dbref."_".$object_table."_wards cw, ".$dbref."_list_wards w";
                                $sql2.= " WHERE w.yn = 'Y' AND w.id = cw.cwwardsid AND cw.cwyn = 'Y' AND cw.cwcpid = ".$objid;
                                $sql2.= " ORDER BY w.numval, w.value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
										if($row2['numval']>0) {
											$wecho .= $row2['numval']."; ";
										} else {
											$wecho.= decode($row2['value']."; ");
										}
                                    }
                                mysql_close($con2);
                                $echo.= $wecho;
                                break;
							case "cpstartdate":
							case "cpenddate":
								$v = $obj[$hfield];
								$echo.=date("d M Y",$v);
								break;
							case "cpstartactual":
							case "cpendactual":
								$v = $obj[$hfield];
								if(checkIntRef($v)) {
									$echo.=date("d M Y",$v);
								} else {
									$echo.="";
								}
								break;
							case "progress":
								$sql2 = "SELECT krprogress, krtimeid FROM ".$dbref."_kpi_result INNER JOIN ".$dbref."_kpi ON krkpiid = kpiid AND kpicpid = $objid WHERE krprogress <> '' ORDER BY krtimeid DESC LIMIT 1";
								include("inc_db_con2.php");
									$row2 = mysql_fetch_array($rs2);
								mysql_close($con2);
								if($output == "display" && strlen($row2['krprogress'])>0) {
									$echo.="<small>".decode($row2['krprogress']); 
									$echo.="<br /><small><i>(".date("M Y",mktime(12,0,0,($krtimeid)+6,1,2010)).")</i></small></small>";
								} elseif($output != "display" && strlen($row2['krprogress'])>0) {
									$echo.=decode($row2['krprogress']); 
									$echo.=" (".date("M Y",mktime(12,0,0,($row2['krtimeid'])+6,1,2010)).")";
								}
								break;
                            default:
                                $echo.= decode($obj[$hrow['headfield']])." ";
                                break;
                        }
				}
				$echo.= $cellz[2];
			}
		}
		if($field['results'] == "Y")
		{
			$res = $results[$objid];
			foreach($time as $tim)
			{
				$tid = $tim['id'];
				if($field['crresult']=="Y") {
					$echo.=$cella[22][0].$res[$tid]['b'].$cellz[2];
					$echo.=$cella[22][0].$res[$tid]['a'].$cellz[2];
				}
				if($field['crytd']=="Y") {
					$echo.=$cella[22][0].$res[$tid]['p'].$cellz[2];
					$echo.=$cella[22][0].$res[$tid]['r'].$cellz[2];
				}
			}
			$echo.=$cella[21].$res['b'].$cellz[21];
			$echo.=$cella[21].$res['a'].$cellz[21];
			$echo.=$rowz;
		}
		}//isset results
	}
/*	if($krsum == "Y" && $field['results'] == "Y" && $g['id']!=0) {
		$echo.= drawKRSum('title0',$g['value'],0,0);
		foreach($krarray as $kra) {
			if($kra['filter']=="Y") { 
				$echo.= drawKRSum('row',$krsetup[$kra['sort']]['value'],$groupresults[$kra['sort']],$kra['sort']);
			}
		}
		$echo.= drawKRSum('sum','Total KPIs',array_sum($groupresults),0);
	}*/
	} //end if group(kpis)>0
	$gtotal = $g['res'];	$group_total = "N";
} //end foreach group
	//total for final group
	if($group_total == "N" && count($gtotal) > 0 && $field['results']=="Y") {
			$echo.= $rowa;
			for($c=1;$c<$gspan;$c++) {
				$echo.=$cella[22][9]."".$cellz[2];
			}
			$echo.= $cella[22][9]."Total:".$cellz[2];
			$gbudget = 0;
			$gactual = 0;
			foreach($time as $tim) {
				$timeid = $tim['id'];
				if($field['crresult']=="Y") {
					$echo.= $cella[22][9].number_format($gtotal[$timeid]['b'],2,".",$thou_sep).$cellz[2];
					$echo.= $cella[22][9].number_format($gtotal[$timeid]['a'],2,".",$thou_sep).$cellz[2];
				}
				if($field['crytd']=="Y") {
					$echo.= $cella[22][9]."".$cellz[2];
					$echo.= $cella[22][9]."".$cellz[2];
				}
				$gbudget+=$gtotal[$timeid]['b'];
				$gactual+=$gtotal[$timeid]['a'];
			}
			$echo.= $cella[22][90].number_format($gbudget,2,".",$thou_sep).$cellz[2];
			$echo.= $cella[22][90].number_format($gactual,2,".",$thou_sep).$cellz[2];
			$echo.= $rowz;
	}
/*if($krsum == "Y" && $field['results'] == "Y") {
	$echo.= drawKRSum('title','',0,0);
	foreach($krarray as $kra) {
			if($kra['filter']=="Y") { 
				$echo.= drawKRSum('row',$krsetup[$kra['sort']]['value'],$totals[0][$kra['sort']],$kra['sort']);
			}
	}
	$echo.= drawKRSum('sum','Total KPIs',array_sum($totals[0]),0);
}*/

/*if($sum == "Y") {
	$echo.= drawSum('title','',0,0);
	foreach($glist as $g) {
		$echo.= drawSum('line',$g['value'],$g['res'],0);
	}
	$echo.= drawSum('sum','Total'
	
}*/
	
//END PAGE
$echo.= $table[9].$pagez;

//DISPLAY OUTPUT
switch($output)
{
	case "csv":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/".$object_table."_".date("Ymd_Hi",$today).".csv";
        $newfilename = "capital_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'text/plain';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("download.php");
		break;
	case "excel":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../files/".$cmpcode."/".$chkloc."/".$object_table."_".date("Ymd_Hi",$today).".xml";
        $newfilename = "capital_report_".date("Ymd_Hi",$today).".xls";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'application/ms-excel';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("download.php");
		break;
	default:
		echo $echo;
		break;
}
?>
