<html lang="en">
<head>
	<title>jQuery UI Progressbar - Default functionality</title>

		<link type="text/css" href="lib/jquery-ui-1.7.1.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="lib/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="lib/jquery-ui-1.7.1.custom.min.js"></script>

		<script type="text/javascript">
			$(function(){


				// Progressbar
				$("#progressbar").progressbar({
					value: 20
				});


			});
		</script>
</head>
<body>

<div class="demo">

	<div id="progressbar"></div>

</div><!-- End demo -->



<div class="demo-description">

<p>Default determinate progress bar.</p>

</div><!-- End demo-description -->
<?php
$p = $_GET['p'];
if(strlen($p)==0) { $p = 10; }
echo($p);
echo("<script type=text/javascript>$(\"#progressbar\").progressbar({ value: ".$p."	});</script>");
?>
</body>
</html>
