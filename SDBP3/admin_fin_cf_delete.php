<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>


<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Monthly Cashflows ~ Delete Line Item</b></h1>
<p>&nbsp;</p>
<?php
$lineid = $_GET['l'];
$linemnr = 0;
if(is_numeric($lineid) && $lineid > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_lineitems WHERE lineid = ".$lineid." AND lineyn = 'Y' AND linetype = 'CF'";
    include("inc_db_con.php");
        $linemnr = mysql_num_rows($rs);
        $line = mysql_fetch_array($rs);
    mysql_close();

}

if($linemnr > 0)
{
        $sql = "UPDATE assist_".$cmpcode."_".$modref."_finance_lineitems SET lineyn = 'N' WHERE lineid = ".$lineid;
        include("inc_db_con.php");
        echo("<p>Line item '".$line['linevalue']."' successfully deleted.</p>");
        $urlback = "admin_fin_cf_setup.php";
        include("inc_goback.php");
        $tsql = $sql;
        $told = "";
        $trans = "Deleted line item ".$lineid;
        include("inc_transaction_log.php");
}
else
{
    echo("<p>An error has occurred.  Please go back and try again.</p>");
    $urlback = "admin_fin_cf_setup.php";
    include("inc_goback.php");
}

?>
<p>&nbsp;</p>
</body>
</html>
