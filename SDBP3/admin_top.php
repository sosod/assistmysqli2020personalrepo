<?php
    include("inc_ignite.php");
	
	$sql = "SELECT * FROM ".$dbref."_top WHERE doclocation <> '' ORDER BY id DESC LIMIT 1";
	include("inc_db_con.php");
		$top = mysql_fetch_assoc($rs);
	mysql_close($con);
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
	table th { text-align:left; }
</style>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Admin ~ Top Level Document</h1>
<?php if(count($top)>2 && strlen($top['doclocation'])>0 && file_exists("..".$top['doclocation'])) { ?>
<h2>Current Document</h2>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<th>Document Name:</th>
		<td><?php echo $top['docname']; ?></td>
	</tr>
	<tr>
		<th>Uploaded by:</th>
		<td><?php echo getTK($top['adduser'],$cmpcode,'tkn'); ?></td>
	</tr>
	<tr>
		<th>Uploaded on:</th>
		<td><?php echo $top['adddate']; ?></td>
	</tr>
	<tr>
		<th>File size:</th>
		<td><?php echo formatBytes(filesize("..".$top['doclocation'])); ?></td>
	</tr>
</table>
<p><input type=button value="Delete Current Document" onclick="document.location.href = 'admin_top_process.php?act=del';"></p>
<?php } ?>
<h2>Update Document</h2>
<form name=top action=admin_top_process.php method=post enctype="multipart/form-data"><input type=hidden name=act value=update>
	<input type=file name=ifile>
	<input type=submit value="Update" style="margin-right: 10px;">
<p style="padding-left: 0px; color: #CC0001;">Please note that this will overwrite the current document.</p>
</form>
<?php
$urlback = "admin.php";
include("inc_goback.php");
?>
<h2 style="color: #909090">History</h2>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdloghead>Ref</td>
        <td class=tdloghead>Date</td>
        <td class=tdloghead>User</td>
        <td class=tdloghead>Document Uploaded</td>
    </tr>
    <?php
    $sql = "SELECT * FROM ".$dbref."_top ORDER BY id DESC";
    include("inc_db_con.php");
	while($row = mysql_fetch_array($rs))
	{
		echo "<tr>";
			echo "<td class=tdlog style=\"text-align:center\">".$row['id']."</td>";
			echo "<td class=tdlog>".$row['adddate']."</td>";
			echo "<td class=tdlog>".getTK($row['adduser'],$cmpcode,'tkn')."</td>";
			echo "<td class=tdlog>".$row['docname']."</td>";
		echo "</tr>";
	}
	mysql_close($con);
    ?>
</table>
<?php
$urlback = "admin.php";
include("inc_goback.php");
?>

<p>&nbsp;</p>
</body>

</html>
