<?php
    include("inc_ignite.php");
//	error_reporting(-1);
//SET DEFAULTS FOR PAGE
	$graph = "";
	$group = array();
	$time = array();
	$data_type = strFn("substr",$_REQUEST['type'],5,3);
	switch($data_type) {
		case "TOP":
			$graph = "PIE";
			$title = "Top Level Dashboard Report";
			$sql = "SELECT * FROM ".$dbref."_headings WHERE headtype = 'TL' AND ( headfield NOT LIKE 'top%'  ) AND headfield NOT IN ('wards','area') ORDER BY headdetailsort";
			include("inc_db_con.php");
				while($row = mysql_fetch_array($rs)) {
					$group[$row['headfield']] = array($row['headfield'],$row['headdetail']);
				}
			mysql_close($con);
			$sql = "SELECT * FROM ".$dbref."_list_time ORDER BY sort";
			include("inc_db_con.php");
				$active = "";  $current = "N"; $found_current = "N";
				while($row = mysql_fetch_array($rs)) {
					if($row['id']/3==ceil($row['id']/3)) {
						$val = "Quarter ending ".date("d M Y",$row['eval']);
						if(($row['active'] == "Y" && $row['sval']<$today) || $active != "NN") { $val.= "*"; }
						$active = "";
						if($row['id']==12 && $found_current == "N") { $current = "Y"; }
						$time[$row['id']] = array($row['id'],$val,$current);
						$current = "N";
					} else {
						if($row['sval']<$today) {
							$active.=$row['active'];
						} else {
							$active.="N";
						}
						if($row['sval']< $today && $row['eval'] > $today) { $current = "Y"; $found_current = "Y"; }
					}
				}
			mysql_close($con);
			break;
		case "CF":
			$graph = "CF_BAR";
			$title = "Monthly Cashflow Dashboard Report";
			$group['gfs'] = array('gfs','GFS Classification');
			break;
		case "KPI":
		default:
			$graph = "PIE";
			$title = "Departmental KPI Dashboard Report";
			$sql = "SELECT * FROM ".$dbref."_headings WHERE headtype = 'KPI' AND ( headfield NOT LIKE 'kpi%'  ) AND headfield NOT IN ('wards','area','ktype') ORDER BY headdetailsort";
			include("inc_db_con.php");
				while($row = mysql_fetch_array($rs)) {
					$group[$row['headfield']] = array($row['headfield'],$row['headdetail']);
				}
			mysql_close($con);
			break;
	}
	if($data_type!="TOP") {
		$sql = "SELECT * FROM ".$dbref."_list_time WHERE yn = 'Y' ORDER BY sort";
		include("inc_db_con.php");
			$found_current = "N"; $current = "N";
			while($row = mysql_fetch_array($rs)) {
				$val = date("M-Y",$row['eval']);
				if($row['active'] == "Y" && $row['sval']<$today) { $val.= "*"; }
				if(($row['sval']<$today && $row['eval']>$today) || ($row['id']==12 && $found_current == "N")) {
					$current = "Y";
					$found_current = "Y";
				}
				$time[$row['id']] = array($row['id'],$val,$current);
				$current = "N";
			}
		mysql_close($con);
	}
//SET GENERATOR HEADINGS
	$sec = 1;
//	$sections = array("","Graph type selected:","Select the filter you wish to apply:","Select the grouping you wish to apply:","Select the page layout:","Generate the Report:");
	$sections = array("","Select the filter you wish to apply:","Select the grouping you wish to apply:","Select the page layout:","Generate the Report:");
	
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
    table {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
		margin-left: 20px;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
		padding: 5 5 5 5;
    }
	.filter {
		vertical-align: top;
		padding: 10 10 10 3;
	}
	.head {
		font-weight: bold;
		vertical-align: top;
	}
	img { cursor: hand; vertical-align: middle; }
</style>
<base target="main">
<script type=text/javascript>
function gotoHelp(aname) {
	window.open('help_report_dashboard.php#'+aname);
}
function changeLayout() {
	var groupby = document.getElementById('grp').value;
	var layoutE = document.getElementById('layout');
	var lo1E = document.getElementById('lo1');
	var lo2E = document.getElementById('lo2');
	var lo1aE = document.getElementById('lo1a');
	var lo2aE = document.getElementById('lo2a');
	var ggE = document.getElementById('gg');
	if(groupby=="X") {	//No grouping
		layoutE.disabled = true;
		lo1E.disabled = true;
		lo1aE.disabled = true;
		lo2E.disabled = true;
		lo2aE.disabled = true;
		ggE.disabled = true;
	} else {
		ggE.disabled = false;
		if(ggE.value == "pie") {
			lo1aE.innerText = 'Above the secondary graph(s)';
			lo2aE.innerText = 'Inline with the secondary graph(s)';
			layoutE.disabled = false;
			if(layoutE.value == 1) {
				lo1E.disabled = true;
				lo1aE.disabled = true;
				lo2E.disabled = true;
				lo2aE.disabled = true;
			} else {
				lo1E.disabled = false;
				lo1aE.disabled = false;
				lo2E.disabled = false;
				lo2aE.disabled = false;
			}
		} else {
			lo1aE.innerText = 'Above the secondary graph(s) (Portrait)';
			lo2aE.innerText = 'Inline with the secondary graph(s) (Landscape)';
			layoutE.disabled = true;
			lo1E.disabled = false;
			lo1aE.disabled = false;
			lo2E.disabled = false;
			lo2aE.disabled = false;
		}
	}
}

function changeDirSub(me) {	//changing directorate/subdirectorate changes group.options[1]
	var dirsub = me.value;
	var grpE = document.getElementById('grp');
	var group = grpE.value;
	
	if(dirsub == "ALL") {
		if(grpE.options[1].value != "dir") {
			var c = grpE.options.length;
			for(d=c;d>1;d--) {
				grpE.options[d] = new Option(grpE.options[d-1].text, grpE.options[d-1].value, false, false);
			}
		}
		grpE.options[1] = new Option("Directorate","dir",true,false);
		grpE.value = group;
	} else if(isNaN(parseInt(me.value)) && document.getElementById('dt').value!="TOP") {	//Dir
		if(grpE.options[1].value != "dir") {
			var c = grpE.options.length;
			for(d=c;d>1;d--) {
				grpE.options[d] = new Option(grpE.options[d-1].text, grpE.options[d-1].value, false, false);
			}
		}
		grpE.options[1] = new Option("Sub-Directorate","dir",true,false);
		grpE.value=group;
	} else {								//Sub
		if(grpE.options[1].value == "dir") {
			grpE.options[1] = null;
			if(group == "dir") {
				grpE.value = "X";
			}
		}
	}


}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc>SDBIP <?php echo $modtxt.": ".$title; ?></h1>
<form name=view action=report_dashboard_process.php method=POST>
<?php 
echo chr(10)."<input type=hidden name=data_type value='$data_type' id=dt>";
echo chr(10)."<input type=hidden name=graph value='$graph'>";
/*************** GRAPH + LAYOUT ********************//*
echo chr(10)."<h3>".$sec.". ".$sections[$sec]."</h3>"; $sec++; 
echo chr(10)."<table>";
echo chr(10)."	<tr>";
echo chr(10)."		<td class=head style=\"vertical-align: middle\">$title</td>";
if($data_type=="KPI" || $data_type == "TOP") {
	echo chr(10)."		<td><img src=\"lib/images/pie.gif\" id=kpipie style=\"vertical-align: top\"></td>";
} else {
	echo chr(10)."		<td><img src=\"lib/images/bar.gif\" id=finbar style=\"vertical-align: top\"></td>";
}
echo chr(10)."	</tr>";
echo chr(10)."</table>";


/************ FILTER ******************/
echo chr(10)."<h3>".$sec.". ".$sections[$sec]."</h3>"; $sec++; 
echo chr(10)."<table>";
if($data_type!="TOP") {
	echo chr(10)."	<tr>";
	echo chr(10)."		<td class=head>[Sub-]Directorate:</td>";
	echo chr(10)."		<td><select name=dirsub onchange=\"changeDirSub(this);\" id=dirsub>";
	echo chr(10)."		<option selected value=ALL>All Directorates</option>";
		$dir = array();
		$sql = "SELECT * FROM ".$dbref."_dir d INNER JOIN ".$dbref."_dirsub s ON d.dirid = s.subdirid ";
		$sql.= " WHERE d.diryn = 'Y' AND s.subyn = 'Y' ";
		if($data_type=="CF") {
			$sql.= " AND s.subid IN (SELECT DISTINCT linesubid FROM ".$dbref."_finance_lineitems WHERE lineyn = 'Y' AND linetype = 'CF')";
		} else {
			$sql.= " AND s.subid IN (SELECT DISTINCT kpisubid FROM ".$dbref."_kpi WHERE kpiyn = 'Y')";
		}
		$sql.= " ORDER BY d.dirsort, s.subsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_array($rs)) {
				$d = $row['dirid'];
				$dt = $row['dirtxt'];
				$s = $row['subid'];
				$st = $row['subtxt'];
				$dir[$d]['id'] = $d;
				$dir[$d]['txt'] = $dt;
				$dir[$d]['subs'][] = array('id'=>$s,'txt'=>$st);
			}
		mysql_close($con);
		foreach($dir as $d) {
			echo chr(10)."<option value=D_".$d['id'].">".$d['txt']."</option>";
			foreach($d['subs'] as $s) {
				echo chr(10)."<option value=".$s['id'].">&nbsp;&nbsp;-&nbsp;".$s['txt']."</option>";
			}
		}
	echo chr(10)."		</select></td>";
	echo chr(10)."	</tr>";
} else {
	echo chr(10)."	<tr>";
	echo chr(10)."		<td class=head>Directorate:</td>";
	echo chr(10)."		<td><select name=dirsub id=dirsub onchange=\"changeDirSub(this);\">";
	echo chr(10)."		<option selected value=ALL>All Directorates</option>";
		$dir = array();
		$sql = "SELECT DISTINCT d.* FROM ".$dbref."_dir d INNER JOIN ".$dbref."_toplevel t ON t.topsubid = d.dirid";
		$sql.= " WHERE d.diryn = 'Y' ";
		$sql.= " AND t.topyn = 'Y' ";
		$sql.= " ORDER BY d.dirsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_array($rs)) {
				$d = $row['dirid'];
				$dt = $row['dirtxt'];
				$dir[$d]['id'] = $d;
				$dir[$d]['txt'] = $dt;
			}
		mysql_close($con);
		foreach($dir as $d) {
			echo chr(10)."<option value=D_".$d['id'].">".$d['txt']."</option>";
		}
	echo chr(10)."		</select></td>";
	echo chr(10)."	</tr>";
}
echo chr(10)."	<tr>";
echo chr(10)."		<td class=head>Time Period:</td>";
echo chr(10)."		<td>From: <select name=from id=tf>";
			foreach($time as $t) {
				echo chr(10)."<option value=".$t[0].">".$t[1]."</option>";
			}
echo chr(10)."		</select> to <select name=to id=tt>";
			foreach($time as $t) {
				echo chr(10)."<option ";
				if($t[2]=="Y") { echo chr(10)."selected "; }
				echo chr(10)."value=".$t[0].">".$t[1]."</option>";
			}
echo chr(10)."		</select><br /><i><small>* indicates time periods which are still open for updating.</small></i></td>";
echo chr(10)."	</tr>";
if($data_type!="CF") {
	echo chr(10)."	<tr>";
	echo chr(10)."		<td class=head>KPI Filter: <img src=/pics/help.gif onclick=\"gotoHelp('kpina');\"></td>";
	echo chr(10)."		<td id=lo><select name=kpifilter>";
	echo chr(10)."			<option selected value='exclude'>Exclude</option>";
	echo chr(10)."			<option value='include'>Include</option>";
	echo chr(10)."		</select> KPIs Not Measured<br /><span style='font-size: 6.5pt; font-style: italic;'>(KPIs with no targets/actuals prior to the end of selected time period)</span></td>";
	echo chr(10)."	</tr>";
	if($data_type!="TOP") {
		echo chr(10)."	<tr>";
		echo chr(10)."		<td class=head>&nbsp;</td>";
		echo chr(10)."		<td id=lo><select name=cpfilter>";
		echo chr(10)."			<option selected value='ALL'>All KPIs</option>";
		echo chr(10)."			<option value='OP'>Operational KPIs only</option>";
		echo chr(10)."			<option value='CP'>Capital Project KPIs only</option>";
		echo chr(10)."		</select></td>";
		echo chr(10)."	</tr>";
	}
	echo chr(10)."	<tr>";
	echo chr(10)."		<td class=head>KPI's Met: <img src=../pics/help.gif onclick=\"gotoHelp('kpimet');\"></td>";
	echo chr(10)."		<td id=lo><select name=kpimet>";
	echo chr(10)."			<option selected value='separate'>Separate</option>";
	echo chr(10)."			<option value='combine'>Combine</option>";
	echo chr(10)."		</select> <span style='background-color: #009900;'>&nbsp;&nbsp;</span> KPIs Met,";
	echo chr(10)." <span style='background-color: #006600;'>&nbsp;&nbsp;</span> KPIs Well Met and";
	echo chr(10)." <span style='background-color: #000099;'>&nbsp;&nbsp;</span> KPIs Extremely Well Met</td>";
	echo chr(10)."	</tr>";
}
echo chr(10)."</table>";


/**************** GROUP BY *****************/
echo chr(10)."<h3>".$sec.". ".$sections[$sec]."</h3>"; $sec++; 
echo chr(10)."<table>";
echo chr(10)."	<tr>";
echo chr(10)."		<td class=head>Group By:</td>";
echo chr(10)."		<td><select name=groupby id=grp onchange='changeLayout();'>";
echo chr(10)."			<option selected value=X>No Grouping</option>";
echo chr(10)."			<option value=dir>Directorate</option>";
foreach($group as $g) {
	echo chr(10)."<option value=".$g[0].">".$g[1]."</option>";
}
echo chr(10)."		</select></td>";
echo chr(10)."	</tr>";
if($data_type!="CF") {
echo chr(10)."	<tr>";
echo chr(10)."		<td class=head>Graph:</td>";
echo chr(10)."		<td><select name=group_graph id=gg onchange='changeLayout();'>";
echo chr(10)."			<option value=pie>Individual pie graphs</option>";
echo chr(10)."			<option value=stack>Single bar chart</option>";
echo chr(10)."		</select></td>";
echo chr(10)."	</tr>";
} else {
	echo chr(10)."<input type=hidden name=group_graph id=gg value=pie>";
}
echo chr(10)."</table>";

/**************** LAYOUT *****************/
echo chr(10)."<h3>".$sec.". ".$sections[$sec]."</h3>"; $sec++; 
echo chr(10)."<p style='font-size: 6.5pt; margin-top: -10px;margin-left: 20px;'><i>Please note that Layout options are dependent on the grouping selected.</i></p>";
echo chr(10)."<table>";
echo chr(10)."	<tr>";
if($data_type!="CF") {
	echo chr(10)."		<td class=head>Page Layout: <img src=/pics/help.gif onclick=\"gotoHelp('layout_page_kpi');\"></td>";
	echo chr(10)."		<td>";
	echo chr(10)."			<select name=layout_page id=layout onchange='changeLayout();'>";
		echo chr(10)."				<option selected value=1>1 graph per line</option>";
		echo chr(10)."				<option value=2>2 graphs per line (portrait)</option>";
		echo chr(10)."				<option value=3>3 graphs per line (landscape)</option>";
} else {
	echo chr(10)."		<td class=head>Page Layout: <img src=/pics/help.gif onclick=\"gotoHelp('layout_page_cf');\"></td>";
	echo chr(10)."		<td>";
	echo chr(10)."			<select name=layout_page id=layout onchange='changeLayout();'>";
		echo chr(10)."				<option selected value=1>1 graph per line (portrait)</option>";
		echo chr(10)."				<option value=2>2 graphs per line (landscape)</option>";
}
echo chr(10)."			</select> ";
echo chr(10)."		</td>";
echo chr(10)."	</tr>";
echo chr(10)."	<tr>";
echo chr(10)."		<td class=head>Primary graph: <img src=/pics/help.gif onclick=\"gotoHelp('layout_overall');\"></td>";
echo chr(10)."		<td id=lo>";
echo chr(10)."			<input type=radio name=layout_overall checked id=lo1 value=above> <label id=lo1a for=lo1>Above the secondary graph(s)</label><br />";
echo chr(10)."			<input type=radio name=layout_overall id=lo2 value=inline> <label id=lo2a for=lo2>Inline with the secondary graph(s)</label>";
echo chr(10)."		</td>";
echo chr(10)."	</tr>";
echo chr(10)."</table>";


/**************** GO *****************/
echo chr(10)."<h3>".$sec.". ".$sections[$sec]."</h3>"; $sec++; 
echo chr(10)."<table>";
echo chr(10)."	<tr>";
echo chr(10)."		<td><b>Report Title:</b> <input type=text name=rhead value=\"\" maxlength=100 size=70> <i><small>(Displays at the top of the report.)</small></i></td>";
echo chr(10)."	</tr>";
echo chr(10)."	<tr>";
echo chr(10)."		<td><input type=submit value=\"Generate Report\"></td>";
echo chr(10)."	</tr>";
echo chr(10)."</table>";
echo chr(10)."</form>";
echo chr(10)."<table  style=\"border: 1px solid #AAAAAA;\">";
echo chr(10)."<tr>";
echo chr(10)."<td style=\"font-size:7pt;border:1px solid #AAAAAA; font-style: italic; line-height: 10pt;\">";
echo chr(10)."<b>Note:</b> In order to print the Dashboard Report to PDF, you need to have a PDF printer installed on your computer.";
echo chr(10)."<br />Please contact your IT department or the Ignite Helpdesk for more information.</td>";
echo chr(10)."</tr></table>";

$urlback = "report.php";
include("inc_goback.php");

?>
<p>&nbsp;</p>
<script type=text/javascript>
document.getElementById('tf').options[0].selected;
changeLayout();
</script>
</body>
</html>
