<?php
    include("inc_ignite.php");
    
    $val = $_POST['val'];
    $ps = $_POST['ps'];
    
    if(strlen($val)==1 && $ps > 0 && $ps < 4)
    {
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$val."' WHERE ref = '".strtoupper($modref)."' AND refid = ".$ps;
        include("inc_db_con.php");
        if($ps==2)
        {
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_plcphases_std SET yn = '".$val."' WHERE type = 'P'";
            include("inc_db_con.php");
        }
        else
        {
            if($ps==3)
            {
                $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_plcphases_std SET yn = '".$val."' WHERE type = 'A'";
                include("inc_db_con.php");
            }
        }
    }
    
    
    
    
    include("inc_admin.php");
    
    function fldyn($p)
    {
        global $plcsetup;
        $yn = $plcsetup[$p];
        echo("<option ");
        if($yn != "N") { echo("selected"); }
        echo(" value=Y>Yes</option><option ");
        if($yn == "N") { echo("selected"); }
        echo(" value=N>No</option>");
    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function updatePLC(p) {
    var fld = "plc"+p;
    var target = document.getElementById(fld).value;
    if(target != "Y" && target != "N") { target = "Y"; }
    document.getElementById('val').value = target;
    document.getElementById('ps').value = p;
    document.forms['plc'].submit();
    
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Project Life Cycle</b></h1>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=570>
	<?php include("inc_tr.php"); ?>
		<td width=175 class=tdheaderl height=27>Use Project Life Cycle?</td>
		<td class=tdgeneral>&nbsp;&nbsp;<select id=plc1><?php fldyn(1); ?></select>&nbsp;</td>
		<td width=100 class=tdgeneral align=center><input type=button value="  Update  " onclick="updatePLC(1)"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=tdheaderl height=27>Standard Phases</td>
		<td class=tdgeneral>Configure Standard Phases used in PLC&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure id=c1 onclick="document.location.href = 'setup_plc_std.php';"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=tdheaderl height=27>Use Procurement Phases?</td>
		<td class=tdgeneral>&nbsp;&nbsp;<select id=plc2><?php fldyn(2); ?></select>&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value="  Update  " onclick="updatePLC(2)" id=u2></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=tdheaderl height=27>Procurement Phases</td>
		<td class=tdgeneral>Configure Procurement Phases used in PLC&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure id=c2 onclick="document.location.href = 'setup_plc_proc.php';"></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=tdheaderl height=27>Allow Additional Phases?</td>
		<td class=tdgeneral>&nbsp;&nbsp;<select id=plc3><?php fldyn(3); ?></select>&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value="  Update  " onclick="updatePLC(3)" id=u3></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=tdheaderl height=27>Additional Phases</td>
		<td class=tdgeneral>Configure Additional Phases added by users&nbsp;</td>
		<td class=tdgeneral align=center><input type=button value=Configure id=c3 onclick="document.location.href = 'setup_plc_custom.php';"></td>
	</tr>
</table>
<form name=plc method=post action=setup_plc.php>
<input type=hidden name=val id=val>
<input type=hidden name=ps id=ps>
</form>
<script type=text/javascript>
<?php
    if($plcsetup[1]=="N")
    {
        echo("document.getElementById('c1').disabled = true;");
        echo("document.getElementById('c2').disabled = true;");
        echo("document.getElementById('c3').disabled = true;");
        echo("document.getElementById('u2').disabled = true;");
        echo("document.getElementById('u3').disabled = true;");
        echo("document.getElementById('plc2').disabled = true;");
        echo("document.getElementById('plc3').disabled = true;");
        echo("document.getElementById('plc2').style.backgroundColor = 'dedede';");
        echo("document.getElementById('plc3').style.backgroundColor = 'dedede';");
    }
    else
    {
        if($plcsetup[2]=="N")
        {
            echo("document.getElementById('c2').disabled = true;");
        }
        if($plcsetup[3]=="N")
        {
            echo("document.getElementById('c3').disabled = true;");
        }
    }
?>
</script>
<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
