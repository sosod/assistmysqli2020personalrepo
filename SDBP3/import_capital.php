<?php
    include("inc_ignite.php");
	
$act = $_REQUEST['act'];
$fileloc = $_REQUEST['fl'];
$dtformat = $_REQUEST['dtformat'];

if($dtformat!="NUM") {
	$dft1 = strFn("explode",$dtformat,"/","");
	$d = 0;
	foreach($dft1 as $df) { $dformat[strtoupper($df)] = $d; $d++; }
} else {
	$dformat = "NUM";
}
//print_r($dformat);
$sql = "SELECT subid, subtxt FROM ".$dbref."_dirsub";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$sub[$row['subid']] = $row['subtxt'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_gfs";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$gfs[$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_area";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$area[$row['id']] = $row['value'];
	}
mysql_close($con);
$sql = "SELECT id, value, numval FROM ".$dbref."_list_wards";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$wards[$row['numval']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_fundsource";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$fundsrc[$row['id']] = $row['value'];
	}
mysql_close($con);
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Import - Capital Projects</b></h1>
<?php
if(strlen($fileloc)==0)
{
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                die("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                die("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                die("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
        $err = "Y";
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
        if(strtolower($ext)!="csv")
        {
            die("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
            $err = "Y";
        }
        else
        {
            $filename = substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
set_time_limit(30);
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }
}

if(file_exists($fileloc)==false)
{
    die("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
    $err = "Y";
}

if($err != "Y")
{
            $file = fopen($fileloc,"r");
            $data = array();
	set_time_limit(30);
            while(!feof($file))
            {
                $tmpdata = fgetcsv($file);
//				echo "<P>".count($tmpdata)." - "; print_r($tmpdata);
                if(count($tmpdata)>1)
                {
                    $data[] = $tmpdata;
                }
                $tmpdata = array();
            }
            fclose($file);
	set_time_limit(30);
} else {
	die("<P>Erryn = 'Y'"); 
}

if(count($data)>0)
{
	//print_r($data);
	set_time_limit(1800);
	echo "<form action=import_capital.php method=post><input type=hidden name=fl value=\"".$fileloc."\"><input type=hidden name=dtformat value=\"$dtformat\"><input type=hidden name=act value=sbmt>";
	echo "<table cellpadding=3>";
		$dt = $data[0];
		echo "<tr><td width=30>&nbsp;</td>";
		foreach($dt as $d) {
			echo "<th>".$d."</th>";
		}
		echo "</tr>";
			$cp = 1;
		for($a=1;$a<count($data);$a++) {
			$err = "N";
			$csql = array();
			$dt = $data[$a];
			$cpid = $dt[0];
			$dt[0] .= "-".$cp;
			$cpsubid = $dt[1];
			$dt[1].= ": ".$sub[$cpsubid];
			$cpgfsid = $dt[3];
			$dt[3].= ": ".$gfs[$cpgfsid];
			$fsrc = $dt[9];
				//CHECK FUNDING SRC
				$fs = strFn("explode",$fsrc,";","");
				foreach($fs as $f) {
					if(!checkIntRef($f) || strlen($fundsrc[$f])==0) {
						$err = "Y";
					} else {
						$csql[] = "INSERT INTO ".$dbref."_capital_source (cscpid,cssrcid,csyn) VALUES ($cpid,$f,'Y')";
						$dt[9].= "<br />".$fundsrc[$f];
					}
				}
			$wds = $dt[14];
				//CHECK WARDS
				$ws = strFn("explode",$wds,";","");
				foreach($ws as $w) {
					if(!is_numeric($w) || strlen($w)==0 || strlen($wards[$w]['value'])==0) {
						$err = "Y";
					} else {
						$csql[] = "INSERT INTO ".$dbref."_capital_wards (cwcpid,cwwardsid,cwyn) VALUES ($cpid,".$wards[$w]['id'].",'Y')";
						$dt[14].= "<br />".$wards[$w]['numval']."-".$wards[$w]['id'].": ".$wards[$w]['value'];
					}
				}
			$ara = $dt[15];
				//CHECK FUNDING SRC
				$fs = strFn("explode",$ara,";","");
				foreach($fs as $f) {
					if(!checkIntRef($f) || strlen($area[$f])==0) {
						$err = "Y";
					} else {
						$csql[] = "INSERT INTO ".$dbref."_capital_area (cacpid,caareaid,cayn) VALUES ($cpid,$f,'Y')";
						$dt[15].= "<br />".$area[$f];
					}
				}
			$cpstart = $dt[10];
				if(strlen($cpdate)==0 || $cpdate == 0) {
					$cpstartdate = mktime(12,0,0,7,1,2010);
					$dt[10].=" - ".$cpstartdate." - ".date("d M Y",$cpstartdate);
				} elseif($dformat=="NUM") {
				} else {
					$cpsdt = strFn("explode",$cpstart,"/","");
					$D = $cpsdt[$dformat['D']];
					$M = $cpsdt[$dformat['M']];
					$Y = $cpsdt[$dformat['Y']];
//					echo "<P> $cpid - "; print_r($cpsdt); echo " - $D $M $Y";
					$cpstartdate = mktime(12,0,0,$M,$D,$Y);
					$dt[10].=" - ".$cpstartdate." - ".date("d M Y",$cpstartdate);
				}
			$cpdate = $dt[11];
				if(strlen($cpdate)==0 || $cpdate == 0) {
					$cpenddate = mktime(12,0,0,6,30,2010);
					$dt[11].=" - ".$cpenddate." - ".date("d M Y",$cpenddate);
				} elseif($dformat=="NUM") {
				} else {
					$cpdt = strFn("explode",$cpdate,"/","");
					$D = $cpdt[$dformat['D']];
					$M = $cpdt[$dformat['M']];
					$Y = $cpdt[$dformat['Y']];
//					echo "<P> $cpid - "; print_r($cpsdt); echo " - $D $M $Y";
					$cpenddate = mktime(12,0,0,$M,$D,$Y);
					$dt[11].=" - ".$cpenddate." - ".date("d M Y",$cpenddate);
				}
			$cpdate = $dt[12];
				if(strlen($cpdate)==0 || $cpdate == 0) {
					$cpstartactual = 0;
					$dt[12].=" - N/A";
				} elseif($dformat=="NUM") {
				} else {
					$cpdt = strFn("explode",$cpdate,"/","");
					$D = $cpdt[$dformat['D']];
					$M = $cpdt[$dformat['M']];
					$Y = $cpdt[$dformat['Y']];
					$cpstartactual = mktime(12,0,0,$M,$D,$Y);
					$dt[12].=" - ".$cpstartactual." - ".date("d M Y",$cpstartactual);
				}
			$cpdate = $dt[13];
				if(strlen($cpdate)==0 || $cpdate == 0) {
					$cpendactual = 0;
					$dt[13].=" - N/A";
				} elseif($dformat=="NUM") {
				} else {
					$cpdt = strFn("explode",$cpdate,"/","");
					$D = $cpdt[$dformat['D']];
					$M = $cpdt[$dformat['M']];
					$Y = $cpdt[$dformat['Y']];
					$cpendactual = mktime(12,0,0,$M,$D,$Y);
					$dt[13].=" - ".$cpendactual." - ".date("d M Y",$cpendactual);
				}
			$csql[] = "INSERT INTO ".$dbref."_capital (cpid,cpsubid,cpgfsid,cpnum,cpidpnum,cpvotenum,cpname,cpproject,cpstartdate,cpenddate,cpstartactual,cpendactual,cpyn,cpfundsourceid) VALUES ($cpid,$cpsubid,$cpgfsid,'".code($dt[4])."','".code($dt[5])."','".code($dt[6])."','".code($dt[7])."','".code($dt[8])."',$cpstartdate,$cpenddate,$cpstartactual,$cpendactual,'Y',0)";
			for($t=1;$t<13;$t++) {
				$t2 = $t + 15;
				$b = $dt[$t2];
				if(!is_numeric($b) || strlen($b)==0) { $b = 0; }
				$csql[] = "INSERT INTO ".$dbref."_capital_results (crcpid, crbudget, crtimeid) VALUES ($cpid,$b,$t)";
			}
			$t2 = 29;
			for($y=1;$y<6;$y++) {
				$crr = $dt[$t2]; $t2++; if(!is_numeric($crr) || strlen($crr)==0) { $crr = 0; }
				$oth = $dt[$t2]; $t2++; if(!is_numeric($oth) || strlen($oth)==0) { $oth = 0; }
				$csql[] = "INSERT INTO ".$dbref."_capital_forecast (cfcpid,cfcrr,cfother,cfyearid) VALUES ($cpid,$crr,$oth,$y)";
			}
			if($act=="sbmt" && !(!checkIntRef($cpid) || $cpid != $cp || !checkIntRef($cpsubid) || strlen($sub[$cpsubid])==0 || !checkIntRef($cpgfsid) || strlen($gfs[$cpgfsid])==0 || $err == "Y")) {
				foreach($csql as $sql) {
					if(strlen($sql) > 0) {
						include("inc_db_con.php");
					}
				}
				$class = "tdheadergreen";
			} elseif(!checkIntRef($cpid) || $cpid != $cp || !checkIntRef($cpsubid) || strlen($sub[$cpsubid])==0 || !checkIntRef($cpgfsid) || strlen($gfs[$cpgfsid])==0 || $err == "Y") {
				$class = "tdheaderred"; 
//				$csql = array();
			} else {
				$class = "tdheaderblue"; 
			}
			echo "<tr>";
			echo "<td class=$class >&nbsp;</td>";
			foreach($dt as $d) {
				echo "<td>".$d."</td>";
			}
			echo "</tr>";
			if($act!="sbmt") {
				echo "<tr><td colspan=50>".strFn("implode",$csql,"<br />","")."</td></tr>";
			}
			$cp++;
		}
	echo "</table>";
	if($act != "sbmt") {
		echo "<p><input type=submit></p>";
	} else {
		echo "<P>".count($csql)." SQL queries run!</p>";
	}
	echo "</form>";
} else {
	echo "<P>NO DATA";
}

$urlback = "import.php";
include("inc_goback.php");

?>

</body>

</html>
