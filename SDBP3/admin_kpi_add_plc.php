<?php
    include("inc_ignite.php");

$reqd = array();

function appYN($c,$yn) {
    global $reqd;
    if($yn == "R")
    {
        echo("Required"."<input type=hidden name=app[] id=app".$c." value=Y>");
        $reqd[$c] = $c;
    }
    else
    {
        echo("<select size=1 name=app[] id=app".$c." onchange=\"usePhase(this,".$c."); targetChange(".$c.");\"><option ");
        if($yn != "N") {    echo("selected");    }
        echo(" value=\"Y\">Yes</option><option ");
        if($yn == "N") {    echo("selected");    }
        echo(" value=\"N\">No</option></select>");
    }
}

include("inc_admin.php");

$procstart = 0;
$procend = 0;
$add1 = 0;
$add2 = 0;

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<?php include("inc_head_msie.php"); ?>
<script type=text/javascript src="lib/inc_plc.js"></script>
<style type=text/css>
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<script type="text/javascript">
$(function(){
    $('.datepicker').datepicker({
        showOn: 'both',
        buttonImage: 'calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd',
        changeMonth:true,
        changeYear:true
    });
});
</script>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin ~ Add KPI</b></h1>
<?php
$kpiid = $_POST['kpiid'];
$dirid = $_POST['dirid'];
$subid = $_POST['subid'];
$typ = $_POST['typ'];
$src = $_POST['src'];
$plcid = $_POST['plcid'];
if(strlen($kpiid)>0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiid = ".$kpiid;
    include("inc_db_con.php");
        $kpirow = mysql_fetch_array($rs);
    mysql_close();
}
$plcrow = array();
$phase = array();
if(strlen($plcid)>0 && is_numeric($plcid))
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc WHERE plcid = ".$plcid;
    include("inc_db_con.php");
        $plcrow = mysql_fetch_array($rs);
    mysql_close();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_phases WHERE ppplcid = ".$plcid;
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $phase[$row['pptype']][$row['ppphaseid']] = $row;
        }
    mysql_close();
}

if(count($plcrow)>0)
{
    $plcstartdate = date("Y-m-d",$plcrow['plcstartdate']);
    $plcenddate = date("Y-m-d",$plcrow['plcenddate']);
    $plcname = $plcrow['plcname'];
    $plcdescrip = $plcrow['plcdescription'];
    $proccate = $plcrow['plcproccate'];
    if(strlen($proccate)!=1) { $proccate = 1; }
}
else
{
    $plcstartdate = date("Y-m-d",$today);
    $plcenddate = "";
    $plcname = $kpirow['kpivalue'];
    $plcdescrip = $kpirow['kpidefinition'];
    $proccate = 1;
}

/*echo("<P>kpi-");
print_r($kpirow);
echo("<P>plc-");
print_r($plcrow);
echo("<P>phaseS-");
print_r($phase['S']);
echo("<P>phaseP-");
print_r($phase['P']);
echo("<P>phaseC-");
print_r($phase['C']);
echo("<P>phaseL-");
print_r($phase['L']);
*/ ?>
<h2 class=fc>Project Life Cycle</h2>
<form name=plc method=post action=admin_kpi_add_plc_process.php onsubmit="return Validate();">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid value="<?php echo($subid); ?>">
<input type=hidden name=typ value="<?php echo($type); ?>">
<input type=hidden name=src value="<?php echo($src); ?>">
<input type=hidden name=kpiid value="<?php echo($kpiid); ?>">
<input type=hidden name=plcid value="<?php echo($plcid); ?>">
<table border="0" cellspacing="0" cellpadding="3" style="border: 0px solid #ffffff;">
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>KPI Number:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><?php echo($kpiid); ?></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Name:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><input type="text" name="plcprojectname" size="20" value="<?php echo($plcname); ?>" maxlength=100></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Project Description:&nbsp;</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><input type="text" name="plcprojectdescription" size="56" value="<?php echo($plcdescrip); ?>" maxlength=150></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>Start Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><input type="text" name="plcstartdate" size="20" value="<?php echo($plcstartdate); ?>" readonly="readonly" id=plcstartdate class=datepicker onchange="startDate(0);"></td>
	</tr>
	<tr>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><b>End Date:</b></td>
		<td class=tdgeneral style="border: 0px solid #ffffff;"><input type="text" name="plcenddate" size="20" value="<?php echo($plcenddate); ?>" readonly="readonly" id=plcenddate></td>
	</tr>
</table>
<p style="margin: 0 0 0 0;">&nbsp;</p>
<table border="1" cellspacing="0" cellpadding="3" width=100%>
	<tr>
		<td height=27 colspan="2" class="tdheader"><b>Project Phase</b></td>
		<td width=100 class="tdheader" align="center"><b>Use Phase?</b></td>
		<td width=100 class="tdheader"><b>Calendar<br>Days</b></td>
		<td class="tdheader"><b>Start<br>Date</b></td>
		<td class="tdheader"><b>End<br>Date</b></td>
		<td class="tdheader"><b>% Target<br>of Project</b></td>
	</tr>
<?php
    $c=1;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_std WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $prow = array();
            switch($row['type'])
            {
    case "A":
    $no1 = $c + 1;
//CUSTOM PHASES
    $c2 = $c;
    $add1 = $c;
    $fc=0;
    $phaseC = $phase['C'];
    foreach($phaseC as $pC)
    {
        if($fc==9)
        {
            $s = 28;
            $w = 25;
        }
        else
        {
            $s = 30;
            $w = 18;
        }
?>
	<tr>
        <?php if($c2==$c) { ?>
		<td valign=top rowspan=10 class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
		<?php } ?>
		<td class="tdgeneral"><i><?php echo($fc+1); ?>.</i> <input type=hidden name=phasetype[] value=C id=ptype<?php echo($c); ?>>
            <select name=phaseid[] onchange="changePhase(this,<?php echo($c); ?>);" id=phase<?php echo($c); ?>>
                <option selected value=X>-- SELECT --</option>
                <option value=NEW>Add new</option>
                <?php
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_custom WHERE yn = 'Y' AND dirid = ".$dirid." ORDER BY value";
                include("inc_db_con2.php");
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo("<option ");
                        if($pC['ppphaseid']==$row2['id']) { echo(" selected "); }
                        echo(" value=".$row2['id'].">".$row2['value']."</option>");
                    }
                mysql_close($con2);
                ?>
            </select><?php echo("<span id=new".$c."><br><input type=text name=custom[] onchange=\"newPhase(this,".$c.")\" id=cust".$c." size=".$s." style=\"margin-left: ".$w."\" maxlength=100></span>"); ?>
        </td>
		<td class="tdgeneral" align="center"><?php appYN($c,"Y"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=\"".$pC['ppdays']."\" onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"".date("Y-m-d",$pC['ppstartdate'])."\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"".date("Y-m-d",$pC['ppenddate'])."\" readonly=\"readonly\" id=end".$c." class=datepicker>"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"".$pC['pptarget']."\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
	</tr>
<?php
        $c++;
        $fc++;
    }

    for($f=$fc;$f<10;$f++)
    {
        if($f==9)
        {
            $s = 28;
            $w = 25;
        }
        else
        {
            $s = 30;
            $w = 18;
        }
?>
	<tr>
        <?php if($c2==$c) { ?>
		<td valign=top rowspan=10 class="tdgeneral"><b><?php echo($row['value']); ?>:</b></td>
		<?php } ?>
		<td class="tdgeneral"><i><?php echo($f+1); ?>.</i> <input type=hidden name=phasetype[] value=C id=ptype<?php echo($c); ?>>
            <select name=phaseid[] onchange="changePhase(this,<?php echo($c); ?>);" id=phase<?php echo($c); ?>>
                <option selected value=X>-- SELECT --</option>
                <option value=NEW>Add new</option>
                <?php
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_custom WHERE yn = 'Y' AND dirid = ".$dirid." ORDER BY value";
                include("inc_db_con2.php");
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo("<option value=".$row2['id'].">".$row2['value']."</option>");
                    }
                mysql_close($con2);
                ?>
            </select><?php echo("<span id=new".$c."><br><input type=text name=custom[] onchange=\"newPhase(this,".$c.")\" id=cust".$c." size=".$s." style=\"margin-left: ".$w."\" maxlength=100></span>"); ?>
        </td>
		<td class="tdgeneral" align="center"><?php if($f==0) { appYN($c,"Y"); } else {  appYN($c,"N");  } ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=1 onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"\" readonly=\"readonly\" id=end".$c." class=datepicker>"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"0\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
	</tr>
<?php
        $c++;
    }
    $add2 = $c;
    $no2 = $c;
    break;
            case "P":
            //PROCUREMENT PHASES
                $c2 = $c;
                $days = array();
                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_plcphases_proc WHERE yn = 'Y' OR yn = 'R' ORDER BY sort";
                include("inc_db_con2.php");
                $mnr2 = mysql_num_rows($rs2);
                $r=0;
                $procstart = $c;
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        $prow = $phase[$row['type']][$row2['id']];
                        if(count($prow)>0)
                        {
                            $pcal = $prow['ppdays'];
                            $pstart = date("Y-m-d",$prow['ppstartdate']);
                            $pend = date("Y-m-d",$prow['ppenddate']);
                            $ptarg = $prow['pptarget'];
                            $pyn = "Y";
                        }
                        else
                        {
                            $pcal = 1;
                            $pstart = "";
                            $pend = "";
                            $ptarg = 0;
                            if(count($plcrow)>0) { $pyn = "N"; } else { $pyn = $row['yn']; }
                        }
                        $r++;
                        $days[1][$c] = $row2['days1'];
                        $days[2][$c] = $row2['days2'];
                        $days[3][$c] = $row2['days3'];
                        $days[4][$c] = $row2['days4'];
                        if(strlen($pstart)==0)
                        {
                            $pcal = $row2['days'.$proccate];
                        }
                if($row2['yn'] == "R") { $pyn = "R"; }
?>
	<tr>
        <?php if($c2==$c) { ?>
		<td valign=top rowspan=<?php echo($mnr2); ?> class="tdgeneral"><b><?php echo($row['value']); ?>:
<?php if($plcsetup[4]!="N") { ?>
        <br>
        <i>Project Category: <select name=proccate onchange="procCate(this)">
        <?php
        for($f=1;$f<5;$f++) {
            echo("<option ");
            if($proccate == $f) { echo("selected"); }
            echo(" value=".$f.">".$f."</option>");
         } ?>
        </select></i><?php } else { ?><input type=hidden name=proccate value=1><?php } ?></b></td>
        <?php } ?>
		<td class="tdgeneral"><i><?php echo($row2['value']); ?>:</i></td>
		<td class="tdgeneral" align="center"><?php appYN($c,$pyn); ?><input type=hidden name=phaseid[] value="<?php echo($row2['id']); ?>"><input type=hidden name=phasetype[] value="<?php echo($row['type']); ?>" id="ptype<?php echo($c); ?>"></td>
		<?php if($plcsetup[5]!="N") { ?>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=\"".$pcal."\" onchange=\"startDate(".$c.")\">"); ?></td>
		<?php } else { ?>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=\"".$pcal."\" onchange=\"startDate(".$c.")\" readonly=readonly style=\"background-color: dddddd;\">"); ?></td>
		<?php } ?>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"".$pstart."\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"".$pend."\" readonly=\"readonly\" id=end".$c." class=datepicker>"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"".$ptarg."\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
	</tr>
<?php
                        if($r<$mnr2) { $c++; }
                    }
                mysql_close($con2);
                $procend = $c;
                break;
                default:
                //STANDARD PHASES WHERE TYPE = S
                $prow = $phase[$row['type']][$row['id']];
                if(count($prow)>0)
                {
                    $pcal = $prow['ppdays'];
                    $pstart = date("Y-m-d",$prow['ppstartdate']);
                    $pend = date("Y-m-d",$prow['ppenddate']);
                    $ptarg = $prow['pptarget'];
                    $pyn = "Y";
                }
                else
                {
                    $pcal = 1;
                    $pstart = "";
                    $pend = "";
                    $ptarg = 0;
                    if(count($plcrow)>0) { $pyn = "N"; } else { $pyn = $row['yn']; }
                }
                if($row['yn'] == "R") { $pyn = "R"; }
?>
	<tr>
		<td colspan="2" class="tdgeneral"><b><?php echo($row['value']); ?>:</b><input type=hidden name=phaseid[] value="<?php echo($row['id']); ?>"><input type=hidden name=phasetype[] value="<?php echo($row['type']); ?>" id="ptype<?php echo($c); ?>"></td>
		<td class="tdgeneral" align="center"><?php appYN($c,$pyn); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"cal[]\" size=\"5\" id=\"cal".$c."\" value=".$pcal." onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"startdate[]\" size=\"10\" value=\"".$pstart."\" readonly=\"readonly\" id=start".$c." class=datepicker onchange=\"startDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"enddate[]\" size=\"10\" value=\"".$pend."\" readonly=\"readonly\" id=end".$c." class=datepicker onchange=\"endDate(".$c.")\">"); ?></td>
		<td class="tdgeneral" align="center"><?php echo("<input type=\"text\" name=\"target[]\" size=\"5\" value=\"".$ptarg."\" id=tar".$c." onchange=\"targetChange(".$c.")\">"); ?> %</td>
	</tr>
<?php
                break;
            }
            if($row['type']!="A") {            $c++;    }
        }
    mysql_close();
?>
	<tr>
		<td height=27 colspan="2" class="tdgeneral"><b>Target % of Project Total:</b></td>
		<td class="tdgeneral" align="center" colspan=4></td>
		<td class="tdgeneral" align="center"><label id=tartot style="font-weight: bold;">0</label> %</td>
	</tr>
</table>
<input type=hidden name=c id=c value=<?php echo($c); ?>>
<script type=text/javascript>
<?php echo("var c = ".$c.";"); ?>
<?php echo("var cust1 = ".$add1.";"); ?>
<?php echo("var cust11 = ".$add2.";"); ?>
document.getElementById('plcenddate').style.color = "777777";
var apyn;
var doctype = "add";
<?php
echo(chr(10)."var procstart = ".$procstart.";");
$procend++;
echo(chr(10)."var procend = ".$procend.";");
echo(chr(10)."var days = new Array();");
$dayskeys = array_keys($days);
$d1 = 1;
foreach($days as $d)
{
    echo(chr(10)."days[".$d1."] = new Array(".implode($d,",").");");
    $d1++;
}
echo(chr(10)."var req = new Array(".implode($reqd,",").");");
for($e=1;$e<$c;$e++)
{
    echo(chr(10)."var apptarg = document.getElementById('app".$e."');");
    echo(chr(10)."usePhase(apptarg,".$e.");");
}
echo(chr(10));
?>
</script>
<?php //print_r($required); ?>
<p style="font-size: 1pt; line-height: 1pt;">&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="3" width=100%>
    <tr>
        <td class=tdgeneral align=center><input type=submit value="Step --> 3" id=subbut> <input type=reset></td>
    </tr>
</table>
</form>
<script type=text/javascript>
var targ = document.getElementById('app1');
targetChange(1);
startDate(0);
</script>
</body>

</html>
