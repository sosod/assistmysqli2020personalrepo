<?php
    include("inc_ignite.php");
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - KPI Administrators ~ Add</b></h1>
<p>&nbsp;</p>
<?php
//get form data
$tki = $_POST['tkid'];

//validate in timekeep
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_menu_modules_users u, assist_".$cmpcode."_timekeep t WHERE u.usrmodref = '".strtoupper($modref)."' AND u.usrtkid = t.tkid AND t.tkstatus = 1 AND t.tkid = '".$tki."' ";
$sql.= " AND t.tkid NOT IN (SELECT a.tkid FROM assist_".$cmpcode."_".$modref."_list_admins a WHERE a.yn = 'Y' AND a.type = 'KPI')";
$sql.= " ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
    $rnum = mysql_num_rows($rs);
    $row = mysql_fetch_array($rs);
mysql_close();
    $value = $row['tkname']." ".$row['tksurname'];

if($rnum == 1)
{
//if exists:
    //insert into list_admins
    $sql = "INSERT INTO assist_".$cmpcode."_".$modref."_list_admins SET ";
    $sql.= "  tkid = '".$tki."'";
    $sql.= ", yn = 'Y'";
    $sql.= ", type = 'KPI'";
    $sql.= ", ref = 0";
    include("inc_db_con.php");
        $aid = mysql_insert_id();
$tsql = $sql;
    //insert into sdbip log
        $logref = $aid;
        $logtype = "SAKPI";
        $logaction = "Added KPI administrator '".$value."'";
        $logdisplay = "Y";
        include("inc_log.php");
    //insert into transaction log
$told = "";
$trans = $logaction." with id ".$aid;
include("inc_transaction_log.php");
    //display result
    ?>
    <p>New KPI Administrator '<?php echo($value); ?>' has been successfully added.</p>
    <p>&nbsp;</p>
    <?php
    $urlback = "setup_admin_kpi.php";
    include("inc_goback.php");
    ?>
<?php
}
else
{
//else
    //display error
        ?>
        <p>An error has occurred (Err: SDBIP24).  Please go back and try again.</p>
        <p>&nbsp;</p>
        <?php
        $urlback = "setup_admin_kpi_add.php";
        include("inc_goback.php");
        ?>
        <?php
}
?>

<p>&nbsp;</p>
</body>
</html>
