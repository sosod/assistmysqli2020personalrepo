<?php
    include("inc_ignite.php");
	unset($dlist['prog']);
	unset($dlist['kpistratop']);
	unset($dlist['kpiriskrate']);
	unset($dlist['ktype']);
	unset($dlist['kpitargettype']);
	//prog kpistratop kpiriskrate ktype
$dlist['topcalctype']=array('name'=>"KPI Calculation Type",'id'=>"kpicalctype",'code'=>"Y",'yn'=>"Y",'headfield'=>"topcalctype",'kpi'=>"kpicalctype",'fld'=>"code");
$dlist['toptargettype']=array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"toptargettype",'kpi'=>"toptargettypeid");
$dlist['idp']=array('name'=>"IDP Goal",'id'=>"idpgoal",'code'=>"N",'fld'=>"code",'yn'=>"Y",'headfield'=>"idp",'kpi'=>"topidpid");
foreach($dlist as $dl) {
		$dld = getListKPI($dl);
		$dlist[$dl['headfield']]['data'] = $dld;
}

$nosort = array("wards","area","topbaseline","topcalctype","toptargettype","toptargetannual","toptargetrevised","toppastyear","trcorrective");
$nogroup = array("toppmsref","topunit","toprisk");

$head = array();
$head['sub'] = array('headfield'=>"sub",'headdetail'=>"Directorate");
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'TL' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
        $head[$row['headfield']] = $row;
	}
mysql_close($con);
$head['toptargetannual'] = array('headdetail'=>"Annual Target",'headfield'=>"toptargetannual");
$head['toptargetrevised'] = array('headdetail'=>"Revised Target",'headfield'=>"toptargetrevised");
$head['toptargettype'] = array(
	'headtype'=>'TL',
	'headyn'=>'Y',
	'headdetail'=>'Target Type',
	'headfield'=>'toptargettype',
	'headmaxlen'=>-1
);

/* RESET VALUES = > no report action & no existing report */
function resetFields(&$field,&$filter,&$sort,&$dlistfilter,&$group) {
	global $head;
	global $nosort;
	global $dlist;
	global $timearr;
		$group = "X";
		foreach($head as $h) {
			$hf = $h['headfield'];
			$field[$hf] = array($hf,"Y");
			if($hf == "kpimsr") {
				$filter[$hf] = array($hf,"X","");
			} elseif($dlist[$hf]['yn']=="Y" && count($dlist[$hf]['data'])<6 && $hf != "natkpa" && $hf != "munkpa") {
				foreach($dlist[$hf]['data'] as $d) {
					$dlistfilter[$hf][] = $d['id'];
				}
				$filter[$hf] = array($hf,strFn("implode",$dlistfilter[$hf],"|",""),"any");
			} elseif($hf=="wards" || $hf == "area") {
				$filter[$hf] = array($hf,"X","any");
				$dlistfilter[$hf][0] = "X";
			} else {
				$filter[$hf] = array($hf,"","any");
			}
			if(!in_array($hf,$nosort)) {
				$sort[] = $hf;
			}
		}
		$filter['kpiresult'] = array("kpiresult","ALL","");
		//$field['krgloss'] = array("krgloss","Y");
		//$field['krsum'] = array("krsum","Y");
		$reshead = array("results","krresult","krcomment","krsdbipcomment","krsum","krgloss");
		foreach($reshead as $hf) {
			$field[$hf] = array($hf,"Y");
		}
		$field['cf'] = array("cf",3);
		if($today>$timearr[12]['eval']) {
			$ct = 12; 
		} else {
			$ct = date("m");
			if($ct > 6 ) $ct-=6; else $ct+=6;
			$ct = ceil($ct/3)*3;
		}
		$field['ct'] = array("ct",$ct);
/*	global $head;
	global $nosort;
	global $dlist;
	$group = "X";
		/* no report action & no existing report */
/*		foreach($head as $h) {
			$hf = $h['headfield'];
			$field[$hf] = array($hf,"Y");
			$filter[$hf] = array($hf,"","");
			if(!in_array($hf,$nosort)) {
				$sort[] = $hf;
			}
		}
		$reshead = array("results","krresult","krcomment","krsdbipcomment","krsum","krgloss");
		foreach($reshead as $hf) {
			$field[$hf] = array($hf,"Y");
		}
		$field['cf'] = array("cf",3);
		if($today>$timearr[12]['eval']) {
			$ct = 12; 
		} else {
			$ct = date("m");
			if($ct > 6 ) $ct-=6; else $ct+=6;
			$ct = ceil($ct/3)*3;
		}
		$field['ct'] = array("ct",$ct);
*/}


//echo "<pre>";
$result = array();
$field = array();
$filter = array();
$sort = array();
$var = $_REQUEST;
//print_r($var);
if($var['act']=="SAVE" || $var['act']=="EDIT") {
	/**************** SAVE NEW REPORT ***********************/
	//format    field: headfield1|=|value1|_|     filter: headfield|=|filter|=|filtertype|_|
	$field = array();
	$filter = array();
	$field['sub'] = array("sub",$var['sub']);
	$field['results'] = array("results",$var['results']);
	$field['krresult'] = array("krresult",$var['krresult']);
	$field['krcomment'] = array("krcomment",$var['krcomment']);
	$field['krsdbipcomment'] = array("krmanage",$var['krsdbipcomment']);
	$field['cf'] = array("cf",$var['cf']); //results from
	$field['ct'] = array("ct",$var['ct']); //results to
	$field['krsum'] = array("krsum",$var['krsum']);
	$filter['sub'] = array("sub",$var['subfilter']);
	$sort = $var['sort'];
	$sortby = strFn("implode",$sort,"|_|","");
	$output = $var['output'];
	$groupby = $var['groupby'];
	$name = code($var['rname']);
	$rhead = code($var['rhead']);
	$field['krsum'] = array("krsum",$var['krsum']);
	$filter['kpiresult'] = array("kpiresult",$var['kpiresultfilter'],"");
	$field['krgloss'] = array("krgloss",$var['krgloss']);
	foreach($head as $row) {
		$hf = $row['headfield'];
		$field[$hf] = array($hf,$var[$hf]);
		if($dlist[$hf]['yn']=="Y" || $hf == "wards" || $hf == "area") {
			$filter[$hf] = array();
			$filter[$hf][0] = $hf;
			$filter[$hf][1] = strFn("implode",$var[$hf.'filter'],"|","");
			$filter[$hf][2] = $var[$hf.'filtertype'];
			$dlistfilter[$hf] = $var[$hf.'filter'];
		} else {
			$filter[$hf] = array($hf,code($var[$hf.'filter']),$var[$hf.'filtertype']);
		}
    }
	//CONVERT FROM ARRAY TO STRING
	$field2 = array();
	$filter2 = array();
	foreach($field as $f) {
		$field2[] = strFn("implode",$f,"|=|","");
	}
	$fields = strFn("implode",$field2,"|_|","");
	foreach($filter as $f) {
		$filter2[] = strFn("implode",$f,"|=|","");
	}
	$filters = strFn("implode",$filter2,"|_|","");
	//SAVE TO DB
	if($var['act']=="EDIT") {
		if(checkIntRef($var['i'])) {
			$sql = "UPDATE ".$dbref."_reports SET rname = '$name', rhead = '$rhead', fields = '$fields', filters = '$filters', sort_by = '$sortby', group_by = '$groupby', export_to = '$output', modified_on = $today";
			$sql.= " WHERE id = ".$var['i'];
			include("inc_db_con.php");
			$result[0] = "check";
			$result[1] = "Top Level KPI Report '$name' has been updated. <input type=submit value=\"Generate Report\" onclick=\"document.kpireport.submit();\">";
			$var['a'] = "E";
			$report['id'] = $var['i'];
			$report['rname'] = $name;
			$report['rhead'] = $rhead;
		} else {
			$result[0] = "error";
			$result[1] = "An error occurred trying to update report '$name'.";
		}
	} else {
		$sql = "INSERT INTO ".$dbref."_reports (tkid,rtype,rname,rhead,fields,filters,sort_by,group_by,export_to,yn,added_on,modified_on) ";
		$sql.= "VALUES ('$tkid','TL','$name','$rhead','$fields','$filters','$sortby','$groupby','$output','Y',$today,0)";
		include("inc_db_con.php");
		$report['id'] = mysql_insert_id($con);
		$report['rname'] = $name;
		$report['rhead'] = $rhead;
		$var['a'] = "E";
		$result[0] = "check";
		$result[1] = "Top Level KPI Report '$name' has been saved. <input type=submit value=\"Generate Report\" onclick=\"document.kpireport.submit();\">";
	}
} elseif($var['act']=="DEL" && checkIntRef($var['i'])) {
	$name = $var['rname'];
			$sql = "UPDATE ".$dbref."_reports SET yn = 'N', modified_on = $today";
			$sql.= " WHERE id = ".$var['i'];
			include("inc_db_con.php");
			$result[0] = "check";
			$result[1] = "Top Level KPI Report '$name' has been deleted.";
		/******************* SET DEFAULT INFO ************************/
		/* no report action & no existing report */
		resetFields($field,$filter,$sort,$dlistfilter,$groupby);
} else {
	if(checkIntRef($var['i'])) {
		/**************** GET REPORT DETAIL ********************/
		$report = array();
		$sql = "SELECT * FROM ".$dbref."_reports WHERE id = ".$var['i'];
		include("inc_db_con.php");
			$report = mysql_fetch_assoc($rs);
		mysql_close($con);
		if(count($report)==0) {
			$result[0] = "error";
			$result[1] = "Report not found.";
			$var['a'] = "E";
			$var['i'] = 0;
		} else {
			//CONVERT DB INFO INTO ARRAYS
			$fields = $report['fields'];
			$filters = $report['filters'];
			$sort = $report['sort_by'];
			$sort = explode("|_|",$sort);
			$output = $report['export_to'];
			$groupby = $report['group_by'];
			//FIELD
			$fields2 = strFn("explode",$fields,"|_|","");
			foreach($fields2 as $f) {
				$f2 = strFn("explode",$f,"|=|","");
				$field[$f2[0]] = array($f2[0],$f2[1]);
			}
			//FILTER & FILTERTYPE
			$filter2 = strFn("explode",$filters,"|_|","");
			foreach($filter2 as $f) {
				$f2 = strFn("explode",$f,"|=|","");
				$filter[$f2[0]] = array($f2[0],$f2[1],$f2[2]);
				if($dlist[$f2[0]]['yn']=="Y" || $f2[0] == "wards" || $f2[0] == "area") {
					$dlistfilter[$f2[0]] = strFn("explode",$f2[1],"|","");
				}
			}
		}
	} else {
		/******************* SET DEFAULT INFO ************************/
		resetFields($field,$filter,$sort,$dlistfilter,$groupby);
	}
}
//echo "<pre>"; 
//print_r($filter);
//print_r($field);
//print_r($head); 
//print_r($sort);
//print_r($dlistfilter);
//echo "</pre>";
//echo "<P>".$filters;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<?php include("inc_head_msie.php"); ?>
<style type=text/css>
    table {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 0px solid #ababab;
    }
	.b-w {
		border: 0px solid #ffffff;
	}
	.filter {
		vertical-align: top;
		padding: 10 10 10 3;
	}
	.head {
		font-weight: bold;
	}
</style>
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.0em; height: 18px; width: 200px;}
	#sortable li span { position: absolute; margin-left: -1.3em; }
	.ui-state-highlightsort { height: 1.5em; line-height: 1.2em; }
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({
			placeholder: "ui-state-highlightsort"
		});
		$( "#sortable" ).disableSelection();
	});
	</script>
<script type=text/javascript>
function dirSub(me) {
        var dir = me.value;
        if(dir.length>0 && !isNaN(parseInt(dir)))
        {
            var sub = document.getElementById('sf');
            sub.length = 0;
            var sf = subs[dir];
            var o = 1;
            var ds = false;
            var opt = new Array();
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,true);
            for(s in sf)
            {
                sub.options[o] = new Option(sf[s][1],sf[s][0],ds,ds);
                o++;
            }
        }
}
function checks(c,e) {
	var act;
	if(c=="S") {
	} else {
		if(c=="U") { act = false; } else { act = true; }
		for(i=0;i<e;i++) {	if(document.forms[0].elements[i].nodeName!='SELECT') { document.forms[0].elements[i].checked = act; } }
	}
}
function saveReport(act) {
		switch(act) {
		case "D":
			document.kpireport.act.value = "DEL";
			break;
		case "E":
			document.kpireport.act.value = "EDIT";
			break;
		case "N":	//NEW REPORT
		default:
			document.kpireport.act.value = "SAVE";
		}
		document.kpireport.action = "report_tl.php";
		document.kpireport.submit();
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Report - Top Level</b></h1>
<?php 
	displayRes($result); 
	
	if($var['a']=="V") {
		echo "<p style=\"text-align: center; font-weight: bold; font-size: 16pt;\">Generating report....</p>";
	}
	
?>

<form name=kpireport method=post action=report_tl_process.php>
<?php
/*********** 1. Fields *********/
//echo count($head);
echo "<h3 class=fc>1. Select the information you want in your report:</h3>";
echo "<div style=\"margin-left: 17px\">";
echo "<table cellpadding=3 cellspacing=0>";
    echo "<tr>";
        echo "<td valign=top style=\"border-bottom: 0px\">";
            echo "<table cellpadding=3 cellspacing=0  style=\"border-width: 0px\">";
	$mnr = count($head)-1+6; //echo ":".$mnr.":";
    $m2 = $mnr / 3; //echo ":".$m2.":";
    $m = round($m2);  //echo ":".$m.":";
    if($m < $m2) { $m++; }  //echo ":".$m.":";
    $r=0;
	foreach($head as $row)
    {	if($row['headfield']!="toptargettype" && $row['headfield']!="trcorrective") {
        $r++;
        if($r > $m) {
            $r=1;
			//CLOSE PREVIOUS COLUMN
            echo "</table>";
			echo "</td>";
			//OPEN NEW COLUMN
			echo "<td valign=top style=\"border-bottom: 0px; padding-left: 40px;\">";
            echo "<table cellpadding=3 cellspacing=0 style=\"border: 0px\">";
        }
			//DISPLAY HEADROW
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center><input type=checkbox "; if($field[$row['headfield']][1]=="Y") { echo "checked"; } echo " name=\"".$row['headfield']."\" value=Y></td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\">".$row['headdetail']."</td>";
            	echo "</tr>";
    } }
mysql_close($con);
$timearr = array();
$timearr[3] = array('sort'=>3,'val'=>"");
$timearr[6] = array('sort'=>6,'val'=>"");
$timearr[9] = array('sort'=>9,'val'=>"");
$timearr[12] = array('sort'=>12,'val'=>"");
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND id NOT IN (2,5,8,11) ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
		if($row['id']/3!=ceil($row['id']/3)) {
			//$timearr[$row['id']+2]['val'] = date("d M Y",$row['eval']);
		} else {
			$timearr[$row['id']]['val'].= "Quarter ending ".date("d M Y",$row['eval']);
		}
    }
mysql_close($con);
        $r+=6;
        if($r > $m) {
            $r=1;
			//CLOSE PREVIOUS COLUMN
            echo "</table>";
			echo "</td>";
			//OPEN NEW COLUMN
			echo "<td valign=top style=\"border-bottom: 0px; padding-left: 40px;\">";
            echo "<table cellpadding=3 cellspacing=0 style=\"border: 0px\">";
        }
			//KPI RESULTS HEADROW
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center><input type=checkbox "; if($field['results'][1]=="Y") { echo "checked"; } echo " name=\"results\" value=Y></td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\">Quarterly Results</td>";
				echo "</tr>";
				echo "<tr>";
            		echo "<td style=\"border-bottom: 0px; \" align=center>&nbsp;</td>";
					echo "<td style=\"border-bottom: 0px; padding-left: 10px;vertical-align: top;\">From: <select name=cf id=cf style=\"\">";
                    $t = 3;
                    foreach($timearr as $tim)
                    {
                        echo "<option ";
                        if($t == $field['cf'][1]) { echo " selected "; }
                        echo "value=".$tim['sort'].">".$tim['val']."</option>";
                        $t+=3;
                    }
                    echo "</select><br />To:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select name=ct id=ct style=\"margin-top: 5px;\">";
                    $t = 3;
                    foreach($timearr as $tim)
                    {
                        echo "<option ";
                        if($t == $field['ct'][1]) { echo " selected "; }
                        echo "value=".$tim['sort'].">".$tim['val']."</option>";
                        $t+=3;
                    }
                    echo "</select>"; 
					echo "</td>";
            	echo "</tr>";
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center>&nbsp;</td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\"><input type=checkbox "; if($field['krresult'][1]=="Y") { echo "checked"; } echo " name=\"krresult\" value=Y>&nbsp;Targets, Actuals & Results</td>";
            	echo "</tr>";
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center>&nbsp;</td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\"><input type=checkbox "; if($field['krsdbipcomment'][1]=="Y") { echo "checked"; } echo " name=\"krsdbipcomment\" value=Y>&nbsp;SDBIP Comment</td>";
            	echo "</tr>";
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center>&nbsp;</td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\"><input type=checkbox "; if($field['krcomment'][1]=="Y") { echo "checked"; } echo " name=\"krcomment\" value=Y>&nbsp;Performance Comment</td>";
            	echo "</tr>";
if(isset($head['trcorrective'])) {
	$row = $head['trcorrective'];
	$hf = $h['headfield'];
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center><input type=checkbox "; if($field[$row['headfield']][1]=="Y") { echo "checked"; } echo " name=\"".$row['headfield']."\" value=Y></td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\">".$row['headdetail']."</td>";
            	echo "</tr>";
}
            echo "</table>";
        echo "</td>";
    echo "</tr>";
echo "</table>";
$hend = count($head) + 4 + 1;
	//check/uncheck options
	echo "<p style=\"margin-top: 3px; margin-left: 0px; font-size: 6.5pt; line-height: 7pt;\">";
		echo "<span onclick=\"checks('C',$hend)\" style=\"text-decoration: underline; cursor: hand;\" class=fc>Check All</span>";
		echo " | ";
		echo "<span onclick=\"checks('U',$hend)\" style=\"text-decoration: underline; cursor: hand;\" class=fc>Uncheck All</span>";
	echo "</p>";
echo "<p>Include Summary of Results? <input type=checkbox value=Y name=krsum "; if($field['krsum'][1]=="Y") { echo "checked"; } echo "></p>";
echo "</div>";

unset($head['toptargetannual']);
unset($head['toptargetrevised']); 



/********** 2. FILTERS *********/
echo "<h3 class=fc>2. Select the filter you wish to apply:</h3>";
echo "<div style=\"margin-left: 17px\">";
echo "<table cellspacing=0 cellpadding=3>";
    foreach($head as $hrow)
    {
		$hf = $hrow['headfield'];
		if($hf!="toppastyear" && $hf != "trcorrective") {
			$i=0;
			include("inc_tr.php");
			echo "<td class=\"filter head\"><b>".$hrow['headdetail'].":</b>";
			if($hf=="wards") { echo "<br /><a href=glossary.php#wards target=_blank><img src=/pics/help.gif style=\"border: 0px;\"></a>"; }
			echo "</td>";
			if($dlist[$hf]['yn']=="Y") {
				$dl = $dlist[$hf];
				$s = count($dl['data'])+1;
				echo "<td class=filter>";
				if($s>6 || $hf == "natkpa" || $hf == "munkpa") { $s = 6; 
					echo "<select multiple name=\"".$hf."filter[]\" size=$s ><option "; if(in_array("all",$dlistfilter[$hf],true) || count($dlistfilter[$hf])==0) { echo "selected "; } echo " value=all>All</option>";
					foreach($dl['data'] as $d) {
						if($hrow['headfield']=="topcalctype") {
							$i = $d['code'];
							$v = $d['value'];
						} else {
							$i = $d['id'];
							$dval = decode($d['value']);
							if(strlen($dval)>130) { $dval = strFn("substr",$dval,0,127)."..."; }
							if($hf=="toptargettype" || $dl['code']=="N") {
								$v = $dval;
							} else {
								$v = $dval." (".$d['code'].")";
							}
						}
						echo "<option ";
						if(in_array($i,$dlistfilter[$hf],true)) { echo "selected "; }
						echo "value=".$i.">".$v."</option>";
					}
					echo "</select><input type=hidden name=\"".$hf."filtertype\" value=any>";
					echo "<br><i><small>Use CTRL key to select multiple options</small></i>";
				} else {	//if size<6 then use checkboxes
					$echo = "";
					foreach($dl['data'] as $d) {
						if($hrow['headfield']=="topcalctype") {
							$i = $d['code'];
							$v = $d['value'];
						} else {
							$i = $d['id'];
							$dval = decode($d['value']);
							if(strlen($dval)>130) { $dval = strFn("substr",$dval,0,127)."..."; }
							if($hf=="toptargettype" || $dl['code']=="N") {
								$v = $dval;
							} else {
								$v = $dval." (".$d['code'].")";
							}
						}
						$echo.="<input type=checkbox name=\"".$hf."filter[]\" id=\"".$hf.$i."\" ";
						if(in_array($i,$dlistfilter[$hf],true)) { $echo.="checked "; }
						$echo.="value=".$i."><label for=\"".$hf.$i."\">".$v."</label><br />";
					}
					echo strFn("substr",$echo,0,strlen($echo)-6);
				}
				echo "</td>";
	/*			if($s>6) { $s = 6; }
					echo "<td valign=top>";
					echo "<select multiple name=\"".$hf."filter[]\" size=$s ><option "; if(in_array("all",$dlistfilter[$hf],true) || count($dlistfilter[$hf])==0) { echo "selected "; } echo " value=all>All</option>";
					foreach($dl['data'] as $d) {
						if($hrow['headfield']=="kpicalctype") {
							$i = $d['code'];
							$v = $d['value'];
						} else {
							$i = $d['id'];
							$dval = decode($d['value']);
							if(strlen($dval)>130) { $dval = strFn("substr",$dval,0,127)."..."; }
							if($hf=="kpitargettype" || $dl['code']=="N") {
								$v = $dval;
							} else {
								$v = $dval." (".$d['code'].")";
							}
						}
						echo "<option ";
						if(in_array($i,$dlistfilter[$hf],true)) { echo "selected "; }
						echo "value=".$i.">".$v."</option>";
					}
					echo "</select><input type=hidden name=\"".$hf."filtertype\" value=any>";
					echo "<br><i><small>Use CTRL key to select multiple options</small></i></td>";*/
			} else {
			switch($hrow['headfield'])
			{
				case "sub":
					echo "<td class=filter><select name=subfilter id=df>";
					echo "<option ";
					if($filter['sub'][1]=="ALL" || !checkIntRef($filter['sub'][1])) { echo "selected "; }
					echo "value=ALL>All Directorates</option>";
					$js = "";
					$sql = "SELECT DISTINCT d.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
					$sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
					$sql.= ", assist_".$cmpcode."_".$modref."_toplevel k ";
					$sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND k.topyn = 'Y' ";
					$sql.= " AND k.topsubid = d.dirid";
					$sql.= " ORDER BY d.dirsort";
					include("inc_db_con.php");
						while($row = mysql_fetch_array($rs)) {
							echo "<option ";
							if($row['dirid']==$filter['sub'][1]) { echo "selected "; }
							echo "value=".$row['dirid'].">".$row['dirtxt']."</option>";
						}
					mysql_close($con);
					echo "</select></td>";
					break;
				case "area":
					echo "<td class=filter>";
					echo "<table cellpadding=3 style=\"border: 0px;\"><tr><td width=25% style=\"border: 0px;\">";
					//echo "<input type=checkbox name=areafilter[] value=X "; if(count($dlistfilter[$hf])==0 || in_array("X",$dlistfilter[$hf],true)) { echo "checked "; } echo "> Any&nbsp;<br />";
					echo "<input type=checkbox name=areafilter[] value=1 "; if(in_array("1",$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> 'All'&nbsp;<br />";
					$cols = 1;
							$sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_area WHERE yn = 'Y' AND value <> 'All' ORDER BY value";
							include("inc_db_con2.php");
								$wmnr = mysql_num_rows($rs2)+2;
								$c2 = $wmnr / 4;
								$c = round($c2);
								if($c < $c2) { $c++; }
								$w=1;
												while($row2 = mysql_fetch_array($rs2))
												{
													$w++;
													if($w>$c)
													{
														echo("</td><td valign=top width=25% style=\"border: 0px;\">"); $cols++;
														$w = 1;
													}
													echo "<input type=checkbox name=areafilter[] value=".$row2['id']." "; if(in_array($row2['id'],$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> ".$row2['value']."<br>";
												}
							mysql_close($con2);
					echo "</td></tr></table>";
					echo "<select name=\"".$hf."filtertype\"><option "; if($filter[$hf][2]!="all") { echo "selected"; } echo " value=any>Match any selected</option><option "; if($filter[$hf][2]=="all") { echo "selected"; } echo " value=all>Match all selected</option></select>";
					break;
				case "wards":
					echo "<td class=filter>";
					echo "<table cellpadding=3 style=\"border: 0px;\"><tr><td width=25% style=\"border: 0px;\">";
					//echo "<input type=checkbox name=wardsfilter[] value=X "; if(count($dlistfilter[$hf])==0 || in_array("X",$dlistfilter[$hf],true)) { echo "checked "; } echo "> Any&nbsp;<br>";
					echo "<input type=checkbox name=wardsfilter[] value=1 "; if(in_array("1",$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> 'All'&nbsp;<br>";
					$cols = 1;
							$sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
							include("inc_db_con2.php");
								$wmnr = mysql_num_rows($rs2)+2;
								$c2 = $wmnr / 4;
								$c = round($c2);
								if($c < $c2) { $c++; }
								$w=1;
												while($row2 = mysql_fetch_array($rs2)) {
													$w++;
													if($w>$c)
													{
														echo("</td><td valign=top width=25% style=\"border: 0px;\">"); $cols++;
														$w = 1;
													}
													echo "<input type=checkbox name=wardsfilter[] value=".$row2['id']." "; if(in_array($row2['id'],$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> ".$row2['numval']."<br>";
												}
							mysql_close($con2);
					echo "</td></tr></table>";
					echo "<select name=\"".$hf."filtertype\"><option "; if($filter[$hf][2]!="all") { echo "selected"; } echo " value=any>Match any selected</option><option "; if($filter[$hf][2]=="all") { echo "selected"; } echo " value=all>Match all selected</option></select>";
					break;
				default:
					if($hf!="toppastyear" && $hf != "trcorrective") {
						echo "<td class=filter>";
						echo "<input type=text name=\"".$hrow['headfield']."filter\" size=30 value=\"".decode($filter[$hf][1])."\">&nbsp;<select  name=\"".$hrow['headfield']."filtertype\">";
						echo "<option "; if($filter[$hf][2]=="all") { echo "selected"; } echo " value=all>Match all words</option>";
						echo "<option "; if($filter[$hf][2]=="any") { echo "selected"; } echo " value=any>Match any word</option>";
						echo "<option "; if($filter[$hf][2]=="exact") { echo "selected"; } echo " value=exact>Match exact phrase</option></td>";
					}
					break;
			} //switch
			} //dlist
			echo "</tr>";
		}
    }
	echo "<tr>";
		echo "<td class=\"filter head\">KPI Result:</td>";
		echo "<td class=filter>";
                echo "<select name=kpiresultfilter>";
					echo "<option "; if($filter['kpiresult'][1]=="ALL" || strlen($filter['kpiresult'][1])==0 || !isset($filter['kpiresult'][1])) { echo " selected "; } echo " value=ALL>All</option>";
					foreach($krsetup as $krs) {
						echo "<option "; if($filter['kpiresult'][1]==$krs['sort']) { echo " selected "; } echo " value=".$krs['sort'].">".$krs['value']."</option>";
					}
                echo "</select> <img src=/pics/help.gif style=\"cursor:hand;\" onclick=\"javascript:window.open('glossary.php#krr','Glossary','width=600,height=300');\">";
				echo "<br /><input type=checkbox name=krgloss value=Y "; if($field['krgloss'][1]=="Y") { echo "checked"; } echo "> Include explanation of KPI Result classification in report?";
				echo "<br /><span style=\"font-style: italic;\">This will only display if you select KPI Results in Step 1.</span>";
        echo "</td>";
    echo "</tr>";
echo "</table>";
echo "</div>";


/*************** SORT BY *******************/
//print_r($sort);
echo "<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>";
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<td style=\"font-weight: bold;\">Group by:&nbsp;</td>";
			echo "<td><select name=groupby><option "; if($groupby=="X" || strlen($groupby)==0) { echo "selected"; } echo " value=\"X\">No grouping</option>";
			foreach($head as $h) {
				if(!in_array($h['headfield'],$nogroup) && !in_array($h['headfield'],$nosort)) {
					echo "<option "; if($groupby==$h['headfield']) { echo "selected"; } echo " value=\"".$h['headfield']."\">".$h['headdetail']."</option>";
				}
			}
			echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td style=\"font-weight: bold; vertical-align: top\">Sort by:&nbsp;</td>";
			echo "<td>"; echo "<div class=demo><ul id=sortable>";
			foreach($sort as $hf) {
				echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$hf."\">".$head[$hf]['headdetail']."</li>";
			}
			echo "</ul></div></td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";
/*if(strlen($sort)==0) { $sort = "sub"; }
echo "<h3 class=fc>3. Select the heading by which you wish to sort the report:</h3>";
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3>";
		echo "<tr>";
			echo "<td class=\"tdgeneral b-w\" style=\"border-bottom: 0px\">Sort by:&nbsp;";
			echo "<select name=sort>";
        	echo "<option "; if($sort == "sub") { echo "selected "; } echo "value=sub>Directorate</option>";
			foreach($head as $hrow) {
				if($hrow['headfield']!="wards" && $hrow['headfield']!="toptargettype" && $hrow['headfield']!="sub") {
					echo chr(10)."<option "; if($sort == $hrow['headfield']) { echo "selected "; } echo "value=".$hrow['headfield'].">".$hrow['headdetail']."</option>";
				}
			}
        	echo "</select></td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";*/



/********** OUTPUT ***************/
if(strlen($output)==0) { $output = "display"; }
echo "<h3 class=fc>4. Select the report output method:</h3>";
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3>";
		echo "<tr>";
			echo "<td width=30 class=\"b-w\" align=center><input type=radio name=\"output\" value=\"display\" "; if($output=="display") { echo "checked"; } echo " id=csvn></td>";
			echo "<td class=\"b-w\"><label for=csvn>Onscreen display</label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class=\"b-w\" align=center><input type=radio name=\"output\" value=\"csv\" "; if($output=="csv") { echo "checked"; } echo " id=csvy></td>";
			echo "<td class=\"b-w\"><label for=csvy>Microsoft Excel (Plain Text)</label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class=\"tdgeneral b-w\" align=center><input type=radio name=\"output\" value=\"excel\" "; if($output=="excel") { echo "checked"; } echo " id=excely></td>";
			echo "<td class=\"tdgeneral b-w\"><label for=excely>Microsoft Excel (Formatted)*</label>";
			echo "</td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";



/************** BUTTONS ***************/
echo "<h3 class=fc>5. Click the button:</h3>";
	echo "<p style=\"margin-left: 17px\">";
	echo "<b>Report Title:</b> <input type=text name=rhead value=\"".decode($report['rhead'])."\" maxlength=100 size=70> <i><small>(Displays at the top of the report.)</small></i><br />";
	echo "<input type=hidden name=act value=VIEW>";
	echo "<input type=hidden name=i value=\"".$report['id']."\">";
	echo "<input type=submit value=\"Generate Report\" name=\"B1\">&nbsp;<input type=reset value=Reset>";
	echo "</p><p style=\"margin-left: 17px\">";
	echo "<b>Report Name:</b> <input type=text name=rname value=\"".decode($report['rname'])."\" maxlength=50 size=30> <i><small>(To identify the report in the quick report list.)</small></i><br />";
if(checkIntRef($report['id'])) {
		echo "<input type=button value=\"Save Changes\" onclick=\"saveReport('E')\">&nbsp;";
		echo "<input type=button value=\"Save As New Report\" onclick=\"saveReport('N')\">&nbsp;";
		echo "<input type=button value=\"Delete Report\" onclick=\"saveReport('D')\">&nbsp;";
} else {
	echo "<input type=button value=\"Save Report\" onclick=\"saveReport('A')\">&nbsp;";
}
	echo "</p>";
?>
</form>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel. 
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
<script type=text/javascript>
<?php 
if(checkIntRef($filter['dir'][1])) {
	echo "var targ = document.getElementById('df');";
	echo "targ.value = ".$filter['dir'][1].";";
}
if($var['a']=="V") {
	echo "document.kpireport.submit();";
}
?>
</script>
<?php
//echo "<pre>"; print_r($filter); print_r($dlistfilter);
?>
</body>

</html>
