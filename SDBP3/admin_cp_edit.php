<?php
    include("inc_ignite.php");
    $r = 1;
    

$style[0] = "background-color: #555555;";
$style[1] = "background-color: #cc0001;";
$style[2] = "background-color: #fe9900;";
$style[3] = "background-color: #006600;";
$style[4] = "background-color: #000066;";

$style2[0] = "background-color: #555555; border-left: 1px solid #ffffff;";
$style2[1] = "background-color: #cc0001; border-left: 1px solid #ffffff;";
$style2[2] = "background-color: #fe9900; border-left: 1px solid #ffffff;";
$style2[3] = "background-color: #006600; border-left: 1px solid #ffffff;";
$style2[4] = "background-color: #000066; border-left: 1px solid #ffffff;";


$stylel[0] = "background-color: #dddddd;";
$stylel[1] = "background-color: #ffcccc;";
$stylel[2] = "background-color: #ffeaca;";
$stylel[3] = "background-color: #ceffde;";
$stylel[4] = "background-color: #ccccff;";


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css" />
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ababab;
    border-width: 1px;
    border-style: solid;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; }
</style>
<?php
if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}

?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('#datepicker1').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy'
                });

                //End
                $('#datepicker2').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy'
                });

                //Start
                $('#datepicker3').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startActual',
                    altFormat: 'd_m_yy'
                });

                //End
                $('#datepicker4').datepicker({
                    showOn: 'both',
                    buttonImage: 'calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endActual',
                    altFormat: 'd_m_yy'
                });
			});
		</script>
<script type=text/javascript>
function resetCP() {
document.location.href = document.location.href;
}
function changeDir(me) {
    var newdir = me.value;
    if(newdir.length>0 && !isNaN(parseInt(newdir)))
    {
        var target = document.getElementById('subid');
            target.length = 0;
            var o = 0;
            var ds = true;
            var opt = new Array();
                opt = sub[newdir];
                for(k in opt)
                {
                    if(o==0)
                        ds = true;
                    else
                        ds = false;
                    target.options[o] = new Option(opt[k][1],opt[k][0],ds,ds);
                    o++;
                }
    }
}


function delCP(i,d) {
    if(!isNaN(parseInt(i)) && !isNaN(parseInt(d)))
    {
        var confirmtxt = "";
            confirmtxt = "Are you sure you wish to delete this Capital Project?";
        if(confirm(confirmtxt)==true)
        {
            document.location.href = "admin_cp_delete.php?i="+i+"&d="+d;
        }
    }
    else
    {
        alert("An error occurred.\nPlease reload the page and try again.");
    }
}
function chgSub(me) {
    var s = me.value;
    var s0 = document.getElementById('subid0').value;
    if(s0 != s)
    {
        document.getElementById('movekpi').checked = true;
    }
    else
    {
        document.getElementById('movekpi').checked = false;
    }
}
function Validate(me) {
    var project = me.cpproject.value;
    var startdate = me.cpstartdate.value;
    var enddate = me.cpenddate.value;
    var subid = me.subid.value;
    var valid8 = "true";
    
    if(project.length == 0)
    {
        document.getElementById('cpproject').className = "reqtext";
        document.getElementById('cpproject').focus();
        valid8 = "false";
    }
    else
    {
        document.getElementById('cpproject').className = "blank";
    }
    if(subid.length == 0 || subid == "X")
    {
        document.getElementById('subid').className = "reqtext";
        valid8 = "false";
    }
    else
    {
        document.getElementById('subid').className = "blank";
    }
    
    var dterr = "N";
    if(startdate.length == 0)
    {
        document.getElementById('datepicker1').className = "reqtext";
        valid8 = "false";
        dterr = "Y";
    }
    else
    {
        document.getElementById('datepicker1').className = "blank";
    }
    if(enddate.length == 0)
    {
        document.getElementById('datepicker2').className = "reqtext";
        valid8 = "false";
        dterr = "Y";
    }
    else
    {
        document.getElementById('datepicker2').className = "blank";
    }
    if(dterr == "N")
    {
        var sdatearr = startdate.split("_");
        var edatearr = enddate.split("_");
        var sdate = new Date();
        sdate.setFullYear(sdatearr[2],sdatearr[1],sdatearr[0]);
        var edate = new Date();
        edate.setFullYear(edatearr[2],edatearr[1],edatearr[0]);
        if(edate<sdate)
        {
            dterr = "D";
            document.getElementById('datepicker1').className = "reqtext";
            document.getElementById('datepicker2').className = "reqtext";
        }
    }

    var t = 1;
    var target;
    if(valid8 == "true")
    {
        return true;
    }
    else
    {
        var errtxt = "The following errors have occurred:\n - Please complete all required fields as indicated.";
        if(dterr == "D")
        {
            errtxt = errtxt + "\n - Your end date can not fall before your start date.";
        }
        alert(errtxt);
        return false;
    }
    
    return false;
}
function wardsAll(me) {
        var elements = document.getElementById('addcp');
        var f = parseInt(document.getElementById('f').value);
        var f2 = parseInt(document.getElementById('f2').value);
        var z = 0;
        f++;
    if(me.checked)
    {
        for(z=f;z<f2;z++)
        {
            elements[z].checked = false;
            elements[z].disabled = true;
        }
    }
    else
    {
        for(z=f;z<f2;z++)
        {
            elements[z].disabled = false;
        }
    }
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<a name=top></a><h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Admin - Capital Projects ~ Edit</b></h1>
<?php
$referer = "-".$_SERVER['HTTP_REFERER']."-";
$referer2 = explode("/",$referer);
$referer2[4] = explode(".",$referer2[4]);
$refres = "Y";
//if($referer=="--" || ($referer2[2]!="assist.ignite4u.co.za" && $referer2[2]!="ignite-jeb") || $referer2[4][0]!="admin_cp_edit_list")/
//{
//    $refres = "N";
//}
//print_r($referer2);

$dirid = $_GET['d'];
$subid = $_GET['s'];
$origsub = $subid;
$src = $_GET['r'];
$cpid = $_GET['i'];
$cprow = array();
$cpwards = array();

$err = "Y";

if(is_numeric($cpid) && $cpid > 0)
{
    $mnr = 0;
    //CHECK FOR SUB ADMIN
    $sql = "SELECT a.id FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_".$modref."_capital k ";
    $sql.= "WHERE k.cpsubid = a.ref AND a.type = 'SUB' AND a.yn = 'Y' AND k.cpid = ".$cpid." AND a.tkid = '".$tkid."'";
    include("inc_db_con.php");
    $mnr = $mnr + mysql_num_rows($rs);
    mysql_close($con);
    //CHECK FOR DIR ADMIN
    $sql = "SELECT a.id FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_".$modref."_capital k, assist_".$cmpcode."_".$modref."_dirsub s ";
    $sql.= "WHERE k.cpsubid = s.subid AND s.subdirid = a.ref AND a.type = 'DIR' AND a.yn = 'Y' AND k.cpid = ".$cpid." AND a.tkid = '".$tkid."'";
    include("inc_db_con.php");
    $mnr = $mnr + mysql_num_rows($rs);
    mysql_close($con);
    //CHECK FOR KPI ADMIN
    $sql = "SELECT a.id FROM assist_".$cmpcode."_".$modref."_list_admins a ";
    $sql.= "WHERE a.type = 'KPI' AND a.yn = 'Y' AND a.tkid = '".$tkid."'";
    include("inc_db_con.php");
    $mnr = $mnr + mysql_num_rows($rs);
    mysql_close($con);

    if($mnr==0)
        die("<P>You are not authorised to edit this Capital Project.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");

    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital";
    $sql.= " WHERE cpyn = 'Y'";
    $sql.= " AND cpid = ".$cpid;
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
        if($mnr > 0)
        {
            $cprow = mysql_fetch_array($rs);
            $err = "N";
        }
    mysql_close();
    if($err == "N")
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_wards WHERE cwyn = 'Y' AND cwcpid = ".$cpid;
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $cpwards[$row['cwwardsid']] = "Y";
            }
        mysql_close();
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_area WHERE cayn = 'Y' AND cacpid = ".$cpid;
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $cparea[$row['caareaid']] = "Y";
            }
        mysql_close();
    }
}

$dirrow = array();
if(is_numeric($dirid) && $dirid > 0 && $err == "N")
{

    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$dirid;
    include("inc_db_con.php");
        $mnr = mysql_num_rows($rs);
        if($mnr > 0)
        {
            $dirrow = mysql_fetch_array($rs);
            $err = "N";
        }
        else
        {
            $err = "Y";
        }
    mysql_close();
    
}
else
{
    if(is_numeric($subid) && $subid > 0 && $err == "N")
    {
        $sql = "SELECT d.* FROM assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s WHERE s.subdirid = ".$subid;
        include("inc_db_con.php");
            $mnr = mysql_num_rows($rs);
            if($mnr > 0)
            {
                $dirrow = mysql_fetch_array($rs);
                $err = "N";
            }
            else
            {
                $err = "Y";
            }
        mysql_close();
    }
    else
    {
        $err = "Y";
    }
}


if($err == "N")
{
?>

<p>All fields marked with a * are required.</p>
<form id=addcp name=addcp method=POST action=admin_cp_edit_process.php onsubmit="return Validate(this);" language=jscript>
<h2 class=fc>Capital Project Details</h2>
<table cellpadding=3 cellspacing=0 width=570>
<tr>
    <td class=tdheaderl height=27 width=120>Reference</td>
    <td class=tdgeneral><?php echo($cpid); ?></td>
</tr>
<tr>
    <td class=tdheaderl height=27 width=120>Directorate:</td>
    <td class=tdgeneral><?php echo($dirrow['dirtxt']."<input type=hidden name=dirid value=".$dirid.">"); ?></td>
</tr>
<tr>
    <td class=tdheaderl width=120 height=27 valign=top>Sub-Directorate:*</td>
    <td class=tdgeneral><?php
                $sql = "SELECT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s WHERE s.subid = ".$subid;
                include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['subtxt']."<input type=hidden name=subid value=".$row['subid'].">");
                mysql_close($con);
        ?>
    </td>
</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CP' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
    $f = 1;
    while($row = mysql_fetch_array($rs))
    {
        $f++;
        if($dlist[$row['headfield']]['yn']=="Y" && $row['headfield']!="prog") {
            $dl = $dlist[$row['headfield']];
                    ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                        <td class=tdgeneral height=27 width=<?php echo($widthtg); ?>>
                            <select name=<?php echo($row['headfield']); ?> id=<?php echo($row['headfield']); ?>>
                                <option selected value=X>--- SELECT ---</option>
                                <?php
                                $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_".$dl['id']." WHERE yn = 'Y' ORDER BY value";
                                include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2))
                                    {
                                        $fld = "cp".$row['headfield']."id";
                                        if($row2['id']==$cprow[$fld]) {
                                            $seld = " selected ";
                                        } else {
                                            $seld = "";
                                        }
                                        ?><option <?php echo($seld); ?> value=<?php echo($row2['id']); ?>><?php echo($row2['value']); ?><?php if($dl['code']=="Y") { ?> (<?php echo($row2[$dl['fld']]); ?>)<?php } ?></option><?php
                                    }
                                mysql_close();
                                ?>
                            </select>
                        </td>
                    <?php
        } else {
        switch($row['headfield'])
        {
            case "cpstartdate":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker1" readonly="readonly" value="<?php echo(date("d M Y",$cprow['cpstartdate'])); ?>"> <input type=hidden size=10 name=cpstartdate id=startDate value="<?php echo(date("d_n_Y",$cprow['cpstartdate'])); ?>"></td>
                </tr>
                <?php
                break;
            case "cpenddate":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker2" readonly="readonly" value="<?php echo(date("d M Y",$cprow['cpenddate'])); ?>"> <input type=hidden size=10 name=cpenddate value="<?php echo(date("d_n_Y",$cprow['cpenddate'])); ?>" id=endDate></td>
                </tr>
                <?php
                break;
            case "cpstartactual":
                if(checkIntRef($cprow['cpstartactual'])) {
                    $value = date("d M Y",$cprow['cpstartactual']);
                } else {
                    $value = "";
                }
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker3" readonly="readonly" value="<?php echo($value); ?>"> <input type=hidden size=10 name=cpendactual value="<?php echo(date("d_n_Y",$cprow['cpendactual'])); ?>" id=endActual></td>
                </tr>
                <?php
                break;
            case "cpendactual":
                if(checkIntRef($cprow['cpendactual'])) {
                    $value = date("d M Y",$cprow['cpendactual']);
                } else {
                    $value = "";
                }
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
                    <td class=tdgeneral><input type=text size=15 id="datepicker4" readonly="readonly" value="<?php echo($value); ?>"> <input type=hidden size=10 name=cpendactual value="<?php echo(date("d_n_Y",$cprow['cpendactual'])); ?>" id=endActual></td>
                </tr>
                <?php
                break;
            case "cpproject":
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:*</td>
                    <td class=tdgeneral><?php echo("<textarea rows=5 cols=30 name=".$row['headfield']." id=".$row['headfield'].">".$cprow['cpproject']."</textarea>  <span style=\"font-size: 7.5pt;\">(max ".$row['headmaxlen']." characters)</span>"); ?></td>
                </tr>
                <?php
                break;
            case "progress":
                break;
            case "wards":
                ?>
                <tr>
                    <td class=tdheaderl width=120 valign=top><?php echo($row['headdetail']); ?>:<input type=hidden id=f size=2 value=<?php echo($f); ?>></td>
                    <td class=tdgeneral>
                    <?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $c = 0;
                            $c2 = 0;
                                ?>
                                <table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
                                    <tr>
                                        <td class=tdgeneral style="border: 0px solid #ffffff;"><input type=checkbox <?php if($cpwards[1]== "Y") { echo(" checked "); } ?> name=wards[] value=1 onclick="wardsAll(this);"> All</td>
                                        <?php
                                            $c++;
                                            $c2++;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                echo("<td class=tdgeneral style=\"border: 0px solid #ffffff;\"><input type=checkbox ");
                                                if($cpwards[$row2['id']] == "Y") { echo(" checked "); }
                                                echo("name=wards[] value=".$row2['id']." >".$row2['value']."</td>");
                                                $c++;
                                                if($c==5)
                                                {
                                                    $c = 0;
                                                    echo("</tr><tr>");
                                                }
                                                $c2++;
                                            }
                                        ?>
                                    </tr>
                                </table>
                                <?php
                        mysql_close($con2);
                    ?>
                    </td>
                </tr>
                <?php
                $f2 = $f+$c2;
                break;
            case "area":
                ?>
                <tr>
                    <td class=tdheaderl width=120 valign=top><?php echo($row['headdetail']); ?>:</td>
                    <td class=tdgeneral>
                    <?php
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_area WHERE yn = 'Y' AND value <> 'All' ORDER BY value";
                        include("inc_db_con2.php");
                            $c = 0;
                            $c2 = 0;
                                ?>
                                <table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
                                    <tr>
                                        <td class=tdgeneral style="border: 0px solid #ffffff;"><input type=checkbox <?php if($cparea[1]== "Y") { echo(" checked "); } ?> name=area[] value=1> All</td>
                                        <?php
                                            $c++;
                                            $c2++;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                echo("<td class=tdgeneral style=\"border: 0px solid #ffffff;\"><input type=checkbox ");
                                                if($cparea[$row2['id']] == "Y") { echo(" checked "); }
                                                echo("name=area[] value=".$row2['id']." >".$row2['value']."</td>");
                                                $c++;
                                                if($c==5)
                                                {
                                                    $c = 0;
                                                    echo("</tr><tr>");
                                                }
                                                $c2++;
                                            }
                                        ?>
                                    </tr>
                                </table>
                                <?php
                        mysql_close($con2);
                    ?>
                    </td>
                </tr>
                <?php
//                $f2 = $f+$c2;
                break;
            default:
                $leng = $row['headmaxlen'];
                if(!checkIntRef($leng) || $leng > 30) { $leng = 30; }
                ?>
                <tr>
                    <td class=tdheaderl width=120><?php echo($row['headdetail']); ?>:</td>
					<?php if($row['headfield']!="cpvotenum") { ?>
                    <td class=tdgeneral><input type=text size=<?php echo($leng); ?> maxlength=<?php echo($row['headmaxlen']); ?> name=<?php echo($row['headfield']); ?> value="<?php echo($cprow[$row['headfield']]); ?>">  <span style="font-size: 7.5pt;">(max <?php echo($row['headmaxlen']); ?> characters)</span></td>
					<?php } else { ?>
                    <td class=tdgeneral><?php echo($cprow[$row['headfield']]); ?> <input type=hidden name=<?php echo($row['headfield']); ?> value="<?php echo($cprow[$row['headfield']]); ?>"></td>
					<?php } ?>
                </tr>
                <?php
                break;
        }
        }
    }
mysql_close();
?>
</table>
<?php
if($subid == 0)
{
    $subid = $cprow['cpsubid'];
}
?>
</table>
<p style="margin:0 0 0 0;padding:0 0 0 0;font-size:1pt;">&nbsp;</p>
<input type=hidden name=cpid value="<?php echo($cpid); ?>">
<input type=hidden name=dirid value="<?php echo($dirid); ?>">
<input type=hidden name=subid0 id=subid0 value="<?php echo($subid); ?>">
<input type=hidden name=origsub id=origsub value="<?php echo($origsub); ?>">
<input type=hidden name=r value="<?php echo($src); ?>">
<input type=hidden name=refres value="<?php echo($refres); ?>">
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
        <td class=tdgeneral align=center><input type=hidden id=f2 value=<?php echo($f2); ?>><input type=submit value=" Save Changes ">&nbsp;&nbsp;&nbsp;<?php if($src == "k") { ?><input type=button value=Reset onclick=resetCP();>&nbsp;&nbsp;&nbsp;<input type=button value=Delete onclick="delCP(<?php echo($cpid); ?>,<?php echo($dirid); ?>)"><?php } else { echo("<input type=reset>"); }?></td>
    </tr>
</table>
</form>
<?php

?>
<h2 style="margin-top: 20px;" class=fc>Capital Project Progress</h2>
<table cellpadding=5 cellspacing=0 width=570>
<tr>
    <td class=tdgeneral width=130>&nbsp;</td>
    <td class=tdheader width=110 >Budget</td>
    <td class=tdheader width=110 >Actual</td>
    <td class=tdheader width=110 >% Spent - YTD</td>
    <td class=tdheader width=110 >R Spent - YTD</td>
</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_capital_results WHERE crcpid = ".$cpid." ORDER BY crtimeid";
include("inc_db_con.php");
    $crrow = array();
    while($row = mysql_fetch_array($rs))
    {
        $crrow[$row['crtimeid']] = $row;
    }
mysql_close();

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    $s=1;
    $accytd = 0;
    $totytd = 0;
    while($row = mysql_fetch_array($rs))
    {
        $accytd = 0;
            //if($row['sval']<$today) { $rowspan = 4; } else { $rowspan=1; }
        $rowspan=1;
        if($crrow[$row['id']]['craccpercytd']>0) { $accytd = $crrow[$row['id']]['craccpercytd']; }
        if($crrow[$row['id']]['crtotrandytd']>0) { $totytd = $crrow[$row['id']]['crtotrandytd']; }
?>
    <tr>
        <td class=tdheaderl valign=top style="<?php echo($style[$s]); ?>" rowspan=<?php echo($rowspan); ?>><?php echo(date("d M Y",$row['eval'])); ?>:</td>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php if($crrow[$row['id']]['crbudget']>0 || $crrow[$row['id']]['cractual']>0 ) { if($crrow[$row['id']]['crbudget']==0) { echo("R -"); } else { echo("R ".number_format($crrow[$row['id']]['crbudget'],0)); } } ?>&nbsp;</td>
    <?php if($row['sval']<$today) { //if($row['sval']<$today) { ?>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php if($crrow[$row['id']]['crbudget']>0 || $crrow[$row['id']]['cractual']>0 ) { echo("R ".number_format($crrow[$row['id']]['cractual'],0)); } ?>&nbsp;</td>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php if(checkIntRef($accytd)) { echo(number_format($accytd,2)." %"); } ?>&nbsp;</td>
        <td class=tdgeneral align=right style="<?php echo($stylel[$s]); ?>"><?php echo("R ".number_format($totytd,0)); ?>&nbsp;</td>
    <?php
        } else {   //if tp < today ?>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
        <td class=tdgeneral style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
    <?php
        }   //if tp < today
        $s++;
        if($s>4) { $s=1; }
    }
mysql_close();
?>
</table>
<p><a href=#top><img src=/pics/tri_up.gif border=0 align=absmiddle><span style="text-decoration: none;"> </span>Top</a></p>
<?php

}
else    //if err
{
    ?>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
//$urlback = "admin.php";
include("inc_goback_history.php");
?>
<script type=text/javascript>
<?php echo($js); ?>
</script>
</body>
</html>
