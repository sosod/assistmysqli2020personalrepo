<?php
	include("inc_ignite.php");
	$gfslist = getListKPI($dlist['gfs']);
	$head = getHeadings("headtype = 'CF' AND headdetailsort = 0 AND headyn = 'Y'");
	$result = array();
	$v = $_REQUEST;
	$act = $v['act'];
	switch($act)
	{
		case "add":
			$title = "Add Line Item";
			$newact = "addsave";
			break;
		case "addsave":
			$title = "Add Line Item";
			$newact = "addsave";
			$res = "Save failed. Please complete all required fields (*).";
			$err = "N";
			if(!checkIntRef($v['subid'])) { $err = "Y"; }
			if(strlen($v['line'])==0) { $err = "Y"; }
			if(!checkIntRef($v['gfs'])) { $err = "Y"; }
			if($err == "Y") {
				$result[0] = "error";
				$result[1] = $res;
			} else {
				$line = $v['line']; $subid = $v['subid']; $gfs = $v['gfs']; $vote = $v['vote'];
				$sort = 0;
				$sql = "SELECT max(linesort) FROM ".$dbref."_finance_lineitems WHERE linesubid = $subid";
				include("inc_db_con.php");
					$rowsort = mysql_fetch_array($rs);
				mysql_close($con);
				$sort += $rowsort['linesort'];
				$sql = "INSERT INTO ".$dbref."_finance_lineitems (lineid, linevalue, lineyn, linesort, linetype, linesubid, linegfsid, linevote)";
				$sql.= " VALUES (null,'$line','Y',$sort,'CF',$subid,$gfs,'$vote')";
				include("inc_db_con.php");
					$lineid = mysql_insert_id($con);
				if(!checkIntRef($lineid)) { 
					$result[0] = "error";
					$result[1] = "Save failed.  An unknown error has occurred.  Please try again.";
				} else {
					$lact = "Created new line item $lineid ";
					$lsql = $sql;
					for($t=0;$t<12;$t++) {
						$rev = $v['revenue'][$t];  if(strlen($rev)==0 || !is_numeric($rev)) { $rev = 0; $v['revenue'][$t] = 0; }
						$op = $v['opex'][$t];  if(strlen($op)==0 || !is_numeric($op)) { $op = 0; $v['opex'][$t] = 0; }
						$cap = $v['capex'][$t];  if(strlen($cap)==0 || !is_numeric($cap)) { $cap = 0; $v['capex'][$t] = 0; }
						$sql = "INSERT INTO ".$dbref."_finance_cashflow (cfid,cflineid,cfyn,cftimeid,cfrev1,cfop1,cfcp1)";
						$sql.= " VALUES (null,$lineid,'Y',".$v['time'][$t].",$rev,$op,$cap)";
						include("inc_db_con.php");
							$lsql.= " | ".$sql;
					}
					logChange($lineid,'',$lact,'N',$lsql);
					$result[0] = "check";
					$result[1] = "New Line Item created.";
					$v = array();
				}
			}
			break;
		case "del":
			$title = "Edit Line Item";
			$newact = "delete";
			$l = $v['l'];
			if(checkIntRef($l)) {
				$sql = "UPDATE ".$dbref."_finance_cashflow SET cfyn = 'N' WHERE cflineid = $l";
				include("inc_db_con.php");
					$lact = "Cashflow items deleted due to line item $l deletion. ".mysql_affected_rows($con)." cashflow records affected.";
					logChange($l,'',$lact,'N',$sql);
				$sql = "UPDATE ".$dbref."_finance_lineitems SET lineyn = 'N' WHERE lineid = $l";
				include("inc_db_con.php");
					$lact = "Line item $l deleted. ".mysql_affected_rows($con)." line records affected.";
					logChange($l,'',$lact,'N',$sql);
				$result[0] = "check";
				$result[1] = "Line item $l successfully deleted.";
			} else {
				$result[0] = "error";
				$result[1] = "Invalid line item.";
			}
			break;
		case "edit":
			$title = "Edit Line Item";
			$newact = "editsave";
			$l = $v['l'];
			if(checkIntRef($l)) {
				$sql = "SELECT * FROM ".$dbref."_finance_lineitems WHERE lineid = $l";
				include("inc_db_con.php");
					if(mysql_num_rows($rs)>0) {
						$line = mysql_fetch_array($rs);
						$v['subid'] = $line['linesubid'];
						$v['line'] = $line['linevalue'];
						$v['vote'] = $line['linevote'];
						$v['gfs'] = $line['linegfsid'];
					} else {
						$result[0] = "error";
						$result[1] = "Line Item not found.";
					}
			} else {
				$result[0] = "error";
				$result[1] = "Line Item not found.";
			}
			break;
		case "editsave":
			$title = "Edit Line Item";
			$newact = "editsave";
			$l = $v['l'];
			if(checkIntRef($l)) {	//IF LINEID IS VALID
				//VALIDATE ALL REQUIRED FIELDS
				$res = "Save failed. Please complete all required fields (*).";
				$err = "N";
				if(!checkIntRef($v['subid'])) { $err = "Y"; }
				if(strlen($v['line'])==0) { $err = "Y"; }
				if(!checkIntRef($v['gfs'])) { $err = "Y"; }
				if($err == "Y") {
					$result[0] = "error";
					$result[1] = $res;
				} else {
					//UPDATE DB
					$line = code($v['line']); $subid = $v['subid']; $gfs = $v['gfs']; $vote = code($v['vote']);
					$sql = "UPDATE ".$dbref."_finance_lineitems SET ";
					$sql.= " linevalue = '$line', linesubid = $subid, linegfsid = $gfs, linevote = '$vote'";
					$sql.= " WHERE lineid = $l";
					include("inc_db_con.php");
					if(mysql_affected_rows($con)>1) {
						$result[0] = "error";
						$result[1] = "An unexpected error occurred. ".mysql_affected_rows($con)." line items where updated.";
					} else {
						$lact = "Edited line item $l";
						logChange($l,'',$lact,'N',$sql);
						$result[0] = "check";
						$result[1] = "Line Item updated.";
					}
				}
			} else {
				$result[0] = "error";
				$result[1] = "Invalid Line Item";
			}
			break;
		default:
			$title = "Add Line Item";
			$act = "add";
			$newact = "addsave";
			break;
	}

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table th {	
	text-align: left;
}
table td {
	border: 1px solid #ffffff;
}
.required {
	/* background-color: #ffffff; */
}
</style>
		<link type="text/css" href="lib/jquery-ui-1.7.1.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="lib/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="lib/jquery-ui-1.7.1.custom.min.js"></script>
<script type=text/javascript>
function delLine(l) {
	if(!isNaN(parseInt(l))) {
		if(confirm("Are you sure you wish to delete this line item?"))
			document.location.href = 'admin_fin_cf.php?act=del&l='+l;
	} else {
		alert("An error has occurred.\n\nIgnite Assist is unable to delete this line item.\nPlease reload the page and try again.");
	}
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Finance - Monthly Cashflows ~ <?php echo $title; ?></h1>
<?php displayResult($result); ?>

<?php 
if($result[0]=="check") {
	if($act == "addsave") {
		echo "<p style=\"margin-top:20px\"><input type=button value=\"Add Another Line Item\" onclick=\"document.location.href='admin_fin_cf.php?act=add';\"></p>";
	}
	$urlback = "admin_fin_cf_setup.php";
	include("inc_goback.php");
} else { 
?>
<form name=frm method=post action=admin_fin_cf.php>
<?php echo "<input type=hidden name=act value=$newact>"; ?>
<?php echo "<input type=hidden name=l value=$l>"; ?>
	<table cellpadding=3 cellspacing=0 width=600>
<?php
if($act == "edit" || $act == "editsave" || $act == "del") {
		echo "<tr>";
			echo "<th>Reference:</th>";
			echo "<td colspan=2 class=fc style=\"font-weight:bold;\">$l</td>";
		echo "</tr>";
}
	echo "<tr>";
			echo "<th width=135>Sub-Directorate:*</th>";
			echo "<td colspan=2><select name=subid class=required><option value=X ";
			if(!checkIntRef($v['subid'])) { echo "selected "; }
			echo ">--- SELECT ---</option>";
			 
				$sql = "SELECT d.dirtxt, s.subtxt, s.subid FROM ".$dbref."_dir d, ".$dbref."_dirsub s";
				$sql.= " WHERE d.dirid = s.subdirid AND d.diryn = 'Y' AND s.subyn = 'Y' ";
				$sql.= " ORDER BY d.dirsort, s.subsort";
				include("inc_db_con.php");
					while($row = mysql_fetch_array($rs))
					{
						echo "<option ";
						if(checkIntRef($v['subid']) && $v['subid']==$row['subid']) { echo "selected "; }
						echo "value=".$row['subid'].">".$row['dirtxt']." - ".$row['subtxt']."</option>";
					}
				mysql_close($con);
			?>
			</select></td>
		</tr>
		<?php 
		$hd = $head['line']; 
		$width = ceil($hd['headmaxlen']/4);
		echo "<tr>";
			echo "<th>".$hd['headdetail'].":*</th>";
			echo "<td colspan=2><input type=text class=required name='".$hd['headfield']."' value='".$v['line']."' size=$width maxlength=".$hd['headmaxlen']."> <span style=\"font-style: italic;font-size: 6.5pt;\">(Maximum ".$hd['headmaxlen']." characters)</span></td>";
		echo "</tr>";
		$hd = $head['vote']; 
		$width = ceil($hd['headmaxlen']);
		echo "<tr>";
			echo "<th>".$hd['headdetail']."</th>";
			echo "<td colspan=2><input type=text name='".$hd['headfield']."' value='".$v['vote']."' size=$width maxlength=".$hd['headmaxlen']."> <span style=\"font-style: italic;font-size: 6.5pt;\">(Maximum ".$hd['headmaxlen']." characters)</span></td>";
		echo "</tr>";
		$hd = $head['gfs']; 
		echo "<tr>";
			echo "<th>".$hd['headdetail'].":*</th>";
			echo "<td colspan=2><select name='".$hd['headfield']."' class=required><option ";
			if(!checkIntRef($v['gfs'])) { echo "selected "; }
			echo "value=X>--- SELECT ---</option>";
			foreach($gfslist as $g)
			{
				echo "<option ";
				if(checkIntRef($v['gfs']) && $g['id']==$v['gfs']) { echo "selected "; }
				echo "value=".$g['id'].">".$g['value']."</option>";
			}
			echo "</select></td>";
		echo "</tr>";
if($act=="add" || $act == "addsave") {
$style['th'][1] = "background-color: #cc0001; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][2] = "background-color: #fe9900; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][3] = "background-color: #006600; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][4] = "background-color: #000066; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";

$style['thl'][1]['R'] = "background-color: #ffabab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['R'] = "background-color: #ffddab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['R'] = "background-color: #a3ffc2; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['R'] = "background-color: #babaff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['O'] = "background-color: #ffcccc; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['O'] = "background-color: #ffe6bd; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['O'] = "background-color: #ceffde; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['O'] = "background-color: #ccccff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['C'] = "background-color: #ffdbdf; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['C'] = "background-color: #fff1de; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['C'] = "background-color: #deffea; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['C'] = "background-color: #ddddff; color: #000; border-bottom: 1px solid #fff;";
		
$sql = "SELECT * FROM ".$dbref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
    ?>
    <tr>
		<td colspan=3 height=27 style="background-color: #888; color: #fff; font-weight: bold;"><b>Original Budget</b></td>
    </tr>
    <?php
        $s=1;
        $t=0;
        while($row = mysql_fetch_array($rs))
        {
            ?>
            <tr>
                <td rowspan=3 valign=top style="<?php echo($style['th'][$s]); ?> font-weight: bold;"><?php echo(date("M Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
                <td width=70 style="<?php echo($style['thl'][$s]['R']); ?> font-weight: bold; ">Revenue:</td>
                <td valign=top style="<?php echo($style['thl'][$s]['R']); ?>">R <input type=text size=15 name=revenue[] value="<?php echo($v['revenue'][$t]); ?>" id=r<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <tr>
                <td width=70 style="<?php echo($style['thl'][$s]['O']); ?> font-weight: bold; ">Op. Exp.:</td>
                <td valign=top style="<?php echo($style['thl'][$s]['O']); ?>">R <input type=text size=15 name=opex[] value="<?php echo($v['opex'][$t]); ?>" id=o<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <tr>
                <td width=70 style="<?php echo($style['thl'][$s]['C']); ?> font-weight: bold; ">Cap. Exp.:</td>
                <td valign=top style="<?php echo($style['thl'][$s]['C']); ?>">R <input type=text size=15 name=capex[] value="<?php echo($v['capex'][$t]); ?>" id=c<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <?php
            $t++;
            $s++;
            if($s>4) { $s=1; }
        }
    }
mysql_close($con);
?>
    <tr>
		<th>&nbsp;<input type=hidden name=t value=<?php echo($t); ?> size=5></th>
		<td colspan=2 valign=top ><input type=submit value=" Add "></td>
    </tr>
<?php } else {
?>
    <tr>
		<th>&nbsp;</th>
		<td colspan=2 valign=top ><input type=submit value=" Save Changes "> <input type=button value=Delete onclick="delLine(<?php echo $l; ?>);"></td>
    </tr>
<?php
} ?>
	</table>
</form>
<?php 
	$urlback = "admin_fin_cf_setup.php";
include("inc_goback.php");	
} //if result = check ?>
</body>
</html>