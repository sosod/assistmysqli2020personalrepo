<?php
$sum = array();
$sum[0] = array(
	'rb'=>0,
	'ra'=>0,
	'ob'=>0,
	'oa'=>0,
	'cb'=>0,
	'ca'=>0
);
/********* GET INFO *********/
$sqlgroup = "";
$sql = "SELECT sum(cfrev8) as rb, sum(cfrev2) as ra, sum(cfop8) as ob, sum(cfop2) as oa, sum(cfcp8) as cb, sum(cfcp2) as ca";
if($groupby == "dir") {
	if($mds['type']=="M") {
		$sql.= ", dirid";
		$sqlgroup = "dirid";
	} else {
		$sql.= ", subid";
		$sqlgroup = "subid";
	}
} elseif($groupby == "gfs") {
	$sql.= ", linegfsid";
		$sqlgroup = "linegfsid";
}
$sql.= " FROM ".$dbref."_finance_cashflow";
$sql.= " INNER JOIN ".$dbref."_finance_lineitems ON lineid = cflineid";
$sql.= " INNER JOIN ".$dbref."_dirsub s ON linesubid = subid";
$sql.= " INNER JOIN ".$dbref."_dir d ON dirid = subdirid";
$sql.= " WHERE cfyn = 'Y' AND lineyn = 'Y' AND subyn = 'Y' AND diryn = 'Y' ";
$sql.= " AND cftimeid >= $from AND cftimeid <= $to ";
	if(checkIntRef($dirsub)) {
		$sql.= " AND s.subid = ".$dirsub;
		$sub = $dirsub;
		$dir = "ALL";
	} elseif($dirsub!="ALL") {
		$dirsub2 = substr($dirsub,2,strlen($dirsub));
		if(checkIntRef($dirsub2)) {
			$sql.= " AND d.dirid = ".$dirsub2;
		}
		$dir = $dirsub2;
		$sub = "ALL";
	} else {
		$sub = "ALL";
		$dir = "ALL";
	}
if(strlen($sqlgroup)>0) {
	$sql.= " GROUP BY ".$sqlgroup;
}
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
		$sum[0]['rb'] += $row['rb'];
		$sum[0]['ra'] += $row['ra'];
		$sum[0]['ob'] += $row['ob'];
		$sum[0]['oa'] += $row['oa'];
		$sum[0]['cb'] += $row['cb'];
		$sum[0]['ca'] += $row['ca'];
		if(strlen($sqlgroup)>0) {
			$moreid = $row[$sqlgroup];
			if(!isset($sum[$moreid])) { $sum[$moreid] = array(); }
			$sum[$moreid] = $row;
		}
	}
mysql_close($con);

/********* GROUPS ***********/
$group = array();
if($groupby!="X") {
	switch($sqlgroup) {
		case "dirid":
			$sql = "SELECT dirid as id, dirtxt as value FROM ".$dbref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
			break;
		case "subid":
			$sql = "SELECT subid as id, subtxt as value FROM ".$dbref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$mds['dir']." ORDER BY subsort";
			break;
		case "linegfsid":
			$sql = "SELECT id, value FROM ".$dbref."_list_gfs ORDER BY value";
			break;
	}
	include("inc_db_con.php");
		while($row = mysql_fetch_array($rs)) {
			$group[$row['id']] = $row;
		}
	mysql_close($con);
}
/********* TITLE ************/
$overall_title = "";
if($mds['type']=="M") {
	if($groupby!="X" && $groupby != "dir") {
		$overall_title = $dlist[$groupby]['name'];
	} else {
		$overall_title = $cmpname;
	}
} else {
	if($mds['type'] == "D") { 
		$sql = "SELECT dirtxt as txt FROM ".$dbref."_dir WHERE dirid = ".$mds['dir'];
	} else {
		$sql = "SELECT subtxt as txt FROM ".$dbref."_dirsub WHERE subid = ".$mds['sub'];
	}
	include("inc_db_con.php");
		$dsrow = mysql_fetch_array($rs);
	mysql_close($con);
	if($groupby!="X" && $groupby != "dir") {
		$overall_title = $dlist[$groupby]['name'];
		if(isset($dsrow['txt'])) { $overall_title.= " for ".$dsrow['txt']; }
	} else {
		$overall_title = $dsrow['txt'];
	}
}
?>