<?php
    include("inc_ignite.php");
$fin = array();
$kpia = array();
$tl = array();
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheader { border: 1px solid #ffffff; }
.tdgeneral { border-bottom: 1px solid #ababab; border-right: 1px solid #ababab; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Administrators Report</b></h1>
<p>&nbsp;</p>
<?php
$sql = "SELECT a.tkid FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$modref."_list_admins a ";
$sql.= "WHERE a.tkid = t.tkid AND t.tkstatus <> 0 AND t.tkid <> '0000' AND a.type = 'FIN' AND a.yn = 'Y'";
include("inc_db_con.php");
$fin = array();
while($row = mysql_fetch_array($rs))
{
    $fin[] = $row['tkid'];
}
mysql_close($con);
$sql = "SELECT a.tkid FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$modref."_list_admins a ";
$sql.= "WHERE a.tkid = t.tkid AND t.tkstatus <> 0 AND t.tkid <> '0000' AND a.type = 'KPI' AND a.yn = 'Y'";
include("inc_db_con.php");
$kpi = array();
while($row = mysql_fetch_array($rs))
{
    $kpia[] = $row['tkid'];
}
mysql_close($con);
$sql = "SELECT a.tkid FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$modref."_list_admins a ";
$sql.= "WHERE a.tkid = t.tkid AND t.tkstatus <> 0 AND t.tkid <> '0000' AND a.type = 'TL' AND a.yn = 'Y'";
include("inc_db_con.php");
$kpi = array();
while($row = mysql_fetch_array($rs))
{
    $tl[] = $row['tkid'];
}
mysql_close($con);
?>
<table cellpadding=3 cellspacing=0 width=570>
	<tr>
		<td class=tdheader width=30>Ref</td>
		<td class=tdheader width=170>User</td>
		<td class=tdheader width=370>Administrative Access</td>
	</tr>
	<?php
    $sql = "SELECT DISTINCT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$modref."_list_admins a ";
    $sql.= "WHERE t.tkid = a.tkid AND t.tkstatus <> 0 AND t.tkid <> '0000' AND a.yn = 'Y' ";
    $sql.= "ORDER BY t.tkname, t.tksurname";
    include("inc_db_con.php");
    if(mysql_num_rows($rs)==0)
    {
    ?>
	<tr>
		<td class=tdgeneral colspan=4>No Administrators available.  Please use the Setup - Administrators page to add an Administrator.</td>
	</tr>
    <?php
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['tkid'];
            $val = $row['tkname']." ".$row['tksurname'];
            include("inc_tr.php");
    ?>

		<td class=tdheader valign=top><?php echo($id); ?></td>
		<td class=tdgeneral valign=top><b><?php echo($val); ?></b></td>
		<td class=tdgeneral valign=top><ul style="margin: 2 0 -4 40;">
		  <?php
		  $style = "margin: 0 0 2 -35; padding: 0 0 0 0;";
		  $style2 = "margin: 4 0 -3 -15; padding: 0 0 0 0;";
		  if(in_array($id,$tl)) { echo("<li style=\"list-style-image: url(/pics/bullet4o.gif);\">Top Level Administrator</li>"); $style = $style2; }
		  if(in_array($id,$fin)) { echo("<li style=\"list-style-image: url(/pics/bullet4o.gif);\">Finance Administrator</li>"); $style = $style2; }
		  if(in_array($id,$kpia)) { echo("<li style=\"list-style-image: url(/pics/bullet4o.gif);\">KPI Administrator</li>"); $style = $style2; }
		  $sql2 = "SELECT d.* FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_".$modref."_dir d ";
          $sql2.= "WHERE a.tkid = '".$id."' AND a.yn = 'Y' AND a.type = 'DIR' AND a.ref = d.dirid AND d.diryn = 'Y' ORDER BY d.dirsort";
		  include("inc_db_con2.php");
		  if(mysql_num_rows($rs2)>0) { echo("<p style=\"".$style."\"><u>Directorates</u></p>");  $style = $style2; }
		  while($row2 = mysql_fetch_array($rs2))
		  {
            echo("<li style=\"list-style-image: url(/pics/bullet1r.gif);\">".$row2['dirtxt']."</li>");
		  }
		  mysql_close($con2);
		  $sql2 = "SELECT s.*, d.dirtxt FROM assist_".$cmpcode."_".$modref."_list_admins a, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s ";
          $sql2.= "WHERE a.tkid = '".$id."' AND a.yn = 'Y' AND a.type = 'SUB' AND a.ref = s.subid AND s.subyn = 'Y' AND d.dirid = s.subdirid AND d.diryn = 'Y' ORDER BY s.subsort";
		  include("inc_db_con2.php");
		  if(mysql_num_rows($rs2)>0) { echo("<p style=\"".$style."\"><u>Sub-Directorates</u></p>"); }
		  while($row2 = mysql_fetch_array($rs2))
		  {
            echo("<li style=\"list-style-image: url(/pics/bullet2g.gif);\">".$row2['subtxt']." (".$row2['dirtxt'].")</li>");
		  }
		  mysql_close($con2);
            ?></ul></td>
	</tr>
    <?php
        }
    }
    mysql_close($con);
    ?>
</table>
<?php
$urlback = "setup_admin.php";
include("inc_goback.php");
?>

<p>&nbsp;</p>
</body>
</html>
