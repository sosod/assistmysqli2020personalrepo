<?php
    include("inc_ignite.php");
$dlist['kpicalctype']=array('name'=>"KPI Calculation Type",'id'=>"kpicalctype",'code'=>"Y",'yn'=>"Y",'headfield'=>"kpicalctype",'kpi'=>"kpicalctypeid",'fld'=>"code");
$dlist['kpitargettype']=array('name'=>"Target Type",'id'=>"targettype",'code'=>"Y",'fld'=>"code",'yn'=>"Y",'headfield'=>"kpitargettype",'kpi'=>"kpitargettypeid");
foreach($dlist as $dl) {
		$dld = getListKPI($dl);
		$dlist[$dl['headfield']]['data'] = $dld;
}

$nosort = array("kpioutcome","kpichallenges","kpiriskdef","kpiriskrate","wards","area","kpicpmun","kpibaseline","kpitargetunit","kpipoe","kpicalctype","kpitargettype","kpistratop","kpimsr");
$nogroup = array("dir","kpimsr","kpivalue","kpidefinition","kpiidpnum");

$head = array();
$head['dir'] = array('headfield'=>"dir",'headdetail'=>"Directorate");
$head['sub'] = array('headfield'=>"sub",'headdetail'=>"Sub-Directorate");
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'KPI' AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
include("inc_db_con.php");
	while($row = mysql_fetch_array($rs)) {
        $head[$row['headfield']] = $row;
	}
mysql_close($con);
$head['kpitargettype'] = array(
	'headtype'=>'KPI',
	'headyn'=>'Y',
	'headdetail'=>'Target Type',
	'headfield'=>'kpitargettype',
	'headmaxlen'=>-1
);

/* RESET VALUES = > no report action & no existing report */
function resetFields(&$field,&$filter,&$sort,&$dlistfilter,&$group) {
	global $head;
	global $nosort;
	global $dlist;
	global $timearr;
		$group = "X";
		foreach($head as $h) {
			$hf = $h['headfield'];
			$field[$hf] = array($hf,"Y");
			if($hf == "kpimsr") {
				$filter[$hf] = array($hf,"X","");
			} elseif($dlist[$hf]['yn']=="Y" && count($dlist[$hf]['data'])<6 && $hf != "natkpa" && $hf != "munkpa") {
				foreach($dlist[$hf]['data'] as $d) {
					$dlistfilter[$hf][] = $d['id'];
				}
				$filter[$hf] = array($hf,strFn("implode",$dlistfilter[$hf],"|",""),"any");
			} elseif($hf=="wards" || $hf == "area") {
				$filter[$hf] = array($hf,"X","any");
				$dlistfilter[$hf][0] = "X";
			} elseif($hf=="sub") {
				$filter[$hf] = array($hf,array("ALL"),"");
			} else {
				$filter[$hf] = array($hf,"","any");
			}
			if(!in_array($hf,$nosort)) {
				$sort[] = $hf;
			}
		}
		$filter['kpiresult'] = array("kpiresult","ALL","");
		$reshead = array("results","krresult","krprogress","krmanage","krsum","krgloss");
		foreach($reshead as $hf) {
			$field[$hf] = array($hf,"Y");
		}
		$field['cf'] = array("cf",1);
		/*if($today>$timearr[12]['eval']) {
			$ct = 12; 
		} else {
			$ct = date("m");
			if($ct > 6 ) $ct-=6; else $ct+=6;
		}*/ $ct = 12;
		$field['ct'] = array("ct",$ct);
}


//echo "<pre>";
$result = array();
$field = array();
$filter = array();			//filter[headfield] = array(headfield,filter,filtertype);
$sort = array();
$var = $_REQUEST;
//print_r($var);
if($var['act']=="SAVE" || $var['act']=="EDIT") {
	/**************** SAVE NEW REPORT ***********************/
	//format    field: headfield1|=|value1|_|     filter: headfield|=|filter|=|filtertype|_|
	$field = array();
	$filter = array();
	$field['dir'] = array("dir",$var['dir']);
	$field['sub'] = array("sub",$var['sub']);
	$field['results'] = array("results",$var['results']);
	$field['krresult'] = array("krresult",$var['krresult']);
	$field['krprogress'] = array("krprogress",$var['krprogress']);
	$field['krmanage'] = array("krmanage",$var['krmanage']);
	$field['cf'] = array("cf",$var['cf']); //results from
	$field['ct'] = array("ct",$var['ct']); //results to
	$field['krsum'] = array("krsum",$var['krsum']);
	$filter['dir'] = array("dir",$var['dirfilter']);
	$filter['sub'] = array("sub",strFn("implode",$var['subfilter'],"|",""));
	$dlistfilter['sub'] = $var['subfilter'];
	$sort = $var['sort'];
	$sortby = strFn("implode",$sort,"|_|","");
	$output = $var['output'];
	$groupby = $var['groupby'];
	$name = code($var['rname']);
	$rhead = code($var['rhead']);
	$field['krsum'] = array("krsum",$var['krsum']);
	$filter['kpiresult'] = array("kpiresult",$var['kpiresultfilter'],"");
	$field['krgloss'] = array("krgloss",$var['krgloss']);
	foreach($head as $row) {
		$hf = $row['headfield'];
		if($hf !="dir" && $hf != "sub") {
		$field[$hf] = array($hf,$var[$hf]);
		if($dlist[$hf]['yn']=="Y" || $hf == "wards" || $hf == "area") {
			$filter[$hf] = array();
			$filter[$hf][0] = $hf;
			$filter[$hf][1] = strFn("implode",$var[$hf.'filter'],"|","");
			$filter[$hf][2] = $var[$hf.'filtertype'];
			$dlistfilter[$hf] = $var[$hf.'filter'];
		} else {
			$filter[$hf] = array($hf,code($var[$hf.'filter']),$var[$hf.'filtertype']);
		}
		}
    }
//print_r($filter);
	//CONVERT FROM ARRAY TO STRING
	$field2 = array();
	$filter2 = array();
	foreach($field as $f) {
		$field2[] = strFn("implode",$f,"|=|","");
	}
	$fields = strFn("implode",$field2,"|_|","");
	foreach($filter as $f) {
		$filter2[] = strFn("implode",$f,"|=|","");
	}
	$filters = strFn("implode",$filter2,"|_|","");
	//SAVE TO DB
	if($var['act']=="EDIT") {
		if(checkIntRef($var['i'])) {
			$sql = "UPDATE ".$dbref."_reports SET rname = '$name', rhead = '$rhead', fields = '$fields', filters = '$filters', sort_by = '$sortby', group_by = '$groupby', export_to = '$output', modified_on = $today";
			$sql.= " WHERE id = ".$var['i'];
			include("inc_db_con.php");
			$result[0] = "check";
			$result[1] = "Departmental KPI Report '$name' has been updated. <input type=submit value=\"Generate Report\" onclick=\"document.kpireport.submit();\">";
			$var['a'] = "E";
			$report['id'] = $var['i'];
			$report['rname'] = $name;
			$report['rhead'] = $rhead;
		} else {
			$result[0] = "error";
			$result[1] = "An error occurred trying to update report '$name'.";
		}
	} else {
		$sql = "INSERT INTO ".$dbref."_reports (tkid,rtype,rname,rhead,fields,filters,sort_by,group_by,export_to,yn,added_on,modified_on) ";
		$sql.= "VALUES ('$tkid','KPI','$name','$rhead','$fields','$filters','$sortby','$groupby','$output','Y',$today,0)";
		include("inc_db_con.php");
		$report['id'] = mysql_insert_id($con);
		$report['rname'] = $name;
		$report['rhead'] = $rhead;
		$var['a'] = "E";
		$result[0] = "check";
		$result[1] = "Departmental KPI Report '$name' has been saved. <input type=submit value=\"Generate Report\" onclick=\"document.kpireport.submit();\">";
	}
} elseif($var['act']=="DEL" && checkIntRef($var['i'])) {
	$name = $var['rname'];
			$sql = "UPDATE ".$dbref."_reports SET yn = 'N', modified_on = $today";
			$sql.= " WHERE id = ".$var['i'];
			include("inc_db_con.php");
			$result[0] = "check";
			$result[1] = "Departmental KPI Report '$name' has been deleted.";
		/******************* SET DEFAULT INFO ************************/
		/* no report action & no existing report */
		resetFields($field,$filter,$sort,$dlistfilter,$groupby);
} else {
	if(checkIntRef($var['i'])) {
		/**************** GET REPORT DETAIL ********************/
		$report = array();
		$sql = "SELECT * FROM ".$dbref."_reports WHERE id = ".$var['i'];
		include("inc_db_con.php");
			$report = mysql_fetch_assoc($rs);
		mysql_close($con);
		if(count($report)==0) {
			$result[0] = "error";
			$result[1] = "Report not found.";
			$var['a'] = "E";
			$var['i'] = 0;
		} else {
			//CONVERT DB INFO INTO ARRAYS
			$fields = $report['fields'];
			$filters = $report['filters'];
			$sort = strFn("explode",$report['sort_by'],"|_|","");
			$groupby = $report['group_by'];
			$output = $report['export_to'];
			//FIELD
			$fields2 = strFn("explode",$fields,"|_|","");
			foreach($fields2 as $f) {
				$f2 = strFn("explode",$f,"|=|","");
				$field[$f2[0]] = array($f2[0],$f2[1]);
			}
			//FILTER & FILTERTYPE
			$filter2 = strFn("explode",$filters,"|_|","");
			foreach($filter2 as $f) {
				$f2 = strFn("explode",$f,"|=|","");
				$filter[$f2[0]] = array($f2[0],$f2[1],$f2[2]);
				if($dlist[$f2[0]]['yn']=="Y" || $f2[0] == "wards" || $f2[0] == "area" || $f2[0] == "sub") {
					$dlistfilter[$f2[0]] = strFn("explode",$f2[1],"|","");
				}
			}
		}
	} else {
		/******************* SET DEFAULT INFO ************************/
		resetFields($field,$filter,$sort,$dlistfilter,$groupby);
	}
}
//echo "<pre>"; 
//print_r($filter);
//print_r($field);
//print_r($field); print_r($head); 
//print_r($dlistfilter);
//echo "</pre>";
?>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<?php include("inc_head_msie.php"); ?>
<style type=text/css>
    table {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
    }
    table td {
        border-width: 0px;
        border-style: solid;
        border-color: #ababab;
        border-bottom: 0px solid #ababab;
    }
	.b-w {
		border: 0px solid #ffffff;
	}
	.filter {
		vertical-align: top;
		padding: 10 10 10 3;
	}
	.head {
		font-weight: bold;
	}
</style>
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.0em; height: 18px; width: 200px;}
	#sortable li span { position: absolute; margin-left: -1.3em; }
	.ui-state-highlightsort { height: 1.5em; line-height: 1.2em; }
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({
			placeholder: "ui-state-highlightsort"
		});
		$( "#sortable" ).disableSelection();
	});
	</script>

<script type=text/javascript>
function dirSub(me) {
        var dir = me.value;
            var sub = document.getElementById('sf');
            sub.length = 0;
            var o = 1;
            var ds = false;
            var opt = new Array();
			if(subssel["ALL"]=="Y") { ds = true; }
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,ds);
			ds = false;
			if(dir.length>0 && !isNaN(parseInt(dir))) {
				var sf = subs[dir];
				for(s in sf)
				{
					if(subssel[sf[s][0]]=="Y") { ds = true; }
					sub.options[o] = new Option(sf[s][1],sf[s][0],false,ds);
					ds = false;
					o++;
				}
			} else {
				var sf = Array();
				for(sd in subs) {
					sf = subs[sd];
					for(s in sf)
					{
						if(subssel[sf[s][0]]=="Y") { ds = true; }
						sub.options[o] = new Option(sf[s][1],sf[s][0],false,ds);
						ds = false;
						o++;
					}
				}
			}
			var sel = "N";
			for(i=0;i<sub.options.length;i++) {
				if(sub.options[i].selected) { sel = "Y"; }
			}
			if(sel == "N") { sub.value = "ALL"; }
/*        var dir = me.value;
        if(dir.length>0 && !isNaN(parseInt(dir)))
        {
            var sub = document.getElementById('sf');
            sub.length = 0;
            var sf = subs[dir];
            var o = 1;
            var ds = false;
            var opt = new Array();
			if(subssel["ALL"]=="Y") { ds = true; }
            sub.options[0] = new Option("All Sub-Directorates","ALL",true,ds);
			ds = false;
            for(s in sf)
            {
				if(subssel[sf[s][0]]=="Y") { ds = true; }
                sub.options[o] = new Option(sf[s][1],sf[s][0],false,ds);
				ds = false;
                o++;
            }
			var sel = "N";
			for(i=0;i<sub.options.length;i++) {
				if(sub.options[i].selected) { sel = "Y"; }
			}
			if(sel == "N") { sub.value = "ALL"; }
        }*/
}
function checks(c,e) {
	var act;
	if(c=="S") {
		for(i=0;i<e;i++) {	
			if(i<19 || (i > 20 && i < 23) || (i > 24 && i < 27) || (i > 28 && i < 31)) { act = true; } else { act = false; }
			document.forms[0].elements[i].checked = act;	
		}
	} else {
		if(c=="U") { act = false; } else { act = true; }
		for(i=0;i<e;i++) {	if(document.forms[0].elements[i].nodeName!='SELECT') { document.forms[0].elements[i].checked = act; } }
	}
}
function saveReport(act) {
	switch(act) {
		case "D":
			document.kpireport.act.value = "DEL";
			break;
		case "E":
			document.kpireport.act.value = "EDIT";
			break;
		case "N":	//NEW REPORT
		default:
			document.kpireport.act.value = "SAVE";
	}
	document.kpireport.action = "report_kpi.php";
	document.kpireport.submit();
}
</script>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Support >> Other Reports >> Departmental SDBIP</b></h1>
<?php 
	displayRes($result); 
	
	if($var['a']=="V") {
		echo "<p style=\"text-align: center; font-weight: bold; font-size: 16pt;\">Generating report....</p>";
	}
	
?>

<form name=kpireport method=post action=support_report_kpi_process.php>
<?php
/*********** 1. Fields *********/
echo "<h3 class=fc>1. Select the information to be displayed in the report:</h3>";
echo "<div style=\"margin-left: 17px\">";
echo "<table cellpadding=3 cellspacing=0>";
    echo "<tr>";
        echo "<td valign=top width=33.3% style=\"border-bottom: 0px\">";
            echo "<table cellpadding=3 cellspacing=0  style=\"border-width: 0px\">";
	$mnr = count($head)+3;
    $m2 = $mnr / 3;
    $m = round($m2);
    if($m < $m2) { $m++; }
    $r=0;
	foreach($head as $row)
    {	if($row['headfield']!="kpitargettype") {
        $r++;
        if($r > $m) {
            $r=1;
			//CLOSE PREVIOUS COLUMN
            echo "</table>";
			echo "</td>";
			//OPEN NEW COLUMN
			echo "<td valign=top width=33.3% style=\"border-bottom: 0px\">";
            echo "<table cellpadding=3 cellspacing=0 style=\"border: 0px\">";
        }
			//DISPLAY HEADROW
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center><input type=checkbox "; if($field[$row['headfield']][1]=="Y") { echo "checked"; } echo " name=\"".$row['headfield']."\" value=Y></td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\">".$row['headdetail']."</td>";
            	echo "</tr>";
    } }
mysql_close($con);
$timearr = array();
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $timearr[$row['sort']]['sort'] = $row['sort'];
        $timearr[$row['sort']]['val'] = date("M-y",$row['eval']);
    }
mysql_close($con);
			//KPI RESULTS HEADROW
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center><input type=checkbox "; if($field['results'][1]=="Y") { echo "checked"; } echo " name=\"results\" value=Y></td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\">KPI Results<br>from <select name=cf id=cf>";
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo "<option ";
                        if($t == $field['cf'][1]) { echo " selected "; }
                        echo "value=".$tim['sort'].">".$tim['val']."</option>";
                        $t++;
                    }
                    echo "</select> to <select name=ct id=ct>";
                    $t = 1;
                    foreach($timearr as $tim)
                    {
                        echo "<option ";
                        if($t == $field['ct'][1]) { echo " selected "; }
                        echo "value=".$tim['sort'].">".$tim['val']."</option>";
                        $t++;
                    }
                    echo "</select>"; 
					echo "</td>";
            	echo "</tr>";
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center>&nbsp;</td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\"><input type=checkbox "; if($field['krresult'][1]=="Y") { echo "checked"; } echo " name=\"krresult\" value=Y>&nbsp;Monthly Targets, Actuals & Results</td>";
            	echo "</tr>";
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center>&nbsp;</td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\"><input type=checkbox "; if($field['krprogress'][1]=="Y") { echo "checked"; } echo " name=\"krprogress\" value=Y>&nbsp;Monthly Progress Comment</td>";
            	echo "</tr>";
            	echo "<tr>";
            		echo "<td style=\"border-bottom: 0px\" align=center>&nbsp;</td>";
            		echo "<td style=\"border-bottom: 0px; padding-left: 10px;\"><input type=checkbox "; if($field['krmanage'][1]=="Y") { echo "checked"; } echo " name=\"krmanage\" value=Y>&nbsp;Monthly Management Comment</td>";
            	echo "</tr>";
            echo "</table>";
        echo "</td>";
    echo "</tr>";
echo "</table>";
$hend = count($head) + 4 + 1;
	//check/uncheck options
	echo "<p style=\"margin-top: 3px; font-size: 6.5pt; line-height: 7pt;\">";
		echo "<span onclick=\"checks('C',$hend)\" style=\"text-decoration: underline; cursor: hand;\" class=fc>Check All</span>";
		echo " | ";
		echo "<span onclick=\"checks('U',$hend)\" style=\"text-decoration: underline; cursor: hand;\" class=fc>Uncheck All</span>";
	echo "</p>";
echo "<p>Include Summary of Results? <input type=checkbox value=Y name=krsum "; if($field['krsum'][1]=="Y") { echo "checked"; } echo "></p>";
echo "</div>";


/********** 2. FILTERS *********/
echo "<h3 class=fc>2. Select the filter you wish to apply:</h3>";
echo "<div style=\"margin-left: 17px\">";
echo "<table cellspacing=0 cellpadding=3>";
    foreach($head as $hrow)
    {
		$hf = $hrow['headfield'];
        $i=0;
		include("inc_tr.php");	//hover
		//echo "<tr>";
		echo "<td class=\"filter head\">".$hrow['headdetail'].":&nbsp;";
		if($hf=="wards") { echo "<br /><a href=glossary.php#wards target=_blank><img src=/pics/help.gif style=\"border: 0px;\"></a>"; }
		echo "</td>";
		if($dlist[$hf]['yn']=="Y" && $hf != "dir" && $hf != "sub") {
			$dl = $dlist[$hf];
			$s = count($dl['data'])+1;
            echo "<td class=filter>";
			if($s>6 || $hf == "natkpa" || $hf == "munkpa") { $s = 6; 
                echo "<select multiple name=\"".$hf."filter[]\" size=$s ><option "; if(in_array("all",$dlistfilter[$hf],true) || count($dlistfilter[$hf])==0) { echo "selected "; } echo " value=all>All</option>";
				foreach($dl['data'] as $d) {
					if($hrow['headfield']=="kpicalctype") {
						$i = $d['code'];
						$v = $d['value'];
					} else {
						$i = $d['id'];
						$dval = decode($d['value']);
						if(strlen($dval)>130) { $dval = strFn("substr",$dval,0,127)."..."; }
						if($hf=="kpitargettype" || $dl['code']=="N") {
							$v = $dval;
						} else {
							$v = $dval." (".$d['code'].")";
						}
					}
					echo "<option ";
					if(in_array($i,$dlistfilter[$hf],true)) { echo "selected "; }
					echo "value=".$i.">".$v."</option>";
				}
                echo "</select><input type=hidden name=\"".$hf."filtertype\" value=any>";
                echo "<br><i><small>Use CTRL key to select multiple options</small></i>";
			} else {	//if size<6 then use checkboxes
				$echo = "";
				foreach($dl['data'] as $d) {
					if($hrow['headfield']=="kpicalctype") {
						$i = $d['code'];
						$v = $d['value'];
					} else {
						$i = $d['id'];
						$dval = decode($d['value']);
						if(strlen($dval)>130) { $dval = strFn("substr",$dval,0,127)."..."; }
						if($hf=="kpitargettype" || $dl['code']=="N") {
							$v = $dval;
						} else {
							$v = $dval." (".$d['code'].")";
						}
					}
					$echo.="<input type=checkbox name=\"".$hf."filter[]\" id=\"".$hf.$i."\" ";
					if(in_array($i,$dlistfilter[$hf],true)) { $echo.="checked "; }
					$echo.="value=".$i."><label for=\"".$hf.$i."\">".$v."</label><br />";
				}
				echo strFn("substr",$echo,0,strlen($echo)-6);
			}
			echo "</td>";
		} else {
        switch($hrow['headfield'])
        {
			case "dir":	//filter is set via javascript at the end
                echo "<td class=filter>";
				echo "<select name=dirfilter id=df onchange=\"dirSub(this);\">";
				echo "<option selected value=ALL>All Directorates</option>";
				$js = "";
				$sql = "SELECT DISTINCT d.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
				$sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
				$sql.= ", assist_".$cmpcode."_".$modref."_kpi k ";
				$sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND k.kpiyn = 'Y' ";
				$sql.= " AND s.subdirid = d.dirid";
				$sql.= " AND s.subid = k.kpisubid";
				$sql.= " ORDER BY d.dirsort, s.subsort";
				include("inc_db_con.php");
					while($row = mysql_fetch_array($rs)) {
						echo "<option value=".$row['dirid'].">".$row['dirtxt']."</option>";
						$js.= "subs[".$row['dirid']."] = new Array();".chr(10);
					}
				mysql_close($con);
				echo "</select></td>";
				break;
			case "sub":	//filter is set via javascript at the end
				//if(!checkIntRef($filter['dir'][1]) && count($filter['sub'][1])>0) { $filter['sub'][1][] = "ALL"; } 
                echo "<td class=filter>"; //print_r($filter['sub']);
				echo "<select name=subfilter[] id=sf size=6 multiple>";
				echo "<option "; if(in_array("ALL",$dlistfilter['sub']) || count($dlistfilter['sub'])==0) { echo "selected "; } echo " value=ALL>All Sub-Directorates</option>";
				$sql = "SELECT DISTINCT s.* FROM assist_".$cmpcode."_".$modref."_dirsub s";
				$sql.= ", assist_".$cmpcode."_".$modref."_dir d ";
				$sql.= ", assist_".$cmpcode."_".$modref."_kpi k ";
				$sql.= "WHERE s.subyn = 'Y' AND d.diryn = 'Y' AND k.kpiyn = 'Y' ";
            $sql.= " AND s.subdirid = d.dirid";
            $sql.= " AND s.subid = k.kpisubid";
            $sql.= " ORDER BY d.dirsort, s.subsort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $did2 = $row['subdirid'];
                    $sort2 = $row['subsort'];
                    $id2 = $row['subid'];
                    $val2 = $row['subtxt'];
                    echo "<option "; if(in_array($row['subid'],$dlistfilter['sub'])) { echo " selected "; } echo " value=".$row['subid'].">".$row['subtxt']."</option>";
                    $js.= "subs[".$did2."][".$sort2."] = new Array(".$id2.",\"".html_entity_decode($val2, ENT_QUOTES, "ISO-8859-1")."\");".chr(10);
                }
            mysql_close($con);
			echo "</select><br><i><small>Use CTRL key to select multiple options</small></i></td>";
				break;
            case "area":
                echo "<td class=filter>";
				echo "<table cellpadding=3 style=\"border: 0px;\"><tr><td width=25% style=\"border: 0px;\">";
				//echo "<input type=checkbox name=areafilter[] value=X "; if(count($dlistfilter[$hf])==0 || in_array("X",$dlistfilter[$hf],true)) { echo "checked "; } echo "> Any&nbsp;<br />";
                echo "<input type=checkbox name=areafilter[] value=1 "; if(in_array("1",$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> 'All'&nbsp;<br />";
				$cols = 1;
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_area WHERE yn = 'Y' AND value <> 'All' ORDER BY value";
                        include("inc_db_con2.php");
                            $wmnr = mysql_num_rows($rs2)+2;
                            $c2 = $wmnr / 4;
                            $c = round($c2);
                            if($c < $c2) { $c++; }
                            $w=1;
                                            while($row2 = mysql_fetch_array($rs2))
                                            {
                                                $w++;
                                                if($w>$c)
                                                {
                                                    echo("</td><td valign=top width=25% style=\"border: 0px;\">"); $cols++;
                                                    $w = 1;
                                                }
                                                echo "<input type=checkbox name=areafilter[] value=".$row2['id']." "; if(in_array($row2['id'],$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> ".$row2['value']."<br>";
                                            }
                        mysql_close($con2);
				echo "</td></tr></table>";
				echo "<select name=\"".$hf."filtertype\"><option "; if($filter[$hf][2]!="all") { echo "selected"; } echo " value=any>Match any selected</option><option "; if($filter[$hf][2]=="all") { echo "selected"; } echo " value=all>Match all selected</option></select>";
				break;
            case "wards":
                echo "<td class=filter>";
				echo "<table cellpadding=3 style=\"border: 0px;\"><tr><td width=25% style=\"border: 0px;\">";
//				echo "<input type=checkbox name=wardsfilter[] value=X "; if(count($dlistfilter[$hf])==0 || in_array("X",$dlistfilter[$hf],true)) { echo "checked "; } echo "> Any&nbsp;<br>";
                echo "<input type=checkbox name=wardsfilter[] value=1 "; if(in_array("1",$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> 'All'&nbsp;<br>";
				$cols = 1;
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_wards WHERE yn = 'Y' AND value <> 'All' ORDER BY numval";
                        include("inc_db_con2.php");
                            $wmnr = mysql_num_rows($rs2)+2;
                            $c2 = $wmnr / 4;
                            $c = round($c2);
                            if($c < $c2) { $c++; }
                            $w=1;
                                            while($row2 = mysql_fetch_array($rs2)) {
                                                $w++;
                                                if($w>$c)
                                                {
                                                    echo("</td><td valign=top width=25% style=\"border: 0px;\">"); $cols++;
                                                    $w = 1;
                                                }
                                                echo "<input type=checkbox name=wardsfilter[] value=".$row2['id']." "; if(in_array($row2['id'],$dlistfilter[$hf],true) || $dlistfilter[$hf][0] == "X") { echo "checked "; } echo "> ".$row2['numval']."<br>";
                                            }
                        mysql_close($con2);
				echo "</td></tr></table>";
				echo "<select name=\"".$hf."filtertype\"><option "; if($filter[$hf][2]!="all") { echo "selected"; } echo " value=any>Match any selected</option><option "; if($filter[$hf][2]=="all") { echo "selected"; } echo " value=all>Match all selected</option></select>";
                break;
            case "kpimsr":
                echo "<td class=filter>";
				echo "<input type=radio name=\"".$hrow['headfield']."filter\" id=msrX "; if($filter[$hf][1]=="X") { echo "checked"; } echo " value=X> <label for=msrX>Doesn't matter</label><br />";
				echo "<input type=radio name=\"".$hrow['headfield']."filter\" id=msrY "; if($filter[$hf][1]=="Y") { echo "checked"; } echo " value=Y> <label for=msrY>Linked to Top Level only</label><br />";
				echo "<input type=radio name=\"".$hrow['headfield']."filter\" id=msrN "; if($filter[$hf][1]=="N") { echo "checked"; } echo " value=N> <label for=msrN>Not linked to Top Level</label></td>";
                break;
            default:
                echo "<td class=filter>";
				echo "<input type=text name=\"".$hrow['headfield']."filter\" size=30 value=\"".decode($filter[$hf][1])."\">&nbsp;<select  name=\"".$hrow['headfield']."filtertype\">";
				echo "<option "; if($filter[$hf][2]=="all") { echo "selected"; } echo " value=all>Match all words</option>";
				echo "<option "; if($filter[$hf][2]=="any") { echo "selected"; } echo " value=any>Match any word</option>";
				echo "<option "; if($filter[$hf][2]=="exact") { echo "selected"; } echo " value=exact>Match exact phrase</option></td>";
                break;
        } //switch
		} //dlist
        echo "</tr>";
    }
	include("inc_tr.php");
        echo "<td class=\"filter head\">";
		echo "Capital Projects:</td>";
		echo "<td class=filter>";
			echo "<input type=radio name=kpicpid value=ALL checked id=cpa> <label for=cpa>All KPIs</label><br />";
			echo "<input type=radio name=kpicpid value=0 id=cp0> <label for=cp0>Operational KPIs only</label><br />";
			echo "<input type=radio name=kpicpid value=1 id=cp1> <label for=cp1>KPIs associated with Capital Projects only</label>";
        echo "</td>";
    echo "</tr>";
	include("inc_tr.php");
		echo "<td class=\"filter head\">";
		echo "KPI Result:</td>";
		echo "<td class=filter>";
                echo "<select name=kpiresultfilter>";
					echo "<option "; if($filter['kpiresult'][1]=="ALL" || strlen($filter['kpiresult'][1])==0 || !isset($filter['kpiresult'][1])) { echo " selected "; } echo " value=ALL>All</option>";
					foreach($krsetup as $krs) {
						echo "<option "; if($filter['kpiresult'][1]==$krs['sort']) { echo " selected "; } echo " value=".$krs['sort'].">".$krs['value']."</option>";
					}
/*					echo "<option selected value=ALL>All</option>";
					foreach($krsetup as $krs) {
						echo "<option value=".$krs['sort'].">".$krs['value']."</option>";
					}*/
                echo "</select> <img src=/pics/help.gif style=\"cursor:hand;\" onclick=\"javascript:window.open('glossary.php#krr','Glossary','width=600,height=300');\">";
				echo "<br /><input type=checkbox name=krgloss value=Y "; if($field['krgloss'][1]=="Y") { echo "checked"; } echo "> Include explanation of KPI Result classification in report?";
				echo "<br /><span style=\"font-style: italic;\">This will only display if you select KPI Results in Step 1.</span>";
        echo "</td>";
    echo "</tr>";
	include("inc_tr.php");
        echo "<td class=\"filter head\">";
		echo "KPI Status:<br /><span style=\"font-weight: normal; font-style: italic\">(Since SDBIP load on<br />Ignite Assist)</span></td>";
		echo "<td class=filter>";
			echo "<input type=radio name=status value=D checked id=stata> <label for=stata>Deleted</label><br />";
			echo "<input type=radio name=status value=C id=stat0> <label for=stat0>Added (and not deleted)</label><br />";
			echo "<input type=radio name=status value=E id=stat1> <label for=stat1>Edited (and not deleted)</label>";
        echo "</td>";
    echo "</tr>";
echo "</table>";
echo "</div>";



/*************** SORT BY *******************/
echo "<h3 class=fc>3. Choose your group and sort options:</h3>";
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<td style=\"font-weight: bold;\">Group by:&nbsp;</td>";
			echo "<td><select name=groupby><option "; if($groupby=="X" || strlen($groupby)==0) { echo "selected"; } echo " value=\"X\">No grouping</option>";
			foreach($head as $h) {
				if(!in_array($h['headfield'],$nogroup) && !in_array($h['headfield'],$nosort)) {
					echo "<option "; if($groupby==$h['headfield']) { echo "selected"; } echo " value=\"".$h['headfield']."\">".$h['headdetail']."</option>";
				}
			}
			echo "</select></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td style=\"font-weight: bold; vertical-align: top\">Sort by:&nbsp;</td>";
			echo "<td>"; echo "<div class=demo><ul id=sortable>";
			foreach($sort as $hf) {
				echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$hf."\">".$head[$hf]['headdetail']."</li>";
			}
			echo "</ul></div></td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";



/********** OUTPUT ***************/
if(strlen($output)==0) { $output = "display"; }
echo "<h3 class=fc>4. Choose the document format of your report:</h3>";
echo "<div style=\"margin-left: 17px\">";
	echo "<table cellspacing=0 cellpadding=3 >";
		echo "<tr>";
			echo "<td width=30 class=\"b-w\" align=center><input type=radio name=\"output\" value=\"display\" "; if($output=="display") { echo "checked"; } echo " id=csvn></td>";
			echo "<td class=\"b-w\"><label for=csvn>Onscreen display</label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class=\"b-w\" align=center><input type=radio name=\"output\" value=\"csv\" "; if($output=="csv") { echo "checked"; } echo " id=csvy></td>";
			echo "<td class=\"b-w\"><label for=csvy>Microsoft Excel (Plain Text)</label></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class=\"tdgeneral b-w\" align=center><input type=radio name=\"output\" value=\"excel\" "; if($output=="excel") { echo "checked"; } echo " id=excely></td>";
			echo "<td class=\"tdgeneral b-w\"><label for=excely>Microsoft Excel (Formatted)*</label>";
			echo "</td>";
		echo "</tr>";
	echo "</table>";
echo "</div>";



/************** BUTTONS ***************/
echo "<h3 class=fc>5. Generate the report:</h3>";
	echo "<p style=\"margin-left: 17px\">";
	echo "<b>Report Title:</b> <input type=text name=rhead value=\"".decode($report['rhead'])."\" maxlength=100 size=70> <i><small>(Displays at the top of the report.)</small></i><br />";
	echo "<input type=hidden name=act value=VIEW>";
	echo "<input type=hidden name=i value=\"".$report['id']."\">";
	echo "<input type=submit value=\"Generate Report\" name=\"B1\">&nbsp;<input type=reset value=Reset>";
/*	echo "</p><p style=\"margin-left: 17px\">";
	echo "<b>Report Name:</b> <input type=text name=rname value=\"".decode($report['rname'])."\" maxlength=50 size=30> <i><small>(To identify the report in the quick report list.)</small></i><br />";
if(checkIntRef($report['id'])) {
		echo "<input type=button value=\"Save Changes\" onclick=\"saveReport('E')\">&nbsp;";
		echo "<input type=button value=\"Save As New Report\" onclick=\"saveReport('N')\">&nbsp;";
		echo "<input type=button value=\"Delete Report\" onclick=\"saveReport('D')\">&nbsp;";
} else {
	echo "<input type=button value=\"Save Report\" onclick=\"saveReport('A')\">&nbsp;";
}*/
	echo "</p>";
?>
</form>
<table cellspacing="0" cellpadding="5" width=700 style="border: 1px solid #AAAAAA;">
<tr>
<td style="font-size:8pt;border:1px solid #AAAAAA;">* Please note the following with regards to the formatted Microsoft Excel report:<br /><ol>
<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel. 
It is safe to click on the "Yes" button to open the document.</li>
</ol></td>
</tr></table>
<script type=text/javascript>
<?php 
	echo "var subs = new Array();";
	echo $js; 
	echo "var subssel = new Array();";
if(count($dlistfilter['sub'])>0) {
	foreach($dlistfilter['sub'] as $s) {
		if(!is_numeric($s)) {
			echo "subssel['".$s."'] = \"Y\";";
		} else {
			echo "subssel[".$s."] = \"Y\";";
		}
	}
}
if(checkIntRef($filter['dir'][1])) {
	echo "var targ = document.getElementById('df');";
	echo "targ.value = ".$filter['dir'][1].";";
	echo "dirSub(targ);";
}
if($var['a']=="V") {
	echo "document.kpireport.submit();";
} else {
}
?>
</script>
<?php
//echo "<pre>"; print_r($filter); print_r($dlistfilter);
?>
</body>

</html>
