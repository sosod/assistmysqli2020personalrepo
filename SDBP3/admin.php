<?php
    include("inc_ignite.php");

	$administ = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_admins WHERE yn = 'Y' AND tkid = '".$tkid."'";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs)) {
            $administ[$row['type']][$row['ref']] = "Y";
        }
    mysql_close($con);

	
if($setuptopleveltype=="TBL") {
	$sql = "SHOW TABLES LIKE '".$dbref."_toplevel'";
	include("inc_db_con.php");
		$tlmnr = mysql_num_rows($rs);
	mysql_close($con);
	if($tlmnr>0) {
	$subarr = array();
	$sql = "SELECT DISTINCT dirid, dirtxt FROM ".$dbref."_toplevel, ".$dbref."_dir WHERE topyn != 'C' AND diryn = 'Y' AND dirid = topsubid ORDER BY dirsort";
	include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) {
			$sval = decode($row['dirtxt']);
			if(strlen($sval)>30) { 
				$sval = substr($sval,0,29)."..."; 
				$row['dirtxt'] = code($sval);
			}
			$subarr[] = $row;
		}
	mysql_close($con);
	}
}
	?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ababab;
    border-width: 0px;
}
.tdheaderl { border-bottom: 1px solid #ababab; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    font-weight: normal;
}

.line1 { border-bottom: 1px solid #ababab; padding-left: 10px; }
.line2 { border-bottom: 1px solid #ababab; font-style: italic; padding-left: 30px; }
<?php
if(strFn("strpos",$_SERVER['HTTP_USER_AGENT'],"Firefox","")) {
	echo ".float { float: right; margin-right: 10px; }";
} else {
	echo ".float { float: right; margin-top: -17px; margin-right: 10px; }";
} ?>
</style>
<script type=text/javascript>
function autoTL() {
	if(confirm("This will update all Top Level KPIs from the Departmental SDBIP.\n\nAre you sure you wish to continue?\n\nNote: This update will happen automatically at midnight."))
		document.location.href = 'admin_tl_auto.php?act=preview';
}
function adminKPI() {
    var d = document.getElementById('kpidirid').value;
    if(!isNaN(parseInt(d)))
    {
        document.location.href = "admin_dskpi.php?d="+d;
    }
    else
    {
        alert("Please select which Directorate you wish to administer.");
    }
}
function updateTop() {
    var d = document.getElementById('tsid').value;
    if(!isNaN(parseInt(d)))
    {
        document.location.href = "admin_tl_update_list.php?s="+d;
    }
    else
    {
        alert("Please select which Sub-Directorate you wish to update.");
    }
}
function editTop(type) {
	if(type=="link") {
		var d = document.getElementById('etsid').value;
		var url = "admin_tl_edit_list.php?s="+d;
	} else {
		var d = document.getElementById('tl_edit2').value;
		var url = "admin_tl_edit2_list.php?s="+d;
	}
	if(!isNaN(parseInt(d)))
		document.location.href = url;
	else
		alert("Please select which Sub-Directorate you wish to update.");
}
</script>
<base target="main">

<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1>SDBIP <?php echo($modtxt); ?>: Admin</h1>
<?php
if(count($administ)==0)
	die("<P>You are not authorised to view this page.<br>Please login to <a href=http://assist.ignite4u.co.za/>Ignite Assist</a> and try again.</p>");

?>
<p>&nbsp;</p>
<table cellpadding=5 cellspacing=0 width=600>
    <?php
//FINANCE ADMIN
	if($administ['FIN'][0] == "Y") {
    ?>
    <tr>
        <td class=tdheaderl height=30>Finance Administration</td>
    </tr>
	<?php include("inc_tr.php"); ?>
		<td height=25 class=line1>Update financial information
			<input type=button value=Configure onclick="document.location.href = 'admin_fin.php';" class=float>
		</td>
	</tr>
	<?php
    }

//DIR ADMIN
if($tkuser=="support" && $tkname=="Ignite Support") {
        $sql = "SELECT d.dirid, d.dirtxt FROM assist_".$cmpcode."_".$modref."_dir d WHERE d.diryn = 'Y' ORDER BY d.dirsort";
} else {
        $sql = "SELECT DISTINCT d.dirid, d.dirtxt FROM assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_list_admins a WHERE d.diryn = 'Y' AND a.yn = 'Y' AND a.tkid = '".$tkid."' AND a.type = 'DIR' AND d.dirid = a.ref ORDER BY d.dirsort";
}
        include("inc_db_con.php");
            if(mysql_num_rows($rs)>0)
            {
                ?>
    <tr>
        <td class=tdheaderl height=30>Directorate Administration</td>
    </tr>
                <?php
            }
            while($row = mysql_fetch_array($rs))
            {
    ?>
	<?php include("inc_tr.php"); ?>
		<td class=line1><?php echo($row['dirtxt']); ?>
			<input type=button value=Configure onclick="document.location.href = 'admin_dir.php?d=<?php echo($row['dirid']); ?>';" class=float>
		</td>
	</tr>
	<?php
            }
        mysql_close($con);

//SUB ADMIN
        $sql = "SELECT DISTINCT s.subid, s.subtxt FROM assist_".$cmpcode."_".$modref."_dirsub s, assist_".$cmpcode."_".$modref."_list_admins a WHERE s.subyn = 'Y' AND a.yn = 'Y' AND a.tkid = '".$tkid."' AND a.type = 'SUB' AND s.subid = a.ref ORDER BY s.subsort";
        include("inc_db_con.php");
            if(mysql_num_rows($rs)>0)
            {
                ?>
    <tr>
        <td class=tdheaderl height=30>Sub-Directorate Administration</td>
    </tr>
                <?php
            }
            while($row = mysql_fetch_array($rs))
            {
    ?>
	<?php include("inc_tr.php"); ?>
		<td class=line1><?php echo($row['subtxt']); ?>
			<input type=button value=Configure onclick="document.location.href = 'admin_sub.php?s=<?php echo($row['subid']); ?>';" class=float></td>
	</tr>
	<?php
            }
        mysql_close($con);


//KPI ADMIN
    if($administ['KPI'][0] == "Y")
    {
    ?>
        <tr>
            <td class=tdheaderl height=30>KPI Administration</td>
        </tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Add KPI to all Directorates
			<input type=button value="    Add    " onclick="document.location.href = 'admin_kpi_add.php?r=k&t=op&s=0&d=0';" class=float></td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Update Directorate
			<span class=float><select id=kpidirid><option selected value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $dirt = html_entity_decode($row['dirtxt'], ENT_QUOTES, "ISO-8859-1");
                if(strlen($dirt)>30)
                {
                    $dirt = trim(substr($dirt,0,22))."...";
                }
                echo("<option value=".$row['dirid'].">".$dirt."</option>");
            }
        mysql_close($con);
        ?>
			</select> <input type=button value=" Update " id=kaup onclick="adminKPI();"></span>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Project Life Cycle
			<input type=button value=" Update " onclick="document.location.href = 'admin_kpi_plc.php?r=k&t=plc&s=0&d=0';" class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>PMS report
			<input type=button value="Generate" onclick="document.location.href = 'admin_kpi_pms.php?r=k&d=0';" class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>User Activity
			<input type=button value="   View   " onclick="document.location.href = 'admin_user_activity.php';" class=float>
		</td>
	</tr>
	<?php if($setuptopleveltype == "DOC") { 
	include("inc_tr.php"); ?>
		<td class=line1>Top Level
			<input type=button value="Update" style="margin-right: 10px;" onclick="document.location.href = 'admin_top.php';" class=float>
		</td>
	</tr>
    	<?php
		} //setuptopleveltype
    }	//KPIADMIN

//ASSURANCE PROVIDER
    if($administ['AP'][0] == "NOT DONE YET")
    {
    ?>
        <tr>
            <td width=570 class=tdheaderl height=30>Assurance Provider</td>
        </tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Review KPI
			<input type=button value="   View   " onclick="document.location.href = '';" id=a1 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Top Level Audit Log
			<input type=button value="   View   " onclick="document.location.href = '';" id=a2 class=float>
		</td>
	</tr>
	<?php
	}	//ASSURANCE PROVIDER
	
//$tlmnr = 0;
//TOPLEVEL ADMIN
    if(($administ['TL'][0] == "Y" && $tlmnr > 0 && $setuptopleveltype=="TBL") || ($tkuser == "support" && $tkname == "Ignite Support"))
    {
		//if($settopleveltype=="TBL" || ($tkuser == "support" && $tkname == "Ignite Support")) {
    ?>
        <tr>
            <td class=tdheaderl height=30>Top Level Administration</td>
        </tr>
	<?php if($setuptopleveltype == "TBL" && $tlmnr > 0) { ?>
	<?php include("inc_tr.php"); ?>
		<td class=line1>Manual Update
			<span class=float><select id=tsid><option value=X selected>--- SELECT ---</option><?php foreach($subarr as $s) { echo "<option value=".$s['dirid'].">".$s['dirtxt']."</option>"; } ?></select> <input type=button value=" Update " onclick="updateTop();" id=t1></span>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line1 height=30>Automatic Update:</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line2>Force Update from Departmental SDBIP
			<input type=button value=" Update " onclick="autoTL()" id=t5 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line2>History of updates done
			<input type=button value="   View   " onclick="document.location.href = 'admin_tl_auto_hist.php';" id=t6 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
		<td class=line2>Exception report
			<input type=button value="   View   " onclick="document.location.href = 'admin_tl_auto_exception.php';" id=t6 class=float>
		</td>
	</tr>
	<?php include("inc_tr.php"); ?>
 		<td class=line1>Edit Top Level KPI
			<span class=float><select id=etsid><option value=X selected>--- SELECT ---</option><?php foreach($subarr as $s) { echo "<option value=".$s['dirid'].">".$s['dirtxt']."</option>"; } ?></select> <input type=button value="    Edit    " onclick="editTop('link');" id=t2></span>
		</td>
	</tr>
	<?php //include("inc_tr.php"); ?>
<!--		<td style="border-bottom: 1px solid #ababab; padding-left: 10px;" width=<?php echo($width1); ?> class=tdgeneral>Add Top Level KPI</td>
		<td style="border-bottom: 1px solid #ababab;" width=<?php echo($width2); ?>  class=tdgeneral align=right><input type=button value="    Add    " onclick="document.location.href = '';" id=t3 style="margin-right: 10px;"></td>
	</tr>-->
	<?php include("inc_tr.php"); ?>
		<td class=line1>Audit Log
			<input type=button value="   View   " onclick="document.location.href = 'admin_tl_log_view.php';" id=t5 class=float>
		</td>
	</tr>
<?php 
	}	//topleveltype = tbl
} //TL ADMIN
//}	//describe top
  

    ?>
</table>
<p>&nbsp;</p>
</body>


</html>
