<?php 
	include("inc_ignite.php"); 
	
	$topid = $_REQUEST['t'];
	if(!checkIntRef($topid)) {
		die("<p>An error has occurred.  Please try again.");
	} else {
				/*** GET TOP DETAILS ***/
				$sql = "SELECT * FROM ".$dbref."_toplevel WHERE topid = $topid";
				include("inc_db_con.php");
				if(mysql_num_rows($rs)==0) {
					die("<p>An error has occurred.  Please try again.");
				} else {
					$top = mysql_fetch_assoc($rs);
				}
				mysql_close($con);
				$sql = "SELECT * FROM ".$dbref."_toplevel_wards WHERE twtopid = $topid AND twyn = 'Y'";
				include("inc_db_con.php");
				$wa['wards'] = array();
					while($row = mysql_fetch_assoc($rs)) {
						$wa['wards'][$row['twtopid']][$row['twwardsid']] = "Y";
					}
				mysql_close($con);	
				$sql = "SELECT * FROM ".$dbref."_toplevel_area WHERE tatopid = $topid AND tayn = 'Y' ";
				include("inc_db_con.php");
				$wa['area'] = array();
					while($row = mysql_fetch_assoc($rs)) {
						$wa['area'][$row['tatopid']][$row['taareaid']] = "Y";
					}
				mysql_close($con);
				$sql = "SELECT trtarget, trtimeid FROM ".$dbref."_toplevel_result WHERE trtopid = $topid ORDER BY trtimeid";
				include("inc_db_con.php");
				$tr = array();
				while($row = mysql_fetch_array($rs)) {
					$tr[$row['trtimeid']] = $row;
				}
				mysql_close($con);

		/** GET HEADINGS **/
		$head = array();
		$head_required = array();
		$sql = "SELECT headdetail, headfield, headrequired FROM ".$dbref."_headings WHERE headtype = 'TL' AND headdetailyn = 'Y' ORDER BY headdetailsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				$head[$row['headfield']] = $row;
				if($row['headrequired']=="Y") {
					$head_required[] = $row['headfield'];
				}
			}
		mysql_close($con);
		$head['topcalcaction'] = array('headdetail'=>"Calculation Action",'headfield'=>"topcalcaction");
		$head['toptargettypeid'] = array('headdetail'=>"Target type",'headfield'=>"toptargettypeid");
		$head['toptargetannual'] = array('headdetail'=>"Annual Target",'headfield'=>"toptargetannual");
		$head['toptargetrevised'] = array('headdetail'=>"Revised Target",'headfield'=>"toptargetrevised");
		//$head['assockpi'] = array('headlist'=>"Assoc. SDBIP KPIs",'headfield'=>"assockpi");
		$head['topsubid'] = array('headdetail'=>"Directorate",'headfield'=>"topsubid");
/************** GET LISTS *********************/
		$sql = "SELECT id, sval, eval, activeKPI as active FROM ".$dbref."_list_time WHERE id IN (1,3,4,6,7,9,10,12)";
		include("inc_db_con.php");
		$sval = 0;
			while($row = mysql_fetch_assoc($rs)) {
				$id = $row['id'];
				switch($id) {
					case 1: case 4: case 7: case 10:
						$sval = $row['sval'];
						break;
					case 3: case 6: case 9: case 12:
						$time[$id] = $row;
						$time[$id]['sval'] = $sval;
						break;
				}
			}
		mysql_close($con);
$lists = array();
$sql = "SELECT id, value FROM ".$dbref."_list_gfs WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['gfs'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_idpgoal WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['idp'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_kpicalctype WHERE yn = 'Y'";// ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$calctype[$row['code']] = $row;
	}
mysql_close($con);
$calcaction = array(
	'SUM'=> array('id'=>"SUM",'value'=>"Sum",'code'=>"SUM"),
	'AVE'=> array('id'=>"AVE",'value'=>"Average",'code'=>"AVE"),
);
$sql = "SELECT id, value, code FROM ".$dbref."_list_munkpa WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['munkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_natkpa WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['natkpa'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_targettype WHERE yn = 'Y'";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$targettype[$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, code FROM ".$dbref."_list_tas WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['tas'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value, numval FROM ".$dbref."_list_wards WHERE yn = 'Y' ORDER BY numval";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['wards'][$row['id']] = $row;
	}
mysql_close($con);
$sql = "SELECT id, value FROM ".$dbref."_list_area WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$lists['area'][$row['id']] = $row;
	}
mysql_close($con);
$drivers = array();
$sql = "SELECT DISTINCT topdriver FROM ".$dbref."_toplevel ORDER BY topdriver";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$drivers[] = decode($row['topdriver']);
	}
mysql_close($con);

	
		/** DETERMINE WHAT ACTION MUST BE TAKEN **/
		$act = isset($_REQUEST['act']) && strlen($_REQUEST['act']) > 0 ? $_REQUEST['act'] : "VIEW";
		switch($act) {
			case "SAVE":
				$docid = 0;
				$yn = 'P';
				if(isset($_FILES["authdoc"]) && strlen($_FILES["authdoc"]["name"])) {
					if($_FILES["authdoc"]["error"]>0) {
						if($_FILES["authdoc"]["error"]==2)
						{
							echo("<h2>Error</h2><p>Error: ".$_FILES["authdoc"]["error"]." - File size exceeds max file size limit.</p>");
						}
						else
						{
							echo("<h2>Error</h2><p>Error: ".$_FILES["authdoc"]["error"]."</p>");
						}
					} else {
						//echo "<P>UPLOAD FILE: ".$_FILES["authdoc"]["name"]."</p>";
						$userfilename = $_FILES["authdoc"]["name"];
						$sysfilename = "TL".$topid."_authdoc_".date("YmdHis");
						$doc = strFn("explode",$userfilename,".","");
						if(count($doc)>1) {
							$docext = $doc[count($doc)-1];
							$sysfilename.=".".$docext;
						}
						$path = strtoupper($modref)."/";
						checkFolder($path);
						$doclocation = "../files/".$cmpcode."/".$path;
						copy($_FILES["authdoc"]["tmp_name"], $doclocation.$sysfilename);
						if(file_exists($doclocation.$sysfilename)) {
							$sql = "INSERT INTO ".$dbref."_toplevel_log_doc (doctkid, docdate, docfilename, doclocation, doccontent, docyn) VALUES ('$tkid',$today,'".code($userfilename)."','".$doclocation.$sysfilename."','','Y')";
							include("inc_db_con.php");
							$docid = mysql_insert_id();
							if(checkIntRef($docid)) {
								$yn = 'A';
							}
						}
					}
				} else {
					//echo "<p>NO FILE!</p>";
				}
				$newtop = array();
				$var = $_REQUEST;
				$sql = "UPDATE ".$dbref."_toplevel SET ";
				$sf = "";
				foreach($head as $hf => $h) {
					if($hf != "wards" && $hf != "area") {
						if(strlen($sf)>0) { $sf.= ", "; }
						$value = $var[$hf];
						$fld = substr($hf,0,3);
						if($fld == "top") { $newtop[$hf] = code($value); $value = "'".code($value)."'"; } else { $hf = 'top'.$hf.'id'; $newtop[$hf] = $value; }
						$sf.= $hf." = ".$value;
					}
				}
				$sql.= $sf." WHERE topid = ".$topid;
				include("inc_db_con.php");
				foreach(array('w'=>"wards",'a'=>"area") as $k => $f) {
					$delete = array();
					$new = array();
					$current = array();
					$values = $var[$f];
					//find delete & current wards/area
					if($k == "w") { $fld = "l.value, l.numval"; } else { $fld = "l.value"; }
					$sql = "SELECT t.*, $fld FROM ".$dbref."_toplevel_".$f." t INNER JOIN ".$dbref."_list_".$f." l ON t.t".$k.$f."id = l.id WHERE t.t".$k."topid = $topid AND t.t".$k."yn = 'Y'";
					include("inc_db_con.php");
					while($row = mysql_fetch_array($rs)) {
						$i = $row['t'.$k.$f.'id'];
						if(!in_array($i,$values)) {
							$d = array();
							$d[0] = $i;
							if(k=="w") {
								$d[1] = checkIntRef($row['numval']) ? "Ward ".$row['numval'] : $row['value'];
							} else {
								$d[1] = $row['value'];
							}
							$delete[] = $d;
						} else {
							$current[] = $i;
						}
					}
					mysql_close($con);
					//find new wards/area
					foreach($values as $i) {
						if(!in_array($i,$current)) {
							$new[] = $i;
						}
					}
					//DELETE WARDS/AREA WHERE NECESSARY
					if(count($delete)>0) {
						$d = array();
						$dname = array();
						foreach($delete as $del) {
							$d[] = $del[0];
							$dname[] = $del[1];
						}
						$sql = "UPDATE ".$dbref."_toplevel_".$f." SET t".$k."yn = 'N' WHERE t".$k."topid = $topid AND t".$k.$f."id IN (".implode(",",$d).")";
						include("inc_db_con.php");
						logTLEdit($topid,$f,0,'Deleted '.$head[$f]['headdetail'].' '.implode(", ",$dname),'E',$docid,$sql,0,'Deleted '.$head[$f]['headdetail'].' '.implode(", ",$dname).' from TL KPI '.$topid.'.',$yn);
					}
					//ADD NEW WARDS/AREA WHERE NECESSARY
					if(count($new)>0) {
						$d = array();
						$dname = array();
						$sql = "SELECT l.id, $fld FROM ".$dbref."_list_".$f." l WHERE id IN (".implode(",",$new).")";
						include("inc_db_con.php"); 
						$nsql = array();
						while($row = mysql_fetch_array($rs)) {
							$i = $row['id'];
							$d[] = $i;
							if($k=="w") {
								$dname[] = isset($row['numval']) && $row['numval']>0 ? "Ward ".$row['numval'] : $row['value'];
							} else {
								$dname[] = $row['value'];
							}
							$nsql[] = "(null,$topid,$i,'Y')";
						}
						mysql_close($con);
						$sql = "INSERT INTO ".$dbref."_toplevel_".$f." (t".$k."id,t".$k."topid,t".$k."".$f."id,t".$k."yn) VALUES ".implode(",",$nsql);
						include("inc_db_con.php");
						logTLEdit($topid,$f,0,'Added '.$head[$f]['headdetail'].': '.implode(", ",$dname),'E',$docid,$sql,0,'Added '.$head[$f]['headdetail'].': '.implode(", ",$dname).' to TL KPI '.$topid.'.',$yn);
					}
				}
				//echo $sql;
				//arrPrint($top);
				//arrPrint($newtop);
				$topkeys = array_keys($newtop);
				foreach($topkeys as $key) {
					if($newtop[$key]!=$top[$key]) {
						if(isset($head[$key])) { $hf = $key; } else { $hf = substr($key,3,-2); }
						//echo "<P>".$hf." CHANGED FROM ".$top[$key]." TO ".$newtop[$key];
						$old = $top[$key];
						$new = $newtop[$key];
						if($hf=="toptargettypeid") { $old = isset($targettype[$old]) ? $targettype[$old]['value'] : $old; $new = isset($targettype[$new]) ? $targettype[$new]['value'] : $new; }
						elseif($hf=="topcalctype") { $old = isset($calctype[$old]) ? $calctype[$old]['value'] : $old; $new = isset($calctype[$new]) ? $calctype[$new]['value'] : $new; } 
						elseif(isset($lists[$hf])) { $old = $lists[$hf][$old]['value']; $new = $lists[$hf][$new]['value']; }
						logTLEdit($topid,$hf,$old,$new,'E',$docid,$sql,0,$head[$hf]['headdetail'].' edited for TL KPI '.$topid.'.',$yn);
					}
				}
				//RESULTS
				for($m=1;$m<5;$m++) {
					$i = $m*3;
					if($var['tr'.$i]!=$tr[$i]['trtarget']) {
						$sql = "UPDATE ".$dbref."_toplevel_result SET trtarget = ".$var['tr'.$i]." WHERE trtopid = $topid AND trtimeid = $i ";
						include("inc_db_con.php");
						$old = $tr[$i]['trtarget'];
						if($top['toptargettypeid']==1) { $old = "R ".number_format($old); } elseif ($top['toptargettypeid']==2) { $old.=" %"; }
						$new = $var['tr'.$i];
						if($var['toptargettypeid']==1) { $new = "R ".number_format($new); } elseif ($var['toptargettypeid']==2) { $new.=" %"; }
						logTLEdit($topid,'trtarget_'.$i,$old,$new,'E',$docid,$sql,0,'Target edited for TL KPI '.$topid.'.',$yn);
					}
				}
				echo "<script type=text/javascript>document.location.href = 'admin_tl_edit_list.php?s=".$top['topsubid']."&r[]=check&r[]=Changes+saved+to+Top+Level+KPI+".$topid."';</script>";
				break;
			case "DEL":
				//echo "DELETE MEEEEEEEEE";
				$docid = 0;
				$yn = 'P';
				if(isset($_FILES["authdoc"]) && strlen($_FILES["authdoc"]["name"])) {
					if($_FILES["authdoc"]["error"]>0) {
						if($_FILES["authdoc"]["error"]==2)
						{
							echo("<h2>Error</h2><p>Error: ".$_FILES["authdoc"]["error"]." - File size exceeds max file size limit.</p>");
						}
						else
						{
							echo("<h2>Error</h2><p>Error: ".$_FILES["authdoc"]["error"]."</p>");
						}
					} else {
						//echo "<P>UPLOAD FILE: ".$_FILES["authdoc"]["name"]."</p>";
						$userfilename = $_FILES["authdoc"]["name"];
						$sysfilename = "TL".$topid."_authdoc_".date("YmdHis");
						$doc = strFn("explode",$userfilename,".","");
						if(count($doc)>1) {
							$docext = $doc[count($doc)-1];
							$sysfilename.=".".$docext;
						}
						$path = strtoupper($modref)."/";
						checkFolder($path);
						$doclocation = "../files/".$cmpcode."/".$path;
						copy($_FILES["authdoc"]["tmp_name"], $doclocation.$sysfilename);
						if(file_exists($doclocation.$sysfilename)) {
							$sql = "INSERT INTO ".$dbref."_toplevel_log_doc (doctkid, docdate, docfilename, doclocation, doccontent, docyn) VALUES ('$tkid',$today,'".code($userfilename)."','".$doclocation.$sysfilename."','','Y')";
							include("inc_db_con.php");
							$docid = mysql_insert_id();
							if(checkIntRef($docid)) {
								$yn = 'A';
							}
						}
					}
				} else {
					//echo "<p>NO FILE!</p>";
				}
				$sql = "UPDATE ".$dbref."_toplevel SET topyn = 'N' WHERE topid = $topid ";
				include("inc_db_con.php");
				logTLEdit($topid,$hf,0,"KPI Deleted",'D',$docid,$sql,0,'TL KPI '.$topid.' deleted.',$yn);
				echo "<script type=text/javascript>document.location.href = 'admin_tl_edit_list.php?s=".$top['topsubid']."&r[]=check&r[]=Top+Level+KPI+".$topid."+Deleted';</script>";
				break;
			case "VIEW":
			default:
				break;
		}
	}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php 
	include("inc_style.php"); 
	echo $stylecss;
?>
		<link type="text/css" href="lib/jquery-ui-1.7.1.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="lib/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="lib/jquery-ui-1.8.9.custom.min.js"></script>
<script type=text/javascript>
	$(function() {
		var topDrivers = [<?php echo "\"".strFn("implode",$drivers,"\",".chr(10)."\"","")."\""; ?>];
		$( ".drivers" ).autocomplete({
			source: topDrivers
		});
	});
	var required_fields = new Array('topsubid','gfs','natkpa','tas','munkpa','topvalue','topcalctype','toptargettypeid');
	var headings = new Array('Directorate','GFS Classification','National KPA','TAS Focus Area','Municipal KPA','KPI Name','Calculation Type','Target Type');
	
	function Validate(me) {
		var err = '';
		for(key in required_fields) {
			fld = required_fields[key];
			switch(fld) {
				case "topvalue":
				case "topcalctype":
					if(!(me[fld].value.length>0)) {
						me[fld].className = 'required';
						err = err + '\n' + headings[key];
					}
					break;
				default:
					if(isNaN(parseInt(me[fld].value))  || !(me[fld].value.length>0)) {
						me[fld].className = 'required';
						err = err + '\n' + headings[key];
					}
			}
		}
		if(err.length > 0) {
			alert("Please complete the required fields:"+err);
			return false;
		} else {
			return true;
		}
		return false;
	}
	function delTL(t) {
		if(document.getElementById('ifile').value.length==0) {
			alert("Please attach proof that you are authorised to delete this top level KPI.");
		} else {
			if(confirm("Are you sure you wish to delete this Top Level KPI?")==true) {
				document.getElementById('act').value = "DEL";
				document.forms[0].submit();
			}
		}
	}
	function changeTargetType(me) {
		var tt = me.value;
		var a = new Array('atoptargetannual','atoptargetrevised','a3','a6','a9','a12');
		var b = new Array('btoptargetannual','btoptargetrevised','b3','b6','b9','b12');
		var atxt = "";
		var btxt = "";
		switch(tt) {
			case "1": atxt = "R "; btxt = ""; break;
			case "2": atxt = ""; btxt = "%"; break;
			case "3": default: atxt = ""; btxt = ""; break;
		}
		for(i=0;i<6;i++) {
			document.getElementById(a[i]).innerText = atxt;
			document.getElementById(b[i]).innerText = btxt;
		}
	}
</script>
<style type=text/css>
.left { text-align: left; }
.3 { background-color: #CC0001; }
.6 { background-color: #FE9900; }
.9 { background-color: #009900; }
.12 { background-color: #000099; }
.0 { background-color: #999999; }
.3b { background-color: #ffcccc; }
.6b { background-color: #ffeaca; }
.9b { background-color: #ceffde; }
.12b { background-color: #ccccff; }
.0b { background-color: #EEEEEE; }
.logth { background-color: #777777; }
.logtd { color: #777777; }
.required { background-color: #ffbbbb; }
.blank { background-color: #ffffff; }
.ui-widget-content { border: 1px solid #CC0001; }
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus { border: 1px solid #cc0001; background: #dddddd url(images/ui-bg_glass_75_dadada_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #000000; outline: none; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php 
echo "<h1>SDBIP $modtxt: Admin ~ Top Level: Edit</h1>";
echo "<form id=edit action=admin_tl_edit_detail.php method=post onsubmit=\"return Validate(this);\"  enctype=\"multipart/form-data\" >";
echo "<input type=hidden name=act value=SAVE id=act />";
/****************** DETAILS ********************************/
echo "<h2>Details</h2>";
echo "<table cellpadding=5 cellspacing=0 width=650>";
	echo "<tr>";
		echo "<th class=left>Reference:</th>";
		echo "<td>$topid <input type=hidden name=t value=$topid /></td>";
	echo "</tr>";
	echo "<tr>";
		echo "<th class=left>Directorate:</th>";
		echo "<td><select name=topsubid>";
		$sql = "SELECT * FROM ".$dbref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
		include("inc_db_con.php");
			while($row = mysql_fetch_assoc($rs)) {
				echo "<option ";
				if($top['topsubid']==$row['dirid']) { echo " selected "; }
				echo " value=".$row['dirid'].">".$row['dirtxt']."</option>";
			}
		mysql_close($con);
		echo "</select></td>";
	echo "</tr>";
	foreach($head as $h) {
		$hf = $h['headfield'];
		if($hf!="topsubid") {
			echo "<tr>";
				echo "<th class=left style=\"vertical-align: top\">".$h['headdetail'].":</th>";
				echo "<td>";
				switch($hf) {
					case "munkpa":
					case "tas":
					case "natkpa":
					case "gfs":
					case "idp":
						echo "<select name=".$hf.">";
						foreach($lists[$hf] as $l) {
							echo "<option value=".$l['id'];
							if($l['id'] == $top['top'.$hf.'id']) { echo " selected "; }
							echo ">";
	//						if(strlen($l['value'])<70)
								echo $l['value'];
	//						else
	//							echo strFn("substr",$l['value'],1,65)."...";
							if(isset($l['code'])) {
								echo " (".$l['code'].")";
							}
							echo "</option>";
						}
						//$value = $lists[$hf][$top['top'.$hf.'id']];
						echo "</select>";
						break;
					case "topcalctype":
						echo "<select name=".$hf.">";
						foreach($calctype as $l) {
							echo "<option value=".$l['code'];
							if($l['code'] == $top[$hf]) { echo " selected "; }
							echo ">";
								echo $l['value'];
							echo "</option>";
						}
						echo "</select>";
						echo "&nbsp;<a href=glossary.php#kpicalc target=_blank><img src=/pics/help.gif class=noborder></a>";
						break;
					case "topcalcaction":
						echo "<select name=".$hf.">";
						foreach($calcaction as $l) {
							echo "<option value=".$l['code'];
							if($l['code'] == $top[$hf]) { echo " selected "; }
							echo ">";
								echo $l['value'];
							echo "</option>";
						}
						echo "</select>";
						echo "<br /><small><span style=\"font-weight: bold; color: #cc0001;\">Important Note:</span><br />Calculation Action only applies if there is more than 1 departmental KPI associated with the Top Level KPI.<br />It does not influence how the departmental KPI monthly actuals are converted to the Top Level quarterly actuals during the automatic update.</small>";
						break;
					case "toptargettypeid":
						$tt = $top[$hf];
						echo "<select name=".$hf." id=".$hf." onchange=\"changeTargetType(this)\" >";
						foreach($targettype as $l) {
							echo "<option value=".$l['id'];
							if($l['id'] == $top[$hf]) { echo " selected "; }
							echo ">";
								echo $l['value'];
							echo "</option>";
						}
						echo "</select>";
						break;
					case "toptargetannual":
					case "toptargetrevised":
						$size = 10;
						echo "<label id=a".$hf." for=".$hf."></label> <input type=text size=$size name=".$hf." value=\"".decode($top[$hf])."\" style=\"text-align: right\" id=".$hf." /> <label id=b".$hf." for=".$hf."></label> ";
						//echo formatResult($tt,$top[$hf]);
						break;
						break;
					case "wards":
					case "area":
						echo "<table class=noborder>";
							echo "<tr><td class=noborder style=\"border-width: 0px; vertical-align: top;\">";
								$w = 0;
								foreach($lists[$hf] as $wd) {
									echo "<input type=checkbox name=".$hf."[] ";
									if(isset($wa[$hf][$topid][$wd['id']]) && $wa[$hf][$topid][$wd['id']]=="Y") { echo "checked"; }
									echo " value=".$wd['id'].">";
									if($hf=="wards" && isset($wd['numval']) && $wd['numval']!=0) { echo $wd['numval']; } else { echo $wd['value']; }
									echo " ";
									$w++;
									if($w==4) { echo "</td><td class=noborder style=\"border-width: 0px; vertical-align: top;\">"; $w = 0; }
								}
							echo "</td></tr>";
						echo "</table>";
						break;
					default:
						if(in_array($hf,array("topvalue","topunit","toprisk","topdriver"))) { $size = 50; } elseif(in_array($hf,array("topbaseline","toppyp","topdriver"))) { $size = 25; } else { $size = 15; }
						echo "<input type=text size=$size name=".$hf." value=\"".decode($top[$hf])."\" ";
						if($hf == "topdriver") { echo " class=drivers "; }
						echo ">";
						break;
				}
				echo "</td>";
			echo "</tr>";
		}
	}
echo "</table>";

/*********************** RESULTS **************************/
echo "<h2>Results</h2>";
echo "<table cellpadding=5 cellspacing=0 width=650>";
foreach($time as $t) {
	echo "<tr>";
		echo "<th width=150  class=\"".$t['id']." left\">Quarter ending<br />".date("d M Y",$t['eval'])."</th>";
		echo "<th width=100 class=\"".$t['id']." left\">Target:</th>";
		echo "<td width=400 class=\"".$t['id']."b\"><label id=a".$t['id']." for=".$t['id']."></label> <input type=text style=\"text-align: right;\" size=10 value=\"".$tr[$t['id']]['trtarget']."\" name=\"tr".$t['id']."\" id=\"".$t['id']."\" /> <label id=b".$t['id']." for=".$t['id']."></label></td>";
	echo "</tr>";
}
echo "</table>";


echo "<h2>Authorisation</h2>";
echo "<p>If possible please provide proof that you are authorised to make this edit:</p>";
echo "<table cellpadding=5 cellspacing=0 width=650>";
echo "<tr>";
echo "<th class=left width=125>Proof:&nbsp;</th>";
echo "<td><input type=file name=authdoc size=50 id=ifile /></td>";
echo "</tr>";
echo "</table>";

echo "<p>&nbsp;</p>";
echo "<table cellpadding=5 cellspacing=0 width=650><tr><td style=\"text-align: center;\"><input type=submit value=\"Save Changes\" style=\"text-align: center;padding: 5 5 5 5; \"> <input type=button value=\"Delete\" style=\"text-align: center;padding: 5 5 5 5; float: right; margin-top: -30px; \" onclick=\"delTL($topid);\"></td></tr></table>";
echo "</form>";



/********************* ASSOCIATED KPIs *****************************//*
$kpi = array();
$sql = "SELECT kpiid, kpivalue FROM ".$dbref."_kpi WHERE kpimsr = $topid";
include("inc_db_con.php");
if(mysql_num_rows($rs)>0) {
	while($row = mysql_fetch_assoc($rs)) {
		$kpi[$row['kpiid']] = $row;
	}
}
mysql_close($con);
if(count($kpi)>0) {
	echo "<h2>Departmental SDBIP: Associated KPIs</h2>";
	echo "<table cellpadding=5 cellspacing=0 width=650>";
		echo "<tr>";
			echo "<th width=35>Ref</th>";
			echo "<th>KPI Name</th>";
		echo "</tr>";
	foreach($kpi as $k) {
		echo "<tr>";
			echo "<th>".$k['kpiid']."</th>";
			echo "<td><a href=\"view_kpi_detail.php?k=".$k['kpiid']."\">".$k['kpivalue']."</a></td>";
		echo "</tr>";
	}
	echo "</table>";
}


/******************** AUDIT LOG **************************/
echo "<h2 class=logtd>Transaction Log</h2>";
include("inc_tl_log.php");

echo "<p>&nbsp;</p>";
?>
<script type=text/javascript>
changeTargetType(document.getElementById('toptargettypeid'));
</script>
</body>
</html>