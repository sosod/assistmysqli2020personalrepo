<?php
    $r = 1;
foreach($dlist as $dl) {
    $dld = getListKPI($dl);
    $dlist[$dl['headfield']]['data'] = $dld;
}

?>
<link rel="stylesheet" href="lib/locked-column.css" type="text/css">
<base target="main">
		<script type="text/javascript">
function lockCol(tblID) {

	var table = document.getElementById(tblID);
	var button = document.getElementById('toggle');
	var cTR = table.getElementsByTagName('TR');  //collection of rows

	if (table.rows[0].cells[0].className == '') {
		for (i = 0; i < cTR.length; i++)
			{
			var tr = cTR.item(i);
			tr.cells[0].className = 'locked'
			tr.cells[1].className = 'locked'
			}
		//button.innerText = "Unlock First Column";
		}
		else {
		for (i = 0; i < cTR.length; i++)
			{
			var tr = cTR.item(i);
			tr.cells[0].className = ''
			//tr.cells[1].className = ''
			}
		//button.innerText = "Lock First Column";
		}
}
</script>
<script type=text/javascript>
function goTo() {
    var i = document.getElementById('gt').value;
    if(!document.getElementById('i2-'+i))
    {
        alert("No KPI with that reference number exists on this page.");
    }
    else
    {
    var i2 = document.getElementById('i2-'+i).value;
    //alert(i);
    //i=i-2;
    var r0 = document.getElementById('r').value;
    r0 = parseInt(r0);
    var r;
    var t;
    var tbc;
    var t0 = document.getElementById('t').value;
    //alert(t0);
    if(t0!=0)
    {
        for(r=0;r<r0;r++)
        {
            t = t0 + "-" + r;
            if(r>0)
            {
                document.getElementById(t).style.backgroundColor = '#ffffff';
                if(r==1)
                {
                    document.getElementById(t).style.borderBottomColor = '#ffffff';
                }
            }
            else
            {
                document.getElementById(t).style.backgroundColor = '#cc0001';
                document.getElementById(t).style.borderBottomColor = '#cc0001';
            }
        }
    }
    for(r=0;r<r0;r++)
    {
        t = i + "-" + r;
        if(r>0)
        {
            document.getElementById(t).style.backgroundColor = '#FFB039';
            if(r==1)
            {
                document.getElementById(t).style.borderBottomColor = '#FFB039';
            }
        }
        else
        {
            document.getElementById(t).style.backgroundColor = '#FFB039';
            document.getElementById(t).style.borderBottomColor = '#FFB039';
        }
    }
    document.getElementById('t').value = i;
    document.location.href = "#"+i2;
    }
}
function showGraphs() {
        var target = document.getElementById('graphs');
        target.style.display = "block";
        var target2 = document.getElementById('sgraphs');
        target2.style.display = "none";
}
function hideGraphs() {
        var target = document.getElementById('graphs');
        target.style.display = "none";
        var target2 = document.getElementById('sgraphs');
        target2.style.display = "block";
}
function showMonths() {
    var button = document.getElementById('dispbut');
    var cellsend = document.getElementById('cellsend').value;
    alert("Under development");
    if(button.value == "Year-to-date")
    {
        button.value = " All months ";
/*        var cel;
        for(c=1;c<cellsend;c++)
        {
            cel = "c"+c;
            document.getElementById(cel).style.display = "none";
        }
*/    }
    else
    {
        button.value = "Year-to-date";
/*        var cel;
        for(c=1;c<cellsend;c++)
        {
            cel = "c"+c;
            document.getElementById(cel).style.display = "block";
        }
*/    }
}
</script>
<?php
$cells = 1;
$wfrom = $whenfrom;
$wto = $whento;

$style['th']['i']['a'] = "background-color: #cc0001; color:#ffffff; text-align: center; border: 1px solid #cc0001; border-top: 0px; border-left: 0px;";
$style['th']['i']['b'] = "background-color: #cc0001; color:#ffffff; text-align: center; border: 1px solid #cc0001; border-left: 0px;";
$style['th']['n']['a'] = "background-color: #cc0001; color:#ffffff; text-align: center; border: 1px solid #cc0001; border-left: 1px solid #fff; border-right: 1px solid #ffffff; border-top: 0px; border-bottom: 0px;";
$style['th']['n']['b'] = "background-color: #cc0001; color:#ffffff; text-align: center; border: 1px solid #cc0001; border-left: 1px solid #fff; border-right: 1px solid #ffffff; border-top: 0px; border-bottom: 0px;";
$style['th']['h']['a'] = "background-color: #cc0001; color:#ffffff; text-align: center; border: 1px solid #cc0001; border-top: 0px; border-bottom: 0px;";
$style['th']['h']['b'] = "background-color: #cc0001; color:#ffffff; text-align: center; border: 1px solid #cc0001; border-left: 1px solid #ffffff; border-top: 0px; border-bottom: 0px;";

$style['sec'][1] = "background-color: #777; color:#ffffff; text-align: left; border: 1px solid #777; border-left: 0px;";
$style['sec'][2] = "background-color: #777; color:#ffffff; text-align: left; border: 1px solid #777; border-right: 1px solid #ababab;";

$style['sub'][1] = "background-color: #aaa; color:#ffffff; text-align: left; border: 1px solid #aaa; border-bottom: 1px solid #aaa; border-left: 0px;";
$style['sub'][2] = "background-color: #aaa; color:#ffffff; text-align: left; border: 1px solid #aaa; border-bottom: 1px solid #aaa; border-right: 1px solid #ababab;";

$style['kpi'][1][0] = "background-color: #cc0001; color: #ffffff; text-align: center; border-right: 0px solid #fff; border-left: 0px; border-bottom: 1px solid #ababab;";
$style['kpi'][2][0] = "background-color: #ffffff; color: #000000; text-align: left; border-left: 0px; border-bottom: 1px solid #ababab;";
$style['kpi'][3][0] = "background-color: #ffffff; color: #000000; text-align: left; border-left: 0px solid #fff; border-right: 1px solid #ababab;";

$style['kpi'][1][1] = "background-color: #cc0001; color: #ffffff; text-align: center; border-right: 0px solid #fff; border-left: 0px; border-bottom: 1px solid #cc0001;";
$style['kpi'][2][1] = "background-color: #ffffff; color: #000000; text-align: left; border-left: 0px; border-bottom: 1px solid #ffffff;";
$style['kpi'][3][1] = "background-color: #ffffff; color: #000000; text-align: left; border-left: 0px solid #fff; border-right: 1px solid #ababab;";

$style['none'][1] = "background-color: #cc0001; color: #ffffff; text-align: center; border-right: 0px solid #fff; border-left: 0px; border-bottom: 1px solid #cc0001;";
//$style['none'][1] = "background-color: #cc0001; color: #000000; text-align: left; font-weight: normal; border: 1px solid #fff; border-left: 0px;";
$style['none'][2] = "background-color: #ffffff; color: #000000; text-align: left; font-weight: normal; border: 1px solid #fff; ";

$style['th'][0] = "background-color: #555555; color: #fff; text-align: center; border-left: 1px solid #ffffff;";
$style['th'][1] = "background-color: #cc0001; color: #fff; text-align: center; border-left: 1px solid #ffffff;";
$style['th'][2] = "background-color: #fe9900; color: #fff; text-align: center; border-left: 1px solid #ffffff;";
$style['th'][3] = "background-color: #006600; color: #fff; text-align: center; border-left: 1px solid #ffffff;";
$style['th'][4] = "background-color: #000066; color: #fff; text-align: center; border-left: 1px solid #ffffff;";

$style['thl'][0] = "background-color: #dddddd; color: #000; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff;";
$style['thl'][1] = "background-color: #ffcccc; color: #000; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff;";
$style['thl'][2] = "background-color: #ffeaca; color: #000; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff;";
$style['thl'][3] = "background-color: #ceffde; color: #000; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff;";
$style['thl'][4] = "background-color: #ccccff; color: #000; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff;";


$style[0] = "background-color: #555555; color: #fff; text-align: center;";
$style[1] = "background-color: #cc0001; color: #fff; text-align: center;";
$style[2] = "background-color: #fe9900; color: #fff; text-align: center;";
$style[3] = "background-color: #006600; color: #fff; text-align: center;";
$style[4] = "background-color: #000066; color: #fff; text-align: center;";

$stylel[0] = "background-color: #dddddd; color: #000; text-align: center;";
$stylel[1] = "background-color: #ffcccc; color: #000; text-align: center;";
$stylel[2] = "background-color: #ffeaca; color: #000; text-align: center;";
$stylel[3] = "background-color: #ceffde; color: #000; text-align: center;";
$stylel[4] = "background-color: #ccccff; color: #000; text-align: center;";

if($what != "KPI_OP") { $section[0] = array("Capital Projects","> 0"); }
if($what != "KPI_CP") { $section[1] = array("Operational Performance","= 0"); }

$warr = explode("_",$who);
$dtype = $warr[0];
$did = $warr[1];
$subs = array();
if($warr[0]=="d")
{
    $sql = "SELECT dirid id, dirtxt txt, dirsort sort, dirid sdid FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$warr[1];
    include("inc_db_con.php");
        $deptrow = mysql_fetch_array($rs);
    mysql_close();
    $sb=0;
    $sql = "SELECT subid id, subtxt txt, subsort sort, subdirid sdid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subdirid = ".$warr[1];
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $subs[$sb] = $row;
            $sb++;
        }
    mysql_close();
}
else
{
    $sql = "SELECT subid id, subtxt txt, subsort sort, subdirid sdid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subid = ".$warr[1];
    include("inc_db_con.php");
        $deptrow = mysql_fetch_array($rs);
    mysql_close();
    $subs[0] = $deptrow;
    $deptrow = array();
    $sql = "SELECT dirid id, dirtxt txt, dirsort sort, dirid sdid FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$subs[0]['sdid'];
    include("inc_db_con.php");
        $deptrow = mysql_fetch_array($rs);
    mysql_close();
}
$totalcols = 5;

                    $row2id = 0;

?>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: View KPIs - <?php echo($deptrow['txt']); ?></b></h1>
<?php include("inc_progressbar.php"); ?>
<p id=sgraphs><input type=button onclick=showGraphs() value="Show graph"></p>
<span id=graphs>
<center>
    <table cellpadding=3 cellspacing=0 style="border: 1px solid #ffffff;">
        <tr>
            <td valign=top style="border: 1px solid #ffffff;">
<?php
//WHEN
$wt = 1;
$wf = 1;
            $sqlt = "";
            switch($when)
            {
                case "ALL":
                    $totalhead = "";
                    $wt = 12;
                    break;
                case "Q1":
                    $sqlt.= " AND sort < 4 ";
                    $totalhead = " for Q1";
                    $wt = 3;
                    break;
                case "Q2":
                    $sqlt.= " AND sort > 3 AND sort < 7 ";
                    $totalhead = " for Q2";
                    $wf = 4;
                    $wt = 6;
                    break;
                case "Q3":
                    $sqlt.= " AND sort > 6 AND sort < 10 ";
                    $totalhead = " for Q3";
                    $wf = 7;
                    $wt = 9;
                    break;
                case "Q4":
                    $sqlt.= " AND sort > 9 ";
                    $totalhead = " for Q4";
                    $wf = 10;
                    $wt = 12;
                    break;
                case "YTD":
                    $sqlt.= " AND sval < ".$today." ";
                    $totalhead = " for Year-to-date";
                    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sval < ".$today." AND eval > ".$today." ORDER BY sort DESC";
                    include("inc_db_con.php");
                        if(mysql_num_rows($rs)>0)
                        {
                            $row = mysql_fetch_array($rs);
                            $wt = $row['sort'];
                        }
                        else
                        {
                            $wt=12;
                        }
                    mysql_close();
                    break;
                case "SEL":
                    $sqlt.= " AND sval >= ".$wfrom." AND eval <= ".$wto." ";
                    $totalhead = "";
                    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND sval = ".$wfrom." ORDER BY sort";
                    include("inc_db_con.php");
                        $row = mysql_fetch_array($rs);
//                        echo("<P>".$sql."-");
//                        print_r($row);
                        $wf = $row['sort'];
                    mysql_close();
                    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' AND eval = ".$wto." ORDER BY sort";
                    include("inc_db_con.php");
                        $row = mysql_fetch_array($rs);
//                        echo("<P>".$sql."-");
//                        print_r($row);
                        $wt = $row['sort'];
                    mysql_close();
                    break;
                default:
                    $totalhead = "";
                    $wt = 12;
                    break;
            }
//echo("<P>".$wfrom."-".$wto);
//Title text
$titletxt = "";
$anima = "N";
include("inc_report_dashboard_kpi_pie.php");
$p = 10;
?>
            </td>
        </tr>
        <tr>
            <td align=center style="border: 1px solid #ffffff;"><input type=button onclick=hideGraphs() value="Hide graph"></td>
        </tr>
    </table></center>
</span>
<?php if($graph == "Y") { ?>
<script type=text/javascript>document.getElementById('sgraphs').style.display = "none";</script>
<?php } else { ?>
<script type=text/javascript>document.getElementById('graphs').style.display = "none";</script>
<?php } ?>
<script type=text/javascript>
incProg(10);
</script>
<table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
    <tr class=tdgeneral>
        <td align=left style="border: 0px solid #ffffff;"><small><b>Go to:</b> <input type=text name=kpiid size=5 id=gt> <input type=button value=" Go " onclick=goTo()></small></td>
        <td align=right style="border: 0px solid #ffffff;">&nbsp;<b>Display:</b> <select id=dis><option <?php if($display!="all") { echo("selected"); } ?> value=sum>Limited</option><option <?php if($display=="all") { echo("selected"); } ?> value=all>All</option></select> <input type=button value=Go onclick="changeDisplay();"></td>
    </tr>
</table>
<div id="tbl-container">
<table id="tbl" cellpadding=0 cellspacing=0>
        <tr>
            <th style="<?php echo($style['th']['i']['a']); ?>" valign=bottom>KPI</th>
            <th style="<?php echo($style['th']['n']['a']); ?>" valign=bottom>KPI</th>
            <?php
if($display=="all")
{
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headyn = 'Y' AND headdetailyn = 'Y' AND headtype = 'KPI' ORDER BY headdetailsort";
}
else
{
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headyn = 'Y' AND headlistyn = 'Y' AND headtype = 'KPI' ORDER BY headlistsort";
}
            include("inc_db_con.php");
                $h = 0;
                while($row = mysql_fetch_array($rs))
                {
                    if($row['headliststd'] != "KPI Name")
                    {
                        if($h>0) { $st = "b"; } else { $st = "a"; }
            ?>
            <th style="<?php echo($style['th']['h'][$st]); ?>" rowspan=2><?php echo($row['headlist']); ?></th>
            <?php
                    $heading[$h] = $row;
                    $h++;
                    $totalcols++;
                    }
                }
            mysql_close();

            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ";
            $sql.= $sqlt;
            $sql.= " ORDER BY sort";
            include("inc_db_con.php");
                $th = 0;
                $s = 1;
                $timehead = array();
                while($row = mysql_fetch_array($rs))
                {
                    if($row['sval']>$today) { $colspan = 1; } else { $colspan=4; }
                    //$colspan=4;
                    $totalcols = $totalcols + $colspan;
                    ?>
            <th colspan=<?php echo($colspan); ?> style="<?php echo($style['th'][$s]); ?>"><?php echo(date("M",$row['eval'])."&nbsp;".date("Y",$row['eval'])); ?></th>
                    <?php
                    $timehead[$th] = $row;
                    $th++;
                    $s++;
                    if($s>4) { $s = 1; }
                }
            mysql_close();
            ?>
            <th colspan=3 style="<?php echo($style['th'][0]); ?>">Performance-To-Date</th>  
        </tr>
        <tr>
            <th valign=top style="<?php echo($style['th']['i']['b']); ?>">Ref</th>
            <th valign=top style="<?php echo($style['th']['n']['b']); ?>">Name</th>
            <?php
            $s = 1;
            foreach($timehead as $thead)
            {
                ?>
                <th style="<?php echo($style['thl'][$s]); ?>">Target</th>
                <?php if($thead['sval']<$today) { ?>
                <th style="<?php echo($style['thl'][$s]); ?>">Actual</th>
                <th style="<?php echo($style['thl'][$s]); ?>">R</th>
                <th style="<?php echo($style['thl'][$s]); ?>">Progress</th>
                <?php
                    }
                $s++;
                if($s>4) { $s = 1; }
            }
            ?>
                <th style="<?php echo($style['thl'][0]); ?>">Target</th>
                <th style="<?php echo($style['thl'][0]); ?>">Actual</th>
                <th style="<?php echo($style['thl'][0]); ?>">R</th>
        </tr>
    <?php foreach($section as $sec) { ?>
        <tr>
            <td style="<?php echo($style['sec'][1]); ?>">&nbsp;</td>
            <td colspan=55 style="<?php echo($style['sec'][2]); ?>"><?php echo($sec[0]); ?></td>
        </tr>
        <?php
        $subcount = count($subs);
        $p2 = 45/$subcount;
        $subcount2 = 0;
        foreach($subs as $subrow)
        {
            $sql = "SELECT count(kpiid) as cc FROM assist_".$cmpcode."_".$modref."_kpi WHERE kpiyn = 'Y' AND kpisubid = ".$subrow['id']." AND kpicpid ".$sec[1];
            include("inc_db_con.php");
                $ccrow = mysql_fetch_array($rs);
            mysql_close();
            
            if($ccrow['cc']>0)
            {
            $subcount2++;
?>
        <tr>
            <td style="<?php echo($style['sub'][1]); ?>">&nbsp;</td>
            <td colspan=55 style="<?php echo($style['sub'][2]); ?>"><?php echo($subrow['txt']); ?></td>
        </tr>
<?php
            $sql = "SELECT kpiid, kpivalue";
            $sql.= ", kpiprogid prog";
            $sql.= ", kpinatkpaid natkpa";
            $sql.= ", kpimunkpaid munkpa";
            $sql.= ", kpitypeid ktype";
            $sql.= ", kpigfsid gfs";
            $sql.= ", kpitasid tas";
            $sql.= ", kpi.*";
            $sql.= " FROM assist_".$cmpcode."_".$modref."_kpi kpi";
            $sql.= " WHERE kpicpid ".$sec[1]." AND kpiyn = 'Y'";
            $sql.= " AND kpisubid = ".$subrow['id'];
            $sql.= " AND kpiid IN (SELECT DISTINCT krkpiid FROM assist_".$cmpcode."_".$modref."_kpi_result)";
/*			switch($what) {
				case "KPI_CP":
					$sql.= " AND kpicpid > 0 ";
					break;
				case "KPI_OP":
					$sql.= " AND kpicpid = 0 ";
					break;
				default:
					break;
			}*/
            //$sql.= " AND kpisubid = ".$who[1];
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
                    $mnr = mysql_num_rows($rs);
                    $p3 = $p2 / $mnr;
                    $mnr2 = 0;
                    while($row = mysql_fetch_array($rs))
                    {
                        $kttid = $row['kpitargettypeid'];
                        $p = $p + $p3;
                        echo("<script type=text/javascript>incProg(".$p.");</script>");
                        $kct = $row['kpicalctype'];
                        $mnr2++;
                        if($mnr2==$mnr && $sec[0] == "Operational Performance" && $subcount == $subcount2) { $st = 0; } else { $st = 1; }
                        $r=0;
        ?>
        <tr>
            <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][1][$st]); ?>"><input type=hidden name=i2 id=<?php echo("i2-".$row['kpiid']); ?> value=<?php echo($row2id); ?>><a name=<?php echo($row['kpiid']); ?>></a><?php echo($row['kpiid']); ?><img src=/pics/blank.gif width=1 height=44 align=absmiddle></td>
            <?php $r++; ?>
            <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][2][$st]); ?>"><a href=view_kpi_detail.php?k=<?php echo($row['kpiid']); ?>><?php echo($row['kpivalue']); ?></a> <img src="/pics/blank.gif" height=1 width=150></td>
            <?php
            $row2id = $row['kpiid'];
            foreach($heading as $head)
            {
                $r++;
                $row2 = array();
                $dl = $dlist[$head['headfield']];
                if($dl['yn'] == "Y") {
                        $row2 = $dl['data'][$row[$head['headfield']]];
                        if($dl['code']=="Y") {
                            $val = "<span title=\"".$row2['value']."\" style=\"text-decoration: underline\">".$row2['code']."</span>";
                        } else {
                            $val = $row2['value'];
                        }
                        ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php echo($val); ?></a></td><?php
                } else {
                switch($head['headfield'])
                {
                    case "kpidefinition":
                        ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php echo($row[$head['headfield']]); ?> <img src="/pics/blank.gif" height=1 width=150></td><?php
                        break;
                    case "area":
                        $sql2 = "SELECT w.value FROM assist_".$cmpcode."_".$modref."_kpi_area k, assist_".$cmpcode."_".$modref."_list_area w WHERE w.id = k.kaareaid AND k.kakpiid = ".$row['kpiid']." AND k.kayn = 'Y' AND w.yn = 'Y'";
                        include("inc_db_con2.php");
                            if(mysql_num_rows($rs2)>1)
                            {
                                ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php
                                    while($row2=mysql_fetch_array($rs2))
                                    {
                                        echo($row2['value']."; ");
                                    }
                                ?></td><?php
                            }
                            else
                            {
                                $row2 = mysql_fetch_array($rs2);
                                ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php echo($row2['value']); ?></td><?php
                            }
                        mysql_close($con2);
                        break;
                    case "wards":
                        $sql2 = "SELECT w.id, w.numval value FROM assist_".$cmpcode."_".$modref."_kpi_wards k, assist_".$cmpcode."_".$modref."_list_wards w WHERE w.id = k.kwwardsid AND k.kwkpiid = ".$row['kpiid']." AND k.kwyn = 'Y' AND w.yn = 'Y'";
                        include("inc_db_con2.php");
                            if(mysql_num_rows($rs2)>1)
                            {
                                ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php
                                    while($row2=mysql_fetch_array($rs2))
                                    {
										if($row2['id']==1) { $row2['value']="All"; }
                                        echo($row2['value']."; ");
                                    }
                                ?></td><?php
                            }
                            else
                            {
                                $row2 = mysql_fetch_array($rs2);
										if($row2['id']==1) { $row2['value']="All"; }
                                ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php echo($row2['value']); ?></td><?php
                            }
                        mysql_close($con2);
                        break;
                    default:
                        ?><td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($style['kpi'][3][$st]); ?>"><?php echo($row[$head['headfield']]); ?></td><?php
                        break;
                }
                }//if dlist = y
            }
            $s = 1;
            $krt0 = 0;
            $kra0 = 0;
            $rt = 0;
            $ra = 0;
            $rr = 0;
			$kcount = 0;
            foreach($timehead as $thead)
            {
                switch($kct)
                {
                    case "CO":
                        $sql2 = "SELECT max(krtarget) as krtarget, max(kractual) as kractual FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$row['kpiid']." AND krtimeid <= ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$row['kpiid']." AND krtimeid = ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2x = mysql_fetch_array($rs2);
                            $row2['krprogress'] = $row2x['krprogress'];
                            $row2['krtargettypeid'] = $row2x['krtargettypeid'];
                        mysql_close($con2);
                        break;
                    default:
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_result WHERE krkpiid = ".$row['kpiid']." AND krtimeid = ".$thead['id'];
                        include("inc_db_con2.php");
                            $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        break;
                }
                $krt = $row2['krtarget'];
                $kra = $row2['kractual'];

if($thead['eval']<$today || $kra>0)
{
                if($kct == "CO")
                {
                    $krt0 = $krt;
                    $kra0 = $kra;
                }
                else
                {
                    $krt0 += $krt;
                    $kra0 += $kra;
                }
                $krr = calcKR($kct,$krt,$kra);
} else {
    $krr = "0";
}
					if($krt!=0) { $kcount++; }
                
                switch($kttid)
                {
                    case 1:
                        $krtarget = "R ".number_format($krt,2);
                        $kractual = "R ".number_format($kra,2);
                        break;
                    case 2:
                        $krtarget = number_format($krt,2)."%";
                        $kractual = number_format($kra,2)."%";
                        break;
                    default:
                        $krtarget = number_format($krt,2);
                        $kractual = number_format($kra,2);
                        break;
                }

                ?>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($stylel[$s]); ?>"><?php if($krt>0 || $kra>0 || $kct=="ZERO") { echo($krtarget); } else { echo("&nbsp;"); } ?></td>
      
                <?php if($thead['sval']<$today) { ?>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($stylel[$s]); ?>"><?php if($krt>0 || $kra>0 || $kct=="ZERO") { echo($kractual); } else { echo("&nbsp;"); } ?></td>
        
                <?php if($krt>0 || $kra>0 || $kct == "ZERO") { ?>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($styler[$krr]); ?>">&nbsp;</td>
        
                <?php } else { ?>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($stylel[$s]); ?>">&nbsp;</td>
       
                <?php } ?>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($stylel[$s]); ?>"><?php echo($row2['krprogress']); ?> <img src="/pics/blank.gif" height=1 width=150></td>
                <?php
                }
                $s++;
                if($s>4) { $s = 1; }
            }
				
				$krt0 = $results[$row['kpiid']]['t'];
				$kra0 = $results[$row['kpiid']]['a'];
				if(($kct=="STD" || $kct=="REV") && $kcount > 0) { $krt0 /= $kcount; $kra0/=$kcount; }
                $rr = calcKR($kct,$krt0,$kra0);

                switch($kttid)
                {
                    case 1:
                        $target = "R ".number_format($krt0,2);
                        $actual = "R ".number_format($kra0,2);
                        break;
                    case 2:
                        $target = number_format($krt0,2)."%";
                        $actual = number_format($kra0,2)."%";
                        break;
                    default:
                        $target = number_format($krt0,2);
                        $actual = number_format($kra0,2);
                        break;
                }
                $r++;
            ?>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($stylel[0]); ?>"><?php echo($target); ?></td>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($stylel[0]); ?>"><?php echo($actual); ?></td>
                <td id=<?php echo($row['kpiid']."-".$r); ?> style="<?php echo($styler[$rr]); ?>">&nbsp;</td>
        </tr>
        <?php
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td style="<?php echo($style['none'][1]); ?>">&nbsp;</td>
                        <td colspan=<?php echo($totalcols-1); ?> style="<?php echo($style['none'][2]); ?>">&nbsp;&nbsp;No KPIs found. <img src="/pics/blank.gif" height=1 width=150></td>
                    </tr>
                    <?php
                }
            mysql_close();
            }//ccrow>0
            else
            {
                $p = $p+$p2;
                echo("<script type=text/javascript>incProg(".$p.");</script>");
            }
        }//foreach sub
        }//foreach section
        ?>
</table>
</div>
<?php
//print_r($heading);
?>
<input type=hidden id=cellsend value=<?php echo($cells); ?>>
<input type=hidden id=r value=<?php echo($r); ?>>
<input type=hidden id=t value=0>
<script type="text/javascript"> lockCol('tbl'); </script>
                <script type=text/javascript>
                incProg(100);
                //var t = setTimeout("return hide();",2000);
                hide();
                </script>

