<?php
/*************** FUNCTIONS ******************/
function logAction($a) {
	$action = "N/A";
	switch($a) {
		case "A":
			$action = "Auto Update";
			break;
		case "C":
			$action = "Create KPI";
			break;
		case "D":
			$action = "Delete KPI";
			break;
		case "E":
			$action = "Edit KPI";
			break;
		case "K":
		case "R":
			$action = "Dept. KPI";
			break;
		case "U":
			$action = "Update Results";
			break;
		default:
			break;
	}
	return $action;
}	
function logStatus($s,$docid) {
	$action = "N/A";
	switch($s) {
		case "A":
			$action = "Approved";
			if($docid > 0) {
				$action.="<br /><a href=dl_file.php?d=".$docid."&t=toplevel_log_doc>View</a>";
			}
			break;
		case "P":
			$action = "Pending Approval";
			break;
		case "R":
			$action = "Rejected";
			break;
		case "Y":
		case "N":
		default:
			$action = "N/A";
			break;
	}
	return $action;
}	
function formatResult($tt,$val) {
	$echo = "";
	switch($tt) {
		case "1":
			$echo = "R ".$val;
			break;
		case "2":
			$echo = $val."%";
			break;
		case "3":
		default:
			$echo = $val;
			break;
	}
	return $echo;
}
/*********** END FUNCTIONS *******************/








/************* DISPLAY LOG OF SELECTED KPI **************/ 
echo "<table cellpadding=3 cellspacing=0>";
/***** HEADING *****/
	echo "<tr>";
		echo "<th class=logth>Ref</th>";
		echo "<th class=logth>Date</th>";
		echo "<th class=logth>User</th>";
		echo "<th class=logth>Activity Type</th>";
		echo "<th class=logth>Transaction</th>";
		echo "<th class=logth>Status</th>";
	echo "</tr>";
/***** DETAILS *****/
	/*** GET USERS ***/
	$tk = array();
$sql = "SELECT tkid, tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid IN (SELECT DISTINCT logtkid FROM ".$dbref."_toplevel_log WHERE logtopid = $topid)";
include("inc_db_con.php");
	while($row = mysql_fetch_assoc($rs)) {
		$tk[$row['tkid']] = $row;
	}
mysql_close($con);
	/*** GET LOGS ***/
$sql = "SELECT * FROM ".$dbref."_toplevel_log WHERE logtopid = $topid AND logyn <> 'N' AND logtype != 'I'";
include("inc_db_con.php");
	while($log = mysql_fetch_assoc($rs)) {
		$usrnm = isset($tk[$log['logtkid']]) ? $tk[$log['logtkid']]['tkname']." ".$tk[$log['logtkid']]['tksurname'] : "Ignite Support";
		$old = $log['logold'];
		$new = $log['lognew'];
		$hf = $log['logfield'];
		$trans = "";
		switch($log['logtype']) {
			case "A":
			case "U":
				$fld = explode("_",$log['logfield']);
				if($fld[0]=="tractual") {
					$old = formatResult($top['toptargettypeid'],$old);
					$new = formatResult($top['toptargettypeid'],$new);
				}
				$trans = "Updated ";
				$trans.= isset($head[$fld[0]]) ? $head[$fld[0]]['headlist'] : $fld[0];
				$value = isset($head[$fld[0]]) ? $head[$fld[0]]['headlist'] : $fld[0];
				$trans.= " for the Quarter ending ".date("M Y",$time[$fld[1]]['eval']);
				$trans.= ". <br />";
				if(strlen($old)>0) {
					$trans.= "Original $value: <i>'$old'</i><br />";
				}
				$trans.= "New $value: <i>'$new'</i>";
				break;
			case "K":
				$trans = "Created link to Departmental KPI $new.";
				break;
			case "R":
				$trans = "Removed link to Departmental KPI $old.";
				break;
			case "E":
				if($hf=="wards" || $hf=="area") {
					$trans = $new;
				} else {
					$fld = explode("_",$log['logfield']);
					if($fld[0]=="trtarget") {
						$trans = "Updated ";
						$trans.= "target";
						$value = "target";
						$trans.= " for the Quarter ending ".date("M Y",$time[$fld[1]]['eval']);
						$trans.= ". <br />";
						if(strlen($old)>0) {
							$trans.= "Original $value: <i>'$old'</i><br />";
						}
						$trans.= "New $value: <i>'$new'</i>";
					} else {
						$trans = "Changed ".$head[$log['logfield']]['headlist']." from '$old' to '$new'.";
					}
				}
				break;
			case "D":
				$trans = $new;
				break;
			case "C":
			default:
				$trans = $new;
				break;
		}
		echo "<tr>";
			echo "<th class=logth>".$log['logid']."</th>";
			echo "<td class=logtd style=\"text-align:center\">".date("d-M-Y H:i",$log['logdate'])."</td>";
			echo "<td class=logtd>".$usrnm."</td>";
			echo "<td class=logtd>".logAction($log['logtype'])."</td>";
			echo "<td class=logtd>".$trans."</td>";
			echo "<td class=logtd style=\"text-align: center\">".logStatus($log['logyn'],$log['logdocid'])."</td>";
		echo "</tr>";
	}
mysql_close($con);
/***** END *****/
echo "</table>";

?>