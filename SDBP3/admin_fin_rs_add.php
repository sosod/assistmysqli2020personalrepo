<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script type=text/javascript>
function Validate(me) {
    var val = me.val.value;
    var t = me.t.value;
    t = parseInt(t);
    var valid8 = "true";
    if(val.length==0)
    {
        document.getElementById('val').className = 'reqtext';
        document.getElementById('val').focus();
        valid8 = "false";
    }
    else
    {
            document.getElementById('val').className = 'reqmet';
    }
    var target;
    var targ;
    var t2 = 0;
    var berr = "N";
    for(t2=0;t2<t;t2++)
    {
        targ = "b"+t2;
        target = document.getElementById(targ);
        if(target.value.length > 0)
        {
            if(isNaN(parseInt(target.value)))
            {
                target.className = "reqtext";
                valid8 = "false";
                berr = "Y";
            }
            else
            {
                if(parseFloat(target.value)!=escape(target.value))
                {
                    target.className = "reqtext";
                    valid8 = "false";
                    berr = "Y";
                }
                else
                {
                    target.className = "blank";
                }
            }
        }
        else
        {
            target.value = 0;
            target.className = "blank";
        }
    }
    if(valid8 == "false")
    {
        var errtxt = "Please complete all missing field(s) as indicated."
        if(berr == "Y")
        {
            errtxt = errtxt+"\nNote: Only numbers are allowed for budget figures.";
        }
        alert(errtxt);
        return false;
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table {
    border: 1px solid #ababab;
}
table td {
    border-color: #ffffff;
    border-width: 1px;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px;
    border-right: 0px;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Finance - Revenue By Source ~ Add Line Item</b></h1>
<p>&nbsp;</p>
<form name=add method=post action=admin_fin_rs_add_process.php onsubmit="return Validate(this);">
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl width=120>Line Item:</td>
		<td class=tdgeneral valign=top width=450><input type=text size=50 maxlength=100 name=val id=val> <span style="font-size: 7.5pt;">(max 100 characters)</span></td>
	</tr>
<?php
$style['th'][1] = "background-color: #cc0001; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][2] = "background-color: #fe9900; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][3] = "background-color: #006600; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";
$style['th'][4] = "background-color: #000066; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;";

$style['thl'][1]['R'] = "background-color: #ffabab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['R'] = "background-color: #ffddab; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['R'] = "background-color: #a3ffc2; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['R'] = "background-color: #babaff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['O'] = "background-color: #ffcccc; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['O'] = "background-color: #ffe6bd; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['O'] = "background-color: #ceffde; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['O'] = "background-color: #ccccff; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][1]['C'] = "background-color: #ffdbdf; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][2]['C'] = "background-color: #fff1de; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][3]['C'] = "background-color: #deffea; color: #000; border-bottom: 1px solid #fff;";
$style['thl'][4]['C'] = "background-color: #ddddff; color: #000; border-bottom: 1px solid #fff;";

$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)>0)
    {
    ?>
    <tr>
		<td class=tdgeneral width=120 colspan=1 height=27 style="background-color: #888; color: #fff; font-weight: bold; border-bottom: 1px solid #fff; "><b>Budget</b></td>
		<td class=tdgeneral valign=top width=450  style="background-color: #888; color: #fff; font-weight: bold; border-bottom: 1px solid #fff;">&nbsp;</td>
    </tr>
    <?php
        $s=1;
        $t=0;
        while($row = mysql_fetch_array($rs))
        {
            ?>
            <tr>
                <td class=tdgeneral width=120 valign=top style="<?php echo($style['th'][$s]); ?> font-weight: bold;"><?php echo(date("M Y",$row['eval'])); ?>:<input type=hidden name=time[] value=<?php echo($row['id']); ?>></td>
                <td class=tdgeneral width=450 valign=top style="<?php echo($style['thl'][$s]['R']); ?>">R <input type=text size=10 name=budget[] id=b<?php echo($t); ?> style="text-align: right"> <span style="font-size: 7.5pt;">(numbers only)</span></td>
        	</tr>
            <?php
            $t++;
            $s++;
            if($s>4) { $s=1; }
        }
    }
mysql_close();
?>
    <tr>
		<td class=tdheaderl width=120>&nbsp;<input type=hidden name=t value=<?php echo($t); ?> size=5></td>
		<td class=tdgeneral width=450 valign=top><input type=submit value=" Add "></td>
    </tr>
</table>
</form>
<?php
$urlback = "admin_fin_rs_setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
