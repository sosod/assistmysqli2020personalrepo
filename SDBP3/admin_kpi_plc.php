<?php
    include("inc_ignite.php");
    $r = 1;
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<title>www.Ignite4u.co.za</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<link rel="stylesheet" href="lib/locked-column.css" type="text/css">
<base target="main">
<script type=text/javascript>
function viewPLC(k,p,d,s) {
    if(parseInt(k) && parseInt(p) && parseInt(d) && parseInt(s))
    {
        if(parseInt(k)>0 && parseInt(p)>0 && parseInt(d)>0 && parseInt(s)>0)
        {
            document.getElementById('k').value = k;
            document.getElementById('p').value = p;
            document.getElementById('d').value = d;
            document.getElementById('s').value = s;
            document.forms['view'].submit();
        }
    }
}
</script>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
$src = $_GET['r'];
$p = $_GET['p'];    //kpiid for highlight function
?>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Project Life Cycles</b></h1>
<form name=view action=admin_kpi_plc_view.php method=post>
<input type=hidden id=k name=kpiid>
<input type=hidden id=p name=plcid>
<input type=hidden id=t name=tbl value=temp>
<input type=hidden id=d name=dirid>
<input type=hidden id=s name=subid>
<input type=hidden id=y name=typ value=plc>
<input type=hidden id=r name=src value=k>
</form>
<table cellpadding=3 cellspacing=0 width=100%>
    <tr>
        <td class=tdheader style="border-right: 1px solid #ffffff;" width=40 height=27>Ref</td>
        <td class=tdheader style="border-right: 1px solid #ffffff;">Directorate</td>
        <td class=tdheader style="border-right: 1px solid #ffffff;">Sub-Directorate</td>
        <td class=tdheader style="border-right: 1px solid #ffffff;">PLC Name</td>
        <td class=tdheader style="border-right: 1px solid #ffffff;">KPI Details</td>
        <td class=tdheader style="border-right: 1px solid #ffffff;">User</td>
        <td class=tdheader style="border-left: 1px solid #ffffff;">Date</td>
        <td class=tdheader style="border-left: 1px solid #ffffff;" width=40>&nbsp;</td>
    </tr>
<?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_kpi_plc_temp WHERE tmpyn = 'Y'";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $tkrow = array();
            $kpirow = array();
            if(strlen($row['tmptkid'])>0)
            {
                $sql2 = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$row['tmptkid']."'";
                include("inc_db_con2.php");
                    $tkrow = mysql_fetch_array($rs2);
                mysql_close($con2);
            }
            if(strlen($row['plckpiid'])>0 && is_numeric($row['plckpiid']))
            {
                $sql2 = "SELECT k.kpivalue, k.kpiid, s.subtxt, d.dirtxt, d.dirid, s.subid FROM assist_".$cmpcode."_".$modref."_kpi k, assist_".$cmpcode."_".$modref."_dir d, assist_".$cmpcode."_".$modref."_dirsub s WHERE k.kpiid = ".$row['plckpiid']." AND k.kpisubid = s.subid AND s.subdirid = d.dirid AND d.diryn = 'Y' AND s.subyn = 'Y' AND k.kpiyn = 'Y'";
                include("inc_db_con2.php");
                    $kpirow = mysql_fetch_array($rs2);
                mysql_close($con2);
            }
            if(count($row)>0 && count($tkrow)==4 && count($kpirow)==12)
            {
            ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdheader><?php echo($row['plcid']); ?></td>
        <td class=tdgeneral><?php echo($kpirow['dirtxt']); ?>&nbsp;</td>
        <td class=tdgeneral><?php echo($kpirow['subtxt']); ?>&nbsp;</td>
        <td class=tdgeneral><?php echo($row['plcname']); ?>&nbsp;</td>
        <td class=tdgeneral><?php echo($kpirow['kpivalue']." (".$kpirow['kpiid'].")"); ?>&nbsp;</td>
        <td class=tdgeneral><?php echo($tkrow['tkname']." ".$tkrow['tksurname']); ?>&nbsp;</td>
        <td class=tdgeneral><?php echo(date("d M Y",$row['tmpdate'])); ?>&nbsp;</td>
        <td class=tdgeneral align=center><input type=button value=View onclick="<?php echo("viewPLC(".$kpirow['kpiid'].",".$row['plcid'].",".$kpirow['dirid'].",".$kpirow['subid'].")"); ?>"></td>
    </tr>
            <?php
            }
        }
    mysql_close();
?>
</table>
<?php include("inc_goback_history.php"); ?>
</body>
</html>
