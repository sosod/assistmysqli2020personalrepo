<?php
    $r = 1;
?>
<link rel="stylesheet" href="lib/locked-column.css" type="text/css">

<base target="main">
		<script type="text/javascript">
function lockCol(tblID) {

	var table = document.getElementById(tblID);
	var button = document.getElementById('toggle');
	var cTR = table.getElementsByTagName('TR');  //collection of rows

	if (table.rows[0].cells[0].className == '')
    {
			var tr = cTR.item(0);
			tr.cells[0].className = 'locked';
			tr.cells[1].className = 'locked';
			var tr = cTR.item(1);
			tr.cells[0].className = 'locked';
			var tr = cTR.item(2);
			tr.cells[0].className = 'locked';

		for (i = 3; i < cTR.length; i++)
        {
			var tr = cTR.item(i);
			tr.cells[0].className = 'locked';
			tr.cells[1].className = 'locked';
        }
		//button.innerText = "Unlock First Column";
    }
    else
    {
		for (i = 0; i < cTR.length; i++)
			{
			var tr = cTR.item(i);
			tr.cells[0].className = '';
//			tr.cells[1].className = '';
			}
		//button.innerText = "Lock First Column";
    }
}
</script>
<script type=text/javascript>
function goTo() {
    var i = document.getElementById('gt').value;
    //alert(i);
    i=i-2;
    document.location.href = "#"+i;
}
function showGraphs() {
        var target = document.getElementById('graphs');
        target.style.display = "block";
        var target2 = document.getElementById('sgraphs');
        target2.style.display = "none";
}
function hideGraphs() {
        var target = document.getElementById('graphs');
        target.style.display = "none";
        var target2 = document.getElementById('sgraphs');
        target2.style.display = "block";
}
function showMonths() {
    var button = document.getElementById('dispbut');
    var cellsend = document.getElementById('cellsend').value;
    alert("Under development");
    if(button.value == "Year-to-date")
    {
        button.value = " All months ";
/*        var cel;
        for(c=1;c<cellsend;c++)
        {
            cel = "c"+c;
            document.getElementById(cel).style.display = "none";
        }
*/    }
    else
    {
        button.value = "Year-to-date";
/*        var cel;
        for(c=1;c<cellsend;c++)
        {
            cel = "c"+c;
            document.getElementById(cel).style.display = "block";
        }
*/    }
}
</script>
<?php
$gfs = getListKPI($dlist['gfs']);
$cells = 1;
$wfrom = $whenfrom;
$wto = $whento;

$style['th']['i']['a'] = "background-color: #fff; color:#000; text-align: center; border: 1px solid #fff; border-top: 0px; border-left: 0px;";
$style['th']['i']['b'] = "background-color: #fff; color:#000; text-align: center; border: 1px solid #fff; border-left: 0px;";

$style['dir'][1] = "background-color: #555; color:#ffffff; text-align: left; border: 1px solid #555; border-left: 0px;";
$style['dir'][2] = "background-color: #555; color:#ffffff; text-align: left; border: 1px solid #555; border-right: 1px solid #ababab;";


$style['sub'][1] = "background-color: #888; color:#ffffff; text-align: left; border: 1px solid #888; border-left: 0px; border-top: 1px solid #ababab;";
$style['sub'][2] = "background-color: #888; color:#ffffff; text-align: left; border: 1px solid #888; border-right: 1px solid #ababab; border-top: 1px solid #ababab; border-left: 1px solid #ababab;";

$style['kpi'][1][0] = "background-color: #fff; color: #000; text-align: left; border-bottom: 1px solid #ffffff; border-right: 0px; border-left: 0px solid #ababab;";
$style['kpi'][1][1] = "background-color: #fff; color: #000; text-align: center; border-bottom: 1px solid #ffffff; border-right: 0px; border-left: 0px solid #ababab;";

$style['none'][1] = "background-color: #ffffff; color: #000000; text-align: left; font-weight: normal; border: 1px solid #ababab; border-bottom: 0px; border-right: 0px;";
$style['none'][2] = "background-color: #ffffff; color: #000000; text-align: left; font-weight: normal; border: 1px solid #ababab; border-bottom: 0px; border-left: 0px;";


$style['th'][1] = "background-color: #cc0001; color: #fff; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff;";
$style['th'][2] = "background-color: #fe9900; color: #fff; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff;";
$style['th'][3] = "background-color: #006600; color: #fff; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff;";
$style['th'][4] = "background-color: #000066; color: #fff; text-align: center; border-left: 1px solid #ffffff; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff;";
/*
$style['thl'][1] = "background-color: #ffcccc; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][2] = "background-color: #ffeaca; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][3] = "background-color: #ceffde; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][4] = "background-color: #ccccff; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
*/
$style['thl'][1]['R'] = "background-color: #ffabab; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][2]['R'] = "background-color: #ffddab; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][3]['R'] = "background-color: #a3ffc2; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][4]['R'] = "background-color: #babaff; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";

$style['thl'][1]['O'] = "background-color: #ffcccc; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][2]['O'] = "background-color: #ffe6bd; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][3]['O'] = "background-color: #ceffde; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][4]['O'] = "background-color: #ccccff; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";

$style['thl'][1]['C'] = "background-color: #ffdbdf; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][2]['C'] = "background-color: #fff1de; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][3]['C'] = "background-color: #deffea; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";
$style['thl'][4]['C'] = "background-color: #ddddff; color: #000; text-align: center; border-left: 1px solid #ffffff; border-bottom: 1px solid ffffff;";



$style[0] = "background-color: #555555; color: #fff; text-align: center;";
$style[1] = "background-color: #cc0001; color: #fff; text-align: center;";
$style[2] = "background-color: #fe9900; color: #fff; text-align: center;";
$style[3] = "background-color: #006600; color: #fff; text-align: center;";
$style[4] = "background-color: #000066; color: #fff; text-align: center;";

/*
$stylel[0] = "background-color: #dddddd; color: #000; text-align: right;";
$stylel[1] = "background-color: #ffcccc; color: #000; text-align: right;";
$stylel[2] = "background-color: #ffeaca; color: #000; text-align: right;";
$stylel[3] = "background-color: #ceffde; color: #000; text-align: right;";
$stylel[4] = "background-color: #ccccff; color: #000; text-align: right;";
*/

$stylel[0] = "background-color: #dddddd; color: #000; text-align: right;";

$stylel[1]['R'] = "background-color: #ffabab; color: #000; text-align: right;";
$stylel[2]['R'] = "background-color: #ffddab; color: #000; text-align: right;";
$stylel[3]['R'] = "background-color: #a3ffc2; color: #000; text-align: right;";
$stylel[4]['R'] = "background-color: #babaff; color: #000; text-align: right;";

$stylel[1]['O'] = "background-color: #ffcccc; color: #000; text-align: right;";
$stylel[2]['O'] = "background-color: #ffe6bd; color: #000; text-align: right;";
$stylel[3]['O'] = "background-color: #ceffde; color: #000; text-align: right;";
$stylel[4]['O'] = "background-color: #ccccff; color: #000; text-align: right;";

$stylel[1]['C'] = "background-color: #ffdbdf; color: #000; text-align: right;";
$stylel[2]['C'] = "background-color: #fff1de; color: #000; text-align: right;";
$stylel[3]['C'] = "background-color: #deffea; color: #000; text-align: right;";
$stylel[4]['C'] = "background-color: #ddddff; color: #000; text-align: right;";



$section[0] = array("Capital Projects","> 0");
$section[1] = array("Operational Performance","= 0");

if($who=="ALL")
{
    $dtype = "m";
    $did = 0;
    $subs = array();
    $dept = array();
        $sql = "SELECT dirid id, dirtxt txt, dirsort sort, dirid sdid FROM assist_".$cmpcode."_".$modref."_dir WHERE diryn = 'Y'";
        include("inc_db_con.php");
            $dr = 0;
            while($row = mysql_fetch_array($rs))
            {
                $dept[$dr] = $row;
                $dr++;
            }
        mysql_close();
        foreach($dept as $dir)
        {
            $sb=0;
            $sql = "SELECT subid id, subtxt txt, subsort sort, subdirid sdid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$dir['id'];
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $dirsubs[$dir['id']][$sb] = $row;
                    $sb++;
                }
            mysql_close();
        }
}
else
{
    $who = explode("_",$who);
    $dtype = $who[0];
    $did = $who[1];
    $subs = array();
    if($who[0]=="d")
    {
        $sql = "SELECT dirid id, dirtxt txt, dirsort sort, dirid sdid FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$who[1];
        include("inc_db_con.php");
            $dept[0] = mysql_fetch_array($rs);
        mysql_close();
        $sb=0;
        $sql = "SELECT subid id, subtxt txt, subsort sort, subdirid sdid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subdirid = ".$who[1];
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $dirsubs[$who[1]][$sb] = $row;
                $sb++;
            }
        mysql_close();
    }
    else
    {
        $sql = "SELECT subid id, subtxt txt, subsort sort, subdirid sdid FROM assist_".$cmpcode."_".$modref."_dirsub WHERE subyn = 'Y' AND subid = ".$who[1];
        include("inc_db_con.php");
            $deptrow = mysql_fetch_array($rs);
        mysql_close();
        $dirsubs[$deptrow['sdid']][0] = $deptrow;
        $dept = array();
        $sql = "SELECT dirid id, dirtxt txt, dirsort sort, dirid sdid FROM assist_".$cmpcode."_".$modref."_dir WHERE dirid = ".$deptrow['sdid'];
        include("inc_db_con.php");
            $dept[0] = mysql_fetch_array($rs);
        mysql_close();
    }
}
$totalcols = 2;

if(count($dept)>0)
{
    $h1 = "Entire Municipality";
}
else
{
    $h1 = $dept[0]['txt'];
}

$who2 = "N";
$wt = 1;
$wf = 1;


            switch($when)
            {
                case "ALL":
                    $sqlt = "";
                    $wt = 12;
                    $totalhead = "";
                    break;
                case "Q1":
                    $sqlt= " AND sort < 4 ";
                    $wt = 3;
                    $totalhead = " for Q1";
                    break;
                case "Q2":
                    $sqlt= " AND sort > 3 AND sort < 7 ";
                    $wt = 6;
                    $wf = 4;
                    $totalhead = " for Q2";
                    break;
                case "Q3":
                    $sqlt= " AND sort > 6 AND sort < 10 ";
                    $wt = 9;
                    $wf = 7;
                    $totalhead = " for Q3";
                    break;
                case "Q4":
                    $sqlt= " AND sort > 9 ";
                    $wt = 12;
                    $wf = 10;
                    $totalhead = " for Q4";
                    break;
                case "YTD":
                    $sqlt= " AND sval < ".$today." ";
                    $sql = "SELECT max(sort) as sort FROM assist_".$cmpcode."_".$modref."_list_time WHERE sval < ".$today." AND yn = 'Y'";
                    include("inc_db_con.php");
                        $row = mysql_fetch_array($rs);
                        $wt = $row['sort'];
                    mysql_close();
                    if(strlen($wt)==0) {
                        $wt = 1;
                        $sqlt= " AND sort = 1 ";
                    }
                    $totalhead = " for Year-to-date";
                    break;
                case "SEL":
                    $sqlt= " AND sval >= ".$wfrom." AND eval <= ".$wto." ";
                    $sql = "SELECT min(sort) as sort FROM assist_".$cmpcode."_".$modref."_list_time WHERE sval >= ".$wfrom." AND yn = 'Y'";
                    include("inc_db_con.php");
                        $row = mysql_fetch_array($rs);
                        $wf = $row['sort'];
                    mysql_close();
                    if(strlen($wf)==0) {
                        $wf = 1;
                        $sqlt= " AND sort = 1 ";
                    }
                    $sql = "SELECT max(sort) as sort FROM assist_".$cmpcode."_".$modref."_list_time WHERE eval <= ".$wto." AND yn = 'Y'";
                    include("inc_db_con.php");
                        $row = mysql_fetch_array($rs);
                        $wt = $row['sort'];
                    mysql_close();
                    if(strlen($wt)==0) {
                        $wt = 1;
                        $sqlt= " AND sort = 1 ";
                    }
                    $totalhead = "";
                    break;
                default:
                    $sqlt = "";
                    $wt = 12;
                    $totalhead = "";
                    break;
            }

//check for vote
$lvote = "N";
$sql = "DESCRIBE assist_".$cmpcode."_".$modref."_finance_lineitems";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $fld = $row['Field'];
        if($fld == "linevote") { $lvote = "Y"; }
    }
mysql_close();

if($lvote == "Y")
{
    $sql = "SELECT count(lineid) as lc FROM assist_".$cmpcode."_".$modref."_finance_lineitems WHERE linevote <> ''";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $lc = $row['lc'];
        $lc = $lc * 1;
    mysql_close();
    if($lc == 0) { $lvote = "N"; }
    if($lvote == "Y")
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CF' AND headfield = 'vote' AND headyn = 'Y'";
        include("inc_db_con.php");
        if(mysql_num_rows($rs)==0) {
            $lvote = "N";
        } else {
            $lvotehead = mysql_fetch_array($rs);
        }
        mysql_close();
    }
}

?>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: View Monthly Cashflows - <?php echo($h1); ?></b></h1>
<p id=sgraphs><input type=button onclick=showGraphs() value="Show graph"></p>
<span id=graphs>
<center>
    <table cellpadding=3 cellspacing=0 style="border: 1px solid #ffffff;">
        <tr>
            <td valign=top  style="border: 1px solid #ffffff;"><?php include("inc_report_dashboard_fin_bar.php"); ?></td>
        </tr>
        <tr>
            <td align=center style="border: 1px solid #ffffff;"><input type=button onclick=hideGraphs() value="Hide graph"></td>
        </tr>
    </table></center>
</span>
<?php if($graph == "Y") { ?>
<script type=text/javascript>document.getElementById('sgraphs').style.display = "none";</script>
<?php } else { ?>
<script type=text/javascript>document.getElementById('graphs').style.display = "none";</script>
<?php } ?>
<table cellpadding=2 cellspacing=0 width=100% style="border: 0px solid #ffffff;">
    <tr class=tdgeneral>
        <td align=left style="border: 0px solid #ffffff;">&nbsp;</td>
        <td align=right style="border: 0px solid #ffffff;">&nbsp;<b>Display:</b> <select id=dis><option <?php if($display!="all") { echo("selected"); } ?> value=sum>Limited</option><option <?php if($display=="all") { echo("selected"); } ?> value=all>All</option></select> <input type=button value=Go onclick="changeDisplay();"></td>
    </tr>
</table>

<div id="tbl-container">
<table id="tbl" cellpadding=0 cellspacing=0 style="margin-top: 1px; margin-left: -1px;">
<!--        <tr>
            <th style="<?php echo($style['th']['i']['a']); ?>" valign=bottom height=1>.<img src=/pics/blank.gif width=200 height=1></th>
        </tr> -->
<?php
$fields = array();
$fields['budget'] = 1;
$fields['actual'] = 2;
$fields['accytd'] = 3;
$fields['totytd'] = 4;
$fields['var'] = 5;
$fields['vire'] = 6;
$fields['adjest'] = 7;
$fields['adjbudget'] = 8;
$heading = array();
$cols2 = 0;
if($display == "all")
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CF' AND headlistsort > 0 AND headyn = 'Y' AND headdetailyn = 'Y' ORDER BY headdetailsort";
}
else
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_headings WHERE headtype = 'CF' AND headlistsort > 0 AND headyn = 'Y' AND headlistyn = 'Y' ORDER BY headlistsort";
}
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $heading[] = $row;
    if($row['headfield'] == "vire" || $row['headfield']=="adjest" || $row['headfield']=="adjbudget" || $row['headfield']=="budget") { $cols2++; }
}
mysql_close();
?>
        <tr>
            <th style="<?php echo($style['th']['i']['a']); ?>" valign=bottom>&nbsp;<img src=/pics/blank.gif width=150 height=1></th>
            <th style="<?php echo($style['th']['i']['a']); ?>" valign=bottom rowspan=3><?php if($lvote == "Y") { echo($lvotehead['headlist']); } else { echo("&nbsp;"); } ?></th>
            <th style="<?php echo($style['th']['i']['a']); ?>" valign=bottom rowspan=3>GFS Classification</th>
            <?php

            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE yn = 'Y' ";
            $sql.= $sqlt;
            $sql.= " ORDER BY sort";
            include("inc_db_con.php");
                $th = 0;
                $s = 1;
                $timehead = array();
                $mnr = mysql_num_rows($rs);
                $mnr2 = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $mnr2++;
                    if($row['sval']>$today) { $colspan = 3*$cols2; } else { $colspan=count($heading)*3; }
                    //$colspan=15;
                    $totalcols = $totalcols + $colspan;
                    ?>
            <th colspan=<?php echo($colspan); ?> style="<?php echo($style['th'][$s]); ?>
            <?php //if($mnr2==$mnr) { echo("border-right: 1px solid #ababab;"); } ?>"><?php echo(date("M Y",$row['eval'])); ?></th>
                    <?php
                    $timehead[$th] = $row;
                    $th++;
                    $s++;
                    if($s>4) { $s = 1; }
                }
            mysql_close();
            ?>
        </tr>
        <tr>
            <th valign=top style="<?php echo($style['th']['i']['b']); ?>">&nbsp;</th>
            <?php
            $s = 1;
            $c = 0;
$cate[0] = array("R","rev");
$cate[1] = array("O","op");
$cate[2] = array("C","cp");
            foreach($timehead as $thead)
            {
                if($thead['sval']<$today) { $cs = count($heading); } else { $cs = $cols2; }
                $c++;
                ?>
                <th colspan=<?php echo($cs); ?> style="<?php echo($style['thl'][$s]['R']); ?>">Revenue</th>
                <th colspan=<?php echo($cs); ?> style="<?php echo($style['thl'][$s]['O']); ?>">Operational Exp.</th>
                <th colspan=<?php echo($cs); ?> style="<?php echo($style['thl'][$s]['C']); ?>
                <?php //if($c==count($timehead)) { echo("border-right: 1px solid #ababab;"); } ?>">Capital Exp.</th>
                <?php
                $s++;
                if($s>4) { $s = 1; }
            }
            ?>
        </tr>
        <tr>
            <th valign=top style="<?php echo($style['th']['i']['b']); ?>">&nbsp;</th>
            <?php
            $s = 1;
            $c = 0;
            foreach($timehead as $thead)
            {
                $c++;
                foreach($cate as $ct)
                {
                    foreach($heading as $head)
                    {
                        if($head['headfield']=="actual" || $head['headfield'] == "accytd" || $head['headfield'] == "totytd" || $head['headfield'] == "var")
                        {
                            if($thead['sval']<$today) {
                                ?>
                                <th style="<?php echo($style['thl'][$s][$ct[0]]); ?>"><?php echo($head['headlist']); ?> <img src=\pics\blank.gif height=1 width=75></th>
                                <?php
                            }
                        }
                        else
                        {
                            ?>
                            <th style="<?php echo($style['thl'][$s][$ct[0]]); ?>"><?php echo($head['headlist']); ?> <img src=\pics\blank.gif height=1 width=75></th>
                            <?php
                        }
                    }
                }
                $s++;
                if($s>4) { $s = 1; }
            }
            ?>
        </tr>
        <?php
        foreach($dept as $dir)
        {
            $subs = $dirsubs[$dir['id']];
        if(count($subs)>0)
        {
?>
        <tr>
            <td colspan=3 style="<?php echo($style['dir'][1]); ?>"><?php echo($dir['txt']); ?></td>
            <td colspan=<?php echo($totalcols-3); ?> style="<?php echo($style['dir'][1]); ?>">&nbsp;</td>
        </tr>
<?php

        }
$dircount = 0;
        foreach($subs as $subrow) { ?>
<?php
            $sql = "SELECT *";
            $sql.= " FROM assist_".$cmpcode."_".$modref."_finance_lineitems";
            $sql.= " WHERE lineyn = 'Y'";
            $sql.= " AND linesubid = ".$subrow['id'];
            $sql.= " ORDER BY linesort";
            include("inc_db_con.php");
                if(mysql_num_rows($rs)>0)
                {
?>
        <tr>
            <td colspan=3 style="<?php echo($style['sub'][1]); ?>"><?php echo($subrow['txt']); ?></td>
            <td colspan=<?php echo($totalcols-3); ?> style="<?php echo($style['sub'][1]); ?>">&nbsp;</td>
        </tr>
<?php
                    $mnr = mysql_num_rows($rs);
                    $mnr2 = 0;
                    while($row = mysql_fetch_array($rs))
                    {
                        $mnr2++;
                        $dircount++;
                        ?>
        <tr>
            <td style="<?php echo($style['kpi'][1][0]); ?>"><?php echo($row['linevalue']); ?></td>
            <td style="<?php echo($style['kpi'][1][1]); ?>" align=center><?php echo($row['linevote']); ?></td>
            <td style="<?php echo($style['kpi'][1][0]); ?>"><?php echo($gfs[$row['linegfsid']]['value']); ?></td>
                        <?php
                        $s = 1;
                        $c=0;
                        foreach($timehead as $thead)
                        {
                            $c++;
                            $sql2 = "SELECT * FROM assist_".$cmpcode."_".$modref."_finance_cashflow WHERE cflineid = ".$row['lineid']." AND cftimeid = ".$thead['id']." AND cfyn = 'Y'";
                            include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                            mysql_close($con2);
                foreach($cate as $ct)
                {
                    foreach($heading as $head)
                    {
                        $adjbudget = 0;
                        if($head['headfield']=="adjbudget")
                        {
                            if($row2['cf'.$ct[1]."8"]==0)
                            {
                                $adjbudget = $row2['cf'.$ct[1]."7"] + $row2['cf'.$ct[1]."6"] + $row2['cf'.$ct[1]."1"];
                                $row2['cf'.$ct[1]."8"] = $adjbudget;
                            }
                        }
                        if($head['headfield']=="actual" || $head['headfield'] == "accytd" || $head['headfield'] == "totytd" || $head['headfield'] == "var")
                        {
                            if($thead['sval']<$today) {
                                ?>
                                <td style="<?php echo($stylel[$s][$ct[0]]); ?>"><?php echo(number_format($row2['cf'.$ct[1].$fields[$head['headfield']]],2)); if($head['headfield']=="accytd") { echo "%"; }?></th>
                                <?php
                            }
                        }
                        else
                        {
                                ?>
                                <td style="<?php echo($stylel[$s][$ct[0]]); ?>"><?php echo(number_format($row2['cf'.$ct[1].$fields[$head['headfield']]],2)); ?></th>
                                <?php
                        }
                    }
                }
                            $s++;
                            if($s>4) { $s = 1; }
                            $row2 = array();
                        }   //foreach time
                        echo("</tr>");
                    }
                }
                else
                {
                }
            mysql_close();
        ?>



        <?php } //foreach sub-dir
        if($dircount==0)
        {
                    ?>
                    <tr>
                        <td colspan=3 style="<?php echo($style['none'][1]); ?>">&nbsp;&nbsp;No finances found. <img src="/pics/blank.gif" height=1 width=150></td>
                        <td colspan=<?php echo($totalcols-5); ?> style="<?php echo($style['none'][2]); ?>"><img src="/pics/blank.gif" height=1 width=150></td>
                    </tr>
                    <?php
        }
        
        } //foreach dept
        ?>

</table>
<?php
//print_r($heading);
?>
</div>

<input type=hidden id=cellsend value=<?php echo($cells); ?>>
<script type="text/javascript"> lockCol('tbl'); </script>

