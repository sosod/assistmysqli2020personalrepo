<?php
include("inc_ignite.php");
//include("inc_logfile.php");
//include("inc_errorlog.php");

$display = array();
$d=0;
$sql = "SELECT * FROM assist_".$cmpcode."_td_display WHERE yn = 'Y' ORDER BY sort ASC";
//include("inc_db_con.php");
$rows = $me->mysql_fetch_all($sql);
    //while($row = mysql_fetch_array($rs))
foreach($rows as $row) {
        $display[$d] = $row['value'];
        $d++;
    }
//mysql_close();

$tsort = isset($_GET['sort']) ? $_GET['sort'] : "na";

$an = "na";
$at = "ta";
$af = "fa";
$am = "ma";
$ae = "ea";
$ad = "da";
$aj = "ja";
$al = "la";

switch($tsort)
{
    case "da":
        $ad = "dd";
        break;
    case "ea":
        $ae = "ed";
        break;
    case "fa":
        $ae = "ed";
        break;
    case "ja":
        $aj = "jd";
        break;
    case "la":
        $al = "ld";
        break;
    case "ma":
        $ae = "ed";
        break;
    case "na":
        $ae = "ed";
        break;
    case "ta":
        $ae = "ed";
        break;
    default:
        break;
}


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/assist.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type="text/css">
 a { color: #ffffff; }
 a:active { color: #ffffff; }
 a:visited { color: #ffffff; }
 a:hover { color: #ffffff; }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc>User Directory</h1>
<table>
	<tr>
		<?php
        foreach($display as $dy)
        {
            switch($dy)
            {
                case "dept":
                    echo("<th><a href=main.php?sort=".$ad.">Department</th>");
                    break;
                case "email":
                    echo("<th><a href=main.php?sort=".$ae.">Email Address</a></th>");
                    break;
                case "fax":
                    echo("<th><a href=main.php?sort=".$af.">Fax</a></th>");
                    break;
                case "job":
                    echo("<th><a href=main.php?sort=".$aj.">Job Title</th>");
                    break;
                case "loc":
                    echo("<th><a href=main.php?sort=".$al.">Location</th>");
                    break;
                case "mobile":
                    echo("<th><a href=main.php?sort=".$am.">Mobile</a></th>");
                    break;
                case "name":
                    echo("<th><a href=main.php?sort=".$an.">Name</a></th>");
                    break;
                case "tel":
                    echo("<th><a href=main.php?sort=".$at.">Telephone</a></th>");
                    break;
                default:
                    break;
            }
        }
        ?>
	</tr>
<?php

//GET LIST OF USERS WHERE STATUS = ACTIVE AND TKID != ADMIN
$sql = "SELECT t.*, j.value jobt, l.value loc, d.value dept ";
$sql.= "FROM assist_".$cmpcode."_timekeep t";
$sql.= ", assist_".$cmpcode."_list_jobtitle j";
$sql.= ", assist_".$cmpcode."_list_loc l";
$sql.= ", assist_".$cmpcode."_list_dept d";
$sql.= " WHERE t.tkstatus = 1 AND t.tkid <> '0000'";
$sql.= " AND t.tkdesig = j.id ";
$sql.= " AND t.tkloc = l.id ";
$sql.= " AND t.tkdept = d.id ";
switch($tsort)
{
    case "da":
        $sql.= "ORDER BY d.value, tkname, tksurname";
        break;
    case "dd":
        $sql.= "ORDER BY d.value DESC, tkname DESC, tksurname DESC";
        break;
    case "ea":
        $sql.= "ORDER BY tkemail, tkname, tksurname";
        break;
    case "ed":
        $sql.= "ORDER BY tkemail DESC, tkname DESC, tksurname DESC";
        break;
    case "fa":
        $sql.= "ORDER BY tkfax, tkname, tksurname";
        break;
    case "fd":
        $sql.= "ORDER BY tkfax DESC, tkname DESC, tksurname DESC";
        break;
    case "ja":
        $sql.= "ORDER BY j.value, tkname, tksurname";
        break;
    case "jd":
        $sql.= "ORDER BY j.value DESC, tkname DESC, tksurname DESC";
        break;
    case "la":
        $sql.= "ORDER BY l.value, tkname, tksurname";
        break;
    case "ld":
        $sql.= "ORDER BY l.value DESC, tkname DESC, tksurname DESC";
        break;
    case "ma":
        $sql.= "ORDER BY tkmobile, tkname, tksurname";
        break;
    case "md":
        $sql.= "ORDER BY tkmobile DESC, tkname DESC, tksurname DESC";
        break;
    case "na":
        $sql.= "ORDER BY tkname, tksurname";
        break;
    case "nd":
        $sql.= "ORDER BY tkname DESC, tksurname DESC";
        break;
    case "ta":
        $sql.= "ORDER BY tktel, tkname, tksurname";
        break;
    case "td":
        $sql.= "ORDER BY tktel DESC, tkname DESC, tksurname DESC";
        break;
    default:
        $sql.= "ORDER BY tkname, tksurname";
        break;
}
//include("inc_db_con.php");
$rows = $me->mysql_fetch_all($sql);
$r = 0;
//while($row = mysql_fetch_array($rs))
foreach($rows as $row){
    $r++;
?>
	<?php include("inc_tr.php"); ?>
		<?php
        foreach($display as $dy)
        {
            switch($dy)
            {
                case "dept":
                    echo("<td>".$row['dept']."&nbsp;</td>");
                    break;
                case "email":
                    echo("<td>".$row['tkemail']."&nbsp;</td>");
                    break;
                case "fax":
                    echo("<td>".$row['tkfax']."&nbsp;</td>");
                    break;
                case "job":
                    echo("<td>".$row['jobt']."&nbsp;</td>");
                    break;
                case "loc":
                    echo("<td>".$row['loc']."&nbsp;</td>");
                    break;
                case "mobile":
					$v = $row['tkmobile'];
					if(strlen($v)>0) {
						$value = "";
						for($i=0;$i<strlen($v);$i++) {
							if(is_numeric(substr($v,$i,1))) { $value.=substr($v,$i,1); }
						}
						if(strlen($value)==10) {
							$v = substr($value,0,3)." ".substr($value,3,3)." ".substr($value,6,4);
						} else {
							$v = $value;
						}
					}
                    echo("<td>".$v."&nbsp;</td>");
                    break;
                case "name":
                    echo("<td>".$row['tkname']." ".$row['tksurname']."&nbsp;</td>");
                    break;
                case "tel":
					$v = $row['tktel'];
					if(strlen($v)>0) {
						$value = "";
						for($i=0;$i<strlen($v);$i++) {
							if(is_numeric(substr($v,$i,1))) { $value.=substr($v,$i,1); }
						}
						if(strlen($value)==10) {
							$v = "(".substr($value,0,3).") ".substr($value,3,3)." ".substr($value,6,4);
						} else {
							$v = $value;
						}
					}
					echo("<td>".$v."&nbsp;</td>");
                    break;
                default:
                    break;
            }
        }
        ?>
	</tr>
<?php
}
//mysql_close();
?>
</table>
<?php
//GET THE MOST RECENT USER MODIFIED DATE
$sql = "SELECT tkmoddate FROM assist_".$cmpcode."_timekeep ORDER BY tkmoddate DESC";
//include("inc_db_con.php");
//$row = mysql_fetch_array($rs);
$row = $me->mysql_fetch_one($sql);
$tkmoddate = $row['tkmoddate'];
//mysql_close();
//GET THE MOST RECENT USER ADD DATE
$sql = "SELECT tkadddate FROM assist_".$cmpcode."_timekeep ORDER BY tkadddate DESC";
//include("inc_db_con.php");
//$row = mysql_fetch_array($rs);
$row = $me->mysql_fetch_one($sql);
$tkadddate = $row['tkadddate'];
//mysql_close();
$tkupdate = "";
//WHICHEVER DATE IS THE MOST RECENT, SELECT THAT ONE AS THE 'UPDATED' DATE
if($tkadddate > $tkmoddate)
{
    $tkupdate = $tkadddate;
}
else
{
    $tkupdate = $tkmoddate;
}
//FORMAT AND DISPLAY THE 'UPDATED' DATE
$tkdate = date("d-M-Y",$tkupdate);
echo("<P><i>Updated: ".$tkdate."</i></p>");
?>
</body>
</html>
