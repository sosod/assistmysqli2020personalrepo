<?php
include("inc_head_setup.php");
include("inc/setup.php");
include("inc_admin.php");
?>
<h1><b><?php echo($modtitle); ?>: Setup</b></h1>
<?php displayResult($result); ?>
<form name=update method=get action=setup.php><input type=hidden name=a value=modadmin>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=30>
        <th class=lft>Module Admin</th>
        <td><select name=v>
<?php
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($setup[0][0] == "0000")
{
?>
    <option selected value=0000>Ignite Assist Administrator</option>
<?php
}
else
{
?>
    <option value=0000>Ignite Assist Administrator</option>
<?php
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = '".$modref."' ";
$sql .= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tid = $row['tkid'];
    if($tid == $setup[0][0])    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
mysql_close($con);

?>
</select></td>
        <td width=80 align=center><input type=submit value=" Save "></td>
    </tr>
    <tr height=30>
        <th class=lft>Defaults</th>
        <td>Configure default settings used throughout <?php echo $modtitle; ?>.</td>
        <td width=80 align=center><input type=button value=Configure onclick="setupGoTo('defaults');" id=1></td>
    </tr>
    <tr height=30>
        <th class=lft>User Access</th>
        <td>Configure user access.&nbsp;</td>
        <td align=center><input type=button value=Configure id=2 onclick="setupGoTo('access');"></td>
    </tr>
    <tr height=30>
        <th class=lft>Directorates</th>
        <td>Configure (Sub-)Directorate Administrators.</td>
        <td align=center><input type=button value=Configure id=3 onclick="setupGoTo('admin');"></td>
    </tr>
    <?php if($setup[2][0]=="Y") { ?>
    <tr height=30>
        <th class=lft><?php echo($setup[3][0]); ?></th>
        <td>Configure <?php echo($setup[3][0]); ?> and their Administrators.</td>
        <td align=center><input type=button value=Configure id=4 onclick="setupGoTo('wards');"></td>
    </tr>
    <?php } ?>
    <?php if($setup[1][0]=="Y") { ?>
    <tr height=30>
        <th class=lft>Towns</th>
        <td>Configure Towns and their Administrators.</td>
        <td align=center><input type=button value=Configure id=5 onclick="setupGoTo('towns');"></td>
    </tr>
    <?php } ?>
    <tr height=30>
        <th class=lft>Time Periods</th>
        <td>Configure Time Periods.</td>
        <td align=center><input type=button value=Configure id=6 onclick="setupGoTo('time');"></td>
    </tr>
    <tr height=30>
        <th class=lft>Lists</th>
        <td>Configure Drop Down Lists</td>
        <td align=center><input type=button value=Configure id=7 onclick="setupGoTo('lists');"></td>
    </tr>
    <tr height=30>
        <th class=lft>User Report</th>
        <td>Generate a report of the users of <?php echo($modtitle); ?>.</td>
        <td align=center><input type=button value=Generate id=8 onclick="setupGoTo('user_report');"></td>
    </tr>
</table>
</form>
</body>

</html>
