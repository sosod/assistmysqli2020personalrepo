<?php
    if($_FILES["ifile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
    {
        switch($_FILES["ifile"]["error"])
        {
            case 2:
                die("<h3 class=fc>Error</h3><p>Error: The file you are trying to import exceeds the maximum allowed file size of 5MB.</p>");
                break;
            case 4:
                die("<h3 class=fc>Error</h3><p>Error: Please select a file to import.</p>");
                break;
            default:
                die("<h3 class=fc>Error</h3><p>Error: ".$_FILES["ifile"]["error"]."</p>");
                break;
        }
    }
    else    //IF ERROR WITH UPLOAD FILE
    {
        $ext = substr($_FILES['ifile']['name'],-3,3);
//        echo($ext);
        if(strtolower($ext)!="csv")
        {
            die("<h3 class=fc>Error</h3><p>Error: Invalid file type.  Only CSV files may be imported.</p>");
        }
        else
        {
            $filename = substr($_FILES['ifile']['name'],0,-4)."_-_".date("Ymd_Hi",$today).".csv";
            $fileloc = "../files/".$cmpcode."/".$filename;
            //UPLOAD UPLOADED FILE
            copy($_FILES["ifile"]["tmp_name"], $fileloc);
        }
    }

if(file_exists($fileloc)==false)
{
    die("<h3 class=fc>Error</h3><p>Error: An error occurred while trying to import the file.  Please go back and try again.</p>");
}

$file = fopen($fileloc,"r");
$f = 1;
$r=1;
$data = array();
while(!feof($file))
{
    $tmpdata = fgetcsv($file);
    if(count($tmpdata)>1)
    {
        $data[$f] = $tmpdata;
        $f++;
    }
    $tmpdata = array();
}
fclose($file);

//print_r($data);

$ref = $_POST['ref'];
if($ref != $data[2][1])
{
    die("<h3>Error</h3><p>The Dashboard Reference in the import file does not match the selected Dashboard.</p>");
}

include("inc_head.php");
include("inc/Admin.php");
$frequency = getCaptureFreq();
$time = getTime($variables['timeid']);
$timeid = $variables['timeid'];
$timedate = $variables['timedate'];
//print_r($setup);
//$variables = $_REQUEST;
$ref = $variables['ref'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
    $records = getRecords($ref);
    $fields = getFields($ref);
    $vars = getVariablesData($ref);
    $types = getTypes();
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}

$thead = "";
if($dash['dashfrequency']>3) {
    switch($dash['dashfrequency'])
    {
        case 4:
            $thead.="Month ending ";
            break;
        case 5:
            $thead.="2-Months ending ";
            break;
        case 6:
            $thead.="Quarter ending ";
            break;
        case 7:
            $thead.="4-Months ending ";
            break;
        case 8:
            $thead.="6-Months ending ";
            break;
        case 9:
            $thead.="Year ending ";
            break;
    }
    $thead.= date("d F Y",$time[$timeid]['eval']);
    $timedate = date("d-m-Y",$time[$timeid]['eval']);
} else {
    $td = strFn("explode",$timedate,"-","");
    $thead = date("d F Y",mktime(12,0,0,$td[1],$td[0],$td[2]));
    //$thead = "IN PROGRESS";
}
?>
		<script type="text/javascript">
			$(function(){

                //Start
                $('.datepicker').datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/images/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-mm-yy',
                    changeMonth:true,
                    changeYear:true
                });

			});

</script>

<h1><?php echo($modtitle); ?>: Admin - Update a Dashboard</h1>
<h2>Dashboard Details</h2>
<form id=frm action="admin_update_draw3.php" method=post>
<input type=hidden name=ref value="<?php echo($ref);?>">
<input type=hidden name=timeid value="<?php echo($timeid);?>">
<input type=hidden name=frame value="<?php echo($variables['frame']);?>">
<input type=hidden name=timedate value="<?php echo($timedate);?>">
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
</table>
<p>&nbsp;</p>
<div align=center>
<?php
$frame = array();
$frameid = strFn("explode",$variables['frame'],"_","");
if(checkIntRef($frameid[1]))
{
    switch($frameid[0])
    {
        case "D":
            $frame['dir'] = getSection("D",$ref,$frameid[1]);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);
            break;
        case "S":
//            $frame['dir'] = getSection("D",$ref,$frameid[1]);
            $frame['sub'] = getSection("S",$ref,$frameid[1]);
            foreach($frame['sub'] as $sb) { $dirid = $sb[0]['subdirid']; }
            $frame['dir'] = getSection("D",$ref,$dirid);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);
            break;
        case "T":
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSection("T",$ref,$frameid[1]);
            break;
        case "W":
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['wards'] = getSection("W",$ref,$frameid[1]);
            break;
        default:
            die("An error has occurred.  Please go back and try again.");
            break;
    }
}
else
{
    die("An error has occurred.  Please go back and try again.");
}

//echo("<P>FRAME-");
//print_r($frame);
    $values = array();
//echo("<P>VAR-"); print_r($vars);
    $i = 1;

        $sections = array(
            "dir"=>$dash['dashdir'],
            "sub"=>$dash['dashsub'],
            "towns"=>$dash['dashtowns'],
            "wards"=>$dash['dashwards']
        );

            $monthcols = count($fields);

        if($sections['towns']<0) { unset($sections['towns']); }
        if($sections['wards']<0) { unset($sections['wards']); }
        $l = 0;
        $m = 2;
        for($m2=1;$m2<=$m;$m2++)
        {
            $l = $l + ($monthcols * $m);
        }
        $m--;
//echo("<P>SECTIONS-"); print_r($sections);
        echo("<table cellpadding=3 cellspacing=0 width=100%>");
        echo("<tr>");
        echo("    <td rowspan=2>&nbsp;</td>");
        echo("    <td class=tdheader colspan=$monthcols > $thead </td>");
        echo("</tr>");
        echo("<tr>");
            $mx = echoFieldDash($ref,$m,$fields);
        echo("</tr>");
        $dr = 7;
        foreach($frame['dir'] as $dir)
        {
            $lev = 1;
            $did = $dir['id'];
            $txt = $dir['txt'];
            $subs = $frame['sub'][$did];
                echo("<tr>");
                    echo("<td class=level".$lev." colspan=$l >$txt</td>");
                echo("</tr>");
                foreach($subs as $sub)
                {
                    $lev = 2;
                    $subid = $sub['id'];
                    $txt2 = $sub['txt'];
                    echo(chr(10)."<tr>");
                        echo("<td class=level".$lev." colspan=$l >$txt2</td>");
                    echo("</tr>");
                    $lev = 3;
                    $extras = array(1=>"wards",2=>"towns");
                    $preview = 0;
                    foreach($extras as $ex)
                    {
                        if($ex == "wards") { $type = "W"; } else { $type = "T"; }
                        if($sections[$ex]>0)
                        {
                            $preview++;
                            $secs = $frame[$ex];
                            $s = 1;
                            foreach($secs as $sec)
                            {
                                $sid = $sec['id'];
                                $sdfid = $sec['dfid'];
                                $stxt = $sec['txt'];
                                echo(chr(10)."<tr>");
                                    echo("<td class=level".$lev." colspan=$l >$stxt</td>");
                                echo("</tr>");
                                $s++;
                                    foreach($records as $rec)
                                    {
                                        $dr++;
                                        $recid = $rec['recid'];
                                        $fn = $rec['recfn'];
                                        if($fn == "Y") {
                                            $class = "total"; $varclass = "vartotal"; $dr--;
                                        } else {
                                            if($fn=="H") {
                                                $class = "heading"; $varclass = "varheading"; $dr--;
                                            } else {
                                                $class = "record"; $varclass = "varrecord";
                                            }
                                        }
                                        echo("<tr height=27>");
                                        echo(chr(10)."<td class=".$class." width=100>".$rec['rectxt']."</td>");
                                        $df = 3;
                                        foreach($fields as $fld)
                                        {
                                            $df++;
                                            $value = $data[$dr][$df];
                                            //$varclass = "varrecord";
                                            $fldid = $fld['fldid'];
                                            $ffn = $fld['fldfn'];
                                            if($ffn=="Y") { $df--; }
//                                            if($ffn == "Y" || $fn == "Y") { $class = "total"; $varclass = "vartotal"; }
//                                            if($fn == "Y" && $ffn == "Y") { $style = " style=\"text-decoration: underline;\""; } else { $style = ""; }
//                                            if($fld['fldtarget']=="Y" && $ffn!="Y" && $fn == "Y") { $varclass.= " target"; }
                                            if($ffn == "Y" && $fn != "H") {
                                                $class = "total"; $varclass = "vartotal";
                                            }
                                            
//                                            if($fn == "Y" && $ffn == "Y") { $style = " style=\"text-decoration: underline;\""; } else { $style = ""; }
                                            $style = "";
                                            if($fld['fldtarget']=="Y" && $ffn!="Y" && $fn == "N") { $varclass.= " target"; }
                                            $fldid = $fld['fldid'];
                                            $var = $vars[$recid][$fldid];
                                            echo(chr(10)."<td class=\"$varclass\" $style valign=middle>"); // print_r($var);
                                        if($fn=="H") { echo("&nbsp;");
                                        } else {
                                            echo("<input type=hidden name=dloc[] value=\"$did-$subid-$sdfid-$recid-$fldid\">");
                                            echo("<input type=hidden name=dtime[] value=\"".$timedate."\">");
                                            if($ffn=="Y" || $fn == "Y")
                                            {
                                                echo("<input type=hidden name=dfn[] value=\"Y\">");
                                                echo("TBC<input type=hidden name=dval[] value=\"\">");
                                            }
                                            else
                                            {
                                                echo("<input type=hidden name=dfn[] value=\"N\">");
                                                if($fld['fldtarget']=="Y" && $tkid != $dash['dashowner'])
                                                {
                                                    echo("<input type=hidden size=5 name=dval[]>&nbsp;");
                                                }
                                                else
                                                {
                                                    switch($var['vartype'])
                                                    {
                                                        case "BLK":
                                                            echo("<input type=hidden size=5 name=dval[] value=\"\">&nbsp;");
                                                            break;
                                                        case "NUM":
                                                            echo("<input type=text size=5 name=dval[] value=\"$value\"> ".$var['varunit']);
                                                            break;
                                                        case "PERC":
                                                            echo("<input type=text size=5 name=dval[] value=\"$value\"> %");
                                                            break;
                                                        case "R":
                                                            echo("R <input type=text size=5 name=dval[] value=\"$value\">");
                                                            break;
                                                        case "TS":
                                                            echo("<input type=text size=25 name=dval[] value=\"$value\" maxlength=50>");
                                                            break;
                                                        case "TM":
                                                            echo("<input type=text size=25 name=dval[] value=\"$value\" maxlength=200>");
                                                            break;
                                                        case "TL":
                                                            echo("<textarea rows=3 cols=20 name=dval[]>$value</textarea>");
                                                            break;
                                                        case "DT":
                                                            echo("<input type=text size=9 class=datepicker name=dval[] value=\"$value\">");
                                                            break;
                                                        case "LT":
                                                            $list = $var['varunit'];
                                                            if(is_numeric($list) && strlen($list)>0)
                                                            {
                                                                echo("<select name=dval[]><option selected value=X>--- SELECT ---</option>");
                                                                $sql3 = "SELECT * FROM ".$dbref."_list_dropdowns_values WHERE listid = $list AND yn = 'Y' ORDER BY value";
                                                                include("inc_db_con3.php");
                                                                    while($row3 = mysql_fetch_array($rs3))
                                                                    {
                                                                        echo("<option ");
                                                                        if($value==$row3['value']) { echo(" selected "); }
                                                                        echo("value=".$row3['id'].">".$row3['value']."</option>");
                                                                    }
                                                                mysql_close($con3);
                                                                echo("</select>");
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                        }   //recfn = H
                                            echo("</td>");
                                        }
                                        echo("</tr>");
                                    }
                            }
                        }
                    }
                }
        }
        echo("</table>");

?>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=100%>
	<tr>
		<td style="text-align:center;"><input type=submit value="  Preview  " id=1></td>
    </tr>
</table>
</div>
</form>
<?php
$urlback = "admin_update.php";
include("inc_goback.php");
?>
</body>
</html>
