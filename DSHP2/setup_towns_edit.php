<?php
include("inc_head_setup.php");
include("inc/setup.php");
$row = array();
$ref = $_GET['i'];
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function Validate(me) {
    var nt = me.w.value;
    var ot = me.w2.value;
    
    if(nt.length>0 && nt != ot)
    {
        return true;
    }
    else
    {
        alert("Please enter a new value that is different from the original value.");
        return false;
    }
}
function editWard(i) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you wish to delete this town?")==true)
        {
            document.location.href = "setup_towns.php?a=towns&act=del&w="+i;
        }
    }
    else
    {
        alert("An error has occured.\nPlease reload the page and try again.");
    }
}
function delUser(i,t) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you wish to delete "+t+" as a user from this town?")==true)
        {
            document.location.href = "setup_towns_edit.php?a=towns&act=delu&w="+i+"&i=<?php echo($ref); ?>";
        }
    }
    else
    {
        alert("An error has occured.\nPlease reload the page and try again.");
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Towns ~ Edit</b></h1>
<?php displayResult($result); ?>
<?php

if(strlen($ref)>0 && is_numeric($ref))
{
    $sql = "SELECT * FROM ".$dbref."_list_towns WHERE id = $ref AND yn = 'Y'";
    include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $detail = mysql_fetch_array($rs);
    mysql_close($con);
}
else
{
    $mnr = 0;
}
if($mnr == 0)
{
    echo("<h2>ERROR</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
?>
<form name=ward method=get action=setup_towns.php onsubmit="return Validate(this)" language=jscript>
<input type=hidden name=a value=towns><input type=hidden name=act value=edit>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;">Reference:</th>
        <td><?php echo($ref); ?><input type=hidden name=i value=<?php echo($ref); ?>></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;">Original value:</th>
        <td><?php echo($detail['value']); ?><input type=hidden name=w2 id=otxt value="<?php echo(decode($detail['value'])); ?>"></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;border-top: 1px solid #ffffff;">New value:</th>
        <td><input type=text size=50 maxlength=100 id=ntxt name=w value="<?php echo(decode($detail['value'])); ?>"></td>
    </tr>
<?php
//    $sql2 = "SELECT a.id, t.tkname, t.tksurname FROM ".$dbref."_list_admins a, assist_".$cmpcode."_timekeep t WHERE a.yn = 'Y' AND a.type = 'T' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$ref;
//    $sql2.= " ORDER BY t.tkname, t.tksurname";
    $sql2 = userList("T",$ref,"L");
    include("inc_db_con2.php");
        if(mysql_num_rows($rs2)==0)
        {
            $usrs[0][1] = "No Administrators";
        }
        else
        {
            $usrs = array();
            while($row2 = mysql_fetch_array($rs2))
            {
                $usr[0] = $row2['id'];
                $usr[1] = $row2['tkname']." ".$row2['tksurname'];
                $usrs[] = $usr;
            }
        }
    mysql_close($con2);

?>
    <tr height=27>
        <td class=tdheader valign=top style="text-align: left; padding-top: 5px; border-top: 1px solid #ffffff;">Administrators:</th>
        <?php if($usrs[0][1] == "No Administrators") { ?>
        <td>No Administrators</td>
        <?php } else { ?>
        <td><table cellpadding=1 cellspacing=0 style="border: 0px;" width=300>
        <?php foreach($usrs as $us) { ?>
        <?php include("inc_tr.php"); ?>
            <td width=271 style="border: 1px solid #ffffff; border-right: 0px;"><?php echo($us[1]); ?></td>
            <td width=29 align=center style="border: 1px solid #ffffff; border-left: 0px;"><input type=button value=Del onclick="delUser(<?php echo($us[0]); ?>,<?php echo("'".$us[1]."'"); ?>)"></td>
        </tr>
        <?php } ?>
        </table></td>
        <?php } ?>
    </tr>
    <tr height=27>
        <td colspan=2><input type=button value="Add users" onclick="document.location.href = 'setup_towns_users.php?i=<?php echo($ref); ?>';"> <input type=submit value="Save changes"> <input type=reset> <input type=button value=" Delete " onclick="editWard(<?php echo($ref); ?>)"> <input type=button value=Cancel onclick="document.location.href = 'setup_towns.php';"></td>
    </tr>
</table>
</form>
<?php
}
$urlback = "setup_towns.php";
include("inc_goback.php");
displayChangeLog("S_TOWNS",$ref,$urlback);
?>
</body>

</html>
