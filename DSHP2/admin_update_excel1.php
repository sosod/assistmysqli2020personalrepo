<?php
include("inc_head.php");
include("inc/Admin.php");
$frequency = getCaptureFreq();
$time = getTime(0);
//print_r($setup);
$variables = $_REQUEST;
$ref = $variables['ref'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}
$timeid = $variables['timeid'];
$frame = $variables['frame'];
$timedate = $variables['timedate'];
?>
<h1><?php echo($modtitle); ?>: Admin - Update a Dashboard</h1>
<h2>Dashboard Details</h2>
<form name=frmn id=frm action="admin_update_excel3.php" method=post language=jscript enctype="multipart/form-data" >
<input type=hidden name=ref value="<?php echo($ref);?>">
<input type=hidden name=timeid value="<?php echo($timeid); ?>">
<input type=hidden name=frame value="<?php echo($frame); ?>">
<input type=hidden name=timedate value="<?php echo($timedate); ?>">
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
</table>
<h2>Update details</h2>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Time Period:</td>
		<td width=400><?php echo(date("F Y",$time[$timeid]['eval'])); ?></td>
	</tr>
<?php
if(strlen($timedate)>0)
{
    $tmdt = strFn("explode",$timedate,"-","");
    ?>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Date:</td>
		<td width=400><?php echo(date("d F Y",mktime(12,0,0,$tmdt[1],$tmdt[0],$tmdt[2]))); ?></td>
	</tr>
    <?php
}
?>
<?php
$fm = strFn("explode",$frame,"_","");
$framed = getSection($fm[0],$ref,$fm[1]);
?>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Frame:</td>
		<td width=400><?php echo($framed[0]['txt']); ?></td>
	</tr>
</table>
<h2>Action</h2>
<ol>
    <li><input type=button value=Export onclick="document.location.href = 'admin_update_excel2.php?ref=<?php echo($ref); ?>&timeid=<?php echo($timeid); ?>&timedate=<?php echo($timedate); ?>&frame=<?php echo($frame); ?>';"></li>
    <li>Update the dashboard details in the CSV document saved in step 1.</li>
    <li>Import updated information: <input type=file name=ifile id=ifl> <input type=submit value=Import id=imp>
    <Br>Note: Once you have clicked the "Import" button, you will be given an opportunity to review the Import data before finalising the import.
    <br>Until you click the "Accept" button, on the next page, no information will be updated.</li>
</ol>
<p style="margin-bottom: 0;"><b>Import file information:</b>
<ol type=i>
<li>The data needs to be in CSV format, seperated by commas, in the same layout as the export file generated in step 1.</li>
<li>Please note:<ul>
    <li>By using this function, all variables for specified time period and frame will be updated with the information in the import file.</li>
    <li>Function Fields & Records will not be exported into the import file.</li>
    <li>Target Fields will not update from the import file.<Br>If you are the Dashboard Owner and wish to update Target Fields, please use the Onscreen Update method.</li>
    </ul></li>
<li>Variable Types:<ul>
    <li><i>Number/Rand/Percentage: </i>These must be entered without commas, currency (e.g. R) or percentage (%) symbols i.e. 10000 instead of R 10,000 and 23.47 instead of 23.47%.</i></li>
    <li><i>Date: </i>Dates must be entered in the following format dd-mm-yyyy e.g. 1 Nov 2009 becomes 01-11-2009<br>(Hint: To prevent Excel from automatically formatting the date, capture it with an apostrophe in front ('01-11-2009).)</i></li>
    <li><i>List: </i>The List value must be captured exactly as it is in the list on the Update form.</i></li>
    </ul></li>
</ol>
</p>

</form>
<?php
$urlback = "admin_update.php";
include("inc_goback.php");
?>
<script type=text/javascript>//document.getElementById('imp').disabled = true;</script>
<p>&nbsp;</p>
</body>
</html>
