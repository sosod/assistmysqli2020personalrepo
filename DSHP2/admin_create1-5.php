<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreate1.php");
include("inc/AdminCreateProgress.php");

$list = "SELECT id, value FROM ".$dbref."_list_towns WHERE yn = 'Y' ORDER BY value";
$page = array("next"=>"6","step"=>"5","title"=>"Towns");

$type = "T";
$admins = array();
        $sql2 = userList($type,0,"Lb");
        include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2))
            {
                $id2 = "a_".$row2['id'];
                $val2 = $row2['tkname']." ".$row2['tksurname'];
                $r2 = $row2['ref'];
                $admins[$r2].="<option value=$id2> $val2 </option>";
            }
        mysql_close($con2);
$users = "";
        $sql2 = userList($type,0,"A");
        include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2))
            {
                $id2 = "u_".$row2['tkid'];
                $val2 = $row2['tkname']." ".$row2['tksurname'];
                $users.="<option value=$id2> $val2 </option>";
            }
        mysql_close($con2);

?>

<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 1.".$page['step'].": Setup ".$page['title']."</h2>");

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(strlen($ref)==0 || $ref == 0)
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    if($dash['dashtowns']<0)
    {
        echo("<script type=text/javascript>");
        echo("document.location.href = 'admin_create1-".$page['next'].".php?ref=".$ref."';");
        echo("</script>");
    }
    else
    {

echo("<h3>".$dash['dashname']." (ref: ".$ref.")</h3>");
echo("<form id=frm onsubmit=\"return Validate(this);\" action=\"admin_create1-".$page['next'].".php\" method=post>");
echo("<input type=hidden name=step value=\"1-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>"); ?>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <th><?php echo($page['title']); ?></th>
        <th>Applicable?</th>
        <th>Primary Admin</th>
        <th>Secondary Admin</th>
    </tr>
    <?php
    $sql = $list;
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $id = $row['id'];
            $disp = $row['value'];
            $chkname = "yn_".$id;
            $admin1 = "pa_".$id;
            $admin2 = "sa_".$id;
            ?>
    <tr>
        <td style="padding-left: 7px;"><?php echo("$disp <input type=hidden name=id[] value=$id>"); ?></td>
        <td style="text-align:center;"><?php echo("<input type=checkbox name=$chkname value=Y>"); ?></td>
        <td style="text-align:center;"><?php echo("<select name=$admin1>");
        if(count($admins[$id])>0) {
            echo("<option selected value=X>--- ADMINS ---</option>");
            echo($admins[$id]);
            echo("<option value=X>--- ALL USERS ---</option>");
        } else {
            echo("<option selected value=X>--- SELECT ---</option>");
        }
        echo($users);
        echo("</select>"); ?></td>
        <td style="text-align:center;"><?php echo("<select name=$admin2>");
        if(count($admins[$id])>0) {
            echo("<option selected value=X>--- ADMINS ---</option>");
            echo($admins[$id]);
            echo("<option value=X>--- ALL USERS ---</option>");
        } else {
            echo("<option selected value=X>--- SELECT ---</option>");
        }
        echo($users);
        echo("</select>"); ?></td>
    </tr>
            <?php
        }
    mysql_close($con);
    ?>
</table>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type="submit" value="Next -->"></td>
	</tr>
</table>
</form>
<?php
        $stepprogress = setProgress(1);
        $totalprogress = setProgress(0);
        displayProgress("Step 1 Process",$stepprogress,$totalprogress);
    }   //endif dashdir < 0
} //endif ref error ?>
<P>&nbsp;</p>
</body>
</html>
