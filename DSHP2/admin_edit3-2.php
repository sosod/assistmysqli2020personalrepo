<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEdit3.php");
include("inc/AdminEditProgress.php");
$step = "3-2";
$page = array("next"=>"3","step"=>"2","title"=>"Fields - Pre-defined Lists");

$ref = $variables['ref'];
$listid = $variables['listid'];
?>
<script type=text/javascript>
function Validate() {
    var targ = document.getElementById('nextaction').value;
    if(targ == "preview")
    {
        document.forms['frm'].action = "admin_edit3-4.php";
    }
    else
    {
        document.forms['frm'].action = "admin_edit3-3.php";
    }
    document.forms['frm'].submit();
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 3.".$page['step'].": Setup ".$page['title']."</h2>");

if(strlen($ref)==0) { $ref = $result[2]; }
if(!checkIntRef($ref) || !checkIntRef($listid))
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    $fields = getFields($ref);
    $colspan=1;
    if(count($records)>0) { $colspan = 2; }
    echoDashboardTitle($ref,$dash['dashname'],$step);
echo("<form name=frm id=frm  action=\"admin_edit3-3.php\" method=post>");
echo("<input type=hidden name=step value=\"3-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=\"".$dash['dashstatus']."\">");
?>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class="tdheader b-right-w">Use?</td>
        <td class="tdheader b-right-w b-left-w">Field Name</td>
        <td class="tdheader b-right-w b-left-w">Function?</td>
        <td class="tdheader b-left-w">Target?</td>
    </tr>
    <?php
    $sql = "SELECT * FROM ".$dbref."_list_dropdowns_values WHERE listid = $listid AND yn = 'Y' ORDER BY value";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $id = $row['id'];
        $val = $row['value'];
        ?>
    <tr>
        <td width=30 align=center><input type=checkbox name=ddid[] checked value=<?php echo $id; ?>></td>
        <td class=txt><?php echo($val); ?></td>
        <td width=50 align=center><?php echo(YesNo("N")); ?></td>
        <td width=50 align=center><?php echo(YesNo("N")); ?></td>
    </tr>
        <?php
    }
    mysql_close($con);
    ?>
    <tr>
        <td colspan=4 align=center><input type=hidden name=recsort value=<?php echo($fields[count($fields)-1]['fldsort']); ?>><select name=nextaction><option selected value=more>Add more</option><option value=preview>Go to Preview</option></select> <input type=button value="Next -->" onclick="Validate();"></td>
    </tr>
</table>
    <?php
    if(count($fields) > 0)
    {
    ?>
<h4>Existing Fields</h4>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class="tdheader b-right-w b-bottom-w">Ref</td>
        <td class="tdheader b-right-w b-left-w">Field Name</td>
        <td class="tdheader b-right-w b-left-w">Function?</td>
        <td class="tdheader b-right-w b-left-w">Target?</td>
    </tr>
    <?php
        foreach($fields as $row)
        {
            $rn = $row['fldtxt'];
            $ri = $row['fldid'];
            $rf = $row['fldfn'];
            $rt = $row['fldtarget'];
            ?>
    <?php include("inc_tr.php"); ?>
        <td class="tdheader b-bottom-w b-top-w" width=40><?php echo($ri); ?></td>
        <td class=txt><?php echo($rn); ?></td>
        <td width=60 align=center><?php echo(YesNo($rf)); ?></td>
        <td width=60 align=center><?php echo(YesNo($rt)); ?></td>
    </tr>
            <?php
        }
        ?>
</table>
        <?php
    }
?>
<?php


        $stepprogress = setProgress(3);
        $totalprogress = setProgress(0);
        displayProgress("Step 3 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<p>&nbsp;</p>
</body>
</html>
