<?php
include("inc_head_setup.php");
include("inc/setup.php");
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function editWard(i) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        document.location.href = "setup_towns_edit.php?i="+i;
    }
    else
    {
        alert(i.length+"-"+isNaN(parseInt(i))+"An error has occured.\nPlease reload the page and try again.");
    }
}
function Validate(me) {
    if(me.w.value.length>0)
        return true;
    else
        return false;
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Towns</b></h1>
<?php displayResult($result); ?>
<form name=addward method=get action=setup_towns.php onsubmit="return Validate(this)" language=jscript>
<input type=hidden name=a value=towns><input type=hidden name=act value=add>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=30>
        <th width=30>Ref</th>
        <th width=460>Towns</th>
        <th width=50>Admins</th>
        <th width=60>&nbsp;</th>
    </tr>
    <?php
    $sql = "SELECT * FROM ".$dbref."_list_towns WHERE yn = 'Y' AND value <> '' ORDER BY value";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
    ?>
    <tr>
        <th><?php echo($row['id']); ?></th>
        <td style="padding-left: 5px;"><?php echo($row['value']); ?></td>
        <td align=center><?php
            //$sql2 = "SELECT count(a.id) as cid FROM ".$dbref."_list_admins a, assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u WHERE a.yn = 'Y' AND a.type = 'T' AND a.tkid = t.tkid AND t.tkstatus = 1 AND u.usrtkid = t.tkid AND usrmodref = '$modref' AND ref = ".$row['id'];
            $sql2 = userList("T",$row['id'],"C");
            include("inc_db_con2.php");
                $r2 = mysql_fetch_array($rs2);
                echo($r2['cid']);
            mysql_close($con2);
        ?></td>
        <td align=center><input type=button value=Edit onclick="editWard(<?php echo($row['id']); ?>)"></td>
    </tr>
    <?php
        }
    mysql_close($con);
    ?>
    <tr>
        <th>&nbsp;</th>
        <td colspan=2><input type=text size=50 maxlength=100 name=w></td>
        <td align=center><input type=submit value=" Add "></td>
    </tr>
</table>
</form>
<?php
$urlback = "setup.php";
include("inc_goback.php");
displayChangeLog("S_TOWNS","X",$urlback);
?>
</body>

</html>
