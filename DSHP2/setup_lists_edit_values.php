<?php
include("inc_head_setup.php");
include("inc/setup.php");
$row = array();
$ref = $_GET['i'];
$idv = $_GET['v'];
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function Validate(me) {
    var nt = me.w.value;
    var ot = me.w2.value;
    
    if(nt.length>0 && nt != ot)
    {
        return true;
    }
    else
    {
        alert("Please enter a new value that is different from the original value.");
        return false;
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Lists ~ Edit</b></h1>
<?php displayResult($result); ?>
<?php

if(strlen($ref)>0 && is_numeric($ref))
{
    $sql = "SELECT * FROM ".$dbref."_list_dropdowns WHERE id = $ref AND yn = 'Y'";
    include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $listdetail = mysql_fetch_array($rs);
    mysql_close($con);
    if($mnr>0 && strlen($idv)>0 && is_numeric($idv))
    {
        $sql = "SELECT * FROM ".$dbref."_list_dropdowns_values WHERE id = $idv AND yn = 'Y' AND listid = $ref";
        include("inc_db_con.php");
        $mnr2 = mysql_num_rows($rs);
        $detail = mysql_fetch_array($rs);
        mysql_close($con);
        if($mnr2>0)
        {
            $mnr = $mnr + $mnr2;
        }
        else
        {
            $mnr = 0;
        }
    }
}
else
{
    $mnr = 0;
}

if($mnr == 0)
{
    echo("<h2>ERROR</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
?>
<h2><?php echo($listdetail['value']); ?></h2>
<form name=ward method=get action=setup_lists_edit.php onsubmit="return Validate(this)" language=jscript>
<input type=hidden name=a value=lists><input type=hidden name=act value=editv>
<input type=hidden name=i value=<?php echo($ref); ?>><input type=hidden name=l value=<?php echo($idv); ?>>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;">Reference:</th>
        <td><?php echo($ref); ?></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;">Original List Name:</th>
        <td><?php echo($detail['value']); ?><input type=hidden name=v2 id=otxt value="<?php echo(decode($detail['value'])); ?>"></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;border-top: 1px solid #ffffff;">New List Name:</th>
        <td><input type=text size=50 maxlength=100 id=ntxt name=v value="<?php echo(decode($detail['value'])); ?>"></td>
    </tr>
    <tr height=27>
        <td colspan=2><input type=submit value="Save changes"> <input type=reset></td>
    </tr>
</table>
</form>
<?php
}
$urlback = "setup_lists_edit.php?i=".$ref;
include("inc_goback.php");
?>
</body>

</html>
