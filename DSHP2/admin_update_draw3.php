<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminUpdate.php");
//print_r($variables);
$frequency = getCaptureFreq();
$time = getTime($variables['timeid']);
$timeid = $variables['timeid'];
$timedate = $variables['timedate'];
$timeend = $time[$timeid]['eval'];
$timeenddays = date("d",$timeend);
//print_r($setup);
//$variables = $_REQUEST;
//print_r($variables);
$ref = $variables['ref'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
    $records = getRecords($ref);
    $fields = getFields($ref);
    $vars = getVariablesData($ref);
    $types = getTypes();
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}

$thead = "";
if($dash['dashfrequency']>3) {
    switch($dash['dashfrequency'])
    {
        case 4:
            $thead.="Month ending ";
            break;
        case 5:
            $thead.="2-Months ending ";
            break;
        case 6:
            $thead.="Quarter ending ";
            break;
        case 7:
            $thead.="4-Months ending ";
            break;
        case 8:
            $thead.="6-Months ending ";
            break;
        case 9:
            $thead.="Year ending ";
            break;
    }
    $thead.= date("d F Y",$time[$timeid]['eval']);
    $timedate = date("d-m-Y",$time[$timeid]['eval']);
} else {
    $td = strFn("explode",$timedate,"-","");
    $thead = date("d F Y",mktime(12,0,0,$td[1],$td[0],$td[2]));
//    $thead = "IN PROGRESS";
}
?>
<script type=text/javascript>
function Validate(me) {
    document.getElementById('act').value = me;
    document.forms['frme'].submit();
}
function delComm(dc) {
    var url = "admin_update_comment_delete.php?dc="+dc;
    newwin = window.open(url,"comment","location=0,menu=0,status=1,scrollbars=0,resizable=0,height=200,width=350");
}
function cancelDel(dc) {
    document.getElementById(dc).disabled = true;
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Update a Dashboard</h1>
<h2>Dashboard Details</h2>
<form name=frme id=frm action="admin_update_draw4.php" method=post>
<input type=hidden name=ref value="<?php echo($ref);?>">
<input type=hidden name=timeid value="<?php echo($timeid);?>">
<input type=hidden name=frame value="<?php echo($variables['frame']);?>">
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
</table>
<p><span id=inprog>In Progress...</span><span id=go>Please click the Accept button to complete the Update.</span>&nbsp;</p>
<script type=text/javascript>
document.getElementById('go').style.display = "none";
</script>
<!-- <div align=center> -->
<?php
$frame = array();
$frameid = strFn("explode",$variables['frame'],"_","");
if(checkIntRef($frameid[1]))
{
    switch($frameid[0])
    {
        case "D":
            $frame['dir'] = getSection("D",$ref,$frameid[1]);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);
            $dfid = $frame['dir'][0]['dfid'];
            break;
        case "S":
//            $frame['dir'] = getSection("D",$ref,$frameid[1]);
            $frame['sub'] = getSection("S",$ref,$frameid[1]);
            foreach($frame['sub'] as $sb) { $dirid = $sb[0]['subdirid']; }
            $frame['dir'] = getSection("D",$ref,$dirid);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);
            $dfid = $frame['sub'][$dirid][0]['dfid'];
            break;
        case "T":
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSection("T",$ref,$frameid[1]);
            $dfid = $frame['towns'][0]['dfid'];
            break;
        case "W":
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['wards'] = getSection("W",$ref,$frameid[1]);
            $dfid = $frame['wards'][0]['dfid'];
            break;
        default:
            die("An error has occurred.  Please go back and try again.");
            break;
    }
}
else
{
    die("An error has occurred.  Please go back and try again.");
}

    $i = 1;

        $sections = array(
            "dir"=>$dash['dashdir'],
            "sub"=>$dash['dashsub'],
            "towns"=>$dash['dashtowns'],
            "wards"=>$dash['dashwards']
        );

            $monthcols = count($fields);

        if($sections['towns']<0) { unset($sections['towns']); }
        if($sections['wards']<0) { unset($sections['wards']); }
        $l = 0;
        $m = 2;
        for($m2=1;$m2<=$m;$m2++)
        {
            $l = $l + ($monthcols * $m);
        }
        $m--;
//echo("<P>SECTIONS-"); print_r($sections);
        echo("<table cellpadding=3 cellspacing=0>");
        echo("<tr>");
        echo("    <td rowspan=2>&nbsp;</td>");
        echo("    <td class=tdheader colspan=$monthcols > $thead </td>");
        echo("</tr>");
        echo("<tr>");
            $mx = echoFieldDash($ref,$m,$fields);
        echo("</tr>");
        foreach($frame['dir'] as $dir)
        {
            $lev = 1;
            $did = $dir['id'];
            $txt = $dir['txt'];
            $subs = $frame['sub'][$did];
                echo("<tr>");
                    echo("<td class=level".$lev." colspan=$l >$txt</td>");
                echo("</tr>");
                foreach($subs as $sub)
                {
                    $lev = 2;
                    $subid = $sub['id'];
                    $txt2 = $sub['txt'];
                    echo(chr(10)."<tr>");
                        echo("<td class=level".$lev." colspan=$l >$txt2</td>");
                    echo("</tr>");
                    $lev = 3;
                    $extras = array(1=>"wards",2=>"towns");
                    $preview = 0;
                    foreach($extras as $ex)
                    {
                        if($ex == "wards") { $type = "W"; } else { $type = "T"; }
                        if($sections[$ex]>0)
                        {
                            $preview++;
                            $secs = $frame[$ex];
                            $s = 1;
                            foreach($secs as $sec)
                            {
                                $sid = $sec['id'];
                                $sdfid = $sec['dfid'];
                                $stxt = $sec['txt'];
                                echo(chr(10)."<tr>");
                                    echo("<td class=level".$lev." colspan=$l >$stxt</td>");
                                echo("</tr>");
                                $s++;
                                    foreach($records as $rec)
                                    {
                                        $recid = $rec['recid'];
                                        $fn = $rec['recfn'];
//                                        if($fn == "Y") { $class = "total"; $varclass = "vartotal"; } else { $class = "record"; $varclass = "varrecord"; }
                                        switch($fn) {
                                            case "H":
                                                $class = "heading"; $varclass = "varheading";
                                                break;
                                            case "Y":
                                                $class = "total"; $varclass = "vartotal";
                                                break;
                                            default:
                                                $class = "record"; $varclass = "varrecord";
                                                break;
                                        }
                                        echo("<tr height=27>");
                                        echo(chr(10)."<td class=".$class." width=100>".$rec['rectxt']."</td>");
                                        foreach($fields as $fld)
                                        {
                                            $fldid = $fld['fldid'];
                                            $ffn = $fld['fldfn'];
                                            if($ffn == "Y" && $fn != "H") { $class = "total"; $varclass = "vartotal"; }
                                            if($fn == "Y" && $ffn == "Y") { $varclass.= " style=\"text-decoration: underline;\""; }
                                            $var = $vars[$recid][$fldid];
                                            echo(chr(10)."<td class=$varclass valign=middle>"); // print_r($var);
                                        if($fn=="H") { echo("&nbsp;");
                                        } else {
                                            if($ffn=="Y" || $fn == "Y")
                                            {
/*                                                echo("DIR: $did");
                                                echo("<br>SUB: $subid");
                                                echo("<br>FRAME: $sdfid");
                                                echo("<br>REC: $recid");
                                                echo("<br>FLD: $fldid");
                                                echo("<br>VALUE: ".$data[$did][$subid][$sdfid][$recid][$fldid]['val']);
                                                echo("<br>TIME: ".$data[$did][$subid][$sdfid][$recid][$fldid]['time']);
                                                echo("<br>FN: ".$data[$did][$subid][$sdfid][$recid][$fldid]['fn']);
                                                echo("<br>VAR: "); print_r($var);*/
                                                $sqlfn = $data[$did][$subid][$sdfid][$recid][$fldid]['fn'];
                                                $sqltd = strFn("explode",$data[$did][$subid][$sdfid][$recid][$fldid]['time'],"-","");
                                                $sqlval = fnDisplay($recid,$fldid,$values[$did][$subid],$var,$sdfid);
                                                $data[$did][$subid][$sdfid][$recid][$fldid]['val'] = $sqlval;
                                                $vtype = $var['vartype'];
                                                if($vtype == "NUM" || $vtype == "R" || $vtype == "PERC" || $vtype == "DT")
                                                    $values[$did][$subid][$sdfid][$recid][$fldid] = $sqlval;
                                                
                                            }
                                            else
                                            {
/*                                                echo("DIR: $did");
                                                echo("<br>SUB: $subid");
                                                echo("<br>FRAME: $sdfid");
                                                echo("<br>REC: $recid");
                                                echo("<br>FLD: $fldid");
                                                echo("<br>VALUE: ".$data[$did][$subid][$sdfid][$recid][$fldid]['val']);
                                                echo("<br>TIME: ".$data[$did][$subid][$sdfid][$recid][$fldid]['time']);
                                                echo("<br>FN: ".$data[$did][$subid][$sdfid][$recid][$fldid]['fn']);
                                                echo("<br>VAR: "); print_r($var);*/
                                                $sqlval = $data[$did][$subid][$sdfid][$recid][$fldid]['val'];
                                                if($var['vartype']!="DT") {
                                                    if($var['vartype']!="LT")
                                                    {
                                                        $sqlval = code($sqlval);
                                                    } else {
                                                        if($sqlval == "X")
                                                        {
                                                            $sqlval = "";
                                                        }
                                                    }
                                                } else {
                                                    if(strlen($sqlval)>0)
                                                    {
                                                        $sqlv = strFn("explode",$sqlval,"-","");
                                                        $sqlval = mktime(12,0,0,$sqlv[1],$sqlv[0],$sqlv[2]);
                                                    } else {
                                                        $sqlval = "";
                                                    }
                                                }
                                                switch($var['vartype'])
                                                {
                                                    case "R":
                                                        echo("R ".number_format($sqlval,2));
                                                        break;
                                                    case "NUM":
                                                        echo(number_format($sqlval,2)." ".$var['varunit']);
                                                        break;
                                                    case "PERC":
                                                        echo(number_format($sqlval,2)." ".$var['varunit']);
                                                        break;
                                                    case "DT":
                                                        echo(date("d M Y",$sqlval));
                                                        break;
                                                    case "LT":
                                                        if(is_numeric($sqlval) && strlen($sqlval)>0) {
                                                            $sql3 = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = ".$sqlval;
                                                            include("inc_db_con3.php");
                                                                $row3 = mysql_fetch_array($rs3);
                                                                echo($row3['value']);
                                                            mysql_close($con3);
                                                        } else {
                                                            echo(" ");
                                                        }
                                                        break;
                                                    default:
                                                        echo(strFn("str_replace",decode($sqlval),chr(10),"<br>"));
                                                        break;
                                                }
                                                $sqlfn = $data[$did][$subid][$sdfid][$recid][$fldid]['fn'];
                                                $sqltd = strFn("explode",$data[$did][$subid][$sdfid][$recid][$fldid]['time'],"-","");
                                            }
                                            $sql = "DELETE FROM ".$dbref."_dashboard_data WHERE ";
                                            $sql.= "  datadashid = $ref";
                                            $sql.= " AND datadirid = $did";
                                            $sql.= " AND datasubid = $subid";
                                            $sql.= " AND dataframeid = $sdfid";
                                            $sql.= " AND datarecid = $recid";
                                            $sql.= " AND datafldid = $fldid";
                                            $sql.= " AND datatimeid = $timeid";
                                            $sql.= " AND datatimeday = ".($sqltd[0]*1);
                                            $sql.= " AND datatimemonth = ".($sqltd[1]*1);
                                            $sql.= " AND datatimeyear = ".$sqltd[2];
                                            include("inc_db_con.php");

                                            $sql = "INSERT INTO ".$dbref."_dashboard_data SET ";
                                            $sql.= "  datadashid = $ref";
                                            $sql.= ", datadirid = $did";
                                            $sql.= ", datasubid = $subid";
                                            $sql.= ", dataframeid = $sdfid";
                                            $sql.= ", datarecid = $recid";
                                            $sql.= ", datafldid = $fldid";
                                            $sql.= ", datafn = '$sqlfn'";
                                            $sql.= ", datastatus = 'P'";
                                            $sql.= ", datavalue = '$sqlval'";
                                            $sql.= ", datatimeid = $timeid";
                                            $sql.= ", datatimeday = ".($sqltd[0]*1);
                                            $sql.= ", datatimemonth = ".($sqltd[1]*1);
                                            $sql.= ", datatimeyear = ".$sqltd[2];
                                            $sql.= ", datamoduser = '$tkid'";
                                            $sql.= ", datamoddate = now()";
                                            include("inc_db_con.php");
                                                $dataid = mysql_insert_id($con);
                                                echo("<input type=hidden name=dataid[] value=".$dataid." >");
                                        }
                                            echo("</td>");
                                        }
                                        echo("</tr>");
                                    }
                            }
                        }
                    }
                }
        }
        echo("</table>");

?>
</div>
<h2>Comments</h2>
<table cellpadding=5 cellspacing=0 width=600>
<?php
//print_r($frame);
//echo(":".$dfid.":");
$comments = getComments($ref,$timeid,$dfid);
//print_r($comments);
foreach($comments as $comm)
{
    echo("<tr>");
    echo("<td width=90%>");
            $dt = strfn("explode",$comm['dcdate']," ","");
            $dt2 = strfn("explode",$dt[0],"-","");
            $dt =  strfn("explode",$dt[1],":","");
            $dt3 = mktime($dt[0],$dt[1],$dt[2],$dt2[1],$dt2[2],$dt2[0]);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$comm['dctkid']."'";
    include("inc_db_con.php");
        $tk = mysql_fetch_array($rs);
    mysql_close($con);
    $sql = "SELECT * FROM ".$dbref."_dashboard_frame WHERE dfid = ".$comm['dcdfid'];
    include("inc_db_con.php");
        $df = mysql_fetch_array($rs);
    mysql_close($con);
    $fr = getSection($df['dftype'],$ref,$df['dfforeignid']);
    echo("<p  style=\"text-indent: -25px; margin-left: 25px\">".$tk['tkname']." ".$tk['tksurname']." (".$fr[0]['txt'].") commented on ".date("d F Y H:i",$dt3)."<br><i>&quot;".$comm['dccomment']."&quot;</i>");
    echo("</td>");
    echo("<td align=center width=10%>");
    if($comm['dctkid']==$tkid) {
        echo("<input type=button value=\"Delete\" onclick=\"delComm(".$comm['dcid'].");\" id=".$comm['dcid'].">");
    } else {
        echo("&nbsp;</p>");
    }
    echo("</td>");
    echo("</tr>");
}
?>
	<tr>
		<td colspan=2><b>New comment:</b><br>
        <textarea rows=5 cols=100 name=comment></textarea></td>
    </tr>
</table>
<div align=center>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=100%>
	<tr>
		<td style="text-align:center"><input type=hidden value="<?php echo($variables['dash']); ?>" name=dash><input type=hidden name=action id=act value=""> <input type=button value="  Accept  " onclick="Validate('Y');"> <input type=button value="  Reject  " onclick="Validate('N');"></td>
    </tr>
</table>
</div>
</form>
<script type=text/javascript>
document.getElementById('inprog').style.display = "none";
document.getElementById('go').style.display = "block";
</script>
<?php
$urlback = "admin_update.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
