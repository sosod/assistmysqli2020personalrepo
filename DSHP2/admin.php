<?php include("inc_head.php"); ?>
<?php
$admin = "N";
$create = "N";
$update = "N";
$dashadmin = "N";
if($access['activate']=="Y" || $access['capture']=="Y" || $access['dash']=="Y")
{
    $admin = "Y";
    if($access['activate']=="Y" || $access['dash']=="Y") { $create = "Y"; $update = "Y"; }
    if($update == "N" && $access['capture']=="Y") { $update = "Y"; }
    if($access['dash'] == "Y") { $dashadmin = "Y"; }
}
?>
<h1><b><?php echo($modtitle); ?>: Admin</b></h1>
<?php displayResult($result); ?>
<?php
if($admin == "N")
{
    echo("<P>You do not have permission to create or update a dashboard.</p>");
}
else
{
?>
<table cellpadding=3 cellspacing=0 width=600>
    <?php if($create == "Y") { ?>
    <tr height=30>
        <th style="padding-left: 5px;" class=lft colspan=2>Create</th>
    </tr>
    <tr height=30>
        <td width=530 style="padding-left: 10px;">Create a new dashboard.</td>
        <td width=70 style="text-align:center;"><input type=button value=Create id=1 onclick="adminGoTo('create1-1');"></td>
    </tr>
    <tr height=30>
        <td width=530 style="padding-left: 10px;">Finalise an incomplete new dashboard.</td>
        <td width=70 style="text-align:center;"><input type=button value=Create id=2 onclick="adminGoTo('create_pending');"></td>
    </tr>
    <?php } ?>
    <?php if($update == "Y" || $create=="Y") { ?>
    <tr height=30>
        <th style="padding-left: 5px;" class=lft colspan=2>Update</th>
    </tr>
    <?php if($create == "Y") { ?>
    <tr height=30>
        <td style="padding-left: 10px;">Edit my dashboards.</td>
        <td style="text-align:center;"><input type=button value=Update id=3 onclick="adminGoTo('edit');"></td>
    </tr>
    <?php }
    if($update == "Y") { ?>
    <tr height=30>
        <td style="padding-left: 10px;">Update a dashboard.</td>
        <td style="text-align:center;"><input type=button value=Update id=4 onclick="adminGoTo('update');"></td>
    </tr>
    <?php } } ?>
    <?php if($dashadmin == "Y") { ?>
    <tr height=30>
        <th style="padding-left: 5px;" class=lft colspan=2>Dashboard</th>
    </tr>
    <tr height=30>
        <td style="padding-left: 10px;">Transfer ownership of dashboards.</td>
        <td style="text-align:center;"><input type=button value=Update id=6 onclick="adminGoTo('dash_own');"></td>
    </tr>
    <tr height=30>
        <td style="padding-left: 10px;">Administer all dashboards.</td>
        <td style="text-align:center;"><input type=button value=Update id=5 onclick="adminGoTo('dash');"></td>
    </tr>
    <tr height=30>
        <td style="padding-left: 10px;">User Responsibility Report</td>
        <td style="text-align:center;"><input type=button value=View id=5 onclick="adminGoTo('userreport');"></td>
    </tr>
    <?php } ?>
</table>
<script type=text/javascript>
//butKill(5,5);
//butKill(3,3);
</script>
<?php
}
?>
</body>

</html>
