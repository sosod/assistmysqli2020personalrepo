//TR HOVER
function hovCSS(me) {
  document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
  document.getElementById(me).className = 'blank';
}
function clickCSS(me) {
    var targ = document.getElementById(me);
    if(targ.className == "tdhover2") { targ.className = 'blank'; } else { targ.className = 'tdhover2' }
}

//CONFIGURE BUTTON IN SETUP
function setupGoTo(name) {
    var url = "setup_"+name+".php";
    document.location.href = url;
}

//BUTTONS IN ADMIN
function adminGoTo(name) {
    var url = "admin_"+name+".php";
    document.location.href = url;
}

//BUTTON DISABLE FOR DEV
function butKill(a,b) {
    for(f=a;f<=b;f++)
    {
        document.getElementById(f).disabled = true;
    }
}

//Preview dashboard
function previewDash(ref) {
    if(!isNaN(parseInt(ref)))
    {
        preview = window.open("admin_create_preview.php?ref="+ref,"preview","location=0,menu=1,status=1,scrollbars=1,resizable=1");
        preview.moveTo(0,0);
        preview.location.reload();
    }
}

function moveOn(actt) {
    if(actt.length==1)
    {
        if(actt!="C" || (actt=="C" && confirm("Are you sure you wish to delete this dashboard?.")) )
        {
            document.getElementById('actt').value = actt;
            document.forms['frm'].submit();
        }
    }
}
