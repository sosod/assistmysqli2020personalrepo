<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEdit3.php");
include("inc/AdminEditProgress.php");
$step = "3-3";
$page = array("next"=>"4","step"=>"3","title"=>"Fields - Manual");

?>
<script type=text/javascript>
function Validate() {
    var targ = document.getElementById('nextaction').value;
    if(targ == "preview")
    {
        document.forms['frm'].action = "admin_edit3-4.php";
    }
    else
    {
        document.forms['frm'].action = "admin_edit3-3.php";
    }
    document.forms['frm'].submit();
}
function delRecord(ri,ref) {
    if(!isNaN(parseInt(ri)) && !isNaN(parseInt(ref)) && confirm("Are you sure you wish to delete field "+ri+"?"))
    {
        var url = "admin_edit3-3.php?";
        url = url + "stepact=save";
        url = url + "&step=3-3";
        url = url + "&ref="+ref;
        url = url + "&recid="+ri;
        url = url + "&nextaction=delete";
        document.location.href = url;
    }
}
function editRecord(ri,ref) {
    if(!isNaN(parseInt(ri)) && !isNaN(parseInt(ref)))
    {
        var url = "admin_edit3-3.php?";
        url = url + "stepact=save";
        url = url + "&step=3-3";
        url = url + "&ref="+ref;
        url = url + "&fldid="+ri;
        url = url + "&nextaction=edit";
        document.location.href = url;
    }
}
function editSave(ref,act,ri) {
    var rn = document.getElementById('rn').value;
    var fn = document.getElementById('fn').value;
    var rt = document.getElementById('rt').value;
    if(!isNaN(parseInt(ri)) && !isNaN(parseInt(ref)) && rn.length>0 && fn.length==1 && rt.length==1)
    {
        var url = "admin_edit3-3.php?";
        url = url + "stepact=save";
        url = url + "&step=3-3";
        url = url + "&ref="+ref;
        url = url + "&recid="+ri;
        url = url + "&rn="+escape(rn);
        url = url + "&fn="+escape(fn);
        url = url + "&rt="+escape(rt);
        url = url + "&nextaction="+act;
        document.location.href = url;
    }
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 3.".$page['step'].": Setup ".$page['title']."</h2>");

if(strlen($ref)==0) { $ref = $result[2]; }
if(strlen($ref)==0 || $ref == 0)
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    $colspan=1;
    echoDashboardTitle($ref,$dash['dashname'],$step);
echo("<form name=frm id=frm onsubmit=\"return Validate(this);\" action=\"admin_edit3-3.php\" method=post>");
echo("<input type=hidden name=step value=\"3-".$page['step']."\">");
echo("<input type=hidden name=ref value=$ref >");
echo("<input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=\"".$dash['dashstatus']."\">");

if($nextaction != "edit")
{
    $fields = getFields($ref);
?>
<h4>Add New Fields</h4>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class="tdheader b-right-w">Field Name</td>
        <td class="tdheader b-left-w">Function?</td>
        <td class="tdheader b-left-w">Target?</td>
    </tr>
    <?php
    for($t=1;$t<6;$t++)
    {
        ?>
    <tr>
        <td class=txt><input type=text name=rn[] maxlength=150 size=50></td>
        <td align=center><select name=fn[]><?php echo(selectYesNo("N")); ?></select></td>
        <td align=center><select name=rt[]><?php echo(selectYesNo("N")); ?></select></td>
    </tr>
        <?php
    }
    ?>
    <tr>
        <td colspan=3 style="text-align:center;"><input type=hidden name=recsort value=<?php echo($fields[count($fields)-1]['fldsort']+1); ?>><select name=nextaction id=nextaction><option value=more>Add more</option><option value=preview>Go to Preview</option></select> <input type=button value="Next -->" onclick="Validate();"> <input type=button value="Display Order" onclick="document.location.href = 'admin_order.php?ref=<?php echo($ref); ?>&type=FLD&src=admin_edit';"></td>
    </tr>
</table>
    <?php
    if(count($fields) > 0)
    {
    ?>
<h4>Existing Fields</h4>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td class="tdheader b-right-w b-bottom-w">Ref</td>
        <td class="tdheader b-right-w b-left-w">Field Name</td>
        <td class="tdheader b-right-w b-left-w">Function?</td>
        <td class="tdheader b-right-w b-left-w">Target?</td>
        <td class="tdheader b-left-w">&nbsp;</td>
    </tr>
    <?php
        foreach($fields as $row)
        {
            $rn = $row['fldtxt'];
            $ri = $row['fldid'];
            $rf = $row['fldfn'];
            $rt = $row['fldtarget'];
            if($rf == "Y") {
                $style = "font-weight: bold;";
            } else {
                if($rt=="Y") {
                    $style = "font-style: italic;";
                } else {
                    $style = "";
                }
            }
            ?>
    <?php include("inc_tr.php"); ?>
        <td class="tdheader b-bottom-w b-top-w" width=40><?php echo($ri); ?></td>
        <td class=txt style="<?php echo($style); ?>"><?php echo($rn); ?></td>
        <td width=60 style="text-align:center;<?php echo($style); ?>"><?php echo(YesNo($rf)); ?></td>
        <td width=60 style="text-align:center;<?php echo($style); ?>"><?php echo(YesNo($rt)); ?></td>
        <td align=center width=60><input type=button value=Edit onclick="editRecord(<?php echo $ri; ?>,<?php echo $ref; ?>);"> <input type=button value=Del onclick="delRecord(<?php echo $ri; ?>,<?php echo $ref; ?>);"></td>
    </tr>
            <?php
        }
        ?>
</table>
        <?php
    }
} else { //if nextaction!=edit
    $fldid = $variables['fldid'];
    $field = getField($fldid);
    //$field = $field[0];
    ?>
<h4>Edit Field</h4>
<table cellpadding=3 cellspacing=0 width=500>
    <tr height=27>
        <td width=170 class="tdheaderl b-bottom-w">Field Reference:</td>
        <td width=330><?php echo($recid); ?></td>
    </tr>
    <tr height=27>
        <td colspan=2 class="tdheaderl b-top-w">Original Details:</td>
    </tr>
    <tr height=27>
        <td class="level2">Field Name:</td>
        <td><?php echo($field['fldtxt']); ?></td>
    </tr>
    <tr height=27>
        <td class="level2">Function?:</td>
        <td><?php echo(YesNo($field['fldfn'])); ?></td>
    </tr>
    <tr height=27>
        <td class="level2">Target?:</td>
        <td><?php echo(YesNo($field['fldtarget'])); ?></td>
    </tr>
    <tr height=27>
        <td colspan=2 class="tdheaderl b-top-w">New Details:</td>
    </tr>
    <tr height=27>
        <td class="level2">Field Name:</td>
        <td><?php echo("<input type=text id=rn size=50 maxlength=100 value=\"".$field['fldtxt']."\">"); ?></td>
    </tr>
    <tr height=27>
        <td class="level2">Function?:</td>
        <td><select id=fn><?php echo(selectYesNo($field['fldfn'])); ?></select></td>
    </tr>
    <tr height=27>
        <td class="level2">Target?:</td>
        <td><select id=rt><?php echo(selectYesNo($field['fldtarget'])); ?></select></td>
    </tr>
    <tr height=27>
        <td colspan=2 align=center><input type=button value="Save Changes" onclick="editSave(<?php echo("$ref,'editsave',$fldid"); ?>);"> <input type=button value=Cancel onclick="editSave(<?php echo("$ref,'cancel',$recid"); ?>);"></td>
    </tr>
</table>
    <?php
}
        $stepprogress = setProgress(3);
        $totalprogress = setProgress(0);
        displayProgress("Step 3 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<p>&nbsp;</p>
</body>
</html>
