<?php
include("inc_ignite.php");
include("inc/dashp.php");
include("inc/getLists.php");

include("inc/Admin.php");

$colors = array("#009900","#000099","#fe9900","#cc0001","#555555");

function fnDisplayReport($recid,$fldid,$values,$var) {
    global $dbref;
    global $cmpcode;
    global $ref;
    global $timeenddays;
    if($var['varfn']=="N" || strlen($var['varfn'])<1)
    {
        $echo = "N/A";
    }
    else
    {
        $v = 0;
        $fid = array();
        $fid = getVariablesFn($var['varid']);
        $fnsrc = $var['varfnsrc'];
        $vals = array();
        foreach($fid as $f)
        {
            if($fnsrc=="REC")
            {
                $vals[] = $values[$f][$fldid];
            }
            else
            {
                $vals[] = $values[$recid][$f];
            }
        }
        $val = doFn($var['varfn'],$vals);
        switch($var['vartype'])
        {
            case "DT":
                $val2 = $val2 / 86400;
                if($val2 != floor($val2)) {
                    $echo = number_format($val2,2);
                } else {
                    $echo = $val2;
                }
                $echo.= " day(s)";
                break;
            case "NUM":
                if($var['varfn']!= "COUNT")
                    $echo = number_format($val,2)." ".$var['varunit'];
                else
                    $echo = $val." ".$var['varunit'];
                break;
            case "PERC":
                $echo = number_format($val,2)." ".$var['varunit'];
                break;
            case "R":
                $echo = "R ".number_format($val,2);
                break;
            default:
                $echo = $val;
                break;
        }
    }
    $ret = array($echo,$val);
    return $ret;
}

function drawTable($from,$to) {
    global $cmpcode;
    global $dbref;
    global $ref;
    global $dash;
    global $records;
    global $fields;
    global $vars;
    global $time;
    global $data;
    $ndata = array();
    $result = "\r\n";
    
    for($t=$from;$t<=$to;$t++)
    {
        $timeenddays = $timeenddays + date("d",$time[$t]['eval']);
    }

    if($from==$to) {
        $result.="\"".date("F Y",$time[$from]['eval'])."\"";
    } else {
        $result.="\"".date("F Y",$time[$from]['sval'])." - ".date("F Y",$time[$to]['eval'])."\"";
    }
    $result.="\r\n";

$result.= "\"\"";
        foreach($fields as $fld)
        {
            $result.=",\"".decode($fld['fldtxt'])."\"";
        }
$result.= "\r\n";
    foreach($records as $rec)
    {
        $r = $rec['recid'];
        $fn = $rec['recfn'];
        $result.= "\"".decode($rec['rectxt'])."\"";
        if($fn!="H") {
        foreach($fields as $fld)
        {
            $f = $fld['fldid'];
            $v = $vars[$r][$f];
            $result.= ",\"";
            if($v['vartype']=="BLK") {
                $echo = "";
            } else {
            if(strlen($v['varfn'])==0)
            {
                $sql = "SELECT * FROM ".$dbref."_dashboard_data WHERE datadashid = $ref AND datarecid = $r AND datafldid = $f AND datastatus = 'Y'";
                if($dash['dashfrequency']>3)
                {
                    $sql.= " AND datatimeid >= $from AND datatimeid <= $to ";
                }
                $sql.= " ORDER BY datatimeid, datadirid, datasubid, dataframeid";
                include("inc_db_con.php");
                $c = 0;
                while($row = mysql_fetch_array($rs))
                {
                    if(strlen($row['datavalue'])>0)
                    {
                    if($v['vartype']=='NUM' || $v['vartype']=='R' || $v['vartype']=='PERC')
                    {
                        $ndata[$r][$f] = $ndata[$r][$f] + $row['datavalue'];
                    }
                    else
                    {
                        switch($v['vartype'])
                        {
                            case "DT":
                                if(strlen($row['datavalue'])>0) {
                                    if(strlen($ndata[$r][$f])>0) { $ndata[$r][$f].=chr(10); }
                                    $ndata[$r][$f].= date("d M Y",$row['datavalue']);
                                }
                                break;
                            case "LT":
                                $lti = $row['datavalue'];
                                if(checkIntRef($lti)) {
                                    $sql3 = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = ".$lti;
                                    include("inc_db_con3.php");
                                        $row3 = mysql_fetch_array($rs3);
                                        if(strlen($ndata[$r][$f])>0) { $ndata[$r][$f].=chr(10); }
                                        $ndata[$r][$f].= decode($row3['value']);
                                    mysql_close($con3);
                                }
                                break;
                            default:
                                if(strlen($row['datavalue'])>0) {
                                    if(strlen($ndata[$r][$f])>0) { $ndata[$r][$f].=chr(10); }
                                    $ndata[$r][$f].= decode($row['datavalue']);
                                }
                                break;
                        }
                    }
                    }
                    $c++;
                }
                $val = $ndata[$r][$f];
                $echo = "";
                switch($v['vartype'])
                {
                    case "NUM":
                        $echo = number_format($val,2)." ".$v['varunit'];
                        break;
                    case "R":
                        $echo = "R ".number_format($val,2);
                        break;
                    case "PERC":
                        $echo = number_format($val,2)." %";
                        break;
                    default:
                        $echo = $val;
                        break;
                }
            }
            else
            {
                $ret = fnDisplayReport($r,$f,$ndata,$v);
                $ndata[$r][$f] = $ret[1];
                $echo = $ret[0];
            } //varfn
            }   //vartype = blk
            $result.= $echo."\"";
        }
        }
        $result.= "\r\n";
    }

$return = array("d"=>$ndata,"r"=>$result);
    return $return;
}


function drawComment($from,$to) {
    global $cmpcode;
    global $dbref;
    global $ref;

    $comments = getAllComments($ref,$from);
    if(count($comments)>0)
    {
        $table = "\"Update Comments\"\r\n";
        foreach($comments as $comm)
        {
            $table.= "\"";
            $dt = strfn("explode",$comm['dcdate']," ","");
            $dt2 = strfn("explode",$dt[0],"-","");
            $dt =  strfn("explode",$dt[1],":","");
            $dt3 = mktime($dt[0],$dt[1],$dt[2],$dt2[1],$dt2[2],$dt2[0]);
            $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$comm['dctkid']."'";
            include("inc_db_con.php");
                $tk = mysql_fetch_array($rs);
            mysql_close($con);
            $sql = "SELECT * FROM ".$dbref."_dashboard_frame WHERE dfid = ".$comm['dcdfid'];
            include("inc_db_con.php");
                $df = mysql_fetch_array($rs);
            mysql_close($con);
            $fr = getSection($df['dftype'],$ref,$df['dfforeignid']);
            $table.= $tk['tkname']." ".$tk['tksurname']." (".$fr[0][2].") commented on ".date("d F Y H:i",$dt3).":".chr(10).decode($comm['dccomment']);
            $table.= "\"";
            $table.= "\r\n";
        }
        $table.= "\r\n";
    }
    return $table;
}




$ref = $variables['ref'];
$summary = $variables['summary'];
$from = $variables['from'];
$to = $variables['to'];
$tpact = $variables['tpact'];
$graph = $variables['graph'];
//$output = $variables['output'];
$data = array();

if(!checkIntRef($ref)) { echoError(); }

$dash = getDashboard($ref);
$fields = getFields($ref);
$records = getRecords($ref);
$vars = getVariables($ref);
$time = getTime();
$frequency = getCaptureFreq();
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
$types = getTypes();

if($graph!="N")
{
    $g = 0;
    $r = 0;
    foreach($records as $rec)
    {
        $f = 0;
        $gdn = "gd_".$rec['recid']."_";
        foreach($fields as $fld)
        {
            $gdn2 = $gdn.$fld['fldid'];
//            echo("<BR> $gdn2");
            if($variables[$gdn2]=="Y" && $g<5)
            {
                $gd[$r]['id'] = $rec['recid'];
                $gd[$r]['f'][$f]['id'] = $fld['fldid'];
                $gd[$r]['f'][$f]['name'] = $fld['fldtxt']." (".$rec['rectxt'].")";
                $f++;
                $g++;
            }
            else
            {
//                $gd[$rec['recid']][$fld['fldid']] = "N";
            }
        }
        $r++;
    }
}
$csv = "";
$csv.= "\"".$modtitle.": Report\"\r\n";
$csv.= "\"Dashbord Ref:\",\"$ref\"\r\n";
$csv.= "\"Dashboard Name:\",\"".$dash['dashname']."\"\r\n";
$csv.= "\"Dashboard Owner:\",\"".$dashowner['tkname']." ".$dashowner['tksurname']."\"\r\n";
$csv.= "\"Capture Frequency:\",\"".$frequency[$dash['dashfrequency']]['value']."\"\r\n";
$csv.= "\"Link to SDBIP?:\",\"".YesNo($dash['dashsdbip'])."\"\r\n";
$csv.= "\"Report Comment:\",\"".decode($dash['dashcomment']);
    if(strlen($dash['dashcomment'])>0) { $csv.= chr(10)."(Last updated: ".$dash['dashcommentdate'].")"; }
    $csv.= "\"\r\n";
$csv.= "\r\n";


$table = "";
switch($tpact)
{
    case "compare":
        if($from==$to) {
            $result = drawTable($from,$to);
            $data[$to] = $result['d'];
            $table = $result['r'];
                if($variables['updatecomm']=="Y") {
                    $table.= drawComment($to,$to);
                }
        } else {
            $from = $from*1;
            $to = $to*1;
            for($t=$from;$t<=$to;$t++)
            {
                $result = drawTable($t,$t);
                $data[$t] = $result['d'];
                $table.= $result['r'];
                if($variables['updatecomm']=="Y") {
                    $table.= drawComment($t,$t);
                }
            }
        }
        break;
    case "combine":
            $result = drawTable($from,$to);
            $data[$to] = $result['d'];
            $table = $result['r'];
        break;
}

$csv.=$table;

        $filename = "../files/".$cmpcode."/dashboard_report_".date("Ymd_Hi",$today).".csv";
        $newfilename = "dashboard_report_".date("dFY_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$csv."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);

//echo($csv);
?>
