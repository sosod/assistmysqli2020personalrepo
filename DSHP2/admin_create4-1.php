<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreate4.php");
include("inc/AdminCreateProgress.php");
//print_r($variables);
$step = "4-1";
$page = array("next"=>"4-2","step"=>"1","title"=>"Variables");
    $list = getList();

$r0 = $variables['r0'];
$r1 = $variables['r1'];
$ref = $variables['ref'];

if(strlen($r0)>0 && strlen($r1)>0)
{
    $result[0] = $r0;
    $result[1] = $r1;
    $result[2] = $ref;
}

$types = array();
$sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 3 AND yn = 'Y' AND code IN ('NUM','R','PERC','DT') ORDER BY sort";
include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $types[] = $row;
    }
mysql_close($con);
$functions = getFunctions();
?>
<script type=text/javascript src="lib/AdminCreateVariables.js"></script>
<script type=text/javascript>
function fldApply(f) {
//    alert(f);
    var id = typearr[f];
    var fld;
    var targ;
    fld = "t-f"+f;
    var t = document.getElementById(fld).value;
    fld = "u-f"+f;
    var u = document.getElementById(fld).value;
    fld = "l-f"+f;
    var l = document.getElementById(fld).value;
//    alert(t+"-"+u+"-"+l);
    for(i=0;i<id.length;i++)
    {
//        alert(id[i]);
        fld = "t-"+id[i];
        document.getElementById(fld).value = t;
        fld = "u-"+id[i];
        targ = document.getElementById(fld);
        targ.value = u;
        switch(t)
        {
            case "NUM":
                targ.disabled = false;
                break;
            case "LT":
                targ.disabled = true;
                fld = "l-"+id[i];
                document.getElementById(fld).value = l;
            default:
                targ.disabled = true;
                break;
        }
        if(t != "LT")
        {
            fld = "ut-"+id[i];
            targ = document.getElementById(fld);
            targ.style.display = "inline";
            fld = "ul-"+id[i];
            targ = document.getElementById(fld);
            targ.style.display = "none";
        }
        else
        {
            fld = "ut-"+id[i];
            targ = document.getElementById(fld);
            targ.style.display = "none";
            fld = "ul-"+id[i];
            targ = document.getElementById(fld);
            targ.style.display = "inline";
        }

    }
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 4.".$page['step'].": Setup ".$page['title']."</h2>");

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(!checkIntRef($ref))
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    $vars = getVariables($ref);
    $records = getRecords($ref);
    $fields = getFields($ref);
    

echo("<h3>".$dash['dashname']." (ref: ".$ref.")</h3>");
echo("<form name=frm id=frm action=\"admin_create4-2.php\" method=post>");
echo("<input type=hidden name=step value=\"4-".$page['step']."\"><input type=hidden name=ref value=$ref ><input type=hidden name=stepact value=save>");
?>
<div style="text-align:center;">
<?php
previewVar41();
?>
<span id=recfn>
<input type=hidden name=recfnvals id=recfnv value="">
    <table cellpadding=3 cellspacing=0 >
        <tr>
            <td class="tdheader lft b-bottom-w">Perform Calculation?</th>
            <td><?php echo("<select name=r-yn id=r-yn onchange=\"useRecFn(this);\">".selectYesNo("Y")."</select><br>"); ?></td>
        </tr>
        <tr>
<?php
        //function choice
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Function:</td>");
            echo("<td><select name=r-fn id=r-fn onchange=\"changeRecFn('Y');\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 2 AND yn = 'Y' AND code <> 'SAVE' ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($functions as $row)
                {
//                    if($row['code']!="PERC")
//                    {
                        $r++;
                        echo("<option ");
                        if($r==1) { echo(" selected "); }
                        echo("value=".$row['code'].">".$row['value']."</option>");
//                    }
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo records list
        //all
            echo("<td valign=top class=\"tdheader lft b-top-w b-bottom-w\">Records to Sum/Count:</td>");
            echo("<td><select size=2 multiple name=r-rsum id=r-rsum>");
            foreach($records as $recs)
            {
                if($recs['recid']!=$recid)
                    echo("<option value=".$recs['recid'].">".$recs['rectxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Record 1:</td>");
            echo("<td><select name=r-r1 id=r-r1><option selected value=X>--- SELECT ---</option>");
            foreach($records as $recs)
            {
                if($recs['recid']!=$recid)
                    echo("<option value=".$recs['recid'].">".$recs['rectxt']."</option>");
            }
            echo("</select></td></tr><tr>");
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Record 2:</td>");
            echo("<td><select name=r-r2 id=r-r2><option selected value=X>--- SELECT ---</option>");
            foreach($records as $recs)
            {
                if($recs['recid']!=$recid)
                    echo("<option value=".$recs['recid'].">".$recs['rectxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo type
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Type:</td>");
            echo("<td><select name=r-t id=r-t onchange=\"changeRecType(this);\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 3 AND yn = 'Y' AND code IN ('NUM','R','PERC','DT') ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($types as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo unit
            echo("<td class=\"tdheader lft b-top-w\">Units:</td>");
            echo("<td><input name=r-u id=r-u type=text size=10> e.g. %, km etc.</td>");
?>
        </tr>
        <tr>
            <td colspan=2  style="text-align:center;"><input type=button value="Save function settings" onclick="saveRecFn();"> <input type=button value="Cancel function setup" onclick="cancelRecFn();"></td>
        </tr>
    </table>
</span>
<script type=text/javascript>document.getElementById('recfn').style.display = "none"; </script>




<span id=fldfn>
<input type=hidden name=fldfnvals id=fldfnv value="">
    <table cellpadding=3 cellspacing=0 >
        <tr>
            <td class="tdheader lft b-bottom-w">Perform Calculation?</th>
            <td><?php echo("<select name=f-yn id=f-yn onchange=\"useFldFn(this);\">".selectYesNo("Y")."</select><br>"); ?></td>
        </tr>
        <tr>
<?php
        //function choice
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Function:</td>");
            echo("<td><select name=f-fn id=f-fn onchange=\"changeFldFn('Y');\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 2 AND yn = 'Y' ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($functions as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo records list
        //all
            echo("<td valign=top class=\"tdheader lft b-top-w b-bottom-w\">Fields to Sum/Count:</td>");
            echo("<td><select size=2 multiple name=f-fsum id=f-fsum>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Field 1:</td>");
            echo("<td><select name=f-f1 id=f-f1><option selected value=X>--- SELECT ---</option>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td></tr><tr>");
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Field 2:</td>");
            echo("<td><select name=f-f2 id=f-f2><option selected value=X>--- SELECT ---</option>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo type
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Type:</td>");
            echo("<td><select name=f-t id=f-t onchange=\"changeFldType(this);\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 3 AND yn = 'Y' AND code IN ('NUM','R','PERC') ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($types as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo unit
            echo("<td class=\"tdheader lft b-top-w\">Units:</td>");
            echo("<td><input name=f-u id=f-u type=text size=10> e.g. %, km etc.</td>");
?>
        </tr>
        <tr>
            <td colspan=2 style="text-align:center;"><input type=button value="Save function settings" onclick="saveFldFn();"> <input type=button value="Cancel function setup" onclick="cancelFldFn();"></td>
        </tr>
    </table>
</span>
<script type=text/javascript>document.getElementById('fldfn').style.display = "none"; </script>




<span id=subfn>
<input type=hidden name=subfnvals id=subfnv value="">
    <table cellpadding=3 cellspacing=0 >
        <tr>
            <td class="tdheader lft b-bottom-w">Perform Calculation?</th>
            <td><?php echo("<select name=s-yn id=s-yn onchange=\"useSubFn(this);\">".selectYesNo("Y")."</select><br>"); ?></td>
        </tr>
        <tr>
<?php
        //function choice
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Function:</td>");
            echo("<td><select name=s-fn id=s-fn>");
            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 2 AND yn = 'Y' AND code IN ('SUM','SAVE','COUNT') ORDER BY sort";
            include("inc_db_con.php");
                $r = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Records to sum/count:</td>");
            echo("<td><select name=s-sum id=s-sum><option selected value=X>--- SELECT ---</option>");
            foreach($records as $recs)
            {
                    echo("<option value=".$recs['recid'].">".$recs['rectxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo type
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Type:</td>");
            echo("<td><select name=s-t id=s-t onchange=\"changeSubType(this);\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 3 AND yn = 'Y' AND code IN ('NUM','R','PERC','TME') ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($types as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo unit
            echo("<td class=\"tdheader lft b-top-w\">Units:</td>");
            echo("<td><input name=s-u id=s-u type=text size=10> e.g. %, km etc.</td>");
?>
        </tr>
        <tr>
            <td colspan=2 style="text-align:center;"><input type=button value="Save function settings" onclick="saveSubFn();"> <input type=button value="Cancel function setup" onclick="cancelSubFn();"></td>
        </tr>
    </table>
</span>
<script type=text/javascript>document.getElementById('subfn').style.display = "none"; </script>





<span id=subffn>
<input type=hidden name=subffnvals id=subffnv value="">
    <table cellpadding=3 cellspacing=0 >
        <tr>
            <td class="tdheader lft b-bottom-w">Perform Calculation?</th>
            <td><?php echo("<select name=sf-yn id=sf-yn onchange=\"useSubFFn(this);\">".selectYesNo("Y")."</select><br>"); ?></td>
        </tr>
        <tr>
<?php
        //function choice
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Function:</td>");
            echo("<td><select name=sf-fn id=sf-fn onchange=\"changeSubFFn('Y');\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 2 AND yn = 'Y' ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($functions as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo records list
        //all
            echo("<td valign=top class=\"tdheader lft b-top-w b-bottom-w\">Fields to Sum/Count:</td>");
            echo("<td><select size=2 multiple name=sf-fsum id=sf-fsum>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Field 1:</td>");
            echo("<td><select name=sf-f1 id=sf-f1><option selected value=X>--- SELECT ---</option>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td></tr><tr>");
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Field 2:</td>");
            echo("<td><select name=sf-f2 id=sf-f2><option selected value=X>--- SELECT ---</option>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo type
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Type:</td>");
            echo("<td><select name=sf-t id=sf-t onchange=\"changeSubFType(this);\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 3 AND yn = 'Y' AND code IN ('NUM','R','PERC') ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($types as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo unit
            echo("<td class=\"tdheader lft b-top-w\">Units:</td>");
            echo("<td><input name=sf-u id=sf-u type=text size=10> e.g. %, km etc.</td>");
?>
        </tr>
        <tr>
            <td colspan=2 style="text-align:center;"><input type=button value="Save function settings" onclick="saveSubFFn();"> <input type=button value="Cancel function setup" onclick="cancelSubFFn();"></td>
        </tr>
    </table>
</span>
<script type=text/javascript>document.getElementById('subffn').style.display = "none"; </script>




<span id=dirfn>
<input type=hidden name=dirfnvals id=dirfnv value="">
    <table cellpadding=3 cellspacing=0 >
        <tr>
            <td class="tdheader lft b-bottom-w">Perform Calculation?</th>
            <td><?php echo("<select name=d-yn id=d-yn onchange=\"useDirFn(this);\">".selectYesNo("Y")."</select><br>"); ?></td>
        </tr>
        <tr>
<?php
        //function choice
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Function:</td>");
            echo("<td><select name=d-fn id=d-fn>");
            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 2 AND yn = 'Y' AND code IN ('SUM','SAVE','COUNT') ORDER BY sort";
            include("inc_db_con.php");
                $r = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
            <td colspan=2 style="text-align:center;"><input type=button value="Save function settings" onclick="saveDirFn();"> <input type=button value="Cancel function setup" onclick="cancelDirFn();"></td>
        </tr>
    </table>
    <table class=noborder cellpadding=0 cellspacing=0 style="align: center"><tr><td class=noborder><ul><li>This function will act on the Sub-Totals of each <br>of the Sub-Directorates within the Directorate.</li>
            <li>The Sub-Total function for Sub-Directorates must be<Br>setup in order for this Total function to work.</li></ul></td></tr></table>
</span>
<script type=text/javascript>document.getElementById('dirfn').style.display = "none"; </script>




<span id=dirffn>
<input type=hidden name=dirffnvals id=dirffnv value="">
    <table cellpadding=3 cellspacing=0 >
        <tr>
            <td class="tdheader lft b-bottom-w">Perform Calculation?</th>
            <td><?php echo("<select name=df-yn id=df-yn onchange=\"useDirFFn(this);\">".selectYesNo("Y")."</select><br>"); ?></td>
        </tr>
        <tr>
<?php
        //function choice
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Function:</td>");
            echo("<td><select name=df-fn id=df-fn onchange=\"changeDirFFn('Y');\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 2 AND yn = 'Y' ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($functions as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo records list
        //all
            echo("<td valign=top class=\"tdheader lft b-top-w b-bottom-w\">Fields to Sum/Count:</td>");
            echo("<td><select size=2 multiple name=df-fsum id=df-fsum>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Field 1:</td>");
            echo("<td><select name=df-f1 id=df-f1><option selected value=X>--- SELECT ---</option>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td></tr><tr>");
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Field 2:</td>");
            echo("<td><select name=df-f2 id=df-f2><option selected value=X>--- SELECT ---</option>");
            foreach($fields as $flds)
            {
                if($flds['fldid']!=$fldid)
                    echo("<option value=".$flds['fldid'].">".$flds['fldtxt']."</option>");
            }
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo type
            echo("<td class=\"tdheader lft b-top-w b-bottom-w\">Type:</td>");
            echo("<td><select name=df-t id=df-t onchange=\"changeDirFType(this);\">");
//            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 3 AND yn = 'Y' AND code IN ('NUM','R','PERC') ORDER BY sort";
//            include("inc_db_con.php");
                $r = 0;
//                while($row = mysql_fetch_array($rs))
                foreach($types as $row)
                {
                    $r++;
                    echo("<option ");
                    if($r==1) { echo(" selected "); }
                    echo("value=".$row['code'].">".$row['value']."</option>");
                }
//            mysql_close($con);
            echo("</select></td>");
?>
        </tr>
        <tr>
<?php
    //echo unit
            echo("<td class=\"tdheader lft b-top-w\">Units:</td>");
            echo("<td><input name=df-u id=df-u type=text size=10> e.g. %, km etc.</td>");
?>
        </tr>
        <tr>
            <td colspan=2 style="text-align:center;"><input type=button value="Save function settings" onclick="saveDirFFn();"> <input type=button value="Cancel function setup" onclick="cancelDirFFn();"></td>
        </tr>
    </table>
</span>
<script type=text/javascript>document.getElementById('dirffn').style.display = "none"; </script>






<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type=submit value="Next -->"></td>
	</tr>
</table>
</form>
<?php
        $stepprogress = setProgress(4);
        $totalprogress = setProgress(0);
        displayProgress("Step 4 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<P>&nbsp;</p>
</body>
</html>
