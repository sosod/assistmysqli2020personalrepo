<?php
include("inc_head.php");
include("inc/Admin.php");


$ref = $variables['ref'];

if(!checkIntRef($ref)) { echoError(); }
$comm = $variables['dashcomment'];
$dash = getDashboard($ref);
$result = array();
if(strlen($comm)>0)
{
    $comm = code($comm);
    $sql = "UPDATE ".$dbref."_dashboard SET dashcomment = '$comm', dashcommentdate = now() WHERE dashid = $ref";
    include("inc_db_con.php");
    logAct("D_COMM",$ref,"N","Update dashboard comment from '".$dash['dashcomment']."' to '$comm'",$sql);
    $dash = "";
            $result[0]="check";
            $result[1]="Report comment updated.";
            $result[2] = $ref;
}

$dash = getDashboard($ref);
$frequency = getCaptureFreq();
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
$types = getTypes();

?>
<h1><b><?php echo($modtitle); ?>: Admin - Comment</b></h1>
<?php displayResult($result); ?>
<form name=comm action=admin_comment.php method=post><input type=hidden name=ref value=<?php echo($ref); ?>>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
</table>
<p>&nbsp;</p>
<h2>Report Comment</h2>
<p><textarea cols=75 rows=10 name=dashcomment><?php echo(decode($dash['dashcomment'])); ?>
</textarea>
<?php if($dash['dashcommentdate']!=$dash['dashadddate'] && $dash['dashcommentdate']>0) { ?>
<br>
<i>Last updated: <?php echo($dash['dashcommentdate']); ?></i><?php } ?></p>
<p>&nbsp;</p>
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td style="text-align:center"><input type=submit value="  Save Comment  "></td>
    </tr>
</table>

</form>
</body>

</html>
