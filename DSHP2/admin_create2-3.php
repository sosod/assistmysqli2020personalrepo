<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreate2.php");
include("inc/AdminCreateProgress.php");
$step = "2-3";
$page = array("next"=>"4","step"=>"3","title"=>"Records - Manual");

//result from display order change
$dr = $variables['dr'];
if(strlen($dr)>0)
{
    $result[0] = $variables['da'];
    $result[1] = $dr;
}

?>
<script type=text/javascript>
function Validate() {
    var targ = document.getElementById('nextaction').value;
    if(targ == "preview")
    {
        document.forms['frm'].action = "admin_create2-4.php";
    }
    else
    {
        document.forms['frm'].action = "admin_create2-3.php";
    }
    document.forms['frm'].submit();
}
function delRecord(ri,ref) {
    if(!isNaN(parseInt(ri)) && !isNaN(parseInt(ref)) && confirm("Are you sure you wish to delete record "+ri+"?"))
    {
        var url = "admin_create2-3.php?";
        url = url + "stepact=save";
        url = url + "&step=2-3";
        url = url + "&ref="+ref;
        url = url + "&recid="+ri;
        url = url + "&nextaction=delete";
        document.location.href = url;
    }
}
function editRecord(ri,ref) {
    if(!isNaN(parseInt(ri)) && !isNaN(parseInt(ref)))
    {
        var url = "admin_create2-3.php?";
        url = url + "stepact=save";
        url = url + "&step=2-3";
        url = url + "&ref="+ref;
        url = url + "&recid="+ri;
        url = url + "&nextaction=edit";
        document.location.href = url;
    }
}
function editSave(ref,act,ri) {
    var rn = document.getElementById('rn').value;
    var fn = document.getElementById('fn').value;
    if(!isNaN(parseInt(ri)) && !isNaN(parseInt(ref)) && rn.length>0 && fn.length==1)
    {
        var url = "admin_create2-3.php?";
        url = url + "stepact=save";
        url = url + "&step=2-3";
        url = url + "&ref="+ref;
        url = url + "&recid="+ri;
        url = url + "&rn="+escape(rn);
        url = url + "&fn="+escape(fn);
        url = url + "&nextaction="+act;
        document.location.href = url;
    }
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<?php
echo("<h2>Step 2.".$page['step'].": Setup ".$page['title']."</h2>");

if(strlen($ref)==0) { $ref = $result[2]; }
if(strlen($ref)==0 || $ref == 0)
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    $colspan=1;
    if(count($records)>0) { $colspan = 2; }
    echoDashboardTitle($ref,$dash['dashname'],$step);
echo("<form name=frm id=frm onsubmit=\"return Validate(this);\" action=\"admin_create2-3.php\" method=post>");
echo("<input type=hidden name=step value=\"2-".$page['step']."\">");
echo("<input type=hidden name=ref value=$ref >");
echo("<input type=hidden name=stepact value=save>");
echo("<input type=hidden name=oldstep value=\"".$dash['dashstatus']."\">");

if($nextaction != "edit")
{
    $records = getRecords($ref);
?>
<h4>Add New Records</h4>
<table cellpadding=3 cellspacing=0 width=500>
    <tr height=27>
        <td class="tdheader b-right-w">Record Name</td>
        <td class="tdheader b-left-w">Type</td>
    </tr>
    <?php
    for($t=1;$t<6;$t++)
    {
        ?>
    <tr>
        <td class=txt><input type=text name=rn[] maxlength=150 size=50></td>
        <td style="text-align:center"><select name=fn[]><?php echo(selectRecFn("N")); ?></select></td>
    </tr>
        <?php
    }
	$recsort = $records[count($records)-1]['recsort']+1;
	if(checkIntRef($recsort)) { $recsort = 1; }
    ?>
    <tr>
        <td colspan=2 style="text-align:center"><input type=hidden name=recsort value=<?php echo($recsort); ?>><select name=nextaction id=nextaction><option selected value=more>Add more</option><option value=preview>Go to Preview</option></select> <input type=button value="Next -->" onclick="Validate();"> <input type=button value="Display Order" onclick="document.location.href = 'admin_order.php?ref=<?php echo($ref); ?>&type=REC&src=admin_create';"></td>
    </tr>
</table>
    <?php
    if(count($records) > 0)
    {
    ?>
<h4>Existing Records</h4>
<table cellpadding=3 cellspacing=0 width=500>
    <tr height=27>
        <td class="tdheader b-right-w b-bottom-w">Ref</td>
        <td class="tdheader b-right-w b-left-w">Record Name</td>
        <td class="tdheader b-right-w b-left-w">Type</td>
        <td class="tdheader b-left-w">&nbsp;</td>
    </tr>
    <?php
        foreach($records as $row)
        {
            $rn = $row['rectxt'];
            $ri = $row['recid'];
            $rf = $row['recfn'];
            if($rf=="Y") {
                $style = "style=\"font-weight: bold;\"";
            } else {
                if($rf == "H") {
                    $style = "style=\"text-decoration: underline;background-color: #ddffdd;\"";
                } else {
                    $style = "style=\"\"";
                }
            }
            ?>
    <?php include("inc_tr.php"); ?>
        <td class="tdheader b-bottom-w b-top-w" width=40><?php echo($ri); ?></td>
        <td class=txt <?php echo($style); ?>><?php echo($rn); ?></td>
        <td width=60 style="text-align:center" <?php echo($style); ?>><?php echo(displayRecFn($rf)); ?></td>
        <td style="text-align:center" width=60 <?php echo($style); ?>><input type=button value=Edit onclick="editRecord(<?php echo $ri; ?>,<?php echo $ref; ?>);"> <input type=button value=Del onclick="delRecord(<?php echo $ri; ?>,<?php echo $ref; ?>);"></td>
    </tr>
            <?php
        }
        ?>
</table>
        <?php
    }
} else { //if nextaction!=edit
    $recid = $variables['recid'];
    $record = getRecord($recid);
    ?>
<h4>Edit Record</h4>
<table cellpadding=3 cellspacing=0 width=500>
    <tr height=27>
        <td width=170 class="tdheaderl b-bottom-w">Record Reference:</td>
        <td width=330><?php echo($recid); ?></td>
    </tr>
    <tr height=27>
        <td colspan=2 class="tdheaderl b-top-w">Original Details:</td>
    </tr>
    <tr height=27>
        <td class="level2">Record Name:</td>
        <td><?php echo($record['rectxt']); ?></td>
    </tr>
    <tr height=27>
        <td class="level2">Type:</td>
        <td><?php echo(displayRecFn($record['recfn'])); ?></td>
    </tr>
    <tr height=27>
        <td colspan=2 class="tdheaderl b-top-w">New Details:</td>
    </tr>
    <tr height=27>
        <td class="level2">Record Name:</td>
        <td><?php echo("<input type=text id=rn size=50 maxlength=100 value=\"".$record['rectxt']."\">"); ?></td>
    </tr>
    <tr height=27>
        <td class="level2">Type:</td>
        <td><select id=fn><?php echo(selectRecFn($record['recfn'])); ?></select></td>
    </tr>
    <tr height=27>
        <td colspan=2 align=center><input type=button value="Save Changes" onclick="editSave(<?php echo("$ref,'editsave',$recid"); ?>);"> <input type=button value=Delete onclick="editSave(<?php echo("$ref,'cancel',$recid"); ?>);"></td>
    </tr>
</table>
    <?php
}
        $stepprogress = setProgress(2);
        $totalprogress = setProgress(0);
        displayProgress("Step 2 Process",$stepprogress,$totalprogress);
} //endif ref error ?>
</div>
<p>&nbsp;</p>
</body>
</html>
