<?php
include("inc_ignite.php");
include("inc_errorlog.php");

$cateid = $_GET['c'];

$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cateid;
include("inc_db_con.php");
$cate = mysql_fetch_array($rs);
mysql_close();

?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var minfile = me.minfile.value;
    var mday = me.mday.value;
    var mmon = me.mmon.value;
    var myear = me.myear.value;
    var mintoc = me.mintoc.value;
    var valid8 = "false";
    
    if(mday.length == 0 || mmon.length == 0 || myear.length == 0)
    {
        alert("Please indicate the date of the document.");
    }
    else
    {
        if(minfile.length == 0)
        {
            alert("Please select a file for upload.");
        }
        else
        {
//            var ftype = minfile.substr(minfile.lastIndexOf(".")+1,3);
//            if(ftype.toLowerCase() != "pdf")
//            {
//                alert("Invalid file.  Only PDF documents may be uploaded.");
//            }
//            else
//            {
                valid8 = "true";
//            }
        }
    }
    if(valid8 == "true")
    {
        if(mintoc.length == 0)
        {
            if(confirm("You have not entered any table of contents.\n\nAre you sure you wish to continue?") == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
    return false;
}

function showMin(mfn) {
    var url = "http://assist.ignite4u.co.za"+mfn;
    //alert(url);
    newwin = window.open(url,"","dependent=0 ,toolbar=1,location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=700,height=500")
}

function delMin(m) {
    if(confirm("Are you sure you want to delete document "+m+"?")==true)
    {
        //alert("delete");
        document.location.href = "update_delete.php?m="+m;
    }
    else
    {
        //alert("aborting");
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Update</h1>
<h2 class=fc><?php echo($cate['catetitle']); ?></b></h2>
<form name=update action="update_process.php" method="post" enctype="multipart/form-data" lang=jscript onsubmit="return Validate(this);">
<table border=1 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border-style: solid" width=500>
    <tr>
        <td class=tdgeneral><b>Category:</b>&nbsp;</td>
        <td class=tdgeneral><?php echo($cate['catetitle']); ?><input type=hidden name=mincateid value=<?php echo($cate['cateid']);?>></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>Date:</b></td>
        <td class=tdgeneral><input type=text name=mday size=3><select name=mmon><?php
            $month = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
            echo("<option selected value=1>".$month[1]."</option>");
            for($m=2;$m<13;$m++)
            {
                echo("<option value=".$m.">".$month[$m]."</option>");
            }
            ?></select><input type=text size=5 name=myear value=<?php echo(date("Y")); ?>></td>
    </tr>
    <tr>
        <td class=tdgeneral valign=top><b>Contents:</b>&nbsp;</td>
        <td class=tdgeneral><textarea name=mintoc rows=3 cols=50></textarea></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>File:</b></td>
        <td class=tdgeneral><input type=file name=minfile></td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=2><input type=submit value=Update> <input type=reset></td>
    </tr>
</table>
</form>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_content m WHERE doccateid = ".$cateid." AND docyn = 'Y' ORDER BY docdate DESC";
include("inc_db_con.php");
$m = mysql_num_rows($rs);
if($m > 0)
{
    echo("<hr class=fc>");
    echo("<table border=1 cellpadding=5 cellspacing=0 style=\"border-collapse: collapse; border-style: solid\" width=500>");
    echo("    <tr>");
    echo("        <td class=tdheader width=30>Ref</td>");
    echo("        <td class=tdheader width=150>Date</td>");
    echo("        <td class=tdheader width=270>Contents</td>");
    echo("        <td class=tdheader width=50>&nbsp;</td>");
    echo("    </tr>");
    while($row = mysql_fetch_array($rs))
    {
        $floc = "/files/".$cmpcode."/".$row['docfilename'];
        $floc2 = "../files/".$cmpcode."/".$row['docfilename'];
        if(file_exists($floc2))
        {
            echo("    <tr>");
            echo("        <td class=tdheader align=center valign=top>".$row['docid']."</td>");
            echo("        <td class=tdgeneral valign=top>".date("d F Y",$row['docdate'])."&nbsp;</td>");
            echo("        <td class=tdgeneral valign=top>");
            if(strlen($row['doctoc']) > 0)
            {
                $mintoc = explode(chr(10),$row['doctoc']);
                $mt = 0;
                foreach($mintoc as $toc)
                {
                    if(strlen($toc) > 0)
                    {
                        if($mt>0)
                        {
                            echo("<br>");
                        }
                        else
                        {
                            echo("<p style=\"margin: 0 0 0 0;\">");
                        }
                        echo($toc);
                        $mt++;
                    }
                }
            }
            else
            {
                echo("&nbsp;");
            }
            echo("</td>");
            echo("        <td class=tdgeneral valign=top align=center><input type=button value=View onclick=\"showMin('".$floc."')\"><br><input type=button value=Delete onclick=\"delMin('".$row['docid']."')\"></td>");
            echo("    </tr>");
        }
    }
    echo("</table>");
}
mysql_close();
?>

</body>

</html>
