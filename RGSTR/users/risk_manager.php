<?php
$scripts = array( 'risk_manager.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<div>
<form method="post" name="risk-manager-form" id="risk-manager-form"> 
<div id="risk_manager_message"></div>
<table width="100%" border="1" id="risk_manager_table">
  <tr>
    <th>Ref</th>
    <th>User</th>
    <th>Action</th>
  </tr>
  <tr>
    <td>#</td>
    <td>
    	<select id="risk_manager_users" name="risk_manager_users">
        	<option>--user--</option>
        </select>
    </td>
    <td>
    	<input type="submit" name="update_risk_manager" id="update_risk_manager" value="Update" />
    </td>
  </tr>
</table>
</form>
</div>