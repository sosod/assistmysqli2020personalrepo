<?php //error_reporting(-1);
//	$scripts = array( 'glossary.js');
$scripts = array();
	$styles = array( 'colorpicker.css' );
	
	$folder = "glossary";
	$page = "index";
	
	//$page_title = "Glossary";
	require_once("../inc/header.php");
	

$nameObj = new Naming();

$ireName = $nameObj->getAName("inherent_risk_exposure");
$rrName = $nameObj->getAName("residual_risk_exposure");
$feName = $nameObj->getAName("financial_exposure");
$ceName = $nameObj->getAName("percieved_control_effective");
$rtName = $nameObj->getAName("risk_type");
$rlName = $nameObj->getAName("risk_level");
$rsName = $nameObj->getAName("risk_status");
$asName = $nameObj->getAName("action_status");
$rcName = $nameObj->getAName("risk_category");
$roName = $nameObj->getAName("risk_owner");	
$impactName = $nameObj->getAName("impact");	
$likeName = $nameObj->getAName("likelihood");	
	
	
function displayList($objects,$fields,$status,$color,$rating,$shortcode,$name="name") {
	echo "
	<table width=700px>
		<tr>";
	foreach($fields as $key=>$f) {
		echo "<th>".$f."</th>";
	}
	echo "
		</tr>";
	foreach($objects as $o) {
		echo "
		<tr>
		";
		foreach($fields as $key=>$f) {
			switch($key) {
				case $status:
					echo "<td width='50px' style='width: 50px' class=center>";
					if($o[$key]==1) {
						echo "Active";
					} else {
						echo "Inactive";
					}
					break;
				case $color:
					echo "<td width=50px><div style='background-color: #".$o[$key]."; width: 45px; margin: 0 auto;'>&nbsp;</div>";
					break;
				case $rating:
				case $shortcode:
					echo "<td width='75px' style='width: 75px; text-align: center;'>";
					echo $o[$key];
					break;
				default:
					echo "<td ".($name==$key ? "width='100px' " : "").">".$o[$key];
					break;
			}
			echo "</td>";
		}
		echo "
		</tr>";
	}
	echo "
	</table>";
}
	
	
	$gloss = new GLOSSARY();
	$glossary = $gloss->getGlossary();
	//ASSIST_HELPER::arrPrint($gloss->getGlossary());
	
	
?>
<form method="post">
<div id="glossary_message"></div>
<table id="glossary_table2" width="50%">
	<tr>
    	<th>Category</th>
    	<th>Terminology</th>
    	<th>Explanation</th>                          
    </tr>
<?php 
foreach($glossary as $g) {
	echo "
	<tr>
		<td>".$g['category']."</td>
		<td>".$g['terminology']."</td>
		<td>".$g['explanation']."</td>
	</tr>
	";
}
?>
</table>
</form>
<h3>List Items</h3>

<h4><?php echo $rtName; ?></h4>
<?php
$object = new RiskType();
$objects = $object->getRiskTypes();
$fields = array(
	'shortcode'=>"Short Code",
	'name'=>"Name",
	'description'=>"Description",
	'active'=>"Status"
);
$status = "active";
$color = "";
$rating = "";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode);
?>
<h4><?php echo $rlName; ?></h4>
<?php
$object = new RiskLevel();
$objects = $object->getRiskLevels();
$fields = array(
	'shortcode'=>"Short Code",
	'name'=>"Name",
	'description'=>"Description",
	'status'=>"Status"
);
$status = "status";
$color = "";
$rating = "";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode);
?>
<h4><?php echo $rcName; ?></h4>
<?php
$object = new RiskCategory();
$objects = $object->getCategory();
$fields = array(
	'shortcode'=>"Short Code",
	'risktype'=>"Associated ".$rtName,
	'name'=>"Name",
	'descr'=>"Description",
	'active'=>"Status"
);
$status = "active";
$color = "";
$rating = "";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode);
?>
<h4><?php echo $feName; ?></h4>
<?php
$object = new FinancialExposure();
$objects = $object->getFinancialExposure();
foreach($objects as $key => $o) {
	$o['rating'] = $o['exp_from']." to ".$o['exp_to'];
	$objects[$key] = $o;
}
$fields = array(
	'rating'=>"Rating",
	'name'=>"Name",
	'definition'=>"Description",
	'color'=>"Color",
	'status'=>"Status"
);
$status = "status";
$color = "color";
$rating = "rating";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode);
?>
<h4><?php echo $impactName; ?></h4>
<?php
$object = new Impact();
$objects = $object->getImpact();
foreach($objects as $key => $o) {
	$o['rating'] = $o['rating_from']." to ".$o['rating_to'];
	$objects[$key] = $o;
}
$fields = array(
	'rating'=>"Rating",
	'assessment'=>"Name",
	'definition'=>"Description",
	'color'=>"Color",
	'active'=>"Status"
);
$status = "active";
$color = "color";
$rating = "rating";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode,"assessment");
?>
<h4><?php echo $likeName; ?></h4>
<?php
$object = new Likelihood();
$objects = $object->getLikelihood();
foreach($objects as $key => $o) {
	$o['rating'] = $o['rating_from']." to ".$o['rating_to'];
	$objects[$key] = $o;
}
$fields = array(
	'rating'=>"Rating",
	'assessment'=>"Name",
	'definition'=>"Description",
	'probability'=>"Probability",
	'color'=>"Color",
	'active'=>"Status"
);
$status = "active";
$color = "color";
$rating = "rating";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode,"assessment");
?>
<h4><?php echo $ireName; ?></h4>
<?php
$object = new InherentRisk();
$objects = $object->getIR();

foreach($objects as $key => $o) {
	$o['rating'] = $o['rating_from']." to ".$o['rating_to'];
	$objects[$key] = $o;
}
$fields = array(
	'rating'=>"Rating",
	'magnitude'=>"Magnitude",
	'response'=>"Response",
	'color'=>"Color",
	'active'=>"Status"
);
$status = "active";
$color = "color";
$rating = "rating";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode,"magnitude");
?>
<h4><?php echo $ceName; ?></h4>
<?php
$object = new ControlEffectiveness();
$objects = $object->getControlEffectiveness();

$fields = array(
	'shortcode'=>"Short Code",
	'effectiveness'=>"Name",
	'qualification_criteria'=>"Qualification Criteria",
	'rating'=>"Rating",
	'color'=>"Color",
	'active'=>"Status"
);
$status = "active";
$color = "color";
$rating = "rating";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode,"effectiveness");
?>
<h4><?php echo $rrName; ?></h4>
<?php
$object = new ResidualRisk();
$objects = $object->getRR();

foreach($objects as $key => $o) {
	$o['rating'] = $o['rating_from']." to ".$o['rating_to'];
	$objects[$key] = $o;
}
$fields = array(
	'rating'=>"Rating",
	'magnitude'=>"Magnitude",
	'response'=>"Response",
	'color'=>"Color",
	'active'=>"Status"
);
$status = "active";
$color = "color";
$rating = "rating";
$shortcode = "shortcode";
displayList($objects,$fields,$status,$color,$rating,$shortcode,"magnitude");
?>
<script type=text/javascript>
$(function() {
	$("th").css("text-align","center");
});
</script>