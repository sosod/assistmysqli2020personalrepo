<?php
$scripts = array( 'jquery.ui.action1.js','menu.js','actions.js'  );
$styles = array( 'colorpicker.css' );

$folder = "admin";
$page = "admin_update";
$menuT = array(
	1 => array('id'=> "update_risk",'url'=>"admin_update.php",'active'=> "",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "update_actions",'url'=>"update_actions.php",'active'=> "Y",'display'=>"|ACTIONNAME|"),
);

$page_title = "Update |ACTIONNAME|";

require_once("../inc/header.php");
?>
<script language="javascript">
    $(function(){
        $("table#edit_action_table").find("th").css({"text-algn":"left"})
        $("#actions").action1({updateAction:true, page:"edit_actions", section:"admin",objectName:window.risk_object_name, actionName:window.action_object_name});
    });
</script>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="actions"></div>
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
