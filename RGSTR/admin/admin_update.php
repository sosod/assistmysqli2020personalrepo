<?php
$scripts = array( 'jquery.ui.riskaction.js', 'menu.js',  );
$styles = array( 'colorpicker.css' );
//$title = "Admin >> Update >> Risks";

$folder = "admin";
$page = "admin_update";
$menuT = array(
	1 => array('id'=> "update_risk",'url'=>"admin_update.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "update_actions",'url'=>"update_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);

$page_title = "Update |OBJECTNAME|";

require_once("../inc/header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<script language="javascript">
    $(function(){
        $("#risk").riskaction({updateRisk:true, updateActions:true , section:"admin", view:"viewAll", page:'admin_update', showActions: false,objectName:window.risk_object_name, actionName:window.action_object_name});
    });
</script>
<div id="risk"></div>

