<?php
$scripts = array( 'editrisk.js','menu.js','ajaxfileupload.js'  );
$styles = array( 'colorpicker.css' );
$folder = "admin";
$page = "admin_update";
$redirect = "admin_update.php";
$menuT = array(
	1 => array('id'=> "update_risk",'url'=>"admin_update.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "update_actions",'url'=>"update_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);

$page_title = "Update |OBJECTNAME|";


require_once("../inc/header.php");

include_once("../common/update_risk.php");

?>