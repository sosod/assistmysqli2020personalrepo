<?php
$scripts = array( 'action.js' ,'functions.js', 'ajaxfileupload.js' );
$styles = array( 'colorpicker.css' );
$folder = "admin";
$page = "admin_edit";
$menuT = array(
	1 => array('id'=> "edit_risk",'url'=>"admin_edit.php",'active'=> "",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "edit_actions",'url'=>"edit_actions.php",'active'=> "Y",'display'=>"|ACTIONNAME|"),
);

$page_title = "Edit |ACTIONNAME|";

require_once("../inc/header.php");

require_once("controller.php");

$act 		= new RiskAction("", "", "", "", "", "", "","", "");
$action 	= $act -> getRiskAction( $_REQUEST['id'] );

$stat 		= new RiskStatus("", "", "");
$statuses 	= $stat -> getStatus();
$latestUp 	= $act -> getLatestActionUpdates( $_REQUEST['id'] );

$stastusId  = (empty($latestUp) ? $action['status'] : $latestUp['name']);
$uaccess 	= new UserAccess( "", "", "", "", "", "", "", "", "");
$users 		= $uaccess -> getUsers();

?>
<script>
$(function(){
	$("table#edit_action_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form method="post" name="edit-action-table" id="edit-action-table">
   <table align="left" id="edit_action_table" border="1" class=form>
   	<tr>
    	<th>Ref:</th>
        <td><?php echo RiskAction::REFTAG.$_REQUEST['id']; ?></td>
    </tr>   	<tr>
    	<th>Action:</th>
        <td>
        	<textarea class="smallbox" name="action" id="action"><?php echo $action['action']; ?></textarea>
        </td>
    </tr>
   	<tr>
    	<th>Action Owner:</th>
        <td>
        	<select name="action_owner" id="action_owner">
            	<option>--action owner--</option>
                <?php
                foreach($users as $user ){
				?>
               	<option value="<?php echo $user['tkid']; ?>" 
				<?php  if($user['tkid']==$action['action_owner']){ ?>
                selected="selected" <?php } ?>>
					<?php echo $user['tkname']." ".$user['tksurname']; ?>
                </option>
                <?php
				}
				?>
            </select>
        </td>
    </tr>
   	<!--<tr>
    	<th>Status:</th>
        <td>
        	<select name="action_status" id="action_status">
            	<option>--action status--</option>
                <?php
                foreach($statuses as $status ){
				?>
               	<option value="<?php echo $status['id']; ?>" <?php  if($status['name']==$stastusId){ ?>
                	selected="selected"
                 <?php } ?>>
					<?php echo $status['name']; ?>
                </option>
                <?php
				}
				?>
            </select>
        </td>
    </tr> -->
    <tr>
    	<th>Deliverable:</th>
        <td><textarea class="mainbox" id="deliverable" name="deliverable"><?php echo $action['deliverable']; ?></textarea></td>
    </tr>
   	<tr>
    	<th>Time Scale:</th>
        <td><input type="text" id="timescale" name="timescale" value="<?php echo $action['timescale']; ?>" /></td>
    </tr>
   	<tr>
    	<th>Deadline:</th>
        <td><input type="text" id="deadline" name="deadline" value="<?php echo $action['deadline']; ?>" class="datepicker" readonly="readonly"/></td>
    </tr>
       	<tr>
    	<th>Remind On:</th>
        <td><input type="text" id="remindon" name="remindon" value="<?php echo $action['remindon']; ?>" class="futuredate"  readonly="readonly"/></td>
    </tr>
<!--    <tr>
    	<th>Attachments</th>
    	<td>
    	<span id="file_loading">
	    	<?php 
	    		if(!empty($attachments))
	    		{
	    		   $attachments = @unserialize( $action['attachment'] );
	    		   if(isset($attachments) && !empty($attachments))
	    		   {
    				echo "<ul id='attachment_list'>";
    				 foreach( $attachments as $index => $filename)
    				  {
					$ext = substr( $filename , strpos($filename, ".")+1);
					$file = $index.".".$ext;
					if(file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$file))
					{						
						echo "<li id='parent_".$index."' class='attachmentlist'><span><a href='download.php?folder=action&file=".$file."&modref=".$_SESSION['modref']."&new=".$filename."&content=".$ext."&company=".$_SESSION['cc']."'>".$filename."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='".$filename."' id='".$index."' style='color:red' class='remove removeattach'>Remove</a></span></li>";
					}    				  	
				//echo "<li id='li_".$index."'><a href='#'>".$filename."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='".$index."'>Remove</a></li>";    						
    				  }
    				echo "</ul>";
	    		   }
	    		}    		
	    	?>
 		</span><br />
        <input type="file" id="action_attachments" name="action_attachments" onchange="ajaxFileUpload(this)" />
    	</td>            
    </tr> -->
    <tr>
		<th></th>
    	<td>
		   <input type="submit" name="edit_action" id="edit_action" value="Save Changes" class="isubmit" />
		   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   		   <input type="submit" name="cancel_action" id="cancel_action" value="Cancel"   /><!--
		   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		   <input type="submit" name="delete_action" id="delete_action" value="Delete" class="idelete" />     -->   
		   <input type="hidden" name="actionid" id="actionid" value="<?php echo $_REQUEST['id'] ?>"  />
		   <input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>" class="logid"  />
	        </td>
    </tr>
   <tr>
   	<td  align="left" class="noborder">
    	<?php $me->displayGoBack("",""); ?>
    </td>
   	<td class="noborder" align="right">
    	<?php $admire_helper->displayAuditLogLink( "action_edit", false); ?>
    </td>    
   </tr>
   </table>
</form>
<div id="actioneditlog" style="clear:both;"></div>
</div>

