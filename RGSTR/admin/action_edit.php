<?php
$scripts = array( 'jquery.ui.action.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "Action Edit";
require_once("../inc/header.php");
$risk 		 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$rsk 		 =  $risk -> getRisk( $_REQUEST['id'] );
$riskStatus  = $risk -> getRiskUpdate( $rsk['id'] );
$statusId 	 = ((empty($riskStatus) || !isset($riskStatus )) ? $rsk['statusId'] : $riskStatus['status'] );
$type 		 = new RiskType( "", "", "", "" );
$types		 = $type-> getType();	
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$imp 		 = new Impact( "", "", "", "", "");
$impacts 	 = $imp -> getImpact() ;
$like 		 = new Likelihood( "", "", "", "", "","");
$likelihoods = $like -> getLikelihood() ;
$ihr		 = new ControlEffectiveness( "", "", "", "", "");
$controls    = $ihr -> getControlEffectiveness();
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUserAccess() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<script type="text/javascript">
	$(function(){
		$("table#updaterisktable").find("th").css({"text-align":"left"})	
	})
</script>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder" width="40%">
		<table align="left" id="updaterisktable" width="100%">
		 <tr>
		        <th><?php nameFields('risk_item','Risk Item'); ?>:</th>
		        <td>#<?php echo $rsk['id'] ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
		        <td><?php echo $rsk['type']; ?></td>
		      </tr>
		      <tr>
		            <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
		            <td><?php echo $rsk['level']; ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('risk_category','Risk category'); ?>:</th>
		        <td><?php echo $rsk['category']; ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
		        <td><?php echo $rsk['description'] ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
		        <td><?php echo $rsk['background'] ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('impact','Impact'); ?>:</th>
		        <td><?php echo $rsk['impact'] ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
		        <td><span style="background-color:<?php echo "#".$rsk['impact_color']; ?>"><?php echo $rsk['impact_rating']; ?></span></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('likelihood','Likelihood'); ?>:</th>
		        <td><?php echo $rsk['likelihood'] ?></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
		        <td><span style="background-color:<?php echo "#".$rsk['like_color']; ?>"><?php echo $rsk['likelihood_rating']; ?></span></td>
		      </tr>
		      <tr>
		        <th><?php nameFields('control_rating','Control Effectveness Rating'); ?>:</th>
		        <td><span style="background-color:<?php echo "#".$rsk['effect_color']; ?>"><?php echo $rsk['control_effectiveness_rating']; ?></span></td>
		      </tr>
		      <tr>
		            <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
		            <td><?php echo $rsk['risk_financial_exposure']; ?></td>
		      </tr>
		      <tr>
		            <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
		            <td> <?php echo $rsk['actual_financial_exposure']; ?></td>
		      </tr>
		      <tr>
		            <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
		            <td> <?php echo $rsk['kpi_ref']; ?> </td>
		      </tr>
		      <tr>
		        <th><?php nameFields('ris_owner','Responsibility of risk'); ?>:</th>
		        <td><?php echo $rsk['risk_owner']; ?></td>
		      </tr>
		      <tr>
		        <td align="left" class="noborder">
		             <?php $me->displayGoBack("",""); ?>
		        </td>
		        <td align="right" class="noborder"></td>
		      </tr>
		   </table>    
	</td>
    <td class="noborder">
		<script language="javascript">
			$(function(){
				$("#actions").action({risk_id:<?php echo $_GET['id']; ?>, edit:true, loggedInUser:$("#userid").val(), section:"manage"});
			});
		</script>
		<div id="actions"></div>
		<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />    
	</td>
  </tr>
</table>
