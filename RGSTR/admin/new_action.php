<?php
$page = "action_new";
$folder = "risk";

$scripts = array( 'jquery.ui.action.js', 'add_action.js','menu.js','functions.js', 'ajaxfileupload.js');
$styles = array( 'colorpicker.css' );
$page_title = "Add Action";
require_once("../inc/header.php");

//$me->arrPrint($_REQUEST);
?>
<script>
$(function(){
		$("table#addaction_table").find("th").css({"text-align":"left"})	
});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table id="risk_action_table" class="noborder">
	<tr>
    	<td width="50%" valign="top" class="noborder">
    		<table class="noborder">
    			<tr>
    				<td colspan="2"><h4>Risk Details</h4></td>
    			</tr>
				<tr id="goback">
					<td class="noborder" align="left" width="150">
						<?php $me->displayGoBack("",""); ?>
					</td>
					<td class="noborder"></td>
				</tr>				
    			 <tr>
    				<td colspan="2" class="noborder">
    				<script language="javascript">
						$(function(){
							//get all the actions for this risk	
						  $("#actions").action({risk_id:<?php echo $_REQUEST['id']; ?>, extras:false,section:"admin"});
						})
					</script>
    				
    				<div id="actions"></div></td>
    			</tr>
    		</table>
        </td>
     <td width="50%" valign="top" class="noborder">
      <table align="left" id="addaction_table" width="100%" style="border-collapse:collapse;">
      	  <tr><td colspan="2"><h4>Add New Action</h4></td></tr>
          <tr>
            <th>Risk:</th>
            <td><?php echo $_REQUEST['id']; ?></td>
          </tr>
          <tr>
            <th>Action:</th>
            <td><textarea name="action" id="action"></textarea></td>
          </tr>
          <tr>
            <th>Deliverable:</th>
            <td><textarea name="deliverable" id="deliverable"></textarea></td>
          </tr>
          <tr>
            <th>Action Owner:</th>
            <td>
                <select id="action_owner" name="action_owner">
                
                </select>
            </td>
          </tr>
          <tr>
            <th>Time Scale:</th>
            <td><input type="text" name="timescale" id="timescale" /></td>
          </tr>
          <tr>
            <th>Deadline:</th>
            <td><input type="text" name="deadline" id="deadline" class="datepicker" readonly="readonly" /></td>
          </tr>
          <tr>
            <th>Remind On:</th>
            <td><input type="text" name="remindon" id="remindon" class="datepicker" readonly="readonly" /></td>
          </tr>
          <tr>
            <th>Attachments:</th>
            <td>
                <span id="loading"></span>
                <span id="file_loading"></span>
                <input type="file" id="action_attachments" name="action_attachments" onchange="ajaxFileUpload(this)"/>
            </td>
          </tr>
          <tr>
            <th></th>
            <td>
            <input type="hidden" id="risk_id" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" />
            <input type="submit" id="save_action" value="Save Action" name="save_action" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" id="cancel_action" value="Cancel Action" name="cancel_action" />
            </td>
          </tr>
        </table>						       
      </td>
  </tr>
</table>
	