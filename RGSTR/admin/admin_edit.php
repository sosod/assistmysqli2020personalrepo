<?php
$scripts = array( 'jquery.ui.riskaction.js', 'menu.js',  );
$styles = array( 'colorpicker.css' );

$folder = "admin";
$page = "admin_edit";
$menuT = array(
	1 => array('id'=> "edit_risk",'url'=>"admin_edit.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "edit_actions",'url'=>"edit_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);

$page_title = "Edit |OBJECTNAME|";


require_once("../inc/header.php");

if(isset($_REQUEST['r']) && is_array($_REQUEST)) {
	$me->displayResult($_REQUEST['r']);
}

?>
<script language="javascript">
    $(function(){
        $("#risk").riskaction({editRisk:true, editActions:false, section:"admin", view:"viewAll", page:'admin_edit', showActions:false,objectName:window.risk_object_name, actionName:window.action_object_name});
    });
</script>
<div id="risk"></div>

