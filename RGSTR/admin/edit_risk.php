<?php

$scripts = array();
$styles = array();

$folder = "admin";
$page = "admin_edit";

$redirect = "admin_edit.php";

$menuT = array(
	1 => array('id'=> "edit_risk",'url'=>"admin_edit.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "edit_actions",'url'=>"edit_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);

$page_title = "Edit |OBJECTNAME|";

include("../inc/header.php");
//echo "end header";
//echo "<br />start_include";
include("../common/edit_risk.php");
//echo "<p>end include";

?>