<?php
/**	
	* @package 	: Control Effectiveness
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist 
**/
class ControlEffectiveness extends DBConnect
{
	protected $shortcode;
	protected $effectiveness;
	protected $qualification_criteria;
	protected $rating;
	protected $color;

	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
	
	function __construct( $shortcode="", $effectiveness="", $qualification_criteria="", $rating="",  $color="FFFFFF" )
	{
		$this -> shortcode				 = trim($shortcode);
		$this -> effectiveness 			 = trim($effectiveness);
		$this -> qualification_criteria  = trim($qualification_criteria);
		$this -> rating  				 = trim($rating);
		$this -> color 		 			 = trim($color);						
		parent::__construct();
	}
	
	function saveControlEffectiveness()
	{
		$insert_data = array( 
						"shortcode" 			  => $this -> shortcode,
						"effectiveness"   		  => $this -> effectiveness,
						"qualification_criteria"  => $this -> qualification_criteria,
						"rating"  				  => $this -> rating,
						"color"       			  => $this -> color,
						);
		$response = $this -> insert( "control_effectiveness" , $insert_data );
		echo $response;//$this -> insertedId();
	}		
	
	function updateControlEffectiveness( $id )
	{
		$insert_data = array( 
						"shortcode" 			  => $this -> shortcode,
						"effectiveness"   		  => $this -> effectiveness,
						"qualification_criteria"  => $this -> qualification_criteria,
						"rating"  				  => $this -> rating,
						"color"       			  => $this -> color,
						);
        $logsObj = new Logs();
        $data    = $this->getAControlEffectiveness( $id );
        $logsObj -> setParameters( $insert_data, $data, "control_effectiveness");						
		$response = $this -> update( "control_effectiveness" , $insert_data, "id=$id" );
		return $response;	
	}			
				
	function getControlEffectiveness()
	{
		$response = $this -> get( "SELECT *, effectiveness as name FROM ".$_SESSION['dbref']."_control_effectiveness WHERE active <> 2" );
		return $response;
	}

    public function getActiveControlEffectiveness()
    {
        $response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_control_effectiveness WHERE 1 AND active = 1" );
        return $response;
    }
	function getActiveForEdit()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_control_effectiveness");// WHERE active = 1 " );
		$data = array(
			'active'=>array(),
			'inactive'=>array(),
		);
		foreach($response as $key => $r) {
			$response[$key]['name'] = $r['effectiveness'];
			if($r['active']==1) {
				$data['active'][$key] = $response[$key];
			} else {
				$data['inactive'][$key] = $response[$key];
			}
		}
		
		return $data;
	}
	

			
	function getAControlEffectiveness( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_control_effectiveness WHERE id = $id" );
		return $response;
	}
	
	function getControlRating( $id )
	{
		$response = $this -> get( "SELECT rating
								   FROM ".$_SESSION['dbref']."_control_effectiveness 
								   WHERE id = '".$id."' " );
		foreach($response as $row ) {
			$rating 	= $row['rating'];
		} 
		
		echo json_encode( array( "rating" => $rating ) );
	}	
	

	function updateControlEffectivenessStatus( $id, $status )
	{
		$updatedata = array(
						'active' => $status
		);
        $logsObj = new Logs();
        $data    = $this->getAControlEffectiveness( $id );
        $logsObj -> setParameters( $updatedata, $data, "control_effectiveness");								
		$response = $this -> update( 'control_effectiveness', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateControlEffectiveness( $id )
	{
		$update_data = array(
						'active' => "1"
		);
        $logsObj = new Logs();
        $data    = $this->getAControlEffectiveness( $id );
        $logsObj -> setParameters( $update_data, $data, "control_effectiveness");								
		$response = $this -> update( 'control_effectiveness', $update_data, "id=$id" );
		echo $response;
	}
	
	
	

	
	

	
	
	
	
	
	
	
	
	function addItem($rate, $name,$def,$color) {
		$insert_data = array( 
						"rating"		=> $rate,
						"effectiveness"			=> $name,
						"qualification_criteria"	=> $def,
						"color"			=> $color,
						"active" 		  => InherentRisk::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "control_effectiveness" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $rate, $name, $def, $color )
	{
		$insertdata = array(
						"rating" 			 => $rate,
						"effectiveness"		 => $name,
						"qualification_criteria" 		 => $def,
						"color" 		 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAControlEffectiveness( $id );
		$var = array(
			'id'=>$id,
			'rating'=>$rate,
			'effectiveness'=>$name,
			'qualification_criteria'=>$def,
			'color'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "control_effectiveness");
		$response = $this -> update( "control_effectiveness", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAControlEffectiveness( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "control_effectiveness");
		$response = $this -> update( 'control_effectiveness', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
		
	
	
	
	
		
		
		
	
	
	function getReportList() {
		$sql = "SELECT DISTINCT L.id, L.effectiveness as name FROM ".$_SESSION['dbref']."_control_effectiveness L 
				INNER JOIN ".$_SESSION['dbref']."_risk_register R
				ON R.percieved_control_effectiveness = L.id
				WHERE L.active <> 2 
				ORDER BY rating, effectiveness"; 
		$data = $this -> get( $sql );
		return $data;
	}
	
	
}
?>