<?php
/**
	* Action Status
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 ,Ignite Assist Risk Module
**/
class ActionStatus extends DBConnect
{
	
	protected $risk_status;
	
	protected $client_term;
	
	protected $color;
	
	/*  Status:
			0 = Inactive
			1 = Active
			2 = Deleted
	*/
	const INACTIVE = 0;
	const ACTIVE = 1;
	const DELETED = 2;
	const DEFAULTS = 4;

	
	function __construct($risk_status="", $client_term="", $color="FFFFFF")
	{
		$this -> risk_status = trim($risk_status);
		$this -> client_term = trim($client_term);
		$this -> color 		 = trim($color);
		parent::__construct();
	}
	
	function saveStatus() {
		$insert_data = array( 
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color,
						"insertuser"    	 => $_SESSION['tid'],						 
						);
		$response = $this -> insert( "action_status" , $insert_data );
		echo $response;//$this -> insertedId();
	}
	function addStatus($name,$client_term,$color) {
		$this -> risk_status = trim($name);
		$this -> client_term = trim($client_term);
		$this -> color 		 = trim($color);
		$insert_data = array( 
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "action_status" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	function getStatus()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE status <> 2" );
		echo json_encode( $response );		
	}	
	
	function getActionStatuses()
	{
		return $this->getStatuses();
	}


	function getStatuses()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE status <> 2" );
		return $response;		
	}
	function getStatusesWithCount()
	{
		$count = $this-> get("SELECT A.status as id, COUNT(A.id) as rc FROM ".$_SESSION['dbref']."_actions A GROUP BY A.status",true); 
		$status = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE status <> 2",true );
		$response = array();
		foreach($status as $s) {
			$s['count'] = isset($count[$s['id']]) ? $count[$s['id']]['rc'] : 0;
			$response[] = $s;
		}
		return $response;		
	}	
	function getAStatus( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_action_status WHERE id = '".$id."'" );
		if(isset($response['client_terminology']) && strlen($response['client_terminology'])>0) {
			$response['value'] = $response['client_terminology'];
		} else {
			$response['value'] = $response['name'];
		}
		return $response;		
	}

    function getOrderedStatuses()
    {
        $statuses = $this -> getStatuses();
        $tmp      = array();
        $s_list   = array();
        foreach($statuses as $s_index => $s_val)
        {
            if($s_val['id'] == 3)
            {
                $tmp[$s_val['id']]    = $s_val;
            } else {
                $s_list[$s_val['id']] = $s_val;
            }
        }
        $status_list = array_merge($s_list, $tmp);
        return $status_list;
    }

	function updateStatus( $id )
	{
		$insertdata = array(
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color,
						"insertuser"    	 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $insertdata, $data, "action_status");
		$response = $this -> update( "action_status", $insertdata , "id=$id");
		return $response;
	}

	function editStatus( $id, $name, $client, $color )
	{
		$insertdata = array(
						"name" 				 => $name,
						"client_terminology" => $client,
						"color" 			 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
		$data['default_terminology'] = $data['name'];
		$data['colour'] = $data['color'];
		unset($data['color']);
		unset($data['name']);
		$var = array(
			'id'=>$id,
			'default_terminology'=>$name,
			'client_terminology'=>$client,
			'colour'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "action_status");
		$response = $this -> update( "action_status", $insertdata , "id=$id");
		return $response;
	}
		
	
	function setStatus($id,$new_status) {
        $data    = $this->getAStatus( $id );
		$stat = $data['status'];
		$status = $new_status + ( ($stat & ACTIONSTATUS::DEFAULTS)==ACTIONSTATUS::DEFAULTS ? ACTIONSTATUS::DEFAULTS : 0);
		$updatedata = array(
						'status' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $var['status'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "action_status");
		$response = $this -> update( 'action_status', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	function changeActionStatus( $id, $status)
	{
		$updatedata = array(
						"status" 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "action_status");
		$response = $this -> update( 'action_status', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateStatus( $id )
	{
		$update_data = array(
					"status"		 => "1",
					"insertuser"     => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "action_status");
		$response = $this -> update( 'action_status', $update_data, "id=$id" );
		echo $response;
	}


	
	function getReportList() {
		return $this->getStatuses();
	}

	
}
?>
