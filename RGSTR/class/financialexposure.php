<?php

class FinancialExposure extends DBConnect
{
	protected $from;
	
	protected $to;
	
	protected $name;
	
	protected $definition;
	
	protected $color;
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
		
	function __construct( $from="", $to="", $name="", $definition="",  $color ="FFFFFF")
	{
		$this -> from 		 = trim($from);
		$this -> to 		 = trim($to);
		$this -> name  		 = trim($name);
		$this -> definition  = trim($definition);
		$this -> color 		 = trim($color);						
		parent::__construct();
	}
			
	function saveFinancialExposure()
	{
		$insert_data = array( 
						"exp_from" 		  => $this -> from,
						"exp_to"   		  => $this -> to,
						"name"  	  => $this -> name,
						"definition"  => $this -> definition,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "financial_exposure" , $insert_data );
		echo $response;//$this -> insertedId();
	}		
		
	function updateFinancialExposure( $id )
	{
		$insert_data = array( 
						"exp_from" 		  => $this -> from,
						"exp_to"   		  => $this -> to,
						"name"  	  => $this -> name,
						"definition"  => $this -> definition,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getAFinancialExposure( $id );
        $logsObj -> setParameters( $_POST, $data, "financial_exposure");
		$response = $this -> update( "financial_exposure" , $insert_data, "id=$id" );
		return $response;		
	}	
			
	function getFinancialExposure()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_financial_exposure WHERE  status <> 2 " );
		return $response;
	}
	
	function getActiveFinancialExposure()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_financial_exposure WHERE status & 1 = 1 " );
		return $response;
	}
	
	
	function getAFinancialExposure( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_financial_exposure WHERE id = '".$id."'" );
		return $response;
	}
	
	
	function getFinancialExposureRating( $id )
	{
		$response = $this -> get( "SELECT from, to FROM ".$_SESSION['dbref']."_financial_exposure WHERE id = '".$id."' " );
		$diff = $to = $from = 0;
		foreach($response as $row ) {
			$diff 	= $row['to'] - $row['from'];
			$to 	= $row['to'];
			$from 	= $row['from'];
		} 
		
		echo json_encode( array( "from" => $from, "to" => $to , "diff" => $diff) );
	}	

	function updateFinancialExposureStatus( $id , $status)
	{
		$updatedata = array(
						'status'	 =>  $status,
						"insertuser" => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAFinancialExposure( $id );
        $logsObj -> setParameters( $_POST, $data, "financial_exposure");
		$response = $this -> update( 'financial_exposure', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateFinancialExposure( $id )
	{
		$update_data = array(
						'status' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAFinancialExposure( $id );
        $logsObj -> setParameters( $_POST, $data, "financial_exposure");
		$response = $this -> update( 'financial_exposure', $update_data, "id=$id" );
		echo $response;
	}

	
	
	
	
	
	
	
	
	function addItem($from, $to, $name,$def,$color) {
		$insert_data = array( 
						"exp_from"		=> $from,
						"exp_to"		=> $to,
						"name"			=> $name,
						"definition"	=> $def,
						"color"			=> $color,
						"status" 		  => FinancialExposure::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "financial_exposure" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $from, $to, $name, $def, $color )
	{
		$insertdata = array(
						"exp_from" 			 => $from,
						"exp_to" 			 => $to,
						"name" 				 => $name,
						"definition" 		 => $def,
						"color" 		 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAFinancialExposure( $id );
		$var = array(
			'id'=>$id,
			'exp_from'=>$from,
			'exp_to'=>$to,
			'name'=>$name,
			'definition'=>$def,
			'color'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "financial_exposure");
		$response = $this -> update( "financial_exposure", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'status' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAFinancialExposure( $id );
        $var['status'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "financial_exposure");
		$response = $this -> update( 'financial_exposure', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
		
	
	
	
	
		
	
	function getReportList()
	{
		$response = $this -> get( "SELECT FE.id, FE.name FROM ".$_SESSION['dbref']."_financial_exposure FE INNER JOIN ".$_SESSION['dbref']."_risk_register Q ON Q.financial_exposure = FE.id WHERE  FE.status & 2 <> 2 " );
		return $response;
	}
		
	
}


?>