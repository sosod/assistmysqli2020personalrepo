<?php
$scripts = array( 'risk_types.js','menu.js' , 'functions.js');
$styles = array();
$page_title = "Set Up Risk Statuses";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="risk_type_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="risk-type-form" name="risk-type-form">
			<table border="1" id="risk_type_table">
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th>Risk Type</th>
			    <th>Description</th>
			    <th></th>
			    <th>Status</th>
			    <th></th>    
			  </tr>
			  <tr>
			    <td>#</td>
			    <td><input type="text" name="risktype_shortcode" id="risktype_shortcode" /></td>
			    <td><input type="text" name="risktype" id="risktype" /></td>
			    <td><textarea id="risktype_descri" name="risktype_descri"></textarea></td>
			    <td><input type="submit" name="add_risktype" id="add_risktype" value="Add" /></td>
			    <td></td>
			    <td></td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "types_logs", true); ?></td>
	</tr>
</table>
</div>

