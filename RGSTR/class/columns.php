<?php
/**
	* @package 	: columns
	
**/
class Columns extends DBConnect{

	protected $id;
	
	protected $name;
	
	protected $client_terminology;
	
	protected $ordernumber;
	
	protected $active;
	
	function __construct(){
		parent::__construct();
	}
		
	function getActiveColumns() {
		
		$response = $this -> get( "SELECT id, name, client_terminology, ordernumber AS position, active FROM ".$_SESSION['dbref']."_header_names
								    WHERE active = 1 AND type = 'risk' ORDER BY ordernumber" );
		return $response;	
	}
		
	function getInActiveColumns() {		
		$response = $this -> get( "SELECT id, name, client_terminology, ordernumber AS position FROM ".$_SESSION['dbref']."_header_names 
									WHERE active = 0 AND type = 'risk' ORDER BY ordernumber" );
		return $response;	
	}

	function saveColumnsOrder( $activeColumns, $inactiveColumns) {	
		$activeArray = array();
		$resInAct	 = 0;
		$resAct		 = 0;		
		foreach( $activeColumns as $key => $id ) {
			$resAct = $this -> update( "header_names", array( 
															"active"	 	=> 1 ,
															 "insertuser"   => $_SESSION['tid'],
															) ,"id = $id"); 
		}
		
		foreach( $inactiveColumns as $key => $id ) {
			$resInAct = $this -> update( "header_names", array( 
																"active" 		=> 0,
																"insertuser"    => $_SESSION['tid'], 
																) ,"id = $id"); 
		}
		
	}

}
?>