<?php
/**
	* Risk Statuses
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist , Risk Assiat Module

**/  
class RiskStatus 
	extends DBConnect
//public function __construct() {}
{

	protected $risk_status;
	
	protected $client_term;
	
	protected $color;
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;

	function __construct( $risk_status="", $client_term="", $color="")
	{
		$this -> risk_status = trim($risk_status);
		$this -> client_term = trim($client_term);
		$this -> color 		 = trim($color);
		parent::__construct();
	}

	function saveStatus()
	{
		$insert_data = array( 
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "status" , $insert_data );
		echo $response;//$this -> insertedId();
	}
	
	function addStatus($name,$client_term,$color) {
		$this -> risk_status = trim($name);
		$this -> client_term = trim($client_term);
		$this -> color 		 = trim($color);
		$insert_data = array( 
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "status" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	function getStatus()
	{
		$response = $this -> get( "SELECT id, IF(LENGTH(client_terminology)>0, client_terminology, name) as name, client_terminology, color, active, defaults FROM ".$_SESSION['dbref']."_status WHERE active <> 2 " );
		return $response;		
	}
	
	function getActiveStatus() {
		$response = $this -> get( "SELECT id, IF(LENGTH(client_terminology)>0, client_terminology, name) as name, color, active, defaults FROM ".$_SESSION['dbref']."_status WHERE active = 1 " );
		return $response;		
	}

	function getStatusWithCount() {
		$count = $this-> get("SELECT R.status as id, COUNT(R.id) as rc FROM ".$_SESSION['dbref']."_risk_register R GROUP BY R.status",true); 
		$status = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_status WHERE active <> 2 ",true );
		$response = array();
		foreach($status as $s) {
			$s['count'] = isset($count[$s['id']]) ? $count[$s['id']]['rc'] : 0;
			$response[] = $s;
		}
		return $response;		
	}

	function getAStatus( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_status WHERE id = '".$id."'" );
		return $response;		
	}

	function updateStatus( $id )
	{
		$insertdata = array(
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "status");
		$response = $this -> update( "status", $insertdata , "id=$id");
		return $response;
	}

	function editStatus( $id, $name, $client, $color )
	{
		$insertdata = array(
						"name" 				 => $name,
						"client_terminology" => $client,
						"color" 			 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
		$data['default_terminology'] = $data['name'];
		$data['colour'] = $data['color'];
		unset($data['color']);
		unset($data['name']);
		$var = array(
			'id'=>$id,
			'default_terminology'=>$name,
			'client_terminology'=>$client,
			'colour'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "status");
		$response = $this -> update( "status", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "status");
		$response = $this -> update( 'status', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	function changeStatus( $id , $status)
	{
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $_POST['active'] = $status;
        unset($_POST['status']);
        $logsObj -> setParameters( $_POST, $data, "status");
		$response = $this -> update( 'status', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateStatus( $id )
	{
		$update_data = array(
						'active'			 => "1",
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $_POST['active'] = 1;
        unset($_POST['status']);
        $logsObj -> setParameters( $_POST, $data, "status");
		$response = $this -> update( 'status', $update_data, "id=$id" );
		echo $response;
	}

	function getReportList() {
		return $this->getStatus();
	}	

}
?>