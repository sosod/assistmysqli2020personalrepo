<?php
$scripts = array( 'action_status.js','menu.js','jscolor.js', 'functions.js');
$styles = array();
$page_title = "Set Up Action Statuses";
require_once("../inc/header.php");
?>
<div>
<div id="actionstatus_message"></div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
</div>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form id="form-action-status" name="form-action-status">
			<table border="1" id="action_status_table">
			  <tr>
			    <th>Ref</th>
			    <th>Action Status</th>
			    <th>&lt;Client&gt; Terminology</th>
			    <th>Colour</th>
			    <th></th>
			    <th>Status</th>
			    <th></th>    
			  </tr>
			  <tr>
			    <td>#</td>
			    <td><input type="text" name="actionstatus" id="actionstatus" /></td>
			    <td><input type="text" name="action_client_term" id="action_client_term" /></td>
			    <td><input type="text" name="action_color" id="action_color" class="color"  value="e2ddcf"/></td>
			    <td><input type="submit" name="add_actionstatus" id="add_actionstatus" value="Add" /></td>
				<td></td>
			    <td>
			    </td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "action_status_logs" , true); ?></td>
	</tr>
</table>
