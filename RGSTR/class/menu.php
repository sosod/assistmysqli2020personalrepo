<?php
/**
	* Database connection 
	* @author 	 : admire<azinamo@gmail.com>
	* @copyright : 2011 Ignite Assit risk module
	* @filename  : menu.php
**/
class Menu extends DBConnect{
	
	function __construct(){
		parent::__construct();
	}
	
	function getMenu(){
		$query = mysqli_query( "SELECT id , parent_id , name , client_name , folder FROM  ".$_SESSION['dbref']."_menu " );
		$menudata = array();
		 while( $row = mysqli_fetch_assoc( $query ) ) {
		 	$menudata[$row['id']] = array( $row['id'] , $row['parent_id'] ,($row['client_name'] == "" ? $row['name'] : $row['client_name'] ) ,$row['folder'] ); 
		 }
		 return $menudata;
	}
	
	function getAllMenu()
	{
		$x = $this->get("SELECT 0 as id, MAX(id) as max FROM ".$_SESSION['dbref']."_menu");
		//$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_menu WHERE parent_id < ".($x[0]['max']+1) );
		$response = $this->get("SELECT c . * , IF(LENGTH(p.client_name)=0, p.name, p.client_name) as parent_name 
				FROM  `".$_SESSION['dbref']."_menu` c
				LEFT JOIN ".$_SESSION['dbref']."_menu p 
				  ON p.id = c.parent_id
				WHERE c.parent_id < ".($x[0]['max']+1)." AND c.folder <> ''
				ORDER BY p.parent_id,c.parent_id, c.id",false);
		//$results  = array(0=>array('id'=>0,'parent_id'=>0,'name'=>$x[0]['max'])); 
		$results = array();
		foreach( $response as $index => $val)
		{	
			if( strstr($val['name'], "_" )) {
				$name = ucwords( str_replace("_", " ", $val['name'] ) );
				$results[]  = array("id" => $val['id'], "parent_id" => $val['parent_id'],"parent_name"=>$val['parent_name'], "name" => $name, "client_name" => $val['client_name'], "description" => $val['description'], "folder" => $val['folder'], "viewby" => $val['viewby'], "status" => $val['status'], "insertdate" => $val['insertdate'] );				
			} else {
				$results[]  = $val;
			}				
		}
		return $results;
	}

	
	function updateMenuName( $old_ref , $client_value, $id ){
		$insert_data = array(
			'client_name' => $client_value,
			'insertuser'  => $_SESSION['tid'],
		);
		$where = "id = '".$old_ref."'";
        $logsObj = new Logs();
        $data    = $this->getAMenu( $old_ref );
        $_POST['id'] = $data['id'];
        $_POST['client_name'] = $client_value;
        unset($_POST['value']);
        unset($_POST['name']);
        $logsObj -> setParameters( $_POST, $data, "menu");
		$response =  $this -> update( 'menu', $insert_data, "id={$id}");
		echo  $response;
	}
	
	function getAMenu( $folder ){
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_menu WHERE folder = '".$folder."'" );
        return $response;
	}
	
	function getSubMenu($folder_name)
    {
        $results = array();
        if($folder_name)
        {
            $parent  = $this->getRow("SELECT id FROM ".$_SESSION['dbref']."_menu WHERE folder = '".$folder_name."' LIMIT 1");
            if($parent)
            {
                $query 	 = "SELECT id, name, client_name, folder FROM ".$_SESSION['dbref']."_menu
				   WHERE parent_id = ".$parent['id']." ";
                $results =  $this -> get( $query );
            }
        }
		return $results;
	}
	
	function getMenuName($folder) {
		$row = $this->getAMenu($folder);
		return ($row['client_name'] == "" ? $row['name'] : $row['client_name'] );
	}
	
}
?>