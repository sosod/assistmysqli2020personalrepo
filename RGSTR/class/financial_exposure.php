<?php
$scripts = array( 'financialexposure.js','menu.js', 'jscolor.js', 'functions.js' );
$styles = array();
$page_title = "Set Up Risk Impacts";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="financial_exposure_message"></div>
<table class="noborder">	
	<tr>
		<td class="noborder" colspan="2">
			<form id="financial_exposure-form" name="financial_exposure-form">
			<table border="1" id="financial_exposure_table">
			  <tr>
			    <th width="18">Ref</th>
			    <th width="100">Rating</th>
			    <th width="149">Finacial Exposure</th>
			    <th width="161">Definition</th>
			    <th width="144">Color assigned to financial exposure</th>
			    <th width="79"></th>
			    <th width="63">Status</th>    
			    <th width="8"></th>
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			   		<input type="text" name="from" id="from" size="2" /> 
			   	to
			   		 <input type="text" name="to" id="to" size="2" />
			    </td>
			    <td><input type="text" name="name" id="name" /></td>
			    <td><textarea id="definition" name="definition"></textarea></td>
			    <td><input type="text" name="color" id="color" class="color" value="e2ddcf" /></td>
			    <td><input type="submit" name="add" id="add" value="Add" /></td>
			    <td></td>
			    <td></td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "financial_exposure_logs" , true); ?></td>
	</tr>
</table>
</div>

