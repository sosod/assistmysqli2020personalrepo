<?php
/**
	* @package 	: Risk Action
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 ignite Assist
**/
class RiskAction extends DBConnect
{
	/**
		@int
	**/
	protected $id;
	/**
		@int
	**/
	protected $risk_id;
	/**
		@text
	**/
	protected $action;
	/**
		@int
	**/
	protected $action_owner;
	/**
		@text
	**/
	protected $deliverable;
	/**
		@text
	**/
	protected $timescale;
	/**
		@var
	**/
	protected $deadline;
	/**
		@var
	**/
	protected $progress;
	/**
		@var
	**/
	protected $attachment;
	/**
		@var
	**/
	protected $status;		
	/**
		@int
	**/
	protected $action_status; 
	/**
		@varchar
	**/
	
	protected $remindon;
	
	protected $_errors = array();
	
	const REQUEST_APPROVAL	= 1;
	const DECLINE_APPROVAL	= 2;
	const ACTION_APPROVED	= 4;
	const ACTION_AWAITING   = 8;
    const REFTAG            = 'RA';

	function __construct( $risk_id=0, $action="", $action_owner="", $deliverable="", $timescale="", $deadline="", $remindon="", $progress="", $status=""  )
	{
		$this -> risk_id 		= $risk_id;
		$this -> action  		= $action;
		$this -> action_owner 	= $action_owner;
		$this -> deliverable 	= $deliverable;
		$this -> timescale 		= $timescale;
		$this -> deadline 		= $deadline;
		$this -> remindon 		= $remindon;
		$this -> progress 		= $progress;
		$this -> status			= $status;
		$this -> attachment 	= (isset($_SESSION['uploads']['action']) ? serialize( $_SESSION['uploads']['action'] ) : "");
		parent::__construct();
	}

    /**
    Creates a new risk update
     **/

    function _checkValidity( $deadline, $remindon)
    {
        if( strtotime($remindon) > strtotime($deadline))
        {
            return FALSE;
        } else {
            return TRUE;
        }
    }

	function saveRiskAction()
	{
        $riskObj = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
        $risk    = $riskObj -> getARisk( $this -> risk_id );
        if( $this-> _checkValidity($this->deadline, $this -> remindon))
        {
            $insert_data = array(
                'risk_id' 		=> $this -> risk_id,
                'action'  		=> $this -> action,
                'action_owner' 	=> $this -> action_owner,
                'deliverable' 	=> $this -> deliverable,
                'timescale' 	=> $this -> timescale,
                'deadline' 		=> $this -> deadline,
                'remindon' 		=> $this -> remindon,
                'progress' 		=> $this -> progress,
                'status' 		=> $this -> status,
                "attachment"	=> $this -> attachment,
                'insertuser'	=> $_SESSION['tid'],
	            'action_status' => 0,
            );
            $response = $this -> insert( 'actions', $insert_data );
            if(isset($_SESSION['uploads']['action']))
            {
                unset($_SESSION['uploads']['action']);
            }
            return $response;//$this -> insertedId();
        } else {
            return  "The remind date cannot be after the deadline";
        }
	}

    /**
    Edit risk action
     **/
    function editRiskAction($action_id, $update_data, $updates)
    {
        $res  = 0;
        if(!empty($update_data))
        {
            $res  = $this -> update('actions', $update_data, "id='".$action_id."'");
        }
        if(!empty($updates) && $res>0)
        {
            $mar = $this -> insert('action_edit', $updates);
            $res  +=  $mar;//$this -> insertedId();
        }
        return $res;
    }

    /*
    Update risk action
    */
    function updateAction($action_id, $update_data, $updates)
    {
        $res  = 0;
        if(!empty($update_data))
        {
            $res  = $this -> update('actions', $update_data, "id='".$action_id."'");
        }
        if(!empty($updates))
        {
            $mar = $this -> insert('actions_update', $updates);
            $res  +=  $mar;//$this -> insertedId();
        }
        return $res;
    }

    function getActionDetail($action_id)
    {
        $result = $this -> getRow("SELECT A.id, A.id AS ref, A.action, A.deliverable, A.timescale, A.deadline, A.remindon,A.action_on,
                                   A.progress, A.action_owner AS owner, A.status, RAS.name as actionstatus, A.action_status,
                                   CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner, TK.tkemail, A.attachment, A.risk_id
                                   FROM ".$_SESSION['dbref']."_actions A
                                   LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RAS.id = A.status
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.action_owner
                                   WHERE A.id = '".$action_id."'
                                  ");
        return $result;
    }

    function getRiskActionDetail($action_id)
    {
        $result = $this -> getRow("SELECT A.id, A.id AS ref, A.action, A.deliverable, A.timescale, A.deadline, A.remindon,A.action_on,
                                   A.progress, A.action_owner AS owner, A.status, RAS.name as actionstatus, A.action_status,
                                   CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner, TK.tkemail, A.attachment, A.risk_id, R.description as risk_description, R.sub_id as risk_sub_id
                                   FROM ".$_SESSION['dbref']."_actions A
                                   INNER JOIN ".$_SESSION['dbref']."_risk_register R
								   ON R.id = A.risk_id
                                   LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RAS.id = A.status
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.action_owner
                                   WHERE A.id = '".$action_id."'
                                  ");
        return $result;
    }
    /*
        Fetch risk actions
    */
    function fetchAll($option_sql = "")
    {
        $response = $this -> get("SELECT DISTINCT(A.id), A.id AS ref, A.action AS query_action, A.deliverable, A.timescale, A.deadline,
								  A.progress, A.remindon, A.action_status, RAS.name AS statusname,
								  A.action_owner AS owner,	CONCAT(TK.tkname, ' ', TK.tksurname) AS action_owner,
								  DP.value AS department, A.risk_id, QR.sub_id, A.id as action_ref
								  FROM ".$_SESSION['dbref']."_actions A
								  LEFT OUTER JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid = A.action_owner
								  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON A.status = RAS.id
								  LEFT JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								  INNER JOIN ".$_SESSION['dbref']."_risk_register QR ON QR.id = A.risk_id AND QR.active = 1
								  WHERE 1 AND A.active = 1
								  $option_sql
						         ");

        foreach($response as $ra_id => $ra_data){
           if(!is_numeric($ra_data['owner'])){
               $response[$ra_id]['action_owner'] = "[Unspecified]";
           }
        }

        return $response;
    }

    function getTotalActions($option_sql = "")
    {
        $response = $this -> getRow("SELECT COUNT(DISTINCT(A.id)) AS total
								     FROM ".$_SESSION['dbref']."_actions A
								     LEFT OUTER JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid = A.action_owner
								     LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON A.status = RAS.id
								     LEFT JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								     INNER JOIN ".$_SESSION['dbref']."_risk_register QR ON QR.id = A.risk_id
								     WHERE 1 AND A.active = 1
								     $option_sql
						         ");
        return $response['total'];
    }


    /**
    Sets the view options for each used based on the user access settings
    @param , userAccess array , pageRequest page requesting the view
    @return string, sql string
     **/
    function _setViewAccess($user_access, $options = array(), $risks = array())
    {
        $viewType        = (isset($options['view']) ? $options['view'] : "viewMyn");
        $view_access_sql = "";
        //$view_access_sql  .= " AND DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB'";
        if(!empty($risks))
        {
            $view_access_sql  .= " AND A.risk_id IN('".implode('\',\'', $risks)."')";
        }
        //$view_access_sql = " AND (DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB' ";
        if(!empty($user_access))
        {
            $sub_sql = "";
            $page    = (isset($options['page']) ? $options['page'] : "");
            if($page == "edit_actions")
            {
                $page = "edit";
            } elseif($page == "update_actions"){
                $page = "update";
            }
            if(isset($user_access[$page]))
            {
                foreach($user_access[$page] as $index => $val)
                {
                    $sub_sql .= " QR.sub_id = ".$val." OR";
                }
                if($page == "edit")
                {
                    $view_access_sql .= " AND (".rtrim($sub_sql, "OR").") ";
                } else {
                    if(!empty($sub_sql))
                    {
                        $view_access_sql .= " OR (".rtrim($sub_sql, "OR").") ";
                    }
                }
            }
        }

        return $view_access_sql;
    }

    private function getRiskWhereAdmin($user_risk_ref)
    {
        $riskObj = new Risk();
        $option_sql  = " AND DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB'";
        //$option_sql  .= " AND subdirid IN(".$user_risk_ref.")";
        $risks        = $riskObj->fetchAll($option_sql);
        $risk_list    = array();
        foreach($risks as $index => $risk)
        {
           $risk_list[] = $risk['id'];
        }
      return $risk_list;
    }

    function getActions($options = array())
    {
        $optionSql = "";
        if(isset($options['id']) && !empty($options['id']))
        {
            $optionSql = " AND A.risk_id = '".$options['id']."' ";
        }
        $userAccessObj  = new UserAccess( "", "", "", "", "", "", "", "", "" );
        //$userAccess     = $userAccessObj-> getUser( $_SESSION['tid'] );

        $user_risk_ref  = $userAccessObj -> getUserRefDirectorate();
        $risks          = $this->getRiskWhereAdmin($user_risk_ref);
        //$adminObj  = new Administrator($_SESSION['tid']);
        $user_access   = array(); //$adminObj->getAdminAccess();
        $user_sql     = "";
        if(isset($options['page']))
        {
            if($options['section'] != "admin")
            {
                if($options['page'] == "edit_actions")
                {

                    $user_sql = $this -> _setViewAccess($user_access, $options, $risks);

                    if(!empty($user_sql))
                    {
                        $optionSql .= $user_sql;
                    } else {
                        /*
                         get nothing since the edit under manage is done to actions
                         where you are the responsible owner, that is sub_id is in useraccess[edit] array
                        */
                        $optionSql .= " AND A.id = 0 ";
                    }
                }
                if($options['page'] == "update_actions")
                {
                    //$user_sql   = $this -> _setViewAccess($useraccess, $options);
                    $optionSql .= " AND A.action_status & ".RiskAction::ACTION_AWAITING." <> ".RiskAction::ACTION_AWAITING." ";
                    $optionSql .= " AND A.action_status & ".RiskAction::ACTION_APPROVED." <> ".RiskAction::ACTION_APPROVED." ";
                    $optionSql .= " AND (A.action_owner = '".$_SESSION['tid']."' ".$user_sql.")";
                }
            }
        }
        if(isset($options['page']) && $options['page'] !== "view")
        {
            //$optionSql .= " AND A.action_status & ".RiskAction::AWAITING_APPROVAL." <> ".RiskAction::AWAITING_APPROVAL." ";
            //$optionSql .= " AND A.action_status & ".RiskAction::ACTION_APPROVED." <> ".RiskAction::ACTION_APPROVED." ";
        }
        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
            $optionSql .= " AND QR.financial_year = '".$options['financial_year']."' ";
        }
        if(isset($options['action_owner']) && !empty($options['action_owner']))
        {
            $optionSql .= " AND A.action_owner = '".$options['action_owner']."' ";
        }
        if(isset($options['action_id']) && !empty($options['action_id']))
        {
            $optionSql .= " AND A.id = '".$options['action_id']."' ";
        }


        $total   = $this -> getTotalActions($optionSql);
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $optionSql .= " LIMIT ".(int)$options['start'].",".$options['limit'];
        }
        $actions = $this -> fetchAll($optionSql);
        $data    = $this -> sortActions($actions, $total, $options, $risks);
        return $data;
    }

    function sortActions($actions, $total = 0, $options = array(), $risks = array())
    {
        $columnObj           = new ActionColumns();
        $columns             = $columnObj -> getHeaderList();
        $data['total']       = $total;
        $data['headers']     = array();
        $data['actions']     = array();
        $data['avgProgress'] = 0;
        $data['progress']    = 0;
        $data['isOwner']     = array();
        $data['actionStatus']= array();
        $adminObj            = null; //new Administrator($_SESSION['tid']);
        $user_access         = array(); //$adminObj -> getAdminAccess();

        foreach($actions as $index => $action)
        {
            $data['progress']                   += $action['progress'];
            $data['actionStatus'][$action['id']] = $action['action_status'];
            if($action['owner'] == $_SESSION['tid'])
            {
                $data['isOwner'][$action['id']]  = TRUE;
            } else {
                if($options['page'] == "edit" || $options['page'] == "edit_actions")
                {
                    if(isset($user_access['edit'][$action['sub_id']]) || in_array($action['risk_id'], $risks))
                    {
                        $data['isOwner'][$action['id']]  = TRUE;
                    } else {
                        $data['isOwner'][$action['id']]  = FALSE;
                    }
                } elseif($options['page'] == "update" || $options['page'] == "update_actions") {
                    if(isset($user_access['update'][$action['sub_id']]))
                    {
                        $data['isOwner'][$action['id']]  = TRUE;
                    } else {
                        $data['isOwner'][$action['id']]  = FALSE;
                    }
                } else {
                    $data['isOwner'][$action['id']]  = FALSE;
                }

            }
            foreach($columns as $field => $val)
            {
                if(array_key_exists($field, $action))
                {
                    if($field == "action_status")
                    {
                        $data['actions'][$action['id']][$field] = $action['statusname'];
                        $data['headers'][$field]                = $val;
                    } elseif($field == "progress"){
                        $data['actions'][$action['id']][$field] = ASSIST_HELPER::format_percent($action['progress']);
                        $data['headers'][$field]                = $val;
                    } elseif($field == "ref"){
                        $data['actions'][$action['id']][$field] = RiskAction::REFTAG."".$action[$field];
                        $data['headers'][$field]                = $val;
                    } else {
                        if(Util::isTextTooLong($action[$field]))
                        {
                            $text = Util::cutString($action[$field], $field."_".$action['id']);
                            $data['actions'][$action['id']][$field] = $text;
                            $data['headers'][$field]                = $val;
                        } else {
                            $data['actions'][$action['id']][$field] = $action[$field];
                            $data['headers'][$field]                = $val;
                        }
                    }
                }
            }
        }
        if($total != 0)
        {
            $data['avgProgress']  = ASSIST_HELPER::format_percent(($data['progress'] / $total));
        }

        if(empty($data['headers']))
        {
            $data['headers'] = $columns;
        }

        if(empty($data['actions']))
        {
            $msg = "";
            if(isset($options['action_id']) && !empty($options['action_id']))
            {
                $msg = "There are no Actions that matched the entered criteria";
            } else if((isset($options['financial_year']) && !empty($options['financial_year'])) &&
                ($options['page'] == "update_actions" || $options['page'] == "action_update"))
            {
                $msg = "You do not have any outstanding Actions to update for the selected criteria";
            } else if( ((isset($options['financial_year']) && !empty($options['financial_year'])) ||
                    (isset($options['id']) && !empty($options['id'])) ||
                    (isset($options['action_owner']) && !empty($options['action_owner']))) &&
                ($options['page'] == "edit_actions" || $options['page'] == "action_edit")) {
                $msg = "There are no Actions to edit for the selected criteria";
            } else {
                if(isset($options['page']))
                {
                    if($options['page'] == "edit_actions" || $options['page'] == "action_edit")
                    {
                        $msg = "You do not have access to edit any Actions";
                    } elseif($options['page'] == "update_actions" || $options['page'] == "action_update") {
                        $msg = "You do not have any outstanding Actions to update";
                    }
                } else {
                    $msg = "There are no Actions";
                }
            }
            $data['action_message'] = $msg;
        }
        $data['columns'] = count($data['headers']);
        $data['total']       = $total;
        return $data;
    }


    public function getAwaitingActions($options = array())
    {
        $option_sql = '';
        $option_sql = " AND A.status & ".RiskAction::ACTION_AWAITING." = ".RiskAction::ACTION_AWAITING." AND A.progress = 100";
        $total      = $this->getTotalActions($option_sql);
        $actions    = $this->fetchAll($option_sql);
        $actions    = $this->sortActions($actions, $total, $options);
        return $actions;
    }

    public function getActionsApproved($options = array())
    {
        $option_sql = '';
        $option_sql = " AND A.status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED."  AND A.progress = 100";
        $total      = $this->getTotalActions($option_sql);
        $actions    = $this->fetchAll($option_sql);
        $actions    = $this->sortActions($actions, $total, $options);
        return $actions;
    }


    /**
		Creates a new risk update
	**/
	function newRiskActionUpdate($id , $description, $approval, $attachment, $changes)
	{
		$insert_data = array(
						'action_id' 		=> $id,
						'remindon' 			=> $this -> remindon ,
						'progress' 			=> $this -> progress,
						'status' 			=> $this -> status,
						'description' 		=> $description,
						'request_approval'	=> ($approval == "on" ? "1" : "0"),
						'attachment'		=> ((isset($attachment) && !empty($attachment))  ? serialize($attachment) : ""),
						'insertuser'	    => $_SESSION['tid'],
						'changes'			=> $changes,						
		);
		$updatedata =  array(
						  	"progress" => $this->progress,
						  	"status"	 => $this->status,
						  	"remindon" => $this->remindon
						 );
		if( $this->progress == "100" || $this->status == "3")
		{
		  if($approval == "on"){
		  	$updatedata['action_status'] = RiskAction::ACTION_AWAITING + RiskAction::REQUEST_APPROVAL;
		  } else {
		  	$updatedata['action_status'] = RiskAction::ACTION_AWAITING ;
		  }
		}
		$response2 = $this -> update( 'actions', $updatedata, "id = $id" );
		$response = $this -> insert( 'actions_update', $insert_data );											 
		return $response;//$this->insertedId();
	}


	function getRiskActions($start, $limit) {
		
		$response = $this -> get("SELECT 
								  RA.id ,
								  RA.id AS ref,
								  RA.action,
								  RA.action AS risk_action,
								  RA.deliverable,
								  RA.timescale,
								  RA.deadline,
								  RA.progress,
								  RA.remindon,
								  RA.status,
								  RA.action_status AS actionstatus,
								  RAS.color as statuscolor,
	  							  RA.action_owner AS owner, 
								  CONCAT(TK.tkname,' ',TK.tksurname) action_owner,
								  DP.value ,
								  RAS.name as action_status
								  FROM  ".$_SESSION['dbref']."_actions RA 
								  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
								  LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid  = RA.action_owner 
								  INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept									
								  WHERE RA.risk_id = '".$this -> risk_id."' AND RA.active = 1 
								  ORDER BY RA.insertdate DESC
								  LIMIT $start, $limit
		 ");
		 foreach( $response as $key => $row ) {

		 	if( $row['owner'] !== $_SESSION['tid'] )
			{
				$response[$key]['isUpdate'] = "No";
			} else {
				$response[$key]['isUpdate'] = "Yes";
			}		 	  
		 }
		return $response;
	}
	
	function _getTotalAction(){
	
		$response = $this -> getRow( " 
									SELECT 
									COUNT(*) AS total
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid  = RA.action_owner 
								    INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept									
									WHERE RA.risk_id = '".$this -> risk_id."' AND RA.active = 1
		 ");
		 return $response['total'];	
	}
	
	function getActionStats(){
	
		$response = $this -> getRow( " 
									SELECT 
									COUNT(*) AS total,
									SUM(progress) AS totalProgress,
									AVG(progress) AS averageProgress
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON  TK.tkid  = RA.action_owner 
								    INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept									
									WHERE RA.risk_id = '".$this -> risk_id."' AND RA.active = 1
		 ");
		 return $response;	
	}	
	
	function _getApprovedIds(){
		$response = $this -> get("SELECT action_id FROM ".$_SESSION['dbref']."_action_approved");
		$actionIds = "";
		foreach( $response as $res ) {
			$actionIds .=  $res['action_id'].","; 
		}
		$actionIds = rtrim($actionIds,",");
		return $actionIds;
	}
	
	function getActionToApprove( $user, $start, $limit) {
		$approved = $this -> _getApprovedIds();	
		if( $approved == ""){
			$approvedIn = "";
		} else{
			$approvedIn = "AND RA.id NOT IN ( ".$approved." )";
		}
		
		$userQuery = "";
		if( $user == ""){
			$userQuery = " 1 ";
		} else {
			$userQuery = "RA.action_owner = '".$user."'";	
		}
		
		$response = $this -> get( " SELECT 
									RA.id ,
									RA.id AS ref,
									RA.action,
									RA.action AS risk_action,
									RA.action_owner,
									RA.insertuser,
									RA.action_status AS status,									
									RA.deliverable,
									RA.timescale,
									RA.deadline, 
									RA.progress,
									RA.remindon,
									RAS.name as action_status,
									RAS.color as statuscolor,
									UA.user 
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
									WHERE $userQuery  
									AND RA.action_status & ".RiskAction::REQUEST_APPROVAL." = ".RiskAction::REQUEST_APPROVAL."
									ORDER BY RA.insertdate DESC									
									LIMIT $start, $limit 
		 ");
		return array( "data" => $response, "total" => $this->_totalToBeApproved( $user ) );
	}
	
	/**
		Get the total actions to be approved
	**/
	function _totalToBeApproved( $user )
	{
		$approved = $this -> _getApprovedIds();	
		if( $approved == ""){
			$approvedIn = "";
		} else{
			$approvedIn = "AND RA.id NOT IN ( ".$approved." )";
		}
		
		$userQuery = "";
		if( $user == ""){
			$userQuery = " 1 ";
		} else {
			$userQuery = "RA.action_owner = '".$user."'";	
		}

		$response = $this -> get( " SELECT 
									COUNT(*) AS total 
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
									WHERE $userQuery $approvedIn
									AND RA.action_status & ".RiskAction::REQUEST_APPROVAL." = ".RiskAction::REQUEST_APPROVAL."
		 ");
		 $total = 0;
		 foreach($response as $key => $value){
		 	$total = $value;
		 }
		 return $total; 		
	}
	
	function unlockAction( $id , $status){
		$up = $this->update( "actions" , array("action_status" => RiskAction::ACTION_AWAITING), "id=$id" );
		return $up;
	}
	/**
		Fetched the ids of the approved actions for this user
		@param : user id 
		@return string
	**/
	function _getUserApprovedIds( $id ) 
	{
		$response = $this -> get("SELECT action_id 
								  FROM ".$_SESSION['dbref']."_action_approved AA
								  INNER JOIN ".$_SESSION['dbref']."_actions A ON AA.action_id = A.id
								  WHERE action_owner = '".$id."'
								  ");
		$userActionIds = "";
		foreach( $response as $res ) {
			$userActionIds .=  $res['action_id'].","; 
		}
		$userActionIds = rtrim($userActionIds,",");
		return $userActionIds;			
	}
	/**
		Get all the approved actions 
	**/
	function getApprovedActions( $user, $start, $limit) {
		$approved = $this -> _getApprovedIds();	
		
		$userQuery = "";
		if( $user == ""){
			$userQuery = " 1 ";
		} else {
			$userQuery = "RA.action_owner = '".$user."'";	
		}		
		$response = $this -> get( " SELECT 
									RA.id ,
									RA.id AS ref,
									RA.action,
									RA.action AS risk_action,
									RA.deliverable,
									RA.action_status AS status,										
									RA.timescale, 
									RA.deadline,
									RA.progress, 
									RA.remindon,
									RAS.name as action_status, 
									RAS.color as statuscolor, 
									RA.insertuser,
									UA.user 
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
									WHERE $userQuery  
									AND RA.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED."
									ORDER BY RA.insertdate DESC									
									LIMIT $start, $limit
		 "); 
		return array( "data" => $response, "total" => $this->_toTotalApprovedActions( $user ) );
	}
	/**
		Get the total count of the approved actions
	**/
	function _toTotalApprovedActions( $user )
	{
		//$approved = $this -> _getApprovedIds();	
		
		$userQuery = "";
		if( $user == ""){
			$userQuery = " 1 ";
		} else {
			$userQuery = "RA.action_owner = '".$user."'";	
		}		
		$response = $this -> get("SELECT 
								   COUNT(*) AS total
								   FROM  ".$_SESSION['dbref']."_actions RA 
								   LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
								   LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
								   WHERE $userQuery  
								   AND RA.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED."
								 ");
		 $total = 0;
		 foreach($response as $key => $value){
		 	$total = $value;
		 }
		 return $total;
	}
	
	
	function getUserApprovedActions( $id ) {
		$approved = $this -> _getUserApprovedIds( $id );	
		
		$reponse = $this -> get( "  SELECT RA.id ,RA.action, RA.deliverable, RA.timescale, RA.deadline, RA.progress, RA.remindon,
									RA.action_owner, RAS.name as status, RAS.color as statuscolor,  UA.user 
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
									WHERE RA.action_owner = '".$id."' AND RA.id IN ( ".$approved." )
		 ");
		return $reponse;
	}
		
	
	
	function getAAction( $id ) {	

		$reponse = $this -> getRow( " SELECT 
									  RA.id ,
									  RA.action,
									  RA.deliverable,
									  RA.timescale,
									  RA.deadline,
									  RA.progress AS updatedprogres,
									  RA.progress,
									  RA.remindon,
									  RA.action_status,
									  RAS.name AS status,
									  RAST.name AS updatedstatus,
									  RA.status AS updatestatusid,
									  RAS.color AS statuscolor,
									  UA.tkname ,
									  UA.tksurname, 
									  RA.action_owner,
									  RA.risk_id,
									  RR.description ,
									  AC.tkemail AS action_creator ,
									  RA.attachment,
									  UA.tkemail AS action_owner_email							  
									  FROM  ".$_SESSION['dbref']."_actions RA 
									  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id								
									  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid	
									  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep AC ON RA.insertuser = AC.tkid
									  LEFT JOIN ".$_SESSION['dbref']."_action_status RAST ON RA.status = RAST.id
									  INNER JOIN  ".$_SESSION['dbref']."_risk_register RR ON RR.id = RA.risk_id
									  WHERE RA.id = '".$id."' ORDER BY RA.insertdate DESC
									  
		 ");
		return $reponse;
	}
		
	/**
		Get all actions for this logged in user	
	**/	
	function getRiskAction( $id ) {
		$response = $this -> getRow( " 
									SELECT 
									RA.id ,
									RA.id AS ref,
									RA.action,
									RA.action AS risk_action,
									RA.deliverable,
									RA.timescale,
									RA.deadline,
									RA.progress,
									RA.remindon,
									RAS.name as status,
									RAS.color as statuscolor,
									CONCAT(UA.tkname,' ',UA.tksurname) AS owner,
									RA.action_owner,
									RA.attachment,
									RA.action_status
									FROM  ".$_SESSION['dbref']."_actions RA 
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid
									WHERE RA.id = '".$id."'
		 ");
		 $latestUpdate = $this -> getLatestActionUpdates( $id );
		return $response;
	}
	
	function getRiskAssurance( $id ) {
								   	
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_assurance AA
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.insertuser
								   INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   WHERE action_id = '".$id."' ");
		return $response;
	}
	
	function saveActionAssurance($action_id, $date_tested, $response, $signoff, $assurance, $attachment, $active)
	{
		$insert_data = array(
						'action_id' 		=> $action_id,
						'response'  		=> $response,
						'signoff' 			=> $signoff,
						'attachment' 		=> $attachment,
						'insertuser'		=> $_SESSION['tid'],						
		);
		$response = $this -> insert( 'action_assurance', $insert_data );
		echo $response;//$this -> insertedId();
	}
	
	function getApprovalRequestStatus( $id )
	{
		$response = $this -> getRow("SELECT action_status FROM ".$_SESSION['dbref']."_actions  WHERE id = $id");
		return $response;
	}
	
	function setActionStatus( $id, $state, $status = "")
	{
		$updatedata = array();
		$insertdata = array();
		if($status != "")
		{
		  $updatedata =  array( "action_status" => $state, "status" => $status );
		} else {
		  $updatedata =  array( "action_status" => $state );
		}
		
		if( ($state & RiskAction::DECLINE_APPROVAL) == RiskAction::DECLINE_APPROVAL)
		{
			$insertdata['user'] = $_SESSION['tkn'];
			$insertdata['request_approval'] = "Declined action";
			$insertdata['response'] = $_POST['response'];
			$insertdata['action_status'] = array("from" => "Completed", "to" => "In Progress");
		}
		
		$response = $this -> insert( 'actions_update', array("changes" => serialize($insertdata), "action_id" => $id));	
		$res   = $this -> update( "actions" ,$updatedata, "id=$id");
		return $res;
	}
	
	/*function approveAction( $id, $response, $approval, $notification, $attachment, $status)
	{
		$insertdata  = array(  "action_id" 	    => $id,
							   "description"    => $response,
							   "approval" 		=> $approval,
							   "attachment" 	=> $attachment,
							   "notification" 	=> $notification,						   
							   "insertuser"		=> $_SESSION['tid'],
			);
		$this->update("actions", array("action_status" => RiskAction::ACTION_APPROVED ), "id=$id");
		$this->insert( "action_approved", $insertdata );
		return $this -> insertedId();
	}*/
	function approveAction($id,$response) {
		$defObj = new Defaults("","");
		$action_object_name = $defObj->getObjectName("ACTION");
		$actionStatusObj = new ActionStatus();
		$currentstatus = $actionStatusObj->getAStatus(3);
		$action = $this->getAAction($id);
		$changes = array(
			'response'=>$response,
			'approval_status'=>$action_object_name." has been approved.",
			'user'=>$_SESSION['tkn'],
			'timestamp'=>date("d-M-Y H:i:s"),
			'currentstatus'=>$currentstatus['value'],
		);
		$insert_data = array(
			'action_id'=>$id,
			'changes'=>base64_encode(serialize($changes)),
			'description'=>$response,
			'status'=>$action['updatestatusid'],
			'insertuser'=>$_SESSION['tid'],
		);
		$update_data = array(
			'action_status'=> ( (isset($action['action_status']) ? $action['action_status'] :RiskAction::ACTION_AWAITING ) - RiskAction::ACTION_AWAITING + RiskAction::ACTION_APPROVED),
		);
		$this->update("actions", $update_data, "id=$id");
		$mar = $this->insert("actions_update", $insert_data);
		$insert_id = $mar;//$this->insertedId();
		//email
		$msg = $response;
		$this->sendApprovalEmail($id,$msg,"Approved");
		
		return $insert_id;
	}

	function declineAction($id,$response) {
		$defObj = new Defaults();
		$action_object_name = $defObj->getObjectName("ACTION");
		$actionStatusObj = new ActionStatus();
		$compl_status = $actionStatusObj->getAStatus(3);
		$ip_status = $actionStatusObj->getAStatus(2);
		$action = $this->getAAction($id);
		$changes = array(
			'response'=>$response,
			'approval_status'=>$action_object_name." has been declined.",
			'status'=>array('to'=>$ip_status['value'],'from'=>$compl_status['value']),
			'action_progress'=>array('to'=>"99%",'from'=>"100%"),
			'user'=>$_SESSION['tkn'],
			'timestamp'=>date("d-M-Y H:i:s"),
			'currentstatus'=>$ip_status['value'],
		);
		$insert_data = array(
			'action_id'=>$id,
			'changes'=>base64_encode(serialize($changes)),
			'description'=>$response,
			'status'=>$action['updatestatusid'],
			'insertuser'=>$_SESSION['tid'],
		);
		$update_data = array(
			'action_status'=> ($action['action_status'] - RiskAction::ACTION_AWAITING),
			'status'=>2,
			'progress'=>99,
		);
		$mar = $this->update("actions", $update_data, "id=$id");
		$mar = $this->insert("actions_update", $insert_data);
		$insert_id = $this->insertedId();
		//email
		$msg = $response;
		$this->sendApprovalEmail($id,$msg,"Declined");
		
		return $mar;//$insert_id;
	}

	function unlockApprovedAction($id,$response) {
		$defObj = new Defaults();
		$action_object_name = $defObj->getObjectName("ACTION");
		$actionStatusObj = new ActionStatus();
		$compl_status = $actionStatusObj->getAStatus(3);
		$ip_status = $actionStatusObj->getAStatus(2);
		$action = $this->getAAction($id);
		$changes = array(
			'response'=>$response,
			'approval_status'=>$action_object_name." has been unlocked.",
			'status'=>array('to'=>$ip_status['value'],'from'=>$compl_status['value']),
			'action_progress'=>array('to'=>"99%",'from'=>"100%"),
			'user'=>$_SESSION['tkn'],
			'timestamp'=>date("d-M-Y H:i:s"),
			'currentstatus'=>$ip_status['value'],
		);
		$insert_data = array(
			'action_id'=>$id,
			'changes'=>base64_encode(serialize($changes)),
			'description'=>$response,
			'status'=>$action['updatestatusid'],
			'insertuser'=>$_SESSION['tid'],
		);
		$update_data = array(
			'action_status'=> ($action['action_status'] - RiskAction::ACTION_APPROVED),
			'status'=>2,
			'progress'=>99,
		);
		$mar = $this->update("actions", $update_data, "id=$id");
		$this->insert("actions_update", $insert_data);
		$insert_id = $this->insertedId();
		//email
		$msg = $response;
		$this->sendApprovalEmail($id,$msg,"Unlocked");
		
		return $mar;// $insert_id;
	}
	
	function sendApprovalEmail($action_id,$msg,$subject_word) {
		$defObj = new Defaults("","");
		$action_object_name = $defObj->getObjectName("ACTION");
		$risk_object_name = $defObj->getObjectName("OBJECT");
        $action	        = $this->getRiskActionDetail($action_id) ;
		
			$user = new UserAccess();
				//Risk Owners (Sub-Dir Admins)
                $risk_sub_id = isset($action['risk_sub_id']) ? $action['risk_sub_id'] : 0;
				$to1 = $user->getRiskOwnerEmails($risk_sub_id);
				$action_owner = isset($action['owner']) ? $action['owner'] : "";
				$to1[] = $user->getActionOwnerEmail($action_owner,true);
				//merge and check for duplicates
				$to = array();
				$to_dup = array();
				foreach($to1 as $t) {
					$x = str_replace("@","",str_replace(" ","",strtolower($t['name'])."".strtolower($t['email'])));
					if(!in_array($x,$to_dup)) {
						$to[] = $t;
						$to_dup[] = $x;
					}
				}
				//Risk Managers
					//set risk manager as cc recipient
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
				$cc2 = array();
				foreach($ccm as $c) {
					$cc2[] = $c['email'];
				}
				$cc = implode(",",$cc2);
			//Get From
				$from = $user->getCurrentUser();
			//Subject
			$subject = "".$action_object_name." ".$action_id." ".$subject_word;
			//Create messager
        $risk_description = isset($action['risk_description']) ? $action['risk_description'] : '';
$message = $action_object_name." ".$action_id." has been ".strtolower($subject_word)." by ".$from['name']." with the message:
".$msg."

The associated ".$risk_object_name." is ".isset($action['risk_id']) ? $action['risk_id'] : ''.": ".$risk_description.".";
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			$email->sendEmail();
	}
	
	
	function updateRiskAction( $id , $changes)
	{
		$vali = new Validation();
		if($vali->validStartEnd($this->deadline, $this->remindon)){	
				$insert_data = array(
								'action'  		=> $this -> action,
								'action_owner' 	=> $this -> action_owner,
								'deliverable' 	=> $this -> deliverable,
								'timescale' 	=> $this -> timescale,
								'deadline' 		=> $this -> deadline,
								'remindon' 		=> $this -> remindon,
								'progress' 		=> $this -> progress,
								'status' 		=> $this -> status,
								'attachment'	=> $this -> attachment,
								'insertuser'    => $_SESSION['tid'],										
				);
				$res = $this -> insert( 'action_edit', array_merge($insert_data ,array("action_id" => $id, "changes" => $changes ) ) );
				$response = $this -> update( 'actions', $insert_data , "id = $id" );
				return $res;
		
			} else {
				return  "The remind on date cannot be after the deadline";
			}			
	}	
	
	function getActionEditLog( $id )
	{
					  							
		$latest = $this->getLatestActionUpdates( $id );
		$join   = ""; 
		if( isset($latest) && !empty($latest)){
			$join = "LEFT JOIN ".$_SESSION['dbref']."_actions_update A ON A.action_id = AU.action_id ";			
		} else{
			$join = "INNER JOIN ".$_SESSION['dbref']."_actions A ON A.id = AU.action_id ";			
		}
		$response = $this -> get("SELECT 
								  AU.id,
								  AU.insertdate,
								  AU.changes , 
								  AST.name AS status,
								  AST.color,
								  CONCAT(TK.tkname, ' ', TK.tksurname) AS updateby
								  FROM ".$_SESSION['dbref']."_action_edit AU
								  INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AU.insertuser
								  ".$join."
								  LEFT JOIN ".$_SESSION['dbref']."_action_status AST ON A.status = AST.id
								  WHERE AU.action_id = '".$id."'
								  ORDER BY AU.insertdate ASC, AU.id ASC
								  ");
		return $response;		
	}
	
	function getActionUpdates( $id )
	{
		
		
		$response = $this -> get("SELECT 
								  AU.id,
								  AU.changes,
								  AU.insertdate,
								  AST.name AS status
								  FROM ".$_SESSION['dbref']."_actions_update AU
								  INNER JOIN ".$_SESSION['dbref']."_action_status AST ON AST.id = AU.status 
								  WHERE AU.action_id = '".$id."'
							      ORDER BY AU.insertdate DESC, AU.id ASC								  
								  "
								  );
		return $response;
	}

	function getLatestActionUpdates( $id )
	{

		$response = $this -> getRow("SELECT AU.progress, AU.description, AU.insertdate, ASt.name, AU.remindon, AU.status, AU.attachment
									  FROM ".$_SESSION['dbref']."_actions_update AU
									  INNER JOIN ".$_SESSION['dbref']."_action_status ASt ON ASt.id = AU.status 
									  WHERE AU.action_id = '".$id."' ORDER BY insertdate desc");
		return $response;
	}
	
	function deleteRiskAction( $id )
	{
		$action_id = $id;
        $statusObj 		= new ActionStatus("","", "");
        $statuses 		= $statusObj -> getStatuses();
//        $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
        $action	        = $this -> getActionDetail($id) ;
		$riskObj = new Risk();
		$riskOld = $riskObj->getRiskDetail($action['risk_id']);
		$defObj = new Defaults("","");
		$action_object_name = $defObj->getObjectName("ACTION");
		$risk_object_name = $defObj->getObjectName("OBJECT");

        $updates = array();
		$update_data = array("active" => 0);
		$change_arr = array();
			//$change_arr['active'] = array('from'=>1,'to'=>0);
            $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
            $change_arr['user']   = $_SESSION['tkn'];
            $change_arr['comment']   = $action_object_name." ".self::REFTAG.$id." has been deleted";
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $id;

		$res                      = $this -> editRiskAction($id, $update_data, $updates);
		if($res>0) { 
			$response = array("error"=>false,"text"=>$action_object_name." ".self::REFTAG.$id." has been deleted successfully");
		//Get recipients emails
			$user = new UserAccess();
				//Risk Owners (Sub-Dir Admins)
				$to1 = $user->getRiskOwnerEmails($riskOld['sub_id']);
				$to1[] = $user->getActionOwnerEmail($action['owner'],true);
				$to = array();
				$to_dup = array();
				foreach($to1 as $t) {
					$x = str_replace("@","",str_replace(" ","",strtolower($t['name'])."".strtolower($t['email'])));
					if(!in_array($x,$to_dup)) {
						$to[] = $t;
						$to_dup[] = $x;
					}
				}
				//Risk Managers
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
				$cc2 = array();
				foreach($ccm as $c) {
					$cc2[] = $c['email'];
				}
				$cc = implode(",",$cc2);
			//Get From
				$from = $user->getCurrentUser();
			//Subject
			$subject = "".$action_object_name." ".$action_id." Deleted";
			//Create message
$message = $action_object_name." ".$action_id." has been deleted by ".$from['name'].".

The associated ".$risk_object_name." is ".$action['risk_id'].": ".$riskOld['description'].".";
			//Email
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			$email->sendEmail();
			
		} else {
			$response = array("error"=>true,"text"=>"An error occurred trying to delete action ".self::REFTAG.$id.". Please check that it hasn't already been deleted.");
		}
		return  $response;
	}
	/**
		Activate an actions
		@paran int , action id
		@return int
	**/
	function activateRiskAction( $id )
	{
		$updatedata = array( 
								"active" 		=> 1,
								'insertuser'	=> $_SESSION['tid'],
								);
		$response  = $this -> update( 'actions' , $updatedata , "id = $id");
		echo $response;
	}
	/**
	 Fetched the actions associated with the user for a particular risks
	 @param int riskid , int userid- risk owner
	 @return arrray, of risk ids that the user has actions to it
	**/
	function userAssignedAction( $user ) {
		$response = $this -> get("
								  SELECT id, risk_id, action_owner, action 
								  FROM ".$_SESSION['dbref']."_actions 
								  WHERE action_owner = '".$user."'
								  GROUP BY risk_id"
								  );
		return $response;
	}
	
	function searchActionToApprove( $text )
	{
		$query = "SELECT RA.id ,RA.action, RA.deliverable, RA.timescale, RA.deadline, RA.progress, RA.remindon,
				  RAS.name as status, RAS.color as statuscolor, UA.user  
				  FROM  ".$_SESSION['dbref']."_actions RA 
				  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
				  LEFT JOIN  ".$_SESSION['dbref']."_useraccess UA ON RA.action_owner = UA.id
				  WHERE RA.action LIKE '%".$text."%' OR RAS.name LIKE '%".$text."%'
				  OR RA.progress LIKE '%".$text."%' OR RA.deliverable LIKE '%".$text."%'
		 ";
		 $response = $this -> get( $query );
		 return $response;
	}
	
	function getActionNotification( $where )
	{
		$reponse = $this -> getRow( " SELECT 
								  RA.id ,
								  RA.action,
								  RA.deliverable,
								  RA.timescale,
								  RA.deadline,
								  AU.progress AS updatedprogres,
								  RA.progress,
								  RA.remindon,
								  RA.action_status,
								  RAS.name as status,
								  RAST.name as updatedstatus,
								  RAS.color as statuscolor,
								  UA.tkname ,
								  UA.tksurname, 
								  RA.action_owner,
								  RA.risk_id,
								  RR.description ,
								  AC.tkemail AS action_creator ,
								  UA.tkemail AS action_owner_email							  
								  FROM  ".$_SESSION['dbref']."_actions RA 
								  LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id								
								  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid	
								  LEFT JOIN  assist_".$_SESSION['cc']."_timekeep AC ON RA.insertuser = AC.tkid
								  LEFT JOIN  ".$_SESSION['dbref']."_actions_update AU ON RA.id = AU.action_id
								  LEFT JOIN ".$_SESSION['dbref']."_action_status RAST ON AU.status = RAST.id    
								  INNER JOIN  ".$_SESSION['dbref']."_risk_register RR ON RR.id = RA.risk_id
								  WHERE $where ORDER BY AU.insertdate DESC
		 ");
		return $reponse;	
	}

	
	/*********************
	Functions by Janet Currie
	********************/
	
	function getActionsWaitingApproval($risk_owner,$risk,$action_owner,$folder) {
		$sql = " 
									SELECT 
									RA.id ,
									RA.id AS ref,
									RA.action,
									RA.action AS risk_action,
									RA.deliverable,
									RA.timescale,
									RA.deadline,
									RA.progress,
									RA.remindon,
									CONCAT(UA.tkname,' ',UA.tksurname) AS action_owner,
									RA.action_owner as owner,
									RA.action_status as status,
									RAS.name as action_status
									FROM  ".$_SESSION['dbref']."_actions RA 
									INNER JOIN ".$_SESSION['dbref']."_risk_register R
									  ON R.id = RA.risk_id AND R.active = 1
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid
									".($folder!="admin" ? "INNER JOIN ".$_SESSION['dbref']."_dir_admins RDA ON R.sub_id = RDA.ref AND RDA.type = 'SUB' AND RDA.active = 1 AND RDA.tkid = '".$_SESSION['tid']."'" : "")."
									WHERE RA.action_status & ".RiskAction::ACTION_AWAITING." = ".RiskAction::ACTION_AWAITING."
									AND RA.active = 1
									".(strlen($risk_owner)>0 && is_numeric($risk_owner) ? "AND R.sub_id = ".$risk_owner : "")."
									".(strlen($risk)>0 && is_numeric($risk) ? "AND RA.risk_id = ".$risk : "")."
									".(strlen($action_owner)>3 ? "AND RA.action_owner = '".$action_owner."'" : "")."
		 ";		
		 $response = $this -> get($sql);
		 return $response;
		 //return array($sql);
	}
	
	function getActionsAlreadyApproved($risk_owner,$risk,$action_owner,$folder) {
		$sql = " 
									SELECT 
									RA.id ,
									RA.id AS ref,
									RA.action,
									RA.action AS risk_action,
									RA.deliverable,
									RA.timescale,
									RA.deadline,
									RA.progress,
									RA.remindon,
									CONCAT(UA.tkname,' ',UA.tksurname) AS action_owner,
									RA.action_owner as owner,
									RA.action_status as status,
									RAS.name as action_status
									FROM  ".$_SESSION['dbref']."_actions RA 
									INNER JOIN ".$_SESSION['dbref']."_risk_register R
									  ON R.id = RA.risk_id AND R.active = 1
									LEFT JOIN ".$_SESSION['dbref']."_action_status RAS ON RA.status = RAS.id
									LEFT JOIN  assist_".$_SESSION['cc']."_timekeep UA ON RA.action_owner = UA.tkid
									".($folder!="admin" ? "INNER JOIN ".$_SESSION['dbref']."_dir_admins RDA ON R.sub_id = RDA.ref AND RDA.type = 'SUB' AND RDA.active = 1 AND RDA.tkid = '".$_SESSION['tid']."'" : "")."
									WHERE RA.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED."
									AND RA.active = 1
									".(strlen($risk_owner)>0 && is_numeric($risk_owner) ? "AND R.sub_id = ".$risk_owner : "")."
									".(strlen($risk)>0 && is_numeric($risk) ? "AND RA.risk_id = ".$risk : "")."
									".(strlen($action_owner)>3 ? "AND RA.action_owner = '".$action_owner."'" : "")."
		 ";		
		 $response = $this -> get($sql);
		 return $response;
		 //return array(0=>$sql);
	}
	
	function getCompleteActionLogs($action_id) {
		$logs = array();
		$updates = $this -> get("SELECT 
								  AU.id,
								  AU.changes,
								  AU.insertdate,
								  AST.name AS status
								  FROM ".$_SESSION['dbref']."_actions_update AU
								  LEFT JOIN ".$_SESSION['dbref']."_action_status AST ON AST.id = AU.status 
								  WHERE AU.action_id = ".$action_id."
							      ORDER BY AU.id DESC ");
		foreach($updates as $u) {
			$time = strtotime($u['insertdate']);
			$u['change_arr'] = unserialize(base64_decode($u['changes']));
			$u['user'] = $u['change_arr']['user'];
			$u['currentstatus'] = isset($u['change_arr']['currentstatus']) ? $u['change_arr']['currentstatus'] : "In Progress";
			//$u['change_arr']['progress'] = $u['change_arr']['action_progress'];
			unset($u['change_arr']['user']);
			unset($u['change_arr']['currentstatus']);
			$u['change_arr'] = change_key($u['change_arr'],"action_progress","progress");
			while(isset($logs[$time])) { $time++; }
			$logs[$time] = $u;
		}
		$edits = $this->get("SELECT 
								  AE.id,
								  AE.changes,
								  AE.insertdate,
								  AST.name AS status
								  FROM ".$_SESSION['dbref']."_action_edit AE
								  LEFT JOIN ".$_SESSION['dbref']."_action_status AST ON AST.id = AE.status 
								  WHERE AE.action_id = ".$action_id."
							      ORDER BY AE.id DESC ");
		foreach($edits as $u) {
			$time = strtotime($u['insertdate']);
			$u['change_arr'] = unserialize(base64_decode($u['changes']));
			$u['user'] = $u['change_arr']['user'];
			$u['currentstatus'] = $u['change_arr']['currentstatus'];
			unset($u['change_arr']['user']);
			unset($u['change_arr']['currentstatus']);
			$u['change_arr'] = change_key($u['change_arr'],"action_progress","progress");
			while(isset($logs[$time])) { $time++; }
			$logs[$time] = $u;
		}
		return $logs;
	}
	
	function getAttachmentsForLogs($id) {
		$sql = "SELECT attachment FROM ".$_SESSION['dbref']."_actions WHERE id = ".$id;
		$result = $this->getRow($sql);
		
		$attachments = array();
		if(isset($result['attachment']) && strlen($result['attachment'])>0) {
			$a = base64_decode($result['attachment']);
			$attachments = unserialize($a);
		}
		//$attachments = $result['attachment'];
		return $attachments;
	}

	
	 static function getStatusSQLForWhere($a) 
     {
        $sql = "(
                        ".$a.".active = 1
                        
        )";
        return $sql;
     }     
 	
}

function change_key( $array, $old_key, $new_key) {

    if( ! array_key_exists( $old_key, $array ) )
        return $array;

    $keys = array_keys( $array );
    $keys[ array_search( $old_key, $keys ) ] = $new_key;

    return array_combine( $keys, $array );
}
?>
