<?php
/**
	* @package 	: Imapct
	* @author	: admire<azinamo@gmail.com>
	* @copyright: 2011 , Ignite Assist
**/
class Impact extends DBConnect
{
		protected $rating_from;
		
		protected $rating_to;
		
		protected $assessment;
		
		protected $definition;
		
		protected $color;
		
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
	
		
	function __construct( $rating_from="", $rating_to="", $assessment="", $definition="",  $color="FFFFFF" )
	{
		$this -> rating_from = trim($rating_from);
		$this -> rating_to 	 = trim($rating_to);
		$this -> assessment  = trim($assessment);
		$this -> definition  = trim($definition);
		$this -> color 		 = trim($color);						
		parent::__construct();
	}
			
	function saveImpact()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "impact" , $insert_data );
		echo $response;//$this -> insertedId();
	}		
		
	function updateImpact( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$logsObj = new Logs();
		$data    = $this->getAImpact( $id );
		$logsObj -> setParameters( $insert_data, $data, "impact");
		$response = $this -> update( "impact" , $insert_data, "id=$id" );
		return $response;		
	}	
			
	function getImpact()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE  active <> 2 " );
		foreach($response as $key => $r) {
			$response[$key]['name'] = $r['assessment'];
		}
		return $response;
	}
	
	function getActiveImpact()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE active = 1 " );
		return $response;
	}

	function getActiveForEdit()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE active = 1 " );
		$data = array(
			'active'=>array(),
			'inactive'=>array()
		);
		foreach($response as $key => $r) {
			$response[$key]['name'] = $r['assessment'];
			if($r['active']==1) {
				$data['active'][$key] = $response[$key];
			} else {
				$data['inactive'][$key] = $response[$key];
			}
		}
		
		return $data; //array('active'=>$response);
	}
	
	
	function getAImpact( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE id = '".$id."'" );
		$response['from'] = $response['rating_from'];
		$response['to'] = $response['rating_to'];
		$response['diff'] = $response['to'] - $response['from'];
		return $response;
	}
	
	
	function getImpactRating( $id )
	{
		$response = $this -> get( "SELECT rating_from, rating_to FROM ".$_SESSION['dbref']."_impact WHERE id = '".$id."' " );
		$diff = $to = $from = 0;
		foreach($response as $row ) {
			$diff 	= $row['rating_to'] - $row['rating_from'];
			$to 	= $row['rating_to'];
			$from 	= $row['rating_from'];
		} 
		
		echo json_encode( array( "from" => $from, "to" => $to , "diff" => $diff) );
	}	

	function updateImpactStatus( $id , $status)
	{
		$updatedata = array(
						'active'	 =>  $status,
						"insertuser" => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAImpact( $id );
        $logsObj -> setParameters( $updatedata, $data, "impact");		
		$response = $this -> update( 'impact', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateImpact( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAImpact( $id );
        $logsObj -> setParameters( $updatedata, $data, "impact");		
		$response = $this -> update( 'impact', $update_data, "id=$id" );
		echo $response;
	}

	
	
	
	
	
	
	
	
	function addItem($from, $to, $name,$def,$color) {
		$insert_data = array( 
						"rating_from"		=> $from,
						"rating_to"		=> $to,
						"assessment"			=> $name,
						"definition"	=> $def,
						"color"			=> $color,
						"active" 		  => Impact::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "impact" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $from, $to, $name, $def, $color )
	{
		$insertdata = array(
						"rating_from" 			 => $from,
						"rating_to" 			 => $to,
						"assessment"		 => $name,
						"definition" 		 => $def,
						"color" 		 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAImpact( $id );
		$var = array(
			'id'=>$id,
			'rating_from'=>$from,
			'rating_to'=>$to,
			'assessment'=>$name,
			'definition'=>$def,
			'color'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "impact");
		$response = $this -> update( "impact", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAImpact( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "impact");
		$response = $this -> update( 'impact', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
		
	
	
	
	
		
		
	
	function getReportList() {
		$sql = "SELECT DISTINCT L.id, L.assessment as name FROM ".$_SESSION['dbref']."_impact L 
				INNER JOIN ".$_SESSION['dbref']."_risk_register R
				ON R.impact = L.id
				WHERE L.active <> 2 
				ORDER BY rating_from, rating_to, assessment"; 
		$data = $this -> get( $sql );
		return $data;
	}
	
	
}
?>