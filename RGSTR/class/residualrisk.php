<?php
/**
	* @package 	: Residual Risk
	* @author	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class ResidualRisk extends DBConnect
{

	protected $rating_from;
	protected $rating_to;
	protected $magnitude;
	protected $response;
	protected $color;

	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
			
	function __construct( $rating_from="", $rating_to="", $magnitude="", $response="",  $color="FFFFFF" )
	{
		$this -> rating_from= trim($rating_from);
		$this -> rating_to 	= trim($rating_to);
		$this -> magnitude  = trim($magnitude);
		$this -> response  	= trim($response);
		$this -> color 		= trim($color);						
		parent::__construct();
	}
			
	function saveResidualRisk()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"    => $_SESSION['tid'],						
						);
		$response = $this -> insert( "residual_exposure" , $insert_data );
		echo $response;//$this -> insertedId();
	}			
		
	function updateResidualRisk( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getAResidualRisk( $id );
        $logsObj -> setParameters( $insert_data, $data, "residual_exposure");						
		$response = $this -> update( "residual_exposure" , $insert_data, "id=$id " );
		return $response;	
	}	
	
	function getActiveForEdit() {
		$response = $this->getRR();
		$data = array(
			'active'=>array(),
			'inactive'=>array(),
		);
		foreach($response as $key=>$r) {
			$r['name']=$r['magnitude'];
			if($r['active']==1) {
				$data['active'][$key] = $r;
			} else {
				$data['inactive'][$key] = $r;
			}
		}
		return $data;
	}
		
	function getResidualRisk() {
		echo json_encode( $this->getRR() );
	}
	
	function getRR()
	{
		$response = $this -> get( "SELECT *, magnitude as name FROM ".$_SESSION['dbref']."_residual_exposure WHERE active <> 2" );
		return $response;
	}
	/**
		Fetches all residual risk , and makes and array with the range group
		@return  array
	**/
	function getResidualRiskRanges()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure " );
		$rangesColor = array();
		foreach($response as $key => $resArray ) 
		{
			$range = "";
			// create a string of the range 
			for( $i = $resArray['rating_from']; $i <= $resArray['rating_to'] ; $i++ ) 
			{
				$range .= $i.","; 
			}
			//create an array with the range and the associated color
			$rangesColor[$resArray['magnitude']] = array( "color" => $resArray['color'], "range"=>rtrim($range,",") );	
		}
		return $rangesColor;
	}
	
	/**
		Fetches all residual risk , and makes and array with the range group
		@return  array
	**/
	function getRiskResidualRanges( $value )
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure " );
		$rangesColor = "";
		foreach($response as $key => $resArray ) 
		{
			//$range = "";
			// create a string of the range 
			for( $i = $resArray['rating_from']; $i <= $resArray['rating_to'] ; $i++ ) 
			{
				if( $i == $value )
				{
					$rangesColor['color'] 	  = $resArray['color'];
					$rangesColor['magnitude'] = $resArray['magnitude'];	
					$rangesColor['response'] = $resArray['response'];	
				}
				//$range .= $i.","; 
			}
			//create an array with the range and the associated color
			//$rangesColor[$resArray['color']] = array( "from" => $resArray['rating_from'], "to"=>$resArray['rating_to'] );	
		}
		return $rangesColor;
	}	
	
	function getAResidualRisk( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure WHERE id = $id" );
		return $response;
	}

	function updateResidualRiskStatus( $id, $status )
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAResidualRisk( $id );
        $logsObj -> setParameters( $updatedata, $data, "residual_exposure");								
		$response = $this -> update( 'residual_exposure', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateResidualRisk( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAResidualRisk( $id );
        $logsObj -> setParameters( $update_data, $data, "residual_exposure");								
		$response = $this -> update( 'residual_exposure', $update_data, "id=$id" );
		echo $response;
	}
	
	
	
	

	
	
	
	
	
	
	
	
	function addItem($from, $to, $name,$def,$color) {
		$insert_data = array( 
						"rating_from"		=> $from,
						"rating_to"		=> $to,
						"magnitude"			=> $name,
						"response"	=> $def,
						"color"			=> $color,
						"active" 		  => ResidualRisk::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "residual_exposure" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $from, $to, $name, $def, $color )
	{
		$insertdata = array(
						"rating_from" 			 => $from,
						"rating_to" 			 => $to,
						"magnitude"		 => $name,
						"response" 		 => $def,
						"color" 		 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAResidualRisk( $id );
		$var = array(
			'id'=>$id,
			'rating_from'=>$from,
			'rating_to'=>$to,
			'magnitude'=>$name,
			'response'=>$def,
			'color'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "residual_exposure");
		$response = $this -> update( "residual_exposure", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAResidualRisk( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "residual_exposure");
		$response = $this -> update( 'residual_exposure', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	function findExposure($rating) {
		$sql = "SELECT id, magnitude as name, rating_from, rating_to FROM ".$_SESSION['dbref']."_residual_exposure WHERE rating_from <= $rating AND rating_to >= $rating AND active <> ".ResidualRisk::DELETED;
		$r = $this->getRow($sql);
		if(!isset($r['name'])) {
			$sql = "SELECT id, magnitude as name, rating_from, rating_to FROM ".$_SESSION['dbref']."_residual_exposure WHERE rating_to < $rating AND active <> ".ResidualRisk::DELETED." ORDER BY rating_to DESC LIMIT 1";
			$r = $this->getRow($sql);
		}
		return $r;
	}
	
	
		
	
	
	
	
		
		
	
	
	function getReportList($act="") {
		$sql = "SELECT DISTINCT L.* FROM ".$_SESSION['dbref']."_residual_exposure L 
				WHERE L.active <> 2 
				ORDER BY rating_from"; 
		$response = $this -> get( $sql );
		$data = array();
		foreach($response as $r) {
			if($act != "OUT") {
				$m = $r['rating_from'];
				if($r['rating_from']!=$r['rating_to']) { $m.=" - ".$r['rating_to']; }
				$m = " ($m)";
			} else { $m = ""; }
			$r['name'] = $r['magnitude'].$m;
			$data[] = $r;
		}
		return $data;
	}
}
?>