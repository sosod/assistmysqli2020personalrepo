<?php
$scripts = array( 'change_type_status.js','menu.js' );
$styles = array();
$page_title = "Change Risk Types Status";
require_once("../inc/header.php");
$type 	   = new RiskType( "", "", "", "" );
$risktype  = $type-> getARiskType( $_REQUEST['id'] ) ;	 
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form id="edit-form-risk-type" name="edit-form-risk-type">
<div id="change_risk_type_message"></div>
<table border="1" id="editarisk_type_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $risktype['id']; ?></td>
   <tr>
    <th>Statuses:</th>
    <td>
    	<select id="type_statuses" name="type_statuses">
        	<option value="1" <?php if($risktype['active']==1 ) { ?> selected <?php } ?> >Active</option>
            <option value="0" <?php if($risktype['active']==0 ) { ?> selected <?php } ?> >InActive</option>
        </select>
    </td>
   </tr>
   <tr>
    <th></th>
    <td>
    	<input type="hidden" name="risktype_id" id="risktype_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="changes_risktype" id="changes_risktype" value="Save Changes" />
        <input type="submit" name="cancel_risktype" id="cancel_risktype" value="Cancel" />
    </td>
  </tr>
  <tr>
 	<td class="noborder" align="left"><?php $me->displayGoBack("", ""); ?></td>
 	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>
