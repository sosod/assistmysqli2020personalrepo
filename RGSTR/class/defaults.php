<?php
/**
	* @package 	  : Defaults
	* @author	  : admire<azinamo@gmail.com>
	* @copyright  : 2011 Ignite Assist
**/
class Defaults extends DBConnect{
public $helper;
	function __construct()
	{
		parent::__construct();
		$this->helper = new ASSIST_MODULE_HELPER();
	}
	
	function updateDefaults( $id , $value ) 
	{
		$insert_data = array( "value" => $value );
		$response = "";
        $logsObj = new Logs();
        $data    = $this->getADefault( $id );
        $logsObj -> setParameters( $insert_data, $data, "defaults");		
		$response = $this -> update( 'defaults' , $insert_data, "id=$id" );
		echo $response;
	}
	
	function getDefaults()
	{
		$response = $this -> get("SELECT * FROM  ".$_SESSION['dbref']."_defaults");
		return $response;
	}
	function getADefault( $id )
	{
		$response = $this -> getRow("SELECT * FROM  ".$_SESSION['dbref']."_defaults WHERE id = $id");
		return $response;
	}	
	
	function getObjectName($i) {
		if(isset($_SESSION[strtoupper($_SESSION['modref'])][strtolower($i)."_name"]) && strlen($_SESSION[strtoupper($_SESSION['modref'])][strtolower($i)."_name"])>0) {
			return $_SESSION[strtoupper($_SESSION['modref'])][strtolower($i)."_name"];
		} else {
			switch($i) {
				case "ACTION":
					$sql = "SELECT value FROM assist_".$_SESSION['cc']."_setup WHERE ref = '".$_SESSION['modref']."' AND field = 'action_name'";
					break;
				case "OBJECT":
				default:
					$sql = "SELECT value FROM assist_".$_SESSION['cc']."_setup WHERE ref = '".$_SESSION['modref']."' AND field = 'object_name'";
					break;
			}
			$response = $this->getRow($sql);
			if(!isset($response['value']) || strlen($response['value'])==0) {
				$v = ($i=="ACTION" ? "Action" : "Risk");
				if(!isset($response['value'])) {
					$this->setObjectName(strtolower($i)."_name",$v,"CREATE");
				} else {
					$this->setObjectName(strtolower($i)."_name",$v);
				}
				return $v;
			} else {
				$this->updateObjectName(strtolower($i)."_name",$response['value']);
				return ASSIST_HELPER::decode($response['value']);
			}
		}
	}
	
	function setObjectName($f,$v,$act="UPDATE") {
		$v = ASSIST_HELPER::code($v);
		switch($act) {
		case "CREATE":
			$sql = "INSERT INTO assist_".strtolower($_SESSION['cc'])."_setup SET value = '$v', field = '$f', ref = '".strtoupper($_SESSION['modref'])."', comment='".ucwords(str_replace("_"," ",$f))."'"; 
			$mnr = $this->helper->db_insert($sql);
			break;
		case "UPDATE":
		default:
			$sql = "UPDATE assist_".strtolower($_SESSION['cc'])."_setup SET value = '$v' WHERE field = '$f' AND ref = '".strtoupper($_SESSION['modref'])."'"; 
			$mnr = $this->helper->db_update($sql);
		}
		$this->updateObjectName($f,$v);
		if($mnr>0) {
			return array("ok","Update saved successfully.");
		} else {
			return array("error","An error occurred while trying to save any changes.  Please try again.");
		}
	}
	
	
	function updateObjectName($f,$v) {
		$_SESSION[strtoupper($this->helper->getModRef())][$f] = $v;
	}
	
	function getObjectCode() {
		return $this->helper->getModRef();//$_SESSION['modref'];
	}
	
}
?>