<?php
/**
	* @package 	: Action assurance
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 , Ignite Assist 
**/
class ActionAssurance extends DBConnect
{
	/**
		@var int
	**/
	protected $id;
	/**
		@var int
	**/
	protected $action_id;
	/**
		@var date
	**/
	protected $date_tested;
	/**
		@var char
	**/
	protected $response;
	/**
		@var int
	**/
	protected $signoff;
	/**
		@var char
	**/
	protected $assurance;
	/**
		@var int
	**/
	protected $attachment;
	/**
		@var char
	**/
	protected $active;
	
	function __construct( $action_id, $date_tested, $response, $signoff, $assurance, $attachment, $active )
	{
		$this -> action_id 			= $action_id;
		$this -> date_tested 		= $date_tested;			
		$this -> active 			= $active;
		$this -> response 			= $response;
		$this -> signoff 			= $signoff;			
		$this -> assurance			= $assurance;
		$this -> attachment 		= $attachment;						
		parent::__construct();
	}
	
	function getActionAssurance(){
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_action_assurance WHERE action_id= ".$this -> action_id." " );
		return $response;
	}
	
	function getActionAssurances($id, $start, $limit)
	{
		$result = $this -> get("SELECT 
								   A.id,
								   CONCAT(TK.tkname,' ', TK.tksurname) AS assurance_provider,
								   A.response,
								   DP.value AS department,
								   A.date_tested,
								   A.signoff, 
								   A.attachment				   
								   FROM ".$_SESSION['dbref']."_action_assurance A
								   INNER JOIN  assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
								   INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   WHERE action_id = '".$this -> action_id."'
								   AND status & 2 <> 2  
								   ORDER BY A.insertdate DESC LIMIT $start,$limit");
		$actionAssuranceArr = array();
		$actionAttachments  = array();
		foreach( $result as $i => $valArr)
		{
			$a = "";
			foreach( $valArr as $index => $arr)
			{
				if( $index == "attachment")
				{
					if( !empty($arr))
					{
						$attachment = unserialize($arr);
						foreach( $attachment as $key => $val)
						{
							 $id  =  $key;
							 $ext = substr( $val , strpos($val, ".")+1);
							 $file = $key.".".$ext;
							 if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$file)) { 							 
							 	$a .= "<a href='download.php?folder=risk&file=".$file."&new=".$val."&content=".$ext."&company=".$_SESSION['cc']."&modref=".$_SESSION['modref']."'>".$val."</a><br />";
							 	$actionAttachments[$valArr['id']][$key] = $val;
							 }															
						}
					} 
					if( $a != ""){
						$actionAssuranceArr[$i][$index] = ltrim($a, ",");
					} else {
						$actionAssuranceArr[$i][$index] = "";
					}	 
				} else {
					$actionAssuranceArr[$i][$index] = $arr;
				}
			}			
		}
		return array( "data" => $actionAssuranceArr, "attachment" => $actionAttachments ,"company" => $_SESSION['cc'],  "total" => $this->getTotalActionAssurances() ); 
	}
	
	function getAActionAssurance( $id )
	{
		$result = $this->getRow("SELECT id, response, attachment FROM #_action_assurance WHERE id = {$id}");
		return $result;	
	}
	
	function getTotalActionAssurances()
	{
	
		$result = $this -> getRow("SELECT 
									COUNT(*) AS total
								   FROM ".$_SESSION['dbref']."_action_assurance A
								   INNER JOIN  assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
								   INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   WHERE action_id = '".$this -> action_id."'");
		return $result['total'];
	}
		
	
	function saveActionAssurance()
	{
		
		$insert_data = array(
						'action_id' 		=> $this -> action_id,
						'date_tested'		=> $this -> date_tested ,			
						'response'  		=> $this -> response,
						'signoff' 			=> $this -> signoff,
						'attachment' 		=> $this -> attachment,
						"insertuser"    	=> $_SESSION['tid'],						
		);
		$response = $this -> insert( 'action_assurance', $insert_data );
		return $response;
	}
	
	function updateActionAssurance( $id)
	{	
		$insert_data = array(
						'date_tested'		=> $this -> date_tested ,			
						'response'  		=> $this -> response,
						'signoff' 			=> $this -> signoff,
						'attachment' 		=> $this -> attachment,
						"insertuser"    	=> $_SESSION['tid'],						
		);
		$response = $this -> update( 'action_assurance', $insert_data, "id=$id" );
		return $response;
	}	
	
	
	function deleteActionAssurance( $id ){
		$res = $this->update("action_assurance", array("status" => 2 ), "id=$id");
		return $res;
	}
}
?>