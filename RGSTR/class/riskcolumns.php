<?php
class RiskColumns extends Naming
{

    public function __construct($section = "")
    {
        parent::__construct($section);
    }

	function getNaming( $options = "")
	{
        if(!empty($options))
        {
           $this->option_sql = $options;
        }
		$results = $this->get("SELECT id, name , ignite_terminology, client_terminology, active 
							   FROM #_header_names 
							   WHERE type = 'risk' $this->option_sql
							   ORDER BY ordernumber 
							 ");
		return $results;
	}
	
	
}