<?php
$scripts = array( 'inherent_risk.js','menu.js', 'jscolor.js', 'functions.js' );
$styles = array();
$page_title = "Set Up Risk Categories";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="inherent_risk_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="inherent-risk-form" name="inherent-risk-form">
			<table border="1" id="inherent_risk_table">
			  <tr>
			    <th>Ref</th>
			    <th>Risk Rating</th>
			    <th>Inherent risk magnitude</th>
			    <th>Response</th>    
			    <th>Color assigned to risk rating</th>
			    <th></th>
			    <th>Status</th>    
			    <th></th>     
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			    	<input type="text" name="inherent_rating_from" id="inherent_rating_from" size="5" />
			         to
			         <input type="text" name="inherent_rating_to" id="inherent_rating_to" size="5" /></td>
			    <td><input type="text" name="inherent_risk_magnitude" id="inherent_risk_magnitude" /></td>
			    <td><input type="text" name="inherent_response" id="inherent_response" /></td>
			    <td><input type="text" name="inherent_color" id="inherent_color" class="color" value="fda0f7" /></td>
			    <td><input type="submit" name="add_inherent_risk" id="add_inherent_risk" value="Add" /></td>
			    <td></td>
			    <td></td>    
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "inherent_exposure_logs" , true); ?></td>
	</tr>
</table>
</div>

