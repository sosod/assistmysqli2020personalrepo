<?php
/**
	* @package 		: Risk 
	* @author 		: admire<azinamo@gmail.com>
	* @copyright	: 2011 Ignite Assist
**/
class Risk extends DBConnect
{
	/**
		@ int	 	 	 	 	 	 	
	**/
	protected $id;
	/**
		@int
	**/
	protected $item;
		/**
		@int
	**/
	protected $type;
		/**
		@int
	**/
	protected $level;
	/**
		@int
	**/
	protected $financial_exposure;
	/**
		@int
	**/
	protected $actual_financial_exposure;		
	/**
		@int
	**/
	protected $kpi_ref;			
	/**
		@int
	**/
	protected $category;
	/**
		@char
	**/
	protected $description;
	/**
		@char
	**/
	protected $background;
	/**
		@int
	**/
	protected $impact;
	/**
		@int
	**/
	protected $impact_rating;
	/**
		@int
	**/
	protected $likelihood;
	/**
		@int
	**/
	protected $likelihood_rating;
	/**
		@text
	**/
	protected $inherent_risk_exposure;
	/**
		@text
	**/
	protected $inherent_risk_rating;
	/**
		@char
	**/
	protected $current_controls;
	/**
		@int
	**/
	protected $percieved_control_effectiveness;
	/**
		@int
	**/
	protected $control_effectiveness_rating;
	/**
		@char
	**/
	protected $residual_risk;
	/**
		@char
	**/
	protected $residual_risk_exposure;
	/**
		@char
	**/
	protected $risk_owner;
	/**
		@char
	**/
	protected $actions_to_improve;
	/**
		@char
	**/
	protected $action_owner;
	/**
		@char
	**/
	protected $time_scale;
	/**
		@char
	**/
	protected $status;
	/**
		@char
	**/
	protected $udf;
	/**
		@char
	**/
	protected $attachement;
	/**
		@char
	**/
	protected $insertdate;
	
	protected $start;
	
	protected $limit;
	/**
	 Action ids which are associated to a user
	 @array
	**/
	protected $actionIds = array();
	
	const ACTIVATED = 1;

    const REFTAG    = "R";
	
	function __construct( 
						$type="", $category="",  $description="", $background="", $impact="", $impact_rating="", $likelihood="", 
						$likelihood_rating="", $current_controls="", $percieved_control_effectiveness="",
						$control_effectiveness_rating="", $risk_owner="" , $attachement=""
		  )
	{
		//$this -> item 								= $item;
		$this -> type   							= $type;
		$this -> category 							= $category;
		$this -> background 						= $background;		
		$this -> description 						= $description;
		$this -> impact 							= $impact;
		$this -> impact_rating 						= $impact_rating;
		$this -> likelihood 						= $likelihood; 
		$this -> likelihood_rating 					= $likelihood_rating;
		//$this -> inherent_risk_exposure 			= $inherent_risk_exposure;
		//$this -> inherent_risk_rating 			= $inherent_risk_rating;
		$this -> current_controls 					= $current_controls; 
		$this -> percieved_control_effectiveness 	= $percieved_control_effectiveness;
		$this -> control_effectiveness_rating 		= $control_effectiveness_rating;
		//$this -> residual_risk 						= $residual_risk;
		//$this -> residual_risk_exposure 			= $residual_risk_exposure;
		$this -> risk_owner 						= $risk_owner;	
		//$this -> actions_to_improve 				= $actions_to_improve;
		//$this -> action_owner 						= $action_owner;
		//$this -> time_scale 						= $time_scale;
	     //$this -> status 							= $status;
 		$this -> attachement						=  $attachement;	
		$this->level								= (isset($_REQUEST['level']) ? $_REQUEST['level'] : "");		
		$this->financial_exposure					= (isset($_REQUEST['financial_exposure']) ? $_REQUEST['financial_exposure']: "");			
		$this->actual_financial_exposure			= (isset($_REQUEST['actual_financial_exposure']) ? $_REQUEST['actual_financial_exposure'] : "");
		$this->kpi_ref								= (isset($_REQUEST['kpi_ref']) ? $_REQUEST['kpi_ref'] : "");

		$inherent = ($this -> impact_rating * $this -> likelihood_rating);
		$residual = (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$this->inherent_risk_rating = $inherent;
		$this->residual_risk_rating = $residual;

		parent::__construct();
	}
	




    //trying not to break whats available
    /* ---------------------------------------- NEW CODE EASIER TO WORK WITH --------------------------------------*/

    /* gets a single risk */
    public function getRiskDetail($risk_id)
    {
        $result = $this->getRow("SELECT DISTINCT(RR.id), RR.*, FE.name AS risk_financial_exposure, RLevel.name AS risk_level, RC.name as risk_category,
                                 RCE.effectiveness AS control_rating, IM.assessment as risk_impact, RL.assessment as risk_likelihood, RT.name as risk_type,
                                 CONCAT(D.dirtxt, ' - ', DS.subtxt) AS risk_owner, IF(LENGTH(RS.client_terminology)>0, RS.client_terminology, RS.name) AS risk_status, RR.financial_year_id, MF.value AS financial_year, RR.rbap_ref,
                                 RR.cause_of_risk, RR.reasoning_for_mitigation, RR.percieved_control_effectiveness AS effectiveness
                                 FROM ".$_SESSION['dbref']."_risk_register RR
                                 INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
                                 INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
                                 INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
                                 INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
                                 INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
                                 LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status
                                 LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RR.level
                                 LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
                                 LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
                                 LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
                                 LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                 LEFT JOIN assist_".$_SESSION['cc']."_master_financialyears MF ON MF.id = RR.financial_year_id
                                 WHERE RR.active <> 0 AND RR.id = ".$risk_id." ");
        return $result;
    }

    /* gets all the risks */
    public function fetchAll($option_sql = '')
    {
		$sql = "SELECT DISTINCT(RR.id), RR.description AS risk_description, RR.background AS risk_background, RR.impact_rating AS IMrating,
							  RR.likelihood_rating AS LKrating, RR.control_effectiveness_rating AS controlRating , RR.current_controls,
							  RR.time_scale , RR.inherent_risk_exposure, RR.inherent_risk_rating, RR.residual_risk, RR.residual_risk_exposure,
							  RR.kpi_ref, RR.actual_financial_exposure, RR.rbap_ref, RR.cause_of_risk, RR.reasoning_for_mitigation 
							  , RT.name as risk_type, RT.shortcode as type_code, RT.description as risk_type_description
							  , RLevel.name AS risk_level, RLevel.description as risk_level_description
							  , RC.name as risk_category, RC.description as cat_descr
							  , IM.assessment as impact, IM.definition as impact_descr ,IM.color AS impact_rating
							  , FE.name AS financial_exposure, FE.definition as fin_exp_description
							  , RCE.effectiveness AS percieved_control_effective, RCE.color AS control_rating
							  , RL.assessment as likelihood, RL.color AS likelihood_rating , RL.definition like_descr
						 	  , CONCAT(D.dirtxt, ' - ', DS.subtxt) AS risk_owner, DS.subdirid
							  , IF(LENGTH(RS.client_terminology)>0, RS.client_terminology, RS.name) AS risk_status ,RS.color AS risk_status_color
						 	  , MF.value AS financial_year
                              FROM ".$_SESSION['dbref']."_risk_register RR
                              INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
                              INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
                              INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
                              INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
                              INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
                              LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status
                              LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RR.level
                              LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
                              LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
                              LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
                              LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                              LEFT JOIN assist_".$_SESSION['cc']."_master_financialyears MF ON MF.id = RR.financial_year_id
                              WHERE RR.active = 1  $option_sql
							  
 							"; 
       $results = $this->get($sql);
/* //DEVELOPMENT PURPOSES!!!
ob_start();
echo "<html><body><h3>SQL</h3><p>".$sql."<h3>Response</h3><pre>"; print_r($results); echo "</pre></body></html>";
$echo = ob_get_contents();
ob_end_clean();



$filename = "fetchAll_".date("YmdHis").".html";

			$file = fopen($filename,"w");
			fwrite($file,$echo."\n");
			fclose($file);					  
					  
					  
//*/	   
        return $results;
    }

    public function getTotalRisks($option_sql = '')
    {
        $result = $this->getRow("SELECT COUNT(DISTINCT(RR.id)) AS total
                              FROM ".$_SESSION['dbref']."_risk_register RR
                              INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
                              INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
                              INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
                              INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
                              INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
                              LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status
                              LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RR.level
                              LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
                              LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
                              LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
                              LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                              LEFT JOIN assist_".$_SESSION['cc']."_master_financialyears MF ON MF.id = RR.financial_year_id
                              WHERE RR.active = 1 $option_sql
							");
        return $result['total'];
    }

    private function _getViewAccess($user_access, $options, $user_risk_ref)
    {
        $viewType        = (isset($options['view']) ? $options['view'] : "viewMyn");
        $view_access_sql = "";
        if($viewType == "viewMyn")
        {
            $view_access_sql  .= " AND DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB'";
            if(isset($user_risk_ref) && strlen($user_risk_ref) > 0) {
				$view_access_sql .= " AND DA.ref IN(".$user_risk_ref.")";
			}
            //$view_access_sql = " AND (DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB' ";
/*            if(!empty($user_access))
            {
                $sub_sql = "";
                $page    = (isset($options['page']) ? $options['page'] : "");
                if(isset($user_access[$page]))
                {
                    foreach($user_access[$page] as $index => $val)
                    {
                        $sub_sql .= " RR.sub_id = ".$val." OR";
                    }
                    if(!empty($sub_sql))
                    {
                        $view_access_sql .= " AND (".rtrim($sub_sql, "OR").") ";
                    }
                } else {
                    $view_access_sql .= " AND RR.id = 0";
                }
            } else {
                $view_access_sql .= " AND RR.id = 0";
            }*/
        }
/*        if(isset($options['section']))
        {
            if($options['section'] == "new")
            {
                $view_access_sql = " AND RR.active & ".Risk::ACTIVATED." <> ".Risk::ACTIVATED." ";
            } else {
                $view_access_sql .= " AND RR.active & ".Risk::ACTIVATED." = ".Risk::ACTIVATED." ";
            }
        }*/
        return $view_access_sql;
    }


    public function getRiskActions($options = array())
    {
        $userAccessObj  = new UserAccess( "", "", "", "", "", "", "", "", "" );
        $userAccess     = $userAccessObj-> getUser( $_SESSION['tid'] );

        $user_risk_ref  = $userAccessObj -> getUserRefDirectorate();
        $user_access    = array();

        $option_sql     = $this -> _getViewAccess($user_access, $options, $user_risk_ref);

        $total          = $this->getTotalRisks($option_sql);
		$option_sql.= " ORDER BY RR.id ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $option_sql .= " LIMIT ".$options['start'].",".$options['limit']." ";
        }
        $risks           = $this->fetchAll($option_sql);
        $data            = $this->_sortRisk($risks, $total, $options, $userAccess, $user_risk_ref);
        return $data;
    }

    private function _sortRisk($risks, $total, $options, $userAccess, $risk_str = '')
    {
        $subdirs          = explode(',', $risk_str);
        $columnObj      = new RiskColumns($options['section']);
        $columns        =  $columnObj->getHeaderList();
        $colored  		 = array(
            "control_rating" 	 => "controlRating",
            "impact_rating" 	 => "IMrating",
            "likelihood_rating"  => "LKrating",
        );
		$descriptions = array(
			'risk_type'			=>	"risk_type_description",
			'risk_level'		=>	"risk_level_description",
			'risk_category'		=>	"cat_descr",
			'financial_exposure'	=>	"fin_exp_description",
			'impact'			=>	"impact_descr",
			'likelihood'			=>	"like_descr",
		);
        $default  		= new Defaults("","");
        $defaults 		= $default -> getDefaults();

		$action_name = $default->getObjectName("ACTION");
		if(isset($options['show_actions']) && $options['show_actions']==1) {
			$columns['totalActions'] = "Total ".$action_name."s";
		}

        $res 			= new ResidualRisk( "", "", "", "", "");
        $ihr 			= new InherentRisk("", "", "", "", "");
        $user_access     = array();
        $actionObj       = new RiskAction('', '', '', '', '', '', '', '', '');
        $risk_actions       = $actionObj->fetchAll();
        $data['headers']      = array();
        $data['risks']      = array();
        $data['total']        = $total;
        $data['actions']      = array();
        $data['canEdit']      = array();
        $data['canUpdate']    = array();
        $data['canAddAction'] = array();
        $data['isDirAdmin']   = array();


        foreach($risks as $index => $risk)
        {
            $totalProgress  = 0;
            $totalActions   = 0;
            $riskProgress  = 0;
            if(!empty($risk_actions))
            {
                $actionsProgress = 0;
                foreach($risk_actions as $action_id => $action)
                {
                    if($action['risk_id'] == $risk['id'])
                    {
                        $data['actions'][$risk['id']][$action_id] = $action;
                        $totalActions                             += 1;
                        $actionsProgress                          += $action['progress'];
                    }
                }
                if($totalActions != 0)
                {
                    $riskProgress = ($actionsProgress / $riskProgress);
                }
                $riskProgress  = ASSIST_HELPER::format_percent($riskProgress);
            }
            if(isset($user_access['update']))
            {
                if(isset($user_access['update'][$risk['sub_id']]))
                {
                    $data['canUpdate'][$risk['id']] = TRUE;
                } else {
                    if(isset($options['section']) == "admin")
                    {
                        $data['canUpdate'][$risk['id']] = TRUE;
                    } else {
                        $data['canUpdate'][$risk['id']] = FALSE;
                    }
                }
            }
            if(isset($user_access['edit']))
            {
                if(isset($user_access['edit'][$risk['sub_id']]))
                {
                    $data['canEdit'][$risk['id']] = TRUE;
                } else {
                    if(isset($options['section']) == "admin" && $userAccess['edit_all'] == 1)
                    {
                        $data['canEdit'][$risk['id']] = TRUE;
                    } else {
                        $data['canEdit'][$risk['id']] = FALSE;
                    }
                }
            }
            if(isset($user_access['addaction']))
            {
                if(isset($user_access['addaction'][$risk['sub_id']]))
                {
                    $data['canAddAction'][$risk['id']] = TRUE;
                } else {
                    if(isset($options['section']) == "admin" && $userAccess['edit_all'] == 1)
                    {
                        $data['canAddAction'][$risk['id']] = TRUE;
                    } else {
                        $data['canAddAction'][$risk['id']] = FALSE;
                    }
                }
            }
            if(in_array($risk['subdirid'], $subdirs))
            {
                $data['isDirAdmin'][$risk['id']] = TRUE;
            }

            if($options['section'] == 'admin' || $options['section'] == 'manage')
            {
                $data['canUpdate'][$risk['id']] = TRUE;
                $data['canEdit'][$risk['id']]   = TRUE;
            }

            foreach($columns as $field => $value)
            {
                if($field == "totalActions") {
                    $data['risks'][$risk['id']][$field] = $totalActions." ".$action_name.($totalActions==1 ? "" : "s");
                    $data['headers'][$field]            = $value;
                } elseif($field == "action_progress") {
                    $data['risks'][$risk['id']][$field] = $riskProgress;
                    $data['headers'][$field]            = $value;
                } elseif($field == "attachements") {
                    $attachment                            = "";
                    $attachment                            = Attachment::getAtachmentsList($risk['attachements'], 'risk', FALSE);
                    $data['risks'][$risk['id']][$field] = $attachment;
                    $data['headers'][$field]               = $value;
                } elseif($field == "risk_item") {
                    $data['risks'][$risk['id']][$field] = Risk::REFTAG."".$risk['id'];
                    $data['headers'][$field]               = $value;
                } elseif($field == 'residual_risk_exposure') {
                    $data['headers'][$field]            = $value;
                    $riskExposureArr = $res -> getRiskResidualRanges( ceil($risk[$field]) );
                    $color		= "";
                    $magnitude  = "";
					$descrip = "";
                    if(isset($riskExposureArr) && !empty($riskExposureArr))
                    {
                        $color 		  = $riskExposureArr['color'];
                        $magnitude 	  = $riskExposureArr['magnitude'];
						$descrip	  = $riskExposureArr['response'];
                    }
					if(trim($descrip)!="" && trim(strtolower($descrip))!=trim(strtolower($magnitude))) {
						$rkey = $risk['id']."_".$field;
						$data['risks'][$risk['id']][$field] = "<span style='width:50px; padding: 5px; text-align:center; background-color:#".$color."'>&nbsp;".$magnitude."&nbsp;</span>&nbsp;<img src=\"/pics/plus.gif\" class=view_text status=plus style='cursor:pointer;' onclick=\"showMore('".$rkey."')\" id='".$rkey."' /><span id='ref_".$rkey."' class='view_text hidden'><br />[".$descrip."]</span>";
					} else {
						$data['risks'][$risk['id']][$field] = "<span style='width:50px; padding: 5px; text-align:center; background-color:#".$color."'>&nbsp;".$magnitude."&nbsp;</span>";
					}
                } elseif($field == 'inherent_risk_exposure') {
                    $inherentResults  = $ihr -> getRiskInherentRange( round($risk[$field]) );
                    $color 			  = "";
                    $magnitude 	 	  = "";
					$descrip = "";
                    if(isset($inherentResults) && !empty($inherentResults)){
                        $color 			= $inherentResults['color'];
                        $magnitude 	 	= $inherentResults['magnitude'];
						$descrip	    = $inherentResults['response'];
                    }
                    $data['headers'][$field]            =   $value;
//                    $data['risks'][$risk['id']][$field] =  "<span style='width:50px; padding: 5px; text-align:center; background-color:#".$color."'>".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					if(trim($descrip)!="" && trim(strtolower($descrip))!=trim(strtolower($magnitude))) {
						$rkey = $risk['id']."_".$field;
						$data['risks'][$risk['id']][$field] = "<span style='width:50px; padding: 5px; text-align:center; background-color:#".$color."'>&nbsp;".$magnitude."&nbsp;</span>&nbsp;<img src=\"/pics/plus.gif\" class=view_text status=plus style='cursor:pointer' onclick=\"showMore('".$rkey."')\" id='".$rkey."' /><span id='ref_".$rkey."' class='view_text hidden'><br />[".$descrip."]</span>";
					} else {
						$data['risks'][$risk['id']][$field] = "<span style='width:50px; padding: 5px; text-align:center; background-color:#".$color."'>&nbsp;".$magnitude."&nbsp;</span>";
					}
                } elseif( in_array($field, array_keys($colored)) ) {
                    $data['headers'][$field]            =   $value;
                    $data['risks'][$risk['id']][$field] = "<span style='width:50px; padding: 5px; text-align:center; background-color:#".$risk[$field]."'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."</span>";
				} elseif( isset($descriptions[$field])) {
					$data['headers'][$field] = $value;
					$rkey = $risk['id']."_".$field;
					$d = $risk[$descriptions[$field]];
					if(trim($d)!="" && trim(strtolower($d))!=trim(strtolower($risk[$field])) ) {
						$data['risks'][$risk['id']][$field] = $risk[$field]." <img src=\"/pics/plus.gif\" class=view_text status=plus style='cursor:pointer' onclick=\"showMore('".$rkey."')\" id='".$rkey."' /><span id='ref_".$rkey."' class='view_text hidden'><br />[".$risk[$descriptions[$field]]."]</span>";
					} else {
						$data['risks'][$risk['id']][$field] = $risk[$field];
					}
                } elseif($field == "risk_description") {
                    if(Util::isTextTooLong($risk['risk_description']))
                    {
                        $text = Util::cutString($risk['risk_description'], $field."_".$risk['id']);
                        $data['risks'][$risk['id']][$field] = $risk['risk_description'];
                        $data['headers'][$field]             = $value;
                    } else {
                        $data['risks'][$risk['id']][$field] = $risk['risk_description'];
                        $data['headers'][$field]               = $value;
                    }

                } elseif(isset($risk[$field])) {
                    if(Util::isTextTooLong($risk[$field]))
                    {
                        $text = Util::cutString($risk[$field], $field."_".$risk['id']);
                        $data['risks'][$risk['id']][$field] = $text;
                        $data['headers'][$field]               = $value;
                    } else {
                        if(($field == 'financial_year') && empty($risk[$field]))
                        {
                            $data['risks'][$risk['id']][$field] = 'Unspecified';
                            $data['headers'][$field]            = $value;
                        } else {
                            $data['risks'][$risk['id']][$field] = $risk[$field];
                            $data['headers'][$field]               = $value;
                        }
                    }
                }
            }
        }

        if(empty($data['headers']))
        {
            $data['headers'] = $columns;
        }
        if(empty($data['risks']))
        {
            $msg = "";
            if(isset($options['risk_id']) && !empty($options['query_id']))
            {
                $msg = "There are no Risks that matched the entered criteria";
            } else {
                if(isset($options['page']))
                {
                    if($options['page'] == "edit")
                    {
                        $msg = "You do not have access to edit any Risks";
                    } elseif($options['page'] == "update") {
                        $msg = "You do not have any outstanding Risks to update";
                    } else {
                        $msg = "There are no Risks that matched the selected criteria";
                    }
                } else {
                    $msg = "There are no Risks";
                }
            }
            $data['risk_message'] = $msg;
        }
        $data['columns'] = count($data['headers']);
        return $data;
    }


    public function editRisk($risk_id, $update_data, $insert_data)
    {
		$inherent = ($this -> impact_rating * $this -> likelihood_rating);
		$residual = (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$this->inherent_risk_rating = $inherent;
		$this->residual_risk_rating = $residual;
        $res = 0;
        if(!empty($update_data))
        {
            $res += $this -> update('risk_register', $update_data, "id='".$risk_id."'");
        }
        if(!empty($insert_data))
        {
            $res += $this -> insert("risk_edits", $insert_data);
        }
        return $res;
    }

    public function updateRiskAction($risk_id, $update_data, $insert_data)
    {
        $res = 0;
        if(!empty($update_data))
        {
            $res += $this -> update('risk_register', $update_data, "id='".$risk_id."'");
        }
        if(!empty($insert_data))
        {
            $res += $this -> insert("risk_update", $insert_data);
        }
        return $res;
    }

    public function getRiskChange($postData, $riskData)
    {
        $colObj       = new RiskColumns();
        $columns      = $colObj -> getHeaderList();
		
		$nameObj = new Naming();
		$names = $nameObj->getReportingNaming();

        $typeObj      = new RiskType("", "", "" , "");
        $types        = $typeObj->getRiskTypes();

        $levelObj 		= new RiskLevel("", "", "" , "");
        $levels         = $levelObj->getRiskLevels();

        $finExposureObj = new FinancialExposure("", "", "", "" , "");
        $fin_exposures  = $finExposureObj->getFinancialExposure();

        $impactObj 		= new Impact("", "", "" , "", "");
        $impacts        = $impactObj->getImpact();

        $categoryObj    = new RiskCategory("", "", "" , "");
        $categories     = $categoryObj->getCategory();

        $likelihoodObj  = new Likelihood("", "", "" , "", "", "");
        $likelihoods    = $likelihoodObj->getLikelihood();

        $controlEffObj  = new ControlEffectiveness("", "", "" , "", "");
        $control_effectiveness = $controlEffObj->getControlEffectiveness();

        $statusObj      = new RiskStatus("" , "", "");
        $statuses       = $statusObj->getStatus();

        $directorateObj  = new Directorate("" , "", "", "");
        $directorates    = $directorateObj->getDirectorateList();

        $financial_year_obj = new MasterFinancialYear();
        $financial_years    = $financial_year_obj->retrieveMasterFinacialYears();

        $data['changes']        = array();
        $data['change_message'] = "";
        $data['update_data']    = array();
        $msg                    = "";
		$email_msg = array();
        foreach($postData as $field => $val)
        {
			//$data['changes'][$field]=array('from'=>$field,'to'=>$val);
			//$data['changes'][$field." old"]=array('from'=>(isset($riskData[$field]) ? "TRUE" : "FALSE"),'to'=>(isset($riskData[$field]) ? $riskData[$field] : "?"));
		
            if(isset($riskData[$field]) || $field =="response")
            {
                if($field == 'status')
                {
                    $from = (isset($statuses[$riskData['status']-1]) ? $statuses[$riskData['status']-1]['name'] : " - ");
                    $to   = (isset($statuses[$postData['status']-1]) ? $statuses[$postData['status']-1]['name'] : " - ");
                    if($from != $to)
                    {
                        $data['changes'][$field]          = array("from" => $from, "to" => $to);
						$m 									= $names['risk_status']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data'][$field]      = $postData['status'];
                        $data['changes']['currentstatus'] = $to;
                    }
                } elseif($field == 'level'){
                    $from = (isset($levels[$riskData['level']-1]) ? $levels[$riskData['level']-1]['name'] : " - ");
                    $to   = (isset($levels[$postData[$field]-1]) ? $levels[$postData[$field]-1]['name'] : " - ");
                    if($from != $to)
                    {
                        $data['changes']['risk_level']    = array("from" => $from, "to" => $to);
                        $m                          = $names['risk_level']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['level'] = $postData[$field];
                    }
                } elseif($field == 'category'){
                    $from = (isset($categories[$riskData[$field]-1]) ? $categories[$riskData[$field]-1]['name'] : " - ");
                    $to   = (isset($categories[$postData[$field]-1]) ? $categories[$postData[$field]-1]['name'] : " - ");
                    if($from !== $to)
                    {
                        $data['changes']['category']       = array("from" => $from, "to" => $to);
                        $m                              = $names['risk_category']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['category']   = $postData[$field];
                    }
                } elseif($field == 'type'){
                    $from = (isset($types[$riskData[$field]-1]) ? $types[$riskData[$field]-1]['name'] : " - ");
                    $to   = (isset($types[$postData[$field]-1]) ? $types[$postData[$field]-1]['name'] : " - ");
                    if($from != $to)
                    {
                        $data['changes']['type']     = array("from" => $from, "to" => $to);
                        $m                         = $names['risk_type']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['type'] = $postData[$field];
                    }
                } elseif($field == 'likelihood'){
                    $from = (isset($likelihoods[$riskData[$field]-1]) ? $likelihoods[$riskData[$field]-1]['assessment'] : " - ");
                    $to   = (isset($likelihoods[$postData[$field]-1]) ? $likelihoods[$postData[$field]-1]['assessment'] : " - ");
                    if($from != $to)
                    {
                        $data['changes']['likelihood']     = array("from" => $from, "to" => $to);
                        $m                              = $names['likelihood']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['likelihood'] = $postData[$field];
                    }
                } elseif($field == 'financial_year_id') {
                    $from = (isset($financial_years[$riskData[$field]-1]) ? $financial_years[$riskData[$field]-1]['value'] : " - ");
                    $to   = (isset($financial_years[$postData[$field]-1]) ? $financial_years[$postData[$field]-1]['value'] : " - ");
                    if($from != $to)
                    {
                        $data['changes']['financial_year']       = array("from" => $from, "to" => $to);
                        $m                                    = $names['financial_year']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['financial_year_id'] = $postData[$field];
                    }
                } elseif($field == 'impact'){
                    $from = (isset($impacts[$riskData[$field]-1]) ? $impacts[$riskData[$field]-1]['assessment'] : " - ");
                    $to   = (isset($impacts[$postData[$field]-1]) ? $impacts[$postData[$field]-1]['assessment'] : " - ");
					//$from = "TEXT";
					//$to = "ASSESS";
                    if($from != $to)
                    {
                        $data['changes']['impact']     = array("from" => $from, "to" => $to);
                        $m                          = $names['impact']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['impact'] = $postData[$field];
                    }
                } elseif($field == 'effectiveness'){
                    $from = (isset($control_effectiveness[$riskData[$field]-1]) ? $control_effectiveness[$riskData[$field]-1]['effectiveness'] : " - ");
                    $to   = (isset($control_effectiveness[$postData[$field]-1]) ? $control_effectiveness[$postData[$field]-1]['effectiveness'] : " - ");
                    if($from != $to)
                    {
                        $data['changes']['perceived_control_effectiveness']     = array("from" => $from, "to" => $to);
                        $m                                        = $names['percieved_control_effective']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['percieved_control_effectiveness'] = $postData[$field];
                    }
                } elseif($field == 'user_responsible' || $field=="sub_id"){
                    $from = (!empty($riskData['risk_owner']) ? $riskData['risk_owner'] : " - ");
                    $to   = $directorates[$postData[$field]];//$postData['user_responsible_text'];
                    if($from !== $to)
                    {
                        $data['changes']['risk_owner']  = array("from" => $from, "to" => $to);
                        $m                           = $names['risk_owner']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['sub_id']  = $postData[$field];
                    }
                } elseif($field == 'financial_exposure'){
                    $from = (isset($fin_exposures[$riskData[$field]-1]) ? $fin_exposures[$riskData[$field]-1]['name'] : " - ");
                    $to   = (isset($fin_exposures[$postData[$field]-1]) ? $fin_exposures[$postData[$field]-1]['name'] : " - ");
                    if($from !== $to)
                    {
                        $data['changes']['financial_exposure'] = array("from" => $from, "to" => $to);
                        $m                                  = $names['financial_exposure']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['financial_exposure']= $postData[$field];
                    }
                } elseif($field == "current_controls") {
                    $to                = str_replace("_", ", ", $val);
                    $from              = str_replace("_", ", ", $riskData[$field]);
                    if($from !== $to)
                    {
                        $data['changes']['current_controls']     = array("from" => $from, "to" => $to);
                        $m                                    = $names['current_controls']." changed to ".$to." from ".$from;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                        $data['update_data']['current_controls'] = $postData[$field];
                    }
                } elseif($field == 'attachment'){
                    $attachments = array();
                    if( $riskData[$field] != "")
                    {
                        $attachments = unserialize($riskData[$field]);
                    }
                    if( isset($_SESSION['editdeleted']) && !empty($_SESSION['editdeleted']))
                    {
                        if(isset($attachments) && !empty($attachments))
                        {
                            foreach( $_SESSION['editdeleted'] as $k => $val){
                                if( isset($attachments[$k]) && !empty($attachments[$k]))
                                {
                                    unset($attachments[$k]);

                                    $data['changes'][$field]  = "Deleted attachment ".$val;
                                    $m   	                 = "Deleted attachment ".$val;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
                                }
                            }
                            $_SESSION['uploads']['edits'] = $attachments;
                        }
                    }
				} elseif($field=="response") {
					if(strlen($val)>0) {
						$data['changes'][$field] = $val."";
						$m  = "Added Response: ".$val;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
					}
                } else {
                    $to                = (empty($val) ? '-' : $val);
                    $from              = (empty($riskData[$field]) ? '-' : $riskData[$field]);
                    if($from !== $to)
                    {
                        $data['changes'][$field]     = array("from" => $from, "to" => $to);
						if(isset($names[$field])) {
							$n = $names[$field];
						} elseif(isset($names['risk_'.$field])) {
							$n = $names['risk_'.$field];
						} elseif($field=="control_effectiveness_rating") {
							$n = $names['control_rating'];
						} else {
							$n =  ucwords(explode("_",$field));
						}
                        $m                        = $n." changed to ".$to." from ".$from;
                        $msg                             .= $m."\r\n\n";
						$m = str_ireplace("\r\n"," ",$m);
						$m = str_ireplace("\n\r"," ",$m);
						$m = str_ireplace("\r"," ",$m);
						$m = str_ireplace("\n"," ",$m);
						$m = str_ireplace(chr(10)," ",$m);
						$email_msg[]						= $m;
                        $data['update_data'][$field] = $postData[$field];
                    }
                }
            }
        }
		if(isset($data['changes']['impact_rating']) || isset($data['changes']['likelihood_rating'])) {
			$field = "inherent_risk_exposure";
			$val = $postData['impact_rating']*$postData['likelihood_rating'];
			$data['changes'][$field]     = array("from" => $riskData[$field], "to" => $val);
			$m                        = (isset($names[$field]) ? $names[$field] : ucwords($field))." changed to ".$val." from ".$riskData[$field];
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
		}
		if(isset($data['changes']['inherent_risk_exposure']) || isset($data['changes']['control_effectiveness_rating'])) {
			$field = "residual_risk_exposure";
			$val = $postData['impact_rating']*$postData['likelihood_rating']*$postData['control_effectiveness_rating'];
			$data['changes'][$field]     = array("from" => $riskData[$field], "to" => $val);
			$m                        = (isset($names[$field]) ? $names[$field] : ucwords($field))." changed to ".$val." from ".$riskData[$field];
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
		}
        //check the added attachments and add them to the audit log array
        if( isset($_SESSION['uploads']['editadded']) && !empty($_SESSION['uploads']['editadded']))
        {
            $currentAttachments  = array();
            if( $riskData['attachment'] != "" )
            {
                $currentAttachments = unserialize($riskData['attachment']);
            }
            $_SESSION['uploads']['edits'] = array_merge($_SESSION['uploads']['editadded'] , $currentAttachments);
            foreach( $_SESSION['uploads']['editadded'] as $key => $val)
            {
                $data['changes'][$field]  = "Added attachment ".$val;
                $m   	                 = "Added attachment ".$val;
						$email_msg[]						= $m;
                        $msg                             .= $m."\r\n\n";
            }
        }
		//$msg = "TEST";
       $data['change_message'] = $msg;
       $data['email_change_message'] = $email_msg;
       return $data;
    }


    /*-------------------------------------------------END NEW CODE -------------------------------------------------*/

	function  saveRisk()
	{



		$inherent = ($this -> impact_rating * $this -> likelihood_rating);
		$residual = (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$this->inherent_risk_rating = $inherent;
		$this->residual_risk_rating = $residual;
		$insert_data = array(
			'type'   							=> $this -> type,
			'category' 							=> $this -> category,
			'description' 						=> $this -> description,
			 'background'						=> $this -> background ,
			'impact' 							=> $this -> impact,
			'inherent_risk_exposure'			=> $inherent,
			'inherent_risk_rating'				=> $inherent,
			'residual_risk_exposure'			=> $residual,
			'residual_risk'						=> $residual,						
			'impact_rating' 					=> $this -> impact_rating,
			'likelihood' 						=> $this -> likelihood,
			'likelihood_rating' 				=> $this -> likelihood_rating,
			'current_controls' 					=> $this -> current_controls,
			'percieved_control_effectiveness' 	=> $this -> percieved_control_effectiveness,
			'control_effectiveness_rating' 		=> $this -> control_effectiveness_rating,
			'level' 							=> $this -> level,		
			'financial_exposure'				=> $this -> financial_exposure,		
			'actual_financial_exposure'			=> $this -> actual_financial_exposure,		
			'kpi_ref' 							=> $this -> kpi_ref,												
			'attachment' 						=> $this -> attachement	,		
			'sub_id' 							=> $this -> risk_owner,
			'insertuser'						=> $_SESSION['tid']		
		);

        $insert_data['financial_year_id']        = (isset($_REQUEST['financial_year_id']) ? $_REQUEST['financial_year_id'] : '');
        $insert_data['rbap_ref']                = (isset($_REQUEST['rbap_ref']) ? $_REQUEST['rbap_ref'] : '');
        $insert_data['cause_of_risk']           = (isset($_REQUEST['cause_of_risk']) ? $_REQUEST['cause_of_risk'] : '');
        $insert_data['reasoning_for_mitigation']= (isset($_REQUEST['reasoning_for_mitigation']) ? $_REQUEST['reasoning_for_mitigation'] : '');
		$response  = $this -> insert('risk_register' , $insert_data);
		return $response;//$this -> insertedId();
	}
	
	function  saveRiskUpdate( $id , $status, $response, $changes)
	{
		$inherent = ($this -> impact_rating * $this -> likelihood_rating);
		$residual = (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$insertdata = array(
						'impact' 							=> $this -> impact,
						'impact_rating' 					=> $this -> impact_rating,
						'likelihood' 						=> $this -> likelihood,
						'likelihood_rating' 				=> $this -> likelihood_rating,
						'current_controls' 					=> $this -> current_controls,
						'percieved_control_effectiveness' 	=> $this -> percieved_control_effectiveness,
						'control_effectiveness_rating' 		=> $this -> control_effectiveness_rating,
						'inherent_risk_exposure'			=> $inherent,
						'inherent_risk_rating'				=> $inherent,
						'residual_risk_exposure'			=> $residual,
						'residual_risk'						=> $residual,	
						'status'							=> $status,
						'attachment' 						=> $this -> attachement,
						'insertuser'						=> $_SESSION['tid'],						
		);
		$responseUpdate  = $this -> update( 'risk_register' , $insertdata, "id = $id");
		$response  		 = $this -> insert( 'risk_update' , array_merge($insertdata, array(
															"risk_id" 			=> $id,
															"response"			=> $response,
															"changes"			=> $changes
														) 
											));
		return $responseUpdate;//$this -> insertedId();
	}
	
	function getRiskUpdate( $id )
	{  								  
		$response = $this -> getRow( "SELECT status, name, color FROM ".$_SESSION['dbref']."_risk_update RU
									  INNER JOIN ".$_SESSION['dbref']."_status S ON RU.status = S.id 
									  WHERE RU.risk_id = '".$id."' " );
		return $response;
	}
	
	function getAllRisk( $start , $limit)
	{
		$sqlLimit = "";
		if( $start !== "" && $limit !== "") {
			$sqlLimit = "LIMIT $start , $limit";
		} else {
			$sqlLimit = "";
		}
		$uaccess 		= new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess     = $uaccess -> getUser( $_SESSION['tid'] );
		$userRiskRef     = $uaccess -> getUserRefDirectorate();
		$viewAccess 	= $this -> _setViewAccess( $userAccess , (isset($_GET['page']) ? $_GET['page'] : "") , $userRiskRef );
		$response = $this -> get( "
							SELECT DISTINCT(RR.id) AS risk_item,
							RR.description AS risk_description,
							RR.background AS risk_background,
							RR.impact_rating AS IMrating,
							RR.likelihood_rating AS LKrating, 
							RR.control_effectiveness_rating AS control_rating , 
							RR.current_controls,
							RR.time_scale , 
							RR.inherent_risk_exposure,
							RR.inherent_risk_rating,
							RR.residual_risk,
							RR.residual_risk_exposure,
							RR.kpi_ref,
							RR.actual_financial_exposure,
							FE.name AS financial_exposure,							
							RLevel.name AS risk_level, 							
							RC.name as risk_category, 
							RC.description as cat_descr,
							RCE.effectiveness AS CtrlRating,
							RCE.color AS percieved_control_effective , 								
							IM.assessment as impact,
							IM.definition as impact_descr ,
							IM.color AS impact_rating,
							RL.assessment as likelihood,
							RL.color AS likelihood_rating ,
							RL.definition like_descr,
							RT.name as type,
							RT.shortcode as type_code, 
							CONCAT(D.dirtxt, ' - ', DS.subtxt) AS risk_owner,
							RS.name AS risk_status ,
							RS.color AS risk_status
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE RR.active = 1 $viewAccess ORDER BY RR.id asc $sqlLimit
							");
							
		$totalrisk = $this -> totalRisk( $viewAccess  );
		$nm 			= new Naming();
		$default  		= new Defaults("","");
		$defaults 		= $default -> getDefaults();
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");		
		$data = array();
		$riskArray = array();
		foreach( $response as $key => $risk )
		{
			$updateArray 	= $this -> getRiskUpdate( $risk['risk_item'] );		
			$riskArray[$risk['risk_item']] = array(
							"description"		=> $risk['risk_description'],
							"background"		=> $risk['risk_background'],
							"impactRating"		=> $risk['IMrating'],
							"likelihoodRating"	=> $risk['LKrating'],
							"ctrEffectRating"	=> $risk['control_rating'],
							"currControls"		=> $risk['current_controls'],
							"timeScale"			=> $risk['time_scale'],
							"inhRiskExposure"	=> $residualRanges = $res -> getRiskResidualRanges( $risk['inherent_risk_exposure'] ),
							"inhRiskRating"		=> $risk['inherent_risk_exposure'],
							"residualRisk"		=> $risk['residual_risk'],
							"resRiskExposure"	=> $ihr -> getRiskInherentRange( round( $risk['residual_risk_exposure'] ) ),
							"category"			=> $risk['risk_category'],
							"catDescr"			=> $risk['cat_descr'],
							"effectiveness"		=> $risk['CtrlRating'],
							"effectColor"		=> $risk['percieved_control_effective'],
							"impact"			=> $risk['impact'],
							"impactDescr"		=> $risk['impact_descr'],
							"impactColor"		=> $risk['impact_rating'],
							"likelihood"		=> $risk['likelihood'],	
							"likeColor"			=> $risk['likelihood_rating'],
							"likeDescr"			=> $risk['like_descr'],
							"type"				=> $risk['type'],
							"tkname"			=> $risk['risk_owner'],
							"status"			=> (empty($updateArray) ? "New" : $updateArray['name']),
							"rscolor"			=> (empty($updateArray) ? "" : $updateArray['color']),										
						);

			}
		$data = array( "total" => $totalrisk, "riskData" => $riskArray, "headers" => $nm -> getNameColumn() );
		return $data;
	}
	
	function getRiskEdits( $id )
	{
	
		$response = $this -> get(  "
							SELECT							
							RR.id AS risk_item,
							RR.changes,
							RR.description AS risk_description,
							RR.background AS risk_background,
							RR.impact_rating AS IMrating,
							RR.likelihood_rating AS LKrating, 
							RR.control_effectiveness_rating AS control_rating , 
							RR.current_controls,
							RR.time_scale , 
							RR.inherent_risk_exposure,
							RR.inherent_risk_rating,
							RR.residual_risk,
							RReg.residual_risk_exposure,
							RReg.kpi_ref,
							RReg.actual_financial_exposure,
							FE.name AS financial_exposure,							
							RLevel.name AS risk_level, 								
							RC.name as risk_category, 
							RC.description as cat_descr,
							RCE.effectiveness AS CtrlRating,
							RCE.color AS percieved_control_effective , 								
							IM.assessment as impact,
							IM.definition as impact_descr ,
							IM.color AS impact_rating,
							RL.assessment as likelihood,
							RL.color AS likelihood_rating ,
							RL.definition like_descr,
							RT.name as type,
							RT.shortcode as type_code, 
							RR.risk_owner,
							UIA.tkname AS risk_owner,
							UIA.tksurname,
							RS.name AS risk_status ,
							RS.color AS status_color,
							RR.insertdate,
							RR.insertuser ,
							UIA.tkname AS username,
							UIA.tksurname AS surname 							
							FROM ".$_SESSION['dbref']."_risk_edits RR 
							INNER JOIN ".$_SESSION['dbref']."_risk_register RReg ON RReg.id = RR.risk_id
							INNER JOIN ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RReg.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RReg.financial_exposure	
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RReg.status							
							LEFT JOIN assist_".$_SESSION['cc']."_timekeep UIA ON UIA.tkid = RR.insertuser
							WHERE RR.risk_id = '".$id."' ORDER BY RR.insertdate DESC 
							");

		return $response;
	}
	/**
		Fetches a specified risk id 
		@param risk id
		@return boolelan of ids
	**/
	function _isRiskOwner( $id , $riskid)
	{
		$response = $this -> getRow("SELECT id 
								  FROM ".$_SESSION['dbref']."_risk_register 
								  WHERE risk_owner = '".$id."' AND id = '".$riskid."' AND active = 1 "
								  ); 
		if( isset($response['id']) && !empty($response['id'])) {
			//echo "This risk id ".$riskid." the person is the owner \r\n\n\n";
			return TRUE;
		} else {
			//echo "This risk id ".$riskid." the person is the action owner only \r\n\n\n";		
			return FALSE;
		}						  
	}
	/**
	 Creates a string of comma separated risk ids the users has actions assoociated to it
	 @param array  , that has an index of risk id
	 @return string
	**/
	function _setRiskIds( $risIdkArray )
	{
		$riskids = "";
		if( is_array( $risIdkArray )) 
			foreach( $risIdkArray as $key => $valueArray ) 
			{
				$riskids .= "'".$valueArray['risk_id']."',";
			}
		$riskids = rtrim( ltrim( rtrim($riskids, ","), "'") , "'");
		return $riskids;
	}
	/**
		Sets the view options for each used based on the user access settings
		@param , userAccess array , pageRequest page requesting the view
		@return string, sql string
	**/
	function _setViewAccess( $userAccess, $pageRequest, $userRiskRef )
	{
		//$actions 			= new RiskAction("", "", "", "", "", "", "", "", "");		
		//$userAssignedAction = $actions -> userAssignedAction( $userAccess['user'] );
		//$ids 				= $this -> _setRiskIds( $userAssignedAction );
		$viewAccess = "";
		if( $pageRequest == "viewAll"){
			$viewAccess  = "AND 1";
		} else if( $pageRequest == "viewMyn"){
			$viewAccess   = " AND DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB'";
			$viewAccess  .= $userRiskRef >0 ? " AND DA.ref IN(".$userRiskRef.")" : "";
//$viewAccess  .= " AND DA.ref IN()";

		}
        if(isset($_GET['page']) && $_GET['page'] == "update_risks")
        {
            $viewAccess   = " AND DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB'";
            $viewAccess  .= $userRiskRef >0 ? " AND subdirid IN('".$userRiskRef."')" : "";
        }

		return $viewAccess;
	}
	/**
		Fetches all the risk in detail 
		@return array;
	**/
	function getAllRiskTogether( $start , $limit)
	{
		$nm 			= new RiskColumns();
		
		$headers		= array();
		/*if(isset($_GET['headers']) && !empty($_GET['headers'])){
			$headerArr = $nm -> getHeaderList();
			$headersArray = array();
			foreach( $headerArr as $field => $val)
			{
				foreach($_GET['headers'] as $index => $key)
				{
					if( $key == $field){
						$headersArray[$index][$field] = $val;
					}					
				}
			}				
			ksort($headersArray);
			foreach( $headersArray as $index => $valArr)
			{
				$headers[key($valArr)] = $valArr[key($valArr)];				
			}
		} else 
		*/		
		if( $_GET['section'] == "manage" || $_GET['section'] == "admin"){
			$options = " AND active & 4 = 4 ";
			$headers = $nm -> getHeaderList( $options );	
		} else if($_GET['section'] == "new"){
			$options = " AND active & 8 = 8 ";
			$headers = $nm -> getHeaderList( $options );
		}
		
		$uaccess 		= new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess     = $uaccess -> getUser( $_SESSION['tid'] );
		$userRiskRef     = $uaccess -> getUserRefDirectorate();
		//get the directorates / sub directorate the user is in
		if( isset($_GET['view']))
		{
			if($_GET['view'] == "viewMyn")
			{
				$viewAccess 	= $this -> _setViewAccess( $userAccess , "viewMyn", $userRiskRef );
			} else {
				$viewAccess 	= $this -> _setViewAccess( $userAccess , "viewAll", $userRiskRef );
			} 
		}

		$colored  		= array(
								"control_rating" 	 => "controlRating",
								"impact_rating" 	 => "IMrating",
								"likelihood_rating"  => "LKrating",
								);
		$default  		= new Defaults("","");
		$defaults 		= $default -> getDefaults();
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");
		
$sql = "
							SELECT 
							DISTINCT(RR.id) AS risk_item,
							RR.description AS risk_description,
							RR.background AS risk_background,
							RR.impact_rating AS IMrating,
							RR.likelihood_rating AS LKrating, 
							RR.control_effectiveness_rating AS controlRating, 
							RR.current_controls,
							RR.time_scale , 
							RR.inherent_risk_exposure,
							RR.inherent_risk_rating,
							RR.residual_risk,
							RR.residual_risk_exposure,
							RR.attachment AS attachements,
							RR.kpi_ref,
							RR.actual_financial_exposure,
							RR.rbap_ref,
							RR.cause_of_risk,
							RR.reasoning_for_mitigation,
							FE.name AS financial_exposure,							
							RLEVEL.name AS risk_level, 												
							RC.name as risk_category, 
							RC.description as cat_descr,
							RCE.effectiveness AS percieved_control_effective,
							RCE.color AS control_rating, 								
							IM.assessment as impact,
							IM.definition as impact_descr ,
							IM.color AS impact_rating,
							RLL.assessment as likelihood,
							RLL.color AS likelihood_rating ,
							RLL.definition like_descr,
							RT.name as risk_type,
							RT.shortcode as type_code, 
							CONCAT(D.dirtxt, ' - ', DS.subtxt) AS risk_owner,
							RR.sub_id,
							RS.name AS risk_status,
							RS.color AS riskstatuscolor,
							FY.value as financial_year
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLEVEL ON RLEVEL.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RLL ON RLL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status							
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							LEFT JOIN assist_".$_SESSION['cc']."_master_financialyears FY ON RR.financial_year_id = FY.id
							WHERE RR.active = 1 $viewAccess ORDER BY RR.id asc LIMIT $start, $limit 
							" ;
		$response = $this -> get( $sql);
							
		$riskArray = array();	
		$attachments = "";
		$countAttach = 0;
		foreach( $response as $key => $risk )
		{
			$field = "risk_item";
			$index = $risk[$field];
			$risk[$field] = Risk::REFTAG."".$risk[$field];
			$updateArray 		= $this -> getRiskUpdate( $index );
			foreach($headers as $field => $value) 
			{
				if( $field == "action_progress"){
					$actionObj 		= new RiskAction($index, "", "", "", "", "", "", "", "");
					$actionProgress = $actionObj -> getActionStats();
					$riskArray[$index][$field]  = "<span style='text-align:right; width:100%;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".round($actionProgress['averageProgress'], 2)."%</span>";
				} else if( isset( $risk[$field]) || array_key_exists($field, $risk) )
				{
					//calculated
					if($field === "residual_risk_exposure" ) {		
						$riskExposureArr = $res -> getRiskResidualRanges( ceil($risk[$field]) );
						$color		= "";
						$magnitude  = "";
						if(isset($riskExposureArr) && !empty($riskExposureArr)){
							$color 		  = $riskExposureArr['color'];
							$magnitude 	  = $riskExposureArr['magnitude'];
						}
					 	$risk[$field] =  "<span style='width:50px; background-color:#".$color."'>".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					}
					//calculted
					if($field === "inherent_risk_exposure" ) {
						$inherentResults  = $ihr -> getRiskInherentRange( round($risk[$field]) );
						$color 			  = "";
						$magnitude 	 	  = "";
						if(isset($inherentResults) && !empty($inherentResults)){
							$color 			= $inherentResults['color']; 
							$magnitude 	 	= $inherentResults['magnitude'];
						}
						$risk[$field] =   "<span style='width:50px; background-color:#".$color."'>".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					}
					if($field == "financial_year") {
						if($risk[$field]==NULL || $risk[$field]=="0" || $risk[$field]=="") {
							$risk[$field] = "Unspecified";
						}
					}
					//if($field=="risk_item") { $risk[$field] = Risk::REFTAG."".$risk[$field]; }
					if($field == "attachements" ) {
						$attched  = "";
						
						if( $risk[$field] != "" || $risk[$field] != NULL ){
							$attachments	= unserialize($risk[$field]	);
							$countAttach    = count($attachments);
							if(	isset($attachments) && !empty($attachments)){
								foreach( $attachments as $attchName => $attach ){
									 $ext = substr( $attach , strpos($attach, ".")+1);
									 $file = $attchName.".".$ext; 
									if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/".$file)){
										$attched .= $attach."<br /> ";	
									}
								}
							}
						}
                        if($attched == ""){
                            $risk[$field] = "&nbsp;";
                        } else {
                            $risk[$field] = $attched;
                        }
						//$riskArray[$risk['risk_item']]['attachment'] = $attched;
					}
					
					if($field == "risk_status" ) {
						//$risk[$field] = "<span style='width:50px; text-align:center; background-color:#".$risk['riskstatuscolor']."'>&nbsp;&nbsp;&nbsp;</span>".$risk[$field];
					}		
							
					if( in_array($field, array_keys($colored)) ){
						$riskArray[$index][$field] 	= "<span style='width:50px; text-align:right; background-color:#".$risk[$field]."'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."</span>";
					} else {
						$riskArray[$index][$field]  = $risk[$field];
					}	
				}  
			}
		}
		$totalrisk = $this -> totalRisk( $viewAccess );
		$data 	   = array();
		$data 	   = array( 
						"x"			 => count($headers),
						"total" 	 => (int)$totalrisk ,				 
						"riskData"   => $riskArray,
						"headers"	 => $headers,
						"defaults"	 => $defaults,
						"useraccess" => $userAccess, 
						"actionOnly" => $this -> actionIds,
					  );



 //DEVELOPMENT PURPOSES!!!
/*ob_start();
echo "<html><body><h3>SQL</h3><p>".$sql."<h3>Data</h3><pre>"; print_r($data); echo "</pre><h3>Response</h3><pre>"; print_r($response); echo "</pre></body></html>";
$echo = ob_get_contents();
ob_end_clean();



$filename = "getRiskAllTogether_".date("YmdHis").".html";

			$file = fopen($filename,"w");
			fwrite($file,$echo."\n");
			fclose($file);					  
					  
					  
//*/
					  
		return $data;
	}
	/**
		Fetches the total risks available 
		@return int
	**/
	function totalRisk( $viewAccess)
	{

		$response = $this -> getRow( "
							SELECT COUNT( DISTINCT(RR.id) ) as total
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLL ON RLL.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							INNER JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status	
							WHERE RR.active = 1 $viewAccess  
							" );
		return $response['total'];
	}
	/**
		Fetches detailed information about a single risk
		@return array
	**/
	function getRisk( $id  )
	{	
		$where = "";
		if( $id == "" ) {
			$where = "WHERE 1";	
		} else {
			$where = "WHERE RR.id = $id"; 
		}			
		
		$response = $this -> getRow(
							"SELECT RR.*,
							RR.sub_id AS user_responsible,
							FE.name AS risk_financial_exposure,							
							RLEVEL.name AS risk_level, 									
							RC.name as category,
							RC.id as categoryid,							
							RC.description as cat_descr,
							RCE.effectiveness ,
							RR.control_effectiveness_rating ,							
							RCE.id as effectivenessid,
							RCE.color effect_color , 							
							IM.assessment as impact,
							IM.id as impactid,
							IM.definition as impact_descr ,
							IM.color AS impact_color,
							RL.assessment as likelihood,
							RL.id as likelihoodid,							
							RL.color AS like_color ,
							RL.definition like_descr,
							RT.name as type,
							RT.id as typeid,							
							RT.shortcode as type_code,
							CONCAT(D.dirtxt, ' - ', DS.subtxt) AS risk_owner,
							RS.name AS status ,
							RS.id as statusId,
							RS.color AS rscolor
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLEVEL ON RLEVEL.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status							
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = RR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid	
							$where
							" );
		return $response ;
	}
	/**
		Fetches limited infomation about the risk of specified id
		@return array
	**/
	function getARisk( $id )
	{
		$response = $this -> getRow ( "SELECT * FROM ".$_SESSION['dbref']."_risk_register WHERE id = '".$id."'" );
		return $response;
	}
	
	function getRiskWithHeaders($id) {
		$response = array();
		$response['riskData'] = $this -> getRisk ( $id );
		$rkCols  = new RiskColumns();
		$response['headers'] = $rkCols -> getHeaderList();
		return $response;
	}
	
	function updateRisk( $id, $changes )
	{
		$inherent 	= ($this -> impact_rating * $this -> likelihood_rating);
		$residual 	= (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$insertdata = array(
			'type'   					   => $this -> type,
			'category' 				   => $this -> category,
			'description' 				   => $this -> description,
			'background'				   => $this -> background ,
			'impact' 				        => $this -> impact,
			'inherent_risk_exposure'		   => $inherent,
			'inherent_risk_rating'		   => $inherent,
			'residual_risk_exposure'		   => $residual,
			'residual_risk'			   => $residual,						
			'impact_rating' 			   => $this -> impact_rating,
			'likelihood' 				   => $this -> likelihood,
			'likelihood_rating' 		   => $this -> likelihood_rating,
			'current_controls' 		        => $this -> current_controls,
			'percieved_control_effectiveness'=> $this -> percieved_control_effectiveness,
			'control_effectiveness_rating'   => $this -> control_effectiveness_rating,
			'level' 					   => $this -> level,		
			'financial_exposure'		   => $this -> financial_exposure,		
			'actual_financial_exposure'	   => $this -> actual_financial_exposure,		
			'kpi_ref' 				   => $this -> kpi_ref,			
			'attachment' 				   => $this -> attachement	,		
			'risk_owner' 				   => $this -> risk_owner,
			'insertuser'				   => $_SESSION['tid'],			

		);
		$updatedata  = $insertdata;
		$insertdata['sub_id'] = $this -> risk_owner;
		$response              = 0;
		$response  	       += $this -> update('risk_register' , $insertdata , "id = $id");
		$response             += $this -> insert('risk_edits' , array_merge($updatedata , array("risk_id" => $id,'changes'=> $changes) ) );		
		return $response;
	}
	
	function getAssociatedActions($id) {
		//$db = new ASSIST_DB();
		$sql = "SELECT A.id, A.action, A.action_owner, CONCAT(TK.tkname,' ',TK.tksurname) as owner
				FROM ".$_SESSION['dbref']."_actions A
				INNER JOIN assist_".$_SESSION['cc']."_timekeep TK
				  ON TK.tkid = A.action_owner
				WHERE A.risk_id = ".$id."
				AND A.active = 1";
		//$actions = $db->mysql_fetch_all($sql);
		//unset($db);
		$actions = $this->get($sql);
		return $actions;
		//return array();
	}	
	
	function deleteRisk( $id )
	{
        //$statusObj 		= new RiskStatus("","", "");
        //$statuses 		= $statusObj -> getStatuses();
		$def = new Defaults("","");
			$risk_object_name = $def->getObjectName("OBJECT"); //echo $risk_object_name;
		$risk = $this->getRisk($id);
		$updatedata = array( 
							'active' 		=> 0,
							'insertuser'	=> $_SESSION['tid']
							);
		$response  = $this -> update( 'risk_register' , $updatedata , "id = $id");
		$updatedata['risk_id'] = $id;
		/*$updatedata['changes'] = base64_encode(serialize(array(
			'active'	=> array('from'=>$risk['active'],'to'=>0),
			'insertuser'=> array('from'=>$risk['insertuser'],'to'=>$_SESSION['tid'])
		)));*/
		$change_arr = array();
			//$change_arr['active'] = array('from'=>1,'to'=>0);
            $change_arr['currentstatus'] = $risk['status'];
            $change_arr['user']   = $_SESSION['tkn'];
            $change_arr['comment']   = $risk_object_name." ".$id." deleted";
		$updatedata['changes'] = base64_encode(serialize($change_arr));
			$response             += $this -> insert('risk_edits' , $updatedata );		
		return $response;
		
	}
	
	function getRiskUpdates( $id )
	{	
	
	 $response = $this -> get( "SELECT 
	 							RU.id AS risk_item,
								RU.changes,
	 							RU.response,
								RS.name AS risk_status ,
								RU.insertdate,
								RU.insertuser,
								TK.tkname,
								TK.tksurname
	 							FROM ".$_SESSION['dbref']."_risk_update RU
	 							INNER JOIN ".$_SESSION['dbref']."_status RS ON RU.status = RS.id
								LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = RU.insertuser
								WHERE RU.risk_id = '".$id."' ORDER BY RU.insertdate DESC ,RU.id DESC						
	  " );
	  return $response; 
	}
	
	function _processAttachment( $attachement )
	{
		$attach = unserialize( $attachement );
		$tosave = "";
		if(is_array( $attach )) 
			foreach( $attach as $at ) {
				$tosave .= $at.","; 
			}
		$tosave = rtrim($tosave,",");
		return $tosave;
	}
	
	function getCalculation($type) {
		$row = array();
		switch($type) {
			case "RRE":
				$row['rating'] = $this->residual_risk_rating;
				$obj = new ResidualRisk();
				break;
			case "IRE":
				$row['rating'] = $this->inherent_risk_rating;
				$obj = new InherentRisk();
				break;
		}
		$r = $obj->findExposure($row['rating']);
		$row['name'] = $r['name'];
		//print_r($row);
		return $row;
	}
	
	 static function getStatusSQLForWhere($a) 
     {
        /*$sql = "(
                        ".$a.".active & ".Risk::ACTIVATED. " = ".Risk::ACTIVATED. "
                        
        )";*/
		
		$sql = "( ".$a.".active = 1 )";
        return $sql;
     }

}
?>
