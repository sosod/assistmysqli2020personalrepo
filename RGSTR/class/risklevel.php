<?php
/**
	* Risk level for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class RiskLevel extends DBConnect
{
	
	protected $shortcode;
	
	protected $status;
	
	protected $description;
	
	protected $name;
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
	
	function __construct( $shortcode="", $name="", $description="", $status="")
	{
		$this -> shortcode 	 = trim($shortcode);
		$this -> name 		 = trim($name);
		$this -> description = trim($description);
		$this -> status      = trim($status);		
		parent::__construct();
	}
	
		
	function saveLevel()
	{
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"status" 	 => $this -> status ,
						"insertuser"  => $_SESSION['tid'],
						);
			
		$response = $this -> insert( "level" , $insert_data );
		echo $response;//$this -> insertedId();
	}

    public function getRiskLevels()
    {
        $response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE status <> 2" );
        return $response ;
    }
	
	function getLevel()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE status <> 2" );
		return $response ;		
	}	

    public function getActiveLevel()
    {
        $response = $this->get("SELECT * FROM ".$_SESSION['dbref']."_level WHERE status <> 2 AND status = 1");
        return $response ;
    }


	function getARiskLevel( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE id = '".$id."'" );
		return $response ;		
	}	
	/**
		Get used risk types , so they cannot be deleted
	**/
	function getUsedLevel()
	{	
		$response = $this -> get("SELECT level FROM ".$_SESSION['dbref']."_risk_register GROUP BY level");
		return $response;
	}
	/**
		Fetches all active risk types
	**/
	function getActiveRiskLevel()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_level WHERE active = 1" );
		return $response ;		
	}		
			
	function updateLevel( $id )
	{
		$insertdata = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"status"	  => $this -> status, 
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $logsObj -> setParameters( $_POST, $data, "level");
		$response = $this -> update( "level", $insertdata , "id=$id");
		return $response;
	}
	
	function changeLevelStatus( $id, $status )
	{
		$updatedata = array(
						'status' 	  => $status,
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $_POST['status'] = $status;
        $logsObj -> setParameters( $_POST, $data, "level");
		$response = $this -> update( 'level', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateLevel( $id )
	{
		$update_data = array(
						'status' 	  => "1",
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $_POST['status'] = 1;
        $logsObj -> setParameters( $_POST, $data, "level");
		$response = $this -> update( 'level', $update_data, "id=$id" );
		echo $response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	function addItem($shortcode, $name,$description) {
		$this -> shortcode	 = trim($shortcode);
		$this -> name = trim($name);
		$this -> description = trim($description);
		
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"status" 		  => RiskLevel::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "level" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $code, $name, $description )
	{
		$insertdata = array(
						"shortcode" 			 => $code,
						"name" 				 => $name,
						"description" => $description,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
		$data['short_code'] = $data['shortcode'];
		unset($data['shortcode']);
		$var = array(
			'id'=>$id,
			'short_code'=>$code,
			'name'=>$name,
			'description'=>$description,
		);
        $logsObj -> setParameters( $var, $data, "level");
		$response = $this -> update( "level", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'status' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskLevel( $id );
        $var['status'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "level");
		$response = $this -> update( 'level', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
		
	
	
	
	
	
	
	function getReportList()
	{
		$response = $this -> get( "SELECT DISTINCT L.id, L.name
			   FROM ".$_SESSION['dbref']."_level L 
			   LEFT JOIN ".$_SESSION['dbref']."_risk_register Q
			   ON Q.level = L.id WHERE L.status & 2 <> 2
		 " );
		return $response;
	}

}
?>