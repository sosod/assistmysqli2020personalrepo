<?php
class FormatDisplay
{
	protected $headers;
	
	protected $arrayValues;
	
	protected $type;
	
	function __construct( $values )
	{
		$this->type 	   = ""; 
		$this->arrayValues = $values;
	}
	
	function setType( $name )
	{	
		$this-> type = $name;
	}
	
	//prepares the data to be displayed , so that it has the headers defined in the headers table or on setup
	function displayData()
	{
		$data 	   = array();
		$namingObj = new Naming();
		$this->headers = $namingObj -> getHeaderByType($this->type);		
		foreach($this->arrayValues as $key => $value)
		{	
			$name = "";
			$name = $namingObj->setHeader( $key );
			$data[$name] = $value; 
		}
		return $data;
	}
	
	
}
?>
	
