<?php

class RGSTR_ACTION extends RGSTR {

	protected $section = "action";
	protected $attachment_folder = "action";
	protected $tablename = "actions";

	public function __construct() {
		parent::__construct();
	}
	
	public function getRecord($i) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_actions WHERE id = ".$i;
		return $this->mysql_fetch_one($sql);
	}
	
	public function getParentRecord($risk_id) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_risk_register WHERE id = ".$risk_id;
		return $this->mysql_fetch_one($sql);
	}
	
	public function getFormattedRecord($i) {
		$row = $this->getRecord($i);
		$a = strlen($row['attachment']) > 0 ? unserialize(base64_decode($row['attachment'])) : array();
		$row['attachment'] = $a;
		return $row;
	}

	public function saveUpdateAttachments($i,$new_attach,$updated_attach,$stat) {
		$attach = base64_encode(serialize($updated_attach));
		//$sql = "UPDATE ".$this->getDBRef()."_actions SET attachment = '".$attach."' WHERE id = ".$i;
		//$this->db_update($sql);
		$updatedata = array(
			'attachment'=>$attach,
		);
		
		$log = array();
		foreach($new_attach as $x=>$val) {
			$log['attachments'][$x] = "Attachment <i>".$val."</i> has been added";
		}
		$log['user'] = $_SESSION['tkn'];
		//$log['currentstatus']= $status;
		$statusObj 	= new ActionStatus();
		$statuses    = $statusObj -> getActionStatuses();
			  if($statuses[$stat])
			  {
			     $log['currentstatus'] = $statuses[$stat]['name'];
			  } else {
			     $log['currentstatus'] = "New";
			  }		
			//$obj = new Model();
			//$obj -> setTablename($this->tablename."_update");
			$obj = new RiskAction();
			//$insertdata['response']   = "Added attachments during update.";		
			$insertdata['changes']    = base64_encode(serialize($log));
			$insertdata['action_id']  = $i;
			$insertdata['insertuser'] = $_SESSION['tid'];										
		//$res = $obj -> save( $insertdata );	
		//$res = $obj->insert($this->tablename."_update",$insertdata);
		$res = $obj->updateAction($i, $updatedata, $insertdata);
		
	}
	
	public function displayAttachmentList($i,$can_edit=false,$attach=array(),$need_java=true) {
		$echo = "";
		if(count($attach)==0) {
			$record = $this->getFormattedRecord($i);
			$attach = $record['attachment'];
		}
		$a = array();
		$x = 0;
		if(isset($attach) && is_array($attach) && count($attach)>0) {
			foreach($attach as $src=>$filename) {
				$key = $i."_".$x; $x++;
				$a[$key] = array(
					'system_filename'=>$src,
					'original_filename'=>$filename
				);
			} //echo "<pre>"; print_r($a); echo "</pre>";
			$echo = $this->getObjectAttachmentDisplay($can_edit,$a,$this->getModRef()."/".$this->attachment_folder,$need_java);
		}
		return $echo;
	}
	
	
	public function deleteAttachment($i,$deleted_attach,$updated_attach,$stat) {
		$attach = base64_encode(serialize($updated_attach));
		$sql = "UPDATE ".$this->getDBRef()."_actions SET attachment = '".$attach."' WHERE id = ".$i;
		//$this->db_update($sql);
		$updatedata = array(
			'attachment'=>$attach,
		);
		
		$log = array();
		foreach($deleted_attach as $x=>$val) {
			$log['attachments'][$x] = "Attachment <i>".$val."</i> has been deleted";
		}
		$log['user'] = $_SESSION['tkn'];
		//$log['currentstatus']= $status;
		$statusObj 	= new ActionStatus();
		$statuses    = $statusObj -> getActionStatuses();
			if($statuses[$stat]) {
				$log['currentstatus'] = $statuses[$stat]['name'];
			} else {
				$log['currentstatus'] = "New";
			}		
			$obj = new RiskAction();
			//$obj -> setTablename($this->tablename."_update");
			//$insertdata['response']   = "Deleted attachment";		
			$insertdata['changes']    = base64_encode(serialize($log));
			$insertdata['action_id']  = $i;
			$insertdata['insertuser'] = $_SESSION['tid'];										
		//$res = $obj -> save( $insertdata );	
		$res = $obj->updateAction($i, $updatedata, $insertdata);
	}
	
	
	
	
	
	public function getActionOwnersForApproval($sub_ids=array(),$risk_id=0,$folder) {
		if(count($sub_ids)==0 && $risk_id==0) {
			$userObj = new RGSTR_USER();
			if($folder=="admin") {
				$subs = $userObj->getAllDirectorates();
			} else {
				$subs = $userObj->getMyDirectorates();
			}
			$sub_ids = array_keys($subs);
		}
			$db = new ASSIST_DB();
		if(count($sub_ids)>0) {
			$sql = "SELECT DISTINCT A.action_owner as id, CONCAT(TK.tkname,' ',TK.tksurname) as name
			FROM ".$db->getDBRef()."_actions A
			INNER JOIN ".$db->getDBRef()."_risk_register R
			  ON R.id = A.risk_id AND R.active = 1 AND R.sub_id IN (".implode(",",$sub_ids).")
			INNER JOIN assist_".$db->getCmpCode()."_timekeep TK
			  ON A.action_owner = TK.tkid 
			WHERE A.active = 1 AND (A.action_status & ".RiskAction::ACTION_AWAITING." = ".RiskAction::ACTION_AWAITING.")
			ORDER BY TK.tkname, TK.tksurname";
			$result = $db->mysql_fetch_all_by_id($sql,"id");
		} elseif($risk_id>0) {
			$sql = "SELECT DISTINCT A.action_owner as id, CONCAT(TK.tkname,' ',TK.tksurname) as name
			FROM ".$db->getDBRef()."_actions A
			INNER JOIN ".$db->getDBRef()."_risk_register R
			  ON R.id = A.risk_id AND R.id = ".$risk_id."
			INNER JOIN assist_".$db->getCmpCode()."_timekeep TK
			  ON A.action_owner = TK.tkid 
			WHERE A.active = 1 AND (A.action_status & ".RiskAction::ACTION_AWAITING." = ".RiskAction::ACTION_AWAITING.")
			ORDER BY TK.tkname, TK.tksurname";
			$result = $db->mysql_fetch_all_by_id($sql,"id");
		} else {
			$result = array();
		}
		return $result;
	}
	
	public function getApprovedActionOwners($sub_ids=array(),$risk_id=0,$folder) {
		if(count($sub_ids)==0 && $risk_id==0) {
			$userObj = new RGSTR_USER();
			if($folder=="admin") {
				$subs = $userObj->getAllDirectorates();
			} else {
				$subs = $userObj->getMyDirectorates();
			}
			$sub_ids = array_keys($subs);
		}
			$db = new ASSIST_DB();
		if(count($sub_ids)>0) {
			$sql = "SELECT DISTINCT A.action_owner as id, CONCAT(TK.tkname,' ',TK.tksurname) as name
			FROM ".$db->getDBRef()."_actions A
			INNER JOIN ".$db->getDBRef()."_risk_register R
			  ON R.id = A.risk_id AND R.active = 1 AND R.sub_id IN (".implode(",",$sub_ids).")
			INNER JOIN assist_".$db->getCmpCode()."_timekeep TK
			  ON A.action_owner = TK.tkid 
			WHERE A.active = 1 AND (A.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED.")
			ORDER BY TK.tkname, TK.tksurname";
			$result = $db->mysql_fetch_all_by_id($sql,"id");
		} elseif($risk_id>0) {
			$sql = "SELECT DISTINCT A.action_owner as id, CONCAT(TK.tkname,' ',TK.tksurname) as name
			FROM ".$db->getDBRef()."_actions A
			INNER JOIN ".$db->getDBRef()."_risk_register R
			  ON R.id = A.risk_id AND R.id = ".$risk_id."
			INNER JOIN assist_".$db->getCmpCode()."_timekeep TK
			  ON A.action_owner = TK.tkid 
			WHERE A.active = 1 AND (A.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED.")
			ORDER BY TK.tkname, TK.tksurname";
			$result = $db->mysql_fetch_all_by_id($sql,"id");
		} else {
			$result = array();
		}
		return $result;
	}
	
	
	/** SHORTCUT FUNCTIONS **/
	public function getStatusID($record) { return $record['status']; }
	
}


?>