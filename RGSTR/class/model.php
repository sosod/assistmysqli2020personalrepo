<?php
@session_start();

class Model
{

	private $tablename = "";
	
	protected static $table;
	
	protected $db;
	
	protected $db2;
	
	function __construct($dbRef = "")
	{
		$this -> db 	= new DBConnect( "", "", "", "", "");
	     if(!empty($dbRef))
	     {
	        $this -> db -> setDBRef($dbRef);
	     }
	}
	
	public function getTablename()
	{
		return (!empty($this -> tablename) ? $this -> tablename : static::$table);
	}
	
	public function setTablename($value)
	{
		$this->tablename = $value;
	}
	
	function save($data)
	{    
		$data['insertuser'] = $_SESSION['tid'];
		$table              = (!empty($this -> tablename) ? $this -> tablename : static::$table);
		$this -> db -> insert($table, $data);
		return $this -> db -> insertedId();
	}
	
	 function __call($methodName, $args)
	 {	 
	 	$row = "";	
	 	if( substr($methodName, 0, 6) == "findBy")
	 	{
		   $field = strtolower(substr($methodName, 6));
	 	   $value = $args[0];
	 	   $row   = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE ".$field." = '".$value."'");
	 	}
	 	return $row;
	 }
	 
	 
	function update($id, $data ,Auditlog $auditlog, $obj = "", $naming = null)
	{		
	     $res	   = 0;
	     $result = "";
	     $table   = (!empty($this -> tablename) ? $this -> tablename : static::$table);			     
	     if(is_object($obj))
	     {
	        //observer
             $result = $auditlog -> processChanges($obj, $data, $id, $naming);
	     } 		
	     if(isset($data['multiple']))
	     {
		   unset($data['multiple']);
	     }
	     if(is_array($result) && !empty($result))
	     {
	         $data = array_merge($data, $result);
	     }
		// $this->db->assist_writeError("test","model.update");
	     if(strstr($id, "="))
	     {
		  $this -> db -> update($table, $data, $id);
	     } else {
		   $this -> db -> update($table, $data, "id={$id}");
	     }
	     $affectedRows = $this -> db -> affected_rows();
	     $res = (!empty($result) ? count($result) + $affectedRows : $affectedRows);
	     return $res;
	}
	
	
	public static function updateMessage( $context, $status )
	{
		$response = array();
		if( $status > 0)
		{
			$response = array("text" => ucwords( str_replace("_", " ", $context ) )." updated successfully", "error" => false, "updated" => true);
		} elseif($status == 0){
		    $response = array("text" => " No change made", "error" => false, "updated" => false);
		} else {
			$response = array("text" => "Error updating ".ucwords( str_replace("_", " ", $context ) )."", "error" => true);
		}
		return $response;
	}
	
	public static function saveMessage( $context, $status)
	{
		$response = array();
		if( $status > 0){
			$response = array("text" => ucwords(str_replace("_", " ", $context ) )." saved successfully", "error" => false, "id" => $status );
		} else {
			$response = array("text" => "Error saving ".ucwords(str_replace("_", " ", $context ) )."", "error" => true);
		}
		return $response;
	}

	
	public function saveTestData($echo) {
			$file = fopen("../test.txt","a");
			fwrite($file,"\r\n\r\n {".date("d-m-Y H:i:s")."}    ".$echo);
			fclose($file);
	}

}
?>