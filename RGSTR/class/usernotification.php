<?php
class UserNotification extends DBConnect
{

	protected $id;
	
	protected $notification;
	
	protected $risk_id;
	
	protected $action_id;
	
	protected $recieve_email;
	
	protected $recieve_when;
	
	protected $recieve_what;
	
	protected $user_id;
	
	protected $recieve_day;
	
	protected $active;
	
	protected $inserdate;
	
	function __construct() {
		$this -> notification  = (isset($_POST['notification']) ? $_POST['notification'] : "");
		$this -> risk_id	   = (isset($_POST['risk_id']) ? $_POST['risk_id'] : "");
		$this -> action_id	   = (isset($_POST['action_id']) ? $_POST['action_id'] : "");
		$this -> recieve_email = (isset($_POST['recieve']) ? $_POST['recieve'] : "");
		$this -> recieve_what  = (isset($_POST['recieve_what']) ? $_POST['recieve_what'] : "");
		$this -> recieve_when  = (isset($_POST['recieve_when']) ? $_POST['recieve_when'] : "");		
		$this -> user_id	   = (isset($_SESSION['tid']) ? $_SESSION['tid'] : "");	
		$this -> recieve_day   = (isset($_POST['recieve_day']) ? $_POST['recieve_day'] : "");
		parent::__construct();					
	}
	
	function getNotifications(){
		$response = $this -> get("SELECT * FROM #_user_notifications WHERE user_id = ".$this -> user_id." AND status & 2 <> 2");
		return $response;		
	}
	
	function saveNotifications(){
		$insert_data = array(
				'notification' 		=> $this -> notification,
				'risk_id' 			=> $this -> risk_id,
				'action_id' 		=> $this -> action_id,
				'recieve_email' 	=> $this -> recieve_email,
				'recieve_what' 		=> $this -> recieve_what,
				'recieve_when' 		=> $this -> recieve_when,				
				'user_id'			=> $this -> user_id,
				'recieve_day'		=> $this -> recieve_day,
				"insertuser"  		=> $_SESSION['tid'],				
		);
		$response = $this -> insert( 'user_notifications', $insert_data );
		return $response;//$this -> insertedId();
	}
	
		
	function getAProfile( $id )
	{
		$results = $this -> getRow("SELECT * FROM ".$_SESSION['dbref']."_user_notifications WHERE id = '".$id."'");
		return $results;
	}
	
	function updateNotification( $id ){
		$updatedata = array(
				'notification' 		=> $this -> notification,
				'risk_id' 			=> $this -> risk_id,
				'action_id' 		=> $this -> action_id,
				'recieve_email' 	=> $this -> recieve_email,
				'recieve_what' 		=> $this -> recieve_what,
				'recieve_when' 		=> $this -> recieve_when,				
				'user_id'			=> $this -> user_id,
				'recieve_day'		=> $this -> recieve_day,
				"insertuser"  		=> $_SESSION['tid'],				
		);
		$response = $this -> update( 'user_notifications', $updatedata, "id=$id" );
		return $response;
	}
	
	function deleteNotification( $id ){
		$result = $this -> update("user_notifications", array("status" => 2), "id=$id");
		return $result;	
	}
	
}
?>