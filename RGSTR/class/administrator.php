<?php
/*if(file_exists("../../library/class/assist_helper.php"))
{
    require_once( "../../library/class/assist_helper.php");
} 
if(file_exists("../../library/class/assist_dbconn.php"))
{
    require_once( "../../library/class/assist_dbconn.php" );
} 
if(file_exists("../../library/class/assist_db.php"))
{
    require_once( "../../library/class/assist_db.php" );
} 
if(file_exists("../library/class/assist_db.php"))
{
    require_once("../library/class/assist_db.php");
} 
if(file_exists("../library/class/assist_dbconn.php"))
{
    require_once("../library/class/assist_dbconn.php");
}
if(file_exists("../library/class/assist_helper.php"))
{
    require_once("../library/class/assist_helper.php");
}
//require_once "../../library/class/assist_dbconn.php";
//require_once "../..library/class/assist_db.php";
//require_once "../..library/class/assist_helper.php";
*/
if(is_dir("../../library/class")) {
	require_once "../../library/class/assist_dbconn.php";
	require_once "../../library/class/assist_db.php";
	require_once "../../library/class/assist_helper.php";
} elseif(is_dir("../library/class")) {
	require_once "../library/class/assist_dbconn.php";
	require_once "../library/class/assist_db.php";
	require_once "../library/class/assist_helper.php";
}
class Administrator {

	private $user_id;
	private $admin = array();
	private $access = array();			//array($sub_id=>array('update'=>true....) )
	
	//db record has been deleted
	const INACTIVE = 2;
	//db record is active
	const ACTIVE = 4;
	//labels
	const DIR = "D";
	const SUB = "S";

	public function __construct($u) {
		$this->user_id = $u;
		$db = new ASSIST_DB("client");
		//var_dump($db);
		$sql = "SELECT id, tkid as user_id, type, ref, active FROM ".$db->getDBRef()."_dir_admins 
		        WHERE tkid = '$u' AND active & ".self::ACTIVE." = ".self::ACTIVE;
		$rows = $db->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$t = $r['type'];
			$admin_ref = constant('self::'.$t).$r['ref'];
			$me = new DirectorateAdministrator($r);
			$this->admin[$admin_ref] = array(
				'id'=>$admin_ref,
				'type'=>$t,
				'ref'=>$r['ref'],
				'admin'=>$me,
				'access'=>$me->getAccess($r['active']),
			);
			$i = $r['ref'];
			if($t=="SUB") {
				foreach($this->admin[$admin_ref]['access'] as $act => $res) {
					if($res===true) {
						$this->access[$act][$i] = $i;
					}
				}
			} elseif($t=="DIR") {
				$dir = new Directorate("","","","");
				$subs = $dir->getSubDirectorates($i);
				foreach($subs as $s) {
					$k = $s['id'];
					foreach($this->admin[$admin_ref]['access'] as $act => $res) {
						if($res===true) {
							$this->access[$act][$k] = $k;
						}
					}
				}
			}
		}
	}
	
	static function get_class_constants() {
		$reflect = new ReflectionClass(get_class($this));
		return $reflect->getConstants();
    }
	
	function getAdminAccess() {
		return $this->access;
	}
	function getAdminDetails() {
		return $this->admin;
	}
	
}

?>
