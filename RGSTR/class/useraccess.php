<?php
/**
	* @package 	: User Access
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class UserAccess extends DBConnect
{
	
	protected $user;
	
	protected $module_admin;
	
	protected $create_risks;
	
	protected $create_actions;	//NOT ACCESSIBLE FROM FORM
	
	protected $view_all;	//THIS IS ACTUALLY UPDATE_ALL
	
	protected $edit_all;
	
	protected $update_all;	//NOT IN DATABASE OR ACCESSIBLE FROM FORM
	
	protected $reports;
	
	protected $assurance;
	
	protected $setup;
	
	protected $risk_mngr;
	
	protected $save;
	
	const RISK_MANAGER = 1;
	
	
	function __construct( $user="", $module_admin=0, $create_risks=0, $create_actions=0,  $view_all=0, $edit_all=0, $reports=0, $assurance=0, $setup=0, $risk_mngr=0 )
	{
	
		$this -> user			= trim( $user );
		$this -> module_admin   = trim( $module_admin );
		$this -> create_risks   = trim( $create_risks );
		$this -> create_actions = trim( $create_actions );
		$this -> view_all 	    = trim( $view_all );
		$this -> edit_all       = trim( $edit_all );
		$this -> update_all     = trim( (isset($_REQUEST['u_all']) ? $_REQUEST['u_all'] : "") );		
		$this -> reports        = trim( $reports );		
		$this -> assurance 		= trim( $assurance );
		$this -> setup 		    = trim( $setup );	
		$this -> risk_mngr 		    = trim( $risk_mngr );	
		parent::__construct();
	}

    function getUsersList()
    {
        $response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail,
		                           CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".strtoupper($_SESSION['modref'])."'
								   ORDER BY TK.tkname, TK.tksurname" );
        $results   = array();
        foreach($response as $index => $res)
        {
            $results[$res['tkid']] = $res;
        }
        return $results;
    }


	function saveUserAccess()
	{

		$insert_data = array( 
						"user" 		  => $this -> user,
						"module_admin"   => $this -> module_admin,
						"create_risks"   => $this -> create_risks,
						"create_actions" => $this -> create_actions,
						"view_all"       => $this -> view_all,
						"edit_all" 	  => $this -> edit_all,
						"report"   	  => $this -> reports,
						"assurance"  	  => $this -> assurance,
						"setup"  		  => $this -> setup,
						"insertuser"     => $_SESSION['tid'],											
						);
		$response = $this -> insert('useraccess', $insert_data);
		$id = $response;//$this->insertedId();
		//$user_name = $this->getActionOwnerEmail($insert_data['user']);
		$old_user_data = $this->getUserAccessInfo($id);
		$changes = array('msg'=>"Added user ".$old_user_data['tkname']." ".$old_user_data['tksurname']);
		foreach($insert_data as $key => $new) {
			if($key!="insertuser") {
				$changes[$key] = array('to'=>($new==1?"Yes":"No"),'from'=>"-");
			}
		}
		$changes['user_record_id'] = $id;
		$changes['action'] = "NEW";
		$changes['insertusername'] = $_SESSION['tkn'];
		$changes['datetime'] = date("d-M-Y H:i:s");
		$insertdata = array(
			'setup_id'=>$id,
			'changes'=>base64_encode(serialize($changes)),
			'insertuser'=>$_SESSION['tid'],
		);
		$response = $this -> insert('useraccess_logs', $insertdata);
		echo $id;//$this -> insertedId();
	}
	
	function updateUserAccess()
	{
        $user_data = array(
						"module_admin"  => $this -> module_admin,
						"create_risks"  => $this -> create_risks,
						"create_actions"=> $this -> create_actions,
						"view_all"      => $this -> view_all,
						"edit_all" 		=> $this -> edit_all,
						"report"   		=> $this -> reports,
						"assurance"  	=> $this -> assurance,
						"setup"  		=> $this -> setup,
						"insertuser"    => $_SESSION['tid']
						);
		$old_user_data = $this->getUserAccessInfo($this->user);
		$changes = array('msg'=>"Edited user ".$old_user_data['tkname']." ".$old_user_data['tksurname']);
		foreach($user_data as $key => $new) {
			if($key!="insertuser" && $new != $old_user_data[$key]) {
				$changes[$key] = array('to'=>($new==1?"Yes":"No"),'from'=>($old_user_data[$key]==1?"Yes":"No"));
			}
		}
		$changes['insertuser'] = $_SESSION['tid'];
		$changes['action'] = "EDIT";
		$changes['user_record_id']=$this->user;
		$changes['insertusername'] = $_SESSION['tkn'];
		$changes['datetime'] = date("d-M-Y H:i:s");
		$insertdata = array(
			'setup_id'=>$this->user,
			'changes'=>base64_encode(serialize($changes)),
			'insertuser'=>$_SESSION['tid'],
		);
		$response = $this -> insert('useraccess_logs', $insertdata);
        $response = $this->updateData('useraccess', $user_data, array('id' => $this->user));
        return $response;
	}

	function getUser( $id, $save=false )
	{
		if($id=="0000") {
			$response = array(
						"module_admin"  => 0,
						"create_risks"  => 0,
						"create_actions"=> 0,
						"view_all"      => 0,
						"edit_all" 		=> 0,
						"report"   		=> 0,
						"assurance"  	=> 0,
						"setup"  		=> 1,
						"risk_manager"	=> 0
					);
		} else {
			$response = $this -> getRow("
									SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								    INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.user = '$id'
								  ");
			if(count($response)==0) {
				$response = array(
						"module_admin"  => 0,
						"create_risks"  => 0,
						"create_actions"=> 0,
						"view_all"      => 0,
						"edit_all" 		=> 0,
						"report"   		=> 0,
						"assurance"  	=> 0,
						"setup"  		=> 0,
						"risk_manager"	=> 0
					);
			}
		}
		if($save) {
			$this->save = true;
			$this -> user			= trim( isset($response['user']) ? $response['user'] : 0 );
			$this -> module_admin   = trim( isset($response['module_admin']) ? $response['module_admin'] : 0 );
			$this -> create_risks   = trim( isset($response['create_risks']) ? $response['create_risks'] : 0 );
			$this -> create_actions = trim( isset($response['create_actions']) ? $response['create_actions'] : 0 );
			$this -> view_all 	    = trim( isset($response['view_all']) ? $response['view_all'] : 0 );
			$this -> edit_all       = trim( isset($response['edit_all']) ? $response['edit_all'] : 0 );
			//$this -> update_all     = trim( $response['update_all'] );		
			$this -> reports        = trim( isset($response['report']) ? $response['report'] : 0 );
			$this -> assurance 		= trim( isset($response['assurance']) ? $response['assurance'] : 0 );
			$this -> setup 		    = trim( isset($response['setup']) ? $response['setup'] : 0 );
			$this -> risk_mngr 	  = trim( isset($response['risk_manager']) ? $response['risk_manager'] : 0 );
		}
		return $response;
	}

	public function isARiskManager($id="") {
		if(strlen($id)==0) {
			$id = $_SESSION['tid'];
		}
		if(!$this->save) {
			$this->getUser($id,true);
		}
		return ($this->risk_mngr == self::RISK_MANAGER);
	}
	
    public function getAllAssignedUsers()
    {
        $hlp = new ASSIST_MODULE_HELPER();
       /* $response = $this->get("SELECT UA.* FROM ".$_SESSION['dbref']."_useraccess UA
								INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK
								ON TK.tkid = UA.user AND TK.tkstatus = 1
								INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MMU
								ON MMU.usrtkid = TK.tkid AND MMU.usrmodref = '".strtoupper($_SESSION['modref'])."'
								ORDER BY TK.tkname, TK.tksurname");*/
        $sql = "SELECT UA.* FROM ".$_SESSION['dbref']."_useraccess UA
								INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK
								ON TK.tkid = UA.user AND TK.tkstatus = 1
								INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MMU
								ON MMU.usrtkid = TK.tkid AND MMU.usrmodref = '".strtoupper($_SESSION['modref'])."'
								ORDER BY TK.tkname, TK.tksurname";
	    $response = $hlp->mysql_fetch_all($sql);
        $list     = array();
	    foreach($response as $index => $user)
	    {
		    foreach($user as $fld => $value) { $user[$fld] = $hlp->decode($value); }
		    $list[$user['user']] = $user;
	    }
        return $list;
    }
    /*public function getAllAssignedUsers()
    {
        $response = $this->get("SELECT * FROM ".$_SESSION['dbref']."_useraccess ");
        $list     = array();
        foreach($response as $index => $user)
        {
            $list[$user['user']] = $user;
        }
        return $list;
    }*/
	
	function getUserAccessInfo( $id )
	{	/**
	 * AA-559 Risk Assist - Cannot Edit Users under Setup
	 * Changes by Sondelani Dumalisile (09 March 2021)
	 * comment : Special character's decoding function switched from the Old DBconnect file to latest ASSIST_MODULE_HELPER
	 */
		/*$response = $this -> getRow( "
									SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								    INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.id = $id
								  ");*/
		$hlp = new ASSIST_MODULE_HELPER();
		$response = $hlp->mysql_fetch_one("SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA
								    INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.id = $id");
		$res = array();
		foreach($response as $index => $user)
		{
			if($index == "tkname" || $index == "tksurname") {
				$res[$index] = $hlp->decode($user);
			}else{
				$res[$index] = $user;
			}
		}
		return $res;
	}
	
	function getUserAccess()
	{
		/**
		 * AA-559 Risk Assist - Cannot Edit Users under Setup
		 * Changes by Sondelani Dumalisile (09 March 2021)
		 * comment : Special character's decoding function switched from the Old DBconnect file to latest ASSIST_MODULE_HELPER
		 */
		/*$response = $this -> get( "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE  MU.usrmodref = '".strtoupper(strtoupper($_SESSION['modref']))."' AND TK.tkstatus = 1
								  ORDER BY TK.tkname, TK.tksurname " );*/
		$hlp = new ASSIST_MODULE_HELPER();
		$sql = "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE  MU.usrmodref = '".strtoupper(strtoupper($_SESSION['modref']))."' AND TK.tkstatus = 1
								  ORDER BY TK.tkname, TK.tksurname ";
		$response = $hlp->mysql_fetch_all($sql);
		$list     = array();
		foreach($response as $index => $user)
		{
			foreach($user as $fld => $value) { $user[$fld] = $hlp->decode($value); }
			$list[$user['user']] = $user;
		}
		return $list;
		//return $response;
	}
	
	
	function getAUser()
	{
		$response = $this -> get( "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE  MU.usrmodref = '".strtoupper($_SESSION['modref'])."' AND TK.tkstatus = 1
								  ORDER BY TK.tkname, TK.tksurname " );
		return $response;
	}
		
	function getUsers()
	{							  							    
		$hlp = new ASSIST_MODULE_HELPER();
		/*$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail
								   FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK 
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".strtoupper(strtoupper($_SESSION['modref']))."'
								ORDER BY TK.tkname, TK.tksurname" );*/
		$sql = "SELECT tkid, tkname, tksurname, tkstatus , tkemail
								   FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".strtoupper(strtoupper($_SESSION['modref']))."'
								ORDER BY TK.tkname, TK.tksurname";
		$response = $hlp->mysql_fetch_all($sql);
		$data = array();
		foreach($response as $key => $item) {
			foreach($item as $fld => $value) { $item[$fld] = $hlp->decode($value); }
			$data[$key] = $item;
		}
		return $data;
		//return $response;
	}
	
	function getNonRiskManagerUsers() {
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail
								   FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK 
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   INNER JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user = TK.tkid AND UA.risk_manager = 0
								   WHERE tkid <> '0000' AND tkstatus = 1 AND MU.usrmodref = '".strtoupper($_SESSION['modref'])."'
								   ORDER BY TK.tkname, TK.tksurname
								   " );
		return $response;
	}
	
	function getRiskManager()
	{							  							    
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail
								   FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK 
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   INNER JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user = TK.tkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".strtoupper($_SESSION['modref'])."' AND UA.risk_manager = 1
								ORDER BY TK.tkname, TK.tksurname" );
		return $response;
	}

	function getARiskManager( $id )
	{
		$response  = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_useraccess WHERE user = '".$id."'" );
		return $response; 
	}

/*******
Invalid function - doesn't validate that the risk managers still have access to the module and/or haven't been terminated.   [Janet Currie 14 Jul 2014]
********/
	function getRiskManagerEmails()
	{							  		   
		$response = $this->get("SELECT TK.tkid , TK.tkemail, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK 
								INNER JOIN #_useraccess UA ON UA.user = TK.tkid
								INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								WHERE UA.risk_manager = 1 AND TK.tkstatus = 1 AND MU.usrmodref = '".strtoupper($_SESSION['modref'])."'
							  ");		
		$emailTo = "";
		if(!empty( $response)){
			foreach($response as $managerEmail ) {
			  $emailTo .= $managerEmail['tkemail'].",";
			}
		}		
		$emailTo = rtrim($emailTo, ",");
		return $emailTo;
	}
	
	/******************
	ADDED BY: Janet Currie
	ADDED ON: 14 JULY 2014
	To replace getRiskManagerEmails and getRiskManager
	*******************/
	//emailsOnly = true returns array of name & email for use in ASSIST_EMAIL class
	function getRiskManagers($emailsOnly=false) {
		$m = array();
		$db = new ASSIST_DB();
		if($emailsOnly) {
			$sql = "SELECT DISTINCT CONCAT(TK.tkname,' ',TK.tksurname) as name, TK.tkemail as email ";
		} else {
			$sql = "SELECT DISTINCT TK.tkid, CONCAT(TK.tkname,' ',TK.tksurname) as name, TK.tkemail as email, UA.* ";
		}
		$sql.= " FROM assist_".$db->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$db->getCmpCode()."_menu_modules_users MMU
					ON MMU.usrtkid = TK.tkid AND MMU.usrmodref = '".$db->getModRef()."'
				INNER JOIN ".$db->getDBRef()."_useraccess UA
					ON UA.user = TK.tkid AND UA.risk_manager = 1
				WHERE TK.tkstatus = 1 AND TK.tkid <> '0000'";
		$m = $db->mysql_fetch_all($sql);
		unset($db);
		return $m;
	}
	function getCurrentUser() {
		$db = new ASSIST_DB();
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, tkemail as email
				FROM assist_".$db->getCmpCode()."_timekeep
				WHERE tkid = '".$db->getUserId()."'";
		$row = $db->mysql_fetch_one($sql);
		unset($db);
		return $row;
	}
	function getActionOwnerEmail($ao,$emailFormat=false) {
		$db = new ASSIST_DB();
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, tkemail as email
				FROM assist_".$db->getCmpCode()."_timekeep
				WHERE tkid = '".$ao."'";
		$row = $db->mysql_fetch_one($sql);
		unset($db);
		if($emailFormat) { unset($row['tkid']); }
		return $row;
		//return $sql;
	}
	function getRiskOwnerEmails($subid) {
		$db = new ASSIST_DB();
		$to = array();
		$sql = "SELECT CONCAT(tk.tkname,' ',tk.tksurname) as name, tk.tkemail as email 
				FROM ".$db->getDBRef()."_dir_admins a
				INNER JOIN assist_".$db->getCmpCode()."_menu_modules_users mmu
				  ON a.tkid = mmu.usrtkid AND mmu.usrmodref = '".$db->getModRef()."'
				INNER JOIN assist_".$db->getCmpCode()."_timekeep tk
				  ON tk.tkid = mmu.usrtkid AND tk.tkstatus = 1
				WHERE a.type = 'SUB' AND a.ref = $subid AND a.active = 1";
		$to = $db->mysql_fetch_all($sql);
		unset($db);
		return $to;
	}
	
	/**
		Fetched the email of the user who has been asssigned and action or risk
	**/
	function getUserResponsiblityEmail( $id ) {
		$result  = $this -> getRow("SELECT tkemail, tkname, tksurname FROM assist_".strtolower($_SESSION['cc'])."_timekeep WHERE tkid = '".$id."'");
		return $result;
	}
	
	function getUserEmailUnderDirectorate( $ref )
	{
		$response = $this->get("SELECT TK.tkemail 
								FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK 
								INNER JOIN  ".$_SESSION['dbref']."_dir_admins DA ON DA.tkid = TK.tkid 
								INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								WHERE DA.ref = '".$ref."' AND TK.tkstatus = 1 AND MU.usrmodref = '".strtoupper($_SESSION['modref'])."'
								");
		if(!empty( $response)) {
				foreach($response as $userEmail ) {
				  $emailTo .= $userEmail['tkemail'].",";
				}
			}
		$emailTo = rtrim($emailTo, ",");		
		return $emailTo;
	}
	
	
	function setRiskManager( $id, $status )
	{
		$updatedata = array(
						'risk_manager'  => $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'useraccess', $updatedata, "user=$id" );
		echo $response;
	}

	function isRiskManager()
	{
		$response = $this -> getRow("SELECT risk_manager 
									 FROM ".$_SESSION['dbref']."_useraccess 
									 WHERE user = '".$_SESSION['tid']."' "
									);
		if( $response['risk_manager'] == 1 ) 
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getUserRefDirectorate()
	{
		$response = $this -> get("SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
								   WHERE tkid = '".$_SESSION['tid']."' AND type = 'SUB' AND active = 1 ");
		$riskRefs = array();
		foreach( $response as $row ){
			//$riskRefs .= "'".$row['ref']."',";
			$riskRefs[] = $row['ref'];
		}
//		return rtrim( ltrim( rtrim($riskRefs, ","), "'") , "'");
		return implode(",",$riskRefs);
	}
	
	function getManagerUsers()
	{
		$response = $this -> get( "SELECT TK.tkname, TK.tksurname, UA.user
								   FROM  ".$_SESSION['dbref']."_useraccess UA 
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
									INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE UA.insertuser = '".$_SESSION['tid']."' AND TK.tkstatus = 1 AND MU.usrmodref = '".strtoupper($_SESSION['modref'])."'	
								   " 
								);
		return $response;
	}
	
}
?>
