<?php
class HtmlDisplay implements Display
{

	function displayResults( $data ,$optional)
	{
		echo "<center>";
		echo "<h1>".$optional['company']."</h1>";
		echo "<h1>".$optional['title']."</h1>";
		echo "<table>";
			echo "<tr>";
			foreach( $optional['headers'] as $field => $value){
				echo "<th>";
					echo $value;
				echo "</th>";
			}
			echo "</tr>";
			foreach( $data as $index => $valArr){
				echo "<tr>";
				foreach( $valArr as $key => $value)
				{
					echo "<td>".$value."</td>";
				}
				echo "</tr>";
			}
		echo "</table>";
		echo "</center>";
	}
}