<?php
$scripts = array( 'financial_year.js','menu.js', 'functions.js' );
$styles = array( 'colorpicker.css' );
$page_title = "Finicial Year";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="financial_year_message" class="message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="financial-year-form" name="financial-year-form" method="post">
			<table border="1" id="financial_year_table">
			  <tr>
			    <th>Ref</th>
			    <th>Last day of Financial year</th>
			    <th>Start date</th>
			    <th>End dtae</th>
			    <th></th>
			  </tr>
			  <tr id="show_loading">
			    <td colspan="5" align="center" id="display_message">Loading financial years  .... <img src='../images/loaderA32.gif' /> </td>
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>Year Ending : <input type="text" value="" id="year_ending" name="year_ending" class="date" /></td>
			    <td><input type="text" name="start_date" id="start_date" class="date" /></td>
			    <td><input type="text" name="end_date" id="end_date" class="date" /></td>
			    <td><input type="submit" value="Add" id="add_fin_year" /></td>
			  </tr>
			</table>
			 </form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "financial_years_logs" , true); ?></td>
	</tr>
</table>
</div>


