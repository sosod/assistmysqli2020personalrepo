<?php
/**
	* Likelihood 
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ingite Assit
**/
class Likelihood extends DBConnect
{
	
	protected $rating_from;
	protected $rating_to;
	protected $assessment;
	protected $definition;
	protected $probability;
	protected $color;

	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
	
	function __construct( $rating_from="", $rating_to="", $assessment="", $definition="", $probability="",  $color="FFFFFF" )
	{
		$this -> rating_from = trim($rating_from);
		$this -> rating_to 	 = trim($rating_to);
		$this -> assessment  = trim($assessment);
		$this -> definition  = trim($definition);
		$this -> probability = trim($probability);		
		$this -> color 		 = trim($color);						
		parent::__construct();
	}

	function saveLikelihood()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"probability" => $this -> probability,						
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "likelihood" , $insert_data );
		echo $response;//$this -> insertedId();
	}	
	
	function updateLikelihood( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"probability" => $this -> probability,						
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getALikelihood( $id );
        $logsObj -> setParameters( $insert_data, $data, "likelihood");												
		$response = $this -> update( "likelihood" , $insert_data , "id = $id");
		return $response;		
	}		
		
	function getLikelihood()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_likelihood WHERE  active <> 2" );
		foreach($response as $key => $r) {
			$response[$key]['name'] = $r['assessment'];
			if(strlen($r['probability'])>0 && strpos($r['probability'],"%")!=0) {
				$response[$key]['name'].=" (".$r['probability'].(strpos($r['probability'],"%")>0 ? "" : "%").")";
			}
		}
		return $response;
	}

    public function getActiveLikelihood()
    {
        $response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_likelihood WHERE  active <> 2 AND active = 1" );
        return $response;
    }
	function getActiveForEdit()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_likelihood "); //WHERE active = 1 " );
		$data = array(
			'active'=>array(),
			'inactive'=>array()
		);
		foreach($response as $key => $r) {
			$response[$key]['name'] = $r['assessment']." (".$r['probability'].")";
			if($r['active']==1) {
				$data['active'][$key] = $response[$key];
			} else {
				$data['inactive'][$key] = $response[$key];
			}
		}
		
		return $data;
	}
	
	function getALikelihood( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_likelihood WHERE id = $id" );
		$response['from'] = $response['rating_from'];
		$response['to'] = $response['rating_to'];
		$response['diff'] = $response['to'] - $response['from'];
		return $response;
	}
	
	function getLikelihoodRating( $id )
	{
		$response = $this -> get( "SELECT rating_from, rating_to FROM ".$_SESSION['dbref']."_likelihood WHERE id = '".$id."' " );
		$diff = $to = $from = 0;
		foreach($response as $row ) {
			$diff 	= $row['rating_to'] - $row['rating_from'];
			$to 	= $row['rating_to'];
			$from 	= $row['rating_from'];
		} 
		
		echo json_encode( array( "from" => $from, "to" => $to , "diff" => $diff) );
	}	

	function updateLikelihoodStatus( $id , $status)
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getALikelihood( $id );
        $logsObj -> setParameters( $updatedata, $data, "likelihood");		
		$response = $this -> update( 'likelihood', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateLikelihood( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getALikelihood( $id );
        $logsObj -> setParameters( $update_data, $data, "likelihood");		
		$response = $this -> update( 'likelihood', $update_data, "id=$id" );
		echo $response;
	}


	
	
	
	
	
	
	
	
	function addItem($from, $to, $name,$def,$color,$probability) {
		$insert_data = array( 
						"rating_from"		=> $from,
						"rating_to"		=> $to,
						"assessment"			=> $name,
						"definition"	=> $def,
						"color"			=> $color,
						"probability"			=> $probability,
						"active" 		  => Likelihood::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "likelihood" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $from, $to, $name, $def, $color, $probability )
	{
		$insertdata = array(
						"rating_from" 			 => $from,
						"rating_to" 			 => $to,
						"assessment"		 => $name,
						"definition" 		 => $def,
						"color" 		 => $color,
						"probability" 		 => $probability,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getALikelihood( $id );
		$var = array(
			'id'=>$id,
			'rating_from'=>$from,
			'rating_to'=>$to,
			'assessment'=>$name,
			'definition'=>$def,
			'color'=>$color,
			'probability'=>$probability,
		);
        $logsObj -> setParameters( $var, $data, "likelihood");
		$response = $this -> update( "likelihood", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getALikelihood( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "likelihood");
		$response = $this -> update( 'likelihood', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
		
	
	
	
	
		
		
		
	
	function getReportList() {
		$sql = "SELECT DISTINCT L.* FROM ".$_SESSION['dbref']."_likelihood L 
				INNER JOIN ".$_SESSION['dbref']."_risk_register R
				ON R.likelihood = L.id
				WHERE L.active <> 2 
				ORDER BY rating_from, rating_to, probability, assessment"; 
		$response = $this -> get( $sql );
		$data = array();
		foreach($response as $r) {
			$m = strlen($r['probability'])>0 ? " (".$r['probability'].")" : "";
			$data[] = array(
				'id'=>$r['id'],
				'name'=>$r['assessment'].$m,
			);
		}
		return $data;
	}
	
	
	
}
?>