<?php
/**
	* Inherent Risk
	* @author	: admire<azinamo@gmail.com>
	* @copyright: 2011 , Ignite Assist
**/
class InherentRisk extends DBConnect
{
	protected $rating_from;
	protected $rating_to;
	protected $magnitude;
	protected $response;
	protected $color;
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
		
	function __construct( $rating_from="", $rating_to="", $magnitude="", $response="",  $color="FFFFFF" )
	{
		$this -> rating_from = trim($rating_from);
		$this -> rating_to 	 = trim($rating_to);
		$this -> magnitude   = trim($magnitude);
		$this -> response    = trim($response);
		$this -> color 		 = trim($color);						
		parent::__construct();
	}
		
	function saveInherentRisk()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "inherent_exposure" , $insert_data );
		echo $response;//$this -> insertedId();
	}	
	
	function updateInherentRisk( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getAInherentRisk( $id );
        $logsObj -> setParameters( $insert_data, $data, "inherent_exposure");						
		$response = $this -> update( "inherent_exposure" , $insert_data , "id=$id");
		return $response;		
	}		
		
	function getActiveForEdit() {
		$response = $this->getIR();
		$data = array(
			'active'=>array(),
			'inactive'=>array(),
		);
		foreach($response as $key=>$r) {
			$r['name']=$r['magnitude'];
			if($r['active']==1) {
				$data['active'][$key] = $r;
			} else {
				$data['inactive'][$key] = $r;
			}
		}
		return $data;
	}
		
		
	function getInherentRisk()
	{
		echo json_encode( $this->getIR() );
	}

	function getIR()
	{
		$response = $this -> get( "SELECT *, magnitude as name FROM ".$_SESSION['dbref']."_inherent_exposure WHERE active <> 2" );
		/*foreach($response as $key => $r) {
			$response[$key]['name'] = $r['magnitude'];
		}*/
		return $response;
	}
	
	function getAInherentRisk( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure WHERE id = $id" );
		return $response ;
	}

	function updateInherentRiskStatus( $id, $status )
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAInherentRisk( $id );
        $logsObj -> setParameters( $updatedata, $data, "inherent_exposure");								
		$response = $this -> update( 'inherent_exposure', $updatedata, "id=$id" );
		echo $response;
	}
	/**
		Fetches al the inherent risk and create the range group associating it with a color
		@return array
	**/
	function getInherentRiskRange()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure " );
		$rangesColor = array();
		foreach($response as $key => $row ) 
		{
			$range = "";
			// create a string of the range 
			for( $i = $row['rating_from']; $i <= $row['rating_to'] ; $i++ ) 
			{
				$range .= $i.","; 
			}
			//create an array with the range and the associated color
			$rangesColor[$row['magnitude']] = array( "color" => $row['color'], "range" => rtrim($range,",") );	
		}
		return $rangesColor;
	}
	
	function getRiskInherentRange( $value )
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure " );
		$rangesColor = "";
		
		foreach($response as $key => $resArray ) 
		{
			//$range = "";
			// create a string of the range 
			for( $i = $resArray['rating_from']; $i <= $resArray['rating_to'] ; $i++ ) 
			{
				if( $i == $value ) 
				{
					$rangesColor['color'] 	  = $resArray['color'];
					$rangesColor['magnitude'] = $resArray['magnitude'];	
					$rangesColor['response'] = $resArray['response'];	
				}
				
			}
			//create an array with the range and the associated color
		}
		return $rangesColor;
	}	
	
	function activateInherentRisk( $id )
	{
		$update_data = array(
					'active'		 => "1",
					"insertuser"     => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAInherentRisk( $id );
        $logsObj -> setParameters( $update_data, $data, "inherent_exposure");								
		$response = $this -> update( 'inherent_exposure', $update_data, "id=$id" );
		echo $response;
	}

	
	

	
	
	
	
	
	
	
	
	function addItem($from, $to, $name,$def,$color) {
		$insert_data = array( 
						"rating_from"		=> $from,
						"rating_to"		=> $to,
						"magnitude"			=> $name,
						"response"	=> $def,
						"color"			=> $color,
						"active" 		  => InherentRisk::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "inherent_exposure" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $from, $to, $name, $def, $color )
	{
		$insertdata = array(
						"rating_from" 			 => $from,
						"rating_to" 			 => $to,
						"magnitude"		 => $name,
						"response" 		 => $def,
						"color" 		 => $color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAInherentRisk( $id );
		$var = array(
			'id'=>$id,
			'rating_from'=>$from,
			'rating_to'=>$to,
			'magnitude'=>$name,
			'response'=>$def,
			'color'=>$color,
		);
        $logsObj -> setParameters( $var, $data, "inherent_exposure");
		$response = $this -> update( "inherent_exposure", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAInherentRisk( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "inherent_exposure");
		$response = $this -> update( 'inherent_exposure', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	

	function findExposure($rating) {
		$sql = "SELECT id, magnitude as name, rating_from, rating_to FROM ".$_SESSION['dbref']."_inherent_exposure WHERE rating_from <= $rating AND rating_to >= $rating AND active <> ".InherentRisk::DELETED;
		$r = $this->getRow($sql);
		if(!isset($r['name'])) {
			$sql = "SELECT id, magnitude as name, rating_from, rating_to FROM ".$_SESSION['dbref']."_inherent_exposure WHERE rating_to < $rating AND active <> ".InherentRisk::DELETED." ORDER BY rating_to DESC LIMIT 1";
			$r = $this->getRow($sql);
		}
		return $r;
	}	
	
		
	
	
	
	
		
		
		
	
	function getReportList() {
		$sql = "SELECT L.id, L.magnitude as name, L.rating_from, L.rating_to FROM ".$_SESSION['dbref']."_inherent_exposure L 
				WHERE L.active <> 2 
				ORDER BY rating_from"; 
		$response = $this -> get( $sql );
/*		$data = array();
		foreach($response as $r) {
			//$m = $r['rating_from'];
			//if($r['rating_from']!=$r['rating_to']) { $m.=" - ".$r['rating_to']; }
			$data[] = array(
				'id'=>$r['id'],
				'name'=>$r['magnitude']." ($m)",
			);
		}
		return $data;*/
		return $response;
	}
	
	
	
}
?>