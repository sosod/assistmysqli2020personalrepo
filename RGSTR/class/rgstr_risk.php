<?php 

class RGSTR_RISK extends RGSTR {

	protected $section = "risk";
	protected $attachment_folder = "risk";
	protected $tablename = "risk_register";

	public function __construct() {
		parent::__construct();
	}
	
	public function getRecord($i) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_".self::tablename." WHERE id = ".$i;
		return $this->mysql_fetch_one($sql);
	}
	
	public function getParentRecord($id=0) {
		return array();
	}
	
	public function getFormattedRecord($i) {
		$row = $this->getRecord($i);
		$a = strlen($row['attachment']) > 0 ? unserialize(base64_decode($row['attachment'])) : array();
		$row['attachment'] = $a;
		return $row;
	}

	public function getRisksWithActionsForApproval($sub_ids=array(),$folder) {
		if(count($sub_ids)==0) {
			$userObj = new RGSTR_USER();
			if($folder=="admin") {
				$subs = $userObj->getAllDirectorates();
			} else {
				$subs = $userObj->getMyDirectorates();
			}
			$sub_ids = array_keys($subs);
		}
		if(count($sub_ids)>0) {
			$db = new ASSIST_DB();
			$sql = "SELECT R.id, R.description, R.sub_id 
					FROM ".$db->getDBRef()."_risk_register R 
					INNER JOIN ".$db->getDBRef()."_actions A
					  ON A.risk_id = R.id AND (A.action_status & ".RiskAction::ACTION_AWAITING." = ".RiskAction::ACTION_AWAITING.")
					WHERE R.sub_id IN (".implode(",",$sub_ids).")";
			$result = $db->mysql_fetch_all_by_id($sql,"id");
		} else {
			$result = array();
		}
		return $result;
	}
	public function getRisksWithApprovedActions($sub_ids=array(),$folder) {
		if(count($sub_ids)==0) {
			$userObj = new RGSTR_USER();
			if($folder=="admin") {
				$subs = $userObj->getAllDirectorates();
			} else {
				$subs = $userObj->getMyDirectorates();
			}
			$sub_ids = array_keys($subs);
		}
		if(count($sub_ids)>0) {
			$db = new ASSIST_DB();
			$sql = "SELECT R.id, R.description, R.sub_id 
					FROM ".$db->getDBRef()."_risk_register R 
					INNER JOIN ".$db->getDBRef()."_actions A
					  ON A.risk_id = R.id AND (A.action_status & ".RiskAction::ACTION_APPROVED." = ".RiskAction::ACTION_APPROVED.")
					WHERE R.sub_id IN (".implode(",",$sub_ids).")";
			$result = $db->mysql_fetch_all_by_id($sql,"id");
		} else {
			$result = array();
		}
		return $result;
	}
}

?>