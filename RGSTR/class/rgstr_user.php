<?php 

class RGSTR_USER extends RGSTR {

	protected $section = "";
	protected $attachment_folder = "";
	protected $tablename = "";


	public function __construct() {
		parent::__construct();
	}

	public function getMyDirectorates($tkid = "") {
		if(strlen($tkid)==0) {
			$tkid = $this->getUserID();
		}
		$db = new ASSIST_DB();
		$sql = "SELECT DA.ref as id, CONCAT(D.dirtxt,' - ',S.subtxt) as name 
				FROM ".$db->getDBRef()."_dir_admins DA
				INNER JOIN ".$db->getDBRef()."_dirsub S
				ON S.subid = DA.ref AND S.active = 1
				INNER JOIN ".$db->getDBRef()."_dir D
				ON S.subdirid = D.dirid AND D.active = 1
				WHERE DA.active = 1 AND DA.tkid = '".$tkid."' AND DA.type = 'SUB'
				ORDER BY D.dirsort, S.subsort"; //echo $sql;
		$result = $db->mysql_fetch_all_by_id($sql,"id");
		return $result;
	}

	public function getAllDirectorates() {
		$db = new ASSIST_DB();
		$sql = "SELECT S.subid as id, CONCAT(D.dirtxt,' - ',S.subtxt) as name 
				FROM ".$db->getDBRef()."_dirsub S
				INNER JOIN ".$db->getDBRef()."_dir D
				ON S.subdirid = D.dirid AND D.active = 1
				WHERE S.active = 1 
				ORDER BY D.dirsort, S.subsort"; //echo $sql;
		$result = $db->mysql_fetch_all_by_id($sql,"id");
		return $result;
	}

}

?>