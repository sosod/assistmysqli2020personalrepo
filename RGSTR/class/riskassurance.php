<?php
/**
	* @package 		:  Risk Assurance 
	* @author 		: admire<azinamo@gmail.com>
	* @copyright	: 2011 Ignite Assist	
**/
class RiskAssurance extends DBConnect
{
	/**
		@var int
	**/
	protected $id;
	/**
		@var int
	**/
	protected $risk_id;
	/**
		@var date
	**/
	protected $date_tested;
	/**
		@var char
	**/
	protected $response;
	/**
		@var int
	**/
	protected $signoff;
	/**
		@var char
	**/
	protected $assurance_provider;
	/**
		@var int
	**/
	protected $attachment;
	/**
		@var char
	**/
	protected $active;
	
	function __construct( $risk_id, $date_tested, $response, $signoff, $assurance_provider, $attachment )
	{
		$this -> risk_id 			= $risk_id;
		$this -> date_tested 		= $date_tested;			
		$this -> response 			= $response;
		$this -> signoff 			= $signoff;			
		$this -> assurance_provider = $assurance_provider;
		$this -> attachment 		= $attachment;		
		parent::__construct();				
	}
	
		
	function  saveRiskAssurance()
	{
		$insertdata = array(
					'risk_id' 			=> $this -> risk_id,
					'date_tested' 		=> $this -> date_tested ,
					'response' 			=> $this -> response,
					'signoff'			=> $this -> signoff,		
					'attachment' 		=> $this -> attachment,
					"insertuser"  		=> $_SESSION['tid'],					
		);

		$response  = $this -> insert( 'assurance' , $insertdata);
		 return $response;//$this->insertedId();
	}
		
	function getRiskAssurance( $start, $limit)
	{								
								      
		$response = $this -> get( "SELECT 
								   A.id,
								   CONCAT(TK.tkname,' ', TK.tksurname) AS assurance_provider,
								   A.response,
								   DP.value AS department,
								   A.date_tested,
								   A.signoff, 
								   A.attachment				   
								   FROM ".$_SESSION['dbref']."_assurance A
								   INNER JOIN  assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
								   INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   WHERE risk_id = '".$this -> risk_id."'
								   AND status & 2 <> 2 
								   ORDER BY A.insertdate DESC LIMIT $start,$limit  " );
		$assuranceArr 	= array();
		$assuranceAttachments = array();
		foreach( $response as $index => $valArr)
		{
			foreach( $valArr as $key => $val)
			{
				if( $key == "attachment")
				{
					$a = "";
					if( !empty($val))
					{
						$attachment  = unserialize($val);
						foreach( $attachment as $att => $value)
						{
							 $id  =  $att;
							 $ext = substr( $value , strpos($value, ".")+1);
							 $file = $att.".".$ext;
							 if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/".$file)){
								 $a .= "<a href='download.php?folder=risk&file=".$file."&new=".$value."&content=".$ext."&company=".$_SESSION['cc']."&modref=".$_SESSION['modref']."'>".$value."</a><br />";
								 $assuranceAttachments[$valArr['id']][$att] =  $value;						
							 } 							 
						}						
					}
					if($a == ""){
						$assuranceArr[$index][$key] = "";
					} else {
						$assuranceArr[$index][$key] = $a;
					}
					
				} else {
					$assuranceArr[$index][$key] = $val;
				}
			} 			
		} 
		return array( "data" => $assuranceArr, "attachments" => $assuranceAttachments, "total" => $this->_getTotalRiskAssurance() );
	}
	
	
	function getARiskAssurance( $id )
	{
		$result = $this->getRow("SELECT id, response, signoff, attachment FROM #_assurance WHERE id = {$id} ");
		return $result;	
	}
	
	function _getTotalRiskAssurance()
	{								
								      
		$response = $this -> getRow("SELECT 
								   COUNT(*) AS total
								   FROM ".$_SESSION['dbref']."_assurance A
								   INNER JOIN  assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
								   INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   WHERE risk_id = '".$this -> risk_id."'" );
		return $response['total'];
	}
	
		
	function updateRiskAssurance( $id )
	{
		$updatedata = array(
					'risk_id' 			=> $this -> risk_id,
					'date_tested' 		=> $this -> date_tested,
					'response' 			=> $this -> response,
					'signoff'			=> $this -> signoff,		
					'attachment' 		=> $this -> attachment,
					"insertuser"  		=> $_SESSION['tid'],					
		);
		
		$response  = $this -> update( 'assurance' , $updatedata, "id=$id");
		return $response;
	}
	
	function deleteRiskAssurance( $id ){
		$res =  $this -> update("assurance", array("status" => 2), "id=$id");
		return $res;	
	}
} 
?>