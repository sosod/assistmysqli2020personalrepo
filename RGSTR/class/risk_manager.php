<?php
$scripts = array( 'risk_manager.js','menu.js', 'functions.js' );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form method="post" name="risk-manager-form" id="risk-manager-form"> 
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<table border="1" id="risk_manager_table" width="40%">
			  <tr>
				<th>Ref</th>
				<th>User</th>
				<th>Action</th>
			  </tr>
			  <tr>
				<td>#</td>
				<td>
					<select id="risk_manager_users" name="risk_manager_users">
						<option value="">--user--</option>
					</select>
				</td>
				<td>
					<input type="submit" name="update_risk_manager" id="update_risk_manager" value="Set as risk manager" />
				</td>
			  </tr>
			</table>
			</form>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>
</div>