<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 2013/09/20
 * Time: 4:11 AM
 * To change this template use File | Settings | File Templates.
 */

class MasterFinancialYear extends DBConnect
{

    protected  $table = 'master_financialyears';
	protected $me;

    public function __construct() {
        parent::__construct();
		$this->me = new ASSIST_MASTER_FINANCIALYEARS();
    }

    public function retrieveMasterFinacialYears($filter="") {
		if($filter=="active") {
			//$filter = "".ASSIST_MASTER_FINANCIALYEARS::STATUS_FLD." & ".ASSIST_MASTER_FINANCIALYEARS::ACTIVE." = ".ASSIST_MASTER_FINANCIALYEARS::ACTIVE."";
			return $this->me->getActive();
		} else {
			//$financial_years =  $this->get("SELECT * FROM assist_".$_SESSION['cc']."_".$this->table.(strlen($filter)>0 ? " WHERE ".$filter : "")." ORDER BY start_date, end_date, value");
			return $this->me->getAll();
		}
        //return $financial_years;
    }
	
	function getReportList()
	{
		$data = array();
		$sql = "SELECT fy.id, fy.value FROM assist_".$_SESSION['cc']."_".$this->table." fy INNER JOIN #_risk_register rr ON rr.financial_year_id = fy.id ORDER BY fy.start_date, fy.end_date, fy.value";  
		$response =  $this->get($sql);
		foreach($response as $d) {
			$data[$d['id']] = array(
				'id'=>$d['id'],
				'name'=>$d['value'],
			);
		}
		return $data;
	}	
	

}