<?php
@session_start();
include_once("../inc/init.php");
/*function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	}
}*/
switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ){
	
case "getActions":
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	echo json_encode( $act -> getRiskActions('', '', '', '', '', '', '', '', '') );
	break;
case "getActionsToApprove":
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	echo json_encode( $act -> getActionToApprove() );
	break;
case "getRisk":
	$act = new Risk("", "", "", "", "", "", "","","", "", "", "", "" );
	echo json_encode( $act -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getARisk":
	$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
	echo json_encode( $risk -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getRiskAction( $_REQUEST['id'] ) );
	break;
case "getAAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getAAction( $_REQUEST['id'] ) );
	break;	
case "getUserAccess":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "");
	echo json_encode( $uaccess -> getUserAccess() );
	break;	
case "getUsers":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
	echo json_encode( $uaccess -> getUsers() );
	break;
case "getStatus":
	$stat = new RiskStatus("", "", "");
	echo json_encode( $stat -> getStatus() );
	break;
case "newRiskAction":
	$act = new RiskAction( $_REQUEST['id'],
						   $_REQUEST["r_action"],
						   $_REQUEST["action_owner"],
						   $_REQUEST["deliverable"],
						   $_REQUEST["timescale"],
						   $_REQUEST["deadline"],
						   $_REQUEST["remindon"],
						   $_REQUEST['progress'],
						   $_REQUEST['status']
						  );
		$id 	   = $act -> saveRiskAction();
		$response  = array(); 
		if( $id > 0 ) {
			$response["id"] 			  = $id;
			$response['success']["saved"] = "New Action #".$id." has been successifully saved";
			$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			     		  = $user -> getRiskManagerEmails();
			$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
			$risk 						  = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
			$riskInfo 					  = $risk -> getRisk($_REQUEST['id']);			
			$email 			     		  = new Email();	
			$email -> userFrom   		  = $_SESSION['tkn'];
			$email -> subject    		  = "New Action (Ref: ".$id.") created on Ignite Assist";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			$message   = ""; 
			$message .= "".$email -> userFrom." created an Action relating to risk #".$_REQUEST['id'].
			$message .= " : ".$riskInfo['description']." identified in the Risk Register. The details of the action are as follows :";
			$message .= " \r\n\n";	//blank row above Please log on line
			$message .= "Action Number : ".$id."\r\n"; 
			$message .= "Action : ".$_REQUEST['action']."\r\n";
			$message .= "Deliverable : ".$_REQUEST['deliverable']."\r\n";
			$message .= "Action Owner : ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
			$message .= "Time Scale : ".$_REQUEST['timescale']."\r\n";
			$message .= "Deadline : ".$_REQUEST['deadline']."\r\n";
			$message .= " \r\n\n";	//blank row above Please log on line																		
			$message .= "<i>Please log onto Ignite Assist in order to View or Update the Actions.</i> \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";
			foreach($users as $userEmail ) {
				$emailTo = $userEmail['tkemail'].";";
			}
			$email -> to 	   = $emailTo.";".$responsibleUserEmail['tkemail'];		
			$email -> body     = $message;
			$emailTo 		   = "";
			foreach($users as $userEmail ) {
				$emailTo = $userEmail['tkemail'].";";
			}
			$email -> to 	   = $responsibleUserEmail['tkemail'];
			$email -> from     = "info@ingite4u.com";
			if( $email -> sendEmail() ) {
				$response['success']["emailsent"] = "and a notification has been sent";
			} else {
				$response['error']["emailsent"] = "There was an error sending the email";
			}
		} else {
			$response['error']["saved"] = "Error saving the action";
		}
		echo json_encode( $response );
	break;
case "deleteAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	$act -> deleteRiskAction( $_REQUEST['id'] );
	break;
case "updateRiskAction":
	$act = new RiskAction("",
						  $_REQUEST["r_action"],
						  $_REQUEST["action_owner"],
						  $_REQUEST["deliverable"],
						  $_REQUEST["timescale"],
						  $_REQUEST["deadline"],
						  $_REQUEST["remindon"],
						  "",
						  (isset($_REQUEST['status']) ? $_REQUEST['status'] : "" ) 
						 );
	$action 			=  $act -> getAAction( $_REQUEST['id'] ) ;
	$action['remindon'] = (isset($action['updatedremindon']) ? $action['updatedremindon'] : $action['remindon']);
	$changesArr		= array();
	$changeMessage = "";
	//find the difference made to the action 
	foreach( $_REQUEST as $key => $value)
	{
		if( isset($action[$key]) ||  array_key_exists($key, $action))
		{
			if( $key == "action"){
				continue;
			} else if( $value !== $action[$key] ){
				$changeMessage    .= ucwords($key)." changed to ".$value." from ".$action[$key]." \r\n"; 
				$changesArr[$key]  = array("from" => $action[$key], "to" => $value); 
			} 
		}
		if( $key == "r_action") {
			if( $value !== $action['action'] ){
				$changeMessage 		  .= "Action changed to ".$_REQUEST['r_action']." from ".$action['action']." \r\n"; 
				$changesArr['action']  = array( "from" => $action['action'], "to" => $_REQUEST['r_action'] ); 				
			}
		}
	}
	$changesArr['user'] = $_SESSION['tkn'];
	$res 				= $act -> updateRiskAction( $_REQUEST['id'] , serialize($changesArr) );
	$response 			= array();
	if( $res > 0 ){
		$responeText 				  = "Action #".$_REQUEST['id']." has been successifully edited";
		$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
		$users 			     		  = $user -> getRiskManagerEmails();
		$email 			     		  = new Email();
		$email -> userFrom   		  = $_SESSION['tkn'];
		$email -> subject    		  = "Edit Action";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message   = ""; 
		//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		$message .= " \n";	//blank row above Please log on line
		
		$message .= $email -> userFrom." edited action #".$_REQUEST['id']." related to risk #".$action['risk_id']."";
		$message .= " : ".$action['description']."\r\n\n";		
		if($changeMessage == ""){
			$message  .= "";
		} else {
			$message .= "The following changes were made : \r\n\n";
			$message .= $changeMessage;
		}
		$message .= "\r\n Please log onto Ignite Assist in order to View or Update the Action.\r\n";
		$message  = nl2br($message);		

		$email -> body     = $message;
		$emailTo 		   = "";
		foreach($users as $userEmail ) {
			$emailTo = $userEmail['tkemail'].";";
		}
		$email -> to 	   = rtrim($emailTo,";");
		$email -> from     = "info@ingite4u.com";
		/*
		if( $email -> sendEmail() ) {
			$response = array("error" => false, "text" => $responeText."\r\n\n Email send succesiifuly \r\n" );
		} else {
			$response = array("error" => true , "text" => $responeText."\r\n\n There was an error sending the email \r\n");
		} 	*/
	} else{
		$response = array("error" => true, "text" => "Error occured updating the action ".$res);
	} 
	echo json_encode( $response );
	break;
case "getEditLog":
	$act 	 		= new RiskAction("", "", "", "", "", "", "", "", "" );
	$editLog 		= $act -> getActionEditLog( $_REQUEST['id'] );
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $editLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been edited by ".$changes['user']."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else{
				   $changeMessage .= ucwords($key)." has changed from ".$change['from']." to ".$change['to']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	}
	echo json_encode( $changeLog );
	break;
case "getActionUpdates":
	$act 			= new RiskAction("", "", "","" , "", "", "", "", "");
	$updatesLog     = $act -> getActionUpdates( $_REQUEST['id'] ); 	
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $updatesLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been updated by ".(isset($changes['user']) ? $changes['user'] : "")."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else{
				   $changeMessage .= ucwords($key)." has changed from ".$change['from']." to ".$change['to']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	}
	echo json_encode( $changeLog );
	break;
case "newActionUpdate":
	$act 			= new RiskAction("", "", "","" , "", "", $_REQUEST["remindon"], $_REQUEST['progress'], $_REQUEST['status']);
	$action 		= $act -> getAAction( $_REQUEST['id'] ) ;
	$status 		= new ActionStatus("","", "");
	$actionstatus	= $status->getAStatus( $_REQUEST['status'] ); 
	$changesArr 	= array();
	$changeMessage	= "";
	$requestArr 	= $_REQUEST;
	$action['progress']   = ((isset($action['updatedprogres']) ? $action['updatedprogres'] : $action['progres']) == "" ? 0 : (isset($action['updatedprogres']) ? $action['updatedprogres'] : $action['progres']));
	$requestArr['status'] = $actionstatus['name'];
	$action['status']	  = (isset($action['updatedstatus']) ? $action['updatedstatus'] : $action['status']);   
	foreach( $requestArr as $key => $value)
	{
		if( $key == "action"){
			continue;
		} 
		else if( isset($action[$key]) ||  array_key_exists($key, $action))
		{
			if( $value !== $action[$key] )
			{
			 $changeMessage  .= ucwords($key)." changed to ".$value."".($key == "progress" ? "%" : "")." from ".$action[$key]."".($key == "progress" ? "%" : "")." \r\n"; 
			 $changesArr[$key]  = array("from" => $action[$key], "to" => $value); 
			} 
		}
	}
	$changesArr['user'] = $_SESSION['tkn'];	
	$id 	= $act -> newRiskActionUpdate( 
											$_REQUEST['id'] ,
											$_REQUEST['description'],
											$_REQUEST['approval'],
											$_REQUEST['attachment'],
											serialize($changesArr)
										);
	$response  = array(); 
		if( $id > 0 ) {
			$response["id"] 			  = $id;
			$response['success']["saved"] = "New action update for action #".$_REQUEST['id']." has been successifully saved";
			$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			     		  = $user -> getRiskManagerEmails();		
			$email 			     		  = new Email();
			
			$email -> userFrom   		  = $_SESSION['tkn'];
			$email -> subject    		  = "Update Action";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
			$message .= " \n";	//blank row above Please log on line
			$message .= $_SESSION['tkn']." Updated action #".$id." related to risk #".$action['risk_id']." : ";
			$message .= " ".$action['description']."\r\n\n"; 
			if( $changeMessage == "")
			{
				$message  = "";
			} else{
				$message .= $changeMessage;
			}
			$message .= "Please log onto Ignite Assist in order to View further information on the Risk and or associated Actions \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";
			foreach($users as $userEmail ) {
				$emailTo = $userEmail['tkemail'].";";
			}
			$email -> to 	   = $emailTo;
			$emil -> to 	  .= $action['action_creator']; 
			$email -> from     = "info@ingite4u.com";
			if( $email -> sendEmail() ) {
				$response['success']["emailsent"] = "Email send succesiifuly";
			} else {
				$response['error']["emailsent"] = "There was an error sending the email";
			}
		} else {
			$response['error']["saved"] = "Error saving the action update";
		}
		echo json_encode( $response );
	break;
case "getActionAssurances":
	$action = new ActionAssurance($_REQUEST['id'], "", "", "", "", "", "");
	echo json_encode( $action -> getActionAssurances( $_REQUEST['id'] ) );
	break;
case "getActionAssurance":
	$action = new RiskAction("", "", "", "", "", "", "","", "");
	$action -> saveActionAssurance($_REQUEST['id'], "", $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment'], "" ) ;
	break;
case "newAssuranceAction":
	$newAssurance = new ActionAssurance( $_REQUEST['id'], $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment'], "" );
	$newAssurance -> saveActionAssurance() ;
	break;
case "newApprovalAction":
		$act = new RiskAction("", "", "", "", "", "", "","", "");
		$act -> approveAction(
								$_REQUEST['id'],
								$_REQUEST['response'],
								$_REQUEST['signoff'],
								($_REQUEST['notification'] == "on" ? "1" : "0"),
								$_REQUEST['attachment']
							);
	break;
case "searchActionToApprove":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> searchActionToApprove( $_REQUEST['searchtext'] ) );
	break;
}

?>