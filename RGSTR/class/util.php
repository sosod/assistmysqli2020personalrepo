<?php
class Util
{
     
     public static function toUserId($id)
     {
        return str_pad($id, 4, '0', STR_PAD_LEFT);
     }     
     
     public static function serializeEncode($data)
     {
        return base64_encode(serialize($data));
     }

     public static function serializeDecode($data)
     {
        return @unserialize(base64_decode($data));
     }

     public static function debug($data)
     {
        if(is_array($data))
        {
           print "<pre>";
               print_r($data);
           print "</pre>";
        } else {
           print "<pre>";
               var_dump($data);
           print "</pre>";     
        }
     }
     
     public static function isTextTooLong($text, $length = 10)
     {
        if(!empty($text))
        {
          $textArr  = explode(" ", $text);
          if(count($textArr) > $length)
          {
             return TRUE;
          } else {
             return FALSE;
          }
        } else {
          return FALSE;
        }
     }
     
     public static function cutString($text, $key, $length = 10)
     {
        $words = explode(" ", $text); 
        $newStr = "";
        for($i = 0; $i <= $length; $i++)
        {
          $newStr .= $words[$i]." "; 
        } 
        $value = "<span id='ref_".$key."'>".$newStr."<a href='#' id='view_".$key."' class='viewmore' style='color:orange;' title='".$text."'>more...</a></span>";
         return $value;
     }
     
     public static function cutText($text, $key, $length = 10)
     {
        $words = explode(" ", $text); 
        $newStr = "";
        for($i = 0; $i <= $length; $i++)
        {
          $newStr .= $words[$i]." "; 
        } 
        $value = $newStr." . . . ";
        return $value;
     }  
}
?>
