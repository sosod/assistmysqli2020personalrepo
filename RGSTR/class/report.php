<?php
/**
	Reports , this generates reports
	* @package	 : Report
	* @author 	 : admire<admire@trafficsynergy.com>
	* @copyright : 
**/
class Report extends  DBConnect
{

	protected $generateArray= array();
	
	protected $sql 			=  "";
	
	protected $where		=  "";
	
	protected $from 		=  "";
	
	protected $field		= "";
	
	protected $groupby		= "";
	
	protected $_errors 		= array();

	protected $addtionalHeaders 		= array(); 

	protected $headers = array();
	
	function __construct()
	{
		parent::__construct();
	}
	
	function generateReport( $postArr )
	{		
		$fields        = $this->prepareFields( isset($postArr['header']) ? $postArr['header'] : array() );
		$values        = $this->prepareWhere( $postArr['values'], $postArr['match'] );
		$joinTables	= $this->prepareJoinTables();
		$sortBy        = $this->prepareSortBy( isset($postArr['sort']) ? $postArr['sort'] : array() );
		$sortBy		 	= isset($sortBy) && !empty($sortBy) ? "ORDER BY ".$sortBy : "";
		$groupStr      = $this->prepareGroupBy( isset($postArr['group_by']) ? $postArr['group_by'] : array() );
		$groupBy       = ($groupStr == "" ? "" : " GROUP BY ".$groupStr);
		$results       = $this->get("SELECT DISTINCT(RR.id),".$fields." 
								 FROM #_risk_register RR
	 							 $joinTables
								 WHERE 1
								 $values
								 $groupBy
								 ORDER BY
								 $sortBy
							");	
		return $this->sortResults( $results );	
	}
	
	function sortResults( $response )
	{
		$nm 		= new RiskColumns();
		$res 	= new ResidualRisk( "", "", "", "", "");
		$ihr      = new InherentRisk("", "", "", "", "");		
		$headers 	= $nm -> getHeaderList();
		$colored  		= array(
								"control_rating" 	 => "controlRating",
								"impact_rating" 	 => "IMrating",
								"likelihood_rating"  => "LKrating",
								);				
		$riskArray  = array();
		foreach($response as $key => $risk )
		{
			//$updateArray 		= $this -> getRiskUpdate( $risk['risk_item'] );
			foreach($headers as $field => $value) 
			{
				if( $field == "action_progress" && in_array($field, $this->addtionalHeaders)){
					$actionObj 		= new RiskAction($risk['id'], "", "", "", "", "", "", "", "");
					$actionProgress = $actionObj -> getActionStats();
					$riskArray[$risk['id']][$field]  = round($actionProgress['averageProgress'], 2)."%";
					$this->headers[$field] 	= $value;
				} else if( isset( $risk[$field]) || array_key_exists($field, $risk) ) {
					//calculated
					if($field === "residual_risk_exposure" ) {		
						$riskExposureArr = $res -> getRiskResidualRanges( ceil($risk[$field]) );
						$color		= "";
						$magnitude  = "";
						if(isset($riskExposureArr) && !empty($riskExposureArr)){
							$color 		  = $riskExposureArr['color'];
							$magnitude 	  = $riskExposureArr['magnitude'];
						}
					 	$risk[$field] =  "<span style='width:50px; background-color:#".$color."'>&nbsp;&nbsp;&nbsp;".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					 	$this->headers[$field] 	= $value;
					}
					//calculted
					if($field === "inherent_risk_exposure" ) {
						$inherentResults  = $ihr -> getRiskInherentRange( round($risk[$field]) );
						$color 			  = "";
						$magnitude 	 	  = "";
						if(isset($inherentResults) && !empty($inherentResults)){
							$color 			= $inherentResults['color']; 
							$magnitude 	 	= $inherentResults['magnitude'];
						}
						$risk[$field] =   "<span style='width:50px; background-color:#".$color."'>&nbsp;&nbsp;&nbsp;".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
						$this->headers[$field] 	= $value;
					}

					if($field == "attachements" ) {
						$attched  = "";
						if( $risk[$field] !== "" || $risk[$field] !== NULL ){
							$attachments	= unserialize($risk[$field]);
							$countAttach    = count($attachments);
							if(	isset($attachments) && !empty($attachments)){
								foreach( $attachments as $attchName => $attach ){
									 $attched .= $attach."<br />";
								}
							}
						}
                        if($attched == "")
                        {
                                 $risk[$field] = "&nbsp;";
                             } else {
                                 $risk[$field] = $attched;
                             }
					}
	
					if( in_array($field, array_keys($colored)) ){
						$riskArray[$risk['id']][$field] 	= "<span style='width:50px; text-align:center; background-color:#".$risk[$field]."'>&nbsp;&nbsp;&nbsp;".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."&nbsp;&nbsp;&nbsp;</span>";
						$this->headers[$field] 	= $value;
					} else {
						$riskArray[$risk['id']][$field]  = $risk[$field];
						$this->headers[$field] 	= $value;
					}	
				}  
			}
		}					
		return $riskArray;
	}
	
	function getHeaders()
	{
		return $this->headers;		
	}
	
	function display( Display $display, $data ,$optional)
	{
		$display -> displayResults( $data, $optional);
	}
	
	function prepareFields( $fieldsArr )
	{
          $aliasArr = array(
           "id" 		  		          => "RR.id AS risk_item" ,
           "category"    	               => "RC.name AS risk_category" ,
           "description"  	               => "RR.description AS risk_description" ,
           "background"   	               => "RR.background AS risk_background" ,
           "status"  	  	               => "RS.name AS risk_status",
           "financial_exposure"	          => "FIN.name AS financial_exposure",
           "type"		  		          => "RRT.name AS risk_type",
           "level"		  		          => "RRL.name AS risk_level",
           "risk_owner"        	          => "D.dirtxt  AS risk_owner",
           "kpi_ref"           	          => "RR.kpi_ref",
           "actual_financial_exposure"       => "RR.actual_financial_exposure",
           "control_rating"    	          => "RCE.color AS control_rating, RR.control_effectiveness_rating AS controlRating",
           "percieved_control_effectiveness" => "RCE.effectiveness AS percieved_control_effective", 
           "current_controls" 	          => "RR.current_controls",
           "likelihood"        	          => "LK.assessment AS likelihood ",
           "likelihood_rating"	          => "LK.color AS likelihood_rating, RR.likelihood_rating AS LKrating",
           "impact_rating"     	          => "IM.color impact_rating, RR.impact_rating AS IMrating",
           "impact"            	          => "IM.assessment AS impact",
           "risk_owner"			          => "D.dirtxt  AS risk_owner"
          );			
		$fieldStr =  ""; 
		$calculatedFields = array("residual_risk_exposure", "inherent_risk_exposure", "action_progres");
		foreach( $fieldsArr as $field => $val)
		{
			if( $field == "action_progress"){
				$this->addtionalHeaders[$field] = "action_progress";
			} else {
				if( array_key_exists($field, $aliasArr)){
					$fieldStr .= $aliasArr[$field].",";
				} else {
					$fieldStr .= "RR.".$field.",";
				} 
			}
		}
		$fieldStr = rtrim($fieldStr, ",");
		return $fieldStr;
	}
	
	function prepareWhere( $valuesArr, $matchArr )
	{
		$whereStr = "";
		foreach( $valuesArr as $field => $val)
		{
			if($field == "insertdate"){
				if( $val['from'] != "" || $val['to'] != "" ){ 
					$whereStr .= " AND RR.insertdate BETWEEN '".$val['from']."' AND '".$val['to']."' ";
				}
			} else if( is_array($val)) {
				if(!in_array("all", $val)){
					$whereStr .= " AND RR.".$field." IN(".implode(",", $val).")";
				}
			} else if( $val != "") {
				if( array_key_exists($field, $matchArr)){
					if( $matchArr[$field] === "any")
					{
						$whereStr .= " AND RR.".$field." LIKE '%".$val."%'";
					} else if( $matchArr[$field] == "all"){
						$whereStr .= " AND RR.".$field." LIKE '".$val."'";						
					} else if( $matchArr[$field] == "excat"){
						$whereStr .= " AND RR.".$field." = '".$val."'";
					}
				} else {
					$whereStr .= " AND RR.".$field." = '".$val."'";	
				}
			} 
		}
		return $whereStr;
	}
	
	function prepareGroupBy( $groupBy )
	{
		$groupStr = "";
		if( $groupBy !== ""){
			$groupStr = "RR.".rtrim($groupBy, "_");
		}
		return $groupStr;	
	}
	
	function prepareSortBy( $sortArr )
	{
		$sortStr = "";
		foreach( $sortArr as $field => $val)
		{
			$sortStr .= " RR.".ltrim($val, "__").",";			
		}		
		$sortStr = rtrim($sortStr, ",");
		return $sortStr;
	}
	
	function prepareJoinTables()
	{
        $innerJoins = "";
	    	$innerJoins .= "LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category \r\n";
			$innerJoins .= "INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness \r\n";
			$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_level RRL ON RRL.id = RR.level \r\n";
			$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FIN ON FIN.id = RR.financial_exposure \r\n";
			$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact \r\n";
			$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_likelihood LK ON LK.id = RR.likelihood \r\n";
			$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_types RRT ON RRT.id = RR.type \r\n";
			$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status \r\n";
			$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id \r\n";
			$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id \r\n";
			$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid \r\n";
        return $innerJoins;
	}
	
	
	
	
	/*---------------------------old way -------------------------*/

         function _generateReport()
         {
                $headers = $this -> _generateHeaders( $_REQUEST['header']);
                $where   = $this -> _generateWhereQuery( $_REQUEST['values']);
                $actionHeaders = $this->_getActionHeaders($_REQUEST['aheader']);
                $join 	 = $this -> joinQuery();
                $group 	 = $this -> _createGroup($_REQUEST['group_by']);
                $sort 	 = $this -> _createSort($_REQUEST['sort']);
                $sql  = "SELECT ".$this->fields." FROM ".$_SESSION['dbref']."_risk_register QR \r\n";
                $sql .= $join;
                $sql .= "WHERE 1 \r\n";
                $sql .= $where;
                $sql .= $group;
                $sql .= $sort;
               // echo $sql;
                $results = $this->get($sql);
                $finalResults = $this->getQueryActions( $results );
                $data = array("headers" =>$headers, "actionheaders" => $actionHeaders, "data" => $finalResults);
                return $data;
            }

            function _generateHeaders( $headers )
            {

                $queryFields = array(
                                      "risk_item" 		  => "QR.id AS ref" ,
                                      "risk_category"     => "QC.name AS risk_category" ,
                                      "risk_description"  => "QR.description" ,
                                      "risk_background"   => "QR.background" ,
                                      "risk_status"  	  => "QS.name AS risk_status",
                                      "financial_exposure"=> "FIN.name AS financial_exposure",
                                      "risk_type"		  => "QRT.name AS risk_type",
                                      "risk_level"		  => "QRL.name AS risk_level",
                                      "risk_owner"        => "D.dirtxt  AS risk_owner",
                                      "kpi_ref"           => "QR.kpi_ref",
                                      "actual_financial_exposure"  => "QR.actual_financial_exposure",
                                      "control_rating"    => "QR.control_effectiveness_rating AS control_rating",
                                      "percieved_control_effective" => "CR.effectiveness AS percieved_control_effective",
                                      "current_controls"  => "QR.current_controls",
                                      "likelihood"        => "LK.assessment AS likelihood",
                                      "impact_rating"     => "QR.impact_rating AS impact_rating",
                                      "impact"            => "IM.assessment AS impact",
                                      "deadline"          => "QR.deadline"
                                    );


                $headersObj  = new Naming();
                $headerNames = $headersObj -> getNamingConversion();
                $headerData  = array();
                $fields 	 = "";
                foreach($headers as $key => $values)
                {
                    if( array_key_exists($key ,$headerNames) )
                    {
                        $fields .=  (isset($queryFields[$key]) ? $queryFields[$key] : " QR.".$key).",";
                        $headerData[$key] = $headerNames[$key];
                    }
                }
                $this->fields = rtrim($fields, ",");
                return $headerData;
            }


            function _generateWhereQuery( $keyValues )
            {
                $matches = $_REQUEST['match'];
                $where = " ";
                $dateWhere = "";
                foreach($keyValues as $key => $value){
                    if($value !==  ""){
                        if($key == "_from_date" || $key == "_to_date")
                            {
                                if($key == "_from_date")
                                {
                                    $from = $value;
                                }
                                if($key == "_to_date")
                                {
                                    $to = $value;
                                }
                                if($from == "")
                                {
                                    $from = date("d/m/Y", strtotime("-365 days"));
                                }
                                if($to == "")
                                {
                                    $to = date("d/m/Y");
                                }
                                $dateRange = "BETWEEN '$from' AND '$to'";
                                $dateWhere = " OR QR.insertdate ".$dateRange."\r\n";

                            } else if($value !== "all"){

                                if(isset($matches[$key]))
                                {
                                    if($matches[$key] == "all")
                                    {
                                        $where .= " AND QR.".$key." LIKE '%".$value."%' \r\n";
                                    } else if($matches[$key] == "any"){
                                        $where .= " AND QR.".$key." LIKE '".$value."' \r\n";
                                    } else if($matches[$key] == "exact"){
                                        $where .= " AND QR.".$key." = '".$value."' \r\n";
                                    }
                                } else {
                                        $where .= " AND QR.".$key." = '".$value."' \r\n";
                                }
                            }
                    }
                }

                return $where." ".$dateWhere;
            }

            function joinQuery()
            {
                $innerJoins = "";
			    	$innerJoins .= "LEFT JOIN  ".$_SESSION['dbref']."_categories QC ON QC.id = QR.category \r\n";
					$innerJoins .= "INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = QR.percieved_control_effectiveness \r\n";
					$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_level QRL ON QRL.id = QR.level \r\n";
					$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FIN ON FIN.id = QR.financial_exposure \r\n";
					$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = QR.impact \r\n";
					$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_likelihood LK ON LK.id = QR.likelihood \r\n";
					$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_types QRT ON QRT.id = QR.type \r\n";
					$innerJoins .= "LEFT JOIN ".$_SESSION['dbref']."_status QS ON QS.id = QR.status \r\n";
					$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= QR.sub_id \r\n";
					$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id \r\n";
					$innerJoins .= "INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid \r\n";
                return $innerJoins;
            }

            function _createGroup( $groupField )
            {
                $group = "";
                if($groupField !== ""){
                    $group = " GROUP BY QR.".rtrim($groupField,"_")."\r\n";
                }
                return $group;
            }

            function _createSort( $sortBy )
            {
                $sort = "";
                if($sortBy == ""){
                    $sort = "";
                } else {
                    $sort = " ORDER BY ";

                    foreach($sortBy as $index => $field)
                    {
                        $sort .= "QR.".ltrim($field, "__").",";
                    }
                    $sort = rtrim($sort,",");
                }
                return $sort;
            }

            function _getActionHeaders( $headers )
            {
                $queryFields = array(
                                    "action_status"	  => "ASS.name AS action_status" ,
                                    "risk_action"     => "A.action AS risk_action",
                            );


                $headersObj  = new Naming();
                $headerNames = $headersObj -> getActionColumns();
                $headerData  = array("ref" => "Action Ref");
                $fields 	 = "A.id AS ref ,";
                foreach($headers as $key => $values)
                {
                    if( array_key_exists($key ,$headerNames) )
                    {
                        $fields .=  (isset($queryFields[$key]) ? $queryFields[$key] : " A.".$key).",";
                        $headerData[$key] = $headerNames[$key];
                    }
                }
                $this->actionFields = rtrim($fields, ",");
                return $headerData;
            }


            function getQueryActions( $queries )
            {
                $queryActions = array();
                foreach($queries as $key => $query)
                {
                    //echo $key;
                    $actions = $this->getActions( $query['ref']);
                    $queryActions[$query['ref']] = $query;
                    //echo "<pre>";
                        //print_r($actions);
                   // echo "</pre>";
                    if(!empty($actions))
                    {
                        $queryActions[$query['ref']]['actions'] = $actions;
                    }
                }
                //print "<pre>";
                    //print_r($queryActions);
                //print "</pre>";
                return $queryActions;
            }

            function getActions( $id )
            {
               // echo "SELECT ".$this->actionFields." FROM ".$_SESSION['dbref']."_actions A WHERE risk_id = $id";
                $results = $this->get("SELECT ".$this->actionFields."
                                       FROM ".$_SESSION['dbref']."_actions A
                                       LEFT JOIN ".$_SESSION['dbref']."_action_status ASS ON A.status = ASS.id
                                       WHERE risk_id = $id");
                return $results;
            }


		function _residualVsInherent()
		{
			$response = $this -> get("SELECT id, inherent_risk_rating, residual_risk FROM ".$_SESSION['dbref']."_risk_register");
			return $response;
		}
		
		function _getRiskType() 
		{
			$response = $this -> get( "SELECT 
										RT.name, 
										SUM( inherent_risk_exposure ) AS sumInherent,
										SUM( residual_risk ) AS sumResidual,
										AVG( inherent_risk_exposure ) AS avgInherent,
										AVG( residual_risk ) AS avgResidual
										FROM ".$_SESSION['dbref']."_risk_register RR 
										INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type 
										WHERE 1 GROUP BY RR.type 									
										" );
			return $response;
		}
	
	function _getRiskCategory() 
	{
	
		$response = $this -> get( "SELECT 
									RC.name , 
									SUM( inherent_risk_exposure ) AS sumInherent ,
									SUM( residual_risk ) AS sumResidual,
									AVG( inherent_risk_exposure ) AS avgInherent,
									AVG( residual_risk )	AS avgResidual
									FROM ".$_SESSION['dbref']."_risk_register RR
									INNER JOIN ".$_SESSION['dbref']."_categories RC ON RR.category = RC.id
									WHERE 1 GROUP By RR.category																													
								" );
		return $response;
	}		
		
	function _getRiskPerson() 
	{
		$response = $this -> get( "SELECT 
									TK.tkname, TK.tksurname , 
									D.dirtxt  AS risk_owner,					
									SUM( inherent_risk_exposure ) AS sumInherent,
									SUM( residual_risk ) AS sumResidual,
									AVG( inherent_risk_exposure ) AS avgInherent,
									AVG( residual_risk ) AS avgResidual	
									FROM ".$_SESSION['dbref']."_risk_register RR
									INNER JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
									INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
									INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid										
									LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON RR.risk_owner = TK.tkid																													
									WHERE 1 GROUP BY RR.risk_owner
								" );
		return $response;
	}				
				
	/** ****************************************************************************** */
    function saveQuickReport( $postArr )
    {
        $updatedata = array("name" => $_POST['report_name'], "description" => $_POST['report_description'], "report" => serialize($postArr), "insertuser" => $_SESSION['tid'] );
        if($this->insert("quick_report", $updatedata))
        {
        	echo "<div class='ui-widget ui-icon-closethick ui-state-ok' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Report successfully saved as quick report</div>";
        } else {
            echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Error saving report, try again</div>";
        }
    }

    function getQuickReports( $start, $limit)
    {
        $results = $this->get("SELECT id, name, description, insertuser FROM ".$_SESSION['dbref']."_quick_report WHERE status & 1 = 1 LIMIT $start, $limit");
        return $results;
    }

    function getQuickData( $id )
    {
        $results = $this->getRow("SELECT report FROM ".$_SESSION['dbref']."_quick_report WHERE id = $id");
        return $results['report'];
    }
    
    function editQuickReport( $postArr, $id )
    {
       $res = $this->update("quick_report", array("report" => serialize($postArr), "name" => $postArr['report_name'], "description" => $postArr['report_description'] ), "id=$id");
       if($res > 0)
       {
        	echo "<div class='ui-widget ui-icon-closethick ui-state-ok' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Report successfully updated</div>";
       } else {
            echo "<div class='ui-widget ui-icon-closethick ui-state-error' style='margin-right:0.3em; margin:5px 0px 10px 0px; padding:0.3em; clear:both;'>Error updating report, try again</div>";
       }       
    } 
    
    function updateQuickReport( $id )
    {
       $res = $this->update("quick_report", array("status" => 2), "id=$id");
       return $res;  	
    }
    
    function getRiskRegister()
    {
 		$nm 			= new Naming();
		$default  		= new Defaults("","");
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");				
		$defaults 		= $default -> getDefaults();   	
		$headers		= $nm -> getHeaderList();
		$colored  		= array(
								"control_rating" 	 => "controlRating",
								"impact_rating" 	 => "IMrating",
								"likelihood_rating"  => "LKrating",
								);		
		$response = $this -> get( "
									SELECT DISTINCT(RR.id) AS risk_item,
									RR.description AS risk_description,
									RR.background AS risk_background,
									RR.impact_rating AS IMrating,
									RR.likelihood_rating AS LKrating, 
									RR.control_effectiveness_rating AS controlRating , 
									RR.current_controls,
									RR.time_scale , 
									RR.inherent_risk_exposure,
									RR.inherent_risk_rating,
									RR.residual_risk,
									RR.residual_risk_exposure,
									RR.kpi_ref,
									RR.actual_financial_exposure,
									FE.name AS financial_exposure,							
									RLevel.name AS risk_level, 							
									RC.name as risk_category, 
									RC.description as cat_descr,
									RCE.effectiveness AS percieved_control_effective,
									RCE.color AS control_rating, 								
									IM.assessment as impact,
									IM.definition as impact_descr ,
									IM.color AS impact_rating,
									RL.assessment as likelihood,
									RL.color AS likelihood_rating ,
									RL.definition like_descr,
									RT.name as type,
									RT.shortcode as type_code, 
									D.dirtxt  AS risk_owner,
									RS.name AS risk_status
									FROM ".$_SESSION['dbref']."_risk_register RR 
									INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
									INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
									INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
									INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
									INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
									LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
									LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RR.level
									LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure								
									LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
									INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
									INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
									WHERE RR.active = 1 ORDER BY RR.id
							");
		$riskArray 	= array();
		$headerArr  = array();
		$registerOrder = array(
								"risk_item" 			=> null,
								"risk_description"		=> null,
							    "risk_background"		=> null,	
							    "risk_type"				=> null,
							    "risk_category"			=> null,
								"impact"				=> null,
							    "impact_rating"			=> null,
							    "likelihood"			=> null,
							    "inherent_risk_exposure"=> null,
								"current_controls"		=> null,
		 						"percieved_control_effective"	=> null,
								"control_rating"		=> null,
							    "residual_risk"			=> null,
								"residual_risk_exposure"=> null,
								"financial_exposure"	=> null,
								"actual_financial_exposure" => null,
								"kpi_ref"				=> null,
								"risk_level"			=> null,
								"risk_owner"			=> null,
								"risk_status" 			=> null,
								"action_progress"		=> null
							);
	foreach( $registerOrder as $key => $val)
	{
		$headerA[$key]	= $headers[$key];
	}								
		foreach ( $response as $index => $risk)
		{
			foreach( $headerA as $field => $val)
			{	
				if( $field == "action_progress"){
						$actionObj 		= new RiskAction($risk['risk_item'], "", "", "", "", "", "", "", "");
						$actionProgress = $actionObj -> getActionStats();
						$riskArray[$risk['risk_item']][$field]  = round($actionProgress['averageProgress'], 2)."%";
						$headerArr[$field]	= $val;
				}  else if( array_key_exists($field, $risk)) {					
					if($field === "residual_risk_exposure" ) {		
						$riskExposureArr = $res -> getRiskResidualRanges( ceil($risk[$field]) );
						$color		= "";
						$magnitude  = "";
						if(isset($riskExposureArr) && !empty($riskExposureArr)){
							$color 		  = $riskExposureArr['color'];
							$magnitude 	  = $riskExposureArr['magnitude'];
						}
					 	$risk[$field] =  "<span style='width:50px; background-color:#".$color."'>&nbsp;&nbsp;&nbsp;".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					}					
					
					if($field === "inherent_risk_exposure" ) {
						$inherentResults  = $ihr -> getRiskInherentRange( round($risk[$field]) );
						$color 			  = "";
						$magnitude 	 	  = "";
						if(isset($inherentResults) && !empty($inherentResults)){
							$color 			= $inherentResults['color']; 
							$magnitude 	 	= $inherentResults['magnitude'];
						}
						$risk[$field] =   "<span style='width:50px; background-color:#".$color."'>&nbsp;&nbsp;&nbsp;".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					}					
					
							
					if( in_array($field, array_keys($colored)) ){
						$riskArray[$risk['risk_item']][$field] 	= "<span style='width:50px; text-align:center; background-color:#".$risk[$field]."'>&nbsp;&nbsp;&nbsp;".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."&nbsp;&nbsp;&nbsp;</span>";
					} else {
						$riskArray[$risk['risk_item']][$field]  = $risk[$field];
					}						
					$headerArr[$field]	= $val;
				}			
			}		
		}
		return array("risk" => $riskArray, "header" => $headerArr);
    }
    
    function getRiskperStatus()
    {
    	$results = $this->get("SELECT COUNT(id) AS total, status 
    						   FROM #_risk_register RR
    						   GROUP BY status
    	  					  ");
    	
    	return $results;
    }
    
    function getActionStatus()
    {
    	$results = $this->get("SELECT COUNT(id) AS total , status
    	                       FROM #_actions AA
    	                       GROUP BY status
    						 ");
    	return $results;
    }
    
}


?>
