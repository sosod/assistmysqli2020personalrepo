<?php
class ActionColumns extends Naming
{
	
	function getNaming( $options = "")
	{
		$results = $this->get("SELECT id, name , ignite_terminology, client_terminology, active
							   FROM #_header_names
							   WHERE type = 'action'
							   $options
							   ORDER BY ordernumber 
							 ");
		return $results;
	}
	
}