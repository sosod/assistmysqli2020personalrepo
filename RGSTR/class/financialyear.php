<?php
/**
	* Finicial Year class
	* @author 	: admire<azinamo@gmail.com>
	* copyright	: 2011 Ignite Assist
	* 
**/
class FinancialYear extends DBConnect
{
	protected $last_day;
	
	protected $start_date;
	
	protected $end_date;
	
	protected $_months = array(
						"01" => "January",
						"02" => "Febrauary",
						"03" => "March",
						"04" => "April",
						"05" => "May",
						"06" => "June",
						"07" => "July",
						"08" => "August",
						"09" => "September",
						"10" => "October",
						"11" => "November",
						"11" => "December",
						);
	
	protected $_errors = array();
	
	function __construct( $last_day, $start_date, $end_date ) {
		$this -> last_day 	= $last_day;//$this -> _formatDate( $last_day ) ;
		$this -> start_date = $start_date;//$this -> _formatDate( $start_date ) ;
		$this -> end_date 	= $end_date; //$this -> _formatDate( $end_date );
		//$conn = new DB_Connect("root" ,"","ignite_i".$_SESSION['cc']);
		parent::__construct();
	}
	
	function _formatDate( $date )
	{
		$dateArray 	  = explode("/",$date);
		$formatedDate = $dateArray[1]."-".$this -> _months[$dateArray[0]]."-".$dateArray[2];	
		return $formatedDate;
	}
	/**
		Validate the date posted format
	**/
	function _validateDateFormat( $postedFormat ){
	
		if( strpos( $postedFormat ,"/" ) > 1 ) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	function _validateYearStartEnd(){
		
		$starttimestamp = strtotime($this -> start_date);
		$endtimestamp 	= strtotime($this -> end_date);
		if ( $endtimestamp > $starttimestamp ) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
	
	function getFinYear(){
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_financial_years WHERE active = 1" );
		return $response;
	}
	
	function getAFinYear( $id ){
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_financial_years WHERE id = $id" );
		return $response;
	}
	
	/**
		Save the financial year
	**/
	function saveFinYear()
	{		
	  if( $this -> _validateYearStartEnd() ) {
		$insertdata = array(
						"start_date" 	=> $this -> start_date,
						"end_date"		=> $this -> end_date,
						"last_day"		=> $this -> last_day,
						"insertuser"	=> $_SESSION['tid']
		);
		$insertid = $this -> insert( "financial_years", $insertdata );
		return $insertid;//$this -> insertedId();
	  } else {
	 	$this -> _errors['yeardiff']['error'] = "Start date of year can't be greater than end of year ";
	  }
	  return $this -> _errors;
	}
	/**
	 Update the financial year
	**/
	function updateFinacialYear( $id )
	{
	  if( $this -> _validateYearStartEnd() ) {
		$updatedata = array(
						"start_date" 	=> $this -> start_date,
						"end_date"		=> $this -> end_date,
						"last_day"		=> $this -> last_day,
						"insertuser"	=> $_SESSION['tid']
		);
        $logsObj = new Logs();
        $data    = $this->getAFinYear( $id );
        $logsObj -> setParameters( $_POST, $data, "financial_years");
		$updateid = $this -> update( "financial_years", $updatedata , "id=$id");
		return $updateid;
	  } else {
	 	$this -> _errors['yeardiff']['error'] = "Start date of year can't be greater than end of year ";
	  }
	  return $this -> _errors;	
	}
	
	/*
	function saveFinYear(){
		if( $this -> validateDateFormat( $this -> end_date) ) {
			if( $this -> validateDateFormat( $this -> start_date) ) {		
				if( $this -> _validateYearStartEnd() ) {
					$startyear = $this -> _formatDate( $this -> start_date );
					$endyear   = $this -> _formatDate( $this -> end_date ); 
					
					$finYear   = $this -> getFinYear() ;								 
						$query = mysql_query( "INSERT 
											INTO ".$_SESSION['dbref']."_financial_years( start_date , end_date , last_day, insertuser )
						 					VALUES( '".$startyear."', '".$endyear."', '".$this -> last_day."', '".$_SESSION['tid']."')" );
						return  mysql_insert_id();
				} else {
					$this -> _errors['yeardiff']['error'] = "Start date of year can't be greater than end of year ";
				}	
			} else {
				$this -> _errors['dateformat']['error'] = "Invalid start date format . please select date from date picker";
			}		
		} else {
			$this -> _errors['dateformat']['error'] = "Invalid end date format . please select date from date picker";
		}
		return $this -> _errors;
	}
	
	*/
/*	function updateFinacialYear( $id ) {
	
	if( $this -> validateDateFormat( $this -> end_date) ) {
			if( $this -> validateDateFormat( $this -> start_date) ) {		
				if( $this -> _validateYearStartEnd( ) ) {
					$startyear = $this -> _formatDate( $this -> start_date );
					$endyear   = $this -> _formatDate( $this -> end_date ); 
						$updateData = array(
										'start_date' => $startyear ,
										'end_date'   => $endyear ,
										'last_day'	 => $this -> last_day,
										'insertuser' => $_SESSION['tid']								
						);
						$response  = $this -> update( "financial_years", $updateData, "id=$id" );
					return $response; 
				} else {
					$this -> _errors['yeardiff']['error'] = "Start date of year can't be greater than end of year ";
				}	
			} else {
				$this -> _errors['dateformat']['error'] = "Invalid start date format . please select date from date picker";
			}		
		} else {
			$this -> _errors['dateformat']['error'] = "Invalid end date format . please select date from date picker";
		}
		return $this -> _errors;
	
	}*/
	
	function deleteFinacialYear( $id ){
        $logsObj = new Logs();
        $data    = $this->getAFinYear( $id );
        $logsObj -> setParameters( array("active" => 2), $data, "financial_years");
		$response = $this -> update( "financial_years" , array( "active" => 0 ), "id=$id");
		return $response;
	}
	/**
		Convert fro user readable format , to a date picker format , for editing 
		@return string;
	**/
	function setToDatePickerFormat( $date )
	{
		$datePieces = explode("-", $date);
		$day 		= $datePieces['0'];
		$_month 		= $datePieces['1'];
		$year 		= $datePieces['2'];
		
		foreach( $this -> _months as $key => $month ) {
			if( $month == $_month ) {
				$mon = $key;
			}
		} 
		return $mon."/".$day."/".$year;
	}
}
?>