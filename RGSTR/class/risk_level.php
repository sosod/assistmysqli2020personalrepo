<?php
$scripts = array( 'risk_level.js','menu.js', 'functions.js' );
$styles = array();
$page_title = "Set Up Risk Level";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="risk_level_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="risk-level-form" name="risk-level-form">
			<table border="1" id="risk_level_table">
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th>Risk Level</th>
			    <th>Description</th>
			    <th></th>
			    <th>Status</th>
			    <th></th>    
			  </tr>
			  <tr>
			    <td>#</td>
			    <td><input type="text" name="risklevel_shortcode" id="risklevel_shortcode" /></td>
			    <td><input type="text" name="risklevel" id="risklevel" /></td>
			    <td><textarea id="risklevel_description" name="risklevel_description"></textarea></td>
			    <td><input type="submit" name="add_risklevel" id="add_risklevel" value="Add" /></td>
			    <td></td>
			    <td></td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "level_logs", true); ?></td>
	</tr>
</table>
</div>
