<?php
$scripts = array( 'risk_status.js','menu.js', "201a.js","jscolor.js", 'functions.js' );
$styles = array();
$page_title = "Set Up Risk Statuses";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
		    <form id="risk-status-form" name="risk-status-form" method="post">
		    <table border="1" id="risk_status_table">
		      <tr>
		        <th>Ref</th>
		        <th>Risk Status</th>
		        <th>Your Terminology</th>
		        <th>Colour</th>
		        <th></th>
		        <th>Status</th>
		        <th></th>
		      </tr>
		      <tr>
		        <td>#</td>
		        <td><input type="text" name="status" id="status" /></td>
		        <td><input type="text" name="client_term" id="client_term" /></td>
		        <td>
		      <input name="risktype_color" id="risktype_color" class="color" value="66ff00"></td>
		        <td><input type="submit" name="add_status" id="add_status" value="Add" /></td>
		        <td></td>
		        <td>
		        </td>
		      </tr>
		    </table>
		    </form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "status_logs", true); ?></td>
	</tr>
</table>
</div>

