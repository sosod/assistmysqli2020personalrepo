<?php
/**
	* Risk types for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class RiskType extends DBConnect
{
	
	protected $shortcode;
	
	protected $type;
	
	protected $description;
	
	protected $name;
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
	
	
	function __construct( $shortcode="", $name="", $description="", $type="")
	{
		$this -> shortcode 	 = trim($shortcode);
		$this -> name 		 = trim($name);
		$this -> description = trim($description);
		$this -> type        = trim($type);		
		parent::__construct();
	}
	
	function saveType()
	{
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"type" 		  => $this -> type ,
						"insertuser"  => $_SESSION['tid'],
						);
		$response = $this -> insert( "types" , $insert_data );
		echo $response;//$this -> insertedId();
	}

    function getRiskTypes()
    {
        $response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE active <> 2" );
        return $response ;
    }

    function getType()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE active <> 2" );
		return $response ;		
	}	
	
	function getARiskType( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE id = '".$id."'" );
		return $response ;		
	}	
	/**
		Get used risk types , so they cannot be deleted
	**/
	function getUsedTypes()
	{	
		$response = $this -> get("SELECT type FROM ".$_SESSION['dbref']."_risk_register GROUP BY type");
		return $response;
	}
	/**
		Fetches all active risk types
	**/
	function getActiveRiskType()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_types WHERE active = 1" );
		return $response ;		
	}		
			
	function updateType( $id )
	{
		$insertdata = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"type" 		  => $this -> type, 
						"insertuser"  => $_SESSION['tid'],						
						);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $logsObj -> setParameters( $_POST, $data, "types");
		$response = $this -> update( "types", $insertdata , "id=$id");
		return $response;
	}
	
	function changeTypeStatus( $id, $status )
	{
		$updatedata = array(
						'active' 	  => $status,
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $_POST['active'] = $status;
        unset($_POST['status']);
        $logsObj -> setParameters( $_POST, $data, "types");
		$response = $this -> update( 'types', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateType( $id )
	{
		$update_data = array(
						'active' 	  => "1",
						"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $_POST['active'] = $status;
        unset($_POST['status']);
        $logsObj -> setParameters( $_POST, $data, "types");
		$response = $this -> update( 'types', $update_data, "id=$id" );
		echo $response;
	}

	
	
	
	
	
	function addItem($shortcode, $name,$description) {
		$this -> shortcode	 = trim($shortcode);
		$this -> name = trim($name);
		$this -> description = trim($description);
		$this->type = "";
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"type" 		  => $this -> type ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "types" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $code, $name, $description )
	{
		$insertdata = array(
						"shortcode" 			 => $code,
						"name" 				 => $name,
						"description" => $description,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
		$data['short_code'] = $data['shortcode'];
		unset($data['shortcode']);
		$var = array(
			'id'=>$id,
			'short_code'=>$code,
			'name'=>$name,
			'description'=>$description,
		);
        $logsObj -> setParameters( $var, $data, "types");
		$response = $this -> update( "types", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getARiskType( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "types");
		$response = $this -> update( 'types', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
	
	
	
	
	
	
	

	function getReportList()
	{
		$response = $this -> get( "SELECT DISTINCT L.id, L.name
								   FROM ".$_SESSION['dbref']."_types L 
								   LEFT JOIN ".$_SESSION['dbref']."_risk_register Q
								   ON Q.type = L.id WHERE L.active & 2 <> 2
								 " );
		return $response;
	}

		
	
}
?>