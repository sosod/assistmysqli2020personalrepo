<?php
class SimpleSearch extends Search
{
	
	function search( $searchtext )
	{
		$nm 			= new RiskColumns();		
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");
		$headers 		= $nm -> getHeaderList();
		$colored  		= array(
								"control_rating" 	 => "controlRating",
								"impact_rating" 	 => "IMrating",
								"likelihood_rating"  => "LKrating",
								);		
		$response = $this->get("SELECT 
						DISTINCT(RR.id) AS risk_item,
						RR.description AS risk_description,
						RR.background AS risk_background,
						RR.impact_rating AS IMrating,
						RR.likelihood_rating AS LKrating, 
						RR.control_effectiveness_rating AS controlRating, 
						RR.current_controls,
						RR.time_scale , 
						RR.inherent_risk_exposure,
						RR.inherent_risk_rating,
						RR.residual_risk,
						RR.residual_risk_exposure,
						RR.kpi_ref,
						RR.actual_financial_exposure,
						FE.name AS financial_exposure,							
						RLEVEL.name AS risk_level, 												
						RC.name as risk_category, 
						RC.description as cat_descr,
						RCE.effectiveness AS percieved_control_effective,
						RCE.color AS control_rating, 								
						IM.assessment as impact,
						IM.definition as impact_descr ,
						IM.color AS impact_rating,
						RLL.assessment as likelihood,
						RLL.color AS likelihood_rating ,
						RLL.definition like_descr,
						RT.name as risk_type,
						RT.shortcode as type_code, 
						D.dirtxt  AS risk_owner,
						RR.sub_id,
						RS.name AS risk_status,
						RS.color AS riskstatuscolor
						FROM ".$_SESSION['dbref']."_risk_register RR 
						INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
						INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
						LEFT JOIN ".$_SESSION['dbref']."_level RLEVEL ON RLEVEL.id = RR.level
						LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure								
						INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
						INNER JOIN ".$_SESSION['dbref']."_likelihood RLL ON RLL.id = RR.likelihood
						INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
						LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status							
						LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
						INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
						INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
						WHERE RR.description LIKE '%".$searchtext."%'
						OR RR.kpi_ref LIKE '%".$searchtext."%'
						OR RR.background LIKE '%".$searchtext."%'
						OR RR.current_controls LIKE '%".$searchtext."%'
						OR RR.control_effectiveness_rating LIKE '%".$searchtext."%'
						OR RR.actual_financial_exposure LIKE '%".$searchtext."%'
						OR RR.impact_rating LIKE '%".$searchtext."%'
						OR RR.likelihood_rating LIKE '%".$searchtext."%'
						OR RR.insertdate LIKE '%".$searchtext."%'	
						OR FE.name LIKE '%".$searchtext."%'	
						OR RLEVEL.name LIKE '%".$searchtext."%'
						OR RC.name LIKE '%".$searchtext."%'
						OR RCE.effectiveness LIKE '%".$searchtext."%'
						OR IM.assessment LIKE '%".$searchtext."%'
						OR RLL.assessment LIKE '%".$searchtext."%'
						OR RT.name LIKE '%".$searchtext."%'
						OR RS.name LIKE '%".$searchtext."%'
						OR D.dirtxt LIKE '%".$searchtext."%'
					   ");
								
		$riskArray = array();       
		$headerArr = array();		  
		foreach( $response as $key => $risk )
		{
			foreach($headers as $field => $value) 
			{
				if( $field == "action_progress"){
					$actionObj 		= new RiskAction($risk['risk_item'], "", "", "", "", "", "", "", "");
					$actionProgress = $actionObj -> getActionStats();
					$riskArray[$risk['risk_item']][$field]  = round($actionProgress['averageProgress'], 2)."%";
					$headerArr[$field] = $value;
				} else if( isset( $risk[$field]) || array_key_exists($field, $risk) ) {
					//calculated
					if($field === "residual_risk_exposure" ) {		
						$riskExposureArr = $res -> getRiskResidualRanges( ceil($risk[$field]) );
						$color		= "";
						$magnitude  = "";
						if(isset($riskExposureArr) && !empty($riskExposureArr)){
							$color 		  = $riskExposureArr['color'];
							$magnitude 	  = $riskExposureArr['magnitude'];
						}
					 	$risk[$field] =  "<span style='width:50px; background-color:#".$color."'>&nbsp;&nbsp;&nbsp;".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					}
					//calculted
					if($field === "inherent_risk_exposure" ) {
						$inherentResults  = $ihr -> getRiskInherentRange( round($risk[$field]) );
						$color 			  = "";
						$magnitude 	 	  = "";
						if(isset($inherentResults) && !empty($inherentResults)){
							$color 			= $inherentResults['color']; 
							$magnitude 	 	= $inherentResults['magnitude'];
						}
						$risk[$field] =   "<span style='width:50px; background-color:#".$color."'>&nbsp;&nbsp;&nbsp;".$magnitude."&nbsp;&nbsp;&nbsp;</span>";
					}	
							
					if( in_array($field, array_keys($colored)) ){
						$riskArray[$risk['risk_item']][$field] 	= "<span style='width:50px; text-align:center; background-color:#".$risk[$field]."'>&nbsp;&nbsp;&nbsp;".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."&nbsp;&nbsp;&nbsp;</span>";
					} else {
						$riskArray[$risk['risk_item']][$field]  = $risk[$field];
					}	
					$headerArr[$field] = $value;
				}  
			}
		}
		$data 	   = array( 
						"x"			 => count($headers),
						"total" 	 => count($riskArray),				 
						"riskData"   => $riskArray,
						"headers"	 => $headers 
					  );
		return $data;
	}
	
	
}