<?php
class Profile extends DBConnect
{
	protected $id;
	protected $notification;
	protected $risk_id;
	protected $action_id;
	protected $recieve_email;
	protected $recieve_when;
	protected $recieve_what;
	protected $user_id;
	protected $recieve_day;
	protected $inserdate;
	
	function __construct($notification ,$risk_id, $action_id, $recieve_email, $recieve_when, $recieve_what, $user_id, $recieve_day) 
	{
		$this -> notification = $notification;
		$this -> risk_id	  = $risk_id;		
		$this -> acition_id   = $action_id;
		$this -> recieve_mail = $recieve_mail;		
		$this -> recieve_when = $recieve_when;		
		$this -> recieve_what = $recieve_what;		
		$this -> user_id	  = $user_id;		
		$this -> recieve_day  = $recieve_day;
	}
		
	function getProfile()
	{
		$results = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_user_notifications WHERE active = 1" );
		return $results;
	}	
	
	function getFullProfile()
	{
		$results = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_user_notifications RUN 
								  INNER JOIN  ".$_SESSION['dbref']."_risk_register RRR ON RRR.id = RUN.risk_id
								  INNER JOIN   ".$_SESSION['dbref']."_actions RA ON RA.risk_id = RUN.risk_id							  
								  WHERE RUN.active = 1" );
		return $results;
	}	
		
	function saveProfile()
	{
		$insertdata = array(
			'notification' 		  => $this -> notification,
			'risk_id'			  => $this -> risk_id,		
			'acition_id'		  => $this -> acition_id,
			'recieve_mail'		  => $this -> recieve_mail,		
			'recieve_when' 		  => $this -> recieve_when,		
			'recieve_what' 		  => $this -> recieve_what,		
			'user_id'			  => $this -> user_id,		
			'recieve_day'  		  => $this -> recieve_day,
			"insertuser"    	  => $_SESSION['tid'],			
		);
		$res = $this -> insert( 'user_notifications', $insertdata );
		return $res;//$this -> insertedId();
	}
	
	function updateProfile( $id )
	{
		$updatedata = array(
			'notification' 		  => $this -> notification,
			'risk_id'			  => $this -> risk_id,		
			'acition_id'		  => $this -> acition_id,
			'recieve_mail'		  => $this -> recieve_mail,		
			'recieve_when' 		  => $this -> recieve_when,		
			'recieve_what' 		  => $this -> recieve_what,		
			'user_id'			  => $this -> user_id,		
			'recieve_day'  		  => $this -> recieve_day,
			"insertuser"    	  => $_SESSION['tid'],			
		);
		$response  = $this -> update( 'user_notifications', $updatedata, "id=$id" );
		return $response;
	}

	function deleteProfile( $id ) 
	{
		$delatedata = array(
						 "active" 		=> 0,
						 "insertuser"   => $_SESSION['tid'], 
						 );
		$response  = $this -> update( 'user_notifications', $delatedata, "id=$id" );
		return $response;
	}
	
}
?>