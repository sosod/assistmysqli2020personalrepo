<?php
/**
	* Naming conversion for the module 
	* @author 	: admire<azinamo@gmail.com>
	* copyright	: 2011 Ignite Assist , risk module

**/
class Naming extends DBConnect
{
	/**
		@varchar 
	**/
	protected $item;
	/**
		@varchar 
	**/
	protected $type;
	/**
		@varchar 
	**/
	protected $category;
	/**
		@varchar 
	**/	
	protected $description;
	/**
		@varchar 
	**/	
	protected $background;
	/**
		@varchar 
	**/	
	protected $impact;
	/**
		@varchar 
	**/	
	protected $impact_rating;
	/**
		@varchar 
	**/	
	protected $likelihood;
	/**
		@varchar 
	**/	
	protected $likelihood_rating;
	/**
		@varchar 
	**/	
	protected $inherent_risk_exposure;
	/**
		@varchar 
	**/	
	protected $inherent_risk_rating;
	/**
		@varchar 
	**/	
	protected $current_controls;
	/**
		@varchar 
	**/	
	protected $percieved_control_effectiveness;
	/**
		@varchar 
	**/	
	protected $control_effectiveness_rating;
	/**
		@varchar 
	**/	
	protected $residual_risk;
	/**
		@varchar 
	**/	
	protected $residual_risk_exposure;
	/**
		@varchar 
	**/	
	protected $risk_owner;
	/**
		@varchar 
	**/	
	protected $actions_to_improve;
	/**
		@varchar 
	**/	
	protected $action_owner;
	/**
		@varchar 
	**/	
	protected $time_scale;
	/**
		@text 
	**/	
	protected $status;
	/**
		@text 
	**/	
	protected $udf;
	/**
		@varchar 
	**/	
	protected $company_id;
	/**
		@varchar 
	**/	
	
	protected $headers;

    protected $option_sql;

	function __construct($section = "")
	{
        if($section == "new")
        {
            $this -> option_sql = " AND active & 8 = 8 ";
        } else if($section == "manage" || $section=="admin") {
            $this -> option_sql = " AND active & 4 = 4 ";
        }
        parent::__construct();
	}
	

	
	function getHeaderByType( $type )
	{
		$typeq = "";
		if($type == ""){
			$typeq = " <> 'risk'";
		} else {
			$typeq = " = 'risk'";
		}
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type $typeq " );
		$this->headers = $response;
		return $response;
	}
	
	function getHeaderList( $options = "" )
	{
		$results = $this->getNaming( $options );
		$headRes = array();
		foreach($results as $key => $headerArray){
		  $headRes[trim($headerArray['name'])] = ($headerArray['client_terminology'] == "" ?  $headerArray['ignite_terminology'] : $headerArray['client_terminology'] );
		}
		return  $headRes ;
	}
	
	function saveNaming( $field, $value ,$company_id = NULL )
	{
		$insertdata = array(
						$field 		 => $value,
						'insertuser' => $_SESSION['tid'],
		);
		$response = $this -> update( 'naming_conversion', $insertdata ,"id=1");	

		echo $response; 
	}
	
	function updateHeaderNames( $field, $value  )
	{
		$insertdata = array(
						 "client_terminology"	 => $value,
						 'insertuser' 			 => $_SESSION['tid'],
		);
        $logsObj     = new Logs();
        $data        = $this->getAHeader( $field );
        $_POST['client_terminology'] = $value;
        $_POST['id'] = $data['id'];
        unset($_POST['value']);
        $logsObj -> setParameters( $_POST, $data, "header_names");
		$response = $this -> update( 'header_names', $insertdata ,"name='".$field."'");	

		return $response; 
	}
	
	function getNaming()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_header_names " );
		$this->headers = $response;
		return $response;
	}
	
	function getHeaderNames()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type = 'risk' " );
		$this->headers = $response;
		return $response;
	}

	function getAllHeaderNames()
	{
		//$db = new ASSIST_DB();
		//$sql = "SELECT * FROM ".$db->getDBRef()."_header_names WHERE type <> '' ORDER BY type DESC, name ASC";
		//$response = $db->mysql_fetch_all($sql);
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type <> '' ORDER BY type DESC, name ASC", false );
		$this->headers = $response;
		return $response;
	}
	
	function getNamingConversion()
	{
		$response = $this -> getNameColumn();
		$headRes = array();
		foreach($response as $key => $headerArray){
		  $headRes[$headerArray['name']] = ($headerArray['client_terminology'] == "" ?  $headerArray['ignite_terminology'] : $headerArray['client_terminology'] );
		}
		return  $headRes ;
	}
	
	function nameLabel( $fieldId, $defaultName)
	{
		$namingConversion =  $this-> getNamingConversion();
		if( isset($namingConversion[$fieldId]) && !(empty($namingConversion[$fieldId])) ) {
			return $namingConversion[$fieldId];
		} else {
			return $defaultName;
		}
	}
	
	function rowLabel()
	{
		$namingConversion =  $this-> getNameColumn();
		$keyLabel		  = array(); 
		foreach( $namingConversion  as $key => $valArr){
			$keyLabel[$valArr['name']] = ($valArr['client_terminology'] == "" ? $valArr['ignite_terminology'] : $valArr['client_terminology'] ); 
		}
		return $keyLabel;
	}
	
	function getNameColumn()
	{
		$response = $this -> get("SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type = 'risk' ORDER BY ordernumber");
		return $response;
	}
	function getActionColumns()
    {
		$response = $this -> get("SELECT * FROM ".$_SESSION['dbref']."_header_names WHERE type <> 'risk' ");
        $results = array();
        foreach($response as $index => $values)
        {
            $results[$values['name']] = ($values['client_terminology'] == "" ? $values['ignite_terminology'] : $values['client_terminology'] );
        }
		return $results;
    }

	function setHeader( $key )
	{
		foreach($this->headers as $index => $value){
			if( trim($key) === trim($value['name']) ) {
				return ($value['client_terminology'] != "" ? $value['client_terminology'] : $value['ignite_terminology']);
			} else if(strtolower($key) == "active") {
                return  "Status ";
            } else {
				return ucwords( str_replace("_", " ", $key));
			}
		}
	}	
	function getAHeader( $id )
	{
		$response = $this -> getRow("SELECT id, name AS fieldname, client_terminology FROM ".$_SESSION['dbref']."_header_names WHERE name = '".$id."'");

		return $response;
	}

	function getAName( $id )
	{
		$row = $this -> getRow("SELECT ignite_terminology, client_terminology FROM ".$_SESSION['dbref']."_header_names WHERE name = '".$id."'");
		
		return (strlen($row['client_terminology'])>0 ? $row['client_terminology'] : $row['ignite_terminology']);
	}


	//required for centralised report function
	function getReportingNaming($type="") {
		if($type == "action") {
			$type = "type <> 'risk'";
		} elseif(strlen($type)>0) {
			$type = "type = '".$type."'";
		} else {
			$type = "1";
		}
		$sql = "SELECT name AS id, if(client_terminology<>'',client_terminology,ignite_terminology) as name FROM ".$_SESSION['dbref']."_header_names WHERE $type ORDER BY ordernumber";

		$response = $this -> get($sql);

		$headRes = array();
		foreach($response as $key => $headerArray){
		  $headRes[$headerArray['id']] = $headerArray['name'];
		}
		return  $headRes ;
	}	

}
?>