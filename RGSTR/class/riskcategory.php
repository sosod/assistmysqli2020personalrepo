<?php
/**
	* Risk category
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/	
class RiskCategory extends DBConnect
{
	
	protected $category;
	
	protected $shortcode;
	
	protected $description;
	
	protected $type;
	
	const ACTIVE = 1;
	const INACTIVE = 0;
	const DELETED = 2;
	
	function __construct( $category="", $shortcode="", $description="", $type="")
	{
		$this -> category 	 = trim($category);
		$this -> shortcode 	 = trim($shortcode);
		$this -> description = trim($description);
		$this -> type 		 = trim($type);		
		parent::__construct();
	}

	function getCategory()
	{
		$response = $this -> get( "SELECT RC.id,RC.shortcode, RC.active,  RC.name, RC.description as descr, RT.name as risktype, RT.id as type_id
								   FROM ".$_SESSION['dbref']."_categories RC 
								   INNER JOIN ".$_SESSION['dbref']."_types RT 
								   ON RT.id = RC.type_id WHERE RC.active <> 2
								 " );
		return $response;
	}
	
	function getTypeCategory( $id, $cateid="")
	{
		$response = $this -> get( "SELECT RC.id,RC.shortcode, RC.active,  RC.name, RC.description as descr, RT.name as risktype
								   FROM ".$_SESSION['dbref']."_categories RC 
								   INNER JOIN ".$_SESSION['dbref']."_types RT 
								   ON RT.id = RC.type_id WHERE (RC.type_id = '".$id."' ".($cateid!="" ? " OR RC.id = '".$cateid."'" : "").") AND RC.active <> 2
								 " );
		return $response;
	}	

    public function getActiveTypeCategory($id)
    {
        $response = $this -> get( "SELECT RC.id,RC.shortcode, RC.active,  RC.name, RC.description as descr, RT.name as risktype
								   FROM ".$_SESSION['dbref']."_categories RC
								   INNER JOIN ".$_SESSION['dbref']."_types RT
								   ON RT.id = RC.type_id WHERE RC.type_id = '".$id."'
								   AND (RC.active <> 2 AND RC.active = 1)
								 " );
        return $response;
    }

	function getACategory( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_categories RC WHERE RC.id = $id " );
		return $response;
	}

	function saveCategory()
	{
		$insert_data = array( 
					"name" 		  => $this -> category,
					"shortcode"   => $this -> shortcode,
					"description" => $this -> description,
					"type_id" 	  => $this -> type ,
					"insertuser"  => $_SESSION['tid'],									
					);
		$response = $this -> insert( "categories" , $insert_data );
		echo $response;//$this -> insertedId() ;
	}
	
	function updateCategory( $id )
	{
		$update_data = array( 
					"name" 		  =>  $this -> category ,
					"shortcode"   =>  $this -> shortcode ,
					"description" =>  $this -> description,
					"type_id" 	  =>  $this -> type,
					"insertuser"  => $_SESSION['tid'],										
					);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $logsObj -> setParameters( $_POST, $data, "categories");
		$response = $this -> update( "categories" , $update_data, "id=$id"  );
		return $response;		
	}
	
	
	function deactivateCategory( $id, $status )
	{
		$updatedata = array(
					'active' 	  => $status,
					"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $_POST['active'] = $status;
        unset($_POST['status']);
        $logsObj -> setParameters( $_POST, $data, "categories");
		$response = $this -> update( 'categories', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateCategory( $id )
	{
		$update_data = array(
					'active' 	  => "1",
					"insertuser"  => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $_POST['active'] = 1;
        unset($_POST['status']);
        $logsObj -> setParameters( $_POST, $data, "categories");
		$response = $this -> update( 'categories', $update_data, "id=$id" );
		echo $response;
	}	

	
	
	
	
	
	
	
	function addItem($shortcode, $name,$description,$type_id) {
		$this -> shortcode	 = trim($shortcode);
		$this -> name = trim($name);
		$this -> description = trim($description);
		$this -> type = trim($type_id);
		
		$insert_data = array( 
						"shortcode"   => $this -> shortcode,
						"name"		  => $this -> name,
						"description" => $this -> description,
						"type_id" => $this -> type,
						"active" 		  => RiskCategory::ACTIVE ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "categories" , $insert_data );
		//return $this->insertId();		
		return $response;
	}
	
	
	function editItem( $id, $code, $name, $description, $type )
	{
		$insertdata = array(
						"shortcode" 			 => $code,
						"name" 				 => $name,
						"description" => $description,
						"type_id" => $type,
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
		$nt = new Naming();
		$risk_type_name = str_replace(" ","_",strtolower($nt->getAName("risk_type")));

		$rt = new RiskType();
        $data2    = $this->getACategory( $id );
		$dtype = $rt->getARiskType($data2['type_id']);
		$dtype = $dtype['name'];
		$data = array();
		$data['short_code'] = $data2['shortcode'];
		$data['name'] = $data2['name'];
		$data['description'] = $data2['description'];
		$data[$risk_type_name] = $dtype;
		//unset($data['shortcode']);
		//$data['description'] = $data['descr'];
		//$data[$risk_type_name] = $data['risktype'];
		//unset($data['risktype']);
		//unset($data['descr']);
		$rtype = $rt->getARiskType($type);
		$rtype = $rtype['name'];
		$var = array(
			'id'=>$id,
			'short_code'=>$code,
			'name'=>$name,
			'description'=>$description,
			$risk_type_name=>$rtype,
		);
        $logsObj -> setParameters( $var, $data, "categories");
		$response = $this -> update( "categories", $insertdata , "id=$id");
		return $response;
	}
	
	
	function setStatus($id,$status) {
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getACategory( $id );
        $var['active'] = $status;
		$var['id'] = $id;
        //unset($_POST['status']);
        $logsObj -> setParameters( $var, $data, "categories");
		$response = $this -> update( 'categories', $updatedata, "id=$id" );
		//$response = 1;
		return $response;
	}
	
	
	
		
	
	
	
	
		
	//REPORTING
	function getReportList()
	{
		$response = $this -> get( "SELECT RC.id, RC.name
								   FROM ".$_SESSION['dbref']."_categories RC 
								   LEFT JOIN ".$_SESSION['dbref']."_risk_register Q
								   ON Q.category = RC.id WHERE RC.active & 2 <> 2
								 " );
		return $response;
	}	
	
}
?>