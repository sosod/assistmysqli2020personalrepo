<?php
class ExcellDisplay implements Display
{
	
	function displayResults($data, $optional)
	{
		$csvStr = "";
		$csvStr  = $optional['company']."\r\n";
		$csvStr .= $optional['title']."\r\n";
			$csvStr .= "\r\n";
			foreach( $optional['headers'] as $field => $value){
					$csvStr .= $value;
			}
			foreach( $data as $index => $valArr){
				$csvStr .= "\r\n";
				foreach( $valArr as $key => $value)
				{
					$csvStr .= htmlentities($value).",";
				}
			}
		$csvStr .= "";
	    $filename =  "report_".date("YmdHis").".csv";
	    $fp       =  fopen($filename, "w+");
	    fwrite($fp, $csvStr);
	    fclose($fp);
	    header('Content-Type: application/csv'); 
	    header("Content-length: " . filesize($filename)); 
	    header('Content-Disposition: attachment; filename="' . $filename . '"'); 
	    readfile( $filename );
	    unlink($filename);
	    exit();			
	}
		
}
