$(function(){
	
	
	if( $("#inherentrisk_id").val() == undefined ){
		InherentRisk.get();
	}
	
	$("#add_inherent_risk").click( function() {
		InherentRisk.save();
		return false;
	});	
	
	$("#edit_inherentrisk").click(function(){
		InherentRisk.edit();
		return false;
	});
	
	$("#change_inherentrisk").click(function() {
		InherentRisk.change();
		return false;
	});
	
	$("#cancel_inherentrisk").click(function(){
		history.back();
		return false;								   
	});
});

var InherentRisk		= {
	
		get			: function()
		{
				$.get( "controller.php?action=getInherentRisk", function( data ) {
					$.each( data, function( index, inherentRisk){
						InherentRisk.display( inherentRisk )
					});
				}, "json");
		} , 
		
		save		: function()
		{
			var message 	= $("#inherent_risk_message")						
			var rating_from = $("#inherent_rating_from").val();
			var rating_to 	= $("#inherent_rating_to").val();
			var magnitude	= $("#inherent_risk_magnitude").val();
			var response	= $("#inherent_response").val();
			var color 		= $("#inherent_color").val();
			
			if ( rating_from == "") {
				jsDisplayResult("error", "error", "Please enter the rating for this inherent risk . . .");
				return false;
			} else if( rating_to == "" ) {
				jsDisplayResult("error", "error", "Please enter the rating to for this inherent risk . . .");
				return false;
			} else if ( magnitude == "" ) {
				jsDisplayResult("error", "error", "Please enter the asssessment for this inherent risk . . .");
				return false;
			} else if( response == "" ) {
				jsDisplayResult("error", "error", "Please enter the definition for this inherent . . .");
				return false;			
			} else {
				jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=newInherentRisk",
					   {
						   from		: rating_from,
						   to		: rating_to,
						   magnitude: magnitude,
						   response : response,
						   clr		: color
					}
					   , function( retData  ) {
						   if( retData > 0 ) {
							   $("#inherent_rating_from").val("")
							   $("#inherent_rating_to").val("")
							   $("#inherent_risk_magnitude").val("")
							   $("#inherent_response").val("")
							   jsDisplayResult("ok", "ok", "Inherent risk exposure saved successfully . . . ");
							   var data = { id : retData,  status : 1 , from : rating_from , to : rating_to ,magnitude : magnitude, response : response, color : color }; 
							   InherentRisk.display( data )
						   } else {
							   jsDisplayResult("error", "error", "There was an error saving the inherent risk exposure");  
							}
				},"json")
			}
			return false;	
		} ,
		
		edit		: function()
		{
			var message 	= $("#inherentrisk_message")						
			var rating_from = $("#inherent_rating_from").val();
			var rating_to 	= $("#inherent_rating_to").val();
			var magnitude	= $("#inherent_risk_magnitude").val();
			var response	= $("#inherent_response").val();
			var color 		= $("#inherent_color").val();
			if ( rating_from == "") {
				jsDisplayResult("error", "error", "Please enter the rating for this inherent risk . . .");
				return false;
			} else if( rating_to == "" ) {
				jsDisplayResult("error", "error", "Please enter the rating to for this inherent risk . . .");
				return false;
			} else if ( magnitude == "" ) {
				jsDisplayResult("error", "error", "Please enter the asssessment for this inherent risk . . .");
				return false;
			} else if( response == "" ) {
				jsDisplayResult("error", "error", "Please enter the definition for this inherent . . .");
				return false;			
			} else {
				jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=updateInherentRisk",
					   {
						   id 		: $("#inherentrisk_id").val(), 
						   from		: rating_from,
						   to		: rating_to,
						   magnitude: magnitude,
						   response : response,
						   clr		: color
					}
					   , function( retData  ) {
							if( retData == 1 ) {
								jsDisplayResult("ok", "ok", "Inherent risk successfully updated . . .");					
							} else if( retData == 0 ){
								jsDisplayResult("info", "info", "Inherent risk not changed . . .");
							} else {
								jsDisplayResult("error", "error", "Error updating the inherent risk . . .");
							}
				},"json")
			}
			return false;
		} , 
		
		change		: function()
		{
			
			var message 	= $("#inherentrisk_message")												 
			$.post( "controller.php?action=changeInherent", 
				   {
					   id	  : $("#inherentrisk_id").val(),
					   status : $("#inherent_status :selected").val()
					}, function( deactData ) {
				if( deactData == 1){
					jsDisplayResult("ok", "ok","Inherent risk has successfully been updated . . . ");
				} else if( deactData == 0){
					jsDisplayResult("info", "info", "No change was made to the inherent risk status . . .");
				} else {
					jsDisplayResult("error", "error", "Error saving , please try again . . .");
				}										 
			})									
			return false;				
			
		} , 		
		
		display		: function( val )
		{
			$("#inherent_risk_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.magnitude}))
			  .append($("<td />",{html:val.response}))
			  .append($("<td />")
				 .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))	  
			  )
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"inherentactedit_"+val.id, name:"inherentactedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"inherentactdel_"+val.id, name:"inherentactdel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"inherentactact_"+val.id, name:"inherentactact_"+val.id}))	
				///.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ),value:"Inactivate", id:"inherentactdeact_"+val.id, name:"inherentactdeact_"+val.id}))							
			   )	
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		
				)
			)
			
			$("#change_"+val.id).live("click", function(){
				document.location.href = "change_inherent_status.php?id="+val.id+"&";
				return false;										
			});
			
			$("#inherentactedit_"+val.id).live( "click", function() {
				document.location.href = "edit_inhrentrisk.php?id="+val.id+"&";
				return false;										  
			});
			
			$("#inherentactdel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this inherent risk ")) {	
				jsDisplayResult("info", "info", "Deleting. . . <img src='../images/loaderA32.gif' >");					
				$.post( "controller.php?action=changeInherent", 
					   {
						   id		: val.id,
						   status	: 2
						}, function( delData ) {
					if( delData == 1){
						jsDisplayResult("ok", "ok", "Inherent risk deleted. . . ");
						$("#tr_"+val.id).fadeOut();
					} else if( delData == 0){
						jsDisplayResult("info", "info", "No change was made to the inherent risk. . . ");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again. . . ");
					}										 
				});
				}
				return false;										  
			});
			
			$("#inherentactact_"+val.id).live( "click", function() {
				jsDisplayResult("info", "info", "Activating. . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=ihractactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						jsDisplayResult("error", "error", "Inherent risk activated. . . <img src='../images/loaderA32.gif' >");
					} else if( actData == 0){
						jsDisplayResult("info", "info", "No change was made to the inherent risk . . .");
					} else {
						jsDisplayResult("info", "info", "Error saving , please try again . . .");
					}										 
				})															 
				return false;										  
			});

		}
	
	
}
