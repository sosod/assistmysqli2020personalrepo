// JavaScript Document
$(function(){
	
	/**
	Get all the likelihoods
	**/
	$("#likelihood_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getLikelihood", function( data ) {
		$("#likelihood_message").html("")
		var message = $("#likelihood_message")
		$.each( data, function( index, val) {
			$("#likelihood_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.assessment}))
			  .append($("<td />",{html:val.definition}))
			  .append($("<td />",{html:val.probability}))			  
			  .append($("<td />")
				 .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"})) 
			  )
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"lkactedit_"+val.id, name:"lkactedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"lkactdel_"+val.id, name:"lkactdel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"lkactact_"+val.id, name:"lkactact_"+val.id}))	
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ),value:"Inactivate", id:"lkactdeact_"+val.id, name:"lkactdeact_"+val.id}))							
			   )		
			  .append($("<td />")
				.append($("<input />",{type:"submit", id:"change_"+val.id, name:"change_"+val.id, value:"Change Status"}))		
			  )
			)
			
			$("#change_"+val.id).live("click",function(){
				document.location.href = "change_like_status.php?id="+val.id+"&"
				return false;								   
			});
			
			$("#lkactedit_"+val.id).live( "click", function() {
				document.location.href = "edit_likelihood.php?id="+val.id+"&";
				return false;										  
			});
			$("#lkactdel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this likelihood ")) {	
					jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' >");
					$.post( "controller.php?action=changeLikestat", 
						   {id		: val.id,
						   	status  : 2
						   }, function( delData ) {
						if( delData == 1){
							jsDisplayResult("ok", "ok", "Likelihood deleted . . . ");
							$("#tr_"+val.id).fadeOut();
						} else if( delData == 0){
							jsDisplayResult("info", "info", "No change was made to the likelihood . . . ");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again . . . ");
						}										 
					});
				}
				return false;										  
			});
			$("#lkactact_"+val.id).live( "click", function() {
				jsDisplayResult("info", "info", "Actvating  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=likeactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						jsDisplayResult("ok", "ok", "Likelihood deactivated  . . . ");
					} else if( actData == 0){
						jsDisplayResult("info", "info", "No change was made to the likelihood . . . ");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again. . . ");
					}					 
				})															 
				return false;										  
			});

		});						   
	} ,"json");
	
	/**
		Adding a new impact
	**/
	$("#add_likelihood").click( function() {
		var message 	= $("#likelihood_message")						
		var rating_from = $("#lkrating_from").val();
		var rating_to 	= $("#lkrating_to").val();
		var assessment	= $("#lkassessment").val();
		var definition  = $("#lkdefinition").val();
		var probability = $("#lkprobability").val();		
		var color 		= $("#llkcolor").val();
	
		
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this likelihood . . . ");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this likelihood . . . ");
			return false;
		} else if ( assessment == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this likelihood . . . ");
			return false;
		} else if( definition == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this likelihood . . . ");			
			return false;			
		} else {
			jsDisplayResult("info", "info", "Please enter the definition for this likelihood . . . ");
			$.post( "controller.php?action=newLikelihood",
				   {
					   from		: rating_from,
					   to		: rating_to,
					   assmnt	: assessment,
					   dfn		: definition,
					   prob		: probability,
					   clr		: color
					}
				   , function( retData ) {
				if( retData > 0 ) {
					$("#lkrating_from").val("");
					$("#lkrating_to").val("")
					$("#lkassessment").val("")
					$("#lkdefinition").val("")
					$("#lkprobability").val("")
					jsDisplayResult("ok", "ok", "Likelihood saved successfully. . . ");					
					$("#likelihood_table")
					.append($("<tr />",{id:"tr_"+retData})
					  .append($("<td />",{html:retData}))
					  .append($("<td />",{html:rating_from+" to "+rating_to}))
					  .append($("<td />",{html:assessment}))
					  .append($("<td />",{html:definition}))
					  .append($("<td />",{html:probability}))				  
					  .append($("<td />")
					    .append($("<span />",{html:color}).css({"background-color":"#"+color, "padding":"5px"}))	  
					  )
					  .append($("<td />")
						.append($("<input />",{type:"submit", value:"Edit", id:"lkactedit_"+retData, name:"lkactedit_"+retData}))	
						.append($("<input />",{type:"submit", value:"Del", id:"lkactdel_"+retData, name:"lkactdel_"+retData}))					
					   )
					  .append($("<td />",{html:"<b>Active</b>"})
						//.append($("<input />",{type:"submit", value:"Activate", id:"lkactact", name:"lkactact"}))	
						//.append($("<input />",{type:"submit",value:"Inactivate", id:"lkactdeact", name:"lkactdeact"}))							
					   )
					  .append($("<td />")
						.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))
						)
					)
					$("#change_"+retData).live("click",function(){
						document.location.href = "change_like_status.php?id="+retData+"&"
						return false;								   
					});
					
					$("#lkactedit_"+retData).live("click",function(){
						document.location.href = "edit_likelihood.php?id="+retData+"&";	
						return false;								   
					});					
	
					$("#lkactdel_"+retData).live("click",function(){
						if( confirm(" Are you sure you want to delete this likelihood ")) {	
							jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' >");
							$.post( "controller.php?action=changeLikestat", 
								   {id		: retData,
								   	status  : 2
								   }, function( delData ) {
								if( delData == 1){
									jsDisplayResult("ok", "ok", "Likelihood deleted . . . ");
									$("#tr_"+retData).fadeOut();
								} else if( delData == 0){
									jsDisplayResult("info", "info", "No change was made to the likelihood . . . ");
								} else {
									jsDisplayResult("error", "error", "Error saving , please try again . . . ");
								}										 
							});
						}
						return false;								   
					});							
					
				} else {
					jsDisplayResult("error", "error", "Error saving likelihood. . . ");
				}
			},"json")	
		}
		return false;							 
	});		
	
	
	$("#edit_likelihood").click(function(){  
		
		var message 	= $("#likelihood_message")						
		var rating_from = $("#lkrating_from").val();
		var rating_to 	= $("#lkrating_to").val();
		var assessment	= $("#lkassessment").val();
		var definition  = $("#lkdefinition").val();
		var probability = $("#lkprobability").val();		
		var color 		= $("#llkcolor").val();
	
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this likelihood . . . ");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this likelihood . . . ");
			return false;
		} else if ( assessment == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this likelihood . . . ");
			return false;
		} else if( definition == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this likelihood . . . ");			
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateLikelihood",
				   {
					   id		: $("#likelihood_id").val(),
					   from		: rating_from,
					   to		: rating_to,
					   assmnt	: assessment,
					   dfn		: definition,
					   prob		: probability,
					   clr		: color
					}
				   , function( retData ) {
						 if( retData == 1 ) {
							jsDisplayResult("ok", "ok", "Likelihood successfully updated . . . "); 
						} else if( retData == 0 ){
							jsDisplayResult("info", "info", "Likelihood not changed . . . "); 
						} else {
							jsDisplayResult("error", "error", "Error updating the likelihood . . . ");
						}
			},"json")	
		}
		return false;								  
	});
	
	$("#change_likelihood").click(function(){
		jsDisplayResult("error", "error", "Updating . . . <img src='../images/loaderA32.gif' >");											  
		$.post( "controller.php?action=changeLikestat", 
			   { id		: $("#likelihood_id").val(),
			   	 status : $("#like_status :selected").val()
			   }, function( retData ) {
			if( retData  == 1){
				jsDisplayResult("ok", "ok", "Likelihood status changed . . .");
			} else if( retData  == 0){
				jsDisplayResult("info", "info", "No change was made to the likelihood status . . .");
			} else {
				jsDisplayResult("error", "error", "Error updating status  , please try again . . .");				
			}										 
		})									
		return false;										  								   
	});	
		
		
	$("#cancel_edit_likelihood").click(function(){
		history.back();
		return false;								   
	});	
	
})
