// JavaScript Document
$(function(){
	$("#edit_impact").click(function(){
		var message 	= $("#impact_message")						
		var rating_from = $("#imprating_from").val();
		var rating_to 	= $("#imprating_to").val();
		var assessment	= $("#impact_assessment").val();
		var definition  = $("#impact_definition").val();
		var color 		= $("#impact_color").val();
	
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this impact . . .");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this impact . . .");
			return false;
		} else if ( assessment == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this impact . . .");
			return false;
		} else if( definition == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this impact . . .");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Update . . . <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=updateImpact", 
				   {
					  id		: $("#impact_id").val(),
					  from		: rating_from,
					  to		: rating_to,
					  assmnt	: assessment,
					  dfn		: definition,
					  clr		: color
					}
				   , function( retData ) {
					if( retData == 1 ) {
						jsDisplayResult("ok", "ok", "Impact successifully updated. . . ");						
					} else if( retData == 0 ){
						jsDisplayResult("info", "info", "Impact not changed. . . ");
					} else {
						jsDisplayResult("error", "error", "Error updating the impact. . . ");
					}
			},"json")	
		}
		return false;					 
	});	
	
	$("#change_impact").click(function(){
		var message 	= $("#impact_message")
		jsDisplayResult("error", "error", "Updating status . . . <img src='../images/loaderA32.gif' />");
		$.post("controller.php?action=changeImpstatus",
			   {
					id 		: $("#impact_id").val(),
					status  : $("#impact_status :selected").val() 
				},
			   function( retData ) {
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Impact status updated succesfully . . . ");						
				} else if( retData == 0 ) {
					jsDisplayResult("info", "info", "No change in status was made . . . ");					
				} else {
					jsDisplayResult("error", "error", "Error updating status . . . ");					
				}											  
		});
		return false;
	});
	
	$("#cancel_impact").click(function(){
		history.back();
		return false;								   
	});
})