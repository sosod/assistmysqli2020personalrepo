$(function(){


	if( $("#residual_id").val() == undefined)
	{
		ResidualRisk.get();
	}
	
	$("#add_residual").click( function(){
		ResidualRisk.save();
		return false;
	});
	
	$("#edit_residual").click( function(){
		ResidualRisk.edit();
		return false;
	})
	
	$("#change_residual").click( function(){
		ResidualRisk.change();
		return false;
	})	
		
	$("#cancel_residual").click(function(){
		history.back();
		return false;		 
	});
	
});

var ResidualRisk 		= {
		
		get				: function()
		{
			$.get("controller.php?action=getResidualRisk", function( data ) {
				$.each( data, function( index, val){
					ResidualRisk.display(val);					
				});				
			} ,"json")
		
		} , 

		save			: function()
		{
			var message 		= $("#residual_risk_message")						
			var residual_from 	= $("#residual_risk_from").val();
			var residual_to 	= $("#residual_risk_to").val();
			var residual		= $("#residual_magnitude").val();
			var response  		= $("#residual_response").val();
			var color 			= $("#residual_color").val();

			if ( residual_from == "") {
				jsDisplayResult("error", "error", "Please enter the rating from for this residual risk . . . ");
				return false;
			} else if( residual_to == "" ) {
				jsDisplayResult("error", "error", "Please enter the rating to for this residual risk. . . ");			
				return false;
			} else if ( residual == "" ) {
				jsDisplayResult("error", "error", "Please enter the magnitude for this residual risk . . . ");
				return false;
			} else if( response == "" ) {
				jsDisplayResult("error", "error", "Please enter the response for this residual risk . . . ");
				return false;			
			} else {
				jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=newResidualRisk", 
					   {
						   from		: residual_from,
						   to		: residual_to,
						   magn		: residual,
						   resp		: response,
						   clr		: color
						}
					   , function( retData ) {
					 if( retData > 0 ) {
						 $("#residual_risk_from").val("")
						 $("#residual_risk_to").val("")
						 $("#residual_magnitude").val("")
						 $("#residual_response").val("")
						 jsDisplayResult("ok", "ok", "Residual risk saved successfully . . . ");
						 
						 var data = { id : retData, rating_from : residual_from , rating_to : residual_to, magnitude : residual , response : response , color : color , active : 1  }
						 ResidualRisk.display( data ); 
					 } else {
						 jsDisplayResult("error", "error", "There was an error saving , please try again . . . ");	 
					 }
				},"json")	
			}
		} ,
		
		change			: function()
		{
			jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");										 
			$.post( "controller.php?action=changeResidualStatus", 
				   {
					   id  		: $("#residual_id").val(),
					   status   : $("#residual_status :selected").val() 	   
					}, function( deactData ) {
				if( deactData == 1){
					jsDisplayResult("ok", "ok", "Residual risk status changed . . . ");
				} else if( deactData == 0){
					jsDisplayResult("info", "info", "No change was made to the residual risk . . . ");
				} else {
					jsDisplayResult("error", "error", "Error saving , please try again . . . ");
				}									 
			})					
		} , 
		
		edit			: function()
		{
			var message 		= $("#residual_risk_message")						
			var residual_from 	= $("#residual_risk_from").val();
			var residual_to 	= $("#residual_risk_to").val();
			var residual		= $("#residual_magnitude").val();
			var response  		= $("#residual_response").val();
			var color 			= $("#residual_color").val();
		
			if ( residual_from == "") {
				jsDisplayResult("error", "error", "Please enter the rating from for this residual risk . . . ");
				return false;
			} else if( residual_to == "" ) {
				jsDisplayResult("error", "error", "Please enter the rating to for this residual risk. . . ");			
				return false;
			} else if ( residual == "" ) {
				jsDisplayResult("error", "error", "Please enter the magnitude for this residual risk . . . ");
				return false;
			} else if( response == "" ) {
				jsDisplayResult("error", "error", "Please enter the response for this residual risk . . . ");
				return false;			
			} else {
				jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=updateResidualRisk", 
					   {
						   id		: $("#residual_id").val(),
						   from		: residual_from,
						   to		: residual_to,
						   magn		: residual,
						   resp		: response,
						   clr		: color
						}
					   , function( retData ) {
							if( retData == 1 ) {
								jsDisplayResult("ok", "ok", "Residual risk successfully updated . . . ");	
							} else if( retData == 0 ){
								jsDisplayResult("info", "info", "Residual risk not changed . . . ");
								message.html("");
							} else {
								jsDisplayResult("error", "error", "Error updating the residual risk . . . ");
							}
				},"json")	
			}	
		} ,
		
		
		display			: function( val )
		{
			$("#residual_risk_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.magnitude}))
			  .append($("<td />",{html:val.response}))
			  .append($("<td />")
				 .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))	  
			  )
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"residualedit_"+val.id, name:"residualedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"residualdel_"+val.id, name:"residualdel_"+val.id}))					
			  )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "InActive")+"</b>"}))
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		
			  )
			)
			
			$("#change_"+val.id).live("click", function() {
				document.location.href = "change_residual_status.php?id="+val.id+"&";
				return false;										  
			});
			
			$("#residualedit_"+val.id).live("click", function() {
				document.location.href = "edit_residual.php?id="+val.id+"&";
				return false;										  
			});
			
			$("#residualdel_"+val.id).live("click", function() {
				if( confirm(" Are you sure you want to delete this residual risk ")) {														  					
					jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' >");
					$.post( "controller.php?action=changeResidualStatus", 
					{ id	: val.id , status : 2}, function( delData ) {
						if( delData == 1){
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Residual risk deleted . . . ");
						} else if( delData == 0){
							jsDisplayResult("info", "info", "No change was made to the residual risk . . . ");
						} else {
							jsDisplayResult("info", "info", "Error saving , please try again . . . ");
						}										 
					});
				 }
				return false;										  
			});
			
			$("#residualact_"+val.id).live("click", function() {
				jsDisplayResult("info", "info", "Activating  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=residualactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						jsDisplayResult("ok", "ok", "Residual risk activated . . . ");
					} else if( actData == 0){
						jsDisplayResult("info", "info", "No change was made to the residual risk . . . ");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again . . . ");
					}										 
				})															 
				return false;										  
			});
		}
		
		
		
		
};
