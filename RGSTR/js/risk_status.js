// JavaScript Document
$(function() {

	var message = $("#risk_status_message");
	/**
		Get all the risk statuses
	**/
	//message.html("loading ... <img src='../images/loaderA32.gif' >");
	if($("#displayResultCheck").val()!="ok") {
		JSdisplayResult("info", "info", "Loading... ");
	}
	
		
	$.get("controller.php?action=getRiskstatus", function( data ) {
		//message.html("");	
		
		if($("#displayResultCheck").val()!="ok") {
			JSdisplayResult("reset", "reset", "");
		}
		//$("div.ui-widget").hide();
		$.each( data , function( index, val) {
			
			$("#risk_status_table")
				.append($("<tr />",{id:"row_"+val.id, class:"active"+val.active})
					.append($("<td />",{html:val.id}))		  
					.append($("<td />",{html:val.name}))		  
					.append($("<td />",{html:val.client_terminology}))		  
					.append($("<td />")
						.append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))		
					)
					.append($("<td />", { html:(val.active == 1 ? "<b>Active</b>"  : "<b>Inactive</b>" ) }))
					.append($("<td />")
						.append($("<input />",{type:"submit", value:"Edit", id:"edit_status_"+val.id, name:"edit_status_"+val.id}))		  						
						.append($("<input />",{type:"submit", value:"Del", id:((val.defaults == 1 || val.count>0) ?  "del__"+val.id : "del_"+val.id), name:"del_status_"+val.id,style:"margin-left: 10px"}))
						.append($("<input />",{type:"submit", id:(val.defaults == 1 ? "change__"+val.id : "change_"+val.id), name:"change_"+val.id, value:"Change Status",style:"margin-left: 10px"}))
					)
				)
							  //.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Active", id:"activate_status_"+val.id, name:"activate_status_"+val.id}))		
						//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ), value:"Inactivate", id:"inactivate_status_"+val.id, name:"inactivate_status_"+val.id}))	  
						
				
		if(val.defaults == 1 || val.count>0)
		{
		   $("#del__"+val.id).hide();//.attr('disabled', 'disabled');
		   $("#change__"+val.id).hide();//.attr('disabled', 'disabled');
		}		
				
		$("#edit_status_"+val.id).live( "click" ,function(){
			document.location.href = "setup_edit.php?field=risk_status&id="+val.id+"&";
			return false;					 
		});
		
		$("#del_"+val.id).live( "click" ,function(){
			var message = $("#risk_status_message");
			if( confirm(" Are you sure you want to delete this risk status ")) {
				jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");	
				$.post("controller.php?action=deactivate", { id:val.id }, function( delData ) {
					if( delData == 1){
						jsDisplayResult("ok", "ok", "Status deleted . . .");	
						$("#row_"+val.id).fadeOut();
					} else if( delData == 0){
						jsDisplayResult("info", "info", "No change was made to the status . . .");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again  . . .");
					}		   
				});				
			}
			return false;											 
		});
		
		$("#activate_status_"+val.id).live( "click" ,function(){
			var message = $("#risk_status_message");															  
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");	
			$.post("controller.php?action=activate", { id:val.id }, function( actStat ) {
				if( actStat == 1){
					jsDisplayResult("info", "info", "Status activated . . .");				
				} else if( actStat == 0){
					jsDisplayResult("info", "info", "No change was made to the status . . .");
				} else {
					jsDisplayResult("error", "error", "Error saving , please try again . . .");
				}		   
			});
			return false;												 
		});
		
		$("#inactivate_status_"+val.id).live( "click" ,function(){
			var message = $("#risk_status_message");																
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' />");	
			$.post("controller.php?action=deactivate", { id:val.id }, function( deacData ) {
				if( deacData == 1){
					jsDisplayResult("ok", "ok", "Status deactivated . . .");
				} else if( deacData == 0){
					jsDisplayResult("info", "info", "No change was made to the status . . .");
				} else {
					jsDisplayResult("error", "error", "Error saving , please try again . . .");
				}		   
			});	  
			return false;											 
		});
		
		$("#change_"+val.id).live("click", function(){
			document.location.href = "setup_status.php?field=risk_status&id="+val.id+"&";
			return false;			
		});
		
		});
	}, "json");		   
	/**
		Adding a new risk status
	**/
	$("#add_status").live( "click" ,function() {
		var risk_status = $("#status").val();
		var client_term = $("#client_term").val();
		var color 		= $("#risktype_color").val(); 
		var message = $("#risk_status_message");
		if	( risk_status == "" ) {
			JSdisplayResult("error", "error", "Please enter risk status name");
			message.html(" ");
			return false;
		} else if ( client_term == "") {
			JSdisplayResult("error", "error", "Please enter cleint risk status terminology ");			
			message.html("");
			return false;	
		} else {
			JSdisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");	
			$.post( "controller.php?action=riskstatus" , { 
				   name					: risk_status,
				   client_terminology   : client_term, 
				   color				: color
			}, function( retStat ) {
				 if ( retStat > 0 ) {
				 $("#status").val("");
				 $("#client_term").val("");
				 $("#risktype_color").val("66FF00");
				 JSdisplayResult("ok", "ok", "Addition successfuly saved  . . .");
				 message.html(".. ");
				$("#risk_status_table")
					.append($("<tr />")
						.append($("<td />",{html:retStat}))
						.append($("<td />",{html:risk_status}))
						.append($("<td />",{html:client_term}))
						.append($("<td />")
							.append($("<span />",{html:color}).css({"background-color":"#"+color, "padding":"5px"}))		
						)
						.append($("<td />")
							.append($("<input />",{type:"submit", value:"Edit", id:"edit_status", name:"edit_status"}))		  
							.append($("<input />",{type:"submit", value:"Del", id:"del_status", name:"del_status"}))		  							
						)		
						.append($("<td />", {html:"<b>"+("Active" )+"</b>"})
							//.append($("<input />",{type:"submit", value:"Activate", id:"activate_status", name:"activate_status"}))		  
							//.append($("<input />",{type:"submit", value:"Inactive", id:"inactivate_status", name:"inactivate_status"}))			
						)
						.append($("<td />")
							.append($("<input />",{type:"submit", id:"change_"+retStat, name:"change_"+retStat, value:"Change Status"}))		  
						)
					)
					
					$("#edit_status_"+retStat).live( "click" ,function(){
						document.location.href = "edit_risk_status.php?id="+retStat+"&";
						return false;					 
					});					
					$("#change_"+retStat).live("click", function(){
						document.location.href = "risk_status_status.php?id="+retStat+"&";
						return false;
					});
					
				 } else {
					 JSdisplayResult("error", "error", "There was an error saving risk status , please try again . . .");
				}
			});
		}
		return false;									 
	});
	
	$("#edit_status").click(function(){

		var risk_status = $("#status").val();
		var client_term = $("#client_term").val();
		var color 		= $("#risktype_color").val(); 
		var message 	= $("#edit_risk_status_message");
		
		if	( risk_status == "" ) {
			JSdisplayResult("error", "error", "Please enter the default name");
			return false;
		} else if ( client_term == "") {
			JSdisplayResult("error", "error", "Please enter client terminology ");			
			return false;	
		} else {
			JSdisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=editRiskStatus" , { 
				   id					: $("#risk_status_id").val(),
				   name					: risk_status,
				   client_terminology   : client_term, 
				   color				: color
			}, function( retStat ) {
				if( retStat == 1 ) {
					//JSdisplayResult("ok", "ok", "Status successfully updated . . .");
					//message.html(" .. ");						
					document.location.href = 'risk_status.php?r[]=ok&r[]=Edit+saved+successfully.';
				} else if( retStat == 0 ){
					JSdisplayResult("info", "info", "No change made to the status . . .");
				} else {
					JSdisplayResult("error", "error", "Error updating the staus . . .");
				}
			},"json");
		}
		return false;									 
	})
	
	$("#cancel_status").click(function(){
		history.back();
		return false;								   
	})	
})
