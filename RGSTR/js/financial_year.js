// JavaScript Document
$(function() {
    /**
      Pops a jquery date picker
    **/
    $(".date").live("focus",function(){
      $(this).datepicker({ changeMonth : true, changeYear : true, dateFormat : "d-MM-yy"  });
    });
	
	$("#show_loading").ajaxStart(function(){
		$(this).show();
	});
	
	$("#show_loading").ajaxComplete(function(){
		$(this).fadeOut();
	});	
	
	$.get("controller.php?action=getFinYear", function( finYear ) {
		var message = $("#financial_year_message");											   
		if( $.isEmptyObject( finYear ) ) {
			jsDisplayResult("error", "error", "No financial year heas been set yet");	
		} else {
		$.each( finYear, function( index, fin){
		$("#financial_year_table")
		.append($("<tr />",{id:"tr_"+fin.id})
			.append($("<td />",{html:fin.id}))					  
			.append($("<td />",{html:fin.last_day}))
			.append($("<td />",{html:fin.start_date}))
			.append($("<td />",{html:fin.end_date}))
			.append($("<td />")
				.append($("<input />",{type:"submit", id:"edit_finyear_"+fin.id, id:"edit_finyear_"+fin.id, value:"Edit"}))																		                .append($("<input />",{type:"submit", id:"del_finyear_"+fin.id, id:"del_finyear_"+fin.id, value:"Del"}))
			)				
		)		
		
		$("#edit_finyear_"+fin.id).live("click",function(){
			document.location.href = "edit_financial_year.php?id="+fin.id+"&";
			return false;											  
		});
			$("#del_finyear_"+fin.id).live("click",function(){
			if( confirm(" Are you sure you want to delete this financial year") ){
				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=delFinYear",{ id : fin.id}, function( finData){
					if( finData == 1) {
						$("#tr_"+fin.id).fadeOut();	
						jsDisplayResult("ok", "ok", "Financial year deleted. . .");
					} else {
						jsDisplayResult("error", "error", "Error deleteing the financial year . . .");
					}
				})
			}
			return false;											  
			});
			});
		}	
	},"json");
	
	$("#add_fin_year").live( 'click' , function() {
		var message = $("#financial_year_message");
		var year_ending = $("#year_ending").val()
		var start_date  = $("#start_date").val()
		var end_date 	= $("#end_date").val()
		
		if(end_date == "" ){
			jsDisplayResult("error", "error", "Please select the end date for the year . . .");
			return false;
		} else if( start_date ==  "") {
			jsDisplayResult("error", "error", "Please select the start date for the year . . .");
			return false;			
		} else if( year_ending == "" ) {
			jsDisplayResult("error", "error", "Please select the year ending . . .");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving . . .<img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=financial_year", {
			   last_day		: year_ending,
			   start_date	: start_date,
			   end_date		: end_date 
			  },
			  function( retFinancial ) {
				if( retFinancial > 0 ) {
				$("#year_ending").val("")
				$("#start_date").val("")
				$("#end_date").val("")
				//jsDisplayResult("info", "info", "Financial year saved . . . <img src='../images/loaderA32.gif' />");
				$("#financial_year_table")
					.append($("<tr />",{id:"tr_"+retFinancial})
						.append($("<td />",{html:retFinancial}))		  
						.append($("<td />",{html:year_ending}))
						.append($("<td />",{html:start_date}))
						.append($("<td />",{html:end_date }))
						.append($("<td />")
						  .append($("<input />",{type:"button", value:"Edit", name:"edit_"+retFinancial, id:"edit_"+retFinancial}))
                        .append($("<input />",{type:"button", value:"Del", name:"del_"+retFinancial, id:"del_"+retFinancial}))
						)							
						)
					$("#edit_"+retFinancial).live("click",function(){
						document.location.href = "edit_financial_year.php?id="+retFinancial;
						return false;											  
					});
                    
                    $("#del_"+retFinancial).live("click",function(){
                        if( confirm(" Are you sure you want to delete this financial year") ){
            				jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' />");
                             $("#tr_"+retFinancial).fadeOut();
                                $.post( "controller.php?action=delFinYear",{ id : retFinancial}, function( finData){
                                if( finData == 1) {
                    				jsDisplayResult("info", "info", "Financial year deleted . . . ");
                                } else {
                    				jsDisplayResult("error", "error", "Error deleteing the financial year");
                                }
                            })
                        }
                        return false;
                     });
    				jsDisplayResult("ok", "ok", "Financial year successfully saved . . . ");
				} else {
					$.each(retFinancial, function( index, val){
	    				jsDisplayResult("error", "error", "Error "+index+"\r\n "+val.error+" . . . ");
					});
				}
			
		},"json");
		}
		
		return false;
	})

});
