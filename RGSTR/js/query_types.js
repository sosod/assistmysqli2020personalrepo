// JavaScript Document
$(function(){
	/**
	Get all tisk types
	**/
	var used = [];
	$.getJSON( "controller.php?action=getUsedTypes", function( usedTypes ) {
		$.each( usedTypes , function( index, val){
				used.push(val.type);					 
		});
	});


	$.get( "controller.php?action=getQueryType", function( data ) {

		var message = $("#risk_type_message")														 
		message.html("")
		$.each( data , function( index, val ) {
			$("#risk_type_table")
			  .append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.shortcode}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+val.id, name:"typeedit_"+val.id }))
				  .append($("<input />",{type:"submit", disabled:(($.inArray( val.id, used )) >= 0 ? "disabled" : ""), value:"Del", id:"typedel_"+val.id, name:"typedel_"+val.id }))			  
				 )
				.append($("<td />",{html:"<b>"+(val.active ==1 ? "Active"  : "Inactive" )+"</b>"})
				  //.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"typeactive_"+val.id, name:"typeactive_"+val.id }))
				  //.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ), value:"Inactivate", id:"typedeactive_"+val.id, name:"typedeactive_"+val.id }))							
				)
				.append($("<td />")
					.append($("<input />",{type:"submit", id:"change_"+val.id, name:"change_"+val.id, value:"Change Status"}))		  
				)
			  )
			$("#typeedit_"+val.id).live( "click", function() {
				document.location.href = "edit_query_type.php?id="+val.id;
				return false;										  
			});
			
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_query_type_status.php?id="+val.id;
				return false;										  
			});
			
			$("#typedel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this query type ")) {
					jsDisplayResult("info", "info", "Deleting . . .  <img src='../images/loaderA32.gif' />");
					$.post( "controller.php?action=changeQueryStatus",
						   {id		: val.id,
						   	status  : 2
						   }, function( typDelData ) {
						if( typDelData == 1) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Query Type deleted");
						} else if( typDelData == 0) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("info", "info", "No change was made to the query type");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again");
						}										 
					});
				}
				return false;										  
			});
			$("#typeactive_"+val.id).live( "click", function() {
				jsDisplayResult("info", "info", "Activating . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=QueryActivate", {id:val.id}, function( typDeactData ) {
					if( typDeactData == 1) {
						jsDisplayResult("ok", "ok", "Query Type activated");
					} else if( typDeactData == 0) {
						jsDisplayResult("info", "info", "No change was made to the query type");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again");
					}										 
				})															 
				return false;										  
			});

			
		});
	},"json");
	
	/**
		adding a new risk type
	**/
	$("#add_risktype").click( function() {
		var message   	= $("#risk_type_message");
		var shortcode 	= $("#risktype_shortcode").val();
		var type 		= $("#risktype").val();
		var description	= $("#risktype_descri").val();
		
		if ( shortcode == "" ) {
			jsDisplayResult("error", "error", "Please the short code for this query type");
			return false;
		} else if( type == "") {
			jsDisplayResult("error", "error", "Please enter the risk type");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . .  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=newQueryType",
				   { 
					   shortcode	: shortcode, 
					   name			: type, 
					   description	: description 
				   }, function( retData ) {
				   if( retData > 0 ) {
					   
					 $("#risktype_shortcode").val("");  
					 $("#risktype").val("");
					 $("#risktype_descri").val("");
					jsDisplayResult("ok", "ok", "Query type succesfully saved . . . ");					 
					 $("#risk_type_table")
					  .append($("<tr />")
						.append($("<td />",{html:retData}))
						.append($("<td />",{html:shortcode}))
						.append($("<td />",{html:type}))
						.append($("<td />",{html:description}))
						.append($("<td />")
						  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+retData, name:"typeedit_"+retData }))
						  .append($("<input />",{type:"submit", value:"Del", id:"typedel_"+retData, name:"typedel_"+retData }))			  
						 )
						.append($("<td />",{html:"<b>Active</b>"})
						  //.append($("<input />",{type:"submit", value:"Inactivate", id:"typedeactive", name:"typedeactive" }))							
						)
						.append($("<td />",{type:"submit", id:"change_"+retData, name:"change_"+retData, value:"Change status"}))
					  )
				  		$("#typeedit_"+retData).live( "click", function() {
							document.location.href = "edit_query_type.php?id="+retData;
							return false;										  
						});
					  
					  $("#change_"+retData).live( "click", function() {
						document.location.href = "change_query_type_status.php?id="+retData;
						return false;										  
					});
			   } else {
					jsDisplayResult("error", "error", "Error Saving query type . . . ");
				}
			  
			});
		}
		return false;
	});
	
	
	$("#edit_risktype").click(function(){
		var message   	= $("#edit_risk_type");
		var shortcode 	= $("#risktype_shortcode").val();
		var type 		= $("#risktype").val();
		var description	= $("#risktype_descri").val();
		
		if ( shortcode == "" ) {
			jsDisplayResult("error", "error", "Please the short code for this query type");
			return false;
		} else if( type == "") {
			jsDisplayResult("error", "error", "Please enter the risk type");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating . . .  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=updateQueryType",
		   { 
			   id			: $("#risktype_id").val(), 
			   shortcode	: shortcode, 
			   name			: type, 
			   description	: description 
		   }, function( retData ) {
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Query type successfully updated . . . ");
				} else if( retData == 0 ){
					jsDisplayResult("info", "info", "Query type not changed . . . ");
				} else {
					jsDisplayResult("error", "error", "Error updating the query type . . . ");
				}
			});
		}
		return false;
	});

	$("#cancel_risktype").click(function(){
		history.back();
		return false;								   
	})	
	
	$("#changes_risktype").click(function() {
		var message = $("#change_risk_type_message")
		jsDisplayResult("info", "info", "Updating . . .  <img src='../images/loaderA32.gif' />");
		$.post( "controller.php?action=changeQueryStatus",
			   { id		: $("#risktype_id").val() ,
			   	 status : $("#type_statuses :selected").val()
			   }, function( typDedeactData ) {
			if( typDedeactData == 1) {
				jsDisplayResult("ok", "ok", "Query Type status changed . . . ");
			} else if( typDedeactData == 0) {
				jsDisplayResult("info", "info", "No change was made to the query type status . . . ");
			} else {
				jsDisplayResult("error", "error", "Error saving , please try again . . . ");
			}										 
		})									
		return false;										  
	});	
});
