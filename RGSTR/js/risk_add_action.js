// JavaScript Document
//View Risk
$(function(){
	$("em").css({"color":"#FF0000", "font-size":"0.8em"})	 
		 
	var total 		= 0
	var current 	= 1
	var start 		= 0;
	var limit  		= 10;
	var displaying  = ""
	// initiallly it displays , for  the user logged in ie viewRiskActions
	getRiskData( current, start, limit, "viewAll" );
	$('body').data("displaying", "viewAll");
	// display all the risks and sets the display to diaplay all the risks 
	$("#btn_displayAll").click(function(){
		$(this).parent().hide();
		$("#displayOwn").show();
		getRiskData( current, start, limit, "viewAll" );								
		$('body').data("displaying", "viewAll");		
		return false;
	});
	// diaplay risks for the user logged in , and set to show for the user logged in
	$("#btn_displayOwn").click(function(){
		$(this).parent().hide();
		$("#displayAll").show();							
		getRiskData( current, start, limit, "viewMyn" );
		$('body').data("displaying", "viewMyn");
		return false;
	});	
	
	$("#b1").live("click", function(){
		current = 1;
		start = 0;
		limit = 10;
		displaying = $('body').data("displaying");
		getRiskData( current, start, limit, displaying);
		return false;
	});
	
	$("#b2").live("click", function(){
		limit = 10;
		var pages = Math.ceil( total/10 );
		current = parseFloat( current ) - 1;
			
		start 	= (current-1)*limit;
		displaying = $('body').data("displaying");		
		getRiskData( current, start, limit, displaying);
		return false;
	});
	
	$("#b3").live("click", function(){
		limit = 10;
		
		current = current+1;
		start = parseFloat( current - 1 ) * parseFloat( limit );
		displaying = $('body').data("displaying");		
		getRiskData( current, start, limit, displaying);
		return false;
	});

	$("#b4").live("click", function(){
		current =  Math.ceil( parseFloat( $('body').data('total') )/ 10);
		start = parseFloat(current-1) * parseFloat(limit);
		displaying = $('body').data("displaying");				
		getRiskData( current, start, limit, displaying);		 
		return false;
	});
	
});

function getRiskData( current, start, limit, viewPage ) {
	$("#risk_table").html("Loading risks  .... <img src='../images/loaderA32.gif' />");
	$.get("controller.php?action=getRisk",
	  { 
	  	page		: viewPage,
	  	start		: start,
		limit 		: limit
	  } ,
	  function(  data ) {
		  if( $.isEmptyObject( data ) ){
			$("#risk_table").html("No Risks were found");			  
		  } else {
			$("#risk_table").html("");
			$('body').data('total', data.total )
			createPager( data.total , current);
			populateHeaders( data.headers );
			populateData( data.riskData );			  
		  }
	}, "json");
}

function populateHeaders( headerData ) 
{
	$("#risk_table")
	.append($("<tr />")
		.append($("<th />",{html:namingHeader(headerData['riskitem'],'Risk Item')}))
		.append($("<th />",{html:namingHeader(headerData['risktype'],'Type')}))
		.append($("<th />",{html:namingHeader(headerData['riskcategory'],'Category')}))
		.append($("<th />",{html:namingHeader(headerData['riskdescription'],'Description')}))
		.append($("<th />",{html:namingHeader(headerData['background'],'Background')}))
		.append($("<th />",{html:namingHeader(headerData['riskowner'],'Risk Owner')}))		
		.append($("<th />",{html:""}))		
		)
}

function populateData( riskData )
{
	$.each( riskData, function( index , risk) {				   
		$("#risk_table")
		.append($("<tr />")
			.append($("<td />",{html:index}))		  
			.append($("<td />",{html:risk.type}))		  
			.append($("<td />",{html:risk.category}))		  
			.append($("<td />",{html:risk.description}))
			.append($("<td />",{html:risk.background}))
			.append($("<td />",{html:risk.tkname}))			
			.append($("<td />")
			 .append($("<input />",{type:"button", value:"Add New Action", id:"add_action_"+index, name:"add_action_"+index}))		  
			)
		)
		$("#add_action_"+index).live("click", function(){
			document.location.href = "new_action.php?id="+index;
			return false;
		});
	});
}

function namingHeader( idHeader, defaultName) 
{	
	if( idHeader == "" || idHeader == null  ) 
	{
		return defaultName;	
	} else {
		return idHeader;	
	}
}

function createPager( total , current ) 
{
	var pager;	
	var total = $("body").data("total")
	if( total%10 > 0){
		pager   = Math.ceil(total/10); 				
	} else {
		pager   = Math.floor(total/10); 				
	}
	
	$("#risk_table").html("");	
	$("#risk_table")
	.append($("<tr />")
	 .append($("<td />",{colspan:"7"}).addClass("noborder")
	  .append($("<table />",{id:"paging_risk", cellspacing:"5", width:"100%"}).addClass("noborder"))
	  )		  
	)
	$("#risk_table").html("");
	$("#risk_table")
	.append($("<tr />")
		.append($("<td />",{colspan:"7"})
			.append($("<input />",{type:"submit", name:"b1", value:" |< ", id:"b1", disabled:(current < 2 ? 'disabled' : '' )}))
			.append("&nbsp;")
			.append($("<input />",{type:"submit", name:"b2", value:" < ", id:"b2", disabled:(current < 2 ? 'disabled' : '' )}))
			.append("&nbsp;&nbsp;&nbsp;")
	 		.append($("<span />",{html:" Page "+current+"/"+(pager == 0 || isNaN(pager) ? 1 : pager)+" "}))
	 		.append("&nbsp;&nbsp;&nbsp;")
			.append($("<input />",{type:"submit", name:"b3", value:" > ", id:"b3", disabled:((current==pager || pager == 0) ? 'disabled' : '' )}))
			.append("&nbsp;")
			.append($("<input />",{type:"submit", name:"b4", value:" >| ", id:"b4", disabled:((current==pager || pager == 0) ? 'disabled' : '' )}))					
		)
		//.append($("<td />",{width:"50%"}))
	)		
}