// JavaScript Document
	$.widget( "ui.risk", {
		options : {
			table_id 	: "risk"+(Math.random(57)*98),
			getUrl 		: "controller.php?action=getRisk",
			assurance   : false,
			update 		: false,
			edit		: false,
			approve 	: false,
			view 		: true,
			loadingImg  : "loaderA32.gif",
			element		: this,
			total 		: 0,
			headers 	: [],
			residual	: "",
			inherent	: "",
			current		: 1,
			limit 		: 10,
			start 		: 0,
			page 		: "viewrisk",
			view		: "viewAll",
			section		: ""
		} ,
		/**
			This is executed everytime the widget is instantiated
		**/
		_init			 	: function() { 
			$("em").css({"color":"#FF0000", "font-size":"0.8em"})	
			var risk_widget  = this;
			this._getRisk();			
		} , 
		/**
		Executes when the widget is created , so we call function that we want to work once .......s
		**/
		_create  		 	: function() {		
		}, 
		/**
			Fetches total risks available and  sets the total
		**/
		_getTotalRisk 	 	: function() {
			$.get("controller.php?action=totalRisk", function( totalRisk ){
				$('body').data('total',totalRisk)			
				this.options.total = totalRisk;
			});		
		} ,
		_populateRisk 	: function( risk )
		{
			var that = this;		
			$.each( risk, function( index, riskObj){
			var tr = $("<tr />")								   
				$.each( riskObj, function( key, r_value){
					tr = tr.append($("<td />",{ html:r_value}))										  
				})
				
				if( that.options.page == "editrisk"){
					tr = tr.append($("<td />")
					.append($("<input />",{
					  type	: "submit",
					  name	: "edit_"+index,
					  value	: "Edit Risk",
					  id	: "edit_"+index
					  }))		
					.append($("<br />"))
					.append($("<input />",{type:"submit", name:"action_edit_"+index, value:"Edit Action", id:"action_edit_"+index}))		
					)
					$("#edit_"+index).live("click", function(){
						document.location.href = "editrisk.php?id="+index;								  
					});
					$("#action_edit_"+index).live("click", function(){
						document.location.href = "../actions/action_edit.php?id="+index;								  
					});				
				} else if( that.options.page == "updaterisk"){
					tr = tr.append($("<td />")
						.append($("<input />",{
						  type	: "submit",
						  name	: "update_"+index,
						  value	: "Update Risk",
						  id	: "update_"+index
						  }))		
						.append($("<br />"))
						.append($("<input />",{type:"submit", name:"action_update_"+index, value:"Update Action", id:"action_update_"+index}))		  			
						)	

					$("#update_"+index).live("click", function(){
						document.location.href = "updaterisk.php?id="+index;								  
						return false;
					});
					
					$("#action_update_"+index).live("click", function(){
						document.location.href = "../actions/action_update.php?id="+index;								  
						return false;
					});	
				} else if( that.options.page == "assurancerisk"){
					tr = tr.append($("<td />")
					.append($("<input />",{
					  type	: "submit",
					  name	: "assurance_"+index,
					  value	: "Assurance Risk",
					  id	: "assurance_"+index
					  }))		
					.append($("<br />"))
					.append($("<input />",{type:"submit", name:"action_assurance_"+index, value:"Assurance Action", id:"action_assurance_"+index}))		  			
					)
					$("#assurance_"+index).live("click", function(){
						document.location.href = "riskassurance.php?id="+index;								  
						return false;
					});
					
					$("#action_assurance_"+index).live("click", function(){
						document.location.href = "../actions/action_assurance.php?id="+index;								  
						return false;
					});	
				}
				$("#risk_table").append(tr);
		 	});
			
		}
		,
		_populateHeaders		: function( headers ) {
			var $this = this;
			var tr = $("<tr />")
			$.each( headers, function( index, value){
				tr.append($("<th />", {html:value}))					  
			});	
			tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.page == "editrisk" ? "table-cell" : "none")}))
			tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.page == "updaterisk" ? "table-cell" : "none")}))
			tr = tr.append($("<th />",{html:"&nbsp;"}).css({"display":($this.options.page == "assurancerisk" ? "table-cell" : "none")}))
	
			tr.appendTo("#risk_table");
		} ,
		
		_createPager 		: function(total, columns ) {
			var rk 		= this;
			
			if( this.options.page == "editrisk"){
				columns = parseFloat(columns)+1; 
			} else if( this.options.page == "updaterisk"){
				columns = parseFloat(columns)+1;				
			} else if( this.options.page == "assurancerisk"){
				columns = parseFloat(columns)+1;
			}	
			var pages;
			if( total%rk.options.limit > 0){
				pages   = Math.ceil(total/rk.options.limit); 				
			} else {
				pages   = Math.floor(total/rk.options.limit); 				
			}
			$("#risk_table")
			.append($("<tr/>")
				.append($("<td />",{colspan:columns})
				  .append($("<input />",{type:"button", value:" |< ", id:"first", name:"first", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))
				  .append("&nbsp;")
				  .append($("<input />",{type:"button", value:" < ", id:"previous", name:"previous", disabled:(rk.options.current < 2 ? 'disabled' : '' )}))
				  .append("&nbsp;&nbsp;&nbsp;")
				  .append($("<span />", {colspan:"2",html:"Page "+rk.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages), align:"center"}))	
				  .append("&nbsp;&nbsp;&nbsp;")	
				  .append($("<input />",{type:"button", value:" > ", id:"next", name:"next", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))			  
				  .append("&nbsp;")
				  .append($("<input />",{type:"button", value:" >| ", id:"last", name:"last", disabled:((rk.options.current==pages || pages == 0) ? 'disabled' : '' )}))				  
				 )											
				//.append($("<td />",{colspan:columns-6}))		   
				)
			$("#next").bind("click", function(evt){
				rk._getNext( rk );
			});
			$("#last").bind("click",  function(evt){
				rk._getLast( rk );
			});
			$("#previous").bind("click",  function(evt){
				rk._getPrevious( rk );
			});
			$("#first").bind("click",  function(evt){
				rk._getFirst( rk );
			});			

		} ,		
		_getNext  			: function( rk ) {
			rk.options.current = rk.options.current+1;
			rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
			this._getRisk();
		},	
		_getLast  			: function( rk ) {
			//var rk 			   = this;
			rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
			rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
			this._getRisk();
		},	
		_getPrevious    	: function( rk ) {
			rk.options.current  = parseFloat( rk.options.current ) - 1;
			rk.options.start 	= (rk.options.current-1)*rk.options.limit;
			this._getRisk();			
		},	
		_getFirst  			: function( rk ) {
			rk.options.current  = 1;
			rk.options.start 	= 0;
			this._getRisk();				
		},	
		
		_getRisk 	    	: function() {
			var rk = this;
			var loader = $(this.element).append($("<div />",{id:"loading"}));
			$("#loading").ajaxStart(function(){
				$(this).html("Loading . . <img src='../images/loaderA32.gif' />")
						.css({	
							 	"background-color":"", 
								"position"  :"absolute",
								"z-index"	:"9999",
								"top"		: "200px",
								"left"		: "500px"
							 })				 
			});
			$("#loading").ajaxComplete(function(){
				//$(this).empty();					 
			});						
			$.get( this.options.getUrl, 
				  {
					page 		: this.options.view,
					start		: this.options.start,
					limit 		: this.options.limit,
					section 	: this.options.section
				  }, function( risk ) {
					$("#risk").html("");
					$("#risk").append($("<table />",{id:"risk_table", width:"100%"}));
					rk.options.total =  risk.total;
					rk._createPager( risk.total, risk.x );
					rk._populateHeaders( risk.headers )
					rk._populateRisk( risk.riskData )
					//rk._displayRisk(risk.total, risk.x)
				  }
			,"json")
		},
		
		_displayRisk  : function( total, count) {
			rk = this;
			var dp = Math.round( total/10 );
			
			$("#risk_table")
			 .append($("<tr />")
				.append($("<td />", {colspan:count, align:"Center"})
					.append($("<select />", { id: "display"})
						//.append($("<option />", { html: rk.options.limit}))		    		  						
					)		  
				 )	  			
			  )	
		 
			for( var x=10; x <= 80; x=x+10 ) {
				$("#display").append($("<option />", { html: x , selected : (rk.options.limit == x ? "selected" : "") }))	
			} 
			
			$("#display").live("change", function(){
					$(this).attr("selected", "selected")
					rk.options.limit = $(this).val();
					rk._getRisk();	
				return false;
			});	
		} , 
		
		headerSpecific 		: function()
		{
			that = this;
			//console.log("coming to the header specific function")
			//console.log( that.options.headerNames );	
		}
		 
	});

