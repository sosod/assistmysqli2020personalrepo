// JavaScript Document
$(function(){
		   
		$("#edit_likelihood").click(function(){  
	
		var message 	= $("#likelihood_message")						
		var rating_from = $("#lkrating_from").val();
		var rating_to 	= $("#lkrating_to").val();
		var assessment	= $("#lkassessment").val();
		var definition  = $("#lkdefinition").val();
		var probability = $("#lkprobability").val();		
		var color 		= $("#llkcolor").val();
	
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this likelihood . . . ");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this likelihood . . . ");
			return false;
		} else if ( assessment == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this likelihood . . . ");
			return false;
		} else if( definition == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this likelihood . . . ");			
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateLikelihood",
				   {
					   id		: $("#likelihood_id").val(),
					   from		: rating_from,
					   to		: rating_to,
					   assmnt	: assessment,
					   dfn		: definition,
					   prob		: probability,
					   clr		: color
					}
				   , function( retData ) {
						 if( retData == 1 ) {
							jsDisplayResult("ok", "ok", "Likelihood successfully updated . . . "); 
						} else if( retData == 0 ){
							jsDisplayResult("info", "info", "Likelihood not changed . . . "); 
						} else {
							jsDisplayResult("error", "error", "Error updating the likelihood . . . ");
						}
			},"json")	
		}
		return false;								  
	});
	
	$("#change_likelihood").click(function(){
		jsDisplayResult("error", "error", "Updating . . . <img src='../images/loaderA32.gif' >");											  
		$.post( "controller.php?action=changeLikestat", 
			   { id		: $("#likelihood_id").val(),
			   	 status : $("#like_status :selected").val()
			   }, function( retData ) {
			if( retData  == 1){
				jsDisplayResult("ok", "ok", "Likelihood status changed . . .");
			} else if( retData  == 0){
				jsDisplayResult("info", "info", "No change was made to the likelihood status . . .");
			} else {
				jsDisplayResult("error", "error", "Error updating status  , please try again . . .");				
			}										 
		})									
		return false;										  								   
	});	
		
		
	$("#cancel_edit_likelihood").click(function(){
		history.back();
		return false;								   
	});
	
	   
})
