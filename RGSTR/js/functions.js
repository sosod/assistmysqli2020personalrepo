/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 7/13/11
 * Time: 1:10 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

   $(".logs").click(function(){
       var id = $(this).attr("id");
       var table = id.replace("view_", "");
       $.post("controller.php?action=view_log", { table_name : table}, function( data ){
            $("#log_div").html("");
            if( $.isEmptyObject(data)) {
                $("<div />",{id:"logs",html:"There are no audit logs "}).insertAfter("#log_div")
            } else {

               $("#log_div").append($("<div />",{id:"logs"}).append($("<table />",{id:"log_table"})))

                $("#log_table").append($("<tr />")
                    .append($("<th />",{html:"Date"}).addClass("log"))
                    .append($("<th />",{html:"Ref"}).addClass("log"))
                    .append($("<th />",{html:"Audit log"}).addClass("log"))
                )
                $.each( data, function( index, log){
                    $("#log_table").append($("<tr />")
                        .append($("<td />",{html:log.date}))
                        .append($("<td />",{html:log.ref}))
                        .append($("<td />",{html:log.changes}))
                    )
                });
            }

       },"json");

   });
   
   
   $(".remove").live("click", function(){
	 var id 	  = this.id;
	 var original = $(this).attr("title");
	  if( confirm("Are you sure you want to delete this attachment"))
	  {
		  $.post("controller.php?action=removeActionFile", { id : id , original : original}, function( response ){
			  $(".messages").remove();
			  if( response.error)
			  {
				  $("#li_"+id).html("Error removing the file");
				  $("#attachment_list").prepend("<li class='messages'><div class='ui-state-error ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
			  } else {
				  $("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
				  $("#parent_"+id).fadeOut();
			  }			  
		  },"json");
	  }  
	 return false;  
   });
   
   $(".justremove").live("click", function(){
	   
	   var id 		= $(this).attr("id")
	   var filename = $(this).attr("title");
	   $.post("controller.php?action=justRemoveActionFile",{ id : id, file : filename }, function( response ){
	    $(".messages").remove();
		 if( response.error )
		 {
			 $("#attachment_list").prepend("<li class='messages'><div class='ui-state-error ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
		 } else {
			 $("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
			 $("#li_"+id).fadeOut();
		 }		   
	   },"json");
	  return false; 
   });
    
});

    function ajaxFileUpload( fileupload )
	{
        var panelId  = $(fileupload).attr("id");
		$("#loading")
		.ajaxStart(function(){
			//$("#file_loading").html("Uploading . . . <img src='../images/bar180.gif' />");
		})
		.ajaxComplete(function(){
			//$(this).html("");
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=processActionAttachments',
				secureuri	  : false,
				fileElementId : panelId,
				dataType	  : 'json',
				success       : function (response, status)
				{
					$(".newuploads").remove()
					$(".messages").remove();
					if(!response.error)
					{
						if( $("#attachment_list").length == 0){
							$("#file_loading").append($("<ul />",{id:"attachment_list"}))
						}
						$(".messages").remove();
						$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></li>")
						if( $.isEmptyObject(response.filesuploaded) ){
							//$("#file_loading").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='"+file+"' id='"+index+"' class='justremove'><span></span>Remove</a>" )
							});
						}
						
					} else {
						$("#file_loading").html(" Error uploading  "+response.error);
					}
				},
				error: function (data, status, e)
				{
					$("#file_loading").html(" Ajax error "+e);
				}
			}
		)
		return false;
	}