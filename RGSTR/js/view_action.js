// JavaScript Document
$(function(){
	
		$.get("controller.php?action=getActions", {id : $("#risk_id").val() }, function( data ){
		var message = $("#view_action_message");	
		if( $.isEmptyObject( data ) ) {
			$("#view_action_message").html( "No result were returned" )
		} else {
		  $.each( data , function( index ,val ) {
			$("#action_table")
			.append($("<tr />")
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.action}))
				.append($("<td />",{html:val.status}))
				.append($("<td />",{html:val.progress}))
				.append($("<td />",{html:""}))
				.append($("<td />")	)
			)
		  });	
		}
	}, "json");
	
});