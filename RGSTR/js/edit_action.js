// JavaScript Document
$(function(){
		   
	$("table#edit_action_table").find("th").css({"text-algn":"left"})	   
	
	$("#edit_action").click(function(){
		$("input:text, textarea, select").each(function() {
			$(this).removeClass("required");
		});
		var message 		= $("#edit_action_message").slideDown();
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val();		
		var rs_deliverable  = $("#deliverable").val();
		var rs_timescale    = $("#timescale").val();		
		var rs_deadline     = $("#deadline").val();
		var rs_progress     = $("#progress").val();
		var rs_status       = $("#action_status :selected").val();
		var rs_remindon     = $("#remindon").val();		
		var err = false;
		
		if( rs_action == "" ) {
			$("#action").addClass("required");
			err = true;
		}
		if( rs_action_owner == "" ) {
			$("#action_owner").addClass("required");
			err = true;
		}
		if( rs_deliverable == "" ) {
			//not required
		}
		if( rs_timescale == "" ) {
			//not required
		}
		if( rs_deadline == "" ) {
			$("#deadline").addClass("required");
			err = true;
		}
		if( rs_remindon == "" ) {
			$("#remindon").addClass("required");
			err = true;
		}
		if(err) {
			jsDisplayResult("error", "error", "Please complete the missing information as highlighted.");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Updating "+window.action_object_name+" ...  <img src='../images/loaderA32.gif' />");	
			$.post( "controller.php?action=updateRiskAction" , 
				   {
					   	id				: $("#actionid").val() ,
						r_action 		: rs_action ,
						action_owner    : rs_action_owner ,
						deliverable     : rs_deliverable ,
						timescale       : rs_timescale ,
						deadline     	: rs_deadline ,
						remindon     	: rs_remindon ,
						progress     	: rs_progress ,	
						status			: rs_status
				   } ,
				   function( response ) {
					if( !response.error){ 
						jsDisplayResult("ok", "ok", response.text );
						//message.html( response.text ).addClass('ui-state-highlight')	
					} else {
						jsDisplayResult("error", "error", response.text );
						//message.html( response.text ).addClass('ui-state-error')	
					}

			} ,"json");
		}
		return false;
	});
	
		$("#delete_action").live( "click", function(){
		var message = $("#edit_action_message");													
		if(confirm("Are you sure you want to delete this "+window.action_object_name+"? ")){								 				
			$.post("controller.php?action=deleteAction", { id : $("#actionid").val() }, function( ret ){
				//$("#tr_"+val.id).fadeOut();
				//message.html( "Action deleted" )
						if( ret == 1 ) {
					message.html( ""+window.action_object_name+" deleted" )	
				} else if( ret == 0 ) {
					message.html( ""+window.action_object_name+" is already deactivated" )	
				} else {
					message.html( "Error deleting the "+window.action_object_name+" " )	
				}													  
				});
			}
			return false;
		});

	$("#cancel_action").click(function(){
		history.back();
		return false;
	})	
	
	$("#viewactionlog").click(function(){
		$("#actioneditlog").html("<img src='../images/loaderA32.gif' />");
		$.getJSON("controller.php?action=getEditLog", { id:$("#actionid").val() }, function( editLogs ){												
			$("#actioneditlog").html("");		
			
			$("#actioneditlog").append($("<table />",{id:"action_edit_table",width:"67%"}))																				
				$("#action_edit_table").append($("<tr />")
				  .append($("<th />",{html:"Date"}))	
				  .append($("<th />",{html:"Audit Log"}))	
				  .append($("<th />",{html:"Status"}))	
				)						   			
			$.each( editLogs, function( index , action){
				$("#action_edit_table").append($("<tr />")	
				  .append($("<td />",{html:action.date}))	
				  .append($("<td />",{html:action.changeMessage}))	
				  .append($("<td />",{html:((action.status == null || action.status == "") ? "New" : action.status)}))					  
				)						   
			})										   
		});
		return false;								   
	})
	
	/**
		Remove the attachment
	
	$(".remove_attach").click(function(){
		alert("sssssssssssssssssss")
		var id = $(this).attr("id");
		var filename = $(this).attr("title");
		$.post("controller.php?action=removeActionFile",{ 
			   timeid		: id,
			   originalname : filename 
	    },function( response ){
		   if( response.error )	{
					   
		  } else {
			$("#parent_"+id).fadeOut();
			//$("#risk_message").slideDown().html( response.text )
		 }												  
		},'json');
		return false;							   
	});
	*/
	
});