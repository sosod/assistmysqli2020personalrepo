$(function(){

	Notification.get();
	$("#new").live("click", function(){
		$("<div />",{id:"notificationdialog"})
		 .append($("<table />")
			.append($("<tr />")
			   .append($("<th />"))
			   .append($("<td />",{html:"Reminder Emails"}))			   
			)
			.append($("<tr />")
			   .append($("<th />",{html:"Receive When:"}))
			   .append($("<td />")
				  .append($("<select />",{name:"recieve_when", id:"recieve_when"})
					 .append($("<option />",{text:"Daily", value:"daily"}))
					 .append($("<option />",{text:"Weekly", value:"weekly"}))
				  )	   
			   )			   
			)
			.append($("<tr />",{id:"days"}).css({"display":"none"})
			  .append($("<th />",{html:"Receive Day:"}))
			  .append($("<td />")
				  .append($("<select />",{id:"recieve_day", name:"recieve_day"})
					 .append($("<option />",{value:"1", text:"Monday"}))
					 .append($("<option />",{value:"2", text:"Tuesday"}))
					 .append($("<option />",{value:"3", text:"Wednesday"}))
					 .append($("<option />",{value:"4", text:"Thursday"}))
					 .append($("<option />",{value:"5", text:"Friday"}))
					 .append($("<option />",{value:"6", text:"Saturday"}))
					 .append($("<option />",{value:"0", text:"Sunday"}))
				  )	  
			  )
			)
			.append($("<tr />")
			   .append($("<th />",{html:"What To Receive:"}))
			   .append($("<td />")
				  .append($("<select />",{id:"recieve_what", name:"recieve_what"})
					 .append($("<option />",{text:"Actions due this week", value:"due_this_week"}))
					 .append($("<option />",{text:"Actions due on or before this week", value:"due_on_or_before_week"}))
					 .append($("<option />",{text:"Actions due today", value:"due_today"}))
					 .append($("<option />",{text:"Actions due on or before today", value:"due_on_or_before_today"}))
					 .append($("<option />",{text:"All Incomplete actions", value:"all_incomplete_actions"}))
				  )	   
			   )			   
			)	 		  
			.append($("<tr />")
			   .append($("<th />"))
			   .append($("<td />"))			   
			)	 		  			
		 )
		 .dialog({
			 		autoOpen	: true,
			 		modal		: true,
			 		title		: "New Notification",
			 		width		: "400",
			 		buttons		: {
			 						"Save"		: function()
			 						{
			 							Notification.save()
			 						} ,
			 						"Cancel"	: function()
			 						{			 							
			 							$("#notificationdialog").dialog("destroy");
			 							$("#notificationdialog").remove()
			 						}
			 	
		 						  }
		 })
		
		 $("#recieve_when").live("change", function(){
			 if($(this).val() == "weekly"){
				 $("#days").show(); 
			 } else if( $(this).val() == "daily" ){
				 $("#days").hide();
			 }
			return false; 
		 });
		
	});
	
	
	
});

var Notification = {
		
		save		: function()
		{
			jsDisplayResult("info", "info", "Saving notification . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?action=saveNotification", { 
						recieve_when : $("#recieve_when :selected").val(),
						recieve_day	 : $("#recieve_day :selected").val(),
						recieve_what : $("#recieve_what :selected").val(),
			 		}, function( response ) {
			 			if( response.error ) {
 							$("#notificationdialog").dialog("destroy");
 							$("#notificationdialog").remove()			 				
							jsDisplayResult("error", "error", response.text);
						} else {
 							$("#notificationdialog").dialog("destroy");
 							$("#notificationdialog").remove()
 							Notification.get();
							jsDisplayResult("ok", "ok", response.text);
						}	
			 		}, "json")			
		} , 
		
		update		: function( id )
		{
			jsDisplayResult("info", "info", "Updating notification . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?action=updateNotification",
					{ 
						id			 : id,
						recieve_email: $("#recieve :selected").val(),
						recieve_when : $("#recieve_when :selected").val(),
						recieve_day	 : $("#recieve_day :selected").val(),
						recieve_what : $("#recieve_what :selected").val(),
			 		},	function( response ) {
			 			if( response.error ) {
 							$("#notificationdialog").dialog("destroy");
 							$("#notificationdialog").remove()			 				
							jsDisplayResult("error", "error", response.text);
						} else {
 							$("#notificationdialog").dialog("destroy");
 							$("#notificationdialog").remove()
 							Notification.get();
							jsDisplayResult("ok", "ok", response.text);
						}
					},"json")
		} , 
		
		get			: function()
		{

			$("#profile").html("")
			$("#profile").append($("<tr />")
			   .append($("<th />",{html:"Ref"}))
			   .append($("<th />",{html:"Notification"}))
			   .append($("<th />",{html:"Receive Day"}))
			   .append($("<th />",{html:"Week Day"}))
			   .append($("<th />",{html:"&nbsp;"}))
			)
			$.getJSON("controller.php?action=getNotifications", function( notifications ){
				if($.isEmptyObject(notifications)){
					$("#profile").append($("<tr />")
					   .append($("<td />",{colspan:"6", html:"There are no notification set yet"}))		
					)
				} else {
				$.each( notifications, function( index , notification){
					
					$("#profile").append($("<tr />",{id:"tr_"+notification.id})
						.append($("<td />",{html:notification.id}))
						.append($("<td />",{html:notification.what}))
						.append($("<td />",{html:notification.when}))
						.append($("<td />",{html:notification.day}))
						.append($("<td />")
						  .append($("<input />",{type:"button", name:"edit_"+notification.id, id:"edit_"+notification.id, value:"Edit"}))
						  .append($("<input />",{type:"button", name:"del_"+notification.id, id:"del_"+notification.id, value:"Del"}))
						)
					)
					
					$("#del_"+notification.id).live("click", function(){
						if(confirm("Are you sure you want to delete this notification"))
						{	
							jsDisplayResult("info", "info", "Deleting action status . . . <img src='../images/loaderA32.gif' >");
							$.post("controller.php?action=deleteNotification",{
								id	: notification.id
							}, function( response ){
					 			if( response.error ) {			 				
									jsDisplayResult("error", "error", response.text);
								} else {
		 							$("#tr_"+notification.id).fadeOut();
									jsDisplayResult("ok", "ok", response.text);
								}								
							},"json");
						}
						return false;
					});
					
					$("#edit_"+notification.id).live("click", function(){
							$("<div />",{id:"notificationdialog"})
							 .append($("<table />")
								.append($("<tr />")
								   .append($("<th />"))
								   .append($("<td />",{html:"Reminder Emails"}))			   
								)
								.append($("<tr />")
								   .append($("<th />",{html:"Receive"}))
								   .append($("<td />")
									  .append($("<select />",{id:"recieve", name:"recieve"})
										 .append($("<option />",{value:"0", text:"No", selected:(notification.recieve == 1 ? "selected" : "")}))
										 .append($("<option />",{value:"1", text:"Yes", selected:(notification.recieve == 0 ? "selected" : "")}))
									   )	   
								   )
								)
								.append($("<tr />")
								   .append($("<th />",{html:"Receive When:"}))
								   .append($("<td />")
									  .append($("<select />",{name:"recieve_when", id:"recieve_when"})
										 .append($("<option />",{text:"Daily", value:"daily", selected:(notification.recieve_when == "daily" ? "selected" : "")}))
										 .append($("<option />",{text:"Weekly", value:"weekly", selected:(notification.recieve_when == "weekly" ? "selected" : "")}))
									  )	   
								   )			   
								)
								.append($("<tr />",{id:"days"}).css({"display":(notification.recieve_when == "weekly" ? "table-row" : "none")})
								  .append($("<th />",{html:"Receive Day:"}))
								  .append($("<td />")
									  .append($("<select />",{id:"recieve_day", name:"recieve_day"})
										 .append($("<option />",{value:"1", text:"Monday", selected:(notification.recieve_day == 1 ? "selected" : "")}))
										 .append($("<option />",{value:"2", text:"Tuesday", selected:(notification.recieve_day == 2 ? "selected" : "")}))
										 .append($("<option />",{value:"3", text:"Wednesday", selected:(notification.recieve_day == 3 ? "selected" : "")}))
										 .append($("<option />",{value:"4", text:"Thursday", selected:(notification.recieve_day == 4 ? "selected" : "")}))
										 .append($("<option />",{value:"5", text:"Friday", selected:(notification.recieve_day == 5 ? "selected" : "")}))
										 .append($("<option />",{value:"6", text:"Saturday", selected:(notification.recieve_day == 6 ? "selected" : "")}))
										 .append($("<option />",{value:"0", text:"Sunday", selected:(notification.recieve_day == 0 ? "selected" : "")}))
									  )	  
								  )
								)
								.append($("<tr />")
								   .append($("<th />",{html:"What To Receive:"}))
								   .append($("<td />")
									  .append($("<select />",{id:"recieve_what", name:"recieve_what"})
										 .append($("<option />",{text:"Actions due this week", value:"due_this_week", selected:(notification.recieve_what == "due_this_week" ? "selected" : "")}))
										 .append($("<option />",{text:"Actions due on or before this week", value:"due_on_or_before_week", selected:(notification.recieve_what == "due_on_or_before_week" ? "selected" : "")}))
										 .append($("<option />",{text:"Actions due today", value:"due_today", selected:(notification.recieve_what == "due_today" ? "selected" : "")}))
										 .append($("<option />",{text:"Actions due on or before today", value:"due_on_or_before_today", selected:(notification.recieve_what == "due_on_or_before_today" ? "selected" : "")}))
										 .append($("<option />",{text:"All Incomplete actions", value:"all_incomplete_actions", selected:(notification.recieve_what == "all_incomplete_actions" ? "selected" : "")}))
									  )	   
								   )			   
								)	 		  
								.append($("<tr />")
								   .append($("<th />"))
								   .append($("<td />"))			   
								)	 		  			
							 )
							 .dialog({
								 		autoOpen	: true,
								 		modal		: true,
								 		title		: "New Notification",
								 		width		: "400",
								 		buttons		: {
								 						"Save"		: function()
								 						{
								 							Notification.update( notification.id )
								 						} ,
								 						"Cancel"	: function()
								 						{			 							
								 							$("#notificationdialog").dialog("destroy");
								 							$("#notificationdialog").remove()
								 						}
								 	
							 						  }
							 })
							
							 $("#recieve_when").live("change", function(){
								 if($(this).val() == "weekly"){
									 $("#days").show(); 
								 } else if( $(this).val() == "daily" ){
									 $("#days").hide();
								 }
								return false; 
							 });
						return false;
					});
					
				})
				}
				$("#profile").append($("<tr />")
				  .append($("<td />",{colspan:"6"}).css({"text-align":"right"})
					 .append($("<input />",{type:"button", name:"new", id:"new", value:"New Notification"}))	  
				  )		
				)
				
			});
		}
		
}
