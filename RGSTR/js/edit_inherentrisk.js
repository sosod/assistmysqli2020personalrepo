// JavaScript Document
$(function(){
		   
	$("#edit_inherentrisk").click(function(){
		var message 	= $("#inherentrisk_message")						
		var rating_from = $("#inherent_rating_from").val();
		var rating_to 	= $("#inherent_rating_to").val();
		var magnitude	= $("#inherent_risk_magnitude").val();
		var response	= $("#inherent_response").val();
		var color 		= $("#inherent_color").val();
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating for this inherent risk . . .");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this inherent risk . . .");
			return false;
		} else if ( magnitude == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this inherent risk . . .");
			return false;
		} else if( response == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this inherent . . .");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateInherentRisk",
				   {
					   id 		: $("#inherentrisk_id").val(), 
					   from		: rating_from,
					   to		: rating_to,
					   magnitude: magnitude,
					   response : response,
					   clr		: color
				}
				   , function( retData  ) {
						if( retData == 1 ) {
							jsDisplayResult("ok", "ok", "Inherent risk successfully updated . . .");					
						} else if( retData == 0 ){
							jsDisplayResult("info", "info", "Inherent risk not changed . . .");
						} else {
							jsDisplayResult("error", "error", "Error updating the inherent risk . . .");
						}
			},"json")
		}
		return false;			   
	});
	
	$("#change_inherentrisk").click(function() {
		var message 	= $("#inherentrisk_message")												 
		$.post( "controller.php?action=changeInherent", 
			   {
				   id	  : $("#inherentrisk_id").val(),
				   status : $("#inherent_status :selected").val()
				}, function( deactData ) {
			if( deactData == 1){
				jsDisplayResult("ok", "ok", "Inherent risk deactivated . . .");
			} else if( deactData == 0){
				jsDisplayResult("info", "info", "No change was made to the inherent risk status . . .");
			} else {
				jsDisplayResult("error", "error", "Error saving , please try again . . .");
			}										 
		})									
		return false;									 
	});
		
	$("#cancel_inherentrisk").click(function(){
		history.back();
		return false;								   
	});
});