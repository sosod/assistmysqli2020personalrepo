// JavaScript Document
$(function(){
	
	$.get( "controller.php?action=getGlossary", function( glossaryData ){
		$.each( glossaryData , function( index, glossary){
			$("#glossary_table")
			.append($("<tr />")
				.append($("<td />",{html:glossary.category}))
				.append($("<td />",{html:glossary.terminology}))
				.append($("<td />",{html:glossary.explanation}))
				//.append($("<td />")
					//.append($("<input />",{type:"submit", id:"edit_"+glossary.id, name:"edit_"+glossary.id, value:"Edit"}))		  
				//)
			)
			
			$("#edit_"+glossary.id).live("click", function() {
				document.location.href = "edit.php?id="+glossary.id+"&"; 		
				return false;
			});			
		});			
	
		//$("#glossary_table")
/*		.append($("<tr />")
			.append($("<td />",{colspan:"4", align:"left"})
				.append($("<input />",{type:"submit", value:"New Term", id:"new_term", name:"new_term"}))		  
			)		  
		)*/
	},"json")
	
	$("#new_term").live("click",function(){	
		$("#new_term_table").fadeIn();
		return false;							  
	});
	
	$("#save_glossary").live("click", function(){	
		var message  	= $("#glossary_message");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			jsDisplayResult("error", "error", "Please select category. . . ");			
			return false;
		} else if ( terminology == "" ) {
			jsDisplayResult("error", "error", "Please enter the terminology . . . ");			
			return false;
		} else if ( explanation == "" ){
			jsDisplayResult("error", "error", "Please enter the explanation . . . ");
			return false;
		} else  {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?action=saveGlossary",
			  {
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData == 1 ) {
					message.html("Glossary term saved");
				} else {
					message.html("Error saving the glossary term ");
				}
			});
		 }
				return false;
	});

	$("#cancel_glossary").live("click", function(){
		$("#new_term_table").fadeOut();										 
		return false;										 
	});
});