// JavaScript Document
$(function(){ 
	//set the with of the select to be almost uniform	   
	$("table#risk_new").find("select").css({"width":"200px"})	   
	//set the table th text to align to the left
	$("table#risk_new").find("th").css({"text-align":"left"})

	$(".more_details").hide();

	$("#more_detail").click(function(){			 
		$(".more_details").toggle();
		if($(this).val() == "Less Detail"){
			$(this).val("More Detail");
		} else {
			$(this).val("Less Detail");
		}
		return false;
	});

	//===============================================================================================================
	/**
		get all the risk categories
	**/									
		$.get( "controller.php/?action=getRiskLevel", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#risk_level")
				.append($("<option />",{text:val.name, value:val.id, selected:((val.status & 4) == 4 ?  "selected" : "")}));
			})											 
		},"json");	
	//================================================================================================================
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			$("#financial_exposure")
			.append($("<option />",{text:val.name, value:val.id,  selected:((val.status & 4) == 4 ?  "selected" : "")}));
		})			
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getQueryType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#query_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#query_type").live("change", function() {
		$("#query_category").html("");
		$("#query_category").append($("<option />",{value:"", text:"--query category--"}))											
		$.get( "controller.php/?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#query_category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});

	//================================================================================================================	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#query_user_responsible")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
			
			$("#action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})											 
	},"json");	
	//================================================================================================================	
	$.get("controller.php?action=getDirectorate", function( direData ){
	$.each( direData ,function( index, directorate ) {
			$("#directorate")
			.append($("<option />",{text:directorate.dirtxt	, value:directorate.subid}));
		})
	}, "json");
	//================================================================================================================
	
	$("#add_another_recommendation").click( function(){
		$(this).parent().prepend($("<p />")
		  .append($("<textarea />",{name:"reco"}).addClass("recommend"))		
		)
		return false;									  
	});	
	
	$("#add_another_clientresponse").click( function(){
		$(this).parent().prepend($("<p />")
		  .append($("<textare />",{name:"client"}).addClass("clientresponse"))		
		)
		return false;									  
	});	
	
	$("#add_another_finding").click( function(){
		$(this).parent().prepend($("<p />")
		  .append($("<textare />",{name:"find"}).addClass("find"))		
		)
		return false;									  
	});		
	//================================================================================================================
	/**
		Add New query
	**/
	$("#add_query").click(function() {

		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#query_type :selected").val()
        var r_risk_type 		    = $("#risk_type :selected").val()
		var r_category 				= $("#query_category :selected").val()
		var r_description 			= $("#query_description").val()
		var r_background			= $("#query_background").val();	
		var r_financial_exposure    = $("#financial_exposure").val();
		var r_monetary_implication	= $("#monetary_implication").val();
		var r_detail 				= $("#risk_detail").val();
		var r_risk_level		 	= $("#risk_level :selected").val();
		var r_query_date		 	= $("#query_date").val();
		//var r_recommendation		= $("#recommendation").val();
		//var r_client_response		= $("#client_response").val();
		var r_query_reference 		= $("#query_reference").val();
		var r_directorate 			= $("#directorate :selected").val();
		//var r_subdirectorate 		= $("#subdirectorate").val()

		var r_responsiblity = "";
		$(".responsibility").each( function( index, val){
			if( $(this).val() == "" ){
				message.html("Please select the one responsible ");
				return false;
			} else {
				r_responsiblity  = $(this).val();	
			}									
		});
		var r_attachements = "";
		$(".find").each( function( index, val){
			if( index > 0 ) {
				r_finding += ","+$(this).val();
			}else{
				r_finding = $(this).val();	
			}
		});

		$(".recommend").each( function( index, val){
			if( index > 0 ) {
				r_recommendation += ","+$(this).val();
			}else{
				r_recommendation = $(this).val();	
			}
		});

		$(".clientresponse").each( function( index, val){
			if( index > 0 ) {
				r_client_response += ","+$(this).val();
			}else{
				r_client_response = $(this).val();	
			}
		});		

		if( r_type == "" ) {
			jsDisplayResult("error", "error", "Please select the type for this query");
			return false;
		} else if( r_category == "" ) {
			jsDisplayResult("error", "error", "Please select the risk category for this query");			
			return false;
		} else if( r_query_reference == "" ) {
			jsDisplayResult("error", "error", "Please enter the query reference for this query");			
			return false;
		} else if( r_description == "" ) {
			jsDisplayResult("error", "error", "Please enter the description for this query");			
			return false;
		} else if( r_background == "" ) {
			jsDisplayResult("error", "error", "Please enter the background for this query");			
			return false;
		} else if( r_query_date == "" ) {
			jsDisplayResult("error", "error", "Please enter the query date for  this query");			
			return false;r_risk_level
		} else {
			jsDisplayResult("info", "info", "Saving Query . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=saveQuery",
			  {
				type 			 	  : r_type ,
				type_text			  : $("#query_type :selected").text(),
				category		 	  : r_category ,
				category_text		  : $("#query_category :selected").text(),				
				description 	 	  : r_description ,
				risk_level 	 	  	  : r_risk_level ,
                risk_type             : r_risk_type,
				query_date 	 	  	  : r_query_date ,
			    background		 	  : r_background ,				
				financial_exposure	  : r_financial_exposure ,	
				monetary_implication  : r_monetary_implication ,
				risk_detail		  	  : r_detail,				
				finding	  			  : r_finding , 
				recommendation		  : r_recommendation,					
				query_reference 	  :	r_query_reference,
				user_responsible 	  : r_directorate ,
				client_response	  	  : r_client_response,
				user_responsible_text : $("#directorate :selected").text(),					
				attachment			  : r_attachements
			  } ,
			  function( response ) {				  
				 if( response.saved  ) {
					
					$("#riskid").val( response.id )
					$("#idrisk").html( response.id )					
					$("select#query_type option[value='']").attr("selected", "selected");
                     $("select#risk_type option[value='']").attr("selected", "selected");
					$("select#query_category option[value='']").attr("selected", "selected");					
					$("select#directorate option[value='']").attr("selected", "selected");					
					$("#query_description").val("");
					$("select#risk_level option[value='']").attr("selected", "selected");					
					$("#query_date").val("");					
					$("#query_background").val("");
					$("#financial_exposure").val("");
					$("#monetary_implication").val("");
					$("#risk_detail").val("");
					$("#finding").val("");
					$("#recommendation").val("");
					$("#client_response").val("");
					$("#query_reference").val("");	
					$(".controls").val("");
					
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else {
						jsDisplayResult("ok", "ok", response.text);						
					}
				} else {
					jsDisplayResult("error", "error", response.text);				}
			  } ,
			 "json");
		}				  
		return false;						  
	});
	//================================================================================================================		
	/**
		Show form to add action for this risk 
	**/
	$("#show_add_action").click(function(){
		if( $("#riskid").val() == "") {
			$("#risk_message").html("There was no risk enter associated with this action")	
			return false;			
		} else {
			document.location.href = "action.php";
		}
		//$("#actions_table").fadeIn();	
		return false;
	});

});

	function ajaxFileUpload()
	{
		$("#loading")
		.ajaxStart(function(){
			$(this).show();
		})
		.ajaxComplete(function(){
			$(this).hide();
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=saveAttachment',
				secureuri	  : false,
				fileElementId : 'file_upload',
				dataType	  : 'json',
				success       : function (data, status)
				{
					$("#loading").html( data )
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(" Eroor fron respinse => "+data.error);
						}else
						{
							alert(data.msg);
						}
					}
				},
				error: function (data, status, e)
				{
					alert("Ajax error => "+e);
				}
			}
		)
		return false;

	}