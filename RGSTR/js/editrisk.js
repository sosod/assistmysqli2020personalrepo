// JavaScript Document
// Edit riks
$(function(){
	/**
		Get all the risk categories
	**/
	$("#risk_type").live("change", function(){								
		$("#risk_category").html("");
		var v = $(this).val();
		if(v.length>0) {
			$("#risk_category").append($("<option />",{value:"", text:"--- SELECT ---"}))
			$.get( "controller.php/?action=getTypeCategory", { id : $(this).val() }, function( data ) {
				$.each( data ,function( index, val ) {
					$("#risk_category")
					.append($("<option />",{text:val.name, value:val.id}));
				})											 
			},"json");	
		} else {
			var t = AssistString.substr($("#th_type").html(),0,-1).toUpperCase();
			$("#risk_category").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}))											
		}
		return false;
	});
	//================================================================================================================
	/**
		Get the risk impact ratings
	**/
	$("#risk_impact").live( "change" ,function(){
		$("#risk_impact_rating").html("");
		var v = $(this).val();
		if(v.length>0) {
			$("#risk_impact_rating").append($("<option />",{value:"", text:"--- SELECT ---"}))
			$.get( "../common/common_controller.php?action=getImpactRating", { id:v }, function( data ) {
				if ( data.diff == 0 ) {
					$("#risk_impact_rating")
						.append($("<option />",{value:data.to, text:data.to,selected:"selected" }))
						.trigger("change");
				} else {
					var f = data.from*1;
					var t = data.to*1;
					for( i = f; i <= t; i++ ) {
						$("#risk_impact_rating")
						.append($("<option />",{value:i, text: i}))	
					}
				}
				$("#risk_impact_color").css("background-color","#"+data.color);
			}, "json");
		} else {
			var t = AssistString.substr($("#th_impact").html(),0,-1).toUpperCase();
			$("#risk_category").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}))											
		}
		return false;
	});	
	$("#risk_impact_rating").live("change",function() {
		var i = $(this).val();
		var lk = $("#risk_likelihood_rating").val();
		if(i.length>0 && lk.length>0) {
			var ire = i * lk;
			$("#txt_ire").val(ire);
			$.get("../common/common_controller.php?action=getIR",{rating:ire},function(data) {
				$("#lbl_ire").css("background-color","#"+data[1]).html(data[0]+" ("+ire+")");
			},"json");
		} else {
			//alert("An error has occurred.  Please reload the page and try again.");
		}
		$("#risk_percieved_control").trigger("change");
	});
	//================================================================================================================	
	/**
		Get the risk likelihood ratings
	**/
	$("#risk_likelihood").live( "change" ,function(){
		$("#risk_likelihood_rating option").remove(); //html("");
		var v = $(this).val();
		if(v.length>0) {
			$("#risk_likelihood_rating").append($("<option />",{value:"", text:"--- SELECT ---"}))
			$.get( "../common/common_controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
				if ( data.diff == 0 ) {
					$("#risk_likelihood_rating")
						.append($("<option />",{value:data.to, text:data.to,selected:"selected" }))
						.trigger("change");
				} else {
					var f = data.from*1;
					var t = data.to*1;
					for( i = f; i <= t; i++ ) {
						$("#risk_likelihood_rating")
						.append($("<option />",{value:i, text:i}))	
					}
				}
				$("#risk_likelihood_color").css("background-color","#"+data.color);
			}, "json");
		} else {
			var t = AssistString.substr($("#th_likelihood").html(),0,-1).toUpperCase();
			$("#risk_category").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}))											
		}
		return false;
	});	
	$("#risk_likelihood_rating").live("change",function() {
		$("#risk_impact_rating").trigger("change");
	});
	//================================================================================================================	
	/**
		Get the risk control effectiveness ratings
	**/
	$("#risk_percieved_control").live( "change" ,function(){
		//$("#risk_control_effectiveness_rating").html("")				
		$x = $("#risk_percieved_control option:selected");
		var v = $(this).val(); 
		if(v.length>0) {
			var r = $x.attr("rating");
			var ir = $("#txt_ire").val(); //alert(ir);
			var rr = (Math.round( (r * ir) *100))/100;
			//$("#lbl_rre").html("XXXXX ("+rr+")");
			//$("#div_risk_percieved_control_color").css("background-color","#"+$x.attr("rating_color"));
			$("#lbl_risk_control_effectiveness").html(r);
			$("#risk_percieved_control_color").css("background-color","#"+$x.attr("rating_color"));
			$("#risk_control_effectiveness").val(r);
			$.get("../common/common_controller.php?action=getRR",{rating:rr},function(data) {
				$("#lbl_rre").css("background-color","#"+data[1]).html(data[0]+" ("+rr.toFixed(2)+")");
			},"json");
			
		/*	$("#risk_control_effectiveness_rating").append($("<option />",{text:"--- SELECT ---"}))
			$.get( "controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
					$("#risk_control_effectiveness_rating")
						.append($("<option />",{value:data.rating, text:data.rating }))
			}, "json");*/
		//} else {
			//var t = AssistString.substr($("#th_risk_percieved_control").html(),0,-1).toUpperCase();
			//$("#risk_category").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}))											
		}
		return false;
	});	
	//================================================================================================================
	/*$("#add_another_control").click( function() {
		var id 	   = parseInt(Math.round(Math.random()*44)) + parseInt(Math.round(Math.random()*45));
		var spanId = "currentcontrols_"+id;
		$(this).parent().children("p").append("<br />").append($("<textarea />",{name:"currentcontrols_"+id, id:"currentcontrols_"+id, cols:"50", rows:"3",style:"margin-top:5px"}).addClass("controls"))
		  //.append($("<span />",{id:"counter_"+spanId, html:"200 characters allowed"}))
		//)
		//$(this).parent().prepprepend($("<br />")).prepend($("<textarea />",{name:"currentcontrols_"+id, id:"currentcontrols_"+id, cols:"30", rows:"5"}).addClass("controls").addClass("textcounter").textcounter(10).append($("<br />")))
		return false;									  
	});*/
	$("#current_controls_add").click(function() {
		$("p.p_controls").last().after("<p class=p_controls><textarea name=risk_currentcontrols id=risk_currentcontrols class=controls rows=3 cols=50></textarea></p>");
	});
	//================================================================================================================
	/**
		Add New Risk
	**/
	$(".remove_attach").click(function(){
		var id = $(this).attr("id");
		var filename = $(this).attr("title");
		$.post("controller.php?action=removeFile",{ 
			   timeid		: id,
			   originalname : filename 
	    },function( response ){
		  $(".messages").remove();
		   if( response.error )	{
			   $("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")		   
		  } else {
		    $("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
			$("#parent_"+id).fadeOut().remove();
			//$("#risk_message").slideDown().html( response.text )
		 }												  
		},'json');
		return false;							   
	});
	
	$("#edit_risk").click(function(){
		var err = false;
		$("input:text, textarea, select").each(function() {
			$(this).removeClass("required");
		});
		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#risk_type :selected").val();
		var r_level					= $("#risk_level :selected").val()		
		var r_financial_exposure	= $("#financial_exposure :selected").val()				
		var r_actual_financial_exposure	= $("#actual_financial_exposure").val()
		var r_kpi_ref				= $("#kpi_ref").val()			
		var r_category 				= $("#risk_category :selected").val()
		var r_description 			= $("#risk_description").val()
		var r_background			= $("#risk_background").val();	
		var r_impact 				= $("#risk_impact :selected").val();
		var r_impact_rating			= $("#risk_impact_rating").val();
		var r_likelihood 			= $("#risk_likelihood :selected").val();
		var r_likelihood_rating 	= $("#risk_likelihood_rating").val();
		var r_status				= $("#risk_status :selected").val();
		var r_percieved_control		= $("#risk_percieved_control :selected").val();
		var r_control_effectiveness_rating = $("#risk_control_effectiveness_rating :selected").val();
		var r_user_responsible 		= $("#risk_user_responsible").val();
		var r_attachements 			= [];
		$(".attach").each( function( index, val){
			r_attachements.push($(this).val());
		});
		
		$(".controls").each( function( index, val){
			if( index > 0 ) {
				r_currentcontrols += "_"+$(this).val();
			}else{
				r_currentcontrols = $(this).val();	
			}
		});
		if( r_type == "" ) {
			err = true;
			$("#risk_type").addClass("required");
		}  
		if( r_level == "" ) {
			$("#risk_level").addClass("required");
			err = true;
		}  
		if( r_category == "" ) {
			$("#risk_category").addClass("required");
			err = true;
		}  
		if( r_description == "" ) {
			$("#risk_description").addClass("required");
			err = true;
		}  
		if( r_background == "" ) {
			$("#risk_background").addClass("required");
			err = true;
		} 
		if( r_impact == "" ) {
			$("#risk_impact").addClass("required");
			err = true;
		} 
		if( r_likelihood == "" ) {
			$("#risk_likelihood").addClass("required");
			err = true;
		} 
		if( r_currentcontrols == "" ) {
			$(".controls").addClass("required");
			err = true;
		} 
		if( r_percieved_control == "" ) {
			$("#risk_percieved_control").addClass("required");
			err = true;
		}
		if(r_actual_financial_exposure != ""  && isNaN(r_actual_financial_exposure)){
			$("#actual_financial_exposure").addClass("required");
			err = true;
		}
		if( r_user_responsible == "" ) {
			$("#risk_user_responsible").addClass("required");
			err = true;
		}
		if( r_status == "" ){
			$("#risk_status").addClass("required");
			err = true;
		}
		if(err) {
			jsDisplayResult("error", "error", "Please complete the missing information as highlighted.");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Updating "+window.risk_object_name+" . . .  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=editRisk",
			  {	id 					  : $("#riskid").val(),
				type 			 	  : r_type ,
				level				  	 : r_level,					
				financial_exposure    	 : r_financial_exposure,
				actual_financial_exposure:	r_actual_financial_exposure,
				kpi_ref		  	  	  : r_kpi_ref,					
				category		 	  : r_category ,
				status 			 	  : r_status ,		
				description 	 	  : r_description ,
			    background		 	  : r_background ,
		 		impact			 	  : r_impact ,
				impact_rating	 	  : r_impact_rating ,	
				likelihood 		 	  : r_likelihood ,
				likelihood_rating	  : r_likelihood_rating , 
				current_controls 	  : r_currentcontrols ,
				effectiveness	 	  : r_percieved_control ,	
				control_effectiveness_rating :	r_control_effectiveness_rating ,
				user_responsible 	  : r_user_responsible ,
				attachment			  : r_attachements,
                financial_year_id         : $("#financial_year_id :selected").val(),
                rbap_ref                  : $("#rbap_ref").val(),
                cause_of_risk             : $("#cause_of_risk").val(),
                reasoning_for_mitigation  : $("#reasoning_for_mitigation").val()
			  } ,
			  function( response ) {
				  if( response.hasOwnProperty('updated '))
                  {
					  if( response.error ){
						  jsDisplayResult("error", "error", response.text );  
					  } else {
						  jsDisplayResult("ok", "ok", response.text );
					  }  					  
				  } else {
                      if( response.error ){
                          jsDisplayResult("error", "error", response.text );
                      } else {
                          jsDisplayResult("ok", "ok", response.text );
                      }
				  }
				  /*	message.html("");
					if( ! $.isEmptyObject( retData.success ) ) {
						$.each( retData.success , function( index, val ) {
							message.addClass("ui-state-highlight").append( val+"<br />" );
						});
						if(!$.isEmptyObject(retData.error)){
							$.each( retData.error , function( i, valu ) {
								message.addClass("ui-state-error").append( valu+"<br />" );
							});
						}
					} else {
						$.each( retData.error , function( i, valu ) {
							message.addClass("ui-state-error").append( valu+"<br />" );
						});
					}*/
			  } ,"json");
		}				  
		return false;						  
	});
	//================================================================================================================
	$("#save_update").click(function(){
		var err = false;
		
		$("input:text, textarea, select").each(function() {
			$(this).removeClass("required");
		});
	
		var message 				= $("#risk_message").slideDown("fast");
		var r_impact 				= $("#risk_impact :selected").val();
		var r_impact_rating			= $("#risk_impact_rating").val();
		var r_likelihood 			= $("#risk_likelihood :selected").val();
		var r_likelihood_rating 	= $("#risk_likelihood_rating").val();
		var r_percieved_control		= $("#risk_percieved_control :selected").val();
		var r_control_effectiveness = $("#risk_control_effectiveness ").val();
		var r_user_responsible 		= $("#userrespoid").val();
		var r_response		 		= $("#risk_response").val();	
		var r_status		 		= $("#risk_status :selected").val();
		var r_attachements 			= [];
		$(".attach").each( function( index, val){
			r_attachements.push($(this).val());
		});
		var r_currentcontrols = "";
		$(".controls").each( function( index, val){
			if($(this).val().length>0) {
				if( r_currentcontrols.length > 0 ) {
					r_currentcontrols += "_";
				}
				if($(this).val().length>0) {
					r_currentcontrols += $(this).val();	
				}
			}
		});

		if( r_impact == "" ) {
			err = true;
			$("#risk_impact").addClass("required");
		}
		if( r_impact_rating == "" ) {
			err = true;
			$("#risk_impact_rating").addClass("required");
		}
		if( r_likelihood == "" ) {
			err = true;
			$("#risk_likelihood").addClass("required");
		}
		if( r_likelihood_rating == "" ) {
			err = true;
			$("#risk_likelihood_rating").addClass("required");
		}
		if( r_currentcontrols == "" ) {
			err = true;
			$(".controls").addClass("required");
		} 
		if( r_percieved_control == "" ) {
			err = true;
			$("#risk_percieved_control").addClass("required");
		} 
		if(r_control_effectiveness=="") {
			err = true;
			$("#risk_control_effectiveness").addClass("required");
		}
		if(err) {
			jsDisplayResult("error", "error", "Please complete the missing information as highlighted.");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating "+window.risk_object_name+" ...  <img src='../images/loaderA32.gif' />");
			$.post( "../common/common_controller.php?action=newRiskUpdate",
			  {
				id					  : $("#riskid").val(),
				response 			  : r_response,
			    status		 	  	  : r_status ,
		 		impact			 	  : r_impact ,
				impact_rating	 	  : r_impact_rating ,	
				likelihood 		 	  : r_likelihood ,
				likelihood_rating	  : r_likelihood_rating , 
				current_controls	  : r_currentcontrols ,
				effectiveness		  : r_percieved_control ,	
				control_effectiveness_rating :	r_control_effectiveness ,
				attachment			  : r_attachements

			  } , function(  response  ) {
				 if( response.update ){
					 if( response.error ){
						 jsDisplayResult("error", "error", response.text );
					 } else {
						 //jsDisplayResult("ok", "ok", response.text);
						 //alert("ok");
						 document.location.href = $("#redirect").val()+'?r[]=ok&r[]='+response.text;
					 }
				 } else {
					 jsDisplayResult("error", "error", response.text);
				 }
			  } ,"json");
		}		
		return false;
	});
	
	$("#delete_risk").click(function(){
		var message 	= $("#risk_message").slideDown();	
		var userRespo 	=  $("#risk_user_responsible").val()
		if( confirm( "Are you sure you want to delete this "+window.risk_object_name+"?") ) {
			jsDisplayResult("info", "info", "Deleting "+window.risk_object_name+" ...  <img src='../images/loaderA32.gif' />");
			//message.html( "Deleting risk ...  <img src='../images/loaderA32.gif' />" )			
			$.post("controller.php?action=deleteRisk" ,{ 
					id 			: $("#riskid").val(),
					userRespo	: $("#risk_user_responsible :selected").val()
					}	 ,
				function( response  ) {
					if( response.deleted )
					{
						if( response.error )
						{
							jsDisplayResult("error", "error", response.text );
						} else {
							jsDisplayResult("ok", "ok", response.text );
						}
							$("select#risk_type option[value='']").attr("selected", "selected");
							$("select#risk_impact option[value='']").attr("selected", "selected");
							$("select#risk_impact_rating option[value='']").attr("selected", "selected");					
							$("select#risk_category option[value='']").attr("selected", "selected");
							$("select#risk_likelihood option[value='']").attr("selected", "selected");
							$("select#risk_likelihood_rating option[value='']").attr("selected", "selected");											
							$("select#risk_percieved_control option[value='']").attr("selected", "selected");
							$("select#risk_control_effectiveness option[value='']").attr("selected", "selected");						
							$("select#risk_user_responsible option[value='']").attr("selected", "selected");
							$("select#risk_status option[value='']").attr("selected", "selected");						
							$("#risk_description").val("");
							$("#risk_background").val("");
							$(".controls").val("");						
					} else {
						jsDisplayResult("error", "error", response.text );
					}
				},"json")
		}
		return false;							 
	});
	
	$("#cancel_update").click(function(){
		history.back();							   
		return false;
	});
});
/*
function ajaxFileUpload( fileupload )
	{

		$("#loading")
		.ajaxStart(function(){
			$("#file_loading").html("Uploading . . . <img src='../images/loaderA32.gif' />");
		})
		.ajaxComplete(function(){
			//$(this).html("");
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=saveAttachment',
				secureuri	  : false,
				fileElementId : 'editfile_upload',
				dataType	  : 'json',
				success       : function (response, status)
				{	
					$("#file_loading").html( response )
					if(!response.error)
					{
						$("#file_loading").html("<a hre='#'>"+response.filename+" successfully uploaded <br /></span>" )
									 .css({"color":"green"});
						if( $.isEmptyObject(response.filesuploaded) ){
							$("#file_loading").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#file_loading").append( file+"<br />" )											 
							});							
						}
												
					}else
					{
						$("#file_loading").html(" Error uploading  "+response.error);
					}
				},
				error: function (data, status, e)
				{
					$("#file_loading").html(" Ajax error "+e);
				}
			}
		)
		return false;
	}

*/
 	function ajaxFileUpload( fileupload )
	{
        var panelId  = $(fileupload).attr("id");
		$("#loading")
		.ajaxStart(function(){
			$("#file_loading").html("Uploading . . . <img src='../images/bar180.gif' />");
		})
		.ajaxComplete(function(){
			//$(this).html("");
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=saveAttachment',
				secureuri	  : false,
				fileElementId : panelId,
				dataType	  : 'json',
				success       : function (response, status)
				{
					$(".newuploads").remove();
					$(".messages").remove();
					if(!response.error)
					{
						if( $("#attachment_list").length == 0){
							$("#file_loading").append($("<ul />",{id:"attachment_list"}))
						}				
						$("#attachment_list").prepend("<span class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></span>")
						//$("#file_loading").html("<a hre=''>"+response.filename+" successfully uploaded <br /></span>")
							//		 .css({"color":"blue"})
						if( $.isEmptyObject(response.filesuploaded) ){
							//$("#file_loading").append( "" )
						} else { 
							//$("#file_loading").append($("<ul />",{id:"files"}))
							$.each( response.filesuploaded, function(index, file){
								$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"'  title='"+file+"' class='remove'><span></span>Remove</a>" )
								//$("#files").append("<li id='li_"+index+"'>"+file+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"' class='remove'><span></span>Remove</a></ul>")
								//.append($("<br />"))
							});							
						}
													
						/*
						$("#file_loading").html("<a hre='#'>"+response.filename+" successfully uploaded <br /></span>" )
									 .css({"color":"blue"});
						if( $.isEmptyObject(response.filesuploaded) ){
							$("#file_loading").append( "" )
						} else{
							if( $("#attachments").length == 0){
								$("#file_loading").append($("<ul />",{id:"attachments"}))
							} 
							$.each( response.filesuploaded, function(index, file){
								$("#attachments").append("<li id='li_"+index+"'><a href='#'>"+file+"</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href='#' class='remove removeattach'><span></span>Remove</a></li>")
							});
						}
						*/
					} else
					{
						$("#file_loading").html(" Error uploading  "+response.error);
					}
				},
				error: function (data, status, e)
				{
					$("#file_loading").html(" Ajax error "+e);
				}
			}
		)
		return false;
	}