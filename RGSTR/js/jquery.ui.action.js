$.widget("ui.action", {
	options		: {
		page	  		: "",
		limit	  		: 10,
		start	  		: 0,
		current   		: 1,
		edit	  		: false,
		update    		: false,
		assurance 		: false,
		actioidiv 		: "#actions",
		risk_id  		: 0,
		total 	  		: 0 , 
		columnDisplay   : "table-cell",
		buttonEdit 		: "none",
		buttonUpdate 	: "none",
		extras			: true, 
		loggedInUser	: 0,
		section			: "",
		actionOwners	: [],
		actionStatus	: [],
		statistics		: [],
		objectName		: "Risk",
		actionName		: "Action"
	} , 
	
	_init			: function(){
	//console.log("init");
		this._getAction();
		
	} , 
	
	_create			: function(){		
	} ,
	 
	_getAction		: function(){
//alert("getAction");
		$this = this;
		//$this._loader();			//alert($this.options.risk_id);		 
		//console.log("				start 	: "+$this.options.start);
		//console.log("				limit 	: "+$this.options.limit);
		//console.log("				id		: "+$this.options.risk_id);
		//console.log("				section : "+$this.options.section);
		//console.log("json");
		$.getJSON("controller.php?action=getAddActions", {
				start 	: $this.options.start,
				limit 	: $this.options.limit,
				id		: $this.options.risk_id,
				section : $this.options.section
			}, function( actions ){
			//console.log("actions");
                //console.log(  actions );
				//$("#loading").html();
//alert("actions");
				if( $.isEmptyObject(actions.data)){
					//$this._displayPager( 1 ,actions.countRaw );
					//$this._displayHeaders( actions.rawHeaders );
					$("#actions").append($("<p />",{html:"There are no "+$this.options.actionName+"s for this "+$this.options.objectName+"."}))	
				} else { 
//alert("not empty");
					if(actions.hasOwnProperty('owners'))
					{
						$this.options.actionOwners = actions.owners;
					}
					$("#actions").html("").append($("<table />",{align:"left", id:"table_actions"}))
				$this.options.statistics   = actions.statistics;
				$this.options.actionStatus = actions.status;
					$this.options.total =  actions.statistics.total
					$this._displayPager( actions.statistics.total ,actions.columns );	//alert("paged");
					$this._displayActions( actions.data, actions.headers, actions.columns );
					$this._displayExtras();
				}
			});
		
	} , 
	
	_displayPager	: function( total , columns){
		var $this 	= this;	
		var pages;
		if( total%$this.options.limit > 0){
			pages = Math.ceil( total/$this.options.limit )
		} else {
			pages = Math.floor( total/$this.options.limit )
		}

		$("#table_actions")
		 .append($("<tr />")
			.append($("<td />",{colspan:columns})
			   .append($("<input />",{type:"button", id:"first", name:"first", value:" |< "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"previous", name:"previous", value:" < "}))
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{align:"center", html:"Page "+$this.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages)}))	   			
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", id:"next", name:"next", value:" > "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"last", name:"last", value:" >| "}))		 			   
			 )	   
			.append($("<td />").css({"display" : ($this.options.edit ? "table-cell" : "none")}))
			.append($("<td />").css({"display" : ($this.options.update ? "table-cell" : "none")}))	
			.append($("<td />").css({"display" : ($this.options.assurance ? "table-cell" : "none")}))	   			
		  )
		       if($this.options.current < 2)
		       {
                     $("#first").attr('disabled', 'disabled');
                     $("#previous").attr('disabled', 'disabled');		     
		       }
		       if(($this.options.current == pages || pages == 0))
		       {
                     $("#next").attr('disabled', 'disabled');
                     $("#last").attr('disabled', 'disabled');		     
		       }	
			$("#next").bind("click", function(){
				$this._getNext( $this );
			});
			$("#last").bind("click",  function(){
				$this._getLast( $this );
			});
			$("#previous").bind("click",  function(){
				$this._getPrevious( $this );
			});
			$("#first").bind("click",  function(){
				$this._getFirst( $this );
			});			 
		} , 
		
		_getNext  			: function( $this ) {
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getAction();
		},	
		
		_getLast  			: function( $this ) {

			$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
			$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
			this._getAction();
		},
		
		_getPrevious    	: function( $this ) {
			$this.options.current   = parseFloat( $this.options.current ) - 1;
			$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
			this._getAction();			
		},
		
		_getFirst  			: function( $this ) {
			$this.options.current   = 1;
			$this.options.start 	= 0;
			this._getAction();				
		},
		
	_displayActions		: function( actions, headers,columns ){	
			
		var $this = this;	
			
		var hr = $("<tr />");	
		$.each( headers, function( index, head){
			hr.append($("<th />",{html:head}))			
		});
		hr.append($("<th />").css({"display" : ($this.options.edit ? "table-cell" : "none")}))
		hr.append($("<th />").css({"display" : ($this.options.update ? "table-cell" : "none")}))	
		hr.append($("<th />").css({"display" : ($this.options.assurance ? "table-cell" : "none")}))	   			
		$("#table_actions").append( hr );

		
		$.each( actions, function( index, action){
			var tr = $("<tr />", {id:"tr_"+index});
			$.each( action, function( key, val){
				tr.append($("<td />",{html:val}))				
			});
			var textValue = "";
			var disabled  = false;
			//if its the owner logged in
			if($this.options.actionOwners[index] == "No"){
				textValue = "Update";
				disabled  = true;	
			}
						
			//awaiting approval
			if( (($this.options.actionStatus[index] & 8) == 8) && (($this.options.actionStatus[index] & 1) == 1))
			{
				textValue = " Awaiting \r\napproval . . ";
				disabled  = true;
			}
			// action approved
			if( ($this.options.actionStatus[index] & 4) == 4 )
			{
				textValue = "Locked . . ";
				disabled  = true;								
			}
			
			tr.append($("<td />").css({"display" : ($this.options.edit ? "table-cell" : "none")})
			   .append($("<input />",{
				   					  type    : "button",
				   					  name	: "edit_"+index,
				   					  id		: (disabled  ? "edit__"+index: "edit_"+index ),
				   					  value	:"Edit"
				   				}))
			   .append($("<input />",{	
				   					  type		:  "button",
				   					  name		:  "del_"+index,
				   					  id		: (disabled ? "del__"+index: "del_"+index ),
				   					  value		: "Del"
				   }))
			)
			
			tr.append($("<td />").css({"display" : ($this.options.update ? "table-cell" : "none")})
			   .append($("<input />",{
				   					  type		: "button",
				   					  name		: "update_"+index,
				   					  id		     : (disabled ? "update__"+index: "update_"+index ),
				   					  value		: (disabled ? textValue : "Update")}))		
			)			

			tr.append($("<td />").css({"display" : ($this.options.assurance ? "table-cell" : "none")})
			   .append($("<input />",{type:"button", name:"assurance_"+index, id:"assurance_"+index, value:"Assurance"}))		
			)			
				
			$("#edit_"+index).live("click", function(){
				document.location.href = "edit_action.php?id="+index;
				return false;
			});

			$("#assurance_"+index).live("click", function(){
				document.location.href = "assurance_action.php?id="+index;
				return false;
			});
			
			$("#update_"+index).live("click", function(){
				document.location.href = "update_action.php?id="+index;
				return false;
			});			
			
			$("#del_"+index).live("click", function(){
				if(confirm("Are you sure you want to delete this "+self.options.actionName+"?"))
				{
					jsDisplayResult("info", "info", "Deleting "+self.options.actionName+" . . . <img src='../images/loaderA32.gif' >");
					$.post("controller.php?action=deleteAction", { id : index}, function( response ){
						if( response.error  ) {
							jsDisplayResult("error", "error", response.text );
						} else {
							$("#tr_"+index).fadeOut();
							jsDisplayResult("ok", "ok", response.text);
						}							
					}, "json");
				}
				return false;
			})			
			$("#table_actions").append( tr );
			if(disabled)
			{
			    $("#update__"+index).attr('disabled', 'disabled');
			    $("#edit__"+index).attr('disabled', 'disabled');
			    $("#del__"+index).attr('disabled', 'disabled');
			}			
		});
		$("#table_actions").append($("<tr />",{id:"tr_overalprogress"}).css({"background-color":"#FFF8FE"})
			.append($("<td />",{colspan: (columns-1), html:"<b>Overall "+this.options.actionName+" Progress</b>"}).css({"text-align" : "right"}))
			.append($("<td />",{html:Math.round($this.options.statistics.averageProgress, 3)+"<em>%</em>"}).css({"text-align" : "right"}))
		    .append($("<td />").css({"display" : ($this.options.edit ? "table-cell" : "none")}))
		    .append($("<td />").css({"display" : ($this.options.update ? "table-cell" : "none")}))	
		    .append($("<td />").css({"display" : ($this.options.assurance ? "table-cell" : "none")}))
		);
		
	} ,
	
	_displayExtras		:function(){
		$this = this;
		/*$("#table_actions")
		  .append($("<tr />",{id:"extras"})
			 .css({"visibility":($this.options.extras == true ? "visible" : "hidden")})		
			 .append($("<td />", {align:"right", colspan:"6"})
  				  .append($("<input />",{type:"button", name:"edit_action", id:"add_action", value:"Add Action"})
					.css({"display":$this.options.update == true ? "none" : "inline"} )
					)				   
			  )				
			 .append($("<td />", {align:"right", colspan:"3"}).css({"display":$this.options.columnDisplay}))							
		   )
		 $("#add_action").live("click", function(){
			document.location.href = "add_action.php?id="+$this.options.risk_id;									 
		});*/
	} ,
	
	
	_displayHeaders	: function(headers){
		var $this = this;
		var tr    = $("<tr />")
		$.each( headers, function(index, head){
			tr.append($("<th />",{html:head}))			
		});
		tr.append($("<th />").css({"display" : ($this.options.edit ? "table-cell" : "none")}))
		tr.append($("<th />").css({"display" : ($this.options.update ? "table-cell" : "none")}))	
		tr.append($("<th />").css({"display" : ($this.options.assurance ? "table-cell" : "none")}))	   					
		$("#table_actions").append(tr);	
	} , 
	_loader			: function(){
		$this 		  = this;
		var actiondiv = $("<div />",{id:"loading"});
		actiondiv.html("Loading actions . .  . <img src='../images/loaderA32.gif' />")
					.css({
						position  	: "absolute", 
						top 	  	: "200px;",
						left	  	: "250px",
						"z-index" 	: "999"
					})
		
		
	}
	
	
})
