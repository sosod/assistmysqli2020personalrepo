$.widget("ui.riskaction", {
	
	options		: {
			tableId			: "table_"+(Math.floor(Math.random(56) * 33)),
			url 			: "controller.php?action=getRiskAction",
			total 			: 0,
			start			: 0,
			current			: 1,
			limit			: 10,
			editActions		: false,
			updateActions	: false,
			addAction		: false,
			updateActions	: false,
			assuranceActions: false,
			editRisk		: false,
			updateRisk		: false,
			assuranceRisk	: false,
			viewRisk		: false,
			view			: "",
			section			: "",
			page            : "",
			autoLoad        : true,
			showActions     : false,
			financialYear   : 0,
			riskOwner       : 0,
			riskId          : 0,
		objectName		: "Risk",
		actionName		: "Action"
	} , 
	
	_init		: function()
	{
		if(this.options.autoLoad)
		{
		  this._getRisk();
		}
	
	} , 
	
	_create		: function()
	{
		var self = this;		
		//append($("<form />").append($("<table />",{id : self.options.tableId})))	
		var html = [];
		html.push("<table class='noborder'>");
/*		  if(self.options.section == "admin" || self.options.section == "manage")
		  {
		      html.push("<tr>");
		        html.push("<td class='noborder'>");
		            html.push("<table class='noborder'>");
		              html.push("<tr>");
		                html.push("<th style='text-align:left;'>Financial Year:</th>");
		                html.push("<td class='noborder'>");
		                  html.push("<select id='_financial_year' name='_financial_year' style='width:200px;'>");
		                    html.push("<option value=''>--please select--</option>");
		                  html.push("</select>");
		                html.push("</td>")  
		              html.push("</tr>");
		              html.push("<tr>");
		                html.push("<th style='text-align:left;'>Query Owner:</th>");
		                html.push("<td class='noborder'>");
		                  html.push("<select id='query_owner' name='query_owner' style='width:200px;'>");
		                    html.push("<option value=''>--please select--</option>");
		                  html.push("</select>");
		                html.push("</td>")  
		              html.push("</tr>");
		            html.push("</table>");		      
		        html.push("</td>");
		      html.push("</tr>");
		  }*/
		  html.push("<tr>");
		    html.push("<td class='noborder'>");
		        html.push("<table id='"+self.options.tableId+"'></table>");		      
		    html.push("</td>");
		  html.push("</tr>");
		html.push("</table>");		
	    $(this.element).append(html.join(' '));
	    //self._loadFinancialYears();
	    //self._loadQueryOwners();
	    
/*	    $("#_financial_year").live("change", function(){
	        if($(this).val() !== "")
	        {
	           self.options.financial_year = $(this).val();
	           self._getQuery();
	        } else {
	            self.options.financial_year  = 0;
	            self._getQuery();
	        }
	    });
	    
	    $("#query_owner").live("change", function(){
	        if($(this).val() !== "")
	        {
	           self.options.query_owner = $(this).val();
	           self._getQuery();
	        } else {
	            self.options.query_owner = 0;
	            self._getQuery();
	        }
	    });	  */
	    		

	} , 
	
	_loadFinancialYears     : function()
	{
	    $("#_financial_year").html("");
        $.getJSON("controller.php?action=getFinancialYear",{status:1}, function(financialyears){
          var finHtml = []
          finHtml.push("<option value=''>--please select--</option>");
          $.each(financialyears, function(index, year){
              finHtml.push("<option value='"+year.id+"'>"+year.start_date+" - "+year.end_date+"</option>");
          });  
          $("#_financial_year").html( finHtml.join(' ') );
        }); 
	} ,
	
	_loadQueryOwners              : function()
	{
	   $("#query_owner").empty();
	   $.getJSON("controller.php?action=getDirectorate", function(direData){
	       var ownerHtml = [];
	       ownerHtml.push("<option value=''>--please select--</option>");
		   $.each(direData ,function(index, directorate) {
		       ownerHtml.push("<option value='"+directorate.subid+"'>"+directorate.dirtxt+"</option>");
		   })
		   $("#query_owner").html( ownerHtml.join(' ') );
	   });
	},
	
	_getRisk	: function()
	{ 
	     var self = this;
		 //alert(self.options.objectName+" : "+self.options.actionName);
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
	     ); //alert(self.options.url);
		$.getJSON( self.options.url,{
			start		   : self.options.start,
			limit		   : self.options.limit,
			view		   : self.options.view,
			section		   : self.options.section,
			page           : self.options.page,
			financial_year : self.options.financialYear,
			risk_owner     : self.options.risk_owner,
			risk_id        : self.options.risk_id,
			show_actions	: (self.options.showActions ? 1 : 0)
		},function(riskData){ //alert("process");
		    $("#loadingDiv").remove();
			$("#"+self.options.tableId).html("");
			$(".more_less").remove();

			if(riskData.total != null)
			{
				self.options.total = riskData.total
			}
			self._displayPaging(riskData.columns);
			self._displayHeaders(riskData.headers);
			if($.isEmptyObject(riskData.risks))
			{
			    var _html = [];
			    _html.push("<tr>")
			      _html.push("<td colspan='"+(riskData.columns+1)+"'>");
			         _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
			           _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
			           _html.push("<span style='padding:5px; float:left;'>"+riskData.risk_message+"</span>");
			         _html.push("</p>");
			      _html.push("</td>");
			    _html.push("</tr>");
				$("#"+self.options.tableId).append( _html.join(' ') );
			} else {
				self._display(riskData.risks, riskData.actions, riskData.columns, riskData);
			}			
		});		//alert("get");		
	} , 
	
	_displayHeaders	: function(headers)
	{
		var self = this;
		var html = [];
		html.push("<tr>");
		if(self.options.showActions)
		{
		   html.push("<th>&nbsp;</th>");
		}
		$.each( headers , function( index, head ){
		    html.push("<th>"+head+"</th>");
		});
		if(self.options.assuranceActions)
		{
		  html.push("<th>&nbsp;</th>");
		}
		if(self.options.updateRisk)
		{
		  html.push("<th>&nbsp;</th>");
		}
		if(self.options.editRisk)
		{
		  html.push("<th>&nbsp;</th>");
		}
		if(self.options.addAction)
		{
		  html.push("<th>&nbsp;</th>");
		}
        if(self.options.viewRisk)
        {
          html.push("<th>&nbsp;</th>");
        }  		

		html.push("</tr>");
		$("#"+self.options.tableId).append(html.join(' '));
	} , 
	
	_display		: function(risks, actions, columns, riskData)
	{
	    if($(".more_less").length > 0)
	    {
	        $(".more_less").remove();
	    }
	    if($(".risk_actions").length > 0)
	    {
	        $(".risk_actions").remove();
	    }	    
		var self = this;
		var i    = 1;
		$.each(risks, function(index, risk){
		    i++;
		    var html = [];
		    if(self.options.page == "view" && riskData.hasOwnProperty('canUpdate'))
		    {
               if(riskData.canUpdate.hasOwnProperty(index) && riskData.canUpdate[index] == true)
               {
                  html.push("<tr>");
               } else if(riskData.isDirAdmin.hasOwnProperty(index) && riskData.isDirAdmin[index] == true){
                   html.push("<tr>");
               } else {
                  html.push("<tr>");
               }
		    } else {
		      html.push("<tr>");
		    }
		    if(self.options.showActions)
		    {
		       html.push("<td>");
			     if(risk.totalActions.substr(0,1)!="0") {
					html.push("<a href='#' id='more_less_"+self.options.tableId+"_"+index+"' class='more_less'>");
						html.push("<span class='ui-icon ui-icon-plus'></span>") 
					html.push("</a>");
				}
		       html.push("</td>");
		       
		    }				    
			$.each( risk, function( key , val){
				html.push("<td>"+val+"</td>");
			})
            if(self.options.assuranceActions)
            {
               html.push("<td class=center>");
                html.push("<input type='button' value='"+self.options.objectName+" Assurance' id='assurance_"+index+"' id='assurance_"+index+"' /><br />&nbsp;<br />");
                //html.push("<input type='button' value='"+self.options.actionName+"' id='assurance_actions_"+index+"' id='assurance_actions_"+index+"' />");
              html.push("</td>");
			    $("#assurance_"+index).live("click", function(){
				    document.location.href = "risk_assurance.php?id="+index;
				    return false;
			    });
			
			    $("#assurance_actions_"+index).live("click", function(){
				    document.location.href = "../actions/action_assurance.php?id="+index;								  
				    return false;
			    });	               
            }
            if(self.options.updateRisk)
            {
                html.push("<td>&nbsp;");
                if(riskData.hasOwnProperty('canUpdate'))
                {
                   if(riskData.canUpdate.hasOwnProperty(index))
                   {
                      if(riskData.canUpdate[index] == true || self.options.section == "admin")
                      {
                        html.push("<input type='button' value='Update "+self.options.objectName+"' id='update_"+index+"' id='update_"+index+"' />");
			            $("#update_"+index).live("click", function(){
				            document.location.href = "updaterisk.php?id="+index;
				            return false;
			            });		                        
                      }
                   }
                }
                //html.push("<input type='button' value='Update Actions' id='update_actions_"+index+"' id='update_actions_"+index+"' />");
                html.push("</td>");	
			
			    /*$("#update_actions_"+index).live("click", function(){
			        if(self.options.section == "admin")
			        {
			            document.location.href = "update_actions.php?id="+index;
			        } else {
			            document.location.href = "../actions/action_update.php?id="+index;
			        }			        
				    return false;
			    });*/	
            }
            if(self.options.editRisk)
            {
               html.push("<td>&nbsp;");
                if(riskData.hasOwnProperty('canEdit'))
                {
                   if(riskData.canEdit.hasOwnProperty(index))
                   {
                      if(riskData.canEdit[index] == true && (self.options.section == "admin" || self.options.section == "manage"))
                      {
                        html.push("<input type='button' value='Edit "+self.options.objectName+"' id='edit_"+index+"' id='edit_"+index+"' />");
			            $("#edit_"+index).live("click", function(){
			                document.location.href = "edit_risk.php?id="+index;
			                return false;
			            });		         
                       // html.push("<input type='button' value='Edit "+self.options.actionName+"' id='edit_actions_"+index+"' id='edit_actions_"+index+"' />");
                      }
                   }
                }
               
                /*if(riskData.hasOwnProperty('canAddAction'))
                {
                   if(riskData.canAddAction.hasOwnProperty(index))
                   {    
                      if(riskData.canAddAction[index] == true)
                      {
                        html.push("<input type='button' value='Add Action' id='add_action_"+index+"' id='add_action_"+index+"' />");
		                $("#edit_"+index).live("click", function(){
		                    document.location.href = "editrisk.php?id="+index;
		                    return false;
		                });		                        
                      }
                   }
                }
		        $("#add_action_"+index).live("click", function(){
			        document.location.href = "new_action.php?id="+index;								  
			        return false;
		        });*/               
               html.push("</td>");

			   $("#edit_actions_"+index).live("click", function(){
			        if(self.options.section == "admin")
			        {
			            document.location.href = "edit_actions.php?id="+index;
			        } else {
			            document.location.href = "../actions/action_edit.php?id="+index;
			        }
				    return false;
			   });	
            }
            if(self.options.addAction)
            {
                html.push("<td>&nbsp;");
                if(riskData.hasOwnProperty('canAddAction'))
                {
                   if(riskData.canAddAction.hasOwnProperty(index))
                   {    
                      if(riskData.canAddAction[index] == true)
                      {
                        html.push("<input type='button' value='Add "+self.options.actionName+"' id='add_action_"+index+"' id='add_action_"+index+"' />");
			            $("#edit_"+index).live("click", function(){
			                document.location.href = "editrisk.php?id="+index;
			                return false;
			            });		                        
                      }
                   }
                }
                html.push("</td>");
			    $("#add_action_"+index).live("click", function(){
				    document.location.href = "new_action.php?id="+index;								  
				    return false;
			    });               
            }
            if(self.options.viewRisk)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='View Details' id='view_query_"+index+"' id='view_query_"+index+"' />");
                html.push("</td>");
			    $("#view_query_"+index).live("click", function(){
				    document.location.href = "view_risk.php?id="+index;
				    return false;
			    });               
            }			
			html.push("</tr>");
		    if(self.options.showActions)
		    {
               if(!$.isEmptyObject(actions))
               {
                  if(actions.hasOwnProperty(index))
                  { 
                    html.push("<tr id='risk_action_"+self.options.tableId+"_"+index+"' class='risk_actions' style='display:none;'>");
                      html.push("<td id='show_action_"+self.options.tableId+"_"+index+"' colspan='"+(columns+2)+"'></td>");
                    html.push("</tr>");
                  }
               }
		    }			
			$("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
			$("#"+self.options.tableId).find("#result_message").remove();

	       $("#more_less_"+self.options.tableId+"_"+index).bind("click", function(e){
               if( $("#risk_action_"+self.options.tableId+"_"+index).is(":hidden") )
               {
                  $("#more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
               } else {
                  $("#more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
               }
               $("#risk_action_"+self.options.tableId+"_"+index).toggle();
               e.preventDefault();
	       });
			
		    if(self.options.showActions)
		    {
               if(!$.isEmptyObject(actions))
               {
                  if(actions.hasOwnProperty(index))
                  {
                     $("#show_action_"+self.options.tableId+"_"+index).action1({risk_id:index, section:self.options.section, viewAction:self.options.viewRisk, page:self.options.page,objectName:self.options.objectName,actionName:self.options.actionName});
                  }
               }
		    }	
		});
		
	}, 
	
	_displayPaging	: function(columns)
	{
		var self  = this;
        var html  = [];
		var pages;
		if( self.options.total%self.options.limit > 0){
			pages   = Math.ceil(self.options.total/self.options.limit); 				
		} else {
			pages   = Math.floor(self.options.total/self.options.limit); 				
		}
		html.push("<tr>");
		  if(self.options.showActions)
		  {
		       html.push("<td>&nbsp;</td>");
		  }				    
		  html.push("<td colspan='"+(columns + 1)+"' class='noborder'>");
		    html.push("<input type='button' value=' |< ' id='first' name='first' />");
		    html.push("<input type='button' value=' < ' id='previous' name='previous' />");
/*            if(pages != 0)
            {
               html.push("Page <select id='select_page' name='select_page'>");
               for(var p=1; p<=pages; p++)
               {
                  html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
               }
               html.push("</select>");		    
            } else {
               html.push("Page <select id='select_page' name='select_page' disabled='disabled'>");
               html.push("</select>");		    
            }*/
		    //html.push(self.options.current+" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
		    html.push(self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
		    html.push("<input type='button' value=' > ' id='next' name='next' />");
		    html.push("<input type='button' value=' >| ' id='last' name='last' />");
          if(self.options.assuranceActions)
          {
              html.push("&nbsp;");
          }
          if(self.options.updateRisk)
          {
              html.push("&nbsp;");
          }
          if(self.options.editRisk)
          {
              html.push("&nbsp;");
          }
          if(self.options.addAction)
          {
              html.push("&nbsp;");
          }
          if(self.options.viewRisk)
          {
              html.push("&nbsp;");
          }          
/*          html.push("<span style='float:right;'>");
            html.push("<b>Quick Search for Risk:</b> R <input type='text' name='q_id' id='q_id' value='' />");
             html.push("&nbsp;&nbsp;&nbsp;");
            html.push("<input type='button' name='search' id='search' value='Go' />");
          html.push("</span>");*/
          html.push("</td>");
		html.push("</tr>");
		$("#"+self.options.tableId).append(html.join(' '));
		
		
		$("#select_page").live("change", function(){
		   if($(this).val() !== "" && parseFloat($(this).val())>0)
		   {
		      self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
		      self.options.current = parseFloat($(this).val());
		      self._getQuery();
		   }
		});
        $("#search").live("click", function(e){
           var risk_id = $("#q_id").val();
           if(risk_id != "")
           {
              self.options.risk_id = $("#q_id").val();
              self._getRisk();
           } else {
              $("#q_id").addClass('ui-state-error');
              $("#q_id").focus().select();
           } 
           e.preventDefault();
        });
		
         if(self.options.current < 2)
         {
                 $("#first").attr('disabled', 'disabled');
                 $("#previous").attr('disabled', 'disabled');		     
         }
         if((self.options.current == pages || pages == 0))
         {
                 $("#next").attr('disabled', 'disabled');
                 $("#last").attr('disabled', 'disabled');		     
         }				
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
	} ,		
	
	_getNext  			: function(rk) 
	{
		rk.options.current = (rk.options.current + 1);
		rk.options.start   = (rk.options.current-1) * rk.options.limit;
		this._getRisk();
	},	
	
	_getLast  			: function( rk ) {
		//var rk 			   = this;
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getRisk();
	},	
	_getPrevious    	: function( rk ) 
	{
		rk.options.current  = parseFloat(rk.options.current) - 1;
		rk.options.start 	= (rk.options.current-1) * rk.options.limit;
		this._getRisk();
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getRisk();
	}
	
	
});
