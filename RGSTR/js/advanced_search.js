// JavaScript Document
$(function(){
		//set the with of the select to be almost uniform	   
	$("table#risk_new").find("select").css({"width":"200px"})	   
	//set the table th text to align to the left
	$("table#search_table").find("th").css({"text-align":"left"})

  //======================================================================================================================	
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_type").append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#risk_type").live("change", function() {
		$("#category").html("");
		$("#category").append($("<option />",{value:"", text:"--risk category--"}))											
		$.get( "controller.php/?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#category").append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});
	//================================================================================================================
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getActiveImpact", function( data ) {
		var impactRating = [];
		$.each( data ,function( index, val ) {
			$("#impact").append($("<option />",{text:val.assessment, value:val.id}));
		})			
	},"json");
	//================================================================================================================	
	/**
		Get the risk impact ratings
	**/
	$("#impact").live( "change" ,function(){
		$("#impact_rating").html("");
		$("#impact_rating").append($("<option />",{value:"", text:"--impact rating--"}))
		$.get( "controller.php?action=getImpactRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#impact_rating").append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#impact_rating").append($("<option />",{value:i, text: i}))	
				}
			}
		}, "json");
		return false;
	});
	/*---------------------------financial exposure ---------------------------------*/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			$("#financial_exposure").append($("<option />",{text:val.name, value:val.id,  selected:((val.status & 4) == 4 ?  "selected" : "")}));
		})			
	},"json");
	//================================================================================================================	
	/**
		get all the risk likelihoods
	**/
	$.get( "controller.php/?action=getLikelihood", function( data ) {
		$.each( data ,function( index, val ) {
			$("#likelihood").append($("<option />",{text:val.assessment, value:val.id}));
		})											 
	},"json");
	//================================================================================================================	
	/**
		Get the risk likelihood ratings
	**/
	$("#likelihood").live( "change" ,function(){
		$("#likelihood_rating").html("");
		$("#likelihood_rating").append($("<option />",{value:"", text:"--likelihood rating--"}))
		$.get( "controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#likelihood_rating").append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#likelihood_rating").append($("<option />",{value:i, text:i}))	
				}
			}
		}, "json");
		return false;
	});	
	//================================================================================================================	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getControl", function( data ) {
		$.each( data ,function( index, val ) {
			$("#percieved_control_effective").append($("<option />",{text:val.effectiveness, value:val.id}));
		})											 
	},"json");	
	//================================================================================================================	
	/**
		Get the risk control effectiveness ratings
	**/
	$("#percieved_control_effective").live( "change" ,function(){
		$("#control_rating").html("")
		$("#control_rating").append($("<option />",{value:"", text:"---perceived control rating---"}))
		$.get( "controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
			$("#control_rating").append($("<option />",{value:data.rating, text:data.rating }))
		}, "json");
		return false;
	});		
	//================================================================================================================	
	$.get("controller.php?action=getSubDirectorate", function( direData ){
		$.each( direData ,function( index, directorate ) {
			$("#directorate").append($("<option />",{text:directorate.value, value:directorate.subid}));
		})
	}, "json");

	// -----------------------------------risk levels ----------------------------
	/**
		get all the risk level
	**/									
		$.get( "controller.php/?action=getRiskLevel", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#risk_level").append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
	
		
	//------------------------------------------------search --------------------------	
	$("#search").click(function(){
	$("#search_table").fadeOut();
	$("#span_again").fadeIn();	
	jsDisplayResult("info", "info", "Searching  . . . <img src='../images/loaderA32.gif' >");
	$.getJSON("controller.php?action=advancedSearch", 
			  {
				formdata : $("#advanced-search-form").serializeArray()  
			  }
			  , function( searchResults ){
					  if( $.isEmptyObject( searchResults.riskData ) ){
						  jsDisplayResult("info", "info", "No data was found . . . ");		  
					  } else {
						 jsDisplayResult("ok", "ok", searchResults.total+" results  were found  . . . ");
						$("#searchresults")
							.css({"clear":"both", "margin-left":"0px"})
							.html("")
							.append($("<table />", {id:"risk_table", align:"left"}))
						populateHeaders( searchResults.headers );
						populateData( searchResults.riskData );			  
					  }	
	});
	return false;
	});
	
	$("#search_again").click(function(){
		$("#search_table").fadeIn();
		$("#span_again").fadeOut();			
		$("#heading").html("Advanced Search")		
		return false;					  
	});
	
});


function populateHeaders( headerData ) 
{		
		var th = $("<tr />");
		$.each( headerData , function( index, header){
			th.append($("<th />",{html:header}));							   
		});	
		$("#risk_table").append( th );
}


function populateData( riskData )
{
	$.each( riskData, function( index , risk){
		var tr = $("<tr />")
		$.each( risk, function( i, rsk){

				tr.append($("<td />",{html:rsk}))					  		   			   
		});
		$("#risk_table").append(tr);					   
	});
}