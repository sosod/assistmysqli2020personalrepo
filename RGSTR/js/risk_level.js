// JavaScript Document
$(function(){

	$("#add_risklevel").click(function(){
		RiskLevel.save();
		return false;
	});	
	
	$("#edit_risklevel").click(function(){
		RiskLevel.edit();
		return false;
	});	
	$("#changes_risklevel").click(function(){
		RiskLevel.update();
		return false;
	});		
	
	
	RiskLevel.get();
	
		   
});

var RiskLevel	= {
	
	get	: function()
	{
		$.post("controller.php?action=getRiskLevels", function( data ){
			$.each( data, function( index, level){
				RiskLevel.display( level );				   				   
			})												   
																		   
		}, "json");	
	} , 
	
	save	: function()
	{
		var data = {};
		data.shortcode  = $("#risklevel_shortcode").val();
		data.name		= $("#risklevel").val();
		data.description= $("#risklevel_description").val();
		data.status 	= 1;

		if( data.shortcode == ""){
			jsDisplayResult("error", "error", "Please enter the shortcode . . . ");
			return false;
		} else if(data.name == "") {
			jsDisplayResult("error", "error", "Please enter the risk level name. . . ");
			return false;
		} else if(data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description . . . ");			
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?action=saveRiskLevel", data, function( response ){
				if( response > 0){
					data.ref = response;
					jsDisplayResult("ok", "ok", "Risk level has been saved . . . ");
					RiskLevel.display( data );	
				} else {
					jsDisplayResult("error", "error", "Error occurred saving the risk level . . . ");
					$("#risk_level_message").html("")
				}												   
			}, "json");		
			
		}
	} , 
	
	change_status 	: function()
	{
			
	} , 
	
	edit			: function()
	{
		var data = {};
		data.shortcode  = $("#risklevel_shortcode").val();
		data.name		= $("#risklevel").val();
		data.description= $("#risklevel_description").val();
		data.status 	= $("#risklevel_status").val();
		data.id 		= $("#risklevel_id").val();		

		if( data.shortcode == ""){
			jsDisplayResult("error", "error", "Please enter the shortcode . . . ");
			return false;
		} else if(data.name == "") {
			jsDisplayResult("error", "error", "Please enter the risk level name. . . ");
			return false;
		} else if(data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description . . . ");			
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?action=updateRiskLevel", data, function( response ){
				if( response > 0){
					data.ref = response;
					jsDisplayResult("ok", "ok", "Risk level has been updated . . . ");
					RiskLevel.display( data );	
				} else {
					jsDisplayResult("error", "error", "Risk level has been updated . . . ");
					
				}												   
			}, "json");		
			
		}
	} , 
	
	update			: function()
	{
			var data = {};
			data.status 	= $("#level_statuses").val();
			data.id 		= $("#risklevel_id").val();	
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?action=changelevelstatus", data, function( response ){
				if( response == 1){
					data.ref = response;
					jsDisplayResult("ok", "ok", "Risk level has been updated . . . ");
					RiskLevel.display( data );	
				} else {
					jsDisplayResult("error", "error", "Error occurred saving the risk level . . . ");
				}												   
			}, "json");	
	} , 
	
	display	: function( level )
	{
		var ref = (level.ref == undefined ? level.id : level.ref);
		$("#risk_level_table")
		 .append($("<tr />",{id:"row_"+ref})
			.append($("<td />",{html:ref}))		   
			.append($("<td />",{html:level.shortcode}))				   
			.append($("<td />",{html:level.name}))
			.append($("<td />",{html:level.description}))
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"edit_"+ref, id:"edit_"+ref, value:"Edit"}))		
				.append($("<input />",{type:"submit", value:"Del", id:((level.ref & 4) == 4 ? "del__"+ref : "del_"+ref), name:"del_level_"+ref}))
			 )				   			
			.append($("<td />",{html:((level.status & 1) == 1 ? "Active" : "Inactive")})		  
			 )				   			
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"change_"+ref, id:"change_"+ref, value:"Change Status"}))
			 )				   						
		 )
		 if((level.status & 4) == 4)
		 {
              $("#del__"+ref).attr('disabled', 'disabled');              		    
		 }
		 		 
		 $("#edit_"+ref).live("click",function(){
			document.location.href = "edit_risk_level.php?id="+ref+"&";						   
			return false;							   
		})
		 
		 $("#change_"+ref).live("click",function(){
			document.location.href = "change_level_status.php?id="+ref+"&";						   
			return false;							   
		})		 
		
		$("#del_level_"+ref).live( "click" ,function(){
			var message = $("#risk_level_message");
			if( confirm(" Are you sure you want to delete this risk level ")) {
				jsDisplayResult("info", "info", "Inactivating status . . . <img src='../images/loaderA32.gif' >");
				$.post("controller.php?action=deActivateLevel", { id:ref }, function( delData ) {
					if( delData == 1){
						jsDisplayResult("ok", "ok", "Level deactivated . . . ");
						$("#row_"+ref).fadeOut();
					} else if( delData == 0){
						jsDisplayResult("info", "info", "No change was made to the level . . . ");
					} else {
						jsDisplayResult("info", "info", "Error updating , please try again . . . ");
					}		   
				});				
			}
			return false;											 
		});		
	}
	
	
	
	
	
}

