// JavaScript Document
$(function(){
	var namingItems = [
		{name:"Risk Item" , id:"risk_item" } ,
		{name:"Risk type" , id:"risk_type" } ,
		{name:"Risk Category" , id:"risk_category" } ,
		{name:"Resk description" , id:"risk_description" } , 
		{name:"Background of the risk" , id:"risk_background" } ,
		{name:"impact" , id:"impact" } , 
		{name:"Impact Rating" , id:"impact_rating" } , 
		{name:"Likelihood" , id:"likelihood" } , 
		{name:"Likelihood Rating" , id:"likelihood_rating" } ,
		{name:"Inherent Risk Exposure" , id:"inherent_risk_exposure" } , 
		{name:"Inherent Risk Rating" , id:"inherent_risk_rating" } ,
		{name:"Current Controls" , id:"current_controls" } ,
		{name:"Perceived Control of effectiveness" , id:"percieved_control_effective" } ,
		{name:"Control Effectiveness Rating" , id:"control_rating" } ,
		{name:"Residual Risk" , id:"residual_risk" } ,
		{name:"Residual Risk Exposure" , id:"residual_risk_exposure" } ,
		{name:"Risk Owner" , id:"risk_owner" } ,
		{name:"Actions to improve management of risk" , id:"actions_to_improve" } ,
		{name:"Action Owner" , id:"action_owner" } ,
		{name:"Time Scale" , id:"timescale" } 
  	];
	
	$.get( "old_controller.php?action=getHeaderNames", function( headerNamesData ){												
		forms.generateNamingForm( namingItems , headerNamesData);											   
	},"json");




});

var forms = {
	generateNamingForm  : function( items, namingData ) {
	$("#container").append($("<table />",{id:"contents" , width:"100%"})
		.append($("<tr />")
			.append($("<th />",{html:"Ref"}))
			.append($("<th />",{html:"Section"}))
			.append($("<th />",{html:"Default Terminology"}))
			.append($("<th />",{html:"Your Terminology"}))
			.append($("<th />",{html:""}))			
		)						 
	);
	$.each( namingData, function( index, value) {
		//alert(value.id+" : "+value.name+" : "+value.client_terminology);
		$("#contents")
		 .append($("<tr />",{id:"tr_"+value.id})
			.append($("<td />",{html:value.id}))
			.append($("<td />",{html:(value.type=="risk" ? window.risk_object_name : window.action_object_name)}))
			.append($("<td />",{html:value.ignite_terminology}))
			.append($("<td />")
			  .append($("<input />",{type:"text", name:"name_"+value.id, id:"name_"+value.id, value:value.client_terminology, size:"50" }))	
			  .append($("<span />",{id:"updating_"+value.id })
				.append($("<img />",{src:"../images/arrows16.gif"}))
				.hide()
			  )
			)
			.append($("<td />",{align:"center"})
				.append($("<input />",{type:"submit" , name:"save_"+value.id , id:"save_"+value.id , value:"Update" }))	  
			)			
		  )

		$("#tr_"+value.id).live( "mouseover", function(){
			$(this).addClass("tdhover"); //css({"background-color":"#CCCCCC"})
		});

		$("#tr_"+value.id).live( "mouseout", function(){
			$(this).removeClass("tdhover") //.css({"background-color":"white"})
		});

		$("#save_"+value.id).live( "click" , function() {
			if( $("#"+value.id).val() == "" ){
				$("#naming_message").slideDown().html("No changes were made.")
			}else{
			$("#updating_"+value.id).show();
				jsDisplayResult("info", "info", "Saving changes...  <img src=\"../images/loaderA32.gif\" />" );
				//alert(value.name+" : "+value.id+" : "+$("#"+value.id).val());
			$.post("old_controller.php?action=updateHeaderNames" , 
				   {
					 fieldname : value.name ,
					 value 	   : $("#name_"+value.id).val()
				   } , function( data ) {
						if( data == 1 ) {							
							jsDisplayResult("ok", "ok", "Changes saved. . . ");
						} else if(data == 0) {
							jsDisplayResult("info", "info", "No change was made to the field names. . . ");	
						} else {
							jsDisplayResult("error", "error", "There was an error saving the custom name. . . ");	
						}
			})
		  }
			$("#updating_"+value.id).remove();
		  return false
		});		
		$("input:text").prop("size","50");
	});
	//alert($("#18").val());
/*	$.each( items , function( index , val ) {							 
		console.log( val );					 
		$("#contents").append($("<tr />",{id:"tr_"+val.id})
			.append($("<td />",{html:parseFloat(index)+1}))
			.append($("<td />",{html:val.name}))
			.append($("<td />",{align:"center"})
				.append($("<input />",{
						  type:"text" ,
						  value:(namingData[val.id]== "" || namingData[val.id]== null ? val.name : namingData[val.id])  ,
						  id:val.id ,
						  name:val.id,
						  size:"50"}))		  
			)
			.append($("<td />",{align:"center"})
				.append($("<input />",{type:"submit" , name:"save_"+val.id , id:"save_"+val.id , value:"Update" }))	  
			)
		)	
			$("#"+val.id).live( "focus", function(){
				$("#tr_"+val.id).css({"background-color":"yellow"})							   
			});
			
			$("#"+val.id).live( "focusout", function(){
				$("#tr_"+val.id).css({"background-color":"white"})							   
			});
			
			$("#save_"+val.id).live( "click" , function() {
				if( $("#"+val.id).val() == "" ){
					$("#naming_message").html("no menu changes were made")
				}else{
				$.post("controller.php?action=naming" , 
					   { fieldname : val.id ,
					     value : $("#"+val.id).val()
					   } , function( data ) {
							if( data == 1 ) {
								$("#naming_message").html("Changes saved")
							} else {
								$("#naming_message").html("There was an error saving the custom name")	
							}
				})
			  }
			  return false
			})				
			
		});*/
	}
	
}