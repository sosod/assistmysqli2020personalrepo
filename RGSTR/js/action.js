// JavaScript Document
var actionData = {};
$(function(){

    $("#save_action_update").click(function(e){
        Action.update();
        e.preventDefault();
    });

   $("#edit_action").click(function(e){
       Action.edit();
       e.preventDefault();
    });


    $("#status").change(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 3)
        {
            $("#progress").val(100).attr('disabled', 'disabled');
            $("#approval").removeAttr("disabled");
        } else {
            if( $("#progress").val() == 100 && $("#_progress").val() != 100)
            {
                $("#progress").val($("#_progress").val()).removeAttr('disabled');
            }
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });

    $("#progress").blur(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 100)
        {
            $("#status").val(3);
            $("#progress").attr('disabled', 'disabled');
            $("#approval").removeAttr("disabled");
        } else {
            if( $("#status").val() == 3 && $("#_status").val() != 3)
            {
                $("#status").val( $("#_status").val() );
            }
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });

	$("#add_action").live( "click", function(){
		document.location.href = "add_action.php?id="+$("#risk_id").val();			 								 
		return false;								
	});

	$("#close").live("click", function(){
		$("#add_new_action").hide();		
		return false;
	});


    $(".remove_attach").live("click", function(e){
        var id       = this.id
        var ext      = $(this).attr('title');
        var type     = $(this).attr('ref');
        var filename = $(this).attr("file");
        $.post('controller.php?action=deleteActionAttachment',
            {
                attachment : id,
                ext        : ext,
                id         : $("#action_id").val(),
                type       : type,
                filename   : filename
            }, function(response){
                if(response.error)
                {
                    $("#result_message").css({padding:"5px", clear:"both"}).html(response.text)
                } else {
                    $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text)
                    $("#li_"+ext).fadeOut();
                }
            },'json');
        e.preventDefault();
    });


    $(".upload_action").live("change", function(e){
        Action.uploadAttachment(this.id);
        e.preventDefault();
    });
});


var Action = {


    update : function()
    {
            //var message 	= $("#update_message").slideDown("fast");
            var description = $("#description").val();
            var status		= $("#status :selected").val();
            var progress	= $("#progress").val();
            var remindon	= $("#remindon").val();
            var actionon	= $("#action_on").val();
            //var attachment	= $("#update_attachment").val();
            var approval 	= ($("#approval").is(":checked") ? "on" : "" );

			var err = "";
			var valid8 = true;
			$("input:text, textarea, select").each(function() {
				$(this).removeClass("required");
			});
			
            if( description == "") {
				err+="<li><b>Response</b> is required.</li>";
				valid8 = false;
				$("#description").addClass("required");
            } 
			if( status == "" ) {
				err+="<li><b>Status</b> is required.</li>";
				valid8 = false;
				$("#status").addClass("required");
            } 
			if(actionon == ""){
				err+="<li><b>Action On</b> is required.</li>";
				valid8 = false;
				$("#action_on").addClass("required");
            }
			if( progress == "" ) {
				err+="<li><b>Progress</b> is required.</li>";
				valid8 = false;
				$("#progress").addClass("required");
            } else if( isNaN( progress ) ) {
				err+="<li><b>Progress</b> must be a number.</li>";
				valid8 = false;
				$("#progress").addClass("required");
            } else if(progress == "100" && status !== "3" ){
				err+="<li>Progress cannot be 100% if the Status is not Completed/Addressed.</li>";
				valid8 = false;
				$("#status, #progress").addClass("required");
            } 
			
			if(!valid8) {
				$("<div />",{id:"dlg_msg",title:"Error"})
					.html("<h1 class=red>Error!</h3><p>The following error(s) have occurred:</p><ul>"+err+"</ul><p>Please correct the error(s) and try again.</p>")
					.dialog({
						modal: true,
						buttons:[{
							text: "Ok",
							click: function() { $(this).dialog("close"); }
						}]
					});
					$(".ui-dialog-titlebar").hide();
				return false;
			} else {
                //jsDisplayResult("info", "info", "Update action ...  <img src='../images/loaderA32.gif' />");
				$("<div />",{id:"dlg_msg",title:"Processing..."})
					.html("<p>Your update is being processed.<br />Please be patient...</p><p class=center><img src='/pics/ajax_loader_v2.gif' /></p>")
					.dialog({
						modal: true,
						closeOnEscape: false,
						width: 250,
						height: 300
					});
				$(".ui-dialog-titlebar").hide();
                $.post( "../common/common_controller.php?action=newActionUpdate",
                {
                    id 			: $("#action_id").val(),
                    description	: description ,
                    status		: status ,
                    progress	: progress ,
                    remindon 	: remindon,
                    action_on 	: actionon,
                 
                    approval	: approval
                }, function( response  ) { //alert(response.text);
					$("#result").val("ok");
					$("#response").val(response.text);
					var f = 0;
					$("input:file").each(function() {
						f+=$(this).val().length;
					});
					var page_src = $("#page_src").val(); //alert(f);
					//if(f>0) {
						//alert($("form[name=frm_update]").prop("method"));
						$form = $("form[name=frm_update]");
						$form.attr("target","file_upload_target"); //alert($form.attr("target"));
							//var display = response.text;
							//alert(display);
						$("form[name=frm_update] #email_message").val(response.email_message);
							
						var post_action = "window.parent.$('#dlg_msg').dialog('close');";
						post_action+="window.parent.$('<div />',{id:'dlg_ok',title:'Success'}).html('"+response.text+"').dialog({modal: true,closeOnEscape: true,width: 350,height: 250,buttons:[{text: 'Ok',click: function() { ";
						if(page_src=="FRONTPAGE") {
							post_action+=" window.parent.parent.header.location.href = '/title_login.php?m=action_dashboard'; ";
						} else {
							post_action+=" window.parent.document.location.href = 'update_actions.php?page_src="+page_src+"'; ";
						}
						post_action+=" } }], close: function() { window.parent.document.location.href = 'update_actions.php?page_src="+page_src+"'; }	});";
						$("form[name=frm_update] #action").val("UPDATE_ATTACH");
						$("form[name=frm_update] #after_action").val(post_action);
						//alert($form.attr("action"));
						//alert(post_action);
						$form.submit();
					/*} else {
						//$("#dlg_msg").dialog("close");
						//$("td."+$("#div_update #obj_id").val()+"updateclass").html(okIcon);
						//alert(page_src);
						$("<div />",{id:"dlg_ok",title:"Success"})
							.html(response.text)
							.dialog({
								modal: true,
								closeOnEscape: true,
								width: 350,
								height: 200,
								buttons:[{
									text: "Ok",
									click: function() { 
										if(page_src=="FRONTPAGE") {
											parent.header.location.href = '/title_login.php?m=action_dashboard';
										} else {
											document.location.href = "update_actions.php?page_src="+page_src; 
										}
									}
								}]
							});
					}*/
/*                    if( response.error )
                    {
                        jsDisplayResult("error", "error", response.text );
                    } else {
                        $("#description").val('');
                        jsDisplayResult("ok", "ok", response.text );
                    }*/
                }, "json")
            }
    } ,

    edit: function()
    {
            var message 		= $("#edit_action_message").slideDown();
            var rs_action 		= $("#action").val();
            var rs_action_owner = $("#action_owner :selected").val();
            var rs_deliverable  = $("#deliverable").val();
            var rs_timescale    = $("#timescale").val();
            var rs_deadline     = $("#deadline").val();
            var rs_progress     = $("#progress").val();
            var rs_status       = $("#action_status :selected").val();
            var rs_remindon     = $("#remindon").val();

            if( rs_action == "" ) {
                jsDisplayResult("error", "error", "Please enter the action");
                return false;
            } else if( rs_action_owner == "" ) {
                jsDisplayResult("error", "error", "Please enter the action owner for this risk");
                return false;
            } else if( rs_deliverable == "" ) {
                jsDisplayResult("error", "error", "Please enter the deliverable for this action");
                return false;
            } else if( rs_timescale == "" ) {
                jsDisplayResult("error", "error", "Please enter the time scale for this action");
                return false;
            } else if( rs_deadline == "" ) {
                jsDisplayResult("error", "error", "Please enter the deadline for this action ");
                return false;
            } else {
                jsDisplayResult("info", "info", "Updating action ...  <img src='../images/loaderA32.gif' />");
                $.post( "controller.php?action=updateRiskAction" ,
                    {
                        id				: $("#actionid").val() ,
                        r_action 		: rs_action ,
                        action_owner    : rs_action_owner ,
                        deliverable     : rs_deliverable ,
                        timescale       : rs_timescale ,
                        deadline     	: rs_deadline ,
                        remindon     	: rs_remindon ,
                        progress     	: rs_progress ,
                        status			: rs_status
                    } ,
                    function( response ) {
                        if( !response.error){
                            jsDisplayResult("ok", "ok", response.text );
                            //message.html( response.text ).addClass('ui-state-highlight')
                        } else {
                            jsDisplayResult("error", "error", response.text );
                            //message.html( response.text ).addClass('ui-state-error')
                        }

                    } ,"json");
            }
    },

    uploadAttachment        : function(element_id)
    {
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        $.ajaxFileUpload
        (
            {
                url			  : 'controller.php?action=saveActionAttachment&element='+element_id,
                secureuri	  : false,
                fileElementId : element_id,
                dataType	  : 'json',
                success       : function (response, status)
                {
                    $("#file_upload").html("");
                    if(response.error )
                    {
                        $("#file_upload").html(response.text )
                    } else {
                        if($("#result_message").length > 0)
                        {
                            $("#result_message").remove();
                        }

                        $("#file_upload").html("");
                        if(response.error)
                        {
                            $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                        } else {
                            $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                            ).append($("<div />",{id:"files_uploaded"}))
                            if(!$.isEmptyObject(response.files))
                            {
                                var list = [];
                                list.push("<span>Files uploaded ..</span><br />");
                                $.each(response.files, function(ref, file){
                                    list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                    list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                    list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"' file='"+file.name+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                    list.push("</p>");
                                });
                                $("#files_uploaded").html(list.join(' '));

                                $(".delete").click(function(e){
                                    var id   = this.id
                                    var ext  = $(this).attr('title');
                                    var name = $(this).attr('file');
                                    $.post('controller.php?action=removeActionFile',
                                        {
                                            attachment : id,
                                            ext        : ext,
                                            name       : name,
                                            element    : element_id
                                        }, function(response){
                                            if(response.error)
                                            {
                                                $("#result_message").html(response.text)
                                            } else {
                                                $("#result_message").addClass('ui-state-ok').html(response.text)
                                                $("#li_"+id).fadeOut();
                                            }
                                        },'json');
                                    e.preventDefault();
                                });
                                $("#"+element_id).val("");
                            }
                        }

                    }
                },
                error: function (data, status, e)
                {
                    $("#fileupload").html( "Ajax error -- "+e+", please try uploading again");
                }
            }
        )
        return false;
    }

}

function doAjax(url,dta) {
	var result;
	//alert("ajax ::=> "+dta);
	$.ajax({                                      
		url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
		success: function(d) { 
			//alert("d "+d.text); 
			//console.log(d);
			result=d; 
			//showResponse(d.text);
			//return d;
		},
		error: function(d) { console.log(d); result=d; }
	});
	//alert("result "+result.text);
	return result;
}

function doDeleteAttachment(index,url,dta) {
	var r = doAjax(url,dta);
	var i = index.split("_");
		a = i[0];
		$(function() {
			$("#li_"+index).hide();
		});
	showResponse(r.text);
}

function showResponse(t) {
						$("<div />",{id:"dlg_ok",title:"Success"})
							.html("<p>"+t+"</p>")
							.dialog({
								modal: true,
								closeOnEscape: true,
								width: 250,
								height: 250,
								buttons:[{
									text: "Ok",
									click: function() { $(this).dialog("close"); }
								}]
							});
}