// JavaScript Document
$(function(){
	$("#go").click(function(){
		jsDisplayResult("info", "info", "Searching  . . . <img src='../images/loaderA32.gif' >");
		$.post("controller.php?action=simplesearch",{searchtext: $("#search_text").val()}, function( searchResults ){
			  if( $.isEmptyObject( searchResults.riskData ) ){
				  jsDisplayResult("info", "info", "No data was found . . . ");			  
			  } else {
			    jsDisplayResult("ok", "ok", searchResults.total+" results  were found  . . . ");
				$("#searchresults")
					.css({"clear":"both", "margin-left":"0px"})
					.html("")
				    .append($("<table />", {id:"risk_table", align:"left"}))
				populateHeaders( searchResults.headers );
				populateData( searchResults.riskData , $("#search_text").val() );			  
			  }						
		}, "json");							
		return false;
	});
	
})

function populateHeaders( headerData ) 
{		
		var th = $("<tr />");
		$.each( headerData , function( index, header){
			th.append($("<th />",{html:header}));							   
		});	
		$("#risk_table").append( th );
}


function populateData( riskData , text)
{
	$.each( riskData, function( index , risk){
		var tr = $("<tr />")
		$.each( risk, function( i, rsk){
            if( rsk == null || rsk == ""){
                tr.append($("<td />",{html:rsk}))
            } else {
                if( rsk.indexOf(text) >= 0) {
                    tr.append($("<td />",{html:"<span style='background-color:yellow'>"+rsk+"</span>"}))
                } else {
                    tr.append($("<td />",{html:rsk}))
                }
            }
		});
		$("#risk_table").append(tr);
	});
}
