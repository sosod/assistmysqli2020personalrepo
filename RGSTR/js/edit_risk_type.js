// JavaScript Document
$(function(){
		
		$("#edit_risktype").click(function(){
		var message   	= $("#edit_risk_type");
		var shortcode 	= $("#risktype_shortcode").val();
		var type 		= $("#risktype").val();
		var description	= $("#risktype_descri").val();
		
		if ( shortcode == "" ) {
			jsDisplayResult("error", "error", "Please the short code for this risk type . . . ");
			return false;
		} else if( type == "") {
			jsDisplayResult("error", "error", "Please enter the risk type . . . ");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateRiskType", 
		   { 
			   id			: $("#risktype_id").val(), 
			   shortcode	: shortcode, 
			   name			: type, 
			   description	: description 
		   }, function( retData ) {
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Risk type successfully updated . . . ");
					message.html(" .. ");						
				} else if( retData == 0 ){
					jsDisplayResult("info", "info", "Risk type not changed . . . ");
				} else {
					jsDisplayResult("error", "error", "Error updating the risk type. . . ");
				}
			});
		}
		return false;
		});

	$("#cancel_risktype").click(function(){
		history.back();
		return false;								   
	})
	

})