// JavaScript Document
$(function(){
		   
			$("#changes_risktype").click(function() {
				//var message = $("#change_risk_type_message")
				$.post( "controller.php?action=changetypestatus",
					   { id		: $("#risktype_id").val() ,
					   	 status : $("#type_statuses :selected").val()
					   }, function( typDedeactData ) {
					if( typDedeactData == 1) {
						jsDisplayResult("ok", "ok", "Risk Type status changed . . . ");
					} else if( typDedeactData == 0) {
						jsDisplayResult("info", "info", "No change was made to the risk type status . . . ");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again . . . ");
						message.html(" ").stop().animate({opacity:"0.0"},4000);
					}										 
				})									
				return false;										  
			});
			
})