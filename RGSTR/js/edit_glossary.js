// JavaScript Document
$(function(){
		   	
	$("#edit_glossary").live("click", function(){	
		var message  	= $("#glossary_message");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			jsDisplayResult("error", "error", "Please select category . . . ");
			return false;
		} else if ( terminology == "" ) {
			jsDisplayResult("error", "error", "Please enter the terminology . . . ");			
			return false;
		} else if ( explanation == "" ){
			jsDisplayResult("error", "error", "Please enter the explanation . . . ");
			return false;
		} else  {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?function=updateGlossary",
			  {
				id 			: $("#glossaryid").val(),
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Glossary term saved . . . ");
				} else if( retData == 0 ) {
					jsDisplayResult("info", "info", "No change were made to the glossary . . . ");					
				} else {
					jsDisplayResult("error", "error", "Error saving the glossary term. . . ");
				}
			});
		 }
				return false;
			})


	$("#delete_glossary").click(function() {
		var message  	= $("#glossary_message");										
		if(confirm("Are you sure you want to delete this glossary item ")){
			jsDisplayResult("info", "info", "Deleting . . . <img src='../images/loaderA32.gif' >");
			$.post("controller.php?function=deleteGlossary",{ id : $("#glossaryid").val() }, function( retData ) {
				if( retData == 1 ){
					jsDisplayResult("ok", "ok", "Glossary deleted. . . ");	
				} else {
					jsDisplayResult("error", "error", "There was an error deleting glossary . . . ");
					message.html("")	;
				}
			})	
		}								
		return false;								 
	});
	
	$("#cancel_glossary").live("click", function(){
		$("#new_term_table").fadeOut();										 
		return false;										 
	});
})