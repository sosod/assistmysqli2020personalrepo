// JavaScript Document
$(function(){
	/**	   
		Get all the directorates
	**/
	$("#directorate_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getUsers", function( data ) {		
													  
													  console.log( data )
		$.each( data, function( index, val) {
			$("#dr_users_respo")
				.append($("<option />",{text:val.tkname+" "+val.tksurname, value:val.tkid}))
		});											   
	},"json");
	
	
	$.get( "controller.php?action=getDirectorate", function( data ) {
		$("#directorate_message").html("");
		var message = $("#directorate_message")
		$.each( data, function( index, val) {
		   $("#directorate_table")
		   .append($("<tr />")
			 .append($("<td />",{html:val.id}))		 
			 .append($("<td />",{html:val.shortcode}))		 						 
			 .append($("<td />",{html:val.department}))		 
			 .append($("<td />",{html:val.dept_function}))	
			 .append($("<td />",{html:val.tkname+" "+val.tksurname}))
			 .append($("<td />",{html:(val.assign_actions == 1 ? "Yes" : " No")}))			 
			 .append($("<td />")
				.append($("<input />",{type:"submit", name:"dire_edit_"+val.id, id:"dire_edit_"+val.id, value:"Edit"}))
				.append($("<input />",{type:"submit", name:"dire_del_"+val.id, id:"dire_del_"+val.id, value:"Del"}))							
			 )
			 .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "InActive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), name:"dire_activate_"+val.id, id:"dire_activate_"+val.id, value:"Activate"}))
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ), name:"dire_inactivate_"+val.id, id:"dire_inactivate_"+val.id, value:"InActivate"}))							
			 )		
		   	.append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		  
			)
			)
		   	$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_directorate_status.php?id="+val.id;
				return false;										  
			});		   
		   	$("#dire_edit_"+val.id).live( "click", function() {
				document.location.href = "edit_directorate.php?id="+val.id;
				return false;										  
			});
			$("#dire_del_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this directorate ")) {												 
					$.post( "controller.php?action=changeDireStatus", 
						   {
							   id 	  : val.id,
							   status : 0 
							   }, function( delData ) {
						if( delData == 1){
							message.html("Directorate structure deleted").animate({opacity:"0.0"},4000);
						} else if( delData == 0){
							message.html("No change was made to the directorate structure").animate({opacity:"0.0"},4000);
						} else {
							message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
						}										 
					});
				}
				return false;										  
			});
			$("#dire_activate_"+val.id).live( "click", function() {
				$.post( "controller.php?action=dire_activate", {id:val.id}, function( actData ) {
					if( actData == 1){
						message.html("Directorate structure activated").animate({opacity:"0.0"},4000);
					} else if( actData == 0){
						message.html("No change was made to the directorate structure").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}										 
				})															 
				return false;										  
			});
			$("#dire_inactivate_"+val.id).live( "click", function() {
				$.post( "controller.php?action=dire_deactivate", {id:val.id}, function( deactData ) {
					if( deactData == 1){
						message.html("Directorate structure deactivated").animate({opacity:"0.0"},4000);
					} else if( deactData == 0){
						message.html("No change was made to the directorate structure").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}										 
				})									
				return false;										  
			});
		   
		});										   
	},"json");
	
	$("#add_directorate").click(function(){
	 	var message  	 = $("#directorate_message");
		var d_shortcode  = $("#directorate_shortcode").val();
		var d_department = $("#dr_department").val();
		var d_funtion 	 = $("#dept_function").val();
		var d_user		 = $("#dr_users_respo :selected");
	
		if( d_shortcode == "") {
			message.html("Please enter the short code for this directorate");
			return false;
		} else if( d_department == "") {
			message.html("Please enter the department name for this directorate");
			return false;			
		} else if( d_funtion == "" ) {
			message.html("Please enter the department function for this directorate");
			return false;			
		} else if( d_user.val() == "" ) {
			message.html("Please select user responsible for this directorate");
			return false;			
		} else {
			$.post( "controller.php?action=newDirectorate", 
				   {
					   shortcode	: d_shortcode,
					   dpt			: d_department,
					   fxn			: d_funtion,
					   user			: d_user.val()
					 },
				   function( retData ) {
					 if( retData > 0 ) {
						$("#directorate_shortcode").val("");
						$("#dr_department").val("");
						$("#dept_function").val("");
						$("#dr_users_respo option[value='']").attr('selected', 'selected');
						message.html("Directorate saved successifully . . .");
					   $("#directorate_table")
					   .append($("<tr />")
						 .append($("<td />",{html:retData}))		 
						 .append($("<td />",{html:d_shortcode}))		 						 
						 .append($("<td />",{html:d_department}))		 
						 .append($("<td />",{html:d_funtion}))	
						 .append($("<td />",{html:d_user.text()}))
						 .append($("<td />")
						  	.append($("<input />",{type:"submit", name:"dire_edit", id:"dire_edit", value:"Edit"}))
						  	.append($("<input />",{type:"submit", name:"dire_del", id:"dire_del", value:"Del"}))							
						 )
						 .append($("<td />",{html:"<b>Active</b>"})
						  	//.append($("<input />",{type:"submit", name:"dire_activate", id:"dire_activate", value:"Activate"}))
						  	//.append($("<input />",{type:"submit", name:"dire_inactivate", id:"dire_inactivate", value:"InActivate"}))							
						 )		
						 .append($("<td />")
							.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))	
						  )
					   )
						$("#change_"+retData).live( "click", function() {
							document.location.href = "change_directorate_status.php?id="+retData;
							return false;										  
						});		   
						$("#dire_edit_"+retData).live( "click", function() {
							document.location.href = "edit_directorate.php?id="+retData;
							return false;										  
						})					   
					 } else {
						 message.html("There was an error saving , please try again");
					 } 
			}, "json")
		}
		return false;
	});	
		
})
