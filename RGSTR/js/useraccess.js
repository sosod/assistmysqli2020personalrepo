// JavaScript Document
$(function(){
		   
	$.get("old_controller.php?action=getUnAssignedUsers", function( userData ) {
		$.each( userData, function( index, user ){
			$("#userselect").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid }))
		});										 
	},"json");		   
		   
	/**	   
		Get all users access details   
	**/
	$.get( "old_controller.php?action=getUserAccess", function( userData ) {
	  $('body').data('users',userData);
	  $.each( userData , function( index, user ){
		$("#useraccess_table")
		.append($("<tr />").addClass('user-list')
		   .append($("<td />",{class:"center",html:user.id}))
		   .append($("<td />",{html:user.tkname+" "+user.tksurname}))		   
		   .append($("<td />",{class:"center",html:(user.module_admin == 0 ? "no" : "yes")}))
		   .append($("<td />",{class:"center",html:(user.create_risks == 0 ? "no" : "yes")}))
		   .append($("<td />",{class:"center",html:(user.create_actions == 0 ? "no" : "yes")}))
		   .append($("<td />",{class:"center",html:(user.view_all == 0 ? "no" : "yes")}))
		   .append($("<td />",{class:"center",html:(user.edit_all == 0 ? "no" : "yes")}))		
		   //.append($("<td />",{html:(user.update_all == 0 ? "no" : "yes")}))		   
		   .append($("<td />",{class:"center",html:(user.report == 0 ? "no" : "yes")}))
		   .append($("<td />",{class:"center",html:(user.assurance == 0 ? "no" : "yes")}))
		   .append($("<td />",{class:"center",html:(user.setup == 0 ? "no" : "yes")}))
		   .append($("<td />")
			  .append($("<input />",{type:"submit", name:"edit_"+user.id, id:"edit_"+user.id, value:"Edit"}))
			)
		 )	
		
		$("#edit_"+user.id).click(function() {
			document.location.href = "user_edit.php?id="+user.id;
			return false;
		});
	  })
	  
	}, "json")
	
	/**
		Adding new user access
	**/
	$("#setup_user_access").live('click', function() {
		var message 	 	= $("#useraccess_message");
		var user 		 	= $("#userselect :selected").val();
		var module_admin 	= $("#module_admin :selected");
		var create_risks 	= $("#create_risks :selected");	
		var create_actions  = $("#create_actions :selected");
		var view_all 		= $("#view_all :selected");		
		var edit_all 		= $("#edit_all :selected");
		var update_all 		= $("#update_all :selected");
		var reports 		= $("#reports :selected");	
		var assurance 		= $("#assurance :selected");
		var setup 			= $("#usersetup :selected");	
		
		var _userData 		= $('body').data('users');
		var usersArray 		= [];
	
		if(user == "") {
			jsDisplayResult("error", "error", "Please select the user to set up the access");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post( "old_controller.php?action=newUserAccess",{
					  user		: user,
					  modadmin	: module_admin.val(), 
					  cr_risk	: create_risks.val(),
					  cr_actions: create_actions.val(),
				  	  v_all		: view_all.val(),
					  e_all		: edit_all.val(),
					  u_all		: update_all.val(),
					  reports	: reports.val(),
					  assurance : assurance.val(),
					  setup		: setup.val()
		     }, function(response) {
                $("#module_admin").val(0);
                $("#create_risks").val(0);
                $("#create_actions").val(0);
                $("#view_all").val(0);
                $("#edit_all").val(0);
                $("#reports").val(0);
                $("#assurance").val(0);
                $("#usersetup").val(0);

                //jsDisplayResult("ok", "ok", "User saved");
                //UserAccess.getUnAssignedUsers();
                //UserAccess.listUsers();
				document.location.href = "user_access.php?r[]=ok&r[]=User+access+saved+successfully";
            }, "json" )
		}
		return false;							  
	});
})


var UserAccess = {

    getUnAssignedUsers      : function()
    {
        $("#userselect").append($("<option />", {text:"Loading...", value:""}))
        $.get("old_controller.php?action=getUnAssignedUsers", function( userData ) {
            $("#userselect").empty();
            $("#userselect").append($("<option />",{text:"--select user--", value:""}))
            $.each( userData, function( index, user ){
                $("#userselect").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid }))
            });
        },"json");
    } ,

    listUsers               : function()
    {
        $(".user-list").remove();
        $.get( "old_controller.php?action=getUserAccess", function( userData ) {
            $.each( userData , function( index, user ){
                $("#useraccess_table")
                    .append($("<tr />").addClass('user-list')
                        .append($("<td />",{html:user.id}))
                        .append($("<td />",{html:user.tkname+" "+user.tksurname}))
                        .append($("<td />",{html:(user.module_admin == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.create_risks == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.create_actions == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.view_all == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.edit_all == 0 ? "no" : "yes")}))
                        //.append($("<td />",{html:(user.update_all == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.reports == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.assurance == 0 ? "no" : "yes")}))
                        .append($("<td />",{html:(user.setup == 0 ? "no" : "yes")}))
                        .append($("<td />")
                            .append($("<input />",{type:"submit", name:"edit_"+user.id, id:"edit_"+user.id, value:"Edit"}))
                        )
                    )

                $("#edit_"+user.id).click(function() {
                    document.location.href = "user_edit.php?id="+user.id;
					
                    return false;
                });
            })
        }, "json")
    }


}