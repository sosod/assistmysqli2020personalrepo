// JavaScript Document
$(function(){
/**
	Action Status , manages the action status  
**/
	//$("#actionstatus_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getActionStatus" , function( data ) {
		$("#actionstatus_message").html("");
		//var message = $("#actionstatus_message");
		$.each( data ,function( index, val){
			$("#action_status_table")
				.append($("<tr />",{id:"tr_"+val.id, class:"active"+((val.status & 1) == 1 ? "1" : "0")})
					.append($("<td />",{html:val.id}))		  
					.append($("<td />",{html:val.name}))		  
					.append($("<td />",{html:val.client_terminology}))		  
					.append($("<td />")
					  .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))		
					)		  
					.append($("<td />",{ html:( ((val.status & 1) == 1) ?  "<b>Active</b>"  : "<b>Inactive</b>")}))
					.append($("<td />")
					  .append($("<input />",{type:"submit",name:"edit_"+val.id, value:"Edit", id:"edit_"+val.id }))
					  .append($("<input />",{type:"submit", id:(((val.status & 4) == 4 || val.count>0) ? "del__"+val.id : "del_"+val.id), value:"Del", name:"del_"+val.id , style:"margin-left: 5px"}))
					.append($("<input />",{type:"submit", name:"change_"+val.id, id:((val.status & 4) == 4 ? "change__"+val.id : "change_"+val.id), value:"Change Status", style:"margin-left: 5px"}))
					)		  
			)
				
		     if((val.status & 4) == 4 || val.count>0)
		     {
		        $("#del__"+val.id).hide();
		        $("#change__"+val.id).hide();
		     }		
				
			$("#change_"+val.id).live("click", function(){
				document.location.href = "setup_status.php?field=action_status&id="+val.id+"&";		
				return false
			});		
		
			$("#edit_"+val.id).live( "click", function(){
				document.location.href = "setup_edit.php?field=action_status&id="+val.id+"&";
				return false;						  
			});
			
			$("#del_"+val.id).live( "click", function(){
				//message.html("loading ... <img src='../images/loaderA32.gif' >");
				if( confirm(" Are you sure you want to delete this action status ")) {
					jsDisplayResult("info", "info", "Deleting action status . . . <img src='../images/loaderA32.gif' >");
					$.post( "controller.php?action=changeActionstatus", { id:val.id, status:2 }, function( delData  ) {
						if( delData == 1 ) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Action Status deleted");
							//message.html("Action Status deleted").animate({opacity:"0.0"},4000);	
						} else if( delData == 0 ) {
							jsDisplayResult("info", "info", "No changes were made to the status");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again");
						}						
					});  			
				} else {
						message.html("");
				}
				return false;									  
			});
			
			$("#act_"+val.id).live( "click", function(){
				jsDisplayResult("info", "info", "Updating action status ... <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=activateactionstatus", { id:val.id }, function( actData ) {
					if( actData == 1 ) {
						jsDisplayResult("ok", "ok", "Status activated . . .");
					} else if( actData == 0 ) {
						jsDisplayResult("info", "info", "No changes we made . . .");
					} else {
						jsDisplayResult("error", "error", "Error saving action status");
					}															
				}); 
				return false;									  
			});
			
		
		});
	}, "json");
	
	/**
		Adding a new action status 
	**/
	$("#add_actionstatus").click( function() {
		var message 	= $("#actionstatus_message");
		var name 		= $("#actionstatus").val();
		var client_term = $("#action_client_term").val();
		var color 		= $("#action_color").val();		
		if ( name == "" ) {
			message.html("");
			jsDisplayResult("info", "info", "Please enter the action status name");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving action status . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=newActionStatus", 
				   { name			: name,
				     client_term	: client_term,
					 color			: color
					}, function( retData ) {
						if( retData > 0 ) {
						
						$("#actionstatus").val("");
						$("#action_client_term").val("")
						$("#action_color").val("");
						jsDisplayResult("ok", "ok", "Addition saved successfully.");
						
						$("#action_status_table")
							.append($("<tr />")
								.append($("<td />",{html:retData}))		  
								.append($("<td />",{html:name}))		  
								.append($("<td />",{html:client_term}))		  
								.append($("<td />")
									.append($("<span />",{html:color}).css({"background-color":"#"+color, "padding":"5px"}))
								)		  
								.append($("<td />")
								  .append($("<input />",{type:"submit",name:"edit_", value:"Edit", id:"edit_"+retData}))
								  .append($("<input />",{type:"submit",name:"del_", value:"Del", id:"del_"+retData}))
								)		  
								.append($("<td />",{html:"<b>Active</b>"})
								  //.append($("<input />",{type:"submit",name:"act_", value:"Activate", id:"act_"}))
								  //.append($("<input />",{type:"submit",name:"deact_", value:"Inactivate", id:"deact_"}))
								)
								.append($("<td />")
									.append($("<input />",{type:"submit", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))		  
								)								
							)					
						$("#change_status"+retData).live("click", function(){
							document.location.href = "change_action_status.php?id="+retData+"&";		
							return false
						});		
					} else {
						jsDisplayResult("error", "error", "Error saving  action status");
					}
			});	
		}
		return false;
	});

	
	$("#edit_actionstatus").click( function() {
		
		var message 	= $("#editactionstatus_message");
		var name 		= $("#actionstatus").val();
		var client_term = $("#action_client_term").val();
		var color 		= $("#action_color").val();		
		if ( name == "" ) {
			jsDisplayResult("error", "error", "Please enter the action status name");
			message.html("");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating action status . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateActionStatus", 
			   {
				 id				: $("#action_status_id").val(),
				 name			: name,
				 client_term	: client_term,
				 color			: color
				}, function( retData ) {
					if( retData == 1 ) {
						//jsDisplayResult("ok", "ok", "Action status successifully updated ..");						
						document.location.href = 'action_status.php?r[]=ok&r[]=Edit+saved+successfully.';
					} else if( retData == 0 ){
						jsDisplayResult("info", "info", "No changes found.");
					} else {
						jsDisplayResult("error", "error", "Error updating the action staus");
					}
			},"json");	
		}
		return false;								  
	});	
	
	$("#cancel_actionstatus").click(function(){
		history.back();
		return false;								   
	});
	
	$("#changes_actionstatus").click(function() {
		var message 	= $("#editactionstatus_message");											  
		jsDisplayResult("info", "info", "Updating status ... <img src='../images/loaderA32.gif' >");
		$.post( "controller.php?action=changeActionstatus",
		{
			id		: $("#action_status_id").val(), 
			status 	: $("#action_status_status :selected").val()
		}, function( deactData ){
		if( deactData == 1 ) {
			//jsDisplayResult("ok", "ok", "Status changes . . . ");
			document.location.href = 'action_status.php?r[]=ok&r[]=Status+change+saved+successfully.';
		} else if( deactData == 0 ) {
			jsDisplayResult("info", "info", "No changes were made. . . ");
		} else {
			jsDisplayResult("info", "info", "Error updating the action status . .. ");
		}														
		});
	  return false;									  
	});
	
});


	

	

	
