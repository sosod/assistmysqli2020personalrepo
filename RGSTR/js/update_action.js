// JavaScript Document
$(function(){

	$("#update_status").live("change", function() {
		if( $(this).val() == 3 ) {
		 $("#update_approval").removeAttr("disabled");
		 $("#update_progress").val("100")
		} else {
		 $("#update_progress").val("")	
		 $("#update_approval").attr("disabled","disabled");		
		}												  
	});
	
	$("#show_action_table").html("");
	$.post("controller.php?action=getUpdateAction", { id:$("#action_id").val() }, function( actionData ){
		$.each( actionData.action, function( index, action){
			$("#goback").before($("<tr />",{id:"action_detail"})
				.append($("<th />",{html:actionData.header[index]+":"}).css({"text-align":"left"}))
				.append($("<td />",{html:(index == "progress" ? action+"%" : action )}))			
			)
		});
	},"json");		   
	
	$("table#show_action_table").find("th").css({"text-align":"left"})
	


	$("#cancel_action_update").click(function(){
		history.back();
		return false;
	})
	
	   
	   $(".remove").live("click", function(){
		 var id 	  = this.id;
		 var original = $(this).attr("title");
		  if( confirm("Are you sure you want to delete this attachment"))
		  {
			  $.post("controller.php?action=removeUpdateActionFile", { id : id , original : original}, function( response ){
				  $(".messages").remove();
				  if( response.error)
				  {
					  $("#li_"+id).html("Error removing the file");
					  $("#attachment_list").prepend("<li class='messages'><div class='ui-state-error ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
				  } else {
					  $("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
					  $("#parent_"+id).fadeOut();
				  }			  
			  },"json");
		  }  
		 return false;  
	   });
	   
	   $(".justremove").live("click", function(){
		   
		   var id 		= $(this).attr("id")
		   var filename = $(this).attr("title");
		   $.post("controller.php?action=justRemoveActionFile",{ id : id, file : filename }, function( response ){
		    $(".messages").remove();
			 if( response.error )
			 {
				 $("#attachment_list").prepend("<li class='messages'><div class='ui-state-error ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
			 } else {
				 $("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
				 $("#li_"+id).fadeOut();
			 }		   
		   },"json");
		  return false; 
	   });
	    	
})

    function ajaxFileUpload( fileupload )
	{
        var panelId  = $(fileupload).attr("id");
		$("#loading")
		.ajaxStart(function(){
			//$("#file_loading").html("Uploading . . . <img src='../images/bar180.gif' />");
		})
		.ajaxComplete(function(){
			//$(this).html("");
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=processUpdateActionAttachments',
				secureuri	  : false,
				fileElementId : panelId,
				dataType	  : 'json',
				success       : function (response, status)
				{
					$(".newuploads").remove()
					$(".messages").remove();
					if(!response.error)
					{
						if( $("#attachment_list").length == 0){
							$("#file_loading").append($("<ul />",{id:"attachment_list"}))
						}
						$(".message").remove();
						$("#attachment_list").prepend("<span class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></span>")
						if( $.isEmptyObject(response.filesuploaded) ){
							//$("#file_loading").append( "" )
						} else{
							$.each( response.filesuploaded, function(index, file){
								$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='"+file+"' id='"+index+"' class='justremove'><span></span>Remove</a>" )
							});
						}
						
					} else {
						$("#file_loading").html(" Error uploading  "+response.error);
					}
				},
				error: function (data, status, e)
				{
					$("#file_loading").html(" Ajax error "+e);
				}
			}
		)
		return false;
	}