$.widget("ui.riskassurance", {
	
	options		: {
		risk_id			: 0,
		start			: 0,
		limit			: 10,
		current			: 1,
		total			: 1,
		assuranceObj	: {}, 
		tableId			: "riskassurance_"+(Math.floor(Math.random(454*34) + 4)),
		month			: [],
		toDay			: "",
		objectName		: "Risk",
		actionName		: "Action"
	} , 
	
	
	_init			: function()
	{
		this._getRiskAssurance();		
	} , 
	
	_create				: function()
	{
		this.options.month = { 1  : "Jan", 2  : "Feb", 3  : "Mar", 4  : "Apr", 5  : "May",
				   6  : "Jun", 7  : "Jul", 8  : "Aug", 9  : "Sep", 10 : "Oct",
				   11 : "Nov", 12 : "Dec"
				};	
		var today = new Date();
		var min   = today.getMinutes();
		if( min < 10){
		min = "0"+min;				
		} else{
		min = min;
		}
		this.options.toDay	  = today.getDate()+"-"+this.options.month[today.getMonth()+1]+"-"+today.getFullYear()+" "+today.getHours()+":"+min;		
		$(this.element).append($("<table />",{id:this.options.tableId}))		
	} , 
	
	_getRiskAssurance	: function()
	{
		var self = this;	
		$.getJSON("controller.php?action=getRiskAssurances",{
			start	: self.options.start,
			limit	: self.options.limit,
			id		: self.options.risk_id
		},function( responseData ){

			$("#"+self.options.tableId).html("");
			self.options.total = responseData.total;
			self._displayPaging( responseData.total );
			self._displayHeaders();
			if( $.isEmptyObject( responseData.data) )
			{
				$("#"+self.options.tableId).append($("<tr />")
				   .append($("<td />",{html:"There are no "+self.options.objectName+" assurance yet", colspan:8}))		
				)				
			} else {
				self._display( responseData.data, responseData.attachments  );
			}
			self._addNew();
		});
	} , 
	
	_display			: function( riskassurance, attachments )
	{
		var self = this;
		$.each(riskassurance, function( index , assurance){
			$("#"+self.options.tableId).append($("<tr />", {id:"tr_"+assurance.id})
			 .append($("<td />",{html:assurance.id}))
			 .append($("<td />",{html:self.options.toDay}))
			 .append($("<td />",{html:assurance.date_tested}))
			 .append($("<td />",{html:assurance.response}))
			 .append($("<td />",{html:(assurance.signoff == 1 ?  "Yes" : "No")}))
			 .append($("<td />",{html:assurance.assurance_provider}))
			 //.append($("<td />",{html:assurance.attachment}))
			 .append($("<td />")
			   .append($("<input />",{type:"button", name:"edit_"+assurance.id, value:"Edit", id:"edit_"+assurance.id}))	
			   .append($("<input />",{type:"button", name:"del_"+assurance.id, id:"del_"+assurance.id, value:"Del"}))  			
			  )
			 .hover(function(){
				 $(this).addClass("tdhover")				 
			 }, function(){
				 $(this).removeClass("tdhover")
			 })
			)
			
			$("#del_"+assurance.id).live("click", function(){
				if( confirm("Are you sure you want to delete this "+self.options.objectName+" assurance")){
					jsDisplayResult("info", "info", "Deleting "+self.options.objectName+" assurance . . <img src='../images/loaderA32.gif' />" );
					$.post("controller.php?action=deleteRiskAssurance", { id:assurance.id }, function( response ){
						//$("#newassurance_message").slideDown().html("Risk assurance deleted");
						if( !response.error ){
							jsDisplayResult("ok", "ok", response.text );
							$("#tr_"+assurance.id).fadeOut();																	  
						} else {
							jsDisplayResult("error", "error", response.text );
						}																  
					},"json")
				}									 			
				return false;
			});			
			
			
			$("#edit_"+assurance.id).live("click", function(){
				if( $("#editdialog").length > 0)
				{	
					$("#editdialog").remove();
				}
				
				var atta = "";
				if( !$.isEmptyObject(attachments[assurance.id]))
				{
					atta += "<ul id='attachment_list'>";
						$.each( attachments[assurance.id], function( index, val){
							var dotLocation = val.indexOf(".");
							var ext = val.substring( dotLocation + 1);
							
							var file = index+".".ext;
							atta += "<li id='li_"+index+"'><span><a href='download.php?folder=risk&file="+file+"&new="+val+"&content="+ext+"&company='>"+val+"</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><a href='#' id="+index+" title="+val+" class='remove remove_attach'>Remove</a></span></li>";
						});
					atta += "</ul>";
				}
				
				
				$("<div />",{id:"editdialog"})
				 .append($("<table />")
				   .append($("<tr />")
					 .append($("<th />",{html:"Response:"}))
					 .append($("<td />")
					   .append($("<textarea />",{cols:"30", rows:"7", name:"response", id:"response", value:assurance.response}))		 
					 )
				   )	
				   .append($("<tr />")
					   .append($("<th />",{html:"Sign Off:"}))
					   .append($("<td />")
						 .append($("<select />",{id:"signoff", name:"signoff"})
						   .append($("<option />",{value:"0", text:"No", selected:(assurance.signoff == 0 ? "selected" : "" )}))
						   .append($("<option />",{value:"1", text:"Yes", selected:(assurance.signoff == 1 ? "selected" : "" )}))
						 )	   
					   )
					 )			   
				   .append($("<tr />")
					 .append($("<th />",{html:"Date Tested:"}))
					 .append($("<td />")
					   .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:assurance.date_tested, readonly:"readonly"}).addClass("datepicker"))		 
					 )
				   )
				   /*.append($("<tr />")
					  .append($("<th />",{html:"Attachment"}))
					  .append($("<td />")
						.append($("<div />")
						  .append($("<span />",{id:"file_uploading"}))
						  .append($("<span />",{html:atta}))
						)
						.append($("<input />",{type:"file", name:"aassuranceattachment", id:"aassuranceattachment"})) 	  
					  )
				   )*/
				   .append($("<tr />")
					 .append($("<td />",{colspan:"2"})
						.append($("<div />",{id:"messagediv", width:"100%"}).addClass("ui-state")
						  .append($("<span />",{id:"messageicon"}))
						  .append($("<span />",{id:"messagecontent"}))
						)
					 )	   
				   )			   
				 ).dialog({
					 		autoOpen	: true, 
					 		modal		: true,
					 		title		: "Edit "+self.options.objectName+" Assurance",
					 		buttons		: {
					 						"Update"		: function()
					 						{
					 							
					 							$("#messagediv").addClass("ui-state-info")
					 							$("#messageicon").addClass("ui-icon").addClass("ui-icon-info")				 							
					 							if( $("#response").val() == "")
					 							{
					 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
					 								$("#messagecontent").html("Please enter the assurance response . . ");
					 							} else if( $("#date_tested").val() == "") {
					 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
					 								$("#messagecontent").html("Please enter the assurance date tested . . ");
					 							} else {
					 								$("#messagecontent").html("Updating  . . .<img src='../images/loaderA32.gif' />")
													$.post("controller.php?action=updateRiskAssurance", {
														   id			:  assurance.id,
														   risk_id 		:  self.options.risk_id,
														   date_tested	:  $("#date_tested").val(),
														   response		:  $("#response").val(), 
														   signoff		:  $("#signoff :selected").val()	
													}, function( response ) {
														//$("#newassurance_message").slideDown().html("Risk assurance deleted");
														if( !response.error ){
															jsDisplayResult("ok", "ok", response.text );
								 							self._getRiskAssurance();
														} else {
															jsDisplayResult("error", "error", response.text );
														}							
							 							$(this).dialog("destroy");
							 							$("#editdialog").remove();
													},"json")
					 							}
					 						} , 
					 						 "Cancel"	: function()
					 						{
					 							$(this).dialog("destroy");
					 							$("#editdialog").remove();
					 						}
				 						  }				 
				 });
				
				$("#aassuranceattachment").live("change", function(){
					$("#file_uploading").html("")				
					$("#file_uploading")
					.ajaxStart(function(){
						$("#file_uploading").html("Uploading . . . <img src='../images/loaderA32.gif' />");
					})
					.ajaxComplete(function(){
						//$(this).html("");
					});

					$.ajaxFileUpload
					(
						{
							url			  : 'controller.php?action=updateAssuranceAttachment',
							secureuri	  : false,
							fileElementId : 'aassuranceattachment',
							dataType	  : 'json',
							success       : function (response, status)
							{	
								$("#file_uploading").html("");
								$(".messages").remove();
								$(".newuploads").remove();
								if(!response.error)
								{
									if( $("#attachment_list").length == 0){
										$("#file_uploading").append($("<ul />",{id:"attachment_list"}))
									}				
									$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></li>")
									//$("#file_loading").html("<a hre=''>"+response.filename+" successfully uploaded <br /></span>")
										//		 .css({"color":"blue"})
									if( $.isEmptyObject(response.filesuploaded) ){
										//$("#file_loading").append( "" )
									} else { 
										//$("#file_loading").append($("<ul />",{id:"files"}))
										$.each( response.filesuploaded, function(index, file){
											$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"'  title='"+file+"' class='remove'><span></span>Remove</a>" )
											//$("#files").append("<li id='li_"+index+"'>"+file+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"' class='remove'><span></span>Remove</a></ul>")
											//.append($("<br />"))
										});							
									}
															
								}else
								{
									$("#file_loading").html(" Error uploading  "+response.error);
								}
							},
							error: function (data, status, e)
							{
								$("#file_loading").html(" Ajax error "+e);
							}
						}
					)
					return false;
				});				
				
				
				$(".remove").live("click", function() {
					var id   = $(this).attr("id");
					var file = $(this).attr("title")
					$.post("controller.php?action=deleteAssuranceAttachment",{
						id 		: id, 
						file 	: file
					}, function( response ){
						$(".messages").remove();
						if( !response.error ){							
							$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
							$("#li_"+id).fadeOut();
						} else {
							$("#attachment_list").prepend("<li class='messages'><div class='ui-state-error ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-closethick' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
						}
					},"json");	
					return false;
				});
				
				$(".datepicker").datepicker({	
					showOn			: "both",
					buttonImage 	: "/library/jquery/css/calendar.gif",
					buttonImageOnly	: true,
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "dd-M-yy"
				});	
				return false;
			});
			
		});
	} , 
	
	_displayHeaders		: function()
	{
		var self = this;
		$("#"+self.options.tableId).append($("<tr />")
		   .append($("<th />",{html:"Ref"}))
		   .append($("<th />",{html:"Saved On"}))
		   .append($("<th />",{html:"Date Tested"}))
		   .append($("<th />",{html:"Response"}))
		   .append($("<th />",{html:"Sign Off"}))
		   .append($("<th />",{html:"Assurance Provider"}))
		   //.append($("<th />",{html:"Attachment"}))
		   .append($("<th />",{html:"&nbsp;"}))
		)
	} , 
	
	_displayPaging		: function( total )
	{
		var self 	= this;	
		var pages;
		if( total%self.options.limit > 0) {
			pages = Math.ceil( total/self.options.limit )
		} else {
			pages = Math.floor( total/self.options.limit )
		}
		$("#"+self.options.tableId)
		 .append($("<tr />")
			.append($("<td />",{colspan:7})
			   .append($("<input />",{type:"button", id:"first", name:"first", value:" |< "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"previous", name:"previous", value:" < "}))			   
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{align:"center", html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))	   			
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", id:"next", name:"next", value:" > "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"last", name:"last", value:" >| "}))		 			   
			 )	   
		  )
	       if(self.options.current < 2)
	       {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
	       }
	       if((self.options.current == pages || pages == 0))
	       {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
	       }	
			$("#next").bind("click", function(){
				self._getNext( self );
			});
			$("#last").bind("click",  function(){
				self._getLast( self );
			});
			$("#previous").bind("click",  function(){
				self._getPrevious( self );
			});
			$("#first").bind("click",  function(){
				self._getFirst( self );
			});
	} , 
	_getNext  			: function( $this ) {
		$this.options.current   = $this.options.current+1;
		$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
		this._getRiskAssurance();
	},	
	
	_getLast  			: function( $this ) {
		console.log( "Last clicked ");
		
		$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
		$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
		this._getRiskAssurance();
	},
	
	_getPrevious    	: function( $this ) {
		$this.options.current   = parseFloat( $this.options.current ) - 1;
		$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
		this._getRiskAssurance();			
	},
	
	_getFirst  			: function( $this ) {
		$this.options.current   = 1;
		$this.options.start 	= 0;
		this._getRiskAssurance();				
	},		
	
	_addNew				: function()
	{
		var self = this;
		$("#"+self.options.tableId).append($("<tr />")
		  .append($("<td />",{colspan:7})
			 .append($("<input />",{type:"button", name:"add_new", id:"add_new", value:"Add New"}))	  
		  )		
		)
		
		$("#add_new").live("click", function(){
			if( $("#add_new_dialog").length > 0)
			{	
				$("#add_new_dialog").remove();
			}
			
			$("<div />",{id:"add_new_dialog"})
			 .append($("<table />")
			   .append($("<tr />")
				 .append($("<th />",{html:"Response:"}).addClass("top left"))
				 .append($("<td />")
				   .append($("<textarea />",{cols:"50", rows:"7", name:"response", id:"response"}))		 
				 )
			   )	
			   .append($("<tr />")
				   .append($("<th />",{html:"Sign Off:"}).addClass("top left"))
				   .append($("<td />")
					 .append($("<select />",{id:"signoff", name:"signoff"})
					   .append($("<option />",{value:"0", text:"No"}))
					   .append($("<option />",{value:"1", text:"Yes"}))
					 )	   
				   )
				 )			   
			   .append($("<tr />")
				 .append($("<th />",{html:"Date Tested:"}).addClass("top left"))
				 .append($("<td />")
				   .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", value:"", readonly:"readonly"}).addClass("datepicker"))		 
				 )
			   )
			  /* .append($("<tr />")
				  .append($("<th />",{html:"Attachment"}).addClass("top left"))
				  .append($("<td />")
				    .append($("<span />",{id:"file_uploading"}))
					.append($("<input />",{type:"file", name:"aassuranceattachment", id:"aassuranceattachment"})) 	  
				  )
			   )*/
			   .append($("<tr />")
				 .append($("<td />",{colspan:"2"})
					.append($("<div />",{id:"messagediv", width:"100%"}).addClass("ui-state")
					  .append($("<span />",{id:"messageicon"}))
					  .append($("<span />",{id:"messagecontent"}))
					)
				 )	   
			   )			   
			 ).dialog({
				 		autoOpen	: true, 
				 		modal		: true,
				 		title		: ""+self.options.objectName+" Assurance",
						width		: "auto",
						dialogClass	: "dialogButtons",
				 		buttons		: {
				 						"Save"		: function()
				 						{
				 							
				 							$("#messagediv").addClass("ui-state-info")
				 							$("#messageicon").addClass("ui-icon").addClass("ui-icon-info")				 							
				 							if( $("#response").val() == "")
				 							{
				 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
				 								$("#messagecontent").html("Please enter the assurance response . . ");
				 							} else if( $("#date_tested").val() == "") {
				 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
				 								$("#messagecontent").html("Please enter the assurance date tested . . ");
				 							} else {
				 								$("#messagecontent").html("Saving  . . .<img src='../images/loaderA32.gif' />")
												$.post("controller.php?action=newRiskAssurance", {
													   id 			:  self.options.risk_id,
													   date_tested	:  $("#date_tested").val(),
													   response		:  $("#response").val(), 
													   signoff		:  $("#signoff :selected").val()	
												}, function( response ){
													//$("#newassurance_message").slideDown().html("Risk assurance deleted");
													if( !response.error ){
							 							$("#messagediv").addClass("ui-state-ok")
							 							$("#messageicon").addClass("ui-icon-ok")
							 							$("#messagecontent").html("Saving  . . .")
														jsDisplayResult("ok", "ok", response.text );
							 							self._getRiskAssurance();
													} else {
														jsDisplayResult("error", "error", response.text );
													}							
						 							$(this).dialog("destroy");
						 							$("#add_new_dialog").remove();
												},"json")
				 							}
				 						} , 
				 						 "Cancel"	: function()
				 						{
				 							$(this).dialog("destroy");
				 							$("#add_new_dialog").remove();
				 						}
			 						  }				 
			 });
			

			$("#aassuranceattachment").live("change", function(){
				$("#file_uploading").html("")				
				$("#file_uploading")
				.ajaxStart(function(){
					$("#file_uploading").html("Uploading . . . <img src='../images/loaderA32.gif' />");
				})
				.ajaxComplete(function(){
					//$(this).html("");
				});

				$.ajaxFileUpload
				(
					{
						url			  : 'controller.php?action=saveAssuranceAttachment',
						secureuri	  : false,
						fileElementId : 'aassuranceattachment',
						dataType	  : 'json',
						success       : function (response, status)
						{	
							$("#file_uploading").html("");
							if(!response.error)
							{
								if( $("#attachment_list").length == 0){
									$("#file_uploading").append($("<ul />",{id:"attachment_list"}))
								}				
								$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></li>")
								//$("#file_loading").html("<a hre=''>"+response.filename+" successfully uploaded <br /></span>")
									//		 .css({"color":"blue"})
								if( $.isEmptyObject(response.filesuploaded) ){
									//$("#file_loading").append( "" )
								} else { 
									//$("#file_loading").append($("<ul />",{id:"files"}))
									$.each( response.filesuploaded, function(index, file){
										$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"'  title='"+file+"' class='remove'><span></span>Remove</a>" )
										//$("#files").append("<li id='li_"+index+"'>"+file+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"' class='remove'><span></span>Remove</a></ul>")
										//.append($("<br />"))
									});							
								}
														
							}else
							{
								$("#file_loading").html(" Error uploading  "+response.error);
							}
						},
						error: function (data, status, e)
						{
							$("#file_loading").html(" Ajax error "+e);
						}
					}
				)
				return false;
			});
			
			
			$(".datepicker").datepicker({	
				showOn			: "both",
				buttonImage 	: "/library/jquery/css/calendar.gif",
				buttonImageOnly	: true,
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "dd-M-yy"
			});	
		});
		
		
	}			
	
	
});
