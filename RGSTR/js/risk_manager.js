// JavaScript Document
$(function(){
	/**
		Get all the users 
	**/
	$.get( "controller.php?action=getNonRiskManagerUsers", function( data ) {		
		$.each( data, function( index, val) {
			$("#risk_manager_users")
				.append($("<option />",{text:val.tkname+"  "+val.tksurname, value:val.tkid}))
		});											   
	},"json")
	/**
		Get all risk managers
	**/
	$.getJSON("old_controller.php?action=getRiskManagers", function( riskMangerData ) {
		if( $.isEmptyObject(riskMangerData) ) {
			$("#risk_manager_table")
			 .append($("<tr />")
			   .append($("<td />",{colspan:"4", html:"No risk manger has been set yet"}))		   
			 )
		} else {
			$.each( riskMangerData, function( index, manager) {
			  	$("#risk_manager_table")
				 .append($("<tr />")
					.append($("<td />",{html:manager.tkid}))
					.append($("<td />",{html:manager.tkname+" "+manager.tksurname}))
					.append($("<td />")
						.append($("<input />",{type:"button", id:"edit_"+manager.tkid, value:"Delete"}))		  
					 )					
				  )
				 $("#edit_"+manager.tkid).live("click", function(){
					//document.location.href = "edit_risk_manager.php?id="+manager.tkid;												 
					if(confirm("Are you sure you wish to remove "+manager.tkname+" "+manager.tksurname+" as a Risk Manager?")==true) {
						$.get("controller.php?action=delRiskManager&id="+manager.tkid,function(data) {
							//if(data[0]=="ok") {
								document.location.href = "risk_manager.php?r[]="+data[0]+"&r[]="+data[1];
							//}
						},"json");
					}
					return false;
				 });	 
			});
		}
		/*$("#risk_manager_table")
			 .append($("<tr />")
				.append($("<td />",{colspan:"3"})
				  .append($("<a />",{href:"javascript:history.back()", text:"Go Back "}))		  
				 )	   
			  )*/
	});
	
	
	$("#update_risk_manager").click(function() {
		var message = $("#risk_manager_message")								 
		var user  	= $("#risk_manager_users :selected");
		
		if( user == "" ) {
			message.html("Please select a user to set as risk manger");
			return false;
		} else {
			$.post( "old_controller.php?action=setRiskManager", { 
				   	id		: user.val() ,
					status  : 1 
				}, function( retData ) {
				if( retData == 1 ){																		
/*					message.html(user.text()+" has been set as risk manger");
					$("#risk_manager_users option[value='']").attr("selected", "selected");
					$("#risk_manager_table")
					 .append($("<tr />")
						.append($("<td />",{html:user.val()}))
						.append($("<td />",{html:user.text()}))
						.append($("<td />")
							.append($("<input />",{type:"button", id:"edit_"+user.val(), value:"Edit"}))		  
						 )					
					  )
					 $("#edit_"+user.val()).live("click", function(){
						document.location.href = "edit_risk_manager.php?id="+user.val();												 
						return false;
					 });
*/
document.location.href = "risk_manager.php?r[]=ok&r[]="+user.text()+" has been set as a risk manager.";				 
				} else if( retData == 0 ) {
					message.html("There was no change made to this user");
				} else {
					message.hml("There was an error updating user details , please try again");
				}
			},"json")		
		}
		return false;
	});
	
	$("#save_changes").live("click", function(){
			var message = $("#edit_manager_message");										
			$.post( "old_controller.php?action=setRiskManager", { 
				   id	   : $("#userid").val() ,  
				   status  : $("#risk_manager :selected").val()	
				   }, function( retData ) {
				if( retData == 1 ){																		
					message.html("Risk manager updated ");			
					history.back();
				} else if( retData == 0 ) {
					message.html("Thre was no change made to this user");
				} else {
					message.hml("There was an error updating user details , please try again");
				}
			},"json")	
		return false;
	});
	
	$("#cancel_changes").live("click", function(){
		history.back();
		return false;										
	});
	
})