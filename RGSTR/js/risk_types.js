// JavaScript Document
$(function(){
	/**
	Get all tisk types
	**/
	var used = [];
	$.getJSON( "controller.php?action=getUsedTypes", function( usedTypes ) {
		$.each( usedTypes , function( index, val){
				used.push(val.type);					 
		});
	});

	$.get( "controller.php?action=getRiskType", function( data ) {

		var message = $("#risk_type_message")														 
		message.html("")
		$.each( data , function( index, val ) {
			$("#risk_type_table")
			  .append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.shortcode}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+val.id, name:"typeedit_"+val.id }))
				  .append($("<input />",{type:"submit", value:"Del", id:"typedel_"+val.id, name:"typedel_"+val.id }))
				 )
				.append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				  //.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"typeactive_"+val.id, name:"typeactive_"+val.id }))
				  //.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ), value:"Inactivate", id:"typedeactive_"+val.id, name:"typedeactive_"+val.id }))							
				)
				.append($("<td />")
					.append($("<input />",{type:"submit", id:"change_"+val.id, name:"change_"+val.id, value:"Change Status"}))		  
				)
			  )
			$("#typeedit_"+val.id).live( "click", function() {
				document.location.href = "edit_risk_type.php?id="+val.id+"&";
				return false;										  
			});
			
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_type_status.php?id="+val.id+"&";
				return false;										  
			});
			
			$("#typedel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this risk type ")) {
					jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' >");
					$.post( "controller.php?action=changetypestatus", 
						   {id		:	val.id,
						   	status  : 2
						   }, function( typDelData ) {
						if( typDelData == 1) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Risk Type deleted . . . ");
						} else if( typDelData == 0) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("info", "info", "No change was made to the risk type . . . ");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again . . . ");
						}										 
					});
				}
				return false;										  
			});
			$("#typeactive_"+val.id).live( "click", function() {
				jsDisplayResult("info", "info", "Activating  . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=typeactivate", {id:val.id}, function( typDeactData ) {
					if( typDeactData == 1) {
						jsDisplayResult("ok", "ok", "Risk Type activated . . . ");
					} else if( typDeactData == 0) {
						jsDisplayResult("info", "info", "No change was made to the risk type . . . ");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again . . . ");
					}										 
				})															 
				return false;										  
			});

			
		});
	},"json");
	
	/**
		adding a new risk type
	**/
	$("#add_risktype").click( function() {
		var message   	= $("#risk_type_message");
		var shortcode 	= $("#risktype_shortcode").val();
		var type 		= $("#risktype").val();
		var description	= $("#risktype_descri").val();
		
		if ( shortcode == "" ) {
			jsDisplayResult("error", "error", "Please the short code for this risk type . . . ");
			return false;
		} else if( type == "") {
			jsDisplayResult("error", "error", "Please enter the risk type . . . ");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=newRiskType", 
				   { 
					   shortcode	: shortcode, 
					   name			: type, 
					   description	: description 
				   }, function( retData ) {
				   if( retData > 0 ) {
					   
					 $("#risktype_shortcode").val("");  
					 $("#risktype").val("");
					 $("#risktype_descri").val("");
					 jsDisplayResult("ok", "ok", "Risk type successfully saved . . . ");
					 $("#risk_type_table")
					  .append($("<tr />",{id:"tr_"+retData})
						.append($("<td />",{html:retData}))
						.append($("<td />",{html:shortcode}))
						.append($("<td />",{html:type}))
						.append($("<td />",{html:description}))
						.append($("<td />")
						  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+retData, name:"typeedit_"+retData }))
						  .append($("<input />",{type:"submit", value:"Del", id:"typedel_"+retData, name:"typedel_"+retData }))			  
						 )
						.append($("<td />",{html:"<b>Activess</b>"})
						  //.append($("<input />",{type:"submit", value:"Inactivate", id:"typedeactive", name:"typedeactive" }))							
						)
						.append($("<td />")
						    .append($("<input />",{type:"submit", id:"change_"+retData, name:"change_"+retData, value:"Change Status"}))
						)
					  )
                       
					 $("#change_"+retData).live( "click", function() {
						document.location.href = "change_type_status.php?id="+retData+"&";
						return false;										  
					});

                    $("#typeedit_"+retData).live( "click", function() {
                        document.location.href = "edit_risk_type.php?id="+retData+"&";
                        return false;
                    });

                    $("#typedel_"+retData).live( "click", function() {
                        if( confirm(" Are you sure you want to delete this risk type ")) {
                        	jsDisplayResult("info", "info", "Deleting  . . . <img src='../images/loaderA32.gif' >");
                        	$.post( "controller.php?action=changetypestatus",
                                   {id		:	retData,
                                    status  : 2
                                   }, function( typDelData ) {
                                if( typDelData == 1) {
                                    $("#tr_"+retData).fadeOut();
                                    jsDisplayResult("ok", "ok", "Risk Type deleted . . . ");
                                } else if( typDelData == 0) {
                                    $("#tr_"+retData).fadeOut();
                                    jsDisplayResult("info", "info", "No change was made to the risk type  . . . ");
                                } else {
                                    jsDisplayResult("error", "error", "Error saving , please try again  . . . ");
                                }
                            });
                        }
                        return false;
                    });
                       
			   } else {
				   jsDisplayResult("error", "error", "There was an error trying to save the risk type, please try again . . . ");
				}
			  
			});
		}
		return false;
	});
	
	$("#edit_risktype").click(function(){
		var message   	= $("#edit_risk_type");
		var shortcode 	= $("#risktype_shortcode").val();
		var type 		= $("#risktype").val();
		var description	= $("#risktype_descri").val();
		
		if ( shortcode == "" ) {
			jsDisplayResult("error", "error", "Please the short code for this risk type . . . ");
			return false;
		} else if( type == "") {
			jsDisplayResult("error", "error", "Please enter the risk type . . . ");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateRiskType", 
		   { 
			   id			: $("#risktype_id").val(), 
			   shortcode	: shortcode, 
			   name			: type, 
			   description	: description 
		   }, function( retData ) {
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Risk type successfully updated . . . ");
					message.html(" .. ");						
				} else if( retData == 0 ){
					jsDisplayResult("info", "info", "Risk type not changed . . . ");
				} else {
					jsDisplayResult("error", "error", "Error updating the risk type. . . ");
				}
			});
		}
		return false;
		});

	$("#cancel_risktype").click(function(){
		history.back();
		return false;								   
	})	
	
});
