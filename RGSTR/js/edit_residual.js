// JavaScript Document
$(function(){
	
	$("#edit_residual").click(function(){
									   
		var message 		= $("#residual_risk_message")						
		var residual_from 	= $("#residual_risk_from").val();
		var residual_to 	= $("#residual_risk_to").val();
		var residual		= $("#residual_magnitude").val();
		var response  		= $("#residual_response").val();
		var color 			= $("#residual_color").val();
	
		if ( residual_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this residual risk . . . ");
			return false;
		} else if( residual_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this residual risk. . . ");			
			return false;
		} else if ( residual == "" ) {
			jsDisplayResult("error", "error", "Please enter the magnitude for this residual risk . . . ");
			return false;
		} else if( response == "" ) {
			jsDisplayResult("error", "error", "Please enter the response for this residual risk . . . ");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateResidualRisk", 
				   {
					   id		: $("#residual_id").val(),
					   from		: residual_from,
					   to		: residual_to,
					   magn		: residual,
					   resp		: response,
					   clr		: color
					}
				   , function( retData ) {
						if( retData == 1 ) {
							jsDisplayResult("ok", "ok", "Residual risk successfully updated . . . ");	
						} else if( retData == 0 ){
							jsDisplayResult("info", "info", "Residual risk not changed . . . ");
							message.html("");
						} else {
							jsDisplayResult("error", "error", "Error updating the residual risk . . . ");
						}
			},"json")	
		}
		return false;	 
	});
	
	$("#change_residual").click(function(){
		jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' >");										 
		$.post( "controller.php?action=changeResidualStatus", 
			   {
				   id  		: $("#residual_id").val(),
				   status   : $("#residual_status :selected").val() 	   
				}, function( deactData ) {
			if( deactData == 1){
				jsDisplayResult("ok", "ok", "Residual risk status changed . . . ");
			} else if( deactData == 0){
				jsDisplayResult("info", "info", "No change was made to the residual risk . . . ");
			} else {
				jsDisplayResult("error", "error", "Error saving , please try again . . . ");
			}									 
		})									
		return false;											 
	});
	
	$("#cancel_residual").click(function(){
		history.back();
		return false;		 
	});
});