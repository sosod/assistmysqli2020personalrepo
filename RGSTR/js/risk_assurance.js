// JavaScript Document
$(function(){

var object_name = window.risk_object_name;
var action_name = window.action_object_name;

	$(".datepicker").live("focus",function(){
		$(this).datepicker({ changeMonth:true, changeYear:true , dateFormat:"d-M-yyyy"});							 
	});

	$.get( "controller.php?action=getRiskAssuranceWithHeaders", { id:$("#risk_id").val() }, function( riskD ){
		var riskData = riskD.riskData;
		var headers = riskD.headers;
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_item+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.id}))		  							
			)

			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_type+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.type}))		  				
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_level+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.level}))		  				
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_category+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.category}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_description+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.description}))		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_background+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.background}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.impact+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.impact}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.impact_rating+":"}).css({"text-align":"left"}))		  
				.append($("<td />")
				 .append($("<span />",{html:"&nbsp;&nbsp;"+riskData.impact_rating+"&nbsp;&nbsp;"}).css("background-color",riskData.impact_color))		  
				)		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.likelihood+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.likelihood}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.likelihood_rating+":"}).css({"text-align":"left"}))		  
				.append($("<td />")
				 .append($("<span />",{html:"&nbsp;&nbsp;"+riskData.likelihood_rating+"&nbsp;&nbsp;"}).css("background-color",riskData.like_color))							
				)		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.current_controls+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.current_controls}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.percieved_control_effective+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.effectiveness}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.control_rating+":"}).css({"text-align":"left"}))		  
				.append($("<td />")
				  .append($("<span />",{html:"&nbsp;&nbsp;"+riskData.control_effectiveness_rating+"&nbsp;&nbsp;"}).css("background-color",riskData.effect_color))							
				 )		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.financial_exposure+":"}).css({"text-align":"left"}))		  
				.append($("<td />")
				  .append($("<span />",{html:"&nbsp;&nbsp;"+(riskData.financial_exposure == null ? "" : riskData.financial_exposure)+"&nbsp;&nbsp;"}).css("background-color",""))							
				 )		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.actual_financial_exposure+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.actual_financial_exposure}))		  		  
			)		
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.kpi_ref+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.kpi_ref}))		  		  
			)					
			$("#goback").before($("<tr />")
				.append($("<th />",{html:headers.risk_owner+":"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:riskData.risk_owner}))		  		  
			)
			//.append($("<tr />")
				//.append($("<th />",{html:"UDF's:"}))		  
				//.append($("<td />",{html:""}))		  		  
			//)
			$("#goback").before($("<tr />")
				.append($("<td />",{colspan:"2"})
					.append($("<input />",{type:"submit", value:"View "+window.action_object_name+"(s) associated with this "+window.risk_object_name, id:"view_actions_"+riskData.id}))		
				)
				
			)
			
			$("#view_actions_"+riskData.id).live("click", function(){
				document.location.href = "view_actions.php?id="+riskData.id;
				return false;
			});
	},"json");
	/*
	$.get( "controller.php?action=getRiskAssurances", {id : $("#risk_id").val() }, function( assuranceData ){
		var message 	= $("#newassurance_message")																							
		var currentDate = new Date();
		console.log( assuranceData );
		var today = currentDate.getFullYear()+"-"+currentDate.getMonth().toString()+1+"-"+currentDate.getDate() ;
		if( $.isEmptyObject( assuranceData ) ) {
		}else {
		$.each( assuranceData , function( index, val){
			$("#list_of_risk_assurance")
				.append($("<tr />",{ id:"tr_"+val.id})
					.append($("<td />",{html:today}))
					.append($("<td />",{html:val.date_tested}))
					.append($("<td />",{html:val.response}))
					.append($("<td />",{html:(val.signoff==1 ? "Yes" : "No")}))
					.append($("<td />",{html:val.tkname+" "+val.tksurname+" - "+val.value}))				
					.append($("<td />")
						.append($("<input />",{type:"submit", name:"edit_"+val.id, id:"edit_"+val.id, value:"Edit"}))	
						.append($("<input />",{type:"submit", name:"del_"+val.id, id:"del_"+val.id, value:"Del"}))	
						.append($("<input />",{type:"submit", name:"view_attach_"+val.id, id:"view_attach_"+val.id, value:"View Attach"		}))	
						
					)					
				)
			
			$("#del_"+val.id).live("click", function(){
				if( confirm("Are you sure you want to delete this risk assurance")){
					$("#tr_"+val.id).fadeOut();
				}									 
				return false;										 
			})
			
			$("#edit_"+val.id).live("click", function(){
				$("#riskassurance_response").val( val.response )
				$("select#riskassurance_signoff option[value="+val.signoff+"]").attr("selected", "selected")				
				$("#date_tested").val( val.date_tested )	
				$("#add_newRisk_assurance").val("Update").attr("id", "update_Risk_assurance").attr("name", "update_Risk_assurance")
				$("#new_risk_assurance").show();
				return false;								 
			});
			
			$("#update_Risk_assurance").live("click", function(){
				$.post("controller.php?action=updateRiskAssurance", 
					{
					   risk_id		:  $("#risk_id").val(),
					   id 			:  val.id,
					   date_tested	:  $("#date_tested").val(),
					   response		:  $("#riskassurance_response").val(), 
					   signoff		:  $("#riskassurance_signoff").val(),
					   attachment	:  $("#riskassurance_attachment").val()	   
					}, function( response ){
						if( response == 1){
							message.html("Update Successifull")	
						} 	else {
							message.html("Error Updating")	
						}	
					}  
				,"json")	
				return false;
			});
			
		});
		}

		
		$("#newriskassurance").live("click",function(){
			$("#new_risk_assurance").show();
			return false;
		});
	},"json") */
	/*
	$("#save").live("click", function(){
													   
		var message 	= $("#newassurance_message")
		var response  	= $("#riskassurance_response").val();
		var signoff  	= $("#riskassurance_signoff :selected").val();
		var attachment 	= $("#riskassurance_attachment").val();	
		var date_tested	= $("#date_tested").val();			
		
		if( response == "" ) {
			message.html(" Please enter the response for the assurance")
			return false;
		} else if( signoff == "") {
			message.html(" Please slected th signoff for the assurance")
			return false;
		} else {
			$.post("controller.php?action=newRiskAssurance", {
				   id 			:  $("#risk_id").val(),
				   date_tested	:  date_tested,
				   response		:  response, 
				   signoff		:  signoff,
				   attachment	:  attachment				   
				   }, function( riskAssure ){
					if( riskAssure > 0 ){
						$("#riskassurance_response").val("");	
						$("select#riskassurance_signoff options[value='']").attr("selected","selected");
						$("#riskassurance_attachment").val("");
						
						var dateTested = date_tested.split("/");
						var currentDate = new Date();
						$("#list_of_risk_assurance")
						 .append($("<tr />")
							.append($("<td />",{html:currentDate.getFullYear()+"-"+currentDate.getMonth()+1+"-"+currentDate.getDay()}))
							.append($("<td />",{html: dateTested[2]+"-"+dateTested[0]+"-"+dateTested[1] }))
							.append($("<td />",{html:response}))
							.append($("<td />",{html:$("#riskassurance_signoff :selected").text()}))	
							.append($("<td />",{html:""}))
							.append($("<td />",{html:""}))
							.append($("<td />",{html:$("#risk_id").val()}))							
						  )
						
						message.html("Risk assurance saved successifully ...")	
					} else {
						message.html("There was an error saving  risk assurance")		
					}
			});		
		}
		return false;
	}); */
	
		/**
		Add another browse button to attach files
	**/
	$("#attach_another").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	
	$("#cancel_newRisk_assurance").live("click", function(){
		$("#new_risk_assurance").fadeOut();
		return false;
	});
	
})