// JavaScript Document
$(function(){
		   
	$("#report-header-form").find("input[type='checkbox']").attr("checked","checked");

	$("#sortable").sortable({placeholder:"ui-state-highlight"});
	$("#sortable").disableSelection();

	$(".datepicker").live( "focus",function(){
		$(this).datepicker({changeMonth:true, changeYear:true, dateFormat:"yy-m-d"})
	});
	

    $("#quick_report").live("change", function(){
        document.location.href = "process_report_.php?generate_quick_report=1&id="+$(this).val();
    });
    
	$("#r_checkAll").click(function(){
		//$("#report-header-form").find("input[type='checkbox']").not(".inaction").attr("checked","checked");
        $(".queryf").attr("checked", "checked");
		return false;
	});

	$("#r_uncheckAll").click(function(){
		//$("#report-header-form").find("input[type='checkbox']").not(".inaction").attr("checked","");
        $(".queryf").attr("checked", "");
		return false;
	});

    $("#r_invert").click(function(){
       $(".queryf").each(function(){
            if( $(this).is(":checked")){
	            $(this).attr("checked","");
            } else {
                $(this).attr("checked","checked");
            }
        });
		return false;
	});

	$("#a_checkAll").click(function(){
		$(".actionf").attr("checked","checked");
		return false;
	});

	$("#a_uncheckAll").click(function(){
		$(".actionf").attr("checked","");
		return false;
	});

	$("#a_invert").click(function(){
        $(".actionf").each(function(){

            if( $(this).is(":checked")){
	            $(this).attr("checked","");
            } else {
                $(this).attr("checked","checked");
            }

        });
		return false;
	});
		
	/**
		get all the risk types
	**/
	$.get( "controller.php?action=getRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#_type").live("change", function() {
		$("#_category").html("");
		//$("#_category").append($("<option />",{value:"", text:"--risk category--"}))
		$.get( "controller.php?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#_category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});

      $.get( "controller.php?action=getCategories", function( data ) {
        $.each( data ,function( index, val ) {
            $("#_category")
            .append($("<option />",{text:val.name, value:val.id}));
        })
    },"json");

      $.get( "controller.php?action=getRiskStatus", function( data ) {
        $.each( data ,function( index, val ) {
            $("#_risk_status")
            .append($("<option />",{text:val.name, value:val.id}));
        })
    },"json");

    //=======================================================================================
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getImpact", function( data ) {
		var impactRating = [];
		$.each( data ,function( index, val ) {
			$("#_impact")
			.append($("<option />",{text:val.assessment, value:val.id}));
		})			
	},"json");
	/**
		Get the risk impact ratings
	**/
	$("#_impact").live( "change" ,function(){
		$("#_impact_rating").html("");
		//$("#_impact_rating").append($("<option />",{value:"", text:"--impact rating--"}))
		$.get( "controller.php?action=getImpactRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#_impact_rating")
					.append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#_impact_rating")
					.append($("<option />",{value:i, text: i}))	
				}
			}
		}, "json");
		return false;
	});
 	/**
		get all the risk likelihoods
	**/
	$.get( "controller.php/?action=getRisklevel", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_level")
			.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");

 	/**
		get all the risk likelihoods
	**/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_financial_exposure")
			.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");

	/**
		get all the risk likelihoods
	**/
	$.get( "controller.php/?action=getLikelihood", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_likelihood")
			.append($("<option />",{text:val.assessment, value:val.id}));
		})											 
	},"json");
	/**
		Get the risk likelihood ratings
	**/
	$("#_likelihood").live( "change" ,function(){
		$("#_likelihood_rating").html("");
		$("#_likelihood_rating").append($("<option />",{value:"", text:"--likelihood rating--"}))
		$.get( "controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#_likelihood_rating")
					.append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#_likelihood_rating")
					.append($("<option />",{value:i, text:i}))	
				}
			}
		}, "json");
		return false;
	});	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getControl", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_percieved_control")
			.append($("<option />",{text:val.effectiveness, value:val.id}));
		})											 
	},"json");	
	
	/**
		Get the risk control effectiveness ratings
	**/
	$("#_percieved_control_effectiveness").live( "change" ,function(){
		$.get( "controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
				$("#_control_effectiveness_rating")
					.append($("<option />",{value:data.rating, text:data.rating }))
		}, "json");
		return false;
	});		
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#_risk_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
			
			$("#_action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})											 
	},"json");	

	$("#generate_report").click(function(){
		var messge 		= $("#generate_report_messsage").slideDown("fast");		
		var riskAction  = ( ($("#risk_action").is(":checked")) ? "checked" : "") ;
		var noAction 	= "";
		$(".inaction").each(function(){
			if( $(this).is(":checked") ){
			  noAction = "Yes";
			}							 
		});
		
		
		var sorting =  $("#sortable").sortable("serialize");
		messge.html("Please wait ... genereating report .....  <img src='../images/loaderA32.gif' />")
		$.post( "controller.php?action=generateReport" ,
			   {
				   headers 			: $("#report-header-form").serializeArray(),
				   values  			: $("#report-values-form").serializeArray(),
				   grouping			: $("#grouping-options-form").serializeArray(),
				   documentformat	: $("#document-format-form").serializeArray(),
				   generate			: $("#generate-report-form").serializeArray(),
				   includeaction 	: riskAction,
				   noAction 		: noAction
				} , function( resultS ){
									
				$("#searchBox").hide("slow")							
			
				$("#showResults")
				.append($("<h1 />",{html:"Title : "+$("#report_title").val() }))
				
				$("#showResults")
				 .append($("<table />",{id:"searchResults"}))					
				 
				var th = $("<tr />");
				$.each( resultS.headers , function( index, headerObject){
					th.append($("<th />",{html:headerObject}));							   
				});	
				$("#searchResults").append( th );
				//
				if( $.isEmptyObject( resultS.data ) ){
					$("#searchResults").append($("<tr />")
					 .append($("<td />",{colspan:(resultS.headers).length,html:"There were no matching results found for this report"}))							
					);
				} else {
					$.each( resultS.data , function( i, val ) {
						var tr = $("<tr />")
						$.each( val, function( m, value ){
						 tr.append($("<td />",{html:value}))						  
						})					   
						$("#searchResults").append(tr);
					});					
				}
					
				$("#searchBox").scrollTop();
							
				/*$("#searchBox").slideUp("slow")							
				$("#showResults")
				 .append($("<table />",{id:"searchResults"}))
				 
				 $.each( searchResults, function( index , valueObject ){
					$.each( valueObject, function( i, val){
						 $("<tr />").append($("<th />",{html:val}))	
						$(tr).append($("<td />",{html:val}))
					});	
					$("#searchResults").append(tr);
				});*/
											
		},"json");		
		return false;
	});
	
	
});