// JavaScript Document
$(function(){
	RiskColumns.get();

	$("#table_riskcolumns>tbody").sortable({
		items 	: "tr:not(.disabled)"		
	}).disableSelection();
});


RiskColumns		= {
		
	get			: function()
	{
		$.getJSON("old_controller.php?action=risk_columns", function( responseData ){
			$.each(responseData, function( index, column ){
				$("#table_riskcolumns").append($("<tr />")
				   .append($("<td />",{html:""+column.client_terminology}))
				   .append($("<td />")
					  .append($("<input />",{type:"checkbox", id:"manage_"+column.id, name:"manage", value:column.id}))	   
				   )
				   .append($("<td />")
					  .append($("<input />",{type:"checkbox", id:"new_"+column.id, name:"new",value:column.id}))	   
				   )
				   .append($("<input />",{type:"hidden", id:"position_"+column.id, name:"position", value:column.id})) 
				)
				if((column.active & 4) == 4)
				{
   				   $("#manage_"+column.id).attr("checked", "checked");
				} 
				if((column.active & 8) ==8)
				{    
				   $("#new_"+column.id).attr("checked", "checked");
				}    				
			   //$("#manage_"+column.id).attr("checked", ((column.active & 4) == 4 ? "checked" : ""));
			   //$("#new_"+column.id).attr("checked", ((column.active & 8) == 8 ? "checked" : "")) ;
			});
			
			$("#table_riskcolumns").append($("<tr />").addClass("disabled")
			   .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
				    .append($("<input />",{type:"button", name:"save_changes", id:"save_changes", value:" Save Changes"}))	   
			   )		
			)
			
			$("#save_changes").click(function(){
				var data = {};
				jsDisplayResult("info", "info", "Updating columns ... <img src='../images/loaderA32.gif' >");
				$.post("old_controller.php?action=updateColumns",{
					data	: $("#riskcolumns_form").serializeArray(),
					type	: "RiskColumns"
				}, function( response ){
					if( response.error  ) {
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text);
					}					
				},"json");
				
				
				return false;
			});
			
		});
	}	
		
};
