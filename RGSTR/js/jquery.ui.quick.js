$.widget("ui.quick", {
	
	options			: {
			start		: 0,
			limit		: 10,
			current		: 1,
			total		: 0,
			tableId		: "quick_"+(Math.floor(Math.random(34) * 23 ))
	}  ,
	
	_init			: function()
	{
		this._getQuickReports();		
	} ,
	
	_create			: function()
	{		
		//$(this.element).append($("<table />",{id:this.options.tableId}))
	} , 
	
	_getQuickReports	: function()
	{
		var self = this;
		$.getJSON("controller.php?action=getQuickReports", {
			start	: self.options.start,
			limit	: self.options.limit
		}, function( quickReports ){
			if($.isEmptyObject(quickReports))
			{
				$("#quickreporttable").append($("<tr />")
				    .append($("<td />",{colspan:"7",html:"There are no quick reports yet"}))		
				)
			} else {
				self._display(quickReports);
			}
		});		
	} ,
	
	_display			: function( reports )
	{
		var self = this;
		$.each( reports, function( index , report){
			var tr = $("<tr />",{id:"tr_"+report.id})
			tr.append($("<td />",{html:report.id}))
			tr.append($("<td />",{html:report.name}))
			tr.append($("<td />",{html:report.description}))
			tr.append($("<td />",{html:"On Screen"}))
			tr.append($("<td />")
			   .append($("<input />",{
				   					   type		: "button",
				   					   name		: "generate_"+report.id,
				   					   id		: "generate_"+report.id,
				   					   value	: "Generate"
			   }))		
			)
			tr.append($("<td />")
			   .append($("<input />",{
				   					  type		: "button",
				   					  name		: "edit_"+report.id,
				   					  id		: "edit_"+report.id,
				   					  value		: "Edit"
			   }))		
			)
			tr.append($("<td />")
			   .append($("<input />",{
				   					  type		: "button",
				   					  name		: "del_"+report.id,
				   					  id		: "del_"+report.id,
				   					  value		: "Del"
				   				}))		
			)
			
			$("#generate_"+report.id).live("click", function(){
				document.location.href = "process_report.php?id="+report.id;
				return false;
			});
			
			$("#edit_"+report.id).live("click", function(){
				document.location.href = "edit_quick_report.php?id="+report.id;
				return false;
			});			
			
			$("#del_"+report.id).live("click", function(){
				if(confirm("Are you sure you want to delete this report"))
				{
					jsDisplayResult("info", "info", "Deleting report . . . <img src='../images/loaderA32.gif' >");
					$.post("controller.php?action=deleteReport",{
						id 	: report.id 						
					}, function( response ){
						if( response.error)
						{
							jsDisplayResult("error", "error", response.text );
						} else {
							$("#tr_"+report.id).fadeOut();
							jsDisplayResult("ok", "ok", response.text );
						}
					},"json");					
				}
				return false;
			});			
				
			
			$("#quickreporttable").append( tr );
		});		
	} , 
	
	_displayPaging		: function( total )
	{

	}				
	
	
});