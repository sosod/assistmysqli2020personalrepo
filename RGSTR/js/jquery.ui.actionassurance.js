// JavaScript Document
$.widget("ui.actionassurance", {

	options 		: {
		action_id	: "",
		start		: 0,
		limit		: 10,
		current		: 1,
		total		: 1,
		assuranceObj: {}, 
		tableId		: "assurance_"+(Math.floor(Math.random(56*4)+3)),
		month		: [],
		toDay		: ""
	} , 
	
	_init		: function(){	
		this._getActionAssurance();
	} , 
	
	_create		: function(){
		this.options.month = { 1  : "Jan", 2  : "Feb", 3  : "Mar", 4  : "Apr", 5  : "May",
				   6  : "Jun", 7  : "Jul", 8  : "Aug", 9  : "Sep", 10 : "Oct",
				   11 : "Nov", 12 : "Dec"
				};	
		var today = new Date();
		var min   = today.getMinutes();
		if( min < 10){
		min = "0"+min;				
		} else{
		min = min;
		}
		this.options.toDay	  = today.getDate()+"-"+this.options.month[today.getMonth()+1]+"-"+today.getFullYear()+" "+today.getHours()+":"+min;		
		$(this.element).append($("<table />",{id:this.options.tableId}))
	} , 
	
	_getActionAssurance	: function(){
		$this = this;
		$.getJSON("controller.php?action=getActionAssurances", { 
				  id 	: $this.options.action_id,
				  start : $this.options.start,
				  limit : $this.options.limit				  
		}, function( actionAssurances ) {
			
			$("#"+$this.options.tableId).html("");
			$this.options.total = actionAssurances .total;
			$this._displayPager( actionAssurances .total );
			$this._displayHeaders();
			if( $.isEmptyObject(actionAssurances.data) )
			{
				$("#"+$this.options.tableId).append($("<tr />")
				  .append($("<td />",{colspan:7, html:"There are no action assurances yet"}))		
				)
			} else{ 
				$this._displayActionAssurances( actionAssurances.data, actionAssurances.attachment);
			}
			$this._addNew();
		});
		
	} ,
	
	_displayActionAssurances		: function( actionassurance , attachments )
	{
		var self = this;
		$.each( actionassurance, function( index, assurance){
			$("#"+self.options.tableId).append($("<tr />", {id:"tr_"+assurance.id})
			 .append($("<td />",{html:assurance.id}))
			 .append($("<td />",{html:self.options.toDay}))
			 .append($("<td />",{html:assurance.date_tested}))
			 .append($("<td />",{html:assurance.response}))
			 .append($("<td />",{html:(assurance.signoff == 1 ?  "Yes" : "No")}))
			 .append($("<td />",{html:assurance.assurance_provider}))
			 .append($("<td />",{html:assurance.attachment}))
			 .append($("<td />")
			   .append($("<input />",{type:"button", name:"edit_"+assurance.id, value:"Edit", id:"edit_"+assurance.id}))	
			   .append($("<input />",{type:"button", name:"del_"+assurance.id, id:"del_"+assurance.id, value:"Del"}))  			
			  )	
			 .hover(function(){
				 $(this).addClass("tdhover")				 
			 }, function(){
				 $(this).removeClass("tdhover")
			 })			  
			)
			
			
			$("#del_"+assurance.id).live("click", function(){
				if( confirm("Are you sure you want to delete this risk assurance")){
					jsDisplayResult("info", "info", "Deleting risk assurance . . <img src='../images/loaderA32.gif' />" );
					$.post("controller.php?action=deleteActionAssurance", { id:assurance.id }, function( response ){
						//$("#newassurance_message").slideDown().html("Risk assurance deleted");
						if( !response.error ){
							jsDisplayResult("ok", "ok", response.text );
							$("#tr_"+assurance.id).fadeOut();																	  
						} else {
							jsDisplayResult("error", "error", response.text );
						}																  
					},"json")
				}									 			
				return false;
			});			
			
			
			$("#edit_"+assurance.id).live("click", function(){
				if( $("#editactiondialog").length > 0)
				{
					$("#editactiondialog").remove();
				}
				var att = "";
				if( !$.isEmptyObject( attachments[assurance.id] ))
				{
					att += "<ul id='attachment_list'>";
						$.each( attachments[assurance.id], function( index, val) {
							var dotLocation = val.indexOf(".");
							var ext = val.substring( dotLocation + 1);
							var file = index+".".ext;
							att += "<li id='li_"+index+"'><span><a href='download.php?folder=action&file="+file+"&new="+val+"&content="+ext+"&company='>"+val+"</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><a href='#' id="+index+" title="+val+" class='remove remove_attach'>Remove</a></span></li>";
						})
					att += "</ul>";
				}

				$("<div />",{id:"editactiondialog"})
				 .append($("<table />")
				   .append($("<tr />")
					 .append($("<th />",{html:"Response:"}))
					 .append($("<td />")
					   .append($("<textarea />",{cols:"30", rows:"7", name:"response", id:"response", value:assurance.response}))		 
					 )
				   )	
				   .append($("<tr />")
					   .append($("<th />",{html:"Sign Off:"}))
					   .append($("<td />")
						 .append($("<select />",{id:"signoff", name:"signoff"})
						   .append($("<option />",{value:"0", text:"No", selected:(assurance.signoff == 0 ? "selected" : "" )}))
						   .append($("<option />",{value:"1", text:"Yes", selected:(assurance.signoff == 1 ? "selected" : "" )}))
						 )	   
					   )
					 )			   
				   .append($("<tr />")
					 .append($("<th />",{html:"Date Tested:"}))
					 .append($("<td />")
					   .append($("<input />",{type:"text", name:"datetested", id:"datetested", value:assurance.date_tested, readonly:"readonly"}).addClass("datepicker"))		 
					 )
				   )
				   .append($("<tr />")
					  .append($("<th />",{html:"Attachment"}))
					  .append($("<td />")
					  .append($("<div />")
						  .append($("<span />",{id:"file_uploading"}))
						  .append($("<span />",{html:att}))
						)							  
						.append($("<input />",{type:"file", name:"aassuranceattachment", id:"aassuranceattachment"})) 	  
					  )
				   )
				   .append($("<tr />")
					 .append($("<td />",{colspan:"2"})
						.append($("<div />",{id:"messagediv", width:"100%"}).addClass("ui-state")
						  .append($("<span />",{id:"messageicon"}))
						  .append($("<span />",{id:"messagecontent"}))
						)
					 )	   
				   )			   
				 ).dialog({
					 		autoOpen	: true, 
					 		modal		: true,
					 		title		: "Edit Action Assurance",
					 		buttons		: {
					 						"Update"		: function()
					 						{
					 							
					 							$("#messagediv").addClass("ui-state-info")
					 							$("#messageicon").addClass("ui-icon").addClass("ui-icon-info")				 							
					 							if( $("#response").val() == "")
					 							{
					 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
					 								$("#messagecontent").html("Please enter the assurance response . . ");
					 							} else if( $("#datetested").val() == "") {
					 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
					 								$("#messagecontent").html("Please enter the assurance date tested . . ");
					 							} else {
					 								$("#messagecontent").html("Updating  . . .<img src='../images/loaderA32.gif' />")
													$.post("controller.php?action=updateActionAssurance", {
														   id			:  assurance.id,
														   action_id	:  self.options.action_id,
														   date_tested	:  $("#datetested").val(),
														   response		:  $("#response").val(), 
														   signoff		:  $("#signoff :selected").val()	
													}, function( response ) {
														//$("#newassurance_message").slideDown().html("Risk assurance deleted");
														if( !response.error ){
															jsDisplayResult("ok", "ok", response.text );
								 							self._getActionAssurance();
														} else {
															jsDisplayResult("error", "error", response.text );
														}							
							 							$(this).dialog("destroy");
							 							$("#editactiondialog").remove();
													},"json")
					 							}
					 						} , 
					 						 "Cancel"	: function()
					 						{
					 							$(this).dialog("destroy");
					 							$("#editactiondialog").remove();
					 						}
				 						  }				 
				 });
				
				
				$("#aassuranceattachment").live("change", function(){
					$("#file_uploading").html("")				
					$("#file_uploading")
					.ajaxStart(function(){
						$("#file_uploading").html("Uploading . . . <img src='../images/loaderA32.gif' />");
					})
					.ajaxComplete(function(){
						//$(this).html("");
					});

					$.ajaxFileUpload
					(
						{
							url			  : 'controller.php?action=updateActionAssuranceAttachment',
							secureuri	  : false,
							fileElementId : 'aassuranceattachment',
							dataType	  : 'json',
							success       : function (response, status)
							{	
								$("#file_uploading").html("");
								$(".messages").remove();
								$(".newuploads").remove();
								if(!response.error)
								{
									if( $("#attachment_list").length == 0){
										$("#file_uploading").append($("<ul />",{id:"attachment_list"}))
									}				
									$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></li>")
									//$("#file_loading").html("<a hre=''>"+response.filename+" successfully uploaded <br /></span>")
										//		 .css({"color":"blue"})
									if( $.isEmptyObject(response.filesuploaded) ){
										//$("#file_loading").append( "" )
									} else { 
										//$("#file_loading").append($("<ul />",{id:"files"}))
										$.each( response.filesuploaded, function(index, file){
											$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"'  title='"+file+"' class='remove'><span></span>Remove</a>" )
											//$("#files").append("<li id='li_"+index+"'>"+file+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"' class='remove'><span></span>Remove</a></ul>")
											//.append($("<br />"))
										});							
									}
															
								}else
								{
									$("#file_loading").html(" Error uploading  "+response.error);
								}
							},
							error: function (data, status, e)
							{
								$("#file_loading").html(" Ajax error "+e);
							}
						}
					)
					return false;
				});						
				
				$(".remove").live("click", function() {
					var id   = $(this).attr("id");
					var file = $(this).attr("title")
					$.post("controller.php?action=deleteActionAssuranceAttachment",{
						id 		: id, 
						file 	: file
					}, function( response ){
						$(".messages").remove();
						if( !response.error ){							
							$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
							$("#li_"+id).fadeOut();
						} else {
							$("#attachment_list").prepend("<li class='messages'><div class='ui-state-error ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-closethick' style='float:left; margin-right:0.3em;'></span>"+response.text+"</div></li>")
						}
					},"json");	
					return false;
				});
				
				$(".datepicker").datepicker({	
					showOn			: "both",
					buttonImage 	: "/library/jquery/css/calendar.gif",
					buttonImageOnly	: true,
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "dd-M-yy"
				});	
				return false;
			});			
		});
		
	} , 
	
	_addNew			: function()
	{
		var self = this;
		$("#"+self.options.tableId).append($("<tr />")
		  .append($("<td />",{colspan:8})
			 .append($("<input />",{type:"button", name:"add_new", id:"add_new", value:"Add New"}))	  
		  )		
		)
		
		$("#add_new").live("click", function(){
			
			if( $("#addnewdialog").length > 0)
			{
				$("#addnewdialog").remove();
			}
			
			$("<div />",{id:"addnewdialog"})
			 .append($("<table />")
			   .append($("<tr />")
				 .append($("<th />",{html:"Response:"}))
				 .append($("<td />")
				   .append($("<textarea />",{cols:"30", rows:"7", name:"response", id:"response"}))		 
				 )
			   )	
			   .append($("<tr />")
				   .append($("<th />",{html:"Sign Off:"}))
				   .append($("<td />")
					 .append($("<select />",{id:"signoff", name:"signoff"})
					   .append($("<option />",{value:"0", text:"No"}))
					   .append($("<option />",{value:"1", text:"Yes"}))
					 )	   
				   )
				 )			   
			   .append($("<tr />")
				 .append($("<th />",{html:"Date Tested:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", name:"datetested", id:"datetested", readonly:"readonly"}).addClass("datepicker"))		 
				 )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Attachment"}))
				  .append($("<td />")
					.append($("<span />",{id:"file_uploading"}))
					.append($("<input />",{type:"file", name:"aassuranceattachment", id:"aassuranceattachment"})) 	  
				  )
			   )
			   .append($("<tr />")
				 .append($("<td />",{colspan:"2"})
					.append($("<div />",{id:"messagediv", width:"100%"}).addClass("ui-state")
					  .append($("<span />",{id:"messageicon"}))
					  .append($("<span />",{id:"messagecontent"}))
					)
				 )	   
			   )			   
			 ).dialog({
				 		autoOpen	: true, 
				 		modal		: true,
				 		title		: "Action Assurance",
				 		buttons		: {
				 						"Save"		: function()
				 						{
				 							
				 							$("#messagediv").addClass("ui-state-info")
				 							$("#messageicon").addClass("ui-icon").addClass("ui-icon-info")				 							
				 							if( $("#response").val() == "")
				 							{
				 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
				 								$("#messagecontent").html("Please enter the assurance response . . ");
				 							} else if( $("#datetested").val() == "") {
				 								$("#messageicon").removeClass("ui-infor").addClass("ui-icon-error")
				 								$("#messagecontent").html("Please enter the assurance date tested . . ");
				 							} else {
				 								$("#messagecontent").html("Saving  . . .<img src='../images/loaderA32.gif' />")
												$.post("controller.php?action=newAssuranceAction", {
													   id 			:  self.options.action_id,
													   date_tested	:  $("#datetested").val(),
													   response		:  $("#response").val(), 
													   signoff		:  $("#signoff :selected").val()	
												}, function( response ){
													//$("#newassurance_message").slideDown().html("Risk assurance deleted");
													if( !response.error ){
							 							$("#messagediv").addClass("ui-state-ok")
							 							$("#messageicon").addClass("ui-icon-ok")
														jsDisplayResult("ok", "ok", response.text );
							 							self._getActionAssurance();
													} else {
														jsDisplayResult("error", "error", response.text );
													}							
						 							$(this).dialog("destroy");
						 							$("#addnewdialog").remove();
												},"json")
				 							}
				 						} , 
				 						 "Cancel"	: function()
				 						{
				 							$(this).dialog("destroy");
				 							$("#addnewdialog").remove();
				 						}
			 						  }				 
			 });
			
			$("#aassuranceattachment").live("change", function(){
				$("#file_uploading").html("")				
				$("#file_uploading")
				.ajaxStart(function(){
					$("#file_uploading").html("Uploading . . . <img src='../images/loaderA32.gif' />");
				})
				.ajaxComplete(function(){
					//$(this).html("");
				});

				$.ajaxFileUpload
				(
					{
						url			  : 'controller.php?action=saveActionAssuranceAttachment',
						secureuri	  : false,
						fileElementId : 'aassuranceattachment',
						dataType	  : 'json',
						success       : function (response, status)
						{	
							$("#file_uploading").html("");
							if(!response.error)
							{
								if( $("#attachment_list").length == 0){
									$("#file_uploading").append($("<ul />",{id:"attachment_list"}))
								}				
								$("#attachment_list").prepend("<li class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></li>")
								//$("#file_loading").html("<a hre=''>"+response.filename+" successfully uploaded <br /></span>")
									//		 .css({"color":"blue"})
								if( $.isEmptyObject(response.filesuploaded) ){
									//$("#file_loading").append( "" )
								} else { 
									//$("#file_loading").append($("<ul />",{id:"files"}))
									$.each( response.filesuploaded, function(index, file){
										$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"'  title='"+file+"' class='remove'><span></span>Remove</a>" )
										//$("#files").append("<li id='li_"+index+"'>"+file+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"' class='remove'><span></span>Remove</a></ul>")
										//.append($("<br />"))
									});							
								}
														
							}else
							{
								$("#file_loading").html(" Error uploading  "+response.error);
							}
						},
						error: function (data, status, e)
						{
							$("#file_loading").html(" Ajax error "+e);
						}
					}
				)
				return false;
			});			
			
			$(".datepicker").datepicker({	
				showOn			: "both",
				buttonImage 	: "/library/jquery/css/calendar.gif",
				buttonImageOnly	: true,
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "dd-M-yy"
			});	
		});
		
		
	} , 
	
	_displayHeaders			: function()
	{ 
		var self = this;
		$("#"+self.options.tableId).append($("<tr />")
		   .append($("<th />",{html:"Ref"}))
		   .append($("<th />",{html:"System Date and Time"}))
		   .append($("<th />",{html:"Date Tested"}))
		   .append($("<th />",{html:"Response"}))
		   .append($("<th />",{html:"Sign Off"}))
		   .append($("<th />",{html:"Assurance Provider"}))
		   .append($("<th />",{html:"Attachment"}))
		   .append($("<th />",{html:"&nbsp;"}))
		)
	} ,
	
	
	_displayPager	: function( total ){
		var $this 	= this;	
		var pages;
		if( total%$this.options.limit > 0){
			pages = Math.ceil( total/$this.options.limit )
		} else {
			pages = Math.floor( total/$this.options.limit )
		}

		$("#"+$this.options.tableId)
		 .append($("<tr />")
			.append($("<td />",{colspan:"8"})
			   .append($("<input />",{type:"button", id:"first", name:"first", value:" |< "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"previous", name:"previous", value:" < "}))			   
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<span />",{align:"center", html:"Page "+$this.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages)}))	   			
			   .append("&nbsp;&nbsp;&nbsp;")
			   .append($("<input />",{type:"button", id:"next", name:"next", value:" > "}))
			   .append("&nbsp;")
			   .append($("<input />",{type:"button", id:"last", name:"last", value:" >| "}))		 			   
			 )	   
			//.append($("<td />",{colspan:($this.options.columDisplay == "none" ? "3" : "4")}))	   			
		  )
	       if($this.options.current < 2)
	       {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
	       }
	       if(($this.options.current == pages || pages == 0))
	       {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
	       }			 
			$("#next").bind("click", function(){
				$this._getNext( $this );
			});
			$("#last").bind("click",  function(){
				$this._getLast( $this );
			});
			$("#previous").bind("click",  function(){
				$this._getPrevious( $this );
			});
			$("#first").bind("click",  function(){
				$this._getFirst( $this );
			});			 
		} , 
		
		_getNext  			: function( $this ) {
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getActionAssurance();
		},	
		
		_getLast  			: function( $this ) {
			$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
			$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );			
			this._getActionAssurance();                                                                                  
		},
		
		_getPrevious    	: function( $this ) {
			$this.options.current   = parseFloat( $this.options.current ) - 1;
			$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
			this._getActionAssurance();			
		},
		
		_getFirst  			: function( $this ) {
			$this.options.current   = 1;
			$this.options.start 	= 0;
			this._getActionAssurance();				
		},
	
	_loader			: function(){
		$this 		  = this;
		var actiondiv = $("<div />",{id:"loading"});
		actiondiv.html("Loading actions . .  . <img src='../images/loaderA32.gif' />")
					.css({
						position  	: "absolute", 
						top 	  	: "200px;",
						left	  	: "250px",
						"z-index" 	: "999"
					})
		
		
	} 
		 
		 
})
