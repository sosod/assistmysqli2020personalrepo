$(function(){
	
	QuickReport.get();	
	
	$("#sortable").sortable({placeholder:"ui-state-highlight"});
	$("#sortable").disableSelection();
	
	$("#r_checkAll").click(function(){
        $(".queryf").attr("checked", "checked");
		return false;
	});

	$("#r_uncheckAll").click(function(){
        $(".queryf").attr("checked", "");
		return false;
	});
	
	$("#_type").live("change", function() {
		$("#_category").html("");
		//$("#_category").append($("<option />",{value:"", text:"--risk category--"}))
		$.get( "controller.php?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#_category").append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});	
	
	
	$("#_impact").live( "change" ,function(){
		$("#_impact_rating").html("");
		//$("#_impact_rating").append($("<option />",{value:"", text:"--impact rating--"}))
		$.get( "controller.php?action=getImpactRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#_impact_rating").append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#_impact_rating").append($("<option />",{value:i, text: i}))	
				}
			}
		}, "json");
		return false;
	});
	
	$("#_likelihood").live( "change" ,function(){
		$("#_likelihood_rating").html("");
		$("#_likelihood_rating").append($("<option />",{value:"", text:"--likelihood rating--"}))
		$.get( "controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
			if ( data.diff == 0 ) {
				$("#_likelihood_rating").append($("<option />",{value:data.to, text:data.to }))
			} else {
				for( i = data.from; i <= data.to; i++ ) {
					$("#_likelihood_rating").append($("<option />",{value:i, text:i}))	
				}
			}
		}, "json");
		return false;
	});		
	
	$("#_percieved_control_effectiveness").live( "change" ,function(){
		$.get( "controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
				$("#_control_effectiveness_rating").append($("<option />",{value:data.rating, text:data.rating }))
		}, "json");
		return false;
	});	
});


var QuickReport 		= {
		
		get			: function()
		{
			$.getJSON("controller.php?action=getAQuickReport", {
				id		: $("#reportid").val()
			}, function( responseData ){

				$("#report_description").text( responseData.report_description )
				$("#report_name").val( responseData.report_name )
				$("#report_title").val( responseData.report_title )
				//action headers
				$(".actionf").each(function( key ,value){
					var idKey = $(this).attr("id");
					if( responseData.aheader[idKey] == undefined)
					{
						$(this).attr("checked", "");
					} else {
						$(this).attr("checked", "checked");
					}
				});
				//risk headers
				$(".queryf").each(function( key ,value){
					var idKey = $(this).attr("id");
					if( responseData.header[idKey] == undefined)
					{								
						$("#"+idKey).attr("checked", "");				
					} else {
						$(this).attr("checked", "checked");
					}
				});
				
				$("#group_by option[value="+responseData.group_by+"]").attr("selected", "selected");
				
				$.each(responseData.match, function( key, value){
					$("#match_"+key+" option[value="+value+"]").attr("selected", "selected");
				});
				
				$.each( responseData.values, function( index, value ){
					$("#_"+index).val( value );
					$("#from").val( value['from'] );
					$("#to").val( value['to'] );					
					$("#_"+index+" option[value='"+value+"']").attr("selected", "selected")
				});
				
				var sortables = {
					__id				: "Risk Item",
					__type				: "Risk Type",
					__category			: "Risk Category",
					__description		: "Risk Description",
					__background		: "Risk Background",
					__financial_exposure: "Financial Exposure",
					__impact			: "Impact",
					__level				: "Level",
					__likelihood		: "Likelihood",
					__actual_financial_exposure : "Actual Financial Exposure",
					__kpi_ref			: "Kpi Ref",
					__current_controls	: "Current Controls"
				}			
				
				$.each( responseData.sort, function( index , value){
					$("#sortable").append($("<li />").addClass("ui-state-default")
									.append($("<span />",{id:value}).addClass("ui-icon").addClass("ui-icon-arrowthick-2-n-s").addClass("sort"))
									.append($("<input />",{type:"hidden", name:"sort[]", value:value}))
									.append(sortables[value])
					)					
				});
			})
	
		} 
}			