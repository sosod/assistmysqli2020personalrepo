// JavaScript Document
$(function(){

	/**
		Adding new user access
	**/
	$("#save_user").click(function() {
		var message 	 	= $("#useraccess_message").slideDown();
		var module_admin 	= $("#module_admin :selected");
		var create_risks 	= $("#create_risk :selected");	
		var create_actions  = $("#create_actions :selected");
		var view_all 		= $("#view_all :selected");		
		var edit_all 		= $("#edit_all :selected");
		var update_all 		= $("#update_all :selected");
		var reports 		= $("#reports :selected");	
		var assurance 		= $("#assurance :selected");
		var setup 			= $("#setup :selected");	
		jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");
		$.post( "old_controller.php?action=updateUserAccess", 
		{
		  user		: $("#user_id").val(),
		  modadmin	: module_admin.val(), 
		  cr_risk	: create_risks.val(),
		  cr_actions: create_actions.val(),
		  v_all		: view_all.val(),
		  e_all		: edit_all.val(),
		  u_all		: update_all.val(),
		  reports	: reports.val(),
		  assurance : assurance.val(),
		  setup		: setup.val()
		},
		function( retData ) {
			if( retData == 1 ) {
				//jsDisplayResult("ok", "ok", "User setting succefully updated");
				//history.back();
				document.location.href = "user_access.php?r[]=ok&r[]=Changes+saved+successfully";
			} else if( retData == 0){
				jsDisplayResult("info", "info", "There was no change to user setting");	
			} else {
				jsDisplayResult("error", "error", "There was an error trying to save the user");	
			}
		},"json" )	
		return false;							  
	});
	
	$("#cancel_changes").click(function(){
		history.back();
		return false;
	});
	
});