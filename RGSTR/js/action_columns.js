$(function(){
	
	ActionColumns.get();
	
	$("#tableaction_columns>tbody").sortable({
		items	: "tr:not(.disabled)"
	}).disableSelection();
});

ActionColumns		= {
		
	get			: function()
	{
		$.getJSON("old_controller.php?action=action_columns",function( responseData ){
			$.each( responseData, function( index, column){			
				$("#tableaction_columns").append($("<tr />")
				   .append($("<td />",{html:column.client_terminology}))
				   .append($("<td />")
					  .append($("<input />",{type:"checkbox", name:"manage", id:"manage_"+column.id, value:column.id, checked:((column.active & 4) == 4 ? "checked" : "")}))	   
				   )
				   .append($("<td />")
					   .append($("<input />",{type:"checkbox", name:"new", id:"new_"+column.id, value:column.id, checked:((column.active & 8) == 8 ? "checked" : "")}))	   
				   )
				   .append($("<input />",{type:"hidden", name:"position", value:column.id}))
				)
				if((column.active & 4) == 4)
				{
   				   $("#manage_"+column.id).attr("checked", "checked");
				} 
				if((column.active & 8) ==8)
				{    
				   $("#new_"+column.id).attr("checked", "checked");
				}    
			   //$("#manage_"+column.id).attr("checked", ((column.active & 4) == 4 ? "checked" : ""));
			   //$("#new_"+column.id).attr("checked", ((column.active & 8) == 8 ? "checked" : "")) ;				
				
			});			
			$("#tableaction_columns").append($("<tr />").addClass("disabled")
			   .append($("<td />",{colspan:"3"}).css({"text-align":"right"})
				  .append($("<input />",{type:"button", name:"save_changes", id:"save_changes", value:"Save Changes"}))	   
			   )		
			)
			
			$("#save_changes").click(function(){
				var data = {};
				jsDisplayResult("info", "info", "Updating columns ... <img src='../images/loaderA32.gif' >");
				$.post("old_controller.php?action=updateColumns",{
					data	: $("#actioncolumns_form").serializeArray()
				}, function( response ){
					if( response.error  ) {
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text);
					}					
				},"json");
				
				
				return false;
			});
		})
	
	}	
		
};
