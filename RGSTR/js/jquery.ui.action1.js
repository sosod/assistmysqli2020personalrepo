// JavaScript Document
$.widget("ui.action1", {
	
	options	: 	{
		tableId 		: "actions_"+(Math.floor(Math.random(56) * 34)),
		url				: "controller.php?action=getActions",
		location		: "",
		actionstart 	: 0,
		actionlimit		: 10,
		editAction		: false,
		updateAction	: false,
		approvalAction	: false,
		viewAssurance	: false,
		actiontotal		: 0,
		risk_id			: 0,
		actioncurrent	: 1,
		isOwners		: [], 
		section         : "",
		page            : "",
		viewAction      : "",
		financial_year  : 0,
		action_owner    : 0,
		action_id       : 0,
		objectName		: "Risk",
		actionName		: "Action"
	} , 
	
	_init			: function()
	{
		this._getActions();		
	} , 
    
    _create		: function()
	{
		var self = this;		
		if(self.options.risk_id !== 0)
		{
		   self.options.tableId = self.options.tableId+"__"+self.options.risk_id;
		}
        
		var html = [];
		html.push("<table class='noborder'>");
		  if((self.options.section == "admin" || self.options.section == "manage") && self.options.page !== "view")
		  {
/*		  html.push("<tr>");
		    html.push("<td class='noborder'>");
		        html.push("<table class='noborder'>");
		          html.push("<tr>");
		            html.push("<th style='text-align:left;'>Financial Year:</th>");
		            html.push("<td class='noborder'>");
		              html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
		                html.push("<option value=''>--please select--</option>");
		              html.push("</select>");
		            html.push("</td>")  
		          html.push("</tr>");
		          if(!(self.options.section === "manage" && self.options.page === "update_actions"))
		          {
		          html.push("<tr>");
		            html.push("<th style='text-align:left;'>Action Owner:</th>");
		            html.push("<td class='noborder'>");
		              html.push("<select id='action_owner' name='action_owner' style='width:200px;'>");
		                html.push("<option value=''>--please select--</option>");
		              html.push("</select>");
		            html.push("</td>")  
		          html.push("</tr>");
		          }
		          html.push("<tr>");
		            html.push("<th style='text-align:left;'>Query:</th>");
		            html.push("<td class='noborder'>");
		              html.push("<select id='query' name='query' style='width:200px;'>");
		                html.push("<option value=''>--please select--</option>");
		              html.push("</select>");
		            html.push("</td>")  
		          html.push("</tr>");
		          html.push("<tr class='descriptions' style='display:none;'>");
		            html.push("<td class='noborder' id='query_description' colspan='2'></td>");  
		          html.push("</tr>");
		        html.push("</table>");		      
		    html.push("</td>");
		  html.push("</tr>");*/
		  }
		  html.push("<tr>");
		    html.push("<td class='noborder'>");
		        html.push("<table id='"+self.options.tableId+"'></table>");		      
		    html.push("</td>");
		  html.push("</tr>");
		html.push("</table>");		
	    $(this.element).append(html.join(' '));
/*	    self._loadFinancialYears();
	    self._loadActionOwners();
	    self._loadQueries();
	    
	    $("#financial_year").live("change", function(){
	        if($(this).val() !== "")
	        {
	           self.options.financial_year = $(this).val();
	           self._getActions();
	        } else {
	           self.options.financial_year = 0;
	           self._getActions();
	        }
	    });
	    
	    $("#action_owner").live("change", function(){
	        if($(this).val() !== "")
	        {
	           self.options.action_owner = $(this).val();
	           self._getActions();
	        } else {
	            self.options.action_owner = 0;
	            self._getActions();
	        }
	    });	  
	    
	    $("#query").live("change", function(){
	        if($(this).val() !== "")
	        {
	           self.options.risk_id = $(this).val();
	           self._getActions();
               var longDescription = $("select#query :selected").attr("title");
               $(".descriptions").show();
               $("#query_description").addClass("ui-state-info").show().html(longDescription);
	        } else {
	           self.options.risk_id = 0;
	           self._getActions();
	        }
	    });	  */
	    
	} , 
	
	_loadFinancialYears     : function()
	{
        $.getJSON("controller.php?action=getFinancialYear",{status:1}, function(financialyears){
          $.each(financialyears, function(index, year){
              $("#financial_year").append($("<option />",{text:year.start_date+" - "+year.end_date, value:year.id}))
          });   
        }); 
	} ,
	
	_loadActionOwners              : function()
	{
		$.get("controller.php?action=getUsers", function( data ){
			$.each( data , function( index, val){
				$("#action_owner").append($("<option />",{value:val.tkid, text:val.user}))
			})
		},"json")	
	},
		
	_loadQueries              : function()
	{
	    var self = this;
		$.get("controller.php?action=getQueries", 
		{
		  financial_year: self.options.financial_year,
		  page          : self.options.page,
		  section       : self.options.section
	    }, function(data){
			$.each(data, function(index, val){
				$("#query").append($("<option />",{value:val.id, text:val.query_description, title:val.description}))
			})
		},"json")	
	},	
	
	_getActions		: function()
	{
		var self = this;
		//alert(self.options.objectName+" : "+self.options.actionName);
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
	     );		
		$.getJSON(self.options.location+""+self.options.url, {
			start 	        : self.options.actionstart,
			limit 	        : self.options.actionlimit,
			id		        : self.options.risk_id,
			section         : self.options.section,
			page            : self.options.page,
		    financial_year  : self.options.financial_year,
		    action_owner    : self.options.action_owner,
		    action_id       : self.options.action_id			
		}, function(actionData){
		    $("#loadingDiv").remove();
		    
			$("#"+self.options.tableId).html("");
			self.options.actiontotal =  actionData.total
			self._displayPaging( actionData.total, actionData.columns );
			self._displayHeaders(actionData.headers );
			if($.isEmptyObject(actionData.actions))
			{
			    var _html = [];
			    _html.push("<tr>");
			      _html.push("<td colspan='"+(actionData.columns +1)+"'>");
			         _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
			           _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
			           _html.push("<span style='padding:5px; float:left;'>"+actionData.action_message+"</span>");
			         _html.push("</p>");
			      _html.push("</td>");
			    _html.push("</tr>");
				$("#"+self.options.tableId).append(_html.join(' '));
			} else {
				self._display(actionData.actions, actionData.avgProgress, actionData.isOwner, actionData.actionStatus, actionData.columns);	
			}
		});
	} ,
	
	_display			: function(actions, avgProgress, isOwner, actionStatus, cols)
	{
		var self  = this;
		var count = 0;		
		var html  = []     
		$.each(actions , function(index , action){
		    if(isOwner.hasOwnProperty(index))
		    {
		        if(isOwner[index] || self.options.page=="assurance")
		        {
		          html.push("<tr id='tr_"+index+"'>");
		        } else {
		          html.push("<tr id='tr_"+index+"' class='view-notowner'>");
		        }
		    } else {
		       html.push("<tr id='tr_"+index+"' class='view-notowner'>");
		    }
		    $.each(action, function(key, val){
			    html.push("<td>"+val+"</td>");
		    });

			var disabledBtn = "";
			var awaitingApproval = false;
			var allowUpdate 	 = false;
			var actionOwner		 = false
			if(actionStatus.hasOwnProperty(index))
			{
			    if((actionStatus[index] & 8) == 8)
			    {
				    awaitingApproval    = true;
				    allowUpdate	     = true;
				    disabledBtn         = "disabled='disabled'";
			    } 			    
			}
			if(self.options.editAction)
			{   
			   if(!awaitingApproval)
			   {
		         html.push("<td>");
			     if(isOwner[index] || self.options.section == "admin")
			     {
			        html.push("<input type='button' name='edit_"+index+"' id='edit_"+index+"' value='Edit "+self.options.actionName+"' />");
			     } else {
			        html.push("<input type='button' name='edit_"+index+"' value='Edit "+self.options.actionName+"' disabled='disabled' />");
			     }
			     if(self.options.section == "admin")
			     {
			        html.push("<input type='button' name='del_"+index+"' id='del_"+index+"' value='Delete "+self.options.actionName+"' />");
			     } else {
			        //html.push("<input type='button' name='del_"+index+"' value='Delete Action' disabled='disabled' />");
			        
			     }	
			     html.push("</td>");
			    $("#del_"+index).live("click", function(){
				
				    if(confirm("Are you sure you want to delete this "+self.options.actionName+""))
				    {
					    jsDisplayResult("info", "info", "Deleting "+self.options.actionName+"...  <img src='../images/loaderA32.gif' />" );
					    $.post(self.options.location+"controller.php?action=deleteAction", {id:index},function( response ){
							//console.log(response);
						    if( response.error ){
							    jsDisplayResult("error", "error", response.text );							
						    } else {
							    $("#tr_"+index).fadeOut();
                                self._getActions();
							    jsDisplayResult("ok", "ok", response.text );
						    }
					    },"json");
				    }
				    return false;
			    });
			
			     
			    $("#edit_"+index).live("click", function(){
				    document.location.href = self.options.location+"edit_action.php?id="+index+"&";
			    });		     
			   } else {
                   html.push("<td>&nbsp;</td>");
               }
			}
			if(self.options.updateAction)
			{
			   if(!awaitingApproval)
			   {
			     if(isOwner[index] || self.options.section == "admin")
			     {
			        html.push("<td>");
			        html.push("<input type='button' name='update_"+index+"' id='update_"+index+"' value='Update "+self.options.actionName+"' />");
			        html.push("</td>");
			     } else {
			        html.push("<td>");
			        html.push("<input type='button' name='update_"+index+"' value='Update "+self.options.actionName+"' disabled='disabled' />");
			        html.push("</td>");
			     }		
			     $("#update_"+index).live("click", function(){
				    document.location.href = self.options.location+"update_action.php?id="+index+"&";						
			     });     
			   } else {
                   html.push("<td>&nbsp;</td>");
               }
			}
			if(self.options.assuranceAction)
			{
			   if(!awaitingApproval)
			   {
			      html.push("<td>");
			       html.push("<input type='button' name='assurance_"+index+"' id='assurance_"+index+"' value='Assurance "+self.options.actionName+"' />");
			       $("#assurance_"+index).live("click", function(){
				        document.location.href = self.options.location+"assurance_action.php?id="+index+"&";	
			       });			        
			     html.push("</td>");
			   }
			}
			if(self.options.viewAction)
			{
			    html.push("<td>");
		        html.push("<input type='button' name='view_action_"+index+"' id='view_action_"+index+"' value='View "+self.options.actionName+" Details' />");
		        html.push("</td>");
		        $("#view_action_"+index).live("click", function(){
			        document.location.href = self.options.location+"view_action.php?id="+index+"&";	
		        });			        
			 }
			html.push("</tr>");		
		});		
		html.push("<tr>");
		  html.push("<td colspan="+(cols - 1)+"><b>Overall "+self.options.actionName+" Progress:</b></td>");
		  html.push("<td id='averageProgress'><b>"+avgProgress+"</b></td>");
        if(self.options.editAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }    
        if(self.options.viewAction)
        {
           html.push("<td>&nbsp;&nbsp;</td>");
        }  		  
		html.push("</tr>");
		$("#"+self.options.tableId).append(html.join(' '));
	} ,
	
	_displayHeaders		: function(headers)
	{
		var self = this;
		var html = [];
		html.push("<tr>");
		$.each(headers, function(index, head){
			html.push("<th class='"+(self.options.viewAction ? "th2" : "")+"'>"+head+"</th>");	
		});
        if(self.options.editAction)
        {
          html.push("<th class='"+(self.options.viewAction ? "th2" : "")+"'>&nbsp;&nbsp;</th>");
        }
        if(self.options.updateAction)
        {
          html.push("<th class='"+(self.options.viewAction ? "th2" : "")+"'>&nbsp;&nbsp;</th>");
        }
        if(self.options.assuranceAction)
        {
          html.push("<th class='"+(self.options.viewAction ? "th2" : "")+"'>&nbsp;&nbsp;</th>");
        }
        if(self.options.viewAssurance)
        {
          html.push("<th class='"+(self.options.viewAction ? "th2" : "")+"'>&nbsp;&nbsp;</th>");
        }   
        if(self.options.viewAction)
        {
           html.push("<th class='"+(self.options.viewAction ? "th2" : "")+"'>&nbsp;&nbsp;</th>");
        }   			
        html.push("</tr>");
		$("#"+self.options.tableId).append( html.join(' ') );		
	} ,
	
	_displayPaging		: function(total, cols)
	{
		var self  = this;	
		var html  = [];
		var pages = 0;
        var key   = "_";
        if(self.options.risk_id !== 0 || self.options.risk_id == undefined)
        {
           key = key+"_"+self.options.risk_id;
        }
		if(total%self.options.actionlimit > 0)
		{
		   pages = Math.ceil( total/self.options.actionlimit )
		} else {
		   pages = Math.floor( total/self.options.actionlimit )
		}
        html.push("<tr>");
          html.push("<td colspan='"+(cols + 1)+"'>");
           if(self.options.actioncurrent < 2)
            {
                html.push("<input type='button' id='action_first_"+key+"' value=' |< ' disabled='disabled' />");
                html.push("<input type='button' id='action_previous_"+key+"' value=' < ' disabled='disabled' />");
            } else {
                html.push("<input type='button' id='action_first_"+key+"' value=' |< ' />");
                html.push("<input type='button' id='action_previous_"+key+"' value=' < ' />");                
            }
             html.push("<span>&nbsp;&nbsp;&nbsp;");
/*              if(pages != 0)
              {
                html.push("Page <select id='select_actionpage' name='select_actionpage'>");
                     for(var p=1; p<=pages; p++)
                     {
                        html.push("<option value='"+p+"' "+(self.options.actioncurrent == p ? "selected='selected'" : "")+">"+p+"</option>");
                     }
                html.push("</select>");
                html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
              } else {
                 html.push("Page <select id='select_actionpage' name='select_actionpage' disabled='disabled'>");
                 html.push("</select>");
                 html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
              }   */
             html.push("Page "+self.options.actioncurrent+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
             html.push("&nbsp;&nbsp;&nbsp;</span>");
            if((self.options.actioncurrent == pages || pages == 0))
            {
              //$("#action_next_"+key).attr('disabled', 'disabled');
              //$("#action_last_"+key).attr('disabled', 'disabled');		     
                html.push("<input type='button' id='action_next_"+key+"' value=' > ' disabled='disabled' />");
                html.push("<input type='button' id='action_last_"+key+"' value=' >| ' disabled='disabled' />");              
            } else {
                html.push("<input type='button' id='action_next_"+key+"' value=' > ' />");
                html.push("<input type='button' id='action_last_"+key+"' value=' >| ' />");                
            }

        if(self.options.editAction)
        {
          html.push("&nbsp;");
        }
        if(self.options.updateAction)
        {
          html.push("&nbsp;");
        }
        if(self.options.assuranceAction)
        {
          html.push("&nbsp;");
        }
        if(self.options.viewAssurance)
        {
          html.push("&nbsp;");
        }    
        if(self.options.viewAction)
        {
           html.push("&nbsp;");
        }   	   
/*          html.push("<span style='float:right;'>");
            html.push("<b>Quick Search for Action:</b> QA <input type='text' name='qa_id' id='qa_id' value='' />");
             html.push("&nbsp;&nbsp;&nbsp;");
            html.push("<input type='button' name='action_search' id='action_search' value='Go' />");
          html.push("</span>");  */
          html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));       
        
		$("#select_actionpage").live("change", function(){
		   if($(this).val() !== "" && parseFloat($(this).val())>0)
		   {
		      self.options.actionstart   = parseFloat($(this).val() - 1) * parseFloat(self.options.actionlimit);
		      self.options.actioncurrent = parseFloat($(this).val());
		      self._getActions();
		   }
		});
        $("#action_search").live("click", function(e){
           var action_id = $("#qa_id").val();
           if(action_id != "")
           {
              self.options.action_id = $("#qa_id").val();
              self._getActions();
              self.options.action_id = 0;
           } else {
              $("#qa_id").addClass('ui-state-error');
              $("#qa_id").focus().select();
           } 
           e.preventDefault();
        });        
                  			 
		$("#action_next_"+key).bind("click", function(){
			self._getNext( self );
		});
		$("#action_last_"+key).bind("click",  function(){
			self._getLast( self );
		});
		$("#action_previous_"+key).bind("click",  function(){
			self._getPrevious( self );
		});
		$("#action_first_"+key).bind("click",  function(){
			self._getFirst( self );
		});			 
	} , 
		
		_getNext  			: function(self) 
		{
		    self.options.actioncurrent = (self.options.actioncurrent + 1);
		    self.options.actionstart   = (self.options.actioncurrent - 1) * self.options.actionlimit;
		    self._getActions();
			/*
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getActions();
			*/
		},	
		
		_getLast  			: function($this) 
		{
			$this.options.actioncurrent   =  Math.ceil( parseFloat( $this.options.actiontotal )/ parseFloat( $this.options.actionlimit ));
			$this.options.actionstart 	= parseFloat($this.options.actioncurrent-1) * parseFloat( $this.options.actionlimit );
			this._getActions();
		},
		
		_getPrevious    	: function($this) 
		{
			$this.options.actioncurrent   = parseFloat( $this.options.actioncurrent ) - 1;
			$this.options.actionstart 	= ($this.options.actioncurrent-1)*$this.options.actionlimit;		
			this._getActions();			
		},
		
		_getFirst  			: function($this) 
		{
			$this.options.actioncurrent = 1;
			$this.options.actionstart 	= 0;
			this._getActions();	
		}
	
	
});

