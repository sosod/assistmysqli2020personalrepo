/**
 * 
 * JavaScript Document
**/
$(function(){
	
	FixedReports.getRiskPerType();
	
	
});

FixedReports	= {	
		
		getRiskPerType	: function(){			
		
		$.post("controller.php?action=getRiskPerType", function( responseData ){
			//console.log( responseData );
			var chart;
			chart = new AmCharts.AmSerialChart();		
			chart.dataProvider = responseData;
			chart.categoryField = "name";
			chart.color = "#000000";
			chart.fontSize = 9;
			chart.marginTop = 25;
			chart.marginBottom = 40;
			chart.marginRight = 60;
			chart.angle = 45;
			chart.depth3D = 15;
			chart.plotAreaFillAlphas= 0.9;
			chart.plotAreaBorderAlpha = 0.2;
			
			var graph1 = new AmCharts.AmGraph();
			graph1.title = "Inherent Risk Rating";
			graph1.valueField = "avgInherent";
			graph1.type = "column";
			graph1.lineAlpha = 0;
			graph1.lineColor = "#FF6600";
			graph1.fillAlphas = 1;
			graph1.balloonText="Inherent Risk Rating : [[value]]";		
			chart.addGraph(graph1);

			var graph2 = new AmCharts.AmGraph();				
			graph2.title = "Residual Risk";
			graph2.valueField = "avgResidual";
			graph2.type = "column";
			graph2.lineAlpha = 0;
			graph2.lineColor = "#D2CB00";
			graph2.fillAlphas = 1;
			graph2.balloonText="Residual Risk : [[value]]";
			chart.addGraph(graph2);
			

			var valAxis = new AmCharts.ValueAxis();
				valAxis.gridAlpha = 0.1;
				valAxis.axisAlpha = 0;
				valAxis.minimum = 0;
				valAxis.fontSize=8;
			chart.addValueAxis(valAxis);
			
			var catAxis = chart.categoryAxis;
			 chart.categoryAxis.gridAlpha = 0.1;
			 chart.categoryAxis.axisAlpha = 0;
			 chart.categoryAxis.gridPosition = "start";			
			chart.write("chartdiv");
		}, "json");		
	}
		
		
}
