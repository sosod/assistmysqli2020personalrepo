// JavaScript Document
var actionData = {};
$(function(){
	$(".datepicker").live( "focus",function(){
		$(this).datepicker()								 
		return false;
	});
	$.get("controller.php?action=getActionsToApprove", {id : $("#risk_id").val() }, function( data ){
		var message = $("#risk_action_message");
		
		if( $.isEmptyObject( data ) ) {
			$("#risk_action_message").html( "No result were returned" )
		} else {
		  $.each( data , function( index ,val ) {
			$("#action_table")
			.append($("<tr />")
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.action}))
				.append($("<td />",{html:val.status}))
				.append($("<td />",{html:val.progress}))
				.append($("<td />",{html:""}))
				.append($("<td />")
					.append($("<input />",{type:"submit", value:"Approve", id:"approve_"+val.id, name:"approve_"+val.id}))					
				)
			)
			
			$("#approve_"+val.id).live( "click", function(){
				document.location.href = "approve_action.php?id="+val.id;
				return false;
			});
			
			$("#delete_"+val.id).live( "click", function(){
				$.post("controller.php?action=deleteAction", { id : val.id }, function( ret ){
					if( ret == 1 ) {
						message.html( "Risk action deleted" )	
					} else if( ret == 0 ) {
						message.html( "Risk action is already deactivated" )	
					} else {
						message.html( "Error deleting the risk action " )	
					}													  
				});
				return false;
			});
			$("#update_action_table")
			.append($("<tr />")
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.action}))
				.append($("<td />",{html:val.status}))
				.append($("<td />",{html:val.progress}))
				.append($("<td />",{html:""}))
				.append($("<td />")
					.append($("<input />",{type:"submit", value:"Update", id:"update_"+val.id, name:"update_"+val.id}))					
				)
			)
			$("#update_"+val.id).live( "click", function(){
				$("#update").fadeIn();
				$("#show_action_table").html("");
				$.post("controller.php?action=getAction", { id:val.id }, function( actionData ){
					$("#show_action_table")														  
					.append($("<tr />")
						.append($("<td />",{html:"Action #"}))
						.append($("<td />",{html:actionData.id}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Action "}))							  
						.append($("<td />",{html:actionData.action}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Deliverable"}))							  
						.append($("<td />",{html:actionData.deliverable}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Action Owner"}))							  
						.append($("<td />",{html:actionData.action_owner}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Time Scale"}))							  
						.append($("<td />",{html:actionData.timescale}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Deadline"}))							  
						.append($("<td />",{html:actionData.deadline}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Status"}))							  
						.append($("<td />",{html:actionData.status}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"Progress"}))							  
						.append($("<td />",{html:actionData.progress}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{html:"remind On"}))							  
						.append($("<td />",{html:actionData.remindon}))		  
					 )
					.append($("<tr />")
						.append($("<td />",{colspan:"2"})
							.append($("<input />",{type:"hidden", value:val.id, name:"action_id", id:"action_id"}))		  
						)							  
					 )
				},"json");
				return false;
			});
			
			
		  });	
		}
	}, "json");
	
	$("#add_action").live( "click", function(){
											 								 
		if( $("#addaction_table").length == 0 ) {
		$("#add_new_action").css({ top:"5%", left:"30%", opacity:"0.75" })
		$("#add_new_action")
		.append($("<table />",{border:"1", id:"addaction_table"})
			.append($("<tr />")
				.append($("<td />",{html:"Risk "}))
				.append($("<td />",{html:$("#risk_id").val() }))
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Action #"}))
				.append($("<td />"))
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Action"}))
				.append($("<td />")
					.append($("<textarea />",{name:"action", id:"action"})) 
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Deliverable"}))
				.append($("<td />")
					.append($("<textarea />",{name:"deliverable", id:"deliverable"}))		  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Action Owner"}))
				.append($("<td />")
					.append($("<select />",{id:"action_owner"})
						.append($("<option />",{text:"-- select user --", value:""}))		  
					)		 						  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Time Scale " }))
				.append($("<td />")
					.append($("<input />",{type:"text", id:"timescale", name:"timescale"}))		  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Deadline"}))
				.append($("<td />")
					.append($("<input />",{type:"text", name:"deadline", id:"deadline"}).addClass("datepicker"))		  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Status"}))
				.append($("<td />")
					.append($("<select />",{id:"action_status"})
						.append($("<option />",{value:"", text:"--select status--"}))		  
					)		  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Progress"}))
				.append($("<td />")
					.append($("<input />",{type:"text", name:"progress", id:"progress"}))		  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Remind On"}))
				.append($("<td />")
					.append($("<input />",{type:"text", value:"", id:"remindon", name:"remindon"}).addClass("datepicker"))		  
				)
			)		 
			.append($("<tr />")
				.append($("<td />",{html:"Attachement(s)"}))
				.append($("<td />")
					.append($("<input />",{type:"file", value:"", id:"attachment", name:"attachment"}))		  
				)
			)		
			.append($("<tr />")
				.append($("<td />")
					
				)
				.append($("<td />")
					.append($("<input />",{type:"submit", value:"Save", id:"save_action", name:"save_action"}))		  
					.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
					.append($("<input />",{type:"submit", value:"Cancel", id:"cancel_action", name:"cancel_action"}))		  						
				)
			)
			.append($("<tr />")
				.append($("<td />",{colspan:"2", align:"right"})
					.append($("<a />",{href:"", html:"Close", id:"close"}))		  
				)		  
			)
			
		)
		} else {
			return $("#addaction_table")
		}
		$.get("controller.php?action=getUserAccess", function( data ){
			$.each( data , function( index, val){
				$("#action_owner")
				.append($("<option />",{value:val.id, text:val.user}))								
			})
		},"json")
		
		$.get("controller.php?action=getStatus", function( statuses ){
			$.each( statuses , function( index, val){
			$("#action_status")
			.append($("<option />",{value:val.id, text:val.name}))								
			})
		},"json")
		
		return false;								
	});
	

	
	$("#save_action").live( "click", function(){
		//var message 		= $("#riskaction_message")
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val()		
		var rs_deliverable  = $("#deliverable").val()
		var rs_timescale    = $("#timescale").val()		
		var rs_deadline     = $("#deadline").val()
		var rs_progress     = $("#progress").val()
		var rs_status       = $("#action_status :selected").val()
		var rs_remindon     = $("#remindon").val()		
		
		if( rs_action == "" ) {
			message.html("Please enter the action ")	
			return false;
		} else if( rs_action_owner == "" ) {
			message.html("Please enter the action owner for this risk ")	
			return false;				
		} else if( rs_deliverable == "" ) {
			message.html("Please enter the deliverable for this action ")	
			return false;				
		} else if( rs_timescale == "" ) {
			message.html("Please enter the time scale for this action ")	
			return false;					
		} else if( rs_deadline == "" ) {
			message.html("Please enter the deadline for this action ")	
			return false;					
		} else if( rs_remindon == "" ) {
			message.html("Please enter the remond on for this action ")	
			return false;					
		} else {
			
			$.post( "controller.php?action=newRiskAction" , 
				   {
					   	id				: $("#risk_id").val() ,
						r_action 		: rs_action ,
						action_owner    : rs_action_owner ,
						deliverable     : rs_deliverable ,
						timescale       : rs_timescale ,
						deadline     	: rs_deadline ,
						remindon     	: rs_remindon ,
						progress     	: rs_progress ,	
						status			: rs_status
				   } ,
				   function( data ) {
					  $("#actions_table")
					  .append($("<tr />")
						.append($("<td />"))
						.append($("<td />",{html:rs_action}))
						.append($("<td />",{html:rs_action_owner}))
						.append($("<td />",{html:rs_deliverable}))
						.append($("<td />",{html:rs_timescale}))
						.append($("<td />",{html:rs_deadline}))
						.append($("<td />",{html:rs_remindon}))	
						.append($("<td />")
							.append($("<input />",{type:"submit", value:"Edit"}))
							.append($("<input />",{type:"submit", value:"Del"}))							
						)								
					   )
				   } ,
				   		"json");
		}
			
		return false;
	});
	
	$("#cancel_action").live( "click", function(){
		$("#add_new_action").hide();	
		return false;
	});
	
	$("#close").live("click", function(){
		$("#add_new_action").hide();		
		return false;
	});
	
	$("#edit_action").click(function(){
		var message 		= $("#edit_action_message");
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val();		
		var rs_deliverable  = $("#deliverable").val();
		var rs_timescale    = $("#timescale").val();		
		var rs_deadline     = $("#deadline").val();
		var rs_progress     = $("#progress").val();
		var rs_status       = $("#action_status :selected").val();
		var rs_remindon     = $("#remindon").val();		
		
		if( rs_action == "" ) {
			message.html("Please enter the action ")	
			return false;
		} else if( rs_action_owner == "" ) {
			message.html("Please enter the action owner for this risk ")	
			return false;				
		} else if( rs_deliverable == "" ) {
			message.html("Please enter the deliverable for this action ")	
			return false;				
		} else if( rs_timescale == "" ) {
			message.html("Please enter the time scale for this action ")	
			return false;					
		} else if( isNaN( rs_timescale ) ) {
			message.html(" Time scale must be a number ");	
			return false;							
		} else if( rs_deadline == "" ) {
			message.html("Please enter the deadline for this action ")	
			return false;					
		} else if( rs_remindon == "" ) {
			message.html("Please enter the remond on for this action ")	
			return false;					
		} else {
			
			$.post( "controller.php?action=updateRiskAction" , 
				   {
					   	id				: $("#actionid").val() ,
						r_action 		: rs_action ,
						action_owner    : rs_action_owner ,
						deliverable     : rs_deliverable ,
						timescale       : rs_timescale ,
						deadline     	: rs_deadline ,
						remindon     	: rs_remindon ,
						progress     	: rs_progress ,	
						status			: rs_status
				   } ,
				   function( response ) {
					if( response == 1 ) {
						message.html( "Changes to risk action saved" )
					} else if( response == 0 ){
						message.html( "There were no changes made to the risk action" )	
					}  else {
						message.html( "There was an error saving changes to the risk action ")	
					}
					   
			} ,"json");
		}
		return false;
	});
	
	$("#save_action_update").click(function(){
		var message 	= $("#update_message");
		var description = $("#update_description") .val();
		var status		= $("#update_status :selected") .val();
		var progress	= $("#update_progress").val();
		var remindon	= $("#update_remindon").val();
		var attachment	= $("#update_attachment").val();
		var approve 	= $("#update_approval").val();
		

		if( description == "") {
			message.html("Please enter the description for the update")
			return false;
		} else if( status == "" ) {
			message.html("Please select the status for the update")
			return false;		
		} else if( progress == "" ) {
			message.html("Please enter the progress for the update")
			return false;
		} else if( isNaN( progress ) ) {
			message.html(" Progrss must be a number ");	
			return false;							
		} else if( remindon == "" ) {
			message.html("Please enter the remind date for the update")
			return false;	
		} else {
			
			$.post( "controller.php?action=newActionUpdate", 
				   {
				   	id 			: $("#action_id").val(),
				   	description	: description ,
					status		: status ,
					progress	: progress ,
					remindon 	: remindon,
					attachment	: attachment,
					approval	: approve
				  }, function( retUp ){
					   if( retUp > 0 ) {
							message.html( "Risk action update saved " )   
						} else {
							message.html( "Risk action could not be saved " )  	
						}
					}, "json")
			
		}	
		
		return false;
	})
	
	$("#search").click(function(){
		var search_message = $("#approval_action_message")
		search_message.html("Searching ... <img src='../images/loaderA32.gif' />");
		if( $("#search_action").val() !== "" ){
		$.post( "controller.php?action=searchActionToApprove", 
			   {
					searchtext 	: $("#search_action").val()   
				},
			   function( searchResults ){
				   search_message.html("");
				   $("#action_table").html("");
					if( $.isEmptyObject( searchResults ) ) {
						$("#risk_action_message").html( "No result were returned" )
					} else {
					  $.each( searchResults , function( index ,val ) {
						$("#action_table")
						.append($("<tr />")
							.append($("<td />",{html:val.id}))
							.append($("<td />",{html:val.action}))
							.append($("<td />",{html:val.status}))
							.append($("<td />",{html:val.progress}))
							.append($("<td />",{html:""}))
							.append($("<td />")
								.append($("<input />",{type:"submit", value:"Approve", id:"approve_"+val.id, name:"approve_"+val.id}))					
							)
						)
						$("#approve_"+val.id).live( "click", function(){
							document.location.href = "approve_action.php?id="+val.id;
							return false;
						});						
						
					  });
				 }
			   }
			   , "json")
		} else{
			search_message.html("Enter search text");	
		}
		return false;						
	});
});