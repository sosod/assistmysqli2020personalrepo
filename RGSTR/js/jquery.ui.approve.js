$.widget("ui.approve",{
	
	options			: {	
			limit			: 10,
			start 			: 0,
			current			: 1,		
			total			: 10,				
			loggedUserId	: 0,
			objectName		: "Risk",
			actionName		: "Action",
	} ,
	
	_init			: function()
	{
		this._getActions();		
	} , 
	
	_create			: function()
	{
		var self = this;
		$(this.element).append($("<table />",{width:"100%"}).addClass("noborder")
		   .append($("<tr />")
			 .append($("<td />",{width:"50%"}).addClass("noborder")
			   .append($("<table />",{id:"actions_awaiting", width:"100%"})
				  .append($("<tr />")
					 .append($("<td />",{html:"<h4>"+self.options.actionName+"s Awaiting Approval</h4>",colspan:"8"}))
				  )
				  .append($("<tr />")
					 .append($("<td />",{colspan:"8"})
					   .append($("<select />",{id:"users_awaiting"}).addClass("users"))		 
					 )	  
				  )
			   )		 
			 )
			 .append($("<td />",{width:"50%"}).addClass("noborder")
			   .append($("<table />",{id:"actions_approved",width:"100%"})
				 .append($("<tr />")
				   .append($("<td />",{html:"<h4>"+self.options.actionName+"s Approved</h4>",colspan:"8"}))
				 )
				 .append($("<tr />")
				   .append($("<td />",{colspan:"8"})
					  .append($("<select />",{id:"users"}).addClass("users"))	   
				   )		 
				 )
			   )		 
			 )
		   )		
		)
		
		this._getUsers();
		
	} ,
	
	
	_getActions		: function()
	{
		var self  = this;
		$.getJSON("controller.php?action=getApproveData",{
			start		: self.options.start, 
			limit		: self.options.limit,
			user		: self.options.loggedUserId
		} ,function( responseData ){			
			
			$(".removable").html("")
				
			self._displayHeaders( responseData.headers, "actions_awaiting" );
			self._displayHeaders( responseData.headers, "actions_approved" );
			if( $.isEmptyObject(responseData.awaiting.actions) ){
				$("#no_actionawaiting").remove();
				$("#actions_awaiting").append($("<tr />",{id:"no_actionawaiting"}).addClass("removable")
				  .append($("<td />",{colspan:"8",html:"No "+self.options.actionName+"s awaiting approval"}))
				)
			} else {
				self._display( responseData.awaiting, "actions_awaiting" )				
			}
			
			if( $.isEmptyObject(responseData.approved.actions)){
				$("#no_actionapproved").remove();
				$("#actions_approved").append($("<tr />",{id:"no_actionapproved"}).addClass("removable")
				  .append($("<td />",{colspan:"8",html:"No "+self.options.actionName+"s approved"}))
				)				
			} else {
				self._display( responseData.approved, "actions_approved" )				
			}
			
		});
		
	} , 
	
	
	_display		: function( data, table )
	{
		var self = this;
		$.each( data.actions, function( index, action ){
			var tr = $("<tr />",{id:index}).addClass("removable");
			$.each( action, function( key, val){
				tr.append($("<td />",{html:(key == "progress" ? val+"%" : val)}))				
			});
			
			tr.append($("<td />").css({display:(table == "actions_awaiting" ? "table-cell" : "none")})
			  .append($("<input />",{type:"button", name:"approve_"+index, id:"approve_"+index, value:"Approve"}))		
			)
			
			tr.append($("<td />").css({display:(table == "actions_approved" ? "table-cell" : "none")})
			  .append($("<input />",{type:"button", name:"unlock_"+index, id:"unlock_"+index, value:"Unlock"}))		
			)			
			
			$("#approve_"+index).live("click", function(){
				
				if( $("#approvedialog_"+index).length == 1 ) {
					$("#approvedialog_"+index).remove();
				}
				
				$("<div />",{id:"approvedialog_"+index})
				 .append($("<table />")
				    .append($("<tr />")
				       .append($("<th />",{html:"Response:"}))
				       .append($("<td />")
				    	 .append($("<textarea />",{cols:"30", rows:"7", id:"response", name:"response"})) 	   
				       )
				    )
				   .append($("<tr />")
					  .append($("<th />",{html:"Sign Off:"}))
					  .append($("<td />")
						 .append($("<select />",{id:"sign_off", name:"sign_off"})
						   .append($("<option />",{value:"0", text:"No"}))
						   .append($("<option />",{value:"1", text:"Yes"}))
						 )	  
					  )
				    )
				 )
				 .append($("<tr />")
				   .append($("<td />",{id:"approvemessage_"+index}))		 
				 )
				.dialog({
						 autoOpen	: true,
						 modal		: true, 
						 title		: "Approve/Delcline "+self.options.actionName+" #"+index,
						 buttons	: {
										 "Approve"	: function()
										 {
												jsDisplayResult("info", "info", "Approving "+self.options.actionName+" . . .  <img src='../images/loaderA32.gif' />");			
												$.post( "controller.php?action=newApprovalAction", {
													id			: index, 
													userrespo	: self.options.loggedUserId,
													response	: $("#response").val(), 
													signoff		: $("#sign_off :selected").val()
												}, function( response ){
													if( response.error )
													{
														jsDisplayResult("error", "error", response.text );
													} else {
														jsDisplayResult("ok", "ok", response.text );
													}
													$("#no_actionapproved").remove();
													self._getActions();
												},"json")
												$("#approvedialog_"+index).dialog("destroy");
												$("#approvedialog_"+index).remove();
										 } , 
										 
										 "Decline"	: function()
										 {	
											 	if( $("#response").val() == ""){
											 		$("#approvemessage_"+index).html("<br /> Please enter the reason for the decline");
											 		return false;
											 	} else {
													 jsDisplayResult("info", "info", "Declining "+self.options.actionName+" . . .  <img src='../images/loaderA32.gif' />");	
													 $.post("controller.php?action=sendDecline", { id : index, response : $("#response").val() }, function( response ){
														if( !response.error )
														{
															jsDisplayResult("ok", "ok", response.text );
														} else{
															jsDisplayResult("error", "error", response.text);
														}	
														self._getActions();
													 },"json")
													 $("#approvedialog_"+index).dialog("destroy");
													 $("#approvedialog_"+index).remove();
											 	} 								 
										 }
					
									  }
				});
				
				return false;
			});
			
			$("#unlock_"+index).live("click", function(){
				if( $("#unlockaction_"+index).length == 1 ) {
					$("#unlockaction_"+index).remove();
				}
				
				
				$("<div />",{id:"unlockaction_"+index})
				 .append($("<span />",{html:"Unlocking the "+self.options.actionName+" allows you to edit and update the "+self.options.actionName+" again"}))
				 .dialog({
					 	  title			: "Unlock "+self.options.actionName+" #"+index,
					 	  modal			: true, 
					 	  autoOpen		: true,
					 	  buttons		: {
					 						"Confirm"	: function()
					 						{	
												 jsDisplayResult("info", "info", "Unlocking "+self.options.actionName+" . . .  <img src='../images/loaderA32.gif' />");
												 $.post("controller.php?action=unlockAction",{ id : index },function( response ){
													if( !response.error )
													{
														jsDisplayResult("error", "error", response.text );
													} else{
														jsDisplayResult("ok", "ok", response.text);
													}		
													self._getActions();
												 },"json");
												 $("#unlockaction_"+index).dialog("destroy");
												 $("#unlockaction_"+index).remove();												 
					 						} , 
					 						"Cancel"	: function()
					 						{
												 $("#unlockaction_"+index).dialog("destroy");
												 $("#unlockaction_"+index).remove();
					 						}
				 	     				  }
				 });
				
				return false;
			});
			
			$("#"+table).append( tr )
		});
	} , 
	
	
	_displayHeaders		: function( headers, table )
	{
		var tr = $("<tr />").addClass("removable");
		$.each( headers, function( index , head){
			tr.append($("<th />",{html:head}))
		});	
		tr.append($("<th />",{html:"&nbsp;"}))
		$("#"+table).append( tr )
	} , 
	
	
	_displayPaging		: function()
	{
		
		
	} ,
	_getUsers			    : function(){
		var self = this;
		$.getJSON("controller.php?action=getManagerUsers", function( users ){
			$.each( users, function( index, user ){
				$(".users").append($("<option />",{value:user.user , text:user.tkname+" "+user.tksurname }))			 
			})														
		});	
		$(".users").live("change", function(){
			self.options.loggedUserId = $(this).val();
			self._getActions();
			return false;
		});
	}  
	
	
	
});
