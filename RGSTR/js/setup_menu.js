// JavaScript Document
var menuValues = {};

$(function(){
	$("#container").append($("<table />",{id:"menu_contents" , border:"1"})
		.append($("<tr />")
			.append($("<th />",{html:"Ref"}))
			.append($("<th />",{html:"Section"}))
			.append($("<th />",{html:"Default Terminology"}))
			.append($("<th />",{html:"Your Terminology"}))
			.append($("<th />",{html:""}))			
		)						 
	);
	
	$.get( "old_controller.php?action=getAllMenu", function( menuData ) {
		$.each( menuData, function( index , menu ) {
			//alert(menu.parent_name);
			$("#menu_contents")
			.append($("<tr />",{id:"tr_"+menu.id})
				.append($("<td />",{html:menu.id}))
				.append($("<td />",{html:menu.parent_name, title:menu.description}))
				.append($("<td />",{html:menu.name, title:menu.description}))
				.append($("<td />")
					.append($("<input />",{
							type : "text",
							name : "menu_"+menu.id,
							id	 : "menu_"+menu.id,
							value: ((menu.client_name == "" || menu.client_name == null) ?  menu.name : menu.client_name ),
							size : "50"
							}))		  
				)	
				.append($("<td />")
					.append($("<input />",{type:"submit", id:"save_"+menu.id, name:"save_"+menu.id, value:"Update" }))
				)
			)
			
			$("#tr_"+menu.id).live( "mouseover", function(){
				$(this).addClass("tdhover")
			});
			
			$("#tr_"+menu.id).live( "mouseout", function(){
				$(this).removeClass("tdhover")
			});
			
			$("#save_"+menu.id).live( "click" , function() {	
					jsDisplayResult("info", "info", "Updating...  <img src=\"../images/loaderA32.gif\" />" );			
					//alert(menu.folder+" : "+menu.id+" : "+$("#menu_"+menu.id).val());
					$.post("old_controller.php?action=menu" , {
						   name : menu.folder,
						   id	: menu.id,
						   value : $("#menu_"+menu.id).val() 
						   } , function( retData  ) {						   
							   if( retData == 1 ) {
								jsDisplayResult("ok", "ok", "Menu change successfully updated . . . ");
							   } else if( retData == 0 ){
								jsDisplayResult("info", "info", "No change has been made to the menu . . . ");
							   } else {
								jsDisplayResult("error", "error", "Error saving changes to the menu item . . . ");							   
							   }
							   //$("ul#menulist").find("li[id=#"+id+"]>a").html($("#menu_"+id).val())
					});
				return false;
			}) 
			
		});
	},"json");	
	

})