// JavaScript Document
$(function(){ 
	
	//$(".textcounter").textcounter({maxLength:200});
	
	//set the with of the select to be almost uniform	   
	//$("table#risk_new").find("select").css({"width":"200px"})	   
	//set the table th text to align to the left
	$("table#risk_new").find("th").css({"text-align":"left"})

  // ----------------------- risk types ------------------------------------------------ 	
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	// ---------------------------------- risk categories -------------------------------------------
	/**
		get all the risk categories
	**/
	$("#risk_type").live("change", function() {
		$("#risk_category").html("");
		var v = $(this).val();
		if(v.length>0) {
			$("#risk_category").append($("<option />",{value:"", text:"--- SELECT ---"}))											
			$.get( "controller.php/?action=getCategory", { id : $(this).val() }, function( data ) {
				$.each( data ,function( index, val ) {
					$("#risk_category")
					.append($("<option />",{text:val.name, value:val.id}));
				})											 
			},"json");	
		} else {
			var t = AssistString.substr($("#th_type").html(),0,-1).toUpperCase();
			$("#risk_category").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}))											
		}
		return false;
	});
	// -------------------------------------- risk levels ---------------------------
	/**
		get all the risk level
	**/									
		$.get( "controller.php/?action=getRiskLevel", { id : $(this).val() }, function( data ) {
			$.each( data ,function(index, val ) {
			    $("#risk_level").append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
	//-----------------------------------------risk impacts ------------------------------------------
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			$("#financial_exposure").append($("<option />",{text:val.name, value:val.id}));
		})			
	},"json");

	//----------------------------------active risk impacts --------------------------------------
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getActiveImpact", function( data ) {
		var impactRating = [];
		$.each( data ,function( index, val ) {
			$("#risk_impact")
			.append($("<option />",{text:val.assessment, value:val.id}));
		})			
	},"json");
	
	//----------------------------------- get risk impact ratings -------------------------	
	/**
		Get the risk impact ratings
	**/
	$("#risk_impact").live( "change" ,function(){
		var v = $(this).val();
		$("#risk_impact_rating").html("");
		if(v.length>0) {
			$("#risk_impact_rating").append($("<option />",{value:"", text:"--- SELECT ---"}))
			$.get( "controller.php?action=getImpactRating", { id:v }, function( data ) {
				if ( data.diff == 0 ) {
					$("#risk_impact_rating")
						.append($("<option />",{value:data.to, text:data.to, selected:"selected" }))
				} else {
					for( i = data.from; i <= data.to; i++ ) {
						$("#risk_impact_rating")
						.append($("<option />",{value:i, text: i}))	
					}
				}
			}, "json");
		} else {
			var t = AssistString.substr($("#th_impact").html(),0,-1).toUpperCase();
			$("#risk_impact_rating").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}));
		}
		return false;
	});
	//---------------------------------- risk likelihoods ------------------------------------	
	/**
		get all the risk likelihoods
	**/
	$.get( "controller.php/?action=getLikelihood", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_likelihood")
			.append($("<option />",{text:val.assessment, value:val.id}));
		})											 
	},"json");
	
	// ----------------------------------------- likelihood ratings -------------------------	
	/**
		Get the risk likelihood ratings
	**/
	$("#risk_likelihood").live( "change" ,function(){
		var v = $(this).val();
		$("#risk_likelihood_rating").html("");
		if(v.length>0) {
			$("#risk_likelihood_rating").append($("<option />",{value:"", text:"--- SELECT ---"}))
			$.get( "controller.php?action=getLikelihoodRating", { id:$(this).val() }, function( data ) {
				if ( data.diff == 0 ) {
					$("#risk_likelihood_rating")
						.append($("<option />",{value:data.to, text:data.to, selected:"selected" }))
				} else {
					for( i = data.from; i <= data.to; i++ ) {
						$("#risk_likelihood_rating")
						.append($("<option />",{value:i, text:i}))	
					}
				}
			}, "json");
		} else {
			var t = AssistString.substr($("#th_likelihood").html(),0,-1).toUpperCase();
			$("#risk_likelihood_rating").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}));
		}
		return false;
	});
	
	// -------------------------------------perceived control effectiveness---------------------	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getControl", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_percieved_control")
			.append($("<option />",{text:val.effectiveness, value:val.id}));
		})											 
	},"json");
	
	
	// ---------------------perceived controll efectiveness rating -------------------------	
	/**
		Get the risk control effectiveness ratings
	**/
	$("#risk_percieved_control").live( "change" ,function(){	
		var v = $(this).val();
		if(v.length>0) {
			$("#risk_control_effectiveness").html("");
			$("#risk_control_effectiveness").append($("<option />",{value:"", text:"--- SELECT ---"}))		
			$.get("controller.php?action=getControlRating", { id:$(this).val() }, function( data ) {
					$("#risk_control_effectiveness")
						.append($("<option />",{value:data.rating, text:data.rating,selected:"selected" }))
			}, "json");
		} else {
			var t = AssistString.substr($("#th_risk_percieved_control").html(),0,-1).toUpperCase();
			$("#risk_control_effectiveness").append($("<option />",{value:"", text:"--- SELECT "+t+" ABOVE ---"}));
		}
		return false;
	});		
	
	// ------------------------- get 	user responsible --------------------------------
	/**
		I dont think its still need , waste on network resources
	**/
	$.get( "controller.php/?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_user_responsible")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
			
			$("#action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})											 
	},"json");
	
	//----------------------------- get the directorates / sub directorates ---------------------	
	$.get("controller.php?action=getDirectorate", function( direData ){
	$.each( direData ,function( index, directorate ) {
			$("#directorate")
			.append($("<option />",{text:directorate.dirtxt	, value:directorate.subid}));
		})
	}, "json");
	/*================================================================================================================	
	$.get("controller.php?action=getSubDirectorate", function( subdireData ){													  
		$.each( subdireData ,function( index, subdirectorate ) {
			$("#subdirectorate")
			.append($("<option />",{text:subdirectorate.shortcode, value:subdirectorate.tkid}));
		})
	}, "json");
	*///================================================================================================================
	/*$("#add_another_control").click( function() {
		var id 	   = parseInt(Math.round(Math.random()*44)) + parseInt(Math.round(Math.random()*45));
		var spanId = "currentcontrols_"+id;
		$(this).parent().prepend($("<p />")
		  .append($("<textarea />",{name:"currentcontrols_"+id, id:"currentcontrols_"+id, cols:"30", rows:"5"}).addClass("controls"))
		  //.append($("<span />",{id:"counter_"+spanId, html:"200 characters allowed"}))
		)
		//$(this).parent().prepprepend($("<br />")).prepend($("<textarea />",{name:"currentcontrols_"+id, id:"currentcontrols_"+id, cols:"30", rows:"5"}).addClass("controls").addClass("textcounter").textcounter(10).append($("<br />")))
		return false;									  
	});
	//================================================================================================================
	/**
		Add New Risk
	**/
	$("#add_risk").click(function() {

		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#risk_type :selected").val()
		var r_level					= $("#risk_level :selected").val()		
		var r_financial_exposure	= $("#financial_exposure :selected").val()				
		var r_actual_financial_exposure	= $("#actual_financial_exposure").val()
		var r_kpi_ref				= $("#kpi_ref").val()				
		var r_category 				= $("#risk_category :selected").val()
		var r_description 			= $("#risk_description").val()
		var r_background			= $("#risk_background").val();	
		var r_impact 				= $("#risk_impact :selected").val();
		var r_impact_rating			= $("#risk_impact_rating").val();
		var r_likelihood 			= $("#risk_likelihood :selected").val();
		var r_likelihood_rating 	= $("#risk_likelihood_rating").val();
		var r_percieved_control		= $("#risk_percieved_control :selected").val();
		var r_control_effectiveness = $("#risk_control_effectiveness").val();
		//var r_user_responsible 		= $("#risk_user_responsible").val();
		var r_directorate 			= $("#directorate :selected").val();
		//var r_subdirectorate 		= $("#subdirectorate").val()
	
		
		var r_responsiblity = "";
		$(".responsibility").each( function( index, val){
			if( $(this).val() == "" ){
				jsDisplayResult("error", "error", "Please select the one responsible");
				return false;
			} else {
				r_responsiblity  = $(this).val();	
			}									
		});
		var r_attachements = "";

		$(".controls").each( function( index, val){
			if( index > 0 ) {
				r_currentcontrols += "_"+$(this).val();
			}else{
				r_currentcontrols = $(this).val();	
			}
		});

		if( r_type == "" ) {
			jsDisplayResult("error", "error", "Please select the risk type for this risk ");
			return false;
		} else if( r_level == "" ) {
			jsDisplayResult("error", "error", "Please select the risk level for this risk ");
			return false;
		}else if( r_category == "" ) {
			jsDisplayResult("error", "error", "Please select the risk category for this risk");
			return false;
		} else if( r_description == "" ) {
			jsDisplayResult("error", "error", "Please enter the risk description for this risk");
			return false;
		} else if( r_background == "" ) {
			jsDisplayResult("error", "error", "Please enter the risk background for this risk");
			return false;
		} else if( r_impact == "" ) {
			jsDisplayResult("error", "error", "Please select the risk impact for this risk");
			return false;
		} else if( r_likelihood == "" ) {
			jsDisplayResult("error", "error", "Please select the risk likelihood for this risk");
			return false;
		} else if( r_currentcontrols == "" ) {
			jsDisplayResult("error", "error", "Please enter the risk current controls for this risk");
			return false;
		} else if(r_actual_financial_exposure != ""  && isNaN(r_actual_financial_exposure)){
			jsDisplayResult("error", "error", "Please enter a valid financial exposure");
			return false;			
		} else if( r_percieved_control == "" ) {
			jsDisplayResult("error", "error", "Please enter the risk percieved control effectiveness for this risk");
			return false;
		}  else {
			jsDisplayResult("info", "info", "Saving risk.....  <img src='../images/loaderA32.gif' />");
			//message.html( "Saving risk...  <img src='../images/loaderA32.gif' />" )
			$.post( "controller.php?action=newRisk",
			  {
				type 			 	  	  : r_type,
                financial_year_id         : $("#financial_year_id :selected").val(),
				type_text			  	  : $("#risk_type :selected").text(),
				level				  	  : r_level,
				level_text		  	  	  : $("#risk_level :selected").text(),
				financial_exposure    	  : r_financial_exposure,
				financial_exposure_text   : $("#financial_exposure :selected").text(),
				actual_financial_exposure :	r_actual_financial_exposure,
				kpi_ref		  	  	      : r_kpi_ref,
				category		 	      : r_category ,
				category_text		      : $("#risk_category :selected").text(),
				description 	 	      : r_description ,
			    background		 	      : r_background ,
		 		impact			 	      : r_impact ,
				impact_text			      : $("#risk_impact :selected").text(),
				impact_rating	 	      : r_impact_rating ,
				likelihood 		 	      : r_likelihood ,
				likelihood_text		      : $("#risk_likelihood :selected").text(),
				likelihood_rating	      : r_likelihood_rating ,
				currentcontrols	 	      : r_currentcontrols ,
				percieved_control	      : r_percieved_control ,
				percieved_control_text    : $("#risk_percieved_control :selected").text(),
				control_effectiveness     :	r_control_effectiveness ,
                rbap_ref                  : $("#rbap_ref").val(),
                cause_of_risk             : $("#cause_of_risk").val(),
                reasoning_for_mitigation  : $("#reasoning_for_mitigation").val(),
				user_responsible 	      : r_directorate ,
				user_responsible_text     : $("#directorate :selected").text(),
				attachment			      : r_attachements
			  } ,
			  function( response  ) {
					 
				 if( response.saved ) {
					 if( response.error ){
						 jsDisplayResult("error", "error", response.text);						 
					 } else {
						 jsDisplayResult("ok", "ok", response.text);
					 }
						//$("#idrisk").val( response.id )
						//$("#idrisk").html( data )			
					 //removes for development testing purposes
						/*$("#kpi_ref").val("")						
						$("#actual_financial_exposure").val("")					 
						$("select#financial_exposure option[value='']").attr("selected", "selected");
					    $("select#risk_level option[value='']").attr("selected", "selected");
						$("select#risk_type option[value='']").attr("selected", "selected");
						$("select#risk_impact option[value='']").attr("selected", "selected");
						$("select#risk_likelihood_rating option[value='']").attr("selected", "selected");											
						$("select#risk_percieved_control option[value='']").attr("selected", "selected");$("select#risk_impact_rating option[value='']").attr("selected", "selected");					
						$("select#risk_category option[value='']").attr("selected", "selected");
						$("select#risk_likelihood option[value='']").attr("selected", "selected");
                        $("select#financial_year_id option[value='']").attr("selected", "selected");
						$("select#risk_control_effectiveness option[value='']").attr("selected", "selected");						
						$("select#directorate option[value='']").attr("selected", "selected");					
						$("#risk_description").val("");
						$("#risk_background").val("");
						$(".controls").val("");
                         $("#rbap_ref").val("");
                         $("#cause_of_risk").val("");
                         $("#reasoning_for_mitigation").val("");
						$("#file_loading").html("");*/					 
				 } else {					 
					 jsDisplayResult("error", "error", response.text);
				 }
			  } ,
			 "json");
		}				  
		return false;						  
	});
	//================================================================================================================		
	$(".remove").live( "click", function(){
		if( confirm("Are you sure you want to delete"))
		{
			var id = this.id;
			$.post("controller.php?action=removeFile", { id : this.id }, function( response ){
				if( response != 1 ){
					$("#"+id).html("Error deleting the file");				
				} else {
					$("#li_"+id).fadeOut();				
				}				
			});			
		}
		return false;
	})
});

function ajaxFileUpload( fileupload )
	{

		$("#loading")
		.ajaxStart(function(){
			$("#file_loading").html("Uploading . . . <img src='../images/loaderA32.gif' />");
		})
		.ajaxComplete(function(){
			//$(this).html("");
		});

		$.ajaxFileUpload
		(
			{
				url			  : 'controller.php?action=saveAttachment',
				secureuri	  : false,
				fileElementId : 'fileupload',
				dataType	  : 'json',
				success       : function (response, status)
				{	
					$("#file_loading").html("");
					if(!response.error)
					{
						if( $("#attachment_list").length == 0){
							$("#file_loading").append($("<ul />",{id:"attachment_list"}))
						}				
						$("#attachment_list").prepend("<span class='messages'><div class='ui-state-ok ui-corner-all' style='padding:0.7em;'><span class='ui-icon ui-icon-check' style='float:left; margin-right:0.3em;'></span>"+response.filename+" successfully uploaded</div></span>")
						//$("#file_loading").html("<a hre=''>"+response.filename+" successfully uploaded <br /></span>")
							//		 .css({"color":"blue"})
						if( $.isEmptyObject(response.filesuploaded) ){
							//$("#file_loading").append( "" )
						} else { 
							//$("#file_loading").append($("<ul />",{id:"files"}))
							$.each( response.filesuploaded, function(index, file){
								$("#attachment_list").append( "<li id='li_"+index+"' class='newuploads' style='padding: 0 .7em;'><span>"+file+" <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"'  title='"+file+"' class='remove'><span></span>Remove</a>" )
								//$("#files").append("<li id='li_"+index+"'>"+file+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='"+index+"' class='remove'><span></span>Remove</a></ul>")
								//.append($("<br />"))
							});							
						}
												
					}else
					{
						$("#file_loading").html(" Error uploading  "+response.error);
					}
				},
				error: function (data, status, e)
				{
					$("#file_loading").html(" Ajax error "+e);
				}
			}
		)
		return false;
	}
