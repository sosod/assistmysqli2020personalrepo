// JavaScript Document
$(function(){
			   
	$.get( "controller.php?action=getAAction", { id:$("#action_id").val() }, function( actionData ){
		$("#risk_action_approval")
			.append($("<tr />")
				.append($("<td />",{html:"Action #"}))		  
				.append($("<td />",{html:actionData.id}))		  				
			)
			.append($("<tr />")
				.append($("<td />",{html:"Action"}))		  
				.append($("<td />",{html:actionData.action}))		  				
			)
			.append($("<tr />")
				.append($("<td />",{html:"Deliverable"}))		  
				.append($("<td />",{html:actionData.deliverable}))		  		  
			)
			.append($("<tr />")
				.append($("<td />",{html:"Action Owner"}))		  
				.append($("<td />",{html:actionData.user}))		  
			)
			.append($("<tr />")
				.append($("<td />",{html:"Time Scale"}))		  
				.append($("<td />",{html:actionData.timescale}))		  		  
			)
			.append($("<tr />")
				.append($("<td />",{html:"Deadline"}))		  
				.append($("<td />",{html:actionData.deadline}))		  		  
			)
			.append($("<tr />")
				.append($("<td />",{html:"Status"}))		  
				.append($("<td />",{html:actionData.status}))		  		  
			)
			.append($("<tr />")
				.append($("<td />",{html:"Progress"}))		  
				.append($("<td />",{html:actionData.progress}))		  		  
			)
			.append($("<tr />")
				.append($("<td />",{html:"Remind On"}))		  
				.append($("<td />",{html:actionData.remindon}))		  		  
			)
	},"json");
		
		
	$.get( "controller.php?action=getActionAssurance", {id : $("#action_id").val() }, function( actionAssuranceData ){															
		if( $.isEmptyObject( actionAssuranceData ) ){
		} else{	
		$.each( actionAssuranceData, function(index, val){
			$("#list_assurance_action_table")
			.append($("<tr />")
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))	
				)								 
		});
		}	
		$("#list_assurance_action_table")
		.append($("<tr />")
			.append($("<td />",{colspan:"7"})
			  .append($("<input />",{type:"submit", name:"showadd_new_action_assurance", id:"showadd_new_action_assurance", value:"Add New"}))		
				)		  
			)
		
		$("#showadd_new_action_assurance").live( "click", function(){
			$("#new_action_assurance").show();
			return false;
		});
		
	}, "json");
	
	$("#add_newAction_assurance").live( "click", function(){
		
		var message 	= $("#newassurance_message")
		var response  	= $("#assurance_response").val();
		var signoff  	= $("#assurance_signoff :selected").val();
		var attachment 	= $("#assurance_attachment").val();		
		
		if( response == "" ) {
			message.html(" Please enter the response for the assurance")
			return false;
		} else if( signoff == "") {
			message.html(" Please slected th signoff for the assurance")
			return false;
		} else {
			$.post( "controller.php?action=newAssuranceAction", {
				   id			:	$("#action_id").val(), 
				   response		:	response, 
				   signoff		: 	signoff,
				   attachment	: 	attachment
				   }, function( retAssurance ){
					  
				  })	
		}
		return false;											 
	});
	
})