// JavaScript Document
$(function(){
	
	var message = $("#riskcategory_message");
	/**
		Get the risk types
	**/
	$("#riskcategory_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getRiskType", function( data ) {
		$.each( data , function( index, val ){
			$("#category_risktype")
				.append($("<option />",{value:val.id, text:val.name}));
		});
	},"json");
	/**
		Get all the categries
	**/
	$.get( "controller.php?action=getCategory", function( data ) {
		$("#riskcategory_message").html("");
		message = $("#riskcategory_message");
		$.each( data, function( index, val ) {
		 $("#riskcategory_table")
		  .append($("<tr />",{id:"tr_"+val.id})
			.append($("<td />",{html:val.id}))
			.append($("<td />",{html:val.shortcode}))
			.append($("<td />",{html:val.name}))
			.append($("<td />",{html:val.descr}))
			.append($("<td />",{html:val.risktype}))
			.append($("<td />")
			  .append($("<input />",{type:"submit", value:"Edit", id:"catedit_"+val.id, name:"catedit_"+val.id }))
			  .append($("<input />",{type:"submit", value:"Del", id:"catdel_"+val.id, name:"catdel_"+val.id }))			  
			 )
			.append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
			  //.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"catactive_"+val.id, name:"catactive_"+val.id }))
			  //.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ), value:"Inactivate", id:"catdeactive_"+val.id, name:"catdeactive_"+val.id}))							
			)	
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		  
			)
		  )	
		  
		  $("#change_"+val.id).live("click", function(){
			document.location.href = "change_category_status.php?id="+val.id+"&";
		  	return false;													  
		  });
		  
		  $("#catedit_"+val.id).live( "click", function(){
			document.location.href = "edit_risk_category.php?id="+val.id+"&";
		  	return false;
		  });
		  
		  $("#catdel_"+val.id).live( "click", function(){
			if( confirm(" Are you sure you want to delete this risk category ")) {		
				jsDisplayResult("info", "info", "Deleting risk category . . . <img src='../images/loaderA32.gif' > ");
				$.post( "controller.php?action=catdeactivate", {id:val.id, status:2}, function( catdelData ) {
					if( catdelData == 1){
                        $("#tr_"+val.id).fadeOut();
                        jsDisplayResult("ok", "ok", "Category deactivated . . . ");
					} else if( catdelData == 0){
                        jsDisplayResult("info", "info", "No change was made to the category . . . ");
					} else {
                        jsDisplayResult("error", "error", "Error updating risk category . . . ");
					}								 
				});	
			}
		  	return false;
		  });		  
		  
		  $("#catactive_"+val.id).live( "click", function(){
			jsDisplayResult("info", "info", "Activating risk category . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=catactivate", {id:val.id}, function( catactData ) {
				if( catactData == 1){
					jsDisplayResult("ok", "ok", "Category activated");
				} else if( catactData == 0){
					jsDisplayResult("info", "info", "No change was made to the category .. .  ");
				} else {
					jsDisplayResult("info", "info", "Error updating risk category . . .");
				}									 
			});	
		  	return false;
		  });		  
		  

		  
		});	
	}, "json")
	/**
		Add a new risk category
	**/
	$("#add_riskcategory").click( function() {
		var message     = $("#riskcategory_message");
		var category    = $("#risk_category").val();
		var shortcode   = $("#category_shortcode").val();
		var description = $("#category_description").val();
		var c_risktype  = $("#category_risktype :selected").val();

		if ( shortcode == "") {
			jsDisplayResult("error", "error", "Please enter the short code for this category");	
			return false;
		} else if( category == "") {
			jsDisplayResult("error", "error", "Please enter the category ");	
			return false;
		} else if ( c_risktype == "") {
			jsDisplayResult("error", "error", "Please select the risk type for this category");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving risk category . . . <img src='../images/loaderA32.gif' > ");
			$.post( "controller.php?action=newCategory", 
				  	{ 
					category		: category,
					shortcode		: shortcode,
					description		: description,
					type_id			: c_risktype },
					function( retData ){
					if( retData > 0 ) {
					$("#risk_category").val("");
					$("#category_shortcode").val("");
					$("#category_description").val("");
					jsDisplayResult("ok", "ok", "Risk category succesifully added . . . ");
					$("#riskcategory_table")
					  .append($("<tr />",{id:"tr_"+retData})
						.append($("<td />",{html:retData}))
						.append($("<td />",{html:shortcode}))
						.append($("<td />",{html:category}))
						.append($("<td />",{html:description}))
						.append($("<td />",{html: $("#category_risktype :selected").text() }))
						.append($("<td />")
						  .append($("<input />",{type:"submit", value:"Edit", id:"catedit_"+retData, name:"catedit_"+retData }))
						  .append($("<input />",{type:"submit", value:"Del", id:"catdel_"+retData, name:"catdel_"+retData }))			  
						 )
						.append($("<td />",{html:"<b>Active</b>"})
						 // .append($("<input />",{type:"submit", value:"Activate", id:"typeactive", name:"typeactive" }))
						  //.append($("<input />",{type:"submit", value:"Inactivate", id:"typedeactive", name:"typedeactive" }))							
						)
						.append($("<input />",{type:"submit", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))
					  )
					  
					  $("#catedit_"+retData).live("click", function(){
						document.location.href = "edit_risk_category.php?id="+retData;
						return false;													  
					  });
					  
					  $("#change_"+retData).live("click", function(){
						document.location.href = "change_category_status.php?id="+retData;
						return false;													  
					  });

                      $("#catdel_"+retData).live( "click", function(){
                        if( confirm(" Are you sure you want to delete this risk category ")) {
                            $.post( "controller.php?action=catdeactivate", {id:retData, status:2}, function( catdelData ) {
                                if( catdelData == 1){
                                    $("#tr_"+retData).fadeOut();
                                    message.html("Category deactivated").animate({opacity:"0.0"},4000);
                                } else if( catdelData == 0){
                                    message.html("No change was made to the category ").animate({opacity:"0.0"},4000);
                                } else {
                                    message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
                                }
                            });
                        }
                        return false;
                      });

					} else {
						jsDisplayResult("error", "error", "Error occured saving risk category . . . ");
					}
						
				}, "json");	
		}
		return false;				   
	});

	$("#change_riskcategory").click(function(){
		jsDisplayResult("ok", "ok", "Updating risk category . . . <img src='../images/loaderA32.gif' > ");
		$.post( "controller.php?action=catdeactivate", 
			   { id		: $("#risk_category_id").val(),
			   	 status : $("#category_status :selected").val()
			   }, function( catdeaData ) {
			if( catdeaData == 1){
				jsDisplayResult("ok", "ok", "Category status changed . . .  ");
			} else if( catdeaData == 0){
				jsDisplayResult("info", "info", "No change was made to the category . . .  ");
			} else {
				jsDisplayResult("error", "error", "Error updating . . .  ");
			}									 
		});	
	  	return false;
	  });
	
	$("#edit_riskcategory").click(function(){
		
		var message     = $("#editriskcategory_message");
		var category    = $("#risk_category").val();
		var shortcode   = $("#category_shortcode").val();
		var description = $("#category_description").val();
		var c_risktype  = $("#category_risktype :selected").val();

		if ( shortcode == "") {
			jsDisplayResult("error", "error", "Please enter the short code for this category");	
			return false;
		} else if( category == "") {
			jsDisplayResult("error", "error", "Please enter the category");	
			return false;
		} else if ( c_risktype == "") {
			jsDisplayResult("error", "error", "Please select the risk type for this category");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating risk category . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateCategory", 
				  	{ 
					id				: $("#risk_category_id").val(),
					category		: category,
					shortcode		: shortcode,
					description		: description,
					type_id			: c_risktype },
					function( retData ){
						if( retData == 1 ) {
							jsDisplayResult("ok", "ok", "Category successfully updated ..");						
						} else if( retData == 0 ){
							jsDisplayResult("info", "info", "No change was made ..");
						} else {
							jsDisplayResult("error", "error", "Error updating the category . . .");
						}
			}, "json");	
		}
		return false;				   
					 
	});		  

	$("#cancel_riskcategory").click(function(){
		history.back();
		return false;								   
	})
	
});
