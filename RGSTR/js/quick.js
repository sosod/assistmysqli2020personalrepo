// JavaScript Document
//View Risk
$(function(){
	$("#show_inherentVsresidual").click(function(){
		document.location.href = "inherentVsResidual.php";		
		return false;
	});
	
	
	$("#show_inherentVsresidualperType").click(function(){
		document.location.href = "graphPerType.php";		
		return false;
	});

	$("#show_inherentVsresidualperCategory").click(function(){
		document.location.href = "graphPerCategory.php";		
		return false;
	});
	
	$("#show_inherentVsresidualperPerson").click(function(){
		document.location.href = "graphPerPerson.php";		
		return false;
	});
		
	$("#show_riskregister").click(function(){
		document.location.href = "show_risk_register.php";
		return false;
	});
	
	$("#number_of_risk_in_each_status").click(function(){
		document.location.href = "number_of_risk_in_each_status.php";
		return false;
	});
	
	
	$("#number_of_actions_in_each_status").click(function(){
		document.location.href = "number_of_actions_in_each_status.php";
		return false;
	});	
		
	$("#risk_versus_control").click(function(){
		document.location.href = "risk_versus_control.php";
		return false;
	});	
	
	$("#show_riskregisters").click(function(){
	$("#hideOnshow").hide();
	var total = 0
	
	var current = 1
	var start 	= 0;
	var limit  	= 10;
	
	getRiskData( current, start, limit);
	
	$("#b1").live("click", function(){
		current = 1;
		start = 0;
		limit = 10;
		getRiskData( current, start, limit);
		return false;
	});
	
	$("#b2").live("click", function(){
		limit = 10;
		total = parseFloat( $('body').data('total') );
		var pages = Math.ceil( total/10 );
		current = parseFloat( current ) - 1;
		//start = (current == 1 ? 0 : current ) * limit;
		start = current * limit
		//limit = limit * current;			
		
		//limit = parseFloat( limit ) * parseFloat( current );			
		getRiskData( current, start, limit);
		return false;
	});
	
	$("#b3").live("click", function(){
		limit = 10;
		current = current+1;
		start = parseFloat( current ) * parseFloat( limit );
		getRiskData( current, start, limit);
		return false;
	});

	$("#b4").live("click", function(){
		current =  Math.floor( parseFloat( $('body').data('total') )/ parseFloat( limit ));
		start = parseFloat(current-1) * parseFloat(limit);
		getRiskData( current, start, limit);		 
		return false;
	});
	
	return false						   
	});
		   
});


function getRiskData( current, start, limit ) {
	$("#register").fadeIn();
	$("#risk_table").html("Loading risks  .... <img src='../images/loaderA32.gif' />");
	$.get("controller.php?action=getRisk",
	  { 
	  	page		: "",
	  	start		: start,
		limit 		: limit
	  } ,
	  function(  data ) {
		  if( $.isEmptyObject( data ) ){
			message.html("No Risks were found");			  
		  } else {
			$("#risk_table").html("");
			//createPager( data.total , current);
			populateHeaders( data.headers );
			populateData( data.riskData );			  
		  }
	}, "json");
}

function populateHeaders( headerData ) 
{
	$("#risk_table")
	.append($("<tr />")
		.append($("<th />",{html:namingHeader(headerData['riskitem'],'Risk Item')}))
		.append($("<th />",{html:namingHeader(headerData['risktype'],'Type')}))
		.append($("<th />",{html:namingHeader(headerData['riskcategory'],'Category')}))
		.append($("<th />",{html:namingHeader(headerData['riskdescription'],'Description')}))
		.append($("<th />",{html:namingHeader(headerData['background'],'Background')}))
		.append($("<th />",{html:namingHeader(headerData['impact'],'Impact')}))
		.append($("<th />",{html:namingHeader(headerData['impact_rating'],'Impact Rating')}))				
		.append($("<th />",{html:namingHeader(headerData['likelihood'], 'Likelihood')}))
		.append($("<th />",{html:namingHeader(headerData['likelihood_rating'], 'Likelihood Rating')}))
		.append($("<th />",{html:namingHeader(headerData['inherent_riskexposure'], 'Inherent risk Exposure')}))
		.append($("<th />",{html:namingHeader(headerData['inherent_riskrating'], 'Inherent risk rating')}))				
		.append($("<th />",{html:namingHeader(headerData['current_controls'], 'Current Controls')}))
		.append($("<th />",{html:namingHeader(headerData['percieved_control_effectiveness'], 'Percieved Control Effectiveness')}))
		.append($("<th />",{html:namingHeader(headerData['control_effectiveness_rating'], 'Control Effectiveness Rating')}))
		.append($("<th />",{html:namingHeader(headerData['residual_risk'], 'Residual Risk')}))
		.append($("<th />",{html:namingHeader(headerData['residual_riskexposure'], 'Residual Risk exposure')}))								
		.append($("<th />",{html:namingHeader(headerData['riskowner'], 'Responsibilty of the risk')}))
		.append($("<th />",{html:namingHeader(headerData['status'], 'Status')}))
		)
}

function populateData( riskData )
{
	$.each( riskData, function( index , risk){
		$("#risk_table")
		.append($("<tr />")
		.append($("<td />",{html:index}))		  
		.append($("<td />",{html:risk.type}))		  
		.append($("<td />",{html:risk.category}))		  
		.append($("<td />",{html:risk.description}))		  
		.append($("<td />",{html:risk.background}))		  
		.append($("<td />",{html:risk.impact, title:risk.impactDescr}))		  
		.append($("<td />",{html:risk.impactRating,css:{"background-color":"#"+(risk.impactColor== null ? "" : risk.impactColor)}}))		
		.append($("<td />",{html:risk.likelihood}))		
		.append($("<td />",{html:risk.likelihoodRating,css:{"background-color":"#"+(risk.likeColor== null ? "" : risk.likeColor )}}))
		.append($("<td />",{css:{"background-color":"#"+(risk.inhRiskExposure== null ? "" : risk.inhRiskExposure)}																									}))
		.append($("<td />",{html:risk.inhRiskRating}))
		.append($("<td />",{html:risk.currControls}))		  
		.append($("<td />",{html:risk.effectiveness,css:{"background-color":"#"+(risk.effectColor== null ? "" : risk.effectColor)}}))		
		.append($("<td />",{html:risk.ctrEffectRating}))		
		.append($("<td />",{css:{"background-color":"#"+(risk.resRiskExposure== null ? "" : risk.resRiskExposure)} }))
		.append($("<td />",{html:risk.residualRisk}))
		.append($("<td />",{html:risk.tkname}))		  
		.append($("<td />",{html:risk.status,css:{"background-color":"#"+(risk.rscolor == null ? "" : risk.rscolor )},title:risk.status}))		
		)
	});
}

function namingHeader( idHeader, defaultName) 
{	
	if( idHeader == "" || idHeader == null  ) 
	{
		return defaultName;	
	} else {
		return idHeader;	
	}
}

function createPager( total , current ) 
{
	var pager =  Math.floor( parseFloat( total )/ parseFloat( 10 )	 );
	$("#risk_table").html("");	
	$("#risk_table")
		.append($("<tr />")
			.append($("<td />",{colspan:"19"})
				.append($("<table />",{id:"paging_risk", cellspacing:"5", width:"100%"}))		  
			)		  
		)
	$("#paging_risk").html("");
	$("#paging_risk")
			.append($("<tr />")
				.append($("<td />")
					.append($("<input />",{type:"submit", name:"b1", value:"|<", id:"b1", disabled:(current < 2 ? 'disabled' : '' )}))
					.append($("<input />",{type:"submit", name:"b2", value:"<", id:"b2", disabled:(current < 2 ? 'disabled' : '' )}))
					.append($("<span />",{html:" Page "+current+"/"+(pager==0 ? "1" : pager)+" "}))
					.append($("<input />",{type:"submit", name:"b3", value:">", id:"b3", disabled:((current==pager || pager == 0) ? 'disabled' : '' )}))
					.append($("<input />",{type:"submit", name:"b4", value:">|", id:"b4", disabled:((current==pager || pager == 0) ? 'disabled' : '' )}))					
				)
				.append($("<td />",{width:"50%"})
							  
				)
			)		
}