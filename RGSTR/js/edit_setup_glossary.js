// JavaScript Document
$(function(){
	
	$("#edit_glossary").click(function(){	
		var message  	= $("#glossary_messsage");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		
		if( category == "" ) {
			jsDisplayResult("error", "error", "Please select category . . . ");
			return false;
		} else if ( terminology == "" ) {
			jsDisplayResult("error", "error", "Please enter the terminology . . . ");			
			return false;
		} else if ( explanation == "" ){
			jsDisplayResult("error", "error", "Please enter the explanation . . . ");
			return false;
		} else  {
			jsDisplayResult("info", "info", "Updating . . . <img src='../images/loaderA32.gif' >");
			
			$.post("controller.php?action=updateGlossary",
			  {
				id			: $("#glossary_id").val(),
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Glossary term saved . . . ");
				} else {
					jsDisplayResult("error", "error", "Error saving the glossary term. . . ");
				}
			});
		 }
				return false;
	});		   
})