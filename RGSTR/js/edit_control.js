// JavaScript Document
$(function(){
	
	$("#edit_control").click(function(){
		var message 		= $("#control_message")						
		var ctrl_shortcode	= $("#control_shortcode").val();		
		var control_effect	= $("#control_effect").val();
		var quali_critera 	= $("#quali_critera").val();
		var control_rating	= $("#control_rating").val();
		var color 			= $("#control_color").val();

		if ( ctrl_shortcode == "") {
			jsDisplayResult("error", "error", "Please enter the short code for this control");
			return false;
		} else if( control_effect == "" ) {
			jsDisplayResult("error", "error", "Please enter the control effectiveness");
			return false;
		} else if ( quali_critera == "" ) {
			jsDisplayResult("error", "error", "Please enter the qualification criteria for this control");
			return false;
		} else if( control_rating == "" ) {
			jsDisplayResult("error", "error", "Please enter the control rating for this control");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Updating control . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=updateControl",
				   {
					  id			: $("#control_id").val(),
					  ctrl			: control_effect,
					  qual			: quali_critera,
					  rating		: control_rating,
					  clr			: color,
					  ctrl_shortcode: ctrl_shortcode
					}
				   , function( retData ) {
						if( retData == 1 ) {
							jsDisplayResult("ok", "ok", "Control Effectiveness successfully updated . . . ");						
						} else if( retData == 0 ){
							jsDisplayResult("info", "info", "Control Effectiveness not changed . . . ");
						} else {
							jsDisplayResult("error", "error", "Error updating the control effectiveness . . . ");
						}					    
			},"json")	
		}
		return false;								  
	});
	
	$("#change_control").click(function() {
		var message = $("#control_message")		
		jsDisplayResult("ok", "ok", "Updating control . . . ");
		$.post( "controller.php?action=changeControl", 
			   { 
			   		id		: $("#control_id").val(),
					status  : $("#control_status :selected").val()
			   }, function( deactData ) {
			if( deactData == 1){
				jsDisplayResult("ok", "ok", "Control effectveness status changed. . . ");
			} else if( deactData == 0){
				jsDisplayResult("info", "info", "No change was made to the control effectveness. . . ");
			} else {
				jsDisplayResult("error", "error", "Error updating control . . . ");
			}										 
		})									
		return false;										  						
	});
	
	$("#cancel_control").click(function(){
		history.back();
		return false;										
	});
	
});