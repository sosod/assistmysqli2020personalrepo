// JavaScript Document
$(function(){
	
	/**
		get all the impacts
	**/
	$("#impact_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getImpact", function( data ) {
		$("#impact_message").html("");
		var message = $("#impact_message");
		$.each( data , function( index, val) {
			$("#impact_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.assessment}))
			  .append($("<td />",{html:val.definition}))
			  .append($("<td />")
				.append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding" : "5px"}))	  
			  )
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"impactedit_"+val.id, name:"impactedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"impactde_"+val.id, name:"impactde_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"impactact_"+val.id, name:"impactact_"+val.id}))	
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ),value:"Inactivate", id:"impactdeact_"+val.id, name:"impactdeact_"+val.id}))							
			   )
			 .append($("<td />")
				.append($("<input />",{type:"submit", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		   
				)
			)
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_impact_status.php?id="+val.id+"&";
				return false;										  
			});
			
			$("#impactedit_"+val.id).live( "click", function() {
				document.location.href = "edit_impact.php?id="+val.id+"&";
				return false;										  
			});
			$("#impactde_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this impact ")) {	
					jsDisplayResult("info", "info", "Deleting impact . . . <img src='../images/loaderA32.gif' > ");
					$.post( "controller.php?action=changeImpstatus", 
						   {
							   	id		:	val.id ,
								status  : 2
								
								}, function( impdeData ) {
						if( impdeData == 1){
							jsDisplayResult("ok", "ok", "Impact deactivated . . . ");
							$("#tr_"+val.id).fadeOut();
						} else if( impdeData == 0){
							jsDisplayResult("info", "info", "No change was made to the impact  . . . ");
						} else {
							jsDisplayResult("error", "error", "Error updating, please try again . . . ");
						}									 
					});
				}
				return false;										  
			});
			$("#impactact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=impactactivate", {id:val.id}, function( ipmactData ) {
					if( ipmactData == 1){
						jsDisplayResult("ok", "ok", "Impact activated . . . ");
					} else if( ipmactData == 0){
						jsDisplayResult("info", "info", "No change was made to the impact . . . ");
					} else {
						jsDisplayResult("error", "error", "Error updating , please try again . . . ");
					}										 
				})															 
				return false;										  
			});
			$("#impactdeact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=impactdeactivate", {id:val.id}, function( ipmdeactData ) {
					if( ipmdeactData == 1){
						jsDisplayResult("ok", "ok", "Impact deactivated . . . ");
					} else if( ipmdeactData == 0){
						jsDisplayResult("ok", "ok", "No change was made to the impact . . . ");
					} else {
						jsDisplayResult("ok", "ok", "Error saving , please try again . . . ");	
					}						 
				})									
				return false;										  
			});
		});
	}, "json")
	
	/**
		Adding a new impact
	**/
	$("#add_impact").click( function() {
		var message 	= $("#impact_message")						
		var rating_from = $("#imprating_from").val();
		var rating_to 	= $("#imprating_to").val();
		var assessment	= $("#impact_assessment").val();
		var definition  = $("#impact_definition").val();
		var color 		= $("#impact_color").val();
	
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this impact . . . ");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this impact . . . ");
			return false;
		} else if ( assessment == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this impact . . . ");
			return false;
		} else if( definition == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this impact . . . ");			
			return false;			
		} else {
			jsDisplayResult("error", "error", "Saving . . . <img src='../images/loaderA32.gif' > ");
			$.post( "controller.php?action=newImpact", 
				   {
					  from		: rating_from,
					  to		: rating_to,
					  assmnt	: assessment,
					  dfn		: definition,
					  clr		: color
					}
				   , function( retData ) {
				if ( retData > 0 ) {
					$("#imprating_from").val("");
					$("#imprating_to").val("")
					$("#impact_assessment").val("")
					$("#impact_definition").val("")
					jsDisplayResult("ok", "ok", "Impact saved . . . ");
					
					$("#impact_table")
					.append($("<tr />")
					  .append($("<td />",{html:retData}))
					  .append($("<td />",{html:rating_from+" to "+rating_to}))
					  .append($("<td />",{html:assessment}))
					  .append($("<td />",{html:definition}))
					  .append($("<td />")
						 .append($("<span />",{html:color}).css({"background-color":"#"+color, "padding" : "5px"}))	  
					  )
					  .append($("<td />")
						.append($("<input />",{type:"submit", value:"Edit", id:"impactedit_"+retData, name:"impactedit_"+retData}))	
						.append($("<input />",{type:"submit", value:"Del", id:"impactdel_"+retData, name:"impactdel_"+retData}))					
					   )
					  .append($("<td />",{html:"<b>Active</b>"})
						//.append($("<input />",{type:"submit", value:"Activate", id:"impactact", name:"impactact"}))	
						//.append($("<input />",{type:"submit",value:"Inactivate", id:"impactdeact", name:"impactdeact"}))							
					   )	
					  .append($("<tr />")
						.append($("<td />")
						  .append($("<input />",{type:"button", id:"change_"+retData, name:"change_"+retData, value:"Change status"}))		  
						)		
					  )
					)
					
					$("#change_"+retData).live( "click", function() {
						document.location.href = "change_impact_status.php?id="+retData+"&";
						return false;										  
					});
					
					$("#impactedit_"+retData).live( "click", function() {
						document.location.href = "edit_impact.php?id="+retData+"&";
						return false;										  
					});					
				} else {
					jsDisplayResult("error", "error", "There was an error saving the impact . . . ");
				}
			},"json")	
		}
		return false;							 
	});	
	
	$("#edit_impact").click(function(){
		var message 	= $("#impact_message")						
		var rating_from = $("#imprating_from").val();
		var rating_to 	= $("#imprating_to").val();
		var assessment	= $("#impact_assessment").val();
		var definition  = $("#impact_definition").val();
		var color 		= $("#impact_color").val();
	
		if ( rating_from == "") {
			jsDisplayResult("error", "error", "Please enter the rating from for this impact . . .");
			return false;
		} else if( rating_to == "" ) {
			jsDisplayResult("error", "error", "Please enter the rating to for this impact . . .");
			return false;
		} else if ( assessment == "" ) {
			jsDisplayResult("error", "error", "Please enter the asssessment for this impact . . .");
			return false;
		} else if( definition == "" ) {
			jsDisplayResult("error", "error", "Please enter the definition for this impact . . .");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Update . . . <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=updateImpact", 
				   {
					  id		: $("#impact_id").val(),
					  from		: rating_from,
					  to		: rating_to,
					  assmnt	: assessment,
					  dfn		: definition,
					  clr		: color
					}
				   , function( retData ) {
					if( retData == 1 ) {
						jsDisplayResult("ok", "ok", "Impact successifully updated. . . ");						
					} else if( retData == 0 ){
						jsDisplayResult("info", "info", "Impact not changed. . . ");
					} else {
						jsDisplayResult("error", "error", "Error updating the impact. . . ");
					}
			},"json")	
		}
		return false;					 
	});	
	
	$("#change_impact").click(function(){		
		jsDisplayResult("error", "error", "Updating status . . . <img src='../images/loaderA32.gif' />");
		$.post("controller.php?action=changeImpstatus",
			   {
					id 		: $("#impact_id").val(),
					status  : $("#impact_status :selected").val() 
				},
			   function( retData ) {
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Impact status updated succesfully . . . ");						
				} else if( retData == 0 ) {
					jsDisplayResult("info", "info", "No change in status was made . . . ");					
				} else {
					jsDisplayResult("error", "error", "Error updating status . . . ");					
				}											  
		});
		return false;
	});
	
	$("#cancel_impact").click(function(){
		history.back();
		return false;								   
	});	
	
});
