$.widget("ui.risk",{
	
	options		: {
		start			: 0,
		limit			: 10,
		current			: 1,
		total			: 0,
		tableId			: "risktable_"+(Math.floor(Math.random(343)*45)),
		headers			: [],
		editActions		: false,
		updateActions	: false,
		assuranceActions: false,
		addActions		: false,
		assuranceRisk	: false,
		editRisk		: false,
		updateRisk		: false,
		viewRisk		: false,
		url				: "controller.php?action=getRisk",
		page			: "",
		section			: "",
		view			: "",
		objectName		: "Risk",
		actionName		: "Action"
	} ,
	
	_init				: function()
	{
		this._getRisk();		
	} , 
	
	_create				: function()
	{
		$(this.element).append($("<table />",{id:this.options.tableId, width:"auto"}));		
	} ,
	
	_getRisk			: function()
	{
		var self = this;
		//alert(self.options.objectName);
		var loader = $(self.element).append($("<div />",{id:"loading"}));
		$("#loading").ajaxStart(function(){
			$(this).html("Loading . . <img src='../images/loaderA32.gif' />")
					.css({	
						 	"background-color":"", 
							"position"  :"absolute",
							"z-index"	:"9999",
							"top"		: "200px",
							"left"		: "500px"
						 })				 
		});
		$("#loading").ajaxComplete(function(){
			$(this).empty();					 
		});						
		$.get( self.options.url,{
				page 		: this.options.page,
				start		: this.options.start,
				limit 		: this.options.limit,
				section 	: this.options.section,
				view		: this.options.view,
				headers		: this.options.headers
		}, function( risk ) {
	
			$("#"+self.options.tableId).html("");
			if($.isEmptyObject(risk.riskData)){
				$("#"+self.options.tableId).append($("<tr />")
				   .append($("<td />",{colspan:"5", html:"There are no "+self.options.objectName+" found"}))		
				)
			} else {
				self.options.total =  risk.total;
				self._createPager( risk.total, risk.x );
				self._displayHeaders( risk.headers )
				self._display( risk.riskData )				
			}
		},"json")
		
	} ,  
	
	_display				: function( risks )
	{
		var self = this;
		$.each(risks, function( index , risk){ //alert(index);
			var tr = $("<tr />").hover(function(){
				$(this).addClass("tdhover")
			}, function(){
				$(this).removeClass("tdhover")
			})
			$.each( risk, function( key, val){
				tr.append($("<td />",{html:val}))			
			})
			tr.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.addActions ? "table-cell" : "none")})
				.append($("<input />",{
						type	: "button",
						name	: "add_action_"+index,
						id		: "add_action_"+index,
						value	: "Add "+self.options.actionName
				}))
			)
			
			tr.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.editRisk ? "table-cell" : "none")})
				.append($("<input />",{
						 type	: "button",
						 name	: "edit_"+index,
						 id		: "edit_"+index,
						 value	: "Edit "+self.options.objectName
				}))
				.append($("<br />&nbsp;&nbsp;&nbsp;"))
				.append($("<input />",{
						type	: "button",
						name	: "action_edit_"+index,
						id		: "action_edit_"+index,
						value	: "Edit "+self.options.actionName
				}).css({"display":(self.options.editActions ? "none" : "none")}))
			)
			
			tr.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.updateRisk ? "table-cell" : "none")})
			    .append($("<input />",{
			    		type	: "button",
			    		name	: "update_"+index,
			    		id		: "update_"+index,
			    		value	: "Update "+self.options.objectName
			    }))		
			    .append($("<br />"))
				.append($("<input />",{
						type	: "button",
						name	: "action_update_"+index,
						id		: "action_update_"+index,
						value	: "Update "+self.options.actionName
				}).css({"display":(self.options.updateActions ? "table-cell" : "none")}))			    
			)
			
			tr.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.assuranceRisk ? "table-cell" : "none")})
			   .append($("<input />",{
				   		type	: "button",
				   		name	: "assurance_"+index,
				   		id		: "assurance_"+index,
				   		value	: "Assurance "+self.options.objectName
			   }))		
			  .append($("<input />",{
					type	: "button",
					name	: "action_assurance_"+index,
					id		: "action_assurance_"+index,
					value	: "Assurance "+self.options.actionName
			   }).css({"display":(self.options.assuranceActions ? "table-cell" : "none")}))				   
			)				

			//tr.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.assuranceActions ? "table-cell" : "none")})
			//)
		
			
			$("#add_action_"+index).live("click", function(){
				//alert(index);
				document.location.href = "new_action.php?id="+index;
				return false;
			});			
			
			$("#edit_"+index).live("click", function(){
				document.location.href = "editrisk.php?id="+index;								  
			});
			
			$("#action_edit_"+index).live("click", function(){
				document.location.href = "../actions/action_edit.php?id="+index;								  
			});				
			
			$("#update_"+index).live("click", function(){
				document.location.href = "updaterisk.php?id="+index+"&";								  
				return false;
			});
			
			$("#action_update_"+index).live("click", function(){
				document.location.href = "../actions/action_update.php?id="+index;								  
				return false;
			});			
			
			$("#assurance_"+index).live("click", function(){
				document.location.href = "riskassurance.php?id="+index+"&";								  
				return false;
			});
			
			$("#action_assurance_"+index).live("click", function(){
				document.location.href = "../actions/action_assurance.php?id="+index+"&";								  
				return false;
			});			
			$("#"+self.options.tableId).append( tr )
		});				
	} , 
	
	_displayHeaders			: function( headers )
	{
		var self = this;
		var tr   = $("<tr />");
		$.each(headers, function( key , head){
			tr.append($("<th />",{html:head}));			
		});
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.addActions ? "table-cell" : "none")}))
		//tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.assuranceActions ? "table-cell" : "none")}))
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.editRisk ? "table-cell" : "none")}))
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.updateRisk ? "table-cell" : "none")}))
		tr.append($("<th />",{html:"&nbsp;"}).css({"display":(self.options.assuranceRisk ? "table-cell" : "none")}))		
		$("#"+self.options.tableId).append(tr);
	} , 
	
	_createPager			: function( total , columns )
	{
		var self = this;
		var pages;
		if( total%self.options.limit > 0){
			pages   = Math.ceil(total/self.options.limit); 				
		} else {
			pages   = Math.floor(total/self.options.limit); 				
		}
		
		$("#"+self.options.tableId).append($("<tr/>")
			.append($("<td />",{colspan:columns})
			  .append($("<input />",{type:"button", value:"|<", id:"first", name:"first"}))
			  .append("&nbsp;&nbsp;&nbsp;")
			  .append($("<input />",{type:"button", value:"<", id:"previous", name:"previous"}))
			  .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
			  .append($("<span/>", {colspan:"2",html:"Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages), align:"center"}))	
			  .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
			  .append($("<input />",{type:"button", value:">", id:"next", name:"next"}))
			  .append("&nbsp;&nbsp;&nbsp;")				  
			  .append($("<input />",{type:"button", value:">|", id:"last", name:"last"}))				  
			)	
			.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.addActions ? "table-cell" : "none")}))
			.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.editActions ? "table-cell" : "none")}))
			.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.updateActions ? "table-cell" : "none")}))
			.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.assuranceActions ? "table-cell" : "none")}))
			//.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.editRisk ? "table-cell" : "none")}))
			//.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.updateRisk ? "table-cell" : "none")}))
			//.append($("<td />",{html:"&nbsp;"}).css({"display":(self.options.assuranceRisk ? "table-cell" : "none")}))			
		)
	       if(self.options.current < 2)
	       {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
	       }
	       if((self.options.current == pages || pages == 0))
	       {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
	       }	
		$("#next").on("click", function(evt){
			self._getNext(self);
		});
		$("#last").on("click",  function(evt){
			self._getLast(self);
		});
		$("#previous").on("click",  function(evt){
			self._getPrevious(self);
		});
		$("#first").on("click",  function(evt){
			self._getFirst(self);
		});			

	} ,		
	_getNext  			: function(self) {
		self.options.current = self.options.current+1;
		self.options.start = parseFloat( self.options.current - 1) * parseFloat( self.options.limit );
		this._getRisk();
	},	
	_getLast  			: function(self) {
		self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
		self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
		this._getRisk();
	},	
	_getPrevious    	: function( self ) {
		self.options.current  = parseFloat( self.options.current ) - 1;
		self.options.start 	= (self.options.current-1)*self.options.limit;
		this._getRisk();			
	},	
	_getFirst  			: function( self ) {
		self.options.current  = 1;
		self.options.start 	= 0;
		this._getRisk();				
	}
	
});
