// JavaScript Document
$(function(){
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "yy-mm-dd"
		});						   
	});	
	/**
		Get the risk info to display when adding a new action, to show user which risk is biend refered to 
	**/
	$.getJSON("controller.php?action=getARiskWithHeaders", { id : $("#risk_id").val() }, function( riskD ){
		//$("#riskInfo")
		var riskData = riskD.riskData;
		var headers = riskD.headers;
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:headers.risk_item+":"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.id}))
		  )
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:headers.risk_type+":"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.type}))
		  )
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:headers.risk_category+":"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.cat_descr}))
		  )		 
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:headers.risk_owner+":"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.risk_owner}))
		  )
		 $("#goback").before($("<tr />")
			.append($("<th />",{html:headers.current_controls+":"}).css({"text-align":"left"}))
			.append($("<td />",{html:riskData.current_controls}))
		  )		 
		 $("#goback").before($("<tr />",{rowspan:"6"}))
	});
	//========================================================================================================
	$.get("controller.php?action=getUsers", function( data ){
			$.each( data , function( index, val){
				$("#action_owner")
				.append($("<option />",{value:val.tkid, text:val.tkname+" "+val.tksurname}))								
			})
		},"json")
		
		$.get("controller.php?action=getStatus", function( statuses ){
			$.each( statuses , function( index, val){
			$("#action_status")
			.append($("<option />",{value:val.id, text:val.name}))								
			})
		},"json")
	//=============================================================================================================
	/**
		Add another browse button to attach files
	**/
	$("#another_attach").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
  //==============================================================================================================	
	$("#save_action").live( "click", function() {
		
		$("input:text, textarea, select").each(function() {
			$(this).removeClass("required");
		});
		
		var message 		= $("#addaction_message").slideDown("fast");
		var rs_action 		= $("#action").val();						
		var rs_action_owner = $("#action_owner :selected").val()		
		var rs_deliverable  = $("#deliverable").val()
		var rs_timescale    = $("#timescale").val()		
		var rs_deadline     = $("#deadline").val()
		var rs_progress     = $("#progress").val()
		var rs_status       = $("#action_status :selected").val()
		var rs_remindon     = $("#remindon").val()		
		
		var rs_attachements  	= [];
		$(".attachents").each( function( index, val){
			rs_attachements.push($(this).val());
		});
		var err = false;
		if( rs_action == "" ) {
			$("#action").addClass("required");
			err = true;
		}  
		if( rs_action_owner == "" ) {	
			$("#action_owner").addClass("required");
			err = true;
		}
		if( rs_deliverable == "" ) {
			$("#deliverable").addClass("required");
			err = true;
		}
		/*if( rs_timescale == "" ) {
			$("#timescale").addClass("required");
			err = true;
		}*/
		if( rs_deadline == "" ) {
			$("#deadline").addClass("required");
			err = true;
		}
			
		if(err) {
			jsDisplayResult("error", "error", "Please complete the missing information as highlighted in red.");
			return false;
		} else {
			//message.html( "Saving risk...  <img src='../images/loaderA32.gif' />" )
			jsDisplayResult("info", "info", "Saving "+window.action_object_name+"...  <img src=\"../images/loaderA32.gif\" />" );
			$.post( "controller.php?action=newRiskAction" , 
			   {
					id				: $("#risk_id").val() ,
					r_action 		: rs_action ,
					action_owner    : rs_action_owner ,
					deliverable     : rs_deliverable ,
					timescale       : rs_timescale ,
					deadline     	: rs_deadline ,
					remindon     	: rs_remindon ,
					progress     	: "0" ,	
					status			: "1",
					attachment		: rs_attachements					
			   } ,
			   function( response ) {
				 	 if( response.saved ) {
						 if( response.error ) {
							 jsDisplayResult("error", "error", response.text );
						 } else{
							 jsDisplayResult("ok", "ok", response.text); 
						 }
						 $("#tr_nothing_yet").remove();
						 $("#actions").action({risk_id:$("#risk_id").val(), extras:false, section:"new"});		
						 /*
						 $("#table_actions")
 						  .append($("<tr />")
							.append($("<td />",{html:response.id}))
							.append($("<td />",{html:rs_action}))
							.append($("<td />",{html:rs_deliverable}))							
							.append($("<td />",{html:"New"}))
							.append($("<td />",{html:"0%"}))	
							.append($("<td />",{html:$("#action_owner :selected").text()}))								
						   ).before($("#extras"))
						  */
						 
						$("#action").val("");
						$("#deliverable").val("")
						$("#timescale").val("")
						$("#deadline").val("")
						//$("#progress").val("")
						$("#remindon").val("")	
						$("#file_loading").html("");
							
				 	 } else {
				 		jsDisplayResult("error", "error", response.text);			 		 
				 	 }
				 	 

			   } ,
			   "json");
		}
		return false;
	});
	//=================================================================================================================
	$("#cancel_action").live( "click", function(){
		history.back();	
		return false;
	});
		
});