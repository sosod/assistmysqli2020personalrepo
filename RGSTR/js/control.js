$(function(){
	
	if( $("#control_id").val() == undefined) {
		ControlEffectiveness.get();
	}
	
	$("#edit_control").click(function(){
		ControlEffectiveness.edit();
		return false;
	});
	
	$("#change_control").click(function() {
		ControlEffectiveness.change();
		return false;
	});
		
	$("#add_control").click(function() {
		ControlEffectiveness.save();
		return false;
	})						  
	
});

ControlEffectiveness		= {
		
		get				: function()
		{
			$.get("controller.php?action=getControl", function( data ) {
				$.each( data, function( index, val){
					ControlEffectiveness.display( val );					
				});	
			},"json");
		} , 
		
		save			: function()
		{
			var ctrl_shortcode	= $("#control_shortcode").val();		
			var control_effect	= $("#control_effect").val();
			var quali_critera 	= $("#quali_critera").val();
			var control_rating	= $("#control_rating").val();
			var color 			= $("#control_color").val();
			if ( ctrl_shortcode == "") {
				jsDisplayResult("error", "error", "Please enter the short code for this control . . .");
				return false;
			} else if( control_effect == "" ) {
				jsDisplayResult("error", "error", "Please enter the control effectiveness . . .");
				return false;
			} else if ( quali_critera == "" ) {
				jsDisplayResult("error", "error", "Please enter the qualification criteria for this control . . .");
				return false;
			} else if( control_rating == "" ) {
				jsDisplayResult("error", "error", "Please enter the qualifaction criteria for this control . . .");
				return false;			
			} else {
				jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=newControl",
					   {
						  ctrl			: control_effect,
						  qual			: quali_critera,
						  rating		: control_rating,
						  clr			: color,
						  ctrl_shortcode: ctrl_shortcode
						}
					   , function( retData ) {
						if( retData > 0 ) {   
							$("#control_shortcode").val("")
							$("#control_effect").val("")
							$("#quali_critera").val("")
							$("#control_rating").val("")
							jsDisplayResult("ok", "ok", "Control Effectiveness save successfully. . .");
							var data = { id : retData, shortcode : ctrl_shortcode, effectiveness : control_effect, qualification_criteria : quali_critera , control_rating : rating, color : color, status : 1 };
							ControlEffectiveness.display( data );					
						} else {
							jsDisplayResult("error", "error", "There was an error saving , please try again. . .");
						}
					
				},"json")	
			}
		} , 
		
		edit			: function()
		{
				
			var ctrl_shortcode	= $("#control_shortcode").val();		
			var control_effect	= $("#control_effect").val();
			var quali_critera 	= $("#quali_critera").val();
			var control_rating	= $("#control_rating").val();
			var color 			= $("#control_color").val();

			if ( ctrl_shortcode == "") {
				jsDisplayResult("error", "error", "Please enter the short code for this control");
				return false;
			} else if( control_effect == "" ) {
				jsDisplayResult("error", "error", "Please enter the control effectiveness");
				return false;
			} else if ( quali_critera == "" ) {
				jsDisplayResult("error", "error", "Please enter the qualification criteria for this control");
				return false;
			} else if( control_rating == "" ) {
				jsDisplayResult("error", "error", "Please enter the control rating for this control");
				return false;			
			} else {
				jsDisplayResult("info", "info", "Updating control . . . <img src='../images/loaderA32.gif' >");
				$.post( "controller.php?action=updateControl",
					   {
						  id			: $("#control_id").val(),
						  ctrl			: control_effect,
						  qual			: quali_critera,
						  rating		: control_rating,
						  clr			: color,
						  ctrl_shortcode: ctrl_shortcode
						}
					   , function( retData ) {
							if( retData == 1 ) {
								jsDisplayResult("ok", "ok", "Control Effectiveness successfully updated . . . ");						
							} else if( retData == 0 ){
								jsDisplayResult("info", "info", "Control Effectiveness not changed . . . ");
							} else {
								jsDisplayResult("error", "error", "Error updating the control effectiveness . . . ");
							}					    
				},"json")	
			}								  
		},

		
		change			: function()
		{		
			jsDisplayResult("ok", "ok", "Updating control . . . ");
			$.post( "controller.php?action=changeControl", 
				   { 
				   		id		: $("#control_id").val(),
						status  : $("#control_status :selected").val()
				   }, function( deactData ) {
				if( deactData == 1){
					jsDisplayResult("ok", "ok", "Control effectveness status changed. . . ");
				} else if( deactData == 0){
					jsDisplayResult("info", "info", "No change was made to the control effectveness. . . ");
				} else {
					jsDisplayResult("error", "error", "Error updating control . . . ");
				}										 
			})							
		} , 
		
		display			: function(val)
		{
			$("#control_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.shortcode}))			  
			  .append($("<td />",{html:val.effectiveness}))
			  .append($("<td />",{html:val.qualification_criteria}))
			  .append($("<td />",{html:val.rating}))
			  .append($("<td />")
				.append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))	  
			  )
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"ctrledit_"+val.id, name:"ctrledit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"ctrldel_"+val.id, name:"ctrldel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"ctrlact_"+val.id, name:"ctrlact_"+val.id}))	
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ),value:"Inactivate", id:"ctrldeact_"+val.id, name:"ctrldeact_"+val.id}))							
			   )
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))
			   )
			)
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_control_status.php?id="+val.id+"&";
				return false;										  
			});			
			$("#ctrledit_"+val.id).live( "click", function() {
				document.location.href = "edit_control.php?id="+val.id+"&";
				return false;										  
			});
			$("#ctrldel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this percieved control effectiveness")) {								  
					$.post( "controller.php?action=changeControl",
						   { id 	: val.id,
						   	 status : 2
						   }, function( delData ) {
						if( delData == 1){
							jsDisplayResult("ok", "ok", "Control effectveness deleted . . .");
							$("#tr_"+val.id).fadeOut();
						} else if( delData == 0){
							jsDisplayResult("info", "info", "No change was made to the control effectveness . . .");							
						} else {
							jsDisplayResult("error", "error", "Error deleting control effectveness . . .");
						}										 
					});
				}
				return false;										  
			});
			$("#ctrlact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=ctrlactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						jsDisplayResult("ok", "ok", "Control effectveness deactivated . . .");
					} else if( actData == 0){
						jsDisplayResult("info", "info", "No change was made to the control effectveness . . .");
					} else {
						jsDisplayResult("error", "error", "Error updating . . .");
					}										 
				})															 
				return false;										  
			});

		}
		
}