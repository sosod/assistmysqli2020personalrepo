// JavaScript Document
$(function(){
	/**	   
		Get all the directorates
	**/
	$("#subdir_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getUsers", function( data ) {							   
		$.each( data, function( index, val) {
			$("#subdir_user")
				.append($("<option />",{text:val.tkname+" "+val.tksurname, value:val.tkid}))
		});											   
	},"json");
	
	$.get( "controller.php?action=getDirectorate", function( data ) {
		$.each( data, function( index, val) {
		   $("#subdir_dpt")
			 .append($("<option />",{text:val.department, value:val.id}))   
		});										   
	},"json");

	$.get( "controller.php?action=getSubDirectorate", function( data ) {
		$("#subdir_message").html("")
		var message = $("#subdir_message")
		$.each( data, function( index, val) {
		   $("#sub_directorate_table")
		   .append($("<tr />")
			 .append($("<td />",{html:val.id}))		 
			 .append($("<td />",{html:val.shortcode}))		 						 
			 .append($("<td />",{html:val.department}))		 
			 .append($("<td />",{html:val.division}))	
			 .append($("<td />",{html:val.tkname+" "+val.tksurname}))
			 .append($("<td />",{html:(val.assign_actions == 1 ? "Yes" : "No")}))				 
			 .append($("<td />")
				.append($("<input />",{type:"submit", name:"subdire_edit_"+val.id, id:"subdire_edit_"+val.id, value:"Edit"}))
				.append($("<input />",{type:"submit", name:"subdire_del_"+val.id, id:"subdire_del_"+val.id, value:"Del"}))							
			 )
			 .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "InActive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), name:"subdire_activate_"+val.id, id:"subdire_activate_"+val.id, value:"Activate"}))
				//.append($("<input />",{type:(val.active==0 ? "hidden"  : "submit" ), name:"subdire_inactivate_"+val.id, id:"subdire_inactivate_"+val.id, value:"InActivate"}))							
			 )		
			 .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		   
			 )
			)
		   	$("#change_"+val.id).live( "click", function() {
				document.location.href  = "change_subdirectorate_status.php?id="+val.id;
				return false;										  
			});		   
		   	$("#subdire_edit_"+val.id).live( "click", function() {
				document.location.href  = "edit_subdirectorate.php?id="+val.id;
				return false;										  
			});
			$("#subdire_del_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this sub directorate ")) {											  
					$.post( "controller.php?action=changeSubDirStatus", 
						   { 
						   id		: val.id ,
						   status   : 0
						   }, function( delData ) {
						if( delData == 1){
							message.html("Sub Directorate structure deleted").animate({opacity:"0.0"},4000);
						} else if( delData == 0){
							message.html("No change was made to the  sub directorate structure").animate({opacity:"0.0"},4000);
						} else {
							message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
						}										 
					});
				}
				return false;										  
			});
			$("#subdire_activate_"+val.id).live( "click", function() {
				$.post( "controller.php?action=subdir_activate", {id:val.id}, function( actData ) {
					if( actData == 1){
						message.html("Sub Directorate structure activated").animate({opacity:"0.0"},4000);
					} else if( actData == 0){
						message.html("No change was made to the  sub directorate structure").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}											 
				})															 
				return false;										  
			});
		   
		});										   
	},"json");
	
	$("#add_subdir").click(function(){
	 	var message 	   = $("#subdir_message");
		var sub_shortcode  = $("#sub_shortcode").val();
		var sub_department = $("#subdir_dpt :selected");
		var sub_division   = $("#subdir_division").val();
		var sub_user	   = $("#subdir_user :selected");
	
		if( sub_shortcode == "") {
			message.html("Please enter the short code for this  sub directorate");
			return false;
		} else if( sub_department.val() == "") {
			message.html("Please select department name for this sub directorate");
			return false;			
		} else if( sub_division == "" ) {
			message.html("Please enter the division for this sub directorate");
			return false;			
		} else if( sub_user.val() == "" ) {
			message.html("Please select user responsible for this sub directorate");
			return false;			
		} else {
			$.post( "controller.php?action=newSubDirectorate",
				   {
					   	shortcode	: sub_shortcode,
						dpt			: sub_department.val(),
						fxn			: sub_division,
						user		: sub_user.val()
					},
				   function( retData ) {
					if( retData > 0 ) {
						$("#sub_shortcode").val()
						$("#sub_shortcode").val()
						$("#sub_shortcode").val()
						$("#sub_shortcode").val()
						message.html("Sub Directorate saved succesifully ....")
					   $("#sub_directorate_table")
					   .append($("<tr />")
						 .append($("<td />",{html:retData}))		 
						 .append($("<td />",{html:sub_shortcode}))		 						 
						 .append($("<td />",{html:sub_department.text()}))		 
						 .append($("<td />",{html:sub_division}))	
						 .append($("<td />",{html:sub_user.text()}))
						 .append($("<td />")
						  	.append($("<input />",{type:"submit", name:"subdire_edit_"+retData, id:"subdire_edit_"+retData, value:"Edit"}))
						  	.append($("<input />",{type:"submit", name:"subdire_del_"+retData, id:"subdire_del_"+retData, value:"Del"}))							
						 )
						 .append($("<td />",{html:"<b>Active</b>"})
						  	//.append($("<input />",{type:"submit", name:"subdire_activate", id:"subdire_activate", value:"Activate"}))
						  	//.append($("<input />",{type:"submit", name:"subdire_inactivate", id:"subdire_inactivate", value:"InActivate"}))							
						 )
						 .append($("<td />")
							.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))	   
						  )
					   )
						$("#change_"+retData).live( "click", function() {
							document.location.href  = "change_subdirectorate_status.php?id="+retData;
							return false;										  
						});		   
						$("#subdire_edit_"+retData).live( "click", function() {
							document.location.href  = "edit_subdirectorate.php?id="+retData;
							return false;										  
						});					   
					} else {
						message.html("There was an error saving , please try again");							
					}
			}, "json")
		}
		return false;
	});	
		
})