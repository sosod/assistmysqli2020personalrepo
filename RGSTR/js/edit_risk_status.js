// JavaScript Document
$(function(){


	$("#edit_status").click(function(){

		var risk_status = $("#status").val();
		var client_term = $("#client_term").val();
		var color 		= $("#risktype_color").val(); 
		var message 	= $("#edit_risk_status_message");
		
		if	( risk_status == "" ) {
			jsDisplayResult("error", "error", "Please enter default terminology.");
			return false;
		} else if ( client_term == "") {
			jsDisplayResult("error", "error", "Please enter your terminology.");			
			return false;	
		} else {
			jsDisplayResult("info", "info", "Saving... <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=editRiskStatus" , { 
				   id					: $("#risk_status_id").val(),
				   name					: risk_status,
				   client_terminology   : client_term, 
				   color				: color
			}, function( retStat ) {
				if( retStat == 1 ) {
					//jsDisplayResult("ok", "ok", "Status successfully updated . . .");
					//message.html(" .. ");						
					document.location.href = "setup.php?id="+$("#field").val()+"&r[]=ok&r[]=Changes saved successfully.";
				} else if( retStat == 0 ){
					jsDisplayResult("info", "info", "No change was found.");
				} else {
					jsDisplayResult("error", "error", "An error occurred while trying to save.  Please try again.");
				}
			},"json");
		}
		return false;									 
	})
	
	$("#cancel_status").click(function(){
		history.back();
		return false;								   
	})
	
});