<?php
@session_start();
include_once("../inc/init.php");

include_once("../../library/class/assist_dbconn.php");
include_once("../../library/class/assist_db.php");
include_once("../../library/class/assist_helper.php");
include_once("../../library/class/assist.php");

$def = new Defaults("","");
$risk_object_name = $def->getObjectName("OBJECT"); //echo $risk_object_name;
$action_object_name = $def->getObjectName("ACTION"); //echo $action_object_name;

$result = array();

$db = new ASSIST_DB();
$me = new ASSIST();

switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {

case "calculateExposure":
	$irr = $_REQUEST['irr'];
	$rr = $_REQUEST['rr'];
	
	$sql = "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure WHERE rating_from <= $irr AND rating_to >= $irr ORDER BY id DESC ";
	$i = $db->mysql_fetch_one($sql);
	$result[0] = "<span style='background-color: #".$i['color']."; padding: 2px 4px 2px 4px;'>".$i['magnitude']."</span>";
	//$result[0] = "Test";
	$sql = "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure WHERE rating_from <= $rr AND (rating_to+0.99999999999999999) >= $rr ORDER BY id DESC";
	$r = $db->mysql_fetch_one($sql);
	$result[1] = "<span style='background-color: #".$r['color']."; padding: 2px 4px 2px 4px;'>".$r['magnitude']."</span>";
	//$result[1] = "Test2";
	break;


case "getRR":
	$rr = $_REQUEST['rating'];
	$sql = "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure WHERE rating_from <= $rr AND active = ".ResidualRisk::ACTIVE." ORDER BY rating_from DESC";
	$r = $db->mysql_fetch_one($sql);
	$result[0] = $r['magnitude'];
	$result[1] = $r['color'];
	break;
case "getIR":
	$ir = $_REQUEST['rating'];
	$sql = "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure WHERE rating_from <= $ir ORDER BY rating_from DESC";
	$r = $db->mysql_fetch_one($sql);
	$result[0] = $r['magnitude'];
	$result[1] = $r['color'];
	break;
	
case "getImpactRating":
	$imp = new Impact( "", "", "", "", "");
	$result = $imp->getAImpact( $_REQUEST['id']  );
	break;
case "getLikelihoodRating":
	$lk = new Likelihood( "", "", "", "", "","");
	$result = $lk->getALikelihood( $_REQUEST['id']  );
	break;	

	
	

case "newRiskUpdate":	
	$result = array("text" => "test ".date("d F y H:i:s"), "error" => true, "update" => true);
	$risk_id = $_REQUEST['id'];
	$nameObj = new Naming();
	$names = $nameObj->getReportingNaming();
	$defObj = new Defaults();
	//AA-673 Risk 1s  Risk functions not accessible [SD] 11/08/2021
	$objectName = $defObj->getObjectName("OBJECT");
	$attachments = "";
	$riskObj = new Risk("" , "", "", "", $_REQUEST['impact'], $_REQUEST['impact_rating'], $_REQUEST['likelihood'], $_REQUEST['likelihood_rating'],
				  $_REQUEST['current_controls'], $_REQUEST['effectiveness'], $_REQUEST['control_effectiveness_rating'], "", $attachments
				 );		
        $riskOld  		    = $riskObj -> getRiskDetail($risk_id);
		$formChanges = $_REQUEST;
		unset($formChanges['action']);
		//$formChanges['current_controls'] = implode("_",array_filter($formChanges['current_controls']));
        $changes 		= $riskObj -> getRiskChange($formChanges, $riskOld);

	//$changeRes 			= riskChange( $_REQUEST );
/*	$attachments = "";
	if(isset($_SESSION['uploads']['edits']) && !empty($_SESSION['uploads']['edits']) )
	{
		$attachments = serialize( $_SESSION['uploads']['edits'] );
	}		
	*/
	$changeArr 	 = $changes['changes'];
	$changeArr['user'] = $me->getUserName();
	if(isset($changeArr['status']['to'])) {
		$changeArr['currentstatus'] = $changeArr['status']['to'];
	} else {
		$changeArr['currentstatus'] = $riskOld['risk_status'];
	}
	$changeMessage  = $changes['change_message'];				 
	$id             = $riskObj -> saveRiskUpdate( $_REQUEST['id'], $_REQUEST['status'], $_REQUEST['response'], base64_encode(serialize($changeArr)) );
	if($id > 0)  {
		$text 		 = " The update to $objectName ".Risk::REFTAG.$_REQUEST['id']." has been successfully saved.";//.serialize($formChanges);
		//Get recipients emails
			$user = new UserAccess();
				//Risk Owners (Sub-Dir Admins)
				$to = $user->getRiskOwnerEmails($riskOld['sub_id']);
				//Risk Managers
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
				$cc2 = array();
				foreach($ccm as $c) {
					$cc2[] = $c['email'];
				}
				$cc = implode(",",$cc2);
			//Get From
				$from = $user->getCurrentUser();
			//Subject
			$subject = "".$risk_object_name." ".$risk_id." Updated";
			//Create message
        $riskNew = $riskObj -> getRiskDetail($risk_id);
		$email_details = $changes['email_change_message'];
//foreach($riskNew as $key=>$detail) {
//	$email_details.=chr(10).$key." = ".$detail;
//}
foreach($email_details as $key => $e) {
	$email_details[$key] = " - ".$e.";";
}
			$rr = $riskObj->getCalculation("RRE");
			$ir = $riskObj->getCalculation("IRE");
			//$msg = explode("\r\n",$changeMessage);
$message = $from['name']." has updated ".$risk_object_name." ".$risk_id.".

The update is:
".implode(chr(10),$email_details)."

The complete $risk_object_name details are as follows:
".$names['risk_item'].": ".$risk_id."
".$names['risk_type'].": ".$riskNew['risk_type']."
".$names['risk_level'].": ".$riskNew['risk_level']."
".$names['risk_category'].": ".$riskNew['risk_category']."
".$names['risk_description'].": ".$riskNew['description']."
".$names['risk_background'].": ".$riskNew['background']."
".$names['impact'].": ".$riskNew['risk_impact']." (".$names['impact_rating'].": ".$riskNew['impact_rating'].")
".$names['likelihood'].": ".$riskNew['risk_likelihood']." (".$names['likelihood_rating'].": ".$riskNew['likelihood_rating'].")
".$names['inherent_risk_exposure'].": ".$ir['name']." (".$names['inherent_risk_rating'].": ".$ir['rating'].")
".$names['percieved_control_effective'].": ".$riskNew['control_rating']." (".$names["control_rating"].": ".$riskNew['control_effectiveness_rating'].")
".$names['residual_risk_exposure'].": ".$rr['name']." (".$names['residual_risk'].": ".$rr['rating'].")
".$names['current_controls'].": ".implode("; ",explode("_",$riskNew['current_controls']))."
".$names['financial_exposure'].": ".$riskNew['risk_financial_exposure']."
".$names['actual_financial_exposure'].": R ".$riskNew['actual_financial_exposure']."
".$names['risk_owner'].": ".$riskNew['risk_owner']."

Please log onto Assist in order to View or Update the $risk_object_name.
";
			//new ASSIST_EMAIL object
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			//send email
			$email->sendEmail();	/*
		$response["id"] = $id;
		$state 		 = false;
		$user           = new UserAccess("", "", "", "", "", "", "", "", "" );
		$emailTo		 = $user -> getRiskManagerEmails();
		//$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['user_responsible'] );			
		$email 			 = new Email();
		$email -> userFrom   = $_SESSION['tkn'];
		$email -> subject    = "Update Risk ";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message   = ""; 
		$message .= $email->userFrom." has Updated risk #".$changeRes['responsiblePerson']."\n";
		$message .= " \n";
		if($changeMessage == ""){
			$message .= "";	
		} else {
			$message .= "The following updates were made \r\n\n";
			$message .= $changeMessage."\r\n";
		}
			//blank row above Please log on line
		$message .= "Please log onto Ignite Assist in order to View or Update the risk and to assign  any required\r\n\n";
		$message .= "Actions to responsible users within ".ucwords($_SESSION['cc']);			
		$message  = nl2br($message);			
		$email -> body = $message;
		$email -> to   = $emailTo;
		$email -> from = "no-reply@ignite4u.co.za";
		if( $email -> sendEmail() ) {
			$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email has been sent";
		} else {
			$state = true;
			$text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		}*/
		$state 			  = false;
		$response = array("text" => $text, "error" => $state, "update" => true);
	} else {
		$response = array( "text" => "An error occurred while trying to save the update.  Please try again.", "error" => true, "update" => false );
	}
	if(isset($_SESSION['uploads']['edits'])){
		unset($_SESSION['uploads']['edits']);
	}		
	$result = $response;
	//echo json_encode( $response );*/
	break;	


case "newActionUpdate":
	//Attachments handled separately
        $action_id      = $_POST['id'];
        $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
        $action	        = $actObj -> getActionDetail($action_id) ;
		$riskObj		= new Risk();
		$risk			= $riskObj->getRiskDetail($action['risk_id']);
        $statusObj 		= new ActionStatus("","", "");
        $statuses 		= $statusObj -> getStatuses();
        $nameObj 	    = new Naming();
		$names = $nameObj->getReportingNaming("action");
		$rnames = $nameObj->getReportingNaming();
        $attachment     = array();
        $change_arr 	= array();
        $change_msg	    = "";
		$email_msg = array();
        $update_data    = array();
        $attStr 		= "";
        if(isset($_POST['description']) && !empty($_POST['description'])) {
            $change_arr['response'] = $_POST['description'];
            $m            = "Response: ".$_POST['description'];
            $change_msg                 .= $m."\r\n";
            $email_msg[] = " - ".$m.";";
        }
        if($_POST['status'] !== $action['status']) {
            $from                        = array_key_exists($action['status']-1,$statuses) ? $statuses[$action['status']-1]['name'] : ''; // -1 because $statuses array starts from 0
            $to                          = array_key_exists($_POST['status']-1,$statuses) ? $statuses[$_POST['status']-1]['name'] : '';
            $update_data['status']       = $_POST['status'];
            $m                 = $names['action_status']." changed to ".$to." from ".$from;
            $change_msg                 .= $m."\r\n";
            $email_msg[] = " - ".$m.";";
            $change_arr['action_status'] = array('from' => $from, 'to' => $to);
            $change_arr['currentstatus'] = $to;
        }
        if($_POST['progress'] != $action['progress']) {
            $from                          = $action['progress'];
            $to                            = $_POST['progress'];
            $update_data['progress']       = $_POST['progress'];
            $m                   = $names['progress']." changed to ".$to."% from ".$from."%";
            $change_msg                 .= $m."\r\n";
            $email_msg[] = " - ".$m.";";
            $change_arr['action_progress'] = array('from' => $from."%", 'to' => $to."%");
        }
        if(isset($_POST['action_on'])) {
            if(date("d-M-Y",strtotime($_POST['action_on'])) != date("d-M-Y",strtotime($action['action_on']))) {
                $from                  = (strtotime($action['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($action['action_on'])) );
                $to                    = (strtotime($_POST['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['action_on'])) );
                if(strtotime($from) !== strtotime($to)) {
                    $change_arr['action_on']  = array('from' => $from, 'to' => $to);
                    $m              = "Action on changed to ".$to." from ".$from;
            $change_msg                 .= $m."\r\n";
            $email_msg[] = " - ".$m.";";
                    $update_data['action_on'] = date("Y-m-d", strtotime($_POST['action_on']));
                }
            }
        }
        if(date("d-M-Y",strtotime($_POST['remindon'])) != date("d-M-Y",strtotime($action['remindon']))) {
            $from                           = strtotime($action['remindon'])==0 ? "-" : date("d-M-Y",strtotime($action['remindon']));
            $to                             = strtotime($_POST['remindon'])==0 ? "-" : date("d-M-Y",strtotime($_POST['remindon']));
            $update_data['remindon']        = $_POST['remindon'];
            $m                    = "Reminder changed to ".$to." from ".$from;
            $change_msg                 .= $m."\r\n";
            $email_msg[] = " - ".$m.";";
            $change_arr['action_remind_on'] = array('from' => $from, 'to' => $to);
        }


        if(isset($_POST['approval']) && $_POST['approval'] == 'on') {
            $change_arr['approval'] = "Requested sending of approval email.";
            $change_msg            .= "Requested sending of approval email.\r\n";
        }

        if($_POST['progress'] == 100 && $_POST['status'] == 3) {
            $change_arr['approval_status']  = "Action moves to awaiting approval .";
            $m                    = "The ".$action_object_name." is now awaiting approval.";
            $change_msg                 .= $m."\r\n";
            $email_msg[] = " - ".$m.";";
            if(($action['action_status'] & RiskAction::ACTION_AWAITING) != RiskAction::ACTION_AWAITING)
            {
                $update_data['action_status']   = $action['action_status'] + RiskAction::ACTION_AWAITING;
            }
        }

        $updates = array();
        if(!empty($change_arr)) {
            if(!isset($change_arr['currentstatus'])) {
                $change_arr['currentstatus'] = array_key_exists($action['status'],$statuses) ? $statuses[$action['status']-1]['name'] : '';
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
            $updates['insertuser'] = $_SESSION['tid'];
        }
        $id                      = $actObj -> updateAction($action_id, $update_data, $updates);
        $response  = array();
        if($id > 0) {
            $response["id"] 	 = $id;
            $error 				 = false;
            $text 				 = " New update for $action_object_name ".$action_id." has been successfully saved.".(isset($update_data['action_status']) && (($update_data['action_status'] & RiskAction::ACTION_AWAITING) == RiskAction::ACTION_AWAITING) ? "  The ".$action_object_name." has been sent for approval." :"");
            /*$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
            $emailTo 		     = $user -> getRiskManagerEmails();
            $email 			     = new Email();

            $email -> userFrom  = $_SESSION['tkn'];
            $email -> subject   = " Update $action_object_name ";
            //Object is something like Risk or Action etc.  Object_id is the reference of that object
            $message            = "";
            //$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
            $message           .= " \n";	//blank row above Please log on line
            $message           .= $_SESSION['tkn']." Updated $action_object_name #".$action_id." related to $risk_object_name #".$action['risk_id']."\r\n<br /><br />";
            if(!empty($change_msg))
            {
                $message .= $change_msg;
            }
            if(!empty($att_change_str))
            {
                $message .= $att_change_str;
            }

            $message .= "\r\nPlease log onto Assist in order to View further information \n";
            $message           = nl2br($message);
            $email -> body     = $message;
            $email_to          = "";
            if(!empty($mailTo))
            {
                $email_to = $emailTo.",";
            }
            $email_to          = $action['tkemail'];
            $email -> to 	  .= $email_to;
            //$email -> to 	  = $email_to;
            $email -> from     = "no-reply@ignite4u.co.za";
            if($email -> sendEmail())
            {
                $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
            } else {
                $error = true;
                $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
            }*/
				$user = new UserAccess();
					$to = $user->getActionOwnerEmail($action['owner'],true);
					$from = $user->getCurrentUser();
$msg = implode(chr(10),$email_msg);
$status_st = array_key_exists($_REQUEST['status'],$statuses) ? $statuses[$_REQUEST['status']]: array() ;
$status_name = isset($status_st['name']) ? $status_st['name'] : "";
$email_message = $from['name']." has made an update to ".$action_object_name." ".$action_id.".

The update is:
$msg
_ATTACH_
The complete $action_object_name details are:
".$names['risk_action'].": ".$action['action']."
".$names['deliverable'].": ".$action['deliverable']."
".$names['action_owner'].": ".$to['name']."
".$names['timescale'].": ".$action['timescale']."
".$names['deadline'].": ".$action['deadline']."
".$names['action_status'].": ".$status_name." (".$_REQUEST['progress']."%)
Reminder: ".(isset($_REQUEST['remindon']) && strlen($_REQUEST['remindon'])>0 ? $_REQUEST['remindon'] : "-")."

The associated $risk_object_name details are as follows:
".$rnames['risk_item'].": ".$action['risk_id']."
".$rnames['risk_description'].": ".$risk['description']."
".$rnames['risk_owner'].": ".$risk['risk_owner']."

Please log onto Assist in order to view the $action_object_name.
";
            $response = array("text" => $text, "error" => $error, 'updated' => true,'email_message'=>$email_message);
        } elseif($id == 0){
            $response = array("text" => "No change made to the $action_object_name", "error" => false, "updated" => false);
        } else {
            $response = array("text" => "Error saving the $action_object_name update", "error" => true, "updated" => false);
        }
		$result = $response;
	break;
	
case "getTypeCategory":
		$riskcat = new RiskCategory();
		$result = $riskcat->getActiveTypeCategory($_REQUEST['act_id']);
	break;		
	
	
case "DELETE":
	$nameObj = new Naming();
	$names = $nameObj->getReportingNaming();
	$risk_id = $_REQUEST['id'];
	$riskObj = new Risk();
	$riskOld = $riskObj->getRiskDetail($risk_id);
	$acts = $riskObj->getAssociatedActions($risk_id);
	//$r = 1;
	$r = $riskObj->deleteRisk($_REQUEST['id']);
	if($r>0) {
		if(count($riskOld)>0) {
			//Get recipients emails
			$user = new UserAccess();
				//Risk Owners (Sub-Dir Admins)
				$to = $user->getRiskOwnerEmails($riskOld['sub_id']);
				//Risk Managers
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
					$cc2 = array();
					foreach($ccm as $c) {
						$cc2[] = $c['email'];
					}
					$cc = implode(",",$cc2);
				//Get From
				$from = $user->getCurrentUser();
			//Subject
			$subject = "".$risk_object_name." ".$risk_id." Deleted";
			//Create message
$message = $risk_object_name." ".$risk_id." has been deleted by ".$from['name'].".
".$names['risk_description'].": ".$riskOld['description']."

"; 
			if(count($acts)>1) {
$message.="The associated ".$action_object_name."s also deleted are:
";
				foreach($acts as $a) {
$message.=" - ".$a['id'].": ".$a['action']." [Assigned to: ".$a['owner']."]
";
				}	
			} elseif(count($acts)==1) {
				$a = $acts[0];
$message.="The associated ".$action_object_name." also deleted is:
".$a['id'].": ".$a['action']." [Assigned to: ".$a['owner']."]";
			}
			//send Email
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			$email->sendEmail();
		}
		$result = array("ok",$risk_object_name." ".$risk_id." deleted successfully.");
	} else {
		$result = array("error","An error occurred.  Please try again.");
	}

	//$result = array("error","test");
	break;
	
	
	
	
	
	
	
	
	/*********************
	APPROVAL 
	*******************/
case "ApproveGetRisks":
	$sub_ids = isset($_REQUEST['s']) ? $_REQUEST['s'] :0;
	$page = $_REQUEST['page'];
	$folder = $_REQUEST['folder'];
	$riskObj = new RGSTR_RISK();
	if($page=="approve" || $page==($folder."_approve")) {
		$risks = $riskObj->getRisksWithActionsForApproval($sub_ids,$folder);
	} else {
		$risks = $riskObj->getRisksWithApprovedActions($sub_ids,$folder);
	}
	//echo json_encode($risks);
	$result = $risks;
	break;
case "ApproveGetActionOwnersByRisk":
	$risk_id = isset($_REQUEST['r']) ? $_REQUEST['r'] : 0;
	$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
	$folder = isset($_REQUEST['folder']) ? $_REQUEST['folder'] : "";
	$actionObj = new RGSTR_ACTION();
	if($page=="approve" || $page==($folder."_approve")) {
		$actions = $actionObj->getActionOwnersForApproval(array(),$risk_id,$folder);
	} else {
		$actions = $actionObj->getApprovedActionOwners(array(),$risk_id,$folder);
	}
	//echo json_encode($actions);
	$result = $actions;
	break;

case "getApprovalActions":
	$action_type = $_REQUEST['action_type'];
	$risk_owner = $_REQUEST['risk_owner'];
	$risk = $_REQUEST['risk'];
	$action_owner = $_REQUEST['action_owner'];
	$folder = $_REQUEST['folder'];
	$result = array();

	$actionObj = new RiskAction();
	if($action_type == "WAITING") {
		$result = $actionObj->getActionsWaitingApproval($risk_owner,$risk,$action_owner,$folder);
	} else {
		$result = $actionObj->getActionsAlreadyApproved($risk_owner,$risk,$action_owner,$folder);
	}
	break;
case "ApproveButtonClick":
	$get_what = $_REQUEST['get_what'];
	$action_id = $_REQUEST['action_id'];
	$result = array();
	$nameObj = new Naming();
	$anames = $nameObj->getReportingNaming("action");
	$actStatusObj = new ActionStatus();
	$inprogress_status = $actStatusObj->getAStatus(2);
	$inprogress_name = $inprogress_status['value'];
	switch($get_what) {
		case "viewlogsform":
			$result['title'] = "Activity Log: ".$action_object_name." ".$action_id;
			$actObj = new RiskAction();
			$logs = $actObj->getCompleteActionLogs($action_id);
			$anames['action_on'] = "Action On";
			$html = "<h1>Activity Log: ".$action_object_name." ".$action_id."</h1>
			<table style='margin-bottom: 20px' id=tbl_logs>
				<tr>
					<th>Date</th>
					<th>User</th>
					<th>Changes</th>
					<th>Status</th>
				</tr>";
			foreach($logs as $l) {
				$changes = array();
				if(isset($l['change_arr']['response']) && strlen($l['change_arr']['response'])>0) {
					$changes[] = "Response: ".$l['change_arr']['response'];
					unset($l['change_arr']['response']);
				}
				foreach($l['change_arr'] as $key => $c) {
					$s = "";
					if($key=="attachments") {
						$x = array();
						foreach($c as $a) {
							$x[] = $a;
						}
						$s = implode("<br />",$x);
					} elseif(is_array($c)) {
						$n = isset($anames[$key]) ? $anames[$key] : $key;
						$s = $n." changed to ".$c['to']." from ".$c['from'];
						/*foreach($c as $k=>$d) {
							$m[] = $k."=>".$d;
						}
						$s = implode("<br />",$m);*/
					} elseif(strlen($c)>0) {
						if(isset($anames[$key])) {
							$s = $anames[$key].": ".$c;
						} else {
							$s = $c;
						}
					}
					if(strlen($s)>0) {
						$changes[] = $s;
					}
				}
				if(count($changes)>0) {
					$html.="<tr>
						<td class=center>".date("d-M-Y H:i:s",strtotime($l['insertdate']))."</td>
						<td>".$l['user']."</td>
						<td>";
					$html.=implode("<br />",$changes)."</td>
						<td class=center>".$l['currentstatus']."</td>
					</tr>";
				}
			}
			$rgstrActObj = new RGSTR_ACTION();
			$attachments = $rgstrActObj->displayAttachmentList($action_id,false,array(),false);
			if(strlen($attachments)>0) {
				$html.="<tr><td colspan=4>".$attachments."</td></tr>";
			} else {
				$html.="<tr><td colspan=4>No attachments found.</td></tr>";
			}
			$html.="</table>";
			$result['html'] = $html;
			break;
		case "approveactionform":
			$result['title'] = "Approve/Decline: ".$action_object_name." ".$action_id;
			$html = "<h1>Approve / Decline: ".$action_object_name." ".$action_id."</h1>
			<div style='border: 1px solid #fe9900;margin-bottom: 10px;'>".ASSIST_HELPER::getDisplayResult(array("info","Please note:<ul><li>Declining the ".$action_object_name." will return the ".$anames['action_status']." to ".$inprogress_name." and set the ".$anames['progress']." to 99%.</li><li>Approving the ".$action_object_name." will lock the ".$action_object_name." so that no further updates or edits may be performed.</li><li>Approvals can be reversed by clicking the 'Unlock' button on the 'Approved ".$action_object_name."' page - this will have the same effect as Declining the ".$action_object_name.".</li></ul>"))."</div>
			<table style='margin-bottom: 20px' class=form width=100%>
				<tr>
					<th>Response:</th>
					<td><textarea id=approve_response rows=10 cols=80></textarea></td>
				</tr>
			</table>";
			$result['html'] = $html;
			break;
		case "unlockactionform":
			$result['title'] = "Unlock: ".$action_object_name." ".$action_id;
			$html = "<h1>Unlock: ".$action_object_name." ".$action_id."</h1>
			<div style='border: 1px solid #fe9900;margin-bottom: 10px;'>".ASSIST_HELPER::getDisplayResult(array("info","Please note: Unlocking the ".$action_object_name." will return the ".$anames['action_status']." to ".$inprogress_name." and set the ".$anames['progress']." to 99%.  The ".$action_object_name." will be available for updating and editing again."))."</div>
			<table style='margin-bottom: 20px' class=form width=100%>
				<tr>
					<th>Response:</th>
					<td><textarea id=approve_response rows=10 cols=80></textarea></td>
				</tr>
			</table>";
			$result['html'] = $html;
			break;
	}
	break;
case "processApprovalForm":
	$actObj = new RiskAction();
	$action_id = $_REQUEST['action_id'];
	$response = $_REQUEST['response'];
	$approval_status = $_REQUEST['approval_status'];
	switch($approval_status) {
		case "APPROVE":
			$r = $actObj->approveAction($action_id,$response);
			if($r>0) {
				$result = array("ok",$action_object_name." ".$action_id." has been successfully approved.");
			} else {
				$result = array("error","An error occurred trying to approve ".$action_object_name." ".$action_id.".");
			}
			break;
		case "DECLINE":
			$r = $actObj->declineAction($action_id,$response);
			if($r>0) {
				$result = array("ok",$action_object_name." ".$action_id." has been successfully declined.");
			} else {
				$result = array("error","An error occurred trying to decline ".$action_object_name." ".$action_id.".");
			}
			break;
		case "UNLOCK":
			$r = $actObj->unlockApprovedAction($action_id,$response);
			if($r>0) {
				$result = array("ok",$action_object_name." ".$action_id." has been successfully unlocked.");
			} else {
				$result = array("error","An error occurred trying to unlock ".$action_object_name." ".$action_id.".");
			}
			break;
	}
	
	//$result = array("info","test");
	break;
}

echo json_encode($result);



	function riskChange( $requestArr )
	{
		$risk 		    = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$riskInfo  		= $risk -> getRisk( $requestArr['id'] );
		$names 						  = new Naming();
		$header 					  = $names -> getHeaderNames();
		
		$riskStatus		= ""; //this is the status of the risk currenty   		
		//setting the calues , to ids so can compare the chnage on id, 
		$riskInfo['type'] 			= $riskInfo['typeid'];
		$riskInfo['impact']			= $riskInfo['impactid'];
		$riskInfo['likelihood']		= $riskInfo['likelihoodid'];
		$riskInfo['category']		= $riskInfo['categoryid'];
		$riskInfo['effectiveness']	= $riskInfo['effectivenessid'];
		$riskInfo['status']			= $riskInfo['statusId'];	

		$changeMessage 			= "";
		$changeArr 	   			= array();
		foreach( $requestArr as $key => $value)
		{
			if( isset($riskInfo[$key]) || array_key_exists($key, $riskInfo))
			{
				if( $riskInfo[$key] !== $value){
					if( $key == "type")
					{
						$ty 		= new RiskType("", "", "" , "");
						$fromtype	= $ty -> getARiskType( $riskInfo[$key] );
						$from    	= $fromtype['name']; 
						$totype		= $ty -> getARiskType( $value );
						$to			= $totype['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
/*
					} else if( $key == "level"){
						$lv 		= new RiskLevel("", "", "" , "");
						$fromLevel	= $lv -> getARiskLevel( $riskInfo[$key] );
						$from    	= $fromLevel['name']; 
						$toLevel	= $lv -> getARiskLevel( $value );
						$to			= $toLevel['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "financial_exposure"){
						$fn 		= new FinancialExposure("", "", "", "" , "");
						$fromFin	= $fn -> getAFinancialExposure( $riskInfo[$key] );
						$from    	= $fromFin['name']; 
						$toFin		= $fn -> getAFinancialExposure( $value );
						$to			= $toFin['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "impact"){
						$ty 		= new Impact("", "", "" , "", "");
						$fromimpact	= $ty -> getAImpact( $riskInfo[$key] );
						$from 	    = $fromimpact['assessment']; 
						$toimpact	= $ty -> getAImpact( $value );
						$to			= $toimpact['assessment'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "category"){
						$ty 		= new RiskCategory("", "", "" , "");
						$fromcat	= $ty -> getACategory( $riskInfo[$key] );
						$from 	    = $fromcat['name']; 
						$tocat		= $ty -> getACategory( $value );
						$to			= $tocat['name'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";	
					} else if( $key == "likelihood"){
						$ty 		= new Likelihood("", "", "" , "", "", "");
						$fromlike	= $ty -> getALikelihood( $riskInfo[$key] );
						$from 	    = $fromlike['assessment']; 
						$tolike		= $ty -> getALikelihood( $value );
						$to			= $tolike['assessment'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "effectiveness"){
						$ty 		= new ControlEffectiveness("", "", "" , "", "");
						$fromeffect	= $ty -> getAControlEffectiveness( $riskInfo[$key] );
						$from 	    = $fromeffect['effectiveness']; 
						$toeffect	= $ty -> getAControlEffectiveness( $value );
						$to			= $toeffect['effectiveness'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";							
					 }else if( $key == "status"){
						$ty 		= new RiskStatus("" , "", "");
						$fromstatus	= $ty -> getAStatus( $riskInfo[$key] );
						$from 	    = $fromstatus['name']; 
						$tostatus	= $ty -> getAStatus( $value );
						$to			= $tostatus['name'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
						$riskStatus		 = $to;															
					} else if( $key == "attachment"){
						$attachments = array();
						if( $riskInfo[$key] != "")
						{
							$attachments = unserialize($riskInfo[$key]);
						}
						if( isset($_SESSION['editdeleted']) && !empty($_SESSION['editdeleted']))
						{
							if(isset($attachments) && !empty($attachments))
							{	
								foreach( $_SESSION['editdeleted'] as $k => $val){
									if( isset($attachments[$k]) && !empty($attachments[$k]))
									{
										unset($attachments[$k]);
										
										$changeArr[$key][$k]    = "Deleted attachment ".$val;
										$changeMessage   	   .= "Deleted attachment ".$val."\r\n\n";
									}	
								}
								$_SESSION['uploads']['edits'] = $attachments;
							}	
						}
											
					} else if( $key == "current_controls") {
						$to   = str_replace("_", ",", $value);
						$from = str_replace("_", ",", $riskInfo[$key]);
						$changeArr[$key] = array("from" => $from, "to" => $to );
						$changeMessage  .= setHeader( $key )." changed to ".$to." from ".$from."\r\n\n";						
						
*/
					} else {
						$changeArr[$key] = array("from" => $riskInfo[$key], "to" => $value );
						//$changeMessage  .= setHeader( $key )." changed to ".$value." from ".$riskInfo[$key]."\r\n\n";	
					}
				} 
			} else if( $key == "response" ){
						$changeArr[$key] = $value."";
						$changeMessage  .= "Added Response ".$value."\r\n\n";
			}
		}	
		$changeArr['user']  = $_SESSION['tkn'];
		//set the status of the risk at this time 
		if($riskStatus == '')
		{
		   $statusObj	                = new RiskStatus('', '', '');
		   $fromstatus	                = $statusObj -> getAStatus($riskInfo['status']);
		   $changeArr['currentstatus'] = $fromstatus['name'];  	
		} else {
		   $changeArr['currentstatus'] = $riskStatus;
		}
		
		//check the added attachments and add them to the audit log array
		if( isset($_SESSION['uploads']['editadded']) && !empty($_SESSION['uploads']['editadded']))
		{
			$currentAttachments  = array();
			if( $riskInfo['attachment'] != "" )
			{
				$currentAttachments = unserialize($riskInfo['attachment']);
			} 
			$_SESSION['uploads']['edits'] = array_merge($_SESSION['uploads']['editadded'] , $currentAttachments);
			foreach( $_SESSION['uploads']['editadded'] as $key => $val)
			{
				$changeArr['attachment'][$key] = "Added attachment ".$val;
				$changeMessage  			  .= "Added attachment ".$val."\r\n\n";
			}	
		}
		if( isset($_SESSION['editdeleted']) )
		{
			unset($_SESSION['editdeleted']);
		}
		if( isset($_SESSION['uploads']['editadded']) )
		{
			unset($_SESSION['uploads']['editadded']);
		}		
		return array(
					"changeArr" 		=> $changeArr,
				 	"changeMessage" 	=> $changeMessage,
					"responsiblePerson" => (isset($riskInfo['responsible_person']) ? $riskInfo['responsible_person'] : "")
				);
	} 
	
?>