<?php
$scripts = array( 'editrisk.js', 'menu.js', 'ajaxfileupload.js' );
$styles = array( );
$page_title = "Edit |OBJECTNAME|";
require_once("../inc/header.php");
//require_once("controller.php");
$riskObj 	 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$risk        = $riskObj->getRiskDetail($_REQUEST['id']);

$riskStatus  = $riskObj -> getRiskUpdate($risk['id']);

$statusId 	 = ((empty($riskStatus) || !isset($riskStatus )) ? $risk['status'] : $riskStatus['status'] );
$type 		 = new RiskType( "", "", "", "" );
$types		 = $type-> getActiveRiskType();
$lev 		 = new RiskLevel( "", "", "", "" );
$levels		 = $lev -> getLevel();
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$imp 		 = new Impact( "", "", "", "", "");
$impacts 	 = $imp -> getImpact() ;
$fin 		 = new FinancialExposure( "", "", "", "", "");
$financial 	 = $fin -> getFinancialExposure() ;
$like 		 = new Likelihood( "", "", "", "", "","");
$likelihoods = $like -> getLikelihood() ;
$ihr		 = new ControlEffectiveness( "", "", "", "", "");
$controls    = $ihr -> getControlEffectiveness();
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUsers() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
$dire 		 = new Directorate("", "", "", "");
$direct      = $dire -> getDirectorate();

$financial_year_obj = new MasterFinancialYear();
$financial_years    = $financial_year_obj->retrieveMasterFinacialYears("active");


if(isset($_SESSION['uploads'])){
    unset($_SESSION['uploads']);
}
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();

$rkCols  = new RiskColumns();
$columns = $rkCols -> getHeaderList();


function nameFields( $fieldId , $defaultName ){
	echo getNameFields($fieldId,$defaultName);
}

function getNameFields($fieldId,$defaultName) {
	global $columns;
	if( isset( $columns[$fieldId] )) {
		return $columns[$fieldId];
	} else {
		return $defaultName;
	}
}

?>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form id="edit-risk-form" method="post" enctype="multipart/form-data">
<table align="left" border="1" id="edit_risk_table" class=form>
<tr>
    <th><?php nameFields('risk_item','Risk Item'); ?>:</th>
    <td>#<?php echo $risk['id'] ?></td>
</tr>
<tr>
    <th><?php nameFields('financial_year','Financial Year'); ?>:</th>
    <td>
        <select id="financial_year_id" name="financial_year_id">
            <option value="" selected>Unspecified</option>
            <?php foreach($financial_years as $index => $financial_year): ?>
                <option value="<?php echo $financial_year['id']; ?>" <?php echo ($financial_year['id'] == $risk['financial_year_id'] ? "selected='selected'" : ''); ?>><?php echo $financial_year['value']; ?></option>
            <?php endforeach; ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('rbap_ref','RBAP Ref'); ?>:</th>
    <td>
        <input id="rbap_ref" name="rbap_ref" type="text" value="<?php echo $risk['rbap_ref']; ?>" />
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
    <td>
        <select id="risk_type" name="risk_type">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($types as $type ){
                ?>
                <option value="<?php echo $type['id'] ?>"
                    <?php
                    if( $type['id'] == $risk['type'] )
                    {
                        ?>
                        selected="selected"
                    <?php
                    }
                    ?>
                    ><?php echo $type['name']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
    <td>
        <select id="risk_level" name="risk_level">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($levels as $level ){
                ?>
                <option value="<?php echo $level['id'] ?>"
                    <?php
                    if( $level['id'] == $risk['level'] )
                    {
                        ?>
                        selected="selected"
                    <?php
                    }
                    ?>
                    ><?php echo $level['name']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_category','Risk category'); ?>:</th>
    <td>
        <select id="risk_category" name="risk_category">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($categories as $category ){
                ?>
                <option value="<?php echo $category['id']; ?>"
                    <?php
                    if( $category['id'] == $risk['category'] )
                    {
                        ?>
                        selected="selected"
                    <?php
                    } ?>
                    ><?php echo $category['name']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
    <td><textarea class="mainbox" name="risk_description" id="risk_description"><?php echo $risk['description'] ?></textarea></td>
</tr>
<tr>
    <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
    <td><textarea class="mainbox" name="risk_background" id="risk_background"><?php echo $risk['background'] ?></textarea></td>
</tr>
<tr>
    <th><?php nameFields('cause_of_risk','Cause Of Risk'); ?>:</th>
    <td>
        <textarea id="cause_of_risk" name="cause_of_risk"><?php echo $risk['cause_of_risk']; ?></textarea>
    </td>
</tr>
<tr>
    <th><?php nameFields('reasoning_for_mitigation','Reasoning For Mitigation'); ?>:</th>
    <td>
        <textarea id="reasoning_for_mitigation" name="reasoning_for_mitigation"><?php echo $risk['reasoning_for_mitigation']; ?></textarea>
    </td>
</tr>
<tr>
    <th id=th_impact><?php nameFields('impact','Impact'); ?>:</th>
    <td>
        <select id="risk_impact" name="risk_impact">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($impacts as $impact ){
                ?>
                <option value="<?php echo $impact['id']; ?>"
                    <?php
                    if( $impact['id'] == $risk['impact'] )
                    {
                        $impRating = array( "from" => $impact['rating_from'], "to" => $impact['rating_to']);
                        ?>
                        selected="selected"
                    <?php
                    } ?>
                    ><?php echo $impact['assessment']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
    <td>
        <select id="risk_impact_rating" name="risk_impact_rating">
            <option value="">--- SELECT ---</option>
            <?php
            for( $y = $impRating['from']; $y <= $impRating['to']; $y++) {
                ?>
                <option value="<?php echo $y;  ?>" <?php if($y == $risk['impact_rating'] ) { ?> selected="selected" <?php } ?>>
                    <?php echo $y;  ?>
                </option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th id=th_likelihood><?php nameFields('likelihood','Likelihood'); ?>:</th>
    <td>
        <select id="risk_likelihood" name="risk_likelihood">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($likelihoods as $lk ){
                ?>
                <option value="<?php echo $lk['id']; ?>"
                    <?php
                    if( $lk['id'] == $risk['likelihood'] )
                    {
                        $likeRating = array("from" => $lk['rating_from'], "to" => $lk['rating_to']);
                        ?>
                        selected="selected"
                    <?php
                    } ?>><?php echo $lk['assessment']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
    <td>
        <select id="risk_likelihood_rating" name="risk_likelihood_rating">
            <option value="">--- SELECT ---</option>
            <?php
            for( $i = $likeRating['from']; $i <= $likeRating['to']; $i++ ) {
                ?>
                <option
                    value="<?php echo $i; ?>" <?php if($risk['likelihood_rating'] == $i ) { ?> selected="selected" <?php }?>>
                    <?php echo $i; ?>
                </option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('current_controls','Current Controls'); ?>:</th>
    <td>
        <?php
        $ctrls = array();
        if( $risk['current_controls'] != ""){
            $ctrls = explode("_", $risk['current_controls'] );
        }
        foreach( $ctrls as $index => $val){
            ?>
            <p><textarea name="risk_currentcontrols" id="risk_currentcontrols" class="controls"><?php echo $val; ?></textarea></p>
        <?php
        }
        ?>
        <input type="submit" name="add_another_control" id="add_another_control" value="Add Another"  />
    </td>
</tr>
<tr>
    <th id=th_risk_percieved_control><?php nameFields('percieved_control_effective','Percieved Control Effectveness'); ?>:</th>
    <td>
        <select id="risk_percieved_control" name="risk_percieved_control">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($controls as $ctrl ){
                ?>
                <option value="<?php echo $ctrl['id']; ?>"
                    <?php
                    if( $ctrl['id'] == $risk['percieved_control_effectiveness'] )
                    {
                        $ctrRating = $ctrl['rating'];
                        ?>
                        selected="selected"
                    <?php
                    } ?>><?php echo $ctrl['effectiveness']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('control_rating','Control Effectveness Rating'); ?>:</th>
    <td>
        <select id="risk_control_effectiveness_rating" name="risk_control_effectiveness_rating">
            <option value="">--- SELECT ---</option>
            <option value="<?php echo $ctrRating; ?>" selected="selected" >
                <?php echo $ctrRating; ?>
            </option>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
    <td>
        <select id="financial_exposure" name="financial_exposure">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($financial as $fina ){
                ?>
                <option value="<?php echo $fina['id']; ?>"
                    <?php
                    if( $fina['id'] == $risk['financial_exposure'] )
                    {
                        ?>
                        selected="selected"
                    <?php
                    } ?>><?php echo $fina['name']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
    <td>
        <?php echo ASSIST_HELPER::NUMBER_CURRENCY_SYMBOL."&nbsp;"; ?><input id="actual_financial_exposure" name="actual_financial_exposure" type="text" value="<?php echo  $risk['actual_financial_exposure']; ?>" /> (Numbers only)
    </td>
</tr>
<tr>
    <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
    <td>
        <input id="kpi_ref" name="kpi_ref" type="text" value="<?php echo  $risk['kpi_ref']; ?>"/>
	</td>
</tr>
<tr>
    <th><?php nameFields('risk_owner','Owner'); ?>:</th>
    <td>
        <select id="risk_user_responsible" name="risk_user_responsible" >
            <option value="">--- SELECT ---</option>
            <?php
            foreach($direct as $dir ){
                ?>
                <option value="<?php echo $dir['subid']; ?>"
                    <?php
                    if( $dir['subid'] == $risk['sub_id'] ) {
                        ?>
                        selected="selected"
                    <?php } ?>
                    ><?php echo $dir['dirtxt']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<!-- <tr>
                  <th><strong>UDF's:</strong></th>
                    <td><input type="text" name="udf" id="udf"  /></td>
                  </tr>
                  
                  <tr>
                    <th>Status:</th>
                    <td>
                    <select id="risk_status" name="risk_status">
                            <option value="">--risk status--</option>
 			<?php

			   foreach($statuses as $status ){
				?>
                            <option value="<?php echo $status['id']; ?>"
                                <?php
						if( $status['id'] == $statusId ) 
						{
							?>
                                        selected="selected"
                                        <?php
						} ?>><?php echo $status['name']; ?></option>
				<?php	
				}
				?>                             
                     </select>        
                    </td>
                  </tr>-->
<tr>
    <th><?php nameFields('','Attach documents'); ?>:</th>
    <td>
                    <span id="file_loading">
                     <?php
                     if( $risk['attachment'] !== ""){
                         echo "<ul id='attachment_list'>";
                         $attachments = unserialize( $risk['attachment'] );
                         foreach($attachments as $key => $value){
                             $id  = str_replace("fileupload_", "", $key);
                             $ext = substr( $value , strpos($value, ".")+1);
                             $file = $key.".".$ext;

                             //echo $file."&nbsp;&nbsp;&nbsp;";
                             //echo "File name ../../files/".$_SESSION['cc']."/RGSTR/risk/".$file."<br /><br />";
                             if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/".$file)){
                                 echo "<li id='parent_".$id."' class='attachmentlist'><span><a href='download.php?folder=risk&file=".$file."&new=".$value."&modref=".$_SESSION['modref']."s&content=".$ext."&company=".$_SESSION['cc']."'>".$value."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='".$value."' id='".$key."' class='remove_attach removeattach'>Remove</a></span></li>";
                             }
                         }
                         echo "</ul>";
                     }
                     ?>
                    </span>
        <input id="editfile_upload" name="editfile_upload" type="file" onchange="ajaxFileUpload(this)" />
    </td>
</tr>
<tr>
    <th></th>
    <td>
        <input type="submit" name="edit_risk" id="edit_risk" value="Save Changes" class="isubmit" />
<?php if($section=="admin") { ?>
        <span class=float><input type="submit" name="delete_risk" id="delete_risk" value="Delete Risk" class="idelete" /></span>
<?php } ?>
        <input type="hidden" name="risk_id" value="<?php echo $risk['id']; ?>" id="risk_id" class="logid" />
        <input type="hidden" name="riskid" value="<?php echo $risk['id']; ?>" id="riskid" />
    </td>
</tr>
<tr>
    <td align="left" class="noborder">
        <?php $me->displayGoBack("",""); ?>
    </td>
    <td align="right" class="noborder">
        <?php $admire_helper->displayAuditLogLink( "risk_edits" , false); ?>
        <!-- <a href="" id="viewEditsLog">View Edit Log</a> -->
    </td>
</tr>
</table>
</form>
<div id="editLogs" style="clear:both;"></div>