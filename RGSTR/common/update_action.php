<?php
require_once("../inc/header.php");

$obj_id = $_REQUEST['id'];
$rgstr = new RGSTR_ACTION();

$stat 		= new ActionStatus("", "", "");
$statuses 	=  $stat -> getOrderedStatuses();

$actionObj 	= new RiskAction("", "", "", "", "", "", "", "", "");
$action     = $actionObj -> getActionDetail($_GET['id']);
$risk_id = $action['risk_id'];

$colObj   = new ActionColumns();
$columns  = $colObj -> getHeaderList();

$names = $colObj->getReportingNaming("action");
//ASSIST_HELPER::arrPrint($columns);
$rnames = $colObj->getReportingNaming();
//ASSIST_HELPER::arrPrint($rnames);


$action_on = '';
if(strtotime($action['action_on']) > 0)
{
  $action_on = date("d-M-Y", strtotime($action['action_on']));
}

$page_src = "";

if(isset($_REQUEST['page_src'])) {
	$go_back_link = "update_actions.php?page_src=".$_REQUEST['page_src'];
	$page_src = $_REQUEST['page_src'];
} elseif(isset($_REQUEST['src']) && $_REQUEST['src']=="FRONTPAGE") {
	$page_src = $_REQUEST['src'];
}

?>
<script>
$(function(){
	$("table#update").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $me->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form name=frm_update method=post action="../common/attachment.php" language=jscript enctype="multipart/form-data">
		<input type=hidden name=section value='<?php echo $rgstr->getSection(); ?>' id=section />
		<input type=hidden name=action value='UPDATE_ATTACH' id=action />
		<input type=hidden name=after_action value='' id=after_action />
		<input type=hidden name=obj_id value='<?php echo $obj_id; ?>' id=obj_id />
		<input type=hidden name=page_src value='<?php echo $page_src; ?>' id=page_src />
		<input type=hidden name=email_message value='' id=email_message />

	<table align="left" border="1" id="update" width="100%" class="noborder">
    	<tr>
        	<td valign="top" align="left" width="50%" class="noborder">
            	<table width="100%" class=form>
                   	<tr>
                   	    <td><h4><?php echo $action_object_name; ?> Details</h4></td>
                        <td >
                          <span class=float><input type="button" name="show_risk" id="show_risk" value="View <?php echo $risk_object_name; ?>" /></span>
                        
                        </td>
                   	</tr>  
            		<tr>
            			<th><?php echo (isset($columns['action_ref']) ? $columns['action_ref'] : "Ref"); ?>:</th>
            			<td><?php echo RiskAction::REFTAG.$_GET['id']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['risk_action']; ?>:</th>
            			<td><?php echo nl2br($action['action']); ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deliverable']; ?>:</th>
            			<td><?php echo nl2br($action['deliverable']); ?></td>
            		</tr> 
            		<tr>
            			<th><?php echo $columns['action_owner']; ?>:</th>
            			<td><?php echo $action['action_owner']; ?></td>
            		</tr>             		           		
            		<tr>
            			<th><?php echo $columns['action_status']; ?>:</th>
            			<td><?php echo $action['actionstatus']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['progress']; ?>:</th>
            			<td><?php echo $action['progress']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deadline']; ?>:</th>
            			<td><?php echo $action['deadline']; ?></td>
            		</tr>            		          
            		<tr>
            			<th><?php echo $columns['timescale']; ?>:</th>
            			<td><?php echo $action['timescale']; ?></td>
            		</tr>           		  		
            		<tr id="goback_">
				       	<td align="left" class="noborder">
				        	<?php $me->displayGoBack("",""); ?>
				        </td>
				        <td class="noborder"></td>
            		</tr>
                </table>
            </td>
            <td valign="top" align="right" width="70%" class="noborder">
            	<table id="new_update" width="100%" class=form>
                	<tr>
                    	<td colspan="2"><h4>New Update</h4></td>
                    </tr>
                    <tr>
                    	<th>Response:</th>
                        <td><textarea id="description" name="description" cols="35" rows="7"></textarea></td>
                    </tr>
                    <tr>
                    	<th><?php echo $columns['action_status']; ?>:</th>
                        <td>
                        <select id="status" name="status">
                        	<option disabled=disabled >--- SELECT ---</option>
                            <?php
                            foreach($statuses as $status ) 
                            {
                            	if($action['status'] == "" || $action['status'] == "0")
                            	{
                            		$action['statusid'] = 1;
                            	}
							?>
                            	<option value="<?php echo $status['id']; ?>"
                                 <?php if($status['name']=="New" || $status['id'] == 1 ) { ?>
                                 	disabled="disabled" 
                               <?php } if($status['id'] == $action['status'] ) { ?>
                                	selected="selected" 
								<?php } ?>>
								<?php echo $status['name']; ?>
                               </option>  
                             <?php
							}
                            ?>
                        </select>
			              <input type="hidden" name="_status" id="_status" value="<?php echo $action['status']; ?>"/>
						<!-- <div class=div_info>
			              <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>Completed <?php echo $action_object_name; ?>s will be sent to the relevant person(s) for approval and cannot be updated further.</small>
			              </p>                   
						  </div> -->
                        </td>
                    </tr>
                    <tr>
                    	<th><?php echo $columns['progress']; ?>:</th>
                        <td>
                        	<input type="text" name="progress" id="progress" value="<?php echo $action['progress']; ?>" />
                            <em style="color:#FF0000">%</em>
                            <input type="hidden" name="_progress" id="_progress" value="<?php echo $action['progress']; ?>" />
                        </td>
                    </tr>
                    <tr>
                       <th>Action On:</th>
                       <td>
                          <input type="text" name="action_on" id="action_on" class="historydate" readonly="readonly" value="" />
                       </td>
                    </tr>    
                    <tr>
                    	<th>Reminder:</th>
                        <td><input type="text" name="remindon" id="remindon" value="<?php echo $action['remindon']; ?>"  class="futuredate" readonly="readonly" /></td>
                    </tr>
<tr>
	<th>Attachment:</th>
	<td><?php 
						$rgstr->displayAttachmentForm(); 
						echo $rgstr->displayAttachmentList($obj_id,true);
					?></td>
</tr>
				    </tr> 
                    <tr>
                    	<th></th>
                        <td>
                        	<input type="submit" name="save_action_update" id="save_action_update" value="Save" class="isubmit"  />
                            <input type="submit" name="cancel_action_update" id="cancel_action_update" value="Reset"   />
                            <span style="float:right;">
                            <?php $admire_helper->displayAuditLogLink("actions_update" , false); ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
	<input type="hidden" class="logid" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
</form>
</div>
<div id="view_action_updates" style="clear:both;"></div>




<?php 


ob_start();
//echo "<html><body><h3>SQL</h3><p>".$sql."<h3>Response</h3><pre>"; print_r($results); echo "</pre></body></html>";
include("../common/risk.inc.php"); 
$echo = ob_get_contents();
ob_end_clean();



?>

<div id=dlg_risk title="<?php echo $risk_object_name." ".Risk::REFTAG.$obj_id; ?>">
<?php echo $echo; ?>
</div>
<style type=text/css>
.small-button { font-size: 7pt; }
</style>
<script type=text/javascript>
$(function() {
	var screen = AssistHelper.getWindowSize();
	
	$("#dlg_risk").dialog({
		autoOpen: false,
		modal: true,
		width: "auto",
		height: (screen['height']-25)
	});
	$("#show_risk").button().addClass("small-button").click(function() {
		$("#dlg_risk").dialog("open");
	});;
});
</script>
