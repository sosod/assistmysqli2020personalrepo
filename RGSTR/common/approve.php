<?php
$scripts = array( 'menu.js');//, "jquery.ui.approve.js");
$styles = array();

$menuT = array(
	1 => array('id'=> ($folder=="admin" ? "admin_" : "")."approve",'url'=>($folder=="admin" ? "admin_" : "")."approve.php",'active'=> ($page==(($folder=="admin" ? "admin_" : "")."approve") ? "Y" : ""),'display'=>"Waiting On Approval"),
	2 => array('id'=> ($folder=="admin" ? "admin_" : "")."approve_done",'url'=>($folder=="admin" ? "admin_" : "")."approve_done.php",'active'=> ($page==(($folder=="admin" ? "admin_" : "")."approve_done") ? "Y" : ""),'display'=>"Approved |ACTIONNAME|"),
);

require_once("../inc/header.php");

$rgstrActionObj = new RGSTR_ACTION();

$nameObj = new Naming();
$anames = $nameObj->getReportingNaming("action");
$rnames = $nameObj->getReportingNaming("");

unset($anames['risk_comments']);
unset($anames['action_comments']);
unset($anames['assurance']);
unset($anames['signoff']);

//$me->arrPrint($anames);


//get risk owners
$userObj = new RGSTR_USER();
if($folder=="admin" && $is_risk_manager) {
	$risk_owners = $userObj->getAllDirectorates();
} else {
	$risk_owners = $userObj->getMyDirectorates();
}

if(count($risk_owners)==0) {
	echo "<p>You do not have access to approve any actions.</p>";
} else {
	Assist_Helper::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<table id=tbl_filter class=form>
	<tr>
		<th><?php echo $rnames['risk_owner']; ?>:</th>
		<td><select id=risk_owner><option value=X>--- SELECT ---</option><option value=ALL>All</option><?php
			foreach($risk_owners as $ro) {
				echo "<option value=".$ro['id'].">".$ro['name']."</option>";
			}
		?></select></td>
	</tr>
	<tr>
		<th><?php echo $risk_object_name; ?>:</th>
		<td>
			<select id=risk>
				<option value=X selected>--- SELECT <?php echo $rnames['risk_owner']; ?> ABOVE ---</option>
			</select>
			<div id=div_risk class=ui-state-info style="padding: 5px; margin:5px; width: 600px"></div>
		</td>
	</tr>
	<tr>
		<th><?php echo $anames['action_owner']; ?>:</th>
		<td>
			<select id=action_owner>
				<option value=X selected>--- SELECT <?php echo $rnames['risk_owner']; ?> ABOVE ---</option>
			</select>
		</td>
	</tr>
	<tr>
		<th></th>
		<td><input type=button value=Go id=btn_filter /></td>
	</tr>
</table>
<table id=tbl_actions style='margin-top: 10px;'>
	<tr>
		<?php
		foreach($anames as $fld => $name) {
			echo "<th class=center  id=".$fld.">".$name."</th>";
		}
		?>
		<th id=buttons></th>
	</tr>
	<tr class=actions>
		<td>Actions</td>
	</tr>	
</table>
<script type=text/javascript>
$(function() {
//alert("ok");
	var page = "<?php echo $page; ?>";
	var folder = "<?php echo $folder; ?>";
	var action_type = "<?php echo ($page==(($folder=="admin" ? "admin_" : "")."approve") ? "WAITING" : "APPROVED"); ?>";
	var action_buttons = "<input type=button value='View Logs' class=btn_viewlogsform action_id='_action_index_' style='margin:2px;' /><br />";
	switch(action_type) {
		case "WAITING": action_buttons+="<input type=button value='Approve' class=btn_approveactionform action_id='_action_index_' style='margin:2px;' />"; break;
		case "APPROVED": action_buttons+="<input type=button value='Unlock' class=btn_unlockactionform action_id='_action_index_' style='margin:2px;' />";break;
	}
	var screen = AssistHelper.getWindowSize();
	$("#div_risk").hide();
	var ao_default = $("#action_owner").html();
	var r_default = $("#risk").html();
	var header_row = $("#tbl_actions tr:first").html();

	$("#risk_owner").change(function() {
		openDialog();
		var v = $(this).val();
		var next_action = true;
		$("#risk option").remove();
		var s = "";
		if(v == "X") {
			$("#action_owner option").remove();
			$("#action_owner").html(ao_default);
			$("#risk").html(r_default);
			next_action = false;
		} else if (v=="ALL") {
			$("#risk_owner option").each(function() {
				if($(this).prop("value")!="X" && $(this).prop("value")!="ALL") {
					//if(s.length>0) { s = s + "&"; }
					s = s + "&s[]="+$(this).prop("value");
				}
			});
		} else {
			s = "s[]="+$(this).prop("value");
		}
		//alert(s+" : "+page+" : "+folder);
		if(next_action) {
			var risks = AssistHelper.doAjax("../common/common_controller.php?action=ApproveGetRisks&folder="+folder+"&page="+page,s);
			//console.log(typeof risks);
			$("#risk").append($("<option />",{text:"--- SELECT ---",value:"X",selected:"selected"}));
			$.each(risks,function(index,r) {
				//alert(index);
				if(r.description.length>100) { r.short_description = r.description.substring(0,97)+"..."; } else { r.short_description = r.description; }
				$("#risk").append($("<option />",{value:index,text:"["+index+"] "+r.short_description,full:r.description}));
			});
			updateActionOwner(s);
		}
		closeDialog();
	});
	$("#risk").change(function() {
		openDialog();
		if($(this).val()=="X") {
			$("#div_risk").html("").hide();
			var s = "";
			$("#risk option").each(function() {
				if($(this).prop("value")!="X" && $(this).prop("value")!="ALL") {
					//if(s.length>0) { s = s + "&"; }
					var s = s + "&r[]="+$(this).prop("value");
				}
			});
			updateActionOwner(s);
		} else {
			var t = $("#risk option:selected").text();
			t = AssistString.substr(t,AssistString.stripos(t,"]")+2);
			if($("#risk option:selected").attr("full")!=t) {
				$("#div_risk").html($("#risk option:selected").attr("full")).show();
			} else {
				$("#div_risk").html("").hide();
			}
			var s = "r="+$(this).val();
			updateActionOwner(s);
		}
		closeDialog();
	});
	function updateActionOwner(s) {
		$("#action_owner option").remove();
		var actions = AssistHelper.doAjax("../common/common_controller.php?action=ApproveGetActionOwnersByRisk&folder="+folder+"&page="+page,s);
		//console.log(actions);
		if(actions.length==0) {
			$("#action_owner").attr("disabled",true);
		} else {
			$("#action_owner").attr("disabled",false);
			$("#action_owner").append($("<option />",{text:"--- SELECT ---",value:"X",selected:"selected"}));
			$.each(actions,function(index,a) {
				$("#action_owner").append($("<option />",{value:a.id,text:a.name}));
			});
		}
	}
	$("#btn_filter").click(function() {
		getActions();
	});
	function getActions() {
		openDialog();
		var risk_owner = $("#risk_owner").val();
		var risk = $("#risk").val();
		var action_owner = $("#action_owner").val();
		//action_type - global variable
		var data = "folder="+folder+"&action_type="+action_type+"&risk_owner="+risk_owner+"&risk="+risk+"&action_owner="+action_owner;
		//alert(data);
		var actions = AssistHelper.doAjax("../common/common_controller.php?action=getApprovalActions",data);
		//console.log(actions);
		
		$("#tbl_actions tr").remove();
		$("#tbl_actions").append($("<tr />",{html:header_row}));
		$cells = $("#tbl_actions tr:first").children("th");
		
			$.each(actions, function(index,action) {
				var c = "<tr>";
				$cells.each(function() {
					var fld = $(this).prop("id");
					if(fld=="buttons") {
						//[Sondelani Dumalisile] index as reference made data inaccurate. replace with action['id']. Spotted during standardisation 21 May 2021.
						c+="<td class=center>"+AssistString.str_replace('_action_index_',action['id'],action_buttons)+"</td>";
					} else {
						c = c+"<td>"+action[fld]+(fld=="progress" ? "%":"")+"</td>";
					}
				});
				c+="</tr>";
				$("#tbl_actions").append(c);
		
			});
			
		if($("#tbl_actions tr").size()==1) {
				var cc = -1;
				$cells.each(function() {
					cc++;
				});
				var c="<tr><td colspan="+cc+">There are no actions to display.</td></tr>";
				$("#tbl_actions").append(c);
				$("#tbl_actions tr:first th:last").remove();
		}
		closeDialog();
	}
	$("#tbl_actions").on("click","input:button",function() {
		openDialog();

		var action_id = $(this).attr("action_id");
		var my_class = AssistString.explode("_",$(this).attr("class"));
		my_class = my_class[1]; //alert(my_class);
		var dta = "action_id="+action_id+"&get_what="+my_class;
		var result = AssistHelper.doAjax("../common/common_controller.php?action=ApproveButtonClick",dta);
		switch(my_class) {
			case "viewlogsform":
				var dialog_buttons = [{ 
					text: "Ok", 
					click: function() { 
						$( this ).dialog( "destroy" ); 
					} 
				}];
				break;
			case "approveactionform":
				var dialog_buttons = [{ 
					text: "Approve", 
					click: function() { 
						if($(this).find("textarea").eq(0).val().length>0) {
							updateAction(action_id,"APPROVE",$(this).find("textarea").eq(0).val());
							$(this).dialog("destroy");
						} else {
							alert("Please enter a response.");
						}
					} 
				},{
					text: "Decline",
					click: function() {
						if($(this).find("textarea").eq(0).val().length>0) {
							updateAction(action_id,"DECLINE",$(this).find("textarea").eq(0).val());
							$(this).dialog("destroy");
						} else {
							alert("Please enter a response.");
						}
					}
				},{
					text: "Cancel",
					click: function() {
						$(this).dialog("destroy");
					}
				}];
				break;
			case "unlockactionform":
				var dialog_buttons = [{ 
					text: "Unlock", 
					click: function() { 
						if($(this).find("textarea").eq(0).val().length>0) {
							updateAction(action_id,"UNLOCK",$(this).find("textarea").eq(0).val());
							$(this).dialog("destroy");
						} else {
							alert("Please enter a response.");
						}
					} 
				},{
					text: "Cancel",
					click: function() {
						$(this).dialog("destroy");
					}
				}];
				break;
		}
		
		closeDialog();
		var dlg_id = "dlg_display_"+my_class+"_"+$.now(); //alert(dlg_id);
		$("<div />",{id:dlg_id,title:result.title,html:result.html}).dialog({modal: true,width:(screen.width < 700 ? screen.width-50 : "700")+"px",buttons:dialog_buttons});
		formatDialogButtons($("#"+dlg_id));
		if(my_class=="viewlogsform" && $("#tbl_logs").height()>screen.height) { //alert(screen.height);
			var height = screen.height-50;  //alert(height);
			$("#"+dlg_id).dialog("option","height",height).dialog("option","position",{ my: "center", at: "center", of: window } );
		}
	});
	function closeDialog() { $("#dlg_app_process").dialog("close"); }
	function openDialog() { $("#dlg_app_process").dialog("open"); }
	function formatDialogButtons($me) {
			$me.dialog("widget").find(".ui-dialog-buttonpane button").each(function() {
				switch($(this).text()) {
					case "Approve":
					case "Unlock":
					case "Ok":
						$(this).css(AssistHelper.getGreenCSS());
						break;
					case "Decline":
						$(this).css(AssistHelper.getRedCSS());
						break;
					case "Cancel":
						$(this).css(AssistHelper.getCloseCSS());
				}
			});
	}

	function updateAction(action_id,act,response) {
		if(response.length==0) {
			alert("Please enter a response.");
		} else {
			openDialog();
			var dta = {"action":"processApprovalForm","approval_status":act,"response":response,"action_id":action_id};
			var result = AssistHelper.doAjax("../common/common_controller.php",dta);
			//console.log(result);
			//if(result[0]=="ok") {
				//document.location.href = "approve"+(action_type!="WAITING" ? "_done" : "")+".php?r[]="+result[0]+"&r[]="+result[1];
			//} else {
				var myhtml = AssistHelper.getHTMLResult(result[0],result[1],"");
				closeDialog();
				var dlg_id = "dlg_result_"+$.now();
				$("<div />",{id:dlg_id,html:myhtml}).dialog({modal:true,buttons:[{text:"Ok",click:function(){$(this).dialog("close");if(result[0]=="ok") { document.location.href = document.location.href; }}}]});
				formatDialogButtons($("#"+dlg_id));
				hideDialogTitlebar();
			//}
		}
	}

	<?php echo $rgstrActionObj->getAttachmentJavaForDownload(false,false,"live"); ?>
	$("#dlg_app_process").html("<p>Processing...</p><img src='../../pics/ajax_loader_v2.gif' />").dialog({autoOpen:false,modal:true});
	hideDialogTitlebar();
	getActions();
});
</script>
<div id=dlg_app_process class=center>
</div>
<?php 
} //end if no risk owner access
?>