<?php 
//error_reporting(-1);
//echo "<br />inside include";
$risk_id = $_REQUEST['id'];
//echo $risk_id;

$nameObj = new Naming();
$names = $nameObj->getReportingNaming();

$admire_helper = new ADMIRE_ASSIST_HELPER();
$db = new ASSIST_DB();

$sql = "SELECT RR.*, RS.name as risk_status, RS.color as risk_color 
		FROM ".$db->getDBRef()."_risk_register RR
		INNER JOIN ".$db->getDBRef()."_status RS
		ON RS.id = RR.status
		WHERE RR.id = ".$risk_id;
$risk = $db->mysql_fetch_one($sql);
/*ASSIST_HELPER::arrPrint($risk);

foreach($_REQUEST as $key => $val) {
	echo "<P>".$key." => ";
	if(isset($risk[$key])) {
		echo $val." :: ".$risk[$key];
	} else {
		echo "not found";
	}
}
*/

//ADMIRE CODE TO SAVE CHANGES
if(isset($_REQUEST['action']) && $_REQUEST['action']=="SAVE") {
	$impact_rating = $risk['impact_rating'];
	$likelihood_rating = $risk['likelihood_rating'];
	$control_effectiveness_rating = $risk['control_effectiveness_rating'];
	
        $riskObj 		= new Risk( "","",  "", "", "", $impact_rating, "", $likelihood_rating, "", "",$control_effectiveness_rating, "" , "");
        //$risk_id        = $_POST['id'];
        $riskOld  		    = $riskObj -> getRiskDetail($risk_id);
		$formChanges = $_REQUEST;
		unset($formChanges['action']);
		//$formChanges['current_controls'] = implode("_",array_filter($formChanges['current_controls']));
        $changes 		= $riskObj -> getRiskChange($formChanges, $riskOld);
        $updates        = array();
        $update_data    = array();
        $msg            = "";
        if(isset($changes['changes']) && !empty($changes['changes'])) {
            $msg                        = $changes['change_message'];
            $changes['changes']['user'] = $_SESSION['tkn'];
            $changes['changes']['currentstatus'] = $riskOld['risk_status'];
            $updates['changes']         = base64_encode(serialize($changes['changes']));
            $updates['risk_id']         = $risk_id;
            $updates['insertuser']      = $_SESSION['tid'];
        }
        if(isset($changes['update_data']) && !empty($changes['update_data'])) {
            $update_data = $changes['update_data'];
        }
        $res = $riskObj -> editRisk($risk_id, $update_data, $updates);
		if(count($changes['email_change_message'])>0) {
			foreach($changes['email_change_message'] as $key => $c) {
				$changes['email_change_message'][$key] = " - ".$c.";";
			}
			$msg = implode(chr(10),$changes['email_change_message']);
			$new_risk = $riskObj->getRisk($risk_id);
			//Get recipients emails
			$user = new UserAccess();
				//Risk Owners (Sub-Dir Admins)
				$to = $user->getRiskOwnerEmails($_REQUEST['sub_id']);
				//Risk Managers
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
				$cc2 = array();
				foreach($ccm as $c) {
					$cc2[] = $c['email'];
				}
				$cc = implode(",",$cc2);
				//Get From
				$from = $user->getCurrentUser();
			//Subject
			$subject = "".$risk_object_name." ".$risk_id." Edited";
			//Create message
			$rr = $riskObj->getCalculation("RRE");
			$ir = $riskObj->getCalculation("IRE");
$message = $from['name']." has made changes to ".$risk_object_name." ".$risk_id.".

The changes are:
$msg

The $risk_object_name details are as follows:
".$names['risk_item'].": ".$risk_id."
".$names['risk_type'].": ".$new_risk['type']."
".$names['risk_level'].": ".$new_risk['risk_level']."
".$names['risk_category'].": ".$new_risk['category']."
".$names['risk_description'].": ".$new_risk['description']."
".$names['risk_background'].": ".$new_risk['background']."
".$names['impact'].": ".$new_risk['impact']." (".$names['impact_rating'].": ".$new_risk['impact_rating'].")
".$names['likelihood'].": ".$new_risk['likelihood']." (".$names['likelihood_rating'].": ".$new_risk['likelihood_rating'].")
".$names['inherent_risk_exposure'].": ".$ir['name']." (".$names['inherent_risk_rating'].": ".$ir['rating'].")
".$names['percieved_control_effective'].": ".$new_risk['effectiveness']." (".$names["control_rating"].": ".$new_risk['control_effectiveness_rating'].")
".$names['residual_risk_exposure'].": ".$rr['name']." (".$names['residual_risk'].": ".$rr['rating'].")
".$names['current_controls'].": ".implode("; ",explode("_",$new_risk['current_controls']))."
".$names['financial_exposure'].": ".$new_risk['risk_financial_exposure']."
".$names['actual_financial_exposure'].": ".$new_risk['actual_financial_exposure']."
".$names['risk_owner'].": ".$new_risk['risk_owner']."

Please log onto Assist in order to view the $risk_object_name.
";
			//new ASSIST_EMAIL object
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			//send email
			$email->sendEmail();
		} //end if msg <> ""
		//REDIRECT USER BACK TO EDIT LIST WITH RESULT MESSAGE HERE!!
		if($res>0) {
			echo "
			<script type=text/javascript>
			document.location.href = '".$redirect."?r[]=ok&r[]=Changes+were+saved+successfully';
			</script>
			";
		} else {
			ASSIST_HELPER::displayResult(array("info","No changes were found to be saved."));
		}
/*******************
	end Code to Save changes
	**************************/
}

$sql = "SELECT RR.*, RS.name as risk_status, RS.color as risk_color 
		FROM ".$db->getDBRef()."_risk_register RR
		INNER JOIN ".$db->getDBRef()."_status RS
		ON RS.id = RR.status
		WHERE RR.id = ".$risk_id;
$risk = $db->mysql_fetch_one($sql);

//ASSIST_HELPER::arrPrint($risk);

//ASSIST_HELPER::arrPrint($names);
$fields = array(
	'ref' 							=> array(
			'type'		=>	"REF",
			'heading'	=>	"",
			'field'		=>	"id",
		),
	'financial_year' 				=> array(
			'type'		=>	"LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"financial_year_id",
			'status'	=> "status",
		),
	'rbap_ref' 						=> array(
			'type'		=>	"VC",
			'heading'	=>	"",
			'data'		=>	"",
			'field'		=>	"rbap_ref",
		),
	'risk_type' 					=> array(
			'type'		=>	"LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"type",
			'onchange'	=> true,
			'rating_fld'=> "risk_category",
			'status'	=> "active",
		),
	'risk_level' 					=> array(
			'type'		=>	"LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"level",
			'status'	=> "status",
		),
	'risk_category' 				=> array(
			'type'		=>	"LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"category",
			'status'	=> "active",
		),
	'risk_description' 				=> array(
			'type'		=>	"TEXT",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"description",
		),
	'risk_background' 				=> array(
			'type'		=>	"TEXT",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"background",
		),
	'cause_of_risk' 				=> array(
			'type'		=>	"TEXT",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"cause_of_risk",
		),
	'reasoning_for_mitigation' 		=> array(
			'type'		=>	"TEXT",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"reasoning_for_mitigation",
		),
	'impact' 						=> array(
			'type'		=>	"DISPLAY_LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"",
			'rating_fld'=> "impact_rating",
			'can_edit'	=> false,
			'status'	=> "active",
		),
	'impact_rating' 				=> array(
			'type'		=>	"DISPLAY_RATING",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"",
			'can_edit'	=> false,
			'status'	=> "active",
		),
	'likelihood' 					=> array(
			'type'		=>	"DISPLAY_LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"",
			'rating_fld'=> "likelihood_rating",
			'can_edit'	=> false,
			'status'	=> "active",
		),
	'likelihood_rating' 			=> array(
			'type'		=>	"DISPLAY_RATING",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"",
			'can_edit'	=> false,
			'status'	=> "active",
		),
	'inherent_risk_exposure' 		=> array(
			'type'		=>	"CALC_LIST",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"",
			'can_edit'	=> false,
		),
	'inherent_risk_rating' 			=> array(
			'type'		=>	"DISPLAY",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"",
			'can_edit'	=> false,
		),
	'current_controls' 				=> array(
			'type'		=>	"MULTITEXT_CURRENTCONTROLS",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"",
			'can_edit'	=>false,
		),
	'percieved_control_effective'	=> array(
			'type'		=>	"DISPLAY_LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"percieved_control_effectiveness",
			'rating_fld'=> "control_rating",
			'can_edit'	=> false,
		),
	'control_rating' 				=> array(
			'type'		=>	"DISPLAY_RATING",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"control_effectiveness_rating",
			'can_edit'	=> false,
		),
	'residual_risk_exposure' 		=> array(
			'type'		=>	"CALC_LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"",
			'can_edit'	=> false,
		),
	'residual_risk' 				=> array(
			'type'		=>	"DISPLAY_RATING",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"",
			'can_edit'	=> false,
		),
	'financial_exposure' 			=> array(
			'type'		=>	"LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"",
			'status'	=> "status",
		),
	'actual_financial_exposure'		=> array(
			'type'		=>	"CURRENCY",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"",
		),
	'kpi_ref'						=> array(
			'type'		=>	"VC",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"",
		),
	'risk_owner' 					=> array(
			'type'		=>	"LIST",
			'heading'	=>	"",
			'data'		=> array(),
			'field'		=>	"sub_id",
			'status'	=> "active",
		),
	'attachements' 					=> array(
			'type'		=>	"ATTACH",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"attachment",
		),
	'risk_status' 					=> array(
			'type'		=>	"DISPLAY_COLOR",
			'heading'	=>	"",
			'data'		=> "",
			'field'		=>	"risk_status",
			'color'		=> "risk_color",
			'can_edit'	=> false,
		),
);
unset($fields['attachements']);
foreach($fields as $key => $f) {
	$fields[$key]['heading'] = strlen($f['heading'])==0 ? $names[$key] : $f['heading'];
	$fields[$key]['field'] = strlen($f['field'])==0 ? $key : $f['field'];
	$fields[$key]['status'] = isset($f['status']) ? $f['status'] : "active";
	if(in_array($f['type'],array("LIST","RATING_LIST","CALC_LIST","DISPLAY_LIST"))) {
	//if($f['type']=="LIST") {
		switch($key) {
			case "financial_year":
				$financial_year_obj = new MasterFinancialYear();
				$x    = $financial_year_obj->retrieveMasterFinacialYears("active");
				$z = array(0=>array(
					'id' => 0,
					'value' => "[Unspecified]",
					'start_date' => "",
					'end_date' => "",
					'status' => "1",
				));
				
				$fields[$key]['data']['active'] = array_merge($z,$x);
				//ASSIST_HELPER::arrPrint(array_merge($z,$x));
				break;
			case "risk_category":
				$riskcat 	 = new RiskCategory( "", "", "", "" );
				$fields[$key]['data']['active']  = $riskcat  -> getTypeCategory($risk['type'],$risk['category']) ;
				break;
			case "risk_level":
				$lev 		 = new RiskLevel( "", "", "", "" );
				$fields[$key]['data']['active']		 = $lev -> getLevel();
				break;
			case "risk_owner":
				$dire 		 = new Directorate("", "", "", "");
				$fields[$key]['data']      = $dire -> getActiveForEdit();
				break;
			case "risk_type":
				$type 		 = new RiskType( "", "", "", "" );
				$fields[$key]['data']['active']		 = $type-> getActiveRiskType();
				break;
			case "impact":
				$imp 		 = new Impact( "", "", "", "", "");
				$fields[$key]['data'] 	 = $imp -> getImpact() ;
				/*if(!isset($fields[$key]['data']['active'][$risk['impact']])) {
					$fields[$key]['current_status'] = false;
				} else {
					$fields[$key]['current_status'] = true;
				}*/
				break;
			case "impact_rating":
				$ir = $fields['impact']['data']['active'][$risk['impact']];
				for($i=$ir['rating_from'];$i<=$ir['rating_to'];$i++) {
					$fields[$key]['data']['active'][$i] = array(
						'id'=>$i,
						'name'=>$i
					);
				}
				break;
			case "likelihood":
				$like 		 = new Likelihood( "", "", "", "", "","");
				$fields[$key]['data'] = $like -> getLikelihood() ;
				/*if(!isset($fields[$key]['data']['active'][$risk['likelihood']])) {
					$fields[$key]['current_status'] = false;
				} else {
					$fields[$key]['current_status'] = true;
				}*/
				break;
			case "likelihood_rating":
				$ir = $fields['likelihood']['data']['active'][$risk['likelihood']];
				for($i=$ir['rating_from'];$i<=$ir['rating_to'];$i++) {
					$fields[$key]['data']['active'][$i] = array(
						'id'=>$i,
						'name'=>$i
					);
				}
				break;
			case "percieved_control_effective":
				$ihr		 = new ControlEffectiveness( "", "", "", "", "");
				$fields[$key]['data']    = $ihr -> getControlEffectiveness();
				/*if(!isset($fields[$key]['data']['active'][$risk['percieved_control_effectiveness']])) {
					$fields[$key]['current_status'] = false;
				} else {
					$fields[$key]['current_status'] = true;
				}*/
				break;
			case "control_rating":
				if(isset($fields['percieved_control_effective']['data']['active'][$risk['percieved_control_effectiveness']])) {
					$ir = $fields['percieved_control_effective']['data']['active'][$risk['percieved_control_effectiveness']];
				} else {
					$ir = $fields['percieved_control_effective']['data']['inactive'][$risk['percieved_control_effectiveness']];
				}
				$fields[$key]['data']['active'][$ir['rating']] = array(
					'id'=>$ir['rating'],
					'name'=>$ir['rating'],
				);
				break;
			case "financial_exposure":
				$fin 		 = new FinancialExposure( "", "", "", "", "");
				$fields[$key]['data']['active'] 	 = $fin -> getFinancialExposure() ;
				break;
			case "residual_risk_exposure":
				$rr = new ResidualRisk();
				$fields[$key]['data'] = $rr->getRR();
				break;
			case "inherent_risk_exposure":
				$ir = new InherentRisk();
				$fields[$key]['data'] = $ir->getIR();
				break;
		}
		//echo "<p>".$key;
		//ASSIST_HELPER::arrPrint($fields[$key]['data']);
	}
}

//ASSIST_HELPER::arrPrint($fields);
echo "<div class=float style='width: 250px; position: fixed; top: 0px; right: 10px;'>"; ASSIST_HELPER::displayResult(array("info","Fields marked with a ** can only be changed via the Update page.")); echo "</div>";

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<form name=frm_editrisk method=POST action=edit_risk.php>
	<input type=hidden name=action id=act value=SAVE />
	<input type=hidden name=id id=obj_id value="<?php echo $risk_id; ?>" />
	<input type="hidden" name="risk_id" value="<?php echo $risk_id; ?>" id="risk_id" class="logid" />
<table id=tbl_container class=noborder><tr><td>
	<table id=tbl_editrisk class=hasborder>
<?php
$count = array();
foreach($fields as $key => $f) {
	$fld = $f['field'];
	$fname = $fld;
	echo "
		<tr>
			<th style='text-align: left; width: 200px;'>".$f['heading'].": ".( (isset($f['can_edit']) && $f['can_edit']===false) ? "**" : "")."</th>
			<td>";
	switch($f['type']) {
		case "REF":
			echo RISK::REFTAG.$risk_id;
			break;
		case "LIST":
		case "RATING_LIST":
			//ASSIST_HELPER::arrPrint($f['data']);
			echo "<select name=".$fname." id=".$key." ".(isset($f['onchange']) && $f['onchange']===true ? " class=onchangeme rating_fld='".$f['rating_fld']."' " : "").">";
			foreach($f['data']['active'] as $d) {
				if($d[$f['status']]==1 || $d['id']==$risk[$fld]) {
					echo "<option "
							.($d['id']==$risk[$fld] ? " selected " : "")
							.($d[$f['status']]!=1 ? " disabled=disabled " : "");
					if($f['type']=="LIST" && isset($f['onchange']) && $f['onchange']===true) {
						echo " rating='".(isset($d['rating']) ? $d['rating'] : "0")."' "
								." rating_from='".(isset($d['rating_from']) ? $d['rating_from'] : "0")."' "
								." rating_to='".(isset($d['rating_to']) ? $d['rating_to'] : "0")."' ";
					}
					echo " value=".$d['id'].">"
							.(isset($d['name']) ? $d['name'] : $d['value'])
							."</option>";
				}
			}
			echo "</select>";
			//echo $risk[$fld];
			break;
		case "MULTITEXT_CURRENTCONTROLS":
			if(strlen($risk[$fld])>0) {
				$values = explode("_",$risk[$fld]);
			} else {
				$x = 0;
				$values = array(0=>"");
			}
			echo "<div id=div_current_controls>";
			$ta = array();
			foreach($values as $x => $v) {
				$v = trim(ASSIST_HELPER::decode(stripslashes($v)));
				if($v!="") {
					//$ta[]= "<textarea cols=50 rows=2 name=".$fname."[] id=".$key."_".$x." class=controls>".$v."</textarea>";
					$ta[] = $v;
				}
			}
			if(count($ta)>0) {
				echo "<ul><li>".implode("</li><li>",$ta)."</li></ul>";
			}
			echo "</div>";//<br /><span class=float><input type=button value='Add Another' id=current_controls_add /></span>";
			$count[$key] = $x;
			break;
		case "TEXT":
			$v = ASSIST_HELPER::decode(stripslashes($risk[$fld]));
			echo "<textarea cols=50 rows=5 name=".$fname." id=".$key.">".$risk[$fld]."</textarea>";
			break;
		case "NUM":
		case "CURRENCY":
		case "VC":
			echo ($f['type']=="CURRENCY" ? "R " : "")."<input type=text size=".(in_array($f['type'],array("NUM","CURRENCY")) ? "30" : "60")." name=".$fname." id=".$key." value='".$risk[$fld]."' />";
			break;
		case "ATTACH":
			ASSIST_HELPER::arrPrint(unserialize(base64_decode($risk[$fld])));
			//echo $f['type'];
			break;
		//case "CALC":
			//echo "<input type=hidden id=".$key." name=".$key." value=".$risk[$fld]." /><label id=lbl_".$key.">".$risk[$fld]."</label>";
			//echo "<br />".$risk[$fld];
			//break;
		case "CALC_LIST":
		//ASSIST_HELPER::arrPrint($f['data']);
			$d = $risk[$fld];
			$x = array();
			foreach($f['data'] as $key => $z) {
				if(count($x)==0 || $d>=$z['rating_from']) {
					$x = $z;
				}
				//if($d>=$z['rating_from'] && $d<=$z['rating_to']) {
				//	$x = 
				//}
			}
			echo "<label id=lbl_".$key." style='padding: 1px 4px 1px 4px; background-color: #".$x['color'].";'>".$x['name']."</label>";
			//echo "<br />".$risk[$fld];
			break;
		case "DISPLAY_LIST":
		//ASSIST_HELPER::arrPrint($f['data']);
			$x = array_key_exists($risk[$fld],$f['data']) ? $f['data'][$risk[$fld]] : array();
			$colour = isset($x['color']) ? $x['color'] : '';
			$name = isset($x['name']) ? $x['name'] : '';
			echo "<label id=lbl_".$key." style='padding: 1px 4px 1px 4px; background-color: #".$colour.";'>".$name."</label>";
			break;
		case "DISPLAY_COLOR":
			echo "<label id=lbl_".$key." style='padding: 1px 4px 1px 4px; background-color: #".$risk[$f['color']].";'>".$risk[$fld]."</label>";
			break;
		case "DISPLAY_RATING":
		case "DISPLAY":
		default:
			//echo "<label id=lbl_".$key.">".$risk[$fld]."</label>"."<br />".$risk[$fld];
			echo $risk[$fld];
			break;
	}
	//DEV PURPOSES
	//if(!isset($risk[$fld]) ) { echo "<br />ERROR!! can't find field $fld for key ".$key; }
	echo "
			</td>
		</tr>";
}
?>
		<tr>
			<th>
			</th>
			<td><input type=button value="Save Changes" class=isubmit /> <input type=button value=Reset onclick="document.location.href=document.location.href" /> <?php if($folder=="admin") { echo "<span class=float><input type=button value=Delete class=idelete /></span>"; } ?>
			</td>
		</tr>
	</table>
</form>
<script type=text/javascript>
$(function() {
	var process = $("#dlg_process").html();
	$("#dlg_process").hide();
	var object_id = <?php echo $risk_id; ?>;
	
	$("select.onchangeme").change(function() {
		var id = $(this).prop("id");
		var v = $(this).val();
		var r_fld = $(this).attr("rating_fld");
		$rf = $("#"+r_fld);
		$("#"+r_fld+" option").remove();
		if(v.length>0) {
			switch(id) {
				case "risk_type":	var act = "getTypeCategory"; break;
			}
			$rf.append($("<option />",{value:"", text:"--- SELECT ---"}))
			$.get("../common/common_controller.php?action="+act, { act_id : v }, function( data ) {
				$.each( data ,function( index, val ) {
					$rf
					.append($("<option />",{text:val.name, value:val.id}));
				})											 
			},"json");	
		} else {
			$rf.append($("<option />",{value:"", text:"--- SELECT ABOVE ---"}))											
		}
	});
	
	
	$("input:button.idelete").click(function() {
		if(confirm("Are you sure you wish to DELETE this "+window.risk_object_name+"?\n\n Warning: This will also delete all associated "+window.action_object_name+"!")==true) {
			$("<div />",{html:process}).dialog({title:"Processing...",modal:true});
			$(".ui-dialog-titlebar").hide();
			result = getAjax("../common/common_controller.php?action=DELETE","id="+object_id);
			//console.log(result);
			//alert(result[1]);
			if(result[0]=="error") {
				document.location.href = "edit_risk.php?id="+object_id+"&r[]="+result[0]+"&r[]="+result[1];
			} else {
				document.location.href = "admin_edit.php?r[]=ok&r[]="+result[1];
			}
		}
	});
	$("input:button.isubmit").click(function() {
		$("<div />",{html:process}).dialog({title:"Processing...",modal:true});
		$(".ui-dialog-titlebar").hide();
		$("form[name=frm_editrisk]").submit();
	});
	
});
</script>
<style type=text/css>
th {
	vertical-align: top;
}
#div_current_controls {
	
}
#div_current_controls textarea {
	margin: 2px 0px 2px 0px ;
}
.noborder, .noborder td {
	border: 0px solid #FFFFFF;
}
#tbl_editrisk {
	margin-bottom: 20px;
}
.hasborder, .hasborder td {
	border: 1px solid #ababab;
}
</style>
<?php
echo "<div style='margin-top: 10px; background-color: #fe9900;'><span class=float>".$admire_helper->displayAuditLogLink( "risk_edits" , false)."</span>".ASSIST_HELPER::goBack()."</div>";


//ASSIST_HELPER::arrPrint($fields['residual_risk_exposure']);
//ASSIST_HELPER::arrPrint($fields['inherent_risk_exposure']);
?></td></tr></table>
<div id=dlg_process>
<p class=center>Processing, please be patient.</p><p class=center><img src='../../pics/ajax_loader_v2.gif' /></p>
</div>