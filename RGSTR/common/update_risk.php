<?php

//require_once("controller.php");
$risk 		 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$rsk 		 =  $risk -> getRisk( $_REQUEST['id'] );
$riskStatus  = $risk -> getRiskUpdate( $rsk['id'] );
$statusId 	 = ((empty($riskStatus) || !isset($riskStatus )) ? $rsk['statusId'] : $riskStatus['status'] );
$type 		 = new RiskType( "", "", "", "" );
$types		 = $type-> getType();
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$imp 		 = new Impact( "", "", "", "", "");
$impacts 	 = $imp -> getImpact() ;
$like 		 = new Likelihood( "", "", "", "", "","");
$likelihoods = $like -> getLikelihood() ;
$ihr		 = new ControlEffectiveness( "", "", "", "", "");
$controls    = $ihr -> getControlEffectiveness();
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUserAccess() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getActiveStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
$ireObj = new InherentRisk();
$ire = $ireObj->getIR();
$rrObj = new ResidualRisk();
$rr = $rrObj->getRR();
$admire_helper = new ADMIRE_ASSIST_HELPER();
function nameFields( $fieldId , $defaultName ){
    global $naming, $rowNames;
    if( isset( $rowNames[$fieldId] )) {
        echo $rowNames[$fieldId];
    }	else {
        echo $defaultName;
    }
}

?>
<script>
    $(function(){
        $("table#updaterisk_table").find("th").css({"text-align":"left"})
    });
</script>
<style type=text/css>
.noborder, .noborder td {
	border: 0px solid #ffffff;
}
.hasborder, .hasborder td {
	border: 1px solid #ababab;
}
#updaterisk_table {
	margin-bottom: 20px;
}
</style>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div style="clear:both;">
<table id=tbl_container class=noborder><tr><td>
<form id="update-risk-form" method="post" enctype="multipart/form-data">
<table class=form id="updaterisk_table" class=hasborder>
<tr>
    <th><?php nameFields('risk_item','Risk Item'); ?>:</th>
    <td><?php echo Risk::REFTAG.$rsk['id'] ?></td>
</tr>
<tr>
    <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
    <td>
        <?php echo $rsk['type']; ?>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
    <td>
        <?php echo $rsk['risk_level']; ?>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_category','Risk Category'); ?>:</th>
    <td>
        <?php echo $rsk['category']; ?>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
    <td><?php echo $rsk['description'] ?></td>
</tr>
<tr>
    <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
    <td><?php echo $rsk['background'] ?></td>
</tr>
<tr>
    <th><?php nameFields('impact','Impact'); ?>:</th>
    <td>
        <select id="risk_impact" name="risk_impact">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($impacts as $impact ){
				if($impact['active']==Impact::ACTIVE || $impact['assessment'] == $rsk['impact']) {
					?>
					<option value="<?php echo $impact['id']; ?>" rating_color="<?php echo $impact['color']; ?>"
						<?php
						if( $impact['assessment'] == $rsk['impact'] )
						{
							$impRating = array( "from" => $impact['rating_from'], "to" => $impact['rating_to']);
							$i = $impact;
							
							echo " selected=\"selected\" ";
							if($impact['active']!=Impact::ACTIVE) {
								echo " disabled=disabled ";
							}
						} 
						?>
					><?php echo $impact['assessment']; ?></option>
            <?php
				}
            }
            ?>
        </select> <div id=risk_impact_color style='width: 30px; display:inline; background-color:#<?php echo $i['color']; ?>'>&nbsp;</div>
    </td>
</tr>
<tr>
    <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
    <td>
        <select id="risk_impact_rating" name="risk_impact_rating">
            <option value="">--- SELECT ---</option>
            <?php
            for( $y = $impRating['from']; $y <= $impRating['to']; $y++) {
                ?>
                <option value="<?php echo $y;  ?>" <?php if($y == $rsk['impact_rating'] ) { ?> selected="selected" <?php } ?>>
                    <?php echo $y;  ?>
                </option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<tr>
    <th><?php nameFields('likelihood','Likelihood'); ?>:</th>
    <td>
        <select id="risk_likelihood" name="risk_likelihood">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($likelihoods as $lk ){
				$lk['match'] = ($lk['assessment'] == $rsk['likelihood']);
				if($lk['active']==Likelihood::ACTIVE || $lk['match']) {
					?>
					<option value="<?php echo $lk['id']; ?>" rating_color="<?php echo $lk['color']; ?>"
						<?php
						if( $lk['match']  ) {
							$likeRating = array("from" => $lk['rating_from'], "to" => $lk['rating_to']);
							$k = $lk;
							echo " selected=\"selected\"";
							if($lk['active']!=Likelihood::ACTIVE) {
								echo " disabled=disabled ";
							}
						} 
						?>
					><?php echo $lk['assessment']; ?></option>
            <?php
				}
            }
            ?>
        </select> <div id=risk_likelihood_color style='width: 30px; display:inline; background-color:#<?php echo $k['color']; ?>'>&nbsp;</div>
    </td>
</tr>
<tr>
    <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
    <td>
        <select id="risk_likelihood_rating" name="risk_likelihood_rating">
            <option value="">--- SELECT ---</option>
            <?php
            for( $i = $likeRating['from']; $i <= $likeRating['to']; $i++ ) {
                ?>
                <option
                    value="<?php echo $i; ?>" <?php if($rsk['likelihood_rating'] == $i ) { ?> selected="selected" <?php }?>>
                    <?php echo $i; ?>
                </option>
            <?php
            }
            ?>
        </select> 
    </td>
</tr>
<tr>
    <th><?php nameFields('inherent_risk_exposure','IRE'); ?>:</th>
    <td>
		<?php 
		$x = array();
		foreach($ire as $i) {
			if($i['rating_from']<$rsk['inherent_risk_exposure']) {
				$x = $i;
			}
		}
		if(count($x)==0) {
			$x['color'] = "FFFFFF";
			$x['magnitude'] = "N/A";
		}
			echo "<label id=lbl_ire style='padding: 1 3 1 3; background-color: #".$x['color']."'>".$x['magnitude']." (".$rsk['inherent_risk_exposure'].")"; 
		?></label><input type=hidden id=txt_ire value="<?php echo $rsk['inherent_risk_exposure']; ?>" />
    </td>
</tr>
<tr>
    <th><?php nameFields('current_controls','Current Controls'); ?>:</th>
    <td>
        <?php
        $ctrls = array();
        if( $rsk['current_controls'] != ""){
            $ctrls = explode("_", $rsk['current_controls'] );
			foreach( $ctrls as $index => $val){
				?>
				<p class=p_controls><textarea name="risk_currentcontrols" id="risk_currentcontrols" class="controls"><?php echo $val; ?></textarea></p>
				<!-- <p><input type="text" name="risk_currentcontrols" id="risk_currentcontrols" class="controls"  value="<?php echo $val; ?>" /></p>  -->
			<?php
			}
        } else {
			echo "<p class=p_controls style='font-size: 0px; line-height: 0%; margin: 0px; padding: 0px;'></p>";
		}
        ?>
        <input type="button" name="current_controls_add" id="current_controls_add" value="Add Another"  />
    </td>
</tr>
<tr>
    <th><?php nameFields('percieved_control_effective','Perceived Control Effectiveness'); ?>:</th>
    <td>
        <select id="risk_percieved_control" name="risk_percieved_control">
            <option value="">--- SELECT ---</option>
            <?php
			$x = array();
            foreach($controls as $ctrl ) {
				if($ctrl['active']==1 || $ctrl['id']==$rsk['percieved_control_effectiveness']) {
					?>
					<option value="<?php echo $ctrl['id']; ?>" rating="<?php echo $ctrl['rating']; ?>" rating_color="<?php echo $ctrl['color']; ?>"
					<?php
						if( $ctrl['id'] == $rsk['percieved_control_effectiveness'] )
						{
							$ctrRating = $ctrl['rating'];
							$x = $ctrl;
							echo "selected=\"selected\"";
							if($ctrl['active']!=ControlEffectiveness::ACTIVE) {
								echo " disabled=\"disabled\"";
							}
						} 
					?>
					><?php echo $ctrl['effectiveness']; ?></option>
            <?php
				}
            }
            ?>
        </select> <div id=risk_percieved_control_color style='width: 30px; display:inline; background-color:#<?php echo $x['color']; ?>'>&nbsp;</div>
    </td>
</tr>
<tr>
    <th><?php nameFields('control_rating','Control Effectiveness Rating'); ?>:</th>
    <td>
	<label id=lbl_risk_control_effectiveness ><?php echo $ctrRating; ?></label>
	<input type=hidden value='<?php echo $ctrRating; ?>' id=risk_control_effectiveness name=risk_control_effectiveness />
        <!-- <select id="risk_control_effectiveness" name="risk_control_effectiveness">
            <option value="">--control effectiveness--</option>
            <option value="<?php echo $ctrRating; ?>" selected="selected" >
                <?php echo $ctrRating; ?>
            </option>
        </select> -->
    </td>
</tr>
<tr>
    <th><?php nameFields('residual_risk_exposure','RRE'); ?>:</th>
    <td>
		<?php 
		$x = array();
		foreach($rr as $i) {
			if($i['rating_from']<$rsk['residual_risk_exposure']) {
				$x = $i;
			}
		}
		if(count($x)==0) {
			$x['color'] = "FFFFFF";
			$x['magnitude'] = "N/A";
		}
			echo "<label id=lbl_rre style='padding: 1 3 1 3; background-color: #".$x['color']."'>".$x['magnitude']." (".$rsk['residual_risk_exposure'].")"; 
		?></label>
    </td>
</tr>
<tr>
    <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
    <td>
        <?php echo $rsk['risk_financial_exposure']; ?>
    </td>
</tr>
<tr>
    <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
    <td> <?php echo $rsk['actual_financial_exposure']; ?>
    </td>
</tr>
<tr>
    <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
    <td> <?php echo $rsk['kpi_ref']; ?>
    </td>
</tr>
<tr>
    <th><?php nameFields('risk_owner','Owner'); ?>:</th>
    <td>
        <?php echo $rsk['risk_owner']; ?>
    </td>
</tr>
<tr>
    <th>Response:</th>
    <td>
        <textarea class="mainbox" name="risk_response" id="risk_response" ><?php echo ( (empty($riskStatus['response']) || !isset($riskStatus['response']))  ? "" : $riskStatus['response']); ?></textarea></td>
</tr>
<tr>
    <th><?php nameFields('risk_status','Status'); ?>:</th>
    <td>
        <select id="risk_status" name="risk_status">
            <option value="">--- SELECT ---</option>
            <?php
            foreach($statuses as $status ){
                ?>
                <option value="<?php echo $status['id']; ?>"
                    <?php
                    if( $status['id'] == $rsk['statusId'] )
                    {
						echo " selected=\"selected\" ";
                    } ?>><?php echo $status['name']; ?></option>
            <?php
            }
            ?>
        </select>
    </td>
</tr>
<!-- <tr>
    <th><?php nameFields('','Attach documents'); ?>:</th>
    <td>
            <span id="file_loading">
             <?php
             if( $rsk['attachment'] !== ""){
                 echo "<ul id='attachment'>";
                 $attachments = unserialize( $rsk['attachment'] );
                 foreach($attachments as $key => $value){
                     $id  = $key;
                     $ext = substr( $value , strpos($value, ".")+1);
                     $file = $id.".".$ext;
                     if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/".$file)){
                         echo "<li id='parent_".$id."' class='attachmentlist'><p><a href='download.php?folder=risk&file=".$file."&new=".$value."&modref=".$_SESSION['modref']."&content=".$ext."&company=".$_SESSION['cc']."' class='download'>".$value."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='".$value."' id='".$key."' class='remove_attach removeattach'> Remove</a></p></li>";
                     }
                 }
                 echo "</ul>";
             }
             ?>
          </span>
        <input id="editfile_upload" name="editfile_upload" type="file" onchange="ajaxFileUpload(this)" />
    </td>
</tr> -->
<tr>
    <td></td>
    <td>
        <input type="submit" name="save_update" id="save_update" value="Save Update" class="isubmit" />
        &nbsp;&nbsp;&nbsp;
        <input type="button" name="cancel_update" id="cancel_update" value="Cancel"  />
        <input type="hidden" name="riskid" value="<?php echo $rsk['id']; ?>" id="riskid" />
        <input type="hidden" name="risk_id" value="<?php echo $rsk['id']; ?>" id="risk_id" class="logid" />
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" id="redirect"  />
        <input type="hidden" name="userrespoid" value="<?php echo $rsk['risk_owner']; ?>" id="userrespoid" />
    </td>
</tr>

</table>
</form>
<?php echo "<div style='margin-top: 10px; background-color: #fe9900;'><span class=float>".$admire_helper->displayAuditLogLink( "risk_update" , false)."</span>".ASSIST_HELPER::goBack()."</div>";  ?>
</td></tr></table><!-- tbl_container -->
</div>