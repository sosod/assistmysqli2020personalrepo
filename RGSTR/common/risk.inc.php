<?php

/*** REQUIRES: 
		$obj_id
			**********/

$risk = new RISK();
$risk = $risk->getRiskDetail($risk_id);

//ASSIST_HELPER::arrPrint($risk);

$rkCols  = new RiskColumns();
$columns = $rkCols -> getHeaderList();

$financial_year_obj = new MasterFinancialYear();
$financial_years    = $financial_year_obj->retrieveMasterFinacialYears("active");


function nameFields( $fieldId , $defaultName ){
	echo getNameFields($fieldId,$defaultName);
}

function getNameFields($fieldId,$defaultName) {
	global $columns;
	if( isset( $columns[$fieldId] )) {
		return $columns[$fieldId];
	} else {
		return $defaultName;
	}
}
?>
<div>
<table width="" border="0" id="risk_new" class=form>
 <tr>
        <th>Ref:</th>
        <td>
            <?php echo Risk::REFTAG.$risk_id; ?>
        </td>
      </tr>
 <tr>
        <th><?php nameFields('financial_year','Financial Year'); ?>:</th>
        <td>
            <?php echo (isset($risk['financial_year']) && strlen($risk['financial_year'])>0) ? $risk['financial_year'] : "Unspecified"; ?>
        </td>
      </tr>
    <tr>
        <th><?php nameFields('rbap_ref','RBAP Ref'); ?>:</th>
        <td>
            <?php 
				$fld = "rbap_ref";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
        </td>
    </tr>
	<tr>
        <th id=th_type><?php nameFields('risk_type','Risk Type'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_type";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_level";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
        </td>
      </tr>      
      <tr>
        <th><?php nameFields('risk_category','Risk category'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_category";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
        <td>
            <?php 
				$fld = "description";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
        <td>
            <?php 
				$fld = "background";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
    <tr>
        <th><?php nameFields('cause_of_risk','Cause Of Risk'); ?>:</th>
        <td>
            <?php 
				$fld = "cause_of_risk";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
    </tr>
    <tr>
        <th><?php nameFields('reasoning_for_mitigation','Reasoning For Mitigation'); ?>:</th>
        <td>
            <?php 
				$fld = "reasoning_for_mitigation";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
    </tr>
      <tr>
        <th id=th_impact><?php nameFields('impact','Impact'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_impact";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
        <td>
            <?php 
				$fld = "impact_rating";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th id=th_likelihood><?php nameFields('likelihood','Likelihood'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_likelihood";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
        <td>
            <?php 
				$fld = "likelihood_rating";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th><?php nameFields('current_controls','Current Controls'); ?>:</th>
        <td>
            <?php 
				$fld = "current_controls";
				echo (isset($risk[$fld])) ? "<ul><li>".implode("</li><li>",explode("_",$risk[$fld]))."</li></ul>" : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th id=th_risk_percieved_control><?php nameFields('percieved_control_effective','Perceived Control Effectiveness'); ?>:</th>
        <td>
            <?php 
				$fld = "control_rating";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th><?php nameFields('control_rating','Control Effectveness Rating'); ?>:</th>
        <td>
            <?php 
				$fld = "effectiveness";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>
      <tr>
        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_financial_exposure";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>      
      <tr>
        <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
        <td>
            <?php 
				$fld = "actual_financial_exposure";
				echo (isset($risk[$fld])) ? ASSIST_HELPER::NUMBER_CURRENCY_SYMBOL."&nbsp;".$risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>  
      <tr>
        <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
        <td>
            <?php 
				$fld = "kpi_ref";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
      </tr>                  
     <tr>
        <th><?php nameFields('risk_owner','Risk Owner'); ?>:</th>
        <td>
            <?php 
				$fld = "risk_owner";
				echo (isset($risk[$fld])) ? $risk[$fld] : "Unspecified"; 
			?>
		</td>
    </tr>
   </table>
</div>