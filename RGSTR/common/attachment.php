<?php
//error_reporting(-1);
//echo "<h3>common attachment.php</h3>";
//

include "inc.php";
$result = array(0=>0,1=>"Error");
$s = $_REQUEST['section']; //echo "<P>".$s;

switch($s) {
	case "action":	$object = new RGSTR_ACTION(); $result[0]++; break;
	default: $result[1] = "Invalid Section given".implode("_",$_REQUEST);
}

$def = new Defaults("","");
$risk_object_name = $def->getObjectName("OBJECT"); //echo $risk_object_name;
$action_object_name = $def->getObjectName("ACTION"); //echo $action_object_name;

if($result[0]>0) {
	switch($_REQUEST['action']) {
		case "GET_ATTACH":
			$i = explode("_",$_REQUEST['id']);
			$obj_id = $i[0];
			$attach_id = $i[1];
			$record = $object->getFormattedRecord($obj_id);
			if(count($record['attachment'])>0) {
				$x = 0;
				foreach($record['attachment'] as $src=>$filename) {
					if($x==$attach_id) {
						$a = array('original_filename'=>$filename,'system_filename'=>$src);
						$x++;
						break;
					} else {
						$x++;
					}
				}
				if(isset($a['system_filename']) && strlen($a['system_filename'])>0) {
					$folder = getFullAttachmentPath($object);
					$old = $folder."/".$a['system_filename'];
					if(file_exists($old)) {
						$new = ( (isset($a['original_filename']) && strlen($a['original_filename'])>0) ) ? $a['original_filename'] : $a['system_filename'];
						$object->downloadFile3($old, $new);
					}
				}
			}
			break;
		case "DELETE_ATTACH":
			//$object->arrPrint($_REQUEST);
			$i = explode("_",$_REQUEST['id']);
			$obj_id = $i[0];
			$attach_id = $i[1];
			$record = $object->getFormattedRecord($obj_id);
			if(count($record['attachment'])>0) {
				$updated_attach = array();
				$x = 0;
				foreach($record['attachment'] as $src=>$filename) {
					if($x==$attach_id) {
						$a = array('original_filename'=>$filename,'system_filename'=>$src);
					} else {
						$updated_attach[$src]=$filename;
					}
					$x++;
				}
				if(isset($a['system_filename']) && strlen($a['system_filename'])>0) {
					$src_folder = getFullAttachmentPath($object);
					$dest_folder = getFullAttachmentPath($object,$object->getAttachmentDeleteFolder());
					$old = $src_folder."/".$a['system_filename'];
					if(file_exists($old)) {
						$new_path = $dest_folder."/".$s."_".date("YmdHis")."_".$a['system_filename'];
/*
			*/
						if(copy($old,$new_path)) {
							unlink($old);
						}
						$object->deleteAttachment($obj_id,array($a['system_filename']=>$a['original_filename']),$updated_attach,$object->getStatusID($record));
						$response = array('text' => "Attachment ".$a['original_filename']." has been deleted.",'error'=>false);
					}
				}
			}
			//$response = array('text' => "X.",'error'=>false);
			echo json_encode($response);
			break;
		case "UPDATE_ATTACH":
			$obj_id = $_REQUEST['obj_id'];
			//echo "updating attach";
			$original_filename = "";
			$system_filename = "";
			if(isset($_FILES)) {
				$result[] = count($_FILES)."files are set";
				//$save_folder = $object->getAttachmentFolder();
				//$object->checkFolder($save_folder);
				//$result[] = $save_folder." folder check complete";
				//$folder = "";
				//$location = explode("/",$_SERVER["REQUEST_URI"]);
				//$l = count($location)-2;
				//for($f=0;$f<$l;$f++) {
				//	$folder.= "../";
				//}
				//$folder.="files/".$object->getCmpCode()."/".$save_folder;
				$folder = getFullAttachmentPath($object);
				$result[] = $folder;
				$record = $object->getFormattedRecord($obj_id);
				$new_attach = array();
				if(!isset($record['attachment']) || count($record['attachment'])==0) {
					$i = 0;
					$updated_attach = array();
				} else {
					$updated_attach = $record['attachment'];
					//$i2 = count($record['attachment']);
					$last_sys_filename = explode("_",array_pop(array_keys($record['attachment'])));
					if(count($last_sys_filename)==4) {	//admire naming convention
						$i = count($record['attachment']);
					} else {
						$ri = $last_sys_filename[count($last_sys_filename)-2];
						//$re = explode(".",$ri);
						//$i = $re[0]+1;
						$i = $ri+1;
					}
				}
				$result[] = $i;
				$email_attach = array();
				foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
					$x = array('id'=>$key,'tmp_name'=>$tmp_name);
					$x['error'] = $_FILES['attachments']['error'][$key];
					if($_FILES['attachments']['error'][$key] == 0) {
						$original_filename = $_FILES['attachments']['name'][$key];
						$ext = substr($original_filename, strrpos($original_filename, '.') + 1);
						$parts = explode(".", $original_filename);
						$file = $parts[0];
						$system_filename = strtolower($object->getModRef()."_".$s."_".$obj_id."_".$i."_".date("YmdHis").".".$ext);
						$x['sys'] = $system_filename;
						$full_path = $folder."/".$system_filename;
						$x['path'] = $full_path;
						$new_attach[$system_filename] = $original_filename;
						$updated_attach[$system_filename] = $original_filename;
						$x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
						$i++;
						$result[] = $x;
						$email_attach[] = " - Attachment loaded: ".$original_filename.";";
					}
					$result[] = $new_attach;
				}
				if($updated_attach!=$record['attachment']) {
					$object->saveUpdateAttachments($obj_id,$new_attach,$updated_attach,$record['status']);
				}
				//$message = implode(chr(10),$result);
			} else {
				//$message = "No attachments uploaded";
			}
			$email_message = $_REQUEST['email_message'];
			if(count($email_attach)>0) {
				$message = str_replace("_ATTACH_",implode(chr(10),$email_attach).chr(10),$email_message);
			} else {
				$message = str_replace("_ATTACH_","",$email_message);
			}
			/*$to = "janet@actionassist.co.za";
			$subject = "Action ".$obj_id." updated";
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT");
			$email->sendEmail();*/
			$action = $object->getRecord($obj_id);
			$risk = $object->getParentRecord($action['risk_id']);
				//Get recipients emails
				$user = new UserAccess();
					//Risk Owner
					$to = $user->getRiskOwnerEmails($risk['sub_id']);
					//$to = "janet@actionassist.co.za";
					//Risk Managers
					$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
					$cc2 = array();
					foreach($ccm as $c) {
						$cc2[] = $c['email'];
					}
					$cc = implode(",",$cc2);
					//Get From
					$from = $user->getCurrentUser();
				//Subject
				$subject = "".$action_object_name." ".$obj_id." Updated";
				//new ASSIST_EMAIL object
				$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
				//send email
				$email->sendEmail();
			if(isset($_REQUEST['after_action']) && strlen($_REQUEST['after_action'])>0) {
				echo $object->displayPageHeader()."
				<script type=text/javascript>
					$(function() { 
						".stripslashes($_REQUEST['after_action'])."
					});
				</script>";
			}
			break;
	}
}





function getFullAttachmentPath($object,$save_folder="") {
	$folder = getFilesFolderPath();
	if(strlen($save_folder)==0) {
		$save_folder = $object->getAttachmentFolder();
	}
	//if($check) { 
		$object->checkFolder($save_folder); 
	//}
	$folder.="files/".$object->getCmpCode()."/".$save_folder;
	return $folder;
}

function getFilesFolderPath() {
	$folder = "";
	$location = explode("/",$_SERVER["REQUEST_URI"]);
	$l = count($location)-2;
	for($f=0;$f<$l;$f++) {
		$folder.= "../";
	}
	return $folder;
}


//echo "<pre>";print_r($result);echo "</pre>"; 
?>