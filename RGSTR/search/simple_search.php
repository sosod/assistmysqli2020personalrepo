<?php
$scripts = array('search.js','menu.js');
$styles = array( 'colorpicker.css' );
//$page_title = "Search";
$folder = "search";
$page = "simple_search";
require_once("../inc/header.php");
?>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form method="post" id="search-form" name="search-form">
	<table align="left" class="noborder">
    	<tr>
        	<th class="noborder">Search By </th>
            <td class="noborder">
            	<input type="text" name="search_text" id="search_text" size="50"  /> 
            </td>
            <td class="noborder">
            	<input type="submit" name="go" id="go" value=" Go " />
            </td>
        </tr>
    </table>
    <div id="searchresults" align="left" style="clear:both; margin-left:250px; padding-top:20px;">
    	<table id="risk_table"></table>
    </div>
</form>