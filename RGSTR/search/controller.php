<?php
@session_start();
include_once("../inc/init.php");
switch( $_REQUEST['action'] ) {
	
	case "simplesearch":
		$searchObj  = new SimpleSearch();
		$results 	= $searchObj -> search( $_POST['searchtext'] );
		echo json_encode( $results );
		break;
	case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );	
		break;	
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;
	case "getActiveImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getActiveImpact() );
		break;		
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		//$ditc = $dir -> getDirectorateResponsible();
		echo json_encode( $dir -> getDirectorateResponsible() );
		break;	
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		echo json_encode( $finExp -> getFinancialExposure() );
		break;	
	case "getRiskLevel":
		$level = new Risklevel( "", "", "", "" );
		echo json_encode( $level-> getLevel() );	
		break;			
	case "advancedSearch":
		$adv	 = new AdvancedSearch();
		$results = $adv -> search( $_GET['formdata'] );
		echo json_encode( $results );
		break;							
}
?>
