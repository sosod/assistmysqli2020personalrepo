<?php
$scripts = array( 'advanced_search.js'  );
$styles = array();
//$page_title = "Advanced Search";
$folder = "search";
$page = "advanced_search";
require_once("../inc/header.php");
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<span id="span_again" style="display:none;"><a href="#" id="search_again">Search Again</a></span>       
<div id="search_box" style="display:none; -moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px; text-align:center;">Searching ....<img src="../images/loaderA32.gif" /></div>
<form id="advanced-search-form" method="post" >
<table width="" border="1" id="search_table">
 <!-- <tr>
        <th><?php nameFields('risk_item','Risk Item'); ?></th>
        <td>#<input type="text" name="risk_item" value="" id="risk_item" /></td>
      </tr>
      <tr>
        <th><?php nameFields('risk_type','Risk Type'); ?></th>
        <td>
            <select id="risk_type" name="risk_type">
                <option value="">--risk type--</option>
            </select>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_category','Risk category'); ?></th>
        <td>
            <select id="risk_category" name="risk_category">
                <option value="">--risk category--</option>
            </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_description','Risk Description'); ?></th>
        <td><textarea name="risk_description" id="risk_description"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('risk_background','Background Of Risk'); ?></th>
        <td><textarea name="risk_background" id="risk_background"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('impact','Impact'); ?></th>
        <td>
        <select id="impact" name="impact">
                <option value="">--impact--</option>
         </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('impact_rating','Impact Rating'); ?></th>
        <td>
        <select id="impact_rating" name="impact_rating">
                <option value="">--impact rating--</option>
         </select>        
        </td>
      </tr>
      <tr>
        <th><?php nameFields('likelihood','Likelihood'); ?></th>
        <td>
        <select id="likelihood" name="likelihood">
                <option value="">--likelihood--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?></th>
        <td>
        <select id="likelihood_rating" name="likelihood_rating">
                <option value="">--likelihood rating--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('current_controls','Current Controls'); ?></th>
        <td>
            <input type="text" name="current_controls" id="current_controls" class="controls"  />       
        </td>
      </tr>
      <tr>
        <th><?php nameFields('percieved_effective','Percieved Controll Effectveness'); ?></th>
        <td>
        <select id="percieved_control_effective" name="percieved_control_effective">
                <option value="">--percieved control--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('control_rating','Control Effectveness Rating'); ?></th>
        <td>
        <select id="control_rating" name="control_rating">
                <option value="">--control effectiveness--</option>
         </select>            
        </td>
      </tr>
     <tr>
        <th><?php nameFields('','Directorate responsible'); ?></th>
        <td>
        <select id="directorate" name="directorate" class="responsibility">
                <option value="">--directorate--</option>
         </select>        
        </td>
      </tr>
      <tr>
		<th></th>
        <td>
            <input type="submit" name="search" id="search" value="Search " />                       
       </td>
      </tr>-->
		<tr>
        <th><?php nameFields('risk_item','Risk Item'); ?>:</th>
        <td><span id="idrisk"></span></td>
      </tr>
      <tr>
        <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
        <td>
            <select id="risk_type" name="type">
                <option value="">--risk type--</option>
            </select>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
        <td>
            <select id="risk_level" name="level">
                <option value="">--risk level--</option>
            </select>
        </td>
      </tr>      
      <tr>
        <th><?php nameFields('risk_category','Risk category'); ?>:</th>
        <td>
            <select id="category" name="category">
                <option value="">--risk category--</option>
            </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
        <td><textarea class="mainbox" name="description" id="description"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
        <td><textarea class="mainbox" name="background" id="background"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('impact','Impact'); ?>:</th>
        <td>
        <select id="impact" name="impact">
                <option value="">--impact--</option>
         </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
        <td>
        <select id="impact_rating" name="impact_rating">
                <option value="">--impact rating--</option>
         </select>        
        </td>
      </tr>
      <tr>
        <th><?php nameFields('likelihood','Likelihood'); ?>:</th>
        <td>
        <select id="likelihood" name="likelihood">
                <option value="">--likelihood--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
        <td>
        <select id="likelihood_rating" name="likelihood_rating">
                <option value="">--likelihood rating--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('current_controls','Current Controls'); ?>:</th>
        <td>
            <input type="text" name="current_controls" id="currentcontrols" class="controls"  />         
        </td>
      </tr>
      <tr>
        <th><?php nameFields('percieved_control_effective','Perceived Control Effectiveness'); ?>:</th>
        <td>
        <select id="percieved_control_effective" name="percieved_control_effectiveness">
                <option value="">--perceived control--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('control_rating','Control Effectveness Rating'); ?>:</th>
        <td>
        <select id="control_rating" name="control_effectiveness_rating">
                <option value="">--control effectiveness--</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>
        <select id="financial_exposure" name="financial_exposure">
                <option value="">--financial exposure--</option>
         </select>            
        </td>
      </tr>      
      <tr>
        <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
        <td>
        	<input id="actual_financial_exposure" name="actual_financial_exposure" type="text" />     
        </td>
      </tr>  
      <tr>
        <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
        <td>
        	<input id="kpi_ref" name="kpi_ref" type="text" />     
        </td>
      </tr>                  
      <?php 
        //if ($setups['risk_is_to_directorate'] == 1 ) { 
      ?>
     <tr>
        <th><?php nameFields('','Directorate Responsible'); ?>:</th>
        <td>
        <select id="directorate" name="sub_id" class="responsibility">
                <option value="">--directorate--</option>
         </select>        
        </td>
     </tr>    
      <tr>
		<th></th>
        <td>
            <input type="submit" name="search" id="search" value="Search " />                       
       </td>
      </tr>      
   </table>
</form>
</div>
<div id="searchresults"></div>
<div style="clear:both;"><?php $me->displayGoBack("",""); ?></div>

