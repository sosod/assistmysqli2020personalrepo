<?php
/* requires $sdb as object of ASSIST_DB class => created in main/stats.php */
$now = strtotime(date("d F Y"));
$future = strtotime(date("d F Y",($today+($next_due*24*3600))));
$sql = "SELECT 
		A.deadline AS taskdeadline,
		A.status,
		count(A.id) as c 
		FROM assist_".$cmpcode."_".$modref."_actions A 
			INNER JOIN assist_".$cmpcode."_".$modref."_risk_register R 
			  ON R.id = A.risk_id AND R.active > 0 
		WHERE A.deadline < ".$future." 
		AND A.action_owner = '$tkid' 
		AND A.status <> 3 
		GROUP BY A.deadline";		
$rows = $sdb->mysql_fetch_all($sql);
foreach($rows as $row) {
	$d = strtotime($row['taskdeadline']);
	if($d < $now) {
		$count['past']+=$row['c'];
	} elseif($d==$now) {
		$count['present']+=$row['c'];
	} else {
		$count['future']+=$row['c'];
	}
}

?>