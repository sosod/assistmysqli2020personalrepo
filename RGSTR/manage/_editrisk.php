<?php
$scripts = array( 'editrisk.js', 'menu.js', 'ajaxfileupload.js' );
$styles = array( );
$page_title = "Edit Risk";
require_once("../inc/header.php");
//require_once("controller.php");
$risk 		 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$rsk 		 = $risk -> getARisk( $_REQUEST['id']);
$riskStatus  = $risk -> getRiskUpdate( $rsk['id'] );
$statusId 	 = ((empty($riskStatus) || !isset($riskStatus )) ? $rsk['status'] : $riskStatus['status'] );
$type 		 = new RiskType( "", "", "", "" );
$types		 = $type-> getActiveRiskType();	
$lev 		 = new RiskLevel( "", "", "", "" );
$levels		 = $lev -> getLevel();	
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$imp 		 = new Impact( "", "", "", "", "");
$impacts 	 = $imp -> getImpact() ;
$fin 		 = new FinancialExposure( "", "", "", "", "");
$financial 	 = $fin -> getFinancialExposure() ;
$like 		 = new Likelihood( "", "", "", "", "","");
$likelihoods = $like -> getLikelihood() ;
$ihr		 = new ControlEffectiveness( "", "", "", "", "");
$controls    = $ihr -> getControlEffectiveness();
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUsers() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
$dire 		 = new Directorate("", "", "", "");
$direct      = $dire -> getDirectorate();

if(isset($_SESSION['uploads'])){
    unset($_SESSION['uploads']);
}
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<script>
$(function(){
	$("table#edit_risk_table").find("th").css({"text-align":"left"})
});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
        <form id="edit-risk-form" method="post" enctype="multipart/form-data">
        <table align="left" border="1" id="edit_risk_table">
        <tr>
                    <th><?php nameFields('risk_item','Risk Item'); ?>:</th>
                    <td>#<?php echo $rsk['id'] ?></td>
                  </tr>
                  <tr>
                    <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
                    <td>
                        <select id="risk_type" name="risk_type">
                            <option value="">--risk type--</option>
                            <?php
					foreach($types as $type ){
				?>
                            <option value="<?php echo $type['id'] ?>"
                            <?php
						if( $type['id'] == $rsk['type'] ) 
						{
							?>
                                        selected="selected"
                                        <?php
						}
					?>
                            ><?php echo $type['name']; ?></option>
				<?php	
				}
				?>
                        </select>
                    </td>
                  </tr>
                    <tr>
                        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
                        <td>
                        <select id="risk_level" name="risk_level">
                        <option value="">--risk level--</option>
                            <?php
								foreach($levels as $level ){
							?>
                            <option value="<?php echo $level['id'] ?>"
                            <?php
								if( $level['id'] == $rsk['level'] ) 
								{
									?>
		                                        selected="selected"
		                                        <?php
								}
							?>
                            ><?php echo $level['name']; ?></option>
						<?php	
						}
						?>                        
                        </select>
                        </td>
                    </tr>  
                  <tr>
                    <th><?php nameFields('risk_category','Risk category'); ?>:</th>
                    <td>
                        <select id="risk_category" name="risk_category">
                            <option value="">--risk category--</option>
                           <?php
								foreach($categories as $category ){
							?>
                            <option value="<?php echo $category['id']; ?>"
                            <?php
								if( $category['id'] == $rsk['category'] ) 
								{
									?>
                                        selected="selected"
                                        <?php
								} ?>
							><?php echo $category['name']; ?></option>
						<?php	
						}
						?>
                        </select>    
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
                    <td><textarea class="mainbox" name="risk_description" id="risk_description"><?php echo $rsk['description'] ?></textarea></td>
                  </tr>
                  <tr>
                    <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
                    <td><textarea class="mainbox" name="risk_background" id="risk_background"><?php echo $rsk['background'] ?></textarea></td>
                  </tr>
                  <tr>
                    <th><?php nameFields('impact','Impact'); ?>:</th>
                    <td>
                    <select id="risk_impact" name="risk_impact">
                            <option value="">--impact--</option>
                           <?php
							foreach($impacts as $impact ){
							 ?>
                            <option value="<?php echo $impact['id']; ?>"
                                <?php
								if( $impact['id'] == $rsk['impact'] ) 
								{
									$impRating = array( "from" => $impact['rating_from'], "to" => $impact['rating_to']);
									?>
                                        selected="selected"
                                        <?php
								} ?>
                            ><?php echo $impact['assessment']; ?></option>
							<?php	
							}
							?>                         
                     </select>    
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
                    <td>
                    <select id="risk_impact_rating" name="risk_impact_rating">
                       <option value="">--impact rating--</option>
                       <?php
					   	for( $y = $impRating['from']; $y <= $impRating['to']; $y++) {
					   ?>  
			                       <option value="<?php echo $y;  ?>" <?php if($y == $rsk['impact_rating'] ) { ?> selected="selected" <?php } ?>>
					   <?php echo $y;  ?>
			                       </option>
			                       <?php
					   }
					   ?>
                     </select>        
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('likelihood','Likelihood'); ?>:</th>
                    <td>
                    <select id="risk_likelihood" name="risk_likelihood">
                            <option value="">--likelihood--</option>
                           <?php
					foreach($likelihoods as $lk ){
				?>
                            <option value="<?php echo $lk['id']; ?>"
                                <?php
						if( $lk['id'] == $rsk['likelihood'] ) 
						{
							$likeRating = array("from" => $lk['rating_from'], "to" => $lk['rating_to']);
							?>
                                        selected="selected"
                                        <?php
						} ?>><?php echo $lk['assessment']; ?></option>
				<?php	
				}
				?>                                 
                     </select>            
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
                    <td>
                   <select id="risk_likelihood_rating" name="risk_likelihood_rating">
                   <option value="">--likelihood rating--</option>
                    <?php
			for( $i = $likeRating['from']; $i <= $likeRating['to']; $i++ ) {
		?>
                   <option 
                   value="<?php echo $i; ?>" <?php if($rsk['likelihood_rating'] == $i ) { ?> selected="selected" <?php }?>>
	   <?php echo $i; ?>
                   </option> 	                     
                    <?php
			}
		?>
                     </select>            
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('current_controls','Current Controls'); ?>:</th>
                    <td>
                    	<?php 
                    		$ctrls = array();
                    		if( $rsk['current_controls'] != ""){
                    			$ctrls = explode("_", $rsk['current_controls'] );
                    		}
                    		foreach( $ctrls as $index => $val){
                    	  ?>
							<p><textarea name="risk_currentcontrols" id="risk_currentcontrols" class="controls"><?php echo $val; ?></textarea></p>
						<?php 
                    		}
						?>
                        <input type="submit" name="add_another_control" id="add_another_control" value="Add Another"  />         
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('percieved_control_effective','Percieved Controll Effectveness'); ?>:</th>
                    <td>
                    <select id="risk_percieved_control" name="risk_percieved_control">
                            <option value="">--percieved control--</option>
                           <?php
			   foreach($controls as $ctrl ){
				?>
                            <option value="<?php echo $ctrl['id']; ?>"
                                <?php
						if( $ctrl['id'] == $rsk['percieved_control_effectiveness'] ) 
						{
							$ctrRating = $ctrl['rating'];
							?>
                                        selected="selected"
                                        <?php
						} ?>><?php echo $ctrl['effectiveness']; ?></option>
				<?php	
				}
				?> 
                     </select>            
                    </td>
                  </tr>
                  <tr>
                    <th><?php nameFields('control_rating','Control Effectveness Rating'); ?>:</th>
                    <td>
                    <select id="risk_control_effectiveness_rating" name="risk_control_effectiveness_rating">
                      <option value="">--control effectiveness--</option>
                     <option value="<?php echo $ctrRating; ?>" selected="selected" >
		 	<?php echo $ctrRating; ?>
                        </option> 
                     </select>            
                    </td>
                  </tr>
                                        <tr>
                        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
                        <td>
                        <select id="financial_exposure" name="financial_exposure">
                                <option value="">--financial exposure--</option>
                           <?php
			   foreach($financial as $fina ){
				?>
                            <option value="<?php echo $fina['id']; ?>"
                                <?php
						if( $fina['id'] == $rsk['financial_exposure'] ) 
						{
							?>
                                        selected="selected"
                                        <?php
						} ?>><?php echo $fina['name']; ?></option>
				<?php	
				}
				?>                                 
                         </select>            
                        </td>
                      </tr>      
                      <tr>
                        <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
                        <td>
                            <input id="actual_financial_exposure" name="actual_financial_exposure" type="text" value="<?php echo  $rsk['actual_financial_exposure']; ?>" />     
                        </td>
                      </tr>  
                      <tr>
                        <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
                        <td>
             	<input id="kpi_ref" name="kpi_ref" type="text" value="<?php echo  $rsk['kpi_ref']; ?>"/>                           </td>
                      </tr>  
                  <tr>
                    <th><?php nameFields('directorate','Directorate'); ?>:</th>
                    <td>
                    <select id="risk_user_responsible" name="risk_user_responsible" style="width:150px;">
                            <option value="">--user responsible--</option>
 			<?php
			   foreach($direct as $dir ){						   
				?>
                            <option value="<?php echo $dir['subid']; ?>" 
                            <?php 
					if( $dir['subid'] == $rsk['sub_id'] ) {
				?>
                            selected="selected"
                            <?php } ?>
                            ><?php echo $dir['dirtxt']; ?></option>
				<?php	
				}
				?>                             
                     </select>        
                    </td>
                  </tr>
                  <!-- <tr>
                  <th><strong>UDF's:</strong></th>
                    <td><input type="text" name="udf" id="udf"  /></td>
                  </tr>
                  
                  <tr>
                    <th>Status:</th>
                    <td>
                    <select id="risk_status" name="risk_status">
                            <option value="">--risk status--</option>
 			<?php

			   foreach($statuses as $status ){
				?>
                            <option value="<?php echo $status['id']; ?>"
                                <?php
						if( $status['id'] == $statusId ) 
						{
							?>
                                        selected="selected"
                                        <?php
						} ?>><?php echo $status['name']; ?></option>
				<?php	
				}
				?>                             
                     </select>        
                    </td>
                  </tr>-->
                  <tr>
                    <th><?php nameFields('','Attach documents'); ?>:</th>
                    <td>
                    <span id="file_loading">
                     <?php
						if( $rsk['attachment'] !== ""){
						 echo "<ul id='attachment_list'>";
						  $attachments = unserialize( $rsk['attachment'] );
							foreach($attachments as $key => $value){
							 $id  = str_replace("fileupload_", "", $key);
							 $ext = substr( $value , strpos($value, ".")+1);
							 $file = $key.".".$ext; 
							 
							 //echo $file."&nbsp;&nbsp;&nbsp;";
							 //echo "File name ../../files/".$_SESSION['cc']."/RGSTR/risk/".$file."<br /><br />";
							 if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/".$file)){
							 		echo "<li id='parent_".$id."' class='attachmentlist'><span><a href='download.php?folder=risk&file=".$file."&new=".$value."&modref=".$_SESSION['modref']."s&content=".$ext."&company=".$_SESSION['cc']."'>".$value."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='".$value."' id='".$key."' class='remove_attach removeattach'>Remove</a></span></li>";
							  }
							}
						echo "</ul>";	
						}					
						?>
                    </span>
                    <input id="editfile_upload" name="editfile_upload" type="file" onchange="ajaxFileUpload(this)" />
                    </td>
                  </tr> 
                  <tr>
                    <th></th>
                  <td>
                    <input type="submit" name="edit_risk" id="edit_risk" value="Edit Risk " class="isubmit" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="delete_risk" id="delete_risk" value="Delete Risk" class="idelete" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="hidden" name="risk_id" value="<?php echo $rsk['id']; ?>" id="risk_id" class="logid" />
                        <input type="hidden" name="riskid" value="<?php echo $rsk['id']; ?>" id="riskid" /> 
                   </td>
                  </tr>
                  <tr>
                  <td align="left" class="noborder">
			<?php $me->displayGoBack("",""); ?>
                    </td>
                  <td align="right" class="noborder">
			<?php $admire_helper->displayAuditLogLink( "risk_edits" , false); ?>
			<!-- <a href="" id="viewEditsLog">View Edit Log</a> -->
                    </td>                    
                  </tr>
               </table>
           </form>
     <div id="editLogs" style="clear:both;"></div>

