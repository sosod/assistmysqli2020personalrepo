<?php
$folder = "manage";
$page = "update_risk";

$menuT = array(
	1 => array('id'=> "update_risks",'url'=>"update_risks.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "update_actions",'url'=>"update_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);

$scripts = array( 'jquery.ui.action1.js','menu.js','actions.js'  );
$styles = array( 'colorpicker.css' );
$page_title   = "Update |OBJECTNAME|";
require_once("../inc/header.php");
?>
<script language="javascript">
    $(function(){
        $("table#edit_action_table").find("th").css({"text-algn":"left"})
        $("#actions").action1({updateAction:true, page:"update_actions", section:"manage",objectName:window.risk_object_name, actionName:window.action_object_name});
    });
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="actions"></div>
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
