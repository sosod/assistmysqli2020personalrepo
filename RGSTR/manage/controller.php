<?php
ini_set('error_reporting', 1);
ini_set('display_errors', 1);

@session_start();
include_once("../inc/init.php");

include_once("../../library/class/assist_helper.php");
include_once("../../library/class/assist.php");
include_once("../../library/class/assist_email.php");
$me = new ASSIST();


$def = new Defaults("","");
$risk_object_name = $def->getObjectName("OBJECT"); //echo $risk_object_name;
$action_object_name = $def->getObjectName("ACTION"); //echo $action_object_name;



switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) )
{
    case "editRisk":
        $riskObj 		= new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
        $risk_id        = $_POST['id'];
        $risk  		    = $riskObj -> getRiskDetail($risk_id);

        $changes 		= $riskObj -> getRiskChange($_POST, $risk);
        $updates        = array();
        $update_data    = array();
        $msg            = "";


        if(isset($changes['changes']) && !empty($changes['changes']))
        {
            $msg                        = $changes['change_message'];
            $changes['changes']['user'] = $_SESSION['tkn'];
            $updates['changes']         = base64_encode(serialize($changes['changes']));
            $updates['risk_id']         = $risk_id;
            $updates['insertuser']      = $_SESSION['tid'];
        }
        if(isset($changes['update_data']) && !empty($changes['update_data']))
        {
            $update_data = $changes['update_data'];
        }
        $res                 = $riskObj -> editRisk($risk_id, $update_data, $updates);
        if($res > 1)
        {
			$nameObj = new Naming();
			$names = $nameObj->getReportingNaming();
            $response["id"] 	  = $risk_id;
            $error			 	  = false;
            $text 				  = "$risk_object_name ".Risk::REFTAG.$_REQUEST['id']." has been successfully edited <br />";
            $response = array("text" => $text, "error" => $error, "updated" => true );
        } else if($res == 0) {
            $response  = array("text" => "There was no change made to the $risk_object_name", "error"=> false, "updated" => false);
        }else {
            $response  = array("text" => "Error saving the $risk_object_name edit", "error" => true, "updated" => false);
        }
        Attachment::clear('riskadded', 'risk_'.$_REQUEST['id']);
        echo json_encode( $response );
        break;
	case "updateRisk":
		$changeRes 		= riskChange( $_REQUEST );
		$attachments = "";
		if(isset($_SESSION['uploads']['edits']) && !empty($_SESSION['uploads']['edits']) )
		{
			$attachments = serialize( $_SESSION['uploads']['edits'] );
		}
		
		$risk = new Risk( 	
				$_REQUEST['type'] ,
				$_REQUEST['category'] ,
				$_REQUEST['description'] ,
			     $_REQUEST['background'] ,
		 		$_REQUEST['impact'] ,
				$_REQUEST['impact_rating'] ,
				$_REQUEST['likelihood'] ,
				$_REQUEST['likelihood_rating'],
				$_REQUEST['current_controls'] ,
				$_REQUEST['effectiveness'] ,
				$_REQUEST['control_effectiveness_rating'] ,
				$_REQUEST['user_responsible'],
				$attachments
				);
		$changeArr 	 = $changeRes['changeArr'];
		$changeMessage  = $changeRes['changeMessage'];
		
		$id        = $risk -> updateRisk($_REQUEST['id'], base64_encode(serialize($changeArr)) );
		$response  = array(); 
		if($id > 0) 
		{
			$response["id"] 	  = $id;
			$state 			  = false;
			$text 		       = "$risk_object_name ".Risk::REFTAG.$_REQUEST['id']." has been successfully updated <br />";
			$response = array("text" => $text, "error" => $state, "updated" => true);
		} else if( $id == 0) {
			$response = array("text" => "There was no change made to the $risk_object_name", "error" => false, "updated" => true);
		}else {
			$response = array("text" => "Error saving the $risk_object_name ", "error" => true, "updated" => false) ;
		}
           if(isset($_SESSION['uploads']['edits'])){
             unset($_SESSION['uploads']['edits']);
           }
			echo json_encode( $response );		
		break;
	case "saveAttachment":
			$upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/";
							
			$key 	  				   = key($_FILES);
			$filename 				   = basename($_FILES[$key]['name']);
			$_SESSION['uploads']['editadded'][$key.time()] = $_FILES[$key]['name'];
			$fileuploaded 			   = $key."".time().".";	
			$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
			if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
			{			
				$response = array(
									"text" 			=> "File uploaded . . .",
									"error" 		=> false,
									"filename"  	=> $filename,
									"key"			=> $key,
									"filesuploaded" => $_SESSION['uploads']['editadded']
									);
			} else {
				$response = array("text" => "Error uploading file ...." , "error" => true );
			}		
			echo json_encode( $response );
		break;
	case "saveAssuranceAttachment":
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/")) {
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ) {
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk");
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/deleted");					
				}					
			}		
			$upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/";
								
			$key 	  				   = key($_FILES);
			$filename 				   = basename($_FILES[$key]['name']);
			$_SESSION['uploads']['riskassurance'][$key.time()] = $_FILES[$key]['name'];
			$fileuploaded 			   = $key."".time().".";	
			$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
			if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
			{			
				$response = array(
									"text" 			=> "File uploaded . . .",
									"error" 		=> false,
									"filename"  	=> $filename,
									"key"			=> $key,
									"filesuploaded" => $_SESSION['uploads']['riskassurance']
									);
			} else {
				$response = array("text" => "Error uploading file ...." , "error" => true );
			}		
			echo json_encode( $response );
		break;	
	case "updateAssuranceAttachment":
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/")) {
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ) {
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk");
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/deleted");					
				}					
			}		
			$upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/";
								
			$key 	  				   = key($_FILES);
			$filename 				   = basename($_FILES[$key]['name']);
			$_SESSION['uploads']['editriskassurance'][$key.time()] = $_FILES[$key]['name'];
			$fileuploaded 			   = $key."".time().".";	
			$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
			if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
			{			
				$response = array(
									"text" 			=> "File uploaded . . .",
									"error" 		=> false,
									"filename"  	=> $filename,
									"key"			=> $key,
									"filesuploaded" => $_SESSION['uploads']['editriskassurance']
									);
			} else {
				$response = array("text" => "Error uploading file ...." , "error" => true );
			}		
			echo json_encode( $response );
		break;	
	case "removeFile":
			$ext 	  = substr($_POST['originalname'], strpos($_POST['originalname'], ".")); 
			$filename = $_POST['timeid']."".$ext;
			$upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/";
			$response   = array(); 
			if( rename( $upload_dir."".$filename,$upload_dir."deleted/".$filename ) ){
				$_SESSION['editdeleted'][$_POST['timeid']]  = $_POST['originalname'];
				$response = array("error" => false, "text" => "File removed");
			} else {
				$response = array("error" => true, "text" => "Error removing file . . ");	
			}
			echo json_encode( $response );
		break;
	case "deleteRisk":
		$risk = new Risk( "", "", "", "", "", "", "", "", "", "", "", "","");
		$id =  $risk -> deleteRisk( $_REQUEST['id'] );
		if( $id == 1 ) {
			$response["id"] 	  = $id;
			$state 				  = true;
			$text 				  = "$risk_object_name ".Risk::REFTAG.$_REQUEST['id']." has been successfully deleted <br />";
		}
			$response = array("error" => $state, "text" => $text, "deleted" => true );
		echo json_encode( $response );	
		break;		
	case "getNaming":
		$nm = new Naming();
		echo json_encode( $nm -> getNamingConversion() );	
		break;
	case "getRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode( $risk -> getAllRiskTogether( $_REQUEST['start'], $_REQUEST['limit'] ) );
		//echo json_encode( $risk -> getAllRisk( $_REQUEST['start'], $_REQUEST['limit'] ) );
		break;
    case "getRiskAction":
        $riskObj = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
        $risks   = $riskObj->getRiskActions($_REQUEST);
        echo json_encode($risks);
        /*exit();
        print "<pre>";
           print_r($risks);
        print "<pre>";
        exit();
        echo json_encode( $risks );*/
        //echo json_encode( $risk -> getAllRisk( $_REQUEST['start'], $_REQUEST['limit'] ) );
        break;
    case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );	
		break;
	case "totalRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo $risk -> totalRisk( "" );
		break;			
	case "getDefaults":
		$default = new Defaults("","");
		echo json_encode( $default -> getDefaults() );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		echo json_encode( $dir -> getSubDirectorate() );
		break;
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;		
	case "getTypeCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;		
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "getActiveImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getActiveImpact() );
		break;				
	case "getResidualRisk":
		$res = new ResidualRisk( "", "", "", "", "");
		echo json_encode( $res -> getResidualRiskRanges() );
		break;				
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getInherentRiskRange":
		$ihr = new InherentRisk("", "", "", "", "");
		echo json_encode( $ihr -> getInherentRiskRange() );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getUserAccess":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUserAccess() );
		break;	
	case "getUserAccessInfo":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUser( $_SESSION['tid'] ) );
		break;
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;	
	case "getRiskStatus":
		$getstat = new RiskStatus( "", "", "" );
		echo json_encode( $getstat -> getStatus() );
		break;	
	case "deleteAction":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		$risk -> deleteRiskAction( $_REQUEST['id'] );
		break;	
	case "activateAction":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		$risk -> activateRiskAction( $_REQUEST['id'] );
		break;
    case "updateRiskAction":
        $action_id      = $_POST['id'];
        $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
        $action	        = $actObj -> getActionDetail($action_id) ;
		$riskObj		= new Risk();
		$risk			= $riskObj->getRiskDetail($action['risk_id']);
        $statusObj 		= new ActionStatus("","", "");
        $statuses 		= $statusObj -> getStatuses();
        $user_access 	= new UserAccess( "", "", "", "", "", "", "", "", "");
        $users 	        = $user_access -> getUsersList();
        $attachment     = array();
        $change_arr 	= array();
        $change_msg	    = "\r\n";
        $update_data    = array();
        $attStr 		= "";
		$email_message = array();
$nameObj = new Naming();
$names = $nameObj->getReportingNaming("action");
$rnames = $nameObj->getReportingNaming();
        $att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
        if($_POST['r_action'] !== $action['action']) {
            $from                        = $action['action'];
            $to                          = $_POST['r_action'];
            if(trim($from) != trim($to)) {
                $update_data['action']       = $_POST['r_action'];
                $m                 = $names['risk_action']." changed to ".$to." from ".$from;
                $change_msg                 .= $m."\r\n";
				$email_message[] = " - ".$m.";";
                $change_arr['action_name']   = array('from' => $from, 'to' => $to);
            }
        }
        if($_POST['deliverable'] != $action['deliverable']) {
            $from                             = $action['deliverable'];
            $to                               = $_POST['deliverable'];
            $change_arr['action_deliverable'] = array('from' => $from, 'to' => $to);
                $m                 = $names['deliverable']." changed to ".$to." from ".$from;
                $change_msg                 .= $m."\r\n";
				$email_message[] = " - ".$m.";";
            $update_data['deliverable']       = $_POST['deliverable'];
        }
        if($_POST['action_owner'] != $action['owner']) {
            $from                          = $users[$action['owner']]['user'];
            $to                            = $users[$_POST['action_owner']]['user'];
            $update_data['action_owner']   = $_POST['action_owner'];
                $m                 = $names['action_owner']." changed to ".$to." from ".$from;
                $change_msg                 .= $m."\r\n";
				$email_message[] = " - ".$m.";";
            $change_arr['action_owner']    = array('from' => $from, 'to' => $to);
        }
        if($_POST['timescale'] !== $action['timescale']) {
            $from                        = $action['timescale'];
            $to                          = $_POST['timescale'];
            $update_data['timescale']    = $_POST['timescale'];
                $m                 = $names['timescale']." changed to ".$to." from ".$from;
                $change_msg                 .= $m."\r\n";
				$email_message[] = " - ".$m.";";
            $change_arr['timescale']     = array('from' => $from, 'to' => $to);
        }
        if($_POST['deadline'] !== $action['deadline']) {
            $from                  = (strtotime($action['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($action['deadline'])) );
            $to                    = (strtotime($_POST['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['deadline'])) );
            if(strtotime($from) !== strtotime($to))
            {
                $change_arr['deadline']  = array('from' => $from, 'to' => $to);
                $m                 = $names['deadline']." changed to ".$to." from ".$from;
                $change_msg                 .= $m."\r\n";
				$email_message[] = " - ".$m.";";
                $update_data['deadline'] = $_POST['deadline'];
            }
        }
        if($_POST['remindon'] != $action['remindon']) {
            $from                  = (strtotime($action['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($action['remindon'])) );
            $to                    = (strtotime($_POST['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['remindon'])) );
            if(strtotime($from) !== strtotime($to)) {
                $change_arr['action_remind_on']  = array('from' => $from, 'to' => $to);
                $m                 = "Reminder changed to ".$to." from ".$from;
                $change_msg                 .= $m."\r\n";
				$email_message[] = " - ".$m.";";
                $update_data['remindon']         = $_POST['remindon'];
            }
        }

        if(isset($_SESSION['uploads']))
        {
            if(isset($_SESSION['uploads']['actionchanges']))
            {
                $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
                $change_msg              .= $att_change_str;
            }
            if(isset($_SESSION['uploads']['attachments']))
            {
                if($action['attachement'] != $_SESSION['uploads']['attachments'])
                {
                    $update_data['attachement'] = $_SESSION['uploads']['attachments'];
                }
            }
        }
        $updates = array();
        if(!empty($change_arr))
        {
            if(!isset($change_arr['currentstatus']))
            {
                $change_arr['currentstatus'] = $statuses[$action['status']-1]['name'];
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
        }

		if(is_numeric($_POST['action_owner'])) {
        $res                      = $actObj -> editRiskAction($action_id, $update_data, $updates);
		}else{
			$res = -1;
		}

        $response  = array();
        if($res > 0) {
            $text 				 = "$action_object_name ".RiskAction::REFTAG.$action_id." has been successfully saved <br />";
			if(count($email_message)>0) {
				//Get recipients emails
				$user = new UserAccess();
					//Action Owner
					$to = $user->getActionOwnerEmail($_REQUEST['action_owner'],true);
					//$to = "janet@actionassist.co.za";
					//Risk Managers
					$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
					$cc2 = array();
					foreach($ccm as $c) {
						$cc2[] = $c['email'];
					}
					$cc = implode(",",$cc2);
					//Get From
					$from = $user->getCurrentUser();
				//Subject
				$subject = "".$action_object_name." ".$action_id." Edited";
				//Create message
				$msg = implode(chr(10),$email_message);
$message = $from['name']." has made changes to ".$action_object_name." ".$action_id.".

The changes are:
$msg

The complete $action_object_name details are:
".$names['risk_action'].": ".$_REQUEST['r_action']."
".$names['deliverable'].": ".$_REQUEST['deliverable']."
".$names['action_owner'].": ".$to['name']."
".$names['timescale'].": ".$_REQUEST['timescale']."
".$names['deadline'].": ".$_REQUEST['deadline']."
Reminder: ".$_REQUEST['remindon']."

The associated $risk_object_name details are as follows:
".$rnames['risk_item'].": ".$action['risk_id']."
".$rnames['risk_description'].": ".$risk['description']."
".$rnames['risk_owner'].": ".$risk['risk_owner']."

Please log onto Assist in order to view the $action_object_name.
";
				//new ASSIST_EMAIL object
				$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
				//send email
				$email->sendEmail();
			}
            $response = array("text" => $text, "error" => $error, "updated" => true );
        } elseif($res == 0){
            $response = array("text" => "No changes made to the $action_object_name", "error" => false, "updated" => false);
        } else {
            $response = array("text" => "Error editing the $action_object_name", "error" => true, "updated" => false);
        }
        Attachment::clear("riskadded", 'action_'.$_REQUEST['id']);
        Attachment::clear("riskdeleted", 'action_'.$_REQUEST['id']);
        echo json_encode( $response );
        break;
	case "newRiskUpdate":
		$changeRes 			= riskChange( $_REQUEST );
		$attachments = "";
		if(isset($_SESSION['uploads']['edits']) && !empty($_SESSION['uploads']['edits']) )
		{
			$attachments = serialize( $_SESSION['uploads']['edits'] );
		}		
		$risk = new Risk("" , "", "", "", $_REQUEST['impact'], $_REQUEST['impact_rating'], $_REQUEST['likelihood'], $_REQUEST['likelihood_rating'],
					  $_REQUEST['current_controls'], $_REQUEST['effectiveness'], $_REQUEST['control_effectiveness_rating'], "", $attachments
					 );		
		$changeArr 	 = $changeRes['changeArr'];
		$changeMessage  = $changeRes['changeMessage'];				 
		$id             = $risk -> saveRiskUpdate( $_REQUEST['id'], $_REQUEST['status'], $_REQUEST['response'], base64_encode(serialize($changeArr)) );
		if($id > 0) 
		{
			$response["id"] = $id;
			$state 		 = false;
			$text 		 = "$risk_object_name ".Risk::REFTAG.$_REQUEST['id']." has been successfully updated<br />";
			/*$user           = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo		 = $user -> getRiskManagerEmails();
			//$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['user_responsible'] );			
			$email 			 = new Email();
			$email -> userFrom   = $_SESSION['tkn'];
			$email -> subject    = "Update Risk ";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			$message .= $email->userFrom." has updated $risk_object_name #".$changeRes['responsiblePerson']."\n";
			$message .= " \n";
			if($changeMessage == ""){
				$message .= "";	
			} else {
				$message .= "The following updates were made \r\n\n";
				$message .= $changeMessage."\r\n";
			}
				//blank row above Please log on line
			$message .= "Please log onto Assist in order to View or Update the $risk_object_name and to assign any required\r\n\n";
			$message .= "$action_object_name to responsible users within ".ucwords($_SESSION['cc']);			
			$message  = nl2br($message);			
			$email -> body = $message;
			$email -> to   = $emailTo;
			$email -> from = "no-reply@ignite4u.co.za";
			if( $email -> sendEmail() ) {
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email has been sent";
			} else {
				$state = true;
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}*/
			$response = array("text" => $text, "error" => $state, "update" => true);
		} else {
			$response = array( "text" => "Error saving the $risk_object_name update", "error" => true, "update" => false );
		}
        if(isset($_SESSION['uploads']['edits'])){
            unset($_SESSION['uploads']['edits']);
        }		
		echo json_encode( $response );
		break;
	case "getRiskAssurance":
		$rskass = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");		
		echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		//echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		break;	
	case "getRiskAssuranceWithHeaders":
		$rskass = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");		
		echo json_encode( $rskass -> getRiskWithHeaders( $_REQUEST['id'] ) );
		//echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		break;	
	case "getRiskAssurances":
		$rskass = new RiskAssurance($_REQUEST['id'] , "", "", "", "", "");
		echo json_encode( $rskass -> getRiskAssurance( $_GET['start'], $_GET['limit']) );
		break;
	case "updateRiskAssurance":
		$rskassObj 	   = new RiskAssurance("" , "", "", "", "", "");
		$riskassurance = $rskassObj -> getARiskAssurance( $_REQUEST['id'] );
		$assuranceAtt  = array(); 
		if(!empty($riskassurance['attachment']))
		{
			$assuranceAtt = unserialize( $riskassurance['attachment'] );			
		}
		if( isset($_SESSION['uploads']['editriskassurance']) && !empty($_SESSION['uploads']['editriskassurance']) )
		{
			foreach( $_SESSION['uploads']['editriskassurance'] as $index => $val )
			{				
				$assuranceAtt[$index] = $val;
				$changeArr['attachment'][$index] = "Added new attachment ".$val; 	
			}
			unset($_SESSION['uploads']['editriskassurance']);
		}
		if( isset($_SESSION['uploads']['deletedriskassurance']) && !empty($_SESSION['uploads']['deletedriskassurance']) )
		{
			foreach( $_SESSION['uploads']['deletedriskassurance'] as $dIndex => $dVal) 
			{
				if( isset($assuranceAtt[$dIndex]) )
				{
					unset($assuranceAtt[$dIndex]);
					$changeArr['attachment'][$dIndex] = "Deleted attachment ".$dVal;
				} 	
			}
			unset($_SESSION['uploads']['deletedriskassurance']);	
		}	
		if( !empty($assuranceAtt))
		{
			$attach = serialize($assuranceAtt);
		}
		
		$rskass = new RiskAssurance($_REQUEST['risk_id'] , $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $attach);
		$res    = $rskass -> updateRiskAssurance( $_REQUEST['id'] ) ;	
		if( $res == 1 )
		{
			$response = array("text" => "$risk_object_name assurance successfully updated", "error" => false );
		} else if( $res == 0){
			$response = array("text" => "There was no change made to the $risk_object_name assurance", "error" => false );
		} else {
			$response = array("text" => "Error updating $risk_object_name assurance", "error" => false );
		} 
		echo json_encode( $response );
		break;
	case "newRiskAssurance":
		$riskassuranceAtt = "";
		if( isset($_SESSION['uploads']['riskassurance']) && !empty($_SESSION['uploads']['riskassurance']))
		{
			$riskassuranceAtt = serialize( $_SESSION['uploads']['riskassurance'] ); 	
		}
		$rskass = new RiskAssurance($_REQUEST['id'] , $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $riskassuranceAtt);
		$response = array();
		$res = $rskass -> saveRiskAssurance();
		if($res > 0 ){
			$response = array("text" => "$risk_object_name assurance saved successfully", "error" => false);
		} else {
			$response = array("text" => "Error occured saving the $risk_object_name assurance", "error" => true); 
		}
		if(isset($_SESSION['uploads']['riskassurance']))
		{
			unset( $_SESSION['uploads']['riskassurance'] );
		}
		echo json_encode( $response ) ;
		break;
	case "deleteRiskAssurance":
		$rskass = new RiskAssurance("" ,"", "","", "", "");
		$res = $rskass -> deleteRiskAssurance( $_POST['id'] );
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "$risk_object_name assurance successfully deleted", "error" => false);
		} else {
			$response = array("text" => "Error deleting $risk_object_name assurance", "error" => false);
		}
		echo json_encode( $response );
		break;
	/*case "saveNotification":
		$usernot = new UserNotification();
		$usernot -> saveNotifications();
		break;
		
	case "updateNotification":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> updateNotification(  $_POST['id'] ) );
		break;		
	//case "getNotifications":
		//$usernot = new UserNotification();
		//echo json_encode( $usernot -> getNotifications() );
		//break;
	case "deleteNot":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> deleteNotification( $_REQUEST['id'] ) );
		break;
	*/		
	case "getRiskUpdates":
		$rskupdat 	  	= new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		$riskUpdateLog 	= $rskupdat -> getRiskUpdates( $_REQUEST['id'] );
		$changeLog		= array();
		$changeLog 	    = getRiskChanges( $riskUpdateLog, "updated" );		

		echo json_encode( $changeLog  );
		break;
	case "getApprovedActions":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		echo json_encode( $risk -> getApprovedActions(  $_GET['user'] , $_GET['start'], $_GET['limit'] ) );	
		break;
	case "getActionToApprove":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		echo json_encode( $risk -> getActionToApprove( $_GET['user'] , $_GET['start'], $_GET['limit']) );		
		break;
	case "getUserApprovedActions":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		echo json_encode( $risk -> 	getUserApprovedActions( $_GET['userid'] ) );			
		break;		
	case "deleteAssuranceAttachment":
		$id 	= $_POST['id'];
		$file 	= $_POST['file'];
		$ex = substr( $file, strpos($file, "."));
		$filename = $id."".$ex;
		$oldname  = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/".$filename;
		$newname  = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/deleted/".$filename; 
		$response = array();
		if( rename($oldname, $newname))
		{
			$_SESSION['uploads']['deletedriskassurance'][$id] = $file; 
			$response = array("text" => "File removed", "error" => false);
		} else {
			$response = array("text" => "Error removing file", "error" => true);
		}
		echo json_encode( $response );
		break;
	case "getApproveData":
        $risk               = new RiskAction( "" , "", "", "", "", "", "", "","");
        $actions_to_approve = $risk->getAwaitingActions($_GET);
        $actions_approved   = $risk->getActionsApproved($_GET);
		$results = array( "headers" => $actions_to_approve['headers'], "awaiting" => $actions_to_approve, "approved" => $actions_approved);
		echo json_encode( $results );
		break;	
	case "newApprovalAction":
		$act 	  = new RiskAction("", "", "", "", "", "", "","", "");
		$action   = $act -> getAAction( $_REQUEST['id'] ); 
		$id		  = $act -> approveAction( 
											$_REQUEST['id'],
											$_REQUEST['response'],
											$_REQUEST['signoff'],
											"",
											""	,
											$action['action_status']
										);
		$response = array();
		$error    = false;  
		if( $id > 0 ) {
			$response["id"] 	  = $id;
			$text 				  = "$action_object_name id ".$_REQUEST['id']." has been approved <br />";
			/*$user      		      = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			      = $user -> getRiskManagerEmails();
			$action				  = $act -> getAAction( $_REQUEST['id'] );						
			$email 			      = new Email();
			$email -> userFrom    = $_SESSION['tkn'];
			$email -> subject     = "$action_object_name Approval granted";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			$message .= $email->userFrom." has approved a your request for approval of an $action_object_name #".$_REQUEST['id'];
			$message .= " related to ".$action['risk_id']." : ".$action['description'];
			$message .= " \r\n\n";
				//blank row above Please log on line
			$message .= "Please log onto Assist in order to view the $action_object_name.\n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";
			foreach($users as $userEmail ) {
				$emailTo = $userEmail['tkemail'].",";
			}
			$email -> to 	   = $action['action_owner_email']."".($emailTo=="" ? "" : ",".rtrim($emailTo, ","));
			$email -> from     = "no-reply@ignite4u.co.za";
			if( $email -> sendEmail() ) {
				$text  .= " Email send successfully";
			} else {
				$text .= " There was an error sending the email";
				$error = true;
			}*/
			$response = array( "text" => $text, "error" => $error, "updated" => true);
		} else if( $id == 0) {
			$response = array( "text" => "There was no change made to the $action_object_name", "error" => false, "updated" => false);
		} else {
			$response = array( "text" => "Error saving the $action_object_name approval", "error" => true, "updated" => false);
		}
		echo json_encode( $response );
		break;	
		case "unlockAction":
			$act 	  = new RiskAction("", "", "", "", "", "", "","", "");
			$action   = $act -> getAAction( $_POST['id'] );
			$res 	  =  $act -> unlockAction( $_POST['id'] , $action['action_status']);
			$response = array(); 
			if( $res == 1){
				$response = array( "error" => false , "text" => "$action_object_name unlocked . . "); 			
			} else if( $res == 0) {
				$response = array("error" => true , "text" => "No change made trying to unlock the $action_object_name"); 
			} else {
				$response = array("error" => true, "text" => "Error unlocking the $action_object_name" ); 			
			}
			echo json_encode( $response );
		break;
		case "sendDecline":
			$act 	  = new RiskAction("", "", "", "", "", "", "","", "");
			$status   = $act -> getApprovalRequestStatus( $_REQUEST['id'] );
			$actionstatus = RiskAction::ACTION_AWAITING + RiskAction::DECLINE_APPROVAL; 
			$id	= $act->setActionStatus( $_REQUEST['id'], $actionstatus, 2 );	
			//$act->delete("action_approved", $_REQUEST['id']);
			$response = array();
			$text = "";
			if( $id > 0){ 
				$text		  		= "$action_object_name id ".$_REQUEST['id']." has been declined";
				/*$user      			= new UserAccess("", "", "", "", "", "", "", "", "" );
				$users 			    = $user -> getRiskManagerEmails();
				$action				= $act -> getAAction( $_REQUEST['id'] );						
				$email 			    = new Email();
				$email -> userFrom  = $_SESSION['tkn'];
				$email -> subject   = "$action_object_name Approval declined";
				//Object is something like Risk or Action etc.  Object_id is the reference of that object
				$message   = ""; 
				$message .= $email->userFrom." has declined a your request for approval of an $action_object_name #".$_REQUEST['id'];
				$message .= " related to $risk_object_name ".$action['risk_id']." : ".$action['description'];
				$message .= " \r\n\n";
					//blank row above Please log on line
				$message .= "Please log onto Assist in order to view the $action_object_name.\n";
				$message  = nl2br($message);			
				$email -> body     = $message;
				$emailTo 		   = "";
				foreach($users as $userEmail ) {
					$emailTo = $userEmail['tkemail'].",";
				}
				$email->to = ($emailTo == "" ? "" : rtrim($emailTo, ","))."".(isset($action['action_owner']) ? "" : ",".$action['action_owner']);
				$email -> from     = "no-reply@ignite4u.co.za";
				if( $email -> sendEmail() ) {
					$response = array("error" => false, "text" => $text." <br />&nbsp;&nbsp;&nbsp;&nbsp; Email send successfully" );
				} else {
					$response = array("error" => true, "text" => $text." <br /> &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email" );
				}*/
				$response = array("error" => false, "text" => $text );
			} else {
				$response = array("error" => true, "text" => "There was an error declining $action_object_name" );
			}
			echo json_encode( $response );
		break;
		case "getRiskEdits":
		$risk 		  = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$riskEditLog  = $risk -> getRiskEdits( $_REQUEST['id'] );
		$changeLog 	  = array();		
		$changeLog 	  = getRiskChanges( $riskEditLog, "edited" );
		echo json_encode( $changeLog );
		break;	
		case "getManagerUsers":
		$user = new UserAccess( "", "", "", "", "", "", "", "", "");
		echo json_encode( $user -> getManagerUsers() );
		break;
	case "saveNotification":
		$usernot = new UserNotification();
		$res = $usernot -> saveNotifications();
		$response = array();
		if( $res > 0) {
			$response = array("text" => "Nofication saved successfully", "error" => false);
		} else {
			$response = array("text" => "Error saving notification" , "error" => true);
		}
		echo json_encode($response);
		break;
	case "deleteNotification":
		$usernot = new UserNotification();
		$res = $usernot -> deleteNotification(  $_POST['id'] );
		if( $res > 0)
		{
			$response = array("text" => "Notification deleted successfully", "error" => false);
		} else {
			$response = array("text" => "Error updating notification", "error" => true);
		}
		echo json_encode( $response );	
		break;
	case "updateNotification":
		$usernot = new UserNotification();
		$res = $usernot -> updateNotification($_POST['id'] );
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Notification updated successfully", "error" => false);
		} else {
			$response = array("text" => "Error updating notification ", "error" => true);
		}
		echo json_encode( $response );
		break;		
	case "getNotifications":
		$usernot = new UserNotification();
		$results = $usernot -> getNotifications();
		$notifications = array();
		foreach( $results as $key => $notification)
		{
			$notifications[$notification['id']] = array(
														"id" 	  => $notification['id'],
														"what" 	  => ucwords( str_replace("_", " ", $notification['recieve_what']) ),
														"when"    => ucwords( str_replace("_", " ", $notification['recieve_when']) ),
														"day"     => printDayOfWeek( $notification['recieve_day'] ),
														"recieve" => $notification['recieve_email'],
														"recieve_what" => $notification['recieve_what'],
														"recieve_when" => $notification['recieve_when'],
														"recieve_day" => $notification['recieve_day'],
													  );
		}
		echo json_encode( $notifications );
		break;
    case "getActions":
        $actionObj = new RiskAction('', '', '', '', '', '', '','', '');
        $risk_actions = $actionObj->getActions($_REQUEST);
/*
        $act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
        $colmObj = new ActionColumns();
        $options = "";
        if( $_GET['section'] == "manage")
        {
            $options = " AND active & 4 = 4";
        } else {
            $options = " AND active & 8 = 8";
        }
        $headers 	 = $colmObj -> getHeaderList( $options );
        $riskActions = $act -> getRiskActions( $_GET['start'], $_GET['limit'] );
        $actions 	 = array();
        $status      = array();
        $progress	 = array();
        $headerArr   = array();
        $actionOwner = array();
        foreach( $headers as $field => $head)
        {
            foreach( $riskActions as $index => $valArr)
            {
                $status[$valArr['id']]  	= $valArr['actionstatus'];
                $actionOwner[$valArr['id']] = $valArr['isUpdate'];
                if( array_key_exists($field, $valArr))
                {
                    $actions[$valArr['id']][$field]  = ($field == "progress" ? $valArr[$field]."%" :  $valArr[$field]);
                    $headerArr[$field]				 = $head;
                }
            }
        }
        $actionStats = $act-> getActionStats();*/
        echo json_encode($risk_actions);
        break;
    case "saveActionAttachment":
        $key       = str_replace("_attachment", "", $_REQUEST['element']);
        $attObj    = new Attachment($key, $_REQUEST['element']);
        $results   = $attObj -> upload($key);
        echo json_encode($results);
        exit();
        break;
    case "removeActionFile":
        $key      = str_replace("_attachment", "", $_REQUEST['element']);
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment($key, $_REQUEST['element']);
        $response = $attObj -> removeFile($file, $key);
        echo json_encode($response);
        exit();
        break;
    case "deleteActionAttachment":
        $file      = $_POST['attachment'];
        $attObj    = new Attachment($_POST['type']."_".$_POST['id'], $_POST['type']."_".$_POST['id']);
        $response  = $attObj -> deleteFile($file, $_POST['filename']);
        echo json_encode($response);
        exit();
        break;
    case "newActionUpdate":
        $action_id      = $_POST['id'];
        $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
        $action	        = $actObj -> getActionDetail($action_id) ;
        $statusObj 		= new ActionStatus("","", "");
        $statuses 		= $statusObj -> getStatuses();
        $nameObj 	    = new Naming();
        $attachment     = array();
        $change_arr 	= array();
        $change_msg	    = "";
        $update_data    = array();
        $attStr 		= "";
        //$att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
        if($_POST['status'] !== $action['status']) {
            $from                        = $statuses[$action['status']-1]['name'];
            $to                          = $statuses[$_POST['status']-1]['name'];
            $update_data['status']       = $_POST['status'];
            $change_msg                 .= "Action Status changed to ".$to." from ".$from."\r\n";
            $change_arr['action_status'] = array('from' => $from, 'to' => $to);
            $change_arr['currentstatus'] = $to;
        }

        if($_POST['progress'] != $action['progress']) {
            $from                          = $action['progress'];
            $to                            = $_POST['progress'];
            $update_data['progress']       = $_POST['progress'];
            $change_msg                   .= "Action Progress changed to ".$to."% from ".$from."%\r\n";
            $change_arr['action_progress'] = array('from' => $from."%", 'to' => $to."%");
        }

        if($_POST['remindon'] != $action['remindon']) {
            $from                           = $action['remindon'];
            $to                             = $_POST['remindon'];
            $update_data['remindon']        = $_POST['remindon'];
            $change_msg                    .= "Action Remind On changed to ".$to." from ".$from."\r\n";
            $change_arr['action_remind_on'] = array('from' => $from, 'to' => $to);
        }
        if(isset($_POST['action_on'])) {
            if($_POST['action_on'] != $action['action_on']) {
                $from                  = (strtotime($action['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($action['action_on'])) );
                $to                    = (strtotime($_POST['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['action_on'])) );
                if(strtotime($from) !== strtotime($to)) {
                    $change_arr['action_on']  = array('from' => $from, 'to' => $to);
                    $change_msg              .= "Action on changed from ".$action['action_on']." to ".$_POST['action_on']."\r\n";
                    $update_data['action_on'] = date("Y-m-d", strtotime($_POST['action_on']));
                }
            }
        }

        if(isset($_POST['description']) && !empty($_POST['description'])) {
            $change_arr['response'] = $_POST['description'];
            $change_msg            .= " Added response ".$_POST['description']."\r\n";
        }

        if(isset($_POST['approval']) && $_POST['approval'] == 'on') {
            $change_arr['approval'] = "Requested sending of approval email.";
            $change_msg            .= "Requested sending of approval email.\r\n";
        }

        if($_POST['progress'] == 100 && $_POST['status'] == 3) {
            $change_arr['approval_status']  = "Action moves to awaiting approval .";
            $change_msg                    .= "Action moves to awaiting approval.\r\n";
            if(($action['action_status'] & RiskAction::ACTION_AWAITING) != RiskAction::ACTION_AWAITING)
            {
                $update_data['action_status']   = $action['action_status'] + RiskAction::ACTION_AWAITING;
            }
        }

        if(isset($_SESSION['uploads']))
        {
            /*if(isset($_SESSION['uploads']['actionchanges']))
            {
                $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
                $change_msg              .= $att_change_str.'\r\n';
            }
            if(isset($_SESSION['uploads']['attachments']))
            {
                if($action['attachement'] != $_SESSION['uploads']['attachments'])
                {
                    $update_data['attachment'] = $_SESSION['uploads']['attachments'];
                }
            }*/
        }

        $updates = array();
        if(!empty($change_arr)) {
            if(!isset($change_arr['currentstatus'])) {
                $change_arr['currentstatus'] = $statuses[$action['status']-1]['name'];
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
            $updates['insertuser'] = $_SESSION['tid'];
        }
        $id                      = $actObj -> updateAction($action_id, $update_data, $updates);
        $response  = array();
        if($id > 0) {
            $response["id"] 	 = $id;
            $error 				 = false;
            $text 				 = " New update for $action_object_name ".RiskAction::REFTAG.$action_id." has been successfully saved <br />";
            /*$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
            $emailTo 		     = $user -> getRiskManagerEmails();
            $email 			     = new Email();

            $email -> userFrom  = $_SESSION['tkn'];
            $email -> subject   = " Update $action_object_name ";
            //Object is something like Risk or Action etc.  Object_id is the reference of that object
            $message            = "";
            //$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
            $message           .= " \n";	//blank row above Please log on line
            $message           .= $_SESSION['tkn']." Updated $action_object_name #".$action_id." related to $risk_object_name #".$action['risk_id']."\r\n<br /><br />";
            if(!empty($change_msg))
            {
                $message .= $change_msg;
            }
            if(!empty($att_change_str))
            {
                $message .= $att_change_str;
            }

            $message .= "\r\nPlease log onto Assist in order to View further information \n";
            $message           = nl2br($message);
            $email -> body     = $message;
            $email_to          = "";
            if(!empty($mailTo))
            {
                $email_to = $emailTo.",";
            }
            $email_to          = $action['tkemail'];
            $email -> to 	  .= $email_to;
            //$email -> to 	  = $email_to;
            $email -> from     = "no-reply@ignite4u.co.za";
            if($email -> sendEmail())
            {
                $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
            } else {
                $error = true;
                $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
            }*/
            $response = array("text" => $text, "error" => $error, 'updated' => true);
        } elseif($id == 0){
            $response = array("text" => "No change made to the $action_object_name", "error" => false, "updated" => false);
        } else {
            $response = array("text" => "Error saving the $action_object_name update", "error" => true, "updated" => false);
        }
        //Attachment::clear('riskadded', 'action_'.$action['id']);
        echo json_encode($response);
        break;
	case "ApproveGetActionOwnersBySub":
		$sub_ids = $_REQUEST['s'];
		$actionObj = new RGSTR_ACTION();
		$actions = $actionObj->getActionOwnersForApproval($sub_ids);
		echo json_encode($actions);
		break;
    default:
			//echo "It hasn't found the actions associated";
		break;

	} //end switch $_REQUEST['action'];

	function riskChange( $requestArr )
	{
		$risk 		    = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$riskInfo  		= $risk -> getRisk( $requestArr['id'] );
		$names 						  = new Naming();
		$header 					  = $names -> getHeaderNames();
		
		
		$riskStatus		= ""; //this is the status of the risk currenty   		
		//setting the calues , to ids so can compare the chnage on id, 
		$riskInfo['type'] 			= $riskInfo['typeid'];
		$riskInfo['impact']			= $riskInfo['impactid'];
		$riskInfo['likelihood']		= $riskInfo['likelihoodid'];
		$riskInfo['category']		= $riskInfo['categoryid'];
		$riskInfo['effectiveness']	= $riskInfo['effectivenessid'];
		$riskInfo['status']			= $riskInfo['statusId'];	
		/*print "<pre>";
			print_r($header);
			print_r($riskInfo);
			print_r($requestArr);
		print "</pre>";
		*/				
		$changeMessage 			= "";
		$changeArr 	   			= array();
		foreach( $requestArr as $key => $value)
		{
			if( isset($riskInfo[$key]) || array_key_exists($key, $riskInfo))
			{
				if( $riskInfo[$key] !== $value){
					if( $key == "type")
					{
						$ty 		= new RiskType("", "", "" , "");
						$fromtype	= $ty -> getARiskType( $riskInfo[$key] );
						$from    	= $fromtype['name']; 
						$totype		= $ty -> getARiskType( $value );
						$to			= $totype['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "level"){
						$lv 		= new RiskLevel("", "", "" , "");
						$fromLevel	= $lv -> getARiskLevel( $riskInfo[$key] );
						$from    	= $fromLevel['name']; 
						$toLevel	= $lv -> getARiskLevel( $value );
						$to			= $toLevel['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "financial_exposure"){
						$fn 		= new FinancialExposure("", "", "", "" , "");
						$fromFin	= $fn -> getAFinancialExposure( $riskInfo[$key] );
						$from    	= $fromFin['name']; 
						$toFin		= $fn -> getAFinancialExposure( $value );
						$to			= $toFin['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "impact"){
						$ty 		= new Impact("", "", "" , "", "");
						$fromimpact	= $ty -> getAImpact( $riskInfo[$key] );
						$from 	    = $fromimpact['assessment']; 
						$toimpact	= $ty -> getAImpact( $value );
						$to			= $toimpact['assessment'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";						
					} else if( $key == "category"){
						$ty 		= new RiskCategory("", "", "" , "");
						$fromcat	= $ty -> getACategory( $riskInfo[$key] );
						$from 	    = $fromcat['name']; 
						$tocat		= $ty -> getACategory( $value );
						$to			= $tocat['name'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";	
					} else if( $key == "likelihood"){
						$ty 		= new Likelihood("", "", "" , "", "", "");
						$fromlike	= $ty -> getALikelihood( $riskInfo[$key] );
						$from 	    = $fromlike['assessment']; 
						$tolike		= $ty -> getALikelihood( $value );
						$to			= $tolike['assessment'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "effectiveness"){
						$ty 		= new ControlEffectiveness("", "", "" , "", "");
						$fromeffect	= $ty -> getAControlEffectiveness( $riskInfo[$key] );
						$from 	    = $fromeffect['effectiveness']; 
						$toeffect	= $ty -> getAControlEffectiveness( $value );
						$to			= $toeffect['effectiveness'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";							
					 }else if( $key == "status"){
						$ty 		= new RiskStatus("" , "", "");
						$fromstatus	= $ty -> getAStatus( $riskInfo[$key] );
						$from 	    = $fromstatus['name']; 
						$tostatus	= $ty -> getAStatus( $value );
						$to			= $tostatus['name'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
						$riskStatus		 = $to;															
					} else if($key == "user_responsible"){
						$ty 		= new Directorate("" , "", "", "");
						$fromstatus	= $ty -> getDirecto( $riskInfo[$key] );
						$from 	    = $fromstatus['dirtxt']; 

						$tostatus	= $ty -> getDirecto( $value );
						$to			= $tostatus['dirtxt'];												
						$changeArr[$key]  = array("from" => $from, "to" => $to);
						$changeMessage   .= setHeader($key)." changed to ".$to." from ".$from."\r\n\n";		
					} else if( $key == "attachment"){
						$attachments = array();
						if( $riskInfo[$key] != "")
						{
							$attachments = unserialize($riskInfo[$key]);
						}
						if( isset($_SESSION['editdeleted']) && !empty($_SESSION['editdeleted']))
						{
							if(isset($attachments) && !empty($attachments))
							{	
								foreach( $_SESSION['editdeleted'] as $k => $val){
									if( isset($attachments[$k]) && !empty($attachments[$k]))
									{
										unset($attachments[$k]);
										
										$changeArr[$key][$k]    = "Deleted attachment ".$val;
										$changeMessage   	   .= "Deleted attachment ".$val."\r\n\n";
									}	
								}
								$_SESSION['uploads']['edits'] = $attachments;
							}	
						}
											
					} else if( $key == "current_controls") {
						$to   = str_replace("_", ",", $value);
						$from = str_replace("_", ",", $riskInfo[$key]);
						$changeArr[$key] = array("from" => $from, "to" => $to );
						$changeMessage  .= setHeader( $key )." changed to ".$to." from ".$from."\r\n\n";						
						
					} else {
						$changeArr[$key] = array("from" => $riskInfo[$key], "to" => $value );
						$changeMessage  .= setHeader( $key )." changed to ".$value." from ".$riskInfo[$key]."\r\n\n";	
					}
				} 
			} else if( $key == "response" ){
						$changeArr[$key] = $value."";
						$changeMessage  .= "Added Response ".$value."\r\n\n";
			}
		}	
		$changeArr['user']  = $_SESSION['tkn'];
		//set the status of the risk at this time 
		if($riskStatus == '')
		{
		   $statusObj	                = new RiskStatus('', '', '');
		   $fromstatus	                = $statusObj -> getAStatus($riskInfo['status']);
		   $changeArr['currentstatus'] = $fromstatus['name'];  	
		} else {
		   $changeArr['currentstatus'] = $riskStatus;
		}
		
		//check the added attachments and add them to the audit log array
		if( isset($_SESSION['uploads']['editadded']) && !empty($_SESSION['uploads']['editadded']))
		{
			$currentAttachments  = array();
			if( $riskInfo['attachment'] != "" )
			{
				$currentAttachments = unserialize($riskInfo['attachment']);
			} 
			$_SESSION['uploads']['edits'] = array_merge($_SESSION['uploads']['editadded'] , $currentAttachments);
			foreach( $_SESSION['uploads']['editadded'] as $key => $val)
			{
				$changeArr['attachment'][$key] = "Added attachment ".$val;
				$changeMessage  			  .= "Added attachment ".$val."\r\n\n";
			}	
		}
		if( isset($_SESSION['editdeleted']) )
		{
			unset($_SESSION['editdeleted']);
		}
		if( isset($_SESSION['uploads']['editadded']) )
		{
			unset($_SESSION['uploads']['editadded']);
		}		
		return array(
					"changeArr" 		=> $changeArr,
				 	"changeMessage" 	=> $changeMessage,
					"responsiblePerson" => (isset($riskInfo['responsible_person']) ? $riskInfo['responsible_person'] : "")
				);
	}
	
	function getRiskChanges( $riskLog, $context )
	{
		$changes 		= array();
		$changeLog		= array();
		$changeMessage  = "";
		foreach( $riskLog as $key => $log)
		{	
			$id				   		  = $log['risk_item'];
			$changes	        	  = unserialize($log['changes']);
			$changeLog[$id]['date']   = $log['insertdate'];
			$changeLog[$id]['status'] = $log['risk_status']; 
			if( isset( $changes) && !empty($changes) ){
				$changeMessage  = "The $risk_object_name has been ".$context." by ".(isset($changes['user']) ? $changes['user'] : "")."<br /><br />";
				$changeMessage .= "The following changes were made : <br />";			
				foreach( $changes as $key => $change)
				{
					if( $key == "user")
					{ continue; } else if($key == "response"){
					   $changeMessage .= "".$change." <br /> ";
					} else{
						 $changeMessage .= setHeader( $key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
					}
				}
			}
		  $changeLog[$id]['changeMessage'] = $changeMessage;
		}		
		usort( $changeLog , "compare");
		return $changeLog;
	}
	
	function setHeader( $key )
	{
		$names 		  = new Naming();
		$header 	  = $names -> getHeaderNames();	
		$headerName   = ""; 
		foreach($header as $index => $name )
		{
			if( $name['name'] == $key )
			{
				$headerName = (isset($name['client_terminology']) ? $name['client_terminology'] : $name['ignite_terminology']);
			} else{
				$headerName = ucwords( str_replace("_", " ", $key ) );
			}
		}
		return $headerName;
	} 

	function compare($a ,$b)
	{
		return ( $a['date'] > $b['date'] ? -1 : 1 );
	}
	
	function printDayOfWeek( $number )
	{
		switch ( $number )
		{
			case 0:
				return "Sunday";
			break;
			case 1:
				return "Monday";
			break;
			case 2:
				return "Tuesday";
			break;
			case 3:
				return "Wednesday";
			break;				
			case 4:
				return "Thursaday";
			break;
			case 5:
				return "Friday";
			break;
			case 6:
				return "Saturday";
			break;																				
		}			
	} 	
?>
