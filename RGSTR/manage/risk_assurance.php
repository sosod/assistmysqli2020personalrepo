<?php
$scripts = array( 'risk_assurance.js','jquery.ui.riskassurance.js',  'ajaxfileupload.js');
$styles = array( );
//$page_title = "Assurance of risk";

$folder = "manage";
$page = "assurance";

/*$menuT = array(
	1 => array('id'=> "risk_assurance",'url'=>"risk_assurance.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "action_assurance",'url'=>"../actions/action_assurance.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);*/


$page_title = "|OBJECTNAME|";

require_once("../inc/header.php");
?>
<script>
$(function(){
	$("table#risk_assurance_table").find("th").css({"text-align":"left"})
	$("#riskassurance").riskassurance({risk_id:$("#risk_id").val(),objectName:window.risk_object_name, actionName:window.action_object_name});
});
</script>
<div id="newassurance_message" class="message"></div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form method="post" name="assurance-form" id="assurance-form">
<table align="left" border="1" id="assurance" class="noborder">
	<tr>
    	<td valign="top" class="noborder">
        	<table id="risk_assurance_table">
            	<tr>
                	<td colspan="2"><h4><?php echo $risk_object_name; ?> Information</h4></td>                  
                </tr>
				<tr id="goback">
					<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
					<td class="noborder"></td>
				</tr>
            </table>
        </td>
        <td valign="top" class="noborder">
        	<div id="riskassurance"></div>
         </td>
    </tr>
</table>
<input type="hidden" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" id="risk_id"  /> 
</form>

