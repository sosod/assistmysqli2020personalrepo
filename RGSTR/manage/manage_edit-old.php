<?php
$folder = "manage";
$page = "edit_risk";

$menuT = array(
	1 => array('id'=> "edit_risk",'url'=>"edit.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "edit_action",'url'=>"edit_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);


$scripts = array( 'jquery.ui.riskaction.js', 'menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit |OBJECTNAME|";
require_once("../inc/header.php");
?>
<script language="javascript">
$(function(){
/*	$("#display_mine").click(function(){
			$("#risk").risk({editRisk:true, editActions:true , section:"manage", view:"viewMyn"});
			return false;
		});

		$("#display_all").click(function(){
			$("#risk").risk({editRisk:true, editActions:true , section:"manage", view:"viewAll"});
			return false;
		});		*/
        $("#risk").riskaction({editRisk:true, editActions:true, section:"manage", view:"viewMyn", page:'manage_edit', showActions:false,objectName:window.risk_object_name, actionName:window.action_object_name});
		//$("#risk").risk({editRisk:true, editActions:true , section:"manage", view:"viewMyn"});
});
</script>
<div id="risk"></div>

