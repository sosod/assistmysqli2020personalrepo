<?php
$scripts = array( 'editrisk.js','menu.js','ajaxfileupload.js'  );
$styles = array( 'colorpicker.css' );
$folder = "manage";
$redirect = "update_risks.php";
$page = "update_risk";
$menuT = array(
	1 => array('id'=> "display_risk",'url'=>"update_risks.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "display_action",'url'=>"update_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);



$page_title = "Update |OBJECTNAME|";


require_once("../inc/header.php");

include_once("../common/update_risk.php");

?>