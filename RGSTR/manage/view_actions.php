<?php
$folder = "manage";
$page = "assurance";

$scripts = array( 'jquery.ui.action1.js','menu.js', 'actions.js'  );
$styles = array( 'colorpicker.css' );

$page_title = "|ACTIONNAME|";


require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		//$("#actions").action1({risk_id:<?php echo $_GET['id']; ?>, extras:false, section:"manage"});
        $("#actions").action1({risk_id:<?php echo $_REQUEST['id']; ?>, updateAction:false, page:"assurance", section:"manage",objectName:window.risk_object_name, actionName:window.action_object_name});
	});
</script>
<div id="actions"></div>
