<?php
$folder = "manage";
$page = "edit_action";

$scripts = array( 'action.js' ,'functions.js', 'ajaxfileupload.js' );
$styles = array( 'colorpicker.css' );
$page_title = "Edit |ACTIONNAME|";
require_once("../inc/header.php");
require_once("controller.php");

$act 		= new RiskAction("", "", "", "", "", "", "","", "");
$action 	= $act -> getRiskAction( $_REQUEST['id'] );

$stat 		= new RiskStatus("", "", "");
$statuses 	= $stat -> getStatus();
$latestUp 	= $act -> getLatestActionUpdates( $_REQUEST['id'] );

$stastusId  = (empty($latestUp) ? $action['status'] : $latestUp['name']);
$uaccess 	= new UserAccess( "", "", "", "", "", "", "", "", "");
$users 		= $uaccess -> getUsers();

$nameObj = new Naming();
$names = $nameObj->getReportingNaming("action");


function displayHeader($fld) {
	global $names;
	if(isset($names[$fld])) {
		return $names[$fld];
	} else {
		return ucword(str_replace("_"," ",$fld));
	}
}

?>
<script>
$(function(){
	$("table#edit_action_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form method="post" name="edit-action-table" id="edit-action-table">
   <table align="left" id="edit_action_table" class=form>
   	<tr>
    	<th>Ref:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
   	<tr>
    	<th><?php echo displayHeader('risk_action'); ?>:</th>
        <td>
        	<textarea class="smallbox" name="action" id="action"><?php echo $action['action']; ?></textarea>
        </td>
    </tr>
    <tr>
    	<th><?php echo displayHeader('deliverable'); ?>:</th>
        <td><textarea class="mainbox" id="deliverable" name="deliverable"><?php echo $action['deliverable']; ?></textarea></td>
    </tr>
   	<tr>
    	<th><?php echo displayHeader('action_owner'); ?>:</th>
        <td>
        	<select name="action_owner" id="action_owner">
            	<option>--- SELECT ---</option>
                <?php
                foreach($users as $user) {
					echo "<option value='".$user['tkid']."' ".(($user['tkid']==$action['action_owner'])? "selected='selected'" :"").">".($user['tkname']." ".$user['tksurname'])."</option>";
				}
				?>
            </select>
        </td>
    </tr>
   	<tr>
    	<th><?php echo displayHeader('timescale'); ?>:</th>
        <td><input type="text" id="timescale" name="timescale" value="<?php echo $action['timescale']; ?>" /></td>
    </tr>
   	<tr>
    	<th><?php echo displayHeader('deadline'); ?>:</th>
        <td><input type="text" id="deadline" name="deadline" value="<?php echo $action['deadline']; ?>" class="datepicker" readonly="readonly"/></td>
    </tr>
       	<tr>
    	<th>Reminder:</th>
        <td><input type="text" id="remindon" name="remindon" value="<?php echo $action['remindon']; ?>" class="futuredate"  readonly="readonly"/></td>
    </tr>
    <tr>
		<th></th>
    	<td>
		   <input type="submit" name="edit_action" id="edit_action" value="Save Changes" class="isubmit" />
   		   <input type="submit" name="reset" id="reset" value="Reset"  />
		   <input type="hidden" name="actionid" id="actionid" value="<?php echo $_REQUEST['id'] ?>"  />
		   <input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>" class="logid"  />
	        </td>
    </tr>
   <tr>
   	<td  align="left" class="noborder">
    	<?php $me->displayGoBack("",""); ?>
    </td>
   	<td class="noborder" align="right">
    	<?php
		$admire_helper->displayAuditLogLink( "action_edit", false); ?>
    </td>    
   </tr>
   </table>
</form>
<div id="actioneditlog" style="clear:both;"></div>
</div>

