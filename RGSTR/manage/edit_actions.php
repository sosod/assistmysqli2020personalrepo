<?php
$folder = "manage";
$page = "edit_actions";

	//1 => array('id'=> "edit_risk",'url'=>"edit.php",'active'=> "",'display'=>"|OBJECTNAME|"),
$menuT = array(
	2 => array('id'=> "edit_actions",'url'=>"edit_actions.php",'active'=> "Y",'display'=>"|ACTIONNAME|"),
);

$scripts = array( 'jquery.ui.action1.js','menu.js','actions.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit |ACTIONNAME|";
require_once("../inc/header.php");

?>
<script language="javascript">
    $(function(){
        $("table#edit_action_table").find("th").css({"text-algn":"left"})
        $("#actions").action1({editAction:true, page:"edit_actions", section:"manage",objectName:window.risk_object_name, actionName:window.action_object_name});
    });
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="actions"></div>
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
