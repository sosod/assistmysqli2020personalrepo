<?php
$scripts 	= array( 'action.js?v='.strtotime(date("d-F-Y H:i:s")),'menu.js',  'ajaxfileupload.js'  );
$styles 	= array( 'colorpicker.css',   );
$folder = "manage";
$page = "update_action";

$menuT = array(
	1 => array('id'=> "update_risks",'url'=>"update_risks.php",'active'=> "",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "update_actions",'url'=>"update_actions.php",'active'=> "Y",'display'=>"|ACTIONNAME|"),
);



$page_title = "Update |ACTIONNAME|";

include "../common/update_action.php";

?>