<?php
$folder = "manage";
$page = "view";

$scripts = array('jquery.ui.riskaction.js', 'jquery.ui.action1.js', 'menu.js');
$styles = array();
//$page_title = "View Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
$(function(){
	$("#risk").riskaction({viewRisk:false, section:"manage", view:"viewAll", showActions:true, page: 'view',objectName:window.risk_object_name, actionName:window.action_object_name});
});
</script>
<div id="risk"></div>

