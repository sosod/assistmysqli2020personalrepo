<?php
$folder = "manage";
$page = "update_risks";

$menuT = array(
	1 => array('id'=> "update_risks",'url'=>"update_risks.php",'active'=> "Y",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "update_actions",'url'=>"update_actions.php",'active'=> "",'display'=>"|ACTIONNAME|"),
);

$scripts = array( 'jquery.ui.riskaction.js', 'menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Update |OBJECTNAME|";
require_once("../inc/header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<script language="javascript">
$(function(){
/*	$("#display_mine").click(function(){
			$("#risk").risk({editRisk:true, editActions:true , section:"manage", view:"viewMyn"});
			return false;
		});

		$("#display_all").click(function(){
			$("#risk").risk({editRisk:true, editActions:true , section:"manage", view:"viewAll"});
			return false;
		});		*/
        $("#risk").riskaction({updateRisk:true, section:"manage", view:"viewMyn", page:'manage_update', showActions:false,objectName:window.risk_object_name, actionName:window.action_object_name});
		//$("#risk").risk({editRisk:true, editActions:true , section:"manage", view:"viewMyn"});
});
</script>
<div id="risk"></div>

