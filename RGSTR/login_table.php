<?php
/* requires $tdb as object of ASSIST_DB class => created in main/tables.php */
				$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'deadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'risk_action'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'action_owner'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'deliverable'=>array('text'=>"", 'long'=>true, 'deadline'=>false),
					'progress'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'action_status'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);

$sql = "SELECT name, client_terminology FROM ".$dbref."_header_names WHERE type != 'risk'";
//$rs = $tdb->db_query($sql);
$rows = $tdb->mysql_fetch_all($sql);
	//while($row = mysql_fetch_array($rs)) {
foreach ($rows as $row){
		if(isset($head[$row['name']])) {
			$head[$row['name']]['text'] = $row['client_terminology'];
		}
	}
//unset($rs);

	$sql = "SELECT 	DISTINCT(RA.id) ,
			RA.action,
			RA.deliverable,
			RA.deadline,
			RA.progress,
			RA.status AS statusid,
			RAS.name as status,
			RAS.color as statuscolor,
			CONCAT(TK.tkname,' ',TK.tksurname) AS owner,
			RA.action_owner
			FROM  ".$dbref."_actions RA 
			INNER JOIN ".$dbref."_risk_register R 
			  ON R.id = RA.risk_id AND R.active > 0 
			LEFT JOIN ".$dbref."_action_status RAS ON RA.status = RAS.id			
			LEFT JOIN assist_".$cmpcode."_timekeep TK ON  TK.tkid  = RA.action_owner 
			WHERE RA.action_owner = '$tkid' AND RA.deadline <= ($today + 3600*24*$next_due)  	
			ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " STR_TO_DATE(RA.deadline,'%d-%M-%Y') DESC "; break;
						default:
						case "dead_asc":	$sql.= " STR_TO_DATE(RA.deadline,'%d-%M-%Y') ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20); //echo $sql;
	$tasks = $tdb->mysql_fetch_all_fld($sql,"id");
	
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));	
	foreach($tasks as $id => $task) {
		$deadline = "";
	    $stattusid = $task['statusid'];		
		if($stattusid !== "3") {
			$deaddiff = $today - strtotime($task['deadline']);
			$diffdays = floor($deaddiff/(3600*24));
			if($deaddiff<0) {
				$diffdays*=-1;
				$days = $diffdays > 1 ? "days" : "day";
				$deadline =  "<span class='soon'>Due in $diffdays $days</span>";
			} elseif($deaddiff==0) {
				$deadline =  "<span class='today'><b>Due today</b></span>";
			} else {
				$days = $diffdays > 1 ? "days" : "day";
				$deadline = "<span class='overdue'><b>$diffdays $days</b> overdue</span>";
			}			

			$actions[$id] = array();
			$actions[$id]['ref'] = $id;
			$actions[$id]['deadline'] = $deadline;
			$actions[$id]['risk_action'] = $task['action'];
			$actions[$id]['action_owner'] = $task['owner'];
			$actions[$id]['deliverable'] = $task['deliverable'];
			$actions[$id]['progress'] = ($task['progress'] == "" ? 0 : $task['progress'])."%";
			$actions[$id]['action_status'] = ($task['status'] == "" ? "New" : $task['status']);
			$actions[$id]['link'] = "manage/update_action.php?id=".$task['id'];
		}
	}

?>