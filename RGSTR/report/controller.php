<?php
@session_start();
include_once("../inc/init.php");


switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {
	case "getRiskRegister":
		//$type = new Risk( "", "", "", "" ,"", "", "", "", "", "", "", "", "", "");
		//echo json_encode( $type-> getAllRisk( (isset($_REQUEST['start']) ?  $_REQUEST['start'] : ""), (isset($_REQUEST['limit']) ? $_REQUEST['limit'] : "" ) ) );
		$rep		  = new Report();
		$riskRegister = $rep -> getRiskRegister();
		echo json_encode( $riskRegister );
		break;					
	case "getRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode( $risk -> getAllRiskTogether( $_REQUEST['start'], $_REQUEST['limit'] ) );
		//echo json_encode( $risk -> getAllRisk( $_REQUEST['start'], $_REQUEST['limit'] ) );
		break;		
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;		
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;
	case "getCategories":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;
 	case "getFinancialExposure":
		$fin = new FinancialExposure( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $fin -> getFinancialExposure() );
		break;
	case "getRiskStatus":
		$riskstatus = new RiskStatus( "", "", "" );
		echo json_encode( $riskstatus -> getStatus() );
		break;
	case "getRisklevel":
		$risklevel = new RiskLevel( "", "", "", "");
		echo json_encode( $risklevel -> getLevel() );
		break;

	case "generateReport":
		$rep 		= new Report();
		$dbNames 	= $rep  -> getHeaders();
		$headers 	= array(); 
		$where 		= "";
		$fields		= "";
		$from 		= "";
		$groupby 	= "";
		$riskAction = "";
		if( is_array( $_REQUEST['headers'] ) ){
			foreach( $_REQUEST['headers'] as $key => $headerValue ) {
				//get the heading names to display in the report	
			/*	echo "How about this ".$dbNames[$headerValue['name']]."\r\n\n";*/
	
				foreach( $dbNames as $ref => $value ) {
					//echo $headerValue['name']."\r\n\n";
					if( $headerValue['name'] == $ref ) {
						$headers[] = $value;
					}		
				} 
				//echo $headerValue['name']."\r\n";
				//$headers[] = $rep -> getHeaders( $headerValue['name'] );
				
				$fields .= $rep -> getField( $headerValue['name'] );
				$from 	.= $rep -> getTableFrom( $headerValue['name'] );
			}
		} 
		
		
		$where  = $rep -> getWhere( $_REQUEST['values'] );
				
		//create the group by if it has been chosen
		if( isset( $_REQUEST['grouping']) ) {
			$groupby = $rep -> getGrouping( rtrim( $_REQUEST['grouping'][0]['value'],"_") );
		}
		if($where !== 1 && !empty($_REQUEST['includeaction']) && empty($_REQUEST['noAction']) ) {
			$riskAction = "LEFT JOIN ".$_SESSION['dbref']."_actions RA ON RA.risk_id = RR.id";
		}
		$from 		  = $_SESSION['dbref']."_risk_register RR \r\n".$riskAction." ".$from;

		$response = $rep -> generateReport( rtrim(trim($fields),",") ,$where , $from ,$groupby );
		echo json_encode( array("headers" => $headers, "data" => $response) );
		break;
	case "_generateReport":
		$mainArray = array();
		$headers   = array();
		foreach( $_REQUEST['headers'] as $key => $nameValue ) {
		 foreach($_REQUEST['values'] as $val => $valuesArray ) {	 
		 	// if its a clicked check box ,, the get its value entered	
			if( ltrim($valuesArray['name'],"_") ==  $nameValue['name'] ) {
				
				$mainArray[$nameValue['name']] = array(	
															"value" 		=> $valuesArray['value'],
															"grouping"		=> "",
															"sortable"		=> "",
															"tablePrefix"	=> ""
													  );
				// if there is grouping , add the grouping key to the main array
				foreach( $_REQUEST['grouping'] as $key => $groupValue ) {	
					if( rtrim($groupValue['value'],"_") == $nameValue['name'] )	{
						$mainArray[$nameValue['name']]["grouping"] =  "Yes";
					}
					if( ltrim( $groupValue['value'],"__") == $nameValue['name'] ) {
						$mainArray[$nameValue['name']]["sortable"] =  "Yes";
					}
			   }
				// add table and the table prefix	
				if(array_key_exists( $nameValue['name'], $fieldTable ) && isset( $fieldTable[$nameValue['name']]['table'] ) ) {
					$headers[] = $nameValue['name'] ;
					$mainArray[$nameValue['name']]["searchArray"] =  (isset($fieldTable[$nameValue['name']]['searchField']) ? $fieldTable[$nameValue['name']]['searchField']  : "");
					
					if( isset($fieldTable[$nameValue['name']]['as']) && !empty($fieldTable[$nameValue['name']]['as']) ) {
					 $mainArray[$nameValue['name']]["AS"] =   $fieldTable[$nameValue['name']]['as'];	
					}
					$mainArray[$nameValue['name']]["returnField"] = $fieldTable[$nameValue['name']]['name'];
					$mainArray[$nameValue['name']]["table"]		  =  $fieldTable[$nameValue['name']]['table'];
					$mainArray[$nameValue['name']]["tablePrefix"] =  $fieldTable[$nameValue['name']]['prefix'];	
					$mainArray[$nameValue['name']]["join"] 		  =  $fieldTable[$nameValue['name']]['joinField'];								
				}						
				}

		  }
	    }
		$rep 		= new Report();
		$response 	= $rep -> generateReport( $mainArray );
		echo json_encode( $response );
		break;
		case "residualVsInherent":
			$rep 		= new Report();
			echo json_encode( $rep -> _residualVsInherent() );
		break;	
		case "getRiskPerCategory":
			$rep 		= new Report();
			$rCategory  = $rep -> _getRiskCategory();
			echo json_encode( $rCategory );
		break;		
		case "getRiskPerType":
			$rep 	= new Report();
			$rType  = $rep -> _getRiskType();
			echo json_encode( $rType );
		break;	
		case "getRiskPerson":
			$rep 	= new Report();
			$rPerson  = $rep -> _getRiskPerson();
			echo json_encode( $rPerson );
		break;	
		case "getQuickReports":
			$rep 	= new Report();
			$reports= $rep -> getQuickReports( $_GET['start'], $_GET['limit'] );
			echo json_encode( $reports );				
		break;						
		case "deleteReport":
			$rep= new Report();
			$res= $reports= $rep -> updateQuickReport( $_POST['id'] );
			$response = array();
			if( $res > 0)
			{
				$response = array("text" => "Quick report deleted successfully", "error" => false);
			} else {
				$response = array("text" => "Error deleting quick report", "error" => true);
			}
			echo json_encode( $response );			
		break;
		case "getAQuickReport":
			$rep  = new Report();
			$data = $rep -> getQuickData( $_GET['id']);
			$data = unserialize( $data );
			echo json_encode( $data );
		break;
		case "riskVSstatus":
			$qrep     = new Report();
			$rstatObj = new RiskStatus("", "","");
			$riskstatus = $rstatObj -> getStatus();
			$riskstat   = $qrep -> getRiskperStatus();
			$rkstatus = array();
			$status	  = array();
			foreach( $riskstatus as $index => $statArr)
			{
				$status[$statArr['id']] = $statArr['name'];
			}
			foreach( $riskstat as $i => $val)
			{
				$rkstatus[$i] = array( "status" => $status[$val['status']], "total" => $val['total'] );
			}				
			echo json_encode( $rkstatus );
		break;		
		case "actionVSstatus":
			$qrep     	= new Report();
			$actstatObj = new ActionStatus("", "","");
			$actionstatus =  $actstatObj -> getStatuses();
			$actions 	= $qrep -> getActionStatus();
			$status 	= array();
			$aStatus    = array(); 
			foreach( $actionstatus as $index => $actionStatus)
			{
				$status[$actionStatus['id']] = $actionStatus['name'];				
			}
			foreach($actions as $i => $action)
			{
				$aStatus[$i] = array("status" => $status[$action['status']], "total" => $action['total']);				
			}
			echo json_encode( $aStatus );
		break;
		}
?>
