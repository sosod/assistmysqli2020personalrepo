<?php
/**
 * Convert a hexa decimal color code to its RGB equivalent
 *
 * @param string $hexStr (hexadecimal color value)
 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
 */                                                                                                 
function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
    $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
    $rgbArray = array();
    if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
        $colorVal = hexdec($hexStr);
        $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['blue'] = 0xFF & $colorVal;
    } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
        $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
        $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
        $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
    } else {
        return false; //Invalid hex color code
    }
    return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
} 
?><?php
//error_reporting(-1);
//require_once("../inc/inc_ignite.php");
?><html><head>
<link rel="stylesheet" href="../../assist.css" type="text/css" />
<link rel="stylesheet" href="../../styles/style_blue.css" type="text/css" />
<style type=text/css>
table {
	border-color: #ffffff;
}
table td {
	font-weight: normal;
	text-align: center;
	border-color: #ffffff;
}
.bottom { vertical-align: bottom; }
</style>
</head><body>
<?php
//error_reporting(-1);


/*require_once '../../library/dbconnect/dbconnect.php';
require_once '../class/defaults.php';
require_once '../class/naming.php';

require_once "../../library/class/assist_helper.php";
require_once "../../library/class/assist.php";
require_once "../../library/class/assist_dbconn.php";
require_once "../../library/class/assist_db.php";*/
include_once("../inc/init.php");

$helper = new ASSIST_MODULE_HELPER();
$modref = $helper->getModRef();
$modtext = $helper->getModTitle();
$dbref = $helper->getDBRef();
$cmpname = $helper->getCmpName();
$db = new ASSIST_DB();
$dbref = $db->getDBRef();

$defObj = new Defaults("","");
$risk_object_name = $defObj->getObjectName("OBJECT");
$action_object_name = $defObj->getObjectName("ACTION");

$nameObj = new Naming();
$ireName = $nameObj->getAName("inherent_risk_exposure");
$rrName = $nameObj->getAName("residual_risk_exposure");
$iName = $nameObj->getAName("impact");
$lName = $nameObj->getAName("likelihood");
$ceName = $nameObj->getAName("percieved_control_effective");









function drawStackedBar($values,$chart_title,$layout,$required,$graph_ref,$series) {
global $helper;
	if($required=="Y") {
		require("../../lib/amcharts2-php/AmBarChart.php");
		AmChart::$swfObjectPath = "../../lib/amcharts2/swfobject.js";
		AmChart::$libraryPath = "../../lib/amcharts2/amcolumn/";
		AmChart::$jsPath = "../../lib/amcharts2-php/AmCharts.js";
		AmChart::$jQueryPath = "../../lib/amcharts2/amcolumn/jquery.js";
		AmChart::$loadJQuery = true;
	}
	
	$cwidth = 500;
	$cheight = 350;
	$cdepth = 15;
	$cangle = 45;
	$ccolwidth = 80;
	$style = "font-size: 7pt; line-height: 9pt; padding-right: 10px;";
	$cmargin_top = 20;
	$cmargin_bottom = 40;		
	$cmargin_right = 20;
	$cmargin_left = 20;		

	$chart = new AmBarChart("bar_".$graph_ref);
	//$chart->setTitle("<p class=".$title_class." style=\"margin: 10 0 0 0;\">".$chart_title."</p>");
		
	foreach($series as $s) {
		$chart->addSerie($s['id'], chartEncode(ASSIST_HELPER::decode($s['value'])));
	}

	foreach($values as $v) { 
		$chart->addGraph($v['id'], $v['name'], $v['values'], array("color" => $v['color']));
	}

	$chart->setConfigAll(array(
		"width" => $cwidth,
		"height" => $cheight,
		"depth" => $cdepth,
		"angle" => $cangle,
		"font" => "Tahoma",
		"text_size" => 10,
		"column.type" => "100% stacked",
		"column.width" => $ccolwidth,
		"column.data_labels" => "<!".chartEncode("[CDATA[{percents}%]]").">",
		"column.data_labels_text_color" => "#ffffff",
		"decimals_separator" => ".",
		"plot_area.margins.top" => $cmargin_top,
		"plot_area.margins.right" => $cmargin_right,
		"plot_area.margins.left" => $cmargin_left,
		"plot_area.margins.bottom" => $cmargin_bottom,
		"background.border_alpha" => 0,
		"grid.category.alpha" => 0,
		"grid.value.alpha" => 0,
		"axes.category.alpha" => 0,
		"axes.value.alpha" => 0,
		"values.value.enabled" => "false",
		"legend.enabled" => "false",
		"balloon.enabled" => "true",
		"balloon.alpha" => 80,
		"balloon.show" => "<!".chartEncode("[CDATA[{title}: {value} of {total} ]]").">"
	));

	echo $helper->decode($chart->getCode());

}

function chartEncode($str) {

	$str = str_replace("&","&#38;",$str);
	$str = str_replace("\"","&#34;",$str);
	
	$str = str_replace("[","&#91;",$str);
	$str = str_replace("]","&#93;",$str);

	$str = str_replace("{","&#123;",$str);
	$str = str_replace("}","&#125;",$str);

	return $str;
}

/*** Title Page ***/
?>
<h1 style="text-align: center;"><?php echo $cmpname; ?></h1>
<h2 style="text-align: center;margin-top: -5px; margin-bottom: -5px;">Analysis of <?php echo $risk_object_name; ?> Ratings</h2>
<p style="text-align: center;"><i><small>Report generated on <?php echo date("d M Y H:i"); ?>.</small></i></p>
<?php
/*** Get Data ***/
$data = array();
$cols = 0;
//GET TOTAL RISKS COUNT
$sql = "SELECT count(r.id) as total FROM ".$dbref."_risk_register r WHERE r.active = 1";
//include("generate_db.php");
//$row = mysql_fetch_assoc($rs);
$row = $db->mysql_fetch_one($sql);
$total_risks = $row['total'];
//mysql_close($con);
//IMPACT
$impact_count = 0;
	$data['i'][0] = array(
		'id'			=> 0,
		'value'			=> "[Unspecified]",
		'color'			=> "ABABAB",
		'rating_from'	=> "0",
		'rating_to'		=> "0",
		'active'		=> 1,
		'rc'			=> 0,
	);
$sql = "SELECT i.id, i.assessment as value, i.color, count(r.id) as rc, i.active 
		FROM ".$dbref."_impact i 
		LEFT JOIN ".$dbref."_risk_register r 
		ON r.impact = i.id AND r.active = 1 
		GROUP BY i.assessment ORDER BY i.rating_from ASC";
//include("generate_db.php");
//$rs = $db->db_query($sql);
$rows = $db->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach ($rows as $row){
	if($row['active']==1 || $row['rc']>0) {
		$data['i'][] = $row;
		$impact_count+=$row['rc'];
	}
} 
//mysql_close($con);
if($impact_count<$total_risks) {
	$data['i'][0]['rc'] = ($total_risks-$impact_count);
}
$cols = count($data['i'])>$cols ? count($data['i']) : $cols;
//LIKELIHOOD
$likelihood = array();
$likelihood_count = 0;
	$data['l'][0] = array(
		'id'			=> 0,
		'value'			=> "[Unspecified]",
		'color'			=> "ABABAB",
		'rating_from'	=> "0",
		'rating_to'		=> "0",
		'active'		=> 1,
		'rc'			=> 0,
	);

$sql = "SELECT i.id, i.assessment as value, i.color, count(r.id) as rc, i.active 
		FROM ".$dbref."_likelihood i 
		LEFT JOIN ".$dbref."_risk_register r 
		ON r.likelihood = i.id AND r.active = 1 
		GROUP BY i.assessment ORDER BY i.rating_from ASC";
//include("generate_db.php");
$rows = $db->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach ($rows as $row){
	if($row['active']==1 || $row['rc']>0) {
		$data['l'][] = $row;
		$likelihood_count+=$row['rc'];
	}
} 
//mysql_close($con);
if($likelihood_count<$total_risks) {
	$data['l'][0]['rc'] = ($total_risks-$likelihood_count);
}
$cols = count($data['l'])>$cols ? count($data['l']) : $cols;
//Inherent risk exposure
	$data['e'][0] = array(
		'id'			=> 0,
		'value'			=> "[Unspecified]",
		'color'			=> "ABABAB",
		'rating_from'	=> "0",
		'rating_to'		=> "0",
		'active'		=> 1,
		'rc'			=> 0,
	);
$sql = "SELECT i.id, i.magnitude as value, i.color, i.rating_from as ifrom, i.rating_to as ito, i.active 
		FROM ".$dbref."_inherent_exposure i 
		WHERE active = 1
		ORDER BY i.rating_from ASC";
//include("generate_db.php");
$rows = $db->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach ($rows as $row){
	if($row['active']==1) {
		$row['rc'] = 0;
		if($row['ifrom']>0) {
			$row['ifrom']+=0.0000000001;
		}
		$data['e'][] = $row;
	}
} 
//mysql_close($con);
$inherent_count = 0;
foreach($data['e'] as $key => $d) {
	if($key>0) {
		$sql = "SELECT count(id) as rc 
				FROM ".$dbref."_risk_register 
				WHERE active = 1 
				AND inherent_risk_exposure >=".$d['ifrom']." 
				AND inherent_risk_exposure <= ".$d['ito'];
		//include("generate_db.php");
		//$rs = $db->db_query($sql);
		if($db->db_get_num_rows($sql)>0) {
			$row = $db->mysql_fetch_one($sql);
			$data['e'][$key]['rc'] += $row['rc'];
			$inherent_count+=$row['rc'];
		} else {
			$data['e'][$key]['rc'] += 0;
		}
	}
}
if($inherent_count<$total_risks) {
	$data['e'][0]['rc'] = ($total_risks-$inherent_count);
}
$cols = count($data['e'])>$cols ? count($data['e']) : $cols;
//Control Effectiveness
$control_count = 0;
	$data['c'][0] = array(
		'id'			=> 0,
		'value'			=> "[Unspecified]",
		'color'			=> "ABABAB",
		'rating_from'	=> "0",
		'rating_to'		=> "0",
		'active'		=> 1,
		'rc'			=> 0,
	);
$sql = "SELECT i.id, i.effectiveness as value, i.color, count(r.id) as rc, i.active 
		FROM ".$dbref."_control_effectiveness i 
		LEFT JOIN ".$dbref."_risk_register r 
		ON r.percieved_control_effectiveness = i.id 
		AND r.active = 1 
		GROUP BY i.effectiveness ORDER BY i.rating ASC";
//include("generate_db.php");
$rows = $db->mysql_fetch_all($sql);
//while($row = mysql_fetch_array($rs)) {
foreach ($rows as $row){
	if($row['active']==1 || $row['rc']>0) {
		$data['c'][] = $row;
		$control_count+=$row['rc'];
	}
} 
//mysql_close($con);
if($control_count<$total_risks) {
	$data['c'][0]['rc'] = ($total_risks-$control_count);
}
$cols = count($data['c'])>$cols ? count($data['c']) : $cols;
//residual risk exposure
	$data['r'][0] = array(
		'id'			=> 0,
		'value'			=> "[Unspecified]",
		'color'			=> "ABABAB",
		'rating_from'	=> "0",
		'rating_to'		=> "0",
		'active'		=> 1,
		'rc'			=> 0,
	);

$sql = "SELECT i.id, i.magnitude as value, i.color, i.rating_from as ifrom, i.rating_to as ito, i.active 
		FROM ".$dbref."_residual_exposure i 
		WHERE active = true 
		ORDER BY i.rating_from ASC";
//include("generate_db.php");
//$rs = $db->db_query($sql);
$rows = $db->mysql_fetch_all($sql);

$rre_test = array();
//while($row = mysql_fetch_array($rs)) {
foreach($rows as $row) {
	if($row['active'] ==1) {
		$row['rc'] = 0;
		$row['ito']+=0.999999999999;
		$data['r'][$row['id']] = $row;
		$rre_test[$row['ifrom']] = $row['id'];
	}
}
//mysql_close($con);

$residual_count = 0;
//foreach($data['r'] as $key => $d) {
	//if($key>0) {
		$sql = "SELECT residual_risk_exposure, count(residual_risk_exposure) as rc FROM ".$dbref."_risk_register WHERE active = 1 GROUP BY residual_risk_exposure";
		$rows = $db->mysql_fetch_all($sql);
		foreach($rows as $r) {
			$residual_count+=$r['rc'];
			$rre = $r['residual_risk_exposure']*1;
			$x = 0;
			$prev_id = 0;
			$found = false;
			foreach($rre_test as $test => $id) {
				if($rre<$test) {
					$x = $prev_id;
					$found = true;
					break;
				}
				$prev_id = $id;
			}
			if(!$found) {
				$z = array_keys($rre_test);
				$y = count($z)-1;
				$t = $z[$y];
				if($rre>$t) {
					$x = $prev_id;
				}
			}
			if(!isset($data['r'][$x]['rc'])) {
				$data['r'][$x]['rc'] = $r['rc'];
			} else {
				$data['r'][$x]['rc']+=$r['rc'];
			}
		}
		/*$sql = "SELECT count(id) as rc
				FROM ".$dbref."_risk_register 
				WHERE active = 1 
				AND CAST(residual_risk_exposure AS DECIMAL(5,4)) >=".($d['ifrom'])." 
				AND CAST(residual_risk_exposure AS DECIMAL(5,4)) <= ".($d['ito']);
		//include("generate_db.php");
		$rs = $db->db_query($sql);
		if(mysql_num_rows($rs)>0) {
			$row = mysql_fetch_array($rs);
			$data['r'][$key]['rc'] += $row['rc'];
			$residual_count+=$row['rc'];
		} else {
			$data['r'][$key]['rc'] += 0;
		}*/
	//}
//}
if($residual_count<$total_risks) {
	$data['r'][0]['rc']	= ($total_risks-$residual_count);
}
$cols = count($data['r'])>$cols ? count($data['r']) : $cols;



$graph_ref = "0";
$layout = 2;
$chart_title = "Graph Analysis";
$required = "Y";



	$series = array(
		'i' => array('id'=>"i",'value'=>$iName),
		'l' => array('id'=>"l",'value'=>$lName),
		'e' => array('id'=>"e",'value'=>$ireName),
		'c' => array('id'=>"c",'value'=>$ceName),
		'r' => array('id'=>"r",'value'=>$rrName)
	);
	$values = array();
	foreach($series as $s) {
		foreach($data[$s['id']] as $d) {
			$d['value'] = str_replace("'","",$d['value']);
			$d['value'] = str_replace('"','',$d['value']);
			$values[] = array('id'=>$s['id'].$d['id'], 'name'=>addslashes($d['value']), 'color'=>"#".$d['color'],
				'values'=>array($s['id']=>$d['rc'])
			);
		}
	}
echo "<div align=center>";

//echo "<hr /><h1 class='green'>VALUES ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($values);

drawStackedBar($values,$chart_title,$layout,$required,$graph_ref,$series);
/*** LEGEND ***/


//echo "<hr /><h1 class='green'>DATA ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($data['r']);
//echo "<hr /><h1 class='green'>SERIES ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($series);

$width = 80;
echo "<table style='margin-left: -100px'>";
foreach($series as $s) { 
$total = 0;
		echo chr(10)."<tr><td style=\"font-weight: bold; text-align: right; vertical-align: bottom; padding-bottom: 12px;\">".$s['value']."</td><td><table cellpadding=3>";
			echo chr(10)."<tr>";
			foreach($data[$s['id']] as $d) {
				echo "<td width=$width class=bottom>".$d['value']."</td>";
			}
			echo "<td width=$width ><b>Total</b></td>";
			echo "</tr>";
			echo chr(10)."<tr>";
			foreach($data[$s['id']] as $d) {
				$z = (hex2RGB("#".$d['color']));
				$x = array_sum($z);
				$color = "FFFFFF";
				if($x>350 && $z['blue']<200) { $color = "000000"; }
				echo "<td width=$width style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color'].";color:#".($color)."\">".$d['rc']."</div>";
				//arrPrint($z);
				echo "</td>";
				$total += $d['rc'];
			}
			echo "<td width=$width><div style=\"border: 1px solid #dddddd;\"><b>".$total."</b></div></td>";
			echo "</tr>";
		echo chr(10)."</table></td></tr>";
}
echo "</table>";



/*
$width = 80;
echo "<table cellpadding=3 style=\"border:1px solid #ababab;\">";
foreach($series as $s) { 
$total = 0;
	echo chr(10)."<tr><td style=\"text-align: right;\">";
	echo "<table cellpadding=3>";
		echo chr(10)."<tr><td style=\"font-weight: bold; text-align: right; vertical-align: bottom; padding-bottom: 6px;\">".$s['value']."</td><td><table cellpadding=3>";
			echo chr(10)."<tr>";
			foreach($data[$s['id']] as $d) {
				echo "<td width=$width >".$d['value']."</td>";
			}
			//echo "<td width=$width ><b>Total</b></td>";
			echo "</tr>";
			echo chr(10)."<tr>";
			foreach($data[$s['id']] as $d) {
				echo "<td width=$width style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color'].";\">".$d['rc']."</div></td>";
				$total += $d['rc'];
			}
			//echo "<td width=$width><div style=\"border: 1px solid #dddddd;\"><b>".$total."</b></div></td>";
			echo "</tr>";
		echo chr(10)."</table></td></tr>";
	echo "</table>";
	echo "</td></tr>";
}
echo "</table>";





/*
echo "<table cellpadding=3 style=\"border:1px solid #ababab;\">";
foreach($series as $s) { 
	echo "<tr><td>";
		echo "<table cellpadding=3 style=\"margin-top: 10px;\">";
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				foreach($data[$s['id']] as $d) {
					echo "<td>".$d['value']."</td>";
				}
			echo "</tr>";
			echo "<tr>";
				echo "<td style=\"text-align: right;\">".$s['value']."</td>";
				foreach($data[$s['id']] as $d) {
					echo "<td style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color']."; \">".$d['rc']."</div></td>";
				}
			echo "</tr>";
		echo "</table>";
	echo "</td></tr>";
}
echo "</table>";


*/

/*
echo "<P>&nbsp;</p>";
echo "<table cellpadding=3>";
$a = 0;
foreach($series as $s) {
	if($a > 0) {
		echo "<tr><td colspan=".($cols+1).">&nbsp;</td></tr>";
	}
	echo "<tr>";
		echo "<td>&nbsp;</td>";
		foreach($data[$s['id']] as $d) {
			echo "<td >".$d['value']."</td>";
		}
		if(count($data[$s['id']])<$cols) {
			echo "<td colspan=".($cols-count($data[$s['id']])).">&nbsp;</td>";
		}
	echo "</tr>";
	echo "<tr>";
		echo "<td  style=\"text-align: right;\">".$s['value']."</td>";
		foreach($data[$s['id']] as $d) {
			//echo "<td width=".$width."% style=\"font-weight: normal;\"><span style=\"background-color: #".$d['color']."; \">&nbsp;&nbsp;&nbsp;</span> ".$d['rc']."</td>";
			echo "<td style=\"font-weight: normal;\"><div style=\"background-color: #".$d['color']."; \">".$d['rc']."</div></td>";
		}
		if(count($data[$s['id']])<$cols) {
			echo "<td colspan=".($cols-count($data[$s['id']])).">&nbsp;</td>";
		}
	echo "</tr>";
	$a++;
}
echo "</table>";*/
echo "</div>";

?>
</body></html>