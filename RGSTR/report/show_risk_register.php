<?php 

$navigation = false;
$breadcrumbs = false;

$page = "";
$folder = "";
$page_title = "|OBJECTNAME| Register";
$page_title_src = "";


$scripts = array('menu.js');
$styles = array( 'colorpicker.css' );



require_once("../inc/header.php");	

echo "<h1 class=center>".$_SESSION['ia_cmp_name']."</h1><h2 class=center>".$risk_object_name." Register</h2>";

?>
<script type="text/javascript">
$(function(){
	//$("#risk_table").html("Loading risks  .... <img src='../images/loaderA32.gif' />");
	$.get("controller.php?action=getRiskRegister", function(  data ) {	  
		  if( $.isEmptyObject( data ) ){
			  $("#risk_table").html("No "+window.risk_object_name+" were found");			  
		  } else {
			  	var htr = $("<tr />");
				$.each( data.header , function( index, head){
					htr.append($("<th />",{html:head}));
				})
				$("#risk_table").append( htr );
				
				$.each( data.risk, function( index, risk){
					var tr = $("<tr />")
					$.each( risk , function( key , val){
						tr.append($("<td />",{html:val}))
					})
					$("#risk_table").append( tr );
				})
							  
		  }
	}, "json");
})
</script>
<table id="risk_table">

</table>