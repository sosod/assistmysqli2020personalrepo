<?php
require_once("../../library/class/assist_helper.php");
require_once("../../library/class/assist_dbconn.php");
require_once("../../library/class/assist_db.php");
require_once("../../library/class/assist.php");
$me = new ASSIST();
$assist_db = new ASSIST_DB();

$navigation = false;
$breadcrumbs = false;

$page = "";
$folder = "";
$page_title = "";
$page_title_src = "";

$scripts = array('menu.js',  );
$styles = array( 'colorpicker.css' );

require_once("../inc/header.php");

//$me->displayResult(array("error","Under Development"));
//error_reporting(-1);
$nameObj = new Naming();
$ccName = $nameObj->getAName("control_rating");
$rrName = $nameObj->getAName("residual_risk_exposure");

/* GRAPH STRUCTURE */
/************************
			(1/3;A)
	maxY=A	|     _/
		|   _/	
	line 1	| _/	 _/ (0;2/3A)
	(1;1/3A)|/     _/
		|    _/
	0	|_ _/_ _ _ _
		1    X     0
		  (2/3;0)
		  line 2

	Red = greater than 10% above line 1 OR less than 10% below line 2
	Green = less than 10% below line 1 AND greater than 10% above line 2
	Orange = within 10% above or below line 1 or line 2 (not red or green)
	
	Line 1: Y = mX + C = -AX + 1/3A
	Line 2: Y = mX + C = -AX - 1/3A

*****************************/


/* DETERMINE THE MAXIMUM POSSIBLE Y AXIS VALUE */
	//Get the maximum according to the client determined list of residual risks.
$sql = "SELECT MAX(rating_to) as maxrr FROM ".$db->getDBRef()."_residual_exposure WHERE active <> ".RESIDUALRISK::DELETED;
$row = $assist_db->mysql_fetch_one($sql);
$a = $row['maxrr'];
	//Check the actual residual risks calculated to make sure that there isn't a value higher than what is set in the residual risk list
$sql = "SELECT MAX(residual_risk_exposure*1) as maxrr FROM ".$db->getDBRef()."_risk_register WHERE active = 1";
$row = $assist_db->mysql_fetch_one($sql); //echo $sql;
$a = ($a > $row['maxrr']) ? $a : $row['maxrr'];
	//B = 10% of Max Y = the interval around each mx+c line for calculating the orange bullets
$b = $a*0.1;
$m = ($a-($a/3))/((1/3)-1);	//change y over change x
$c1 = ($a/3)-$m*(1);	//y = mx + c => c = y - mx => testing on point (1;1/3a) for line 1
$c2 = -1*$m*(2/3);	//y = mx + c => c = y - mx => testing on point (2/3;0) for line 2
/* GET THE LIST OF DISTINCT RESIDUAL RISK VALUES AND CONTROL RATINGS AND CALCULATE THEIR COLOR POSITION */
//$sql = "SELECT DISTINCT residual_risk_exposure as y, control_effectiveness_rating as x FROM ".$db->getDBRef()."_risk_register WHERE active = 1 ORDER BY control_effectiveness_rating DESC";
//$risks = $assist_db->mysql_fetch_all($sql);
//echo "<P>Distinct Risks: ".count($risks)."</p>";
$sql = "SELECT residual_risk_exposure as y, control_effectiveness_rating as x FROM ".$db->getDBRef()."_risk_register WHERE active = 1 ORDER BY control_effectiveness_rating DESC";
$risks = $assist_db->mysql_fetch_all($sql);

//DEV PURPOSES
/*echo "<P>Count of All Risks: ".count($risks)."</p>";
echo "<table>
<tr><td><b>Max Y = A:</b></td><td>$a</td></tr>
<tr><td><b>10% A = B:</b></td><td>$b</td></tr>
<tr><td><b>m = A:</b></td><td>$m</td></tr>
<tr><td><b>c1 = A + 1/3A:</b></td><td>$c1</td></tr>
<tr><td><b>c2 = 2/3A:</b></td><td>$c2</td></tr>
</table>
<p>Line 1 = mX + c +/- B = ".$m."X + $c1 +/- $b</p>
<p>Line 2 = mX - c +/- B = ".$m."X - $c2 +/- $b</p>
<table>
<tr><th>X</th><th>Y</th><th>Line1</th><th>Line2</th><th>Line1-B</th><th>Line2+B</th><th>&&=Green</th><th>Line1+B</th><th>Line2-B</th><th>||=Red</th><th>Else Orange</th><th>Final Color</th></tr>
";*/
$results = array(
	'under_red'=>0,
	'under_orange'=>0,
	'optimised_green'=>0,
	'over_orange'=>0,
	'over_red'=>0,
);
$graph_data = array();
foreach($risks as $key => $r) {
	$risks[$key]['color'] = "#fe9900";
	$x = $r['x'];
	$y = $r['y'];
	$e =false;
	//If the color hasn't been calculated before...
	if(!isset($graph_data[$x][$y])) { 
		$graph_data[$x][$y] = array('value'=>1,'color'=>"#FE9900",'result'=>""); 
//echo "<tr><td>$x</td><td>$y</td>";
//$line1 = ($m * $x + $c1);
//$line2 = ($m * $x + $c2);
//echo "<td>$line1</td><td>$line2</td>";
		//Calculate green area
		$line1g = ($m * $x + $c1 - $b);
		$line2g = ($m * $x + $c2 + $b);
//echo "<td>$line1</td><td>$line2</td><td>".($line1g>$y && $y>$line2g ? "<span style='background-color: #ccffcc;'>TRUE</span>" : "FALSE")."</td>";
		if($line1g>$y && $y>$line2g) {
			$risks[$key]['color'] = "#009900";
			$results['optimised_green']++;
			$graph_data[$x][$y]['result'] = "optimised_green";
			$e = true;
		} //else {
			//Calculate red areas
			$line1r = $m * $x + $c1 + $b;
			$line2r = $m * $x + $c2 - $b;
//echo "<td>$line1</td><td>$line2</td><td>".($y>$line1r || $line2r>$y ? "<span style='background-color: #ffcccc'>TRUE</span>" : "FALSE")."</td>";
			if($y>$line1r || $line2r>$y) {
				$risks[$key]['color'] = "#CC0001";
				$e = true;
				if($y>$line1r) {
					$results['under_red']++;
					$graph_data[$x][$y]['result'] = "under_red";
				} else {
					$results['over_red']++;
					$graph_data[$x][$y]['result'] = "over_red";
				}
			}
		//}
			if(!$e) {
				if($line1r > $y && $y > $line1g) {
					$results['under_orange']++;
					$graph_data[$x][$y]['result'] = "under_orange";
				} else {
					$results['over_orange']++;
					$graph_data[$x][$y]['result'] = "over_orange";
				}
			}
//echo "<td>".(!$e ? "<span style='background-color: #ffeccc'>TRUE</span>" : "FALSE")."</td><td style='background-color: ".$risks[$key]['color']."'>".$risks[$key]['color']."</td></tr>";
			$graph_data[$x][$y]['color']=$risks[$key]['color'];
	} else { 	//add another value
		$graph_data[$x][$y]['value']++;
		$results[$graph_data[$x][$y]['result']]++;
	}
}
//echo "</table>";


/*

*/


$title = $rrName." versus ".$ccName;
echo "<h1 class=center>".$_SESSION['ia_cmp_name']."</h1><h2 class=center>".$title."</h2>";

?>
 			<p class='center i'><small><?php echo "Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s"); ?></small></p>
<script src="/library/amcharts3/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts3/xy.js" type="text/javascript"></script>
<script type="text/javascript" src="/library/amcharts3/themes/light.js"></script>
		<script type="text/javascript">
		var $settings = {
					"type": "xy",
					"pathToImages": "/library/amcharts3/images/",
					"startDuration": 0.5,
					"theme": "light",
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "<?php echo $ccName; ?>: <b>[[x]]</b><br><?php echo $rrName; ?>: <b>[[y]]</b><br>Number of <?php echo $risk_object_name; ?>: <b>[[Count]]</b>",
							"bullet": "round",
							"bulletBorderThickness": 5,
							"bulletSize": 11,
							"colorField": "Color",
							"id": "AmGraph-1",
							"lineAlpha": 0,
							"lineColor": "#b0de09",
							"valueAxis": "Not set",
							"valueField": "value",
							"xField": "X",
							"yField": "Y"
						},
						{
							"balloonText": "",
							"dashLength": 3,
							"bullet": "none",
							"id": "AmGraph-2",
							"lineAlpha": 1,
							"lineColor": "#999999",
							"valueField": "value",
							"xField": "Line 1 - X",
							"yField": "Line 1-Y"
						},
						{
							"balloonText": "",
							"dashLength": 3,
							"bullet": "none",
							"id": "AmGraph-3",
							"lineAlpha": 1,
							"lineColor": "#999999",
							"valueField": "value",
							"xField": "Line 2 - X",
							"yField": "Line 2 - Y"
						}
					],
					"guides": [
						{
							"fillAlpha": 0.21,
							"fillColor": "#ff8000",
							"id": "Guide-1",
							"lineAlpha": 0,
							"toValue": -8,
							"value": -3,
							"valueAxis": "ValueAxis-2"
						}
					],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisAlpha": 0,
							"maximum"	: <?php echo $a; ?>,
							"title":"<?php echo $rrName; ?>",
							"titleFontSize":"10",
						},
						{
							"id": "ValueAxis-2",
							"position": "bottom",
							"axisAlpha": 0,
							"maximum" : 1,
							"title":"<?php echo $ccName; ?>",
							"reversed":true,
							"titleFontSize":"10",
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [{"text":"Distribution"}],
					"dataProvider": [<?php 
					$c = 0;
					foreach($graph_data as $x=>$z) {
						foreach($z as $y => $r) {
							echo ($c>0?',':'').'{
								"X":"'.$x.'",
								"Y":"'.$y.'",
								"Value":"1",
								"Count":"'.$r['value'].'",
								"Color":"'.$r['color'].'",';
							if($c==0) {
								echo '
								"Line 1 - X": "1",
								"Line 1-Y": "'.($a/3).'",
								"Line 2 - X": "'.(2/3).'",
								"Line 2 - Y": "0"';
							} elseif($c==1) {
								echo '
								"Line 1 - X": "'.(1/3).'",
								"Line 1-Y": "'.($a).'",
								"Line 2 - X": "0",
								"Line 2 - Y": "'.($a/3*2).'"';
							}
							echo '
							}';
							$c++;
						}
					}
					?>]
				};
			AmCharts.makeChart("chartdiv",$settings);
		var $settings2 = {
					"type": "xy",
					"pathToImages": "/library/amcharts3/images/",
					"startDuration": 0.5,
					"theme": "light",
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "<?php echo $ccName; ?>: <b>[[x]]</b><br><?php echo $rrName; ?>: <b>[[y]]</b><br>Number of <?php echo $risk_object_name; ?>: <b>[[Count]]</b>",
							"bullet": "round",
							"bulletBorderThickness": 5,
							"bulletSize": 11,
							"colorField": "Color",
							"id": "AmGraph-1",
							"lineAlpha": 0,
							"lineColor": "#b0de09",
							"valueAxis": "Not set",
							"valueField": "Count",
							"xField": "X",
							"yField": "Y"
						},
						{
							"balloonText": "",
							"dashLength": 3,
							"bullet": "none",
							"id": "AmGraph-2",
							"lineAlpha": 1,
							"lineColor": "#999999",
							"valueField": "Count",
							"xField": "Line 1 - X",
							"yField": "Line 1-Y"
						},
						{
							"balloonText": "",
							"dashLength": 3,
							"bullet": "none",
							"id": "AmGraph-3",
							"lineAlpha": 1,
							"lineColor": "#999999",
							"valueField": "Count",
							"xField": "Line 2 - X",
							"yField": "Line 2 - Y"
						}
					],
					"guides": [
						{
							"fillAlpha": 0.21,
							"fillColor": "#ff8000",
							"id": "Guide-1",
							"lineAlpha": 0,
							"toValue": -8,
							"value": -3,
							"valueAxis": "ValueAxis-2"
						}
					],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisAlpha": 0,
							"maximum"	: <?php echo $a; ?>,
							"title":"<?php echo $rrName; ?>",
							"titleFontSize":"10",
						},
						{
							"id": "ValueAxis-2",
							"position": "bottom",
							"axisAlpha": 0,
							"maximum" : 1,
							"title":"<?php echo $ccName; ?>",
							"reversed":true,
							"titleFontSize":"10",
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [{"text":"Volume"}],
					"dataProvider": [<?php 
					$c = 0;
					foreach($graph_data as $x=>$z) {
						foreach($z as $y => $r) {
							echo ($c>0?',':'').'{
								"X":"'.$x.'",
								"Y":"'.$y.'",
								"Value":"1",
								"Count":"'.$r['value'].'",
								"Color":"'.$r['color'].'",';
							if($c==0) {
								echo '
								"Line 1 - X": "1",
								"Line 1-Y": "'.($a/3).'",
								"Line 2 - X": "'.(2/3).'",
								"Line 2 - Y": "0"';
							} elseif($c==1) {
								echo '
								"Line 1 - X": "'.(1/3).'",
								"Line 1-Y": "'.($a).'",
								"Line 2 - X": "0",
								"Line 2 - Y": "'.($a/3*2).'"';
							}
							echo '
							}';
							$c++;
						}
					}
					?>]
				};
			AmCharts.makeChart("chartdiv2",$settings2);			
		</script>
<table width="100%" class="noborder">
  <tr>
    <td colspan="2" class="noborder">
    	<center><div id="chartdiv" style="width:750px; height: 550px; background-color: #FFFFFF;"></div></center>
    </td>
  </tr>
  <tr>
  	<td colspan="2" class="noborder center">
		<table style='margin: 0 auto;' class=noborder id=tbl_legend>
			<tr>
				<td >Under-controlled:</td>
				<td class='red b center'><?php echo $results['under_red']; ?></td>
			</tr>
			<tr>
				<td >Almost Under-controlled:</td>
				<td class='orange b center'><?php echo $results['under_orange']; ?></td>
			</tr>
			<tr>
				<td >Optimised:</td>
				<td class='green b center'><?php echo $results['optimised_green']; ?></td>
			</tr>
			<tr>
				<td >Almost Over-controlled:</td>
				<td class='orange b center'><?php echo $results['over_orange']; ?></td>
			</tr>
			<tr>
				<td >Over-controlled:</td>
				<td class='red b center'><?php echo $results['over_red']; ?></td>
			</tr>
			<tr>
				<td colspan=2 id=td_leg><div style='height: 2px; background-color: #000099;width:100%'></div></td>
			</tr>
			<tr>
				<td class=b>Total <?php echo $risk_object_name; ?>:</td>
				<td class='b center'><?php echo array_sum($results); ?></td>
			</tr>
		</table>
  	</td>
  </tr>  
  <tr style='page-break-before: always;'>
    <td colspan="2" class="noborder">
    	<center><div id="chartdiv2" style="width:750px; height: 550px; background-color: #FFFFFF;"></div></center>
    </td>
  </tr>
  <tr class=screen_hidden>
  	<td colspan="2" class="noborder center">
		<table style='margin: 0 auto;' class=noborder id=tbl_legend>
			<tr>
				<td >Under-controlled:</td>
				<td class='red b center'><?php echo $results['under_red']; ?></td>
			</tr>
			<tr>
				<td >Almost Under-controlled:</td>
				<td class='orange b center'><?php echo $results['under_orange']; ?></td>
			</tr>
			<tr>
				<td >Optimised:</td>
				<td class='green b center'><?php echo $results['optimised_green']; ?></td>
			</tr>
			<tr>
				<td >Almost Over-controlled:</td>
				<td class='orange b center'><?php echo $results['over_orange']; ?></td>
			</tr>
			<tr>
				<td >Over-controlled:</td>
				<td class='red b center'><?php echo $results['over_red']; ?></td>
			</tr>
			<tr>
				<td colspan=2 id=td_leg><div style='height: 2px; background-color: #000099;width:100%'></div></td>
			</tr>
			<tr>
				<td class=b>Total <?php echo $risk_object_name; ?>:</td>
				<td class='b center'><?php echo array_sum($results); ?></td>
			</tr>
		</table>
  	</td>
  </tr>  </table>
<style type=text/css>
#tbl_legend td { border: 0px; padding-left: 10px; padding-right: 3px; }
#tbl_legend td.center { padding-left: 3px; padding-right: 10px; }
#tbl_legend #td_leg { padding: 0px; }
.screen_hidden {
	display: none;
}
@media print {
	.screen_hidden { display: block; }
	.print_hide { display: none; }
}
</style>
<span class=print_hide>
<?php $me->displayGoBack("fixed.php"); ?>
</span>