<?php

$navigation = false;
$breadcrumbs = false;

$page = "";
$folder = "";
$page_title = "";
$page_title_src = "";

$scripts = array('menu.js',  );
$styles = array( 'colorpicker.css' );

require_once("../inc/header.php");

$nameObj = new Naming();
$ireName = $nameObj->getAName("inherent_risk_exposure");
$rrName = $nameObj->getAName("residual_risk_exposure");
$rtName = $nameObj->getAName("risk_type");
$rsName = $nameObj->getAName("risk_status");
$asName = $nameObj->getAName("action_status");
$rcName = $nameObj->getAName("risk_category");
$roName = $nameObj->getAName("risk_owner");

$title = $risk_object_name." per ".$rsName;
echo "<h1 class=center>".$_SESSION['ia_cmp_name']."</h1><h2 class=center>".$title."</h2>";



?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<script>
	$(function(){
		$.post("controller.php?action=riskVSstatus", function( response ){
	        var chart;
			var legend;

	        var chartData = [{country:"Czech Republic",litres:156.90},
					{country:"Ireland",litres:131.10},
					{country:"Germany",litres:115.80},
					{country:"Australia",litres:109.90},
					{country:"Austria",litres:108.30},
					{country:"UK",litres:99.00},
					{country:"Belgium",litres:93.00}];

	            chart = new AmCharts.AmPieChart();
	            chart.dataProvider = response;
	            chart.titleField = "status";
				chart.valueField = "total";
				chart.radius  	 = "35%";
				chart.colors     = ["#009900", "#fe9900", "#cc0001"];
				
				legend = new AmCharts.AmLegend();
				legend.align = "center";
				legend.markerType = "circle";
				chart.addLegend(legend);

				chart.write("chartdiv");			
		},"json");	
	});
</script>
<table width="100%" class="noborder">
  <tr>
    <td colspan="2" class="noborder">
    	<center><div id="chartdiv" style="width:auto%; height: 400px;"></div></center>
    </td>
  </tr>
  <tr>
  	<td colspan="2" class="noborder">
  		<center>
 			<?php echo "<font color='black'>Report generated on ".date("d")." ".date('F')." ".date('Y')."   ".date("H:i:s")." </font>"; ?>
 		</center>
  	</td>
  </tr>  
  <tr>
    <td class="noborder"><?php $me->displayGoBack("",""); ?></td>
    <td class="noborder"></td>
  </tr>
</table>

