<?php
require_once("../inc/header.php");
echo "<link rel='stylesheet' href='/assist.css' type='text/css' />";
//require_once( "../../library/dbconnect/dbconnect.php" );
include_once("../class/report.php");
include_once("../class/naming.php");
include_once("../class/riskcolumns.php");
include_once("../class/riskaction.php");
include_once("../class/residualrisk.php");
include_once("../class/inherentrisk.php");
include_once("../class/display.php");
include_once("../class/htmldisplay.php");	
include_once("../class/excelldisplay.php");
		
$rep 		= new Report();
$postArr    = $rep -> getQuickData( $_GET['id'] );
//Sondelani Dumalisile - 24 May 20201
//Picked up during standardisation
// recalculate string $x before unserializing using preg_replace_callback() to make sure no offset error occur.
//https://stackoverflow.com/questions/10152904/how-to-repair-a-serialized-string-which-has-been-corrupted-by-an-incorrect-byte
$postArr = preg_replace_callback('!s:\d+:"(.*?)";!s', function($m) { $m2 = array_key_exists(2,$m) ?$m[2] :""; return "s:" . strlen($m2) . ':"'.$m2.'";'; }, $postArr);
$postArr    = unserialize( $postArr );
$report 	= $rep -> generateReport( $postArr );
$headers 	= $rep -> getHeaders();
$additional = array();
$additional = array("title" => $postArr['report_title'], "company" => $_SESSION['cc'], "headers" => $headers );
$rep -> display( new HtmlDisplay(), $report, $additional );


?>
