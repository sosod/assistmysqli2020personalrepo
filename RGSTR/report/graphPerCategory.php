<?php
$navigation = false;
$breadcrumbs = false;

$page = "";
$folder = "";
$page_title = "";
$page_title_src = "";

$scripts = array( 'risk_per_category.js', 'jquery.ui.risk.js');
$styles = array( 'colorpicker.css' );

require_once("../inc/header.php");

$nameObj = new Naming();
$ireName = $nameObj->getAName("inherent_risk_exposure");
$rrName = $nameObj->getAName("residual_risk_exposure");
$rtName = $nameObj->getAName("risk_type");
$rsName = $nameObj->getAName("risk_status");
$asName = $nameObj->getAName("action_status");
$rcName = $nameObj->getAName("risk_category");

$title = $risk_object_name." Exposure per ".$rcName;
echo "<h1 class=center>".$_SESSION['ia_cmp_name']."</h1><h2 class=center>".$title."</h2>";
?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>       
<table align="center" width="100%" class="noborder">
   <tr>
    <td class="noborder" align="center" valign="top">
    	<center><div id="chartdiv" style="width: auto%; height: 300px; background-color:#FFFFFF"></div></center>
   </td>
  </tr>
  <tr>
  	<td class="noborder">
		<table class="noborder">
			<tr>
				<td class="noborder">
					<script language="javascript">
						$(function(){
							//$("#risk").risk({ section:"new", headers:['risk_item', 'risk_category', 'impact_rating', 'likelihood_rating', 'inherent_risk_rating','control_rating', 'residual_risk']});	
						});
					</script>
					<div id="risk"></div>				
				</td>
				<td class="noborder"> 
					<span style="background-color:#FF6600">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;<?php echo $ireName; ?><br />
					<span style="background-color:#D2CB00">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;<?php echo $rrName; ?>
				</td>
			</tr>
		</table>
  	</td>
  </tr>  
  <tr>
  	<td class="noborder">
		<center>
			<p>
				<?php echo "Report genereated on ".date("d")." ".date("F")." ".date("Y")." at ".date("H:m"); ?>
			</p>	
		</center>      	
  	</td>
  </tr>
</table>