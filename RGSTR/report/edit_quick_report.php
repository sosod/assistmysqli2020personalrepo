<?php
$scripts = array( 'edit_quick.js' );
$styles = array( 'colorpicker.css' );
require_once("../inc/header.php");
$rep = new Report();
if(isset($_POST['update_quick_report']))
{		
	/*require_once( "../../library/dbconnect/dbconnect.php" );
	include_once("../class/report.php");*/ //Declare in the header on init.php
	$rep -> editQuickReport( $_POST , $_POST['reportid']); 
}
$naming 	= new Naming();
$headers    = $naming -> getHeaderList();
$reportData = $rep -> getQuickData( $_GET['id'] );
$quickreport = unserialize($reportData);
//risk type
$type 		 = new RiskType( "", "", "", "" );
$types		 = $type-> getActiveRiskType();	
//get risk levels
$lev 		 = new RiskLevel( "", "", "", "" );
$levels		 = $lev -> getLevel();
//risk categories	
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
//risk impacts
$imp 		 = new Impact( "", "", "", "", "");
$impacts 	 = $imp -> getImpact() ;
//financial exposures
$fin 		 = new FinancialExposure( "", "", "", "", "");
$financials 	 = $fin -> getFinancialExposure() ;
//likelihood
$like 		 = new Likelihood( "", "", "", "", "","");
$likelihoods = $like -> getLikelihood() ;
//control effectiveness
$ihr		 = new ControlEffectiveness( "", "", "", "", "");
$controls    = $ihr -> getControlEffectiveness();
//user access settings
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUsers() ;
//risk statuses
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
print "<pre>";
	//print_r($quickreport);
	//print_r($statuses);
print "</pre>";
?>
<script>
$(function(){
	$("table#query_field_tobeincluded").find("th").css({"text-align":"left", "vertical-align":"top"});
	$("table#query_field_tobeincluded td").css({"border-color":"#FFFFFF"});
	$("table#query_field_tobeincluded td").css({"border-width":"0px"});	
	$("table#query_field_tobeincluded td").css({"border-style":"none"});
	$("table#query_field_tobeincluded td").css({"vertical-align":"top"});	
});
</script>
<form action="" name="report-header-form" id="report-header-form" method="post">
<table id="query_field_tobeincluded" class="noborder" width="60%">
			<tr>
				<th colspan="3" align="left">1. Select the Risk information to be displayed on the report</th>
			</tr>
			<tr>
				<td class="noborder" colspan="3"></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="header[id]" value="1" id="id" class="queryf" /><?php echo $headers['risk_item']; ?></td>
				<td><input type="checkbox" name="header[type]" value="1" id="type" class="queryf" /><?php echo $headers['risk_type']; ?></td>
				<td><input type="checkbox" name="header[category]" value="1" id="category" class="queryf" /><?php echo $headers['risk_category']; ?></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="header[description]" value="1" id="description" class="queryf" /><?php echo $headers['risk_description']; ?></td>
				<td><input type="checkbox" name="header[background]" value="1" id="background" class="queryf" /><?php echo $headers['risk_background']; ?></td>
				<td><input type="checkbox" name="header[risk_owner]" value="1" id="risk_owner" class="queryf" /><?php echo $headers['risk_owner']; ?></td>
			</tr>
			<tr>				
				<td><input type="checkbox" name="header[impact]" value="1" id="impact" class="queryf" /><?php echo $headers['impact']; ?></td>
				<td><input type="checkbox" name="header[impact_rating]" id="impact_rating" value="1" class="queryf" /><?php echo $headers['impact_rating']; ?></td>
				<td><input type="checkbox" name="header[current_controls]" value="1" id="current_controls" class="queryf" /><?php echo $headers['current_controls']; ?></td>											
			</tr>
			<tr>
				<td><input type="checkbox" name="header[likelihood]" id="likelihood" value="1" class="queryf" /><?php echo $headers['likelihood']; ?></td>
				<td><input type="checkbox" name="header[likelihood_rating]" id="likelihood_rating" value="1" class="queryf" /><?php echo $headers['likelihood_rating']; ?></td>
				<td><input type="checkbox" name="header[level]" value="1" id="level" class="queryf" /><?php echo $headers['risk_level']; ?></td>								
			</tr>
			<tr>
				<td><input type="checkbox" name="header[percieved_control_effectiveness]" value="1" id="percieved_control_effectiveness" class="queryf" /><?php echo $headers['percieved_control_effective']; ?></td>
				<td><input type="checkbox" name="header[control_effectiveness_rating]" value="1" id="control_effectiveness_rating" class="queryf" /><?php echo $headers['control_rating']; ?></td>
				<td><input type="checkbox" name="header[kpi_ref]" value="1" id="kpi_ref" class="queryf" /><?php echo $headers['kpi_ref']; ?></td>			
			</tr>
			<tr>
				<td><input type="checkbox" name="header[financial_exposure]" value="1" id="financial_exposure" class="queryf" /><?php echo $headers['financial_exposure']; ?></td>
				<td><input type="checkbox" name="header[actual_financial_exposure]" value="1" id="actual_financial_exposure" class="queryf" /><?php echo $headers['actual_financial_exposure']; ?></td>			
				<td><input type="checkbox" name="header[status]" value="1" id="status" class="queryf" /><?php echo $headers['risk_status']; ?></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="header[residual_risk_exposure]" value="1" id="residual_risk_exposure" class="queryf" /><?php echo $headers['residual_risk_exposure']; ?></td>
				<td><input type="checkbox" name="header[inherent_risk_exposure]" value="1" id="inherent_risk_exposure" class="queryf" /><?php echo $headers['inherent_risk_exposure']; ?></td>			
				<td><input type="checkbox" name="header[action_progress]" value="1" id="action_progres" class="queryf" /><?php echo $headers['action_progress']; ?></td>
			</tr>						
			<tr>
				<td colspan="3">
					<input type="button" value="Check All" id="r_checkAll" name="r_checkAll" />
					<input type="button" value="UnCheck All" id="r_uncheckAll" name="r_uncheckAll" />
					<input type="button" value="Invert" id="r_invert" name="r_invert" />
				</td>
			</tr>
			<tr>
				<td class="noborder" colspan="3"></td>
			</tr>		
			<tr>
				<th colspan="3" align="left">2.Select the Action Information to be displayed on the report</th>
			</tr>
			<tr>
				<td class="noborder" colspan="3"></td>
			</tr>				
			<tr>
				<td><input type="checkbox" name="aheader[action_item]" value="1" id="action_item" class="actionf"><?php echo $headers['ref']; ?></td>
				<td><input type="checkbox" name="aheader[deliverable]" value="1" id="deliverable" class="actionf" ><?php echo $headers['deliverable']; ?></td>
				<td><input type="checkbox" name="aheader[action_owner]" value="1" id="action_owner" class="actionf"><?php echo $headers['action_owner']; ?></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="aheader[risk_action]" value="1" id="risk_action" class="actionf" /><?php echo $headers['risk_action']; ?></td>
				<td><input type="checkbox" name="aheader[action_status]" id="action_status" value="1" class="actionf" /><?php echo $headers['action_status']; ?></td>
				<td><input type="checkbox" name="aheader[progress]" value="1" id="progress" class="actionf" /><?php echo $headers['progress']; ?></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="aheader[deadline]" value="1" id="deadline" class="actionf" /><?php echo $headers['deadline']; ?></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
			<td colspan="3">
				<input type="button" value="Check All" id="a_checkAll" name="a_checkAll" />
				<input type="button" value="UnCheck All" id="a_uncheckAll" name="a_uncheckAll" />
				<input type="button" value="Invert" id="a_invert" name="a_invert" />
			</td>
			</tr>				
			<tr>
				<td class="noborder" colspan="3"></td>
			</tr>
			<tr>
				<th colspan="3">3. Select the filters you wish to apply</th>
			</tr>
			<tr>
				<td class="noborder" colspan="3"></td>
			</tr>	
	       <tr>
	        <th><?php echo $headers['risk_type']; ?>:</th>
	        <td>
	            <select id="_type" name="values[type][]" multiple="multiple">
	                <option value="all">ALL</option>
	                <?php 
	                foreach($types as $index => $type){
	                ?>
	                   <option value="<?php echo $type['id']; ?>"
	                   <?php 
	                   	if(isset($quickreport['values']['types'])){
	                   		if(in_array($type['id'], $quickreport['values']['type'])){
	                	?>
	                		selected="selected"
	                	<?php 	
	                	  }
	                	}
	                   ?>
	                   ><?php echo $type['name']; ?></option>
	                <?php 
	                }
	                ?>
	            </select>
	        </td>
	          <td></td>
	      </tr>
	      <tr>
	        <th><?php echo $headers['risk_level']; ?>:</th>
	        <td>
	            <select id="_level" name="values[level][]" multiple="multiple">
	                <option value="all">ALL</option>
	                <?php 
	                  foreach( $levels as $index => $level)
	                  {
	                ?>
	                   <option
	                   <?php 
	                   if(isset($quickreport['values']['level'])){
	                   	if(in_array($level['id'], $quickreport['values']['level'])){
	                   ?>
	                   		selected="selected"
	                   <?php
	                   	} 
	                   }
	                   ?>
	                   value="<?php echo $level['id']; ?>"><?php echo $level['name']; ?></option>
	                <?php 
	                  }
	                ?>
	            </select>
	        </td>
	          <td></td>
	      </tr>
	      <tr>
	        <th><?php echo $headers['risk_category']; ?>:</th>
	        <td>
	            <select id="_category" name="values[category][]" multiple="multiple">
	              <option value="all">ALL</option>
	              <?php 
	              foreach( $categories as $index => $category)
	              {
	              	?>
	              	<option 
	              	<?php 
	              	if(isset($quickreport['values']['category']))
	              	{
	              		if(in_array($category['id'], $quickreport['values']['category'])){
	              ?>
	    				selected="selected"          	
	              <?php 	
	              		}	
	              	}
	              	?>
		              	value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
	              	<?php 
	              }
	              ?>
	            </select>
	        </td>
	          <td></td>
	      </tr>
	      <tr>
	        <th><?php echo $headers['risk_description']; ?>:</th>
	        <td><textarea name="values[description]" id="_description"></textarea></td>
			<td>
				<select name="match[description]" id="match_description" class="matches">
					<option value="any">Match any word</option>
					<option value="all">Match all words</option>
					<option value="excat">Match excat phrase</option>
				</select>
			</td>	
	      </tr>
	      <tr>
	        <th><?php echo $headers['risk_background']; ?>:</th>
	        <td><textarea name="values[background]" id="_background"></textarea></td>
			<td>
				<select name="match[background]" id="match_background" class="matches">
					<option value="any">Match any word</option>
					<option value="all">Match all words</option>
					<option value="excat">Match excat phrase</option>
				</select>
			</td>	          
	      </tr>
	      <tr>
	        <th><?php echo $headers['impact']; ?>:</th>
	        <td>
	        <select id="_impact" name="values[impact][]" multiple="multiple">
	        	<?php 
        		foreach( $impacts as $index => $impact)
        		{
        		?>
        			<option 
        			<?php 
        			if(isset($quickreport['values']['impact'])){
        				if(in_array($impact['id'], $quickreport['values']['impact'])){
        				?>
        					selected="selected"
						<?php
        				}
        			}
        			?>
        			value="<?php echo $impact['id']; ?>"><?php echo $impact['assessment']; ?></option>
        		<?php 
        		}
	        	?>
	         </select>
	        </td>
	        <td></td>
	      </tr>
	      <tr>
	        <th><?php echo $headers['likelihood']; ?>:</th>
	        <td>
	           <select id="_likelihood" name="values[likelihood][]" multiple="multiple">
	        	<?php 
	        	  foreach( $likelihoods as $index => $likelihood)
	        	  {
	        	  ?>
	        	  	<option
	        	  	<?php 
	        	  	  if(isset($quickreport['values']['likehood']))
	        	  	  {
	        	  	  	if(in_array($likelihood['id'], $quickreport['values']['likehood']))
	        	  	  	{
	        	  	  	?>
	        	  	  		selected="selected"
	        	  	  	<?php 
	        	  	  	}
	        	  	  }
	        	  	?> 
	        	  	value="<?php echo $likelihood['id']; ?>"><?php echo $likelihood['assessment']; ?></option>
	        	  <?php 	        	  	
	        	  }   
	        	?>
	          </select>
	        </td>
	        <td></td>
	      </tr>
      <!--<tr>
        <th><?php echo $headers['likelihood_rating']; ?>:</th>
        <td>
        <select id="_likelihood_rating" name="values[likelihood_rating]" multiple="multiple">
                <option value="">ALL</option>
         </select>
        </td>
          <td></td>
      </tr>-->
      <tr>
        <th><?php echo $headers['current_controls']; ?>:</th>
        <td>
            <input type="text" name="values[current_controls]" id="_current_controls" class="controls"  />
        </td>
          		<td>
			<select name="match[current_controls]" id="match_currentcontrols" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>	
      </tr>
      <tr>
        <th><?php echo $headers['percieved_control_effective']; ?>:</th>
        <td>
        <select id="_percieved_control" name="values[percieved_control_effectiveness][]" multiple="multiple">
           <?php 
          	foreach( $controls as $index => $control){
           ?>
           	  <option 
           	  <?php 
           	    if(isset($quickreport['values']['percieved_control_effectiveness']))
           	    {
           	    	if(in_array($control['id'], $quickreport['values']['percieved_control_effectiveness']))
           	    	{
           	    ?>
           	    	selected="selected"
           	    <?php 	
           	    	}
           	    }
           	  ?>
           	  value="<?php echo $control['id']; ?>"><?php echo $control['effectiveness']?></option>
           <?php 
          	}
           ?>
         </select>
        </td>
         <td></td>
      </tr>
     <!-- <tr>
        <th><?php echo $headers['control_rating']; ?>:</th>
        <td>
        <select id="_control_effectiveness" name="values[control_effectiveness]" multiple="multiple">
                <option value="">ALL</option>
         </select>
        </td>
          <td></td>
      </tr> -->
      <tr>
        <th><?php echo $headers['financial_exposure']; ?>:</th>
        <td>
        <select id="_financial_exposure" name="values[financial_exposure][]" multiple="multiple">
           <?php 
           	foreach($financials as $index => $financial)
           	{
           	?>
           	  <option 
           	  <?php 
           	  	if(isset($quickreport['values']['financial_exposure']))
           	  	{
           	  	  if(in_array($financial['id'], $quickreport['values']['financial_exposure'])){
           	  ?>
           	  	selected="selected"
           	  <?php            	  			
           	  		}	
           	  	}
           	  ?>
           	  value="<?php echo $financial['id']; ?>"><?php echo $financial['name']; ?></option>
           	<?php            		
           	}
           ?>
         </select>
        </td>
          <td></td>
      </tr>
      <tr>
        <th><?php echo $headers['actual_financial_exposure']; ?>:</th>
        <td>
        	<input id="_actual_financial_exposure" name="values[actual_financial_exposure]" type="text" />
        </td>
          <td>
			<select name="match[actual_financial_exposure]" id="match_actual_financial_exposure" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
      </tr>
      <tr>
        <th><?php echo $headers['kpi_ref']; ?>:</th>
        <td>
        	<input id="_kpi_ref" name="values[kpi_ref]" type="text" />
        </td>
          		<td>
			<select name="match[kpi_ref]" id="match_kpi_ref" class="matches">
				<option value="any">Match any word</option>
				<option value="all">Match all words</option>
				<option value="excat">Match excat phrase</option>
			</select>
		</td>
      </tr>
     <tr>
        <th><?php echo $headers['risk_status']; ?>:</th>
        <td>
        <select id="_risk_status" name="values[risk_status][]" class="" multiple="multiple">
           <?php
           foreach( $statuses as $index => $status){
            ?> 
           	<option
           	<?php 
           	 if(isset($quickreport['values']['risk_status']))
           	 {
         		if(in_array($status['id'], $quickreport['values']['risk_status']))
         		{
         	?>
         		selected="selected"
         	<?php		         			
         		}  		           		
           	 }
           	?>
           	 value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
           	<?php 
           }
           ?>
         </select>
        </td>
         <td></td>
      </tr>
    <tr>
        <th>Risk Creation Date</th>
        <td>From:<input type="text" name="values[insertdate][from]" id="from" class="datepicker"></td>
        <td>To:<input type="text" name="values[insertdate][to]" id="to" class="datepicker"></td>
    </tr>
	<tr>
		<td class="noborder" colspan="3"></td>
	</tr>    
	<tr>
		<th colspan="3">4.Choose your group and sort options</th>
	</tr>
	<tr>
		<td class="noborder" colspan="3"></td>
	</tr>	
	<tr>
		<th>Group By:</th>
		<td colspan="2">
			<select name="group_by" id="group_by">
				<option value="">No grouping</option>
				<option value="type_"><?php echo $headers['risk_type']; ?></option>
				<option value="category_"><?php echo $headers['risk_category']; ?></option>
				<option value="financial_exposure_"><?php echo $headers['financial_exposure']; ?></option>
				<option value="level_"><?php echo $headers['risk_level']; ?></option>
				<option value="impact_"><?php echo $headers['impact']; ?></option>
				<option value="likelihood_"><?php echo $headers['likelihood']; ?></option>
				<option value="percieved_control_effectiveness_"><?php echo $headers['percieved_control_effective']; ?></option>
				<option value="status_"><?php echo $headers['risk_status']; ?></option>
			</select>
	  </td>
  </tr>
	<tr>
		<th>Sort by:</th>
		<td colspan="2">
			<ul id="sortable" style="list-style:none;">
				<!-- <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort"  id="__id"></span><input type=hidden name=sort[] value="__id"><?php echo $headers['risk_item']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__type"></span><input type=hidden name=sort[] value="__type"><?php echo $headers['risk_type']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__category"></span><input type=hidden name=sort[] value="__category"><?php echo $headers['risk_category']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__description"></span><input type=hidden name=sort[] value="__description"><?php echo $headers['risk_description']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__background"></span><input type=hidden name=sort[] value="__background"><?php echo $headers['risk_background']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__financial_exposure"></span><input type=hidden name=sort[] value="__financial_exposure"><?php echo $headers['financial_exposure']; ?></li>
                <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__impact"></span><input type=hidden name=sort[] value="__impact"><?php echo $headers['impact']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__level"></span><input type=hidden name=sort[] value="__level"><?php echo $headers['risk_level']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__likelihood"></span><input type=hidden name=sort[] value="__likelihood"><?php echo $headers['likelihood']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__actual_financial_exposure"></span><input type=hidden name=sort[] value="__actual_financial_exposure"><?php echo $headers['actual_financial_exposure']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__kpi_ref"></span><input type=hidden name=sort[] value="__kpi_ref"><?php echo $headers['kpi_ref']; ?></li>
				<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__current_controls"></span><input type=hidden name=sort[] value="__current_controls"><?php echo $headers['current_controls']; ?></li>
				 -->
			</ul>
		</td>
	</tr>
	<tr>
		<td class="noborder" colspan="3"></td>
	</tr>
	<tr>
		<th colspan="3">5.Generate the report</th>
	</tr>
	<tr>
		<td class="noborder" colspan="3"></td>
	</tr>
	<tr>
		<th>Report Title:</th>
		<td><input type="text" id="report_title" name="report_title" value="" /></td>
        <td></td>
	</tr>
	<tr>
		<th>Report Name:</th>
		<td><input type="text" id="report_name" name="report_name" value="" /></td>
        <td></td>
	</tr>
	<tr>
		<th>Report Description:</th>
		<td>
			<textarea id="report_description" name="report_description" cols="30" rows="7"></textarea>
		</td>
        <td></td>
	</tr>	
	<tr>
		<th></th>
		<td><input type="submit" id="update_quick_report" name="update_quick_report" value="Update Quick Report"  /></td>
        <td><input type="hidden" id="reportid" name="reportid" value="<?php echo $_GET['id']; ?>"  /></td>
    </tr>
<tr>
	<td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">
	* Please note the following with regards to the formatted Microsoft Excel report:<br />
	<ol>
		<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
		<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
			<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
			Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
			This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.	
			It is safe to click on the "Yes" button to open the document.
		</li>
	</ol>
	</td>
</tr>
</table>
</form>



