<link rel="stylesheet" href="/assist.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/styles.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery/css/blue/jquery-ui.css" />
<?php
require_once( "../../library/dbconnect/dbconnect.php" );	
include_once("../class/report.php");
include_once("../class/naming.php");
@session_start();
$rep = new Report();
if(isset($_REQUEST['save_quick_report']))
{
    if($_REQUEST['report_name'] !== ""){
         $rep -> saveQuickReport();
    }
} else {
    $report = array();
    if(isset($_REQUEST['generate_quick_report']))
    {
        $_REQUEST = unserialize($rep->getQuickData($_REQUEST['id']));
        $report = $rep -> generateReport();
    } else {
       $report = $rep -> generateReport();
   }
    if(!empty($report)){
        switch($_REQUEST['display'])
        {
            case "screen":
	echo "<center><h1>Ignite Demo</h1><br />";
	echo "<h1>".$_REQUEST['report_title']."<br /></center>";
	$headers       = $report['headers'];
    $headersCount  = count($headers);
	$data 	       = $report['data'];
    $actionHeads   = $report['actionheaders'];
	echo "<table>";
	echo "<tr>";
	foreach($headers as $key => $heads){
		echo  "<th>".$heads."</th>";
	}
    foreach($actionHeads as $k => $head){
		echo  "<th>".$head."</th>";
	}
	echo "</tr>";
	foreach($data as $keys => $valueArr){
		echo  "<tr>";
		foreach($valueArr as $index => $value){
			if(is_array($value)){} else{
				echo  "<td>".$value."</td>";
			}
		}
        if(isset($valueArr['actions'])){
		echo  "<tr>";
		foreach($valueArr as $index => $value){
            if(is_array($value) && !empty($value)){

                foreach($value as $k => $v){
                     echo "<tr>";
                     echo "<td colspan='".$headersCount."'></td>";
                    foreach($v as $i => $val){
                         echo  "<td>".$val."</td>";
                    }
                    echo "</tr>";
                }

            }
		}
		echo  "</tr>";
        } else {

        }

		echo  "</tr>";
	}
	echo  "</table>";
	echo "<center><small><i><font size='1'color='gray'>Report generated on ".date('d')."-".date('F')."-".date('Y')."&nbsp;&nbsp;".date("H:i:s")."</font></i></small></center>";
                break;
            case "excell":
                displayExcell( $report );
                break;
            case "excell_formated":
                displayExcellFormatted( $report );
                break;
            case "pdf":
                displayPdf( $report );
                break;
            default:
                displayScreen( $report );
                break;
        }
    }
    function displayScreen( $reportData ){
        echo "<h1>Ignite Demo</h1><br />";
        echo "<h1>".$_REQUEST['report_title']."<br />";
        $headers = $reportData['headers'];
        $headersCount = count($headers);
        $data 	 = $reportData['data'];
        $actionHeads = $reportData['actionheaders'];
        echo "<table>";
        echo "<tr>";
        foreach($headers as $key => $heads){
            echo  "<th>".$heads."</th>";
        }
        foreach($actionHeads as $k => $head){
            echo  "<th>".$head."</th>";
        }
        echo "</tr>";
        foreach($data as $keys => $valueArr){

            if(isset($valueArr['actions'])){
            echo  "<tr>";
            foreach($valueArr as $index => $value){
                if(is_array($value) && !empty($value)){

                    foreach($value as $k => $v){
                         echo "<tr>";
                         echo "<td colspan='".$headersCount."'></td>";
                        foreach($v as $i => $val){
                             echo  "<td>".$val."</td>";
                        }
                        echo "</tr>";
                    }

                }
            }
            echo  "</tr>";
            } else {

            }
            echo  "<tr>";
            foreach($valueArr as $index => $value){
               echo  "<td>".$value."</td>";
            }
            echo  "</tr>";
        }
        echo  "</table>";
        echo "<small><i><font size='1'color='gray'>Report generated on ".date('d')."-".date('F')."-".date('Y')."&nbsp;&nbsp;".date("H:i:s")."</font></i></small>";
    }


    function displayExcell( $report ){
        echo "Displaying on exceell please wait . . .";
    }

    function displayExcellFormatted( $report ){
        echo "Displaying on exceell please wait . . .";
    }

    function displayPdf( $report ){
            echo "Displaying on pdf please wait . . .";
    }

}
?>