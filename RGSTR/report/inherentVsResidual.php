<?php
$scripts = array( 'fixedreports.js', 'jquery.ui.risk.js');
$styles = array( 'colorpicker.css' );
$page_title = "Generate Report";
require_once("../inc/header.php");
?> 
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
<table align="center" width="100%" class="noborder"> 
  <tr>
  	<td class="noborder"><center><h4><?php echo ucwords($_SESSION['cn']); ?></h4></center></td>
  </tr>
   <tr>
    <td class="noborder" align="center" valign="top">
    	<center><div id="chartdiv" style="width: auto%; height: 300px; background-color:#FFFFFF"></div></center>
   </td>
  </tr>
  <tr>
  	<td class="noborder">
		<table class="noborder">
			<tr>
				<td class="noborder">
					<script language="javascript">
						$(function(){
							//$("#risk").risk({ section:"new", headers:['risk_item', 'impact_rating', 'likelihood_rating', 'inherent_risk_rating','control_rating', 'residual_risk']});	
						});
					</script>
					<div id="risk"></div>				
				</td>
				<td class="noborder"> 
					<span style="background-color:#FF6600">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;Inherent Risk Rating<br />
					<span style="background-color:#D2CB00">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;Residual Risk
				</td>
			</tr>
		</table>
  	</td>
  </tr>
  <tr>
  	<td class="noborder">
		<center>
		<p>
			<?php echo "Report genereated on ".date("d")." ".date("F")." ".date("Y")." at ".date("H:m"); ?>
		</p>	
		</center>      	
  	</td>
  </tr>
</table>

