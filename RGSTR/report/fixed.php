<?php
$scripts = array( 'quick.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$folder = "report";
$page = "fixed";
//$page_title = "Quick Report";
require_once("../inc/header.php");


$nameObj = new Naming();

$ireName = $nameObj->getAName("inherent_risk_exposure");
$rrName = $nameObj->getAName("residual_risk_exposure");
$rtName = $nameObj->getAName("risk_type");
$rsName = $nameObj->getAName("risk_status");
$asName = $nameObj->getAName("action_status");
$rcName = $nameObj->getAName("risk_category");
$roName = $nameObj->getAName("risk_owner");
$ccName = $nameObj->getAName("control_rating");

$i = 1;

?>

<div>
	<div id="quick_report_message"></div>
    <table border="1" id="hideOnshow">
    	<tr>
        	<th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Report Format</th>
            <th>Action</th>
        </tr>
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Register</td>
			<td>The Register details the current <?php echo $risk_object_name; ?></td>
            <td>On Screen</td>            
            <td><input type="button" id="show_riskregister" value=" Generate "  /></td>
        </tr>
<!--        <tr>
        	<td><?php //echo $i; $i++; ?></td>
            <td>Strategic <?php echo $risk_object_name; ?> Register</td>
			<td>Graphical display of the <?php echo $ireName." and ".$rrName; ?> per item</td>
            <td>Bar Graph</td>            
            <td><input type="button" id="show_inherentVsresidual" value=" Generate "  /></td>
        </tr> -->
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Dashboard</td>
			<td>Analysis of <?php echo $risk_object_name; ?> Ratings</td>
			<td>Graph</td>
            <td style="text-align: center;"><input type=button value=Generate onclick="document.location.href='quick_dashboard.php';" /></td>          
        </tr>         
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td><?php echo $risk_object_name; ?> Exposure per <?php echo $rtName; ?></td>
			<td>Graphical display of the <?php echo $ireName." and ".$rrName." per ".$rtName; ?></td>
            <td>Bar Graph</td>            
            <td><input type="button" id="show_inherentVsresidualperType" value=" Generate "  /></td>
        </tr>
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td><?php echo $risk_object_name; ?> Exposure per <?php echo $rcName; ?></td>
			<td>Graphical display of the <?php echo $ireName." and ".$rrName." per ".$rcName; ?></td>
            <td>Bar Graph</td>            
            <td><input type="button" id="show_inherentVsresidualperCategory" value=" Generate "  /></td>
        </tr>  
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td><?php echo $risk_object_name; ?> Exposure per <?php echo $roName; ?></td>
			<td>Graphical display of the <?php echo $ireName." and ".$rrName; ?> per <?php echo $roName; ?></td>
            <td>Bar Graph</td>            
            <td><input type="button" id="show_inherentVsresidualperPerson" value=" Generate "  /></td>
        </tr>           
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td><?php echo $rsName; ?></td>
			<td>View the number of <?php echo $risk_object_name; ?>s in each status</td>
            <td>Pie Graph</td>            
            <td><input type="button" id="number_of_risk_in_each_status" value=" Generate "  /></td>
        </tr>                                                   
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td><?php echo $asName; ?></td>
			<td>View the number of <?php echo $action_object_name; ?>s in each status</td>
            <td>Pie Graph</td>            
            <td><input type="button" id="number_of_actions_in_each_status" value=" Generate "  /></td>
        </tr> 
<?php //if($_SERVER['REMOTE_ADDR']=="105.233.22.21") { ?>		
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td><?php echo $ccName; ?></td>
			<td>Graphical display of the <?php echo $rrName; ?> versus <?php echo $ccName; ?></td>
            <td>Scatter Graph</td>            
            <td><input type="button" id="risk_versus_control" value=" Generate "  /></td>
        </tr>                                          
<?php //} ?>		
    </table>
</div>
<div id="register" style="display:none;">
<center><h1>Ignite Demo</h1></center> 
<table align="left" id="risk_table" border="1" >
<tr>
    <td  id="paging" colspan="15" align="left">
        <table id="paging_risk" border="2" width="100%" cellpadding="5" cellspacing="0">
        </table>              
    </td>    
</tr>        
</table>
</div>