<?php
require_once("../library/class/assist_dbconn.php");
require_once("../library/class/assist_db.php");
require_once("../library/class/assist_helper.php");
include_once("../library/dbconnect/dbconnect.php");
//include_once("class/naming.php");
include_once("class/riskactionreminder.php");
//include_once("class/queryreminder.php");
//include_once("../library/class/assist_email_summary.php");
//include_once("class/administrator.php");
//spl_autoload_register("Loader::autoload");
function emailRGSTR($db, $userarray = array())
{
    $actionObj      = new RiskActionReminder($db);
    $actionResponse = $actionObj -> sendReminders($userarray);
    /*

    $queryObj       = new QueryReminder($db);
    $queryResponse  = $queryObj -> sendReminders($userarray);

    $summaryObj      = new SummaryNotification();
    $summaryResponse = $summaryObj -> getSummaryNotificationActions($db, $userarray);
    */
    $total           = $actionResponse['totalSend'] ;


    unset($actionResponse['totalSend']);

    $response  = $actionResponse;//array_merge($queryResponse, $actionResponse, $summaryResponse);
    $reminders = array();
    $x         = 1;
    foreach($response as $resIndex => $reminder)
    {
      $reminders[$x++] = $reminder;
    }
    $reminders[0] = $total." reminders send ";
    return $reminders;
}
?>
