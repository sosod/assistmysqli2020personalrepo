<?php


/* 
Purpose: feed fields into list and list_edit to facilitate the setup of lists within the RGSTR module.

Created: Janet Currie (20 March 2014)

ASSUMPTIONS:
		STATUS:
			active = 1
			inactive = 2
			deleted = 0
		DEFAULTS:
			defaults = 1
*/


switch($page_title_src) {
case "risk_status":
	$primary_key = "id";
	$audit_log_link = "status_logs";
	$table = $db->getDBRef()."status";
	$inactive_status = RiskStatus::INACTIVE;
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'name'	=> array(
			'heading'	=> "Default Terminology",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'maxlen'	=> "100",
			'is_default'=> true,
		),
		'client_terminology'	=> array(
			'heading'	=> "Your Terminology",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "client_terminology",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = true;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT * FROM ".$db->getDBRef()."_status WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE status = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT * FROM ".$db->getDBRef()."_status WHERE active <> ".RiskStatus::DELETED);
	}
	break;
case "action_status":
	$primary_key = "id";
	$audit_log_link = "action_status_logs";
	$table = $db->getDBRef()."_action_status";
	$inactive_status = ActionStatus::INACTIVE;
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'name'	=> array(
			'heading'	=> "Default Terminology",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'maxlen'	=> "100",
			'is_default'=> true,
		),
		'client_terminology'	=> array(
			'heading'	=> "Your Terminology",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "client_terminology",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "status",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = true;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, name, client_terminology, color, IF( (status &1) =1, 1, 0 ) AS active, IF( (status &4) =4, 1, 0 ) AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_actions WHERE status = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, name, client_terminology, color, IF( (status &1) =1, 1, 0 ) AS active, IF( (status &4) =4, 1, 0 ) AS defaults FROM ".$table." WHERE status & ".ActionStatus::DELETED." <> ".ActionStatus::DELETED);
	}
	break;
case "risk_type":
	$primary_key = "id";
	$audit_log_link = "types_logs";
	$table = $db->getDBRef()."_types";
	$inactive_status = RiskType::INACTIVE;
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'shortcode'	=> array(
			'heading'	=> "Short Code",
			'type'		=> "CODE",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "shortcode",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'name'	=> array(
			'heading'	=> "Name",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'description'	=> array(
			'heading'	=> "Description",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "description",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, shortcode, name, description,  active, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE type = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, shortcode, name, description,  active, 0 AS defaults FROM ".$table." WHERE active & ".RiskType::DELETED." <> ".RiskType::DELETED);
	}
	break;
case "risk_level":
	$primary_key = "id";
	$audit_log_link = "level_logs";
	$table = $db->getDBRef()."_level";
	$inactive_status = RiskLevel::INACTIVE;
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'shortcode'	=> array(
			'heading'	=> "Short Code",
			'type'		=> "CODE",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "shortcode",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'name'	=> array(
			'heading'	=> "Name",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'description'	=> array(
			'heading'	=> "Description",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "description",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, shortcode, name, description, status as active, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE level = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, shortcode, name, description, status as active, 0 AS defaults FROM ".$table." WHERE status  <> ".RiskLevel::DELETED);
	}
	break;
case "risk_category":
	$primary_key = "id";
	$audit_log_link = "categories_logs";
	$table = $db->getDBRef()."_categories";
	$inactive_status = RiskCategory::INACTIVE;
	$rt =new RiskType();
	$items = $rt->getActiveRiskType();
	$nt = new Naming();
	$risk_type_name = $nt->getAName("risk_type");
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'shortcode'	=> array(
			'heading'	=> "Short Code",
			'type'		=> "CODE",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "shortcode",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'name'	=> array(
			'heading'	=> "Name",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'description'	=> array(
			'heading'	=> "Description",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "description",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'type_id'	=> array(
			'heading'	=> $risk_type_name,
			'type'		=> "SELECT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "type_id",
			'maxlen'	=> "250",
			'is_default'=> false,
			'list_items'=> $items,
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, shortcode, name, description, active, 0 AS defaults, type_id FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE category = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("
			SELECT RC.id, RC.shortcode, RC.name, RC.description, RC.active, 0 AS defaults, RT.name as type_id 
			FROM ".$table." RC
			INNER JOIN ".$db->getDBRef()."_types RT 
			  ON RT.id = type_id
			WHERE RC.active  <> ".RiskCategory::DELETED
		);
	}
	break;
case "financial_exposure":
	$primary_key = "id";
	$table = $db->getDBRef()."_financial_exposure";
	$audit_log_link = "financial_exposure_logs";
	$inactive_status = FinancialExposure::INACTIVE;
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'exp'	=> array(
			'heading'	=> "Rating",
			'type'		=> "RATING",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "exp",
			'maxlen'	=> "10",
			'is_default'=> false,
		),
		'name'	=> array(
			'heading'	=> "Name",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'definition'	=> array(
			'heading'	=> "Definition",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "definition",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, exp_from, exp_to, name, definition, status as active, color, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE financial_exposure = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, CONCAT(exp_from,' - ',exp_to) as exp, name, definition, color, status as active, 0 as defaults FROM ".$table." WHERE status  <> ".FinancialExposure::DELETED." ORDER BY exp_from, exp_to");
	}
	break;
case "impact":
	$primary_key = "id";
	$table = $db->getDBRef()."_impact";
	$audit_log_link = "impact_logs";
	$inactive_status = Impact::INACTIVE;
	$nameObj = new Naming();

	$iname = $nameObj->getAName("likelihood");
	$irname = $nameObj->getAName("inherent_risk_exposure");
	$comment = "For each $risk_object_name the $my_page_title Rating is used in conjunction with the $iname Rating to calculate the $irname Rating of that $risk_object_name.";

	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'rating'	=> array(
			'heading'	=> "Rating",
			'type'		=> "RATING",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "rating",
			'maxlen'	=> "10",
			'is_default'=> false,
		),
		'assessment'	=> array(
			'heading'	=> "Assessment",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "assessment",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'definition'	=> array(
			'heading'	=> "Definition",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "definition",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, rating_from, rating_to, assessment, definition, active, color, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE impact = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, CONCAT(rating_from,' to ',rating_to) as rating, assessment, definition, color, active, 0 as defaults FROM ".$table." WHERE active <> ".Impact::DELETED." ORDER BY rating_from, rating_to");
	}
	break;
case "likelihood":
	$primary_key = "id";
	$table = $db->getDBRef()."_likelihood";
	$audit_log_link = "likelihood_logs";
	$inactive_status = Likelihood::INACTIVE;
	
	$nameObj = new Naming();
	$iname = $nameObj->getAName("impact");
	$irname = $nameObj->getAName("inherent_risk_exposure");
	
	$comment = "For each $risk_object_name the $my_page_title Rating is used in conjunction with the $iname Rating to calculate the $irname Rating of that $risk_object_name.";
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'rating'	=> array(
			'heading'	=> "Rating",
			'type'		=> "RATING",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "rating",
			'maxlen'	=> "10",
			'is_default'=> false,
		),
		'assessment'	=> array(
			'heading'	=> "Assessment",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "assessment",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'definition'	=> array(
			'heading'	=> "Definition",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "definition",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'probability'	=> array(
			'heading'	=> "Probability",
			'type'		=> "NUM",
			'value'		=> "",
			'class'		=> "NUM",
			'db'		=> "probability",
			'maxlen'	=> "10",
			'postfix'	=> "%",
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, rating_from, rating_to, assessment, definition, probability, active, color, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE likelihood = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, CONCAT(rating_from,' to ',rating_to) as rating, assessment, definition, probability, color, active, 0 as defaults FROM ".$table." WHERE active <> ".Likelihood::DELETED." ORDER BY rating_from, rating_to");
	}
	break;
case "inherent_risk_exposure":
	$primary_key = "id";
	$table = $db->getDBRef()."_inherent_exposure";
	$audit_log_link = "inherent_exposure_logs";
	$inactive_status = InherentRisk::INACTIVE;
	
	$nameObj = new Naming();
	$iname = $nameObj->getAName("impact");
	$irname = $nameObj->getAName("likelihood");
	$comment = "For each $risk_object_name, the $my_page_title Rating is calculated by multiplying the $iname Rating with the $irname Rating.";
	
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'rating'	=> array(
			'heading'	=> "Rating",
			'type'		=> "RATING",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "rating",
			'maxlen'	=> "10",
			'is_default'=> false,
		),
		'magnitude'	=> array(
			'heading'	=> "Magnitude",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "magnitude",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'response'	=> array(
			'heading'	=> "Definition",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "response",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, rating_from, rating_to, magnitude, response, active, color, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE inherent_risk_exposure >= ".$row['rating_from']." AND inherent_risk_exposure <= ".$row['rating_to']." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, CONCAT(rating_from,' to ',rating_to) as rating, magnitude, response, color, active, 0 as defaults FROM ".$table." WHERE active <> ".InherentRisk::DELETED." ORDER BY rating_from, rating_to");
	}
	break;
case "percieved_control_effective":
	$primary_key = "id";
	$table = $db->getDBRef()."_control_effectiveness";
	$audit_log_link = "control_effectiveness_logs";
	$inactive_status = ControlEffectiveness::INACTIVE;
	$nameObj = new Naming();

	$rname = $nameObj->getAName("residual_risk_exposure");
	$irname = $nameObj->getAName("inherent_risk_exposure");
	$comment = "For each $risk_object_name the $my_page_title Rating is used in conjunction with the $irname to calculate the $rname of that $risk_object_name.";

	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'effectiveness'	=> array(
			'heading'	=> "Effectiveness",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "effectiveness",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'qualification_criteria'	=> array(
			'heading'	=> "Qualification Criteria",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "qualification_criteria",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'rating'	=> array(
			'heading'	=> "Rating",
			'type'		=> "NUM",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "rating",
			'maxlen'	=> "10",
			'postfix'	=> "",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, rating, qualification_criteria, effectiveness, active, color, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE percieved_control_effectiveness = ".$id." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, rating, qualification_criteria, effectiveness, color, active, 0 as defaults FROM ".$table." WHERE active <> ".ControlEffectiveness::DELETED." ORDER BY rating");
	}
	break;
case "residual_risk_exposure":
	$primary_key = "id";
	$table = $db->getDBRef()."_residual_exposure";
	$audit_log_link = "residual_exposure_logs";
	$inactive_status = ResidualRisk::INACTIVE;
	
	$nameObj = new Naming();
	$cname = $nameObj->getAName("percieved_control_effective");
	$irname = $nameObj->getAName("inherent_risk_exposure");
	$comment = "For each $risk_object_name, the $my_page_title is calculated by multiplying the $irname with the $cname Rating.";
	
	$fields = array(
		'id'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
			'db'		=> "id",
			'class'		=> "center",
		),
		'rating'	=> array(
			'heading'	=> "Rating",
			'type'		=> "RATING",
			'value'		=> "",
			'class'		=> "center",
			'db'		=> "rating",
			'maxlen'	=> "10",
			'is_default'=> false,
		),
		'magnitude'	=> array(
			'heading'	=> "Magnitude",
			'type'		=> "VARCHAR",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "magnitude",
			'maxlen'	=> "100",
			'is_default'=> false,
		),
		'response'	=> array(
			'heading'	=> "Definition",
			'type'		=> "TEXT",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "response",
			'maxlen'	=> "250",
			'is_default'=> false,
		),
		'color'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'maxlen'	=> "",
		),
		'active'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
			'db'		=> "active",
			'value'		=> "",
			'class'		=> "center",
			'default_value'		=> "1",
		),
	);	
	$has_defaults = false;
	if($page_action=="EDIT") {
		$id = $_REQUEST['id'];
		$row = $db->mysql_fetch_one("SELECT id, rating_from, rating_to, magnitude, response, active, color, 0 AS defaults FROM ".$table." WHERE id=".$id);
		$count = $db->mysql_fetch_one_value("SELECT count(id) as rc FROM ".$db->getDBRef()."_risk_register WHERE residual_risk_exposure >= ".$row['rating_from']." AND residual_risk_exposure <= ".$row['rating_to']." AND active = 1","rc");
	} else {
		$rows = $db->mysql_fetch_all("SELECT id, CONCAT(CONCAT(rating_from,'.00'),' to ',CONCAT(rating_to,'.99')) as rating, magnitude, response, color, active, 0 as defaults FROM ".$table." WHERE active <> ".ResidualRisk::DELETED." ORDER BY rating_from, rating_to");
	}
	break;
}



?>