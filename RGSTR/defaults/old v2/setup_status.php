<?php
$styles = array();
//$page_title = "Change Risk Status";
$folder = "defaults";
$page = "default";
$page_title = true;

switch($_REQUEST['field']) {
case "risk_status":
	$scripts = array( 'change_risk_status.js','menu.js', 'jscolor.js', 'functions.js' );
	$page_title_src = "risk_status";
	$form_id = "risk-status-form";
	$table_id = "risk_status_table";
	$div_id = "risk_status_message";
	$submit_id = "save_status";
	$cancel_id = "cancel_status";
	$hidden_id = "risk_status_id";
	$select_id = "status_status";
	$fields = array();
	break;
default:
	$scripts = array();
	$page_title_src = "";
}


require_once("../inc/header.php");


switch($_REQUEST['field']) {
	case "risk_status":
		$object = new RiskStatus( "", "", "" );
		$row =  $object -> getAStatus( $_REQUEST['id']) ;	
		break;
	default:
}
?>
<h2>Change Status</h2>
<?php ASSIST_HELPER::JSdisplayResultPrep(""); ?>
    <div>
    <form id="<?php echo $form_id; ?>" name="<?php echo $form_id; ?>" method="post">
    <div id="<?php echo $div_id; ?>"></div>
    <table id="<?php echo $table_id; ?>">
    <tr>
    	 <th>Ref:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
	<tr>
		<th>Current Status:</th>
		<td><?php echo $row['active']==1 ? "Active" : "Inactive"; ?></td>
	</tr>
      <tr>
        <th>New Status:</th>
        <td>
        	<select name="<?php echo $select_id; ?>" id="<?php echo $select_id; ?>">
            	<option value="1" <?php if($row['active'] == 1) { ?> selected <?php } ?> >Active</option>
                <option value="0" <?php if($row['active'] == 0) { ?> selected <?php } ?>  >Inactive</option>
            </select>
        </td>
      </tr>
       <tr>
      	<th></th>
        <td>
        <input type="hidden" name="<?php echo $hidden_id; ?>" id="<?php echo $hidden_id; ?>" value="<?php echo $_REQUEST['id']?>" />
        <input type="hidden" name="field" id="field" value="<?php echo $_REQUEST['field']?>" />
        <input type="submit" name="<?php echo $submit_id; ?>" id="<?php echo $submit_id; ?>" value="Save Changes" class=isubmit />
        <input type="submit" name="<?php echo $cancel_id; ?>" id="<?php echo $cancel_id; ?>" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left"><?php $me->displayGoBack("", ""); ?></td>
      	<td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>
<?php ASSIST_HELPER::displayResult(array("info","Please note:<Br />\"Active\" means that the status is available for use.<br />\"Inactive\" means that the status is not available for use but any ".$risk_object_name." associated with it will still display.")) ?> 
