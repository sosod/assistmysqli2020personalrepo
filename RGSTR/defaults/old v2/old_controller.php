<?php
session_start();
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../../".strtolower($classname).".php" ) ) {
		require_once( "../../".strtolower($classname).".php" );		
	}else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")) {
		require_once( "../../library/dbconnect/".strtolower($classname).".php" );		
	} else {
		require_once( "../../library/".strtolower($classname).".php" );		
	}
}
switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ){
	case "financial_year":
		$fnyr = new FinancialYear( $_REQUEST['last_day'], $_REQUEST['start_date'], $_REQUEST['end_date'] );
		echo json_encode( $fnyr -> saveFinYear() );
		break;
	case "getFinYear":
		$fnyr = new FinancialYear( "", "", "" );
		echo json_encode( $fnyr -> getFinYear() );
		break;
	case "editFinancialYear":
		$fnyr = new FinancialYear( $_REQUEST['last_day'], $_REQUEST['start_date'], $_REQUEST['end_date'] );
		echo json_encode( $fnyr -> updateFinacialYear( $_REQUEST['id']) );
		break;		
	case "delFinYear":
		$fnyr = new FinancialYear( "", "", "" );
		echo $fnyr -> deleteFinacialYear( $_REQUEST['id']);
		break;
	case "menu":
		$menu = new Menu();
		$menu -> updateMenuName( $_REQUEST['name'] , $_REQUEST['value'] );
		break;
	case "getMenu":
		$menu = new Menu();
		echo json_encode( $menu -> getMenu() );
		break;
	case "getAllMenu":
		$menu = new Menu();
		echo json_encode( $menu -> getAllMenu() );		
		break;
	case "getNaming":
		$nm = new Naming();
		echo json_encode( $nm -> getNaming() );
 		break;
	case "getHeaderNames":
		$nm = new Naming();
		echo json_encode( $nm -> getAllHeaderNames() );
 		break;		
	case "updateHeaderNames":
		$nm = new Naming();
		echo json_encode( $nm -> updateHeaderNames( $_REQUEST['fieldname'], $_REQUEST['value'] ) );
		break;
	case "getFieldNames":
		$nm = new Naming();
		echo json_encode( $nm -> getFieldNames() );	
		break;
	case "naming":
		$nm = new Naming();
		$nm -> saveNaming( $_REQUEST['fieldname'], $_REQUEST['value'] ,1);
 		break;
	case "riskstatus":
		$stat = new RiskStatus( $_REQUEST['name'], $_REQUEST['client_terminology'], $_REQUEST['color']);
		$stat -> saveStatus();
		break;
	case "getRiskstatus":
		$getstat = new RiskStatus( "", "", "" );
		echo json_encode( $getstat -> getStatusWithCount() );		
		break;
	case "editRiskStatus":
		$getstat = new RiskStatus( $_REQUEST['name'], $_REQUEST['client_terminology'], $_REQUEST['color']);
		echo json_encode( $getstat -> updateStatus( $_REQUEST['id']) );	
		break;
	case "updaterisk":
		$up = new RiskStatus( $_REQUEST['name'], $_REQUEST['client_terminology'], $_REQUEST['color']);
		$up-> updateStatus( $_REQUEST['id'] );		
		break;
	case "changestatus":
		$deact = new RiskStatus( "", "", "");
		$deact -> changeStatus( $_REQUEST['id'], $_REQUEST['status'] );		
		break;
	case "activate":
		$act = new RiskStatus( "", "", "");
		$act -> activateStatus( $_REQUEST['id'] );		
		break;
	case "deactivate":
		$act = new RiskStatus( "", "", "");
		$act -> changeStatus( $_REQUEST['id'], 2 );
		break;
	case "getActionStatus":
		$actionstatus = new ActionStatus( "", "", "" );
		$response = $actionstatus  -> getStatusesWithCount(); 
		echo json_encode( $response );	
		break;
	case "updateActionStatus":
		$up = new ActionStatus( $_REQUEST['name'], $_REQUEST['client_term'], $_REQUEST['color']);
		echo json_encode( $up-> updateStatus( $_REQUEST['id'] ) );		
		break;
	case "changeActionstatus":
		$deactionstatus = new ActionStatus( "", "", "");
		$deactionstatus -> changeActionStatus( $_REQUEST['id'], $_REQUEST['status'] );		
		break;
	case "activateactionstatus":
		$actactionstatus = new ActionStatus( "", "", "");
		$actactionstatus -> activateStatus( $_REQUEST['id'] );		
		break;			
	case "newActionStatus":
		$actionstatus = new ActionStatus( $_REQUEST['name'], $_REQUEST['client_term'], $_REQUEST['color']);
		$actionstatus -> saveStatus();
		break;
	case "getUsedTypes":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getUsedTypes() );	 
		break;		
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	 
		break;
	case "updateRiskType":
		$type = new RiskType( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], "" );
		echo json_encode( $type-> updateType( $_REQUEST['id']) ) ;	
		break;		
	case "changetypestatus":
		$type = new RiskType( "", "", "", "" );
		$type-> changeTypeStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "typeactivate":
		$type = new RiskType( "", "", "", "" );
		$type-> activateType( $_REQUEST['id'] );	
		break;
	case "newRiskType":
		$type = new RiskType( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], "" );
		$type-> saveType( );	
		break;
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "updateCategory":
		$riskcat = new RiskCategory( $_REQUEST['category'], $_REQUEST['shortcode'], $_REQUEST['description'], $_REQUEST['type_id']);
		echo json_encode( $riskcat  -> updateCategory( $_REQUEST['id']) );
		break;
	case "newCategory":
		$riskcat = new RiskCategory( $_REQUEST['category'], $_REQUEST['shortcode'], $_REQUEST['description'], $_REQUEST['type_id'] );
		$riskcat  -> saveCategory();
		break;
	case "catdeactivate":
		$type = new RiskCategory( "", "", "", "" );
		$type-> deactivateCategory( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "catactivate":
		$type = new RiskCategory( "", "", "", "" );
		$type-> activateCategory( $_REQUEST['id'] );	
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "changeImpstatus":
		$impact = new Impact( "", "", "", "", "");
		$impact -> updateImpactStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "impactactivate":
		$impact = new Impact( "", "", "", "", "");
		$impact -> activateImpact( $_REQUEST['id'] );	
		break;
	case "newImpact":
		$impact = new Impact( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['assmnt'], $_REQUEST['dfn'], $_REQUEST['clr']);
		$impact -> saveImpact();	
		break;
	case "updateImpact":
		$impact = new Impact( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['assmnt'], $_REQUEST['dfn'], $_REQUEST['clr']);
		echo json_encode( $impact -> updateImpact( $_REQUEST['id'] ) );	
		break;
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "changeLikestat":
		$like = new Likelihood( "", "", "", "", "", "");
		$like -> updateLikelihoodStatus( $_REQUEST['id'] , $_REQUEST['status'] );	
		break;
	case "likeactivate":
		$like = new Likelihood( "", "", "", "", "", "");
		$like -> activateLikelihood( $_REQUEST['id'] );	
		break;
	case "newLikelihood":
		$like = new Likelihood( 
						$_REQUEST['from'], $_REQUEST['to'], $_REQUEST['assmnt'], $_REQUEST['dfn'], $_REQUEST['prob'], $_REQUEST['clr']
					);
		$like -> saveLikelihood();	
		break;
	case "updateLikelihood":
		$like = new Likelihood( 
						$_REQUEST['from'],
						$_REQUEST['to'],
						$_REQUEST['assmnt'],
						$_REQUEST['dfn'],
						$_REQUEST['prob'],
						$_REQUEST['clr']
					);
		echo json_encode( $like -> updateLikelihood( $_REQUEST['id']) );	
		break;		
	case "getInherentRisk":
		$ihr= new InherentRisk( "", "", "", "", "");
		$ihr -> getInherentRisk();
		break;
	case "changeInherent":
		$ihr = new InherentRisk( "", "", "", "", "");
		$ihr -> updateInherentRiskStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "ihractactivate":
		$ihr = new InherentRisk( "", "", "", "", "");
		$ihr -> activateInherentRisk( $_REQUEST['id'] );	
		break;
	case "newInherentRisk":
		$ihr = new InherentRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magnitude'], $_REQUEST['response'], $_REQUEST['clr']);
		$ihr -> saveInherentRisk();	
		break;
	case "updateInherentRisk":
		$ihr = new InherentRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magnitude'], $_REQUEST['response'], $_REQUEST['clr']);
		echo $ihr -> updateInherentRisk( $_REQUEST['id'] );	
		break;

	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness() );
		break;
	case "changeControl":
		$ihr = new ControlEffectiveness( "", "", "", "", "");
		$ihr -> updateControlEffectivenessStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "ctrlactivate":
		$ihr = new ControlEffectiveness( "", "", "", "", "");
		$ihr -> activateControlEffectiveness( $_REQUEST['id'] );	
		break;
	case "newControl":
		$ihr = new ControlEffectiveness( $_REQUEST['ctrl_shortcode'], $_REQUEST['ctrl'], $_REQUEST['qual'], $_REQUEST['rating'], $_REQUEST['clr']);
		$ihr -> saveControlEffectiveness();	
		break;
	case "updateControl":
		$ihr = new ControlEffectiveness( $_REQUEST['ctrl_shortcode'], $_REQUEST['ctrl'], $_REQUEST['qual'], $_REQUEST['rating'], $_REQUEST['clr']);
		echo $ihr -> updateControlEffectiveness( $_REQUEST['id'] );	
		break;
	case "getResidualRisk":
		$resrisk = new ResidualRisk( "", "", "", "", "");
		$resrisk -> getResidualRisk();
		break;
	case "changeResidualStatus":
		$resrisk = new ResidualRisk( "", "", "", "", "");
		$resrisk -> updateResidualRiskStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "residualactivate":
		$resrisk = new ResidualRisk( "", "", "", "", "");
		$resrisk -> activateResidualRisk( $_REQUEST['id'] );	
		break;
	case "newResidualRisk":
		$resrisk = new ResidualRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magn'], $_REQUEST['resp'], $_REQUEST['clr']);
		$resrisk -> saveResidualRisk();	
		break;
	case "updateResidualRisk":
		$resrisk = new ResidualRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magn'], $_REQUEST['resp'], $_REQUEST['clr']);
		echo $resrisk -> updateResidualRisk( $_REQUEST['id'] );	
		break;
	case "newUserAccess":
		$uaccess = new UserAccess( $_REQUEST['user'], $_REQUEST['modadmin'], $_REQUEST['cr_risk'], $_REQUEST['cr_actions'],
		 $_REQUEST['v_all'], $_REQUEST['e_all'], $_REQUEST['reports'], $_REQUEST['assurance'], $_REQUEST['setup'] );
		$uaccess -> saveUserAccess();
		break;
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;
    case "getUnAssignedUsers":
		$user_access_obj = new UserAccess( "", "", "", "", "", "", "", "", "" );
        $all_users          = $user_access_obj->getUsers();
        $all_assigned_users = $user_access_obj->getAllAssignedUsers();
        $list           = array();
        foreach($all_users as $index => $user)
        {
            if(!isset($all_assigned_users[$user['tkid']]))
            {
                $list[$user['tkid']]  = $user;
            }
        }
        echo json_encode($list);
		break;
	case "updateUserAccess":
		$uaccess = new UserAccess( $_REQUEST['user'], $_REQUEST['modadmin'], $_REQUEST['cr_risk'],  $_REQUEST['cr_actions'],
		 $_REQUEST['v_all'], $_REQUEST['e_all'], $_REQUEST['reports'], $_REQUEST['assurance'], $_REQUEST['setup'] );
		echo json_encode( $uaccess ->updateUserAccess() );
		break;		
	case "getUserAccess":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUserAccess() );
		break;
	case "setRiskManager":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		$uaccess -> setRiskManager( $_REQUEST['id'], $_REQUEST['status']);
		break;
	case "getRiskManagers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getRiskManager() );		
		break;	
	case "newDirectorate":
		$dir = new Directorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		$dir -> saveDirectorate();
		break;
	case "updateDirectorate":
		$dir = new Directorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		echo $dir -> updateDirectorate( $_REQUEST['id'] );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "changeDireStatus":
		$dir = new Directorate( "", "", "" ,"");
		$dir -> updateDirectorateStatus( $_REQUEST['id'], $_REQUEST['status'] );
		break;
	case "dire_activate":
		$dir = new Directorate( "", "", "" ,"");
		$dir -> activateDirectorate( $_REQUEST['id'] );
		break;
	case "newSubDirectorate":
		$dir = new SubDirectorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		$dir -> saveSubDirectorate();
		break;
	case "updateSubDirectorate":
		$dir = new SubDirectorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		echo $dir -> updateSubDirectorate( $_REQUEST['id'] );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		echo json_encode( $dir -> getSubDirectorate() );
		break;
	case "changeSubDirStatus":
		$dir = new SubDirectorate( "", "", "" ,"");
		$dir -> updateSubDirectorateStatus( $_REQUEST['id'], $_REQUEST['status'] );
		break;
	case "updateDefaults":
		$def = new Defaults();
		$def -> updateDefaults( $_REQUEST['name'], ($_REQUEST['value'] == "yes" ? "1" : "0") );
		break;
	case "subdir_activate":
		$dir = new SubDirectorate( "", "", "" ,"");
		$dir -> activateSubDirectorate( $_REQUEST['id'] );
		break;
	case "saveGlossary":
		$glosssary = new Glossary();
		$glosssary -> saveGlossary();
		break;
	case "getGlossary":
		$glosssary = new Glossary();
		echo json_encode( $glosssary -> getGlossary() ); 
		break;
	case "getAGlossary":
		$glosssary = new Glossary();
		echo json_encode( $glosssary -> getAGlossary() ); 
		break;	
	case "updateGlossary":
		$glosssary = new Glossary();
		echo $glosssary -> updateGlossary( $_REQUEST['id']); 
		break;	
	case "deleteGlossary":
		$glosssary = new Glossary();
		echo $glosssary -> deleteGlossary( $_REQUEST['id'] ); 
		break;	
	case "getColumns":
		$cols = new Columns();
		echo json_encode( array( "active" => $cols -> getActiveColumns() , "inactive"=>  $cols -> getInActiveColumns() ) ); 
		break;	
	case "saveColumnOrder":
		$cols = new Columns();
		parse_str( $_REQUEST['active'], $active ); 
		parse_str( $_REQUEST['inactive'], $inactive ); 						
		$cols -> saveColumnsOrder( $active['column'], $inactive['column']);
		break;	
	case "newUDF":
		$udf 	  = new UDF();
		$response = array();
		if( $udf -> saveUDF() > 0 ){
			$response = array( "error" => false, "text" => "UDF saved succesifully" );
		} else {
			$response = array( "error" => true, "text" => "An error occured saving the UDF" );			
		}
		echo json_encode( $response );
		break;
	case "getUDF":
		$udf 	  = new UDF();
		echo json_encode( $udf -> getUDF() );		
		break;		
	case "deleteUDF":
		$udf 	  = new UDF();
		$response = array();
		if( $udf -> deleteUDF( $_REQUEST['id'] ) == 1 ) {
			$response = array("error" => false, "text" => "UDF deleted");
		} else if( $udf -> deleteUDF( $_REQUEST['id'] ) == 0 ){
			$response = array("error" => true, "text" => "No changes were made ");
		} else {
			$response = array("error" => true, "text" => "An error occured deleting the data");
		}	
		echo json_encode( $response );			
		break;	
	case "getDefaults":
		$def = new Defaults();
		echo json_encode( $def -> getDefaults() );
		break;
	case "getRiskLevels":
		$level = new RiskLevel( "", "", "", "" );
		echo json_encode( $level->getLevel() );	 
		break;
	case "updateRiskLevel":
		$level = new RiskLevel( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], $_REQUEST['status'] );
		echo json_encode( $level-> updateLevel( $_REQUEST['id']) ) ;	
		break;		
	case "changelevelstatus":
		$level = new RiskLevel( "", "", "", "" );
		$level-> changeLevelStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "deActivateLevel":
		$level = new RiskLevel( "", "", "", "" );
		$level-> changeLevelStatus( $_REQUEST['id'], 2 );	
		break;		
	case "typeactivate":
		$level = new RiskLevel( "", "", "", "" );
		$level-> activateLevel( $_REQUEST['id'] );	
		break;
	case "saveRiskLevel":
		$level = new RiskLevel( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], $_REQUEST['status'] );
		$level-> saveLevel( );	
		break;
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		echo json_encode( $finExp -> getFinancialExposure() );
		break;
	case "changeFinancialExposureStatus":
		$finExp = new FinancialExposure( "", "", "", "", "");
		$finExp -> updateFinancialExposureStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "FinancialExposureActivate":
		$finExp = new FinancialExposure( "", "", "", "", "");
		$finExp -> activateFinancialExposure( $_REQUEST['id'] );	
		break;
	case "newFinancialExposure":
		$finExp = new FinancialExposure( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['name'], $_REQUEST['definition'], $_REQUEST['color']);
		$finExp -> saveFinancialExposure();	
		break;
	case "updateFinancialExposure":
		$finExp = new FinancialExposure( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['name'], $_REQUEST['definition'], $_REQUEST['color']);
		echo json_encode( $finExp -> updateFinancialExposure( $_REQUEST['id'] ) );	
		break;
    case   "deleteFinExposure":
        $finExp = new FinancialExposure( "","", "", "", "");
        $finExp -> updateFinancialExposureStatus( $_REQUEST['id'],$_REQUEST['status'] ) ;
       break;
    case "view_log":
        $logObj  = new Logs();
        $logs    = $logObj -> viewLogs( $_POST['table_name'] );
        echo json_encode($logs);
        break;
    case "risk_columns":
    	$riskclObj = new RiskColumns();
    	$columns   = $riskclObj -> getNaming();
        $ordered_columns = array();
        foreach($columns as $index => $column)
        {
            $ordered_columns[] = $column;
        }
    	echo json_encode($ordered_columns);
    	break;
    case "action_columns":
    	$actionColumnObj = new ActionColumns();
    	$columns   = $actionColumnObj-> getNaming();
        $ordered_action_columns = array();
        foreach($columns as $index => $column)
        {
            $ordered_action_columns[] = $column;
        }
    	echo json_encode($ordered_action_columns);
    	break;
    case "updateColumns":
			$naming  = new Naming();
    		$managearray = array();
    		$newarray    = array();
    		$positions   = array();
    		$x = $y = $z = 0;
    		foreach( $_POST['data'] as $index => $val)
    		{
    			if( $val['name'] == "manage")
    			{
    				$managearray[$x] = $val['value'];
    				$x++;
    			}

    			if( $val['name'] == "new")
    			{
    				$newarray[$y] = $val['value'];
    				$y++;
    			}
    			
    			if($val['name'] == "position")
    			{
    				$positions[$z] = $val['value'];
    				$z++;
    			}  			
    		}
    		$res  = 0;
    		foreach( $positions as $index => $id)
    		{
    			$updatedata = array();
    			if(in_array($id, $managearray))
    			{
    				$updatedata = array( "active" =>  4, "ordernumber" => $index);
    			}  else {
    				$updatedata = array( "active" =>  0, "ordernumber" => $index);
    			}   

    			if( in_array($id, $newarray)){
    				$updatedata = array( "active" => $updatedata['active'] + 8, "ordernumber" => $index);
    			} else {
    				$updatedata = array( "active" => $updatedata['active'] + 0, "ordernumber" => $index);
    			}
    			$res = $naming -> update("header_names", $updatedata, "id={$id}");		
    		}
    		if( $res >= 0)
    		{
    			$response = array("text" => "Columns updated", "error" => false);	
    		} else {
    			$response = array("text" => "Error updating columns", "error" => true);
    		}
    		echo json_encode($response);
    	break;
	case "saveNotification":
		$usernot = new UserNotification();
		$res = $usernot -> saveNotifications();
		$response = array();
		if( $res > 0) {
			$response = array("text" => "Nofication saved successfully", "error" => false);
		} else {
			$response = array("text" => "Error saving notification" , "error" => true);
		}
		echo json_encode($response);
		break;
	case "deleteNotification":
		$usernot = new UserNotification();
		$res = $usernot -> deleteNotification(  $_POST['id'] );
		if( $res > 0)
		{
			$response = array("text" => "Notification deleted successfully", "error" => false);
		} else {
			$response = array("text" => "Error updating notification", "error" => true);
		}
		echo json_encode( $response );	
		break;
	case "updateNotification":
		$usernot = new UserNotification();
		$res = $usernot -> updateNotification(  $_POST['id'] );
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Notification updated successfully", "error" => false);
		} else {
			$response = array("text" => "Error updating notification ", "error" => true);
		}
		echo json_encode( $response );
		break;		
	case "getNotifications":
		$usernot = new UserNotification();
		$results = $usernot -> getNotifications();
		$notifications = array();
		foreach( $results as $key => $notification)
		{
			$notifications[$notification['id']] = array(
														"id" 	  => $notification['id'],
														"what" 	  => ucwords( str_replace("_", " ", $notification['recieve_what']) ),
														"when"    => ucwords( str_replace("_", " ", $notification['recieve_when']) ),
														"day"     => ucwords( str_replace("_", " ", $notification['recieve_day']) ),
														"recieve" => $notification['recieve_email'],
														"recieve_what" => $notification['recieve_what'],
														"recieve_when" => $notification['recieve_when'],
														"recieve_day" => $notification['recieve_day'],
													  );
		}
		echo json_encode( $notifications );
		break;    	
	default:
		require_once("/views/".$_REQUEST['action'].".php");
		break;
	}
?>
