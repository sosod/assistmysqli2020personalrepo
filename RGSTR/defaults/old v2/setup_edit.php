<?php
$styles = array();
//$page_title = "Edit Risk Status";
$folder = "defaults";
$page = "default";
$page_title = true;

switch($_REQUEST['field']) {
case "risk_status":
	$page_title_src = "risk_status";
	$scripts = array( 'edit_risk_status.js','menu.js', 'jscolor.js' );
	$div_id = "risk_status_message";
	$form_id = "edit-risk-status-form";
	$table_id = "edit_risk_status_table";
	$audit_log_link = "status_logs";
	$fields = array(
		'ref'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
		),
		'default'	=> array(
			'heading'	=> "Default Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "status",
			'id'		=> "status",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'default'	=> true,
		),
		'client'	=> array(
			'heading'	=> "Your Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "client_term",
			'id'		=> "client_term",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "client_terminology",
			'default'	=> false,
		),
		'colour'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'name'		=> "risktype_color",
			'id'		=> "risktype_color",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'default'	=> false,
		),
	);
	$hidden_id = "risk_status_id";
	$submit_id = "edit_status";
	$cancel_id = "cancel_status";
	break;
case "action_status":
	$page_title_src = "action_status";
	$scripts = array( 'action_status.js','menu.js', 'jscolor.js' );
	$div_id = "editactionstatus_message";
	$form_id = "edit-form-action-status";
	$table_id = "editaction_status_table";
	$audit_log_link = "";
	$hidden_id = "action_status_id";
	$submit_id = "edit_actionstatus";
	$cancel_id = "cancel_actionstatus";
	$fields = array(
			'ref'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
		),
		'default'	=> array(
			'heading'	=> "Default Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "actionstatus",
			'id'		=> "actionstatus",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "name",
			'default'	=> true,
		),
		'client'	=> array(
			'heading'	=> "Your Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "action_client_term",
			'id'		=> "action_client_term",
			'value'		=> "",
			'class'		=> "",
			'db'		=> "client_terminology",
			'default'	=> false,
		),
		'colour'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'name'		=> "action_color",
			'id'		=> "action_color",
			'value'		=> "66FF00",
			'class'		=> "color",
			'db'		=> "color",
			'default'	=> false,
		),
	);
	break;
default:
	$page_title_src = "";
	$scripts = array();
	$div_id = "";
	$form_id = "";
	$table_id = "";
	$audit_log_link = "";
	$fields = array();
	$hidden_id = "";
	$submit_id = "";
	$cancel_id = "";
}



require_once("../inc/header.php");

switch($_REQUEST['field']) {
case "risk_status":
	$object = new RiskStatus( "", "", "" );
	$row =  $object -> getAStatus( $_REQUEST['id']) ;	
	break;
case "action_status":
	$actionstatus 	= new ActionStatus( "", "", "" );
	$row	= $actionstatus  -> getAStatus( $_REQUEST['id'] );
	if( ($row['status']&4) ==4) { $row['defaults'] = 1; } else { $row['defaults'] = 0; }
	break;
}
?>
<script>
$(function(){
		$("table#<?php echo $table_id; ?>").find("th").css({"text-align":"left"})
});
</script>
<h2>Edit</h2>
    <div>
    <?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
    <form id="<?php echo $form_id; ?>" name="<?php echo $form_id; ?>" method="post">
    <table id="<?php echo $table_id; ?>">
	<?php
	foreach($fields as $key => $f) {
		echo "
		<tr>
			<th>".$f['heading']."</th>
			<td>";
		switch($f['type']) {
			case "REF":
				echo $_REQUEST['id'];
				break;
			case "VARCHAR":
			case "COLOUR":
				echo "<input type=text value='".$row[$f['db']]."' name='".$f['name']."' id='".$f['id']."' size=50 ".($f['default']==true && $row['defaults']==1 ? "disabled=disabled" : "")." class='".$f['class']."' />";
				break;
				break;
			case "BUTTONS":
			default:
		}
		echo "
			</td>
		</tr>";
	}
	?>
      <tr>
      	<th></th>
        <td>
			<input type="hidden" name="<?php echo $hidden_id; ?>" id="<?php echo $hidden_id; ?>" value="<?php echo $_REQUEST['id']?>" />
			<input type="hidden" name="field" id="field" value="<?php echo $_REQUEST['field']?>" />
			<input type="submit" name="<?php echo $submit_id; ?>" id="<?php echo $submit_id; ?>" value="Save Changes" class=isubmit />
			<input type="submit" name="<?php echo $cancel_id; ?>" id="<?php echo $cancel_id; ?>" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left"><?php displayGoBack("", ""); ?></td>
      	<td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>