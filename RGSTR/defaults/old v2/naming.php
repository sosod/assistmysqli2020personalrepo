<?php
$folder = "defaults";
$page = "default";
$page_title = "Naming Convention";

$scripts = array( 'naming.js','menu.js', 'functions.js' );
$styles = array( 'colorpicker.css' );
//$page_title = "View Risk";
require_once("../inc/header.php");
error_reporting(-1);
$result = array();
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
	$f = $_REQUEST['fld'];
	$v = $_REQUEST['val'];
	$result = $default->setObjectName($f,$v);
	switch($f) {
	case "object_name":
		$risk_object_name = $default->getObjectName("OBJECT");
		break;
	case "action_name":
		$action_object_name = $default->getObjectName("ACTION");
		break;
	}
	
}

//$test = new NAMING();
//$rows = $test->getAllHeaderNames();
//ASSIST_HELPER::arrPrint($rows);


$me->displayResult($result);
?>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="naming_message" class="message"></div>


<table class="noborder">
	<tr>
		<td colspan=2 class=noborder>
			<h2>Object Names</h2>
<table>
	<tr>
		<td></td>
		<th>Default Terminology</th>
		<th>Your Terminology</th>
		<th></th>
	</tr>
	<tr>
		<th>Primary Object Name:</th>
		<td>Risk</td>
		<td><input type=text id=object_name value="<?php echo $risk_object_name; ?>" /></td>
		<td><input type=button value=Save class=object_name_save id=btn_object_name /></td>
	</tr>
	<tr>
		<th>Action Object Name:</th>
		<td>Action</td>
		<td><input type=text id=action_name value="<?php echo $action_object_name; ?>" /></td>
		<td><input type=button value=Save class=object_name_save id=btn_action_name /></td>
	</tr>
</table>
		</td>
	</tr>
  <tr>
    <td colspan="2" class="noborder">
		<h2>Column Names</h2>
		<div id="container" width=100% style='width: 100%'></div>
	</td>
  </tr>
  <tr>
    <td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
    <td class="noborder"><?php $admire_helper->displayAuditLogLink( "header_names_logs"  , true); ?></td>
  </tr>
</table>

<script type=text/javascript>
$(function() {	
	$(".object_name_save").click(function() {
		var i = $(this).prop("id");
		i = AssistString.substr(i,4,i.length);
		var v = $("#"+i).val();
		document.location.href = "naming.php?act=SAVE&fld="+i+"&val="+v;
	});
});
</script>