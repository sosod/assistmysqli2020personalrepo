<?php
$folder = "defaults";
$page = "default";
$page_title = true;

switch($_REQUEST['id']) {
case "risk_status":
	$page_title_src = "risk_status";
	$scripts = array( 'risk_status.js','menu.js', "201a.js","jscolor.js", 'functions.js' );
	$div_id = "risk_status_message";
	$form_id = "risk-status-form";
	$table_id = "risk_status_table";
	$audit_log_link = "status_logs";
	$fields = array(
		'ref'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
		),
		'default'	=> array(
			'heading'	=> "Default Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "status",
			'id'		=> "status",
			'value'		=> "",
			'class'		=> "",
		),
		'client'	=> array(
			'heading'	=> "Your Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "client_term",
			'id'		=> "client_term",
			'value'		=> "",
			'class'		=> "",
		),
		'colour'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'name'		=> "risktype_color",
			'id'		=> "risktype_color",
			'value'		=> "66FF00",
			'class'		=> "color",
		),
		'status'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
		),
		'buttons'	=> array(
			'heading'	=> "",
			'type'		=> "BUTTONS",
			'add'		=> array(
				'name'	=> "add_status",
				'id'	=> "add_id",
				'value'	=> "Add",
			),
			'edit'		=> array(),
		),
	);
	break;
case "action_status":
	$page_title_src = "action_status";
	$scripts = array( 'action_status.js','menu.js','jscolor.js', 'functions.js');
	$div_id = "actionstatus_message";
	$form_id = "form-action-status";
	$table_id = "action_status_table";
	$audit_log_link = "action_status_logs";
	$fields = array(
		'ref'		=> array(
			'heading'	=> "Ref",
			'type'		=> "REF",
		),
		'default'	=> array(
			'heading'	=> "Default Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "actionstatus",
			'id'		=> "actionstatus",
			'value'		=> "",
			'class'		=> "",
		),
		'client'	=> array(
			'heading'	=> "Your Terminology",
			'type'		=> "VARCHAR",
			'name'		=> "action_client_term",
			'id'		=> "action_client_term",
			'value'		=> "",
			'class'		=> "",
		),
		'colour'	=> array(
			'heading'	=> "Colour",
			'type'		=> "COLOUR",
			'name'		=> "action_color",
			'id'		=> "action_color",
			'value'		=> "66FF00",
			'class'		=> "color",
		),
		'status'	=> array(
			'heading'	=> "Status",
			'type'		=> "STATUS",
		),
		'buttons'	=> array(
			'heading'	=> "",
			'type'		=> "BUTTONS",
			'add'		=> array(
				'name'	=> "add_actionstatus",
				'id'	=> "add_actionstatus",
				'value'	=> "Add",
			),
			'edit'		=> array(),
		),
	);
	break;
default:
	$page_title_src = "";
	$scripts = array();
	$div_id = "";
	$form_id = "";
	$table_id = "";
	$audit_log_link = "";
	$fields = array();
	break;
}



$styles = array();
require_once("../inc/header.php");



//$test = new RISKSTATUS();
//$s = $test->getStatusWithCount();
//ASSIST_HELPER::arrPrint($s);


?>
<style type=text/css>
table tr.active0 td {
	color: #555555;
	background-color: #dedede;
}
table tr.active1 td {
}
</style>
<div>
<?php ASSIST_HELPER::JSdisplayResultPrep(""); ?>
<?php 
if(isset($_REQUEST['r']) && is_array($_REQUEST['r']) && $_REQUEST['r'][0]=="ok") {
	echo "<script type=text/javascript>JSdisplayResult(\"ok\", \"ok\", \"".$_REQUEST['r'][1]."\");</script>";
	echo "<input type=hidden name=displayResult id=displayResultCheck value='ok' />";
} else {
	echo "<input type=hidden name=displayResult id=displayResultCheck value='info' />";
}
?>
<div id="<?php echo $div_id; ?>"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
		    <form id="<?php echo $form_id; ?>" name="<?php echo $form_id; ?>" method="post">
		    <table border="1" id="<?php echo $table_id; ?>">
		      <tr><?php
				foreach($fields as $key => $f) {
					echo "<th>".$f['heading']."</th>";
				}
		      ?></tr>
		      <tr><?php  //add form
				foreach($fields as $key => $f) {
					echo "<td>";
						switch($f['type']) {
						case "REF":
							echo "#";
							break;
						case "STATUS":
							break;
						case "BUTTONS":
							echo "<input type='submit' class=isubmit name='".$f['add']['name']."' id='".$f['add']['id']."' value='".$f['add']['value']."' />";
							break;
						case "COLOUR":
						case "VARCHAR":
						default:
							echo "<input type='text' name='".$f['name']."' id='".$f['id']."' class='".$f['class']."' value='".$f['value']."' />";
							break;
						}
					echo "</td>";
				}
			  ?>
			  </tr><!-- <tr>
		        <td>#</td>
		        <td><input type="text" name="status" id="status" /></td>
		        <td><input type="text" name="client_term" id="client_term" /></td>
		        <td>
		      <input name="risktype_color" id="risktype_color" class="color" value="66ff00"></td>
		        <td><input type="submit" name="add_status" id="add_status" value="Add" /></td>
		        <td></td>
		        <td>
		        </td>
		      </tr> -->
		    </table>
		    </form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( $audit_log_link, true); ?></td>
	</tr>
</table>
</div>

<?php ASSIST_HELPER::displayResult(array("info","Please note:<Br />List items which have been used within the module cannot be deleted.  Please use the \"Change Status\" function to mark them as \"Inactive\" instead.")) ?> 
