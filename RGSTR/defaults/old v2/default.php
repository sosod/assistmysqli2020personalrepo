<?php
$page = "default";
$folder = "defaults";

$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "";
require_once("../inc/header.php");

$namingObj = new Naming();
$names = $namingObj->getReportingNaming();

$glossary = $m0->getMenuName("glossary");
//echo $glossary;

?>
<script language="javascript">
	$(function(){
		$(".setup_defaults").click(function() {
			switch(this.id) {
			case "risk_owner":
			case "menu_heading":
			case "naming":
				document.location.href = this.id+".php?id="+this.id;
				break;
			default:
				document.location.href = "setup.php?id="+this.id;
			}
			return false;		  
		});
		//$("table#link_to_page").find("th").css({"text-align":"left"})
	});
</script>
<table class=form>
    <tr>
    	<th>Menu:</th>
        <td>Define the menu headings to be displayed in the module.</td>
        <td><input type="button" name="menu_heading" id="menu_heading" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Naming Convention:</th>
        <td>Define the naming convention of the columns/fields used in the module. </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
<!--    <tr>
    	<th>Financial Years:</th>
        <td>Configure the Financial Years</td>
        <td><input type="button" name="financial_year" id="financial_year" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
	
	<?php 
	$list_items = array("risk_status","action_status","risk_type","risk_level","risk_category","financial_exposure","impact","likelihood","inherent_risk_exposure","percieved_control_effective","residual_risk_exposure");
	foreach($list_items as $l) {
		echo "
		<tr>
			<th>".$names[$l].":</th>
			<td>Configure the ".$names[$l]." items to be used in the module.</td>
			<td><input type='button' name='$l' id='$l' value='Configure' class='setup_defaults'  /></td>
		</tr>
		";
	}
	?>
	
    <tr>
    	<th><?php echo $names['risk_owner']; ?>:</th>
        <td>Configure the <?php echo $names['risk_owner']; ?> and the associated administrators</td>
        <td><input type="button" name="risk_owner" id="risk_owner" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <!-- <tr>
    	<th>Action Status:</th>
        <td>Configure the Action Status</td>
        <td><input type="button" name="action_status" id="action_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Risk Types:</th>
        <td>Configure the Risk Types</td>
        <td><input type="button" name="risk_type" id="risk_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Risk Level:</th>
        <td>Configure the Risk Level</td>
        <td><input type="button" name="risk_level" id="risk_level" value="Configure" class="setup_defaults"  /></td>
    </tr>    
     <tr>
    	<th>Risk Categories:</th>
        <td>Configure the Risk Categories</td>
        <td><input type="button" name="risk_categories" id="risk_categories" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Financial Exposure:</th>
        <td>Define the Financial Exposure  </td>
        <td><input type="button" name="financial_exposure" id="financial_exposure" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Impact:</th>
        <td>Define the Impact assessment and rating criteria </td>
        <td><input type="button" name="impact" id="impact" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Likelihood:</th>
        <td>Define the Risk Likelihood assessment and rating criteria</td>
        <td><input type="button" name="likelihood" id="likelihood" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Inherent Risk Exposure:</th>
        <td>Define the Inherent Risk Exposure rating criteria</td>
        <td><input type="button" name="inherent_risk" id="inherent_risk" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Control Effectiveness:</th>
        <td>Define the Control Effectiveness rating criteria</td>
        <td><input type="button" name="control" id="control" value="Configure"  class="setup_defaults" /></td>
    </tr> 
    <tr>
    	<th>Residual Risk Exposure:</th>
        <td>Define the Residual Risk Exposure rating criteria</td>
        <td><input type="button" name="residual" id="residual" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>(Sub-)Directorate Structure:</th>
        <td>Configure the (Sub-)Directorate Structure and responsibilities </td>
        <td><input type="button" name="setup_dir" id="setup_dir" value="Configure" class="setup_defaults"  /></td>
    </tr>	
<!--    <tr>
    	<th>Module Defaults:</th>
        <td>Configure the Module Defaults</td>
        <td><input type="button" name="default_setting" id="default_setting" value="Configure" class="setup_defaults"  /></td>
    </tr>  -->
    <tr>
    	<th><?php echo $glossary; ?>:</th>
        <td>Configure the data to be shown on the <?php echo $glossary; ?>.</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>List Columns:</th>
        <td>Configure how the columns display on the list pages.</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>    
</table>

