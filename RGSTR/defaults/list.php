<?php
	$scripts = array('menu.js', "201a.js","jscolor.js", 'functions.js' );
$styles = array();
$folder = "defaults";
$page = "default";
$page_title = true;

$page_title_src = $_REQUEST['id'];
$page_action = "LIST";

require_once("../inc/header.php");

$db = new ASSIST_DB();

require_once 'inc_list.php';

/* ASSUMPTIONS:
		STATUS:
			active = 1
			inactive = 2
			deleted = 0
		DEFAULTS:
			defaults = 1
*/

//ASSIST_HELPER::arrPrint($_REQUEST);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<style type=text/css>
table tr.active0 td {
	color: #555555;
	background-color: #dedede;
}
table tr.active1 td {
}
table#tbl_container, table#tbl_container td {
	border: 0px solid #ffffff;
	padding: 0px;
}
table#tbl_data, table#tbl_data td {
	border: 1px solid #ababab;
	padding: 5px;
}
</style>
<table id=tbl_container style='margin-right: 320px'><tr><td>
<form name=frm_add>
	<table id=tbl_data>
		<tr>
		<?php
		foreach($fields as $key => $f) {
			echo "<th ".($f['type']=="TEXT" ? "style='width:300px'" : "").">".$f['heading']."</th>";
		}
		?>
			<th></th>
		</tr>
		<tr>
		<?php
		foreach($fields as $key => $f) {
			echo "<td ".($f['type']=="TEXT" ? "style='width:300px'" : "").">";
			switch($f['type']) {
				case "REF": echo "<div class=center>#</div>"; break;
				case "NUM":
					echo "<input type=text size=3 maxlen=".$f['maxlen']." name='".$f['db']."' id='".$f['db']."' value='".$f['value']."' class='NUM require_me' />&nbsp;".$f['postfix'];
					break;
				case "RATING":
					echo "<div class=center><input type=text size=3 maxlen=".$f['maxlen']." name='".$f['db']."_from' id='".$f['db']."_from' value='".$f['value']."' class=require_me />".($page_title_src=="residual_risk_exposure" ? ".00" : "")."
					<br />to<br /><input type=text size=3 maxlen=".$f['maxlen']." name='".$f['db']."_to' id='".$f['db']."_to' value='".$f['value']."' class=require_me />".($page_title_src=="residual_risk_exposure" ? ".99" : "")."</div>";
					break;
				case "CODE":
				case "VARCHAR":	echo "<input type=text size=".($f['type']=="CODE" ? "10" : "40")." maxlen=".$f['maxlen']." name='".$f['db']."' id='".$f['db']."' value='".$f['value']."' class=require_me />"; break;
				case "TEXT":	echo "<textarea cols=30 rows=5 name='".$f['db']."' id='".$f['db']."' class=require_me>".$f['value']."</textarea>"; break;
				case "COLOUR":	echo "<input type=text class='color require_me' style='text-align: center;' value=".$f['value']." name='".$f['db']."' id='".$f['db']."' />"; break;
				case "SELECT":
					echo "<select class=require_me name='".$f['db']."' id='".$f['db']."'>";
						echo "<option selected value=X>--- SELECT ---</option>";
						foreach($f['list_items'] as $i) {
							echo "<option value=".$i['id'].">".$i['name']."</option>";
						}
					echo "</select>";
					break;
				case "STATUS": 
				default:
					break;
			}
			echo "</td>";
		}
		?>
			<td><input type=button value=Add class='isubmit btn_add' /></td>
		</tr>
		<?php
	foreach($rows as $r) {
		$r[$key] = stripslashes($r[$key]);
		echo "
		<tr class=active".($r['active']==1 ? "1" : "0").">";
		foreach($fields as $key => $f) {	
			echo "<td class='".$f['class']."'>";
			switch($f['type']) {
				case "STATUS":
					echo "<div class='center'>".($r['active']==1 ? "Active" : "Inactive")."</div>";
					break;
				case "COLOUR":
					echo "<div class=center style='background-color: #".$r[$key]."'>".$r[$key]."</div>";
					break;
				default:
					echo str_replace(chr(10),"<br />",stripslashes($r[$key])); break;
			}
			echo "</td>";
		}		
		//buttons
		echo "
			<td>";
			if($r['defaults']==1) {
				echo "<input type=button value=Edit class=btn_edit id=".$r[$primary_key]." /> (*)";
			} elseif($r['active']==$inactive_status) {
				echo "<input type=button value=Restore class=btn_restore id=".$r[$primary_key]."  />";
			} else {
				echo "<input type=button value=Edit class=btn_edit id=".$r[$primary_key]."  />";
				echo "<input type=button value=Deactivate class=btn_deactivate id=".$r[$primary_key]."  />";
			}
		echo "
			</td>
		</tr>
		";
	}
		?>
</table>
</form>
</td></tr><tr><td><?php $me->displayGoBack("default.php"); ?>
			<?php $admire_helper->displayAuditLogLink( $audit_log_link, true); ?></td>
		</tr>
	</table>
	<div style='position: absolute; top: 10px; right: 10px; width: 350px; height: 300px'>
<?php ASSIST_HELPER::displayResult(array("info","Please note: <br /> - When adding a new ".$my_page_title." all fields are required.".($has_defaults ? "<br /> - (*) A system required ".$my_page_title." may not be deactivated or deleted." : "").(isset($comment) ? "<br /> - ".$comment : ""))); ?>
</div>
<script type=text/javascript>
$(function() {
	var my_page_title = '<?php echo $my_page_title; ?>';
	var field = '<?php echo $_REQUEST['id']; ?>';
	$(".btn_edit").click(function() {
		var id = $(this).prop("id");
		if(confirm("Are you sure you wish to edit "+my_page_title+" "+id+"? \n\nAny changes you make might affect any associated "+window.risk_object_name+"s.")==true) {
			document.location.href = 'list_edit.php?field='+field+'&id='+id;
		}
	});
	$(".btn_restore").click(function() {
		var id = $(this).prop("id");
		if(confirm("Are you sure you wish to Restore "+my_page_title+" "+id+"? \n\nRestoring will make the "+my_page_title+" available for use within the module.")==true) {
			var dta = "field="+field+"&action=RESTORE&id="+id;
			var result = doAjax("controller.php",dta);
			document.location.href = 'list.php?id='+field+'&r[]='+result[0]+'&r[]='+result[1];
		}
	});
	$(".btn_deactivate").click(function() {
		var id = $(this).prop("id");
		if(confirm("Are you sure you wish to Deactivate "+my_page_title+" "+id+"? \n\nDeactivating will prevent the "+my_page_title+" from being used within the module but will not affect any associated "+window.risk_object_name+"s.")==true) {
			var dta = "field="+field+"&action=DEACTIVATE&id="+id;
			var result = doAjax("controller.php",dta);
			document.location.href = 'list.php?id='+field+'&r[]='+result[0]+'&r[]='+result[1];
		}
	});
	$(".btn_add").click(function() {
		//alert("adding ");
		var err = false;
		$(".require_me").each(function() {
			if($(this).val().length==0 || $(this).val()=="X") {
				//alert($(this).prop("id"));
				err = true;
			}
		});
		if(err) {
			alert("Please ensure that all fields are completed.");
		} else {
			$frm = $("form[name=frm_add]");
			var dta = "field="+field+"&action=ADD&"+$frm.serialize();
			//alert(dta);
			var result = doAjax("controller.php",dta);
			document.location.href = 'list.php?id='+field+'&r[]='+result[0]+'&r[]='+result[1];
		}
	});
});

function doAjax(url,dta) {
	var result;
	//alert("ajax");
	$.ajax({                                      
		url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
		success: function(d) { 
			//alert("d "+d[1]); 
			//console.log(d);
			result=d; 
			//return d;
		},
		error: function(d) { console.log(d); result=d; }
	});
	//alert("r "+result[1]);
	return result;
}

</script>