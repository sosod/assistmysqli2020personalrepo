<?php

$page = "default";
$folder = "defaults";
$page_title = true;
$page_title_src = "risk_owner";


$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
require_once("../inc/header.php");

$db = new ASSIST_DB();

$dbref = strtolower($db->getDBRef());

$var = $_REQUEST;

if(!isset($_REQUEST['act']) || $_REQUEST['act']!="SUB") {

	$sql = "SELECT dirid as id, dirtxt as txt FROM ".$dbref."_dir WHERE active = true ORDER BY dirsort";
	$dir = $db->mysql_fetch_all($sql);
	$act = "ORDER";
	$url = "risk_owner.php";
	$title = "Display Order";
} else {
	$sql = "SELECT dirid as id, dirtxt as txt FROM ".$dbref."_dir WHERE dirid = ".$_REQUEST['d'];
	$x = $db->mysql_fetch_one($sql);

	$sql = "SELECT subid as id, subtxt as txt FROM ".$dbref."_dirsub WHERE active = true AND subdirid = ".$_REQUEST['d']." AND subhead <> 'Y' ORDER BY subsort";
	$dir = $db->mysql_fetch_all($sql);
	$act = "SUB_ORDER";
	$url = "risk_owner_edit.php";
	$title = "Display Order: ".$x['txt'];
}
//include("inc_db_con.php");
//$dir = array();
//while($row = mysql_fetch_assoc($rs)) {
//	$dir[] = $row;
//}
//mysql_close($con);

?>
<script type=text/javascript>
function Validate(me) {
	return true;
}
$(function(){
	$( "#sortable" ).sortable({
		placeholder: "ui-state-highlightsort"
	});
	$( "#sortable" ).disableSelection();
});
</script>
<h2><?php echo $title; ?></h2>
<form name=edit method=post action=<?php echo $url; ?> onsubmit="return Validate(this);">
	<input type=hidden name=act value=<?php echo $act; ?> />
	<input type=hidden name=dirid value=<?php echo isset($_REQUEST['d']) ? $_REQUEST['d'] : ""; ?> />
	<table width=650>
		<tr>
			<th valign=top width=100><?php echo ($act=="SUB_ORDER" ? "Sub " : "").$my_page_title; ?>:</th>
			<td valign=top><ul id=sortable>
			<?php
				foreach($dir as $d) {
					echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$d['id']."\">".$d['txt']."</li>";
				}
			?>
			</ul></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top><input type=submit value="Save Changes" /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<?php
//$urlback = "setup_dir.php";
//include("inc_goback.php");
$me->displayGoBack("risk_owner.php","");
?>
