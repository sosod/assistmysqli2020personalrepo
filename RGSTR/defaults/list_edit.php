<?php
	$scripts = array('menu.js', "201a.js","jscolor.js", 'functions.js' );
$styles = array();
$folder = "defaults";
$page = "default";
$page_title = true;

$page_title_src = $_REQUEST['field'];
$page_action = "EDIT";

require_once("../inc/header.php");

$db = new ASSIST_DB();

require_once 'inc_list.php';


/* ASSUMPTIONS:
		STATUS:
			active = 1
			inactive = 2
			deleted = 0
		DEFAULTS:
			defaults = 1
*/

//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($row);

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<style type=text/css>
 table tr.active0 td {
	color: #555555;
	background-color: #dedede;
}
table tr.active1 td {
}
table#tbl_container, table#tbl_container td {
	border: 0px solid #ffffff;
	padding: 0px;
}
table#tbl_data, table#tbl_data td {
	border: 1px solid #ababab;
	padding: 5px;
} 
</style>
<table id=tbl_container ><tr><td>
<form name=frm_edit>
	<table id=tbl_data>
<?php
foreach($fields as $key => $f) {
		echo "
			<tr>
				<th>".$f['heading'].":</th>
				<td>";
				if(isset($row[$key])) { $row[$key] = stripslashes($row[$key]); }
				switch($f['type']) {
					case "REF": echo $row[$key]; break;
					case "NUM":
						$v = str_replace($f['postfix'],"",$row[$key]);
						echo "<input type=text size=3 maxlen=".$f['maxlen']." name='".$f['db']."' id='".$f['db']."' value='".$v."' class='NUM require_me' />&nbsp;".$f['postfix'];
						break;
					case "RATING":
						echo "<input type=text size=3 maxlen=".$f['maxlen']." name='".$f['db']."_from' id='".$f['db']."_from' value='".$row[$key."_from"]."' class=require_me />".($page_title_src=="residual_risk_exposure" ? ".00" : "")."
						&nbsp;to&nbsp;<input type=text size=3 maxlen=".$f['maxlen']." name='".$f['db']."_to' id='".$f['db']."_to' value='".$row[$key."_to"]."' class=require_me />".($page_title_src=="residual_risk_exposure" ? ".99" : "")."";
						break;
					case "CODE":
					case "VARCHAR":	
						if($row['defaults']!=1 || !$f['is_default']) {
							if(strpos($row[$key],'"')>0) {
								$row[$key] = str_replace('"','|-|',$row[$key]);
							}
							echo "<input type=text size=".($f['type']=="CODE" ? "10" : "50")." maxlen=".$f['maxlen']." name='".$f['db']."' id='".$f['db']."' value=\"".$row[$key]."\" class=require_me />";
						} else {
							echo $row[$key]."<input type=hidden size=50 maxlen=".$f['maxlen']." name='".$f['db']."' id='".$f['db']."' value='".$row[$key]."' />";;
						}
						break;
					case "TEXT":	echo "<textarea cols=50 rows=5 name='".$f['db']."' id='".$f['db']."' class=require_me>".$row[$key]."</textarea>"; break;
					case "COLOUR":	echo "<input type=text class='color require_me' style='text-align: center;' value=".$row[$key]." name='".$f['db']."' id='".$f['db']."' />"; break;
					case "SELECT":
						echo "<select class=require_me name='".$f['db']."' id='".$f['db']."'>";
							echo "<option value=X>--- SELECT ---</option>";
							foreach($f['list_items'] as $i) {
								echo "<option ".($row[$key]==$i['id'] ? "selected": "")." value=".$i['id'].">".$i['name']."</option>";
							}
						echo "</select>";
						break;
					case "STATUS": echo $row[$key]==1 ? "Active" : "Inactive"; break;
					default:
						break;
				}
		echo "</td>
			</tr>
		";
}
?>
		<tr>
			<th></th>
			<td width=350px>
				<input type=button value='Save Changes' class=isubmit id=save_edit />
				<input type=button value=Cancel id=cancel_edit />
				<?php if($count==0) { ?><span class=float><input type=button value=Delete class=idelete id=del_row /></span><?php } ?>
			</td>
		</tr>
	</table>
</form>
	

</td></tr><tr><td><?php $me->displayGoBack("list.php?id=".$page_title_src); ?>
			</td>
		</tr>
	</table>
	<div style='position: absolute; top: 75px; right: 10px; width: 300px;'>
<?php ASSIST_HELPER::displayResult(array("info","Please note:<br /> - All fields are required.".($has_defaults && $row['defaults']==1 ? "<br /> - A(n) ".$my_page_title." may only be deleted if there are no ".$risk_object_name."s / ".$action_object_name."s associated with it or it is not a system required ".$my_page_title."." : ""))); ?>
</div>
<script type=text/javascript>
$(function() {
	var my_page_title = '<?php echo $my_page_title; ?>';
	var field = '<?php echo $page_title_src; ?>';
	var id = '<?php echo $id; ?>';
	
	$("input:text").each(function() {
		var v = $(this).val();
		v2 = AssistString.explode("|-|",v);
		v3 = AssistString.implode('"',v2);
		$(this).val(v3);
	});
	
	$("#save_edit").click(function() {
		var err = false;
		$(".require_me").each(function() {
				//alert($(this).prop("id"));
			if($(this).val().length==0) {
				err = true;
			}
		});

		if(err) {
			alert("Please ensure that all fields are completed.");
		} else {
			$frm = $("form[name=frm_edit]");
			var dta = "field="+field+"&action=EDIT&id="+id+"&"+$frm.serialize();
			//alert(dta);
			var result = doAjax("controller.php",dta);
			document.location.href = 'list.php?id='+field+'&r[]='+result[0]+'&r[]='+result[1];
		}
	});
	
	$("#cancel_edit").click(function() {
		history.back();
	});
	
	$("#del_row").click(function() {
		if(confirm("Are you sure you wish to delete this "+my_page_title+"?")==true) {
			var dta = "field="+field+"&action=DELETE&id="+id;
			//alert(dta);
			var result = doAjax("controller.php",dta);
			document.location.href = 'list.php?id='+field+'&r[]='+result[0]+'&r[]='+result[1];
		}
	});
	
});

function doAjax(url,dta) {
	var result;
	//alert("ajax");
	$.ajax({                                      
		url: url, 		  type: 'POST',		  data: dta,		  dataType: 'json', async: false,
		success: function(d) { 
			//alert("d "+d[1]); 
			//console.log(d);
			result=d; 
			//return d;
		},
		error: function(d) { console.log(d); result=d; }
	});
	//alert("r "+result[1]);
	return result;
}

</script>