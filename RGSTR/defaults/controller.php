<?php
/*session_start();
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../../".strtolower($classname).".php" ) ) {
		require_once( "../../".strtolower($classname).".php" );		
	}else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")) {
		require_once( "../../library/dbconnect/".strtolower($classname).".php" );		
	} else {
		require_once( "../../library/".strtolower($classname).".php" );		
	}
}*/
require_once "../inc/init.php";

//require_once '../../library/dbconnect/dbconnect.php';
//require_once '../class/riskstatus.php';
//require_once '../class/logs.php';

/* REQUIRED FIELDS:
		action = RESTORE, DELETE, DEACTIVATE, EDIT, ADD
		field = which list
		id = list item id
*/

$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";
$field = isset($_REQUEST['field']) ? $_REQUEST['field'] : $action;

switch($field) {
case "getHeaderNames":
		$nm = new Naming();
		echo json_encode( $nm -> getAllHeaderNames() );
 		break;		
/******
     NEW CODE !!!
	      ******/
case "risk_status":
	$obj = new RiskStatus();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,RISKSTATUS::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,RISKSTATUS::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addStatus($_REQUEST['name'],$_REQUEST['client_terminology'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,RISKSTATUS::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editStatus( $id,$_REQUEST['name'],$_REQUEST['client_terminology'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "action_status":
	$obj = new ActionStatus();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,ACTIONSTATUS::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,ACTIONSTATUS::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addStatus($_REQUEST['name'],$_REQUEST['client_terminology'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,ACTIONSTATUS::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editStatus( $id,$_REQUEST['name'],$_REQUEST['client_terminology'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "risk_type":
	$obj = new RiskType();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,RiskType::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,RiskType::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['shortcode'],$_REQUEST['name'],$_REQUEST['description']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,RiskType::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['shortcode'], $_REQUEST['name'],$_REQUEST['description']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "risk_level":
	$obj = new RiskLevel();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,RiskLevel::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,RiskLevel::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['shortcode'],$_REQUEST['name'],$_REQUEST['description']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,RiskLevel::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['shortcode'], $_REQUEST['name'],$_REQUEST['description']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "risk_category":
	$obj = new RiskCategory();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,RiskCategory::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,RiskCategory::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['shortcode'],$_REQUEST['name'],$_REQUEST['description'],$_REQUEST['type_id']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,RiskCategory::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['shortcode'], $_REQUEST['name'],$_REQUEST['description'],$_REQUEST['type_id']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "financial_exposure":
	$obj = new FinancialExposure();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,FinancialExposure::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,FinancialExposure::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['exp_from'],$_REQUEST['exp_to'],$_REQUEST['name'],$_REQUEST['definition'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,FinancialExposure::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['exp_from'],$_REQUEST['exp_to'], $_REQUEST['name'],$_REQUEST['definition'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "impact":
	$obj = new Impact();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,Impact::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,Impact::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['rating_from'],$_REQUEST['rating_to'],$_REQUEST['assessment'],$_REQUEST['definition'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,Impact::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['rating_from'],$_REQUEST['rating_to'], $_REQUEST['assessment'],$_REQUEST['definition'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
case "likelihood":
	$obj = new Likelihood();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,Likelihood::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,Likelihood::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['rating_from'],$_REQUEST['rating_to'],$_REQUEST['assessment'],$_REQUEST['definition'],$_REQUEST['color'],$_REQUEST['probability']."%");
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,Likelihood::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['rating_from'],$_REQUEST['rating_to'], $_REQUEST['assessment'],$_REQUEST['definition'],$_REQUEST['color'],$_REQUEST['probability']."%");
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "inherent_risk_exposure":
	$obj = new InherentRisk();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,InherentRisk::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,InherentRisk::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['rating_from'],$_REQUEST['rating_to'],$_REQUEST['magnitude'],$_REQUEST['response'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,InherentRisk::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['rating_from'],$_REQUEST['rating_to'], $_REQUEST['magnitude'],$_REQUEST['response'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "percieved_control_effective":
	$obj = new ControlEffectiveness();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,ControlEffectiveness::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,ControlEffectiveness::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['rating'],$_REQUEST['effectiveness'],$_REQUEST['qualification_criteria'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,ControlEffectiveness::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['rating'],$_REQUEST['effectiveness'], $_REQUEST['qualification_criteria'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "residual_risk_exposure":
	$obj = new ResidualRisk();
	switch($action) {
		case "RESTORE":
			$r = $obj->setStatus( $id,ResidualRisk::ACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ), 				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "DEACTIVATE":
			$r = $obj->setStatus( $id,ResidualRisk::INACTIVE);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made.")			);
			break;
		case "ADD":
			$r = $obj->addItem($_REQUEST['rating_from'],$_REQUEST['rating_to'],$_REQUEST['magnitude'],$_REQUEST['response'],$_REQUEST['color']);
			$result = array(				( $r>0 ? "ok" : "error" ),				( $r>0 ? "Update saved successfully." : "An error occurred while trying to save the update.")			);
			break;
		case "DELETE":
			$r = $obj->setStatus( $id,ResidualRisk::DELETED);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Delete processed successfully." : "No change was made.")			);
			break;
		case "EDIT":
			$r = $obj->editItem( $id,$_REQUEST['rating_from'],$_REQUEST['rating_to'], $_REQUEST['magnitude'],$_REQUEST['response'],$_REQUEST['color']);
			$result = array(				( $r==1 ? "ok" : "info" ),				( $r==1 ? "Update saved successfully." : "No change was made to the edit.")			);
			break;
		default:
			$result = array(				"error",				"Unknown action. Please try again."			);
	}
	break;
case "delRiskManager":
	$uaObj = new UserAccess( "", "", "", "", "", "", "", "", "" );
	$r = $uaObj->setRiskManager( $_REQUEST['id'], 0 );
	if($r>0) {
		$result = array("ok","Risk Manager updated successfully.");
	} else {
		$result = array("error","An error occurred.  Please try again.");
	}
	break;
case "getNonRiskManagerUsers":
	$uaObj = new UserAccess( "", "", "", "", "", "", "", "", "" );
	$result = $uaObj->getNonRiskManagerUsers();
	break;
default:
	$result = array(		"error",		"Unknown list. Please try again."	);
}

echo json_encode($result);




/*class RiskStatus {
	public function __construct() {}
}*/
?>