<?php
$page = "default";
$folder = "defaults";

$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "";
require_once("../inc/header.php");

$namingObj = new Naming();
$names = $namingObj->getReportingNaming();
$m0 = new Menu();
$glossary = $m0->getMenuName("glossary");
//echo $glossary;

$to_be_done = array();

?>
<script language="javascript">
	$(function(){
		$(".setup_defaults").click(function() {
			switch(this.id) {
			case "risk_owner":
			case "menu_heading":
			case "naming":
			case "glossary":
			case "columns":
				document.location.href = this.id+".php?id="+this.id;
				break;
			default:
				document.location.href = "list.php?id="+this.id;
			}
			return false;		  
		});
		//$("table#link_to_page").find("th").css({"text-align":"left"})
	});
</script>
<style type="text/css">
	#div_left, #div_right {
		padding: 0px 20px 20px 20px;
		width: 45%;
		height: 100%;
	}
	#div_container {
		padding: 0px 10px 0px 10px;
	}
</style>
<div id=div_container>
<div id=div_right >
	<h2>Lists</h2>
	<table class="form middle">
	<?php 
	$list_items = array("risk_status","action_status","risk_type","risk_level","risk_category","financial_exposure","impact","likelihood","inherent_risk_exposure","percieved_control_effective","residual_risk_exposure");
	foreach($list_items as $l) {
		echo "
		<tr>
			<th>".$names[$l].":</th>
			<td>Configure the ".$names[$l]." items to be used in the module.
				<span class='float spn_button'><input type='button' name='$l' id='$l' value='Configure' class='setup_defaults'  />".(in_array($l,$to_be_done) ? $l : "")."</span>
			</td>
		</tr>
		";
	}
	?>
	
	</table>
</div>
<div id=div_left >
	<h2>Module Settings</h2>
	<table class="form middle">
	    <tr>
	    	<th>Menu:</th>
	        <td>Define the menu headings to be displayed in the module.
				<span class="float spn_button"><input type="button" name="menu_heading" id="menu_heading" value="Configure" class="setup_defaults"  /></span>
			</td>
	    </tr>
	    <tr>
	    	<th>Naming Convention:</th>
	        <td>Define the naming convention of the columns/fields used in the module. 
				<span class="float spn_button"><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></span>
			</td>
	    </tr>
	    <tr>
	    	<th><?php echo $names['risk_owner']; ?>:</th>
	        <td>Configure the <?php echo $names['risk_owner']; ?> and the associated administrators
				<span class="float spn_button"><input type="button" name="risk_owner" id="risk_owner" value="Configure" class="setup_defaults"  /></span>
			</td>
	    </tr>
	
	<!--    <tr>
	    	<th>Module Defaults:</th>
	        <td>Configure the Module Defaults
				<span class="float spn_button"><input type="button" name="default_setting" id="default_setting" value="Configure" class="setup_defaults"  /></span>
			</td>
	    </tr>  -->
	    <tr>
	    	<th><?php echo $glossary; ?>:</th>
	        <td>Configure the data to be shown on the <?php echo $glossary; ?>.
				<span class="float spn_button"><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></span>
			</td>
	    </tr>
	    <tr>
	    	<th>List Columns:</th>
	        <td>Configure how the columns display on the list pages.
				<span class="float spn_button"><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></span>
			</td>
	    </tr>    
	</table>
</div>

</div> <!-- end div container -->
<script type=text/javascript>
$(function() {
	$("h2").css("margin-top","15px");

    $("input:button").button().css("font-size","75%");
    $("#div_right").css("float","right");
    $("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
    $("table.form").css({"width":"100%"});
    $(window).resize(function() {
    	var x = 0;
    	$("#div_right").children().each(function() {
    		x+=AssistString.substr($(this).css("height"),0,-2)*1;
    	});
    	$("#div_container").css("height",(x+50)+"px");
    });
	$(window).trigger("resize");
});
</script>
