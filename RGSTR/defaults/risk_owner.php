<?php
$page = "default";
$folder = "defaults";
$page_title = true;
$page_title_src = "risk_owner";



$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
//$page_title = "Directorates";
require_once("../inc/header.php");


function logAction ($db, $transaction, $tsql) {
	$today = strtotime(date("Y-m-d H:i:s"));
	$modref = $db->getModRef();
	$tkid = $db->getUserID();
	$transaction = ASSIST_HELPER::code($transaction);
	$tsql = ASSIST_HELPER::code($tsql);
	$sql = "INSERT INTO assist_".$db->getCmpCode()."_log (date, tkid, ref, transaction,tsql) VALUES 
	($today,'$tkid','$modref','".$transaction."','".$tsql."')";
	$db->db_insert($sql);
}



$db = new ASSIST_DB();

$dbref = strtolower($db->getDBRef());

$var = $_REQUEST;
if(!isset($var['act'])) { $var['act'] = ""; }

switch($var['act']) {
	case "ADD":
		if(!isset($var['dir']) || !isset($var['sub1'])) {
			$result[0] = "error";
			$result[1] = "An error occurred and the Directorate could not be created.";
		} else {
			$dirtxt = $var['dir'];
			$dirsort = (isset($var['dsort']) && is_numeric($var['dsort'])) ? $var['dsort'] : 1;
			$sub1 = $var['sub1'];
			$sub = $var['sub'];
			$sql = "INSERT INTO ".$dbref."_dir SET dirtxt = '".code($dirtxt)."', dirsort = $dirsort , active = true";
			$dirid = $db->db_insert($sql);
			logAction ($db, "Added $my_page_title $dirid", $sql);
				//$dirid = mysql_insert_id();
			if(ASSIST_HELPER::checkIntRef($dirid)) {
				//Primary
				$sql = "INSERT INTO ".$dbref."_dirsub (subtxt,active,subdirid,subsort,subhead) VALUES ('".ASSIST_HELPER::code($sub1)."', true, $dirid , 1, 'Y')";
				//Secondary
				foreach($sub as $key => $s) {
					if(strlen($s)>0) {
						$sql.= ", ('".code($s)."', true, $dirid , ".($key+2).", 'N')";
					}
				}
				$db->db_insert($sql);
				logAction ($db, "Added Subs to $my_page_title $dirid", $sql);
				//include("inc_db_con.php");
				$result[0] = "check";
				$result[1] = $my_page_title." '$dirtxt' has been successfully created.";
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred and the $my_page_title could not be created.";
			}
		}
		break;
	case "DEL":
		$dirid = $var['dirid'];
		if(ASSIST_HELPER::checkIntRef($dirid)) {
			$sql = "SELECT dirtxt FROM ".$dbref."_dir WHERE dirid = $dirid";
			$dir = $db->mysql_fetch_one($sql);
			//include("inc_db_con.php");
				//$dir = mysql_fetch_assoc($rs);
			//mysql_close($con);
			//delete directorate
			$sql = "UPDATE ".$dbref."_dir SET active = false WHERE dirid = $dirid";
			$d = $db->db_update($sql);
			logAction ($db, "Deleted $my_page_title $dirid", $sql);
			//include("inc_db_con.php");
				//$d = mysql_affected_rows();
			//delete sub-directorate
			$sql = "UPDATE ".$dbref."_dirsub SET active = false WHERE subdirid = $dirid";
			$s = $db->db_update($sql);
						logAction ($db, "Deleted Subs for $my_page_title $dirid", $sql);

			//include("inc_db_con.php");
				//$s = mysql_affected_rows();
			if($d==0 && $s==0) {
				$result[0] = "info";
				$result[1] = "No change was done.";
			} else {
				$result[0] = "check";
				$result[1] = $my_page_title." '".$dir['dirtxt']."' and its associated Subs have been deleted.";
			}
		} else {
				$result[0] = "error";
				$result[1] = "An error occurred and the $my_page_title could not be deleted.";
		}
		break;
	case "ORDER":
		$sort = $_REQUEST['sort'];
		if(count($sort)>0) {
			$done = 0;
			foreach($sort as $key => $d) {
				$sql = "UPDATE ".$dbref."_dir SET dirsort = $key WHERE dirid = $d";
				//include("inc_db_con.php");
					$done+=$db->db_update($sql);
							logAction ($db, "Changed display order of $my_page_title $d", $sql);
			}
			if($done>0) {
				$result[0] = "check";
				$result[1] = $my_page_title." reordered.";
			} else {
				$result[0] = "info";
				$result[1] = "No change was made.";
			}
		} else {
			$result[0] = "error";
			$result[1] = "An error occurred.  Please go back and try again.";
		}
		break;
	default:
		break;
}




?>
<script type=text/javascript>
function editDir(id) {
    var err = "N";
    if(!isNaN(parseInt(id)) && escape(id) == id)
    {
        id = parseInt(id);
        if(id>0)
        {
            document.location.href = "risk_owner_edit.php?d="+id;
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }
    
    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
function editSub(id) {
    var err = "N";
    if(!isNaN(parseInt(id)) && escape(id) == id)
    {
        id = parseInt(id);
        if(id>0)
        {
            document.location.href = "risk_owner_sub.php?d="+id;
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }

    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
</script>
<?php $me->displayResult(isset($result) ? $result : array()); ?>
<p><input type=button value="Add New" id=btn_add> <input type=button value="Display Order" id=display_order onclick="document.location.href = 'risk_owner_order.php';"></p>
<table cellpadding=3 cellspacing=0 width=650 id=tbl_dir>
	<tr>
		<th class=center width=30>Ref</th>
		<th class=center >Name</th>
		<th class=center >Section</th>
		<th class=center width=50>&nbsp;</th>
	</tr>
	<?php
    $sql = "SELECT * FROM ".$dbref."_dir WHERE active = true ORDER BY dirsort";
	$rows = $db->mysql_fetch_all($sql);
    //include("inc_db_con.php");
    $dirnum = count($rows); //mysql_num_rows($rs);
    if($dirnum==0) {
    ?>
	<tr>
		<td colspan=4>No Directorates available.  Please click "Add New" to add Directorates.</td>
	</tr>
    <?php
    }    else    {
        //while($row = mysql_fetch_array($rs)) {
		foreach($rows as $row) {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
            //include("inc_tr.php");
    ?>
	<tr>
		<th class=center><?php echo($id); ?></th>
		<td><b><?php echo($val); ?></b></td>
		<td><ul style="margin: 2 0 0 40;">
		  <?php
            $sql2 = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = ".$id." AND active = true ORDER BY subhead DESC, subsort ASC";
			$rows2 = $db->mysql_fetch_all($sql2);
            //include("inc_db_con2.php");
                //while($row2 = mysql_fetch_array($rs2)) {
				foreach($rows2 as $row2) {
                    $id2 = $row2['subid'];
                    $val2 = $row2['subtxt'];
                    if($row2['subhead']=="Y") { $val2.="*"; }
            ?>
                    <li><?php echo($val2); ?></li>
            <?php
                }
			//unset($rs2);
            ?></ul></td>
		<td align=center valign=top>
		<input type=button value="Edit <?php echo $my_page_title; ?>" class=btn_edit_dir ref=<?php echo($id); ?> /><br /><input type=button value="Edit Administrators" onclick="document.location.href = 'risk_owner_admin.php?dirid=<?php echo $id; ?>';" /></td>
		</tr>
    <?php
        }
    }
	//unset($rs);
    ?>
</table>
<script type=text/javascript>
	$(function() {
		if($("#tbl_dir tr").size()<=1) {
			$("#display_order").hide();
		}
		$("input:button.btn_edit_dir").click(function() {
			var i = $(this).attr("ref");
			document.location.href = 'risk_owner_edit.php?dirid='+i;
		});
	});
</script>

<div title=Add id=dlg_add>
<h2>Add <?php echo $my_page_title; ?></h2>
<form name=frm_add method=post action=risk_owner.php >
	<input type=hidden name=act value=ADD />
	<input type=hidden name=dsort value=999 />
	<table  width=650 >
		<tr>
			<th width=150>Parent <?php echo $my_page_title; ?>:</th>
			<td><input type=text size="50" name=dir maxlength=50 class=require_me>&nbsp;</td>
			<td width=110><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th >Primary Sub <?php echo $my_page_title; ?>:</th>
			<td><input type=text size="50" name=sub1 maxlength=50 class=require_me>&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th  >Additional Sub <?php echo $my_page_title; ?>:</th>
			<td style='line-height: 150%;'>
				<input type=text size="50" name=sub[] maxlength=50  /><br />
				<input type=text size="50" name=sub[] maxlength=50 class=subs /><br />
				<input type=text size="50" name=sub[] maxlength=50 class=subs /><br />
				<input type=text size="50" name=sub[] maxlength=50 class=subs /><br />
				<input type=text size="50" name=sub[] maxlength=50 class=subs />&nbsp;
			</td>
			<td valign=top><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=button value=" Add " class=isubmit /> <input type=button value=Cancel /></td>
		</tr>
	</table>
</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dlg_add").dialog({
		autoOpen: false,
		modal: true,
		width: "675px"
	});
	$("input:text.subs").css("margin-top","3px");
	$("#btn_add").click(function() {
		$("#dlg_add").dialog("open");
	});
	
	$("#dlg_add input:button.isubmit").click(function() {
		var err = false;
		$("input:text.require_me").each(function() {
			//alert($(this).prop("name"));
			if($(this).val().length==0) {
				err = true;
				$(this).addClass("required");
			} else {	
				$(this).removeClass("required");
			}
			
		});
		if(err) {
			alert("Please complete the required fields.");
		} else {
			$("form[name=frm_add]").submit();
		}
	});
	
});
</script>