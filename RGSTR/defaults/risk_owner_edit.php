<?php
$page = "default";
$folder = "defaults";
$page_title = true;
$page_title_src = "risk_owner";



$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
//$page_title = "Directorates";
require_once("../inc/header.php");

function logAction ($db, $transaction, $tsql) {
	$today = strtotime(date("Y-m-d H:i:s"));
	$modref = $db->getModRef();
	$tkid = $db->getUserID();
	$transaction = ASSIST_HELPER::code($transaction);
	$tsql = ASSIST_HELPER::code($tsql);
	$sql = "INSERT INTO assist_".$db->getCmpCode()."_log (date, tkid, ref, transaction,tsql) VALUES 
	($today,'$tkid','$modref','".$transaction."','".$tsql."')";
	$db->db_insert($sql);
}


$db = new ASSIST_DB();

$dbref = strtolower($db->getDBRef());

$var = $_REQUEST;
if(!isset($var['act'])) { $var['act'] = ""; }

if($var['act']=="EDIT") {
	$dirid = $var['dirid'];
	if(!ASSIST_HELPER::checkIntRef($dirid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		if(strlen($var['dir'])>0 && strlen($var['sub1'])>0) {
			$done = array('D'=>0,'S'=>0);
			//update dir
			$sql = "SELECT * FROM ".$dbref."_dir WHERE dirid = ".$dirid;
			$dir = $db->mysql_fetch_one($sql);
			$sql = "UPDATE ".$dbref."_dir SET dirtxt = '".ASSIST_HELPER::code($var['dir'])."' WHERE dirid = $dirid";
			//include("inc_db_con.php");
			$x = $db->db_update($sql);
			if($x > 0) { $done['D'] = 1; }
			logAction ($db, "Updated $my_page_title $dirid to (".ASSIST_HELPER::code($var['dir']).") from (".$dir['dirtxt'].")", $sql);
			//update primary sub
			$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subhead = 'Y' AND active = true AND subdirid = ".$dirid;
			$sub = $db->mysql_fetch_one($sql);
			$sql = "UPDATE ".$dbref."_dirsub SET subtxt = '".code($var['sub1'])."' WHERE subhead = 'Y' AND subdirid = $dirid AND active = true";
			$mar = $db->db_update($sql);
			logAction ($db, "Updated Primary Sub of $my_page_title $dirid to (".ASSIST_HELPER::code($var['sub1']).") from (".$sub['subtxt'].")", $sql);
			//include("inc_db_con.php");
			if($mar > 0) { $done['S'] = 1; }
			if($done['D']==true && $done['S']==true) {
				$result[0] = "check";
				$result[1] = "Update saved successfully.";
			} elseif($done['D']==true) {
				$result[0] = "check";
				$result[1] = "Update saved successfully.";
			} elseif($done['S']==true) {
				$result[0] = "check";
				$result[1] = "Update saved successfully.";
			} else {
				$result[0] = "info";
				$result[1] = "No changes where made.";
			}
		} else {
			$result[0] = "error";
			$result[1] = "An error occurred.  No change was made.";
		}
	}
} elseif($var['act']=="SUB_EDIT") {
	$subid = $var['subid'];
	if(!ASSIST_HELPER::checkIntRef($subid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subid = $subid ";
		//include("inc_db_con.php");
			//$sub = mysql_fetch_assoc($rs);
		//mysql_close($con);
		$sub = $db->mysql_fetch_one($sql);
		$dirid = $sub['subdirid'];
		if(!ASSIST_HELPER::checkIntRef($dirid)) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$sql = "UPDATE ".$dbref."_dirsub SET subtxt = '".ASSIST_HELPER::code($var['sub'])."' WHERE subid = $subid";
			//include("inc_db_con.php");
			$mar = $db->db_update($sql);
			logAction ($db, "Updated Sub $subid of $my_page_title $dirid to (".ASSIST_HELPER::code($var['sub']).") from (".$sub['subtxt'].")", $sql);
			if($mar>0) {
				$result[0] = "check";
				$result[1] = "Sub $my_page_title '".$var['sub']."' updated.";
			} else {
				$result[0] = "info";
				$result[1] = "No change was made.";
			}
		}
	}
} elseif($var['act']=="SUB_DEL") {
	$subid = $var['subid'];
	if(!ASSIST_HELPER::checkIntRef($subid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subid = $subid ";
		//include("inc_db_con.php");
			$sub = $db->mysql_fetch_one($sql); //mysql_fetch_assoc($rs);
		//mysql_close($con);
		$dirid = $sub['subdirid'];
		if(!ASSIST_HELPER::checkIntRef($dirid)) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$sql = "UPDATE ".$dbref."_dirsub SET active = false WHERE subid = $subid";
			logAction ($db, "Deleted Sub $subid of $my_page_title $dirid", $sql);
			//include("inc_db_con.php");
			//if(mysql_affected_rows()>0) {
			$mar = $db->db_update($sql);
			if($mar>0) {
				$result[0] = "check";
				$result[1] = "Sub $my_page_title '".$sub['subtxt']."' deleted.";
			} else {
				$result[0] = "info";
				$result[1] = "No change was made.";
			}
		}
	}
} elseif($var['act']=="SUB_ADD") {
	$dirid = $var['dirid'];
	if(!ASSIST_HELPER::checkIntRef($dirid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$sql = "SELECT max(subsort) as sub FROM ".$dbref."_dirsub WHERE subdirid = $dirid";
			$sub = $db->mysql_fetch_one($sql); //mysql_fetch_assoc($rs);
		$subsort = (isset($sub['sub']) && is_numeric($sub['sub'])) ? $sub['sub'] + 1 : 2;
		$sql = "INSERT INTO ".$dbref."_dirsub SET subtxt = '".code($var['sub'])."', active = true, subdirid = $dirid, subhead = 'N', subsort = $subsort ";
		$mar = $db->db_insert($sql);
		logAction ($db, "Added Sub $mar to $my_page_title $dirid", $sql);
		if($mar>0) {
			$result[0] = "check";
			$result[1] = "Sub $my_page_title '".$var['sub']."' created.";
		} else {
			$result[0] = "info";
			$result[1] = "No change was made.";
		}
	}
} elseif($var['act']=="SUB_ORDER") {
	$dirid = $var['dirid'];
	$sort = $var['sort'];
	if(!ASSIST_HELPER::checkIntRef($dirid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$lqs[] = "UPDATE ".$dbref."_dirsub SET subsort = 1 WHERE subhead = 'Y' AND subdirid = $dirid";
		foreach($sort as $key => $s) {
			$lqs[] = "UPDATE ".$dbref."_dirsub SET subsort = ".($key+2)." WHERE subid = $s";
		}
		foreach($lqs as $sql) {
			//include("inc_db_con.php");
			$db->db_update($sql);
			logAction ($db, "Changed display order of Sub $s of $my_page_title $dirid", $sql);
		}
		$result[0] = "check";
		$result[1] = "Display order saved.";
	}
} else {
	$dirid = $_REQUEST['dirid'];
	$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
}
if(!ASSIST_HELPER::checkIntRef($dirid)) {
	die("An error has occurred.  Please go back and try again.");
} else {
	$sql = "SELECT * FROM ".$dbref."_dir WHERE dirid = $dirid AND active = true";
	$dir = $db->mysql_fetch_one($sql);
	//include("inc_db_con.php");
		//if(mysql_num_rows($rs)==0) {
		if(count($dir)==0) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			//$dir = mysql_fetch_assoc($rs);
		}
	//mysql_close($con);
	$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = $dirid AND active = true ORDER BY  subsort ASC";
	//include("inc_db_con.php");
	$rows = $db->mysql_fetch_all($sql);
	$subhead = array();
	$sub = array();
	//while($row = mysql_fetch_assoc($rs)) {
	foreach($rows as $row) {
		if($row['subhead'] == "Y") {
			$subhead = $row;
		} else {
			$sub[] = $row;
		}
	}
	//mysql_close($con);
}

?>
<script type=text/javascript>
function Validate(me) {
}

$(function() {
	$("form[name=edit]").submit(function() {
		var err = false;
		$("input:text.require_me").each(function() {
			if($(this).val().length==0) {
				$(this).addClass("required");
				err = true;
			} else {
				$(this).removeClass("required");
			}
		});
		if(err) {
			alert("Please complete the required fields.");
			return !err;
		} else {
			return !err;
		}
	});
});

function dirDel(d) {
	if(isNaN(parseInt(d))) {
		alert("ERROR!\nAn error occurred.  Please refresh the page and try again.");
	} else {
		if(confirm("Are you sure that you wish to delete this <?php echo $my_page_title; ?>?\n\nThis action will also delete all associated Subs\nand make any associated <?php echo $risk_object_name; ?> inaccessible.")==true) {
			document.location.href = "risk_owner.php?act=DEL&dirid="+d;
		}
	}
}
</script>
<h2>Edit</h2>
<?php ASSIST_HELPER::displayResult($result); ?>
<form name=edit method=post action=risk_owner_edit.php>
	<input type=hidden name=act value=EDIT />
	<input type=hidden name=dirid value=<?php echo($dirid); ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=150>Name:</th>
			<td><input type=text size="50" name=dir maxlength=50 value="<?php echo ASSIST_HELPER::decode($dir['dirtxt']); ?>" class=require_me />&nbsp;</td>
			<td width=110><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th >Primary Sub:</th>
			<td><input type=text size="50" name=sub1 maxlength=50 value="<?php echo ASSIST_HELPER::decode($subhead['subtxt']); ?>" class=require_me />&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top >Subs:</th>
			<td valign=top colspan=2><table class=noborder >
			<?php
				foreach($sub as $s) {
					echo "<tr><td class=noborder id=td_".$s['subid'].">".ASSIST_HELPER::decode($s['subtxt'])."</td><td class=\"noborder right\">&nbsp;<input type=button value=\" Edit \" class=btn_edit ref=".$s['subid']." /></td></tr>";
				}
			?>
				<tr><td class=noborder colspan=2><input type=button value="Add New" id=btn_add_sub /> <?php if(count($sub)>1) { ?><input type=button value="Display Order" onclick="document.location.href='risk_owner_order.php?act=SUB&d=<?php echo $dirid; ?>';" /><?php } ?></td></tr>
			</table></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=submit value="Save Changes" class=isubmit /> <input type=reset value="Reset" /> <span class=float><input type=button value=" Delete~" class=idelete onclick="dirDel(<?php echo $dirid; ?>)" /></span></td>
		</tr>
	</table>
</form>
<?php ASSIST_HELPER::displayResult(array("info","Please note:<br />
* This is an automatically generated Sub $my_page_title that is required by the system and cannot be deleted without deleting the entire $my_page_title.<br />
~ This will delete both the $my_page_title and its associated Subs.")); ?>
<?php
//$urlback = "setup_dir.php";
//include("inc_goback.php");
$me->displayGoBack("risk_owner.php");
?>

<div id=dlg_add_sub title="Add Sub">
<p>Please enter the name for the new Sub:</p>
<p><input type=text name=new_sub size=50 maxlength=50 /></p>
<p><input type=button value="Add" class=isubmit /></p>
</div>
<script type=text/javascript>
$(function() {
	$("#dlg_add_sub").dialog({
		autoOpen: false,
		modal: true,
		width: "350px"
	});
	$("#btn_add_sub").click(function() {
		$("#dlg_add_sub").dialog("open");
	});
	$("#dlg_add_sub input:button").click(function() {
		if($("#dlg_add_sub input:text").val().length==0) {
			alert("Please enter the Sub's name.");
		} else {
			var dta = "dirid=<?php echo $dirid; ?>&act=SUB_ADD&sub="+$("#dlg_add_sub input:text").val();
			document.location.href = "risk_owner_edit.php?"+dta;
		}
	});
});
</script>

<div id=dlg_edit_sub title="Edit Sub">
<input type=hidden name=subid value='' id=subid />
<p>Please enter the new name for Sub '<label id=lb_edit></label>':</p>
<p><input type=text name=edit_sub id=edit_sub size=50 maxlength=50 /></p>
<p><input type=button value="Save Changes" class=isubmit /><span class=float><input type=button value=Delete class=idelete /></span></p>
</div>

<script type=text/javascript>
$(function() {
	$("#dlg_edit_sub").dialog({
		autoOpen: false,
		modal: true,
		width: "350px"
	});
	$(".btn_edit").click(function() {
		var i = $(this).attr("ref");
		$("#dlg_edit_sub #subid").val(i);
		var t = $("#td_"+i).html();
		$("#dlg_edit_sub label#lb_edit").html(t);
		$("#dlg_edit_sub").dialog("open");
		$("#dlg_edit_sub #edit_sub").focus();
		$("#dlg_edit_sub #edit_sub").val(t);
	});
	$("#dlg_edit_sub input:button.isubmit").click(function() {
		if($("#dlg_edit_sub #edit_sub").val().length==0) {
			alert("Please enter the Sub's name.");
		} else {
			var dta = "dirid=<?php echo $dirid; ?>&act=SUB_EDIT&subid="+$("#dlg_edit_sub #subid").val()+"&sub="+$("#dlg_edit_sub #edit_sub").val();
			document.location.href = "risk_owner_edit.php?"+dta;
		}
	});
	$("#dlg_edit_sub input:button.idelete").click(function() {
		if(confirm("Are you sure you wish to delete this Sub <?php echo $my_page_title; ?>? \n WARNING: This will make any associated <?php echo $risk_object_name; ?>s inaccessible.")==true) {
			var dta = "dirid=<?php echo $dirid; ?>&act=SUB_DEL&subid="+$("#dlg_edit_sub #subid").val();
			document.location.href = "risk_owner_edit.php?"+dta;
		}
	});
});
</script>