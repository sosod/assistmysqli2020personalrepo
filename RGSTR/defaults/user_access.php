<?php
$folder = "defaults";
$page = "useraccess";
$scripts = array( 'useraccess.js','menu.js', 'functions.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "User Access";
require_once("../inc/header.php");

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
				<form method="post" id="useraccess-form" name="useraccess-form">
				<table border="1" id="useraccess_table" class=list>
				  <tr>
				    <th>Ref</th>
				    <th>User</th>
				    <th>Module Admin</th>
				    <th>Create Risks</th>
				    <th>Create Actions</th>
				    <th>Update All</th>
				    <th>Edit All</th>
				<!-- <th>Update All</th> -->
				    <th>Reports</th>
				    <th>Assurance</th>
				    <th>Setup</th>
				    <th>Action</th>
				  </tr>
				  <tr>
				    <td>#</td>
				    <td>
						<select name="userselect" id="userselect">
				        	<option value="">--select user--</option>
				        </select>
				    </td>
				    <td>
				    	<select id="module_admin" name="module_admin">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="create_risks">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>   
				    <td>
				    	 <select id="create_actions">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>   
				    <td>
				    	 <select id="view_all" name="view_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	<select id="edit_all" name="edit_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <!-- <td>
				    	<select id="update_all" name="update_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td> -->				    
				    <td>
				    	 <select id="reports" name="reports">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="assurance" name="assurance">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>    
				    <td>
				    	 <select id="usersetup" name="usersetup">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td class=lasttd><input type="submit" value="Add" id="setup_user_access" name="setup_user_access" /></td>
				  </tr>
				</table>
				</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "useraccess_logs", true); ?></td>
	</tr>
</table>
</div>
<script type=text/javascript>
$(function() {
	$tr = $("#useraccess_table tr:first").next();
	$tr.children("td").css({"text-align":"center","width":"100px"});
	$tr.children("td:first").css({"width":""});
	$tr.children("td:last").css({"width":""});
	
});
</script>