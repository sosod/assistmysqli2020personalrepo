<?php
$page = "default";
$folder = "defaults";
$page_title = true;
$page_title_src = "risk_owner";



$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
//$page_title = "Directorates";
require_once("../inc/header.php");

function logAction ($db, $transaction, $tsql) {
	$today = strtotime(date("Y-m-d H:i:s"));
	$modref = $db->getModRef();
	$tkid = $db->getUserID();
	$transaction = ASSIST_HELPER::code($transaction);
	$tsql = ASSIST_HELPER::code($tsql);
	$sql = "INSERT INTO assist_".$db->getCmpCode()."_log (date, tkid, ref, transaction,tsql) VALUES 
	($today,'$tkid','$modref','".$transaction."','".$tsql."')";
	$db->db_insert($sql);
}


$db = new ASSIST_DB();

$dbref = strtolower($db->getDBRef());



$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "";
switch($act) {
case "ADD":
	$dirid = $_REQUEST['dirid'];
	$a = $_REQUEST['adminid'];
	$s = $_REQUEST['subid'];
	$sql = "SELECT * FROM ".$dbref."_dir_admins WHERE tkid = '$a' AND type = 'SUB' AND ref = ".$s;
	$test = $db->mysql_fetch_all($sql);
	if(count($test)>0) {
		$result = array("info","Selected Administrator already exists for Sub ".$s);
	} else {
		$sql = "INSERT INTO ".$dbref."_dir_admins SET tkid = '$a', active = 1, type = 'SUB', ref = ".$s;
		$db->db_insert($sql);
		logAction($db,"Added Administrator to Sub ".$s,$sql);
		$result = array("ok","Administrator added successfully.");
	}
	break;
case "DEL":
	$sql = "SELECT * FROM ".$dbref."_dir_admins WHERE id = ".$_REQUEST['admin'];
	$ad = $db->mysql_fetch_one($sql);
	$sql = "UPDATE ".$dbref."_dir_admins SET active = 0 WHERE id = ".$_REQUEST['admin'];
	$db->db_update($sql);
	logAction($db,"Deactivated Administrator (User ID: ".$ad['tkid'].") for Sub ".$ad['ref'],$sql);
	$result = array("ok","Administrator deactivated successfully.");
	break;
case "RESTORE":
	$sql = "SELECT * FROM ".$dbref."_dir_admins WHERE id = ".$_REQUEST['admin'];
	$ad = $db->mysql_fetch_one($sql);
	$sql = "UPDATE ".$dbref."_dir_admins SET active = 1 WHERE id = ".$_REQUEST['admin'];
	$db->db_update($sql);
	logAction($db,"Restored Administrator (User ID: ".$ad['tkid'].") for Sub ".$ad['ref'],$sql);
	$result = array("ok","Administrator Restored successfully.");
	break;
default:
	$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
}

	$dirid = $_REQUEST['dirid'];













if(!ASSIST_HELPER::checkIntRef($dirid)) {
	die("An error has occurred.  Please go back and try again.");
} else {
	$sql = "SELECT * FROM ".$dbref."_dir WHERE dirid = $dirid AND active = true";
	$dir = $db->mysql_fetch_one($sql);
	$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = $dirid AND active = true ORDER BY  subsort ASC";
	$subs = $db->mysql_fetch_all_by_id($sql,"subid");
	$ids = array_keys($subs);
	if(count($ids)>0) {
		$sql = "SELECT a.*, CONCAT(tk.tkname,' ',tk.tksurname) as admin 
				FROM ".$dbref."_dir_admins a
				INNER JOIN assist_".$db->getCmpCode()."_menu_modules_users mmu
				  ON a.tkid = mmu.usrtkid AND mmu.usrmodref = '".$db->getModRef()."'
				INNER JOIN assist_".$db->getCmpCode()."_timekeep tk
				  ON tk.tkid = mmu.usrtkid AND tk.tkstatus = 1
				WHERE a.type = 'SUB' AND a.ref IN (".implode(",",$ids).")
				ORDER BY a.ref, tk.tkname, tk.tksurname";
		$admins = $db->mysql_fetch_all_by_id2($sql,"ref","id");
	} else {
		$admins = array();
	}
}

?>
<script type=text/javascript>
$(function() {
	$("#tbl_admins input:button.DEL").click(function() {
		var s = $(this).attr("sub");
		var a = $(this).attr("admin");
		
		var tk = $("#td_"+s+"_"+a).html();
		if(confirm("Are you sure you wish to deactivate administrator '"+tk+"' for Sub "+s+"?")==true) {
			document.location.href = "risk_owner_admin.php?dirid=<?php echo $dirid; ?>&act=DEL&admin="+a;
		}
	});
	$("#tbl_admins input:button.RESTORE").click(function() {
		var s = $(this).attr("sub");
		var a = $(this).attr("admin");
		
		var tk = $("#td_"+s+"_"+a).html();
		if(confirm("Are you sure you wish to restore administrator '"+tk+"' for Sub "+s+"?")==true) {
			document.location.href = "risk_owner_admin.php?dirid=<?php echo $dirid; ?>&act=RESTORE&admin="+a;
		}
	});
});
</script>
<style type=text/css>
.active0 {
	background-color: #efefef;
	color: #555555;
}
</style>
<h2>Administrators: <?php echo $dir['dirtxt']; ?></h2>
<?php ASSIST_HELPER::displayResult($result); ?>
<p><input type=button value=Add id=addme /></p>
	<input type=hidden name=dirid value=<?php echo($dirid); ?>>
	<table id=tbl_admins>
		<tr>
			<th colspan=2><?php echo $my_page_title; ?></th>
			<th colspan=2>Administrators</th>
		</tr>
	<?php 
	foreach($subs as $s) {
		$a = (array_key_exists($s['subid'],$admins) ? $admins[$s['subid']] : array());
		echo "
		<tr>
			<td class=center rowspan=".count($a).">".$s['subid']."</td>
			<td class=b rowspan=".count($a).">".$s['subtxt']."</td>
			";
		$x = 1;
		foreach($a as $z) {
			if($x>1) { echo "</tr><tr>"; }
			echo "<td class=active".$z['active']." id=td_".$z['ref']."_".$z['id'].">";
				echo $z['admin'];
			echo "</td><td class=active".$z['active'].">";
			switch($z['active']) {
				case 0:
					echo "<input type=button value=Restore class=RESTORE sub=".$z['ref']." admin=".$z['id']." />";
					break;
				case 1:
				default:
					echo "<input type=button value=Deactivate class=DEL sub=".$z['ref']." admin=".$z['id']." />";
					break;
			}
			echo "</td>";
			$x++;
		}
		if($x==1) {
			echo "<td colspan=2>No administrators have been assigned.</td>";
		}
		echo "
		</tr>";
	}
	?></table>
</form>
<?php
$me->displayGoBack("risk_owner.php");



		$sql = "SELECT tk.tkid as id, CONCAT(tk.tkname,' ',tk.tksurname) as name 
				FROM assist_".$db->getCmpCode()."_timekeep tk
				INNER JOIN assist_".$db->getCmpCode()."_menu_modules_users mmu
				  ON tk.tkid = mmu.usrtkid AND mmu.usrmodref = '".$db->getModRef()."'
				WHERE tk.tkstatus = 1
				ORDER BY tk.tkname, tk.tksurname";

$users = $db->mysql_fetch_all_by_id($sql,"id");

?>

<div id=dlg_add title='Add Administrator' />
<table>
	<tr>
		<th>Sub <?php echo $my_page_title; ?>:</th>
		<td><select id=subid><option value=X selected>--- SELECT ---</option><?php
		foreach($subs as $s) {
			echo "<option value=".$s['subid'].">".$s['subtxt']."</option>";
		}
		?></select></td>
	</tr>
	<tr>
		<th>Administrator:</th>
		<td><select id=adminid><option value=X selected>--- SELECT ---</option><?php
		foreach($users as $i => $r) {
			echo "<option value=".$i.">".$r['name']."</option>";
		}
		?></select></td>
	</tr>
	<tr>
		<th></th>
		<td><input type=button value=Save class=isubmit /></td>
	</tr>
</table>

</div>
<script type=text/javascript>
$(function() {
	$("#dlg_add").dialog({
		autoOpen: false,
		modal: true
	});
	$("#addme").click(function() {
		$("#dlg_add").dialog("open");
	});
	$("#dlg_add input:button.isubmit").click(function() {
		var s = $("#dlg_add #subid").val();
		var a = $("#dlg_add #adminid").val();
		if(s.length==0 || s == "X" || a.length==0 || a=="X") {
			alert("Please complete all the fields.");
		} else {
			var url = "risk_owner_admin.php?dirid=<?php echo $dirid; ?>&act=ADD&subid="+s+"&adminid="+a;
			document.location.href = url;
		}
	});
});
</script>