<?php
$page = "default";
$folder = "defaults";
$page_title = false;
$page_title_src = "glossary";


$scripts = array( 'setup_glossary.js', 'functions.js');
$styles = array( 'colorpicker.css' );
//$page_title = "Glossary";
require_once("../inc/header.php");
?>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<div id="glossary_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post">
			<table align="left" id="glossary_table">
				<tr>
			    	<th>Category</th>
			    	<th>Terminology</th>
			    	<th>Explanation</th>   
			    	<th></th>                        
			    </tr>
			    <tr>
			    	<td><textarea name="category" id="category" class="smallbox"></textarea></td>
			    	<td><textarea name="terminolgy" id="terminolgy" class="smallbox"></textarea></td>
			    	<td><textarea name="explanation" id="explanation" class="mainbox"></textarea></td>
			    	<td><input type="submit" name="save_glossary" id="save_glossary" value="Add" /></td>
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", "")?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "glossary_logs" , true); ?></td>
	</tr>
</table>
