<?php
$scripts = array( 'edit_likelihood.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit Likehood";
require_once("../inc/header.php");
$liek 	= new Likelihood("", "", "", "", "","");
$likelihoods =  $liek -> getALikelihood( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#likelihood_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form id="edit-likelihood-form" name="edit-likelihood-form">
<div id="likelihood_message"></div>
<table border="1" id="likelihood_table">
  <tr>
  	
    <th>Ref #:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Rating:</th>
    <td>
   		<input type="text" name="lkrating_from" id="lkrating_from" size="2" value="<?php echo $likelihoods['rating_from']; ?>" /> 
   	to
   		 <input type="text" name="lkrating_to" id="lkrating_to" size="2" value="<?php echo $likelihoods['rating_to']; ?>" />
    </td>
    </tr>
    <tr>
    <th>Assessment:</th>
    <td><input type="text" name="lkassessment" id="lkassessment" value="<?php echo $likelihoods['assessment']; ?>" /></td>
    </tr> 
    <tr>    
    <th>Probability:</th>
    <td><input type="text" name="lkprobability" id="lkprobability" value="<?php echo $likelihoods['probability']; ?>"/></td>
    </tr>
    <tr>    
    <th>Definition:</th>
    <td><textarea cols="30" rows="7" id="lkdefinition" name="lkdefinition"><?php echo $likelihoods['definition']; ?></textarea></td>
    </tr>
    <tr>
    <th>Color:</th>
    <td><input type="text" name="lkcolor" id="llkcolor" class="color" value="<?php echo ($likelihoods['color'] == "" ? "e2ddcf" :$likelihoods['color'] ) ?>"/></td>
    </tr>
  <tr>
      <th>&nbsp;</th>
    <td>
    <input type="hidden" name="likelihood_id" id="likelihood_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="edit_likelihood" id="edit_likelihood" value="Edit" />
    <input type="submit" name="cancel_edit_likelihood" id="cancel_edit_likelihood" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left">
    	<?php $helper->displayGoBack("", ""); ?>
    </td>
    <td class="noborder"></td>
  </tr>
</table>
</form>
</div>
