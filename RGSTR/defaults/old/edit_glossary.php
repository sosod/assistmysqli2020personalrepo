<?php
$scripts = array( 'edit_setup_glossary.js');
$styles = array( );
$page_title = "Setup Glossary";
require_once("../inc/header.php");
$glos = new Glossary();
$term = $glos -> getAGlossary( $_REQUEST['id']);
?>
<script>
$(function(){
		$("table#inherentrisk_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="glossary_messsage"></div>
<table id="edit_glossary_table">
	<tr>
    	<th>Ref #:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
	<tr>
    <th>Category:</th>
        <td><input type="text" name="category" id="category" value="<?php echo $term['category']; ?>" /></td>
    </tr>    
	<tr>
    	<th>Terminology:</th>
        <td><input type="text" name="terminolgy" id="terminolgy" value="<?php echo $term['terminology']; ?>" /></td>
    </tr>    
	<tr>
    	<th>Explanation:</th>
        <td><textarea cols="30" rows="7" name="explanation" id="explanation"><?php echo $term['explanation']; ?></textarea></td>
    </tr>
    <tr>
    	<td></td>
    	<td>
        	<input type="hidden" id="glossary_id"  name="glossary_id" value="<?php echo $_REQUEST['id']; ?>" />
        	<input type="submit" id="edit_glossary"  name="edit_glossary" value="Save Changes" />
        	<input type="submit" id="cancel_glossary"  name="cancel_glossary" value="Cancel" />                        
        </td>        
    </tr>    
    <tr>
    	<td align="left" class="noborder">
        	<?php $helper->displayGoBack("", ""); ?>
        </td>
        <td class="noborder"></td>
    </tr>
</table>
</div>