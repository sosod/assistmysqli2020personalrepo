<?php
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "financial_exposure";

$scripts = array( 'financialexposure.js','menu.js', 'jscolor.js', 'functions.js', 'ignite.textcounter.js' );
$styles = array();
//$page_title = "Set Up Risk Impacts";
require_once("../inc/header.php");
?>
<div>
<?php $helper->JSdisplayResultObj(""); ?>
<div id="financial_exposure_message"></div>
<table class="noborder">	
	<tr>
		<td class="noborder" colspan="2">
			<form id="financial_exposure-form" name="financial_exposure-form">
			<table border="1" id="financial_exposure_table">
			  <tr>
			    <th>Ref</th>
			    <th>Rating</th>
			    <th><?php echo $object_title; ?></th>
			    <th>Definition</th>
			    <th>Colour</th>
			    <th></th>
			    <th>Status</th>    
			    <th></th>
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			   		<input type="text" name="from" id="from" size="5" value="" /> 
			   	to
			   		 <input type="text" name="to" id="to" size="5" value="" />
			    </td>
			    <td>
			    	<textarea name="name" id="name" class="textcounter"></textarea>
			    </td>
			    <td>
			    	<textarea class="mainbox" id="definition" name="definition"></textarea>
			    </td>
			    <td>
			    	<input type="text" name="color" id="color" class="color" value="e2ddcf" />
			    </td>
			    <td><input type="submit" name="add" id="add" value="Add" /></td>
			    <td></td>
			    <td></td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "financial_exposure_logs" , true); ?></td>
	</tr>
</table>
</div>

