<?php
$scripts = array( 'edit_control.js','menu.js','jscolor.js' );
$styles = array();
$page_title = "Edit Control Effectiveness";
require_once("../inc/header.php");
$ihr	= new ControlEffectiveness( "", "", "", "", "");
$ctrl  = $ihr -> getAControlEffectiveness( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#edit_control_table").find("th").css({"text-align":"left"})
});
</script>
    <div>
        <?php $helper->JSdisplayResultObj(""); ?>
    <form id="edit-risk-control-form" name="edit-risk-control-form" method="post">
    <div id="control_message"></div>
    <table border="1" id="edit_control_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Short Code:</th>
        <td><input type="text" name="control_shortcode" id="control_shortcode" value="<?php echo $ctrl['shortcode']; ?>" /></td>
      </tr>
      <tr>
        <th>Control Effectiveness:</th>
        <td><input type="text" name="control_effect" id="control_effect" value="<?php echo $ctrl['effectiveness']; ?>" /></td>
      </tr>
      <tr>
      	<th>Qualification Criteria:</th>
        <td>
        <input type="text" name="quali_critera" id="quali_critera" value="<?php echo $ctrl['qualification_criteria']; ?>" />
        </td>
      </tr>
      <tr>
      	<th>Rating:</th>
        <td>
		<input type="text" id="control_rating" name="control_rating" value="<?php echo $ctrl['rating']; ?>" />
        </td>
      </tr>
      <tr>
      	<th>Color:</th>
        <td>
		<input type="text" id="control_color" name="control_color" class="color" value="<?php echo ($ctrl['color']== "" ? "blue" : $ctrl['color']) ; ?>" />
        </td>
      </tr>
      <tr>   
      	<th></th>
        <td>
        <input type="hidden" name="control_id" id="control_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_control" id="edit_control" value="Edit" />
        <input type="submit" name="cancel_control" id="cancel_control" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td align="left" class="noborder">
			<?php $helper->displayGoBack("",""); ?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
    </div>

