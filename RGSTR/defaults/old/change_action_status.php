<?php
$scripts = array( 'action_status.js','menu.js', 'jscolor.js' );
$styles = array();
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "action_status";
require_once("../inc/header.php");
$actionstatus 	= new ActionStatus( "", "", "" );
$action_status	= $actionstatus  -> getAStatus( $_REQUEST['id'] );	
?>
<h2>Change Status</h2>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="edit-form-action-status" name="edit--action-status">
<div id="editactionstatus_message"></div>
<table border="1" id="editaction_status_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $action_status['id']; ?></td>
   <tr>
    <th>Action Status:</th>
    <td>
    	<select id="action_status_status" name="action_status_status">
        	<option value="1" <?php if( ($action_status['status'] & 1) == 1 ) {?> selected <?php } ?> >Active</option>
        	<option value="0" <?php if( ($action_status['status'] & 1) ==0 ) {?> selected <?php } ?>>Inactive</option>            
        </select>
    </td>
    </tr>
    <tr>
    <th></th>
    <td>
    	<input type="hidden" name="action_status_id" id="action_status_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="changes_actionstatus" id="changes_actionstatus" value=" Update " class="isubmit" />
        <input type="submit" name="cancel_actionstatus" id="cancel_actionstatus" value=" Cancel " class="idelete" />
    </td>
  </tr>
 <tr>
 	<td align="left" class="noborder">
    	<?php $me->displayGoBack("", ""); ?>
    </td>
    <td class="noborder"></td>
 </tr>
</table>
</form>
</div>