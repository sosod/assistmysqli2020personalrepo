<?php
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "risk_type";


$scripts = array( 'risk_types.js','menu.js' , 'functions.js');
$styles = array();
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="risk_type_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="risk-type-form" name="risk-type-form">
			<table border="1" id="risk_type_table">
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th><?php echo $object_title; ?></th>
			    <th>Description</th>
			    <th></th>
			    <th>Status</th>
			    <th></th>    
			  </tr>
			  <tr>
			    <td>#</td>
			    <td><input type="text" name="risktype_shortcode" id="risktype_shortcode" /></td>
			    <td><input type="text" name="risktype" id="risktype" /></td>
			    <td><textarea id="risktype_descri" name="risktype_descri"></textarea></td>
			    <td><input type="submit" name="add_risktype" id="add_risktype" value="Add" /></td>
			    <td></td>
			    <td></td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "types_logs", true); ?></td>
	</tr>
</table>
</div>

