<?php
$page = "default";
$folder = "defaults";
$page_title = true;
$page_title_src = "risk_owner";



$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
//$page_title = "Directorates";
require_once("../inc/header.php");
$dbref = strtolower($dbref);

$var = $_REQUEST;
if(!isset($var['act'])) { $var['act'] = ""; }

switch($var['act']) {
	case "ADD":
		if(!isset($var['dir']) || !isset($var['sub1'])) {
			$result[0] = "error";
			$result[1] = "An error occurred and the Directorate could not be created.";
		} else {
			$dirtxt = $var['dir'];
			$dirsort = (isset($var['dsort']) && is_numeric($var['dsort'])) ? $var['dsort'] : 1;
			$sub1 = $var['sub1'];
			$sub = $var['sub'];
			$sql = "INSERT INTO ".$dbref."_dir SET dirtxt = '".code($dirtxt)."', dirsort = $dirsort , active = true";
			include("inc_db_con.php");
				$dirid = mysql_insert_id();
			if(checkIntRef($dirid)) {
				//Primary
				$sql = "INSERT INTO ".$dbref."_dirsub (subtxt,active,subdirid,subsort,subhead) VALUES ('".code($sub1)."', true, $dirid , 1, 'Y')";
				//Secondary
				foreach($sub as $key => $s) {
					if(strlen($s)>0) {
						$sql.= ", ('".code($s)."', true, $dirid , ".($key+2).", 'N')";
					}
				}
				include("inc_db_con.php");
				$result[0] = "check";
				$result[1] = "Directorate '$dirtxt' has been successfully created.";
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred and the Directorate could not be created.";
			}
		}
		break;
	case "DEL":
		$dirid = $var['dirid'];
		if(checkIntRef($dirid)) {
			$sql = "SELECT dirtxt FROM ".$dbref."_dir WHERE dirid = $dirid";
			include("inc_db_con.php");
				$dir = mysql_fetch_assoc($rs);
			mysql_close($con);
			//delete directorate
			$sql = "UPDATE ".$dbref."_dir SET active = false WHERE dirid = $dirid";
			include("inc_db_con.php");
				$d = mysql_affected_rows();
			//delete sub-directorate
			$sql = "UPDATE ".$dbref."_dirsub SET active = false WHERE subdirid = $dirid";
			include("inc_db_con.php");
				$s = mysql_affected_rows();
			if($d==0 && $s==0) {
				$result[0] = "info";
				$result[1] = "No change was done.";
			} else {
				$result[0] = "check";
				$result[1] = "Directorate '".$dir['dirtxt']."' and its associated Sub-Directorates have been deleted.";
			}
		} else {
				$result[0] = "error";
				$result[1] = "An error occurred and the Directorate could not be deleted.";
		}
		break;
	case "ORDER":
		$sort = $_REQUEST['sort'];
		if(count($sort)>0) {
			$done = 0;
			foreach($sort as $key => $d) {
				$sql = "UPDATE ".$dbref."_dir SET dirsort = $key WHERE dirid = $d";
				include("inc_db_con.php");
					$done+=mysql_affected_rows();
			}
			if($done>0) {
				$result[0] = "check";
				$result[1] = "Directorates reordered.";
			} else {
				$result[0] = "info";
				$result[1] = "No change was made.";
			}
		} else {
			$result[0] = "error";
			$result[1] = "An error occurred.  Please go back and try again.";
		}
		break;
	default:
		break;
}




?>
<script type=text/javascript>
function editDir(id) {
    var err = "N";
    if(!isNaN(parseInt(id)) && escape(id) == id)
    {
        id = parseInt(id);
        if(id>0)
        {
            document.location.href = "setup_dir_edit.php?d="+id;
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }
    
    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
function editSub(id) {
    var err = "N";
    if(!isNaN(parseInt(id)) && escape(id) == id)
    {
        id = parseInt(id);
        if(id>0)
        {
            document.location.href = "setup_dir_sub.php?d="+id;
        }
        else
        {
            err = "Y";
        }
    }
    else
    {
        err = "Y";
    }

    if(err == "Y")
        alert("An error has occurred.\nPlease reload the page and try again.");
}
</script>
<?php displayResult(isset($result) ? $result : array()); ?>
<p><input type=button value="Add New" onclick="document.location.href = 'setup_dir_add.php';"> <input type=button value="Display Order" id=display_order onclick="document.location.href = 'setup_dir_order.php';"></p>
<table cellpadding=3 cellspacing=0 width=650 id=tbl_dir>
	<tr>
		<th class=center width=30>Ref</th>
		<th class=center >Name</th>
		<th class=center >Section</th>
		<th class=center width=50>&nbsp;</th>
	</tr>
	<?php
    $sql = "SELECT * FROM ".$dbref."_dir WHERE active = true ORDER BY dirsort";
    include("inc_db_con.php");
    $dirnum = mysql_num_rows($rs);
    if(mysql_num_rows($rs)==0) {
    ?>
	<tr>
		<td colspan=4>No Directorates available.  Please click "Add New" to add Directorates.</td>
	</tr>
    <?php
    }    else    {
        while($row = mysql_fetch_array($rs)) {
            $id = $row['dirid'];
            $val = $row['dirtxt'];
            //include("inc_tr.php");
    ?>
	<tr>
		<th class=center><?php echo($id); ?></th>
		<td><b><?php echo($val); ?></b></td>
		<td><ul style="margin: 2 0 0 40;">
		  <?php
            $sql2 = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = ".$id." AND active = true ORDER BY subhead DESC, subsort ASC";
            include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2)) {
                    $id2 = $row2['subid'];
                    $val2 = $row2['subtxt'];
                    if($row2['subhead']=="Y") { $val2.="*"; }
            ?>
                    <li><?php echo($val2); ?></li>
            <?php
                }
			unset($rs2);
            ?></ul></td>
		<td align=center valign=top>
		<input type=button value="Edit Directorate" onclick="editDir(<?php echo($id); ?>)"><br /><input type=button value="Edit Administrators" onclick="document.location.href = 'setup_admin_dir_config.php?d=<?php echo $id; ?>';" /></td>
		</tr>
    <?php
        }
    }
	unset($rs);
    ?>
</table>
<script type=text/javascript>
	$(function() {
		if($("#tbl_dir tr").size()<=1) {
			$("#display_order").hide();
		}
	});
</script>