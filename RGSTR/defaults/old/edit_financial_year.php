<?php
$scripts = array( 'edit_financial_year.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit Finicial Year";
require_once("../inc/header.php");
$fnyr  = new FinancialYear( "", "", "" );
$year  = $fnyr -> getAFinYear( $_REQUEST['id']);
//$start = $fnyr -> setToDatePickerFormat( $year['start_date'] );
//$end   = $fnyr -> setToDatePickerFormat( $year['end_date'] );
?>
<script>
$(function(){
	$("table#edit_fin_year").find("th").css({"text-align":"left"})
});
</script>
<div>
<div id="container"></div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="editfinancial_year_message" class="message"></div>
    <table border="1" id="edit_fin_year">  
      <tr>
        <th>Last day of Financial year:</th>
		<td><input type="text" name="edit_last_year" id="edit_last_year" class="date" value="<?php echo $year['last_day']; ?>"></td>	
      </tr>
      <tr>
        <th>Start date:</th>
        <td><input type="text" name="edit_start_date" id="edit_start_date" class="date" value="<?php echo $year['start_date']; ?>" /></td>
      </tr>
      <tr>
        <th>End date:</th>
        <td><input type="text" name="edit_end_date" id="edit_end_date" class="date" value="<?php echo $year['end_date']; ?>" /></td>
      </tr>
      <tr>
        <th>
        <input type="hidden" value="<?php echo $_REQUEST['id']; ?>" name="fini_year_id" id="fini_year_id" />
       </th>
        <td> 
        	<input type="submit" value="Save Changes" id="save_changes_fin_year" />
            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	<input type="submit" value="Cancel" id="cancel_changes_fin_year" />
        </td>        
      </tr>
      <tr>
      	<td align="left" class="noborder">
       		<?php $helper->displayGoBack("", ""); ?>
       </td>
	   <td class="noborder"></td>
      </tr>
    </table>
</div>	
