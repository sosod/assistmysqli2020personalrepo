<?php
$scripts = array( 'edit_inherentrisk.js','menu.js' , 'jscolor.js');
$styles = array();
$page_title = "Edit Inherent Risk";
require_once("../inc/header.php");

$ihr 	= new InherentRisk( "", "", "", "", "");
$inherentrisk =  $ihr -> getAInherentRisk( $_REQUEST['id'] ) ;
?>
<div>
<?php $helper->JSdisplayResultObj(""); ?>
<form id="edit-inherentrisk-form" name="edit-inherentrisk-form">
<div id="inherentrisk_message"></div>
<table border="1" id="inherentrisk_table" cellpadding="5" cellspacing="0">
  <tr>
  	
    <th>Ref #:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Status:</th>
    <td>
   		<select name="inherent_status" id="inherent_status">
        	<option value="1" <?php if($inherentrisk['active']==1) { ?> selected <?php } ?> >Active</option>
        	<option value="0" <?php if($inherentrisk['active']==0) { ?> selected <?php } ?> >InActive</option>            
        </select>
    </td>
    </tr>
  <tr>
      <th>&nbsp;</th>
    <td>
    <input type="hidden" name="inherentrisk_id" id="inherentrisk_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="change_inherentrisk" id="change_inherentrisk" value="Save Changes" />
    <input type="submit" name="cancel_inherentrisk" id="cancel_inherentrisk" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder"><?php $helper->displayGoBack("", ""); ?></td>
  	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>

