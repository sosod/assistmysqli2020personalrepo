<?php
$scripts = array( 'risk_category.js','menu.js' );
$styles = array('' );
$page_title = "Edit Risk Status";
require_once("../inc/header.php");
$typ 		= new RiskType( "", "", "", "");
$types 		= $typ -> getType() ;	
$riskcat 	= new RiskCategory( "", "", "", "" );
$category 	= $riskcat  -> getACategory( $_REQUEST['id']);
?>
<script>
$(function(){
	$("table#edit_riskcategory_table").find("th").css({"text-align":"left"});
});
</script>
<?php $helper->JSdisplayResultObj(""); ?>
    <div>
    <form id="edit-risk-category-form" name="edit-risk-category-form" method="post">
    <div id="editriskcategory_message"></div>
    <table border="1" id="edit_riskcategory_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Short Code:</th>
        <td><input type="text" name="category_shortcode" id="category_shortcode" value="<?php echo stripslashes($category['shortcode'] ); ?>" /></td>
      </tr>
      <tr>
        <th>Risk Category:</th>
        <td><input type="text" name="risk_category" id="risk_category" value="<?php echo stripslashes( $category['name'] ); ?>" /></td>
      </tr>
      <tr>
      	<th>Description of Risk Category:</th>
        <td>
        <textarea cols="30" rows="7" name="category_description" id="category_description"><?php echo stripslashes( $category['description'] ); ?></textarea>
        </td>
      </tr>
      <tr>
      <tr>
      	<th>Risk Type:</th>
        <td>
            <select name="category_risktype" id="category_risktype">
           	 <option value="">--Select risk type--</option>
             <?php foreach($types as $type) {
			 ?>
               <option value="<?php echo $type['id']; ?>"
               	<?php echo (trim($category['type_id'])==trim($type['id']) ? "selected" : "")?>
               ><?php echo $type['name']; ?></option>
             <?php
			 }  ?>
            </select>
       </td>
      </tr>
      <tr>
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="risk_category_id" id="risk_category_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_riskcategory" id="edit_riskcategory" value="Edit" />
        <input type="submit" name="cancel_riskcategory" id="cancel_riskcategory" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td align="left" class="noborder">
        	<?php $helper->displayGoBack("", ""); ?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>

