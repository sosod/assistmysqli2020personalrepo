<?php
$scripts = array( 'edit_risk_type.js','menu.js','jpickermin.js','jpicker-1.1.6.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Edit Risk Types";
require_once("../inc/header.php");
$type 	   = new RiskType( "", "", "", "" );
$risktype  = $type-> getARiskType( $_REQUEST['id'] ) ;	 
?>
<script>
$(function(){
		$("table#editarisk_type_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $helper->JSdisplayResultObj(""); ?>
<form id="edit-form-risk-type" name="edit-form-risk-type">
<div id="edit_risk_type"></div>
<table border="1" id="editarisk_type_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $risktype['id']; ?></td>
   <tr>
    <th>Short Code:</th>
    <td><input type="text" name="risktype_shortcode" id="risktype_shortcode" value="<?php echo $risktype['shortcode']; ?>" /></td>
   </tr>
  <tr>
   <th>Risk Type:</th>
   <td><input type="text" name="risktype" id="risktype" value="<?php echo $risktype['name']; ?>"/></td>
  </tr>
  <tr> 
    <th>Description:</th>
    <td><textarea id="risktype_descri" name="risktype_descri"><?php echo $risktype['description']; ?></textarea></td>
   </tr>
  <tr>  
    <th></th>
    <td>
    	<input type="hidden" name="risktype_id" id="risktype_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="edit_risktype" id="edit_risktype" value="Edit" />
        <input type="submit" name="cancel_risktype" id="cancel_risktype" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left"><?php $helper->displayGoBack("", ""); ?></td>
  	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>
