<?php
$scripts = array( 'financialexposure.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit Impact";
require_once("../inc/header.php");

$financialExposure 	= new FinancialExposure( "", "", "", "", "");
$FinancialExposure =  $financialExposure -> getAFinancialExposure( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#impact_table").find("th").css({"text-align":"left"})
});
</script>
<div>
    <?php $helper->JSdisplayResultObj(""); ?>
<form id="edit-financial_exposure-form" name="edit-financial_exposure-form">
<div id="financial_exposure_message"></div>
<table border="1" id="financial_exposure_table">
  <tr>
  	
    <th>Ref #:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Rating:</th>
    <td>
   		<input type="text" name="from" id="from" size="2" value="<?php echo $FinancialExposure['exp_from']; ?>" /> 
   	to
   		 <input type="text" name="to" id="to" size="2" value="<?php echo $FinancialExposure['exp_to']; ?>" />
    </td>
    </tr>
    <tr>
    <th>Assessment:</th>
    <td><input type="text" name="name" id="name" value="<?php echo $FinancialExposure['name']; ?>" /></td>
    </tr>
    <tr>
    <th>Definition:</th>
    <td><textarea cols="30" rows="7" id="definition" name="definition"><?php echo $FinancialExposure['definition']; ?></textarea></td>
    </tr>
    <tr>
    <th>Colour:</th>
    <td><input type="text" name="color" id="color" class="color" value="<?php echo ($FinancialExposure['color'] == "" ? "e2ddcf" :$FinancialExposure['color'] ) ?>"/></td>
    </tr>
  <tr>
      <th>&nbsp;</th>
    <td>
    <input type="hidden" name="financial_exposure_id" id="financial_exposure_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="edit" id="edit" value="Edit" />
    <input type="submit" name="cancel_financial_exposure" id="cancel_financial_exposure" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left"><?php $helper->displayGoBack("", ""); ?></td>
  	<td></td>
  </tr>
</table>
</form>
</div>

