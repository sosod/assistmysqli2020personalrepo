<?php
$scripts = array( 'edit_control.js','menu.js');
$styles = array();
$page_title = "Edit Control Effectiveness";
require_once("../inc/header.php");
$ihr	= new ControlEffectiveness( "", "", "", "", "");
$ctrl  = $ihr -> getAControlEffectiveness( $_REQUEST['id'] ) ;
?>
  <div>
  <?php $admire_helper->JSdisplayResultObj(""); ?>
    <form id="change-risk-control-form" name="change-risk-control-form" method="post">
    <div id="control_message"></div>
    <table border="1" id="change_control_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Status:</th>
        <td>
        	<select name="control_status" id="control_status">
            	<option value="1" <?php if($ctrl['active']==1) {?> selected="selected" <?php }?>>Active</option>
            	<option value="0" <?php if($ctrl['active']==0) {?> selected="selected" <?php }?>>InActive</option>                
            </select>
        </td>
      </tr>
      <tr>   
      	<th></th>
        <td>
        <input type="hidden" name="control_id" id="control_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="change_control" id="change_control" value="Save Changes" />
        <input type="submit" name="cancel_control" id="cancel_control" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left">
        	<?php displayGoBack("",""); ?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>
