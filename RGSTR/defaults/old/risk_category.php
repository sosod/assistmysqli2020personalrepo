<?php
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "risk_category";

$scripts = array( 'risk_category.js','menu.js', 'functions.js' );
$styles = array();
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="riskcategory_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="risk-category-form" name="risk-category-form">
			<table border="1" id="riskcategory_table">
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th><?php echo $object_title; ?></th>
			    <th>Description of <?php echo $object_title; ?></th>
			    <th><?php echo $nameObj->getAName("risk_type"); ?></th>
			    <th></th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			  <tr>
			    <td>#</td>
			    <td><input type="text" name="category_shortcode" id="category_shortcode" /></td>
			    <td><input type="text" name="risk_category" id="risk_category" /></td>
			    <td>
				   	<textarea cols="30" rows="7" id="category_description" name="category_description"></textarea>
			   </td>
			    <td>
			    <select name="category_risktype" id="category_risktype">
			    	<option value="">--- SELECT ---</option>
			    </select>
			    </td>
			    <td><input type="submit" name="add_riskcategory" id="add_riskcategory" value="Add" /></td>
			   		<td></td>    
			   		
			  	</tr>
			</table>
			</form>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "categories_logs", true); ?></td>		
	</tr>
</table>
</div>

