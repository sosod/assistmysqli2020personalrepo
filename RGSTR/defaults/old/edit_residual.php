<?php
$scripts = array( 'edit_residual.js','menu.js', 'jscolor.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Edit Residial Risk";
require_once("../inc/header.php");
$residual	= new ResidualRisk(  "", "", "", "", "");
$rsd  = $residual -> getAResidualRisk( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#residual_risk_table").find("th").css({"text-align":"left"})
});
</script>
    <div>
    <?php $admire_helper->JSdisplayResultObj(""); ?>
    <form id="edit-residual-form" name="edit-residual-form" method="post">
    <div id="residual_risk_message"></div>
    <table border="1" id="residual_risk_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Risk Rating:</th>
        <td>
        <input type="text" name="residual_risk_from" id="residual_risk_from" size="2" value="<?php echo $rsd['rating_from']; ?>" /> 
   				to
   		 <input type="text" name="residual_risk_to" id="residual_risk_to" size="2" value="<?php echo $rsd['rating_to']; ?>" />
        </td>
      </tr>
      <tr>
        <th>Residual Risk magnitude:</th>
        <td><input type="text" name="residual_magnitude" id="residual_magnitude" value="<?php echo $rsd['magnitude']; ?>" /></td>
      </tr>
      <tr>
      	<th>Response:</th>
        <td>
        <textarea cols="30" rows="7" name="residual_response" id="residual_response"><?php echo $rsd['response']; ?></textarea>
        </td>
      </tr>
      <tr>
      	<th>Colour:</th>
        <td>
		<input type="text" id="residual_color" name="residual_color" class="color" value="<?php echo ($rsd['color']== "" ? "blue" : $rsd['color']) ; ?>" />
        </td>
      </tr>
      <tr>   
      	<th></th>
        <td>
        <input type="hidden" name="residual_id" id="residual_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_residual" id="edit_residual" value="Edit" />
        <input type="submit" name="cancel_residual" id="cancel_residual" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td align="left" class="noborder">
        	<?php $helper->displayGoBack("",""); ?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
</div>

