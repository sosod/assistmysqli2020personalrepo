<?php
$scripts = array( 'action_status.js','menu.js', 'jscolor.js' );
$styles = array();
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "action_status";
require_once("../inc/header.php");
$actionstatus 	= new ActionStatus( "", "", "" );
$action_status	= $actionstatus  -> getAStatus( $_REQUEST['id'] );
?>
<script>
$(function(){
		$("table#editaction_status_table").find("th").css({"text-align":"left"})
});
</script>
<h2>Edit</h2>
<div>
<div id="editactionstatus_message"></div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="edit-form-action-status" name="edit--action-status">
<table border="1" id="editaction_status_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $action_status['id']; ?></td>
   <tr>
    <th>Action Status:</th>
    <td><input type="text" name="actionstatus" id="actionstatus" <?php if(($action_status['status'] & 4) == 4) { ?> disabled="disabled"<?php } ?> value="<?php echo $action_status['name']; ?>" /></td>
   </tr>
  <tr>
   <th>&lt;Client&gt; Terminology:</th>
   <td><input type="text" name="action_client_term" id="action_client_term" value="<?php echo $action_status['client_terminology']; ?>" /></td>
  </tr>
  <tr> 
    <th>Colour:</th>
    <td><input type="text" name="action_color" id="action_color"  class="color"  value="<?php echo ($action_status['color'] == "" ? "e2ddcf" : $action_status['color'] ); ?>"/></td>
   </tr>
  <tr>  
    <th></th>
    <td>
    	<input type="hidden" name="action_status_id" id="action_status_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="edit_actionstatus" id="edit_actionstatus" value="Save Changes" class="isubmit" />
        <input type="submit" name="cancel_actionstatus" id="cancel_actionstatus" value="Cancel" class="idelete" />
    </td>
  </tr>
 <tr>
 	<td class="noborder" align="left">
    	<?php $helper->displayGoBack("", ""); ?>
    </td>
    <td class="noborder"></td>
 </tr>
</table>
</form>
</div>
