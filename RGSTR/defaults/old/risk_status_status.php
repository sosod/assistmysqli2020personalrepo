<?php
$scripts = array( 'change_risk_status.js','menu.js', 'jscolor.js', 'functions.js' );
$styles = array();
//$page_title = "Change Risk Status";
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "risk_status";
require_once("../inc/header.php");
$getstat = new RiskStatus( "", "", "" );
$status =  $getstat -> getAStatus( $_REQUEST['id']) ;	
?>
<h2>Change Status</h2>
    <div>
    <form id="risk-status-form" name="risk-status-form" method="post">
    <div id="risk_status_message"></div>
    <table border="1" id="risk_status_table" cellpadding="5" cellspacing="0">
    <tr>
    	 <th>Ref:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
	<tr>
		<th>Current Status:</th>
		<td><?php echo $status['active']==1 ? "Active" : "Inactive"; ?></td>
	</tr>
      <tr>
        <th>New Status:</th>
        <td>
        	<select name="status_status" id="status_status">
            	<option value="1" <?php if($status['active'] == 1) { ?> selected <?php } ?> >Active</option>
                <option value="0" <?php if($status['active'] == 0) { ?> selected <?php } ?>  >Inactive</option>
            </select>
        </td>
      </tr>
       <tr>
      	<th></th>
        <td>
        <input type="hidden" name="risk_status_id" id="risk_status_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="save_status" id="save_status" value="Save Changes" class=isubmit />
        <input type="submit" name="cancel_status" id="cancel_status" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left"><?php displayGoBack("", ""); ?></td>
      	<td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>
<?php ASSIST_HELPER::displayResult(array("info","Please note:<Br />\"Active\" means that the status is available for use.<br />\"Inactive\" means that the status is not available for use but any ".$risk_object_name." associated with it will still display.")) ?> 
