<?php
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "residual_risk_exposure";

$scripts = array( 'residual_risk.js','menu.js', 'jscolor.js', 'functions.js');
$styles = array();
//$page_title = "Set Up Risk residual risks";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="residual_risk_message"></div>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form method="post" name="form-residual-risk">
			<table border="1" id="residual_risk_table">
			  <tr>
			    <th>Ref</th>
			    <th>Rating</th>
			    <th>Name</th>
			    <th>Response</th>    
			    <th>Colour</th>
			    <th></th>    
			    <th>Status</th>        
			    <th></th>    
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			    	<input type="text" name="residual_risk_from" id="residual_risk_from" size="3" />
			        to
			        <input type="text" name="residual_risk_to" id="residual_risk_to" size="3" /></td>
			    <td><input type="text" name="residual_magnitude" id="residual_magnitude" /></td>
			    <td><textarea cols="30" rows="7" id="residual_response" name="residual_response"></textarea></td>
			    <td><input type="text" name="residual_color" id="residual_color"  class="color" value="abf207" /> </td>
			    <td><input type="submit" name="add_residual" id="add_residual" value="Add" /></td>
			    <td></td>
			    <td></td>        
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("",""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "residual_exposure_logs", true); ?></td>		
	</tr>
</table>
</div>
