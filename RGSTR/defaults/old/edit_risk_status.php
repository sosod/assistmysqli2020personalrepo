<?php
$scripts = array( 'edit_risk_status.js','menu.js', 'jscolor.js' );
$styles = array();
//$page_title = "Edit Risk Status";
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "risk_status";

require_once("../inc/header.php");
$getstat = new RiskStatus( "", "", "" );
$status =  $getstat -> getAStatus( $_REQUEST['id']) ;	
?>
<script>
$(function(){
		$("table#edit_risk_status_table").find("th").css({"text-align":"left"})
});
</script>
<h2>Edit</h2>
    <div>
    <?php $admire_helper->JSdisplayResultObj(""); ?>
    <form id="edit-risk-status-form" name="edit-risk-status-form" method="post">
    <table border="1" id="edit_risk_status_table" cellpadding="5" cellspacing="0">
    <tr>
    	 <th>Ref:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Default Terminology:</th>
        <td><input type="text" name="status" id="status" <?php if($status['defaults'] == 1) { ?> disabled="disabled"<?php } ?>  value="<?php echo $status['name']; ?>" size=50 /></td>
      </tr>
      <tr>
        <th>Your Terminology:</th>
        <td><input type="text" name="client_term" id="client_term" value="<?php echo $status['client_terminology']; ?>" size=50 /></td>
      </tr>
      <tr>
      	<th>Color:</th>
        <td>
         <input name="risktype_color" id="risktype_color" class="color" value="<?php echo ($status['color']=="" ? "66ff00" : $status['color']); ?>">
        <!-- <input type="text" name="color" id="color" value="<?php echo ($status['color']=="" ? "e2ddcf" : $status['color']); ?>" />--> </td>
      </tr>
      <tr>
      	<th></th>
        <td>
			<input type="hidden" name="risk_status_id" id="risk_status_id" value="<?php echo $_REQUEST['id']?>" />
			<input type="submit" name="edit_status" id="edit_status" value="Save Changes" class=isubmit />
			<input type="submit" name="cancel_status" id="cancel_status" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left"><?php $helper->displayGoBack("", ""); ?></td>
      	<td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>