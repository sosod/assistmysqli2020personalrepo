<?php
$scripts = array( 'financialexposure.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit financial exposure";
require_once("../inc/header.php");
$financialExposure 	= new FinancialExposure( "", "", "", "", "");
$FinancialExposure =  $financialExposure -> getAFinancialExposure( $_REQUEST['id'] ) ;
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="change-financial_exposure-form" name="change-financial_exposure-form">
<div id="financial_exposure_message"></div>
<table border="1" id="financial_exposure_table">
  <tr>
    <th>Ref #:</th>
        <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Statuses:</th>
    <td>
    	<select id="financial_exposure_status">
        	<option value="1" <?php if($FinancialExposure['status']==1) { ?> selected <?php } ?> >Active</option>
            <option value="0" <?php if($FinancialExposure['status']==0) { ?> selected <?php } ?> >InActive</option>
        </select>
	</td>
    </tr>
    <tr>
      <th>&nbsp;</th>
    <td>
    <input type="hidden" name="financial_exposure_id" id="financial_exposure_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="change" id="change" value="Edit" />
    <input type="submit" name="cancel_financial_exposure" id="cancel_financial_exposure" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left"><?php $helper->displayGoBack("", ""); ?></td>
  	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>

