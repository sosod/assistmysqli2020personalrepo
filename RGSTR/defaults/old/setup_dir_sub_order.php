<?php
$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");
$dbref = strtolower($dbref);

$var = $_REQUEST;

$id = $_REQUEST['d'];

if(!checkIntRef($id)) {
	die("An error has occurred.  Please go back and try again.");
} else {
	$sql = "SELECT d.dirtxt FROM ".$dbref."_dir d WHERE d.dirid = $id";
	include("inc_db_con.php");
		if(mysql_num_rows($rs)==0) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$dir = mysql_fetch_assoc($rs);
		}
	mysql_close($con);
	$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = $id AND active = true";
	include("inc_db_con.php");
	$sub = array();
	$subhead = array();
	while($row = mysql_fetch_assoc($rs)) {
		if($row['subhead']=="Y") {
			$subhead = $row;
		} else {
			$sub[] = $row;
		}
	}
	mysql_close($con);
}

?>
<script type=text/javascript>
function Validate(me) {
	return true;
}
$(function(){
	$( "#sortable" ).sortable({
		placeholder: "ui-state-highlightsort"
	});
	$( "#sortable" ).disableSelection();
});
</script>
<h3>Order Sub-Directorates</h3>
<form name=edit method=post action=setup_dir_edit.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=SUB_ORDER />
	<input type=hidden name=dirid value=<?php echo($id); ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=200>Directorate:</th>
			<td><?php echo decode($dir['dirtxt']); ?>&nbsp;</td>
		</tr>
		<tr>
			<th >Primary Sub-Directorate:</th>
			<td><?php echo decode($subhead['subtxt']); ?>&nbsp;</td>
		</tr>
		<tr>
			<th valign=top >Sub-Directorates:</th>
			<td valign=top><ul id=sortable>
			<?php
				foreach($sub as $s) {
					echo "<li class=\"ui-state-default\" style=\"cursor:hand;\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>&nbsp;<input type=hidden name=sort[] value=\"".$s['subid']."\">".$s['subtxt']."</li>";
				}
			?>
			</ul></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top><input type=submit value="Save Changes" /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<?php
$urlback = "setup_dir_edit.php?d=".$id;
include("inc_goback.php");
?>
