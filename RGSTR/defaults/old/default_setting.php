<?php
$scripts = array( 'defaults.js','menu.js', 'functions.js', 'functions.js');
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Set Up Risk residual risks";
require_once("../inc/header.php");
?>
<?php $helper->JSdisplayResultObj(""); ?>
<div id="defaults_message"></div>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<form method="post" id="defaults-form" name="defaults-form">
			<table border="1" id="defaults_table">
			  <tr>
			    <td>Ref</td>
			    <td>Defaults to be applied</td>
			    <td>To be applied</td>
			    <td>Action</td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $helper->displayGoBack("",""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "defaults_logs" , true); ?></td>
	</tr>
</table>


