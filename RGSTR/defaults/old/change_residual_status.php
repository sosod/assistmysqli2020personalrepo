<?php
$scripts = array( 'edit_residual.js','menu.js', 'jscolor.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Change Residial Risk Status";
require_once("../inc/header.php");
$residual	= new ResidualRisk(  "", "", "", "", "");
$rsd  = $residual -> getAResidualRisk( $_REQUEST['id'] ) ;
?>
    <div>
    <?php $admire_helper->JSdisplayResultObj(""); ?>
    <form id="edit-residual-form" name="edit-residual-form" method="post">
    <div id="residual_risk_message"></div>
    <table border="1" id="residual_risk_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Status:</th>
        <td>
        	<select name="residual_status" id="residual_status">
            	<option value="1" <?php if($rsd['active']==1) { ?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if($rsd['active']==0) { ?> selected="selected" <?php } ?>>InActive</option>                
            </select>
        </td>
      </tr>
     <tr>   
      	<th></th>
        <td>
        <input type="hidden" name="residual_id" id="residual_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="change_residual" id="change_residual" value="Save Changes" />
        <input type="submit" name="cancel_residual" id="cancel_residual" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td align="left" class="noborder">
        	<?php displayGoBack("",""); ?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
 </div>
