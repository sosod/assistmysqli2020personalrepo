<?php
$scripts = array( 'risk_level.js','menu.js' );
$styles = array();
$page_title = "Change Risk Level Status";
require_once("../inc/header.php");
$level 	   = new RiskLevel( "", "", "", "" );
$risklevel  = $level-> getARiskLevel( $_REQUEST['id'] ) ;
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="edit-form-risk-level" name="edit-form-risk-level">
<div id="change_risk_level_message"></div>
<table border="1" id="editarisk_level_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $risklevel['id']; ?></td>
   <tr>
    <th>Statuses:</th>
    <td>
    	<select id="level_statuses" name="type_statuses">
        	<option value="1" <?php if($risklevel['status']==1 ) { ?> selected <?php } ?> >Active</option>
            <option value="0" <?php if($risklevel['status']==0 ) { ?> selected <?php } ?> >InActive</option>
        </select>
    </td>
   </tr>
   <tr>
    <th></th>
    <td>
    	<input type="hidden" name="risklevel_id" id="risklevel_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="changes_risklevel" id="changes_risklevel" value="Save Changes" />
        <input type="submit" name="cancel_risklevel" id="cancel_risklevel" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left"><?php displayGoBack("", ""); ?></td>
  	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>

