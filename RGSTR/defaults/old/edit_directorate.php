<?php
$scripts = array( 'edit_directorate.js','menu.js');
$styles = array();
$page_title = "Edit Risk Status";
require_once("../inc/header.php");
$uaccess 	= new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		= $uaccess -> getUserAccess() ;
$dir 		= new Directorate( "", "", "" ,"");
$directorate=$dir -> getADirectorate( $_REQUEST['id'] );
?>
<script>
$(function(){
		$("table#edit_directorate_table").find("th").css({"text-align":"left"})
});
</script>
    <div>
    <form id="edit-directorate-form" name="edit-directorate-form" method="post">
    <div id="directorate_message"></div>
    <table border="1" id="edit_directorate_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Short Code:</th>
        <td><input type="text" name="directorate_shortcode" id="directorate_shortcode" value="<?php echo $directorate['shortcode']; ?>" /></td>
      </tr>
      <tr>
        <th>Department:</th>
        <td><input type="text" name="dr_department" id="dr_department" value="<?php echo $directorate['department']; ?>" /></td>
      </tr>
      <tr>
      	<th>Department Function:</th>
        <td>
       <textarea name="dept_function" id="dept_function"><?php echo $directorate['dept_function']; ?></textarea>
        </td>
      </tr>
      <tr>
      <tr>
      	<th>Responsible for directorate:</th>
        <td>
            <select name="dr_users_respo" id="dr_users_respo">
           	 <option value="">--Select risk type--</option>
             <?php foreach($users  as $user) {
			 ?>
               <option value="<?php echo $user['tkid']; ?>"
               	<?php echo (trim($directorate['user_responsible'])==trim($user['id']) ? "selected" : "")?>
               ><?php echo $user['tkname']." ".$user['tksurname']; ?></option>
             <?php
			 }  ?>
            </select>
       </td>
      </tr>
      <tr>
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="directorate_id" id="directorate_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_directorate" id="edit_directorate" value="Edit" />
        <input type="submit" name="cancel_directorate" id="cancel_directorate" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left">
      		<?php $helper->displayGoBack("",""); ?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
    </div>

