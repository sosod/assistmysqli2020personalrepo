<?php
$folder = "defaults";
$page = "useraccess";
$scripts = array( 'useraccess.js','menu.js',  );
$styles = array( 'colorpicker.css' );
//$page_title = "User Access";
require_once("../inc/header.php");
?>
<script language="javascript">
$(function(){
	$(".usersmenu").click(function() {
			document.location.href = this.id+".php";
			return false;		  
		});
		$("table#user_access_menu_table").find("th").css({"text-align":"left"})
	});
</script>
<div>
	<table id="user_access_menu_table" width="40%">
    	<tr>
            <th>Risk manager:</th>
            <td>Define the Risk Manager(s)</td>
            <td><input type="button" name="risk_manager" id="risk_manager" class="usersmenu"  value="Configure"/></td>
        </tr>
    	<tr>
            <th>User Access:</th>
            <td>Configure User Access</td>
            <td><input type="button" name="user_access" id="user_access" value="Configure" class="usersmenu" /></td>
        </tr>
    </table>
</div>
