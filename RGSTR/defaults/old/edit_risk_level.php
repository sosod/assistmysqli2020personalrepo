<?php
$scripts = array( 'risk_level.js','menu.js','jpickermin.js','jpicker-1.1.6.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Edit Risk Level";
require_once("../inc/header.php");
$level 	   = new RiskLevel( "", "", "", "" );
$risklevel  = $level-> getARiskLevel( $_REQUEST['id'] ) ;	 
?>
<script>
$(function(){
		$("table#editarisk_level_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="edit-form-risk-level" name="edit-form-risk-level">
<div id="edit_risk_level"></div>
<table border="1" id="editarisk_level_table" cellpadding="5" cellspacing="0">
  <tr>
    <th>Ref #:</th>
    <td><?php echo $risklevel['id']; ?></td>
   <tr>
    <th>Short Code:</th>
    <td><input type="text" name="risklevel_shortcode" id="risklevel_shortcode" value="<?php echo $risklevel['shortcode']; ?>" /></td>
   </tr>
  <tr>
   <th>Risk Level:</th>
   <td><input type="text" name="risklevel" id="risklevel" value="<?php echo $risklevel['name']; ?>"/></td>
  </tr>
  <tr> 
    <th>Description:</th>
    <td><textarea cols="30" rows="7" id="risklevel_description" name="risklevel_description"><?php echo $risklevel['description']; ?></textarea></td>
   </tr>
  <tr>  
    <th></th>
    <td>
    	<input type="hidden" name="risklevel_id" id="risklevel_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="hidden" name="risklevel_status" id="risklevel_status" value="<?php echo $risklevel['status'] ?>" />        
    	<input type="submit" name="edit_risklevel" id="edit_risklevel" value="Edit" />
        <input type="submit" name="cancel_risklevel" id="cancel_risklevel" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td class="noborder" align="left"><?php $helper->displayGoBack("", ""); ?></td>
  	<td class="noborder"></td>
  </tr>
</table>
</form>
</div>

