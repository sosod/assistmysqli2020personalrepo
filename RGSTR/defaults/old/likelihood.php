<?php
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "likelihood";

$scripts = array( 'likelihood.js','menu.js', "jscolor.js", 'functions.js' );
$styles = array();
//$page_title = "Set Up Risk Likelihood";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="likelihood_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="likelihood-form" name="likelihood-form">
			<table border="1" id="likelihood_table">
			  <tr>
			    <th width="18">Ref</th>
			    <th width="56">Rating</th>
			    <th width="144">Assessment</th>
			    <th width="161">Definition</th>    
			    <th width="144">Probability</th>
			    <th width="144">Colour</th>
			    <th width="109"></th>
			    <th width="58">Status</th>    
			    <th width="31"></th>        
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			    	<input type="text" name="lkrating_from" id="lkrating_from" size="3" /> 
			        to 
			        <input type="text" name="lkrating_to" id="lkrating_to" size="3" /></td>
			    <td><input type="text" name="lkassessment" id="lkassessment" /></td>
			    <td><textarea name="lkdefinition" id="lkdefinition"></textarea></td>
			    <td><input type="text" name="lkprobability" id="lkprobability" /></td>    
			    <td><input type="text" name="lkcolor" id="llkcolor" class="color" value="e82929" /></td>
			    <td><input type="submit" name="add_likelihood" id="add_likelihood" value="Add" /></td>
			    <td></td>
			    <td></td>    
			  </tr>
			
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "likelihood_logs" , true); ?></td>		
	</tr>
</table>
</div>



