<?php
$folder = "defaults";
$page = "useraccess";
$scripts = array( 'risk_manager.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Risk Manager - Edit";
require_once("../inc/header.php");
$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
$user = $uaccess -> getARiskManager( $_REQUEST['id']);
?>
<div id="edit_manager_message" class="messsage"></div>
<table>
	<tr>
    	<td colspan="2">Update the risk manager</td>        
    </tr>
	<tr>
    	<th>Set as risk manager</th>
    	<td>
        	<select id="risk_manager" name="risk_manager">
            	<option value="1" <?php if($user['risk_manager'] == 1) { ?> selected <?php } ?>>Yes</option>
            	<option value="0" <?php if($user['risk_manager'] == 0) { ?> selected <?php } ?>>No</option>                
            </select>
        </td>        
    </tr>    
	<tr>
    	<td colspan="2">
            <input type="hidden" name="userid" value="<?php echo $_REQUEST['id']; ?>" id="userid" />        
        	<input type="submit" name="save_changes" value="Save Changes" id="save_changes" />
            <input type="submit" name="cancel_changes" value="Cancel" id="cancel_changes" />
        </td>        
    </tr>    
</table>