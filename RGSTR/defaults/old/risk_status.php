<?php
$folder = "defaults";
$page = "default";
$page_title = true;
$page_title_src = "risk_status";

$scripts = array( 'risk_status.js','menu.js', "201a.js","jscolor.js", 'functions.js' );
$styles = array();
//$page_title = "Set Up Risk Statuses";
require_once("../inc/header.php");
?>
<div>
<?php ASSIST_HELPER::JSdisplayResultPrep(""); ?>
<?php 
if(isset($_REQUEST['r']) && is_array($_REQUEST['r']) && $_REQUEST['r'][0]=="ok") {
	echo "<script type=text/javascript>JSdisplayResult(\"ok\", \"ok\", \"".$_REQUEST['r'][1]."\");</script>";
	echo "<input type=hidden name=displayResult id=displayResultCheck value='ok' />";
} else {
	echo "<input type=hidden name=displayResult id=displayResultCheck value='info' />";
}
?>
<div id="risk_status_message"></div>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
		    <form id="risk-status-form" name="risk-status-form" method="post">
		    <table border="1" id="risk_status_table">
		      <tr>
		        <th>Ref</th>
		        <th>Default Terminology</th>
		        <th>Your Terminology</th>
		        <th>Colour</th>
		        <th></th>
		        <th>Status</th>
		        <th></th>
		      </tr>
		      <tr>
		        <td>#</td>
		        <td><input type="text" name="status" id="status" /></td>
		        <td><input type="text" name="client_term" id="client_term" /></td>
		        <td>
		      <input name="risktype_color" id="risktype_color" class="color" value="66ff00"></td>
		        <td><input type="submit" name="add_status" id="add_status" value="Add" /></td>
		        <td></td>
		        <td>
		        </td>
		      </tr>
		    </table>
		    </form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink( "status_logs", true); ?></td>
	</tr>
</table>
</div>

