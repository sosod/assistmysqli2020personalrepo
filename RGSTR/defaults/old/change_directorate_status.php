<?php
$scripts = array( 'edit_directorate.js','menu.js');
$styles = array();
$page_title = "Edit Risk Status";
require_once("../inc/header.php");
$uaccess 	= new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		= $uaccess -> getUserAccess() ;
$dir 		= new Directorate( "", "", "" ,"");
$directorate=$dir -> getADirectorate( $_REQUEST['id'] );
?>

    <div>
    <form id="change-directorate-form" name="change-directorate-form" method="post">
    <div id="directorate_message"></div>
    <table border="1" id="change_directorate_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Status:</th>
        <td>
        	<select name="directorate_status" id="directorate_status">
            	<option value="1" <?php if($directorate['active']==1) {?> selected <?php } ?>>Active</option>
            	<option value="0" <?php if($directorate['active']==0) {?> selected <?php } ?>>InActive</option>                
            </select>
        </td>
      </tr>
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="directorate_id" id="directorate_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="change_directorate" id="change_directorate" value="Save Changes" />
        <input type="submit" name="cancel_directorate" id="cancel_directorate" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td class="noborder" align="left">
      		<?php $me->displayGoBack("", "")?>
        </td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
    </div>
