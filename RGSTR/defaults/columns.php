<?php
$page = "default";
$folder = "defaults";

$menuT = array(
	'risk_columns'=>array('id'=>"risk_columns",'url'=>"columns.php",'active'=> "Y",'display'=>"|OBJECTNAME| Columns"),
	'action_columns'=>array('id'=>"action_columns",'url'=>"action_columns.php",'active'=> "",'display'=>"|ACTIONNAME| Columns")
);

$scripts = array( 'columns.js','menu.js' , 'functions.js');
$styles = array();
$page_title = "List Columns - |OBJECTNAME|";
require_once("../inc/header.php");

?>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<?php ASSIST_HELPER::displayResult(array("info","Please note: To change the order in which the columns display, click and drag on the column name into the desired position before clicking the 'Save Changes' button.")); ?>
<div>
<form action="" id="riskcolumns_form">
<table id="table_riskcolumns">
	<tr class="disabled">
        <th>Client Terminology</th>
   		<th>Show On Manage Pages</th>
        <th>Show On New Pages</th>        
    </tr>
</table>
</form>
</div>
<?php $me->displayGoBack("",""); ?>