// JavaScript Document
$(function(){
		   
	$.get("controller.php?action=getUsers", function( userData ) {
		$.each( userData, function( index, user ){
			$("#userselect").append($("<option />",{text:user.tkname+" "+user.tksurname, value:user.tkid }))
		});										 
	},"json");		   
		   
	/**	   
		Get all users access details   
	**/
	$.get( "controller.php?action=getUserAccess", function( userData ) {
	  $('body').data('users',userData);
	  $.each( userData , function( index, user ){
		$("#useraccess_table")
		.append($("<tr />")
		   .append($("<td />",{html:user.id}))
		   .append($("<td />",{html:user.tkname+" "+user.tksurname}))		   
		   .append($("<td />",{html:(user.module_admin == 0 ? "no" : "yes")}))
		   .append($("<td />",{html:(user.create_risks == 0 ? "no" : "yes")}))
		   .append($("<td />",{html:(user.view_all == 0 ? "no" : "yes")}))
		   .append($("<td />",{html:(user.edit_all == 0 ? "no" : "yes")}))		
		   //.append($("<td />",{html:(user.update_all == 0 ? "no" : "yes")}))		   
		   .append($("<td />",{html:(user.reports == 0 ? "no" : "yes")}))
		   .append($("<td />",{html:(user.assurance == 0 ? "no" : "yes")}))
		   .append($("<td />",{html:(user.setup == 0 ? "no" : "yes")}))
		   .append($("<td />")
			  .append($("<input />",{type:"submit", name:"edit_"+user.id, id:"edit_"+user.id, value:"Edit"}))
			)
		 )	
		
		$("#edit_"+user.id).click(function() {
			document.location.href = "edit_user.php?id="+user.id;
			return false;
		});
	  })
	  
	}, "json")
	
	/**
		Adding new user access
	**/
	$("#setup_user_access").live('click', function() {
		var message 	 	= $("#useraccess_message");
		var user 		 	= $("#userselect :selected").val();
		var module_admin 	= $("#module_admin :selected");
		var create_risks 	= $("#create_risks :selected");	
		var create_actions  = $("#create_actions :selected");
		var view_all 		= $("#view_all :selected");		
		var edit_all 		= $("#edit_all :selected");
		var update_all 		= $("#update_all :selected");
		var reports 		= $("#reports :selected");	
		var assurance 		= $("#assurance :selected");
		var setup 			= $("#usersetup :selected");	
		
		var _userData 		= $('body').data('users');
		var usersArray 		= []; 
		$.each( _userData, function( index , userObj ){
			usersArray.push( userObj.tkid );
		});	
	
		if(user == "") {
			jsDisplayResult("error", "error", "Please enter the user to set up the access");	
			return false;
		} else if( $.inArray( user, usersArray ) >=0 ) {
			jsDisplayResult("error", "error", "The user you selected, has already been setup, you can only edit this user");
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving . . . <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=newUserAccess", 
				  {
					  user		: user,
					  modadmin	: module_admin.val(), 
					  cr_risk	: create_risks.val(),
					  cr_actions: create_actions.val(),
				  	  v_all		: view_all.val(),
					  e_all		: edit_all.val(),
					  u_all		: update_all.val(),
					  reports	: reports.val(),
					  assurance : assurance.val(),
					  setup		: setup.val()
				  },
				  function( retData ){
					if( retData > 0 ) 
					{
					jsDisplayResult("ok", "ok", "User saved");
					$("select#userselect options[value='']").attr("selected", "selected");
					$("#useraccess_table")
					.append($("<tr />")
					   .append($("<td />",{html:retData}))
					   .append($("<td />",{html:$("#userselect :selected").text()}))
					   .append($("<td />",{html:module_admin.text()}))
					   .append($("<td />",{html:create_risks.text()}))
					   //.append($("<td />",{html:create_actions.text()}))
					   .append($("<td />",{html:view_all.text()}))
					   .append($("<td />",{html:edit_all.text()}))
					   .append($("<td />",{html:update_all.text()}))
					   .append($("<td />",{html:reports.text()}))
					   .append($("<td />",{html:assurance.text()}))
					   .append($("<td />",{html:setup.text()}))	
					   .append($("<td />")
						  .append($("<input />",{type:"submit", name:"edit", id:"edit", value:"Edit"}))
						)
					)
					$("selecct#userselect option[value='']").attr("selected","selected")
					$("selecct#module_admin option[value='']").attr("selected","selected")
					$("selecct#create_risks option[value='']").attr("selected","selected")
					//$("selecct#create_actions option[value='']").attr("selected","selected")
					$("selecct#view_all option[value='']").attr("selected","selected")
					$("selecct#edit_all option[value='']").attr("selected","selected")
					$("selecct#update_all option[value='']").attr("selected","selected")
					$("selecct#reports option[value='']").attr("selected","selected")
					$("selecct#assurance option[value='']").attr("selected","selected")
					$("selecct#usersetup option[value='']").attr("selected","selected")	
				} else {
					jsDisplayResult("error", "error", "There was an error trying to save the user");	
				}
				}, "json" )	
		}
		return false;							  
	});
})
