<?php
$page = "action_new";
$folder = "risk";

$scripts = array( 'jquery.ui.action.js', 'add_action.js','menu.js','functions.js', 'ajaxfileupload.js');
$styles = array( 'colorpicker.css' );
$page_title = "Add |ACTIONNAME|";
require_once("../inc/header.php");


/*$objId = $_REQUEST['id'];
$numbers = array("0","1","2","3","4","5","6","7","8","9");
$id = "";
for($i=0;$i<strlen($objId);$i++) {
	if(in_array($objId[$i],$numbers)) { $id.=$objId[$i]; }
}
echo $id;*/


$rkCols  = new ActionColumns();
$columns = $rkCols -> getHeaderList();

//ASSIST_HELPER::arrPrint($columns);

function nameFields( $fieldId , $defaultName ){
	echo getNameFields($fieldId,$defaultName);
}

function getNameFields($fieldId,$defaultName) {
	global $columns;
	if( isset( $columns[$fieldId] )) {
		return $columns[$fieldId];
	} else {
		return $defaultName;
	}
}


?>
<script>
$(function(){
		$("table#addaction_table").find("th").css({"text-align":"left"})	
});
</script>
<style type=text/css>
	th { text-align: left; vertical-align: top; }
</style>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table id="risk_action_table" class="noborder">
	<tr>
    	<td width="50%" valign="top" class="noborder">
    		<table class="noborder">
    			<tr>
    				<td colspan="2"><h4><?php echo $risk_object_name; ?> Details</h4></td>
    			</tr>
				<tr id="goback">
					<td class="noborder" align="left" width="150">
						<?php $me->displayGoBack("",""); ?>
					</td>
					<td class="noborder"></td>
				</tr>				
    			 <tr>
    				<td colspan="2" class="noborder">
    				<script language="javascript">
						$(function(){
							//get all the actions for this risk	
						  $("#actions").action({risk_id:<?php echo $_REQUEST['id']; ?>, extras:false, objectName:window.risk_object_name, actionName:window.action_object_name});
						})
					</script>
    				
    				<div id="actions"></div></td>
    			</tr>
    		</table>
        </td>
     <td width="50%" valign="top" class="noborder">
      <table align="left" id="addaction_table" width="100%" style="border-collapse:collapse;">
      	  <tr><td colspan="2"><h4>Add New <?php echo $action_object_name; ?></h4></td></tr>
          <tr>
            <th><?php echo $risk_object_name; ?>:</th>
            <td><?php echo RISK::REFTAG.$_REQUEST['id']; ?></td>
          </tr>
          <tr>
            <th><?php nameFields("risk_action","Action"); ?>:</th>
            <td><textarea name="action" id="action"></textarea></td>
          </tr>
          <tr>
            <th><?php nameFields("deliverable","Deliverable"); ?>:</th>
            <td><textarea name="deliverable" id="deliverable"></textarea></td>
          </tr>
          <tr>
            <th><?php nameFields("action_owner","Action Owner"); ?>:</th>
            <td>
                <select id="action_owner" name="action_owner">
                
                </select>
            </td>
          </tr>
          <tr>
            <th><?php nameFields("timescale","Time Scale"); ?>:</th>
            <td><input type="text" name="timescale" id="timescale" /></td>
          </tr>
          <tr>
		  
            <th><?php nameFields("deadline","Deadline"); ?>:</th>
            <td><input type="text" name="deadline" id="deadline" class="datepicker" readonly="readonly" /></td>
          </tr>
          <tr>
            <th>Remind On:</th>
            <td><input type="text" name="remindon" id="remindon" class="datepicker" readonly="readonly" /></td>
          </tr>
<!--          <tr>
            <th>Attachments:</th>
            <td>
                <span id="loading"></span>
                <span id="file_loading"></span>
                <input type="file" id="action_attachments" name="action_attachments" onchange="ajaxFileUpload(this)"/>
            </td>
          </tr> -->
          <tr>
            <th></th>
            <td>
            <input type="hidden" id="risk_id" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" />
            <input type="submit" id="save_action" value="Save <?php echo $action_object_name; ?>" name="save_action" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" id="cancel_action" value="Cancel" name="cancel_action" />
            </td>
          </tr>
        </table>						       
      </td>
  </tr>
</table>
	