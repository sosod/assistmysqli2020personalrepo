<?php
	$scripts = array( 'approve_risk.js','menu.js');
	$styles = array( 'colorpicker.css' );
	$page_title = "Approve Risk";
	require_once("../inc/header.php");
?>
<div>
<h4>Risk Register</h4>
        <table align="left" id="risk_table" border="1">
            <tr>
                <th><?php nameFields('riskitem','Risk Item'); ?></th>
                <th><?php nameFields('risktype','Type'); ?></th>
                <th><?php nameFields('riskcategory','Category'); ?></th> 
                <th><?php nameFields('riskdescription','Description'); ?></th>            
                <th><?php nameFields('background','Background'); ?></th> 
                <th><?php nameFields('impact','Impact'); ?></th> 
                <th><?php nameFields('impact_rating','Impact Rating'); ?></th> 
                <th><?php nameFields('likelihood','Likelihood'); ?></th> 
                <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?></th> 
                <th><?php nameFields('current_controls','Current Controls'); ?></th> 
                <th><?php nameFields('percieved_control_effectiveness','Percieved Control Effectiveness'); ?></th> 
                <th><?php nameFields('control_effectiveness_rating','Control Effectiveness Rating'); ?></th> 
                <th><?php nameFields('riskowner','Responsibilty of the risk'); ?></th> 
                <th><?php nameFields('status','Status'); ?></th> 
                <th><?php nameFields('','Attachements'); ?></th> 
                <th><?php //nameFields('','Actions'); ?></th>
            </tr>
        </table>

</div>	

