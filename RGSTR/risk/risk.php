<?php
/**
	* @package 		: Risk 
	* @author 		: admire<azinamo@gmail.com>
	* @copyright	: 2011 Ignite Assist
**/
class Risk extends DBConnect
{
	/**
		@ int	 	 	 	 	 	 	
	**/
	protected $id;
	/**
		@int
	**/
	protected $item;
		/**
		@int
	**/
	protected $type;
		/**
		@int
	**/
	protected $level;
	/**
		@int
	**/
	protected $financial_exposure;
	/**
		@int
	**/
	protected $actual_financial_exposure;		
	/**
		@int
	**/
	protected $kpi_ref;			
	/**
		@int
	**/
	protected $category;
	/**
		@char
	**/
	protected $description;
	/**
		@char
	**/
	protected $background;
	/**
		@int
	**/
	protected $impact;
	/**
		@int
	**/
	protected $impact_rating;
	/**
		@int
	**/
	protected $likelihood;
	/**
		@int
	**/
	protected $likelihood_rating;
	/**
		@text
	**/
	protected $inherent_risk_exposure;
	/**
		@text
	**/
	protected $inherent_risk_rating;
	/**
		@char
	**/
	protected $current_controls;
	/**
		@int
	**/
	protected $percieved_control_effectiveness;
	/**
		@int
	**/
	protected $control_effectiveness_rating;
	/**
		@char
	**/
	protected $residual_risk;
	/**
		@char
	**/
	protected $residual_risk_exposure;
	/**
		@char
	**/
	protected $risk_owner;
	/**
		@char
	**/
	protected $actions_to_improve;
	/**
		@char
	**/
	protected $action_owner;
	/**
		@char
	**/
	protected $time_scale;
	/**
		@char
	**/
	protected $status;
	/**
		@char
	**/
	protected $udf;
	/**
		@char
	**/
	protected $attachement;
	/**
		@char
	**/
	protected $insertdate;
	
	protected $start;
	
	protected $limit;
	/**
	 Action ids which are associated to a user
	 @array
	**/
	protected $actionIds = array();
	
	function __construct( 
						$type, $category,  $description, $background, $impact, $impact_rating, $likelihood, 
						$likelihood_rating, $current_controls, $percieved_control_effectiveness,
						$control_effectiveness_rating, $risk_owner , $attachement
		  )
	{
		//$this -> item 								= $item;
		$this -> type   							= $type;
		$this -> category 							= $category;
		$this -> background 						= $background;		
		$this -> description 						= $description;
		$this -> impact 							= $impact;
		$this -> impact_rating 						= $impact_rating;
		$this -> likelihood 						= $likelihood; 
		$this -> likelihood_rating 					= $likelihood_rating;
		//$this -> inherent_risk_exposure 			= $inherent_risk_exposure;
		//$this -> inherent_risk_rating 			= $inherent_risk_rating;
		$this -> current_controls 					= $current_controls; 
		$this -> percieved_control_effectiveness 	= $percieved_control_effectiveness;
		$this -> control_effectiveness_rating 		= $control_effectiveness_rating;
		//$this -> residual_risk 						= $residual_risk;
		//$this -> residual_risk_exposure 			= $residual_risk_exposure;
		$this -> risk_owner 						= $risk_owner;	
		//$this -> actions_to_improve 				= $actions_to_improve;
		//$this -> action_owner 						= $action_owner;
		//$this -> time_scale 						= $time_scale;
		//$this -> status 							= $status;
 		$this -> attachement						=  $attachement;	
		$this->level								= (isset($_REQUEST['level']) ? $_REQUEST['level'] : "");		
		$this->financial_exposure					= (isset($_REQUEST['financial_exposure']) ? $_REQUEST['financial_exposure']: "");			
		$this->actual_financial_exposure			= (isset($_REQUEST['actual_financial_exposure']) ? $_REQUEST['actual_financial_exposure'] : "");
		$this->kpi_ref								= (isset($_REQUEST['kpi_ref']) ? $_REQUEST['kpi_ref'] : "");


		parent::__construct();
	}
	
	
	function  saveRisk()
	{
		$inherent = ($this -> impact_rating * $this -> likelihood_rating);
		$residual = (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$insertdata = array(
			'type'   							=> $this -> type,
			'category' 							=> $this -> category,
			'description' 						=> $this -> description,
			 'background'						=> $this -> background ,
			'impact' 							=> $this -> impact,
			'inherent_risk_exposure'			=> $inherent,
			'inherent_risk_rating'				=> $inherent,
			'residual_risk_exposure'			=> $residual,
			'residual_risk'						=> $residual,						
			'impact_rating' 					=> $this -> impact_rating,
			'likelihood' 						=> $this -> likelihood,
			'likelihood_rating' 				=> $this -> likelihood_rating,
			'current_controls' 					=> $this -> current_controls,
			'percieved_control_effectiveness' 	=> $this -> percieved_control_effectiveness,
			'control_effectiveness_rating' 		=> $this -> control_effectiveness_rating,
			'level' 							=> $this -> level,		
			'financial_exposure'				=> $this -> financial_exposure,		
			'actual_financial_exposure'			=> $this -> actual_financial_exposure,		
			'kpi_ref' 							=> $this -> kpi_ref,												
			'attachment' 						=> $this -> attachement	,		
			'sub_id' 							=> $this -> risk_owner,
			'insertuser'						=> $_SESSION['tid']		
		);
		
		$response  = $this -> insert( 'risk_register' , $insertdata);
		return $response;//$this -> insertedId();
	}
	
	function  saveRiskUpdate( $id , $status, $response, $changes)
	{
		$inherent = ($this -> impact_rating * $this -> likelihood_rating);
		$residual = (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$insertdata = array(
						'impact' 							=> $this -> impact,
						'impact_rating' 					=> $this -> impact_rating,
						'likelihood' 						=> $this -> likelihood,
						'likelihood_rating' 				=> $this -> likelihood_rating,
						'current_controls' 					=> $this -> current_controls,
						'percieved_control_effectiveness' 	=> $this -> percieved_control_effectiveness,
						'control_effectiveness_rating' 		=> $this -> control_effectiveness_rating,
						'inherent_risk_exposure'			=> $inherent,
						'inherent_risk_rating'				=> $inherent,
						'residual_risk_exposure'			=> $residual,
						'residual_risk'						=> $residual,	
						'status'							=> $status,
						'insertuser'						=> $_SESSION['tid'],						
		);
		$responseUpdate  = $this -> update( 'risk_register' , $insertdata, "id = $id");
		$response  		 = $this -> insert( 'risk_update' , array_merge($insertdata, array(
															"risk_id" 			=> $id,
															"response"			=> $response,
															"changes"			=> $changes
														) 
											));
		return $responseUpdate;//$this -> insertedId();
	}
	
	function getRiskUpdate( $id )
	{  								  
		$response = $this -> getRow( "SELECT status, name, color FROM ".$_SESSION['dbref']."_risk_update RU
									  INNER JOIN ".$_SESSION['dbref']."_status S ON RU.status = S.id 
									  WHERE RU.risk_id = '".$id."' " );
		return $response;
	}
	
	function getAllRisk( $start , $limit)
	{
		$sqlLimit = "";
		if( $start !== "" && $limit !== "") {
			$sqlLimit = "LIMIT $start , $limit";
		} else {
			$sqlLimit = "";
		}
		$uaccess 		= new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess     = $uaccess -> getUser( $_SESSION['tid'] );
		$userRiskRef     = $uaccess -> getUserRefDirectorate();
		$viewAccess 	= $this -> _setViewAccess( $userAccess , (isset($_GET['page']) ? $_GET['page'] : "") , $userRiskRef );
		$response = $this -> get( "
							SELECT DISTINCT(RR.id) AS risk_item,
							RR.description AS risk_description,
							RR.background AS risk_background,
							RR.impact_rating AS IMrating,
							RR.likelihood_rating AS LKrating, 
							RR.control_effectiveness_rating AS control_rating , 
							RR.current_controls,
							RR.time_scale , 
							RR.inherent_risk_exposure,
							RR.inherent_risk_rating,
							RR.residual_risk,
							RR.residual_risk_exposure,
							RR.kpi_ref,
							RR.actual_financial_exposure,
							FE.name AS financial_exposure,							
							RLevel.name AS risk_level, 							
							RC.name as risk_category, 
							RC.description as cat_descr,
							RCE.effectiveness AS CtrlRating,
							RCE.color AS percieved_control_effective , 								
							IM.assessment as impact,
							IM.definition as impact_descr ,
							IM.color AS impact_rating,
							RL.assessment as likelihood,
							RL.color AS likelihood_rating ,
							RL.definition like_descr,
							RT.name as type,
							RT.shortcode as type_code, 
							D.dirtxt  AS risk_owner,
							RS.name AS risk_status ,
							RS.color AS risk_status
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status		
							LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure								
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE RR.active = 1 $viewAccess ORDER BY RR.id asc $sqlLimit
							");
							
		$totalrisk = $this -> totalRisk( $viewAccess  );
		$nm 			= new Naming();
		$default  		= new Defaults("","");
		$defaults 		= $default -> getDefaults();
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");		
		$data = array();
		$riskArray = array();
		foreach( $response as $key => $risk )
		{
			$updateArray 	= $this -> getRiskUpdate( $risk['risk_item'] );		
			$riskArray[$risk['risk_item']] = array(
							"description"		=> $risk['risk_description'],
							"background"		=> $risk['risk_background'],
							"impactRating"		=> $risk['IMrating'],
							"likelihoodRating"	=> $risk['LKrating'],
							"ctrEffectRating"	=> $risk['control_rating'],
							"currControls"		=> $risk['current_controls'],
							"timeScale"			=> $risk['time_scale'],
							"inhRiskExposure"	=> $residualRanges = $res -> getRiskResidualRanges( $risk['inherent_risk_exposure'] ),
							"inhRiskRating"		=> $risk['inherent_risk_exposure'],
							"residualRisk"		=> $risk['residual_risk'],
							"resRiskExposure"	=> $ihr -> getRiskInherentRange( round( $risk['residual_risk_exposure'] ) ),
							"category"			=> $risk['risk_category'],
							"catDescr"			=> $risk['cat_descr'],
							"effectiveness"		=> $risk['CtrlRating'],
							"effectColor"		=> $risk['percieved_control_effective'],
							"impact"			=> $risk['impact'],
							"impactDescr"		=> $risk['impact_descr'],
							"impactColor"		=> $risk['impact_rating'],
							"likelihood"		=> $risk['likelihood'],	
							"likeColor"			=> $risk['likelihood_rating'],
							"likeDescr"			=> $risk['like_descr'],
							"type"				=> $risk['type'],
							"tkname"			=> $risk['risk_owner'],
							"status"			=> (empty($updateArray) ? "New" : $updateArray['name']),
							"rscolor"			=> (empty($updateArray) ? "" : $updateArray['color']),																																										
						);

			}
		$data = array( "total" => $totalrisk, "riskData" => $riskArray, "headers" => $nm -> getNameColumn() );
		return $data;
	}
	
	function getRiskEdits( $id )
	{
	
		$response = $this -> get(  "
							SELECT							
							RR.id AS risk_item,
							RR.changes,
							RR.description AS risk_description,
							RR.background AS risk_background,
							RR.impact_rating AS IMrating,
							RR.likelihood_rating AS LKrating, 
							RR.control_effectiveness_rating AS control_rating , 
							RR.current_controls,
							RR.time_scale , 
							RR.inherent_risk_exposure,
							RR.inherent_risk_rating,
							RR.residual_risk,
							RReg.residual_risk_exposure,
							RReg.kpi_ref,
							RReg.actual_financial_exposure,
							FE.name AS financial_exposure,							
							RLevel.name AS risk_level, 								
							RC.name as risk_category, 
							RC.description as cat_descr,
							RCE.effectiveness AS CtrlRating,
							RCE.color AS percieved_control_effective , 								
							IM.assessment as impact,
							IM.definition as impact_descr ,
							IM.color AS impact_rating,
							RL.assessment as likelihood,
							RL.color AS likelihood_rating ,
							RL.definition like_descr,
							RT.name as type,
							RT.shortcode as type_code, 
							RR.risk_owner,
							UIA.tkname AS risk_owner,
							UIA.tksurname,
							RS.name AS risk_status ,
							RS.color AS status_color,
							RR.insertdate,
							RR.insertuser ,
							UIA.tkname AS username,
							UIA.tksurname AS surname 							
							FROM ".$_SESSION['dbref']."_risk_edits RR 
							INNER JOIN ".$_SESSION['dbref']."_risk_register RReg ON RReg.id = RR.risk_id
							INNER JOIN ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLevel ON RLevel.id = RReg.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RReg.financial_exposure								
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RReg.status							
							LEFT JOIN assist_".$_SESSION['cc']."_timekeep UIA ON UIA.tkid = RR.insertuser
							WHERE RR.risk_id = '".$id."' ORDER BY RR.insertdate DESC 
							");

		return $response;
	}
	/**
		Fetches a specified risk id 
		@param risk id
		@return boolelan of ids
	**/
	function _isRiskOwner( $id , $riskid)
	{
		$response = $this -> getRow("SELECT id 
								  FROM ".$_SESSION['dbref']."_risk_register 
								  WHERE risk_owner = '".$id."' AND id = '".$riskid."' AND active = 1 "
								  ); 
		if( isset($response['id']) && !empty($response['id'])) {
			//echo "This risk id ".$riskid." the person is the owner \r\n\n\n";
			return TRUE;
		} else {
			//echo "This risk id ".$riskid." the person is the action owner only \r\n\n\n";		
			return FALSE;
		}						  
	}
	/**
	 Creates a string of comma separated risk ids the users has actions assoociated to it
	 @param array  , that has an index of risk id
	 @return string
	**/
	function _setRiskIds( $risIdkArray )
	{
		$riskids = "";
		if( is_array( $risIdkArray )) 
			foreach( $risIdkArray as $key => $valueArray ) 
			{
				$riskids .= "'".$valueArray['risk_id']."',";
			}
		$riskids = rtrim( ltrim( rtrim($riskids, ","), "'") , "'");
		return $riskids;
	}
	/**
		Sets the view options for each used based on the user access settings
		@param , userAccess array , pageRequest page requesting the view
		@return string, sql string
	**/
	function _setViewAccess( $userAccess, $pageRequest, $userRiskRef ) 
	{
		//$actions 			= new RiskAction("", "", "", "", "", "", "", "", "");		
		//$userAssignedAction = $actions -> userAssignedAction( $userAccess['user'] );
		//$ids 				= $this -> _setRiskIds( $userAssignedAction );
		$viewAccess = "";
		if( $pageRequest == "viewAll"){
			$viewAccess  = "AND 1";
		} else if( $pageRequest == "viewMyn"){
			$viewAccess   = " AND DA.tkid = '".$_SESSION['tid']."' AND DA.active = 1 AND DA.type = 'SUB'";
			$viewAccess  .= " AND subdirid IN('".$userRiskRef."')";
		}
		return $viewAccess;
	}
	/**
		Fetches all the risk in detail 
		@return array;
	**/
	function getAllRiskTogether( $start , $limit)
	{
		$nm 			= new RiskColumns();
		$headers		= array();
		if( $_GET['section'] == "manage"){
			$options = " AND active & 4 = 4 ";
			$headers = $nm -> getHeaderList( $options );	
		} else if($_GET['section'] == "new"){
			$options = " AND active & 8 = 8 ";
			$headers = $nm -> getHeaderList( $options );
		}
		
		$uaccess 		= new UserAccess( "", "", "", "", "", "", "", "", "" );
		$userAccess     = $uaccess -> getUser( $_SESSION['tid'] );
		//get the directorates / sub directorate the user is in 
		$userRiskRef     = $uaccess -> getUserRefDirectorate();
		$viewAccess 	= $this -> _setViewAccess( $userAccess , (isset($_GET['page']) ? $_GET['page'] : ""), $userRiskRef );
		
		//echo "User directorates ".$userRiskRef."";
		
		//echo "<br /> View access is ".$viewAccess."<br />";
		
		//echo "View access for this user are ".$viewAccess." <br />";
		//$nm 			= new Naming();
		//$headers 		= $nm -> getNamingConversion();
		//$headersNew		= $nm -> getNameColumn(); 
		$colored  		= array(
								"percieved_control_effective" => "CtrlRating",
								"impact_rating" 			  => "IMrating",
								"likelihood_rating" 		  => "LKrating",
								);
		$default  		= new Defaults("","");
		$defaults 		= $default -> getDefaults();
		$res 			= new ResidualRisk( "", "", "", "", "");
		$ihr 			= new InherentRisk("", "", "", "", "");
		
		$response = $this -> get( "
							SELECT 
							DISTINCT(RR.id) AS risk_item,
							RR.description AS risk_description,
							RR.background AS risk_background,
							RR.impact_rating AS IMrating,
							RR.likelihood_rating AS LKrating, 
							RR.control_effectiveness_rating AS control_rating , 
							RR.current_controls,
							RR.time_scale , 
							RR.inherent_risk_exposure,
							RR.inherent_risk_rating,
							RR.residual_risk,
							RR.residual_risk_exposure,
							RR.attachment AS attachements,
							RR.kpi_ref,
							RR.actual_financial_exposure,
							FE.name AS financial_exposure,							
							RLEVEL.name AS risk_level, 												
							RC.name as risk_category, 
							RC.description as cat_descr,
							RCE.effectiveness AS CtrlRating,
							RCE.color AS percieved_control_effective , 								
							IM.assessment as impact,
							IM.definition as impact_descr ,
							IM.color AS impact_rating,
							RLL.assessment as likelihood,
							RLL.color AS likelihood_rating ,
							RLL.definition like_descr,
							RT.name as risk_type,
							RT.shortcode as type_code, 
							D.dirtxt  AS risk_owner,
							RR.sub_id,
							RS.name AS risk_status,
							RS.color AS riskstatuscolor
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLEVEL ON RLEVEL.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure								
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RLL ON RLL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status							
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE RR.active = 1 $viewAccess ORDER BY RR.id asc LIMIT $start, $limit 
							" );
							
		$riskArray = array();

		$attachments = "";
		$countAttach = 0;               
		foreach( $response as $key => $risk )
		{
			$updateArray 		= $this -> getRiskUpdate( $risk['risk_item'] );
			foreach($headers as $field => $value) 
			{
				if( $field == "action_progress"){
					$actionObj 		= new RiskAction($risk['risk_item'], "", "", "", "", "", "", "", "");
					$actionProgress = $actionObj -> getActionStats();
					$riskArray[$risk['risk_item']][$field]  = round($actionProgress['averageProgress'], 2)."%";
				} else if( isset( $risk[$field]) || array_key_exists($field, $risk) )
				{
					//calculated
					if($field === "residual_risk_exposure" ) {		
					 $risk[$field] =  "<span style='width:50px; background-color:#".$res -> getRiskResidualRanges( ceil($risk[$field]) )."'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
					}
				//calculted
					if($field === "inherent_risk_exposure" ) {
							$risk[$field] =   "<span style='width:50px; background-color:#".$ihr -> getRiskInherentRange( round($risk[$field]) )."'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
					}

					if($field == "attachements" ) {
						$attched  = "";
						if( $risk[$field] !== "" || $risk[$field] !== NULL ){
							$attachments	= unserialize($risk[$field]);
							$countAttach    = count($attachments);
							if(	isset($attachments) && !empty($attachments)){
								foreach( $attachments as $attchName => $attach ){
									 $attched .= $attach."<br />";
								}
							}
						}
                        if($attched == ""){
                            $risk[$field] = "&nbsp;";
                        } else {
                            $risk[$field] = $attched;
                        }
						//$riskArray[$risk['risk_item']]['attachment'] = $attched;

					}
					
					if($field == "risk_status" ) {
						//$risk[$field] = "<span style='width:50px; text-align:center; background-color:#".$risk['riskstatuscolor']."'>&nbsp;&nbsp;&nbsp;</span>".$risk[$field];
					}		
							
					if( in_array($field, array_keys($colored)) ){
						$riskArray[$risk['risk_item']][$field] 	= "<span style='width:50px; text-align:center; background-color:#".$risk[$field]."'>&nbsp;&nbsp;&nbsp;".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."&nbsp;&nbsp;&nbsp;</span>";
					}	else {
						$riskArray[$risk['risk_item']][$field]  = $risk[$field];
					}	
				}  
			}
		}
		$totalrisk = $this -> totalRisk( $viewAccess );
		$data 	   = array();
		$data 	   = array( 
						"x"			 => count($headers),
						"total" 	 => (int)$totalrisk ,				 
						"riskData"   => $riskArray,
						"headers"	 => $headers,
						"defaults"	 => $defaults,
						"useraccess" => $userAccess, 
						"actionOnly" => $this -> actionIds,
					  );
		return $data;
	}
	/**
		Fetches the total risks available 
		@return int
	**/
	function totalRisk( $viewAccess)
	{

		$response = $this -> getRow( "
							SELECT COUNT( DISTINCT(RR.id) ) as total
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLL ON RLL.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure								
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							INNER JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status	
							WHERE RR.active = 1 $viewAccess  
							" );
		return $response['total'];
	}
	/**
		Fetches detailed information about a single risk
		@return array
	**/
	function getRisk( $id  )
	{	
		$where = "";
		if( $id == "" ) {
			$where = "WHERE 1";	
		} else {
			$where = "WHERE RR.id = $id"; 
		}			
		
		$response = $this -> getRow(
							"SELECT RR.*,
							RR.sub_id AS user_responsible,
							FE.name AS risk_financial_exposure,							
							RLEVEL.name AS risk_level, 									
							RC.name as category,
							RC.id as categoryid,							
							RC.description as cat_descr,
							RCE.effectiveness ,
							RR.control_effectiveness_rating ,							
							RCE.id as effectivenessid,
							RCE.color effect_color , 							
							IM.assessment as impact,
							IM.id as impactid,
							IM.definition as impact_descr ,
							IM.color AS impact_color,
							RL.assessment as likelihood,
							RL.id as likelihoodid,							
							RL.color AS like_color ,
							RL.definition like_descr,
							RT.name as type,
							RT.id as typeid,							
							RT.shortcode as type_code,
							D.dirtxt  AS risk_owner,
							RS.name AS status ,
							RS.id as statusId,
							RS.color AS rscolor
							FROM ".$_SESSION['dbref']."_risk_register RR 
							INNER JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = RR.category
							INNER JOIN  ".$_SESSION['dbref']."_control_effectiveness RCE ON RCE.id = RR.percieved_control_effectiveness
							LEFT JOIN ".$_SESSION['dbref']."_level RLEVEL ON RLEVEL.id = RR.level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = RR.financial_exposure								
							INNER JOIN ".$_SESSION['dbref']."_impact IM ON IM.id = RR.impact
							INNER JOIN ".$_SESSION['dbref']."_likelihood RL ON RL.id = RR.likelihood
							INNER JOIN ".$_SESSION['dbref']."_types RT ON RT.id = RR.type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = RR.status							
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = RR.sub_id
							INNER JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid	
							$where
							" );
		return $response ;
	}
	/**
		Fetches limited infomation about the risk of specified id
		@return array
	**/
	function getARisk( $id )
	{
		$response = $this -> getRow ( "SELECT * FROM ".$_SESSION['dbref']."_risk_register WHERE id = '".$id."'" );
		return $response;
	}
	
	function updateRisk( $id, $changes )
	{
		$inherent 	= ($this -> impact_rating * $this -> likelihood_rating);
		$residual 	= (($this -> impact_rating * $this -> likelihood_rating) * $this -> control_effectiveness_rating);
		$insertdata = array(
			'type'   							=> $this -> type,
			'category' 							=> $this -> category,
			'description' 						=> $this -> description,
			'background'						=> $this -> background ,
			'impact' 							=> $this -> impact,
			'inherent_risk_exposure'			=> $inherent,
			'inherent_risk_rating'				=> $inherent,
			'residual_risk_exposure'			=> $residual,
			'residual_risk'						=> $residual,						
			'impact_rating' 					=> $this -> impact_rating,
			'likelihood' 						=> $this -> likelihood,
			'likelihood_rating' 				=> $this -> likelihood_rating,
			'current_controls' 					=> $this -> current_controls,
			'percieved_control_effectiveness' 	=> $this -> percieved_control_effectiveness,
			'control_effectiveness_rating' 		=> $this -> control_effectiveness_rating,
			'level' 							=> $this -> level,		
			'financial_exposure'				=> $this -> financial_exposure,		
			'actual_financial_exposure'			=> $this -> actual_financial_exposure,		
			'kpi_ref' 							=> $this -> kpi_ref,			
			'attachment' 						=> $this -> attachement	,		
			'risk_owner' 						=> $this -> risk_owner,
			'insertuser'						=> $_SESSION['tid'],			

		);
	
		$response  	  = $this -> update( 'risk_register' , $insertdata , "id = $id");
		$responseEdit = $this -> insert( 'risk_edits' , array_merge( $insertdata , array("risk_id" => $id,'changes'=> $changes) ) );		
		return $response;
	}
	
	function deleteRisk( $id )
	{
		// send email notifying of the delete
		$updatedata = array( 
							'active' 		=> 0,
							'insertuser'	=> $_SESSION['tid']
							);
		$response  = $this -> update( 'risk_register' , $updatedata , "id = $id");
		return $response;
	}
	
	function getRiskUpdates( $id )
	{	
	
	 $response = $this -> get( "SELECT 
	 							RU.id AS risk_item,
								RU.changes,
	 							RU.response,
								RS.name AS risk_status ,
								RU.insertdate,
								RU.insertuser,
								TK.tkname,
								TK.tksurname
	 							FROM ".$_SESSION['dbref']."_risk_update RU
	 							INNER JOIN ".$_SESSION['dbref']."_status RS ON RU.status = RS.id
								LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = RU.insertuser
								WHERE RU.risk_id = '".$id."' ORDER BY RU.insertdate DESC ,RU.id DESC						
	  " );
	  return $response; 
	}
	
	function _processAttachment( $attachement )
	{
		$attach = unserialize( $attachement );
		$tosave = "";
		if(is_array( $attach )) 
			foreach( $attach as $at ) {
				$tosave .= $at.","; 
			}
		$tosave = rtrim($tosave,",");
		return $tosave;
	}
	
}
?>