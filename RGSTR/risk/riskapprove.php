<?php
$scripts = array( 'risk_assurance.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Assurance of risk";
require_once("../inc/header.php");
?>
<form method="post" name="assurance-form" id="assurance-form">
<table align="left" border="1" id="assurance">
	<tr>
    	<td valign="top">
        	<table id="risk_assurance_table">
            	<tr>
                	<td colspan="2">Risk Information</td>                  
                </tr>
            </table>
        </td>
        <td valign="top">
 			<table align="left" id="list_of_risk_assurance">
             	<tr>
                	<th>System Date and Time </th>                  
                	<th>Date Tested</th>                  
                	<th>Response</th>                  
                	<th>Sign Off</th>                 
                	<th>Assurance Provider</th>                  
                	<th>Attachment</th>                  
                	<th>Action</th>                                                                              
                </tr>
            </table>       
         </td>
    </tr>
</table>
 <div>
	<table align="left" id="new_risk_assurance" border="1" style="display:none;">
    	<tr>
        	<td colspan="2"><div id="newassurance_message"></div></td>
        </tr>
    	<tr>
        	<td colspan="2"><h2>New Risk Approval</h2></td>
        </tr>
     	<tr>
        	<td>Response</td>
        	<td><textarea name="riskassurance_response" id="riskassurance_response" ></textarea></td>            
        </tr>
     	<tr>
        	<td>Sign-Off</td>
        	<td>
            	<select name="riskassurance_signoff" id="riskassurance_signoff">
                	<option value="1">Yes</option>
                	<option value="0">No</option>                    
                </select>
            </td>            
        </tr>           
     	<tr>
        	<td>Attachment</td>
        	<td><input type="file" name="riskassurance_attachment" id="riskassurance_attachment" /></td>            
        </tr>   
     	<tr>
        	<td></td>
        	<td>
            	<input type="submit" name="add_newRisk_assurance" id="add_newRisk_assurance" value="Save" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	<input type="submit" name="cancel_newRisk_assurance" id="cancel_newRisk_assurance" value="Cancel" />
            </td>            
        </tr>                       
    </table>
   </div>
<input type="hidden" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" id="risk_id"  /> 
</form>
</body>
</html>
