<?php
@session_start();
include_once("../inc/init.php");

$def = new Defaults("","");
$risk_object_name = $def->getObjectName("OBJECT"); //echo $risk_object_name;
$action_object_name = $def->getObjectName("ACTION"); //echo $action_object_name;
$risk_object_code = $def->getObjectCode();

switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {
	case "newRisk":
		$risk = new Risk( 
				$_REQUEST['type'] ,
				$_REQUEST['category'] ,
				$_REQUEST['description'] ,
			    $_REQUEST['background'] ,
		 		$_REQUEST['impact'] ,
				$_REQUEST['impact_rating'] ,
				$_REQUEST['likelihood'] ,
				$_REQUEST['likelihood_rating'],
				$_REQUEST['currentcontrols'] ,
				$_REQUEST['percieved_control'] ,
				$_REQUEST['control_effectiveness'] ,
				$_REQUEST['user_responsible'],
				(isset($_SESSION['uploads']) ? serialize($_SESSION['uploads']) : "")
		);
		$id = $risk -> saveRisk();
		$response = array();
		$text 	  = "";
		if($id > 0)
        {
			$nameObj = new Naming();
			$names = $nameObj->getReportingNaming();
			$response["id"] 	= $id;
			$state 				= false;
			$text 				= $risk_object_name." #".$id." has been successfully saved <br />";
			//Get recipients emails
			$user = new UserAccess();
				//Risk Owners (Sub-Dir Admins)
				$to = $user->getRiskOwnerEmails($_REQUEST['user_responsible']);
				//Risk Managers
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
				$cc2 = array();
				foreach($ccm as $c) {
					$cc2[] = $c['email'];
				}
				$cc = implode(",",$cc2);
			//Get From
				$from = $user->getCurrentUser();
			//Subject
			$subject = "New ".$risk_object_name." [".$id."]";
			//Create message
			$rr = $risk->getCalculation("RRE");
			$ir = $risk->getCalculation("IRE");
$message = $from['name']." has created a new ".$risk_object_name.".

The details are as follows:
".$names['risk_item'].": ".$id."
".$names['risk_type'].": ".$_REQUEST['type_text']."
".$names['risk_level'].": ".$_REQUEST['level_text']."
".$names['risk_category'].": ".$_REQUEST['category_text']."
".$names['risk_description'].": ".$_REQUEST['description']."
".$names['risk_background'].": ".$_REQUEST['background']."
".$names['impact'].": ".$_REQUEST['impact_text']." (".$names['impact_rating'].": ".$_REQUEST['impact_rating'].")
".$names['likelihood'].": ".$_REQUEST['likelihood_text']." (".$names['likelihood_rating'].": ".$_REQUEST['likelihood_rating'].")
".$names['inherent_risk_exposure'].": ".$ir['name']." (".$names['inherent_risk_rating'].": ".$ir['rating'].")
".$names['percieved_control_effective'].": ".$_REQUEST['percieved_control_text']." (".$names["control_rating"].": ".$_REQUEST['control_effectiveness'].")
".$names['residual_risk_exposure'].": ".$rr['name']." (".$names['residual_risk'].": ".$rr['rating'].")
".$names['current_controls'].": ".implode("; ",explode("_",$_REQUEST['currentcontrols']))."
".$names['financial_exposure'].": ".$_REQUEST['financial_exposure_text']."
".$names['actual_financial_exposure'].": ".$_REQUEST['actual_financial_exposure']."
".$names['risk_owner'].": ".$_REQUEST['user_responsible_text']."

Please log onto Assist in order to View or Update the $risk_object_name.
";
			//new ASSIST_EMAIL object
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			//send email
			$email->sendEmail();
/*			$user      		    = new UserAccess("", "", "", "", "", "", "", "", "" );
			$managerEmailTo     = $user -> getRiskManagerEmails();				
			$emailTo      		= $user -> getUserEmailUnderDirectorate( $_REQUEST['user_responsible'] );
			$email 			    = new Email();
			//$names 				= new Naming();
			$header 			= $nameObj -> getHeaderNames();
			$email -> userFrom  = $_SESSION['tkn'];
			$email -> subject   = "New ".$risk_object_name;
			$message   = ""; 
			$message .= $email -> userFrom." has created a New $risk_object_name #".$id." \n\n";
			$message .= "The details of the $risk_object_name are as follows : \r\n";
			$message .= $names["risk_item"].": ".$id."\r\n"; 
			$message .= $names["risk_type"].": ".$_REQUEST['type_text']."\r\n";
			$message .= $names["level"].": ".$_REQUEST['level_text']."\r\n";			
			$message .= $names["risk_category"].": ".$_REQUEST['category_text']."\r\n";		
			$message .= $names["risk_description"].": ".$_REQUEST['description']."\r\n";
			$message .= $names["risk_background"].": ".$_REQUEST['background']."\r\n";
			$message .= $names["impact"].": ".$_REQUEST['impact_text']."\r\n";
			$message .= $names["impact_rating"].": ".$_REQUEST['impact_rating']."\r\n";
			$message .= $names["likelihood"].": ".$_REQUEST['likelihood_text']."\r\n";
			$message .= $names["likelihood_rating"].": ".$_REQUEST['likelihood_rating']."\r\n";
			$message .= $names["current_controls"].": ".$_REQUEST['currentcontrols']."\r\n";
			$message .= $names["percieved_control_effective"].": ".$_REQUEST['percieved_control_text']."\r\n";
			$message .= $names["control_rating"].": ".$_REQUEST['control_effectiveness']."\r\n";
			$message .= $names["financial_exposure"].": ".$_REQUEST['financial_exposure_text']."\r\n";		
			$message .= $names["actual_financial_exposure"].": ".$_REQUEST['actual_financial_exposure']."\r\n";		
			$message .= $names["kpi_ref"].": ".$_REQUEST['kpi_ref']."\r\n";																		
			$message .= $names['risk_owner'].": ".$_REQUEST['user_responsible_text']."\r\n";					
			$message .= " \r\n\n";	//blank row above Please log on line																		
			$message .= "Please log onto Assist in order to View or Update the $risk_object_name and to assign any required ".$action_object_name."(s) to responsible users. \r\n";			
			$message .= " \n";	//blank row above Please log on line
			$message  = nl2br($message);			
			$email -> body     = $message;
			
			$email->to 	= ($emailTo == "" ? "" : $emailTo.",")."".($managerEmailTo == "" ? "" : $managerEmailTo);
			$email -> from     = "no-reply@ignite4u.co.za";
			if( $email -> sendEmail() ) {
				//array_push($response, "and a notification has been sent");
					$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email has been sent";
			} else {
				$state = true;
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}*/
			$response = array("text" => $text, "error" => $state , "saved" => true);
		} else {
			$response = array("text" => "Error saving the ".$risk_object_name, "error" => true, "saved" => false);
		}
		if( isset($_SESSION['uploads'])) {
			unset($_SESSION['uploads']);
		} 
		echo json_encode( $response );
		break;
	case "saveAttachment":
	
			//$upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/";
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref'])){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']) ){
					if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk") ){
						mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/deleted");
					}
				}					
			}
			$upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/risk/";
			$key 	  				   = key($_FILES);
			$filename 				   = basename($_FILES[$key]['name']);
			$uploadname 			   = $key."".time();
			$_SESSION['uploads'][$uploadname] = $_FILES[$key]['name'];
			$ext 					   = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			$uploadnames[] 			   = $uploadname; 
			if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$uploadname.".".$ext) )
			{			
				$response = array(
									"text" 			=> "File uploaded .....",
									"error" 		=> false,
									"filename"  	=> $filename,
									"key"			=> $key,
									"filesuploaded" => $_SESSION['uploads'],
									"uploadname"	=> $uploadnames
									);
			} else {
				$response = array("text" => "Error uploading file ...." , "error" => true );
			}		
			echo json_encode( $response );
		break;
    case "processActionAttachments":
    	
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref'])){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ){
					if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
						mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted");					
					}
				}					
			}  else if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
				if(mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted");					
				}
			}  	
            $upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/";
            $key 	  				            = key($_FILES);
            $filename 				            = basename($_FILES[$key]['name']);
            $_SESSION['uploads']['action'][$key.time()]   = $_FILES[$key]['name'];
            $fileuploaded 			            =  $key."".time().".";
            $ext 					            = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );

            if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
            {
                $response = array(
                        "text" 			=> "File uploaded .....",
                        "error" 		=> false,
                        "filename"  	=> $filename,
                        "key"			=> $key,
                        "filesuploaded" => $_SESSION['uploads']['action']
                        );
            } else {
				$response = array("text" => "Error uploading file ...." , "error" => true );
            }
            echo json_encode( $response );
        break;
    case "removeFile":
    		$filename = $_POST['id'];
    		$response = array();
    		if( isset($_SESSION['uploads'][$filename]))
    		{
    			unset($_SESSION['uploads'][$filename] );
    		} else {
    			unset($_SESSION['uploads'][$filename] );
    		}		
    		echo 1;
    	break;
    case "removeActionFile":
    		$filename = $_POST['id'];
    		if( isset($_SESSION['uploads']['action'][$filename]))
    		{
    			unset( $_SESSION['uploads']['action'][$filename]);
    			echo "1";
    		} else {
    			echo "-1";	
    		}
    	break;
	case "newRiskAction":
		$risk = new RiskAction( 
				$_REQUEST['id'] ,
				$_REQUEST['r_action'] ,
				$_REQUEST['action_owner'] ,
			    $_REQUEST['deliverable'] ,
		 		$_REQUEST['timescale'] ,
				$_REQUEST['deadline'] ,
				$_REQUEST['remindon'] ,
				$_REQUEST['progress'],
				$_REQUEST['status']
		);
		$id 	   = $risk -> saveRiskAction();
		$response  = array(); 
		if( $id > 0 )
        {
			$nameObj = new Naming();
			$names = $nameObj->getReportingNaming("action");
			$rnames = $nameObj->getReportingNaming();

			$state 				 = false;		
			$text				 = "New $action_object_name #".$id." has been successfully saved <br />";
			//Get recipients emails
			$user = new UserAccess();
				//Action Owner
				$to = $user->getActionOwnerEmail($_REQUEST['action_owner']);
				unset($to['tkid']);
				//Risk Managers
				$ccm = $user->getRiskManagers(true);	//emailsOnly = true returns array of name & email
				$cc2 = array();
				foreach($ccm as $c) {
					$cc2[] = $c['email'];
				}
				$cc = implode(",",$cc2);
			//Get From
				$from = $user->getCurrentUser();
			//Set the subject
			$subject = "New $action_object_name [".$id."]";
			//Set the message
			$risk_id = $_REQUEST['id'];
			$risk 				 = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
			$riskInfo 			 = $risk -> getRisk($risk_id);

$message = $from['name']." has created a new ".$action_object_name." related to $risk_object_name ".$risk_id.".

The $risk_object_name details are as follows:
".$rnames['risk_description'].": ".$riskInfo['description']."
".$rnames['risk_owner'].": ".$riskInfo['risk_owner']."

The $action_object_name details are as follows:
$action_object_name Ref: ".$id." 
".$names['risk_action'].": ".$_REQUEST['r_action']."
".$names['deliverable'].": ".$_REQUEST['deliverable']."
".$names['action_owner'].": ".$to['name']."
".$names['timescale'].": ".$_REQUEST['timescale']."
".$names['deadline'].": ".$_REQUEST['deadline']."

Please log onto Assist in order to View or Update the $action_object_name.
";
			//new ASSIST_EMAIL object
			$email = new ASSIST_EMAIL($to,$subject,$message,"TEXT",$cc);
			//send email
			$email->sendEmail();
/*			$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo  		     = $user -> getRiskManagerEmails();
			$responsibleUserEmail= $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
			$email 			     = new Email();
			$email -> userFrom   = $_SESSION['tkn'];
			$risk 				 = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
			$riskInfo 			 = $risk -> getRisk($_REQUEST['id']);
			$email -> subject    = "New ".$action_object_name;
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message  = "";			
			$message .= $email -> userFrom." created an $action_object_name relating to $risk_object_name #".$_REQUEST['id'];
			$message .= " : ".$riskInfo['description']." identified in the $risk_object_name Register. The details of the $action_object_name are as follows :";
			$message .= " \r\n\n";	//blank row above Please log on line
			$message .= "$action_object_name Ref : ".$id."\r\n"; 
			$message .= $names['risk_name'].": ".$_REQUEST['r_action']."\r\n";
			$message .= $names['deliverable'].": ".$_REQUEST['deliverable']."\r\n";
			$message .= $names['action_owner'].": ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
			$message .= $names['timescale'].": ".$_REQUEST['timescale']."\r\n";
			$message .= $names['deadline'].": ".$_REQUEST['deadline']."\r\n";
			$message .= " \r\n\n";	//blank row above Please log on line																		
			$message .= "<i>Please log onto Assist in order to View or Update the $action_object_name.</i> \n";
			$message  = nl2br($message);			
			$email -> body  = $message;
			$email -> to = ($emailTo == "" ? "" : $emailTo.",")."".($responsibleUserEmail['tkemail'] == "" ? "" : $responsibleUserEmail['tkemail']);
			$email -> from     = "no-reply@ignite4u.co.za";
			if( $email -> sendEmail() ) {
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email has been sent";
			} else {
				$state	= true;
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}*/
			$response = array("text" => $text, "error" => $state, "saved" => true, "id" => $id);
		} else {
			$response = array("text" => "Error saving the $action_object_name \r\n&nbsp;&nbsp;&nbsp;&nbsp;<br />".$id, "error" => true , "saved" => false );
		}
        unset($_SESSION['uploads']['action']);
		echo json_encode( $response );
		break;
	case "updateRisk":
		$risk = new Risk( 	
				$_REQUEST['type'] ,
				$_REQUEST['category'] ,
				$_REQUEST['description'] ,
			    $_REQUEST['background'] ,
		 		$_REQUEST['impact'] ,
				$_REQUEST['impact_rating'] ,
				$_REQUEST['likelihood'] ,
				$_REQUEST['likelihood_rating'],
				$_REQUEST['currentcontrols'] ,
				$_REQUEST['percieved_control'] ,
				$_REQUEST['control_effectiveness'] ,
				$_REQUEST['user_responsible'],
				serialize($_REQUEST['attachment'])
				);
		$risk -> updateRisk( $_REQUEST['id'] , $_REQUEST['status'] );
	case "getRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode( $risk -> getAllRiskTogether( $_REQUEST['start'], $_REQUEST['limit'] ) );
		break;	
	case "getARisk":
		$riskObj  = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		$risk     = $riskObj -> getRisk( $_REQUEST['id'] );
		$risk['current_controls'] = str_replace("_", ", ", $risk['current_controls'] );
		echo json_encode( $risk );
		break;			
	case "getARiskWithHeaders":
		$riskObj  = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		$risk     = $riskObj -> getRiskWithHeaders( $_REQUEST['id'] );
		$cc = explode("_",$risk['riskData']['current_controls']);
		$cc2 = array();
		foreach($cc as $c) {
			if(strlen(trim($c))>0) {
				$cc2[] = trim($c);
			}
		}
		$risk['riskData']['current_controls'] = implode(";<br />",$cc2);
//		$risk['riskData']['current_controls'] = str_replace("_", ",<br />", $risk['riskData']['current_controls'] );
		echo json_encode( $risk );
		break;			
	case "getDefaults":
		$default = new Defaults("","");
		echo json_encode( $default -> getDefaults() );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		//$ditc = $dir -> getDirectorateResponsible();
		echo json_encode( $dir -> getDirectorateResponsible() );
		break;
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;		
	case "getRiskLevel":
		$level = new Risklevel( "", "", "", "" );
		echo json_encode( $level-> getActiveLevel() );
		break;						
	case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );	
		break;				
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getActiveTypeCategory( $_REQUEST['id']) );
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		echo json_encode( $finExp -> getActiveFinancialExposure() );
		break;		
	case "getActiveImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getActiveImpact() );
		break;		
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getActiveLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getActiveControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getUserAccess":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUserAccess() );
		break;	
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;	
	case "getRiskStatus":
		$getstat = new RiskStatus( "", "", "" );
		echo json_encode( $getstat -> getStatus() );
		break;	
	case "deleteAction":
			$act = new RiskAction("", "", "", "", "", "", "","", "");
			$res = $act -> deleteRiskAction( $_REQUEST['id'] );
			$response = array();
			if( $res > 0)
			{
				$response = array("text" => "$action_object_name successfully deleted . . ", "error" => false);		
			} else {
				$response = array("text" => "Error deleting $action_object_name . . ", "error" => true);		
			}
			echo json_encode( $response );
		break;	
	case "activateAction":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		$risk -> activateRiskAction( $_REQUEST['id'] );
	break;
	case "getRiskAssurance":
		$rskass = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		//echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		break;	
	case "getRiskAssurances":
		$rskass = new RiskAssurance($_REQUEST['id'] , "", "", "", "", "");
		echo json_encode( $rskass -> getRiskAssurance() );
		break;
	case "newRiskAssurance":
		$rskass = new RiskAssurance($_REQUEST['id'] , "", $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment']);
		$rskass -> saveRiskAssurance() ;
		break;
	case "saveNotification":
		$usernot = new UserNotification();
		$usernot -> saveNotifications();
		break;
	case "getNotifications":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> getNotifications() );
		break;
	case "getStatus":
		$stat = new RiskStatus("", "", "");
		echo json_encode( $stat -> getStatus() );
		break;
	case "getAddActions":
			$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
			$colmObj = new ActionColumns();
			$options = "";
			if( $_GET['section'] == "manage")
			{
				$options = " AND active & 4 = 4";	
			} else {
				$options = " AND active & 8 = 8";
			}
			$headers 	 = $colmObj -> getHeaderList( $options );
			$riskActions = $act -> getRiskActions( $_GET['start'], $_GET['limit'] );
			$actions 	 = array();
			$status      = array();
			$progress	 = array();
			$headerArr   = array(); 
			$actionOwner = array();
			foreach( $headers as $field => $head)
			{
				foreach( $riskActions as $index => $valArr)
				{
					$status[$valArr['id']]  	= $valArr['status'];
					$actionOwner[$valArr['id']] = $valArr['isUpdate'];
					if( array_key_exists($field, $valArr))
					{
						$actions[$valArr['id']][$field]  = ($field == "progress" ? $valArr[$field]."%" :  $valArr[$field]);
						$headerArr[$field]				 = $head;			
					}	
				}	
			}	
			$actionStats = $act-> getActionStats();
			echo json_encode( array("data" => $actions, "status" => $status, "headers" => $headerArr, "columns" => count($headerArr),"statistics" => $actionStats, "owners" => $actionOwner ,"rawHeaders" => $headers, "countRaw" => count($headers) ));
		break;	
		default:
			echo "It hasn't found the actions associated";
		break;
	}
	

?>