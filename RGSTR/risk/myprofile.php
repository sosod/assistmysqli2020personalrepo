<?php
$scripts = array( 'myprofile.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "My profile";
require_once("../inc/header.php");
?>
<form method="post" name="profile-form" id="profile-form">
<h4>Notifications</h4>
<div id="profile_message"></div>
<table border="1" id="profile" align="left">
             	<tr>
                	<th>Notification #</th>                  
                	<th>Notification</th>                  
                	<th>Risk</th>                  
                	<th>Action</th>                 
                	<th>Date</th>                  
                	<th>Action</th>                                                                              
                </tr>
               	<tr>
                 	<!-- <td colspan="6"><input type="submit" name="new_notification" id="new_notification" value="New" /></td>                                                                             --> 
                </tr>
</table>
 <div>
	<table id="notification" border="1" style="display:none; clear:both;" align="left">
    <tr>
		<th colspan="2">
        	Reminder Emails
        </th>                     
        <td></td>
     </tr>
      <tr>
     	<td>Risk</td>
        <td>
        	<select name="recieve_risk" id="recieve_risk">
            	<option>-select risk-</option>              
            </select>
            <select name="recieve_risk_aciton" id="recieve_risk_action" style="display:none;">
            	<option>-select action-</option>              
            </select>
        </td>
     </tr>
     <tr>
     	<td>Recieve</td>
        <td>
        	<select name="recieve" id="recieve">
            	<option value="">-select-</option>
                <option value="1">yes</option>
                <option value="0">no</option>                
            </select>
        </td>
     </tr>
     <tr>
     	<td>When</td>
        <td>
        	<select name="recieve_when" id="recieve_when">
            	<option value="">-select-</option>
                <option value="weekly">Weekly</option>
                <option value="daily">Daily</option>                                
            </select>
            <select id="recieve_day" name="recieve_day" style="display:none">
            	<option value="">--select day--</option>
            	<option value="mon">Monday</option>                
            	<option value="tues">Tuesday</option>                
            	<option value="wed">Wednesday</option>                
            	<option value="thurs">Thursday</option>                
            	<option value="fri">Friday</option>                
            	<option value="sat">Saturday</option>                
            	<option value="sun">Sunday</option>                
            </select>
        </td>
     </tr>
     <tr>
     	<td>What</td>
        <td>
        	<select name="recieve_what" id="recieve_what">
            	<option value="">---</option>
                <option value="due_this_week">Actions due this week</option>
                <option value="due_on_or_befor_week">Actions due on or before this week</option>
                <option value="due_today">Actions due today</option>
                <option value="due_on_or_before_today">Actions due on or before today</option>
                <option value="all_incomplete_actions">All Incomplete actions</option>
            </select>
        </td>
     </tr>
     <tr>
     	<td><input type="submit" name="update" id="update" value="Update Profile" /></td>
        <td><input type="submit" name="reset" id="reset" value="Reset" /></td>
     </tr>
    </table>
   </div>
<input type="hidden" name="risk_id" value="<?php //echo $_REQUEST['id']; ?>" id="risk_id"  /> 
</form>
</body>
</html>
