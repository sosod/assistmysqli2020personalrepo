<?php
$page = "index";
$folder = "risk";

$scripts = array( 'risk.js','menu.js', 'ajaxfileupload.js', "ignite.textcounter.js");
$styles = array();
$page_title = "";
require_once("../inc/header.php");

if($userInfo['create_risks']!=1) {
	die("<p>You do not have access to create any ".$risk_object_name."s.</p>");
}

$rkCols  = new RiskColumns();
$columns = $rkCols -> getHeaderList();

$financial_year_obj = new MasterFinancialYear();
$financial_years    = $financial_year_obj->retrieveMasterFinacialYears("active");


function nameFields( $fieldId , $defaultName ){
	echo getNameFields($fieldId,$defaultName);
}

function getNameFields($fieldId,$defaultName) {
	global $columns;
	if( isset( $columns[$fieldId] )) {
		return $columns[$fieldId];
	} else {
		return $defaultName;
	}
}
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="new-risk-form" method="post" enctype="multipart/form-data">
<table width="" border="0" id="risk_new" class=form>
 <tr>
        <th><?php nameFields('financial_year','Financial Year'); ?>:</th>
        <td>
            <select id="financial_year_id" name="financial_year_id">
                <option selected value="0">Unspecified</option>
                <?php foreach($financial_years as $index => $financial_year): ?>
                    <option value="<?php echo $financial_year['id']; ?>"><?php echo $financial_year['value']; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
      </tr>
    <tr>
        <th><?php nameFields('rbap_ref','RBAP Ref'); ?>:</th>
        <td>
            <input id="rbap_ref" name="rbap_ref" type="text" />
        </td>
    </tr>
	<tr>
        <th id=th_type><?php nameFields('risk_type','Risk Type'); ?>:</th>
        <td>
            <select id="risk_type" name="risk_type">
                <option value="">--- SELECT ---</option>
            </select>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
        <td>
            <select id="risk_level" name="risk_level">
                <option value="">--- SELECT ---</option>
            </select>
        </td>
      </tr>      
      <tr>
        <th><?php nameFields('risk_category','Risk category'); ?>:</th>
        <td>
            <select id="risk_category" name="risk_category">
                <option value="">--- SELECT <?php echo strtoupper(getNameFields('risk_type','Risk Type')); ?> ABOVE ---</option>
            </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('risk_description','Risk Description'); ?>:</th>
        <td><textarea class="mainbox" name="risk_description" id="risk_description"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('risk_background','Background Of Risk'); ?>:</th>
        <td><textarea class="mainbox" name="risk_background" id="risk_background"></textarea></td>
      </tr>
    <tr>
        <th><?php nameFields('cause_of_risk','Cause Of Risk'); ?>:</th>
        <td>
           <textarea id="cause_of_risk" name="cause_of_risk"></textarea>
        </td>
    </tr>
    <tr>
        <th><?php nameFields('reasoning_for_mitigation','Reasoning For Mitigation'); ?>:</th>
        <td>
            <textarea id="reasoning_for_mitigation" name="reasoning_for_mitigation"></textarea>
        </td>
    </tr>
      <tr>
        <th id=th_impact><?php nameFields('impact','Impact'); ?>:</th>
        <td>
        <select id="risk_impact" name="risk_impact">
                <option value="">--- SELECT ---</option>
         </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('impact_rating','Impact Rating'); ?>:</th>
        <td>
        <select id="risk_impact_rating" name="risk_impact_rating">
                <option value="">--- SELECT <?php echo strtoupper(getNameFields('impact','Impact')); ?> ABOVE ---</option>
         </select>        
        </td>
      </tr>
      <tr>
        <th id=th_likelihood><?php nameFields('likelihood','Likelihood'); ?>:</th>
        <td>
        <select id="risk_likelihood" name="risk_likelihood">
                <option value="">--- SELECT ---</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?>:</th>
        <td>
        <select id="risk_likelihood_rating" name="risk_likelihood_rating">
                <option value="">--- SELECT <?php echo strtoupper(getNameFields('likelihood','Likelihood')); ?> ABOVE ---</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('current_controls','Current Controls'); ?>:</th>
        <td>
           <p><textarea id="risk_currentcontrols" name="risk_currentcontrols" class="controls"></textarea></p>
            <input type="submit" name="add_another_control" id="add_another_control" value="Add Another"  />         
        </td>
      </tr>
      <tr>
        <th id=th_risk_percieved_control><?php nameFields('percieved_control_effective','Perceived Control Effectiveness'); ?>:</th>
        <td>
        <select id="risk_percieved_control" name="risk_percieved_control">
                <option value="">--- SELECT ---</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('control_rating','Control Effectveness Rating'); ?>:</th>
        <td>
        <select id="risk_control_effectiveness" name="risk_control_effectiveness">
                <option value="">--- SELECT ---</option>
         </select>            
        </td>
      </tr>
      <tr>
        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>
        <select id="financial_exposure" name="financial_exposure">
                <option value="">--- SELECT ---</option>
         </select>            
        </td>
      </tr>      
      <tr>
        <th><?php nameFields('actual_financial_exposure','Actual Financial Exposure'); ?>:</th>
        <td>
        	<?php echo ASSIST_HELPER::NUMBER_CURRENCY_SYMBOL."&nbsp;"; ?><input id="actual_financial_exposure" name="actual_financial_exposure" type="text" />   (Numbers only)   
        </td>
      </tr>  
      <tr>
        <th><?php nameFields('kpi_ref','KPI Ref'); ?>:</th>
        <td>
        	<input id="kpi_ref" name="kpi_ref" type="text" />     
        </td>
      </tr>                  
      <?php 
        //if ($setups['risk_is_to_directorate'] == 1 ) { 
      ?>
     <tr>
        <th><?php nameFields('risk_owner','Risk Owner'); ?>:</th>
        <td>
        <select id="directorate" name="directorate" class="responsibility">
                <option value="">--- SELECT ---</option>
         </select>        
        </td>
    </tr>
<!--      <tr>
        <th><?php nameFields('','Attach a document'); ?>:</th>
        <td><?php ASSIST_HELPER::displayResult(array("error","This functionality has been temporarily disabled.  It will be restored as soon as possible.")); ?>
        </td>
      </tr> -->
      <tr>
         <td align="right">
         <!-- Import Rsik
            <input id="importrisk" name="importrisk_<?php echo date("YmdHis"); ?>" type="file" />                          
            -->
       </td>      
        <td>
            <input type="submit" name="add_risk" id="add_risk" value=" Add <?php echo $risk_object_name; ?> " class="isubmit"/>
            <!-- <?php if($setups['actions_aplied_to_risks'] == 1 ) { ?>
                <input type="submit" name="show_add_action" id="show_add_action" value="Add Action " />
            <?php } ?>  -->      
         </td>
      </tr>
   </table>
</form>
</div>

