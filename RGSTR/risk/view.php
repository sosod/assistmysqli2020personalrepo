<?php
$scripts = array( 'view.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>

<h4>Risk Register</h4>

        <table align="left" id="risk_table" border="1">
        	<tr>
            	<td  id="paging" colspan="15" align="left">
					<?php
						echo "Here we are ";
					 drawPaging("", "", "", "", "", "", "", ""); ?>             
                </td>
            </tr>
            <tr>
                <th><?php nameFields('riskitem','Risk Item'); ?></th>
                <th><?php nameFields('risktype','Type'); ?></th>
                <th><?php nameFields('riskcategory','Category'); ?></th> 
                <th><?php nameFields('riskdescription','Description'); ?></th>            
                <th><?php nameFields('background','Background'); ?></th> 
                <th><?php nameFields('impact','Impact'); ?></th> 
                <th><?php nameFields('impact_rating','Impact Rating'); ?></th> 
                <th><?php nameFields('likelihood','Likelihood'); ?></th> 
                <th><?php nameFields('likelihood_rating','Likelihood Rating'); ?></th> 
                <th><?php nameFields('current_controls','Current Controls'); ?></th> 
                <th><?php nameFields('percieved_control_effectiveness','Percieved Control Effectiveness'); ?></th> 
                <th><?php nameFields('control_effectiveness_rating','Control Effectiveness Rating'); ?></th> 
                <th><?php nameFields('riskowner','Responsibilty of the risk'); ?></th> 
                <th><?php nameFields('status','Status'); ?></th> 
                <th><?php nameFields('','Attachements'); ?></th> 
              <!--  <th><?php nameFields('','Actions'); ?></th> -->
            </tr>
        </table>
<!--<form method="post">
<table width="100%" id="risk_table" border="1">
    <tr>
        <th></th>
        <th>Risk Item</th>
        <th>Type</th>
        <th>category</th> 
        <th>Description</th>            
        <th>Background</th> 
        <th>Impact</th> 
        <th>Impact Rating</th> 
        <th>Likelihood</th> 
        <th>Likelihood Rating</th> 
        <th>Current Controls</th> 
        <th>Precieved Control Effectiveness</th> 
        <th>Control Effectiveness Rating</th> 
        <th>Responsibilty of the risk</th> 
        <th>Status</th> 
        <th>Attachements</th> 
        <th>Actions</th>
    </tr>
<?php
   foreach( $rsk as $row ){
   ?>
	    <tr>
        <td></td>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['type']; ?></td>
        <td><?php echo $row['category']; ?></td> 
        <td><?php echo $row['risk_descr']; ?></td>            
        <td><?php echo $row['background']; ?></td> 
        <td><?php echo $row['impact']; ?></td> 
        <td><?php echo $row['impact_color']; ?></td> 
        <td><?php echo $row['likelihood']; ?></td> 
        <td bgcolor="<?php echo $row['like_color']; ?>"></td> 
        <td><?php echo str_replace(",","\r\n\n",$row['current_controls']); ?></td> 
        <td><?php echo $row['effectiveness']; ?></td> 
        <td bgcolor="<?php echo $row['effect_color']; ?>"></td> 
        <td><?php echo $row['user']; ?></td> 
        <td><?php echo $row['type']; ?>Status</td> 
        <td><?php echo $row['type']; ?>Attachements</td> 
        <td>
     	<table>
      <tr>
     <td>Risk</td>
    <td><input type="submit" name="view" id="view" value="View" class="btn" /></td>
    <td><input type="submit" name="edit" id="edit" value="Edit" class="btn" /></td>
    <td><input type="submit" name="update" id="update" value="Update" class="btn" /></td>
    <td><input type="submit" name="approve" id="approve" value="Approve" class="btn"/></td>
   <td><input type="submit" name="assurance" id="assurance" value="Assurance" class="btn" /></td 
     </tr>
    <tr>
     <td>Action</td>
    <td><input type="submit" name="view_action" id="view_action" value="View" class="btn" /></td>
    <td><input type="submit" name="edit_action" id="edit_action" value="Edit" /></td>
    <td><input type="submit" name="approve_action" id="approve_action" value="Approve" /></td>
    <td><input type="submit" name="assurance_action" id="assurance_action" value="Assurance" />
        <input type="hidden" name="riskid" value="<?php echo $row['id']; ?>"  />
    </td>
     </tr>
           </table>                                  
        </td>
    </tr>  
  <?php
   } 
?>    
</table>
</form>-->
<script language="javascript">
	$(function(){
		$("#demo5").paginate({
				count 		: 10,
				start 		: 1,
				display     : 7,
				border					: true,
				border_color			: '#fff',
				text_color  			: '#fff',
				background_color    	: 'black',	
				border_hover_color		: '#ccc',
				text_hover_color  		: '#000',
				background_hover_color	: '#fff', 
				images					: false,
				mouse					: 'press',
				onChange     			: function( page ){
											$('._current','#paginationdemo').removeClass('_current').hide();
											$('#p'+page).addClass('_current').show();
										  }
			});
	});
</script>
</div>	
</body>
</html>
