<?php
/*function __autoload($classname){
	if( file_exists( '../class/'.strtolower($classname).".php" ) ){
		require_once( '../class/'.strtolower($classname).".php" );
	}
}*/
$menu = new Menu();
$links  = $menu->getMenu();
?>
<ul id="menulist">
<?php
foreach($links as $link) {
	if( $link['1'] == 0 ) {
?>
	<li id="#<?php echo strtolower($link['3']); ?>"><a href="" ><?php  echo $link['2']; ?></a></li>
<?php	
	}
}
?>
</ul>
<div id="menu-submenu">
<div id="manage" class="submenu">
    <ul>
        <li><a href="" id="view">View</a></li>
        <li><a href="" id="edit">Edit</a></li>
        <li><a href="" id="update">Update</a></li>
        <li><a href="" id="approve">Approve</a></li>
        <li><a href="" id="myprofile">My Profile</a></li>                
        <li><a href="" id="assurance">Assuarance</a></li>    
    </ul>
</div>
<div id="report" class="submenu">
    <ul>
        <li><a href="" id="generate">Generate</a></li>
        <li><a href="" id="quick">Quick Report</a></li>
        <li><a href="" id="fixed">Fixed</a></li>
        <li><a href="" id="edr">EDR</a></li>
        <li><a href="" id="graph">Graph</a></li>                
        <li><a href="" id="statistics">Statistics</a></li>    
    </ul>
</div>
<div id="setup" class="submenu">
    <ul>
        <li><a href="" id="defaults">Defaults</a></li>
        <li><a href="" id="users">User Access</a></li>
        <li><a href="" id="notifications">Notifications</a></li>
    </ul>
</div>