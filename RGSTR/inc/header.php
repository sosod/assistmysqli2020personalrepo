<?php
//include_once("inc_ignite.php");
//@session_start();

//error_reporting(-1);
//require_once("../../inc_assist.php");
include_once("init.php");
$me = new ASSIST_MODULE_HELPER();
$db 	 = new DBConnect();
$default = new Defaults("","");
$admire_helper = new ADMIRE_ASSIST_HELPER();



/*$risk_object_name = isset($_SESSION[strtoupper($_SESSION['modref']]['object_name'])) ? $_SESSION[$_SESSION['modref']]['object_name'] : "";
$action_object_name = isset($_SESSION[$_SESSION['modref']]['action_name']) ? $_SESSION[$_SESSION['modref']]['action_name'] : "";

$risk_object_name = strlen($risk_object_name)>0 ? $me->getObjectName("OBJECT") : "";
$action_object_name = strlen($action_object_name)>0 $me->getObjectName("ACTION") : "";

$risk_object_name = strlen($risk_object_name)>0 ? $risk_object_name : "Risk";
$action_object_name = strlen($action_object_name)>0 ? $action_object_name : "Action";
*/

$risk_object_name = $default->getObjectName("OBJECT"); //echo $risk_object_name;
$action_object_name = $default->getObjectName("ACTION"); //echo $action_object_name;

	$requestUri  = preg_split('[\\/]', $_SERVER['REQUEST_URI'], -1, PREG_SPLIT_NO_EMPTY	);//explode("/",$_SERVER['REQUEST_URI']);
	$count 		 = count($requestUri);
	$pagename 	 = "";
	$basename    = ""; //foldername used to get the submenu 
    foreach( $requestUri as $key => $val)
	{
		if( $val == $_SESSION['modlocation'])
		{
			continue;
		} else {	
			//if the page has a php extenstion get the basename, 
			if( strpos($val,".") > 0){
				$pagename = substr($val, 0, strpos($val, "."));
				$basename = $requestUri[$count-2];
			} else {
				$basename = $val;
				$pagename = $val;
			}				
		}		
	}
	$links	    = array();
	$assuranceV = array();
	$user 		= new UserAccess( "", "", "", "", "", "", "", "", "");
	$userInfo 	= $user -> getUser( $_SESSION['tid'], true);
	$is_risk_manager = $user->isARiskManager();
//	$me->arrPrint($userInfo);

	if( (isset($userInfo) && !empty($userInfo)) || $_SESSION['tid'] == "0000" )
    {
    	$menu 		= new Menu();
   	 	$links  	= $menu -> getSubMenu( (($basename == "actions") ? "manage"  : $basename) ); 
		$defObj     = new Defaults();
		$assuranceV = $defObj -> getADefault(6);
	
				
			if(!empty( $links )){
			
				$mainMenu = array();
				if(strpos($pagename, ".") > 0){ 
					$page_name = substr($pagename, 0, strpos($pagename, "."));
				} else {
					$page_name = $pagename;
				}
			//echo $pagename." :: ".$page_name;
			$pn = explode("_","_".$pagename);
			$pn[0] = $basename;
//$me->arrPrint($pn);	
//$me->arrPrint($links);		
				//$page_name   = ($pagename == "manage" ? "view" ? $pagename);
				//(($page_name ==  strtolower(($link['client_name'] == "" ? $link['name'] : $link['client_name'])) )? "Y" : "");

				foreach( $links as $link ) { 
					$lf = explode("_",$link['folder']);
					$active = "";
					if(strtolower($link['client_name']) == "defaults")
					{
						$user = array("risk_manager", "user_access", "useraccess");
						if(in_array($page_name, $user))
						{
							$page_name = "useraccess";
						} else {
							$page_name = "defaults"; 
						}
					}

					$client_name = empty($link['client_name']) ? $link['client_name'] : "";
					if( ($page_name == strtolower($client_name)) || (!empty($client_name) && strstr($page_name, $client_name))){
						$active = "Y";		
					} else if( $page_name == strtolower($link['folder']) || strstr($page_name, strtolower($link['folder']))) {
						$active = "Y";
					} else if($page_name == strtolower($link['name'])) {
						$active = "Y";		
					}		
					if( $link['folder'] == "assurance" ){
						if($assuranceV['value'] == 1 && $userInfo['assurance'] == 1){
						//echo $page_name." is active ".(($page_name ==  strtolower(($link['client_name'] == "" ? $link['name'] : $link['client_name'])) )? "Y" : "")."<br /><br />";
						$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php",'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
						}
					} else if($lf[0] == 'admin'){ //echo "lf[0] = admin";
                        if($link['folder']=="admin_edit" && $userInfo['edit_all'] == 1) {
								if($active!="Y" && $pn[1]=="edit") { $active = "Y"; }
                                $mainMenu[$link['folder']] = array("id" => $link['folder'], "url" => "../admin/".strtolower($link['folder']).".php", 'active'=> $active, 'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']));
                                //$pagename = 'admin_edit';
                        } elseif($link['folder']=="admin_update" && $userInfo['view_all'] == 1) { 
							if($active!="Y" && $pn[1]=="update") { $active = "Y"; }
                                $mainMenu[$link['folder']] = array("id" => $link['folder'], "url" => "../admin/".strtolower($link['folder']).".php", 'active'=> $active, 'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']));
                                //$pagename = 'admin_update';
                        } elseif($link['folder']=="admin_approve" && $userInfo['risk_manager'] == 1) { 
							if($active!="Y" && $pn[1]=="approve") { $active = "Y"; }
                                $mainMenu[$link['folder']] = array("id" => $link['folder'], "url" => "../admin/".strtolower($link['folder']).".php", 'active'=> $active, 'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']));
                                //$pagename = 'admin_update';
                        }
                    } else {
						if($userInfo['risk_manager'] == 1){
							$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php",'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
						} else {
							if($userInfo['view_all'] == 1 && $link['folder'] == "view"){
								$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
							} else if($userInfo['edit_all'] == 1 && $link['folder'] == "edit"){
								$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
							} else if($userInfo['create_risks'] == 1 && $link['folder'] == "index"){
								$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
							} else if($userInfo['create_actions'] == 1 && $link['folder'] == "action"){
								$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
							} else {
								$mainMenu[$link['id']] = array("id" => $link['id'], "url" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'display'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name'])) ;
							}
						}

					} //$me->arrPrint($mainMenu);
				}
			}	
	}
				if($basename=="admin" && $pagename=="admin_edit" && $userInfo['edit_all']!=1) {
					//echo $pagename;
					//$me->arrPrint($mainMenu);
					if(count($mainMenu)==0) {
						header('Location: ../manage/view.php');
					} elseif(!isset($mainMenu[$pagename])) {
						$k = array_keys($mainMenu);
						$goto = $mainMenu[$k[0]]['url'];
						//echo " => ".$goto;
						header('Location: '.$goto.'');
					}
					//echo ":";
				}
	
	
//echo $basename." :: ".$pagename;
?>
<html>
<head>
<script type="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery.min.js"></script>
<script type="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui.min.js"></script>

<!-- <script type="text/javascript" src="/library/jquery/js/common.js"></script> -->
<script type="text/javascript" src="/library/js/assiststring.js?<?php echo time(); ?>"></script>
<script type="text/javascript" src="/library/js/assisthelper.js?<?php echo time(); ?>"></script>
<script type="text/javascript" src="/library/js/assistform.js?<?php echo time(); ?>"></script>
<link rel="stylesheet" href="/library/jquery-ui-1.8.24/css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php

$defaultScripts = array( 'generic_functions.js','json2.js');
$defaultStyles 	= array( 'styles.css');
$scripts = isset($scripts) ? $scripts : array();
$styles = isset($styles) ? $styles : array();
$scripts = array_merge($defaultScripts,$scripts);
$styles = array_merge($defaultStyles,$styles);
foreach( $scripts as $script){ 
?>
	<script type=text/javascript src="../js/<?php echo trim($script)."?".time(); ?>"></script>
<?php
} 
foreach( $styles as $sty) {
 ?>
 	<link rel="stylesheet" type="text/css" href="../css/<?php echo trim($sty); ?>" />
<?php
}
 ?>
	<script type=text/javascript>
		$(document).ready(function(){
			window.risk_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($risk_object_name); ?>");
			window.action_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($action_object_name); ?>");

            $(".datepicker").datepicker({
                showOn		  : "both",
                buttonImage 	  : "/library/jquery/css/calendar.gif",
                buttonImageOnly  : true,
                changeMonth	  : true,
                changeYear	  : true,
                dateFormat 	  : "dd-M-yy",
                altField		  : "#startDate",
                altFormat		  : 'd_m_yy'
            });

            $(".historydate").datepicker({
                showOn			: "both",
                buttonImage 	     : "/library/jquery/css/calendar.gif",
                buttonImageOnly	: true,
                changeMonth		: true,
                changeYear		: true,
                dateFormat 		: "dd-M-yy",
                maxDate             : 0
            });

            $(".futuredate").datepicker({
                showOn			: "both",
                buttonImage 	     : "/library/jquery/css/calendar.gif",
                buttonImageOnly	: true,
                changeMonth		: true,
                changeYear		: true,
                dateFormat 		: "dd-M-yy",
                minDate             : +1
            });
/*
            var textarea = $("textarea");
			$.each( textarea, function() {				
				if( $(this).hasClass('smallbox') )
				{
					$(this).attr("cols", "30");
					$(this).attr("rows", "5");
				} else if( $(this).hasClass('mainbox') ){
					$(this).attr("cols", "30");
					$(this).attr("rows", "7");
				} else {
					$(this).attr("cols", "30");
					$(this).attr("rows", "5");
				} 	
			});	
	*/		
			//$("th").css({"text-align":"left"})
				
			var input = $("input")
			$.each( input, function(){
				if( $(this).hasClass("color") ){
					$(this).attr("size", "8")
				} else if( $(this).attr("size") == ""){
					$(this).attr("size", "30")
				} else  {
					$(this).attr("size", $(this).attr("size") )
				}
				//$("input").attr("size", "30");
			})	
			
						
			$(".datepicker").datepicker({	
				showOn			: "both",
				buttonImage 	: "/library/jquery/css/calendar.gif",
				buttonImageOnly	: true,
				changeMonth		: true,
				changeYear		: true,
				dateFormat 		: "dd-M-yy"
			});	

		});
		function hideDialogTitlebar() {
			$(function() {
				$(".ui-dialog-titlebar").hide();
			});
		}
	</script>
	<style type=text/css>
	.hidden { visibility: hidden; }
	.view_text {
		color: #777777;
		font-style: italic;
	}
	.div_info {
		width: 50%; float: right; display:inline;
	}
	</style>
</head>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
	if( (isset($userInfo) && !empty($userInfo)) || $_SESSION['tid'] == "0000" ) {
			/*if(!empty( $links )){
				echoNavigation(1,$mainMenu);
				$columnspage = array("columns", "action_columns");
				$pages       = array("view", "edit", "update", "action", "edit_actions", "update_actions", "update_risks", 'updaterisk', 'update_action', 'admin_edit', 'admin_update', 'update_risk', 'edit_risk', 'edit_action');
				if(in_array($pagename, $pages))
                {
					if( $pagename == "view") {
                       // $menuT[1] = array('id'=>"display_mine",'url'=>"#",'active'=> "Y",'display'=>"Display Mine");
							//$menuT[2] = array('id'=>"display_all",'url'=>"#",'active'=> "",'display'=>"Display All", "disabled" => ($userInfo['view_all'] == 1 ? "" : "disabled") );
					} else if($basename == 'admin') {
                        if(in_array($pagename, array('admin_update', 'update_actions', 'update_action', 'update_risk')))
                        {
                           $menuT[1] = array('id'=> "display_risk",'url'=>"admin_update.php",'active'=> (in_array($pagename, array('admin_update', 'update_risk'))  ? 'Y' : ''),'display'=>"Risk");
                           $menuT[2] = array('id'=> "display_action",'url'=>"update_actions.php",'active'=> (in_array($pagename, array('update_actions', 'update_action'))  ? 'Y' : ''), 'display'=>"Action");
                        } else if(in_array($pagename, array('admin_edit', 'edit_actions', 'edit_risk', 'edit_action')))  {
                            $menuT[1] = array('id'=> "display_risk",'url'=>"admin_edit.php",'active'=> (in_array($pagename, array('admin_edit', 'edit', 'edit_risk'))  ? 'Y' : ''), 'display'=>"Risk");
                            $menuT[2] = array('id'=> "display_action",'url'=>"edit_actions.php",'active'=> (in_array($pagename, array('edit_actions', 'edit_action'))  ? 'Y' : ''), 'display'=>"Action");
                        }
                    }  else if( $pagename == "edit" || $pagename == "edit_actions") {
                        $menuT[1] = array('id'=> "display_risk",'url'=>"edit.php",'active'=> ($pagename == "edit" ? 'Y' : ''),'display'=>"Risk");
                        $menuT[2] = array('id'=> "display_action",'url'=>"edit_actions.php",'active'=> ($pagename == "edit_actions" ? 'Y' : ''),'display'=>"Action");
					}  else if( in_array($pagename, array('update_risks', 'update', 'updaterisk', 'update_action')) ) {
                        $menuT[1] = array('id'=> "display_risk",'url'=>"update_risks.php",'active'=> (($pagename == 'update_risks' || $pagename == 'updaterisk') ? 'Y' : ''),'display'=>"Risk");
                        $menuT[2] = array('id'=> "display_action",'url'=>"update.php",'active'=> (($pagename == 'update' || $pagename == 'update_action' ) ? 'Y' : ''),'display'=>"Action");
					} else {
						if(!strstr($pagename, 'admin'))
                        {
                            //$menuT[2] = array('id'=>"display_all",'url'=>"#",'active'=> "",'display'=>"Display All");
                        }
					}
					 
					echoNavigation(2,$menuT);
				} else if( in_array($pagename, $columnspage)){
					if( $pagename == "columns"){
						$menuT[1] = array('id'=>"risk_columns",'url'=>"columns.php",'active'=> "Y",'display'=>"Risk Columns");
						$menuT[2] = array('id'=>"action_columns",'url'=>"action_columns.php",'active'=> "",'display'=>"Action Columns");
					} else {
						$menuT[1] = array('id'=>"risk_columns",'url'=>"columns.php",'active'=> "",'display'=>"Risk Columns");
						$menuT[2] = array('id'=>"action_columns",'url'=>"action_columns.php",'active'=> "Y",'display'=>"Action Columns");
					}
					echoNavigation(2,$menuT);		
				}
			}
			*/
		//echo "<p>module_query_report.displayPageHeading</p>";
		
		if( (!isset($navigation) || $navigation===true) && (!isset($breadcrumbs) || $breadcrumbs===true) ) {
            $page = isset($page) ? $page : "";
            $folder = isset($folder) ? $folder : "";
			//Navigation buttons & page title
			//echo "page: ".$page."<br />";
			$header = array(0=>"",1=>"",2=>"");
			$location = explode("_",$page); //echo "location"; $me->arrPrint($location);
			$m0 = new Menu();
			$m0_menu = $m0->getAMenu($folder);	//$me->arrPrint($m0_menu);
			$header[0] = "<a href='/".$_SESSION['modlocation']."/".$folder."/' class=breadcrumb>".($m0_menu['client_name'] == "" ? $m0_menu['name'] : $m0_menu['client_name'])."</a>";
			$h = array(1=>$folder);
			if(isset($location[1]) && $folder!=$location[0]) { $h[2] = $location[0]; } //echo "h"; $me->arrPrint($h);
			//If in New - check that user has access to create risks otherwise redirect to action.
			if($h[1]=="risk") {
				if($userInfo['create_risks']==0) {
					if($userInfo['create_actions']==1 && $location[0]!="action") {
						echo "<script type=text/javascript>document.location.href = 'action.php';</script>";
					} elseif($userInfo['create_actions']==0) {
						die("<p>You do not have access to create any ".$risk_object_name."s or any ".$action_object_name."s.");
					}
				}
			}
			foreach($h as $k=>$i) {
				$m_menu = $m0->getSubMenu($i); //echo "m_menu"; $me->arrPrint($m_menu);
				$menu = array();
				foreach($m_menu as $key => $m) { //$me->arrPrint($m);
					$m['url'] = "/".$_SESSION['modlocation']."/".$folder."/".$m['folder'].".php";
					$m['display'] = ($m['client_name'] == "" ? $m['name'] : $m['client_name']);
					if($k==1 && $folder!="admin") { 
						$l = $location[$k-1]; 
					} elseif($folder=="admin" && $page=="admin_approve_done"){
						$l = "admin_approve"; 
					} else {
						$l = $page; 
					} 
					//echo "l: ".$l;
					$m['active'] = ($m['folder'] == $l ? true : false);
					if($m['active']===true) { $header[$k]="<a href='".$m['url']."' class=breadcrumb>".$m['display']."</a>"; }
					$can_i_display_menu = true;
					switch($h[1]) {
						case "risk":
							if($m['folder']=="index" && $userInfo['create_risks']==0) {
								$can_i_display_menu = false;
							} elseif($m['folder']=="action" && $userInfo['create_actions']==0) {
								$can_i_display_menu = false;
							}
							break;
						case "manage":
							if($m['folder']=="assurance" && $userInfo['assurance']==0) {
								$can_i_display_menu = false;
							}
							break;
						case "admin":
							//echo "<p>in admin <br />m[folder]==".$m['folder']."<br />risk manager: ".$userInfo['risk_manager'];
							if(($m['folder']=="admin_approve" || $m['folder']=="approve") && $userInfo['risk_manager']!=1) {
								$can_i_display_menu = false;
							} elseif(($m['folder']=="admin_edit" || $m['folder']=="edit") && $userInfo['edit_all']!=1) {
								$can_i_display_menu = false;
							} elseif(($m['folder']=="admin_update" || $m['folder']=="update") && $userInfo['view_all']!=1) {
								$can_i_display_menu = false;
							}
							break;
					}
					if($can_i_display_menu) {
						$menu[$key] = $m;
					}
				}
				if(count($menu)>0 && (!isset($navigation) || $navigation===true) ) {
					$me->drawNavButtons($menu); //$me->arrPrint($menu);
				}
			}
			if(isset($menuT) && count($menuT)>0 && (!isset($navigation) || $navigation===true) ) {
				$search = array("|OBJECTNAME|","|ACTIONNAME|");
				$replace = array($risk_object_name,$action_object_name);
				$y = json_encode($menuT);
				$menuT2 = json_decode(str_replace($search,$replace,$y),true); 
				$me->drawNavButtons($menuT2);
			}
			foreach($header as $key => $h) { if(strlen($h)==0) { unset($header[$key]); } }
			if(!isset($breadcrumbs) || $breadcrumbs===true) {
				if(isset($page_title)) {
					if($page_title===true) {
						$nameObj = new Naming();
						$object_title = $nameObj->getAName($page_title_src);
						$header[] = $object_title;
					} elseif($page_title===false) {
						$object_title = $m0->getMenuName($page_title_src);
						$header[] = $object_title;
					} elseif(strlen($page_title)>0) { 
						$page_title = str_replace("|OBJECTNAME|",$risk_object_name,$page_title); 
						$page_title = str_replace("|ACTIONNAME|",$action_object_name,$page_title); 
						$header[] = $page_title; 
					}
				}
				echo "<h1>".implode(" >> ",$header)."</h1>";
				$my_page_title = array_pop($header);	
			}
		} else {
			
		}
	} else {
		echo "<p>Invalid user access.  Please contact your administrator to setup your user access.</p>";
		exit();
	}
?>