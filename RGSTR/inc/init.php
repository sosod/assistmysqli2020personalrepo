<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../../".strtolower($classname).".php" ) ) {
		require_once( "../../".strtolower($classname).".php" );		
	}else if(file_exists("../../library/dbconnect/".strtolower($classname).".php")) {
		require_once( "../../library/dbconnect/".strtolower($classname).".php" );		
	}else if(file_exists("../../library/class/".strtolower($classname).".php")) {
		require_once( "../../library/class/".strtolower($classname).".php" );		
	}else if(file_exists("../../S/class/".strtolower($classname).".php")) {//AA-673 Risk 1s  Risk functions not accessible [SD] 12/08/2021
		require_once( "../../S/class/".strtolower($classname).".php" );
	}else if(file_exists("../../RGSTR/class/".strtolower($classname).".php")) {
		require_once( "../../RGSTR/class/".strtolower($classname).".php" );
	} elseif("../../library/class/assist_helper.php") {
        require_once( "../../library/class/assist_helper.php" );
    } else {
		require_once( "../../library/".strtolower($classname).".php" );		
	}
}

require_once("../../library/class/assist_dbconn.php");
require_once("../../library/class/assist_db.php");
require_once("../../library/class/assist_email.php");
//require_once ("../../library/dbconnect/dbconnect_old.php");
require_once ("../../RGSTR/class/useraccess.php");
require_once ("../../RGSTR/class/naming.php");
require_once ("../../RGSTR/class/menu.php");
require_once ("../../RGSTR/class/risk.php");
include_once("../class/report.php");
?>