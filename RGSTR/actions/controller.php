<?php
@session_start();
include_once("../inc/init.php");
switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ){
	
case "getActions":
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	$colmObj = new ActionColumns();
	$options = "";
	if( $_GET['section'] == "manage")
	{
		$options = " AND active & 4 = 4";	
	} else {
		$options = " AND active & 8 = 8";
	}
	$headers 	 = $colmObj -> getHeaderList( $options );
	$riskActions = $act -> getRiskActions( $_GET['start'], $_GET['limit'] );
	$actions 	 = array();
	$status      = array();
	$progress	 = array();
	$headerArr   = array(); 
	$actionOwner = array();
	foreach( $headers as $field => $head)
	{
		foreach( $riskActions as $index => $valArr)
		{
			$status[$valArr['id']]  	= $valArr['actionstatus'];
			$actionOwner[$valArr['id']] = $valArr['isUpdate'];
			if( array_key_exists($field, $valArr))
			{
				$actions[$valArr['id']][$field]  = ($field == "progress" ? $valArr[$field]."%" :  $valArr[$field]);
				$headerArr[$field]				 = $head;			
			}	
		}	
	}	
	$actionStats = $act-> getActionStats();
	echo json_encode( array("data" => $actions, "status" => $status, "headers" => $headerArr, "columns" => count($headerArr),"statistics" => $actionStats, "owners" => $actionOwner, "rawHeaders" => $headers, "countRaw" => count($headers) ));
	break;
    case "processActionAttachments":
    	
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/")){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ){
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted");					
				}					
			}  else if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
				mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
			}  	
            $upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/";
            $key 	  				            = key($_FILES);
            $filename 				            = basename($_FILES[$key]['name']);
            $_SESSION['uploads']['action']['added'][$key.time()]   = $_FILES[$key]['name'];
            $fileuploaded 			            =  $key."".time().".";
            $ext 					            = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
            if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
            {
	            
            	$filenow = array();
	            foreach( $_SESSION['uploads']['action']['added'] as $f => $file)
	            {
            		$xt = substr( $file, strpos($file,".") + 1 );
	            	if( file_exists($upload_dir."".$f.".".$xt)){
	            		if( strpos($file,".") > 0 ){
	            			$filenow[$f] = $file;
	            		}	
	            	} else {
	            		unset($_SESSION['uploads']['action']['added'][$f] );
	            	}	            	
	            }            	
                $response = array(
                        "text" 			=> "File uploaded .....",
                        "error" 		=> false,
                        "filename"  	=> $filename,
                        "key"			=> $key,
                        "filesuploaded" => $filenow
                        );
            } else {
            $response = array("text" => "Error uploading file ...." , "error" => true );
            }
            echo json_encode( $response );
        break;	
case "getActionsToApprove":
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	echo json_encode( $act -> getActionToApprove() );
	break;
case "getRisk":
	$act = new Risk("", "", "", "", "", "", "","","", "", "", "", "" );
	echo json_encode( $act -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getRiskWithHeaders":
	$act = new Risk("", "", "", "", "", "", "","","", "", "", "", "" );
	echo json_encode( $act -> getRiskWithHeaders( $_REQUEST['id'] ) );
	break;	
case "getARisk":
	$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
	echo json_encode( $risk -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getRiskAction( $_REQUEST['id'] ) );
	break;
case "getUpdateAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	$response  = $act -> getRiskAction( $_REQUEST['id'] );
	$actionObj = new Naming();
	$headers   =  $actionObj -> getHeaderList(); 
	$response['action_status'] = $response['status'];
	$response['action_owner'] = $response['owner'];	
	$headerArr = array();
	$responseArr  = array();
	foreach( $response as $key => $val)
	{
		if(array_key_exists($key, $headers)){
			$headerArr[$key] = $headers[$key];
			$responseArr[$key]  = $val;
		}
	} 
	echo json_encode( array("action" => $responseArr, "header"=>$headerArr) );
	break;
case "getAAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getAAction( $_REQUEST['id'] ) );
	break;	
case "getUserAccess":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "");
	echo json_encode( $uaccess -> getUserAccess() );
	break;	
case "getUsers":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
	echo json_encode( $uaccess -> getUsers() );
	break;
case "getStatus":
	$stat = new RiskStatus("", "", "");
	echo json_encode( $stat -> getStatus() );
	break;
case "newRiskAction":
		$risk = new RiskAction( 
				$_REQUEST['id'] ,
				$_REQUEST['r_action'] ,
				$_REQUEST['action_owner'] ,
			    $_REQUEST['deliverable'] ,
		 		$_REQUEST['timescale'] ,
				$_REQUEST['deadline'] ,
				$_REQUEST['remindon'] ,
				$_REQUEST['progress'],
				$_REQUEST['status']
		);
		$id 	   = $risk -> saveRiskAction();
		$response  = array(); 
		if( $id > 0 ) {
			$state 				 = false;		
			$text				 = "New Action #".$id." has been successfully saved <br />";
			$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			     = $user -> getRiskManagerEmails();
			$responsibleUserEmail= $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
			$email 			     = new Email();
			$email -> userFrom   = $_SESSION['tkn'];
			$risk 				 = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
			$riskInfo 			 = $risk -> getRisk($_REQUEST['id']);
			$email -> subject    = "New Action";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message  = "";			
			$message .= $email -> userFrom." created an Action relating to risk #".$_REQUEST['id'];
			$message .= " : ".$riskInfo['description']." identified in the Risk Register. The details of the action are as follows :";
			$message .= " \r\n\n";	//blank row above Please log on line
			$message .= "Action Number : ".$id."\r\n"; 
			$message .= "Action : ".$_REQUEST['r_action']."\r\n";
			$message .= "Deliverable : ".$_REQUEST['deliverable']."\r\n";
			$message .= "Action Owner : ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
			$message .= "Time Scale : ".$_REQUEST['timescale']."\r\n";
			$message .= "Deadline : ".$_REQUEST['deadline']."\r\n";
			$message .= " \r\n\n";	//blank row above Please log on line																		
			$message .= "<i>Please log onto Ignite Assist in order to View or Update the Action.</i> \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";
			foreach($users as $userEmail ) {
				$emailTo = $userEmail['tkemail'].",";
			}
			$email -> to = ($emailTo == "" ? "" : rtrim($emailTo.","))."".($responsibleUserEmail['tkemail'] == "" ? "" : ",".$responsibleUserEmail['tkemail']);
			$email -> from     = "info@ignite4u.com";
			if( $email -> sendEmail() ) {
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification has been sent";
			} else {
				$state	= true;
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("text" => $text, "error" => $state, "saved" => true, "id" => $id);
		} else {
			
			$response = array("text" => "Error saving the action \r\n", "error" => true , "saved" => false );
		}
        unset($_SESSION['uploads']['action']);
		echo json_encode( $response );
	break;
case "deleteAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	$res = $act -> deleteRiskAction( $_REQUEST['id'] );
	$response = array();
	if( $res > 0)
	{
		$response = array("text" => "Action successfully deleted . . ", "error" => false);		
	} else {
		$response = array("text" => "Error deleting action . . ", "error" => true);		
	}
	echo json_encode( $response );
	break;
case "updateRiskAction":
	$_actObj            = new RiskAction("", "", "", "", "", "", "", "", "");
	$risk_action        =  $_actObj -> getAAction( $_REQUEST['id'] ) ;
	$changesArr		= array();
	$changeMessage      = "";
	$attachmentChanges  = array();
	if( isset($_SESSION['uploads']['action']['added']) && !empty($_SESSION['uploads']['action']['added']))
	{
		$attachments = array();
		if( $risk_action['attachment'] != "")
		{
			$attachments = unserialize($risk_action['attachment']);
		} 

		foreach( $_SESSION['uploads']['action']['added'] as $index => $val)
		{
			$changesArr['attachment'][$index] = "Attachment added ".$val;
			$changeMessage  				  .= "Attachment added ".$val;
		}
		$attachmentChanges = array_merge( $attachments, $_SESSION['uploads']['action']['added'] );
		//$_SESSION['uploads']['action']['added'] = array_merge( $attachments, $_SESSION['uploads']['action']['added'] );		
		//$_SESSION['uploads']['action'] = array_merge( $attachments, $_SESSION['uploads']['action']['added'] );
		unset( $_SESSION['uploads']['action']['added'] ); 
	}
	if( isset( $_SESSION['uploads']['action']['deleted']))
	{
		$attachements = array();
		if( $risk_action['attachment'] != "" )
		{			
			$attachements = unserialize( $risk_action['attachment'] );	
		}
			
		if( isset($attachements) && !empty($attachements))
		{
			foreach( $_SESSION['uploads']['action']['deleted']  as $index => $name)
			{
				if( isset($attachements[$index]))
				{
					unset( $attachements[$index] );
					if( isset( $attachmentChanges[$index])) {
						unset($attachmentChanges[$index]);
					}
					$changesArr['attachment'][$index] = "Deleted attachment ".$name;
					$changeMessage 					 .= "Deleted attachment ".$name;					
				}					
			}
			$attachmentChanges = array_merge( $attachements, $attachmentChanges);
			unset($_SESSION['uploads']['action']['deleted']);
			//$attachmentChanges['deleted'] = $_SESSION['uploads']['action']['deleted'];
		}
	}
	$_SESSION['uploads']['action'] = $attachmentChanges;		
	$act = new RiskAction("",
						  $_REQUEST["r_action"],
						  $_REQUEST["action_owner"],
						  $_REQUEST["deliverable"],
						  $_REQUEST["timescale"],
						  $_REQUEST["deadline"],
						  $_REQUEST["remindon"],
						  "",
						  (isset($_REQUEST['status']) ? $_REQUEST['status'] : "1" ) 
						 );
	$action 	=  $act -> getAAction( $_REQUEST['id'] ) ;
	$action['remindon'] = (isset($action['updatedremindon']) ? $action['updatedremindon'] : $action['remindon']);
	$user      	 = new UserAccess("", "", "", "", "", "", "", "", "" );
	$users          = $user -> getUsers();
	
	//find the difference made to the action 
	foreach( $_REQUEST as $key => $value)
	{
        if(isset($action[$key]) ||  array_key_exists($key, $action))
	   {
		 if( $key == "action")
		 {
		     continue;
		 } else if($key == "action_owner") {
		     $from = "";
		     $to   = "";
		     foreach($users as $index => $_user)
		     {
		       if($_user['tkid'] === $action[$key])
		       {
                    $from = $_user['tkname']." ".$_user['tksurname'];
		       }
		       if($_user['tkid'] === $value)
		       {
                    $to = $_user['tkname']." ".$_user['tksurname'];
		       }		       
		     }
		     if($from != $to)
		     {
			  $changeMessage    .= ucwords($key)." changed to ".$to." from ".$from." \r\n"; 
			  $changesArr[$key]  = array("to" => $to, "from" => $from); 		 
			}
		 } else if( $value !== $action[$key] ) {
			$changeMessage    .= ucwords($key)." changed to ".$value." from ".$action[$key]." \r\n"; 
			$changesArr[$key]  = array("to" => $value, "from" => $action[$key]); 
		 } 
	   }
	   if( $key == "r_action") 
	   {
		 if($value !== $action['action'])
		 {
		     $changeMessage 	  .= "Action changed to ".$_REQUEST['r_action']." from ".$action['action']." \r\n"; 
			$changesArr['action']  = array( "to" => $_REQUEST['r_action'] ,"from" => $action['action']); 				
		 }
	   }
	}
	$changesArr['currentstatus'] = $action['status'];	
	$changesArr['user'] = $_SESSION['tkn'];
	$res 				= $act -> updateRiskAction( $_REQUEST['id'] , base64_encode(serialize($changesArr)) );
	$response 			= array();
	if( $res > 0 ){
		$responeText = "Action #".$_REQUEST['id']." has been successfully edited <br />";
		$emailTo 	         = $user -> getRiskManagerEmails();
		$email             = new Email();
		$email -> userFrom = $_SESSION['tkn'];
		$email -> subject  = "Edit Action";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message   = ""; 
		//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		$message .= " \n";	//blank row above Please log on line
		
		$message .= $email -> userFrom." edited action #".$_REQUEST['id']." related to risk #".$action['risk_id']."";
		$message .= " : ".$action['description']."\r\n\n";		
		if($changeMessage == ""){
			$message  .= "";
		} else {
			$message .= "The following changes were made : \r\n\n";
			$message .= $changeMessage;
		}
		$message .= "\r\n Please log onto Ignite Assist in order to View or Update the Action.\r\n";
		$message  = nl2br($message);		

		$email -> body     = $message;
		$email -> to 	    = $emailTo;
		$email -> from     = "no-reply@ignite4u.co.za";
		if( $email -> sendEmail() ) {
			$response = array("error" => false, "text" => $responeText." &nbsp;&nbsp;&nbsp; Notification email send successfully \r\n" );
		} else {
			$response = array("error" => true , "text" => $responeText." &nbsp;&nbsp;&nbsp; There was an error sending the email \r\n");
		} 	
	} else{
		$response = array("error" => true, "text" => "Error occured updating the action <br /> &nbsp;&nbsp;".$res);
	} 
	if(isset($_SESSION['uploads']['action']))
	{
		unset($_SESSION['uploads']['action']);		
	}
	echo json_encode( $response );
	break;
case "getEditLog":
	$act 	 		= new RiskAction("", "", "", "", "", "", "", "", "" );
	$editLog 		= $act -> getActionEditLog( $_REQUEST['id'] );
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $editLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been edited by ".$changes['user']."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else{
				   $changeMessage .= ucwords($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	  usort($changeLog, "compare");
	}
	echo json_encode( $changeLog );
	break;
case "getActionUpdates":
	$act 			= new RiskAction("", "", "","" , "", "", "", "", "");
	$updatesLog     = $act -> getActionUpdates( $_REQUEST['id'] ); 	
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $updatesLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been updated by ".(isset($changes['user']) ? $changes['user'] : "")."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else if($key == "description"){
					$changeMessage 	.= $change."\r\n<br />";
				}else{
				   $changeMessage .= "\r\n".ucwords($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	  usort($changeLog, "compare");
	}
	echo json_encode( $changeLog );
	break;	
case "newActionUpdate"  :
    $_actObj = new RiskAction("", "", "", "", "", "", "", "", "");
    $risk_action =  $_actObj -> getLatestActionUpdates( $_REQUEST['id'] ) ;
    $changesArr 	= array();
    $changeMessage	= "";
    $attachmentChanges = array();
    if( isset($_SESSION['uploads']['action']['updated']) && !empty($_SESSION['uploads']['action']['updated']))
    {
    $attachments = array();
    if( $risk_action['attachment'] != "")
    {
    $attachments = unserialize( $risk_action['attachment'] );
    }

    foreach( $_SESSION['uploads']['action']['updated'] as $index => $val)
    {
    $changesArr['attachment'][$index] = "Attachment added ".$val;
    $changeMessage  				  .= "Attachment added ".$val;
    }
    $attachmentChanges = array_merge( $attachments, $_SESSION['uploads']['action']['updated'] );
    //$_SESSION['uploads']['action']['added'] = array_merge( $attachments, $_SESSION['uploads']['action']['added'] );
    //$_SESSION['uploads']['action'] = array_merge( $attachments, $_SESSION['uploads']['action']['added'] );
    unset( $_SESSION['uploads']['action']['updated'] );
    }
    if( isset( $_SESSION['uploads']['action']['updatedeleted']))
    {
    $attachements = array();
    if( $risk_action['attachment'] != "" )
    {
    $attachements = unserialize( $risk_action['attachment'] );
    }

    if( isset($attachements) && !empty($attachements))
    {
    foreach( $_SESSION['uploads']['action']['updatedeleted']  as $index => $name)
    {
        if( isset($attachements[$index]))
        {
            unset( $attachements[$index] );
            if( isset( $attachmentChanges[$index])) {
                unset($attachmentChanges[$index]);
            }
            $changesArr['attachment'][$index] = "Deleted attachment ".$name;
            $changeMessage 					 .= "Deleted attachment ".$name;
        }
    }
    $attachmentChanges = array_merge( $attachements, $attachmentChanges);
    unset($_SESSION['uploads']['action']['updatedeleted']);
    //$attachmentChanges['deleted'] = $_SESSION['uploads']['action']['deleted'];
    }
    }
    $act 			= new RiskAction("", "", "","" , "", "", $_REQUEST["remindon"], $_REQUEST['progress'], $_REQUEST['status']);
    $action 		= $act -> getAAction( $_REQUEST['id'] ) ;
    $status 		= new ActionStatus("","", "");
    $actionstatus	= $status->getAStatus( $_REQUEST['status'] );

    $requestArr 	= $_REQUEST;
    $action['progress']   = ((isset($action['updatedprogres']) ? $action['updatedprogres'] : $action['progress']) == "" ? 0 : (isset($action['updatedprogres']) ? $action['updatedprogres'] : $action['progress']));
    $requestArr['status'] = $actionstatus['name'];
    $action['status']	  = (isset($action['updatedstatus']) ? $action['updatedstatus'] : $action['status']);
    foreach( $requestArr as $key => $value)
    {
        if( $key == "action")
        {
            continue;
        } else if( $key == "description") {
            $changeMessage   .= "Added update description: ".$value."\r\n\n";
            $changesArr['response'] = $value;
        } else if( isset($action[$key]) ||  array_key_exists($key, $action)) {
            if( $value !== $action[$key] )
            {
                 $changeMessage  .= ucwords(($key == 'remindon' ? 'Remind On' : $key)).": changed to ".$value."".($key == "progress" ? "%" : "")." from ".$action[$key]."".($key == "progress" ? "%" : "")."\r\n\n";
                 $changesArr[$key]  = array("from" => $action[$key], "to" => $value);
            }
        }
    }
    if( trim($requestArr['status']) != trim($action['status'])) {
        $changesArr['currentstatus'] = $requestArr['status'];
    } else {
        $changesArr['currentstatus'] = $action['status'];
    }
    $changesArr['user'] = $_SESSION['tkn'];
    $id 	= $act -> newRiskActionUpdate(
                                    $_REQUEST['id'] ,
                                    $_REQUEST['description'],
                                    $_REQUEST['approval'],
                                    $attachmentChanges,
                                    base64_encode( serialize($changesArr) )
                                );
    $response  = array();
    if( $id > 0 )
    {
    $response["id"] 	 = $id;
    $state		 = false;
    $text 		 = "New action update for action #".$_REQUEST['id']." has been successfully saved <br />";
    $user      	 = new UserAccess("", "", "", "", "", "", "", "", "" );
    $emailTo     = $user -> getRiskManagerEmails();
    $email 		 = new Email();

    $email -> userFrom   = $_SESSION['tkn'];
    $email -> subject    = "Update Action";
    //Object is something like Risk or Action etc.  Object_id is the reference of that object
    $message   = "";
    //$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
    $message .= " \n";	//blank row above Please log on line
    $message .= $_SESSION['tkn']." Updated action #".$id." related to risk #".$action['risk_id'].": ";
    $message .= " ".$action['description']."\r\n\n";
    if( $changeMessage == "")
    {
        $message  = "";
    } else{
        $message .= $changeMessage;
    }
    $message .= "Please log onto Ignite Assist in order to View further information on the Risk and or associated Actions \n";
    $message  = nl2br($message);
    $email -> body     = $message;
    $to 	   = ($emailTo == "" ? "" : $emailTo.",")." ".$action['action_creator'];
    $email->to = rtrim($to, ",");
    $email -> from     = "no-reply@ignite4u.co.za";
    if( $email -> sendEmail() ) {
        $text .= " &nbsp;&nbsp;&nbsp; Notification email send successfully";
    } else {
        $state = true;
        $text .= " &nbsp;&nbsp;&nbsp; There was an error sending the email";
    }
    $response = array("text" => $text, "error" => $state, "update" => true);
    } else {
    $response = array("text" => "Error saving the action update", "error" => true , "update" => false);
    }
    echo json_encode( $response );
	break;
case "getActionAssurances":
	$action = new ActionAssurance($_REQUEST['id'], "", "", "", "", "", "");
	echo json_encode( $action -> getActionAssurances( $_REQUEST['id'], $_REQUEST['start'], $_REQUEST['limit'] ) );
	break;
case "getActionAssurance":
	$action = new RiskAction("", "", "", "", "", "", "","", "");
	$action -> saveActionAssurance($_REQUEST['id'], "", $_REQUEST['response'], $_REQUEST['signoff'], "", "", "" ) ;
	break;
case "newAssuranceAction":
	$actionAssurance = "";
	if( isset($_SESSION['uploads']['action']['actionassurance']) && isset($_SESSION['uploads']['action']['actionassurance']))
	{
		$actionAssurance = serialize( $_SESSION['uploads']['action']['actionassurance'] );
	}
	$newAssurance = new ActionAssurance( $_REQUEST['id'], $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $actionAssurance, "" );
	$res 		  = $newAssurance -> saveActionAssurance();
	$response 	  = array();
	if( $res > 0){
		$response = array("text" => "Action assurance saved successfully ", "error" => false );
	} else {
		$response = array("text" => "Error saving action assurance", "error" => true );
	}
	if( isset($_SESSION['uploads']['action']['actionassurance']))
	{
		unset($_SESSION['uploads']['action']['actionassurance']);
	}
	echo json_encode( $response );
	break;
case "updateActionAssurance":
	$actionAssuranceObj = new ActionAssurance("", "", "", "", "", "", "");
	$actionassurance 	= $actionAssuranceObj -> getAActionAssurance( $_REQUEST['id'] );	
	
	$actionAssuranceAtt = array();
	if( !empty($actionassurance['attachment']))
	{
		$actionAssuranceAtt = unserialize( $actionassurance['attachment'] );		
	}
	
	if( isset($_SESSION['uploads']['action']['updateactionassurance']) && !empty($_SESSION['uploads']['action']['updateactionassurance']))
	{
		foreach( $_SESSION['uploads']['action']['updateactionassurance'] as $i => $val)
		{
			$actionAssuranceAtt[$i] = $val;			
		}
		unset( $_SESSION['uploads']['action']['updateactionassurance'] );
	}
	
	if( isset($_SESSION['uploads']['action']['actionassurancedeleted']) && !empty($_SESSION['uploads']['action']['actionassurancedeleted']))
	{
		foreach( $_SESSION['uploads']['action']['actionassurancedeleted']  as $k => $dval)
		{
			if( isset($actionAssuranceAtt[$k]) )
			{
				unset( $actionAssuranceAtt[$k] );				
			}			
		}
		unset( $_SESSION['uploads']['action']['actionassurancedeleted'] ); 	
	} 
	$att = "";
	if( !empty($actionAssuranceAtt) )
	{
		$att = serialize( $actionAssuranceAtt );	
	}
	
	$newAssurance = new ActionAssurance( $_REQUEST['action_id'], $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $att, "" );
	$response = array();
	$res 	  = $newAssurance -> updateActionAssurance( $_POST['id'] );
	if( $res == 1)
	{
		$response = array("text" => "Action assurance successfully updated ", "error" => false);
	} else if( $res == 0) {
		$response = array("text" => "No change was made to the action assurance", "error" => false);
	} else {
		$response = array("text" => "Error updating action assurance", "error" => true);	
	}
	echo json_encode( $response );
	break;
case "deleteActionAssurance":
	$newAssurance = new ActionAssurance( "", "", "", "", "", "", "" );
	$res = $newAssurance -> deleteActionAssurance( $_POST['id'] );
	$response = array();
	if( $res > 0)
	{
		$response = array("text" => "Action assurance deleted" , "error" => false ); 
	} else {
		$response = array("text" => "Error deleting action assurance" , "error" => true );
	}
	echo json_encode( $response );	
	break;	
case "newApprovalAction":
		$act = new RiskAction("", "", "", "", "", "", "","", "");
		$act -> approveAction( 
								$_REQUEST['id'],
								$_REQUEST['response'],
								$_REQUEST['signoff'],
								($_REQUEST['notification'] == "on" ? "1" : "0"),
								$_REQUEST['attachment']
							);
	break;
case "sendRequestApprovalEmail":
			$response = array(); 
			$emailTo  = "";
			$act 	  = new RiskAction("", "", "", "", "", "", "","", "");
			$status   = $act -> getApprovalRequestStatus( $_REQUEST['id'] );
			if( ($status['action_status'] & RiskAction::REQUEST_APPROVAL) == RiskAction::REQUEST_APPROVAL  )
			{
				$response = array("error" => true, "text" => "A request for approval has already been sent");
			} else {
				$user      		  = new UserAccess("", "", "", "", "", "", "", "", "" );
				$emailTo   		  = $user -> getRiskManagerEmails();
				$action			  = $act -> getAAction( $_REQUEST['id'] );
				$email 			  = new Email();
				$email -> userFrom= $_SESSION['tkn'];
				$email -> subject = "Action Approval Request";
				//Object is something like Risk or Action etc.  Object_id is the reference of that object
				$message   = ""; 
				$message .= $email->userFrom." has submitted a request for approval of an action #".$_REQUEST['id'];
				$message  .= " related to Risk ".$action['risk_id']." : ".$action['description'];
				$message .= " \r\n\n";
					//blank row above Please log on line
				$message .= "Please log onto Ignite Assist to approve the action.\r\n\n";
				$message  = nl2br($message);		
				$email -> body     = $message;
				$to 	   = ($emailTo == "" ? "" : $emailTo.",")."".$action['action_creator'];
				$email -> to = rtrim( $to, ",");
				$email -> from     = "no-reply@ignite4u.co.za";
				if( $email -> sendEmail() ) {
					$id	= $act->setActionStatus( $_REQUEST['id'], $status['action_status'] + RiskAction::REQUEST_APPROVAL );					
					$response = array("text" => "Email send successfully", "error" => false);					
				} else {
					$response = array( "error" => true, "text" => "There was an error sending the request approval email" );
				}
			} 
		echo json_encode( $response );			
		break;	
	case "searchActionToApprove":
		$act = new RiskAction("", "", "", "", "", "", "","", "");
		echo json_encode( $act -> searchActionToApprove( $_REQUEST['searchtext'] ) );
	break;
	case "removeActionFile":
		$filename  = $_POST['id'];
		$file      = $_POST['original'];
		$ext 	   = substr( $file, strpos($file, ".") ); 
		$oldname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$filename."".$ext;
		$newname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted/".$filename."".$ext;
		$response = array();
		if( rename($oldname, $newname) )
		{
			$_SESSION['uploads']['action']['deleted'][$filename] = $_POST['original'];
			$response = array("text" => "File removed", "error" => false);
		} else {
			$response = array("text" => "An error occured removing the 	", "error" => true);			
		}
		echo json_encode( $response );
	break;
	case "justRemoveActionFile":
		try {
			$id    = $_POST['id'];
			$file  = $_POST['file'];
			$ext 	   = substr( $file, strpos($file, ".") ); 
			$oldname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$id."".$ext;
			$newname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted/".$id."".$ext;
			$response = array();
			if( rename( $oldname, $newname) )
			{
				if( isset($_SESSION['uploads']['action']['added'][$id]) ) {
					unset( $_SESSION['uploads']['action']['added'][$id] );
				}
				$response  = array("text" => "File removed", "error" => false);
			} else {
				$response  = array("text" => "Error removing the file", "error" => true);
			}			
		} catch (Exception $e) {
			$response =  "Error occured ".$e->getMessage();
		} 
		echo json_encode( $response );
	break;
	case "processUpdateActionAttachments":
    	
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/")){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ){
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted");					
				}					
			}  else if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
				mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
			}  	
            $upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/";
            $key 	  				            = key($_FILES);
            $filename 				            = basename($_FILES[$key]['name']);
            $_SESSION['uploads']['action']['updated'][$key.time()]   = $_FILES[$key]['name'];
            $fileuploaded 			            =  $key."".time().".";
            $ext 					            = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
            if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
            {
	            
            	$filenow = array();
	            foreach( $_SESSION['uploads']['action']['updated'] as $f => $file)
	            {
            		$xt = substr( $file, strpos($file,".") + 1 );
	            	if( file_exists($upload_dir."".$f.".".$xt)){
	            		if( strpos($file,".") > 0 ){
	            			$filenow[$f] = $file;
	            		}	
	            	} else {
	            		unset($_SESSION['uploads']['action']['updated'][$f] );
	            	}	            	
	            }            	
                $response = array(
                        "text" 			=> "File uploaded .....",
                        "error" 		=> false,
                        "filename"  	=> $filename,
                        "key"			=> $key,
                        "filesuploaded" => $filenow
                        );
            } else {
            $response = array("text" => "Error uploading file ...." , "error" => true );
            }
            echo json_encode( $response );
	break;
	case "removeUpdateActionFile":
		$filename  = $_POST['id'];
		$file      = $_POST['original'];
		$ext 	   = substr( $file, strpos($file, ".") ); 
		$oldname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$filename."".$ext;
		$newname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted/".$filename."".$ext;
		$response = array();
		if( rename($oldname, $newname) )
		{
			$_SESSION['uploads']['action']['updatedeleted'][$filename] = $_POST['original'];
			$response = array("text" => "File removed", "error" => false);
		} else {
			$response = array("text" => "An error occured removing the 	", "error" => true);			
		}
		echo json_encode( $response );
	break;
	case "saveActionAssuranceAttachment":
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/")){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ){
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted");					
				}					
			}  else if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
				mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
			}  	
            $upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/";
            $key 	  				            = key($_FILES);
            $filename 				            = basename($_FILES[$key]['name']);
            $_SESSION['uploads']['action']['actionassurance'][$key.time()]   = $_FILES[$key]['name'];
            $fileuploaded 			            =  $key."".time().".";
            $ext 					            = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
            if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
            {
	            
            	$filenow = array();
	            foreach( $_SESSION['uploads']['action']['actionassurance'] as $f => $file)
	            {
            		$xt = substr( $file, strpos($file,".") + 1 );
	            	if( file_exists($upload_dir."".$f.".".$xt)){
	            		if( strpos($file,".") > 0 ){
	            			$filenow[$f] = $file;
	            		}	
	            	} else {
	            		unset($_SESSION['uploads']['action']['actionassurance'][$f] );
	            	}	            	
	            }            	
                $response = array(
                        "text" 			=> "File uploaded .....",
                        "error" 		=> false,
                        "filename"  	=> $filename,
                        "key"			=> $key,
                        "filesuploaded" => $filenow
                        );
            } else {
            $response = array("text" => "Error uploading file ...." , "error" => true );
            }
            echo json_encode( $response );
	break;
	case "updateActionAssuranceAttachment":
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/")){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/") ){
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted");					
				}					
			}  else if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action")){
				mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action");
			}  	
            $upload_dir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/";
            $key 	  				            = key($_FILES);
            $filename 				            = basename($_FILES[$key]['name']);
            $_SESSION['uploads']['action']['updateactionassurance'][$key.time()]   = $_FILES[$key]['name'];
            $fileuploaded 			            =  $key."".time().".";
            $ext 					            = substr( $_FILES[$key]['name'], strpos($_FILES[$key]['name'],".") + 1 );
			
            if( move_uploaded_file( $_FILES[$key]['tmp_name'], $upload_dir."".$fileuploaded."".$ext) )
            {
	            
            	$filenow = array();
	            foreach( $_SESSION['uploads']['action']['updateactionassurance'] as $f => $file)
	            {
            		$xt = substr( $file, strpos($file,".") + 1 );
	            	if( file_exists($upload_dir."".$f.".".$xt)){
	            		if( strpos($file,".") > 0 ){
	            			$filenow[$f] = $file;
	            		}	
	            	} else {
	            		unset($_SESSION['uploads']['action']['updateactionassurance'][$f] );
	            	}	            	
	            }            	
                $response = array(
                        "text" 			=> "File uploaded .....",
                        "error" 		=> false,
                        "filename"  	=> $filename,
                        "key"			=> $key,
                        "filesuploaded" => $filenow
                        );
            } else {
            $response = array("text" => "Error uploading file ...." , "error" => true );
            }
            echo json_encode( $response );
	break;
	case "deleteActionAssuranceAttachment":
		$filename  = $_POST['id'];
		$file      = $_POST['file'];
		$ext 	   = substr( $file, strpos($file, ".") ); 
		$oldname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$filename."".$ext;
		$newname = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/deleted/".$filename."".$ext;
		$response = array();
		if( rename($oldname, $newname) )
		{
			$_SESSION['uploads']['action']['actionassurancedeleted'][$filename] = $_POST['file'];
			$response = array("text" => "File removed", "error" => false);
		} else {
			$response = array("text" => "An error occured removing the 	", "error" => true);			
		}
		echo json_encode( $response );
	break;
}

	function compare($a ,$b)
	{
		return ( $a['date'] > $b['date'] ? -1 : 1 );
	}

?>
