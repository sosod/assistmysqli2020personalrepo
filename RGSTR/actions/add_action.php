<?php
$scripts = array( 'jquery.ui.action.js', 'add_action.js','menu.js');
$styles = array( 'colorpicker.css' );
$page_title = "Action Edit";
require_once("../inc/header.php");
?>
<script>
$(function(){
	$("table#addaction_table").find("th").css({"text-align":"left"})
	$("#actions").action({risk_id:<?php echo $_GET['id']; ?>, extras:false});		
});
</script>
<div id="addaction_message" class="message"></div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<table id="risk_action_table" class="noborder">
	<tr>
    	<td valign="top" width="600" class="noborder">
        	<table id="riskInfo" width="100%"></table><br />
            <br />
          <div id="actions"></div>
         <!-- <table id="actiondetails" width="100%">
          	<tr>
            	<th>Action #</th>
                <th>Action </th>                
                <th>Status</th>
                <th>Progress</th>
            </tr>
          </table> -->
        </td>
        <td class="noborder"></td>
        <td width="500" valign="top" class="noborder">
         <table align="left" id="addaction_table" width="100%" style="border-collapse:collapse;">
          <tr>
            <th>Risk:</th>
            <td><?php echo $_REQUEST['id']; ?></td>
          </tr>
          <tr>
            <th>Action:</th>
            <td><textarea name="action" id="action"></textarea></td>
          </tr>
          <tr>
            <th>Deliverable:</th>
            <td><textarea name="deliverable" id="deliverable"></textarea></td>
          </tr>
          <tr>
            <th>Action Owner:</th>
            <td>
                <select id="action_owner" name="action_owner">
                
                </select>
            </td>
          </tr>
          <tr>
            <th>Time Scale:</th>
            <td><input type="text" name="timescale" id="timescale" /></td>
          </tr>
          <tr>
            <th>Deadline:</th>
            <td><input type="text" name="deadline" id="deadline" class="datepicker" /></td>
          </tr>
          <!--  <tr>
            <th>Status:</th>
            <td>
            <select id="action_status" name="action_status">
            </select>
            </td>
          </tr>
         <tr>
            <th>Progress:</th>
            <td><input type="text" name="progress" id="progress" value="" /> <em style="color:#FF0000;">%</em></td>
          </tr>
          -->
          <tr>
            <th>Remind On:</th>
            <td><input type="text" name="remindon" id="remindon" class="datepicker" /></td>
          </tr>
          <tr>
            <th>Attachments:</th>
            <td>
            <input type="file" id="action_attachments" name="action_attachments" />
            <input type="submit" id="another_attach" name="another_attach" value="Add Another" />
            </td>
          </tr>
          <tr>
            <th></th>
            <td>
            <input type="hidden" id="risk_id" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" />
            <input type="submit" id="save_action" value="Save Action" name="save_action" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" id="cancel_action" value="Cancel Action" name="cancel_action" />
            </td>
          </tr>
          <tr>
            <td colspan="2" align="left">
                <?php $me->displayGoBack("",""); ?>
            </td>
          </tr>
        </table>						       
        </td>
    </tr>
</table>