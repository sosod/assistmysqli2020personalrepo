<?php 
$scripts = array( 'jquery.ui.action.js', 'action_assurance.js','menu.js',  );
$styles = array( 'colorpicker.css'  );
//$page_title = "Action Assurance";

$folder = "manage";
$page = "assurance";

/*$menuT = array(
	1 => array('id'=> "risk_assurance",'url'=>"../manage/risk_assurance.php",'active'=> "",'display'=>"|OBJECTNAME|"),
	2 => array('id'=> "action_assurance",'url'=>"action_assurance.php",'active'=> "Y",'display'=>"|ACTIONNAME|"),
);*/


$page_title = "|ACTIONNAME|";

require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#actions").action({risk_id:<?php echo $_GET['id']; ?>, assurance: true, extras:false,  loggedInUser:$("#userid").val(), section:"manage", objectName: window.risk_object_name, actionName: risk_action_name});
	})
</script>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form id="action-assurance-form">
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
	<table id="assurance_action_table" border="1" align="left" width="auto" class="noborder">
    <tr>
    <td valign="top" class="noborder">
		<table id="risk_action_assurance" border="1" align="left">
        	<tr>
            	<td colspan="2"><h4><?php echo $risk_object_name; ?> Information</h4></td>
            </tr>
			
			<tr id="goback">
				<td align="left" class="noborder"><?php $me->displayGoBack("",""); ?></td>
				<td class="noborder"></td>
			</tr>			
        </table>
    </td>
    <td valign="top" class="noborder">
    	<h4 style="clear:both;">List of <?php echo $action_object_name; ?> associated to <?php echo $risk_object_name; ?></h4>
    	<div id="list_risk_ations"></div>
    </td>
    </tr>
    </table>
	<input type="hidden" value="<?php echo $_REQUEST['id']; ?>" id="risk_id" name="risk_id"  /> 
</form>

