<?php
$scripts 	= array( 'update_action.js','menu.js', 'ajaxfileupload.js' );
$styles 	= array( 'colorpicker.css',   );
$page_title = "Action Edit";
require_once("../inc/header.php");
$stat 		= new ActionStatus("", "", "");
$statuses 	= $stat -> getStatuses();
$update 	= new RiskAction("", "", "", "", "", "", "", "", "");
$latestUp 	= $update -> getAAction( $_REQUEST['id'] );
$updates    = $update -> getLatestActionUpdates( $_REQUEST['id'] );
?>
<script>
$(function(){
	$("table#update").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj("");//JSdisplayResultObj(""); ?>
<form method="post" id="update-action-form" name="update-action-form">
	<table align="left" width="100%" border="1" id="update" class="noborder">
    	<tr>
        	<td valign="top" align="left" width="50%"  class="noborder">
				<table width="100%">
					<tr>
						<td colspan="2"><h4>Action Details</h4></td>
					</tr>
					<tr>
						<th>Reference:</th>
						<td><?php echo $latestUp['id']; ?></td>
					</tr>
					<tr>
						<th>Risk Action:</th>
						<td><?php echo $latestUp['action']; ?></td>
					</tr>					
					<tr>
						<th>Deliverable:</th>
						<td><?php echo $latestUp['deliverable']; ?></td>
					</tr>										
					<tr>
						<th>Timescale:</th>
						<td><?php echo $latestUp['timescale']; ?></td>
					</tr>							
					<tr>
						<th>Deadline:</th>
						<td><?php echo $latestUp['deadline']; ?></td>
					</tr>											
					<tr>
						<th>Progress:</th>
						<td><?php echo ($latestUp['progress'] == "" ? "0%" : $latestUp['progress']."%"); ?></td>
					</tr>
					<tr>
						<th>Action Owner:</th>
						<td><?php echo ($latestUp['tkname']." ".$latestUp['tksurname']); ?></td>
					</tr>															
					<tr>
						<th>Action Status:</th>
						<td><?php echo $latestUp['updatedstatus']; ?></td>
					</tr>																																										
				    <tr>
				    	<th>Attachments</th>
				    	<td>
					    	<?php 
					    		if( trim($latestUp['attachment']) != "")
					    		{
					    			$attachments = unserialize( $latestUp['attachment'] );
					    			if(isset($attachments) && !empty($attachments))
					    			{
					    				echo "<ul>";
					    				 foreach( $attachments as $index => $filename)
					    				  {
											$ext = substr( $filename , strpos($filename, ".")+1);
											$file = $index.".".$ext;
											if( file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$file)){						
												echo "<li id='parent_".$index."' class='attachmentlist'><span><a href='download.php?folder=action&file=".$file."&new=".$filename."&content=".$ext."&modref=".$_SESSION['modref']."&company=".$_SESSION['cc']."'>".$filename."</span></li>";
											}    				  	
					    					//echo "<li id='li_".$index."'><a href='#'>".$filename."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id='".$index."'>Remove</a></li>";    						
					    				  }
					    				echo "</ul>";
					    			}
					    		}    		
					    	?>
				 		</span><br />
				    	</td>            
				    </tr>					
					<tr>
						<td class="noborder"><?php $me->displayGoBack("",""); ?></td>
						<td class="noborder"><?php $admire_helper->displayAuditLogLink( "actions_update" , false); ?></td>
					</tr>
				</table>
            </td>
            <td valign="top" align="right" width="50%"  class="noborder">
            	<table id="new_update" width="100%">
                	<tr>
                    	<td colspan="2" ><h4>New Action Update</h4></td>
                    </tr>
                    <tr>
                    	<th>Description:</th>
                        <td><textarea class="mainbox" id="update_description" name="update_description"><?php //echo $latestUp['description']; ?></textarea></td>
                    </tr>
                    <tr>
                    	<th>Status:</th>
                        <td>
                        <select id="update_status" name="update_status">
                        	<option>--status--</option>
                            <?php
                            foreach( $statuses as $status ) {
							?>
                            	<option value="<?php echo $status['id']; ?>"
                                 <?php if($status['name']=="New") { ?> 
                                 	disabled="disabled" 
                               <?php } if(($status['id'] == $latestUp['updatestatusid']) || ($status['name'] == "New") ) { ?>
                                selected="selected" 
								<?php } ?>>
								<?php echo $status['name']; ?>
                               </option>  
                             <?php
							}
                            ?>
                        </select>
                        </td>
                    </tr>
                    <tr>
                    	<th>Progress:</th>
                        <td>
                        	<input type="text" name="update_progress" id="update_progress" value="<?php echo $latestUp['progress']; ?>" />
                            <em style="color:#FF0000">%</em>
                        </td>
                    </tr>
                    <tr>
                    	<th>Remind On:</th>
                        <td><input type="text" name="update_remindon" id="update_remindon" value="<?php echo $latestUp['remindon']; ?>"  class="datepicker" readonly="readonly"/></td>
                    </tr>
                    <tr>
                    	<th>Attachment:</th>
                        <td>
                        <span id="file_loading">
                        	<ul id="attachment_list">
                            <?php 
                        		if( trim($updates['attachment']) != "" )
                        		{
                        			$updateAttachment = unserialize( $updates['attachment'] );
                        			if( isset($updateAttachment) && !empty($updateAttachment))
                        			{
                        				foreach( $updateAttachment as $index => $file)
                        				{
											$ext 	  	= substr( $file , strpos($file, ".")+1);
											$filename = $index.".".$ext;
											if( file_exists("../../files/".$_SESSION['cc']."/RGSTR/action/".$filename)){						
												echo "<li id='parent_".$index."' class='attachmentlist'><span><a href='download.php?folder=action&file=".$filename."&new=".$file."&content=".$ext."&company=".$_SESSION['cc']."'>".$file."</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' title='".$file."' id='".$index."' style='color:red' class='remove removeattach'>Remove</a></span></li>";
											}                          					
                        				}
                        			} 	
                        		}
                        	?>
                        	</ul>
                        </span>
				        <input type="file" id="action_attachments" name="action_attachments" onchange="ajaxFileUpload(this)" />
                        <!--  <input type="submit" name="attach_another" id="attach_another" value="Add Another" />  -->
                        </td>
                    </tr>
                    <tr>
                    	<th></th>
                        <td>
                        	<input type="checkbox" name="update_approval" id="update_approval" 
							<?php if($latestUp['progress'] !== "100") { ?> disabled="disabled" <?php } ?>  />
                            Request approval that the action has been addressed
                        </td>
                    </tr>
                    <tr>
                    	<th></th>
                        <td>
                        	<input type="submit" name="save_action_update" id="save_action_update" value=" Save " class="isubmit" />
                            <input type="submit" name="cancel_action_update" id="cancel_action_update" value=" Cancel " class="idelete"  />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>" class="logid"  />
</form>
</div>
<div id="view_action_updates" style="clear:both;"></div>