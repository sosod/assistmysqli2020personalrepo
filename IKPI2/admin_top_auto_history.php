<?php
require_once("inc_header.php");
$first_sdbip_id = $sdbipObject->getFirstSDBIPID();
$get_history_with_no_id = ($first_sdbip_id == $sdbip_id);


$topObject = new SDBP6_TOPKPI();
$cmpcode = strtolower($topObject->getCmpCode());
$modref = $topObject->getModRef();
?><p style="font-style: italic; margin-top: 0px;">As At <?php echo date("d M Y H:i"); ?></p>
<?php
$path = strtoupper($modref)."/STASKS";
$topObject->checkFolder($path);
$path = "../files/$cmpcode/".$path."/";
$files = glob($path."*");

//$files = array_reverse($files);
if(count($files) > 0) {
	echo "<ol>";
	$files_to_display = array();
	foreach($files as $f) {
		$fn = explode("/", $f);//strFn("explode",$f,"/","");
		$file = $fn[count($fn) - 1];
		$d = explode("_", $file);
		if((count($d) == 3 && $get_history_with_no_id) || (count($d) == 4 && $d[1] == $sdbip_id)) {
			$object_type = $d[0];
			if($object_type == $topObject->getMyObjectType()) {
				if(count($d) == 3) {
					$t = $d[1]."_".$d[2];
				} else {
					$t = $d[2]."_".$d[3];
				}
				$startdate = $topObject->getFileDate($t);
				$files_to_display[$startdate] = $file;
			}
		}
	}
	krsort($files_to_display);
	foreach($files_to_display as $startdate => $file) {
		echo "<li><a href=# id=".$file." class=auto_hist>".date("d M Y H:i:s", $startdate)."</a>";
	}
	echo "</ol>";
} else {
	echo "<p>No history available.</p>";
}
?>
<script type=text/javascript>
	$(function () {
		$("a.auto_hist").click(function () {
			i = $(this).attr("id");
			document.location.href = '<?php echo $path; ?>/' + i;
		});
	});
</script>