<?php
require_once("inc_header.php");

$uaObject = new SDBP6_USERACCESS();
$prior_sdbip = $uaObject->getPriorSDBIPModuleDetails();

//error trap to check that user hasn't ended up on this page by mistake
// & that prior sdbip does actually exist
if($prior_sdbip === false) {
	echo "
	<div width=750px class=tbl-error style='width:750px'>
			<h1>Error</h1>
			<p>Prior module could not be found.  Please go back and try again.</p>
	</div>";
	die();
}

$map = $uaObject->getMapOfPriorSDBIPUserAccess();
$prior_users = $uaObject->getPriorSDBIPUserAccess($prior_sdbip['modref']);
$active_users = $uaObject->getActiveUsersFormattedForSelect();
$ua_field_names = $uaObject->getUserAccessFields();
$ua_field_defaults = $uaObject->getUserAccessDefaults();

ASSIST_HELPER::displayResult(array("info", "Users which have already been granted access will not appear on this page.<br />For an explanation of the various types of user access, please see the previous page."));
?>
<form name=frm_user>
	<table id=tbl_useraccess>
		<thead>
		<tr>
			<td colspan=2></td>
			<?php
			foreach($ua_field_names as $fld => $name) {
				echo "<td class='b center green'>".(!isset($map[$fld]) || $map[$fld] === false ? "NEW" : "")."</td>";
			}
			?>
			<td></td>
		</tr>
		<tr>
			<th colspan=2>User</th>
			<?php
			foreach($ua_field_names as $fld => $name) {
				echo "<th class='".(!isset($map[$fld]) || $map[$fld] === false ? "new" : "")."'>".str_replace(" ", "<br />", $name)."</th>";
			}
			?>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$rows_displayed = 0;
		foreach($prior_users as $user_id => $p) {
			if(!isset($active_users[$user_id])) {
				$rows_displayed++;
				echo "
			<tr>
				<td class='b right'>".$user_id."<input type=hidden name=tkid[$user_id] id=tkid_".$user_id." value=$user_id class=user_id /></td><td class=b>".$p['user_name']."</td>";
				foreach($ua_field_names as $fld => $name) {
					if(isset($map[$fld]) && $map[$fld] !== false) {
						$val = $p[$map[$fld]];
						$class = "center";
					} else {
						$class = "center new";
						$val = $ua_field_defaults[$fld];
					}
					echo "<td class='".$class."'>";
					$js .= $displayObject->drawFormField("BOOL_BUTTON", array('name' => $fld."[$user_id]", 'id' => $fld."_".$user_id, 'yes' => 1, 'no' => 0, 'form' => "vertical"), $val);
					echo "</td>";
				}
				echo "<td class='last center'><button class=btn-save-one>".$uaObject->getActivityName("SAVE")."</button></td>";
				echo "
			</tr>";
			}
		}
		?>
		</tbody>
		<?php
		if($rows_displayed > 0) {
			echo "

		<tfoot>
			<tr>
				<td colspan=".(count($ua_field_names) + 3)." class=center><button id=btn_save_all class=btn-save-all>".$uaObject->getActivityName("SAVE")." All</button></td>
			</tr>
		</tfoot>";
		} else {
			echo "

		<tfoot>
			<tr>
				<td colspan=".(count($ua_field_names) + 3)." >No users available to import.</td>
			</tr>
		</tfoot>";

		}
		?>
	</table>
</form>
<script type="text/javascript">
	$(function () {

		<?php echo $js; ?>
		$("#tbl_useraccess tr:first").find("td").css("border", "1px solid #ffffff");
		$("#tbl_useraccess td.new").css("background", "#eeffee");
		$("#tbl_useraccess th.new").css("background", "#009900");

		$("button.btn-save-all").button({
			icons: {primary: "ui-icon-disk"}
		}).addClass("ui-button-bold-green").removeClass("ui-state-default")
			.hover(function () {
				$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
			}, function () {
				$(this).addClass("ui-button-bold-green").removeClass("ui-button-bold-orange");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var $form = $("form[name=frm_user]");
			var dta = AssistForm.serialize($form);
			var result = AssistHelper.doAjax("inc_controller.php?action=USERACCESS.saveMultiplePriorModuleUsers", dta);
			if (result[0] == "ok") {
				if (result[2] == 0) {
					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "setup_useraccess.php");
				} else {
					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
				}
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		});
		$("button.btn-save-one").button({
			icons: {primary: "ui-icon-disk"}
		}).addClass("ui-button-minor-green").removeClass("ui-state-default")
			.hover(function () {
				$(this).removeClass("ui-button-minor-green").addClass("ui-button-minor-orange");
			}, function () {
				$(this).addClass("ui-button-minor-green").removeClass("ui-button-minor-orange");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = "";
			var user_id = "";
			$(this).parent().parent().find("input:hidden").each(function () {
				if ($(this).hasClass("user_id")) {
					user_id = $(this).val();
					dta = "tkid=" + $(this).val();
				} else {
					dta += "&" + $(this).prop("id") + "=" + $(this).val();
				}
			});
			var result = AssistHelper.doAjax("inc_controller.php?action=USERACCESS.saveSinglePriorModuleUser", dta);
			AssistHelper.finishedProcessing(result[0], result[1]);
			if (result[0] == "ok") {
				$(this).parent().parent().find("td:gt(1)").each(function () {
					if ($(this).hasClass("last")) {
						var h = "";
					} else {
						var x = $(this).find("input:hidden:first").val();
						if (parseInt(x) == 1) {
							var h = "<span class=green>Yes</span>";
						} else {
							var h = "<span class=red>No</span>";
						}
					}
					$(this).html(h);
				});
				$("#tkid_" + user_id).val("");
			}
		});
	});
</script>