<?php

if(isset($mod_object) && ASSIST_HELPER::checkIntRefForWholeIntegersOnly($mod_object)) {
	$sdbip_id = $mod_object;
} else {
	//first get sdbip_id
	$sdbipObject = new SDBP6_SDBIP($modref);
	$sdbip = $sdbipObject->getAvailableSDBIPsForChooser("ACTIVATED");
	//get oldest SDBIP
	if(count($sdbip) > 0) {
		$sdbip_ids = array_keys($sdbip);
		$sdbip_id = $sdbip_ids[count($sdbip_ids) - 1];
	} else {
		$sdbip_id = 0;
	}
}
$head = array();
$actions = array();


//HEADINGS
$headingObject = new SDBP6_HEADINGS($modref);
$headings = $headingObject->getMainObjectHeadings($available_objects[0], "LIST", "DASHBOARD", "", true);
foreach($headings as $f => $h) {
	$head[$f] = array(
		'text' => $h['name']
	);
}
$head['time'] = array('text' => $headingObject->replaceAllNames("|time|"), 'class' => "right");
$head['deadline'] = array('text' => "Deadline", 'class' => "center");
$head['link'] = array('text' => "");


//USER ACCESS
$userObject = new SDBP6_USERACCESS($modref);
$my_user_access = $userObject->getMyUserAccess();
if($my_user_access['time_third'] == true) {
	$time_user = "tertiary";
} elseif($my_user_access['time_second'] == true) {
	$time_user = "secondary";
} else {
	$time_user = "primary";
}

//GET TIME PERIODS
$timeObject = new SDBP6_SETUP_TIME($modref);
$dashboard_time = $timeObject->getTimePeriodsForFrontPageDashboard($time_user, $future, $sdbip_id);
$available_time_periods = $dashboard_time['available'];
$all_time_periods = $dashboard_time['all'];
//$available_time_periods = $timeObject->getAvailableTimePeriodsForFrontPageDashboard($time_user,$future);
//$all_time_periods = $timeObject->getAllTimePeriodsForFrontPageDashboard($time_user,$future);

//ASSIST_HELPER::arrPrint($available_time_periods);

//ADMIN ACCESS
$adminObject = new SDBP6_SETUP_ADMINS();
$admin_access = $adminObject->getMyUpdateAccessForFrontPageDashboard($sdbip_id);

//ASSIST_HELPER::arrPrint($admin_access);


$available_objects = array("DEPTKPI");//,"TOPKPI","PROJECT");
$admin_sections = array('DEPTKPI' => SDBP6_DEPTKPI::ADMIN_SECTION, 'TOPKPI' => SDBP6_TOPKPI::ADMIN_SECTION, 'PROJECT' => SDBP6_PROJECT::ADMIN_SECTION);


//foreach object type
foreach($available_objects as $object_type) {
	$admin_section = $admin_sections[$object_type];
	if(isset($admin_access[$admin_section])) {
		$aa = $admin_access[$admin_section];
		//Added PARENT_ORG as an option - found that the getLoginStats function isn't triggered if a user does not have top level org structure update access EXCEPT they can have a KPI rejected & returned to them which then doesn't show. AA-540 JC 17 March 2021
		if(count($aa['ORG']) > 0 || count($aa['OWNER']) > 0 || count($aa['PARENT_ORG']) > 0) {
			$class = "SDBP6_".$object_type;
			$myObject = new $class();
			if($myObject->getMyObjectType() == $available_objects[0]) {
				$head[$myObject->getResultsRevisedFieldName()]['class'] = "right";
			}
			/** @var SDBP6_DEPTKPI $myObject */
			$objects = $myObject->getLoginTableObjects($available_time_periods[$myObject->getTimeType()], $aa['ORG'], $aa['OWNER'], $future, $headings, $all_time_periods[$myObject->getTimeType()], $sdbip_id);
			foreach($objects as $i => $o) {
				$actions[$object_type.$i] = $o;
			}
		}
	}
}

//ASSIST_HELPER::ArrPrint($head);

?>