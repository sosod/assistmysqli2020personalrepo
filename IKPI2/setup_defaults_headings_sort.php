<?php
include("inc_header.php");

$section = $_REQUEST['section'];
$page_type = strtoupper($_REQUEST['type']);

//$items = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id'=>$parent_id));

$headings = $headingObject->replaceAllNames($headingObject->getMainObjectHeadings($section, $page_type));

if($page_type == "LIST") {

} else {
	$headings = $headings['rows'];
}

$items = array();
foreach($headings as $id => $head) {
	$items[$id] = $head['name'];
}

//ASSIST_HELPER::arrPrint($headings);


if(count($items) == 0) {
	ASSIST_HELPER::displayResult(array("error", "No items found to sort."));
	die();
}

switch($page_type) {
	case "LIST":
		echo "<h2>List Pages</h2>";
		break;
	default:
		echo "<h2>Detailed View / Form Pages</h2>";
		break;
}
?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<div style='border: 1px solid #999999; border-radius:5px;padding-right:50px'>
				<form name=frm_sort>
					<input type=hidden name=section value='<?php echo $section; ?>' />
					<input type=hidden name=page_type value='<?php echo $page_type; ?>' />
					<ul id=ul_sortable>
						<?php
						foreach($items as $key => $i) {
							echo "<li class=ui-state-default style='background:url(); background-color:#FFFFFF'><img src=/pics/sort_icon.png />&nbsp;".$i."<input type=hidden name=order[] value='".$key."' /></li>";
						}
						?>
					</ul>
				</form>
			</div>
		</td>
	</tr>
	<Tr>
		<td class=center>
			<button id=btn_save><?php echo $helper->getActivityName("save"); ?> Display Order</button>
		</td>
	</Tr>
	<tr>
		<td>
			<?php $js .= $displayObject->drawPageFooter($helper->getGoBack($_SERVER['HTTP_REFERER'])); ?>
		</td>
	</tr>
</table>
<style>

	#ul_sortable {
		list-style-image: url();
		list-style-type: none;
		margin: 0;
		padding: 0;
		width: 100%
	}

	#ul_sortable li {
		padding: 10px;
		margin: 7px;
		height: 20px;
		width: 100%
	}

	.ui-state-highlight {
		height: 1.5em;
		line-height: 1.2em;
	}
</style>
<script type="text/javascript">
	$(function () {
		$("#ul_sortable").sortable({}).disableSelection();

		$("#btn_save").button({
			icon: {primary: "ui-icon-disk"}
		}).removeClass("ui-state-default").addClass("ui-button-state-green")
			.hover(function () {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_sort]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=HEADINGS.SORT", dta);
			if (result[0] == "ok") {
				AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "<?php echo $_SERVER['HTTP_REFERER']; ?>");
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}

		});


		var width = 0;
		$("#ul_sortable li:first").each(function () {
			width = ($(this).width());
		});
		$("#ul_sortable li").closest("table").width(width + 75);
		$("#ul_sortable li").closest("div").width(width + 50);

	});
</script>