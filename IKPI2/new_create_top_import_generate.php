<?php
require_once("../module/autoloader.php");


$myObject = new SDBP6_TOPKPI();
$object_type = $myObject->getMyObjectType();
$results_object_type = $myObject->getMyChildObjectType();
$has_time = true;
$time_setting = $myObject->getTimeSetting();
//$sdbip_id = $myObject->getParentID(0); - not returning valid SDBIP ID.  Need to go directly to session to get current SDBIP ID  #AA-627 JC 9 June 2021
$sdbip_details = $myObject->getCurrentSDBIPFromSessionData("NEW");
$sdbip_id = $sdbip_details['id'];

$headingObject = new SDBP6_HEADINGS();
$headings = $headingObject->getMainObjectHeadings($object_type, "IMPORT", "NEW", "", true);
$results_headings = $headingObject->getMainObjectHeadings($results_object_type, "IMPORT", "NEW", "", true);


include("common/import_template_generator.php");

/*
$fdata = "\"Parent GUID\",\"Line Item GUID\",\"Line Item Name\",\"Description\",\"Has sub-levels?\",\"Can be assigned?\"\r\n\"\",\"Required & Unique\",\"Required\",\"\",\"Yes / No\",\"Yes / No\"\r\n";

$me = new MSCOA1_HELPER();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$today = time();

        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/".$modref."_template_".date("Ymd_Hi",$today).".csv";
        $newfilename = "template.csv";
        $file = fopen($filename,"w");
        fwrite($file,$fdata."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);

*/
?>