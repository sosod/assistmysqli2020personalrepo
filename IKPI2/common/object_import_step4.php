<?php
//ASSIST_HELPER::arrPrint($_REQUEST);

//Get data from temporary table
$import_ref = $_REQUEST['import_ref'];
$sql = "SELECT * FROM ".$myObject->getTableName()."_temp WHERE import_ref = '".$import_ref."'";
$data = $myObject->mysql_fetch_all_by_id($sql, "import_key");


//setup variables
$copy_fields = array();
foreach($headings['rows'] as $fld => $Head) {
	$copy_fields[] = $fld;
}

//process data
if(count($data) > 0) {
	echo "<p class=b>Importing:<ul>";
	//loop through each row from temp table and save to live table
	foreach($data as $key => $row) {
		//add one to the key to match the row number in excel (starts at 1) where key comes from array key (starts at 0)
		echo "<li>Row ".($key + 1)."...";
		//get data for new list item
		$import_data = array();
		//parent id - handled in step 3 instead
		$import_data[$myObject->getParentFieldName()] = $row[$myObject->getParentFieldName()];
		foreach($copy_fields as $fld) {
			$import_data[$fld] = $row[$fld];
		}
		//process results
		$results = unserialize(base64_decode($row['results']));
		foreach($results as $k => $r) {
			$import_data[$k] = $r;
		}
		$import_data[$myObject->getResultsAdjustmentsFieldName()] = array();
		//send data to SDBP6_*->addObject for processing
		$result = $myObject->addObject($import_data);
		if($result[0] == "ok") {
			echo " done.</li>";
		} else {
			echo "<span class=red>Error: ".$result[1]."</span></li>";
		}

	}//end foreach data as row
	echo "</ul>
	";
//update temp table to acknowledge successful import
	$sql = "UPDATE ".$myObject->getTableName()."_temp SET import_status = 1 WHERE import_ref = '".$import_ref."'";
	$myObject->db_update($sql);

	echo "
	<script type=text/javascript>
	$(function() {
		AssistHelper.processing();
		AssistHelper.finishedProcessingWithRedirect('ok','Import completed successfully','".$my_url."');
	});
	</script>";

} else {
	ASSIST_HELPER::displayResult(array("error", "No data found to import - the holding table does not contain any data for reference: ".$import_ref.".  Please try again otherwise contact your Assist Administrator."));

}
?>