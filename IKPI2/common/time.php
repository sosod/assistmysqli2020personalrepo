<?php
/**
 * Required Object - timeObject comes out of inc_header
 */
/** @var SDBP6_SDBIP $sdbipObject - from inc_header */

require_once("inc_header.php");


if(isset($_REQUEST['my_sdbip_id'])) {
	$sdbip_id = $_REQUEST['my_sdbip_id'];
	$sdbip_details = $sdbipObject->getRawObject($sdbip_id);
	$sdbip_details = $sdbipObject->formatSDBIPRecord($sdbip_details);
}

/**
 * Double check to confirm that user can have access to this page
 */
$has_right_user_access = false;
//page can only be accessed from manage or admin sections
if($page_section == "MANAGE" || $page_section == "ADMIN") {
	//get logged in user's user access settings
	$userObj = new SDBP6_USERACCESS();
	$my_user_access = $userObj->getMyUserAccess();
	//check if in manage & manage access granted or in admin & admin access granted - if yes, then grant access to page
	if($page_section == "MANAGE" && $my_user_access['manage_time'] == true) {
		$has_right_user_access = true;
	} elseif($page_section == "ADMIN" && $my_user_access['admin_time'] == true) {
		$has_right_user_access = true;
	}
} else {
	//wrong section therefore no user access can have been given therefore assume no permissions
	$has_right_user_access = false;
}
if(!isset($has_right_user_access) || $has_right_user_access !== true) {
	die(ASSIST_HELPER::displayResult(array("error", "You do not have the necessary user access permissions to access this page.")));
}


$all_time = $timeObject->getActiveTimePeriodsForSetup($sdbip_id);


$time_users = $timeObject->getTimeUsers();
$time_headings = array(
	'ref' => "Ref",
	'display_start' => "Start Date",
	'display_end' => "End Date",
	'code' => "Code",
);
$time_tu_headings = array(
	'display_status' => "Status",
	'rem' => "Reminder",
	'close' => "Closure",
);
?>
	<table id=tbl_time_list>
		<tr>
			<?php
			foreach($time_headings as $tr => $th) {
				echo "<th rowspan=2>".$th."</th>";
			}
			foreach($time_users as $tu => $tu_name) {
				echo "<th colspan=3>".$tu_name."</th>";
			} ?>
			<th rowspan=2></th>
		</tr>
		<tr>
			<?php
			foreach($time_users as $tu => $tu_name) {
				foreach($time_tu_headings as $tur => $tuh) {
					echo "<th>$tuh</th>";
				}
			} ?>
		</tr>
		<?php
		foreach($all_time as $time_id => $time) {
			echo "
	<tr>";
			foreach($time_headings as $tr => $th) {
				echo "<td>".$time[$tr]."</td>";
			}

			foreach($time_users as $tu => $tu_name) {
				//Status: now < start date = "NYS", start_Date < now < closure = "Open", else "Closed";
				foreach($time_tu_headings as $tur => $tuh) {
					echo "
			<td id=td_".$tu."_".$time_id.">".$time[$tu][$tur]."</td>
			";
				}
			}
			echo "
		<td><button class=btn-edit ref=".$time_id.">".$timeObject->getActivityName("EDIT")."</button></td>
	</tr>
	";
		}
		?>
	</table>
	<table id=tbl_footer class=noborder>
		<tr>
			<td class=noborder><?php
				$js .= $displayObject->drawPageFooter($helper->getGoBack($src_url), "setup", array('section' => "TIME"));
				?></td>
		</tr>
	</table>
	<div id=div_notice style='width:500px'>
		<?php
		ASSIST_HELPER::displayResult(array("info", "<span class=b>* Please Note:</span><ul class=black><li>Start and End dates are inclusive.</li><li>Reminders will be emailed on the morning of the date selected.</li><li>Time periods will be closed at 23:59 on the date selected.</li></ul>"))
		?>
	</div>
	<div id=div_edit>
		<table id=tbl_container class='tbl-container not-max'>
			<tr>
				<td>
					<form name=frm_edit>
						<input type=hidden name=time_id id=time_id value="" />
						<h2 style='margin-top: 0px'><?php echo $timeObject->replaceAllNames("|edit| |".$timeObject->getMyObjectType()."|"); ?> <label id=lbl_ref></label></h2>
						<h3><label id=lbl_start></label> - <label id=lbl_end></label></h3>
						<table id=tbl_edit class=form>
							<tr>
								<th><?php echo $time_headings['code']; ?>:</th>
								<td><?php
									$js .= $displayObject->drawSmallInputText("", array('id' => "code", 'name' => "code"));
									?></td>
							</tr>
							<?php foreach($time_users as $tu => $tu_name) {
								//if page section = admin then offer open button otherwise nothing
								$button = "";
								if($page_section == "ADMIN") {
									$open_button = "<button id=btn_".$tu."_open class=btn-open ref='".$tu."'>Open</button>";
								} else {
									$open_button = "";
								}
								echo "
			<tr>
				<th>".$tu_name.":</th>
				<td>
					<table id=tbl_".$tu." class=sub-table>
						<tr>
							<td class=b>".$time_tu_headings['display_status'].":</td>
							<td><label for=btn_".$tu."_close id=lbl_".$tu."_status></label><span class=float></span></td>
							<td rowspan=3 class='center'><p style='margin-top:0px'><button id=btn_".$tu."_close class=btn-close ref='".$tu."'>Close</button>".$open_button."</p></td>
						</tr>
						<tr>
							<td class=b>".$time_tu_headings['rem'].":</td>
							<td>";
								$js .= $displayObject->drawDatePicker("", array('id' => "rem_".$tu, 'name' => "rem_".$tu));
								echo " &nbsp;&nbsp;<button class=btn-clear ref='rem_".$tu."'>Clear</button>
							</td>
						</tr>
						<tr>
							<td class=b>".$time_tu_headings['close'].":</td>
							<td>";
								$js .= $displayObject->drawDatePicker("", array('id' => "close_".$tu, 'name' => "close_".$tu));
								echo " &nbsp;&nbsp;<button class=btn-clear ref='close_".$tu."'>Clear</button>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
							} ?>
						</table>
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
		$(function () {
			<?php echo $js; ?>



			/******************************************************************************
			 * Page specific JS
			 */

			$("#tbl_time_list tr td").addClass("center");
			$("#tbl_time_list tr").find("td:first").addClass("b");
			var url = "<?php echo $_SERVER['REQUEST_URI']; ?>";

			<?php
			foreach($time_users as $tu => $tu_name) {
				echo "
	var original_min_close = new Array();
	original_min_close['".$tu."'] = new Date();";
				echo "
			$('#rem_".$tu."').datepicker('option','onSelect',  function() { remOnSelect('".$tu."') });
		";

			}
			?>

//format the edit buttons
			$("button.btn-edit").button({
				icons: {primary: "ui-icon-pencil"}
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(function () {
					$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
				}, function () {
					$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
				}).click(function (e) {
				e.preventDefault();

				//clear form
				$("#code").val("");
				$(".datepicker").each(function () {
					$.datepicker._clearDate(this);
				});

				var i = $(this).attr("ref");
				var data = AssistHelper.doAjax("inc_controller.php?action=TIME.getDetailsForEdit", "time_id=" + i);
				//console.log(data);
				$("#lbl_ref").text(data['ref']);
				$("#lbl_start").text(data['display_start']);
				$("#lbl_end").text(data['display_end']);
				$("#time_id").val(i);
				$("#code").val(data['code']);
				var current_rem = new Date();
				var current_close = new Date();
				<?php
				foreach($time_users as $tu => $tu_name) {
					echo "
			$(\"#lbl_".$tu."_status\").html(data['$tu']['display_status']);
			if(data['$tu']['is_open']==true) {
				$(\"#btn_".$tu."_close\").show();
				$(\"#btn_".$tu."_open\").hide();
				$('#rem_".$tu."').datepicker('enable');
				$('#close_".$tu."').datepicker('enable');
			} else {
				$(\"#btn_".$tu."_close\").hide();
				$(\"#btn_".$tu."_open\").show();
				$('#rem_".$tu."').datepicker('disable');
				$('#close_".$tu."').datepicker('disable');
			}
			$('#rem_".$tu."').datepicker('option','minDate',data['$tu']['rem_min']);
			$('#close_".$tu."').datepicker('option','minDate',new Date(data['$tu']['close_min']));
			original_min_close['".$tu."'] = new Date(data['$tu']['close_min']);

			if(data['$tu']['rem']!=\"-\") {
				current_rem = new Date(data['$tu']['rem']);
				$('#rem_".$tu."').datepicker('setDate',current_rem);
				remOnSelect('".$tu."');
			}
			if(data['$tu']['close']!=\"-\") {
				current_close = new Date(data['$tu']['close']);
				$('#close_".$tu."').datepicker('setDate',current_close);
			}

			";
				}



				?>

				$("#div_edit").dialog("option", "width", "1000px");
				$("#div_edit").dialog("open");
				$("#div_edit").dialog("option", "width", $("#tbl_container").width() + 50);

			});

//format the sub-tables for the time_user settings
			$("table.sub-table").removeClass("form").css("border", "0px").css("margin", "0px").find("td").css("border", "0px");

//button to clear the datepicker fields in the edit dialog
			$("button.btn-clear").button({}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(function () {
					$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
				}, function () {
					$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
				}).click(function (e) {
				e.preventDefault();
				var r = $(this).attr("ref");
				$("#" + r).val("");
			}).hide();

//button to reopen closed time periods - only available on ADMIN page
			$("button.btn-open").button({}).removeClass("ui-state-default").addClass("ui-button-state-green")
				.hover(function () {
					$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");
				}, function () {
					$(this).addClass("ui-button-state-green").removeClass("ui-button-state-orange");
				}).click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				var tu = $(this).attr('ref');
				var time_id = $("#time_id").val();
				var dta = "time_id=" + time_id + "&tu=" + tu;
				var result = AssistHelper.doAjax("inc_controller.php?action=TIME.openTime", dta);
				//Don't redirect after opening in case user wants to add reminder / closure date
				//but hide open button and show close button
				//if(result[0]=="ok") {
				//	AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
				//} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
				//}
				$("#btn_" + tu + "_close").show();
				$("#btn_" + tu + "_open").hide();
				$("#rem_" + tu).datepicker('enable');
				$("#close_" + tu).datepicker('enable');
				$("#lbl_" + tu + "_status").html(result[2]);
				$("#td_" + tu + "_" + time_id).html(result[2]);
			});

//button to close time periods
			$("button.btn-close").button({}).removeClass("ui-state-default").addClass("ui-button-state-red")
				.hover(function () {
					$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-red");
				}, function () {
					$(this).addClass("ui-button-state-red").removeClass("ui-button-state-orange");
				}).click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				var tu = $(this).attr('ref');
				var time_id = $("#time_id").val();
				var dta = "time_id=" + time_id + "&tu=" + tu;
				var result = AssistHelper.doAjax("inc_controller.php?action=TIME.manualCloseTime", dta);
				if (result[0] == "ok") {
					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			});


			var w = $("#tbl_container").width() + 50;
			$("#div_edit").dialog({
				modal: true,
				autoOpen: false,
				width: w,
				buttons: [{
					text: "Save Changes",
					icons: {primary: "ui-icon-disk"},
					click: function () {
						AssistHelper.processing();
						var dta = AssistForm.serialize($("form[name=frm_edit]"));
						var result = AssistHelper.doAjax("inc_controller.php?action=TIME.edit", dta);
						if (result[0] == "ok") {
							AssistHelper.finishedProcessingWithRedirect(result[0], result[1], url);
						} else {
							AssistHelper.finishedProcessing(result[0], result[1]);
						}
					},
					class: "ui-button-bold-green"
				}, {
					text: "Cancel",
					icons: {primary: "ui-icon-closethick"},
					click: function () {
						$(this).dialog("close");
					},
					class: "ui-button-minor-grey"
				}]
			});
			AssistHelper.hideDialogTitlebar("id", "div_edit");

//on changing of reminder date check if minimum closure date needs to be adjusted
			function remOnSelect(tu) {
				//get new reminder
				var new_rem_date = $("#rem_" + tu).datepicker("getDate");
				//if it is not null
				if (new_rem_date) {
					//if new reminder falls on a date later than the minimum provided by the database (i.e. end date + 1)
					if (new_rem_date > original_min_close[tu]) {
						//clear the closure datepicker so that any entered value has to fall inside new minimums
						//this must happen before changing the min as that auto changes the setDate
						if ($("#close_" + tu).datepicker("getDate") < new_rem_date) {
							$.datepicker._clearDate($("#close_" + tu));
						}
						//change the closure minimum date to be the same as the new reminder (rem happens at 00:00, close happens at 23:59 so both can happen on the same date)
						$("#close_" + tu).datepicker("option", "minDate", new_rem_date);
					} else {
						//otherwise reset back to the original minimum closure date
						$("#close_" + tu).datepicker("option", "minDate", original_min_close[tu]);
					}
				}

			}


			//formatting
			var footer_width = $("#tbl_time_list").width();
			$("#tbl_footer").width(footer_width);

		});
	</script>

<?php
//ASSIST_HELPER::arrPrint($sdbip_details);
//ASSIST_HELPER::arrPrint($all_time);
//ASSIST_HELPER::arrPrint($time_names);
?>