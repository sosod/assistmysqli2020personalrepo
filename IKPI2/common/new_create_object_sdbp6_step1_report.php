<?php
$is_import_from_sdbip_page = true;
$page_query_string = $_SERVER['QUERY_STRING'];


//skipping inc_new_statuscheck so need to get sdbip_details - assuming to be new
$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();
$my_sdbip_id = $sdbip_details['id'];
$source_sdbip_id = $sdbip_details['alt_module'];
$my_mscoa_version_code = $sdbip_details['mscoa_version'];
//record start of phase 1 of import
$import_ref = $sdbipObject->getExternalImportRef($sdbip_details, $page_object);

//use echo variable to output to screen for development purposes - using variable to make it easier to turn off for production
$echo = "";

//get external module settings
$old_modloc = $sdbip_details['alt_module_settings']['modlocation'];
$old_modref = $sdbip_details['alt_module'];
$linked_idp = $old_modref;
$old_modref = $sdbipObject->getModRef();
$old_dbref = $sdbipObject->getDBRef();//strtolower("assist_".$sdbipObject->getCmpCode()."_".$old_modref);

//create objects
$my_modref = $myObject->getModRef();
$field_prefix = $myObject->getTableField();
$external_class = "SDBP6_".$old_modloc;
$extObject = new $external_class($old_modref, $page_object, $myObject->getCmpCode(), true);

//get table
$alt_table = $myObject->getAltModuleTable($old_modloc);
if($alt_table === false) {
	//stop phase process and record error
	$myObject->endExternalImportPhaseWithError($import_ref, "Couldn't identify the source module's Project table.");
	die("ERROR!  Couldn't identify the source module's Project table.");
}
//get id field
$alt_id_field = $myObject->getAltModuleIDField($old_modloc, $old_modref);
if($alt_id_field === false) {
	//stop phase process and record error
	$myObject->endExternalImportPhaseWithError($import_ref, "Couldn't identify the source module's Project System Ref field.");
	die("ERROR!  Couldn't identify the source module's Project System Ref field.");
}


//get mapping
$new_field_settings = $headingObject->getHeadingsForInternalMapping($page_object, false, true);    //array('new_field'=>array('name'=>"",'type'=>"",'list'=>""))
$field_map = $extObject->getDefaultFieldMap($new_field_settings);//$old_modloc, $old_modref,$page_object);	//array('old_field'=>"new_field")
$field_map_by_new_field = array_flip($field_map);
$old_field_settings = $new_field_settings; //$extObject->getHeadingsForMapping($field_map,false,true);


//get lists from external module
//$old_lists = $extObject->getListItems($field_map,$old_field_settings,$old_modref,$linked_idp);

//get local lists
//$new_lists = $myObject->getListItemsForExternalMapping($field_map,$new_field_settings,false,$sdbip_details['mscoa_version']);

$all_lists = array();
$all_segments = array();
$listObject = new SDBP6_LIST();
$listObject->setSDBIPID($my_sdbip_id);
$segmentObject = new SDBP6_SEGMENTS();

//foreach headings - get lists & segments
foreach($new_field_settings as $fld => $head) {
	$list = $head['list'];
	switch($head['type']) {
		case "LIST":
		case "MULTILIST":
			$listObject->changeListType($list);
			$new_lists[$fld] = $listObject->getAllListItemsFormattedForSelect();
			break;
		case "SEGMENT":
		case "MULTISEGMENT":
			$segmentObject->setSection($list);
			$new_lists[$fld] = $segmentObject->getAllListItemsFormattedForSelect(array('version' => $my_mscoa_version_code));
			break;
		case "OBJECT":
		case "MULTIOBJECT":
			//get copied items for orgstructure - everything else is... TODO
			if(substr($list, 0, strlen("SDBP6_SETUP_ORGSTRUCTURE")) == "SDBP6_SETUP_ORGSTRUCTURE") {
				$l = explode("|", $list);
				$class_name = $l[0];
				$extra_info = isset($l[1]) ? $l[1] : "";
				$objObject = new $class_name();
				$objObject->setSDBIPID($my_sdbip_id);
				$new_lists[$fld] = $objObject->getActiveObjectsFormattedForSelect($extra_info);
			} else {
				$objObject = new $list();
				$objObject->setSDBIPID($my_sdbip_id);
				$new_lists[$fld] = $objObject->getAllListItemsFormattedForSelect();
			}
			break;
	}
}


//get insert records
$sql = "SELECT * FROM ".$myObject->getTableName()."_temp WHERE import_ref = '$import_ref'";
$insert_records = $myObject->mysql_fetch_all($sql);
//DISPLAY RESULTS
echo "
<h3>Phase 1: Results</h3>


<table><tr><th>Source Ref</th>";
foreach($new_field_settings as $fld => $head) {


	$extra_text = "";

	$new_field_settings[$fld]['mapped'] = true;

	echo "<th>".$head['name'].""
		.(strlen($extra_text) > 0 ? "
<br /><span class='i' style='font-weight:normal'>".$extra_text."</span>" : "")."</th>";
}
echo "</tr>";
foreach($insert_records as $i) {
	$i['link_details'] = unserialize($i['results']);
	if(!isset($i['link_details']['src_ref'])) {
		$i['link_details']['src_ref'] = $extObject->getRefTag().$i['link_details']['src_id'];
	}
//	ASSIST_HELPER::arrPrint($i['link_details']);
	echo "<tr>
		<td class='b'>".$i['link_details']['src_ref']."</td>";
	foreach($new_field_settings as $fld => $head) {
		if(strpos($head['list'], "|") !== false) {
			$l = explode("|", $head['list']);
			$head['list'] = $l[0];
		}
		$class = "";
		if(!$headingObject->isTextField($head['type'])) {
			$class = "center";
		}
		echo "<td class='$class'>";
		$nl = isset($new_lists[$fld]) ? $new_lists[$fld] : array();
		if(isset($i[$fld])) {//} && isset($field_map_by_new_field[$fld])) {
			if($head['list'] == "SDBP6_PROJECT") {
				$v = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $i[$fld]);
				$a = array();
				if(count($v) > 0) {
					foreach($v as $x) {
						if(isset($nl[$x])) {
							$a[] = $nl[$x];
						}
					}
					if(count($a) > 0) {
						echo implode("<br />", $a);
					} else {
						echo $myObject->getUnspecified();
					}
				} else {
					echo $myObject->getUnspecified();
				}
			} elseif($headingObject->isDateField($head['type'])) {
				echo date("d F Y", strtotime($i[$fld]));
			} elseif($head['type'] == "SEGMENT" || $head['type'] == "OBJECT" || $head['type'] == "LIST") {
				if($i[$fld] == 0 || !isset($nl[$i[$fld]])) {
					echo $myObject->getUnspecified();
				} else {
					echo $nl[$i[$fld]];
				}
			} elseif($head['type'] == "MULTISEGMENT" || $head['type'] == "MULTIOBJECT" || $head['type'] == "MULTILIST") {
				$v = $i[$fld];
				if($v == "0" || strlen($v) == 0) {
					echo $myObject->getUnspecified();
				} else {
					$v = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $v);
					$a = array();
					if(count($v) > 0) {
						foreach($v as $x) {
							if(isset($nl[$x])) {
								$a[] = $nl[$x];
							}
						}
					}
					if(count($a) > 0) {
						echo implode("<br />", $a);
					} else {
						echo $myObject->getUnspecified();
					}
				}
//			} elseif($headingObject->isListField($head['type'])) {
//				if(strpos($head['type'], "MULTI") !== false) {
//					$v = isset($i['link_details']['list_results'][$fld]) ? $i['link_details']['list_results'][$fld] : "";
//					if(!is_array($v) || count($v)==0) {
//						echo $myObject->getUnspecified();
//					} else {
//						$a = array();
//						foreach($v as $x) {
//							if($x[0]!="NEW") {
//								$new_list = $new_lists_for_display[$head['list']];
//								$a[] = $new_list[$x[1]];
//							} else {
//								$a[] = $myObject->getDisplayIcon("ok")." ".$added_list_items[$head['list']][$x[1]];
//							}
//						}
//						echo implode("<br />",$a);
//					}
//				} else {
//					if($i[$fld] == 0 || !isset($i['link_details']['list_results'][$fld])) {
//						echo $myObject->getUnspecified();
//					} else {
//						$v = $i['link_details']['list_results'][$fld];
//						if($v[0]!="NEW") {
//							$new_list = $new_lists_for_display[$head['list']];
//							echo $new_list[$i[$fld]];
//						} else {
//							echo $myObject->getDisplayIcon("ok")." ".$added_list_items[$head['list']][$i[$fld]];
//						}
//					}
//				}
			} elseif($headingObject->isBooleanField($head['type'])) {
				if($i[$fld] == 1) {
					echo "Yes";
				} else {
					echo "No";
				}
			} else {
				echo $i[$fld];
			}
		} else {
			echo ASSIST_HELPER::displayIconAsDiv("error");
		}
		echo "</td>";
	}
	echo "</tr>";
}

echo "</table>

";


?>
	<script type="text/javascript">
		$(function () {
			$("#div_status_icons").find("table").css("border", "0px solid #ffffff").find("td").css("border", "0px solid #ffffff");
			$("#div_status_icons").css("position", "absolute").css("right", "10px").css("top", "10px");
			$(".grey").css("color", "#777777");
		});
	</script>
<?php
//ASSIST_HELPER::arrPrint($sdbip_details);

?>