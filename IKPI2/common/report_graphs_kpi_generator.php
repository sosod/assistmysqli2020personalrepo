<?php
/**
 * @var SDBP6_DEPTKPI $myObject - set to DEPTKPI for dev purposes, could also be SDBP6_TOPKPI
 * @var INT $sdbip_id - from inc_header
 * @var SDBP6_HEADINGS $headingObject - from inc_header
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var string $js - from inc_header - variable to hold all javascript/jquery before echoing at the end of the document
 */
$object_type = $myObject->getMyObjectType();
$object_name = $myObject->getObjectName("KPI");
$object_names = $myObject->getObjectName("KPIS");
$filter_fields_not_allowed = $myObject->getGraphFilterFieldsNotAllowed();
$extra_filter_options = $myObject->getExtraFilterFieldsForGraphs();
$group_by_options = $headingObject->getHeadingsAvailableForGraphing($object_type, $filter_fields_not_allowed);
$field_types_allowed_for_grouping = array(
	"LIST", "MULTILIST", "SEGMENT", "MULTISEGMENT", "OBJECT", "MULTIOBJECT"
);

$available_filters = array();
//limit filter for now
$default_grouping = $myObject->getDepartmentFieldName();
$allowed_filters = array(
	$myObject->getDepartmentFieldName(),
);
if(isset($group_by_options[$myObject->getTableField()."_natkpa_id"])) {
	$allowed_filters[] = $myObject->getTableField()."_natkpa_id";
}
//		$headingObject->
foreach($allowed_filters as $fld) {
	if(isset($group_by_options[$fld])) {
		$available_filters[$fld] = $group_by_options[$fld];
	}
}

//ASSIST_HELPER::arrPrint($filter_fields_not_allowed);
//ASSIST_HELPER::arrPrint($group_by_options);

$timeObject = new SDBP6_SETUP_TIME();
$time = $timeObject->getTimeForReportFiltering($sdbip_id, $myObject->getTimeType());
$time_keys = array_keys($time);
$start_time = $time_keys[0];
$end_time = $time_keys[count($time_keys) - 1];
$time_options = array();
foreach($time as $i => $t) {
	$time_options[$i] = $t['name'];
}

$resultObject = new SDBP6_SETUP_RESULTS();
$result_settings = $resultObject->getMyActiveResultSettingsForReporting();
//ASSIST_HELPER::arrPrint($result_settings);
?>
<form name="frm_generate" method="post" action="report_graphs_kpi_process.php">
	<input type="hidden" name=object_type value="<?php echo $object_type; ?>" />
	<input type="hidden" name=sdbip_id value="<?php echo $sdbip_id; ?>" />
	<h2>1. Filter Results</h2>
	<table id="tbl_filter" class="graph-tbl">
		<tr>
			<td class="b"><?php echo $myObject->getObjectName("TIME"); ?>:</td>
			<td><?php
				$js .= $displayObject->drawFormField("LIST", array('id' => "start_time_id", 'name' => "start_time_id", 'options' => $time_options, 'req' => true), $start_time);
				echo " to ";
				$js .= $displayObject->drawFormField("LIST", array('id' => "end_time_id", 'name' => "end_time_id", 'options' => $time_options, 'req' => true), $end_time);
				?></td>
		</tr>
		<?php
		foreach($available_filters as $fld => $h) {
			$type = $h['type'];
			$head = $h['name'];
			//$head = $available_filters[$fld];
			?>
			<tr>
				<td class="b"><?php echo(strpos($head, "|") !== false ? $myObject->replaceAllNames($head) : $head); ?>:</td>
				<td><?php
					$display_type = "LIST";
					$listObject = new SDBP6_LIST();
					$listObject->setSDBIPID($sdbip_id);
					$segmentObject = new SDBP6_SEGMENTS();
					$options = array();
					$list = $h['list_table'];
					$required = false;
					switch($type) {
						case "LIST":
						case "MULTILIST":
							$listObject->changeListType($list);
							//SET FOR NATIONAL KPA - might need tweaking for other filters
							$options0 = array('ALL' => "All", 'UNSPECIFIED' => $myObject->getUnspecified()) + $listObject->getActiveListItemsFormattedForSelect();
							foreach($options0 as $key => $item) {
								$options[$key."|".base64_encode($item)] = $item;
							}
							$default_value = "ALL|".base64_encode("All");
							$required = true;
							break;
						case "OBJECT":
						case "MULTIOBJECT":
							if(strpos($list, "|")) {
								$l = explode("|", $list);
								$list = $l[0];
								$options = $l[1];
							} else {
								$options = array();
							}
							$objObject = new $list();
							$objObject->setSDBIPID($sdbip_id);
							if($list == "SDBP6_SETUP_ORGSTRUCTURE") {
								if($object_type == SDBP6_DEPTKPI::OBJECT_TYPE) {
									$options = $objObject->getActiveObjectsFormattedForFiltering();
								} else {
									$options = $objObject->getActiveListItemForGraphReports("TOP");
								}
								$options = array('ALL' => "Entire Organisation") + $options;
								$default_value = "ALL";
							} else {
								$options = $objObject->getActiveObjectsFormattedForSelect();
							}
							unset($objObject);
							break;
						case "SEGMENT":
						case "MULTISEGMENT":
							$segmentObject->setSection($list);
							$options = $segmentObject->getActiveListItemsFormattedForSelect();
							break;
						case "BOOL":
							$display_type = "LIST";
							$options = array('YES' => "Yes", 'NO' => "No");
							break;
					}
					$js .= $displayObject->drawFormField($display_type, array('id' => $fld, 'name' => $fld, 'options' => $options, 'req' => $required), (isset($default_value) ? $default_value : ""));
					unset($default_value);
					?></td>
			</tr>
			<?php
		}
		?>
		<tr>
			<td class="b"><?php echo $object_name." Filter:"; ?></td>
			<td><?php
				$fld = "kpi_notmeasured";
				$options = array("INCLUDE" => "Include", "EXCLUDE" => "Exclude");
				$js .= $displayObject->drawFormField("LIST", array('id' => $fld, 'name' => $fld, 'options' => $options, 'req' => true), "INCLUDE");
				echo $result_settings[0]['value']; ?><br />
				<span style="font-size: 6.5pt" class=i>* <?php echo $object_name; ?> with no targets & actuals prior to the end of the time period selected.</span>
			</td>
		</tr>
		<tr>
			<td class="b"><?php echo $object_names; ?> Met:</td>
			<td><?php
				$fld = "kpi_group";
				$options = array("SEPARATE" => "Separate", "COMBINE" => "Combine");
				$js .= $displayObject->drawFormField("LIST", array('id' => $fld, 'name' => $fld, 'options' => $options, 'req' => true), "SEPARATE");

				for($x = 3; $x <= 5; $x++) {
					echo "&nbsp;<span class=".$result_settings[$x]['style'].">&nbsp;&nbsp;</span>&nbsp;".$result_settings[$x]['value'];
				}
				?></td>
		</tr>
	</table>
	<h2>2. Layout Settings</h2>
	<table id="tbl_layout" class="graph-tbl">
		<tr>
			<td class="b">Include Sub-Graphs?</td>
			<td><?php
				$js .= $displayObject->drawFormField("BOOL", array('id' => "sel_do", 'name' => "do_group", 'small_size' => true, 'extra_act' => "includeSubGraphs"), 1);
				?></td>
		</tr>
		<tr class=tr_group>
			<td class="b">Group By:</td>
			<td><?php
				$options = array();
				foreach($group_by_options as $fld => $h) {
					if(strpos($h['list_table'], "|")) {
						$l = explode("|", $h['list_table']);
						$h['list_table'] = $l[0];
					}
					if($fld == $default_grouping) {
						$default_grouping = $h['type']."|".$h['list_table']."|".$fld;
					}
					if(in_array($h['type'], $field_types_allowed_for_grouping)) {
						$options[$h['type']."|".$h['list_table']."|".$fld] = $h['name'];
					}
				}
				$js .= $displayObject->drawFormField("LIST", array('id' => "group_by", 'name' => "group_by", 'options' => $options, 'req' => true), $default_grouping);
				?></td>
		</tr>
		<tr class=tr_group>
			<td class="b">Graph Type:</td>
			<td><select name=graph_type id=sel_gtype>
					<option selected value=BAR_PERC>Bar graph (% of KPI Status)</option>
					<option value=BAR_REG>Bar graph (Number of KPIs)</option>
					<option value=PIE_ABOVE>Pie graphs</option>
					<!--	<option value=PIE_ABOVE>Individual Pie graphs (Separate primary graph)</option> -->
					<!--	<option value=PIE_INLINE>Individual Pie graphs (Primary graph inline)</option> -->
				</select><!--<span id=spn_gtype><input type=checkbox value=1 name=bar_grid id=chk_grid> <label for=chk_grid> Display grid?</label></span> --></td>
		</tr>
	</table>


	<h2>3. Generate</h2>
	<table id="tbl_generate" class="graph-tbl">
		<tr>
			<td class="b">Report title:</td>
			<td><input type=text name=report_title size=50 value="" /></td>
		</tr>
		<tr>
			<td class="b"></td>
			<td>
				<button id="btn_generate">Generate</button>
				<span class="float"><input type=reset value="Reset" /></span></td>
		</tr>
	</table>
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>

<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		var table_w = 0;
		var th_w = $("#tbl_filter tr:first td:first").width() + 10;
		$(".graph-tbl").each(function () {
			$(this).find("tr:first").find("td:first").width(th_w);
			if ($(this).width() > table_w) {
				table_w = $(this).width();
			}
		});
		table_w += 50;
		$(".graph-tbl").each(function () {
			$(this).width(table_w);
		});
		$("#sel_gtype").change(function () {
			if ($(this).val() == "BAR_PERC" || $(this).val() == "BAR_REG") {
				$("#spn_gtype").show();
			} else {
				$("#spn_gtype").hide();
			}
		});
		$("#sel_gtype").trigger("change");


		$("#btn_generate").button({}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			$("form[name=frm_generate]").submit();
		})
	});

	function includeSubGraphs($btn, act) {
		$(function () {
			if (act == "no") {
				$(".tr_group").hide();
			} else {
				$(".tr_group").show();
			}
		});
	}
</script>