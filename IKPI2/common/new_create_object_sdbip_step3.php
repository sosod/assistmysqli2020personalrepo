<?php
$import_ref = $_REQUEST['import_ref'];
$sdbip_id = $_REQUEST['sdbip_id'];
$is_import_from_sdbip_page = true;
$page_query_string = $_SERVER['QUERY_STRING'];

//skipping inc_new_statuscheck so need to get sdbip_details - assuming to be new
$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();

//use echo variable to output to screen for development purposes - using variable to make it easier to turn off for production
$echo = "";

//get external module settings
$old_modloc = $sdbip_details['alt_module_settings']['modlocation'];
$old_modref = $sdbip_details['alt_module'];
$linked_idp = 0;
$old_dbref = strtolower("assist_".$sdbipObject->getCmpCode()."_".$old_modref);

//create objects
$my_modref = $myObject->getModRef();
$field_prefix = $myObject->getTableField();
$ref_field = $field_prefix."_ref";
$name_field = $myObject->getNameFieldName();
$id_field = $myObject->getIDFieldName();
$status_field = $myObject->getStatusFieldName();
$ref_tag = $myObject->getRefTag();
//$result_prefix = $myObject->getResultsTableField();
//$target_fields = array();
//$description_field = $result_prefix."_target_description";
//if($page_object==SDBP6_TOPKPI::OBJECT_TYPE || $page_object==SDBP6_DEPTKPI::OBJECT_TYPE) {
//	$original_field = $myObject->getResultsOriginalFieldName();
//	$target_fields[] = $original_field;
//}
//$target_fields[] = $description_field;
//$result_object = $myObject->getMyChildObjectType();
$records = $myObject->getImportedRecords();
//$results_records = $myObject->getImportedResultsRecords(array_keys($records));

$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object] = array();

//get mapping
$field_map = $myObject->getFieldMap($old_modloc, $old_modref, $page_object);    //array('old_field'=>"new_field")
$all_headings = $headingObject->getMainObjectHeadings($page_object, "DETAILS", "", "", true);
$all_headings = $all_headings['rows'];
$new_field_settings = $headingObject->getHeadingsForExternalMapping($page_object, false, true);    //array('new_field'=>array('name'=>"",'type'=>"",'list'=>""))
$id_heading = $headingObject->getAHeadingRecordsByField($id_field, $page_object);
//$target_heading = array();
//foreach($target_fields as $fld) {
//	$target_heading[$fld] = $headingObject->getAHeadingRecordsByField($fld,$result_object);
//}
//returns false if field doesn't exist
$second_parent_field = $myObject->getSecondaryParentFieldName();//proj
$third_parent_field = $myObject->getTertiaryParentFieldName();//top
$fixed_headings = array($ref_field, $name_field);
$extra_fields = array();
if($second_parent_field !== false) {
	$extra_fields[] = $second_parent_field;
}
if($third_parent_field !== false) {
	$extra_fields[] = $third_parent_field;
}
//ASSIST_HELPER::arrPrint($all_headings);

//time_periods
//$timeObject = new SDBP6_SETUP_TIME();
//$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id,$myObject->getTimeType());
//ASSIST_HELPER::arrPrint($time_periods);
$lists = array();

$headings = array();
foreach($fixed_headings as $fld) {
	if($fld !== false && isset($all_headings[$fld])) {
		$headings[$fld] = $all_headings[$fld];
	}
}
foreach($extra_fields as $fld) {
	if($fld !== false && isset($all_headings[$fld])) {
		$headings[$fld] = $all_headings[$fld];
	}
}
//ASSIST_HELPER::arrPrint($new_field_settings);
//ASSIST_HELPER::arrPrint($fixed_headings);
//ASSIST_HELPER::arrPrint($field_map);
//ASSIST_HELPER::arrPrint($extra_fields);
foreach($new_field_settings as $fld => $head) { //echo "<P>".$fld." = :".(!in_array($fld,$fixed_headings) && (!in_array($fld,$field_map) || in_array($fld,$extra_fields))).":";
	if(!in_array($fld, $fixed_headings) && (!in_array($fld, $field_map) || in_array($fld, $extra_fields))) {
		$headings[$fld] = $all_headings[$fld];
		$headings[$fld]['list'] = $head['list'];
		if($head['type'] == "LIST" || $head['type'] == "MULTILIST") {
			$listObject = new SDBP6_LIST($head['list']);
			$listObject->setSDBIPID($sdbip_id);
			$lists[$head['list']] = $listObject->getActiveListItemsFormattedForSelect();
			unset($listObject);
		} elseif($head['type'] == "OBJECT" || $head['type'] == "MULTIOBJECT") {
			$list = $head['list'];
			$list_name = $list;
			if(strpos($list, "|") !== false) {
				$lon = explode("|", $list);
				$list = $lon[0];
				$extra_info = $lon[1];
			} else {
				$extra_info = "";
			}
			$listObject = new $list();
			$lists[$list_name] = $listObject->getActiveListItemsFormattedForSelect($extra_info);
			unset($listObject);
		} elseif($head['type'] == "SEGMENT" || $head['type'] == "MULTISEGMENT") {
			$listObject = new SDBP6_SEGMENTS($head['list']);
			$lists[$head['list']] = $listObject->getActiveListItemsFormattedForSelect();
			unset($listObject);
		}
	}
}

//for dev purposes
$c = 0;

ASSIST_HELPER::displayResult(array("info", "Capture the missing information.  Assist will automatically save as you go.<br />All required fields must be captured before this ".$sdbip_object_name." can be confirmed and activated."));
?>
	<table id="tbl_records">
		<thead>
		<tr><?php
			$rowspan = 1;
			echo "<th rowspan='".$rowspan."'>".$id_heading['name']."</th>";
			foreach($headings as $fld => $head) {
				echo "<th  rowspan='".$rowspan."'>".$head['name'].(!in_array($fld, $field_map) && $head['required'] ? "*" : "")."</th>";
			}
			echo "<th rowspan='".$rowspan."'>Results</th>";
			echo "<th rowspan='".$rowspan."'>Status</th>"
			?></tr>
		</thead>
		<tbody>
		<?php
		foreach($records as $row) {
			$c++;
			$i = $row[$id_field];
			$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object][$i] = array('status' => $row[$status_field], 'fields' => array());
			echo "<tr><td class='b' id='td_id_".$i."'>".$ref_tag.$row[$id_field]."</td>";
			foreach($headings as $fld => $head) {
				echo "<td class=".$fld." id='td_".$fld."_".$i."'>";
				if(!in_array($fld, $field_map) || in_array($fld, $extra_fields)) {
					$type = $head['type'];
					if($head['required']) {
						$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object][$i]['fields'][$fld] = array('type' => $type, 'value' => $row[$fld]);
					}
					switch($type) {
						case "DATE":
							$val = $row[$fld] == "0000-00-00" ? "" : date("d-M-Y", strtotime($row[$fld]));
							if($head['required']) {
								$_SESSION[$myObject->getModRef()]['IMPORT'][$page_object][$i]['fields'][$fld]['value'] = $val;
							}
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i, 'options' => array('buttonImage' => false), 'class' => "auto_save"), $val);
							break;
						case "MULTILIST":
						case "MULTISEGMENT":
							$row[$fld] = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $row[$fld]);
						case "LIST":
						case "SEGMENT":
							$js .= $displayObject->drawFormField("LIST", array('id' => $fld."_".$i, 'options' => $lists[$head['list']], 'class' => "auto_save", 'req' => true, 'unspecified' => false, 'alternative_display' => (strpos($type, "SEGMENT") !== false)), $row[$fld]);
							break;
						case "MULTIOBJECT":
							$row[$fld] = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $row[$fld]);
						case "OBJECT":
							$js .= $displayObject->drawFormField("LIST", array('id' => $fld."_".$i, 'options' => $lists[$head['list']], 'class' => "auto_save", 'req' => true, 'unspecified' => false), $row[$fld]);
							break;
						case "BOOL":
						case "BOOL_BUTTON":
							$js .= $displayObject->drawFormField("LIST", array('id' => $fld."_".$i, 'options' => array(1 => "Yes", 0 => "No"), 'class' => "auto_save", 'req' => true, 'unspecified' => false), $row[$fld]);
							break;
						case "TEXT":
						default:
							$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$i, 'class' => "auto_save"), $row[$fld]);
					}
				} else {
					echo $row[$fld];
				}
				echo "</td>";
			}

			echo "<td class='td_results'><button class='btn_result' id='btn_result_".$i."' obj_id='$i'>Edit Results</button> </td>";
			echo "<td class='td_status'>";
			if(($row[$status_field] & SDBP6::CONVERT_IMPORT) == SDBP6::CONVERT_IMPORT) {
				ASSIST_HELPER::displayResult(array("info", "In Progress"));
			} else {
				ASSIST_HELPER::displayResult(array("ok", "Complete"));
			}
			echo "</td></tr>";
			if($c > 5) {
				//break;//kill after 1 line for dev purposes
			}
		}
		?>
		</tbody>
	</table>
<?php
//setup for results
$result_prefix = $myObject->getResultsTableField();
$result_object = $myObject->getMyChildObjectType();
$target_fields = array();
$description_field = $myObject->getResultsDescriptonFieldName();
if($page_object == SDBP6_TOPKPI::OBJECT_TYPE || $page_object == SDBP6_DEPTKPI::OBJECT_TYPE) {
	$original_field = $myObject->getResultsOriginalFieldName();
	$target_fields[] = $original_field;
}
$target_fields[] = $description_field;
//ASSIST_HELPER::arrPrint($target_fields);
$target_heading = array();
foreach($target_fields as $fld) {
	$target_heading[$fld] = $headingObject->getAHeadingRecordsByField($fld, $result_object);
}
//ASSIST_HELPER::arrPrint($target_heading);
//time_periods
$timeObject = new SDBP6_SETUP_TIME();
$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $myObject->getTimeType());

?>
	<div id="dlg_result" title="Edit Results">
		<form name="frm_results">
			<input type="hidden" name="obj_id" id="obj_id" value="0" />
			<h4 id="title_result">OBJECT NAME HERE</h4>
			<table id="tbl_results">
				<tr>
					<th>&nbsp;</th>
					<?php
					$colspan = 1;
					foreach($target_heading as $fld => $head) {
						echo "<th>".$head['name']."</th>";
						$colspan++;
					}
					?>
				</tr>
				<?php
				foreach($time_periods as $time_id => $time) {
					echo "<tr><td class='b'>".$time.":</td>";
					foreach($target_heading as $fld => $head) {
						$type = $head['h_type'];
						echo "<td>";
						switch($type) {
							case "NUM":
								$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$time_id, 'name' => $fld."[".$time_id."]", 'warn' => false, 'field' => $fld, 'time_id' => $time_id), 0);
								break;
							case "TEXT":
							default:
								$js .= $displayObject->drawFormField($type, array('id' => $fld."_".$time_id, 'name' => $fld."[".$time_id."]", 'field' => $fld, 'time_id' => $time_id), "");
								break;
						}
						echo "</td>";
					}
					echo "</tr>";
				}
				echo "<tr><td colspan=".$colspan." class='center'><button id='btn_save_results'>Save</button></td></tr>";
				?>
			</table>
		</form>
	</div>
	<script type="text/javascript">
		$(function () {
			<?php echo $js; ?>

			$(".datepicker").datepicker("option", "onSelect", function (my_val) {
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "DATE", $(this));
			});

			$("input.auto_save").blur(function () {
				if ($(this).hasClass("datepicker")) {
					//console.log($(this).datepicker("getDate"));
				} else {
					var my_val = $(this).val();
					var i = $(this).prop("id");
					saveUpdate(i, my_val, "TEXT", $(this));
				}
			});
			$("select.auto_save").change(function () {
				var my_val = $(this).val();
				if (my_val != "X") {
					var i = $(this).prop("id");
					saveUpdate(i, my_val, "LIST", $(this));
				}
			});
			$("textarea.auto_save").change(function () {
				var my_val = $(this).text();
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "TEXT", $(this));
			});
			$("textarea.result_auto_save").change(function () {
				var my_val = $(this).val();
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "RESULT", $(this));
			});
			$("input.result_auto_save").change(function () {
				var my_val = $(this).val();
				var i = $(this).prop("id");
				saveUpdate(i, my_val, "RESULT", $(this));
			});

			function saveUpdate(i, my_val, typ, $me) {
				var dta = "settings=" + i + "&my_val=" + my_val + "&type=" + typ;
				var r = AssistHelper.doAjax("inc_controller.php?action=<?php echo $page_object.".importPhase3"; ?>", dta);
				if (r[0] == "ok") {
					$("#td_" + i).addClass("ui-state-ok");
					$("#" + i).addClass("ui-state-ok").before(r[1]);
				}
				$me.parent().parent().find("td.td_status").html(r[2]);
			}


			var name_field = "<?php echo $name_field; ?>";
			var w = $("#tbl_results").width() + 50;
			$("#dlg_result").dialog({
				autoOpen: false,
				modal: true,
				width: w
			});
			$("button.btn_result").button().click(function (e) {
				e.preventDefault();
				var i = $(this).attr("obj_id");
				$("#obj_id").val(i);
				var id_ref = $("#td_id_" + i).html();
				var name = $("#td_" + name_field + "_" + i).html();
				$("#title_result").html(id_ref + ": " + name);
				var dta = "obj_id=" + i;
				var d = AssistHelper.doAjax("inc_controller.php?action=<?php echo $page_object; ?>.importGetResult", dta);
				$("form[name=frm_results]").find("input:text, textarea").each(function () {
					var t = $(this).attr("time_id");
					var f = $(this).attr("field");
					var v = AssistString.decode(d[t][f]);
					$("#" + f + "_" + t).val(v);
				});
				$("#dlg_result").dialog("open");
			});
			$("#btn_save_results").button({
				icons: {primary: "ui-icon-disk"}
			}).removeClass("ui-state-default").addClass("ui-button-bold-green").hover(
				function () {
					$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
				}, function () {
					$(this).addClass("ui-button-bold-green").removeClass("ui-button-bold-orange");
				}
			).click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				var dta = AssistForm.serialize($("form[name=frm_results]"));
				var r = AssistHelper.doAjax("inc_controller.php?action=<?php echo $page_object; ?>.importSaveResult", dta);
				AssistHelper.finishedProcessing(r[0], r[1]);
				if (r[0] == "ok") {
					$("#dlg_result").dialog("close");
				}
			});

		});
	</script>
<?php
//ASSIST_HELPER::arrPrint($_SESSION[$myObject->getModRef()]['IMPORT']);
?>