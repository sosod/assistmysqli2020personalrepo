<?php
$is_import_from_sdbip_page = true;
$page_query_string = $_SERVER['QUERY_STRING'];

//skipping inc_new_statuscheck so need to get sdbip_details - assuming to be new
$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();
$sdbip_id = $sdbip_details['id'];
//import_ref
$import_ref = $_REQUEST['import_ref'];

//use echo variable to output to screen for development purposes - using variable to make it easier to turn off for production
$echo = "";

//get external module settings
$old_modloc = $sdbip_details['alt_module_settings']['modlocation'];
$old_modref = $sdbip_details['alt_module'];
$linked_idp = 0;
$old_dbref = strtolower("assist_".$sdbipObject->getCmpCode()."_".$old_modref);

//create objects
$my_modref = $myObject->getModRef();
$field_prefix = $myObject->getTableField();
//start phase 2
$myObject->startExternalImportPhase2($import_ref);

//get all headings to check for status
$all_headings = $headingObject->getMainObjectHeadings($page_object, "DETAILS", "", "", true);
$all_headings = $all_headings['rows'];

//ASSIST_HELPER::arrPrint($all_headings);

//heading settings to process new list items
$new_field_settings = $headingObject->getHeadingsForExternalMapping($page_object, false, true);    //array('new_field'=>array('name'=>"",'type'=>"",'list'=>""))
//get new list items
$added_list_items = array();
$sql = "SELECT * FROM ".$myObject->getDBRef()."_list_import_external_temp WHERE import_ref = '$import_ref'";
$rows = $myObject->mysql_fetch_all($sql);
foreach($rows as $r) {
	$added_list_items[$r['local_list']][$r['id']] = $r;
}

$new_list_items = array();
//add list items to relevant table
foreach($added_list_items as $list => $rows) {
	$listObject = new SDBP6_LIST($list);
	$listObject->setSDBIPID($sdbip_id);
	foreach($rows as $i => $r) {
		$link = $r;
		unset($link['id']);
		unset($link['value']);
		unset($link['import_ref']);
		unset($link['status']);
		unset($link['insertuser']);
		unset($link['insertdate']);
		$name = $r['value'];
		$new_id = $listObject->addListItemFromExternalImport($name, $link);
		$new_list_items[$list][$i] = $new_id;
	}
	unset($listObject);
}


//get insert records
$sql = "SELECT * FROM ".$myObject->getTableName()."_temp WHERE import_ref = '$import_ref'";
$insert_records = $myObject->mysql_fetch_all($sql);

//DISPLAY RESULTS
foreach($insert_records as $record) {
	//prep add item
	$row = $record;
	unset($row['import_id']);
	unset($row['results']);
	unset($row['import_key']);
	unset($row['import_ref']);
	unset($row['import_status']);
	unset($row['import_user']);
	unset($row['import_date']);

	//get list item details
	$extra_data = unserialize($record['results']);
	$record['link_details'] = $extra_data;
	$list_details = $record['link_details']['list_results'];

	//check for required field
	$all_required_fields_populated = true;
	foreach($row as $fld => $rv) {
		//process list item id changes from additions above
		if(isset($list_details[$fld])) {
			$detail = $list_details[$fld];
			$type = $new_field_settings[$fld]['type']; //echo $type;ASSIST_HELPER::arrPrint($detail);
			if(strpos($type, "MULTI") === false) {
				if($detail[0] == "EXISTING") {
					//do nothing - field already contains correct id
				} else {
					$original_id = $detail[1];
					$list = $new_field_settings[$fld]['list'];
					$new_id = isset($new_list_items[$list][$original_id]) ? $new_list_items[$list][$original_id] : 0;
					$row[$fld] = $new_id;
				}
			} else {
				if(count($detail) > 0) {
					$v = array();
					foreach($detail as $d) {
						if($d[0] == "EXISTING") {
							$v[] = $d[1];
						} else {
							$original_id = $d[1];
							$list = $new_field_settings[$fld]['list'];
							$new_id = isset($new_list_items[$list][$original_id]) ? $new_list_items[$list][$original_id] : 0;
							$v[] = $new_id;
						}
					}
					$row[$fld] = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $v);
				} else {
					$row[$fld] = "";
				}
			}
		}

		if(isset($all_headings[$fld]) && $all_headings[$fld]['required'] == true) {
			$head = $all_headings[$fld];
			$type = $head['type'];
			switch($type) {
				case "LIST":
				case "OBJECT":
				case "SEGMENT":
					if((int)$rv == 0) {
						$all_required_fields_populated = false;
					}
					break;
				case "MULTILIST":
				case "MULTIOBJET":
				case "MULTISEGMENT":
					if(strlen($rv) == 0) {
						$all_required_fields_populated = false;
					} else {
						$l = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $rv);
						$l = ASSIST_HELPER::removeBlanksFromArray($l);
						foreach($l as $n => $m) {
							if((int)$m == 0) {
								unset($l[$n]);
							}
						}
						if(count($l) == 0) {
							$all_required_fields_populated = false;
						}
					}
					break;
				case "NUM":
					if(strlen($rv) == 0) {
						$all_required_fields_populated = false;
					}
					break;
				case "DATE":
					if(strlen($rv) == 0 || $rv == "0000-00-00" || strtotime($rv) == 0) {
						$all_required_fields_populated = false;
					}
					break;
				case "BOOL":
				case "TEXT":
				case "MEDVC":
				case "SMLVC":
				case "LRGVC":
				default:
					if(strlen($rv) == 0) {
						$all_required_fields_populated = false;
					}
					break;
			}
		}

	}


	//add object
	$row[$field_prefix.'_src_type'] = SDBP6::OBJECT_SRC_SDBIP;
	$row['import_status'] = SDBP6::EXTERNAL_IMPORT;
	if($all_required_fields_populated) {
		$row['import_status'] += SDBP6::CONVERT_SDBP6ED;
	} else {
		$row['import_status'] += SDBP6::CONVERT_IMPORT;
	}
	$row['import_targets'] = $extra_data['targets'];
	$r = $myObject->addObject($row);
	$id = $r['object_id'];


	//add external link
	$var = array(
		'sdbip_id' => $sdbip_id,
		'local_type' => $page_object,
		'local_id' => $id,
		'src_modref' => $record['link_details']['src_modref'],
		'src_id' => $record['link_details']['src_id'],
		'status' => SDBP6::ACTIVE,
		'insertuser' => $myObject->getUserID(),
		'insertdate' => date("Y-m-d H:i:s")
	);
	$sql = "INSERT INTO ".$myObject->getDBRef()."_import_external_links SET ".$myObject->convertArrayToSQLForSave($var);
	$myObject->db_insert($sql);


}


//end phase 2 - disabled for development
$myObject->endExternalImportPhase2($import_ref);


//notify user
//$to = $myObject->getAnEmail($myObject->getUserID());
$to = "faranath@gmail.com";
if(strlen($to) > 0) {
	$subject = $myObject->getModTitle()." ".$myObject->replaceObjectNames("|".$page_object."|")." Import Phase 2 Complete";
	$message = "Dear ".$myObject->getUserName()."
	
	Phase 2 of the Import from IDP process for ".$myObject->getModTitle()." > ".$myObject->replaceObjectNames("|".$page_object."|")." is complete.  You can now populate any missing information either in Phase 3 or via the Edit function.
	"; //echo $message;
	$emailObject = new ASSIST_EMAIL($to, $subject, $message);
//	$emailObject->sendEmail();
}
?>
	<script type="text/javascript">
		$(function () {
			parent.finishDialog("ok", "Phase 2 completed successfully.");
		});
	</script>
<?php
//ASSIST_HELPER::arrPrint($sdbip_details);

?>