<?php
/** @var INT $mscoa_version_id - src inc_header.php */
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "STEP0";
$max_rows_to_import = 500;

$result_object_type = $myObject->getMyChildObjectType();

$headings = $headingObject->getMainObjectHeadings($object_type, "IMPORT", "NEW", "", true);
$results_headings = $headingObject->getMainObjectHeadings($result_object_type, "IMPORT", "NEW", "", true);
$timeObject = new SDBP6_SETUP_TIME();
$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, $time_setting);


switch($action) {
	case "STEP3":
		//import file for review
		include($step3_url);
		break;
	case "STEP4":
		//accept import
		include($step4_url);
		break;
	case "STEP0":
	default:
		//display step0 import page


		$menu_text0 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route1), "|"));
		$menu_text2 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route2), "|"));
		ASSIST_HELPER::displayResult(array("info", "Note: To edit any $mys_object_name created on this page, before this $sdbip_object_name is ".$sdbipObject->getActivityName("activated").", please go to ".$menu_text0.".  Once the $sdbip_object_name has been ".$sdbipObject->getActivityName("activated").", $mys_object_name can be edited at $menu_text2."));

		?>
		<h2>Import Process</h2>
		<form name=frm_import method=post action=<?php echo $my_url; ?> enctype="multipart/form-data">
			<input type=hidden name=action value='STEP3' />
			<table width=500px class=form>
				<tr>
					<th width=50px>Step 1:</th>
					<td>Generate template (if not done already).
						<span class=float><button id=btn_step0_generate class=btn-step0><?php echo $sdbipObject->replaceActivityNames("|generate|"); ?></button></span>
						<?php
						if($myObject->getMyObjectType() == SDBP6_DEPTKPI::OBJECT_TYPE) {
							echo "<br />&nbsp;&nbsp;&nbsp;<input type='checkbox' value='INCL' name='incl_top_kpi' id='incl_top_kpi' />&nbsp;Pre-populate with ".$myObject->replaceObjectNames("|".SDBP6_TOPKPI::OBJECT_TYPE."|")." details?";
						} else {
							echo "<input type='hidden' value='NO' name='incl_top_kpi' id='incl_top_kpi' />";
						}
						?>
					</td>
				</tr>
				<tr>
					<th>Step 2:</th>
					<td>Populate the template.</td>
				</tr>
				<tr>
					<th>Step 3:</th>
					<td>Import the template.<br />&nbsp;
						<span class=float><input type=file name=import_file value='' /> <button id=btn_step0_import class=btn-step0><?php echo $sdbipObject->replaceActivityNames("|import|"); ?></button></span></td>
				</tr>
				<tr>
					<th>Step 4:</th>
					<td>Finalise and Accept the Import. The <?php echo $mys_object_name; ?> will not be imported until the "Accept" button is clicked.</td>
				</tr>

			</table>
		</form>
		<h2>Guidelines on using the template</h2>
		<ul>
			<li class=red>WARNING: It is advisable NOT to make any changes to the Headings via <?php echo $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_setup_defaults_headings"), "|")); ?> between generating the template in Step 1 and finalising the import in Step 3. Changes to the Headings can change the structure of the template which will cause the process to fail.</li>
			<li>The template is in comma-separated values (CSV) format. If using Windows, please ensure that your computer's Regional Settings (also known as Region) has the following settings:
				<ul>
					<li>Decimal: . (period / full stop)</li>
					<li>List separator: , (comma)</li>
					<li>Thousand separator: " " (space)</li>
				</ul>
			</li>
			</li>
			<li>The columns of the template must be in the order given below.</li>
			<li>The first 2 rows will be ignored (assumed to be heading rows).</li>
			<li>It is advisable to import no more than 500 rows at a time, to reduce the risk of the process taking too long and logging you out before it is complete.</li>
			<li>Valid list items can be found <a href=#lists>here</a>.</li>
		</ul>
		<h3>Template Columns</h3>
		<?php

		$lists = array();

		$str = "A";
		?>
		<table class=form>
			<?php
			foreach($headings['rows'] as $fld => $head) {
				if($headingObject->isListField($head['type'])) {
					if(!isset($lists[$head['list_table']])) {
						$lists[$head['list_table']] = $head;
					} else {
						$lists[$head['list_table']]['name'] .= " & ".$head['name'];
					}
				}
				$help = $myObject->replaceAllNames($head['help'])."  ";
				if($head['required'] == true) {
					$help .= "<span class=red>This field is required.</span>";
				}
				echo "
	<tr>
		<th width=80px>Column ".$str.":</th>
		<td class=b>".$head['name']."</td>
		<td>".$help."</td>
	</tr>
	";
				$str++;
			}
			foreach($time_periods as $ti => $tname) {
				$help = "Numbers only, unformatted and without target types i.e. '1123456.78' not '1,123,456.78' or '78.5' not '78.5%' ";
				echo "
	<tr>
		<th width=80px>Column ".$str.":</th>
		<td class=b>".$tname."</td>
		<td>".$help."</td>
	</tr>
	";
				$str++;
			}

			?>
		</table>
		<h3 id=lists>List Items</h3>
		<?php

		foreach($lists as $table => $list) {
			$type = $list['type'];
			$lot = $table;
			$list_name = $list['name'];
			$button_name = $myObject->getActivityName("open");
			$export_button_name = $myObject->getActivityName("export");
			echo "<div style='width:350px;' id=div_".$lot." class=div_segment list_name='".addslashes($list_name)."' list_type=".$type." list_table=".$lot." my_btn=btn_".$lot."_open>
		<h3 class=object_title>".$list_name."</h3>
		<p class=right><button class=btn_open id=btn_".$lot."_open>".$button_name."</button>&nbsp;<button class=btn_export id=btn_".$lot."_export>".$export_button_name."</button></p>
	</div>";
		}


		?>
		<div id=div_lists>
			<iframe id=ifr_lists></iframe>
		</div>
		<script type="text/javascript">
			$(function () {
				<?php echo $js; ?>
//STEP 0 CODE
				$(".btn-step0").button()
					.removeClass("ui-state-default")
					.addClass("ui-button-bold-grey")
					.hover(
						function () {
							$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
						},
						function () {
							$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey");
						}
					);
				$("#btn_step0_import").button({
					icons: {primary: "ui-icon-arrowreturnthick-1-n"}
				}).click(function (e) {
					e.preventDefault();
					AssistHelper.processing();
					$("form[name=frm_import]").submit();
				});
				$("#btn_step0_generate").button({
					icons: {primary: "ui-icon-arrowreturnthick-1-s"}
				}).click(function (e) {
					e.preventDefault();
					<?php
					if($myObject->getMyObjectType() == SDBP6_DEPTKPI::OBJECT_TYPE) {
						echo "
			if($('#incl_top_kpi').is(':checked')) {
				var my_top = 'YES';
			} else {
				var my_top = 'NO';
			}
			";
					} else {
						echo "var my_top = 'NO';";
					}
					?>
					document.location.href = "<?php echo $generate_url; ?>?incl_top=" + my_top;

				});


//list section code
				var scr_dimensions = AssistHelper.getWindowSize();
				var div_h = 0;
				var tbl_h = 0;
				var dlg_width = 0;
				var dlg_height = 0;
				if (scr_dimensions.width > 700) {
					dlg_width = 700;
				} else {
					dlg_width = scr_dimensions.width * 0.75;
				}
				if (scr_dimensions.height > 700) {
					dlg_height = 700;
				} else {
					dlg_height = scr_dimensions.height * 0.75;
				}
				$("#div_lists").dialog({
					modal: true,
					autoOpen: false,
					width: dlg_width,
					height: dlg_height,
					close: function () {
						$("#ifr_lists").prop("src", "");
					}
				});
				$("div.div_segment").button().click(function (e) {
					e.preventDefault();
					AssistHelper.processing();
					//$("#ifr_lists").html("Loading...");
					var list_type = $(this).attr("list_type");
					var table = $(this).attr("list_table");
					var name = $(this).attr("list_name");
					var url = '<?php echo $list_view_page."?mscoa_version=".$mscoa_version_id."&".(isset($sdbip_id) ? "sdbip_id=".$sdbip_id."&" : ""); ?>t=' + table + '&n=' + name + '&y=' + list_type;
					$("#div_lists").dialog("option", "title", name);
					$("#ifr_lists").prop("width", dlg_width - 35).prop("height", dlg_height - 50).prop("src", url).css("border", "1px solid #FFFFFF");
					window.setTimeout(openDialog, 3000);
				});
				$("div.div_segment").hover(
					function () {
						$me = $(this).find("#btn_" + $(this).attr("list_table") + "_open");
						$me.addClass("ui-button-state-orange").removeClass("ui-button-state-blue");
						$(this).find("h3").addClass("orange");
						$(this).addClass("orange-border").removeClass("light-blue-border");
					},
					function () {
						$me = $(this).find("#btn_" + $(this).attr("list_table") + "_open");
						$me.removeClass("ui-button-state-orange").addClass("ui-button-state-blue");
						$(this).find("h3").removeClass("orange");
						$(this).removeClass("orange-border").addClass("light-blue-border");
					}
				).css({"margin": "10px", "background": "url()"});//,"border-color":"#0099FF"});
				$("div.div_segment").find("h3").css("font-family", "Tahoma");

				$("button.btn_open").button({
					icons: {primary: "ui-icon-newwin"}
				}).removeClass("ui-state-default").addClass("ui-button-state-blue")
					.hover(
						function () {
							$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-blue");
							$me = $(this).closest("div");
							$me.find("h3").addClass("orange");
							$me.addClass("orange-border").removeClass("light-blue-border");
						},
						function () {
							$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-blue");
							$me = $(this).closest("div");
							$me.find("h3").removeClass("orange");
							$me.removeClass("orange-border").addClass("light-blue-border");
						}
					);
				$("button.btn_open").click(function (e) {
					e.preventDefault();
					$(this).parent("div.div_segment").trigger("click");
				});
				$("button.btn_export").button({
					icons: {primary: "ui-icon-arrowreturnthick-1-s"}
				}).removeClass("ui-state-default").addClass("ui-button-state-grey")
					.hover(
						function () {
							$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-grey");
							$me = $(this).closest("div");
							$me.find("h3").addClass("orange");
							$me.addClass("orange-border").removeClass("light-blue-border");
						},
						function () {
							$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
							$me = $(this).closest("div");
							$me.find("h3").removeClass("orange");
							$me.removeClass("orange-border").addClass("light-blue-border");
						}
					).click(function (e) {
					e.preventDefault();
					e.stopPropagation();	//don't trigger the parent div click event
					AssistHelper.processing();
					var list_type = $(this).closest("div").attr("list_type");
					var table = $(this).closest("div").attr("list_table");
					var name = $(this).closest("div").attr("list_name");
					var url = '<?php echo $list_view_page; ?>?mscoa_version=<?php echo $mscoa_version_id; ?>&<?php echo isset($sdbip_id) ? "sdbip_id=".$sdbip_id."&" : ""; ?>page=export&t=' + table + '&n=' + name + '&y=' + list_type;
					document.location.href = url;
					AssistHelper.closeProcessing();
				});

				function formatButtons() {
					$("button.xbutton").children(".ui-button-text").css({"font-size": "80%", "padding": "4px", "padding-left": "24px"});
					$("button.xbutton").css({"margin": "2px"});
				}

				formatButtons();

				$("#tbl_container, #tbl_container td").css("border", "1px solid #ffffff");
				$(".div_segment").addClass("light-blue-border")//.css("border","1px solid #9999ff");


			});

			function openDialog() {
				$(function () {
					$("#div_lists").dialog("open");
					AssistHelper.closeProcessing();
				});
			}

		</script>


	<?php


} //end switch on which page to display


?>