<?php
//ASSIST_HELPER::varPrint(get_defined_vars());
/** @var SDBP6_DISPLAY $displayObject */
/* SET VARIABLES */
//$myObject = new SDBP6_? //set in calling page
//$page_section = NEW / MANAGE / ADMIN //set in calling page
//$sdbip_id = ? //set in header
$object_type = $myObject->getMyObjectType();
$object_id = $_REQUEST['object_id'];
$time_id = $_REQUEST['time_id'];
$page_src = $_REQUEST['page_src']; //Where is the page being called from?  Pop-up/dialog / normal page / front page dashboard - dictates formatting and post-update action

$activity_name = $myObject->getActivityName($page_action);
$object_name = $myObject->getObjectName($object_type);
$reftag = $myObject->getRefTag();
$time_type = $myObject->getTimeType();
$form_object = $myObject->getRawObject($object_id);
$targettype_id = $form_object[$myObject->getTargetTypeTableField()];
$targetTypeObject = new SDBP6_SETUP_TARGETTYPE("", $targettype_id);

if($page_src == "dialog") {
	$page_redirect_path = "dialog";
} elseif($page_src == "dashboard") {
	$page_redirect_path = "dashboard";
} else {
	//use page_redirect_path set in calling page
}

/* Get details to draw page */
//Get object details (needed by Current Results & Review Form)
$object_details = $myObject->getRawObject($object_id);//array('results'=>$myObject->getRawResults($object_id));
//Get Time Period information
$timeObject = new SDBP6_SETUP_TIME();
$time_name = $timeObject->getATimePeriodName($sdbip_id, $time_id, $time_type);
//Get object summary information
$summary_form = $displayObject->getObjectForm($object_type, $myObject, $object_id, null, null, 0, "SUMMARY.VIEW", $page_redirect_path, true, 0, $form_object);
//Get update form information
$update_form = $displayObject->getUpdateForm("VIEW", $object_type, $myObject, $sdbip_id, $object_id, $time_id, $targettype_id, $page_redirect_path);
//Get update form information
$review_form = $displayObject->getReviewForm($page_action, $object_type, $myObject, $sdbip_id, $object_id, $time_id, $object_details, $page_redirect_path);
//get Complete results table (same as View page)
//if($page_src!="dialog") {
$headingObject = new SDBP6_HEADINGS();
$child_headings = $headingObject->getMainObjectHeadings($myObject->getMyChildObjectType(), "DETAILS", $page_section);
$time_periods = array();//getChildObjectForm will populate this list if empty array is sent
$current_results = $displayObject->getChildObjectForm("VIEW", $object_details, $child_headings, $time_periods, $sdbip_id, "frm_update_".$object_type."_current_results", $time_type, $page_section, $targetTypeObject->getSymbolsForFormatting(), $myObject);
//} else {
//	$current_results = array('display'=>"",'js'=>"");
//}
//get approve history
$approve_history = $myObject->getApproveHistory($object_id, $time_id);
//get assurance history
$assurance_history = $myObject->getAssuranceHistory($object_id, $time_id);

?>

<div id=update_form_div>
	<h2><?php echo $activity_name." ".$object_name." ".$reftag.$object_id." for ".$time_name; ?></h2>
	<div id=review_form_div class="update-div form-div">
		<h3><?php echo $myObject->getActivityName($page_action); ?> form</h3>
		<?php
		echo $review_form['display'];
		$js .= $review_form['js'];
		ASSIST_HELPER::displayResult(array("info", "Please note: <ul class=black><li>The 'Notify Me' setting will only apply until another review of this ".$object_name." is performed, regardless of who performs that review.</li><li>Additional Notification recipients will only receive an email if this update is rejected.  The email will be sent regardless of whether you select to return the update to a user for changes or not.</li></ul>"));
		?>
	</div>
	<div id=div_update_form class=update-div>
		<h3><?php echo $myObject->getActivityName("UPDATE"); ?> form</h3>
		<?php
		echo $update_form['display'];
		$js .= $update_form['js'];
		?>
	</div>

	<div id=div_object_summary class="update-div form-div">
		<h3>Summary Details</h3>
		<?php
		echo $summary_form['display'];
		$js .= $summary_form['js'];

		$js .= $displayObject->drawPageFooter($myObject->getGoBack());

		?>
	</div>

	<div id=div_full_results class="update-div results-div" style='clear:both'>
		<h2>Current Results</h2>
		<?php
		echo $current_results['display'];
		//echo str_replace(chr(10),"<br />",$current_results['js']);
		$js .= $current_results['js'];


		$js .= $displayObject->drawPageFooter($myObject->getGoBack(), $myObject->getMyLogTable(), array("object_id" => $object_id));

		?>
	</div>

	<div id=div_approve_history class="update-div history-div left-div">
		<h2><?php echo $myObject->getActivityName("APPROVE"); ?> Review History</h2>
		<table id=tbl_review_history>
			<tr><?php
				foreach($approve_history['head'] as $fld => $name) {
					echo "<th>".$name."</th>";
				}
				?></tr>
			<?php
			if(count($approve_history['rows']) > 0) {
				foreach($approve_history['rows'] as $i => $row) {
					echo "<tr>";
					foreach($approve_history['head'] as $fld => $name) {
						$val = $row[$fld];
						echo "<td>".$val."</td>";
					}
					echo "</tr>";
				}
			} else {
				echo "<tr><td colspan=".count($approve_history['head']).">No reviews available</td></tr>";
			}
			?>
		</table>
	</div>

	<div id=div_assurance_history class="update-div history-div right-div">
		<h2><?php echo $myObject->getActivityName("ASSURANCE"); ?> Review History</h2>
		<table id=tbl_assurance_history>
			<tr><?php
				foreach($assurance_history['head'] as $fld => $name) {
					echo "<th>".$name."</th>";
				}
				?></tr>
			<?php
			if(count($assurance_history['rows']) > 0) {
				foreach($assurance_history['rows'] as $i => $row) {
					echo "<tr>";
					foreach($assurance_history['head'] as $fld => $name) {
						$val = $row[$fld];
						echo "<td>".$val."</td>";
					}
					echo "</tr>";
				}
			} else {
				echo "<tr><td colspan=".count($assurance_history['head']).">No reviews available</td></tr>";
			}
			?>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		/* DEV CODE */
		//$("#update_form_div").css({"border":"1px dashed #ffffff","padding":"3px"});
		//code needed after development to keep floated divs inline with partners
		$("#update_form_div").find("div.update-div").css({
			'border': '1px solid #ffffff',
			'background-color': '#ffffff'
		});


		/* FORM CODE */
		<?php echo $js; ?>




		/* PAGE CODE */

		$(window).resize(function () {
			var windowSize = AssistHelper.getWindowSize();
			var window_width = windowSize['width'];
			//1150px tested as smallest width capable of managing approve form & update form side-by-side
			if (window_width < 1150) {
				$("div.update-div").width("");
				var $review_form = $("#review_form_div");
				$("#div_update_form").after($review_form);
				$review_form.removeClass("float");
			} else {
				var $review_form = $("#review_form_div");
				$("#div_update_form").before($review_form);
				$review_form.addClass("float");
				var half_width = (window_width - 50) * 0.5;
				$("div.update-div").find("table:first").each(function () {
					var w = $(this).width() + 30;
					if ($(this).parents("div:first").hasClass("results-div")) {
						//results-div = full size so do nothing
					} else {
						$(this).parents("div:first").width(half_width);
						//$(this).parents("div:first").width(w).css("padding-left","10px").css("padding-bottom","10px");
					}
				});
			}
		});
		$(window).trigger("resize");

//MOVE THE HISTORY TABLE INTO THE FORM DIV
		<?php
		if($page_action == "APPROVE") {
		?>
		$("#div_approve_history").width($("#div_approve_history").width() - 10).appendTo($("div.form-div:first"));
		$("#div_assurance_history").width($("#div_assurance_history").width() - 10).appendTo($("div.form-div:first"));
		<?php
		} else {
		?>
		$("#div_assurance_history").width($("#div_assurance_history").width() - 10).appendTo($("div.form-div:first"));
		$("#div_approve_history").width($("#div_approve_history").width() - 10).appendTo($("div.form-div:first"));
		<?php
		}
		?>

		<?php
		if($page_src == "dialog") {
		?>
		window.parent.$('#dlg_update').dialog('open');
		<?php
		}
		?>

	});

	function saveFinished(icon, message) {
		//alert(message);
		if (icon == "ok") {
			AssistHelper.finishedProcessingWithRedirect(icon, message, "<?php echo $page_redirect_path; ?>");
		} else {
			AssistHelper.finishedProcessing(icon, message);
		}
	}


</script>


<?php

//ASSIST_HELPER::arrPrint($_REQUEST);
//echo $myObject->getMyObjectType();

?>
