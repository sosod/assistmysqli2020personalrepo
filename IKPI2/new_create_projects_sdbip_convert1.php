<?php
//JC NOTE: doing this old school to handle multiple modrefs at once and to assist with displaying progress onscreen
$no_page_heading = true;
require_once("inc_header.php");
$is_import_from_sdbip_page = true;
$page_object = SDBP6_PROJECT::OBJECT_TYPE;
$page_link = "new_create_projects";
$page_query_string = $_SERVER['QUERY_STRING'];
$page_action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "SAVE";

echo "
<h1>Conversion Started</h1><div width=50% stle='margin:0 auto'>";

ASSIST_HELPER::displayResult(array("info", "This process can take some time and most of the activity takes place behind the scenes.  If your browser indicates that the page has become unresponsive, please choose to Wait rather than closing the page."));

echo "</div><ul><li>Getting this module's $sdbip_object_name details...
";

//skipping inc_new_statuscheck so need to get sdbip_details - assuming to be new
$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();

echo "<span class=green>done;</span></li>";
echo "
<li>Getting old module's settings... ";

$old_modloc = $sdbip_details['alt_module_settings']['modlocation'];
$old_modref = $sdbip_details['alt_module'];
$old_dbref = strtolower("assist_".$sdbipObject->getCmpCode()."_".$old_modref);


$myObject = new SDBP6_PROJECT();
$my_modref = $myObject->getModRef();
$external_class = "SDBP6_".$old_modloc;
$extObject = new $external_class($old_modref, $page_object, $myObject->getCmpCode());

//get table
$alt_table = $myObject->getAltModuleTable($old_modloc);
if($alt_table === false) {
	die("ERROR!  Couldn't identify the alternative module's Project table.");
}
//get id field
$alt_id_field = $myObject->getAltModuleIDField($old_modloc);
if($alt_id_field === false) {
	die("ERROR!  Couldn't identify the alternative module's Project System Ref field.");
}

echo "<span class=green>done;</span></li>";

echo "
<li>Get field map & settings... ";

//get field map
$field_map = $myObject->getFieldMap($old_modloc, $old_modref, $page_object);    //array('old_field'=>"new_field")
$new_field_settings = $headingObject->getHeadingsForExternalMapping($page_object, false, true);    //array('new_field'=>array('name'=>"",'type'=>"",'list'=>""))
$old_field_settings = $extObject->getHeadingsForMapping($field_map, false, true);
echo "<span class=green>done;</span></li>";


//get existing list items
echo "
<li>Get list item mapping... ";
$list_map = $myObject->getListItemMapping();
$old_lists = $extObject->getListItems($field_map, $old_field_settings);
$new_lists = $myObject->getListItemsForExternalMapping($field_map, $new_field_settings);

echo "<span class=green>done;</span></li>";
//check for missing items
//can't be done here as needs to loop through values in case converting TEXT or DATE field to LIST - JC 2018-11-02


/*echo "
<li>Setting up for the conversion;</li>";

$myObject->setExternalMappingVariables($extObject,$field_map,$old_field_settings,$new_field_settings,$list_map,$old_lists,$new_lists);
/*
echo "<hr />Field map";
ASSIST_HELPER::arrPrint($field_map);
echo "<hr />New field settings";
ASSIST_HELPER::arrPrint($new_field_settings);
echo "<hr />old field settings";
ASSIST_HELPER::arrPrint($old_field_settings);
echo "<hr />list map";
ASSIST_HELPER::arrPrint($list_map);
echo "<hr />list OLD";
ASSIST_HELPER::arrPrint($old_lists);
echo "<hr />list NEW";
ASSIST_HELPER::arrPrint($new_lists);

*/


echo "
<li>Get number of objects to be converted... ";

$sql = "SELECT count(".$alt_id_field.") as c FROM ".$old_dbref."_".$alt_table;
$object_count = $myObject->mysql_fetch_one_value($sql, "c");
//$object_count=0;
if($object_count > 0) {

	echo "
	<span class=green>Found $object_count Project(s) to be converted;</span></li>";


	$max_objects_to_be_processed_at_once = 10;
	$processed_objects = 1;

	while($processed_objects < $object_count) {
		$next_limit = ($processed_objects + $max_objects_to_be_processed_at_once) < $object_count ? ($processed_objects - 1 + $max_objects_to_be_processed_at_once) : $object_count;
		echo "
		<li>Processing Project records ".($processed_objects)." to ".($next_limit).":<ol>";
		//sql - get records
		switch($old_modloc) {
			case "SDBP5B":
				echo "
				<li>Getting old records;</li>";
				//$records= array(1=>array());
				$records = $myObject->getOldRecordsSDBP5B($old_dbref, $alt_table, $processed_objects - 1, $max_objects_to_be_processed_at_once);
				//process records
				foreach($records as $project_id => $project_data) {
					$processed_objects++;
//DEV CODE TO SPEED UP PROCESS
//if($processed_objects==3) { $processed_objects = $object_count; }
					echo "
					<li>Processing Project P".$project_id.";</li>";
					$new_record = array(
						'link_details' => array(
							'sdbip_id' => $sdbip_details['id'],
							'src_type' => $sdbip_details['alt_action'],
							'proj_id' => 0,
							'src_modloc' => $old_modloc,
							'src_modref' => $old_modref,
							'src_id' => $project_id,
						)
					);
					foreach($field_map as $old_field => $new_field) {
						if($new_field != "0" && $new_field != false && isset($new_field_settings[$new_field]['type'])) {
							$old_setting = $old_field_settings[$old_field];
							$old_list_table = $old_setting['list'];
							$new_setting = $new_field_settings[$new_field];
							$new_list_table = $new_setting['list'];
							$original_value = $project_data[$old_field];
							if($old_setting['type'] == "DATE") {    //assume date has been converted to timestamp
								if($new_setting['type'] == "DATE") {
									if($original_value * 1 == 0) {
										$new_value = "0000-00-00";
									} else {
										$new_value = date("Y-m-d", $original_value);
									}
								} elseif($new_setting[$new_field] == "LIST") {
									//if no date provided, assume Unspecified
									if($original_value * 1 == 0) {
										$new_value = 0;
									} else {
										//else create comparable text to check if list item exists - NO LIST MAP AS THIS IS NOT CCOMING FROM A LIST FIELD
										$list_value = date("d F Y", $original_value);
										$compare = $myObject->formatTextForComparison($list_value);
										//if comparable list item exists then use that id
										if(isset($new_lists[$new_list_table][$compare])) {
											$new_value = $new_lists[$new_list_table][$compare];
										} else {
											//else make a new list item
											$new_value = "MAKE NEW LIST ITEM FOR $original_value ";
										}
									}
								} else {
									if($original_value * 1 == 0) {
										$new_value = "";
									} else {
										$new_value = date("d F Y", $original_value);
									}
								}
								//} elseif($old_setting['type']=="AREA") {
//same as list BUT MULTI
								//	$new_value = "AREA";
								//} elseif($old_setting['type']=="WARDS") {
								//	$new_value = "WARDS";
//same as LIST but MULTI
							} elseif($old_setting['type'] == "LIST" || $old_setting['type'] == "AREA" || $old_setting['type'] == "WARDS") {
								//convert to array if not already in array - for standardisation
								if(!is_array($original_value)) {
									$old_value = array($original_value);
								} else {
									$old_value = $original_value;
								}
								$new_value = array();
								//if new_setting type = "LIST" || "OBJECT" || "SEGMENT"
								$list_types = array("LIST", "SEGMENT", "OBJECT");
								$multi_list_types = array("MULTILIST", "MULTISEGMENT", "MULTIOBJECT");
								if(in_array($new_setting['type'], $list_types) || in_array($new_setting['type'], $multi_list_types)) {
									foreach($old_value as $original_value) {
										//check that previous unspecified has not been picked up
										if(in_array(0, $new_value)) {
											//do nothing
											//check if item is not unspecified
										} elseif($original_value * 1 == 0) {
											//set all results to unspecified - ignore any previous values
											$new_value = array(0);
										} else {
											//check if list item exists in list_map
											//if yes, set new_value = list_map value
											if(isset($list_map[$new_list_table][$old_list_table][$original_value])) {
												$new_value[] = $list_map[$new_list_table][$old_list_table][$original_value];
												//else - list map element not exist
											} elseif(!isset($old_lists[$old_list_table][$original_value])) {
												//give error code if item not found in old lists
												$new_value = array(-1);
											} else {
												//get comparison text from old_lists
												$test_value = $old_lists[$old_list_table][$original_value]['processed']; //echo "<p class=b>".$test_value."</p>";
												//echo "<p>".$new_list_table."</p>";
												//ASSIST_HELPER::ArrPrint($new_lists[$new_list_table]);
												//check if comparison text exists in new_lists
												if(isset($new_lists[$new_list_table][$test_value])) {
													//if yes
													//get new_lists id
													$new = $new_lists[$new_list_table][$test_value];
													//create new list_map record
													//echo "<h2 class=red>CREATE NEW LIST MAP RECORD FOR $original_value TO $new_value </h2>";
													$myObject->createNewListItemMapping($old_modref, $new_list_table, $old_list_table, $original_value, $new);
													$list_map[$new_list_table][$old_list_table][$original_value] = $new;
													$new_value[] = $new;
												} else {
													//else
													//get original text from old_lists
													$original_details = $old_lists[$old_list_table][$original_value];
													if($new_setting['type'] == "SEGMENT" || $new_setting['type'] == "MULTISEGMENT") {
														echo "<p class=red>No match in Library for ".$original_details['display']."</p>";
														$new_value = array(-1);
													} elseif($new_setting['type'] == "OBJECT" && stripos($new_list_table, "ORGSTRUCTURE")) {
														echo "<p class=red>No match Organisational Structure for ".$original_details['display']."</p>";
														$new_value = array(-1);
													} else {
														//create object
														switch($new_setting['type']) {
															case "LIST":
															case "MULTILIST":
																$listObject = new SDBP6_LIST($new_list_table);
																$listObject->setSDBIPID($sdbip_id);
																break;
															case "OBJECT":
															case "MULTIOBJECT":
																if(strpos($new_list_table, "|") !== false) {
																	$lon = explode("|", $new_list_table);
																	$list = $lon[0];
																	$extra_info = $lon[1];
																} else {
																	$extra_info = "";
																}
																$listObject = new $list($my_modref);
																break;
														}
														//add new list item record
														$new = $listObject->addListItemFromExternalConversion($original_details);
														if($new > 0) {
															//only update if a successful id has been returned
															//update local lists
															$new_lists[$new_list_table][$original_details['processed']] = $new;
															//create new list_map record
															//echo "<h2 class=green>MAKE NEW LIST ITEM FOR $original_value ".$old_lists[$old_list_table][$original_value]['display']."</h2>";
															//echo "<h2 class=red>CREATE NEW LIST MAP RECORD FOR $original_value TO $new_value </h2>";
															$myObject->createNewListItemMapping($old_modref, $new_list_table, $old_list_table, $original_value, $new);
															$list_map[$new_list_table][$old_list_table][$original_value] = $new;
															$new_value[] = $new;
														} else {
															echo "<p class=red>Error when trying to create new list item ".$original['details']['display']."</p>";
														}
													}
												}
											}
										}
									}
									if(in_array($new_setting['type'], $multi_list_types)) {
										//leave new_value as array
									} else {
										//otherwise take first list item
										if(is_array($new_value)) {
											if(count($new_value) > 0) {
												$new_value = $new_value[0];
											} else {
												$new_value = 0;
											}
										}
									}
									//else - not going into list field
								} else { //assume text field
									foreach($old_value as $original_value) {
										//get original text from old_lists
										if($original_value * 1 == 0) {
											//$new_value = "";
										} elseif(!isset($old_lists[$old_list_table][$original_value]['display'])) {
											//$new_value = "";
										} else {
											$new_value[] = $old_lists[$old_list_table][$original_value]['display'];
										}
									}
									$new_value = implode("; ", $new_value);
								}
							} else {
								$new_value = $original_value;
							}
							$new_record[$new_field] = $new_value;
						}
					}
					//save to db
					if($page_action != "TEST") {
						$id = $myObject->addObjectFromExternalConversion($new_record);
						//ASSIST_HELPER::ArrPrint($new_record);
						echo "<p class=green>".$myObject->getObjectName($myObject->getMyObjectType())." successfully converted to ".$myObject->getRefTag().$id."</p>";
					}
				}
				break;
		}
		echo "</ol></li>";

	}

} else {

	echo "
	<li>No Projects found to be converted. Conversion will now finish;</li>";


}


echo "
</ul><h1 class=green>Conversion ".($page_action == "TEST" ? "Test" : "")." Finished!</h1>

";
if($page_action != "TEST") {
	echo "
<p><button id=btn_convert_step3>Next Step</button></p>";

} else {

	echo "
<p><button id=btn_close>Close Test Window</button></p>";

}

echo "

<script type=text/javascript>
//stop autoscrolling but wait an extra second to get to the bottom of the page
setTimeout(window.parent.stopScrolling(),2000);

$(function() {
	$('#btn_convert_step3').button({
		icons:{'secondary':'ui-icon-arrowthick-1-e'}
	}).removeClass('ui-state-default').addClass('ui-button-bold-green')
	.hover(function() {
			$(this).addClass('ui-button-bold-orange').removeClass('ui-button-bold-green');
		},function() {
			$(this).addClass('ui-button-bold-green').removeClass('ui-button-bold-orange');
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		window.parent.document.location.href = '".$page_link."_sdbip.php?".$page_query_string."';
	});

	$('#btn_close').button({
		icons:{'primary':'ui-icon-closethick'}
	}).removeClass('ui-state-default').addClass('ui-button-bold-green')
	.hover(function() {
			$(this).addClass('ui-button-bold-orange').removeClass('ui-button-bold-green');
		},function() {
			$(this).addClass('ui-button-bold-green').removeClass('ui-button-bold-orange');
	}).click(function(e) {
		e.preventDefault();
		window.parent.closeDialog();
	});

});


</script>
";
//ASSIST_HELPER::arrPrint($sdbip_details);

?>