<?php
require_once("inc_header.php");


$object_id = 0;//$_REQUEST['object_id']; - no object_id available because we're creating a new one!
$myObject = new SDBP6_DEPTKPI();

$page_section = "MANAGE";
$page_action = "CREATE";

$page_redirect_path = "manage_create_dept.php?";

//Need filter by to check which sub-directorates the user has access to
$filter_by = $myObject->getFilterByOptions($page_section, $page_action);

//FOR DEPTKPI - remove links to DIR - DEPTKPIs can only be added to LOWEST LEVEL SUB
foreach($filter_by['who'] as $i => $f) {
	if(strpos($i, "DIR") !== false) {
		unset($filter_by['who'][$i]);
	}
}
//ASSIST_HELPER::arrPrint($filter_by); //die();

if(count($filter_by['who']) > 0) {

	include("common/form_object.php");

} else {
	ASSIST_HELPER::displayResult(array("info", "You do not have the necessary ".$myObject->getObjectName("ADMIN")." access to ".$myObject->getActivityName($page_action)." ".$myObject->getObjectName($myObject->getMyObjectName(true)).".  Please contact your ".$myObject->getObjectName("SDBIP")." ".$myObject->getObjectName("ADMIN")." if you believe this to be an error."));
}

?>
<?php markTime("end of page"); ?>