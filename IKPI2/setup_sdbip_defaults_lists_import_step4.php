<?php
//ASSIST_HELPER::arrPrint($_REQUEST);

//Get data from temporary table
$import_ref = $_REQUEST['import_ref'];
$sql = "SELECT * FROM ".$listObject->getDBRef()."_list_import WHERE import_ref = '".$import_ref."'";
$data = $listObject->mysql_fetch_all_by_id($sql, "import_key");

//setup variables
$copy_fields = array();
foreach($headings['rows'] as $fld => $Head) {
	$copy_fields[] = $fld;
}

//process data
if(count($data) > 0) {
	echo "<p class=b>Importing:<ul>";
	//loop through each row from temp table and save to live table
	foreach($data as $key => $row) {
		//add one to the key to match the row number in excel (starts at 1) where key comes from array key (starts at 0)
		echo "<li>Row ".($key + 1)."...";
		//get data for new list item
		$import_data = array('sdbip_id' => $sdbip_id);
		foreach($copy_fields as $fld) {
			$import_data[$fld] = $row[$fld];
		}
		//send data to SDBP6_LIST->addObject for processing
		$listObject->addObject($import_data);

		//$sql = "INSERT INTO ".$listObject->getTableName()." SET ".$listObject->convertArrayToSQLForSave($import_data);
		//$id = $listObject->db_insert($sql);

		echo "done.</li>";

	}//end foreach data as row
	echo "</ul>
	";
//update temp table to acknowledge successful import
	$sql = "UPDATE ".$listObject->getDBRef()."_list_import SET import_status = 1 WHERE import_ref = '".$import_ref."'";
	$listObject->db_update($sql);

	echo "
	<script type=text/javascript>
	$(function() {
		AssistHelper.processing();
		AssistHelper.finishedProcessingWithRedirect('ok','Import completed successfully','".$my_url."');
	});
	</script>";

} else {
	ASSIST_HELPER::displayResult(array("error", "No data found to import - the holding table does not contain any data for reference: ".$import_ref.".  Please try again otherwise contact your Assist Administrator."));

}
?>