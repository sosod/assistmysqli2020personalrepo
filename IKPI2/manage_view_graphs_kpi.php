<?php
require_once("inc_header.php");


//get object results
$object_type = $_REQUEST['gt'];
$class_name = "SDBP6_".$object_type."KPI";
$myObject = new $class_name();
$dir_id = $_REQUEST['i'];
$results = $myObject->getSummaryOfResultsByOrgTopID($sdbip_id, $dir_id, "YTD");
//ASSIST_HELPER::arrPrint($results);
$count = $results['results']['ALL'];
$sub_count = $results['results'];

//get results settings
$resultObject = new SDBP6_SETUP_RESULTS();
$result_settings = $resultObject->getMyActiveResultSettings(true);
//$result_settings = array();
foreach($result_settings as $i => $r) {
	$result_settings[$i]['text'] = $r['code'];
}
//ASSIST_HELPER::arrPrint($result_settings);

$kr2 = array();
foreach($result_settings as $ky => $k) {
	$k['obj'] = isset($results['results']['ALL'][$k['code']]) ? $results['results']['ALL'][$k['code']] : 0;
	$val = array();
	foreach($k as $key => $s) {
		$val[] = $key.":\"".$s."\"";
	}
	$kr2[$ky] = implode(",", $val);
}

$chartData = "{".implode("},{", $kr2)."}";
$sub_chartData = array();
foreach($results['org_list'] as $s_id => $s) {
	if(isset($sub_count[$s_id])) {
		$sval = explode(" ", ASSIST_HELPER::decode($s));
		$s_name = "";
		$s_n = "";
		foreach($sval as $sv) {
			if(strlen($s_n.$sv) > 20) {
				$s_name .= $s_n."\\n";
				$s_n = "";
			}
			$s_n .= " ".$sv;
		}
		$s_name .= " ".$s_n;
		$cD = "{text:\"".$s_name."\"";
		foreach($result_settings as $ky => $k) {
			$cD .= ",R".$k['id'].":".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
		}
		$cD .= "}";
		$sub_chartData[] = $cD;
	}
}
$graph_width = count($sub_chartData) * 100 + 100;


?>
<script type="text/javascript">
	var chart;
	var legend;
	var chartData = [<?php echo $chartData; ?>];
	var sub_chartData = [<?php echo implode(",", $sub_chartData) ?>];

	$(document).ready(function () {
		dir_chart = new AmCharts.AmPieChart();
		dir_chart.color = "#ffffff";
		dir_chart.dataProvider = chartData;
		dir_chart.titleField = "value";
		dir_chart.valueField = "obj";
		dir_chart.colorField = "color";
		dir_chart.labelText = "[[percents]]%";
		dir_chart.labelRadius = -25;
		dir_chart.angle = 10;
		dir_chart.depth3D = 15;
		dir_chart.hideLabelsPercent = 0;
		dir_chart.hoverAlpha = 0.5;
		dir_chart.startDuration = 0;
		dir_chart.radius = 150;
		dir_chart.marginBottom = 30;
		dir_chart.write("dir");

		sub_chart = new AmCharts.AmSerialChart();
		sub_chart.color = "#ffffff";
		sub_chart.dataProvider = sub_chartData;
		sub_chart.categoryField = "text";
		sub_chart.marginLeft = 47;
		sub_chart.marginTop = 30;
		sub_chart.marginBottom = 75;
		sub_chart.fontSize = 10;
		sub_chart.angle = 45;
		sub_chart.depth3D = 15;
		sub_chart.plotAreaBorderAlpha = 0.2;
		sub_chart.rotate = false;

		<?php foreach($result_settings as $ky => $k) { ?>
		var graph = new AmCharts.AmGraph();
		graph.title = "<?php echo $k['value']; ?>";
		graph.labelText = "[[percents]]%";
		graph.valueField = "R<?php echo $k['id']; ?>";
		graph.type = "column";
		graph.lineAlpha = 0;
		graph.fillAlphas = 1;
		graph.lineColor = "<?php echo $k['color']; ?>";
		graph.balloonText = "[[percents]]%";
		sub_chart.addGraph(graph);
		<?php } ?>

		var valAxis = new AmCharts.ValueAxis();
		valAxis.stackType = "regular";	//100%
		valAxis.gridAlpha = 0.1;
		valAxis.axisAlpha = 0;
		valAxis.color = "#000000";
		sub_chart.addValueAxis(valAxis);

		sub_chart.categoryAxis.gridAlpha = 0.1;
		sub_chart.categoryAxis.axisAlpha = 0;
		sub_chart.categoryAxis.color = "#000000";
		sub_chart.categoryAxis.fontSize = 7;
		//sub_chart.categoryAxis.labelRotation = 0;
		//sub_chart.categoryAxis.autoGridCount = true;
		//sub_chart.categoryAxis.gridCount = 1;
		sub_chart.categoryAxis.gridPosition = "start";

		sub_chart.write("sub");
	});
</script>
<div align=center>
	<table class=noborder>
		<tr class=no-highlight>
			<td class="center noborder"><span style="font-weight: bold;"><?php echo $results['org_name']; ?></span>
				<div id="dir" style="width:400px; height:350px; background-color:#ffffff;"></div>
			</td>
			<td class="center noborder"><span style="font-weight: bold;"><?php echo $head_sub; ?></span>
				<div id="sub" style="width:<?php echo $graph_width; ?>px; height:400px; background-color:#ffffff;"></div>
			</td>
		</tr>
		<tr class=no-highlight>
			<td colspan=2 class=noborder>
				<div align=center>
					<table>
						<thead>
						<tr>
							<td rowspan=2 style="border-top-color: #ffffff; border-left-color: #ffffff;"></td>
							<th rowspan=2><?php echo str_replace(" ", "<br />", $results['org_name']); ?></th>
							<th colspan=<?php echo count($results['org_list']); ?>><?php echo $head_sub; ?></th>
						</tr>
						<tr>
							<?php
							foreach($results['org_list'] as $s_id => $s) {
								echo "<th class=th2 style=\"font-size: 7pt;\">".str_replace(" ", "<br />", $s)."</th>";
							}
							?>
						</tr>
						</thead>
						<tbody>
						<?php
						foreach($result_settings as $ky => $k) {
							echo "<tr>
				<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
				".$k['value']."</td>";
							echo "<td class=center style=\"font-weight: bold; background-color: #dddddd;\">".(isset($count[$k['code']]) ? $count[$k['code']]." (".number_format(($count[$k['code']] / array_sum($count) * 100), 2)."%)" : "-")."</td>";
							foreach($results['org_list'] as $s_id => $s) {
								echo "<td class=center>".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']]." (".number_format(($sub_count[$s_id][$k['text']] / array_sum($sub_count[$s_id]) * 100), 2)."%)" : "-")."</td>";
							}
							echo "</tr>";
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<td class=right rowspan=2 style="background-color: #dddddd;"><b>Total:</b></td>
							<td class="center" style="background-color: #555555; color: #ffffff; font-weight: bold;"><?php echo array_sum($count); ?></td>
							<?php
							foreach($results['org_list'] as $s_id => $s) {
								echo "<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? array_sum($sub_count[$s_id]) : "-")."</b></td>";
							}
							?>
						</tr>
						<tr>
							<td class="center" style="background-color: #555555; color: #ffffff; font-weight: bold;">100%</td>
							<?php
							foreach($results['org_list'] as $s_id => $s) {
								echo "<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? number_format((array_sum($sub_count[$s_id]) / array_sum($count) * 100), 2)."%" : "-")."</b></td>";
							}
							?>
						</tr>
						</tfoot>
					</table>
				</div>
			</td>
		</tr>
	</table>
</div>
?>