<?php
include("inc_header.php");

$parent_id = $_REQUEST['parent_id'];

$items = $orgObject->getActiveObjectsFormattedForSelect(array('parent_id' => $parent_id, 'sdbip_id' => $sdbip_id));

//ASSIST_HELPER::arrPrint($items);


if(count($items) == 0) {
	ASSIST_HELPER::displayResult(array("error", "No items found to sort."));
	die();
}
?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<form name=frm_sort><input type=hidden name=parent_id value='<?php echo $parent_id; ?>' />
				<ul id=ul_sortable>
					<?php
					foreach($items as $key => $i) {
						echo "<li class=ui-state-default style='background:url(); background-color:#FFFFFF'><img src=/pics/sort_icon.png />&nbsp;".$i."<input type=hidden name=order[] value='".$key."' /></li>";
					}
					?>
				</ul>
			</form>
		</td>
	</tr>
	<Tr>
		<td class=center>
			<button id=btn_save><?php echo $helper->getActivityName("save"); ?> Display Order</button>
		</td>
	</Tr>
	<tr>
		<td>
			<?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_sdbip_defaults_orgstructure.php')); ?>
		</td>
	</tr>
</table>
<style>

	#ul_sortable {
		list-style-image: url();
		list-style-type: none;
		margin: 0;
		padding: 0;
		width: 100%
	}

	#ul_sortable li {
		padding: 10px;
		margin: 7px;
		height: 20px;
		width: 100%
	}

	.ui-state-highlight {
		height: 1.5em;
		line-height: 1.2em;
	}
</style>
<script type="text/javascript">
	$(function () {
		$("#ul_sortable").sortable({}).disableSelection();

		$("#btn_save").button({
			icon: {primary: "ui-icon-disk"}
		}).removeClass("ui-state-default").addClass("ui-button-state-green")
			.hover(function () {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_sort]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ORGSTRUCTURE.SORT", dta);
			if (result[0] == "ok") {
				AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "setup_sdbip_defaults_orgstructure.php");
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}

		});


		var width = 0;
		$("#ul_sortable li:first").each(function () {
			width = ($(this).width());
		});
		$("#ul_sortable li").closest("table").width(width + 25);

	});
</script>