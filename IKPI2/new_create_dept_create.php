<?php
/**
 * @var array $sdbip_details - from inc_header
 */

require_once("inc_header.php");
$page_object = SDBP6_DEPTKPI::OBJECT_TYPE;
require_once("inc_new_statuscheck.php");
$header_already_included = true;

//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($sdbip_details);
//echo "<hr />";


$myObject = new SDBP6_DEPTKPI();
$object_type = $myObject->getMyObjectType();
$parent_object_type = $myObject->getMyParentObjectType();
$class_name = "SDBP6_".$parent_object_type;
$parentObject = new $class_name();
$parent_object_id = $sdbip_details['id'];

$page_section = "NEW";
$page_action = "EDIT";


$add_button = true;
$add_button_label = "|add| |".$object_type."|"; //echo $add_button_label;
$add_button_function = "showAddDialog();";

$page_direct = "new_create_dept_create_edit.php";
$page_redirect_path = "new_create_dept_create_add.php?";

include("common/generic_list_page.php");
markTime("end generic list page");
?>

	<script type="text/javascript">
		$(function () {
			<?php
			if(isset($data['js'])) {

				echo $data['js'];
			}
			?>

		});	//end start of jquery

		function showAddDialog() {
			AssistHelper.processing();
			document.location.href = '<?php echo $page_redirect_path; ?>';
		}
	</script>
<?php markTime("end of page"); ?>