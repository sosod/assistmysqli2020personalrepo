<?php
/**
 * $modref - from calling page
 */
$now = strtotime(date("d F Y", ($today))); //echo date("d F Y H:i:s",$now);
/** @var INT $next_due - from main_login.php = number of days to display */
$future = strtotime(date("d F Y", ($today + (($next_due) * 24 * 3600)))); //echo date("d F Y H:i:s",$future);

/**
 * REMINDER - SDBIP does not have "overdue" - once deadline has passed, result is no longer accessible
 */

//----PROCESS----
//FIRST CHECK FOR NORMAL KPIs TO BE UPDATED
//1. get user access - primary, secondary, tertiary
//2. check time periods - any open (based on user access] with deadline date before $future?
//3. check access - any update permissions? - NOT PARENT ORG
//4. foreach object - count result records that meet time & access settings where result has not been updated ++++ any approve/assurance records

$sdbipObject = new SDBP6_SDBIP();
$available_sdbips = $sdbipObject->getAllActivatedObjects(false, false, false, "ASC");

if(count($available_sdbips) > 0) {
	$sdbip_ids = array_keys($available_sdbips);
	$line = array();
//USER ACCESS
	$userObject = new SDBP6_USERACCESS($modref);
	$my_user_access = $userObject->getMyUserAccess();
	if($my_user_access['time_third'] == true) {
		$time_user = "tertiary";
	} elseif($my_user_access['time_second'] == true) {
		$time_user = "secondary";
	} else {
		$time_user = "primary";
	}

//GET TIME PERIODS
	$timeObject = new SDBP6_SETUP_TIME($modref);
	$available_time_periods = $timeObject->getAvailableTimePeriodsForFrontPageDashboard($time_user, $future, $sdbip_ids);

//ASSIST_HELPER::arrPrint($available_time_periods);

//ADMIN ACCESS
	$adminObject = new SDBP6_SETUP_ADMINS($modref);
	$admin_access = $adminObject->getMyUpdateAccessForFrontPageDashboard($sdbip_ids);
//ASSIST_HELPER::arrPrint($admin_access);


//CALCULATION TYPES (to check for calc types that allow 0 target consideration)
	$calcObject = new SDBP6_SETUP_CALCTYPE($modref);
	$zero_calc_types = $calcObject->getZeroResultCalculationTypesForDashboard();
//ASSIST_HELPER::arrPrint($zero_calc_types);

	$available_objects = array("DEPTKPI");//,"TOPKPI","PROJECT");
	$admin_sections = array('DEPTKPI' => SDBP6_DEPTKPI::ADMIN_SECTION, 'TOPKPI' => SDBP6_TOPKPI::ADMIN_SECTION, 'PROJECT' => SDBP6_PROJECT::ADMIN_SECTION);


//foreach object type
	foreach($sdbip_ids as $sdbip_id) {
		$count = array(
			'overdue' => 0,
			'present' => 0,
			'future' => 0,
		);
		foreach($available_objects as $object_type) {
			$admin_section = $admin_sections[$object_type];
			if(isset($admin_access[$admin_section])) {
				$aa = $admin_access[$admin_section];
				$aa_owner = isset($aa['OWNER']) ? $aa['OWNER'] : array();
				$aa_org = isset($aa['ORG']) ? $aa['ORG'] : array();
				//Added PARENT_ORG as an option - found that the getLoginStats function isn't triggered if a user does not have top level org structure update access EXCEPT they can have a KPI rejected & returned to them which then doesn't show. AA-540 JC 17 March 2021
				if(count($aa_org) > 0 || count($aa_owner) > 0 || count($aa['PARENT_ORG']) > 0) {
					$class = "SDBP6_".$object_type;
					$myObject = new $class($modref);
					/** @var SDBP6_DEPTKPI $myObject */
					$objects = $myObject->getLoginStats($available_time_periods[$sdbip_id][$myObject->getTimeType()], $aa_org, $aa_owner, $future, $zero_calc_types, $sdbip_id);
					$count['present'] += $objects['present'];
					$count['future'] += $objects['future'];
				}
			}
		}
		$line[$available_sdbips[$sdbip_id]['name']] = array('count' => $count, 'object_id' => $sdbip_id);
	}

} else {
	$line = array();
}

?>