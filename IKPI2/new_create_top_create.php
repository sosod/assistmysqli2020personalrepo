<?php
require_once("inc_header.php");
$page_object = SDBP6_TOPKPI::OBJECT_TYPE;
require_once("inc_new_statuscheck.php");
$header_already_included = true;


$myObject = new SDBP6_TOPKPI();
$object_type = $myObject->getMyObjectType();
$parent_object_type = $myObject->getMyParentObjectType();
$class_name = "SDBP6_".$parent_object_type;
$parentObject = new $class_name();
$parent_object_id = $sdbip_details['id'];

$page_section = "NEW";
$page_action = "EDIT";


$add_button = true;
$add_button_label = "|add| |".$object_type."|"; //echo $add_button_label;
$add_button_function = "showAddDialog();";

$page_direct = "new_create_top_create_edit.php";

include("common/generic_list_page.php");
markTime("end generic list page");
?>

	<div id=dlg_add title="<?php echo $helper->replaceAllNames($add_button_label); ?>">
		<?php

		//$section = "NEW";
		$page_redirect_path = "new_create_top_create_add.php?";
		//$page_action = "Add";

		//$uaObject = new IDP3_USERACCESS();
		/**
		 * ADDs 2 SECONDS TO PAGE LOAD TIME
		 */
		//	$data = $displayObject->getObjectForm($object_type, $myObject, 0, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
		//	echo $data['display'];

		?>
	</div>
	<script type="text/javascript">
		$(function () {
			<?php
			if(isset($data['js'])) {
				echo $data['js'];
			}
			if(isset($js)) {
				echo $js;
			}
			?>
			//dialog code must come after $js to allow for child_results_table formatting to work before dialog is hidden
			$("#dlg_add").dialog({
				autoOpen: false,
				modal: true
			});

		});	//end start of jquery

		function showAddDialog() {
			/*$(function() {
				$("#dlg_add").dialog("open");
				var my_window = AssistHelper.getWindowSize();
				if(my_window['width']>700) {
					$("#dlg_add table.form").css("width","700px");
					$("#dlg_add").dialog("option","width",750);
				} else {
					$("#dlg_add table.form").css("width",(my_window['width']-100)+"px");
					$("#dlg_add").dialog("option","width",(my_window['width']-50));
				}
				if((parseInt(AssistString.substr($("#dlg_add table.form").css("height"),0,-2)))>my_window['height']-50) {
					$("#dlg_add").dialog("option","height",my_window['height']-50);
				}
			});*/
			AssistHelper.processing();
			document.location.href = '<?php echo $page_redirect_path; ?>';
		}

		function selectNextTabbableOrFocusableForDatepicker(selector) {
			var selectables = $(selector);
			var current = $(':focus');
			var nextIndex = 0;
			var movementIndex = 1;
			if (current.hasClass('btn_yes')) {
				movementIndex++;
			}
			if (current.length === 1) {
				var currentIndex = selectables.index(current);
				if (currentIndex + movementIndex < selectables.length) {
					nextIndex = currentIndex + movementIndex;
				}
			}

			selectables.eq(nextIndex).focus();
		}

		var import_ref = '<?php echo $myObject->getExternalImportRef($sdbip_details, $object_type); ?>';

		function checkAddStatus(import_ref) {
			var url = "inc_controller.php?action=<?php echo $object_type; ?>.isPhase2InProgress";
			var dta = "import_ref=" + import_ref;
			var r = AssistHelper.doAjax(url, dta);
			if (r[0] == "done") {
				$("#btn_paging_add").show();
				clearInterval(myTimer);
			} else {
				$("#btn_paging_add").hide();
			}
		}

		var myTimer = setInterval(checkAddStatus, 10000, import_ref);
		checkAddStatus(import_ref);

	</script>
<?php markTime("end of page"); ?>