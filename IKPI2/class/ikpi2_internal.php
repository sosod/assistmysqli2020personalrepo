<?php


class IKPI2_INTERNAL extends SDBP6 {

	private $modref;

	public function __construct($modref) {
		//echo $modref;
		parent::__construct($modref);
		$this->modref = $modref;
	}


	public function getFilters($include_blanks = false) {
		$filters = array();
		$parentObject = new SDBP6_PARENT(0, $this->modref);
		$parent_objects = $parentObject->getOrderedObjects();
		if(count($parent_objects) > 0) {
			$childObject = new SDBP6_CHILD(0, $this->modref);
			$child_objects = $childObject->getOrderedObjects(array_keys($parent_objects));

			if(count($child_objects) > 0) {
				$activityObject = new SDBP6_ACTIVITY(0, $this->modref);
				$activity_objects = $activityObject->getOrderedObjects(array_keys($parent_objects), 0, $this->modref);
			}
			foreach($parent_objects as $p_id => $p) {
				if(isset($child_objects[$p_id]) && count($child_objects[$p_id])) {
					$f = $p['name'].": ";
					foreach($child_objects[$p_id] as $c_id => $c) {
						if((isset($activity_objects[$c_id]) && count($activity_objects[$c_id]) > 0) || $include_blanks) {
							$count = isset($activity_objects[$c_id]) ? count($activity_objects[$c_id]) : 0;
							$filters[$c_id] = $f.$c['name']." (".$count." record".($count != 1 ? "s" : "").")";
						}
					}
				}
			}
		}
		return $filters;
	}

	public function getLegacyDataStructure() {
		$parentObject = new SDBP6_PARENT(0, $this->modref);
		$parent_objects = $parentObject->getOrderedObjects();
		if(count($parent_objects) > 0) {
			$childObject = new SDBP6_CHILD(0, $this->modref);
			$child_objects = $childObject->getOrderedObjects(array_keys($parent_objects));

			if(count($child_objects) > 0) {
				$activityObject = new SDBP6_ACTIVITY(0, $this->modref);
				$activity_objects = $activityObject->getOrderedObjects(array_keys($parent_objects), 0, $this->modref);
			}

			$data_structure = array();
			foreach($parent_objects as $p_id => $p) {
				$data_structure[$p_id]['parent_object'] = $p;
				if(isset($child_objects[$p_id]) && count($child_objects[$p_id]) > 0) {
					foreach($child_objects[$p_id] as $c_id => $c) {
						$data_structure[$p_id]['children_objects'][$c_id]['child_object'] = $c;
						if((isset($activity_objects[$c_id]) && count($activity_objects[$c_id]) > 0)) {
							foreach($activity_objects[$c_id] as $activity_id => $activity) {

								$data_structure[$p_id]['children_objects'][$c_id]['child_activities'][$activity_id] = $activity;
							}
						} else {
							unset($data_structure[$p_id]['children_objects'][$c_id]);
						}
					}
				} else {
					unset($data_structure[$p_id]);
				}
			}
		}
		return $data_structure;
	}


	public function getLinesByFilter($filter) {
		$data = array(
			'head' => $this->getHeadings(),
			'rows' => $this->getRows($filter),
		);
		return $data;
	}


	public function getLinesByID($id) {
		$data = array(
			'head' => $this->getHeadings(),
			'rows' => $this->getRows(0, $id),
		);
		return $data;
	}


	public function getHeadings() {
		$headObject = new SDBP6_HEADINGS($this->modref);
		$headings = $headObject->getHeadingsForInternal();

		$data = array();
		foreach($headings as $fld => $h) {
			$data[$fld] = $h['name'];
		}
		return $data;
	}


	public function getRows($filter = 0, $id = array()) {
		$parentObject = new SDBP6_PARENT(0, $this->modref);
		if($this->checkIntRef($filter)) {
			$children = $parentObject->getOrderedObjectsByChild($filter);
		} else {
			$children = $parentObject->getOrderedObjectsByActivity($id);
		}
		return $children;
	}


	public function getMainObjectsForPerformanceModuleSetup($limit_by_fin_year = false) {
		$myObject = new SDBP6_SDBIP($this->modref);
		$rows = $myObject->getAllActivatedObjects(true, false, $limit_by_fin_year);
		$data = array();
		foreach($rows as $key => $row) {
			$data[] = array('id' => $key."_".$row['fin_year_id'], 'value' => $row[$myObject->getNameFieldName()]." (Financial Year: ".$row['fin_year_name'].")");
		}
		return $data;
	}


	public function getAvailableHeadingsForPerformanceGrouping($generic_fields) {
		$myObject = new SDBP6_HEADINGS($this->modref);
		$fields = $myObject->getFieldsForExternalModule($generic_fields);
		/*
		 * returns: array(generic_field=>field name) only for items which are active in all kpi sections
		 */
		return $fields;
	}


	public function getASDBIPName($object_id) {
		$myObject = new SDBP6_SDBIP($this->modref);
		$info = $myObject->getASDBIPName($object_id);
		if($info !== false && is_array($info)) {
			return $info['name'];
		}
		return $info;
	}


	public function __destruct() {
		parent::__destruct();
	}
}


?>