<?php

/**
 * Class IKPI2_PMS - to manage the link between IKPI2 & various PM* modules - Should match SDBP6_PMS class exactly
 *
 * Updates:
 * AA-584 - Calculate PUA results if the period under assessment does not start with the first month. [JC] April 2021
 */
class IKPI2_PMS extends ASSIST_MODULE_HELPER {
	protected $result_settings = array(
		0 => array('id' => 0, 'r' => 0, 'value' => "KPI Not Yet Measured", 'text' => "N/A", 'style' => "result0", 'color' => "#999999", 'glossary' => "KPIs with no targets or actuals in the selected period."),
		1 => array('id' => 1, 'r' => 1, 'value' => "KPI Not Met", 'text' => "R", 'style' => "result1", 'color' => "#CC0001", 'glossary' => "0% >= Actual/Target < 75%"),
		2 => array('id' => 2, 'r' => 2, 'value' => "KPI Almost Met", 'text' => "O", 'style' => "result2", 'color' => "#FE9900", 'glossary' => "75% >= Actual/Target < 100%"),
		3 => array('id' => 3, 'r' => 3, 'value' => "KPI Met", 'text' => "G", 'style' => "result3", 'color' => "#009900", 'glossary' => "Actual/Target = 100%"),
		4 => array('id' => 4, 'r' => 4, 'value' => "KPI Well Met", 'text' => "G2", 'style' => "result4", 'color' => "#005500", 'glossary' => "100% > Actual/Target < 150%"),
		5 => array('id' => 5, 'r' => 5, 'value' => "KPI Extremely Well Met", 'text' => "B", 'style' => "result5", 'color' => "#000077", 'glossary' => "Actual/Target >= 150%"),
	);

	protected $sections = array(
		'KPI' => array(
			'code' => "D",
			'name' => "Departmental SDBIP",
			'status' => 1,
			'page' => "dept",
			'field' => "kpi_",
		),
		'KAS' => array(
			'code' => "D",
			'name' => "Departmental Assurance Review",
			'status' => 1,
			'page' => "dept",
			'field' => "kas_",
		),
		'TOP' => array(
			'code' => "TL",
			'name' => "Top Layer SDBIP",
			'status' => 1,
			'page' => "top",
			'field' => "top_",
		),
		'CAPITAL' => array(
			'code' => "CAP",
			'name' => "Capital Projects",
			'status' => 1,
			'page' => "capital",
		),
		'CASHFLOW' => array(
			'code' => "CF",
			'name' => "Monthly Cashflows",
			'status' => 1,
			'page' => "cashflow",
		),
		'REVBYSRC' => array(
			'code' => "RS",
			'name' => "Revenue By Source",
			'status' => 1,
			'page' => "revbysrc",
		),
	);
	private $module_defaults = array();

	private $pms_heading_fields = array(
		"kpi_munkpaid",
		"kpi_idpid",
		"kpi_pdoid",
		"kpi_value",
		"kpi_unit",
		"kpi_baseline",
		"kpi_poe",
		"kpi_subid",
		"kpi_ownerid",
	);
	private $scoring_heading_fields = array(
		"kpi_munkpaid",
		"kpi_value",
		"kpi_unit",
		"kpi_baseline",
		"kpi_poe",
	);

	private $modref;


	private $object_types = array(
		'KPI' => "DEPTKPI",
		'TOP' => "TOPKPI",
		'PROJ' => "PROJECT",
	);
	private $field_prefix = array(
		'KPI' => "kpi",
		'TOP' => "top",
		'PROJ' => "kpi",
	);

	//SDBP6->ASSURANCE status fields
	private $assurance_status_text = array(
		0 => "Not reviewed",
		16 => "Signed-off",
		32 => "Rejected",
		64 => "Updated after Rejection",
		128 => "Auto signed off",
	);

	//SDBP5B Statuses - removed AA-341 [JC] 21 Feb 2020
//	const ASSURANCE_ACCEPT = 2;
//	const ASSURANCE_REJECT = 4;
//	const ASSURANCE_UPDATED = 8;
	//SDBP6 Assurance/Approve Statuses
	const NOT_REVIEWED = 0;
	const REVIEW_OK = 16;
	const REVIEW_REJECT = 32;
	const REVIEW_UPDATED = 64;    //when user updates result in response to rejection
	const REVIEW_AUTO = 128;    //when approve/assurance is done by auto process

	public function __construct($modref = "") {
		parent::__construct("client", "", false, $modref);
		if(strlen($modref) > 0) {
			$this->modref = $modref;
		}
	}


	public function getSections() {
		return $this->sections;
	}

	public function getSection($s) {
		return $this->sections[$s];
	}

	public function getAssuranceText($a) {
		if(isset($this->assurance_status_text[$a])) {
			return $this->assurance_status_text[$a];
		} else {
			return $a;
		}
	}

	public function getFieldPrefix($types) {
		$data = array();
		foreach($types as $t) {
			$data[$t] = $this->field_prefix[$t];
		}
		return $data;
	}


	public function getKPITableField() {
		return SDBP6_DEPTKPI::TABLE_FLD;
	}

	public function getTopTableField() {
		return SDBP6_TOPKPI::TABLE_FLD;
	}


	/*******************************************************
	 * INDIVIDUAL PERFORMANCE FUNCTIONS
	 ******************************************************/
	public function getFilters($var) {
		$type = isset($var['type']) ? $var['type'] : "KPI";
		$sdbip_id = $var['modobject'];
		$db = new ASSIST_DB();
		$db->setDBRef($var['modref']);
		switch($type) {
			case "TOP":    //strategic KPIs
				$own_sql = "SELECT O.id, O.name as value, count(top_owner_id) as c
						FROM ".$db->getDBRef()."_list_owner O
						INNER JOIN ".$db->getDBRef()."_top ON top_owner_id = O.id
						WHERE (top_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." 
						AND top_sdbip_id = ".$sdbip_id."
						GROUP BY O.id
						HAVING count(top_owner_id)>0
						ORDER BY O.name
						";
				$sub_sql = "SELECT O.org_id as id, O.org_name as value, count(top_sub_id) as c
						FROM ".$db->getDBRef()."_setup_orgstructure O
						INNER JOIN ".$db->getDBRef()."_top ON top_sub_id = O.org_id
						WHERE (top_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE."
						AND top_sdbip_id = ".$sdbip_id."
						GROUP BY O.org_id
						HAVING count(top_sub_id)>0
						ORDER BY O.org_order
						";
				break;
			case "PROJ":    //AKA KPIs linked to capital projects = capital KPIs
			case "KPI":    //operational KPIs = dept kpis not linked to capital projects
			default:
				$own_sql = "SELECT O.id, O.name as value, count(kpi_owner_id) as c
						FROM ".$db->getDBRef()."_list_owner O
						INNER JOIN ".$db->getDBRef()."_kpi ON kpi_owner_id = O.id
						WHERE (kpi_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." AND kpi_cap_op = ".($type == "PROJ" ? 1 : 0)."
						AND kpi_sdbip_id = ".$sdbip_id."
						GROUP BY O.id
						HAVING count(kpi_owner_id)>0
						ORDER BY O.name
						";
				$sub_sql = "SELECT O.org_id as id, CONCAT(A.org_name,' - ',O.org_name) as value, count(kpi_sub_id) as c
						FROM ".$db->getDBRef()."_setup_orgstructure O
						INNER JOIN ".$db->getDBRef()."_setup_orgstructure A ON O.org_parent_id = A.org_id
						INNER JOIN ".$db->getDBRef()."_kpi ON kpi_sub_id = O.org_id
						WHERE (kpi_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE." AND kpi_cap_op = ".($type == "PROJ" ? 1 : 0)."
						AND kpi_sdbip_id = ".$sdbip_id."
						GROUP BY O.org_id
						HAVING count(kpi_sub_id)>0
						ORDER BY A.org_order, O.org_order
						";
		}
		$owners = $db->mysql_fetch_all($own_sql);
		$subs = $db->mysql_fetch_all($sub_sql);
		//TODO - get object names from SDBP6 module
		if($type == "TOP") {
			$names = array('subs' => "Responsible Departments", 'owners' => "Responsible Owners");
		} else {
			$names = array('subs' => "Responsible Departments", 'owners' => "Responsible Owners");
		}
		return array('subs' => $subs, 'owners' => $owners, 'names' => $names);
	}


	/**
	 * @param $var
	 * @param bool $get_headings
	 * @param bool $get_results
	 * @param array $time_ids
	 * @return array
	 */
	public function getKPIs($var, $get_headings = true, $get_results = false, $time_ids = array(), $primary_object_id = 0, $active_only = true, $start_time_id = 1) {
//		return $this->getCapOpKPIs("KPI", $var,$get_headings,$get_results,$time_ids);
		return $this->getKPIsByType("KPI", $var, $get_headings, $get_results, $time_ids, $primary_object_id, $active_only, $start_time_id);
	}

	/**
	 * @param $var
	 * @param bool $get_headings
	 * @param bool $get_results
	 * @param array $time_ids
	 * @return array
	 */
	public function getProjects($var, $get_headings = true, $get_results = false, $time_ids = array(), $primary_object_id = 0, $active_only = true, $start_time_id = 1) {
		//return $this->getCapOpKPIs("PROJ", $var,$get_headings,$get_results,$time_ids);
		return $this->getKPIsByType("PROJ", $var, $get_headings, $get_results, $time_ids, $primary_object_id, $active_only, $start_time_id);
	}
	//GET SAME RESULTS FOR OP & CAP KPIs AS FROM SDBP6/IKPI2 - only different is kpi_cap_op = 0/1 - this is because multiple projects can be linked to a single KPI or not at all
	//KILLED 19 Oct 2019 AA-247 [JC] - replaced with centralised call getKPIsByType()
	/*private function getCapOpKPIs($type,$var,$get_headings=true,$get_results=false,$time_ids=array()) {
		$object_type = $this->object_types[$type];
		$modref = $var['modref'];
		$filter_type = $var['filter_type'];
		$filter_id = $var['filter_id'];
		$group_field = $var['group_field'];
		$db = new SDBP6_DEPTKPI($modref,0,true);
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE($modref);
		$fld_prefix = "kpi";
		$parent_object_id = isset($var['sdbip_id'])?$var['sdbip_id']:false;
//		$db->setDBRef($modref);
//die();

		$data = array('head'=>array(),'objects'=>array(),'grouping'=>array());

		//GET HEADING DETAILS
//		if($get_headings) {
			$data['head'] = $this->getHeadings($var['primary_source'],true,false,SDBP6_DEPTKPI::OBJECT_TYPE,$group_field);
//		}

		//GET LISTS
		$lists = array();
		$grouping = array();
		foreach($data['head']['raw'] as $fld=>$h) {
			$grouping_check = false;
			//get list items
			switch($h['field_type']) {
				case "MULTILIST":
				case "LIST": $grouping_check = true;
					//get list items if not already available
					if(!isset($lists[$fld])) {
						$listObject = new SDBP6_LIST($h['h_table'],$modref);
						$l = $listObject->getAllListItemsFormattedForSelect();
						unset($listObject);
						$lists[$fld] = $l;
					}
					break;
				case "MULTIOBJECT":
				case "OBJECT": $grouping_check = true;
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$list_object_name = $h['h_table'];
						$extra_info = array();
						if(strpos($list_object_name,"|")!==false) {
							$lon = explode("|",$list_object_name);
							$list_object_name = $lon[0];
							$extra_info = $lon[1];
						}
						if($list_object_name=="SDBP6_TOPKPI" || $list_object_name=="SDBP6_PROJECT") {
							$listObject = new $list_object_name($modref,0,true);
						} else {
							$listObject = new $list_object_name($modref);
						}
						$l = $listObject->getActiveObjectsFormattedForSelect($extra_info);
						unset($listObject);
						$lists[$fld] = $l;
					}
					break;
				case "MULTISEGMENT":
				case "SEGMENT": $grouping_check = true;
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$list_object_type = $h['h_table'];
						$listObject = new SDBP6_SEGMENTS($list_object_type);
						$l = $listObject->getAllListItemsFormattedForSelect(false);
						unset($listObject);
						$lists[$fld] = $l;
					}
					break;
			} //end switch by type
			if($grouping_check) {
				//check if grouping field
				$f = explode("_",$fld);
				unset($f[0]);
				$fld2 = implode("_",$f);
				if($group_field==$fld2) {
					$grouping = $l;
				}
			}
		}
		$data['grouping'] = $grouping;

		//GET KPIS
		$sql = "SELECT *, kpi_id as id, ".$db->getTargetTypeTableField()." as target_type_id
					, CONCAT('".$modref."/".$db->getRefTag()."',kpi_id) as ref
					, IF((kpi_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE.", 1, 0) as active
					, 1 as cap_active
				FROM `".$db->getDBRef()."_kpi`
				WHERE kpi_cap_op = ".($type=="KPI"?0:1)."
				".($parent_object_id!==false?"AND ".$db->getParentFieldName()." = ".$parent_object_id:"")."
				AND
				";
		switch($filter_type) {
			case "ID":
				$sql.=" ".$fld_prefix."_id IN (".implode(",",$filter_id).") ";
				break;
			case "sub":
				$sql.=" ".$fld_prefix."_sub_id = ".$filter_id;
				break;
			default:
				$sql.=" ".$fld_prefix."_owner_id = ".$filter_id;
				break;
		}
		$rows = $db->mysql_fetch_all_by_id($sql, "id");

		//if total columns exist then get the summary
		if(isset($data['head']['main']['original_total']) || isset($data['head']['main']['revised_total']) || isset($data['head']['main']['actual_total'])) {
			$summary_values = $db->getSummaryResults(array_keys($rows));
		} else {
			$summary_values = array();
		}

		//TODO get results
		if($get_results && count($rows)>0 && count($time_ids)>0) {
			$sql = "SELECT kr_target , kr_actual , kr_timeid as time_id, kr_kpiid as id, kr_assurance_status as kr_assurance
					FROM ".$db->getDBRef()."_kpi_results
					WHERE kr_kpiid IN (".implode(",",array_keys($rows)).")"; //echo $sql;
			$res = $db->mysql_fetch_all_by_id2($sql, "id", "time_id");
			$results = array();
			foreach($res as $kpi_id => $rs) {
				$values = array('target'=>array(), 'actual'=>array());
				$calctype = $rows[$kpi_id]['kpi_calctype_id'];
				$targettype = $rows[$kpi_id]['kpi_targettype_id'];
				//foreach($rs as $t_id=>$tr) {
					$assurance = "N/A";
				foreach($time_ids as $t_id) {
					$tr = $rs[$t_id];
					$values['target'][$t_id] = $tr['kr_target'];
					$values['actual'][$t_id] = $tr['kr_actual'];
					$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, $t_id);
					$results[$kpi_id][$t_id] = $k_res;
					$results[$kpi_id][$t_id]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
					$results[$kpi_id][$t_id]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
					$results[$kpi_id][$t_id]['kr_assurance'] = $tr['kr_assurance'];
					if($tr['kr_target']>0 || $tr['kr_assurance']>0) {
						$assurance = $tr['kr_assurance'];
					}
				}
				$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, "ALL");
				$results[$kpi_id]["YTD"] = $k_res;
				$results[$kpi_id]["YTD"]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
				$results[$kpi_id]["YTD"]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
				$results[$kpi_id]['YTD']['kr_assurance'] = $this->getAssuranceText($assurance);
			}

		}
		foreach($rows as $i=>$r) {
			$r['group'] = $r[$fld_prefix.'_'.$group_field];
			if(isset($summary_values[$i])) {
				foreach($summary_values[$i] as $fld => $v) {
					$rows[$i][$fld] = $v;
					$r[$fld] = $v;
				}
			}
			foreach($data['head']['raw'] as $fld=>$h) {
				if($h['h_type']=="LIST" || $h['h_type']=="MULTILIST" || $h['h_type']=="OBJECT" || $h['h_type']=="MULTIOBJECT" || $h['h_type']=="SEGMENT" || $h['h_type']=="MULTISEGMENT") {
					$r[$fld] = isset($lists[$fld][$r[$fld]]) ? $lists[$fld][$r[$fld]] : $this->getUnspecified();
				} elseif($h['h_type']=="NUM") {
					$r[$fld] = $targetTypeObject->formatNumberBasedOnTargetType((isset($r[$fld]) ? $r[$fld] : 0),$r['target_type_id']);
				}
			}
			$data['objects'][$i] = $r;
			if($get_results && count($data['objects'])>0 && count($time_ids)>0) {
				$data['objects'][$i]['results'] = $results[$i];
			}
		}


		return $data;
	}
*/


	public function getGenericHeadings($modref, $main = true, $results = true, $object_type = "DEPTKPI", $group_field = false) {
		$head = $this->getHeadings($modref, $main, $results, $object_type, $group_field);
		$class_name = "SDBP6_".$object_type;
		/** @var SDBP6_DEPTKPI $myObject */
		$myObject = new $class_name($modref, 0, true);
		$fld_prefix = $myObject->getTableField();
		$results_fld_prefix = $myObject->getResultsTableField();
//		ASSIST_HELPER::arrPrint($head);
		$data = array();
		if(isset($head['main'])) {
			$data['main'] = array();
			foreach($head['main'] as $fld => $h) {
				if($fld == "ref") {
					$f2 = "sys_ref";
				} else {
					$f = explode("_", $fld);
					if($f[0] == $fld_prefix) {
						unset($f[0]);
					}
					$f2 = implode("_", $f);
				}
				$data['main'][$f2] = $h;
			}
		}
		if(isset($head['results'])) {
			$data['results'] = array();
			foreach($head['results'] as $fld => $h) {
				$f = explode("_", $fld);
				if($f[0] == $results_fld_prefix) {
					unset($f[0]);
				}
				$f2 = implode("_", $f);
				$data['results'][$f2] = $h;
			}
		}
		$data['raw'] = $head['raw'];
//		ASSIST_HELPER::arrPrint($data);
		return $data;
	}

	public function getHeadings($modref, $main = true, $results = true, $object_type = "DEPTKPI", $group_field = false) {
		$data = array();
		$headObj = new SDBP6_HEADINGS($modref);
		$head = $headObj->getFieldsForExternalModule(array(), $object_type, $group_field);
		if($main) {
			foreach($head as $hi => $h) {
				if($h['fld'] == "kpi_id" || $h['fld'] == "top_id") {
					$h['fld'] = "ref";
					$head[$hi]['fld'] = "ref";
				}
				$data['main'][$h['fld']] = $h['name'];
				$data['raw'][$h['fld']] = $h;
			}
		}

		if($results) {
			$head = $headObj->getResultsFieldsForExternalModule(array(), $object_type);
			foreach($head as $hi => $h) {
				$data['results'][$h['fld']] = $h['name'];
				$data['raw'][$h['fld']] = $h;
			}
		}
//ASSIST_HELPER::arrPrint($data);
		return $data;
	}


	public function getScoreHeadings($modref, $main = true, $results = true) {
		$db = new ASSIST_DB();
		$db->setDBRef($modref);

		$data = array();

		$sql = "SELECT section, field, h_client, h_type, h_table
				FROM `".$db->getDBRef()."_setup_headings_setup`
				INNER JOIN ".$db->getDBRef()."_setup_headings
				  ON h_id = head_id
				WHERE (
					section = 'KPI' AND field IN ('".implode("','", $this->scoring_heading_fields)."')
				) OR ( section = 'KPI_R' )";
		$head = $db->mysql_fetch_all_by_id($sql, "field");

		if($main) {
			$data['main']['ref'] = "Ref";
			foreach($this->scoring_heading_fields as $fld) {
				$data['main'][$fld] = $head[$fld]['h_client'];
				$data['raw'][$fld] = $head[$fld];
				unset($head[$fld]);
			}
		}

		if($results) {
			foreach($head as $fld => $h) {
				$data['results'][$fld] = $h['h_client'];
				$data['raw'][$fld] = $h;
			}
		}

		return $data;
	}


	public function getKPAs($modref, $merge = false, $primary = "", $field = "") {
		if($merge == true) {
			//returns $data['list] = array of all kpa values & $data['convert']['modref']['original kpa id'] = $data['list']['merged key']
			$db = new ASSIST_DB();
			$data = array();
			$list = array();
			$check = array();
			$flip = array();
			foreach($modref as $mrf) {
				$mr = $mrf['modref'];
				$db->setDBRef($mr);
				$sql = "SELECT * FROM ".$db->getDBRef()."_list_munkpa";
				$rows = $db->mysql_fetch_value_by_id($sql, "id", "value");
				$list[$mr] = $rows;
				$check[$mr] = array();
				foreach($list[$mr] as $i => $l) {
					$x = strtolower($l);
					$x = str_replace(" ", "", $x);
					$x = base64_encode($x);
					$check[$mr][$i] = $x;
				}
				$flip[$mr] = array_flip($check[$mr]);
			}
			$data['list'] = $list[$primary];
			$data['convert'] = array();
			$data['convert'][$primary] = array();
			foreach($data['list'] as $i => $l) {
				$data['convert'][$primary][$i] = $i;
			}
			foreach($check as $mr => $c) {
				if($mr != $primary) {
					foreach($c as $i => $l) {
						if(isset($flip[$primary][$l])) {
							$data['convert'][$mr][$i] = $flip[$primary][$l];
						} else {
							$data['list'][] = $list[$mr][$i];
							end($data['list']);
							$data['convert'][$mr][$i] = key($data['list']);
						}
					}
				}
			}
		} elseif(is_array($modref)) {
			//to be programmed
		} else {
			$db = new ASSIST_DB();
			$db->setDBRef($modref);

			$data = array();

			$sql = "SELECT * FROM ".$db->getDBRef()."_list_munkpa";
			$data = $db->mysql_fetch_value_by_id($sql, "id", "value");
		}
		return $data;
	}


	/*
		public function getProjects($var,$get_headings=true,$get_results=false,$time_ids=array()) {
			$modref = $var['modref'];
			$filter_type = $var['filter_type'];
			$filter_id = $var['filter_id'];

			$db = new ASSIST_DB();
			$db->setDBRef($modref);

			$data = array('head'=>array(),'objects'=>array());

			//GET HEADING DETAILS
			//if($get_headings) {
				$data['head'] = $this->getHeadings($var['primary_source'],true,false);
			//}

			//GET LISTS
			$lists = array();
			foreach($data['head']['raw'] as $fld=>$h) {
				if($h['h_type']=="LIST") {
					if($fld=="kpi_subid") {
						$sql = "SELECT S.id, IF(D.value=S.value, S.value, CONCAT(D.value,': ',S.value)) as value
								FROM ".$db->getDBRef()."_subdir S INNER JOIN ".$db->getDBRef()."_dir D ON D.id = S.dirid";
					} else {
						$sql = "SELECT * FROM ".$db->getDBRef()."_list_".$h['h_table'];
					}
					$l = $db->mysql_fetch_value_by_id($sql, "id", "value");
					$lists[$fld] = $l;
				}
			}

			//GET KPIS
			$sql = "SELECT cap_id as id, kpi_id as k_id
						, CONCAT('".$modref."/".SDBP5_CAPITAL::REFTAG."',cap_id) as ref
						, kpi_natkpaid, kpi_munkpaid, kpi_idpid, kpi_pdoid, kpi_value, kpi_unit, kpi_baseline, kpi_poe, kpi_ownerid, kpi_subid
						, kpi_calctype
						, kpi_targettype
						, cap_active
						, kpi_active as active
					FROM `".$db->getDBRef()."_capital`
					INNER JOIN ".$db->getDBRef()."_kpi
					ON kpi_capitalid = cap_id
					WHERE
					";
			switch($filter_type) {
				case "ID":
					$sql.=" kpi_capitalid IN (".implode(",",$filter_id).") ";
					break;
				case "sub":
					$sql.=" kpi_subid = ".$filter_id;
					break;
				default:
					$sql.=" kpi_ownerid = ".$filter_id;
					break;
			}
			$rows = $db->mysql_fetch_all_by_id($sql, "id");
			if($get_results && count($rows)>0 && count($time_ids)>0) {
				$k_id = array();
				foreach($rows as $r) { $k_id[$r['k_id']] = $r['id']; }
				$sql = "SELECT kr_target , kr_actual , kr_timeid as time_id, kr_kpiid as id , kr_assurance_status as kr_assurance
						FROM ".$db->getDBRef()."_kpi_results
						WHERE kr_kpiid IN (".implode(",",array_keys($k_id)).")"; //echo $sql;
				$res = $db->mysql_fetch_all_by_id2($sql, "id", "time_id");
				$results = array();
				foreach($res as $kid => $rs) {
					$kpi_id = $k_id[$kid];
					$values = array('target'=>array(), 'actual'=>array());
					$calctype = $rows[$kpi_id]['kpi_calctype'];
					$targettype = $rows[$kpi_id]['kpi_targettype'];
					//foreach($rs as $t_id=>$tr) {
						$assurance = "N/A";
					foreach($time_ids as $t_id) {
						$tr = $rs[$t_id];
						$values['target'][$t_id] = $tr['kr_target'];
						$values['actual'][$t_id] = $tr['kr_actual'];
						$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, $t_id);
						$results[$kpi_id][$t_id] = $k_res;
						$results[$kpi_id][$t_id]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
						$results[$kpi_id][$t_id]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
						if($tr['kr_target']>0 || $tr['kr_assurance']>0) {
							$assurance = $tr['kr_assurance'];
						}
					}
					$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, "ALL");
					$results[$kpi_id]["YTD"] = $k_res;
					$results[$kpi_id]["YTD"]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
					$results[$kpi_id]["YTD"]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
					$results[$kpi_id]["YTD"]['kr_assurance'] = $this->getAssuranceText($assurance);
				}

			}

			foreach($rows as $i=>$r) {
				$r['kpa'] = $r['kpi_munkpaid'];
				foreach($data['head']['raw'] as $fld=>$h) {
					if($h['h_type']=="LIST") {
						$r[$fld] = $lists[$fld][$r[$fld]];
					}
				}
				$data['objects'][$i] = $r;
				if($get_results && count($data['objects'])>0 && count($time_ids)>0) {
					$data['objects'][$i]['results'] = $results[$i];
				}

			}


			return $data;
		}
	*/

	public function getKPIIDFromCAP($cap_id, $modref) {
		$db = new ASSIST_DB();
		$db->setDBRef($modref);

		$sql = "SELECT kpi_id FROM ".$db->getDBRef()."_kpi WHERE kpi_proj_id = $cap_id ";
		$kpi_id = $db->mysql_fetch_one_value($sql, "kpi_id");
		return $kpi_id;
	}


	public function getTopKPIs($var, $get_headings = true, $get_results = false, $time_ids = array(), $primary_object_id = 0, $active_only = true, $start_time_id = 1) {
		return $this->getKPIsByType("TOP", $var, $get_headings, $get_results, $time_ids, $primary_object_id, $active_only, $start_time_id);
	}


	public function getKPIsByType($type, $var, $get_headings = true, $get_results = false, $time_ids = array(), $primary_object_id = 0, $active_only = true, $start_time_id = 1) {

		//private function getCapOpKPIs($type,$var,$get_headings=true,$get_results=false,$time_ids=array()) {
		//$object_type = $this->object_types[$type];
		$modref = $var['modref'];
		$filter_type = $var['filter_type'];
		$filter_id = $var['filter_id'];
		$group_field = $var['group_field'];
		$parent_object_id = isset($var['sdbip_id']) ? $var['sdbip_id'] : false;
		switch($type) {
			case "TOP":
				$db = new SDBP6_TOPKPI($modref, 0, true);
				$object_type = SDBP6_TOPKPI::OBJECT_TYPE;
				$time_type = $db->getTimeType();
				$filter = "";
				break;
			case "KPI":
			case "PROJ":
				$db = new SDBP6_DEPTKPI($modref, 0, true);
				$object_type = SDBP6_DEPTKPI::OBJECT_TYPE;
				$filter = "kpi_cap_op = ".($type == "KPI" ? 0 : 1)." AND ";
				$time_type = $db->getTimeType();
				break;
		}
		$fld_prefix = $db->getTableField();
		$results_fld_prefix = $db->getResultsTableField();
		$targetTypeObject = new SDBP6_SETUP_TARGETTYPE($modref);
		$timeObject = new SDBP6_SETUP_TIME($modref);
		//Get all time periods regardless of time_type otherwise results processing won't work for Quarterly targets - use time_increment to skip months where applicable #AA-314 [JC] 24 Jan 2020
		$original_times = $timeObject->getActiveTimeObjectsFormattedForSelect($parent_object_id, "MONTH");
		$original_times_keys = array_keys($original_times);
		if($time_type == "QUARTER") {
			$time_increment = 3;
		} else {
			$time_increment = 1;
		}
//		echo $time_type;
//		ASSIST_HELPER::arrPrint($original_times);
//		ASSIST_HELPER::arrPrint($original_times_keys);
//		$db->setDBRef($modref);
//die();

		$data = array('head' => array(), 'objects' => array(), 'grouping' => array());

		//GET HEADING DETAILS
//		if($get_headings) {
		$data['head'] = $this->getHeadings($var['primary_source'], true, false, $object_type, $group_field);
//		}

		//GET LISTS
		$lists = array();
		$grouping = array();
		foreach($data['head']['raw'] as $fld => $h) {
			$grouping_check = false;
			//get list items
			switch($h['field_type']) {
				case "MULTILIST":
				case "LIST":
					$grouping_check = true;
					//get list items if not already available
					if(!isset($lists[$fld])) {
						$listObject = new SDBP6_LIST($h['h_table'], $modref);
						$l = $listObject->getAllListItemsFormattedForSelect();
						unset($listObject);
						//restore coding removed in previous function so that it can be saved to the DB in PM6 - [AA-535] JC
						foreach($l as $i => $j) {
							$l[$i] = ASSIST_HELPER::code($j);
						}
						$lists[$fld] = $l;
					}
					break;
				case "MULTIOBJECT":
				case "OBJECT":
					$grouping_check = true;
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$list_object_name = $h['h_table'];
						$extra_info = array();
						if(strpos($list_object_name, "|") !== false) {
							$lon = explode("|", $list_object_name);
							$list_object_name = $lon[0];
							$extra_info = $lon[1];
						}
						if($list_object_name == "SDBP6_TOPKPI" || $list_object_name == "SDBP6_PROJECT") {
							$listObject = new $list_object_name($modref, 0, true);
						} else {
							$listObject = new $list_object_name($modref);
						}
						/** @var SDBP6_TOPKPI $listObject */
						$l = $listObject->getActiveObjectsFormattedForSelect($extra_info);    //checked and confirmed no coding happening in the function so items are coming back raw and able to be saved straight to the DB in PM6  [AA-535]
						unset($listObject);
						$lists[$fld] = $l;
					}
					break;
				case "MULTISEGMENT":
				case "SEGMENT":
					$grouping_check = true;
					//get list items if not already available
					if(!isset($all_my_lists[$fld])) {
						$list_object_type = $h['h_table'];
						$listObject = new SDBP6_SEGMENTS($list_object_type);
						$l = $listObject->getAllListItemsFormattedForSelect(false);    //checked and confirmed no coding happening in the function so items are coming back raw and able to be saved straight to the DB in PM6  [AA-535]
						unset($listObject);
						$lists[$fld] = $l;
					}
					break;
			} //end switch by type
			if($grouping_check) {
				//check if grouping field
				$f = explode("_", $fld);
				unset($f[0]);
				$fld2 = implode("_", $f);
				if($group_field == $fld2) {
					$grouping = $l;
				}
			}
		}
		$data['grouping'] = $grouping;

		//GET KPIS
		$sql = "SELECT *, ".$db->getIDFieldName()." as id, ".$db->getTargetTypeTableField()." as target_type_id, ".$db->getCalcTypeTableField()." as calc_type_id
					, CONCAT('".$modref."/".$db->getRefTag()."',".$db->getIDFieldName().") as ref
					, IF((".$db->getStatusFieldName()." & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE.", 1, 0) as active
					, 1 as cap_active
				FROM `".$db->getTableName()."`
				WHERE $filter
				".($parent_object_id !== false ? " ".$db->getParentFieldName()." = ".$parent_object_id." AND " : "")."
				";
		switch($filter_type) {
			case "ID":
				$sql .= " ".$fld_prefix."_id IN (".implode(",", $filter_id).") ";
				break;
			case "sub":
				$sql .= " ".$fld_prefix."_sub_id = ".$filter_id;
				break;
			default:
				$sql .= " ".$fld_prefix."_owner_id = ".$filter_id;
				break;
		}
		//dev testing for specific KPI - #AA-314
//			$sql.=" AND ".$db->getIDFieldName()." = 2";
		$rows = $db->mysql_fetch_all_by_id($sql, "id");
		//Check for deleted log records #AA-568 JC
		if(count($rows) > 0 && !$active_only) {
			$sql = "SELECT log_object_id as object_id, log_insertdate as log_date FROM ".$db->getFullLogTableName()." WHERE (log_log_type = '".SDBP6_LOG::DELETE."' OR log_log_type = '".SDBP6_LOG::DEACTIVATE."') AND log_object_id IN (".implode(",", array_keys($rows)).") ORDER BY log_insertdate ASC";
			$deleted_logs = $db->mysql_fetch_all_by_id($sql, "object_id");
		} else {
			$deleted_logs = array();
		}

		//if total columns exist then get the summary
		if(isset($data['head']['main']['original_total']) || isset($data['head']['main']['revised_total']) || isset($data['head']['main']['actual_total'])) {
			$summary_values = $db->getSummaryResults(array_keys($rows), $parent_object_id);
		} else {
			$summary_values = array();
		}
		//get results
		if($get_results && count($rows) > 0 && count($time_ids) > 0) {
			$calcObject = new SDBP6_SETUP_CALCTYPE($this->modref);
			$this->result_settings = $db->getResultOptions($modref);
			$result_options = $this->result_settings;
			$sql = "SELECT ".$db->getResultsRevisedFieldName()." as target , ".$db->getResultsActualFieldName()." as actual 
						, ".$db->getResultsTimeFieldName()." as time_id, ".$db->getResultsParentFieldName()." as id
						, ".$results_fld_prefix."_assurance as assurance
					FROM ".$db->getResultsTableName()."
					WHERE ".$db->getResultsParentFieldName()." IN (".implode(",", array_keys($rows)).")"; //echo $sql;
			$res = $db->mysql_fetch_all_by_id2($sql, "id", "time_id");

			$results = array();
			foreach($res as $kpi_id => $rs) {  //ASSIST_HELPER::arrPrint($rs);
				$values = array('target' => array(), 'actual' => array());
				$pua_values = array('target' => array(), 'actual' => array());
				$calctype = $rows[$kpi_id]['calc_type_id'];
				$targettype = $rows[$kpi_id]['target_type_id'];
				$assurance = "N/A";
				$calculate_pua = false;
				foreach($time_ids as $t => $raw_t_id) {
					// #AA-584 - Include PUA (period-under-assessment) results if not starting at period 1
					//if($start_time_id>1 && $raw_t_id>=$start_time_id) {
					$calculate_pua = true;
					//}
					// #AA-314 - SDBP6 TL Quarterly targets not being processed correctly after the first quarter [JC] 24 Jan 2020
					if(($t + 1) % $time_increment !== 0) {
						//echo "<h4 class=red>Time period not applicable - am I processing a Quarterly target maybe?  Either way... SKIP ME!!!</h4>"; // #AA-314
					} else {
						//echo "<h4 class='green'>Time period all clear.  Okay to proceed</h4>"; // #AA-314
						$t_id = $original_times_keys[$raw_t_id - 1]; //echo "<h4>".$t_id."</h4>";
						$tr = isset($rs[$t_id]) ? $rs[$t_id] : array('target' => 0, 'actual' => 0, 'assurance' => 0);
						$values['target'][$raw_t_id] = $tr['target'];
						$values['actual'][$raw_t_id] = $tr['actual'];
						if($calculate_pua) {
							$pua_values['target'][$raw_t_id] = $tr['target'];
							$pua_values['actual'][$raw_t_id] = $tr['actual'];
						}
						$res_values = $calcObject->calculatePTDValues($calctype, $values['target'], $values['actual'], $result_options);
						$k_res = $res_values;
						$result_id = $calcObject->calculatePeriodResult($calctype, $res_values['target'], $res_values['actual'], $result_options);
						$k_res['r'] = $result_options[$result_id];
						//						$k_res = $calcObject->calculatePeriodResult($calctype,$values['target'],$values['actual'],$result_options);//->KPIcalcResult($values, $calctype, $time_ids, $raw_t_id);
						//ASSIST_HELPER::arrPrint($k_res);
						$results[$kpi_id][$raw_t_id] = $k_res;
						$results[$kpi_id][$raw_t_id]['target'] = $targetTypeObject->formatNumberBasedOnTargetType($k_res['target'], $targettype);//$this->formatResult($k_res['target'], $targettype);
						$results[$kpi_id][$raw_t_id]['actual'] = $targetTypeObject->formatNumberBasedOnTargetType($k_res['actual'], $targettype);//$this->formatResult($k_res['actual'], $targettype);
						$results[$kpi_id][$raw_t_id]['assurance'] = $tr['assurance'];
						if($tr['target'] > 0 || $tr['assurance'] > 0) {
							$assurance = $tr['assurance'];
						}
					}
				}

				if($calculate_pua) {
					$res_values = $calcObject->calculatePTDValues($calctype, $pua_values['target'], $pua_values['actual'], $result_options);
					$k_res = $res_values;
					$result_id = $calcObject->calculatePeriodResult($calctype, $res_values['target'], $res_values['actual'], $result_options);
					$k_res['r'] = $result_options[$result_id];
					$results[$kpi_id]["PUA"] = $k_res;
					$results[$kpi_id]["PUA"]['target'] = $targetTypeObject->formatNumberBasedOnTargetType($k_res['target'], $targettype);//$this->formatResult($k_res['target'], $targettype);
					$results[$kpi_id]["PUA"]['actual'] = $targetTypeObject->formatNumberBasedOnTargetType($k_res['actual'], $targettype);;//$this->formatResult($k_res['actual'], $targettype);
					$results[$kpi_id]['PUA']['assurance'] = $this->getAssuranceText($assurance);
				}
//					$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, "ALL");
				$res_values = $calcObject->calculatePTDValues($calctype, $values['target'], $values['actual'], $result_options);
				$k_res = $res_values;
				$result_id = $calcObject->calculatePeriodResult($calctype, $res_values['target'], $res_values['actual'], $result_options);
				$k_res['r'] = $result_options[$result_id];
//					ASSIST_HELPER::arrPrint($k_res);
				$results[$kpi_id]["YTD"] = $k_res;
				$results[$kpi_id]["YTD"]['target'] = $targetTypeObject->formatNumberBasedOnTargetType($k_res['target'], $targettype);//$this->formatResult($k_res['target'], $targettype);
				$results[$kpi_id]["YTD"]['actual'] = $targetTypeObject->formatNumberBasedOnTargetType($k_res['actual'], $targettype);;//$this->formatResult($k_res['actual'], $targettype);
				$results[$kpi_id]['YTD']['assurance'] = $this->getAssuranceText($assurance);
			}
//				ASSIST_HELPER::arrPrint($results[$kpi_id]);
//break;
		}
//echo "<hr />";
//ASSIST_HELPER::arrPrint($results);
		foreach($rows as $i => $r) {
			$r['group'] = $r[$fld_prefix.'_'.$group_field];
			//check if item is marked as deleted & record deleted date if that is the case (default to now if date not available) #AA-568
			if($r['active'] == false) {
				$r['is_deleted'] = true;
				$r['deleted_date'] = isset($deleted_logs[$i]['log_date']) ? $deleted_logs[$i]['log_date'] : date("Y-m-d H:i:s");
			} else {
				$r['deleted_date'] = false;
				$r['is_deleted'] = false;
			}
			if(isset($summary_values[$i])) {
				foreach($summary_values[$i] as $fld => $v) {
					$rows[$i][$fld] = $v;
					$r[$fld] = $v;
				}
			}
			foreach($data['head']['raw'] as $fld => $h) {
				if($h['h_type'] == "LIST" || $h['h_type'] == "MULTILIST" || $h['h_type'] == "OBJECT" || $h['h_type'] == "MULTIOBJECT" || $h['h_type'] == "SEGMENT" || $h['h_type'] == "MULTISEGMENT") {
					$r[$fld] = isset($lists[$fld][$r[$fld]]) ? $lists[$fld][$r[$fld]] : $this->getUnspecified();
				} elseif($h['h_type'] == "NUM") {
					$r[$fld] = $targetTypeObject->formatNumberBasedOnTargetType((isset($r[$fld]) ? $r[$fld] : 0), $r['target_type_id']);
				}
			}
			$data['objects'][$i] = $r;
			if($get_results && count($data['objects']) > 0 && count($time_ids) > 0) {
				$data['objects'][$i]['results'] = $results[$i];
			}
		}

//ASSIST_HELPER::arrPrint($data);
		return $data;
	}

	/*
	$object_type = $this->object_types['TOP'];
	$modref = $var['modref'];
	$filter_type = $var['filter_type'];
	$filter_id = $var['filter_id'];
	$group_field = $var['group_field'];
	$db = new SDBP6_TOPKPI($modref,0,true);
	$targetTypeObject = new SDBP6_SETUP_TARGETTYPE($modref);
//		$db->setDBRef($modref);
//die();

	$data = array('head'=>array(),'objects'=>array(),'grouping'=>array());

	//GET HEADING DETAILS
//		if($get_headings) {
	$data['head'] = $this->getHeadings($var['primary_source'],true,false,$object_type,$group_field);
//		}

	//GET LISTS
	$lists = array();
	$grouping = array();
	foreach($data['head']['raw'] as $fld=>$h) {
		$grouping_check = false;
		//get list items
		switch($h['field_type']) {
			case "MULTILIST":
			case "LIST": $grouping_check = true;
				//get list items if not already available
				if(!isset($lists[$fld])) {
					$listObject = new SDBP6_LIST($h['h_table'],$modref);
					$l = $listObject->getAllListItemsFormattedForSelect();
					unset($listObject);
					$lists[$fld] = $l;
				}
				break;
			case "MULTIOBJECT":
			case "OBJECT": $grouping_check = true;
				//get list items if not already available
				if(!isset($all_my_lists[$fld])) {
					$list_object_name = $h['h_table'];
					$extra_info = array();
					if(strpos($list_object_name,"|")!==false) {
						$lon = explode("|",$list_object_name);
						$list_object_name = $lon[0];
						$extra_info = $lon[1];
					}
					$listObject = new $list_object_name($modref);
					$l = $listObject->getActiveObjectsFormattedForSelect($extra_info);
					unset($listObject);
					$lists[$fld] = $l;
				}
				break;
			case "MULTISEGMENT":
			case "SEGMENT": $grouping_check = true;
				//get list items if not already available
				if(!isset($all_my_lists[$fld])) {
					$list_object_type = $h['h_table'];
					$listObject = new SDBP6_SEGMENTS($list_object_type);
					$l = $listObject->getAllListItemsFormattedForSelect(false);
					unset($listObject);
					$lists[$fld] = $l;
				}
				break;
		} //end switch by type
		if($grouping_check) {
			//check if grouping field
			$f = explode("_",$fld);
			unset($f[0]);
			$fld2 = implode("_",$f);
			if($group_field==$fld2) {
				$grouping = $l;
			}
		}
	}
	$data['grouping'] = $grouping;

	//GET KPIS
	$sql = "SELECT *, top_id as id
				, CONCAT('".$modref."/".$db->getRefTag()."',top_id) as ref
				, IF((top_status & ".SDBP6::ACTIVE.") = ".SDBP6::ACTIVE.", 1, 0) as active
				, 1 as cap_active
			FROM `".$db->getDBRef()."_top`
			WHERE
			";
	switch($filter_type) {
		case "ID":
			$sql.=" top_id IN (".implode(",",$filter_id).") ";
			break;
		case "sub":
			$sql.=" top_sub_id = ".$filter_id;
			break;
		default:
			$sql.=" top_owner_id = ".$filter_id;
			break;
	}
	$rows = $db->mysql_fetch_all_by_id($sql, "id");
	if($get_results && count($rows)>0 && count($time_ids)>0) {
		$sql = "SELECT kr_target , kr_actual , kr_timeid as time_id, kr_kpiid as id, kr_assurance_status as kr_assurance
				FROM ".$db->getDBRef()."_kpi_results
				WHERE kr_kpiid IN (".implode(",",array_keys($rows)).")"; //echo $sql;
		$res = $db->mysql_fetch_all_by_id2($sql, "id", "time_id");
		$results = array();
		foreach($res as $kpi_id => $rs) {
			$values = array('target'=>array(), 'actual'=>array());
			$calctype = $rows[$kpi_id]['kpi_calctype_id'];
			$targettype = $rows[$kpi_id]['kpi_targettype_id'];
			//foreach($rs as $t_id=>$tr) {
			$assurance = "N/A";
			foreach($time_ids as $t_id) {
				$tr = $rs[$t_id];
				$values['target'][$t_id] = $tr['kr_target'];
				$values['actual'][$t_id] = $tr['kr_actual'];
				$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, $t_id);
				$results[$kpi_id][$t_id] = $k_res;
				$results[$kpi_id][$t_id]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
				$results[$kpi_id][$t_id]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
				$results[$kpi_id][$t_id]['kr_assurance'] = $tr['kr_assurance'];
				if($tr['kr_target']>0 || $tr['kr_assurance']>0) {
					$assurance = $tr['kr_assurance'];
				}
			}
			$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, "ALL");
			$results[$kpi_id]["YTD"] = $k_res;
			$results[$kpi_id]["YTD"]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
			$results[$kpi_id]["YTD"]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
			$results[$kpi_id]['YTD']['kr_assurance'] = $this->getAssuranceText($assurance);
		}

	}
	foreach($rows as $i=>$r) {
		$r['group'] = $r['top_'.$group_field];
		foreach($data['head']['raw'] as $fld=>$h) {
			if(isset($r[$fld])) {
				if($h['h_type']=="LIST" || $h['h_type']=="MULTILIST" || $h['h_type']=="OBJECT" || $h['h_type']=="MULTIOBJECT" || $h['h_type']=="SEGMENT" || $h['h_type']=="MULTISEGMENT") {
					$r[$fld] = isset($lists[$fld][$r[$fld]]) ? $lists[$fld][$r[$fld]] : $this->getUnspecified();
				} elseif($h['h_type']=="NUM") {
					$r[$fld] = $targetTypeObject->formatNumberBasedOnTargetType($r[$fld],$r[$db->getTargetTypeTableField()]);
				}
			}
		}
		$data['objects'][$i] = $r;
		if($get_results && count($data['objects'])>0 && count($time_ids)>0) {
			$data['objects'][$i]['results'] = $results[$i];
		}
	}

	return $data;
}

*/

	/*
		public function getTopKPIs($var,$get_headings=true,$get_results=false,$time_ids=array()) {
			$modref = $var['modref'];
			$filter_type = $var['filter_type'];
			$filter_id = $var['filter_id'];

			//change time_ids to only reflect quarters
			foreach($time_ids as $key => $t_id) {
				if(round($t_id/3)!=($t_id/3)) {
					unset($time_ids[$key]);
				}
			}

			$db = new ASSIST_DB();
			$db->setDBRef($modref);

			$data = array('head'=>array(),'objects'=>array());

			//GET HEADING DETAILS
			//if($get_headings) {
				$data['head'] = $this->getHeadings($var['primary_source'],true,false);
			//}

			//GET LISTS
			$lists = array();
			foreach($data['head']['raw'] as $fld=>$h) {
				if($h['h_type']=="LIST") {
					if($fld=="kpi_subid") {
						$sql = "SELECT D.id, D.value as value
								FROM ".$db->getDBRef()."_dir D ";
					} else {
						$sql = "SELECT * FROM ".$db->getDBRef()."_list_".$h['h_table'];
					}
					$l = $db->mysql_fetch_value_by_id($sql, "id", "value");
					$lists[$fld] = $l;
				}
			}

			//GET KPIS
			$sql = "SELECT top_id as id
						, CONCAT('".$modref."/".SDBP5_TOP::REFTAG."',top_id) as ref
						, top_natkpaid as kpi_natkpaid
						, top_munkpaid as kpi_munkpaid
						, top_idp as kpi_idpid
						, top_pdoid as kpi_pdoid
						, top_value as kpi_value
						, top_unit as kpi_unit
						, top_baseline as kpi_baseline
						, top_poe as kpi_poe
						, top_ownerid as kpi_ownerid
						, top_dirid as kpi_subid
						, top_calctype as kpi_calctype
						, top_targettype as kpi_targettype
						, top_active as active
						, 1 as cap_active
					FROM `".$db->getDBRef()."_top`
					WHERE
					";
			switch($filter_type) {
				case "ID":
					$sql.=" top_id IN (".implode(",",$filter_id).") ";
					break;
				case "sub":
					$sql.=" top_dirid = ".$filter_id;
					break;
				default:
					$sql.=" top_ownerid = ".$filter_id;
					break;
			}
			$rows = $db->mysql_fetch_all_by_id($sql, "id");
			if($get_results && count($rows)>0 && count($time_ids)>0) {
				$sql = "SELECT tr_target as kr_target , tr_actual as kr_actual, tr_timeid as time_id, tr_topid as id, 0 as kr_assurance
						FROM ".$db->getDBRef()."_top_results
						WHERE tr_topid IN (".implode(",",array_keys($rows)).")"; //echo $sql;
				$res = $db->mysql_fetch_all_by_id2($sql, "id", "time_id");
				$results = array();
				foreach($res as $kpi_id => $rs) {
					$values = array('target'=>array(), 'actual'=>array());
					$calctype = $rows[$kpi_id]['kpi_calctype'];
					$targettype = $rows[$kpi_id]['kpi_targettype'];
					//foreach($rs as $t_id=>$tr) {
						$assurance = "N/A";
					foreach($time_ids as $t_id) {
						$tr = $rs[$t_id];
						$values['target'][$t_id] = $tr['kr_target'];
						$values['actual'][$t_id] = $tr['kr_actual'];
						$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, $t_id);
						$results[$kpi_id][$t_id] = $k_res;
						$results[$kpi_id][$t_id]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
						$results[$kpi_id][$t_id]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
						$results[$kpi_id][$t_id]['kr_assurance'] = $tr['kr_assurance'];
						if($tr['kr_target']>0 || $tr['kr_assurance']>0) {
							$assurance = $tr['kr_assurance'];
						}
					}
					$k_res = $this->KPIcalcResult($values, $calctype, $time_ids, "ALL");
					$results[$kpi_id]["YTD"] = $k_res;
					$results[$kpi_id]["YTD"]['kr_target'] = $this->formatResult($k_res['target'], $targettype);
					$results[$kpi_id]["YTD"]['kr_actual'] = $this->formatResult($k_res['actual'], $targettype);
					$results[$kpi_id]['YTD']['kr_assurance'] = "";//$this->getAssuranceText($assurance);
				}

			}
			foreach($rows as $i=>$r) {
				$r['kpa'] = $r['kpi_munkpaid'];
				foreach($data['head']['raw'] as $fld=>$h) {
					if($h['h_type']=="LIST") {
						$r[$fld] = $lists[$fld][$r[$fld]];
					}
				}
				$data['objects'][$i] = $r;
				if($get_results && count($data['objects'])>0 && count($time_ids)>0) {
					$data['objects'][$i]['results'] = $results[$i];
				}
			}


			return $data;
		}

	*/


	/******************************
	 * Functions for drawing KPI details
	 */
	/*
	public function getKPIDetailDisplay($id,$is_capital_project=false) {
		$data = array('display'=>"",'js'=>"",'kpi_id'=>$id);



		//change $data['kpi_id'] if is_capital_project=true
		return $data;
	}

	public function getKPIResultsDisplay($id) {
		$data = array('display'=>"",'js'=>"");




		return $data;
	}

	public function getCAPDetailDisplay($id) {
		$data = array('display'=>"",'js'=>"");



		return $data;
	}

	public function getCAPResultsDisplay($id) {
		$data = array('display'=>"",'js'=>"");




		return $data;
	}


	*/


	private function formatResult($val, $tt, $decimal = 2) {
		switch($tt) {
			case 1:
			case "currency":
				return "R ".number_format($val, $decimal);
				break;
			case 2:
			case "perc":
			case "%":
				return number_format($val, $decimal)."%";
				break;
			case 3:
			case "number":
				return number_format($val, $decimal);
				break;
			default:
				return $val;
				break;
		}
	}


	/***
	 * function KPIcalcResult(
	 *        array of all targets/actuals,
	 *        calctype,
	 *        time period range,
	 *        current time id
	 * )
	 */
//	private function KPIcalcResult($values,$ct,$filter,$t) {
//		$calcObject = new SDBP6_SETUP_CALCTYPE($this->modref);
//	}
	/*$result_settings = $this->result_settings;
	//$result = array('r'=>0,'style'=>"result0",'text'=>"N/A",'target'=>0,'actual'=>0);
	$result = $result_settings[0];
	/* determine target & actual to use in calculating result */
	/*$targ = 0; $actl = 0;
	$tc = 0; $targsum=0; $actlsum=0;
	if($t=="ALL") {		//calculate overall result
		switch($ct) {
			case "CO":
				foreach($values['target'] as $i => $vt) {
					$va = $values['actual'][$i];
					if($targ < ($vt*1)) { $targ = $vt; }
					if($actl < ($va*1)) { $actl = $va; }
				}
				break;
			case "ACC":
			case "ZERO":
				$targ = array_sum($values['target']);
				$actl = array_sum($values['actual']);
				break;
			case "STD":
			case "REV":
				//$targ = array_sum($values['target']);		$targsum = $targ;
				//$actl = array_sum($values['actual']);		$actlsum = $actl;
				$tc = 0;
				foreach($values['target'] as $i => $vt) {
					if(($vt*1)>0) { $tc++; }
					$targ+=($vt*1);
					$actl+=($values['actual'][$i]*1);
				}
				$targsum = $targ;
				$actlsum = $actl;
				if($tc == 0) { $tc = 1; }
				$targ/=$tc;
				$actl/=$tc;
				break;
			case "LAST":
			case "LASTREV":
				foreach($values['target'] as $i => $vt) {
					$va = $values['actual'][$i];
					if(($vt*1)>0) { $targ = $vt; }
					if(($va*1)>0) { $actl = $va; }
				}
				break;
			case "NA":
				break;
		}
	} else {
				$targ = $values['target'][$t];
				$actl = $values['actual'][$t];
	}
	$targ*=1;	$actl*=1;
	/* compare targ & actl to determine result value */
	/*switch($ct) {
		case "CO":
		case "STD":
		case "ACC":
		case "LAST":
			if($targ!= 0 || $actl != 0) {
				if($targ==0) {
					//$result['r']=5;	$result['style']="result5";	$result['text']="B";
					$result = $result_settings[5];
				} else {
					$div = $actl/$targ;
					if($div<0.75) {
						$result = $result_settings[1];
						//$result['r']=1;	$result['style']="result1";	$result['text']="R";
					} elseif($div < 1) {
						$result = $result_settings[2];
						//$result['r']=2;	$result['style']="result2";	$result['text']="O";
					} elseif($div==1) {
						$result = $result_settings[3];
						//$result['r']=3;	$result['style']="result3";	$result['text']="G";
					} elseif($div < 1.5) {
						$result = $result_settings[4];
						//$result['r']=4;	$result['style']="result4";	$result['text']="G2";
					} elseif($div >= 1.5) {
						$result = $result_settings[5];
						//$result['r']=5;	$result['style']="result5";	$result['text']="B";
					}
				}
			}
			break;
		case "REV":
		case "ZERO":
		case "LASTREV":
			if($ct=="ZERO" || (in_array($ct,array("REV","LASTREV")) && ($targ!=0 || $actl!=0))) {
				if($actl < $targ) {
					$result = $result_settings[5];
					//$result['r']=5;	$result['style']="result5";	$result['text']="B";
				} elseif($actl==$targ) {
					$result = $result_settings[3];
					//$result['r']=3;	$result['style']="result3";	$result['text']="G";
				} else {
					$result = $result_settings[1];
					//$result['r']=1;	$result['style']="result1";	$result['text']="R";
				}
			}
			break;
		case "NA":
			break;
	}
	$result['target'] = $targ;
	$result['actual'] = $actl;
	$result['tc'] = $tc;
	$result['sum'] = array('target'=>$targsum,'actual'=>$actlsum);
	$result['values'] = $values;
	return $result;*/
	//}


}


?>