<?php
/**
 * From calling page
 * @var array $list_items2
 * From inc_header
 * @var SDBP6 $helper
 * @var SDBP6_SDBIP $sdbipObject
 * @var SDBP6_HEADINGS $headingObject
 * @var SDBP6_DISPLAY $displayObject
 * @var int $sdbip_id
 */
require_once("inc_header.php");

//Setting the url of the page sans queries
//$noparams = explode("&",$_SERVER['REQUEST_URI']);
//$pageonly = explode("/",$noparams[0]);
//$thispage = $pageonly[2];

$list_id = isset($_REQUEST['l']) ? $_REQUEST['l'] : "error";
if($list_id == "error") {
	$list_id = isset($_REQUEST['list']) ? $_REQUEST['list'] : "error";
}
if($list_id == "error") {
	$route = "menu_setup_sdbip_defaults";
	$menu_text = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route), "|"));
	ASSIST_HELPER::displayResult(array("error", "An error occurred while trying to determine which list you are trying to access.  Please go back to $menu_text and try again."));
	die();
}
$thispage = "setup_sdbip_defaults_lists.php?l=".$list_id;
//echo $list_id;
$listObj = new SDBP6_LIST($list_id);
$listObj->setSDBIPID($sdbip_id);
//echo $listObj->getListTable();
$fields = $listObj->getFieldNames();
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
$items = $listObj->getListTable();
$list_data = $listObj->getListItemsForSetup($sdbip_id);

//remove sdbip_id from columns - already displayed in page heading
if(isset($fields['sdbip_id'])) {
	unset($fields['sdbip_id']);
}

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

if($list_id == "strategic_objective") {
	ASSIST_HELPER::displayResult(array("info", "This list will be automatically populated if the ".$helper->getObjectName("SDBIP")." is linked to a(n) ".$helper->getObjectName("IDP")."."));
}

?>
<?php

?>
<div class="horscroll">
	<table class='tbl-container not-max'>
		<tr>
			<td>
				<span class=float style='margin-bottom: 5px'><button id=btn_import><?php echo $helper->getActivityName("IMPORT"); ?></button></span>
				<h2><span id=list_title><?php echo $helper->replaceAllNames($headingObject->getAListHeading($list_id)); ?></span></h2>
				<form name=frm_list>
					<input type="hidden" id="sdbip_id" name="sdbip_id" value="<?php echo $sdbip_id; ?>" />
					<input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id; ?>" />
					<input type="hidden" id="sort" name="sort" value="99" />
					<table class=list id=master_list>
						<thead>
						<tr><?php
							foreach($fields as $fld => $val) {
								echo "<th>".$val.(isset($required_fields[$fld]) && $required_fields[$fld] == true ? "*" : "")."</th>";
							}
							?>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php
						//Add list
						echo "<tr id=\"add_row\">";
						foreach($fields as $fld => $name) {
							echo "<td >";
							//echo $fld;
							if($fld == "status") {
								echo "<div class=center>Active</div><input type=\"hidden\" name='$fld' id='$fld' value=".SDBP6::ACTIVE." />";
							} else if($types[$fld] == "LIST") {
								$js .= $displayObject->drawFormField($types[$fld], array('id' => $fld, 'name' => $fld, 'class' => "list_num", 'options' => $list_items2, 'require_me' => true, 'unspecified' => false));
							} else if($fld == "colour") {
								echo "<input type=\"hidden\" name='$fld' id=\"add_colour\" value=\"#FFFFFF\" />";
								$js .= $displayObject->drawFormField($types[$fld], array('id' => 'clr_'.$fld, 'name' => $fld, 'require_me' => (!isset($required_fields[$fld]) || $required_fields[$fld] == true ? "1" : "0")));
							} elseif($types[$fld] == "BOOL_BUTTON") {
								$js .= $displayObject->drawFormField($types[$fld], array('id' => $fld, 'name' => $fld, 'require_me' => (!isset($required_fields[$fld]) || $required_fields[$fld] == true ? "1" : "0"), 'form' => "vertical"));
							} else {
								$js .= $displayObject->drawFormField($types[$fld], array('id' => $fld, 'name' => $fld, 'require_me' => (!isset($required_fields[$fld]) || $required_fields[$fld] == true ? "1" : "0")));
							}
							echo "</td>";

						}
						echo "<td class=center><button id=btn_add>Add</button></td></tr>";

						?>
						<?php
						foreach($list_data as $key => $val) {
							echo "<tr id=\"tr_".$val['id']."\" ".((($val['status'] & SDBP6::INACTIVE) == SDBP6::INACTIVE) ? "class=inact" : "")." >";
							foreach($fields as $fld => $head) {
								$valkey = $fld;
								$valval = $val[$fld];
								switch($valkey) {
									case "sort":
										break;
									case "status":
										if(($valval & SDBP6::SYSTEM_DEFAULT) == SDBP6::SYSTEM_DEFAULT) {
											echo "<td sys=1 class=\"center\">System Default";
										} else if(($valval & SDBP6::ACTIVE) == SDBP6::ACTIVE) {
											echo "<td class=\"center\">Active";
										} else if(($valval & SDBP6::INACTIVE) == SDBP6::INACTIVE) {
											echo "<td class=\"center\">Inactive";
										} else {
											echo "<td class=\"center\">";
										}

										echo "</td>";
										break;
									case "dir_id":
									case "function_id":
										if(isset($list_items[$valval])) {
											echo "<td fld=\"".$valkey."\">".($list_items[$valval]['name'])."</td>";
										} else {
											echo "<td fld=\"".$valkey."\">".$helper->getUnspecified()."</td>";
										}
										break;
									case "core":
										echo "<td class=center fld=\"".$valkey."\">";
										$displayObject->drawBoolForDisplay($valval);
										echo "</td>";
										break;
									case "colour":
										echo "<td style=\"background-color:".$valval."\" fld=\"".$valkey."\"></td>";
										break;
									default:
										echo "<td fld=\"".$valkey."\">".ASSIST_HELPER::decode($valval)."</td>";
									//$js .=" rows['".$val['id']."']['". $valkey ."'] = ". $valval ." ; ";
								}
							}
							if(($val['status'] & SDBP6::SYSTEM_DEFAULT) == SDBP6::SYSTEM_DEFAULT) {
								echo "<td></td>";
							} elseif((($val['status'] & SDBP6::ACTIVE) == SDBP6::ACTIVE)) {
								echo "<td class=center><button id='".$val['id']."' class='btn_edit' can_delete='".($val['can_delete'] == true ? 1 : 0)."' >Edit</button></td>";
							} else {
								echo "<td class=center><button id='".$val['id']."' class='btn_restore'>Restore</button></td>";
							}
							echo "</tr>";
						}
						?>

						</tbody>
					</table>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_sdbip_defaults.php'), "list", array('section' => $list_id, 'sdbip_id' => $sdbip_id)); ?>
			</td>
		</tr>
	</table>
</div>
<div id=div_dialog title="Edit">
	<h1>Edit form</h1>
	<form name=edit_frm>
		<input type="hidden" name="sdbip_id" id="sdbip_id" value='<?php echo $sdbip_id; ?>' />
		<input type="hidden" name="id" id="ref" value='0' />
		<input type="hidden" name="list_id" id="list_id" value='<?php echo $list_id; ?>' />
		<table id=ed_frm class=form>
			<?php

			foreach($fields as $fld => $name) {
				echo "<tr>
    				<th>$name</th>
    				<td fld=edit_td_".$fld." id=\"edit_td_".$fld."\">";
				if($fld == "status") {
					echo "Active";
				} else if($types[$fld] == "LIST") {
					$js .= $displayObject->drawFormField($types[$fld], array('id' => "edit_".$fld, 'name' => $fld, 'options' => $list_items2, 'require_me' => 1, 'unspecified' => false));
				} else if($types[$fld] == "BOOL_BUTTON") {
					$js .= $displayObject->drawFormField($types[$fld], array('id' => "edit_".$fld, 'name' => $fld, 'require_me' => 1));
				} elseif($fld == "name") {
					$js .= $displayObject->drawFormField($types[$fld], array('id' => "edit_".$fld, 'name' => $fld, 'require_me' => 1));
				} else {
					$js .= $displayObject->drawFormField($types[$fld], array('id' => "edit_".$fld, 'name' => $fld, 'require_me' => 0));
				}
				echo "</td>";
				echo "</tr>";
			}

			?>

		</table>
	</form>
	<?php ASSIST_HELPER::displayResult(array("info", "<p>Please note: </p><ul style='color:black'><li>Deactivate will make an item inactive and not available within the module but it can be restored at a later date.</li><li>Delete will remove an item from the module and it cannot be restored at a later date.<br />Items can only be deleted if they are not in use elsewhere within in the module.</li></ul>")); ?>
</div>

<div id="div_dialog_restore" title="Restore">
	<input type=hidden id=message_dialog_action name=message_dialog_action value='Restore' />
	<input type=hidden id=message_dialog_id name=message_dialog_id value='0' />
	<div id=message_dialog_div></div>
</div>
<div id="dlg_error" title="Error">
	<?php ASSIST_HELPER::displayResult(array("error", "Error: Please complete all the required fields highlighted in red.")); ?>
</div>
<?php
$js .= "
$('#dlg_error').dialog({
	autoOpen:false,
	modal:true,
	buttons: [{
		text: \"Ok\",
		click: function (e) {
			e.preventDefault();
			$(this).dialog('close');
		}
	}]
});
AssistHelper.hideDialogTitlebar(\"id\", \"dlg_error\");
";
?>
<script type="text/javascript">
	(function ($) {
		$.fn.hasScrollBar = function () {
			return this.get(0).scrollHeight > this.height();
		}
	})(jQuery);

	//Thanks to Erick Petru
	function rgb2hex(rgb) {
		if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

		rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}

		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}

	$(function () {
		<?php
		/*****************
		 * echo stored $js strings here
		 */
		echo $js;
		?>


		var breadcrumb = "<?php echo $helper->getBreadcrumbDivider(); ?>";
		var list_name = $("#list_title").html();
		$("#h1_page_heading").append(" " + breadcrumb + " " + list_name);
		$("#list_title").parent("h2").hide();


		$("#btn_import").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-n"}
		})
			.removeClass("ui-state-default")
			.addClass("ui-button-bold-grey")
			.css("background-color", "#FFFFFF")
			.hover(
				function () {
					$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
				},
				function () {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey");
				}
			).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = "setup_sdbip_defaults_lists_import.php?list=<?php echo $list_id; ?>";
		});


		$("#div_dialog").dialog({
			modal: true,
			autoOpen: false,
			width: "auto",
			buttons: [{
				text: "Save Changes",
				click: function () {
					var okay = true;
					var id = $(this).attr("num");
					$("#ed_frm td textarea").each(function () {
						$(this).removeClass("required");
						var req = $(this).attr("require_me");
						if (req == true) {
							var lngth = $(this).val();
							var howlong = lngth.length;
							if (howlong == undefined || howlong == 0) {
								okay = false;
								$(this).addClass("required");
							}
						}
					});

					$("#ed_frm td input").each(function () {
						$(this).removeClass("required");
						var req = $(this).attr("require_me");
						if (req == true && !$(this).hasClass("color")) {
							var length = parseInt($(this).val());
							if (length == 0) {
								okay = false;
								$(this).addClass("required");
							}
						}
					});
					$("#ed_frm td select").each(function () {
						$(this).removeClass("required");
						var req = $(this).attr("require_me");
						if (req == true && ($(this).val() == undefined || $(this).val().length == 0 || $(this).val() == "X" || $(this).find("option:selected").length == 0)) {
							okay = false;
							$(this).addClass("required");
						}
					});

					if (okay != true) {
						$("#dlg_error").dialog("open");
					} else {
						//All changes are within bounds
						AssistHelper.processing();
						var dta = AssistForm.serialize($("form[name=edit_frm]"));
						var lid = $("#list_id").val();
						var result = AssistHelper.doAjax("inc_controller.php?action=Lists.SimpleEdit", dta);
						if (result[0] == "ok") {
							//document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
							AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "<?php echo $thispage; ?>");
						} else {
							AssistHelper.finishedProcessing(result[0], result[1]);
						}


					}
					//$(this).dialog("close");
				}
			}, {
				text: "<?php echo $sdbipObject->getActivityName("deactivate"); ?>",
				click: function () {
					var id = $(this).attr("num");
					AssistHelper.formatDialogButtons($("#div_dialog_restore"), 0, AssistHelper.getRedCSS());
					$("#div_dialog_restore #message_dialog_action").val("Deactivate");
					$("#div_dialog_restore #message_dialog_id").val(id);
					$('#div_dialog_restore #message_dialog_div').html("<p num=" + id + " >Are you sure you want to <?php echo $sdbipObject->getActivityName("deactivate"); ?> item " + id + "?</p><p class=i>You will be able to <?php echo $sdbipObject->getActivityName("restore"); ?> the item at a later stage if required.");
					$('#div_dialog_restore').dialog("open");

					/*if(confirm("Are you sure you wish to deactivate this item?\n\nYou will be able to restore the item at a later stage if required.")==true) {
						AssistHelper.processing();
						var lid = $("#list_id").val();
						var result = AssistHelper.doAjax("inc_controller.php?action=Lists.Deactivate","id="+id+"&list_id="+lid);
						if(result[0]=="ok") {
							AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"<?php echo $thispage; ?>");
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
                }*/
				}
			}, {
				text: "Restore",
				click: function () {
					$(this).dialog("close");
				}
			}, {
				text: "<?php echo $sdbipObject->getActivityName("delete"); ?>",
				click: function () {
					var id = $(this).attr("num");
					AssistHelper.formatDialogButtons($("#div_dialog_restore"), 0, AssistHelper.getRedCSS());
					$("#div_dialog_restore #message_dialog_action").val("Delete");
					$("#div_dialog_restore #message_dialog_id").val(id);
					$('#div_dialog_restore #message_dialog_div').html("<p num=" + id + " >Are you sure you want to <?php echo $sdbipObject->getActivityName("delete"); ?> item " + id + "?</p><p class=b>Warning: This action cannot be undone.</p>");
					$('#div_dialog_restore').dialog("open");
				}
			}, {
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
				}
			}]
		});

		$("#div_dialog_restore").dialog({
			modal: true,
			autoOpen: false,
			width: "auto",
			buttons: [{
				text: "OK",
				click: function () {
					AssistHelper.processing();
					//var qid = $(this)[0].firstChild.getAttribute("num");
					//var id = parseInt(qid);
					var id = $("#div_dialog_restore #message_dialog_id").val();
					var lid = $("#list_id").val();
					var act = $("#div_dialog_restore #message_dialog_action").val();
					var result = AssistHelper.doAjax("inc_controller.php?action=Lists." + act, "id=" + id + "&list_id=" + lid);
					if (result[0] == "ok") {
						AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "<?php echo $thispage; ?>");
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				}
			}, {
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.formatDialogButtons($("#div_dialog"), 0, AssistHelper.getDialogSaveCSS());
		AssistHelper.formatDialogButtons($("#div_dialog"), 1, AssistHelper.getRedCSS());
		AssistHelper.formatDialogButtons($("#div_dialog"), 2, AssistHelper.getGreenCSS());
		AssistHelper.formatDialogButtons($("#div_dialog"), 3, AssistHelper.getRedCSS());
		AssistHelper.formatDialogButtons($("#div_dialog"), 4, AssistHelper.getCloseCSS());
		AssistHelper.formatDialogButtons($("#div_dialog_restore"), 0, AssistHelper.getGreenCSS());
		AssistHelper.formatDialogButtons($("#div_dialog_restore"), 1, AssistHelper.getCloseCSS());

		AssistHelper.hideDialogTitlebar("id", "div_dialog_restore");

		$("#btn_add").button({
			icons: {primary: "ui-icon-plus"}
		}).removeClass('ui-state-default').addClass('ui-button-bold-grey').hover(
			function () {
				$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
			}, function () {
				$(this).addClass("ui-button-bold-grey").removeClass("ui-button-bold-orange");
			}
		).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var okay = true;

			$("#master_list tr#add_row").find("input:text, textarea").removeClass("required").each(function () {
				var require_me = $(this).attr("require_me");
				require_me = (require_me == true || require_me == "1" || require_me == 1) ? true : false;
				if (require_me == true && $(this).val().length == 0) {
					okay = false
					$(this).addClass("required");
				}
			});
			$("#master_list tr#add_row").find("select").removeClass("required").each(function () {
				var require_me = $(this).attr("require_me");
				require_me = (require_me == true || require_me == "1" || require_me == 1) ? true : false;
				if (require_me == true && ($(this).val() == undefined || $(this).val().length == 0 || $(this).val() == "X" || $(this).find("option:selected").length == 0)) {
					okay = false
					$(this).addClass("required");
				}
			});


			if (okay != true) {
				//alert("Please fill in all of the required fields as highlighted.");
				AssistHelper.closeProcessing();
				$("#dlg_error").dialog("open");
			} else {
				var dta = AssistForm.serialize($("form[name=frm_list]"));
				var result = AssistHelper.doAjax("inc_controller.php?action=Lists.SimpleAdd", dta);
				//console.log(result);
				//alert(result);
				if (result[0] == "ok") {
					//document.location.href = "<?php echo $thispage; ?>" +"&r[0]="+result[0]+"&r[1]="+result[1];
					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "<?php echo $thispage; ?>");
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			}
		});

		$(".btn_restore").button({
			icons: {primary: "ui-icon-check"}
		}).removeClass('ui-state-default').addClass('ui-button-bold-grey').hover(
			function () {
				$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
			}, function () {
				$(this).addClass("ui-button-bold-grey").removeClass("ui-button-bold-orange");
			}
		).click(function (e) {
			e.preventDefault();
			var id = $(this).prop("id");
			AssistHelper.formatDialogButtons($("#div_dialog_restore"), 0, AssistHelper.getGreenCSS());
			$("#div_dialog_restore #message_dialog_action").val("Restore");
			$("#div_dialog_restore #message_dialog_id").val(id);
			$('#div_dialog_restore #message_dialog_div').html("<p num=" + id + " >Are you sure you want to <?php echo $sdbipObject->getActivityName("restore"); ?> item " + id + "?</p>");
			$('#div_dialog_restore').dialog("open");
		});


		$(".btn_edit").button({
			icons: {primary: "ui-icon-pencil"}
		}).removeClass('ui-state-default').addClass('ui-button-bold-grey').hover(
			function () {
				$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
			}, function () {
				$(this).addClass("ui-button-bold-grey").removeClass("ui-button-bold-orange");
			}
		).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			AssistHelper.formatDialogButtons($("#div_dialog"), 2, AssistHelper.getDisplayCSS("hidden"));
			var i = $(this).prop("id");
			var can_delete = $(this).attr("can_delete");
			var sys = Array();
			if ($("#tr_" + i + " td.center").attr("sys") == 1) {
				sys[i] = true;
				AssistHelper.formatDialogButtons($("#div_dialog"), 3, AssistHelper.getDisplayCSS("hidden"));
				AssistHelper.formatDialogButtons($("#div_dialog"), 1, AssistHelper.getDisplayCSS("hidden"));
			} else if (can_delete == 0) {
				AssistHelper.formatDialogButtons($("#div_dialog"), 3, AssistHelper.getDisplayCSS("hidden"));
			} else {
				AssistHelper.formatDialogButtons($("#div_dialog"), 3, AssistHelper.getDisplayCSS("inline"));
				AssistHelper.formatDialogButtons($("#div_dialog"), 1, AssistHelper.getDisplayCSS("inline"));
			}
			$("#tr_" + i + " td").each(function () {
				var h = $(this).html();
				var x = $(this).text();
				var f = $(this).attr("fld");
				$("#div_dialog #edit_td_" + f + " p").hide();
				//	console.log(f);
				if (f == "id") {
					$('#div_dialog #ref').val(i);
					$('#div_dialog #edit_td_id').html(i);
				} else if (sys[i] == true && f != "client_name") {
					$("#div_dialog #edit_" + f).hide();
					$("#div_dialog input.color").hide();
					$("#div_dialog #edit_" + f).val(h);
					$("#div_dialog #lbl_edit_" + f).hide();
					$("#div_dialog #edit_td_" + f + " br").hide();
					$("#div_dialog #edit_td_" + f + " p").show();
					$("#div_dialog #edit_td_" + f + " p").html(h);
					if (f == "colour") {
						$("#div_dialog #edit_td_" + f + " p").html("<div style='height:13px; width:100%; padding-bottom:1px; padding-top:1px; background-color:" + $(this).css("background-color") + "'></div>");
					}
				} else if (f == "colour") {
					var this_col_rgb = $(this).css("background-color");
					var this_col = rgb2hex(this_col_rgb);
					$("#div_dialog input.color").css("background-color", this_col);
					$("#div_dialog input.color").show();
					$("#div_dialog #edit_td_" + f + " p").html("<input type='hidden' id='secret_colour' name='colour' value='" + this_col + "' />");
				} else if (f == "core") {
					if ($(this).find("div.ui-icon").hasClass("ui-icon-check")) {
						$("#edit_core_yes").trigger("click");
					} else {
						$("#edit_core_no").trigger("click");
					}
				} else if (f == "dir_id" || f == "function_id") {
					$("#div_dialog #edit_td_" + f + " br").show();
					$("#div_dialog #edit_" + f).show();
					$("#div_dialog #lbl_edit_" + f).show();
					$("#div_dialog #edit_td_" + f + " p").hide();
					//$("#div_dialog #edit_"+f).val(h);
					$("#div_dialog #edit_" + f).keyup();
					var z = 0;
					$("#div_dialog #edit_" + f + " option").each(function () {
						if ($(this).prop("text") == x) {
							z = $(this).prop("value");
						}
					});
					$("#div_dialog #edit_" + f).val(z);
				} else {
					$("#div_dialog #edit_td_" + f + " br").show();
					$("#div_dialog #edit_" + f).show();
					$("#div_dialog #lbl_edit_" + f).show();
					$("#div_dialog #edit_td_" + f + " p").hide();
					$("#div_dialog #edit_" + f).val(h);
					$("#div_dialog #edit_" + f).keyup();
				}


			});
			<?php if ($list_id == "assessment_frequency"){ ?>
			AssistHelper.formatDialogButtons($("#div_dialog"), 1, AssistHelper.getDisplayCSS("inline"));
			AssistHelper.formatDialogButtons($("#div_dialog"), 1, AssistHelper.getRedCSS());
			<?php    } ?>
			//for each i, make an array with key=>heading and val=>value, with arbitrary numbers of headings and values.
			//console.log(rows);
			$("#div_dialog").attr("num", i);
			AssistHelper.closeProcessing();
			$("#div_dialog").dialog("open");

			//change value of hidden input on colour picker change
			$("#div_dialog input.color").change(function () {
				var colour = "#" + $(this).val();
				$("#secret_colour").val(colour);
			});
		});
		$("#master_list input.color").change(function () {
			var addcolour = "#" + $(this).val();
			$("#add_colour").val(addcolour);
		});

		<?php
		if((isset($fields['dir_id']) || isset($fields['function_id'])) && count($list_items2) == 0) {
			echo "$(\"#master_list\").find(\"input:button\").each(function() {
			$(this).prop(\"disabled\",true);
		});";
		}
		?>
		//}
		$("input.color").attr("spellcheck", "false");


		//Autoscrolling tables if they exceed screen width
		/*	$(window).resize(function() {
				var w= AssistHelper.getWindowSize();
				var wi = w["width"];
				var wid = wi-30;
				var widt = ""+wid+"px";
				$(".horscroll").css({"width":widt});
				console.log($(".horscroll").get(0).scrollWidth +" : "+ $(".horscroll").get(0).clientWidth);
				if($(".horscroll").get(0).scrollWidth > $(".horscroll").get(0).clientWidth) {
					$(".horscroll").css("border","1px solid #dedede");
				} else {
					$(".horscroll").css("border","0px solid #dedede");
				}
			});
			$(window).trigger("resize");*/


	});
</script>
<script type="text/javascript" src="../library/jquery-plugins/jscolor/jscolor.js"></script>
<style>
	.for_display {
		margin: 0px;
		padding: 0px;
	}

	/*.horscroll {
        overflow-x: auto;
        overflow-y: hidden;
    	white-space:nowrap;
  } */
</style>