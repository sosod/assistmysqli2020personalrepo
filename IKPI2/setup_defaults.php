<?php
/**
 * @var SDBP6_HELPER $helper - from inc_header
 */
require_once("inc_header.php");
ASSIST_HELPER::displayResult(array("info", "This page allows you to manage global settings which affect the entire module.  To manage settings specific to an individual ".$helper->replaceAllNames("|SDBIP|")." please go to ".$helper->replaceAllNames($helper->createPageTitleBreadcrumb($helper->createMenuTrailFromLastLink("menu_setup_sdbip"), "|"))."."));
?>
	<style type="text/css">
		#div_left, #div_right {
			padding: 0px 20px 20px 20px;
			width: 45%;
			height: 100%;
		}

		#div_container {
			padding: 0px 10px 0px 10px;
		}
	</style>
	<div id=div_container>
		<div id=div_left>
			<h2>Global Module Settings</h2>
			<table class="form middle">
				<tr>
					<th width=200px>Module Preferences:</th>
					<td>Configure module settings.<span class=float><input type=button value=Configure class=btn_setup id='preferences' /></span></td>
				</tr>
				<tr>
					<th width=200px>Naming:</th>
					<td>Configure the naming convention for the various objects and activities.<span class=float><input type=button value=Configure class=btn_setup id='names' /></span></td>
				</tr>
				<tr>
					<th width=200px>Menus:</th>
					<td>Configure the naming convention for the menus.<span class=float><input type=button value=Configure class=btn_setup id='menu' /></span></td>
				</tr>
				<tr>
					<th>Headings:</th>
					<td>Configure the naming convention for the field headings.<span class=float><input type=button value=Configure class=btn_setup id='headings' /></span></td>
				</tr>
				<!--<tr>
	        <th width=200px><?php /*echo $helper->getObjectName("ORGSTRUCTURE"); */ ?>:</th>
	        <td>Define the <?php /*echo $helper->getObjectName("ORGSTRUCTURE"); */ ?> for use within the module.<span class=float><input type=button value=Configure class=btn_setup id=orgstructure /></span></td>
	    </tr>
	    <tr>
	        <th width=200px><?php /*echo $helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL); */ ?>:</th>
	        <td>Assign <?php /*echo $helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL); */ ?> to the various <?php /*echo $helper->getObjectName("ORGSTRUCTURE"); */ ?> elements and define their roles.<span class=float><input type=button value=Configure class=btn_setup id=admins /></span></td>
	    </tr>-->
				<tr>
					<th width=200px><?php echo $helper->getObjectName("CALCULATIONTYPES"); ?>:</th>
					<td>Configure the <?php echo $helper->getObjectName("CALCULATIONTYPES"); ?> used by the module.<span class=float><input type=button value=Configure class=btn_setup id='calctype' /></span></td>
				</tr>
				<tr>
					<th width=200px><?php echo $helper->getObjectName("RESULTSSETTING"); ?>:</th>
					<td>Configure the <?php echo $helper->getObjectName("RESULTSSETTING"); ?> calculated by the module.<span class=float><input type=button value=Configure class=btn_setup id='results' /></span></td>
				</tr>
				<tr>
					<th width=200px><?php echo $helper->getObjectName("TARGETTYPES"); ?>:</th>
					<td>Configure the <?php echo $helper->getObjectName("TARGETTYPES"); ?> used by the module.<span class=float><input type=button value=Configure class=btn_setup id='targettypes' /></span></td>
				</tr>
			</table>
		</div>
	</div> <!-- end div container -->
	<script type=text/javascript>
		$(function () {
			$("h2").css("margin-top", "15px");

			$("input:button").button().css("font-size", "75%");
			$("input:button.btn_setup").click(function () {
				document.location.href = "setup_defaults_" + $(this).prop("id") + ".php";
			});
			$("input:button.btn_list").click(function () {
				document.location.href = "setup_defaults_lists.php?l=" + $(this).prop("id");
			});
			$("input:button.btn_report").click(function () {
				document.location.href = "setup_defaults_" + $(this).prop("id") + ".php";
			});
			$("#div_right").css("float", "right");
			$("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
			$("table.form").css({"width": "100%"});
			$(window).resize(function () {
				var x = 0;
				var y = 0;
				$("#div_right").children().each(function () {
					x += AssistString.substr($(this).css("height"), 0, -2) * 1;
				});
				$("#div_left").children().each(function () {
					y += AssistString.substr($(this).css("height"), 0, -2) * 1;
				});
				if (x > y) {
					var z = x;
				} else {
					var z = y;
				}
				$("#div_container").css("height", (z + 50) + "px");
			});
			$(window).trigger("resize");
		});
	</script>

<?php

//ASSIST_HELPER::varPrint(get_defined_vars());
?>