<?php
include("inc_header.php");

/** @var SDBP6_SETUP_ORGSTRUCTURE $orgObject */
$org_structure = $orgObject->getOrgStructureFromDBForSetup(false, $sdbip_id);

?>

	<table id=tbl_container>
		<tr>
			<td class=container>
				<?php
				$is_add_page = true;
				$is_edit_page = true;
				$is_view_page = true;
				$max_display_order = 0;

				//ASSIST_HELPER::arrPrint($org_structure);

				$dialog_url = "setup_sdbip_defaults_orgstructure_object.php";
				$query_string = array();
				foreach($_REQUEST as $key => $r) {
					if($key != "r") {
						$query_string[] = $key."=".$r;
					}
				}
				$dialog_redirect = "setup_sdbip_defaults_orgstructure.php?".implode("&", $query_string);

				//needed for colspan counting
				function drawTextTree($i, $s, $indent, $level) {
					global $org_structure;
					global $total_levels;

					if($total_levels < $level) {
						$total_levels = $level;
					}
					if($s['has_children'] == true) {
						if(isset($org_structure[$i])) {
							$icon = "+";
						} else {
							$icon = "-";
						}
					} else {
						$icon = "*";
					}

					$echo = "<p>".$icon."".$s['name']."&nbsp;V&nbsp;E&nbsp;";
					if($s['has_children'] == true) {
						$echo .= "AC";
						if(isset($org_structure[$i])) {
							$indent .= "----";
							$level++;
							foreach($org_structure[$i] as $i2 => $s2) {
								drawTextTree($i2, $s2, $indent, $level);
							}
							$level--;
						}
					}
					$echo .= "</p>";

				}


				$total_levels = 2;
				function drawTableTree($i, $s, $indent, $parent_active = true) {
					global $org_structure;
					global $total_levels;
					global $orgObject;
					global $idp_object_id;
					global $is_edit_page;
					$s['can_assign'] = true;
					$s['has_children'] = $indent == 0 ? true : false;
					$is_active = $parent_active == true && $s['status'] == SDBP6::ACTIVE;

					if($s['has_children'] == true) {
						if(isset($org_structure[$i])) {
							$icon = "ui-icon-triangle-1-se";
						} else {
							$icon = "ui-icon-triangle-1-e";
						}
					} else {
						$icon = "ui-icon-carat-1-sw";
					}
					$icon_color = $is_active ? "222222" : "888888";


					$has_edit_button = false;
					if($is_edit_page && $is_active) {
						$has_edit_button = true;
					}
					$has_restore_button = false;
					if($is_edit_page && !$is_active && $parent_active) {
						$has_restore_button = true;
					}

					$echo = "";
					//don't display on view page
					if($is_active || $is_edit_page) {
						$echo .= "<tr class='".($is_active == true ? "" : "not-active")." ".($indent == 0 ? "b" : "")."'>";
						for($x = 0; $x < $indent; $x++) {
							$echo .= "
			<td class=placeholder style='background: url(/pics/icons/vertical-line.png)'>&nbsp;</td>";
						}
						$echo .= "
		<td>".$orgObject->getDisplayIcon($icon, "", $icon_color)."</td>
		<td colspan=".($total_levels - $indent).">[".$s['ref']."] ".$s['name']."</td>";
						$echo .= "
		<td class=right>";
						if($s['has_children'] == true && $is_edit_page && $is_active == true && $indent == 0) {
							$echo .= "
			<button class='action-button btn_add' parent_id=".$s['id']." object_id='0'>".$orgObject->replaceActivityNames("|add|")." Child</button>";
						}
						$echo .= "
		</td>
		<td>&nbsp;<button class='action-button btn_view' parent_id=".$s['parent_id']." object_id=".$s['id'].">".$orgObject->replaceActivityNames("|view|")."</button>
			".($has_edit_button ? "&nbsp;<button class=' action-button btn_edit' parent_id=".$s['parent_id']." object_id=".$s['id'].">".$orgObject->replaceActivityNames("|edit|")."</button>&nbsp;" : "")."
			".($has_restore_button ? "&nbsp;<button class=' action-button btn_restore' parent_id=".$s['parent_id']." object_id=".$s['id'].">".$orgObject->replaceActivityNames("|restore|")."</button>&nbsp;" : "");
						if($s['has_children'] == true && $is_edit_page && $is_active == true && $indent == 0) {
							$echo .= "<button class='btn_sort_child' object_id=".$s['id'].">Sort Children</button>";
						}
						$echo .= "
		</td></tr>";
						if($s['has_children'] == true) {
							if(isset($org_structure[$i])) {
								$indent++;
								foreach($org_structure[$i] as $i2 => $s2) {
									$echo .= drawTableTree($i2, $s2, $indent, $is_active);
								}
								$indent--;
							}
						}
					}
					return $echo;

				}

				$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : "SETUP"; //echo $section;
				//$segmentObject = $orgObject;
				//echo "getList start";
				//segment_data = $org_structure;
				//echo "getList end";
				$segment_details = array('name' => "ABC", 'help' => "DEF");
				//ASSIST_HELPER::arrPrint($org_structure);

				//$total_levels = 0;

				/***
				 * Basic text version
				 */
				//needed for colspan counting
				$indent = "";
				$levels = 1;
				if(isset($org_structure[0]) && count($org_structure[0]) > 0) {
					foreach($org_structure[0] as $i => $s) {
						$max_display_order = $s['display_order'];
						drawTextTree($i, $s, $indent, $levels);
					}
				} else {

				}

				/**
				 * Table version
				 */
				echo "
	<table id=tbl_segment_tree >
	";
				if($is_edit_page || $is_add_page) {
					echo "
		<tr>
			<td id=td_add colspan=1>
				<span class=float >
					<button class='action-button btn_plus top-level' parent_id='0' object_id=0>".$orgObject->replaceActivityNames("|add|")." Top Level</button>
					<button class='btn_sort' object_id=0>Sort Top Level</button>
				</span>
			</td>
		</tr>
		";
				}
				$echo = "";
				if(isset($org_structure[0]) && count($org_structure[0]) > 0) {
					foreach($org_structure[0] as $i => $s) {
						$indent = 0;
						$echo .= drawTableTree($i, $s, $indent);
					}
				} else {
					if($is_edit_page) {
						$echo .= "<tr><td>Please click the Add button above to start.</td></tr>";
					} else {
						$echo .= "<tr><td>No line items available to display.</td></tr>";
					}
				}
				echo $echo."</table>";


				?>
			</td>
		</tr>
		<tr>
			<td>
				<?php /** @var SDBP6_HELPER $helper */
				/** @var SDBP6_DISPLAY $displayObject */
				$js .= $displayObject->drawPageFooter($helper->getGoBack('setup_sdbip_defaults.php'), "setup", array('section' => "org", 'sdbip_id' => $sdbip_id)); ?>

			</td>
		</tr>
	</table>
	<div id=dlg_child class=dlg-child title="">
		<iframe id=ifr_form_display style="border:0px solid #000000;" src="">

		</iframe>
	</div>
	<script type="text/javascript">
		$(function () {
			<?php echo $js; ?>
			var top_max_display_order = <?php echo $max_display_order; ?>;
			$("span").css("border", "0px dashed #009900");
			$("#tbl_container, #tbl_container td").css({"border": "0px dashed #009900"});
			$("#tbl_segment_tree, #tbl_segment_tree td").css({"border": "0px dashed #009900", "margin": "0 auto"});

			$("tr.not-active td").css("color", "#999999");

			var cells = $("#tbl_segment_tree tr:first").children("td").length;
			$("#tbl_segment_tree tr:gt(0)").each(function () {
				var c = $(this).children("td").length;
				if (c > cells) {
					cells = c;
				}
			});
			if (cells > 1) {
				$("#tbl_segment_tree #td_add").prop("colspan", cells);
			}

			var scr_size = AssistHelper.getWindowSize();
			//var width = parseInt(AssistString.substr($("#tbl_segment_tree").css("width"),0,-2));
			//if(width<(scr_size['width']*0.5)) {
			//	width = (scr_size['width']*0.5);
			//}
			//$("#tbl_container").css("width",(width+50)+"px");

			$("#tbl_segment_tree tr:gt(0)").hover(function () {
				$(this).css("background-color", "#dedede");
				$(this).children("td.placeholder").css("background", "url(/pics/icons/vertical-line-hover.png)");
			}, function () {
				$(this).css("background-color", "");
				$(this).children("td.placeholder").css("background", "url(/pics/icons/vertical-line.png)");
			});

			$("button.btn_plus").button({
				icons: {primary: "ui-icon-plus"}
			}).removeClass("ui-state-default").addClass("ui-button-bold-green")
				.hover(
					function () {
						$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
					}, function () {
						$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
					}
				);
			$("button.btn_add").button({
				icons: {primary: "ui-icon-plus"}
			}).removeClass("ui-state-default").addClass("ui-button-minor-green")
				.hover(
					function () {
						$(this).removeClass("ui-button-minor-green").addClass("ui-button-minor-orange");
					}, function () {
						$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-green");
					}
				).click(function (e) {
				e.preventDefault();
			})
				.children(".ui-button-text")
				.css({"padding-top": "2px", "padding-bottom": "0px"});

			$("button.btn_view").button({
				icons: {primary: "ui-icon-extlink"}
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(
					function () {
						$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
					},
					function () {
						$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
					}
				).click(function (e) {
				e.preventDefault();

			}).children(".ui-button-text")
				.css({"padding-top": "0px", "padding-bottom": "0px"});

			$("button.btn_edit").button({
				icons: {primary: "ui-icon-pencil"}
			}).removeClass("ui-state-default")
				.addClass("ui-button-minor-grey")
				.hover(
					function () {
						$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
					},
					function () {
						$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
					}
				).click(function (e) {
				e.preventDefault();
			})
				.children(".ui-button-text")
				.css({"padding-top": "2px", "padding-bottom": "0px"});

			$("button.btn_restore").button({
				icons: {primary: "ui-icon-arrowrefresh-1-e"}
			}).removeClass("ui-state-default")
				.addClass("ui-button-minor-grey")
				.hover(
					function () {
						$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
					},
					function () {
						$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
					}
				).click(function (e) {
				e.preventDefault();
			})
				.children(".ui-button-text")
				.css({"padding-top": "2px", "padding-bottom": "0px"});

			$(".btn_sort").button({
				icons: {primary: "ui-icon-arrowthick-2-n-s"}
			}).click(function (e) {
				e.preventDefault();
				document.location.href = 'setup_sdbip_defaults_orgstructure_sort.php?sdbip_id=<?php echo $sdbip_id; ?>&parent_id=' + ($(this).attr("object_id"));
			}).removeClass("ui-state-default").addClass("ui-button-state-grey")
				.hover(function () {
					$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-grey");
				}, function () {
					$(this).addClass("ui-button-state-grey").removeClass("ui-button-state-orange");
				});
			$(".btn_sort_child").button({
				icons: {primary: "ui-icon-arrowthick-2-n-s"}
			}).click(function (e) {
				e.preventDefault();
				document.location.href = 'setup_sdbip_defaults_orgstructure_sort.php?sdbip_id=<?php echo $sdbip_id; ?>&parent_id=' + ($(this).attr("object_id"));
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(function () {
					$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
				}, function () {
					$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
				});


			var my_window = AssistHelper.getWindowSize();
			if (my_window['width'] > 800) {
				var my_width = 850;
			} else {
				var my_width = 800;
			}
			var my_height = my_window['height'] - 50;

			$("div.dlg-child").dialog({
				autoOpen: false,
				modal: true,
				width: my_width,
				height: my_height,
				beforeClose: function () {
					AssistHelper.closeProcessing();
				},
				open: function () {
					$(this).dialog('option', 'width', my_width);
					$(this).dialog('option', 'height', my_height);
					$(this).find("#ifr_form_display").find("input:text:first").focus();
				}
			});

			$("button.action-button").click(function () {
				AssistHelper.processing();
				var i = $(this).attr("object_id");
				var p = $(this).attr("parent_id");
				var t = "SETUP_ORGSTRUCTURE";
				var dta = "sdbip_id=<?php echo $sdbip_id; ?>&object_type=" + t + "&object_id=" + i + "&parent_id=" + p + "&max_order=" + top_max_display_order;
				if ($(this).hasClass("btn_restore")) {
					var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ORGSTRUCTURE.RESTORE", dta);
					if (result[0] == "ok") {
						AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				} else {
					$dlg = $("div#dlg_child");
					var act = "view";
					if ($(this).hasClass("btn_edit")) {
						act = "edit";
					} else if ($(this).hasClass("btn_add") || $(this).hasClass("btn_plus")) {
						act = "add";
					}
					var obj = t.toLowerCase();
					var heading = AssistString.ucwords(act);
					var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action=" + act + "&" + dta;
					//console.log(url);
					$dlg.dialog("option", "title", heading);
					if (act == "view") {
						$dlg.dialog("option", "buttons", [{
							text: "Close", click: function () {
								$(this).dialog("close");
							}
						}]);
					} else {
						$dlg.dialog("option", "buttons", []);
					}
					$("#ifr_form_display").prop("width", (my_width - 30) + "px").prop("height", (my_height - 50) + "px").prop("src", url);
					//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
					//$dlg.dialog("open");
					/*
									*/
				}
			});


		});

		function dialogFinished(icon, result) {
			if (icon == "ok") {
				//alert(result)
				//document.location.href = '<?php echo $dialog_redirect; ?>&r[]=ok&r[]='+result;
				$dlg = $("div#dlg_child");
				$dlg.dialog("close");
				AssistHelper.processing();
				AssistHelper.finishedProcessingWithRedirect(icon, result, '<?php echo $dialog_redirect; ?>');
			} else {
				AssistHelper.processing();
				AssistHelper.finishedProcessing(icon, result);
			}
		}

	</script>
<?php


?>