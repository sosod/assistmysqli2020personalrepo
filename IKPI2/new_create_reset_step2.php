<?php
$_REQUEST['step'] = "reset";
$is_reset_page = true;
require_once("inc_header.php");
$is_reset_page = true;

//Ignore whatever was picked up in inc_header - use sdbip_id passed by previous page
$sdbip_id = $_REQUEST['reset_sdbip_id'];
$sdbip_details = $sdbipObject->getRawObject($sdbip_id);
$sdbip_details = $sdbipObject->formatSDBIPRecord($sdbip_details);

$fin_year_name = $sdbip_details['fin_year_name'];
$summary_details = $sdbip_details['summary_details'];
$is_confirmed = $sdbip_details['is_confirmed'];
$is_activated = $sdbip_details['is_activated'];

if($sdbip_details['idp_module'] == "FALSE_LINK") {
	$echo = $displayObject->getDataField("BOOL_BUTTON", 0);
	$idp_module = $echo['display'];
	$js .= $echo['js'];
} else {
	$idp_name = $sdbipObject->getLinkedIDP($sdbip_details['idp_module'], $sdbip_details['idp_id']);
	$echo = $displayObject->getDataField("BOOL_BUTTON", 1);
	$idp_module = $echo['display']." - ".$idp_name;
	$js .= $echo['js'];
}

if($sdbip_details['alt_module'] == "FALSE_LINK") {
	$echo = $displayObject->getDataField("BOOL_BUTTON", 0);
	$alt_module = $echo['display'];
	$js .= $echo['js'];
} elseif($sdbipObject->checkIntRef($sdbip_details['alt_module'])) {
	$alt_name = $sdbip_details['alt_module_name'];//$sdbipObject->getSDBIPName($sdbip_details['alt_module']);
	$echo = $displayObject->getDataField("BOOL_BUTTON", 1);
	$alt_module = $echo['display']." - ".$alt_name." [".ucwords($sdbip_details['alt_action'])."]";
	$js .= $echo['js'];
} else {
	$alt_name = $sdbipObject->getLinkedSDBIP($sdbip_details['alt_module']);
	$echo = $displayObject->getDataField("BOOL_BUTTON", 1);
	$alt_module = $echo['display']." - ".$alt_name." [".ucwords($sdbip_details['alt_action'])."]";
	$js .= $echo['js'];
}
$sdbip_status = "Active";
$is_activated = false;
$is_confirmed = false;
$is_new = false;
if($sdbip_details['is_activated']) {
	$sdbip_status = "Activated";
	$is_activated = true;
} elseif($sdbip_details['is_confirmed']) {
	$sdbip_status = "Confirmed";
	$is_confirmed = true;
} elseif($sdbip_details['is_new']) {
	$sdbip_status = "New";
	$is_new = true;
} else {
	$sdbip_status = "Unknown";
}

$echo = $displayObject->getDataField("BOOL_BUTTON", $sdbip_details['mscoa_compliant']);
$mscoa_compliant = $echo['display'];
$js .= $echo['js'];

$mscoa_version = $sdbip_details['mscoa_version'];


//get time period details
$timeObject = new SDBP6_SETUP_TIME();
$time_periods = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_details['sdbip_id']);


$sdbip_list = $sdbipObject->getListOfSDBIPObjects();
//ASSIST_HELPER::arrPrint($sdbip_details);
?>


<div id="div_container">

	<!-- <table style="margin:0 auto;" id="tbl_change">
		<tr><td class="center b"><?php echo $sdbip_object_name; ?>:&nbsp;&nbsp;<select id="sel_sdbip"><?php
	foreach($sdbip_list as $i => $s) {
		echo "<option value=".$i." ".($i == $sdbip_id ? "selected" : "").">".$s.($i == $sdbip_id ? "*" : "")."</option>";
	}
	?></select>&nbsp;&nbsp;<button id="btn_sel_go">Change SDBIP</button></td></tr>
	</table> -->
	<?php
	ASSIST_HELPER::displayResult(array("warn",
		"WARNING: If you ".$sdbipObject->getActivityName("RESET")." or ".$sdbipObject->getActivityName("DELETE").", you will REMOVE all elements of that object selected, including any updates which may have been done.  This cannot be undone."))
	?>
	<table class=form id=tbl_details style='margin:0 auto'>
		<tr>
			<th>Ref:</th>
			<td><?php echo $sdbip_details['ref']; ?></td>
		</tr>
		<tr>
			<th><?php echo $sdbipObject->getObjectName("financialyear"); ?>:</th>
			<td><?php echo $fin_year_name; ?></td>
		</tr>
		<tr>
			<th>Name:</th>
			<td><?php echo $sdbip_details['name']; ?></td>
		</tr>
		<tr>
			<th>Linked to IDP Module:</th>
			<td><?php echo $idp_module; ?></td>
		</tr>
		<tr>
			<th>Linked to alternative <?php echo $sdbip_object_name; ?>:</th>
			<td><?php echo $alt_module; ?></td>
		</tr>
		<tr>
			<th>Time Periods:</th>
			<td><?php echo implode("<br />", $time_periods); ?></td>
		</tr>
		<tr>
			<th>mSCOA Compliant</th>
			<td><?php echo $mscoa_compliant; ?></td>
		</tr>
		<tr>
			<th>mSCOA Version</th>
			<td><?php echo $mscoa_version; ?></td>
		</tr>
		<tr>
			<th>Created On:</th>
			<td><?php echo date("d F Y H:i", strtotime($sdbip_details['insertdate'])); ?></td>
		</tr>
		<tr>
			<th>Created By:</th>
			<td><?php echo $sdbipObject->getUserName($sdbip_details['insertuser']); ?></td>
		</tr>
		<tr>
			<th>Confirmed On:</th>
			<td><?php echo($is_confirmed && strlen($sdbip_details['confirmdate']) > 0 ? date("d F Y H:i", strtotime($sdbip_details['confirmdate'])) : "N/A"); ?></td>
		</tr>
		<tr>
			<th>Confirmed By:</th>
			<td><?php echo($is_confirmed && strlen($sdbip_details['confirmuser']) > 0 ? $sdbipObject->getUserName($sdbip_details['confirmuser']) : "N/A"); ?></td>
		</tr>
		<tr>
			<th>Activated On:</th>
			<td><?php echo($is_activated && strlen($sdbip_details['activationdate']) > 0 ? date("d F Y H:i", strtotime($sdbip_details['activationdate'])) : "N/A"); ?></td>
		</tr>
		<tr>
			<th>Activated By:</th>
			<td><?php echo($is_activated && strlen($sdbip_details['activationuser']) > 0 ? $sdbipObject->getUserName($sdbip_details['activationuser']) : "N/A"); ?></td>
		</tr>
		<tr>
			<th>Status:</th>
			<td><?php echo $sdbip_status; ?></td>
		</tr>
		<tr>
			<th>Stats:</th>
			<td>
				<table id=tbl_stats>
					<?php
					$reset_name = $sdbipObject->getActivityName("RESET");
					$objects = $sdbipObject->getModuleSections();
					foreach($objects as $ot) {
						switch($ot) {
							case "PROJECT":
								$object_name = $sdbipObject->getObjectName("PROJECT");
								$objects_name = $sdbipObject->getObjectName("PROJECTS");
								$row_heading = $sdbipObject->getObjectName("PROJECTS");
								$field = SDBP6_PROJECT::OBJECT_TYPE;
								break;
							case "TOPKPI":
								$object_name = $sdbipObject->getObjectName("KPI");
								$objects_name = $sdbipObject->getObjectName("KPIS");
								$row_heading = $sdbipObject->getObjectName("TOPKPIS");
								$field = SDBP6_TOPKPI::OBJECT_TYPE;
								break;
							case "DEPTKPI":
								$object_name = $sdbipObject->getObjectName("KPI");
								$objects_name = $sdbipObject->getObjectName("KPIS");
								$row_heading = $sdbipObject->getObjectName("DEPTKPIS");
								$field = SDBP6_DEPTKPI::OBJECT_TYPE;
								break;
						}
						$show_object_reset_button = false;
						if(!$is_activated && !$is_confirmed) {
							$show_object_reset_button = true;
						}
						if($summary_details[$field]['count'] > 0) {
							echo "
							<tr>
								<td><span class=b style='padding-right:10px;'>".$row_heading.":</span>
								".$summary_details[$field]['count']." ".strtolower($summary_details[$field]['count'] == 1 ? $object_name : $objects_name)."
								".($show_object_reset_button ? "<span class=float style='margin-left: 20px'><button class=btn-reset id=".strtoupper($field).">".$reset_name." all ".$row_heading."</button></span>" : "")."</td>
							</tr>";
						} else {
							echo "
							<tr class='tr-zero'>
								<td><span class=b style='padding-right:10px;'>".$row_heading.":</span>
									".$summary_details[$field]['count']." ".strtolower($summary_details[$field]['count'] == 1 ? $object_name : $objects_name)."
								</td>
							</tr>";
						}
					}
					/*	$object_name = $sdbipObject->getObjectName("KPI");
						$objects_name = $sdbipObject->getObjectName("KPIS");
						$row_heading = $sdbipObject->getObjectName("TOPKPIS");
						$field = SDBP6_TOPKPI::OBJECT_TYPE;
						if($summary_details[$field]['count'] > 0) {
							echo "
							<tr>
								<td><span class=b style='padding-right:10px;'>".$row_heading.":</span>
								".$summary_details[$field]['count']." ".strtolower($summary_details[$field]['count'] == 1 ? $object_name : $objects_name)."
								<span class=float style='margin-left: 20px'><button class=btn-reset id=".strtoupper($field).">".$reset_name." all ".$row_heading."</button></span></td>
							</tr>";
						}
						else {
							echo "
							<tr class='tr-zero'>
								<td><span class=b style='padding-right:10px;'>".$row_heading.":</span>
									".$summary_details[$field]['count']." ".strtolower($summary_details[$field]['count'] == 1 ? $object_name : $objects_name)."
								</td>
							</tr>";
						}
						$row_heading = $sdbipObject->getObjectName("DEPTKPIS");
						$field = SDBP6_DEPTKPI::OBJECT_TYPE;
						if($summary_details[$field]['count'] > 0) {
							echo "
							<tr>
								<td><span class=b style='padding-right:10px;'>".$row_heading.":</span>
								".$summary_details[$field]['count']." ".strtolower($summary_details[$field]['count'] == 1 ? $object_name : $objects_name)."
								<span class=float style='margin-left: 20px'><button class=btn-reset id=".strtoupper($field).">".$reset_name." all ".$row_heading."</button></span></td>
							</tr>";
						}
						else {
							echo "
							<tr class='tr-zero'>
								<td><span class=b style='padding-right:10px;'>".$row_heading.":</span>
									".$summary_details[$field]['count']." ".strtolower($summary_details[$field]['count'] == 1 ? $object_name : $objects_name)."
								</td>
							</tr>";
						}*/

					?>
				</table>
			</td>
		</tr>
		<tr>
			<th></th>
			<td>
				<button class=btn-delete><?php echo $sdbipObject->getActivityName("DELETE")." entire ".$sdbip_details['name']; ?></button>
			</td>
		</tr>
	</table>
	<?php
	ASSIST_HELPER::displayResult(array("warn",
		"WARNING: Make sure that you are working on the correct $sdbip_object_name!<br />Any action taken on this page cannot be undone!"));


	$js .= $displayObject->drawPageFooter($sdbipObject->getGoBack("new_create_reset.php", "", true));
	?>
</div>

<!-- <div id=div_warning><?php ASSIST_HELPER::displayResult(array("info",
	"Please note: If this $sdbip_object_name has been converted from an alternate $sdbip_object_name module, then removing this $sdbip_object_name will NOT restore the prior $sdbip_object_name module.  You will be able to access the module via the ".$sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_create"), "|"))." process to create a new $sdbip_object_name, however the module will remain closed for normal use.  If you want to restore the alternate module then please contact your Business Partner.")); ?></div>
--><p>&nbsp;</p>
<div id=dlg_confirm></div>
<script type=text/javascript>
	$(function () {
		<?php echo $js; ?>
		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});
		$(".btn-delete").button({
			icons: {primary: "ui-icon-trash"}
		}).removeClass("ui-state-default").addClass("ui-button-state-red")
			.hover(function () {
				$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-red");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-red");
			}).click(function (e) {
			e.preventDefault();
			confirmSubmission("<span class=b><?php echo addslashes($sdbipObject->getActivityName("DELETE")); ?> entire <?php echo addslashes($sdbip_object_name); ?>?</span><br /><br />WARNING: This action cannot be undone!", "confirm", "SDBIP", $(this));
		});


		$(".btn-reset").button({}).removeClass("ui-state-default").addClass("ui-button-minor-red")
			.hover(function () {
				$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-red").css("background-color", "#fffff");
			}, function () {
				$(this).css("background-color", "").removeClass("ui-button-minor-orange").addClass("ui-button-minor-red");
			}).click(function (e) {
			e.preventDefault();
			var i = $(this).prop("id");
			var x = $(this).html();
			confirmSubmission("<span class=b>" + x + "?</span><br /><br />WARNING: This action cannot be undone!", "confirm", i, $(this));
		});


		$("#tbl_stats").css("border", "0px solid #FFFFFF").find("td").css("border", "0px solid #FFFFFF");
		$("#tbl_stats tr").hover(function () {
			$(this).css("background-color", "#eeeeee");
		}, function () {
			$(this).css("background-color", "");
		});
		$("table tr").css("height", "35px").find("td").css("vertical-align", "middle");
		var row_count = $("table tr").length;
		//console.log(row_count);
		//$("table tr:lt(" + (row_count - 5) + ")").find("th").css("vertical-align", "middle");
		$("tr.tr-zero td").css("color", "#ababab");

		function confirmSubmission(display_text, next_step, action, $btn) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>' + display_text + '</p><label id=lbl_hide></label>');
			if (next_step == 'ok') {
				var buttons = [{
					text: "Ok", click: function () {
						$(this).dialog("close");
					}
				}];
			} else if (next_step == 'confirm') {
				var buttons = [
					{
						text: "Confirm & Continue", click: function () {
							$(this).dialog("close");
							nextStep(action, $btn);
						}, class: 'ui-state-ok'
					},
					{
						text: "Cancel", click: function () {
							$(this).dialog("close");
						}, class: 'ui-state-error'
					}
				];
			} else {
				var buttons = [
					{
						text: "Yes", click: function () {
							nextStep(next_step, $('#step').val());
							$(this).dialog("close");
						}, class: 'ui-state-ok'
					},
					{
						text: "No", click: function () {
							$(this).dialog("close");
						}, class: 'ui-state-error'
					}
				];
			}
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}

		function nextStep(action, $btn) {
			AssistHelper.processing();
			switch (action) {
				case "SDBIP":
					var result = AssistHelper.doAjax("inc_controller.php?action=SDBIP.RESET", "object_id=<?php echo $sdbip_details['id']; ?>");
					if (result[0] == "ok") {
						AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "new_create_step1.php");
					} else {
						AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
					}
					break;
				case "PROJECTS":
				case "PROJECT":
				case "TOPKPI":
				case "DEPTKPI":
					var result = AssistHelper.doAjax("inc_controller.php?action=" + action + ".RESET", "parent_id=<?php echo $sdbip_details['id']; ?>");
					if (result[0] == "ok") {
						$btn.parent().parent().css("color", "#ababab");
						$btn.removeClass("ui-button-minor-red").addClass("ui-button-minor-grey").hide();
					}
					AssistHelper.finishedProcessing(result[0], result[1]);
					break;
			}
		}

		$("#tbl_details").find("tr").find("th").css("padding-right", "25px");
		$("#tbl_details").find("tr").find("td").css("padding-right", "30px");
		$("#tbl_details").width($("#tbl_details").width() + 5).css("margin", "0 auto");
		$("#tbl_change").width($("#tbl_details").width()).css("margin", "0 auto");
		$("#div_container").width($("#tbl_details").width() + 5).css("margin", "0 auto").css("margin-bottom", "10px");
		// $("#tbl_change").width("100%").css("margin-bottom","10px");


		$("#btn_sel_go").button({
			icons: {primary: "ui-icon-arrow-1-e"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = "new_create_reset.php?sdbip_id=" + $("#sel_sdbip").val();
		}).removeClass("ui-state-default").addClass("ui-button-state-grey")
			.hover(function () {
				$(this).removeClass("ui-button-state-grey").addClass("ui-button-state-orange");
			}, function () {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-grey");
			});
	});
</script>