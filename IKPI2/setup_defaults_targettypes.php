<?php
require_once("inc_header.php");

$myObject = new SDBP6_SETUP_TARGETTYPE();

/* Get existing target types */
$target_types = $myObject->getObjectsForSetup();

/* set some properties/settings */
$headings = $myObject->getTargetTypeHeadings(true);
$list_options = $myObject->getListsForSetup();
$glossary = $myObject->getExplanatoryGlossary();
$ref_tag = $myObject->getRefTag();

/* Display table */
?>
<style type="text/css">
	#tbl_results td {
		padding: 7px;
		vertical-align: middle;
	}

	#tbl_results input {
		padding: 5px;
	}
</style>
<form name=frm_save>
	<table class='tbl-container not-max'>
		<tr>
			<td>
				<table id=tbl_results>
					<tr>
						<?php
						foreach($headings as $fld => $head) {
							echo "<th>".str_replace("|", "<br />", $head['name'])."</th>";
						}
						?>
						<th></th>
					</tr>
					<?php
					/* Add row */
					?>
					<tr id=add_row>
						<?php
						$form_fields = array();
						foreach($headings as $fld => $head) {
							$h_type = $head['type'];
							switch($h_type) {
								case "REF":
									$display = $ref_tag."#";
									$class = "center";
									break;
								case "LIST":
									$options = array(
										'id' => $fld,
										'name' => $fld,
										'unspecified' => false,
										'req' => 1,
										'options' => $list_options[$fld],
										'padding' => true,
									);
									$x = $displayObject->createFormField($h_type, $options);
									$display = $x['display'];
									$form_fields[$fld] = $display;
									$js .= $x['js'];
									$class = "";
									break;
								case "BOOL_BUTTON":
								case "BOOL":
									$options = array(
										'id' => $fld,
										'name' => $fld,
										'small_size' => true,
										'other' => "class='is_bool_input'",
									);
									//'form'=>"vertical",
									$x = $displayObject->createFormField($h_type, $options, "0");
									$display = $x['display'];
									$js .= $x['js'];
									$class = "center";
									break;
								default:
									$options = array(
										'id' => $fld,
										'name' => $fld,
										'req' => true,
									);
									$x = $displayObject->createFormField($h_type, $options, "");
									$display = $x['display'];
									$form_fields[$fld] = $display;
									$js .= $x['js'];
									$class = "";
									break;
							}
							echo "
			<td class='$class'>".$display."</td>";
						}
						?>
						<td>
							<button class=btn-add><?php echo $myObject->getActivityName("ADD"); ?></button>
						</td>
					</tr>
					<?php
					/* existing types */
					foreach($target_types as $i => $type) {
						$status = $type['status'];
						if(($status & SDBP6::ACTIVE) == SDBP6::ACTIVE) {
							$is_active = true;
						} else {
							$is_active = false;
						}
						echo "
	<tr ".(!$is_active ? "class=inactive" : "").">";
						foreach($headings as $fld => $head) {
							$h_type = $head['type'];
							switch($h_type) {
								case "REF":
								case "SMLVC":
									$class = "center";
									$val = ($h_type == "REF" ? $ref_tag : "").$type[$fld];
									if($fld == "code") {
										$val .= "<input type=hidden id=old_code_".$i." class='old-code' value='".$val."' />";
									}
									break;
								case "LIST":
									$class = "";
									$val = isset($list_options[$head['table']][$type[$fld]]) ? $list_options[$head['table']][$type[$fld]] : $myObject->getUnspecified();
									break;
								case "BOOL_BUTTON":
								case "BOOL":
									$class = "center";
									$x = $displayObject->getDataField($h_type, $type[$fld]);
									$js .= $x['js'];
									$val = $x['display'];
									break;
								default:
									$class = "";
									$val = $type[$fld];
									break;
							}
							echo "
			<td class='$class'>".$val."</td>";
						}
						if($is_active) {
							echo "
			<td><button class=btn-edit id=edit_".$type['id']." object_id=".$type['id'].">".$myObject->getActivityName("EDIT")."</button></td>";
						} else {
							echo "
			<td><button class=btn-restore id=restore_".$type['id']." object_id=".$type['id'].">".$myObject->getActivityName("RESTORE")."</button></td>";
						}
						echo "
	</tr>";
					}
					?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'), "targettype", array('section' => "targettype")); ?>
			</td>
		</tr>
	</table>
</form>

<div id=div_edit title=Edit>
	<form name=frm_edit>
		<h2>Edit</h2>
		<input type=hidden name=object_id id=object_id value=0/>
		<table id=tbl_edit class=form>
			<?php
			foreach($headings as $fld => $head) {
				echo "
		<tr>
			<th>".str_replace("|", " ", $head['name']).":</th>
			<td>";
				if(isset($form_fields[$fld])) {
					echo $form_fields[$fld];
				} elseif($fld == "id") {
					echo "<label id=lbl_id for=object_id class=is_label></label>";
				} elseif($head['type'] == "BOOL_BUTTON" || $head['type'] == "BOOL") {
					$options = array(
						'id' => "edit_".$fld,
						'name' => $fld,
						'small_size' => true,
						'other' => "class='is_bool_input'",
					);
					$x = $displayObject->createFormField($h_type, $options, "0");
					echo $x['display'];
					$js .= $x['js'];
				}
				echo "</td>
		</tr>";
			}

			?>
		</table>
	</form>
</div>
<div id=dlg_confirm></div>
<script type="text/javascript">
	$(function () {
		var ref_tag = '<?php echo $ref_tag; ?>';
		<?php echo $js; ?>
		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});

		$("#div_edit").dialog({
			modal: true,
			autoOpen: false,
			width: "auto",
			buttons: [{
				text: "<?php echo addslashes($myObject->getActivityName("save")); ?> Changes",
				icons: {primary: "ui-icon-disk"},
				click: function (e) {
					e.preventDefault();
					AssistHelper.processing();
					var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_TARGETTYPE.EDIT", AssistForm.serialize($("form[name=frm_edit]")));
					if (result[0] == "ok") {
						AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				}
			}, {
				text: "<?php echo addslashes($myObject->getActivityName("deactivate")); ?>",
				icons: {primary: "ui-icon-trash"},
				click: function (e) {
					e.preventDefault();
					var display_text = "Are you sure you wish to <?php echo addslashes(strtolower($myObject->getActivityName("deactivate"))); ?> this item?";
					confirmSubmission(display_text, "confirm", "DEACTIVATE", $("#div_edit #object_id").val());
				}
			}, {
				text: "Cancel",
				icons: {primary: "ui-icon-closethick"},
				click: function (e) {
					e.preventDefault();
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "div_edit");
		AssistHelper.formatDialogButtonsByClassWithHover($("#div_edit"), 0, "ui-button-bold-green", "ui-button-bold-orange");
		AssistHelper.formatDialogButtonsByClassWithHover($("#div_edit"), 1, "ui-button-minor-grey", "ui-button-minor-red");
		AssistHelper.formatDialogButtonsByClassWithHover($("#div_edit"), 2, "ui-button-minor-grey", "ui-button-minor-orange");


//ADD button
		$("button.btn-add").button({
			icons: {primary: "ui-icon-plus"}
		}).removeClass("ui-state-default").addClass("ui-button-state-green")
			.hover(function () {
				$(this).removeClass("ui-button-state-green").addClass("ui-button-state-orange")
			}, function () {
				$(this).addClass("ui-button-state-green").removeClass("ui-button-state-orange")
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var err = false;
			var code = "";
			var dta = "";
			//check each item for missing data - assume all are required.
			$("#add_row").find("input, select").each(function () {
				$(this).removeClass("required");
				var v = $(this).val();
				if (v.length == 0 || v == "X") {
					$(this).addClass("required");
					err = true;
				} else {
					if ($(this).prop("id") == "code") {
						code = v;
					}
					dta += (dta.length > 0 ? "&" : "") + $(this).prop("name") + "=" + v;
				}
			});
			if (err) {
				AssistHelper.finishedProcessing("error", "Please complete the required fields as highlighted.");
			} else {
				//check for unique code
				$("input.old-code").each(function () {
					var v = $(this).val();
					if (v == code) {
						err = true;
						$("#code").addClass("required");
						AssistHelper.finishedProcessing("error", "The code you have entered has already been used.  Please enter a unique code.");
					}
				});
				if (!err) {
					var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_TARGETTYPE.ADD", dta);
					if (result[0] == "ok") {
						AssistHelper.finishedProcessingWithRedirect("ok", result[1], document.location.href);
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				}
			}
		});


//EDIT button
		$("button.btn-edit").button({
			icons: {primary: "ui-icon-pencil"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(function () {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange")
			}, function () {
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange")
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var i = $(this).attr("object_id");
			var me = AssistHelper.doAjax("inc_controller.php?action=SETUP_TARGETTYPE.GETRAWOBJECTDETAILS", "object_id=" + i);
			$("#div_edit").find("input, select, label").each(function () {
				var v = "";
				switch ($(this).prop("id")) {
					case "object_id":
						v = i;
						break;
					case "lbl_id":
						v = ref_tag + i;
						break;
					default:
						if ($(this).hasClass("is_bool_input")) {
							f = AssistString.substr($(this).prop("id"), 5, $(this).prop("id").length);
							v = me[f];
						} else {
							v = me[$(this).prop("id")];
						}
						break;
				}
				if ($(this).hasClass("is_label")) {
					$(this).text(v);
				} else if ($(this).hasClass("is_bool_input")) {
					if (v * 1 == 0) {
						$("#div_edit #" + $(this).prop("id") + "_no").trigger("click");
					} else {
						$("#div_edit #" + $(this).prop("id") + "_yes").trigger("click");
					}
				} else {
					$(this).val(v);
				}
			});
			AssistHelper.closeProcessing();
			$("#div_edit").dialog("open");
		});


//RESTORE button
		$("button.btn-restore").button({
			icons: {primary: "ui-icon-check"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(function () {
				$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange")
			}, function () {
				$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange")
			}).click(function (e) {
			e.preventDefault();
			var i = $(this).attr("object_id");
			var display_text = "Are you sure that you want to <?php echo addslashes(strtolower($myObject->getActivityName("RESTORE")." ".$myObject->getObjectName($myObject->getMyObjectType()))." ".$ref_tag); ?>" + i + "?";
			confirmSubmission(display_text, "confirm", "RESTORE", i);

		});

		$("#code").addClass("center");


		function confirmSubmission(display_text, next_step, action, id) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>' + display_text + '</p><label id=lbl_hide></label>');
			if (next_step == 'ok') {
				var buttons = [{
					text: "Ok", click: function () {
						$(this).dialog("close");
					}
				}];
			} else if (next_step == 'confirm') {
				var buttons = [
					{
						text: "Confirm & Continue", click: function () {
							$(this).dialog("close");
							nextStep(action, id);
						}, class: 'ui-state-ok'
					},
					{
						text: "Cancel", click: function () {
							$(this).dialog("close");
						}, class: 'ui-state-error'
					}
				];
			} else {
				var buttons = [
					{
						text: "Yes", click: function () {
							nextStep(action, id);
							$(this).dialog("close");
						}, class: 'ui-state-ok'
					},
					{
						text: "No", click: function () {
							$(this).dialog("close");
						}, class: 'ui-state-error'
					}
				];
			}
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}

		function nextStep(action, id) {
			AssistHelper.processing();
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_TARGETTYPE." + action, "object_id=" + id);
			AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
		}

	});

</script>


</body>
</html>

