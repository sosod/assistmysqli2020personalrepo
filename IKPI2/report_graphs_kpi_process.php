<?php
/**
 * @var SDBP6_DEPTKPI $myObject - set to DEPTKPI for dev purposes, could also be SDBP6_TOPKPI
 * @var SDBP6_HEADINGS $headingObject - from inc_header
 * @var SDBP6_HELPER $helper - from inc_header
 * @var int $mscoa_version_id - from inc_header
 */
$no_page_heading = true;
require_once("inc_header.php");

markTime("start of page");


//ASSIST_HELPER::arrPrint($_REQUEST);
/*
Departmental SDBIP Report
Report drawn on 04 October 2019 at 12:23
for the months of July 2018 to June 2019.
*/


//get variables
$passed_request = $_REQUEST;
$sdbip_id = $_REQUEST['sdbip_id'];
unset($passed_request['sdbip_id']);
$object_type = $_REQUEST['object_type'];
unset($passed_request['object_type']);
$class_name = "SDBP6_".$object_type;
/**  */
$myObject = new $class_name();
$myObject->setSDBIPID($sdbip_id);
$include_notmeasured = $_REQUEST['kpi_notmeasured'] == "INCLUDE";
unset($passed_request['kpi_notmeasured']);
$separate_met = $_REQUEST['kpi_group'] == "SEPARATE";
unset($passed_request['kpi_group']);
$start_time = $_REQUEST['start_time_id'];
unset($passed_request['start_time_id']);
$end_time = $_REQUEST['end_time_id'];
unset($passed_request['end_time_id']);
$dir_id = $_REQUEST[$myObject->getDepartmentFieldName()];
unset($passed_request[$myObject->getDepartmentFieldName()]);
$is_sub = false;
$show_subs = $_REQUEST['do_group'] == 1 ? true : false;
unset($passed_request['do_group']);
if($dir_id == "ALL" || $dir_id == "0" || $dir_id == "X") {
	$dir_id = "ALL";
//AA-487 JC 9 Oct 2020 - TOPKPI-specific sub selection settings
} elseif($myObject->getMyObjectType() == "TOPKPI") {
	$is_sub = true;
//	$show_subs = false;
} else {
	$d = explode("_", $dir_id);
	$dir_id = $d[1];
	if($d[0] == "SUB") {
		$is_sub = true;
//		$show_subs = false;
	}
}
$report_title = $_REQUEST['report_title'];
unset($passed_request['report_title']);
$graph_type = $_REQUEST['graph_type'];
unset($passed_request['graph_type']);
$g = explode("_", $graph_type);
$is_bar = false;
$is_perc = false;
$is_pie_above = false;
if($g[0] == "BAR") {
	$is_bar = true;
	if($g[1] == "PERC") {
		$is_perc = true;
	}
} else {
	if($g[1] == "ABOVE") {
		$is_pie_above = true;
	}
}

$filter_id = $dir_id;
$passed_request['dir_filter'] = $filter_id;

//if($show_subs) {
$g = explode("|", $_REQUEST['group_by']);
$group_by = array(
	'type' => $g[0],
	'table' => $g[1],
	'field' => $g[2]
);
if($is_sub && $group_by['type']=="OBJECT" && $group_by['table']=="SDBP6_SETUP_ORGSTRUCTURE") {
	$show_subs = false;
	$group_by = false;
	$group_heading = "Grouping";
} else {
	$group_heading = $helper->replaceAllNames($headingObject->getAHeadingNameByField($g[2]));
}
unset($passed_request['group_by']);

//filter heading
//TODO: Make this MULTIPLE FILTER FRIENDLY! filter heading needs to be an array to implode
$filter_heading = "";
if(count($passed_request) > 1) { //check for more than 1 (dir_filter added automatically)
	foreach($passed_request as $filter_fld => $original_value) {
		if($filter_fld != "dir_filter") {
			//convert base64_encoded value to correct structure
			$a = explode("|", $original_value);
			$filter_value = $a[0];
			$passed_request[$filter_fld] = $filter_value;
			//if value = ALL then no need to filter so pull out, otherwise carry on processing
			if($filter_value == "ALL") {
				unset($passed_request[$filter_fld]);
			} else {
				//if filter value is set to hard coded "UNSPECIFIED" then change to correct value of 0 before carrying on processing
				if($filter_value == "UNSPECIFIED") {
					$passed_request[$filter_fld] = 0;
					$filter_text = $helper->getUnspecified();
				} else {
					$filter_text = base64_decode($a[1]);
				}
				$filter_heading = "Filtered for ".$helper->replaceAllNames($headingObject->getAHeadingNameByField($filter_fld)).": ".$filter_text;
			}
		}
	}
}


markTime("start of object processing");

//ASSIST_HELPER::arrPrint($passed_request);
//ASSIST_HELPER::arrPrint($group_by);

//get object results

if(strlen($report_title) == 0) {
	$report_title = $myObject->getObjectName($object_type)." Report";
}
$results = $myObject->getSummaryOfResultsForGraphReporting($sdbip_id, $passed_request, $group_by, array('start' => $start_time, 'end' => $end_time), $is_sub,$mscoa_version_id);
//ASSIST_HELPER::arrPrint($results);
$count = isset($results['results']['ALL']) ? $results['results']['ALL'] : 0;
$sub_count = $results['results'];
$time = $results['time'];


markTime("end of get results - start of result settings");


//get results settings
$resultObject = new SDBP6_SETUP_RESULTS();
$result_settings = $resultObject->getMyActiveResultSettings(true);
//$result_settings = array();
if(!$include_notmeasured) {
	$not_measured_code = $result_settings[0]['code'];
	unset($result_settings[0]);
//AA-483 JC 9 Oct 2020 - centralised application of not_measured selection so that both bar & pie graphs get the update
	$total_kpis_including_not_measured = array_sum($count);
	unset($count[$not_measured_code]);
	foreach($sub_count as $s_id => $sc) {
		unset($sub_count[$s_id][$not_measured_code]);
	}
}
if(!$separate_met) {
	$r3 = $result_settings[3]['code'];
	$r4 = $result_settings[4]['code'];
	$r5 = $result_settings[5]['code'];
	$count[$r3] += isset($count[$r4]) ? $count[$r4] : 0;
	$count[$r3] += isset($count[$r5]) ? $count[$r5] : 0;
	foreach($sub_count as $org_id => $c) {
		if(!isset($sub_count[$org_id][$r3])) {
			$sub_count[$org_id][$r3] = 0;
		}
		$sub_count[$org_id][$r3] += isset($c[$r4]) ? $c[$r4] : 0;
		$sub_count[$org_id][$r3] += isset($c[$r5]) ? $c[$r5] : 0;
	}
	unset($result_settings[4]);
	unset($result_settings[5]);
}
foreach($result_settings as $i => $r) {
	$result_settings[$i]['text'] = $r['code'];
}

//ASSIST_HELPER::arrPrint($result_settings);


markTime("start of echo");

echo "
<h1 class=center style=\"margin-top: 0;margin-bottom: 0;\">".$report_title."</h1>
<p class=\"center i\" style=\"margin-top: 0px; font-size: 7pt;\">Report drawn on ".date("d F Y")." at ".date("H-i")."<br />for the months of ".$time[$start_time]['name']." to ".$time[$end_time]['name'].(strlen($filter_heading) > 0 ? "<br />".$filter_heading : "").".</p>
";


if($is_bar) {
	/******************************************************
	 * BAR GRAPHS
	 */


	$kr2 = array();
	foreach($result_settings as $ky => $k) {
		$k['obj'] = isset($results['results']['ALL'][$k['code']]) ? $results['results']['ALL'][$k['code']] : 0;
		$val = array();
		foreach($k as $key => $s) {
			$val[] = $key.":\"".$s."\"";
		}
		$kr2[$ky] = implode(",", $val);
	}

	$chartData = "{".implode("},{", $kr2)."}";
	if($show_subs) {
		$sub_chartData = array();
		foreach($results['org_list'] as $s_id => $s) {
			if(isset($sub_count[$s_id])) {
				$sval = explode(" ", ASSIST_HELPER::decode($s));
				$s_name = "";
				$s_n = "";
				foreach($sval as $sv) {
					if(strlen($s_n.$sv) > 20) {
						$s_name .= $s_n."\\n";
						$s_n = "";
					}
					$s_n .= " ".$sv;
				}
				$s_name .= " ".$s_n;
				$cD = "{text:\"".$s_name."\"";
				foreach($result_settings as $ky => $k) {
					$cD .= ",R".$k['id'].":".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']] : 0);
				}
				$cD .= "}";
				$sub_chartData[] = $cD;
			}
		}
	}
	$graph_width = (isset($sub_chartData) ? count($sub_chartData) * 100 : 0) + 100;

	?>
	<script type="text/javascript">
		var chart;
		var legend;
		var chartData = [<?php echo $chartData; ?>];
		<?php if($show_subs) { ?>
		var sub_chartData = [<?php echo implode(",", $sub_chartData) ?>];
		<?php } ?>
		$(document).ready(function () {
			dir_chart = new AmCharts.AmPieChart();
			dir_chart.color = "#ffffff";
			dir_chart.dataProvider = chartData;
			dir_chart.titleField = "value";
			dir_chart.valueField = "obj";
			dir_chart.colorField = "color";
			dir_chart.labelText = "[[percents]]%";
			dir_chart.labelRadius = -25;
			dir_chart.angle = 10;
			dir_chart.depth3D = 15;
			dir_chart.hideLabelsPercent = 0;
			dir_chart.hoverAlpha = 0.5;
			dir_chart.startDuration = 0;
			dir_chart.radius = 150;
			dir_chart.marginBottom = 30;
			dir_chart.write("dir");

			<?php if($show_subs) { ?>
			sub_chart = new AmCharts.AmSerialChart();
			sub_chart.color = "#ffffff";
			sub_chart.dataProvider = sub_chartData;
			sub_chart.categoryField = "text";
			sub_chart.marginLeft = 47;
			sub_chart.marginTop = 30;
			sub_chart.marginBottom = 75;
			sub_chart.fontSize = 9;
			sub_chart.angle = 45;
			sub_chart.depth3D = 15;
			sub_chart.plotAreaBorderAlpha = 0.2;
			sub_chart.rotate = false;
			<?php } ?>

			<?php foreach($result_settings as $ky => $k) { ?>
			var graph = new AmCharts.AmGraph();
			graph.title = "<?php echo $k['value']; ?>";
			graph.labelText = "[[percents]]%";
			graph.valueField = "R<?php echo $k['id']; ?>";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "<?php echo $k['color']; ?>";
			graph.balloonText = "[[percents]]%";
			<?php if($show_subs) { ?>
			sub_chart.addGraph(graph);
			<?php } } ?>

			var valAxis = new AmCharts.ValueAxis();
			valAxis.stackType = "<?php echo($is_perc ? "100%" : "regular"); ?>";	//100%
			valAxis.gridAlpha = 0.1;
			valAxis.axisAlpha = 0;
			valAxis.color = "#000000";
			<?php if($show_subs) { ?>
			sub_chart.addValueAxis(valAxis);

			sub_chart.categoryAxis.gridAlpha = 0.1;
			sub_chart.categoryAxis.axisAlpha = 0;
			sub_chart.categoryAxis.color = "#000000";
			sub_chart.categoryAxis.fontSize = 7;
			sub_chart.categoryAxis.labelRotation = 0;
			sub_chart.categoryAxis.autoGridCount = true;
			sub_chart.categoryAxis.gridCount = 1;
			sub_chart.categoryAxis.gridPosition = "start";

			sub_chart.write("sub");
			<?php } ?>
		});
	</script>
	<div align=center>
		<table class=noborder>
			<tr class=no-highlight>
				<td class="center noborder"><span style="font-weight: bold;"><?php echo $results['org_name']; ?></span>
					<div id="dir" style="width:400px; height:350px; background-color:#ffffff;"></div>
				</td>
				<?php if($show_subs) { ?>
					<td class="center noborder"><span style="font-weight: bold;"><?php echo $group_heading; ?></span>
						<div id="sub" style="width:<?php echo $graph_width; ?>px; height:400px; background-color:#ffffff;"></div>
					</td>
				<?php } ?>
			</tr>
			<tr class=no-highlight>
				<td colspan=2 class=noborder>
					<div align=center>
						<table>
							<thead>
							<tr>
								<td rowspan=2 style="border-top-color: #ffffff; border-left-color: #ffffff;"></td>
								<th rowspan=2><?php echo str_replace(" ", "<br />", $results['org_name']); ?></th>
								<?php if($show_subs) { ?>
									<th colspan=<?php echo count($results['org_list']); ?>><?php echo $group_heading; ?></th><?php } ?>
							</tr>
							<?php if($show_subs) { ?>
								<tr>
									<?php
									foreach($results['org_list'] as $s_id => $s) {
										echo "<th class=th2 style=\"font-size: 7pt;\">".str_replace(" ", "<br />", $s)."</th>";
									}
									?>
								</tr>
							<?php } ?>
							</thead>
							<tbody>
							<?php
							foreach($result_settings as $ky => $k) {
								echo "<tr>
				<td style=\"font-weight: bold;\"><span style=\"background-color: ".$k['color']."\">&nbsp;&nbsp;</span>
				".$k['value']."</td>";
								echo "<td class=center style=\"font-weight: bold; background-color: #dddddd;\">".(isset($count[$k['code']]) ? $count[$k['code']]." (".number_format(($count[$k['code']] / array_sum($count) * 100), 2)."%)" : "-")."</td>";
								if($show_subs) {
									foreach($results['org_list'] as $s_id => $s) {
										echo "<td class=center>".(isset($sub_count[$s_id][$k['text']]) ? $sub_count[$s_id][$k['text']]." (".number_format(($sub_count[$s_id][$k['text']] / array_sum($sub_count[$s_id]) * 100), 2)."%)" : "-")."</td>";
									}
								}
								echo "</tr>";
							}
							?>
							</tbody>
							<tfoot>
							<tr>
								<td class=right rowspan=2 style="background-color: #dddddd;"><b>Total:</b></td>
								<td class="center" style="background-color: #555555; color: #ffffff; font-weight: bold;"><?php echo (is_array($count) ? array_sum($count) : $count).(!$include_notmeasured ? "*" : ""); ?></td>
								<?php
								if($show_subs) {

									foreach($results['org_list'] as $s_id => $s) {
										echo "<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? array_sum($sub_count[$s_id]) : "-")."</b></td>";
									}
								}
								?>
							</tr>
							<tr>
								<td class="center" style="background-color: #555555; color: #ffffff; font-weight: bold;">100%</td>
								<?php
								if($show_subs) {
									foreach($results['org_list'] as $s_id => $s) {
										echo "<td class=center style=\"background-color: #dddddd;\"><b>".(isset($sub_count[$s_id]) ? number_format((array_sum($sub_count[$s_id]) / array_sum($count) * 100), 2)."%" : "-")."</b></td>";
									}
								}
								?>
							</tr>
							</tfoot>
						</table>
						<?php
						if(!$include_notmeasured) {
							echo "<P class=i>* Excludes ".($total_kpis_including_not_measured - array_sum($count))." ".$myObject->replaceAllNames("|KPIS|")." which had no targets/actuals for the period selected.</P>";
						}
						?>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<?php
} else {

	/*************************************************************
	 * PIE GRAPHS
	 */

//ASSIST_HELPER::arrPrint($result_settings);
//ASSIST_HELPER::arrPrint($results);

	$kr2 = array();
	foreach($result_settings as $ky => $k) {
		$k['obj'] = isset($results['results']['ALL'][$k['code']]) ? $results['results']['ALL'][$k['code']] : 0;
		$val = array();
		foreach($k as $key => $s) {
			$val[] = $key.":\"".$s."\"";
		}
		$kr2[$ky] = implode(",", $val);
	}

	$chartData = "{".implode("},{", $kr2)."}";

	if($show_subs) {
		$sub_chartData = array();
		foreach($results['org_list'] as $s_id => $s) {
			if(isset($sub_count[$s_id])) {
				foreach($result_settings as $ky => $k) {
					$k['obj'] = isset($sub_count[$s_id][$k['code']]) ? $sub_count[$s_id][$k['code']] : 0;
					$val = array();
					foreach($k as $key => $s) {
						$val[] = $key.":\"".$s."\"";
					}
					$kr2[$ky] = implode(",", $val);
				}

				$sub_chartData['sub_'.$s_id] = "{".implode("},{", $kr2)."}";
			}
		}
	}
	$graph_width = (isset($sub_chartData) ? count($sub_chartData) * 100 : 0) + 100;

	?>
	<script type="text/javascript">
		var chart;
		var legend;
		var chartData = [<?php echo $chartData; ?>];
		<?php
		if($show_subs) {
			foreach($results['org_list'] as $s_id => $s) {
				echo "var subChartData_".$s_id." = [".$sub_chartData['sub_'.$s_id]."];";
			}
		}
		?>
		$(document).ready(function () {
			dir_chart = new AmCharts.AmPieChart();
			dir_chart.color = "#ffffff";
			dir_chart.dataProvider = chartData;
			dir_chart.titleField = "value";
			dir_chart.valueField = "obj";
			dir_chart.colorField = "color";
			dir_chart.labelText = "[[percents]]%";
			dir_chart.labelRadius = -25;
			dir_chart.angle = 20;
			dir_chart.depth3D = 15;
			dir_chart.hideLabelsPercent = 0;
			dir_chart.hoverAlpha = 0.5;
			dir_chart.startDuration = 0;
			dir_chart.radius = 150;
			dir_chart.marginBottom = 30;
			dir_chart.write("dir");

			<?php foreach($result_settings as $ky => $k) { ?>
			var graph = new AmCharts.AmGraph();
			graph.title = "<?php echo $k['value']; ?>";
			graph.labelText = "[[percents]]%";
			graph.valueField = "R<?php echo $k['id']; ?>";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			graph.lineColor = "<?php echo $k['color']; ?>";
			graph.balloonText = "[[percents]]%";
			<?php } ?>

			<?php
			if($show_subs) {
			foreach($results['org_list'] as $s_id => $s) {
			?>
			<?php echo "sub_chart_".$s_id; ?> = new AmCharts.AmPieChart();
			<?php echo "sub_chart_".$s_id; ?>.
			color = "#ffffff";
			<?php echo "sub_chart_".$s_id; ?>.
			dataProvider = subChartData_<?php echo $s_id; ?>;
			<?php echo "sub_chart_".$s_id; ?>.
			titleField = "value";
			<?php echo "sub_chart_".$s_id; ?>.
			valueField = "obj";
			<?php echo "sub_chart_".$s_id; ?>.
			colorField = "color";
			<?php echo "sub_chart_".$s_id; ?>.
			labelText = "[[percents]]%";
			<?php echo "sub_chart_".$s_id; ?>.
			labelRadius = -25;
			<?php echo "sub_chart_".$s_id; ?>.
			angle = 10;
			<?php echo "sub_chart_".$s_id; ?>.
			depth3D = 10;
			<?php echo "sub_chart_".$s_id; ?>.
			hideLabelsPercent = 0;
			<?php echo "sub_chart_".$s_id; ?>.
			hoverAlpha = 0.5;
			<?php echo "sub_chart_".$s_id; ?>.
			startDuration = 0;
			<?php echo "sub_chart_".$s_id; ?>.
			radius = 100;
			<?php echo "sub_chart_".$s_id; ?>.
			marginBottom = 30;
			<?php echo "sub_chart_".$s_id; ?>.
			write("<?php echo "sub_".$s_id; ?>");
			<?php
			}
			}
			?>


		});
	</script>
	<div align=center>
		<table class=noborder>
			<tr class=no-highlight>
				<td class="center noborder" colspan="3">
					<table class=noborder width=100%>
						<tr class=no-highlight>
							<td class="center noborder" colspan=2><span style="font-weight: bold;font-size:10pt;"><?php echo $results['org_name']; ?></span></td>
						</tr>
						<tr class=no-highlight>
							<td width=50% class="right  noborder">
								<div id="dir" style="width:400px; height:400px; background-color:#ffffff;float:right"></div>
							</td>
							<td width=50% class="middle noborder">
								<table>
									<tbody>
									<?php
									//AA-483 JC 9 Oct 2020 - need to use the count & sub_count so that % calc uses not_measured selected
									$c = $count;//$results['results']['ALL'];
									$total_count = array_sum($c);//array_sum($results['results']['ALL']);
									foreach($result_settings as $r_id => $r) {
										$x = isset($c[$r['code']]) ? $c[$r['code']] : 0;
										$p = round($x / $total_count * 100, 2);
										echo "
										<tr>
											<td style='font-weight: bold;'><span style='background-color: ".$r['color']."'>&nbsp;&nbsp;</span> ".$r['value']."</td>
											<td class=center style='font-weight: bold;'>".($x)." (".$p."%)</a></td>
										</tr>";
									}
									?>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php
			if($show_subs) {
			?>
			<tr class=no-highlight>
				<td colspan="3" class="center b noborder"><?php echo $group_heading; ?></td>
			</tr>
			<tr>
				<?php
				$row_count = 0;
				foreach($results['org_list'] as $s_id => $s) {
					$sval = explode(" ", ASSIST_HELPER::decode($s));
					$s_name = "";
					$s_n = "";
					foreach($sval as $sv) {
						if(strlen($s_n.$sv) > 30) {
							$s_name .= $s_n."<br />";
							$s_n = "";
						}
						$s_n .= " ".$sv;
					}
					$s_name .= " ".$s_n;

					echo "
							<td class='center noborder'>
							<span class='center i'>".$s_name."</span>
							<div id='sub_".$s_id."'  style='width:230px; height:230px; background-color:#ffffff;'></div>
							<table>
									<tbody>";
					//AA-483 JC 9 Oct 2020 - need to use the count & sub_count so that % calc uses not_measured selected
					$c = $sub_count[$s_id];//$results['results'][$s_id];
					$total_count = array_sum($c);//array_sum($results['results'][$s_id]);
					foreach($result_settings as $r_id => $r) {
						$x = isset($c[$r['code']]) ? $c[$r['code']] : 0;
						$p = round($x / $total_count * 100, 2);
						echo "
										<tr>
											<td style='font-weight: bold;'><span style='background-color: ".$r['color']."'>&nbsp;&nbsp;</span> ".$r['value']."</td>
											<td class=center style='font-weight: bold;'>".($x)." (".$p."%)</a></td>
										</tr>";
					}
					echo "
									</tbody>
								</table>
							</td>
							";
					$row_count++;
					if($row_count == 3) {
						echo "</tr><tr>";
						$row_count = 0;
					}
				}
				}
				?>
				</td>
			</tr>
		</table>
	</div>
	<?php


}
markTime("end of page");
?>