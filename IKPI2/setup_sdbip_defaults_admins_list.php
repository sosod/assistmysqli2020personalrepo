<?php
/**
 * @var string $object_type - from calling page
 * @var SDBP6_SETUP_ADMINS $adminObject - from calling page
 * @var SDBP6_SETUP_ORGSTRUCTURE $orgObject - from calling page
 * @var string $dialog_redirect - where to go to when done - from calling page
 *
 * @var int $sdbip_id - from inc_header
 * @var SDBP6 $helper - from inc_header
 * @var SDBP6_SDBIP $sdbipObject - from inc_header
 * @var SDBP6_DISPLAY $displayObject - from inc_header
 * @var string $js - placeholder for javascript/jquery - from inc_header
 */
$dialog_url = "setup_sdbip_defaults_admins_object.php";

$field_names = $adminObject->getFieldNames($object_type);
$field_defaults = $adminObject->getFieldDefaults();
$field_glossary = $adminObject->getFieldGlossary();
//ASSIST_HELPER::arrPrint($field_names);

if($object_type == "OWNER") {
	$listObject = new SDBP6_LIST("owner");
	$listObject->setSDBIPID($sdbip_id);
	$temp = $listObject->getActiveListItemsFormattedForSelect();
	$structure = array(0 => array());
	$c = 0;
	foreach($temp as $id => $name) {
		$structure[0][$id] = array(
			'id' => $id,
			'ref' => $id,
			'parent_id' => 0,
			'display_order' => $c,
			'name' => $name,
		);
		$c++;
	}
	$highlight_sections = false;
	$group_tables = false;
	$section_name = $helper->getObjectName("RESPONSIBLE");
} else {
	$structure = $orgObject->getOrgStructureFromDBForSetup(true, $sdbip_id);
	$highlight_sections = true;
	$group_tables = true;
	$section_name = $helper->getObjectName("ORGSTRUCTURE");
}
//ASSIST_HELPER::arrPrint($structure);

$admins = $adminObject->getAdminsForSetup($object_type);
//ASSIST_HELPER::arrPrint($admins);

$top_level_orgstructure_only_sections = $adminObject->getTopLevelOnlySections();


function drawTableRow($tier, $id, $row) {
	global $admins;
	global $field_names;
	global $adminObject;
	global $structure;
	global $top_level_orgstructure_only_sections;
	global $highlight_sections;

	$head_admins = isset($admins[$id]) ? array_keys($admins[$id]) : array();
	$rowspan = count($head_admins) == 0 ? 1 : count($head_admins);
	$indent = $tier > 1 ? "&nbsp;&nbsp;-&nbsp;" : "";
	echo "
	<tr class=tier".$tier.">
		<td rowspan=".$rowspan.">".$indent."<span class=org_name>".$row['name']." [".$row['ref']."]</span>
			<button class='action-button btn-add' object_id=0 parent_id=".$id." tier=".$tier.">".$adminObject->getActivityName("add")."</button>
		</td>
		";
	if(count($head_admins) == 0) {
		echo "<td>&nbsp;</td>";
		$x = 1;
		foreach($field_names as $sec => $section) {
			$x++;
			echo "<td colspan=".count($section['access'])." ".($x % 2 == 0 && $highlight_sections == true ? "class=highlight" : "").">&nbsp;</td>";
		}
		echo "<td style='border:1px solid #ffffff'>&nbsp;</td>";
	} else {
		$head_count = 0;
		foreach($head_admins as $head_tkid) {
			$the_admin = $admins[$id][$head_tkid];
			$td_class = ($the_admin['status'] != true ? "inact" : "");
			if($head_count > 0) {
				echo "
				</tr><tr class='tier".$tier."'>";
			}
			echo "<td class='$td_class'>".$the_admin['name']."</td>";
			$x = 1;
			foreach($field_names as $sec => $section) {
				$x++;
				foreach($section['access'] as $key => $field) {
					if(!in_array($sec, $top_level_orgstructure_only_sections) || $tier == 1) {
						$access = isset($the_admin[$sec][$key]) ? $the_admin[$sec][$key] : false;
						echo "<td style='text-align:center' class='$td_class ".($x % 2 == 0 && $td_class != "inact" && $highlight_sections == true ? "highlight" : "")."'>".$adminObject->getDisplayIcon(($access == true ? "ok" : "error"), "", ($the_admin['status'] != true ? "888888" : ""))."</td>";
					} else {
						echo "<td style='text-align:center' class='$td_class ".($x % 2 == 0 && $td_class != "inact" && $highlight_sections == true ? "highlight" : "")."'>-</td>";
					}
				}
			}
			echo "<td class='' style='border: 1px solid #FFFFFF'>";
			if($the_admin['status'] == true) {
				echo "<button class='action-button btn-edit' object_id='".$head_tkid."' parent_id=".$id.">".$adminObject->getActivityName("edit")."</button>";
			} else {
				echo "<button class='btn-restore' object_id='".$head_tkid."' parent_id=".$id.">".$adminObject->getActivityName("restore")."</button>";
			}
			echo "</td>";
			$head_count++;
		}
	}
	echo "
	</tr>";
	if(isset($structure[$id]) && count($structure[$id]) > 0) {
		foreach($structure[$id] as $sub_id => $sub) {
			drawTableRow(2, $sub_id, $sub);
		}
	}

}


if(!isset($structure[0]) || count($structure[0]) == 0) {
	if($object_type == "OWNER") {
		$route = "<span class=' i'>".$sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_setup_sdbip_defaults_lists"), "|"))."</span>";;
		$result = array("error", "No ".$helper->getObjectName("OWNERS")." available.  Please ensure that the list has been populated in ".$route.".");
	} else {
		$route = "<span class=' i'>".$sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_setup_sdbip_defaults_orgstructure"), "|"))."</span>";;
		$result = array("error", "No ".$helper->getObjectName("ORGSTRUCTURE")." available.  Please ensure that the list has been populated in ".$route.".");
	}
	ASSIST_HELPER::displayResult($result);
	die();
}
?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<table id=tbl_admins style="border:0px solid #fff">
				<?php
				$table_count = 0;
				foreach($structure[0] as $head_id => $head) {
					if($table_count > 0 && $group_tables == true) {
						echo "<tr><td class=full-holder>&nbsp;</td><td style='border:1px solid #ffffff'></td></tr>";
					}
					if($table_count == 0 || $group_tables == true) {
						?>
						<tr>
							<th rowspan=2><?php echo $section_name; ?></th>
							<th rowspan=2><?php echo $adminObject->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL); ?></th>
							<?php
							foreach($field_names as $section) {
								echo "<th colspan=".count($section['access']).">".$section['name']."</th>";
							}
							?>
							<td rowspan=2 style='border:1px solid #FFFFFF'></td>
						</tr>
						<tr><?php
						foreach($field_names as $section) {
							foreach($section['access'] as $key => $field) {
								echo "<th class=th2>".str_replace(" ", "<br />", $field)."</th>";
							}
						}
						?></tr><?php
					}
					$table_count++;
					drawTableRow(1, $head_id, $head);
				}
				//$v = substr()
				?>
			</table>
			<?php
			$js .= $displayObject->drawPageFooter($helper->getGoBack('setup_sdbip_defaults.php'), "setup", array('section' => "ADMIN_".$object_type, 'sdbip_id' => $sdbip_id));
			?>
		</td>
	</tr>
</table>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		var w = (parseInt($("#tbl_admins th.th2:last").css("width"))) + 5;
		$("#tbl_admins th.th2").css("width", w + "px");
		$("#tbl_admins td.holder").prop("colspan", $("#tbl_admins th.th2").size() + 2);
		$("#tbl_admins td.full-holder").prop("colspan", $("#tbl_admins th.th2").size() + 2).css("border", "1px solid #ffffff");
		$("#tbl_admins tr").find("span.ui-icon").css("margin", "0 auto");
		$("tr.tier1 td").addClass("b");
		$("tr.tier2 td").addClass("i");
		$("td.highlight").css("background-color", "rgb(245,245,245)");//"#f7f7ff");
		$("td.inact").css("background-color", "#dddddd").css("color", "#888888");//.find("span.ui-icon").removeClass("ui-state-ok").removeClass("ui-state-error").addClass("ui-button-minor-grey");

		$("button.btn-add").button({
			icons: {primary: "ui-icon-plus"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-green").addClass("float")
			.hover(
				function () {
					$(this).removeClass("ui-button-minor-green").addClass("ui-button-minor-orange");
				}, function () {
					$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-green");
				}
			).click(function (e) {
			e.preventDefault();
		})
			.children(".ui-button-text")
			.css({"padding-top": "2px", "padding-bottom": "0px"});

		$("button.btn-edit").button({
			icons: {primary: "ui-icon-pencil"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey").addClass("center")
			.hover(
				function () {
					$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
				}, function () {
					$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
				}
			).click(function (e) {
			e.preventDefault();
		})
			.children(".ui-button-text")
			.css({"padding-top": "2px", "padding-bottom": "0px"});

		$("button.btn-restore").button({
			icons: {primary: "ui-icon-check"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey").addClass("center")
			.hover(
				function () {
					$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
				}, function () {
					$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
				}
			).click(function (e) {
			e.preventDefault();
			var dta = "sdbip_id=<?php echo $sdbip_id; ?>&parent_id=" + $(this).attr("parent_id") + "&object_type=<?php echo $object_type; ?>&object_id=" + $(this).attr("object_id");
			var admin_name = $(this).closest("tr").find("td.inact:first").html();
			var org_name = $(this).closest("tr").find("td:first").find("span.org_name").html();
			var display_text = "Are you sure you wish to <?php echo addslashes($helper->getActivityName("RESTORE")); ?> " + admin_name + " to " + org_name + "?";
			confirmSubmission(display_text, "RESTORE", dta);
		})
			.children(".ui-button-text")
			.css({"padding-top": "2px", "padding-bottom": "0px"});


//ACTIONS


		function confirmSubmission(display_text, action, dta) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>' + display_text + '</p><label id=lbl_hide></label>');
			var buttons = [
				{
					text: "Confirm & Continue", click: function () {
						$(this).dialog("close");
						nextStep(action, dta);
					}, class: 'ui-state-ok'
				},
				{
					text: "Cancel", click: function () {
						$(this).dialog("close");
					}, class: 'ui-state-error'
				}
			];
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}

		function nextStep(action, dta) {
			AssistHelper.processing();
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ADMINS." + action, dta);
			if (result[0] == "ok") {
				AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		}


	});
</script>


<?php

/************************************************************************************************************
 * DIALOG Add/Edit Section
 */

?>
<div id=dlg_child class=dlg-child title="">
	<iframe id=ifr_form_display style="border:0px solid #000000;" src="">

	</iframe>
</div>
<div id=dlg_confirm></div>
<script type=text/javascript>
	$(function () {
		var my_window = AssistHelper.getWindowSize();
		if (my_window['width'] > 800) {
			var my_width = 850;
		} else {
			var my_width = 800;
		}
		var my_height = my_window['height'] - 50;

		$("div.dlg-child").dialog({
			autoOpen: false,
			modal: true,
			width: my_width,
			height: my_height,
			beforeClose: function () {
				AssistHelper.closeProcessing();
			},
			open: function () {
				$(this).dialog('option', 'width', my_width);
				$(this).dialog('option', 'height', my_height);
			}
		});

		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});

		$("button.action-button").click(function () {
			AssistHelper.processing();
			var object_class = "SETUP_ADMINS";
			var i = $(this).attr("object_id");
			var p = $(this).attr("parent_id");
			var t = "<?php echo $object_type; ?>"; //t = type = OWNER for responsible person
			var dta = "sdbip_id=<?php echo $sdbip_id; ?>&object_type=" + t + "&object_id=" + i + "&parent_id=" + p;
			if ($(this).hasClass("btn_restore")) {
				var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ADMINS.RESTORE", dta);
				if (result[0] == "ok") {
					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], document.location.href);
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			} else {
				$dlg = $("div#dlg_child");
				var act = "view";
				if ($(this).hasClass("btn-edit")) {
					act = "edit";
				} else if ($(this).hasClass("btn-add")) {
					act = "add";
				}
				var obj = t.toLowerCase();
				var heading = AssistString.ucwords(act);
				var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action=" + act + "&" + dta;
				//console.log(url);
				$dlg.dialog("option", "title", heading);
				if (act == "view") {
					$dlg.dialog("option", "buttons", [{
						text: "Close", click: function () {
							$(this).dialog("close");
						}
					}]);
				} else {
					$dlg.dialog("option", "buttons", []);
				}
				//console.log("dlg_w "+my_width);
				//console.log("dlg_h "+my_height);
				var ifr_width = my_width - 25;
				var ifr_height = my_height - 50;
				//console.log("ifr_w "+ifr_width);
				//console.log("ifr_h "+ifr_height);
				//console.log($("#ifr_form_display").prop("width"));
				$("#ifr_form_display").width(ifr_width).height(ifr_height).prop("src", url);
				//console.log($("#ifr_form_display").prop("width"));
				//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
				//$dlg.dialog("open");
			}
		});


	});

	function dialogFinished(icon, result) {
		if (icon == "ok") {
			//alert(result)
			//document.location.href = '<?php echo $dialog_redirect; ?>&r[]=ok&r[]='+result;
			$dlg = $("div#dlg_child");
			$dlg.dialog("close");
			AssistHelper.processing();
			AssistHelper.finishedProcessingWithRedirect(icon, result, '<?php echo $dialog_redirect; ?>');
		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon, result);
		}
	}

</script>


