<?php


if(!isset($_REQUEST['action']) || $_REQUEST['action'] != "GO") {

	require_once("inc_header.php");
	?>

	<div id=div_go style='width:500px;margin:25px auto;padding:10px'>
		<p>This will update all <?php echo $helper->replaceAllNames("|TOPKPIS|"); ?> from their linked <?php echo $helper->replaceAllNames("|DEPTKPIS|"); ?> for <?php echo $sdbip_name." [".$sdbipObject->getRefTag().$sdbip_id."]*"; ?>.<br />&nbsp;</p>
		<p>Are you sure you wish to continue?<br />&nbsp;</p>
		<p class="i">Note: This update will happen automatically at midnight each day and does not need to be manually triggered.</p>
		<p class="center">
			<button id="btn_go">Update</button>
			<button id="btn_cancel">Cancel</button>
		</p>
	</div>
	<p class="center i">* To change the <?php echo $helper->replaceAllNames("|SDBIP|"); ?>, please go to <br /><?php echo $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_admin_top_update"), "|")); ?> or <br /><?php echo $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_admin_top_edit"), "|")); ?> and <br />change (and apply) the <?php echo $helper->replaceAllNames("|SDBIP|"); ?> in the page filter.</p>
	<script type="text/javascript">
		$(function () {
			$("#btn_go").button({
				icons: {primary: "ui-icon-check"}
			}).click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				document.location.href = "admin_top_auto_force.php?action=GO&sdbip_id=<?php echo $sdbip_id; ?>";
			}).removeClass("ui-state-default").addClass("ui-button-state-green")
				.hover(function () {
					$(this).addClass("ui-button-state-orange").removeClass("ui-button-state-green");
				}, function () {
					$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-green");
				});
			$("#btn_cancel").button().click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				document.location.href = "admin_top_update.php";
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(function () {
					$(this).addClass("ui-button-minor-orange").removeClass("ui-button-minor-grey");
				}, function () {
					$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey");
				});
			$("#div_go").addClass("tbl-ok").addClass("light-green-back").css("border", "2px solid #009900");
		});
	</script>
	<?php
	die();

}

require_once("../module/autoloader.php");
include_once("../SDBP6/class/sdbp6_topkpi.php");
$autojob = false;
if(isset($_REQUEST['autojob'])) {
	$autojob = $_REQUEST['autojob'];
}
//Create objects
$sdbipObject = new SDBP6_SDBIP("", $autojob);
$deptObject = new SDBP6_DEPTKPI("", 0, $autojob);
$topObject = new SDBP6_TOPKPI("", 0, $autojob);
$timeObject = new SDBP6_SETUP_TIME("", $autojob);
$targetObject = new SDBP6_SETUP_TARGETTYPE("", 0, $autojob);
$calcTypeObject = new SDBP6_SETUP_CALCTYPE("", $autojob);
$setupObject = new SDBP6_SETUP_PREFERENCES("", $autojob);
$headingObject = new SDBP6_HEADINGS("", $autojob);
//Get values
$echo = "";
//sdbip
if(isset($sdbip_id) && isset($sdbip_details) && count($sdbip_details) > 0) {
	$all_sdbip_details = array($sdbip_id => $sdbip_details);
} elseif(isset($_REQUEST['sdbip_id'])) {
	$sdbip_id = $_REQUEST['sdbip_id'];
	$all_sdbip_details[$sdbip_id] = $sdbipObject->getFormattedActivatedSDBIPDetails($autojob, $sdbip_id, 1);
} else {
	$all_sdbip_details = $sdbipObject->getAllActivatedSDBIPDetails($autojob);
}
$target_errors_found = array();
$actual_errors_found = array();
$kpi_processed_successfully = array();
if(count($all_sdbip_details) > 0) {
	foreach($all_sdbip_details as $sdbip_id => $sdbip_details) {

		if(isset($sdbip_details['sdbip_id'])) {

			$sdbip_id = $sdbip_details['sdbip_id'];
			$sdbip_name = $sdbip_details['name'];
//lists
			$list_of_deptkpi_linked_to_topkpi = $deptObject->getCompleteListOfKPIsLinkedToTL($sdbip_id);
			$top_keys = array_keys($list_of_deptkpi_linked_to_topkpi);
			$list_of_linked_topkpis = $topObject->getListOfLinkedTLKPIsByID($sdbip_id, $top_keys);
			//second validate the lists just in case of faulty links
			foreach($list_of_deptkpi_linked_to_topkpi as $top_id => $d) {
				if(!isset($list_of_linked_topkpis[$top_id])) {
					unset($list_of_deptkpi_linked_to_topkpi[$top_id]);
				}
			}
			$top_keys = array_keys($list_of_linked_topkpis);
			$dept_time = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, "MONTH");
			$top_time = $timeObject->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "QUARTER");
			$target_types = $targetObject->getActiveListItemsFormattedForSelect();
			$calc_types = $calcTypeObject->getAllCalcTypeOptions();
//top layer details
			$top_name = $topObject->getObjectName($topObject->getMyObjectType());
			$top_field_prefix = $topObject->getTableField();
			$top_target_field = $topObject->getResultsRevisedFieldName();
			$top_actual_field = $topObject->getResultsActualFieldName();
			$top_calctype_fld = $top_field_prefix.'_calctype_id';
			$top_unit_fld = $top_field_prefix.'_unit_id';
			$top_approve_fld = $topObject->getResultsTableField().'_approve';
			$top_assurance_fld = $topObject->getResultsTableField().'_assurance';
//dept details
			$dept_name = $deptObject->getObjectName($deptObject->getMyObjectType());
			$dept_target_field = $deptObject->getResultsRevisedFieldName();
			$dept_actual_field = $deptObject->getResultsActualFieldName();
			$dept_field_prefix = $deptObject->getTableField();
			$dept_calctype_fld = $dept_field_prefix.'_calctype_id';
			$dept_unit_fld = $dept_field_prefix.'_unit_id';
			$dept_approve_fld = $deptObject->getResultsTableField().'_approve';
			$dept_assurance_fld = $deptObject->getResultsTableField().'_assurance';
//headings
			$calc_type_fields = array($top_calctype_fld, $dept_calctype_fld);
			$target_type_fields = array($top_unit_fld, $dept_unit_fld);
			$headings = $deptObject->replaceAllNames($headingObject->getAHeadingNameByField(array($top_field_prefix.'_ref',
				$top_field_prefix.'_name',
				$top_field_prefix.'_calctype_id',
				$top_field_prefix.'_unit_id')));
			$results_heading = $headingObject->getAHeadingNameByField($top_target_field);
			$actual_heading = $headingObject->getAHeadingNameByField($top_actual_field);
//settings
			$colours = array(1 => "red", 2 => "orange", 3 => "green", 4 => "blue");
			$signoff_approve_setting = $setupObject->getAnswerToQuestionByCode("TOPKPI_auto_approve");
			$signoff_assurance_setting = $setupObject->getAnswerToQuestionByCode("TOPKPI_auto_assurance");

			$status = array();
			$count = 0;

//perform validation & calculations
			foreach($list_of_linked_topkpis as $top_id => $top_kpi) {
				$status[$top_id] = array('error_ct' => false,
					'error_tt' => false,
					'warn_result' => false,
					'no_update' => false,);
				$top_calc = $top_kpi[$top_calctype_fld];
				$top_tt = $top_kpi[$top_unit_fld];
				$can_signoff_approve = array();
				$can_signoff_assurance = array();
				foreach($list_of_deptkpi_linked_to_topkpi[$top_id] as $kpi_id => $kpi) {
					$dept_calc = $kpi[$dept_calctype_fld];
					if($kpi[$dept_calctype_fld] != $top_calc) {
						$status[$top_id]['error_ct'] = true;
						$status[$top_id]['no_update'] = true;
					}
					if($kpi[$dept_unit_fld] != $top_tt) {
						$status[$top_id]['error_tt'] = true;
						$status[$top_id]['no_update'] = true;
					}
					if($status[$top_id]['no_update'] === false) {
						$results = $kpi['results'];
						foreach($top_time as $t_id => $time) {
							if(!isset($can_signoff_approve[$t_id])) {
								$can_signoff_approve[$t_id] = true;
							}
							if(!isset($can_signoff_assurance[$t_id])) {
								$can_signoff_assurance[$t_id] = true;
							}
							$included_time = $time['included_time_periods'];
							$targets = array();
							$actuals = array();
							foreach($included_time as $d_id) {
								$targets[$d_id] = $results[$d_id][$dept_target_field];
								$actuals[$d_id] = $results[$d_id][$dept_actual_field];
								if(($results[$d_id][$dept_approve_fld] & SDBP6::REVIEW_OK) != SDBP6::REVIEW_OK) {
									$can_signoff_approve[$t_id] = false;
								}
								if(($results[$d_id][$dept_assurance_fld] & SDBP6::REVIEW_OK) != SDBP6::REVIEW_OK) {
									$can_signoff_assurance[$t_id] = false;
								}
							}
							$calculations = $calcTypeObject->calculatePTDValues($dept_calc, $targets, $actuals);
							$list_of_deptkpi_linked_to_topkpi[$top_id][$kpi_id]['calculations'][$t_id] = $calculations;
							$list_of_linked_topkpis[$top_id]['calculations']['target'][$t_id][$kpi_id] = $calculations['target'];
							$list_of_linked_topkpis[$top_id]['calculations']['actual'][$t_id][$kpi_id] = $calculations['actual'];
//				ASSIST_HELPER::arrPrint($list_of_deptkpi_linked_to_topkpi[$top_id][$kpi_id]['calculations'][$t_id]);
						}
					} else {
						$list_of_deptkpi_linked_to_topkpi[$top_id][$kpi_id]['calculations'] = array();
					}
				}
				if($status[$top_id]['no_update'] === false) {
					$dept_calculation_target = array_sum($list_of_linked_topkpis[$top_id]['calculations']['target'][$t_id]);
					$top_target = $list_of_linked_topkpis[$top_id]['results'][$t_id][$top_target_field];
					if(!isset($dept_ptd_calc_type)) {
						if(count($list_of_linked_topkpis[$top_id]['calculations']['target'][$t_id]) > 1 && ($dept_calculation_target / count($list_of_linked_topkpis[$top_id]['calculations']['target'][$t_id])) == $top_target) {
							$dept_ptd_calc_type = "AVE";
						} else {
							$dept_ptd_calc_type = "SUM";
						}
						$list_of_linked_topkpis[$top_id]['calculation_action'] = $dept_ptd_calc_type;
					}
				} else {
					$list_of_linked_topkpis[$top_id]['calculation_action'] = "";
				}
				unset($dept_ptd_calc_type);
			}


//draw onscreen
			$echo = "
<table id='tbl_exception'>";
//first heading row - main fields + quarters
			$echo .= "
	<tr>";
			foreach($headings as $fld => $h) {
				$echo .= "<th rowspan='3'>".$h."</th>";
			}
			$echo .= "
		<th rowspan='3'>Calculation Action</th>
		<th rowspan='3' width='250px'>Status</th>
";
			$c = 1;
			$total_colspan = 0;
			foreach($top_time as $t_id => $time) {
				if($time['has_started']) {
					$total_colspan += 11;
					$echo .= "<th colspan=11 class='".$colours[$c]."-back'>".$time['name']."</th>";
				} else {
					$total_colspan += 6;
					$echo .= "<th colspan=6 class='".$colours[$c]."-back'>".$time['name']."</th>";
				}
				$c = $c > 3 ? 1 : $c + 1;
			}
			$echo .= "
	</tr>";
//second heading row - monthly time periods
			$echo .= "
	<tr>";
			$c = 1;
			foreach($top_time as $t_id => $time) {
				if($time['has_started']) {
					$included = $time['included_time_periods'];
					foreach($included as $d_id) {
						//targets & actuals = colspan 2
						$echo .= "<th colspan=2 class='".$colours[$c]."-back'>".$dept_time[$d_id]."</th>";
					}
					//calc target
					$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>Calculated ".$dept_name." ".$results_heading."</th>";
					//top target
					$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>".$top_name." ".$results_heading."</th>";
					//new calc actual
					$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>New Calculated ".$dept_name." ".$actual_heading."</th>";
					//prev calc actual
					$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>Prior ".$top_name." ".$actual_heading."</th>";
				} else {
					$included = $time['included_time_periods'];
					foreach($included as $d_id) {
						//targets only = colspan 1
						$echo .= "<th colspan=1 class='".$colours[$c]."-back'>".$dept_time[$d_id]."</th>";
					}
					$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>Calculated ".$dept_name." ".$results_heading."</th>";
					$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>".$top_name." ".$results_heading."</th>";
				}
				//update status
				$echo .= "<th rowspan='2' class='".$colours[$c]."-back'>Update status</th>";
				$c = $c > 3 ? 1 : $c + 1;
			}
			$echo .= "</tr>";
//third heading row - field names
			$echo .= "
	<tr>";
			$c = 1;
			foreach($top_time as $t_id => $time) {
				if($time['has_started']) {
					$included = $time['included_time_periods'];
					foreach($included as $d_id) {
						//targets & actuals = colspan 2
						$echo .= "<th colspan=1 class='".$colours[$c]."-back'>".$results_heading."</th>";
						$echo .= "<th colspan=1 class='".$colours[$c]."-back'>".$actual_heading."</th>";
					}
				} else {
					$included = $time['included_time_periods'];
					foreach($included as $d_id) {
						//targets only = colspan 1
						$echo .= "<th colspan=1 class='".$colours[$c]."-back'>".$results_heading."</th>";
					}
				}
				$c = $c > 3 ? 1 : $c + 1;
			}
			$echo .= "</tr>";

//Loop through top layers
			foreach($list_of_linked_topkpis as $top_id => $top_kpi) {
				$echo .= "<tr class='tr_top'>";
				foreach($headings as $fld => $h) {
					if(in_array($fld, $calc_type_fields)) {
						$top_kpi[$fld."_raw"] = $top_kpi[$fld];
						$top_kpi[$fld] = $calc_types[$top_kpi[$fld]]['name'];
					} elseif(in_array($fld, $target_type_fields)) {
						$top_kpi[$fld."_raw"] = $top_kpi[$fld];
						$top_kpi[$fld] = $target_types[$top_kpi[$fld]];
					}
					$echo .= "<td>".$top_kpi[$fld]."</td>";
				}
				$dept_ptd_calc_type = $top_kpi['calculation_action'];
				$echo .= "<td class='center' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>".($dept_ptd_calc_type == "AVE" ? "Average" : "Accumulative")."</td>";
				$td_class = "";
				if($status[$top_id]['no_update'] === true) {
					$td_class = "red b light-red-back";
				} elseif($status[$top_id]['warn_result'] === true) {
					$td_class = "orange b light-orange-back";
				} else {
					$td_class = "green light-green-back";
				}
				$display_status = array();
				if($status[$top_id]['error_ct']) {
					$display_status[] = $topObject->getDisplayIcon("error")." ".$headings[$top_calctype_fld]." fields do not match.  No update will be possible.";
				}
				if($status[$top_id]['error_tt']) {
					$display_status[] = $topObject->getDisplayIcon("error")." ".$headings[$top_unit_fld]." fields do not match.  No update will be possible.";
				}
				if($status[$top_id]['warn_result']) {
					$display_status[] = $topObject->getDisplayIcon("warn")." The ".$top_name." and ".$dept_name." ".$results_heading." fields are inconsistent.  The update will happen but the ".$results_heading." should be reviewed for consistency / completeness. ";
				}
				if(count($display_status) == 0) {
					$display_status[] = $topObject->getDisplayIcon("ok")." All okay";
				}
				$echo .= "<td class='$td_class' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>".implode("<br />", $display_status)."</td>";

				if($status[$top_id]['no_update'] === false) {
					$c = 1;
					foreach($top_time as $t_id => $time) {
						$included = $time['included_time_periods'];
						$has_started = false;
						if($time['has_started']) { //actuals
							$has_started = true;
						}
						foreach($included as $d_id) {
							$echo .= "<td class='center light-".$colours[$c]."-back'>-</td>";
							if($has_started) {
								$echo .= "<td class='center light-".$colours[$c]."-back'>-</td>";
							}
						}
						if($status[$top_id]['no_update'] !== false) {
							$target_errors_found[$top_id][] = $t_id;
							$dept_calculation_target = 0;
						} else {
							$dept_calculation_target = array_sum($top_kpi['calculations']['target'][$t_id]);
						}
						$top_target = $top_kpi['results'][$t_id][$top_target_field];
						if($has_started) {
							if($status[$top_id]['no_update'] !== false) {
								$actual_errors_found[$top_id][] = $t_id;
								$dept_calculation_actual = 0;
							} else {
								$dept_calculation_actual = array_sum($top_kpi['calculations']['actual'][$t_id]);
							}
							$top_actual = $top_kpi['results'][$t_id][$top_actual_field];
						} else {
							$dept_calculation_actual = 0;
							$top_actual = 0;
						}
						if(!isset($dept_ptd_calc_type)) {
							if(count($top_kpi['calculations']['target'][$t_id]) > 1 && ($dept_calculation_target / count($top_kpi['calculations']['target'][$t_id])) == $top_target) {
								$dept_ptd_calc = $dept_calculation_target / count($top_kpi['calculations']['target'][$t_id]);
								$dept_ptd_calc_actual = $dept_calculation_actual / count($top_kpi['calculations']['actual'][$t_id]);
								$dept_ptd_calc_type = "AVE";
							} else {
								$dept_ptd_calc = $dept_calculation_target;
								$dept_ptd_calc_actual = $dept_calculation_actual;
								$dept_ptd_calc_type = "SUM";
							}
						} elseif($dept_ptd_calc_type == "AVE") {
							$dept_ptd_calc = $dept_calculation_target / count($top_kpi['calculations']['target'][$t_id]);
							$dept_ptd_calc_actual = $dept_calculation_actual / count($top_kpi['calculations']['actual'][$t_id]);
						} else {
							$dept_ptd_calc = $dept_calculation_target;
							$dept_ptd_calc_actual = $dept_calculation_actual;
						}
						if($status[$top_id]['no_update'] === false) {
							$echo .= "<td class='right light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($dept_ptd_calc, $top_kpi[$top_unit_fld.'_raw'])."</td>";
							$echo .= "<td class='right light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($top_target, $top_kpi[$top_unit_fld.'_raw'])."</td>";
						} else {
							$echo .= "<td colspan=2>Err</td>";
						}
						if($status[$top_id]['no_update'] === false) {
							if($has_started) {
								$echo .= "<td class='right light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($dept_ptd_calc_actual, $top_kpi[$top_unit_fld.'_raw'])."</td>";
								$echo .= "<td class='right light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($top_actual, $top_kpi[$top_unit_fld.'_raw'])."</td>";
								//update status
								$echo .= "<td class='center light-".$colours[$c]."-back' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>";
								//check for assurance/approve status
								$top_approve = false;
								if($signoff_approve_setting) {
									$current_top_approve = $top_kpi['results'][$t_id][$top_approve_fld];
									$test_approve = $can_signoff_approve[$t_id];
									if($test_approve == true && (($current_top_approve & SDBP6::NOT_REVIEWED) == SDBP6::NOT_REVIEWED)) {
										$top_approve = SDBP6::REVIEW_OK;
									}
								}
								$top_assurance = false;
								if($signoff_assurance_setting) {
									$current_top_assurance = $top_kpi['results'][$t_id][$top_assurance_fld];
									$test_assurance = $can_signoff_assurance[$t_id];
									if($test_assurance == true && (($current_top_assurance & SDBP6::NOT_REVIEWED) == SDBP6::NOT_REVIEWED)) {
										$top_assurance = SDBP6::REVIEW_OK;
									}
								}
								//Record changes
								if(round($top_actual, 4) != round($dept_ptd_calc_actual, 4) || $top_approve !== false || $top_assurance !== false) {
									$result = $topObject->updateObjectByAutoUpdate($sdbip_id, $top_id, $t_id, $top_kpi[$top_unit_fld.'_raw'], round($dept_ptd_calc_actual, 4), $top_approve, $top_assurance);
									if($result[0] == "ok") {
										$count++;
									}
									$echo .= $topObject->getDisplayIcon("ok")." <span class=green>$actual_heading updated.</span>";
								} else {
									$echo .= "No changed detected.";
								}
								$echo .= "</td>";
							} else {
								//update status
								$echo .= "<td class='center light-".$colours[$c]."-back' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>Period has not started</td>";
							}
						} else {
							$echo .= "<td class='center' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>Error</td>";
						}

						$c = $c > 3 ? 1 : $c + 1;
					}
				} else {
					$echo .= "<td class='' colspan='$total_colspan' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>No updating possible due to errors.</td>";
				}
				$echo .= "</tr>";
				foreach($list_of_deptkpi_linked_to_topkpi[$top_id] as $kpi_id => $kpi) {
					$echo .= "<tr class='tr_dept'>";
					foreach($headings as $fld => $h) {
						$fld = $dept_field_prefix.substr($fld, strlen($top_field_prefix), 100);
						if(in_array($fld, $calc_type_fields)) {
							$kpi[$fld."_raw"] = $kpi[$fld];
							$kpi[$fld] = $calc_types[$kpi[$fld]]['name'];
						} elseif(in_array($fld, $target_type_fields)) {
							$kpi[$fld."_raw"] = $kpi[$fld];
							$kpi[$fld] = $target_types[$kpi[$fld]];
						}
						$echo .= "<td>".$kpi[$fld]."</td>";
					}
					if($status[$top_id]['no_update'] === false) {
						$c = 1;
						foreach($top_time as $t_id => $time) {
							$included = $time['included_time_periods'];
							$has_started = false;
							if($time['has_started']) {
								$has_started = true;
							}
							foreach($included as $d_id) {
								$echo .= "<td class='right d-result light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($kpi['results'][$d_id][$dept_target_field], $kpi[$dept_unit_fld.'_raw'])."</td>";
								if($has_started) {
									$echo .= "<td class='right d-result light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($kpi['results'][$d_id][$dept_actual_field], $kpi[$dept_unit_fld.'_raw'])."</td>";
								}
							}
							if($status[$top_id]['no_update']) {
								$echo .= "<td class='center d-result light-".$colours[$c]."-back'>".$topObject->getDisplayIcon("error")."</td>";
							} else {
								$echo .= "<td class='right d-result light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($kpi['calculations'][$t_id]['target'], $kpi[$dept_unit_fld.'_raw'])."</td>";
							}
							$echo .= "<td class='center d-result light-".$colours[$c]."-back'>-</td>";
							if($has_started) {
								$echo .= "<td class='right d-result light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($kpi['calculations'][$t_id]['actual'], $kpi[$dept_unit_fld.'_raw'])."</td>";
								$echo .= "<td class='center d-result light-".$colours[$c]."-back'>-</td>";
							}
							$c = $c > 3 ? 1 : $c + 1;
						}
					}
					$echo .= "</tr>";
				}
				unset($dept_ptd_calc_type);
				//kept for debugging purposes
				if(!isset($target_errors_found[$top_id]) && !isset($actual_errors_found[$top_id])) {
					$kpi_processed_successfully[] = $top_id;
				}
			}

			$echo .= "
</table>
<p class='b'>".$count." change(s) saved.</p>

	<script type=\"text/javascript\">
		$(function() {
			$('.tr_dept').find('td').css({'font-size':'90%'}).addClass('i');
			$('.tr_dept').find('td:not(.d-result)').css({'background-color':'#eeeeee'});
			$('.tr_top').find('td').addClass('b');
		})
	</script>
";


//if force update then echo onscreen
			if(!isset($_REQUEST['autojob']) || $_REQUEST['autojob'] == false) {
				$scripts = array();
				ASSIST_HELPER::echoPageHeader("1.10.0", $scripts, array("/assist_jquery.css?".time(), "/assist3.css?".time()));

				$menuObject = new SDBP6_MENU("", $autojob);
				$active_button = $menuObject->drawPageTop($menuObject->getPageMenu("admin_top_auto_force"), true, true, $sdbip_name);
				echo $echo."<p class='i'>Run ".date("Y-m-d H:i:s")."</p></body></html>";
				$cmpcode = $topObject->getCmpCode();
			}


//WRITE RESULTS TO FILE
			$data = "";
			$data .= $topObject->getFixedPageHeader()."<h1>".$topObject->replaceObjectNames("|".$topObject->getMyObjectType()."|")." Auto Update</h1>".$echo."<p class='i'>Auto run ".date("Y-m-d H:i:s")."</p></body></html>";
//WRITE DATA TO FILE
			$folder = strtoupper($topObject->getModRef())."/STASKS/";
			$topObject->checkFolder($folder, 1, false, strtolower($cmpcode));
			$filename = "../files/".strtolower($cmpcode)."/".strtoupper($topObject->getModRef())."/STASKS/".$topObject->getMyObjectType()."_".$sdbip_id."_".date("Ymd_His").".html";
			$file = fopen($filename, "w");
			fwrite($file, $data."\n");
			fclose($file);


		}//if sdbip not yet activated
	}
}
?>


<?php
/* Display for dev purposes
echo "<hr /><h1>DEPT KPIS</h1>";
ASSIST_HELPER::arrPrint($list_of_deptkpi_linked_to_topkpi);
echo "<hr /><h1>Top KPIS</h1>";
ASSIST_HELPER::arrPrint($list_of_linked_topkpis);
echo "<hr /><h1>Dept Time</h1>";
ASSIST_HELPER::arrPrint($dept_time);
echo "<hr /><h1>Top Time</h1>";
ASSIST_HELPER::arrPrint($top_time);
echo "<hr /><h1>Target Types</h1>";
ASSIST_HELPER::arrPrint($target_types);
echo "<hr /><h1>Calc Types</h1>";
ASSIST_HELPER::arrPrint($calc_types);
echo "<hr /><h1>Headings</h1>";
ASSIST_HELPER::arrPrint($headings);
echo "<hr />";
*/
?>