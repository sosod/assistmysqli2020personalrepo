<?php
require_once("inc_header.php");

//Create objects
$deptObject = new SDBP6_DEPTKPI();
$topObject = new SDBP6_TOPKPI();
$timeObject = new SDBP6_SETUP_TIME();
$targetObject = new SDBP6_SETUP_TARGETTYPE();
$calcTypeObject = new SDBP6_SETUP_CALCTYPE();

//Get values
/** @var int $sdbip_id -> from inc_header */
/** @var SDBP6_HEADINGS $headingObject -> from inc_header */
$echo = "";
$dept_name = $deptObject->getObjectName($deptObject->getMyObjectType());
$top_name = $topObject->getObjectName($topObject->getMyObjectType());
$list_of_deptkpi_linked_to_topkpi = $deptObject->getCompleteListOfKPIsLinkedToTL($sdbip_id);
$top_keys = array_keys($list_of_deptkpi_linked_to_topkpi);
$list_of_linked_topkpis = $topObject->getListOfLinkedTLKPIsByID($sdbip_id, $top_keys);
$dept_time = $timeObject->getActiveTimeObjectsFormattedForSelect($sdbip_id, "MONTH");
$top_time = $timeObject->getActiveTimeObjectsFormattedForFiltering($sdbip_id, "QUARTER");
$target_types = $targetObject->getActiveListItemsFormattedForSelect();
$calc_types = $calcTypeObject->getAllCalcTypeOptions();
$top_field_prefix = $topObject->getTableField();
$top_target_field = $topObject->getResultsRevisedFieldName();
$top_calctype_fld = $top_field_prefix.'_calctype_id';
$top_unit_fld = $top_field_prefix.'_unit_id';
$dept_target_field = $deptObject->getResultsRevisedFieldName();
$dept_field_prefix = $deptObject->getTableField();
$dept_calctype_fld = $dept_field_prefix.'_calctype_id';
$dept_unit_fld = $dept_field_prefix.'_unit_id';
$calc_type_fields = array($top_calctype_fld, $dept_calctype_fld);
$target_type_fields = array($top_unit_fld, $dept_unit_fld);
$headings = $deptObject->replaceAllNames($headingObject->getAHeadingNameByField(array($top_field_prefix.'_ref', $top_field_prefix.'_name', $top_field_prefix.'_calctype_id', $top_field_prefix.'_unit_id')));
$results_heading = $headingObject->getAHeadingNameByField($top_target_field);
$colours = array(1 => "red", 2 => "orange", 3 => "green", 4 => "blue");

$status = array();


//perform validation & calculations
foreach($list_of_linked_topkpis as $top_id => $top_kpi) {
	$status[$top_id] = array(
		'error_ct' => false,
		'error_tt' => false,
		'warn_result' => false,
		'no_update' => false,
	);
	$top_calc = $top_kpi[$top_calctype_fld];
	$top_tt = $top_kpi[$top_unit_fld];
	foreach($list_of_deptkpi_linked_to_topkpi[$top_id] as $kpi_id => $kpi) {
		$dept_calc = $kpi[$dept_calctype_fld];
		if($kpi[$dept_calctype_fld] != $top_calc) {
			$status[$top_id]['error_ct'] = true;
			$status[$top_id]['no_update'] = true;
		}
		if($kpi[$dept_unit_fld] != $top_tt) {
			$status[$top_id]['error_tt'] = true;
			$status[$top_id]['no_update'] = true;
		}
		if($status[$top_id]['no_update'] === false) {
			$results = $kpi['results'];
			foreach($top_time as $t_id => $time) {
				$included_time = $time['included_time_periods'];
				$targets = array();
				$dummy_actuals = array();
				foreach($included_time as $d_id) {
					$targets[$d_id] = $results[$d_id][$dept_target_field];
					$dummy_actuals[$d_id] = 0;
				}
				$calculations = $calcTypeObject->calculatePTDValues($dept_calc, $targets, $dummy_actuals);
				$list_of_deptkpi_linked_to_topkpi[$top_id][$kpi_id]['calculations'][$t_id] = $calculations['target'];
				$list_of_linked_topkpis[$top_id]['calculations'][$t_id][$kpi_id] = $calculations['target'];
//				ASSIST_HELPER::arrPrint($list_of_deptkpi_linked_to_topkpi[$top_id][$kpi_id]['calculations'][$t_id]);
			}
		} else {
			$list_of_deptkpi_linked_to_topkpi[$top_id][$kpi_id]['calculations'] = array();
		}
	}
}


//draw onscreen
$echo = "
<table id='tbl_exception'>
	<tr>";
foreach($headings as $fld => $h) {
	$echo .= "<th rowspan='2'>".$h."</th>";
}
$c = 1;
foreach($top_time as $t_id => $time) {
	$echo .= "<th colspan=5 class='".$colours[$c]."-back'>".$time['name']."</th>";
	$c = $c > 3 ? 1 : $c + 1;
}
$echo .= "
		<th rowspan='2'>Calculation Action</th>
		<th rowspan='2' width='250px'>Status</th>
	</tr>
	<tr>";
$c = 1;
foreach($top_time as $t_id => $time) {
	$included = $time['included_time_periods'];
	foreach($included as $d_id) {
		$echo .= "<th class='".$colours[$c]."-back'>".$dept_time[$d_id]."</th>";
	}
	$echo .= "<th class='".$colours[$c]."-back'>Calculated ".$dept_name." ".$results_heading."</th>";
	$echo .= "<th class='".$colours[$c]."-back'>".$top_name." ".$results_heading."</th>";
	$c = $c > 3 ? 1 : $c + 1;
}
$echo .= "</tr>";
foreach($list_of_linked_topkpis as $top_id => $top_kpi) {
	$echo .= "<tr class='tr_top'>";
	foreach($headings as $fld => $h) {
		if(in_array($fld, $calc_type_fields)) {
			$top_kpi[$fld."_raw"] = $top_kpi[$fld];
			$top_kpi[$fld] = $calc_types[$top_kpi[$fld]]['name'];
		} elseif(in_array($fld, $target_type_fields)) {
			$top_kpi[$fld."_raw"] = $top_kpi[$fld];
			$top_kpi[$fld] = $target_types[$top_kpi[$fld]];
		}
		$echo .= "<td>".$top_kpi[$fld]."</td>";
	}
	$c = 1;
	foreach($top_time as $t_id => $time) {
		$included = $time['included_time_periods'];
		foreach($included as $d_id) {
			$echo .= "<td class='center light-".$colours[$c]."-back'>-</td>";
		}
		$dept_calculation = array_sum($top_kpi['calculations'][$t_id]);
		$top_target = $top_kpi['results'][$t_id][$top_target_field];
		if(!isset($dept_ptd_calc_type)) {
			if(count($top_kpi['calculations'][$t_id]) > 1 && ($dept_calculation / count($top_kpi['calculations'][$t_id])) == $top_target) {
				$dept_ptd_calc = $dept_calculation / count($top_kpi['calculations'][$t_id]);
				$dept_ptd_calc_type = "AVE";
			} else {
				$dept_ptd_calc = $dept_calculation;
				$dept_ptd_calc_type = "SUM";
			}
		} elseif($dept_ptd_calc_type == "AVE") {
			$dept_ptd_calc = $dept_calculation / count($top_kpi['calculations'][$t_id]);
		} else {
			$dept_ptd_calc = $dept_calculation;
		}
		$echo .= "<td class='right light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($dept_ptd_calc, $top_kpi[$top_unit_fld.'_raw'])."</td>";
		$echo .= "<td class='right light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($top_target, $top_kpi[$top_unit_fld.'_raw'])."</td>";
		if($dept_calculation != $top_target) {
			$status[$top_id]['warn_result'] = true;
		}
		$c = $c > 3 ? 1 : $c + 1;
	}
	$echo .= "<td class='center' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>".($dept_ptd_calc_type == "AVE" ? "Average" : "Accumulative")."</td>";
	$td_class = "";
	if($status[$top_id]['no_update'] === true) {
		$td_class = "red b light-red-back";
	} elseif($status[$top_id]['warn_result'] === true) {
		$td_class = "orange b light-orange-back";
	} else {
		$td_class = "green light-green-back";
	}
	$display_status = array();
	if($status[$top_id]['error_ct']) {
		$display_status[] = $topObject->getDisplayIcon("error")." ".$headings[$top_calctype_fld]." fields do not match.  No update will be possible.";
	}
	if($status[$top_id]['error_tt']) {
		$display_status[] = $topObject->getDisplayIcon("error")." ".$headings[$top_unit_fld]." fields do not match.  No update will be possible.";
	}
	if($status[$top_id]['warn_result']) {
		$display_status[] = $topObject->getDisplayIcon("warn")." The ".$top_name." and ".$dept_name." ".$results_heading." fields are inconsistent.  The update will happen but the ".$results_heading." should be reviewed for consistency / completeness. ";
	}
	if(count($display_status) == 0) {
		$display_status[] = $topObject->getDisplayIcon("ok")." All okay";
	}

	$echo .= "<td class='$td_class' rowspan='".(1 + count($list_of_deptkpi_linked_to_topkpi[$top_id]))."'>".implode("<br />", $display_status)."</td>";
	$echo .= "</tr>";
	foreach($list_of_deptkpi_linked_to_topkpi[$top_id] as $kpi_id => $kpi) {
		$echo .= "<tr class='tr_dept'>";
		foreach($headings as $fld => $h) {
			$fld = $dept_field_prefix.substr($fld, strlen($top_field_prefix), 100);
			if(in_array($fld, $calc_type_fields)) {
				$kpi[$fld."_raw"] = $kpi[$fld];
				$kpi[$fld] = $calc_types[$kpi[$fld]]['name'];
			} elseif(in_array($fld, $target_type_fields)) {
				$kpi[$fld."_raw"] = $kpi[$fld];
				$kpi[$fld] = $target_types[$kpi[$fld]];
			}
			$echo .= "<td>".$kpi[$fld]."</td>";
		}
		$c = 1;
		foreach($top_time as $t_id => $time) {
			$included = $time['included_time_periods'];
			foreach($included as $d_id) {
				$echo .= "<td class='right d-result light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($kpi['results'][$d_id][$dept_target_field], $kpi[$dept_unit_fld.'_raw'])."</td>";
			}
			if($status[$top_id]['no_update']) {
				$echo .= "<td class='center d-result light-".$colours[$c]."-back'>".$topObject->getDisplayIcon("error")."</td>";
			} else {
				$echo .= "<td class='right d-result light-".$colours[$c]."-back'>".$targetObject->formatNumberBasedOnTargetType($kpi['calculations'][$t_id], $kpi[$dept_unit_fld.'_raw'])."</td>";
			}
			$echo .= "<td class='center d-result light-".$colours[$c]."-back'>-</td>";
			$c = $c > 3 ? 1 : $c + 1;
		}
		$echo .= "</tr>";
	}
	unset($dept_ptd_calc_type);
}

$echo .= "
</table>
";
echo $echo;
?>
	<script type="text/javascript">
		$(function () {
			$('.tr_dept').find('td').css({'font-size': '90%'}).addClass('i');
			$('.tr_dept').find('td:not(.d-result)').css({'background-color': '#eeeeee'});
			$('.tr_top').find('td').addClass('b');
		})
	</script>


<?php
/* Display for dev purposes
echo "<hr /><h1>DEPT KPIS</h1>";
ASSIST_HELPER::arrPrint($list_of_deptkpi_linked_to_topkpi);
echo "<hr /><h1>Top KPIS</h1>";
ASSIST_HELPER::arrPrint($list_of_linked_topkpis);
echo "<hr /><h1>Dept Time</h1>";
ASSIST_HELPER::arrPrint($dept_time);
echo "<hr /><h1>Top Time</h1>";
ASSIST_HELPER::arrPrint($top_time);
echo "<hr /><h1>Target Types</h1>";
ASSIST_HELPER::arrPrint($target_types);
echo "<hr /><h1>Calc Types</h1>";
ASSIST_HELPER::arrPrint($calc_types);
echo "<hr /><h1>Headings</h1>";
ASSIST_HELPER::arrPrint($headings);
echo "<hr />";
*/
?>