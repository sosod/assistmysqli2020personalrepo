<?php
/**
 * @var SDBP6_HELPER $helper - from inc_header
 * @var SDBP6_HEADINGS $headingObject - from inc_header
 * @var int $sdbip_id - from inc_header
 */

require_once("inc_header.php");

ASSIST_HELPER::displayResult(array("info", "This page allows you to manage settings which affect a specific ".$helper->replaceAllNames("|SDBIP|").".  To manage global settings which affect the entire module please go to ".$helper->replaceAllNames($helper->createPageTitleBreadcrumb($helper->createMenuTrailFromLastLink("menu_setup_defaults"), "|"))."."));

$list_names = $headingObject->getStandardListHeadings(true);
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($list_names);

$listObject = new SDBP6_LIST();
$listObject->setSDBIPID($sdbip_id);

?>
	<style type="text/css">
		#div_left, #div_right {
			padding: 0px 20px 20px 20px;
			width: 45%;
			height: 100%;
		}

		#div_container {
			padding: 0px 10px 0px 10px;
		}
	</style>
	<div id=div_container>
		<div id=div_right>
			<h2>Lists</h2>
			<table class="form middle">
				<?php
				foreach($list_names as $list) {
					$key = $list['id'];
					$listObject->changeListType($key);
					$c = $listObject->getCountOfActiveItems($sdbip_id);
					echo "	<tr>
	        					<th width=200px style='position:relative;'>".$list['name'].":<span style='font-weight:".($c == 0 ? "bold" : "normal").";font-style:italic;font-size:80%;position:absolute;bottom:5px;right:5px'>(".$c." active item".($c == 1 ? "" : "s").")</span></th>
	        					<td>Configure the ".$list['name']." list items for use in the ".$list['usage']." section".(count($list['objects']) > 1 ? "s" : "").".<span class=float><input type=button value=Configure class=btn_list id='".$key."' /></span></td>
	    					</tr>";
				}
				?>

			</table>
		</div>
		<div id=div_left>
			<h2>Module Settings</h2>
			<table class="form middle">

				<tr>
					<th width=200px><?php echo $helper->getObjectName("ORGSTRUCTURE"); ?>:</th>
					<td>Define the <?php echo $helper->getObjectName("ORGSTRUCTURE"); ?> for use within the module.<span class=float><input type=button value=Configure class=btn_setup id='orgstructure' /></span></td>
				</tr>
				<tr>
					<th width=200px><?php echo $helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL); ?>:</th>
					<td>Assign <?php echo $helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL); ?> to the various <?php echo $helper->getObjectName("ORGSTRUCTURE"); ?> elements and define their roles.<span class=float><input type=button value=Configure class=btn_setup id='admins' /></span></td>
				</tr>
			</table>
		</div>
	</div> <!-- end div container -->
	<script type=text/javascript>
		$(function () {
			$("h2").css("margin-top", "15px");

			$("input:button").button().css("font-size", "75%");
			$("input:button.btn_setup").click(function () {
				document.location.href = "setup_sdbip_defaults_" + $(this).prop("id") + ".php";
			});
			$("input:button.btn_list").click(function () {
				document.location.href = "setup_sdbip_defaults_lists.php?l=" + $(this).prop("id");
			});
			$("input:button.btn_report").click(function () {
				document.location.href = "setup_sdbip_defaults_" + $(this).prop("id") + ".php";
			});
			$("#div_right").css("float", "right");
			$("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
			$("table.form").css({"width": "100%"});
			$(window).resize(function () {
				var x = 0;
				var y = 0;
				$("#div_right").children().each(function () {
					x += AssistString.substr($(this).css("height"), 0, -2) * 1;
				});
				$("#div_left").children().each(function () {
					y += AssistString.substr($(this).css("height"), 0, -2) * 1;
				});
				if (x > y) {
					var z = x;
				} else {
					var z = y;
				}
				$("#div_container").css("height", (z + 50) + "px");
			});
			$(window).trigger("resize");
		});
	</script>

<?php

//ASSIST_HELPER::varPrint(get_defined_vars());
?>