<?php
require_once("inc_header.php");

$myObject = new SDBP6_REPORT_FIXED();

die(ASSIST_HELPER::displayResult(array("info", "This page is currently not available.  Please try again later.")));
$report_sections = $myObject->getReportSectionsForAdmin();
$reports_per_section = $myObject->getReportsGroupedBySectionForAdmin();

//ASSIST_HELPER::arrPrint($reports_per_section);


?>
<div id="div_container">
	<?php
	foreach($report_sections as $sec_id => $section) {
		$section_code = $section['code'];
		?>
		<h2><?php echo $section['name']; ?></h2>
		<table id="tbl_section_<?php echo $sec_id; ?>">
			<thead>
			<tr>
				<th rowspan="2">Ref</th>
				<th colspan="2">Default Settings</th>
				<th colspan="2">Your Settings</th>
				<th rowspan="2">Output Type</th>
				<th rowspan="2">Active</th>
				<th rowspan="2">&nbsp;</th>
			</tr>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Name</th>
				<th>Description</th>
			</tr>
			</thead>
			<tbody><?php
			foreach($reports_per_section[$sec_id] as $report_id => $report) {

				echo "
				<tr id='tr_".$report_id."'>
					<td class='b center'>".$section_code.$report_id."</td>
					<td>".$report['system_name']."</td>
					<td>".nl2br($report['system_description'])."</td>
					<td></td>
					<td></td>
					<td class='center'>".$report['output_type']."</td>
					<td class='center'></td>
					<td class='center'>
						<button class='btn-save' report_id='".$report_id."'>Save Changes</button>
					</td>
				</tr>";
			}
			?></tbody>
		</table>
		<?php
	}
	?>
	<p>Activity log</p>
</div>

<script type="text/javascript">
	$(function () {
		$("#div_container").css({"border": "2px dashed #fe9900"});
		$("button.btn-save").button({icons: {primary: "ui-icon-disk"}}).removeClass("ui-state-default").addClass("ui-button-minor-green");
	});
</script>