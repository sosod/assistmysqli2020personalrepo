<?php
$no_page_heading = true;
include("inc_header.php");

//ASSIST_HELPER::arrPrint($_REQUEST);


$is_add_page = $_REQUEST['page_action'] == "add";
$is_edit_page = $_REQUEST['page_action'] == "edit";
$is_view_page = $_REQUEST['page_action'] == "view";
$object_type = $_REQUEST['object_type']; //echo $object_type;
$object_id = $_REQUEST['object_id'];    //admin record id - needed for edit
$parent_id = $_REQUEST['parent_id'];    //directorate record id


$field_names = $adminObject->getFieldNames($object_type);
$field_defaults = $adminObject->getFieldDefaults();
$field_glossary = $adminObject->getFieldGlossary();
$top_level_orgstructure_only_sections = $adminObject->getTopLevelOnlySections();

if($object_type == "OWNER") {
	$orgObject = new SDBP6_LIST("owner");
	$orgObject->setSDBIPID($sdbip_id);
	$org_item = $orgObject->getAListItemName($parent_id);
	$org_name = $org_item;
	$all_fields = true;
} else {
	$org_item = $orgObject->getRawObjectDetails($parent_id);
	$org_name = $org_item['full_name'];
	$all_fields = ($org_item['parent_id'] == 0);
}


if($is_add_page) {
	$admins = $adminObject->getAvailableAdminsForSetup($object_type, $parent_id);
	$admin_details = array();
} else {
	$admin_details = $adminObject->getAdminDetails($object_type, $object_id, $parent_id);
	//ASSIST_HELPER::arrPrint($admin_details);
	$admin_details = $admin_details[$parent_id][$object_id];
}

echo "<h2>".$org_name."</h2>";

//ASSIST_HELPER::arrPrint($admin_details);
//ASSIST_HELPER::arrPrint($org_item);
?>
<form name=frm_admin>
	<input type=hidden name=sdbip_id value='<?php echo $sdbip_id; ?>' />
	<input type=hidden name=object_type value='<?php echo $object_type; ?>' />
	<input type=hidden name=object_id id=object_id value='<?php echo $object_id; ?>' />
	<input type=hidden name=parent_id value='<?php echo $parent_id; ?>' />
	<table class='tbl-container not-max'>
		<tr>
			<td>
				<table class=form>
					<tr>
						<th><?php echo $helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL); ?>:</th>
						<td><?php
							if($is_add_page) {
								$option = array(
									'id' => "admin_tkid",
									'name' => "admin_tkid",
									'req' => true,
									'options' => $admins
								);
								$js .= $displayObject->drawFormField("LIST", $option);
							} else {
								echo $admin_details['name'];
							}
							?></td>
					</tr>
					<?php
					foreach($field_names as $sec => $section) {
						echo "
		<tr>
			<th>".$section['name'].":</th>
			<td>";
						if($all_fields || !in_array($sec, $top_level_orgstructure_only_sections)) {
							echo "
				<table class=tbl-settings>";
							foreach($section['access'] as $access => $a_name) {
								echo "
					<tr>
						<td class=b>".$a_name.":</td>
						<td>";
								$option = array(
									'id' => $sec."_".$access,
									'name' => $sec."[$access]",
									'small_size' => true,
									'horizontal' => true,
								);
								$value = isset($admin_details[$sec][$access]) ? $admin_details[$sec][$access] : 0;
								$js .= $displayObject->drawFormField("BOOL", $option, $value);
								echo "</td>
					</tr>";
							}
							echo "</table>";
						} else {
							echo "N/A";
						}
						echo "</td>
		</tr>";
					}
					?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width=100%>
					<tr>
						<td class=center>
							<?php if($is_edit_page) {
								echo "<span class=float><button class=btn_delete>".$helper->getActivityName("DEACTIVATE")."</button></span>";
							} ?>
							<button class=btn_save><?php echo $helper->getActivityName("SAVE"); ?></button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<div id=dlg_confirm></div>
<script type=text/javascript>
	$(function () {
		<?php
		//$js.=$displayObject->getIframeDialogJS($object_type, "dlg_child", $var);
		echo $js;
		?>
//dialog JS - because displayObject version not working

		var dlg_size_buffer = 70;
		var ifr_size_buffer = 20;

		//var my_height = $('table.tbl-container').height(); console.log("tbl-cont h "+my_height);
		//if(AssistString.stripos(my_height,'px')>0) {
		//my_height += ifr_size_buffer;
		//window.parent.$('#dlg_child').find('iframe').height(my_height);
		//var dlg_height = window.parent.$('#dlg_child').dialog('option','height');
		//var test_height = my_height+dlg_size_buffer;
		//if(dlg_height > test_height) {
		//	window.parent.$('#dlg_child').dialog('option','height',(test_height));
		//	var check = !(AssistHelper.hasScrollbar(window.parent.$('#dlg_child')));
		//	while(!check) {
		//		window.parent.$('#dlg_child').dialog('option','height',(test_height));
		//		test_height+=dlg_size_buffer;
		//		if(!(dlg_height > test_height) || !(AssistHelper.hasScrollbar(window.parent.$('#dlg_child')))) {
		//			check = true;
		//		}
		//	}
		//}
		//}


		window.parent.$('#dlg_child').dialog('open');


		$('#dlg_confirm').dialog({
			autoOpen: false,
			modal: true
		});

		var page_action = '<?php echo $is_add_page ? "ADD" : "EDIT"; ?>';

//PAGE FORMATTING
		var table_width = 0;
		$("table.tbl-settings").each(function () {
			table_width = table_width > $(this).width() ? table_width : $(this).width();
		});
		$("table.tbl-settings").width(table_width).css("border", "0px").find("td").css("border", "0px");
		$("table.tbl-settings tr").hover(function () {
			$(this).find("td").css("background-color", "#efefef");
		}, function () {
			$(this).find("td").css("background-color", "");
		});
		var td_width = 0;
		$("table.tbl-settings tr").each(function () {
			var w = $(this).find("td:first").width();
			td_width = w > td_width ? w : td_width;
		});
		$("table.tbl-settings tr").each(function () {
			$(this).find("td:first").width(td_width);
		});


//BUTTONS
		$("button.btn_save").button({
			icons: {primary: "ui-icon-disk"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green")
			.hover(
				function () {
					$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
				}, function () {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
				}
			).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			if (page_action == "ADD") {
				var admin = $("#admin_tkid").val();
			} else {
				var admin = $("#object_id").val();
			}
			if (admin.length == 0 || admin == "X") {
				AssistHelper.finishedProcessing("error", "Please select a(n) <?php echo addslashes($helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL)); ?>.");
			} else {
				var dta = AssistForm.serialize($("form[name=frm_admin]"));
				console.log(dta);
				console.log(page_action);
				var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ADMINS." + page_action, dta);
				console.log(result);
				if (!$.isArray(result)) {
					AssistHelper.finishedProcessing("error", result.responseText);
				} else if (result[0] == "ok") {
					window.parent.dialogFinished(result[0], result[1]);
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			}
		});

		$("button.btn_delete").button({
			icons: {primary: "ui-icon-trash"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(
				function () {
					$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-red");
				}, function () {
					$(this).removeClass("ui-button-minor-red").addClass("ui-button-minor-grey");
				}
			).click(function (e) {
			e.preventDefault();
			var dta = "sdbip_id=<?php echo $sdbip_id; ?>&parent_id=<?php echo $parent_id; ?>&object_type=<?php echo $object_type; ?>&object_id=" + $("#object_id").val();
			var display_text = "Are you sure you wish to <?php echo addslashes($helper->getActivityName("DEACTIVATE")); ?> this <?php echo addslashes($helper->getObjectName(SDBP6_SETUP_ADMINS::CLASS_OBJECT_TYPE_PLURAL)); ?>?";
			confirmSubmission(display_text, "DEACTIVATE", dta);
		});


//ACTIONS


		function confirmSubmission(display_text, action, dta) {
			$('#dlg_confirm').html('<h1>Please Confirm</h1><p>' + display_text + '</p><label id=lbl_hide></label>');
			var buttons = [
				{
					text: "Confirm & Continue", click: function () {
						$(this).dialog("close");
						nextStep(action, dta);
					}, class: 'ui-state-ok'
				},
				{
					text: "Cancel", click: function () {
						$(this).dialog("close");
					}, class: 'ui-state-error'
				}
			];
			$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
			AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
			$('#dlg_confirm #lbl_hide').focus();
		}

		function nextStep(action, dta) {
			AssistHelper.processing();
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ADMINS." + action, dta);
			if (result[0] == "ok") {
				window.parent.dialogFinished(result[0], result[1]);
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		}


	});
</script>