<?php
require_once("inc_header.php");

echo ASSIST_HELPER::getFloatingDisplay(array("info", "Any blank headings will be replaced with the default terminology."));
echo "<div id=div_result>";
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
echo "</div>";


$menu_names = $menuObject->getRenameableMenuFromDB();


function drawTier($tier, $menu_item) {
	global $menu_names;
	global $displayObject;
	global $c;
	if($menu_item['can_rename'] == 1) {
		$ff = $displayObject->createFormField("MEDVC", array('id' => $menu_item['section'], 'name' => $menu_item['section'], 'orig' => $menu_item['display_text'], 'class' => "valid8me"), $menu_item['display_text']);
		$can_save = true;
	} else {
		$ff = $displayObject->getDataField("MEDVC", "*");
		$can_save = false;
	}
	echo "
	<tr class=tier".$tier.">";
	for($i = 0; $i < $tier; $i++) {
		echo "<td width=25px class=right>".($i == ($tier - 1) ? ASSIST_HELPER::getDisplayIconAsDiv("ui-icon-arrowreturnthick-1-e") : "")."</td>";
	}
	echo "
		<td colspan=".($c - $tier)." ".(($c - $tier) == 1 ? "width='250px'" : "style=''").">".$menu_item['mdefault'].(!$can_save ? "" : "")."</td>";
	echo "
		<td id=td_".$menu_item['id'].">".$ff['display']."</td>
	</tr>";
	if(isset($menu_names[$menu_item['id']]) && count($menu_names[$menu_item['id']]) > 0) {
		$next_tier = $tier + 1;
		foreach($menu_names[$menu_item['id']] as $m_id => $m) {
			drawTier($next_tier, $m);
		}
	}
}

/* Count depth for colspan calculation */
function calcDepth($m) {
	global $menu_names;
	$c = 1;

	$next_c = 0;
	if(isset($menu_names[$m['id']]) && count($menu_names[$m['id']]) > 0) {
		foreach($menu_names[$m['id']] as $m_id => $m2) {
			$next_c2 = calcDepth($m2);
			if($next_c2 > $next_c) {
				$next_c = $next_c2;
			}
		}
	}

	$c += $next_c;

	return $c;
}

$c = 0;
foreach($menu_names[0] as $m) {
	$c2 = calcDepth($m);
	if($c2 > $c) {
		$c = $c2;
	}
}

ASSIST_HELPER::displayResult($menuObject->replaceAllNames(array("info", "* indicates items that are named according to the |menu_setup| > Naming settings and cannot be changed on this page.")));
?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<form name=frm_menu_names>
				<input type=hidden name=section value='menu_names' />

				<table id=tbl_menu_names>
					<tr>
						<th colspan=<?php echo $c; ?>>Menu Structure</th>
						<th>Your Terminology</th>
					</tr>
					<?php
					foreach($menu_names[0] as $m) {
						drawTier(0, $m);
					}
					?>
					<tr>
						<td colspan=<?php echo $c; ?>></td>
						<td colspan=1 class=center><input type=button value="Save All" class=btn_save_all/></td>
					</tr>
				</table>
			</form>

		</td>
	</tr>
	<tr>
		<td><?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'), "setup", array('section' => "MENU"), "menu"); ?></td>
	</tr>
</table>

<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$("#tbl_menu_names, #tbl_menu_names td").css("border", "#fefefe");
		$("#tbl_menu_names tr.tier0 td").addClass("b").css({"font-size": "125%", "background-color": "#e6e6f5"});
		$("#tbl_menu_names tr.tier1 td").addClass("b").css("font-size", "100%");
		$("#tbl_menu_names tr.tier2 td").addClass("").css("font-size", "100%");
		$("#tbl_menu_names tr.tier3 td").addClass("i").css("font-size", "90%");

		$("input:text.valid8me").keyup(function () {
			if ($(this).val() != $(this).attr('orig')) {
				$(this).addClass("orange-border");
			} else {
				$(this).removeClass("orange-border");
			}
			var pi = $(this).parents("table").prop("id");
			var c = $("#" + pi + " .orange-border").length;
			if (c == 0) {
				$("#" + pi + " tr:last input:button").val("No Changes to Save");
				$("#" + pi + " tr:last input:button").prop("disabled", true);
			} else {
				$("#" + pi + " tr:last input:button").prop("disabled", false);
				var v = (c > 1 ? "Save All " + c + " Changes" : "Save The " + c + " Change");
				$("#" + pi + " tr:last input:button").val(v);
			}
		});

		$(".btn_save_all, .btn_save").click(function () {
			var pi = $(this).parents("table").prop("id");
			if (pi == "tbl_object_names") {
				$form = $("form[name=frm_object_names]");
			} else {
				$form = $("form[name=frm_menu_names]");
			}
			if ($(this).hasClass("btn_save")) {
				$("#div_result").hide();
				var dta = "section=" + $form.find("input:hidden[name=section]").val();
				var r = $(this).attr("ref");
				var v = $("#" + r).val();
				dta += "&" + r + "=" + v;
			} else {
				var dta = AssistForm.serialize($form);
			}
			//alert(dta);
			AssistHelper.processing();
			var result = AssistHelper.doAjax("inc_controller.php?action=Menu.Edit", dta);
			if (result[0] == "ok" && !$(this).hasClass("btn_save")) {
				document.location.href = 'setup_defaults_menu.php?r[]=' + result[0] + '&r[]=' + result[1];
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
				if (result[0] == "ok" && $(this).hasClass("btn_save")) {
					$("#" + r).attr("orig", v);
					$("input:text.valid8me").trigger("keyup");
				}
			}
		});

		$("#tbl_menu_names").find("input:text.valid8me:first").each(function () {
			$(this).trigger("keyup");
		});

	});
</script>