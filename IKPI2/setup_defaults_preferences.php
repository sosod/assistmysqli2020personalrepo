<?php
require_once("inc_header.php");

$me = new SDBP6_SETUP_PREFERENCES();
$all_questions = $me->getQuestions();
$sections = $me->getSections();

ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<form name=frm_pref>
	<table class='tbl-container not-max'>
		<?php
		foreach($sections as $section => $section_name) {

			echo "
	<tr><td>
	<h2>".$section_name."</h2>";

			$questions = $all_questions[$section];

			echo "<table id=tbl_".$section." class=tbl-pref width=100%>";

			foreach($questions as $key => $q) {
				$options = array(
					'name' => "Q_".$key,
					'id' => "Q_".$key,
				);
				switch($q['type']) {
					case "BOOL":
						$options['form'] = "horizontal";
						$options['small_size'] = true;
						break;
					case "LIST":
						if(isset($q['options']['OBJECT'])) {                //AA-361 - Add option for object based module preference [JC]
							$class_name = "SDBP6_".$q['options']['SDBP6'];
							$objObject = new $class_name();
							unset($q['options']['OBJECT']);
							unset($q['options']['SDBP6']);
							$my_keys = array_keys($q['options']);
							$function_name = $q['options'][$my_keys[0]];
							$q['options'] = $objObject->$function_name();
						}
						$options['options'] = $q['options'];
						$options['req'] = true;
						$options['allow_unspecified'] = false;
						break;
				}


				echo "
		<tr>
			<th>$key</th>
			<td>".$q['name']." ".(strlen($q['description']) > 0 ? "<img src='../pics/help_icon.png' class=help_me style='float: right; margin-left: 15px; margin-right: 5px; cursor:pointer;' title='".ASSIST_HELPER::code($q['description'])."' />" : "")."</td>
			<td class=center>";
				$js .= $displayObject->drawFormField($q['type'], $options, $q['value']);
				echo "</td>
		</tr>";
			}
			echo "</table>
	</td></tr>";
		} //end foreach section
		?>
		<tr>
			<td><p>&nbsp;</p>
				<table width=100%>
					<tr>
						<td class=center>
							<button id=btn_save>Save All Sections</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><?php $js .= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'), "setup", array('section' => "PREF")); ?></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$("img.help_me").click(function () {
			var t = $(this).prop("title");
			$("<div />", {id: "dlg_help", html: "<p><img src='../pics/help_icon.png' style='margin-right: 10px;' />" + t + "</p>"}).dialog({
				modal: true,
				buttons: [{
					text: "OK", click: function () {
						$(this).dialog("destroy");
					}
				}]
			});
			AssistHelper.hideDialogTitlebar("id", "dlg_help");
		});
		$("#btn_save").button({icons: {primary: "ui-icon-disk"}})
			.removeClass("ui-state-default").addClass("ui-button-bold-green")
			.hover(function () {
				$(this).addClass("ui-button-bold-orange").removeClass("ui-button-bold-green");
			}, function () {
				$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
			}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_pref]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_PREFERENCES.Update", dta);
			AssistHelper.finishedProcessing(result[0], result[1]);
		});
	});
</script>