<?php
require_once("inc_header.php");


$dept_name = $sdbipObject->getObjectName("DEPTSDBIP");
$top_name = $sdbipObject->getObjectName("TOPSDBIP");
$project_name = $sdbipObject->getObjectName("PROJECTS");
$fin_name = $sdbipObject->getObjectName("FINANCE");

$names = array(SDBP6_DEPTKPI::OBJECT_TYPE => $dept_name, SDBP6_TOPKPI::OBJECT_TYPE => $top_name, SDBP6_PROJECT::OBJECT_TYPE => $project_name, SDBP6_FINANCE::OBJECT_TYPE => $fin_name);
$graph_sections = array(SDBP6_DEPTKPI::OBJECT_TYPE => "DEPT", SDBP6_TOPKPI::OBJECT_TYPE => "TOP", SDBP6_PROJECT::OBJECT_TYPE => "PROJECT", SDBP6_FINANCE::OBJECT_TYPE => "FIN");
//only display according to active sections depending on module use AA-338 JC 19 Feb 2020
$active_sections = $sdbipObject->getActiveSections();
foreach($graph_sections as $key => $type) {
	if(!in_array($key, $active_sections)) {
		unset($graph_sections[$key]);
	}
}
//temporarily remove project & finance - still TBD
unset($graph_sections[SDBP6_PROJECT::OBJECT_TYPE]);
unset($graph_sections[SDBP6_FINANCE::OBJECT_TYPE]);

$orgObject = new SDBP6_SETUP_ORGSTRUCTURE();
$list = $orgObject->getActiveObjectsFormattedForSelect("TOP");
//ASSIST_HELPER::arrPrint($list);


?>
	<table id="tbl_graph">
		<tr>
			<th></th>
			<?php
			foreach($graph_sections as $key => $section) {
				echo "<th>".str_replace(" ", "<br />", $names[$key])."</th>";
			}
			?>
		</tr>
		<?php
		$i = 0;
		$l = $sdbipObject->getCmpName();
		echo "<tr>
					<td class='b'>".$l."</td>";
		foreach($graph_sections as $key => $section) {
			echo "		<td class=center><button class='btn_view' id='btn_".strtolower($section)."_".$i."' graph_type='".$section."' dir_id='$i'>".$sdbipObject->getActivityName("VIEW")."</button></td>";
		}
		echo "		</tr>";
		foreach($list as $i => $l) {
			echo "<tr>
					<td class='b'>".$l."</td>";
			foreach($graph_sections as $key => $section) {
				echo "		<td class=center><button class='btn_view' id='btn_".strtolower($section)."_".$i."' graph_type='".$section."' dir_id='$i'>".$sdbipObject->getActivityName("VIEW")."</button></td>";
			}
			echo "		</tr>";
//					<td class=center><button class='btn_view' id='btn_dept_".$i."' graph_type='DEPT' dir_id='$i'>".$sdbipObject->getActivityName("VIEW")."</button></td>
//					<td class=center><button class='btn_view' id='btn_top_".$i."' graph_type='TOP' dir_id='$i'>".$sdbipObject->getActivityName("VIEW")."</button></td>
			//<td class=center><button class='btn_view' id='btn_project_".$i."' graph_type='PROJECT' dir_id='$i'>".$sdbipObject->getActivityName("VIEW")."</button></td>
			//<td class=center><button class='btn_view' id='btn_find_".$i."' graph_type='FINANCE' dir_id='$i'>".$sdbipObject->getActivityName("VIEW")."</button></td>
		}
		?>
	</table>
	<script type=text/javascript>
		$(function () {
			$("button.btn_view").button({
				icons: {primary: "ui-icon-newwin"}
			}).click(function (e) {
				e.preventDefault();
				AssistHelper.processing();
				var gt = $(this).attr("graph_type");
				var i = $(this).attr("dir_id");
				if (gt == "DEPT" || gt == "TOP") {
					var url = "manage_view_graphs_kpi.php?gt=" + gt + "&i=" + i;
				} else {
					var url = "manage_view_graphs_fin.php?gt=" + gt + "&i=" + i;
				}
				document.location.href = url;
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
				.hover(function () {
					$(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange");
					$(this).closest("tr").css("background-color", "#eeeeee");
				}, function () {
					$(this).addClass("ui-button-minor-grey").removeClass("ui-button-minor-orange");
					$(this).closest("tr").css("background-color", "");
				});

			//formating
			$("#tbl_graph td").css({"padding": "7px", "vertical-align": "middle"});
			var w = 0;
			$("#tbl_graph tr:first th:gt(0)").each(function () {
				if ($(this).width() > w) {
					w = $(this).width();
				}
			});
			$("#tbl_graph tr:first th:gt(0)").width(w);

		});
	</script>
<?php

?>