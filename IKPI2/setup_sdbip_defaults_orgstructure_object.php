<?php
/**
 * REQUIRED VARIABLES FROM HEADER:
 * @var SDBP6_SETUP_ORGSTRUCTURE $orgObject
 * @var string $js
 * @var SDBP6_DISPLAY $displayObject
 * @var SDBP6_HELPER $helper
 *
 * Added Delete vs Deactivate button & Used By check #AA-625 JC [3 June 2021]
 *
 */
$no_page_heading = true;
include("inc_header.php");
$thispage = "setup_sdbip_defaults_orgstructure.php";
//echo "<hr /><h1 class='green'>Helllloooo $sdbip_id from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($_REQUEST);
//echo "<hr />";

$is_add_page = strtolower($_REQUEST['page_action']) == "add";
$is_edit_page = strtolower($_REQUEST['page_action']) == "edit";
$is_view_page = strtolower($_REQUEST['page_action']) == "view";
$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];
$parent_id = $_REQUEST['parent_id'];
$max_display_order = $_REQUEST['max_order'];
$sdbip_id = $_REQUEST['sdbip_id'];


//set/get raw details to populate form
if($is_add_page) {
	$object_details = array(
		'id' => 0,
		'parent_id' => $parent_id,
		'status' => 2,
		'name' => "",
		'used_by' => false,
	);
} else {
	$object_details = $orgObject->getRawObjectDetails($object_id, true);
}
//get possible parent objects if not bottom of the list
$orgObject->setSDBIPID($sdbip_id);
if($parent_id > 0) {
	$parents = $orgObject->getListOfActiveParentsFormattedForSelect();
} else {
	$parents = array();
}
//if edit - check if item is in use & can be deleted or only deactivated #AA-625 JC 3 June 2021
$show_deactivate_button = true;
$show_delete_button = false;
if($is_edit_page) {
	$used_by = $object_details['used_by'];
} else {
	$used_by = false;
}
if($used_by === false) {
	$show_delete_button = true;
	$show_deactivate_button = false;
}
?>
<table class='tbl-container not-max'>
	<tr>
		<td>
			<div id="div_form" width="450px" style="width:450px;">
				<form name=frm_orgstructure>
					<input type="hidden" name="sdbip_id" id="sdbip_id" value="<?php echo $sdbip_id; ?>" />
					<?php
					//ONLY DISPLAY ORDER FIELD IF ADDING NEW ITEM - after that order is handled by sort function and not via edit function
					if($is_add_page) {
						?>
						<input type=hidden name=order id=sort_me value="<?php echo $max_display_order + 1; ?>" />
						<?php
					} elseif($is_edit_page) {
						?>
						<input type=hidden name=object_id id=object_id value="<?php echo $object_id; ?>" />
						<?php
					}
					?>
					<table class=form>
						<tr>
							<th width="100px">Ref:</th>
							<td width="350px"><?php echo $object_id; ?></td>
						</tr>
						<tr>
							<th>Name:</th>
							<td><?php
								if($is_view_page) {
									$js .= $displayObject->drawDataField("TEXT", $object_details['name']);
								} else {
									$js .= $displayObject->drawMediumInputText($object_details['name'], array('name' => "name", 'id' => "name", 'required' => true), 150);
								}
								?></td>
						</tr>
						<?php
						//If adding a new top level then display child boxes for quick add
						if($parent_id == 0 && $is_add_page) {
							?>
							<tr>
								<th>Subs:</th>
								<td><input type=hidden name=add_type value=TOP/>
									<table class=hidden-sub-table>
										<?php
										for($i = 0; $i < 10; $i++) {
											echo "
					<tr><td>";
											$js .= $displayObject->drawMediumInputText("", array('name' => "sub[$i]", 'id' => "sub_".$i), 150);
											echo "
					</td></tr>
					";
										}
										?>
									</table>
								</td>
							</tr>
							<?php
							//else if adding/editing a child - display parent details(view) or select (add/edit)
						} elseif($parent_id > 0) {
							?>
							<tr>
								<th>Parent:</th>
								<td><?php
									if($is_view_page) {
										$js .= $displayObject->drawDataField("TEXT", $parents[$object_details['parent_id']]);
									} else {
										$js .= $displayObject->drawSelect($object_details['parent_id'], array('name' => "parent_id", 'id' => "parent_id", 'unspecified' => false), $parents);
									}
									?></td>
							</tr>
							<?php
						}
						//Display how many objects reference this item & prevent deletion #AA-625 JC 3 June 2021
						if($is_edit_page || $is_view_page) {
							?>
							<tr>
								<th width="100px">Used By:</th>
								<td width="350px"><?php echo($used_by === false ? "Not in use" : $used_by); ?></td>
							</tr>
							<?php
						}
						?>
					</table>
					<p class=float>
						<?php
						if($is_add_page) {
							echo "<button class=btn_plus>".$helper->getActivityName("SAVE")."</button>";
						}
						if($is_edit_page) {
							echo "<button class=btn_plus>".$helper->getActivityName("SAVE")." Changes</button>&nbsp;".($show_deactivate_button ? "<button class=btn_deactivate>".$helper->getActivityName("Deactivate")."</button>" : "").($show_delete_button ? "<button class=btn_delete>".$helper->getActivityName("DELETE")."</button>" : "");
						}
						?>
					</p>
				</form>
			</div>
		</td>
	</tr>
	<?php if($is_edit_page) { ?>
		<tr>
			<td>
				<div id="div_note">
					<?php
					ASSIST_HELPER::displayResult(array("info", "Please note: If the item is in use within the module, you will only be able to ".$helper->getActivityName("Deactivate")." it and it will remain available for viewing and reporting functions but will not be available for creating or editing.  If the item is not in use, you will be able to ".$helper->getActivityName("Delete")." it and the item will not be available anywhere within the module.  Warning: Delete can not be undone!"));
					?>
				</div>
			</td>
		</tr>
	<?php } ?>
</table>
<div id="div_dialog_delete" title="Delete">
	<input type=hidden id=message_dialog_action name=message_dialog_action value=DELETE/>
	<input type=hidden id=message_dialog_id name=message_dialog_id value=0/>
	<div id=message_dialog_div class="red"><span class="b">Are you sure you wish to DELETE this item?</span><br /><br />WARNING: Delete can NOT be undone!</div>
</div>
<script type=text/javascript>
	$(function () {
		<?php
		$js .= $displayObject->getIframeDialogJS($object_type, "dlg_child", array(), true);
		echo $js;
		?>
		var action = "<?php echo strtoupper($_REQUEST['page_action']); ?>";
// console.log($("table.tbl-container").height());
// $("#div_note").hide();
// console.log($("table.tbl-container").height());
// $("#div_note").show();

		$("button.btn_plus").button({
			icons: {primary: "ui-icon-disk"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green")
			.hover(
				function () {
					$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
				}, function () {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
				}
			).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			$("input:text, select").removeClass("required");
			var err = false;
			$("input:text").each(function () {
				if ($(this).attr("required") == true && $(this).val().length == 0) {
					err = true;
					$(this).addClass("required");
				}
			});
			$("select").each(function () {
				if ($(this).attr("required") == true && ($(this).val().length == 0 || $(this).val() == "X" || $(this).val() == undefined)) {
					err = true;
					$(this).addClass("required");
				}
			});
			if (err) {
				AssistHelper.finishedProcessing("error", "Please complete all required fields.");
			} else {
				var dta = AssistForm.serialize($("form[name=frm_orgstructure]"));
				var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ORGSTRUCTURE." + action, dta);
				if (result[0] == "ok") {
					window.parent.dialogFinished(result[0], result[1]);
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}
			}
		});
		$("button.btn_deactivate").button({
			icons: {primary: "ui-icon-trash"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(
				function () {
					$(this).addClass("ui-button-minor-red").removeClass("ui-button-minor-grey");
				}, function () {
					$(this).removeClass("ui-button-minor-red").addClass("ui-button-minor-grey");
				}
			).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var dta = "object_id=" + $("#object_id").val();
			var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ORGSTRUCTURE.DEACTIVATE", dta);
			if (result[0] == "ok") {
				window.parent.dialogFinished(result[0], result[1]);
			} else {
				AssistHelper.finishedProcessing(result[0], result[1]);
			}
		});
		//Delete button #AA-625
		$("button.btn_delete").button({
			icons: {primary: "ui-icon-trash"}
		}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(
				function () {
					$(this).addClass("ui-button-minor-red").removeClass("ui-button-minor-grey");
				}, function () {
					$(this).removeClass("ui-button-minor-red").addClass("ui-button-minor-grey");
				}
			).click(function (e) {
			e.preventDefault();
			var i = $("#object_id").val();
			$("#div_dialog_delete #message_dialog_id").val(i);
			$("#div_dialog_delete").dialog("open");
		});
//Hi-jinks to accommodate "Are you sure you want to delete" confirmation #AA-625 JC 3 June 2021
		$("#div_dialog_delete").dialog({
			modal: true,
			autoOpen: false,
			width: "auto",
			buttons: [{
				text: "OK",
				click: function () {
					AssistHelper.processing();
					var dta = "object_id=" + $("#div_dialog_delete #message_dialog_id").val();
					var result = AssistHelper.doAjax("inc_controller.php?action=SETUP_ORGSTRUCTURE.DELETE", dta);
					if (result[0] == "ok") {
						localDialogFinished(result[0], result[1]);
						//window.parent.parent.dialogFinished(result[0],result[1]);
//					AssistHelper.finishedProcessingWithRedirect(result[0], result[1], "<?php echo $thispage; ?>");
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}
				}
			}, {
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
				}
			}]
		});
		AssistHelper.formatDialogButtons($("#div_dialog_restore"), 0, AssistHelper.getGreenCSS());
		AssistHelper.formatDialogButtons($("#div_dialog_restore"), 1, AssistHelper.getCloseCSS());
		AssistHelper.hideDialogTitlebar("id", "div_dialog_restore");
	});

	function localDialogFinished(icon, result) {
		window.parent.dialogFinished(icon, result);
	}

	//End #AA-625 hi-jinks
</script>