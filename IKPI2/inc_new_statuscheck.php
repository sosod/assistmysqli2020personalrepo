<?php


$step = isset($_REQUEST['step']) ? $_REQUEST['step'] : "start";
$original_step = $step;

if(!isset($sdbipObject)) {
	$sdbipObject = new SDBP6_SDBIP();
}
if(!isset($sdbip_object_name)) {
	$sdbip_object_name = $sdbipObject->getObjectName("SDBIP");
}
if(!isset($idp_object_name)) {
	$idp_object_name = $sdbipObject->getObjectName("IDP");
}
if(!isset($mscoa_object_name)) {
	$mscoa_object_name = "mSCOA";
}

//ASSIST_HELPER::arrPrint($sdbip_details);
//echo "<h1>:".$is_sdbip_activated.":</h1>";

//is_sdbip_activated - already set in inc_header
//if no activated sdbip, then check if one has been started - REMOVED TO ACCOMMODATE ROLL OVER AA-327 JC 28 March 2020
//if on reset page then do the check for activated etc.  Don't do it on any other page - endless loop results AA-419 JC 9 June 2020
if($step == "reset") {
	if($is_sdbip_activated == false) {
		//echo "<h3 class=green>NO ACTIVATED SDBIP FOUND.</h3>";
		$has_sdbip_been_started = $sdbipObject->hasSDBIPBeenCreatedAndNotYetActivated();
	} else {
//	//echo "<h3 class=red>ACTIVATED SDBIP FOUND.</h3>";
		$has_sdbip_been_started = true;
		//ignore activation status if on reset page or undo activation page
//	$step = ($step!="reset"&&$step!="undo_activate"?"activated_error":$step);
	}
} else {
	$has_sdbip_been_started = $sdbipObject->hasSDBIPBeenCreatedAndNotYetActivated();
}
//echo "<h1>:".$has_sdbip_been_started.":".$step.":</h1>";
//get current sdbip details
if($has_sdbip_been_started == true) {
	//don't do this if the sdbip has been activated - endless loop results
//	if($step!="activated_error") {
	if($step != "reset") { //don't do anything if reset page - inc_header should have handled it on it's own
		if($is_sdbip_activated && ($step == "reset" || $step == "undo_activate")) {
			$sdbip_details = $sdbipObject->getCurrentActivatedSDBIPDetails();
		} else {
			$sdbip_details = $sdbipObject->getCurrentNewSDBIPDetails();
		}
	}
	$sdbip_id = $sdbip_details['id'];
	if($sdbip_details['is_confirmed'] == false) {
		$step = "new";
	}
//	}
} else {
	//echo "<h3 class=green>NO STARTED SDBIP FOUND.</h3>";
	$sdbip_details = array();
	$sdbip_details['is_confirmed'] = false;
}
//echo "<h1>:".$step.":</h1>";
//ASSIST_HELPER::arrPrint($sdbip_details);
//display error message if SDBIP has already been activated and kill the New page
if($step == "activated_error") { //SHOULD NOT LONGER TRIGGER AFTER AA-327 JC 28 March 2020
	$reset_text = "<span class='u i' style='font-size:85%'>".$sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_create_reset"), "|"))."</span>";
	$undo_activation_text = "<span class='u i' style='font-size:85%'>".$sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_activate_undo"), "|"))."</span>";
	echo "
			<div width=750px class=tbl-error style='width:750px'>
				<h1>Alert!</h1>
				<p class=b>A $sdbip_object_name has already been ".$sdbipObject->getActivityName("activated")." and this page is no longer available.</p>
				<p>If the $sdbip_object_name has been ".$sdbipObject->getActivityName("activated")." in error, please go to ".$undo_activation_text." and follow the steps to undo the ".$sdbipObject->getActivityName("activation").".</p>
				<p>If you need to completely reset the $sdbip_object_name and start again, please go to ".$reset_text." and follow the steps on that page to reset the ".$sdbipObject->getObjectName("SDBIP")." and start again.</p>
				<p>If you are needing to add new ".$sdbipObject->getObjectName("PROJECTS")." or ".$sdbipObject->getObjectName("KPIS")." to the ".$sdbipObject->getActivityName("activated")." $sdbip_object_name, then please access the ".$sdbipObject->getActivityName("create")." pages within the ".$sdbipObject->getObjectName("menu_manage")." or ".$sdbipObject->getObjectName("menu_admin")." sections.  New ".$sdbipObject->getObjectName("PROJECTS")." or ".$sdbipObject->getObjectName("KPIS")." can not be added to an ".$sdbipObject->getActivityName("activated")." $sdbip_object_name from the ".$sdbipObject->getObjectName("menu_new")." section.</p>
			</div>
			";
	die();
} elseif($step != "start" && (isset($is_create_step1_page) && $is_create_step1_page == true)) {
	echo "
	<div width=750px class=tbl-info style='width:750px'>
		<h1>New $sdbip_object_name In Progress</h1>
		<p>Automatically redirecting you to the <a href='".$sdbip_details['next_step']."'>next step</a>...</p>
	</div>
	<script type=text/javascript>
	$(function() {
		var url = '".$sdbip_details['next_step']."';
		document.location.href = url;
		//alert(url);
	});
	</script>";
	die();
} elseif($step == "start" && (!isset($is_create_step1_page) || $is_create_step1_page == false)) {
	$menu_text = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_create_step1"), "|"));
	echo "
	<div width=750px class=tbl-info style='width:750px'>
			<h1>Alert!</h1>
			<p>A new $sdbip_object_name is currently not in progress and therefore this page is not available.</p>
			<p>Please go to ".$menu_text." and create a new ".$sdbip_object_name." in order to make this page available.</p>
	</div>
	";
	die();
} elseif($step == "new" && isset($is_import_from_idp_page) && $is_import_from_idp_page == true && ($sdbip_details['idp_module'] == "FALSE_LINK")) {
	$menu_path = $navigation_path;
	unset($menu_path[count($menu_path) - 1]);
	$menu_path[] = "import";
	$route = "menu_".implode("_", $menu_path);
	$redirect_route = implode("_", $menu_path).".php";
	$menu_text1 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route), "|"));
	array_pop($menu_path);
	$menu_path[] = "create";
	$route = "menu_".implode("_", $menu_path);
	$menu_text2 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route), "|"));
	echo "
	<div width=750px class=tbl-info style='width:750px'>
			<h1>Alert!</h1>
			<p>The new $sdbip_object_name has not been linked to a(n) ".$idp_object_name." module and therefore this page is not available.</p>
			<p>Please use the navigation buttons to go to $menu_text1 or $menu_text2 in order to continue the ".$sdbipObject->getActivityName("CREATE")." process.</p>
	</div>
	<script type=text/javascript>
		//AssistHelper.processing();
		//document.location.href= '$redirect_route';
	</script>
	";
	die();
//If page is import from SDBIP and not linked to SDBIP then cancel
} elseif($step == "new" && isset($is_import_from_sdbip_page) && $is_import_from_sdbip_page == true && ($sdbip_details['alt_module'] == "FALSE_LINK")) {
	$menu_path = $navigation_path;
	unset($menu_path[count($menu_path) - 1]);
	$menu_path[] = "import";
	$route = "menu_".implode("_", $menu_path);
	$redirect_route = implode("_", $menu_path).".php";
	$menu_text1 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route), "|"));
	array_pop($menu_path);
	$menu_path[] = "create";
	$route = "menu_".implode("_", $menu_path);
	$menu_text2 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route), "|"));
	echo "
	<div width=750px class=tbl-info style='width:750px'>
			<h1>Alert!</h1>
			<p>The new $sdbip_object_name has not been linked to a prior ".$sdbip_object_name." module and therefore this page is not available.</p>
			<p>Please use the navigation buttons to go to $menu_text1 or $menu_text2 in order to continue the ".$sdbipObject->getActivityName("CREATE")." process.</p>
	</div>
	<script type=text/javascript>
		//AssistHelper.processing();
		//document.location.href= '$redirect_route';
	</script>
	";
	die();
//if page is manual create || manual import and sdbip is linked to alt for CONVERT then cancel
} elseif($step == "new" && (!isset($is_reset_page) || $is_reset_page !== true) && (!isset($is_import_from_sdbip_page) || $is_import_from_sdbip_page !== true) && ($sdbip_details['alt_module'] != "FALSE_LINK" && $sdbip_details['alt_action'] == "CONVERT")) {
	$menu_path = $navigation_path;
	unset($menu_path[count($menu_path) - 1]);
	$menu_path[] = "sdbip";
	$route = "menu_".implode("_", $menu_path);
	$redirect_route = implode("_", $menu_path).".php";
	$menu_text1 = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink($route), "|"));
	echo "
	<div width=750px class=tbl-info style='width:750px'>
			<h1>Alert!</h1>
			<p>The new $sdbip_object_name has been linked to a prior ".$sdbip_object_name." module for conversion and therefore this page is not available as the $sdbip_object_name details must be converted exactly as is from the prior module.  Changes can be made once the $sdbip_object_name has been ".$sdbipObject->getActivityName("ACTIVATED").".</p>
			<p>Please use the navigation buttons to go to $menu_text1 in order to continue the ".$sdbipObject->getActivityName("CREATE")." process.</p>
	</div>
	<script type=text/javascript>
		//AssistHelper.processing();
		//document.location.href= '$redirect_route';
	</script>
	";
	die();
//if not ACTIVATE page & current SDBIP has been confirmed then die()
} elseif($step != "activate" && $step != "reset" && $step != "undo_activate" && $sdbip_details['is_confirmed'] == true) {
	$menu_text = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_activate_do"), "|"));
	echo "
	<div width=750px class=tbl-info style='width:750px'>
			<h1>Alert!</h1>
			<p>The $sdbip_object_name has already been ".$sdbipObject->getActivityName("confirmed")." and therefore this page is not available.</p>
			<p>Please go to ".$menu_text." in order to continue.</p>
	</div>
	";
	die();
//if is ACTIVATE page & next step = new (i.e. confirmed=false)
} elseif(($original_step == "activate" || $original_step == "undo_activate") && $step == "new") {
	if($original_step == "activate") {
		$menu_text = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_confirm"), "|"));
		$action_word = "confirmed";
	} else {
		$menu_text = $sdbipObject->replaceAllNames($sdbipObject->createPageTitleBreadcrumb($sdbipObject->createMenuTrailFromLastLink("menu_new_activate_do"), "|"));
		$action_word = "activated";
	}
	echo "
	<div width=750px class=tbl-info style='width:750px'>
			<h1>Alert!</h1>
			<p>The $sdbip_object_name has not yet been ".$sdbipObject->getActivityName($action_word)." and therefore this page is not available.</p>
			<p>Please go to ".$menu_text." in order to continue.</p>
	</div>
	";
	die();
}


/**
 * Backup code to hide unnecessary buttons if SDBP6_MENU->getPageMenu doesn't kick in
 */

if($step == "new" && ($sdbip_details['idp_module'] == "FALSE_LINK") && isset($page_object) && ($page_object == SDBP6_TOPKPI::OBJECT_TYPE || $page_object == SDBP6_PROJECT::OBJECT_TYPE)) {
	?>
	<script type=text/javascript>
		$(function () {
			$("input[name=radiom3]").each(function () {
				if (AssistString.stripos($(this).val(), "_idp.") !== false) {
					var i = $(this).prop("id");
					$("label[for=" + i + "]").hide();
					$(this).prop("disabled", "disabled");
					$(this).hide();
				}
			});
		});
	</script>
	<?php

}
if($step == "new" && ($sdbip_details['alt_module'] == "FALSE_LINK") && (isset($page_object) && ($page_object == SDBP6_TOPKPI::OBJECT_TYPE || $page_object == SDBP6_PROJECT::OBJECT_TYPE))) {
	?>
	<script type=text/javascript>
		$(function () {
			$("input[name=radiom3]").each(function () {
				if (AssistString.stripos($(this).val(), "_sdbip.") !== false) {
					var i = $(this).prop("id");
					$("label[for=" + i + "]").hide();
					$(this).prop("disabled", "disabled");
					$(this).hide();
				}
			});
		});
	</script>
	<?php

}


?>

