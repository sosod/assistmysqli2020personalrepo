<?php
require_once("inc_header.php");


$object_id = $_REQUEST['object_id'];
$myObject = new SDBP6_PROJECT();

$page_section = "ADMIN";
$page_action = "EDIT";

$page_redirect_path = "admin_projects_edit.php?";

if(isset($_REQUEST['inactive_object']) && $_REQUEST['inactive_object'] == true) {
	$page_action = "RESTORE";
}
include("common/form_object.php");


?>
<?php markTime("end of page"); ?>