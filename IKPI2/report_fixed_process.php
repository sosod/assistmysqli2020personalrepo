<?php
$no_page_heading = true;
require_once("inc_header.php");
$myObject = new SDBP6_REPORT_FIXED();

//if($myObject->isSupportUser()) { error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT); }

$report_id = $_REQUEST['i'];
$report_settings = $myObject->getReportDetails($report_id);

$timeObject = new SDBP6_SETUP_TIME();
$time_periods = $timeObject->getTimeForReportFiltering($sdbip_id);

?>
	<style type="text/css">
		.row-total {
			background-color: #dedede;
			font-weight: bold;
		}

		.row-variance {
			font-style: italic;
		}

		.row-budget {
			background-color: #efefef;
		}

		.group-title {
			background-color: #bbbbbb;
			font-weight: bold;
			text-decoration: underline;
		}

		.group-total {
			background-color: #dedede;
			font-style: italic;
		}

		.group-total-total {
			background-color: #bbbbbb;
			font-style: italic;
		}

		.grand-total {
			background-color: #bbbbbb;
			font-weight: bold;
		}

		.grand-total-total {
			background-color: #999999;
			font-weight: bold;
		}
	</style>
<?php

if($report_settings['is_custom'] === true) {


	//get custom report details
	//redirect to normal report generator masquerading as a quick report


} elseif($report_settings['is_fin_schedule'] === true) {


	include("report_fixed_schedule.php");


} else {


	//handle otherwise
	switch($report_settings['code']) {
		case "projbymsc_fin":
		case "projbydir_fin":
		case "projbydir_perf":
		case "projbymsc_perf":
			$code = $report_settings['code'];
			include("report_fixed_projbydir.php");
			break;
		default:
			echo "<P>Sorry, I don't recognise the report settings given for report ".$report_settings['code'].".</P>";
			break;
	}


}


//ASSIST_HELPER::arrPrint($schedule_settings);

?>