<?php
require_once('inc_header.php');
ASSIST_HELPER::displayResult((isset($_REQUEST['r']) && is_array($_REQUEST['r']) ? $_REQUEST['r'] : array()));

/***********
 * CHECK FOR MSCOA COMPLIANCE, VERSION & FINTEG STATUS
 */
$mscoa_compliant = $sdbip_details['mscoa_compliant'];
$mscoa_version = $sdbip_details['mscoa_version'];
$finteg_status = $sdbip_details['finteg'];

if($mscoa_compliant != 1) {
	$message = array("error", "This section is only available for mSCOA-compliant financials.  The current SDBIP is not indicated as being mSCOA-compliant.  If you believe this to be in error, please contact your Business Partner.");

} elseif($finteg_status != "MANUAL") {
	switch($finteg_status) {
		case "DIRECT":
			$finteg_message = "direct integration with your financial system";
			break;
		case "CUSTOM":
		default:
			$finteg_message = "a custom integration process";
			break;
	}
	$message = array("ok", "This section is only available for manual upload of financials.  Your current SDBIP is indicated as being automatically updated through ".$finteg_message.".  You do not need to manually upload any financials.");
}


if(isset($message)) {

	ASSIST_HELPER::displayResult($message);
	die();

}


$message = array("ok", "This section is available for manual upload of financials through the importing of the NT data string.  If you would like to arrange for direct integration with your financial system or for a custom financial integration process, please contact your Assist Business Partner.");
ASSIST_HELPER::displayResult($message);


//Notes to self: 2/2/2020 JC AA-147
//-----1. Display SDBIP info in the table below - DONE 3/2 JC
//-----2. Check if fin years have codes - if not then error die - DONE 3/2 JC
//-----3. Check if time periods have codes - if not then warn (assumes M01 etc) any deviation could cause corruption? or error die? - DONE 3/2 JC
//-----3.2 Check if Projects loaded - if not then warn that Project based financial reporting won't be available - DONE 3/2 JC
//4. Redo test and figure out where the hell I was
//   -------- wasn't I figuring out how to treat budget v adjustment v actual? - Yup 3/2
//   -------- how are time periods being worked out?  How is M01 suddenly equal to 4? - issue identified, solution devised => put the onus on the user and force them to select
//++++   - and don't forget to centralise Project finance updating with the stasks/SDBP6_FINTEG code - tested & seems to work but still need to centralise [JC] 11/2
//-------- 5. Add SDBIP settings - mscoa version/compliance - die with glorious error message if non-compliant, die with friendly error message if compliant but custom/direct
//------6. Useful info
//------ - NT data string in ??? format? can add MSC GUID on end if desired (take advantage of all fixed reports)
//---- - Any unknown Project GUID will go to "Unspecified" - DONE 3/2 JC
//7. what next?
// - add option to "hide" info messages only until next login (not error messages) - store bool check in SESSION and add "hide" button to message - click button = set bool to true and hides div, only reload of page check session & if bool = true then don't display
//---- - ALERT ALERT ALERT - ALL DATA WILL BE DELETED - DONE 3/2 JC
//-----8. When form finished, restore warning message

//Notes to self 3/2/2020 JC AA-147
//------9. Make user select time period - ignore item 3 above - save time in code trying to figure out what the user has loaded and bypass hard-coded time period
// -----   - test passing of time_id
//------10. Implement selection of data_type - send financials to correct column based on time selected
//-------11. Rework process to accommodate the passing of a single type for a single time period, rather than the all types for all periods we're used to

//NOTES to self 11/2/2020 JC AA-147
//12. Option to hide messages (7 above)
//13. Do away with time period selection and use code
//14. Actually implement fin year validation
//15. Adjustments!!!!!!

$data_types = array('budget' => "Budget", 'actual' => "Actual");
//Adjustments to come

/*
** Do the necessary checks here for any errors to die for or any additional info the user might need.
*/
$can_upload = true;
$error = array();
$info = array();
$info[] = "Please Note: The first time you upload financials the process will take longer than usual as the individual financial line items are created.  Subsequent uploads will be faster.";
$info[] = "File Format Information: The import file must be a plain text, pipe (|) separated version of the NT data string.  The only permitted variation is the inclusion of the MSC GUID at the end of each line.  The MSC GUID can be left out however any MSC based Financial Fixed Reports will not generate correctly as no MSC information will be available.";
//Is the fin ref set? - if not then die!
if(strlen($sdbip_details['fin_year_ref']) == 0) {
	$can_upload = false;
	$error[] = "The ".$sdbipObject->replaceAllNames("|FINANCIALYEAR|")." code is blank.  This is required in order to match the imported financial data with the SDBIP to verify that the correct ".$sdbipObject->replaceAllNames("|FINANCIALYEAR|")." data is being loaded onto the correct ".$sdbipObject->replaceAllNames("|SDBIP|").". Please add the Code before continuing.  This can be done by logging in as the Assist Administrator and going to Master Setups > Financial Years.  Please contact your Assist Administrator if you do not have the necessary privileges to access Assist as the Assist Administrator.";
}
//Are there projects?  Not a die error, just warn
if($sdbip_details['summary_details']['PROJECT']['count'] < 1) {
	$info[] = "There are no ".$sdbipObject->replaceAllNames("|PROJECTS|")." loaded.  All ".$sdbipObject->replaceAllNames("|PROJECT|")." related information will default to the \"Unspecified\" ".$sdbipObject->replaceAllNames("|PROJECT|")." AND ".$sdbipObject->replaceAllNames("|Project|")." specific financial reporting will not be available.";
} else {
	$info[] = "Any financials belonging to ".$sdbipObject->replaceAllNames("|PROJECT|")." GUIDs which can not be found in the ".$sdbipObject->replaceAllNames("|SDBIP|")." will default to the \"Unspecified\" ".$sdbipObject->replaceAllNames("|PROJECT|").". This can not be changed later without re-importing the financials.  Please make any necessary corrections before importing the financials.";
}
$time_periods = $timeObject->getTimePeriodsWithCodeForFinances($sdbip_id);
foreach($time_periods as $key => $time) {
	$time_periods[$key] = $time['name'].(strlen($time['code']) > 0 ? " (".$time['code'].")" : "");
}
/* REMOVED as unnecessary due to selection of time period by user - JC 3 Feb 2020 AA-147
 * //Time periods - codes?
$empty_time = false;
$custom_time = false;
foreach($time_periods as $key => $time) {
	$time_periods[$key]['status'] = "";//"ok"; - see note below re default format
	if(strlen($time['code'])==0) {
		$empty_time=true;
		$time_periods[$key]['status'] = "info";
	} elseif(substr($time['code'],0,1)!="M") {
//		$custom_time = true;
		$time_periods[$key]['status'] = ""; //decided to ignore for now until certain of default format - JC AA-147 3 Feb 2020
	}
}
if($empty_time) {
	$info[] = "There is one or more ".$sdbipObject->replaceAllNames("|TIME|")." with no code.  The code will default to the format \"M+month position in ".$sdbipObject->replaceAllNames("|financialyear|")."\" E.g. If July is the first year of the ".$sdbipObject->replaceAllNames("|financialyear|")." then the code will default to M01.";
}*/
/*
** Display any issues identified above
*/
foreach($error as $err) {
	ASSIST_HELPER::displayResult(array("error", $err));
}
foreach($info as $in) {
	ASSIST_HELPER::displayResult(array("info", $in));
}
//can the time period display - no longer needed due to user selecting time period (to solve hard coded time period id issue in finteg_file_handler) AA-147 [JC] 3 Feb 2020
?> <!--
<div id="div_time_periods" style="display: inline-block; float:right;margin-right:20px">
	<table id="tbl_time" class="list">
		<tr>
			<th>Code</th>
			<th>Time Period</th>
		</tr>
		<?php
foreach($time_periods as $time_id => $time) {
	?>
		<tr>
			<td><?php if(strlen($time['status']) > 0) {
		echo ASSIST_HELPER::displayIconAsDiv($time['status'], "", "", "float");
	}
	echo "<div style='display:inline-block;margin-right:10px'>".$time['code']."</div>"; ?></td>
			<td><?php echo $time['name']; ?></td>
		</tr>
		<?php
}
?>
	</table>
</div> -->
<div id="div_form_table" style="width:49%;display:inline-block">
	<form name="newupload" method="post" enctype="multipart/form-data">
		<table id="tbl_action" class="form" width="600">
			<tbody>
			<tr>
				<th width="175"><?php echo $sdbipObject->replaceAllNames("|SDBIP|"); ?> Ref:</th>
				<td><?php echo $sdbip_details['ref']; ?><input type="hidden" value="<?php echo $sdbip_details['sdbip_id']; ?>" name="sdbip_id" id="sdbip_id" /></td>
			</tr>
			<tr>
				<th><?php echo $sdbipObject->replaceAllNames("|SDBIP|"); ?> Name:</th>
				<td><?php echo $sdbip_details['name']; ?></td>
			</tr>
			<tr>
				<th><?php echo $sdbipObject->replaceAllNames("|FINANCIALYEAR|"); ?>:</th>
				<td><?php echo $sdbip_details['fin_year_name']; ?><input type="hidden" value="<?php echo $sdbip_details['fin_year_id']; ?>" name="fin_year_id" id="fin_year_id" /></td>
			</tr>
			<tr>
				<th><?php echo $sdbipObject->replaceAllNames("|FINANCIALYEAR|"); ?> Code:</th>
				<td><?php
					if(strlen($sdbip_details['fin_year_ref']) == 0) {
						ASSIST_HELPER::displayIconAsDiv("error", "", "", "float");
					} else {
						echo ASSIST_HELPER::displayIconAsDiv("ok", "", "", "float")."<div style='display:inline-block'>".$sdbip_details['fin_year_ref']."</div> ";
					}
					?></td>
			</tr>
			<tr>
				<th>Number of <?php echo $sdbipObject->replaceAllNames("|PROJECTS|"); ?>:</th>
				<td><?php echo $sdbip_details['summary_details']['PROJECT']['count']; ?></td>
			</tr>
			<tr>
				<th>Time Period:</th>
				<td><?php
					/** @var SDBP6_DISPLAY $displayObject */
					$js .= $displayObject->drawFormField('LIST', array('id' => 'time_id',
						'name' => 'time_id',
						'required' => true,
						'options' => $time_periods)); ?></td>
			</tr>
			<tr>
				<th>Type:</th>
				<td><?php
					/** @var SDBP6_DISPLAY $displayObject */
					$js .= $displayObject->drawFormField('LIST', array('id' => 'data_type',
						'name' => 'data_type',
						'required' => true,
						'options' => $data_types)); ?></td>
			</tr>
			<?php
			if($can_upload) {
				?>
				<tr>
					<th>Attach NT File:</th>
					<td id="attach_containers">
						<?php
						/** @var SDBP6_DISPLAY $displayObject */
						$js .= $displayObject->drawFormField('ATTACH', array('id' => 'finteg_attachment',
							'name' => 'finteg_attachment',
							'required' => true,
							'allow_multiple' => false,
							'action' => 'FINANCE.UPLOADFILESTOPENDINGFOLDER',
							'page_direct' => 'admin_finance_manual.php?')); ?>
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<button id="btn_submit"><?php echo $sdbipObject->replaceAllNames("|IMPORT|")." & ".$sdbipObject->replaceAllNames("|SAVE|"); ?></button>
					</td>
				</tr>
				<?php
			} else {
				echo "<tr><td colspan='2'>";
				ASSIST_HELPER::displayResult(array("error", "An error has been found and the financials can not be imported until it is corrected.  Please review the error messages listed above this form."));
				echo "</td></tr>";
			} ?>
			</tbody>
		</table>
	</form>

	<div id="div_warn" width="600" style="width:580px;padding:10px;border:0px solid #fe9900;">
		<div id="dlg_confirm">

			<?php
			$warning_text = "<b>WARNING:</b> Clicking the \"".$sdbipObject->replaceAllNames("|IMPORT|")." & ".$sdbipObject->replaceAllNames("|SAVE|")."\" button above will <u>completely remove</u> all existing financials for the given time period and replace them with the financials in the imported document.<p class='center b'>This action cannot be undone!</p>  <p>Please be very sure that you are importing the correct file before continuing.</p>";
			ASSIST_HELPER::displayResult(array("warn", $warning_text));
			?>
			<label id="lbl_hide"> </label>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		<?php echo $js; ?>
		$('#btn_submit').button({
			icons: {primary: "ui-icon-arrowreturnthick-1-n"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green").css("background-color", "#FFFFFF")
			.hover(function () {
				$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
			}, function () {
				$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
			}).click(function (e) {
			e.preventDefault();
			console.log($('form').serialize());
			var count = 0;
			var attachments = false;
			$('input:file').each(function () {
				if ($(this).val().length > 0) {
					attachments = true;
					count++;
				}
			});
			if (attachments == true) {
				AssistHelper.processing();
				confirmSubmission();
				//SDBP6Helper.processObjectFormWithAttachment($('form[name="newupload"]'), 'FINANCE.UPLOADFILESTOPENDINGFOLDER', 'admin_finance_manual.php?');
			} else {
				alert('Please attach the NT string file.');
			}

		});
	});

	function saveFinished(icon, message) {
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon, message);
	}

	function nextStep() {
		AssistHelper.closeProcessing();

		SDBP6Helper.processObjectFormWithAttachment($('form[name="newupload"]'), 'FINANCE.UPLOADFILESTOPENDINGFOLDER', 'admin_finance_manual.php?');
		// alert("nextStep");
	}

	function confirmSubmission() {
		$("#dlg_confirm").dialog({autoOpen: false, modal: true});
		var buttons = [
			{
				text: "Confirm & Continue", click: function () {
					AssistHelper.processing();
					nextStep();
					$(this).dialog("destroy");
				}, class: 'ui-state-ok'
			},
			{
				text: "Cancel", click: function () {
					$(this).dialog("destroy");
					AssistHelper.closeProcessing();
				}, class: 'ui-state-error'
			}
		];
		$('#dlg_confirm').dialog('option', 'buttons', buttons).dialog('open');
		AssistHelper.hideDialogTitlebar('id', 'dlg_confirm');
		$('#dlg_confirm #lbl_hide').focus();
	}
</script>
<?php //require_once('inc_footer.php'); ?>
