<?php include("inc_ignite.php");

include("inc_pay.php");
$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
$cmpcode3 = strtolower($_POST['dcmpcode']);

 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function dateType(me) {
    var dtype = me.value;
    switch(dtype)
    {
        case "D":
            var target = document.getElementById('dday1');
            target.style.display = "inline";
            document.docup.dday1.disabled = false;
            document.docup.dmon1.disabled = false;
            document.docup.dyear1.disabled = false;
            var target = document.getElementById('d2');
            target.style.display = "none";
            break;
        case "M":
            document.docup.dday1.disabled = true;
            var target = document.getElementById('dday1');
            target.style.display = "none";
            document.docup.dmon1.disabled = false;
            document.docup.dyear1.disabled = false;
            var target = document.getElementById('d2');
            target.style.display = "none";
            break;
        case "P":
            document.docup.dday1.disabled = false;
            var target = document.getElementById('dday1');
            target.style.display = "inline";
            document.docup.dmon1.disabled = false;
            document.docup.dyear1.disabled = false;
            var target = document.getElementById('d2');
            target.style.display = "inline";
            break;
    }
}

function selDate(me) {
    if(me.value == "DD" || me.value == "YYYY")
    {
        me.value = "";
    }
    me.className='dateField';
}

function offDate(me,t) {
    if(me.value == "")
    {
        if(t == "D")
            me.value = "DD";
        else
            me.value = "YYYY";
        me.className='dateField1';
    }
}

function Validate(me) {
    var dtkid = me.dtkid.value;
    var dtype = me.dtype.value;
    var datetype = me.datetype.value;
    var dtitle = me.dtitle.value;
    var dfile = me.dfile.value;
    var err = "";
    var target;
    
    if(dtkid == "X" || dtkid.length == 0)
    {
        err = err+"\n - Employee";
        target = "dtkid";
        document.getElementById(target).className = 'required';
    }
    if(dtype == "X" || dtype.length == 0)
    {
        err = err+"\n - Document Type";
        target = "dtype";
        document.getElementById(target).className = 'required';
    }
    if(datetype.length == 0)
    {
        err = err+"\n - Date Type";
        target = "datetype";
        document.getElementById(target).className = 'required';
    }
    else
    {
        switch(datetype)
        {
            case "D":
                var d1 = me.dday1.value;
                var m1 = me.dmon1.value;
                //alert(m1);
                var y1 = me.dyear1.value;
                //alert(y1);
                if(d1.length > 2 || d1.length < 1 || d1 == "DD" || isNaN(parseInt(d1)))
                {
                    err = err+"\n - Date: Day";
                    target = "dday1";
                    document.getElementById(target).className = 'required';
                }
                if(m1.length > 2 || m1.length < 1)
                {
                    err = err+"\n - Date: Month";
                    target = "dmon1";
                    document.getElementById(target).className = 'required';
                }
                if(y1.length != 4 || y1 == "YYYY" || isNaN(parseInt(y1)))
                {
                    err = err+"\n - Date: Year (4-digit year)";
                    target = "dyear1";
                    document.getElementById(target).className = 'required';
                }
                break;
            case "M":
                var m1 = me.dmon1.value;
                //alert(m1);
                var y1 = me.dyear1.value;
                //alert(y1);
                if(m1.length > 2 || m1.length < 1)
                {
                    err = err+"\n - Date: Month";
                    target = "dmon1";
                    document.getElementById(target).className = 'required';
                }
                if(y1.length != 4 || y1 == "YYYY" || isNaN(parseInt(y1)))
                {
                    err = err+"\n - Date: Year (4-digit year)";
                    target = "dyear1";
                    document.getElementById(target).className = 'required';
                }
                break;
            case "P":
                var d1 = me.dday1.value;
                var m1 = me.dmon1.value;
                //alert(m1);
                var y1 = me.dyear1.value;
                //alert(y1);
                if(d1.length > 2 || d1.length < 1 || d1 == "DD" || isNaN(parseInt(d1)))
                {
                    err = err+"\n - Date: Day (From)";
                    target = "dday1";
                    document.getElementById(target).className = 'required';
                }
                if(m1.length > 2 || m1.length < 1)
                {
                    err = err+"\n - Date: Month (From)";
                    target = "dmon1";
                    document.getElementById(target).className = 'required';
                }
                if(y1.length != 4 || y1 == "YYYY" || isNaN(parseInt(y1)))
                {
                    err = err+"\n - Date: Year (From) (4-digit year)";
                    target = "dyear1";
                    document.getElementById(target).className = 'required';
                }
                var d2 = me.dday2.value;
                var m2 = me.dmon2.value;
                var y2 = me.dyear2.value;
//                alert(m2);
//                alert(y2);
                if(d2.length > 2 || d2.length < 1 || d2 == "DD" || isNaN(parseInt(d2)))
                {
                    err = err+"\n - Date: Day (To)";
                    target = "dday2";
                    document.getElementById(target).className = 'required';
                }
                if(m2.length > 2 || m2.length < 1)
                {
                    err = err+"\n - Date: Month (To)";
                    target = "dmon2";
                    document.getElementById(target).className = 'required';
                }
                if(y2.length != 4 || y2 == "YYYY" || isNaN(parseInt(y2)))
                {
                    err = err+"\n - Date: Year (To) (4-digit year)";
                    target = "dyear2";
                    document.getElementById(target).className = 'required';
                }
                break;
            default:
                err = err+"\n - Date Type";
                target = "datetype";
                document.getElementById(target).className = 'required';
        }
    }
    if(dtitle.length == 0)
    {
        err = err+"\n - Document Title";
        target = "dtitle";
        document.getElementById(target).className = 'required';
    }
    if(dfile.length == 0)
    {
        err = err+"\n - File";
        target = "dfile";
        document.getElementById(target).className = 'required';
    }
    if(me.ftype[0].checked == false && me.ftype[1].checked == false)
    {
        err = err+"\n - File Type";
        target = "f1l";
        document.getElementById(target).className = 'required';
        target = "f2l";
        document.getElementById(target).className = 'required';
    }

    if(err.length > 0)
    {
        alert("Please complete the following required fields as indicated:"+err);
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Payroll Update - Add New Document</b></h1>
<?php
displayResult($result);
if($paycmp == "Y" && (strlen($cmpcode3)==0 || $cmpcode == "X"))
{
?>
<form name=docup method=post action=doc.php>
<table border=1 cellpadding=3 cellspacing=0 width=600 style="border-collapse: collapse; border-style: solid;">
    <tr  valign=top>
        <td class=tdheaderl width=80><b>Company:</b>&nbsp;</td>
        <td width=520><select name=dcmpcode>
        <option selected value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT c.cmpcode, c.cmpname FROM assist_pay_docusers d, assist_company c ";
        $sql.= "WHERE d.cmpcode = c.cmpcode AND d.tkid = '".$tkid."' ";
        $sql.= "ORDER BY c.cmpname";
        include("inc_db_con_assist.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option value=".$row['cmpcode'].">".$row['cmpname']." (".$row['cmpcode'].")</option>");
            }
        mysql_close();
        ?>
        </select>&nbsp;</td>
    </tr>
    <tr valign=top>
        <td class=tdheaderl ><b>Employee:</b>&nbsp;</td>
        <td><select name=dtkid>
        <option selected value=X>Please select a company first</option>
        </select>&nbsp;</td>
    </tr>
    <tr class=tdgeneral valign=top>
        <td colspan=2><input type=submit value=" Go ">&nbsp;</td>
    </tr>
</table>
</form>
<?php
}
else
{
    if(strlen($cmpcode3)==0) { $cmpcode3 = $cmpcode; }
?>
<form name=docup method=post action=doc_process.php enctype="multipart/form-data" onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=cc3 value=<?php echo($cmpcode3); ?>>
<table border=1 cellpadding=3 cellspacing=0 width=600 style="border-collapse: collapse; border-style: solid;">
    <tr valign=top>
        <td class=tdheaderl width=150><b>Employee:</b>&nbsp;</td>
        <td width=450><select name=dtkid>
        <option selected value=X>--- SELECT ---</option>
        <?php
        $sql3 = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode3."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
        $sql3.= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
        $sql3.= "AND t.tkid = m.usrtkid AND m.usrmodref = 'PAY' ";
        $sql3.= "ORDER BY t.tkname, t.tksurname";
        include("inc_db_con3.php");
            while($row = mysql_fetch_array($rs3))
            {
                echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
            }
        mysql_close();
        ?>
        </select>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=dc1 valign=top>
        <td class=tdheaderl ><b>Document type:</b>&nbsp;</td>
        <td><select name=dtype><option selected value=X>--- SELECT ---</option>
        <?php
        $sql = "SELECT * FROM assist_".$cmpcode."_pay_list_doctype WHERE yn = 'Y' ORDER BY value";
        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                $ref = $row['id'];
                $val = $row['value'];
                echo("<option value=".$ref.">".$val."</option>");
            }
        mysql_close();
        ?>
        </select>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=dc2 valign=top>
        <td class=tdheaderl ><b>Date type:</b>&nbsp;</td>
        <td><select name=datetype onchange=dateType(this)>
        <option selected value=D>Specific date (dd mmm yyyy)</option>
        <option value=M>Specific month (mmm yyyy)</option>
        <option value=P>Specific period (dd mmm yyyy - dd mmm yyyy)</option>
        </select>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=dc3 valign=top>
        <td class=tdheaderl ><b>Date:</b>&nbsp;</td>
        <td><input type=text size=2 value=DD name=dday1 onfocus="selDate(this)" onblur="offDate(this,'D')" class=dateField1>
        <select name=dmon1>
        <?php
        $mtoday = date("n");
        for($m=1;$m<13;$m++)
        {
            echo("<option ");
            if($mtoday == $m)
            {
                echo("selected");
            }
            echo(" value=".$m.">".date("F",mktime(12,0,0,$m,1,2008))."</option>");
        }
        ?>
        <input type=text name=dyear1 size=4 value=YYYY onfocus="selDate(this)" onblur="offDate(this,'Y')" class=dateField1>
        <span id=d2> - <input type=text name=dday2 size=2 value=DD onfocus="selDate(this)" onblur="offDate(this,'D')" class=dateField1>
        <select name=dmon2>        <?php
        $mtoday = date("n");
        for($m=1;$m<13;$m++)
        {
            echo("<option ");
            if($mtoday == $m)
            {
                echo("selected");
            }
            echo(" value=".$m.">".date("F",mktime(12,0,0,$m,1,2008))."</option>");
        }
        ?>
</select>
        <input type=text name=dyear2 size=4 value=YYYY onfocus="selDate(this)" onblur="offDate(this,'Y')" class=dateField1></span></td>
    </tr>
    <tr class=tdgeneral id=dc4 valign=top>
        <td class=tdheaderl ><b>Document title:</b>&nbsp;</td>
        <td><input type=text name=dtitle size=50>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=dc5 valign=top>
        <td class=tdheaderl ><b>File:</b>&nbsp;</td>
        <td><input type=file name=dfile>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=dc7 valign=top>
        <td class=tdheaderl  valign=top width=80><b>File Type:</b>&nbsp;</td>
        <td width=520><input checked type=radio name=ftype id=f1 value=I> <label for="f1" id=f1l>Individual file <small><i>(the file contains a single document and is specific to the user)</label></i></small><br>
        <input type=radio name=ftype id=f2 value=G> <label for="f2" id=f2l>Group document <small><i>(the file contains multiple documents that the user needs to distribute to their employees)</label></i></small>&nbsp;</td>
    </tr>
    <tr class=tdgeneral id=dc6 valign=top>
        <td colspan=2><input type=submit value=Submit class=isubmit> <i>(Note: All fields are required.)</i></td>
    </tr>
</table>
</form>
<script language=JavaScript>
            var target = document.getElementById('d2');
            target.style.display = "none";
</script>
<?php
}
?>
</body>

</html>
