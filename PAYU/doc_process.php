<?php include("inc_ignite.php");

include("inc_pay.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<?php include("inc_style.php"); ?>
<style type="text/css">
.lab1
{
    color: 006600;
}
.lab2
{
    background-color: #cc0001;
    color: #cc0001;
}
.lab3
{
    background-color: #006600;
    color: #006600;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=10 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Payroll Update - Documents</b></h1>
<span id=load><center>
<table class=noborder cellpadding=3 cellspacing=0 style="border-collapse: collapse; border-style: solid; border-width: 0px" width=350>
    <tr>
        <td class="noborder tdgeneral" colspan=2 align=center><label id=label1 for=lbl>Processing, please wait...&nbsp;<input type=hidden name=lbl></td>
    </tr>
    <tr>
        <td class="noborder tdgeneral" align=right width=300><table cellpadding=0 cellspacing=0 width=252 style="border-collapse: collapse; border-style: solid; border-width: 1px;" class=noborder>
            <tr>
                <td class="noborder tdgeneral" style="line-height: 90%"><label id=label3 for=lbl class=lab2>|</label></td>
            </tr>
            </table>
        </td>
        <td class="noborder tdgeneral" width=50><label id=label2 for=lbl>0%</label>&nbsp;</td>
    </tr>
    <tr id=tr1>
        <td class="noborder tdgeneral" colspan=2 align=center><br><input type=button value="Add Another" onclick="document.location.href='doc.php';"></td>
    </tr>
</table>
</center></span>
<script language=JavaScript>
    var target = document.getElementById('tr1');
    target.style.display = "none";
</script>
<?php
//GET FORM DETAILS
$cmpcode3 = $_POST['cc3'];
$dtkid = $_POST['dtkid'];
$dtype = $_POST['dtype'];
$datetype = $_POST['datetype'];
$dday1 = $_POST['dday1'];
$dmon1 = $_POST['dmon1'];
$dyear1 = $_POST['dyear1'];
$dday2 = $_POST['dday2'];
$dmon2 = $_POST['dmon2'];
$dyear2 = $_POST['dyear2'];
$dtitle = $_POST['dtitle'];
$ftype = $_POST['ftype'];
$ddate1 = 0;
$ddate2 = 0;
//echo($dday1." ".$dmon1." ".$dyear1." - ".$dday2." ".$dmon2." ".$dyear2);
//die();
$dtitle = str_replace("'","&#39",$dtitle);
?>
    <script language=JavaScript>
        label3.innerText="|||||";
        label2.innerText="10%";
    </script>
<?php
//CHECK VARIABLES / DOCUMENT
if(strlen($cmpcode3)==0) { die("company code error"); }
if(strlen($dtkid)==0 || $dtkid=="X") { die("user error"); }
if($dtype<1) { die("document type error"); }
if(strlen($datetype)!=1) { die("datetype error"); }
switch($datetype)
{
    case "D":
        if(!mktime(12,0,0,$dmon1,$dday1,$dyear1)) { die("date error"); } else { $ddate1 = mktime(12,0,0,$dmon1,$dday1,$dyear1); }
        break;
    case "M":
        if(!mktime(12,0,0,$dmon1,1,$dyear1)) { die("date error"); } else { $ddate1 = mktime(12,0,0,$dmon1,1,$dyear1); }
        break;
    case "P":
        if(!mktime(12,0,0,$dmon1,$dday1,$dyear1)) { die("from date error"); } else { $ddate1 = mktime(12,0,0,$dmon1,$dday1,$dyear1); }
        if(!mktime(12,0,0,$dmon2,$dday2,$dyear2)) { die("to date error"); } else { $ddate2 = mktime(12,0,0,$dmon2,$dday2,$dyear2); }
        break;
    default:
        break;
}
if(strlen($dtitle)==0) { die("title error"); }
if(strlen($ftype)!=1) { die("file type error"); }
if ($_FILES["dfile"]["error"] > 0) { die("file error"); }
?>
    <script language=JavaScript>
        label3.innerText="||||||||||";
        label2.innerText="20%";
    </script>
<?php
//INSERT DOCUMENT RECORD
$docid = "";
$sql3 = "INSERT INTO assist_".$cmpcode3."_pay_documents SET ";
$sql3.= " tkid = '".$dtkid."', ";
$sql3.= " typeid = ".$dtype.", ";
$sql3.= " datetype = '".$datetype."', ";
$sql3.= " date1 = ".$ddate1.", ";
$sql3.= " date2 = ".$ddate2.", ";
$sql3.= " title = '".$dtitle."', ";
$sql3.= " filename = '', ";
$sql3.= " filetype = '".$ftype."', ";
$sql3.= " descrip = '', ";
$sql3.= " adduser = '".$tkid."', ";
$sql3.= " adddate = ".$today.", ";
$sql3.= " yn = 'N', ";
$sql3.= " moduser = '".$tkid."', ";
$sql3.= " moddate = ".$today;
include("inc_db_con3.php");
$docid = mysql_insert_id();
    $tref = "PAYU";
    $tsql = $sql3;
    $trans = "Created new payroll document record ".$docid;
    include("inc_transaction_log.php");
if(strlen($docid)==0) { die("doc insert error"); }
?>
    <script language=JavaScript>
        label3.innerText="||||||||||||||||||||";
        label2.innerText="40%";
    </script>
<?php
//UPLOAD DOC
$fname = $_FILES["dfile"]["name"];
$f1narr = explode(".",$fname);
$f1nc = count($f1narr);
$f1ext = $f1narr[$f1nc-1];
$filename = "payroll_".$docid.".".$f1ext;
$fileloc = "../files/".$cmpcode3."/".$filename;
?>
    <script language=JavaScript>
        label3.innerText="||||||||||||||||||||||||||||||";
        label2.innerText="60%";
    </script>
<?php
$err = "Y";
if(copy($_FILES["dfile"]["tmp_name"], $fileloc))
{
    $tref = "PAYU";
    $tsql = "";
    $trans = "Uploaded payroll document ".$filename;
    include("inc_transaction_log.php");
}
else
{
    die("file upload error");
}
?>
    <script language=JavaScript>
        label3.innerText="||||||||||||||||||||||||||||||||||||||||";
        label2.innerText="80%";
    </script>
<?php
//UPDATE DOCUMENT RECORD
$sql3 = "UPDATE assist_".$cmpcode3."_pay_documents SET ";
$sql3.= " filename = '".$filename."', ";
$sql3.= " yn = 'Y' ";
$sql3.= " WHERE id = ".$docid;
include("inc_db_con3.php");
    $tref = "PAYU";
    $tsql = $sql3;
    $trans = "Updated filename in record for document ".$docid;
    include("inc_transaction_log.php");
?>
    <script language=JavaScript>
        label3.innerText="||||||||||||||||||||||||||||||||||||||||||||||||||";
        label2.innerText="100%";
        label1.innerText = "Processing complete.  Thank you for your patience.";
        label1.className = 'lab1';
        label3.className = 'lab3';
    </script>
<?php
//MOVE ON
?>
<script language=JavaScript>
    target = document.getElementById('tr1');
//    target.style.display = "inline";
document.location.href = 'doc.php?r[]=ok&r[]=Document+added+successfully';
</script>
</body>

</html>
