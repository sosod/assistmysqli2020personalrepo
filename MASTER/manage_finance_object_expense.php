<?php
//$_REQUEST['object_type'] = "FINANCE";

include("inc_header.php");

include("0_finance_test_data.php");
$default_width = "400px";
$currency_default_width = "110px";
$perc_default_width = "100px";


$object_type = "CONTRACT";
$object_id = $_REQUEST['object_id'];
			
$myObject = new MASTER_CONTRACT();

$test_items = array();
for($i=1;$i<=5;$i++) {
	$test_items[$i] = "Test item ".$i;
}
 
?>
<h3 class=idelete>Under development</h3>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,false,array(),true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<h2>!New Expenditure!</h2>
			<table class=form>
				<tr>
					<th>!Service Provider!:</th>
					<td><?php $js.=$displayObject->drawFormField("LIST",array('options'=>array(),'unspecified'=>false))?></td>
				</tr>
				<tr>
					<th>!Invoice Number!:</th>
					<td><?php $js.=$displayObject->drawFormField("MEDVC",array()); ?></td>
				</tr>
				<tr>
					<th>!Invoice Amount!:</th>
					<td><?php $js.=$displayObject->drawFormField("CURRENCY",array()); ?></td>
				</tr>
				<tr>
					<th>!VAT!:</th>
					<td><?php $js.=$displayObject->drawFormField("CURRENCY",array()); ?></td>
				</tr>
				<tr>
					<th>!Retention Amount!:</th>
					<td><?php $js.=$displayObject->drawFormField("CURRENCY",array()); ?></td>
				</tr>
				<tr>
					<th>!Vote Number!:</th>
					<td><?php $js.=$displayObject->drawFormField("MEDVC",array()); ?></td>
				</tr>
				<tr>
					<th>!Funding Type!:</th>
					<td><?php $js.=$displayObject->drawFormField("LIST",array('options'=>$test_items)); ?></td>
				</tr>
				<tr>
					<th>!Funding Source!:</th>
					<td><?php $js.=$displayObject->drawFormField("MULTILIST",array('options'=>$test_items)); ?></td>
				</tr>
				<tr>
					<th>!Comments!:</th>
					<td><?php $js.=$displayObject->drawFormField("TEXT"); ?></td>
				</tr>
				<tr>
					<th>!Attachment!:</th>
					<td><?php $js.=$displayObject->drawFormField("ATTACH",array('action'=>"NEW",'page_direct'=>"manage_finance.php?")); ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=3><div id=div_budget style='background-color: #ffffff;overflow:auto; border: 1px solid #ababab;'>
<?php
$total_contract_budget = 0; 
echo "
			<table id=tbl_budget style='margin: 0 auto;'>
				<thead>
				<tr>
					<th width=".$default_width." id=th_first>".$helper->getObjectName("deliverable")."</th>
					<th >Allocate ".$expense_heading." Amount to be paid</th>
					<th >Allocate Past ".$retention_heading." Amount to be paid</th>
					<th >Allocate ".$retention_heading." Amount to be held</th>
					<th >Past ".$expense_heading." Allocation</th>
					<th >Past ".$retention_heading." Allocation</th>
					<th >".$budget_heading."</th>
					<th >".$var_heading."</th>
				</tr>
			</thead>";
			/**
			 * FIRST ROW = CONTRACT_SUPPLIER settings
			 */
			echo "
				<tr class='trhover-dark b'>
					<td class=right width=".$default_width.">".$helper->getObjectName("contract")." ".$total_heading.":</td>
					<td class=right>N/A</td>
					<td class=right>N/A</td>
					<td class=right>N/A</td>
					<td class=right>R total expense recorded</td>
					<td class=right>R total retention recorded</td>";
			echo "
					<td class=right>"; $displayObject->drawDataField("CURRENCY",$total_contract_budget,$currency_options); echo "</td>
					<td class=right>"; $displayObject->drawDataField("CURRENCY","0",$currency_options); echo "</td>
				</tr>";
			/**
			 * SECOND ROW = Totals of values captured by user
			 */
			echo "
				<tr class='trhover b'>
					<td class=right width=".$default_width.">Total:</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"total_total_budget "),"sum of new expense"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"total_total_budget "),"sum of new retention paid"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"total_total_budget "),"sum of new retention held"); echo "</td>
					<td class=right>R total expense allocated</td>
					<td class=right>R total retention allocated</td>
					<td class=right>"; $displayObject->drawDataField("CURRENCY","0",$currency_options); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('class'=>"total_total_budget "),"sum of variance"); echo "</td>
				</tr>";
			/**
			 * ROWS 3+ = Deliverables settings
			 */
foreach($child_objects_ordered[0] as $i => $c) {
	$a = $c;
	echo "
				<tr class=b>
					<td width=".$default_width.">".$a['reftag'].": ".$a['name']."</td>";
	echo "
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$i,'class'=>"D_total_budget D".$i),"new expense"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$i,'class'=>"D_total_budget D".$i),"new retention paid"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$i,'class'=>"D_total_budget D".$i),"new retention held"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"D_total_budget D".$i),"existing expense allocated to deliverable"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"D_total_budget D".$i),"existing retention allocated to deliverable"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"D_total_budget D".$i),"budget as per new"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$i,'class'=>"D_total_budget D".$i),"new expense + new retention paid + old expense - budget"); echo "</td>
				</tr>";
	if($c['del_type']=="MAIN") {
		foreach($child_objects_ordered[$i] as $x => $z) {
			$a = $z;
			echo "
				<tr>
					<td width=".$default_width.">".$a['reftag'].": ".$a['name']."</td>";
	echo "
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$x,'class'=>"D_total_budget D".$x),"new expense"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$x,'class'=>"D_total_budget D".$x),"new retention paid"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("CURRENCY",array('symbol'=>"",'del'=>$x,'class'=>"D_total_budget D".$x),"new retention held"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$x,'class'=>"D_total_budget D".$x),"existing expense allocated to deliverable"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$x,'class'=>"D_total_budget D".$x),"existing retention allocated to deliverable"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$x,'class'=>"D_total_budget D".$x),"budget as per new"); echo "</td>
					<td class=right>"; $displayObject->drawFormField("LABEL",array('del'=>$x,'class'=>"D_total_budget D".$x),"new expense + new retention paid + old expense - budget"); echo "</td>
				</tr>";
		}
	}
}
?>
			</table></div></td>
	</tr>
	<tr>
		<td colspan=3 class=center><input type=button value="<?php echo $helper->getActivityName("save"); ?>" class=isubmit /></td>
	</tr>
	<tr>
		<td colspan=3><?php echo $displayObject->drawPageFooter($helper->getGoBack(),"","",""); ?></td>
	</tr>
</table>
<script type="text/javascript" >
$(function() {
	<?php echo $js; ?>
	$("#tbl_budget .number-only").parent().children("div").hide();
	var screen = AssistHelper.getWindowSize();
	var table_width = AssistString.substr($("#tbl_budget").css("width"),0,-2)*1;
	var div_width = (screen.width-40);
	$("#div_budget").css("width",div_width+"px");
	var h = 0;
	$("#tbl_budget tr:gt(3)").each(function() {
		var x = AssistString.substr($(this).css("height"),0,-2)*1;
		if(x>h) { h = x; }
	});
	var max_width = 500;
	var add_width = 0;
	var increment = 50;
	var h2 = 0;
	while(h>50) {
		add_width+=increment;
		table_width+=increment;
		$("#tbl_budget").prop("width",table_width+"px");
		if(add_width>=max_width) {
			h = 0;
		} else {
			h2 = 0;
			$("#tbl_budget tr:gt(3)").each(function() {
				var x = AssistString.substr($(this).css("height"),0,-2)*1;
				if(x>h2) { h2 = x; }
			});
			h = h2;
		}
	}
	var default_size = $(".number-only.budget:first").prop("size"); 
	$(".number-only").addClass("right");
});
</script>

