<?php
$page_redirect_path = "admin_template_new.php?object_type=CONTRACT&object_id=";

include("inc_header.php");

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
$object_id = $_REQUEST['object_id'];

$myObject = new CNTRCT_CONTRACT();
$childObject = new CNTRCT_DELIVERABLE();
$child_type = "DELIVERABLE";
$child_options = array('type'=>"LIST",'section'=>"ADMIN",'contract_id'=>$object_id);
$child_objects = $childObject->getObject($child_options); 
$child_redirect = "new_confirm_details.php?object_type=DELIVERABLE&object_id=";
$child_name = $helper->getDeliverableObjectName(true);

$gchildObject = new CNTRCT_ACTION();
$gchild_type = "ACTION";
$gchild_options = array('type'=>"LIST",'section'=>"ADMIN",'C.contract_id'=>$object_id);
$gchild_objects = $gchildObject->getObject($gchild_options); 
$gchild_redirect = "new_confirm_details.php?object_type=ACTION&object_id=";
$gchild_name = $helper->getActionObjectName(true);

//ASSIST_HELPER::arrPrint($child_objects);
?>
<div id="div_error">
	
</div>
<h2>Summary of <?php echo $helper->getContractObjectName()." ".$myObject->getRefTag().$object_id; 
	$summ = $myObject->getSummary($object_id); //ASSIST_HELPER::arrPrint($childObject->getDeliverableNamesForSubs(7)); ?></h2>
<table class=tbl-container width=100%>
	<tr>
		<td width=48%>
		<?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false);
			echo "<input type=hidden name=contract_id value=".$object_id." id=contract_id /><input type=button value=Confirm class=isubmit id=btn_confirm />";
		?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=47%>
			<h2><?php echo $child_name; ?></h2>
				<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>"View",'class'=>"btn_view"),$child_type,$child_options); ?><br><br>
			<h2><?php echo $gchild_name; ?></h2>
				<?php $js.=$displayObject->drawListTable($gchild_objects,array('value'=>"View",'class'=>"btn_view", 'pager'=>"sub_child"),$gchild_type,$gchild_options); ?>
		</td>
	</tr>
	<tr>
		<td colspan=3>
		</td>
	</tr>
	<?php 
	if($object_type=="CONTRACT") {
	?>
	<tr>
		<td colspan=3 class=center><?php
		if($myObject->canIConfirm($object_id)) {
		} else {
			ASSIST_HELPER::displayResult(array("error","The ".$helper->getContractObjectName()." cannot be confirmed without at least 1 ".$helper->getDeliverableObjectName()." and ".$helper->getActionObjectName()));
		}
		?></td>
	</tr>
	<?php } ?>
</table>
<div id="confirm_div" style="display:none">
	<h3>Customise this template</h3>
	<p>Capture the following information:</p><br>
		<table>
			<tr>
				<th>Template name</th>				
		 		<td>
					<form id="temp_details" >
			 			<input req="1" name="template_name" id="temp_name" type="text" />
			 		<!--	<input type="hidden" name="template_source_contract_id" value="<?php echo $object_id; ?>" />
			 			<input type="hidden" name="template_insertuser" value="<?php echo $object_id; ?>" />	-->
					</form>
		 		</td>
			</tr>
		</table>
</div>
<script type="text/javascript">
	<?php echo $js; ?>
	$('#btn_confirm').click(function(){
		$('#confirm_div').show().dialog({
			modal:true,
			buttons:[{
					text:"Okay",
					click:function(){
					 	AssistHelper.processing();
					 	$form = $("#temp_details");
			//k		 	var serialized = CntrctHelper.processObjectForm($form, "Template.Add&id=<?php //echo $object_id; ?>", "admin_template_new.php?");
					 	var serialize = AssistForm.serialize($form);
						var result = AssistHelper.doAjax("inc_controller.php?action=Template.Add&id="+<?php echo $object_id; ?>, serialize);
					 	//console.log(result);
						$(this).dialog("close");
						AssistHelper.finishedProcessing(result[0],result[1]);
						setTimeout(function(){
							document.location.href = "<?php echo $page_redirect_path; ?>";
						},2000);
				 	}
				},{
					text:"Cancel",
					click:function(){
						$(this).dialog("close");
					}
			}]
		});
		AssistHelper.hideDialogTitlebar("id", "confirm_div");
	});
	$('#btn_process').click(function(){
		AssistHelper.processing();
		AssistHelper.finishedProcessing(result[0], result[1]);
	});
	
	$("* [role='button']").hoverIntent({
		over:function(){
			var hint = $(this).prop("classList")[0];
			AssistHelper.showHelp(hint);
		}, 
		out:function(){
			AssistHelper.hideHelp(30);
		}
	});

</script>
