<?php
error_reporting(-1);
require_once("inc_header.php");
$me = new ASSIST_HELPER();
$mod = new ASSIST_MODULE_HELPER();
$db = new ASSIST_DB();
$sections = array(
	'CON'=>"Contract",
	'DEL'=>"Deliverable",
	'ACT'=>"Action",
	'ASS'=>"Assurance",
	'FIN'=>"Finance",
	'LIST'=>"Lists"
);

foreach($sections as $key => $sec) {
	$toc[] = "<a href=#".$key.">".$sec."</a>";
}

echo "<p style=\"margin-top: -10px; font-size: 7.5pt\"><a name=top></a>".implode(" | ",$toc)."</p>";

/*
foreach($sections as $key => $sec) {
	echo "<h2><a name=".$key."></a>".$sec."</h2>";
	if($key!="LIST") {
		$gloss = 0;
		$echo = "";
			foreach($mheadings[$key] as $fld => $h) {
				if(strlen($h['glossary'])>0) {
					$gloss++;
					$echo .="<tr>";
						$echo.= "<th style=\"vertical-align: top;\" class=left>".$h['h_client'].":&nbsp;</td>";
						$echo.= "<td>".$h['glossary']."</td>";
					$echo.= "</tr>";
				}
			}
		if(!$gloss) {
			echo "<p>No definitions available.</p>";
		} else {
			echo "<table width=600px>";
			echo $echo;
			echo "</table>";
		}
		echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		if(in_array($key,array("KPI","TOP"))) {
			$mhead = $me->getModuleHeadings(array($key."_R"));
			$mhead = $mhead[$key."_R"];
			$gloss = 0;
			echo "<h3>".$sec.": Results</h3>";
			$echo = "";
			foreach($mhead as $fld => $h) {
				if(strlen($h['glossary'])>0) {
					$gloss++;
					$echo.= "<tr>";
						$echo.= "<th style=\"vertical-align: top;\" class=left>".$h['h_client'].":&nbsp;</td>";
						$echo.= "<td>".$h['glossary']."</td>";
					$echo.= "</tr>";
				}
			}
			if(!$gloss) {
				echo "<p>No definitions available.</p>";
			} else {
				echo "<table width=600px>";
				echo $echo;
				echo "</table>";
			}
			echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		}
		echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
	} else {
		$sql = "SELECT * FROM ".$mod->getDBRef()."_setup_headings WHERE h_type = 'LIST' AND h_table NOT IN ('dir','subdir','owner','targettype','finance_payment_term','finance_payment_frequency')";
		$rs = $db->mysql_fetch_all($sql);
		ASSIST_HELPER::arrPrint($rs);
		foreach($rs as $h) {
			echo "<h3>".$h['h_client']."</h3>";
			?>
			<table>
				<tr>
					<th>Ref</th>
					<th>List Item</th>
					<?php //if(in_array($h['h_table'],$code_lists)) { echo "<th>Code</th>"; } ?>
					<?php if($h['h_table']=="calctype") { echo "<th>Description</th>"; } ?>
				</tr>
				<?php 
				$rs2 = "SELECT * FROM ".$mod->getDBRef()."_list_".$h['h_table']." WHERE 'status' = 2 ORDER BY sort";
				while($row = $db->mysql_fetch_all($rs2)) {
				?>
					<tr>
						<th><?php echo $row['id']; ?></th>
						<td><?php echo $row['value']; ?>&nbsp;&nbsp;</td>
						<?php //if(in_array($h['h_table'],$code_lists)) { echo "<td class=centre>".$row['code']."</td>"; } ?>
					<?php if($h['h_table']=="calctype") { echo "<td>".$row['descrip']."</td>"; } ?>
					</tr>
				<?php
				}
				?>
			</table>
			<?php
			echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
		}
	}
}
*
 * 
 */
echo "<p><a href=#top><img src=/pics/tri_up.gif style=\"vertical-align: middle; border-width: 0px;\"></a> <a href=#top>Back to Top</a></p>";
echo "<p style=\"text-align: center; font-size: 2pt; \"><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /><img src=/pics/assist_line_h2.gif /></p>";
?>