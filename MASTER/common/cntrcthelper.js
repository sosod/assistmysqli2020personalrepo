var CntrctHelper = {
	
	processObjectForm : function($form,page_action,page_direct,$onScreen) {
		//console.log($form);
		//alert(page_action);
		var valid = CntrctHelper.validateForm($form,false,$onScreen);
		//var valid=false;
		if(valid) {
			if(typeof $form.find("#customer_type") !== "undefined"){
				$type = $form.find("#customer_type");
				if($type.val() == 1){
					$form.find("input[id*='customer_o_'], select[id*='customer_o_']").remove();
				}else if($type.val() == 2){
					$form.find("input[id*='customer_p_'], select[id*='customer_p_']").remove();
				}
			} 
			window.isValid = true;
			AssistHelper.processing();
			var dta = AssistForm.serialize($form); 
	//console.log(dta);
			if(page_action == "serialize") {
				return dta;
			} else {
				//alert("inc_controller.php?action="+page_action+' - DTA ='+dta);
				var result = AssistHelper.doAjax("inc_controller.php?action="+page_action,dta);
				//Remove console.log when finished, and uncomment redirects!
	//console.log(result);
	//console.log(result.responseText);
				if(result[0]==="ok") {
					if(page_direct=="home") {
						CntrctHelper.routeHome();
					} else {
						document.location.href = page_direct+'r[]='+result[0]+'&r[]='+result[1];
					}
				} else if(result[0]==="info"){
					AssistHelper.closeProcessing();
					return result;
				}else{
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		}else{
			window.isValid = false;
		}	
	},
	processObjectFormWithAttachment : function($form,page_action,page_direct,$onScreen) {
		//console.log('raw form:      -');
		//console.log($form);
		//console.log("-00------------pOFwA--"+AssistForm.serialize($form));
		var valid = CntrctHelper.validateForm($form,false,$onScreen);
		//var valid=true;
		console.log(valid);
		if(valid) {
			AssistHelper.processing();
			$onScreen.prop("target","file_upload_target");
			$onScreen.prop("action","inc_controller.php");
			//$form.prop("action","inc_controller.php?Contract.Add");
		//	$form.append("<input type=hidden name=page_direct value='"+page_direct+"' />");
		//	$form.append("<input type=hidden name=action value='"+page_action+"' />");
			//alert(AssistForm.serialize($form));
			$onScreen.submit();
			/*
			if(page_action == "serialize") {
				return serial;
			} else {
				var dta = AssistForm.serialize($form); 
				//alert(page_action+"\n\n"+dta);
				var result = AssistHelper.doAjax("inc_controller.php?action="+page_action,dta);
				//Remove console.log when finished!
				console.log(result);
				if(result[0]=="ok") {
					if(page_direct=="home") {
						CntrctHelper.routeHome();
					} else {
						document.location.href = page_direct+'r[]='+result[0]+'&r[]='+result[1];
					}
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
			*/
			/*
			*/
			AssistHelper.closeProcessing();
		}	
	},
	routeHome : function() {
		parent.header.$("#backHome").trigger("click");
	},
	validateForm : function($form,supress,$onScreen) {
		suppress = (typeof suppress === "undefined") ? false : suppress;
		$onScreen = (typeof $onScreen === "undefined") ? $form : $onScreen;	
	/**
	 * 1. loop through $form input fields and check for any with attr("req")=="required" where no value has been input
	 * 2. if previous loop found missing required field(s)
	 * 		2.a display error message listing missing fields
	 * 		2.b reject form
	 * 	 else
	 *		serialize $form 		
	 * 		send to inc_controller with action=page_action
	 *  	if ajax response[0] = "ok"
	 * 			redirect to page_direct+r[]=result[0]&r[]=result[1]
	 * 		else
	 * 			display error message
	 */
		//console.log('Am validating, ok?');
		//console.log($form);
		$formchild = $form.find('button.am_required1:visible, input.am_required1:visible, select.am_required1:visible, textarea.am_required1:visible');
		//console.log('form children:'+$formchild.length);
		var fields = [];
		var result = [];
		var serial = [];
		var err_list = "<h2 class=idelete>Error</h2><p>The form you've filled in is incomplete.</p><p>The following fields are required but have not been captured/selected:</p><ul>";
		var num_arr = ['0','1','2','3','4','5','6','7','8','9','.','-'];
		$formchild.each(function(){
			//console.log($(this).prop("id"));
			if(!($(this).prop('id')=="btn_save")) {
				var simple_id = $(this).prop("id");
				$onScreen.find('#'+simple_id).removeClass("required");
				//$(this).removeClass("required");
				var val = $(this).val(); 
				var type = $(this).prop("type");
				var req = $(this).attr("req");
			//console.log("is validating"+val+" ."+type+" ."+req);
				var role = $(this).prop("role");
				var id_raw = $(this).parents("tr").children("th")[0].innerHTML;
				var id_arr = id_raw.split(":");
				var id = id_arr[0];
				if(req == 1){
					if(type != "submit" && type != "button"){
						if(type == "select-one"){
							if(val == "X" || val=="0" || val==0 || val==undefined || val==null){
								fields.push(id);
								//$(this).addClass("required");
								$onScreen.find('#'+simple_id).addClass("required");
							}
						}else{
							if(val.length == 0 || val == undefined || val == null){
								fields.push(id);
								//$(this).addClass("required");
								$onScreen.find('#'+simple_id).addClass("required");
							}else if($(this).hasClass('number-only')) {
								//alert("number!!");
								for ( var i = 0; i < val.length; i++ ) {
									if($.inArray(val.charAt(i),num_arr)<0) {
										fields.push(id);
										$(this).addClass("required");
									}
								}
							}
						}
					}
				} else if($(this).hasClass("number-only")) {
								//alert("number!!");
								for ( var i = 0; i < val.length; i++ ) {
									if($.inArray(val.charAt(i),num_arr)<0) {
										fields.push(id);
										$(this).addClass("required");
									}
								}
				}
				val = AssistString.decode(val);
				serial.push(val);
			}
		});
			//console.log(fields);
		for(var i in fields){
			err_list += "<li>" + fields[i] + "</li>";
		}
		err_list += "</ul><p>Please complete the required fields and try again.</p><br>";
		if(fields.length != 0){
			if(suppress!==true){
				$("#div_error").html(err_list);
				$("#div_error").dialog({
					modal: true,
					resizable: false,
					buttons: {
						"Okay":function(){
							$(this).dialog("close");
						}
					}
				});
				AssistHelper.hideDialogTitlebar("id","div_error");
			}
			return false;
		} else {
			return true;
		}
	}
	
};