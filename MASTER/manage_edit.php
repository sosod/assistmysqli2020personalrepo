<?php

$_REQUEST['object_type'] = "CONTRACT";
$page_section = "MANAGE";
$page_action = "EDIT";
$child_object_type = "CONTRACT";
$button_label = "|".$page_action."|";
$edit_con = true;
include("common/generic_list_page.php");

?>
<div style='display:none' id='confirm_restore'>
	<h3>Restore</h3>
	<p>Are you sure you would like to restore <?php echo $myObject->getObjectName("Customer"); ?> <span id='cust_number'></span>?<br>
        It will become available in the rest of the module.</p>
</div>

<script type="text/javascript">
	$(function(){
		$('.restore_btn').click(function(){
			var ref = $(this).attr('ref');
			//alert(ref);
			$('#cust_number').text(ref);
			$('#confirm_restore').dialog({
				modal:true,
				width:300,
				buttons:{
					"Restore":function(){
						AssistHelper.processing();
						var result = AssistHelper.doAjax("inc_controller.php?action=Contract.restore&object_id="+ref);
						$(this).dialog('close');
						// console.log("THIS IS WHERE THE RESULT GOES");
						// console.log(result);
						if(result[0]=='ok'){
                            document.location.href='manage_edit.php?r[]='+result[0]+'&r[]='+result[1];
                        }else{
                            AssistHelper.finishedProcessing(result[0],result[1]);
                        }
					},
					"Cancel":function(){
						$(this).dialog('close');
					}
				}
			});
		});
		//AssistHelper.hideDialogTitlebar('id','confirm_restore');
	});
</script>