<?php
include("inc_header.php");

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
$object_id = $_REQUEST['object_id'];
$logObject = new MASTER_LOG(strtolower($object_type));
$audit_log = $logObject->getObject(array('object_id'=>$object_id));
//echo "HI";
//ASSIST_HELPER::arrPrint($audit_log);
$audit_log = $audit_log[0];
$child_redirect = "manage_view.php?";

?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,false,array(),false,true); ?></td>
		<td width=5%>&nbsp;</td>
		<td width=48%><h2>Activity Log</h2><?php 
			echo $audit_log;
		?></td>
	</tr>
</table>
<?php echo $helper->getGoBack("manage_view.php","Go Back"); ?>
<script type=text/javascript>
var url = "<?php echo $child_redirect; ?>";
$(function() {
	<?php 
	echo $js; 
	?>
	$("#view_all").val("View Complete "+window.contract_object_name);
	$("input:button.btn_view").click(function() {
		tableButtonClick($(this));
	});
	$("#view_all").click(function(){
		AssistHelper.processing();
		if(confirm("Please be patient while the page loads.  It can take some time depending on the amount of information.")==true) {
			document.location.href = "manage_view_all.php?object_id=<?php echo $object_id; ?>";
		} else {
			AssistHelper.closeProcessing();
		}
	});
});
function tableButtonClick($me) {
	var i = $me.attr("ref");
	url = url+i;
	document.location.href = url;
}
</script>
<?php
//markTime("end");
?>