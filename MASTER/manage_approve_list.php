<?php
require_once("inc_header.php");

echo "<div id=assist_helper_display_result>"; ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); echo "</div>";

//$_REQUEST['object_type'] = "ACTION";
$page_section = "MANAGE";
$page_action = !isset($_REQUEST['page_action']) ? "APPROVE" : $_REQUEST['page_action'];

$object_type = $_REQUEST['object_type'];
$page_direct = "manage_approve_".strtolower($object_type)."_object.php?object_id=";
//$js.= " var page_direct = '".$page_direct."';
//";
$page_type = "LIST";
switch($object_type) {
	case "CONTRACT":
		$myObject = new MASTER_CONTRACT();
		break;
	case "DELIVERABLE":
		$myObject = new MASTER_DELIVERABLE();
		break;
	case "ACTION":
	default:
		$myObject = new MASTER_ACTION();
		break;
}
$options = array(
	'type'=>$page_type,
	'section'=>$page_section,
);
//	'page'=>$page_action,
//$helper->arrPrint($options);
$button_label = $helper->getActivityName("open");

?>
<div id="tabs">
	<ul>
		<li><a href="#outstanding">Outstanding</a></li>
		<li><a href="#done">Approved</a></li>
	</ul>
	<div id="outstanding">
		<?php
		$options['page'] = "APPROVE_".$object_type;
		$options['trigger'] = ($page_action=="APPROVE");
			$js.=$displayObject->drawListTable($myObject->getObject($options),array('pager'=>"outstanding",'value'=>$button_label,'class'=>"btn_outstanding"),$object_type,$options); ?>
	</div>
	<div id="done">
		<?php 
		$options['page'] = "APPROVED_".$object_type;
		$options2 = $options;
		$options2['trigger'] = ($page_action!="APPROVE");
		$js.=$displayObject->drawListTable($myObject->getObject($options2),array('pager'=>"done",'value'=>$button_label,'class'=>"btn_done"),$object_type,$options); 
		?>
	</div>
</div>
<script type="text/javascript" >
var page_direct = '<?php echo $page_direct; ?>';
$(function() {
	<?php echo $js; ?>
	$("#tabs").tabs({
		active: <?php echo ($page_action=="APPROVE" ? "0" : "1"); ?>,
		activate: function(event,ui) {
			$("#assist_helper_display_result").hide();
			AssistHelper.processing();
			var me = ui.newPanel.selector; //AssistString.substr(ui.newPanel.selector,1);
			//alert(me+" => "+$(me+"_list_view tr").length);
			if($(me+"_list_view tr").length<=2) { //alert("less than 2");
				//$(me).paging('_activatePagingObject',$(me+" button.firstbtn"));
				$(""+me+" button.firstbtn").trigger("click");
			}
			AssistHelper.closeProcessing();
		}
	});
	$("input:button.btn_outstanding, input:button.btn_done").click(function() { 
		//alert("click"); 
		tableButtonClick($(this)); 
	});
});
function tableButtonClick($me) {
	//console.log($me.hasClass("btn_done"));
	document.location.href = page_direct+$me.attr("ref")+"&page_action="+($me.hasClass("btn_done") ? "APPROVED" : "APPROVE");
}
</script>
