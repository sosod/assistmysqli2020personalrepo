<?php

if(!isset($_REQUEST['tab'])) {
	$_REQUEST['tab'] = "dashboard";
}

include("inc_header.php");
unset($displayObject);
$displayObject = new MASTER_DISPLAY_FINANCE();

$page_redirect_path = "manage_finance_object.php?object_id=".$_REQUEST['object_id'];

$object_id = $_REQUEST['object_id'];

//$helper->arrPrint($_REQUEST);

$tabs = array(
	0=>"dashboard",
	1=>"budget",
	2=>"adjustment",
	3=>"income",
	4=>"expense",
	5=>"retention",
	8=>"budget_1",
	7=>"budget_2",
	6=>"budget_3",
);

$tab_keys = array_flip($tabs);



?>

<div id="tabs">
	<ul>
		<li><a href="#dashboard">Dashboard</a></li>
		<li><a href="#budget"><?php echo $helper->getObjectName("budget"); ?></a></li>
		<li><a href="#adjustment"><?php echo $helper->getObjectName("adjustment"); ?></a></li>
		<li><a href="#income"><?php echo $helper->getObjectName("income"); ?></a></li>
		<li><a href="#expense"><?php echo $helper->getObjectName("expense"); ?></a></li>
		<li><a href="#retention"><?php echo $helper->getObjectName("retention"); ?></a></li>
		<?php // READ SUPPLIER NAMES IN REVERSE ORDER TO SOLVE FLOAT RIGHT SORTING ISSUE!!!!! ?>
		<li class=supplier-tab><a href="#budget_3"><?php echo "Supplier 3*"; ?></a></li>
		<li class=supplier-tab><a href="#budget_2"><?php echo "Supplier 2*" ?></a></li>
		<li class=supplier-tab><a href="#budget_1"><?php echo "Supplier 1*" ?></a></li>
	</ul>

<?php 
foreach($tabs as $t) {
	echo "<div id=".$t.">";
	if($_REQUEST['tab']==$t) {
		echo "<div id=div_placeholder >";
		if($t=="dashboard") {
			include("finance_object_dashboard.php");
		} elseif(strpos($t, "_")>0) {
			include("finance_object_supplier.php");
		} else {
			include("finance_object.php");
		}
		echo "</div>";
	} else {
		echo "
		<div class=center>
			<p>Loading... please wait</p>
		</div>
		";
	}
	echo "</div>";
			//<p><img src=/pics/ajax_loader_v2.gif /></p>
}
?>
</div>


<script type=text/javascript>
$(function() {
	
	<?php echo $js; ?>
	
	$("#tabs").tabs({
		active: <?php echo $tab_keys[$_REQUEST['tab']]; ?>,
		activate: function( event, ui ) {
			AssistHelper.processing();
			var tab = AssistString.substr(ui.newPanel.selector,1);
			document.location.href = "manage_finance_object.php?object_id=<?php echo $_REQUEST['object_id']; ?>&tab="+tab;
		}
	}).css("margin","0px");
	$(".supplier-tab").css("float","right");



	var offset = $("#tabs").offset();
	$(window).resize(function() {
		var screen = AssistHelper.getWindowSize();
		var space_available_for_tabs = screen.height - offset.top;
		var tab_height = space_available_for_tabs<500 ? 500 : (space_available_for_tabs-10);
		var acc_height = tab_height - 60;
		$("#tabs").css("height",+"px");
		$("#div_placeholder").css({"height":acc_height+"px"});
		<?php
		if($_REQUEST['tab']=="dashboard" || strpos($_REQUEST['tab'],"_")>0) {
			echo "$('#div_placeholder').css({'overflow':'auto'});";
		} else {
			echo "$('#accordion').accordion('refresh');";
		}
		?>
	});
	$(window).trigger("resize");
});
</script>