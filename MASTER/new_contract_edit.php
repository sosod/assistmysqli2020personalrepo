<?php
require_once("inc_header.php");
$_REQUEST['object_type'] = "CONTRACT";
$page_section = "MANAGE";
$page_action = "EDIT_NEW";
$child_object_type = "CONTRACT";
$button_label = "Complete";
$only_new = true;
$edit_con = true;
if(isset($_REQUEST['object_id'])){
	$object_id = $_REQUEST['object_id'];	
	$object_type = "CONTRACT";
	$section = "ADMIN";
	$page_redirect_path = "manage_view.php?";
	$page_action = "Edit";
	
	ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
	
	$child_object_type = "CONTRACT";
	$child_object_id = $object_id;
	$childObject = new MASTER_CONTRACT($object_id);
	echo "
	<table class=tbl-container>
		<tr>
			<td width=60%><h2>". $helper->getObjectName($object_type)." Details: <span id='master_heading'>".$helper->getObjectName($object_type)."</span> [". $childObject->getRefTag()."".$object_id."]</h2>
	";
			//echo"<form name='massive_form' enctype='multipart/form-data' language='jscript' method='post'>";
	$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
	
	//$data['display'].=$footer['echo'];
	echo $data['display'];
	$js.=$data['js'];
	//echo"</form>";
	
				//include("common/generic_object_form.php");
				//$js.= $helper->drawPageFooter($helper->getGoBack('admin_contract.php'),strtolower($object_type),"object_id=".$object_id."&log_type=".MASTER_LOG::EDIT);
	echo"		
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>";
	$footer = $displayObject->drawPageFooter($helper->getGoBack("manage_edit.php","Go Back"),"assist_".$helper->getCmpCode()."_customer_contract_log","cl_contract_id=".$object_id,""); 
	$js.=$footer;
	echo"<script type=text/javascript>
	$(function() {
		".$js."
		$('#customer_type').trigger('change');
	});
	
	</script>
	<style>
		.cert_number{
			margin-top:-20px;
			padding:5px;
			background-color:#062F9A;
			font-weight:bold;
			color:white;
			border-radius: 3px;		
		}
	</style>	";
}else{
	include("common/generic_list_page.php");
}
?>


<div style='display:none' id='confirm_restore'>
    <h3>Restore</h3>
    <p>Are you sure you would like to restore <?php echo $myObject->getObjectName("Customer"); ?> <span id='cust_number'></span>?<br>
        It will become available in the rest of the module.</p>
</div>

<script type="text/javascript">
    $(function(){
        $('.restore_btn').click(function(){
            var ref = $(this).attr('ref');
            //alert(ref);
            $('#cust_number').text(ref);
            $('#confirm_restore').dialog({
                modal:true,
                width:300,
                buttons:{
                    "Restore":function(){
                        AssistHelper.processing();
                        var result = AssistHelper.doAjax("inc_controller.php?action=Contract.restore&object_id="+ref);
                        $(this).dialog('close');
                        //console.log(result);
                        if(result[0]=='ok'){
                            document.location.href='new_contract_edit.php?r[]='+result[0]+'r[]='+result[1];
                        }else{
                            AssistHelper.finishedProcessing(result[0],result[1]);
                        }
                    },
                    "Cancel":function(){
                        $(this).dialog('close');
                    }
                }
            });
        AssistHelper.hideDialogTitlebar('id','confirm_restore');
        });
    });
</script>