<?php
include("inc_header.php");

$logObject = new CNTRCT_LOG("deliverable");
$object_id = $_REQUEST['deliverable_id'];
$deliverable_display = $displayObject->getDetailedView("DELIVERABLE", $object_id, true);
$js.=$deliverable_display['js']; 
?>
<table class=tbl-container>
	<tr>
		<td width=50%><?php echo $deliverable_display['display']; ?></td>
		<td width=50%><h2>Activity Log</h2><?php 
			$audit_log = $logObject->getObject(array('object_id'=>$object_id));
			echo $audit_log[0];
			//$js.=$helper->drawPageFooter($helper->getGoBack(),"contract",array('object_id'=>$contract_id)); 
		?></td>
	</tr>
	<tr>
		<td colspan=2>
			<h2><?php echo $helper->getActionObjectName(true); ?></h2>
			<?php
			$childObject = new CNTRCT_ACTION();
			$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id)); 
			?>
			<table width=100%>
				<tr>
					<?php
					foreach($child_objects['head'] as $fld=>$head) {
						echo "<th>".$head['name']."</th>";
					}
					?>
					<th></th>
				</tr>
				<?php
				foreach($child_objects['rows'] as $id => $obj) {
					echo "<tr>";
					foreach($obj as $fld=>$val) {
						echo "<td>".$val."</td>";
					}
					echo "<td class=center><input type=button value=View ref=".$id." class=btn_view /></td>";
					echo "</tr>";
				}
				?>
			</table>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php echo $js; ?>
	$("input:button.btn_view").click(function() {
		var i = $(this).attr("ref");
		alert("going to view deliverable "+i);
	});
});
</script>