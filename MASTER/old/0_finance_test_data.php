<?php

$income_heading = "!income!";
$expense_heading = "!expense!";
$var_heading = "!variance!";
$budget_heading = "!budget!";
$retention_heading = "!retention!";
$perc_heading = "!!% completion!";
$total_heading = "!total!";
$currency_options = array('symbol'=>"");

$suppliers = Array(
	'0' =>Array(
			'cs_project_value' => '456789.00',
			'cs_supplier_id' => '2',
			'contract_supplier' => 'Pick n Pay',
			'id' => '2',
			'budget' => '456789.00'
		),
	'1' =>Array(
			'cs_project_value' => '1006789.00',
			'cs_supplier_id' => '3',
			'contract_supplier' => 'Takealot.com',
			'id' => '3',
			'budget' => '1006789.00'
		),
	'2' =>Array(
			'cs_project_value' => '57391.00',
			'cs_supplier_id' => '4',
			'contract_supplier' => 'Makro',
			'id' => '4',
			'budget' => '57391.00',
		),
);




$child_objects_ordered = array(
	'0' => array(
			'72' => array(
					'del_id' => "72",
					'del_type' => "DEL",
					'del_parent_id' => "0",
					'name' => "main deliverable 1",
					'reftag' => "CD72",
				),
			'73' => array(
					'del_id' => "73",
					'del_type' => "DEL",
					'del_parent_id' => "0",
					'name' => "main deliverable 2",
					'reftag' => "CD73",
				),
			'74' => array(
					'del_id' => "74",
					'del_type' => "DEL",
					'del_parent_id' => "0",
					'name' => "main deliverable 3",
					'reftag' => "CD74",
				),
			'75' => array(
					'del_id' => "75",
					'del_type' => "DEL",
					'del_parent_id' => "0",
					'name' => "main deliverable 4",
					'reftag' => "CD75",
				),
			'78' => array(
					'del_id' => "78",
					'del_type' => "MAIN",
					'del_parent_id' => "0",
					'name' => "parent deliverable 1",
					'reftag' => "CD78",
				),
			'76' => array(
					'del_id' => "76",
					'del_type' => "DEL",
					'del_parent_id' => "0",
					'name' => "main deliverable 5",
					'reftag' => "CD76",
				),
			'77' => array(
					'del_id' => "77",
					'del_type' => "DEL",
					'del_parent_id' => "0",
					'name' => "main deliverable 6",
					'reftag' => "CD77",
				),
			'79' => array(
					'del_id' => "79",
					'del_type' => "MAIN",
					'del_parent_id' => "0",
					'name' => "parent deliverable 2",
					'reftag' => "CD79",
				),
		),
	'78' => array(
			'80' => array(
					'del_id' => "80",
					'del_type' => "SUB",
					'del_parent_id' => "78",
					'name' => "sub-deliverable 1",
					'reftag' => "CD80",
				),
			'81' => array(
					'del_id' => "81",
					'del_type' => "SUB",
					'del_parent_id' => "78",
					'name' => "sub-deliverable 1.2",
					'reftag' => "CD81",
				),
		),
	'79' => array(
			'82' => array(
					'del_id' => "82,",
					'del_type' => "SUB",
					'del_parent_id' => "79",
					'name' => "sub-deliverable 2.1",
					'reftag' => "CD82",
				)
		)
);


?>