<?php
$parent_object_id = $object_id;
$details = array();
$budObject = new CNTRCT_FINANCE_BUDGET();
$details['budget'] = $budObject->getSummaryData($parent_object_id);
$adjObject = new CNTRCT_FINANCE_ADJUSTMENT();
$details['adjustment'] = $adjObject->getSummaryData($parent_object_id);
$expObject = new CNTRCT_FINANCE_EXPENSE();
$details['expense'] = $expObject->getSummaryData($parent_object_id);
$incObject = new CNTRCT_FINANCE_INCOME();
$details['income'] = $incObject->getSummaryData($parent_object_id);
$retObject = new CNTRCT_FINANCE_RETENTION();
$details['retention'] = $retObject->getSummaryData($parent_object_id);

//ASSIST_HELPER::arrPrint($summary);

$summary = array(
	'original_budget'=>array(
		'total'=>$details['budget']['rows']['total'],
		'allocated'=>$details['budget']['rows']['deliverables'],
		'unallocated'=>$details['budget']['rows']['unallocated'],  
	),
	'adjustments'=>array(
		'total'=>$details['adjustment']['rows']['total'],
		'allocated'=>$details['adjustment']['rows']['deliverables'],
		'unallocated'=>$details['adjustment']['rows']['unallocated'],  
	),
	'total_budget'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
	'expenses'=>array(
		'total'=>$details['expense']['rows']['total'],
		'allocated'=>$details['expense']['rows']['deliverables'],
		'unallocated'=>$details['expense']['rows']['unallocated'],  
	),
	'unspent_budget'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
	'retention_held'=>array(
		'total'=>$details['expense']['rows']['retention'],
		'allocated'=>$details['expense']['rows']['retention_deliverables'],
		'unallocated'=>$details['expense']['rows']['retention_unallocated'],  
	),
	'retention_paid'=>array(
		'total'=>$details['retention']['rows']['total'],
		'allocated'=>$details['retention']['rows']['deliverables'],
		'unallocated'=>$details['retention']['rows']['unallocated'],  
	),
	'retention_outstanding'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
	'income_received'=>array(
		'total'=>$details['income']['rows']['total'],
		'allocated'=>$details['income']['rows']['deliverables'],
		'unallocated'=>$details['income']['rows']['unallocated'],  
	),
	'unfunded_budget'=>array(
		'total'=>0,
		'allocated'=>0,
		'unallocated'=>0,
	),
);

$columns = array("total","allocated","unallocated");

foreach($columns as $c) {
	$summary['total_budget'][$c] = $summary['original_budget'][$c] + $summary['adjustments'][$c];
	$summary['unspent_budget'][$c] = $summary['total_budget'][$c] - $summary['expenses'][$c];
	$summary['unfunded_budget'][$c] = $summary['total_budget'][$c] - $summary['income_received'][$c];
	
}

foreach($summary as $key => $s) {
	foreach($columns as $c) {
		$summary[$key][$c] = ASSIST_HELPER::format_currency($s[$c]);
	}
}


$delObject = new CNTRCT_DELIVERABLE();
$add_deliverables = $delObject->getOrderedObjects($parent_object_id);


?>
<table class=tbl-container>
	<tr>
		<td width=48%>
			<?php
			$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
			echo $contract['display'];
			$js.=$contract['js'];
			?>
		</td>
		<td width=4%>
			
		</td>
		<td width=48%>
			<h2>Finance Summary For Supplier 1</h2>
			<table id=tbl_finance>
				<tr>
					<th width=31% style='background-color: #FFFFFF; border-bottom: 1px solid #ababab;'></th>
					<th width=23%>Total</th>
					<th width=23%>Allocated to Deliverables</th>
					<th width=23%>Unallocated</th>
				</tr>
				<?php $field = "original_budget"; ?>
				<tr>
					<td class=th>Original Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "adjustments"; ?>
				<tr>
					<td class=th>+ Adjustments</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "total_budget"; ?>
				<tr class=total>
					<td class=th>= Total Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<tr class=divider>
					<td colspan=4></td>
				</tr>
				<?php $field = "total_budget"; ?>
				<tr>
					<td class=th>Total Budget</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "expenses"; ?>
				<tr>
					<td class=th>- Expenses Invoiced (incl. Retention)</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "unspent_budget"; ?>
				<tr class=total>
					<td class=th>= Budget Remaining / (Overspent)</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<tr class=divider>
					<td colspan=4></td>
				</tr>
				<?php $field = "retention_held"; ?>
				<tr>
					<td class=th>Retention Held</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "retention_paid"; ?>
				<tr>
					<td class=th>- Retention Paid</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
				<?php $field = "retention_outstanding"; ?>
				<tr class=total>
					<td class=th>= Retention Balance</td>
					<td class=right><?php echo $summary[$field]['total']; ?></td>
					<td class=right><?php echo $summary[$field]['allocated']; ?></td>
					<td class=right><?php echo $summary[$field]['unallocated']; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=3><div id=div_finance_deliverables style='margin: 0 auto;'>
					<?php
					echo "<h2>".$helper->getDeliverableObjectName()."</h2>";
					
					//$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list"));
					?>
					<table id=tbl_finance_deliverables class=finance-list>
						<tr>
							<th></th>
							<th><?php echo $helper->getObjectName("budget"); ?></th>
							<th><?php echo $helper->getObjectName("adjustment"); ?></th>
							<th>Total<br /><?php echo $helper->getObjectName("budget"); ?></th>
							<th></th>
							<th><?php echo $helper->getObjectName("expense"); ?><br />Invoiced</th>
							<th>Unspent/(Overspent)<br /><?php echo $helper->getObjectName("budget"); ?></th>
							<th></th>
							<th><?php echo $helper->getObjectName("retention"); ?><br />Held</th>
							<th><?php echo $helper->getObjectName("retention"); ?><br />Paid</th>
							<th><?php echo $helper->getObjectName("retention"); ?><br />Outstanding</th>
						</tr>
						<?php
						foreach($add_deliverables[0] as $i => $del) {
								echo "
								<tr>
									<td class=b>".$del['reftag'].": ".$del['name']."</td>
									<td>0.00</td>
									<td>0.00</td>
									<td class=b>0.00</td>
									<td style='background-color:#dedede;'></td>
									<td>0.00</td>
									<td class=b>0.00</td>
									<td style='background-color:#dedede;'></td>
									<td>0.00</td>
									<td>0.00</td>
									<td class=b>0.00</td>
								</tr>";
							if($del['del_type']!="DEL") {
								if(isset($add_deliverables[$i])) {
									foreach($add_deliverables[$i] as $ci => $cdel) {
										echo "
										<tr class=i style='background-color: #f5f5f5;'>
											<td>&nbsp;&nbsp;+&nbsp;".$cdel['reftag'].": ".$cdel['name']."</td>
											<td>0.00</td>
											<td>0.00</td>
											<td class=b>0.00</td>
											<td style='background-color:#dedede;'></td>
											<td>0.00</td>
											<td class=b>0.00</td>
											<td style='background-color:#dedede;'></td>
											<td>0.00</td>
											<td>0.00</td>
											<td class=b>0.00</td>
										</tr>";
									}
								}
							}
						}
						?>
					</table>
		</div>
		</td>
	</tr>
</table>
<style type="text/css">
	td.th {
		font-weight: bold;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
	tr.divider td {
		height: 10px;
		border-right: 1px solid #ffffff;
		border-left: 1px solid #ffffff;
	}
</style>
<script type="text/javascript">
	$(function() {
		var table_width = (AssistString.substr($("#tbl_finance_deliverables").css("width"),0,-2))*1;
		$("#div_finance_deliverables").css("width",(table_width+3)+"px");
	});
</script>