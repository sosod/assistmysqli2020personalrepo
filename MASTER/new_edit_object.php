<?php
include("inc_header.php");

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];


switch($object_type) {
	//case "CONTRACT":
		//$myObject = new MASTER_CONTRACT($object_id);
		
		//break;
	case "DELIVERABLE":
		$childObject = new MASTER_DELIVERABLE($object_id);		
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new MASTER_CONTRACT($parent_id);
		$parent_object_type = "CONTRACT";
		//$page_redirect_path = "new_deliverable_add.php?object_type=";
		break;
	case "ACTION":
		$childObject = new MASTER_ACTION($object_id);		
		$parent_id = $childObject->getParentID($object_id);
		$myObject = new MASTER_DELIVERABLE($parent_id);
		$parent_object_type = "DELIVERABLE";
		//$page_redirect_path = "new_action_add.php?";
		break;
}
$page_redirect_path = "new_".strtolower($object_type)."_add_object.php?object_type=".$parent_object_type."&object_id=".$parent_id."&";
$page_action = "Edit";
?>
<table class=tbl-container>
	<tr>
	<?php if($object_type!="CONTRACT") { ?>
		<td width=47%><?php $js.= $displayObject->drawDetailedView($parent_object_type, $parent_id, true); ?></td>
		<td width=5%>&nbsp;</td>
	<?php } ?>
		<td width=48%><h2><?php echo $helper->getObjectName($object_type); ?> Details</h2><?php 
			//include("common/generic_object_form.php");
							//getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path)
			$data = $displayObject->getObjectForm($object_type, $childObject, $object_id, $parent_object_type, $myObject, $parent_id, $page_action, $page_redirect_path);
echo $data['display'];
$js.=$data['js'];
						
			$js.= $displayObject->drawPageFooter($helper->getGoBack($page_redirect_path));
		?></td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	
	?>
});
</script>