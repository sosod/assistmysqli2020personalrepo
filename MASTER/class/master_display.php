<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
class MASTER_DISPLAY extends ASSIST_MODULE_DISPLAY {
    
    protected $activity_names;
	protected $default_attach_buttons = true;
	
    public function __construct() {
    	$me = new MASTER();
		$an = $me->getAllActivityNames();
		$this->activity_names = $me->getAllActivityNames();
		$this->cmpcode = $me->getCmpCode();
		unset($me);
    	
        parent::__construct($an);
    }
    
	public function disableAttachButtons() { $this->default_attach_buttons = false; }
	public function enableAttachButtons() { $this->default_attach_buttons = true; }
	
	
	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */
	
	
	public function getDataFieldNoJS($type,$val,$options=array()) {
		$d = $this->getDataField($type, $val,$options);
		return $d['display'];
	}
	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type,$val,$options=array()) {
		$d = $this->getDataField($type,$val,$options);
		if(is_array($d)) {
			echo $d['display'];
		} else {
			echo $d;
		}
	}
	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 	 * 
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type,$val,$options=array()) {
		//$myObject = new MASTER_CONTRACT();	
		$data = array('display'=>"",'js'=>"");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val); 
				break;;
			case "CURRENCY":
				if(strlen($val)==0 || is_null($val)) {  $val = 0;  }
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right']==true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val,$options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val,false);
				}
				break;
			case "BOOL":
				$data['display'] = $this->getBoolForDisplay($val,(isset($options['html']) ? $options['html'] : true));
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			case "STATUS":
				$data['raw']=$val;
				$val *= 1;
				if(($val & MASTER::INACTIVE) == MASTER::INACTIVE){
					$val = "Inactive";
				}else if(($val & MASTER::ACTIVE) == MASTER::ACTIVE){
					$val = "Active";
				}else{
					//$val = $myObject->getUnspecified();
					$val = "System Generated";
				}
				$data['display'] = $val;
				break;
			case "ATTACH":
				$can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
				$object_type = isset($options['object_type']) ? $options['object_type'] : "X";
				$object_id = isset($options['object_id']) ? $options['object_id'] : "0";
				$buttons = isset($options['buttons']) ? $options['buttons'] : true;
				$data['display'] = $this->getAttachForDisplay($val,$can_edit,$object_type,$object_id,$buttons);
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(isset($options['html']) && $options['html']==true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				} else {
					$val = $val;
				}
				$data = array('display'=>$val,'js'=>"");
				//$data = array('display'=>$type);
		}
		return $data;
	}
	
    
	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type,$options=array(),$val="") {
		//echo "dFF VAL:-".$val."- ARR: "; print_r($options);
		$ff = $this->createFormField($type,$options,$val);
		//if(is_array($ff['display'])) {
			//print_r($ff);
		//}
		echo $ff['display'];
		return $ff['js'];
	}
	/** 
	 * Returns string of selected form field
	 * 
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "STATUS":
				$data = $this->getDataField($type, $val);
				break;
			case "LABEL":
				$data=$this->getLabel($val,$options);
				break;
			case "SMLVC":
				$data=$this->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$this->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				$data=$this->getLimitedTextArea($val,$options);
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$this->getTextArea($val,$options,$rows,$cols);
				} else {
					$data=$this->getTextArea($val,$options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getSelect($val,$options,$items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val)>0) {
						$val2[] = $val;
					}
				} 
				$data=$this->getMultipleSelect($val2,$options,$items);
				break;
			case "DATE": //echo "date!";
				if(!is_array($options) || count($options)==0){
					//$extra['minDate'] = "0";
					$options['options'] = "";
				}else{
					$extra = $options['options'];
				}
				unset($options['options']);
				$data=$this->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$this->getColour($val,$options);
				break;
			case "RATING":
				$data=$this->getRating($val, $options);
				break;	
			case "CURRENCY": $size = 15;
			case "PERC": 
			case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
			case "NUM": $size = !isset($size) ? 0 : $size;
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} //ASSIST_HELPER::arrPrint($options);
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
					if(!$has_sym) {
						$data['display']="R&nbsp;".$data['display'];
					} elseif(strlen($symbol)>0) {
						if($symbol_postfix==true) {
							$data['display']=$data['display']."&nbsp;".$symbol;
						} else {
							$data['display']=$symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="&nbsp;%";
                }
                break;
			case "BOOL_BUTTON":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val,$options);
				break;
			case "ATTACH"://echo "attach: ".$val;
				//$data = array('display'=>$val,'js'=>"");
				$data = $this->getAttachmentInput($val,$options);
				break;
			case "CALC" :
				$options['extra'] = explode("|",$options['extra']);
				$data = $this->getCalculationForm($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /**
	 * (ECHO) Function to display the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$data = $this->getPageFooter($left,$log_table,$var,$log_id);
		echo $data['display'];
		return $data['js'];
    }
    /**
	 * Function to create the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
		if(strlen($log_table)>0){
			$js .= "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}
				
			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
    }
 	
	
	
	
	
	
	
	
	
	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */
	
	
	
	
	/*****
	 * returns the details table for the object it is fed.
	 * 
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 * 
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new MASTER_ACTION();
				break;
			case "CONTRACT":
				if($child_type=="ACTION") {
					$myObject = new MASTER_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new MASTER_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new MASTER_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id,false,false,false,array(),false,false);
	}
	/**
	 * Draw the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 * 
	 * @echo HTML
	 * @return JS
	 */
	public function drawDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons="X") {
		if($attachment_buttons=="X") { $attachment_buttons = $this->default_attach_buttons; }
		$me = $this->getDetailedView($object_type, $object_id,$include_parent_button,$sub_table,$view_all_button,$button,$compact_view,$attachment_buttons);
		echo $me['display'];
		return $me['js'];
	}
	/**
	 * Generate the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 * 
	 * @return HTML
	 * @return JS
	 */
	public function getDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons=true) {
		//echo $object_type." :: ".$object_id;
		$cntrct = new MASTER();
		$ext = new MASTER_EXTERNAL();
		$listObj = new MASTER_LIST();
		$js = "";
		//add code to get js from assist_module_display to trigger on attachment click for download
		$echo = "";
		if($attachment_buttons){
			$js.=$this->getAttachmentDownloadJS($object_type,$object_id);
		}
		$parent_buttons = "";
		switch($object_type) {
			case "CONTRACT": 
				$myObject = new MASTER_CONTRACT();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("FINANCE")."' class='float btn_parent' id=FINANCE />";
				} 
				break;
		}
		if($view_all_button===true) {
			$parent_buttons.="<input class='float btn_view_all' type=button id='view_all' value='View All' />";
		}
		if($include_parent_button || $view_all_button) {
				$js.= "
						$('input:button.btn_view_all').button();
						$('input:button.btn_parent').button().click(function() {
							var my_window = AssistHelper.getWindowSize();
							var w = (my_window.width*".($object_type!="CONTRACT" ? "0.5" :"0.9").").toFixed(0);
							var h = (my_window.height*0.9).toFixed(0);
							var i = $(this).prop('id');
							var dta = 'child_type=".$object_type."&object_type='+i+'&child_id='+".$object_id."
							var x = AssistHelper.doAjax('inc_controller.php?action=Display.getParentDetailedView',dta);
							$('<div />',{html:x.display,title:x.title}).dialog({
								width: w,
								height: h,
								modal: true
							}).find('table.th2 th').addClass('th2');
						});";
		} //echo "cv: ".$compact_view.":";
				$object = $myObject->getAObject($object_id);
				//ASSIST_HELPER::arrPrint($object);
				
				$subs = $myObject->getRawCertObject($object_id);
				if(is_array($subs['customer_c_certificate'])){
					if(count($subs['customer_c_certificate'])>0){
						foreach($subs['customer_c_certificate'] as $s=>$b){
							foreach($b as $key=>$val){
								foreach($object['head']['16']['sub'] as $thing){
									//ASSIST_HELPER::arrPrint($thing);
									if($thing['h_field'] == $key && $thing['h_type'] == "LIST"){
										//DO THE LIST THINGY HERE!	
										$listObject = new MASTER_LIST($thing['h_table']);
										$list_item = $listObject->getAListItemName($b['customer_c_type']);
										$subs['customer_c_certificate'][$s][$key]=$list_item;
									}
								}
							}
						}
					}
				}
				//Hide the Person/Organisation section appropriately
				if($subs['customer_type']==1){
					unset($object['head']['11']);
					foreach($subs as $a=>$b){
						if(stripos($a, 'customer_o')!== false){
							unset($subs[$a]);
						}
					}
					foreach($object['head'] as $c=>$d){
						if(stripos($c, 'customer_o')!== false){
							unset($object['head'][$c]);
						}
					}
				}else if($subs['customer_type']==2){
					unset($object['head']['12']);
					foreach($subs as $a=>$b){
						if(stripos($a, 'customer_p') !== false){
							unset($subs[$a]);
						}
					}
					foreach($object['head'] as $c=>$d){
						if(stripos($c, 'customer_p')!== false){
							unset($object['head'][$c]);
						}
					}
				}
			//ASSIST_HELPER::arrPrint($subs);
				
				
				
			$echo.="
				$parent_buttons
			<h2 class='".($sub_table !== false ? "sub_head":"")."'>".($object_type=="DELIVERABLE" && $sub_table ? "Sub-" : "").$myObject->getObjectName($object_type)." Details: ".$myObject->getObjectName($object_type)." ".$object_id."</h2>
					<table class='form ".($sub_table !== false ? "th2":"")."' width=100%>";
					//ASSIST_HELPER::arrPrint($object['head']);
					//ASSIST_HELPER::arrPrint($object['head']);
					foreach($object['head'] as $fld => $head) {
						//echo $head['name'];
						if(is_numeric($fld)){
							if($head['name']=='Documents'){
								//$mDoc = new MDOC();
								//$data = $mDoc->getModuleDocumentTable($myObject->getModRef(), $object_type, $object_id, "VIEW", $myObject->getObjectName($object_type),"");
								//$val = $data['html'];
								//$js .= $data['js'];
//$val = "activate me";
							}else{
								$val = "<table class=th2 width=100%>";
								$tmp = isset($head['sub'])?$head['sub']:array();	
						//ASSIST_HELPER::arrPrint($tmp);
								foreach($tmp as $sub_head){
									$field = $sub_head['h_field'];
									if($sub_head['h_type']=="LIST"){
			//echo $sub_head['h_table'];	
										$listObj->changeListType($sub_head['h_table']);
										$value = $subs[$field]>0?$listObj->getAListItemName($subs[$field]):$myObject->getUnspecified();
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$value."</td></tr>";
									}else if($sub_head['h_type']=="MASTER"){
										$hdlist = strtoupper($sub_head['h_table']);
										$listObject = new $hdlist();
										$value = $subs[$field]>0?$listObject->getAListItemName($subs[$field]):$myObject->getUnspecified();
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$value."</td></tr>";
									}else if($sub_head['h_type']=="BOOL"){
										$subs[$field] = isset($subs[$field])?$subs[$field]:0;
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$this->getBoolForDisplay($subs[$field])."</td></tr>";
									}else if($sub_head['h_section']=="CONTRACT_CERT"){
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".(strlen($subs[$field])>0?$subs[$field]:$myObject->getUnspecified())."</td></tr>";
									}else if($sub_head['h_type']=="ATTACH"){
										$subs[$field] = isset($subs[$field])?$subs[$field]:0;
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".$this->getAttachForDisplay($subs[$field],false,"CONTRACT",$object_id)."</td></tr>";
									}else{
										$val .="<tr><th width=40%>".$sub_head['h_client'].":</th><td>".(strlen($subs[$field])>0?$subs[$field]:$myObject->getUnspecified())."</td></tr>";
									}
								}
								$val .= "</table>";
							}
						}else if(isset($object['rows'][$fld])) {
							$val = $object['rows'][$fld];
							if(is_array($val)) {
								if(isset($val['display'])) {
									$js.=$val['js'];
									$val = $val['display'];
								} else {
									$v = "<table class=th2 width=100%>";
									foreach($val as $a) {
										foreach($a as $b => $c)
										$v.="<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
									}
									$v.="</table>";
									$val = $v;
								}
							}
						} else {
							$val = $fld;
						}
						$echo.= "
						<tr>
							<th width=40%>".$myObject->replaceObjectNames($head['name']).":</th>
							<td ".($val==$fld ? "class=idelete" : "").">".(strlen($val)>0?$val:$myObject->getUnspecified())."</td>
						</tr>";
					}
			//Get any linked items from ACTION modules
			$linkedItems = $ext->getLinksToTasks($object_id);
			if(count($linkedItems)>0){
				$echo.= "<tr>
							<th width=40%>Linked Items:</th>
								<td>
									<table class=\"tbl_container noborder\" style=\"width:650px;\">
									<tbody>
										<tr>
											<td style=\"border-color:white; width=\"100px\" class=\"\">
												<table class=\"tbl_audit_log\" style=\"width: 100%;\">
												<tbody>
													<tr>
														<th class=\"th_audit_log_date\" width=\"100px\">Date</th>
														<th class=\"th_audit_log_user\" width=\"100px\">User</th>
														<th class=\"th_audit_log_changes\">Linked Item</th>
														<th class=\"th_audit_log_button\" width=\"65px\">&nbsp;</th>
													</tr>";
				foreach($linkedItems as $link){
							$btn = "<input type=\"button\" class=\"linked_action_button\" ref=\"".$link['object_id']."\" module=\"".$link['module']."\" value=\"View\" />";
							$echo.= "					<tr class='linked_action'><td class=\"center\">".$link['object_deadline']."</td><td class=\"center\">".$link['object_adder']."</td><td>".$link['object_name']."</td><td class=\"center\">$btn</td></tr>";
				}
				$echo.= "						</table>
											</td>
										</tr>
									</table>
								</td>";
				//Secret iframe dialog to take you to the Task
				$echo .="<div hidden id='task-link-dialog'>
							<iframe height=450px width=650px src=''></iframe>
						</div>";
				
				//View button popup
				$js.="
					$('.linked_action_button').each(function(){
						$(this).button().click(function(){
							//alert($(this).attr('ref'));
							$('#task-link-dialog iframe').prop('src','../ACTION/view_external.php?id='+$(this).attr('ref')+'&modloc='+$(this).attr('module'));
							$('#task-link-dialog').dialog({
								width:700,
								height:500
							});
						});
					});
				";
			}
			//$echo .= ASSIST_HELPER::arrPrint($ext->getLinksToTasks($object_id));
			
			//ASSIST_HELPER::arrPrint($button);
			if($button !== false && is_array($button) && count($button)>0){
				$echo.="
						<tr>
							<th width=40%></th>
							<td><input type=button value='".$button['value']."' class='".$button['class']."' ref=".$object_id." /></td>
						</tr>";
			}		
			$echo.="</table>";
			
			
		return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type).($object_type!="FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
	}
	
		
















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */



	
	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}
	public function getPaging($i,$options,$button,$object_type="",$object_options=array()) {
		/**************
		$options = array(
			'totalrows'=> mysql_num_rows(),
			'totalpages'=> totalrows / pagelimit,
			'currentpage'=> start / pagelimit,
			'first'=> start==0 ? false : 0,
			'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
			'prev'=> start==0 ? false : (start - pagelimit),
			'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
			'pagelimit'=>pagelimit,
		);
		 **********************************/
		$data = array('display'=>"",'js'=>"");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier'><span id='$i'>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
			for($p=1;$p<=$options['totalpages'];$p++) {
				$data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
			}
		$data['display'].="<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
						<!-- <span class=float>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span> -->
					</span></td>
				</tr>
			</table>";
			$op = "options[set]=true";
			foreach($object_options as $key => $v) {
				$op.="&options[$key]=$v";
			} 
		$data['js'] = "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';
		
		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i." button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
		return $data;
	}
	
	
	/*****
	 * List View
	 */
	public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false) {
		//ASSIST_HELPER::arrPrint($child_objects['rows']);
		$me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table);
		echo $me['display'];
		return $me['js'];
	}
	public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false) {
		//ASSIST_HELPER::arrPrint($child_objects); 
		$data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
		//$items_total = $contractObject->getPageLimit();
		//$these_items = array_chunk($child_objects['rows'], $items_total);
		$page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		$display_paging = true;
		if($button===false) {
			$display_paging = false;
		} elseif(isset($button['pager']) && $button['pager']===false) {
			$display_paging = false;
		}
		if($display_paging){
			$paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options);
			$data['display'].=$paging['display'];
			$data['js'].=$paging['js'];
		}
		unset($button['pager']);
		$data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' width=100%>
				<tr id='head_row'>
					";
					foreach($child_objects['head'] as $fld=>$head) {
						$data['display'].="<th>".$head['name']."</th>";
					}
					
				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
			$rows = $this->getListTableRows($child_objects, $button);
			$data['display'].=$rows['display']."</table>";
			$data['display'].="</td></tr></table>";
			//$data['display'].=ASSIST_HELPER::arrPrint($child_objects['rows']);
			$data['js'].=$rows['js'];
		return $data;
	}
	public function getListTableRows($child_objects,$button) {
		$data = array('display'=>"",'js'=>"");
		//ASSIST_HELPER::arrPrint($child_objects['rows']);
		if(count($child_objects['rows'])==0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				//$data['display'].="<tr>";
				$is_inactive = isset($obj['customer_user_status']['raw']) && (($obj['customer_user_status']['raw'] & MASTER::INACTIVE)==MASTER::INACTIVE);
				$data['display'].=$is_inactive?"<tr class='inact'>":"<tr>";
				foreach($obj as $fld=>$val) {
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'].="<td>".$val['display']."</td>";
							$data['js'].=isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'].="<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'].="<td>".$val."</td>";
					}
				}
					if($is_inactive){
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='Restore' ref=".$id." class='restore_btn' /></td>" : "");
					}else{
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class='".$button['class']."' /></td>" : "");
					}
				$data['display'].="</tr>";
			}
		}
		//$data['js'].="$('.inactive_tr');";
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path
	 * @param $display_form = true (include form tag) 
	 * @param $tbl_id = 0 (table id to differentiate between multiple forms on a single page) 
	 */
	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$display_form=true,$tbl_id=0) {
		//$form_object_type = "CUSTOMER";    
		$cert_counter = 0;
		$headingObject = new MASTER_HEADINGS();
		$mDoc = new MDOC();
		$is_activation_page = false;
		if(stripos($_SERVER['REQUEST_URI'], "new_contract_edit") !== false){
			$is_activation_page = true;
		}
		
		//Checking if the required lists have been filled
		//echo "You're in the right place";
		$list_names = $headingObject->getStandardListHeadings(); //ASSIST_HELPER::arrPrint($list_names);
		$empty_lists = array();
		foreach($list_names as $lkey=>$lval){
			$listObj = new MASTER_LIST($lkey);
			$list_data = $listObj->getActiveListItemsFormattedForSelect();
			if(count($list_data)<= 0 || $list_data = null || !is_array($list_data)){
				$empty_lists[] = $lval;
			}
		}
		
		//ASSIST_HELPER::arrPrint($list_names);
		//ASSIST_HELPER::arrPrint($empty_lists);
		//If they havent, show only a big warning message
		if(count($empty_lists) > 0){
			$plural = count($empty_lists)>1?"s":"";
			$unplural = count($empty_lists)>1?"":"s";
			$echo = "<div style='width:400px; padding:20px; background-color:#eeeeee; border-radius:5px; margin:auto;'><h2>Attention</h2><p>This module cannot be used until all of the setup has been completed.<br>Only the module administrator or people with <em>Setup</em> access can create the lists which are required by this module.<br>The following list".$plural." need".$unplural." to be set up before the module can be used:</p>
			<ul>";
			foreach($empty_lists as $list){
				$echo .= "<li>".$list."</li>";
			}
			$echo .= "</ul>
			<p>Please contact your Assist Administrator for assistance.</p></div>";
			echo $echo;
			return;
		}
		
		
		$th_class = "";
		$tbl_class = "";
		
		$attachment_form = false;
		
		$data = array('display'=>"",'js'=>"");
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";
		$head_object_type = $form_object_type;
		$fld_prefix = $formObject->getTableField()."_";
		//echo $fld_prefix;
		$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type,"FORM","NEW",$fld_prefix));
		$headings['tabs'][0]="General";
		foreach($headings['sub'] as $index=>$item){
			foreach($item as $key=>$val){
				$field = $val['field'];
				$headings['sub'][$index][$field]=$val;
				unset($headings['sub'][$index][$key]);
			}
			
		}
		foreach($headings['rows'] as $key=>$val){
			if(is_numeric($key) && $key!=11){
				$headings['rows'][$key]['subs']=isset($headings['sub'][$key])?$headings['sub'][$key]:"";
				$headings['tabs'][$key]=$val['name'];
			}
		}
		unset($headings['sub']);
		//return array("display"=>ASSIST_HELPER::arrPrint($headings));
		
	//ASSIST_HELPER::arrPrint($headings);
//	ASSIST_HELPER::arrPrint($formObject);
		
		$pa = ucwords($form_object_type).".".$page_action;
		$pd = $page_redirect_path;
		
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE) || (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		$is_add_page = (strrpos(strtoupper($page_action),"ADD")!==FALSE);
		$is_update_page = FALSE;
		$is_copy_page = (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		
		$copy_protected_fields = array();
		$copy_protected_heading_types = array();
		
		//return array("display"=>ASSIST_HELPER::arrPrint($headings));
		
		if($is_edit_page) {
			$form_object = $formObject->getRawCertObject($form_object_id);
			$form_activity = "EDIT";
			$echo.="<input type='hidden' name='object_id' value=".$form_object_id." />";
			if($is_copy_page) {
				$copy_protected_fields = $formObject->getCopyProtectedFields();
				$copy_protected_heading_types = $formObject->getCopyProtectedHeadingTypes();
			}
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}
		//ASSIST_HELPER::arrPrint($form_object);

		$js.="
				var page_action = '".$pa."';
				var page_direct = '".$pd."';
				
		".$this->getAttachmentDownloadJS($form_object_type, $form_object_id,$form_activity);
		$echo.="
			<div id=div_error class=div_frm_error>
				
			</div>
			".($display_form ? "<!-- <form name=frm_object method=post language=jscript enctype=\"multipart/form-data\"> -->" : "");
			//."
				//<tablee class='form $tbl_class' width=100% id=tbl_object_form_".$tbl_id.">";
			$form_valid8 = true;
			$form_error = array();
		
		//Drawing div for Tabs
		$echo.="<div id='tab_div'><ul>";
		foreach($headings['tabs'] as $key=>$val){
			$echo.="<li><a href='#tabs-".$key."'>".$val."</a></li>";	
		}
		$echo.="</ul>";
		foreach($headings['tabs'] as $key=>$value){
			//General headings	
			if($key == '0' || $key == 0){
	//ASSIST_HELPER::arrPrint($headings['rows']);
				/*
				$docObject = $mDoc;
				$myObject = new MASTER();
				$echo.= ASSIST_HELPER::getFloatingDisplay(array("info","Please click the 'Save and Continue' button on each section as you complete it, and activate the ".$myObject->getObjectName($form_object_type)." by clicking the 'Activate' button on the final section. The ".$myObject->getObjectName($form_object_type)." will only be useable once this final step has been completed."));  
				//$echo.= ASSIST_HELPER::arrPrint(array("mdoc34",$docObject->getSpecialAccessForMdoc(34)));  
				$echo.= ASSIST_HELPER::arrPrint(array("object3",$docObject->getSpecialAccessForObject(3,true))); 
				$modHelp = new ASSIST_MODULE_HELPER();
				$people = $modHelp->getTKList(true,"MDOC");
				//$echo.= ASSIST_HELPER::arrPrint(array("userList",$users)); 
				$echo.='<table class=form width=400px>
						            <tr>
						                <th width=84%>User</th><th>&nbsp;</th>
						            </tr>
						            <tr id="extra_access_original">
						                <td>
						                	<select name="extra_access">';
					                			$echo.='<option value="X" selected>---   SELECT   ---</option>';
						                		foreach($people as $key=>$val){
						                			$echo.='<option value="'.$key.'">'.$val[0].'</option>';
						                		}
				$echo.='	               	</select>
										</td>
										<td><input id="delete_extra_access" type="button" class="extra_access_delete" value="Delete" style="color:red" /></td>
						            </tr>
						            <tr id="extra_access_insert_point">
						                <td>&nbsp;</td>
						                <td><input id="append_extra_access" type="button" value="Add New" style="color:green" /></td>
						            </tr>
						        </table>';
				$js.='
					window.extra_access_row = $("#extra_access_original").clone(true);
					$("#extra_access_original").remove();
					
					$("#append_extra_access").button().click(function(){
						$cln = extra_access_row.clone(true).insertBefore($("#extra_access_insert_point"));
						$cln.find(".extra_access_delete").button().on("click",deleteExtraAccess);
					});
					
					function deleteExtraAccess(){
						$(this).parents("tr").remove();						
					}
				';
				*/
				$echo.="<div id='tabs-".$key."'>";//<form id='new_".$key."' class='new_frm_object'>
				$echo.="<table class='tbl-container'><tr><td><form name='section_".$key."' enctype='multipart/form-data' language='jscript' method='post'><table class='form'>";
				foreach($headings['rows'] as $fld=>$head){
					if(!is_numeric($fld)){
						//$echo.="<li>".$fld."</li>";
						
						//JUNGLE IS MASSIVE//
						
					$val = "";
					$h_type = $head['type'];
					if($h_type!="HEADING" && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required'],'class'=>"am_required".$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST") {
							if($display_me) {
								$list_items = array();
								$listObject = new MASTER_LIST($head['list_table']); //echo "<h2>".$head['list_table']."<?h2>";
								//return array("display"=>$head['list_table']);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								//ASSIST_HELPER::arrPrint($list_items);
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new MASTER_USERACCESS();
									//$list_items = $listObject->getActiveUsersFormattedForSelect();
									$list_items = $listObject->getAllActiveUsersFormattedForSelect();
									break; 
								case "OWNER":
									$listObject = new MASTER_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									//$listObject = new MASTER_MASTER($head['list_table']);
									$hdlist = strtoupper($head['list_table']); //echo $head['list_table'];
									$listObject = new $hdlist();
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break; 
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$data['js'].=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}
						}
					} elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);

					} else {//if($fld=="contract_supplier") {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}//echo "<br />".$fld."::".$pval.":";
					} 
					if($display_me) {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."</td>
						</tr>";
					}
						
						//JUNGLE IS DONE BEING MASSIVE//
					}
				//$echo.="</table>";
				}
				$echo.="</table></form></td></tr></table></div>";
			}else{
			//Tabbed headings
				$echo.="<div id='tabs-".$key."'>";
				$echo.="<table class='tbl-container'><tr><td><form name='section_".$key."' enctype='multipart/form-data' language='jscript' method='post'><table class='form' id=tbl_object_form_".$key.">";	
				//$echo.="<form class='new_frm_object' id='new_".$key."'><table class='tbl-container'><tr><td><table class='form' id=tbl_object_form_".$key.">";	
	//ASSIST_HELPER::arrPrint($headings['rows'][$key]);
			
				/*
				//removing "Documents" from here
			
				foreach($headings['rows'][$key] as $fld=>$head){
					if($fld == "id" && $head == 16){
						$headings['rows'][$key]['subs'] = array();
					}
				}
				*/
				
				foreach($headings['rows'][$key]['subs'] as $fld=>$head){
					//$echo.="<h4>".$head['name']."</h4>";	
					//MASSIVE FORM PROCESSING STARTS HERE//
					
						
					$val = "";
					$h_type = $head['type'];
		//ASSIST_HELPER::arrPrint($head);
					if($h_type!="HEADING" && strtoupper($head['section'])!= 'CONTRACT_CERT' && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						//ASSIST_HELPER::arrPrint($form_object);
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required'],'class'=>"am_required".$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST") {
							if($display_me) {
								$list_items = array();
								$listObject = new MASTER_LIST($head['list_table']);
								//return array("display"=>$head['list_table']);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								//return array("display"=>$list_items);
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new MASTER_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break; 
								case "OWNER":
									$listObject = new MASTER_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									//$listObject = new MASTER_MASTER($head['list_table']);
									$hdlist = strtoupper($head['list_table']);
									$listObject = new $hdlist();
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break;
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							//if($fld == "customer_p_dob"){
								//$options['options'] = array("yearRange"=>"'".(date('Y')-50).':'.date('Y')."'");
							//}else{
								$options['options'] = array();
							//}
							$options['class'] = "";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$data['js'].=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}
						}
					} else if(strtoupper($head['section'])== 'CONTRACT_CERT'){
						//-----------------------------------------------------CERTIFICATE STUFF----------------------------------------//
			//$form_object = new MASTER_CONTRACT();
			//$form_object = $form_object->getAObject();
			//ASSIST_HELPER::arrPrint($form_object);
						if(isset($form_object['customer_c_certificate']) && !$is_add_page){
							if(is_array($form_object['customer_c_certificate'])){
								if(count($form_object['customer_c_certificate'])>0){
								//ASSIST_HELPER::arrPrint($form_object['customer_c_certificate']);
								foreach($form_object['customer_c_certificate'] as $index=>$item){
									if($item['customer_c_status']==1){
										$display_me = false;
										$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
										if($head['required']==1) { $options['title'] = "This is a required field."; }
										if($h_type=="LIST") {
											if($display_me) {
												$list_items = array();
												$listObject = new MASTER_LIST($head['list_table']);
												//return array("display"=>$head['list_table']);
												$list_items = $listObject->getActiveListItemsFormattedForSelect();
												//return array("display"=>$list_items);
												//ASSIST_HELPER::arrPrint($list_items);
												if(isset($list_items['list_num'])) {
													$list_parent_association = $list_items['list_num'];
													$list_items = $list_items['options'];
													$options['list_num'] = $list_parent_association;
												}
												$options['options'] = $list_items;
												if(count($list_items)==0 && $head['required']==1) {
													$form_valid8 = false;
													$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
													$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
												}
												if($is_edit_page || $is_update_page) {
													$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 ? $item[$fld] : "X";
												} else {
													$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
												}
												unset($listObject);
											}
										} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
											$list_items = array();
											switch($h_type) {
												case "USER":
													$listObject = new MASTER_USERACCESS();
													$list_items = $listObject->getActiveUsersFormattedForSelect();
													break; 
												case "OWNER":
													$listObject = new MASTER_CONTRACT_OWNER();
													$list_items = $listObject->getActiveOwnersFormattedForSelect();
													break;
												case "MASTER":
													//$listObject = new MASTER_MASTER($head['list_table']);
													$hdlist = strtoupper($head['list_table']);
													$listObject = new $hdlist();
													$list_items = $listObject->getActiveItemsFormattedForSelect();
													break;
												default:
													echo $h_type; 
													break;
											}
											$options['options'] = $list_items;
											$h_type = "LIST";
											if(count($list_items)==0 && $head['required']==1) {
												$form_valid8 = false;
												$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
												$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
											}
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && ($item[$fld]>0 || !is_numeric($item[$fld])) ? $item[$fld] : "X";
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
											}
										} elseif($h_type=="DATE") {
											$options['options'] = array();
											$options['class'] = "";
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 && strtotime($item[$fld])>0 ? date("d-M-Y",strtotime($item[$fld])) : "";
												if($is_update_page) {
													$options['options']['maxDate'] = "'+0D'";
												}
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
											}
										} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
											$options['extra'] = "processCurrency";
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 ? $item[$fld] : "";
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
											}
										} elseif($h_type=="BOOL"){
											$h_type = "BOOL_BUTTON";
											$options['yes'] = 1;
											$options['no'] = 0;
											$options['extra'] = "boolButtonClickExtra";
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) && strlen($item[$fld])>0 && $item[$fld]>0 ? $item[$fld] : "0";
											} else {
												$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
											}
										} elseif($h_type=="REF") {
											if($page_action=="Add") {
												$val = "System Generated";
											} else {
												$val = $formObject->getRefTag().$form_object_id;
											}
										} elseif($h_type=="ATTACH") {
											$attachment_form = true;
											$options['action'] = $pa;
											$options['page_direct'] = $pd;
											$options['can_edit'] = ($is_edit_page || $is_update_page);
											$options['object_type'] = $form_object_type;
											$options['object_id'] = $form_object_id;
											$options['page_activity'] = $form_activity;
											$val = isset($item[$fld]) ? $item[$fld] : ""; 
											if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
										} else {
											if($is_edit_page || $is_update_page) {
												$val = isset($item[$fld]) ? $item[$fld] : "";
											} else {
												$val = isset($head['default_value']) ? $head['default_value'] : "";
											}
										}
										if($display_me) {
											$display = $this->createFormField($h_type,$options,$val);
											$data['js'].=$display['js'];
											if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
												if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
													$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
												}
											}
										}
										if($display_me) {
										$echo.= $fld=='customer_c_status'?"
										<tr id=tr_".$fld."_".$index." sect='".$index."'".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
											<th class='".$th_class."' width=40%>Certificate ".$item['customer_c_certificate'].":</th>
											<td>Status: ".$item['customer_c_status']."<span class='right'><input type='button' class='edit_btn' value='Edit' ref='".$item['customer_c_certificate']."'></input></span>
											"//<td>".($fld=='customer_c_status'?"<span class='cert_number' style='float:right'>".$item['customer_c_certificate']."</span><input type='hidden' name='customer_c_certificate' value='".$item['customer_c_certificate']."' />":"")."
											."</td>
										</tr>":"";
										
										$echo.= "
											<tr class='edit_frm' field='$fld' ref=".$item['customer_c_certificate'].">
												<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
												<td>".$display['display']."</td>
											</tr>
											
										";
										/*
										if($cert_counter <=0){
											$echo .="<div id='edit_frm' style='background-color:green'>
												<table class='form'><tr id=tr_".$fld."_".$index." sect='".$index."'".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
													<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
														<td>".$display['display'].($fld=='customer_c_status'?"<span class='cert_number' style='float:right'>".$item['customer_c_certificate']."</span><input type='hidden' name='customer_c_certificate' value='".$item['customer_c_certificate']."' />":"")."
														</td>
													</tr>
												</table>
											</div>";
										}
										$cert_counter ++;
										*/
										}
									}
								}
								//Existing certificates: make the dialog
								//Hidden empty form, taken from normal add form				
								$display_me = true;
								$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
								if($head['required']==1) { $options['title'] = "This is a required field."; }
								if($h_type=="LIST") {
									if($display_me) {
										$list_items = array();
										$listObject = new MASTER_LIST($head['list_table']);
										//return array("display"=>$head['list_table']);
										$list_items = $listObject->getActiveListItemsFormattedForSelect();
										//return array("display"=>$list_items);
										//ASSIST_HELPER::arrPrint($list_items);
										//ASSIST_HELPER::arrPrint($head);
										if(isset($list_items['list_num'])) {
											$list_parent_association = $list_items['list_num'];
											$list_items = $list_items['options'];
											$options['list_num'] = $list_parent_association;
										}
										$options['options'] = $list_items;
										if(count($list_items)==0 && $head['required']==1) {
											$form_valid8 = false;
											$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
											$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
										}
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
										unset($listObject);
									}
								} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
									$list_items = array();
									switch($h_type) {
										case "USER":
											$listObject = new MASTER_USERACCESS();
											$list_items = $listObject->getActiveUsersFormattedForSelect();
											break; 
										case "OWNER":
											$listObject = new MASTER_CONTRACT_OWNER();
											$list_items = $listObject->getActiveOwnersFormattedForSelect();
											break;
										case "MASTER":
											//$listObject = new MASTER_MASTER($head['list_table']);
											$hdlist = strtoupper($head['list_table']);
											$listObject = new $hdlist();
											$list_items = $listObject->getActiveItemsFormattedForSelect();
											break;
										default:
											echo $h_type; 
											break;
									}
									$options['options'] = $list_items;
									$h_type = "LIST";
									if(count($list_items)==0 && $head['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								} elseif($h_type=="DATE") {
									$options['options'] = array();
									$options['class'] = "";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
									$options['extra'] = "processCurrency";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="BOOL"){
									$h_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
								} elseif($h_type=="REF") {
									if($page_action=="Add") {
										$val = "System Generated";
									}
								} elseif($h_type=="ATTACH") {
									$attachment_form = true;
									$options['action'] = $pa;
									$options['page_direct'] = $pd;
									$options['can_edit'] = ($is_edit_page || $is_update_page);
									$options['object_type'] = $form_object_type;
									$options['object_id'] = $form_object_id;
									$options['page_activity'] = $form_activity;
									$val = isset($item[$fld]) ? $item[$fld] : ""; 
									if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
								} else {
									$val = isset($head['default_value']) ? $head['default_value'] : "";
								}
								if($display_me) {
									$display = $this->createFormField($h_type,$options,$val);
									$data['js'].=$display['js'];
									if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
										if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
											$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
										}
									}
								}
								//ASSIST_HELPER::arrPrint($form_object['customer_c_certificate']);
								//ASSIST_HELPER::arrPrint($head);
								//$my_id = 
								
								 
						/*
						$echo.= "
							<tr class='edit_frm' field=$fld ref=".$form_object['customer_c_certificate'].">
								<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
								<td>".$display['display']."</td>
							</tr>
						";
						*/
								$echo .=($cert_counter <=0)?"	<table><tr>
												<th class='".$th_class."' width=40%>New Document</th>
												<td>&nbsp;<span class='right'><input type='button' class='edit_btn' value='Add'></input></span></td>
												</tr>
											</table>":"";
								$cert_counter ++;
							}
						//-----------------END if there are existing xertificates-------------------------------------/
						}else{
							//-----------------Add dialog / empty edit dialog----------------------//
							$docObject = $mDoc;
							$myObject = new MASTER();
							$echo.= ASSIST_HELPER::getFloatingDisplay(array("info","Please click the 'Save and Continue' button on each section as you complete it, and activate the ".$myObject->getObjectName($form_object_type)." by clicking the 'Activate' button on the final section. The ".$myObject->getObjectName($form_object_type)." will only be useable once this final step has been completed."));  
							//$echo.= ASSIST_HELPER::arrPrint(array("mdoc34",$docObject->getSpecialAccessForMdoc(34)));  
							//$echo.= ASSIST_HELPER::arrPrint(array("object3",$docObject->getSpecialAccessForObject($form_object_id,true)));
							$existingAccess = $docObject->getSpecialAccessForObject($form_object_id,true);
							//$echo.= ASSIST_HELPER::arrPrint($existingAccess); 
							$accessCount = count($existingAccess);
							if($accessCount > 0){
								$existingAccess = reset($existingAccess);
								$existingAccess = json_decode($existingAccess);
							}
							//$echo.= ASSIST_HELPER::arrPrint($existingAccess); 
							$modHelp = new ASSIST_MODULE_HELPER();
							$people = $modHelp->getTKList(true,"MDOC");
							//<span class="ui-state-info ui-corner-all" style="float:right;" id="extra_access_help"><strong>?</strong></span>
									                	//<div class="help-tip" style="background-color:#4285f4; padding:5px; margin:auto;">'.ASSIST_HELPER::drawStatus("info2").'
							$echo.='<table class="tbl_container noborder" style="width:650px;"><tbody><tr><td style="border-color:white;" class="">
										<table id="extra_user_access_table" class=form style="width: 645px;">
									            <tr>
									                <th width=84%>Extra User Access</th>
									                <th>
									                	<div class="help-tip" style="padding:5px; margin:auto;">
									                		<p>Choose people here to give them access to all of the documents below - even if they are not usually allowed to see this '.$myObject->getObjectName($form_object_type).'.</p>
									                	</div>
								                	</th>
									            </tr>
									            <tr id="extra_access_original">
									                <td>
									                	<select name="extra_access">';
								                			$echo.='<option value="X" selected>---   SELECT   ---</option>';
									                		foreach($people as $key=>$val){
									                			$echo.='<option value="'.$key.'">'.$val[0].'</option>';
									                		}
							$echo.='	               	</select>
													</td>
													<td><input id="delete_extra_access" type="button" class="extra_access_delete" value="Delete" style="color:red" /></td>
									            </tr>';
							if($accessCount>0){
								foreach($existingAccess as $usr){
									$d = time();
									$d *= rand(1, 1000);	
									$echo.='
													<tr id="extra_access_existing">
										                <td>
										                	<select id="extra_access_'.$d.'" name="extra_access_'.$d.'">
										                		<option value="X">---   SELECT   ---</option>';
																foreach($people as $key=>$val){
										                			$sel = $key == $usr ? "selected" : "";	
										                			$echo.='<option '.$sel.' value="'.$key.'">'.$val[0].'</option>';
										                		}
									$echo.='               	</select>
														</td>
														<td><input id="delete_extra_access" type="button" class="extra_access_delete_existing" value="Delete" style="color:red" /></td>
										            </tr>';
								}					
							}
							$echo.='
									            <tr id="extra_access_insert_point">
									                <td><div class="extra_help_hidden" style="display: none;"><em>Choose people here to give them access to all of the documents below - even if they are not usually allowed to see this '.$myObject->getObjectName($form_object_type).'.</em></div></td>
									                <td><input id="append_extra_access" type="button" value="Add New" style="color:green" /></td>
									            </tr>
									        </table>
								        </td></tr></tbody></table>';
							$js.='
								window.extra_access_row = $("#extra_access_original").clone(true);
								$("#extra_access_original").remove();
								$("#extra_user_access_table").find(".extra_access_delete_existing").button().on("click",deleteExistingExtraAccess);
								
								$("#append_extra_access").button().click(function(){
									var d = new Date();
									var n = d.getTime();
									n *= Math.floor(Math.random() * n);
									$cln = extra_access_row.clone(true).insertBefore($("#extra_access_insert_point"));
									$cln.find(".extra_access_delete").button().on("click",deleteExtraAccess);
									$cln.find("select").prop("name","extra_access_"+n).prop("id","extra_access_"+n);
								});
								
								function deleteExistingExtraAccess(){
									$(this).parents("tr #extra_access_existing").remove();						
									//console.log($(this).parents("tr #extra_access_existing"));						
								}
								
								function deleteExtraAccess(){
									$(this).parents("tr #extra_access_original").remove();						
									//console.log($(this).parents("tr #extra_access_original"));						
								}
								
								$(".help-tip").on("click",function(){
									$(".extra_help_hidden").toggle(
										"slow",
										function() {
									});
								});
							';
							include_once("../MDOC/class/mdoc.php");
							include_once("../MDOC/class/mdoc_headings.php");
							$myObject = new MASTER();
							$mDocObj = $mDoc;
                            //$masterDocObj = new MASTER_MDOC();
                            //$echo .= $mDocObj->getObjectTypeID();
                            //$obj_type = "";
							$formatted_object_type = $myObject->getObjectName($form_object_type);
                            //echo $this->cmpcode." ".$form_object_type." ".$form_object_id." ".$formatted_object_type;
                            $phppath = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
                            $pathsplit = explode("/",$phppath,2);
                            $path = $page_redirect_path == "manage_view.php?" ? "../../".$pathsplit[1]:$page_redirect_path;
							$docData = $mDocObj->getModuleDocumentTable($this->cmpcode,$form_object_type,$form_object_id,strtoupper($page_action),$formatted_object_type,$path);
							$echo .= "<br>";
							$echo .= $docData['html'];
							$echo .= "<br>";
							$js .= $docData['js'];
							/*
							//Add button
							$echo .=($cert_counter <=0)?"	<table class='form'><tr>
											<th class='".$th_class."' width=40%>New Document</th>
											<td>&nbsp;<span class='right'><input type='button' class='edit_btn' value='Add'></input></span></td>
											</tr>
										</table>":"";
							$cert_counter ++;
							*/
							//Hidden empty form, taken from normal add form				
								$display_me = true;
								$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
								if($head['required']==1) { $options['title'] = "This is a required field."; }
								if($h_type=="LIST") {
									if($display_me) {
										$list_items = array();
										$listObject = new MASTER_LIST($head['list_table']);
										//return array("display"=>$head['list_table']);
										$list_items = $listObject->getActiveListItemsFormattedForSelect();
										//return array("display"=>$list_items);
										//ASSIST_HELPER::arrPrint($list_items);
										//ASSIST_HELPER::arrPrint($head);
										if(isset($list_items['list_num'])) {
											$list_parent_association = $list_items['list_num'];
											$list_items = $list_items['options'];
											$options['list_num'] = $list_parent_association;
										}
										$options['options'] = $list_items;
										if(count($list_items)==0 && $head['required']==1) {
											$form_valid8 = false;
											$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
											$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
										}
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
										unset($listObject);
									}
								} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
									$list_items = array();
									switch($h_type) {
										case "USER":
											$listObject = new MASTER_USERACCESS();
											$list_items = $listObject->getActiveUsersFormattedForSelect();
											break; 
										case "OWNER":
											$listObject = new MASTER_CONTRACT_OWNER();
											$list_items = $listObject->getActiveOwnersFormattedForSelect();
											break;
										case "MASTER":
											//$listObject = new MASTER_MASTER($head['list_table']);
											$hdlist = strtoupper($head['list_table']);
											$listObject = new $hdlist();
											$list_items = $listObject->getActiveItemsFormattedForSelect();
											break;
										default:
											echo $h_type; 
											break;
									}
									$options['options'] = $list_items;
									$h_type = "LIST";
									if(count($list_items)==0 && $head['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								} elseif($h_type=="DATE") {
									$options['options'] = array();
									$options['class'] = "";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
									$options['extra'] = "processCurrency";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
								} elseif($h_type=="BOOL"){
									$h_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
								} elseif($h_type=="REF") {
									if($page_action=="Add") {
										$val = "System Generated";
									}
								} elseif($h_type=="ATTACH") {
									$attachment_form = true;
									$options['action'] = $pa;
									$options['page_direct'] = $pd;
									$options['can_edit'] = ($is_edit_page || $is_update_page);
									$options['object_type'] = $form_object_type;
									$options['object_id'] = $form_object_id;
									$options['page_activity'] = $form_activity;
									$val = isset($item[$fld]) ? $item[$fld] : ""; 
									if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
								} else {
									$val = isset($head['default_value']) ? $head['default_value'] : "";
								}
								if($display_me) {
									$display = $this->createFormField($h_type,$options,$val);
									$data['js'].=$display['js'];
									if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
										if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
											$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
										}
									}
								}
								if($display_me) {
								$echo.= "
								
									<tr class='edit_frm' field='$fld'>
										<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
										<td>".$display['display']."</td>
									</tr>
								";
								}
							}
						//----------------------------------------------END EDIT CERTIFICATE STUFF------------------------------------//
						}else if($is_add_page){
						//---------------------------------------------------Add CERTIFICATE STUFF------------------------------------//
						
						include_once("../MDOC/class/mdoc.php");
						include_once("../MDOC/class/mdoc_headings.php");
						$myObject = new MASTER();
						$mDocObj = $mDoc;
                        //$masterDocObj = new MASTER_MDOC();
                        //echo $masterDocObj->getObjectTypeID();
                        //$obj_type = "";
						$formatted_object_type = $myObject->getObjectName($form_object_type);
                        //echo $this->cmpcode." ".$form_object_type." ".$form_object_id." ".$formatted_object_type;
                        $phppath = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
                        $pathsplit = explode("/",$phppath,2);
                        $path = $page_redirect_path == "manage_view.php?" ? "../../".$pathsplit[1]:$page_redirect_path;
						$docData = $mDocObj->getModuleDocumentTable($this->cmpcode,$form_object_type,$form_object_id,strtoupper($page_action),$formatted_object_type,$path);
						$echo .= "<br>";
						$echo .= $docData['html'];
						$echo .= "<br>";
						$js .= $docData['js'];
						
						//ASSIST_HELPER::arrPrint($head);
							$display_me = true;
							$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
							if($head['required']==1) { $options['title'] = "This is a required field."; }
							if($h_type=="LIST") {
								if($display_me) {
									$list_items = array();
									$listObject = new MASTER_LIST($head['list_table']);
									//return array("display"=>$head['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									//return array("display"=>$list_items);
									//ASSIST_HELPER::arrPrint($list_items);
									//ASSIST_HELPER::arrPrint($head);
									if(isset($list_items['list_num'])) {
										$list_parent_association = $list_items['list_num'];
										$list_items = $list_items['options'];
										$options['list_num'] = $list_parent_association;
									}
									$options['options'] = $list_items;
									if(count($list_items)==0 && $head['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
									unset($listObject);
								}
							} elseif(in_array($h_type,array("MASTER","USER","OWNER"))) {
								$list_items = array();
								switch($h_type) {
									case "USER":
										$listObject = new MASTER_USERACCESS();
										$list_items = $listObject->getActiveUsersFormattedForSelect();
										break; 
									case "OWNER":
										$listObject = new MASTER_CONTRACT_OWNER();
										$list_items = $listObject->getActiveOwnersFormattedForSelect();
										break;
									case "MASTER":
										//$listObject = new MASTER_MASTER($head['list_table']);
										$hdlist = strtoupper($head['list_table']);
										$listObject = new $hdlist();
										$list_items = $listObject->getActiveItemsFormattedForSelect();
										break; 
									default:
										echo $h_type; 
										break;
								}
								$options['options'] = $list_items;
								$h_type = "LIST";
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							} elseif($h_type=="DATE") {
								$options['options'] = array();
								$options['class'] = "";
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
								$options['extra'] = "processCurrency";
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							} elseif($h_type=="BOOL"){
								$h_type = "BOOL_BUTTON";
								$options['yes'] = 1;
								$options['no'] = 0;
								$options['extra'] = "boolButtonClickExtra";
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
								//echo "<p>".$fld.":".$val;
							} elseif($h_type=="REF") {
								if($page_action=="Add") {
									$val = "System Generated";
								}
							} elseif($h_type=="ATTACH") {
								$attachment_form = true;
								$options['action'] = $pa;
								$options['page_direct'] = $pd;
								$options['can_edit'] = ($is_edit_page || $is_update_page);
								$options['object_type'] = $form_object_type;
								$options['object_id'] = $form_object_id;
								$options['page_activity'] = $form_activity;
								$val = isset($item[$fld]) ? $item[$fld] : ""; 
								if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
							if($display_me) {
								$display = $this->createFormField($h_type,$options,$val);
								$js.=$display['js'];
								if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
									if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
										$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
									}
								}
							}
							if($display_me) {
							$echo.= "
							<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
								<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
								<td>".$display['display']."
								</td>
							</tr>";
							}
						//-------------------------------------------------END Add CERTIFICATE STUFF----------------------------------//
						}else{
							$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : ":O";
							
							if($display_me) {
								$display = $this->createFormField($h_type,$options,$val);
								$data['js'].=$display['js'];
								if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
									if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
										$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
									}
								}
							}
						}
						
					} else if(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);

					} else {//if($fld=="contract_supplier") {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}//echo "<br />".$fld."::".$pval.":";
					} 
					if($display_me && strtoupper($head['section'])!== 'CONTRACT_CERT') {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
					
						
					
					//MASSIVE FORM PROCESSING ENDS HERE//
				}
				//$echo.="<p>Hello $val!</p></div>";
				$echo.="</table></form></td></tr></table></div>";
			}
		}
		$echo.="</div>";
		if($is_edit_page){
			$echo.="<div id='edit_frm_container' style='display:none'><form id='popup-frm'><input type='hidden' name='customer_c_parent' id='customer_c_parent' value='$form_object_id' /><table id='append_here'></table></form></div>";
		}
		
		//Writing the delete and deactivate confirmation popups
		$echo .= "<div id='deactivate_popup' style='display:none'><div style='padding:10px'><h2>Deactivate</h2><p>Are you sure you want to deactivate this item?<br>You can always reactivate it later.</p></div></div>";
		$echo .= "<div id='delete_popup' style='display:none'><div style='padding:10px'><h2>Delete</h2><p>Are you sure you want to delete this item?<br>You cannot get it back!</p></div></div>";
		
		//Writing script for tabs
		$data['js'].="	//Set the tab object for later reference
					window.tabObj = $(\"#tab_div\").tabs();
					
			";
		//$echo.=ASSIST_HELPER::arrPrint($headings);
		//return array("display"=>$echo,"js"=>$js);
		
			if(!$form_valid8) {
				$data['js'].="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			}
				/*
				if(!$is_copy_page) {
					$echo.="
					<tr id='saver'>
						<th></th>
						<td>
							<input type=button value=\"Save ".$formObject->getObjectName($form_object_type)."\" class=isubmit id=btn_save />
							<input id='very_hidden_id' type=hidden name=object_id value=".$form_object_id." />
							".(($form_object_type!="CONTRACT" && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />" : "")."
						</td>
					</tr>";
				}
			 */
		$data['js'].=($is_add_page)?"	
				//Preparing a blank row
					window.first_cert = true;
					window.newRow = '<tr class=\"remove_row\" style=\"border:none\"><td style=\"border:none\"></td><td style=\"border:none\">&nbsp;</td></tr>';
				//Counting new forms added
					window.cert_count = 0;
				//Doing the Array
					$('#tbl_object_form_16').find('input, select, textarea').each(function(){
						var newOne = $(this).prop('name') + '[]';	
						$(this).prop('name', newOne);
					});
				//'Add another' button
				":"";
				
		//$data['js'].=($is_edit_page)?"$('#tbl_object_form_16').append('<span id=\"another_tr\"><input type=\"button\" class=\"another_btn\" value=\"Add another\" /></span>');":"";
		if($is_edit_page){
		$data['js'].="
				var editing = false;
				$('tr.edit_frm').each(function(){
					$(this).hide();
				});
				$(document).on('click','.edit_btn',function(){
					$('#append_here').empty();
					window.clickedBtn = $(this).attr('ref');
					//alert('hit'+$(this).attr('ref'));
					if($(this).attr('ref')!='undefined' && $(this).attr('ref')!= undefined){
						//alert('existing');
						editing = $(this).attr('ref');
					}else{
						editing = false;
					}
					//alert(editing);
					$('#edit_frm_container').dialog({
						modal:true,
						width:500,
						buttons:{
							'Save':function(){
								AssistHelper.processing();
								//CntrctHelper.validateForm($('#popup-frm'));
								//alert(dta);
								if(editing !== false){
									var act = 'CERT_Edit';
									$('#popup-frm').append('<input type=hidden name=customer_c_certificate value='+clickedBtn+' />');
								}else{
									var act = 'CERT_Add';
								}
								var dta = AssistForm.serialize($('#popup-frm'));
								//var result = 'inc_controller.php?action=CERTS.'+act+' | '+dta;
								var result = AssistHelper.doAjax('inc_controller.php?action=CERTS.'+act,dta);
								if(result[0]=='ok'){
									document.location.reload();
								}else{
									AssistHelper.finishedProcessing(result[0],result[1]);
								}
								//console.log(result);
								$(this).dialog('close');
							},
							'Delete':function(){
								var dta = 'customer_c_certificate='+clickedBtn;
								var result = AssistHelper.doAjax('inc_controller.php?action=CERTS.CERT_Del',dta);
								$(this).dialog('close');
								if(result[0]=='ok'){
									document.location.reload();
								}else{
									AssistHelper.finishedProcessing(result[0],result[1]);
								}
								
							},
							'Cancel':function(){
								$(this).dialog('close');
							}
						}
					});
					if(editing !== false){
						$('tr .edit_frm').each(function(){
							if($(this).attr('ref') == editing){
								$(this).clone().appendTo($('#append_here'));
							}
						});
					}else{
						//$('#append_here').empty();
						var counter = [];
						$('tr .edit_frm').each(function(){
							if($.inArray($(this).attr('field'),counter)==-1){
								counter.push($(this).attr('field'));
								$(this).clone().appendTo($('#append_here'));
							}
						});
						$('#append_here tbody tr').each(function(){
							$(this).show();
						});
						//Resetting form
						$(':input','#popup-frm')
						  .removeAttr('checked')
						  .removeAttr('selected')
						  .not(':button, :submit, :reset, :hidden, :radio, :checkbox')
						  .val('');
					}
					$('#append_here tbody tr').each(function(){
						$(this).show();
					});
				});
			";
		}
		$obj_id = $is_edit_page?$_REQUEST['object_id']:'0';
		if(!$is_edit_page){
			$is_edit_page = 0;
		}
		if(isset($_REQUEST['tab'])){
			$tab_choice = $_REQUEST['tab'];
		}else{
			$tab_choice = 0;
		}
		$delete_buttons = (!$is_activation_page && $is_edit_page)?"<input type=button id=\"deactivate_btn\" class=\"idelete\" value=\"Deactivate\" />":"";
		$delete_buttons .= $is_edit_page?"<input type=button id=\"delete_btn\" class=\"idelete\" value=\"Delete\" />\\":"\\";
if(!isset($myObject)) { $myObject = new MASTER(); }
$final_save_button_text = "Activate ".$myObject->getObjectName('CONTRACT')."";
if($is_edit_page && !$is_activation_page) { 
	$tabs_js = "
	$('[href=\"#tabs-16\"]').closest('li').hide();
$('div[role=\"tabpanel\"]').each(function(){
						if($(this).prop('id')=='tabs-16') {
							$(this).html('');
						} else if($(this).prop('id') == 'tabs-17'){
							var link = $(this).prop('id');
							$(this).append('<table class=\"tbl_container noborder\" style=\"border:none; width:100%\" ><tr><td class=\"noborder\" style=\"padding:5px; border:none;\">\
											<table class=\"form btn_row\">\
												<tr>\
													<th width=\"40%\">&nbsp;</th>\
														<td><span style=\"float:right; margin-bottom:5px\">\
															<input type=button class=\"next_page isubmit\" link='+link+' value=\"Save\" />$delete_buttons
														</span></td>\
													</tr>\
											</table>\
										</td></tr></table>');

						}else{
							var link = $(this).prop('id');
							$(this).append('<table class=\"tbl_container noborder\" style=\"border:none; width:100%\" ><tr><td class=\"noborder\" style=\"padding:5px; border:none;\">\
											<table class=\"form btn_row\">\
												<tr>\
													<th width=\"40%\">&nbsp;</th>\
														<td><span style=\"float:right; margin-bottom:5px\">\
															<input type=button class=\"next_page isubmit\" link='+link+' value=\"Save and continue\" />$delete_buttons
														</span></td>\
													</tr>\
											</table>\
										</td></tr></table>');
						}
					});
		";
} else {
	$tabs_js = "
	$('[href=\"#tabs-16\"]').text('Activate ".$myObject->getObjectName('CONTRACT')."');
$('div[role=\"tabpanel\"]').each(function(){
						if($(this).prop('id') == 'tabs-16'){

							$(this).html('').append('<table class=\"tbl_container noborder\" style=\"border:none; width:100%\" ><tr><td class=\"noborder\" style=\"padding:5px; border:none;\"><table class=\"form btn_row\"><tr><th width=\"40%\" style=\"background-color: #ffffff;\">&nbsp;</th><td style=\"border:1px solid #ffffff;\"><input type=button value=\"Activate ".$myObject->getObjectName('CONTRACT')."\" class=isubmit id=btn_save /><form id=finished_editing ><input id=\"very_hidden_finished\" type=hidden name=customer_user_status value=2 /><input id=\"very_hidden_id\" type=hidden name=object_id value=".$form_object_id." /></td></tr></table></td></tr></table>');

						}else{
							var link = $(this).prop('id');
							$(this).append('<table class=\"tbl_container noborder\" style=\"border:none; width:100%\" ><tr><td class=\"noborder\" style=\"padding:5px; border:none;\">\
											<table class=\"form btn_row\">\
												<tr>\
													<th width=\"40%\">&nbsp;</th>\
														<td><span style=\"float:right; margin-bottom:5px\">\
															<input type=button class=\"next_page isubmit\" link='+link+' value=\"Save and continue\" />$delete_buttons
														</span></td>\
													</tr>\
											</table>\
										</td></tr></table>');
						}
					});
		";
}

		$data['js'].="
				window.isValid = false;
				//Next tab + save button
//$('#tab_div').find('#tabs-16').hide();
//$('[href=\"#tabs-16\"]').closest('li').hide();
//$('[href=\"#tabs-16\"]').text('Activate ".$myObject->getObjectName('CONTRACT')."');
".$tabs_js."
										$('.btn_row').css('width',$('#tr_customer_id').css('width'));
					window.customerSeed = ".$obj_id.";
					
					window.tab = '$tab_choice';
					if(window.tab > 0){
						tabObj.tabs('option','active',window.tab);
					}
					//alert(window.tab);
					$('input.next_page').click(function(){
						var myLink = $(this).attr('link'); //console.log(myLink); console.log($(this));
						//var result = CntrctHelper.processObjectForm($"."form,page_action,page_direct,$"."originalForm);
						//var result = AssistForm.serialize($('#'+myLink).find('form'));
						//var result = CntrctHelper.processObjectForm($('#'+myLink).find('form'),'serialize');
						//alert(myLink + ' AND edit-page = '+$is_edit_page+'');
						if(myLink == 'tabs-0' && $is_edit_page !==1){
							//First page -> Add customer
							var result = CntrctHelper.processObjectForm($('#'+myLink).find('form'),page_action,page_direct);
							AssistHelper.finishedProcessing(result[0],result[1]);
							//console.log(result.object_id);
							if(typeof result.object_id !== undefined && result[0] == 'info'){
								window.customerSeed = result.object_id;
								if(window.isOrg){
									var myTab = 0;
								}else{
									var myTab = 1;
								}
								document.location.href = 'new_contract_edit.php?object_id='+result.object_id+'&tab='+myTab+'&is_org='+window.isOrg+'&r[]='+result[0]+'&r[]='+result[1];
							}else{AssistHelper.finishedProcessing(result[0],result[1]);}
							
							//alert(result.object_id);
						}else{
							//ADD THE FORM FOR FIRST TAB
							if(myLink == 'tabs-0'){
								$('#'+myLink).find('td').wrap('<form name=\"section-0\" id=\"section-0\"></form>');
								//alert('Done');
							}
							//alert('Type of seeders = '+ typeof window.customerSeed + ' AND seed = '+window.customerSeed+'');
							//Next page -> update customer
							//alert('Hit the not-first-tab!');
							if(window.customerSeed !== undefined && window.customerSeed > 0){
								$('#'+myLink).find('form').append('<input type=hidden name=object_id value='+window.customerSeed+' />');
								//console.log('Here it comes:');
								//console.log(AssistForm.serialize($('#'+myLink).find('form')));
								var result = CntrctHelper.processObjectForm($('#'+myLink).find('form'),page_action+'|ED',page_direct);
							}else{
								AssistHelper.processing();
								AssistHelper.finishedProcessing('info','Please complete the first section of the customer\'s details before continuing');
							}
						}
						//console.log(result);
						//To open the next tab	
						var nextTabIndex = $('li[role=\"tab\"][aria-controls=\"'+myLink+'\"][display!=\"none\"]').next().index();
						//Check if it's an organisation, and thus if next tab should be next next tab
						if(myLink=='tabs-17' && ".(!$is_activation_page ?"0":"1")." != 1) {
							if(isValid) {
								if(result[0]!='error') {
									document.location.href = 'manage_edit.php?r[]=ok&r[]='+result[1];
								} else {
									AssistHelper.processing();
									AssistHelper.finishedProcessing(result[0],result[1]);
								}
							}
						} else {
							if(myLink == 'tabs-0' && window.isOrg){
								nextTabIndex ++;
							}
							if(isValid){
								tabObj.tabs('option','active',nextTabIndex);
								window.tab = nextTabIndex;
							}
						}
					});
					
					$(document).on('click','#deactivate_btn',function(){
						$('#deactivate_popup').dialog({
							modal:true,
							width:500,
							buttons:{
								'Deactivate':function(){
									deactivation($obj_id);
									$(this).dialog('close');
								},
								'Cancel':function(){
									$(this).dialog('close');
								}
							}
						});
						AssistHelper.hideDialogTitlebar('id','deactivate_popup');
						AssistHelper.hideDialogCSS('id','deactivate_popup');	
					});
					
					function deactivation(id){
				//alert('DEACTIVATING');
						AssistHelper.processing();
						var result = AssistHelper.doAjax('inc_controller.php?object_id=".$obj_id."&action=CONTRACT.Deactivate','manage_view.php');
				//console.log(result);
						//alert(result);
						document.location.href = 'manage_view.php?r[]='+result[0]+'&r[]='+result[1];
					}
					
					$(document).on('click','#delete_btn',function(){
						$('#delete_popup').dialog({
							modal:true,
							width:500,
							buttons:{
								'Delete':function(){
									deletion($obj_id);
									$(this).dialog('close');
								},
								'Cancel':function(){
									$(this).dialog('close');
								}
							}
						});	
						AssistHelper.hideDialogTitlebar('id','delete_popup');
						AssistHelper.hideDialogCSS('id','delete_popup');	
					});
					
					function deletion(id){
						//alert('DELETING');
						AssistHelper.processing();
						var result = AssistHelper.doAjax('inc_controller.php?object_id=".$obj_id."&action=CONTRACT.Delete','manage_view.php');
						//console.log(result);
						//alert(result);
						//AssistHelper.finishedProcessing(result[0],result[1]);
						document.location.href = 'manage_view.php?r[]='+result[0]+'&r[]='+result[1];
					}
					
					/*
					$('#tbl_object_form_16').append('<tr id=\"another_tr\"><th></th><td><input type=\"button\" class=\"another_btn\" value=\"Add another\" /></td></tr>');
				//Copying certificate html for later
					window.certHtml = $('#tbl_object_form_16').html();
					certHtml = certHtml.slice(0,-18);
					certHtml += '<input type=\"button\" class=\"remove_btn\" value=\"Remove\" /></td></tr></tbody><tr class=\"remove_row\" style=\"border:none\"><td style=\"border:none\"></td><td style=\"border:none\">&nbsp;</td></tr>';
					$('#tbl_object_form_16 tbody').addClass('keepme');
				//Appending the copied html to the table on button click
					$(document).on('click','.another_btn',function(){
						//Checking whether or not to add first new row
							if(first_cert === true){
								$('#tbl_object_form_16').append(newRow);
							}
							first_cert = false;
						//Appending a remove button to new ones
							$('#tbl_object_form_16').append(certHtml);
						//Counting new certificate
							cert_count ++;
							//alert(cert_count);
					});
				//Listener for remove buttons
					$(document).on('click','.remove_btn',function(){
						cert_count --;
						//console.log($(this).parents('tbody'));
						$('#tbl_object_form_16 tbody:not(:first)').remove();
						if(cert_count <= 0){
							$('.remove_row').remove();
							first_cert = true;
						}else{
							$(this).parents('.remove_row:first').remove();
						}
					});
					*/
				";
				if(!$is_add_page){
					$data['js'].="	
				//Listener for remove buttons
					$(document).on('click','.remove_btn',function(){
						cert_count --;
						//console.log($(this).parents('tbody'));
						$(this).parents('tbody').remove();
						if(cert_count <= 0){
							$('.remove_row').remove();
							first_cert = true;
						}else{
							$(this).parents('.remove_row:first').remove();
						}
					});
				//Arranging the current certificates into sub-tables
				sectNums = {sects:[]};
				//var finalHtml = '<tr><td>';
				var finalHtml = '';
				
				$('#tbl_object_form_16 tr').each(function(){
					if($(this).prop('id')!=='saver'){
						var sect = $(this).attr('sect');
						if(!$.isArray(sectNums.sects[sect])){
							sectNums.sects[sect]=new Array();
						}
						sectNums.sects[sect].push('<tr>'+$(this).html()+'</tr>');
					}
				});
	//console.log(sectNums);
				
				for(i=0;i<sectNums.sects.length;i++){
					finalHtml +='<tr class=\"noborder\"><td class=\"noborder\"><table class=\"th2\">';
					var hString = sectNums.sects[i].join('');
					finalHtml += hString;
					finalHtml +='</table></td></tr>';
				}
				//finalHtml +='</td></tr>';
				
	//console.log(finalHtml);
				$('#tbl_object_form_16 tbody').html(finalHtml);
				$('#tbl_object_form_16').addClass('tbl-container noborder');
				$('#tbl_object_form_16 td').each(function(){
					$(this).css('border','none');
				});
				//$('#tbl_object_form_16').trigger('pagecreate');
				";
				}
				$data['js'].="
				window.firstRun = true;

				$('a[href=\"#tabs-14\"]').click(function(){
					if(firstRun !== false){
						$(\"#tbl_object_form_14 tbody tr\").each(function(){
							if($(this).prop(\"id\")!=='tr_customer_bil_crm'){
								//$(this).hide();
							}else{
								//$(this).show();
							}
						});
					}
					firstRun = false;
				});
				
				//SET WINDOW.TAB TO -1 WHEN YOU CLICK THE DOCUMENTS TAB
				$('a[href=\"#tabs-16\"]').click(function(){
					window.tab = -1;		
					//alert(window.tab);		
				});
					
				
				$(\"#customer_bil_crm_yes, #customer_bil_crm_no\").click(function(){
					if($(\"#customer_bil_crm\").val() == 1){
						//alert('SAME');
						//$(this).parents(\"table\").hide();
						$(this).parents(\"tr\").siblings().each(function(){
							if($(this).prop('id')!=='saver'){
								$(this).hide();
							}
						});
					}else{
						//alert('NOT');
						$(this).parents(\"tr\").siblings().each(function(){
							$(this).show();
						});
					}
				});
				$(\"#customer_bil_crm_yes\").trigger('click');
				//Customertype should change the tab visibility
				window.prsnContent = '';
				window.orgContent = '';
				window.isOrg = false;
				$('#customer_type').change(function(){
					if($(this).val()==1){
						//PERSON
							//tabObj.css('background-color','#eeffed');
						$('#tabs-0').find('tr[id*=\"_o_\"]').each(function(){
							//$(this).hide();	//removed on 29 Aug 2017 by JC to allow for capturing of both person & org names irrespective of customer type
						});
						$('#tabs-0').find('tr[id*=\"_p_\"]').each(function(){
							//$(this).show(); //removed on 29 Aug 2017 by JC to allow for capturing of both person & org names irrespective of customer type
						});
						$('#tabs-11').hide();
						$('#tabs-12').show();
						$('li[aria-controls=tabs-11]').hide();
						$('li[aria-controls=tabs-12]').show();
						$('#master_heading').text($('#customer_p_name').val()+' '+$('#customer_p_surname').val());
						window.isOrg = false;
					}else if($(this).val()==2){
						//ORGANISATION		
						$('#tabs-0').find('tr[id*=\"_p_\"]').each(function(){
							//$(this).hide(); //removed on 29 Aug 2017 by JC to allow for capturing of both person & org names irrespective of customer type
						});
						$('#tabs-0').find('tr[id*=\"_o_\"]').each(function(){
							//$(this).show(); //removed on 29 Aug 2017 by JC to allow for capturing of both person & org names irrespective of customer type
						});
							//tabObj.css('background-color','#edf9ff');
						$('#tabs-12').hide();
						$('#tabs-11').show();
						$('li[aria-controls=tabs-12]').hide();
						$('li[aria-controls=tabs-11]').show();
						$('#master_heading').text($('#customer_o_name').val());
						window.isOrg = true;
					}
						tabObj.tabs('refresh');
				});
				$('#customer_type').trigger('change');
				
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($form_object_type)."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"
				
				
				".$js."
				
				";
		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'].="
				
				
				$(\"form[class=new_frm_object] select\").each(function() {
					if($(this).children(\"option\").length<=1 && !$(this).hasClass(\"required\")) {
					}
					$(this).trigger('change');
				});
				$(\"#btn_save\").click(function() {
					/* NEW */
					AssistHelper.processing();/*
					$('#extra_user_access_table').clone().prop('id','extra_access_cloned').appendTo($('#finished_editing').hide());
					var selects = $('#extra_user_access_table').find('select');
				//	console.log(selects);
					$(selects).each(function(i) {
					    var select = this;
					    $('#extra_access_cloned').find('select').eq(i).val($(select).val());
					});*/
					var result = CntrctHelper.processObjectForm($('#finished_editing'),page_action,page_direct);
				//	console.log(result);
					AssistHelper.finishedProcessing(result[0],result[1]);
					return;
					/* NEW */
					$"."originalForm = $(\"form[name=massive_form]\");
					var f = 0;
					$"."originalForm.find('input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//console.log('EEK:'+f);
					if(f==0){
						$"."originalForm.find('input:hidden#has_attachments').val(0);
					}
					$"."form = $"."originalForm.clone(); 
					if($('#customer_type').val()==1){
						$"."form.find('div #tabs-11').find('input, select, textarea, button').remove();
					}else if($('#customer_type').val()==2){
						$"."form.find('div #tabs-12').find('input, select, textarea, button').remove();
						/*	
						for(i=0;i<$"."form.length;i++){
							if($"."form[i].id=='new_12'){
								$"."form.splice(i, 1);
							}
						}
						*/
					}
					if($(customer_bil_crm).val() == 1){
						$"."form.find('div #tabs-14').find('input, select, textarea, button').not('#customer_bil_crm').remove();
					}
				//console.log('Hello there--'+AssistForm.serialize($"."form));
				//debugger;
				//	$(\"input[name|='excluder']\").each(function(){
						//console.log($(this).prop('checked'));
				//	});
					//alert(AssistForm.serialize($"."form));";
		if($attachment_form) {
			$data['js'].="
					if(f>0) {
						/*
						$"."superForm = $"."form.find(':first');
						$"."form.find(':gt(0)').each(function(){
							$(this).find('input, select, textarea,button').each(function(){
								console.log('input  '+$(this).prop('name'));
								console.log($(this));
								$"."superForm.add($(this));
							});
						});
						console.log($"."superForm);
						CntrctHelper.processObjectFormWithAttachment($"."superForm,page_action,page_direct);
						//alert('processing with attach');
						*/
						console.log($"."form);
						CntrctHelper.processObjectFormWithAttachment($"."form,page_action,page_direct,$"."originalForm);
						//alert('processing with attach');
					} else {
						$('#has_attachments').val(0);
						CntrctHelper.processObjectForm($"."form,page_action,page_direct,$"."originalForm);
					}
					
					";
		} else {
			$data['js'].="
				//Checking each individual element
					//var errorArr = [];
				//Making the thing do the other thing (attach hidden input with Object ID)
					$('#new_0').append('<input type=hidden name=object_id value=".$form_object_id." />');
					/*
					$"."form.each(function(){
						if(!CntrctHelper.validateForm($(this),true)){
							//alert($(this).prop('id'));
							$('li[role=tab]').each(function(){
								if($(this).attr('aria-selected')){
									CntrctHelper.validateForm($(this),false);		
								}
							});
						}
					});
					var f = 0;
					$('#firstdoc input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//alert('HI : Frigate = '+f);
					if(f>0) {
						//alert('Doing attachment form');
						CntrctHelper.processObjectFormWithAttachment($"."form,page_action,page_direct);
					} else {
						$('#has_attachments').val(0);
						//alert(AssistForm.serialize($"."form));
						CntrctHelper.processObjectForm($"."form,page_action,page_direct);
					}
					*/
					$('#has_attachments').val(0);
					CntrctHelper.processObjectForm($"."form,page_action,page_direct,$"."originalForm);
					";
		}
		$data['js'].="
					console.log('end of function'+AssistForm.serialize($"."form));
					return false;
				});
				
				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}
				
				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");		
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();   
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
						}
					}
				}
				
				$data['js'].="
				$(\"#del_type\").change(function() {
					if($(this).val()==\"SUB\") {
						$(\".tr_del_type\").show();
					} else {
						$(\".tr_del_type\").hide();
					}
				});
				
				if($(\"#del_parent_id\").children(\"option\").length<=1) {
					$(\"#del_type option[value=SUB]\").prop(\"disabled\",true);
				}
				$(\"#del_type\").trigger(\"change\");
				
				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});
				
				//alert(AssistForm.serialize($(\"form[name=frm_deliverable]\")));
				
				$(\"form[name=frm_object] select\").each(function() {
					//alert($(this).children(\"option\").length);
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});
				
				$('form select').change(function(){
					//alert('form_change');
				});
			";
			
		//Disable all tabs but the first one on 'NEW' page
		if($is_add_page){
			$data['js'].="
				/*
				var tabCount =  $('#tab_div >ul >li').size();
				for(var x=1; x<5; x++){
					$(\"#tab_div\").tabs('option','disabled',[x]);
				}
				*/
				$(\"#tab_div\").tabs('option','disabled',[1,2,3,4,5,6]);
			";
		}
				
		$data['display'] = $echo;
		return $data;		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** 
	 * Returns filter for selecting parent objects according to section it is fed
	 * 
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
		$data['display']="
			<div id='filter'>";
				
		switch (strtoupper($section)) {
			case 'CONTRACT':
				$data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index=>$key){
					$data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js']="
					fyear = 0;
					
					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;
			
			case 'DELIVERABLE':
				$dsp = "contracts get";
				
				break;
			
			case 'ACTION':
				$dsp = "deliverables get";
				
				break;
			
			default:
				$dsp = "Invalid arguments supplied";
				break;
		}
		
		$data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";
		
        return $data;
    }
	

	/** 
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 * 
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from MASTER_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from MASTER_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
    public function getListView($section, $fyears=array(), $headings=array()){
    	$data = array('display'=>"",'js'=>"");
			switch(strtoupper($section)){
				case "CONTRACT":
					$filter = $this->getFilter($section, $fyears);
					//For the financial year filter
					$data['display'] = $filter['display'];
					//For the list view table
					$data['display'].= 
									'<table class=tbl-container><tr><td>
										<table class=list id=master_list>	
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';
					
												foreach($headings as $key=>$val){
													$string = "<th id='".$val['field']."' >".$val['name']."</th>";
													$data['display'].= $string;
												}
					$data['display'].=
													"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
					//For the javascript
					
					break;
				default:
					$data['display'] = "Lol, not for you";
					break;
			}
		
		return $data;
    }
}


?>
