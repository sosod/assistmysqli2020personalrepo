<?php
				$sub_head = $c_headings['sub'][$head['id']];
				$val = $r['contract_assess_type'];
				$sub_display = true;
					$td = array();
					foreach($sub_head as $fld=>$shead) {
						$v = $shead['field'];
						switch($v) {
							case "contract_assess_other_name":
								if($cObject->mustIAssessOther($val)) { 
									$v = $r[$v];
								} else {
									$sub_display = false;
								} 
								break;
							case "contract_assess_qual": 	$cas = !isset($cas) ? $cObject->mustIAssessQuality($val) : $cas;
							case "contract_assess_quan": 	$cas = !isset($cas) ? $cObject->mustIAssessQuantity($val) : $cas;
							case "contract_assess_other":	$cas = !isset($cas) ? $cObject->mustIAssessOther($val) : $cas; 
								//$v = $contract['contract_assess_type'];
								//if(($v & $cas) == $cas) {
								if($cas) {
									$v = ASSIST_HELPER::getDisplayIconAsDiv("ok")." Yes";
								} else {
									$v = ASSIST_HELPER::getDisplayIconAsDiv("0")." No";
								}
								break;
						}
						if($sub_display) {
							$td[] = array($shead['name']=>$v);
						}
						unset($cas);
					}
					$val = $td;

?>