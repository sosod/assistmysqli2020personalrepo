<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class MASTER_CONTRACT_SUPPLIER extends MASTER {
    /*************
     * CONSTANTS
     */
    const TABLE = "contract_supplier";
    const TABLE_FLD = "cs";
	
	
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getTableName() {
		return $this->getDBRef()."_".self::TABLE;
	}
	
    public function getTableField() { return self::TABLE_FLD; }
	
	public function addObject($contract_id,$supplier_id,$project_value) {
		$insert = array(
			'cs_contract_id'=>$contract_id,
			'cs_supplier_id'=>$supplier_id,
			'cs_project_value'=>$project_value,
			'cs_status'=>MASTER::ACTIVE,
		);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert);
		$id = $this->db_insert($sql);
		return $id;
	}

	public function editObject($object_id,$old_data,$supplier_id,$project_value) {
		$mar = 0;
		$insert = array(
			'cs_supplier_id'=>$supplier_id,
			'cs_project_value'=>$project_value,
		);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert)." WHERE cs_id = ".$object_id;
		$mar+= $this->db_update($sql);
		return $mar;
	}
	
	public function deactivateObject($object_id,$old_data) {
		$insert = array(
			'cs_status'=>($old_data['cs_status'] - MASTER::ACTIVE)
		);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert)." WHERE cs_id = ".$object_id;
		$mar+= $this->db_update($sql);
		return $mar;
	}
	
	
	
	
	

	public function getObjectByContractID($contract_id) {
		$sql = "SELECT CS.cs_project_value, CS.cs_supplier_id, contract_supplier.name as contract_supplier, CS.cs_supplier_id as id, CS.cs_project_value as budget 
				FROM ".$this->getTableName()." CS 
				INNER JOIN ".$this->getDBRef()."_list_contract_supplier contract_supplier ON CS.cs_supplier_id = contract_supplier.id
				WHERE CS.cs_contract_id = ".$contract_id." AND (CS.cs_status & ".MASTER::ACTIVE." = ".MASTER::ACTIVE.")";
		return $this->mysql_fetch_all($sql);
	}
	public function getObjectForSelectByContractID($contract_id) {
		$items = $this->getObjectByContractID($contract_id);
		$data = array();
		foreach($items as $i) {
			$data[$i['cs_supplier_id']] = $i['contract_supplier'];
		}
		return $data;
	}
	public function getRawObjectByContractID($contract_id) {
		$sql = "SELECT CS.* 
				FROM ".$this->getTableName()." CS 
				WHERE CS.cs_contract_id = ".$contract_id." AND (CS.cs_status & ".MASTER::ACTIVE." = ".MASTER::ACTIVE.")";
		return $this->mysql_fetch_all_by_id($sql,"cs_id");
	}

}  
?>