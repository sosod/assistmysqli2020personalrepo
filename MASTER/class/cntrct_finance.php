<?php
  
class CNTRCT_FINANCE extends CNTRCT_HELPER {
	
	//protected $table_name = "_";				//set in child class
	//protected $field_prefix = "f";			//set in child class
	//protected $ref_tag = "CF";				//set in child class
	
	//protected $id_field = "_id";	//set in child class
	//protected $parent_field = "_contract";	//set in child class
	//protected $status_field = "_status";		//set in child class
	//protected $primary_finance_field = "_amount";	//set in child class

	//protected $del_table_name = "_";				//set in child class
	//protected $del_field_prefix = "f";			//set in child class
	//protected $del_parent_field = "_contract";	//set in child class
	//protected $del_status_field = "_status";		//set in child class
	//protected $del_primary_finance_field = "_amount";	//set in child class
	
	//protected $object_type = "FINANCE_";			//set in child class
	
		
	//protected $js = "";						//set in child class

	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
		$this->id_field = $this->getTableField().$this->id_field;
		$this->parent_field = $this->getTableField().$this->parent_field;
		$this->status_field = $this->getTableField().$this->status_field;
		$this->primary_finance_field = $this->getTableField().$this->primary_finance_field;
		$this->secondary_finance_field = $this->getTableField().$this->secondary_finance_field;
		$this->extra_finance_field = $this->getTableField().$this->extra_finance_field;
		
		$this->del_parent_field = $this->getDeliverableTableField().$this->del_parent_field;
		$this->del_status_field = $this->getDeliverableTableField().$this->del_status_field;
		$this->del_primary_finance_field = $this->getDeliverableTableField().$this->del_primary_finance_field;
		if($this->del_secondary_finance_field!==false) {
			$this->del_secondary_finance_field = $this->getDeliverableTableField().$this->del_secondary_finance_field;
		}
		if($this->del_extra_finance_field!==false) {
			$this->del_extra_finance_field = $this->getDeliverableTableField().$this->del_extra_finance_field;
		}
		
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	//set in child classes
	/*public function getObject() {
		return array("error","parent function not programmed");
	}
	public function addObject() {
		return array("error","parent function not programmed");
	}
	public function editObject() {
		return array("error","parent function not programmed");
	}
	public function deleteObject() {
		return array("error","parent function not programmed");
	}*/
	
	public function addMyObject($var) {
		unset($var['attachments']);
		unset($var['object_id']); //remove incorrect deliverable_id value from add form
		$var[$this->getParentFieldName()] = $var['contract_id'];
		unset($var['contract_id']);
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		if($this->getMyObjectName()=="INCOME" || $this->getMyObjectName()=="EXPENSE") {
			$var[$this->primary_finance_field] = $var[$this->secondary_finance_field] + $var[$this->extra_finance_field];
		}
		$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$import_data = $this->convertArrayToSQL($var);

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$import_data;
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CREATE,
			);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","An unexpected error occurred.  Please try again.");
	}
	
	
	
	
	
	
	
	
	/***************************
	 * GET functions
	 */
	public function hasTarget() { return false; }	//needed for centralisation of display class
	public function hasDeadline() { return false; }	//needed for centralisation of display class
	
	public function getMyObjectType() { return $this->object_type; }
	public function getMyObjectName() { return str_ireplace("FINANCE_","",$this->object_type); }
	public function getLogTable() { return CNTRCT_FINANCE::LOG_TABLE; }
	public function getMyLogTable() { return CNTRCT_FINANCE::LOG_TABLE; }
	
	public function getRefTag() { return $this->ref_tag; }

	public function getTableName() { return $this->getDBRef().$this->table_name; }
	public function getTableField() { return $this->field_prefix; }

	public function getIDFieldName() {		return $this->id_field; 	}
	public function getParentFieldName() {		return $this->parent_field; 	}
	public function getStatusFieldName() {		return $this->status_field; 	}
	public function getPrimaryFinanceFieldName() {		return $this->primary_finance_field; 	}
	public function getSecondaryFinanceFieldName() {		return $this->secondary_finance_field; 	}

	public function getDeliverableTableName() { return $this->getDBRef().$this->del_table_name; }
	public function getDeliverableTableField() { return $this->del_field_prefix; }

	public function getDeliverableParentFieldName() {		return $this->del_parent_field; 	}
	public function getDeliverableStatusFieldName() {		return $this->del_status_field; 	}
	public function getDeliverablePrimaryFinanceFieldName() {		return $this->del_primary_finance_field; 	}
	public function getDeliverableSecondaryFinanceFieldName() {		return $this->del_secondary_finance_field; 	}
	public function getDeliverableExtraFinanceFieldName() {		return $this->del_extra_finance_field; 	}

	
	public function getExtraObjectFormJS() {	return $this->js; }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Function to get list of records for EDIT list table
	 * @param (Int) $parent_object_id = contract id
	 * @param (String) $section = which section of the module: MANAGE || ADMIN
	 * 
	 * @return (array) ('head'=>array of headings,'rows'=>array of records,'paging'=>paging array) 
	 */
	public function getListObjects($parent_object_id,$section="MANAGE") {
		$displayObject = new CNTRCT_DISPLAY_FINANCE();
		$listObject = new CNTRCT_LIST();
		
		$ref_tag = $this->getRefTag();
		$data = array(
			'head'=>array(),
			'rows'=>array(),
			'paging'=>array(
				'totalrows' => 0,
				'totalpages' => 0,
				'currentpage' => 0,
				'pagelimit' => 0,
				'first' => 0,
				'prev' => 0,
				'next' => 0,
				'last' => 0,			
			)
		);
		
		//HEADINGS
		$headObject = new CNTRCT_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(),"LIST",$section);
		$data['head'] = $this->replaceObjectNames($headings);
		
		//SQL
		$sql = "SELECT O.* ";
		$left_joins = array();
		foreach($headings as $fld=>$head) {
				$lj_tblref = "O";
				if($head['type']=="LIST") {
					$listObject->changeListType($head['list_table']);
					$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
										AND (".$head['list_table'].".status & ".CNTRCT::DELETED.") <> ".CNTRCT::DELETED;
				} elseif($head['type']=="MASTER") {
					$tbl = $head['list_table'];
					$masterObject = new CNTRCT_MASTER($fld);
					$fy = $masterObject->getFields();
					$sql.=", ".$fld.".".$fy['name']." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$fy['table']." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$fy['id'];
				} elseif($head['type']=="USER") {
					$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "INNER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
				} elseif($head['type']=="OWNER") {
					$ownerObject = new CNTRCT_CONTRACT_OWNER();
					$tbl = $head['list_table'];
					$dir_tbl = $fld."_parent";
					$sub_tbl = $fld."_child";
					$own_tbl = $fld;
					$sql.= ", CONCAT(".$dir_tbl.".dirtxt, ' - ',".$sub_tbl.".subtxt) as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$ownerObject->getChildTable()." AS ".$sub_tbl." ON ".$lj_tblref.".".$fld." = ".$sub_tbl.".subid";
					$left_joins[] = "INNER JOIN ".$ownerObject->getParentTable()." AS ".$dir_tbl." ON ".$sub_tbl.".subdirid = ".$dir_tbl.".dirid";
					$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_list_".$tbl." AS ".$own_tbl." ON ".$sub_tbl.".subid = ".$own_tbl.".owner_subid";
				}
		}
		$sql.= " FROM ".$this->getTableName()." O 
				".(count($left_joins)>0 ? implode(" ",$left_joins) : "")."
				WHERE O.".$this->getParentFieldName()." = ".$parent_object_id." 
				AND (O.".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		
		//FORMATTING
		foreach($rows as $key => $r) {
			$row = array();
			foreach($headings as $fld=>$head) {
				$type = $head['type'];
						if($headObject->isListField($head['type'])) {
							$row[$fld] = $r[$head['list_table']];
						} elseif($this->isDateField($fld) || $head['type']=="DATE") {
							$row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
						} else {
							$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
						}
			}
			$data['rows'][$key] = $row;
		}
		
		
		
		//END
		return $data;
	}
		
	
	
	
	
	
	
	/**
	 * Function to return raw data from the database
	 * @param (Int) $parent_object_id = contract id
	 * @param (String) $result_format = DEFAULT (as is from the database) || "SUMMARY" (SUM of primary finance field)
	 * 
	 * @return (Array)  mysql_fetch_all
	 */
	public function getRawRows($parent_object_id,$result_format = "DEFAULT", $get_secondary=false) {
		switch($result_format) {
			case "SUMMARY":
				if(!$get_secondary) {
					$sql = "SELECT IF(SUM(".$this->getPrimaryFinanceFieldName().")<>0,SUM(".$this->getPrimaryFinanceFieldName()."),0) as ".$this->getPrimaryFinanceFieldName();
				} else {
					$sql = "SELECT IF(SUM(".$this->getSecondaryFinanceFieldName().")<>0,SUM(".$this->getSecondaryFinanceFieldName()."),0) as ".$this->getSecondaryFinanceFieldName();
				}
				$sql_postfix = "";
				break;
			case "DEFAULT":
			default:
				$sql = "SELECT * ";
				$sql_postfix = "";
		}
		$sql.= " FROM ".$this->getTableName()
				." WHERE ".$this->getParentFieldName()." = ".$parent_object_id
				." AND (".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." ".$sql_postfix;
		$data = $this->mysql_fetch_all($sql); 
		return $data;
	}
	
	
	
	
	
	/****************************
	 * DELIVERABLE RELATED FUNCTIONS
	 */
	
	/**
	 * Function to return raw data from the database
	 * @param (Int) $parent_object_id = contract id
	 * @param (String) $result_format = DEFAULT (as is from the database) || "SUMMARY" (SUM of primary finance field)
	 * 
	 * @return (Array)  mysql_fetch_all
	 */
	public function getDeliverableRawRows($parent_object_id,$result_format = "DEFAULT", $get_for_secondary=false) {
		switch($result_format) {
			case "SUMMARY":
				if($get_for_secondary) {
					$sql = "SELECT IF(SUM(".$this->getDeliverableSecondaryFinanceFieldName().")>0,SUM(".$this->getDeliverableSecondaryFinanceFieldName()."),0) as ".$this->getDeliverableSecondaryFinanceFieldName();
				} else {
					$sql = "SELECT IF(SUM(".$this->getDeliverablePrimaryFinanceFieldName().")>0,SUM(".$this->getDeliverablePrimaryFinanceFieldName()."),0) as ".$this->getDeliverablePrimaryFinanceFieldName();
				}
				$sql_postfix = "";
				break;
			case "DEFAULT":
			default:
				$sql = "SELECT del.* ";
				$sql_postfix = "";
		}
		$delObject = new CNTRCT_DELIVERABLE();
		$sql.= " FROM ".$this->getDeliverableTableName()." as del 
				 INNER JOIN ".$this->getTableName()." as b
				     ON del.".$this->getDeliverableParentFieldName()." = b.".$this->getIDFieldName()."
				     AND b.".$this->getParentFieldName()." = ".$parent_object_id
				." INNER JOIN ".$delObject->getTableName()." as obj_del 
				  ON del.".$this->getDeliverableTableField()."_".$delObject->getIDFieldName()." = obj_del.".$delObject->getIDFieldName()
				  ." AND obj_del.".$delObject->getParentFieldName()." = b.".$this->getParentFieldName()
				." WHERE (b.".$this->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." AND (del.".$this->getDeliverableStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." AND (obj_del.".$delObject->getStatusFieldName()." & ".CNTRCT::ACTIVE.") = ".CNTRCT::ACTIVE
				." ".$sql_postfix; //echo $sql;
		$data = $this->mysql_fetch_all($sql);
		return $data;
	}
	 	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>