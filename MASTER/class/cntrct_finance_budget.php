<?php
  
class CNTRCT_FINANCE_BUDGET extends CNTRCT_FINANCE {
	
	protected $table_name = "_finance_budget";
	protected $field_prefix = "fb";
	
	protected $ref_tag = "CFB";
	
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $status_field = "_status";
	protected $primary_finance_field = "_budget_amount";
	protected $secondary_finance_field = "";
	protected $extra_finance_field = "";
	
	protected $del_table_name = "_deliverable_budget";
	protected $del_field_prefix = "db";
	protected $del_parent_field = "_fb_id";
	protected $del_status_field = "_status";
	protected $del_primary_finance_field = "_budget";
	protected $del_secondary_finance_field = "_perc_retention";
	protected $del_extra_finance_field = "_perc_completion";
	
	protected $js = "";
	
	protected $object_type = "FINANCE_BUDGET";

	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function getObject() {
		return array("error","function not programmed");
	}
	public function addObject($var) {
		return $this->addMyObject($var);
	}
	public function editObject() {
		return array("error","function not programmed");
	}
	public function deleteObject() {
		return array("error","function not programmed");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***************************
	 * FINANCE_BUDGET functions - Deliverable related functions below
	 */

	
	/** 
	 * Function to determine the data to be displayed in the Finance Summary table
	 * @param (INT) $parent_object_id = Contract ID
	 * 
	 * @return (Array)  ('head'=>array of headings,'rows'=>array of values)
	 */
	public function getSummaryData($parent_object_id) {
		$data = array('head'=>array(),'rows'=>array());
		
		$headings = array(
			'total'=>array(
				'name'=>"Total Budget",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'deliverables'=>array(
				'name'=>"Budget Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'unallocated'=>array(
				'name'=>"Unallocated Budget",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
		);
		
		$data['head'] = $headings;
		
		
		$total_budget = $this->getRawRows($parent_object_id,"SUMMARY");
		$data['rows']['total'] = $total_budget[0][$this->getPrimaryFinanceFieldName()];
		
		$deliverable_total_budget = $this->getDeliverableRawRows($parent_object_id,"SUMMARY");
		$data['rows']['deliverables'] = $deliverable_total_budget[0][$this->getDeliverablePrimaryFinanceFieldName()];
		$data['rows']['unallocated'] = $data['rows']['total'] - $data['rows']['deliverables'];
		
		
		return $data;
	}
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*******************************************
	 * DELIVERABLE Functions
	 */

	 	
	
	
	
	
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>