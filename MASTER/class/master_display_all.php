<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
class MASTER_DISPLAY_ALL extends ASSIST_MODULE_DISPLAY {
    
    
    public function __construct() {
        parent::__construct();
    }
    
	
	
	
	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */
	
	
	public function getDataFieldNoJS($type,$val,$options=array()) {
		$d = $this->getDataField($type, $val,$options);
		return $d['display'];
	}
	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type,$val,$options=array()) {
		echo $this->getDataField($type,$val,$options);
	}
	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 	 * 
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type,$val,$options=array()) {
		$data = array('display'=>"",'js'=>"");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val); 
				break;;
			case "CURRENCY":
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right']==true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val,$options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val,false);
				}
				break;
			case "BOOL":
				$data['display'] = $this->getBoolForDisplay($val);
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(isset($options['html']) && $options['html']==true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				}
				$data = array('display'=>$val,'js'=>"");
		}
		return $data;
	}
	
    
	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type,$options=array(),$val="") {
		//echo "dFF VAL:".$val." ARR: "; print_r($options);
		$ff = $this->createFormField($type,$options,$val);
		//if(is_array($ff['display'])) {
			//print_r($ff);
		//}
		echo $ff['display'];
		return $ff['js'];
	}
	/** 
	 * Returns string of selected form field
	 * 
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:".$val." ARR: "; print_r($options);
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "SMLVC":
				$data=$this->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$this->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				$data=$this->getLimitedTextArea($val,$options);
				break;
			case "TEXT":
				$data=$this->getTextArea($val,$options);
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getSelect($val,$options,$items);
				break;
			case "DATE":
				$extra = $options['options'];
				unset($options['options']);
				$data=$this->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$this->getColour($val,$options);
				break;
			case "RATING":
				$data=$this->getRating($val, $options);
				break;	
			case "CURRENCY":
			case "PERC":
			case "PERCENTAGE":
			case "NUM":
                $data=$this->getNumInputText($val,$options);
                if($type=="CURRENCY") {
                      $data['display']="R ".$data['display'];
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="%";
                }
                break;
			case "BOOL_BUTTON":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */
	
	
	
	
	/*****
	 * returns the details table for the object it is fed.
	 * 
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 * 
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new MASTER_ACTION();
				break;
			case "CONTRACT":
				if($child_type=="ACTION") {
					$myObject = new MASTER_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new MASTER_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new MASTER_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id);
	}
	public function drawDetailedView($object_type,$object_id,$include_parent_button=false, $sub_table=false) {
		$me = $this->getDetailedView($object_type, $object_id,$include_parent_button, $sub_table);
		echo $me['display'];
		return $me['js'];
	}
	public function getDetailedView($object_type,$object_id,$include_parent_button=false, $sub_table=false) {
		$js = "";
		$echo = "";
		$parent_buttons = "";
		switch($object_type) {
			case "ACTION": 
				$myObject = new MASTER_ACTION();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getDeliverableObjectName()."' class='float btn_parent' id=DELIVERABLE /> <input type=button value='".$myObject->getContractObjectName()."' class='float btn_parent' id=CONTRACT />";
				} 
				break;
			case "DELIVERABLE": 
				$myObject = new MASTER_DELIVERABLE();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getContractObjectName()."' class='float btn_parent' id=CONTRACT />";
				} 
				break;
			case "CONTRACT": 
				$myObject = new MASTER_CONTRACT();
				if($include_parent_button) {
					$parent_buttons = "<input class='float btn_parent' type=button id='view_all' value='View All' />";
				} 
				break;
			case "FINANCE":
				$myObject = new MASTER_FINANCE();
				break;
			case "TEMPCON":
				$myObject = new MASTER_TEMPLATE();
				break;
		}
		if($object_type=="FINANCE") {
			$result = $myObject->getDetailedObjectForDisplay($object_id);
			$js = $result['js'];
			$echo = $result['display'];
		} else {
			if($include_parent_button) {
				$js = "
						$('input:button.btn_parent').button().click(function() {
							var my_window = AssistHelper.getWindowSize();
							var w = (my_window.width*".($object_type!="CONTRACT" ? "0.5" :"0.9").").toFixed(0);
							var h = (my_window.height*0.9).toFixed(0);
							var i = $(this).prop('id');
							var dta = 'child_type=".$object_type."&object_type='+i+'&child_id='+".$object_id."
							var x = AssistHelper.doAjax('inc_controller.php?action=Display.getParentDetailedView',dta);
							$('<div />',{html:x.display,title:x.title}).dialog({
								width: w,
								height: h,
								modal: true
							}).find('table.th2 th').addClass('th2');
						});";
			}
			if($object_type != "TEMPCON"){
				$object = $myObject->getObject(array('type'=>"DETAILS",'id'=>$object_id));
			}else{
				$object = $myObject->getListObject($object_id, "CON");
			}
			
			$echo.="
				$parent_buttons
			<h2 class='".($sub_table !== false ? "sub_head":"")."'>".$myObject->getObjectName($object_type)." Details</h2>
					<table class='form ".($sub_table !== false ? "th2":"")."' width=100%>";
					foreach($object['head'] as $fld => $head) {
						if(isset($object['rows'][$fld])) {
							$val = $object['rows'][$fld];
							if(is_array($val)) {
								if(isset($val['display'])) {
									$js.=$val['js'];
									$val = $val['display'];
								} else {
									$v = "<table class=th2 width=100%>";
									foreach($val as $a) {
										foreach($a as $b => $c)
										$v.="<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
									}
									$v.="</table>";
									$val = $v;
								}
							}
						} else { $val = $fld; }
						$echo.= "
						<tr>
							<th width=40%>".$head['name'].":</th>
							<td ".($val==$fld ? "class=idelete" : "").">".$val."</td>
						</tr>";
					}
			$echo.="</table>";
		}
		return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type).($object_type!="FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
	}
	
		
















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */



	
	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}
	public function getPaging($i,$options,$button,$object_type="",$object_options=array()) {
		/**************
		$options = array(
			'totalrows'=> mysql_num_rows(),
			'totalpages'=> totalrows / pagelimit,
			'currentpage'=> start / pagelimit,
			'first'=> start==0 ? false : 0,
			'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
			'prev'=> start==0 ? false : (start - pagelimit),
			'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
			'pagelimit'=>pagelimit,
		);
		 **********************************/
		$data = array('display'=>"",'js'=>"");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier'><span id='$i'>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
			for($p=1;$p<=$options['totalpages'];$p++) {
				$data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
			}
		$data['display'].="</select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
						<span class=float>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span>
					</span></td>
				</tr>
			</table>";
			$op = "options[set]=true";
			foreach($object_options as $key => $v) {
				$op.="&options[$key]=$v";
			}
		$data['js'] = "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';
		
		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i." button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
		return $data;
	}
	
	
	/*****
	 * List View
	 */
	public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false) {
		$me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table);
		echo $me['display'];
		return $me['js'];
	}
	public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false) {
		//ASSIST_HELPER::arrPrint($child_objects); 
		$data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
		//$items_total = $contractObject->getPageLimit();
		//$these_items = array_chunk($child_objects['rows'], $items_total);
		$page_id = (isset($button['pager']) ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		if($button !== false){
			$paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options);
			$data['display'].=$paging['display'];
			$data['js'].=$paging['js'];
		}
		
		$data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' width=100%>
				<tr id='head_row'>
					";
					foreach($child_objects['head'] as $fld=>$head) {
						$data['display'].="<th>".$head['name']."</th>";
					}
					
				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
			$rows = $this->getListTableRows($child_objects, $button);
			$data['display'].=$rows['display']."</table>";
			$data['display'].="</td></tr></table>";
			$data['js'].=$rows['js'];
		return $data;
	}
	public function getListTableRows($child_objects,$button) {
		$data = array('display'=>"",'js'=>"");
		if(count($child_objects['rows'])==0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				$data['display'].="<tr>";
				foreach($obj as $fld=>$val) {
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'].="<td>".$val['display']."</td>";
							$data['js'].=isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'].="<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'].="<td>".$val."</td>";
					}
				}
				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class=".$button['class']." /></td>" : "");
				$data['display'].="</tr>";
			}
		}
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Function generate the object form 
	 */
	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path) {
		
		$headingObject = new MASTER_HEADINGS();
		
		$data = array('display'=>"",'js'=>"");
		$js = "";
		$echo = "";
		
		$headings = $headingObject->getMainObjectHeadings($form_object_type,"FORM");
		
		$js.="
				var page_action = '".ucwords($form_object_type).".".$page_action."';
				var page_direct = '".$page_redirect_path."';
				
		";
		
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE);


		$echo.="
			<div id=div_error class=div_frm_error>
				
			</div>
			<form name=frm_object>
				<table class=form width=100%>";
			$form_valid8 = true;
			$form_error = array();
			foreach($headings['rows'] as $fld => $head) {
				if($head['parent_id']==0) {
					$val = "";
					$h_type = $head['type'];
					if($h_type!="HEADING") {
						$display_me = true;
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST") {
							if($head['section']=="DELIVERABLE_ASSESS") {
								if($parentObject->mustIDoAssessment()===FALSE || $parentObject->mustIAssignWeights()===FALSE) {
									$display_me = false;
								} else {
									//validate the assessment status field here
									$lt = explode("_",$head['list_table']);
									switch($lt[1]) {
										case "quality": $display_me = $parentObject->mustIAssessQuality(); break;
										case "quantity": $display_me = $parentObject->mustIAssessQuantity(); break;
										case "other": $display_me = $parentObject->mustIAssessOther(); break;
									}
								}
							}
							if($display_me) {
								$list_items = array();
								$listObject = new MASTER_LIST($head['list_table']);
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER","TEMPLATE","DEL_TYPE","DELIVERABLE"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new MASTER_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break; 
								case "TEMPLATE":
									$listObject = new MASTER_TEMPLATE();
									$list_items = $listObject->getActiveTemplatesFormattedForSelect();
									break;
								case "OWNER":
									$listObject = new MASTER_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									$listObject = new MASTER_MASTER($head['list_table']);
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break; 
								case "DELIVERABLE":
									$listObject = new MASTER_DELIVERABLE();
									$list_items = $listObject->getDeliverablesForParentSelect($parent_object_id);
									break; 
								case "DEL_TYPE":
									$listObject = new MASTER_DELIVERABLE();
									$list_items = $listObject->getDeliverableTypes($parent_object_id);
									break; 
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $childObject->getRefTag().$object_id;
							}
						} else {
							if($is_edit_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$js.=$display['js'];
						}
					} else {//if($fld=="contract_supplier") {
						if($is_edit_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}
						if($fld=="contract_assessment_statuses") {
							$listObject = new MASTER_LIST("deliverable_status");
							$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
							$last_deliverable_status = 0;
							$sub_head = array();
							$sh_type = "BOOL";
							foreach($list_items as $key => $status) {
								$sub_head[$fld."-".$key] = array(
									'field'=>$fld."-".$key,
									'type'=>$sh_type,
									'name'=>$status,
									'required'=>1,
									'parent_link'=>"",
									'default_value'=>($key==5 ? "1" : "0"),
								);
								$last_deliverable_status = $key;
							}
							unset($listObject);
						} else {
							$sub_head = $headings['sub'][$head['id']];
						}
						$td = "
						<div class=".$fld."><span class=spn_".$fld.">
							<table class=sub_form width=100%>";
							$add_another[$fld] = false;
							foreach($sub_head as $shead) {
								$sh_type = $shead['type'];
								$sfld = $shead['field'];
								if($fld=="contract_supplier") {
									$options = array('name'=>$sfld."[]",'req'=>$head['required']);
								} else {
									$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
								}
								$val = "";
								if($sh_type=="LIST") {
									$list_items = array();
									$listObject = new MASTER_LIST($shead['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									$options['options'] = $list_items;
									if(count($list_items)==0 && $shead['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$shead['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									if(count($list_items)>1) {
										$add_another[$fld] = true;
									}
									unset($listObject);
								} elseif($sh_type=="BOOL"){
									$sh_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
								} elseif($sh_type=="CURRENCY") {
									$options['extra'] = "processCurrency";
									$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
								}
								$sdisplay = $this->createFormField($sh_type,$options,$val);
								$js.= $sdisplay['js'];
								$td.="
								<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
									<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
									<td>".$sdisplay['display']."</td>
								</tr>";
							}
						$td.= "
							</table></span>
							".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
						</div>";
						$display = array('display'=>$td);
					} 
					if($display_me) {
						$echo.= "
						<tr ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
				}
			}
			if(!$form_valid8) {
				$js.="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			}
			$echo.="<tr>
					<th></th>
					<td>
						<input type=button value=\"Save ".$formObject->getObjectName($form_object_type)."\" class=isubmit id=btn_save />
						".($form_object_type=="CONTRACT" ? "<input type=hidden name=last_deliverable_status value='".$last_deliverable_status."' />" : "")."
						<input type=hidden name=object_id value=".$form_object_id." />
						".(($form_object_type!="CONTRACT" && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />" : "")."
					</td>
				</tr>
			</table>
		</form>";
		$data['js'].="
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($form_object_type)."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"
				
				
				".$js."
				
				";
		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'].="
				
				
				$(\"form[name=frm_object] select\").each(function() {
					if($(this).children(\"option\").length<=1 && !$(this).hasClass(\"required\")) {
					}
				});
				$(\"#btn_save\").click(function() {
					$"."form = $(\"form[name=frm_object]\");
					CntrctHelper.processObjectForm($"."form,page_action,page_direct);
					return false;
				});
				
				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}
				
				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");		
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('div.".$key." span:first-child').html();
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('table:last').after(".$key.");
							});
							";
						}
					}
				}
				
				$data['js'].="
				$(\"#del_type\").change(function() {
					if($(this).val()==\"SUB\") {
						$(\".tr_del_type\").show();
					} else {
						$(\".tr_del_type\").hide();
					}
				});
				
				if($(\"#del_parent_id\").children(\"option\").length<=1) {
					$(\"#del_type option[value=SUB]\").prop(\"disabled\",true);
				}
				$(\"#del_type\").trigger(\"change\");
				
				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});
				
				//alert(AssistForm.serialize($(\"form[name=frm_deliverable]\")));
				
				$(\"form[name=frm_object] select\").each(function() {
					//alert($(this).children(\"option\").length);
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});
				
			";
				
				
		$data['display'] = $echo;
		return $data;		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** 
	 * Returns filter for selecting parent objects according to section it is fed
	 * 
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
		$data['display']="
			<div id='filter'>";
				
		switch (strtoupper($section)) {
			case 'CONTRACT':
				$data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index=>$key){
					$data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js']="
					fyear = 0;
					
					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;
			
			case 'DELIVERABLE':
				$dsp = "contracts get";
				
				break;
			
			case 'ACTION':
				$dsp = "deliverables get";
				
				break;
			
			default:
				$dsp = "Invalid arguments supplied";
				break;
		}
		
		$data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";
		
        return $data;
    }
	

	/** 
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 * 
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from MASTER_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from MASTER_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
    public function getListView($section, $fyears=array(), $headings=array()){
    	$data = array('display'=>"",'js'=>"");
			switch(strtoupper($section)){
				case "CONTRACT":
					$filter = $this->getFilter($section, $fyears);
					//For the financial year filter
					$data['display'] = $filter['display'];
					//For the list view table
					$data['display'].= 
									'<table class=tbl-container><tr><td>
										<table class=list id=master_list>	
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';
					
												foreach($headings as $key=>$val){
													$string = "<th id='".$val['field']."' >".$val['name']."</th>";
													$data['display'].= $string;
												}
					$data['display'].=
													"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
					//For the javascript
					
					break;
				default:
					$data['display'] = "Lol, not for you";
					break;
			}
		
		return $data;
    }
}


?>