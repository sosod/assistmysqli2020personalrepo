<?php

class CNTRCT_FINANCE_EXPENSE extends CNTRCT_FINANCE {
	
	
	protected $object_type = "FINANCE_EXPENSE";
	protected $ref_tag = "CFE";

	protected $table_name = "_finance_expense";
	protected $field_prefix = "fe";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $status_field = "_status";
	protected $primary_finance_field = "_total_amount";
	protected $secondary_finance_field = "_retention";
	protected $extra_finance_field = "";
	
	protected $del_table_name = "_deliverable_expense";
	protected $del_field_prefix = "de";
	protected $del_parent_field = "_fe_id";
	protected $del_status_field = "_status";
	protected $del_primary_finance_field = "_amount";
	protected $del_secondary_finance_field = "_retention_amount";
	protected $del_extra_finance_field = "";
	
	


	protected $js = "";

	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function getObject() {
		return array("error","function not programmed");
	}
	public function addObject($var) {
		//return array("error","function not programmed");
		return $this->addMyObject($var);
	}
	public function editObject() {
		return array("error","function not programmed");
	}
	public function deleteObject() {
		return array("error","function not programmed");
	}
	
	
	
	
	
	
	
	
	
	
	/***************************
	 * FINANCE functions
	 */

	
	/** 
	 * Function to determine the data to be displayed in the Finance Summary table
	 * @param (INT) $parent_object_id = Contract ID
	 * 
	 * @return (Array)  ('head'=>array of headings,'rows'=>array of values)
	 */
	public function getSummaryData($parent_object_id) {
		$data = array('head'=>array(),'rows'=>array());
		
		$headings = array(
			'total'=>array(
				'name'=>"Total Expenses",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'deliverables'=>array(
				'name'=>"Expenses Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'unallocated'=>array(
				'name'=>"Unallocated Expenses",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
			'retention'=>array(
				'name'=>"Retention Held",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'retention_deliverables'=>array(
				'name'=>"Retention Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'retention_unallocated'=>array(
				'name'=>"Unallocated Retention",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
		);
		
		$data['head'] = $headings;
		
		
		$total = $this->getRawRows($parent_object_id,"SUMMARY");
		$data['rows']['total'] = $total[0][$this->getPrimaryFinanceFieldName()];

		$deliverable_total = $this->getDeliverableRawRows($parent_object_id,"SUMMARY");
		$data['rows']['deliverables'] = $deliverable_total[0][$this->getDeliverablePrimaryFinanceFieldName()];
		$data['rows']['unallocated'] = $data['rows']['total'] - $data['rows']['deliverables'];
		
		$ret_total = $this->getRawRows($parent_object_id,"SUMMARY",true);
		$data['rows']['retention'] = $ret_total[0][$this->getSecondaryFinanceFieldName()];

		$ret_deliverable_total = $this->getDeliverableRawRows($parent_object_id,"SUMMARY",true);
		$data['rows']['retention_deliverables'] = $ret_deliverable_total[0][$this->getDeliverableSecondaryFinanceFieldName()];
		$data['rows']['retention_unallocated'] = $data['rows']['retention'] - $data['rows']['retention_deliverables'];
		
		return $data;
	}
	
		
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>